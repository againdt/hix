/**
 * 
 */
package com.getinsured.hix.batch.enrollment.util;

import java.util.List;

/**
 * @author negi_s
 *
 */
public class Enrollment1095XmlWrapper {
	
	private List<Integer> partitionList;
	private boolean isFreshRun;
	private String originalBatchId;
	private String batchCategoryCode;
	private String batchId;
	private String fileCreationTimeStamp;
	private Integer coverageYear;
	private Integer numberOfFiles = 0;
	private Long fileSize = 0l;
	private Integer recordCount = 0;
	private Integer skippedCount = 0;
	private boolean voidIndicator = false;
	private boolean regenerateIndicator = false;
	
	public Enrollment1095XmlWrapper(List<Integer> partitionList, boolean isFreshRun, String originalBatchId,
			String batchCategoryCode, Integer coverageYear, String batchId, String fileCreationTimeStamp) {
		this.partitionList = partitionList;
		this.isFreshRun = isFreshRun;
		this.originalBatchId = originalBatchId;
		this.batchCategoryCode = batchCategoryCode;
		this.coverageYear = coverageYear;
		this.batchId = batchId;
		this.fileCreationTimeStamp = fileCreationTimeStamp;
	}
	
	public Enrollment1095XmlWrapper(List<Integer> partitionList, boolean isFreshRun, String originalBatchId,
			String batchCategoryCode, Integer coverageYear, String batchId, String fileCreationTimeStamp, boolean voidIndicator) {
		this.partitionList = partitionList;
		this.isFreshRun = isFreshRun;
		this.originalBatchId = originalBatchId;
		this.batchCategoryCode = batchCategoryCode;
		this.coverageYear = coverageYear;
		this.batchId = batchId;
		this.fileCreationTimeStamp = fileCreationTimeStamp;
		this.voidIndicator = voidIndicator;
	}
	
	public Enrollment1095XmlWrapper(List<Integer> partitionList, boolean isFreshRun, String originalBatchId,
			String batchCategoryCode, Integer coverageYear, String batchId, String fileCreationTimeStamp, boolean voidIndicator, boolean regenerateIndicator) {
		this.partitionList = partitionList;
		this.isFreshRun = isFreshRun;
		this.originalBatchId = originalBatchId;
		this.batchCategoryCode = batchCategoryCode;
		this.coverageYear = coverageYear;
		this.batchId = batchId;
		this.fileCreationTimeStamp = fileCreationTimeStamp;
		this.voidIndicator = voidIndicator;
		this.regenerateIndicator = regenerateIndicator;
	}
	
	public Integer getNumberOfFiles() {
		return numberOfFiles;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public Integer getSkippedCount() {
		return skippedCount;
	}

	public List<Integer> getPartitionList() {
		return partitionList;
	}
	public void setPartitionList(List<Integer> partitionList) {
		this.partitionList = partitionList;
	}
	public boolean isFreshRun() {
		return isFreshRun;
	}
	public void setFreshRun(boolean isFreshRun) {
		this.isFreshRun = isFreshRun;
	}
	public String getOriginalBatchId() {
		return originalBatchId;
	}
	public void setOriginalBatchId(String originalBatchId) {
		this.originalBatchId = originalBatchId;
	}
	public String getBatchCategoryCode() {
		return batchCategoryCode;
	}
	public void setBatchCategoryCode(String batchCategoryCode) {
		this.batchCategoryCode = batchCategoryCode;
	}

	public Integer getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}

	public void setNumberOfFiles(Integer numberOfFiles) {
		this.numberOfFiles = numberOfFiles;
	}

	public  void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public synchronized void incrementRecordCount(Integer incrementBy) {
		this.recordCount += incrementBy;
	}

	public synchronized void incrementSkippedCount(Integer incrementBy) {
		this.skippedCount += incrementBy;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getFileCreationTimeStamp() {
		return fileCreationTimeStamp;
	}

	public void setFileCreationTimeStamp(String fileCreationTimeStamp) {
		this.fileCreationTimeStamp = fileCreationTimeStamp;
	}

	public boolean isVoidIndicator() {
		return voidIndicator;
	}

	public void setVoidIndicator(boolean voidIndicator) {
		this.voidIndicator = voidIndicator;
	}
	public boolean isRegenerateIndicator() {
		return regenerateIndicator;
	}
	public void setRegenerateIndicator(boolean regenerateIndicator) {
		this.regenerateIndicator = regenerateIndicator;
	}
}
