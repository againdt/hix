package com.getinsured.hix.broker.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.broker.repository.IExternalEmployerRepository;
import com.getinsured.hix.model.ExternalEmployer;

/**
 * Implements {@link ExternalEmployerService} to find and save external
 * employers
 * 
 */
@Service("externalEmployerService")
@Transactional
public class ExternalEmployerServiceImpl implements ExternalEmployerService {

	@Autowired
	private IExternalEmployerRepository externalEmployerRepository;

	/**
	 * Finds external employer based on employer case id(same as external employer id)
	 * 
	 * @see ExternalEmployerService#findByExternalEmployerId(long)
	 * 
	 * @param externalEmpId
	 * @return ExternalEmployer
	 * 
	 */
	@Override
	public ExternalEmployer findByExternalEmployerId(long externalEmpId) {
		return externalEmployerRepository.getExternalEmployer(externalEmpId);
	}
}