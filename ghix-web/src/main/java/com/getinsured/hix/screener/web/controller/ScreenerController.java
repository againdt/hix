package com.getinsured.hix.screener.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.affiliate.model.AffiliateClick;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.EligLead.STAGE;
import com.getinsured.hix.model.estimator.mini.EligLeadRequest;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorResponse;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.screener.util.ScreenerUtil;
import com.getinsured.hix.ui.model.eligibility.prescreen.PhixRequest;

/**
 * This controller class handles requests from the private exchange home page.
 *
 * @see HIX-14382
 *
 * @author Nikhil Talreja, Sunil Desu
 * @since 19 August, 2013
 */

@Controller
public class ScreenerController extends ScreenerUtil {

	private static final Logger LOGGER = Logger
			.getLogger(ScreenerController.class);

	@Autowired
	private ZipCodeService zipCodeService;
	
	
	//AffId and URL for legacy site
	private String affId;
	@Value("261")
	public void setAffId(String affId) {
		this.affId = affId;
	}
	
	private String legacySite;
	@Value("")
	public void setLegacySite(String legacySite) {
		this.legacySite = legacySite;
	}
	
	
	/**
	 * This method handles the GET request for the Private Exchange home page for the MVP version.
	 *
	 * @param model
	 *            - Spring Model
	 * @return String - return URL of the home page
	 *
	 * @author Nikhil Talreja, Sunil Desu
	 * @since 10 September, 2013
	 * 
	 * Removed Request Mapping - HIX-65716
	 */
	public String getPhixHomeMvp(Model model, HttpServletRequest request, 
			@RequestParam(value = CLICK_ID, required = false, defaultValue="-1") long clickId, 
			@RequestParam(value = LEAD_ID, required = false, defaultValue="-1") long leadId ) {

		//Check if click Id is present in session, if yes, use it fetch click record
		long clickIdFromSession = 0;

		if (request.getSession().getAttribute(CLICK_ID) != null
				&& StringUtils.isNumeric(request.getSession()
						.getAttribute(CLICK_ID).toString())) {
			clickIdFromSession = (new Long(request.getSession()
					.getAttribute(CLICK_ID).toString()));
			if (clickIdFromSession > 0) {
				clickId = clickIdFromSession;
				LOGGER.debug("Using click Id from session " + clickId);
			}
		}

		LOGGER.info("Getting Private Exchange home page");
		LOGGER.debug("Lead Id " + leadId);
		/*
		 * PHIX home page interaction will have to handled in the following way:
		 * 1. Retrieve the AFF_CLICK record based on clickId 2. If leadId (in
		 * above record) is not null and has a value, retrieve the record from
		 * ELIG_LEAD table which has all the household data 3. If leadId is
		 * null, retrieve data from AFF_CLICK and generate an ELIG_LEAD entry 4.
		 * If clickId is null but leadId is not (Request has come from member
		 * portal), fetch details from ELIG_LEAD table 5. if clickId and leadId
		 * both are null, send a blank request to home page
		 * 
		 * leadId should be in session and not in phixRequest
		 */

		PhixRequest phixRequest = null;
		request.getSession().setAttribute(LEAD_ID, leadId);
		if (clickId > 0) {
			LOGGER.debug("Using click Id from request parameter: " + clickId);
			AffiliateClick clickRecord = fetchClickRecord(clickId);
			if (clickRecord != null) {

				long leadIdFromClickTable = clickRecord.getLeadId();

				if (leadIdFromClickTable > 0) {
					EligLead eligLeadRecord = fetchEligLeadRecord(leadIdFromClickTable);
					if (eligLeadRecord != null) {
						phixRequest = createPhixRequest(eligLeadRecord,0);
						request.getSession().setAttribute(LEAD_ID,
								leadIdFromClickTable);
					}
				} else {
					LOGGER.info("Creating PHIX request from AFF_CLICK record");
					phixRequest = createPhixRequest(clickRecord);
					request.getSession().setAttribute(LEAD_ID, leadId);
				}
			} else {
				LOGGER.info("No record exists in AFF_CLICK table for click id "
						+ clickId);
			}
		} else {
			LOGGER.info("No click id recieved in request, checking for lead Id");
			if (leadId > 0) {
				EligLead eligLeadRecord = fetchEligLeadRecord(leadId);
				if (eligLeadRecord != null) {
					LOGGER.info("Creating PHIX request from ELIG_LEAD record");
					phixRequest = createPhixRequest(eligLeadRecord,0);
					request.getSession().setAttribute(LEAD_ID, leadId);
				}
			} else {
				LOGGER.info("No lead id recieved in request");
			}
		}

		request.getSession().setAttribute(CLICK_ID, clickId);

		model.addAttribute(PHIX_REQUEST, phixRequest);
		return "phixhomemvp";
	}

	/**
	 * This method is the AJAX Call to calculate savings for PHIX eligibility
	 *
	 * @param phixRequest
	 *            - The request for which savings need to be calculated
	 * @return PhixRequest - The updated request having savings value
	 *
	 * @author Nikhil Talreja
	 * @since August 20, 2013
	 */
	@RequestMapping(value = "calculateSavings", method = RequestMethod.POST)
	@ResponseBody
	public PhixRequest calculateSavings(@RequestBody PhixRequest phixRequest,HttpServletRequest httpRequest) {

		LOGGER.info("Calculating Savings for request");
		LOGGER.info(phixRequest);

		try {
			
			//Check if call to API is required, else create/update record in ELIG_LEAD table alone
			if(StringUtils.equalsIgnoreCase(phixRequest.getCallToApiRequired(), "Y")){
				
				MiniEstimatorResponse estimatorResponse = callEstimatorApi(phixRequest,httpRequest);
			
				phixRequest.setAptc(estimatorResponse.getAptcAmount());
				phixRequest.setAptcEligible(estimatorResponse.getAptcEligible());
				phixRequest.setAllPlansAvailable(estimatorResponse.getAllPlansAvailable());
				phixRequest.setPlanInfoAvailable(estimatorResponse.getPlanInfoAvailable());
				phixRequest.setExpectedMonthlyPremium(estimatorResponse.getExpectedMonthlyPremium());
				phixRequest.setSlspPremiumAfterAptc(estimatorResponse.getSlspMonthlyPremiumAfterAPTC());
				phixRequest.setSlspPremiumBeforeAptc(estimatorResponse.getSlspMonthlyPremiumBeforeAPTC());
				phixRequest.setLbpPremiumAfterAptc(estimatorResponse.getLbpMonthlyPremiumAfterAPTC());
				phixRequest.setLbpPremiumBeforeAptc(estimatorResponse.getLbpMonthlyPremiumBeforeAPTC());
				phixRequest.setLspPremiumAfterAptc(estimatorResponse.getLspMonthlyPremiumAfterAPTC());
				phixRequest.setLspPremiumBeforeAptc(estimatorResponse.getLspMonthlyPremiumBeforeAPTC());
				phixRequest.setMedicaidEligible(estimatorResponse.getMedicaidHousehold());
				phixRequest.setMedicareEligible(findChipMedicareEligibility(estimatorResponse.getMembers(),"Medicare"));
				phixRequest.setChipEligible(findChipMedicareEligibility(estimatorResponse.getMembers(),"CHIP"));
				phixRequest.setChipHousehold(estimatorResponse.getChipHousehold());
				phixRequest.setMedicareHousehold(estimatorResponse.getMedicareHousehold());
				phixRequest.setNoOfAptcCsr(findNoOfMembersForEligibilityType(estimatorResponse.getMembers(),"APTC")
						+ findNoOfMembersForEligibilityType(estimatorResponse.getMembers(),"APTC/CSR"));
				phixRequest.setNoOfChip(findNoOfMembersForEligibilityType(estimatorResponse.getMembers(), "CHIP"));
				phixRequest.setNoOfMedicare(findNoOfMembersForEligibilityType(estimatorResponse.getMembers(), "Medicare"));
				phixRequest.setNoOfMedicaid(findNoOfMembersForEligibilityType(estimatorResponse.getMembers(), "Medicaid"));
				phixRequest.setCallUs(getCallUsInfo(phixRequest,estimatorResponse));
				phixRequest.setSbeWebsite(estimatorResponse.getHixURL());
				phixRequest.setStateName(getStateName(phixRequest.getStateCode()));
				phixRequest.setCsrLevel(estimatorResponse.getCsrLevelSilver());
				phixRequest.setStatus(estimatorResponse.getStatus());
				
				httpRequest.getSession().setAttribute(LEAD_ID, estimatorResponse.getLeadId());
			}
			else{
				LOGGER.info("Call to API will be skipped for this request");
				EligLead record = populateEligLeadRecord(phixRequest);
				
				if(httpRequest.getSession().getAttribute(LEAD_ID) !=null
						&& StringUtils.isNumeric(httpRequest.getSession().getAttribute(LEAD_ID).toString())){
					record.setId(new Long(httpRequest.getSession().getAttribute(LEAD_ID).toString()));
				}
				
				if(httpRequest.getSession().getAttribute(CLICK_ID) !=null
						&& StringUtils.isNumeric(httpRequest.getSession().getAttribute(CLICK_ID).toString())){
					record.setClickId(new Long(httpRequest.getSession().getAttribute(CLICK_ID).toString()));
				}
				
				
				record.setStage(STAGE.PRE_SHOPPING);
				record = createEligLeadRecord(record);
				httpRequest.getSession().setAttribute(LEAD_ID, record.getId());
			}
			//Adding affid and form submission URL for legacy site re-direction
			phixRequest.setAffFlowCustomField1(affId);
			Object affIfFromSession = httpRequest.getSession().getAttribute(AFF_CUSTOM_FIELD_1);
			if(affIfFromSession != null){
				phixRequest.setAffFlowCustomField1(affIfFromSession.toString());
			}
			phixRequest.setLegacySiteUrl(legacySite);
			
		} catch (Exception e) {
			LOGGER.error("Exception occured while calculating savings", e);
			phixRequest.setStatus(FAILURE);
		}
		
		return phixRequest;
	}

	/**
	 * This method gets the pre launch results page for private exchange
	 *
	 * @param model
	 *            - SPring UI model
	 * @return String - return URL of the home page
	 * @author Nikhil Talreja
	 * @since 19 August, 2013
	 */
	//@RequestMapping(value = "phix/prelaunch", method = RequestMethod.POST)
	public String getPreLaunchResultsPage(Model model, HttpServletRequest httpRequest) {
		
		String phixRequest = httpRequest.getParameter(PHIX_REQUEST);
		
		if(phixRequest != null){
			
			//LOGGER.debug("Request " + phixRequest.replaceAll("&quot;", "\""));
			try{
				ObjectMapper mapper = new ObjectMapper();
				PhixRequest request = mapper.readValue(phixRequest.replaceAll("&quot;", "\""), PhixRequest.class);
				if(request.getHouseholdIncome() < 0){
					request.setHouseholdIncome(-1);
				}
				else{
					request.setHouseholdIncome(new Double(request.getHouseholdIncome()).intValue());
				}
				model.addAttribute(PHIX_REQUEST, request);
			}
			catch(Exception e){
				LOGGER.error("Exception occured while getting request",e);
			}
			
			String stateCode = httpRequest.getParameter("stateCode");
			LOGGER.debug("State Code " + stateCode);
			model.addAttribute("showStateMessage",GI_SUPPORTED_STATE.get(stateCode));
			model.addAttribute(STATE_NAME,new StateHelper().getStateName(stateCode));
			
			
			String peopleNeedingCoverage = httpRequest
					.getParameter("peopleNeedingCoverage");
			if(StringUtils.isBlank(peopleNeedingCoverage)){
				peopleNeedingCoverage = "0";
			}
			model.addAttribute("peopleNeedingCoverage", peopleNeedingCoverage);
			String usage = httpRequest.getParameter("usage");
			if(StringUtils.isBlank(usage)){
				usage = "No response entered.";
			}
			model.addAttribute("usage",usage);
			
			String countyName = httpRequest.getParameter("countyName");
			String name = httpRequest.getParameter("name");
			String email = httpRequest.getParameter("emailValue");
			String phone = httpRequest.getParameter("phoneValue");
			String isOkToCall = httpRequest.getParameter("isOkToCall");
			model.addAttribute("countyName", countyName);
			model.addAttribute("name", name);
			model.addAttribute("email", email);
			model.addAttribute("phone", phone);
			model.addAttribute("isOkToCall", isOkToCall);
		}
		
		return "phix/prelaunch";
	}
	
	/**
	 * This method is the AJAX call to update ELIG_LEAD record with details submitted by the user
	 * on pre-launch results page
	 *
	 * @param httpRequest
	 *            - HttpServletRequest HTTP reques object
	 * @return String - return result of update
	 * @author Nikhil Talreja
	 * @since 19 August, 2013
	 */
	//@RequestMapping(value = "phix/updateContactDetailsInEligLeadRecord", method = RequestMethod.POST)
	@ResponseBody
	public String updateContactDetailsInEligLeadRecord(HttpServletRequest httpRequest) {
//
//		LOGGER.info("Updating record in ELIG_LEAD table from pre launch results page");
//		
//		try{
//			String leadId = httpRequest.getSession().getAttribute(LEAD_ID).toString();
//			LOGGER.debug("Lead Id : " + leadId);
//			
//			if(StringUtils.isNumeric(leadId)){
//				long lead = Long.parseLong(leadId);
//				
//				String leadName = httpRequest.getParameter("leadName");
//				String leadEmail = httpRequest.getParameter("leadEmail");
//				String leadPhone = httpRequest.getParameter("leadPhone");
//				//String isOkToCall = httpRequest.getParameter("isOkToCall");
//				
//				//Validate incoming parameters
//				if(!validateContactDetails(lead,httpRequest)){
//					return FAILURE;
//				}
//				else{
//					LOGGER.debug("Calling API to update lead record");
//					EligLeadRequest request = new EligLeadRequest();
//					request.setId(lead);
//					request.setEmail(leadEmail);
//					request.setName(leadName);
//					request.setPhone(leadPhone);		
//					//request.setIsOkToCall(isOkToCall);
//					
//					//For PHIX 0.55, Ok to call is always stored as YES if number is input
//					request.setIsOkToCall(NO);
//					if(StringUtils.isNotBlank(leadPhone)){
//						request.setIsOkToCall(YES);
//					}
//
//					updateContactDetailsInEligLeadRecord(request);
//					
//					try{				
//						LOGGER.debug("Sending signup confirmation email");
//						//Send signup confirmation mail
//						prelaunchSignupConfirmationEmail.setEmailId(leadEmail);
//						prelaunchSignupConfirmationEmail.setContactName(leadName);
//						Notice email = prelaunchSignupConfirmationEmail.generateEmail();
//						prelaunchSignupConfirmationEmail.sendEmail(email);
//					}
//					catch (Exception e) {
//						LOGGER.error("Exception occured while sending signup confirmation email ", e);
//					}	
//					
//					return SUCCESS;
//				}
//			}
//			else{
//				LOGGER.info("Lead Id is invalid");
//				return FAILURE; 
//			}
//			
//		}
//		catch (Exception e) {
//			LOGGER.error("Exception occured while updating lead record ", e);
				return FAILURE; 
//		}	
	}
	
	
	/**
	 * This method is the AJAX call to update ELIG_LEAD record with new stage
	 *  
	 * @param httpRequest
	 *            - HttpServletRequest HTTP reques object
	 * @return String - return result of update
	 * @author Nikhil Talreja
	 * @since 03 September, 2013
	 */
	//@RequestMapping(value = "phix/updateStageInEligLeadRecord", method = RequestMethod.POST)
	@ResponseBody
	public String updateStageInEligLeadRecord(HttpServletRequest httpRequest) {
		
		try{
			
			String leadId = null;
			if((httpRequest.getSession())!=null && (httpRequest.getSession().getAttribute(LEAD_ID))!=null){
				leadId = httpRequest.getSession().getAttribute(LEAD_ID).toString();
			}
			String type = httpRequest.getParameter("type");
			LOGGER.debug("Lead Id : " + leadId);
			LOGGER.debug("Type :" + type);
			LOGGER.debug("Calling API to update lead stage");
			
			if(StringUtils.isNumeric(leadId)){
				long lead = Long.parseLong(leadId);
				EligLeadRequest request = new EligLeadRequest();
				request.setId(lead);
				if(StringUtils.equalsIgnoreCase(type,"gps")){
					request.setStage(STAGE.PRE_SHOPPING);
				}
				else if (StringUtils.equalsIgnoreCase(type,"legacy")){
					request.setStage(STAGE.SHOPPING);
				}
				return updateOtherInputsInEligLeadRecord(request);
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception occured while updating lead record ", e);
			return FAILURE;
		}
		
		return SUCCESS;
	}

	/**
	 * This method gets the thank you page after user submits details on
	 * pre-launch results page
	 *
	 * @param model
	 *            - SPring UI model
	 * @return String - return URL of the home page
	 * @author Nikhil Talreja
	 * @since 02 September, 2013
	 */
	//@RequestMapping(value = "phix/thankyou", method = RequestMethod.POST)
	public String getThankYouPage(Model model,HttpServletRequest httpRequest) {

		LOGGER.info("Getting Thank You page");
		
		model.addAttribute("showStateMessage",httpRequest.getParameter("showStateMessage"));
		model.addAttribute(STATE_NAME,httpRequest.getParameter(STATE_NAME));
		
		return "phix/thankyou";
	}
	
	/**
	 * This method redirects the user to Plan Tiles page for the MVP version
	 *
	 * @param httpRequest
	 *            - HTTP request object
	 * @return String - return URL of the tiles page
	 * @author Nikhil Talreja
	 * @since 17 September, 2013
	 */
	//@RequestMapping(value = "phix/plandisplay", method = RequestMethod.POST)
	public String getPlanTilesPage(HttpServletRequest httpRequest) {

		LOGGER.info("Getting MVP tiles view page");
		
		return "redirect:/private/saveIndividualPHIX";
	}
	
	/**
	 * This method is the AJAX call to update ELIG_LEAD record with new stage
	 *  
	 * @param httpRequest
	 *            - HttpServletRequest HTTP reques object
	 * @return String - return result of update
	 * @author Sunil Desu
	 * @since 14 May 2014
	 */
	@RequestMapping(value = "iex/updateStageInEligLeadRecord", method = RequestMethod.POST)
	@ResponseBody
	public String updateStageInEligLead(HttpServletRequest httpRequest) {
		
		try{
			
			String leadId = null;
			if((httpRequest.getSession())!=null && (httpRequest.getSession().getAttribute(LEAD_ID))!=null){
				leadId = httpRequest.getSession().getAttribute(LEAD_ID).toString();
			}
			String coverageYear = httpRequest.getParameter("coverageYear");
			LOGGER.debug("Calling API to update lead stage with Lead Id : " + leadId);
			if(StringUtils.isNumeric(leadId)){
				long lead = Long.parseLong(leadId);
				EligLeadRequest request = new EligLeadRequest();
				request.setId(lead);
				request.setStage(STAGE.PRE_SHOPPING);
				if(StringUtils.isNumeric(coverageYear)){
					request.setCoverageYear(Integer.parseInt(coverageYear));
				}
				return updateOtherInputsInEligLeadRecord(request);
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception occured while updating lead record ", e);
			return FAILURE;
		}
		
		return SUCCESS;
	}

	@RequestMapping(value = "iex/browseplans", method = RequestMethod.POST)
	@ResponseBody
	public String browseplans(@RequestBody PhixRequest phixRequest,HttpServletRequest httpRequest) {
		try{
			phixRequest.setSkipEligibility(true);
			MiniEstimatorResponse estimatorResponse = callEstimatorApi(phixRequest,httpRequest);
			httpRequest.getSession().setAttribute(LEAD_ID, estimatorResponse.getLeadId());
		}catch(Exception ex){
			LOGGER.error("Exception occured while updating lead record ", ex);
			return FAILURE;
		}
		return SUCCESS;
	}
}
