package com.getinsured.iex.hub.qac.servicehandler;


import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.JAXBElement;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ssap.iex.HouseholdMember;
import com.getinsured.iex.hub.platform.ssap.iex.SSAP;
import com.getinsured.iex.hub.platform.ssap.iex.SSAPTaxHouseHold;
import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.exchange._1.ObjectFactory;
import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.VerifyRequestPayloadType;
import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.VerifyResponsePayloadType;
import com.getinsured.iex.hub.qac.prefixmappers.QACNamespaceMapper;
import com.getinsured.iex.hub.qac.wrapper.InsuranceApplicantRequestWrapper;
import com.getinsured.iex.hub.qac.wrapper.PersonWrapper;
import com.getinsured.iex.hub.qac.wrapper.VerifyRequestPayloadWrapper;
import com.getinsured.iex.hub.qac.wrapper.VerifyResponsePayloadWrapper;

public class QACServiceHandler extends com.getinsured.iex.hub.platform.AbstractServiceHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(QACServiceHandler.class);
	private static com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.ObjectFactory extnFactory = new com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.ObjectFactory();
	private Jaxb2Marshaller marshaller;
	
	@SuppressWarnings("unchecked")
	private void processJSONObject(JSONObject obj) throws HubServiceException, HubMappingException{
	        Set<Entry<String, Object>> keySet = obj.entrySet();
	        Iterator<Entry<String, Object>> cursor = keySet.iterator();
	        Entry<String, Object> jsonElement = null;
	        while(cursor.hasNext()){
	               jsonElement = cursor.next();
	               handleJsonElement(jsonElement.getKey(), jsonElement.getValue());
	        }
	 }
	 
	 private void handleJsonElement(String key, Object value) throws HubServiceException, HubMappingException {
	        if(value instanceof JSONObject){
	           processJSONObject((JSONObject)value);
	        }else if(value instanceof JSONArray){
	        	processJSONArray((JSONArray) value);
	        }
	        else if(value instanceof String || value instanceof Boolean){
	        	logger.info("Processing:"+key+" For value:"+value);
	        	this.handleInputParameter(key, value.toString());
	        }
	 }

	
	private void processJSONArray(JSONArray value) throws HubServiceException, HubMappingException {
		int arrayLen = value.size();
		Object tmp;
		for(int i = 0; i < arrayLen; i++){
			tmp = value.get(i);
			if(tmp instanceof JSONObject){
				processJSONObject((JSONObject)tmp);
			}
			// should we expect anything other than JSONObject in this array?
		}
	}
	
	public Object getRequestpayLoad() throws HubServiceException, HubMappingException {
		SSAP ssap = null;
		try{
			ssap = new SSAP(getJsonInput());
		}catch(ParseException  ex){
			listJSONKeys();
			throw new HubMappingException("Failed to process input data ["+ex.getMessage()+"]", ex);
		}
		
		this.setServiceName("QAC");
		HouseholdMember houseHoldMember = null;
		SSAPTaxHouseHold taxHousehold = null;
		
		
		JAXBElement<VerifyRequestPayloadType> verifyRequestPayloadType = null;
		Iterator<SSAPTaxHouseHold> householdCursor = ssap.houseHoldIterator();
		while(householdCursor.hasNext()){
			taxHousehold = householdCursor.next();
			Iterator<HouseholdMember> memberCursor = taxHousehold.iterator();
			
			while(memberCursor.hasNext()){
					houseHoldMember =memberCursor.next();
					
					PersonWrapper personWrapper = new PersonWrapper();
					this.extractSSAPData(houseHoldMember, personWrapper);
					
					InsuranceApplicantRequestWrapper applicantWrapper = new InsuranceApplicantRequestWrapper();
					this.extractSSAPData(houseHoldMember, applicantWrapper);
					
					this.extractSSAPData(houseHoldMember.getSsnInfo(), personWrapper);
					
					applicantWrapper.setPerson(personWrapper);
				
					VerifyRequestPayloadWrapper request = new VerifyRequestPayloadWrapper();
					request.setApplicant(applicantWrapper);
					com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.exchange._1.ObjectFactory obj = new ObjectFactory();
					verifyRequestPayloadType = obj.createVerifyRequest(request.getSystemRepresentation());
			}
		}
		return verifyRequestPayloadType;
	}
	
	public Object getPayLoadFromJSONInput(String jsonSectionKey,String jsonStr) throws HubServiceException, HubMappingException{
		
		JSONObject obj = null;
		HubServiceException hse;
		JSONParser parser = new JSONParser();
		
		try {
			obj = (JSONObject)parser.parse(jsonStr);
		} catch (ParseException e) {
			logger.error("Invalid JSON:"+jsonStr);
			hse = new HubServiceException("Failed to parse JSON input", e);
			throw hse;
		}
		
		JSONArray taxHousehold = (JSONArray) obj.get(jsonSectionKey);
		
		if(taxHousehold == null ){
			throw new HubServiceException("Applicants expected for NMEC Service");
		}
		
		ObjectFactory objectFactory =  new ObjectFactory();
		
		VerifyRequestPayloadType request = extnFactory.createVerifyRequestPayloadType();
		JAXBElement<VerifyRequestPayloadType> payloadElement = null;
		
		this.setServiceName("QAC");
		for(int j = 0; j < taxHousehold.size(); j ++)
		{
			JSONObject mecApplicant = (JSONObject)taxHousehold.get(j);
		
			obj = (JSONObject) mecApplicant.get("householdMember");
			PersonWrapper applicantWrapper = new PersonWrapper();
		
			String namespace = PersonWrapper.class.getName();
			
			JSONObject objPerson = (JSONObject) obj.get("person");
			setRequestParameterFromJson(applicantWrapper, objPerson, "dateOfBirth", "personBirthDate", namespace);
			
			JSONObject objPersonName = (JSONObject) objPerson.get("name");
			setRequestParameterFromJson(applicantWrapper, objPersonName, "firstName", "personGivenName", namespace);
			setRequestParameterFromJson(applicantWrapper, objPersonName, "lastName", "personSurName", namespace);
			
			JSONObject objSSN = (JSONObject) objPerson.get("socialSecurityCard");
			setRequestParameterFromJson(applicantWrapper, objSSN, "socialSecurityNumber", "personSSN", namespace);
			
					
			JSONObject objInsurance = (JSONObject) obj.get("insurance");
			setRequestParameterFromJson(applicantWrapper, objInsurance, "coverageStartDate", "coverageStartDate", namespace);
			setRequestParameterFromJson(applicantWrapper, objInsurance, "coverageEndDate", "coverageEndDate", namespace);
			
			request.getRequestApplicant().setRolePlayedByPerson(applicantWrapper.getSystemRepresentation());
			
		}
		payloadElement = objectFactory.createVerifyRequest(request);
		return payloadElement;
	}
	
	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {
			
			JAXBElement<VerifyResponsePayloadType> resp= (JAXBElement<VerifyResponsePayloadType>)responseObject;
			
			VerifyResponsePayloadType payload = resp.getValue();
			VerifyResponsePayloadWrapper payloadWrapper = new VerifyResponsePayloadWrapper(payload);
			this.requestStatus = RequestStatus.SUCCESS;
			return payloadWrapper.toJSONString(); 
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}
	

	@Override
	public String getServiceIdentifier() {
		return "H46";
	}
	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			String[] contextPaths = new String[2];
			contextPaths[0] = "com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.exchange._1";
			contextPaths[1] = "com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1";
			
			this.marshaller= this.getMarshaller(contextPaths,new QACNamespaceMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			String[] contextPaths = new String[2];
			contextPaths[0] = "com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.exchange._1";
			contextPaths[1] = "com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1";
			
			this.marshaller= this.getMarshaller(contextPaths,new QACNamespaceMapper());
		}
		return this.marshaller;
	}
}
