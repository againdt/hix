package com.getinsured.hix.indportal.dto;

import java.util.List;

public class IndPortalValidationDTO {

	private boolean status;
	private List<String> errors;
	
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}	
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IndPortalValidationDTO [status=").append(status).append(", errors=").append(errors).append("]");
		return builder.toString();
		 
	}
}
