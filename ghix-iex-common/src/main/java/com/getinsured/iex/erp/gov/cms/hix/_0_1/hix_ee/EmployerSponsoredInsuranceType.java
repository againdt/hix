//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_pm.InsurancePlanType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.EmployerInsuranceSponsorshipStatusCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.PlanYearCategoryCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ComplexObjectType;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ReferenceType;


/**
 * A data type for insurance
 * 				coverage sponsored by an employer offered to current / former
 * 				employees and their families.
 * 
 * <p>Java class for EmployerSponsoredInsuranceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmployerSponsoredInsuranceType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ESILowestCostPlan" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ESIPlanYearCode" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ESIPlanRateUnknownIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ESIExpectedChange" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ESIExpectedChangeDate" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ESIViaCurrentEmployeeIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ESIRetireePlanIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ESIMinimumAVStandardUnknownIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ESIContactPersonReference" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ESIExpectedLowestCostPlan" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmployerSponsoredInsuranceType", propOrder = {
    "esiLowestCostPlan",
    "esiPlanYearCode",
    "esiPlanRateUnknownIndicator",
    "esiExpectedChange",
    "esiExpectedChangeDate",
    "esiViaCurrentEmployeeIndicator",
    "esiRetireePlanIndicator",
    "esiMinimumAVStandardUnknownIndicator",
    "esiContactPersonReference",
    "esiExpectedLowestCostPlan"
})
public class EmployerSponsoredInsuranceType
    extends ComplexObjectType
{

    @XmlElement(name = "ESILowestCostPlan")
    protected InsurancePlanType esiLowestCostPlan;
    @XmlElement(name = "ESIPlanYearCode")
    protected PlanYearCategoryCodeType esiPlanYearCode;
    @XmlElement(name = "ESIPlanRateUnknownIndicator")
    protected Boolean esiPlanRateUnknownIndicator;
    @XmlElement(name = "ESIExpectedChange")
    protected EmployerInsuranceSponsorshipStatusCodeType esiExpectedChange;
    @XmlElement(name = "ESIExpectedChangeDate")
    protected DateType esiExpectedChangeDate;
    @XmlElement(name = "ESIViaCurrentEmployeeIndicator")
    protected Boolean esiViaCurrentEmployeeIndicator;
    @XmlElement(name = "ESIRetireePlanIndicator")
    protected Boolean esiRetireePlanIndicator;
    @XmlElement(name = "ESIMinimumAVStandardUnknownIndicator")
    protected Boolean esiMinimumAVStandardUnknownIndicator;
    @XmlElement(name = "ESIContactPersonReference")
    protected List<ReferenceType> esiContactPersonReference;
    @XmlElement(name = "ESIExpectedLowestCostPlan")
    protected InsurancePlanType esiExpectedLowestCostPlan;

    /**
     * Gets the value of the esiLowestCostPlan property.
     * 
     * @return
     *     possible object is
     *     {@link InsurancePlanType }
     *     
     */
    public InsurancePlanType getESILowestCostPlan() {
        return esiLowestCostPlan;
    }

    /**
     * Sets the value of the esiLowestCostPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsurancePlanType }
     *     
     */
    public void setESILowestCostPlan(InsurancePlanType value) {
        this.esiLowestCostPlan = value;
    }

    /**
     * Gets the value of the esiPlanYearCode property.
     * 
     * @return
     *     possible object is
     *     {@link PlanYearCategoryCodeType }
     *     
     */
    public PlanYearCategoryCodeType getESIPlanYearCode() {
        return esiPlanYearCode;
    }

    /**
     * Sets the value of the esiPlanYearCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanYearCategoryCodeType }
     *     
     */
    public void setESIPlanYearCode(PlanYearCategoryCodeType value) {
        this.esiPlanYearCode = value;
    }

    /**
     * Gets the value of the esiPlanRateUnknownIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getESIPlanRateUnknownIndicator() {
        return esiPlanRateUnknownIndicator;
    }

    /**
     * Sets the value of the esiPlanRateUnknownIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setESIPlanRateUnknownIndicator(Boolean value) {
        this.esiPlanRateUnknownIndicator = value;
    }

    /**
     * Gets the value of the esiExpectedChange property.
     * 
     * @return
     *     possible object is
     *     {@link EmployerInsuranceSponsorshipStatusCodeType }
     *     
     */
    public EmployerInsuranceSponsorshipStatusCodeType getESIExpectedChange() {
        return esiExpectedChange;
    }

    /**
     * Sets the value of the esiExpectedChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmployerInsuranceSponsorshipStatusCodeType }
     *     
     */
    public void setESIExpectedChange(EmployerInsuranceSponsorshipStatusCodeType value) {
        this.esiExpectedChange = value;
    }

    /**
     * Gets the value of the esiExpectedChangeDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateType }
     *     
     */
    public DateType getESIExpectedChangeDate() {
        return esiExpectedChangeDate;
    }

    /**
     * Sets the value of the esiExpectedChangeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateType }
     *     
     */
    public void setESIExpectedChangeDate(DateType value) {
        this.esiExpectedChangeDate = value;
    }

    /**
     * Gets the value of the esiViaCurrentEmployeeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getESIViaCurrentEmployeeIndicator() {
        return esiViaCurrentEmployeeIndicator;
    }

    /**
     * Sets the value of the esiViaCurrentEmployeeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setESIViaCurrentEmployeeIndicator(Boolean value) {
        this.esiViaCurrentEmployeeIndicator = value;
    }

    /**
     * Gets the value of the esiRetireePlanIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getESIRetireePlanIndicator() {
        return esiRetireePlanIndicator;
    }

    /**
     * Sets the value of the esiRetireePlanIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setESIRetireePlanIndicator(Boolean value) {
        this.esiRetireePlanIndicator = value;
    }

    /**
     * Gets the value of the esiMinimumAVStandardUnknownIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getESIMinimumAVStandardUnknownIndicator() {
        return esiMinimumAVStandardUnknownIndicator;
    }

    /**
     * Sets the value of the esiMinimumAVStandardUnknownIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setESIMinimumAVStandardUnknownIndicator(Boolean value) {
        this.esiMinimumAVStandardUnknownIndicator = value;
    }

    /**
     * Gets the value of the esiContactPersonReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the esiContactPersonReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getESIContactPersonReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReferenceType }
     * 
     * 
     */
    public List<ReferenceType> getESIContactPersonReference() {
        if (esiContactPersonReference == null) {
            esiContactPersonReference = new ArrayList<ReferenceType>();
        }
        return this.esiContactPersonReference;
    }

    /**
     * Gets the value of the esiExpectedLowestCostPlan property.
     * 
     * @return
     *     possible object is
     *     {@link InsurancePlanType }
     *     
     */
    public InsurancePlanType getESIExpectedLowestCostPlan() {
        return esiExpectedLowestCostPlan;
    }

    /**
     * Sets the value of the esiExpectedLowestCostPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsurancePlanType }
     *     
     */
    public void setESIExpectedLowestCostPlan(InsurancePlanType value) {
        this.esiExpectedLowestCostPlan = value;
    }

}
