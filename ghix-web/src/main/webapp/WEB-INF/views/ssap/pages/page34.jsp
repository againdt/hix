<%@ include file="../include.jsp" %>
<div id="appscr84"  style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h1 class="titleForPages">
				<spring:message code="ssap.page34.reviewApplication"/>
			</h1>
		</div>
		<form id="appscr84Form" class="form-horizontal">
			<div id="appscr84addReviewSummary"></div>
		</form>
	</div>
</div>