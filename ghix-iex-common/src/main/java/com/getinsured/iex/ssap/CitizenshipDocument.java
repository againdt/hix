
package com.getinsured.iex.ssap;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author chopra_s
 */
public class CitizenshipDocument {
  @Expose
  @SerializedName("SEVISId")
  @JsonProperty("SEVISId")
  private String sEVISId;

  @Expose
  private String foreignPassportOrDocumentNumber;

  @Expose
  private String sevisId;

  @Expose
  private String alienNumber;

  @Expose
  private NameOnDocument nameOnDocument = new NameOnDocument();

  @Expose
  private String documentDescription;

  @Expose
  private Boolean nameSameOnDocumentIndicator = null; // UI doesn't want anything preselected until user picks.

  @Expose
  private String visaNumber;

  @Expose
  private Date documentExpirationDate;

  @Expose
  private String i94Number;

  @Expose
  private String foreignPassportCountryOfIssuance;

  @Expose
  private String cardNumber;

  public String getSEVISId() {
    return sEVISId;
  }

  public void setSEVISId(String sEVISId) {
    this.sEVISId = sEVISId;
  }

  public String getForeignPassportOrDocumentNumber() {
    return foreignPassportOrDocumentNumber;
  }

  public void setForeignPassportOrDocumentNumber(String foreignPassportOrDocumentNumber) {
    this.foreignPassportOrDocumentNumber = foreignPassportOrDocumentNumber;
  }

  public String getSevisId() {
    return sevisId;
  }

  public void setSevisId(String sevisId) {
    this.sevisId = sevisId;
  }

  public String getAlienNumber() {
    return alienNumber;
  }

  public void setAlienNumber(String alienNumber) {
    this.alienNumber = alienNumber;
  }

  public NameOnDocument getNameOnDocument() {
    return nameOnDocument;
  }

  public void setNameOnDocument(NameOnDocument nameOnDocument) {
    this.nameOnDocument = nameOnDocument;
  }

  public String getDocumentDescription() {
    return documentDescription;
  }

  public void setDocumentDescription(String documentDescription) {
    this.documentDescription = documentDescription;
  }

  public Boolean getNameSameOnDocumentIndicator() {
    return nameSameOnDocumentIndicator;
  }

  public void setNameSameOnDocumentIndicator(Boolean nameSameOnDocumentIndicator) {
    this.nameSameOnDocumentIndicator = nameSameOnDocumentIndicator;
  }

  public String getVisaNumber() {
    return visaNumber;
  }

  public void setVisaNumber(String visaNumber) {
    this.visaNumber = visaNumber;
  }

  public Date getDocumentExpirationDate() {
    return documentExpirationDate;
  }

  public void setDocumentExpirationDate(Date documentExpirationDate) {
    this.documentExpirationDate = documentExpirationDate;
  }

  public String getI94Number() {
    return i94Number;
  }

  public void setI94Number(String i94Number) {
    this.i94Number = i94Number;
  }

  public String getForeignPassportCountryOfIssuance() {
    return foreignPassportCountryOfIssuance;
  }

  public void setForeignPassportCountryOfIssuance(String foreignPassportCountryOfIssuance) {
    this.foreignPassportCountryOfIssuance = foreignPassportCountryOfIssuance;
  }

  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }

}
