
package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class Employer {

    @Expose
    private Phone phone=new Phone();
    @Expose
    private String employerContactPersonName;
    @Expose
    private String employerContactEmailAddress;

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public String getEmployerContactPersonName() {
        return employerContactPersonName;
    }

    public void setEmployerContactPersonName(String employerContactPersonName) {
        this.employerContactPersonName = employerContactPersonName;
    }

    public String getEmployerContactEmailAddress() {
        return employerContactEmailAddress;
    }

    public void setEmployerContactEmailAddress(String employerContactEmailAddress) {
        this.employerContactEmailAddress = employerContactEmailAddress;
    }

}
