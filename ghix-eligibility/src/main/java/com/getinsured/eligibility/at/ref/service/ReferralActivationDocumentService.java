package com.getinsured.eligibility.at.ref.service;

import com.getinsured.eligibility.at.ref.exception.ReferralException;

/**
 * @author chopra_s
 *
 */
public interface ReferralActivationDocumentService {
	String updateDocumentStatus(final String jsonInput) throws ReferralException;
	
	Integer sendNotificationToUploadDoc(long argument);
}
