<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld" %>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<!--[if IE]>
	<style type="text/css">
		form#commentForm + table.table {
			visibility: hidden;
		}
	</style>
<![endif]-->

<script type="text/javascript">
$(function() {
	$(".newcommentiframe").click(function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        if (href.indexOf('#') != 0) {
        	$('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal({backdrop:false});
		}
	});
});

function closeCommentBox() {
	$("#newcommentiframe").remove();
	window.location.reload(true);
}
function closeIFrame() {
	$("#newcommentiframe").remove();
	window.location.reload(true);
}
</script>

<div class="gutter10">
<div class="row-fluid">	
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/crm/employer/searchemployer"/>">Employers</a></li>
			<li>Comments</li>
		</ul> 
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
		<h1>
		<span class="span3">${employer.name}</span> <small class="span9">Contact Name:
			${employer.contactFirstName} ${employer.contactLastName}</small>
	</h1>
	</div>
	<!--page-breadcrumb ends-->

	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
			<h4>About this Employer</h4>
			</div>
			<ul class="nav nav-list">
				<li ><a href="/hix/crm/crmemployer/details/${employerId}">Summary</a></li>
				<li><a href="/hix/crm/crmemployer/employercontactinfo/${employerId}">Contact Info</a></li>
				<!-- <li class="navmenu disabled"><a href="#">Messages</a></li> -->
				<li class="active"><a href="#">Comments</a></li>
				<li class="navmenu"><a href="/hix/crm/crmemployer/ticket/history/${employerId}">Ticket History</a></li>
				<li ><a href="/hix/crm/crmemployer/securityQuestions/${employerId}">Security Questions</a></li>
			</ul>
			<br>
			<div class="header">
				<h4><i class="icon-cog icon-white"></i> Actions</h4>
			</div>
			<ul class="nav nav-list">
				 <c:if test="${checkDilog == null}">  
				<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employer Account</a></li>
			</c:if>
			<c:if test="${checkDilog != null}">  
				<li class="navmenu"><a href="/hix/account/user/switchUserRole?switchToModuleName=employer&switchToModuleId=${employerId}&switchToResourceName=${employer.name}"  role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employer Account</a></li>
			</c:if>
			<li class="navmenu"><a href="<c:url value="/crm/crmemployer/passwordreset"/>/${employerId}?sourcePage=employercomments" role="button" data-toggle="modal"><i class="icon-envelope"></i><spring:message  code='label.sendPasswordResetLink'/></a></li>
			
				 <!--  <li class="navmenu disabled"><a href="#"><i class="icon-envelope-unread"></i> Compose Message</a></li>

				<li class="navmenu disabled"><a href="#"><i class="icon-comment"></i> New Comment
				</a></li> -->
			</ul>
		</div>
		
		
		<div id="rightpanel" class="span9">
		<div class="header">
			<h4>Comments</h4>
		</div>
			<div class="gutter10">
			<form class="form-vertical" id="commentForm" name="commentForm" action="" method="POST"">
		              <df:csrfToken/>
				<table class="table">
					<!-- <thead>
						<tr class="graydrkaction">
							<th scope="col"><h4>Comments</h4></th>
							 <th scope="col" class="txt-right"><a class="btn btn-primary showcomments">Add Comments</a> </th>
						</tr>
					</thead> -->
					<tbody>
						<tr>
							<td colspan="2" class="">					
								<comment:view targetId="${employer.id}" targetName="EMPLOYER">
								</comment:view>
								
								<jsp:include page="../../views/platform/comment/addcomments.jsp">
									<jsp:param value="" name=""/>
								</jsp:include>
								<input type="hidden" value="EMPLOYER" id="target_name" name="target_name" /> 
								<input type="hidden" value="${employer.id}" id="target_id" name="target_id" />
								
							</td>
						</tr>
					</tbody>
				</table>
				<input type="hidden" name="employerName" id="employerName" value="${employer.name}" />
			</form>
			</div>
		</div>
	</div>	
	
	
	</div>	 
	<form name="dialogForm" id="dialogForm" action="<c:url value="/crm/employer/home" />" novalidate="novalidate">
              <df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left">View Employer Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					Clicking "Employer View" will take you to the Employer's portal for ${employer.name}.<br/>
					Through this portal you will be able to take actions on behalf of the employer, such as view <br/>
					and edit employee list, fill out employer eligibility etc.<br/>
					Proceed to Employer view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
  <input class="pull-left"  type="checkbox" id="checkEmployerView" name="checkEmployerView"  > 
    <div class="pull-left">&nbsp; Don't show this message again.</div>
    <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
   <button class="btn btn-primary" type="submit">Employer View</button>
   <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="employer" />
	<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="${employerId}" />
	<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${employer.name}" />
    <!-- a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>
</form>
<!--  Latest UI -->