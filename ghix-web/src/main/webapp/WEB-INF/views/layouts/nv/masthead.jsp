<%@page import="com.getinsured.hix.model.UserRole"%>
<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page session="true" %>
<%@page import="java.util.Set"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>

<%
String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
String isUserFullyAuthorized =  (String) request.getSession().getAttribute("isUserFullyAuthorized");
String isUserSwitchToOtherView = (String)request.getSession().getAttribute("isUserSwitchToOtherView");
Set<UserRole> userRoles = (Set<UserRole>)request.getSession().getAttribute("userConfiguredRoles");
String userActiveRoleLabel =  (String) request.getSession().getAttribute("userActiveRoleLabel");


	/*
	 * START:: HIX-10725 - Program Menu Links for Inbox, account manintenance
	 *         HIX-19109 - Masthead links w.r.t to userActiveRoleName 
	 */
	 String myAccountUrl= GhixConstants.MY_ACCOUNT_URL;
	 String myInboxUrl= GhixConstants.MY_INBOX_URL;
	 String userName =  (String) request.getSession().getAttribute("USER_NAME");
	 String logoutUrl = request.getContextPath()+"/account/user/logout";
	 String logoUrl = (String)request.getSession().getAttribute("userLandingPage");
	 
	if(StringUtils.isBlank(myAccountUrl)){
		myAccountUrl="#";
	}
	
	if(StringUtils.isNotBlank(myInboxUrl) && StringUtils.isNotBlank(userName)) {
		myInboxUrl = myInboxUrl + "?languageCode=" + session.getAttribute("lang") ;
	}else{
		myInboxUrl="#";
	}
	
	 String switchToModuleNameLocal=(String)request.getSession().getAttribute("switchToModuleName");
	 String showFindAgentInEm="FALSE";
	 if(   ("Y".equalsIgnoreCase(isUserSwitchToOtherView)) && "employer".equalsIgnoreCase(switchToModuleNameLocal) ){
		 showFindAgentInEm="TRUE";
	 }
	
	if(StringUtils.isBlank(logoutUrl)){
		logoutUrl="#";
	}
	
	if(StringUtils.isBlank(isUserSwitchToOtherView)){
		isUserSwitchToOtherView="N";
	}
	
	
	String 	faqUrl = "/faq/home";
	
	if(StringUtils.isNotBlank(userActiveRoleName)){
		if(StringUtils.equalsIgnoreCase(userActiveRoleName, "admin") || StringUtils.equalsIgnoreCase(userActiveRoleName, "operations") || StringUtils.equalsIgnoreCase(userActiveRoleName, "csr")){
			faqUrl= "/faq/admin";
		}else if(StringUtils.equalsIgnoreCase(userActiveRoleName, "issuer") || StringUtils.equalsIgnoreCase(userActiveRoleName, "issuer_representative")){
			faqUrl= "/faq/issuer";
		}else{
			faqUrl= "/faq/"+userActiveRoleName;
		}
	}

	/*
	 * END:: HIX-10725 - Program Menu Links for Inbox, account manintenance
	 *         HIX-19109 - Masthead links w.r.t to userActiveRoleName 
	 */
	 
	
	 
%>
<c:set var="userActiveRoleName" value="<%=userActiveRoleName%>" />
<c:set var="isUserFullyAuthorized" value="<%=isUserFullyAuthorized%>" />
<c:set var="isUserSwitchToOtherView" value="<%=isUserSwitchToOtherView%>" />
<c:set var="logoUrl" value="<%=logoUrl%>" />
<c:set var="faqUrl" value="<%=faqUrl%>" />
<c:set var="userRoles" value="<%=userRoles%>" />
<c:set var="userActiveRoleLabel" value="<%=userActiveRoleLabel%>" />
<c:set var="showFindAgentInEm" value="<%=showFindAgentInEm%>" />
<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" />
<c:set var="globalInboxNoticeCountInterval" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.GLOBAL_INBOX_NOTICE_COUNT_INTERVAL)%>" />

<script type="text/javascript">


$(document).ready(function() {
	$('.masthead_logo').trigger('mousemove');
$('a.lang').click(function (){
	
	var val= $(this).attr('id');
	
	var currUrl = window.location.href;
	var newUrl="";

	/* Check if Query String parameter is set or not
	* If yes	
	*/
	if(currUrl.indexOf("?", 0) > 0)
	{
		/* Check if locale is already set without querystring param  */
		 if(currUrl.indexOf("?lang=", 0) > 0)
			  newUrl = "?lang="+val;
		/*  Check if locale is already set with querystring param  */
		 else if(currUrl.indexOf("&lang=", 0) > 0) 
			 newUrl = currUrl.substring(0, currUrl.length-2)+val; 
		 else	 
			  newUrl = currUrl + "&lang="+val;
	}/* If No  */
	else
		newUrl = currUrl + "?lang="+val;
		window.location = newUrl;
	});
});
  
</script>


<div>
<input type="hidden" id="globalInboxNoticeCountInterval" value="${globalInboxNoticeCountInterval}"/>
</div>

<sec:authorize access="isAuthenticated()" var="isUserAuthenticated"> 
  <script type="text/javascript">
  
	function countUnreadMessages() {
		var currentTime = new Date().getTime();
		var currentTime = new Date().getTime();
		var secureInboxUnread = '${sessionScope.unreadRecords}';
		var lastUpdatedTime = '${sessionScope.lastUpdatedTime}';
		var counter = currentTime-lastUpdatedTime;
			
		if(secureInboxUnread == null || secureInboxUnread =='' || counter >= 15000){
			var pathURL = "${pageContext.request.contextPath}/inbox/secureInboxUnread";
			var csrftoken;
			csrftoken= '${sessionScope.csrftoken}';
			
			$.post(pathURL,{ csrftoken:csrftoken,currentTime:currentTime}, function(data) {
				if(data != null && data>0) {
					$(".unreadRecords").removeClass("superHide");
					$(".unreadRecords").html(data);
				}
				else {
					
					var parsedResponse = $.parseHTML(data);
					if(null!=parsedResponse && $(parsedResponse).find('.application-error').length>0){
						window.location.href = '/hix/error/pageExpired';
					}else{ 
						$(".unreadRecords").addClass("superHide");
						$("#notificationMessages .unreadRecords").html(0).removeClass("superHide");
					}
				}
			});
		 }else{
		 		$(".unreadRecords").addClass("superHide");
				$("#notificationMessages .unreadRecords").html(0).removeClass("superHide");
		 	}
		}
		

		$(document).ready(function(){
			(function(){
				var globalInboxNoticeCountInterval = document.getElementById("globalInboxNoticeCountInterval").value
 	 			setInterval(function(){ countUnreadMessages()}, globalInboxNoticeCountInterval);
 			})();
		});
</script> 
</sec:authorize>


<%
	/* if session attribute is not set  */
	if(request.getParameter("lang") == null && session.getAttribute("lang") == null){
		session.setAttribute("lang","en");
	}
	else if(request.getParameter("lang") !=null && request.getParameter("lang") != session.getAttribute("lang")   ){
			session.setAttribute("lang", request.getParameter("lang"));
	}
	
	String enval=null;String esval=null;
	
	
	if(session.getAttribute("lang") !=null && session.getAttribute("lang").equals("en")  ){
		enval="selected";
    }
	else if(session.getAttribute("lang") !=null && session.getAttribute("lang").equals("es")){
		esval="selected";
    }
	else {
		enval="";
    }
%>
<div class="navbar navbar-fixed-top navbar-inverse" id="masthead" role="banner">
    <input type="hidden" id="exchangeName" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/>
	<input type="hidden" id="stateCode" value="${stateCode}"/>
	
	<c:if test="${isUserAuthenticated=='true'}">
<!-- HIX-27186 User has only one role (default role) then do not show masthead with multi role drop down  -->
			<c:if test="${userRoles.size() > 1}">
				<div id="first-menu-bgcolor-nm"></div>
			</c:if>
	</c:if>
  <div class="navbar-inner">
    <div class="container">
		<button type="button" aria-label="navigation-button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <ul class="nav">
            <li>

                <c:if test="${isUserAuthenticated=='false'}">
                    <a href="#" class="masthead_logo" title="Link open in new window or tab" onclick="openInNewTab('https://www.nevadahealthlink.com/')"><img width="150" class="brand" src="<gi:cdnurl value="/resources/img/" /><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE)%>"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/></a>
                </c:if>

                <c:if test="${isUserAuthenticated=='true'}">
                    <c:choose>
                        <c:when test="${isUserFullyAuthorized != null && isUserFullyAuthorized=='false' }">
                            <a href="#" class="masthead_logo"><img width="150" class="brand masthead-img" src="<gi:cdnurl value="/resources/img/" /><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE)%>"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/><span style="display: none;"><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%></span></a>
                        </c:when>
                        <c:otherwise>
                            <a href="#" class="masthead_logo" title="Link open in new window or tab" onclick="openInNewTab('https://www.nevadahealthlink.com/')"><img width="150" class="brand masthead-img" src="<gi:cdnurl value="/resources/img/" /><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE)%>"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/></a>
                        </c:otherwise>
                    </c:choose>
                 </c:if>
            </li>
        </ul>
        <div class="nav-collapse collapse pull-right">
	      	 <c:if test="${isUserAuthenticated=='true'}">
				<!-- HIX-27186 User has only one role (default role) then do not show masthead with multi role drop down  -->
				<c:if test="${userRoles.size() > 1}">
			      	<div class="row-fluid" id="role-header-nm">
				      	<ul class="nav pull-right">
				      		 <li><a href=<%=logoutUrl%>>Logout</a></li>
				      		 <c:if test="${userActiveRoleName=='employee' && isUserSwitchToOtherView=='N'}">
								<c:choose>
									<c:when test="${isUserFullyAuthorized != null && isUserFullyAuthorized=='false' }">
										<li id="masthead-account-settings" hidden="true"><a href="#">Account Settings</a></li>
									</c:when>
									<c:otherwise>
										<li id="masthead-account-settings"><a href="<c:url value='/shop/employee/spplanselection/accountsettings'/>">Account Settings</a></li>
									</c:otherwise>
								</c:choose>
							 </c:if>

                             <c:if test="${isUserSwitchToOtherView == 'N' && userActiveRoleName!='employee'}">
                                <c:choose>
                                    <c:when test="${isUserFullyAuthorized != null && isUserFullyAuthorized=='false' }">
                                        <li id="masthead-account-settings" hidden="true"><a href="#">Account Settings</a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li id="masthead-account-settings"><a href="<c:url value='/account/user/accountsettings'/>">Account Settings</a></li>
                                    </c:otherwise>
                                </c:choose>
                             </c:if>

				      		 <li class="dropdown">
				      		 	<c:choose>
				            		<c:when test="${(sUser.defRole.name).equalsIgnoreCase(userActiveRoleName)}">
				            			<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-key"></i> ${userActiveRoleLabel}  (Change<b class="caret"></b>) </a>
				            		</c:when>
				            		<c:otherwise>
				            			<a class="dropdown-toggle" data-toggle="dropdown" href="#">${userActiveRoleLabel}  (Change<b class="caret"></b>) </a>
				            		</c:otherwise>
				            	</c:choose>
				            	<ul class="dropdown-menu">
									<li><a>My Roles</a></li>
									<li class="divider"></li>
									<!-- HIX-27183 : Showing roles from UserRole in masthead.jsp and providing hyperlink to only active roles of user -->
				            		<c:forEach var="assigned" items="${userRoles}">
				            		<c:set var="ecryptedrolename" ><encryptor:enc value="${assigned.role.name}" isurl="true"/> </c:set>
		            			    <c:choose>
                                        <c:when test="${sUser.defRole.name == assigned.role.name}">
                                        <!-- HIX-27183 : Showing tick mark on the active role of user -->
                                            <li><a href="<c:url value="/account/user/switchUserRole/${ecryptedrolename}?assigned=true" />"><c:if test="${userActiveRoleLabel == assigned.role.label}"><i class="icon-ok"></i></c:if><i class="icon-key"></i> ${assigned.role.label}</a></li>
                                        </c:when>
		            				    <c:otherwise>

		            					    <c:choose>
                                                <c:when test="${userActiveRoleLabel == assigned.role.label}">
                                                <!-- HIX-27183 : Showing tick mark on the active role of user -->
                                                <li><a href="<c:url value="/account/user/switchUserRole/${ecryptedrolename}?assigned=true" />"><i class="icon-ok"></i>${assigned.role.label}</a></li>
                                                </c:when>
                                                <c:otherwise>
		            					            <c:if test="${assigned.isActiveUserRole()==true}">
                                                        <li><a href="<c:url value="/account/user/switchUserRole/${ecryptedrolename}?assigned=true" />">
                                                        ${assigned.role.label}</a></li>
		            					            </c:if>
		            					        </c:otherwise>
		            					    </c:choose>
		            				    </c:otherwise>
		            			    </c:choose>
									</c:forEach>
				                </ul>
				            </li>
				      	</ul>
					</div> <!-- ./End role-header-nm -->
				</c:if>
	         </c:if>
            <div class="row-fluid">
             <ul class="nav pull-right clearfix" id="second-menu-nm">
                <c:if test="${isUserAuthenticated=='true'}">
                    <li><a href="<c:url value="/account/user/switchNonDefRole/${userActiveRoleName}" />"><i class="icon-home" style="font-size: 18px;"></i><span class="aria-hidden">Home</span></a></li>
	                <li id="masthead-inbox"><a href="<c:url value="<%=myInboxUrl%>" />"><i class="icon-envelope"></i> Inbox <span class="unreadRecords superHide"></span></a></li>

                     <c:if test="${userActiveRoleName=='individual' || showFindAgentInEm=='TRUE'}">
						<li class="dropdown">
						 	<a class="dropdown-toggle" data-toggle="dropdown" href="#">Help & Support <b class="caret"></b></a>
								<ul class="dropdown-menu">
                            	    <li><a id="pop_findAgent" href="#">Find Local Assistance</a></li>
									<li><a href="https://help.nevadahealthlink.com" target="_blank">Frequently Asked Questions</a></li>
                            		<li><a href="https://help.nevadahealthlink.com/hc/en-us/articles/360029638331-Contact-Us" target="_blank">Contact Us</a></li>
								 </ul>
						 </li><!-- Hide "Find an Agent" from Employee view until NM3 -->
					 </c:if>

					 <c:if test="${userActiveRoleName=='broker' || userActiveRoleName=='issuer_representative'|| userActiveRoleName == 'assister'}">
						 <li class="dropdown">
						 	<a class="dropdown-toggle" data-toggle="dropdown" href="#">Help & Support <b class="caret"></b></a>
						 		<ul class="dropdown-menu">
									<li><a href="https://help.nevadahealthlink.com" target="_blank">Frequently Asked Questions</a></li>
                            		<li><a href="https://help.nevadahealthlink.com/hc/en-us/articles/360029638331-Contact-Us" target="_blank">Contact Us</a></li>
				        	</ul>
	                	</li>
					 </c:if>

                    <c:if test="${userRoles.size()==1}">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">My Account <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <c:if test="${isUserSwitchToOtherView == 'N'}">
                                    <c:choose>
                                        <c:when test="${isUserFullyAuthorized != null && isUserFullyAuthorized=='false' }">
                                            <li id="masthead-account-settings" hidden="true"><a href="#">Account Settings</a></li>
                                        </c:when>
                                        <c:otherwise>
                                            <li id="masthead-account-settings"><a href="<c:url value='/account/user/accountsettings'/>">Account Settings</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>

                                <c:choose>
                                    <c:when test="${switchToModuleNameLocal =='admin' || switchToModuleNameLocal=='issuer_admin' || switchToModuleNameLocal=='broker_admin' || switchToModuleNameLocal=='csr'}">
                                        <li id="masthead-dashboard"><a href="/hix/ticketmgmt/ticket/ticketlist">Dashboard</a></li>
                                    </c:when>
                                     <c:when test="${switchToModuleNameLocal=='issuer_representative'}">
                                        <li id="masthead-dashboard"><a href="/hix/planmgmt/issuer/landing/info">Dashboard</a></li>
                                    </c:when>
                                     <c:when test="${switchToModuleNameLocal=='broker'}">
                                        <li id="masthead-dashboard"><a href="<c:url value='/broker/certificationapplication'/>">Dashboard</a></li>
                                     </c:when>
                                     <c:when test="${switchToModuleNameLocal=='consumer'}">
                                        <li id="masthead-dashboard"><a href="/hix/memberportal/home">Dashboard</a></li>
                                     </c:when>
                                     <c:otherwise>
                                        <li id="masthead-dashboard"><a href="/hix/account/user/getDashBoard">Dashboard</a></li>
                                     </c:otherwise>
                                </c:choose>
                                <li><a href=<%=logoutUrl%>>Logout</a></li><!-- all -->
                            </ul>
                        </li>
                    </c:if>
	          	</c:if>
		        <!-- Loged In Ends -->

		        <!-- Logged Out -->
                <c:if test="${isUserAuthenticated=='false'}">

                    <li><a href="<c:url value='/account/user/login'/>">Log In</a></li><!-- all -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Help & Support <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#" id="pop_findAgent">Find Local Assistance</a></li> <!-- HIX -19718 -->
                            <li><a href="https://help.nevadahealthlink.com" target="_blank">Frequently Asked Questions</a></li>
                            <li><a href="https://help.nevadahealthlink.com/hc/en-us/articles/360029638331-Contact-Us" target="_blank">Contact Us</a></li>
                        </ul>
                    </li>
                </c:if>
		        <!-- Logged Out Ends -->
             </ul>
            </div>
        </div>
        <!-- nav-collapse ends -->
    </div>
    <!-- container ends -->
  </div>
  <!-- navbar-inner ends -->
</div><!-- navbar ends -->

<script type="text/javascript">
	/*
		HIX-18015  :  If the current url is one of registration flow, then
		              it required to hide the following masthead urls;
		              inbox
		              My Account -->Account Settings
		              My Account -->Dashboard
	
	*/
	var registrationUrls=new Array("account/securityquestions","/account/phoneverification/sendcode","/user/activation", "account/signup");
	
	var currDocUrl = document.URL;
	var urlIdx=0;
	var noOfRegUrls = registrationUrls.length;
	
	while(urlIdx <noOfRegUrls ){
		if(currDocUrl.indexOf(registrationUrls[urlIdx]) >0){
			$('#masthead-inbox').hide();
			$('#masthead-account-settings').hide();
			$('#masthead-dashboard').hide();
		} 
		urlIdx++;
	}
</script>

<script>
$('a.dropdown-toggle, .dropdown-menu a').on('touchstart', function(e) {
	  e.stopPropagation();
});	
</script>


<!-- navbar  ends -->
