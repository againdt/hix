package com.getinsured.hix.batch.enrollment.terminator;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.skip.EnrollmentAdminEffectuation;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;

public class TerminateEnrollmentAdminEffectuationJob   implements Tasklet {
	private static final Logger LOGGER = LoggerFactory.getLogger(TerminateEnrollmentAdminEffectuationJob.class);
	private EnrollmentAdminEffectuation enrollmentAdminEffectuation;
	private String enrollmentAdminEffArchivePath;
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		
		String enrollmentAdminEffectuationArchivePath;
		
		if (EnrollmentConfiguration.isCaCall()) {
			enrollmentAdminEffectuationArchivePath = enrollmentAdminEffArchivePath;
		} else {
			enrollmentAdminEffectuationArchivePath = DynamicPropertiesUtil.getPropertyValue(
					EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_ADMIN_EFFECTUATION_ARCHIVE_CSV_PATH);
		}
		
		//String enrollmentAdminEffectuationArchivePath = "D:\\vino\\Effectuation\\TestIRSArchive";
		
		copyWipFolderToArchive(new File(enrollmentAdminEffectuation.getWipFolderPath()), new File(enrollmentAdminEffectuationArchivePath+File.separator+enrollmentAdminEffectuation.getWipFolderName()));
		
		FileUtils.deleteDirectory(new File(enrollmentAdminEffectuation.getWipFolderPath()));
		
		enrollmentAdminEffectuation.setWipFolderName(null);
		enrollmentAdminEffectuation.setWipFolderPath(null);
		
		return null;
	}
	
	private void copyWipFolderToArchive(File srcDir, File destDir){
		try {
			FileUtils.copyDirectory(srcDir, destDir);
		} catch (IOException e) {
			LOGGER.error("Error in copyWipFolderToArchive()"+e.toString(),e);
		}
	}

	public EnrollmentAdminEffectuation getEnrollmentAdminEffectuation() {
		return enrollmentAdminEffectuation;
	}

	public void setEnrollmentAdminEffectuation(
			EnrollmentAdminEffectuation enrollmentAdminEffectuation) {
		this.enrollmentAdminEffectuation = enrollmentAdminEffectuation;
	}

	public String getEnrollmentAdminEffArchivePath() {
		return enrollmentAdminEffArchivePath;
	}

	public void setEnrollmentAdminEffArchivePath(String enrollmentAdminEffArchivePath) {
		this.enrollmentAdminEffArchivePath = enrollmentAdminEffArchivePath;
	}

}
