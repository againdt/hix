 <%@ include file="../include.jsp" %>
<div id="generalDetail" style="display: block;">


<div style="margin:2%">

 <!--Start Carrier-Details-->
 <div class="Carrier-Details">
  <ul>
    <li>Monthly Premium</li>
    <li>Carrier Details</li>       
    <li>Plan Details</li>                   
    <li>Annual Deductibles</li>           
    <li>Est.Out of Pocket Costs</li>
  </ul>
 </div>
 <!--end Carrier-Details-->
 
 
 <!--Start CarrDetBox-->
  <div class="CarrDetBox">
    <div class="MonPre">$132</div>
      <div class="CarDet">
        <img src="img/colorado.jpg">
        <div class="font-size9">Rating in Progress</div>
        <div class="font-size11"><input name="" type="checkbox" value="">Select to<br>
         Compare</div>
   </div>
    
    <div class="PlanDetai">
      <li>HealthOP Bobcat EPO</li>
      <li>Perferred Drug List</li>
      <li>EPO/Catastrophic</li>
    </div>
    
     <div class="AnnualDed">
        <li>$6,350 <span>/ Person</span></li>
        <li>$6,350 <span>/ Family</span></li>
     </div>
     
     <div class="EstPocket">
       <li>Annual Max, Costa
$6,350<span> / Person</span></li>
        <li>$12,700 <span>/ Family</span></li>
     </div>
      <div class="clear"></div> 
      <div class="Add-btn">ADD TO CART</div>
    
 </div>
 <!--end CarrDetBox-->
 
<div class="clear"></div>
   
<div id="accordion">
	<dl class="accordion" id="slider">
       <!--Start General Details-->
		<dt>General Details</dt>
		<dd id="general">
			<span class="accorGnrDetTitFont">
               <div class="accorGnrDetTit">General Details</div>
               <div class="accorGnrDetTit1">In Network(1)</div>
               <div class="accorGnrDetTit2">In Network(2)</div>
               <div class="accorGnrDetTit3">Out of Network</div>
            </span>
            
            
		</dd>
        <!--End General Details-->
        

		<!--Start Testing-->
        <dt id="major">Major</dt>
		     <dd id="fullDetail1">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
                </div>
               
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="homeCareAndHospice" onclick="homecare()">Home Care and Hospice</dt>
		     <dd id="fullDetail2">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
               
               
		 </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="facilities" onclick="facilities()">Facilities</dt>
		     <dd id="fullDetail3">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="prescription" onclick="prescription()">Prescription and Drugs</dt>
		    <dd id="fullDetail4">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="testing" onclick="testing()">Testing</dt>
		     <dd id="fullDetail5">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="dentalAndVision" onclick="dental()">Dental and Vision</dt>
		     <dd id="fullDetail6">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="maternity" onclick="maternity()">Maternity</dt>
		     <dd id="fullDetail7">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="diagnostic">Diagnostic</dt>
		     <dd id="fullDetail8">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="basic">Basic</dt>
		     <dd id="fullDetail9">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="habilitativeAndRehabilitativeServices" onclick="hars()">Habilitative and Rehabilitative Services</dt>
		    <dd id="fullDetail10">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="mentalHealthBenefits" onclick="mhb()">Mental Health Benifits</dt>
		     <dd id="fullDetail11">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="emergency" onclick="emergency()">Emergency</dt>
		     <dd id="fullDetail12">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="medicalDevices" onclick="medicalDevices()">Medical Devices</dt>
		     <dd id="fullDetail13">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="additionalBenefits">Additional Benefits</dt>
		    <dd id="fullDetail14">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="highCostServices" onclick="hcs()">High Cost Services</dt>
		     <dd id="fullDetail15">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
          <!--End Testing-->
          
          <!--Start Testing-->
        <dt id="providerOfficeVisits" onclick="pov()">Provider Office Visits</dt>
		     <dd id="fullDetail16">
			   <div class="accoLineTitleBGG">
                <span class="accorGnrDetTitFont">
                  <div class="accorGnrDetTit"></div>
                  <div class="accorGnrDetTit1">In Network(1)</div>
                  <div class="accorGnrDetTit2">In Network(2)</div>
                  <div class="accorGnrDetTit3">Out of Network</div>
                 </span>
               </div>
                
		  </dd>
          <!--End Testing-->
          
         
          
          
         <!--Start Plan Documents-->
        <dt>Plan Documents</dt>
		     <dd>
                 <span>
                   <div class="accoLineDocu">HC_COMCC</div>
                   <div class="accoLineDocu">HC_COMCC</div>
                   <div class="accoLineDocu">HC_COMCC</div>
               </span>
		  </dd>
		</dl>
          <!--End Plan Documents-->
          
        
</div>
</div>

 <script type="text/javascript">


	var slider1=new accordion.slider("slider1");
	slider1.init("slider");
	$( "#generalDetail").css("display", "none");
	//$("#generalDetail").hide();



//var slider2=new accordion.slider("slider2");
//slider2.init("slider2",-1,"open");

</script> 


</div>
