<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
    targetNamespace="http://vlpdvc.ee.sim.dsh.cms.hhs.gov" xmlns="http://vlpdvc.ee.sim.dsh.cms.hhs.gov"
    elementFormDefault="qualified"
    xmlns:vlpdvc="http://vlpdvc.ee.sim.dsh.cms.hhs.gov"
    version="1.0">
    
    <xsd:annotation>
        <xsd:documentation>
            <SchemaComponent>
                <EffectiveDt>2013-05-01</EffectiveDt>
                <VersionDt>2013-04-09</VersionDt>
                <VersionDescriptionTxt>
                    This is the initial version of v33 DHSVerifiedCases request and response
                    callback service.
                </VersionDescriptionTxt>
                <DescriptionTxt>Request and Response payloads for the SOA service DHSVerifiedCases, 
                    callback service of Verify Lawful Presence. Being a callback service, this service will
                    be hosted by the exchange, and called by the Hub.
                </DescriptionTxt>
            </SchemaComponent>
        </xsd:documentation>
    </xsd:annotation>
    
    <!--##########################################################-->
    <!--#                                                        #-->
    <!--#                        Root Nodes                      #-->
    <!--#                                                        #-->
    <!--##########################################################-->
    
    <xsd:element name="DHSVerifiedCasesRequest" type="vlpdvc:DHSVerifiedCasesRequestType">
        <xsd:annotation>
            <xsd:documentation>
                <Component>
                    <DictionaryEntryNm>Request Message - DHSVerifiedCases of VLP </DictionaryEntryNm>
                    <DescriptionTxt>Request payload for the SOA service DHSVerifiedCases (from
                        Verify Lawful Presence)</DescriptionTxt>
                </Component>
            </xsd:documentation>
        </xsd:annotation>
    </xsd:element>
    
    <xsd:element name="DHSVerifiedCasesResponse" type="vlpdvc:DHSVerifiedCasesResponseType">
        <xsd:annotation>
            <xsd:documentation>
                <Component>
                    <DictionaryEntryNm>Response Message - DHSVerifiedCases of VLP  </DictionaryEntryNm>
                    <DescriptionTxt>Response payload for the SOA service DHSVerifiedCases (from
                        Verify Lawful Presence)</DescriptionTxt>
                </Component>
            </xsd:documentation>
        </xsd:annotation>
    </xsd:element>
    
    <!--##########################################################-->
    <!--#                                                        #-->
    <!--#                      Complex Types                     #-->
    <!--#                                                        #-->
    <!--##########################################################-->
    
    <xsd:complexType name="DHSVerifiedCasesRequestType">
        <xsd:sequence>
            <xsd:element ref="vlpdvc:CaseListArray"/>
        </xsd:sequence>
    </xsd:complexType>
    
    <xsd:complexType name="CaseListArrayType">
        <xsd:sequence>
            <xsd:element ref="vlpdvc:CaseList" maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>
    
    <xsd:complexType name="CaseListType">
        <xsd:sequence>
            <xsd:element ref="vlpdvc:CaseNumber"/>
            <xsd:element ref="vlpdvc:CaseType"/>
        </xsd:sequence>
    </xsd:complexType>
    
    
    <xsd:complexType name="DHSVerifiedCasesResponseType">
        <xsd:sequence>
            <xsd:element ref="vlpdvc:ResponseMetadata"/>
        </xsd:sequence>
    </xsd:complexType>
    
    <xsd:complexType name="ResponseMetadataType">
        <xsd:sequence>
            <xsd:element ref="vlpdvc:ResponseCode"/>
            <xsd:element ref="vlpdvc:ResponseDescriptionText"/> 
        </xsd:sequence>
    </xsd:complexType> 
    
    <!--##########################################################-->
    <!--#                                                        #-->
    <!--#                       Simple Types                     #-->
    <!--#                                                        #-->
    <!--##########################################################-->
    
    <xsd:simpleType name="CaseTypeType">
        <xsd:annotation>
            <xsd:documentation>This flag indicates the type of case resolution. The value of 2 indicates that the case has
                been resolved at the additional level and the user needs to call RetrieveAdditResolution
                and a value of 3 indicates that the case has been resolved at the third level and the user
                user needs to call RetrieveThirdResolution.
            </xsd:documentation>
        </xsd:annotation>
        <xsd:restriction base="xsd:string">
            <xsd:enumeration value="2"/>
            <xsd:enumeration value="3"/>
        </xsd:restriction>
    </xsd:simpleType>    
    
    <xsd:simpleType name="CaseNumberType">
        <xsd:annotation>
            <xsd:documentation>The unique case verification number assigned to this query.
            </xsd:documentation>
        </xsd:annotation>
        <xsd:restriction base="xsd:string">
            <xsd:pattern value="[a-z|A-Z|0-9]{13}[A-Z]{2}"/>
        </xsd:restriction>
    </xsd:simpleType>  
    
    <!-- ============================================-->
    <!--          Error Handling Datatypes           -->
    <!-- ============================================-->
    
    <xsd:simpleType name="ResponseCodeType">
        <xsd:annotation>
            <xsd:documentation>
                Response code populated by the Exchange indicating failure or success of the request.
                HS000000 - Successful
                HX009000 - System error while submitting information
            </xsd:documentation>
        </xsd:annotation>
        <xsd:restriction base="xsd:string">
            <xsd:enumeration value="HS000000"/>
            <xsd:enumeration value="HX009000"/>
        </xsd:restriction>
    </xsd:simpleType>
    
    <xsd:simpleType name="ResponseDescriptionTextType">
        <xsd:annotation>
            <xsd:documentation>
                Description of the Response Code.
                HS000000 - Successful
                HX009000 - System error while submitting information
            </xsd:documentation>
        </xsd:annotation>
        <xsd:restriction base="xsd:string">
            <xsd:minLength value="1"/>
            <xsd:maxLength value="50"/>
        </xsd:restriction>
    </xsd:simpleType>
    
    <!--##########################################################-->
    <!--#                                                        #-->
    <!--#                       Elements                         #-->
    <!--#                                                        #-->
    <!--##########################################################-->
    
    <xsd:element name="CaseListArray" type="vlpdvc:CaseListArrayType">
        <xsd:annotation>
            <xsd:documentation>This can hold multiple instances of the CaseList object, creating
                the "Array".
            </xsd:documentation>
        </xsd:annotation>
    </xsd:element>
    <xsd:element name="CaseList" type="vlpdvc:CaseListType">
        <xsd:annotation>
            <xsd:documentation>Contains CaseNumber and CaseType. Repeatable to create an array
                in the CaseListArray object.
            </xsd:documentation>
        </xsd:annotation>
    </xsd:element>
    <xsd:element name="CaseNumber" type="vlpdvc:CaseNumberType">
        <xsd:annotation>
            <xsd:documentation>The unique case verification number assigned to this query.
            </xsd:documentation>
        </xsd:annotation>
    </xsd:element>
    <xsd:element name="CaseType" type="vlpdvc:CaseTypeType">
        <xsd:annotation>
            <xsd:documentation>This flag indicates the type of case resolution. The value of 2 indicates that the case has
                been resolved at the additional level and the user needs to call RetrieveAdditResolution
                and a value of 3 indicates that the case has been resolved at the third level and the user
                user needs to call RetrieveThirdResolution.
            </xsd:documentation>
        </xsd:annotation>
    </xsd:element>    
    
    <xsd:element name="ResponseMetadata" type="vlpdvc:ResponseMetadataType">
        <xsd:annotation>
            <xsd:documentation>Response code and description from exchange to the Hub.
            </xsd:documentation>
        </xsd:annotation>
    </xsd:element>
    <xsd:element name="ResponseCode" type="vlpdvc:ResponseCodeType">
        <xsd:annotation>
            <xsd:documentation>
                Response code populated by the Exchange indicating failure or success of the request.
                HS000000 - Successful
                HX009000 - System error while submitting information
            </xsd:documentation>
        </xsd:annotation>
    </xsd:element>
    <xsd:element name="ResponseDescriptionText" type="vlpdvc:ResponseDescriptionTextType">
        <xsd:annotation>
            <xsd:documentation>
                Description of the Response Code.
                HS000000 - Successful
                HX009000 - System error while submitting information
            </xsd:documentation>
        </xsd:annotation>
    </xsd:element>
    
</xsd:schema>

