package com.getinsured.iex.hub.ifsv.wrapper;



import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.ifsv.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.hub.ifsv.niem.niem.niem_core._2.ObjectFactory;
import com.getinsured.iex.hub.ifsv.niem.niem.niem_core._2.PersonNameTextType;
import com.getinsured.iex.hub.ifsv.niem.niem.niem_core._2.PersonType;
import com.getinsured.iex.hub.platform.HubServiceException;

/**
 * Wrapper class for com.getinsured.iex.hub.ifsv.niem.niem.niem_core._2.PersonType
 * 
 * @author Nikhil Talreja
 * @since 27-Feb-2014
 *
 */
public class PersonTypeWrapper implements JSONAware{
	
	private ObjectFactory factory;
	private com.getinsured.iex.hub.ifsv.niem.niem.proxy.xsd._2.ObjectFactory proxyFactory; 
	
	private PersonType person;

	private String personGivenName;
	private String personMiddleName;
	private String personSurName;
	private String personSSNIdentification;
	
	private static final String FIRST_NAME_KEY = "personGivenName";
	private static final String MIDDLE_NAME_KEY = "personMiddleName";
	private static final String LAST_NAME_KEY = "personSurName";
	private static final String SSN_KEY = "personSSNIdentification";
	
	public PersonTypeWrapper(){
		factory = new ObjectFactory();
		proxyFactory = new com.getinsured.iex.hub.ifsv.niem.niem.proxy.xsd._2.ObjectFactory();
		this.person = factory.createPersonType();
		this.person.setPersonName(factory.createPersonNameType());
		this.person.setPersonSSNIdentification(factory.createIdentificationType());
	}

	public PersonType getPerson() {
		return person;
	}

	public String getPersonGivenName() {
		return personGivenName;
	}

	public void setPersonGivenName(String personGivenName) {
		if(personGivenName != null && personGivenName.trim().length() > 0){
			PersonNameTextType firstName = factory.createPersonNameTextType();
			firstName.setValue(personGivenName);
			this.personGivenName = personGivenName;
			this.person.getPersonName().setPersonGivenName(firstName);
		}
	}

	public String getPersonMiddleName() {
		return personMiddleName;
	}

	public void setPersonMiddleName(String personMiddleName) {
		if(personMiddleName != null && personMiddleName.trim().length() > 0){
			PersonNameTextType middleName = factory.createPersonNameTextType();
			middleName.setValue(personMiddleName);
			this.personMiddleName = personMiddleName;
			this.person.getPersonName().setPersonMiddleName(middleName);
		}
	}

	public String getPersonSurName() {
		return personSurName;
	}

	public void setPersonSurName(String personSurName) {
		if(personSurName != null && personSurName.trim().length() > 0){
			PersonNameTextType lastName = factory.createPersonNameTextType();
			lastName.setValue(personSurName);
			this.personSurName = personSurName;
			this.person.getPersonName().setPersonSurName(lastName);
		}
	}

	public String getPersonSSNIdentification() {
		return personSSNIdentification;
	}

	public void setPersonSSNIdentification(String personSSNIdentification) {
		com.getinsured.iex.hub.ifsv.niem.niem.proxy.xsd._2.String ssn = proxyFactory.createString();
		ssn.setValue(personSSNIdentification);
		IdentificationType identificationType = factory.createIdentificationType();
		identificationType.setIdentificationID(ssn);
		this.personSSNIdentification = personSSNIdentification;
		this.person.setPersonSSNIdentification(identificationType);
	}

	/**
	 * Converts the JSON String to PersonTypeWrapper object
	 * 
	 * @param jsonString - String representation for PersonTypeWrapper
	 * @return PersonTypeWrapper Object
	 */
	public static PersonTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create PersonTypeWrapper from null or empty input");
		}

		Object tmpObj = null;
		
		PersonTypeWrapper wrapper = new PersonTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(FIRST_NAME_KEY);
		if(tmpObj != null){
			wrapper.personGivenName = (String)tmpObj;
			wrapper.setPersonGivenName(wrapper.personGivenName);
		}
		
		tmpObj = obj.get(MIDDLE_NAME_KEY);
		if(tmpObj != null){
			wrapper.personMiddleName = (String)tmpObj;
			wrapper.setPersonMiddleName(wrapper.personMiddleName);
		}
		
		tmpObj = obj.get(LAST_NAME_KEY);
		if(tmpObj != null){
			wrapper.personSurName = (String)tmpObj;
			wrapper.setPersonSurName(wrapper.personSurName);
		}
		
		tmpObj = obj.get(SSN_KEY);
		if(tmpObj != null){
			wrapper.personSSNIdentification = (String)tmpObj;
			wrapper.setPersonGivenName(wrapper.personSSNIdentification);
		}
		
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(FIRST_NAME_KEY, this.personGivenName);
		obj.put(MIDDLE_NAME_KEY, this.personMiddleName);
		obj.put(LAST_NAME_KEY, this.personSurName);
		obj.put(SSN_KEY, this.personSSNIdentification);
		return obj.toJSONString();
	}
	
	public static PersonTypeWrapper fromJSONObject(JSONObject jsonObj) throws HubServiceException{
		PersonTypeWrapper wrapper = new PersonTypeWrapper();
		wrapper.setPersonGivenName((String) jsonObj.get(FIRST_NAME_KEY));
		wrapper.setPersonMiddleName((String) jsonObj.get(MIDDLE_NAME_KEY));
		wrapper.setPersonSurName((String) jsonObj.get(LAST_NAME_KEY));
		wrapper.setPersonGivenName((String) jsonObj.get(SSN_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(FIRST_NAME_KEY, this.personGivenName);
		obj.put(MIDDLE_NAME_KEY, this.personMiddleName);
		obj.put(LAST_NAME_KEY, this.personSurName);
		obj.put(SSN_KEY, this.personSSNIdentification);
		return obj;
	}

}
