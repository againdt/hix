<!-- 
 @author Ashok 
 @UserStory : HIX-2308
 @Jira-id : HIX-2461
 -->
<%@ page import="com.getinsured.hix.platform.util.PhoneUtil"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page isELIgnored="false"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>

<div class="gutter10">
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="pgheader.account"/></a></li>
            <li><a href="<c:url value='/planmgmt/managerepresentative'/>"><spring:message  code="label.manageRepresentatives"/></a></li>
            <li>${USER_OBJ.firstName} ${USER_OBJ.lastName}</li>
        </ul><!--page-breadcrumb ends-->
    	<h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${hiosissuerid}"/>"/> ${USER_OBJ.firstName}<c:if test="${USER_OBJ.lastName != ''}"> ${USER_OBJ.lastName}</c:if>  <small><spring:message code="pgheader.authRepAddedOn"/> ${repUserCreatedOn}</small></h1>
    </div>
</div>
<div class="row-fluid">
	<div class="gutter10">
	<div class="span3" id="sidebar">
               <div class="header">
                   <h4 class="margin0"><spring:message  code="label.details"/></h4>
               </div>
               <!--  beginning of side bar -->
               <ul class="nav nav-list">
                   <li class="active"><spring:message  code="label.repInfoReview"/></li>
               </ul>
               <!-- end of side bar -->
	</div>
		<!-- end of span3 -->
           <div class="span9" id="rightpanel">
           <c:set var="encIssuerRepId" ><encryptor:enc value="${repId}" isurl="true"/> </c:set>
                <div class="header">
                   <h4 class="span9"><spring:message  code="label.repInfo"/></h4>
                   <a href='<c:url value="/planmgmt/managerepresentative"/>' class="btn btn-small pull-right"><spring:message  code="label.btnCancel"/></a>

					<c:if test="${STATE_CODE != 'CA' || (STATE_CODE == 'CA' && displayStatus != null && fn:toUpperCase(displayStatus) == 'INACTIVE')}">
						<a href='<c:url value="/planmgmt/editrepresentative/${encIssuerRepId}"/>' class="btn btn-small pull-right"><spring:message  code="label.editColumnHeading"/></a>
					</c:if>
               </div>
               
               <div class="gutter10">
 	 			<table class="table table-condensed table-border-none">
	 	 	 		<c:choose>
	 	 	 			<c:when test="${isFromUser eq 'USER'}">
	 	 					<tr>
	 	 						<td class="txt-right span3"><spring:message  code="label.firstName"/></td>
	 	 	 					<td><strong>${USER_OBJ.firstName}</strong></td>
	 	 	 				</tr>
	 	 	 				<tr>
	 	 	 					<td class="txt-right span3"><spring:message  code="label.lastName"/></td>
	 	 	 					<td><strong>${USER_OBJ.lastName}</strong></td>
	 	 	 				</tr>
	 	 	 				<tr>
	 	 	 					<td class="txt-right span3"><spring:message  code="label.title"/></td>
	 	 	 					<td><strong>${issuerRepObj.title}</strong></td>
	 	 	 				</tr>
	 	 	 				<tr>
	 	 	 					<td class="txt-right span3"><spring:message  code="label.phoneNumber"/></td>
	 	 	 					<c:set var="phoneNumber" value="${USER_OBJ.phone}"/>	 	 	 					
	 	 	 					<td><strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("phoneNumber"))%></strong></td>
	 	 	 				</tr>
	 	 	 				<tr>
	 	 	 					<td class="txt-right span3"><spring:message  code="label.emailAddress"/></td>
	 	 	 					<td><strong>${USER_OBJ.email}</strong></td>
	 	 	 				</tr>
	 	 	 				<tr>
	 	 	 					<td class="txt-right span3"><spring:message  code="label.primaryContact"/></td>
	 	 	 					<td><strong>${issuerRepObj.primaryContact}</strong></td>
	 	 	 				</tr>
	 	 	 			</c:when>
	 	 	 			<c:otherwise>
	 	 	 				<tr>
	 	 	 					<td class="txt-right span3"><spring:message  code="label.firstName"/></td>
	 	 						<td><strong>${issuerRepObj.firstName}</strong></td>
	 	 					</tr>
	 	 					<tr>
	 						</tr>
	 	 					<tr>
	 	 						<td class="txt-right span3"><spring:message  code="label.emailAddress"/></td>
	 	 	 					<td><strong>${repUser.userRecord.email}</strong></td>
	 	 					</tr>
	 	 					<tr>
	 	 						<td class="txt-right span3"><spring:message  code="label.primaryContact"/></td>
	 	 						<td><strong>${issuerRepObj.primaryContact}</strong></td>
	 	 					</tr>
	 	 	 			</c:otherwise>
	 	 	 		</c:choose>                                             
 	 			</table>
 	 		</div>
 	 	</div><!--  end of span9 -->
	</div>
</div><!-- end row-fluid -->
