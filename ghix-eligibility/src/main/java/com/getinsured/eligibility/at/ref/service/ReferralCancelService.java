package com.getinsured.eligibility.at.ref.service;

/**
 * @author chopra_s
 * 
 */
public interface ReferralCancelService {
	boolean cancelActivation(int ssapApplicationId);
}
