const getUpdatedFlags = (flags, flagsForSaving) => {
    flagsForSaving.forEach(([masks = null, value]) => {
        masks !== null && masks.forEach(mask => {
            if(mask.value === value) {
                flags |= mask.mask;
            } else {
                flags &= ~mask.mask;
            }
        });
    });
    return flags;
};

const getFlagValue = flags => {
    return masks => {
        const neededMask = masks.find(mask => mask.mask === (flags & mask.mask));
        if(!neededMask) {
            console.log('Flag not found');
            return neededMask;
        } else {
            return neededMask.value;
        }
    };
};

const convertFlagsToValues = (flags, masks) => {
    const result = {};

    Object.keys(masks).forEach(maskName => {
        const neededMask = (m => m ? m : {value: 'not got'})(masks[maskName].find(mask => mask.mask === (flags & mask.mask)));
        result[maskName.split('Flags')[0]] = neededMask.value;
    });
    return result;
};

const setInitialValue = (masks, values) => {

    const initialData = Object.values(masks);
    if(initialData.length !== values.length) {
        console.log('Cannot get initial flags value');
        return undefined;
    } else {
        return initialData
            .map((ind, idx) => ind.find(i => i.value === values[idx]).mask)
            .reduce((acc, mask) => {
                acc |= mask;
                return acc;
            });
    }
};

export default {
    convertFlagsToValues,
    getFlagValue,
    getUpdatedFlags,
    setInitialValue
};