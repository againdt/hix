package com.getinsured.iex.indportal.dto;

public class ReinstateEnrollmentDTO {
	
	private String healthEnrollmentId;
	private Boolean reinstateHealth;
	private String dentalEnrollmentId;
	private Boolean reinstateDental;
	private String caseNumber;
	
	public String getHealthEnrollmentId() {
		return healthEnrollmentId;
	}
	public void setHealthEnrollmentId(String healthEnrollmentId) {
		this.healthEnrollmentId = healthEnrollmentId;
	}
	public Boolean getReinstateHealth() {
		return reinstateHealth;
	}
	public void setReinstateHealth(Boolean reinstateHealth) {
		this.reinstateHealth = reinstateHealth;
	}
	public String getDentalEnrollmentId() {
		return dentalEnrollmentId;
	}
	public void setDentalEnrollmentId(String dentalEnrollmentId) {
		this.dentalEnrollmentId = dentalEnrollmentId;
	}
	public Boolean getReinstateDental() {
		return reinstateDental;
	}
	public void setReinstateDental(Boolean reinstateDental) {
		this.reinstateDental = reinstateDental;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

}
