//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.08.09 at 06:16:25 PM PDT 
//


package com.getinsured.ridpv2.niem.niem.structures._2;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.PrimaryRequestInformationType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.RequestPayloadType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.ResponsePayloadType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.SecondaryRequestInformationType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.SessionIdentificationType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.VerificationAnswerAndQuestionNumberType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.VerificationAnswerSetType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.VerificationQuestionSetType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.VerificationQuestionType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.VerificationResponseType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.AddressType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.ContactInformationType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.DateType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.LocationType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.PersonLanguageType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.PersonNameType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.PersonType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.StreetType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.StructuredAddressType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.TelephoneNumberType;


/**
 * <p>Java class for ComplexObjectType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ComplexObjectType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute ref="{http://niem.gov/niem/structures/2.0}id"/>
 *       &lt;attribute ref="{http://niem.gov/niem/structures/2.0}metadata"/>
 *       &lt;attribute ref="{http://niem.gov/niem/structures/2.0}linkMetadata"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplexObjectType")
@XmlSeeAlso({
    ResponsePayloadType.class,
    RequestPayloadType.class,
    LocationType.class,
    VerificationResponseType.class,
    VerificationQuestionType.class,
    SecondaryRequestInformationType.class,
    VerificationAnswerSetType.class,
    PrimaryRequestInformationType.class,
    ContactInformationType.class,
    VerificationQuestionSetType.class,
    VerificationAnswerAndQuestionNumberType.class,
    SessionIdentificationType.class,
    PersonLanguageType.class,
    AddressType.class,
    DateType.class,
    StreetType.class,
    PersonNameType.class,
    StructuredAddressType.class,
    TelephoneNumberType.class,
    PersonType.class,
    IdentificationType.class
})
public abstract class ComplexObjectType {

    @XmlAttribute(name = "id", namespace = "http://niem.gov/niem/structures/2.0")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "metadata", namespace = "http://niem.gov/niem/structures/2.0")
    @XmlIDREF
    @XmlSchemaType(name = "IDREFS")
    protected List<Object> metadata;
    @XmlAttribute(name = "linkMetadata", namespace = "http://niem.gov/niem/structures/2.0")
    @XmlIDREF
    @XmlSchemaType(name = "IDREFS")
    protected List<Object> linkMetadata;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadata property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadata().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getMetadata() {
        if (metadata == null) {
            metadata = new ArrayList<Object>();
        }
        return this.metadata;
    }

    /**
     * Gets the value of the linkMetadata property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the linkMetadata property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLinkMetadata().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getLinkMetadata() {
        if (linkMetadata == null) {
            linkMetadata = new ArrayList<Object>();
        }
        return this.linkMetadata;
    }

}
