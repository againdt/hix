<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page import="java.util.Date"%>	
<link href="<c:url value="/resources/css/kendo.common.min.css"/>" rel="stylesheet" />
<link href="<c:url value="/resources/css/kendo.default.min.css"/>" rel="stylesheet" />
<link href="<c:url value="/resources/css/kendo.dataviz.bootstrap.min.css"/>" rel="stylesheet" />

<script type="text/javascript">
	var baseURL = '<c:url value="/resources/js/vega/"/>';
	var jobName='';
	var newSchedule='';
	var gridService='/hix/admin/getJobList';
	var gridUpdateService='/hix/admin/scheduleJob?jobName='+jobName+'&cronExp='+newSchedule;
</script>
<style>
#kGrid td{
	word-break:break-word;
}
#kGrid .k-grid-Update{
	width:80px;
}
</style>

<!--  <script type="text/javascript" src="<c:url value="/resources/js/jquery-1.8.2.min.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.9.2.custom.min.js" />"></script> 
 <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/angular.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-route.js"/>"></script> -->
<script type="text/javascript" src="<c:url value="/resources/js/kendo.all.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/vega/required.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/vega/baseComponent.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/vega/vega.js"/>"></script>

<script type="text/javascript" src="<c:url value="/resources/js/jobList.js"/>"></script>
<div class="gutter10">
	<div>
		<h1>Job Schedules</h1>
		<label for="Server time" align="right">Server Time:
			 <input id="serverTime"  value="" readonly="true" class="k-textbox" style="border:none;width: 200px;" /> </label>
	</div>
	<df:csrfToken/>

	</div>
	<div class="row-fluid" id="jobListApp">	</div>
</div>