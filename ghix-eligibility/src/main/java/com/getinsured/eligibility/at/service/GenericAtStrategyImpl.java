package com.getinsured.eligibility.at.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.mes.queue.dto.ATSpanInfoRequestDTO;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.service.ReferralOEService;
import com.getinsured.eligibility.at.resp.service.SsapApplicantService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.eligibility.at.resp.si.dto.AtAdditionalParamsDTO;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralUtil;

/**
 * This calss has been copied from IDProcessing
 * 
 */
@Component("genericAtStrategyImpl")
public class GenericAtStrategyImpl extends AccountTransferStrategyImpl {

	

	private static final Logger LOGGER = Logger.getLogger(GenericAtStrategyImpl.class);

	@Autowired
	@Qualifier("customValidatorImpl")
	private CustomValidator customValidator;

	@Autowired
	@Qualifier("referralOEService")
	private ReferralOEService referralOEService;

	@Autowired
	@Qualifier("renewalDetermination")
	private BaseDetermination renewalDetermination;

	@Autowired
	@Qualifier("mainDetermination")
	private BaseDetermination mainDetermination;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Override
	public void process(AccountTransferRequestPayloadType request,AtAdditionalParamsDTO additionalParams) {
		LOGGER.info("REFERRAL FLOW STARTS GI WS PAYLOAD: "+additionalParams.getGiWsPayloadId());
		Integer giwsPayloadId = additionalParams.getGiWsPayloadId();
		String requester = additionalParams.getRequester();
		
		final String identificationId = GenericAtStrategyImpl.retrieveIdentificationId(request);
		if (identificationId == null) {
			/* should not reach here... */
			return;
		}
		
		final long coverageYear = ReferralUtil.extractCoverageYear(request.getInsuranceApplication().getApplicationExtension());
		final boolean isRenewal = referralOEService.isRenewal(coverageYear);
		// TODO: if renewal stop
		AccountTransferRequestDTO accountTransferRequestDTO;
		if (isRenewal) {
			accountTransferRequestDTO = renewalDetermination.determineProcessor(identificationId, coverageYear, request, giwsPayloadId, null, null);
		} else {
			accountTransferRequestDTO = mainDetermination.determineProcessor(identificationId, coverageYear, request, giwsPayloadId, null, null);
		}
		// Set requester of the method. This will be further used to determine which step of the referral process to be called.
		accountTransferRequestDTO.setRequester(StringUtils.isNotEmpty(requester) ? requester : ReferralProcessingConstants.REQUESTER_AT);
		//Set additional fields if any which will be used in spring integration flow.
		accountTransferRequestDTO.setAdditionalFieldsMapXmlString(additionalParams.getAdditionalFieldsMapXmlString());
		accountTransferRequestDTO.setAtSpanInfoId(additionalParams.getAtSpanInfoId());
		
		callReferralProcess(accountTransferRequestDTO);
		LOGGER.info("ID - REFERRAL/LCE FLOW ENDS "+giwsPayloadId);
	}

	

	private static String retrieveIdentificationId(AccountTransferRequestPayloadType request) {
		String identificationId = null;
		final PersonType pType = (PersonType) request.getInsuranceApplication().getSSFPrimaryContact().getRoleOfPersonReference().getRef();
		final String pcRefId = pType.getId();
		List<PersonType> personList = request.getPerson();
		for (PersonType personType : personList) {
			if (StringUtils.equalsIgnoreCase(pcRefId, personType.getId())) {
				if (personType.getPersonAugmentation() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID() != null) {
					identificationId = personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID().getValue();
				}

				if (identificationId == null) {
					if (personType.getPersonAugmentation() != null && personType.getPersonAugmentation().getPersonCHIPIdentification() != null && personType.getPersonAugmentation().getPersonCHIPIdentification().getIdentificationID() != null) {
						identificationId = personType.getPersonAugmentation().getPersonCHIPIdentification().getIdentificationID().getValue();
					}
				}
				break;
			}
		}
		return identificationId;
	}

	@Override
	public List<String> validate(AccountTransferRequestPayloadType request) {
		return customValidator.validate(request);
	}

	@Override
	public void processEditApplication(String caseNumber) {
		LOGGER.info("NV - REFERRAL/LCE FLOW DTO STARTS "+caseNumber);

		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByCaseNumber(caseNumber);

		SsapApplication ssapApplication = null;
		if (ssapApplications != null && ssapApplications.size() > 0) {
			ssapApplication = ssapApplications.get(0);
		} else {
			throw new GIRuntimeException("No ssap application found!");
}
		final boolean isRenewal = referralOEService.isRenewal(ssapApplication.getCoverageYear());
		// TODO: if renewal stop
		AccountTransferRequestDTO accountTransferRequestDTO;
		List<SsapApplicant> ssapApplicants =  ssapApplicantRepository.findPrimaryByApplication(ssapApplication.getId());
		SsapApplicant ssapApplicant = null;
		if (ssapApplicants != null && ssapApplicants.size() > 0) {
			ssapApplicant = ssapApplicants.get(0);
		} else {
			throw new GIRuntimeException("No ssap applicant found!");
		}
		if (isRenewal) {
			accountTransferRequestDTO = renewalDetermination.determineProcessor(ssapApplicant.getExternalApplicantId(), ssapApplication.getCoverageYear(), null, null,ssapApplicant,caseNumber);
		} else {
			accountTransferRequestDTO = mainDetermination.determineProcessor(ssapApplicant.getExternalApplicantId(), ssapApplication.getCoverageYear(), null, null, ssapApplicant,caseNumber);
		}
		accountTransferRequestDTO.setCaseNumber(caseNumber);
		callApplicationProcess(accountTransferRequestDTO);
		LOGGER.info("NV - REFERRAL/LCE FLOW DTO Ends "+caseNumber);
	}

}
