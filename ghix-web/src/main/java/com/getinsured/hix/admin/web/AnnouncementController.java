package com.getinsured.hix.admin.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.TSLocalTime;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.admin.service.AnnouncementRoleService;
import com.getinsured.hix.admin.service.AnnouncementService;
import com.getinsured.hix.admin.util.AnnouncementComparator;
import com.getinsured.hix.admin.util.AnnouncementDTO;
import com.getinsured.hix.admin.util.AnnouncementDTO.ModuleName;
import com.getinsured.hix.indportal.IndividualPortalConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Announcement;
import com.getinsured.hix.model.Announcement.Status;
import com.getinsured.hix.model.AnnouncementRole;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIException;

@Controller
public class AnnouncementController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AnnouncementController.class);
	private static final int LISTING_NUMBER = 5;
	public static final String FAILURE = "failure";
    public static final String SUCCESS = "success";
    public static final String ACTIVE = "Active";
	
	@Autowired private UserService userService;
	@Autowired private AnnouncementService announcementService;
	@Autowired private AnnouncementRoleService announcementRoleService;
	@Autowired private RoleService roleService;
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
	
	//@RequestMapping(value="/admin/announcement/create",method = RequestMethod.GET)
	public String createAnnouncement (Model model, HttpServletRequest request) {
		LOGGER.info("Create Announcement Page ");
		
		if(! this.isAllowed(model, request) ){
			return "redirect:/account/user/loginfailed";
		}
		
		List<Role> roles = roleService.findAll();
		model.addAttribute("roles", roles);
		model.addAttribute("page_title", "Getinsured Health Exchange: Announcement Create Page" );
		
		return "admin/announcement/create";
	}
	
	//@RequestMapping(value="/admin/announcement/create",method = RequestMethod.POST)
	public String createSubmit (@ModelAttribute("Announcement") Announcement announcement, Model model, HttpServletRequest request) {
		LOGGER.info("Create Announcement Submit Page ");

		AccountUser user = null;
		try{
           user = userService.getLoggedInUser();
        }catch(InvalidUserException ex){
	      LOGGER.error("User not logged in");
	      return "redirect:/account/user/login";
        }
		
		if(! this.isAllowed(model, request) ){
			return "redirect:/account/user/loginfailed";
		}
		
		String[] roles = request.getParameterValues("roles");
		List<AnnouncementRole> announcementRoleList = announcementService.setAnnouncementRoleList(announcement, roles); 
		
		announcement.setCreatedBy(user.getId());
		announcement.setCreatedDate(new TSDate());
		announcement.setAnnouncementRole(announcementRoleList);
		Announcement announcementObj = announcementService.save(announcement);
		
		List<Role> roleList = roleService.findAll();
		model.addAttribute("roles", roleList);
		
		return "redirect:manage";
	}
	
	//@RequestMapping(value="/admin/announcement/chkName", method=RequestMethod.POST)
	public @ResponseBody String chkName(@RequestParam String name) {
		Announcement announcementObj = announcementService.findByName(name);
		if(announcementObj != null ) { return "EXIST"; }
		return "NEW";
	}
	
	//@RequestMapping(value="/admin/announcement/edit/{id}",method = RequestMethod.GET)
	public String editAnnouncement (@PathVariable("id") Integer id, Model model, HttpServletRequest request) {
		LOGGER.info("Edit Announcement Page ");
		
		if(! this.isAllowed(model, request) ){
			return "redirect:/account/user/loginfailed";
		}
		
		List<Role> roles = roleService.findAll();
		Announcement announcementObj = announcementService.findById(id);
		ArrayList<Integer> rolelist = new ArrayList<Integer>();
		if(announcementObj != null) {
			for (int i = 0; i < announcementObj.getAnnouncementRole().size(); i++) {
				int role_id =  announcementObj.getAnnouncementRole().get(i).getRole().getId();
				rolelist.add(role_id);
			}
		}else{
			return "redirect:../create";
		}
		model.addAttribute("announcementObj", announcementObj);
		model.addAttribute("rolelist", rolelist);
		model.addAttribute("roles", roles);
		model.addAttribute("page_title", "Getinsured Health Exchange: Announcement Edit Page" );
		
		return "admin/announcement/edit";
	}
	
	//@RequestMapping(value="/admin/announcement/edit",method = RequestMethod.POST)
	public String editAnnouncementSubmit(@ModelAttribute("Announcement") Announcement announcementFormObj , Model model, HttpServletRequest request) {
		LOGGER.info("Edit Announcement Page ");
		if(! this.isAllowed(model, request) ){
			return "redirect:/account/user/loginfailed";
		}
		AccountUser user = null;
		try{
           user = userService.getLoggedInUser();
        }catch(InvalidUserException ex){
	      LOGGER.error("User not logged in");
        }

		announcementFormObj.setUpdatedBy(user.getId());
		announcementService.editAnnouncement(announcementFormObj);
		model.addAttribute("page_title", "Getinsured Health Exchange: Announcement Edit Submit Page" );
		
		return "redirect:manage";
	}
	
	
	//@RequestMapping(value="/admin/announcement/manage")
	public String searchAnnouncement (Model model, HttpServletRequest request) {
		LOGGER.info("Search Announcement Pag");
		
		if(! this.isAllowed(model, request) ){
			return "redirect:/account/user/loginfailed";
		}
		List<Role> roles = roleService.findAll();
		Map<String, Object> searchCriteria = this.createSearchCriteria(request);
		Map<String, Object> announcementListAndRecordCount = announcementService.searchAnnouncement(searchCriteria);
		List<Announcement> announcements = (List<Announcement>) announcementListAndRecordCount.get("announcementlist");
		
		Long iResultCt = (Long) announcementListAndRecordCount.get("recordCount");
		int iResultCount = iResultCt.intValue();
		
		model.addAttribute("page_title", "Getinsured Health Exchange: Announcement Listing" );
	
		model.addAttribute("announcementlist", announcements);
		model.addAttribute("searchCriteria", searchCriteria);
		model.addAttribute("resultSize", iResultCount);
		model.addAttribute("pageSize", LISTING_NUMBER);
		model.addAttribute("submit", "submit");
		model.addAttribute("sortOrder", searchCriteria.get("sortOrder"));
		model.addAttribute("roles", roles);
		
		return "admin/announcement/manage";
	}
			
	private boolean isAllowed(Model model, HttpServletRequest request){
		AccountUser user = null;
		HashMap<String, Object> errorMsg = new HashMap<String, Object>(); 
		
		try{
           user = userService.getLoggedInUser();
           if(!userService.isAdmin(user)){
        	   errorMsg.put("message", "User not authorized.");
	       	   model.addAttribute("authfailed", errorMsg.get("message"));
	    	   request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION",errorMsg);
	    	   return false;
           }
        }catch(InvalidUserException ex){
	      LOGGER.error("User not logged in");
	      errorMsg.put("message", "User not logged in.");
		  model.addAttribute("authfailed", errorMsg.get("message"));
		  request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION",errorMsg);
		  return false;
        }
		return true;
	}
	
	private Map<String, Object> createSearchCriteria( HttpServletRequest request ){
		HashMap<String, Object> searchCriteria = new HashMap<String, Object>(); 
		
		String sortBy =  (request.getParameter("sortBy") == null) ?  "id" : request.getParameter("sortBy");
		searchCriteria.put("sortBy", ((sortBy.equals("")) ? "id" : sortBy ));
	
		String sortOrder =  (request.getParameter("sortOrder") == null) ?  "DESC" : request.getParameter("sortOrder");
		sortOrder = (sortOrder.equals("")) ? "DESC" : sortOrder;
		
		String changeOrder =  (request.getParameter("changeOrder") == null) ?  "false" : request.getParameter("changeOrder");
		sortOrder = (changeOrder.equals("true") ) ? ((sortOrder.equals("DESC")) ? "ASC" : "DESC" ): sortOrder;
		request.removeAttribute("changeOrder");
		searchCriteria.put("sortOrder", sortOrder);
		
		int startRecord = (request.getParameter("pageNumber") != null) ? (Integer.parseInt(request.getParameter("pageNumber")) - 1) * LISTING_NUMBER : 0;
		searchCriteria.put("startRecord",startRecord);
		searchCriteria.put("pageSize",LISTING_NUMBER);
		
		String name = (request.getParameter("name") == null) ?  "" : request.getParameter("name");
		String effectiveStartDate = (request.getParameter("effectiveStartDate") == null) ?  "" : request.getParameter("effectiveStartDate");
		String effectiveEndDate = (request.getParameter("effectiveEndDate") == null) ?  "" : request.getParameter("effectiveEndDate");
		String role = (request.getParameter("role") == null) ?  "" : request.getParameter("role");
		String status = (request.getParameter("status") == null) ?  "" : request.getParameter("status");

		if(! name.equals("")){
			searchCriteria.put("name", name);
		}
		if(! effectiveStartDate.equals("")){
			searchCriteria.put("effectiveStartDate", effectiveStartDate);
		}
		if(! effectiveEndDate.equals("")){
			searchCriteria.put("effectiveEndDate", effectiveEndDate);
		}
		if(! role.equals("")){
			searchCriteria.put("role", role);
		}
		if(! status.equals("")){
			searchCriteria.put("status", status);
		}
		return searchCriteria;
	}
	
	/* Announcement CR Coding Start */
	
	@RequestMapping(value="/admin/dashboardAnnouncement",method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'IND_PORTAL_CREATE_ANNOUNCEMENTS')")
	public String manageAnnouncements() {
		return "admin/dashboardAnnouncements";
	}
	
	/**
     * HIX-82138 :: Get list of Announcement.
     * Sorted in DESC order of announcement effective end date
     * If todays date is after effective start date and 
     * before effective end date set the status to active
     * @author sahoo_s
     * @return List<AnnouncementDTO>
     */
	@RequestMapping(value="/announcements",method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'IND_PORTAL_VIEW_ANNOUNCEMENTS')")
	@ResponseBody
	public List<AnnouncementDTO> getAnnouncements() {
		List<AnnouncementDTO> announcementDTOList = null;
		try {
			List<Announcement> announcementsList = new ArrayList<Announcement>();
			announcementsList = announcementService.findAll();
			
			if (null != announcementsList && announcementsList.size() > 0) {
				announcementDTOList = new ArrayList<AnnouncementDTO>();
				for (Announcement announcement : announcementsList) {
					AnnouncementDTO announcementDTO = prepareAnnouncementDTO(announcement);
					announcementDTOList.add(announcementDTO);
				}
				Collections.sort(announcementDTOList, new AnnouncementComparator());
			}
		} catch (Exception e) {
			LOGGER.error("Error occured while fetching all active announcements");
		}
		return announcementDTOList;
	}
	
	@RequestMapping(value="/announcements/",method = RequestMethod.POST)
	@GiAudit(transactionName = "Create announcement for Individual", eventType = EventTypeEnum.ANNOUNCEMENTS, eventName = EventNameEnum.CREATE_ANNOUNCEMENTS)
	@PreAuthorize("hasPermission(#model, 'IND_PORTAL_CREATE_ANNOUNCEMENTS')")
	@ResponseBody
	public String createAnnoncement(@RequestBody AnnouncementDTO announcementDTO,HttpServletRequest httpServletRequest) {
		LOGGER.info("Add New Announcement Submit Page ");

		try {
			if (null != announcementDTO) {
				
				Announcement announcement = new Announcement();
				AccountUser user = userService.getLoggedInUser();
				
				if (StringUtils.isBlank(announcementDTO.getAnnouncementName())) {
					throw new GIException("Announcement Name is mandatory");
				}else if (announcementDTO.getAnnouncementName().length() > 100) {
					throw new GIException("Announcement Name length is more than 100 Characters");
				}
				announcement.setName(announcementDTO.getAnnouncementName().trim());
				
				if (StringUtils.isBlank(announcementDTO.getEffectiveStartDate())) {
					throw new GIException("Effective Start Date is mandatory");
				}else {
					LocalDate todaysDate = TSLocalTime.getLocalDateInstance();
					LocalDate effectiveStartDate = new DateTime(dateFormat.parse(announcementDTO.getEffectiveStartDate())).toLocalDate();
					if (effectiveStartDate.isBefore(todaysDate)) {
						throw new GIException("Effective start date should not be less than todays date");
					}
					announcement.setEffectiveStartDate(effectiveStartDate.toDate());
				}
				
				if (StringUtils.isBlank(announcementDTO.getEffectiveEndDate())) {
					throw new GIException("Effective End Date is mandatory");
				}else {
					LocalDate effectiveStartDate = new DateTime(dateFormat.parse(announcementDTO.getEffectiveStartDate())).toLocalDate();
					LocalDate effectiveEndDate =  new DateTime(dateFormat.parse(announcementDTO.getEffectiveEndDate())).toLocalDate();
					if (effectiveEndDate.isBefore(effectiveStartDate)) {
						throw new GIException("Effective end date should not be less than Effective start date");
					}
					announcement.setEffectiveEndDate(effectiveEndDate.toDate());
				}
				
				if (StringUtils.isBlank(announcementDTO.getAnnouncementText())) {
					throw new GIException("Announcement Text is mandatory");
				}
				announcement.setAnnouncementText(announcementDTO.getAnnouncementText().trim());
				announcement.setModuleName(ModuleName.INDIVIDUAL_PORTAL.getValue());
				announcement.setStatus(Status.ACTIVE);
				announcement.setCreatedBy(user.getId());
				announcementService.save(announcement);
			}
		} catch (GIException | ParseException | InvalidUserException e) {
			LOGGER.error("Error occured while creating an announcement");
			return FAILURE;
		} catch (Exception e) {
			LOGGER.error("Error occured while creating an announcement");
			return FAILURE;
		}
		return SUCCESS;
	}
	
	@RequestMapping(value="/announcement",method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'IND_PORTAL_VIEW_ANNOUNCEMENTS')")
	@ResponseBody
	public AnnouncementDTO getAnnouncement(@RequestParam(value="announcementId", required=true) String announcementId) {
		LOGGER.info("Get Announcement by ID from datadase");
		
		AnnouncementDTO announcementDTO = null;
		try {
			if (StringUtils.isNumeric(announcementId) && Integer.valueOf(announcementId)!=0) {
				Announcement announcement = announcementService.findById(Integer.valueOf(announcementId));
				if (null != announcement) {
					announcementDTO = prepareAnnouncementDTO(announcement);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error occured while fetching active announcement by announcement id");
		}
		return announcementDTO;
	}

	@RequestMapping(value="/announcements/{announcementId}",method = RequestMethod.POST)
	@GiAudit(transactionName = "Update the existing announcement", eventType = EventTypeEnum.ANNOUNCEMENTS, eventName = EventNameEnum.EDIT_ANNOUNCEMENTS)
	@PreAuthorize("hasPermission(#model, 'IND_PORTAL_EDIT_ANNOUNCEMENTS')")
	@ResponseBody
	public String updateAnnoncement(@RequestBody AnnouncementDTO announcementDTO,@PathVariable("announcementId") String announcementId) {
		LOGGER.info("Update the Announcement");

		try {
			if (null != announcementDTO && null != announcementId) {
				Announcement announcementDbObj = announcementService.findById(Integer.valueOf(announcementId));
				AccountUser user = userService.getLoggedInUser();
				
				if (StringUtils.isBlank(announcementDTO.getAnnouncementName())) {
					throw new GIException("Announcement Name is mandatory");
				}else if (announcementDTO.getAnnouncementName().length() > 100) {
					throw new GIException("Announcement Name length is more than 100 Characters");
				}
				announcementDbObj.setName(announcementDTO.getAnnouncementName().trim());
				
				if (StringUtils.isBlank(announcementDTO.getEffectiveStartDate())) {
					throw new GIException("Effective Start Date is mandatory");
				}else {
					Date effectiveStartDate = dateFormat.parse(announcementDTO.getEffectiveStartDate());
					announcementDbObj.setEffectiveStartDate(effectiveStartDate);
				}
				
				if (StringUtils.isBlank(announcementDTO.getEffectiveEndDate())) {
					throw new GIException("Effective End Date is mandatory");
				}else {
					LocalDate effectiveStartDate = new DateTime(dateFormat.parse(announcementDTO.getEffectiveStartDate())).toLocalDate();
					LocalDate effectiveEndDate =  new DateTime(dateFormat.parse(announcementDTO.getEffectiveEndDate())).toLocalDate();
					if (effectiveEndDate.isBefore(effectiveStartDate)) {
						throw new GIException("Effective end date should not be prior to Effective start date");
					}
					announcementDbObj.setEffectiveEndDate(effectiveEndDate.toDate());
				}
				if (StringUtils.isBlank(announcementDTO.getAnnouncementText())) {
					throw new GIException("Announcement Text is mandatory");
				}
				announcementDbObj.setAnnouncementText(announcementDTO.getAnnouncementText());
				announcementDbObj.setStatus(Status.ACTIVE);
				announcementDbObj.setUpdatedBy(user.getId());
				announcementService.save(announcementDbObj);
			}
		} catch (GIException | ParseException | InvalidUserException e) {
			LOGGER.error("Error occured while updating the announcement by announcement id");
			return FAILURE;
		} catch (Exception e) {
			LOGGER.error("Error occured while updating the announcement by announcement id");
			return FAILURE;
		}
		return SUCCESS;
	}
	
	private AnnouncementDTO prepareAnnouncementDTO(Announcement announcement) throws ParseException {
		
		AnnouncementDTO announcementDTO = new AnnouncementDTO();
		announcementDTO.setId(announcement.getId());
		announcementDTO.setAnnouncementName(announcement.getName());
		announcementDTO.setAnnouncementText(announcement.getAnnouncementText());
		announcementDTO.setModuleName(announcement.getModuleName());
		Date currentDate = new TSDate();
		Date effectiveStartDate = announcement.getEffectiveStartDate();
		Date effectiveEndDate = announcement.getEffectiveEndDate();
		announcementDTO.setEffectiveStartDate(dateFormat.format(effectiveStartDate));
		announcementDTO.setEffectiveEndDate(dateFormat.format(effectiveEndDate));
		if (currentDate.after(effectiveStartDate) && currentDate.before(convertToCalenderDate(effectiveEndDate))) {
			announcementDTO.setStatus(ACTIVE);
		}
		announcementDTO.setUpdatedDate(dateFormat.format(announcement.getUpdatedDate()));
		
		return announcementDTO;
	}
	
	private Date convertToCalenderDate(Date effectiveEndDate) throws ParseException {
		Calendar calendar = TSCalendar.getInstance();
		calendar.setTime(effectiveEndDate);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		effectiveEndDate = calendar.getTime();
	    return effectiveEndDate;
	}
	
	/* Announcement CR Coding End */
	
}



