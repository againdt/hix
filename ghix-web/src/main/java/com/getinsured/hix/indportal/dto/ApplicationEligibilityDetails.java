package com.getinsured.hix.indportal.dto;

import java.util.List;

public class ApplicationEligibilityDetails {

	private HouseholdEligibilityDetails householdEligibilityDetails;
	private List<MemberEligibilityDetails> membersEligibilityDetails;

	public HouseholdEligibilityDetails getHouseholdEligibilityDetails() {
		return householdEligibilityDetails;
	}

	public void setHouseholdEligibilityDetails(
			HouseholdEligibilityDetails householdEligibilityDetails) {
		this.householdEligibilityDetails = householdEligibilityDetails;
	}

	public List<MemberEligibilityDetails> getMembersEligibilityDetails() {
		return membersEligibilityDetails;
	}

	public void setMembersEligibilityDetails(
			List<MemberEligibilityDetails> membersEligibilityDetails) {
		this.membersEligibilityDetails = membersEligibilityDetails;
	}

}
