package com.getinsured.hix.planmgmt.util;

import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffDocument.DOC_TYPE;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.ecm.MimeConfig;
import com.getinsured.hix.platform.external.cloud.service.ExternalCloudService;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;
import com.getinsured.timeshift.util.TSDate;
import com.getinsured.timeshift.TSLocalTime;

@Component
@DependsOn("dynamicPropertiesUtil")
public class PlanMgmtUtil {

	private static final Logger LOGGER = Logger.getLogger(PlanMgmtUtil.class);
	@Autowired private ExternalCloudService externalCloudService;
	private static final String URL_REG_EXP_1 = "(((https?:[/][/])((www)))[.]([-]|[a-z]|[A-Z]|[0-9]|[~])+[.]([a-z]|[A-Z]|[0-9]|[~])+([a-z]|[A-Z]|[0-9]|[/.]|[~])*)";
	private static final String URL_REG_EXP_2 = "^http(s)?:\\/\\/(www\\.)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$";
	private static Map<String, String> mimeMap = null;

	static {
		mimeMap = new HashMap<String, String>();
		mimeMap.put("gif", "image/gif");
		mimeMap.put("jpeg", "image/jpeg");
		mimeMap.put("png", "image/png");
		mimeMap.put("jpg", "image/jpeg");	
	}
	
	/**
	 * This method creates a record in SERFF_PLAN_MGMT table for any operation
	 * Only the generic fields are created in this method. Operation specific fields are updated in other method
	 * @author Nikhil Talreja
	 * @since 08 March, 2013
	 * 
	 * @param operation Operation String like validate, update passed by using constants. 
	 * @return SerffPlanMgmt instance.
	 * @throws UnknownHostException
	 */
	public SerffPlanMgmt createSerffPlanMgmtRecord(String operation) throws UnknownHostException {
		return createSerffPlanMgmtRecord(operation, SerffPlanMgmt.REQUEST_STATUS.P, SerffPlanMgmt.REQUEST_STATE.B);
	}
	
	public SerffPlanMgmt createSerffPlanMgmtRecord(String operation, SerffPlanMgmt.REQUEST_STATUS requestStatus,
			SerffPlanMgmt.REQUEST_STATE requestState) throws UnknownHostException {

		LOGGER.debug(" : createSerffPlanMgmtRecord() : Creating record in SERFF_PLAN_MGMT table: operation : " + SecurityUtil.sanitizeForLogging(String.valueOf(operation)));

		SerffPlanMgmt record = new SerffPlanMgmt();
		
		record.setCallerType(SerffPlanMgmt.CALLER_TYPE.O);
		record.setCallerIp(PlanMgmtConstants.LOCALHOST_IP);
		record.setProcessIp("");

		record.setOperation(operation);
		record.setEnv(SerffPlanMgmt.ENV.DEV);
		record.setStartTime(new TSDate());
		record.setRequestStatus(requestStatus);
		
		InetAddress processIp = InetAddress.getLocalHost();
		
		if (null != processIp) {
			LOGGER.debug(" : createSerffPlanMgmtRecord()  : " + SecurityUtil.sanitizeForLogging(String.valueOf(processIp.getHostAddress())));
			record.setCreatedBy(processIp.getHostAddress());
		}

		//If request came from UI, set created by as user's id
		if(record.getCallerType().equals(SerffPlanMgmt.CALLER_TYPE.U)){
			record.setCreatedBy("USER ID");
		}

		record.setRequestState(requestState);
		record.setRequestStateDesc("New request recieved");

		return record;
	}

	/**
	 * This method adds attachment to ECM
	 * @author Nikhil Talreja
	 * @throws ContentManagementServiceException
	 * @since 12 March, 2013
	 * 
	 * @param ecmService 
	 *        Autowired ecmService instance which handles operations related to ECM.
	 *        
	 * @param record
	 *        SerffPlanMgmt instance saved in database. 		  
	 * 
	 * @param basePath
	 * 		  Path where document is to be saved in ECM.
	 * 
	 * @param fileName
	 * 		  Name of the document.
	 * 
	 * @param content
	 * 		  Byte representation of the document.
	 * 
	 * @return SerffDocument that is saved in ECM.
	 * @throws  
	 */
	public SerffDocument addAttachmentInECM(
			ContentManagementService ecmService, SerffPlanMgmt record,
			String basePath, String fileName, byte[] content) throws GIException{

		LOGGER.info("Executing addAttachmentInECM() Starts");
		
		String ecmDocName = null;

		try {
			ecmDocName = ecmService.createContent(basePath, fileName, content
					, ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY, null);
		}
		catch (ContentManagementServiceException e) {
			LOGGER.error(" addAttachmentInECM() : ContentManagementServiceException : ", e);
			throw new GIException(Integer.parseInt(PlanMgmtConstants.UNEXPECTED_GENERAL_ERROR_CODE), PlanMgmtConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE_EMSG_ECM_RESPONSE + " : " + e.getMessage(), "");
		}
		catch (RuntimeException exception) {
			LOGGER.error(" addAttachmentInECM() : RuntimeException : ",exception);
			LOGGER.error(PlanMgmtConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE_EMSG_ECM_RESPONSE);
			throw new GIException(Integer.parseInt(PlanMgmtConstants.UNEXPECTED_GENERAL_ERROR_CODE), PlanMgmtConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE + " : "+exception.getMessage(), "");
		}

		if (StringUtils.isNotEmpty(ecmDocName)) {
			SerffDocument attachment = new SerffDocument();
			attachment.setSerffReqId(record);
			attachment.setEcmDocId(ecmDocName);
			attachment.setDocName(fileName);
			attachment.setDocType(getFileExtension(fileName));
			attachment.setDocSize(content.length);
			return attachment;
		}
		LOGGER.info("Executing addAttachmentInECM() End");
		return null;
	}
	
	
	/**
	 * This method returns the file extension for a file
	 * @author Nikhil Talreja
	 * @since Apr 8, 2013
	 * 
	 * @param fileName
	 *        Name of file whose extension is to be extracted.
	 * 
	 * @return File extension. 		  
	 * 
	 */
	public  DOC_TYPE getFileExtension(String fileName){

		LOGGER.debug(" : Executing getFileExtension() : filename : " + SecurityUtil.sanitizeForLogging(String.valueOf(fileName)));
		
		if(StringUtils.isNotEmpty(fileName)){
			int indexOfDot = fileName.lastIndexOf('.');
			if(indexOfDot != -1){
				String extension = fileName.substring(indexOfDot+1);
				LOGGER.debug(" : getFileExtension() : extension " + SecurityUtil.sanitizeForLogging(String.valueOf(extension)));	
				return DOC_TYPE.valueOf(extension);
			}

		}
		return null;
	}
	
	public boolean isValidPath(String filePath){
		boolean isValidPath = false;
		try{
			if(null != filePath){
				if(GhixUtils.isGhixValidPath(filePath)){
					isValidPath = true;
				}else{
					isValidPath = false;
				}
			}
		}catch(Exception ex){
			isValidPath = false;
			LOGGER.error("exception occure in isValidPath: ", ex);
		}
		return isValidPath;
	}

	/**
	 * @param logoURL
	 * @param hiosIssuerId
	 * @param appURL
	 * @return
	 */
	public static String getIssuerLogoURLByHIOSID(String logoURL, String hiosIssuerId, String appURL) {
		String issuerLogoURL = null;
		if(org.apache.commons.lang3.StringUtils.isNotBlank(logoURL)) {
			issuerLogoURL = logoURL;
		} else {
			issuerLogoURL = appURL + PlanMgmtConstants.LOGO_BY_HIOSID_URL_PATH + hiosIssuerId;
		}
		return issuerLogoURL;
	}

	/**
	 * @param logoURL
	 * @param encIssuerId
	 * @param appUrl
	 * @return
	 */
	public static String getIssuerLogoURLForAdminByHIOSID(String logoURL, String hiosIssuerId, String appUrl) {
		String issuerLogoURL = null;
		if(org.apache.commons.lang3.StringUtils.isNotBlank(logoURL)) {
			issuerLogoURL = logoURL;
		} else {
			issuerLogoURL = appUrl + PlanMgmtConstants.ADMIN_LOGO_BY_HIOSID_URL_PATH + hiosIssuerId;
		}
		return issuerLogoURL;
	}
	
	/**
	 * Method is used upload Company LOGO at CDN server and set LOGO Contents(BLOG) and CDN URL in Issuer table.
	 */
	public String uploadLogoAndSetDetails(MultipartFile companyLogo, Issuer issuerObj) {

		LOGGER.info("Execution of uploadLogoAndSetDetails() start");
		Long companyLogoSize = 0L;
		String errorMsg = null;
		
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();

		try {

			boolean hasInvalidRequest = false;
			
			if (null == companyLogo || companyLogo.isEmpty()) {
				hasInvalidRequest = true;
				LOGGER.info("Company logo is not selected.");
				
			}else {
				String fileExtension = getFileExtension(companyLogo.getOriginalFilename().toLowerCase()).name();
	
				//If actual Mime type and the file extension does not match 
				//	OR 
				//	actual mime type is not in allowed mime type list(Not checked for CA)
				if(!mimeMap.containsKey(fileExtension)) {
					LOGGER.error("Invalid Comapny Logo file type: "+fileExtension);
					errorMsg = "Invalid Logo file type, Allowed logo file types : " + mimeMap.keySet();
					LOGGER.error(errorMsg);
					hasInvalidRequest =  true;	
				}else if (!stateCode.equalsIgnoreCase("CA")) {
					String actualMimetype = MimeConfig.getMimeConfig().validateIncomingContent(companyLogo.getBytes(), companyLogo.getOriginalFilename());
					if(!actualMimetype.equals(mimeMap.get(fileExtension))) {
						LOGGER.error("Invalid Comapny Logo file type: "+actualMimetype);
						errorMsg = "Invalid Logo file type, Allowed logo file types : " + mimeMap.keySet();
						LOGGER.error(errorMsg);
						hasInvalidRequest = true;
					}
				}

				if (null == issuerObj) {
					hasInvalidRequest = true;
					LOGGER.error("Issuer Object is empty.");
				}

				if (!hasInvalidRequest) {
					String companyLogoStr = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.COMPANYLOGOFILESIZE);
					if (StringUtils.isNotBlank(companyLogoStr)) {
						companyLogoSize = Long.parseLong(companyLogoStr);
					}
					else {
						companyLogoSize = PlanMgmtConstants.COMPANY_LOGO_FILE_SIZE;
					}

					if (companyLogo.getSize() < companyLogoSize) {
						if (StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue("global.CdnUrl"))) {
							URL docUrl = externalCloudService.upload(PlanMgmtConstants.CDN_ISSUER_LOGO_PATH, issuerObj.getId() + "_"+ issuerObj.getHiosIssuerId() + companyLogo.getOriginalFilename(), companyLogo.getBytes());

							if (null != docUrl) {
								issuerObj.setLogoURL(docUrl.toString());
								if (LOGGER.isDebugEnabled()) {
									LOGGER.debug("CDN Logo URL: " + docUrl.toString());
								}
							}else {
								LOGGER.warn("Failed to upload Company Logo to CDN.");
							}
						}else {
							LOGGER.debug("Skipping to upload logo to CDN as CDN is not enable.");
						}
						issuerObj.setLogo(companyLogo.getBytes());
					}else {
						errorMsg = "Company logo size("+ companyLogo.getSize() +") is exceeding maximum size " + companyLogoSize;
						LOGGER.error(errorMsg);
					}
					
				}
			}
			LOGGER.info("Execution of uploadLogoAndSetDetails() end");
			
			if(errorMsg!=null) {
				return errorMsg;
			}
			return PlanMgmtConstants.SUCCESS;
		}catch (Exception ex) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.SAVE_COMPANY_PROFILE_EXCEPTION.getCode(), null, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH);
		}
	}
	
	public boolean isValidUrl(String urlString){
		boolean isValidUrl = false;
		try{
			if(StringUtils.isNotEmpty(urlString)){
				if(urlString.startsWith("http://www") || urlString.startsWith("https://www")){
					if(urlString.trim().matches(URL_REG_EXP_1)){
						isValidUrl = true;
					}
				}else{
					if(urlString.trim().matches(URL_REG_EXP_2)){
						isValidUrl = true;
					}
				}
			}else{
				isValidUrl = true;
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in isValidUrl : ", ex);
		}
		return isValidUrl;
	}

	/**
	 * 
	 */
	public String getContentType(byte[] data) {
	
		String contentType = "application/octet-stream";

		if (data == null) {
			return contentType;
		}

		byte[] header = new byte[11];
		System.arraycopy(data, 0, header, 0, Math.min(data.length, header.length));
		int c1 = header[0] & 0xff;
		int c2 = header[1] & 0xff;
		int c3 = header[2] & 0xff;
		int c4 = header[3] & 0xff;
		int c5 = header[4] & 0xff;
		int c6 = header[5] & 0xff;
		int c7 = header[6] & 0xff;
		int c8 = header[7] & 0xff;
		int c9 = header[8] & 0xff;
		int c10 = header[9] & 0xff;
		int c11 = header[10] & 0xff;

		if (c1 == 'B' && c2 == 'M') {
			contentType = "image/bmp";
		}
		else if (c1 == 0x49 && c2 == 0x49 && c3 == 0x2a && c4 == 0x00) {
			contentType = "image/tiff";
		}
		else if (c1 == 0x4D && c2 == 0x4D && c3 == 0x00 && c4 == 0x2a) {
			contentType = "image/tiff";
		}
		else if (c1 == 'G' && c2 == 'I' && c3 == 'F' && c4 == '8') {
			contentType = "image/gif";
		}
		else if (c1 == 137 && c2 == 80 && c3 == 78 && c4 == 71 && c5 == 13 && c6 == 10 && c7 == 26 && c8 == 10) {
			contentType = "image/png";
		}
		else if (c1 == 0xFF && c2 == 0xD8 && c3 == 0xFF) {

			if (c4 == 0xE0) {
				contentType = "image/jpeg";
			}
			/**
			 * File format used by digital cameras to store images. Exif Format can be read by any application supporting JPEG. Exif Spec can be found at:
			 * http://www.pima.net/standards/it10/PIMA15740/Exif_2-1.PDF
			 */
			else if ((c4 == 0xE1) && (c7 == 'E' && c8 == 'x' && c9 == 'i' && c10 == 'f' && c11 == 0)) {
				contentType = "image/jpeg";
			}
			else if (c4 == 0xEE) {
				contentType = "image/jpg";
			}
		}
		return contentType;
	}
	
	public Date getEnrollmentEndDate(){
		Date monthEndDate = null;
		try{
			LocalDate monthEnd = TSLocalTime.getLocalDateInstance().plusMonths(1).withDayOfMonth(1).minusDays(1);
			monthEndDate = monthEnd.toDate();
		}catch(Exception ex){
			LOGGER.error("Exception occured in getEnrollmentEndDate : ", ex);
		}
		return monthEndDate;
	}
	
	public Date getDeCertificationDate(Date enrollmentEndDate){
		Date monthStartDate = null;
		try{
			LocalDate startDate = new LocalDate(enrollmentEndDate).plusDays(1);
			monthStartDate = startDate.toDate();
		}catch(Exception ex){
			LOGGER.error("Exception occured in getDeCertificationDate : ", ex);
		}
		return monthStartDate;
	}
	
	public boolean checkIsPlanDecertified(String planStatus, Date decertificationEffectiveDate){
		if(Plan.PlanStatus.DECERTIFIED.toString().equalsIgnoreCase(planStatus) || null != decertificationEffectiveDate){
			return true;
		}
		return false;
	}

	public int getBaseYear(){
		int baseYear = 2013;

		String exchangeStartDateString = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_START_YEAR).trim();

		//Configuration Exists
		if(StringUtils.isNumeric(exchangeStartDateString)) {
			baseYear = Integer.parseInt(exchangeStartDateString);
		}

		return baseYear;
	}
}
