/**
 * 
 */
package com.getinsured.hix.indportal.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author vishwanath_sr
 * Response class of Enrollment for Location updation from Preferences
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PreferenceEnrollResponse {

	private String isEnrollmentExists; 
	private String status;
	private String errMsg; 
	private Integer errCode; 
	private Integer execDuration; 
	private Integer startTime; 
	private boolean terminationFlag; 
	private boolean dentalTerminationFlag;
	
	/**
	 * @return the isEnrollmentExists
	 */
	public String getIsEnrollmentExists() {
		return isEnrollmentExists;
	}
	/**
	 * @param isEnrollmentExists the isEnrollmentExists to set
	 */
	public void setIsEnrollmentExists(String isEnrollmentExists) {
		this.isEnrollmentExists = isEnrollmentExists;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the errMsg
	 */
	public String getErrMsg() {
		return errMsg;
	}
	/**
	 * @param errMsg the errMsg to set
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	/**
	 * @return the errCode
	 */
	public Integer getErrCode() {
		return errCode;
	}
	/**
	 * @param errCode the errCode to set
	 */
	public void setErrCode(Integer errCode) {
		this.errCode = errCode;
	}
	/**
	 * @return the execDuration
	 */
	public Integer getExecDuration() {
		return execDuration;
	}
	/**
	 * @param execDuration the execDuration to set
	 */
	public void setExecDuration(Integer execDuration) {
		this.execDuration = execDuration;
	}
	/**
	 * @return the startTime
	 */
	public Integer getStartTime() {
		return startTime;
	}
	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Integer startTime) {
		this.startTime = startTime;
	}
	/**
	 * @return the terminationFlag
	 */
	public boolean isTerminationFlag() {
		return terminationFlag;
	}
	/**
	 * @param terminationFlag the terminationFlag to set
	 */
	public void setTerminationFlag(boolean terminationFlag) {
		this.terminationFlag = terminationFlag;
	}
	/**
	 * @return the dentalTerminationFlag
	 */
	public boolean isDentalTerminationFlag() {
		return dentalTerminationFlag;
	}
	/**
	 * @param dentalTerminationFlag the dentalTerminationFlag to set
	 */
	public void setDentalTerminationFlag(boolean dentalTerminationFlag) {
		this.dentalTerminationFlag = dentalTerminationFlag;
	} 
}
