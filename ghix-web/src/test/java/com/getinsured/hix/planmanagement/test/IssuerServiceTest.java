package com.getinsured.hix.planmanagement.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.planmgmt.repository.IIssuerRepository;
import com.getinsured.hix.planmgmt.service.IssuerService;

public class IssuerServiceTest extends BaseTest {
	@SuppressWarnings("unused")
	@Autowired private IssuerService issuerService;
	@Autowired private IIssuerRepository iIssuerRepository;
	
	@Test
	public void getIssuerByIdTest(){
		Integer issuerId=2;
		System.out.println("Using issuerid: " + issuerId);
		Issuer issuer=iIssuerRepository.findOne(issuerId);
		System.out.println("Issuer Information from Testcase: " + issuer.getName());
		System.out.println("Issuer Information from Testcase: " + issuer.getEmailAddress());
		assertNotNull(issuer);
	}
	
	@Test
	public void getAllIssuer(){
		List<Object[]> issuerNamesList = iIssuerRepository.getAllIssuerNames();
		List<String> issuerNames = new ArrayList<String>();
		
		if(!(issuerNamesList.isEmpty())) {
			for(int i=0; i<issuerNamesList.size(); i++) {
				issuerNames.add(issuerNamesList.get(i)[1].toString());
			}
		}
		
		for(int i=0;i<issuerNames.size();i++){
			System.out.println("Issuer Name: " + issuerNames.get(i));
		}

	}

}
