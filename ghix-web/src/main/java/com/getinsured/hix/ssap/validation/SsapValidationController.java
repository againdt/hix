package com.getinsured.hix.ssap.validation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.dto.QleRequestDto;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Controller
@RequestMapping("/ssap")
public class SsapValidationController {

	private static final String CASE_NUMBER = "Case Number";
	private static final String EVENT_NAME = "Event Name";
	private static final Logger LOGGER = LoggerFactory.getLogger(SsapValidationController.class);
	private static final String UPLOAD_FAILURE = "Upload_Failure";
	private static final String FILE_COULD_NOT_BE_UPLOADED = "File could not be uploaded ";
	
	
	private static final String APPLICANT_EVENT_ID  = "Applicant Event ID";
	private static final String APPLICATION_TYPE  = "Application Type";
	private static final String NON_FINANCIAL_APPLICATION  = "Non Financial Application";
	private static final String FINANCIAL_APPLICATION  = "Financial Application";
	private static final String APP_EVENT_QLE_VALIDATION  = "APPEVENT_QLE_VALIDATION";
	private static final String APP_EVENT_UPLOAD_DOCUMENT  = "UPLOAD_DOCUMENT";
	private static final String APP_EVENT_CSR_OVERRIDE   = "CSR_OVERRIDE";
	private static final String DEFAULT_QLE_DOCUMENT_SUBJECT = "Ticket submission for Qle Document Verification";
	
	@Autowired private AppEventService appEventService;
	@Autowired private LookupService lookupService;
	@Autowired private SsapApplicantEventRepository ssapApplicantEventRepository;
	@Autowired
	GhixRestTemplate ghixRestTemplate;

	@Autowired
	UserService userService;
	@Autowired private Gson platformGson;

	@Autowired
	private ConsumerPortalUtil consumerUtil;

	@RequestMapping(value={"/applicant/events/{caseNumber}/{gated}","/applicant/events/{caseNumber}"},produces=MediaType.APPLICATION_JSON_VALUE,method = RequestMethod.GET)
	@ResponseBody
	public String getEventDataForApplication(@PathVariable("caseNumber") String caseNumber,@PathVariable("gated") Optional<String> gated,HttpServletResponse response) {
		AccountUser accountUser;
		String result = "500";
		String eventType = null;
		try {
			if(gated.isPresent()){
				if(!getValidGatedValues().contains(gated.get())){
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					return null;
				}
				eventType = gated.get();
			}else{
				eventType = "y";
			}
			accountUser = userService.getLoggedInUser();
			result  = ghixRestTemplate.getForObject(GhixEndPoints.ELIGIBILITY_URL + "ssap/retrieveEventsByApplication/"+accountUser.getActiveModuleId()+"/"+caseNumber+"/"+eventType,String.class);
		} catch (Exception ex) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			LOGGER.error("Exception occured while fetching event details: "+caseNumber,ex);
		}
		return result; 
	}


	private Set<String> getValidGatedValues() {
		Set<String> validGatedValues = new HashSet<String>();
		validGatedValues.add("y");
		validGatedValues.add("n");
		validGatedValues.add("all");
		return validGatedValues;
	}
	

	@RequestMapping(value="/admin/qle/override",produces=MediaType.APPLICATION_JSON_VALUE )
	@ResponseBody
	@GiAudit(transactionName="Admin QLE validation override", eventType = EventTypeEnum.PII_WRITE,eventName = EventNameEnum.NON_FINANCIAL_APP)
	@PreAuthorize("hasPermission(#model, 'IND_PORTAL_CSR_OVERRIDES')")
	public String applicantEventOverride(@RequestParam("applicantEventId") Long applicantEventId,@RequestParam("overrideComments") String overrideComment, @RequestParam (required=false) String caseNumber,
			HttpServletResponse response) {
		String result = "500";
		QleRequestDto qleRequestDto = null;
		AccountUser accountUser = null;
		try {
			qleRequestDto = new QleRequestDto();
			accountUser = userService.getLoggedInUser();
			qleRequestDto.setApplicantEventId(applicantEventId);
			qleRequestDto.setOverrideComment(overrideComment);
			qleRequestDto.setUserId((long)accountUser.getId());
			qleRequestDto.setUserName(accountUser.getUsername());
			qleRequestDto.setName(accountUser.getFirstName()+" "+accountUser.getLastName());
			result  = ghixRestTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL + "ssap/adminQleOverride",qleRequestDto,String.class);
			
			if(null != result && "200".equals(result.trim())){
				logQLEAppEvent(APP_EVENT_QLE_VALIDATION, APP_EVENT_CSR_OVERRIDE, String.valueOf(applicantEventId) , accountUser.getActiveModuleId() , accountUser.getActiveModuleName(),overrideComment);
				if(StringUtils.isNotBlank(caseNumber)){
					try {
						ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"nonfinancial/eligibility/updateEligibilityStatus",
								accountUser.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, caseNumber);
			         }catch(Exception e) {
			             LOGGER.error("Exception while updating EligibilityStatus", e);
			         } 
				}				
			}
			
		} catch (Exception ex) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			LOGGER.error("Exception occured while overriding qle validation for applicantEventId : "+applicantEventId,ex);
		}
		//set internal server error when status is 500
		if(result.equals("500")){
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return result;
		
	}
	
	@RequestMapping(value="/applicant/event/uploadDocument",method = RequestMethod.POST)
	@ResponseBody
	public String saveEventDocuments(@RequestParam Long applicantEventId,@RequestParam String caseNumber, @RequestParam("eventLabel") String eventLabel, @RequestParam String documentType,@RequestParam("document") MultipartFile file,HttpServletRequest request){

		AccountUser user ;
		String errorMsg = "";
		try{
			user = userService.getLoggedInUser();
			String relativePath =  user.getId()+"/ssap/applicant/events/documents";
			String originalFileName = file.getOriginalFilename(); 
			if(StringUtils.isNotEmpty(originalFileName)){
				
				QleRequestDto restRequest = new QleRequestDto();
				
				restRequest.setApplicantEventId(applicantEventId);
				restRequest.setCaseNumber(caseNumber);
				restRequest.setFileCategory("Document Verification");
				restRequest.setFileContents(file.getBytes());
				restRequest.setFileName(originalFileName);
				restRequest.setFileSubCategory("SSAP:SubCategory");
				restRequest.setFileType(documentType);//This is drop down selected value.
				restRequest.setModuleName(user.getActiveModuleName());
				restRequest.setRelativePath(relativePath);
				restRequest.setTableName("SSAP_APPLICANT_EVENTS");
				restRequest.setTicketCategory("QLE");
				restRequest.setTicketType("Qle Validation");
				
				String ticketSubject = DEFAULT_QLE_DOCUMENT_SUBJECT;
				Household household = consumerUtil.getHouseholdRecord(user.getActiveModuleId());
				if(household != null && StringUtils.isNotBlank(eventLabel) && StringUtils.isNotBlank(household.getFirstName()) &&
					StringUtils.isNotBlank(household.getLastName())) {
					ticketSubject = "Verify " + eventLabel + " of " + household.getFirstName() + " " + household.getLastName();
				}
				restRequest.setTicketSubject(ticketSubject);
				restRequest.setTicketComment("Ticket submission for Qle Document Verification");
				restRequest.setUserId((long) user.getId());
				restRequest.setUserName(user.getUsername());
				restRequest.setModuleId(user.getActiveModuleId());
				restRequest.setRoleId(user.getDefRole().getId());
				
				String returnedResult = ghixRestTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL + "ssap/saveDocumentForApplicantEvent", restRequest, String.class);
				
				if(returnedResult != null){
					Map<String, Object> responseMap= platformGson.fromJson(returnedResult, new TypeToken<Map<String, Object>>() {}.getType());
					String status = String.valueOf(responseMap.get("status"));
					String error = String.valueOf(responseMap.get("error"));
					if(status.equals("200")){
						logQLEAppEvent(APP_EVENT_QLE_VALIDATION, APP_EVENT_UPLOAD_DOCUMENT, String.valueOf(applicantEventId) , restRequest.getModuleId(), restRequest.getModuleName(),null);
						returnedResult = String.valueOf(responseMap.get("documentList"));
					}else{
						returnedResult = FILE_COULD_NOT_BE_UPLOADED +". "+ error;
					}
				}else{
					returnedResult = FILE_COULD_NOT_BE_UPLOADED;
				}
				
				return returnedResult;
			}else{
				errorMsg = FILE_COULD_NOT_BE_UPLOADED+" - File name cannot be null";
			}
			
			
		} catch(GIRuntimeException e) {
			LOGGER.error("ERROR IN SSAP APPLICANT DOCUMENT UPLOAD:",e);
			errorMsg = FILE_COULD_NOT_BE_UPLOADED+e.getMessage();
		} catch(Exception e) {
			LOGGER.error("ERROR IN SSAP APPLICANT DOCUMENT UPLOAD:",e);
			errorMsg = FILE_COULD_NOT_BE_UPLOADED+e.getMessage();
		}
		
		return errorMsg;
	}
	
	 
	
	@RequestMapping(value="/event/documents/{eventId}",produces=MediaType.APPLICATION_JSON_VALUE )
	@ResponseBody
	public String getDocumentListForEvent(@PathVariable("eventId") String eventId,HttpServletResponse response) {
		String result = "500";
		try {
			result  = ghixRestTemplate.getForObject(GhixEndPoints.ELIGIBILITY_URL + "ssap/retrieveDocumentListForEvent/"+eventId,String.class);
		} catch (Exception ex) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			LOGGER.error("Exception occured while fetching event details: "+eventId,ex);
		}
		return result; 
	}
	
	private void logQLEAppEvent(String eventName,String eventType,String applicantEventId, Integer moduleId, String moduleName, String comment){
		LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(eventName, eventType);
		
		EventInfoDto eventInfoDto = new EventInfoDto();
		eventInfoDto.setModuleId(moduleId);//user.getActiveModuleId()
		eventInfoDto.setModuleName("INDIVIDUAL");//user.getActiveModuleName().toUpperCase()
		eventInfoDto.setEventLookupValue(lookupValue);//lookupValue
		if(null != comment){
			eventInfoDto.setComments(comment);
		}
		
		Map<String, String> mapEventParam = new HashMap<>();
		
		SsapApplicantEvent spE = ssapApplicantEventRepository.findOne(Long.parseLong(applicantEventId));
		
		String finAsistence = spE.getSsapAplicationEvent().getSsapApplication().getFinancialAssistanceFlag() == null || "N".equalsIgnoreCase(  spE.getSsapAplicationEvent().getSsapApplication().getFinancialAssistanceFlag()   )? NON_FINANCIAL_APPLICATION : FINANCIAL_APPLICATION ;
		finAsistence = spE.getSsapAplicationEvent().getSsapApplication().getApplicationType() + " ( "+ finAsistence + " )"; 
		
		mapEventParam.put(CASE_NUMBER ,spE.getSsapAplicationEvent().getSsapApplication().getCaseNumber() );
		mapEventParam.put(APPLICATION_TYPE, finAsistence );
		mapEventParam.put(EVENT_NAME,spE.getSepEvents().getLabel() );
		
		appEventService.record(eventInfoDto, mapEventParam);
	}

}
