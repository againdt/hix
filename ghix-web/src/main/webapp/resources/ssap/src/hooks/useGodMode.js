import { useState, useEffect } from 'react';
import CONFIG from '../.env';

export const useGodMode = () => {

    const path = window.location.origin;

    const [isHyperNavigatorShown, switchHyperNavigation] = useState(CONFIG.ENV.DEVELOPMENT);
    const [ isHyperNavigationAvailable, switchHyperNavigationAvailability ] = useState(false);


    const codes = [
        {
            code: 16,
            type: 'isShift'
        },
        {
            code: 17,
            type: 'isCtrl'
        },
        {
            code: 81,
            type: 'isQ'
        },
    ];

    const [ keys, setKeys ] = useState(Object.assign(
        {},
        ...codes.map(code => ({
            [code.type]: false
        }))
    ));

    const eligiblePaths = [
        'eng.vimo.com',
        'ghixqa.com',
        'localhost:8888',
        'localhost:8080'
    ];

    useEffect(() => {

        if(CONFIG.ENV.DEVELOPMENT) {
            !isHyperNavigationAvailable && switchHyperNavigationAvailability(!isHyperNavigationAvailable);
            return undefined;
        }
        if(path && eligiblePaths.some(p => path.includes(p))) {
            !isHyperNavigationAvailable && switchHyperNavigationAvailability(!isHyperNavigationAvailable);
        }
    }, [ ] );

    useEffect(() => {
        document.onkeydown = keyDownEvent;
        document.onkeyup = keyUpEvent;

        const updateKeys = (type, value) => {
            setKeys(keys => ({
                ...keys,
                [type]: value
            }));
        };

        function keyDownEvent() {
            const isNeededCode = codes.find(code => code.code === event.keyCode);
            if(isNeededCode) {
                updateKeys(isNeededCode.type, true);
            }
        }

        function keyUpEvent() {
            const isNeededCode = codes.find(code => code.code === event.keyCode);
            if(isNeededCode) {
                updateKeys(isNeededCode.type, false);
            }

            if(Object.values(keys).every(key => key)) {
                switchHyperNavigation(!isHyperNavigatorShown);
            }
        }
    });

    return isHyperNavigationAvailable && isHyperNavigatorShown;
};