import React from 'react';
import ButtonContainer from '../../containers/ButtonContainer';

// Components

/**
 * This Component creates container for all bottom-page buttons (usually: 'Back', 'Save' and 'Continue')
 */
const Controls = () =>
    (<div className="usa-grid controls-container">
        <div className="usa-width-one-whole controls">
            <div className="usa-width-one-half">
                <ButtonContainer type="leftButton"/>
            </div>
            <div className="usa-width-two-third buttons-group">
                <ButtonContainer type="middleButton"/>
                <ButtonContainer type="rightButton"/>
            </div>
        </div>
    </div>);

export default Controls;
