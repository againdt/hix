(function (models) {
	models.Cart = Backbone.Model.extend({
		
		urlRoot : "individualCartItems",
		
		url : function () {
			if(this.id)
				return 'individualCartItems/'+this.id+'?'+(Math.random()*10);
			else
				return 'individualCartItems?'+(Math.random()*10);
		},

		//to specify default attributes for model
		defaults: {
			//attrName : default value
			"id"		: "",
			"aptc" 			: ""
		},
		sync : function(method, model, options){
			options.headers = options.headers || {};
		    _.extend(options.headers, { 'csrftoken': $('#csrfToken').val() });
		    return Backbone.sync(method, model, options);
		}
	});
})(App.models);