package com.getinsured.hix.agency.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.getinsured.hix.dto.agency.AgencyBookOfBusinessDTO;
import com.getinsured.hix.dto.agency.AgencyBrokerListDTO;
import com.getinsured.hix.dto.agency.AgencyCertificationStatusDto;
import com.getinsured.hix.dto.agency.AgencyDelegationListDTO;
import com.getinsured.hix.dto.agency.AgencyInformationDto;
import com.getinsured.hix.dto.agency.SearchAgencyBookOfBusinessDTO;
import com.getinsured.hix.dto.agency.SearchAgencyDelegationDTO;
import com.getinsured.hix.dto.agency.SearchBrokerDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.agency.Agency;

public interface AgencyService {

	public Agency saveOrUpdate(Agency agency);
	
	public Agency saveAgencyInformation(Agency agency);
	
	public Agency findById(Long agencyId);
	
	public Agency findByBrokerId(int agentId);
	
	public Page<Agency> findAll(Pageable pageable); 
	
	public void createModuleUser(Broker broker);
	
	public void saveAgencyAgentCertificationStatus(Agency agency);

	public AgencyCertificationStatusDto loadAgencyCertificationHistory(Long agencyId);

	Comment saveComments(String targetId, String commentText);

	Agency updateAgencyCertificationStatus(Agency agency);
	AgencyDelegationListDTO findAllDelegations(SearchAgencyDelegationDTO searchAgencyDelegationDTO) ;
	String approveRequest(int brokerId, int individualId, AccountUser user);

	AgencyBookOfBusinessDTO findAllIndividuals(SearchAgencyBookOfBusinessDTO searchAgencyBookOfBusinessDTO);

	AgencyBrokerListDTO searchAgents(SearchBrokerDTO searchBrokerDTO);

	int findNoOfEnrollees(long enrollmentId);
	
	Agency findByAgencyAssistantId(long agencyAssistantId);

	AgencyInformationDto loadAgencyManagers(AgencyInformationDto agencyInformationDto, int agencyId);
}
