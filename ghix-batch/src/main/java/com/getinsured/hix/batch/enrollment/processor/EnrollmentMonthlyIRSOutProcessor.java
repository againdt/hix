package com.getinsured.hix.batch.enrollment.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;


@Component("enrollmentMonthlyIRSOutProcessor")
public class EnrollmentMonthlyIRSOutProcessor  implements ItemProcessor< String, String>{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyIRSOutProcessor.class);

	@Override
	public String process(String householdCaseId) throws Exception {
		return householdCaseId;
	}
}
