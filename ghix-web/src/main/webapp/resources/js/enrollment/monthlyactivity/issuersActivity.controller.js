(function() {
    'use strict';
    angular
        .module('reconciliationWorkbenchApp')
        .controller('issuersActivityCtrl', ['$scope', '$http', '$timeout',  'workbenchService', 'NotifyingService', 'issuerNotifyingService', function ($scope, $http, $timeout, workbenchService, NotifyingService, issuerNotifyingService) {
            $scope.issuers = [];

            $scope.gridOptions1 = {
                enableSorting: false,
                enableColumnMenus: false,
//                minRowsToShow: 15,
//                rowHeight: 30,
                columnDefs: [
                    { field: 'issuerName', displayName: 'Issuer'.toUpperCase(), width: "195", cellTemplate: "<a href=\"\" ng-click=\"grid.appScope.getMonthlyReconData(row.entity)\"> {{ row.entity.issuerName}} </a>" },
                    { field: 'totalNumberOfFiles', displayName: 'No. of Files'.toUpperCase(), width: "75" }, //totalNumberOfFiles
                    { field: 'lastReconDate', displayName: 'Last Received'.toUpperCase() }, //lastReconDate
                    { field: 'totalEnrlCntInFile', displayName: 'In File'.toUpperCase(), width: "75"}, //totalEnrlCntInFile
                    { field: 'totalEnrlInHix', displayName: 'In HIX'.toUpperCase()}, //totalEnrlInHix
                    { field: 'enrlSuccessInHixCount', displayName: 'Fully Matched'.toUpperCase() }, //enrlSuccessInHixCount
                    { field: 'totalEnrlDiscrepancyCount', displayName: 'Discrepancies'.toUpperCase(), width: "110" }, //totalEnrlDiscrepancyCount
                    { field: 'enrollmentFailedCount', displayName: 'Errors'.toUpperCase() }, //enrollmentFailedCount
                    // { field: '', displayName: 'Policies Missing in File' }, // Kuldeep to add new field
                    { field: 'enrlMissingInFileCount', displayName: 'Missing in File'.toUpperCase() }, //enrlMissinginHixCount
                    { field: 'totalDiscrepancyCount', displayName: 'Total Discrepancy'.toUpperCase() } //totalDiscrepancyCount
                ]
            };

            $scope.getMonthlyReconData = function (issuerData) {
                issuerNotifyingService.notify(issuerData);
            };
            
            $scope.getTableHeight = function() {
                var headerHeight = 30; // your header height
                return {
                   height: (($scope.gridOptions1.data.length *  $scope.gridOptions1.rowHeight)+ headerHeight) + "px"
                };
             };

            NotifyingService.subscribe($scope, function issuersActivity() {
                $scope.issuers = workbenchService.getIssuersActivity();
                $scope.gridOptions1.data = [];
                if($scope.issuers) {
                    // $timeout(function () {
                    $timeout(function() {
                        angular.forEach( $scope.issuers, function( row ) {
                            $scope.gridOptions1.data.push( row );
                        });
                    }, 0);
                }
            });
        }]);
})();