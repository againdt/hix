﻿define(['button'], function (button) {
    var submitreset = new baseComponent();
    submitreset.type = "submitreset";
    submitreset.getHTML = function (cgfObj) {
        var defCfg = {
        };
        cgfObj = $.extend(defCfg, cgfObj);
        var compData = cgfObj;
        var vm = this;
        vm.context.registerComponent(button);

        var vm = this;
        var $retobj = $("<div class=\"control-group\"></div>");
        var $controls = $("<div class=\"controls\"></div>");
        $retobj.append($controls);

        var submitButtonComp = {
            caption: "Submit",
            name: "btnSubmit",
            css: "btn-primary",
            etype:"submit"
        };
        var resetButtonComp = {
            caption: "Reset",
            name: "btnReset",
            etype: "reset"
        };

        if (compData.submit) {
            submitButtonComp["click"] = compData.submit;
            submitButtonComp.etype = "button";
        };
        if (compData.reset) {
            resetButtonComp["click"] = compData.reset;
            resetButtonComp.etype = "button";
        };

        var btnSubmit = vm.context.comp["button"].getHTML(submitButtonComp);
        var $btnSubmit = $(btnSubmit);

        var btnReset = vm.context.comp["button"].getHTML(resetButtonComp);
        var $btnReset = $(btnReset);
        $btnReset.addClass("margin10-l");

        $controls.append($btnSubmit);
        $controls.append($btnReset);

        return $controls[0].outerHTML;
    };
    return submitreset;
});