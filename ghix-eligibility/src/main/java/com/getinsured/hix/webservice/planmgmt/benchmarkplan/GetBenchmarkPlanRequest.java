
package com.getinsured.hix.webservice.planmgmt.benchmarkplan;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="householdCaseId">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="10"/>
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="coverageStartDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="members">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="member" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="relation">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="self"/>
 *                                   &lt;enumeration value="spouse"/>
 *                                   &lt;enumeration value="child"/>
 *                                   &lt;enumeration value="dependent"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="dob" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="countyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="tobacco" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="Y"/>
 *                                   &lt;enumeration value="N"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "householdCaseId",
    "coverageStartDate",
    "members"
})
@XmlRootElement(name = "getBenchmarkPlanRequest")
public class GetBenchmarkPlanRequest {

    @XmlElement(required = true)
    protected String householdCaseId;
    @XmlElement(required = true)
    protected String coverageStartDate;
    @XmlElement(required = true)
    protected GetBenchmarkPlanRequest.Members members;

    /**
     * Gets the value of the householdCaseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHouseholdCaseId() {
        return householdCaseId;
    }

    /**
     * Sets the value of the householdCaseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHouseholdCaseId(String value) {
        this.householdCaseId = value;
    }

    /**
     * Gets the value of the coverageStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoverageStartDate() {
        return coverageStartDate;
    }

    /**
     * Sets the value of the coverageStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoverageStartDate(String value) {
        this.coverageStartDate = value;
    }

    /**
     * Gets the value of the members property.
     * 
     * @return
     *     possible object is
     *     {@link GetBenchmarkPlanRequest.Members }
     *     
     */
    public GetBenchmarkPlanRequest.Members getMembers() {
        return members;
    }

    /**
     * Sets the value of the members property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetBenchmarkPlanRequest.Members }
     *     
     */
    public void setMembers(GetBenchmarkPlanRequest.Members value) {
        this.members = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="member" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="relation">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="self"/>
     *                         &lt;enumeration value="spouse"/>
     *                         &lt;enumeration value="child"/>
     *                         &lt;enumeration value="dependent"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="dob" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="countyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="tobacco" minOccurs="0">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="Y"/>
     *                         &lt;enumeration value="N"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "member"
    })
    public static class Members {

        @XmlElement(required = true)
        protected List<GetBenchmarkPlanRequest.Members.Member> member;

        /**
         * Gets the value of the member property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the member property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMember().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GetBenchmarkPlanRequest.Members.Member }
         * 
         * 
         */
        public List<GetBenchmarkPlanRequest.Members.Member> getMember() {
            if (member == null) {
                member = new ArrayList<GetBenchmarkPlanRequest.Members.Member>();
            }
            return this.member;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="relation">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="self"/>
         *               &lt;enumeration value="spouse"/>
         *               &lt;enumeration value="child"/>
         *               &lt;enumeration value="dependent"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="dob" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="countyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="tobacco" minOccurs="0">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="Y"/>
         *               &lt;enumeration value="N"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "relation",
            "dob",
            "zip",
            "countyCode",
            "tobacco"
        })
        public static class Member {

            @XmlElement(required = true, defaultValue = "self")
            protected String relation;
            @XmlElement(required = true)
            protected String dob;
            @XmlElement(required = true)
            protected String zip;
            @XmlElement(required = true)
            protected String countyCode;
            @XmlElement(defaultValue = "N")
            protected String tobacco;

            /**
             * Gets the value of the relation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelation() {
                return relation;
            }

            /**
             * Sets the value of the relation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelation(String value) {
                this.relation = value;
            }

            /**
             * Gets the value of the dob property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDob() {
                return dob;
            }

            /**
             * Sets the value of the dob property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDob(String value) {
                this.dob = value;
            }

            /**
             * Gets the value of the zip property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getZip() {
                return zip;
            }

            /**
             * Sets the value of the zip property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setZip(String value) {
                this.zip = value;
            }

            /**
             * Gets the value of the countyCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountyCode() {
                return countyCode;
            }

            /**
             * Sets the value of the countyCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountyCode(String value) {
                this.countyCode = value;
            }

            /**
             * Gets the value of the tobacco property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTobacco() {
                return tobacco;
            }

            /**
             * Sets the value of the tobacco property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTobacco(String value) {
                this.tobacco = value;
            }

        }

    }

}
