package com.getinsured.eligibility.prescreen.calculator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.prescreen.calculator.dto.StatePovertyGuidelines;
import com.getinsured.eligibility.prescreen.repository.IStatePovertyGuidelinesRepository;
import com.getinsured.hix.model.eligibility.CmsPovertyGuidelines;
import com.getinsured.hix.model.prescreen.calculator.FplRequest;
import com.getinsured.hix.model.prescreen.calculator.FplResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration.IEXConfigurationEnum;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.StateHelper.State;

/**
 * This class implements the Federal Poverty Line (FPL %) Calculator. The
 * calculation is based on the federal poverty guidelines governed by the U.S
 * department of health and human services.
 * 
 * <p>
 * FPL calculator depends it calculation on the state, family size and family
 * income of the individual.
 * </p>
 * <p>
 * New: JIRA @see HIX-7494
 * </p>
 * <p>
 * History: Before we are using Rule Engine and Team decided to go with Java
 * code to avoid rule engine all together. This decision was made on 04/10/2013
 * Old JIRA @see REDHAT22
 * </p>
 * 
 * 
 * @author Sunil Desu
 * @since April 11 2013
 * 
 */
@Component
public class FplCalculator {

	private IStatePovertyGuidelinesRepository iStatePovertyGuidelinesRepository;

	public FplCalculator() {
		super();
	}

	@Autowired
	public FplCalculator(IStatePovertyGuidelinesRepository iStatePovertyGuidelinesRepository) {
		super();
		this.iStatePovertyGuidelinesRepository = iStatePovertyGuidelinesRepository;
		// Alaska
		createAlaskaPovertyGuidelines();
		// Hawaii
		createHawaiiPovertyGuidelines();
		// Other states + DC
		createOtherStatesPovertyGuidelines();
		getLatestAvailCoverageYear();
	}

	private static final Logger LOGGER = Logger.getLogger(FplCalculator.class);

	private static final StateHelper STATE_HELPER = new StateHelper();
	private static List<State> statesList = STATE_HELPER.getAllStates();
	private static final String ALASKA_ABBR = "AK";
	private static final String HAWAII_ABBR = "HI";
	private static final String OTHER_STATES = "OTHER";
	private static final int MAX_PERSON_FOR_FPL = 8;
	private static final int YEAR_DECREMENT_VALUE = 1;
	private static final int MAX_FAMILY_SIZE = 100;
	private static final double HUNDRED_POINT_ZERO = 100.00;
	private static  int LATEST_AVAILABLE_COVERAGE_YEAR = 0;
	

	/**
	 * This map contains the year values.
	 * 
	 * <p>
	 * Key is the coverage year
	 * </p>
	 * 
	 * <p>
	 * Value returned for a matching key is year values for a state and given year.
	 * </p>
	 * 
	 */
	private static List<CmsPovertyGuidelines> YEAR_VALUES_LIST = new ArrayList<CmsPovertyGuidelines>();

	/**
	 * This map contains the poverty guidelines.
	 * 
	 * <p>
	 * Key is the coverage year
	 * </p>
	 * 
	 * <p>
	 * Value returned for a matching key is state poverty guideline.
	 * </p>
	 * 
	 */
	private static final Map<Integer, Map<String, StatePovertyGuidelines>> STATE_POVERTY_GUIDELINES = new HashMap<Integer, Map<String, StatePovertyGuidelines>>();

	/**
	 * Create Poverty Guideline for Alaska state
	 */
	private void createAlaskaPovertyGuidelines() {

		List<CmsPovertyGuidelines> yearValues = getYearValues();
		createStatePovertyGuidelineObj(ALASKA_ABBR, yearValues);
	}

	/**
	 * Create Poverty Guideline for Hawaii state
	 */
	private void createHawaiiPovertyGuidelines() {

		List<CmsPovertyGuidelines> yearValues = getYearValues();
		createStatePovertyGuidelineObj( HAWAII_ABBR, yearValues);
	}

	/**
	 * Create Poverty Guideline for OTHER states
	 */
	private void createOtherStatesPovertyGuidelines() {

		List<CmsPovertyGuidelines> yearValues = getYearValues();
		createStatePovertyGuidelineObj(OTHER_STATES, yearValues);
	}

	/**
	 * This method all yearValues from DB.
	 * 
	 * @return List<CmsPovertyGuidelines>
	 */

	private List<CmsPovertyGuidelines> getYearValues() {
		if (YEAR_VALUES_LIST.isEmpty())
			YEAR_VALUES_LIST.addAll(iStatePovertyGuidelinesRepository.findAll());
		return YEAR_VALUES_LIST;
	}
	
	private void getLatestAvailCoverageYear() {
		LATEST_AVAILABLE_COVERAGE_YEAR = iStatePovertyGuidelinesRepository.getLatestCoverageYear().intValue();
	}

	/**
	 * Create createStatePovertyGuidelineObj based on data passed
	 * 
	 * @param coverageYear
	 * @param stateCode
	 * @param yearValues
	 * @param addtnlPersonValue
	 */
	private static void createStatePovertyGuidelineObj(String stateCode,
			List<CmsPovertyGuidelines> yearValues) {

		if (yearValues != null && !yearValues.isEmpty()) {
			for (CmsPovertyGuidelines yearValue : yearValues) {
				StatePovertyGuidelines statePovGuidelines = new StatePovertyGuidelines();
				int coverageYear = yearValue.getYear().intValue();
				statePovGuidelines.setCoverageYear(coverageYear);
				statePovGuidelines.setStateCode(stateCode);
				statePovGuidelines
						.setIncrementPerAdditionalPerson(Float.parseFloat(yearValue.getAdditionalPerson().toString()));

				Map<Integer, Float> thresholds = statePovGuidelines.getThresholds();

				thresholds.put(new Integer(1), Float.parseFloat(yearValue.getPovertyFamilySize1().toString()));
				thresholds.put(new Integer(2), Float.parseFloat(yearValue.getPovertyFamilySize2().toString()));
				thresholds.put(new Integer(3), Float.parseFloat(yearValue.getPovertyFamilySize3().toString()));
				thresholds.put(new Integer(4), Float.parseFloat(yearValue.getPovertyFamilySize4().toString()));
				thresholds.put(new Integer(5), Float.parseFloat(yearValue.getPovertyFamilySize5().toString()));
				thresholds.put(new Integer(6), Float.parseFloat(yearValue.getPovertyFamilySize6().toString()));
				thresholds.put(new Integer(7), Float.parseFloat(yearValue.getPovertyFamilySize7().toString()));
				thresholds.put(new Integer(8), Float.parseFloat(yearValue.getPovertyFamilySize8().toString()));

				if (!STATE_POVERTY_GUIDELINES.containsKey(coverageYear)) {
					STATE_POVERTY_GUIDELINES.put(coverageYear, new HashMap<String, StatePovertyGuidelines>());
				}
				Map<String, StatePovertyGuidelines> stateToPovertyGuideObjMap = STATE_POVERTY_GUIDELINES.get(coverageYear);
				stateToPovertyGuideObjMap.put(stateCode, statePovGuidelines);
			}
		}
		

	}

	/**
	 * This is the method that will be exposed for invocation for the calculation of
	 * FPL.
	 * 
	 * @author Sunil Desu
	 * @since April 11 2013
	 * 
	 * @param fplRequest
	 *            is the request object which contains the required parameters for
	 *            FPL Calculation. State, family size and family income are required
	 *            and cannot be {@code null}.
	 * 
	 * @return fplResponse is the response object which contains
	 *         <ol>
	 *         <li>the percentage value of FPL
	 *         <li>error message, which is populated if the processing results in an
	 *         error condition
	 *         </ol>
	 */
	@SuppressWarnings("static-access")
	public FplResponse calculateFpl(FplRequest fplRequest, boolean isMedicaidChip, int familySize) {

		FplResponse fplResponse = new FplResponse();
		fplResponse.startResponse();
		fplResponse.setAppID(fplRequest.getAppID());

		String monthDay = DynamicPropertiesUtil
				.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PRESCREENER_MEDICAID_USE_LATEST_FPL_FROM);
		String[] str = monthDay.split("/");
		int currentYear = (int) fplRequest.getCoverageYear();
		int nextYear = currentYear;
		int modifiedCoverageYear = 0;
		int monthOfYear = Integer.parseInt(str[0]);
		int dayOfMonth = Integer.parseInt(str[1]);
		modifiedCoverageYear = currentYear - YEAR_DECREMENT_VALUE;

		// HIX-108270: FPL values for Medicaid and CHIP eligibility from 7/1 to 6/31

		Calendar today = DateUtil.getEffectiveDate(null).getInstance();
		int month = today.get(Calendar.MONTH);
		month = month + 1;
		int day = today.get(Calendar.DAY_OF_MONTH);
		if (isMedicaidChip) {
			if (month >= monthOfYear && day >= dayOfMonth) {
				modifiedCoverageYear = nextYear;
			}
			if (familySize > 0) {
				fplRequest.setFamilySize(familySize);
			}
		}

		fplRequest.setCoverageYear(modifiedCoverageYear);

		String errMsg = "";
		try {

			validateRequest(fplRequest);

			LOGGER.info(
					"FPL Calculator is called for fpl calculation for the following record: " + fplRequest.toString());

			String state = OTHER_STATES;
			if (fplRequest.getState().trim().equalsIgnoreCase(ALASKA_ABBR)
					|| fplRequest.getState().trim().equalsIgnoreCase("ALASKA")) {
				state = ALASKA_ABBR;
			} else if (fplRequest.getState().trim().equalsIgnoreCase(HAWAII_ABBR)
					|| fplRequest.getState().trim().equalsIgnoreCase("HAWAII")) {
				state = HAWAII_ABBR;
			}

			StatePovertyGuidelines  statePovertyGuidelines = null;
			if (STATE_POVERTY_GUIDELINES.containsKey((int) fplRequest.getCoverageYear())) {
				statePovertyGuidelines = STATE_POVERTY_GUIDELINES.get((int) fplRequest.getCoverageYear()).get(state);
				LOGGER.info("Requested Coverage year is "+(int) fplRequest.getCoverageYear()+" and StatePovertyGuidelines provided for year "+(int) fplRequest.getCoverageYear());
			} else {
				statePovertyGuidelines = STATE_POVERTY_GUIDELINES.get(LATEST_AVAILABLE_COVERAGE_YEAR).get(state);
				LOGGER.info("Requested Coverage year is "+(int) fplRequest.getCoverageYear()+" and StatePovertyGuidelines provided for year "+LATEST_AVAILABLE_COVERAGE_YEAR);
			}
			
			
			
			fplResponse.setFplPercentage(computeFpl(fplRequest.getFamilySize(), fplRequest.getFamilyIncome(),statePovertyGuidelines));
		} catch (IllegalArgumentException iae) {
			errMsg = "Illegal argument exception occured in FPL calculator for the following record: "
					+ fplRequest.toString() + iae.getMessage();
			LOGGER.error(errMsg);
			fplResponse.setErrMsg(errMsg);
		} catch (Exception ex) {
			errMsg = "General exception occured in FPL calculator for the following record: " + fplRequest.toString()
					+ ex.getMessage();
			LOGGER.error(errMsg);
			fplResponse.setErrMsg(errMsg);
		} finally {
			fplResponse.endResponse();
		}
		LOGGER.info("FPL calculator response: " + fplResponse.toString());
		return fplResponse;
	}

	/**
	 * This method validates the FplRequest object that is received.
	 * <p>
	 * State, family size and family income are validated.
	 * </p>
	 * 
	 * @author Sunil Desu
	 * @since April 12 2013
	 * 
	 * @param fplRequest
	 * @throws NullPointerException
	 *             if request object is null.
	 * @throws NullPointerException
	 *             is state field is not populated or empty string is sent.
	 * @throws IllegalArgumentException
	 *             if family size is less than 1.
	 * @throws IllegalArgumentException
	 *             if family income entered is less than 0.
	 * @throws IllegalArgumentException
	 *             if state that is entered is not a valid state name.
	 */
	private void validateRequest(FplRequest fplRequest) {

		validateStateAndFamilySize(fplRequest);

		// Family income check and validation
		if (fplRequest.getFamilyIncome() < 0) {
			throw new IllegalArgumentException("Family Income cannot be less than 0.");
		}

		// State validation
		if (fplRequest.getState().trim().length() == 2) {
			if (STATE_HELPER.getStateName(fplRequest.getState().trim().toUpperCase()) == null) {
				throw new IllegalArgumentException("Invalid state entered.");
			}
			return;
		} else {
			boolean isStateValid = false;
			for (State state : statesList) {
				if (state.getName().equalsIgnoreCase(fplRequest.getState().trim())) {
					isStateValid = true;
					break;
				}
			}
			if (!isStateValid) {
				throw new IllegalArgumentException("Invalid state entered.");
			}
		}

	}

	/**
	 * This method validates the family size and state for a FPL request
	 * 
	 * @author - Nikhil Talreja
	 * @since - 26 December, 2013
	 * @throws -
	 *             IllegalArgumentException, if any validation fails
	 */
	private void validateStateAndFamilySize(FplRequest fplRequest) {

		if (fplRequest == null) {
			throw new IllegalArgumentException("FPL calculator request cannot be null.");
		}

		// State check
		if (fplRequest.getState() == null || fplRequest.getState().trim().length() == 0) {
			throw new IllegalArgumentException("State is a required field for FPL calculation.");
		}

		// Family size check and validation
		if (fplRequest.getFamilySize() <= 0) {
			throw new IllegalArgumentException("Family size cannot be less than 1.");
		}

		// Family size check and validation
		if (fplRequest.getFamilySize() >= MAX_FAMILY_SIZE) {
			throw new IllegalArgumentException("Family size cannot be greater than " + MAX_FAMILY_SIZE + ".");
		}
	}

	/**
	 * This method computes the FPL percentage based on the input parameters. This
	 * method is called after the request validation is successful.
	 * 
	 * @author Sunil Desu
	 * @since April 12 2013
	 * 
	 * @param familySize
	 *            is the number of individuals in the family/household whose FPL is
	 *            being calculated
	 * @param familyIncome
	 *            is the income that includes the income of all the individuals in a
	 *            household.
	 * @param povertyGuidelines
	 *            are the guidelines set by the US department of health and human
	 *            services.
	 * @param incrementPerAddtnlPerson
	 *            is the additional amount to be added to povertyGuidelines per each
	 *            additional person above 8 members in a household, as set by the
	 *            U.S. department of human and health services.
	 * @return FPL percentage value.
	 * 
	 */
	private double computeFpl(int familySize, double familyIncome, StatePovertyGuidelines statePovertyGuideline) {
		double fpl = 0.0;
		double denominator = 0.0;
		Map<Integer, Float> povertyGuidelines = statePovertyGuideline.getThresholds();

		if (familySize <= MAX_PERSON_FOR_FPL) {
			denominator = povertyGuidelines.get(familySize);
		} else {
			denominator = povertyGuidelines.get(MAX_PERSON_FOR_FPL)
					+ (familySize - MAX_PERSON_FOR_FPL) * statePovertyGuideline.getIncrementPerAdditionalPerson();
		}

		fpl = familyIncome / denominator * HUNDRED_POINT_ZERO;

		return fpl;
	}
}
