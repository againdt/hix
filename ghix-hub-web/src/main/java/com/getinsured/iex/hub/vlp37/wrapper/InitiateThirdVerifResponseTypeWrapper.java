package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv.InitiateThirdVerifResponseType;

public class InitiateThirdVerifResponseTypeWrapper implements JSONAware{

	private InitiateThirdVerifResponseType initiateThirdVerifResponseType;
	
	private InitiateThirdVerifResponseMetadataWrapper responseMetadata;
	private String lawfulPresenceVerifiedCode;
	
	public InitiateThirdVerifResponseTypeWrapper(InitiateThirdVerifResponseType initialVerificationResponseType){
		
		if(initiateThirdVerifResponseType == null){
			return;
		}
		
		InitiateThirdVerifResponseMetadataWrapper responseMetadataWrapper = new InitiateThirdVerifResponseMetadataWrapper(initialVerificationResponseType.getResponseMetadata());
		this.responseMetadata = responseMetadataWrapper;
		
	}

	public InitiateThirdVerifResponseType getInitiateThirdVerifResponseType() {
		return initiateThirdVerifResponseType;
	}

	public void setInitiateThirdVerifResponseType(
			InitiateThirdVerifResponseType initiateThirdVerifResponseType) {
		this.initiateThirdVerifResponseType = initiateThirdVerifResponseType;
	}

	public InitiateThirdVerifResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(InitiateThirdVerifResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}

	public String getLawfulPresenceVerifiedCode() {
		return lawfulPresenceVerifiedCode;
	}

	public void setLawfulPresenceVerifiedCode(String lawfulPresenceVerifiedCode) {
		this.lawfulPresenceVerifiedCode = lawfulPresenceVerifiedCode;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseMetadata",this.responseMetadata);
		obj.put("LawfulPresenceVerifiedCode",this.lawfulPresenceVerifiedCode);
		return obj.toJSONString();
	}

}
