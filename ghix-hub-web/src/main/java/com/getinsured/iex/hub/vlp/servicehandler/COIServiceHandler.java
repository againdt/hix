package com.getinsured.iex.hub.vlp.servicehandler;

import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.GetCaseDetailsResponseType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlpcoi.GetCountryOfIssuanceListResponseType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlpcoi.ObjectFactory;
import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.vlp.wrapper.GetCountryOfIssuanceListResponseTypeWrapper;

/**
 * Service handler for COI Service
 * 
 * @author - Nikhil Talreja
 * @since - 09-Apr-2014
 */
public class COIServiceHandler extends AbstractServiceHandler {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GetCaseDetailsServiceHandler.class);
	
	private static ObjectFactory factory;
	
	static{
		
		if(factory == null){
			factory = new ObjectFactory();
		}
	}	
	
	@Override
	public Object getRequestpayLoad() {
		return factory.createGetCountryOfIssuanceListRequest("");
	}

	@Override
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		
		String message = null;
		try{
			@SuppressWarnings("unchecked")
			JAXBElement<GetCountryOfIssuanceListResponseType> resp= (JAXBElement<GetCountryOfIssuanceListResponseType>)responseObject;
			GetCountryOfIssuanceListResponseType responsePayload = resp.getValue();
			GetCountryOfIssuanceListResponseTypeWrapper response = new GetCountryOfIssuanceListResponseTypeWrapper(responsePayload);

			if(response != null){
				message = response.toJSONString();
				LOGGER.debug("Response Payload " + message);
				if(response.getResponseMetadata() != null){
					this.setResponseCode(response.getResponseMetadata().getResponseCode());
				}
				this.requestStatus = RequestStatus.SUCCESS;
			}
			
			LOGGER.debug("Response message " + message);
			
		}catch(ClassCastException ce){
			message = "Class in response: " + responseObject.getClass().getName()+" Expecting:"+ GetCaseDetailsResponseType.class.getName();
			this.requestStatus = RequestStatus.FAILED;
			throw new HubServiceException(message, ce);
		}
		return message;
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Marshaller getServiceMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

}
