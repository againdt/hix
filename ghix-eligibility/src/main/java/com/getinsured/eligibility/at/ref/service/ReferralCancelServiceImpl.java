package com.getinsured.eligibility.at.ref.service;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.enums.ReferralActivationEnum;
import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.repository.ReferralActivationRepository;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.platform.accountactivation.repository.IAccountActivationRepository;

/**
 * @author chopra_s
 * 
 */
@Component("referralCancelService")
@Scope("singleton")
public class ReferralCancelServiceImpl implements ReferralCancelService {
	private static final Logger LOGGER = Logger.getLogger(ReferralCancelServiceImpl.class);

	@Autowired
	@Qualifier("referralActivationRepository")
	private ReferralActivationRepository referralActivationRepository;

	@Autowired
	private IAccountActivationRepository accountActivationRepository;

	private static String LCE_REFERRAL_YES = "Y";
	
	private static String CREATED_OBJECT_TYPE = "INDIVIDUAL_REFERRAL";
	
	@Override
	public boolean cancelActivation(int ssapApplicationId) {
		boolean isLce = false;
		final ReferralActivation referralActivation = referralActivationRepository.findBySsapApplication(ssapApplicationId);
		if (referralActivation == null) {
			LOGGER.debug("No ReferralActivation found for ssapApplicationId " + ssapApplicationId);
		} else {
			if (referralActivation.isActivationComplete()) {
				LOGGER.debug("ReferralActivation " + referralActivation.getId() + " workflow is  " + ReferralActivationEnum.ACTIVATION_COMPELETE);
			} else {
				cancelActivation(referralActivation);
				cancelAccountActivation(ssapApplicationId);
				isLce = referralActivation.getIsLce().equals(LCE_REFERRAL_YES);
			}
		}
		return isLce;
	}

	private void cancelAccountActivation(int ssapApplicationId) {
		final AccountActivation accountActivation = accountActivationRepository.findByCreatedObjectId(ssapApplicationId, CREATED_OBJECT_TYPE);
		if (accountActivation == null) {
			return;
		}
		if (!accountActivation.getStatus().equals(AccountActivation.STATUS.PROCESSED)) {
			accountActivation.setStatus(AccountActivation.STATUS.PROCESSED);
			accountActivationRepository.save(accountActivation);
		}
	}

	private void cancelActivation(ReferralActivation referralActivation) {
		referralActivation.setLastUpdatedOn(new TSDate());
		referralActivation.setWorkflowStatus(ReferralActivationEnum.CANCEL_ACTIVATION);
		referralActivationRepository.save(referralActivation);
	}

}
