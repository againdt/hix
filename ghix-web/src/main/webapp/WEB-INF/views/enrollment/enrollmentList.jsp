
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<jsp:include page="/WEB-INF/views/layouts/untiletopbar.jsp" />

<%@ page isELIgnored="false"%>

<div id="maincontainer" class="container">
<div class="row">
	<div class="span4">
		<h2>Application Administration</h2>
	</div>
	<div>
		<br><br/>
		<p><a href="application">Create Application</a></p>
	</div>
	
	<div>
		<form class="form-stacked" id="frmlist" name="frmlist" action="applicationList" method="POST">
			<fieldset>
				<legend></legend>
				<p></p>
				<br />
				<div class="profile">
					<div class="clearfix">
						  <display:table pagesize="5" export="true" name="applicationList" sort="list" id="data" requestURI="applicationList">
							  <display:setProperty name="paging.banner.placement">bottom</display:setProperty>
							  
							  <display:setProperty name="export.excel" value="true" />
							  <display:setProperty name="export.csv" value="true" />
							  <display:setProperty name="export.xml" value="true" />
							  
							  <display:setProperty name="export.excel.filename" value="application.xls"/>
							  <display:setProperty name="export.csv.filename" value="application.csv"/>
							  <display:setProperty name="export.xml.filename" value="application.xml"/>
							 
							  <display:column title="Id">
							  		<a href='application/${data.id}'>${data.id}</a>
							  </display:column>
							  <display:column property="policyNumber" title="Policy Number" sortable="true"/>
							  <display:column property="status" title="Status" sortable="true"/>
							  <display:column property="effectiveDate" title="Effective Date" format="{0,date,MM/dd/yyyy}" sortable="true"/>
							  <display:column property="terminationDate" title="Termination Date" format="{0,date,MM/dd/yyyy}" sortable="true"/>
							  <display:column property="created" title="Created" format="{0,date,MM/dd/yyyy}" sortable="true"/>
						</display:table>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>
</div>
<%@ include file="/WEB-INF/views/layouts/untilefooter.jsp"%>
