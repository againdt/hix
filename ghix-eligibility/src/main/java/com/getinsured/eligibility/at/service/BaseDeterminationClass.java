package com.getinsured.eligibility.at.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.getinsured.eligibility.active.enrollment.service.ActiveEnrollmentService;
import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.dto.ApplicationsWithSameIdDTO;
import com.getinsured.eligibility.at.ref.service.ReferralCancelService;
import com.getinsured.eligibility.at.ref.service.ReferralOEService;
import com.getinsured.eligibility.at.resp.service.SsapApplicantService;
import com.getinsured.eligibility.at.resp.si.dto.SsapApplicationCmrCreatedDto;
import com.getinsured.eligibility.at.resp.si.transform.PersonTransformer;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.AtResponseTypeEnum;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.eligibility.redetermination.service.RenewalApplicationService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
public abstract class BaseDeterminationClass implements BaseDetermination, ApplicationContextAware {
	private static final String ID_PROCESS_INITIAL_REFERRAL = "ID - PROCESS Initial REFERRAL";

	private static final String ID_PROCESS_QE_REFERRAL = "ID - PROCESS QE REFERRAL";

	private static final String ID_PROCESS_LCE_REFERRAL = "ID - PROCESS LCE REFERRAL";

	private static final String ID_PROCESS_RENEWAL_REFERRAL = "ID - PROCESS RENEWAL REFERRAL";

	private static final Logger LOGGER = Logger.getLogger(BaseDeterminationClass.class);
	
	private static ApplicationContext applicationContext;
	
	String mutipleEligibiltySpanEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_MULTIPLE_ELIGIBILITY_SPAN_CONFIG);

	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		applicationContext = appContext;
		
	}
	
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	

	@Autowired
	@Qualifier("activeEnrollmentService")
	private ActiveEnrollmentService activeEnrollmentService;

	@Autowired
	@Qualifier("cmrHouseholdRepository")
	private CmrHouseholdRepository cmrHouseholdRepository;
	
	 

	@Autowired
	@Qualifier("referralOEService")
	private ReferralOEService referralOEService;

	@Autowired
	private ReferralCancelService referralCancelService;
	 
	@Autowired
	private SsapApplicantService ssapApplicantService;
	
	//@Autowired private QLEValidationService qleValidationService;
	
	@Autowired private IUserRepository iUserRepository;

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private RenewalApplicationService renewalApplicationService;

	 
	@Override
	public AccountTransferRequestDTO determineProcessor(String identificationId, long coverageYear, AccountTransferRequestPayloadType request, Integer giwsPayloadId, SsapApplicant originSsapApplicant, String caseNumber) {
		AccountTransferRequestDTO accountTransferRequestDTO = new AccountTransferRequestDTO();
		accountTransferRequestDTO.setAccountTransferRequestPayloadType(request);
		accountTransferRequestDTO.setGiwsPayloadId(giwsPayloadId);
		accountTransferRequestDTO.setFullDetermination(true);
		accountTransferRequestDTO.setNotificationType(ReferralConstants.WITHOUT_LINK);
		accountTransferRequestDTO.setCoverageYear(coverageYear);

		/*final boolean isFinal = ReferralUtil.isFullDetermination(request, IDAHO);
		String responseType;
		if (isFinal) {
			LOGGER.info("ID - REFERRAL/LCE FLOW STARTS WITH FINAL DECISION - STARTS");
			responseType = AtResponseTypeEnum.UPDATED.toString();

		} else {
			LOGGER.info("ID - REFERRAL/LCE FLOW STARTS WITHOUT FINAL DECISION - STARTS");
			responseType = AtResponseTypeEnum.FULL_DETERMINATION.toString();
		}*/
		
		accountTransferRequestDTO.setAtResponseType(AtResponseTypeEnum.UPDATED.toString());
		 List<SsapApplication> ssapApplications =null ;
		 
		//This flag is used for fetching cmr base on householdCase ID
		final String useExternalId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_HOUSEHOLD_CASE_ID_ENABLE);
		final String linkATUsingAppId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_LINK_AT_USING_APPLICATION_ID);
			
		if(null != useExternalId && "TRUE".equalsIgnoreCase(useExternalId) && request!=null) {
			
			List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes =request.getInsuranceApplication().getApplicationIdentification();
			String hhCaseId = null ;
			for(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType : iTypes) {
				if( null != linkATUsingAppId && "TRUE".equalsIgnoreCase(linkATUsingAppId) && (tIdentificationType.getIdentificationCategoryText() == null) ){
					hhCaseId = tIdentificationType.getIdentificationID().getValue();
					break;
				}
				if(  ReferralConstants.HOUSE_HOLD_CASE_ID.equalsIgnoreCase( tIdentificationType.getIdentificationCategoryText( ).getValue()  )   ) {
					hhCaseId = tIdentificationType.getIdentificationID().getValue();
					break;
				}
			}
		
			ssapApplications = ssapApplicantService.findSsapApplicationForHHCaseId(hhCaseId);
			
		}else {
			//This else block is being executed for medicaid-id in AT
			if(identificationId != null) {
			 ssapApplications = ssapApplicantService.findSsapApplicationForExternalApplicantId(identificationId);
			}
				
			  //HIX-108144 -- Explore undiscovered applications.
				if(null !=ssapApplications && !ssapApplications.isEmpty()) {
	                       Set<BigDecimal> cmrIdSet = new HashSet<BigDecimal>();
	                       for(SsapApplication localApp :ssapApplications) {
	                    	   	if( null != localApp.getCmrHouseoldId() &&  localApp.getCmrHouseoldId().longValue() != 0 ) {
	                    	   		cmrIdSet.add( localApp.getCmrHouseoldId());
	                    	   	}
	                               
	                       }
	                       
	                       for(BigDecimal cmrId:cmrIdSet ) {
	                               List<SsapApplication> localAppList = ssapApplicationRepository.findByCmrHouseoldId(cmrId);
	                               if(null != localAppList && !localAppList.isEmpty()) {
	                                       
	                                       for (SsapApplication localApp :localAppList ) {
	                                               boolean matchedApp = false;
	                                                for (SsapApplication appReturnFromExternalId :ssapApplications) {
	                                                        if(appReturnFromExternalId.getId() == localApp.getId()) {
	                                                                matchedApp = true;
	                                                        }
	                                                }
	                                                if(!matchedApp) {
	                                                        ssapApplications.add(localApp);
	                                                }
	                                       }
	                               }
	                       }
	               }//End of discovery of undiscovered applications.
			  
			  
			  if(ssapApplications == null || ssapApplications.isEmpty()) {
					if(ssapApplications == null) {
						ssapApplications = new ArrayList<SsapApplication>();
					}
					
					PersonType person = null;
					if(request!=null) {
						person = PersonTransformer.getPrimaryPerson(request);
					}
					String ssn = null;
					Date dob = null;
					String lastName = null;
					String gender = null;
					String firstName = null;
					if(person != null) {
						ssn =PersonTransformer.formatSSN(person); 
						dob = ReferralUtil.extractDate(person.getPersonBirthDate());
						lastName = PersonTransformer.extractPersonInfoString(person.getPersonName().get(0).getPersonSurName());
						gender =  PersonTransformer.extractPersonInfoString(person.getPersonSexText());
						firstName = PersonTransformer.extractPersonInfoString(person.getPersonName().get(0).getPersonGivenName());
					}
					else if(originSsapApplicant != null) {
						ssn =originSsapApplicant.getSsn(); 
						dob = originSsapApplicant.getBirthDate();
						lastName = originSsapApplicant.getLastName();
						gender =  originSsapApplicant.getGender();
						firstName = originSsapApplicant.getFirstName();
					}
					if(person != null || originSsapApplicant != null) {
						if(StringUtils.isNotBlank(ssn) && dob!=null) {
							List<SsapApplicant> ssapApplicants =  ssapApplicantRepository.findBySsnAndDobWithAllCMR(ssn, dob);
							for(SsapApplicant ssapApplicant : ssapApplicants) {
								if(lastName.equalsIgnoreCase(ssapApplicant.getLastName()) || "FEMALE".equalsIgnoreCase(gender) ) {
									ssapApplications.add(ssapApplicant.getSsapApplication());
								}
							}
					    }/* Don't Autolink the HH in LCE flow based on name and dob 
						else if(ssapApplications == null || ssapApplications.isEmpty()){//this loop is to get NF app based on fn,ln, dob as old logic if ssn is null for doing NF-F lce
					    	List<SsapApplicant> ssapApplicants =  ssapApplicantRepository.findByNameAndDobWithAllCMR(ReferralUtil.lowercase(firstName),ReferralUtil.lowercase(lastName), dob);
							for(SsapApplicant ssapApplicant : ssapApplicants) {
								ssapApplications.add(ssapApplicant.getSsapApplication());
							}
					    } */
					}
				}	
		} // END of else block of medicade-ID
		  //if request is null then get the applications list and iterate it if the case number match then remove the application and then send the list
		List<SsapApplication> finalSsapApplications = new ArrayList<SsapApplication>();
		if(request == null && StringUtils.isNotBlank(caseNumber)){
			for (SsapApplication ssapApplication : ssapApplications) {
				if(!caseNumber.equals(ssapApplication.getCaseNumber())){
					finalSsapApplications.add(ssapApplication);
				}
			}
			
			processLogicToDetermine(accountTransferRequestDTO, finalSsapApplications);
		}else{
			processLogicToDetermine(accountTransferRequestDTO, ssapApplications);
		}

		populateProcessToBeExecuted(accountTransferRequestDTO);

		return accountTransferRequestDTO;
	}

	
	private void populateProcessToBeExecuted(AccountTransferRequestDTO accountTransferRequestDTO) {
		if (accountTransferRequestDTO.isRenewal()) {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info(ID_PROCESS_RENEWAL_REFERRAL);
			}
			accountTransferRequestDTO.setProcessToBeExecuted(PROCESS_RENEWAL_REFERRAL);
		} else if (accountTransferRequestDTO.isLCE()) {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info(ID_PROCESS_LCE_REFERRAL);
			}
			accountTransferRequestDTO.setProcessToBeExecuted(PROCESS_LCE_REFERRAL);
		} else if (accountTransferRequestDTO.isQE()) {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info(ID_PROCESS_QE_REFERRAL);
			}
			accountTransferRequestDTO.setProcessToBeExecuted(PROCESS_QE_REFERRAL);
		} else {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info(ID_PROCESS_INITIAL_REFERRAL);
			}
			accountTransferRequestDTO.setProcessToBeExecuted(PROCESS_INITIAL_REFERRAL);
		}
	}

	/**
	 * In MainDetermination flow- </br>
	 *   <p>  Its being decided whether LCE|QEP|OPE flow should be triggered. Depending on this LCE|OE|QE referral flow will be triggered.</p>
	 *    <p> If old applications are not Enrolled or already closed then they are being closed. </p>
	 *    <p> Latest Enrolled application-no with active enrollment is being searched so that pay-load application can be compared with. </p>
	 *     
	 *     
	 * @param accountTransferRequestDTO - This DTO contains information of application which has been pushed through pay-load
	 * @param ssapApplications - This list contains old applications which has match with pay-load application.
	 */
	abstract void processLogicToDetermine(AccountTransferRequestDTO accountTransferRequestDTO, List<SsapApplication> ssapApplications);

	ApplicationsWithSameIdDTO populateDataInDto(SsapApplication ssapApplication) {
		ApplicationsWithSameIdDTO fieldData = new ApplicationsWithSameIdDTO();

		fieldData.setApplicationId(ssapApplication.getId());
		fieldData.setStatus(ssapApplication.getApplicationStatus());
		fieldData.setCmrId(ReferralUtil.convertToInt(ssapApplication.getCmrHouseoldId()));
		fieldData.setCoverageYear(ssapApplication.getCoverageYear());
		fieldData.setFinancialAssistance(ssapApplication.getFinancialAssistanceFlag());

		return fieldData;
	}

	boolean isEnrollmentActive(SsapApplication enApp) {
		return activeEnrollmentService.isEnrollmentActiveCheck(enApp.getId(), ReferralConstants.EXADMIN_USERNAME);
	}

	long retrieveComparedApplicationId(List<SsapApplication> listOfApps, Set<BigDecimal> cmrIds) {
		if (ReferralUtil.listSize(listOfApps) == ReferralConstants.ONE) {
			LOGGER.info("Found one Enrolled Application Only - " + listOfApps.get(0).getId());
			return listOfApps.get(0).getId();
		} else {
			if (ReferralUtil.collectionSize(cmrIds) == ReferralConstants.ONE) {
				LOGGER.info("Found Multiple Enrolled Applications, But One Consumer Only Only - " + listOfApps.get(0).getId());
				return listOfApps.get(0).getId();
			} else {
				Set<SsapApplicationCmrCreatedDto> data = new TreeSet<>();
				for (SsapApplication ssapApplication : listOfApps) {
					data.add(new SsapApplicationCmrCreatedDto(ssapApplication.getId(), cmrHouseholdRepository.findCreatedDateOfCmr(ssapApplication.getCmrHouseoldId().intValue()), ssapApplication.getCreationTimestamp(), ssapApplication
					        .getCmrHouseoldId()));
				}
				LOGGER.info("Found Multiple Enrolled Applications and Multiple Consumers - " + data);
				return ((SsapApplicationCmrCreatedDto) data.iterator().next()).getSsapApplicationId();
			}
		}
	}

	long retrieveLastComparedToApplication(List<SsapApplication> sameYearApplications) {
		if(sameYearApplications  != null && !sameYearApplications.isEmpty()) {
			return sameYearApplications.get(0).getId();
		}
		return ReferralConstants.NONE;
	}

	long retrieveComparedToApplication(List<SsapApplication> applications, long coverageYear) {
		SsapApplication returnApplication = null;
		for (SsapApplication ssapApplication : applications) {
			//In renewal also for OE, QEP application, we are going to get previous nf application in the normal flow, so removing Y condition to return NF appln also
			//if (ReferralConstants.Y.equals(ssapApplication.getFinancialAssistanceFlag()) && ssapApplication.getCoverageYear() == coverageYear) {
			if (ssapApplication.getCoverageYear() == coverageYear) {
				if (returnApplication == null) {
					returnApplication = ssapApplication;
				}
				if (ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus())) {
					return ssapApplication.getId();
				}
			}
		}
		return (returnApplication == null ? ReferralConstants.NONE : returnApplication.getId());
	}

	void closeApplications(List<SsapApplication> otherApps) {
		for (SsapApplication ssapApplication : otherApps) {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("ID - CLOSE " + ssapApplication.getApplicationStatus() + " APP...");
			}
			closeApplication(ssapApplication);
			closeRenewalAppliction(ssapApplication);
		}
	}

	boolean closeApplication(SsapApplication erApp) {
		/* close app */
		//ssapApplicationService.closeApplication(erApp.getId());
		
		/*
		 * qleValidationService is not auto wired. Instead it is loaded from app context. 
		 * This is done for smooth functioning of ghix-batch in MN - MES flow.
		 * **/
		QLEValidationService qleValidationService = (QLEValidationService) applicationContext.getBean("qleValidationService");
		AccountUser updateUser = iUserRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
		qleValidationService.discardApp(erApp.getCaseNumber(),  ApplicationStatus.CLOSED, updateUser.getId());
		/* If application status is other than UC then dont ignore reseting activation object (as there is none) */
		if (!ApplicationStatus.UNCLAIMED.getApplicationStatusCode().equals(erApp.getApplicationStatus())){
			return true;
		}
		
		int id = (int) erApp.getId();
		/* call Referral API to Cancel Activation and from result determine whether UC was LCE */
		return referralCancelService.cancelActivation(id);
	}

	boolean isQEPeriod(long coverageYear) {
		return referralOEService.isQE(coverageYear);
	}

	boolean isOEPeriod(long coverageYear) {
		return referralOEService.isOpenEnrollment(coverageYear);
	}
	
	boolean isQEPDuringOE(long coverageYear) {
		return referralOEService.isQEPDuringOE(coverageYear);
	}

	/**
	 * If MES config is set to TRUE && application is in QU / DQ or ER status don't close them.
	 * @param applicationStatus
	 * @return
	 */
	protected boolean mesApplicationsAllowedToClose(String applicationStatus) {
		boolean isMesApplicationClosingAllowed = true;
		if(mutipleEligibiltySpanEnabled.equalsIgnoreCase(Boolean.TRUE.toString()) 
				&& (ApplicationStatus.DEQUEUED.toString().equals(applicationStatus)
				|| ApplicationStatus.QUEUED.toString().equals(applicationStatus)
				|| ApplicationStatus.ELIGIBILITY_RECEIVED.toString().equals(applicationStatus))
				){
		return false;	
		}
		return isMesApplicationClosingAllowed;
	}

	private void closeRenewalAppliction(SsapApplication ssapApplication) {
		renewalApplicationService.markRenewalApplicationClose(ssapApplication.getId());
	}
}
