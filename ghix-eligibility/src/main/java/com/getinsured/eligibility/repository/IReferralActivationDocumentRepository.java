package com.getinsured.eligibility.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.ConsumerDocument;

/**
 * @author chopra_s
 */
@Repository
@Qualifier("ireferralActivationDocumentRepository")
@Transactional(readOnly = true)
public interface IReferralActivationDocumentRepository extends JpaRepository<ConsumerDocument, Long> {

}
