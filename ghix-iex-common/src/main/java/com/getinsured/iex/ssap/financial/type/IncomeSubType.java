package com.getinsured.iex.ssap.financial.type;

/**
 * List of sub-types for {@link IncomeType#OTHER}.
 * @author Yevgen Golubenko
 * @since 5/6/19
 */
public enum IncomeSubType {
  CANCELED_DEBT,
  CASH_SUPPORT,
  COURT_AWARD,
  GAMBLING_PRIZE_AWARD,
  JURY_DUTY_PAY,
  OTHER_INCOME,
  UNSPECIFIED
}
