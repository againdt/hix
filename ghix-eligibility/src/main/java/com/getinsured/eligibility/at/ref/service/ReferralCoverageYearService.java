package com.getinsured.eligibility.at.ref.service;

/**
 * @author chopra_s
 *
 */
public interface ReferralCoverageYearService {
	int executeCoverageYear(final int referralActivationId,final int coverageYear);
}
