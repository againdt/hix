package com.getinsured.iex.dto;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the SSAP_VERIFICATIONS database table.
 * 
 */
public class SsapVerification implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;

	private Timestamp creationTimestamp;

	private Timestamp effectiveEndDate;

	private Timestamp effectiveStartDate;

	private Timestamp lastUpdateTimestamp;

	private String source;

	private String verficationResults;

	private String verificationStatus;

	private String verificationType;

	private SsapApplicant ssapApplicant;

	private List<SsapVerificationGiwspayload> ssapVerificationGiwspayloads;

	public SsapVerification() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getCreationTimestamp() {
		return this.creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Timestamp getEffectiveEndDate() {
		return this.effectiveEndDate;
	}

	public void setEffectiveEndDate(Timestamp effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}

	public Timestamp getEffectiveStartDate() {
		return this.effectiveStartDate;
	}

	public void setEffectiveStartDate(Timestamp effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public Timestamp getLastUpdateTimestamp() {
		return this.lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getVerficationResults() {
		return this.verficationResults;
	}

	public void setVerficationResults(String verficationResults) {
		this.verficationResults = verficationResults;
	}

	public String getVerificationStatus() {
		return this.verificationStatus;
	}

	public void setVerificationStatus(String verificationStatus) {
		this.verificationStatus = verificationStatus;
	}

	public String getVerificationType() {
		return this.verificationType;
	}

	public void setVerificationType(String verificationType) {
		this.verificationType = verificationType;
	}

	public SsapApplicant getSsapApplicant() {
		return this.ssapApplicant;
	}

	public void setSsapApplicant(SsapApplicant ssapApplicant) {
		this.ssapApplicant = ssapApplicant;
	}

	public List<SsapVerificationGiwspayload> getSsapVerificationGiwspayloads() {
		return this.ssapVerificationGiwspayloads;
	}

	public void setSsapVerificationGiwspayloads(List<SsapVerificationGiwspayload> ssapVerificationGiwspayloads) {
		this.ssapVerificationGiwspayloads = ssapVerificationGiwspayloads;
	}

	public SsapVerificationGiwspayload addSsapVerificationGiwspayload(SsapVerificationGiwspayload ssapVerificationGiwspayload) {
		getSsapVerificationGiwspayloads().add(ssapVerificationGiwspayload);
		ssapVerificationGiwspayload.setSsapVerification(this);

		return ssapVerificationGiwspayload;
	}

	public SsapVerificationGiwspayload removeSsapVerificationGiwspayload(SsapVerificationGiwspayload ssapVerificationGiwspayload) {
		getSsapVerificationGiwspayloads().remove(ssapVerificationGiwspayload);
		ssapVerificationGiwspayload.setSsapVerification(null);

		return ssapVerificationGiwspayload;
	}

}