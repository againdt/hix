package com.getinsured.eligibility.ssap.integration.household.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.exception.SSAPIntegrationException;
import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.getinsured.eligibility.ssap.integration.util.SsapIntegrationUtil;
import com.jayway.jsonpath.JsonPath;

@Component(value="ssacRuleServiceActivator")
public class SSACRuleServiceActivator {
	
	private static final String SSAC_SUCCESS_RESPONSE_CODE = "HS000000";
	private static final String NON_RETRYABLE_ERROR_RESPONSE_CODE_PREFIX = "HE";
    private static final Logger LOGGER = LoggerFactory.getLogger(SSACRuleServiceActivator.class);
	
	@SuppressWarnings("unchecked")
	@ServiceActivator
	public Message<Map<String, String>>[] split(Message<String> message) throws UnsupportedEncodingException, SSAPIntegrationException, ParseException {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Activating rule for SSAC response: {}", message.getPayload());
		}

		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("SSAC Verification integration invoked.");
		}

		List<Message<Map<String, String>>> requests = null;
		String ssacResponse = message.getPayload();
		String ssacResponseForMember = null;
		String ssapJSON = message.getHeaders().get(SSAPIntegrationConstants.FILTERED_SSAP_JSON_HEADER).toString();
		JSONParser parser = new JSONParser();
		JSONObject ssapJSONObject = (JSONObject)parser.parse(ssapJSON);
		ssapJSONObject = (JSONObject) ssapJSONObject.get("singleStreamlinedApplication");
		JSONArray taxHouseholds = (JSONArray)ssapJSONObject.get("taxHousehold");
		requests = new ArrayList<Message<Map<String, String>>>();
		
		// Check if we need to invoke the rule for each member
		for(Object taxHousehold : taxHouseholds ) {
			JSONObject household = (JSONObject) taxHousehold;
			JSONArray householdMembers = (JSONArray) household.get("householdMember");
			for(Object householdMember : householdMembers) {
				JSONObject member = (JSONObject) householdMember;
		        JSONObject socialSecurityCard = (JSONObject)member.get("socialSecurityCard");
		        Object socialSecurityNumber = socialSecurityCard.get("socialSecurityNumber");
		        if(null != socialSecurityNumber && StringUtils.isNotBlank(socialSecurityNumber.toString())) {
		            String ssn = socialSecurityNumber.toString();
    				if(null != ssacResponse && ssacResponse.contains("SSACompositeResponse")) { 
    					ssacResponseForMember = extractSSACResponseForMember(ssn, ssacResponse);
    				} else {
    					ssacResponseForMember = null;
    				}
		        } else {
		            ssacResponseForMember = SSAPIntegrationConstants.NO_SSN;
		        }
		        
				Map<String, String> rulePayload = new HashMap<String, String>();
				rulePayload.put("ssapJson", ssapJSON);
				rulePayload.put("hubResponse", ssacResponseForMember);
				rulePayload.put("personId", member.get("personId").toString());
				if(ssacResponse != null && ssacResponse.contains("html")) {
					// rulePayload.put("rawResponse", ssacResponse);
					LOGGER.error("Error response for SSAC for member with person id: " +
							rulePayload.get("personId") +
							" (" + member.get("name") + ") => "
							+ ssacResponse);
				}
				Message<Map<String, String>> ruleMessage = MessageBuilder.withPayload(rulePayload).setHeader("Content-Type", "application/x-www-form-urlencoded").build();
				requests.add(ruleMessage);
			}
		}
		return requests.toArray(new Message[requests.size()]);
	}

	private String extractSSACResponseForMember(String ssn, String payload) throws SSAPIntegrationException {
	    String ssacResponse = null;
		net.minidev.json.JSONArray ssacResponses = JsonPath.read(payload,"$.SSACompositeResponse[?(@SSACompositeIndividualResponse.PersonSSNIdentification == '" + ssn + "')]");
		net.minidev.json.JSONObject memberSsacResponse = (net.minidev.json.JSONObject)ssacResponses.get(0);
		String responseCode = JsonPath.read(memberSsacResponse.toJSONString(),"$.SSACompositeIndividualResponse.ResponseMetadata.ResponseCode");
		if(SsapIntegrationUtil.isSuccessOrNonRetryableResponseCode(responseCode)) {
		    ssacResponse = memberSsacResponse.toJSONString(); 
		}
		return ssacResponse;
	}
}
