package com.getinsured.ssap.exception;

/**
 * Exception that signifies that verification is required for SSAP.
 *
 * @author Yevgen Golubenko
 * @since 2018-12-21
 */
public class VerificationRequiredException extends Exception implements SsapException
{
  private VerificationType type = VerificationType.NOT_SPECIFIED;

  /**
   * Constructs new exception with given message.
   * @param message Message text.
   */
  public VerificationRequiredException(String message) {
    super(message);
  }

  /**
   * Constructs new exception with given type of verification that is missing.
   * @param type {@link VerificationType}.
   */
  public VerificationRequiredException(VerificationType type) {
    this.type = type;
  }

  /**
   * Constructs new exception with given message and type of verification that is missing.
   * @param message Any message that you want to associate with this exception.
   * @param type {@link VerificationType}.
   */
  public VerificationRequiredException(String message, VerificationType type) {
    super(message);
    this.type = type;
  }

  /**
   * Returns type of verification that is required.
   * @return one of types defined in {@link VerificationType}.
   */
  public VerificationType getType() {
    return type;
  }

  public void setType(VerificationType type) {
    this.type = type;
  }
}
