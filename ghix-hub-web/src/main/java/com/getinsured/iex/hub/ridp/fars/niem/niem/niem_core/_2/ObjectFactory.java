
package com.getinsured.iex.hub.ridp.fars.niem.niem.niem_core._2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.getinsured.iex.hub.ridp.fars.niem.niem.proxy.xsd._2.Date;
import com.getinsured.iex.hub.ridp.fars.niem.niem.proxy.xsd._2.String;
import com.getinsured.iex.hub.ridp.fars.niem.niem.usps_states._2.USStateCodeType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.niem.niem.niem_core._2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LocationStateUSPostalServiceCode_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "LocationStateUSPostalServiceCode");
    private final static QName _Date_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "Date");
    private final static QName _FullTelephoneNumber_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "FullTelephoneNumber");
    private final static QName _AddressDeliveryPoint_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "AddressDeliveryPoint");
    private final static QName _DateRepresentation_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "DateRepresentation");
    private final static QName _LocationState_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "LocationState");
    private final static QName _IdentificationID_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "IdentificationID");
    private final static QName _StreetName_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "StreetName");
    private final static QName _LocationStreet_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "LocationStreet");
    private final static QName _TelephoneNumberRepresentation_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "TelephoneNumberRepresentation");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.niem.niem.niem_core._2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FullTelephoneNumberType }
     * 
     */
    public FullTelephoneNumberType createFullTelephoneNumberType() {
        return new FullTelephoneNumberType();
    }

    /**
     * Create an instance of {@link ProperNameTextType }
     * 
     */
    public ProperNameTextType createProperNameTextType() {
        return new ProperNameTextType();
    }

    /**
     * Create an instance of {@link StreetType }
     * 
     */
    public StreetType createStreetType() {
        return new StreetType();
    }

    /**
     * Create an instance of {@link PersonNameTextType }
     * 
     */
    public PersonNameTextType createPersonNameTextType() {
        return new PersonNameTextType();
    }

    /**
     * Create an instance of {@link TextType }
     * 
     */
    public TextType createTextType() {
        return new TextType();
    }

    /**
     * Create an instance of {@link TelephoneNumberType }
     * 
     */
    public TelephoneNumberType createTelephoneNumberType() {
        return new TelephoneNumberType();
    }

    /**
     * Create an instance of {@link IdentificationType }
     * 
     */
    public IdentificationType createIdentificationType() {
        return new IdentificationType();
    }

    /**
     * Create an instance of {@link DateType }
     * 
     */
    public DateType createDateType() {
        return new DateType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link USStateCodeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "LocationStateUSPostalServiceCode", substitutionHeadNamespace = "http://niem.gov/niem/niem-core/2.0", substitutionHeadName = "LocationState")
    public JAXBElement<USStateCodeType> createLocationStateUSPostalServiceCode(USStateCodeType value) {
        return new JAXBElement<USStateCodeType>(_LocationStateUSPostalServiceCode_QNAME, USStateCodeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Date }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "Date", substitutionHeadNamespace = "http://niem.gov/niem/niem-core/2.0", substitutionHeadName = "DateRepresentation")
    public JAXBElement<Date> createDate(Date value) {
        return new JAXBElement<Date>(_Date_QNAME, Date.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FullTelephoneNumberType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "FullTelephoneNumber", substitutionHeadNamespace = "http://niem.gov/niem/niem-core/2.0", substitutionHeadName = "TelephoneNumberRepresentation")
    public JAXBElement<FullTelephoneNumberType> createFullTelephoneNumber(FullTelephoneNumberType value) {
        return new JAXBElement<FullTelephoneNumberType>(_FullTelephoneNumber_QNAME, FullTelephoneNumberType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "AddressDeliveryPoint")
    public JAXBElement<Object> createAddressDeliveryPoint(Object value) {
        return new JAXBElement<Object>(_AddressDeliveryPoint_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "DateRepresentation")
    public JAXBElement<Object> createDateRepresentation(Object value) {
        return new JAXBElement<Object>(_DateRepresentation_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "LocationState")
    public JAXBElement<Object> createLocationState(Object value) {
        return new JAXBElement<Object>(_LocationState_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "IdentificationID")
    public JAXBElement<String> createIdentificationID(String value) {
        return new JAXBElement<String>(_IdentificationID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProperNameTextType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "StreetName")
    public JAXBElement<ProperNameTextType> createStreetName(ProperNameTextType value) {
        return new JAXBElement<ProperNameTextType>(_StreetName_QNAME, ProperNameTextType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StreetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "LocationStreet", substitutionHeadNamespace = "http://niem.gov/niem/niem-core/2.0", substitutionHeadName = "AddressDeliveryPoint")
    public JAXBElement<StreetType> createLocationStreet(StreetType value) {
        return new JAXBElement<StreetType>(_LocationStreet_QNAME, StreetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "TelephoneNumberRepresentation")
    public JAXBElement<Object> createTelephoneNumberRepresentation(Object value) {
        return new JAXBElement<Object>(_TelephoneNumberRepresentation_QNAME, Object.class, null, value);
    }

}
