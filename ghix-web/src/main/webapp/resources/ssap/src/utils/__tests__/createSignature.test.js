
import createSignature from '../createSignature';

describe('Test for signature creation: ', () => {

    const wrongTypes = [
        5,
        [5],
        function() {},
        null
    ];

    const testObj = {
        firstName: 'John',
        middleName: 'Fitzgerald',
        lastName: 'Kennedy',
        suffix: 'Jr.'
    };

    const testWrongObj = {
        firstName: 'John',
        lastName: 'Kennedy',
        suffix: 'Jr.'
    };

    wrongTypes.forEach(t => {
        it('Not an object has been passed', () => {
            expect(() => {
                createSignature(t);
            }).toThrow('Not an object has been passed to createSignature');
        });
    });

    it('Should throw an error of destructure', () => {
        expect(() => {
            createSignature(testWrongObj);
        }).toThrow('There is no such prop "middleName" in passed for destructure object');
    });

    it('Signature combination should match to one of these', () => {
        expect(
            createSignature(testObj)
        ).toEqual([
            `${testObj.firstName} ${testObj.lastName}`,
            `${testObj.firstName} ${testObj.middleName} ${testObj.lastName}`,
            `${testObj.firstName} ${testObj.middleName} ${testObj.lastName} ${testObj.suffix}`,
            `${testObj.firstName} ${testObj.lastName} ${testObj.suffix}`
        ]);
    });
});