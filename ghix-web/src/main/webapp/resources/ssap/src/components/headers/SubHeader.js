import React, { Fragment } from 'react';
import { FormattedMessage } from 'react-intl';

/**
 * This Component creates Subheader (in case of many usages - Subheaders) for every page
 */
const SubHeader = props => {
    const {
        additionalElement,
        additionalElementClassName,
        additionalElementStyle,
        auxClassName,
        auxText,
        headerClassName,
        text,
        textCode
    } = props;

    return (
        <Fragment>
            <div className="usa-width-one-whole page-header">
                <h4
                    className={`${headerClassName}`}
                >
                    {textCode &&
                        <FormattedMessage id={`${textCode}`} />
                    }
                    {!textCode && text} &nbsp;
                    {
                        auxText && <span className={`${auxClassName}`}> {auxText} </span>
                    }
                </h4>
                {
                    additionalElement && (
                        <span
                            className={additionalElementClassName}
                            style={additionalElementStyle}
                        >
                            {additionalElement}
                        </span>
                    )
                }
            </div>
        </Fragment>
    );
};

export default SubHeader;