package com.getinsured.eligibility.ssap.integration.ridp.transformer;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import net.minidev.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.si.handler.AptcCsrCalculatorResponse;
import com.getinsured.eligibility.prescreen.calculator.CsrCalculator;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanRequest;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanRequest.Members;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanRequest.Members.Member;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.ObjectFactory;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.jayway.jsonpath.JsonPath;

/**
 * 
 * @author choudhary_a
 * 
 */
@Component(value = "houseHoldDataTransformer")
public class HouseHoldDataTransformer {
	
	private static final Pattern APTC_MAX_AMOUNT_PATTERN = Pattern.compile("(.*)(\\\"APTCMaximumAmount\\\":\\s*)(\\d*)(.*)$");
	private static final Pattern APTC_REMAINING_PATTERN = Pattern.compile("(.*)(\\\"APTCRemainingBHCAmount\\\":\\s*\\\")(\\d*)(.*)$");

	private static Logger lOGGER = Logger.getLogger(HouseHoldDataTransformer.class);
	
	@Autowired
	private CsrCalculator csrCalculator;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	
	private static SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
	
	
	private static final Map<String , String> BLOOD_RELATIONS = new HashMap<String , String>() {{
	    put("18", "self");
	    put("01", "spouse");
	    put("09", "child");
	    put("10", "child");
	    put("17", "child");
	    put("19", "child");
	    put("53-19", "child");
	    
	}};
	
	
	public String transform(Message<String> message) throws GIException {

		lOGGER.debug("message :>" + message.getPayload());

		List<JSONObject> houseHolds = JsonPath.read(message.getPayload().toString(),"$.singleStreamlinedApplication.taxHousehold[*].householdMember[*]");

		ObjectFactory factory = new ObjectFactory();
		GetBenchmarkPlanRequest getBenchmarkPlanRequest = factory.createGetBenchmarkPlanRequest();
		//benchmark expects 10 character 
		getBenchmarkPlanRequest.setHouseholdCaseId(((String)JsonPath.read(message.getPayload().toString(),"$.singleStreamlinedApplication.applicationGuid")).substring(0, 10));
		
		SsapApplicationEvent ssapApplicationEvent =  ssapApplicationEventRepository.findEventForOpenEnrollment(Long.valueOf((String) JsonPath.read(message.getPayload().toString(),"$.singleStreamlinedApplication.ssapApplicationId")));
		if(ssapApplicationEvent == null){
			throw new GIException("Coverage Date not found for "+ (String) JsonPath.read(message.getPayload().toString(),"$.singleStreamlinedApplication.ssapApplicationId"));
		}
		Timestamp coveragets = ssapApplicationEvent.getCoverageStartDate();
        if(coveragets == null){
        	throw new GIException("Coverage Date not found for "+ (String) JsonPath.read(message.getPayload().toString(),"$.singleStreamlinedApplication.ssapApplicationId"));
        }
		Date date = new TSDate(coveragets.getTime());
        
		getBenchmarkPlanRequest.setCoverageStartDate(SIMPLE_DATE_FORMAT.format(date));// mm/dd/yyyy
		Members members = factory.createGetBenchmarkPlanRequestMembers();
		
		List<JSONObject> relations = JsonPath.read(message.getPayload().toString(),"$.singleStreamlinedApplication.taxHousehold[*].householdMember[0].bloodRelationship");
		
		String primaryZipcode = (String)((JSONObject)((JSONObject) houseHolds.get(0).get("householdContact")).get("homeAddress")).get("postalCode");
		String primaryCountycode = (String)((JSONObject)((JSONObject) houseHolds.get(0).get("householdContact")).get("homeAddress")).get("primaryAddressCountyFipsCode");

		for (JSONObject houseHold : houseHolds) {
		
			String  personId = ((Integer) houseHold.get("personId")).toString(); 	
			String relationShip = "dependant";
			for(JSONObject relation : relations){
				String  relatedPersonId =  relation.get("relatedPersonId").toString(); 	
				if(relatedPersonId.equals(personId)){
					String realtionKey = (String) relation.get("relation");
					String individualPersonIdKey =  relation.get("individualPersonId").toString();
					if ("1".equals(individualPersonIdKey) && BLOOD_RELATIONS.containsKey(realtionKey) ){
						relationShip = BLOOD_RELATIONS.get(realtionKey);
					} 
					break;
				}
			}
			Member member = factory.createGetBenchmarkPlanRequestMembersMember();
			JSONObject houseHoldContact = checkNull((JSONObject) houseHold.get("householdContact"));
			JSONObject homeAddress = checkNull((JSONObject) houseHoldContact.get("homeAddress"));
			String inputDateStr = houseHold.get("dateOfBirth").toString();
			
			if((boolean)houseHold.get("livesWithHouseholdContactIndicator")){
				if (StringUtils.isEmpty(homeAddress.get("primaryAddressCountyFipsCode").toString()) 
					|| StringUtils.isEmpty(inputDateStr)
					|| StringUtils.isEmpty(homeAddress.get("postalCode").toString()))
				{
					throw new GIException("Invalid JSON received. countyCode dateOfBirth postalCode are missing.");
				}
				member.setCountyCode((String) homeAddress.get("primaryAddressCountyFipsCode"));
				member.setZip((String) homeAddress.get("postalCode"));
			}else{
				member.setCountyCode(primaryCountycode);
				member.setZip(primaryZipcode);
			}
			
			member.setDob(changeDateFormatString(inputDateStr));// mm/dd/yyyy
			// TODO how to identify the relation
			member.setRelation(relationShip);
			member.setTobacco(((boolean) houseHold.get("tobaccoUserIndicator")) ? "Y": "N");
			members.getMember().add(member);
		}
		getBenchmarkPlanRequest.setMembers(members);

		lOGGER.debug("starting transformation");
		StringWriter st = new StringWriter();
		try {
			JAXBContext jc = JAXBContext
					.newInstance("com.getinsured.hix.webservice.planmgmt.benchmarkplan");
			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(getBenchmarkPlanRequest, st);
		} catch (Exception e) {
			lOGGER.error(e.getMessage(), e);
			throw new GIException(e);
		}
		lOGGER.debug(" tarnsformed string" + st.toString());
		return st.toString();
	}

/*	public String createSSAPApplicationUpdateRequest(Message<String> message)
			throws GIException {

		
		String aPTCMaximumAmount = null;
		String aPTCRemainingBHCAmount = null;
		double fpl = (Double)message.getHeaders().get("FPL");
		boolean isNativeAmericanHousehold = (Boolean)message.getHeaders().get("IS_NATIVE_AMERICAN_FAMILY");
		Pattern p = Pattern
				.compile("(.*)(\\\"APTCMaximumAmount\\\":\\s*)(\\d*)(.*)$");
		Matcher m = p.matcher(message.getPayload());
		if (m.matches()) {
			aPTCMaximumAmount = m.group(3);
		}
		p = Pattern
				.compile("(.*)(\\\"APTCRemainingBHCAmount\\\":\\s*\\\")(\\d*)(.*)$");
		m = p.matcher(message.getPayload());
		if (m.matches()) {
			aPTCRemainingBHCAmount = m.group(3);
		}

		return "{\"maximumAPTC\": " + aPTCMaximumAmount + ",\"electedAPTC\": "
				+ aPTCRemainingBHCAmount + ",\"csr\": \"" + csrCalculator.computeCSR(fpl, isNativeAmericanHousehold)+ "\"}";
	}
*/
	
	public AptcCsrCalculatorResponse createSSAPApplicationUpdateRequest(Message<String> message)
			throws GIException {

		AptcCsrCalculatorResponse aptcCsrCalculatorResponse = new AptcCsrCalculatorResponse();		
		String aPTCMaximumAmount = null;
		String aPTCRemainingBHCAmount = null;
		double fpl = Double.valueOf(message.getHeaders().get("FPL").toString());
		boolean isNativeAmericanHousehold = (Boolean)message.getHeaders().get("IS_NATIVE_AMERICAN_FAMILY") == null ? false : (Boolean)message.getHeaders().get("IS_NATIVE_AMERICAN_FAMILY");
		
		Matcher m = APTC_MAX_AMOUNT_PATTERN.matcher(message.getPayload());
		if (m.matches()) {
			aPTCMaximumAmount = m.group(3);
		}
		
		m = APTC_REMAINING_PATTERN.matcher(message.getPayload());
		if (m.matches()) {
			aPTCRemainingBHCAmount = m.group(3);
		}
		
		aptcCsrCalculatorResponse.setAvailableAPTCAmount(StringUtils.isNumeric(aPTCRemainingBHCAmount) ? new BigDecimal(aPTCRemainingBHCAmount) : null);
		aptcCsrCalculatorResponse.setMaxAPTCAmount(StringUtils.isNumeric(aPTCMaximumAmount)? new BigDecimal(aPTCMaximumAmount) : null);
		aptcCsrCalculatorResponse.setCsrLevel(csrCalculator.computeCSR(fpl, isNativeAmericanHousehold));
		aptcCsrCalculatorResponse.setNativeAmerican(isNativeAmericanHousehold);
		aptcCsrCalculatorResponse.setFplAmount(new BigDecimal(fpl));
		return aptcCsrCalculatorResponse;
	}

	/**
	 * checks null or throw exception
	 * 
	 * @param jsonObj
	 * @return
	 * @throws GIException
	 */
	private JSONObject checkNull(JSONObject jsonObj) throws GIException {

		if (jsonObj == null) {
			GIException giex = new GIException();
			lOGGER.error(" Attribute cannot be empty or null ", giex);
			throw giex;
		} else {
			return jsonObj;
		}

	}

	public String changeDateFormatString(String inputDateStr) {
		final String OLD_FORMAT = "MMM dd, yyyy hh:ww:ss";
		final String NEW_FORMAT = "MM/dd/yyyy";

		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date d = null;
		try {
			d = sdf.parse(inputDateStr);
		} catch (Exception e) {
			lOGGER.error(e);
		}

		sdf.applyPattern(NEW_FORMAT);
		return sdf.format(d);
	}
}
