<!doctype html>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>

<link rel="stylesheet" href="//cdn.rawgit.com/angular-ui/bower-ui-grid/master/ui-grid.min.css">

<style>
.ui-grid-pager-control input {
    height: 14px;
}

.ui-grid-pager-control .ui-grid-pager-max-pages-number {
    vertical-align: middle;
}

#containsOfRequestArea {
  width: 95%;
  height: 95%;
}

#containsOfResponseArea {
  width: 95%;
  height: 95%;
}

.grid-left-align {
    text-align: left;
}
</style>

<script type="text/javascript" src='<c:url value="/resources/js/angular-animate.js" />'></script>
<script type="text/javascript" src='<c:url value="/resources/js/angular-touch.js" />'></script>
<script type="text/javascript" src='<c:url value="/resources/js/ui-grid.js" />'></script>

<script src="<gi:cdnurl value="/resources/js/lce/ui-bootstrap-tpls-0.11.0.js"/>"></script>
<script src="<gi:cdnurl value="/resources/js/dashboardAnnouncement/angular-ui-router.min.js"/>"></script>

<script src='<c:url value="/resources/js/planMgmt/serffTransferStatusList.js" />'></script>

<script type="text/javascript">
	$(document).ready(function() {
		var scope = angular.element(document.getElementById("planLoadStatusListDiv")).scope();
			scope.$apply(function () {
			scope.setIssuerList(${issuerList});
			scope.setStatusList(${serffTransferList});
		});
	});
</script>

<div class="gutter10" id="planLoadStatusListDiv" ng-app="planLoadStatusListApp" ng-controller="PlanLoadStatusListCtrl">

	<div class="row-fluid">

		<div class="row-fluid">
			<h1><spring:message  code="label.header.serffTransferStatus"/></h1>
		</div>

		<div class="row-fluid">
			<form class="form-vertical gutter10 lightgray" id="frmPlanLoadStatus" name="frmPlanLoadStatus" action="<c:url value="/admin/serff/serffTransferStatus" />" method="POST">
				<df:csrfToken/>

				<div class="control-group span3">
					<label for="issuerFilter" class="control-label"><spring:message  code="label.issuer"/></label>
					<div class="controls">
						<select name="issuerFilter" ng-model="issuerFilter" onchange="$('#selectedIssuer').val(this.options[this.selectedIndex].value);">
							<option value=""><spring:message  code="label.selectIssuer"/></option>
							<option ng-repeat="issuer in issuerList" value="{{issuer.hiosIssuerId}}">
								[{{issuer.hiosIssuerId}}] {{issuer.shortName}}
							</option>
						</select>
						<input type="hidden" name="selectedIssuer" id="selectedIssuer" value="${selectedIssuer}" />
					</div> <!-- end of controls-->
				</div> <!-- end of control-group -->

				<div class="control-group span3">
					<label for="statusFilter" class="control-label"><spring:message  code="label.status"/></label>
					<div class="controls">
						<select name="statusFilter" ng-model="statusFilter" onchange="$('#selectedStatus').val(this.options[this.selectedIndex].value);">
							<option value=""><spring:message  code="label.selectStatus"/></option>
							<option ng-repeat="status in statusList" value="{{status.key}}">
								{{status.value}}
							</option>
						</select>
						<input type="hidden" name="selectedStatus" id="selectedStatus" value="${selectedStatus}" />
					</div> <!-- end of controls-->
				</div> <!-- end of control-group -->

				<div class="control-group span2">
					<label for="dateFilter" class="control-label"><spring:message  code="label.date"/></label>
					<div class="controls">
						<input type="text" name="dateFilter" id="dateFilter" onchange="$('#selectedDate').val(this.value);" value="${selectedDate}" maxlength="10" size="10" class="span8">
						<input type="hidden" name="selectedDate" id="selectedDate" value="${selectedDate}" />
					</div> <!-- end of controls-->
				</div> <!-- end of control-group -->

				<div class="control-group span2">
					&nbsp;
					<div class="controls"> &nbsp;&nbsp;
						<input type="button" class="btn" ng-click="resetFilter()" id="ResetFilter" name="ResetFilter" value="<spring:message code='label.btnReset' />" title="<spring:message code='label.btnReset' />" />
						<input type="submit" class="btn btn-primary" value="<spring:message code='label.go' />" title="<spring:message  code='label.go' />">
					</div>
				</div> <!-- end of control-group -->
			</form>
		</div>
	</div><!--  end of row-fluid -->

   	<div>
		<div ui-grid="gridOptions" style="height: 100%" class="grid" ui-grid-pagination ui-grid-resize-columns></div>
	</div>

	<div id="modalContainer">
		<!-- Modal type="text/ng-template" -->
		<div class="modal hide fade in" id="requestModal" ng-style="displayNone">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><spring:message code="label.header.serffTemplateRequest"/></h4>
					</div>
					<div class="modal-body">
						<textarea id="containsOfRequestArea" name="containsOfRequestArea" rows="20" cols="100"></textarea>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal hide fade in" id="responseModal" ng-style="displayNone">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><spring:message code="label.header.serffTemplateResponse"/></h4>
					</div>
					<div class="modal-body">
						<textarea id="containsOfResponseArea" name="containsOfResponseArea" rows="20" cols="100"></textarea>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="attachmentsModal" ng-style="displayNone">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><spring:message code="label.header.serffAttachmentsDetail"/></h4>
					</div>
					<div class="modal-body">
						<div ui-grid="gridOptionsForAttachments" class="grid" ui-grid-resize-columns></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
