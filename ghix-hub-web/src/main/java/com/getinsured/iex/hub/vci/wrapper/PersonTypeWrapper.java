package com.getinsured.iex.hub.vci.wrapper;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;

import org.apache.xerces.dom.ElementNSImpl;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.SSNRestrictionType;
import com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.ObjectFactory;
import com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.PersonNameTextType;
import com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.PersonType;
import com.getinsured.iex.hub.vci.gov.niem.niem.proxy.xsd._2.Date;

/**
 * Wrapper com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.PersonType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 *
 */
public class PersonTypeWrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonTypeWrapper.class);
	
	private com.getinsured.iex.hub.vci.gov.niem.niem.proxy.xsd._2.ObjectFactory proxyFactory; 
	private com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.ObjectFactory niemCoreFactory;
	
	private PersonType person;
	
	private String personBirthDate;
	private String personGivenName;
	private String personMiddleName;
	private String personSurName;
	private String personSSNIdentification;
	
	private static final String DOB_KEY = "personBirthDate";
	private static final String FIRST_NAME_KEY = "personGivenName";
	private static final String MIDDLE_NAME_KEY = "personMiddleName";
	private static final String LAST_NAME_KEY = "personSurName";
	private static final String SSN_KEY = "personSSNIdentification";
	
	public PersonTypeWrapper(){
		proxyFactory = new com.getinsured.iex.hub.vci.gov.niem.niem.proxy.xsd._2.ObjectFactory();
		niemCoreFactory = new ObjectFactory();
		this.person = niemCoreFactory.createPersonType();
		this.person.setPersonName(niemCoreFactory.createPersonNameType());
		this.person.setPersonSSNIdentification(niemCoreFactory.createIdentificationType());
	}
	
	public PersonTypeWrapper(PersonType person){
		
		if(person == null){
			return;
		}
		
		if(person.getPersonBirthDate() != null 
				&& person.getPersonBirthDate().getDateRepresentation() != null){
			Date dob = (Date) person.getPersonBirthDate().getDateRepresentation().getValue();
			this.personBirthDate = dob.getValue().toString();
		}
		
		if(person.getPersonName() != null){
			this.personGivenName = person.getPersonName().getPersonGivenName().getValue();
			this.personSurName =  person.getPersonName().getPersonSurName().getValue();
			if(person.getPersonName().getPersonMiddleName() != null){
				this.personMiddleName = person.getPersonName().getPersonMiddleName().getValue();
			}
		}
		
		if(person.getPersonSSNIdentification() != null){
			ElementNSImpl ssn = (ElementNSImpl)person.getPersonSSNIdentification();
			this.personSSNIdentification = ssn.getFirstChild().getTextContent();
		}
	}

	public PersonType getPerson() {
		return person;
	}

	public String getPersonBirthDate() {
		return personBirthDate;
	}

	public void setPersonBirthDate(String personBirthDate) throws HubServiceException {
		
		try{
			if(personBirthDate != null && personBirthDate.trim().length() > 0){
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				sdf.setLenient(false);
				java.util.Date javaDate = sdf.parse(personBirthDate);
	
				com.getinsured.iex.hub.vci.gov.niem.niem.proxy.xsd._2.Date date = proxyFactory.createDate();
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(javaDate);
				date.setValue(DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH)+1, gc.get(Calendar.DAY_OF_MONTH),DatatypeConstants.FIELD_UNDEFINED));
				
				JAXBElement<com.getinsured.iex.hub.vci.gov.niem.niem.proxy.xsd._2.Date> dob = niemCoreFactory.createDate(date);
				DateType value = niemCoreFactory.createDateType();
				value.setDateRepresentation(dob);
	
				this.personBirthDate = personBirthDate;
				this.person.setPersonBirthDate(value);
			}
		}
		catch(Exception he){
			LOGGER.error("Failed to set Date of birth",he);
			throw new HubServiceException("Failed to create household information",he);
		}	
		
	}

	public String getPersonGivenName() {
		return personGivenName;
	}

	public void setPersonGivenName(String personGivenName) {
		if(personGivenName != null && personGivenName.trim().length() > 0){
			PersonNameTextType firstName = niemCoreFactory.createPersonNameTextType();
			firstName.setValue(personGivenName);
			this.personGivenName = personGivenName;
			this.person.getPersonName().setPersonGivenName(firstName);
		}
	}

	public String getPersonMiddleName() {
		return personMiddleName;
	}

	public void setPersonMiddleName(String personMiddleName) {
		if(personMiddleName != null && personMiddleName.trim().length() > 0){
			PersonNameTextType middleName = niemCoreFactory.createPersonNameTextType();
			middleName.setValue(personMiddleName);
			this.personMiddleName = personMiddleName;
			this.person.getPersonName().setPersonMiddleName(middleName);
		}
	}

	public String getPersonSurName() {
		return personSurName;
	}

	public void setPersonSurName(String personSurName) {
		if(personSurName != null && personSurName.trim().length() > 0){
			PersonNameTextType lastName = niemCoreFactory.createPersonNameTextType();
			lastName.setValue(personSurName);
			this.personSurName = personSurName;
			this.person.getPersonName().setPersonSurName(lastName);
		}
	}

	public String getPersonSSNIdentification() {
		return personSSNIdentification;
	}

	public void setPersonSSNIdentification(String personSSNIdentification) {
		com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.ObjectFactory extnFactory = new com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.ObjectFactory();
		SSNRestrictionType ssn = extnFactory.createSSNRestrictionType();
		ssn.setValue(personSSNIdentification);
		this.personSSNIdentification = personSSNIdentification;
		this.person.setPersonSSNIdentification(ssn);
	}

	/**
	 * Converts the JSON String to PersonTypeWrapper object
	 * 
	 * @param jsonString - String representation for PersonTypeWrapper
	 * @return PersonTypeWrapper Object
	 * @throws DatatypeConfigurationException 
	 * @throws ParseException 
	 */
	public static PersonTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create PersonTypeWrapper from null or empty input");
		}

		Object tmpObj = null;
		
		PersonTypeWrapper wrapper = new PersonTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		tmpObj = obj.get(DOB_KEY);
		if(tmpObj != null){
			wrapper.personBirthDate = (String)tmpObj;
			wrapper.setPersonBirthDate(wrapper.personBirthDate);
		}
		
		tmpObj = obj.get(FIRST_NAME_KEY);
		if(tmpObj != null){
			wrapper.personGivenName = (String)tmpObj;
			wrapper.setPersonGivenName(wrapper.personGivenName);
		}
		
		tmpObj = obj.get(MIDDLE_NAME_KEY);
		if(tmpObj != null){
			wrapper.personMiddleName = (String)tmpObj;
			wrapper.setPersonMiddleName(wrapper.personMiddleName);
		}
		
		tmpObj = obj.get(LAST_NAME_KEY);
		if(tmpObj != null){
			wrapper.personSurName = (String)tmpObj;
			wrapper.setPersonSurName(wrapper.personSurName);
		}
		
		tmpObj = obj.get(SSN_KEY);
		if(tmpObj != null){
			wrapper.personSSNIdentification = (String)tmpObj;
			wrapper.setPersonGivenName(wrapper.personSSNIdentification);
		}
		
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(DOB_KEY, this.personBirthDate);
		obj.put(FIRST_NAME_KEY, this.personGivenName);
		obj.put(MIDDLE_NAME_KEY, this.personMiddleName);
		obj.put(LAST_NAME_KEY, this.personSurName);
		obj.put(SSN_KEY, this.personSSNIdentification);
		return obj.toJSONString();
	}
	
	public static PersonTypeWrapper fromJSONObject(JSONObject jsonObj) throws HubServiceException {
		PersonTypeWrapper wrapper = new PersonTypeWrapper();
		wrapper.setPersonBirthDate((String)jsonObj.get(DOB_KEY));
		wrapper.setPersonGivenName((String) jsonObj.get(FIRST_NAME_KEY));
		wrapper.setPersonMiddleName((String) jsonObj.get(MIDDLE_NAME_KEY));
		wrapper.setPersonSurName((String) jsonObj.get(LAST_NAME_KEY));
		wrapper.setPersonGivenName((String) jsonObj.get(SSN_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(DOB_KEY, this.personBirthDate);
		obj.put(FIRST_NAME_KEY, this.personGivenName);
		obj.put(MIDDLE_NAME_KEY, this.personMiddleName);
		obj.put(LAST_NAME_KEY, this.personSurName);
		obj.put(SSN_KEY, this.personSSNIdentification);
		return obj;
	}

}
