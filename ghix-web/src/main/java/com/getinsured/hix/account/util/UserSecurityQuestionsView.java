package com.getinsured.hix.account.util;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;
import org.owasp.esapi.ESAPI;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.SecurityQuestionsHelper;
import com.getinsured.hix.platform.util.SecurityQuestionsHelper.QuestionSet;

/*
 *  Enhanced to support configurable no of security questions
 *  HIX-46480 : Configuration to set the number of SecurityQuestion Sets ; 
 *  -Venkata Tadepalli
 *  
 */
public class UserSecurityQuestionsView extends TagSupport {
	
	//@Autowired private UserRepository repository;
	
	private static final long serialVersionUID = 1L;

	private ApplicationContext applicationContext;
	
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	private JspWriter out;
	
	private String  redirectURL;
	
	private String submitRedirectURL;

	private String[] userSecurityQuestion = new String[3];
	private String[] userSecurityAnswer = new String[3];

	
	private int noOfSecQtns = 2; // Default value
	
	//HIX-52205 - Content change for security questions lightbox
	private String tmpltFormStart_single_question = "<form id='frmsecurityquesions' name='frmsecurityquesions' action='appContextPath/account/user/securityquestions' method='POST' role='form'>"
			+ "<input type='hidden' id='submitRedirectURL2' name='submitRedirectURL' value='submitRedirectURLValue' disabled='true'>"
			+ "<df:csrfToken/>"
			+ "<div class='modal-body'>"
			+ "<p>This question adds an extra layer of security to your account. We may ask you to provide an answer to this question to verify your security in certain specific situations.</p><p>All fields on this form marked with an asterisk (*) are required unless otherwise indicated.</p>"
			;
			

	//HIX-52205 - Content change for security questions lightbox
	private String tmpltFormStart_multiple_questions = "<form id='frmsecurityquesions' name='frmsecurityquesions' action='appContextPath/account/user/securityquestions' method='POST' role='form'>"
			+ "<input type='hidden' id='submitRedirectURL2' name='submitRedirectURL' value='submitRedirectURLValue' disabled='true'>"
			+ "<df:csrfToken/>"
			+ "<div class='modal-body'>"
			+ "<p>These questions add an extra layer of security to your account. We may ask you to provide answers to these questions to verify your security in certain specific situations.</p><p>All fields on this form marked with an asterisk (*) are required unless otherwise indicated.</p>";

	private String tmpltFormQtn =
			"<div class='control-group row-fluid'>"
			 +"<div class='col-sm-3 col-md-3 col-lg-3'>"
	            +"<label for='securityQuestion1' class='required control-label'>Security question 1: <img src='appContextPath/resources/images/requiredAsterix.png' width='10' height='10' alt='Required!' /></label>"
	             +"</div>"
	             +"<div class='col-sm-9 col-md-9 col-lg-9'>"
	            +"<div class='controls'>"
								+"<select  id='securityQuestion1' name='securityQuestion1' path='securityQuestionsList1' onchange='javascript:clearAnswerOne();'>"
								+"	questionSet1"
								+"</select>"
								+"<div id='securityQuestion1_error'></div>"
	+"            </div><!-- end of controls-->"
	+"            </div>"
	+"      </div><!-- /form-group 1 - Question1 -->";
	private String tmpltFormAns =
			"<div class='control-group security-ans row-fluid'>"
			 +"<div class='col-sm-3 col-md-3 col-lg-3'>"
				+"<label for='securityAnswer1' class='control-label required'>Answer </label></div>"
				+"<div class='col-sm-9 col-md-9 col-lg-9'>"
				+"<div class='controls'>"
				+"	<input type='password' name='securityAnswer1' id='securityAnswer1' value='answerValue1'/>"
				+"	<div id='securityAnswer1_error'></div>"
				+"</div>	<!-- end of controls-->"
				+"</div>"
	+"		</div>	<!-- end of control-group -->";
	        
						
	private String tmpltFormEnd = 
			"    </div><!-- /modal-body -->"
	        +"<div class='modal-footer'>"
	        +"        <button class='btn pull-left 'id='cancelbutton2' name='cancelbutton' onClick=\"return goToPage(appContextPath/redirectURL);\">Cancel</button>    "
	+"		  <input type='button'  name='changePwdSave' id='changePwdSave' aria-hidden='true' onClick='javascript:checkUserCurrentPassword();' value='Save'	class='btn btn-primary pull-right' style='margin:0px;' />"
	+"</div>"
	+"</form>";
	

private String tmpltCurrentPwd =  "<div class='control-group row-fluid'>"
+"<div class='col-sm-3 col-md-3 col-lg-3'>"
+"<label for='currentpswd' class='required control-label'>Current Password<img src='appContextPath/resources/images/requiredAsterix.png' width='10' height='10' alt='Required!' /></label>"
+"</div>"
+"<div class='col-sm-9 col-md-9 col-lg-9'>"
+"<div class='controls'>"
+"<input type='password' class='form-control' id='currentpswdans' name='currentpswdans' onkeypress='resetErrorMsg(event, this)' value=''/>"
+"<div id='currentpswdans_error' class=''></div>"
+"</div>"
+"</div>"
+"</div>";

	
	public String getRedirectURL() {
		return redirectURL;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	public String getSubmitRedirectURL() {
		return submitRedirectURL;
	}

	public void setSubmitRedirectURL(String submitRedirectURL) {
		this.submitRedirectURL = submitRedirectURL;
	}

	
	public String getTmpltFormQtn() {
		return tmpltFormQtn;
	}

	public void setTmpltFormQtn(String tmpltFormQtn) {
		this.tmpltFormQtn = tmpltFormQtn;
	}
	
	
	public String getTmpltCurrentPwd() {
		return tmpltCurrentPwd;
	}

	public void setTmpltCurrentPwd(String tmpltCurrentPwd) {
		this.tmpltCurrentPwd = tmpltCurrentPwd;
	}

	public String getTmpltFormAns() {
		return tmpltFormAns;
	}

	public void setTmpltFormAns(String tmpltFormAns) {
		this.tmpltFormAns = tmpltFormAns;
	}

	private String getQuestionDiv(int idx) {
		
		//HIX-52205 - Content change for security questions lightbox
    		StringBuilder tmpltFormQtn = new StringBuilder("<div class='control-group row-fluid'><div class='col-sm-3 col-md-3 col-lg-3 '>");

                if (noOfSecQtns == 1) {
                        tmpltFormQtn.append("<label for='securityQuestion${IDX}' class='control-label'>Security question<img src='appContextPath/resources/images/requiredAsterix.png' width='10' height='10' alt='Required!' /></label>");
                } else {
                        tmpltFormQtn.append("<label for='securityQuestion${IDX}' class='control-label'>Security question ${IDX}: <img src='appContextPath/resources/images/requiredAsterix.png' width='10' height='10' alt='Required!' /></label>");
                }
    		tmpltFormQtn.append("</div>");

    		tmpltFormQtn.append("<div class='col-sm-9 col-md-9 col-lg-9'><div class='controls'>");
    		tmpltFormQtn.append("<select  id='securityQuestion${IDX}' name='securityQuestion${IDX}' path='securityQuestionsList${IDX}' class='form-control' onchange='javascript:clearAnswer(${IDX});'>");
    		tmpltFormQtn.append("questionSet${IDX}").append("</select>");
    		tmpltFormQtn.append("<div id='securityQuestion${IDX}_error'></div>");
    		tmpltFormQtn.append("</div><!-- end of controls-->");
    		tmpltFormQtn.append("</div>");
    		tmpltFormQtn.append("</div><!-- /form-group ${IDX} - Question${IDX} -->");

    		String result = StringUtils.replace(tmpltFormQtn.toString(), "${IDX}", idx + "");
    		return result;
	}
	
	private String getAnswerDiv(int idx){
		String tmpltFormAns =
				"<div class='control-group security-ans'>"
				    +"<div class='col-sm-3 col-md-3 col-lg-3'>"
					+"<label for='securityAnswer${IDX}' class='control-label required'>Answer</label>"
					+"</div>"
					+"<div class='col-sm-9 col-md-9 col-lg-9'>"
					+"<div class='controls'>"
					+"	<input type='password' name='securityAnswer${IDX}' id='securityAnswer${IDX}' class='form-control' value='answerValue${IDX}'/>"
					+"	<div id='securityAnswer${IDX}_error'></div>"
					+"</div>	<!-- end of controls-->"
					+"</div>"
		+"		</div>	<!-- end of control-group -->";
		
		tmpltFormAns = StringUtils.replace(tmpltFormAns, "${IDX}", idx+"");
		
		return tmpltFormAns;
	}
	
	@Override
	public int doStartTag() throws JspException {
		this.setProperties();
		applicationContext = RequestContextUtils.getWebApplicationContext(
				pageContext.getRequest(),
				pageContext.getServletContext()
			);

		UserService userService = applicationContext.getBean("userService",UserService.class);
		UserRepository repository = applicationContext.getBean("userRepository",UserRepository.class);
		
		try {
				//AccountUser domainUser = userService.getLoggedInUser();
			    // get from the user object from the database
				AccountUser domainUser = repository.findByUserName(userService.getLoggedInUser().getUsername());
				StringBuffer tempTemplate=new StringBuffer();
				
				String strNoOfSecQtns = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.NO_OF_SECURITY_QUESTIONS.getValue());
				
				if(StringUtils.isNotBlank(strNoOfSecQtns)){
					int tempNoOfSecQtns = Integer.parseInt(strNoOfSecQtns);
					
					if(tempNoOfSecQtns>0 && tempNoOfSecQtns<4){ //The no of questions should be between 1 -3
						noOfSecQtns = tempNoOfSecQtns;
					}
					
				}
				
				//HIX-52205 - Content change for security questions lightbox
				if(noOfSecQtns > 1){
					tempTemplate = tempTemplate.append(this.tmpltFormStart_multiple_questions);
				}
				else{
					tempTemplate = tempTemplate.append(this.tmpltFormStart_single_question);
				}
				
				tempTemplate = tempTemplate.append(this.tmpltCurrentPwd);
				
				for(int secQtnIdx=1;secQtnIdx<=noOfSecQtns;secQtnIdx++){
					userSecurityQuestion[secQtnIdx-1] = domainUser.getSecurityQuestion(secQtnIdx);
					userSecurityAnswer[secQtnIdx-1] = domainUser.getSecurityAnswer(secQtnIdx);
					
					String tempQuestionTemplate = this.getQuestionDiv(secQtnIdx);
					tempQuestionTemplate = tempQuestionTemplate.replace("questionSet"+secQtnIdx,getQuestionSet(secQtnIdx));
					tempTemplate = tempTemplate.append(tempQuestionTemplate);
					
					String tempAnswerTemplate = this.getAnswerDiv(secQtnIdx);
					tempAnswerTemplate = tempAnswerTemplate.replace("answerValue"+secQtnIdx, ESAPI.encoder().encodeForHTMLAttribute(userSecurityAnswer[secQtnIdx-1]));
					tempTemplate = tempTemplate.append(tempAnswerTemplate);

					
				}
				
				
				tempTemplate = tempTemplate.append(this.tmpltFormEnd);
				
				String returnTeamplate = tempTemplate.toString();
				
				String contextPath = ((HttpServletRequest)pageContext.getRequest()).getContextPath();
				returnTeamplate = returnTeamplate.replace("appContextPath/redirectURL", "'"+contextPath+redirectURL.trim()+"'");
				if(null!=submitRedirectURL){
					ghixJasyptEncrytorUtil  = (GhixJasyptEncrytorUtil)applicationContext.getBean("ghixJasyptEncrytorUtil");
					String url =ghixJasyptEncrytorUtil.encryptStringByJasypt(submitRedirectURL.trim());
					returnTeamplate = returnTeamplate.replace("submitRedirectURLValue", url);
				}
				else{
					returnTeamplate = returnTeamplate.replace("<input type='hidden' id='submitRedirectURL2' name='submitRedirectURL' value='submitRedirectURLValue' disabled='true'>", "");
				}
				String token = (String) ((HttpServletRequest) pageContext.getRequest()).getSession().getAttribute("csrftoken");
				returnTeamplate = returnTeamplate.replace("<df:csrfToken/>", "<input type='hidden' id='csrftoken' name='csrftoken' value='"+token+"'>");
				returnTeamplate = returnTeamplate.replace("appContextPath",contextPath);
	            this.out.println(returnTeamplate);
	        } catch (Exception ex) {
	            throw new JspException("Error in UserSecurityQuestionsView tag", ex);
	        }
	    return SKIP_BODY;
	}

	private void setProperties(){
    	this.out     = pageContext.getOut();
	}

	private String getQuestionSet(int Idx) {
		List<QuestionSet> securityQuestionsList;
		
		securityQuestionsList = new SecurityQuestionsHelper().getQuestionSet(Idx);
		int noOfQtns = securityQuestionsList.size();
		
		String selectStr = "";
		
		for(int i=0; i<noOfQtns; i++){
			if(securityQuestionsList.get(i).getName().equalsIgnoreCase(userSecurityQuestion[Idx-1])){
				selectStr += "<option value=\""+securityQuestionsList.get(i).getName()+"\" selected>"+securityQuestionsList.get(i).getName()+"</option>";
			}
			else{
				selectStr += "<option value=\""+securityQuestionsList.get(i).getName()+"\">"+securityQuestionsList.get(i).getName()+"</option>";
			}
		}
			return selectStr;
	}
	

}
