package com.getinsured.iex.hub.consumers.hub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.rabbitmq.client.Channel;

public class APCQueueConsumer extends MessageProcessor implements ChannelAwareMessageListener {

	@Autowired
	private WebServiceTemplate apcServiceTemplate;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(APCQueueConsumer.class);
	
	
	
	private void invokeAPC() throws HubMappingException,
			BridgeException, HubServiceException, InputDataValidationException {
	
		Exception ex = null;
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		String apcRequestJSON = " { \"Income\" : { \"incomeAmount\" : \"50.00\",      \"incomeDate\" : \"2014\",      \"incomeFederalPovertyLevelPercent\" : \"49.50\"    },  \"SLCSPPremium\" : { \"insurancePremiumAmount\" : \"50.50\" },  \"requestID\" : \"500000000\"}";
		HubServiceBridge nesiMecBridge = HubServiceBridge.getHubServiceBridge("APCRequest");
		handler = nesiMecBridge.getServiceHandler();
		handler.setJsonInput(apcRequestJSON);
		
		Object payLoad = handler.getRequestpayLoad();
		if(payLoad != null)
		{
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("APTC");
			requestRecord.setEndpointOperationName("APTC");
			response = this.executeRequest(requestRecord, handler, apcServiceTemplate);
		}	
		
	}
	@Override
	public void onMessage(Message message, Channel channel) throws BridgeException  {
		String threadId = Thread.currentThread().getName();
		String routingKey = message.getMessageProperties().getReceivedRoutingKey();
		if(routingKey == null){
			throw new IllegalArgumentException("No routing key available for incoming IFSV message, don't know how to process it");
		}
		LOGGER.info("Thread[" + threadId + "]Routing Key:"
				+ routingKey
				+ " Received Message:" + new String(message.getBody())
				+ " from channel:" + channel.getChannelNumber());

		try {
			invokeAPC();
		} catch (SoapFaultClientException e) {
			LOGGER.error("Request failed with Reason:"
					+ e.getFaultStringOrReason() + " message:"
					+e.getMessage()
					+ "And fault code:"+e.getFaultCode(),e);
		} catch (HubServiceException e) {
			LOGGER.info("Exception", e);
		} catch (HubMappingException e) {
			LOGGER.info("Exception", e);
		} catch (InputDataValidationException ie) {
			LOGGER.info("Exception", ie);
		}
	}
		
}
