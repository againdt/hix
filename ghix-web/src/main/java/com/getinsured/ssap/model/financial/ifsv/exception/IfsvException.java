package com.getinsured.ssap.model.financial.ifsv.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Describes IFSV exception.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-12
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IfsvException {
  @JsonProperty("Exception Type")
  private String exceptionType;

  @JsonProperty("ExceptionMessage")
  private String exceptionMessage;

  @JsonProperty("ExceptionCause")
  private IfsvExceptionCause exceptionCause;

  public String getExceptionType() {
    return exceptionType;
  }

  public void setExceptionType(String exceptionType) {
    this.exceptionType = exceptionType;
  }

  public String getExceptionMessage() {
    return exceptionMessage;
  }

  public void setExceptionMessage(String exceptionMessage) {
    this.exceptionMessage = exceptionMessage;
  }

  public IfsvExceptionCause getExceptionCause() {
    return exceptionCause;
  }

  public void setExceptionCause(IfsvExceptionCause exceptionCause) {
    this.exceptionCause = exceptionCause;
  }

  @Override
  public String toString() {
    return "IfsvException{" +
        "exceptionType='" + exceptionType + '\'' +
        ", exceptionMessage='" + exceptionMessage + '\'' +
        ", exceptionCause=" + exceptionCause +
        '}';
  }
}
