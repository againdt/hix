package com.getinsured.hix.planmgmt.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.platform.accountactivation.repository.IGHIXCustomRepository;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;

/**
 * 
 * @author sharma_k
 * @since 05th June 2013
 * Repository class created for IssuerRepresentative verification process.
 *
 */
@Repository
@Transactional(readOnly = true)
public class IssuerRepresentativeRepository implements IGHIXCustomRepository<IssuerRepresentative, Integer>
{

		@Autowired
		private IIssuerRepresentativeRepository iIssuerRepresentativeRepository;
		@Autowired private UserService userService;

		@Override
		public boolean recordExists(Integer id) 
		{
			boolean isRecordPresent = false;
			
			if(id == 0) {
				isRecordPresent = true;
			} else {
				isRecordPresent = iIssuerRepresentativeRepository.exists(id);
			}
			
			return isRecordPresent;
		}

		@Override
		@Transactional(readOnly=false)
		public void updateUser(AccountUser accountUser, Integer id) 
		{
			IssuerRepresentative issuerRepresentative = iIssuerRepresentativeRepository.findOne(id);
			/**
			 * Module user creation required for Issuer_Representative
			 * Since in both Issuer creation and Representative creation flow the created module_user is ModuleUserService.ISSUER_MODULE
			 * so added the below code for the current flow.
			 */
			userService.createModuleUser(issuerRepresentative.getIssuer().getId(), ModuleUserService.ISSUER_MODULE, accountUser);
			
			issuerRepresentative.setUserRecord(accountUser);
			iIssuerRepresentativeRepository.save(issuerRepresentative);
		}

}