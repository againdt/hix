package com.getinsured.hix.batch.ssap.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

/**
 * New age out service to dis-enroll members by triggering IND71G
 * @author negi_s
 *
 */
public interface SsapAgeOutDisEnroll71GService {
	
	public Map<String,String> disenrollQHP(Long applicationId);
	public void logData(Set<Future<Map<String,String>>> tasks);
	public List<Long> getSsapAppForAgeOut(Long coverageYear,String applicationStatus);
}
