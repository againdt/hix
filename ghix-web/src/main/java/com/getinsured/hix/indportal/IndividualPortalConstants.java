package com.getinsured.hix.indportal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;

/**
 * Constant file for Ind Portal
 * 
 */
@Component
public final class IndividualPortalConstants {
    
    public static final String DOCUMENT_URL = "download/document?documentId=";
    
    /* HIX-38644 - Changes for State Referral Account Starts*/
    public static final String FROM_STATE_REFERRAL = "fromStateReferral";
    public static final String REFERRAL_SSAP_APPLICATION_ID = "referralSsapApplicationId";
    /* HIX-38644 - Changes for State Referral Account Ends*/
    public static final String LEAD_ID = "leadId";
    public static final String STAGE = "stage";
    public static final String APTC = "aptc";
    public static final String CSR = "csr";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String PLAN_NAME = "planName";
    public static final String PLAN_TYPE = "planType";
    public static final String MONTHLY_PLAN_PREMIUM = "monthlyPremium";
    public static final String NET_PLAN_PREMIUM = "netPremium";
    public static final String OFFICE_VISIT = "officeVisit";
    public static final String GENERIC_MEDICATION = "genericMedication";
    public static final String DEDUCTIBLE = "deductible";
    public static final String OUT_OF_POCKET = "outOfPocket";
    public static final String ISSUER_LOGO = "issuerLogo";
    public static final String FAVORITE_HEALTH_PLAN_SELECTED = "favoriteHealthPlanSelected";
    public static final String FAVORITE_DENTAL_PLAN_SELECTED = "favoriteDentalPlanSelected";
    public static final String DOLLAR_SYMBOL = "$";
    public static final String NUMBER_FORMAT = "%.0f";
    public static final double HUNDRED  = 100.0;
    public static final int ADULT_AGE = 18;
    public static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";
    public static final String UI_DATE_FORMAT = "MMMMMMMMMMMM dd, yyyy";
    public static final String MEDIUM_DATE_FORMAT = "MMM d, y";
    public static final String MEDIUM_DATE_TIME_FORMAT = "MMM d, y, h:mm:ss a";
    
    public static final String APPEVENT_APPLICATION = "APPEVENT_APPLICATION";
    public static final String APPEVENT_CSR_OVERRIDES = "APPEVENT_CSR_OVERRIDES";
    public static final String APPLICATION_CANCEL = "APPLICATION_CANCEL";
    public static final String CSR_OVERRIDES_OPEN_SEP = "CSR_OVERRIDES_OPEN_SEP";
    public static final String CSR_OVERRIDES_EDIT_NON_FIN_APP = "CSR_OVERRIDES_EDIT_NON_FIN_APP";
    public static final String CSR_OVERRIDES_INITIATE_NON_FIN_VERIF = "CSR_OVERRIDES_INITIATE_NON_FIN_VERIF";
    public static final String CSR_OVERRIDES_RERUN_NON_FIN_ELIG = "CSR_OVERRIDES_RERUN_NON_FIN_ELIG";
    public static final String CSR_OVERRIDES_TERMINATE_PLAN = "CSR_OVERRIDES_TERMINATE_PLAN";
    public static final String CSR_OVERRIDES_SEND_DEMO_UPDATES = "CSR_OVERRIDES_SEND_DEMO_UPDATES";
    public static final String CSR_OVERRIDES_REINSTATE_ENROLLMENTS = "CSR_OVERRIDES_REINSTATE_ENROLLMENTS";
    public static final String CSR_OVERRIDES_CHANGE_COVERAGE_START_DATE = "CSR_OVERRIDES_CHANGE_COVERAGE_START_DATE";
    public static final String CSR_OVERRIDES_SEP_DENIAL = "CSR_OVERRIDES_SEP_DENIAL";
    public static final String CSR_OVERRIDES_ENROLLMENT_STATUS_UPDATE = "CSR_OVERRIDES_ENROLLMENT_STATUS_UPDATE";
    public static final String CSR_OVERRIDES_MARK_OTR = "CSR_OVERRIDES_MARK_OTR";
    public static final String CSR_OVERRIDES_SEND_OUTBOUND_AT = "CSR_OVERRIDES_SEND_OUTBOUND_AT";
    public static final String CSR_OVERRIDES_OVERRIDE_PROGRAM_ELIG = "CSR_OVERRIDES_OVERRIDE_PROGRAM_ELIG";

    /* HIX-84551 */
    public static final String APPEVENT_PREFERENCES = "APPEVENT_PREFERENCES";
    public static final String PREFERENCES_DATA_UPDATE = "PREFERENCES_DATA_UPDATE";
    public static final String COMMUNICATION_PREFERENCE_UPDATE = "COMMUNICATION_PREFERENCE_UPDATE";
    public static final String CONTACT_INFORMATION_UPDATE = "CONTACT_INFORMATION_UPDATE";
    public static final String TEXT_MESSAGING = "TEXT_MESSAGING";
    public static final String A1095_RECEIPT_PREFERENCE = "1095-A_RECEIPT_PREFERENCE" ;
    
    public static final String OVERRIDE_COMMENT = "Override Comment";
    public static final String CMR_HOUSEHOLD_ID = "CMR Household Id";
    public static final String APPLICATION_ID = "Application Id";
    /* HIX-84551 */
    public static final String PREFERENCES_CHANGE_DATE = "Preferences Change Date";
    
    /* HIX-101055 */
    public static final String PREFERENCES_PREV_NOTIFICATION = "Previous Notification Preference";
    public static final String PREFERENCES_NEW_NOTIFICATION = "New Notification Preference";
    public static final String PREF_TEXT_MESSAGING = "Text Messaging Preference";
    public static final String PREF_PREV_TEXT_MESSAGING = "Previous Text Messaging Preference";
    public static final String PREF_COMMUNICATION = "prefCommunication";
	public static final String PREV_PREF_COMMUNICATION = "prevPrefCommunication";
	public static final String TEXT_MESSAGING_PREF = "textMessaging";
	public static final String TEXT_MESSAGING_PREF_PREV = "prevTextMessaging";
	public static final String PREF_COMMUNICATION_MAIL = "Postal Mail";
	public static final String PREF_COMMUNICATION_EMAIL = "E-mail";
	public static final String PREF_COMMUNICATION_EMAIL_AND_MAIL = "Both (E-mail and Postal)";
	public static final String _1095_A_NOTIFICATION_PREFERENCE = "1095-A Notification Preference";
	public static final String UPDATED_BY = "Updated By";
    
    /* HIX-99333 */
    public static final String CSR_OVERRIDES_SEP_DETAILS = "CSR_OVERRIDES_SEP_DETAILS";
    public static final String SSAP_APPLICANT_NAME = "Applicant";
    public static final String EVENT_TYPE = "Event";
    public static final String EVENT_DATE = "Event Date";
    public static final String SEP_START_DATE = "Special Enrollment Period Start Date";
    public static final String SEP_END_DATE = "Special Enrollment Period End Date";
    public static final String SSAP_CASE_NUMBER = "Case Number";
    public static final String SSAP_APPLICATION_TYPE = "Application Type";
    
    /* HIX-102773 */
    public static final String APPEVENT_ELIGIBILITY_INFORMATION = "APPEVENT_ELIGIBILITY_INFORMATION";
    public static final String APPEVENT_ESD_OVERRIDE_REQUIRED = "APPEVENT_ESD_OVERRIDE_REQUIRED";
    public static final String APPEVENT_ESD_OVERRIDDEN = "APPEVENT_ESD_OVERRIDDEN";
    public static final String ESD_APPLN_CASE_NUMBER = "Application Case Number";
    public static final String ESD_ELIGIBILITY_START_DATE = "Eligibility Start Date (ESD)";
    public static final String ESD_OVERRIDE_STATUS = "ESD Override Status";
    public static final String ESD_ENROLLMENT_START_DATE = "Enrollment Start Date";
    public static final String ESD_ENROLLMENT_END_DATE = "Enrollment End Date";
    public static final String ESD_SEP_START_DATE = "SEP Start Date";
    public static final String ESD_SEP_END_DATE = "SEP End Date";
    
    
    public static final int THREE = 3;
    public static final int FIVE = 5;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int FIFTEEN = 15;
    public static final String YES = "Y";
    public static final String INDIVIDUAL_LOWER_CASE = "individual"; 
    public static final String APPLICATION_JSON="application/json";
    public static final String CATEGORY="category";
    public static final String ENROLLMENT = "ENROLLMENT";
    public static final String SSAP_APPLICATION = "SSAP_APPLICATIONS";
    
    public static final Set<String> VALID_INDIVIDUAL_PORTAL_LOGIN_ROLES = new HashSet<String>();
    public static final Map<String, String> INDIVIDUAL_PORTAL_STAGE_PROGRESS_NUMBER = new HashMap<String, String>();
    public static final String DESIGNATED_BROKER_PROFILE = "designatedBrokerProfile";
    public static final String DESIGNATED_ASSISTER_PROFILE = "designatedAssisterProfile";
    public static final String DESIGNATE_ASSISTER = "designateAssister";
    
    public static final String EXCHANGE_ADMIN = "exadmin@ghix.com";
    
    public static final Set<String> MEMBER_ELIGIBILITY_FOR_COVERAGE = new HashSet<String>();
    
    public static final Map<String, Map<String, String>> REQUEST_CATEGRY_TO_TKM_MAPPING = new HashMap<String, Map<String, String>>();
    public static final Map<String, String> COMPLAINTS_TYPE_TO_TKM_MAPPING = new HashMap<String, String>();
    public static final Map<String, String> SUGGESTIONS_TYPE_TO_TKM_MAPPING = new HashMap<String, String>();
    public static final Map<String, String> OTHER_TYPE_TO_TKM_MAPPING = new HashMap<String, String>();
    public static final Map<String, String> ISSUES_TYPE_TO_TKM_MAPPING = new HashMap<String, String>();
    public static final Map<String, String> ENROLLMENT_STATUS = new HashMap<String, String>();
    
    public static final String PORTAL_VIEW_AND_PRINT_PERMISSION = "hasPermission(#model, 'IND_PORTAL_VIEW_AND_PRINT')";
    public static final String PORTAL_START_APP_PERMISSION = "hasPermission(#model, 'IND_PORTAL_START_APP')";
    public static final String PORTAL_RESUME_APP_PERMISSION = "hasPermission(#model, 'IND_PORTAL_RESUME_APP')";
    public static final String PORTAL_ENROLL_APP_PERMISSION = "hasPermission(#model, 'IND_PORTAL_ENROLL_APP')";
    public static final String PORTAL_EDIT_APP_PERMISSION = "hasPermission(#model, 'IND_PORTAL_EDIT_APP')";
    public static final String PORTAL_REPORT_CHANGES_PERMISSION = "hasPermission(#model, 'IND_PORTAL_REPORT_CHANGES')";
    public static final String PORTAL_INIT_VERIFICATIONS_PERMISSION = "hasPermission(#model, 'IND_PORTAL_INIT_VERIFICATIONS')";
    public static final String PORTAL_VIEW_VER_RESULTS_PERMISSION = "hasPermission(#model, 'IND_PORTAL_VIEW_VER_RESULTS')";
    public static final String PORTAL_RUN_ELIGIBILITY_PERMISSION = "hasPermission(#model, 'IND_PORTAL_RUN_ELIGIBILITY')";
    public static final String PORTAL_VIEW_ELIG_RESULTS_PERMISSION = "hasPermission(#model, 'IND_PORTAL_VIEW_ELIG_RESULTS')";
    public static final String PORTAL_CONTACT_US_PERMISSION = "hasPermission(#model, 'IND_PORTAL_CONTACT_US')";
    public static final String PORTAL_SUBMIT_APPEAL_PERMISSION = "hasPermission(#model, 'IND_PORTAL_SUBMIT_APPEAL')";
    public static final String PORTAL_DISENROLL_PERMISSION = "hasPermission(#model, 'IND_PORTAL_DISENROLL')";
    public static final String PORTAL_UPDATE_CARRIER_PERMISSION = "hasPermission(#model, 'IND_PORTAL_UPDATE_CARRIER')";
    public static final String PORTAL_EDIT_SPCL_ENRLMNT_PERIOD_PERMISSION = "hasPermission(#model, 'IND_PORTAL_EDIT_SPCL_ENRLMNT_PERIOD')";
    public static final String PORTAL_INBOX_PERMISSION = "hasPermission(#model, 'IND_PORTAL_INBOX')";
    public static final String PORTAL_ACCOUNT_SETTINGS_PERMISSION = "hasPermission(#model, 'IND_PORTAL_ACCOUNT_SETTINGS')";
    public static final String PORTAL_CHANGE_PASSWORD_PERMISSION = "hasPermission(#model, 'IND_PORTAL_CHANGE_PASSWORD')";
    public static final String PORTAL_CHANGE_SEC_QSTNS_PERMISSION = "hasPermission(#model, 'IND_PORTAL_CHANGE_SEC_QSTNS')";
    public static final String PORTAL_APPEALS = "hasPermission(#model, 'IND_PORTAL_APPEALS')";
    public static final String PORTAL_REFERRALS = "hasPermission(#model, 'IND_PORTAL_REFERRALS')";
    public static final String IND_PORTAL_REINSTATE_ENROLLMENT = "hasPermission(#model, 'IND_PORTAL_REINSTATE_ENROLLMENT')";
    public static final String PORTAL_MANAGE_PREFERENCES = "hasPermission(#model, 'IND_PORTAL_MANAGE_PREFERENCES')";
    public static final String PORTAL_ONE_TOUCH_RENEWAL = "hasPermission(#model, 'IND_PORTAL_ONE_TOUCH_RENEWAL')";
    public static final String PORTAL_SHOW_RENEWAL_TAB = "hasPermission(#model, 'IND_PORTAL_SHOW_RENEWAL_TAB')";
    public static final String PORTAL_OVERRIDE_SEP_DETAILS_PERMISSION = "hasPermission(#model, 'IND_PORTAL_OVERRIDE_SEP_DETAILS')";
    public static final String PORTAL_OVERRIDE_COVERAGE_DATE_PERMISSION = "hasPermission(#model, 'IND_PORTAL_OVERRIDE_COVERAGE_DATE')";
    public static final String PORTAL_OVERRIDE_SEP_DENIAL_PERMISSION = "hasPermission(#model, 'IND_PORTAL_OVERRIDE_SEP_DENIAL')";
    public static final String PORTAL_VIEW_UPLOAD_DOCUMENT_PERMISSION = "hasPermission(#model, 'VIEW_UPLOAD_DOCUMENT')";
    public static final String PORTAL_TKM_VIEW_TICKET_PERMISSION = "hasPermission(#model, 'TKM_VIEW_TICKET')";
    
    //HIX-79572 : CSR Override Option for Overriding the Enrollment Status
    public static List<String> ALLOWABLE_ENROLLMENT_STATUS_FOR_1095_OVERRIDE = new ArrayList<String>(); 
	
    /* constants for ind56*/
    public static final String TERMINATION_DATE = "TerminationDate";
    public static final String TERMINATION_REASON_CODE = "TerminationReasonCode";
        
    public static final Set<String> CONDITIONAL_ELIGIBILITIES = new HashSet<String>();
    public static final Map<String,String> IND_DSPL_STATUS = new HashMap<String, String>();
    public static final Set<String> ACTIVE_ENROLLMENT_STATUS = new HashSet<String>();
    
    /**
     * List of statuses for which cancel application is possible 
     */
    public static final Set<String> ACTIVE_STATUS_SSAP = new HashSet<String>();
    public static final List<String> STATUS_FOR_CANCELLING = new ArrayList<String>();
    public static final Set<String> ELIGIBLE_MEMBER_STATUS = new HashSet<String>();
   
    static{
        VALID_INDIVIDUAL_PORTAL_LOGIN_ROLES.add("INDIVIDUAL");

        INDIVIDUAL_PORTAL_STAGE_PROGRESS_NUMBER.put("WELCOME", "1");
        INDIVIDUAL_PORTAL_STAGE_PROGRESS_NUMBER.put("PRE_ELIGIBILITY", "1");
        INDIVIDUAL_PORTAL_STAGE_PROGRESS_NUMBER.put("PRE_SHOPPING", "2");
        INDIVIDUAL_PORTAL_STAGE_PROGRESS_NUMBER.put("PRE_SHOPPING_COMPLETE", "3");
        INDIVIDUAL_PORTAL_STAGE_PROGRESS_NUMBER.put("ENROLLMENT_COMPLETED", "4");

        MEMBER_ELIGIBILITY_FOR_COVERAGE.add("APTC");
        MEMBER_ELIGIBILITY_FOR_COVERAGE.add("APTC/CSR");
        MEMBER_ELIGIBILITY_FOR_COVERAGE.add("NA");
        MEMBER_ELIGIBILITY_FOR_COVERAGE.add("Ineligible");
        MEMBER_ELIGIBILITY_FOR_COVERAGE.add("APTC/IndianEligibility2");
        MEMBER_ELIGIBILITY_FOR_COVERAGE.add("APTC/IndianEligibility3");
        
        REQUEST_CATEGRY_TO_TKM_MAPPING.put("complaints", COMPLAINTS_TYPE_TO_TKM_MAPPING);
        REQUEST_CATEGRY_TO_TKM_MAPPING.put("suggestions", SUGGESTIONS_TYPE_TO_TKM_MAPPING);
        REQUEST_CATEGRY_TO_TKM_MAPPING.put("otherRequest", OTHER_TYPE_TO_TKM_MAPPING);
        REQUEST_CATEGRY_TO_TKM_MAPPING.put("technicalIssue", ISSUES_TYPE_TO_TKM_MAPPING);

        COMPLAINTS_TYPE_TO_TKM_MAPPING.put(CATEGORY,"Complaint");
        COMPLAINTS_TYPE_TO_TKM_MAPPING.put("broker","Broker");
        COMPLAINTS_TYPE_TO_TKM_MAPPING.put("carrier","Issuer");
        COMPLAINTS_TYPE_TO_TKM_MAPPING.put("exchangeStaff","Exchange Staff");
        COMPLAINTS_TYPE_TO_TKM_MAPPING.put("otherStaff","Other Complaint");
        
        SUGGESTIONS_TYPE_TO_TKM_MAPPING.put(CATEGORY,"Feedback");
        SUGGESTIONS_TYPE_TO_TKM_MAPPING.put("Feedback","Provide Feedback");
        
        OTHER_TYPE_TO_TKM_MAPPING.put(CATEGORY,"Triage");
        OTHER_TYPE_TO_TKM_MAPPING.put("Triage","Triage");
        
        ISSUES_TYPE_TO_TKM_MAPPING.put(CATEGORY,"Issues");
        ISSUES_TYPE_TO_TKM_MAPPING.put("Issues","Technical Issues");
        
        CONDITIONAL_ELIGIBILITIES.add("CAX");
        CONDITIONAL_ELIGIBILITIES.add("CAE");
        CONDITIONAL_ELIGIBILITIES.add("CAM");
        
            
        IND_DSPL_STATUS.put("Cancelled","EN_CN");
        IND_DSPL_STATUS.put("Terminated","EN_TMTD");
        IND_DSPL_STATUS.put("Enrolled","EN_EN");
        IND_DSPL_STATUS.put("Order Confirmed","EN_ORCN");
        IND_DSPL_STATUS.put("Payment Received", "EN_PR");
        IND_DSPL_STATUS.put("Pending", "EN_PE");
        IND_DSPL_STATUS.put("Aborted", "EN_AB");
        IND_DSPL_STATUS.put("eCommitted", "EN_ECM");
        IND_DSPL_STATUS.put("Approved", "EN_AP");
        IND_DSPL_STATUS.put("Inforce", "EN_INF");
        IND_DSPL_STATUS.put("Withdrawn", "EN_WD");
        IND_DSPL_STATUS.put("ESIG PENDING", "EN_ESG_PN");
        IND_DSPL_STATUS.put("DOES NOT EXIST", "EN_DN_EXT");
        IND_DSPL_STATUS.put("SOLD", "EN_SLD");
        IND_DSPL_STATUS.put("SUSPENDED", "EN_SP");
        IND_DSPL_STATUS.put("PAYMENT_PROCESSED", "EN_PMT_PRC");
        IND_DSPL_STATUS.put("PAYMENT_FAILED", "EN_PMT_FLD");
        IND_DSPL_STATUS.put("DECLINED", "EN_DCN");
        
        ENROLLMENT_STATUS.put("TERM", "Terminated");
        ENROLLMENT_STATUS.put("CANCEL", "Cancelled");
        ENROLLMENT_STATUS.put("PENDING", "Pending");
        ENROLLMENT_STATUS.put("CONFIRM", "Enrolled");
        
        ACTIVE_ENROLLMENT_STATUS.add("Pending");
        ACTIVE_ENROLLMENT_STATUS.add("Enrolled");
        
    	ELIGIBLE_MEMBER_STATUS.add("Advanced Premium Tax Credit");
    	ELIGIBLE_MEMBER_STATUS.add("Cost Sharing Reduction");
    	ELIGIBLE_MEMBER_STATUS.add("Medical and Dental Insurance");

    	STATUS_FOR_CANCELLING.add("OP");
        STATUS_FOR_CANCELLING.add("SG");
        STATUS_FOR_CANCELLING.add("SU");
        STATUS_FOR_CANCELLING.add("ER");
        
    	ACTIVE_STATUS_SSAP.add("OP");
        
        ALLOWABLE_ENROLLMENT_STATUS_FOR_1095_OVERRIDE.add("PENDING");
        ALLOWABLE_ENROLLMENT_STATUS_FOR_1095_OVERRIDE.add("TERM");
    }
    
    //SSAP Application Status 
    /**
     * Enum for SSAP application status
     *
     */
    public enum SSAPStatus{
        
        OP("op"),SG("sg"),SU("su"),ER("er"),EN("en"), PN("pn"), CL("cl");
        
        private String value;
        
        private SSAPStatus(String value){
            this.value = value;
        }
        
        public String getValue(){
            return value;
        }
    }
    

    public static final String CASE_NUMBER = "caseNumber";
    public static final String ENROLLMENT_EXCEPTION_MESSAGE = "Exception occured while fetching enrollment details :";

    public static final int SIXTY = 60;
    public static final int THOUSAND = 1000;
    public static final int NO_OF_HOURS_IN_DAY = 24;
    public static final int NINETY = 90;
    public static final int MAX_COMMENT_LENGTH = 4000;
    public static final String FAILURE = "failure";
    public static final String SUCCESS = "success";
    public static final int ELEVEN_PM = 23;
    public static final int FIFTY_NINE = 59;

    public static final String DENTAL_PLAN = "dental";
    public static final String HEALTH_PLAN = "health";
    
    public static final String COVERAGE_YEAR = "coverageYear";

	//SEP Event Types
	public static final int TYPE_0_EVENT = 0; 
	public static final int TYPE_1_EVENT = 1;
	public static final int TYPE_2_EVENT = 2;
	public static final int TYPE_3_EVENT = 3;
	public static final int TYPE_4_EVENT = 4;
	
	public static final double SIGNED_STAGE = 3.5;
	public static final int ENROLL_STAGE = 4;
	public static final int ENROLL_COMPLETED_STAGE = 5;
	
	public static final String PLAN_MGMT_EXCEPTION_MESSAGE = "Exception occured while getting plan data by plan Id (PlanMgmt)";
	
	public static final String INDIVIDUAL_ROLE = "INDIVIDUAL";
	
	public static final String FALSE = "false";
	public static final String TRUE = "true";
	
	//Individual Portal max date of birth
    public static final int MAX_AGE = 104;
	
    
    public static final String APPEAL = "APPEAL";

    
    public static final String NO = "N";
    public static final String QUERY_PARAM_SEPERATOR = "&";
    public static final String QUERY_START = "?";
    public static final String PREF_COMMUNICATION_SECURE_EMAIL = "Secure Inbox";
	public static final String PREF_COMMUNICATION_SECURE_EMAIL_AND_MAIL = "Both (Secure Inbox and Postal)";
     
    private IndividualPortalConstants(){
    }


}
