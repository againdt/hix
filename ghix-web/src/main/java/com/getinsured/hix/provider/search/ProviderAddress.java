package com.getinsured.hix.provider.search;

import java.util.StringTokenizer;

import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import org.codehaus.jettison.json.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
*Simple representation of provider address which can be converted into JSON object
*/
public class ProviderAddress implements Comparable<ProviderAddress>, JSONAware{
	private static Logger logger = LoggerFactory.getLogger(ProviderAddress.class);
	private String addline1;
	private String addLine2;
	private String[] phone;
	private String[] fax;
	private String city;
	private String state;
	private String zip;
	private Double lat = 0.0d;
	private Double lon = 0.0d;
	private float proximity;

	public ProviderAddress(String addressData){
		this.init(addressData);
	}

	public double getDistanceFromUserLocation() throws InvalidOperationException{
		return this.proximity;
	}

	/*
	 * Data format: (Single String)
	 * prac_addr_1:1812 Verdugo Blvd,
	 * prac_addr_2:null,
	 * city:Glendale,
	 * state:CA,
	 * zip:91208,
	 * prac_addr_latitude:34.204532,
	 * prac_addr_longitude:-118.216321,
	 * prac_phone:6263975000|6264454441|6619495000|6619495366|8187907100,
	 * prac_fax:null,
	 * practice_email_address:null"
	 */
	private void init(String addressData) {
		StringTokenizer st = new StringTokenizer(addressData,",");
		String dataField = "";
		while(st.hasMoreTokens()){
			dataField = st.nextElement().toString();
			pupulateData(dataField);
		}

	}

	private void pupulateData(String dataField) {
		int colonIdx = dataField.indexOf(':');
		if(colonIdx == -1){
			logger.error("Invalid data format, data is expected in <Field>:<value> format");
			return;
		}
		String fName = dataField.substring(0,colonIdx);
		String fval = dataField.substring(colonIdx+1);
		this.setAddressField(fName, fval);
	}

	private void setAddressField(String fName, String fval) {
		if(fName.equals("prac_addr_1")){
			this.addline1 = fval;
		}else if(fName.equals("prac_addr_2")){
			if(fval.equalsIgnoreCase("null")){
				this.addLine2 = "";
			}else{
				this.addLine2 = fval;
			}
		}else if(fName.equals("prac_phone")){
			if(!fval.equals("null")){
				this.phone = fval.split("\\|");
			}else{
				this.phone = null;
			}
		}else if(fName.equals("prac_fax")){
			if(!fval.equals("null")){
				this.fax = fval.split("\\|");
			}else{
				this.fax = null;
			}
		}else if(fName.equals("city")){
			this.city = fval;
		}else if(fName.equals("state")){
			this.state = fval;
		}else if(fName.equals("zip")){
			this.zip = fval;
		}else if(fName.equals("prac_addr_latitude")){
			if(!fval.equals("null")){
				this.lat = Double.valueOf(fval);
			}
		}else if(fName.equals("prac_addr_longitude")){
			if(!fval.equals("null")){
				this.lon = Double.valueOf(fval);
			}
		}else if(fName.equals("practice_email_address")){
			this.zip = fval;
		}else{
			logger.error("Unknown field ["+fName+"] with value ["+fval+"] encountered in provider address");
		}
	}

	public String getAddline1() {
		return addline1;
	}

	public void setAddline1(String addline1) {
		this.addline1 = addline1;
	}

	public String getAddLine2() {
		if(addLine2.equals("null")){
			this.addLine2="";
		}
		return addLine2;
	}

	public void setAddLine2(String addLine2) {
		this.addLine2 = addLine2;
	}

	public String[] getPhone() {
		return this.phone;
	}

	public void setPhone(String[] phone) {
		this.phone = phone;
	}

	public String[] getFax() {
		return fax;
	}

	public void setFax(String[] fax) {
		this.fax = fax;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}
	
	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public float getProximity() {
		return proximity;
	}

	public void setProximity(float proximity) {
		this.proximity = proximity;
	}

	// Ascending sort
	@Override
	public int compareTo(ProviderAddress o) {
		if(this.lat.doubleValue() == 0.0d || this.lon.doubleValue() == 0.0d )
			return -1;
		double difference = (this.proximity - o.getProximity());
		if(difference > 0 ){
			return 1;
		}else if(difference < 0){
			return -1;
		}else if(difference == 0){
			return 0;
		}else{
			return 0;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONArray faxes = null;
		JSONArray phones = null;
		JSONObject  obj = new JSONObject();
		obj.put("add_1", this.addline1);
		if(addLine2 != null){
			obj.put("add_2", this.addLine2);
		}
		obj.put("city", this.city);
		obj.put("state", this.state);
		obj.put("zip", this.zip);
		obj.put("lat", this.lat);
		obj.put("lon", this.lon);
		obj.put("proximity", this.proximity);
		if(this.phone != null){
			phones = new JSONArray();
			for(String phone: this.phone){
				phones.put(phone);
			}
			obj.put("phones", phones);
		}
		if(this.fax != null){
			faxes = new JSONArray();
			for(String faxNo: this.fax){
				faxes.put(faxNo);
			}
			obj.put("fax", faxes);
		}
		return obj.toJSONString();
	}
}
