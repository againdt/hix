package com.getinsured.consumer.service;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.getinsured.consumer.util.GoodRxUtil;

public class GoodRxServiceTest {
	
	GoodRxService service;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		service = new GoodRxServiceImpl();
	}

	@Test
	public void testUtilForInvalidSingleArg() {
		Assert.assertEquals(1, GoodRxUtil.parseArgs("sample").length);
	}
	
	@Test
	public void testUtilForEmptyArg() {
		Assert.assertEquals(null, GoodRxUtil.parseArgs(""));
	}
	
	@Test
	public void testUtilForValidArg() {
		Assert.assertEquals(2, GoodRxUtil.parseArgs("name=lipitor&manufacturer=brand").length);
	}
	
	@Test
	public void testUtilForEmptyKeyVal() {
		Assert.assertEquals(null, GoodRxUtil.parseKeyVal(null));
	}
	
	@Test
	public void testUtilForValidKeyVal() {
		Assert.assertEquals(2, GoodRxUtil.parseKeyVal("name=lipitor").length);
	}
	
	@Test
	public void testUtilForInValidKeyVal() {
		Assert.assertEquals(null, GoodRxUtil.parseKeyVal("name=lipitor=brand=something"));
	}
	
	@Test
	public void testUtilForAppendApiKey() {
		StringBuffer sb = new StringBuffer("abc=def");
		GoodRxServiceImpl impl = new GoodRxServiceImpl();
		impl.appendAPIKey(sb);
		Assert.assertEquals(new StringBuffer("abc=def&apikey=d6de803087").toString(), sb.toString());
	}
	
	@Test
	public void testUtilForEncryption() {
		Assert.assertEquals("f7bc83f430538424b13298e6aa6fb143ef4d59a14946175997479dbc2d1a3cd8", GoodRxUtil.hmacSHA256("The quick brown fox jumps over the lazy dog", "key"));
	}
	
	@Test
	public void testUtilForEncoding() {
		//source http://ostermiller.org/calc/encode.html
		Assert.assertEquals("dGhpcyBpcyBzdHJpbmcgZXhhbXBsZS4uLi53b3chISE%3D", GoodRxUtil.encodeBase64("this is string example....wow!!!".getBytes()));
	}
	
	@Test
	public void testServiceForSample() {
		//source http://ostermiller.org/calc/encode.html
		Assert.assertEquals("value",service.getFairPrice("name=lipitor"));
	}
	
	

}
