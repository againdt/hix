package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrlPopulateExtEnrollmentService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;

public class EnrlPopulateExtEnrollmentTask implements Tasklet{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlPopulateExtEnrollmentTask.class);
	private EnrlPopulateExtEnrollmentService enrlPopulateExtEnrollmentService;
	private JobService jobService;
	private Job job;
	private JobLauncher jobLauncher;
	private String replace;
	long jobExecutionId = -1;

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

		if(null != enrlPopulateExtEnrollmentService){
			jobExecutionId=chunkContext.getStepContext().getStepExecution().getJobExecutionId();
			String batchJobStatus=null;

			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}

			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||
					batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}

			LOGGER.trace("EnrlPopulateExtEnrollmentTask calling EnrollmentReconciliationService.processReconciliationInFile");

			StepExecution stepExecution = chunkContext.getStepContext().getStepExecution();

			enrlPopulateExtEnrollmentService.processExternalCSV(jobService, stepExecution, replace);

		}else{

			LOGGER.error("EnrlPopulateExtEnrollmentService is null ");
			throw new RuntimeException("EnrlPopulateExtEnrollmentService is null") ;
		}

		return RepeatStatus.FINISHED;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public JobLauncher getJobLauncher() {
		return jobLauncher;
	}

	public void setJobLauncher(JobLauncher jobLauncher) {
		this.jobLauncher = jobLauncher;
	}

	public EnrlPopulateExtEnrollmentService getEnrlPopulateExtEnrollmentService() {
		return enrlPopulateExtEnrollmentService;
	}

	public void setEnrlPopulateExtEnrollmentService(EnrlPopulateExtEnrollmentService enrlPopulateExtEnrollmentService) {
		this.enrlPopulateExtEnrollmentService = enrlPopulateExtEnrollmentService;
	}

	public String getReplace() {
		return replace;
	}

	public void setReplace(String replace) {
		this.replace = replace;
	}

}
