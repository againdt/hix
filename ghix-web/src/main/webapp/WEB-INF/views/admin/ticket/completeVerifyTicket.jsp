<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<style type="text/css">
    .modal .header{
        border-radius: 6px 6px 0 0 !important;
        padding: 10px !important;
    }
    .modal .dialogClose{
        margin: -4px !important;
        padding: 4px;
    }
    .Ajaxloader{
        background-color: #F5F5F5;
        background-image: url("/hix/resources/images/Eli-loader.gif");
        background-position: center center;
        background-repeat: no-repeat;
        height: 769px;
        margin: -10% 0;
        opacity: 0.75;
        position: fixed;
        width: 100%;
        z-index: 2147483647;
    }

    #taskRejectionReason {
        display: none;
    }

    .controls .errorspan {
        display: none;
        background-image: none;
        color: #a94442;
        background-color: #f2dede;
        border-color: #ebccd1;
        padding-top: 5px;
        margin-bottom: 5px;
    }

    .excl {
        background: #ea5200 none repeat scroll 0 0;
        color: #fff;
        font-weight: bold;
        margin: 0 5px;
        padding: 0 4px 0 5px;
        width: auto;
        font-style: normal;
    }
</style>
<input type="hidden" id="ticketIdVerify" name="ticketId" value="${encTicketid}">
<div id="markVerifyCompleteDialogAction" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markVerifyCompleteDialogAction" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-header markCompleteHeader">
        <button aria-hidden="true" data-dismiss="modal" id="verifyCrossCloseComplete" class="close" type="button" onclick="cancelVerifyModel();">&times;</button>
        <h3><spring:message  code="header.markTaskAsComplete"/></h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <h4 id="taskVerifyName" name="taskName"></h4>
        </div>
        <div class="row-fluid">
            <div class="span5">
                <input type="hidden" name="validateComments" id="validateVerifyComments" value="">

                <div class="taskPropertiesControls" id="taskPropertiesVerifyControls">
                    <div class="control-group">
                        <label class="control-label">
                            Mark document as
                            <img id="statusRequired" src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" />
                        </label>
                        <div class="controls">
                            <select id="ticketStatusVerify" name="selectField">
                                <option value="select" disabled selected>Select</option>
                                <option value="accepted">Accepted</option>
                                <option value="rejected">Rejected</option>
                            </select>
                            <span class="errorspan" id="statuserror">
                                <em class="excl">!<span class="aria-hidden">Error: Invalid Entry </span></em>Please select a status.
                            </span>
                        </div>
                    </div>
                </div>
                <div class="taskRejectionReason" id="taskRejectionReason">
                    <div class="control-group">
                        <label class="control-label">
                            Reason
                            <img id="rejectionRequired" src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" />
                        </label>
                        <div class="controls">
                            <select id="rejectionReason" name="rejectionReasonField">
                                <option value="select" disabled selected>Select</option>
                                <option value="Invalid document - Doesn't match application">Invalid document - Doesn't match application</option>
                                <option value="Invalid document - More recent document required">Invalid document - More recent document required</option>
                                <option value="Invalid document - Not applicable">Invalid document - Not applicable</option>
                                <option value="Invalid document - Unreadable">Invalid document - Unreadable</option>
                            </select>
                            <span class="errorspan" id="rejectionerror">
                                <em class="excl">!<span class="aria-hidden">Error: Invalid Entry </span></em>Please select a reason
                            </span>
                        </div>
                    </div>
                </div>

                <div class="control-group" >
                    <label class="control-label"><spring:message code="text_task.notes"/>
                        <img id="commentsReqd" src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" />
                    </label>
                    <div class="controls">
                        <textarea name="commentBox" id="commentVerifyBoxAction"  maxlength="1999"></textarea>
                        <span class="errorspan" id="commentsError">
                            <em class="excl">!<span class="aria-hidden">Error: Invalid Entry </span></em>Please enter comments
                        </span>
                    </div>
                </div>

                <div class="control-group" id="completeVerifyTaskloader" style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top:-52px;margin-left: -47px;">
                    <img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer clearfix">
        <button class="btn pull-left" id="ticketListCompleteTaskCancelBtnVerify" data-dismiss="modal" aria-hidden="true" onclick="cancelVerifyModel();">
            <spring:message code="button.cancel"/>
        </button>
        <button class="btn btn-primary" id="ticketListtaskCompleteBtnVerify" onclick="javascript:submitVerifyTaskCompletion();">
            <spring:message code="btnTxt.taskComplete"/>
        </button>
    </div>
</div>
<!-- Cancel confirmation dialog  -->
<!-- Cancel ticket dialog ends -->
<script type="text/javascript">

    $('document').ready(function() {
        $('#ticketStatusVerify').change(function() {
            var str = $(this).val();

            if (str.toLowerCase() === 'rejected') {
                $('#taskRejectionReason').show();
            } else {
                $('#taskRejectionReason').hide();
            }
        });
    });

    function cancelVerifyModel() {
        $('#markVerifyCompleteDialogAction').modal('hide');
        $('#ticketStatusVerify').val("select");
        $('#rejectionReason').val("select");
        $('#commentVerifyBoxAction').val("");
        $("#statuserror").hide();
        $("#commentsError").hide();
        $("#rejectionerror").hide();
        $('#taskRejectionReason').hide();
    }

    function getTicketActiveTaskDataVerifyDoc() {
        if ($('#validateVerifyComments').val() == 'N') {
            $('#commentsReqd').hide();
        } else {
            $('#commentsReqd').show();
        }

        $('body').prepend('<div class="Ajaxloader"></div>').css('overflow','hidden');
        var ticketId = $("#ticketIdVerify").val();
        var validateUrl = '<c:url value="/ticketmgmt/ticket/userTickettasklist"></c:url>';

        $.ajax(
            {
                url : validateUrl,
                type : "POST",
                data : {
                    ticketId : ticketId,
                    '${df:csrfTokenParameter()}' : '<df:csrfToken plainToken="true"/>'
                },
                success: function(response,xhr)
                {
                    $('.Ajaxloader').remove();
                    $('body').css('overflow','auto');
                    if(isInvalidCSRFToken(xhr))
                        return;
                    parseVerifyTicketActiveTaskData(response);
                },
                error: function(e)
                {
                    $('.Ajaxloader').remove();
                    $('body').css('overflow','auto');
                    alert("Failed to Fetch Data");
                }
            });
    }

    function parseVerifyTicketActiveTaskData(response){
        var newresponse = JSON.parse(response);
        var myList = newresponse;
        var columnSet = [];
        var taskId;
        var actTaskId;
        var processInstId;
        var taskStatus;
        var taskName;
        var isAuthorized;
        for (var i = 0 ; i < myList.length ; i++) {
            var rowHash = myList[i];
            for (var key in rowHash) {
                if(key=="TaskId"||key=="ActivitiTaskId"||key=="ProcessInstanceId"||key=="Status"||key=="TaskName"||key=="IsAuthorized"){
                    columnSet.push(key);
                }
            }
        }

        for (var i = 0 ; i < myList.length ; i++) {
            for (var colIndex = 0 ; colIndex < columnSet.length ; colIndex++) {

                if(columnSet[colIndex]=="TaskId"){
                    taskId= myList[i][columnSet[colIndex]];
                }
                if(columnSet[colIndex]=="ActivitiTaskId"){
                    actTaskId= myList[i][columnSet[colIndex]];
                }
                if(columnSet[colIndex]=="ProcessInstanceId"){
                    processInstId= myList[i][columnSet[colIndex]];
                }
                if(columnSet[colIndex]=="Status"){
                    taskStatus=myList[i][columnSet[colIndex]];
                }
                if(columnSet[colIndex]=="TaskName"){
                    taskName=myList[i][columnSet[colIndex]];
                }

                if(columnSet[colIndex]=="IsAuthorized"){
                    isAuthorized=myList[i][columnSet[colIndex]];
                }

            }

            if (taskStatus ==='Claimed') {
                $('body').css('overflow','auto');
                tkmTaskId = taskId;
                actProInstId = processInstId;
                encTask(actTaskId, function(encActTaskId)
                {
                    actTask = encActTaskId;
                });
                $('#markVerifyCompleteDialogAction').modal('show');
            }

            if (taskStatus ==='UnClaimed') {
                claimtask(taskId,actTaskId);
            }

            $("#taskVerifyName").text(taskName);
        }
    }

    function completeVerifyTask(taskId, encActTaskId, processInstId) {
        actTask = encActTaskId;
        tkmTaskId = taskId;
        actProInstId = processInstId;
        $('#markVerifyCompleteDialogAction').modal('show');
    }

    function encTask(actTaskId, callback) {
        var validateUrl = '<c:url value="/ticketmgmt/ticket/getEncValue"> </c:url>';
        $('body').prepend('<div class="Ajaxloader"></div>').css('overflow','hidden');
        var taskFormPropertiesUrl = validateUrl;
        $.ajax({
                url : taskFormPropertiesUrl,
                type : "GET",
                data : {
                    id : actTaskId
                },
                success: function(response, xhr) {
                    $('.Ajaxloader').remove();
                    $('body').css('overflow','auto');
                    actTaskId = JSON.parse(response);
                    return callback(actTaskId);
                },
                error: function(e) {
                    $('.Ajaxloader').remove();
                    $('body').css('overflow','auto');
                    alert("Failed to Fetch Data");
                }
            });
    }

    function validateCompleteTicketModal() {
        var errors = false;
        var comments = document.getElementById('commentVerifyBoxAction').value;
        var validateComments = document.getElementById('validateVerifyComments').value;
        var ticketStatusObj = document.getElementById('ticketStatusVerify');
        var ticketStatus = ticketStatusObj !== undefined ? ticketStatusObj.options[ticketStatusObj.selectedIndex].text : "";
        var rejectionReasonObj = document.getElementById('rejectionReason');
        var rejectionReason = rejectionReasonObj !== undefined ? rejectionReasonObj.options[rejectionReasonObj.selectedIndex].text : "";

        if(ticketStatus.toLowerCase() === 'select' || ticketStatus === "") {
            $("#statuserror").show();
            errors = true;
        } else {
            $("#statuserror").hide();
        }

        if (ticketStatus.toLowerCase() === 'rejected' && rejectionReason !== "") {
            if (rejectionReason.toLowerCase() === 'select') {
                $("#rejectionerror").show();
                errors = true;
            } else {
                $("#rejectionerror").hide();
            }
        } else {
            $("#rejectionerror").hide();
        }

        if (comments === '' && validateComments === 'Y') {
            $("#commentsError").show();
            errors = true;
        } else {
            $("#commentsError").hide();
        }

        return errors;
    }

    function submitVerifyTaskCompletion(){
        var errors;
        var comments = document.getElementById('commentVerifyBoxAction').value;
        var validateComments = document.getElementById('validateVerifyComments').value;
        var commentLength = document.getElementById('commentVerifyBoxAction').value.length;
        var ticketStatusObj = document.getElementById('ticketStatusVerify');
        var ticketStatus = ticketStatusObj !== undefined ? ticketStatusObj.options[ticketStatusObj.selectedIndex].text : "";
        var rejectionReasonObj = document.getElementById('rejectionReason');
        var rejectionReason = rejectionReasonObj !== undefined ? rejectionReasonObj.options[rejectionReasonObj.selectedIndex].text : "";

        errors = validateCompleteTicketModal();

        if (validateComments == 'N' && comments == '') {
            commentLength = 0;
        }

        if (!errors) {
            var formPropertiesMap = {};
            var myObject = new Object();
            myObject.ActivitiTaskId = actTask + "";
            myObject.ProcessInstanceId = actProInstId + "";
            myObject.TaskId = tkmTaskId + "";
            myObject.TicketId = $("#ticketIdVerify").val();
            myObject.ValidateComments = validateComments;

            formPropertiesMap["ticketStatus"] = ticketStatus;
            formPropertiesMap["taskRejectionReason"] = rejectionReason;

            myObject.formPropertiesMap = formPropertiesMap;
            var myStringJSON = JSON.stringify(myObject);

            $("#completeVerifyTaskloader").show();
            $("#verifyCrossCloseComplete").attr("disabled",true);
            $("#ticketListtaskCompleteBtnVerify").attr("disabled",true);

            var validateUrl = '<c:url value="/ticketmgmt/ticket/completeUserTask"></c:url>';
            var completeTaskUrl = validateUrl;

            $.ajax({
                    url : completeTaskUrl,
                    type : "POST",
                    data : {
                        taskObject : myStringJSON,
                        comments : comments,
                        commentLength : commentLength,
                        ${df:csrfTokenParameter()} : '<df:csrfToken plainToken="true" />'
                    },
                    success: function(response, xhr) {
                        if(isInvalidCSRFToken(xhr))
                            return;
                        $("#completeVerifyTaskloader").hide();
                        data = response.split('|');

                        if (data[0] == "SUCCESS") {
                            if (data.length > 1) {
                                notifyCompleteResponse(false);
                            } else {
                                notifyCompleteResponse(true);
                            }
                        }
                    },
                    error: function(e) {
                        $("#completeVerifyTaskloader").hide();�
                        alert("Failed to Complete Ticket");
                    }
                });
        }
    }
</script>
