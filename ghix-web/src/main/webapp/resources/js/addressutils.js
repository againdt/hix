/**
 * viewValidAddressList is a javascript function. This performs AJAX call to validate address and refreshes the content on JSP.
 * 
 * <p> Address Modal gets openup only for matching records returned from actual web service.
 * 
 * Developer needs to invoke this from address section of JSP by passing below params. <p> All are mandatory. 
 * 
 * @param address1
 * @param address2
 * @param city
 * @param state
 * @param zip
 * @param ids - should be ids for address1, address2, city, state, zip fields from JSP and be ~ separated.
 */

function viewValidAddressList(address1, address2, city, state, zip, ids) {
	var enteredAddress = address1+","+address2+","+city+","+state+","+zip;
	
	$.ajax({
		url: "/hix/platform/validateaddress",
	    data: {enteredAddress: enteredAddress,
	    		ids: ids},
	    success: function(data){
	   		var href = '/hix/platform/address/viewvalidaddress';
	   		$('#addressIFrame').remove(); //remove any present modal 
	   		if(data=="SUCCESS"){
		          $('<div class="modal" id="addressIFrame"><div class="modal-header" style="border-bottom:0;"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">×</button></div><div class="modal-body" style="max-height:420px; padding: 10px 0px;"><iframe id="modalData" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:400px;"></iframe></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true">Close</button></div></div>').modal({backdrop:false});
	   		}
	    }
	});
}


/**
 * viewValidAddressListNew is a javascript function. This performs AJAX call to validate address and refreshes the content on JSP.
 * 
 * <p> Address Modal gets openup only for matching records returned from actual web service.
 * 
 * Developer needs to invoke this from address section of JSP or modal by passing below params. 
 * <p> All are mandatory. 
 *  
 * @param address1, value in the form
 * @param address2, value in the form
 * @param city, value in the form
 * @param state, value in the form
 * @param zip, value in the form
 * 
 * @param model_address1, value fetched from appServer(db)
 * @param model_address2, value fetched from appServer(db)
 * @param model_city, value fetched from appServer(db)
 * @param model_state, value fetched from appServer(db)
 * @param model_zip, value fetched from appServer(db)
 * 
 * @param ids - Should be ids for address1, address2, city, state, zip fields from JSP and be ~ separated. 
 * 				Also there should be hidden fields ids for lat, lon, county and rdi.
 * 				see example:- address1_business~address2_business~city_business~state_business~zip_business~lat_business~lon_business~rdi_business~county_business
 */
function viewValidAddressListNew(address1, address2, city, state, zip, 
		model_address1, model_address2, model_city, model_state, model_zip, 
		ids) {
	var enteredAddress = address1+","+address2+","+city+","+state+","+zip;

	var imgpath = '/hix/resources/img/ajax_loader_blue_128.gif';
	//alert('dfghjkl;')
	$('#addressProgressPopup, .modal-backdrop').remove(); //remove wait popup
	// lock the screen and inform user about validation 'Please wait... while we validate your address...'
	$('<div class="modal popup-address" id="addressProgressPopup"><div class="modal-header" style="border-bottom:0; "><h4 class="margin0">Please wait... while we validate your address...</h4></div><div class="modal-body center" style="max-height:470px; padding: 10px 0px;"><img src=\"' + imgpath + '\"></div></div>').modal({backdrop:"static",keyboard:false});
	
	if (!address1 || !city || !state || !zip){
		// This check is performed to protect the rest generic flow if client misses to validate the entered address data
		$('#addressProgressPopup, .modal-backdrop').remove(); //remove wait popup
		$('<div class="modal popup-address" id="addressIFrame" tabindex="-1"><div class="modal-header" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" aria-hidden="true">×</button><h4 class="margin0">Address Missing</h4></div></div><div class="modal-body" style="max-height:470px; padding: 10px 10px;">Please provide complete address.</div><div class="modal-footer"><button class="btn" id="iFrameClose" aria-hidden="true">OK</button></div></div>').modal({backdrop:false, keyboard:false});
		$('#addressIFrame').before('<div class="modal-backdrop fade in addressErrModal"></div>');
		closeButtonFunction();
	}else{
		// check whether model and page values are same. If same, donot call web service
		if (model_address1 == address1 && model_address2 == address2 && model_city == city && model_state == state && model_zip == zip){
			//no ChangeOfAddress, ignore this call...
			$('#addressProgressPopup, .modal-backdrop').remove();	//remove wait popup	
		}else{		
			$.ajax({
				url: "/hix/platform/validateaddressnew",
			    data: {	enteredAddress: enteredAddress,
			    		ids: ids},
			    
			    success: function(data){
			   		var href = '/hix/platform/address/viewvalidaddressnew';
			   		var retVal=data.split("~");
			   		var idxx=ids.split("~");
			   		$('#addressProgressPopup').remove();	//remove wait popup
			   		$('#addressIFrame').remove(); //remove any present modal 
			   		
		   			try{
		   				//reset hidden information if any it holds....
				   		document.getElementById(idxx[5]).value=parseFloat(0.00); //latitude
			   			document.getElementById(idxx[6]).value=parseFloat(0.00); //longitude
			   			document.getElementById(idxx[7]).value=""; //rdi
			   			//document.getElementById(idxx[8]).value=""; //county
			   			if(data=="SUCCESS"){
					          $('<div class="modal popup-address" id="addressIFrame"><div class="modal-header" style="border-bottom:0; "><h4>Check Your Address <button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></h4></div><div class="modal-body" ><iframe id="modalData" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:235px;"></iframe></div></div>').modal({backdrop:false});
					          $('#addressIFrame').before('<div class="modal-backdrop addressErrModal"></div>');
					          closeButtonFunction();
				   		}else if(data=="IGNORE"){
				   			//do nothing...
				   			$('.modal-backdrop').remove();
				   		}else if(retVal[0]=="FAILURE"){
				   			//popup Confirm Address lightbox
				   			$('.modal-backdrop').remove(); //remove wait popup
				   			$('<div class="modal popup-address" id="addressIFrame"><div class="modal-header" style="border-bottom:0; "><h4>Confirm Address <button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></h4></div><div class="modal-body" style="max-height:470px;">' + retVal[1] + '</div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true">OK</button></div></div>').modal({backdrop:false});
				   			$('#addressIFrame').before('<div class="modal-backdrop addressErrModal"></div>');
				   			closeButtonFunction();
				   			
				   		}else if(retVal[0]=="MATCH_FOUND"){
				   			// As MATCH_FOUND, go ahead and update hidden information without opening address light-box....
				   			document.getElementById(idxx[5]).value=parseFloat(retVal[1]); //latitude
				   			document.getElementById(idxx[6]).value=parseFloat(retVal[2]); //longitude
				   			document.getElementById(idxx[7]).value=retVal[3]; //rdi
				   			document.getElementById(idxx[8]).value=retVal[4]; //county
				   			$('.modal-backdrop').remove();
				   		}
		   				
		   			}catch(err){
		   				
		   				$('.modal-backdrop').remove();
		   			}
			   		
			    }
			});
		}
	}
	
}

function closeButtonFunction(){
	
	$('#iFrameClose, #crossClose, .close').bind('click',function(){
		$('.modal-backdrop').remove(); //don't remove HIX-20877, HIX-21085
		$('.addressErrModal, #addressIFrame').remove();
		if($('#editdependent, #einModal, #worksite').is(':visible')){
			$('#addressIFrame, .addressErrModal').remove();
			$('body').append('<div class="modal-backdrop"></div>');
		}
		
		if (!$('#worksite, #editdependent, #frmworksites, #einModal').is(':visible')){
			$('.modal-backdrop').remove();
		}
	});
	
	
}

