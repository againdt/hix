<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="com.getinsured.hix.util.GhixConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<style>
#contactExchnagePopupModal {
	width:800px;
	display:none;
}
.row-fluid select.span12{width:99%;}
</style>
<script id="saveIndTicketForm" type="text/ng-template">
				
		<div id="contactExchnagePopupModal" class="modal hide fade" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
			<form action="" id="saveIndTicketForm" name="saveIndTicketForm" novalidate>
			<div class="modal-header">
						<button type="button" class="close reset-form" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3>Create a <%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)%> ticket</h3>
			</div>
			<div class="modal-body">
				<div class="contactExchangeContainer">
					<h2>Support Request for {{modalContactData.firstName + '  ' + modalContactData.lastName}} </h2>
					<div class="row-fluid ">
						<div class="span3 gray">
							<div class="control-group clearfix">
								<label for="requestType" class="control-label">Request Type <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<select size="1" id="requestType" name="category" class="span12" aria-label="Request Type"  ng-model="requestType.requestTypeModel" ng-options="key as key for (key , value) in categoryRes" ng-change="changeSubCategory(requestType.requestTypeModel)" required>
										<option value="">Select</option>
									</select>
									<span class="span12" style="color:red" ng-show="saveIndTicketForm.category.$error.required && saveIndTicketFormSubmitted">Request Type is Required</span>	
								</div>
								<div id="requestType_error"></div>
							</div>
							<div class="control-group clearfix">
								<label for="requestType" class="control-label">Request Sub-Type <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<select size="1" id="requestSubType" name="type" class="span12" aria-label="Request Type" ng-model="requestType.reqSubTypeModel" ng-options="subType as subType for subType in subTypeArray" required>
										<option value="">Select</option>
									</select>
									<span class="span12" style="color:red" ng-show="saveIndTicketForm.type.$error.required && saveIndTicketFormSubmitted">Request Sub-Type is Required</span>
									
								</div>
								<div id="requestSubType_error"></div>
							</div>
							<div class="control-group clearfix">
								<label for="priority" class="control-label">Priority <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									
									 <select ng-model="requestType.priorityModel" ng-options="priority as priority for priority in priorityArray" class="span12"/>
																					
								</div>
								
							</div>
						</div>
						<div class="span9 gray">
							<div class="control-group clearfix">
								<label for="subject" class="control-label">Subject <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<input type="text" name="subject" ng-model="requestType.subjectModel" class="span12" maxlength="100" required/>
									<span class="span12" style="color:red" ng-show="saveIndTicketForm.subject.$error.required && saveIndTicketFormSubmitted">Subject is Required</span>
								</div>
							</div>
							<div class="control-group clearfix">
								<label for="description"  class="control-label">Description <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<span class="span12" style="color:red" ng-show="saveIndTicketForm.description.$error.required && saveIndTicketFormSubmitted">Description  is Required</span>
								<textarea rows="20" cols="20" class="span12" name="description" maxlength="2000" id="description" ng-model="requestType.descriptionModel" required>

									Please enter the following primary consumer information (if available).
									Application ID:
									Applicant's Last Name:
									Applicant's Date of Birth:
									Enter the following issuer and plan information:

									Enter the following issuer and plan information: 
									Issuer Name:
									Consumer Plan:

									Describe the situation with as much detail as possible:

									Please enter the steps that you have taken so far to assist the consumer with the above issue (if any)
								</textarea>
								</div>
							</div>
						</div>
					</div>
				</div><!-- contactExchangeContainer -->
						
			</div>
			<div class="modal-footer">
				<button type="button" class="btn reset-form" data-dismiss="modal" ng-click="saveIndTicketFormSubmitted=false" >Cancel</button>                  
				<input type="submit" ng-click="saveIndTicketFormSubmitted=false;submitTicketInfo()" class="btn btn-primary pull-right" value="Submit" ng-if="saveIndTicketForm.$valid" />
				<input type="submit" class="btn btn-primary pull-right" ng-click="$parent.saveIndTicketFormSubmitted=true"  value="Submit" ng-if="!saveIndTicketForm.$valid" />
			</div>
		</form>
	</div>		
	</script>
	<script id="saveTicketSuccessForm" type="text/ng-template" >
	     <div id="ticketSuccessPopupModal" class="modal hide" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>Success</h3>
			</div>
			<div class="modal-body">
				<div class="ticketSuccessContainer"
					<p>A ticket has been created for {{modalContactData.firstName + '  ' + modalContactData.lastName}}. Please keep the following ticket number for future reference. <span id="ticketNumber" class="display_block txt-center gutter10-t"><strong>{{ticketNumber}}</strong></span></p>
					<p class="txt-center"><button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></p>
				</div>
			</div>
		</div>
	</script>