package com.getinsured.eligibility.at.ref.service;

import static com.getinsured.eligibility.at.ref.service.LceProcessHandlerService.APP_EVENT_TYPE;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.AccountTransferCategoryEnum;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.resp.si.dto.ApplicationExtension;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
/**
 * @author chopra_s
 * 
 */
@Component("lceAptcOnlyHandlerService")
@Scope("singleton")
public class LceAptcOnlyHandlerService extends LceProcessHandlerBaseService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(LceAptcOnlyHandlerService.class);

	private static final String APP_EVENT_APTC_CHANGE = "APTC_CHANGED";
	
	@Autowired
	@Qualifier("lceAppExtensionEvent")
	private LceAppExtensionEvent lceAppExtensionEvent;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	
	@Autowired
	private LookupService lookupService;
	
	@Autowired
	private AppEventService appEventService;
	
	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;
	
	@Override
	//@ReferralTransactionAnno :::: Commented annotation to process circular calls (Referral-> IND71G -> Enrollment -> Referral) 
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceAptcOnlyHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		final ApplicationExtension applicationExtension = lceProcessRequestDTO.getApplicationExtension();
		SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		SsapApplication enrolledApplication = loadCurrentApplication(enrolledApplicationId);
		final SsapApplicant primaryApplicant = ReferralUtil.retreiveApplicant(currentApplication, ReferralConstants.PRIMARY);
		final Map<String, List<ApplicantEvent>> applicantExtensionEvents = ApplicationExtensionEventUtil.populateApplicantEventsFromExtension(currentApplication.getSsapApplicants(), lceProcessRequestDTO.getApplicationExtension(), primaryApplicant);
		boolean changeInEligibility = false;
		if((currentApplication.getExchangeEligibilityStatus().toString().contains("APTC") 
				   && ("QHP".equalsIgnoreCase(enrolledApplication.getExchangeEligibilityStatus().toString()) 
				   || "CSR".equalsIgnoreCase(enrolledApplication.getExchangeEligibilityStatus().toString()))) || 
				(enrolledApplication.getExchangeEligibilityStatus().toString().contains("APTC") 
						   && ("QHP".equalsIgnoreCase(currentApplication.getExchangeEligibilityStatus().toString()) 
						   || "CSR".equalsIgnoreCase(currentApplication.getExchangeEligibilityStatus().toString())))) 
		{
			manageApplicationApplicantEventForConversion(currentApplication, "QE", applicantExtensionEvents);
			changeInEligibility = true;
		}else {
			boolean isAPTCIncreased;

			if(currentApplication.getMaximumAPTC() != null && enrolledApplication.getMaximumAPTC() != null) {
                isAPTCIncreased = currentApplication.getMaximumAPTC().compareTo(enrolledApplication.getMaximumAPTC()) == 1;
            }
			else{
				isAPTCIncreased = false;
			}

			createAPTCUpdateEvent(currentApplication, applicationExtension,isAPTCIncreased);
		}
		Date coverageStartDate = updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		Optional<ApplicationValidationStatus> validationStatus = getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplicationId);//HIX-108047
		
		if(isApplicationValidated(validationStatus)){
			final AptcUpdate aptcUpdate = populateAptcUpdateRequest(currentApplication, enrolledApplicationId,coverageStartDate);
			if (aptcUpdate == null) {
				LOGGER.info("Health/Dental enrollment was not found - " + currentApplication.getId());
				failedAPTCChanges(currentApplication, enrolledApplicationId, applicationExtension,lceProcessRequestDTO);
			} else {
				
				if(enrolledApplication.getExchangeEligibilityStatus().toString().contains("APTC") && 
						"QHP".equalsIgnoreCase(currentApplication.getExchangeEligibilityStatus().toString()))  //For F-NF
				{
					aptcUpdate.setIsNonFinancialConversion(true);
				}
				String changePlan = "N";
				if(changeInEligibility) {
					changePlan = "Y";
				}
				//final String status = executeAptcChangesApi(currentApplication, enrolledApplicationId, aptcUpdate);
				String previousApplicationStatus = enrolledApplication.getApplicationStatus();
				String previousApplicationDentalStatus = enrolledApplication.getApplicationDentalStatus();
				final boolean status = ind71GCall(lceProcessRequestDTO.getEnrolledApplicationId(), lceProcessRequestDTO.getCurrentApplicationId(),currentApplication.getCoverageYear(), aptcUpdate,changePlan) ;
				if (status) {
					currentApplication.setEffectiveDate(EligibilityUtils.getDateWithoutTimeUsingFormat(aptcUpdate.getAptcEffectiveDate()));
					successAPTCChanges(currentApplication, enrolledApplicationId, applicationExtension,previousApplicationStatus,previousApplicationDentalStatus);
				} else {
					failedAPTCChanges(currentApplication, enrolledApplicationId, applicationExtension,lceProcessRequestDTO);
				}
			}
		}
		else{
			failedAPTCChanges(currentApplication, enrolledApplicationId, applicationExtension,lceProcessRequestDTO);
		}
		super.closePreviousERApplication(currentApplication, lceProcessRequestDTO);
		LOGGER.info("LceAptcOnlyHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}


	private void createAPTCUpdateEvent(SsapApplication currentApplication, ApplicationExtension applicationExtension,boolean isAPTCIncreased) {
		lceAppExtensionEvent.createAptcOnlyEvent(currentApplication, applicationExtension,isAPTCIncreased);
	}

	private void successAPTCChanges(SsapApplication currentApplication, long enrolledApplicationId, ApplicationExtension applicationExtension, String previousApplicationStatus,String previousApplicationDentalStatus) {
		LOGGER.info("Handle Success APTC changes case for Application id - " + currentApplication.getId());
		final SsapApplication enrolledApplication = loadApplication(enrolledApplicationId);
		/*final String status = moveEnrollmentToNewApplication(currentApplication, enrolledApplication);
		if (!status.equals(GhixConstants.RESPONSE_SUCCESS)) {
			throw new GIRuntimeException(ERROR_MOVE_ENROLLMENT + currentApplication.getId());
		}*/
		//HIX-109703
		//String previousApplicationStatus = enrolledApplication.getApplicationStatus();
		super.closeSsapApplication(enrolledApplication);
		currentApplication.setApplicationStatus(previousApplicationStatus);	
		currentApplication.setApplicationDentalStatus(previousApplicationDentalStatus);
		super.updateSsapApplication(currentApplication);
		//updateCurrentAppToEN(currentApplication);
		logAPTCChangeEvent(currentApplication, enrolledApplication);
		copyEHBAmount(currentApplication, enrolledApplication);
		updateAllowEnrollment(currentApplication, Y);
		sendNotification(currentApplication,enrolledApplicationId);

	}


	private void sendNotification(SsapApplication currentApplication,long enrolledApplicationId) {
		// TODO : the below notice will be triggered in case of MN - we need to remove this once content is finalized
		String stateCode = DynamicPropertiesUtil
				.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if (stateCode.equalsIgnoreCase("MN")) {
			// new eligible for APTC only
			boolean checkAPTCEligibilityOnly = checknewEigibleAptcOnlyHH(currentApplication,enrolledApplicationId);		
			// check loss of aptc eligibility only
			boolean checkLossofAPTCEligibilityOnly = checklossofEligiblityAptcOnlyHH(currentApplication,enrolledApplicationId);
			if(checkAPTCEligibilityOnly) {
				// trigger EE032(newly eligible for APTC) 
				triggerNFtoFinancialConversionEmail(currentApplication);
			} else if (checkLossofAPTCEligibilityOnly) {
				// trigger EE056(loss of eligiblity for APTC) 
				triggerNonFinConversionNoticeWithAptcIneligibile(currentApplication.getCaseNumber());
			} else {
				// max aptc amount only change - trigger EE053
				triggerAPTCSuccessEmail(currentApplication);		
			}
		} else {
			triggerAPTCSuccessEmail(currentApplication);	
		}
	}


	private void failedAPTCChanges(SsapApplication currentApplication, long enrolledApplicationId, ApplicationExtension applicationExtension, LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("Handle Failure APTC changes case for Application id - " + currentApplication.getId());
		updateAllowEnrollment(currentApplication, Y);
		updateCurrentAppToER(currentApplication);
		triggerChangeActionEmail(currentApplication,lceProcessRequestDTO,true);
	}
	
	private void logAPTCChangeEvent(SsapApplication currentApplication, SsapApplication enrolledApplication){
		EventInfoDto eventInfoDto = new EventInfoDto();
		Map<String, String> mapEventParam = new HashMap<String, String>();
		
		//event type and category
		LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(APP_EVENT_TYPE,APP_EVENT_APTC_CHANGE);
		eventInfoDto.setEventLookupValue(lookupValue);
		mapEventParam.put("Current APTC", String.valueOf(currentApplication.getMaximumAPTC()));
		mapEventParam.put("Previous APTC", String.valueOf(enrolledApplication.getMaximumAPTC()));

		if(currentApplication.getMaximumStateSubsidy() != null || enrolledApplication.getMaximumStateSubsidy() != null){
            mapEventParam.put("Current CA Premium Subsidy", String.valueOf(currentApplication.getMaximumStateSubsidy()));
            mapEventParam.put("Previous CA Premium Subsidy", String.valueOf(enrolledApplication.getMaximumStateSubsidy()));
        }

		eventInfoDto.setModuleId(currentApplication.getCmrHouseoldId().intValue());
		eventInfoDto.setModuleName(GhixRole.INDIVIDUAL.toString());
		appEventService.record(eventInfoDto, mapEventParam);
	}
	
	private void manageApplicationApplicantEventForConversion(SsapApplication currentApplication, String accountTransferCategory, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		LOGGER.info("manageApplicationApplicantEventForConversion  for application - " + currentApplication.getId());
		if (AccountTransferCategoryEnum.QE.value().equals(accountTransferCategory)) {
			ssapApplicationEventService.createAndUpdateApplicationEventForNonFinancialQE(currentApplication, SsapApplicationEventTypeEnum.SEP, ReferralConstants.Y, applicantExtensionEvents);
		} else if (AccountTransferCategoryEnum.OE.value().equals(accountTransferCategory)) {
			ssapApplicationEventService.updateSsapApplicationEventForNonFinancial(currentApplication, SsapApplicationEventTypeEnum.SEP, ReferralConstants.Y);
		}
	}
}
