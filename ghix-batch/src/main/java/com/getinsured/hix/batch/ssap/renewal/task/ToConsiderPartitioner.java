package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.ssap.renewal.service.ToConsiderService;
import com.getinsured.hix.batch.ssap.renewal.util.RenewalUtils;
import com.getinsured.hix.batch.ssap.renewal.util.ToConsiderPartitionerParams;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * ToConsiderPartitioner class is used to get read SsapApplication IDs from database and do Partitioner.
 * 
 * @since July 19, 2020
 */
@Component("toConsiderPartitioner")
@Scope("step")
public class ToConsiderPartitioner implements Partitioner {

	private static final Logger LOGGER = LoggerFactory.getLogger(ToConsiderPartitioner.class);
	private static final String RENEWAL_TO_CONSIDER_JOB = "ssapToConsiderJob";
	private static final String STATE_CODE = "CA";
	
	private Integer serverCount;
	private Integer serverName;
	private Long batchSize;
	private String toConsiderCommitInterval;
	private ToConsiderService toConsiderService;
	private BatchJobExecutionService batchJobExecutionService;
	private ToConsiderPartitionerParams toConsiderPartitionerParams;
	private String processErrorRecords;
	
	
	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		int servName = 0;
		int servCount = 1;

		Long renewalYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
		
		LOGGER.info("Renewal ToConsider Batch job to Process Applications with batchSize: {}, toConsiderCommitInterval: {}, renewalYear: {}", batchSize, toConsiderCommitInterval, renewalYear);
		
		Map<String, ExecutionContext> partitionMap = null;
		StringBuffer errorMessage = new StringBuffer();
		String errorCode = "";
		try 
		{
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			if (!hasRunningBatchSizeOne() && !STATE_CODE.equalsIgnoreCase(stateCode)) {
				errorMessage.append(RenewalUtils.EMSG_RUNNING_BATCH);
				return partitionMap;
			}
			
			String defaultBatchSize = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_BATCHSIZE);
			if(batchSize == null || batchSize == 0L){
				batchSize = Long.valueOf(defaultBatchSize);
			}
			if (!validateParams(errorMessage)) {
				return partitionMap;
			}

			List<Long> ssapApplicationIdsList = null;
			if(StringUtils.isNotBlank(processErrorRecords) && "Y".equalsIgnoreCase(processErrorRecords))
			{
				toConsiderPartitionerParams.setProcessErrorRecords(processErrorRecords);
				toConsiderService.processErrorApplications(renewalYear, servCount, servName);
			}
			
			if(STATE_CODE.equalsIgnoreCase(stateCode)) {
				if(serverCount!=null && serverName!=null){
					if(serverCount <= 0 || serverName>=serverCount){
						throw new GIException("Invalid Server Count or Server Name");
					}
					servName = serverName;
					servCount = serverCount;
				}
			} 
			
			ssapApplicationIdsList = toConsiderService.getSsapApplicationIdsByServerName(renewalYear, servCount, servName, batchSize);
			toConsiderPartitionerParams.setServerName(servName);
			
			partitionMap = new HashMap<String, ExecutionContext>();

			if (CollectionUtils.isNotEmpty(ssapApplicationIdsList)) 
			{
				LOGGER.info("Number of ssapApplicationIds for renewal : {}", ssapApplicationIdsList.size());
				toConsiderPartitionerParams.clearSsapApplicationIdList();
				toConsiderPartitionerParams.addAllToSsapApplicationIdList(ssapApplicationIdsList);

				int maxCommitInterval = 1;
				int size = ssapApplicationIdsList.size();

				if (StringUtils.isNumeric(toConsiderCommitInterval)) {
					maxCommitInterval = Integer.valueOf(StringUtils.trim(toConsiderCommitInterval));
				}

				int numberOfApplicationIdToCommit = size / maxCommitInterval;
				if (size % maxCommitInterval != 0) {
					numberOfApplicationIdToCommit++;
				}

				int firstIndex = 0;
				int lastIndex = 0;

				for (int i = 0; i < numberOfApplicationIdToCommit; i++) {
					firstIndex = i * maxCommitInterval;
					lastIndex = (i + 1) * maxCommitInterval;

					if (lastIndex > size) {
						lastIndex = size;
					}
					ExecutionContext value = new ExecutionContext();
					value.putInt("startIndex", firstIndex);
					value.putInt("endIndex", lastIndex);
					value.putInt("partition", i);
					value.putString("PROCESS_ERROR_RECORDS", processErrorRecords);
					partitionMap.put("partition - " + i, value);
				}
			}
			else {
				String message = "No application found to renew for year : " + renewalYear;
				Integer giMonitorId = toConsiderService.logToGIMonitor(message,"RENEWALBATCH_50008");
				LOGGER.info("Message: {} - MonitorId: {}", message, giMonitorId);
			}
		}
		catch (Exception ex) {
			errorMessage.append("ToConsiderPartitioner failed to execute : ").append(ex.getMessage());
			errorCode = "RENEWALBATCH_50009";
			LOGGER.error(errorMessage.toString(), ex);
		}
		finally {

			if (StringUtils.isNotBlank(errorMessage)) {
				toConsiderService.saveAndThrowsErrorLog(errorMessage.toString(),errorCode);
			}
		}
		return partitionMap;
	}

	private boolean validateParams(StringBuffer errorMessage) {

		boolean hasValidParams = true;

		if (null == batchSize || 0 == batchSize) {
			errorMessage.append("Invalid batch size : ");
			errorMessage.append(batchSize);
			hasValidParams = false;
		}

		if (!hasValidParams) {
			LOGGER.error(errorMessage.toString());
		}
		return hasValidParams;
	}

	/**
	 * Method is used to get Running Batch List.
	 */
	private boolean hasRunningBatchSizeOne() {

		boolean hasRunningBatchSizeOne = false;

		List<BatchJobExecution> batchExecutionList = batchJobExecutionService.findRunningJob(RENEWAL_TO_CONSIDER_JOB);
		if (batchExecutionList != null && batchExecutionList.size() == 1) {
			hasRunningBatchSizeOne = true;
		}
		return hasRunningBatchSizeOne;
	}

	public Long getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(Long batchSize) {
		this.batchSize = batchSize;
	}

	public String getToConsiderCommitInterval() {
		return toConsiderCommitInterval;
	}

	public void setToConsiderCommitInterval(String toConsiderCommitInterval) {
		this.toConsiderCommitInterval = toConsiderCommitInterval;
	}

	public ToConsiderService getToConsiderService() {
		return toConsiderService;
	}

	public void setToConsiderService(ToConsiderService toConsiderService) {
		this.toConsiderService = toConsiderService;
	}

	public ToConsiderPartitionerParams getToConsiderPartitionerParams() {
		return toConsiderPartitionerParams;
	}

	public void setToConsiderPartitionerParams(
			ToConsiderPartitionerParams toConsiderPartitionerParams) {
		this.toConsiderPartitionerParams = toConsiderPartitionerParams;
	}

	public BatchJobExecutionService getBatchJobExecutionService() {
		return batchJobExecutionService;
	}

	public void setBatchJobExecutionService(BatchJobExecutionService batchJobExecutionService) {
		this.batchJobExecutionService = batchJobExecutionService;
	}

	public String getProcessErrorRecords() {
		return processErrorRecords;
	}

	public void setProcessErrorRecords(String processErrorRecords) {
		this.processErrorRecords = processErrorRecords;
	}
	
	public Integer getServerCount() {
		return serverCount;
	}

	public void setServerCount(Integer serverCount) {
		this.serverCount = serverCount;
	}

	public Integer getServerName() {
		return serverName;
	}

	public void setServerName(Integer serverName) {
		this.serverName = serverName;
	}

}
