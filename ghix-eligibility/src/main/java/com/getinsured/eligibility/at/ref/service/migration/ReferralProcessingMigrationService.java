package com.getinsured.eligibility.at.ref.service.migration;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.referral.ui.dto.PendingReferralDTO;
import com.getinsured.eligibility.referral.ui.dto.ReferralVerification;
import com.getinsured.hix.dto.consumer.ssap.AptcHistoryRequest;
import com.getinsured.iex.referral.ReferralLinkingOverride;
import com.getinsured.iex.ssap.model.SsapApplicant;

/**
 * Process the request for Referrals
 * 
 * @author chopra_s
 * 
 */
public interface ReferralProcessingMigrationService {

	String INCOMING_DATA_DATE_FORMAT_NOT_PROPER = "Incoming data Date Format not proper -";

	String PRIMARY_APPLICANT_NOT_FOUND_FOR_SSAP_APPLICATION_ID = "Primary Applicant not found for ssapApplicationId - ";

	String DOB2 = "dob";

	String SSN_NUMBER = "ssnNumber";

	String LAST_NAME = "lastName";

	String FIRST_NAME = "firstName";

	String INCOMING_DATA_NOT_PROPER = "Incoming data not proper - ";

	String UPDATION_NOT_ALLOWED = "No updation is allowed as data not proper";

	String INVALID_WORKFLOW = "No updation is allowed on invalid workflow status";

	String NO_REFERRALACTIVATION_FOUND = "No Referral Activation data found for this Id - ";

	String NO_SSAP_FOUND = "No SSapapplication found for this application - ";

	String UNEXPECTED_ERROR = "Unexpected error occurred while processing the request.";

	String NO_DATA = "No Data found for primary contact.";

	String PROCESS_SUCCESS = "Referral processing completed successfully.";

	String PARSE_EXCEPTION = "Error occurred while parsing date.";

	int CSR_VERIFICATION_PENDING = 1;

	String executeReferral(AccountTransferRequestDTO accountTransferRequest);

	long executeOeReferral(AccountTransferRequestDTO accountTransferRequest) throws Exception;
	
	long executeLceReferral(AccountTransferRequestDTO accountTransferRequest) throws Exception;

	long executeQeReferral(AccountTransferRequestDTO accountTransferRequest) throws Exception;

	long executeRenewalReferral(AccountTransferRequestDTO accountTransferRequest) throws Exception;
	
	ReferralActivation referralActivationById(long id);

	ReferralActivation referralActivationBySsapApplication(int id);

	ReferralActivation referralActivationByUserAndSsapApplication(int user, int ssapApplication);

	ReferralActivation referralActivationByCmrAndSsapApplication(int cmrHouseholdId, int ssapApplication);

	Long linkUserCmrActivation(Integer[] arguments);

	Integer validateReferralAnswers(ReferralVerification referralVerification);

	Integer updateActivationStatus(Integer[] arguments);

	Integer persistCoverageYear(Integer[] arguments);

	Map<String, String> processRidp(Integer[] arguments);

	List<PendingReferralDTO> pendingReferralsForCmr(int cmrHousehold);

	Map<String, String> linkingStatusForCSR(long ssapplicationId, BigDecimal cmrhouseholdid);

	long getActiveApplicationCountForCMR(int cmrhouseholdId,int currSsapApplicationId);
	
	Map<Integer, String> getSsapQuestions(String applicantId, boolean isCsr);
	
	SsapApplicant getSsapApplicant(String applicantId);
	
	String getApplicantGuid(String ssapApplicationId);
	
	Integer updateReferralActivation(String[] arguments);

	int checkHouseholdExistsAndThenCreate(long ssapApplicationId,String householdCaseID);

	ReferralLinkingOverride compareHouseholdAndPrimary(long ssapApplicationId, int householdId);
	
	Integer updateReferralCount(String referralId);
	
	Integer updateReferralSuccessCount(String referralId);
	
	Map<Integer, String> getSsapQuestionsFromHome(String applicantId,boolean isCsr);

	Integer findHouseholdFromSsnDoB(long ssapApplicationId);
	
	Map<String,BigDecimal> getMonthWiseAPTC(AptcHistoryRequest aptcHistoryRequest);
}
