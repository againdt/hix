package com.getinsured.eligibility.at.outbound.service;

import java.util.List;

import com.getinsured.iex.ssap.model.OutboundATApplication;

public interface OutboundATApplicationService {
	
	List<OutboundATApplication> findBySsapApplicationId(long ssapApplicationId);

	boolean isOuboundATSent(long ssapApplicationId);
	
}