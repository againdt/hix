package com.getinsured.hix.batch.ssap.renewal.service;

import java.util.List;

import org.springframework.batch.core.UnexpectedJobExecutionException;

/**
 * Interface is used to provide services for Outbound.
 * 
 * @since September 12, 2019
 */
public interface OutboundService {

	String JOB_NAME = "ssapRedeterminationOutboundJob";
	String ERROR_CODE = "RENEWALBATCH_50014";

	void saveAndThrowsErrorLog(String errorMessage) throws UnexpectedJobExecutionException;

	Integer logToGIMonitor(Exception e, String caseNumber);

	List<Long> getSsapApplIDsByCoverageYearAndApplStatusAndEligStatus(long coverageYear,
			List<String> applicationStatusList, Long batchSize);

	String outboundSsap(Long applicationId);
}
