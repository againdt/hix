package com.getinsured.hix.indportal.dto;

/**
 * Story HIX-59165
 * Display Member Level Coverage for eligible 
 * members of the HH after Change Reporting
 * 
 * Sub-task HIX-59822
 * Update PlanSummaryDetails DTO to 
 * include coverage start and end date for Member
 * 
 * @author Nikhil Talreja
 * 
 */
public class PlanSummaryMember {
	
	private String name;
	
	private String coverageStartDate;
	
	private String coverageEndDate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public String getCoverageEndDate() {
		return coverageEndDate;
	}

	public void setCoverageEndDate(String coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}
	
}
