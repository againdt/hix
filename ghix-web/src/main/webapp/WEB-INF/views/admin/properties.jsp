<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page isELIgnored="false"%>
<div class="gutter10">
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
                    <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
                    <li><a href="<c:url value="/admin/property"/>"><spring:message   text="Application Properties List"/></a></li>
                    <li><spring:message  text = '' /></li>
                </ul><!--page-breadcrumb ends-->
		<h1><a name="skip"></a>${Header}&nbsp;&nbsp;<small><span id="propertiesResultSize">${resultSize}</span>&nbsp;<spring:message   text="total properties"/></small></h1>
    </div><!--  end of row-fluid -->
    <div class="row-fluid">
    	
    	
        <div class="" id="">
			<form class="form-vertical gutter10 lightgray" id="frm_search" action="<c:url value='/admin/property?ACTION=${ACTION}'/>" method="GET">
				<input type="hidden" name="ACTION" id="frm_search_section" value="${ACTION}" />
				<div class="form-horizontal">
					<input type="text" id="frm_search_term" name="term" class="form-control" style="width:770px;" placeholder="Search for properties starting with: ${ACTION}" />
					<button id="frm_cancel_btn" class="btn btn-default" style="display:none;">Cancel Search</button>
				</div>
			</form>
			<style type="text/css">
				.selection {
					background-color: rgba(255,255,0,1); // rgba(255,255,0,1);//<-- yellow // rgba(75,167,96,0.35)//<-- GI
				}
			</style>
			<script type="text/javascript">
				$("#frm_search_term").keyup(function() {
					var term = $("#frm_search_term").val();
					var section = $("#frm_search_section").val();
					var url = '<c:url value='/admin/property/search'/>?section=' + section + "&term=" + term;

					$.getJSON(url, function(data) {
						var output = "";
						$(".pagination").hide();
						$("#frm_cancel_btn").show();
						$("#propertiesResultSize").text(0);
						if(data != null && "" != data && data.length > 0)
						{
							$("#propertiesResultSize").text(data.length);
							for (var i = 0; i < data.length; i++) {
								var prop_id = data[i].id;
								var prop_key = data[i].propertyKey;
								var prop_val = data[i].propertyValue;
								var prop_desc = data[i].description;

								if(prop_val == null)
								{
									prop_val = "";
								}
								if(prop_desc == null)
								{
									prop_desc = "";
								}

								output += "<input type='hidden' id='id" + i +"' name='id' value='" + prop_id + "' />";
								output += "<input type='hidden' id='desc" + i +"' name='desc' value='" + prop_desc + "' />";

								output += "<tr>";
								output += '<td align="center" style="width:250px"><input type="hidden" id="propertyKey' + i +'" name="propertyKey" value="'+ prop_key + '">';
								output += '<span id="propertyValueSpan' + i +'">' +  highlightSelectionInKey(prop_key, term, section) + '</span></td>';

								output += "<td><input type='text' id='propertyValue" + i +"' style='width:200px' value='" + prop_val + "'/></td>";

								output += "<td>" + prop_desc + "</td>";
								output += "<td><input type='button' value='Save' class='btn btn-primary btn-small' onclick='saveDetails(" + i + ")' />";
								output += "</tr>";
							}
						}
						else
						{
							output += "<tr><td colspan='4'><ul><li>No properties that matched your term.</li></ul></td></tr>";
						}
						$("#tbody_result").html(output);
					});
				});

				function highlightSelectionInKey(keyString, highlightPart, section)
				{
					try {
						var originalKey = keyString;

						originalKey = originalKey.replace(new RegExp(section), "").substring(1);
						var regex = new RegExp(highlightPart, "i");
						var newValue = originalKey.replace(regex, function (x) {
							return "<span class='selection'>" + x + "</span>";
						});

						newValue = section + "." + newValue;

						return newValue;
					}
					catch(e) {

					}
					return keyString;
				}
			</script>
	        <form class="form-vertical gutter10 lightgray" id="frmprops" name="frmprops"  action="<c:url value="/admin/property?ACTION=${ACTION}" />" method="POST">
				
				
				<%int icount = 0;%>
			<c:choose>
						<c:when test="${fn:length(giAppProperties) > 0}">
						
							<table class="table table-striped">
								<thead>
									<tr class="header">
										<th scope="col">Property Key</th>
								     	<th scope="col">Property Value</th>
								     	<th scope="col">Property Desc</th>
								     	<th scope="col">Submit</th>
								     </tr>
								</thead>
								<tbody id="tbody_result">
									<c:forEach items="${giAppProperties}" var="giAppProperties">
									<%icount++ ;%>
									<input type="hidden" id="id<%=icount %>" name="id" value="${giAppProperties.id}" >
									<input type="hidden" id="desc<%=icount %>" name="desc" value="${giAppProperties.description}" >
										<tr>
											<td align="center"><input type="text" style="font-weight:bold;width:300px" id="propertyKey<%=icount %>" name="propertyKey" value="${giAppProperties.propertyKey}" readonly="readonly"  class="xlarge" ></td>

											<td align="center"><input type="text" style="width:200px" id="propertyValue<%=icount %>" name="propertyValue" value="${giAppProperties.propertyValue}"  class="xlarge"></td>

											<td align="center" style="word-break: break-all">${giAppProperties.description}</td>
											<td align="center"><input type = "button" width = "100px" class="btn btn-primary btn-small" id="save-user" value="Save" onclick="saveDetails(<%=icount %>)" ></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<div class="center"><dl:paginate resultSize="12" pageSize="12" hideTotal="true"/></div>
						</c:when>
						
						<c:otherwise>
						<div class="alert alert-info"><spring:message  code='issuer.resultList'/></div>
					</c:otherwise>
			</c:choose>
			
			
			
			</form>			
        	<form style="display:none" id="submissionForm" name="submissionForm"  action="<c:url value="/admin/property?ACTION=${ACTION}" />" method="POST">
        		<df:csrfToken/>
        		<input type="hidden" name="actualpropertyKey" id="actualpropertyKey" value="">
				<input type="hidden" name="actualpropertyValue" id="actualpropertyValue" value="">
				<input type="hidden" name="actualId" id="actualId" value="">
				<input type="hidden" name="description" id="description" value="">
				<input type="hidden" name="ACTION" id="ACTION" value="${ACTION}">
        	</form>
        
        	
				
				</div>
    </div><!--  end of row-fluid -->
</div>    	<!--  end of gutter10 -->

<div id="dialog-modal" style="display: none;">
  <p><spring:message  text='Property Value Updated Successfully'/></p>
</div>
<div id="dialog-modal_error" style="display: none;">
<p>Message: ${sessionErrorInfo}</p>
</div>
<div id="dialog-modal-confirm" style="display: none;">
<p>Property Value Would Be Updated</p>
</div>
<script type="text/javascript">
var icount = "";
//window.onload = closeSuccess;
 $('#get-prop').click(function (){
	 
	 var value = document.getElementById('propertyKey').value ;
   	 var url='<c:url value="/admin/propertyValue/'+ value +'"/>';
	 $.get(url,function() {
		location.href='<c:url value="/admin/propertyValue/'+ value +'"/>'
	 });
	 
  });

 function saveDetails(i){

	 document.getElementById("actualId").value =   document.getElementById("id"+i).value ; 
	 document.getElementById("description").value =   document.getElementById("desc"+i).value ; 
	 document.getElementById("actualpropertyKey").value =   document.getElementById("propertyKey"+i).value;
	 document.getElementById("actualpropertyValue").value =   document.getElementById("propertyValue"+i).value ;

	
	 
	 $( "#dialog-modal-confirm" ).dialog({
		     modal: true,
			 title: "<spring:message  text='Please Confirm'/>",
			 buttons: {
			   Ok: function() {
				   $("#submissionForm").submit();
			    $( this ).dialog( "close" );
			   }
			   }
		    });
	
 }


 function closeSuccess() {
	 	//sleep(3000); 
	 	//alert('hi');
		$("#success").remove();
		$('.modal-backdrop').remove();
		//window.location.replace = "/hix/broker/viewprofile";
	}


 $(document).ready(function()
		 {
	 
	 		//alert("Success Message: "+'${successMsg}'); 
		 	if('${successMsg}' != null && '${successMsg}' != '' && '${successMsg}' != null && '${successMsg}' != '')
		 	{
		 		 $( "#dialog-modal" ).dialog({
		 		     modal: true,
		 			 title: "<spring:message  text='${successMsg}'/>",
		 			 buttons: {
		 			   Ok: function() {
		 			    $( this ).dialog( "close" );
		 			   }
		 			   }
		 		    });
		 	}
		 	else if('${sessionErrorInfo}' != null && '${sessionErrorInfo}' != '')
		 	{
		 		$( "#dialog-modal_error" ).dialog({
		 		      height: 140,
		 		      modal: true,
		 		      title: "<spring:message  code='label.propertyError'/>",
		 		      buttons: {
		 					Ok: function() {
		 					$( this ).dialog( "close" );
		 					}
		 					}
		 		    });			
		 	}
		 	
		 	$('body').click(function(){
		 		$('.popover').fadeOut();
		 	});
		 });
</script>
