To simplify you development process, there is a script which you can:

* call by hand from the terminal
* integrate with other scripts 
* configure Intellij instance 

to install correct `configuration.properties` file before launching tomcat 
instance, so that way you won't start by accident your tomcat which will use incorrect configuration.properties.

This README file will show you how to configure Intellij for this.

Follow three simple steps:

* Select `Edit Configurations` from the launch configuration drop down:

![Shows configuration step 1](intellij-1.png)

* select configuration you want to edit.
  * At the bototm you will see `Before launch: Build, Build Artifacts`, click `+` to add <br/>
  new build step.
* select `Run External Tool`

![Shows configuration step 2](intellij-2.png)

In `Create Tool` window:
* Select program `devconf.py` (in current directory, along with this README.md)
* Enter distinguished `name`, and `description`.
* Add arguments: here type partial server name, such as camainqa, id6dev, nv2dev, etc. <br/>
  this name will be pattern matched against config properties located in current directory!
  <br/>
  For example if you enter `ide2e`, script will look for property files such as:
  * configuration.ide2e.properties
  * configuration.properties.ide2e
  * ide2e.configuration.properties
  * ide2e.properties
  * ide2e.configuration
* Select `Working directory` to be current directory (where `devconf.py` file is)
* Mark 'Synchronize files after execution'
* Save and close dialog.
![Shows configuration step 3](intellij-3.png)

<br/>
After you have added this custom step, your `Before launch:` list should look something like this
<br/>


![Shows configuration step 4](intellij-4.png)

Now you can launch your server as you did before. After it builds needed war file,
it will execute this script. In `Run` window you will see something like this if it
succeeded:

![Shows output of the custom script](intellij-5.png)


