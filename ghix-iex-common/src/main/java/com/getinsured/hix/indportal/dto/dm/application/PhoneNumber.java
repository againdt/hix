package com.getinsured.hix.indportal.dto.dm.application;

public class PhoneNumber {
	 private String number;
	 private String type;
	 private String ext;


	 // Getter Methods 

	 public String getNumber() {
	  return number;
	 }

	 public String getType() {
	  return type;
	 }

	 public String getExt() {
	  return ext;
	 }

	 // Setter Methods 

	 public void setNumber(String number) {
	  this.number = number;
	 }

	 public void setType(String type) {
	  this.type = type;
	 }

	 public void setExt(String ext) {
	  this.ext = ext;
	 }
	}
