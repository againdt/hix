package com.getinsured.hix.batch.agency.service;

public class AgencyMigrationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Long agencyId;
	String agentId;
	Long rowNo;
	
	public AgencyMigrationException(String message){
		super(message);
	}
	
	

	public AgencyMigrationException(Long agencyId, String agentId, Long rowNo,String message) {
		super(message);
		this.agencyId = agencyId;
		this.agentId = agentId;
		this.rowNo = rowNo;
	}



	public Long getAgencyId() {
		return agencyId;
	}

	public String getAgentId() {
		return agentId;
	}

	public Long getRowNo() {
		return rowNo;
	}
	
	
	
	

}
