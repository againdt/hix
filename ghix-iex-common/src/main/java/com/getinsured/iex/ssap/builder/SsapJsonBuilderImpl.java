package com.getinsured.iex.ssap.builder;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.*;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.IexEnumAdapterFactory;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

/**
 * @author chopra_s
 * 
 */
@Component("ssapJsonBuilder")
@Scope("singleton")
public class SsapJsonBuilderImpl implements SsapJsonBuilder {
	private static final Logger LOGGER = Logger.getLogger(SsapJsonBuilderImpl.class);

	private static final String ERROR_IN_JSON_STRING = "Json String is not in correct format";

	private static final Type MAP_SSAP_TYPE = new TypeToken<Map<String, SingleStreamlinedApplication>>() {
	}.getType();

	private static Gson gson = null;
	static {
		final GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapterFactory(new IexEnumAdapterFactory());
		gsonBuilder.serializeNulls();
		//gsonBuilder.excludeFieldsWithoutExposeAnnotation();
		gsonBuilder.setDateFormat(ReferralConstants.JSON_DATE_FORMAT);
		gsonBuilder.serializeSpecialFloatingPointValues();
		gson = gsonBuilder.create();
	}

	@Override
	public void buildForJson(SingleStreamlinedApplication singleStreamlinedApplication) {
		final int size = ReferralUtil.listSize(singleStreamlinedApplication.getTaxHousehold());
		if (size != 0) {
			modifyHousehold(singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember());
		}
	}

	private void modifyHousehold(List<HouseholdMember> members) {
		final int size = ReferralUtil.listSize(members);
		if (size == 0) {
			return;
		}
		HouseholdMember householdMember = null;
		for (int i = 0; i < size; i++) {
			householdMember = members.get(i);
			updateImmigrationDetails(householdMember);
			updtaeHealthcoverageDetails(householdMember);
			updateSSN(householdMember);
		}
	}

	private void updateSSN(HouseholdMember householdMember) {

		if(ReferralUtil.isNotNullAndEmpty(householdMember.getSocialSecurityCard().getSocialSecurityNumber()))
		{
			householdMember.getSocialSecurityCard().setSocialSecurityCardHolderIndicator(true);
		}
	}

	private void updateImmigrationDetails(HouseholdMember householdMember) {
		CitizenshipImmigrationStatus citImmigrationStatus = householdMember.getCitizenshipImmigrationStatus();
		int size = ReferralUtil.listSize(citImmigrationStatus.getEligibleImmigrationDocumentType());
		if (size == 0) {
			citImmigrationStatus.getEligibleImmigrationDocumentType().add(new EligibleImmigrationDocumentType());
		}

	}

	private void updtaeHealthcoverageDetails(HouseholdMember householdMember) {
		HealthCoverage healthCoverage = householdMember.getHealthCoverage();
		int size = ReferralUtil.listSize(healthCoverage.getCurrentEmployer());
		if (size == 0) {
			healthCoverage.getCurrentEmployer().add(new CurrentEmployer());
		}

		size = ReferralUtil.listSize(healthCoverage.getFormerEmployer());
		if (size == 0) {
			healthCoverage.getFormerEmployer().add(new FormerEmployer());
		}

	}

	@Override
	public String transformToJson(SingleStreamlinedApplication singleStreamlinedApplication) {
		String sReturn = null;
		Map<String, SingleStreamlinedApplication> data = new HashMap<String, SingleStreamlinedApplication>();
		data.put("singleStreamlinedApplication", singleStreamlinedApplication);
		sReturn = gson.toJson(data);
		return sReturn;
	}

	@Override
	public String modifyJsonForClonedApp(SsapApplication ssapApplication) {
		String ssapJsonString = null;
		JSONObject ssapJsonObj;
		try {
			ssapJsonString = ssapApplication.getApplicationData();
			ssapJsonObj = new JSONObject(ssapJsonString);
			JSONObject singleStreamlinedApplicationJson = ssapJsonObj.getJSONObject("singleStreamlinedApplication");
			singleStreamlinedApplicationJson.put("applicationGuid", "" + ssapApplication.getCaseNumber());
			singleStreamlinedApplicationJson.put("ssapApplicationId", "" + ssapApplication.getId());
			modifySsapApplicants(singleStreamlinedApplicationJson, ssapApplication);
			ssapJsonString = ssapJsonObj.toString();
		} catch (JSONException jsonException) {
			LOGGER.error(ERROR_IN_JSON_STRING, jsonException);
			throw new GIRuntimeException(jsonException);
		}
		return ssapJsonString;
	}

	private void modifySsapApplicants(JSONObject singleStreamlinedApplicationJson, SsapApplication ssapApplication) throws JSONException {
		JSONArray jsonArray = (JSONArray) singleStreamlinedApplicationJson.get("taxHousehold");
		if (jsonArray == null || jsonArray.length() == 0) {
			return;
		}

		jsonArray = (JSONArray) ((JSONObject) jsonArray.get(0)).get("householdMember");
		if (jsonArray == null || jsonArray.length() == 0) {
			return;
		}
		final int size = jsonArray.length();
		JSONObject member;
		SsapApplicant ssapApplicant;
		int personId;
		for (int i = 0; i < size; i++) {
			member = (JSONObject) jsonArray.get(i);
			personId = (int) member.get("personId");
			ssapApplicant = ReferralUtil.retreiveApplicant(ssapApplication, personId);
			member.put("applicantGuid", ssapApplicant.getApplicantGuid());
		}
	}

	@Override
	public SingleStreamlinedApplication transformFromJson(String json) throws JsonSyntaxException  {
		final Map<String, SingleStreamlinedApplication> data = gson.fromJson(json, MAP_SSAP_TYPE);
		return data.get("singleStreamlinedApplication");
	}

}
