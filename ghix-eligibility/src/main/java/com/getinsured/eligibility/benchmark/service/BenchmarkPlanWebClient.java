package com.getinsured.eligibility.benchmark.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.webclient.SoapWebClient;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanRequest;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanResponse;


/**
 * Assumptions:
 * 	All members in the tax household section of ssap json would be send to BenchMark Service.
 *
 *
 * @author kazi_e
 *
 */
@Component
@DependsOn("ghixEndPoints")
public class BenchmarkPlanWebClient {

	private static final String BENCH_MARK_PLAN_SERVICE_WSDL = "webservice/planmgmt/endpoints/BenchMarkPlanService.wsdl";

	private static final String ENDPOINT_FUNCTION = "getBenchMarkPremium";

	@Autowired private SoapWebClient<GetBenchmarkPlanResponse> soapWebClient;
	@Autowired private WebServiceTemplate benchmarkWebServiceTemplate;

	private static final String WSDL_URL = GhixEndPoints.PLAN_MGMT_URL + BENCH_MARK_PLAN_SERVICE_WSDL;

	public GetBenchmarkPlanResponse invokeBenchMarkWebService(GetBenchmarkPlanRequest request) {
		return soapWebClient.send(request, WSDL_URL, ENDPOINT_FUNCTION, benchmarkWebServiceTemplate);
	}

}
