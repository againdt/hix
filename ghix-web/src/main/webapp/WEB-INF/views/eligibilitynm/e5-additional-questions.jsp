
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Build your household</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link type="text/css" rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<link rel="stylesheet" href="../resources/css/eligibility.css" type="text/css">

<form class="form-horizontal">
<div class="row-fluid">
    <div class="row-fluid" id="titlebar">
      <div class="span3">
          <a class='btn btn-link' href="e1-1-start-app">Back </a>
      </div>
      <div class="span9 pull-left">
        <h1>Additional Questions</h1>
      </div>
    </div>
    <!--titlebar-->
    <div class="row-fluid" id="main">
      <div class="span3 gutter10" id="sidebar">
        <ul class="nav nav-tabs nav-stacked"  data-spy="affix">
          <li> <a href="e1-1-start-app"> <i class="icon-ok"></i> Start Your Application</a></li>
          <li><a href="e3-build-your-household">  <i class="icon-ok"></i> Build Your Household</a></li>
          <li><a href="e4-household-income"> <i class="icon-ok"></i> Your Household Income </a></li>
          <li class="active"><a href="e5-additional-questions"> <i class="icon-ok"></i> Additional Questions </a></li>
          <li><a href="e6-review-declare-file"> Review &amp; Sign </a></li>
        </ul>
      </div>
      <!--sidebar-->
      <div class="span9" id="rightpanel">
      <img src="../resources/eligibilitynm/img/build.png" alt="header image" class="img-polaroid">
        <div class="margin-right"> <!-- gutter10  -->
           <div class="span8">
            <h4>Coming up in this section</h4>
            <p> In this section, you will be asked for identifying information about who is applying for health coverage and who lives in your household.</p>
          </div>
          <div class="span4">
            <h5>You may need</h5>
            <hr>
            <p> <i class="icon-ok"></i> Your Employee ID Number</p>
            <p> <i class="icon-ok"></i> Information about employer benefits or other health coverage.</p>
           <!--  <p> <i class="icon-ok"></i> Information about your family's health care needs.</p> -->
            <p> <i class="icon-time"></i> Estimated time needed to complete this section: 10 minutes.</p>
          </div>
           
            <div class="well well-small clear">
                  <div class="buttons pull-right"> 
                    <a class="btn next" href="#additional1">Next</a> 
                    <a class="btn edit secondary">Next</a> 
                    <a class="btn cancel secondary">Cancel</a>
                    <a class="btn save btn-primary">Save</a> 
                  </div><!-- .buttons pull-right -->
                </div><!-- .well well-small clear -->
           

            <div id="additional1" class="section">
            <div class="opacity">
                <div class="header">
                <h4>Health Coverage (APTC Eligible)</h4>
             </div> 
              <h5>Is Janet Jacobs enrolled in health coverage from any of the following?</h5>
              <div class="control-group">
                <select class="span9">
                  <option>Idaho Medicaid Program</option>
					<option>Idaho's State Children's Health Program</option>
					<option>Medicare</option>
					<option>TRICARE (Don't choose this one if you have Direct Care or Line of Duty.)</option>
					<option>VA health care program</option>
					<option>Peace Corps </option>
					<option>Individual insurance (non-group coverage)</option>
                  <option selected>None of the above</option>
                </select>
              </div><!-- /.control-group -->
             
              <h5>Is Joe Jacobs enrolled in health coverage from any of the following?</h5>
              <div class="control-group">
                <select class="span9">
                  <option>Idaho Medicaid Program</option>
					<option>Idaho's State Children's Health Program</option>
					<option>Medicare</option>
					<option>TRICARE (Don't choose this one if you have Direct Care or Line of Duty.)</option>
					<option>VA health care program</option>
					<option>Peace Corps </option>
					<option>Individual insurance (non-group coverage)</option>
                  <option selected>None of the above</option>
                </select>
              </div><!-- /.control-group -->
             </div><!-- opacity -->
                <div class="well well-small clear">
                  <div class="buttons pull-right"> 
                    <a class="btn next" href="#additional2">Next</a> 
                    <a class="btn edit secondary">Edit</a> 
                    <a class="btn cancel secondary">Cancel</a>
                    <a class="btn save btn-primary">Save</a> 
                  </div><!-- .buttons pull-right -->
                </div><!-- .well well-small clear -->
              </div><!-- #additional2 .section -->   
                
                
                
            <div id="additional2" class="section">
              <div class="opacity">
                  <div class="header">
                <h4>Employer Health Coverage (APTC Eligible)</h4>
            </div>
              <p class="gutter10"><a href="#"><em><i class="icon-print"></i> Employer Coverage Tool</em></a></p>
              
              <h5>Is Janet Jacobs currently eligible for health coverage through a job (even if it's from another person's job, like a spouse)?</h5>
                <div class="control-group">
                  <label class="checkbox inline">
                    <input type="checkbox" value="Yes">
                  Yes </label>
                <label class="checkbox inline">
                   <input type="checkbox" value="No" checked>
                  No </label>
             </div>
             
             <h5>Is Joe Jacobs currently eligible for health coverage through a job (even if it's from another person's job, like a spouse or parent/guardian)?</h5>
                <div class="control-group">
                  <label class="checkbox inline">
                    <input type="checkbox" value="Yes">
                  Yes </label>
                <label class="checkbox inline">
                   <input type="checkbox" value="No" checked>
                  No </label>
             </div>
             
                <h5>Will Janet Jacobs be eligible for health coverage from a job during 2014 (even if it's from another person's job, like spouse)?</h5>
                <div class="control-group">
                  <label class="checkbox inline">
                    <input type="checkbox" value="Yes">
                  Yes </label>
                <label class="checkbox inline">
                   <input type="checkbox" value="No" checked>
                  No </label>
             </div>
             
             <h5>Will Joe Jacobs be eligible for health coverage from a job during 2014 (even if it's from another person's job, like spouse or parent/guardian)?</h5>
                <div class="control-group">
                  <label class="checkbox inline">
                    <input type="checkbox" value="Yes">
                  Yes </label>
                <label class="checkbox inline">
                   <input type="checkbox" value="No" checked>
                  No </label>
             </div>

              </div><!-- opacity -->
                <div class="well well-small clear">
                  <div class="buttons pull-right"> 
                    <a class="btn next" href="#additional3">Next</a> 
                    <a class="btn edit secondary">Edit</a> 
                    <a class="btn cancel secondary">Cancel</a>
                    <a class="btn save btn-primary">Save</a> 
                  </div><!-- .buttons pull-right -->
                </div><!-- .well well-small clear -->
              </div><!-- #household .section -->
              
            
            
            <!-- .household-->
            <div id="additional3" class="section">
              <div class="opacity">
                  <div class="header">
                  <h4>Tell us about Janet Jacob's employer </h4>
              	</div>
                
                <div class="control-group" style="padding-top:20px;">
                  <label class="control-label">Employer Name:</label>
                  <div class="controls">
                    <input type="text" class="input-medium">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Street Address</label>
                  <div class="controls">
                    <input type="text" class="input-medium">&nbsp;
                    Apt/Suite <input type="text" class="input-mini" placeholder="">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">City</label>
                  <div class="controls">
                    <input type="text" class="input-medium">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">State</label>
                   <div class="controls">
                    <select name="inputState" class="span4" id="inputState">
                      <option value="">Select a State</option>
                      <option value="AL">Alabama</option>
                      <option value="AK">Alaska</option>
                      <option value="AZ">Arizona</option>
                      <option value="AR">Arkansas</option>
                      <option value="CA">California</option>
                      <option value="CO">Colorado</option>
                      <option value="CT">Connecticut</option>
                      <option value="DE">Delaware</option>
                      <option value="DC">District Of Columbia</option>
                      <option value="FL">Florida</option>
                      <option value="GA">Georgia</option>
                      <option value="HI">Hawaii</option>
                      <option value="ID" >Idaho</option>
                      <option value="IL">Illinois</option>
                      <option value="IN">Indiana</option>
                      <option value="IA">Iowa</option>
                      <option value="KS">Kansas</option>
                      <option value="KY">Kentucky</option>
                      <option value="LA">Louisiana</option>
                      <option value="ME">Maine</option>
                      <option value="MD">Maryland</option>
                      <option value="MA">Massachusetts</option>
                      <option value="MI">Michigan</option>
                      <option value="MN">Minnesota</option>
                      <option value="MS">Mississippi</option>
                      <option value="MO">Missouri</option>
                      <option value="MT">Montana</option>
                      <option value="NE">Nebraska</option>
                      <option value="NV">Nevada</option>
                      <option value="NH">New Hampshire</option>
                      <option value="NJ">New Jersey</option>
                      <option value="NM">New Mexico</option>
                      <option value="NY">New York</option>
                      <option value="NC">North Carolina</option>
                      <option value="ND">North Dakota</option>
                      <option value="OH">Ohio</option>
                      <option value="OK">Oklahoma</option>
                      <option value="OR">Oregon</option>
                      <option value="PA">Pennsylvania</option>
                      <option value="RI">Rhode Island</option>
                      <option value="SC">South Carolina</option>
                      <option value="SD">South Dakota</option>
                      <option value="TN">Tennessee</option>
                      <option value="TX">Texas</option>
                      <option value="UT">Utah</option>
                      <option value="VT">Vermont</option>
                      <option value="VA">Virginia</option>
                      <option value="WA">Washington</option>
                      <option value="WV">West Virginia</option>
                      <option value="WI">Wisconsin</option>
                      <option value="WY">Wyoming</option>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Zip Code</label>
                  <div class="controls">
                    <input type="text" class="input-medium">
                  </div>
                </div>
              
              <div class="control-group">
               <label class="control-label" for="inputSuffix">Employer's Phone Number</label>
                <div class="controls">
                        <input type="text" name="phone1" id="phone1" value="" maxlength="3" class=" input-mini">
                  <input type="text" name="phone2" id="phone2" value="" maxlength="3" class="input-mini">
                  <input type="text" name="phone3" id="phone3" value="" maxlength="4" class="input-mini">
                </div>
                </div>
                
                <div class="control-group">
               <label class="control-label" for="inputSuffix">Employer's ID Number (EIN)</label>
                <div class="controls">
                        <input type="text" value="" maxlength="2" class=" input-mini">
                  <input type="text" value="" maxlength="7" class="input-small">
                </div>
                </div>
              </div><!-- opacity -->
                <div class="well well-small clear">
                  <div class="buttons pull-right"> 
                    <a class="btn btn-primary" href="e6-review-declare-file">Save &amp; Continue</a> 
                    <a class="btn edit secondary">Edit</a> 
                    <a class="btn cancel secondary">Cancel</a>
                    <a class="btn save btn-primary">Save</a> 
                  </div><!-- .buttons pull-right -->
                </div><!-- .well well-small clear -->
              </div><!-- #household2 .section -->

     
           </div>
    <!--gutter10-->
  </div>
  <!--rightpanel-->
      
</div>
<!--row-fluid-->
</div>
<!--main-->
</form>

<script type="text/javascript" src="../resources/eligibilitynm/js/ghixcustom.js"></script>
<script>
    $(document).ready(function () {
        $(".flip").click(function () {
            $(".mailingAddress").fadeToggle();
        });
        $('input, textarea').placeholder();
    });
    $(".next").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        var hash = $(this.hash);
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(hash).closest('.section').show();
        $(currentsection).next().addClass('activediv');
        $(hash).prev('div').removeClass('activediv');
        $('html,body').delay(300).animate({
            scrollTop: $(this.hash).offset().top - 100
        }, 500)
        $(hash).delay(700).prev('div').find('.edit').show();
        $(hash).delay(700).prev('div').find('.next').hide();
    });
    $(".edit").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 1.0
        });
        $(currentsection).find('.cancel, .save').show();
        $('.activediv').animate({
            opacity: 0.45
        });
        $(currentsection).find('.edit').hide();
    });
    $(".save").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(currentsection).find('.cancel, .save').hide();
        $(currentsection).find('.edit').show();
        $('.activediv').animate({
            opacity: 1
        });
        $('html,body').delay(300).animate({
            scrollTop: $(currentsection).nextAll('.activediv').offset().top - 100
        }, 500)
    });
    $(".cancel").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(currentsection).find('.cancel, .save').hide();
        $(currentsection).find('.edit').show();
        $('.activediv').animate({
            opacity: 1
        });
        $('html,body').delay(300).animate({
            scrollTop: $('div').nextAll('.activediv').offset().top - 100
        }, 500)
    });
</script>
<script>
$(document).ready(function(){
$('.qualify').hide();
$('#qualify-coverage').modal('hide');
$('#sidebar').affix();
$("input[name='immigration']").change(function(){
	$('.immigration').fadeToggle(this.value == "Yes");
});

$('.pending').hide();
$('input:radio[name="incarcerated"]').change(
    function(){
    if ($(this).is(':checked') && $(this).val() == 'Yes') {
  $('.pending').fadeToggle();
    }
    else{
      $('.pending').hide();
      }
}); 

$('#dob').datepicker();

});
</script>
</body>
</html>
