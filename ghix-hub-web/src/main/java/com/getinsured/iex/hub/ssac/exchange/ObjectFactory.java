
package com.getinsured.iex.hub.ssac.exchange;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.getinsured.iex.hub.ssac.extension.SSACompositeRequestType;
import com.getinsured.iex.hub.ssac.extension.SSACompositeResponseType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.hhs.cms.dsh.sim.ee.ssac package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SSACompositeResponse_QNAME = new QName("http://ssac.ee.sim.dsh.cms.hhs.gov", "SSACompositeResponse");
    private final static QName _SSACompositeRequest_QNAME = new QName("http://ssac.ee.sim.dsh.cms.hhs.gov", "SSACompositeRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.hhs.cms.dsh.sim.ee.ssac
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSACompositeResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ssac.ee.sim.dsh.cms.hhs.gov", name = "SSACompositeResponse")
    public JAXBElement<SSACompositeResponseType> createSSACompositeResponse(SSACompositeResponseType value) {
        return new JAXBElement<SSACompositeResponseType>(_SSACompositeResponse_QNAME, SSACompositeResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSACompositeRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ssac.ee.sim.dsh.cms.hhs.gov", name = "SSACompositeRequest")
    public JAXBElement<SSACompositeRequestType> createSSACompositeRequest(SSACompositeRequestType value) {
        return new JAXBElement<SSACompositeRequestType>(_SSACompositeRequest_QNAME, SSACompositeRequestType.class, null, value);
    }

}
