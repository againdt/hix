package com.getinsured.ssap.util;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SSL utility methods.
 *
 * @author Yevgen Golubenko
 * @since 2/17/19
 */
public final class SSLUtil
{
  private static final Logger log = LoggerFactory.getLogger(SSLUtil.class);

  private static final TrustManager[] UNQUESTIONING_TRUST_MANAGER = new TrustManager[]{
      new X509TrustManager()
      {
        public X509Certificate[] getAcceptedIssuers()
        {
          return null;
        }

        public void checkClientTrusted(X509Certificate[] certs, String authType)
        {
        }

        public void checkServerTrusted(X509Certificate[] certs, String authType)
        {
        }
      }
  };

  /**
   * Turns off SSL checking (to be used for RestTemplate making class to https)
   * @throws NoSuchAlgorithmException if algorithm exception occurs.
   * @throws KeyManagementException if key management exception occurs.
   */
  public static void turnOffSslChecking() throws NoSuchAlgorithmException, KeyManagementException
  {
    log.info("Installing all-trusting trust manager and turning OFF SSL checking");
    final SSLContext sc = SSLContext.getInstance("SSL");
    sc.init(null, UNQUESTIONING_TRUST_MANAGER, null);
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
  }

  /**
   * Used to turn ON SSL checking.
   * @throws NoSuchAlgorithmException if algorithm exception occurs.
   * @throws KeyManagementException if key management exception occurs.
   */
  public static void turnOnSslChecking() throws KeyManagementException, NoSuchAlgorithmException
  {
    log.info("Turning ON SSL checking");
    SSLContext.getInstance("SSL").init(null, null, null);
  }

  private SSLUtil()
  {
    throw new UnsupportedOperationException("Do not instantiate libraries.");
  }
}
