<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>


<div class="row-fluid">
	<form class="form-vertical margin0" id="frmviewAddressList" name="frmviewAddressList" action="viewvalidaddress" method="POST" target="_parent">
			<p class="margin0"><spring:message code="label.platform.address.bestmatch"/></p>
			<div>
				<c:if test="${enteredLocation != null}">
					<h4><spring:message code="label.platform.address.enteredAddress"/></h4>
					<table class="table table-border-none popup-table">
						<tbody>
							<tr>
								<td class="span1"><input type="radio" class="margin0" value="${enteredLocation.address1}, ${enteredLocation.address2}, ${enteredLocation.city}, ${enteredLocation.state}, ${enteredLocation.zip},${enteredLocation.lat},${enteredLocation.lon}" name="addr" id="userdefault"/></td>
								<td class="span6">${enteredLocation.address1}</td>
							</tr>
							<tr>
								<td></td>
								<td>${enteredLocation.address2}</td>
							</tr>
							<tr>
								<td></td>
								<td>${enteredLocation.city},
								${enteredLocation.state}
								${enteredLocation.zip}
								</td>
							</tr>							
						</tbody>
					</table>
				</c:if>
			</div>			
			<div class="rowContent">
				<h4><spring:message code="label.platform.address.likelyMatch"/></h4> 
				<div class="row-fluid">
					<div id="validAddressList">
						<input type="hidden" name="id" id="id" value="">
						<c:if test="${fn:length(validAddressList) > 0}">
							<c:forEach items="${validAddressList}" var="address" begin="0" varStatus="rowCounter">
								<table class="table table-border-none popup-table">
									<tbody>
										<tr>
											<td class="span1"><input type="radio" class="margin0" value="${address.address1},${address.address2},${address.city},${address.state},${address.zip},${address.lat},${address.lon},${address.rdi},${address.county}" name="addr" id="${rowCounter.index}"/></td>
											<td class="span6">${address.address1}</td>
										</tr>
										<tr>
											<td></td>										
											<td class="span6">${address.address2}</td>
										</tr>
										<tr>
											<td></td>
											<td>
												${address.city},
												${address.state}
												${address.zip}
											</td>
										</tr>										
									</tbody>
								</table>
							</c:forEach>
						</c:if>
						<div class="marginTop20">
							<input name="submitAddr" id="submitAddr" type="button" value=' <spring:message code="label.platform.address.OK"/> ' class="btn btn-primary pull-right marginR20" />
							<input name="back_button_from_iframe" id="back_button_from_iframe" type="button" value='<spring:message code="label.platform.address.cancel"/>' class="btn" />
						</div>
					</div>
				</div>
			</div>			
		<input type="hidden" name="ids" id="ids" value="${ids}">
	</form>
</div>
<script type="text/javascript" src="<c:url value="/resources/js/modal-template.js" />"></script>


<script type="text/javascript" >
$(document).ready(function () {
	window.parent.$( "h3:contains('Check Your Address')" ).text('<spring:message code="label.platform.address.checkAddress"/>');
});
	
	$('#submitAddr').bind('click',function(event){
		 var csrfURL = '<c:url value="/platform/address/viewvalidaddress"> <c:param name="${df:csrfTokenParameter()}"> <df:csrfToken plainToken="true" />  </c:param> </c:url>';
		 var  pathURL=csrfURL;
		 $.post(pathURL, {selectedAdrs : $("input:radio:checked").val()}, function(data) {
			if(data=="SUCCESS"){
				application.submit_button_of_iframe(event.target.id);
			}
			else {
				//console.log('valid address else')
			}
		}); 
	});

	$('#back_button_from_iframe').bind('click',function(event){
		application.back_button_from_iframe(event.target.id);
	});
	
	
</script>
