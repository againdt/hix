import React from 'react';

const RenderField = ({
  input,
  meta: { touched, error, warning },
  label,
  type,
  ...props
}) => (
  <div>
    {props.label ? <label>{label}</label> : ''}
    <div>
      <input {...input} placeholder={label} type={type} />
      {touched &&
        ((error && <span className="ds-c-field--error">{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
);

export default RenderField;
