<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<div class="gutter10">
	<div class="page-header">
		<h1>Employer Application: <small>Eligibility Status</small></h1>
	</div>
	<div class="row-fluid">		
		<div class="span3">
			<div class="gutter10"></div>
		</div>
		<div class="span9">
			<form class="form-horizontal" id="frmsignature" name="frmsignature" action="signature" method="POST">
				<df:csrfToken/>
							 <h3>Preliminary Results</h3>
							 <c:choose>
							 	<c:when test="${eligibityFlag == true}">
							 		<p>Based on the information you have entered so far, you are likely eligible to participate in the ${state} Health Exchange for small businesses.</p>
							 	</c:when>
							 	<c:otherwise>
							 		<p>Based on the information you've entered so far, your company is not eligible to participate in the ${state} Health Exchange for small businesses.</p>
							 	</c:otherwise>
							 </c:choose>
							 <h3>Rights and Responsibilities</h3>
	                         <p>
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
							 </p>
				<div class="form-actions">
					<a href="/hix/shop/employer/roster/info" class="btn btn-primary  pull-right">Add Employees</a>
				</div>
			</form>
		</div>
	</div>
</div>