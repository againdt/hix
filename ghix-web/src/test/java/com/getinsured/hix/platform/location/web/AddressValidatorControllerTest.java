package com.getinsured.hix.platform.location.web;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.ui.Model;

import com.getinsured.hix.BaseTest;

public class AddressValidatorControllerTest extends BaseTest {

	private static final String SUCCESS = "SUCCESS";
	private static final String FAILURE = "FAILURE";
	@Autowired  AddressValidatorController  addressValidatorController;

	@Test
	public void testValidateAddressValidAdress() {
		String enteredAddress = "2200 Mission College Blvd,,Santa Clara,CA,95054";
		String ids = "address1~addres2~city~state~zip";

		Model model = null;
		MockHttpServletRequest request = new MockHttpServletRequest();
		String result = addressValidatorController.validateAddress(model, request, enteredAddress, ids);

		assertNotNull(result);
		assertTrue(result.equals(FAILURE));
	}

	@Test
	public void testValidateAddressInvalidLine1() {
		String enteredAddress = "2200 Mission College Blvd.,,Santa Clara,CA,95054";
		String ids = "address1~addres2~city~state~zip";

		Model model = null;
		MockHttpServletRequest request = new MockHttpServletRequest();
		String result = addressValidatorController.validateAddress(model, request, enteredAddress, ids);

		assertNotNull(result);
		assertTrue(result.equals(SUCCESS));
	}


	@Test
	public void testValidateAddressWrongLine1() {
		String enteredAddress = "A-201,,Santa Clara,CA,95054";
		String ids = "address1~addres2~city~state~zip";

		Model model = null;
		MockHttpServletRequest request = new MockHttpServletRequest();
		String result = addressValidatorController.validateAddress(model, request, enteredAddress, ids);

		assertNotNull(result);
		assertTrue(result.equals(FAILURE));
	}

	@Test
	public void testValidateAddressInvalidCity() {
		String enteredAddress = "2200 Mission College Blvd,,Santa Claraaa,CA,95054";
		String ids = "address1~addres2~city~state~zip";

		Model model = null;
		MockHttpServletRequest request = new MockHttpServletRequest();
		String result = addressValidatorController.validateAddress(model, request, enteredAddress, ids);

		assertNotNull(result);
		assertTrue(result.equals(SUCCESS));
	}

	@Test
	public void testValidateAddressWrongCity() {
		String enteredAddress = "2200 Mission College Blvd,,Mumbai,CA,95054";
		String ids = "address1~addres2~city~state~zip";

		Model model = null;
		MockHttpServletRequest request = new MockHttpServletRequest();
		String result = addressValidatorController.validateAddress(model, request, enteredAddress, ids);

		assertNotNull(result);
		assertTrue(result.equals(SUCCESS));
	}


	@Test
	public void testValidateAddressInvalidState() {
		String enteredAddress = "2200 Mission College Blvd,,Santa Clara,CAA,95054";
		String ids = "address1~addres2~city~state~zip";

		Model model = null;
		MockHttpServletRequest request = new MockHttpServletRequest();
		String result = addressValidatorController.validateAddress(model, request, enteredAddress, ids);

		assertNotNull(result);
		assertTrue(result.equals(SUCCESS));
	}

	@Test
	public void testValidateAddressWrongState() {
		String enteredAddress = "2200 Mission College Blvd,,Santa Clara,MAHA,95054";
		String ids = "address1~addres2~city~state~zip";

		Model model = null;
		MockHttpServletRequest request = new MockHttpServletRequest();
		String result = addressValidatorController.validateAddress(model, request, enteredAddress, ids);

		assertNotNull(result);
		assertTrue(result.equals(SUCCESS));
	}

	@Test
	public void testValidateAddressInvalidZip() {
		String enteredAddress = "2200 Mission College Blvd,,Santa Clara,CA,9505";
		String ids = "address1~addres2~city~state~zip";

		Model model = null;
		MockHttpServletRequest request = new MockHttpServletRequest();
		String result = addressValidatorController.validateAddress(model, request, enteredAddress, ids);

		assertNotNull(result);
		assertTrue(result.equals(SUCCESS));
	}

	@Test
	public void testValidateAddressWrongZip() {
		String enteredAddress = "2200 Mission College Blvd,,Santa Clara,CA,12";
		String ids = "address1~addres2~city~state~zip";

		Model model = null;
		MockHttpServletRequest request = new MockHttpServletRequest();
		String result = addressValidatorController.validateAddress(model, request, enteredAddress, ids);

		assertNotNull(result);
		assertTrue(result.equals(SUCCESS));
	}

	@Test
	public void testValidateAddressInvalidData() {
		String enteredAddress = ",,Santa Clara,CA,12";
		String ids = "address1~addres2~city~state~zip";

		Model model = null;
		MockHttpServletRequest request = new MockHttpServletRequest();
		String result = addressValidatorController.validateAddress(model, request, enteredAddress, ids);

		assertNotNull(result);
		assertTrue(result.equals(FAILURE));
	}


}
