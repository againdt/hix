package com.getinsured.iex.household.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.RelationshipType;
import com.getinsured.iex.household.repository.RelationshipTypeRepository;

/**
 *
 * @author Dheeraj Maralkar
 */
@Service
public class RelationshipTypeServiceImpl implements RelationshipTypeService {
	private static final Logger log = LoggerFactory.getLogger(RelationshipTypeServiceImpl.class);

	private final RelationshipTypeRepository relationshipTypeRepository;

	@Autowired
	public RelationshipTypeServiceImpl(final RelationshipTypeRepository relationshipTypeRepository) {
		this.relationshipTypeRepository = relationshipTypeRepository;
	}

	@Override
	public boolean isRelationshipStandardForCode(String code) {
		if(log.isDebugEnabled())
			log.debug("checking isRelation standard for code {}",code);
		RelationshipType relationshipType = relationshipTypeRepository.findOneByLookupValueLookupValueCode(code);
		if(relationshipType==null) {
			if(log.isWarnEnabled())
				log.warn("Relation code does not exists : {}",code);
			return false;
		}
		return relationshipType.getIsStandard();
	}

	@Override
	public boolean isRelationshipStandardForLabel(String label) {
		if(log.isDebugEnabled())
			log.debug("checking isRelation standard for label {}",label);
		RelationshipType relationshipType = relationshipTypeRepository.findOneByLookupValueLookupValueLabelIgnoreCase(label);
		if(relationshipType==null) {
			if(log.isWarnEnabled())
				log.warn("Relation code does not exists : {}",label);
			return false;
		}
		return relationshipType.getIsStandard();
	}

}
