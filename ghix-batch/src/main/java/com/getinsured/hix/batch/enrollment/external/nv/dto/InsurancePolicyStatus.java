package com.getinsured.hix.batch.enrollment.external.nv.dto;

import java.util.ArrayList;

public class InsurancePolicyStatus {
	 private String insurancePolicyStatusDateTime;
	 private String definingInsurancePolicyStatusTypeCodeName;
	 private ArrayList <MaintenanceReason> maintenanceReasons = new ArrayList <MaintenanceReason> ();


	 // Getter Methods 

	 public String getInsurancePolicyStatusDateTime() {
	  return insurancePolicyStatusDateTime;
	 }

	 public String getDefiningInsurancePolicyStatusTypeCodeName() {
	  return definingInsurancePolicyStatusTypeCodeName;
	 }

	 // Setter Methods 

	 public void setInsurancePolicyStatusDateTime(String insurancePolicyStatusDateTime) {
	  this.insurancePolicyStatusDateTime = insurancePolicyStatusDateTime;
	 }

	 public void setDefiningInsurancePolicyStatusTypeCodeName(String definingInsurancePolicyStatusTypeCodeName) {
	  this.definingInsurancePolicyStatusTypeCodeName = definingInsurancePolicyStatusTypeCodeName;
	 }

	public ArrayList<MaintenanceReason> getMaintenanceReasons() {
		return maintenanceReasons;
	}

	public void setMaintenanceReasons(ArrayList<MaintenanceReason> maintenanceReasons) {
		this.maintenanceReasons = maintenanceReasons;
	}
	}
