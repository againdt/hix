package com.getinsured.iex.hub.ssac.wrapper;

import java.math.BigInteger;
import java.util.List;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ssac.extension.QualifyingYearAndQuarterType;
import com.getinsured.iex.hub.ssac.extension.SSAQuartersOfCoverageType;

public class SSAQuartersOfCoverageWrapper implements JSONAware {

	private BigInteger qQuantity;
	private List<QualifyingYearAndQuarterType> qList;

	public SSAQuartersOfCoverageWrapper(
			SSAQuartersOfCoverageType quatersOfcoverage) {
		this.qQuantity = quatersOfcoverage.getLifeTimeQuarterQuantity();
		qList = quatersOfcoverage.getQualifyingYearAndQuarter();
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("LifeTimeQuarterQuantity", this.qQuantity);
		JSONObject details;
		for(QualifyingYearAndQuarterType detail: this.qList){
			details = new JSONObject();
			details.put("QualifyingYear", detail.getQualifyingYear().getValue().toString());
			details.put("QualifyingQuarter", detail.getQualifyingQuarter());
			obj.put("QualifyingYearAndQuarter", detail);
		}
		return obj.toJSONString();
	}

}
