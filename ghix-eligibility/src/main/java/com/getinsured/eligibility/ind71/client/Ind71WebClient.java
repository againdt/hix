package com.getinsured.eligibility.ind71.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalResponse;

@Component
@DependsOn("ghixEndPoints")
public class Ind71WebClient {

	private static final String IND71_SERVICE_WSDL = "webservice/plandisplay/endpoints/AutoRenewalService.wsdl";

	//private static final String ENDPOINT_FUNCTION = "AUTORENEWAL";

	//@Autowired private SoapWebClient<AutoRenewalResponse> soapWebClient;
	@Autowired private WebServiceTemplate ind71WebServiceTemplate;

	private static final String WSDL_URL = GhixEndPoints.PLANDISPLAY_URL + IND71_SERVICE_WSDL;

	public AutoRenewalResponse invoke(AutoRenewalRequest request) {
		ind71WebServiceTemplate.setDefaultUri(WSDL_URL);
		return  (AutoRenewalResponse) ind71WebServiceTemplate.marshalSendAndReceive(request);
		//return soapWebClient.send(request, WSDL_URL, ENDPOINT_FUNCTION, ind71WebServiceTemplate); // No need to log as this is taken care by IND71 server
	}

}