<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/style.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<script type="text/javascript">
$(function() {
    $(".newcommentiframe").click(function(e){
          e.preventDefault();
          var href = $(this).attr('href');
          if (href.indexOf('#') != 0) {
             $('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal({backdrop:false});
      }
    });
  });
  
  function closeIFrame() {
    $("#newcommentiframe").remove();
    window.location.href = '/hix/individual/individualcomments/'+${individualUser.id};
  }
  
  function closeCommentBox() {
	  $("#newcommentiframe").remove();	  
	  var url = '/hix/individual/individualcomments/'+${individualUser.id};
	  window.location.assign(url);
  }
</script>

<div class="row-fluid">
	<div style="font-size: 14px; color: red">
		<c:if test="${errorMsg != 'false'}">
			<c:out value="${errorMsg}"></c:out>
		</c:if>
	</div>
</div>


<!--  Latest UI -->
<div class="gutter10-lr">
	<div class="l-page-breadcrumb">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message
							code="label.back" /></a></li>
				<li><a href="<c:url value="/broker/individuals"/>"><spring:message
							code="label.individuals" /></a></li>
				<li>Active</li>
				<li>${individualUser.firstName} ${individualUser.lastName}</li>
				<li>Income Information</li>
			</ul>
		</div>
		<!--  end of row-fluid -->
	</div>
	<!--  end l-page-breadcrumb -->
	<c:if test="${message != null}">
		<div class="errorblock alert alert-info">
			<p>${message}</p>
		</div>
	</c:if>
	<h1>${individualUser.firstName} ${individualUser.lastName}</h1>
	<div class="row-fluid">
		<div id="sidebar" class="span3">
				<h4 class="header">About this Household</h4>
			<ul class="nav nav-list">
				<li class="link"><a
					href="/hix/individual/individualcase/${individualUser.id}">Summary</a></li>
				<li class="active"><a href="#">Income Information</a></li>
				<li class="link"><c:url value="/inbox/secureInboxSearch"
						var="searchURL">
						<c:param name="searchText"
							value="${individualUser.firstName} ${individualUser.lastName}" />
					</c:url> <a href="${searchURL}">Messages</a></li>
				<li class="link"><a
					href="/hix/individual/individualcomments/${individualUser.id}">Comments</a></li>
			</ul>
			<br>
			<div class="header"><h4><i class="icon-cog icon-white"></i> Actions</h4>
			</div>
			<ul class="nav nav-list">
				<li>
					<c:choose>
	                  	<c:when test="${showSwitchRolePopup == \"N\"}">
							<c:choose>
								<c:when test="${isCACall == true}">
									<a href="<c:url value="${switchAccUrl}&_pageLabel=individualHomePage&ahbxId=${individualUser.id}&ahbxUserType=1&recordId=${brokerId}&recordType=2"/>" role="button" data-toggle="modal"><i class="icon-eye-open"></i> View Individual Account</a>
								</c:when>
								<c:otherwise>
									<a href="<c:url value='/account/user/switchUserRole/individual/${individualUser.id}'/>" role="button" data-toggle="modal"><i class="icon-eye-open"></i> View Individual Account</a>
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<a href="#viewindModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> View Individual Account</a>
						</c:otherwise>
					</c:choose>
				</li>
				<li><a href="#new-msg" data-toggle="modal"
					onclick="resetForm();saveDraftOfMessage();"><i
						class="icon-envelope-unread"></i> Compose Message</a></li>
				<li><a name="addComment"
					href="<c:url value="/individual/newcomment?target_id=${individualUser.id}&target_name=DESIGNATEBROKER&individualName=${individualUser.firstName} ${individualUser.lastName}"/>"
					id="addComment" class="newcommentiframe"> <i
						class="icon-comment"></i>New Comment
				</a></li>
			</ul>

		</div>
		<!-- Modal -->
		
		<div id="rightpanel" class="span9">
			<form class="form-horizontal" id="" name="" action="" method="POST">
				<div class="header">
					<h4>Summary</h4>
				</div>
				<table class="table table-border-none">
					<tbody>
						<tr>
							<td class="span3 txt-right">Total Projected Household Income</td>
							<td><strong>&#36;35,250</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Current Plan</td>
							<td><strong>N&#92;A</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Eligibility Status</td>
							<td><strong>Denied on 7&#92;11</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Medicaid Eligibility</td>
							<td><strong></strong></td>
						</tr>
						<tr>
							<td class="txt-right">Current Monthly Income</td>
							<td><strong>&#36;35,250</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Medicaid Family Size</td>
							<td><strong>5</strong></td>
						</tr>
					</tbody>
				</table>
			</form>

		</div>
	</div>
</div>


		<!-- Modal -->
			<jsp:include page="individualviewmodal.jsp" />
		<!-- Modal end -->
<!--  Latest UI -->
