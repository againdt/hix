<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@ page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>
<c:set var="bootstrap_style" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BOOTSTRAP_LESS_FILE)%>"></c:set>
<%-- <link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" /> --%>
<link href="<c:url value="/resources/css/${bootstrap_style}" />" media="screen,print" rel="stylesheet" type="text/css" />

<style>
	body {
		background-image:none; 
		padding-top: 0;
	}
</style>

<div class="gutter10">   
				<div class="row-fluid result-count-header">
					<h1>		
 						<span class="search pull-right">
							<a class="btn btn-primary" href="/hix/entity/locateassister/entitydetail/${enrollmentEntity.id}/${site.id}"><spring:message code="label.assister.backbtn"/></a>
						</span>
					</h1>
				</div> 
				<div class="row-fluid">
					<form id="entityListfrm" name="entityListfrm" action="searchentities" method="post">
						<input type="hidden" name="currentpage" id="currentpage" value="">
						<c:choose>
							<c:when test="${fn:length(listOfAssistersWithLanguages) > 0}">
								<table id="entitySearchResult" class="table table-condense">
									<thead>
										<tr class="header list-header">
											<th><spring:message code="label.assister.assistername"/></th>
											<th><spring:message code="label.assister.contactinfo"/></th>
											<th><spring:message code="label.assister.languages"/></th>
										</tr>
									</thead>
									<c:forEach var="assisterMap" items="${listOfAssistersWithLanguages}">
										<c:set var="assister" value="${assisterMap.key}"/>
										<c:set var="assisterLanguages" value="${assisterMap.value}"/>
									    <tr>
											<td>
												<a href="<c:url value="/entity/locateassister/assisterdetail/${assister.id}/${assister.primaryAssisterSite.id}"/>">${assister.firstName} ${assister.lastName}</a>
											</td>
											<td>
												<c:if test="${assister.mailingLocation!=null}">
													<c:if test="${assister.mailingLocation.zip!=null}">
														${assister.mailingLocation.address1} ${assister.mailingLocation.address2} ${assister.mailingLocation.city} ${assister.mailingLocation.state}
														<br><c:out value="${assister.mailingLocation.zip}" />
													</c:if>
												</c:if>
												<br><c:out value="${assister.primaryPhoneNumber}" />
												<br><c:out value="${assister.emailAddress}" />
											</td>
											<td class="">${assisterLanguages.spokenLanguages}</td>
										</tr>
									</c:forEach>
								</table>
								<div class="center">
									<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
								</div>
							</c:when>
							<c:otherwise>
								<b><spring:message code="label.assister.recordsnotfound"/></b>
							</c:otherwise>
						</c:choose>
					</form>
				</div>
			</div>						
<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<!-- End Google Analytics -->	