package com.getinsured.hix.batch.migration.couchbase.ecm;

import java.util.LinkedHashMap;
import java.util.Map;

import com.getinsured.hix.platform.ecm.ECMConstants;

public class DataConfig {

	private static final String _03 = "_03";

	private static final String _02 = "_02";

	private static final String _01 = "_01";

	private DataConfig() {
	}

	final static String COUCH_MIGRATION = "COUCH_MIGRATION";
	
	final static String MIGRATION_STATUS = "MIGRATION_STATUS";
	
	static final String PRIMARY_ID = "PRIMARY_ID";

	static final String MIGRATION_ID = "MIGRATION_ID";
	
	static final String COUNT_OF_REC = "countOfRecords";
	
	static final String TABLE_NAME = "tableName";

	static final String COLUMN_NAME = "columnName";

	static final String DB_PRIMARY_ID = "dbPrimaryId";

	static final String DB_MIGRATION_ID = "dbMigrationId";

	static final String COUCHBASE_ID = "COUCHBASE_ID";
	
	static final String EXISTING_ECM_ID = "EXISTING_ECM_ID";
	
	static final String ECM_ID = "ecmId";
	
	@SuppressWarnings("serial")
	static Map<String, CouchbaseECMMigrationDTO> migrationTables = new LinkedHashMap<String, CouchbaseECMMigrationDTO>() {
		{
			
			put(Tables.ADMIN_DOCUMENT + _01, new CouchbaseECMMigrationDTO(Tables.ADMIN_DOCUMENT, Columns.DOCUMENT_NAME, 
					ECMConstants.ADMIN.DOC_CATEGORY, ECMConstants.ADMIN.ADMIN_DOCUMENT));
			
			put(Tables.BROKER_DOCUMENTS + _01, new CouchbaseECMMigrationDTO(Tables.BROKER_DOCUMENTS, Columns.DOCUMENT_NAME, 
					ECMConstants.Broker.DOC_CATEGORY, ECMConstants.Broker.BROKER_DOCUMENTS));

			put(Tables.CAP_ENROLLMENT_PAYMENT + _01, new CouchbaseECMMigrationDTO(Tables.CAP_ENROLLMENT_PAYMENT, Columns.ECM_DOC_ID,
					ECMConstants.CAP.DOC_CATEGORY, ECMConstants.CAP.APPLICATION_DOCUMENT));
						
			put(Tables.CMR_DOCUMENTS + _01, new CouchbaseECMMigrationDTO(Tables.CMR_DOCUMENTS, Columns.ECM_DOCUMENT_ID,
					ECMConstants.CAP.DOC_CATEGORY, ECMConstants.CAP.APPLICATION_DOCUMENT));
			
			put(Tables.D2C_ECM_DOCUMENTS + _01, new CouchbaseECMMigrationDTO(Tables.D2C_ECM_DOCUMENTS, Columns.ECM_ID,
					ECMConstants.DirectConsumer.DOC_CATEGORY, ECMConstants.DirectConsumer.APPLICATION_CONFIGURATION));
			
			put(Tables.INBOX_MSG_DOC + _01, new CouchbaseECMMigrationDTO(Tables.INBOX_MSG_DOC, Columns.DOCUMENT_ID, 
					ECMConstants.Platform.DOC_CATEGORY, ECMConstants.Platform.NOTICE));

			put(Tables.ISSUER_DOCUMENT + _01, new CouchbaseECMMigrationDTO(Tables.ISSUER_DOCUMENT, Columns.DOCUMENT_NAME,
					ECMConstants.Issuer.DOC_CATEGORY, ECMConstants.Issuer.DOC_SUB_CATEGORY_QRATING));

			put(Tables.ISSUERS + _01, new CouchbaseECMMigrationDTO(Tables.ISSUERS, Columns.CERTIFICATION_DOC,
					ECMConstants.Issuer.DOC_CATEGORY, ECMConstants.Issuer.DOC_SUB_CATEGORY_QRATING));

			put(Tables.NOTICES + _01, new CouchbaseECMMigrationDTO(Tables.NOTICES, Columns.ECM_ID, 
					ECMConstants.Platform.DOC_CATEGORY, ECMConstants.Platform.NOTICE));
			
			put(Tables.PLAN + _01, new CouchbaseECMMigrationDTO(Tables.PLAN, Columns.SUPPORT_FILE,
					ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY));
			put(Tables.PLAN + _02, new CouchbaseECMMigrationDTO(Tables.PLAN, Columns.BROCHURE_UCM_ID,
					ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY));
			put(Tables.PLAN + _03, new CouchbaseECMMigrationDTO(Tables.PLAN, Columns.EOC_DOC_ID,
					ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY));

			put(Tables.PLAN_DENTAL + _01, new CouchbaseECMMigrationDTO(Tables.PLAN_DENTAL, Columns.SBC_UCM_ID,
					ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY));
			put(Tables.PLAN_DENTAL + _02, new CouchbaseECMMigrationDTO(Tables.PLAN_DENTAL, Columns.BENEFIT_FILE,
					ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY));
			put(Tables.PLAN_DENTAL + _03, new CouchbaseECMMigrationDTO(Tables.PLAN_DENTAL, Columns.RATE_FILE,
					ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY));

			put(Tables.PLAN_HEALTH + _01, new CouchbaseECMMigrationDTO(Tables.PLAN_HEALTH, Columns.BENEFIT_FILE,
					ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY));
			put(Tables.PLAN_HEALTH + _02, new CouchbaseECMMigrationDTO(Tables.PLAN_HEALTH, Columns.RATE_FILE,
					ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY));
			put(Tables.PLAN_HEALTH + _03, new CouchbaseECMMigrationDTO(Tables.PLAN_HEALTH, Columns.SBC_UCM_ID,
					ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY));

			put(Tables.PM_PLAN_NETWORK_REPORT + _01, new CouchbaseECMMigrationDTO(Tables.PM_PLAN_NETWORK_REPORT, Columns.ECM_DOCUMENT_ID, 
					ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY));
			put(Tables.PM_PLAN_NETWORK_REPORT + _01, new CouchbaseECMMigrationDTO(Tables.PM_PLAN_NETWORK_REPORT, Columns.VENDOR_DOCUMENT_ID, 
					ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY));

			put(Tables.SERFF_DOCUMENT + _01, new CouchbaseECMMigrationDTO(Tables.SERFF_DOCUMENT, Columns.ECM_DOC_ID,
					ECMConstants.Serff.DOC_CATEGORY, ECMConstants.Serff.DOC_SUB_CATEGORY, Columns.SERFF_DOC_ID));

			put(Tables.SERFF_PLAN_DOCUMENTS_JOB + _01, new CouchbaseECMMigrationDTO(Tables.SERFF_PLAN_DOCUMENTS_JOB, Columns.UCM_ID_NEW, 
					ECMConstants.Serff.DOC_CATEGORY, ECMConstants.Serff.DOC_SUB_CATEGORY));
			put(Tables.SERFF_PLAN_DOCUMENTS_JOB + _02, new CouchbaseECMMigrationDTO(Tables.SERFF_PLAN_DOCUMENTS_JOB, Columns.UCM_ID_OLD, 
					ECMConstants.Serff.DOC_CATEGORY, ECMConstants.Serff.DOC_SUB_CATEGORY));

			put(Tables.TKM_DOCUMENTS + _01, new CouchbaseECMMigrationDTO(Tables.TKM_DOCUMENTS, Columns.DOCUMENT_PATH,
					ECMConstants.TicketMgmt.DOC_CATEGORY, ECMConstants.TicketMgmt.ATTACHMENT));
			
		}
	};

	static interface SubCategory {
		String SUBCATEGORY = "SBC";
	}

	static interface Tables {
		
		String ADMIN_DOCUMENT = "ADMIN_DOCUMENT";
		String BROKER_DOCUMENTS = "BROKER_DOCUMENTS";
		String CAP_ENROLLMENT_PAYMENT = "CAP_ENROLLMENT_PAYMENT";
		String CMR_DOCUMENTS = "CMR_DOCUMENTS";
		String D2C_ECM_DOCUMENTS = "D2C_ECM_DOCUMENTS";
		String INBOX_MSG_DOC = "INBOX_MSG_DOC";
		String ISSUER_DOCUMENT = "ISSUER_DOCUMENT";
		String ISSUERS = "ISSUERS";
		String NOTICES = "NOTICES";
		String PLAN = "PLAN";
		String PLAN_DENTAL = "PLAN_DENTAL";
		String PLAN_HEALTH = "PLAN_HEALTH";
		String PM_PLAN_NETWORK_REPORT = "PM_PLAN_NETWORK_REPORT";
		String SERFF_DOCUMENT = "SERFF_DOCUMENT";
		String SERFF_PLAN_DOCUMENTS_JOB = "SERFF_PLAN_DOCUMENTS_JOB";
		String TKM_DOCUMENTS = "TKM_DOCUMENTS";
	}

	static interface Columns {
		String DOCUMENT_PATH = "DOCUMENT_PATH";
		String UCM_ID_OLD = "UCM_ID_OLD";
		String UCM_ID_NEW = "UCM_ID_NEW";
		String SERFF_DOC_ID = "SERFF_DOC_ID";
		String ECM_DOC_ID = "ECM_DOC_ID";
		String SBC_UCM_ID = "SBC_UCM_ID";
		String RATE_FILE = "RATE_FILE";
		String BENEFIT_FILE = "BENEFIT_FILE";
		String EOC_DOC_ID = "EOC_DOC_ID";
		String BROCHURE_UCM_ID = "BROCHURE_UCM_ID";
		String SUPPORT_FILE = "SUPPORT_FILE";
		String DOCUMENT_NAME = "DOCUMENT_NAME";
		String VENDOR_DOCUMENT_ID = "VENDOR_DOCUMENT_ID";
		String ECM_DOCUMENT_ID = "ECM_DOCUMENT_ID";
		String EXISTING_ECM_ID = "EXISTING_ECM_ID";
		String ECM_ID = "ECM_ID";
		String DOCUMENT_ID = "DOCUMENT_ID";
		String CERTIFICATION_DOC = "CERTIFICATION_DOC";
		
	}
	static enum MigrationStatusEnum {
		NOTSTARTED, COUCHBASEDONE, ECMFILENOTFOUND, MIGRATIONDONE, DUPLICATE
	}

	static enum MigrationStepEnum {
		IDENTIFY, COPY, UPDATE;

		public static MigrationStepEnum fromValue(String v) {
			MigrationStepEnum data = null;
			for (MigrationStepEnum c : MigrationStepEnum.class.getEnumConstants()) {
				if (c.name().equalsIgnoreCase(v)) {
					data = c;
					break;
				}
			}
			return data;
		}
	}
}
