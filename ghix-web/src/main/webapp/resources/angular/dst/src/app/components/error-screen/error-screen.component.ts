import { Component, OnInit, Input, NgModule } from '@angular/core';

@Component({
    selector: 'error-screen',
    styleUrls: ['./error-screen.component.scss'],
    template: `<!---->
<div class="overlay"></div>
<div class="overlay-content">
    <div class="alert alert-danger" role="alert" [attr.aria-label]="message| translate"  >
        <p automationId= "not-available-error-message" style="margin-bottom:0px">{{ message | translate}}</p>
    </div>
    <button id="goback_button" automationId="goback_button" class="btn-success btn" (click)="goback()">{{'ERROR_SCREEN.button.goback' | translate}}</button>
</div>
<!---->`,
})
export class ErrorScreenComponent implements OnInit {
    @Input()
    message: string;


    constructor() { }

    ngOnInit() {
    }

    public goback() {
        let url: string;
        try {
            url = (<any>document.querySelector("#jsp_dstCancelRedirectUrl")).value;
        } catch (err) {
            url = ""
        }
        window.location.href = url
    }

}
