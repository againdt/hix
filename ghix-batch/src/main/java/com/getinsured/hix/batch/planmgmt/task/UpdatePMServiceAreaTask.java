package com.getinsured.hix.batch.planmgmt.task;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.util.GhixBatchConstants;

public class UpdatePMServiceAreaTask implements Tasklet{
	private static final Logger LOGGER = Logger.getLogger(UpdatePMServiceAreaTask.class);
	 
	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
		LOGGER.info("Inside UpdatePMServiceAreaTask" );
		Connection con = null;
		CallableStatement cstmt = null;
		try {
			Context initialContext = new InitialContext();
		    DataSource datasource = (DataSource)initialContext.lookup("java:comp/env/jdbc/ghixDS");
		      if (datasource != null) {
		    	  con = datasource.getConnection();
		      }
		      String tableName = "";
		      SimpleDateFormat sdf = new SimpleDateFormat("MM-dd H:m:s");
		      Date date = new Date();
		      
		      String sysDate = sdf.format(date);
				int month = Integer.parseInt(sysDate.substring(0, 2));
				int day = Integer.parseInt(sysDate.substring(3, 5));
				
		    
				if(month <= 3 && day <=31){
					tableName = "PM_SERVICE_AREA_QT2";
				}else if(month <= 6 && day <=31){
					tableName = "PM_SERVICE_AREA_QT3";
				}else if(month <= 9 && day <=31){
					tableName = "PM_SERVICE_AREA_QT4";
				}else{
					tableName = "PM_SERVICE_AREA_QT1";
				}
		      LOGGER.info("DataBase Meta Data Driver Name::"+ con.getMetaData().getDriverName());
		      
		      StringBuilder truncateTable = new StringBuilder("TRUNCATE TABLE ").append(tableName);
		      Statement createStmt = con.createStatement();
		      createStmt.executeUpdate(truncateTable.toString());

			String msg = null;
			boolean isPostgresDB = false;
			
			if (GhixBatchConstants.DB_TYPE != null
					&& GhixBatchConstants.DB_TYPE
							.equalsIgnoreCase(GhixBatchConstants.POSTGRESQL)) {
				isPostgresDB = true;
			}
			LOGGER.info("Postgres DB: " + isPostgresDB);

			if (isPostgresDB) {
				cstmt = con.prepareCall("{ ? = call synchronizePMServiceArea(?) }");
				cstmt.registerOutParameter(1, java.sql.Types.VARCHAR);
				cstmt.setString(2, tableName);
				cstmt.executeUpdate();
				msg = cstmt.getString(1);
			}
			else {
				cstmt = con.prepareCall("call synchronizePMServiceArea(?,?)");
				cstmt.setString(1, tableName);
				cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);
				cstmt.executeUpdate();
				msg = cstmt.getString(2);
			}
			LOGGER.info("Return message from synchronizePMServiceArea(): " + msg);

			if (msg.equalsIgnoreCase("Successfully Executed")) {
				 LOGGER.info("Successfully Executed");
				/*Statement dropStmt = con.createStatement();
				dropStmt.executeUpdate("drop synonym PM_SERVICE_AREA");
				StringBuilder createSyn = new StringBuilder("create synonym PM_SERVICE_AREA for ").append(tableName);
				Statement createStmt = con.createStatement();
				createStmt.executeUpdate(createSyn.toString());*/

				if (!isPostgresDB) {
					con.commit();
				}
			}
			else {
				LOGGER.error("failed to execute procedure");
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception occured while executing  UpdatePMServiceAreaTask", e);
		}
		finally{
			
			if (null != cstmt) {
				try {
					cstmt.close();
					cstmt = null;
				} catch (Exception sqlException) {
					LOGGER.error(sqlException.getMessage(),sqlException);
				}
			}
			
			if (null != con) {
				try {
					con.close();
					con = null;
				} catch (Exception sqlException) {
					LOGGER.error(sqlException.getMessage(),sqlException);
				}
			}
			
		}
		return null;
	}


}
