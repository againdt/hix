package com.getinsured.hix.batch.enrollment.reader;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.enrollment.service.EnrollmentMonthlyPremiumsBatchService;
import com.getinsured.hix.batch.enrollment.skip.EnrollmentMonthlyPremiumParams;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;

public class EnrollmentMonthlyPremiumsReader implements ItemReader<Integer> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyPremiumsReader.class);
	List<Integer> enrollmentIdList;		
	int partition;
	int startIndex;
	int endIndex;
	int loopCount;
	private EnrollmentMonthlyPremiumParams enrollmentMonthlyPremiumParams;
	private EnrollmentMonthlyPremiumsBatchService enrollmentMonthlyPremiumsBatchService;
	private String fillOnlySLCSP;
	private JobService jobService;
	private String updateLastSlice;
	private boolean lastSlice=false;
	long jobExecutionId = -1;
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){

		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader ");

		ExecutionContext ec = stepExecution.getExecutionContext();
		jobExecutionId=stepExecution.getJobExecution().getId();
		if(ec != null){
			partition =ec.getInt("partition");
			startIndex=ec.getInt("startIndex");
			endIndex=ec.getInt("endIndex");
			if(enrollmentMonthlyPremiumParams!=null && enrollmentMonthlyPremiumParams.getEnrollmentIdList()!=null && !enrollmentMonthlyPremiumParams.getEnrollmentIdList().isEmpty()) {
				if(startIndex<enrollmentMonthlyPremiumParams.getEnrollmentIdList().size() && endIndex<=enrollmentMonthlyPremiumParams.getEnrollmentIdList().size()){
					enrollmentIdList= new ArrayList<Integer>(enrollmentMonthlyPremiumParams.getEnrollmentIdList().subList(startIndex, endIndex));
				}
				LOGGER.info("======= Enrollment Monthly Premium Populate :: Thread Name: "+Thread.currentThread().getName() +" StartIndex: "+startIndex+" EndIndex: "+ endIndex  +" No Of householdIds: " +enrollmentIdList);
			}
		}
	}

	@Override
	public Integer read() throws Exception, UnexpectedInputException,
	ParseException, NonTransientResourceException {
		LOGGER.info("For partition "+ partition+" count value ="+loopCount);
		if(enrollmentIdList!=null && !enrollmentIdList.isEmpty()){
			
			String batchJobStatus=null;
			if(jobService != null && jobExecutionId != -1){
				try{
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
				}catch(Exception e){
					LOGGER.error("Failed to get job status :: "+e.getMessage());
				}
			}
			
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new RuntimeException(EnrollmentConstants.BATCH_STOP_MSG);
			}
			
			Integer enrollmentId=null;
			if(loopCount<enrollmentIdList.size()){
				loopCount++;
				enrollmentId=enrollmentIdList.get(loopCount-1);
				try{
					enrollmentMonthlyPremiumsBatchService.populateMonthlyEnrolmentPremium(enrollmentId, fillOnlySLCSP, lastSlice);
				}catch(Exception e){
					enrollmentMonthlyPremiumParams.putToSkippedEnrollmentMap(enrollmentId, EnrollmentUtils.shortenedStackTrace(e, 2));
				}
				return enrollmentId;
			}else{
				return null;
			}
		}
		return null;
	}

	/**
	 * @return the enrollmentMonthlyPremiumParams
	 */
	public EnrollmentMonthlyPremiumParams getEnrollmentMonthlyPremiumParams() {
		return enrollmentMonthlyPremiumParams;
	}

	/**
	 * @param enrollmentMonthlyPremiumParams the enrollmentMonthlyPremiumParams to set
	 */
	public void setEnrollmentMonthlyPremiumParams(EnrollmentMonthlyPremiumParams enrollmentMonthlyPremiumParams) {
		this.enrollmentMonthlyPremiumParams = enrollmentMonthlyPremiumParams;
	}

	public EnrollmentMonthlyPremiumsBatchService getEnrollmentMonthlyPremiumsBatchService() {
		return enrollmentMonthlyPremiumsBatchService;
	}

	public void setEnrollmentMonthlyPremiumsBatchService(
			EnrollmentMonthlyPremiumsBatchService enrollmentMonthlyPremiumsBatchService) {
		this.enrollmentMonthlyPremiumsBatchService = enrollmentMonthlyPremiumsBatchService;
	}

	public String getFillOnlySLCSP() {
		return fillOnlySLCSP;
	}

	public void setFillOnlySLCSP(String fillOnlySLCSP) {
		this.fillOnlySLCSP = fillOnlySLCSP;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public String getUpdateLastSlice() {
		return updateLastSlice;
	}

	public void setUpdateLastSlice(String updateLastSlice) {
		this.updateLastSlice = updateLastSlice;
		if(updateLastSlice!=null && !updateLastSlice.equalsIgnoreCase("null")){
			this.lastSlice= Boolean.parseBoolean(updateLastSlice.toLowerCase());
		}
	}
	
}
