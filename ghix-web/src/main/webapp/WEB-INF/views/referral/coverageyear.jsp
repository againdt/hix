<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.springframework.validation.FieldError"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
	<div class="row-fluid" id="titlebar">
		<h1>
			<spring:message code="label.referral.pageheader" />
		</h1>
	</div>
	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
				<h4>
					<spring:message code="label.referral.sideheader" />
				</h4>
			</div>
			<ul class="nav nav-list">
				<li><spring:message code="label.referral.securityquestions" />
				</li>
				<li class="active"><spring:message
						code="label.referral.coverageyear" /></li>
				<li><spring:message code="label.referral.linktoapplication" />
				</li>
			</ul>
		</div>
		<% 
			 Object fieldErrorObj = session.getAttribute("fieldErrors");
			 if(fieldErrorObj != null)
	         {
	        	 %>
						<font color="red"><spring:message code="${fieldErrors}" /></font><br/>
				<%         	 
	         	session.removeAttribute("fieldErrors");
	         }
	         %>	
		<div id="rightpanel" class="span9 dashboard-rightpanel">
			<div class="header">
				<h4>
					<spring:message code="label.referral.coverageyear" />
				</h4>
			</div>
			<form:form id="frmReferral"
				name="frmReferral" method="POST"
				class="form-horizontal gutter10">
				<df:csrfToken />
				<div>
					<div class="control-group">
						<label class="control-label"><spring:message code="label.referral.select.coverageyear" /><img
							src="<c:url value="/resources/images/requiredAsterix.png" />"
							width="10" height="10" alt="Required!" />
						</label>
						<div class="controls">
							<select name="coverageYear" class="input-small" >
								<c:forEach items="${coverageYears}" var="coverageYear">
									<option value='${coverageYear}'>${coverageYear}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-actions">
						<input type="submit"
							value="<spring:message code="label.submit-button"/>"
							class="btn btn-primary pull-right">
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>
