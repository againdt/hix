package com.getinsured.hix.agency.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.agency.AgencyAud;

public interface IAgencyAudRepository extends JpaRepository<AgencyAud, Long> {

	@Query(nativeQuery=true, 
			value= "select distinct a.id,a.last_update_timestamp,a.CERTIFICATION_STATUS,c.COMMENT_TEXT,u.FIRST_NAME,u.LAST_NAME, "
					+ "a.AGENCY_SUPPORT_DOC_ID,a.AGENCY_CONTRACT_DOC_ID,a.AGENCY_E_O_DECLR_DOC_ID,a.REV from Agency_Aud a "
					+ "left join users u on a.last_updated_by=u.ID  "
					+ "left join comments c on c.id=a.COMMENTS_ID "
					+ "where a.id = :agencyId and a.CERTIFICATION_STATUS is not null order by REV ASC")
	List<Object[]> getAgencyCertificationHistory(@Param("agencyId")long agencyId);
}
