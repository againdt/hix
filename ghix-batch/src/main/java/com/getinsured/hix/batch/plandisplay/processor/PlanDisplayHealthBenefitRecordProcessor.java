package com.getinsured.hix.batch.plandisplay.processor;
 
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
 
import com.getinsured.hix.batch.plandisplay.reader.PlanDisplayHealthBenefitMapper;
 
public class PlanDisplayHealthBenefitRecordProcessor implements ItemProcessor<PlanDisplayHealthBenefitMapper, SolrInputDocument>{
        private static Logger logger = LoggerFactory.getLogger(PlanDisplayHealthBenefitRecordProcessor.class);
 
        @Override
        public SolrInputDocument process(PlanDisplayHealthBenefitMapper fromReader) throws Exception {
                logger.info("In process ---> "+fromReader.getName());
                return prepareSolrDocument(fromReader);
        }
        
        private SolrInputDocument prepareSolrDocument(PlanDisplayHealthBenefitMapper providerData){
                
                SolrInputDocument solrInputDocument = providerData.getSolrInputDocument();
                logger.info("Processing Data completed : "+providerData.getName());
                return solrInputDocument;
        }
 
}