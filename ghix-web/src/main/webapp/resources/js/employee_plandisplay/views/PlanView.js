(function (views) {
	views.Plans = Backbone.View.extend({
		events : {
			'click a.addToCart': 'addToCart',
			'click a.removeCartItem': 'removeCartItem',
			'click a.addToCompare' : 'addToCompare',
			'click a.removeCompare' : 'removeCompare',
			'click a.showCart' : 'showCart',
			'mouseover a.showCart' : 'showCart',
			'click button.keepCartPlan' : 'keepCartPlan',
			'click button.replaceCartPlan' : 'replaceCartPlan',
			'click a.saveCart' : 'saveCart'
				
		},
		el : '#mainSummary',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'loadAll', 'addOne','addToCart','addToCompare','removeCompare','showCart','removeCartItem','keepCartPlan','replaceCartPlan','saveCart');
			var self = this;
			
			
			self.collection.fetch({
				success : function () {

					if(self.collection.length==0){
						$('#mainSummary').empty();
						$('#mainSummary').hide();
						$('#mainSummary_err').show();
					}
						
					self.collection.setSort('planScore', 'desc');
					self.collection.pager();
					self.loadAll();
					
					
				},
				silent:true
			});
			
			self.collection.cartSummary.fetched = false;
			self.cartCollection = options.cartCollection;
			
			
			self.cartCollection.fetch({
				success : function () {
					self.cartCollection.setCartSummary(false);
					self.collection.cartSummary.fetched = true;
				}
			});
			
			self.planModel = options.planModel;
			self.favoriteModel = options.favriteModel;
			
			$('.addToCart').bind('click', this.addToCart);
			$('.showCart').bind('click', this.showCart);
			$('.showCart').bind('mouseover', this.showCart);
			$('.saveCart').bind('click', this.saveCart);
			
			$('.replaceCartPlan').bind('click', this.replaceCartPlan);
			$('.keepCartPlan').bind('click', this.keepCartPlan);
			
			
			self.collection.bind('reset', self.addAll);
		},

		addAll : function () {
			var self = this;
			$('#mainSummary').empty();
			$('#planSummary').empty();
			$('#yourSavings').empty();
			$('#cost').empty();
			$('#doctorVisits').empty();
			$('#drugs').empty();
			$('#labServices').empty();
			$('#emergencyServices').empty();
			$('#maternity').empty();
			$('#pediatricServices').empty();
			$('#mentalhealth').empty();
			$('#planRatings').empty();
			self.collection.each (self.addOne);
		},
		
		loadAll : function () {
			var self = this;
			self.collection.loadIssuers();
			self.loadCartSummary();
			if(self.collection.showCompare==false){
				self.loadCompareSummary();
			}
			self.collection.updateCompareSummary();
		},
		
		addOne : function (model) {
			var self = this;
		
			var viewSummary = new Summary({model:model});

			$('#mainSummary').append(viewSummary.render());
			$('#planSummary').append(viewSummary.getPlanSummary());
			$('#yourSavings').append(viewSummary.getYourSavings());
			$('#cost').append(viewSummary.getCost());
			$('#doctorVisits').append(viewSummary.getDoctorVisits());
			$('#drugs').append(viewSummary.getDrugs());
			$('#labServices').append(viewSummary.getLabServices());
			$('#emergencyServices').append(viewSummary.getEmergencyServices());
			$('#maternity').append(viewSummary.getMaternity());
			$('#pediatricServices').append(viewSummary.getPediatricServices());
			$('#mentalhealth').append(viewSummary.getMentalhealth());
			$('#planRatings').append(viewSummary.getPlanRatings());
		},
		
		loadCartSummary : function () {
			var self = this;
			var fetchedVal = self.collection.cartSummary.fetched;
			if(fetchedVal == false){
				$('#planCount').html("(Loading...)");
				setTimeout(function(){self.loadCartSummary();}, 1000);
				return;
			}else{
				self.collection.loadCartSummary(self.cartCollection.cartSummary,self.cartCollection.cartItems, self.cartCollection.selectedCartItem);
				this.updateCartSummary();
				this.updateCart();
			}
		},
		
		loadCompareSummary: function(){
			var self = this;
			_.each(self.collection.orgmodels, function(model){
				if(model.get("isFavorite")=='YES'){
					self.collection.addToCompare(model.get("id"));
					$('#compareCount').html(self.collection.compareModels.length);
					$('#compare_'+model.get("id") + '.addToCompare').hide();
					$('#remove_'+model.get("id") + '.removeCompare').show();
				}
			});
		},
		searchPlan: function (models,planId) {
			var planModel = "";
			_.each(models, function(model){
				if( model.get("id") == planId) {
					planModel = model;
					
				}
			});
			
			return planModel;
		},
		
		searchCart: function (cartItems,planId) {
			var isPlanPresent = false;
			_.each(cartItems, function(cartItem){
				if( cartItem == planId) {
					isPlanPresent = true;
				}
			});
			return isPlanPresent;
		},
		addToCart: function (e) {
			var self = this;
			var planModel = "";
			if(self.collection.cartSummary.fetched != false){
				itemId = e.target.id.replace("cart_","");
				planModel = this.searchPlan(this.collection.models, itemId);
				
				//if plan is not in cart collection
				if( jQuery.inArray(itemId, this.collection.cartItems) === -1 ){
					
					// if cart collection is empty
					if(self.collection.cartItems.length > 0){
						var existingCartId = self.collection.selectedCartItem;
						var existingCartModel = this.searchPlan(this.collection.models, existingCartId);
						$('#missinginfo').modal("show");
						$('#planInCart').html(existingCartModel.get("name"));
						$('#planSelected').html(planModel.get("name"));
						$('#newCartPlan').val(itemId);
					}
					else{ // if plan is in cart array
						$("#cart_"+itemId).hide();
						$("#removeCart_"+itemId).show();
						$('#cart-box').show();
						self.collection.setCartSummary(itemId);
						self.showCartDiv();
						
						//save to DB cart
						var cartModel = new App.models.Plan({id:self.collection.selectedCartItem});
						cartModel.save();
						
						$('#cartmessageplan').fadeIn(250,function() {
							setTimeout(function(){
								$('#cartmessageplan').hide();
							},1500)
						});
					}
				}
				else{ // if selected plan is already in cart
					alert('This plan is already in Cart !!!!');
				}
				this.updateCartSummary();
			}
			else{	
				alert('Please wait cart is loading !!!');
			}
		},
		
		removeCartItem: function (e) {
			var self=this;
			
			if(e.target.id.indexOf('removeCart_') >= 0){
				itemId = e.target.id.replace("removeCart_","");
			}else{
				itemId = e.target.id.replace("plan_","");
				$("#cart-box").hide();
			}
			
			self.collection.removeFromCart(itemId);
			$("#cart_"+itemId).show();
			$("#removeCart_"+itemId).hide();
			
			//removing element from DB cart
			
			var CartModel = new App.models.Cart({id:itemId});
			CartModel.destroy();
			
			$('#cartmessageplanremove').fadeIn(250,function() {
				setTimeout(function(){
					$('#cartmessageplanremove').hide();
				},1500)
			});
			this.updateCartSummary();
		},
		updateCartSummary : function () {
			var self = this;

			$('#planCount').html("("+self.collection.cartSummary.planCount+")");
			_.each(this.collection.cartItems, function(cartItem){
				$("#cart_"+cartItem).hide();
				$("#removeCart_"+cartItem).show();
			});
			
			
		},
		
		updateCart : function () {
			var self = this;
			var monthlyPayrollDeduction = 0;
			$('#mpdValue').html(monthlyPayrollDeduction);
			
			if(this.collection.cartItems.length > 0){
					var existingCartId = self.collection.selectedCartItem;
					var existingCartModel = this.searchPlan(this.collection.models, existingCartId); 
					var template_cart = _.template($('#tmpCart').html());
					monthlyPayrollDeduction +=  existingCartModel.get("monthlyPayrollDeduction");
					$('#tbody_cart').html(template_cart(existingCartModel.toJSON()));
					$('.removeCartElement').bind('click', this.removeCartItem);
					$('#mpdValue').html(monthlyPayrollDeduction);
					$('#chkOutBtn').show();
					$('#cartBtn').show();
					$('#emptyCart').hide();
					$('#cart-body').show();
			}else{
				$('#chkOutBtn').hide();
				$("#cart").hide();
			}
			
		},
		
		emptyCart : function () {
			$("#cart").show();
			$('#emptyCart').show();
			$('#cart-body').hide();
		},
		
		showCart : function () {
			$('#cart-box').show();
			this.emptyCart();
			this.updateCart();
		},
		
		showCartDiv : function () {
			$('#cart-box').show();
			this.emptyCart();
			this.updateCart();
			$('#cart-box').fadeIn(500,function() {
				/*setTimeout(function(){
					$('#cart-box ').hide();
				},1500)*/
			});
		},
		saveCart : function (e) {
			var self=this;
			if(self.collection.selectedCartItem !="" ){
					location.href="plancart";	
			}
			else{
				alert('Please Select One Plan to Proceed !!!');
			}
		},
		
		
		keepCartPlan : function () {
			this.emptyCart();
			$('#newCartPlan').val("");
			$('#cart-box').show();
			this.updateCart();
			$('#missinginfo').modal("hide");
		},
		replaceCartPlan: function () {
			var self = this;
			
			//first remove existing item from cart
			var existingCartId = self.collection.selectedCartItem;
			self.collection.removeFromCart(existingCartId);
			
			$("#cart_"+existingCartId).show();
			$("#removeCart_"+existingCartId).hide();
			
			//now add new item in cart
			var itemId = $('#newCartPlan').val();
			self.collection.setCartSummary(itemId);
			$("#cart_"+itemId).hide();
			$("#removeCart_"+itemId).show();
			$('#newCartPlan').val("");
			$('#missinginfo').modal("hide");
			//save cart to DB	
			//save to DB cart
			var cartModel = new App.models.Plan({id:itemId});
			cartModel.save();

			this.showCartDiv();
			
			$('#cartmessageplanreplace').fadeIn(250,function() {
				setTimeout(function(){
					$('#cartmessageplanreplace').hide();
				},1500)
			});
		},
		
		addToCompare: function (e) {
			if(e.target.id.indexOf('ompare_')){
				$('#favmessage').fadeIn(250,function() {
					setTimeout(function(){
						$('#favmessage').hide();
					},1500);
				});
			}
			this.collection.addToCompare(e.target.id.replace("compare_",""));
			$('#compareCount').html(this.collection.compareModels.length);
			
			var selid = e.target.id.replace("compare_","");
			
			$('#compare_'+selid + '.addToCompare').hide();
			$('#remove_'+selid + '.removeCompare').show();
			
			//Persisting favorite
			var FavoriteModel = new App.models.Favorite({
				id:selid
			});
			FavoriteModel.save();
		},
		
		removeCompare: function (e) {
			var self=this;
			/* if not compare view*/
			if(this.collection.showCompare == false){
				this.collection.removeCompare(e.target.id.replace("remove_",""),"pview");
				var selid = e.target.id.replace("remove_","");
				$('#compare_'+selid + '.addToCompare').show();
				$('#remove_'+selid + '.removeCompare').hide();
						
			}
			else{ /*if from compare view*/
				var selid = e.target.id.replace("remove_","");
				this.collection.removeCompare(selid,"cview");
				/*if no items in compare array*/
				if(this.collection.compareModels.length < 1)
					this.collection.showCompare == false;
				
			}
			
			//Persisting favorite
			var FavoriteModel = new App.models.Favorite({
				id:selid
			});
			FavoriteModel.destroy();
			this.collection.updateCompareSummary();
		}
		
	});
	
	var Summary = Backbone.View.extend({
		template: _.template($('#resultItemTemplateMainSummary').html()),
		template_planSummary: _.template($('#resultItemTemplatePlanSummary').html()),
		template_yourSavings: _.template($('#resultItemTemplateSavings').html()),
		template_cost: _.template($('#resultItemTemplateCost').html()),
		template_doctorVisits: _.template($('#resultItemTemplateDoctorVisit').html()),
		template_drugs: _.template($('#resultItemTemplateDrugs').html()),
		template_labServices: _.template($('#resultItemTemplateLabServices').html()),
		template_emergencyServices: _.template($('#resultItemTemplateEmergencyServices').html()),
		template_maternity: _.template($('#resultItemTemplateMaternity').html()),
		template_pediatricServices: _.template($('#resultItemTemplatePediatricServices').html()),
		template_mentalhealth: _.template($('#resultItemTemplateMentalHealth').html()),
		template_planRatings: _.template($('#resultItemTemplatePlanRatings').html()),

		initialize: function() {
			this.model.bind('change', this.render, this);
			this.model.bind('destroy', this.remove, this);
		},
		
		render : function () {
			return this.template(this.model.toJSON());
		},
		
		getPlanSummary : function () {
			return this.template_planSummary(this.model.toJSON());
		},
		getYourSavings : function () {
			return this.template_yourSavings(this.model.toJSON());
		},
		getCost : function () {
			return this.template_cost(this.model.toJSON());
		},
		getDoctorVisits : function () {
			return this.template_doctorVisits(this.model.toJSON());
		},
		getDrugs : function () {
			return this.template_drugs(this.model.toJSON());
		},
		getLabServices : function () {
			return this.template_labServices(this.model.toJSON());
		},
		getEmergencyServices : function () {
			return this.template_emergencyServices(this.model.toJSON());
		},
		getMaternity : function () {
			return this.template_maternity(this.model.toJSON());
		},
		getPediatricServices : function () {
			return this.template_pediatricServices(this.model.toJSON());
		},
		getMentalhealth : function () {
			return this.template_mentalhealth(this.model.toJSON());
		},
		getPlanRatings : function () {
			return this.template_planRatings(this.model.toJSON());
		}
	});
})(App.views);