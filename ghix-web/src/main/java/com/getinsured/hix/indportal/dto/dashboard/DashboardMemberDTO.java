package com.getinsured.hix.indportal.dto.dashboard;

public class DashboardMemberDTO {

	private String dob;
	private String relation;
	private boolean seekingCoverage;
	private boolean nativeAmerican;

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public boolean getSeekingCoverage() {
		return seekingCoverage;
	}

	public void setSeekingCoverage(boolean seekingCoverage) {
		this.seekingCoverage = seekingCoverage;
	}

	public boolean getNativeAmerican() {
		return nativeAmerican;
	}

	public void setNativeAmerican(boolean nativeAmerican) {
		this.nativeAmerican = nativeAmerican;
	}

}
