package com.getinsured.iex.hub.nmec.wrapper;


import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.nmec.niem.niem.usps_states._2.USStateCodeSimpleType;
import com.getinsured.iex.hub.nmec.niem.niem.usps_states._2.USStateCodeType;

/**
 * @author Vishaka Tharani
 * @since February 25, 2014
 *
 */
public class USStateCodeTypeWrapper implements JSONAware{

	private static com.getinsured.iex.hub.nmec.niem.niem.usps_states._2.ObjectFactory stateFactory;
	
	private USStateCodeType usStateCodeType = null;
	private String locationStateUSPostalServiceCode;

	public USStateCodeTypeWrapper() {
		stateFactory = new com.getinsured.iex.hub.nmec.niem.niem.usps_states._2.ObjectFactory();
	}

	public USStateCodeType getSystemRepresentation() {
		return this.usStateCodeType;
	}

	
	public void setLocationStateUSPostalServiceCode(String locationStateUSPostalServiceCode)
	{
		if(locationStateUSPostalServiceCode != null && locationStateUSPostalServiceCode.length() > 0){
			if(usStateCodeType == null)	{
			usStateCodeType = stateFactory.createUSStateCodeType();
			}
			usStateCodeType.setValue(USStateCodeSimpleType.valueOf(locationStateUSPostalServiceCode));
			this.locationStateUSPostalServiceCode = locationStateUSPostalServiceCode;
		}
	}
	
	public String getLocationStateUSPostalServiceCode() {
		return this.locationStateUSPostalServiceCode;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("LocationStateUSPostalServiceCode",locationStateUSPostalServiceCode);
		return obj.toJSONString();
	}
}
