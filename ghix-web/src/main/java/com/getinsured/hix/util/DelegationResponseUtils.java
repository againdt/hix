package com.getinsured.hix.util;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.model.DelegationRequest;
import com.getinsured.hix.model.DelegationResponse;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints.AHBXEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

@Component
public class DelegationResponseUtils 
{
	@Autowired private RestTemplate restTemplate;
	private static final Logger LOGGER = Logger.getLogger(DelegationResponseUtils.class);
	
	/**
	 * 
	 * @param criteria
	 * @return
	 * @throws GIException
	 * rest call to GHIX-AHBX for retrieving Delegation info.
	 */
	public DelegationResponse validateIssuerResponse(DelegationRequest delagationrequest)throws GIException
	{
		DelegationResponse delegationResponse = new DelegationResponse();
		
		try{
			//get the response
			String response = restTemplate.postForObject(AHBXEndPoints.DELEGATIONCODE_RESPONSE, delagationrequest, String.class);
			/**
			 * UnMarshal and from PlatformResponse object
			 */
			XStream xstream = GhixUtils.getXStreamStaxObject();
			delegationResponse =  (DelegationResponse) xstream.fromXML(response);

			/**
			 * If AHBX call failed, throw Exception to the caller
			 */
			if (delegationResponse != null && delegationResponse.getStatus().equals("FAILURE"))
			{
				LOGGER.warn("Received failure response: Error_Code - "+delegationResponse.getErrCode()+" Message - "+delegationResponse.getErrMsg());
				throw new GIException(delegationResponse.getErrCode(), delegationResponse.getErrMsg(), "WARN");
			}
			
			
		}catch(HttpClientErrorException ex){
			
			LOGGER.error("Failure to receive response from Service ::"+AHBXEndPoints.DELEGATIONCODE_RESPONSE);
			LOGGER.error("Error_Code - "+ex.getStatusCode()+" Message - "+ex.getMessage());
			delegationResponse.setErrCode(Integer.parseInt(ex.getStatusCode().toString()));
			delegationResponse.setErrMsg(delegationResponse.getErrMsg());
			
			
		}catch(Exception ex){
			LOGGER.error("Exception :: "+ex.getMessage());
			delegationResponse.setErrCode(500);
			delegationResponse.setErrMsg(ex.getMessage());
			
		}
					
		return delegationResponse;
	}

	
	/*
	 	public DelegationResponse validateIssuerResponse(DelegationRequest delagationrequest)throws GIException
	{
		/ *final String recordId ="recordId";
		final String recordType = "recordType";* /
		DelegationResponse delegationResponse = new DelegationResponse();
		

		//get the response
		/ *if(criteria == null || criteria.size() == 0 )
		{	
			delegationResponse.setErrCode(7000);
			delegationResponse.setErrMsg("");
			delegationResponse.setModuleStatusCode("");
			
			return delegationResponse;
		}* /
		
		/ *DelegationRequest delegateRequest = new DelegationRequest();
			if (criteria.get(recordId) != null	&& !criteria.get(recordId).toString().isEmpty()
					&& criteria.get(recordType) != null && !criteria.get(recordType).toString().isEmpty())
			{
				delegateRequest.setRecordId(Long.valueOf(criteria.get(recordId).toString()));
				delegateRequest.setRecordType(criteria.get(recordType).toString());* /
			
				String response = restTemplate.postForObject(AHBXEndPoints.DELEGATIONCODE_RESPONSE, delagationrequest, String.class);
				/ **
				 * UnMarshal and from PlatformResponse object
				 * /
				XStream xstream = GhixUtils.getXStreamStaxObject();
				delegationResponse =  (DelegationResponse) xstream.fromXML(response);
			/ *}
			else
			{
				LOGGER.info("Invalid input or required parameters are null");
				delegationResponse.setModuleStatusCode("");
				delegationResponse.setStatus("RESPONSE_FAILURE");
				
				if(criteria.get(recordId) == null)
				{
					delegationResponse.setErrMsg("Invalid Data: RecordId is null or empty");
				}
				else if(criteria.get(recordType) == null)
				{
					delegationResponse.setErrMsg("Invalid Data: RecordType is null or empty");
				}
			}* /
			/ * *
			 * If AHBX call failed, throw Exception to the caller
			 * /
			if (delegationResponse != null && delegationResponse.getStatus().equals("FAILURE"))
			{
				LOGGER.warn("Received failure response: Error_Code - "+delegationResponse.getErrCode()+" Message - "+delegationResponse.getErrMsg());
				throw new GIException(delegationResponse.getErrCode(), delegationResponse.getErrMsg(), "WARN");
			}	
		return delegationResponse;
	}

	 
	 */
}
