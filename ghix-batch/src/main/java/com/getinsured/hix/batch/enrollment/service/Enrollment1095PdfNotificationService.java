package com.getinsured.hix.batch.enrollment.service;

import java.util.Map;

import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @Since 16th September 2015
 * @author Sharma_K
 * Interface to send 1095 Pdf notifcation to Consumer Over Secure Inbox
 */
public interface Enrollment1095PdfNotificationService 
{
	
	/**
	 * 
	 * @param noticeId Integer Notice ID {Notices next Seq. ID}
	 * @param pdfFileName String Expected File Name: "1095A_Form_Initial_timeStamp.pdf", "1095A_Form_Correction_timeStamp.pdf" ,"1095A_Form_Void_timeStamp.pdf"
	 * @param pdfDataInbytes byte[] //PDF Data in Bytes
	 * @param houseHoldCaseId Integer //CMR HouseHoldCaseID
	 * @param enrollmentId EnrollmentID
	 * @param policyIssuerName Carrier Name
	 * @return String ECM_DOC_ID of PDF sent to Consumer
	 * @throws GIException
	 */
	String send1095ASecureInboxMsg(final Integer noticeId, final String pdfFileName, final byte[] pdfDataInbytes, final Integer houseHoldCaseId, final Integer enrollmentId, final String policyIssuerName, final String emailBody) throws GIException;
	
	/**
	 * 
	 * @param noticeId Notice ID {Notices next Seq. ID}
	 * @param pdfFileName String Expected File Name: "1095A_Form_Initial_timeStamp.pdf", "1095A_Form_Correction_timeStamp.pdf" ,"1095A_Form_Void_timeStamp.pdf"
	 * @param houseHoldCaseId //CMR HouseHoldCaseID
	 * @param previousEcmDocId //ECM_DOC_ID of last pdf sent to consumer
	 * @param enrollmentId EnrollmentID
	 * @param policyIssuerName Carrier Name
	 * @return String ECM_DOC_ID of newly sent pdf 
	 * @throws GIException
	 */
	String resend1095ASecureInboxMsg(final Integer noticeId, final String pdfFileName, final Integer houseHoldCaseId, final String previousEcmDocId, final Integer enrollmentId, final String policyIssuerName, final String emailBody) throws GIException;
	
	/**
	 * 
	 * @param requestMap Map<String, String>
	 * @throws GIException
	 */
	void sendPdfGenerationEmail(final Map<String, String> requestMap)throws GIException;
}
