package com.getinsured.eligibility.at.ref.service;

import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
@Component("referralApplicationCompareService")
@DependsOn("dynamicPropertiesUtil")
@Scope("singleton")
public class ReferralApplicationCompareServiceImpl implements ReferralApplicationCompareService {
	private static final Logger LOGGER = Logger.getLogger(ReferralApplicationCompareServiceImpl.class);

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Override
	public void executeCompare(SingleStreamlinedApplication singleStreamlinedApplication, long compareApplicationId) {
		LOGGER.info("Compare Current Referral with Previous Application starts");
		
		//As per Srinis comment (e-MAIL#Subject-ID state specific check)Comparison will be applicable for all states
		/*if (!isStateID()) {
			LOGGER.debug("Comparison only for ID state");
			return;
		}*/

		if (compareApplicationId == 0l) {
			LOGGER.debug("No compared to applications");
			return;
		}

		SsapApplication previousApplication = loadSsapApplication(compareApplicationId);

		if (previousApplication == null) {
			LOGGER.debug("No previous applications");
			return;
		}
		LOGGER.info("Found Previous Application - " + previousApplication.getId());
		final List<SsapApplicant> applicants = previousApplication.getSsapApplicants();
		final List<HouseholdMember> householdMembers = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
		final boolean isExemptHouseHold = checkIsExemptHouseHold(previousApplication, singleStreamlinedApplication);
		
		comparePreviousApplication(applicants, householdMembers, isExemptHouseHold);
		LOGGER.info("Compare Current Referral with Previous Application ends");
	}
	
	private boolean checkIsExemptHouseHold(SsapApplication previousApplication, SingleStreamlinedApplication singleStreamlinedApplication) {
		singleStreamlinedApplication.setExemptHousehold(previousApplication.getExemptHousehold());
		return StringUtils.equalsIgnoreCase(ReferralConstants.Y, previousApplication.getExemptHousehold());
	}

	private void comparePreviousApplication(List<SsapApplicant> applicants, List<HouseholdMember> householdMembers, boolean isExemptHouseHold) {
		final int iSize = ReferralUtil.listSize(applicants);
		final int jSize = ReferralUtil.listSize(householdMembers);
		SsapApplicant applicant = null;
		HouseholdMember householdMember = null;
		for (int i = 0; i < iSize; i++) {
			applicant = applicants.get(i);
			for (int j = 0; j < jSize; j++) {
				householdMember = householdMembers.get(j);
				if(isExistingMember(applicant,householdMember)) {
				householdMember.setApplicantGuid(applicant.getApplicantGuid());
					householdMember.setTobaccoUserIndicator(applicant.getTobaccouser() != null && ReferralConstants.Y.equals(applicant.getTobaccouser())); /* Carry Tobacco Indicators */
					if (isExemptHouseHold) {
						householdMember.setHardshipExempt(applicant.getHardshipExempt());
						householdMember.setEcnNumber(applicant.getEcnNumber());
					}
					break;
				}
			}
		}
	}
	
	private boolean isExistingMember(SsapApplicant applicant, HouseholdMember householdMember) {
		/*
		 * Order of Member matching
		 * 1. Check externalApplicantId matched
		 * 2. Check SSN AND DOB matched
		 * 3. Check FirstName LastName and DOB matched
		 * else 
		 * return member not matched
		 * */
		
		if ( StringUtils.equalsIgnoreCase(applicant.getExternalApplicantId(), householdMember.getExternalId()))
		{
			return true;
		}
		else if(null!=applicant.getSsn() &&  
				Objects.equals(applicant.getSsn(), householdMember.getSocialSecurityCard().getSocialSecurityNumber()) 
				&& Objects.equals(applicant.getBirthDate(), householdMember.getDateOfBirth() )) {
			return true;
		}
		else if(Objects.equals(applicant.getBirthDate(), householdMember.getDateOfBirth()) &&
				StringUtils.equalsIgnoreCase(applicant.getFirstName(), householdMember.getName().getFirstName()) &&
				StringUtils.equalsIgnoreCase(applicant.getLastName(), householdMember.getName().getLastName()))
		{
			return true;
		}
		return false;
	}
	private boolean compareWithSsnDOB(SsapApplicant applicant, HouseholdMember householdMember) {
		 
		if(Objects.equals(applicant.getSsn(), householdMember.getSocialSecurityCard().getSocialSecurityNumber()) && Objects.equals(applicant.getBirthDate(), householdMember.getDateOfBirth() )) {
			return true;
		}
		 
		return false;
	}

	private SsapApplication loadSsapApplication(long appId) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(appId);

	}

	private boolean isStateID() {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		return "ID".equals(stateCode);
	}

}
