package com.getinsured.hix.platform.batch.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.admin.service.NoSuchStepExecutionException;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 */
@Component
public class StopOperationTracker {
    private static final Logger logger = LoggerFactory.getLogger(StopOperationTracker.class);
    private final JobService jobService;
    private final JobRepository jobRepository;

    // TODO: parameterize
    private static final long DEFAULT_STEP_STOP_DELAY_SECONDS = 5;
    private static final long DEFAULT_JOB_STOP_DELAY_SECONDS = 10;

    @Autowired
    public StopOperationTracker(JobService jobService,
                                JobRepository jobRepository) {
        this.jobService = jobService;
        this.jobRepository = jobRepository;
    }

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private final Set<Long> stepTracker = new HashSet<>();
    private final Set<Long> jobTracker = new HashSet<>();

    public boolean triedStop(StepExecution stepExecution) {
        boolean result = stepTracker.contains(stepExecution.getId());
        logger.info("triedStop={}, {}", result, stepExecution);
        return result;
    }

    public void scheduleStop(StepExecution stepExecution, long stopDelaySeconds) {
        if (!stepTracker.contains(stepExecution.getId())) {
            stepTracker.add(stepExecution.getId());
            scheduler.schedule(new StepExecutionStopper(stepExecution), stopDelaySeconds > 0 ? stopDelaySeconds : DEFAULT_STEP_STOP_DELAY_SECONDS, TimeUnit.SECONDS);
        }
    }

    public class StepExecutionStopper implements Runnable {
        private final Long jobId;
        private final Long stepId;
        private final boolean isSingleStepJob;

        public StepExecutionStopper(StepExecution stepExecution) {
            this.jobId = stepExecution.getJobExecutionId();
            this.stepId = stepExecution.getId();
            isSingleStepJob = stepExecution.getJobExecution().getStepExecutions().size() == 1;
        }

        @Override
        public void run() {
            try {
                StepExecution stepExecution = jobService.getStepExecution(jobId, stepId);
                if (stepExecution.getStatus().isLessThan(BatchStatus.STOPPED) && !stepExecution.getStatus().equals(BatchStatus.COMPLETED)) {
                    stepExecution.setStatus(BatchStatus.STOPPED);
                    stepExecution.setExitStatus(ExitStatus.STOPPED);
                    if (isSingleStepJob) {
                        jobRepository.updateExecutionContext(stepExecution);
                    }

                    stepExecution.setEndTime(new Date());
                    stepExecution.setExitStatus(ExitStatus.STOPPED);
                    if (isSingleStepJob) {
                        jobRepository.update(stepExecution);
                    }

                    logger.info("Updated step to STOPPED: {}", stepExecution);
                }
            } catch (NoSuchStepExecutionException | NoSuchJobExecutionException e) {
                logger.error(e.getMessage(), e);
            } finally {
                stepTracker.remove(stepId);
            }
        }
    }

    public void scheduleStop(JobExecution jobExecution, long stopDelaySeconds) {
        if (!jobTracker.contains(jobExecution.getId())) {
            jobTracker.add(jobExecution.getId());
            scheduler.schedule(new JobExecutionStopper(jobExecution), stopDelaySeconds > 0 ? stopDelaySeconds : DEFAULT_JOB_STOP_DELAY_SECONDS, TimeUnit.SECONDS);
        }
    }

    public class JobExecutionStopper implements Runnable {
        private final long jobId;
        private final boolean isSingleStepJob;

        public JobExecutionStopper(JobExecution jobExecution) {
            this.jobId = jobExecution.getId();
            isSingleStepJob = jobExecution.getStepExecutions().size() == 1;
        }

        @Override
        public void run() {
            try {
                JobExecution jobExecution = jobService.getJobExecution(jobId);
                if (jobExecution.getStatus().isLessThan(BatchStatus.STOPPED) && !jobExecution.getStatus().equals(BatchStatus.COMPLETED)) {
                    jobExecution.setStatus(BatchStatus.STOPPED);
                    if (isSingleStepJob) {
                        jobRepository.updateExecutionContext(jobExecution);
                    }
                    jobExecution.setEndTime(new Date());
                    jobExecution.setExitStatus(ExitStatus.STOPPED);
                    if (isSingleStepJob) {
                        jobRepository.update(jobExecution);
                    }
                    logger.info("Updated job to STOPPED: {}", jobExecution);
                }
            } catch (NoSuchJobExecutionException e) {
                logger.error(e.getMessage(), e);
            }
            finally {
                jobTracker.remove(jobId);
            }
        }
    }
}