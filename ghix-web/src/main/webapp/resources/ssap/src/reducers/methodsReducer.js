
import { methodsActionTypes } from '../constants/actionTypes';

const initialState = {};

export default (state = initialState, action) => {
    switch(action.type) {
    case methodsActionTypes.SET_METHODS:
        return {
            ...state,
            [action.owner]: action.methods
        };
    case methodsActionTypes.CLEAR_METHODS:
        return {
            ...state,
            [action.owner]: {}
        };
    default:
        return state;
    }
};
