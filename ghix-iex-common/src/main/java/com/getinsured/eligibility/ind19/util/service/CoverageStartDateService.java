package com.getinsured.eligibility.ind19.util.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;

import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * @author Sunil Desu
 *
 */
public interface CoverageStartDateService {
	
	/**
	 * HIX-70832
	 * IND 19 Improvement to Support Dental CR, SEPs in OE Overlap Period, and future dated LCE
	 */
	@Deprecated
	Timestamp computeCoverageStartDate(BigDecimal applicationId, Map<String,EnrollmentShopDTO> enrollments, boolean isChangePlanFlow, boolean isChangeSEPPlanFlow) throws GIException;
	
	Timestamp computeCoverageStartDateIncludingTermMembers(BigDecimal applicationId, boolean isChangePlanFlow, boolean isChangeSEPPlanFlow, Long priorApplicationId) throws GIException;
	
}
