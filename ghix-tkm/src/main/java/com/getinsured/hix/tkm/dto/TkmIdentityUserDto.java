package com.getinsured.hix.tkm.dto;

import java.util.HashSet;
import java.util.Set;

import org.activiti.engine.identity.Picture;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.persistence.entity.ByteArrayRef;
import org.activiti.engine.impl.persistence.entity.UserEntity;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.UserRole;


public class TkmIdentityUserDto implements UserEntity {
	
	private String id;
	private String email;
	private String  firstName;
	private String lastName;
	private String fullName;
	private String password;
	private Set<UserRole> userRole = new HashSet<UserRole>();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public TkmIdentityUserDto(AccountUser accountUser){
		this.email=accountUser.getEmail();
		this.firstName = accountUser.getFirstName();
		this.id = String.valueOf(accountUser.getId());
		this.lastName = accountUser.getLastName();
		this.password = accountUser.getPassword();
		this.userRole=accountUser.getUserRole();
		this.fullName=accountUser.getFullName();
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public void setId(String id) {
	this.id=id;	
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public void setFirstName(String firstName) {
		this.firstName=firstName;
		
	}

	@Override
	public void setLastName(String lastName) {
		this.lastName=lastName;
		
	}

	@Override
	public String getLastName() {
		return this.lastName;
	}

	@Override
	public void setEmail(String email) {
		this.email=email;
		
	}

	@Override
	public String getEmail() {
		return this.email;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public void setPassword(String password) {
		this.password=password;
		
	}

	public String getFullName() {
		return fullName;
	}

	public Set<UserRole> getUserRole() {
		return userRole;
	}

	@Override
	public boolean isPictureSet() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isInserted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setInserted(boolean inserted) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isUpdated() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setUpdated(boolean updated) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setDeleted(boolean deleted) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getPersistentState() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRevision(int revision) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getRevision() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getRevisionNext() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Picture getPicture() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPicture(Picture picture) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ByteArrayRef getPictureByteArrayRef() {
		// TODO Auto-generated method stub
		return null;
	}


}
