package com.getinsured.eligibility.verification.controller;

import javax.xml.ws.http.HTTPException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.verification.rulesets.CitizenshipVerificationRule;
import com.getinsured.eligibility.verification.rulesets.DeathVerificationRule;
import com.getinsured.eligibility.verification.rulesets.EquifaxVerificationRule;
import com.getinsured.eligibility.verification.rulesets.IncarcerationVerificationRule;
import com.getinsured.eligibility.verification.rulesets.IncomeVerificationRule;
import com.getinsured.eligibility.verification.rulesets.MECVerificationRule;
import com.getinsured.eligibility.verification.rulesets.ResidencyVerificationRule;
import com.getinsured.eligibility.verification.rulesets.SSNVerificationRule;
import com.getinsured.eligibility.verification.rulesets.VLPVerificationRule;
import com.getinsured.eligibility.verification.util.SSAPConstants;
import com.getinsured.eligibility.verification.util.SSAPUtils;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;


/**
 * Handles requests for the Verification Rules.
 */
@Controller
@RequestMapping(value = "/verification/rule")
public class VerificationRulesController {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationRulesController.class);

	@Autowired
	private ResidencyVerificationRule residencyVerificationRule;
	@Autowired
	private DeathVerificationRule deathVerificationRule;
	@Autowired
	private SSNVerificationRule ssnVerificationRule;
	@Autowired
	private IncarcerationVerificationRule incarcerationVerificationRule;
	@Autowired
	private MECVerificationRule mecVerificationRule;
	@Autowired
	private CitizenshipVerificationRule citizenshipVerificationRule;
	@Autowired
	private VLPVerificationRule vlpVerificationRule;
	@Autowired
	private IncomeVerificationRule incomeVerificationRule;
	@Autowired
	private EquifaxVerificationRule equifaxVerificationRule;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() throws HTTPException, Exception {

		return "Welcome to Eligibility module";
	}
	@RequestMapping(value = "/test/{rule}", method = RequestMethod.GET)
	@ResponseBody
	public String test(@PathVariable("rule") String rule) throws HTTPException, Exception {
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		String url = "";
		
		if (rule.equalsIgnoreCase("death")) {
			url = "http://localhost:8080/ghix-eligibility/verification/rule/death";
		} else if (rule.equalsIgnoreCase("ssn")) {
			url = "http://localhost:8080/ghix-eligibility/verification/rule/ssn";
		} else if (rule.equalsIgnoreCase("incarceration")) {
			url = "http://localhost:8080/ghix-eligibility/verification/rule/incarceration";
		} else if (rule.equalsIgnoreCase("residency")) {
			url = "http://localhost:8080/ghix-eligibility/verification/rule/residency";
		} else if(rule.equalsIgnoreCase("citizenship")){
			url = "http://localhost:8080/ghix-eligibility/verification/rule/citizenship";
		} else if(rule.equalsIgnoreCase("mec")){
			url = "http://localhost:8080/ghix-eligibility/verification/rule/mec";
		} else if(rule.equalsIgnoreCase("vlp")){
			url = "http://localhost:8080/ghix-eligibility/verification/rule/vlp";
		}
		
		if(rule.equalsIgnoreCase("mec")){
			map.add("ssapJson", SSAPUtils.loadSSAPJSON());
			map.add("esiJson", SSAPUtils.loadESIJSON());
			map.add("nonEsiJson", SSAPUtils.loadNonESIJSON());
		} else if(rule.equalsIgnoreCase("vlp")){
			map.add("ssapJson", SSAPUtils.loadSSAPJSON());
			map.add("hubResponse", SSAPUtils.loadHUBJSON());
		} else {
			map.add("ssapJson", SSAPUtils.loadSSAPJSON());
			map.add("hubResponse", SSAPUtils.loadHUBJSON());
		}

		

		restTemplate.postForObject(url, map, String.class);
		
		return "Testing Verification Rule";
	}

	/**
	 * This rule will verify Residency Status of Household Member
	 * 
	 * @param ssapJson
	 * @param hubResponse
	 * @return verification status string
	 */
	@RequestMapping(value = "/residency", method = RequestMethod.POST)
	@ResponseBody
	public String verifyResidencyRule(String ssapJson) throws HTTPException, Exception {

		LOGGER.info("Verify Residency Rule : Starts");

		String verificationStatus = null;
		try {
			if (!SSAPUtils.isEmpty(ssapJson)) {
				if(residencyVerificationRule.verifyResidency(ssapJson)){
					verificationStatus = SSAPConstants.SUCCESS;
				} else {
					verificationStatus = SSAPConstants.FAILED;
				}
			} else {
				verificationStatus = SSAPConstants.FAILED;
			}
		} catch (Exception exception) {
			verificationStatus = SSAPConstants.FAILED;
			LOGGER.error("Exception occurred while Verifying Residency : " + exception);
			String rule = SSAPConstants.RULES.RESIDENCY_VERIFICATION_RULE.toString();
			giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
					null, null, null, null, rule), rule));
		}

		LOGGER.info("Verify Residency Rule : Ends : Verification Status : " + verificationStatus);

		return verificationStatus;
	}

	/**
	 * This rule will verify death Status of Household Member
	 * 
	 * @param ssapJson
	 * @param hubResponse
	 * @return verification status string
	 */
	@RequestMapping(value = "/death", method = RequestMethod.POST)
	@ResponseBody
	public String verifyDeathRule(String ssapJson, String hubResponse, String personId) throws HTTPException, Exception {
		LOGGER.info("Verify Death Rule : Starts : Person ");

		String verificationStatus = null;
		try {
			if (!SSAPUtils.isEmpty(ssapJson) && !SSAPUtils.isEmpty(personId)) {
				if(deathVerificationRule.verifyDeath(ssapJson, hubResponse, personId)){
					verificationStatus = SSAPConstants.SUCCESS;
				} else {
					verificationStatus = SSAPConstants.FAILED;
				}
			} else {
				verificationStatus = SSAPConstants.FAILED;
			}
		} catch (Exception exception) {
			verificationStatus = SSAPConstants.FAILED;
			LOGGER.error("Exception occurred while Verifying Death : " + exception);
			String rule = SSAPConstants.RULES.DEATH_VERIFICATION_RULE.toString();
			giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
					hubResponse, null, null, null, rule), rule));
		}

		LOGGER.info("Verify Death Rule : Ends : Verification Status : " + verificationStatus);

		return verificationStatus;
	}

	/**
	 * This rule will verify Social Security Number of Household Member
	 * 
	 * @param ssapJson
	 * @param hubResponse
	 * @return verification status string
	 */
	@RequestMapping(value = "/ssn", method = RequestMethod.POST)
	@ResponseBody
	public String verifySSNRule(String ssapJson, String hubResponse, String personId) throws HTTPException, Exception {
		LOGGER.info("Verify SSN Rule : Starts");

		String verificationStatus = null;
		try {
			if (!SSAPUtils.isEmpty(ssapJson) && !SSAPUtils.isEmpty(personId)) {
				if(ssnVerificationRule.verifySSN(ssapJson, hubResponse, personId)){
					verificationStatus = SSAPConstants.SUCCESS;
				} else {
					verificationStatus = SSAPConstants.FAILED;
				}
			} else {
				verificationStatus = SSAPConstants.FAILED;
			}
		} catch (Exception exception) {
			verificationStatus = SSAPConstants.FAILED;
			LOGGER.error("Exception occurred while Verifying SSN : " + exception);
			String rule = SSAPConstants.RULES.SSN_VERIFICATION_RULE.toString();
			giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
					hubResponse, null, null, null, rule), rule));
		}

		LOGGER.info("Verify SSN Rule : Ends : Verification Status : " + verificationStatus);

		return verificationStatus;
	}

	/**
	 * This rule will verify Incarceration Status of Household Member
	 * 
	 * @param ssapJson
	 * @param hubResponse
	 * @return verification status string
	 */
	@RequestMapping(value = "/incarceration", method = RequestMethod.POST)
	@ResponseBody
	public String verifyIncarcerationRule(String ssapJson, String hubResponse, String personId) throws HTTPException, Exception {
		LOGGER.info("Verify Incarceration Rule : Starts");

		String verificationStatus = null;
		try {
			if (!SSAPUtils.isEmpty(ssapJson) && !SSAPUtils.isEmpty(personId)) {
				if(incarcerationVerificationRule.verifyIncarceration(ssapJson, hubResponse, personId)){
					verificationStatus = SSAPConstants.SUCCESS;
				} else {
					verificationStatus = SSAPConstants.FAILED;
				}
			} else {
				verificationStatus = SSAPConstants.FAILED;
			}
		} catch (Exception exception) {
			verificationStatus = SSAPConstants.FAILED;
			LOGGER.error("Exception occurred while Verifying Incarceration : " + exception);
			String rule = SSAPConstants.RULES.INCARCERATION_VERIFICATION_RULE.toString();
			giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
					hubResponse, null, null, null, rule), rule));
		}

		LOGGER.info("Verify Incarceration Rule : Ends : Verification Status : " + verificationStatus);

		return verificationStatus;
	}
	
	/**
	 * This rule will verify MEC Status of Household Member
	 * 
	 * @param ssapJson
	 * @param hubResponse
	 * @param type
	 * 		ESI/NON-ESI
	 * @return verification status string
	 */
	@RequestMapping(value = "/mec", method = RequestMethod.POST)
	@ResponseBody
	public String verifyMECRule(String ssapJson, String esiJson, String nonEsiJson, String personId) throws HTTPException, Exception {
		LOGGER.info("Verify MEC Rule : Starts");

		String verificationStatus = null;
		try {
			if (!SSAPUtils.isEmpty(ssapJson) && !SSAPUtils.isEmpty(personId)) {
				if(mecVerificationRule.verifyMEC(ssapJson, esiJson, nonEsiJson, personId)){
					verificationStatus = SSAPConstants.SUCCESS;
				} else {
					verificationStatus = SSAPConstants.FAILED;
				}
			} else {
				verificationStatus = SSAPConstants.FAILED;
			}
		} catch (Exception exception) {
			verificationStatus = SSAPConstants.FAILED;
			LOGGER.error("Exception occurred while Verifying MEC : " + exception);
			String rule = SSAPConstants.RULES.MEC_VERIFICATION_RULE.toString();
			giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
					null, null, esiJson, nonEsiJson, rule), rule));
		}

		LOGGER.info("Verify MEC Rule : Ends : MEC Status : " + verificationStatus);

		return verificationStatus;
	}
	
	/**
	 * This rule will verify Citizenship Status of Household Member
	 * 
	 * @param ssapJson
	 * @param hubResponse
	 * @return verification status string
	 */
	@RequestMapping(value = "/citizenship", method = RequestMethod.POST)
	@ResponseBody
	public String verifyCitizenshipRule(String ssapJson, String hubResponse, String personId) throws HTTPException, Exception {
		LOGGER.info("Verify Citizenship Rule : Starts");

		String verificationStatus = null;
		try {
			if (!SSAPUtils.isEmpty(ssapJson) && !SSAPUtils.isEmpty(personId)) {
				if(citizenshipVerificationRule.verifyCitizenship(ssapJson, hubResponse, personId)){
					verificationStatus = SSAPConstants.SUCCESS;
				} else {
					verificationStatus = SSAPConstants.FAILED;
				}
			} else {
				verificationStatus = SSAPConstants.FAILED;
			}
		} catch (Exception exception) {
			verificationStatus = SSAPConstants.FAILED;
			LOGGER.error("Exception occurred while Verifying Citizenship : " + exception);
			String rule = SSAPConstants.RULES.CITIZENSHIP_VERIFICATION_RULE.toString();
			giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
					hubResponse, null, null, null, rule), rule));
		}

		LOGGER.info("Verify Citizenship Rule : Ends : Citizenship Status : " + verificationStatus);

		return verificationStatus;
	}
	
	/**
	 * This rule will verify VLP Status of Household Member
	 * 
	 * @param ssapJson
	 * @param hubResponse
	 * @param alienNumber
	 * @return verification status string
	 */
	@RequestMapping(value = "/vlp", method = RequestMethod.POST)
	@ResponseBody
	public String verifyVLPRule(String ssapJson, String vlpJson, String ssapApplicationId) throws HTTPException, Exception {
		LOGGER.info("Verify VLP Rule : Starts");

		String verificationStatus = null;
		try {
			if (!SSAPUtils.isEmpty(ssapJson) && !SSAPUtils.isEmpty(ssapApplicationId)) {
				if(vlpVerificationRule.verifyVLP(ssapJson, vlpJson, ssapApplicationId)){
					verificationStatus = SSAPConstants.SUCCESS;
				} else {
					verificationStatus = SSAPConstants.FAILED;
				}
			} else {
				verificationStatus = SSAPConstants.FAILED;
			}
		} catch (Exception exception) {
			verificationStatus = SSAPConstants.FAILED;
			LOGGER.error("Exception occurred while Verifying VLP : " + exception);
			String rule = SSAPConstants.RULES.VLP_VERIFICATION_RULE.toString();
			giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
					null, vlpJson, null, null, rule), rule));
		}

		LOGGER.info("Verify VLP Rule : Ends : VLP Status : " + verificationStatus);

		return verificationStatus;
	}
	
	/**
	 * This rule will verify Income Status of Household Member
	 * 
	 * @param ssapJson
	 * @param hubResponse
	 * @return verification status string
	 */
	@RequestMapping(value = "/income", method = RequestMethod.POST)
	@ResponseBody
	public String verifyIncomeRule(String ssapJson, String hubResponse) throws HTTPException, Exception {
		LOGGER.info("Verify Income Rule : Starts");

		String verificationStatus = null;
		try {
			if (!SSAPUtils.isEmpty(ssapJson)) {
				verificationStatus = incomeVerificationRule.verifyIncome(ssapJson, hubResponse);
			} else {
				verificationStatus = SSAPConstants.FAILED;
			}
		} catch (Exception exception) {
			verificationStatus = SSAPConstants.FAILED;
			LOGGER.error("Exception occurred while Verifying Income : " + exception);
			String rule = SSAPConstants.RULES.INCOME_VERIFICATION_RULE.toString();
			giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
					hubResponse, null, null, null, rule), rule));
		}

		LOGGER.info("Verify Income Rule : Ends : Income Status : " + verificationStatus);

		return verificationStatus;
	}
	
	/**
	 * This rule will verify Equifax Status of Household 
	 * 
	 * @param ssapJson
	 * @param hubResponse
	 * @return verification status string
	 */
	@RequestMapping(value = "/equifax", method = RequestMethod.POST)
	@ResponseBody
	public String verifyEquifaxRule(String ssapJson, String hubResponse) throws HTTPException, Exception {
		LOGGER.info("Verify Equifax Rule : Starts");

		String verificationStatus = null;
		
		try {
			if (!SSAPUtils.isEmpty(ssapJson)) {
				if(equifaxVerificationRule.verifyEquifax(ssapJson, hubResponse)){
					verificationStatus = SSAPConstants.SUCCESS;
				} else {
					verificationStatus = SSAPConstants.FAILED;
				}
			} else {
				verificationStatus = SSAPConstants.FAILED;
			}
		} catch (Exception exception) {
			verificationStatus = SSAPConstants.FAILED;
			LOGGER.error("Exception occurred while Verifying Equifax : " + exception);
			String rule = SSAPConstants.RULES.EQUIFAX_VERIFICATION_RULE.toString();
			giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
					hubResponse, null, null, null, rule), rule));
		}

		LOGGER.info("Verify Equifax Rule : Ends : Equifax Status : " + verificationStatus);

		return verificationStatus;
	}
}
