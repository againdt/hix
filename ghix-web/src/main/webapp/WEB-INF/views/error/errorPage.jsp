<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isErrorPage="true"%>

<style>
	td {
		padding: 5px 5px 0px;
	} 
</style>


<h3 class="margin20-t">Application Error</h3>
<table width="100%" style="border: 1px solid red;">
	<tr>
		<td>Error:</td>
				<td><c:out value="${pageContext.exception}" /></td>
			</tr>
	<tr>
				<td>URI:</td>
				<td><c:out value="${pageContext.errorData.requestURI}" /></td>
			</tr>
	<tr>
				<td>Status code:</td>
				<td>${pageContext.errorData.statusCode}</td>
			</tr>
</table>

<p class="margin20-t">
	<strong>Please contact the support team.</strong>
</p>

<div class="margin50-t">
	<a href="/hix">Back to the application</a>
</div>
