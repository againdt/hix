package com.getinsured.hix.planmgmt.util;

import java.util.List;

public class ServiceAreaDetails {
	private String name;
	private String issuerPlanNumber;
	private String startDate;
	private String endDate;
	private String serfServiceAreaId;
	private List<ZipCounty> zipCounty;
	
	public List<ZipCounty> getZipCounty() {
		return zipCounty;
	}
	public void setZipCounty(List<ZipCounty> zipCounty) {
		this.zipCounty = zipCounty;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}
	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getSerfServiceAreaId() {
		return serfServiceAreaId;
	}
	public void setSerfServiceAreaId(String serfServiceAreaId) {
		this.serfServiceAreaId = serfServiceAreaId;
	}
	
}
