package com.getinsured.iex.hub.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * @author Yevgen Golubenko
 * @since 12/11/18
 */
@Configuration
@EnableSwagger2
@EnableWebMvc
public class SwaggerConfiguration
{
//  @Bean
//  public Docket api(){
//    return new Docket(DocumentationType.SWAGGER_2)
//        .select()
//        .apis(RequestHandlerSelectors.any())
//        .paths(PathSelectors.regex("/.*"))
//        .build()
//        .apiInfo(apiInfo());
//  }
//
//  private ApiInfo apiInfo() {
//    return new ApiInfoBuilder()
//        .title("GHIX Hub Services")
//        .description("Hub Service API")
//        .version("2.0")
//        .termsOfServiceUrl("https://company.getinsured.com/privacy-policy/")
//        .license("LICENSE")
//        .licenseUrl("https://www.getinsured.com")
//        .build();
//  }
}
