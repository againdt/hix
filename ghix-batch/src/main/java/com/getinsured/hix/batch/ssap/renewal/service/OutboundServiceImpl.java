package com.getinsured.hix.batch.ssap.renewal.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.timeshift.util.TSDate;

/**
 * Class is used to provide services for Outbound Job.
 * 
 * @since September 12, 2019
 */
@Service("outboundService")
public class OutboundServiceImpl implements OutboundService {

	private static final Logger LOGGER = LoggerFactory.getLogger(OutboundServiceImpl.class);
	private static final String FAILURE = "failure";
	private static final String SUCCESS = "success";

	@Autowired
	private SsapCloneApplicationService ssapCloneApplicationService;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private GIMonitorService giMonitorService;
	@Autowired
	IntegrationLogService integrationLogService;

	/**
	 * Method is use to save and throws Outbound Job Error.
	 */
	@Override
	public synchronized void saveAndThrowsErrorLog(String errorMessage) throws UnexpectedJobExecutionException {
		giMonitorService.saveOrUpdateErrorLog(ERROR_CODE, new TSDate(), this.getClass().getName(),
				errorMessage, null, errorMessage, GIRuntimeException.Component.BATCH.getComponent(), null);
		throw new UnexpectedJobExecutionException(errorMessage);
	}

	/**
	 * Method is use to save and throws Outbound Job Error.
	 */
	@Override
	public synchronized Integer logToGIMonitor(Exception e, String caseNumber) {

		Integer giMonitorId = null;
		GIMonitor giMonitor = giMonitorService.saveOrUpdateErrorLog(ERROR_CODE, new Date(), this.getClass().getName(),
				e.getLocalizedMessage() + "\n" + e.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(e), null,
				caseNumber, GIRuntimeException.Component.BATCH.getComponent(), null);

		if (giMonitor != null) {
			giMonitorId = giMonitor.getId();
		}
		return giMonitorId;
	}

	/**
	 * Method is used to get SSAP Application ID by Coverage year, Application Status, Eligibility Status and Batch size.
	 */
	@Override
	public synchronized List<Long> getSsapApplIDsByCoverageYearAndApplStatusAndEligStatus(long coverageYear,
			List<String> applicationStatusList, Long batchSize) {
		return ssapCloneApplicationService.getSsapApplIDsByCoverageYearAndApplStatusAndEligStatus(coverageYear, applicationStatusList, batchSize);
	}

	/**
	 * Invoke Outbound AT for SSAP application.
	 */
	@Override
	public synchronized String outboundSsap(Long applicationId) {

		String restResponse = FAILURE.toUpperCase();
		String status = FAILURE;
		LOGGER.debug("Invoking the SSAP Outbound AT");

		try {
			restResponse = ghixRestTemplate.getForObject(GhixEndPoints.EligibilityEndPoints.GENERATE_PUSH_AT + "?applicationId=" + applicationId, String.class);

			if (null != restResponse && restResponse.toLowerCase().contains(SUCCESS)) {
				status = SUCCESS;
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception in invoke SSAP Outbound AT", e);
			logToGIMonitor(e, Long.toString(applicationId));
		}
		finally {
			integrationLogService.save(restResponse, JOB_NAME, status.toUpperCase(), applicationId, null);
		}
		LOGGER.info("Outbound AT - Response: " + restResponse);
		return restResponse;
	}
}
