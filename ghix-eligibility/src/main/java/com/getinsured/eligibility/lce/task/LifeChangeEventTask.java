package com.getinsured.eligibility.lce.task;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.ssap.integration.household.service.NonFinancialApplicationEligibilityService;
import com.getinsured.eligibility.ssap.validation.service.QleValidationService;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.SsapEndPoints;
import com.getinsured.hix.platform.util.GhixRestServiceInvoker;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.dto.LifeChangeEventDTO;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.dto.SsapSEPIntegrationDTO;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.lce.ChangedApplicant;
import com.getinsured.iex.lce.RequestParamDTO;
import com.getinsured.iex.lce.SepTransientDTO;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.LifeChangeEventUtil;

@Component
public class LifeChangeEventTask {
	private static final String IS_MAILING_ADDRESS_CHANGE_EVENT = "isMailingAddressChangeEvent";

	private static final String TRIGGER_IND57 = "triggerIND57";

	private static final String OLD_SSAP_JSON = "oldSsapJson";

	private static final Logger LOGGER = LoggerFactory.getLogger(LifeChangeEventTask.class);
	
	private static final int NO_OF_DAYS_TO_DENY_SEP = 70;
	
	@Autowired
	private GhixRestServiceInvoker ghixRestServiceInvoker;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private UserService userService;
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private SsapApplicantEventRepository ssapApplicantEventRepository;
	@Autowired
	SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	@Autowired
	private NoticeService noticeService;
	@Autowired
	CmrHouseholdRepository cmrHouseholdRepository;
	@Autowired
	NonFinancialApplicationEligibilityService nonFinancialApplicationEligibilityService;
	
	@Autowired
	private PreferencesService preferencesService;
	@Autowired
	private QleValidationService qleValidationService;
	
	
	
	private static final String DO_MOVE_ENROLLMENT = "doMoveEnrollment";
	private static final String IS_DISENROLL_CASE = "isDisenrollCase";
	private static final String IS_NO_CHANGE__CASE = "isNoChangeCase";
	
	private static final String EXCHANGE_FULL_NAME = "exchangeFullName";	
	private static final String STATE_NAME = "exgStateName";
	private static final String COUNTRY_NAME = "countryName";
	private static final String EXCHANGE_URL = "exchangeURL";
	private static final String  EXCHANGE_PRIVACY_URL = "exchangePrivacyURL";
	private static final String EXCHANGE_PHONE = "exchangePhone";
	private static final String EXCHANGE_NAME = "exgName";
	private static final String EXCHANGE_ADDRESS_ONE = "exchangeAddress1";
	private static final String EXCHANGE_ADDRESS_TWO = "exchangeAddress2";
	private static final String EXCHANGE_CITY_NAME = "exgCityName";
	private static final String EXCHANGE_ZIP = "zip";
	private static final String PRIMARY_APPLICANT = "primaryContact";
	private static final String CASE_NUMER = "caseNumber";
	private static final String APPLICANTS = "applicants";
	private static final String EXCHANGE_ADDRESS_EMAIL = "exchangeAddressEmail";
	private static final String USER_NAME = "userName";
	private static final String SPECIAL_ENROLLMENT_END_DATE = "SpecialEnrollmentEndDate";
	private static final String DATE_FORMAT_FOR_NOTICE ="MMM dd, yyyy";
	private static final String EMPTY_STRING="";
	private static final String ES = "ES";
	private static final String LOCAL_ES = "es";
	private static final String DATE_FORMAT = "MMMM dd, YYYY";
	private static final String SPANISH_DATE = "spanishDate";
	private static final String ENGLISH_DATE = "englishDate";
	private static final int SEP_DENIAL_NO_OF_DAYS_TO_APPEAL = 90;
	private static final String NUMBER_OF_DAYS = "Numberofdays";
	
	//@Async
	public String process(SepTransientDTO sepTransientDTO, List<RequestParamDTO> events, String userName, Map<String, Boolean> demographicFlagMap, Map<Long, List<String>> demographicEventsData, List<Boolean> homeAddressChangeDemoFlags) throws Exception {
		LOGGER.info("Life Change Event Task  : Starts");
		
		boolean doMoveEnrollment = false;
		boolean isDisenrollCase = false;
		SsapSEPIntegrationDTO ssapSEPIntegrationDTO = new SsapSEPIntegrationDTO();
		
		Map<Long, List<RequestParamDTO>> applicantsWithEvent = getChangedApplicantsWithEvents(events);
		
		try {
			//Ssap Integration call to run Hub Verifications and check Eligibility
			ssapSEPIntegrationDTO.setApplicationId(sepTransientDTO.getCurrentApplication().getId());
			ssapSEPIntegrationDTO.setIsAddressChangeDemoEvent(demographicFlagMap.get(LifeChangeEventConstant.IS_ADDRESS_DEMOGRAPHIC_CHANGE));

			List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplication(sepTransientDTO.getCurrentApplication());
			Set<Long> enrolledPersons = sepTransientDTO.getEnrolledPersons();
			Map<String,Boolean> result = isMoveEnrollmentCase(ssapSEPIntegrationDTO, ssapApplicants, enrolledPersons, applicantsWithEvent);
			doMoveEnrollment = result.get(DO_MOVE_ENROLLMENT);  
			isDisenrollCase = result.get(IS_DISENROLL_CASE);
			Boolean isSepDenialCase =false;
			if(!doMoveEnrollment && !isDisenrollCase){
				List<SsapApplicationEvent> applicationEvents = ssapApplicationEventRepository.findBySsapApplication(sepTransientDTO.getCurrentApplication());
				List<SsapApplicantEvent> ssapApplicantEvents = ssapApplicantEventRepository.findByApplicationEventId(applicationEvents.get(0).getId());
				isSepDenialCase = processSepDenial(enrolledPersons,ssapApplicantEvents,ssapSEPIntegrationDTO);
			}
			ssapSEPIntegrationDTO.setIsDisenrollCase(isDisenrollCase);
			ssapSEPIntegrationDTO.setDoMoveEnrollment(doMoveEnrollment);
			ssapSEPIntegrationDTO.setIsSepDenialCase(isSepDenialCase);
			ssapSEPIntegrationDTO.setIsNoChangeCase(result.get(IS_NO_CHANGE__CASE));
			//if sep denial case call eligibily api
			if(isSepDenialCase){
				//get result from Eligibility it is not disenroll case process
				// Rest api will be called here to get result and set it to
				//isDisenrollScenario, for now it hardcoded till Api is not there.
				boolean isEnrollmentAllowed = nonFinancialApplicationEligibilityService.checkIfEligibilityIsToBeDenied(sepTransientDTO.getCurrentApplication().getCaseNumber());
				if(isEnrollmentAllowed){
					//trigger qle service to update to application 
					qleValidationService.processQleForSepDenial(sepTransientDTO.getCurrentApplication().getCaseNumber(), sepTransientDTO.getUserId().intValue());
					//close the application
					updateApplicationSEPDenied(sepTransientDTO.getCurrentApplication().getId(), ApplicationStatus.CLOSED);
					//send notification
					AccountUser accountUser =  userService.findById(sepTransientDTO.getUserId().intValue());
					Household household = cmrHouseholdRepository.findOne(sepTransientDTO.getCmrHouseholdId().intValue()); 
					//get user
					
					sendNotification(null,accountUser, LifeChangeEventConstant.LCE_SEP_DENIAL_NOTIFICATION_FILE_NAME, 
							LifeChangeEventConstant.LCE_SEP_DENIAL_NOTIFICATION_TEMPLATE_NAME, LifeChangeEventConstant.LCE_SEP_DENIAL_ECM_RELATIVE_PATH, null,ssapApplicants, household);
					//trigger integration API
					triggerSSAPIntegration(ssapSEPIntegrationDTO);
					//return the response
					return LifeChangeEventConstant.SEP_DENIED;
				}
			}
			
			if(demographicFlagMap.get(LifeChangeEventConstant.IS_DEMOGRAPHIC_CHANGE) || demographicFlagMap.get(LifeChangeEventConstant.IS_ADDRESS_DEMOGRAPHIC_CHANGE)){
				LOGGER.info("Demographic Changes Processing  : Starts");
				
				SsapApplication parentApplication = sepTransientDTO.getParentApplication();
				LifeChangeEventDTO lifeChangeEventDTO = new LifeChangeEventDTO();
				lifeChangeEventDTO.setOldApplicationId(parentApplication.getId());
				lifeChangeEventDTO.setUserName(userName);
				
				LOGGER.info("Fetching Enrollment Details : Ends"+lifeChangeEventDTO);
					
				lifeChangeEventDTO.setPersonIds(enrolledPersons);
				
				if(enrolledPersons!=null && enrolledPersons.size() > 0){
					String oldSsapJson = parentApplication.getApplicationData();
					//Update previous application json for enrolled members if there is change in Demo events
					LOGGER.info("Updating Enrolled Members Info in Json and DB : Starts");
					Map<String,Object> updateResult = updateSsapJsonForEnrolledApplicants(sepTransientDTO.getCurrentApplication().getApplicationData(), enrolledPersons, oldSsapJson, demographicEventsData);
					boolean triggerIND57 = (boolean)updateResult.get(TRIGGER_IND57);
					LOGGER.info("Updating Enrolled Members Info in Json and DB : Ends");
					
					if(triggerIND57) {
						//Invoking IND57
						oldSsapJson = (String)updateResult.get(OLD_SSAP_JSON);
						parentApplication.setApplicationData(oldSsapJson);
						ssapApplicationRepository.saveAndFlush(parentApplication);
						LOGGER.info("IND57 Call for Case Number : Starts"+parentApplication.getCaseNumber());
						lifeChangeEventDTO.setCaseNumber(parentApplication.getCaseNumber());
						lifeChangeEventDTO.setUserName(userName);
						boolean isMailingAddressChangeEvent = (boolean) updateResult.get(IS_MAILING_ADDRESS_CHANGE_EVENT);
						if(isMailingAddressChangeEvent){
							lifeChangeEventDTO.setMailingAddressChanged(isMailingAddressChangeEvent);
							lifeChangeEventDTO.setUpdatedMailingAddressLocationId(getLocationFromApplicants(ssapApplicants));
						}else{
							lifeChangeEventDTO.setMailingAddressChanged(isMailingAddressChangeEvent);	
						}
						invokeRestService(SsapEndPoints.LCE_INVOKE_IND57, lifeChangeEventDTO);
						LOGGER.info("IND57 Call : Ends");
					}
				}
				
				LOGGER.info("Demographic Changes Processing  : Ends");
			}
			
			triggerSSAPIntegration(ssapSEPIntegrationDTO);
			return GhixConstants.RESPONSE_SUCCESS;
		} catch(Exception exception){
			exception.printStackTrace();		
			LOGGER.info("Life Change Event Task  : Ends");
			return GhixConstants.RESPONSE_FAILURE;
		}
	}
		
	private Map<String,Object> updateSsapJsonForEnrolledApplicants(String newJson, Set<Long> oldEnrolledApplicants, String oldSsapJson, Map<Long, List<String>> demographicEventsData) throws Exception{
		Map<String,Object> result= new HashMap<String,Object>();
		Set<Long> newPersonIds = demographicEventsData.keySet();
		boolean triggerIND57 = false;
		boolean isMailingAddressChangeEvent = false;
		for (Long enrolledPersonId:oldEnrolledApplicants) {
			if(newPersonIds.contains(enrolledPersonId)){
				triggerIND57 = true;
				List<String> events = demographicEventsData.get(enrolledPersonId);
				for(String event:events){
					if(LifeChangeEventConstant.DEMOGRAPHICS_MAILING_ADDRESS_CHANGE.equals(event)){
						//oldSsapJson = LifeChangeEventUtil.updateJsonForDemographicEvents(event, String.valueOf(LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID), oldSsapJson, newJson);
						isMailingAddressChangeEvent=true;
					}
					oldSsapJson = LifeChangeEventUtil.updateJsonForDemographicEvents(event, enrolledPersonId.toString(), oldSsapJson, newJson);
					
				}
			}
		}
		result.put(TRIGGER_IND57, triggerIND57);
		result.put(OLD_SSAP_JSON, oldSsapJson);
		result.put(IS_MAILING_ADDRESS_CHANGE_EVENT, isMailingAddressChangeEvent);
		return result;
	}
	
	private String invokeRestService(String url, LifeChangeEventDTO lifeChangeEventDTO){
		try {
			return ghixRestServiceInvoker.invokeRestService(lifeChangeEventDTO, url, HttpMethod.POST, GIRuntimeException.Component.LCE.toString());
		} catch(Exception exception){
			throw exception;
		}
	}
	
	private void triggerSSAPIntegration(SsapSEPIntegrationDTO ssapSEPIntegrationDTO) {
		try {
			LOGGER.info("Invoking the SSAP orchestration flow : starts");
			restTemplate.postForObject(GhixEndPoints.SsapIntegrationEndpoints.LCE_INTEGRATION_URL,	ssapSEPIntegrationDTO, String.class);
			LOGGER.info("Invoking the SSAP orchestration flow : ends");
		} catch (Exception exception) {
			LOGGER.error("Error invoking the SSAP orchestration flow", exception);
			giWSPayloadRepository.saveAndFlush(LifeChangeEventUtil.createGIWSPayload(exception, "SSAP Application ID : "+String.valueOf(ssapSEPIntegrationDTO), LifeChangeEventConstant.ENDPOINT_OPERATION_INVOKE_SSAP_INTEGRATION));
		}
	}
	
	private Map<String,Boolean> isMoveEnrollmentCase(SsapSEPIntegrationDTO ssapSEPIntegrationDTO, List<SsapApplicant> ssapApplicants, Set<Long> enrolledPersons, Map<Long, List<RequestParamDTO>> applicantsWithEvent) {
		boolean doMoveEnrollment = true;
		boolean isDisenrollCase = true;
		Map<String,Boolean> result= new HashMap<String,Boolean>();
		List<Boolean> moveApplicantFlags = new ArrayList<Boolean>();
		boolean isNoChangeCase = true;
		
		for (SsapApplicant ssapApplicant : ssapApplicants) {
	        if(ssapApplicant.getApplyingForCoverage().equalsIgnoreCase(LifeChangeEventConstant.YES)) {
	        	isDisenrollCase = false;
	        }
	        
	        if(ssapApplicant.getStatus().equals(LifeChangeEventConstant.Status.LCE_NO_CHANGE.toString())) {
	        	moveApplicantFlags.add(true);	
	        } else if(!onEnrolledApplication(enrolledPersons, ssapApplicant) && ssapApplicant.getApplyingForCoverage().equalsIgnoreCase(LifeChangeEventConstant.NO) && !ssapApplicant.getStatus().equals(LifeChangeEventConstant.Status.LCE_NO_CHANGE.toString())) {
	        	moveApplicantFlags.add(true);	
	        } else {
	        	boolean isDemoEvent = true;
	        	isNoChangeCase = false;
	        	List<RequestParamDTO> applicantEvents = applicantsWithEvent.get(ssapApplicant.getPersonId());
	        	for (RequestParamDTO sepEvents : applicantEvents) {
	        		if(!(LifeChangeEventUtil.getDemographicEvents().contains(sepEvents.getEventSubCategory())) && !((sepEvents.getEventSubCategory().equals(LifeChangeEventConstant.ADDRESS_CHANGE_WITHIN_STATE) && ssapSEPIntegrationDTO.getIsAddressChangeDemoEvent()))) {
	        			isDemoEvent = false;
	        			break;
	        		}
				}
	        	moveApplicantFlags.add(isDemoEvent);	
	        }
	    }
		
		for (Boolean moveApplicantFlag : moveApplicantFlags) {
			if(!moveApplicantFlag) {
				doMoveEnrollment = false;
				break;
			}
		}
  
		result.put(DO_MOVE_ENROLLMENT, doMoveEnrollment);
		result.put(IS_DISENROLL_CASE, isDisenrollCase);
		result.put(IS_NO_CHANGE__CASE, isNoChangeCase);
		
		return result;
	}
	
	private boolean onEnrolledApplication(Set<Long> enrolledPersons, SsapApplicant ssapApplicant) {
		if(enrolledPersons!= null && enrolledPersons.contains(ssapApplicant.getPersonId())){
			return true;
		}
		return false;
	}
	
	public boolean processSepDenial(Set<Long> enrolledPersons,List<SsapApplicantEvent> ssapApplicantEvents,SsapSEPIntegrationDTO ssapSEPIntegrationDTO) {
		String eventDaysInPast = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_SEP_DENIAL_DAYS);
		boolean isSepDenial = false;
		int noOfDaysInPastToDenySEP = NO_OF_DAYS_TO_DENY_SEP;
		if(eventDaysInPast != null && NumberUtils.isNumber(eventDaysInPast)){
			noOfDaysInPastToDenySEP = Integer.parseInt(eventDaysInPast);
		}
		for(SsapApplicantEvent ssapApplicantEvent : ssapApplicantEvents ){
			//ignore the dates for Demo Events 
			if(LifeChangeEventUtil.getDemographicEvents().contains(ssapApplicantEvent.getSepEvents().getName()) || (ssapApplicantEvent.getSepEvents().getName().equals(LifeChangeEventConstant.ADDRESS_CHANGE_WITHIN_STATE) && ssapSEPIntegrationDTO.getIsAddressChangeDemoEvent())
					|| ssapApplicantEvent.getSepEvents().getChangeType().equals(LifeChangeEventConstant.CHANGE_TYPE_REMOVE) || ssapApplicantEvent.getSepEvents().getName().equals(LifeChangeEventConstant.TRIBE_STATUS_GAIN_IN_TRIBE_STATUS	)){
				continue;
			}
			//get SsapApplicant for this event
			SsapApplicant applicantForTheEvents = ssapApplicantEvent.getSsapApplicant();
			//NO CHANGE are not involved in SEP Denial 
			if(ssapApplicantEvent.getEventDate() != null && (!applicantForTheEvents.getStatus().equals(LifeChangeEventConstant.Status.LCE_NO_CHANGE.toString())) && (enrolledPersons.contains(applicantForTheEvents.getPersonId()) ||((!enrolledPersons.contains(applicantForTheEvents.getPersonId())) && applicantForTheEvents.getApplyingForCoverage().equals(LifeChangeEventConstant.YES) ) )){
				Calendar cal = TSCalendar.getInstance();
				cal.setTime(ssapApplicantEvent.getEventDate());
				LifeChangeEventUtil.setMaxTimeForDay(cal);
				cal.add(Calendar.DATE, (noOfDaysInPastToDenySEP-1));
				Calendar todaysDate = TSCalendar.getInstance();
				if(todaysDate.getTimeInMillis() > cal.getTimeInMillis() ){
					isSepDenial = true;
					break;
				}
			}
		}
		
		return isSepDenial;
	}
	
	public Map<Long, List<RequestParamDTO>> getChangedApplicantsWithEvents(List<RequestParamDTO> events)
			throws Exception {
		Map<Long, List<RequestParamDTO>> setOfModifiedApplicants = new  HashMap<>();
		List<RequestParamDTO> personEvents = null;
		
		for(RequestParamDTO requestParamDTO:events){
			List<ChangedApplicant> changedApplicants = requestParamDTO.getChangedApplicants();
			if(changedApplicants != null){
				for (ChangedApplicant changedApplicant : changedApplicants) {
					if(setOfModifiedApplicants.get(changedApplicant.getPersonId())!=null){
						personEvents = setOfModifiedApplicants.get(changedApplicant.getPersonId());
						personEvents.add(requestParamDTO);
					} else {
						personEvents = new ArrayList<>();
						personEvents.add(requestParamDTO);
					}
					setOfModifiedApplicants.put(changedApplicant.getPersonId(), personEvents);
				}
			}
		}
		
		return setOfModifiedApplicants;
	}

	@Async
	public void sendNotification(List<SsapApplicantResource> applicants,AccountUser user, String fileName, String templateName, String relativePath, Date sepEndDate,List<SsapApplicant> ssapApplicants, Household household) throws Exception {
		LOGGER.info("Sending Notification : Starts");
		
		Map<String, Object> individualTemplateData = new HashMap<String, Object>();
		List<String> sendToEmailList = new LinkedList<String>();;
		DateFormat dateFormat = null;
		DateFormat dateFormatForSpanish = null;
		try {
			individualTemplateData.put(LifeChangeEventConstant.NOTIFICATION_SEND_DATE, new TSDate());
			individualTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);
			individualTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
			individualTemplateData.put(STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
			individualTemplateData.put(COUNTRY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
			individualTemplateData.put(EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));	
			individualTemplateData.put(EXCHANGE_PRIVACY_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL));	
			individualTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			individualTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			
			individualTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
			individualTemplateData.put(EXCHANGE_ADDRESS_TWO , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_2));
			individualTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
			individualTemplateData.put(EXCHANGE_ZIP , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
			//set data for SEP Denial Notification
			individualTemplateData.put(NUMBER_OF_DAYS,SEP_DENIAL_NO_OF_DAYS_TO_APPEAL);
			
			//initialize the date formatter
			dateFormat = new SimpleDateFormat(DATE_FORMAT);
			dateFormatForSpanish = new SimpleDateFormat(DATE_FORMAT,new Locale(LOCAL_ES, ES));
			//set English and Spanish date into template data
			Date todaysDate = new TSDate(); 
			individualTemplateData.put(ENGLISH_DATE ,dateFormat.format(todaysDate));			
			individualTemplateData.put(SPANISH_DATE ,dateFormatForSpanish.format(todaysDate));			
			
			//get name of primary applicant
			String name = household.getFirstName() + " " + household.getLastName();//LifeChangeEventUtil.getNameOfPrimaryApplicantFromSsapApplicant(ssapApplicants);
			individualTemplateData.put(PRIMARY_APPLICANT , name);
			individualTemplateData.put(USER_NAME , name);
			individualTemplateData.put(EXCHANGE_ADDRESS_EMAIL , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
			individualTemplateData.put(APPLICANTS , applicants);
			if(sepEndDate!=null){
				DateFormat df = new SimpleDateFormat(DATE_FORMAT_FOR_NOTICE);
				individualTemplateData.put(SPECIAL_ENROLLMENT_END_DATE, df.format(sepEndDate));
			}else{
				individualTemplateData.put(SPECIAL_ENROLLMENT_END_DATE, EMPTY_STRING);
			}
			
			individualTemplateData.put("ssapApplicationId", ssapApplicants.get(0).getSsapApplication().getId());
			// put the case number for the application 
			individualTemplateData.put(CASE_NUMER, ssapApplicants.get(0).getSsapApplication().getCaseNumber());
			
			PreferencesDTO preferencesDTO = preferencesService.getPreferences(household.getId(), false);
			
			GhixNoticeCommunicationMethod commPref= preferencesDTO.getPrefCommunication();
		
			sendToEmailList.add(preferencesDTO.getEmailAddress());
			
			
			fileName = fileName + TimeShifterUtil.currentTimeMillis() + LifeChangeEventConstant.PDF;
			String exchangeName = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
			noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, individualTemplateData, relativePath,
					fileName, LifeChangeEventConstant.INDIVIDUAL, household.getId(), sendToEmailList,
					exchangeName, name, getLocation(preferencesDTO), commPref);	
			
			
		} catch(Exception exception){
			LOGGER.error("Error sending notification for " + templateName);
		}
		
		LOGGER.info("Sending Notification : Ends");
	}
	
	private Location getLocation(PreferencesDTO preferencesDTO) {
		
		if(preferencesDTO.getLocationDto() == null){
			return null;
		}
		
		Location returnLocation=new Location();
		
		returnLocation.setAddress1(preferencesDTO.getLocationDto().getAddressLine1());
		returnLocation.setAddress2(preferencesDTO.getLocationDto().getAddressLine2());
		returnLocation.setCity(preferencesDTO.getLocationDto().getCity());
		returnLocation.setCounty(preferencesDTO.getLocationDto().getCountyName());
		returnLocation.setCountycode(preferencesDTO.getLocationDto().getCountyCode());
		returnLocation.setState(preferencesDTO.getLocationDto().getState());
		returnLocation.setZip(preferencesDTO.getLocationDto().getZipcode());

		return returnLocation;
	}
	
	private void updateApplicationSEPDenied(long applicationId,ApplicationStatus applicationStatus){
		SsapApplication ssapApplication = ssapApplicationRepository.findAndLoadApplicantsByAppId(applicationId);
		ssapApplication.setApplicationStatus(applicationStatus.getApplicationStatusCode());
		ssapApplication.setAllowEnrollment(LifeChangeEventConstant.YES);
		ssapApplication.setSepQepDenied(LifeChangeEventConstant.Y);
		ssapApplicationRepository.saveAndFlush(ssapApplication);
	}
	
	private Long getLocationFromApplicants(List<SsapApplicant> applicants) {
		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				if (ssapApplicant.getMailiingLocationId() != null){
					return ssapApplicant.getMailiingLocationId().longValue();
				} 
			}
		}

		return null;
	}
	
}
