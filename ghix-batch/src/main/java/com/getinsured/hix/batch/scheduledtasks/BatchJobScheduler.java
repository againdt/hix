package com.getinsured.hix.batch.scheduledtasks;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.platform.batch.BatchSchedulingLauncher;

public class BatchJobScheduler  extends QuartzJobBean {
	private static final Logger LOGGER = Logger.getLogger(BatchJobScheduler.class);
	private HashMap<String,String> jobParameterMap;
	private String jobName;
	private String jobToLaunch;
	
	public String getJobToLaunch() {
		return jobToLaunch;
	}
	public void setJobToLaunch(String jobToLaunch) {
		this.jobToLaunch = jobToLaunch;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public BatchJobScheduler(){
		jobParameterMap = new HashMap<String, String>(); 
	}
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Job " +  jobName + " launched using scheduler at : " +  dateFormat.format(date));
		}
		BatchSchedulingLauncher jobLauncher = (BatchSchedulingLauncher) GHIXApplicationContext.getBean(jobToLaunch);
		if(jobLauncher != null){
			if(jobLauncher.getJob() == null) {
				throw new JobExecutionException("Ignoring, No Job set with this launcher, perhaps duplicate Job loading") {
					private static final long serialVersionUID = 1L;
					public Throwable fillInStackTrace(){
						return null;
					}
				};
			}
			jobLauncher.setJobParameterMap(jobParameterMap);
			if(jobLauncher != null){
				try {
					jobLauncher.launch();
				} catch (org.springframework.batch.core.JobExecutionException e) {
					throw new JobExecutionException(e);
				}
			}
		}
	}
	public void setJobParameterMap(HashMap<String, String> jobParameterMap) {
		this.jobParameterMap = jobParameterMap;
	}
	
}
