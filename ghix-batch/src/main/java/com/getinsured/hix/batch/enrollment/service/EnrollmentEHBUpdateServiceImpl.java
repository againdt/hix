package com.getinsured.hix.batch.enrollment.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitResponse;
import com.getinsured.hix.enrollment.repository.IEnrolleeAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentEventAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentLocationAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LocationAud;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.model.enrollment.EnrolleeAud;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentAud;
import com.getinsured.hix.planmgmt.controller.PlanRateBenefitRestController;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;


@Service("enrollmentEHBUpdateService")
public class EnrollmentEHBUpdateServiceImpl implements EnrollmentEHBUpdateService {
	@Autowired private IEnrollmentAudRepository enrollmentAudRepository;
	@Autowired private IEnrollmentEventAudRepository enrollmentEventAudRepository;
	@Autowired private UserService userService;
	@Autowired private IEnrolleeAudRepository enrolleeAudRepository;
	@Autowired private  PlanRateBenefitRestController planRateBenefitRestController;
	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private IEnrollmentLocationAudRepository locationAudRepository;
	@Autowired private BatchJobExecutionService batchJobExecutionService;
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEHBUpdateServiceImpl.class);
	private static Integer carrierId; 
	
	@Override
	public void updateEnrollmentEHBAmount(Integer enrollmentId) throws GIException{
		/*AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);*/
		try{
			List<EnrollmentAud> enrollmentAuds= enrollmentAudRepository.getEnrollmentAudbyId(enrollmentId);
			Float lastEHBAmount=0.0f;
			String oldStatus="";
			String REQUIRED_DATE_FORMAT = "MM/dd/yyyy";
			for(EnrollmentAud aud: enrollmentAuds){
				
				if(aud.getUpdatedBy()==null||aud.getUpdatedBy().compareTo(getCarrierId())!=0) {
					//prepare request to PlanMgmt
					String newStatus=aud.getEnrollmentStatusLkp().getLookupValueCode();
					if(aud.getRevtype()==0 || (  !isEnrollmentTermEvent(oldStatus,newStatus ) && isSpecial(aud.getRev(), aud.getId()) )){
						PlanRateBenefitRequest planRateBenefitRequest= new PlanRateBenefitRequest();
						Map<String, Object> cRequestParams= new HashMap<>();
						List<Map<String, String>> censusData =new ArrayList<Map<String, String>>();
						List<EnrolleeAud> enrolleeAudList= new ArrayList<>();
						if(EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(newStatus) || EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(newStatus) ){
							enrolleeAudList= enrolleeAudRepository.getEnrolleeAudByRevRetro(aud.getId(), aud.getRev());
							if(EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(newStatus) ){
							enrolleeAudList=filterOutRetroTermEnrollees(enrolleeAudList, aud);
							}
						}else{
							enrolleeAudList = enrolleeAudRepository.getEnrolleeAudByRev(aud.getId(), aud.getRev());
						}
								Location loc= getSubscHomeAddress(enrolleeAudList, aud.getUpdatedOn());
								for (EnrolleeAud eAud: enrolleeAudList){
									
									Map<String, String> memberData= new HashMap<>();
									if(eAud.getBirthDate()!=null){
										memberData.put("dob", DateUtil.dateToString(eAud.getBirthDate(), REQUIRED_DATE_FORMAT));
									}
									if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
											EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_AGE_BASED_REQUOTING))) {//HIX-105931
										if(eAud.getAge() != null) {
											memberData.put("age", ""+eAud.getAge());
										}
									}
									if(eAud.getHomeAddressid()!=null){
										if(loc!=null){
											memberData.put("zip", loc.getZip());
											memberData.put("countycode", loc.getCountycode());
										}
										
									}
									if(eAud.getTobaccoUsageLkp()!=null && eAud.getTobaccoUsageLkp().getLookupValueCode().equalsIgnoreCase("T")){
										memberData.put("tobacco", "y");
									}else{
										memberData.put("tobacco", "n");
									}
									
									if(eAud.getRelationshipToHCPLkp()!=null){
										memberData.put("relation", eAud.getRelationshipToHCPLkp().getLookupValueCode().trim());
									}
									memberData.put("id", eAud.getExchgIndivIdentifier());
									censusData.add(memberData);
								}
								cRequestParams.put("censusData", censusData);
								cRequestParams.put("coverageStartDate", DateUtil.dateToString(aud.getGrossPremEffDate(), REQUIRED_DATE_FORMAT));
								planRateBenefitRequest.setRequestParameters(cRequestParams);
								PlanRateBenefitResponse response= planRateBenefitRestController.getBenchmarkRateHelper( planRateBenefitRequest);
								if(response!=null && response.getStatus().equalsIgnoreCase("SUCCESS")){
									Map<String,Object> responseData=response.getResponseData();
									if(responseData!=null &&responseData.get("benchMarkPre")!=null) {
										lastEHBAmount= Float.valueOf(responseData.get("benchMarkPre").toString());
									}else{
										throw new Exception("No EHB Amount returned for enrollment id: "+ aud.getId()+ " revision Id: "+ aud.getRev() );
									}
								}else{
									throw new Exception("Failure returned for enrollment id: "+ aud.getId()+ " revision Id: "+ aud.getRev() );
								}
								
								
					}else{
						
					}
				}
				aud.setSlcspAmt(lastEHBAmount);
				oldStatus=aud.getEnrollmentStatusLkp().getLookupValueCode();
			}
			enrollmentAudRepository.save(enrollmentAuds);
			Enrollment enrollment= enrollmentRepository.findById(enrollmentId);
			enrollment.setSlcspAmt(lastEHBAmount);
			enrollmentRepository.saveAndFlush(enrollment);
			}catch(Exception e){
				LOGGER.error("Failure to update EHB Amount for enrollment id: "+ enrollmentId);
				throw new GIException("Failure to update EHB Amount for enrollment id: "+ enrollmentId + "  "+ e);
			}
	}
	
	private Location getSubscHomeAddress(List<EnrolleeAud> enrolleeList, Date updateOn) throws GIException{
		Location subscAddress=null;
		if(enrolleeList!=null && enrolleeList.size()>0){
			EnrolleeAud subscriber= null;
			for(EnrolleeAud eAud: enrolleeList){
				if( eAud!=null && eAud.getPersonTypeLkp()!=null && eAud.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
					
						 subscriber= eAud;
						
					/*}catch(Exception e){
						LOGGER.error("Error in getting location audid for subscriber of enrollee  "+ eAud.getId()+" and revision id "+eAud.getRev() + "home location id "+eAud.getHomeAddressid().getId());
						throw new GIException("Error in getting location audid for subscriber of enrollee  "+ eAud.getId()+" and revision id"+eAud.getRev());
					}*/
					break;
				}
			}
			
			if(subscriber==null && updateOn!=null){
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(updateOn);
					calendar.add(Calendar.SECOND, EnrollmentConstants.FIVE);
					updateOn= calendar.getTime();
					Integer enrollmentId = enrolleeList.get(0).getEnrollment().getId();
				
					List<EnrolleeAud> subscriberAudList = enrolleeAudRepository.getEnrollSubscByDate(enrollmentId ,updateOn);
					if(subscriberAudList != null && !subscriberAudList.isEmpty()){
						subscriber = subscriberAudList.get(0);
					}
					else{
						throw new GIException("No subscriber found in Database for enrollmentId: "+enrollmentId);
					}
				
			}
			if(subscriber!=null){
				subscAddress=getLocationFromRevision(subscriber.getHomeAddressid().getId(), subscriber.getUpdatedOn());
			}
			
		}
		
		return subscAddress;
	}
	
	private boolean isEndDateChanged(EnrolleeAud enrolleeAud){
		boolean isChanged=false;
		if(enrolleeAud!=null){
			EnrolleeAud prevEnrolleAud= enrolleeAudRepository.getLastEnrolleeAud(enrolleeAud.getId(), enrolleeAud.getUpdatedOn());
			if(prevEnrolleAud!=null && !DateUtils.isSameDay(enrolleeAud.getEffectiveEndDate(), prevEnrolleAud.getEffectiveEndDate()) ){
				isChanged= true;
			}
		}
		return isChanged;
	}
	private List<EnrolleeAud> filterOutRetroTermEnrollees(List<EnrolleeAud> enrolleeAudList, EnrollmentAud aud){
		List<EnrolleeAud> filteredEnrolleeAudList= new ArrayList<>();
		if(enrolleeAudList!=null && enrolleeAudList.size()>0 && aud!=null){
			for(EnrolleeAud enrolleeAud: enrolleeAudList){
				if(enrolleeAud.getLastEventId()!=null && EnrollmentConstants.EVENT_TYPE_CHANGE.equalsIgnoreCase(enrolleeAud.getLastEventId().getEventTypeLkp().getLookupValueCode())
						&& !DateUtils.isSameDay(enrolleeAud.getEffectiveEndDate(), aud.getBenefitEndDate())
						&& enrolleeAud.getEffectiveEndDate().before( aud.getBenefitEndDate())
						&& isEndDateChanged(enrolleeAud)
						){
					continue;
				}
				filteredEnrolleeAudList.add(enrolleeAud);
			}
		}
		return filteredEnrolleeAudList;
	}
	boolean isEnrollmentTermEvent(String oldStatus, String newStatus){
		boolean enrlTermEvent=false;
		if(!EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(oldStatus) && !EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(oldStatus) && (EnrollmentConstants.ENROLLMENT_STATUS_TERM.equalsIgnoreCase(newStatus) || EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(newStatus) ))
		{
			enrlTermEvent=true;
		}
		return enrlTermEvent;
	}
	
	private Location setLocationFieldsFromAudit(Location localLocation, LocationAud revLocation) throws GIException {
		if(localLocation != null && revLocation!= null){
			try{
				localLocation.setId(revLocation.getId());
				localLocation.setAddOnZip(revLocation.getAddOnZip());
				localLocation.setAddress1(revLocation.getAddress1());
				localLocation.setAddress2(revLocation.getAddress2());
				localLocation.setCity(revLocation.getCity());
				localLocation.setCounty(revLocation.getCounty());
				localLocation.setCountycode(revLocation.getCountycode());
				localLocation.setLat(revLocation.getLat());
				localLocation.setLon(revLocation.getLon());
				localLocation.setRdi(revLocation.getRdi());
				localLocation.setState(revLocation.getState());
				localLocation.setZip(revLocation.getZip());
			}
			catch (Exception e){
				LOGGER.error("Exception in setNormalFieldsFromAudit()"+e.getMessage(),e);
				throw new GIException("Exception in setLocationFieldsFromAudit() :: " ,e);
			}
		}
		return localLocation;
	}
	private Location getLocationFromRevision(Integer locationID, Date eventDate) throws GIException{
		Location localLocation=null;
		try{
			if(eventDate!=null){
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(eventDate);
				calendar.add(Calendar.SECOND, EnrollmentConstants.FIVE);
				eventDate= calendar.getTime();
			}
			//LocationAud revLocationAud = locationRepository.getLocationAudByIdAndRev(locationID, revisionNumber);
			List<LocationAud> revLocationAudList = locationAudRepository.getLocationAudByIdAndDate(locationID, eventDate);
			if(revLocationAudList != null && !revLocationAudList.isEmpty()){
				localLocation = new Location();
				localLocation = setLocationFieldsFromAudit(localLocation, revLocationAudList.get(0));
			}
		}catch(Exception e){
			LOGGER.error("Exception in getLocationFromRevision"+e.getMessage(),e);
			throw new GIException("Exception in getLocationFromRevision"+e.getMessage(),e);
		}
		return localLocation;
	}
	
	boolean isSpecial(Integer revId, Integer enrollmentId){
		long count=0;
		count= enrollmentEventAudRepository.getSpecialEventCount(enrollmentId,revId );
		if(count>0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public List<BatchJobExecution> getRunningBatchList(String jobName) {
		return batchJobExecutionService.findRunningJob(jobName);
	}

	@Override
	public List<Integer> getUniqueHealthEnrollmentIds(String coverageYear) {
		
		return enrollmentRepository.getUniqueHealthEnrollmentIds(coverageYear);
	}
	
	public Integer getCarrierId(){
		if(carrierId == null || carrierId == 0){
			AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
			if(user != null){
				carrierId = user.getId();
			}	
		}
		return carrierId;
	}
	
}
