<!-- 
 @author Ashok 
 @UserStory : HIX-2308
 @Jira-id : HIX-2461
 -->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="row-fluid">
    <div class="gutter10">
    	<h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${hiosissuerid}"/>"/>${repUser.firstName}<c:if test="${repUser.lastName != ''}"> ${repUser.lastName}</c:if>  <small><spring:message code="pgheader.authRepAddedOn"/> ${repUserCreatedOn}</small></h1>
    </div>
</div>
	<div class="row-fluid">
	<c:set var="encIssuerRepId" ><encryptor:enc value="${repId}" isurl="true"/> </c:set>
		<div class="gutter10">
		<div class="span3" id="sidebar">
                <div class="header">
                    <h4><spring:message  code="label.details"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ul class="nav nav-list">
                    <li class="active"><spring:message  code="label.repInfoReview"/></li>
                </ul>
                <!-- end of side bar -->
		</div>
			<!-- end of span3 -->
            <div class="span9" id="rightpanel">
                <form class="form-horizontal" id="frmIssuerEditRep" name="frmIssuerEditRep" action="<c:url value="/planmgmt/editrepresentative/${encIssuerRepId}" />" method="POST">
                    <df:csrfToken/>
                    <input type="hidden" name="editRep" id="editRep" value="edit">
                    <input type="hidden" name="id" id="id" value="<encryptor:enc value="${repObjForIssuer.id}"></encryptor:enc>"/>
                    <input type="hidden" name="primaryContactOld" id="primaryContactOld" value="${primaryContact}">
                        <div class="header">
                            <h4 class="span9"><spring:message  code="label.repInfo"/></h4>
                            <a href='<c:url value="/planmgmt/managerepresentative"/>' class="btn btn-small"><spring:message  code="label.btnCancel"/></a>
                           <%--  <c:if test="${STATE_CODE == 'ID'}"> --%>
                            	<input type="button" class="btn btn-small btn-primary" value="Save" onclick="javascript:submitForm();"/>
                            <%-- </c:if>	 --%>
                        </div>
                        <div class="gutter10">
        
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.firstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                    <div class="controls">
                                      <input type="text" name="firstName" id="firstName" value="${repUser.firstName}" class="input-large">
                                        <c:if test="${isIssuerRepDuplicate == 'true'}">
                                        <div class="error">
                                               <label class="error"><span><spring:message  code="err.duplicateRepresentative"/></span></label>
                                        </div> 
                                        </c:if>
                                      <div id="firstName_error" class=""></div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.lastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                    <div class="controls"><input type="text" name="lastName" id="lastName" value="${repUser.lastName}" class="input-large">
                                        <div id="lastName_error" class=""></div>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.title"/> <!-- <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> --></label>
                                    <div class="controls"><input type="text" name="title" id="title" value="${repObjForIssuer.title}" class="input-large">	
                                        <div id="title_error" class=""></div>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.phoneNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                    <div class="controls">
                                   <%--  <input type="text" name="phone" id="phone" value="${repObjForIssuer.phone}" class="input-large"> --%>
                                    
                                     <input type="text" name="phone1" id="phone1" value="${phone1}" maxlength="3" class="area-code input-mini" /> 
								<input type="text" name="phone2" id="phone2" value="${phone2}" maxlength="3" class="input-mini" /> 
								<input type="text" name="phone3" id="phone3" value="${phone3}" maxlength="4" class="input-small" /> 
	                            <input type="hidden" name="phone" id="phone" class="input-large">
                                        <div id="phone_error" class=""></div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.emailAddress"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                   <c:choose>
										<c:when test="${isFromUser eq 'USER'}">
											<div class="controls"><input type="text" name="email" id="email" value="${repUser.email}" class="input-large" readonly="true" maxlength="50">
												<div id="email_error" class=""></div>
											</div>
	 									</c:when>
										<c:otherwise>
											<div class="controls"><input type="text" name="email" id="email" value="${representativeObj.email}" class="input-large" readonly="true" maxlength="50">
	 											<div id="email_error" class=""></div>
											</div>
	 									</c:otherwise>
									</c:choose>
                                </div>
                                 <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.primaryContact"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                    <div class="controls">
	                                    <select class="input-small" name="primaryContact" id="primaryContact" <c:if test="${null != repObjForIssuer.status && fn:toUpperCase(repObjForIssuer.status) == 'INACTIVE' }"> disabled="true"</c:if> onchange="validatePrimaryContact(this.value,'${primaryContact}');">
			                             	<c:forEach var="primaryContactObj" items="${primaryContactList}">
									    		<option <c:if test="${primaryContactObj == primaryContact}"> SELECTED </c:if> value="${primaryContactObj}">${primaryContactObj}</option>
											</c:forEach>
									    </select>
									    <div class="error" id="primaryContact_error" style="display:none">
                                      			<label class="error"><span><em class='excl'>!</em><spring:message  code="err.isIssuerHasPrimaryContact"/></span></label>
                                   		</div> 
									     
                                    </div>
                                </div>
                                <%-- below code added by kuldeep for Jira HIX-7757 --%>
                                <c:if test="${fn:toUpperCase(STATE_CODE) == 'CA'}">
									<div class="control-group">
	                       				<label for="addressLine1" class="required control-label"><spring:message  code="label.streetaddress1"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
			                            <div class="controls">
			                                <input type="text" name="location.address1" id="addressLine1" class="input-xlarge" value="${repObjForIssuer.location.address1}">
			                                <div id="addressLine1_error" class=""></div>
			                            </div> <!-- end of controls-->
	                   				</div><!-- end of control-group -->
                    
                  					<div class="control-group">
				                        <label for="addressLine2" class="required control-label"><spring:message  code="label.streetaddress2"/><%-- <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> --%></label>
				                            <div class="controls">
				                                <input type="text" name="location.address2" id="addressLine2" class="input-xlarge" value="${repObjForIssuer.location.address2}">
				                                <div id="addressLine2_error" class=""></div>
				                            </div> <!-- end of controls-->
				                    </div><!-- end of control-group -->
                    
				                    <div class="control-group">
				                        <label for="city" class="required control-label"><spring:message  code="label.city"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				                            <div class="controls">
				                                <input type="text" name="location.city" id="city" class="input-xlarge" value="${repObjForIssuer.location.city}">
				                                <div id="city_error" class=""></div>
				                            </div> <!-- end of controls-->
				                    </div><!-- end of control-group -->
                    
				                    <div class="control-group">
				                    	<label for="state" class="required control-label"><spring:message  code="label.emplState"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				                        <div class="controls">
				                        	<select  id="state" name="location.state">
				                            	<option value="">Select</option>
				                                    <c:forEach var="state" items="${statelist}">
				                                        <option <c:if test="${state.code == repObjForIssuer.location.state}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
				                                    </c:forEach>
				                             </select>
				                         	<div id="state_error"></div>
				                         </div>
				                    </div><!-- end of control-group -->                        
                        
			                        <div class="control-group">
			                            <label for="zip" class="required control-label"><spring:message  code="label.zipCode"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
			                            <div class="controls">
			                            	<input type="text" name="location.zip" id="zip" class="input-small" maxlength="5" value="${repObjForIssuer.location.zip}">
			                                <div id="zip_error" class=""></div>
			                            </div> <!-- end of controls-->
			                        </div><!-- end of control-group -->
                        	</c:if>						    
                       		</div>
                    
                </form>	
        	</div><!--  end of span9 -->
		</div>
	</div><!-- end row-fluid -->

<script type="text/javascript">

function validatePrimaryContact(seletedValue, actualValue){
	document.getElementById("primaryContact_error").style.display="none";
	if((actualValue.toUpperCase() != "NO") && (seletedValue.toUpperCase() == "NO")){
		document.getElementById("primaryContact_error").style.display="block";
	}
}
function submitForm(){
	if(!(($("#primaryContactOld").val()).toUpperCase() != "NO" &&	($("#primaryContact").val()).toUpperCase() == "NO")){
	$("#frmIssuerEditRep").submit();
	}
} 


/* jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, ""); 
	return this.optional(element) || phone_number.length > 9 &&
		phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
}, "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>");
 */
var validator = $("#frmIssuerEditRep").validate({ 
	rules : {
		firstName : {required : true},
		lastName : {required : true},
		//phone : { required : true, number : true, minlength : 10, maxlength : 10},
		phone1 : {required : true, digits: true, phoneUS: true},
		phone2 : {required : true, digits: true, phoneUS: true},
		phone3 : {required : true, digits: true, phoneUS: true},
		email : { required : true, emailIdCheck: true},
		"location.address1" : { required : true},
		"location.city" : {required : true},
		"location.state" : { required : true},
		"location.zip" : { required : true, digits: true, minlength:5, maxlength:5}
	},
	messages : {
		firstName : { required : "<span><em class='excl'>!</em><spring:message  code='err.firstName' javaScriptEscape='true'/></span>"},
		lastName: { required : "<span><em class='excl'>!</em><spring:message  code='err.lastName' javaScriptEscape='true'/></span>"},
		phone3 : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
		     digits : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
		            minlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
		            maxlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>"
		      },
			  
		/* phone : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
	              number : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>",
	              minlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneMinMaxlength' javaScriptEscape='true'/></span>",
	              maxlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneMinMaxlength' javaScriptEscape='true'/></span>"
          		}, */
		email : { required : "<span><em class='excl'>!</em><spring:message  code='err.email' javaScriptEscape='true'/></span>",
				  emailIdCheck : "<span><em class='excl'>!</em><spring:message  code='err.invalidEmail' javaScriptEscape='true'/></span>"},
        "location.address1" : { required : "<span><em class='excl'>!</em><spring:message  code='err.addressLine1' javaScriptEscape='true'/></span>"},
        "location.city" : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerCity' javaScriptEscape='true'/></span>"},
        "location.state": { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerState' javaScriptEscape='true'/></span>"},
        "location.zip" : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerZip' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error');
	} 
});

//phone number validation
$.validator.addMethod("phoneUS", function(value, element, param) {
 if($.browser.msie==true && $.browser.version=='8.0'){
  phoneNumber1 = $("#phone1").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
  phoneNumber2 = $("#phone2").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
  phoneNumber3 = $("#phone3").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
 }else{
  phoneNumber1 = $("#phone1").val().trim();
  phoneNumber2 = $("#phone2").val().trim();
  phoneNumber3 = $("#phone3").val().trim();
 }
 var phoneNumber=phoneNumber1+phoneNumber2+phoneNumber3;
 
 if(!isNaN(phoneNumber1) && !isNaN(phoneNumber2) && !isNaN(phoneNumber3) && phoneNumber.length==10){
  $("#phone").val(phoneNumber1+phoneNumber2+phoneNumber3);
  return true;
 }else{
  if(isNaN(phoneNumber1) || phoneNumber1.length!=3){
   $('#phone1').removeClass('input-mini valid').addClass('input-small error');
  }else{
   $('#phone1').removeClass('input-mini error').addClass('input-small valid');
  }
  if(isNaN(phoneNumber2) || phoneNumber2.length!=3){
   $('#phone2').removeClass('input-mini valid').addClass('input-small error');
  }else{
   $('#phone2').removeClass('input-mini error').addClass('input-small valid');
  }
  if(isNaN(phoneNumber3) || phoneNumber3.length!=4){
   $('#phone3').removeClass('input-mini valid').addClass('input-small error');
  }else{
   $('#phone3').removeClass('input-mini error').addClass('input-small valid');
  }
  return false;
 } 
},"<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>");

$.validator.addMethod("emailIdCheck", function(value, element) {
	//var emailRegEx = new RegExp("^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$");
	var emailRegEx = new RegExp("^[_A-Za-z0-9\u002e_]+([A-Za-z0-9\u002e]+)@[A-Za-z0-9]+([\u002e][a-zA-Z]{2,})+$");
	if("" != value && !emailRegEx.test(value)){
		return false;
	}else{
		return true;
	}
});
</script>		
