(function() {
    'use strict';
    angular
        .module('reconciliationWorkbenchApp')
        .factory('clearSearchResultsService', clearSearchResultsService);

    clearSearchResultsService.$inject = ['$rootScope'];

    function clearSearchResultsService ($rootScope){
        return {
            subscribe: function(scope, callback) {
                var handler = $rootScope.$on('clearSearchResults', callback);
                scope.$on('$destroy', handler);
            },

            notify: function() {
                $rootScope.$emit('clearSearchResults');
            }
        };
    }
})();