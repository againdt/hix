package com.getinsured.hix.batch.enrollment.task;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.util.EnrollmentEmailUtils;
import com.getinsured.hix.batch.util.GhixBatchConstants;
import com.getinsured.hix.dto.enrollment.EnrollmentBatchEmailDTO;
import com.getinsured.hix.enrollment.service.EnrollmentBatchService;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author ajinkya_m
 * 
 */
public class EnrollmentXMLIndividualTask implements Tasklet {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentXMLIndividualTask.class);
	private EnrollmentBatchService enrollmentBatchService;
	private EnrollmentEmailUtils enrollmentEmailUtils;
	private static final String ENROLLMENT_TYPE_INDIVIDUAL = "FI";
	private String startDateStr;
	private String endDateStr;
	private String carrierResendFlag;
	private String hiosIssuerIdStr;
	private String hiosIssuerId;
	
	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws GIException {
		LOGGER.info("EnrollmentXMLIndividualTask.execute : START :: ");
		Date lastRunDate=null;
		
		boolean datesProvided=false;
		//System.out.println("Dates provided :: Start Date = "+startDateStr+ " End Date = "+endDateStr);
		LOGGER.debug("Dates provided :: Start Date = " + startDateStr + " End Date = " + endDateStr);
		EnrollmentBatchEmailDTO enrollmentBatchEmailDTO = new EnrollmentBatchEmailDTO();
		enrollmentBatchEmailDTO.setFileLocation(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.XMLEXTRACTPATH));
		enrollmentBatchEmailDTO.setErrorCode(201);
		enrollmentBatchEmailDTO.setAppServer(GhixBatchConstants.appserver);
		
		try {
			if((startDateStr == null && endDateStr != null) || (startDateStr != null && endDateStr == null)){
				LOGGER.error("Please provide both Start Date and End Date");
				throw new GIException("Please provide both Start Date and End Date");
			}else if(startDateStr!=null && endDateStr!=null){
				if((startDateStr.equalsIgnoreCase("null") && endDateStr.equalsIgnoreCase("null")) || (startDateStr.isEmpty() && endDateStr.isEmpty())){
					startDateStr = null;
					endDateStr = null;
				}else{
					if(DateUtil.isValidDate(startDateStr, EnrollmentConstants.BATCH_DATE_FORMAT) && DateUtil.isValidDate(endDateStr, EnrollmentConstants.BATCH_DATE_FORMAT)){
						if(!DateUtil.isValidDateInterval(DateUtil.StringToDate(startDateStr, EnrollmentConstants.BATCH_DATE_FORMAT),DateUtil.StringToDate(endDateStr,EnrollmentConstants.BATCH_DATE_FORMAT))){
							
							LOGGER.error(" EnrollmentXMLIndividualTask.execute : Start date is greater than End date.");
							enrollmentBatchEmailDTO.setErrorMessage("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
							//callEnrollmentBatchFailureEmailNotification(enrollmentBatchEmailDTO, chunkContext);
							throw new GIException("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
							
						}
						datesProvided=true;
					}else{
						LOGGER.error("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
						enrollmentBatchEmailDTO.setErrorCode(201);
						enrollmentBatchEmailDTO.setErrorMessage("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
						throw new GIException("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
					}
				}
			}
			//validate carrierResendFlag
			if(carrierResendFlag != null){
				if(carrierResendFlag.equalsIgnoreCase(GhixBatchConstants.RESEND_FLAG) || carrierResendFlag.equalsIgnoreCase(GhixBatchConstants.HOLD_FLAG)){
					datesProvided=true;
					carrierResendFlag= carrierResendFlag.toUpperCase();
				}else{
					LOGGER.error("Please provide valid value for Carrier_send_flag else remove the Carrier_send_flag parameter");
					enrollmentBatchEmailDTO.setErrorMessage("Please provide valid value for Carrier_send_flag else remove the Carrier_send_flag parameter");
					throw new GIException("Please provide valid value for Carrier_send_flag else remove the Carrier_send_flag parameter");
				}
			}
			
			// Apply Validation on HIOSIssuer Id 
			if(isNotNullAndEmpty(hiosIssuerIdStr)){
			 hiosIssuerId = hiosIssuerIdStr;
			 
			}
			
			//enrollmentBatchService.serveEnrollmentRecordJob(ENROLLMENT_TYPE_INDIVIDUAL,startDateStr,endDateStr, carrierResendFlag, hiosIssuerId);
			LOGGER.info("serveEnrollmentRecordJob :: executed successfully");
			
			if(datesProvided){
				lastRunDate=enrollmentBatchService.getJOBLastRunDate(chunkContext.getStepContext().getJobName());
			chunkContext.getStepContext().getStepExecution().getJobExecution().setStartTime(lastRunDate);
			}
			
		} catch (Exception e) {
			LOGGER.info("serveEnrollmentRecordJob :: failed to execute"+ e.getCause());
			enrollmentBatchEmailDTO.setErrorMessage(e.getMessage());
			enrollmentEmailUtils.enrollmentBatchFailureEmailNotification(enrollmentBatchEmailDTO, chunkContext);
			throw new GIException("serveEnrollmentRecordJob :: failed to execute"+ e.getCause(), e);
		}
		
		LOGGER.info("EnrollmentXMLIndividualTask.execute : END :: ");
		
		
		
		return RepeatStatus.FINISHED;
	}

	public String getStartDateStr() {
		return startDateStr;
	}

	public void setStartDateStr(String startDateStr) {
		if(startDateStr != null && ("".equalsIgnoreCase(startDateStr) || "null".equalsIgnoreCase(startDateStr))){
			this.startDateStr = null;
		}else{
			this.startDateStr = startDateStr;
		}
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		if(endDateStr != null && ("".equalsIgnoreCase(endDateStr) || "null".equalsIgnoreCase(endDateStr))){
			this.endDateStr = null;
		}else{
			this.endDateStr = endDateStr;
		}
	}

	public EnrollmentBatchService getEnrollmentBatchService() {
		return enrollmentBatchService;
	}

	public void setEnrollmentBatchService(
			EnrollmentBatchService enrollmentBatchService) {
		this.enrollmentBatchService = enrollmentBatchService;
	}

	public EnrollmentEmailUtils getEnrollmentEmailUtils() {
		return enrollmentEmailUtils;
	}

	public void setEnrollmentEmailUtils(EnrollmentEmailUtils enrollmentEmailUtils) {
		this.enrollmentEmailUtils = enrollmentEmailUtils;
	}

	public String getCarrierResendFlag() {
		return carrierResendFlag;
	}

	public void setCarrierResendFlag(String carrierResendFlag) {
		if(carrierResendFlag != null && ("".equalsIgnoreCase(carrierResendFlag) || "null".equalsIgnoreCase(carrierResendFlag))){
			this.carrierResendFlag = null;
		}else{
			this.carrierResendFlag = carrierResendFlag;
		}
	}

	public String getHiosIssuerIdStr() {
		return hiosIssuerIdStr;
	}

	public void setHiosIssuerIdStr(String hiosIssuerIdStr) {
		if(hiosIssuerIdStr != null && ("".equalsIgnoreCase(hiosIssuerIdStr) || "null".equalsIgnoreCase(hiosIssuerIdStr))){
			this.hiosIssuerIdStr = null;
		}else{
			this.hiosIssuerIdStr = hiosIssuerIdStr;
		}
	}
}
