package com.getinsured.ssap.controller

import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil
import com.getinsured.hix.ssap.SsapConstants
import com.getinsured.iex.ssap.SingleStreamlinedApplication
import com.getinsured.iex.ssap.model.SsapApplication
import com.getinsured.ssap.model.eligibility.EligibilityResponse
import com.getinsured.ssap.model.eligibility.ResponseStatus
import com.getinsured.ssap.model.eligibility.RunMode
import com.getinsured.ssap.service.EligibilityEngineIntegrationService
import com.getinsured.ssap.util.JsonUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*


/**
 * Controller to launch eligibility engine for given SSAP id.
 *
 * @author Yevgen Golubenko
 * @since 2019-07-11
 */
@RestController
open class EligibilityController @Autowired constructor(
  private val eligibilityEngineIntegrationService: EligibilityEngineIntegrationService,
  private val ssapApplicationRepository: SsapApplicationRepository,
  private val ghixJasyptEncrytorUtil: GhixJasyptEncrytorUtil
) {
  companion object {
    val log: Logger = LoggerFactory.getLogger(EligibilityController::class.java)
  }

  /**
   * Method that is called from batch process.
   * @param encApplicationId encoded ssap application id.
   */
  @RequestMapping(value = ["/api/newssap/eligibility/{encApplicationId}"], method = [RequestMethod.GET])
  open fun callEligibility(@PathVariable encApplicationId: String): ResponseEntity<Any> {
    val applicationId: Long = ghixJasyptEncrytorUtil.decryptStringByJasypt(encApplicationId).toLong()
    return invokeEligibility(applicationId, null, null);
  }
                             
  /**
   * This method is exposed primarily for one of the ghix-batch jobs. If you are
   * thinking about refactoring or removing this, ghix-batch would be a first place for you
   * to check to see if this is still used or not (July 11, 2019)
   *
   * @param applicationId SSAP application id.
   * @param sepEvent Special Enrollment Period Event Name (optional)
   * @param sepEventDate Special Enrollment Period Event Date (optional).
   * @since 2019-07-11
   */
  @PreAuthorize(SsapConstants.SSAP_EDIT) // TODO: Does batch job going to use full credentials?
  @RequestMapping(value = ["/newssap/eligibility/invoke/{applicationId}"], method = [RequestMethod.GET])
  open fun invokeEligibility(@PathVariable applicationId: Long,
                             @RequestParam("sepEvent", required = false) sepEvent: String?,
                             @RequestParam("sepEventDate", required = false) sepEventDate: String?): ResponseEntity<Any> {
    log.info("Invoking eligibility engine for SSAP: {}", applicationId)

    val ssapApplication: SsapApplication = ssapApplicationRepository.findOne(applicationId)
      ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
        linkedMapOf(
          "code" to HttpStatus.NOT_FOUND.value(),
          "message" to "No SSAP found for given id",
          "eligibilityResult" to null)
      )

    val singleStreamlinedApplication: SingleStreamlinedApplication = JsonUtil.parseApplicationDataJson(ssapApplication)
    val eligibilityResponse: EligibilityResponse = eligibilityEngineIntegrationService.invokeEligibilityEngine(singleStreamlinedApplication, sepEvent, sepEventDate, RunMode.BATCH)

    log.info("Eligibility engine invoked for SSAP: {}, Eligibility Response: {}", eligibilityResponse)

    val response: LinkedHashMap<String, Any> = LinkedHashMap()

    if (eligibilityResponse.status == ResponseStatus.success) {
      response["status"] = HttpStatus.OK.value()
      response["message"] = "Eligibility engine invoked for given SSAP id"
    } else {
      response["status"] = HttpStatus.INTERNAL_SERVER_ERROR.value()
      eligibilityResponse.errors?.forEach { (k, v) ->
        if (!response.containsKey("message")) {
          response["message"] = "$k: $v"
        }
      }

      if (!response.containsKey("message")) {
        response["message"] = "error occurred"
      }
    }
    response["eligibilityResult"] = eligibilityResponse

    return ResponseEntity.ok(response)
  }
}
