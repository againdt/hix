package com.getinsured.ahbx.shop.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.ahbx.shop.employercaseorderid.CaseOrderIdSaveDTO;
import com.getinsured.ahbx.shop.employercaseorderid.CaseOrderIdSaveResponse;
import com.getinsured.ahbx.shop.employercaseorderid.SaveCaseOrderIds;
import com.getinsured.ahbx.shop.employercaseorderid.SaveCaseOrderIdsResponse;
import com.getinsured.ahbx.shop.employerdetails.EmployeeRosterDTO;
import com.getinsured.ahbx.shop.employerdetails.EmployerInfoResponse;
import com.getinsured.ahbx.shop.employerdetails.RetrieveEmployerDetails;
import com.getinsured.ahbx.shop.employerdetails.RetrieveEmployerDetailsResponse;
import com.getinsured.ahbx.shop.util.ShopConstants;
import com.getinsured.ahbx.util.GIException;
import com.getinsured.hix.dto.shop.EmployeeDTO;
import com.getinsured.hix.dto.shop.EmployeeDetailsDTO;
import com.getinsured.hix.dto.shop.EmployerDTO;
import com.getinsured.hix.dto.shop.EmployerLocationDTO;
import com.getinsured.hix.dto.shop.LocationDTO;
import com.getinsured.hix.dto.shop.ShopResponse.ResponseType;
import com.getinsured.hix.model.Employee.EmployeeBooleanFlag;
import com.getinsured.hix.model.EmployeeApplication.TypeOfEmployee;
import com.getinsured.hix.model.EmployeeDetails;
import com.getinsured.hix.model.EmployeeDetails.EmplDetailsBooleanFlag;
import com.getinsured.hix.model.EmployerLocation;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.webclient.SoapWebClient;

@Service("employerWsService")
public class EmployerWsServiceImpl implements EmployerWsService {
	private static final Logger LOGGER = Logger.getLogger(EmployerWsServiceImpl.class);
	@Autowired private WebServiceTemplate webServiceTemplate;
	@Autowired private SoapWebClient<JAXBElement<RetrieveEmployerDetailsResponse>> soapWebClient;
	@Autowired private SoapWebClient<JAXBElement<SaveCaseOrderIdsResponse>> soapWebClientCaseOrderID;
	@Autowired private ZipCodeService zipCodeService;
	
	@Override
	@SuppressWarnings("unchecked")
	public EmployerInfoResponse getEmployerDetails(String employerCaseId) throws GIException {
		RetrieveEmployerDetails employerDetailsRequest = new RetrieveEmployerDetails();
		employerDetailsRequest.setEmployerCaseId(employerCaseId);

		String wsdlUrl = ShopConstants.AHBX_GET_EMPLOYER_WSDL;
		RetrieveEmployerDetailsResponse returnResObject = new RetrieveEmployerDetailsResponse();
		//LOGGER.info("AHBX Employer Request - " + employerDetailsRequest);		
		try{
			webServiceTemplate.setDefaultUri(wsdlUrl);
			//JAXBElement<RetrieveEmployerDetailsResponse> returnObject = (JAXBElement<RetrieveEmployerDetailsResponse>)webServiceTemplate.marshalSendAndReceive(employerDetailsRequest);
			JAXBElement<RetrieveEmployerDetailsResponse> returnObject = soapWebClient.send(employerDetailsRequest, wsdlUrl, "IND03", webServiceTemplate);
			returnResObject = returnObject.getValue();
		}catch(WebServiceIOException e){
			throw new GIException(e);
		}catch(SoapFaultClientException e){
			throw new GIException(e);
		}catch(Exception e) {
			throw new GIException(e);
		}
		//LOGGER.info("AHBX Employer Response - " + returnResObject);
		return returnResObject.getReturn();
	}
	
	@Override
	public Map<String, Object> createEmployerData(EmployerInfoResponse employerInfoResponse, ResponseType responseType) throws GIException{
		Map<String, Object> responseData  = new HashMap<String, Object>();

		responseData.put("Employer", this.createEmployerDTO(employerInfoResponse));
		responseData.put("Employees", this.createEmployeesDTO(employerInfoResponse));
		responseData.put("effectiveCoverageDate", employerInfoResponse.getEffectiveCoverageDate());
		responseData.put("dependentCoverageIndicator", employerInfoResponse.getDependentCoverageIndicator() != null ? employerInfoResponse.getDependentCoverageIndicator() : "N");
		return responseData;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public CaseOrderIdSaveResponse sendCaseOrderID(int employerCaseID, int orderID)  throws GIException{
		
		SaveCaseOrderIds saveCaseOrderIdsReq = new SaveCaseOrderIds();
		CaseOrderIdSaveDTO caseOrderIdSaveDTOReq = new CaseOrderIdSaveDTO();
		caseOrderIdSaveDTOReq.setEmployerCaseID(employerCaseID);
		caseOrderIdSaveDTOReq.setOrderID(orderID);
		
		saveCaseOrderIdsReq.getSaveCaseOrderID().add(caseOrderIdSaveDTOReq);
		
		//LOGGER.info("AHBX Employer Case Order Request " + saveCaseOrderIdsReq.toString());
		String wsdlUrl = ShopConstants.AHBX_SEND_CASEORDERID_WSDL;
		SaveCaseOrderIdsResponse saveCaseOrderIdsResponse = new SaveCaseOrderIdsResponse();
		try{
			webServiceTemplate.setDefaultUri(wsdlUrl);
			//JAXBElement<SaveCaseOrderIdsResponse> returnObject = (JAXBElement<SaveCaseOrderIdsResponse>)webServiceTemplate.marshalSendAndReceive(saveCaseOrderIdsReq);
			JAXBElement<SaveCaseOrderIdsResponse> returnObject = soapWebClientCaseOrderID.send(saveCaseOrderIdsReq, wsdlUrl, "sendCaseOrderID", webServiceTemplate);
			saveCaseOrderIdsResponse = returnObject.getValue();
			//LOGGER.info("AHBX Employer Case Order Response === " + saveCaseOrderIdsResponse.toString());
		}catch(WebServiceIOException e){
			throw new GIException(e);
		}catch(SoapFaultClientException e){
			throw new GIException(e);
		}catch(Exception e) {
			LOGGER.error("", e);
			throw new GIException(e);
		}
		return saveCaseOrderIdsResponse.getReturn();
	}
	
	private EmployerDTO createEmployerDTO( EmployerInfoResponse employerInfoResponse) throws GIException {
		EmployerDTO employerDTO = new EmployerDTO();

		try {
			employerDTO.setExternalId(employerInfoResponse.getEmployerCaseId());
			employerDTO.setName(employerInfoResponse.getBusinessName());
			employerDTO.setTotalEmployees(employerInfoResponse.getEmployeeCountSent());
			employerDTO.setFullTimeEmp(employerInfoResponse.getEmployeeCountSent());
			employerDTO.setOrgType(EmployerDTO.OrgType.CORPORATION);
			Float floatObj = new Float(employerInfoResponse.getSbtcPercentage());
			employerDTO.setTaxCredit(floatObj.floatValue());
			
			List<String> workLocationZipCodeList = employerInfoResponse.getWorkLocationZipCodeList();
			String countyCode = employerInfoResponse.getEmployerQuotingcounty();
			
			if(countyCode == null){
				throw new GIException("EmployerQuotingcounty is not available in IND03 response");
			}
			
			List<String> primaryZipCodes = zipCodeService.getZipByCountyCodeAndZips(workLocationZipCodeList, countyCode);
			
			if(primaryZipCodes == null || primaryZipCodes.size() == 0){
				throw new GIException("countyCode " + countyCode + " is not matching with Employer WorkLocationZipCodes "+ workLocationZipCodeList +" in IND03 response");
			}
			
			List<EmployerLocation> employerLocationList = new ArrayList<EmployerLocation>();
			Boolean primaryLoc = false;
			for (String zipcode : workLocationZipCodeList) {
				LocationDTO locationDTO = new LocationDTO();
				locationDTO.setZip((zipcode));
				
				EmployerLocationDTO employerLocationDTO = new EmployerLocationDTO();
				employerLocationDTO.setLocation(locationDTO);
				if(primaryZipCodes.contains(zipcode) && primaryLoc == false){
					employerLocationDTO.setPrimaryLocation(EmployerLocation.PrimaryLocation.YES);
					locationDTO.setCountycode(countyCode);
					primaryLoc = true;
				}else{
					employerLocationDTO.setPrimaryLocation(EmployerLocation.PrimaryLocation.NO);
				}
				employerLocationList.add(employerLocationDTO);
			}
			employerDTO.setLocations(employerLocationList);
		}catch (Exception e) {
			throw new GIException(e);
		}
		return employerDTO;
		
	}
	
	private List<EmployeeDTO> createEmployeesDTO( EmployerInfoResponse employerInfoResponse) throws GIException {
		List<EmployeeRosterDTO> employeeRosterList = employerInfoResponse.getEmployeeRosterDTO();
		List<EmployeeDTO> employeeDTOList = new ArrayList<EmployeeDTO>();
		SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyy");
		try{
			
			String dependentCoverageIndicator = employerInfoResponse.getDependentCoverageIndicator() != null ? employerInfoResponse.getDependentCoverageIndicator() : "N";
			
			int empCount = 1;
			for (EmployeeRosterDTO employeeRosterDTO : employeeRosterList) {
				
				EmployeeDTO employeeDTO = new EmployeeDTO();
				String income = String.valueOf(employeeRosterDTO.getSalary());
				//employeeDTO.setYearlyIncome(Double.parseDouble(income));
				employeeDTO.setId(Integer.valueOf(employeeRosterDTO.getEmployeeRosterId()));
				
				EmployerLocationDTO employerLocationDTO = new EmployerLocationDTO();
				LocationDTO locationDTO = new LocationDTO();
				locationDTO.setZip(String.valueOf(employeeRosterDTO.getWorkSiteZipCode()));
				employerLocationDTO.setLocation(locationDTO);
				employeeDTO.setEmployerLocation(employerLocationDTO);
				employeeDTO.setName("Employee " + (empCount++) );
				//employeeDTO.setTypeOfEmployee(TypeOfEmployee.valueOf(employeeRosterDTO.getEmploymentType().replace("-", "").toUpperCase()));
				if(StringUtils.isNotEmpty(employeeRosterDTO.getSpouse())){
					EmployeeBooleanFlag isMarried = (employeeRosterDTO.getSpouse().equals("Y") ? EmployeeBooleanFlag.YES : EmployeeBooleanFlag.NO);
					employeeDTO.setIsMarried(isMarried);
				}
				
				List<EmployeeDetails> employeeDetailsList = new ArrayList<EmployeeDetails>();
				EmployeeDetailsDTO employeeDetailsDTO =  new EmployeeDetailsDTO();
				LocationDTO empHomeLocationDTO = new LocationDTO();
				empHomeLocationDTO.setZip(String.valueOf(employeeRosterDTO.getZipcode()));
				empHomeLocationDTO.setCountycode(employeeRosterDTO.getEmployeeQuotingcounty());
				employeeDetailsDTO.setLocation(empHomeLocationDTO);
				Date dobDt;
				try {
					dobDt = formatter.parse(employeeRosterDTO.getEmployeeDOB());
				} catch (ParseException e) {
					throw new GIException("Invalid employee dob " + employeeRosterDTO.getEmployeeDOB(), e);
				}
				
				employeeDetailsDTO.setDob(dobDt);
				EmplDetailsBooleanFlag smoker = (employeeRosterDTO.getSmoker().equals("N") ? EmployeeDetails.EmplDetailsBooleanFlag.NO : EmployeeDetails.EmplDetailsBooleanFlag.YES);
				employeeDetailsDTO.setSmoker(smoker);
				employeeDetailsDTO.setType(EmployeeDetails.Type.EMPLOYEE);
				//Add employee details
				employeeDetailsList.add(employeeDetailsDTO);
				
				//Add spouse details
				if(StringUtils.isNotEmpty(employeeRosterDTO.getSpouse())){
					if(employeeRosterDTO.getSpouse().equals("Y")){
						EmployeeDetailsDTO employeeSpouseDetailsDTO =  new EmployeeDetailsDTO();
						if(employeeRosterDTO.getSpouseDOB() != null){
							try {
								Date spouseDobDt = formatter.parse(employeeRosterDTO.getSpouseDOB()); 
								employeeSpouseDetailsDTO.setDob(spouseDobDt);
							} catch (ParseException e) {
								throw new GIException("Invalid employee dob " + employeeRosterDTO.getEmployeeDOB(), e);
							}
						}else{
							employeeSpouseDetailsDTO.setDob(dobDt);
						}
						employeeSpouseDetailsDTO.setType(EmployeeDetails.Type.SPOUSE);
						employeeSpouseDetailsDTO.setLocation(empHomeLocationDTO);
						employeeDetailsList.add(employeeSpouseDetailsDTO);
					}
				}
				
				// IND03 v7 changes
				if(dependentCoverageIndicator.equals("Y")){
					for(int i=0; i < employeeRosterDTO.getNoOfDependents(); i++){
						if(employeeRosterDTO.getDependentDOB().get(i) != null){
							EmployeeDetailsDTO employeeDepe1DetailsDTO = this.getDependentData(employeeRosterDTO.getDependentDOB().get(i), empHomeLocationDTO); 
							employeeDetailsList.add(employeeDepe1DetailsDTO);
						}
					}
				}
				
				employeeDTO.setEmployeeDetails(employeeDetailsList);
				employeeDTO.setDependentCount(employeeDetailsList.size()-1);
				employeeDTOList.add(employeeDTO);
			}
		}catch (Exception e) {
			throw new GIException(e);
		}
		return employeeDTOList;
	}
	
	private EmployeeDetailsDTO getDependentData(String dependentDOB, LocationDTO empHomeLocationDTO) throws GIException{
		EmployeeDetailsDTO employeeDepeDetailsDTO =  new EmployeeDetailsDTO();
		SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyy");
		Date dependentDob = null;
		try {
			dependentDob = formatter.parse(dependentDOB); 
		} catch (ParseException e) {
			throw new GIException("Invalid dependent dob " + dependentDOB, e);
		}
		employeeDepeDetailsDTO.setDob(dependentDob);
		employeeDepeDetailsDTO.setType(EmployeeDetails.Type.CHILD);
		employeeDepeDetailsDTO.setLocation(empHomeLocationDTO);
		return employeeDepeDetailsDTO;
	}
}