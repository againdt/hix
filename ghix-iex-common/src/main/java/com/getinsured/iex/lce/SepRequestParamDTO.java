package com.getinsured.iex.lce;

import java.util.List;

public class SepRequestParamDTO {

	List<RequestParamDTO> events;
	String ssapJSON;
	Long householdId;
	String esignDate;
	String esignFirstName;
	String esignMiddleName;
	String esignLastName;
	Long userId;
	String userName;
	String addTaxDependentReason;
	String firstName;
	String lastName;
	long coverageYear;
	String caseNumber;
	Boolean isRenewalFlow = true;
	
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getSsapJSON() {
		return ssapJSON;
	}
	public void setSsapJSON(String ssapJSON) {
		this.ssapJSON = ssapJSON;
	}
	public Long getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(Long householdId) {
		this.householdId = householdId;
	}
	public List<RequestParamDTO> getEvents() {
		return events;
	}
	public void setEvents(List<RequestParamDTO> events) {
		this.events = events;
	}
	public String getEsignDate() {
		return esignDate;
	}
	public void setEsignDate(String esignDate) {
		this.esignDate = esignDate;
	}

	public String getEsignFirstName() {
		return esignFirstName;
	}

	public void setEsignFirstName(String esignFirstName) {
		this.esignFirstName = esignFirstName;
	}

	public String getEsignMiddleName() {
		return esignMiddleName;
	}

	public void setEsignMiddleName(String esignMiddleName) {
		this.esignMiddleName = esignMiddleName;
	}

	public String getEsignLastName() {
		return esignLastName;
	}

	public void setEsignLastName(String esignLastName) {
		this.esignLastName = esignLastName;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAddTaxDependentReason() {
		return addTaxDependentReason;
	}
	public void setAddTaxDependentReason(String addTaxDependentReason) {
		this.addTaxDependentReason = addTaxDependentReason;
	}
	public long getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Boolean getIsRenewalFlow() {
		return isRenewalFlow;
	}
	public void setIsRenewalFlow(Boolean isRenewalFlow) {
		this.isRenewalFlow = isRenewalFlow;
	}	
	
	

}
