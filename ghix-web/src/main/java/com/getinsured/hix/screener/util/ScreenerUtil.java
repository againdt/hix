/**
 * 
 */
package com.getinsured.hix.screener.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.TSLocalTime;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.affiliate.model.AffiliateClick;
import com.getinsured.hix.eligibility.prescreen.util.PrescreenConstants;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.EligLeadRequest;
import com.getinsured.hix.model.estimator.mini.Member;
import com.getinsured.hix.model.estimator.mini.MemberRelationships;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorRequest;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorResponse;
import com.getinsured.hix.model.estimator.mini.PreEligibilityResults;
import com.getinsured.hix.model.estimator.mini.StateChipMedicaid;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.config.PhixFlowConfiguration.PhixFlowConfigurationEnum;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.ui.model.eligibility.prescreen.PhixMember;
import com.getinsured.hix.ui.model.eligibility.prescreen.PhixRequest;

/**
 * Utility class for PHIX Eligibility
 * @author Nikhil Talreja
 * @since 20 August, 2013 
 * 
 */
@Component("screenerUtil")
public class ScreenerUtil {
	
	private static final Logger LOGGER = Logger.getLogger(ScreenerUtil.class);

	@Autowired private HIXHTTPClient hIXHTTPClient;
	@Autowired private RestTemplate restTemplate;

	//Constants 
	public static final int NAME_LEN = 100;
	public static final int EMAIL_LEN = 250;
	public static final int PHONE_LEN = 10;
	
	public static final String LEAD_ID = "leadId";
	public static final String CLICK_ID = "clickId";
	public static final String EDIT_MODE = "edit";
	public static final String FAILURE = "failure";
	public static final String SUCCESS = "success";
	public static final String PHIX_REQUEST = "phixRequest";
	public static final String STATE_NAME = "stateName"; 
	public static final String AFF_CUSTOM_FIELD_1 = "affFlowCustomField1";
	public static final String WIDGET_RESPONSE = "widgetResponse";
	public static final String EFFECTIVE_START_DATE = "EFFECTIVE_START_DATE";
	
	private static final int FIFTEEN = 15;
	private static final int FOUR = 4;
	private static final int SIXTY = 60;
	private static final int THOUSAND = 1000;
	private static final int NO_OF_HOURS_IN_DAY = 24;
	private static final int LEAP_YEAR_DAYS = 366;
	private static final int NON_LEAP_YEAR_DAYS = 365;
	private static final String DATE_FORMAT_AFF_CLICK = "mmddyyyy";
	
	protected static final String NO = "N";
	protected static final String YES = "Y";
	
	public static final Map<String, String> GI_SUPPORTED_STATE = new HashMap<String, String>();
	static{
		GI_SUPPORTED_STATE.put("AL", YES);
		GI_SUPPORTED_STATE.put("AK", YES);
		GI_SUPPORTED_STATE.put("AZ", YES);
		GI_SUPPORTED_STATE.put("AR", YES);
		GI_SUPPORTED_STATE.put("CA", YES);
		GI_SUPPORTED_STATE.put("CO", YES);
		GI_SUPPORTED_STATE.put("CT", YES);
		GI_SUPPORTED_STATE.put("DE", YES);
		GI_SUPPORTED_STATE.put("DC", YES);
		GI_SUPPORTED_STATE.put("FL", YES);
		GI_SUPPORTED_STATE.put("GA", YES);
		GI_SUPPORTED_STATE.put("HI", YES);
		GI_SUPPORTED_STATE.put("ID", YES);
		GI_SUPPORTED_STATE.put("IL", YES);
		GI_SUPPORTED_STATE.put("IN", YES);
		GI_SUPPORTED_STATE.put("IA", YES);
		GI_SUPPORTED_STATE.put("KS", YES);
		GI_SUPPORTED_STATE.put("KY", YES);
		GI_SUPPORTED_STATE.put("LA", YES);
		GI_SUPPORTED_STATE.put("ME", YES);
		GI_SUPPORTED_STATE.put("MD", YES);
		GI_SUPPORTED_STATE.put("MA", YES);
		GI_SUPPORTED_STATE.put("MI", YES);
		GI_SUPPORTED_STATE.put("MN", YES);
		GI_SUPPORTED_STATE.put("MS", YES);
		GI_SUPPORTED_STATE.put("MO", YES);
		GI_SUPPORTED_STATE.put("MT", YES);
		GI_SUPPORTED_STATE.put("NE", YES);
		GI_SUPPORTED_STATE.put("NV", YES);
		GI_SUPPORTED_STATE.put("NH", YES);
		GI_SUPPORTED_STATE.put("NJ", YES);
		GI_SUPPORTED_STATE.put("NM", YES);
		GI_SUPPORTED_STATE.put("NY", YES);
		GI_SUPPORTED_STATE.put("NC", YES);
		GI_SUPPORTED_STATE.put("ND", YES);
		GI_SUPPORTED_STATE.put("OH", YES);
		GI_SUPPORTED_STATE.put("OK", YES);
		GI_SUPPORTED_STATE.put("OR", YES);
		GI_SUPPORTED_STATE.put("PA", YES);
		GI_SUPPORTED_STATE.put("RI", YES);
		GI_SUPPORTED_STATE.put("SC", YES);
		GI_SUPPORTED_STATE.put("SD", YES);
		GI_SUPPORTED_STATE.put("TN", YES);
		GI_SUPPORTED_STATE.put("TX", YES);
		GI_SUPPORTED_STATE.put("UT", YES);
		GI_SUPPORTED_STATE.put("VT", YES);
		GI_SUPPORTED_STATE.put("VA", YES);
		GI_SUPPORTED_STATE.put("WA", YES);
		GI_SUPPORTED_STATE.put("WV", YES);
		GI_SUPPORTED_STATE.put("WI", YES);
		GI_SUPPORTED_STATE.put("WY", YES);
	}
	
	//public static final Map<String, Map<String,String>> CALL_US_INFO = new HashMap<String, Map<String,String>>();
	public static final String SUBSIDIZED = "_SUBSIDIZED";
	public static final String UNSUBSIDIZED = "_UNSUBSIDIZED";
	public static final String ALL_PUBLIC_PROGRAM = "_ALL_PUBLIC";
	
	//For PHIX1
	public static final String FFM = "FFM";
	//public static final String LEGACY_SHOPPING = "LEGACY";
	//public static final String CALL_AN_AGENT = "CALL";
	//public static final String PUBLIC_PROGRAMS = "PUBLIC";
	//public static final String SBE = "SBE";
	
	public static final int MIN_FAMILY_SIZE = 1;
	public static final int MAX_FAMILY_SIZE = 7;
	private static final int ADULT_AGE = 18;

	/*static{
		Map<String,String> values = new HashMap<String, String>();
		
		//Result 1 - 8 states
		values.put(SUBSIDIZED, CALL_AN_AGENT);
		values.put(UNSUBSIDIZED, LEGACY_SHOPPING);
		values.put(ALL_PUBLIC_PROGRAM, SBE);
		CALL_US_INFO.put("CA", values);
		CALL_US_INFO.put("CO", values);
		CALL_US_INFO.put("DC", values);
		CALL_US_INFO.put("KY", values);
		CALL_US_INFO.put("MD", values);
		CALL_US_INFO.put("MN", values);
		CALL_US_INFO.put("NV", values);
		CALL_US_INFO.put("OR", values);
		
		//Result 2 - 4 States
		values = new HashMap<String, String>();
		values.put(SUBSIDIZED, PUBLIC_PROGRAMS);
		values.put(UNSUBSIDIZED, LEGACY_SHOPPING);
		values.put(ALL_PUBLIC_PROGRAM, PUBLIC_PROGRAMS);
		CALL_US_INFO.put("NJ", values);
		CALL_US_INFO.put("SD", values);
		CALL_US_INFO.put("WV", values);
		CALL_US_INFO.put("WY", values);
		
		//Result 3 - 32 states
		values = new HashMap<String, String>();
		values.put(SUBSIDIZED, FFM);
		values.put(UNSUBSIDIZED, LEGACY_SHOPPING);
		values.put(ALL_PUBLIC_PROGRAM, PUBLIC_PROGRAMS);
		CALL_US_INFO.put("AL", values);
		CALL_US_INFO.put("AK", values);
		CALL_US_INFO.put("AR", values);
		CALL_US_INFO.put("AZ", values);
		CALL_US_INFO.put("DE", values);
		CALL_US_INFO.put("FL", values);
		CALL_US_INFO.put("GA", values);
		CALL_US_INFO.put("ID", values);
		CALL_US_INFO.put("IL", values);
		CALL_US_INFO.put("IN", values);
		CALL_US_INFO.put("IA", values);
		CALL_US_INFO.put("KS", values);
		CALL_US_INFO.put("LA", values);
		CALL_US_INFO.put("MI", values);
		CALL_US_INFO.put("MS", values);
		CALL_US_INFO.put("MO", values);
		CALL_US_INFO.put("MT", values);
		CALL_US_INFO.put("NE", values);
		CALL_US_INFO.put("NH", values);
		CALL_US_INFO.put("NM", values);
		CALL_US_INFO.put("NC", values);
		CALL_US_INFO.put("ND", values);
		CALL_US_INFO.put("OH", values);
		CALL_US_INFO.put("OK", values);
		CALL_US_INFO.put("PA", values);
		CALL_US_INFO.put("SC", values);
		CALL_US_INFO.put("TN", values);
		CALL_US_INFO.put("TX", values);
		CALL_US_INFO.put("UT", values);
		CALL_US_INFO.put("VA", values);
		CALL_US_INFO.put("WI", values);
		CALL_US_INFO.put("ME", values);
		
		//Result 4 - 3 states
		values = new HashMap<String, String>();
		values.put(SUBSIDIZED, SBE);
		values.put(UNSUBSIDIZED, LEGACY_SHOPPING);
		values.put(ALL_PUBLIC_PROGRAM, SBE);
		CALL_US_INFO.put("CT", values);
		CALL_US_INFO.put("HI", values);
		CALL_US_INFO.put("WA", values);
		
		//Result 5 - 4 states
		values = new HashMap<String, String>();
		values.put(SUBSIDIZED, SBE);
		values.put(UNSUBSIDIZED, SBE);
		values.put(ALL_PUBLIC_PROGRAM, SBE);
		CALL_US_INFO.put("MA", values);
		CALL_US_INFO.put("NY", values);
		CALL_US_INFO.put("RI", values);
		CALL_US_INFO.put("VT", values);

	}*/
	
	/**
	 * Checks if click Id is present in session
	 * 
	 * @author - Nikhil Talreja
	 * @since 22 October, 2013
	 */
	public long checkClickIdInSession(HttpServletRequest request){
		
		long clickIdFromSession = 0;
		if((request.getSession().getAttribute(LEAD_ID) == null
				|| !StringUtils.isNumeric(request.getSession()
						.getAttribute(LEAD_ID).toString())) && request.getSession().getAttribute(CLICK_ID) != null
				&& StringUtils.isNumeric(request.getSession().getAttribute(CLICK_ID).toString())){
			clickIdFromSession = (new Long(request.getSession().getAttribute(CLICK_ID).toString()));
			if(clickIdFromSession > 0){
				LOGGER.debug("Using click Id from session " + clickIdFromSession);
			}
		}
		request.getSession().setAttribute(CLICK_ID, clickIdFromSession);
		return clickIdFromSession;
	}
	
	/**
	 * Checks if lead Id is present in session
	 * 
	 * @author - Nikhil Talreja
	 * @since 22 October, 2013
	 */
	public long checkLeadIdInSession(HttpServletRequest request){
		
		long leadIdFromSession = 0;
		if (request.getSession().getAttribute(LEAD_ID) != null
				&& StringUtils.isNumeric(request.getSession()
						.getAttribute(LEAD_ID).toString())) {
			leadIdFromSession = (new Long(request.getSession().getAttribute(LEAD_ID).toString()));
			if (leadIdFromSession > 0) {
				LOGGER.debug("Using Lead Id from session " + leadIdFromSession);
			}
		}
		request.getSession().setAttribute(LEAD_ID, leadIdFromSession);
		return leadIdFromSession;
	}
	
	/**
	 * Calls the estimator API
	 * 
	 * @author - Nikhil Talreja
	 * @throws IOException  
	 * @throws URISyntaxException 
	 * @since 20 August, 2013
	 */
	public MiniEstimatorResponse callEstimatorApi(PhixRequest eligibilityHomeRequest,HttpServletRequest httpRequest) throws IOException, URISyntaxException{
		
		if(eligibilityHomeRequest == null){
			LOGGER.warn("EligibilityHomeRequest object was null");
			throw new IllegalArgumentException("EligibilityHomeRequest object was null");
		}
		
		MiniEstimatorRequest request = createEstimatorRequest(eligibilityHomeRequest,httpRequest);
		LOGGER.info("Calling Estimator API");
		
		ObjectMapper mapper = new ObjectMapper();
		String response = hIXHTTPClient.getPOSTData(GhixEndPoints.PhixEndPoints.ESTIMATOR_API, mapper.writeValueAsString(request), PrescreenConstants.CONTENT_TYPE);
		return mapper.readValue(response, MiniEstimatorResponse.class);
	}

	/**
	 * Creates a request for Mini Estimator API from EligibilityHomeRequest
	 * 
	 * @author - Nikhil Talreja
	 * @since 20 August, 2013
	 */
	private MiniEstimatorRequest createEstimatorRequest(
			PhixRequest phixRequest,HttpServletRequest httpRequest) {
		
		LOGGER.info("Creating request for Estimator API");
		MiniEstimatorRequest request = new MiniEstimatorRequest();
		request.setZipCode(phixRequest.getZipCode());
		request.setCountyCode(phixRequest.getCountyCode());
		request.setFamilySize(phixRequest.getFamilySize());
		request.setHhIncome(phixRequest.getHouseholdIncome());
		request.setDocVisitFrequency(phixRequest.getDocVisitFrequency());
		request.setNoOfPrescriptions(phixRequest.getNoOfPrescriptions());
		request.setBenefits(phixRequest.getBenefits());
		request.setAffiliateId(phixRequest.getAffiliateId());
		request.setFlowId(phixRequest.getFlowId());
		request.setAffiliateHouseholdId(phixRequest.getAffiliateHouseholdId());
		request.setSkipEligibilityCheck(phixRequest.isSkipEligibility());
		request.setEffectiveStartDate(phixRequest.getCoverageStartDate());
		request.setCoverageYear(phixRequest.getCoverageYear());
		request.setCsrLevel(phixRequest.getCsrLevel());
		
		addAffiliateFieldsToEstimatorRequest(request,httpRequest);
		
		//Skip affiliate validation if a direct request is made to GI
		LOGGER.info("Affililate Key validation will be skipped for this request");
		request.setSkipAffiliateValidation(true);
		
		if(httpRequest.getSession().getAttribute(LEAD_ID) != null
				&& StringUtils.isNumeric(httpRequest.getSession().getAttribute(LEAD_ID).toString())){
			request.setLeadId(new Long(httpRequest.getSession().getAttribute(LEAD_ID).toString()));
		}
		
		if(httpRequest.getSession().getAttribute(CLICK_ID) != null
				&& StringUtils.isNumeric(httpRequest.getSession().getAttribute(CLICK_ID).toString())){
			request.setClickId(new Long(httpRequest.getSession().getAttribute(CLICK_ID).toString()));
		}
		
		if(httpRequest.getSession().getAttribute(EFFECTIVE_START_DATE) != null 
				&& StringUtils.isNotBlank(httpRequest.getSession().getAttribute(EFFECTIVE_START_DATE).toString())){
			request.setEffectiveStartDate(httpRequest.getSession().getAttribute(EFFECTIVE_START_DATE).toString());
		}
		
		request.setEmail(phixRequest.getEmail());
		request.setPhone(phixRequest.getPhone());
		
		if(phixRequest.getMembers() != null && phixRequest.getMembers().isEmpty()){
			request.setMembers(null);
		}
		else{
			List<Member> members = new ArrayList<Member>();
			int memberNumber = 1;
			for(PhixMember homeMember : phixRequest.getMembers()){
				
				Member member = new Member();
				if(homeMember.getRelationshipToPrimary() != null){
					member.setRelationshipToPrimary(homeMember.getRelationshipToPrimary());
				}
				member.setDateOfBirth(homeMember.getDateOfBirth());
				member.setIsTobaccoUser(homeMember.getIsTobaccoUser());
				if(StringUtils.equalsIgnoreCase(homeMember.getIsNativeAmerican(), YES)){
					member.setIsNativeAmerican(YES);
				}
				if(StringUtils.equalsIgnoreCase(homeMember.getIsPregnant(), YES)){
					member.setIsPregnant(YES);
				}
				member.setMemberNumber(memberNumber++);
				if(StringUtils.equalsIgnoreCase(homeMember.getIsSeekingCoverage(), NO)){
					member.setIsSeekingCoverage(NO);	
				}
				members.add(member);
			}
			request.setMembers(members);
		}
		
		//HIX-64221 Support for 2016 Issuer Plan Preview
		if(!phixRequest.getShowIncomeSection()){
			request.setPlanVerification(true);
		}
		
		return request;
	}
	
	/**
	 * Adds affiliate related fields to estimator request
	 * 
	 * @author - Nikhil Talreja
	 * @since 03 September, 2013
	 */
	private void addAffiliateFieldsToEstimatorRequest(MiniEstimatorRequest request,HttpServletRequest httpRequest){
		
		if(httpRequest.getSession().getAttribute("affiliateId") != null
				&& StringUtils.isNumeric(httpRequest.getSession().getAttribute("affiliateId").toString())){
			request.setAffiliateId(new Long(httpRequest.getSession().getAttribute("affiliateId").toString()));
		}
		if(httpRequest.getSession().getAttribute("apiKey") != null){
			request.setApiKey(httpRequest.getSession().getAttribute("apiKey").toString());
		}
		if(httpRequest.getSession().getAttribute("affiliateHouseholdId") != null){
			request.setAffiliateHouseholdId((httpRequest.getSession().getAttribute("affiliateHouseholdId").toString()));
		}
	}
	
	/**
	 * This method checks if any member is eligible for CHIP/Medicare
	 * 
	 * @author - Nikhil Talreja
	 * @since 22 August, 2013
	 * 
	 * @param - type - Type of eligiblity to be checked for
	 */
	public String findChipMedicareEligibility(List<Member> members,String type) {
		
		if(members != null && members.isEmpty()){
			return NO;
		}
		
		if(!CollectionUtils.isEmpty(members)){
		for(Member member : members){
			if("Y".equalsIgnoreCase(member.getIsSeekingCoverage()) && StringUtils.equalsIgnoreCase(member.getMemberEligibility(), type)){
				return YES;
			}
		}
		}
		
		return NO;
	}
	
	/**
	 * This method checks if any member is eligible for a type of subsidy and returns the count
	 * 
	 * @author - Nikhil Talreja
	 * @since 03 October, 2013
	 * 
	 */
	public int findNoOfMembersForEligibilityType(List<Member> members,String type) {
		
		if(members != null && members.isEmpty()){
			return 0;
		}
		
		int count = 0;
		for(Member member : members){
			if("Y".equalsIgnoreCase(member.getIsSeekingCoverage()) && StringUtils.equalsIgnoreCase(member.getMemberEligibility(), type)){
				count++;
			}
		}
		
		return count;
	}
	
	/**
	 * Calls the API to update ELIG_LEAD record with contact details
	 * 
	 * @author - Nikhil Talreja
	 * @throws IOException 
	 * @throws URISyntaxException 
	 * @since 29 August, 2013
	 */
	public String updateContactDetailsInEligLeadRecord(EligLeadRequest request) throws IOException, URISyntaxException{
		ObjectMapper mapper = new ObjectMapper();
		return hIXHTTPClient.getPOSTData(GhixEndPoints.PhixEndPoints.UPDATE_CONTACT_DETAILS_FOR_LEAD, mapper.writeValueAsString(request), PrescreenConstants.CONTENT_TYPE);
	}
	
	/**
	 * Calls the API to update stage for ELIG_LEAD record
	 * 
	 * @author - Nikhil Talreja
	 * @throws IOException 
	 * @throws URISyntaxException 
	 * @since 03 September, 2013
	 */
	public String updateOtherInputsInEligLeadRecord(EligLeadRequest request) throws IOException, URISyntaxException{
		ObjectMapper mapper = new ObjectMapper();
		return hIXHTTPClient.getPOSTData(GhixEndPoints.PhixEndPoints.UPDATE_OTHER_INPUTS_FOR_LEAD, mapper.writeValueAsString(request), PrescreenConstants.CONTENT_TYPE);
	}
	
	/**
	 * Calls the API to update flow type for ELIG_LEAD record
	 * 
	 * @author - Nikhil Talreja
	 * @throws IOException 
	 * @throws URISyntaxException 
	 * @since 22 November, 2013
	 */
	public String updateFlowTypeInEligLeadRecord(EligLeadRequest request) throws IOException, URISyntaxException{
		ObjectMapper mapper = new ObjectMapper();
		return hIXHTTPClient.getPOSTData(GhixEndPoints.PhixEndPoints.UPDATE_FLOW_FOR_LEAD, mapper.writeValueAsString(request), PrescreenConstants.CONTENT_TYPE);
	}
	
	/**
	 * Method to find a record from AFF_CLICK table based on click Id
	 *
	 * @author Nikhil Talreja
	 * @since 30 August, 2013
	 * 
	 * @param long clickId - click id for which record needs to be found 
	 * @return Affiliate - Affiliate object
	 */
	public AffiliateClick fetchClickRecord(long clickId){
		
		LOGGER.info("Fetching AFF_CLICK record for click Id " + clickId);
		ResponseEntity<AffiliateClick> affiliateResponseEntity = null;
		
		try{
			String url = String.format(GhixEndPoints.AffiliateServiceEndPoints.GET_AFFILIATE_CLICK, clickId);
			LOGGER.debug("Affiliate click URL : "+url);
			
			affiliateResponseEntity = restTemplate.getForEntity(url, AffiliateClick.class);
			return affiliateResponseEntity.getBody();
		}
		catch(Exception e){
			LOGGER.error("Exception occured while fetching record from AFF_CLICK table",e);
			return null;
		}
	}
	
	/**
	 * Method to find a record from ELIG_LEAD table based on lead Id
	 *
	 * @author Nikhil Talreja
	 * @since 30 August, 2013
	 * 
	 * @param long leadId - lead id for which record needs to be found 
	 * @return EligLead - EligLead object
	 */
	public EligLead fetchEligLeadRecord(long leadId){
		
		try{
			ObjectMapper mapper = new ObjectMapper();
			String response = hIXHTTPClient.getPOSTData(GhixEndPoints.PhixEndPoints.GET_LEAD, "\""+leadId+"\"", PrescreenConstants.CONTENT_TYPE);
			return mapper.readValue(response, EligLead.class);
		}
		catch(Exception e){
			LOGGER.error("Exception occured while fetching record from ELIG_LEAD table",e);
			return null;
		}
	}
	
	/**
	 * Method to create a record in ELIG_LEAD table based
	 *
	 * @author Nikhil Talreja
	 * @since 04 September, 2013
	 * 
	 * @param EligLead record - record to be saved 
	 * @return EligLead - EligLead object
	 */
	public EligLead createEligLeadRecord(EligLead record){
		
		try{
			ObjectMapper mapper = new ObjectMapper();
			//LOGGER.debug("Record " + record);
			String response = hIXHTTPClient.getPOSTData(GhixEndPoints.PhixEndPoints.SAVE_LEAD, mapper.writeValueAsString(record), PrescreenConstants.CONTENT_TYPE);
			return mapper.readValue(response, EligLead.class);
		}
		catch(Exception e){
			LOGGER.error("Exception occured while fetching record from ELIG_LEAD table",e);
			return null;
		}
	}
	
	
	/**
	 * Method to create a record of EligLead type using PhixRequest
	 *
	 * @author Nikhil Talreja
	 * @since 04 September, 2013
	 * 
	 * @return EligLead - EligLead object
	 */
	public EligLead populateEligLeadRecord(PhixRequest phixRequest) {
		
		EligLead record = new EligLead();
		record.setZipCode(phixRequest.getZipCode());
		record.setCountyCode(phixRequest.getCountyCode());
		
		if(phixRequest.getMembers()!=null && phixRequest.getMembers().size() > 0){
			try{
				ObjectMapper mapper = new ObjectMapper();
				record.setMemberData(mapper.writeValueAsString(phixRequest.getMembers()));
			}
			catch (Exception e){
				LOGGER.error("Member data could not be saved ",e);
			}
		}
		record.setFamilySize(phixRequest.getFamilySize());
		record.setHouseholdIncome(phixRequest.getHouseholdIncome());
		record.setDocVisitFrequency(phixRequest.getDocVisitFrequency());
		record.setNoOfPrescriptions(phixRequest.getNoOfPrescriptions());
		record.setBenefits(phixRequest.getBenefits());
		
		return record;
	}
	
	/**
	 * Creates a Phix request from ELIG_LEAD record
	 *
	 * @author Nikhil Talreja
	 * @param year 
	 * @since 30 August, 2013
	 * 
	 * @param EligLead eligLead - Record used to create Phix Request 
	 * @return PhixRequest
	 */
	public PhixRequest createPhixRequest(EligLead eligLead, int year){
		
		PhixRequest request = null;
		boolean isRelationsNull = false;
		if(eligLead != null){
			request = new PhixRequest();
			PreEligibilityResults results = null;
			if(year != 0){
				results = getPreEligResultForCoverageYear(eligLead, year);
				if(results != null){
					request.setZipCode(results.getZipCode());
					request.setCountyCode(results.getCountyCode());
					request.setFamilySize(results.getFamilySize());
					request.setHouseholdIncome(results.getHouseholdIncome());
				}
			}
			else{
				request.setZipCode(eligLead.getZipCode());
				request.setCountyCode(eligLead.getCountyCode());
				request.setFamilySize(eligLead.getFamilySize());
				request.setHouseholdIncome(eligLead.getHouseholdIncome());
			}
			request.setDocVisitFrequency(eligLead.getDocVisitFrequency());
			request.setNoOfPrescriptions(eligLead.getNoOfPrescriptions());
			request.setBenefits(eligLead.getBenefits());
			request.setName(eligLead.getName());
			request.setEmail(eligLead.getEmailAddress());
			request.setPhone(eligLead.getPhoneNumber());
			request.setIsOkToCall(eligLead.getIsOkToCall());
			setAffiliateFields(request, eligLead);
			
			//HIX-67559 : Provide consumer the ability to select coverage year during anonymous pre-screener and shopping.
			Date coverageStartDate = eligLead.getCoverageStartDate();
			if(results != null){
				coverageStartDate = results.getCoverageStartDate();
				request.setCoverageYear(year);
			}
			else if (coverageStartDate != null) {
				Calendar calendar = TSCalendar.getInstance();
				calendar.setTime(coverageStartDate); 
				int coverageStartYear = calendar.get(Calendar.YEAR);
				request.setCoverageYear(coverageStartYear);
			}
			
			if(coverageStartDate != null) {
				SimpleDateFormat df = new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT);
				request.setCoverageStartDate(df.format(coverageStartDate));
			}
			
			try{
				String memberData = eligLead.getMemberData();
				if(results != null){
					memberData = results.getMemberData();
				}
				if(StringUtils.isNotBlank(memberData)){
					ObjectMapper mapper = new ObjectMapper();
					List<Member> members = mapper.readValue(memberData, new TypeReference<List<Member>>() { });
					LOGGER.debug("Members from lead are "+members.toString());
					for(Member member : members){
						PhixMember phixMember = new PhixMember();
						phixMember.setRelationshipToPrimary(member.getRelationshipToPrimary());
						phixMember.setDateOfBirth(member.getDateOfBirth());
						phixMember.setIsTobaccoUser(member.getIsTobaccoUser());
						phixMember.setIsSeekingCoverage(member.getIsSeekingCoverage());
						phixMember.setIsPregnant(member.getIsPregnant());
						phixMember.setIsNativeAmerican(member.getIsNativeAmerican());
						if(phixMember.getRelationshipToPrimary()==null){
							isRelationsNull = true;
							phixMember.setRelationshipToPrimary(MemberRelationships.SELF);
						}
						request.getMembers().add(phixMember);
					}
					//FIXME: Remove logic based prepoulation
					if(request.getMembers().size()>1 && isRelationsNull){
						PhixMember tempPhixMember1 = new PhixMember();
						PhixMember tempPhixMember2 = new PhixMember();
						for(int  i=0; i<request.getMembers().size()-1;i++){
							for(int  j=0; j<request.getMembers().size()-1;j++){
								if(getAgeFromDob(request.getMembers().get(j).getDateOfBirth(),GhixConstants.REQUIRED_DATE_FORMAT) <
										getAgeFromDob(request.getMembers().get(j+1).getDateOfBirth(),GhixConstants.REQUIRED_DATE_FORMAT)){
									tempPhixMember1=request.getMembers().get(j);
									tempPhixMember2=request.getMembers().get(j+1);
									request.getMembers().set(j, tempPhixMember2);
									request.getMembers().set(j+1, tempPhixMember1);
							}
							}
						}
						for(int  i=1; i<request.getMembers().size();i++){
							request.getMembers().get(i).setRelationshipToPrimary(MemberRelationships.CHILD);
						}
						if(getAgeFromDob(request.getMembers().get(1).getDateOfBirth(), GhixConstants.REQUIRED_DATE_FORMAT) > ADULT_AGE){
							request.getMembers().get(1).setRelationshipToPrimary(MemberRelationships.SPOUSE);
						}
					}
				}
			} catch(Exception e){
				LOGGER.error("Error fetching member data from lead table",e);
			}
		}
		
		return request;
		
	}
	
	/**
	 * Gets the pre eligibility results for a lead for a particular year
	 * 
	 * @author Nikhil Talreja
	 * 
	 * @param lead
	 * @param newResult
	 * @param coverageYear
	 * @return
	 */
	private PreEligibilityResults getPreEligResultForCoverageYear(EligLead lead, int coverageYear){
		
		if(lead != null){
			List<PreEligibilityResults> results = lead.getPreEligibilityResults();
			if(results != null && !results.isEmpty()){
				for(PreEligibilityResults result: results){
					if(result.getCoverageYear() == coverageYear){
						return result;
					}
				}
			}
		}
		
		return null;
	}
	
	public boolean hasCutOffDatePassed() throws ParseException{
		
		String coverageYearOptionCutOffDate = DynamicPropertiesUtil
				.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PRESCREENER_SHOW_CURRENT_YEAR);

		if (StringUtils.isNotBlank(coverageYearOptionCutOffDate)) {
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			LocalDate todaysDate = TSLocalTime.getLocalDateInstance();
			LocalDate cutOffDate = new DateTime(formatter.parseObject(coverageYearOptionCutOffDate)).toLocalDate();
			if (todaysDate.isAfter(cutOffDate)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Adds affiliate related fields to Phix Request
	 * Part of HIX-30280
	 *
	 * @author Nikhil Talreja
	 * @since 27 February, 2014
	 * 
	 * @param EligLead eligLead - Record used to create Phix Request 
	 * @return PhixRequest
	 */
	private void setAffiliateFields(PhixRequest request, EligLead eligLead){
		request.setAffiliateId(eligLead.getAffiliateId());
		request.setFlowId(eligLead.getFlowId());
		request.setAffiliateHouseholdId(eligLead.getExtAffHouseholdId());
	}
	
	/**
	 * Creates a Phix request from AFF_CLICK record
	 *
	 * @author Nikhil Talreja
	 * @since 30 August, 2013
	 * 
	 * @param EligLead eligLead - Record used to create Phix Request 
	 * @return PhixRequest
	 */
	public PhixRequest createPhixRequest(AffiliateClick clickRecord){
		
		PhixRequest request = null;
		
		if(clickRecord != null){
			request = new PhixRequest();
			request.setZipCode(clickRecord.getZip());
			request.setCountyCode(clickRecord.getCounty());
			if(clickRecord.getFamilySize() != null){
				request.setFamilySize(clickRecord.getFamilySize().intValue());
			}
			request.setHouseholdIncome(clickRecord.getAnnualHouseholdIncome());
			request.setEmail(clickRecord.getEmail());
			request.setPhone(clickRecord.getPhone());
			try{
				if(StringUtils.isNotBlank(clickRecord.getMemberData())){
					ObjectMapper mapper = new ObjectMapper();
					List<Member> members = mapper.readValue(clickRecord.getMemberData(), new TypeReference<List<Member>>() { });
					LOGGER.debug("Members from aff click are "+members.toString());
					for(Member member : members){
						PhixMember phixMember = new PhixMember();
						phixMember.setDateOfBirth(validateDob(member.getDateOfBirth()));
						phixMember.setIsTobaccoUser(member.getIsTobaccoUser());
						phixMember.setIsSeekingCoverage(member.getIsSeekingCoverage());
						if(phixMember.getRelationshipToPrimary() == null){
							phixMember.setRelationshipToPrimary(MemberRelationships.SELF);
						}
						request.getMembers().add(phixMember);
					}
					
					if(request.getMembers().size() > 1){
						PhixMember tempPhixMember1 = new PhixMember();
						PhixMember tempPhixMember2 = new PhixMember();
						for(int  i=0; i<request.getMembers().size()-1;i++){
							for(int  j=0; j<request.getMembers().size()-1;j++){
								if(getAgeFromDob(request.getMembers().get(j).getDateOfBirth(),DATE_FORMAT_AFF_CLICK) <
										getAgeFromDob(request.getMembers().get(j+1).getDateOfBirth(),DATE_FORMAT_AFF_CLICK)){
									tempPhixMember1=request.getMembers().get(j);
									tempPhixMember2=request.getMembers().get(j+1);
									request.getMembers().set(j, tempPhixMember2);
									request.getMembers().set(j+1, tempPhixMember1);
								}
							}
						}
						for(int  i=1; i<request.getMembers().size();i++){
							request.getMembers().get(i).setRelationshipToPrimary(MemberRelationships.CHILD);
						}
						if(getAgeFromDob(request.getMembers().get(1).getDateOfBirth(),DATE_FORMAT_AFF_CLICK) > ADULT_AGE){
							request.getMembers().get(1).setRelationshipToPrimary(MemberRelationships.SPOUSE);
						}
					}
				}
				/**
				 * Fix for HIX-29782
				 * 
				 * Comments by Assaf : No DOB is provided, only number of family members, in this case, 
				 * it will be better to ignore the # of family members and display 
				 * as if no info was provided about that
				 * 
				 */
				/*else if(StringUtils.isBlank(clickRecord.getMemberData()) 
						&& clickRecord.getFamilySize() > MIN_FAMILY_SIZE 
						&& clickRecord.getFamilySize() <= MAX_FAMILY_SIZE){
					for(int i=0;i<clickRecord.getFamilySize();i++){
						PhixMember phixMember = new PhixMember();
						phixMember.setIsSeekingCoverage("Y");
						request.getMembers().add(phixMember);
					}
				}*/
			} catch(Exception e){
				LOGGER.error("Error fetching member data from click table",e);
			}
			request.setAffiliateHouseholdId(clickRecord.getAffiliateHouseholdId());
			request.setAffiliateId(clickRecord.getAffiliateId());
			request.setFlowId(clickRecord.getAffiliateFlowId());
		}
		
		return request;
		
	}
	
	/**
	 * Method to find the Primary sales channel for a state from STATE_MEDICAID_CHIP table
	 *
	 * @author Nikhil Talreja
	 * @since 10 Septemeber, 2013
	 * 
	 */
	public StateChipMedicaid getSalesChannelForState(String stateCode) {

		try{
			String response = hIXHTTPClient.getPOSTData(GhixEndPoints.PhixEndPoints.GET_STATE_CHIP_MEDICAID_RECORD_BY_STATE, stateCode, PrescreenConstants.CONTENT_TYPE);
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(response, StateChipMedicaid.class);
		}
		catch(Exception e){
			LOGGER.error("Error fetching primary sales channel for state",e);
			return null;
		}
		
		
	}
	
	/**
	 * Method to validate contact details before updating ELIG_LEAD record
	 * This is used for pre launch sign up page
	 *
	 * @author Nikhil Talreja
	 * @since 23 Septemeber, 2013
	 * 
	 */
	public boolean validateContactDetails(long lead, HttpServletRequest httpRequest){
		
		String emailPattern = "^([A-Za-z0-9_\\-\\.])+\\@([A-Za-z0-9_\\-\\.])+\\.([A-Za-z]{2,4})";
		
		String leadName = httpRequest.getParameter("leadName");
		String leadEmail = httpRequest.getParameter("leadEmail");
		String leadPhone = httpRequest.getParameter("leadPhone");
		String isOkToCall = httpRequest.getParameter("isOkToCall");
		
		LOGGER.debug("Name " + leadName);
		LOGGER.debug("Email " + leadEmail);
		LOGGER.debug("Phone " + leadPhone);
		LOGGER.debug("Ok to call " + isOkToCall);
		
		if(lead <= 0){
			LOGGER.info("Lead Id is invalid");
			return false; 
		}
		else if (StringUtils.length(leadName) > NAME_LEN){
			LOGGER.info("Lead Name is too long. Max allowed characters are 100");
			return false; 
		}
		else if (StringUtils.length(leadEmail) > EMAIL_LEN){
			LOGGER.info("Email is too long. Max allowed characters are 250");
			return false; 
		}
		else if(StringUtils.isNotBlank(leadEmail) && !leadEmail.matches(emailPattern)){
			LOGGER.info("Invalid email Id");
			return false;
		}
		else if(StringUtils.isNotBlank(leadPhone) && 
				(StringUtils.length(leadPhone) != PHONE_LEN) ){
			LOGGER.info("Invalid phone number");
			return false;
		}
		
		return true;
	}
	
	/**
	 * Method fetches State based configuration for Call Us variable 
	 * used on PHIX results page.
	 * 
	 * Change is applicable to PHIX0.55 - HIX-19730
	 *
	 * @author Nikhil Talreja
	 * @since 07 October, 2013
	 * 
	 */
	public String getCallUsInfo(PhixRequest phixRequest,MiniEstimatorResponse response){
		
		LOGGER.debug("State Code " + phixRequest.getStateCode());
		String stateCode = phixRequest.getStateCode();
		String redirectionType = null;
		String redirectionFlow = null;
		if(StringUtils.isBlank(stateCode)){
			return FFM;
		}
		else{
			//All public programs - Entire household is eligible for one or more public programs
			if(StringUtils.equalsIgnoreCase(response.getMedicaidHousehold(), YES)
					|| StringUtils.equalsIgnoreCase(response.getChipHousehold(), YES)
					|| StringUtils.equalsIgnoreCase(response.getMedicareHousehold(),YES)
					|| phixRequest.getNoOfChip() + phixRequest.getNoOfMedicare() == CollectionUtils.size(phixRequest.getMembers())){
				redirectionType = ALL_PUBLIC_PROGRAM;
			}
			//Subsidized (APTC eligible is Y and APTC > 0)
			else if(StringUtils.equalsIgnoreCase(YES, response.getAptcEligible())){
				
				String aptcVal = StringUtils.replace(response.getAptcAmount(),"$", "");
				if(NumberUtils.isNumber(aptcVal) && new Double(aptcVal).doubleValue() > 0.0){
					//return values.get(SUBSIDIZED);
					redirectionType = SUBSIDIZED;
				}
				else if(!NumberUtils.isNumber(aptcVal) ){
					//return values.get(SUBSIDIZED);
					redirectionType = SUBSIDIZED;
				}
				else{
					//return values.get(UNSUBSIDIZED);
					redirectionType = UNSUBSIDIZED;
				}
			}	
			//Un-subsidized
			else{
				//return values.get(UNSUBSIDIZED);
				redirectionType = UNSUBSIDIZED;
			}
			
		}
		//Logic to catch invalid state codes
		try{
			redirectionFlow = DynamicPropertiesUtil.getPropertyValue(PhixFlowConfigurationEnum.valueOf(stateCode+redirectionType));
		}
		catch(IllegalArgumentException e){
			LOGGER.error("Invalid state code ", e);
			redirectionFlow = FFM;
		}
		LOGGER.debug("Re-direction type : " + redirectionType);
		LOGGER.debug("Re-direction flow : " + redirectionFlow);
		return redirectionFlow;
	}
	
	/**
	 * Method fetches State name for a state code
	 * used on PHIX results page.
	 * 
	 * Change is applicable to PHIX0.55 - HIX-19730
	 *
	 * @author Nikhil Talreja
	 * @since 07 October, 2013
	 * 
	 */
	public String getStateName(String stateCode){
		
		if(StringUtils.isEmpty(stateCode)){
			return null;
		}
		else if(stateCode.equalsIgnoreCase("DC")){
			return "District of Columbia";
		}
		else{
			return new StateHelper().getStateName(stateCode);
		}
		
	}
	
	/**
	 * Computes effective date for coverage start
	 * 
	 * Following logic is implemented as part of HIX-24255
	 * 
	 * 1. If current date is on or before 15th of a month, effective date is 1st of next month
	 * 2. If current date is after 15th of a month, effective date is 1st of next to next month
	 * 
	 * Exmaples:
	 * 1. Current Date - 12th July 2014 , Effective Date - 1st August 2014
	 * 2. Current Date - 19th July 2014, Effective Date - 1st September 2014
	 * 3. Current Date - 28th November 2013, Effective Date - 1st January 2014
	 * 4. Current Date - 25th December 2013, Effective Date - 1st February 2014
	 * 
	 * @author - Sunil Desu, Nikhil Talreja
	 * @since 28 November, 2013
	 */
	
	protected Calendar computeEffectiveDate(){
		
		Calendar effectiveDate = TSCalendar.getInstance();
		Calendar currentDate = TSCalendar.getInstance();
		currentDate.set(Calendar.HOUR_OF_DAY, 0);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);	

		//Current Date is on or before 15 of month
		if(currentDate.get(Calendar.DAY_OF_MONTH) <= FIFTEEN){	
			effectiveDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH) + 1);	
		}
		//Current Date is after 15 of month
		else{
			effectiveDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH) + 2);
		}
		
		effectiveDate.set(Calendar.DAY_OF_MONTH, 1);
		effectiveDate.set(Calendar.HOUR_OF_DAY, 0);
		effectiveDate.set(Calendar.MINUTE, 0);
		effectiveDate.set(Calendar.SECOND, 0);
		effectiveDate.set(Calendar.MILLISECOND, 0);	
			
		return effectiveDate;
	}
	
	/**
	 * Finds age from Date of birth
	 * 
	 *
	 * @author Sunil Desu, Nikhil Talreja
	 * @since 15 November, 2013
	 * 
	 */
	public double getAgeFromDob(String dob, String dateFormat){
		
		try{
			if(StringUtils.isBlank(dob) || StringUtils.isBlank(dateFormat)){
				return 0;
			}
	
			Calendar cal = computeEffectiveDate();
			long today = cal.getTimeInMillis();
		    Date dobDate = stringToDate(dob, dateFormat);
		    long birthDate = dobDate.getTime();
		    
		    long diff = today - birthDate;
		    double diffYears = 0;
		    long millisinYear = 0;
		    
		    if(cal.get(Calendar.YEAR) % FOUR == 0){
		    	millisinYear = (long)THOUSAND * SIXTY * SIXTY
		    	        * NO_OF_HOURS_IN_DAY * LEAP_YEAR_DAYS;
		    }
		    else{
		    	millisinYear = (long)THOUSAND * SIXTY * SIXTY
		    	        * NO_OF_HOURS_IN_DAY * NON_LEAP_YEAR_DAYS;
		    	
		    }
		    
		    diffYears =  (double)diff / millisinYear;
		    
		    return diffYears;
		}
		catch (Exception e){
			LOGGER.error("Error occured while calculating age",e);
			return 0;
		}

	}
	
	/**
	 * Method to validate DOB
	 *
	 * @author Nikhil Talreja
	 * @since 10 July, 2013
	 */
	
	public String validateDob(String dob){
		
		if(dob == null){
			return null;
		}
		else{
			try{
				Date dateOfBirth = stringToDate(dob, DATE_FORMAT_AFF_CLICK);
				if(dateOfBirth.compareTo(computeEffectiveDate().getTime()) > 0){
					return null;
				}
			}
			catch (Exception e){
				LOGGER.error("DOB is invalid");
				return null;
			}

		}
		
		return dob;
	}
	
	/**
	 * Performs strict conversion of String to Date
	 * 
	 * @author Nikhil Talreja
	 * @since 22 November, 2013
	 * 
	 */
	private Date stringToDate(String dateStr, String format) throws ParseException {
	    SimpleDateFormat dateFormat = new SimpleDateFormat(format);
	    dateFormat.setLenient(false);
	    Date convertedDate = new TSDate();
		convertedDate = dateFormat.parse(dateStr);
	    return convertedDate;
	}
	
	/**
	 * Creates response for eligibility widget which is added to session
	 * 
	 * @author Nikhil Talreja
	 * @since 01 December, 2013
	 * 
	 */
	public void createResponseForWidget(PhixRequest phixRequest, MiniEstimatorResponse estimatorResponse){
		
		phixRequest.setAptc(estimatorResponse.getAptcAmount());
		phixRequest.setAptcEligible(estimatorResponse.getAptcEligible());
		phixRequest.setSubsidyEligible(estimatorResponse.getSubsidyEligible());
		phixRequest.setAllPlansAvailable(estimatorResponse.getAllPlansAvailable());
		phixRequest.setPlanInfoAvailable(estimatorResponse.getPlanInfoAvailable());
		phixRequest.setExpectedMonthlyPremium(estimatorResponse.getExpectedMonthlyPremium());
		phixRequest.setSlspPremiumAfterAptc(estimatorResponse.getSlspMonthlyPremiumAfterAPTC());
		phixRequest.setSlspPremiumBeforeAptc(estimatorResponse.getSlspMonthlyPremiumBeforeAPTC());
		phixRequest.setLbpPremiumAfterAptc(estimatorResponse.getLbpMonthlyPremiumAfterAPTC());
		phixRequest.setLbpPremiumBeforeAptc(estimatorResponse.getLbpMonthlyPremiumBeforeAPTC());
		phixRequest.setLspPremiumAfterAptc(estimatorResponse.getLspMonthlyPremiumAfterAPTC());
		phixRequest.setLspPremiumBeforeAptc(estimatorResponse.getLspMonthlyPremiumBeforeAPTC());
		phixRequest.setMedicaidEligible(estimatorResponse.getMedicaidHousehold());
		phixRequest.setMedicareEligible(findChipMedicareEligibility(estimatorResponse.getMembers(),"Medicare"));
		phixRequest.setChipEligible(findChipMedicareEligibility(estimatorResponse.getMembers(),"CHIP"));
		phixRequest.setChipHousehold(estimatorResponse.getChipHousehold());
		phixRequest.setMedicareHousehold(estimatorResponse.getMedicareHousehold());
		phixRequest.setNoOfAptcCsr(findNoOfMembersForEligibilityType(estimatorResponse.getMembers(),"APTC")
				+ findNoOfMembersForEligibilityType(estimatorResponse.getMembers(),"APTC/CSR"));
		phixRequest.setNoOfChip(findNoOfMembersForEligibilityType(estimatorResponse.getMembers(), "CHIP"));
		phixRequest.setNoOfMedicare(findNoOfMembersForEligibilityType(estimatorResponse.getMembers(), "Medicare"));
		phixRequest.setCallUs(getCallUsInfo(phixRequest,estimatorResponse));
		phixRequest.setSbeWebsite(estimatorResponse.getHixURL());
		phixRequest.setStateName(getStateName(phixRequest.getStateCode()));
		phixRequest.setCsrLevel(estimatorResponse.getCsrLevelSilver());
		phixRequest.setStatus(estimatorResponse.getStatus());
	}
	
	/**
	 * Finds numeric APTC amount from APTC Amount string
	 * E.g. $360.5 returns 360.5
	 * JIRA: HIX-24323
	 * 
	 * @author - Nikhil Talreja
	 * @since 26 November, 2013
	 */
	public double determineAptcAmount(String aptcAmount) {
		
		try{
			return Double.parseDouble(aptcAmount.replaceAll("\\$", ""));
		}
		catch(Exception e){
			//In case of APTC = N/A or blank
			return -1;
		}
		
	}
	
	
	
	/**
	 * Finds if entire household is eligible for public programs
	 * JIRA: HIX-24323
	 * 
	 * @author - Nikhil Talreja
	 * @since 26 November, 2013
	 */
	public boolean determineAllPublicHousehold(PhixRequest response){
		
		if(StringUtils.equalsIgnoreCase(response.getMedicaidEligible(), NO)
				&& StringUtils.equalsIgnoreCase(response.getChipHousehold(), NO)
				&& StringUtils.equalsIgnoreCase(response.getMedicareHousehold(), NO)
				&& response.getNoOfChip() + response.getNoOfMedicare() != org.apache.commons.collections.CollectionUtils.size(response.getMembers())){
			return true;
		}
		
		return false;
	}

	
}

