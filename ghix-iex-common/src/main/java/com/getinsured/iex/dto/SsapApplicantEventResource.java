package com.getinsured.iex.dto;

import java.sql.Timestamp;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SsapApplicantEventResource extends ResourceSupport {
	
	@JsonInclude(Include.NON_NULL)
	private String ssapAplicationEvent;
	
	
	@JsonInclude(Include.NON_NULL)
	private String ssapApplicant;
	private Timestamp eventDate;
	private Timestamp reportStartDate;
	private Timestamp reportEndDate;
	private Timestamp enrollmentStartDate;
	private Timestamp enrollmentEndDate;
	private Timestamp coverageStartDate;
	private Timestamp coverageEndDate;
	public Object _links;
	private Integer createdBy;
	private Timestamp createdTimeStamp;
	private String eventPrecedenceIndicator;
	private String sepEvents;
	private Integer lastUpdatedBy;
	private Timestamp lastUpdateTimestamp;
	
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getSepEvents() {
		return sepEvents;
	}

	public void setSepEvents(String sepEvents) {
		this.sepEvents = sepEvents;
	}

	public String getEventPrecedenceIndicator() {
		return eventPrecedenceIndicator;
	}

	public void setEventPrecedenceIndicator(
			String eventPrecedenceIndicator) {
		this.eventPrecedenceIndicator = eventPrecedenceIndicator;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(Timestamp createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}
	
	public Timestamp getEventDate() {
		return eventDate;
	}

	public void setEventDate(Timestamp eventDate) {
		this.eventDate = eventDate;
	}

	public Timestamp getReportStartDate() {
		return reportStartDate;
	}

	public void setReportStartDate(Timestamp reportStartDate) {
		this.reportStartDate = reportStartDate;
	}

	public Timestamp getReportEndDate() {
		return reportEndDate;
	}

	public void setReportEndDate(Timestamp reportEndDate) {
		this.reportEndDate = reportEndDate;
	}

	public Timestamp getEnrollmentStartDate() {
		return enrollmentStartDate;
	}

	public void setEnrollmentStartDate(Timestamp enrollmentStartDate) {
		this.enrollmentStartDate = enrollmentStartDate;
	}

	public Timestamp getEnrollmentEndDate() {
		return enrollmentEndDate;
	}

	public void setEnrollmentEndDate(Timestamp enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}

	public Timestamp getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(Timestamp coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public Timestamp getCoverageEndDate() {
		return coverageEndDate;
	}

	public void setCoverageEndDate(Timestamp coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}

	public String getSsapAplicationEvent() {
		return ssapAplicationEvent;
	}

	public void setSsapAplicationEvent(String ssapAplicationEvent) {
		this.ssapAplicationEvent = ssapAplicationEvent;
	}

	public String getSsapApplicant() {
		return ssapApplicant;
	}

	public void setSsapApplicant(String ssapApplicant) {
		this.ssapApplicant = ssapApplicant;
	}

	public Object get_links() {
		return _links;
	}

	public void set_links(Object _links) {
		this._links = _links;
	}

}
