<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/employer-registration-util.tld" prefix="util"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/shop/jquery.cookie.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/backsafe.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/modal-address-util.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-template.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<script type="text/javascript">
/* function shiftbox(element,nextelement){
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength){
   		nextelement = document.getElementById(nextelement);
   		nextelement.focus();
	}
} */



jQuery.validator.addMethod("phonecheck", function(value, element, param) {
	 phone1 = $("#phone1").val(); 
  	 phone2 = $("#phone2").val(); 
  	 phone3 = $("#phone3").val(); 
  	var intPhone1= parseInt( phone1);
  	if( 
  			(phone1 == "" || phone2 == "" || phone3 == "")  || 
  			(isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3)) ||  
  			(phone1.length != 3 || phone2.length != 3 || phone3.length != 4)){ 
  		return false; 
  	}else if(intPhone1<100){
  		return false;
  	} 
  	
  	var phone = $("#phone1").val()+ "-" +$("#phone2").val()+ "-" +$("#phone3").val();
	$("#contactnumber").val(phone);

	var phonenumber = $("#phone1").val() + $("#phone2").val() + $("#phone3").val();
		
	var phoneno = /^\d{10}$/;
	  if(phonenumber.match(phoneno))      {
        return true;
      }
	return false;
});

</script>

<div class="gutter10">
	<div class="page-header">
		<h1><spring:message  code="label.setupcompany"/> <small><spring:message  code="label.ContactInfoh2"/></small></h1>
	</div>
	
	
	<div class="row-fluid">	
		<!-- add id for skip side bar -->	
		<div class="span3" id="sidebar">
			<util:nav pageName="Enter Contact Info" navStepNo="1"></util:nav>
			
		</div>
		<div class="span9" id="rightpanel">
			<form class="form-horizontal" id="frmcontact" name="frmcontact" action="contactinfo" method="POST">
				<df:csrfToken/>
				<input type="hidden" name="contactNumber" id="contactnumber" value="">
				<input type="hidden" name="role.id" id="role.id" value="${roleid}">
				<div class="header">
					 <h4><spring:message  code="label.ContactInfoh3"/></h4>
					 </div>
					<p><spring:message  code="label.ContactInfoh4"/> ${exchangeName} <spring:message  code="label.ContactInfoh5"/></p>
					<div class="profile">
						<div class="control-group">
							<label for="contactFirstName" class="control-label required"><spring:message  code="label.emplFirstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">		
								<input type="text" name="contactFirstName" id="contactFirstName" value="${empContactData.contactFirstName}"  class="input-xlarge">
								<div id="contactFirstName_error"></div>								
							</div>
						</div>
						<div class="control-group">
							<label for="contactLastName" class="control-label required"><spring:message  code="label.emplLastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="contactLastName" id="contactLastName" value="${empContactData.contactLastName}"  class="input-xlarge">
								<div id="contactLastName_error"></div>					
							</div>
						</div>
						<div class="control-group">
							<c:choose>
								<c:when test="${contactNumber != null && contactNumber != ''}">
									<c:set var="phone" value="${contactNumber}" />
									<c:set var="phoneParts" value="${fn:split(phone,'-')}" />
	
									<c:set var="phone1" value="${phoneParts[0]}" />
									<c:set var="phone2" value="${phoneParts[1]}" />
									<c:set var="phone3" value="${phoneParts[2]}" />
								</c:when>
							</c:choose>
							
							<label class="control-label required"><spring:message  code="label.employerphone"/> <span class="aria-hidden"><spring:message  code="label.firstPhone"/></span> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<%-- <label for="phone2" class="control-label required aria-hidden"><spring:message  code="label.employerphone"/> <spring:message  code="label.secondPhone"/> <spring:message  code="label.required"/> </label>
							<label for="phone3" class="control-label required aria-hidden"><spring:message  code="label.employerphone"/> <spring:message  code="label.thirdPhone"/> <spring:message  code="label.required"/> </label> --%>
							<div class="controls">
								<input type="text" name="phone1" id="phone1" size="3" maxlength="3" class="input-mini" value="${phone1}">
								<input type="text" name="phone2" id="phone2" size="3" maxlength="3" class="input-mini" value="${phone2}">
								<input type="text" name="phone3" id="phone3" size="4" maxlength="4" class="input-mini" value="${phone3}">
								<div id="phone3_error"></div>
							</div>
						</div>
						<%-- <div class="control-group">
							<label class="control-label required"><spring:message  code="label.employerprefcontact"/></label>
							<div class="span2">
								<label class="radio" for="communicationPref-Email"><input type="radio" checked="checked" name="communicationPref" id="communicationPref-Email" value="Email"><spring:message  code="label.emplConfirmEmail"/></label>
							</div>
							<div class="controls">
							<div class="span2">
								<label class="radio" for="communicationPref-Mail"><input type="radio" name="communicationPref" id="communicationPref-Mail" value="Mail"> <spring:message  code="label.emplConfirmMail"/></label>
							</div>
						</div>
						</div> --%>
						
						<div class="control-group">
							<label for="contactEmail" class="control-label required"><spring:message  code="label.emplEmail"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" value="${empContactData.contactEmail}" name="contactEmail" id="contactEmail" class="input-xlarge" size="30" >
								<div id="contactEmail_error"></div>			
							</div>
						</div>
						<div class="control-group">
							<label for="location" class="control-label required"><spring:message  code="label.employerworksiteaddr"/> </label>
							<div class="controls">
								<select name="location" id="location" class="span8">
								</select>
								&nbsp;&nbsp;
								<small>
								<a href="#modal" id="worksite_ModalLink" role="button" class="" data-toggle="modal">
								<spring:message  code="label.employeraddworksiteaddr"/></a></small>
								
								<%-- <small>
									<a href="javascript:void(0)" id="worksite_ModalLink" role="button" class="" data-toggle="modal">
										<i class="icon-plus-sign"></i>
										<spring:message  code="label.employeraddworksiteaddr"/>
									</a>
								</small> --%>
							</div>
						</div>
			
					</div>
					<!-- profile -->
				<div class="form-actions">
					<input type="submit" name="submitBtn" id="submitBtn" onClick="javascript:validateForm();"  Value="<spring:message  code='label.next'/>" title="<spring:message  code='label.next'/>" class="btn btn-primary  pull-right"><br><br>
				</div>
				
				<input type="hidden" name="jsonStateValue" id="jsonStateValue" value='${doctorsJSON}' />
				<input type="hidden" name="defaultStateValue" id="defaultStateValue" value='${defaultStateCode}' />
				<input type="hidden" name="employerId" id="employerId" value="${employer_id}"/>
				
			</form>
			
			 <!-- Modal -->
			 <%-- <div id="einModal" class="modal hide fade" tabindex="-1"
				role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<form class="form-horizontal" id="frmworksites" name="frmworksites" action="addWorksites" method="POST">
				<df:csrfToken/>
				<input type="hidden" name="id" id="id" value="${employer_id}"/>
				<div class="modal-header">
					<button type="button" class=" clearForm close " aria-hidden="true" id="closeModal">�</button>
					<p id="myModalLabel">&nbsp;</p>
				</div>
				<div class="modal-body">
					<h3><spring:message  code="label.employeradditionalworksite"/></h3>
						<input type="hidden" name="locations[0].primaryLocation" id="primaryLocation" value="NO">
						<input type="hidden" name="locations[0].location.lat_0" id="lat_0" />
						<input type="hidden" name="locations[0].location.lon_0" id="lon_0" />
						<input type="hidden" name="rdi" id="rdi_0" value="" />
							<div class="profile">
								<div class="control-group">
									<label for="address1" class="control-label required"><spring:message  code="label.employeraddress1"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" name="locations[0].location.address1" id="address1_0"  class="input-xlarge" size="30">
										<div id="address1_0_error"></div>			
									</div>
								</div>
								
								<div class="control-group">
									<label for="address2" class="control-label required"><spring:message  code="label.employeraddress2"/></label>
									<div class="controls">
										<input type="text"  name="locations[0].location.address2" id="address2_0"  class="input-xlarge" size="30">
										<div id="address2_0_error"></div>			
									</div>
								</div>
								
								<div class="control-group">
									<label for="city" class="control-label required"><spring:message  code="label.employercity"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" name="locations[0].location.city" id="city_0" class="input-xlarge">
										<div id="city_0_error"></div>			
									</div>
								</div>
								
								<div class="control-group">
									<label for="state" class="control-label required"><spring:message  code="label.employerstate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<select class="input-medium" name="locations[0].location.state" id="state_0"  >
											 <option value="">Select</option>
											 <c:forEach var="state" items="${statelist}">
									    		<option <c:if test="${state.code == defaultStateCode}"> SELECTED </c:if>  value="${state.code}">${state.name}</option>
											</c:forEach>
										</select>
										<div id="state_0_error"></div>			
									</div>
								</div>
								<div class="control-group">
									<label for="zip" class="control-label  required "><spring:message  code="label.employerzipcode"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text"  name="locations[0].location.zip" id="zip_0"  maxlength="5" class="input-mini zipCode">
										<div id="zip_0_error"></div>			
									</div>
								</div>
								
								
								<div class="control-group">
									<label for="county_0" class="control-label required"><spring:message  code="label.country"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<select class="input-large" name="locations[0].location.county"  id="county_0">
										<option value="">Select County...</option>
										</select>
										<div id="county_0_error"></div>		
									</div>
								</div>
								
								
							</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn clearForm" data-dismiss="modal" aria-hidden="true" id="cancelBtnModal" value="<spring:message  code="label.cancel"/>">
					<input type="button" name="submitbutton" id="submitbutton" onClick="javascript:validateLocation();" class="btn btn-primary pull-right" value="<spring:message  code="label.addaddress"/>" />
				</div>
			  </form>
			</div>  --%>
			
		</div>
	</div>
</div>

<script type="text/javascript">
var empDetailsJSON = ${empDetailsJSON};
var contactlocationid = ${contactLocationId};
var empid = '${employer_id}';
var defaultstate = '${stateCode}';

populateLocations(empDetailsJSON, contactlocationid);

function populateLocations(empDetailsJSON, contactlocationid){
	application.populate_location_on_parent_page(empDetailsJSON, contactlocationid,'LOCATIONID');
}

function validateLocation(){
		var subUrl = '<c:url value="/shop/employer/addWorksites">
		                  <c:param name="${df:csrfTokenParameter()}"> 
		                    <df:csrfToken plainToken="true" />
		                  </c:param> 
	                </c:url>';
	if( $("#frmworksites").validate().form() ) {
		$.ajax(
				{
	        		type: "POST",
	       			url: subUrl,
	        		data: $("#frmworksites").serialize(),
	        		dataType:'json',
	        		beforeSend:function(){
	        			
	        		},
	        		success: function(response,xhr){
			              if(isInvalidCSRFToken(xhr))
			                return;
	        			populateLocations(response);
	        			application.remove_modal();
	        		},
	       			error: function( xhr, ajaxOptions, thrownError){
	        			
	       				//alert('Error: ' + e);
	        		}
	        });
	}
}

function validateForm(){
	$("#frmcontact").submit();
}

 var validator = $("#frmcontact").validate({ 
	rules : {
		contactFirstName : {
			required : true
		},
		contactLastName : {
			required : true
		},
		phone3 : {phonecheck : true},
		contactEmail : { required : true, email: true}
	},
	messages : {
		contactFirstName : {
			required : "<span> <em class='excl'>!</em><spring:message code='label.validateempfirstname' javaScriptEscape='true'/></span>"
		},
		contactLastName : {
			required : "<span> <em class='excl'>!</em><spring:message code='label.validateemplastname' javaScriptEscape='true'/></span>"
		},	
		phone3: { phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNo' javaScriptEscape='true'/></span>"
		},
		contactEmail : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateemailadr' javaScriptEscape='true'/></span>", 
			email: "<span> <em class='excl'>!</em><spring:message code='label.validateemail' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error');
	} 
}); 

function populateCityNState(e){
	var address1_e='address1_0'; var address2_e='address2_0'; var city_e= 'city_0'; var state_e='state_0'; var zip_e='zip_0';var lat_e='lat_0';var lon_e='lon_0';
	var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e;				
	viewValidAddressList($('#address1_0').val(),$('#address2_0').val(),$('#city_0').val(),$('#state_0').val(),$('#zip_0').val(),idsText);	
}

jQuery.validator.addMethod("zipcheck", function(value, element, param) {
  	elementId = element.id; 
  	var numzip = new Number($("#"+elementId).val());
    var filter = /^[0-9]+$/;
    if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() ) || numzip<=0 || (!filter.test($("#"+elementId).val()))){
  		return false;	
  	}return true;
});


function clearCounty(){
	$('#county_0').html('');
	$('#county_0').html('<option value="">Select County...</option>');
	$('#state_0').val(defaultstate).attr("selected", "selected");
} 

function getCountyList(eIndex, zip, county) {
	var subUrl =  '<c:url value="/shop/employer/application/addCounties">
		                  <c:param name="${df:csrfTokenParameter()}"> 
		                    <df:csrfToken plainToken="true" />
		                  </c:param> 
	                </c:url>';
	$.ajax({
		type : "POST",
		url : subUrl,
		data : {zipCode : zip}, 
		dataType:'json',
		success : function(response,xhr) {
          if(isInvalidCSRFToken(xhr))
            return;
			populateCounties(response, eIndex, county);
		},error : function(e) {
			alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
		}
	});
}

function populateCounties(response, eIndex,county){
	$('#county'+eIndex).html('');
	var optionsstring = '<option value="">Select County...</option>';
	var i =0;
	$.each(response, function(key, value) {
		var optionVal = key+'#'+value;
		var selected = (county == key) ? 'selected' : '';
		var options = '<option value="'+optionVal+'" '+ selected +'>'+ key +'</option>';
		optionsstring = optionsstring + options;
		i++;
	});
	$('#county'+eIndex).html(optionsstring);
}

function postPopulateIndex(indexValue, zipCodeValue, zipId){
	//console.log(indexValue, zipCodeValue);
	jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
	getCountyList( '_'+indexValue,zipCodeValue, '' );
	//$('#county_'+indexValue).focus();
}

</script>
