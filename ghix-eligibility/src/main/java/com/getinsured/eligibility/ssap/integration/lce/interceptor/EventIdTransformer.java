package com.getinsured.eligibility.ssap.integration.lce.interceptor;

import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component(value = "eventIdTransformer")
public class EventIdTransformer {

	public String transform(Message<String> message) {

		return "payload is  " + message.getPayload();
	}
}
