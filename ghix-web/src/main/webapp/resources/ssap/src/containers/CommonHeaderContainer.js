import React from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';

// Components

import CommonHeader from '../components/headers/CommonHeader';
import ButtonContainer from '../containers/ButtonContainer';



const CommonHeaderContainer = props => {
    const {stateCode} = props;

    const getPropsForHeader = () => {

        const {
            adjustedFlow,
            page
        } = props;

        let updatedProps = {
            headerText: adjustedFlow.length > 0 ? (adjustedFlow[page].page.navBarTextCode ? <FormattedMessage id={adjustedFlow[page].page.navBarTextCode} />: adjustedFlow[page].page.navBarText) : ''
        };

        const extraButton = adjustedFlow.length > 0 ?  adjustedFlow[page].page.extraButtons : '';

        if(extraButton) {
            updatedProps = {
                ...updatedProps,
                additionalElement: (
                    <ButtonContainer
                        type={Object.keys(extraButton)[0]}
                        args={[]}
                    />
                ),
                additionalElementClassName: 'header-btn'
            };
        }



        return updatedProps;
    };
    const isMN = stateCode === 'MN';
    return (
        <div
            className={`${isMN ? 'fade-in main__section__content__title header-without-sideNav'  : 'fade-in main__section__content__title'}`}
        >
            <CommonHeader
                { ...getPropsForHeader() }
            />
        </div>
    );

};

const mapStateToProps = state => ({
    adjustedFlow: state.adjustedFlow,
    flow: state.flow,
    key: `header_key_${state.page}`,
    page: state.page,
    stateCode: state.headerLinks.stateCode,
    messages: state.messages
});

export default connect(mapStateToProps)(CommonHeaderContainer);
