package com.getinsured.iex.hub.ifsv.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.ErrorMessageType;
import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.ObjectFactory;

/**
 * Wrapper com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.ErrorMessageType
 * 
 * @author Nikhil Talreja
 * @since 27-Feb-2014
 *
 */
public class ErrorMessageTypeWrapper implements JSONAware {
	
	private ErrorMessageType errorMessageType;
	
	private ResponseMetadataWrapper responseMetadata;
	private String xPathContent;
	
	private static final String RESPONSE_METADATA_KEY = "ResponseMetadata";
	private static final String XPATH_KEY = "XPathContent";
	
	public ErrorMessageTypeWrapper(ErrorMessageType errorMessageType){
		
		ObjectFactory factory = new ObjectFactory();
		this.errorMessageType = factory.createErrorMessageType();
		
		this.responseMetadata = new ResponseMetadataWrapper(errorMessageType.getResponseMetadata());
		if(errorMessageType.getXPathContent() != null){
			this.xPathContent = errorMessageType.getXPathContent().getValue();
		}
		
		this.errorMessageType.setResponseMetadata(errorMessageType.getResponseMetadata());
		this.errorMessageType.setXPathContent(errorMessageType.getXPathContent());
	}

	public ErrorMessageType getErrorMessageType() {
		return errorMessageType;
	}

	public ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(ResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}

	public String getxPathContent() {
		return xPathContent;
	}

	public void setxPathContent(String xPathContent) {
		this.xPathContent = xPathContent;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		
		JSONObject obj = new JSONObject();
		
		if(this.responseMetadata != null){
			obj.put(RESPONSE_METADATA_KEY,this.responseMetadata);
		}
		
		obj.put(XPATH_KEY,this.xPathContent);
		return obj.toJSONString();
	}
	
}
