package com.getinsured.hix.indportal.dto.dm.application;

import java.util.Map;

public class Attestations {
	private AttestationApplication application;
	private Map<String,AttestationsMember> members;
	private AttestationHousehold household;


	 // Getter Methods 

	 public AttestationHousehold getHousehold() {
		return household;
	}


	public void setHousehold(AttestationHousehold household) {
		this.household = household;
	}



	 // Setter Methods 

	 public Map<String, AttestationsMember> getMembers() {
		return members;
	}


	public void setMembers(Map<String, AttestationsMember> members) {
		this.members = members;
	}


	public AttestationApplication getApplication() {
		return application;
	}


	public void setApplication(AttestationApplication application) {
		this.application = application;
	}



	}
