package com.getinsured.eligibility.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.qlevalidation.RealTimeServiceUtil;
import com.getinsured.eligibility.qlevalidation.RealTimeServiceUtil.HmsStatus;
import com.getinsured.hix.dto.eligibility.EventVerificationDetailsDTO;
import com.getinsured.hix.dto.eligibility.EventVerificationDetailsListDTO;
import com.getinsured.hix.dto.eligibility.RealTimeHMSResponse;
import com.getinsured.hix.dto.eligibility.RealTimeVerificationDTO;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.JWTTokenizer;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Controller
@RequestMapping("/realtime")
public class RealTimeVerificationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RealTimeVerificationController.class);

	@Autowired
	private RealTimeServiceUtil realTimeServiceUtil;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private JWTTokenizer jWTTokenizer;
	@Autowired
	private LookupService lookupService;

	@RequestMapping(value = "/event/verification/details", method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String realTimeVerification(@RequestBody EventVerificationDetailsDTO request) {
		String response = null;

		try {
			String validationStatus = realTimeServiceUtil.processRealTimeVerification(request);
			response = validationStatus;
		} catch (Exception exception) {
			throw new GIRuntimeException("Exception occurred while saving event verification details - ",exception);
		}

		return response;
	}
	
	@ResponseBody
	@RequestMapping(value={"/event/verification/details/{caseNumber}"},method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public EventVerificationDetailsListDTO getEventVerificationDetails(@PathVariable("caseNumber") String  caseNumber){
		try {
			return realTimeServiceUtil.findEventVerificationDetails(caseNumber);
		} catch (Exception ex) {
			LOGGER.error("Exception occurred while fetching event verification details - ",ex);
			throw new GIRuntimeException("Exception occurred while fetching event verification details - ",ex);
		}
	}
	
	@ResponseBody
	@RequestMapping(value={"/event/verification/issuers"},method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<LookupValue> loadIssers(){
		try {
			return lookupService.getLookupValueList("HMS_ISSUERS");
		} catch (Exception ex) {
			LOGGER.error("Exception occurred while fetching issuers - ",ex);
			throw new GIRuntimeException("Exception occurred while fetching issuers - ",ex);
		}
	}

	@RequestMapping(value = {
			"/mockService" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RealTimeHMSResponse realTimeMockService(@RequestBody RealTimeVerificationDTO realTimeVerificationDTO,
			HttpServletRequest httpServletRequest) {
		try {
			LOGGER.info("realTimeServiceUtil.tokenKey : "+realTimeServiceUtil.tokenKey);
			LOGGER.info("Value : "+httpServletRequest.getHeader(realTimeServiceUtil.tokenKey));
			String jwtToken = httpServletRequest.getHeader(realTimeServiceUtil.tokenKey);
			jWTTokenizer.verifyJWT(jwtToken);
			LOGGER.info("verifyJWT");
			String request = JacksonUtils.getJacksonObjectWriter(RealTimeVerificationDTO.class)
					.writeValueAsString(realTimeVerificationDTO);
			LOGGER.info("request"+request);
			LOGGER.info("generateMockUrl"+generateMockUrl(realTimeVerificationDTO.getLastName()));
			return ghixRestTemplate.postForObject(
					GhixEndPoints.ELIGIBILITY_URL + generateMockUrl(realTimeVerificationDTO.getLastName()), request,
					RealTimeHMSResponse.class);
		} catch (Exception ex) {
			LOGGER.error("Exception occurred while triggerring HMS mock", ex);
			throw new GIRuntimeException("Exception occurred while triggerring HMS mock.", ex);
		}
	}
	
	@RequestMapping(value = {
			"/mockVerifiedService" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RealTimeHMSResponse realTimeMockVerifiedService(
			@RequestBody RealTimeVerificationDTO realTimeVerificationDTO) {
		RealTimeHMSResponse realTimeHMSResponse = new RealTimeHMSResponse();
		realTimeHMSResponse.setResponseCode(HmsStatus.VERIFIED.getValue());
		realTimeHMSResponse.setResultType(RealTimeHMSResponse.ResultType.SUCCESS);
		return realTimeHMSResponse;
	}

	@RequestMapping(value = {
			"/mockNotVerifiedService" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RealTimeHMSResponse realTimeMockNotVerifiedService(
			@RequestBody RealTimeVerificationDTO realTimeVerificationDTO) {
		RealTimeHMSResponse realTimeHMSResponse = new RealTimeHMSResponse();
		realTimeHMSResponse.setResponseCode(HmsStatus.NOT_VERIFIED.getValue());
		realTimeHMSResponse.setResultType(RealTimeHMSResponse.ResultType.SUCCESS);
		return realTimeHMSResponse;
	}

	@RequestMapping(value = {
			"/mockNoMatchService" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RealTimeHMSResponse realTimeMockNoMatchService(
			@RequestBody RealTimeVerificationDTO realTimeVerificationDTO) {
		RealTimeHMSResponse realTimeHMSResponse = new RealTimeHMSResponse();
		realTimeHMSResponse.setResponseCode(HmsStatus.NO_MATCH.getValue());
		realTimeHMSResponse.setResultType(RealTimeHMSResponse.ResultType.INFO);
		return realTimeHMSResponse;
	}

	@RequestMapping(value = {
			"/mockInconclusiveService" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RealTimeHMSResponse realTimeMockInconclusiveService(
			@RequestBody RealTimeVerificationDTO realTimeVerificationDTO) {
		RealTimeHMSResponse realTimeHMSResponse = new RealTimeHMSResponse();
		realTimeHMSResponse.setResponseCode(HmsStatus.INCONCLUSIVE.getValue());
		realTimeHMSResponse.setResultType(RealTimeHMSResponse.ResultType.SUCCESS);
		return realTimeHMSResponse;
	}
	
	@RequestMapping(value = {
	"/mockDelayedInconclusiveService" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RealTimeHMSResponse realTimeMockDelayedInconclusiveService(
		@RequestBody RealTimeVerificationDTO realTimeVerificationDTO) {
	RealTimeHMSResponse realTimeHMSResponse = new RealTimeHMSResponse();
	realTimeHMSResponse.setResponseCode(HmsStatus.INCONCLUSIVE.getValue());
	realTimeHMSResponse.setResultType(RealTimeHMSResponse.ResultType.SUCCESS);
			
	//delaying response for 30 seconds
		try {
			LOGGER.info("Sleeping Start for 30 Seconds HMS Call");
			Thread.sleep(30000);
			LOGGER.info("Sleeping End for 30 Seconds HMS Call");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	
		return realTimeHMSResponse;
	}


	@RequestMapping(value = {
			"/mockErrorService" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RealTimeHMSResponse realTimeMockErrorService(@RequestBody RealTimeVerificationDTO realTimeVerificationDTO) {
		RealTimeHMSResponse realTimeHMSResponse = new RealTimeHMSResponse();
		realTimeHMSResponse.setResponseCode(HmsStatus.ERROR.getValue());
		realTimeHMSResponse.setResultType(RealTimeHMSResponse.ResultType.SUCCESS);
		return realTimeHMSResponse;
	}
	
	@RequestMapping(value = {
	"/mockExceptionService" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RealTimeHMSResponse realTimeMockExceptionService(
		@RequestBody RealTimeVerificationDTO realTimeVerificationDTO) throws Exception {
		RealTimeHMSResponse realTimeHMSResponse = new RealTimeHMSResponse();
		realTimeHMSResponse.setResponseCode(HmsStatus.NO_MATCH.getValue());
		realTimeHMSResponse.setResultType(RealTimeHMSResponse.ResultType.INFO);
		throw new Exception("Exception Mock Service Invoked");
	}
	
	@RequestMapping(value = {
	"/mockResultTypeErrorService" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RealTimeHMSResponse realTimeMockResultTypeService(@RequestBody RealTimeVerificationDTO realTimeVerificationDTO) {
		RealTimeHMSResponse realTimeHMSResponse = new RealTimeHMSResponse();
		realTimeHMSResponse.setResponseCode(HmsStatus.PENDING.getValue());
		realTimeHMSResponse.setResultType(RealTimeHMSResponse.ResultType.ERROR);
		return realTimeHMSResponse;
	}

	public String generateMockUrl(String name) {
		String url = null;
		try {
			switch (name) {
				case "NoMatch":
					url = "realtime/mockNoMatchService";
					break;
				case "NotVerified":
					url = "realtime/mockNotVerifiedService";
					break;
				case "Verified":
					url = "realtime/mockVerifiedService";
					break;
				case "Inconclusive":
					url = "realtime/mockInconclusiveService";
					break;
				case "Delayed":
					url = "realtime/mockDelayedInconclusiveService";
					break;
				case "Exception":
					url = "realtime/mockExceptionService";
					break;
				case "ResultTypeError":
					url = "realtime/mockResultTypeErrorService";
					break;
				default:
					url = "realtime/mockErrorService";
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE RETURNING RANDOM MOCK API URL: ", ex);
		}

		return url;
	}
}
