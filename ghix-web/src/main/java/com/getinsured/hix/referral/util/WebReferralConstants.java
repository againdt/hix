package com.getinsured.hix.referral.util;

/**
 * @author chopra_s
 * 
 */
public interface WebReferralConstants {
	int FAILURE = 0;
	int SUCCESS = 1;
	int FAILURE_AGAIN = 2;
	int DOCUMENT_FAILURE = 2;
	int USER_LINKED_TO_DIFFERENT_EXTERNAL_ID = 3;

	// CSR Verification Constants
	String REFERRAL_ACTIVATION_DOCUMENTS = "referralActivationDocuments";
	String REFERRAL_TKM_CATEGORY = "Document Verification";
	String REFERRAL_TKM_TYPE = "Referral Identity Verification";
	String REFERRAL_TKM_MODULE_NAME = "REFERRAL_ACTIVATION";
	String INDIVIDUAL_ROLE = "INDIVIDUAL";
	String REFERRAL_FROM_CSR = "REFERRAL_FROM_CSR";
}
