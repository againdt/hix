<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/employee-dashboard-util" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/shop-enrollment-data" prefix="enrollData"%>
<script type="text/javascript" src="/hix/resources/js/bootstrap-datepicker.js"></script>
<link href="/hix/resources/css/datepicker.css" rel="stylesheet"  type="text/css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />

<!--If Dental plan is not there then execute below script
<script type='text/javascript'>
	$('#covDentalPlan').remove();
	$('.floatDiv').removeClass('span12').addClass('span6');
</script>-->
<div class="gutter10">
  <h1>Welcome Back,&nbsp;<span id="user-name">${employee.name}</span></h1>
  <div class="row-fluid">
    
    <div class="span3" id="sidebar">
      <util:myStuff employee="${employee}"></util:myStuff>
      <util:quickLinks employee="${employee}"></util:quickLinks>
    </div><!--/span3-->
    
	<div id="rightpanel" class="span9">   
	
		<div class="header">  
			<h4>My Plans Terminate Health and Dental coverage</h4>        
		</div>

		<div class="gutter10">
		<!--Health Plan-->
			<div id="cogHealthplan" class="span6 info-block pull-left">
				  <div class="gutter10">
					   <div class="header"><h4>Applicant Name,Spouse Name, Child Name</h4></div>
					   <div class="gutter10 "> 
						  <div id="plan-selected" class="alert alert-info">
							<div class="info-header center">
								<img src="/hix/resources/img/sample-plan.png" alt="Sample Plan Image">
								<p><a href="#">CA SILVER+ 2014</a></p>
								<h5 class="payment">$230/mo</h5>
							</div>
							<!--Mask If the plan is terminated
							<div class="mask" id="plan-mask-sample"><img alt="This is a sample plan summary" src="/hix/resources/img/example.png"></div>
							-->
							<div class="data-display well">
								 <dl class="dl-horizontal">
									<dt>Plan Type:</dt>
									<dd id="plan-type">PPO</dd>
									<dt>Office Visit:</dt>
									<dd id="off-visit">$25</dd>
									<dt>Generic Medications:</dt>
									<dd id="generic-medic">$10</dd>
									<dt>Deductible:</dt>
									<dd id="deductible">$1,500</dd>
									<dt>Max Out-of-Pocket:</dt>
									<dd id="m-o-p">$5,000</dd>
								  </dl>
							</div>                    
						  </div>
						  <div class="gutter10-tb">
								
						  </div>
						</div>               
				  </div> <!-- .gutter10-->
			</div>
			
			<!-- Dental Plan -->
			<div id="covDentalPlan" class="span6 info-block pull-left">
			  <div class="gutter10">
			   <div class="header"><h4>Child Name</h4></div>
			   <div class="gutter10 ">
				  <div id="plan-selected" class="alert alert-info">
					<div class="info-header center">
						<img src="/hix/resources/img/sample-plan.png" alt="Sample Plan Image">
						<p><a href="#">CA SILVER+ 2014</a></p>
						<h5 class="payment">$230/mo</h5>
					</div>
					<!--Mask If the plan is terminated
					<div class="mask" id="plan-mask-sample"><img alt="This is a sample plan summary" src="/hix/resources/img/example.png"></div>
					-->
					<div class="data-display well">
					 <dl class="dl-horizontal">
						<dt>Plan Type:</dt>
						<dd id="plan-type">PPO</dd>
						<dt>Office Visit:</dt>
						<dd id="off-visit">$25</dd>
						<dt>Generic Medications:</dt>
						<dd id="generic-medic">$10</dd>
						<dt>Deductible:</dt>
						<dd id="deductible">$1,500</dd>
						<dt>Max Out-of-Pocket:</dt>
						<dd id="m-o-p">$5,000</dd>
					  </dl>
					</div>                    
				  </div>
				  <div class="gutter10-tb"></div>
				</div>		
			  </div> <!-- .gutter10-->
			</div>
			
			
		<div class="row-fluid pull-left floatDiv span12">
					<div class="row-fluid span12">
						<div class="gutter10 alert-error">             
								<p><span><strong>Please Note: </strong>A health plan can be terminated only when you experience a qualifying event.If you have experienced one please select an event and the date to continue</span></p>            
						</div>
						<div class="gutter10 span12">			
							<div class="control-group pull-left span12">
								<div class="control-label pull-left span3">
									<label class="control-label ">Event</label>
								</div>
								<div class="controls span7 ">
										<select name="healthtermEvent" id="healthtermEvent" onchange="javascript:getMappingConfig()" class="ie8-select input-medium">
										<option value="" <c:if="" test="${searchCriteria.healthtermEvent == ''}"> selected="selected" &gt;Select state</option>
										
											<option value="${termEvent.code}" <c:if="" test="${searchCriteria.termEvent == termEvent.code}"> selected="selected" &gt;${termEvent.name}</option>
										
										</select>
								</div>
							</div>
									
							<div class="control-group pull-left span12">
								<label for="planLevel" class="control-label span3">Date</label>
								<div id="date" class="input-append date date-picker ">	
									<input type="text" onkeypress="resetErrorMsg(this)" value="${searchCriteria.healthtermDate}" id="healthtermDate" name="healthtermDate" class="span5 valid" aria-required="true" aria-label="Termination Date" tabindex="0"> <span class="add-on"><i class="icon-calendar"></i></span>
									<div class="help-inline" id="startDate_error" role=""></div>							
								</div>
								<span id="commsnDate_error"></span>
							</div>
								
							<div class="control-group span12 margin0 pull-left">
								<br><label class="checkbox"><input type="checkbox" id="emp_attest"> I attest that I want to terminate employer coverage, and I understand that I will be unable to enrol again until the next open enrolment season ,unless I experience a qualifying agent .</label><br><br>         
							</div><br>
										
							<div class="control-group margin0 pull-left">
								<div class="control-label pull-left span5">
									<label class="control-label span12">Your Signature</label>
								</div>
								<div class="controls span3">
									<input type="text" class="input-medium">
								</div>
							</div>
									
							<div class="control-group pull-left">
								<label for="planLevel" class="control-label span5">Date</label>
								<div id="date" class="input-append date date-picker input-xlarge">	
									<input type="text" onkeypress="resetErrorMsg(this)" value="${searchCriteria.terminationDate}" id="terminationDate" name="commissionDate" class="span5 valid" aria-required="true" aria-label="Start Date" tabindex="0"> <span class="add-on"><i class="icon-calendar"></i></span>
									<div class="help-inline" id="startDate_error" role=""></div>							
								</div>
								<span id="commsnDate_error"></span>
							</div>
						</div>
					  </div>
					
					<div class="row-fluid txt-right">
						<a href="#" class="btn ">Cancel</a>
						<a href="#" class="btn btn-primary">Confirm Termination</a>
					</div>	
				</div>			
		</div>
  
	</div><!-- Right Panel-->
  </div><!--/row-fluid-->    
</div><!--/main .gutter10-->

<script type="text/javascript">


function imgSize(){
    $(".carrierlogo").each(function(){
        var width = $(this).width();
        var height = $(this).height();
        var maxWidth = 160;
		var maxHeight = 60;
		
		// Check if the current width is larger than the max
	    if(width > maxWidth){
	        ratio = maxWidth / width; // get ratio for scaling image
	        $(this).width(maxWidth); // Set new width
	        $(this).height((height * ratio)); // Scale height based on ratio
	        height = height * ratio; // Reset height to match scaled image
	    	width = width * ratio; // Reset width to match scaled image
	    }
		
		// Check if current height is larger than max
		if(height > maxHeight){                                     
		  ratio = maxHeight / height; // get ratio for scaling image
		  $(this).height(maxHeight); // Set new height
		  $(this).width((width * ratio)); // Scale width based on ratio    
		}
	    
		$(this).css('display','block');
    });
}

</script>

