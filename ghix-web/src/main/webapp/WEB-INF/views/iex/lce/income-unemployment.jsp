<script type="text/ng-template" id="income-unemployment.jsp">
  <div class="gutter10 jobIncome" ng-controller="incomeUnemployment" >
    <h3><spring:message code="label.lce.unemploymentincome"/></h3>
    <form class="form-horizontal" id="unemploymentIncome" name="unemploymentIncome" novalidate>
      <div class="control-group">
        <spring:message code="label.lce.fromwhatemployment"/> {{householdMember.name.firstName}} {{householdMember.name.lastName}} <spring:message code="label.lce.getunemploymentbenefits"/>
        <label class="control-label" for="typeOfRequest"><br><spring:message code="label.lce.govtorformeremployer"/>:</label>
          <div class="controls"><br>
            <input type="text" class="span5" name="stateGovernmentOrFormerEmployerName" ng-model="householdMember.detailedIncome.unemploymentBenefit.stateGovernmentName" required ng-trim>
          </div>
          <div class="controls margin5-t">
            <span class="validation-error-text-container" ng-show="submitted && unemploymentIncome.stateGovernmentOrFormerEmployerName.$error.required"><spring:message code="label.lce.incomeunemployment.error1"/></span>
          </div>
      </div>

      <spring:message code="label.lce.howmuchdid"/> {{householdMember.name.firstName}} {{householdMember.name.lastName}} <spring:message code="label.lce.get"/>
      <div class="control-group">
        <label class="control-label" for="typeOfRequest"></label>
          <div class="controls margin10-t">
            <div class="input-append input-prepend">
              <span class="add-on">$</span>
              <input type="text" class="margin10-r span4" name="unemploymentBenefitIncome" ng-model="householdMember.detailedIncome.unemploymentBenefit.benefitAmount" required numbers-Only>
              <select class="span5" name="unemploymentBenefitIncomeFrequency" ng-model="householdMember.detailedIncome.unemploymentBenefit.incomeFrequency" required>
                <option><spring:message code="label.lce.weekly"/>Weekly</option>
			          <option><spring:message code="label.lce.biweekly"/>Bi-Weekly</option>
		  	        <option><spring:message code="label.lce.monthly"/>Monthly</option>
			          <option><spring:message code="label.lce.bimonthly"/>Bi-Monthly</option>
			          <option><spring:message code="label.lce.yearly"/>Yearly</option>
			          <option><spring:message code="label.lce.onceonly"/>Once Only</option>
              </select>
           </div>
         </div>
         <div class="controls margin5-t">
           <span class="validation-error-text-container" ng-show="submitted && unemploymentIncome.unemploymentBenefitIncome.$error.required"><spring:message code="label.lce.incomeunemployment.error2"/><br/></span>
           <span class="validation-error-text-container" ng-show="submitted && unemploymentIncome.unemploymentBenefitIncomeFrequency.$error.required"><spring:message code="label.lce.incomeunemployment.error3"/></span>
         </div>
      </div>

      <spring:message code="label.lce.did"/> {{householdMember.name.firstName}} {{householdMember.name.lastName}} <spring:message code="label.lce.workwhilegettingpaid"/>
      <div class="control-group">
        <label class="control-label" for="typeOfRequest"></label>
          <div class="controls">
            <label class="radio inline" for="unemploymentyes">
              <input type="radio" id="unemploymentyes" name="unemployment" value="Yes" ng-model="unemployment"> <spring:message code="label.lce.yes"/>
            </label>
            <label class="radio inline" for="unemploymentno">
              <input type="radio" id="unemploymentno" name="unemployment" value="No" ng-model="unemployment"> <spring:message code="label.lce.no"/>
            </label>
          </div>
      </div>

      <div ng-show="unemployment=='Yes'">
      <spring:message code="label.lce.whatisthedate"/>
      <div class="control-group">
        <label class="control-label" for="typeOfRequest"><spring:message code="label.lce.start"/>:</label>
          <div class="controls">

            <form class="form-horizontal" novalidate name="form" ng-submit="submit()">
              <div id="date" class="input-append" datetimez ng-model="var1">
                <input data-format="MM/dd/yyyy" type="text" id="input1" name="input1" class="span10"></input>
                <span class="add-on">
                  <i class="icon-calendar"></i>
                </span>
              </div>
            <!-- </form> -->
          </div>
      </div>

      <div class="control-group">
        <label class="control-label" for="typeOfRequest"><spring:message code="label.lce.end"/>:</label>
          <div class="controls">
            <form class="form-horizontal" novalidate name="form" ng-submit="submit()">
              <div id="date" class="input-append" datetimez ng-model="var1">
                <input data-format="MM/dd/yyyy" type="text" id="input1" name="input1" class="span10"></input>
                <span class="add-on">
                  <i class="icon-calendar"></i>
                </span>
              </div>
            </form>

          </div>
      </div>
    </div>
  </form>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a ng-show="unemploymentIncome.$valid"class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
      <a ng-show="!unemploymentIncome.$valid"class="btn btn-primary" ng-click="submitted=true"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>