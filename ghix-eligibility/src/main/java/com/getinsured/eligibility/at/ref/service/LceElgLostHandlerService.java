package com.getinsured.eligibility.at.ref.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.EnrollmentAttributesDTO;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.resp.si.dto.ApplicationExtension;
import com.getinsured.eligibility.at.sep.service.SepEventsService;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
@Component("lceElgLostHandlerService")
@Scope("singleton")
public class LceElgLostHandlerService extends LceProcessHandlerBaseService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(LceElgLostHandlerService.class);

	@Autowired
	private EntireHHLostEligibilityAndDisenrollHandler entireHHLostEligibilityAndDisenroll;

	@Autowired
	private EntireHHLostEligibilityAndConversionHandler entireHHLostEligibilityAndConversion;

	@Autowired
	@Qualifier("sepEventsService")
	private SepEventsService sepEventsService;

	private enum NonQhpIndicatorTypes {
		ALL, MIXED, NONE;
	}

	@Override
	@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceElgLostHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		String costSharing=lceProcessRequestDTO.getEnrolledApplicationAttributes().getCostSharing();
		final SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		final SsapApplication enrolledApplication = loadApplication(enrolledApplicationId);
		final ApplicationExtension applicationExtension = lceProcessRequestDTO.getApplicationExtension();
		final List<String> enrolledApplicantsGuid = fetchEnrolledApplicantsGuid(lceProcessRequestDTO);
		executehouseholdLostEligibility(currentApplication, enrolledApplication, applicationExtension,costSharing,enrolledApplicantsGuid, lceProcessRequestDTO);
		super.closePreviousERApplication(currentApplication,lceProcessRequestDTO);	
		LOGGER.info("LceElgLostHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}

	/**
	 * Categorize the application when
	 * 
	 * ALL - when all previously enrolled applicants having non Qhp indicator in application extn MIXED - Some are having non Qhp indicator NONE - No one is having non qhp indicator.
	 * @param enrolledApplicantsGuid 
	 * @param lceProcessRequestDTO 
	 * @param healthPlanId 
	 * @param enrolledPlanId 
	 * 
	 */

	private void executehouseholdLostEligibility(SsapApplication currentApplication, SsapApplication enrolledApplication, ApplicationExtension currentApplicationExtension,String costSharing, List<String> enrolledApplicantsGuid, LCEProcessRequestDTO lceProcessRequestDTO) {

		final List<SsapApplicant> currentSsapApplicants = currentApplication.getSsapApplicants();
		final Map<String, ApplicantEvent> applicantEventMap = ApplicationExtensionEventUtil.populateMaxApplicantEventforApplicant(currentSsapApplicants, currentApplicationExtension, enrolledApplicantsGuid);
		final NonQhpIndicatorTypes nonQhpIndicatorType = populateNonQhpIndicatorType(currentSsapApplicants, enrolledApplicantsGuid, applicantEventMap);
		final boolean isExchangeEligRequired = "TRUE".equalsIgnoreCase( DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ELIGIBILITY_ENABLE)) ? true :false;
		
		if(!isExchangeEligRequired && (currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.MEDICAID)) {
			entireHHLostEligibilityAndDisenroll.handleAllApplicantHasNonQhpIndicator(currentApplication, enrolledApplication, applicantEventMap);
		} else if (NonQhpIndicatorTypes.ALL.equals(nonQhpIndicatorType) || isExchangeEligRequired) {
			LOGGER.info("All Applicant has Non Qhp Indicators for ssapid " + currentApplication.getId());
			// Disenroll case
			entireHHLostEligibilityAndDisenroll.handleAllApplicantHasNonQhpIndicator(currentApplication, enrolledApplication, applicantEventMap);
		} else if (NonQhpIndicatorTypes.MIXED.equals(nonQhpIndicatorType)) {
			LOGGER.info("Some Applicant has Non Qhp Indicators for ssapid " + currentApplication.getId());
			entireHHLostEligibilityAndConversion.executeSomeApplicantHasNonQhpIndicator(currentApplication, enrolledApplication, applicantEventMap,enrolledApplicantsGuid, lceProcessRequestDTO);
		} else if (NonQhpIndicatorTypes.NONE.equals(nonQhpIndicatorType)) {
			LOGGER.info("No Applicant has Non Qhp Indicators for ssapid " + currentApplication.getId());
			entireHHLostEligibilityAndConversion.executeNoApplicantHasNonQhpIndicator(currentApplication, enrolledApplication, applicantEventMap, enrolledApplicantsGuid, costSharing, lceProcessRequestDTO);
		} else {
			LOGGER.error("Error in determining Household lost eligibility type for" + currentApplication.getId());
			throw new GIRuntimeException("Error in processing Household lost eligibility application for" + currentApplication.getId());
		}

	}

	private NonQhpIndicatorTypes populateNonQhpIndicatorType(List<SsapApplicant> currentSsapApplicants, List<String> applicantGuidsOnEnrolledApplication, Map<String, ApplicantEvent> applicantEventMap) {

		final List<String> applicantGuidsOnCurrentApplication = fetchApplicantGuidsForApplicantsOnCurrentApplication(currentSsapApplicants);

		NonQhpIndicatorTypes nonQhpIndicatorType = null;

		if (applicantGuidsOnCurrentApplication.size() == ReferralConstants.ONE) {
			// Special scenario: in oe household with more than 1 enrolled and in sep primary alone in request with or without indicator falls here
			// other or condition is for household with 2 members and primary is in eligbile . sep with eligible primary.
			if (ReferralUtil.listSize(applicantGuidsOnEnrolledApplication) > ReferralConstants.ONE || !applicantGuidsOnEnrolledApplication.contains(applicantGuidsOnCurrentApplication.get(0))) {
				return NonQhpIndicatorTypes.ALL;
			} else { // to handle when household size is 1 in oe/qep and in sep if primary is in-eligible with or without non qhp indicator falls here
				if (applicantEventMap.containsKey(applicantGuidsOnCurrentApplication.get(0))) {
					return NonQhpIndicatorTypes.ALL;
				} else {
					return NonQhpIndicatorTypes.NONE;
				}
			}
		}
		// when no events in map it will fall under none
		if (applicantEventMap.size() == ReferralConstants.NONE) {
			return NonQhpIndicatorTypes.NONE;
		}

		// to handle scenario in sep when household size is more than 1 .
		for (String onApplicant : applicantGuidsOnCurrentApplication) {
			if (applicantGuidsOnEnrolledApplication.contains(onApplicant)) {
				if (applicantEventMap.containsKey(onApplicant)) {
					nonQhpIndicatorType = NonQhpIndicatorTypes.ALL;
				} else {
					nonQhpIndicatorType = NonQhpIndicatorTypes.MIXED;
					break;
				}
			}

		}

		return nonQhpIndicatorType;
	}

	

	private List<String> fetchEnrolledApplicantsGuid(LCEProcessRequestDTO lceProcessRequestDTO) {
		List<String> applicantGuids = new ArrayList<String>();
		EnrollmentAttributesDTO enrollmentAttributesDTO = lceProcessRequestDTO.getEnrolledApplicationAttributes();
		if (ReferralUtil.listSize(enrollmentAttributesDTO.getHealthEnrollees()) > 0 || ReferralUtil.listSize(enrollmentAttributesDTO.getDentalEnrollees()) > 0) {
			if (ReferralUtil.listSize(enrollmentAttributesDTO.getHealthEnrollees()) > 0) {
				applicantGuids.addAll(enrollmentAttributesDTO.getHealthEnrollees());
			}// for dental only
			else if (ReferralUtil.listSize(enrollmentAttributesDTO.getDentalEnrollees()) > 0) {
				applicantGuids.addAll(enrollmentAttributesDTO.getDentalEnrollees());
			}
		} else {
			throw new GIRuntimeException("No enrollee found. Error in processing Household lost eligibility application for" + lceProcessRequestDTO.getCurrentApplicationId());
		}

		return applicantGuids;
	}

}
