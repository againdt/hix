package com.getinsured.hix.batch.agent;

import java.util.List;



public class AgentDailySummaryNoticesDTO {
	
	private Integer ID;	
	private String agentfirstName;
	private String agentlastName;
	private String emailAddress;
	private Integer totalActiveIndividualsWithActionableNotices;
	
	public Integer getTotalActiveIndividualsWithActionableNotices() {
		return totalActiveIndividualsWithActionableNotices;
	}
	public void setTotalActiveIndividualsWithActionableNotices(
			Integer totalActiveIndividualsWithActionableNotices) {
		this.totalActiveIndividualsWithActionableNotices = totalActiveIndividualsWithActionableNotices;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	private List<Individual> individualDetailsList;
	
	public String getAgentfirstName() {
		return agentfirstName;
	}
	public void setAgentfirstName(String agentfirstName) {
		this.agentfirstName = agentfirstName;
	}
	public String getAgentlastName() {
		return agentlastName;
	}
	public void setAgentlastName(String agentlastName) {
		this.agentlastName = agentlastName;
	}
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public List<Individual> getIndividualDetailsList() {
		return individualDetailsList;
	}
	public void setIndividualDetailsList(List<Individual> individualDetailsList) {
		this.individualDetailsList = individualDetailsList;
	}

}
