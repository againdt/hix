import consoleAdjusterReducer, { initialState } from '../consoleAdjusterReducer';
import { consoleActionTypes } from '../../constants/actionTypes';


const anotherAction = {
    type: 'ANOTHER_ACTION',
    payload: []
};

const switchAction = {
    type: consoleActionTypes.SWITCH_CONSOLE_MODE,
    payload: {
        isInvalidInputsShown: true
    }
};

const clearAction = {
    type: consoleActionTypes.CLEAR_STATE
};

describe('Test for consoleAdjusterReducer', () => {

    it('This reducer hasn\'t been invoked', () => {
        expect(consoleAdjusterReducer(initialState, anotherAction)).toEqual(initialState);
    });

    it('Should switch console mode', () => {
        expect(consoleAdjusterReducer(initialState, switchAction)).toEqual({
            ...initialState,
            ...switchAction.payload
        });
    });

    it('Should clear state', () => {
        expect(consoleAdjusterReducer({ welcomeText: 'Hi there!' }, clearAction)).toEqual(initialState);
    });

});