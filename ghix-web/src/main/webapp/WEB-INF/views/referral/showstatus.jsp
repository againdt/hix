<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div>
	<div class="gutter10 center-text">
		<p>
			<spring:message code="msg.referral.invalid.page" />
			<div class="gutter10 center-text">
				<p><a href="<c:url value='/indportal' />" class="btn btn-primary">Goto DashBoard</a></p>
			</div>
		</p>
	</div>
</div>
