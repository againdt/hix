<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>
<div class="accordion-group">
	<div class="control-group form-inline">
		 <spring:message code="label.uploadSummary"/>
		 <ul>
			 <c:forEach var="errorsListObj" items="${errorsList}" varStatus="i">
			 <%int count = 0; %>
			 		<c:out value="============================================="></c:out>
					 	<ul>  
			                <c:forEach var="errObj" items="${errorsListObj}" varStatus="j">
			                    <li style="list-style: none"><% if(count>0) {%>&nbsp;&nbsp;&nbsp;&nbsp;	-  <%}count++;%><b><c:out value="${errObj.value}"/></b></li>
			                </c:forEach>
		                </ul> 
	                <c:out value="============================================"></c:out> 
        	 </c:forEach>  
		 </ul>
		 <ul>
		 	<li><spring:message code="label.numberRowsInFile"/>Number rows in file ${TOTAL_ROWS} </li>
		 	<li><spring:message code="label.numberRowsInserted"/>Number rows inserted ${SUCCESSFUL_ROWS}  </li>
		 	<li><spring:message code="label.numberRowsInsertionFailed"/>Number rows insertion failed ${FAILURE_ROWS} </li>
		 </ul>
	</div>
</div>

