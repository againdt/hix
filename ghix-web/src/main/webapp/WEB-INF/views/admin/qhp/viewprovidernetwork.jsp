<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
<c:set var="encPlanId" ><encryptor:enc value="${planId}" isurl="true"/> </c:set>
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message code="label.plans"/></a></li>
            <li><a href="<c:url value="/admin/planmgmt/manageqhpplans"/>"><spring:message code="label.manageQHPS"/></a></li>
            <li><a href="<c:url value="/admin/planmgmt/viewqhpdetail/${encPlanId}" />"><spring:message code="label.linkPlanDetails"/></a></li>
            <li><spring:message code="label.linkProviderNetwork"/></li>
        </ul><!--page-breadcrumb ends -->
	</div>
	<div class="row-fluid">
			<h1><a name="skip"></a><img src="<c:url value="/admin/issuer/company/profile/logo/hid/${plan.issuer.hiosIssuerId}"/>"/>${plan.name} <small><spring:message code="label.linkProviderNetwork"/></small></h1>
	
	
				<c:if test="${displayEditButton == 'YES'}"><a href="<c:url value="/admin/planmgmt/editqhpprovidernetwork/${encPlanId}"></c:url>" class="pull-right"><i class="icon icon-user"></i> <spring:message code="button.label"/></a></c:if>

	</div>
	
	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message code="label.aboutPlan"/></h4>
       		</div>
       		<!--  beginning of side bar -->
	            <ul class="nav nav-list graybg">
     				<li><a href="<c:url value="/admin/planmgmt/viewqhpdetail/${encPlanId}" />"><spring:message  code='label.linkPlanDetails'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpbenefits/${encPlanId}"/>"><spring:message  code='label.linkPlanBenefits'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhprates/${encPlanId}" />"><spring:message  code='label.linkPlanRates'/></a></li>
                    <li class="active"><a href="<c:url value="/admin/planmgmt/viewqhpprovidernetwork/${encPlanId}" />"><spring:message  code='label.linkProviderNetwork'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpenrollmentavail/${encPlanId}"/>"><spring:message  code='label.linkEnrollmentAvail'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpcertification/${encPlanId}" />"><spring:message  code='label.linkCertificationStatus'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhphistory/${encPlanId}"/>"><spring:message  code='label.linkPlanHistory'/></a></li>
			   </ul>
				<br /><br />
				
				<%--Currently fixing this issue without any flag entries in plan-management's configuration file. please confirm whether plan-management's configuration file approach is required.--%>
				<%--
				<h4 class="margin0">Actions</h4>
				<ul class="nav nav-list graybg">
					<li><a href="<c:url value="/admin/planmgmt/editqhpdetail/${planId}" />"><!--<i class="icon icon-pencil" style=""></i>--> <spring:message  code='label.linkEditPlanDetails'/></a></li>
					<li><a href="<c:url value="/admin/planmgmt/editqhpbenefits/${planId}" />"><!--<i class="icon icon-arrow-up"></i> --><spring:message  code='label.linkEditPlanBenefits'/></a></li>
					<li><a href="<c:url value="/admin/planmgmt/editqhprates/${planId}" />"><!--<i class="icon icon-arrow-up"></i>--> <spring:message  code='label.linkEditPlanRates'/></a></li>
					<li><a href="<c:url value="/admin/planmgmt/editqhpprovidernetwork/${planId}" />"><!--<i class="icon icon-folder-open"></i>--> <spring:message  code='label.linkEditProviderNetwork'/></a></li>
					<li><a href="<c:url value="/admin/planmgmt/editqhpenrollmentavail/${planId}" />"><!--<i class="icon icon-folder-open"></i>--> <spring:message  code='label.linkEditEnrollmentAvail'/></a></li>
					<li><a href="<c:url value="/admin/planmgmt/editqhpcertification/${planId}" />"><!--<i class="icon icon-folder-open"></i>--> <spring:message  code='label.linkEditCertificationStatus'/></a></li>
				</ul>
				--%>
			
        </div><!-- end of span3 -->
        
		<div class="span9" id="rightpanel">
			<display:table name="data" pagesize="${pageSize}" list="rates" requestURI="" sort="list" class="table table-border-none table-striped"  defaultsort="1" defaultorder="descending" >
			           <display:column property="lastUpdateTimestamp" titleKey="label.date" format="{0,date,MMM dd, yyyy}" sortable="true" />
			           <display:column property="providerName" titleKey="label.providerNetwork" sortable="true" />
			           <display:column property="providerType" titleKey="label.type" sortable="true" />
			           <display:column property="userName" titleKey="label.userName" sortable="false" />
			           <display:setProperty name="paging.banner.placement" value="bottom" />
			           <display:setProperty name="paging.banner.some_items_found" value=''/>
			           <display:setProperty name="paging.banner.all_items_found" value=''/>
			           <display:setProperty name="paging.banner.group_size" value='50'/>
			           <display:setProperty name="paging.banner.last" value=''/>
			           <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
			           <display:setProperty name="paging.banner.page.selected" value='<a class="active"><strong>{0}</strong></a>'/>
			           <display:setProperty name="paging.banner.onepage" value=''/>
			           <display:setProperty name="paging.banner.one_item_found" value=''/>
			           <display:setProperty name="paging.banner.first" value='<span class="pagelinks">
			           <div class="pagination center">
						<ul>
							<li>{0}</li>
							<li><a href="{3}"><spring:message code="label.nextPage"/></a></li>
						</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.last" value='<span class="pagelinks">
						<div class="pagination center">
							<ul>
								<li><a href="{2}"><spring:message code="label.prevPage"/></a></li>
								<li>{0}</li>
							</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.full" value='
						<div class="pagination center">
							<ul>
								<li><a href="{2}"><spring:message code="label.prevPage"/></a></li>
								<li>{0}</li>
								<li><a href="{3}"><spring:message code="label.nextPage"/></a></li>
							</ul>
						</div>
						'/>
			</display:table>
		</div><!-- end of .span9 -->
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	})
})
</script>


