<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld"%>

<div class="gutter10">
	<h1>Unlock Your Account</h1>
	<%-- <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
		<c:if test="${errorMsg != ''}">
			<p><c:out value="${errorMsg}"></c:out><p/>
		</c:if>
			<br>
		</div>
	</div> --%>
	<div class="row-fluid">
	
		<div class="span3" id="sidebar">
			<div class="header">
				<h4>You should know</h4>
			</div>
			<ul class="nav nav-list">
			    <li>To unlock your account you will need your email, answers to security questions and certain miscellaneous personal information.</li>
		    </ul>
		</div>
		
		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="pull-left">Step 2: Verify your phone number</h4>
			</div>
			
<%-- 			<%-- <c:choose> --%>
<%-- 				<c:when test="${not empty isAdminRole}"> --%>
<!-- 					<div class="row-fluid margin5-t marginL5"  id="member-portal">		  	 -->
<!-- 						<div class="well-step"> -->
<!-- 							<div class="row-fluid"> -->
<!-- 								<div id="step-progress"> -->
<!-- 									<div id="progress-bar"> -->
<!-- 										<ul> -->
<!-- 											<li id="step-one" class="step-desc active-state step4"><span class="s-no">1</span></li> -->
<!-- 											<li id="step-two" class="step-desc active-state step4"><span class="s-no">2</span></li> -->
<!-- 											<li id="step-three" class="step-desc inactive-state step4"><span class="s-no">3</span></li> -->
<!-- 											<li id="step-four" class="step-desc inactive-state step4"><span class="s-no">4</span></li> -->
<!-- 										</ul> -->
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 						</div>  -->
<!-- 						</div>  -->
<%-- 					</c:when> --%>
<%-- 					<c:otherwise> --%>
<!-- 					<div class="row-fluid margin5-t marginL5"  id="member-portal">		  	 -->
<!-- 						<div class="well-step"> -->
<!-- 							<div class="row-fluid"> -->
<!-- 								<div id="step-progress"> -->
<!-- 									<div id="progress-bar"> -->
<!-- 										<ul> -->
<!-- 											<li id="step-one" class="step-desc active-state step5"><span class="s-no">1</span></li> -->
<!-- 											<li id="step-two" class="step-desc active-state step5"><span class="s-no">2</span></li> -->
<!-- 											<li id="step-three" class="step-desc inactive-state step5"><span class="s-no">3</span></li> -->
<!-- 											<li id="step-four" class="step-desc inactive-state step5"><span class="s-no">4</span></li> -->
<!-- 											<li id="step-five" class="step-desc inactive-state step5"><span class="s-no">5</span></li> -->
<!-- 										</ul> -->
<!-- 									</div> -->
									<!--/End Progress-->
<!-- 								</div> -->
								<!--/End step-progress-->
<!-- 							</div> -->
							<!--/End row-fluid-->
<!-- 						</div> -->
						<!-- End .well-step -->
<!-- 						</div> -->
<%-- 					</c:otherwise> --%>
<%-- 				</c:choose>  --%>
			
			
								<c:if test="${!empty errorMsg}">
									<div class="alert alert-error alert-error-custom">
										<c:out value="${errorMsg}"></c:out>
									</div>
								</c:if>									
				

			<form id="frmVerifyPhoneNo" name="frmVerifyPhoneNo" method="post" class="form-horizontal gutter10-t" action="<c:url value="/account/unlock/sendcodeunlocking"/>">
				<df:csrfToken/>
				<div class="control-group">
					<label for="phoneNumber" class="control-label">Phone Number</label>
						<div class="controls">
						<%-- <c:set var="phoneNumber" value="1234567890"/> --%>
							<input type="text" name="phoneNumberToDisplay"  id="phoneNumberToDisplay" class="input-small" readonly value="(***)-***-${fn:substring(phoneNumber, fn:length(phoneNumber) - 4, fn:length(phoneNumber))}">
							<input type="hidden" name="phoneNumber"  id="phoneNumber" value="<encryptor:enc value="${phoneNumber}"/>">
							
						 	<select name="codeType"id="select-codetye" class="input-medium">
									<option ${codeType == 'sms' ? 'selected="selected"' : ''} value="sms">SMS</option>
                                    <option ${codeType == 'call' ? 'selected="selected"' : ''} value="call">VOICE</option>                                                                         
							</select>	
							
							<input type="submit" id="sendCodeBtn" value="Send Code" class="btn btn-primary" title="Send Code">								
						</div>
				</div>	
				</form>
				
				<form class="form-horizontal" id="frmVerifyCode"   method="post" name="frmVerifyCode" action="<c:url value="/account/unlock/sendcodeunlocking" />">
					<df:csrfToken/>
					<div class="control-group">
						<label for="phoneCode" class="control-label">Enter Code</label>
							<div class="controls">
								<input type="text" name="phoneCode"  id="phoneCode" name="phoneCode" class="xlarge" size="30">	
								<div class="error help-inline" id="phoneCode_error"></div>				
								

							</div>
					</div>				
					
					<div class="control-group">
						<div class="controls">							
							<input type="submit" value="Verify Code" class="btn btn-primary btn-large" title="Verify Code">
						</div>
					</div>		
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">

$( document ).ready(function() {
	//Form validator
	var validator = $("#frmVerifyCode").validate({ 
		rules : {
			phoneCode: { 
				required : true	
			}
		},
		messages : {
			phoneCode: { 
				required : "<span> <em class='excl'>!</em>Code is required.</span>"
				}
		},
		onkeyup: false,
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		}
	});	
});


</script>