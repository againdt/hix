<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script type="text/javascript" src="../resources/js/jquery.validate.min.js"></script>

<!-- <body id="broker-registration-confirm"> -->
	<!-- start of secondary navbar - TO DO: Move to topnav.jsp -->
<!-- 	<ul class="nav nav-tabs"> -->
<!-- 		<li class="active"><a href="#">Home</a></li> -->
<!-- 		<li><a href="#">Find Insurance</a></li> -->
<!-- 		<li><a href="#">Learn More</a></li>    -->
<!-- 		<li><a href="#">Get Assistance</a></li>    -->
<!-- 	</ul> -->
	<!-- end of secondary navbar -->
<div class="gutter10">
<div class="row-fluid">
	<h1>Thank you!</h1>
</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
		<!--  beginning of side bar -->
		<ul class="nav nav-list">
			<li class="nav-header">Registration</li>
		</ul>
		<!-- end of side bar -->
		</div><!-- end of span3 -->

		<div class="span9" id="rightpanel">
			<div class="header">
				<h4>Thank you!</h4>
				
			</div><!-- end of page-header -->

			<div class="alert alert-info">
				<p>Please check your email inbox and your spam folder for the confirmation email. Click the link provided to activate your account with GetInsured Health Exchange.</p>
			</div>
		</div><!-- end of span9 -->
	</div><!--  end of row-fluid -->
</div>
