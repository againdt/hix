package com.getinsured.iex.util;

import java.net.URI;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.lce.ChangedApplicant;
import com.getinsured.iex.lce.RequestParamDTO;
import com.getinsured.iex.org.nmhix.ssa.person.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * 
 * Life Change Event Utility Class.
 *
 */
@Component
public class LifeChangeEventUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LifeChangeEventUtil.class);
	
	@Autowired 
	private UserService userService;
	
	/**
	 * @see LifeChangeEventUtil#isEmpty(String)
	 * 
	 *      Checks for null or blank String objects
	 * 
	 * @param value
	 *            the string to check
	 * @return true or false 
	 */
	public static boolean isEmpty(String value) {
		return (value != null && !"".equals(value.trim())) ? false : true;
	}
	
	public static boolean isNull(Object value){
		return (value!=null && !"null".equals(value.toString())?false:true);
	}
	
	public static String getJsonAsString(JsonElement value){
		return (value!=null && !value.isJsonNull()?value.getAsString():null);
	}
	
	/**
	 * @see LifeChangeEventUtil#formatSSN(String)
	 * 
	 *      removes all '-' from SSN
	 * 
	 * @param ssn
	 *            the ssn to be formatted
	 * @return formatted ssn 
	 */
	public static String formatSSN(String ssn){
		return ssn.replaceAll("[- ]+", "");
	}
	
	/**
	 * @see LifeChangeEventUtil#getIdFromLink(String)
	 * 
	 * Fetch the application id from the given link/url
	 * 
	 * @param link
	 * @return ssapApplicatinId
	 * @throws Exception
	 */
	public static long getIdFromLink(String link) throws Exception{
		LOGGER.info("Get SSAP Applicant ID by parsing given link : Starts : Link : "+link);
		long ssapApplicantRowId = 0;
		
		try {
			if(!isEmpty(link)){
				ssapApplicantRowId = Long.valueOf(link.substring(link.lastIndexOf(LifeChangeEventConstant.SLASH)+1));
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while fetching SsapApplicant ID from Json : " , exception);
			throw exception;
		}
		
		LOGGER.info("Get SSAP Applicant ID by parsing given link : Ends : "+ssapApplicantRowId);
		return ssapApplicantRowId;
	}
	
	public static Long getIdFromLinks(Object links, String index) throws Exception{
		LOGGER.info("Get SSAP Applicant ID by parsing given link : Starts");
		String link = null;
		long ssapApplicantRowId = 0;
		
		try {
			if(links!=null){
				JSONObject jsonObjectLinks = new JSONObject((Map<String,String>)links);
				Map<String,String> self = (Map<String,String>)jsonObjectLinks.get(index);
				String selfHref= (self.get(LifeChangeEventConstant.HREF)==null)?"":self.get(LifeChangeEventConstant.HREF).toString();
				if("".equals(selfHref)) {
					throw new Exception("The end point to update the applicant is incorrect or not defined");
				}
				else {
					link = URI.create(selfHref).toString();
					ssapApplicantRowId = Long.valueOf(link.substring(link.lastIndexOf(LifeChangeEventConstant.SLASH)+1));
				}
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while fetching SsapApplicant ID from Json : " , exception);
			throw exception;
		}
		
		LOGGER.info("Get SSAP Applicant ID by parsing given link : Ends : "+link);
		return ssapApplicantRowId;
	}
	
	private static boolean isTrue(JsonElement livesWithHouseHoldContactIndicator) {
		return (livesWithHouseHoldContactIndicator !=null && !"null".equals(livesWithHouseHoldContactIndicator.toString()) && "true".equals(livesWithHouseHoldContactIndicator.toString())? true: false);
	}

	/**
	 * @see LifeChangeEventUtil#parseJSON(String)
	 * 
	 *      parsing the JSON with given String
	 * 
	 * @param jsonString
	 *            the JSON string to be parsed
	 * @return Object representation of JSON
	 * @throws Exception 
	 */
	public static SingleStreamlinedApplication parseJSON(String jsonString) {
		LOGGER.info("Parse JSON : Starts");

		Gson gson = new Gson();
		JsonParser jsonParser = new JsonParser();
		SingleStreamlinedApplication singleStreamlinedApplication = null;
		try {
			JsonObject jsonObject = jsonParser.parse(jsonString).getAsJsonObject();
			singleStreamlinedApplication = gson.fromJson(jsonObject.get(LifeChangeEventConstant.SINGLE_STREAMLINED_APPLICATION), SingleStreamlinedApplication.class);
		} catch (Exception e) {
			LOGGER.error("Exception occurred while parsing JSON string: " , e);
		}

		LOGGER.info("Parse JSON : Ends");
		return singleStreamlinedApplication;
	}
	
	
	public static String getLink(Object links, String index) throws Exception{
		LOGGER.info("Get SSAP Applicant ID by parsing given link : Starts");
		String link = null;
		
		try {
			if(links!=null){
				JSONObject jsonObjectLinks = new JSONObject((Map<String,String>)links);
				Map<String,String> self = (Map<String,String>)jsonObjectLinks.get(index);
				String selfHref= (self.get(LifeChangeEventConstant.HREF)==null)?"":self.get(LifeChangeEventConstant.HREF).toString();

				if("".equals(selfHref)) {
					throw new Exception("The end point to update the applicant is incorrect or not defined");
				}
				else {
					link = URI.create(selfHref).toString();
				}
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while fetching SsapApplicant ID from Json : " , exception);
			throw exception;
		}
		
		LOGGER.info("Get SSAP Applicant ID by parsing given link : Ends : "+link);
		return link;
	}
	
	public static String setApplicationIdAndGuid(Long applicationId, String ssapJson, String guid) throws Exception {

		JsonParser parser = new JsonParser();
		JsonObject  singleStreamlinedApplication;
		JsonObject jsonObject = null;
		JsonElement applicationGuid;
		JsonElement ssapApplicationId;

		try {
			Object obj = parser.parse(ssapJson);
			jsonObject = (JsonObject) obj;
			
			singleStreamlinedApplication = jsonObject.get(LifeChangeEventConstant.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();
			
			ssapApplicationId = singleStreamlinedApplication.get(LifeChangeEventConstant.SSAP_APPLICATION_ID);
			applicationGuid = singleStreamlinedApplication.get(LifeChangeEventConstant.APPLICATION_GUID);
			
			if(ssapApplicationId!=null){
				singleStreamlinedApplication.remove(LifeChangeEventConstant.SSAP_APPLICATION_ID);
			}
			
			if(applicationGuid!=null){
				singleStreamlinedApplication.remove(LifeChangeEventConstant.APPLICATION_GUID);
			}
			
			singleStreamlinedApplication.addProperty(LifeChangeEventConstant.SSAP_APPLICATION_ID, applicationId.toString());
			singleStreamlinedApplication.addProperty(LifeChangeEventConstant.APPLICATION_GUID, guid);
		} catch (Exception exception) {
			throw exception;
		}
		
		return jsonObject.toString();
	}
	
	public static String setApplicantsGuid(String ssapJson, Map<String, String> applicantsGuid) throws Exception {
		JsonParser parser = new JsonParser();
		JsonObject  singleStreamlinedApplication;
		JsonArray taxHousehold;
		JsonArray householdMembers;
		String personId;
		JsonObject jsonObject = null;
		JsonElement applicantGuid;
		
		try {
			Object obj = parser.parse(ssapJson);
			jsonObject = (JsonObject) obj;
			
			singleStreamlinedApplication = jsonObject.get(LifeChangeEventConstant.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();
			taxHousehold = singleStreamlinedApplication.getAsJsonArray(LifeChangeEventConstant.TAX_HOUSEHOLD);
			householdMembers = taxHousehold.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_MEMBER).getAsJsonArray();
			
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				JsonObject householdMember = iterator.next().getAsJsonObject();
				personId = householdMember.get(LifeChangeEventConstant.PERSON_ID).getAsString();
				
				applicantGuid = householdMember.get(LifeChangeEventConstant.APPLICANT_GUID);
				
				if(applicantGuid!=null){
					householdMember.remove(LifeChangeEventConstant.APPLICANT_GUID);
				}
				
				householdMember.addProperty(LifeChangeEventConstant.APPLICANT_GUID, applicantsGuid.get(personId));
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		return jsonObject.toString();
	}
	
	public static String setPersonIdInSsapJson(String ssapJson, long personId) throws Exception {

		JsonParser parser = new JsonParser();
		JsonObject  singleStreamlinedApplication;
		JsonArray taxHousehold;
		JsonArray householdMembers;
		JsonElement personObj;
		JsonObject jsonObject = null;

		try {
			Object obj = parser.parse(ssapJson);
			jsonObject = (JsonObject) obj;
			
			singleStreamlinedApplication = jsonObject.get(LifeChangeEventConstant.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();
			taxHousehold = singleStreamlinedApplication.getAsJsonArray(LifeChangeEventConstant.TAX_HOUSEHOLD);
			householdMembers = taxHousehold.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_MEMBER).getAsJsonArray();
			
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				JsonObject householdMember = iterator.next().getAsJsonObject();
				personObj = householdMember.get(LifeChangeEventConstant.PERSON_ID);
				if(personObj==null){
					householdMember.addProperty(LifeChangeEventConstant.PERSON_ID, personId);
					break;
				}
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		return jsonObject.toString();
	}
	
	public static String getApplicantEmailAddress(String ssapJson, long pPersonId) throws Exception {

		JsonParser parser = new JsonParser();
		JsonObject  singleStreamlinedApplication;
		JsonArray taxHousehold;
		JsonArray householdMembers;
		JsonObject jsonObject = null;
		String personId;
		JsonObject householdContact;
		JsonObject contactPreferences;
		String emailAddress = null;

		try {
			Object obj = parser.parse(ssapJson);
			jsonObject = (JsonObject) obj;
			
			singleStreamlinedApplication = jsonObject.get(LifeChangeEventConstant.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();
			taxHousehold = singleStreamlinedApplication.getAsJsonArray(LifeChangeEventConstant.TAX_HOUSEHOLD);
			householdMembers = taxHousehold.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_MEMBER).getAsJsonArray();
			
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				JsonObject householdMember = iterator.next().getAsJsonObject();
				personId = householdMember.get(LifeChangeEventConstant.PERSON_ID).getAsString();
				
				if(personId.equals(pPersonId)){
					householdContact = householdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
					contactPreferences = householdContact.get(LifeChangeEventConstant.CONTACT_PREFERENCES).getAsJsonObject();
					emailAddress = contactPreferences.get(LifeChangeEventConstant.EMAIL_ADDRESS).getAsString();
					break;
				}
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		return emailAddress;
	}
	
	public Household setNameInCmrHousehold(JsonObject householdMember, Household household) throws Exception {
		JsonObject name;
		String firstName;
		String lastName;
		String middleName;
		try{
            name = householdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject();
			firstName = LifeChangeEventUtil.getJsonAsString(name.get(LifeChangeEventConstant.FIRST_NAME));
			lastName = LifeChangeEventUtil.getJsonAsString(name.get(LifeChangeEventConstant.LAST_NAME));
			middleName = LifeChangeEventUtil.getJsonAsString(name.get(LifeChangeEventConstant.MIDDLE_NAME));
			household.setFirstName(firstName);
			household.setLastName(lastName);
			household.setMiddleName(middleName);
			
			if(household.getUser() != null){
				household.getUser().setFirstName(firstName);
				household.getUser().setLastName(lastName);
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		return household;
	}
	
	public static JsonArray getHouseholdMembers(String jsonString) throws Exception {

		JsonParser parser = new JsonParser();
		JsonObject  singleStreamlinedApplication;
		JsonArray taxHousehold;
		JsonArray householdMembers;

		try {
			Object obj = parser.parse(jsonString);
			JsonObject jsonObject = (JsonObject) obj;
			
			singleStreamlinedApplication = jsonObject.get(LifeChangeEventConstant.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();
			
			taxHousehold = singleStreamlinedApplication.getAsJsonArray(LifeChangeEventConstant.TAX_HOUSEHOLD);
			
			householdMembers = taxHousehold.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_MEMBER).getAsJsonArray();
		} catch (Exception exception) {
			throw exception;
		}
		
		return householdMembers;
	}
	
	public static Date getDate(String dateOfChange) {
		Date eventDate = null;
		try {
			SimpleDateFormat sm = new SimpleDateFormat(LifeChangeEventConstant.DATE_FORMAT);
			eventDate = sm.parse(dateOfChange);
		} catch (Exception e) {

		}
		
		if(eventDate == null) {
			eventDate = new TSDate();
		}
		return eventDate;
	}


	public static String removePersonFromJson(String personId, String ssapJson) throws Exception {
		LOGGER.info("Remove Person From Json : Starts");

		JsonParser parser = new JsonParser();
		JsonObject  singleStreamlinedApplication;
		JsonArray taxHousehold;
		JsonArray householdMembers;
		JsonArray newArrayOfMembers = new JsonArray();
		JsonElement personObj;
		String personID;
		JsonObject jsonObject = null;

		try {
			Object obj = parser.parse(ssapJson);
			jsonObject = (JsonObject) obj;
			
			singleStreamlinedApplication = jsonObject.get(LifeChangeEventConstant.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();
			taxHousehold = singleStreamlinedApplication.getAsJsonArray(LifeChangeEventConstant.TAX_HOUSEHOLD);
			householdMembers = taxHousehold.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_MEMBER).getAsJsonArray();
			
			for(JsonElement element:householdMembers){
				JsonObject householdMember = element.getAsJsonObject();
				personObj = householdMember.get(LifeChangeEventConstant.PERSON_ID);
				
				if(personObj!=null ){
					personID = householdMember.get(LifeChangeEventConstant.PERSON_ID).getAsString();
					if((!personID.equals(personId))|| personId.equals("1") || personId.equals("01")){
						newArrayOfMembers.add(element);
					}
				} 
			}
			taxHousehold.get(0).getAsJsonObject().remove(LifeChangeEventConstant.HOUSEHOLD_MEMBER);
			taxHousehold.get(0).getAsJsonObject().add(LifeChangeEventConstant.HOUSEHOLD_MEMBER, newArrayOfMembers);
		} catch (Exception exception) {
			throw exception;
		}
		
		LOGGER.info("Remove Person From Json : Ends");
		
		return jsonObject.toString();
	}
	
	public static String updateRelationshipCode(String personId, String ssapJson) throws Exception {
		
		LOGGER.info("Update Relationship Code : Starts"+personId);

		JsonParser parser = new JsonParser();
		JsonObject  singleStreamlinedApplication;
		JsonArray taxHousehold;
		JsonArray householdMembers;
		JsonObject jsonObject = null;
		JsonArray bloodRelationshipArray;
		JsonArray newBloodRelationshipArray = new JsonArray();
		String relatedTo;
		String individualPersonId;
		JsonObject bloodRelationship;
		JsonObject primaryMember;

		try {
			Object obj = parser.parse(ssapJson);
			jsonObject = (JsonObject) obj;
			
			singleStreamlinedApplication = jsonObject.get(LifeChangeEventConstant.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();
			taxHousehold = singleStreamlinedApplication.getAsJsonArray(LifeChangeEventConstant.TAX_HOUSEHOLD);
			householdMembers = taxHousehold.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_MEMBER).getAsJsonArray();
			
			primaryMember = householdMembers.get(0).getAsJsonObject();
			bloodRelationshipArray = primaryMember.getAsJsonArray(LifeChangeEventConstant.BLOOD_RELATIONSHIP);
			
			if(bloodRelationshipArray!=null && bloodRelationshipArray.size()>0){
				for(JsonElement element:bloodRelationshipArray){
					bloodRelationship = element.getAsJsonObject();
					relatedTo = bloodRelationship.get(LifeChangeEventConstant.RELATED_PERSON_ID).getAsString();
					individualPersonId = bloodRelationship.get(LifeChangeEventConstant.INDIVIDUAL_PERSON_ID).getAsString();
					if( (!personId.equals(relatedTo) && !personId.equals(individualPersonId)) || personId.equals("1") || personId.equals("01") ){
						newBloodRelationshipArray.add(element);
					} 
				}
			}
			
			primaryMember.remove(LifeChangeEventConstant.BLOOD_RELATIONSHIP);
			primaryMember.add(LifeChangeEventConstant.BLOOD_RELATIONSHIP, newBloodRelationshipArray);
		} catch (Exception exception) {
			throw exception;
		}
		
		LOGGER.info("Update Relationship Code : Ends");
		
		return jsonObject.toString();
	}
	
	public static String getSpousePersonId(String ssapJson) throws Exception {
		JsonParser parser = new JsonParser();
		JsonObject  singleStreamlinedApplication;
		JsonArray taxHousehold;
		JsonArray householdMembers;
		JsonObject jsonObject = null;
		JsonArray bloodRelationshipArray;
		String relatedTo;
		String relation;
		String individualPersonId;
		JsonObject bloodRelationship;
		JsonObject primaryMember;

		try {
			Object obj = parser.parse(ssapJson);
			jsonObject = (JsonObject) obj;
			
			singleStreamlinedApplication = jsonObject.get(LifeChangeEventConstant.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();
			taxHousehold = singleStreamlinedApplication.getAsJsonArray(LifeChangeEventConstant.TAX_HOUSEHOLD);
			householdMembers = taxHousehold.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_MEMBER).getAsJsonArray();
			
			primaryMember = householdMembers.get(0).getAsJsonObject();
			bloodRelationshipArray = primaryMember.getAsJsonArray(LifeChangeEventConstant.BLOOD_RELATIONSHIP);
			
			if(bloodRelationshipArray!=null && bloodRelationshipArray.size()>0){
				for(JsonElement element:bloodRelationshipArray){
					bloodRelationship = element.getAsJsonObject();
					relatedTo = bloodRelationship.get(LifeChangeEventConstant.RELATED_PERSON_ID).getAsString();
					individualPersonId = bloodRelationship.get(LifeChangeEventConstant.INDIVIDUAL_PERSON_ID).getAsString();
					relation = bloodRelationship.get(LifeChangeEventConstant.RELATION).getAsString();
					if(String.valueOf(LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID).equals(individualPersonId) && LifeChangeEventConstant.SPOUSE_RELATIONSHIP_CODE.equals(relation)){
						return relatedTo;
					}
				}
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		return null;
	}

	
	public static JsonObject getHouseholdMember(JsonArray householdMembers, String personId) throws Exception {
		LOGGER.info("Get Tax Filer : Starts");
		JsonObject householdMember;
		String person;
		
		try {
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				householdMember = iterator.next().getAsJsonObject();
				person = householdMember.get(LifeChangeEventConstant.PERSON_ID).getAsString();
				if(person.equals(personId)){
					return householdMember;
				}
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while fetching Household Member by Person ID : " , exception);
			throw exception;
		}

		LOGGER.info("Get Household Member by Person ID : Ends ");
		return null;
	}
	
	public static boolean checkDeletedApplicants(JsonObject householdMember){
		JsonElement active;
		boolean flag = false;
		String status;
		
		try {
			active = householdMember.get(LifeChangeEventConstant.ACTIVE);
			if(active!=null){
				status = active.getAsString();
				if(!LifeChangeEventUtil.isEmpty(status) && LifeChangeEventConstant.FALSE.equals(status)){
					flag = true;
				}
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		return flag;
	}
	
	public static long getApplicantId(SsapApplicantResource ssapApplicantResource) throws Exception{
		return getIdFromLinks(ssapApplicantResource.get_links(), LifeChangeEventConstant.SELF);
	}

	public static Object removeSsapApplicationFromApplicant(Object links){
		JSONObject jsonObjectLinks;
		Map<String, LinkedHashMap<String, String>> map = new LinkedHashMap<>();
		Map<String, LinkedHashMap<String, String>> updatedMap = new LinkedHashMap<>();
		Set<Entry<String, LinkedHashMap<String, String>>> entries = new HashSet<>();
		
		try{
			jsonObjectLinks = new JSONObject((Map<String,LinkedHashMap<String, String>>)links);
			map = (Map<String,LinkedHashMap<String, String>>)links;
			entries = map.entrySet();
			
			for (Map.Entry<String, LinkedHashMap<String, String>> entry : map.entrySet() ) {
				if("ssapApplication".equals(entry.getKey())){
					updatedMap.put(entry.getKey(), null);
				} else {
					updatedMap.put(entry.getKey(), entry.getValue());
				}
			}
			
			jsonObjectLinks.remove("ssapApplication");
		} catch(Exception exception){
			throw exception;
		}
		
		return (Object)updatedMap;
	}
	
	public static String removeSsapApplicationLink(SsapApplicantResource ssapApplicantResource) throws Exception{
		String link = null;
		StringBuilder sb = new StringBuilder();
		
		try {
			Object links = ssapApplicantResource.get_links();
			
			if(links!=null){
				JSONObject jsonObjectLinks = new JSONObject((Map<String,String>)links);
				Map<String,String> self = (Map<String,String>)jsonObjectLinks.get("ssapApplication");
				String selfHref= (self.get(LifeChangeEventConstant.HREF)==null)?"":self.get(LifeChangeEventConstant.HREF).toString();
				URI myUri = null;
				if("".equals(selfHref)) {
					throw new Exception("The end point to update the applicant is incorrect or not defined");
				}
				else {
					myUri = URI.create(selfHref);
					link = myUri.toString();
				}
				sb.append(link.substring(0, link.lastIndexOf("/")+1)).append("0");
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while fetching SsapApplicant ID from Json : " , exception);
			throw exception;
		}
		
		return sb.toString();
	}
	
	public static Date formatDate(String date) throws Exception{
		DateFormat dateFormat = new SimpleDateFormat(LifeChangeEventConstant.DATE_FORMAT);
		SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss aa");	
		Calendar calendar = new GregorianCalendar();
		Date formattedDate = null;
		
		try{
			calendar.setTime(formatter.parse(date));
			formattedDate = dateFormat.parse(dateFormat.format(calendar.getTime()));
		} catch(Exception exception){
			throw exception;
		}
		
		return formattedDate;
	}
	
	public static String formatTimestamp(Timestamp tStamp ) throws Exception{
		DateFormat dateFormat = new SimpleDateFormat(LifeChangeEventConstant.DATE_FORMAT);
		String formattedDate = null;
		
		try{
			formattedDate =  dateFormat.format( tStamp );
		} catch(Exception exception){
			throw exception;
		}
		
		return formattedDate;
	}
	
	public static Date parseDate(String date) {
		String[] dateFormats = { "MM/dd/yyyy", "MMM dd, yyyy HH:mm:ss aa" };

		for (String format : dateFormats) {
			try {
				return new SimpleDateFormat(format).parse(date);
			} catch (Exception exception) {
				
			}
		}
		return null;
	}
	
	public static Date setYearInDate(String date, int noOfYears){
		Calendar c = TSCalendar.getInstance();
		c.setTime(parseDate(date));
		c.add(Calendar.YEAR, noOfYears);
		return c.getTime();
	}
	
	/**
	 * Convert exception trace to Json
	 * 
	 * @param e
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static String exceptionToJson(Exception exception){
		if(exception == null){
			return "None";
		}
		JSONObject obj  = new JSONObject();
		JSONObject error = new JSONObject();
		error.put("Exception Type", exception.getClass().getName());
		error.put("ExceptionMessage", exception.getMessage());
		error.put("ExceptionCause", exception.getCause());
		obj.put("Exception", error);
		
		String response = obj.toJSONString();
		response = response.replaceAll("\\\\", "");
		response = response.replaceAll("(\"\\{)", "\\{");
		response = response.replaceAll("(\\}\")", "\\}");
		
		return response;
	}
	
	/**
	 * @see SSAPUtils#createGIWSPayload(Throwable, String)
	 * 
	 *     creates GIWSPayload record 
	 * 
	 * @param throwable
	 * @param json    
	 *            
	 * @return payload 
	 */
	public static GIWSPayload createGIWSPayload(Exception exception, String json, String operation){
		GIWSPayload payload = new GIWSPayload();
		payload.setExceptionMessage(exceptionToJson(exception));
		payload.setStatus(LifeChangeEventConstant.FAILED);
		payload.setCreatedTimestamp(new TSDate());
		payload.setEndpointFunction(LifeChangeEventConstant.ENDPOINT_FUNCTION);
		payload.setRequestPayload(json);
		payload.setEndpointOperationName(operation);
		
		return payload;
	}
	
	public static List<String> getEventSubCategories(){
		List<String> list = new ArrayList<>();
		list.add("BIRTH");
		list.add("ADOPTION");
		list.add("REMOVE_DEPENDENT");
		list.add("DEATH_OF_DEPENDENT");
		list.add("DEPENDENT_CHILD_AGES_OUT");
		list.add("DEPENDENT_AGES_OUT");
		list.add("ADD_DEPENDENT");
		list.add("DEATH_OF_PRIMARY_APPLICANT");
		list.add("HEAD_OF_HOUSEHOLD");
		list.add("EMPLOYER_SPONSORED");
		list.add("OTHER_MINIMUM_ESSENTIAL_COVERAGE_ENDS");
		list.add("INCARCERATION");
		list.add("LAWFUL_PRESENCE");
		list.add("CHANGE_IN_RESIDENCE_OR_MAILING_ADDRESS");
		list.add("INCOME");
		list.add("REACH_AGE_65_OR_MEDICARE");
		list.add("TRICARE_OR_VETERAN_AFFAIR");
		list.add("NEW_THROUGH_SPOUSE");
		list.add("OTHER_MINIMUM_ESSENTIAL_COVERAGE_BEGINS");
		
		return list;
	}
	
	public static String updateJsonForDemographicEvents(String eventName, String pPersonId, String oldSsapJson, String newSsapJson) throws Exception {
		LOGGER.info("Update Json For Demographic Events : Starts");
		
		JsonParser parser = new JsonParser();
		JsonObject  singleStreamlinedApplication;
		JsonArray taxHousehold;
		JsonArray householdMembers;
		String personId;
		JsonObject jsonObject = null;
		JsonObject modifiedHouseholdMember;
		
		try {
			Object obj = parser.parse(oldSsapJson);
			jsonObject = (JsonObject) obj;
			
			singleStreamlinedApplication = jsonObject.get(LifeChangeEventConstant.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();
			taxHousehold = singleStreamlinedApplication.getAsJsonArray(LifeChangeEventConstant.TAX_HOUSEHOLD);
			householdMembers = taxHousehold.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_MEMBER).getAsJsonArray();
			
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				JsonObject householdMember = iterator.next().getAsJsonObject();
				personId = householdMember.get(LifeChangeEventConstant.PERSON_ID).getAsString();
				
				if(pPersonId.equals(personId)){
					modifiedHouseholdMember = getHouseholdMember(newSsapJson, pPersonId);
					
					if(LifeChangeEventConstant.DEMOGRAPHICS_NAME_CHANGE.equals(eventName)){
						householdMember = updateChangeInNameData(householdMember, modifiedHouseholdMember,personId);
					} else if(LifeChangeEventConstant.DEMOGRAPHICS_SSN_CHANGE.equals(eventName)){
						householdMember = updateChangeInSsnData(householdMember, modifiedHouseholdMember);
					} else if(LifeChangeEventConstant.DEMOGRAPHICS_MAILING_ADDRESS_CHANGE.equals(eventName)){
						householdMember = updateChangeInMailingAddressData(householdMember, modifiedHouseholdMember);
					} else if(LifeChangeEventConstant.ADDRESS_CHANGE_WITHIN_STATE.equals(eventName)) {
						householdMember = updateHomeAddressData(householdMember, modifiedHouseholdMember, personId);
					} else if(LifeChangeEventConstant.DEMOGRAPHICS_ETHNICITY_RACE_CHANGE.equals(eventName)) {
						householdMember = updateEthnicityAndRaceData(householdMember, modifiedHouseholdMember);
					} else if(LifeChangeEventConstant.DEMOGRAPHICS_GENDER_CHANGE.equals(eventName)) {
						householdMember = updateGenderChangeData(householdMember, modifiedHouseholdMember);
					}else if(LifeChangeEventConstant.DEMOGRAPHICS_PHONE_NUMBER_CHANGE.equals(eventName)) {
						householdMember = updatePhoneNumberData(householdMember, modifiedHouseholdMember,personId,householdMembers);
					}else if(LifeChangeEventConstant.DEMOGRAPHICS_EMAIL_CHANGE.equals(eventName)) {
						householdMember = updateEmailData(householdMember, modifiedHouseholdMember,personId);
					}
					
					
					break;
				}
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		LOGGER.info("Update Json For Demographic Events : Ends");
		
		return jsonObject.toString();
	}
	
	private static JsonObject updateHomeAddressData(JsonObject householdMember, JsonObject modifiedHouseholdMember, String personId) {
		if(personId.equals(LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID)) {
			return updateHomeAddressJson(householdMember, modifiedHouseholdMember);
		}
		else {			
			JsonElement livesWithHouseHoldContactIndicator = modifiedHouseholdMember.get(LifeChangeEventConstant.LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR);
			if(modifiedHouseholdMember.get(LifeChangeEventConstant.LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR) != null &&  !modifiedHouseholdMember.get(LifeChangeEventConstant.LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR).isJsonNull()){
				householdMember.addProperty(LifeChangeEventConstant.LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR,modifiedHouseholdMember.get(LifeChangeEventConstant.LIVES_WITH_HOUSEHOLD_CONTACT_INDICATOR).getAsBoolean());
			}
			if(modifiedHouseholdMember.get("livesAtOtherAddressIndicator") != null && !modifiedHouseholdMember.get("livesAtOtherAddressIndicator").isJsonNull()){
				householdMember.addProperty("livesAtOtherAddressIndicator",modifiedHouseholdMember.get("livesAtOtherAddressIndicator").getAsBoolean());
			}
			if(modifiedHouseholdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT) !=null && modifiedHouseholdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject().get("homeAddressIndicator") != null &&  !modifiedHouseholdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject().get("homeAddressIndicator").isJsonNull() && householdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT) != null){
				householdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject().addProperty("homeAddressIndicator",modifiedHouseholdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject().get("homeAddressIndicator").getAsBoolean());
			}
			if(isTrue(livesWithHouseHoldContactIndicator)) {
				return updateHomeAddressJson(householdMember, modifiedHouseholdMember);
			}
			else {
				return updateOtherAddressJson(householdMember, modifiedHouseholdMember);
			}
		}
	}
	private static JsonObject updateEthnicityAndRaceData(JsonObject householdMember, JsonObject modifiedHouseholdMember) {
		
		JsonParser parser = new JsonParser();
		Object obj = parser.parse(modifiedHouseholdMember.get("ethnicityAndRace").toString());
		JsonObject jsonObject = (JsonObject) obj;
		householdMember.getAsJsonObject().add("ethnicityAndRace",jsonObject);
		return householdMember;
	}

	private static JsonObject updateGenderChangeData(JsonObject householdMember, JsonObject modifiedHouseholdMember) {
		householdMember.addProperty(LifeChangeEventConstant.GENDER, getJsonAsString(modifiedHouseholdMember.get(LifeChangeEventConstant.GENDER)));
		return householdMember;
	}
	
	private static JsonObject updatePhoneNumberData(JsonObject householdMember, JsonObject modifiedHouseholdMember,String personId,JsonArray householdMembers) {
		if(personId !=null && personId.equals(String.valueOf(LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID))){
			JsonObject householdContact = householdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
			JsonObject modifiedHouseholdContact = modifiedHouseholdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
			if(householdContact != null && !householdContact.isJsonNull() && modifiedHouseholdContact !=null && !modifiedHouseholdContact.isJsonNull()){
				Iterator<JsonElement> iterator =  householdMembers.iterator();
				while(iterator.hasNext()){
					JsonObject householdObject  = iterator.next().getAsJsonObject();
					JsonObject householdContactObject = householdObject.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
					householdContactObject.get(LifeChangeEventConstant.PHONE).getAsJsonObject().addProperty(LifeChangeEventConstant.PHONE_NUMBER,getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.PHONE).getAsJsonObject().get(LifeChangeEventConstant.PHONE_NUMBER)));
					householdContactObject.get(LifeChangeEventConstant.OTHER_PHONE).getAsJsonObject().addProperty(LifeChangeEventConstant.PHONE_NUMBER,getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.OTHER_PHONE).getAsJsonObject().get(LifeChangeEventConstant.PHONE_NUMBER)));
					householdContactObject.get(LifeChangeEventConstant.OTHER_PHONE).getAsJsonObject().addProperty(LifeChangeEventConstant.PHONE_EXTENSTION,getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.OTHER_PHONE).getAsJsonObject().get(LifeChangeEventConstant.PHONE_EXTENSTION)));
					
				}
				
			}
		}
		return householdMember;
	}
	
	private static JsonObject updateEmailData(JsonObject householdMember, JsonObject modifiedHouseholdMember,String personId) {
		if(personId !=null && personId.equals(String.valueOf(LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID))){
			JsonObject householdContact = householdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
			JsonObject modifiedHouseholdContact = modifiedHouseholdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
			if(householdContact != null && !householdContact.isJsonNull() && modifiedHouseholdContact !=null && !modifiedHouseholdContact.isJsonNull()){
				householdContact.get(LifeChangeEventConstant.CONTACT_PREFERENCES).getAsJsonObject().addProperty(LifeChangeEventConstant.EMAIL_ADDRESS,getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.CONTACT_PREFERENCES).getAsJsonObject().get(LifeChangeEventConstant.EMAIL_ADDRESS)));
				
			}
		}
		return householdMember;
	}
	
	private static JsonObject updateOtherAddressJson(JsonObject householdMember, JsonObject modifiedHouseholdMember) {
		JsonObject otherAddress = householdMember.get(LifeChangeEventConstant.OTHER_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.ADDRESS).getAsJsonObject();
		JsonObject modifiedHouseholdContact = modifiedHouseholdMember.get(LifeChangeEventConstant.OTHER_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.ADDRESS).getAsJsonObject();

		otherAddress.addProperty(LifeChangeEventConstant.STREET_ADDRESS_1, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.STREET_ADDRESS_1)));
		otherAddress.addProperty(LifeChangeEventConstant.STREET_ADDRESS_2, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.STREET_ADDRESS_2)));
		otherAddress.addProperty(LifeChangeEventConstant.CITY, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.CITY)));
		otherAddress.addProperty(LifeChangeEventConstant.STATE, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.STATE)));
		otherAddress.addProperty(LifeChangeEventConstant.POSTAL_CODE, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.POSTAL_CODE)));
		otherAddress.addProperty(LifeChangeEventConstant.COUNTY, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.COUNTY)));
		
		JsonElement primaryAddressFipsCode = modifiedHouseholdContact.get(LifeChangeEventConstant.PRIMARY_ADDRESS_COUNTY_FIPS_CODE);
				
		if(!isNull(primaryAddressFipsCode)){
				otherAddress.addProperty(LifeChangeEventConstant.PRIMARY_ADDRESS_COUNTY_FIPS_CODE, getJsonAsString(primaryAddressFipsCode));
		}
		return householdMember;
	}

	private static JsonObject updateHomeAddressJson(JsonObject householdMember,
			JsonObject modifiedHouseholdMember) {
		JsonObject householdContact = householdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
		JsonObject modifiedHouseholdContact = modifiedHouseholdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
		
		householdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.STREET_ADDRESS_1, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.STREET_ADDRESS_1)));
		householdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.STREET_ADDRESS_2, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.STREET_ADDRESS_2)));
		householdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.CITY, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.CITY)));
		householdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.STATE, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.STATE)));
		householdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.POSTAL_CODE, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.POSTAL_CODE)));
		householdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.COUNTY, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.COUNTY)));
		
		
		JsonElement primaryAddressFipsCode = modifiedHouseholdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.PRIMARY_ADDRESS_COUNTY_FIPS_CODE);
		if(!isNull(primaryAddressFipsCode)){
			householdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.PRIMARY_ADDRESS_COUNTY_FIPS_CODE, getJsonAsString(primaryAddressFipsCode));
		}
		
		LOGGER.info("Update Change In Mailing Address Data : Ends");

		return householdMember;
	}

	public static JsonObject getHouseholdMember(String ssapJson, String pPersonId) throws Exception {
		LOGGER.info("Get Household Member by Person ID : Starts ");

		JsonObject householdMember;
		String person;
		JsonParser parser = new JsonParser();
		JsonObject  singleStreamlinedApplication;
		JsonArray taxHousehold;
		JsonArray householdMembers;
		JsonObject jsonObject = null;
		
		try {
			Object obj = parser.parse(ssapJson);
			jsonObject = (JsonObject) obj;
			
			singleStreamlinedApplication = jsonObject.get(LifeChangeEventConstant.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();
			taxHousehold = singleStreamlinedApplication.getAsJsonArray(LifeChangeEventConstant.TAX_HOUSEHOLD);
			householdMembers = taxHousehold.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_MEMBER).getAsJsonArray();
			
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				householdMember = iterator.next().getAsJsonObject();
				person = householdMember.get(LifeChangeEventConstant.PERSON_ID).getAsString();
				
				if(pPersonId.equals(person)){
					return householdMember;
				}
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while fetching Household Member by Person ID : " , exception);
			throw exception;
		}

		LOGGER.info("Get Household Member by Person ID : Ends ");
		
		return null;
	}
	
	private static JsonObject updateChangeInNameData(JsonObject householdMember, JsonObject modifiedHouseholdMember,String personId){
		LOGGER.info("Update Change In Name Data : Starts");
		
		householdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().addProperty(LifeChangeEventConstant.FIRST_NAME, getJsonAsString(modifiedHouseholdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().get(LifeChangeEventConstant.FIRST_NAME)));
		householdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().addProperty(LifeChangeEventConstant.MIDDLE_NAME,getJsonAsString(modifiedHouseholdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().get(LifeChangeEventConstant.MIDDLE_NAME)));
		householdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().addProperty(LifeChangeEventConstant.LAST_NAME, getJsonAsString(modifiedHouseholdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().get(LifeChangeEventConstant.LAST_NAME)));
		householdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().addProperty(LifeChangeEventConstant.SUFFIX, getJsonAsString(modifiedHouseholdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().get(LifeChangeEventConstant.SUFFIX)));
		
		JsonObject socialSecurityCard = modifiedHouseholdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject();
		
		JsonElement nameSameOnSsnIndicator = socialSecurityCard.get(LifeChangeEventConstant.NAME_SAME_ON_SSN_INDICATOR);
		if(!isNull(nameSameOnSsnIndicator)){
			householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.NAME_SAME_ON_SSN_INDICATOR, getJsonAsString(nameSameOnSsnIndicator));
		}
		
		JsonElement firstNameOnSsnCard = socialSecurityCard.get(LifeChangeEventConstant.FIRST_NAME_ON_SSN_CARD);
		if(!isNull(firstNameOnSsnCard)){
			householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.FIRST_NAME_ON_SSN_CARD, getJsonAsString(firstNameOnSsnCard));
		}
		
		JsonElement middleNameOnSsnCard = socialSecurityCard.get(LifeChangeEventConstant.MIDDLE_NAME_ON_SSN_CARD);
		if(!isNull(middleNameOnSsnCard)){
			householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.MIDDLE_NAME_ON_SSN_CARD, getJsonAsString(middleNameOnSsnCard));
		}
		
		JsonElement lastNameOnSsnCard = socialSecurityCard.get(LifeChangeEventConstant.LAST_NAME_ON_SSN_CARD);
		if(!isNull(lastNameOnSsnCard)){
			householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.LAST_NAME_ON_SSN_CARD, getJsonAsString(lastNameOnSsnCard));
		}
		
		JsonElement suffixOnSsnCard = socialSecurityCard.get(LifeChangeEventConstant.SUFFIX_ON_SSN_CARD);
		if(!isNull(suffixOnSsnCard)){
			householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.SUFFIX_ON_SSN_CARD, getJsonAsString(suffixOnSsnCard));
		}
		
		LOGGER.info("Update Change In Name Data : Ends");
	
		return householdMember;
	}
	
	private static JsonObject updateChangeInSsnData(JsonObject householdMember, JsonObject modifiedHouseholdMember){
		LOGGER.info("Update Change In SSN Data : Starts");
		
		JsonObject socialSecurityCard = modifiedHouseholdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject();
		householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.SOCIAL_SECURITY_CARD_HOLDER_INDICATOR, getJsonAsString(modifiedHouseholdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD_HOLDER_INDICATOR)));
		householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.SOCIAL_SECURITY_NUMBER, getJsonAsString(modifiedHouseholdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().get(LifeChangeEventConstant.SOCIAL_SECURITY_NUMBER)));
		householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.NAME_SAME_ON_SSN_INDICATOR, getJsonAsString(modifiedHouseholdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().get(LifeChangeEventConstant.NAME_SAME_ON_SSN_INDICATOR)));
		householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.FIRST_NAME_ON_SSN_CARD,getJsonAsString(modifiedHouseholdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().get(LifeChangeEventConstant.FIRST_NAME_ON_SSN_CARD)));
		householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.MIDDLE_NAME_ON_SSN_CARD, getJsonAsString(modifiedHouseholdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().get(LifeChangeEventConstant.MIDDLE_NAME_ON_SSN_CARD)));
		householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.LAST_NAME_ON_SSN_CARD, getJsonAsString(modifiedHouseholdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().get(LifeChangeEventConstant.LAST_NAME_ON_SSN_CARD)));
		householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.SUFFIX_ON_SSN_CARD, getJsonAsString(modifiedHouseholdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().get(LifeChangeEventConstant.SUFFIX_ON_SSN_CARD)));
		
		JsonElement reasonableExplanationForNoSsn = socialSecurityCard.get(LifeChangeEventConstant.REASONABLE_EXPLANATION_FOR_NO_SSN);
		if(!isNull(reasonableExplanationForNoSsn)){
			householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.REASONABLE_EXPLANATION_FOR_NO_SSN, getJsonAsString(reasonableExplanationForNoSsn));
		}
		
		JsonElement individualMandateExceptionIndicator = socialSecurityCard.get(LifeChangeEventConstant.INDIVIDUAL_MANDATE_EXCEPTION_INDICATOR);
		if(!isNull(individualMandateExceptionIndicator)){
			householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.INDIVIDUAL_MANDATE_EXCEPTION_INDICATOR, getJsonAsString(individualMandateExceptionIndicator));
		}
		
		JsonElement individualMandateExceptionId = socialSecurityCard.get(LifeChangeEventConstant.INDIVIDUAL_MANDATE_EXCEPTION_ID);
		if(!isNull(individualMandateExceptionId)){
			householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject().addProperty(LifeChangeEventConstant.INDIVIDUAL_MANDATE_EXCEPTION_ID, getJsonAsString(individualMandateExceptionId));
		}
		
		LOGGER.info("Update Change In SSN Data : Ends");
	
		return householdMember;
	}
	
	private static JsonObject updateChangeInMailingAddressData(JsonObject householdMember, JsonObject modifiedHouseholdMember){
		LOGGER.info("Update Change In Mailing Address Data : Starts");
		
		JsonObject householdContact = householdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
		JsonObject modifiedHouseholdContact = modifiedHouseholdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
		
		householdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.STREET_ADDRESS_1, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.STREET_ADDRESS_1)));
		householdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.STREET_ADDRESS_2, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.STREET_ADDRESS_2)));
		householdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.CITY, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.CITY)));
		householdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.STATE, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.STATE)));
		householdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.POSTAL_CODE, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.POSTAL_CODE)));
		householdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.COUNTY, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.COUNTY)));
		
		householdContact.addProperty(LifeChangeEventConstant.MAILING_ADDRESS_SAME_AS_HOME_ADDRESS_INDICATOR, modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS_SAME_AS_HOME_ADDRESS_INDICATOR).getAsBoolean());
		JsonElement primaryAddressFipsCode = modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.PRIMARY_ADDRESS_COUNTY_FIPS_CODE);
		if(!isNull(primaryAddressFipsCode)){
			householdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.PRIMARY_ADDRESS_COUNTY_FIPS_CODE, primaryAddressFipsCode.getAsString());
		}
		
		LOGGER.info("Update Change In Mailing Address Data : Ends");
	
		return householdMember;
	}
	
	private static JsonObject updateChangeInHomeAddressData(JsonObject householdMember, JsonObject modifiedHouseholdMember){
		LOGGER.info("Update Change In Mailing Address Data : Starts");
		
		JsonObject householdContact = householdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
		JsonObject modifiedHouseholdContact = modifiedHouseholdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
		
		householdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.STREET_ADDRESS_1, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.STREET_ADDRESS_1)));
		householdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.STREET_ADDRESS_2, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.STREET_ADDRESS_2)));
		householdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.CITY, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.CITY)));
		householdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.STATE, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.STATE)));
		householdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.POSTAL_CODE, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.POSTAL_CODE)));
		householdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.COUNTY, getJsonAsString(modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.COUNTY)));
		
		householdContact.addProperty(LifeChangeEventConstant.MAILING_ADDRESS_SAME_AS_HOME_ADDRESS_INDICATOR, modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS_SAME_AS_HOME_ADDRESS_INDICATOR).getAsBoolean());
		JsonElement primaryAddressFipsCode = modifiedHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().get(LifeChangeEventConstant.PRIMARY_ADDRESS_COUNTY_FIPS_CODE);
		if(!isNull(primaryAddressFipsCode)){
			householdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject().addProperty(LifeChangeEventConstant.PRIMARY_ADDRESS_COUNTY_FIPS_CODE, getJsonAsString(primaryAddressFipsCode));
		}
		
		LOGGER.info("Update Change In Mailing Address Data : Ends");
	
		return householdMember;
	}
	
	
	public static Set<String> getDateOfBirthEvents(){
		Set<String> dobEvents = new HashSet<String>();
		dobEvents.add(LifeChangeEventConstant.DEMOGRAPHICS_DOB_CHANGE);
		dobEvents.add(LifeChangeEventConstant.DEMOGRAPHICS_DOB_CHANGE_REMOVE);
		return dobEvents;
	}
	
	public static boolean isDobEvent(String eventName){
		return getDateOfBirthEvents().contains(eventName);
	}
	
	
	public static Set<String> getDemographicEvents(){
		Set<String> demoEvents = new HashSet<String>();
		demoEvents.add(LifeChangeEventConstant.DEMOGRAPHICS_NAME_CHANGE);
		demoEvents.add(LifeChangeEventConstant.DEMOGRAPHICS_SSN_CHANGE);
		demoEvents.add(LifeChangeEventConstant.DEMOGRAPHICS_MAILING_ADDRESS_CHANGE);
		demoEvents.add(LifeChangeEventConstant.DEMOGRAPHICS_ETHNICITY_RACE_CHANGE);
		demoEvents.add(LifeChangeEventConstant.DEMOGRAPHICS_GENDER_CHANGE);
		demoEvents.add(LifeChangeEventConstant.DEMOGRAPHICS_PHONE_NUMBER_CHANGE);
		demoEvents.add(LifeChangeEventConstant.DEMOGRAPHICS_EMAIL_CHANGE);
		return demoEvents;
	}
	
	public static Set<String> getActionRequiredDemographicEvents(){
		Set<String> actionReqiredDemoEvents = new HashSet<String>();

		actionReqiredDemoEvents.add(LifeChangeEventConstant.ADDRESS_CHANGE_WITHIN_STATE);
		actionReqiredDemoEvents.add(LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_INTO_STATE);
		actionReqiredDemoEvents.add(LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_OUT_OF_STATE);
		actionReqiredDemoEvents.add(LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_OUT_OF_STATE_DEPENDENTS);
		actionReqiredDemoEvents.add(LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_INTO_STATE_OTHER);
		actionReqiredDemoEvents.add(LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_OUT_OF_STATE_OTHER);
		
		actionReqiredDemoEvents.add(LifeChangeEventConstant.DEMOGRAPHICS_DOB_CHANGE_REMOVE);
		actionReqiredDemoEvents.add(LifeChangeEventConstant.DEMOGRAPHICS_DOB_CHANGE);
		
		actionReqiredDemoEvents.add(LifeChangeEventConstant.TRIBE_STATUS_GAIN_IN_TRIBE_STATUS);
		
		actionReqiredDemoEvents.add(LifeChangeEventConstant.DEMOGRAPHICS_RELATIONSHIP_CHANGE);
		actionReqiredDemoEvents.add(LifeChangeEventConstant.DEMOGRAPHICS_RELATIONSHIP_CHANGE_REMOVE);
		
		actionReqiredDemoEvents.add(LifeChangeEventConstant.IMMIGRATION_STATUS_CHANGE_GAIN_CITIZENSHIP);
		actionReqiredDemoEvents.add(LifeChangeEventConstant.IMMIGRATION_STATUS_CHANGE_GAIN_LPR);
		actionReqiredDemoEvents.add(LifeChangeEventConstant.IMMIGRATION_STATUS_CHANGE_CHANGE_LPR);
		actionReqiredDemoEvents.add(LifeChangeEventConstant.IMMIGRATION_STATUS_CHANGE_LOSE_CITIZENSHIP);
		
		return actionReqiredDemoEvents;
	}

	public static Set<String> getNativeAmericanEvents(){
		//using set as in future new event can be added in tribes.
		Set<String> tribeEvents = new HashSet<String>();
		tribeEvents.add(LifeChangeEventConstant.GAIN_IN_TRIBE_STATUS);
		return tribeEvents;
	}
	
	public static boolean isSeekingCoverage(JsonObject householdMember){
		JsonElement seekingCoverage;
		boolean flag = false;
		
		try {
			seekingCoverage = householdMember.get(LifeChangeEventConstant.SEEKING_COVERAGE);
			if(seekingCoverage!=null){
				if(!LifeChangeEventUtil.isEmpty(seekingCoverage.getAsString()) && LifeChangeEventConstant.TRUE.equals(seekingCoverage.getAsString())){
					flag = true;
				}
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		return flag;
	}
	
	public static String getNameOfPrimaryApplicantFromSsapApplicant(List<SsapApplicant> ssapApplicants){
		String name = "";
		for(SsapApplicant ssapApplicant : ssapApplicants){
			if(ssapApplicant.getPersonId() == LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID){
				name = ssapApplicant.getFirstName() + " " + ssapApplicant.getLastName(); 
				break;
			}
		} 
		return name;
	}
	
	public static void resetTimeInCalendar(Calendar calendar){
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}

	public static void setMaxTimeForDay(Calendar calendar){
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
	}
	
	public static void validateDate(RequestParamDTO requestParamDTO){
		if( requestParamDTO.getEventSubCategory().equals(LifeChangeEventConstant.TRIBE_STATUS_GAIN_IN_TRIBE_STATUS)){
		   if(requestParamDTO.getChangedApplicants()!= null && !requestParamDTO.getChangedApplicants().isEmpty() ){
			Iterator<ChangedApplicant> itr = requestParamDTO.getChangedApplicants().iterator();
			while(itr.hasNext()){
				Date date = parseDate(itr.next().getEventDate());
				Calendar eventCal = null;
				Calendar todaysCal = null;
				if(date != null){
					eventCal =  TSCalendar.getInstance();
					eventCal.setTime(date);
					todaysCal =  TSCalendar.getInstance(); 
					setMaxTimeForDay(todaysCal);
				}
				
				if(date==null || eventCal.after(todaysCal)){
					GIRuntimeException giRuntimeException = new GIRuntimeException();
					giRuntimeException.setComponent(com.getinsured.hix.platform.util.exception.GIRuntimeException.Component.LCE.toString());
					giRuntimeException.setNestedStackTrace("Event Date is invalid or future date : "+requestParamDTO.getEventSubCategoryDate());
					giRuntimeException.setErrorMsg(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					throw giRuntimeException; 
				}
			}
		  }
		}
		
	}
	
}

