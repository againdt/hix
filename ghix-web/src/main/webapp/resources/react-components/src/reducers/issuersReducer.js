import { ActionTypes } from '../constants/constants';

const issuersReducer = (state=[], {type, payload}) => {
    switch (type) {
        case (ActionTypes.RECEIVED_ISSUERS_DATA_SUCCESS):
            return payload;
        default:
            return state;
    }
};

export default issuersReducer;