(function (views) {
	views.dentalPlans = Backbone.View.extend({
		events : {
			'click a.addToCart': 'addToCart',
			'click input.addToCart': 'addToCart',
			'click a.addToCompare' : 'addToCompare',
			'click a.removeCompare' : 'removeCompare',
			'click a.showCart' : 'showCart',
			'click a.goToCart' : 'goToCart',
			'click a.checkOut' : 'checkOut',
			'change input.filter_checkbox' : 'filter'
		},
		el : '#mainSummary',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'loadAll', 'addOne','addToCart','addToCompare','removeCompare','showCart','goToCart','checkOut', 'filter');
			var self = this;
			
			
			self.collection.fetch({
				success : function () {

					if(self.collection.length==0){
						$('#mainSummary').empty();
						$('#mainSummary').hide();
						$('#mainSummary_err').show();
					}
					
					self.collection.setSort('planScore', 'desc');
					self.collection.pager();
					self.loadAll();
					/*show/hide cart buttons depending on individual type*/
					if($('#indType').val() == 'ANONYMOUS'){
						$(".addToCart").hide();
						$('#cartBtn').hide();
					}
					
				},
				silent:true
			});
			
			self.collection.cartSummary.fetched = false;
			self.cartCollection = options.cartCollection;
			var currentGroupId = $('#currentGroupId').val();
			
			self.cartCollection.fetch({
				success : function () {
					self.cartCollection.setCartSummary(false,currentGroupId);
					self.collection.cartSummary.fetched = true;
				}
			});
			
			self.cartModel = options.cartModel;
			
			$('.showCart').bind('click', this.showCart);
			$('.goToCart').bind('click', this.goToCart);
			$('.filter_checkbox').bind('change', this.filter);
			
			
			self.collection.bind('reset', self.addAll);
		},

		addAll : function () {
			var self = this;
			$('#mainSummary').empty();
			
			$('#estimatedCost').find('.plan-details').remove();
			$('#dentistSearch').find('.plan-details').remove();
			$('#providerSearch').find('.plan-details').remove();
			$('#productType').find('.plan-details').remove();
			
			$('#planDeductible1').find('.plan-details').remove();
			$('#planDeductible2').find('.plan-details').remove();
			$('#planDeductible3').find('.plan-details').remove();
			$('#planDeductible4').find('.plan-details').remove();
			
			$('#childrensDental1').find('.plan-details').remove();
			$('#childrensDental2').find('.plan-details').remove();
			$('#childrensDental3').find('.plan-details').remove();
			$('#childrensDental4').find('.plan-details').remove();
			$('#childrensDental5').find('.plan-details').remove();

			self.collection.each (self.addOne);
		},
		
		loadAll : function () {
			var self = this;
			self.collection.loadIssuers();
			self.loadCartSummary();
			self.collection.updateCompareSummary();
		},
		
		addOne : function (model) {
			var self = this;
			var pgrmType = $('#PGRM_TYPE').val();
			model.set({"PGRM_TYPE":pgrmType});
			var viewSummary = new Summary({model:model});
			var providerLinkOnOff = $('#providerLinkOnOff').val();
			model.set({"providerLinkOnOff":providerLinkOnOff});

			$('#mainSummary').append(viewSummary.render());
			
			$('#estimatedCost').append(viewSummary.getEstimatedCost());
			$('#dentistSearch').append(viewSummary.getDentist());
			$('#providerSearch').append(viewSummary.getProvider());
			$('#productType').append(viewSummary.getProductType());
			
			$('#planDeductible1').append(viewSummary.getDeductible1());
			$('#planDeductible2').append(viewSummary.getDeductible2());
			$('#planDeductible3').append(viewSummary.getDeductible3());
			$('#planDeductible4').append(viewSummary.getDeductible4());
			
			$('#childrensDental1').append(viewSummary.getChildrensDental1());
			$('#childrensDental2').append(viewSummary.getChildrensDental2());
			$('#childrensDental3').append(viewSummary.getChildrensDental3());
			$('#childrensDental4').append(viewSummary.getChildrensDental4());
			$('#childrensDental5').append(viewSummary.getChildrensDental5());
		},
		
		
		loadCartSummary : function () {
			var self = this;
			var fetchedVal = self.collection.cartSummary.fetched;
			if(fetchedVal == false){
				$('#planCount').html("(Loading...)");
				setTimeout(function(){self.loadCartSummary();}, 1000);
				return;
			}else{
				self.collection.loadCartSummary(self.cartCollection.cartSummary,self.cartCollection.cartItems,self.cartCollection.models);
			}
		},
		
		
		addToCart: function (e) {
			var self = this;
			var planModel = "";
			var exchangeType = $('#exchangeType').val();
			var stateCode = $('#stateCode').val();
			var multipleItemsEntryOnOff = $('#multipleItemsEntryOnOff').val();
			
			if(e.target.id.indexOf('cart_') >= 0){
				itemId = e.target.id.replace("cart_","");
			}else{
				itemId = e.target.id.replace("plan_","");
			}

			if($("#"+e.target.id).attr("class").indexOf('addedItem') >= 0){
				if($("#cart_"+itemId).length){
					$("#cart_"+itemId).attr("class","addToCart btn btn-primary btn-small icon-shopping-cart");
					$("#cart_"+itemId).html('&nbsp;'+' Add');
				}
				
				self.collection.emptyCart(self.cartCollection.models);
				 if(!(exchangeType == 'STATE')){
					 $('#cart-box').show();
				 }
				planModel = self.collection.searchPlan(this.cartCollection.models, itemId);
				orgId = planModel.id;
				
				planModel.set({id: planModel.get("orderItemId")});
				planModel.destroy({
					success : function () {
						planModel.set({id: orgId});
						self.cartCollection.remove(planModel);
						self.collection.updateCart(self.cartCollection.models);
					}
				});
				
				
				self.collection.removeFromCart(itemId, planModel.get("level").toLowerCase());
				
				$('#cartmessageplanremove').fadeIn(250,function() {
					setTimeout(function(){
						$('#cartmessageplanremove').hide();
					},1500);
				});
			}
			else{
				
				var planAddedForGroup = self.collection.searchCart(self.cartCollection.models,this.collection.cartItems,itemId);
				if(planAddedForGroup == false){
//					planModel = self.collection.searchPlan(this.collection.models, itemId);
//					var groupsEhb = self.collection.getEhb(self.cartCollection.models,planModel);
//					var ehb = groupsEhb["groupEhb_"+planModel.get("groupId")];
//					
//					if(exchangeType == 'STATE')
//					{
//						if(ehb > 10){
//							alert("high Ehb msg. cart already have plan with all ehb's");
//							return false;
//						}
//					}
//					else
//					{
//						if(ehb > 0.5 && ehb < 10){
//							alert("low Ehb msg. can not add multiple dental plans");
//							return false;
//						}
//						if(ehb > 10){
//							alert("high Ehb msg. cart already have plan with all ehb's");
//							return false;
//						}
//					}
					
					
					var dentalPlanAdded = self.collection.dentalPlanAdded(self.cartCollection.models);
					if(dentalPlanAdded == true){
						alert("Dental Plan is already added. If you wish to add this dental plan kindly remove the previously added one.");
						return false;
					}
					
					/*if(ehb > 0.5 && ehb < 10){
						alert("low Ehb msg. can not add multiple dental plans");
						return false;
					}
					if(ehb > 10){
						alert("high Ehb msg. cart already have plan with all ehb's");
						return false;
					}*/
					
					if( jQuery.inArray(itemId, this.collection.cartItems) === -1 ) {
						$("#"+itemId).attr("class","addToCart addedItem btn btn-primary");
						$("#"+itemId).attr("title","Remove this plan");//added to fav
						 if(!(exchangeType == 'STATE')){
							 $("#cart").show();
							 $('#cart-box').show();
						 }
						planModel = self.collection.searchPlan(this.collection.models, itemId);
						
						this.cartModel.save({id: planModel.get("planId"), premium: planModel.get("premiumAfterCredit"), premiumBeforeCredit: planModel.get("premiumBeforeCredit"), planDetailsByMember: planModel.get("planDetailsByMember"), aptc: planModel.get("aptc")},{
							success : function (model,resp) {
								var newModel = new Backbone.Model({id: planModel.id, name: planModel.get("name"), planType: planModel.get("planType"), 
									level: planModel.get("level"), premium: planModel.get("premium"), issuer: planModel.get("issuer"), 
									issuerText: planModel.get("issuerText"), networkType: planModel.get("networkType"), deductible: planModel.get("deductible"),
									premiumBeforeCredit: planModel.get("premiumBeforeCredit"), aptc: planModel.get("aptc"), premiumAfterCredit: planModel.get("premiumAfterCredit"),
									groupId: planModel.get("groupId"), groupName: planModel.get("groupName"), groupMembers: planModel.get("groupMembers"), 
									planId: planModel.get("planId"), ehb: planModel.get("ehb"), orderItemId:model.id, issuerLogo: planModel.get("issuerLogo")});
								
								self.cartCollection.add(newModel);
								
								self.collection.emptyCart(self.cartCollection.models);
								self.collection.updateCart(self.cartCollection.models);
								$('.removeFromCart').bind('click', self.addToCart);
								$('.checkOut').bind('click', self.checkOut);
							}
						});
						
						this.collection.setCartSummary(itemId, planModel.get("level").toLowerCase());
						
						$('#cartmessageplan').fadeIn(250,function() {
							setTimeout(function(){
								$('#cartmessageplan').hide();
							},1500);
						});
					}else{
						alert("Selected plan is already present in the cart");
					}
				}else{
					alert("Plan for current group is already present in the cart");
				}
			}
			
			this.collection.updateCartSummary();
		},
		
		checkOut: function () {
			var self = this;

			if(self.cartCollection.models.length <= 0){
				alert("Cart is empty");
			}
			window.location = "showcart";
		},
		
		showCart : function () {
			var self = this;
			self.collection.emptyCart(self.cartCollection.models);
			$('#cart-box').show();
			self.collection.updateCart(self.cartCollection.models);
			$('.removeFromCart').bind('click', self.addToCart);
			$('.checkOut').bind('click', this.checkOut);
		},
		
		goToCart: function(){
	 	 	 		
	 	 	 		          var self = this;
	 	 	 		          var fullEhb = true;
	 	 	 		          var groupsEhb = self.collection.getEhb(self.cartCollection.models);
	 	 	 		          if(self.cartCollection.models.length <= 0){
	 	 	 		              alert("Cart is empty");
	 	 	 		          }
	 	 	 		          else{
	 	 	 		                window.location = "showcart";
	 	 	 		          }
	 	 	 		                        
	 	 	 		                        
		},
		
		addToCompare: function (e) {
			if(e.target.id.indexOf('ompare_')){
				$('#favmessage').fadeIn(250,function() {
					setTimeout(function(){
						$('#favmessage').hide();
					},1500);
				});
			}
			this.collection.addToCompare(e.target.id.replace("compare_",""));
			$('#compareCount').html(this.collection.compareModels.length);
			
			var selid = e.target.id.replace("compare_","");
			
			$('#compare_'+selid + '.addToCompare').hide();
			$('#remove_'+selid + '.removeCompare').show();
		},
		
		removeCompare: function (e) {
			var self=this;
			/* if not compare view*/
			if(this.collection.showCompare == false){
				this.collection.removeCompare(e.target.id.replace("remove_",""),"pview");
				var selid = e.target.id.replace("remove_","");
				$('#compare_'+selid + '.addToCompare').show();
				$('#remove_'+selid + '.removeCompare').hide();
						
			}
			else{ /*if from compare view*/
				
				this.collection.removeCompare(e.target.id.replace("remove_",""),"cview");
				/*if no items in compare array*/
				if(this.collection.compareModels.length < 1)
					this.collection.showCompare == false;
				
			}
			this.collection.updateCompareSummary();
			
			
		},
		
		getFilterRules : function() {

			var matches = [];
			$(".filter_checkbox").each(function() {
				if (this.checked) {
					matches.push($(this).val());
				}
			});

			var rules = new Object();
			var nameObj = new Object();
			nameObj['type'] = 'pattern';
			nameObj['field'] = 'level';
			nameObj['value'] = matches;
			rules['planName'] = nameObj;

			return rules;
		},

		filter : function(e) {
			e.preventDefault();
			var rules = this.getFilterRules();
			this.collection.setFieldFilter(rules);
			this.collection.preserveOtherOptions();			
		}
		
	});
	
	var Summary = Backbone.View.extend({
		template: _.template($('#resultItemTemplateMainSummary').html()),
		estimatedCostTpl: _.template($('#estimatedCostTpl').html()),
		providerSearchTpl: _.template($('#providerSearchTpl').html()),
		providerSearchLinkTpl: _.template($('#providerSearchLinkTpl').html()),
		productTypeTpl: _.template($('#productTypeTpl').html()),
		deductibleIndTpl: _.template($('#deductibleIndTpl').html()),
		deductibleFlyTpl: _.template($('#deductibleFlyTpl').html()),
		benefitTpl: _.template($('#benefitTpl').html()),
		
		initialize: function() {
			this.model.bind('change', this.render, this);
			this.model.bind('destroy', this.remove, this);
		},
		
		render : function () {
			return this.template(this.model.toJSON());
		},
		
		getEstimatedCost : function () {return this.estimatedCostTpl({ANNUAL_PREMIUM_AFTER_CREDIT :  this.model.toJSON().annualPremiumAfterCredit});},
		getDentist : function () {return this.providerSearchTpl({PROVIDERS :  this.model.toJSON().dentists, PROVIDERS_SUPPORTED : this.model.toJSON().dentistsSupported});},
		getProvider : function () {return this.providerSearchLinkTpl({PROVIDER_LINK :  this.model.toJSON().providerLink});},
		getProductType : function () {return this.productTypeTpl({NETWORK_TYPE :  this.model.toJSON().networkType});},
		
		getDeductible1 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_MEDICAL : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible});},
		getDeductible2 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_MEDICAL : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
		getDeductible3 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_MEDICAL : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
		getDeductible4 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_MEDICAL : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
		
		getChildrensDental1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.ACCIDENTAL_DENTAL}); },
		getChildrensDental2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.BASIC_DENTAL_CARE_CHILD}); },
		getChildrensDental3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.DENTAL_CHECKUP_CHILDREN}); },
		getChildrensDental4 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.MAJOR_DENTAL_CARE_CHILD}); },
		getChildrensDental5 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.ORTHODONTIA_CHILD}); }
	});
})(App.views);