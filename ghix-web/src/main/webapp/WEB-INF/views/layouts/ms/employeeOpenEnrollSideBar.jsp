<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<style type="text/css">

.sidebar ol li{border:none;}

ul.nav-list li.dropdownNav{padding:10px 0 0 10px;}
ul.nav-list li.dropdownNav ol{margin-left: -10px;}
ul.nav-list li.dropdownNav.active{background-position: right 6%; }
ul.nav-list li.dropdownNav .icon-ok{color:#5A5A5A;}
li.navmenu span {
	float: left;
	padding-right: 10px;
}
li.navmenu .icon-ok {
	left: -3px;
	position: relative;
	margin-right: 0 !important;
}
</style>
<c:if test="${ANONYMOUS_INDIVIDUAL_TYPE != 'ANONYMOUS' }">
<div class="header">
<h4>Steps</h4>
</div>
    <ol class="nav nav-list sidebar">
		<li class="navmenu"><a href="<c:url value="/shop/employee/home" />"><span class="icon-ok"></span> Dashboard</a></li> 
		<li id="1" class="navmenu"><a href="<c:url value="/shop/employee/verifyemployee" />"> Verify Information</a></li>
		<li id="2" class="navmenu"><a href="<c:url value="/shop/employee/verifydependent" />"> Update Dependents</a></li>					
		<li id="3" class="navmenu "><a href="<c:url value="/plandisplay/planselection" />"> Shop for Health Plan</a></li>
		<li id="4" class="navmenu"><a href="<c:url value="/plandisplay/dental" />"> Shop for Children's Dental</a></li>
		<li id="5" class="navmenu"><a href="#"> View Cart</a></li>
		<li id="6" class="navmenu"><a href="#"> Sign Application</a></li>
	</ol>
</c:if>	
 <script type="text/javascript">
$(document).ready(function(){
	 var pageName = '${pageName}'; 
	 var childFlag = '${childFlag}';
	
	/* var pageNumber = '${pageNumber}';
	for (var i = 0; i <= pageNumber; i++) {
		$('#'+i).addClass('active');
	} */
	//pageName = 'shopForHousehold';
	var stepNumber = 1;
	/* if (pageName == 'getStarted')
		{
			stepNumber = 1;
			$('#'+stepNumber).addClass('active');
		}
	else */ 
	if (pageName == 'verifyInfo')
	{
		stepNumber = 1;
		$('#'+stepNumber).addClass('active');
	}
	else if (pageName == 'verifyDependants')
	{
		stepNumber = 2;
		$('#'+stepNumber).addClass('active');
	}
	else if (pageName == 'affordability')
	{
		stepNumber = 2;
		$('#2').addClass('active');
	}
	else if (pageName == 'selectPlan')
	{
		stepNumber = 3;
		$('#3').addClass('active');
	}
	else if (pageName == 'preferences')
	{
		stepNumber = 3;
		$('#3').addClass('active');
	}
	else if (pageName == 'shopForHousehold')
	{
		stepNumber = 3;
		$('#3').addClass('active');
	}
	else if (pageName == 'shopForDental')
	{
		stepNumber = 4;
		$('#4').addClass('active');
	}
	else if (pageName == 'cart')
	{
		stepNumber = 5;
		$('#5').addClass('active');
	}
	else if (pageName == 'signApp')
	{
		stepNumber = 6;
		$('#'+stepNumber).addClass('active');
	}
	for (var i = 0; i < stepNumber; i++) {
		
		if(childFlag == true || childFlag == 'true'){
			
				$('#'+i).find('a:first').before('<span class="icon-ok"></span>');
		}else
			{
			if(i != 4)			
				$('#'+i).find('a:first').before('<span class="icon-ok"></span>');	
			}
		
	}
	for (var k = 6; k >= stepNumber; k--) {
		
		$('#'+k).find('a:first').attr('href', '#');
	}
	
	$('span.icon-ok').parents('li').css('list-style', 'none');
});

</script>