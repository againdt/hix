package com.getinsured.eligibility.at.ref.service.nonfinancial;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.active.enrollment.service.ActiveEnrollmentService;
import com.getinsured.eligibility.at.ref.common.AccountTransferCategoryEnum;
import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.ref.dto.EnrollmentAttributesDTO;
import com.getinsured.eligibility.at.ref.dto.NFProcessDTO;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerBaseService;
import com.getinsured.eligibility.at.ref.service.ReferralLceNotificationService;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author chopra_s
 * 
 */
@Component("referralNFConversionService")
@Scope("singleton")
public class ReferralNFConversionServiceImpl extends LceProcessHandlerBaseService implements ReferralNonFinancialService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralNFConversionServiceImpl.class);

	@Autowired
	@Qualifier("activeEnrollmentService")
	private ActiveEnrollmentService activeEnrollmentService;

	@Autowired
	@Qualifier("referralNFCompareService")
	private ReferralNFCompareService referralNFCompareService;

	@Autowired
	@Qualifier("ssapEnrolleeHandler")
	private SsapEnrolleeHandler ssapEnrolleeHandler;

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Override
	@ReferralTransactionAnno
	public boolean execute(NFProcessDTO nfProcessRequestDTO) throws Exception {
		LOGGER.info("ReferralNFConversionServiceImpl starts for - " + nfProcessRequestDTO);
		final SsapApplication currentApplication = loadCurrentApplication(nfProcessRequestDTO.getCurrentApplicationId());

		if (currentApplication == null) {
			LOGGER.error(ReferralConstants.NO_SSAP_FOUND + nfProcessRequestDTO.getCurrentApplicationId());
			throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + nfProcessRequestDTO.getCurrentApplicationId());
		}

		final SsapApplication nonFinancialApplication = loadCurrentApplication(nfProcessRequestDTO.getNfApplicationId());

		if (nonFinancialApplication == null) {
			LOGGER.error(ReferralConstants.NO_SSAP_FOUND + nfProcessRequestDTO.getNfApplicationId());
			throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + nfProcessRequestDTO.getNfApplicationId());
		}

		final boolean blnEnrollmentActive = isEnrollmentActive(nfProcessRequestDTO.getNfApplicationId());
		if (!blnEnrollmentActive) {
			LOGGER.info("Enrollment is not active, handle manual scenario for application - " + nfProcessRequestDTO.getCurrentApplicationId());
			handleManual(nfProcessRequestDTO, currentApplication);
			return true;
		}
		boolean blnComplete = false;
		final CompareMainDTO compareMainDTO = referralNFCompareService.executeCompare(currentApplication, nonFinancialApplication);

		final boolean blnConversion = checkConversionCriteria(compareMainDTO);
		// TODO: R4.3 - handle NF to F - Change in CS level
		// process if there is a change in CSR, call IND71 and return blnComplete = true; to avoid APTC value.
		if (blnConversion) {
			handleConversion(compareMainDTO, currentApplication, nonFinancialApplication, nfProcessRequestDTO.getAccountTransferCategory());
		} else {
			boolean isCsChangeDetected = checkCsChangeConversion(compareMainDTO);
			if (isCsChangeDetected){
				nfProcessRequestDTO.setCSChangeDetected(true);
				nfProcessRequestDTO.setCompareMainDTO(compareMainDTO);
				handleCsConversion(compareMainDTO, currentApplication, nonFinancialApplication, nfProcessRequestDTO.getAccountTransferCategory());
			}else {
				handleManualSep(compareMainDTO, nfProcessRequestDTO, currentApplication);
				blnComplete = true;
			}
		}
		if(!blnComplete){
			updateValidationAndSetInNfProcessDto(nfProcessRequestDTO,currentApplication.getCaseNumber());
		}
		if(!blnComplete && !nfProcessRequestDTO.isValidationDone()){
			triggerDocumentRequiredEmail(currentApplication);
		}
		
		LOGGER.info("ReferralNFConversionServiceImpl ends for - " + nfProcessRequestDTO);
		return blnComplete;
	}
	
	private void updateValidationAndSetInNfProcessDto(NFProcessDTO nfProcessRequestDTO,String caseNumber) {
		
		Optional<ApplicationValidationStatus> validationStatus = getValidationStatus(caseNumber);
		ignoreGatedNonPrimaryEvents(caseNumber);
		nfProcessRequestDTO.setValidationDone(isApplicationValidated(validationStatus));
		
	}

	private boolean checkCsChangeConversion(CompareMainDTO compareMainDTO) {
		
		boolean healthAndDental = false;
		if (compareMainDTO != null 
				&& compareMainDTO.getEnrolledApplication() != null
				&& compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes() != null
		        && compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getHealthPlanId() > ReferralConstants.NONE
		        && compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getDentalPlanId() > ReferralConstants.NONE){
			healthAndDental = true;
		}
		
		String applicationCSRLevel = compareMainDTO.getCurrentApplication().getCsrLevel() != null ? 
				compareMainDTO.getCurrentApplication().getCsrLevel() : ReferralConstants.CS1;
		
		boolean status = !healthAndDental 
				&& !checkIfCatastrophicPlan(compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes())
				&& !StringUtils.equalsIgnoreCase(applicationCSRLevel, 
						compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getCostSharing());
		
		/*boolean status1 = !healthAndDental 
				&& !StringUtils.equalsIgnoreCase(ReferralConstants.CS1, 
						compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getCostSharing())
				&& !StringUtils.equalsIgnoreCase(compareMainDTO.getCurrentApplication().getCsrLevel(), 
						compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getCostSharing())
				&& !checkIfCatastrophicPlan(compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes());*/
		        
		return status;
	}

	private boolean checkConversionCriteria(CompareMainDTO compareMainDTO) {
		LOGGER.info("Check Conversion criteria (Cost sharing & Catastrophic) for enrolled application - " + compareMainDTO.getEnrolledApplication().getId());
		boolean blnReturn = false;
		if (compareMainDTO != null && compareMainDTO.getEnrolledApplication() != null && compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes() != null
		        && compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getHealthPlanId() > ReferralConstants.NONE) {
			//ssapEnrolleeHandler.invokeSharedPlanMgnt(compareMainDTO);
			if(StringUtils.isNotBlank(compareMainDTO.getEnrolledApplication().getCsrLevel())) {
				compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setCostSharing(compareMainDTO.getEnrolledApplication().getCsrLevel());
			}else {
				compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().setCostSharing(ReferralConstants.CS1);	
			}
			blnReturn = StringUtils.equalsIgnoreCase(ReferralConstants.CS1, compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getCostSharing())
			        && !checkIfCatastrophicPlan(compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes());
			
			
			/* if app was enrolled in CS3 and now also converted to CS3, apply end to end automation */
			if (!checkIfCatastrophicPlan(compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes())
				&& StringUtils.equalsIgnoreCase(ReferralConstants.CS3, compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getCostSharing())
				&& StringUtils.equalsIgnoreCase(ReferralConstants.CS3,compareMainDTO.getCurrentApplication().getCsrLevel())){
				
				blnReturn = true;
			}
			
			/**
			 * R4.3.5 - Allow gain in CSR level to apply automatically for NF to F when NF enrolled in CS1 plan
			 */
			
			boolean healthAndDental = false;
			if (compareMainDTO != null 
					&& compareMainDTO.getEnrolledApplication() != null
					&& compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes() != null
			        && compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getHealthPlanId() > ReferralConstants.NONE
			        && compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getDentalPlanId() > ReferralConstants.NONE){
				healthAndDental = true;
			}
			
			if (!healthAndDental && allowCS1toCSXatAppLevel(compareMainDTO)){
				blnReturn = false;
			}
		}
		LOGGER.info("Check Conversion criteria (Cost sharing & Catastrophic) for enrolled application - " + compareMainDTO.getEnrolledApplication().getId() + " Value is - " + blnReturn);
		return blnReturn;
	}

	private boolean allowCS1toCSXatAppLevel(CompareMainDTO compareMainDTO) {
		String applicationCSRLevel = compareMainDTO.getCurrentApplication().getCsrLevel() != null ? compareMainDTO.getCurrentApplication().getCsrLevel() : ReferralConstants.CS1;
		return StringUtils.equalsIgnoreCase(ReferralConstants.CS1, compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getCostSharing())
		        && !checkIfCatastrophicPlan(compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes())
		        && !StringUtils.equalsIgnoreCase(ReferralConstants.CS1, applicationCSRLevel);
	}
	
	private void handleCsConversion(CompareMainDTO compareMainDTO, SsapApplication currentApplication, SsapApplication nonFinancialApplication, String accountTransferCategory) throws Exception {
		LOGGER.info("Handle conversion scenario for application - " + compareMainDTO.getCurrentApplication().getId());
		updateCurrentAppToER(currentApplication);
		referralNFCompareService.demoUpdate(compareMainDTO);
		manageCurrentApplicationApplicants(currentApplication, compareMainDTO);
		changeApplicationType(currentApplication, ReferralConstants.SEP);
		manageApplicationApplicantEventForConversion(currentApplication, accountTransferCategory);
		/*boolean status = executeIND71(currentApplication.getId(), compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getHealthEnrollees(),
				compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getDentalEnrollees());
		
		if (status) {
			LOGGER.info("IND71 update successful for  " + currentApplication.getId());
			status = processCSSuccess(currentApplication.getId(), nonFinancialApplication.getId());
			triggerAutomationCSNotice(currentApplication.getCaseNumber());
		} */
		
	}

	private void handleConversion(CompareMainDTO compareMainDTO, SsapApplication currentApplication, SsapApplication nonFinancialApplication, String accountTransferCategory) throws Exception {
		LOGGER.info("Handle conversion scenario for application - " + compareMainDTO.getCurrentApplication().getId());
		updateCurrentAppToER(currentApplication);
		referralNFCompareService.demoUpdate(compareMainDTO);
		manageCurrentApplicationApplicants(currentApplication, compareMainDTO);
		changeApplicationType(currentApplication, ReferralConstants.SEP);
		manageApplicationApplicantEventForConversion(currentApplication, accountTransferCategory);
	}

	private void manageCurrentApplicationApplicants(SsapApplication currentApplication, CompareMainDTO compareMainDTO) {
		LOGGER.info("manageCurrentApplicationApplicants  for application - " + currentApplication.getId());
		final SingleStreamlinedApplication ssappln = ssapJsonBuilder.transformFromJson(compareMainDTO.getCurrentApplication().getApplicationData());
		for (CompareApplicantDTO applicantDTO : compareMainDTO.getCurrentApplication().getApplicants()) {
			updateApplicantGuid(ssappln, applicantDTO);
		}
		final String ssapJson = ssapJsonBuilder.transformToJson(ssappln);
		updateSSAPJSON(currentApplication, ssapJson);

		modifyCurrentApplicants(compareMainDTO);
	}

	private void updateSSAPJSON(SsapApplication currentApplication, String ssapJson) {
		currentApplication.setApplicationData(ssapJson);
		updateSsapApplication(currentApplication);
    }

	private void modifyCurrentApplicants(CompareMainDTO compareMainDTO) {
		final List<CompareApplicantDTO> currentList = compareMainDTO.getCurrentApplication().getApplicants();
		for (CompareApplicantDTO currentApplicant : currentList) {
			persistSSAPApplicantGuid(currentApplicant);
		}
	}

	private void persistSSAPApplicantGuid(CompareApplicantDTO applicantDTO) {
		SsapApplicant ssapapplicant = ssapApplicantRepository.findSsapApplicantById(applicantDTO.getId());
		ssapapplicant.setApplicantGuid(applicantDTO.getApplicantGuid());
		ssapApplicantRepository.save(ssapapplicant);
	}

	private void updateApplicantGuid(SingleStreamlinedApplication ssappln, CompareApplicantDTO applicantDTO) {
		for (HouseholdMember houseHoldMember : ssappln.getTaxHousehold().get(0).getHouseholdMember()) {
			if (houseHoldMember.getPersonId() == applicantDTO.getPersonId()) {
				houseHoldMember.setApplicantGuid(applicantDTO.getApplicantGuid());
				break;
			}
		}
	}

	private void manageApplicationApplicantEventForConversion(SsapApplication currentApplication, String accountTransferCategory) {
		LOGGER.info("manageApplicationApplicantEventForConversion  for application - " + currentApplication.getId());
		if (AccountTransferCategoryEnum.QE.value().equals(accountTransferCategory)) {
			ssapApplicationEventService.createAndUpdateApplicationEventForNonFinancialQE(currentApplication, SsapApplicationEventTypeEnum.SEP, ReferralConstants.Y, null);
		} else if (AccountTransferCategoryEnum.OE.value().equals(accountTransferCategory)) {
			ssapApplicationEventService.updateSsapApplicationEventForNonFinancial(currentApplication, SsapApplicationEventTypeEnum.SEP, ReferralConstants.Y);
		}
	}

	private void manageApplicationApplicantEventForManual(SsapApplication currentApplication) {
		LOGGER.info("manageApplicationApplicantEventForManual for application - " + currentApplication.getId());
		ssapApplicationEventService.createAndUpdateApplicationEventForNonFinancialQE(currentApplication, SsapApplicationEventTypeEnum.QEP, ReferralConstants.N, null);
	}

	private void manageSepApplicationApplicantEventForManual(SsapApplication currentApplication, String accountTransferCategory) {
		LOGGER.info("manageSepApplicationApplicantEventForManual for application - " + currentApplication.getId());
		if (AccountTransferCategoryEnum.QE.value().equals(accountTransferCategory)) {
			ssapApplicationEventService.createAndUpdateApplicationEventForNonFinancialQE(currentApplication, SsapApplicationEventTypeEnum.SEP, ReferralConstants.N, null);
		} else if (AccountTransferCategoryEnum.OE.value().equals(accountTransferCategory)) {
			ssapApplicationEventService.updateSsapApplicationEventForNonFinancial(currentApplication, SsapApplicationEventTypeEnum.SEP, ReferralConstants.N);
		}
	}

	private void handleManual(NFProcessDTO nfProcessRequestDTO, SsapApplication currentApplication) {
		LOGGER.info("Handle manual scenario for application - " + nfProcessRequestDTO.getCurrentApplicationId());
		updateCurrentAppToER(currentApplication);
		if (AccountTransferCategoryEnum.QE.value().equals(nfProcessRequestDTO.getAccountTransferCategory())) {
			manageApplicationApplicantEventForManual(currentApplication);
		}
		updateAllowEnrollment(currentApplication, Y);
		updateValidationAndSetInNfProcessDto(nfProcessRequestDTO,currentApplication.getCaseNumber());
		triggerNFtoFinancialConversionManualEmail(currentApplication);
	}

	private void handleManualSep(CompareMainDTO compareMainDTO, NFProcessDTO nfProcessRequestDTO, SsapApplication currentApplication) throws Exception {
		LOGGER.info("Handle manual sep scenario for application - " + nfProcessRequestDTO.getCurrentApplicationId());
		updateCurrentAppToER(currentApplication);
		referralNFCompareService.demoUpdate(compareMainDTO);
		manageCurrentApplicationApplicants(currentApplication, compareMainDTO);		
		changeApplicationType(currentApplication, ReferralConstants.SEP);
		manageSepApplicationApplicantEventForManual(currentApplication, nfProcessRequestDTO.getAccountTransferCategory());
		updateAllowEnrollment(currentApplication, Y);
		updateValidationAndSetInNfProcessDto(nfProcessRequestDTO,currentApplication.getCaseNumber());
		triggerNFtoFinancialConversionManualEmail(currentApplication);
	}

	private boolean isEnrollmentActive(long id) {
		try {
			return activeEnrollmentService.isEnrollmentActive(id, ReferralConstants.EXADMIN_USERNAME);
		} catch (Exception e) {
			/** Eat the exception to go in the manual flow*/
			return false;
		}
	}

	private boolean checkIfCatastrophicPlan(EnrollmentAttributesDTO enrolledApplicationAttributes) {
		return SsapEnrolleeHandler.isEnrolledForHealth(enrolledApplicationAttributes) && Plan.PlanLevel.CATASTROPHIC.toString().equals(enrolledApplicationAttributes.getHealthPlanLevel());
	}
}
