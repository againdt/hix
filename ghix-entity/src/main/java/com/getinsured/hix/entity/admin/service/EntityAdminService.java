package com.getinsured.hix.entity.admin.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.EntityDocuments;

/**
 * Encapsulates service layer method calls for Entity Admin for Enrollment
 * Entities
 */
public interface EntityAdminService {

	/**
	 * Finds the Status History for the given Enrollment Entity
	 * 
	 * @param userId
	 *            the given Enrollment Entity User id
	 * @return list of Enrollment Entity's status history
	 */
	List<Map<String, Object>> loadEntityrStatusHistoryForAdmin(Integer userId);

	/**
	 * Saves the given Enrollment Entity Document
	 * 
	 * @param EntityDocuments
	 *            object to save
	 * @return the saved Enrollment Entity Document
	 */
	 EntityDocuments saveEntityDocuments(EntityDocuments entityDocuments);

	/**
	 * Finds the Enrollment Entity Document
	 * 
	 * @param id
	 *            Enrollment Entity Id
	 * @return the Enrollment Entity Document
	 */
	EntityDocuments getEntityDocumentsByRecordID(Integer id);

	/**
	 * Finds the list of Assisters
	 * 
	 * @param assisterRequestDTO
	 *            contains the Assister search criteria parameters
	 * @return list of Assisters
	 */
	List<Assister> searchAssister(AssisterRequestDTO assisterRequestDTO);
}
