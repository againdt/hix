<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="stateCode" value="${StateConfiguration.stateCode}" />
<c:if test="${stateCode == 'CA'}">
	<jsp:include page="./ca/masthead.jsp" />
</c:if>
<c:if test="${stateCode == 'CT'}">
	<jsp:include page="./ct/masthead.jsp" />
</c:if>
<c:if test="${stateCode eq 'ID'}">
	<jsp:include page="./id/masthead.jsp" />
</c:if>
<c:if test="${stateCode == 'MN'}">
	<jsp:include page="./mn/masthead.jsp" />
</c:if>
<c:if test="${stateCode == 'NV'}">
	<jsp:include page="./nv/masthead.jsp" />
</c:if>
<c:if test="${stateCode == 'WA'}">
	<jsp:include page="./wa/masthead.jsp" />
</c:if>
