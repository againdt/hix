package com.getinsured.eligibility.at.ref.service.migration;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.resp.si.dto.ApplicationExtension;
import com.getinsured.eligibility.at.sep.service.SepEventsService;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.iex.ssap.model.SsapApplication;

@Component("lceAppExtensionEventMigration")
public class LceAppExtensionEventMigrationImpl implements LceAppExtensionEventMigration {
	private static final Logger LOGGER = LoggerFactory.getLogger(LceAppExtensionEventMigrationImpl.class);

	private enum Events {
		ADD, REMOVE, QUALIFYING_EVENT, OTHER
	}

	private static Map<String, SepEvents> sepEventsOther;
	
	private static final String SEP_AUTO_APTC_UPDATE = "AUTO_APTC_UPDATE";
	
	@Autowired
	@Qualifier("sepEventsService")
	private SepEventsService sepEventsService;

	@Autowired
	@Qualifier("ssapApplicationEventMigrationService")
	private SsapApplicationEventMigrationService ssapApplicationEventMigrationService;
	
	@Autowired
	private MigrationUtil migrationUtil;

	@PostConstruct
	public void doPost() {
		sepEventsOther = sepEventsService.findFinancialSepEventsByChangeType(Events.OTHER.name());
	}

	@Override
	public void createAptcOnlyEvent(SsapApplication currentApplication, ApplicationExtension applicationExtension) {
		LOGGER.info("Create APTC only Event for Application id - " + currentApplication.getId());
		SepEvents event = sepEventsOther.get(SEP_AUTO_APTC_UPDATE);
		Date applicantEventDate;
		Date applicantReportDate;
		Timestamp activityDate = migrationUtil.getActivityDate(currentApplication.getId());
		if(activityDate!=null){
			applicantEventDate = activityDate;
			applicantReportDate = activityDate;
		} else {
			applicantEventDate = ApplicationExtensionEventUtil.defaultApplicantEventDate();
			applicantReportDate = ApplicationExtensionEventUtil.defaultApplicantReportDate();
		}
		
		ssapApplicationEventMigrationService.createAptcOnlyApplicationEvent(currentApplication, event, applicantEventDate, applicantReportDate);
	}


	@Override
	public Map<String, ApplicantEvent> createCitizenshipEvent(SsapApplication currentApplication, ApplicationExtension applicationExtension) {
		LOGGER.info("Create Citizenship Event for Application id - " + currentApplication.getId());
		final Map<String, ApplicantEvent> applicantExtensionEvent = ApplicationExtensionEventUtil.populateApplicantEventFromExtension(currentApplication.getSsapApplicants(), applicationExtension);
		ssapApplicationEventMigrationService.createCitizenshipApplicationEvent(currentApplication, applicantExtensionEvent);
		return applicantExtensionEvent;

	}

}
