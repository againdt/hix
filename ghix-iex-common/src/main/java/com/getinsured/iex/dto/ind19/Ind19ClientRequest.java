package com.getinsured.iex.dto.ind19;

import java.util.Date;

import com.getinsured.hix.indportal.dto.Group;

public class Ind19ClientRequest {
	
	private String caseNumber;
	private Long houseHoldId;
	private Integer assisterId;
	private Integer brokerId;
	//HIX-58645 Send Primary Contact Phone number in IND 19
	private String phoneNumber;
	//HIX-63743 Change plan option for native Americans
	private boolean isChangePlanFlow;
	//HIX-67563 - Change Plan and Enroll for SEP
	private boolean isSEPPlanFlow;
	//HIX-72082
	private int coverageYear;
	//HIX-102831
	private Date financialEffectiveDate;
	private Group group;
	
	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public Long getHouseHoldId() {
		return houseHoldId;
	}

	public void setHouseHoldId(Long houseHoldId) {
		this.houseHoldId = houseHoldId;
	}

	public Integer getAssisterId() {
		return assisterId;
	}

	public void setAssisterId(Integer assisterId) {
		this.assisterId = assisterId;
	}

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public boolean getIsChangePlanFlow() {
		return isChangePlanFlow;
	}
	public void setIsChangePlanFlow(boolean isChangePlanFlow) {
		this.isChangePlanFlow = isChangePlanFlow;
	}
	public boolean getIsSEPPlanFlow() {
		return isSEPPlanFlow;
	}

	public void setIsSEPPlanFlow(boolean isSEPPlanFlow) {
		this.isSEPPlanFlow = isSEPPlanFlow;
	}

	public int getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(int coverageYear) {
		this.coverageYear = coverageYear;
	}

	/**
	 * @return the financialEffectiveDate
	 */
	public Date getFinancialEffectiveDate() {
		return financialEffectiveDate;
	}

	/**
	 * @param financialEffectiveDate the financialEffectiveDate to set
	 */
	public void setFinancialEffectiveDate(Date financialEffectiveDate) {
		this.financialEffectiveDate = financialEffectiveDate;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Ind19ClientRequest [caseNumber=");
		builder.append(caseNumber);
		builder.append(", houseHoldId=");
		builder.append(houseHoldId);
		builder.append(", assisterId=");
		builder.append(assisterId);
		builder.append(", brokerId=");
		builder.append(brokerId);
		builder.append(", phoneNumber=");
		builder.append(phoneNumber);
		builder.append("]");
		return builder.toString();
	}

}
