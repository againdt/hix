package com.getinsured.iex.hub.vlp37.servicehandler;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.JAXBElement;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv.InitiateThirdVerifRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv.InitiateThirdVerifResponseType;
import com.getinsured.iex.hub.vlp37.prefixmappers.VLPNamespacePrefixMapper;
import com.getinsured.iex.hub.vlp37.service.VLP_PostProcessor;
import com.getinsured.iex.hub.vlp37.wrapper.InitiateThirdVerifRequestTypeWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.InitiateThirdVerifResponseTypeWrapper;



public class InitiateThirdVerifServiceHandler extends AbstractServiceHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(InitiateThirdVerifServiceHandler.class);
	
	private Jaxb2Marshaller marshaller;
	private String caseNumber = null;

	private InitiateThirdVerifRequestType reqObj;
	
	
	public Object getRequestpayLoad() throws HubServiceException{
		
			try{
				JSONParser parser = new JSONParser();
				JSONObject payloadObj = (JSONObject) parser.parse(getJsonInput());
				processJSONObject(payloadObj);
				InitiateThirdVerifRequestTypeWrapper obj = (InitiateThirdVerifRequestTypeWrapper)getNamespaceObjects().get(InitiateThirdVerifRequestTypeWrapper.class.getName());
				if(obj == null){
					throw new HubServiceException("Required VLP Step 3 Request parameters not available");
				}
				reqObj = obj.getRequest();
				caseNumber = reqObj.getCaseNumber();
				if(caseNumber == null) {
					throw new RuntimeException("Case Number is mandatory");
				}
				setJsonInput(obj.toJSONString());
				return reqObj;
				
			}catch(Exception pe){
				throw new HubServiceException("Failed to generate the payload:",pe);
			}
	}
	
	@SuppressWarnings("unchecked")
	private void processJSONObject(JSONObject obj) throws HubServiceException, HubMappingException{
	        Set<Entry<String, Object>> keySet = obj.entrySet();
	        Iterator<Entry<String, Object>> cursor = keySet.iterator();
	        Entry<String, Object> jsonElement = null;
	        while(cursor.hasNext()){
	               jsonElement = cursor.next();
	               handleJsonElement(jsonElement.getKey(), jsonElement.getValue());
	        }
	 }
	 
	 private void handleJsonElement(String key, Object value) throws HubServiceException, HubMappingException {
	        if(value instanceof JSONObject){
	           processJSONObject((JSONObject)value);
	        }else if(value instanceof JSONArray){
	        	processJSONArray((JSONArray) value);
	        }
	        else if(value instanceof String || value instanceof Boolean){
	        	this.handleInputParameter(key, value);
	        }
	 }

	
	private void processJSONArray(JSONArray value) throws HubServiceException, HubMappingException {
		int arrayLen = value.size();
		Object tmp;
		for(int i = 0; i < arrayLen; i++){
			tmp = value.get(i);
			if(tmp instanceof JSONObject){
				processJSONObject((JSONObject)tmp);
			}
			// should we expect anything other than JSONObject in this array?
		}
	}

	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		String message = "";
		LOGGER.info("Initiating response handling for Step 3");
		try{
			JAXBElement<InitiateThirdVerifResponseType> resp= (JAXBElement<InitiateThirdVerifResponseType>)responseObject;
			InitiateThirdVerifResponseType payload = resp.getValue();
			InitiateThirdVerifResponseTypeWrapper response = new InitiateThirdVerifResponseTypeWrapper(payload);
			this.getContext().put("CaseNumber", this.caseNumber);
			VLP_PostProcessor responseHandler = (VLP_PostProcessor) this.getContext().get(ServiceHandler.RESPONSE_HANDLER);
			this.requestStatus = responseHandler.submit(reqObj, payload);
			if(response != null && response.getInitiateThirdVerifResponseType() != null){	
				message = response.toJSONString();
			}
			return message;
		}catch(ClassCastException ce){
			message = "Help! I do not know any thing about "+responseObject.getClass().getName()+" Expecting:"+ InitiateThirdVerifResponseTypeWrapper.class.getName();
			this.requestStatus = RequestStatus.HUB_ERROR_RESPONSE;
			throw new HubServiceException(message, ce);
		}
	}
	
	
	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	@Override
	public String getServiceIdentifier() {
		return "H94";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}

}
