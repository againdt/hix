package com.getinsured.iex.ssap.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.iex.ssap.model.AptcHistory;

@RestResource(path="AptcHistory")
public interface AptcHistoryRepository  extends JpaRepository<AptcHistory, Long> {
	
	@RestResource(path="findAptcHistoryBySsapApplicationId")
    @Query("FROM AptcHistory a where a.id = " +
            " (select MAX (id) from AptcHistory b where b.ssapApplicationId=:ssapApplicationId and (b.aptcAmount is not null or b.stateSubsidyAmt is not null)) "+
            " and  a.ssapApplicationId=:ssapApplicationId and (a.aptcAmount is not null or a.stateSubsidyAmt is not null) ")
	public AptcHistory findAptcHistoryBySsapApplicationId(@Param("ssapApplicationId") long ssapApplicationId);
	
	@RestResource(path="findAptcHistoryBySsapApplicationIdForCap")
    @Query("FROM AptcHistory a where a.id = " +
            " (select MAX (id) from AptcHistory b where b.ssapApplicationId=:ssapApplicationId) "+
            " and  a.ssapApplicationId=:ssapApplicationId ")
	public AptcHistory findAptcHistoryBySsapApplicationIdForCap(@Param("ssapApplicationId") long ssapApplicationId);

	@RestResource(path="findAptcHistoryByCmrHouseholdIdCovYear")
	@Query("Select aptcHistory from AptcHistory as aptcHistory where cmrHouseholdId = :cmrHouseholdId and coverageYear = :coverageYear and aptcAmount is not null order by aptcHistory.creationTimestamp desc ")
	public List<AptcHistory> findAptcHistoryByCmrHouseholdIdCovYear(@Param("cmrHouseholdId") BigDecimal cmrHouseholdId,@Param("coverageYear") long coverageYear);
	
	@Query("FROM AptcHistory a where a.id = " +
			" (select MAX (id) from AptcHistory b where b.ssapApplicationId=:ssapApplicationId) "+
			" and  a.ssapApplicationId=:ssapApplicationId ")
	public AptcHistory findLatestSlcspAmount(@Param("ssapApplicationId") long ssapApplicationId);
	
	@Modifying
	@Transactional
	@Query(" UPDATE AptcHistory SET slcspAmount = :slcspAmount WHERE ssapApplicationId=:ssapApplicationId ")
	void updateSLCSPAmtForSSAPApplication(@Param("slcspAmount") BigDecimal slcspAmount, @Param("ssapApplicationId") Long ssapApplicationId);

}
