import {activePageActionTypes} from '../constants/actionTypes';
import CONFIG from '../.env';

export const initialState = CONFIG.ENV.DEVELOPMENT ? 32 : 0;

export default (state = initialState, action) => {
    switch (action.type) {
    case activePageActionTypes.CHANGE_PAGE:
        switch(action.page) {
        case 'increase':
            return state + 1;
        case 'decrease':
            return state - 1;
        default:
            if(typeof action.page !== 'number') {
                throw new Error('Invalid value (not "increase", "decrease" or number) has been passed in activePageReducer');
            }
            return action.page;
        }
    case activePageActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};
