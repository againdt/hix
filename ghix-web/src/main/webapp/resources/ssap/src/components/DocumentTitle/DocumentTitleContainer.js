import React, {useEffect} from 'react';
import { connect } from 'react-redux';

import DocumentTitle from './DocumentTitle';

function DocumentTitleContainer({stateCode, page, adjustedFlow}) {

    let title = 'SSAP';

    useEffect(() => {
        if (adjustedFlow && adjustedFlow[page]) {
            title = 'SSAP';
            switch (stateCode) {
            case 'CA':
                document.title = `${adjustedFlow[page].page.navBarText} - Covered California`;
                break;
            case 'MN':
                document.title = `${adjustedFlow[page].page.navBarText} - MNsure`;
                break;
            case 'NV':
                document.title = `${adjustedFlow[page].page.navBarText} - Nevada Health Link`;
                break;
            case 'ID':
                document.title = `${adjustedFlow[page].page.navBarText} - Your Health Idaho`;
                break;
            default:
                break;
            }
        }
    }, [page, stateCode]);

    return (
        <DocumentTitle title={title} />
    );
}

const mapStateToProps = state => ({
    adjustedFlow: state.adjustedFlow,
    page: state.page,
    stateCode: state.headerLinks.stateCode
});

export default connect(mapStateToProps)(DocumentTitleContainer);
