package com.getinsured.hix.indportal.dto.dm.application;

import java.math.BigDecimal;
import java.util.ArrayList;

public class MedicaidHouseholdComposition {
	 private String medicaidTaxRoleType;
	 private String medicaidHouseHoldStatus;
	 private String medicaidHouseHoldStatusReason;
	 private Integer medicaidHouseholdSize;
	 private ArrayList < BigDecimal > medicaidHouseholdMemberIdentifiers;


	 // Getter Methods 

	 public ArrayList<BigDecimal> getMedicaidHouseholdMemberIdentifiers() {
		return medicaidHouseholdMemberIdentifiers;
	}

	public void setMedicaidHouseholdMemberIdentifiers(ArrayList<BigDecimal> medicaidHouseholdMemberIdentifiers) {
		this.medicaidHouseholdMemberIdentifiers = medicaidHouseholdMemberIdentifiers;
	}

	public String getMedicaidTaxRoleType() {
	  return medicaidTaxRoleType;
	 }

	 public String getMedicaidHouseHoldStatus() {
	  return medicaidHouseHoldStatus;
	 }

	 public String getMedicaidHouseHoldStatusReason() {
	  return medicaidHouseHoldStatusReason;
	 }

	 public Integer getMedicaidHouseholdSize() {
	  return medicaidHouseholdSize;
	 }

	 // Setter Methods 

	 public void setMedicaidTaxRoleType(String medicaidTaxRoleType) {
	  this.medicaidTaxRoleType = medicaidTaxRoleType;
	 }

	 public void setMedicaidHouseHoldStatus(String medicaidHouseHoldStatus) {
	  this.medicaidHouseHoldStatus = medicaidHouseHoldStatus;
	 }

	 public void setMedicaidHouseHoldStatusReason(String medicaidHouseHoldStatusReason) {
	  this.medicaidHouseHoldStatusReason = medicaidHouseHoldStatusReason;
	 }

	 public void setMedicaidHouseholdSize(Integer medicaidHouseholdSize) {
	  this.medicaidHouseholdSize = medicaidHouseholdSize;
	 }
	}
