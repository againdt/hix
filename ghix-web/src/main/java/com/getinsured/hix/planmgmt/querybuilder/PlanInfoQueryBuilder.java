package com.getinsured.hix.planmgmt.querybuilder;

public class PlanInfoQueryBuilder {

	private static String healthPlanInfoQuery;
	private static String healthPlanInfoQueryForPostGres;
	private static String dentalPlanInfoQuery;
	
	public static String getHealthPlanInfoQuery(){
		if(PlanInfoQueryBuilder.healthPlanInfoQuery != null){
			return PlanInfoQueryBuilder.healthPlanInfoQuery;
		}	
				
		StringBuilder buildQuery = new StringBuilder(1024);
		buildQuery.append("SELECT p.id AS planId, p.NAME AS planName, p.NETWORK_TYPE AS networkType, i.NAME AS issuerName, ");
		buildQuery.append("i.COMPANY_LOGO as companyLogo, phc.name as costName,  phc.in_network_ind, phc.in_network_fly, ");
		buildQuery.append("listagg(phb.name, ',')  within group (order by phb.name) as benefitlist, ");
		buildQuery.append("listagg( CASE WHEN phb.network_t1_display IS null THEN 'NA' ELSE phb.network_t1_display END, ',')  WITHIN GROUP (order by phb.name) AS NETWORK_T1_DISPLAY ");
		buildQuery.append("FROM "); 
		buildQuery.append("plan p, Issuers i, plan_health ph, ");
		buildQuery.append("plan_health_benefit phb, plan_health_cost phc ");
		buildQuery.append("WHERE ");
		buildQuery.append("p.issuer_id = i.id ");
		buildQuery.append("AND p.INSURANCE_TYPE='HEALTH' ");
		buildQuery.append("AND ph.plan_id = p.id ");
		buildQuery.append("AND phb.plan_health_id = ph.id ");
		buildQuery.append("AND phc.plan_health_id = ph.id ");
		buildQuery.append("AND phc.name in ('DEDUCTIBLE_MEDICAL', 'DEDUCTIBLE_INTG_MED_DRUG') ");
		buildQuery.append("AND phb.name in ('PRIMARY_VISIT', 'GENERIC') ");
		buildQuery.append("AND p.ID IN(:param_planIdList) ");
		buildQuery.append("GROUP BY (p.id,  p.INSURANCE_TYPE, p.NAME,p.NETWORK_TYPE,i.NAME,i.COMPANY_LOGO, phc.name,phc.in_network_ind, phc.in_network_fly ) ");
		 
		PlanInfoQueryBuilder.healthPlanInfoQuery =  buildQuery.toString().trim();
		return PlanInfoQueryBuilder.healthPlanInfoQuery;
	}
	
	
	public static String getHealthPlanInfoQueryForPostGres(){
		if(PlanInfoQueryBuilder.healthPlanInfoQueryForPostGres != null){
			return PlanInfoQueryBuilder.healthPlanInfoQueryForPostGres;
		}	
				
		StringBuilder buildQuery = new StringBuilder(1024);
		buildQuery.append("SELECT p.id AS planId, p.NAME AS planName, p.NETWORK_TYPE AS networkType, i.NAME AS issuerName, ");
		buildQuery.append("i.COMPANY_LOGO as companyLogo, phc.name as costName,  phc.in_network_ind, phc.in_network_fly, ");
		buildQuery.append("string_agg(phb.name, ',' order by phb.name) as benefitlist, ");
		buildQuery.append("string_agg( CASE WHEN phb.network_t1_display IS null THEN 'NA' ELSE phb.network_t1_display END, ',' order by phb.name) AS NETWORK_T1_DISPLAY ");
		buildQuery.append("FROM "); 
		buildQuery.append("plan p, Issuers i, plan_health ph, ");
		buildQuery.append("plan_health_benefit phb, plan_health_cost phc ");
		buildQuery.append("WHERE ");
		buildQuery.append("p.issuer_id = i.id ");
		buildQuery.append("AND p.INSURANCE_TYPE='HEALTH' ");
		buildQuery.append("AND ph.plan_id = p.id ");
		buildQuery.append("AND phb.plan_health_id = ph.id ");
		buildQuery.append("AND phc.plan_health_id = ph.id ");
		buildQuery.append("AND phc.name in ('DEDUCTIBLE_MEDICAL', 'DEDUCTIBLE_INTG_MED_DRUG') ");
		buildQuery.append("AND phb.name in ('PRIMARY_VISIT', 'GENERIC') ");
		buildQuery.append("AND p.ID IN(:param_planIdList) ");
		buildQuery.append("GROUP BY (p.id,  p.INSURANCE_TYPE, p.NAME,p.NETWORK_TYPE,i.NAME,i.COMPANY_LOGO, phc.name,phc.in_network_ind, phc.in_network_fly ) ");
		 
		PlanInfoQueryBuilder.healthPlanInfoQueryForPostGres =  buildQuery.toString().trim();
		return PlanInfoQueryBuilder.healthPlanInfoQueryForPostGres;
	}

	
	public static String getDentalPlanInfoQuery(){
		if(PlanInfoQueryBuilder.dentalPlanInfoQuery != null){
			return PlanInfoQueryBuilder.dentalPlanInfoQuery;
		}			
		
		StringBuilder buildQuery = new StringBuilder(1024);
		buildQuery.append("SELECT p.id AS planId, p.NAME AS planName, p.NETWORK_TYPE AS networkType, i.NAME AS issuerName, ");
		buildQuery.append("i.COMPANY_LOGO as companyLogo, pdc.name as costName, pdc.in_network_ind, pdc.in_network_fly ");
		buildQuery.append("FROM "); 
		buildQuery.append("plan p, Issuers i, plan_dental pd, plan_dental_cost pdc ");
		buildQuery.append("WHERE ");
		buildQuery.append("p.issuer_id = i.id ");
		buildQuery.append("AND p.INSURANCE_TYPE='DENTAL' ");
		buildQuery.append("AND pd.plan_id = p.id ");
		buildQuery.append("AND pdc.plan_dental_id = pd.id ");
		buildQuery.append("AND pdc.name in ('DEDUCTIBLE_MEDICAL', 'DEDUCTIBLE_INTG_MED_DRUG') ");
		buildQuery.append("AND p.ID IN(:param_planIdList) ");

		PlanInfoQueryBuilder.dentalPlanInfoQuery =  buildQuery.toString().trim();
		return PlanInfoQueryBuilder.dentalPlanInfoQuery;
	}
	
}
