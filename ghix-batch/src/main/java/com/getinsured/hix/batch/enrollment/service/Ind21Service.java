package com.getinsured.hix.batch.enrollment.service;

import java.util.Map;

import com.getinsured.hix.platform.util.exception.GIException;

public interface Ind21Service {
	 Map<Integer, String> sendCarrierUpdatedDataToAHBX(long jobExecutionId) throws Exception;

}
