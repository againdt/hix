package com.getinsured.hix.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import com.getinsured.hix.model.entity.Assister;

public interface IAssisterRepository extends JpaRepository<Assister, Integer>, RevisionRepository<Assister, Integer, Integer>{
	
}