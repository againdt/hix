/**
 * Created by sellathurai_s on 8/30/18.
 */

import React from "react";
import { FormattedMessage } from 'react-intl';

class ProcessHouseHold {

    constructor(tabData, config) {
        this.tabData = tabData;
        this.config = config;
    }

    isTrue(field) {
        if(field && typeof field === 'string') {
            return (field && (field.toLowerCase() === "y" || field.toLowerCase() === "yes" || field === true));
        } else {
            return field;
        }
    }

    concatStatuses(eligibilityStatuses) {
        // Concatenates all statuses with '|'.
        return eligibilityStatuses.length === 0 ? eligibilityStatuses : eligibilityStatuses.map((eligibilityStatus, index) => {
            return index < eligibilityStatuses.length - 1 ? [eligibilityStatus, '|'] : [eligibilityStatus]
        }).reduce((a, b) => [...a, ...b]);
    }
    
    getAssessedMedicaidOrChipEligibility(){
    	
    	let isEligible = false;
    	
    	let assessedChipEligibile = this.isEligible(this.tabData.applicants, "assessedChipEligibile");
    	let assessedMedicaidMAGIEligibile = this.isEligible(this.tabData.applicants, "assessedMedicaidMAGIEligibile");
    	let assessedMedicaidNonMAGIEligibile = this.isEligible(this.tabData.applicants, "assessedMedicaidNonMAGIEligibile");
    	if(assessedChipEligibile || assessedMedicaidMAGIEligibile || assessedMedicaidNonMAGIEligibile){
    		isEligible = true;
    	}
    	return isEligible;
    }

    getEligibleStatus(applicant, skipNotEligibleAptc = false) {
        const stateCode = $('#stateCode').val();

        let eligibilityStatuses = [];

        if (!this.isTrue(applicant.seekingCoverage)) {
            eligibilityStatuses = eligibilityStatuses.concat(<FormattedMessage id="household_section.person_status.not_seeking_coverage" key="not_seeking_coverage"/>);
            return this.concatStatuses(eligibilityStatuses);
        }

        if (applicant.nativeAmerican && this.isTrue(applicant.nativeAmerican)) {
            eligibilityStatuses = eligibilityStatuses.concat(<FormattedMessage id="household_section.person_status.ai_an" key="ai_an"/>);
        }

        if (stateCode !== 'ID' && (applicant.chipEligible && this.isTrue(applicant.chipEligible))) {
            eligibilityStatuses = eligibilityStatuses.concat(<FormattedMessage id="household_section.person_status.chip_eligible" key="chip_eligible"/>);
            return this.concatStatuses(eligibilityStatuses);
        }
        
        if (stateCode !== 'ID' && (applicant.assessedChipEligibile && this.isTrue(applicant.assessedChipEligibile))) {
            eligibilityStatuses = eligibilityStatuses.concat(<FormattedMessage id="household_section.person_status.chip_assessed" key="chip_assessed"/>);
            return this.concatStatuses(eligibilityStatuses);
        }

        if (stateCode !== 'ID' && ((applicant.medicaidMAGIEligibility && this.isTrue(applicant.medicaidMAGIEligibility)) || (applicant.medicaidNonMAGIEligibile && this.isTrue(applicant.medicaidNonMAGIEligibile)))) {
            eligibilityStatuses = eligibilityStatuses.concat(<FormattedMessage id="household_section.person_status.medical_eligible" key="medical_eligible"/>);
            return this.concatStatuses(eligibilityStatuses);
        }
        
        if (stateCode !== 'ID' && ((applicant.assessedMedicaidMAGIEligibile && this.isTrue(applicant.assessedMedicaidMAGIEligibile)) || (applicant.assessedMedicaidNonMAGIEligibile && this.isTrue(applicant.assessedMedicaidNonMAGIEligibile)))) {
            eligibilityStatuses = eligibilityStatuses.concat(<FormattedMessage id="household_section.person_status.medical_assessed" key="medical_assessed"/>);
            return this.concatStatuses(eligibilityStatuses);
        }

        if (!this.isTrue(applicant.exchangeEligible)) {
            eligibilityStatuses = eligibilityStatuses.concat(<FormattedMessage id="household_section.person_status.not_eligible" key="not_eligible"/>);
            return this.concatStatuses(eligibilityStatuses);
        }

        let aptcEligible = this.isEligible(this.tabData.applicants, "aptcEligible");
        let stateSubsidyEligible = this.isEligible(this.tabData.applicants, "stateSubsidyEligible");

        // If all the members are not eligible for APTC, don't need to display this status.
        if (!skipNotEligibleAptc && (aptcEligible || stateSubsidyEligible)) {
            if (!(this.isTrue(applicant.aptcEligible) || this.isTrue(applicant.stateSubsidyEligible))) {
                eligibilityStatuses = eligibilityStatuses.concat(<FormattedMessage id="household_section.person_status.not_eligible_for_aptc" key="not_eligible_for_aptc"/>);
                return this.concatStatuses(eligibilityStatuses);
            }
        }

        return this.concatStatuses(eligibilityStatuses);
    }

    /**
     *  Check if all the members are not eligible for APTC
     * @returns {boolean}
     */
    checkTotalNotEligibleForAPTC = () => {
        let aptcEligible = this.isEligible(this.tabData.applicants, "aptcEligible");
        let stateSubsidyEligible = this.isEligible(this.tabData.applicants, "stateSubsidyEligible");
        let totalNotEligibleForAPTC = 0;
        if (this.tabData.applicants) {
            this.tabData.applicants.filter((applicant) => {
                if (aptcEligible || stateSubsidyEligible) {
                    if (!(this.isTrue(applicant.aptcEligible) || this.isTrue(applicant.stateSubsidyEligible))) {
                        totalNotEligibleForAPTC++;
                    }
                }
            });
            return totalNotEligibleForAPTC === this.tabData.applicants.length;
        }
        
        return false;
    };


    getHouseHoldMembers = () => {
        let members = [];
        const totalNotEligibleForAPTC = this.checkTotalNotEligibleForAPTC();
        if (this.tabData.applicants) {
            members = this.tabData.applicants.map((item) => {
                return {
                    label: item.firstName + (item.middleName ? (" " + item.middleName) : "") + " " + item.lastName + (item.nameSuffix ? (" " + item.nameSuffix) : ""),
                    eligible: totalNotEligibleForAPTC ? this.getEligibleStatus(item, true) : this.getEligibleStatus(item) ,
                    textType: "ds-u-color--success"
                }
            });
        }
        return members;
    };

    isEligible(memberListData, flag) {

        if(memberListData !== undefined && memberListData[flag] !== undefined){
            var preEligibilityData = memberListData;
            var elegibilityFlag = this.tabData.preEligibility[flag];
            if(elegibilityFlag === true){
                return (preEligibilityData.members && preEligibilityData.members.length > 0);
            }
        }
        else if (memberListData !== undefined) {
            let members = memberListData.filter((item, i) => this.isTrue(item[flag]), this);
            return (members && members.length > 0);
        }
        
        return false;
    }

    getHouseHoldStatus() {
        let status = [];

        let aptcEligible = this.isEligible(this.tabData.applicants, "aptcEligible");

        if (aptcEligible) {
            status.push({
                label: <FormattedMessage id="eligibility_status.advance_tax_credit"/>,
                textType: 'ds-u-font-weight--normal'
            });

            if (this.tabData.aptc !== null && this.tabData.aptc !== undefined) {
                status.push({
                    label:
                        <FormattedMessage id="eligibility_status.aptc_per_month">
                            {(txt) => {
                                return `$${parseFloat(this.tabData.aptc).toFixed(2)} ${txt}`
                            }}
                        </FormattedMessage>,
                    textType: 'ds-u-font-weight--bold'
                });
            }
        }

        let stateSubsidyEligible = this.isEligible(this.tabData.applicants, "stateSubsidyEligible");
        if (stateSubsidyEligible && typeof this.tabData.stateSubsidy !== "undefined") {
            status.push({
                label: <FormattedMessage id="eligibility_status.state_subsidy"/>,
                textType: 'ds-u-font-weight--normal'
            });

            status.push({
                label:

                    <FormattedMessage id="eligibility_status.aptc_per_month">
                        {(txt) => {
                            return `$${parseFloat(this.tabData.stateSubsidy).toFixed(2)} ${txt}`
                        }}
                    </FormattedMessage>,
                textType: 'ds-u-font-weight--bold'
            });
        }


        let csrEligible = this.isEligible(this.tabData.applicants, "csrEligible");

        if (csrEligible) {
            status.push({
                label: <FormattedMessage id="eligibility_status.eligible_csr "/>,
                textType: 'ds-u-font-weight--normal'
            });
        }

        if (!aptcEligible && !csrEligible && !stateSubsidyEligible) {
            status.push({
                label: <FormattedMessage id="eligibility_status.eligible_csr_aptc"/>,
                textType: 'ds-u-font-weight--normal'
            });
        }

        return status;
    }

    getHouseHoldLinkInfo() {
        var linkInfo = "";
        if ($("#stateCode").val() === "CA") {
            if ($("#householdCaseID").val()) {
                linkInfo = "https://" + calheersEnv + "/static/lw-web/eligibility?householdCaseId=" + $("#householdCaseID").val();
            } else {
                linkInfo = "https://" + calheersEnv + "/static/lw-web/eligibility";
            }
        }

        return {
            linkText: <FormattedMessage id="eligibility_status.view_details"/>,
            link: linkInfo
        };
    }

    getPreEligibility() {
        if (this.tabData.preEligibility &&
            this.tabData.preEligibility.members &&
            this.tabData.preEligibility.members.length > 0) {
            return "Pre Eligibility";
        }

        return "N/A";
    }

    getEligibilityType(isAPTCEligible, isCSREligible) {
        // console.log("getEligibilityType");
        let csr_tc=[];
        const taxCredit = <FormattedMessage id="eligibility_status.tax_credit"/>;
        const csr = <FormattedMessage id="eligibility_status.cost_sharing_reduction_program"/>;
        
        csr_tc= csr_tc.concat(<FormattedMessage id="eligibility_status.tax_credit" key="tax_credit"/>);
        csr_tc= csr_tc.concat(<FormattedMessage id="eligibility_status.and" key="and"/>)
        csr_tc=csr_tc.concat(<FormattedMessage id="eligibility_status.cost_sharing_reduction_program" key="cost_sharing_reduction_program"/>);
        <FormattedMessage id="household_section.person_status.not_eligible_for_aptc" key="not_eligible_for_aptc"/>

        if (isAPTCEligible && isCSREligible)
            return csr_tc;
        else if (isAPTCEligible) {
            return taxCredit;
        } else if (isCSREligible) {
            return csr;
        }
    }

    getHouseHoldEligibility() {
        const ELIG_ONCE_COMPLETED_MSG = <FormattedMessage id="no_household_section.elig_once_completed"/>;
        const ELIG_FOR_TC_CSR = <FormattedMessage id="no_household_section.elig_once_completed"/>;
        const ELIG_FOR = <FormattedMessage id="no_household_section.elig_for"/>;
        const ELIG_FOR_PRE = <FormattedMessage id="no_household_section.elig_for_pre"/>;

        let houseHoldEligibilityData = {};

        let isPreEligAPTCEligible = (this.tabData.preEligibility && this.tabData.preEligibility.members) ?
            this.isEligible(this.tabData.preEligibility, "aptcEligible") : false;
        let isPreEligCSREligible = (this.tabData.preEligibility && this.tabData.preEligibility.members) ?
            this.isEligible(this.tabData.preEligibility, "csrEligible") : false;

        let eligType = this.getEligibilityType(isPreEligAPTCEligible, isPreEligCSREligible);

        switch (this.tabData.applicationStatus) {
            case 'ER':
            case 'PN':
            case 'EN':
            case 'CL':
                houseHoldEligibilityData = {
                    type: 'MULTIPLE_COL',
                    content: '',
                    members: this.getHouseHoldMembers(),
                    status: this.getHouseHoldStatus(),
                    linkInfo: this.getHouseHoldLinkInfo(),
                };
                break;

            case 'CC':
            case 'UC':
                houseHoldEligibilityData = {
                    type: 'SINGLE_COL',
                    content: ELIG_ONCE_COMPLETED_MSG,
                    members: this.getHouseHoldMembers(),
                    status: [],
                    linkInfo: [],
                };
                break;

            case 'OP':
                houseHoldEligibilityData = {
                    type: 'SINGLE_COL',
                    members: this.getHouseHoldMembers(),
                    status: [],
                    linkInfo: [],
                };

                if (this.getPreEligibility() === "N/A") {
                    houseHoldEligibilityData.content = ELIG_ONCE_COMPLETED_MSG;
                } else {
                    houseHoldEligibilityData.content = <div>{ELIG_FOR_TC_CSR}{ELIG_FOR}{eligType}</div>;
                }
                break;

            default:
                houseHoldEligibilityData = {
                    type: 'SINGLE_COL',
                    members: this.getHouseHoldMembers(),
                    status: [],
                    linkInfo: [],
                };

                if (this.getPreEligibility() === "N/A") {
                    houseHoldEligibilityData.content = ELIG_ONCE_COMPLETED_MSG;
                } else {
                    if (this.tabData.applicationStatus !== 'OP') {
                        houseHoldEligibilityData.content = <div>{ELIG_FOR_TC_CSR}{ELIG_FOR}{eligType}</div>;
                    } else {
                        houseHoldEligibilityData.content = <div>{ELIG_FOR_TC_CSR}{ELIG_FOR_PRE}{eligType}</div>;
                    }
                }
        }

        // console.log(" Results of  getHouseHoldEligibility - " + JSON.stringify(houseHoldEligibilityData));

        return houseHoldEligibilityData;
    }

    getHouseHoldInfo() {
        return this.getHouseHoldEligibility();
    }
}

export { ProcessHouseHold as default };