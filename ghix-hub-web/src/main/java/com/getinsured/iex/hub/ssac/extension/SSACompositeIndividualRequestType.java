
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Boolean;
import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;



/**
 * Request from HUB to SSA for each individual for whom verification is to be performed.
 *             
 * 
 * <p>Java class for SSACompositeIndividualRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSACompositeIndividualRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}Person"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}RequestCitizenshipVerificationIndicator"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}RequestIncarcerationVerificationIndicator"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}RequestTitleIIMonthlyIncomeVerificationIndicator"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}RequestTitleIIAnnualIncomeVerificationIndicator"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}RequestQuartersOfCoverageVerificationIndicator"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}RequestTitleIIMonthlyIncomeDate" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}RequestTitleIIAnnualIncomeDate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSACompositeIndividualRequestType", propOrder = {
    "person",
    "requestCitizenshipVerificationIndicator",
    "requestIncarcerationVerificationIndicator",
    "requestTitleIIMonthlyIncomeVerificationIndicator",
    "requestTitleIIAnnualIncomeVerificationIndicator",
    "requestQuartersOfCoverageVerificationIndicator",
    "requestTitleIIMonthlyIncomeDate",
    "requestTitleIIAnnualIncomeDate"
})
public class SSACompositeIndividualRequestType
    extends ComplexObjectType
{

    @XmlElement(name = "Person", required = true)
    protected PersonType person;
    @XmlElement(name = "RequestCitizenshipVerificationIndicator", required = true)
    protected Boolean requestCitizenshipVerificationIndicator;
    @XmlElement(name = "RequestIncarcerationVerificationIndicator", required = true)
    protected Boolean requestIncarcerationVerificationIndicator;
    @XmlElement(name = "RequestTitleIIMonthlyIncomeVerificationIndicator", required = true)
    protected Boolean requestTitleIIMonthlyIncomeVerificationIndicator;
    @XmlElement(name = "RequestTitleIIAnnualIncomeVerificationIndicator", required = true)
    protected Boolean requestTitleIIAnnualIncomeVerificationIndicator;
    @XmlElement(name = "RequestQuartersOfCoverageVerificationIndicator", required = true)
    protected Boolean requestQuartersOfCoverageVerificationIndicator;
    @XmlElement(name = "RequestTitleIIMonthlyIncomeDate")
    protected String requestTitleIIMonthlyIncomeDate;
    @XmlElement(name = "RequestTitleIIAnnualIncomeDate")
    protected String requestTitleIIAnnualIncomeDate;

    /**
     * Gets the value of the person property.
     * 
     * @return
     *     possible object is
     *     {@link PersonType }
     *     
     */
    public PersonType getPerson() {
        return person;
    }

    /**
     * Sets the value of the person property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonType }
     *     
     */
    public void setPerson(PersonType value) {
        this.person = value;
    }

    /**
     * Gets the value of the requestCitizenshipVerificationIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRequestCitizenshipVerificationIndicator() {
        return requestCitizenshipVerificationIndicator;
    }

    /**
     * Sets the value of the requestCitizenshipVerificationIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequestCitizenshipVerificationIndicator(Boolean value) {
        this.requestCitizenshipVerificationIndicator = value;
    }

    /**
     * Gets the value of the requestIncarcerationVerificationIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRequestIncarcerationVerificationIndicator() {
        return requestIncarcerationVerificationIndicator;
    }

    /**
     * Sets the value of the requestIncarcerationVerificationIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequestIncarcerationVerificationIndicator(Boolean value) {
        this.requestIncarcerationVerificationIndicator = value;
    }

    /**
     * Gets the value of the requestTitleIIMonthlyIncomeVerificationIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRequestTitleIIMonthlyIncomeVerificationIndicator() {
        return requestTitleIIMonthlyIncomeVerificationIndicator;
    }

    /**
     * Sets the value of the requestTitleIIMonthlyIncomeVerificationIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequestTitleIIMonthlyIncomeVerificationIndicator(Boolean value) {
        this.requestTitleIIMonthlyIncomeVerificationIndicator = value;
    }

    /**
     * Gets the value of the requestTitleIIAnnualIncomeVerificationIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRequestTitleIIAnnualIncomeVerificationIndicator() {
        return requestTitleIIAnnualIncomeVerificationIndicator;
    }

    /**
     * Sets the value of the requestTitleIIAnnualIncomeVerificationIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequestTitleIIAnnualIncomeVerificationIndicator(Boolean value) {
        this.requestTitleIIAnnualIncomeVerificationIndicator = value;
    }

    /**
     * Gets the value of the requestQuartersOfCoverageVerificationIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getRequestQuartersOfCoverageVerificationIndicator() {
        return requestQuartersOfCoverageVerificationIndicator;
    }

    /**
     * Sets the value of the requestQuartersOfCoverageVerificationIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequestQuartersOfCoverageVerificationIndicator(Boolean value) {
        this.requestQuartersOfCoverageVerificationIndicator = value;
    }

    /**
     * Gets the value of the requestTitleIIMonthlyIncomeDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestTitleIIMonthlyIncomeDate() {
        return requestTitleIIMonthlyIncomeDate;
    }

    /**
     * Sets the value of the requestTitleIIMonthlyIncomeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestTitleIIMonthlyIncomeDate(String value) {
        this.requestTitleIIMonthlyIncomeDate = value;
    }

    /**
     * Gets the value of the requestTitleIIAnnualIncomeDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestTitleIIAnnualIncomeDate() {
        return requestTitleIIAnnualIncomeDate;
    }

    /**
     * Sets the value of the requestTitleIIAnnualIncomeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestTitleIIAnnualIncomeDate(String value) {
        this.requestTitleIIAnnualIncomeDate = value;
    }

}
