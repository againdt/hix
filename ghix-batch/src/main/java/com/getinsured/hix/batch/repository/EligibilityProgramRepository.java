package com.getinsured.hix.batch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.eligibility.model.EligibilityProgram;

public interface EligibilityProgramRepository extends JpaRepository<EligibilityProgram, Long> {

    @Query("Select ep " +
            " FROM EligibilityProgram as ep "+
            " inner join fetch ep.ssapApplicant as ssapApplicant"+
            " inner join fetch ssapApplicant.ssapApplication as ssapApplication"+
            " where ssapApplication.id = :ssapApplicationId ")
    List<EligibilityProgram> getEligibilitiesForApplication(@Param("ssapApplicationId") Long ssapApplicationId);

}
