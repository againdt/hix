package com.getinsured.iex.hub.ssac.wrapper;

import java.io.IOException;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.GHIXApplicationContext;
import com.getinsured.iex.hub.ssac.extension.ObjectFactory;
import com.getinsured.iex.hub.ssac.extension.SSACompositeIndividualRequestType;
import com.google.gson.Gson;


public class SSACompositeIndividualRequestWrapper implements JSONAware {

	private PersonWrapper person;
	private SSACompositeIndividualRequestType individual;
	private boolean personCitizenIndicator;
	private boolean personIncarcerationIndicator;
	private boolean qtrCoverageIndicator;
	private boolean titleIIAnnualIncomeIndicator;
	private boolean titleIIMonthlyIncomeIndicator;
	private String requestTitleIIMonthlyIncomeDate;
	private String requestTitleIIAnnualIncomeDate;

	private static Gson gson ;
	
	static{
		gson = (Gson) GHIXApplicationContext.getBean("iex_platformGson");
	}
	public SSACompositeIndividualRequestWrapper() {
		ObjectFactory factory = new ObjectFactory();
		this.individual = factory.createSSACompositeIndividualRequestType();
		
	}
	
	public void setPerson(PersonWrapper person) {
		this.individual.setPerson(person.getPersonType());
	}
	
	public PersonWrapper getPerson() {
		return this.person;
	}
	
	public boolean isPersonCitizenIndicator() {
		return personCitizenIndicator;
	}

	public void setPersonCitizenIndicator(boolean personCitizenIndicator) {
		RequestCitizenshipVerificationIndicatorWrapper reqCitizenshipVerificationIndicator = new RequestCitizenshipVerificationIndicatorWrapper();
		reqCitizenshipVerificationIndicator.setRequestCitizenshipVerificationIndicator(personCitizenIndicator);
		this.personCitizenIndicator = personCitizenIndicator;
		this.individual.setRequestCitizenshipVerificationIndicator(reqCitizenshipVerificationIndicator.getSystemRepresentation());
	}

	public boolean isPersonIncarcerationIndicator() {
		return personIncarcerationIndicator;
	}

	public void setPersonIncarcerationIndicator(boolean personIncarcerationIndicator) {
		RequestIncarcerationVerificationIndicatorWrapper reqIncarcerationVerificationWrapper = new RequestIncarcerationVerificationIndicatorWrapper();
		reqIncarcerationVerificationWrapper.setIncarcerationVerificationRequested(personIncarcerationIndicator);
		this.personIncarcerationIndicator = personIncarcerationIndicator;
		this.individual.setRequestIncarcerationVerificationIndicator(reqIncarcerationVerificationWrapper.getSystemRepresentation());
	}

	public boolean isQtrCoverageIndicator() {
		return qtrCoverageIndicator;
	}

	public void setQtrCoverageIndicator(boolean qtrCoverageIndicator) {
		RequestQuartersOfCoverageVerificationIndicatorWrapper reqQtrOfCoverage = new RequestQuartersOfCoverageVerificationIndicatorWrapper();
		reqQtrOfCoverage.setQuartersOfCoverageVerificationRequested(qtrCoverageIndicator);
		this.qtrCoverageIndicator = qtrCoverageIndicator;
		this.individual.setRequestQuartersOfCoverageVerificationIndicator(reqQtrOfCoverage.getSystemRepresentation());
	}

	public boolean isTitleIIAnnualIncomeIndicator() {
		return titleIIAnnualIncomeIndicator;
	}

	public void setTitleIIAnnualIncomeIndicator(boolean titleIIAnnualIncomeIndicator) {
		RequestTitleIIAnnualIncomeVerificationIndicatorWrapper reqTitleAnnualIndicator = new RequestTitleIIAnnualIncomeVerificationIndicatorWrapper();
		reqTitleAnnualIndicator.setTitleIIAnnualIncomeVerificationRequested(titleIIAnnualIncomeIndicator);
		this.titleIIAnnualIncomeIndicator = titleIIAnnualIncomeIndicator;
		this.individual.setRequestTitleIIAnnualIncomeVerificationIndicator(reqTitleAnnualIndicator.getSystemRepresentation());
	}

	public boolean isTitleIIMonthlyIncomeIndicator() {
		return titleIIMonthlyIncomeIndicator;
	}

	public void setTitleIIMonthlyIncomeIndicator(
			boolean titleIIMonthlyIncomeIndicator) {
		RequestTitleIIMonthlyIncomeVerificationIndicatorWrapper reqTitleMonthlyIndicator = new RequestTitleIIMonthlyIncomeVerificationIndicatorWrapper();
		reqTitleMonthlyIndicator.setTitleIIMonthlyIncomeVerificationRequested(titleIIMonthlyIncomeIndicator);
		this.titleIIMonthlyIncomeIndicator = titleIIMonthlyIncomeIndicator;
		this.individual.setRequestTitleIIMonthlyIncomeVerificationIndicator(reqTitleMonthlyIndicator.getSystemRepresentation());
	}

	public String getRequestTitleIIMonthlyIncomeDate() {
		return requestTitleIIMonthlyIncomeDate;
	}

	public void setRequestTitleIIMonthlyIncomeDate(
			String requestTitleIIMonthlyIncomeDate) {
		this.requestTitleIIMonthlyIncomeDate = requestTitleIIMonthlyIncomeDate;
		this.individual.setRequestTitleIIMonthlyIncomeDate(requestTitleIIMonthlyIncomeDate);
	}

	public String getRequestTitleIIAnnualIncomeDate() {
		return requestTitleIIAnnualIncomeDate;
	}

	public void setRequestTitleIIAnnualIncomeDate(
			String requestTitleIIAnnualIncomeDate) {
		this.requestTitleIIAnnualIncomeDate = requestTitleIIAnnualIncomeDate;
		this.individual.setRequestTitleIIAnnualIncomeDate(requestTitleIIAnnualIncomeDate);
	}

	public static SSACompositeIndividualRequestWrapper fromJsonString(String jsonString) throws IOException{
		return gson.fromJson(jsonString, SSACompositeIndividualRequestWrapper.class);
	}
	
	public SSACompositeIndividualRequestType getSystemRepresentation(){
		return this.individual;
	}
	
	public String toJSONString() {
		// TBD
		JSONObject obj = new JSONObject();
		//obj.put(SUR_NAME_KEY, this.surName);
		return obj.toJSONString();
	}

	
}
