package com.getinsured.hix.batch.planmgmt.task;

import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.planmgmt.service.PlanMgmtService;

public class UpdateEnrollmentAvailabilityTask implements Tasklet{
	
	private static final Logger LOGGER = Logger.getLogger(UpdateEnrollmentAvailabilityTask.class);

	private PlanMgmtService planMgmtService;
	
	public PlanMgmtService getPlanMgmtService() {
		return planMgmtService;
	}

	public void setPlanMgmtService(PlanMgmtService planMgmtService) {
		this.planMgmtService = planMgmtService;
	}
	
	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext){
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Update plan enrollment availability job started on "+new Timestamp(System.currentTimeMillis()));
		}
		try {
			String response = planMgmtService.updateEnrollmentAvailability();
			 if(LOGGER.isDebugEnabled()){
				 LOGGER.debug("update enrollment availability response " + response);
			 }
		} catch (Exception e) {
			 LOGGER.error("Exception occur while trigger update plan enrollment availability job : " + e);
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Update plan enrollment availability job finished on "+new Timestamp(System.currentTimeMillis()));
		}	
		
		return RepeatStatus.FINISHED;
	}	
}
