package com.getinsured.eligibility.at.service;

import java.util.List;
import java.util.Map;

import com.getinsured.eligibility.at.dto.AccountTransferResponseDTO;
import com.getinsured.eligibility.ssap.model.EligibilityResponse;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferResponsePayloadType;



/**
 * Encapsulates service layer method calls for AccountTransfer.
 *
 * @author Ekram Ali Kazi
 */
public interface AccountTransferService{

	AccountTransferResponseDTO prepareATResponse(AccountTransferRequestPayloadType request, String inputXml);

	void process(AccountTransferRequestPayloadType request,
			Integer giwsPayloadId);

	void process(AccountTransferRequestPayloadType request,
			AccountTransferResponseDTO result);

	AccountTransferRequestPayloadType prepareForReTrigger(Integer giwsPayloadId);

	void logProcessingTime(long giwsPayloadId, long time);

	public GIWSPayload logServicePayload(Long ssapApplicationId, String request, String endpointUrl,String endpointFunction, String response, String status, String operationName);
	
	/*
	 * Added a responseCode for returning different codes for schema and business validations
	 */

	AccountTransferResponsePayloadType formFailureResponse(List<String> errorList, String responseCode);
	
	boolean createSpan(AccountTransferRequestPayloadType request,AccountTransferResponseDTO result);

	void processResponseAT(String clonedAppId, Map<String, Boolean> seekingCoverageaAndQHPMap,
			Map<String, String> applicantEventsMap);
	
	EligibilityResponse invokeEligibilityEngine(long ssapApplicationId);
}
