package com.getinsured.iex.hub.consumers.hub;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.amqp.support.converter.JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.messaging.InvalidProcessingInstructions;
import com.getinsured.iex.hub.platform.messaging.PostProcessor;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.rabbitmq.client.Channel;

public class SSACQueueConsumer extends MessageProcessor implements ChannelAwareMessageListener {

	@Autowired
	private WebServiceTemplate ssacServiceTemplate;

	private String dummyDeatchVerificationPayload = "{\"personId\":1,\"name\":{\"firstName\":\"George\",\"middleName\":\"B\",\"lastName\":\"Prince\"},\"dateOfBirth\":\"Jul 13, 1950 10:00:00 PM\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"221-21-2503\"},\"citizenshipImmigrationStatus\":{\"citizenshipAsAttestedIndicator\":true},\"incarcerationStatus\":{\"incarcerationAsAttestedIndicator\":true}}";
	private static final Logger LOGGER = LoggerFactory.getLogger(SSACQueueConsumer.class);

	private void invokeSSACForSSNVerification(String applicationId, String applicationData, String clientIp) throws HubServiceException {
		
		try{
			ServiceHandler handler = null;
			long appId = -1;
			ResponseEntity<String> hubResponse = null;
			HubServiceBridge ssacServiceBridge = null;
			GIWSPayload request = new GIWSPayload();
			request.setEndpointFunction("SSAC");
			request.setEndpointOperationName("SSN_VERIFICATION");
			request.setRetryCount(MAX_RETRY_COUNT);
			request.setAccessIp(clientIp);
			request.setStatus("PENDING");
			LOGGER.info("Processing request for application Id:"+applicationId);
			if(applicationId != null){
				appId = Long.parseLong(applicationId);
				request.setSsapApplicationId(appId);
			}
			ssacServiceBridge = HubServiceBridge.getHubServiceBridge("VerifySSAComposite");
			ssacServiceBridge.addContextParameter("APPID", appId);
			handler  = ssacServiceBridge.getServiceHandler();
			handler.setJsonInput(applicationData);
			hubResponse = this.executeRequest(request, handler, this.ssacServiceTemplate);
			// Reaching here means everything went well, Now invoke the rules
			Map<String, Object> context = new HashMap<String, Object>();
			context.put(PostProcessor.INPUT_KEY,applicationData);
			context.put(PostProcessor.RESPONSE_KEY, hubResponse);
			context.put(PostProcessor.RULE_NAME_KEY, PostProcessor.SSN_VERIFICATION_RULE);
			this.initiatePostProcessing(PostProcessor.INVOKE_RULES, context);
		}
		catch(Exception e){
			throw new HubServiceException(e);
		}
	}

	private void invokeSSACForIncarcerationVerification(String applicationId, String applicationData, String clientIp) throws HubServiceException {
		
	
		try{
			ServiceHandler handler = null;
			long appId = -1;
			ResponseEntity<String> hubResponse = null;
			HubServiceBridge ssacServiceBridge = null;
			GIWSPayload request = new GIWSPayload();
			request.setEndpointFunction("SSAC");
			request.setEndpointOperationName("INCARCERATION");
			request.setRetryCount(MAX_RETRY_COUNT);
			request.setAccessIp(clientIp);
			request.setStatus("PENDING");
			LOGGER.info("Processing request for application Id:"+applicationId);
			if(applicationId != null){
				appId = Long.parseLong(applicationId);
				request.setSsapApplicationId(appId);
			}
			ssacServiceBridge = HubServiceBridge.getHubServiceBridge("VerifySSAComposite");
			ssacServiceBridge.addContextParameter("APPID", appId);
			handler  = ssacServiceBridge.getServiceHandler();
			handler.setJsonInput(applicationData);
			hubResponse = this.executeRequest(request, handler, this.ssacServiceTemplate);
			// Reaching here means everything went well, Now invoke the rules
			Map<String, Object> context = new HashMap<String, Object>();
			context.put(PostProcessor.INPUT_KEY,applicationData);
			context.put(PostProcessor.RESPONSE_KEY, hubResponse);
			context.put(PostProcessor.RULE_NAME_KEY, PostProcessor.INCARCERATION_VERIFICATION_RULE);
			this.initiatePostProcessing(PostProcessor.INVOKE_RULES, context);
		}
		catch(Exception e){
			throw new HubServiceException(e);
		}
	}

	private void invokeSSACForDeathVerification(String applicationId, String applicationData, String clientIp) throws HubServiceException {
		
		try{
			ServiceHandler handler = null;
			long appId = -1;
			ResponseEntity<String> hubResponse = null;
			HubServiceBridge ssacServiceBridge = null;
			GIWSPayload request = new GIWSPayload();
			request.setEndpointFunction("SSAC");
			request.setEndpointOperationName("DEATH_VERIFICATION");
			request.setRetryCount(MAX_RETRY_COUNT);
			request.setAccessIp(clientIp);
			request.setStatus("PENDING");
			LOGGER.info("Processing request for application Id:"+applicationId);
			if(applicationId != null){
				appId = Long.parseLong(applicationId);
				request.setSsapApplicationId(appId);
			}
			ssacServiceBridge = HubServiceBridge.getHubServiceBridge("VerifySSAComposite");
			ssacServiceBridge.addContextParameter("APPID", appId);
			handler  = ssacServiceBridge.getServiceHandler();
			handler.setJsonInput(applicationData);
			hubResponse = this.executeRequest(request, handler, this.ssacServiceTemplate);
			// Reaching here means everything went well, Now invoke the rules
			Map<String, Object> context = new HashMap<String, Object>();
			context.put(PostProcessor.INPUT_KEY,applicationData);
			context.put(PostProcessor.RESPONSE_KEY, hubResponse);
			context.put(PostProcessor.RULE_NAME_KEY, PostProcessor.DEATH_VERIFICATION_RULE);
			this.initiatePostProcessing(PostProcessor.INVOKE_RULES, context);
		}
		catch(Exception e){
			throw new HubServiceException(e);
		}
	}
	
	private void invokeTestPayload(Message msg) throws ParseException, HubMappingException, BridgeException, HubServiceException, InvalidProcessingInstructions, InputDataValidationException{
		String body = new String(msg.getBody());
		LOGGER.debug("Received payload:"+body);
		JSONParser parser = new JSONParser();
		JSONObject obj = (JSONObject) parser.parse(body);
		String applicationId = (String) obj.get("applicationId");
		String clientIp = (String) obj.get("clientIp");
		String operation = (String) obj.get("operation");
		JSONObject applicationData = (JSONObject)obj.get("payload");
		if(operation.equalsIgnoreCase("ssn")){
			this.invokeSSACForSSNVerification(applicationId, applicationData.toJSONString(), clientIp);
		}else if(operation.equalsIgnoreCase("death")){
			this.invokeSSACForDeathVerification(applicationId, applicationData.toJSONString(), clientIp);
		}else if(operation.equalsIgnoreCase("incarceration")){
			this.invokeSSACForIncarcerationVerification(applicationId, applicationData.toJSONString(), clientIp);
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onMessage(Message message, Channel channel) throws BridgeException, ParseException, HubMappingException, HubServiceException, InvalidProcessingInstructions, InputDataValidationException {
		String routingKey = message.getMessageProperties().getReceivedRoutingKey();
		
		if(routingKey == null){
			throw new IllegalArgumentException("No routing key available for incoming RIDP message, don't know how to process it");
		}
		
		if(routingKey.equalsIgnoreCase("ghix-test-ssac")){
			invokeTestPayload(message);
			return;
		}
		JsonMessageConverter conv = new JsonMessageConverter();
		Object payLoadObj = conv.fromMessage(message);
		try {
			Map<String,String> payload = (HashMap<String, String>) payLoadObj;
			String applicationId = payload.get("applicationId");
			//String applicationData = payload.get("applicationData");
			String clientIp = payload.get("clientIp");
			//String operation = payload.get("operation");
			LOGGER.info("Application Id received:"+applicationId);
			//invokeSSACForSSNVerification(applicationId, applicationData,clientIp);
			//invokeSSACForIncarcerationVerification();
			invokeSSACForDeathVerification(applicationId, this.dummyDeatchVerificationPayload,clientIp);
		}catch (Exception e) {
			LOGGER.info("Exception", e);
		}
	}
}
