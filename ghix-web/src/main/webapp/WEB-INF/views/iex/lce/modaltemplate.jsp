<script type="text/ng-template" id="modaltemplate.jsp">
  <div class='ng-modal' ng-show='show'>
    <div class='ng-modal-overlay'></div>
    <div class='ng-modal-dialog' ng-style='dialogStyle'>
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' ng-click='hideModal()'>
          <span aria-hidden='true'><i class='icon-remove'></i></span>
          <span class='aria-hidden'>close</span>
        </button>
        <h3 ng-switch on="modalEventType"> 
			<div ng-switch-when="name"><spring:message code="label.lce.basicInfoMT"/></div>
			<div ng-switch-when="dob"><spring:message code="label.lce.basicInfoMT"/></div>
			<div ng-switch-when="ssn"><spring:message code="label.lce.basicInfoMT"/></div>
			<div ng-switch-when="tribe"><spring:message code="label.lce.tribeMT"/></div>
			<div ng-switch-when="immigrationStatus"><spring:message code="label.lce.immigrationMT"/></div>

			<div ng-switch-default><spring:message code="label.lce.reportChange"/></div>

		</h3>	
      </div>
      <div class='modal-body' id='modalTemplate' ng-transclude></div>
      <div class='hidden'>
        <a class='btn btn-primary ng-modal-close' ng-click='hideModal()'><spring:message code="lce.cancelbutton"/></a>
        <a class='btn btn-primary ng-modal-close' ng-click='hideModal()'><spring:message code="label.lce.save"/></a>
      </div>
    </div>
  </div>
</script>