<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%-- <jsp:include page="/WEB-INF/views/layouts/untiletopbar.jsp" /> --%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page isELIgnored="false"%>


<div class="gutter10">
    <div class="row-fluid">
		<h1><spring:message  code='pgheader.notifications'/>&nbsp;&nbsp;<small>${resultSize}&nbsp;<spring:message  code='pheader.totalNotifications'/></small></h1>     
    </div><!--  end of row-fluid -->                                                                                      
    <div  style="align:center ;font-size: 14px; color: green ">
	    <c:if test="${Template_Updt_Msg != null && Template_Updt_Msg != ''}">
			<p><c:out value="${Template_Updt_Msg}"></c:out><p/>
			<c:remove var="Template_Updt_Msg"/>
		</c:if>
	</div>
    <div class="row-fluid">
    	<div class="span3" id="sidebar">
    		<div class="header">
				<h4 class="margin0">Refine Results</h4>
			</div>
		
             <form class="form-vertical gutter10 lightgray" id="frmnoticelist" name="frmnoticelist" action="<c:url value="/admin/managenotice" />" method="POST" >
					<df:csrfToken/>
					
					<input type="hidden" id="sortBy" name="sortBy" value="id">
					<input type="hidden" id="sortOrder" name="sortOrder" value="DESC" >
					<input type="hidden" id="pageNumber" name="pageNumber" value="1">
					<input type="hidden" id="changeOrder" name="changeOrder" >
						
                    <div class="gutter10 graybg">
                    	<div class="control-group">
                            <label for="notificationname" class="control-label"><spring:message  code='label.name'/></label>
                            <div class="controls">
                                <input class="input-medium" type="text" value="${searchCriteria.name}"  name="notificationName" id="notificationName"  size="20"/>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                        
                        <div class="control-group">
                            <label for="user" class="control-label"><spring:message  code='label.user'/></label>
                            <div class="controls">
                                <select  name="user" id="user" class="input-medium">
                                    <option value="">All</option>
                                 <c:forEach var="users" items="${usersList}">
									<option <c:if test="${users == searchCriteria.user}"> SELECTED </c:if> value="${users}">${users.roleName}</option>
								</c:forEach>
                                    
                                </select>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                         <div class="control-group">
                            <label for="type" class="control-label"><spring:message  code='label.type'/></label>
                            <div class="controls">
                                <select  name="type" id="type" class="input-medium">
                                    <option value="">All</option>
                                 <c:forEach var="types" items="${typeStatusList}">
									<option <c:if test="${types == searchCriteria.type}"> SELECTED </c:if> value="${types}">${types.code}</option>
								</c:forEach>
                                    
                                </select>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                        <div class="control-group">
                            <label for="method" class="control-label"><spring:message  code='label.method'/></label>
                            <div class="controls">
                                <select  name="method" id="method" class="input-medium">
                                    <option value="">All</option>
                                 <c:forEach var="methods" items="${methodsList}">
									<option <c:if test="${methods == searchCriteria.method}"> SELECTED </c:if> value="${methods}">${methods.method}</option>
								</c:forEach>
                                    
                                </select>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                        <div class="control-group">
	                        <div >
	                        <label class="checkbox">
                  <input type="hidden" name="hideTerminatedValue" id="hideTerminatedValue" value="${fn:contains(searchCriteria.hideTerminated, 'NO')? 'NO' : 'YES'}" />
                  <input type="checkbox" name="hideTerminated" value='NO' id="hideTerminated" ${fn:contains(searchCriteria.hideTerminated, 'YES') ? 'checked="checked"' : ''} onclick="update_hideTerminated(this.id)">
                  Show Terminated </label>
	                       </div>
                         </div><!-- end of control-group -->
                        
                        <div class="center">
                           <input type="submit" name="submitButton" id="submitButton" value="<spring:message  code="label.go"/>" class="btn" title="<spring:message  code="label.go"/>"/>
                        </div>
                    </div>
					
					
			 </form>
                 <a href="<c:url value="/admin/noticetype"/>" class="btn btn-primary span11" id="addNewNotification" name="addNewNotification" style="display:none;"><spring:message  code='label.addNewNotification'/></a>
            </div>
	
    
	
	 <div class="span9" id="rightpanel">
	 			
            	<c:choose>
					<c:when test="${submit != '' || resultSize >= 1}">
                        <c:choose>
                            <c:when test="${fn:length(notificationlist) > 0}">
                                <table class="table table-striped">
                                    <thead>
                                        <tr class="header">
                                             <th style="width: 350px;"><dl:sort customFunctionName="traverse" title="Notification Name" sortBy="notificationName" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th><dl:sort customFunctionName="traverse" title="Language" sortBy="language" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th><dl:sort customFunctionName="traverse" title="Type" sortBy="type" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>   
                                             <th><dl:sort customFunctionName="traverse" title="Last Edited" sortBy="updated" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col" class="header" style="width: 30px; padding: 0;"></th>
                                             
                                        </tr>
                                    </thead>
                                    <c:forEach items="${notificationlist}" var="noticetype">
                                            <tr>
                                                <td><a href="<c:url value="/admin/noticetype/${noticetype.id}"/>">${noticetype.notificationName} </a></td>
										<td>${noticetype.language.language}</td>
										<td>${noticetype.type.code}</td>
										<td><fmt:formatDate value="${noticetype.updated}"
												pattern="MM/dd/yyyy" /></td>
										<td><div class="dropdown">
											 <a class="dropdown-toggle" data-toggle="dropdown" href="#" alt="dropdown"><i class="icon-cog"></i><i class="caret"></i><span class="hide">Action</span></a>
											 <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
											  <li><a href="<c:url value="/admin/noticetype/${noticetype.id}"/>" class="offset1"><i class="icon-eye-open"></i> View Details</a></li>
									          <li>
									          
												<c:if test="${noticetype.templateEditable=='1'}">
													 <a href="<c:url value="/admin/editTemplate/${noticetype.id}"/>" class="offset1"><i class="icon-pencil"></i> Edit Template</a>
												</c:if>
												<c:if test="${noticetype.templateEditable=='0'}">
													<a href="#" class="offset1"><i class="icon-pencil"></i> Edit Template</a>
												</c:if>
									         
									          </li> 		
									         </ul>
											</div>
										</td>
									</tr>
                                   </c:forEach>
                                </table>
                                <div class="center">
                                    <dl:paginate customFunctionName="traverse" resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
                                </div>
                             </c:when>
                            <c:otherwise>
                               <h4 class="alert alert-info margin0"><spring:message  code='label.norecords'/></h4>
                            </c:otherwise>
                        </c:choose>	
		 			</c:when>
				</c:choose>
		</div><!--span9 ends-->
	
	</div>
	
	
	</div>
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">
				<p>This information is required to determine initial eligibility to
					use the SHOP Exchange. All fields are subjected to a basic format
					validation of the input (e.g. a zip code must be a 5 digit number).
					The question mark icon which opens a light-box provides helpful
					information. The EIN number can be validated with an external data
					source if an adequate web API is available.</p>
			</div>
		</div>
	</div>

<script type="text/javascript">
function update_hideTerminated(elem){
	if(document.getElementById(elem).checked){
		$('#hideTerminatedValue').val('YES');
	}else{
		$('#hideTerminatedValue').val('NO');
	}
}

function traverse(url){
	var queryString = {};
	url.replace(
		    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
		    function($0, $1, $2, $3) { queryString[$1] = $3; }
		);
	 if( queryString["sortBy"] ) $("#sortBy").val(queryString["sortBy"]);
	 if( queryString["sortOrder"] ) $("#sortOrder").val(queryString["sortOrder"]);
	 if( queryString["pageNumber"] ) $("#pageNumber").val(queryString["pageNumber"]);
	 if( queryString["changeOrder"] ) $("#changeOrder").val(queryString["changeOrder"]);
	
	$("#frmnoticelist").submit();
}
</script>

<!-- </div> -->
<%-- <%@ include file="/WEB-INF/views/layouts/untilefooter.jsp"%> --%>
