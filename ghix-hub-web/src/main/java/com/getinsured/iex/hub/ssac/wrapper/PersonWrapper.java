package com.getinsured.iex.hub.ssac.wrapper;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.GHIXApplicationContext;
//import gov.niem.niem.proxy.xsd._2.Date;
import com.getinsured.iex.hub.ssac.extension.ObjectFactory;
import com.getinsured.iex.hub.ssac.extension.PersonNameType;
import com.getinsured.iex.hub.ssac.extension.PersonSSNIdentificationType;
import com.getinsured.iex.hub.ssac.extension.PersonType;
import com.getinsured.iex.hub.ssac.extension.RestrictedGivenNameType;
import com.getinsured.iex.hub.ssac.extension.RestrictedMiddleNameType;
import com.getinsured.iex.hub.ssac.extension.RestrictedSurNameType;
import com.getinsured.iex.hub.ssac.extension.SSACompositeIndividualRequestType;
import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.DateType;
import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Date;
import com.google.gson.Gson;



public final class PersonWrapper implements JSONAware {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestQuartersOfCoverageVerificationIndicatorWrapper.class);
	
	private ObjectFactory factory;
	private PersonType personType;
	private PersonNameType personNameType = null;
	private String surName;
	private String givenName;
	private String middleName;
	private String ssn;
	private String birthDate;
	private PersonSSNIdentificationType personSSNIdentificationType = null;
	
	private static Gson gson ;
	
	static{
		gson = (Gson) GHIXApplicationContext.getBean("iex_platformGson");
	}

	public PersonWrapper() {
		this.factory = new ObjectFactory();
		this.personType = factory.createPersonType();
	}

	public PersonType getPersonType() {
		return this.personType;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		if(givenName != null && givenName.length() > 0){
		if(personNameType == null) {
			personNameType = factory.createPersonNameType();
			personType.setPersonName(personNameType);
		}
		RestrictedGivenNameType restrictedGivenName = factory.createRestrictedGivenNameType();
		restrictedGivenName.setValue(givenName);
		personNameType.setPersonGivenName(restrictedGivenName);
		this.givenName = givenName;
		}
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		if(middleName != null && middleName.length() > 0){
		if(personNameType == null) {
			personNameType = factory.createPersonNameType();
			personType.setPersonName(personNameType);
		}
		RestrictedMiddleNameType restrictedMiddleName = factory.createRestrictedMiddleNameType();
		restrictedMiddleName.setValue(middleName);
		personNameType.setPersonMiddleName(restrictedMiddleName);
		this.middleName = middleName;
		}
	}

	public void setSurName(String surName) {
		if(surName != null && surName.length() > 0){
		if(personNameType == null) {
			personNameType = factory.createPersonNameType();
			personType.setPersonName(personNameType);
		}
		RestrictedSurNameType restrcitedSurNameType = factory.createRestrictedSurNameType();
		restrcitedSurNameType.setValue(surName);
		personNameType.setPersonSurName(restrcitedSurNameType);
		this.surName = surName;
		}
	}

	public String getSurName() {
		return surName;
	}

	public void setSsn(String ssn) {
		if(ssn != null && ssn.length() > 0){
		if(personSSNIdentificationType == null) {
			personSSNIdentificationType = factory.createPersonSSNIdentificationType();
			personType.setPersonSSNIdentification(personSSNIdentificationType);
		}
		personSSNIdentificationType.setValue(ssn);
		this.ssn = ssn;
		}
	}

	public String getSsn() {
		return ssn;
	}

	public void setBirthDate (String birthDate) throws HubServiceException {
		
		try{
			if(birthDate != null && birthDate.length() > 0){
				this.birthDate = birthDate;
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		        java.util.Date date = simpleDateFormat.parse(birthDate);
		        GregorianCalendar gregorianCalendar = (GregorianCalendar)GregorianCalendar.getInstance();
		        gregorianCalendar.setTime(date);
		        XMLGregorianCalendar xMLGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		        xMLGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
		        com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.ObjectFactory xsd = new com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.ObjectFactory();
				com.getinsured.iex.hub.ssac.niem.niem_core._2_0.ObjectFactory niemCoreFac = new com.getinsured.iex.hub.ssac.niem.niem_core._2_0.ObjectFactory();
				Date newDate = xsd.createDate();
				newDate.setValue(xMLGregorianCalendar);		
				DateType d = new DateType();
				d = niemCoreFac.createDateType();
				JAXBElement<Date> dRep = niemCoreFac.createDate(newDate);
				d.setDateRepresentation(dRep);
				personType.setPersonBirthDate(d);
			}
		}
		catch(Exception e){
			LOGGER.error("Failed to set date of birth",e);
			throw new HubServiceException(e);
		}
	}

	public String getBirthDate() {
		return birthDate;
	}

	public static PersonWrapper fromJsonString(String jsonString) throws IOException{
		return gson.fromJson(jsonString, PersonWrapper.class);
	}

	
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		return obj.toJSONString();
	}

	public static void main(String[] args) {
		try{	
			PersonWrapper person = fromJsonString("{\"surName\": \"test123\", \"ssn\": \"123456789\", \"birthDate\": \"2009-08-08\"}");
			SSACompositeIndividualRequestType req = person.factory.createSSACompositeIndividualRequestType();
			req.setPerson(person.personType);
			//JAXBElement<SSACompositeIndividualRequestType> ssaCompReq = person.factory.createSSACompositeIndividualRequest(req);
			JAXBContext jc = JAXBContext.newInstance("gov.hhs.cms.dsh.sim.ee.ssac.extn");
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			//m.marshal(ssaCompReq, System.out);
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
		}
	}
	

}
