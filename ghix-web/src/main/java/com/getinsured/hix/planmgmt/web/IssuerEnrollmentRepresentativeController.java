/**
 * <p>
 * This controller for role ISSUER ENROLLMENT REPRESENTATIVE. 
 * ISSUER ENROLLMENT REPRESENTATIVE can view enrollment and enrollee data of respective issuer  
 * Ref jira: HIX-92916
 * </p>
 * 
 * @author santanu
 * @since 20-Oct-2016
 */

package com.getinsured.hix.planmgmt.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPremiumDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberRequestDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.planmgmt.service.IssuerEnrollmentRepresentativeService;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.util.PlanMgmtConstants;

@Controller
@DependsOn("dynamicPropertiesUtil")
@RequestMapping(value = "/planmgmt/issuer/enrollmentrep")
public class IssuerEnrollmentRepresentativeController {

	@Autowired 
	private IssuerService issuerService;
	@Autowired 
	private IssuerEnrollmentRepresentativeService enrollmentRepService;
	@Autowired 
	private UserService userService;
	@Autowired 
	private LookupService lookupService;
	@Autowired 
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerEnrollmentRepresentativeController.class);
	
	/*  ##### controller for landing page ##### */
	@RequestMapping(value = "/landingpage", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.ISSUER_ENROLLMENT_REP_VIEW_ENROLLMENTS)
	@GiAudit(transactionName = "IssuerEnrollmentRep", eventType = EventTypeEnum.PLAN_MANAGEMENT, eventName = EventNameEnum.PII_READ)
	public String launchLandingScreen(Model model) {
		
		// redirect suspended issuer enrollment representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_ENROLLMENT_REP_REDIRECT_URL;
		}
			
		Issuer issuerObj = null;
		
		try {		
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Lands on issuer enrollment representatives landing page ");
			}
			AccountUser user = userService.getLoggedInUser();
			
			if (user != null) {
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					LOGGER.info("Record Type : "+user.getRecordType()+ " : Record Id : "+user.getRecordId());
					if (!StringUtils.isBlank(user.getRecordId())) {
						IssuerRepresentative issuerRepresentative = issuerService.findIssuerRepresentativeById(Integer.valueOf(user.getRecordId()));
						LOGGER.info("Existing Issuer Representative User Record : "+issuerRepresentative.getUserRecord());
						if (issuerRepresentative.getUserRecord() == null) {
							LOGGER.debug("Updating Issuer Representative data with User Id ::"
									+  SecurityUtil.sanitizeForLogging(String.valueOf(user.getId())));
							issuerRepresentative.setUserRecord(user);
							issuerRepresentative.setUpdatedBy(user);
							issuerService.saveRepresentative(issuerRepresentative);
							ModuleUser issuerRepUser = userService.findModuleUser(issuerRepresentative.getIssuer().getId(), ModuleUserService.ISSUER_MODULE, user);
							if (issuerRepUser == null) {
								userService.createModuleUser(issuerRepresentative.getIssuer().getId(), ModuleUserService.ISSUER_MODULE, user, true);
							}
						}
					}
				}
				
				issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			}
		} catch (Exception e) {
			LOGGER.error("error occurs inside launchLandingScreen() ", e);
		}
		
		return "planmgmt/issuer/enrollmentrep/landingpage";
	}
	
	
	
	/*  ##### controller for  view enrollments ##### */
	@RequestMapping(value = "/enrollments")
	@GiAudit(transactionName = "Enrollments for Issuer Enrollment Rep", eventType = EventTypeEnum.PM_ISSUER_REP_ENROLLMENTS, eventName = EventNameEnum.PII_READ)
	@PreAuthorize(PlanMgmtConstants.ISSUER_ENROLLMENT_REP_VIEW_ENROLLMENTS)
	public String getEnrollments(Model model, HttpServletRequest request,  HttpServletResponse response) {
		
		// redirect suspended issuer enrollment representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_ENROLLMENT_REP_REDIRECT_URL;
		}
		
		try
		{
			AccountUser user = userService.getLoggedInUser();
			
			if(null != user && user.getId() > 0)
			{
				Issuer issuerObj = null;
				issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				if(null != issuerObj)
				{
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("Logged in userId : " + user.getId() + ", Issuer Id : " + issuerService.getIssuerId());
					}
					int currentPage = 1;
					Long iResultCt = (long) 0;
					List<EnrollmentSubscriberDTO> applicationlist = null;
								
					EnrolleeResponse enrolleeResponse = null;
					EnrollmentSubscriberRequestDTO enrollmentSubscriberRequest = new EnrollmentSubscriberRequestDTO();			
					// prepare search criteria
					enrollmentSubscriberRequest = enrollmentRepService.createEnrollmentSubscriberRequest(request, issuerObj.getId(), true);
					logSearchCriteria(enrollmentSubscriberRequest);
					
					// call search enrollee API
					enrolleeResponse = enrollmentRepService.callSearchEnrolleeAPI(enrollmentSubscriberRequest, user);
							
					if (null != enrolleeResponse && enrolleeResponse.getEnrollmentSubscriberDTOList() !=  null &&
							!enrolleeResponse.getEnrollmentSubscriberDTOList().isEmpty()) { // if response is not empty
						model.addAttribute("totalrecords", enrolleeResponse.getTotalRecordCount());
							if(LOGGER.isInfoEnabled()){
							LOGGER.info("Total # of enrollments " + enrolleeResponse.getTotalRecordCount());
							}
							// get list of enrollees
						applicationlist = enrolleeResponse.getEnrollmentSubscriberDTOList();
						iResultCt = enrolleeResponse.getTotalRecordCount();
			
							String param = request.getParameter(PlanMgmtConstants.PAGE_NUMBER);
							if (param != null) {
								currentPage = Integer.parseInt(param);
							}
						}	
					
					model.addAttribute(PlanMgmtConstants.APPLICATIONLIST, applicationlist);
					model.addAttribute("enrollmentSubscriberRequest", enrollmentSubscriberRequest);
					model.addAttribute("resultSize", (iResultCt != null) ? iResultCt.intValue()	: PlanMgmtConstants.ZERO);
					model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
					model.addAttribute(PlanMgmtConstants.CURRENT_PAGE, currentPage);
					model.addAttribute("yearList", enrollmentRepService.getYearsList());
					model.addAttribute("enrollmentStatusList", lookupService.getLookupValueList("ENROLLMENT_STATUS"));
					model.addAttribute("insuranceTypeList", lookupService.getLookupValueList("INSURANCE_TYPE"));	

				}
				else
				{
					LOGGER.error("Error occurs inside getEnrollments() : issuerObj is null ");
				}
			}
			
		}
		catch (Exception e) 
		{
			LOGGER.error("error occurs inside getEnrollments() ", e);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error occured in Enrollment Search page of Issuer Enrollment Rep. "));
			

		}
		
		return "planmgmt/issuer/enrollmentrep/enrollments";

	}
	
	
		
	/*  #####  controller for download enrollee data  ##### */
	@RequestMapping(value = "/exportreport")
	@PreAuthorize(PlanMgmtConstants.ISSUER_ENROLLMENT_REP_VIEW_ENROLLMENTS)
	public void doExportReport(Model model, HttpServletRequest request,  HttpServletResponse response) {
		
		if(!issuerService.isIssuerRepSuspended()){
			try
			{
				AccountUser user = userService.getLoggedInUser();
				
				if(null != user && user.getId() > 0)
				{
					Issuer issuerObj = null;
					issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
					if(null != issuerObj)
					{
						
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug("Logged in userId : " + user.getId() + ", Issuer Id : " + issuerService.getIssuerId());
						}
						
						List<EnrollmentSubscriberDTO> enrollmentSubscriberDTOList = null;
						EnrolleeResponse enrolleeResponse = null;
						
						// prepare search criteria
						EnrollmentSubscriberRequestDTO enrollmentSubscriberRequestDTO = enrollmentRepService.createEnrollmentSubscriberRequest(request, issuerObj.getId(), false);
						
						// call search enrollee API
						enrolleeResponse = enrollmentRepService.callSearchEnrolleeAPI(enrollmentSubscriberRequestDTO, user);
				
							if (null != enrolleeResponse && enrolleeResponse.getEnrollmentSubscriberDTOList() != null &&
									!enrolleeResponse.getEnrollmentSubscriberDTOList().isEmpty()){
								enrollmentSubscriberDTOList = enrolleeResponse.getEnrollmentSubscriberDTOList();
									if(LOGGER.isInfoEnabled()){
										LOGGER.info("Total # of enrollments ready for export " +enrolleeResponse.getTotalRecordCount());
									}
									enrollmentRepService.downloadEnrolleeData(response, enrollmentSubscriberDTOList, enrollmentSubscriberRequestDTO.getCoverageYear());
								}	
							enrolleeResponse= null;
							enrollmentSubscriberDTOList = null;
						}
					}
					else
					{
						LOGGER.error("Error occurs inside doExportReport() : issuerObj is null ");
					}
			}
			catch (Exception e) 
			{
				LOGGER.error("error occurs inside doExportReport() ", e);
			}
		}	
	}
	
	
	
	/*  #####  controller for view enrollment details ##### */
	@RequestMapping(value = "/enrollmentdetails/{encId}")
	@GiAudit(transactionName = "Enrollment Details View for Issuer Enrollment Rep", eventType = EventTypeEnum.PM_ISSUER_REP_ENROLLMENT_DETAILS, eventName = EventNameEnum.PII_READ)
	@PreAuthorize(PlanMgmtConstants.ISSUER_ENROLLMENT_REP_VIEW_ENROLLMENTS)
	public String getEnrollmentDetails(Model model, HttpServletRequest request, @PathVariable("encId") String encEnrollmentId) {
		
		// redirect suspended issuer enrollment representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_ENROLLMENT_REP_REDIRECT_URL;
		}
		
		try
		{
			AccountUser user = userService.getLoggedInUser();
			
			Integer enrollmentId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encEnrollmentId));
			
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Requested enrollment id : " + enrollmentId);
			}
			
			if(null != user && user.getId() > 0)
			{
				Issuer issuerObj = null;
				issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				if(null != issuerObj)
				{
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("Logged in userId : " + user.getId() + ", Issuer Id : " + issuerService.getIssuerId());
					}
					EnrollmentDataDTO enrollmentDataDTO  = new EnrollmentDataDTO ();
					enrollmentDataDTO = enrollmentRepService.callEnrollmentDetailsAPI(enrollmentId, user.getUsername());
					
					// sort premium list by month
					if(null != enrollmentDataDTO.getEnrollmentPremiumDtoList()){
						Collections.sort(enrollmentDataDTO.getEnrollmentPremiumDtoList(), new SortMonth());
					}
					
					model.addAttribute("enrollmentDataDTO", enrollmentDataDTO);

          String isStateSubsidyEnabled = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_SUBSIDY);

          if("Y".equalsIgnoreCase(isStateSubsidyEnabled)) {
            model.addAttribute("isStateSubsidyEnabled", true);
          }

					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS) , new GiAuditParameter("Enrollment ID", enrollmentId));
							
				}
				else
				{
					LOGGER.error("Error occurs inside getEnrollmentDetails() : issuerObj is null ");
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while fetching Enrollment details. "));
				}
			}
		} 
		catch (Exception e) 
		{
			LOGGER.error("Error while fetching Enrollment details. ", e);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while fetching Enrollment details. "));
			
		}
		
		return "planmgmt/issuer/enrollmentrep/enrollmentdetails";
	}
	
	
	
	/*  #####  controller for suspend screen  ##### */
	@RequestMapping(value = "/suspend", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.ISSUER_ENROLLMENT_REP_VIEW_ENROLLMENTS)
	public String showSuspendScreen() {
		
		return "planmgmt/issuer/showsuspendscreen";
	}	
			
	private void logSearchCriteria(EnrollmentSubscriberRequestDTO enrollmentSubscriberRequest){
		ArrayList<GiAuditParameter> params = new ArrayList<GiAuditParameter>();
		if (enrollmentSubscriberRequest.getCoverageYear() != null){
		    GiAuditParameter param1 = new GiAuditParameter("Year", enrollmentSubscriberRequest.getCoverageYear());
		    params.add(param1);
		}
		
		if (enrollmentSubscriberRequest.getSubscriberName() != null){
		    GiAuditParameter param2 = new GiAuditParameter("Subscriber Name", "");
		    params.add(param2);
		}
		
		if (enrollmentSubscriberRequest.getEnrollmentID() != null){
		    GiAuditParameter param3 = new GiAuditParameter("Policy ID", "");
		    params.add(param3);
		}
		
		if (enrollmentSubscriberRequest.getCMSPlanID() != null){
		    GiAuditParameter param4 = new GiAuditParameter("Plan Number", "");
		    params.add(param4);
		}
		
		if (enrollmentSubscriberRequest.getEnrollmentStatus() != null){
		    GiAuditParameter param5 = new GiAuditParameter("Status", "");
		    params.add(param5);
		}
		
		if (enrollmentSubscriberRequest.getExchgSubscriberIdentifier() != null){
		    GiAuditParameter param6 = new GiAuditParameter("Subscriber ID", "");
		    params.add(param6);
		}
		
		if (enrollmentSubscriberRequest.getSSNLast4Digit() != null){
		    GiAuditParameter param7 = new GiAuditParameter("Last 4 digits of SSN", "");
		    params.add(param7);
		}
		
		
		if (enrollmentSubscriberRequest.getSubscriberDateOfBirth() != null){
		    GiAuditParameter param7 = new GiAuditParameter("DOB of Subscriber", "");
		    params.add(param7);
		}
		
		GiAuditParameterUtil.add(params);
		return;
	}
			
	
}



class SortMonth implements Comparator<EnrollmentPremiumDTO> {
	public int compare(EnrollmentPremiumDTO compareTo, EnrollmentPremiumDTO compareWith) {
		return (compareTo.getMonth() < compareWith.getMonth()) ? -1 :
			(compareTo.getMonth() > compareWith.getMonth()) ? 1 : 0;
	}

		
}
