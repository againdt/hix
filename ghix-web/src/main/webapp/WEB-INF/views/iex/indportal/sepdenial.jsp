<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/ng-template" id="sepdenial">

<div class="header row-fluid ng-scope" ng-init="calculateSEPEnd()">
  <h4>SEP was denied on SOME DAY</h4>
</div>

<div class="well ng-scope">
	<div class="well-step">
		<div class="row-fluid">
		    <form name="denialForm" class="form-horizontal margin10">
		        <div class="row-fluid">
		          <div>
		              <div class="txt-center span2"><u>Applicant</u></div>
		              <div class="txt-center span3"><u>Event Category</u></div>
		              <div class="txt-center span5"><u>Event Name</u></div>
		              <div class="txt-center span2"><u>Event Date</u></div>
		          </div>
		          
		          <div class="margin5" ng-repeat="sepEvent in eventDetails">
		              <div scope="row" class="span2 margin5-b">{{sepEvent.applicant}}</div>
		              <div scope="row" class="span3 margin5-b">{{sepEvent.eventType}}</div>   
		              <div scope="row" class="span5 margin5-b">{{sepEvent.eventName}}</div>    
		              <div scope="row" class="span2 margin5-b">
		                <label for="eventDate{{$index+1}}" class="aria-hidden"></label>
		                <input type="text" class="dateinput narrow-date" placeholder="mm/dd/yyyy" ui-mask="99/99/9999" ng-model="sepEvent.eventDate" ng-blur="validateEDate(sepEvent.eventDate, $index)" ng-class="{'seventy-day' : sepEvent.over70Days }" num-only required>
		              </div>    
		              <div scope="row" class="span12 margin5-b" ng-if="sepEvent.dateError">   
		                <span class="popover-content ng-binding zip-warning redBorder pull-right" ng-if="sepEvent.dateError">{{sepEvent.dateErrorMsg}} </span>     
		              </div>
		          </div>
		        </div>                            

				<p class="alert margin10">Note: Event date(s) that were older tahn 70 days that caused SEP Denial are shown in red. </p>
				<div class="margin10">
					<div class="margin20-b margin10-t">
						<span class="span3 margin5-t">Coverage Start Date</span> 
						<label for="coverage start date" class="aria-hidden"></label>
							<input type="text" class="dateinput" placeholder="mm/dd/yyyy" ui-mask="99/99/9999" ng-model="sepStartDate" ng-blur="validateSepDate(sepStartDate)"  num-only required></input>
							<span ng-if="SepDateErrorMsg" class="popover-content ng-binding zip-warning redBorder">{{SepDateErrorMsg}} </span>
					</div>
					<div class="margin20-b">	
						<span class="span3 margin5-t">SEP End Date</span>
						<label for="SEP end date" class="aria-hidden"></label>
							<input type="text" class="dateinput" placeholder="mm/dd/yyyy" ui-mask="99/99/9999" ng-model="sepEndDate" ng-blur="dateCheck($event, 'termDateErr')"  num-only required></input>
						
					</div>
				
				</div>
				<p class="alert alert-danger margin5">Note: Please review the above information carefully. Once you click CONFIRM, the consumer will be notified that a Special Enrollment Period has been granted and they should log in to review their options.</p>
				<div class="form-actions form-horizontal centered-btn-grp">
      				<button class="btn btn-secondary margin10-r" ng-disabled="denialForm.$invalid || eventDetailsErrorCount > 0" ><spring:message code="indportal.portalhome.confirm" javaScriptEscape="true"/></button>
      				<button class="btn btn-secondary"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></button>
    			</div>


		     
		
		    </form>
		</div>
	</div>
</div>

</script>
