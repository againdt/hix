<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<jsp:useBean id="now" class="java.util.Date" />

<style>
.agreement {
	background: #ebebeb
}

#eSignature {
	border-bottom: 1px solid #bbb;
}

.esign {
	background: #f2f2f2
}

.agree::-webkit-scrollbar {
	width: 10px;
	height: 10px;
}

.agree::-webkit-scrollbar-track {
	background: #fff;
}

.agree::-webkit-scrollbar-thumb {
	background: #bbb;
}
i.icon-print {
	color: inherit;
}
</style>
<script type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward();
    $("#submitButton").removeAttr("disabled");
    }
</script>
<body onload="noBack();"
    onpageshow="if (event.persisted) noBack();" onunload="" aria-hidden="true"></body>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript">

	// Variables for dataLayer
	var healthPlanIssuer = '${healthPlanIssuer}';
	var healthPlanName = '${healthPlanName}';
	var healthPlanLevel = '${healthPlanLevel}';

	var dentalPlanIssuer = '${dentalPlanIssuer}';
	var dentalPlanName = '${dentalPlanName}';
	var dentalPlanLevel = '${dentalPlanLevel}';

	var healthPlanInfo = healthPlanIssuer + ' | ' +  healthPlanName + ' | ' +  healthPlanLevel;
    var dentalPlanInfo = dentalPlanIssuer + ' | ' +  dentalPlanName + ' | ' +  dentalPlanLevel;

	
	if(healthPlanName != '' && dentalPlanName != '' ) {
		planInfo = healthPlanInfo + ' :: ' + dentalPlanInfo;
	}
	else if(healthPlanName != '' && dentalPlanName == '' ) {
		planInfo = healthPlanInfo;
	}
	else if(healthPlanName == '' && dentalPlanName != '' ) {
		planInfo = dentalPlanInfo;
	}

	function validateForm() {
		if ($("#frmesign").validate().form()) {
			if ($("input[name=terms]:checked").val() == undefined) {
				alert('<spring:message code='label.validateTerms' javaScriptEscape='true'/>');
				return false;
			} 
			else if ($("input[name=agreement]:checked").val() == undefined && $("input[name=agreement]").length > 0) {
				alert('<spring:message code='label.validateAgreement' javaScriptEscape='true'/>');
				return false;
			} 
			else {
				// Checkout Step 4
				window.dataLayer.push({
					'event': 'checkout',
					'eventCategory': 'Plan Selection - ecommerce',
					'eventAction': 'Enroll Button Click',
					'eventLabel': planInfo,
					'ecommerce': {
						'checkout': {
							'actionField': {'step': 4}
						}
					}
				});

				$("#frmesign").submit();
				$("#submitButton").attr("disabled", true);
			}
		}
	}

</script>

<div class="gutter10">

<div class="row-fluid">
	<div style="font-size: 14px; color: red">
		<c:if test="${errorMsg != ''}">
			<p>
				<c:out value="${errorMsg}"></c:out>
			<p />
		</c:if>
	</div>
</div>
<c:if test="${errorMsg == ''}">
    <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1" id="rightpanel">
	<div class="row-fluid" id="titlebar">
		<h1 class="custom-margin"><spring:message code="enroll.eSignature"/></h1>
	</div>
	
	<input type="hidden" name="healthPlanName" id="healthPlanName" value="${healthPlanName}" />
	<input type="hidden" name="dentalPlanName" id="dentalPlanName" value="${dentalPlanName}" />

	<div class="row-fluid">
		<form method="post" action="esignature" name="frmesign" id="frmesign">
		<df:csrfToken/>
			<input type="hidden" name="cart_id" id="cart_id" value="${cartId}" />
			<input type="hidden" name="aptc" id="aptc" value="${aptc}" />
			<input type="hidden" name="fininfoId" id="fininfoId" value="${fininfoId}" />
			<input type="hidden" name="employeeFullName" id="employeeFullName" value="${employeeFullName}" />
			<input type="hidden" name="enrollmentType" id="enrollmentType" value="${enrollment_type}" />

				<p>
					<c:choose>
						<c:when test="${enrollment_type == enrollment_type_individual}">
							<spring:message code="enroll.nm.esignature.individual.header_1" htmlEscape="false"/>
							<c:choose>
								<c:when test="${aptc!=''}">
									<spring:message code="enroll.nm.esignature.individual.header_2" htmlEscape="false"/>
								</c:when>
							</c:choose>
							<spring:message code="enroll.nm.esignature.individual.header_3" htmlEscape="false"/>
							<spring:message code="enroll.nm.esignature.individual.header_4" htmlEscape="false"/>
						</c:when>
						<c:otherwise>
							<spring:message code="enroll.nm.esignature.header"/>
						</c:otherwise>
					</c:choose>
				</p>
				<p><spring:message code="enroll.esignature.important"/></p>
				<div class="header margin10-b">
					<h4 class="pull-left"><b>I.&nbsp;</b><spring:message code="enroll.esignature.exchangeAgreement"/></h4>
					<a data-original-title="" href="#"class="btn btn-primary btn-small pull-right" onclick="printContent('user-agreement');"><i class="icon-print"></i>&nbsp;<spring:message code="enroll.print"/></a>
				</div>
				<div class="agreement">
				<label for="user-agreement" class="hide"><b>I.&nbsp;</b><spring:message code="enroll.esignature.exchangeAgreement"/></label>
					<div class="gutter10">
						<div class="control-group">
							<div class="controls">
								<span class="span12 agree ms-esignature-agree" tabindex="0" id="user-agreement" readonly="readonly">
									<c:choose>
										<c:when test="${enrollment_type == enrollment_type_individual}">
											<spring:message code="enroll.exchange.individual.agreement" arguments="${stateName}" htmlEscape="false"/>
										</c:when>
										<c:otherwise>
												<spring:message code="enroll.exchange.shop.agreement" arguments="${stateName}"/>
										</c:otherwise>
									</c:choose>
								</span>
							</div>
						</div>

						<div class="control-group pull-left">
							<div class="controls">
								<c:choose>
									<%-- Different Text For Individual --%>
									<c:when test="${enrollment_type == enrollment_type_individual}">
										<label class="checkbox" for="ind-terms"> <input type="checkbox"
											name="terms" id="ind-terms" value="1"> <spring:message code="enroll.esignature.individual.agreeTermsofService"/>
										</label>
									</c:when>
									<%--Different Text For Employees --%>
									<c:otherwise>
										<label class="checkbox" for="emp-terms"> <input type="checkbox"
											name="terms" id="emp-terms" value="1"> <spring:message code="enroll.esignature.employee.agreeTermsofService"/>
										</label>
									</c:otherwise>
								</c:choose>
								<%-- APTC Validation not applicable for Employees --%>
								<c:if test="${showFileTaxReturn==true && enrollment_type == enrollment_type_individual}">
									<label class="checkbox" for="agreement">
										<input type="checkbox" name="agreement" id="agreement" value="1"><spring:message arguments="${taxYearObj.yearOfEffectiveDate},${taxYearObj.taxDueDate},${taxYearObj.taxDueYear}" code="enroll.esignature.aptcAgreement" htmlEscape="false"/>
									</label>
								</c:if>
							</div>
						</div>


						<div class="row-fluid">
							<c:if test="${showpin==true}">
								<br />
								<div class="control-group pull-left">
									<label for="pin_esig"><spring:message code="enroll.esignature.PIN"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /><a href="#"
										rel="tooltip" data-placement="top"
										data-original-title="<spring:message code="enroll.esignature.pinTooltip"/>"
										class="info"><i class="icon-question-sign"></i></a></label>
									<div class="controls">
										<input type="text" class="input-small" id="pin_esig"
											name="pin_esig" maxlength="4" value="${pin_esig }" />
									</div>
									<div class="" id="pin_esig_error"></div>

								</div>
							</c:if>

							<!-- Tax Filers Signature -->
							<c:if test="${enrollment_type == enrollment_type_individual && aptc!=''}">
								<br/>
								<div class="control-group pull-left">
								<label><b>II.&nbsp;<spring:message code='enroll.nm.taxFilersSignature'/></b></label>
										<br/>
										<label class="capitalize-none"><spring:message arguments="${taxYearObj.yearOfEffectiveDate},${taxYearObj.taxDueDate},${taxYearObj.taxDueYear}" code="enroll.nm.esignature.taxFilerAgreement"/></label>
										<br/>
										<label class="checkbox" for="taxFiler_esign">
											<input type="checkbox" name="taxFiler_esign" id="taxFiler_esign" value="1"><spring:message arguments="${taxYearObj.yearOfEffectiveDate},${taxYearObj.taxDueDate},${taxYearObj.taxDueYear}" code="enroll.esignature.aptcAgreement" htmlEscape="false"/>
										</label>
										<div class="" id="taxFiler_esign_error"></div>
								</div>
							</c:if>
							<!--Alignment for e-signature and provide -->
							<div class="control-group clear">
							    <label><b><spring:message code='enroll.nm.applicationFilerSignature'/></b></label>
								<br/>
								<label for="applicant_esig" class="capitalize-none"><spring:message code="enroll.esignature.signature"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<input type="text" class="input-xxlarge" id="applicant_esig" maxlength="50" name="applicant_esig" value="${applicant_esig }" />
								</div>
								<div class="" id="applicant_esig_error"></div>

								<c:if test="${enrErrorMsg != null && enrErrorMsg != ''}">
									<div class="error" >
										<label class="error">
											<span>
												<em class="excl">!</em>
												<spring:message code="enroll.esignature.errorMessage1"/>
												<c:if test="${enrErrorMsg2 != ''}"><spring:message code="enroll.esignature.errorMessage2"/> <c:out value="${enrErrorMsg2}"></c:out>)</c:if>.
											</span>
										</label>
									</div>
								</c:if>
							</div>
						</div>
						<div><small><spring:message code="enroll.esignature.requiredFields" htmlEscape="false"/></small></div>
						<!-- gutter10 -->
					</div>
					<div class="control-group esign gutter10-b">
						<div class="row-fluid gutter10-tb">
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							<span class="col-xs-12 col-sm-4 col-md-4 col-lg-3"><spring:message code="enroll.esignature.provideSignature"/>:</span>
							<div id="eSignature" class="controls col-xs-12 col-sm-7 col-md-7 col-lg-8"></div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<span class="gutter20-l esign_date"><spring:message code="enroll.date"/>: &nbsp;<fmt:formatDate value="${currentDate}" type="both" pattern="MM/dd/yyyy" /> </span>

						</div>
						<!-- gutter10 -->
					</div>
                </div>
				</div>
				<div class="form-actions">
					<c:choose>
							<%-- Different Text For Individual --%>
							<c:when test="${enrollment_type == enrollment_type_individual}">
								<a href="<c:url value="/private/showCart"/>" class="btn btn_res"><spring:message
										code="enroll.back" />
								</a>
								<input type="button" name="submitButton" id="submitButton"
									onClick="javascript:validateForm();"
									class="btn btn-primary btn_res pull-right"
									value="<spring:message code="enroll.esignature.submitButtonValue"/>"
									title="<spring:message code="enroll.esignature.submitButtonValue"/>" />
							</c:when>
							<%--Different Text For Employees --%>
							<c:otherwise>
										<a href="<c:url value="/private/shopCart"/>" class="btn"><spring:message
										code="enroll.back" />
								</a>
								<input type="button" name="submitButton" id="submitButton"
									onClick="javascript:validateForm();"
									class="btn btn-primary pull-right"
									value="<spring:message code="enroll.esignature.submitButtonValue"/>"
									title="<spring:message code="enroll.esignature.submitButtonValue"/>" />
							</c:otherwise>
				  </c:choose>
				</div>
			</div>
			<!--rightpanel-->
		</form>
		<!-- End Form -->
	</div>
	<!--row-fluid-->
	</div>
</c:if>
<iframe name=print_frame width=400 height=400 frameborder=0 src=about:blank class="hide" aria-hidden="true"></iframe>
<script type="text/javascript">
	$(document).ready(function() {
	    document.title = 'E-Signature Page';

		$('#applicant_esig').bind('keyup focusout', function(event) {
			$('#eSignature').text($('#applicant_esig').val());
		});
		$('.info').tooltip();
		$('span.icon-ok').parents('li').css('list-style', 'none');

		// Data layer variable to be pushed at the end of shell.jsp
    	dl_pageCategory = 'Checkout';

		// Checkout Step 3
		$('#applicant_esig').bind('focusout', function(event) {
			window.dataLayer.push({
				'event': 'checkout',
				'eventCategory': 'Plan Selection - ecommerce',
				'eventAction': 'E-Signature',
				'eventLabel': planInfo,
				'ecommerce': {
					'checkout': {
						'actionField': {'step': 3}
					}
				}
			});
		});

	});
</script>

<script type="text/javascript">
	jQuery.validator.addMethod("alphabetsOnly", function(value, element) {
		return /^[a-zA-Z-.\s\']+$/i.test(value);
	});
	jQuery.validator.addMethod("fullNameOnly", function(value, element) {
	    if(/\w+\s+\w+/.test(value)) {
	       return true;
	    } else {
	       return false;
	    }
	});
	jQuery.validator.addMethod("employeeFullNameOnly", function(value, element) {
		var employeeFullName=$('#employeeFullName').val();
		var enrollmentType= $('#enrollmentType').val();
		if(enrollmentType=='24'){
		 if(value.toUpperCase()==employeeFullName.toUpperCase()){
	            return true;
	        }
	        else{
	            return false;
	        }
	    }
		return true;
	});
	var validator = $("#frmesign")
			.validate(
					{
						rules : {
							'applicant_esig' : {
								required : true,
								alphabetsOnly : true,
								fullNameOnly : true,
								employeeFullNameOnly : true
							},
							'pin_esig' : {
								required : true,
								number : true,
								maxlength : 4,
								minlength : 4
							},
							'taxFiler_esign' : {
								required : true
							},
						},
						messages : {
							'pin_esig' : {
								required : "<span><em class='excl'>!</em><spring:message code='label.validatePinEsig' javaScriptEscape='true'/></span>"
							},
							'applicant_esig' : {
								required : "<span><em class='excl'>!</em><spring:message code='label.validateApplicantEsig' javaScriptEscape='true'/></span>",
								alphabetsOnly : "<span><em class='excl'>!</em><spring:message code='label.validateSignature'/></span>",
								fullNameOnly : "<span><em class='excl'>!</em><spring:message code='label.validateApplicantEsig'/></span>",
								employeeFullNameOnly : "<span><em class='excl'>!</em><spring:message code='label.validateApplicantEsig'/></span>"
							},
							'taxFiler_esign' : {
								required : "<span><em class='excl'>!</em><spring:message code='label.validateTerms' javaScriptEscape='true'/></span>"
							}
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error")
									.attr('class', 'error');
						}
					});

function printContent(id){
	 var str ="<html>";
		 str=str+"<head></head>";
		 	str=str+"<body>";
		 		str=str+"<table align='center'>";
		 			str=str+"<tr><td align='center'>Exchange Agreement</td></tr>";
		 			str=str+"<tr><td>"+document.getElementById(id).innerHTML+"</td></tr>";
		 		str=str+"</table>";
		 	str=str+"</body>";
		 str=str+"</html>";

	 window.frames["print_frame"].document.body.innerHTML=str;
	 window.frames["print_frame"].window.focus();
	 window.frames["print_frame"].window.print();
	}
</script>
