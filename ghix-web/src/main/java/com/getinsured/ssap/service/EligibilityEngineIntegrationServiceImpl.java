package com.getinsured.ssap.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.hix.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.request.ssap.SsapApplicationRequest;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.CurrentOtherInsurance;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.IncarcerationStatus;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.SpecialCircumstances;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.enums.GenderEnum;
import com.getinsured.iex.ssap.enums.OtherStateOrFederalProgramType;
import com.getinsured.iex.ssap.model.AptcHistory;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.ssap.model.eligibility.EligibilityRequest;
import com.getinsured.ssap.model.eligibility.EligibilityResponse;
import com.getinsured.ssap.model.eligibility.HouseholdMemberRequest;
import com.getinsured.ssap.model.eligibility.HouseholdMemberResponse;
import com.getinsured.ssap.model.eligibility.ResponseStatus;
import com.getinsured.ssap.model.eligibility.RunMode;
import com.getinsured.ssap.repository.EligibilityProgramRepository;
import com.getinsured.ssap.util.BloodRelationshipCode;
import com.getinsured.ssap.util.EligibilityConstants;
import com.getinsured.ssap.util.HouseholdUtil;
import com.getinsured.ssap.util.JsonUtil;
import com.getinsured.ssap.util.SsapHelper;
import com.getinsured.timeshift.TSDateTime;

/**
 * Implementation of {@link EligibilityEngineIntegrationService} interface.
 *
 * @author Yevgen Golubenko
 * @since 4/4/19
 */
@Service("eligibilityEngineIntegrationService")
public class EligibilityEngineIntegrationServiceImpl implements EligibilityEngineIntegrationService
{
  private static final Logger log = LoggerFactory.getLogger(EligibilityEngineIntegrationServiceImpl.class);

  private static final String DEFAULT_NA_CSR_LEVEL = "CS3";
  private static final String DB_TRUE = "Y";
  private static final String DB_FALSE = "N";

  @Value("#{configProp['nativeAmerican.default.cslevel']}")
  private String nativeAmericanCsrLevel;

  public static final String API_DATE_FORMAT = "MM/dd/yyyy";
  private static String ELIGIBILITY_SERVICE_URL;

  private final Properties configProp;
  private final GhixRestTemplate ghixRestTemplate;
  private final SsapApplicantRepository ssapApplicantRepository;
  private final EligibilityProgramRepository eligibilityProgramRepository;
  private final SsapApplicationRepository ssapApplicationRepository;
  private final ExchangeEligibilityDeterminationService exchangeEligibilityDeterminationService;
  private final HubIntegration hubIntegration;
  private final EventCreationService eventCreationService;
  private final SsapNoticeService ssapNoticeService;
  private final UserService userService;
  private final AptcHistoryRepository aptcHistoryRepository;

  private ObjectMapper om = new ObjectMapper();

  @Autowired
  public EligibilityEngineIntegrationServiceImpl(final GhixRestTemplate ghixRestTemplate,
                                                 final Properties configProp,
                                                 final SsapApplicantRepository ssapApplicantRepository,
                                                 final EligibilityProgramRepository eligibilityProgramRepository,
                                                 final SsapApplicationRepository ssapApplicationRepository,
                                                 final ExchangeEligibilityDeterminationService exchangeEligibilityDeterminationService,
                                                 final HubIntegration hubIntegration,
                                                 final EventCreationService eventCreationService,
                                                 final SsapNoticeService ssapNoticeService,
                                                 final UserService userService,
                                                 final AptcHistoryRepository aptcHistoryRepository)
  {
    this.ghixRestTemplate = ghixRestTemplate;
    this.configProp = configProp;
    this.ssapApplicantRepository = ssapApplicantRepository;
    this.eligibilityProgramRepository = eligibilityProgramRepository;
    this.ssapApplicationRepository = ssapApplicationRepository;
    this.exchangeEligibilityDeterminationService = exchangeEligibilityDeterminationService;
    this.hubIntegration = hubIntegration;
    this.eventCreationService = eventCreationService;
    this.ssapNoticeService = ssapNoticeService;
    this.userService = userService;
    this.aptcHistoryRepository = aptcHistoryRepository;
  }

  @PostConstruct
  public void postConstruct()
  {
    ELIGIBILITY_SERVICE_URL = this.configProp.getProperty("endPointEligibilityDetermination");

    if(ELIGIBILITY_SERVICE_URL == null) {
      log.error("Cannot initialize EligibilityEngineIntegrationServiceImpl, 'endPointEligibilityDetermination' not found in configuration.properties! Defaulting to localhost.");
      ELIGIBILITY_SERVICE_URL = "http://localhost:8081/ghix-eligibility-engine/";
    }

    ELIGIBILITY_SERVICE_URL += "eligibilityEngine/eligibilityDetermination";

    log.info("Initialized EligibilityEngineIntegrationServiceImpl with integration URL: {}", ELIGIBILITY_SERVICE_URL);
    om.enable(SerializationFeature.INDENT_OUTPUT);

    if(nativeAmericanCsrLevel == null || "".equals(nativeAmericanCsrLevel)) {
      nativeAmericanCsrLevel = DEFAULT_NA_CSR_LEVEL;
    }
  }

  /**
   * Only when calling pre-eligibility, we need to reset seeksQhp to {@code true} for each HH member.
   * @param application {@link SingleStreamlinedApplication} object.
   */
  void resetSeeksQhpForPreEligibilityCall(final SingleStreamlinedApplication application) {
    if(application != null && application.getTaxHousehold() != null && application.getTaxHousehold().get(0) != null &&
      application.getTaxHousehold().get(0).getHouseholdMember() != null) {
      application.getTaxHousehold().get(0).getHouseholdMember().forEach(hhm -> hhm.setSeeksQhp(true));
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public EligibilityResponse callEligibilityEngine(final SingleStreamlinedApplication application, final RunMode runMode) throws Exception {

    if(runMode == RunMode.PRE_ELIGIBILITY) {
      resetSeeksQhpForPreEligibilityCall(application);
    }

    EligibilityRequest eligibilityRequest = getEligibilityRequestFromApplication(application, runMode);
    eligibilityRequest.setRunMode(runMode);

    try
    {
      log.info("Invoking eligibility engine: {} with payload: {}", ELIGIBILITY_SERVICE_URL, om.writeValueAsString(eligibilityRequest));
    }
    catch (JsonProcessingException e)
    {
      log.info("Could not write log", e);
    }

    EligibilityResponse eligibilityResponse = ghixRestTemplate.postForObject(ELIGIBILITY_SERVICE_URL, eligibilityRequest, EligibilityResponse.class);

    if(eligibilityResponse != null) {
      eligibilityResponse.setRequestId(eligibilityRequest.getRequestId());
    }

    return eligibilityResponse;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public EligibilityResponse invokeEligibilityEngine(final SingleStreamlinedApplication application,
                                                     final String specialEnrollmentEvent,
                                                     final String specialEnrollmentDate,
                                                     final RunMode runMode)
  {
    EligibilityResponse eligibilityResponse = null;

    try {
      SsapApplication ssapApplication = ssapApplicationRepository.getOne(Long.parseLong(application.getSsapApplicationId()));

      /*ssapApplication.setApplicationStatus(ApplicationStatus.SUBMITTED.getApplicationStatusCode());
      log.info("Updating Application Status to {} for SSAP id: {}", ApplicationStatus.SUBMITTED, ssapApplication.getId());
      ssapApplication = ssapApplicationRepository.save(ssapApplication);*/
      AccountUser currentUser = null;
      try {
        currentUser = userService.getLoggedInUser();
      }catch (Exception e) {
        log.error("Exception occurred while feching current loggedin user: {}", e.getMessage());
      }

      log.debug("CurrentUser fetched : {}",currentUser!=null ? currentUser.getId() : null);

      SsapApplicationRequest updateStatusRequest = new SsapApplicationRequest();
      updateStatusRequest.setUserId(null!=currentUser? new BigDecimal(currentUser.getId()): null);
      updateStatusRequest.setCaseNumber(ssapApplication.getCaseNumber());
      updateStatusRequest.setApplicationStatus(ApplicationStatus.SUBMITTED.getApplicationStatusCode());

      log.info("Updating Application Status to {} for SSAP id: {}", ApplicationStatus.SUBMITTED, ssapApplication.getId());
      ghixRestTemplate.postForObject(GhixEndPoints.EligibilityEndPoints.SSAP_APP_STATUS_UPDATE, updateStatusRequest, Void.class);

      eligibilityResponse = callEligibilityEngine(application, runMode);

      if(eligibilityResponse != null && eligibilityResponse.getErrors().isEmpty()) {
        /*ssapApplication.setApplicationStatus(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
        log.info("Updating Application Status to {} for SSAP id: {}", ApplicationStatus.ELIGIBILITY_RECEIVED, ssapApplication.getId());
        ssapApplication = ssapApplicationRepository.save(ssapApplication);*/
        log.info("Updating Application Status to {} for SSAP id: {}", ApplicationStatus.ELIGIBILITY_RECEIVED, ssapApplication.getId());
        updateStatusRequest.setApplicationStatus(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
        ghixRestTemplate.postForObject(GhixEndPoints.EligibilityEndPoints.SSAP_APP_STATUS_UPDATE, updateStatusRequest, Void.class);


        processResponse(eligibilityResponse, application, runMode);

        eventCreationService.startEventCreation(ssapApplication.getId(), specialEnrollmentEvent, specialEnrollmentDate, application.getApplicationGuid(), ssapApplication.getApplicationType());
        eventCreationService.logIndividualAppEvent("APPLICATION_SUBMITTED", ssapApplication.getId(), application.getApplyingForFinancialAssistanceIndicator());

        if (runMode != RunMode.BATCH) {
          hubIntegration.process(ssapApplication.getId()); // TODO: CHECK Results of ELIGIBILITY flags for app and user.
        }

        ssapNoticeService.sendEligibilityNotification(ssapApplication.getId(), application.getApplicationGuid());
      } else {
        log.error("Eligibility Response was null or had errors: {}", eligibilityResponse);
      }
    }
    catch(Exception e) {
      log.error("Exception occurred during eligibility engine call: {}", e.getMessage());
      log.error("Exception details for eligibility engine call", e);
      eligibilityResponse = new EligibilityResponse();
      eligibilityResponse.setStatus(ResponseStatus.failure);
      Map<String, String> error = new HashMap<>();
      error.put("exception", e.getMessage());
      eligibilityResponse.setErrors(error);
    }

    try
    {
      log.info("EligibilityResponse: {}", om.writeValueAsString(eligibilityResponse));
    }
    catch (JsonProcessingException e)
    {
      log.error("Unable to write JSON", e);
    }

    return eligibilityResponse;
  }

  /**
   * Processes {@link EligibilityResponse} object. Based on the
   * decision of eligibility engine, we store appropriate records
   * in the database.
   *
   * @param response {@link EligibilityResponse} object.
   * @param application {@link SingleStreamlinedApplication} object.
   */
  private void processResponse(final EligibilityResponse response, final SingleStreamlinedApplication application, final RunMode runMode)
  {
    log.info("Processing eligibility response: {}", response);

    if (response.getStatus() == ResponseStatus.failure)
    {
      log.error("Eligibility engine responded with a failure for SSAP id: {}", application.getSsapApplicationId());
      if (response.getErrors() != null && response.getErrors().size() > 0)
      {
        response.getErrors().forEach((code, message) -> {
          log.error("- Eligibility error: code: {}, message: {}", code, message);
        });
      }
      return;
    }

    final List<EligibilityProgram> eligibilityPrograms = new ArrayList<>();
    final List<SsapApplicant> applicantList = ssapApplicantRepository.findBySsapApplicationId(Long.parseLong(application.getSsapApplicationId()));
    final Date now = TSDateTime.getInstance().toDate();

    if (response.getMembers() != null)
    {
      final HouseholdMember primaryTaxFiler = HouseholdUtil.getPrimaryTaxHouseholdMember(application.getTaxHousehold());

      response.getMembers().forEach(hhmr -> {
        log.info("- eligibility member: applicantGuid: {}, aptc eligible: {}, chip eligible: {}, csr eligible: {}, exchange eligible: {}, medicaid: {}",
          hhmr.getApplicantId(), hhmr.isAptcEligible(), hhmr.isCsrEligible(), hhmr.isCsrEligible(), hhmr.isExchangeEligible(), hhmr.isMedicaid());

        SsapApplicant applicant = applicantList.stream().filter(a -> a.getApplicantGuid().equals(String.valueOf(hhmr.getApplicantId()))).findAny().orElse(null);

        log.info("Applicant found for applicant guid: {} => {}", hhmr.getApplicantId(), applicant);

        if (applicant != null)
        {
          final EligibilityProgram exchangeEligibilityProgram = new EligibilityProgram();
          exchangeEligibilityProgram.setSsapApplicant(applicant);
          exchangeEligibilityProgram.setEligibilityDeterminationDate(now);
          exchangeEligibilityProgram.setEligibilityType(EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE);
          exchangeEligibilityProgram.setEligibilityStartDate(hhmr.getExchangeEligibilityStartDate());
          exchangeEligibilityProgram.setEligibilityEndDate(hhmr.getExchangeEligibilityEndDate());
          exchangeEligibilityProgram.setEligibilityIndicator(hhmr.isExchangeEligible() ? "TRUE" : "FALSE");
          eligibilityPrograms.add(exchangeEligibilityProgram);

          if (hhmr.isExchangeEligible())
          {
            log.info("- household applicant: {} is eligible for Exchange", hhmr.getApplicantId());
            applicant.setEligibilityStatus(ExchangeEligibilityStatus.QHP.name());
          }
          else
          {
            log.info("- household applicant: {} is NOT eligible for Exchange: ", hhmr.getApplicantId());
            applicant.setEligibilityStatus(ExchangeEligibilityStatus.NONE.name());
          }

          if (hhmr.isChip())
          {
            log.info("- household applicant: {} is eligible for CHIP", hhmr.getApplicantId());
            final EligibilityProgram eligibilityProgram = new EligibilityProgram();
            eligibilityProgram.setSsapApplicant(applicant);
            eligibilityProgram.setEligibilityDeterminationDate(now);

            // CHIP eligibility will not have start/end date.
            eligibilityProgram.setEligibilityType(EligibilityConstants.ASSESSED_CHIP_ELIGIBILITY_TYPE);
            eligibilityProgram.setEligibilityIndicator("TRUE");

            eligibilityPrograms.add(eligibilityProgram);
          }

          if (hhmr.isCsrEligible())
          {
            log.info("- household applicant: {} is eligible for CSR", hhmr.getApplicantId());
            final EligibilityProgram eligibilityProgram = new EligibilityProgram();
            eligibilityProgram.setSsapApplicant(applicant);
            eligibilityProgram.setEligibilityDeterminationDate(now);

            eligibilityProgram.setEligibilityType(EligibilityConstants.CSR_ELIGIBILITY_TYPE);
            eligibilityProgram.setEligibilityStartDate(hhmr.getCsrEligibilityStartDate());
            eligibilityProgram.setEligibilityEndDate(hhmr.getCsrEligibilityEndDate());
            eligibilityProgram.setEligibilityIndicator("TRUE");

            eligibilityPrograms.add(eligibilityProgram);

          }

          if (hhmr.isMedicaid())
          {
            log.info("- household applicant: {} is eligible for Medicaid", hhmr.getApplicantId());
            final EligibilityProgram eligibilityProgram = new EligibilityProgram();
            eligibilityProgram.setSsapApplicant(applicant);
            eligibilityProgram.setEligibilityDeterminationDate(now);

            // Dates will not be returned and will be null the same as for CHIP.
            if(hhmr.isMagiDetermination()) {
              eligibilityProgram.setEligibilityType(EligibilityConstants.ASSESSED_MEDICAID_MAGI_ELIGIBILITY_TYPE);
              eligibilityProgram.setEligibilityIndicator("TRUE");
            } else {
              eligibilityProgram.setEligibilityType(EligibilityConstants.ASSESSED_MEDICAID_NON_MAGI_ELIGIBILITY_TYPE);
              eligibilityProgram.setEligibilityIndicator("TRUE");
            }

            eligibilityPrograms.add(eligibilityProgram);
          }

          if(hhmr.isAptcEligible()) {
            final EligibilityProgram eligibilityProgram = new EligibilityProgram();
            eligibilityProgram.setSsapApplicant(applicant);
            eligibilityProgram.setEligibilityDeterminationDate(now);

            eligibilityProgram.setEligibilityType(EligibilityConstants.APTC_ELIGIBILITY_TYPE);
            eligibilityProgram.setEligibilityIndicator("TRUE");
            eligibilityProgram.setEligibilityStartDate(response.getAptcEligibilityStartDate());
            eligibilityProgram.setEligibilityEndDate(response.getAptcEligibilityEndDate());

            eligibilityPrograms.add(eligibilityProgram);
          }

          applicant.setCsrLevel(null);
          if(hhmr.getCsrLevel() != null) {
            log.info("CSRLevel for applicant: {} is {}", applicant.getId(), hhmr.getCsrLevel());
            applicant.setCsrLevel(hhmr.getCsrLevel());
          }

          log.info("Saving SSAP Applicant with eligibility status: {} and csr level: {}", applicant.getEligibilityStatus(), applicant.getCsrLevel());

          ssapApplicantRepository.save(applicant);
        }
      });

      if (eligibilityPrograms.size() > 0)
      {
        log.info("- saving eligibility {} programs for {} household member(s)", eligibilityPrograms.size(), response.getMembers().size());

        // Delete existing program eligibility records for this SSAP and Applicant if they exist.
        eligibilityPrograms.forEach(ep -> {

          if(log.isInfoEnabled()) {
            log.info("Deleting existing ssap {} program eligibility records for applicant: {}", ep.getSsapApplicant().getSsapApplication().getId(), ep.getSsapApplicant().getId());
          }

          eligibilityProgramRepository.deleteBySsapApplicant(ep.getSsapApplicant());
        });

        // TODO: Update NonFinancialApplicationEligibilityService to not override these
        final List<EligibilityProgram> savedPrograms = eligibilityProgramRepository.save(eligibilityPrograms);

        if (savedPrograms.size() > 0)
        {
          log.info("Saved eligibility program records for household members of SSAP: {}, total records saved: {}", application.getSsapApplicationId(), savedPrograms.size());
        }
        else
        {
          log.error("Unable to create eligibility program records: passed: {}, saved: {}", eligibilityPrograms.size(), savedPrograms.size());
        }
      }

      SsapApplication ssapApplication = ssapApplicationRepository.findOne(Long.valueOf(application.getSsapApplicationId()));

      if (ssapApplication != null)
      {
        response.getMembers().forEach(householdMemberResponse -> {
          application.getTaxHousehold().get(0).getHouseholdMember().stream()
            .filter(member -> member.getApplicantGuid().equals(String.valueOf(householdMemberResponse.getApplicantId())))
            .findFirst()
            .ifPresent(member -> member.setEligibilityResponse(householdMemberResponse));
        });

        JsonUtil.setApplicationData(ssapApplication, application);

        ssapApplication.setEligibilityReceivedDate(now);

        if (response.getMembers() != null && !response.getMembers().isEmpty() &&
          response.getMembers().stream().anyMatch(HouseholdMemberResponse::isAptcEligible))
        {
          ssapApplication.setCsrLevel(getLowestCSRLevel(response.getMembers()));
          ssapApplication.setMaximumAPTC(new BigDecimal(response.getAptcAmount()));
          log.info("Set Maximum APTC Amount from eligibility engine to: {}", ssapApplication.getMaximumAPTC());
        }
        else
        {
          ssapApplication.setCsrLevel(null);
        }

        ssapApplication = exchangeEligibilityDeterminationService.determinateExchangeEligibility(ssapApplication, response);

        // Determinate Native American Household
        final boolean anyoneIsNativeAmericanWithQhp = HouseholdUtil.anyoneNativeAmericanAndQhp(application);

        if(anyoneIsNativeAmericanWithQhp) {
          ssapApplication.setNativeAmerican(DB_TRUE);
        }

        //HIX-117924: Setting NF AI/AN Applications to Default CS and QHP Eligibility Status
        if(DB_TRUE.equalsIgnoreCase(ssapApplication.getNativeAmerican()) && DB_FALSE.equalsIgnoreCase(ssapApplication.getFinancialAssistanceFlag())){
          ssapApplication.setCsrLevel(nativeAmericanCsrLevel);
          ssapApplication.setExchangeEligibilityStatus(ExchangeEligibilityStatus.QHP);
        }

        // Determinate eligibility status of the SSAP
        final boolean anyoneSeekingCoverageNotIncarcerated = HouseholdUtil.getPrimaryTaxHouseholdMembers(application)
          .stream().anyMatch(
            hhm -> hhm.getApplyingForCoverageIndicator() != null &&
              hhm.getApplyingForCoverageIndicator() &&
              !consideredIncarcerated(hhm)
          );

        if(anyoneSeekingCoverageNotIncarcerated) {
          if (runMode != RunMode.BATCH) {
            ssapApplication.setEligibilityStatus(EligibilityStatus.CAE);
          }

          if("OE".equals(ssapApplication.getApplicationType())) {
            ssapApplication.setAllowEnrollment(DB_TRUE);
          }
        }
        else
        {
          // TODO: Do we need to only look at primary tax household if they seek coverage?
          final boolean anyoneSeekingCoverage = application.getTaxHousehold().get(0).getHouseholdMember().stream().anyMatch(
            hhm -> hhm.getApplyingForCoverageIndicator() != null && hhm.getApplyingForCoverageIndicator()
          );

          if(!anyoneSeekingCoverage && "SEP".equals(ssapApplication.getApplicationType())) {
            ssapApplication.setApplicationStatus(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
          } else {
            ssapApplication.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
          }

          ssapApplication.setEligibilityStatus(EligibilityStatus.DE);
          ssapApplication.setAllowEnrollment(DB_FALSE);
        }

        log.info("[Saving SSAP]:\n[exchange eligibility status: {}]\n[eligibility status: {}]\n" +
            "[application status: {}]\n[csr level: {}]\n[allow enrollment: {}]\n[ai/na hh: {}]",
          ssapApplication.getExchangeEligibilityStatus(),
          ssapApplication.getEligibilityStatus(),
          ssapApplication.getApplicationStatus(),
          ssapApplication.getCsrLevel(),
          ssapApplication.getAllowEnrollment(),
          ssapApplication.getNativeAmerican());
        ssapApplicationRepository.save(ssapApplication);
        
        insertAptcHistory(response, ssapApplication, application);
      }
      else
      {
        log.error("Unable to find SSAP by given application id: {}", application.getSsapApplicationId());
      }
    }
  }
  
  public void insertAptcHistory(final EligibilityResponse response, SsapApplication ssapApplication, final SingleStreamlinedApplication application) {
	  AptcHistory aptcHistory = new AptcHistory();
	  if(application.getApplyingForFinancialAssistanceIndicator() && response.getSlspBenchmarkPremium() >= 0) {
		if(response.getAptcAmount() >=0) {
			aptcHistory.setEffectiveDate(response.getAptcEligibilityStartDate());
			aptcHistory.setEligStartDate(response.getAptcEligibilityStartDate());
			aptcHistory.setEligEndDate(response.getAptcEligibilityEndDate());
			aptcHistory.setAptcAmount(new BigDecimal(response.getAptcAmount()));
		}
		else if(response.getMembers().size()>0)
		{
			aptcHistory.setEffectiveDate(response.getMembers().get(0).getExchangeEligibilityStartDate());
			aptcHistory.setEligStartDate(response.getMembers().get(0).getExchangeEligibilityStartDate());
			aptcHistory.setEligEndDate(response.getMembers().get(0).getExchangeEligibilityEndDate());
		}
		aptcHistory.setSlcspAmount(new BigDecimal(response.getSlspBenchmarkPremium()));
	  }
	  aptcHistory.setSsapApplicationId(ssapApplication.getId());
	  aptcHistory.setIsRecalc("N");
	  aptcHistory.setCoverageYear(ssapApplication.getCoverageYear());
	  aptcHistory.setCmrHouseholdId(ssapApplication.getCmrHouseoldId());
	  aptcHistoryRepository.save(aptcHistory);
  }


  /**
   * Returns CSR level based on the priority list.
   *
   * @param members household member responses from eligibility engine/
   * @return CSR with highest precedence or null
   */
  private String getLowestCSRLevel(final List<HouseholdMemberResponse> members) {
    List<String> memberLevels = members.stream().map(HouseholdMemberResponse::getCsrLevel).distinct().collect(Collectors.toList());
    return Stream.of("CS1", "CS4", "CS5", "CS6", "CS3", "CS2").filter(memberLevels::contains).findFirst().orElse(null);
  }

  public EligibilityRequest getEligibilityRequestFromApplication(SingleStreamlinedApplication application, RunMode runMode)
  {
    final EligibilityRequest request = new EligibilityRequest();

    final HouseholdMember primaryHouseholdMember = HouseholdUtil.getPrimaryTaxHouseholdMember(application.getTaxHousehold().get(0));
    final TaxHousehold taxHousehold = application.getTaxHousehold().get(0);

    assert taxHousehold != null : "Tax Household for SSAP is null; ssap id" + application.getSsapApplicationId();

    SsapHelper.setApplicationTypeForEligibilityEngine(application, request, runMode);
    request.setRequestId(UUID.randomUUID().toString());
    request.setApplicationId(Long.parseLong(application.getSsapApplicationId()));
    request.setCoverageYear(Math.toIntExact(application.getCoverageYear()));
    request.setZipCode(primaryHouseholdMember.getHouseholdContact().getHomeAddress().getPostalCode());

    if(primaryHouseholdMember.getHouseholdContact().getHomeAddress().getCountyCode() != null) {
    	request.setCountyCode(primaryHouseholdMember.getHouseholdContact().getHomeAddress().getCountyCode());
    } else {
    	request.setCountyCode(primaryHouseholdMember.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode());
    }
    request.setFinancialAssistance(application.getApplyingForFinancialAssistanceIndicator());

    // Financial Steps
    if (request.isFinancialAssistance())
    {
      request.setHouseholdIncome(HouseholdIncomeCalculator.calculateTotalHouseholdIncomeForEligibility(application));
      if(runMode == RunMode.BATCH) {
        request.setFailureToReconcile(taxHousehold.isFailureToReconcile());
      } else {
        request.setFailureToReconcile(!taxHousehold.isReconciledAptc());
      }
      request.setHouseholdSize(calculateHouseholdSize(application));
    }
    else {
      request.setHouseholdSize(HouseholdSizeCalculator.calculateNonFinancialSize(application.getTaxHousehold().get(0).getHouseholdMember()));
    }

    request.setMembers(getHouseholdMemberRequests(application));

    return request;
  }

  /**
   * Returns List of {@link HouseholdMemberRequest} objects from given {@link SingleStreamlinedApplication}.
   * @param application {@link SingleStreamlinedApplication} object.
   * @return {@link List} of {@link HouseholdMemberRequest} objects.
   */
  private List<HouseholdMemberRequest> getHouseholdMemberRequests(SingleStreamlinedApplication application)
  {
    final List<HouseholdMember> householdMember = HouseholdUtil.getPrimaryTaxHouseholdMembers(application);
    final List<HouseholdMemberRequest> primaryHouseholdMemberRequest = getHouseholdMemberRequestList(application, householdMember, true);

    // final boolean anyoneHasTaxRelationship = HouseholdUtil.anyoneHasTaxRelationship(application);

    if(application.getApplyingForFinancialAssistanceIndicator() != null && application.getApplyingForFinancialAssistanceIndicator() == Boolean.TRUE) {
      final List<HouseholdMember> nonPrimaryHouseholdMembers = HouseholdUtil.getNonPrimaryTaxHouseholdMembers(application);
      final List<HouseholdMemberRequest> nonPrimaryHouseholdMemberRequest = getHouseholdMemberRequestList(application, nonPrimaryHouseholdMembers, false);

      if (!nonPrimaryHouseholdMembers.isEmpty()) {
        final List<HouseholdMemberRequest> returnedList = new ArrayList<>(primaryHouseholdMemberRequest);
        returnedList.addAll(nonPrimaryHouseholdMemberRequest);
        return returnedList;
      }
    }

    return primaryHouseholdMemberRequest;
  }

  /**
   * Gets list of {@link HouseholdMemberRequest} objects for given list of {@link HouseholdMember}'s.
   *
   * @param application {@link SingleStreamlinedApplication} object.
   * @param members List of {@link HouseholdMember} to build requests from.
   * @param primaryHouseholdMembers boolean value if given list of {@link HouseholdMember}'s is part of primary tax household or not.
   * @return List of {@link HouseholdMemberRequest} objects.
   */
  private List<HouseholdMemberRequest> getHouseholdMemberRequestList(SingleStreamlinedApplication application,
                                                                     List<HouseholdMember> members,
                                                                     boolean primaryHouseholdMembers) {
    final TaxHousehold taxHousehold = application.getTaxHousehold().get(0);
    final HouseholdMember primaryHouseholdMember = HouseholdUtil.getPrimaryTaxHouseholdMember(taxHousehold);
    final List<HouseholdMemberRequest> hhr = new ArrayList<>();

    List<BloodRelationship> bloodRelationshipList = HouseholdUtil.getBloodRelationships(taxHousehold);

    for (HouseholdMember hhm : members)
    {
      HouseholdMemberRequest mr = new HouseholdMemberRequest();
      mr.setPrimaryHousehold(primaryHouseholdMembers);
      mr.setApplicantId(Long.parseLong(hhm.getApplicantGuid()));

      // If member not part of the primary tax household
      if (!primaryHouseholdMembers) {
        // If this member does not belong to any other tax household
        boolean danglingMember = HouseholdUtil.isDanglingMember(hhm);
        if (danglingMember) {
          mr.setSeekingCoverage(hhm.getApplyingForCoverageIndicator() == Boolean.TRUE);
        } else {
          // If this member belong to some other tax household
          mr.setSeekingCoverage(false);
        }
      } else {
        // We are looking at primary tax household member
        mr.setSeekingCoverage(hhm.getApplyingForCoverageIndicator() == Boolean.TRUE);
      }

      mr.setDateOfBirth(hhm.getDateOfBirth());
      if(hhm.getName() != null) {
        mr.setFirstName(hhm.getName().getFirstName());
        mr.setLastName(hhm.getName().getLastName());
      }
      mr.setGender(hhm.getGender() != null && hhm.getGender().toLowerCase().contains("f") ? GenderEnum.FEMALE : GenderEnum.MALE);
      mr.setNumberOfBabies(HouseholdSizeCalculator.getNumberOfExpectedChildren(hhm));

      if(hhm.getCitizenshipImmigrationStatus() != null) {
        mr.setCitizen(hhm.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator() == Boolean.TRUE);
        mr.setLegalAlien(hhm.getCitizenshipImmigrationStatus().getLawfulPresenceIndicator() == Boolean.TRUE);
      }

      if(hhm.isChangeInImmigrationSince5Year() != null) {
        mr.setChangeInImmigrationLastFiveYears(hhm.isChangeInImmigrationSince5Year() == Boolean.TRUE);
      }

      if(hhm.getIncarcerationStatus() != null) {
        mr.setIncarcerated(hhm.getIncarcerationStatus().getIncarcerationStatusIndicator());
      }

      if(hhm.getAmericanIndianAlaskaNative() != null) {
        mr.setNativeAmerican(hhm.getAmericanIndianAlaskaNative().getMemberOfFederallyRecognizedTribeIndicator() == Boolean.TRUE);
      }

      if(application.getApplyingForFinancialAssistanceIndicator() != null && application.getApplyingForFinancialAssistanceIndicator()) {
        SpecialCircumstances specialCircumstances = hhm.getSpecialCircumstances();

        boolean hasESI = hhm.isHasESI();
        boolean hasNonESI = false;
        boolean medicare = false;

        if(hhm.getHealthCoverage() != null && hhm.getHealthCoverage().getCurrentOtherInsurance() != null) {
          CurrentOtherInsurance currentOtherInsurance = hhm.getHealthCoverage().getCurrentOtherInsurance();
          hasNonESI = currentOtherInsurance.isHasNonESI();

          // If member has State or Federal Medicare Program and person is eligible, pass medicare = true to eligibility.
          if(hhm.getHealthCoverage().getCurrentlyHasHealthInsuranceIndicator() == Boolean.TRUE &&
            currentOtherInsurance.getOtherStateOrFederalPrograms() != null &&
            !currentOtherInsurance.getOtherStateOrFederalPrograms().isEmpty())
          {
            medicare = currentOtherInsurance
              .getOtherStateOrFederalPrograms().stream().anyMatch(
                p -> p != null && p.getType() != null && p.getType() == OtherStateOrFederalProgramType.MEDICARE && p.isEligible()
              );
          }
        }


        if (hhm.getPersonId().equals(primaryHouseholdMember.getPersonId()))
        {
          mr.setRelationshipToPrimary(BloodRelationshipCode.SELF);
        }
        else
        {
          // Eligibility Engine supports only: SELF, SPOUSE, CHILD, all other should be OTHER
          Optional<BloodRelationship> brc = bloodRelationshipList.stream().filter(br -> br.getIndividualPersonId().equals(String.valueOf(hhm.getPersonId())) &&
            br.getRelatedPersonId().equals(String.valueOf(taxHousehold.getPrimaryTaxFilerPersonId()))).findAny();

          if(brc.isPresent()) {
            mr.setRelationshipToPrimary(BloodRelationshipCode.getBloodRelationForCode(brc.get().getRelation(), true));
          } else {
            mr.setRelationshipToPrimary(BloodRelationshipCode.OTHER);
          }
        }

        mr.setHasESI(hasESI);
        mr.setHasNonESI(hasNonESI);
        mr.setMedicare(medicare);

        mr.setPregnant(specialCircumstances.getPregnantIndicator() == Boolean.TRUE);
        mr.setDisabled(hhm.getDisabilityIndicator());
        mr.setDeniedMedicaidChip(hhm.isMedicaidChipDenial());

        // Medicaid MAGI income/hh size
        mr.setHouseholdSizeMedicaid(calculateHouseholdMagiMedicaidSize(application, hhm));
        mr.setHouseholdIncomeMedicaid(calculateHouseholdMagiMedicaidIncome(application, hhm));

        mr.setFiveYearBar(hhm.isFiveYearBar());
        mr.setFosterCare(specialCircumstances.getEverInFosterCareIndicator() == Boolean.TRUE);

        if(hhm.getTaxHouseholdComposition() != null)
        {
          mr.setTaxFilingStatus(hhm.getTaxHouseholdComposition().getTaxFilingStatus());
          mr.setTaxRelationship(hhm.getTaxHouseholdComposition().getTaxRelationship());
        }
      }

      mr.setSeeksQhp(hhm.isSeeksQhp());
      hhr.add(mr);
    }

    return hhr;
  }

  /**
   * Calculates MAGI medicaid household income for given household member.
   * @param application {@link SingleStreamlinedApplication} application.
   * @param member household member for which to calculate MAGI Medicaid household income.
   * @return yearly household income.
   */
  private long calculateHouseholdMagiMedicaidIncome(SingleStreamlinedApplication application, HouseholdMember member) {
    final List<HouseholdMember> members = HouseholdUtil.getPrimaryTaxHouseholdMembers(application);
    final HouseholdMember primaryTaxFiler = HouseholdUtil.getPrimaryTaxHouseholdMember(application.getTaxHousehold());
    return HouseholdIncomeCalculator.calculateMagi(members, member, Math.toIntExact(application.getCoverageYear()), primaryTaxFiler.getPersonId());
  }

  /**
   * Household size calculation for financial application.
   *
   * @param application {@link SingleStreamlinedApplication} object.
   * @return household size.
   */
  private int calculateHouseholdSize(final SingleStreamlinedApplication application)
  {
    // TODO: Supreet mentioned that all members should be counted, not only ones seeking coverage, Make sure Reshma & Supreet on the same page.
    final TaxHousehold taxHousehold = application.getTaxHousehold().get(0);
    final List<HouseholdMember> primaryMembers = HouseholdUtil.getPrimaryTaxHouseholdMembers(application);
    return HouseholdSizeCalculator.calculateSize(primaryMembers, taxHousehold.getPrimaryTaxFilerPersonId());
  }

  /**
   * Calculates MAGI Medicaid household size.
   * @param application {@link SingleStreamlinedApplication} object.
   * @param member household member for which to calculate household size.
   * @return household size for given household member.
   */
  private int calculateHouseholdMagiMedicaidSize(final SingleStreamlinedApplication application, HouseholdMember member) {
    final List<HouseholdMember> members = HouseholdUtil.getPrimaryTaxHouseholdMembers(application);
    final HouseholdMember primaryTaxFiler = HouseholdUtil.getPrimaryTaxHouseholdMember(application.getTaxHousehold());
    return HouseholdSizeCalculator.calculateSizeMagi(members, member, Math.toIntExact(application.getCoverageYear()), primaryTaxFiler.getPersonId());
  }

  /**
   * Returns boolean if given Household member is considered to be incarcerated.
   * Incarceration is determinted by checking
   * ({@link IncarcerationStatus#getIncarcerationStatusIndicator()} {@code == true}) flag.
   *
   * This will not return true however, if they are incarcerated ({@link IncarcerationStatus#getIncarcerationStatusIndicator()} {@code == true})
   * but pending disposition {@link IncarcerationStatus#getIncarcerationPendingDispositionIndicator()} {@code == true}).
   *
   *
   *
   * @param householdMember {@link HouseholdMember}.
   * @return true if they care considered to be incarcerated for the purpose of eligibility.
   */
  private boolean consideredIncarcerated(final HouseholdMember householdMember) {
    boolean result = false;

    if(householdMember != null && householdMember.getIncarcerationStatus() != null) {
      final IncarcerationStatus incarcerationStatus = householdMember.getIncarcerationStatus();
      if(incarcerationStatus.getIncarcerationAsAttestedIndicator() != null &&
        incarcerationStatus.getIncarcerationAsAttestedIndicator())
      {
        result = incarcerationStatus.getIncarcerationPendingDispositionIndicator() == null
          || !incarcerationStatus.getIncarcerationPendingDispositionIndicator();

        if(householdMember.getName() != null) {
          log.debug("Household member: {} {} is considered incarcerated: {}, incarcerated as attested: {}, incarceration pending disposition: {}",
            householdMember.getName().getFirstName(),
            householdMember.getName().getLastName(),
            result,
            incarcerationStatus.getIncarcerationAsAttestedIndicator(),
            incarcerationStatus.getIncarcerationPendingDispositionIndicator());
        }

      }
    }

    return result;
  }
}
