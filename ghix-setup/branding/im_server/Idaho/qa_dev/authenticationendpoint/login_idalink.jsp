<!DOCTYPE html>
<!--
~ Copyright (c) 2005-2014, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
~
~ WSO2 Inc. licenses this file to you under the Apache License,
~ Version 2.0 (the "License"); you may not use this file except
~ in compliance with the License.
~ You may obtain a copy of the License at
~
~    http://www.apache.org/licenses/LICENSE-2.0
~
~ Unless required by applicable law or agreed to in writing,
~ software distributed under the License is distributed on an
~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
~ KIND, either express or implied.  See the License for the
~ specific language governing permissions and limitations
~ under the License.
-->

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.Constants" %>
<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.CharacterEncoder"%>
<%@ page import="org.wso2.carbon.identity.application.common.util.IdentityApplicationConstants"%>

<fmt:bundle basename="org.wso2.carbon.identity.application.authentication.endpoint.i18n.Resources">
<%
	request.getSession().invalidate();
    Map<String, String> baseUrlLookup = new HashMap<String, String>();
    baseUrlLookup.put("idalink", "https://idalink.idaho.gov/");
    baseUrlLookup.put("idalink-dev", "https://idalinkd.dhw.state.id.us/");
    baseUrlLookup.put("idalink-i", "https://idalinki.dhw.state.id.us/");
    baseUrlLookup.put("idalinkqual", "https://idalinkq.dhw.state.id.us/");
    baseUrlLookup.put("idalinkqf", "https://idalinkqf.dhw.state.id.us/");
    baseUrlLookup.put("idalink-pww-dev", "http://idalink.portlandwebworks.com/");
    baseUrlLookup.put("idalinkEzra", "http://idalink-local:8080/");

    Map<String, String> googleAnalyticsLookup = new HashMap<String, String>();
    //Comment out to exclude Analytics from the login page
//    googleAnalyticsLookup.put("idalink",         "UA-XXXXXXXX-X");//DHWs account
//    googleAnalyticsLookup.put("idalinkq",        "UA-XXXXXXXX-X");//DHWs account
//    googleAnalyticsLookup.put("idalinkqf",       "UA-XXXXXXXX-X");//DHWs account
    googleAnalyticsLookup.put("idalink-i",       "UA-55824176-2");//PWWs account
    googleAnalyticsLookup.put("idalink-dev",     "UA-55824176-2");//PWWs account
    googleAnalyticsLookup.put("idalink-pww-dev", "UA-55824176-2");//PWWs account
    googleAnalyticsLookup.put("idalinkEzra",     "UA-55824176-2");//PWWs account

    String username = CharacterEncoder.getSafeText(request.getParameter("username"));
    if (username == null) {
        username = "";
    }

    String referrer = CharacterEncoder.getSafeText(request.getHeader("Referer"));
    if (referrer == null) {
        referrer = "";
    }

    String relyingParty = CharacterEncoder.getSafeText(request.getParameter("relyingParty"));
	if (relyingParty != null && !relyingParty.isEmpty()) {
		Cookie relyingPartyCookie = new Cookie("relyingParty", relyingParty);
		relyingPartyCookie.setPath("/");
		relyingPartyCookie.setMaxAge(36000);
		response.addCookie(relyingPartyCookie);
	}
    String baseUrl = baseUrlLookup.get(relyingParty);
    if (baseUrl == null) {
        baseUrl = referrer;
    }

    String automationAnalytics = "";
    Cookie[] cookies = null;
    cookies = request.getCookies();
    if( cookies != null ){
        for (Cookie cookie : cookies){
            if ( "automation-analytics".equals(CharacterEncoder.getSafeText(cookie.getName()) ) ) {
                automationAnalytics = CharacterEncoder.getSafeText(cookie.getValue());
				break;
            }
        }
    }

    String googleIdentity = "suppressed".equals(automationAnalytics) ? null : googleAnalyticsLookup.get(relyingParty);

%>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>idalink login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" media="screen,print" href="assets_idalink/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen,print" href="assets_idalink/css/styles.css"/>

    <script src="assets_idalink/js/jquery.min.js"></script>
    <script src="assets_idalink/js/bootstrap.min.js"></script>
    <script src="assets_idalink/js/login.js"></script>
    <script src="assets_idalink/js/respond.src.js"></script>
</head>

<body id="user-login">

<% if(googleIdentity != null) { %>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', '<%=googleIdentity%>', 'auto');
    ga('send', 'pageview');

</script>
<% } %>
<div class="masthead" role="navigation">
    <div>
        <div class="idaho-logo">
            <a href="http://idaho.gov/" target="_blank"><img src="assets_idalink/img/idaho_logo.png" alt="Idaho"></a>
        </div>
    </div>
</div>

<div class="content">
    <div class="help-info">
        <button class="help-button help-toggle"><span></span> Help</button>
    </div>
    <div class="page-header">
        <h1>Sign In
            <small>Your online portal for Health Coverage Assistance, Food Assistance and other programs in Idaho.</small></h1>

        <p class="chrome">
            Best viewed in: <img src="assets_idalink/img/chrome.png" alt="Google Chrome" />
        </p>
    </div>


    <div class="login-form">
        <div class="idalink-logo">
            <img src="assets_idalink/img/idaLink_logo.png" />
        </div>

        <div class="form-content">
            <%
                String errorMessage = "";
                boolean loginFailed = false;

                if (CharacterEncoder.getSafeText(request.getParameter(Constants.AUTH_FAILURE)) != null &&
                            "true".equals(CharacterEncoder.getSafeText(request.getParameter(Constants.AUTH_FAILURE)))) {
                    loginFailed = true;

                    if(CharacterEncoder.getSafeText(request.getParameter(Constants.AUTH_FAILURE_MSG)) != null){
                        errorMessage = (String) CharacterEncoder.getSafeText(request.getParameter(Constants.AUTH_FAILURE_MSG));

                        if (errorMessage.equalsIgnoreCase("login.fail.message")) {
                            errorMessage = "Authentication Failed! Please retry or <a href=\"" + baseUrl + "login?p_p_id=58&_58_struts_action=%2Flogin%2Fforgot_password\">click here</a> if you believe you have entered your username and password correctly.";
                        }
                    }
                }

                //Check for account locking message
                String errorCode = CharacterEncoder.getSafeText(request.getParameter("errorCode"));
                if (errorCode != null && errorCode.contains("17003")) {
                    loginFailed = true;
                    errorMessage = "For security reasons your account has been temporarily locked due to several unsuccessful login attempts. Please try again after 10 minutes.";
                }

                if (loginFailed) {
            %>
            <div class="error-message">
                <%=errorMessage%>
            </div>
            <%
                } else {
            %>

             <p>If you already have an idalink or Your Health Idaho account, please login with those credentials.  If you are an Agent with an idalink or Your Health Idaho account, please login with your agent credentials.</p>
			<%
				}
			%>
            <form action="../../commonauth" method="post" id="loginForm" class="" role="form" >
                <div class="basicauth-form">
                    <div>
                        <label for="username">Email Address</label>
                        <div>
                            <input type="text" id='username' name="username" size='30'
                                   value='<%=username%>' />
                        </div>
                    </div>
                    <div>
                        <label for="password">Password</label>

                        <div>
                            <input type="password" id='password' name="password" size='30' />
                        </div>
                    </div>

                    <input type="hidden"
                           name="sessionDataKey"
                           value='<%=CharacterEncoder.getSafeText(request.getParameter("sessionDataKey"))%>' />
                </div>
                <div>
                    <button type="submit" id="submit"><span></span> Enter</button>
                </div>
                <div>
                    <div>
                        <a href="<%=baseUrl%>login?p_p_id=58&_58_struts_action=%2Flogin%2Fforgot_password">Forgot Password?</a>
                    </div>
                </div>
            </form>


        </div>
    </div>

    <div class="registration-info">
        <div class="your-health">
            <img src="assets_idalink/img/logo_id.png" alt="Your Health Idaho" />
            <p>Our partner in bringing health insurance to Idahoans.</p>
        </div>
        <div class="idalink-reg">
            <img src="assets_idalink/img/icon_register.png" alt="Register for idalink " />
            <p>If you are new to idalink and wish to register for an account,<br />
                <a href="<%=baseUrl%>register">Register here</a>.</p>
        </div>
    </div>
</div>

<div class="footer">
    <div class="container">
        <p class="footer-nav">
            
            <a href="http://www.healthandwelfare.idaho.gov/ContactUs/tabid/127/Default.aspx">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="<%=baseUrl%>privacy-and-security" target="_blank">Privacy &amp; Security</a>
        </p>
        <div>
            <p>
                <a href="http://www.healthandwelfare.idaho.gov"
                   target="_blank"
                   title="Idaho Department of Health and Welfare"
                   class="health-welfare">Idaho Department of Health &amp; Welfare</a>
            </p>
        </div>
    </div>
</div>



<section class="help hidden" >
	<header>
		<h1>Help</h1>
		<a class="close">Close</a>
		<nav class="help-nav">
			<ul>
				<li><a href="#" class="active">FAQs</a></li>
			</ul>
		</nav>
	</header>

	<div class="help-content" id="faq">

		<h2>Frequently Asked Questions</h2>
		<p> If you have questions about your eligibility for benefit programs or completing your re-evaluation, or if you are experiencing technical problems with the website, please contact us at 1-877-456-1233 or 208-334-6700 between the hours of 8 a.m. and 6 p.m. Mountain  Time, Monday thru Friday, except holidays.</p>

		<h3 class="help-topic">Creating an Account and Login</h3>

		<div class="help-questions" >
			<h4>What can I do if I have forgotten my username?</h4>
			<p>Your username will be your email address. Please try entering your email address again in case you mistyped it while attempting to login. If you still cannot login, please call 1-877-456-1233 or 208-334-6700, between the hours of 8 a.m. and 6 p.m. Mountain Time, Monday thru Friday, except holidays, for assistance.</p>

			<h4>What do I do if I've forgotten my password?</h4>
			<p>If you've forgotten your password you can click on the 'Forgot Password' link located on the login screen and we will send you a temporary password. You can re-set this password within the portal after passing a security verification. If you cannot reset your password, please call 1-877-456-1233, 208-334-6700, between the hours of 8 a.m. and 6 p.m. Mountain Time, Monday thru Friday, except holidays, for assistance.</p>

			<h4>How do I reset my password?</h4>
			<p>You are able to reset your password by clicking on the "My Account" link and then navigating to Account Settings. Here you must enter your current password and then are able to reset it to a new password.</p>

			<h4>Why didn't I receive an email with my temporary password after registering on idalink?</h4>
			<p>You should first confirm that the email is not in your Spam or Junk folder. If you find the email in your Spam or Junk folder, we recommend that you mark IDHW as a safe sender with your email provider. If you did not find the email, please call 1-877-456-1233 or 208-334-6700, between the hours of 8 a.m. and 6 p.m. Mountain Time, Monday thru Friday, except holidays, for assistance.</p>

			<h4>When submitting my registration, why do I receive an error message stating that an idalink account already exists with the information that I provided?</h4>
			<p>This message means that there is already an account registered using the identifying information that you provided. Please call 1-877-456-1233 or 208-334-6700, between the hours of 8 a.m. and 6 p.m. Mountain Time, Monday thru Friday, except holidays, for assistance.</p>

			<h4>When submitting my registration, why do I receive a message that I may already be registered with Your Health Idaho or idalink?</h4>
			<p>This message means that there is already an account registered with the email address that you provided. This account could have been created on either Your Health Idaho or the idalink website. As your email address is used as your username, we do not allow more than one person to use the same email address when registering.</p>

			<h4>Can I use my Your Health Idaho login information to login to idalink?</h4>
			<p>Yes. If you have already registered with Your Health Idaho, you do not need to register again on idalink. When you sign in to idalink, you will need to provide some additional information to complete your idalink account set up.</p>

			<h4>Why is my account locked?</h4>
			<p>If you attempted to login with an invalid combination of username and password 5 times, your account is locked for a period of 10 minutes. After this time passes, you may try to login again. If you still cannot login, please call 1-877-456-1233 or 208-334-6700, between the hours of 8 a.m. and 6 p.m. Mountain Time, Monday thru Friday, except holidays, for assistance.</p>
		 
		</div>

		<h3 class="help-topic">Viewing Your Benefits &amp; Reporting Changes</h3>
		<div class="help-questions">

			<h4>What benefits can I see on the idalink portal?</h4>
			<p>The idalink portal displays benefit information for programs and people that are currently active or that were discontinued within the last 60 days. You will only see information for programs where you are listed as the primary applicant in our system.</p>

			<h4>Why aren't my benefits displaying as expected when logging into idalink?</h4>
			<p>If you are currently receiving benefits and do not see this information when logging in to idalink, please call 1-877-456-1233 or 208-334-6700, between the hours of 8 a.m. and 6 p.m. Mountain Time, Monday thru Friday, except holidays, for assistance.</p>

			<h4>How do I report a change to my household information (physical and/or mailing address, add/remove household members, a change in income, etc.)?</h4>
			<p>Please call 1-877-456-1233 or 208-334-6700, between the hours of 8 a.m. and 6 p.m. Mountain Time, Monday thru Friday, except holidays, or email mybenefits@dhw.idaho.gov to report your change.</p>

			<h4>What if nothing has changed since my last re-evaluation?</h4>
			<p>Even if nothing has changed since your last re-evaluation, you need to provide the Department with the required information so that your eligibility can be redetermined.</p>

			<h4>What should I do if I believe that my particular situation (change in income, student status, living arrangement, etc.) doesn't need to be reported to the Department at re-evaluation or for my HCA application because it doesn't affect my eligibility.</h4>
			<p>Please make sure you are providing all requested information, whether you believe it affects your case or not. The Department will determine whether or not it affects your case. Not reporting information that is actually required could result in disqualification, an overpayment, or a sanction.</p>

			<h4>Why were my benefits discontinued?</h4>
			<p>Benefits are eligibility based. If you have questions about your benefits, call 1-877-456-1233 or 208-334-6700, between the hours of 8 a.m. and 6 p.m. Mountain Time, Monday thru Friday, except holidays, or email mybenefits@dhw.idaho.gov.</p>

			<h4>What do I need to do if I am trying to complete my re-evaluation but my Food Stamp benefits are already discontinued?</h4>
			<p>If your Food Stamps have stopped because you did not complete all the required steps in the re-evaluation process, you must re-apply.</p>

			<h4>What should I do if I do not agree with the eligibility determination?</h4>
			<p>Call 1-877-456-1233 or 208-334-6700, between the hours of 8 a.m. and 6 p.m. Mountain Time, Monday thru Friday, except holidays, email mybenefits@dhw.idaho.gov, or go to your local office to talk with a case worker.</p>

			<h4>When will my Food Stamps be on my card?</h4>
			<p>Newly approved Food Stamps benefits are paid out the day after benefits are approved. Ongoing Food Stamp benefits are paid out on the first day of each month. To verify benefits are on your card, call JP Morgan at 1-888-432-4328 or go online to <a href="http://www.ebtaccount.jpmorgan.com" target="_blank" title="UCard Center">www.ebtaccount.jpmorgan.com</a>.</p>

			<h4>When will my cash assistance be on my card?</h4>
			<p>Newly approved cash assistance benefits are paid out the day after the cash assistance is approved. Ongoing cash assistance benefits are paid out on the first day of each month. To verify benefits are on your card, call JP Morgan at 1-888-432-4328 or go online to <a href="http://www.ebtaccount.jpmorgan.com" target="_blank" title="UCard Center">www.ebtaccount.jpmorgan.com</a>.</p>
		</div>

		<h3 class="help-topic">Personal &amp; Household Info including Tax Status</h3>
		<div class="help-questions">
			<h4>How long will it take for me to complete my HCA application?</h4>
			<p>Depending on the size of your household and whether you have all of the necessary information is available to you, the application process may take as little as 20 minutes. Once you begin an application online, you have 72 hours to complete it. If you do not complete the online application within 72 hours, you must begin the application process again to ensure that the most recent information is available for your application.</p>

			<h4>What is defined as 'Your Household'?</h4>
			<p>Your household is comprised of all of the people living at the physical address you wrote down in the "About You" section.</p>

			<h4>How do I tell you about new or changed relationships in my household?</h4>
			<p>You can edit existing relationships or define relationships for newly added household members by going to the 'Household Relationships' page in the Personal info section. Clicking on the edit button allows you to make the necessary edits and clicking 'Save' updates the information.</p>

			<h4>What if I can't see all members of my household?</h4>
			<p>The portal displays all household members that you told us about during your initial application or during your re-evaluation. If you would like to add or remove someone from your household during your re-evaluation or HCA application, you can do this in the "Your Household" section of the site.</p>

			<h4>What about the people on my application who aren't applying for health coverage?</h4>
			<p>We ask about income and other information for everyone in your household in order to determine what types of assistance you may qualify for. The amount or type of assistance you qualify for can depend on the number of people in your household, their information, and their relationship to each other.</p>

			<h4>Why am I being asked to enter my preferred spoken and written language?</h4>
			<p>Providing this information allows DHW to provide an interpreter for you at no cost.  This information will not be used in any way towards your eligibility.</p>

			<h4>Why is my tax filing information required for benefits?</h4>
			<p>We need information about everyone you include on your federal tax return (if you file taxes) even if they don't live at the same address. You must intend to file taxes in order to qualify for APTC, but you are not required to file taxes to qualify for other types of assistance.</p>

		</div>
		<h3 class="help-topic">Income &amp; Employment</h3>

		<div class="help-questions">
			<h4>Why is the employer name you have listed on the Job Income page different than the name of my current employer?</h4>
			<p>The employer name listed may be the corporate or official name of your employer.</p>

			<h4>What type of income do I have to report?</h4>
			<p>You are required to report all income received by all members of your household. This includes wages, tips, cash gifts, and self-employment. Self-employment can be anything from owning your own business to donating plasma.</p>

			<h4>What are Gross Receipts or Sales?</h4>
			<p>Gross receipts is the total revenue a company or organization receives before subtracting any costs or expenses. This does not include income from the sale of fixed assets.</p>

			<h4>What is Net Profit from Sale of Assets?</h4>
			<p>The net profit from sale of assets is a profit that results from the sale of a capital asset, such as stock, bond or real estate, where the sale amount exceeds the purchase price. The net profit is the difference between the higher selling price and lower purchase price.</p>

			<h4>What is Cost of Goods sold?</h4>
			<p>Cost of Goods Sold is the cost to produce or manufacture merchandise sold, including material and labor used to produce products for sale and products purchased for resale (less ending inventory of items not sold).</p>

			<h4>What are Wages or Draws (for self-employment)?</h4>
			<p>Wages from self-employment are income received that is not related to the profit earned from a business. It is possible to receive both wages and business income profit. Both of these must be recorded. </p>
			<p>Draws are income that is paid prior to earning it.</p>
		</div>

		<h3 class="help-topic">Employer Coverage, Retroactive Medicaid and Medical Services in the home</h3>

		<div class="help-questions">
			<h4>What does it mean to have "access to" Employer coverage?</h4>
			<p>If someone in the household is eligible for health coverage from a job but is currently not covered, the person is considered to have "access to" employer coverage.</p>

			<h4>What is the difference between Employer Insurance and Other Insurance?</h4>
			<p>Employer insurance is health coverage offered by an employer.  Often, the employer pays a portion of the cost of the health coverage.</p>

			<p>Other coverage is health coverage obtained through a means other than an employer.  An example of other coverage is private health coverage, which is paid for entirely by the individual obtaining the coverage.</p>

			<h4>Why do you ask if anyone applying for Health Coverage Assistance would like help paying for medical costs from the last 3 months?</h4>
			<p>In addition to determining if you are eligible for Medicaid or APTC beginning in the month you apply, we can determine if you, or anyone in your household, are eligible for Medicaid in the three months prior to the month you apply. If you are eligible, Medicaid may help cover the cost of medical services you received in those months.</p>

			<h4>What types of services are included when you ask if anyone applying for Health Coverage Assistance needs medical services provided in the home?</h4>
			<p>In addition to covering medical services provided at medical facilities, some customers may be eligible for in-home care and support to remain safely at home.</p>


		</div>

		<h3 class="help-topic">Submission and Verification Documents</h3>

		<div class="help-questions">
			<h4>What verifications will I need to provide to complete my re-evaluation?</h4>
			<p>When completing a re-evaluation, the information you provide to us needs to be verified. For a list of verifications, please go <a href="http://healthandwelfare.idaho.gov/FoodCashAssistance/ApplyforAssistance/Applyforservices/Providetheseverifications/tabid/1561/Default.aspx" target="_blank" title="DHW - Verifications">here</a>.</p>

			<h4>If I do not have the required verifications to complete my re-evaluation, are there alternative verifications that I can provide?</h4>
			<p>When you do not have the required verifications to complete your re-evaluation, please go <a href="http://healthandwelfare.idaho.gov/FoodCashAssistance/ApplyforAssistance/Forms/tabid/1568/Default.aspx" title="Common Assistance Forms" target="_blank">here</a> for a list of documents that can be used as verification.</p>

			<h4>How do I change the information in my application when it's still pending a determination?</h4>
			<p>You cannot change your application online while it is pending a determination. Please call 1-877-456-1233 or 208-334-6700, between the hours of 8 a.m. and 6 p.m. Mountain Time, Monday thru Friday, except holidays, or email mybenefits@dhw.idaho.gov for assistance.</p>

			<h4>What happens after I submit my HCA application?</h4>
			<p>After submitting your application, you will receive an official notice of your eligibility for Health Coverage Assistance or a request for additional verification.  You can expect to receive this notice within three to five business days.</p>


		</div>

		<h3 class="help-topic">Understanding Your Benefits</h3>

		<div class="help-questions">
			<h4>How do I begin receiving services with Medicaid?</h4>
			<p>IDHW will send you a notification in the mail that details the type of Medicaid that you and your household members are eligible for, as well as next steps to begin using this service.</p>

			<h4>How do I shop and enroll in a health plan using my APTC?</h4>
			<p>If you are eligible for APTC, you will receive an email detailing the next steps for shopping and enrolling in a plan.</p>

			<h4>How can household members have different types of Medicaid coverage?</h4>
			<p>Eligibility for Medicaid is determined for each individual, using information for each household member, such as citizenship/immigration status, income, and resources.</p>

			<h4>What type of services does Medicaid cover?</h4>
			<p>Please contact Molina for all Medicaid coverage questions at 1-866-686-4752 or go online to <a href="http://www.idmedicaid.com/default.aspx" target="_blank">www.idmedicaid.com</a>.</p>
		</div>
	</div>
	
</section>

<div class="reEvalFormOverlay hidden"></div>




</body>
</html>

</fmt:bundle>

