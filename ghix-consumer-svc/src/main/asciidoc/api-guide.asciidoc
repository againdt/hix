= Get Insured Eligibility Services API Guide
Dev Team;
:doctype: book
:toc:
:sectanchors:
:sectlinks:
:toclevels: 4
:source-highlighter: highlightjs

[[overview]]
= Overview

[[overview-http-verbs]]
== HTTP verbs

RESTful notes tries to adhere as closely as possible to standard HTTP and REST conventions in its
use of HTTP verbs.

|===
| Verb | Usage


| `GET`
| Used to retrieve a resource

| `POST`
| Used to create a new resource

| `PATCH`
| Used to update an existing resource, including partial updates

| `DELETE`
| Used to delete an existing resource
|===

[[overview-http-status-codes]]
== HTTP status codes

RESTful notes tries to adhere as closely as possible to standard HTTP and REST conventions in its
use of HTTP status codes.

|===
| Status code | Usage

| `200 OK`
| The request completed successfully

| `201 Created`
| A new resource has been created successfully. The resource's URI is available from the response's
`Location` header

| `204 No Content`
| An update to an existing resource has been applied successfully

| `400 Bad Request`
| The request was malformed. The response body will include an error providing further information

| `404 Not Found`
| The requested resource did not exist
|===

[[overview-errors]]
== Errors

Whenever an error response (status code >= 400) is returned, the body will contain a JSON object
that describes the problem. The error object has the following fields:

|===
| Field | Description

| error
| The HTTP error that occurred, e.g. `Bad Request`

| message
| A description of the cause of the error

| path
| The path to which the request was made

| status
| The HTTP status code, e.g. `400`

| timestamp
| The time, in milliseconds, at which the error occurred
|===


[[overview-hypermedia]]
== Hypermedia

RESTful Notes uses hypermedia and resources include links to other resources in their
responses. Responses are in http://stateless.co/hal_specification.html[Hypertext Application
Language (HAL)] format. Links can be found benath the `_links` key. Users of the API should
not created URIs themselves, instead they should use the above-described links to navigate
from resource to resource.


=== {counter:index}. Get Drug Details

`GET` Method - Returns drug details for search criteria

author : Not Provided

Resource URL

link: [/consumer/drugDetails/{name}]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Find Household By Id

`GET` Method - Returns household for the id

author : Not Provided

Resource URL

link: [/consumer/findHouseholdById]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Get Consumer Call Log History

`POST` Method - Will return call log history for consumer

author : Not Provided

Resource URL

link: [/consumer/getConsumerCallLogHistory]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Search Application Events

`POST` Method - Will return application events for the search criteria

author : Not Provided

Resource URL

link: [/consumer/searchappevents]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Find Member By Id

`POST` Method - Returns member for the id

author : Not Provided

Resource URL

link: [/consumer/findMemberById/{id}]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Manage Applicants

`POST` Method - Will return applicants for the search criteria

author : Not Provided

Resource URL

link: [/consumer/manageapplicants]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Manage Consumers

`POST` Method - Will return consumers for the search criteria

author : Not Provided

Resource URL

link: [/consumer/manageConsumers]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Manage Leads

`POST` Method - Will return leads for the search criteria

author : Not Provided

Resource URL

link: [/consumer/manageLeads]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Find Household By User Id

`POST` Method - Returns household for the user id

author : Not Provided

Resource URL

link: [/consumer/findHouseholdByUserId]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Find Household By User Id

`POST` Method - Returns household for the user id

author : Not Provided

Resource URL

link: [/consumer/findHouseholdRecordByUserId]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Find Members By Household

`POST` Method - Will return membersv for the household

author : Not Provided

Resource URL

link: [/consumer/findMembersByHousehold]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Find Household By EligLead Id

`POST` Method - Will return household for lead id

author : Not Provided

Resource URL

link: [/consumer/findHouseholdByEligLeadId]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Find Household By GIHousehold Id

`POST` Method - Will return household for GI Household Id

author : Not Provided

Resource URL

link: [/consumer/findHouseholdByGIHouseholdId/{id}]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Save Household Information

`GET` Method - Saves household informations

author : Not Provided

Resource URL

link: [/consumer/saveHouseholdInformation]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Find Prescriptions By Household Id

`POST` Method - Returns Prescription for the household id

author : Not Provided

Resource URL

link: [/consumer/findPrescriptionById]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Find Prescriptions By Household

`POST` Method - Returns Prescription for the household

author : Not Provided

Resource URL

link: [/consumer/findPrescriptionsByHousehold]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Find Prescriptions By Name

`POST` Method - Returns Prescription for passed prescription name

author : Not Provided

Resource URL

link: [/consumer/findPrescriptionByName]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Save Prescriptions

`POST` Method - Will save list of prescriptions passed

author : Not Provided

Resource URL

link: [/consumer/savePrescriptions]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Save Prescription

`POST` Method - Will save prescription passed

author : Not Provided

Resource URL

link: [/consumer/savePrescription]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Delete Prescription By Id

`POST` Method - Will delete prescription by id

author : Not Provided

Resource URL

link: [/consumer/deletePrescriptionById]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Delete Prescription

`POST` Method - Will delete prescription

author : Not Provided

Resource URL

link: [/consumer/deletePrescription]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Save Household Enrollment

`POST` Method - Returns saved household enrollment

author : Not Provided

Resource URL

link: [/consumer/saveHouseholdEnrollment]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Save State In Household

`POST` Method - Will save state and return the household

author : Not Provided

Resource URL

link: [/consumer/saveStateInHousehold]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Get Household List

`POST` Method - Returns Households

author : Not Provided

Resource URL

link: [/consumer/getHouseholdList]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Get Link Application Data

`POST` Method - Returns link application data

author : Not Provided

Resource URL

link: [/consumer/getlinkappdata]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Check If Email Exists

`POST` Method - Returns true if email id already exists. Else false

author : Not Provided

Resource URL

link: [/consumer/checkIfEmailExists]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Check Exact Household Exists

`POST` Method - Returns true if exact household already exists. Else false

author : Not Provided

Resource URL

link: [/consumer/checkExactHouseholdExists]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Save Consumer Event Log

`POST` Method - Returns true if consumer event log is successfully saved. Else false

author : Not Provided

Resource URL

link: [/consumer/saveConsumerEventLog]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Relink Household To User

`POST` Method - Will relink household with another household

author : Not Provided

Resource URL

link: [/consumer/relinkHouseholdToUser]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Member Access Code

`POST` Method - Returns access code for the household

author : Not Provided

Resource URL

link: [/consumer/memeber/accesscode]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Save Household

`POST` Method - Returns household if successfully saved. Else null

author : Not Provided

Resource URL

link: [/consumer/saveHousehold]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Save Call Logs

`POST` Method - Returns consumer call log if successfully saved. Else null

author : Not Provided

Resource URL

link: [/consumer/saveCallLogs]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Save Member

`POST` Method - Returns member if successfully saved. Else null

author : Not Provided

Resource URL

link: [/consumer/saveMember]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Find Prescription By Household Id

`POST` Method - Will return prescription for the household id

author : Not Provided

Resource URL

link: [/consumer/findPrescriptionsByHouseholdId]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Find Household Enrollment By Household Id

`GET` Method - Returns household enrollment for the household id

author : Not Provided

Resource URL

link: [/consumer/findHouseholdEnrollmentByHouseholdId]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Save Consumer Call Log

`POST` Method - Returns true if consumer call log is successfully saved. Else false

author : Not Provided

Resource URL

link: [/consumer/saveConsumerCallLog]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Save Consumer Comment Log

`POST` Method - Returns true if consumer comment log is successfully saved. Else false

author : Not Provided

Resource URL

link: [/consumer/saveConsumerCommentLog]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Save Location

`POST` Method - Returns true if location comment log is successfully saved. Else false

author : Not Provided

Resource URL

link: [/consumer/saveLocation]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Search Drug Name

`GET` Method - Will return drug names matches the criteria

author : Not Provided

Resource URL

link: [/consumer/drugSearch/{name}]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----



=== {counter:index}. Consumer Document Update

`POST` Method - Will update consumer document

author : Not Provided

Resource URL

link: [/consumerDocument/update]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


