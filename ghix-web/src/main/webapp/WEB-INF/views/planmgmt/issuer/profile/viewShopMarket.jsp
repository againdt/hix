<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ page import="com.getinsured.hix.platform.util.PhoneUtil"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="<c:url value="/planmgmt/issuer/account/profile/market/individual/info"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
            <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="pgheader.update.shopMarketProfile"/></li>
        </ul><!--page-breadcrumb ends-->
       <%--  <h1><issuerLogo:view/> ${issuerObj.name}</h1> --%>
       <h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
    </div>
    
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.profile"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ul class="nav nav-list">
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/issuerprofile" />"><spring:message  code="pgheader.issuerProfile"/></a></li>
                    <c:if test="${STATE_CODE != 'ID' && STATE_CODE != 'MN'}">
                    	<li><a href="<c:url value="/planmgmt/issuer/account/profile/financial/info/list" />"><spring:message  code="pgheader.financialInformation"/></a></li>
                    </c:if>
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/company/info" />"><spring:message  code="pgheader.update.companyProfile"/></a></li>
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/market/individual/info" />"><spring:message  code="pgheader.update.inidividualMarketProfile"/></a></li>
                     <c:if test="${STATE_CODE != 'ID'}">
                    <li  class="active"><a href="#"><spring:message  code="pgheader.update.shopMarketProfile"/></a></li>
                    </c:if>
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/accreditationdocument/view"/>"><spring:message  code="pgheader.accreditationDocuments"/></a></li>
                    <li><a href="<c:url value="/planmgmt/issuer/certification/status" />"><spring:message  code="pgheader.certificationStatus"/></a></li>
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/viewhistory" />"><spring:message  code="pgheader.issuerHistory"/></a></li>
                    <li><a href="<c:url value="/planmgmt/issuer/crossWalkStatus" />"><spring:message  code="pgheader.planIdCrosswalk"/></a></li>
                    <c:if test="${STATE_CODE == 'ID'}">
                    	<li><a href="<c:url value="/planmgmt/issuer/displayNetworkTransparencyData" />"><spring:message code="pgheader.networkTransparencyStatus"/></a></li>
                    </c:if>
                </ul>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header"> 
                    <h4 class="pull-left">SHOP Market Profile </h4>
                    <a class="btn btn-small pull-right" href="<c:url value="/planmgmt/issuer/account/profile/market/shop/edit"/>"><spring:message  code="label.edit"/></a>
                </div>
		
				<form class="form-horizontal" id="frmIssuerShopMarktProfInfo" name="frmIssuerShopMarktProfInfo" action="" method="POST">
				
				<table class="table table-border-none">
					<tbody>
						<tr>
							<td class="txt-right span5">
								<spring:message  code="label.customerServicePhone"/>
							</td>
							<td>
								<c:set var="custServPhone" value="${issuerObj.shopCustServicePhone}"/>
								<strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custServPhone"))%></strong>  
								&nbsp; &nbsp;
								<spring:message  code="label.phone.ext"/>&nbsp;
								<strong>${issuerObj.shopCustServicePhoneExt}</strong>
							</td>
						</tr>
						<tr>
							<td class="txt-right">
								<spring:message  code="label.customerServiceTollFreeNumber"/>
							</td>
							<td>
								<c:set var="custTollFree" value="${issuerObj.shopCustServiceTollFree}"/>
								<strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custTollFree"))%></strong>
							</td>
						</tr>
						<tr>
							<td class="txt-right">
								<spring:message  code="label.customerServiceTTY"/>
							</td>
							<td>
								<c:set var="custServTTY" value="${issuerObj.shopCustServiceTTY}"/>
								<strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custServTTY"))%></strong>
							</td>
						</tr>
						<tr>
							<td class="txt-right">
								<spring:message  code="label.consumerFacingWebSiteURL"/>
							</td>
							<td>
								<a href="${issuerObj.shopSiteUrl}" target="_blank"><strong>${issuerObj.shopSiteUrl}</strong></a>
							</td>
						</tr>
					</tbody>
				</table>			
				
				</form>
			
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
		</div>
<script type="text/javascript">
	$(document).ready(function(){
		$($.find('a[title="Account"]')).parent().addClass('active');	
	});
</script>

