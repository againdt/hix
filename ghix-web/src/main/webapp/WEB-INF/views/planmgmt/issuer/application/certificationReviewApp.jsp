<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<% Integer pageSizeInt = (Integer)request.getAttribute("pageSize"); %> 
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
	<div class="gutter10">
		<!-- start of secondary navbar -->
		<ul class="nav nav-tabs">
		  	 <li class="active"><a href="#"><spring:message code="label.issuerCertification"/></a></li>    
		</ul>
		<!-- end of secondary navbar -->
	</div>
   
	<div class="row-fluid">
        <div class="span3" id="sidebar">
        	
	        <!--  beginning of side bar -->
	            <ul class="nav nav-list">
	                <li class="nav-header"><spring:message code="label.issuerCertification"/></li>
	                <li><a href="#"><spring:message code="label.authorizeRepresentatives"/></a></li>		                
	                <li><a href="#"><spring:message code="label.submitIssuerApplication"/></a></li>
	                <li class="active"><spring:message code="label.reviewAndConfirm"/></li>                          
	              </ul>
	        <!-- end of side bar -->
        	
        </div><!-- end of span3 -->
        <div class="span9" id="rightpanel">
			<form class="form-vertical gutter10" id="frmbrokerreg" name="frmbrokerreg" action="<c:url value="/planmgmt/issuerreviewapp" />" method="POST">
			<input type="hidden" name="issuerappreviewed" id="issuerappreviewed" value="yes">
			<df:csrfToken/>
		<div class="profile">

	<div class="page-header">
		<h1><spring:message  code="label.pleaseReviewSubmit"/></h1>
	</div><!-- end of page-header -->

	<div class="row-fluid"> 
		<p><spring:message  code="label.attestedDocument"/></p>
		<table class="table">
			<thead>
				<tr class="header">
					<td>1. <spring:message  code="label.issuerUploadAuthority"/></td>
					<td>
						<c:forTokens items="${authorityFile}" delims="," var="authorityFileName">
							<li type="square"> <c:out value="${authorityFileName}"/></li>
						</c:forTokens>
					</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>2. <spring:message  code="label.issuerUploadMarketingReq"/></td>
					<td>
						<c:forTokens items="${marketingFile}" delims="," var="marketingFileName">
							<li type="square"><c:out value="${marketingFileName}"/></li>
						</c:forTokens>
					</td>
				</tr>
				<tr>
					<td>3. <spring:message  code="label.issuerUploadDisclosures"/></td>
					<td>
						<c:forTokens items="${disclosuresFile}" delims="," var="disclosuresFileName">
							<li type="square"> <c:out value="${disclosuresFileName}"/></li>
						</c:forTokens>
					</td>
				</tr>
				<tr>
					<td>4. <spring:message  code="label.issuerUploadAccreditation"/></td>
					<td>
						<c:forTokens items="${accreditationFile}" delims="," var="accreditationFileName">
							<li type="square"><c:out value="${accreditationFileName}"/></li>
						</c:forTokens>
					</td>
				</tr> 
				<tr>
					<td>5. <spring:message  code="label.issuerUploadAdditionalInfo"/></td>
					<td>
						<c:forTokens items="${addInfoFile}" delims="," var="addInfoFileName">
							<li type="square"> <c:out value="${addInfoFileName}"/></li>
						</c:forTokens>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div><!-- end of profile -->

<div class="form-actions">
   <div class="control-group">
     <div class="controls">
       <label class="checkbox">
          <input type="checkbox" value="option1" id="optionsCheckbox" onclick="updatedSubmit()">
		  <spring:message code="label.issuerAttest"/>
       </label>
     </div>
   </div>
<a href="<c:url value="/planmgmt/issuerapplication"/>" class="btn btn-primary"><spring:message code="label.edit"/></a>
<input type="submit" name="submit" id="submit" value="<spring:message  code='label.issuerSubmit'/>" class="btn btn-primary" title="<spring:message  code='label.issuerSubmit'/>" onclick="formSubmit();" disabled="disabled"/> 
</div><!-- end of form-actions -->
    
</form>
						          					
</div><!-- end of span9 -->
</div><!--  end of row-fluid -->

<script type="text/javascript">
function  updatedSubmit() {
	if($("#optionsCheckbox").is(":checked")) {
		$("#submit").removeAttr("disabled");
	} else {
		$("#submit").attr("disabled", "disabled");
	}
}
function  formSubmit() {
	if($("#optionsCheckbox").is(":checked")) {
		$("#frmbrokerreg").submit();
	} else {
		alert ("<spring:message code='label.checkBoxInformation'/>");
	}
	
	return false;
}
</script>
