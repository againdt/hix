package com.getinsured.eligibility.at.ref.service.migration;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.resp.si.handler.AptcCsrCalculatorResponse;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

/**
 * ExchangeBasedAptcCsrCalculatorHandler
 * @author Ekram
 *
 */
@Component
public class ExchangeBasedAptcCsrCalculatorMigrationHandler{

	private static final String Y = "Y";

	private static final String INITIATE_APTC_CALCULATION = "/initiateAPTCCalculation";

	private static final String EXCHANGE_BASED_APTC_CSR_CALCULATOR_RESULT = "EXCHANGE_BASED_APTC_CSR_CALCULATOR_RESULT";

	private static final Logger LOGGER = Logger.getLogger(ExchangeBasedAptcCsrCalculatorMigrationHandler.class);

	@Value("#{configProp['ghixEligibilityServiceURL']}")
	private String erpSvcUrl;

	@Autowired private SsapApplicationRepository ssapApplicationRepository;

	@Autowired private RestTemplate restTemplate;
	@Autowired
	private MigrationUtil migrationUtil;

	public String processERP(Message<ERPResponse> message){

		ERPResponse erpResponse = (ERPResponse) message.getHeaders().get(EligibilityConstants.ERP_RESPONSE);

		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put(EligibilityConstants.SSAP_APPLICATION_ID, erpResponse.getApplicationID());

		try {
			boolean flag = calculateAptcCsr(erpResponse.getApplicationID());

			if (flag){
				resultMap.put(EXCHANGE_BASED_APTC_CSR_CALCULATOR_RESULT, EligibilityConstants.AUTO);
			} else {
				resultMap.put(EXCHANGE_BASED_APTC_CSR_CALCULATOR_RESULT, EligibilityConstants.MANUAL);
			}

		} catch (Exception e) {

			StringBuilder errorReason = new StringBuilder().append(EligibilityConstants.ERROR_PROCESSING_ERP_REQUEST_FOR_SSAP_APPLICATION_ID).
					append(erpResponse.getApplicationID()).append(EligibilityConstants.REASON).append(e.getMessage());

			LOGGER.error(errorReason.toString());
			resultMap.put(EXCHANGE_BASED_APTC_CSR_CALCULATOR_RESULT, EligibilityConstants.ERROR);
			resultMap.put(EligibilityConstants.ERROR_REASON, errorReason.toString());
			migrationUtil.persistGiMonitorId(erpResponse.getSsapApplicationPrimaryKey(), e);
		} finally {
			resultMap.put(EligibilityConstants.ERP_RESPONSE, erpResponse);
		}

		return EligibilityUtils.marshal(resultMap);

	}

	private boolean isAPTCorAPTC_CSREligible(SsapApplication currentApplication) {
		return currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC || currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC_CSR;
		/*return currentApplication.getEligibilityStatus() == EligibilityStatus.AE || currentApplication.getEligibilityStatus() == EligibilityStatus.AX
				|| currentApplication.getEligibilityStatus() == EligibilityStatus.CAE || currentApplication.getEligibilityStatus() == EligibilityStatus.CAX;*/
	}


	private boolean calculateAptcCsr(String caseNumber) {

		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumber(caseNumber);
		if (ssapApplicationList.isEmpty()){
			throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES);
		}
		SsapApplication currentApplication = ssapApplicationList.get(0);

		if (!isAPTCorAPTC_CSREligible(currentApplication)){
			return true;
		}

		// Check global configuration to invoke APTC Calculation API
		String isExchangeBasedAptcCsrCalculationString = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_BASED_APTC_CSR_CALCULATION);

		if (!(Y.equalsIgnoreCase(isExchangeBasedAptcCsrCalculationString))){
			LOGGER.info("No Exchange based APTC/CSR Calculation required!");
			return true;
		}

		//Invoke APTC-Calculator to get max APTC and available APTC amount
		AptcCsrCalculatorResponse aptcCsrCalculatorResponse = restTemplate.postForObject(erpSvcUrl + INITIATE_APTC_CALCULATION, currentApplication.getApplicationData(), AptcCsrCalculatorResponse.class);

		//check maximumAPTC < electedAPTCAmount then update electedAPTCAmount..
		compareAndModifyElectedAPTCAmount(currentApplication, aptcCsrCalculatorResponse.getMaxAPTCAmount());

		//update SSAP_APPLICATION with received max APTC and available APTC amount
		currentApplication.setMaximumAPTC(aptcCsrCalculatorResponse.getMaxAPTCAmount());
		currentApplication.setAvailableAPTC(aptcCsrCalculatorResponse.getAvailableAPTCAmount());
		if (currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC_CSR){
			currentApplication.setCsrLevel(aptcCsrCalculatorResponse.getCsrLevel());
		}
		ssapApplicationRepository.save(currentApplication);

		return true;

	}


	private void compareAndModifyElectedAPTCAmount(SsapApplication ssapApplication, BigDecimal maxAPTCAmount) {

		BigDecimal electedAPTCAmount = ssapApplication.getElectedAPTC();

		if (maxAPTCAmount != null && electedAPTCAmount != null && electedAPTCAmount.compareTo(maxAPTCAmount) > 0) {
			LOGGER.info("Elected APTC Amount is greater than Maximum APTC Amount. Set Elected APTC Amount to Maximum APTC Amount.");
			ssapApplication.setElectedAPTC(maxAPTCAmount);
		}
	}

}
