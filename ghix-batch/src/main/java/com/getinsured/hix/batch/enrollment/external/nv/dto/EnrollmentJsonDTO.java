package com.getinsured.hix.batch.enrollment.external.nv.dto;

import java.math.BigDecimal;
import java.util.List;

public class EnrollmentJsonDTO {
		 private boolean isActive;
		 private int policyTrackingNumber;
		 private String associatedProductDivisionReferenceTypeCodeName;
		 private String selectedInsurancePlan;
		 private InsurancePolicyStatus insurancePolicyStatus;
		 private boolean supersededIndicator;
		 private String specifiedEOYEndDateIndicator;
		 private String insurancePlanPolicyEndDate;
		 private BigDecimal insuranceApplicationIdentifier;
		 private String definingPlanVariantComponentTypeCodeName;
		 private int marketplaceGroupPolicyIdentifier;
		 
		 /*
		 private String documentType;
		 private int numberOfPoliciesTiedToApplication;
		 private ArrayList<Integer> marketplaceGroupPolicyIdentifiersTiedToApplication = new ArrayList <Integer>();
		 private boolean partialDataIndicator;
		 private String lastModifiedDateTime;
		 private String lastModifiedBy;
		 private boolean isCurrentVersion;
		 private int versionNumber;
		 private String versionDateTime;
		 private int marketplaceGroupPolicyIdentifier;
		 private int informingApplicationVersionNumber;
		 private String applicationSEPTypeName;
		 private String batchCorrelationIdentifier;
		 private String initiatingTransactionOriginReferenceTypeCodeName;
		 private BigDecimal benchmarkPolicyPremiumAmount;
		 private BigDecimal maxAPTCAmount;
		 private int issuerHIOSID;
		 private String issuerName;
		 private String initiatingExchangeUser;
		 private String initiatingExchangeUserIdentifier;
		 private String issuerAssignedSubscriberIdentifier;
		 private DefinedAssistor definedAssistor;
		 private DirectEnrollmentPartner directEnrollmentPartner;
		 private ApplicableCostSharingReduction applicableCostSharingReduction;
		 private String referencedPaymentTransactionIdentifier;
		 private String insurancePlanVariantName;
		 private String insurancePlanPolicyStartDate;
		 private int coverageYear;
		 private String insurancePlanPolicyCreationDateTime;
		 private InsurancePolicyPremium insurancePolicyPremium;
		 private String premiumPaidToEndDate;
		 
		 
		 private boolean issuerConfirmationIndicator;
		 private String associatedMetalTierTypeCodeName;
		  * */
		 private List<CoveredInsuredMembers> coveredInsuredMembers;

		 
		 public boolean getIsActive() {
			 return isActive;
		 }

		public void setCoveredInsuredMembers(List<CoveredInsuredMembers> coveredInsuredMembersObject) {
			this.coveredInsuredMembers = coveredInsuredMembersObject;
		}
		public void setSelectedInsurancePlan(String selectedInsurancePlan) {
			this.selectedInsurancePlan = selectedInsurancePlan;
		}
		
		public void setAssociatedProductDivisionReferenceTypeCodeName(String associatedProductDivisionReferenceTypeCodeName) {
			this.associatedProductDivisionReferenceTypeCodeName = associatedProductDivisionReferenceTypeCodeName;
		}
		
		public void setPolicyTrackingNumber(int policyTrackingNumber) {
			this.policyTrackingNumber = policyTrackingNumber;
		}
		
		public void setIsActive(boolean isActive) {
			this.isActive = isActive;
		}
		public List<CoveredInsuredMembers> getCoveredInsuredMembers() {
			return coveredInsuredMembers;
		}
		public String getSelectedInsurancePlan() {
			return selectedInsurancePlan;
		}
		public String getAssociatedProductDivisionReferenceTypeCodeName() {
			return associatedProductDivisionReferenceTypeCodeName;
		}
		
		public int getPolicyTrackingNumber() {
			return policyTrackingNumber;
		}

		 public InsurancePolicyStatus getInsurancePolicyStatus() {
		  return insurancePolicyStatus;
		 }

		 public void setInsurancePolicyStatus(InsurancePolicyStatus insurancePolicyStatus) {
		  this.insurancePolicyStatus = insurancePolicyStatus;
		 }


		 public void setSupersededIndicator(boolean supersededIndicator) {
		  this.supersededIndicator = supersededIndicator;
		 }
		 
		 public boolean getSupersededIndicator() {
			  return supersededIndicator;
		}

		 public String getSpecifiedEOYEndDateIndicator() {
			  return specifiedEOYEndDateIndicator;
		 }
		 public void setSpecifiedEOYEndDateIndicator(String specifiedEOYEndDateIndicator) {
		  this.specifiedEOYEndDateIndicator = specifiedEOYEndDateIndicator;
		 }


		 public String getInsurancePlanPolicyEndDate() {
		  return insurancePlanPolicyEndDate;
		 }

		 public void setInsurancePlanPolicyEndDate(String insurancePlanPolicyEndDate) {
		  this.insurancePlanPolicyEndDate = insurancePlanPolicyEndDate;
		 }
		 
		 public BigDecimal getInsuranceApplicationIdentifier() {
			 return insuranceApplicationIdentifier;
		 }
		 
		 public void setInsuranceApplicationIdentifier(BigDecimal insuranceApplicationIdentifier) {
			 this.insuranceApplicationIdentifier = insuranceApplicationIdentifier;
		 }

		 public String getDefiningPlanVariantComponentTypeCodeName() {
		  return definingPlanVariantComponentTypeCodeName;
		 }
		 
		 public void setDefiningPlanVariantComponentTypeCodeName(String definingPlanVariantComponentTypeCodeName) {
			  this.definingPlanVariantComponentTypeCodeName = definingPlanVariantComponentTypeCodeName;
		 }

		public int getMarketplaceGroupPolicyIdentifier() {
			return marketplaceGroupPolicyIdentifier;
		}

		public void setMarketplaceGroupPolicyIdentifier(int marketplaceGroupPolicyIdentifier) {
			this.marketplaceGroupPolicyIdentifier = marketplaceGroupPolicyIdentifier;
		}
			
		 // Getter Methods 

		 /*public String getDocumentType() {
		  return documentType;
		 }

		 public float getNumberOfPoliciesTiedToApplication() {
		  return numberOfPoliciesTiedToApplication;
		 }

		 public boolean getPartialDataIndicator() {
		  return partialDataIndicator;
		 }

		 public String getLastModifiedDateTime() {
		  return lastModifiedDateTime;
		 }

		 public String getLastModifiedBy() {
		  return lastModifiedBy;
		 }

		 public boolean getIsCurrentVersion() {
		  return isCurrentVersion;
		 }

		 public float getVersionNumber() {
		  return versionNumber;
		 }

		 public String getVersionDateTime() {
		  return versionDateTime;
		 }

		 public float getMarketplaceGroupPolicyIdentifier() {
		  return marketplaceGroupPolicyIdentifier;
		 }


		 public float getInformingApplicationVersionNumber() {
		  return informingApplicationVersionNumber;
		 }

		 public boolean getSupersededIndicator() {
		  return supersededIndicator;
		 }

		 public String getApplicationSEPTypeName() {
		  return applicationSEPTypeName;
		 }

		 public String getBatchCorrelationIdentifier() {
		  return batchCorrelationIdentifier;
		 }

		 public String getInitiatingTransactionOriginReferenceTypeCodeName() {
		  return initiatingTransactionOriginReferenceTypeCodeName;
		 }

		 public BigDecimal getBenchmarkPolicyPremiumAmount() {
		  return benchmarkPolicyPremiumAmount;
		 }

		 public BigDecimal getMaxAPTCAmount() {
		  return maxAPTCAmount;
		 }

		 public float getIssuerHIOSID() {
		  return issuerHIOSID;
		 }

		 public String getIssuerName() {
		  return issuerName;
		 }

		 public String getInitiatingExchangeUser() {
		  return initiatingExchangeUser;
		 }

		 public String getInitiatingExchangeUserIdentifier() {
		  return initiatingExchangeUserIdentifier;
		 }

		 public String getIssuerAssignedSubscriberIdentifier() {
		  return issuerAssignedSubscriberIdentifier;
		 }

		 public DefinedAssistor getDefinedAssistor() {
		  return definedAssistor;
		 }

		 public DirectEnrollmentPartner getDirectEnrollmentPartner() {
		  return directEnrollmentPartner;
		 }

		 public ApplicableCostSharingReduction getApplicableCostSharingReduction() {
		  return applicableCostSharingReduction;
		 }

		 public String getReferencedPaymentTransactionIdentifier() {
		  return referencedPaymentTransactionIdentifier;
		 }


		 public String getInsurancePlanVariantName() {
		  return insurancePlanVariantName;
		 }

		 public String getInsurancePlanPolicyStartDate() {
		  return insurancePlanPolicyStartDate;
		 }

		 public String getInsurancePlanPolicyEndDate() {
		  return insurancePlanPolicyEndDate;
		 }

		 public float getCoverageYear() {
		  return coverageYear;
		 }

		 public String getInsurancePlanPolicyCreationDateTime() {
		  return insurancePlanPolicyCreationDateTime;
		 }

		 public InsurancePolicyPremium getInsurancePolicyPremium() {
		  return insurancePolicyPremium;
		 }

		 public String getPremiumPaidToEndDate() {
		  return premiumPaidToEndDate;
		 }

		 public boolean getIssuerConfirmationIndicator() {
		  return issuerConfirmationIndicator;
		 }

		 public String getAssociatedMetalTierTypeCodeName() {
		  return associatedMetalTierTypeCodeName;
		 }


		 // Setter Methods 

		 public void setDocumentType(String documentType) {
		  this.documentType = documentType;
		 }

		 public void setNumberOfPoliciesTiedToApplication(int numberOfPoliciesTiedToApplication) {
		  this.numberOfPoliciesTiedToApplication = numberOfPoliciesTiedToApplication;
		 }


		 public void setPartialDataIndicator(boolean partialDataIndicator) {
		  this.partialDataIndicator = partialDataIndicator;
		 }

		 public void setLastModifiedDateTime(String lastModifiedDateTime) {
		  this.lastModifiedDateTime = lastModifiedDateTime;
		 }

		 public void setLastModifiedBy(String lastModifiedBy) {
		  this.lastModifiedBy = lastModifiedBy;
		 }

		 public void setIsCurrentVersion(boolean isCurrentVersion) {
		  this.isCurrentVersion = isCurrentVersion;
		 }

		 public void setVersionNumber(int versionNumber) {
		  this.versionNumber = versionNumber;
		 }

		 public void setVersionDateTime(String versionDateTime) {
		  this.versionDateTime = versionDateTime;
		 }

		 public void setMarketplaceGroupPolicyIdentifier(int marketplaceGroupPolicyIdentifier) {
		  this.marketplaceGroupPolicyIdentifier = marketplaceGroupPolicyIdentifier;
		 }

		 public void setInformingApplicationVersionNumber(int informingApplicationVersionNumber) {
		  this.informingApplicationVersionNumber = informingApplicationVersionNumber;
		 }

		 public void setSupersededIndicator(boolean supersededIndicator) {
		  this.supersededIndicator = supersededIndicator;
		 }

		 public void setApplicationSEPTypeName(String applicationSEPTypeName) {
		  this.applicationSEPTypeName = applicationSEPTypeName;
		 }

		 public void setBatchCorrelationIdentifier(String batchCorrelationIdentifier) {
		  this.batchCorrelationIdentifier = batchCorrelationIdentifier;
		 }

		 public void setInitiatingTransactionOriginReferenceTypeCodeName(String initiatingTransactionOriginReferenceTypeCodeName) {
		  this.initiatingTransactionOriginReferenceTypeCodeName = initiatingTransactionOriginReferenceTypeCodeName;
		 }

		 public void setBenchmarkPolicyPremiumAmount(BigDecimal benchmarkPolicyPremiumAmount) {
		  this.benchmarkPolicyPremiumAmount = benchmarkPolicyPremiumAmount;
		 }

		 public void setMaxAPTCAmount(BigDecimal maxAPTCAmount) {
		  this.maxAPTCAmount = maxAPTCAmount;
		 }

		 public void setIssuerHIOSID(int issuerHIOSID) {
		  this.issuerHIOSID = issuerHIOSID;
		 }

		 public void setIssuerName(String issuerName) {
		  this.issuerName = issuerName;
		 }

		 public void setInitiatingExchangeUser(String initiatingExchangeUser) {
		  this.initiatingExchangeUser = initiatingExchangeUser;
		 }

		 public void setInitiatingExchangeUserIdentifier(String initiatingExchangeUserIdentifier) {
		  this.initiatingExchangeUserIdentifier = initiatingExchangeUserIdentifier;
		 }

		 public void setIssuerAssignedSubscriberIdentifier(String issuerAssignedSubscriberIdentifier) {
		  this.issuerAssignedSubscriberIdentifier = issuerAssignedSubscriberIdentifier;
		 }

		 public void setDefinedAssistor(DefinedAssistor definedAssistor) {
		  this.definedAssistor = definedAssistor;
		 }

		 public void setDirectEnrollmentPartner(DirectEnrollmentPartner directEnrollmentPartner) {
		  this.directEnrollmentPartner = directEnrollmentPartner;
		 }

		 public void setApplicableCostSharingReduction(ApplicableCostSharingReduction applicableCostSharingReduction) {
		  this.applicableCostSharingReduction = applicableCostSharingReduction;
		 }

		 public void setReferencedPaymentTransactionIdentifier(String referencedPaymentTransactionIdentifier) {
		  this.referencedPaymentTransactionIdentifier = referencedPaymentTransactionIdentifier;
		 }

		 public void setInsurancePlanVariantName(String insurancePlanVariantName) {
		  this.insurancePlanVariantName = insurancePlanVariantName;
		 }

		 public void setInsurancePlanPolicyStartDate(String insurancePlanPolicyStartDate) {
		  this.insurancePlanPolicyStartDate = insurancePlanPolicyStartDate;
		 }

		 public void setInsurancePlanPolicyEndDate(String insurancePlanPolicyEndDate) {
		  this.insurancePlanPolicyEndDate = insurancePlanPolicyEndDate;
		 }

		 public void setCoverageYear(int coverageYear) {
		  this.coverageYear = coverageYear;
		 }

		 public void setInsurancePlanPolicyCreationDateTime(String insurancePlanPolicyCreationDateTime) {
		  this.insurancePlanPolicyCreationDateTime = insurancePlanPolicyCreationDateTime;
		 }

		 public void setInsurancePolicyStatus(InsurancePolicyStatus insurancePolicyStatus) {
		  this.insurancePolicyStatus = insurancePolicyStatus;
		 }

		 public void setInsurancePolicyPremium(InsurancePolicyPremium insurancePolicyPremium) {
		  this.insurancePolicyPremium = insurancePolicyPremium;
		 }

		 public void setPremiumPaidToEndDate(String premiumPaidToEndDate) {
		  this.premiumPaidToEndDate = premiumPaidToEndDate;
		 }


		 

		 public void setIssuerConfirmationIndicator(boolean issuerConfirmationIndicator) {
		  this.issuerConfirmationIndicator = issuerConfirmationIndicator;
		 }

		 public void setAssociatedMetalTierTypeCodeName(String associatedMetalTierTypeCodeName) {
		  this.associatedMetalTierTypeCodeName = associatedMetalTierTypeCodeName;
		 }*/


		/*@Override
		public String toString() {
			return "EnrollmentJsonDTO [documentType=" + documentType + ", numberOfPoliciesTiedToApplication="
					+ numberOfPoliciesTiedToApplication + ", marketplaceGroupPolicyIdentifiersTiedToApplication="
					+ marketplaceGroupPolicyIdentifiersTiedToApplication + ", isActive=" + isActive
					+ ", partialDataIndicator=" + partialDataIndicator + ", lastModifiedDateTime="
					+ lastModifiedDateTime + ", lastModifiedBy=" + lastModifiedBy + ", isCurrentVersion="
					+ isCurrentVersion + ", versionNumber=" + versionNumber + ", versionDateTime=" + versionDateTime
					+ ", marketplaceGroupPolicyIdentifier=" + marketplaceGroupPolicyIdentifier
					+ ", policyTrackingNumber=" + policyTrackingNumber + ", informingApplicationVersionNumber="
					+ informingApplicationVersionNumber + ", supersededIndicator=" + supersededIndicator
					+ ", applicationSEPTypeName=" + applicationSEPTypeName + ", batchCorrelationIdentifier="
					+ batchCorrelationIdentifier + ", associatedProductDivisionReferenceTypeCodeName="
					+ associatedProductDivisionReferenceTypeCodeName
					+ ", initiatingTransactionOriginReferenceTypeCodeName="
					+ initiatingTransactionOriginReferenceTypeCodeName + ", benchmarkPolicyPremiumAmount="
					+ benchmarkPolicyPremiumAmount + ", maxAPTCAmount=" + maxAPTCAmount + ", issuerHIOSID="
					+ issuerHIOSID + ", issuerName=" + issuerName + ", initiatingExchangeUser=" + initiatingExchangeUser
					+ ", initiatingExchangeUserIdentifier=" + initiatingExchangeUserIdentifier
					+ ", issuerAssignedSubscriberIdentifier=" + issuerAssignedSubscriberIdentifier
					+ ", DefinedAssistorObject=" + definedAssistor + ", DirectEnrollmentPartnerObject="
					+ directEnrollmentPartner + ", ApplicableCostSharingReductionObject="
					+ applicableCostSharingReduction + ", referencedPaymentTransactionIdentifier="
					+ referencedPaymentTransactionIdentifier + ", selectedInsurancePlan=" + selectedInsurancePlan
					+ ", insurancePlanVariantName=" + insurancePlanVariantName + ", insurancePlanPolicyStartDate="
					+ insurancePlanPolicyStartDate + ", insurancePlanPolicyEndDate=" + insurancePlanPolicyEndDate
					+ ", coverageYear=" + coverageYear + ", insurancePlanPolicyCreationDateTime="
					+ insurancePlanPolicyCreationDateTime + ", InsurancePolicyStatusObject="
					+ insurancePolicyStatus + ", InsurancePolicyPremiumObject=" + insurancePolicyPremium
					+ ", premiumPaidToEndDate=" + premiumPaidToEndDate + ", definingPlanVariantComponentTypeCodeName="
					+ definingPlanVariantComponentTypeCodeName + ", insuranceApplicationIdentifier="
					+ insuranceApplicationIdentifier + ", issuerConfirmationIndicator=" + issuerConfirmationIndicator
					+ ", associatedMetalTierTypeCodeName=" + associatedMetalTierTypeCodeName
					+ ", CoveredInsuredMembersObject=" + coveredInsuredMembers + "]";
		}*/
}
