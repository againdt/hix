package com.getinsured.hix.entity.web.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.jsoup.helper.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.dto.entity.AssisterSearchParameters;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityResponseDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntitySearchParameters;
import com.getinsured.hix.entity.service.EntityService;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.AssisterLanguages;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.model.entity.PopulationData;
import com.getinsured.hix.model.entity.PopulationEthnicity;
import com.getinsured.hix.model.entity.PopulationIndustry;
import com.getinsured.hix.model.entity.PopulationLanguage;
import com.getinsured.hix.model.entity.PopulationServedWrapper;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.SiteLanguages;
import com.getinsured.hix.model.entity.SiteLocationHours;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;

@SuppressWarnings("unused")
@Controller
public class EntityAdminController {
	
	private static final int MAX_COMMENT_LENGTH = 4000;
	
	private static final String REQUEST_INSTANCE_IS_MISSING_FOR_THE_REQUEST = "Request instance is missing for the request.";

	private static final String GETINSURED_HEALTH_EXCHANGE_ENROLLMENT_ENTITIES_VIEW_ASSISTERS = "Getinsured Health Exchange Enrollment Entities : View Assisters";

	private static final String ASC = "ASC";

	private static final String TO_DATE = "toDate";

	private static final String FROM_DATE = "fromDate";

	private static final String ENTITYSEARCHPARAM = "entitysearchparam";

	private static final String RECORD_COUNT = "recordCount";

	private static final String LIST = "list";

	private static final String REQ_URI = "reqURI";

	private static final String HAS_PERMISSION_MODEL_EEADMIN_VIEW_ENTITY = "hasPermission(#model, 'EEADMIN_VIEW_ENTITY')";

	private static final String ENTITY_ID = "entityId";

	private static final String UPLOADED_DOCUMENT_CHECK = "uploadedDocumentCheck";

	private static final String HAS_PERMISSION_MODEL_EEADMIN_EDIT_ENTITY = "hasPermission(#model, 'EEADMIN_EDIT_ENTITY')";

	private static final String HAS_PERMISSION_MODEL_EEADMIN_MANAGE_ENTITY = "hasPermission(#model, 'EEADMIN_MANAGE_ENTITY')";

	private static final String UPLOADED_DOCUMENT = "uploadedDocument";
	private static final int FILE_SIZE_EXCEED = 5242880;

	private static final int THREE = 3;
	private static final int SIX = 6;
	private static final int TEN = 10;
	private static final int FOUR = 4;
	private static final Logger LOGGER = LoggerFactory.getLogger(EntityAdminController.class);

	private static final String PAGE_TITLE = "page_title";
	private static final String RESULT_SIZE = "resultSize";
	private static final String PAGE_SIZE = "pageSize";
	private static final String ENROLLMENTENTITY = "enrollmentEntity";
	private static final String REGISTRATION_STATUS = "registerStatus";
	private static final String STATUS_HISTORY = "enrollmentRegisterStatusHistory";
	private static final String LIST_OF_ASSISTERS = "assisterList";
	private static final String NAVIGATION_MENU_MAP = "navigationmenumap";
	private static final String ENTITY_SITE_HOURS_TIME = "ENTITY_SITE_HOURS_TIME";
	private static final String HOURSOFOPERATIONFROMTIME = "HOURSOFOPERATION_FROMTIME";
	private static final String HOURSOFOPERATIONTOTIME = "HOURSOFOPERATION_TOTIME";
	private static final String CA_STATECODE = "CA";
	private static final String DAYSOFOPERATION = "DAYSOFOPERATION";
	private static final String DAYSLIST = "daysList";
	private static final String DAYSLISTNAMES = "daysListNames";
	private static final String FROMTIMELIST = "fromTimelist";
	private static final String TOTIMELIST = "toTimelist";
	private static final String STATELIST = "statelist";
	private static final String ENTITYLANGUAGES = "ENTITY_LANGUAGE";
	private static final String LIST_OF_LANGUAGES = "languagelist";
	private static final String ENTITY_LANGUAGE = "ENTITY_LANGUAGE";
	private static final String ACTIVITY_STATUS_HISTORY = "assisterActivityStatusHistory";
	private static final String COUNTIES_SERVED_LIST = "countiesServedlist";
	private static final String ENROLLMENT_ENTITY = "enrollmentEntity";
	private static final String CANCEL = "cancel";
	private static final String UPDATEPROFILE = "updatedProfile";
	private static final String PAYMENT_METHODS_OBJ = "paymentMethodsObj";
	private static final String LIST_OF_STATES = "statelist";
	private static final String LANGUAGES_LOOKUP_LIST = "languagesLookupList";
	private static final String ETHNICITIES_LOOKUP_LIST = "ethnicitiesLookupList";
	private static final String INDUSTRIES_LOOKUP_LIST = "industriesLookupList";
	private static final String POPULATION_SERVED_WRAPPER = "populationServedWrapper";
	public static final String INDUSTRY_LOOKUP_TYPE = "ENTITY_INDUSTRY";
	public static final String PRIMARYSITE = "PRIMARY";
	public static final String SUBSITE = "SUBSITE";
	public static final String ISPRIMARYSITE = "isPrimarySite";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String SITELIST = "siteList";
	public static final String SITENUMBER = "siteNumber";
	public static final String SUBSITENUMBER = "siteNumber";
	public static final String ISEDIT = "isEdit";
	public static final String LISTOFLANGUAGES = "listOflanguages";
	public static final String LISTOFLANGUAGENAMES = "listOflanguageNames";
	public static final String QUOTE = "&quot;";
	private static final String ENTITY_ADMIN = "entityAdmin";
	private static final String LOGGED_USER = "loggedUser";
	private static final String ASSISTER_LANGUAGES = "assisterLanguages";
	private static final String ASSISTERSTATUS = "ASSISTER_CERTIFICATION_STATUS";
	private static final String PAGE_NUMBER = "pageNumber";
	private static final String ASSISTER = "assister";
	private static final String STATUSLIST_MODEL = "statusList";
	private static final String USER_ID = "userId";
	public static final String ASSISTER_ID = "assisterId";
	public static final String RETRIEVING_EE_DETAIL_REST_CALL_STARTS = "Retrieving Enrollment Entity Detail REST Call Starts";
	public static final String RETRIEVING_EE_DETAIL_REST_CALL_ENDS = "Retrieving Enrollment Entity Detail REST Call Ends";
	public static final String ENTITY_REGISTRATION_STATUS = "ENTITY_REGISTRATION_STATUS";
	private static final String STATUSLIST = "statuslist";
	private static final String ORGANIZATIONTYPELIST = "organizationtypelist";
	private static final String RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS = "Retrieving Assister Detail REST Call Starts : AssisterRequestDTO in Request : ";
	private static final String RETRIEVING_ASSISTER_DETAIL_REST_CALL_ENDS = "Retrieving Assister Detail REST Call Ends : Response Received : ";
	private static final String TOTAL_ENROLLMENT_ENTITIES_BY_SEARCH = "totalenrollmententitiesbysearch";
	private static final String TOTAL_ENROLLMENT_ENTITIES = "totalenrollmententities";
	private static final String ENROLLMENT_ENTITIES_UP_FOR_RENEWAL = "enrollmententitiesupforrenewal";
	private static final String INACTIVE_ENROLLMENT_ENTITIES = "inactiveenrollmententities";
	private static final String ADDNEWSUBSITE = "addNewSubSite";
	private static final String LOCATIONHOURSMAPWITHLIST = "LOCATIONHOURSMAPWITHLIST";
	private static final String SITELANGUAGEMAP = "SITELANGUAGEMAP";
	private static final String ENROLLMENT_ENTITY_MENU = "enrollmentEntityMenu";
	private static final String MENU = "menu";
	private static final String LIST_OF_PRIMARY_SITES = "primarySitelist";
	private static final String LIST_OF_SECONDARY_SITES = "secondarySitelist";
	private static final String LIST_OF_EDUCATIONS = "educationlist";
	private static final String IS_SHOW = "isShow";
	private static final String FILE_INPUT = "fileInput";
	private static final String OTHER_SPOKEN_LANGUAGE = "otherSpokenLanguage";
	private static final String OTHER_WRITTEN_LANGUAGE = "otherWrittenLanguage";
	private static final String LANGUAGE_NAMES = "languageNames";
	private static final String IS_CERTIFICATION_CERTIFIED = "isCertified";
	private static final String LANGUAGE_WRITTEN_NAMES = "languageWrittenNames";
	private static final String ENTITY_LANGUAGE_SPOKEN = "ENTITY_LANGUAGE_SPOKEN";
	private static final String ENTITY_LANGUAGE_WRITTEN = "ENTITY_LANGUAGE_WRITTEN";
	private static final String LIST_OF_LANGUAGES_SPOKEN = "languageSpokenList";
	private static final String LIST_OF_LANGUAGES_WRITTEN = "languageWrittenList";
	private static final String RETRIEVING_ENROOLMENTENTITY_HISTORY_DETAIL_REST_CALL_STARTS = "Retrieving Enrollment Entity History Detail REST Call starts";
	private static final String RETRIEVING_ENROOLMENTENTITY_HISTORY_DETAIL_REST_CALL_ENDS = "Retrieving Enrollment Entity History Detail REST Call Ends";
	private static final String REGISTRATIONSTATUS = "registrationStatus";
	private static final String FILENAME = "fileName";
	private static final String ENTITY_NAME = "entity_name";
	private static final String EMPTY_STRING = "";
	private static final String LANGUAGES_LIST = "languagesList";
	private AccountUser user = null;
    private static final String HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER = "hasPermission(#model, 'ASSISTER_EDIT_ASSISTER')";
	private static final String SHOW_POSTAL_MAIL="showPostalMailOption";
	private static final String CERTIFICATION_STATUS = "certificationStatus";
    private static final int VAL_PAGE_SIZE = 10;
    private static final String CHANGE_ORDER = "changeOrder";
    private static final String ASSISTERLIST = "assisterList";
    private static final String SORT_BY_COL = "sortByCol";
    private static final String SORT_ORDER = "sortOrder";
    private static final String TOTAL_ASSISTERS_BY_SEARCH = "totalassistersbysearch";
    private static final String TOTAL_ASSISTERS = "totalassisters";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String ASSISTER_SEARCH_PARAMETERS = "assisterSearchParameters";
    private static final String ENTITY_ENTITYADMIN_DISPLAYASSISTERS = "entity/entityadmin/displayassisters";
    private static final String STATUS_INACTIVE = "statusInactive";
    private static final String STATUS_ACTIVE = "statusActive";
    private static final String CURRENT_PAGE = "currentPage";
    private static final String RETRIEVING_LIST_OF_ASSISTERS_ASSOCIATED_WITH_THE_LOGGED_IN_ENROLLMENT_ENTITY = "Retrieving List of Assisters associated with the logged in Enrollment Entity";
    private static final String SORT_BY = "sortBy";
    private static final String EXCEPTION_OCCURRED_WHILE_UPLOADING_ENTITY_APPLICATION_DOCUMENT = "Exception occured while uploading entity application document : ";
    private static final String EXCEPTION_OCCURRED_WHILE_RETRIEVING_ASSISTER_INFORMATION = "Exception occured while retrieving assister information : ";
    private static final String EXCEPTION_OCCURRED_WHILE_EDITING_ASSISTER_STATUS = "Exception occured while editing assister status : ";
    private static final String EXCEPTION_OCCURRED_WHILE_MANAGING_ENTITIES = "Exception occured while managing entities : ";
    private static final String ADD_NEW_ENTITY_URL = "addNewEntityUrl";
    
	@Autowired 
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired
	private LookupService lookupService;

	@Autowired
	private EERestCallInvoker restClassCommunicator;

	@Autowired
	private UserService userService;
	
	@Autowired
	private EntityService entityService;
	
	@Autowired
	private EntityUtils entityUtils;

	@Autowired
	private SmartValidator validator;
	
	@Autowired private Gson platformGson;
	
	public static String ADD_NEW_ENTITY_ACCOUNT_URL;
	
	@Value("#{configProp['addNewEntityAhbxUrl']}")
	public void setADD_NEW_ENTITY_ACCOUNT_URL(String aDD_NEW_ENTITY_ACCOUNT_URL) {
		ADD_NEW_ENTITY_ACCOUNT_URL = aDD_NEW_ENTITY_ACCOUNT_URL;
	}

	/**
	 * Handles the POST request for Broker's Certification Status
	 * 
	 * @param broker
	 * @param request
	 * @param result
	 * @param model
	 * @return URL for navigation to edit certification status page.
	 */
	@RequestMapping(value = "/entity/entityadmin/registrationstatus", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_MANAGE_ENTITY)
	public String submitadminregistrationstatus() {

		LOGGER.info("submitadminregistrationstatus: START");

		LOGGER.info("EnrollmentEntity Registration Status page ");

		LOGGER.info("submitadminregistrationstatus: END");

		return "/entity/admin/enrollmententity/registrationstatus";
	}

	/**
	 * Handles the GET request for Broker's Certification Status
	 * 
	 * @param encryptedId
	 * @param model
	 * @return URL for navigation to edit certification status page.
	 */
	@RequestMapping(value = "/entity/entityadmin/registrationstatus/{id}", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_MANAGE_ENTITY)
	public String registarionstatus(Model model, HttpServletRequest request, @PathVariable("id") String encryptedId) {

		LOGGER.info("registrationstatus: START");
		int id = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId));

		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Registarion Status");
		EnrollmentEntity enrollmentEntityObj = new EnrollmentEntity();
		EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequest.setModuleId(id);
		
		try {
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
			String enrollmentEntityObjStr = restClassCommunicator
					.retrieveEnrollmentEntityObjectById(enrollmentEntityRequest);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_ENDS);

			EnrollmentEntityResponseDTO enrollmentEntityResponse =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollmentEntityObjStr);
			enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();
			if (enrollmentEntityObj != null) {

				model.addAttribute(REGISTRATION_STATUS, enrollmentEntityObj.getRegistrationStatus());
			}
			model.addAttribute(ENROLLMENTENTITY, enrollmentEntityObj);
			List<Map<String, Object>> entityCertStatusHistory = null;
			Map<String, Boolean> map = null;
			if (enrollmentEntityObj != null) {
				EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequestDTO.setModuleId(enrollmentEntityObj.getId());
				LOGGER.info(RETRIEVING_ENROOLMENTENTITY_HISTORY_DETAIL_REST_CALL_STARTS);
				String enrollmentEntityHistoryStr = restClassCommunicator
						.retrieveEnrollmentEntityHistoryForAdminByUserId(enrollmentEntityRequestDTO);
				LOGGER.info(RETRIEVING_ENROOLMENTENTITY_HISTORY_DETAIL_REST_CALL_ENDS);
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollmentEntityHistoryStr);
				entityCertStatusHistory = enrollmentEntityResponseDTO.getEnrollmentEntityHistory();
				map = getLeftNavigationMenuMap(enrollmentEntityObj);
				request.getSession().setAttribute(REGISTRATIONSTATUS, enrollmentEntityObj.getRegistrationStatus());
			} else {
				map = new HashMap<String, Boolean>();
			}
			model.addAttribute(STATUS_HISTORY, entityCertStatusHistory);
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving entity registration status : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving entity registration status in registarionstatus method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("registrationstatus: END");

		return "entity/admin/enrollmententity/registrationstatus";
	}

	/**
	 * Handles the GET request for Broker's Edit Certification Status.
	 * 
	 * @param model
	 * @param id
	 * @param request
	 * @param encrytedEntitylistId
	 * @param documentId
	 * @return URL for navigation to edit certification status page.
	 */
	@RequestMapping(value = "/entity/entityadmin/editregistrationstatus/{encrytedEntitylistId}", method = { RequestMethod.GET,
			RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ENTITY)
	public String updateregistrationstatus(Model model, HttpServletRequest request, @PathVariable("encrytedEntitylistId") String encrytedEntitylistId,
			@RequestParam(value = "documentId", required = false) String documentId) {
		Integer id =Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(encrytedEntitylistId));
		LOGGER.info("updateregistrationstatus: START");
		Map<String, Boolean> map = null;
		
		try {
			EnrollmentEntity enrollmentEntityObj = entityUtils.getEnrollmentEntityById(id);
			model.addAttribute(ENROLLMENTENTITY, enrollmentEntityObj);
			List<String> listLookupLabelValue = getLookupValueForRegistrationStatus(ENTITY_REGISTRATION_STATUS);
			model.addAttribute(STATUSLIST, listLookupLabelValue);

			// Get the broker details from broker audit table
			List<Map<String, Object>> entityCertStatusHistory = null;
			if (enrollmentEntityObj != null) {
				EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequestDTO.setModuleId(enrollmentEntityObj.getId());
				LOGGER.info(RETRIEVING_ENROOLMENTENTITY_HISTORY_DETAIL_REST_CALL_STARTS);
				String enrollmentEntityHistoryStr = restClassCommunicator
						.retrieveEnrollmentEntityHistoryForAdminByUserId(enrollmentEntityRequestDTO);
				LOGGER.info(RETRIEVING_ENROOLMENTENTITY_HISTORY_DETAIL_REST_CALL_ENDS);
				
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollmentEntityHistoryStr);
				entityCertStatusHistory = enrollmentEntityResponseDTO.getEnrollmentEntityHistory();
			}
			if (enrollmentEntityObj != null) {
				map = getLeftNavigationMenuMap(enrollmentEntityObj);
				request.getSession().setAttribute(REGISTRATIONSTATUS, enrollmentEntityObj.getRegistrationStatus());
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			model.addAttribute(STATUS_HISTORY, entityCertStatusHistory);
			
			String fileName = (String) request.getSession().getAttribute(FILENAME);
			if (enrollmentEntityObj != null) {
				model.addAttribute("newEnrollmentEntity", enrollmentEntityObj);
				model.addAttribute(UPLOADED_DOCUMENT, documentId);

				model.addAttribute(FILENAME, fileName);
			}
			if ((request.getSession().getAttribute(UPLOADED_DOCUMENT_CHECK) != null)
					&& ("SIZE_FAILURE".equals(request.getSession().getAttribute(UPLOADED_DOCUMENT_CHECK)))) {
				model.addAttribute(UPLOADED_DOCUMENT_CHECK, "SIZE_FAILURE");
			}
			request.getSession().removeAttribute(FILENAME);
			request.getSession().setAttribute("fileUploadId", id);
			request.getSession().removeAttribute(UPLOADED_DOCUMENT_CHECK);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while editing entity registration status : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while editing entity registration status in updateregistrationstatus method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("updateregistrationstatus: END");

		return "entity/admin/enrollmententity/editregistrationstatus";
	}

	/**
	 * Handles the GET/POST request for Broker's update Certification status
	 * 
	 * @param enrollmentEntity
	 * @param result
	 * @param model
	 * @param request
	 * @param entityId
	 * @param newRegistrationRenewalDate
	 * @return URL for navigation to edit certification status page.
	 */
	@RequestMapping(value = "/entity/entityadmin/updateregistrationstatus", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ENTITY)
	public String updateregistrationstatus(@ModelAttribute(ENROLLMENTENTITY) EnrollmentEntity enrollmentEntity,BindingResult result,
			Model model, HttpServletRequest request, @RequestParam(ENTITY_ID) Integer entityId,
			@RequestParam(value = "newRegistrationRenewalDate", required = false) String newRegistrationRenewalDate) {

		LOGGER.info("updateregistrationstatus: START");

		/** Check for XSS Attack - Starts */
		enrollmentEntity.setComments(XssHelper.stripXSS(enrollmentEntity.getComments()));
		/** Check for XSS Attack - Ends */

		String documentId = request.getParameter("documentId");
		
		try {
			EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();

			EnrollmentEntity enrollmentEntityObj = entityUtils.getEnrollmentEntityById(entityId);
			//Fetching first 4000 char for comments text;
			String comments = enrollmentEntity.getComments();
			int beginIndex = 0;
			int endIndex = comments.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  comments.length();
			comments = comments.substring(beginIndex, endIndex);
			enrollmentEntityObj.setComments(comments);
			
			if (!enrollmentEntityObj.getRegistrationStatus().equalsIgnoreCase(enrollmentEntity.getRegistrationStatus()) ) {
				createEntityStatusChangeNotice(enrollmentEntity, enrollmentEntityObj);
			}
			
			if ((("Registered".equalsIgnoreCase(enrollmentEntityObj.getRegistrationStatus()))||("Active".equalsIgnoreCase(enrollmentEntityObj.getRegistrationStatus())))
					&& (EntityUtils.isEmpty(newRegistrationRenewalDate))){
				Calendar date = TSCalendar.getInstance();
				date.add(Calendar.YEAR, 1);
				Date newdate = date.getTime();
				enrollmentEntityObj.setRegistrationRenewalDate(newdate);
				enrollmentEntityObj.setRegistrationDate(new TSDate());
			}
			if (!EntityUtils.isEmpty(newRegistrationRenewalDate)) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
				Date convertedDate = dateFormat.parse(newRegistrationRenewalDate);
				enrollmentEntityObj.setRegistrationRenewalDate(convertedDate);
			}

			validator.validate(enrollmentEntityObj, result, EnrollmentEntity.EntityRegistrationRenewalDate.class);
			if(result.hasErrors()){
				 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			}
			
			setDocumentIdForEnrollmentEntity(documentId, enrollmentEntityObj);
			
			enrollmentEntityRequest.setEnrollmentEntity(enrollmentEntityObj);
			
			saveEnrollmentEntityStatus(enrollmentEntityRequest);
			
			List<String> listLookupLabelValue = getLookupValueForRegistrationStatus(ENTITY_REGISTRATION_STATUS);
			model.addAttribute(STATUSLIST, listLookupLabelValue);

		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while updating entity registration status : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while updating entity registration status in updateregistrationstatus method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		request.getSession().removeAttribute(UPLOADED_DOCUMENT);

		LOGGER.info("updateregistrationstatus: END");

		return "redirect:/entity/entityadmin/registrationstatus/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(entityId));
	}

	/**
	 * Method to set document identifier for EnrollmentEntity.
	 * 
	 * @param documentId The current document identifier.
	 * @param enrollmentEntityObj The EnrollmentEntity instance.
	 * @throws NumberFormatException The NumberFormatException instance.
	 */
	private void setDocumentIdForEnrollmentEntity(String documentId,
			EnrollmentEntity enrollmentEntityObj) {
		LOGGER.info("setDocumentIdForEnrollmentEntity: START");
		
		if (EntityUtils.isEmpty(documentId)
				|| (!EntityUtils.isEmpty(documentId) && "0".equalsIgnoreCase(documentId))) {
			enrollmentEntityObj.setDocumentId(null);
		} else {
			enrollmentEntityObj.setDocumentId(Integer.valueOf(documentId));
		}
		LOGGER.info("setDocumentIdForEnrollmentEntity: END");
	}

	/**
	 * @param enrollmentEntity
	 * @param enrollmentEntityObj
	 * @throws Exception 
	 */
	private void createEntityStatusChangeNotice(
			EnrollmentEntity enrollmentEntity,
			EnrollmentEntity enrollmentEntityObj) throws Exception {
		String priorStatus = enrollmentEntityObj.getRegistrationStatus();
		enrollmentEntityObj.setRegistrationStatus(enrollmentEntity.getRegistrationStatus());
		enrollmentEntityObj.setStatusChangeDate(new TSDate());			
		try {
			if(!EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
				entityService.createEntityStatusChangeNotice(enrollmentEntityObj, priorStatus);
			}
		} catch (Exception e) {
			LOGGER.error("Error generating notice for entity certification status change", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	/**
	 * @param enrollmentEntityRequest
	 * @throws JsonProcessingException 
	 */
	private void saveEnrollmentEntityStatus(
			EnrollmentEntityRequestDTO enrollmentEntityRequest) throws JsonProcessingException {
		LOGGER.info("Retrieving Save EnrollmentEntity Detail REST Call Starts");
		String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequest);
		restClassCommunicator.saveEnrollmentEntityStatus(requestJSON);
		LOGGER.info("Retrieving Save EnrollmentEntity Detail REST Call Ends");
	}

	/**
	 * Handles the POST request for Edit Broker information
	 * 
	 * @param request
	 * @param brokerId
	 * @param model
	 * @return URL for navigation to edit certification status page.
	 * @throws IOException 
	 */
	@RequestMapping(value = "/entity/entityadmin/uploaddocument", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ENTITY)
	public void uploadEntityApplicationDocuments(
			@ModelAttribute(ENROLLMENT_ENTITY) EnrollmentEntity enrollmentEntity,
			@RequestParam(value = FILE_INPUT, required = false) MultipartFile addSupportDoc,
			@RequestParam(value = ENTITY_ID, required = false) Integer entityId,
			HttpServletRequest request, HttpServletResponse response) throws IOException {

		LOGGER.info("uploadEntityApplicationDocuments: START");
				
		try {
			String returnString = "", fileToUpload = null, documentType = null, newRegistrationRenewalDate = null;
			
			if(null == request) { 
				LOGGER.warn(REQUEST_INSTANCE_IS_MISSING_FOR_THE_REQUEST);
			}
			else {
				fileToUpload = request.getParameter("fileToUpload");
				documentType = request.getParameter("REGISTRATIONSTATUS");
				newRegistrationRenewalDate = request.getParameter("newRegistrationRenewalDate");
			}
			
			if(null != enrollmentEntity) {
				if(fileToUpload != null && addSupportDoc != null){
					returnString = populateReturnStringForUploadEntityApplicationDocuments(enrollmentEntity, addSupportDoc, entityId, request,
							fileToUpload, documentType, newRegistrationRenewalDate);				
				}
				
				/** Check for XSS Attack - Starts */
				enrollmentEntity.setComments(XssHelper.stripXSS(enrollmentEntity.getComments()));
				/** Check for XSS Attack - Ends */
			}
			
			response.setContentType("text/html");
			response.getWriter().write(returnString);
			response.flushBuffer();

			LOGGER.info("uploadEntityApplicationDocuments: END");
			return;
		} catch(GIRuntimeException giexception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_UPLOADING_ENTITY_APPLICATION_DOCUMENT, giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_UPLOADING_ENTITY_APPLICATION_DOCUMENT, exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	/**
	 * Method to populate return String for UploadEntityApplicationDocuments(...).
	 * 
	 * @param enrollmentEntity The current EnrollmentEntity instance.
	 * @param addSupportDoc The support documnet MultipartFile instance.
	 * @param entityId The current Enrollment Entity identifier.
	 * @param request The current HttpServletRequest instance.
	 * @param fileToUpload The file to upload.
	 * @param documenttype The document type.
	 * @param newRegistrationRenewalDate The new registration renewal date.
	 * @return returnString The return String.
	 * @throws Exception The Exception instance.
	 */
	private String populateReturnStringForUploadEntityApplicationDocuments(
			EnrollmentEntity enrollmentEntity, MultipartFile addSupportDoc,
			Integer entityId, HttpServletRequest request, String fileToUpload,
			String documenttype, String newRegistrationRenewalDate)
			throws Exception {
		LOGGER.info("populateReturnStringForUploadEntityApplicationDocuments: START");
		String returnString = null;
		if (addSupportDoc.getSize() < FILE_SIZE_EXCEED) {
			if (!EntityUtils.isEmpty(newRegistrationRenewalDate)) {
				formatRegistrationRenewalDate(enrollmentEntity, newRegistrationRenewalDate);
			}
			int documentId = uploadFile(fileToUpload, "ADD_SUPPORT_FOLDER", addSupportDoc, entityId, documenttype, request,
					enrollmentEntity);
			returnString = fileToUpload + "|" + addSupportDoc.getOriginalFilename() + "|" + documentId;
		}
		else{
			 returnString = fileToUpload + "|" + addSupportDoc.getOriginalFilename() + "|" + -1;
		}
		LOGGER.info("populateReturnStringForUploadEntityApplicationDocuments: END");
		return returnString;
	}

	/**
	 * Method to format registration renewal date.
	 * 
	 * @param enrollmentEntity The current EnrollmentEntity instance.
	 * @param newRegistrationRenewalDate The new registration renewal date.
	 */
	private void formatRegistrationRenewalDate(
			EnrollmentEntity enrollmentEntity, String newRegistrationRenewalDate) {
		LOGGER.info("formatRegistrationRenewalDate: START");
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		Date convertedDate;
		try {
			convertedDate = dateFormat.parse(newRegistrationRenewalDate);
			enrollmentEntity.setRegistrationRenewalDate(convertedDate);
		} catch (ParseException parseException) {
			LOGGER.error("Cannot upload Document", parseException);
		}
		LOGGER.info("formatRegistrationRenewalDate: END");
	}

	/**
	 * Handles the POST request for Edit Broker information
	 * 
	 * @param request
	 * @param brokerId
	 * @param model
	 * @return URL for navigation to edit certification status page.
	 * @throws ContentManagementServiceException
	 * @throws IOException
	 */
	@RequestMapping(value = "/entity/entityadmin/viewAttachment", method = RequestMethod.GET)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_VIEW_ENTITY)
	public void viewAttachment(@RequestParam String  encrypteddocumentId, HttpServletResponse response)
			throws ContentManagementServiceException, IOException {

		LOGGER.info("viewAttachment: START");

		EntityDocuments entityDocuments = null;
		
		try {
			Integer documentId=Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(encrypteddocumentId));
			
			if (documentId != null) {
				EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequest.setModuleId(documentId);
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
				String entityDocumentsStr = restClassCommunicator
						.retrieveEntityDocumentsObjectById(enrollmentEntityRequest);
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_ENDS);

				EnrollmentEntityResponseDTO enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityDocumentsStr);
				entityDocuments = enrollmentEntityResponse.getEntityDocuments();

			}
			byte[] attachment = null;

			if (entityDocuments != null) {
				EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequest.setEntityType(entityDocuments.getDocumentName());
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
				String attachmentStr = restClassCommunicator.retrieveDocumentsfromECMbyId(enrollmentEntityRequest);
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_ENDS);

				EnrollmentEntityResponseDTO enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(attachmentStr);
				attachment = enrollmentEntityResponse.getAttachment();
			} else {
				attachment = "No Document is attached".getBytes();
			}

			if (entityDocuments != null) {
				response.setContentType(entityDocuments.getMimeType().replaceAll("[\n\r]", ""));

				response.addHeader("Content-Disposition",
						"attachment; filename=" + entityDocuments.getOrgDocumentName().replaceAll("[\n\r]", ""));
			} else {
				response.setContentType("text/plain");

				response.setContentLength(attachment.length);
			}
			FileCopyUtils.copy(attachment, response.getOutputStream());

		} catch (IOException iOException) {
			LOGGER.error("Unable to show attachment", iOException);
			FileCopyUtils.copy("UNABLE TO SHOW ATTACHMENT.PLEASE CONTACT ADMIN".getBytes(), response.getOutputStream());
		}

		LOGGER.info("viewAttachment: END");
	}

	// uploading the files into their specific folders
	private int uploadFile(String fileToUpload, String folderName, MultipartFile file, int entityId,
			String documenttype, HttpServletRequest request, EnrollmentEntity enrollmentEntity) throws Exception {

		LOGGER.info("uploadFile: START");

		EnrollmentEntity enrollmentEntityObj = null;
		int documentId1 = 0;
		
		try {
			if (entityId != 0) {
				enrollmentEntityObj = entityUtils.getEnrollmentEntityById(entityId);
			}

			String documentId = null;

			// creating a new file name with the time stamp
			String modifiedFileName = FilenameUtils.getBaseName(file.getOriginalFilename()) + GhixConstants.UNDERSCORE
					+ TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT
					+ FilenameUtils.getExtension(file.getOriginalFilename());
			// upload file to DMS

			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setAttachment(file.getBytes());
			enrollmentEntityRequestDTO.setModuleId(entityId);
			enrollmentEntityRequestDTO.setEntityType(modifiedFileName);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
			String documentIdStr = restClassCommunicator.saveDocumentsOnECM(enrollmentEntityRequestDTO);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_ENDS);
			EnrollmentEntityResponseDTO enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(documentIdStr);
			documentId = enrollmentEntityResponse.getDocument();

			String createdBy = userService.getLoggedInUser().getEmail();

			MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
			String mimeType = mimeTypesMap.getContentType(file.getOriginalFilename());
			EntityDocuments entityDocs = new EntityDocuments();
			entityDocs.setCreatedBy(createdBy);
			entityDocs.setCreatedDate(new TSDate());
			entityDocs.setDocumentName(documentId);
			entityDocs.setDocumentType(documenttype);
			entityDocs.setMimeType(mimeType);
			entityDocs.setOrgDocumentName(file.getOriginalFilename());
			// EnrollmentEntityRequestDTO enrollmentEntityRequestDTO=new
			// EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setEntityDocuments(entityDocs);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
			String entityDocsStr = restClassCommunicator.saveDocument(enrollmentEntityRequestDTO);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_ENDS);

			enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityDocsStr);
			EntityDocuments entityDocuments = enrollmentEntityResponse.getEntityDocuments();

			documentId1 = entityDocuments.getID();
			
			if(null != enrollmentEntityObj) {
				enrollmentEntityObj.setComments(enrollmentEntity.getComments());
				enrollmentEntityObj.setRegistrationStatus(enrollmentEntity.getRegistrationStatus());
				enrollmentEntityObj.setRegistrationRenewalDate(enrollmentEntity.getRegistrationRenewalDate());
			}
			
			request.getSession().setAttribute(ENROLLMENT_ENTITY, enrollmentEntityObj);
		} catch (Exception ex) {
			LOGGER.error("Fail to uplaod Entity Document in uploadFile method file. Exception: ", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("uploadFile: END");

		return documentId1;
	}


	private List<String> getLookupValueForRegistrationStatus(String string) {

		LOGGER.info("getLookupValueForRegistrationStatus: START");

		List<LookupValue> listLookupValueForRegistrationStatus = lookupService
				.getLookupValueList(ENTITY_REGISTRATION_STATUS);
		List<String> listLookupLabelValue = new ArrayList<String>();
		for (LookupValue lookupValue : listLookupValueForRegistrationStatus) {

			listLookupLabelValue.add(lookupValue.getLookupValueLabel());
		}

		LOGGER.info("getLookupValueForRegistrationStatus: END");

		return listLookupLabelValue;
	}

	/**
	 * Handles the Manage Enrollment Entity request by Admin
	 * 
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate manage Agent page
	 */
	@RequestMapping(value = "/entity/entityadmin/manage", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_MANAGE_ENTITY)
	public String manageEntity(Model model, HttpServletRequest request) {

		LOGGER.info("manageEntity: START");

		LOGGER.info("Inside manage enrollment entity");
		Integer pageSize = TEN;
		request.setAttribute(PAGE_SIZE, TEN);
		request.setAttribute(REQ_URI, LIST);
		String param = request.getParameter(PAGE_NUMBER);
		Integer startRecord = 0;
		int currentPage = 1;
		if (param != null) {
			startRecord = (Integer.parseInt(param) - 1) * pageSize;
			currentPage = Integer.parseInt(param);
			model.addAttribute(PAGE_NUMBER, param);
		} else {
			model.addAttribute(PAGE_NUMBER, 1);
		}
		EnrollmentEntity eeObj = new EnrollmentEntity();

		try {
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setPageSize(pageSize);
			enrollmentEntityRequestDTO.setStartRecord(startRecord);
			enrollmentEntityRequestDTO.setEnrollmentEntity(eeObj);
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.info("Calling ghix-entity to retrieve EE list");
			// Rest call for enrollment entity service
			String enrollEntityResponse = restClassCommunicator.retrieveEnrollmentEntityMap(requestJSON);
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
			LOGGER.info("Return from REST call.");

			Map<String, Object> entityListAndRecordCount = enrollmentEntityResponseDTO.getEntityListAndRecordCount();

			Map<EnrollmentEntity, Integer> eentityWithAssisterCount = (HashMap<EnrollmentEntity, Integer>) entityListAndRecordCount
					.get("enrollmententitymap");

			// Long iResultCt = (Long) entityListAndRecordCount.get("recordCount");
			// int iResultCount = iResultCt.intValue();
			model.addAttribute("enrollmententitiesmap", eentityWithAssisterCount);
			model.addAttribute(RESULT_SIZE, entityListAndRecordCount.get(RECORD_COUNT));
			model.addAttribute("currentPage", currentPage);
			request.getSession().setAttribute(ENTITYSEARCHPARAM, eeObj);
			model.addAttribute(ENROLLMENT_ENTITY, eeObj);

			// Fetching List of Languages
			List<LookupValue> listOflanguages = lookupService.getLookupValueList(ENTITY_LANGUAGE);
			model.addAttribute(LIST_OF_LANGUAGES, listOflanguages);

			List<String> listLookupLabelValue = getLookupValueForRegistrationStatus(ENTITY_REGISTRATION_STATUS);
			listLookupLabelValue.add(EntityConstants.STATUS_INCOMPLETE);
			model.addAttribute(STATUSLIST, listLookupLabelValue);
			model.addAttribute(ORGANIZATIONTYPELIST, getOrganizationTypes());
			request.getSession().setAttribute(LOGGED_USER, ENTITY_ADMIN);
			if(EntityUtils.checkStateCode(EntityConstants.NV_STATE_CODE)) {
				request.getSession().setAttribute(EntityConstants.GLOBAL_STATE_CODE1, EntityUtils.checkStateCode(EntityConstants.NV_STATE_CODE));
			}else {
				request.getSession().setAttribute(EntityConstants.GLOBAL_STATE_CODE, EntityUtils.checkStateCode(EntityConstants.ID_STATE_CODE));
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_MANAGING_ENTITIES, giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_MANAGING_ENTITIES, exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		model.addAttribute(ENROLLMENT_ENTITY, eeObj);
		request.getSession().removeAttribute(MENU);
		request.getSession().setAttribute(MENU, ENROLLMENT_ENTITY_MENU);
		LOGGER.info("Redirecting to EE manage list page");

		LOGGER.info("manageEntity: END");

		return "entity/entityadmin/manage";
	}

	/**
	 * Handles the enrollment entity's list page on Manage Admin tab
	 * 
	 * @param enrollmentEntity
	 * @param entityName
	 * @param status
	 * @param organizationType
	 * @param language
	 * @param fromDate
	 * @param toDate
	 * @param zipCode
	 * @param receivedPayments
	 * @param countyServed
	 * @param result
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/entity/entityadmin/managelist", method = { RequestMethod.GET, RequestMethod.POST })
	@GiAudit(transactionName = "ManageEntityAdmin", eventType = EventTypeEnum.ENTITY_ADMIN, eventName = EventNameEnum.PII_READ)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_MANAGE_ENTITY)
	public String enrollmententityList(@ModelAttribute(ENROLLMENTENTITY) EnrollmentEntity enrollmentEntity,
			Model model, HttpServletRequest request) {

		LOGGER.info("enrollmententityList: START");
		String entityName = null, entityNumber = null, status = null, organizationType = null, language = null, fromDate = null, toDate = null, zipCode = null, receivedPayments = null, countyServed = null;
		String param = null;
		int pageSize = TEN;
		entityName = request.getParameter("entityName");
		entityNumber = request.getParameter("entityNumber");
		status = request.getParameter("status");
		organizationType = request.getParameter("organizationType");
		language = request.getParameter("language");
		fromDate = request.getParameter(FROM_DATE);
		toDate = request.getParameter(TO_DATE);
		zipCode = request.getParameter("zipCode");
		receivedPayments = request.getParameter("receivedPayments");
		countyServed = request.getParameter("countyServed");
		//fetch page size
		String requestPageSize = request.getParameter("pageSize");
		//if request page size is not empty
		if(!StringUtils.isBlank(requestPageSize)){
			pageSize = Integer.parseInt(requestPageSize);
		}
		request.setAttribute(PAGE_SIZE,pageSize);
		request.setAttribute(REQ_URI, LIST);
		param = request.getParameter(PAGE_NUMBER);

		Integer startRecord = 0;
		int currentPage = 1;
		if (param != null) {
			startRecord = (Integer.parseInt(param) - 1) * pageSize;
			currentPage = Integer.parseInt(param);
			model.addAttribute(PAGE_NUMBER, param);
		} else {
			model.addAttribute(PAGE_NUMBER, 1);
		}
		EnrollmentEntity eeObj = (EnrollmentEntity) request.getSession().getAttribute(ENTITYSEARCHPARAM);
		if (eeObj == null) {
			eeObj = enrollmentEntity;
		}
		
		try {
			String sortBy =  (request.getParameter("sortBy") == null) ?  ENTITY_NAME : request.getParameter("sortBy");
			String sortOrder = determineSortOrder(request);
			
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setPageSize(pageSize);
			enrollmentEntityRequestDTO.setStartRecord(startRecord);
			enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntity);
			enrollmentEntityRequestDTO.setEntityName(entityName);
			enrollmentEntityRequestDTO.setEntityNumber(entityNumber); 
			enrollmentEntityRequestDTO.setStatus(status);
			enrollmentEntityRequestDTO.setOrganizationType(organizationType);
			enrollmentEntityRequestDTO.setReceivedPayments(receivedPayments);
			enrollmentEntityRequestDTO.setCountyServed(countyServed);

			enrollmentEntityRequestDTO.setLanguage(language);
			enrollmentEntityRequestDTO.setFromDate(fromDate);
			enrollmentEntityRequestDTO.setToDate(toDate);

			if(EntityUtils.isNullOrBlank(zipCode)) {
				enrollmentEntityRequestDTO.setZipCode(zipCode);
			}
		
			enrollmentEntityRequestDTO.setSortBy(sortBy);
			enrollmentEntityRequestDTO.setSortOrder(sortOrder);
			
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = retriveEnrollmentEntityMap(enrollmentEntityRequestDTO);
			Map<String, Object> entityListAndRecordCount = enrollmentEntityResponseDTO.getEntityListAndRecordCount();


			Object tmpObject= entityListAndRecordCount.get("enrollmententitymap");
			Map<EnrollmentEntity, Integer> eentityWithAssisterCount = new HashMap<EnrollmentEntity, Integer>();
			EnrollmentEntity tmpEnrollmentEntity=null;
			ObjectMapper mapper = new ObjectMapper();
			if(tmpObject instanceof Map){
				Map tmpMap = (Map)tmpObject;
				for(Object tmpKeyObject:tmpMap.keySet()){
					 
					if(tmpKeyObject instanceof String){
						tmpEnrollmentEntity = mapper.readValue( (String)tmpKeyObject, EnrollmentEntity.class );
						eentityWithAssisterCount.put(tmpEnrollmentEntity, (Integer) tmpMap.get(tmpKeyObject));
					}
					 
					
				}
			}
			//Map<EnrollmentEntity, Integer> eentityWithAssisterCount = (HashMap<EnrollmentEntity, Integer>) entityListAndRecordCount.get("enrollmententitymap");

			List<String> listLookupLabelValue = getLookupValueForRegistrationStatus(ENTITY_REGISTRATION_STATUS);
			List<LookupValue> listOflanguages = lookupService.getLookupValueList(ENTITY_LANGUAGE);
			Collections.sort(listOflanguages);
			Collections.reverse(listOflanguages);
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			Set<String> lookupCountiesServed = entityUtils.populateCountiesServedDataSet(stateCode);
			
			listLookupLabelValue.add(EntityConstants.STATUS_INCOMPLETE);
			
			EnrollmentEntitySearchParameters enrollmentEntitySearchParameters = setEnrollmentEntitySearchParameters(
					entityName, entityNumber, status, organizationType, fromDate, toDate, language, zipCode, countyServed,
					receivedPayments);

			setModalAndSession(enrollmentEntity, model, request, currentPage, enrollmentEntityResponseDTO, entityListAndRecordCount, 
					eentityWithAssisterCount, enrollmentEntitySearchParameters, listLookupLabelValue, lookupCountiesServed, listOflanguages);
			
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS) , new GiAuditParameter("EntityWithAssisterCount", eentityWithAssisterCount),
					new GiAuditParameter("entityListAndRecordCount", entityListAndRecordCount), new GiAuditParameter("EnrollmentEntitySearchParameters", enrollmentEntitySearchParameters.toString()));
		
			
			// Fetching List of Languages
			model.addAttribute("sortOrder", sortOrder);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
			
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				model.addAttribute(ADD_NEW_ENTITY_URL, ADD_NEW_ENTITY_ACCOUNT_URL);
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while processing enrollmententityList : ", giexception);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while fetching EnrollmentEntity "));
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while processing enrollmententityList in enrollmententityList method : ", exception);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while fetching EnrollmentEntity "));
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return "entity/entityadmin/manage";
	}

	/**
	 * Method to determine SortOrder.
	 * 
	 * @param request The current HttpServletRequest instance.
	 * @return sortOrder The sort order String.
	 */
	private String determineSortOrder(HttpServletRequest request) {
		LOGGER.info("determineSortOrder: START");
		String sortOrder =  (request.getParameter("sortOrder") == null) ?  ASC : request.getParameter("sortOrder");
		sortOrder = (sortOrder.equals("")) ? ASC : sortOrder;
		String changeOrder =  (request.getParameter("changeOrder") == null) ?  "false" : request.getParameter("changeOrder");
		sortOrder = (changeOrder.equals("true") ) ? (sortOrder.equals(ASC) ? "DESC" : ASC ): sortOrder;
		LOGGER.info("determineSortOrder: END");
		return sortOrder;
	}

	/**
	 * Handles the enrollment entity's list page on Manage Admin tab for
	 * Pagination
	 * 
	 * @param enrollmentEntity
	 * @param pEntityName
	 * @param pStatus
	 * @param pOrganizationType
	 * @param pLanguage
	 * @param pFromDate
	 * @param pToDate
	 * @param pZipCode
	 * @param pReceivedPayments
	 * @param pCountyServed
	 * @param result
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/entity/entityadmin/managelistforpagination", method = { RequestMethod.GET,
			RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_MANAGE_ENTITY)
	public String enrollmententityListForPagination(
			@ModelAttribute(ENROLLMENTENTITY) EnrollmentEntity enrollmentEntity,
			Model model, HttpServletRequest request) {

		LOGGER.info("enrollmententityListForPagination: START");
		String pEntityName = null, pStatus = null, pOrganizationType = null, pLanguage = null, pFromDate = null, pToDate = null, pZipCode = null, pReceivedPayments = null, pCountyServed = null;
		String param = null;
		EnrollmentEntity eeObj = null;
		Integer pageSize = TEN;
		
		if(null == request) { 
			LOGGER.warn(REQUEST_INSTANCE_IS_MISSING_FOR_THE_REQUEST);
		}
		else {
			pEntityName = request.getParameter("pEntityName");
			pStatus = request.getParameter("pStatus");
			pOrganizationType = request.getParameter("pOrganizationType");
			pLanguage = request.getParameter("pLanguage");
			pFromDate = request.getParameter("pFromDate");
			pToDate = request.getParameter("pToDate");
			pZipCode = request.getParameter("pZipCode");
			pReceivedPayments = request.getParameter("pReceivedPayments");
			pCountyServed = request.getParameter("pCountyServed");
			
			request.setAttribute(PAGE_SIZE, pageSize);
			request.setAttribute(REQ_URI, LIST);
			param = request.getParameter(PAGE_NUMBER);
			
			if(null != request.getSession()) {
				eeObj = (EnrollmentEntity) request.getSession().getAttribute(ENTITYSEARCHPARAM);
			}
		}
		
		LOGGER.info("Inside EE manage list");
		Integer startRecord = 0;
		int currentPage = 1;
		if (param != null) {
			startRecord = (Integer.parseInt(param) - 1) * pageSize;
			currentPage = Integer.parseInt(param);
			model.addAttribute(PAGE_NUMBER, param);
		} else {
			model.addAttribute(PAGE_NUMBER, 1);
		}
		if (eeObj == null) {
			eeObj = enrollmentEntity;
		}
		
		try {
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setPageSize(pageSize);
			enrollmentEntityRequestDTO.setStartRecord(startRecord);
			enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntity);
			enrollmentEntityRequestDTO.setEntityName(pEntityName);
			enrollmentEntityRequestDTO.setStatus(pStatus);
			enrollmentEntityRequestDTO.setOrganizationType(pOrganizationType);
			enrollmentEntityRequestDTO.setReceivedPayments(pReceivedPayments);
			enrollmentEntityRequestDTO.setCountyServed(pCountyServed);

			enrollmentEntityRequestDTO.setLanguage(pLanguage);
			enrollmentEntityRequestDTO.setFromDate(pFromDate);
			enrollmentEntityRequestDTO.setToDate(pToDate);

			if (EntityUtils.isNullOrBlank(pZipCode)) {
				enrollmentEntityRequestDTO.setZipCode(pZipCode);
			}

			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.info("Calling ghix-entity to retrieve EE list");
			// Rest call for enrollment entity service
			String enrollEntityResponse = restClassCommunicator.retrieveEnrollmentEntityMap(requestJSON);
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
			LOGGER.info("Return from REST call.");

			Map<String, Object> entityListAndRecordCount = enrollmentEntityResponseDTO.getEntityListAndRecordCount();

			Map<EnrollmentEntity, Integer> eentityWithAssisterCount = (HashMap<EnrollmentEntity, Integer>) entityListAndRecordCount
					.get("enrollmententitymap");

			// Long iResultCt = (Long) entityListAndRecordCount.get("recordCount");
			// int iResultCount = iResultCt.intValue();

			EnrollmentEntitySearchParameters enrollmentEntitySearchParameters = setEnrollmentEntitySearchParameters(
					pEntityName, pStatus, null, pOrganizationType, pFromDate, pToDate, pLanguage, pZipCode, pCountyServed,
					pReceivedPayments);

			List<String> listLookupLabelValue = getLookupValueForRegistrationStatus(ENTITY_REGISTRATION_STATUS);
			listLookupLabelValue.add(EntityConstants.STATUS_INCOMPLETE);
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			
			Set<String> lookupCountiesServed = entityUtils.populateCountiesServedDataSet(stateCode);	
			
			// Fetching List of Languages
			List<LookupValue> listOflanguages = lookupService.getLookupValueList(ENTITY_LANGUAGE);
			
			setModalAndSession(enrollmentEntity, model, request, currentPage,
					enrollmentEntityResponseDTO, entityListAndRecordCount,
					eentityWithAssisterCount, enrollmentEntitySearchParameters,
					listLookupLabelValue, lookupCountiesServed, listOflanguages);
			
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while processing enrollmententityListForPagination : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while processing enrollmententity List For Pagination : ", exception);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error While Fetching EnrollmentEntity "));
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("Redirecting to EE manage list page");

		LOGGER.info("enrollmententityListForPagination: END");

		return "entity/entityadmin/manage";
	}

	/**
	 * Method to set Modal and Session attributes.
	 * 
	 * @param enrollmentEntity The current EnrollmentEntity instance.
	 * @param model The current Model instance.
	 * @param request The current HttpServletRequest instance.
	 * @param currentPage The current page.
	 * @param enrollmentEntityResponseDTO The current EnrollmentEntityResponseDTO instance.
	 * @param entityListAndRecordCount The entity list and record count collection.
	 * @param eentityWithAssisterCount The entity with Assister count collection.
	 * @param enrollmentEntitySearchParameters The current EnrollmentEntitySearchParameters instance.
	 * @param listLookupLabelValue The list of listLookupLabelValue.
	 * @param countiesServedData The set of counties served data.
	 * @param listOflanguages The list of languages.
	 */
	private void setModalAndSession(EnrollmentEntity enrollmentEntity,
			Model model, HttpServletRequest request, int currentPage,
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO,
			Map<String, Object> entityListAndRecordCount,
			Map<EnrollmentEntity, Integer> eentityWithAssisterCount,
			EnrollmentEntitySearchParameters enrollmentEntitySearchParameters,
			List<String> listLookupLabelValue,
			Set<String> countiesServedData,
			List<LookupValue> listOflanguages) {
		LOGGER.info("setModalAndSession: START");
		
		model.addAttribute("enrollmententitiesmap", eentityWithAssisterCount);
		model.addAttribute(RESULT_SIZE, entityListAndRecordCount.get(RECORD_COUNT));
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("enrollmentEntitySearchParameters", enrollmentEntitySearchParameters);
		model.addAttribute(STATUSLIST, listLookupLabelValue);
		model.addAttribute(LIST_OF_LANGUAGES, listOflanguages);
		model.addAttribute(ORGANIZATIONTYPELIST, getOrganizationTypes());
		model.addAttribute(TOTAL_ENROLLMENT_ENTITIES, enrollmentEntityResponseDTO.getTotalEnrollmentEntities());
		model.addAttribute(TOTAL_ENROLLMENT_ENTITIES_BY_SEARCH, enrollmentEntityResponseDTO.getTotalEnrollmentEntitiesBySearch());
		model.addAttribute(ENROLLMENT_ENTITIES_UP_FOR_RENEWAL, enrollmentEntityResponseDTO.getEnrollmentEntitiesUpForRenewal());
		model.addAttribute(INACTIVE_ENROLLMENT_ENTITIES, enrollmentEntityResponseDTO.getInactiveEnrollmentEntities());
		model.addAttribute(COUNTIES_SERVED_LIST, countiesServedData);
		
		request.getSession().setAttribute("enrollmententitiesmapsession", eentityWithAssisterCount);
		request.getSession().setAttribute("resultSizeSession", entityListAndRecordCount.get(RECORD_COUNT));
		request.getSession().setAttribute(ENTITYSEARCHPARAM, enrollmentEntity);
		request.getSession().setAttribute(LOGGED_USER, ENTITY_ADMIN);
		request.getSession().setAttribute(TOTAL_ENROLLMENT_ENTITIES, enrollmentEntityResponseDTO.getTotalEnrollmentEntities());
		request.getSession().setAttribute(ENROLLMENT_ENTITIES_UP_FOR_RENEWAL, enrollmentEntityResponseDTO.getEnrollmentEntitiesUpForRenewal());
		request.getSession().setAttribute(INACTIVE_ENROLLMENT_ENTITIES, enrollmentEntityResponseDTO.getInactiveEnrollmentEntities());
		request.getSession().removeAttribute(MENU);
		request.getSession().setAttribute(MENU, ENROLLMENT_ENTITY_MENU);

		LOGGER.info("setModalAndSession: END");
	}

	/**
	 * Method to set EnrollmentEntity search parameters.
	 * 
	 * @param entityName The entity's name.
	 * @param status The entity's status.
	 * @param organizationType The entities organiztion type
	 * @param fromDate The entity's from date.
	 * @param toDate The entity's to date.
	 * @param language The entity's languages.
	 * @param zipCode The entity's zip code.
	 * @param countyServed The entity's counties served.
	 * @param receivedPayments The entity's received payments attribute value.
	 * @return EnrollmentEntitySearchParameters The populated enrollmentEntitySearchParameters instance.
	 */
	private EnrollmentEntitySearchParameters setEnrollmentEntitySearchParameters(String entityName, String entityNumber, String status,
			String organizationType, String fromDate, String toDate, String language, String zipCode,
			String countyServed, String receivedPayments) {
		LOGGER.info("setEnrollmentEntitySearchParameters: START");

		EnrollmentEntitySearchParameters enrollmentEntitySearchParameters = new EnrollmentEntitySearchParameters();
		enrollmentEntitySearchParameters.setEntityName(entityName);
		enrollmentEntitySearchParameters.setEntityNumber(entityNumber);
		enrollmentEntitySearchParameters.setStatus(status);
		enrollmentEntitySearchParameters.setOrganizationType(organizationType);
		enrollmentEntitySearchParameters.setFromDate(fromDate);
		enrollmentEntitySearchParameters.setToDate(toDate);
		enrollmentEntitySearchParameters.setLanguage(language);
		enrollmentEntitySearchParameters.setZipCode(zipCode);
		enrollmentEntitySearchParameters.setCountyServed(countyServed);
		enrollmentEntitySearchParameters.setReceivedPayments(receivedPayments);

		LOGGER.info("setEnrollmentEntitySearchParameters: END");

		return enrollmentEntitySearchParameters;
	}

	/**
	 * Handles the view payment information of Entity
	 * 
	 * @param model
	 * @param request
	 * @param paymentId
	 * @return URL for navigation to appropriate Entity page
	 */
	@RequestMapping(value = "/entity/entityadmin/viewpaymentinfo/{id}", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_VIEW_ENTITY)
	public String viewpaymentinfo(Model model, HttpServletRequest request, @PathVariable("id") String encryptedId) {

		LOGGER.info("viewpaymentinfo: START");
		int id = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId));
		LOGGER.info("View Entity Payment Info ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: View Entity Payment Info");
		EnrollmentEntity enrollmentEntityObj = new EnrollmentEntity();
		PaymentMethods paymentMethodsObj = new PaymentMethods();
		
		try {
			EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequest.setModuleId(Integer.valueOf(id));
			LOGGER.info("Retrieving Enrollment Entity Detail By User Id REST Call Starts");
			String enrollmentEntityObjStr = restClassCommunicator.retrieveEnrollmentEntityObjectById(enrollmentEntityRequest);
			LOGGER.info("Retrieving Enrollment Entity Detail By User Id REST Call Ends");

			EnrollmentEntityResponseDTO enrollmentEntityResponse =  JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollmentEntityObjStr); 
			enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();
			
			Validate.notNull(enrollmentEntityObj);

			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityObj);
			if (!StringUtils.isBlank(request.getParameter(CANCEL))
					&& request.getParameter(CANCEL).trim().equalsIgnoreCase(TRUE)) {
				model.addAttribute(UPDATEPROFILE, false);
			}
			request.getSession().setAttribute(UPDATEPROFILE, "false");
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setModuleId(enrollmentEntityObj.getId());
			enrollmentEntityRequestDTO.setEntityType(ModuleUserService.ENROLLMENTENTITY_MODULE);
			LOGGER.info("Retrieving Payment Detail REST Call Starts");
			String paymentMethodsStr = restClassCommunicator.retrievePaymentDetails(enrollmentEntityRequestDTO);
			LOGGER.info("Retrieving Payment Detail REST Call Ends");

			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(paymentMethodsStr);

			paymentMethodsObj = enrollmentEntityResponseDTO.getPaymentMethods();

			Map<String, Boolean> map = null;
			if (enrollmentEntityObj != null) {
				getContactNumber(model, enrollmentEntityObj);
				map = getLeftNavigationMenuMap(enrollmentEntityObj);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			request.getSession().setAttribute(REGISTRATIONSTATUS, enrollmentEntityObj.getRegistrationStatus());
			model.addAttribute(PAYMENT_METHODS_OBJ, paymentMethodsObj);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing payment info : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing payment info in viewpaymentinfo method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("viewpaymentinfo: END");

		return "entity/enrollmententity/viewpaymentinfo";
	}

	/**
	 * Handles the Enrollment Entity Registration - Step 1 view page
	 * 
	 * @param model
	 *            ,
	 * @param request
	 * @return URL for navigation to View Entity Information page - step 1
	 */
	@RequestMapping(value = "/entity/entityadmin/viewentityinformation/{id}", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_VIEW_ENTITY)
	public String viewEntityInformation(Model model, HttpServletRequest request, @PathVariable("id") String encryptedId) {

		int id=  Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId));
		LOGGER.info("viewEntityInformation: START");

		LOGGER.info("View Entity Information");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Enrollment Entities : Entity Information");
		// model.addAttribute("isdlflag", "true");
		EnrollmentEntity enrollEntity = null;
		Map<String, Boolean> map = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setUserId(id);
		
		try {
			String enrollEntityResponse = restClassCommunicator.retrieveEnrollmentEntityObject(enrollmentEntityRequestDTO);

			enrollEntity = getAndSetEnrollmentEntity(model, enrollEntityResponse);

			if (enrollEntity != null) {
				getContactNumber(model, enrollEntity);
				map = getLeftNavigationMenuMap(enrollEntity);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			request.getSession().setAttribute(REGISTRATIONSTATUS, enrollEntity.getRegistrationStatus());
			request.getSession().setAttribute(ENROLLMENTENTITY, enrollEntity);
			model.addAttribute(ORGANIZATIONTYPELIST, getOrganizationTypes());
			model.addAttribute(COUNTIES_SERVED_LIST, getCountiesServed());
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing entity info : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing entity info inviewEntityInformation method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("viewEntityInformation: END");

		return "entity/enrollmententity/viewentityinformation";
	}

	/**
	 * Handles the site view
	 * 
	 * @param enrollmentEntity
	 *            ,
	 * @param request
	 * @param Model
	 * @param Id
	 * @return URL for navigation to site view page
	 */
	@RequestMapping(value = "/entity/entityadmin/viewsite/{id}", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_VIEW_ENTITY)
	public String viewSiteData(Model model, HttpServletRequest request, @PathVariable("id") String encryptedId) {

		LOGGER.info("viewSiteData: START");
		Integer id = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId));
		LOGGER.info("Entering viewSiteData method ");
		model.addAttribute(PAGE_TITLE, "Enrollment Entity: Site View");
		setModelAttributesForSite(model);
		// retrieves all sites associated with the entity based on the entity's
		// user id
		getAllSites(model, id);
		Map<String, Boolean> map = null;
		EnrollmentEntity enrollEntity = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setUserId(id);
		
		try {
			String enrollEntityResponse = restClassCommunicator.retrieveEnrollmentEntityObject(enrollmentEntityRequestDTO);

			enrollEntity = getAndSetEnrollmentEntity(model, enrollEntityResponse);

			if (enrollEntity != null) {
				getContactNumber(model, enrollEntity);
				map = getLeftNavigationMenuMap(enrollEntity);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			request.getSession().setAttribute(REGISTRATIONSTATUS, enrollEntity.getRegistrationStatus());
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing site info : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing site info in viewSiteData method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("Exiting viewSiteData method");

		LOGGER.info("viewSiteData: END");

		return "entity/entityadmin/viewsite";
	}

	/**
	 * Handles the add subsite functionality on viewsite page
	 * 
	 * @param Model
	 * @param request
	 * @return URL for navigation to addsubsite page
	 */
	@RequestMapping(value = "/entity/entityadmin/addsubsite/{entityid}", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ENTITY)
	public String addSubSite(Model model, @PathVariable("entityid") String entityid) {

		LOGGER.info("addSubSite: START");

		LOGGER.info("Entering addSubSite method ");
		
		try {
			String decryptId = ghixJasyptEncrytorUtil.decryptStringByJasypt(entityid);
			Integer id = Integer.parseInt(decryptId);
			setModelAttributesForSite(model);
			getAllSites(model, id);
			//When Entity Admin wants to add Subsite 
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setUserId(id);
			String enrollEntityResponseJSON = restClassCommunicator.retrieveEnrollmentEntityObject(enrollmentEntityRequestDTO);

			EnrollmentEntity enrollEntity = getAndSetEnrollmentEntity(model, enrollEntityResponseJSON);
			
			// Flag to identify on view page whether an add operation or edit for
			// subsite.
			model.addAttribute(ADDNEWSUBSITE, TRUE);
			model.addAttribute(LANGUAGES_LIST,  loadLanguages());
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while adding sub-site : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while adding sub-site in addSubSite method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("addSubSite: END");

		return "entity/entityadmin/viewsite";
	}

	private EnrollmentEntity getAndSetEnrollmentEntity(Model model, String enrollEntityResponse) throws JsonProcessingException, IOException {

		LOGGER.info("getAndSetEnrollmentEntity: START");

		EnrollmentEntity enrollEntity = null;
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
		enrollEntity = enrollmentEntityResponseDTO.getEnrollmentEntity();
		if (enrollEntity == null) {
			enrollEntity = new EnrollmentEntity();
		}
		model.addAttribute(ENROLLMENT_ENTITY, enrollEntity);

		LOGGER.info("getAndSetEnrollmentEntity: END");

		return enrollEntity;
	}

	@RequestMapping(value = "/entity/entityadmin/viewPopulationServed/{id}", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_VIEW_ENTITY)
	public String viewPopulationServed(Model model, HttpServletRequest request, @PathVariable("id") String encryptedId) {

		LOGGER.info("viewPopulationServed: START");
		int id = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId)); 
		LOGGER.info("View Population Served Details");
		Model modelObj = model;
		
		try {
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setUserId(id);

			String strEnrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			String populationServedDetails = restClassCommunicator
					.getPopulationServedDetails(strEnrollmentEntityRequestDTO);
			modelObj = getPopulationServedInformationAsList(populationServedDetails, model);
			EnrollmentEntity enrollEntity = null;
			enrollmentEntityRequestDTO.setUserId(id);
			String enrollEntityResponse = restClassCommunicator
					.getEnrollmentEntityContactDetails(enrollmentEntityRequestDTO);
			enrollEntity = getAndSetEnrollmentEntity(model, enrollEntityResponse);
			Map<String, Boolean> map = null;
			if (enrollEntity != null) {
				getContactNumber(model, enrollEntity);
				map = getLeftNavigationMenuMap(enrollEntity);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			request.getSession().setAttribute(REGISTRATIONSTATUS, enrollEntity.getRegistrationStatus());
			LOGGER.info("Received population served information");
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing population served : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing population served in viewPopulationServed method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("viewPopulationServed: END");

		return "entity/enrollmententity/viewpopulationserved";
	}

	/**
	 * Retrieve and set population served information as model attributes
	 * 
	 * @param populationServedDetails
	 * @param model
	 * @return {@link Model}
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private Model getPopulationServedInformationAsList(String populationServedDetails, Model model) throws JsonProcessingException, IOException {

		LOGGER.info("getPopulationServedInformationAsList: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(populationServedDetails);

		LOGGER.debug("Returning from REST call : " + enrollmentEntityResponseDTO.getResponseCode());

		Map<String, PopulationLanguage> languagesMap = enrollmentEntityResponseDTO.getPopulationServedWrapper()
				.getPopulationLanguages();
		List<PopulationLanguage> languages = extractLangListFromMap(languagesMap);
		model.addAttribute("langList", languages);

		Map<String, PopulationEthnicity> ethnicitiesMap = enrollmentEntityResponseDTO.getPopulationServedWrapper()
				.getPopulationEthnicities();
		List<PopulationEthnicity> ethnicities = extractEthnicityListFromMap(ethnicitiesMap);
		model.addAttribute("ethnicityList", ethnicities);

		Map<String, PopulationIndustry> industriesMap = enrollmentEntityResponseDTO.getPopulationServedWrapper()
				.getPopulationIndustries();
		List<PopulationIndustry> industries = extractIndustryListFromMap(industriesMap);
		model.addAttribute("industryList", industries);

		model.addAttribute(POPULATION_SERVED_WRAPPER, enrollmentEntityResponseDTO.getPopulationServedWrapper());

		model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityResponseDTO.getEnrollmentEntity());

		LOGGER.info("getPopulationServedInformationAsList: END");

		return model;
	}

	/**
	 * Handles the view entity contact information of Entity
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/entity/entityadmin/viewentitycontactinfo/{id}", method = { RequestMethod.GET,
			RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_VIEW_ENTITY)
	public String viewEntityContactInfo(Model model, HttpServletRequest request, @PathVariable("id") String encryptedId) {

		LOGGER.info("viewEntityContactInfo: START");
		int id = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId));
		LOGGER.info("View Entity Contact Information");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Enrollment Entities : Entity Information");

		EnrollmentEntity enrollEntity = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setUserId(id);
		
		try {
			String enrollEntityResponse = restClassCommunicator
					.getEnrollmentEntityContactDetails(enrollmentEntityRequestDTO);

			enrollEntity = getAndSetEnrollmentEntity(model, enrollEntityResponse);
			Map<String, Boolean> map = null;
			if (enrollEntity != null) {
				getContactNumber(model, enrollEntity);
				map = getLeftNavigationMenuMap(enrollEntity);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			request.getSession().setAttribute(REGISTRATIONSTATUS, enrollEntity.getRegistrationStatus());
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing entity contact info : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing entity contact info in viewEntityContactInfo method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("viewEntityContactInfo: END");

		return "entity/enrollmententity/viewentitycontactinfo";
	}

	/**
	 * Handles the view entity contact information of Entity
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/entity/entityadmin/viewlistofassisters/{id}", method = { RequestMethod.GET,
			RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_VIEW_ENTITY)
	public String viewListOfAssisters(Model model, HttpServletRequest request, @PathVariable("id") String encryptedId) {

		LOGGER.info("viewListOfAssisters: START");
		int id = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId));
		LOGGER.info("View Entity Contact Information");
		model.addAttribute(PAGE_TITLE, GETINSURED_HEALTH_EXCHANGE_ENROLLMENT_ENTITIES_VIEW_ASSISTERS);
		List<Assister> listOfAssisters = new ArrayList<Assister>();
		EnrollmentEntity enrollEntity = null;

		try {
			enrollEntity = getEnrollmentEntityByUserId(id, model);

			listOfAssisters = getListOfAssistersByEnrollmentEntity(enrollEntity);
			model.addAttribute(LIST_OF_ASSISTERS, listOfAssisters);
			model.addAttribute(ENROLLMENT_ENTITY, enrollEntity);

			Map<String, Boolean> map = null;
			if (enrollEntity != null) {
				getContactNumber(model, enrollEntity);
				map = getLeftNavigationMenuMap(enrollEntity);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			request.getSession().setAttribute(REGISTRATIONSTATUS, enrollEntity.getRegistrationStatus());
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing list of Assisters : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing list of Assisters inviewListOfAssisters method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("viewListOfAssisters: END");

		return "entity/admin/enrollmententity/viewassisterinfo";
	}

	/**
	 * Admin adds a new Assister for a particular Enrollment Entity
	 * 
	 * @param model
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/entity/entityadmin/addnewassisterbyadmin", method = { RequestMethod.GET })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ENTITY)
	public String addNewAssisterByAdmin(Model model, HttpServletRequest request,
			@RequestParam(value = "enrollmentEntityId", required = false) String encyptedenrollmentEntityId) {
		
		LOGGER.info("addNewAssisterByAdmin: START");

		LOGGER.info("Add Assister Information");
		model.addAttribute(PAGE_TITLE, GETINSURED_HEALTH_EXCHANGE_ENROLLMENT_ENTITIES_VIEW_ASSISTERS);
		List<Assister> listOfAssisters = new ArrayList<Assister>();
		EnrollmentEntity enrollEntity = null;
		List<Site> listOfPrimarySites = null;
		List<Site> listOfSecondarySites = null;
		List<Site> listOfSites = new ArrayList<Site>();
		String enrollmentEntityId=null;
		
		try {
			if(encyptedenrollmentEntityId!=null){
				enrollmentEntityId=ghixJasyptEncrytorUtil.decryptStringByJasypt(encyptedenrollmentEntityId);
			}
			
			enrollEntity = getEnrollmentEntityByUserId(Integer.valueOf(enrollmentEntityId), model);
			listOfAssisters = getListOfAssistersByEnrollmentEntity(enrollEntity);
			model.addAttribute(LIST_OF_ASSISTERS, listOfAssisters);
			String languageDetails =  loadLanguages();
			model.addAttribute(LANGUAGES_LIST, languageDetails);
			Map<String, Boolean> map = null;
			if (enrollEntity != null) {
				//getContactNumber(model, enrollEntity);
				map = getLeftNavigationMenuMap(enrollEntity);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);

			// Fetching Assister's Primary Site
			listOfPrimarySites = getListOfEnrollmentEntitySitesBySiteType(enrollEntity, Site.site_type.PRIMARY.toString());

			// Fetching Assister's Secondary/Sub-Sites
			listOfSecondarySites = getListOfEnrollmentEntitySitesBySiteType(enrollEntity, Site.site_type.SUBSITE.toString());

			listOfSites.addAll(listOfPrimarySites);
			listOfSites.addAll(listOfSecondarySites);

			model.addAttribute(LIST_OF_PRIMARY_SITES, listOfSites);
			model.addAttribute(LIST_OF_SECONDARY_SITES, listOfSites);
			// Fetching List of Languages
			setLanguage(model, ENTITY_LANGUAGE_SPOKEN, LIST_OF_LANGUAGES_SPOKEN);
			setLanguage(model, ENTITY_LANGUAGE_WRITTEN, LIST_OF_LANGUAGES_WRITTEN);
			model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
			model.addAttribute(LIST_OF_EDUCATIONS, getEducationList());
			model.addAttribute(IS_SHOW, "Add");
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while adding new Assister : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while adding new Assister By Admin : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("addNewAssisterByAdmin: END");

		return "entity/admin/enrollmententity/viewassisterinfo";
	}

	/**
	 * @param model
	 */
	private void setLanguage(Model model, String entityLanguageWritten, String listOfLanguages) {
		LookupValue spkLookupValue=null;
		//String entityLanguageWritten = ENTITY_LANGUAGE_WRITTEN;
		List<LookupValue> listOfLanguagesWritten = lookupService.getLookupValueList(entityLanguageWritten);
		for(LookupValue lookupValueLabel:listOfLanguagesWritten){
			if(lookupValueLabel.getLookupValueLabel().equalsIgnoreCase("English")){
				spkLookupValue=lookupValueLabel;
				listOfLanguagesWritten.remove(lookupValueLabel);
				break;
			}
			
		}
		Collections.sort(listOfLanguagesWritten);
		Collections.reverse(listOfLanguagesWritten);
		if(spkLookupValue!=null){
			listOfLanguagesWritten.add(0, spkLookupValue);
		}
		model.addAttribute(listOfLanguages, listOfLanguagesWritten);
	}

	/**
	 * Admin adds a new Assister for a particular Enrollment Entity
	 * 
	 * @param model
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/entity/entityadmin/editnewassisterbyadmin", method = { RequestMethod.GET })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ENTITY)
	public String editNewAssisterByAdmin(Model model,
			@RequestParam(value = "enrollmentEntityId", required = false) String encyptedenrollmentEntityId,
			@RequestParam(value = "assisterId", required = false) String encytptedassisterId) {

		LOGGER.info("editNewAssisterByAdmin: START");
		String enrollmentEntityId=null;
		String assisterId=null;
		String showPostalMailOption=FALSE;
		List<Assister> listOfAssisters = new ArrayList<Assister>();
		EnrollmentEntity enrollEntity = null;
		List<Site> listOfPrimarySites = null;
		List<Site> listOfSecondarySites = null;
		List<Site> listOfSites = new ArrayList<Site>();
		
		try {
			if(encyptedenrollmentEntityId!=null){
				enrollmentEntityId=ghixJasyptEncrytorUtil.decryptStringByJasypt(encyptedenrollmentEntityId);
			}
			if(encytptedassisterId!=null){
			 assisterId=ghixJasyptEncrytorUtil.decryptStringByJasypt(encytptedassisterId);
			}
			LOGGER.info("View Entity Contact Information");
			model.addAttribute(PAGE_TITLE, GETINSURED_HEALTH_EXCHANGE_ENROLLMENT_ENTITIES_VIEW_ASSISTERS);

			enrollEntity = getEnrollmentEntityByUserId(Integer.valueOf(enrollmentEntityId), model);
			listOfAssisters = getListOfAssistersByEnrollmentEntity(enrollEntity);
			model.addAttribute(LIST_OF_ASSISTERS, listOfAssisters);
			showPostalMailOption=EntityUtils.isAllaowMailNotices();
			model.addAttribute(SHOW_POSTAL_MAIL,showPostalMailOption);
			String languageDetails =  loadLanguages();
			model.addAttribute(LANGUAGES_LIST, languageDetails);
			// Edit page block
			if (assisterId != null) {

				// Fetching Assister's Primary Site
				listOfPrimarySites = getListOfEnrollmentEntitySitesBySiteType(enrollEntity,
						Site.site_type.PRIMARY.toString());

				// Fetching Assister's Secondary/Sub-Sites
				listOfSecondarySites = getListOfEnrollmentEntitySitesBySiteType(enrollEntity,
						Site.site_type.SUBSITE.toString());

				listOfSites.addAll(listOfPrimarySites);
				listOfSites.addAll(listOfSecondarySites);

				model.addAttribute(LIST_OF_PRIMARY_SITES, listOfSites);
				model.addAttribute(LIST_OF_SECONDARY_SITES, listOfSites);

				// Fetching List of Languages
				List<LookupValue> listOfLanguagesSpoken = lookupService.getLookupValueList(ENTITY_LANGUAGE_SPOKEN);
				model.addAttribute(LIST_OF_LANGUAGES_SPOKEN, listOfLanguagesSpoken);

				// Fetching List of Languages
				List<LookupValue> listOfLanguagesWritten = lookupService.getLookupValueList(ENTITY_LANGUAGE_WRITTEN);
				model.addAttribute(LIST_OF_LANGUAGES_WRITTEN, listOfLanguagesWritten);

				model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());

				model.addAttribute(LIST_OF_EDUCATIONS, getEducationList());

				// This flag is required to show the Add/Edit page
				model.addAttribute(IS_SHOW, "Edit");

				// Adding the list of languages to model
				AssisterResponseDTO response = setAssisterAndLanguages(model, Integer.valueOf(assisterId));
				Assister assister = response.getAssister();

				// Setting other spoken and written languages
				AssisterLanguages assisterLanguages = response.getAssisterLanguages();
				setOtherSpokenLanguage(assisterLanguages, listOfLanguagesSpoken, model);
				setOtherWrittenLanguage(assisterLanguages, listOfLanguagesWritten, model);

				// This flag is required to show certification number if
				// entered by
				// EE while registering the Assister
				if (assister.getCertificationNumber() != null && !"".equals(assister.getCertificationNumber())) {
					model.addAttribute(IS_CERTIFICATION_CERTIFIED, TRUE);
				}

				// Set language names for other language text box checking
				setLanguageNames(listOfLanguagesSpoken, listOfLanguagesWritten, model);
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while editing Assister : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while editing Assister By Admin: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("editNewAssisterByAdmin: END");

		return "entity/admin/enrollmententity/viewassisterinfo";
	}

	/**
	 * Entity Admin adds a new Assister for a particular Enrollment Entity
	 * 
	 * @param model
	 * @param assister
	 * @param assisterLanguages
	 * @param fileInput
	 * @param request
	 * @param enrollmentEntityId
	 * @return
	 */
	@RequestMapping(value = "/entity/entityadmin/addnewassisterbyadmin", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ENTITY)
	public String saveNewAssister(Model model, @ModelAttribute(ASSISTER) Assister assister,BindingResult assisterBindingResult,
			@ModelAttribute(ASSISTER_LANGUAGES)@Validated AssisterLanguages assisterLanguages,BindingResult languageBindingResult,
			@RequestParam(value = FILE_INPUT, required = false) CommonsMultipartFile fileInput, HttpServletRequest request) {

		LOGGER.info("saveNewAssister: START");

		LOGGER.info("Add New Assister Submit");
		String assisterId = null, assisterLanguageId = null, primaryAssisterSiteId = null, secondaryAssisterSiteId = null, primarySite = null, secondarySite = null, enrollmentEntityId = null,isAssisterCertified=null;
		
		if(null == request) { 
			LOGGER.warn(REQUEST_INSTANCE_IS_MISSING_FOR_THE_REQUEST);
		}
		else {
			assisterId = request.getParameter(ASSISTER_ID);
			assisterLanguageId = request.getParameter("assisterLanguageId");
			primaryAssisterSiteId = request.getParameter("primaryAssisterSite_id");
			secondaryAssisterSiteId = request.getParameter("secondaryAssisterSite_id");
			primarySite = request.getParameter("primarySite");
			secondarySite = request.getParameter("secondarySite");
			enrollmentEntityId = request.getParameter("enrollmentEntityId");
			isAssisterCertified=request.getParameter("isAssisterCertified");
		}
		
		
		model.addAttribute(PAGE_TITLE, "Add New Assister");

		stripXSS(assister, assisterLanguages);

		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		EnrollmentEntity enrollEntity = null;

		try {
			enrollEntity = getEnrollmentEntityByUserId(Integer.valueOf(enrollmentEntityId), model);
			
			setIdsForAssisterModels(assister, assisterLanguages, assisterId, assisterLanguageId, primarySite, secondarySite);
		
			// Associating EE to Assister
			assister.setEntity(enrollEntity);
			assisterRequestDTO.setAssister(assister);
			assisterRequestDTO.setAssisterLanguages(assisterLanguages);
		
			validateAddNewAssister(assisterBindingResult,languageBindingResult,assister,isAssisterCertified);
			
			// Saving Assister details
			AssisterResponseDTO assisterResponseDTO = saveAssisterDetails(assisterRequestDTO);
		
			// saving assister photo
			if (assisterResponseDTO != null && assisterResponseDTO.getAssister() != null)
			{
				entityService.saveAssisterPhoto(assisterResponseDTO.getAssister().getId(), fileInput);
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving Assister information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving Assister information in saveNewAssister method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("saveNewAssister: END");

		return "redirect:/entity/entityadmin/viewlistofassisters/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(enrollmentEntityId);
			
	}

	private void validateAddNewAssister(BindingResult assisterBindingResult,BindingResult languageBindingResult, Assister assister,String isAssisterCertified) {
		
		if(!EntityUtils.isEmpty(isAssisterCertified) && "Yes".equalsIgnoreCase(isAssisterCertified)){
			validator.validate(assister,assisterBindingResult,Assister.AddAssisterByEntity.class,Assister.AssisterCerticationNumber.class,Assister.AssisterInfoGroup.class);
		}else{
			validator.validate(assister,assisterBindingResult,Assister.AddAssisterByEntity.class,Assister.AssisterInfoGroup.class);
		}
		if(assisterBindingResult.hasErrors() || languageBindingResult.hasErrors()){
			 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
	}
	
	

	/**
	 * Method to set identifiers for Assister Models.
	 * 
	 * @param assister The current Assister instance.
	 * @param assisterLanguages The AssisterLanguages instance.
	 * @param assisterId The current assister identifier.
	 * @param assisterLanguageId The assister language identifier.
	 * @param primarySite The primary site.
	 * @param secondarySite The secondary site
	 */
	private void setIdsForAssisterModels(Assister assister, AssisterLanguages assisterLanguages, String assisterId, String assisterLanguageId, String primarySite, String secondarySite) {
		LOGGER.info("setIdsForAssisterModels: START");
		// Setting Assister Id to update Registration Information (Required
		// in
		// case of only Edit Operation)
		if (EntityUtils.isNullOrBlank(assisterId)) {
			assister.setId(Integer.parseInt(assisterId));
		}

		// Setting Assister Languages Id to update Languages (Required in
		// case
		// of only Edit Operation)
		if (EntityUtils.isNullOrBlank(assisterLanguageId)) {
			assisterLanguages.setId(Integer.valueOf(assisterLanguageId));
		}

		// Fetching selected Primary Site object and associating with
		// Assister
		if (EntityUtils.isNullOrBlank(primarySite)) {
			assister.setPrimaryAssisterSite(getSiteById(Integer.valueOf(primarySite)));
		}

		// Fetching selected Secondary/Sub Site object and associating with
		// Assister
		if (EntityUtils.isNullOrBlank(secondarySite)) {
			assister.setSecondaryAssisterSite(getSiteById(Integer.valueOf(secondarySite)));
		}
		LOGGER.info("setIdsForAssisterModels: END");
	}

	/**
	 * @param assister
	 * @param assisterLanguages
	 */
	private void stripXSS(Assister assister, AssisterLanguages assisterLanguages) {
		/** Check for XSS Attack - Starts */
		assister.setComments(XssHelper.stripXSS(assister.getComments()));
		assister.setBusinessLegalName(XssHelper.stripXSS(assister.getBusinessLegalName()));
		assister.setEmailAddress(XssHelper.stripXSS(assister.getEmailAddress()));
		assister.setFirstName(XssHelper.stripXSS(assister.getFirstName()));
		assister.setLastName(XssHelper.stripXSS(assister.getLastName()));
		assister.setPrimaryPhoneNumber(XssHelper.stripXSS(assister.getPrimaryPhoneNumber()));
		assister.setSecondaryPhoneNumber(XssHelper.stripXSS(assister.getSecondaryPhoneNumber()));

		if (assister.getCertificationNumber() != null) {
			String certNo = XssHelper.stripXSS(String.valueOf(assister.getCertificationNumber()));
			assister.setCertificationNumber(!StringUtils.isEmpty(certNo) ? Long.parseLong(certNo) : null);
		}
		if (assister.getMailingLocation() != null) {
			assister.getMailingLocation().setAddress1(XssHelper.stripXSS(assister.getMailingLocation().getAddress1()));
			assister.getMailingLocation().setAddress2(XssHelper.stripXSS(assister.getMailingLocation().getAddress2()));
			assister.getMailingLocation().setCity(XssHelper.stripXSS(assister.getMailingLocation().getCity()));
			if (assister.getMailingLocation().getZip() != null) {
				String zip = XssHelper.stripXSS(String.valueOf(assister.getMailingLocation().getZip()));
				assister.getMailingLocation().setZip(!StringUtils.isEmpty(zip) ? zip : null);
			}
		}

		if (assisterLanguages != null) {
			// Adding check for Assister languages
			assisterLanguages.setSpokenLanguages(XssHelper.stripXSS(assisterLanguages.getSpokenLanguages()));
			assisterLanguages.setWrittenLanguages(XssHelper.stripXSS(assisterLanguages.getWrittenLanguages()));
		}
		/** Check for XSS Attack - Ends */
	}

	private AssisterResponseDTO setAssisterAndLanguages(Model model, Integer assisterID) throws JsonProcessingException, IOException {

		LOGGER.info("setAssisterAndLanguages: START");

		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setId(assisterID);

		LOGGER.debug("Retrieving Assister Detail REST Call Starts.");
		String assisterDetailsResponse = restClassCommunicator.getAssisterDetail(assisterRequestDTO);
		AssisterResponseDTO assisterResponseDTO =  JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
		LOGGER.debug("Retrieving Assister Detail REST Call Ends.");

		model.addAttribute(ASSISTER, assisterResponseDTO.getAssister());
		model.addAttribute(ASSISTER_LANGUAGES, assisterResponseDTO.getAssisterLanguages());

		setContactNumber(model, assisterResponseDTO.getAssister());

		LOGGER.info("setAssisterAndLanguages: END");

		return assisterResponseDTO;
	}

	private void setOtherSpokenLanguage(AssisterLanguages assisterLanguages, List<LookupValue> listOfLanguagesSpoken,
			Model model) {

		LOGGER.info("setOtherSpokenLanguage: START");

		String[] spokenLanguages = assisterLanguages.getSpokenLanguages().split(",");
		List<String> listOfNames = new ArrayList<String>();
		for (LookupValue language : listOfLanguagesSpoken) {
			listOfNames.add(language.getLookupValueLabel());
		}
		StringBuilder strSpoken = new StringBuilder();
		for (String language : spokenLanguages) {
			if (!"".equals(language) && !listOfNames.contains(language)) {
				if (strSpoken.length() == 0) {
					strSpoken.append(language);
				} else {
					strSpoken.append(",");
					strSpoken.append(language);
				}
			}
		}
		if (strSpoken.length() != 0) {
			model.addAttribute(OTHER_SPOKEN_LANGUAGE, strSpoken);
		} else {
			model.addAttribute(OTHER_SPOKEN_LANGUAGE, null);
		}

		LOGGER.info("setOtherSpokenLanguage: END");
	}

	private void setOtherWrittenLanguage(AssisterLanguages assisterLanguages, List<LookupValue> listOfLanguagesWritten,
			Model model) {

		LOGGER.info("setOtherWrittenLanguage: START");

		String[] writtenLanguages = assisterLanguages.getWrittenLanguages().split(",");
		List<String> listOfNames = new ArrayList<String>();
		StringBuilder strWritten = new StringBuilder();
		for (LookupValue language : listOfLanguagesWritten) {
			listOfNames.add(language.getLookupValueLabel());
		}
		for (String language : writtenLanguages) {
			if (!"".equals(language) && !listOfNames.contains(language)) {
				if (strWritten.length() == 0) {
					strWritten.append(language);
				} else {
					strWritten.append(",");
					strWritten.append(language);
				}
			}
		}
		if (strWritten.length() != 0) {
			model.addAttribute(OTHER_WRITTEN_LANGUAGE, strWritten);
		} else {
			model.addAttribute(OTHER_WRITTEN_LANGUAGE, null);
		}

		LOGGER.info("setOtherWrittenLanguage: END");
	}

	private void setLanguageNames(List<LookupValue> listOfLanguagesSpoken, List<LookupValue> listOfLanguagesWritten,
			Model model) {

		LOGGER.info("setLanguageNames: START");

		List<String> listOfNames = new ArrayList<String>();
		List<String> listWrittenNames = new ArrayList<String>();
		for (LookupValue language : listOfLanguagesSpoken) {
			listOfNames.add(language.getLookupValueLabel());
		}
		for (LookupValue language : listOfLanguagesWritten) {
			listWrittenNames.add(language.getLookupValueLabel());
		}
		model.addAttribute(LANGUAGE_NAMES, listOfNames);
		model.addAttribute(LANGUAGE_WRITTEN_NAMES, listWrittenNames);

		LOGGER.info("setLanguageNames: END");
	}

	private EnrollmentEntity getEnrollmentEntityByUserId(Integer userId, Model model) throws JsonProcessingException, IOException {

		LOGGER.info("getEnrollmentEntityByUserId: START");

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setUserId(userId);
		String enrollEntityResponse = restClassCommunicator
				.getEnrollmentEntityContactDetails(enrollmentEntityRequestDTO);

		LOGGER.info("getEnrollmentEntityByUserId: END");

		return getAndSetEnrollmentEntity(model, enrollEntityResponse);
	}

	/**
	 * Retrieves population served information
	 * 
	 * @param enrollmentEntity
	 *            {@link EnrollmentEntity}
	 * @param model
	 *            Model object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to population served page
	 */
	@RequestMapping(value = "/entity/entityadmin/getPopulationServed/{entityid}", method = { RequestMethod.GET,
			RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_VIEW_ENTITY)
	public String getPopulationServed(Model model, HttpServletRequest request, @PathVariable("entityid") String entityid) {

		LOGGER.info("getPopulationServed: START");

		LOGGER.info("Retrieve population served information");
		String decryptId = ghixJasyptEncrytorUtil.decryptStringByJasypt(entityid);
		Integer id = Integer.parseInt(decryptId);
		Model modelObj = model;
		
		try {
			Map<String, Boolean> map = null;
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			// enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntity);
			enrollmentEntityRequestDTO.setUserId(id);

			String strEnrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			String populationServedDetails = restClassCommunicator
					.getPopulationServedDetails(strEnrollmentEntityRequestDTO);
			modelObj = getPopulationServedInformation(populationServedDetails, model);
			enrollmentEntityRequestDTO.setUserId(id);
			String enrollEntityResponseJSON = restClassCommunicator
					.retrieveEnrollmentEntityObject(enrollmentEntityRequestDTO);

			// Retrieve and add EE in model attribute
			EnrollmentEntity enrollEntity = getAndSetEnrollmentEntity(model, enrollEntityResponseJSON);
			//  This check is to be removed once IND 65 integrated.
			if (enrollEntity.getId() != 0) {
				getContactNumber(model, enrollEntity);
				map = getLeftNavigationMenuMap(enrollEntity);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			request.getSession().setAttribute(REGISTRATIONSTATUS, enrollEntity.getRegistrationStatus());
			LOGGER.info("Received population served information");
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving population served : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving population served in getPopulationServed method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("Display population served information");

		LOGGER.info("getPopulationServed: END");

		return "entity/enrollmententity/populationserved";

	}

	/**
	 * Handles the Enrollment Entity Registration - Step 1
	 * 
	 * @param model
	 *            ,
	 * @param request
	 * @return URL for navigation to Entity Information page - step 1
	 */
	@RequestMapping(value = "/entity/entityadmin/entityinformation/{encrytedEntitylistUserid}", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_VIEW_ENTITY)
	public String displayAddEntityInformation(Model model, HttpServletRequest request, @PathVariable("encrytedEntitylistUserid") String encrytedEntitylistUserid) {

		LOGGER.info("displayAddEntityInformation: START");
		
		try {
			Integer id =Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(encrytedEntitylistUserid));

			LOGGER.info("Entity Information step 1 page ");
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Enrollment Entities : Entity Information");
			EnrollmentEntity enrollEntity = null;
			Map<String, Boolean> map = null;
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setUserId(id);
			String enrollEntityResponseJSON = restClassCommunicator.retrieveEnrollmentEntityObject(enrollmentEntityRequestDTO);

			// Retrieve and add EE in model attribute
			enrollEntity = getAndSetEnrollmentEntity(model, enrollEntityResponseJSON);
			if (enrollEntity.getId() != 0) {
				getContactNumber(model, enrollEntity);
				map = getLeftNavigationMenuMap(enrollEntity);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			request.getSession().setAttribute(REGISTRATIONSTATUS, enrollEntity.getRegistrationStatus());
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			

			//request.getSession().setAttribute(ENROLLMENTENTITY, enrollEntity);

			model.addAttribute(ORGANIZATIONTYPELIST, getOrganizationTypes());
			model.addAttribute(COUNTIES_SERVED_LIST, entityUtils.loadCountiesServedData());
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving entity information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving entity information to display : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("displayAddEntityInformation: END");

		return "entity/enrollmententity/entityinformation";
	}

	private Site getSiteById(int siteId) {

		LOGGER.info("getSiteById: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();

		Site site = new Site();
		site.setId(siteId);

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setSite(site);

		try {
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.debug("Retrieving Site By ID REST Call Starts.");
			String listOfSitesResponse = restClassCommunicator.getSiteById(requestJSON);
			enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(listOfSitesResponse);
			LOGGER.debug("Retrieving Site By ID REST Call Ends.");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Retrieving Site By ID : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getSiteById: END");

		return enrollmentEntityResponseDTO.getSite();
	}

	private AssisterResponseDTO saveAssisterDetails(AssisterRequestDTO assisterRequestDTO) throws Exception {

		LOGGER.info("saveAssisterDetails: START");

		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		Assister assister = null;
		String request = null;
		
		try {
			//get logged in user
			user= userService.getLoggedInUser();
			//get assister object
			assister = assisterRequestDTO.getAssister();
			//check for update or new record operation
			if(assister.getId() == 0){
				// new record
				assister.setCreatedBy(user.getId());
			}else{
				//update
				assister.setUpdatedBy(user.getId());
			}
			
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class);
			request = writer.writeValueAsString(assisterRequestDTO);
			LOGGER.debug("Save Assister Detail REST Call Starts.");
			String assisterDetailsResponse = restClassCommunicator.saveAssisterDetails(request);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
			LOGGER.debug("Save Assister Detail REST Call Ends.");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Saving Assister Detail : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("saveAssisterDetails: END");

		return assisterResponseDTO;
	}

	private List<Site> getListOfEnrollmentEntitySitesBySiteType(EnrollmentEntity enrollmentEntity, String siteType) {

		LOGGER.info("getListOfEnrollmentEntitySitesBySiteType: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntity);
		enrollmentEntityRequestDTO.setSiteType(siteType);

		try {
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.debug("Retrieving Enrollment Entity List of Sites REST Call Starts.");
			String listOfSitesResponse = restClassCommunicator.getListOfEnrollmentEntitySitesBySiteType(requestJSON);
			enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(listOfSitesResponse);
			LOGGER.debug("Retrieving Enrollment Entity List of Sites REST Call Ends.");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Retrieving Enrollment Entity List of Sites : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getListOfEnrollmentEntitySitesBySiteType: END");

		return enrollmentEntityResponseDTO.getSiteList();
	}

	private List<String> getEducationList() {

		LOGGER.info("getEducationList: START");

		List<String> educationList = new ArrayList<String>();
		educationList.add("Up to 8th Grade");
		educationList.add("Some High School");
		educationList.add("High School Graduate");
		educationList.add("Some College");
		educationList.add("College Graduate");
		educationList.add("Two Year Associate Degree");
		educationList.add("Inapplicable/Not Ascertained");

		LOGGER.info("getEducationList: END");

		return educationList;
	}

	private Set<String> getOrganizationTypes() {

		LOGGER.info("getOrganizationTypes: START");

		Set<String> organizationTypes = new HashSet<String>();
		organizationTypes.add("School");
		organizationTypes.add("Faith-based organization");
		organizationTypes.add("Community-Based organization");
		organizationTypes.add("Community Clinic");
		organizationTypes.add("Public Partnership");
		organizationTypes.add("Private Partnership");

		LOGGER.info("getOrganizationTypes: END");

		return organizationTypes;
	}

	private void getContactNumber(Model model, EnrollmentEntity enrollEntity) {

		LOGGER.info("getContactNumber: START");

		if (enrollEntity.getPrimaryPhoneNumber() != null && (!"".equals(enrollEntity.getPrimaryPhoneNumber()))) {
			model.addAttribute("primaryPhone1", enrollEntity.getPrimaryPhoneNumber().substring(0, THREE));
			model.addAttribute("primaryPhone2", enrollEntity.getPrimaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute("primaryPhone3", enrollEntity.getPrimaryPhoneNumber().substring(SIX, TEN));
		}
		if (enrollEntity.getSecondaryPhoneNumber() != null && (!"".equals(enrollEntity.getSecondaryPhoneNumber()))) {
			model.addAttribute("secondaryPhone1", enrollEntity.getSecondaryPhoneNumber().substring(0, THREE));
			model.addAttribute("secondaryPhone2", enrollEntity.getSecondaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute("secondaryPhone3", enrollEntity.getSecondaryPhoneNumber().substring(SIX, TEN));
		}
		if (enrollEntity.getFaxNumber() != null && (!"".equals(enrollEntity.getFaxNumber()))) {
			model.addAttribute("faxNumber1", enrollEntity.getFaxNumber().substring(0, THREE));
			model.addAttribute("faxNumber2", enrollEntity.getFaxNumber().substring(THREE, SIX));
			model.addAttribute("faxNumber3", enrollEntity.getFaxNumber().substring(SIX, TEN));
		}
		if (enrollEntity.getPriContactPrimaryPhoneNumber() != null && (!"".equals(enrollEntity.getPriContactPrimaryPhoneNumber()))) {
			model.addAttribute("priContactPrimaryPhoneNumber1", enrollEntity.getPriContactPrimaryPhoneNumber()
					.substring(0, THREE));
			model.addAttribute("priContactPrimaryPhoneNumber2", enrollEntity.getPriContactPrimaryPhoneNumber()
					.substring(THREE, SIX));
			model.addAttribute("priContactPrimaryPhoneNumber3", enrollEntity.getPriContactPrimaryPhoneNumber()
					.substring(SIX, TEN));
		}
		if (enrollEntity.getPriContactSecondaryPhoneNumber() != null && (!"".equals(enrollEntity.getPriContactSecondaryPhoneNumber()))) {
			model.addAttribute("priContactSecondaryPhoneNumber1", enrollEntity.getPriContactSecondaryPhoneNumber()
					.substring(0, THREE));
			model.addAttribute("priContactSecondaryPhoneNumber2", enrollEntity.getPriContactSecondaryPhoneNumber()
					.substring(THREE, SIX));
			model.addAttribute("priContactSecondaryPhoneNumber3", enrollEntity.getPriContactSecondaryPhoneNumber()
					.substring(SIX, TEN));
		}
		if (enrollEntity.getFinPrimaryPhoneNumber() != null && (!"".equals(enrollEntity.getFinPrimaryPhoneNumber()))) {
			model.addAttribute("finPrimaryPhoneNumber1", enrollEntity.getFinPrimaryPhoneNumber().substring(0, THREE));
			model.addAttribute("finPrimaryPhoneNumber2", enrollEntity.getFinPrimaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute("finPrimaryPhoneNumber3", enrollEntity.getFinPrimaryPhoneNumber().substring(SIX, TEN));
		}
		if (enrollEntity.getFinFaxNumber() != null && (!"".equals(enrollEntity.getFinFaxNumber()))) {
			model.addAttribute("finFaxNumber1", enrollEntity.getFinFaxNumber().substring(0, THREE));
			model.addAttribute("finFaxNumber2", enrollEntity.getFinFaxNumber().substring(THREE, SIX));
			model.addAttribute("finFaxNumber3", enrollEntity.getFinFaxNumber().substring(SIX, TEN));
		}

		LOGGER.info("getContactNumber: END");
	}

	private List<String> getCountiesServed() {

		LOGGER.info("getCountiesServed: START");

		List<String> countiesServed = new ArrayList<String>();
		List<LookupValue> lookupCountiesServed = lookupService.getLookupValueList("ENTITY_COUNTIESSERVED");
		for (LookupValue l : lookupCountiesServed) {
			countiesServed.add(QUOTE + l.getLookupValueLabel() + QUOTE);
		}

		LOGGER.info("getCountiesServed: END");

		return countiesServed;
	}

	private void setModelAttributesForSite(Model model) {

		LOGGER.info("setModelAttributesForSite: START");
 
		List<LookupValue> fromTimelistOfhours = null;
		if(EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
			fromTimelistOfhours = lookupService.getLookupValueListForHoursOfOperation(HOURSOFOPERATIONFROMTIME);
		}else{
			fromTimelistOfhours = lookupService.getLookupValueListForHoursOfOperation(ENTITY_SITE_HOURS_TIME);
		}
		model.addAttribute(FROMTIMELIST, fromTimelistOfhours);

		List<LookupValue> toTimelistOfhours = null;
		if(EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
			toTimelistOfhours = lookupService.getLookupValueListForHoursOfOperation(HOURSOFOPERATIONTOTIME);
		}else{
			toTimelistOfhours = lookupService.getLookupValueListForHoursOfOperation(ENTITY_SITE_HOURS_TIME);
		}
		model.addAttribute(TOTIMELIST, toTimelistOfhours);
		List<LookupValue> listOfdays = lookupService.getLookupValueList(DAYSOFOPERATION);
		model.addAttribute(DAYSLIST, listOfdays);
		List<String> listOfDayNames = new ArrayList<String>();
		for (LookupValue l : listOfdays) {
			String dayName = l.getLookupValueLabel();
			listOfDayNames.add(dayName);
		}
		model.addAttribute(DAYSLISTNAMES, listOfDayNames);
		LookupValue lookupLang=null;
		List<LookupValue> listOflanguages = lookupService.getLookupValueList(ENTITYLANGUAGES);
		for(LookupValue value:listOflanguages){
		      if(value.getLookupValueLabel().equalsIgnoreCase("English")){
				lookupLang=value;
				listOflanguages.remove(value);
				break;
			}
		}
		Collections.sort(listOflanguages);
		Collections.reverse(listOflanguages);
        
  		if(lookupLang!=null){
			listOflanguages.add(0, lookupLang);
			}
		model.addAttribute(LISTOFLANGUAGES, listOflanguages);
		List<String> listOfLanguageNames = new ArrayList<String>();
		for (LookupValue l : listOflanguages) {
			String languageName = l.getLookupValueLabel();
			listOfLanguageNames.add(languageName);
		}

		model.addAttribute(LISTOFLANGUAGENAMES, listOfLanguageNames);
		model.addAttribute(STATELIST, new StateHelper().getAllStates());

		LOGGER.info("setModelAttributesForSite: END");
	}

	

	private void getSiteContactNumber(Model model, Site site) {

		LOGGER.info("getSiteContactNumber: START");

		if (site.getPrimaryPhoneNumber() != null && (!"".equals(site.getPrimaryPhoneNumber()))) {
			model.addAttribute("primaryPhone1", site.getPrimaryPhoneNumber().substring(0, THREE));
			model.addAttribute("primaryPhone2", site.getPrimaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute("primaryPhone3", site.getPrimaryPhoneNumber().substring(SIX, TEN));
		}
		if (site.getSecondaryPhoneNumber() != null && (!"".equals(site.getSecondaryPhoneNumber()))) {
			model.addAttribute("secondaryPhone1", site.getSecondaryPhoneNumber().substring(0, THREE));
			model.addAttribute("secondaryPhone2", site.getSecondaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute("secondaryPhone3", site.getSecondaryPhoneNumber().substring(SIX, TEN));
		}

		LOGGER.info("getSiteContactNumber: END");
	}

	/**
	 * Get population served information from entity response DTO
	 * 
	 * @param populationServedDetails
	 *            String returned from ghix-entity
	 * @param model
	 *            Model object
	 * @return Model object
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private Model getPopulationServedInformation(String populationServedDetails, Model model) throws JsonProcessingException, IOException {

		LOGGER.info("getPopulationServedInformation: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(populationServedDetails);

		LOGGER.info("Returning from REST call : " + enrollmentEntityResponseDTO.getResponseCode());

		PopulationServedWrapper populationServedWrapper = enrollmentEntityResponseDTO.getPopulationServedWrapper();

		List<LookupValue> lookupLanguages = lookupService.getLookupValueList(ENTITY_LANGUAGE);
		Collections.sort(lookupLanguages);
		Collections.reverse(lookupLanguages);
		List<LookupValue> lookupEthnicities = lookupService.getLookupValueList("ENTITY_ETHNICITY");

		List<LookupValue> lookupIndustries = lookupService.getLookupValueList("ENTITY_INDUSTRY");

		getOtherPopulationServedData(populationServedWrapper, lookupLanguages, lookupEthnicities, lookupIndustries,
				model);

		model.addAttribute(LANGUAGES_LOOKUP_LIST, lookupLanguages);
		model.addAttribute(ETHNICITIES_LOOKUP_LIST, lookupEthnicities);
		model.addAttribute(INDUSTRIES_LOOKUP_LIST, lookupIndustries);
		model.addAttribute(POPULATION_SERVED_WRAPPER, enrollmentEntityResponseDTO.getPopulationServedWrapper());

		model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityResponseDTO.getEnrollmentEntity());

		LOGGER.info("getPopulationServedInformation: END");

		return model;
	}

	/**
	 * Get other data saved for population served
	 * 
	 * @param populationServedWrapper
	 * @param lookupLanguages
	 * @param lookupEthnicities
	 * @param lookupIndustries
	 * @param model
	 */
	private void getOtherPopulationServedData(PopulationServedWrapper populationServedWrapper,
			List<LookupValue> lookupLanguages, List<LookupValue> lookupEthnicities, List<LookupValue> lookupIndustries,
			Model model) {

		LOGGER.info("getOtherPopulationServedData: START");

		PopulationData[] languages = getLookUpData(populationServedWrapper.getPopulationLanguages(), lookupLanguages, new PopulationLanguage(), FOUR);
		// Creating array of 2 objects to add other population ethnicities
		PopulationData[] ethnicities = getLookUpData(populationServedWrapper.getPopulationEthnicities(), lookupEthnicities, new PopulationEthnicity(), 2);
		// Creating array of 2 objects to add other population industries
		PopulationData[] industries = getLookUpData(populationServedWrapper.getPopulationIndustries(), lookupIndustries, new PopulationIndustry(), 2);
		
		model.addAttribute("langList", languages);
		model.addAttribute("ethnicityList", ethnicities);
		model.addAttribute("industryList", industries);

		LOGGER.info("getOtherPopulationServedData: END");
	}

	/**
	 * @param <E>
	 * @param <F>
	 * @param <G>
	 * @param map
	 * @param lookupLanguages
	 * @return
	 */
	private <E extends PopulationData> PopulationData[] getLookUpData(
			Map<String, E> map,
			List<LookupValue> lookupLanguages, E e, int arraySize) {
		// Creating array of 4 objects to add other population languages
		PopulationData[] languages = new PopulationData[arraySize];
		List<E> associatedLanguages = extractLangListFromMap(map);

		// Creating list of languages available in lookup table
		List<String> langLookupValueList = new ArrayList<String>();
		for (LookupValue lookupLang : lookupLanguages) {
			langLookupValueList.add(lookupLang.getLookupValueLabel());
		}

		// If language other than lookup table is available, then add it into
		// array else add new object to avoid null pointer exception
		int i = 0;
		for (E lang : associatedLanguages) {
			if (i < arraySize) {
				if (!langLookupValueList.contains(lang.getValue())) {
					languages[i] = lang;
					i++;
				} else {
					languages[i] = e;
				}
			}
		}
		return languages;
	}

	/**
	 * Extracts Population Languages list associated with EE from languages map
	 * @param <E>
	 * 
	 * @param languagesMap
	 * @return {@link List} of {@link PopulationLanguage}
	 */
	private <E extends PopulationData> List<E> extractLangListFromMap(Map<String, E> languagesMap) {

		LOGGER.info("extractLangListFromMap: START");

		List<E> languages = new ArrayList<E>();
		if (languagesMap != null) {
			Set<String> langKeys = languagesMap.keySet();
			Iterator<String> langItr = langKeys.iterator();
			String langKey;
			E langValue;
			while (langItr.hasNext()) {
				langKey = langItr.next();
				langValue = languagesMap.get(langKey);
				languages.add(langValue);
			}
		}

		LOGGER.info("extractLangListFromMap: END");

		return languages;
	}

	/**
	 * Extracts Population Ethnicities list associated with EE from ethnicites
	 * map
	 * 
	 * @param ethnicitiesMap
	 * @return {@link List} of {@link PopulationEthnicity}
	 */
	private List<PopulationEthnicity> extractEthnicityListFromMap(Map<String, PopulationEthnicity> ethnicitiesMap) {

		LOGGER.info("extractEthnicityListFromMap: START");

		List<PopulationEthnicity> ethnicities = new ArrayList<PopulationEthnicity>();
		if (ethnicitiesMap != null) {
			Set<String> ethnicityKeys = ethnicitiesMap.keySet();
			Iterator<String> ethnicityItr = ethnicityKeys.iterator();
			String ethnicityKey;
			PopulationEthnicity ethnicityValue;
			while (ethnicityItr.hasNext()) {
				ethnicityKey = ethnicityItr.next();
				ethnicityValue = ethnicitiesMap.get(ethnicityKey);
				ethnicities.add(ethnicityValue);
			}
		}

		LOGGER.info("extractEthnicityListFromMap: END");

		return ethnicities;
	}

	/**
	 * Extracts Population Industries list associated with EE from industries
	 * map
	 * 
	 * @param industriesMap
	 * @return {@link List} of {@link PopulationIndustry}
	 */
	private List<PopulationIndustry> extractIndustryListFromMap(Map<String, PopulationIndustry> industriesMap) {

		LOGGER.info("extractIndustryListFromMap: START");

		List<PopulationIndustry> industries = new ArrayList<PopulationIndustry>();
		if (industriesMap != null) {
			Set<String> industryKeys = industriesMap.keySet();
			Iterator<String> industryItr = industryKeys.iterator();
			String industryKey;
			PopulationIndustry industryValue;
			while (industryItr.hasNext()) {
				industryKey = industryItr.next();
				industryValue = industriesMap.get(industryKey);
				industries.add(industryValue);
			}
		}

		LOGGER.info("extractIndustryListFromMap: END");

		return industries;
	}

	private List<Assister> getListOfAssistersByEnrollmentEntity(EnrollmentEntity enrollmentEntity) {

		LOGGER.info("getListOfAssistersByEnrollmentEntity: START");

		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		
		assisterResponseDTO = getAssisterResponseDTOByEnrollmentEntity(enrollmentEntity);

		LOGGER.info("getListOfAssistersByEnrollmentEntity: END");
		
		return assisterResponseDTO.getListOfAssisters();
	}
	
	/**
	 * Method to get list of assisters with client count by enrollment entity
	 * @param enrollmentEntity enrollment entity object
	 * @return list of assisters with client count by enrollment entity
	 */
	private List<Assister> getListOfAssistersWithClientCountByEnrollmentEntity(EnrollmentEntity enrollmentEntity) {
		LOGGER.info("getListOfAssistersWithClientCountByEnrollmentEntity: START");

		List<Assister> assisterListWithClientCount = new ArrayList<Assister>();
		
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		
		assisterResponseDTO = getAssisterResponseDTOByEnrollmentEntity(enrollmentEntity);
		
		assisterListWithClientCount = getAssistersWithClientCountFromAssisterResponseDTO(assisterResponseDTO);

		LOGGER.info("getListOfAssistersWithClientCountByEnrollmentEntity: END");
		
		return assisterListWithClientCount;
	}
	
	/**
	 * Method to get list of assisters with client count from assisterResponseDTO
	 * @param assisterResponseDTO assister response DTO object
	 * @return list of assisters with client count
	 */
	private List<Assister> getAssistersWithClientCountFromAssisterResponseDTO(AssisterResponseDTO assisterResponseDTO) {
		
		LOGGER.info("getAssistersWithClientCountFromAssisterResponseDTO: START");

		List<Assister> assisterList = null;
		Map<Integer, Integer> assisterCountMap = null;

		assisterList = assisterResponseDTO.getListOfAssisters();
		assisterCountMap = assisterResponseDTO.getAssisterClientCountMap();
		
		Integer clientCount = -1;
		
		if (assisterList != null && !assisterList.isEmpty() && assisterCountMap != null && !assisterCountMap.isEmpty()) {
			for ( Assister assister :  assisterList ) {
				clientCount = 0;
				
				if (assisterCountMap.containsKey(assister.getId()) && assisterCountMap.get(assister.getId()) != null && assisterCountMap.get(assister.getId()) > -1 ) {
						clientCount = assisterCountMap.get(assister.getId());
				}
				
				assister.setClientCount(EMPTY_STRING+clientCount);
			}
		}
		
		LOGGER.info("getAssistersWithClientCountFromAssisterResponseDTO: END");
		
		return assisterList;
	}
	
	
	/**
	 * Method to get AssisterResponseDTO by EnrollmentEntity
	 * @param enrollmentEntity enrollment entity object
	 * @return Assister Response DTO object
	 */
	private AssisterResponseDTO getAssisterResponseDTOByEnrollmentEntity(EnrollmentEntity enrollmentEntity) {
		
		LOGGER.info("getAssisterResponseDTOByEnrollmentEntity: START");

		AssisterRequestDTO assisterRequestDTO = null;
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		String requestXML = null;
		
		try {
			assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setAdmin(true);
			assisterRequestDTO.setEnrollmentEntity(enrollmentEntity);

			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class);
			requestXML = writer.writeValueAsString(assisterRequestDTO);

			LOGGER.debug("Retrieving List Of Assisters By Enrollment Entity REST Call Starts.");
			String listOfAssistersResponse = restClassCommunicator.getListOfAssistersByEnrollmentEntity(requestXML);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(listOfAssistersResponse);
			LOGGER.debug("Retrieving List Of Assisters By Enrollment Entity REST Call Ends.");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while retrieving List Of Assisters By Enrollment Entity in AssisterResponseDTOByEnrollmentEntity: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getAssisterResponseDTOByEnrollmentEntity: END");

		return assisterResponseDTO;

	}

	private Map<String, Boolean> getLeftNavigationMenuMap(EnrollmentEntity enrollmentEntity) {

		LOGGER.info("getLeftNavigationMenuMap: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = null;
		Map<String, Boolean> map = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntity);

		try {
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.debug("Populate Left Navigation Map REST Call Starts.");
			String response = restClassCommunicator.populateNavigationMenuMap(requestJSON);
			enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(response);
			map = enrollmentEntityResponseDTO.getNavigationMenuMap();
			LOGGER.debug("Populate Left Navigation Map REST Call Ends.");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Populating Left Navigation Map : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getLeftNavigationMenuMap: END");

		return map;
	}

	/**
	 * Handles the view entity contact information of Entity
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/entity/entityadmin/displayassisters", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ENTITY_VIEW_ASSISTER')")
	public String listOfEnrollmentEntityAssisters(Model model, HttpServletRequest request) {

		LOGGER.info("listOfEnrollmentEntityAssisters: START");

		LOGGER.info(RETRIEVING_LIST_OF_ASSISTERS_ASSOCIATED_WITH_THE_LOGGED_IN_ENROLLMENT_ENTITY);
		model.addAttribute(PAGE_TITLE, GETINSURED_HEALTH_EXCHANGE_ENROLLMENT_ENTITIES_VIEW_ASSISTERS);

		List<Assister> listOfAssisters = new ArrayList<Assister>();
		request.getSession().setAttribute("showCECLeftnavigation",TRUE);
		EnrollmentEntity enrollEntity = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		Assister assister = new Assister();
		Integer pageSize = VAL_PAGE_SIZE;
		request.setAttribute(PAGE_SIZE, pageSize);
		request.setAttribute(REQ_URI, LIST);
		String param = request.getParameter(PAGE_NUMBER);
		Integer startRecord = 0;
		int countOfAssisters;
		String firstName = null, lastName = null, entityName = null, certificationStatus = null, fromDate = null, toDate = null, statusActive = null, statusInactive = null;
		int currentPage = 1;
		if (param != null) {
			startRecord = (Integer.parseInt(param) - 1) * pageSize;
			currentPage = Integer.parseInt(param);
			model.addAttribute(PAGE_NUMBER, param);
		} else {
			model.addAttribute(PAGE_NUMBER, 1);
		}
		try {
			user = userService.getLoggedInUser();
			
			if (user != null) {
				enrollmentEntityRequestDTO.setModuleId(user.getId());

				LOGGER.debug("Retrieving Enrollment Entity REST Call Starts.");
				String enrollEntityResponse = restClassCommunicator
						.retrieveEnrollmentEntityObjectByUserId(enrollmentEntityRequestDTO);
				LOGGER.debug("Retrieving Enrollment Entity REST Call Ends.");

				enrollEntity = getAndSetEnrollmentEntity(model, enrollEntityResponse);
				if(EntityUtils.isEnrollmentEntityCertifiedAndActive(enrollEntity)){
					throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
				}
				model.addAttribute(ENROLLMENT_ENTITY, enrollEntity);

				AssisterRequestDTO assisterRequestDTO = setAndProcessAssisterRequestDTO(model, request, enrollEntity, assister, pageSize, startRecord);
				
				AssisterResponseDTO assisterResponseDTO = retrieveSearchAssisterList(assisterRequestDTO);
			
				populateAssisterCertificationsStatuses(model);
				
				model.addAttribute(ASSISTERLIST,assisterResponseDTO.getListOfAssisters());
				listOfAssisters = assisterResponseDTO.getListOfAssisters();
			
				model.addAttribute(CURRENT_PAGE, currentPage);
				request.getSession().setAttribute("assistersearchparam", assister);
				
				model.addAttribute(TOTAL_ASSISTERS,
						assisterResponseDTO.getTotalAssisters());
				model.addAttribute(TOTAL_ASSISTERS_BY_SEARCH,
						assisterResponseDTO.getTotalAssistersBySearch());
				assisterRequestDTO.setEnrollmentEntity(enrollEntity);
				
				assisterResponseDTO = getListOfAssistersByEnrollmentEntity(assisterRequestDTO);
				
				countOfAssisters=assisterResponseDTO.getTotalAssisters();
				
				setListOfAssistersInfoForListOfEnrollmentEntityAssisters(model, request, listOfAssisters, countOfAssisters, assisterResponseDTO);
				
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters = new HashMap<String, Object>();
				parameters.put("ASSITER_FIRST_NAME", firstName);
				parameters.put("ASSITER_LAST_NAME", lastName);
				parameters.put(ENTITY_NAME, entityName);
				parameters.put(CERTIFICATION_STATUS, certificationStatus);
				parameters.put(FROM_DATE, fromDate);
				parameters.put(TO_DATE, toDate);
				parameters.put(STATUS_ACTIVE, statusActive);
				parameters.put(STATUS_INACTIVE, statusInactive);
				
				AssisterSearchParameters assisterSearchParameters = setAssisterSearchParameters(parameters);
				
				model.addAttribute(ASSISTER_SEARCH_PARAMETERS, assisterSearchParameters);
				List<LookupValue> statusValuelist = lookupService.getLookupValueList(ASSISTERSTATUS);
				List<String> statuslist = new ArrayList<String>();
				
				populateAndSetStatuslist(model, statusValuelist, statuslist);
				
				if (userService.isEnrollmentEntity(user)) {
					request.getSession().setAttribute("assisterEnrollmentEntity", enrollEntity);
				}
			} else {
				LOGGER.error("User not logged in");
			}
			
			model.addAttribute("ID_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE));
			model.addAttribute("NV_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE));
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
		}catch(AccessDeniedException accessDeniedException){
			LOGGER.error("Exception occured while accepting designation request for Employer / Individual : ", accessDeniedException);
			throw accessDeniedException;
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while displaying assister : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while displaying list of Enrollment Entity Assisters : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("listOfEnrollmentEntityAssisters: END");

		return ENTITY_ENTITYADMIN_DISPLAYASSISTERS;
	}
	
	/**
	 * Method to populate and store EnrollmentEntities count.
	 * 
	 * @param model The Model instance.
	 * @param request The HttpServletRequest instance.
	 * @param countOfAssisters The count of Assisters's.
	 * @param listOfAssisters The List of Assisters's collection.
	 */
	private void populateAndStoreEnrollmentEntitiesCount(Model model,
			HttpServletRequest request, int countOfAssisters,
			List<Assister> listOfAssisters) {
		if (listOfAssisters != null && !listOfAssisters.isEmpty()) {
			model.addAttribute(TOTAL_ENROLLMENT_ENTITIES_BY_SEARCH, countOfAssisters);
			model.addAttribute(RESULT_SIZE,countOfAssisters);
			request.getSession().setAttribute(RESULT_SIZE,countOfAssisters);
		} else {
			model.addAttribute(TOTAL_ENROLLMENT_ENTITIES_BY_SEARCH, 0);
			model.addAttribute(RESULT_SIZE, 0);
			request.getSession().setAttribute(RESULT_SIZE, 0);
		}
	}
	
	/**
	 * Method to set list of Assisters's information for 'listOfEnrollmentEntityAssisters'.
	 *  
	 * @param model The Model instance.
	 * @param request The HttpServletRequest instance.
	 * @param listOfAssisters The List of Assisters's collection.
	 * @param countOfAssisters The Assisters's count
	 * @param assisterResponseDTO The AssisterResponseDTO instance.
	 */
	private void setListOfAssistersInfoForListOfEnrollmentEntityAssisters(
			Model model, HttpServletRequest request,
			List<Assister> listOfAssisters, int countOfAssisters,
			AssisterResponseDTO assisterResponseDTO) {
		if (listOfAssisters != null && !listOfAssisters.isEmpty()) {
			model.addAttribute(TOTAL_ENROLLMENT_ENTITIES_BY_SEARCH, countOfAssisters);
			model.addAttribute(RESULT_SIZE, assisterResponseDTO.getTotalAssisters());
			request.getSession().setAttribute(RESULT_SIZE,assisterResponseDTO.getTotalAssisters());
		} else {
			model.addAttribute(TOTAL_ENROLLMENT_ENTITIES_BY_SEARCH, 0);
			model.addAttribute(RESULT_SIZE, 0);
			request.getSession().setAttribute(RESULT_SIZE, 0);
		}
	}
	
	/**
	 * @param model
	 * @param statusValuelist
	 * @param statuslist
	 */
	private void populateAndSetStatuslist(Model model,
			List<LookupValue> statusValuelist, List<String> statuslist) {
		for (LookupValue val : statusValuelist) {
			statuslist.add(val.getLookupValueLabel());
			model.addAttribute(STATUSLIST_MODEL, statuslist);
		}
	}
	/**
	 * Method to set and process Assister request DTO.
	 * 
	 * @param model The Model instance.
	 * @param request The HttpServletRequest instance.
	 * @param enrollEntity The EnrollmentEntity instance.
	 * @param assister The Assister instance.
	 * @param pageSize The page size.
	 * @param startRecord The start record number.
	 * @return assisterRequestDTO The AssisterRequestDTO instance.
	 */
	private AssisterRequestDTO setAndProcessAssisterRequestDTO(Model model,
			HttpServletRequest request, EnrollmentEntity enrollEntity,
			Assister assister, Integer pageSize, Integer startRecord) {
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setPageSize(pageSize);
		assisterRequestDTO.setStartRecord(startRecord);
		assisterRequestDTO.setAssister(assister);
		assisterRequestDTO.setAdmin(false);
		assisterRequestDTO.setEnrollmentEntityId(enrollEntity.getId());
		String sortBy =  (request.getParameter(SORT_BY) == null) ?  "FIRST_NAME" : request.getParameter(SORT_BY);
		String sortOrder = determineSortOrder(request);

		request.removeAttribute(CHANGE_ORDER);
		
		model.addAttribute(SORT_ORDER, sortOrder);
		model.addAttribute(SORT_BY_COL, sortBy);
		
		assisterRequestDTO.setSortBy(sortBy);
		assisterRequestDTO.setSortOrder(sortOrder);
		return assisterRequestDTO;
	}
	
	/**
	 * @param assisterRequestDTO
	 * @return
	 * @throws IOException 
	 */
	private AssisterResponseDTO getListOfAssistersByEnrollmentEntity(
			AssisterRequestDTO assisterRequestDTO) throws IOException {
		AssisterResponseDTO assisterResponseDTO;
		
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class);
		String requestXML = writer.writeValueAsString(assisterRequestDTO);

		LOGGER.debug("Retrieving List Of Assisters By Enrollment Entity REST Call Starts.");
		String listOfAssistersResponse = restClassCommunicator.getListOfAssistersByEnrollmentEntity(requestXML);
		assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(listOfAssistersResponse);
		LOGGER.debug("Retrieving List Of Assisters By Enrollment Entity REST Call Ends.");
		return assisterResponseDTO;
	}

	/**
	 * Method to set AssisterSearchParameters instance.
	 * 
	 * @param parameters The parameter map.
	 * @return assisterSearchParameters The AssisterSearchParameters instance.
	 */
	private AssisterSearchParameters setAssisterSearchParameters(Map<String, Object> parameters) {

		LOGGER.info("setAssisterSearchParameters: START");

		String firstName = null, lastName = null, entityName = null, certificationStatus = null, fromDate = null, toDate = null, statusActive = null, statusInActive = null;
		
		if(null != parameters) {
			firstName = (String) parameters.get(FIRST_NAME);
			lastName = (String) parameters.get(LAST_NAME);
			entityName = (String) parameters.get(ENTITY_NAME);
			certificationStatus = (String) parameters.get(CERTIFICATION_STATUS);
			fromDate = (String) parameters.get(FROM_DATE);
			toDate = (String) parameters.get(TO_DATE);
			statusActive = (String) parameters.get(STATUS_ACTIVE);
			statusInActive = (String) parameters.get(STATUS_INACTIVE);
		}
		
		AssisterSearchParameters assisterSearchParameters = new AssisterSearchParameters();
		assisterSearchParameters.setFirstName(firstName);
		assisterSearchParameters.setLastName(lastName);
		assisterSearchParameters.setEntityName(entityName);
		assisterSearchParameters.setCertificationStatus(certificationStatus);
		assisterSearchParameters.setFromDate(fromDate);
		assisterSearchParameters.setToDate(toDate);
		assisterSearchParameters.setStatusActive(statusActive);
		assisterSearchParameters.setStatusInActive(statusInActive);

		LOGGER.info("setAssisterSearchParameters: END");

		return assisterSearchParameters;
	}

	private AssisterResponseDTO retrieveSearchAssisterList(AssisterRequestDTO assisterRequestDTO) throws IOException {
		LOGGER.info("retrieveSearchAssisterList: START");
		String requestXML = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class).writeValueAsString(assisterRequestDTO);
		String response = restClassCommunicator.retrieveSearchAssisterList(requestXML);
		AssisterResponseDTO assisterResponseDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class).readValue(response);
		LOGGER.info("retrieveSearchAssisterList: END");
		return assisterResponseDTO;
	}
	
	/**
	 * Method to populate Assister Certification statuses.
	 * 
	 * @param model The model instance.
	 */
	private void populateAssisterCertificationsStatuses(Model model) {
		LOGGER.info("populateAssisterCertificationsStatuses: START");
		List<LookupValue> statusValuelist = lookupService.getLookupValueList("ASSISTER_CERTIFICATION_STATUS");
		List<String> statuslist = new ArrayList<String>();

		for (LookupValue val : statusValuelist) {
			statuslist.add(val.getLookupValueLabel());
			model.addAttribute(STATUSLIST, statuslist);
		}
		LOGGER.info("populateAssisterCertificationsStatuses: END");
	}
		
	
	/* Get Method to display assister information based on id */

	@RequestMapping(value = "/entity/entityadmin/assisterinformation", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER)
	public String viewAssisterInformation(Model model,
			@RequestParam(value = ASSISTER_ID, required = false) String encryptedAssisterId, 
			@RequestParam(value = ENTITY_ID, required = false) String encryptedEntityId) throws GIException {

		LOGGER.info("viewAssisterInformation: START");

		LOGGER.info("View Assister Information");
		String assisterId = null;
		String entityId = null;
		String showPostalMailOption=FALSE;
		AssisterResponseDTO assisterResponseDTO = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		String assisterDetailsResponse = null;
		EnrollmentEntity enrollmentEntity = null;
		int userId = 0;
		
		try {
			
			assisterId = decryptIdentifier(encryptedAssisterId);
			
			entityId = decryptIdentifier(encryptedEntityId);
			
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Assister : Assister Information");

			showPostalMailOption=EntityUtils.isAllaowMailNotices();
			model.addAttribute(SHOW_POSTAL_MAIL,showPostalMailOption);
			
			user = userService.getLoggedInUser();
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			
			if(entityId != null) {
				enrollmentEntityRequestDTO.setModuleId(Integer.parseInt(entityId));
				String enrollEntityResponse = restClassCommunicator.retrieveEnrollmentEntityObjectById(enrollmentEntityRequestDTO);
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO =  JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
				if (enrollmentEntityResponseDTO.getEnrollmentEntity().getUser() != null) {
					userId = enrollmentEntityResponseDTO.getEnrollmentEntity().getUser().getId();
				}
			}
			else {
				enrollmentEntity = getEnrollmentEntityByUserId(user.getId(), model);
			}
			
			assisterResponseDTO = retrieveAssistersDetailsForExchangeAdmin(assisterId, assisterRequestDTO, assisterDetailsResponse);

			EntityUtils.checkEntityAssisterAssociation(assisterId, user, assisterResponseDTO);
		
			Assister assister = assisterResponseDTO.getAssister();
			model.addAttribute(ASSISTER, assister);
			model.addAttribute(ASSISTER_LANGUAGES, assisterResponseDTO.getAssisterLanguages());
			model.addAttribute(USER_ID, assister.getEntity().getUser().getId());
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
	
			// This flag is required to show certification number if
			// entered by
			// EE while registering the Assister
			if (assister.getCertificationNumber() != null && !"".equals(assister.getCertificationNumber())) {
				model.addAttribute(IS_CERTIFICATION_CERTIFIED, TRUE);
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_RETRIEVING_ASSISTER_INFORMATION, giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_RETRIEVING_ASSISTER_INFORMATION, exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("viewAssisterInformation: END");

		return "entity/entityadmin/assisterinformation";
	}
	
	/** 
	 * Method to decrypt identifier.
	 * 
	 * @param encryptedIdentifier The encrypted identifier.
	 * @return decryptedIdentifier The decrypted identifier.
	 */
	private String decryptIdentifier(String encryptedIdentifier) {
		LOGGER.info("decryptIdentifier: START");
		String decryptedIdentifier = null;
		
		if(encryptedIdentifier != null && !encryptedIdentifier.trim().isEmpty()){
			decryptedIdentifier = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedIdentifier);
		}

		LOGGER.info("decryptIdentifier: END");
		return decryptedIdentifier;
	}

	
	/**
	 * Method to retrieve Assister details for Admin.
	 * 
	 * @param assisterId The assister identifier.
	 * @param assisterRequestDTO The AssisterRequestDTO instance.
	 * @param assisterDetailsResponse The stringified assister details response.
	 * @return assisterResponseDTO The AssisterResponseDTO instance.
	 * @throws NumberFormatException The NumberFormatException instance.
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private AssisterResponseDTO retrieveAssistersDetailsForExchangeAdmin(
			String assisterId, AssisterRequestDTO assisterRequestDTO,
			String assisterDetailsResponse) throws NumberFormatException, JsonProcessingException, IOException {
		LOGGER.info("retrieveAssistersDetailsForExchangeAdmin: START");
		
		AssisterResponseDTO assisterResponseDTO;
		if (assisterId != null && !"".equals(assisterId)) {
			assisterRequestDTO.setId(Integer.valueOf(assisterId));
			LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS);
			// retrieving assister details
			assisterDetailsResponse = restClassCommunicator.retrieveAssistersDetailsForExchangeAdmin(assisterRequestDTO);
		}
		assisterResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
		LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_ENDS);

		LOGGER.info("retrieveAssistersDetailsForExchangeAdmin: END");
		return assisterResponseDTO;
	}

	/**
	 * 
	 * Handles the Assister Profile View page
	 * 
	 * 
	 * 
	 * @param model
	 * 
	 * @param encryAssisterId
	 * 
	 * @return URL for navigation to View Assister Information page
	 */

	@RequestMapping(value = "/entity/entityadmin/assisterprofile", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER)
	public String viewAssisterProfile(Model model,
			@RequestParam(value = ASSISTER_ID, required = false) String encryAssisterId) 
					throws GIException{

		LOGGER.info("viewAssisterProfile: START");
		String assisterId = encryAssisterId != null && !"".equals(encryAssisterId)? ghixJasyptEncrytorUtil.decryptStringByJasypt(encryAssisterId):null;
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Assister : Assister Information");
		AssisterResponseDTO assisterResponseDTO = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		String assisterDetailsResponse = null;
		
		try {
			user = userService.getLoggedInUser();			
			if (assisterId != null && !"".equals(assisterId)) {
				assisterRequestDTO.setId(Integer.valueOf(assisterId));
				LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS);
				assisterDetailsResponse = restClassCommunicator
						.retrieveAssistersDetailsForExchangeAdmin(assisterRequestDTO);
			}
			assisterResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
			EntityUtils.checkEntityAssisterAssociation(assisterId, user, assisterResponseDTO);
			LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_ENDS);
			Assister assister = assisterResponseDTO.getAssister();
			model.addAttribute(ASSISTER, assister);
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			model.addAttribute(ASSISTER_LANGUAGES, assisterResponseDTO.getAssisterLanguages());
			setContactNumber(model, assister);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving assister information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving assister profile information : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("viewAssisterProfile: END");

		return "entity/entityadmin/assisterprofile";
	}


	/*
	 * Get Method to display assister status for editing purpose providing user
	 * option to select Inactive/Active Status
	 */

	@RequestMapping(value = "/entity/entityadmin/editassisterstatus", method = RequestMethod.GET)
	@PreAuthorize(EntityUtils.HAS_PERMISSION_MODEL_ENTITY_VIEW_ASSISTER)
	public String displayssisterStatusForEdit(Model model, 
			@RequestParam(value = ASSISTER_ID, required = false) String encryAssisterId)

	{

		LOGGER.info("displayssisterStatusForEdit: START");
		String assisterId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryAssisterId);
		LOGGER.info("edit assister activity status starts here");
		List<Map<String, Object>> assisterActivityStatusHistory = null;
		if (assisterId != null && !EntityUtils.isEmpty(assisterId)) {

			AssisterRequestDTO assisterRequestDto = new AssisterRequestDTO();

			assisterRequestDto.setId(Integer.valueOf(assisterId));

			try {
				user = userService.getLoggedInUser();
				LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS);

				String assisterDetailsResponse = restClassCommunicator
						.retrieveAssistersDetailsForExchangeAdmin(assisterRequestDto);

				AssisterResponseDTO assisterResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
				EntityUtils.checkEntityAssisterAssociation(assisterId, user, assisterResponseDTO);
				Assister assister = assisterResponseDTO.getAssister();

				LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_ENDS);

				if (assister != null) {
					AssisterRequestDTO enrollmentEntityRequestDTO = new AssisterRequestDTO();
					enrollmentEntityRequestDTO.setModuleId(assister.getId());
					LOGGER.info(RETRIEVING_ENROOLMENTENTITY_HISTORY_DETAIL_REST_CALL_STARTS);
					String assisterResponseJSONStr = restClassCommunicator.retrieveAssisterActivityDetailsById(enrollmentEntityRequestDTO);
					LOGGER.info(RETRIEVING_ENROOLMENTENTITY_HISTORY_DETAIL_REST_CALL_ENDS);
					
					ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class);
					AssisterResponseDTO assisterResponseDTOLocal = reader.readValue(assisterResponseJSONStr );
					
					assisterActivityStatusHistory = assisterResponseDTOLocal.getAssisterEntityHistory();
				}
				model.addAttribute(ASSISTER, assister);
				model.addAttribute(ACTIVITY_STATUS_HISTORY, assisterActivityStatusHistory);
				LOGGER.info("edit assister activity status ends here");
			} catch(GIRuntimeException giexception){
				LOGGER.error(EXCEPTION_OCCURRED_WHILE_EDITING_ASSISTER_STATUS, giexception);
				throw giexception;
			} catch(Exception exception){
				LOGGER.error(EXCEPTION_OCCURRED_WHILE_EDITING_ASSISTER_STATUS, exception);
				throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}
		}

		LOGGER.info("displayssisterStatusForEdit: END");

		return "entity/entityadmin/editassisterstatus";
	}

	@RequestMapping(value = "/entity/entityadmin/editassisterstatus", method = RequestMethod.POST)
	@PreAuthorize(EntityUtils.HAS_PERMISSION_MODEL_ENTITY_VIEW_ASSISTER)
	public String submitAssisterStatus(Model model, 
			@ModelAttribute(ASSISTER) Assister assister,
			@RequestParam(value = ASSISTER_ID, required = false) String assisterId,
			@RequestParam(value = "assisterStatus", required = false) String assisterStatus) {

		LOGGER.info("submitAssisterStatus: START");

		LOGGER.info("Inside submitAssisterStatus method");

		Assister assiter = assister;
		Assister assisterToUpdate = new Assister();
		
		try {
			if (assisterId != null && !EntityUtils.isEmpty(assisterId) && assisterStatus != null
					&& !EntityUtils.isEmpty(assisterStatus)) {

				AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();

				assisterToUpdate.setId(Integer.parseInt(assisterId));

				assisterToUpdate.setStatus(assisterStatus);
				assisterToUpdate.setActivityComments(assister.getActivityComments());

				assisterRequestDTO.setAssister(assisterToUpdate);

				List<Map<String, Object>> assisterActivityStatusHistory = null;
				String responseJSON = restClassCommunicator.saveAssisterStatusForExchangeAdmin(assisterRequestDTO);
				AssisterResponseDTO assisterResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class).readValue(responseJSON);

				assiter = assisterResponseDTO.getAssister();
				model.addAttribute(ASSISTER, assister);
				assisterActivityStatusHistory = assisterResponseDTO.getAssisterEntityHistory();
				LOGGER.info("Post assister activity status ends here");
			} else {
				LOGGER.error("Assister is Null in submitAssisterStatus method");
			}
			
			String decryptdAssisterId = ghixJasyptEncrytorUtil.encryptStringByJasypt(assisterId);
			LOGGER.info("submitAssisterStatus: END");
			return "redirect:/entity/entityadmin/assisterstatus?assisterId=" + decryptdAssisterId;
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while editing assister status : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while editing assister status in submitAssisterStatus method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	/**
	 * Handles the search of assisters for enrollmententity
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/entity/entityadmin/searchofenrollmententityassisters", method = { RequestMethod.GET,
			RequestMethod.POST })
	@PreAuthorize(EntityUtils.HAS_PERMISSION_MODEL_ENTITY_VIEW_ASSISTER)
	public String searchOfEnrollmentEntityAssisters(Model model, HttpServletRequest request,
			@ModelAttribute(ASSISTER) Assister assister,
			@RequestParam(value = FIRST_NAME, required = false) String firstName,
	        @RequestParam(value = LAST_NAME, required = false) String lastName,
	        @RequestParam(value = ENTITY_NAME, required = false) String entityName,
	        @RequestParam(value = CERTIFICATION_STATUS, required = false) String certificationStatus,
			@RequestParam(value = FROM_DATE, required = false) String fromDate,
			@RequestParam(value = TO_DATE, required = false) String toDate,
			@RequestParam(value = "statusActive", required = false) String statusActive,
			@RequestParam(value = "statusInactive", required = false) String statusInactive) {

		LOGGER.info("searchOfEnrollmentEntityAssisters: START");

		LOGGER.info(RETRIEVING_LIST_OF_ASSISTERS_ASSOCIATED_WITH_THE_LOGGED_IN_ENROLLMENT_ENTITY);
		model.addAttribute(PAGE_TITLE, GETINSURED_HEALTH_EXCHANGE_ENROLLMENT_ENTITIES_VIEW_ASSISTERS);
		try {
			Integer pageSize = VAL_PAGE_SIZE;
			
			Validate.notNull(request);
			
			request.setAttribute(PAGE_SIZE, pageSize);
			request.setAttribute(REQ_URI, LIST);
			String param = request.getParameter(PAGE_NUMBER);
			Integer startRecord = 0;
			int currentPage = 1;
			int countOfAssisters;
			if (param != null) {
				startRecord = (Integer.parseInt(param) - 1) * pageSize;
				currentPage = Integer.parseInt(param);
				model.addAttribute(PAGE_NUMBER, param);
			} else {
				model.addAttribute(PAGE_NUMBER, 1);
			}
			if(null != request) {
				firstName = request.getParameter(FIRST_NAME);
				lastName = request.getParameter(LAST_NAME);
				entityName = request.getParameter(ENTITY_NAME);
				fromDate = request.getParameter(FROM_DATE);
				toDate = request.getParameter(TO_DATE);
				certificationStatus = request.getParameter(CERTIFICATION_STATUS);
				statusActive = request.getParameter(STATUS_ACTIVE);
				statusInactive = request.getParameter(STATUS_INACTIVE);
			}
			List<Assister> listOfAssisters = new ArrayList<Assister>();
			EnrollmentEntity enrollEntity = getEnrollmentEntityByUserId();
			model.addAttribute(ENROLLMENT_ENTITY, enrollEntity);

			AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setFirstName(firstName);
			assisterRequestDTO.setLastName(lastName);
			assisterRequestDTO.setEntityName(entityName);
			assisterRequestDTO.setAssister(assister);
			assisterRequestDTO.setEnrollmentEntity(enrollEntity);
			assisterRequestDTO.setPageSize(pageSize);
			assisterRequestDTO.setStartRecord(startRecord);
			assisterRequestDTO.setToDate(toDate);
			assisterRequestDTO.setFromDate(fromDate);
			assisterRequestDTO.setStatusactive(statusActive);
			assisterRequestDTO.setStatusInactive(statusInactive);
			assisterRequestDTO.setCertificationStatus(certificationStatus);
			assisterRequestDTO.setAdmin(false);
			assisterRequestDTO.setEnrollmentEntityId(enrollEntity.getId());
			String sortBy =  (request.getParameter(SORT_BY) == null) ?  "FIRST_NAME" : request.getParameter(SORT_BY);
			String sortOrder = determineSortOrder(request);

			request.removeAttribute(CHANGE_ORDER);
			
			model.addAttribute(SORT_ORDER, sortOrder);
			model.addAttribute(SORT_BY_COL, sortBy);
			
			assisterRequestDTO.setSortBy(sortBy);
			assisterRequestDTO.setSortOrder(sortOrder);
			AssisterResponseDTO assisterResponseDTO = retrieveSearchAssisterList(assisterRequestDTO);
			
			populateAssisterCertificationsStatuses(model);
			
			model.addAttribute(ASSISTERLIST,assisterResponseDTO.getListOfAssisters());
			listOfAssisters = assisterResponseDTO.getListOfAssisters();
		
			model.addAttribute(CURRENT_PAGE, currentPage);
			request.getSession().setAttribute("assistersearchparam", assister);
			
			model.addAttribute(TOTAL_ASSISTERS,
					assisterResponseDTO.getTotalAssisters());
			model.addAttribute(TOTAL_ASSISTERS_BY_SEARCH,
					assisterResponseDTO.getTotalAssistersBySearch());
		
			assisterRequestDTO.setEnrollmentEntity(enrollEntity);
			assisterResponseDTO = getListOfAssistersByEnrollmentEntity(assisterRequestDTO);
			countOfAssisters=assisterResponseDTO.getTotalAssisters();
			
			populateAndStoreEnrollmentEntitiesCount(model, request, countOfAssisters, listOfAssisters);
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters = new HashMap<String, Object>();
			parameters.put(FIRST_NAME, firstName);
			parameters.put(LAST_NAME, lastName);
			parameters.put(ENTITY_NAME, entityName);
			parameters.put(CERTIFICATION_STATUS, certificationStatus);
			parameters.put(FROM_DATE, fromDate);
			parameters.put(TO_DATE, toDate);
			parameters.put(STATUS_ACTIVE, statusActive);
			parameters.put(STATUS_INACTIVE, statusInactive);
			
			AssisterSearchParameters assisterSearchParameters = setAssisterSearchParameters(parameters);
			
			model.addAttribute(ASSISTER_SEARCH_PARAMETERS, assisterSearchParameters);
			
			List<LookupValue> statusValuelist = lookupService.getLookupValueList(ASSISTERSTATUS);
			List<String> statuslist = new ArrayList<String>();
			populateAndSetStatuslist(model, statusValuelist, statuslist);
			model.addAttribute(ASSISTER, assister);
			model.addAttribute(FROM_DATE, fromDate);
			model.addAttribute(TO_DATE, toDate);
			model.addAttribute(STATUS_ACTIVE, statusActive);
			model.addAttribute(STATUS_INACTIVE, statusInactive);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while searching assisters : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while searching assisters in searchOfEnrollmentEntityAssisters method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("searchOfEnrollmentEntityAssisters: END");

		return ENTITY_ENTITYADMIN_DISPLAYASSISTERS;
	}

	/**
	 * Handles the reset all functionality
	 * 
	 * @param result
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate Manage list page
	 */
	@RequestMapping(value = "/entity/entityadmin/resetallassisters", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(EntityUtils.HAS_PERMISSION_MODEL_ENTITY_VIEW_ASSISTER)
	public String resetAllAssisters(HttpServletRequest request) {

		LOGGER.info("resetAllAssisters: START");

		try {
			// clearing search criteria stored in session
			request.getSession().setAttribute(ASSISTER, null);
			request.getSession().setAttribute(STATUS_ACTIVE, null);
			request.getSession().setAttribute(FROM_DATE, null);
			request.getSession().setAttribute(TO_DATE, null);
			request.getSession().setAttribute(STATUS_INACTIVE, null);
		} catch(Exception exception){
			LOGGER.error("Exception occured while resetting assisters search : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("resetAllAssisters: END");

		return "redirect:/entity/entityadmin/displayassisters";
	}

	/**
	 * Handles the reset all functionality
	 * 
	 * @param result
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate Manage list page
	 */
	@RequestMapping(value = "/entity/entityadmin/resetall", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(EntityUtils.HAS_PERMISSION_MODEL_ENTITY_VIEW_ASSISTER)
	public String resetAll(Model model) {

		LOGGER.info("resetAll: START");
		model.addAttribute("enrollmentEntitySearchParameters", new EnrollmentEntitySearchParameters());

		LOGGER.info("resetAll: END");

		return "redirect:/entity/entityadmin/managelist";
	}

	private List<Assister> getSearchListOfAssistersByEnrollmentEntity(AssisterRequestDTO assisterRequestDTO) {

		LOGGER.info("getSearchListOfAssistersByEnrollmentEntity: START");

		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		
		try {
			String requestJSON =JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class).writeValueAsString(assisterRequestDTO);

			LOGGER.debug("Retrieving List Of Assisters By Enrollment Entity REST Call Starts.");
			String listOfAssistersResponse = restClassCommunicator
					.getSearchListOfAssistersByEnrollmentEntity(requestJSON);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class).readValue(listOfAssistersResponse);
			LOGGER.debug("Retrieving List Of Assisters By Enrollment Entity REST Call Ends.");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while retrieving List Of Assisters By Enrollment Entity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getSearchListOfAssistersByEnrollmentEntity: END");

		return assisterResponseDTO.getListOfAssisters();
	}

	private void setContactNumber(Model model, Assister assister) {

		LOGGER.info("setContactNumber: START");

		if (assister.getPrimaryPhoneNumber() != null && (!"".equals(assister.getPrimaryPhoneNumber()))) {
			model.addAttribute("primaryPhone1", assister.getPrimaryPhoneNumber().substring(0, THREE));
			model.addAttribute("primaryPhone2", assister.getPrimaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute("primaryPhone3", assister.getPrimaryPhoneNumber().substring(SIX, TEN));
		}

		if (assister.getSecondaryPhoneNumber() != null && (!"".equals(assister.getSecondaryPhoneNumber()))) {
			model.addAttribute("secondaryPhone1", assister.getSecondaryPhoneNumber().substring(0, THREE));
			model.addAttribute("secondaryPhone2", assister.getSecondaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute("secondaryPhone3", assister.getSecondaryPhoneNumber().substring(SIX, TEN));
		}

		LOGGER.info("setContactNumber: END");
	}

	/**
	 * Method to fetch all sites, sitelanguages and sitelocationhours associated
	 * with the logged in entity
	 * 
	 * @param model
	 */
	private void getAllSites(Model model, Integer id) {

		LOGGER.info("getAllSites: START");

		LOGGER.info("Inside getAllSites method");
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		List<Site> sitesList = null;
		List<Site> siteListByType = new ArrayList<Site>();
		Site primary = null;

		try {
			enrollmentEntityRequestDTO.setUserId(id);
			// rest Call to fetch list of sites and associated siteLocation
			// Hours and siteLanguages
			String siteDetails = restClassCommunicator.getAllSites(enrollmentEntityRequestDTO);

			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO =  JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(siteDetails);

			// retrieves list of sites from enrollmententityresponseDTO
			sitesList = enrollmentEntityResponseDTO.getSiteList();

			// creates a map of siteLocationHours objects
			Map<Integer, List<SiteLocationHours>> siteLocationHrsMap = enrollmentEntityResponseDTO
					.getSiteLocationHoursMap();

			// adds siteLocationHoursMap to a model attribute
			model.addAttribute(LOCATIONHOURSMAPWITHLIST, siteLocationHrsMap);

			// creates a map of SiteLanguages objects
			Map<Integer, SiteLanguages> siteLanguagesMap = enrollmentEntityResponseDTO.getSiteLanguagesMap();

			// adds siteLanguageMap to model attribute
			model.addAttribute(SITELANGUAGEMAP, siteLanguagesMap);

			for (Site site : sitesList) {

				// retrieves contact number for the site and assigns them to
				// form fields
				getSiteContactNumber(model, site);

				// put all subsites together in list
				if (!PRIMARYSITE.equalsIgnoreCase(site.getSiteType())) {
					siteListByType.add(site);

				} else {

					// assign primary site to a temporary object
					primary = site;
				}
			}
			// add primary site at 0th position in the list
			siteListByType.add(0, primary);

			// adds siteList to model attribute
			model.addAttribute(SITELIST, siteListByType);
			LOGGER.info("Exiting getAllSites method");

		} catch (Exception exception) {
			LOGGER.error("Exception occured in getAllSites method", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getAllSites: END");
	}

	// uploading the files into their specific folders
	private int uploadFile(String fileToUpload, String folderName, MultipartFile file, int entityId,RedirectAttributes redirectAttributes) throws Exception {

		LOGGER.info("uploadFile: START");

		int documentId1 = 0;
		String documentId = null;
		
		try {
			// creating a new file name with the time stamp
			String modifiedFileName = FilenameUtils.getBaseName(file.getOriginalFilename()) + GhixConstants.UNDERSCORE
					+ TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT
					+ FilenameUtils.getExtension(file.getOriginalFilename());
			// upload file to DMS

			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setAttachment(file.getBytes());
			enrollmentEntityRequestDTO.setModuleId(entityId);
			enrollmentEntityRequestDTO.setEntityType(modifiedFileName);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
			String documentIdStr = restClassCommunicator.saveDocumentsOnECM(enrollmentEntityRequestDTO);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_ENDS);

			EnrollmentEntityResponseDTO enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(documentIdStr);
			
			documentId = enrollmentEntityResponse.getDocument();
			if (documentId == null) {
				if(enrollmentEntityResponse.getResponseCode() == 500){
					redirectAttributes.addFlashAttribute("uploadErrorMessage", enrollmentEntityResponse.getResponseDescription());
				}
				LOGGER.info("uploadFile: END");

				return documentId1;
			}
			String createdBy = userService.getLoggedInUser().getEmail();

			MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
			String mimeType = mimeTypesMap.getContentType(file.getOriginalFilename());
			EntityDocuments entityDocs = new EntityDocuments();
			entityDocs.setCreatedBy(createdBy);
			entityDocs.setCreatedDate(new TSDate());
			entityDocs.setDocumentName(documentId);
			entityDocs.setMimeType(mimeType);
			entityDocs.setOrgDocumentName(file.getOriginalFilename());
			enrollmentEntityRequestDTO.setEntityDocuments(entityDocs);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
			// save document info in broker document
			String entityDocsStr = restClassCommunicator.saveDocument(enrollmentEntityRequestDTO);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_ENDS);

			enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityDocsStr);
			EntityDocuments entityDocuments = enrollmentEntityResponse.getEntityDocuments();

			documentId1 = entityDocuments.getID();
		} catch (Exception ex) {
			LOGGER.error("Fail to uplaod Entity Document file. Ex: ", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("uploadFile: END");

		return documentId1;
	}

	/**
	 * Handles the Submit Document of Entity
	 * 
	 * @param addSupportDoc
	 * @param request
	 * @param fileToUpload
	 * @return String
	 * @throws Exception 
	 */
	@RequestMapping(value = "/entity/entityadmin/uploadsubmit", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ENTITY)
	public String saveEntityWithDocument(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes,
			@RequestParam(value = ENTITY_ID, required = false) Integer id,
			@RequestParam(value = FILE_INPUT, required = false) MultipartFile addSupportDoc,
			@RequestParam(value = "fileToUpload", required = false) String fileToUpload) throws Exception {

		LOGGER.info("saveEntityWithDocument: START");

		LOGGER.info("Post Entity Document Upload Info");

		Integer documentId = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
		
		try {
			if ((addSupportDoc == null) || (addSupportDoc.getSize() > FILE_SIZE_EXCEED)) {
				Integer idRequest = (Integer) request.getSession().getAttribute("fileUploadId");
				request.getSession().setAttribute("uploadedDocument", "SIZE_FAILURE");
				return "redirect:/entity/entityadmin/documententity/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(idRequest));
				
			}
			
			enrollmentEntityRequest.setModuleId(id);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
			// get enrollment entity details
			String enrollmentEntityObjStr = restClassCommunicator.retrieveEnrollmentEntityObjectById(enrollmentEntityRequest);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_ENDS);
		
			EnrollmentEntityResponseDTO enrollmentEntityResponse =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollmentEntityObjStr);
			EnrollmentEntity enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();
			if ((fileToUpload != null) && (fileToUpload.equalsIgnoreCase(FILE_INPUT))) {
				// call uploadFile method to upload document on ECM server
				documentId = uploadFile(fileToUpload, "ADD_SUPPORT_FOLDER", addSupportDoc, enrollmentEntityObj.getId(), redirectAttributes);
			}
			 
			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityObj);//This will be used in case of Failure of Upload document.
			
			if(documentId != 0){ //If document upload is successfull then only create record into DB
				enrollmentEntityObj.setDocumentIdEE(documentId);
				enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntityObj);
				String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

				// Save enrollment Entity
				String strEnrollmentEntityResponse = restClassCommunicator.saveEnrollmentEntityWithDocument(requestJSON);
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(strEnrollmentEntityResponse);
				EnrollmentEntity enrollmentEntityNew = enrollmentEntityResponseDTO.getEnrollmentEntity();
				model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityNew);//This will be used in case of Success of Upload document.
			}
			
			
			Map<String, Boolean> navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(NAVIGATION_MENU_MAP);
			navigationMenuMap.put("DOCUMENT_UPLOAD", true);
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, navigationMenuMap);
			request.getSession().setAttribute(UPLOADED_DOCUMENT, String.valueOf(documentId));
			
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving entity with document : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving entity with document in saveEntityWithDocument method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("saveEntityWithDocument: END");
		
		return "redirect:/entity/entityadmin/documententity/" +ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(id));
	}

	/**
	 * Handles the request for Document upload of Entity
	 * 
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate Document upload of Entity
	 */
	@RequestMapping(value = "/entity/entityadmin/documententity/{id}", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ENTITY)

	public String documentEE(Model model, HttpServletRequest request, @PathVariable("id") String encryptedId,@ModelAttribute("uploadErrorMessage") String uploadErrorMessageInFlash) {

		LOGGER.info("Get Entity Document Information ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Entity Document Information");

		Map<String, Boolean> map = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
		
		try {
			int id = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId));
			
			enrollmentEntityRequest.setModuleId(id);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
			// get enrollment entity details
			String enrollmentEntityObjStr = restClassCommunicator
					.retrieveEnrollmentEntityObjectById(enrollmentEntityRequest);
			LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_ENDS);

			EnrollmentEntityResponseDTO enrollmentEntityResponse =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollmentEntityObjStr);
			EnrollmentEntity enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();

			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityObj);
			List<Map<String, Object>> entityCertStatusHistory = null;
			if (enrollmentEntityObj != null) {
				// get history of documents for enrollment entity
				EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequestDTO.setModuleId(enrollmentEntityObj.getId());
				LOGGER.info(RETRIEVING_ENROOLMENTENTITY_HISTORY_DETAIL_REST_CALL_STARTS);
				String enrollmentEntityHistoryStr = restClassCommunicator
						.retrieveEnrollmentEntityDocumentHistoryById(enrollmentEntityRequestDTO);
				LOGGER.info(RETRIEVING_ENROOLMENTENTITY_HISTORY_DETAIL_REST_CALL_ENDS);
				ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);
				EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = reader.readValue(enrollmentEntityHistoryStr);
				entityCertStatusHistory = enrollmentEntityResponseDTO.getEnrollmentEntityHistory();
			}
			// get left navigation info
			if (enrollmentEntityObj != null) {
				map = getLeftNavigationMenuMap(enrollmentEntityObj);
			} else {
				map = new HashMap<String, Boolean>();
			}
			request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
			model.addAttribute("enrollmentRegisterStatusHistory", entityCertStatusHistory);
			if (!EntityUtils.isEmpty((String) request.getSession().getAttribute(UPLOADED_DOCUMENT))) {
				model.addAttribute(UPLOADED_DOCUMENT, request.getSession().getAttribute(UPLOADED_DOCUMENT));
				request.getSession().removeAttribute(UPLOADED_DOCUMENT);
			}
			if(null != uploadErrorMessageInFlash){
				model.addAttribute("uploadErrorMessageInFlash",uploadErrorMessageInFlash);
			}
			
			request.getSession().setAttribute("fileUploadId", id);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while processing documentEE(...) : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while processing documentEE : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("documentEE: END");

		return "entity/entityadmin/documententityadmin";
	}

	/**
	 * Handles the POST request for Edit Broker information
	 * 
	 * @param request
	 * @param documentId
	 * @param model
	 * @return URL for navigation to document upload
	 * @throws ContentManagementServiceException
	 */
	@RequestMapping(value = "/entity/entityadmin/remove", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ENTITY)
	public String removeDocument(@RequestParam String encrypteddocumentId, @RequestParam String encryptedid, Model model,
			HttpServletRequest request) throws ContentManagementServiceException {

		LOGGER.info("removeDocument: START");
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);
		
		try {
			Integer documentId=Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(encrypteddocumentId));
			Integer id=Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedid));
			if (documentId != null) {
				EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
				enrollmentEntityRequest.setModuleId(documentId);
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
				// delete document from broker document
				String entityDocumentsStr = restClassCommunicator.deleteDocument(enrollmentEntityRequest);
				
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Document delete response:" + entityDocumentsStr);
				}
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_ENDS);

				Map<String, Boolean> map = null;
				enrollmentEntityRequest.setModuleId(id);
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_STARTS);
				// get enrollment entity deatil
				String enrollmentEntityObjStr = restClassCommunicator.retrieveEnrollmentEntityObjectById(enrollmentEntityRequest);
				LOGGER.info(RETRIEVING_EE_DETAIL_REST_CALL_ENDS);
	
				EnrollmentEntityResponseDTO enrollmentEntityResponse = reader.readValue(enrollmentEntityObjStr);
				EnrollmentEntity enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();
				model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntityObj);
				List<Map<String, Object>> entityCertStatusHistory = null;
				if (enrollmentEntityObj != null) {
					EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
					enrollmentEntityRequestDTO.setModuleId(enrollmentEntityObj.getId());
					LOGGER.info(RETRIEVING_ENROOLMENTENTITY_HISTORY_DETAIL_REST_CALL_STARTS);
					// get document history of enrollment entity
					String enrollmentEntityHistoryStr = restClassCommunicator
							.retrieveEnrollmentEntityDocumentHistoryById(enrollmentEntityRequestDTO);
					LOGGER.info(RETRIEVING_ENROOLMENTENTITY_HISTORY_DETAIL_REST_CALL_ENDS);
					
					EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = reader.readValue(enrollmentEntityHistoryStr);
					entityCertStatusHistory = enrollmentEntityResponseDTO.getEnrollmentEntityHistory();
				}
				// get left navigation info
				if (enrollmentEntityObj != null) {
					map = getLeftNavigationMenuMap(enrollmentEntityObj);
				} else {
					map = new HashMap<String, Boolean>();
				}
				request.getSession().setAttribute(NAVIGATION_MENU_MAP, map);
				model.addAttribute("enrollmentRegisterStatusHistory", entityCertStatusHistory);

			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while deleting document : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while deleting document in removeDocument method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("removeDocument: END");

		return "entity/entityadmin/documententityadmin";
	}

	/**
	 * Method to validate languages entered by Assister
	 * 
	 * @param model
	 *            The Model instance.
	 * @param languagesSpoken
	 *            The spoken language.
	 * @return Boolean <code>true</code> if language allowed else
	 *         <code>false</code>.
	 */
	@RequestMapping(value = "/entity/entityadmin/addnewassisterbyadmin/checkLanguagesSpokenForAsister")
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_VIEW_ENTITY)
	public Boolean checkLanguagesSpokenForAssister(@RequestParam String otherSpokenLanguage) {

		LOGGER.info("checkLanguagesSpokenForAssister: START");

		LOGGER.info("spoken language validation start here");

		List<String> languageList = null;
		List<String> countiesServedlist = new ArrayList<String>();
		String otherSpokenLanguageWithoutSpace = null;
		
		try {
			languageList = lookupService.populateLanguageNames("");

			if (null == languageList) {
				languageList = new ArrayList<String>();
			}

			otherSpokenLanguageWithoutSpace = otherSpokenLanguage.trim();
			countiesServedlist = Arrays.asList(otherSpokenLanguageWithoutSpace.split(","));
			for (String countiesStr : countiesServedlist) {
				if (!languageList.contains(countiesStr.trim())) {

					LOGGER.info("checkLanguagesSpokenForAssister: END");

					return false;
				}
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while checking spoken language : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while checking spoken language in checkLanguagesSpokenForAssister method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("checkLanguagesSpokenForAssister: END");

		return true;
	}

	/**
	 * Method to validate languages entered by Assister
	 * 
	 * @param model
	 *            The Model instance.
	 * @param languagesSpoken
	 *            The written language.
	 * @return Boolean <code>true</code> if language allowed else
	 *         <code>false</code>.
	 */
	@RequestMapping(value = "/entity/entityadmin/addnewassisterbyadmin/checkLanguagesWrittenForAssister")
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_VIEW_ENTITY)
	public Boolean checkLanguagesWrittenForAssister(@RequestParam String otherWrittenLanguage) {

		LOGGER.info("checkLanguagesWrittenForAssister: START");

		LOGGER.info("spoken language validation start here");

		List<String> languageList = null;
		List<String> countiesServedlist = new ArrayList<String>();
		String otherWrittenLanguageWithoutSpace = null;
		
		try {
			languageList = lookupService.populateLanguageNames("");

			if (null == languageList) {
				languageList = new ArrayList<String>();
			}

			otherWrittenLanguageWithoutSpace = otherWrittenLanguage.trim();
			countiesServedlist = Arrays.asList(otherWrittenLanguageWithoutSpace.split(","));
			for (String countiesStr : countiesServedlist) {
				if (!languageList.contains(countiesStr.trim())) {

					LOGGER.info("checkLanguagesWrittenForAssister: END");

					return false;
				}
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while checking written language : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while checking written language in checkLanguagesWrittenForAssister : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("checkLanguagesWrittenForAssister: END");

		return true;
	}
	
	private EnrollmentEntity getEnrollmentEntityByUserId()
			throws InvalidUserException, JsonProcessingException, IOException {
		EnrollmentEntity enrollmentEntityObj;
		user = userService.getLoggedInUser();
		EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequest.setModuleId(user.getId());
		LOGGER.info("Retrieving Enrollment Entity Detail REST Call Starts");
		String entityObjStr = restClassCommunicator.retrieveEnrollmentEntityObjectByUserId(enrollmentEntityRequest);
		EnrollmentEntityResponseDTO enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityObjStr);
		enrollmentEntityObj = enrollmentEntityResponse.getEnrollmentEntity();
		if (enrollmentEntityObj == null) {
			enrollmentEntityObj = new EnrollmentEntity();
			enrollmentEntityObj.setUser(user);
		}
		return enrollmentEntityObj;
	} 
	
	private String loadLanguages(){
		JsonArray languageJsonArr = null;
        LOGGER.info("getLanguageList : START");
        String jsonLangData = "";
        
	    List<?> languageSpokenList = null;
	    
		try { 
		    languageSpokenList = lookupService.populateLanguageNames("");
		    languageJsonArr = new JsonArray();
		    for(int i=0;i<languageSpokenList.size();i++)
			    {
			    	String lan = EntityUtils.initializeAndUnproxy(languageSpokenList.get(i));
			    	if(lan!=null){
			    		languageJsonArr.add(new JsonPrimitive(lan));
			    	}
			    }
		    
		    jsonLangData = platformGson.toJson(languageJsonArr);
		 	}
		catch(Exception exception)
		{
			LOGGER.error("Exception occurred while loading Languages : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
        LOGGER.info("getLanguageList : END");
        return jsonLangData;
	}
	
	private EnrollmentEntityResponseDTO retriveEnrollmentEntityMap(
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) throws IOException {
		String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

		// Rest call for enrollment entity service
		String enrollEntityResponse = restClassCommunicator.retrieveEnrollmentEntityMap(requestJSON);
		
		return    JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
	}
}
