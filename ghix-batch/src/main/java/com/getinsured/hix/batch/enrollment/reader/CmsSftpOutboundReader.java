package com.getinsured.hix.batch.enrollment.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

public class CmsSftpOutboundReader implements ItemReader<String> {
	private static final Logger LOGGER = LoggerFactory.getLogger(CmsSftpOutboundReader.class);
	Integer partition;
	String reportType;
	int loopCount = 0;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader ");

		ExecutionContext ec = stepExecution.getExecutionContext();
		if (ec != null && !ec.isEmpty()) {
			partition = ec.getInt("partition");
			reportType = ec.getString("reportType");
			LOGGER.info("CmsSftpOutboundReader : Thread Name: " + Thread.currentThread().getName()
					+ " Report Type :: " + reportType);
		}
	}

	@Override
	public String read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		if(loopCount++ == 0){
		return reportType;
	}
		return null;
	}
}
