package com.getinsured.ssap.model.hub.vlp.response;

import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Response Metadata payload description.
 *
 * @author Yevgen Golubenko
 * @since 2/27/19
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseMetadata {

  @JsonProperty("ResponseCode")
  private ResponseCode responseCode;

  @JsonProperty("ResponseDescriptionText")
  private String responseDescriptionText;

  @JsonProperty("TDSResponseDescriptionText")
  private String tdsResponseDescriptionText;

  public ResponseCode getResponseCode() {
    return responseCode;
  }

  public void setResponseCode(ResponseCode responseCode) {
    this.responseCode = responseCode;
  }

  public String getResponseDescriptionText() {
    return responseDescriptionText;
  }

  public void setResponseDescriptionText(String responseDescriptionText) {
    this.responseDescriptionText = responseDescriptionText;
  }

  public String getTdsResponseDescriptionText() {
    return tdsResponseDescriptionText;
  }

  public void setTdsResponseDescriptionText(String tdsResponseDescriptionText) {
    this.tdsResponseDescriptionText = tdsResponseDescriptionText;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", ResponseMetadata.class.getSimpleName() + "[", "]")
        .add("responseCode=" + responseCode)
        .add("responseDescriptionText='" + responseDescriptionText + "'")
        .add("tdsResponseDescriptionText='" + tdsResponseDescriptionText + "'")
        .toString();
  }
}
