package com.getinsured.hix.plandisplay;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.jsoup.helper.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdOrderItemDTO;
import com.getinsured.hix.dto.plandisplay.PdOrderItemPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.dto.plandisplay.PdSession;
import com.getinsured.hix.enrollment.util.CreateDTOBean;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.plandisplay.GroupData;
import com.getinsured.hix.model.plandisplay.PdCrossWalkIssuerIdRequest;
import com.getinsured.hix.model.plandisplay.PdPreShopRequest;
import com.getinsured.hix.model.plandisplay.PdResponse;
import com.getinsured.hix.model.plandisplay.PdSaveCartRequest;
import com.getinsured.hix.model.plandisplay.PdSubscriberData;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentFlowType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExpenseEstimate;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.PreferencesLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.model.plandisplay.PlanDisplayRequest;
import com.getinsured.hix.model.plandisplay.PlanPreferences;
import com.getinsured.hix.model.plandisplay.PlanPreferencesDET;
import com.getinsured.hix.model.plandisplay.PlanPremium;
import com.getinsured.hix.plandisplay.util.PlanDisplayConstants;
import com.getinsured.hix.plandisplay.util.PlanDisplayRestClient;
import com.getinsured.hix.plandisplay.util.PlanDisplayUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.plandisplay.individualinformation.ProductType;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Component
public class PlanDisplayWebHelper {

	@Autowired private PlanDisplayRestClient planDisplayRestClient;
	@Autowired private PlanDisplayUtil planDisplayUtil;
	@Autowired private Gson platformGson;
	
	public static final String Y = "Y";
	public static final String U = "U";
	public static final String N = "N";
	public static final String NA = "NA";
	public static final String OK = "OK";
	public static final String POOR = "POOR";
	public static final String GOOD = "GOOD";
	public static final String UNKNOWN = "UNKNOWN";
	public static final String IN_NETWORK = "inNetWork";
	public static final String OUT_NETWORK = "outNetWork";
	public static final String OUT_OF_NETWORK = "Out of network";	
	public static final String PEDIATRIC_DENTAL = "Pediatric dental";
	public static final String CHILDRENS_DENTAL = "Children's Dental";
	public static final String AVAILABILITY_UNKNOWN = "Availability unknown";	
	public static final String AVAILABILITY_IN_YOUR_NETWORK = "Available in your network";
	public static final String  PRICEY= "Pricey";
	public static final String  AVERAGE= "Average";
	public static final String  LOWER_COST= "Lower Cost";
	public static final String DOCTOR = "doctor";
	public static final String DENTIST = "dentist";
	public static final String DR = "Dr.";
	public static final String DMD = "DMD";
	public static final String SPACE = " ";
		
	public PdResponse invokePreShopping(EligLead eligLead, InsuranceType insuranceType, Date effectiveStartDate,GroupData groupData) {	
		PdPreShopRequest pdPreShopRequest = new PdPreShopRequest();
		pdPreShopRequest.setEligLead(eligLead);
		pdPreShopRequest.setInsuranceType(insuranceType);	
		String covgDate = DateUtil.dateToString(effectiveStartDate, "MM/dd/yyyy");
		pdPreShopRequest.setEffectiveStartDate(covgDate);
		pdPreShopRequest.setGroupData(groupData);
		PdResponse pdResponse = planDisplayRestClient.savePreShoppingData(pdPreShopRequest);	
		
		return pdResponse;
	}
	
	public List<Map<String,String>> buildCustomGroupList(List<PdPersonDTO> pdPersonList, PdHouseholdDTO pdHouseholdDTO, EnrollmentFlowType specialEnrollmentFlowType, String dentalPlanSelectionConfig) 
	{
		List<PdPersonDTO> dentalEligiblePersonList = new ArrayList<PdPersonDTO>();
		if("PO".equals(dentalPlanSelectionConfig)) 
		{
			 for(PdPersonDTO pdPersonDTO : pdPersonList) 
		     {
				Date covgDate = pdPersonDTO.getDentalCoverageStartDate() == null ? pdHouseholdDTO.getCoverageStartDate() : pdPersonDTO.getDentalCoverageStartDate();
		        int age = PlanDisplayUtil.getAgeFromCoverageDate(covgDate, pdPersonDTO.getBirthDay());
		        if (age < PlanDisplayConstants.CHILD_AGE) {				
		        	dentalEligiblePersonList.add(pdPersonDTO);
		        }
		     }
		} else {
			dentalEligiblePersonList.addAll(pdPersonList);
		}
		
		List<Map<String,String>> customGroupList = new ArrayList<Map<String,String>>();
		for(PdPersonDTO pdPersonDTO : dentalEligiblePersonList) 
		{
			Map<String,String> customGroup = new HashMap<String,String>();
			customGroup.put("id", pdPersonDTO.getId().toString());
			customGroup.put("firstName", pdPersonDTO.getFirstName());
			customGroup.put("seekCoverage", pdPersonDTO.getSeekCoverage().toString());
			customGroup.put("relationship", pdPersonDTO.getRelationship().toString());
			customGroup.put("birthDay", DateUtil.dateToString(pdPersonDTO.getBirthDay(), "MM/dd/yyyy"));
			Date coverageDateforAge = pdHouseholdDTO.getCoverageStartDate();
			if (EnrollmentType.S.equals(pdHouseholdDTO.getEnrollmentType()) && EnrollmentFlowType.KEEP.equals(specialEnrollmentFlowType)){
				// Flow won't reach here as this page is not accessible of KEEP flow. Using Dental coverage start date as we are building custom group for DENTAL plan selection
				coverageDateforAge = pdPersonDTO.getDentalCoverageStartDate() == null ? pdHouseholdDTO.getCoverageStartDate() : pdPersonDTO.getDentalCoverageStartDate();
			}
			int age = PlanDisplayUtil.getAgeFromCoverageDate(coverageDateforAge, pdPersonDTO.getBirthDay());
			customGroup.put("age", age+"");
			customGroupList.add(customGroup);
		}
		return customGroupList;
	}
		
	public PdResponse savePdOrderItem(PdSession pdSession, IndividualPlan individualPlan) {
		PdResponse pdResponse = new PdResponse();
		
		PdSaveCartRequest pdSaveCartRequest = new PdSaveCartRequest();
		pdSaveCartRequest.setInsuranceType(pdSession.getInsuranceType());
		pdSaveCartRequest.setHouseholdId(pdSession.getPdHouseholdDTO().getId());
		pdSaveCartRequest.setMonthlySubsidy(new BigDecimal(Float.toString(individualPlan.getAptc())).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
        if(individualPlan.getStateSubsidy() != null) {
            pdSaveCartRequest.setMonthlyStateSubsidy(individualPlan.getStateSubsidy().setScale(2, BigDecimal.ROUND_HALF_UP));
        }
		pdSaveCartRequest.setPlanId(individualPlan.getPlanId().longValue());
		pdSaveCartRequest.setPremiumAfterCredit(new BigDecimal(Float.toString(individualPlan.getPremiumAfterCredit())).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
		pdSaveCartRequest.setPremiumBeforeCredit(new BigDecimal(Float.toString(individualPlan.getPremiumBeforeCredit())).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
		pdSaveCartRequest.setPremiumData(individualPlan.getPlanDetailsByMember());
		pdSaveCartRequest.setSubsidyData(individualPlan.getContributionData());
		
		boolean validationSuccess = planDisplayUtil.validateEncodedPremium(individualPlan);
		if(!validationSuccess){
			pdResponse.setResponse(null);
			pdResponse.setStatus("Failed.Encoding does not match.");
			
		} else {		
			if(InsuranceType.HEALTH.equals(pdSession.getInsuranceType())){
				setPlanPreferences(individualPlan, pdSession.getPdPreferencesDTO(), pdSaveCartRequest);	
			}	
			pdResponse = planDisplayRestClient.saveCartItem(pdSaveCartRequest);
		}
		return pdResponse;
	}

	/**
	 * Method is used to get Plan Details Member List from order item Person List.
	 */
	public List<Map<String, String>> getPlanDetailsByMember(List<PdOrderItemPersonDTO> pdOrderItemPersonDTOList, List<PdPersonDTO> pdPersonDTOList) {

		if (CollectionUtils.isEmpty(pdOrderItemPersonDTOList) || CollectionUtils.isEmpty(pdPersonDTOList)) {
			return null;
		}

		List<Map<String, String>> healthPlanDetailsByMember = new ArrayList<Map<String, String>>();
		Map<String, String> planDetailByMember = null;
		PdPersonDTO pdPersonDTO = null;

		for (PdOrderItemPersonDTO pdOrderItemPersonDTO : pdOrderItemPersonDTOList) {

			planDetailByMember = new HashMap<String, String>();
			pdPersonDTO = getPdPersonDTOFromList(pdOrderItemPersonDTO.getPdPersonId(), pdPersonDTOList);

			if (null != pdPersonDTO) {
				int age = PlanDisplayUtil.getAgeFromCoverageDate(pdOrderItemPersonDTO.getPersonCoverageStartDate(), pdPersonDTO.getBirthDay());
				planDetailByMember.put("age", String.valueOf(age));
				planDetailByMember.put("premium", String.valueOf(pdOrderItemPersonDTO.getPremium()));
				healthPlanDetailsByMember.add(planDetailByMember);
			}
		}
		return healthPlanDetailsByMember;
	}

	/**
	 * Method is used to get PdPersonDTO from PdPersonDTOList by PdPersonId.
	 */
	private PdPersonDTO getPdPersonDTOFromList(long pdPersonId, List<PdPersonDTO> pdPersonDTOList) {

		PdPersonDTO pdPersonDTO = pdPersonDTOList.stream().filter(predPDPersonDTO -> predPDPersonDTO.getId() == pdPersonId).findFirst().orElse(null);
		return pdPersonDTO;
	}

	private void setPlanPreferences(IndividualPlan individualPlan, PdPreferencesDTO pdPreferencesDTO, PdSaveCartRequest pdSaveCartRequest) {
		PlanPreferences planPreferences = new PlanPreferences();		
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		boolean isCAProfile = stateCode != null ? stateCode.equalsIgnoreCase("CA") : false;	
		List<HashMap<String, Object>> doctorList = individualPlan.getDoctors();
		List<PlanPreferencesDET> doctors = new ArrayList<PlanPreferencesDET>();
		if(doctorList != null){				
			for(HashMap<String, Object> doctorMap : doctorList){
				PlanPreferencesDET doctor = new PlanPreferencesDET();
				doctor.setId((String) doctorMap.get("providerId"));
				String providerName = (String) doctorMap.get("providerName");				
				String netStatus = (String) doctorMap.get("networkStatus");
				String providerType = (String) doctorMap.get("providerType");
				if(DOCTOR.equalsIgnoreCase(providerType)){
					providerName = DR + SPACE + providerName;
				}else if(DENTIST.equalsIgnoreCase(providerType)){
					providerName = DR + SPACE + providerName + SPACE + DMD;
				}
				doctor.setName(providerName);
				String networkStatus = AVAILABILITY_UNKNOWN;
				if(OUT_NETWORK.equalsIgnoreCase(netStatus)){
					networkStatus = OUT_OF_NETWORK;
				}else if(IN_NETWORK.equalsIgnoreCase(netStatus)){
					networkStatus = AVAILABILITY_IN_YOUR_NETWORK;
				}				
				doctor.setValue(networkStatus);
				doctor.setGroupKey((String) doctorMap.get("groupKey"));
				doctor.setProviderType(providerType);
				doctors.add(doctor);
			}
		}		
				
		List<PlanPreferencesDET> benefits = new ArrayList<PlanPreferencesDET>();
		if(pdPreferencesDTO != null){			
			List<String> benefitList = pdPreferencesDTO.getBenefits();
				Map<String, String> benefitsCoverageMap = individualPlan.getBenefitsCoverage();
			if(benefitList != null && benefitsCoverageMap != null){				
				for(String benefitKey : benefitList){
					PlanPreferencesDET benefit = new PlanPreferencesDET();
					if(PEDIATRIC_DENTAL.equalsIgnoreCase(benefitKey)){
						benefit.setName(CHILDRENS_DENTAL);
					}else{
						benefit.setName(benefitKey);
					}
					
					String benefitStr = NA;
					if(GOOD.equals(benefitsCoverageMap.get(benefitKey))){
						benefitStr = GOOD;
					}else if(OK.equals(benefitsCoverageMap.get(benefitKey))){
						benefitStr = OK;
					}else if(POOR.equals(benefitsCoverageMap.get(benefitKey))){
						benefitStr = POOR;
					}
					benefit.setValue(benefitStr);
					benefits.add(benefit);
				}				
			}
			planPreferences.setMedicalUse(pdPreferencesDTO.getMedicalUse());
			planPreferences.setPrescriptionUse(pdPreferencesDTO.getPrescriptionUse());
		}else{
			if(isCAProfile){
			planPreferences.setMedicalUse(PreferencesLevel.LEVEL2);
			planPreferences.setPrescriptionUse(PreferencesLevel.LEVEL2);
			}else{
				planPreferences.setMedicalUse(PreferencesLevel.LEVEL1);
				planPreferences.setPrescriptionUse(PreferencesLevel.LEVEL1);
			}			
		}
		
		if(LOWER_COST.equalsIgnoreCase(individualPlan.getExpenseEstimate())){
			planPreferences.setExpenseEstimate(ExpenseEstimate.LOW);
		}else if(AVERAGE.equalsIgnoreCase(individualPlan.getExpenseEstimate())){
			planPreferences.setExpenseEstimate(ExpenseEstimate.AVERAGE);
		}else if(PRICEY.equalsIgnoreCase(individualPlan.getExpenseEstimate())){
			planPreferences.setExpenseEstimate(ExpenseEstimate.PRICEY);
		}
		
		planPreferences.setDoctors(doctors);
		planPreferences.setBenefits(benefits);
		pdSaveCartRequest.setPlanPreferences(planPreferences);		
	}
	
	public Map<Integer, PlanPremium> convertKeyStringToInteger(Map<String, LinkedHashMap> fromStringKey) {
		Map<Integer, PlanPremium> toIntKey = new HashMap<Integer, PlanPremium>();
		for(Entry<String, LinkedHashMap> planId : fromStringKey.entrySet()) {
			toIntKey.put(Integer.parseInt(planId.getKey()), formPlanPremium(planId.getValue()));
		}
		return toIntKey;
	}

	public PlanPremium formPlanPremium(LinkedHashMap value) {
		PlanPremium obj = new PlanPremium();
		obj.setAvgIndiPremium(Float.valueOf((String)value.get("avgIndiPremium")));
		obj.setPlanName((String)value.get("planName"));
		obj.setTotalDepePremium(Float.valueOf((String)value.get("totalDepePremium")));
		obj.setTotalIndiPremium(Float.valueOf((String)value.get("totalIndiPremium")));
		return obj;
	}
	
	public List<String[]> getSeekingCoverageData(String dataString) throws GIRuntimeException {
		if(StringUtil.isBlank(dataString)) {
			throw new GIRuntimeException("Input data is missing");
		} else {
			String dataArr[] = dataString.split(",");
			List<String[]> list = new ArrayList<>();
			for(int i = 0; i < dataArr.length; i++) {
				String temp[] = dataArr[i].split("=");
				list.add(temp);
			}
			return list;
		}
	}

	@Autowired private CreateDTOBean createDTOBean;
	
	private PdSession getClonedSession(PdSession pdSession)
	{
		PdSession clonedSession = new PdSession();
		PdHouseholdDTO clonedHousehold = new PdHouseholdDTO();

		List<PdPersonDTO> clonedPersonDTOs = new ArrayList<PdPersonDTO>();
		try {
			createDTOBean.copyProperties(clonedSession, pdSession);
			createDTOBean.copyProperties(clonedHousehold, pdSession.getPdHouseholdDTO());
			for(PdPersonDTO personDTO : pdSession.getPdPersonDTOList())
			{
				PdPersonDTO clonedPersonDTO = new PdPersonDTO();
				createDTOBean.copyProperties(clonedPersonDTO, personDTO);
				clonedPersonDTOs.add(clonedPersonDTO);
			}
			clonedSession.setPdHouseholdDTO(clonedHousehold);	
			clonedSession.setPdPersonDTOList(clonedPersonDTOs);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return clonedSession;
	}
	
	private boolean compareOldNNewPlans(
			List<IndividualPlan> oldPlansMinimizedData,
			List<IndividualPlan> individualPlansMinimizedData) {
			boolean hasNewPlan = false;
		for(IndividualPlan newPlan : individualPlansMinimizedData)
		{
			boolean containsInOldList = false;
			for(IndividualPlan oldPlan : oldPlansMinimizedData)
			{
				if(newPlan.getPlanId().equals(oldPlan.getPlanId()))
				{
					containsInOldList = true;
				}
			}
			
			if(!containsInOldList)
			{
				hasNewPlan = true;
				break;
			}
		}
		return hasNewPlan;
	}
	
	public String processAddressChange(PdSession pdSession) {
		String healthEnrollmentID = null;
		String dentalEnrollmentID = null;	
		String subscriberZipcode = null;
		String subscriberCountycode = null;
		Integer healthPlanId = null;
		Integer dentalPlanId = null;
		
		PdSession clonedSession = getClonedSession(pdSession);
		List<IndividualPlan> oldIndividualDentalPlans = null;
		List<IndividualPlan> oldIndividualHealthPlans = null;
		String returnUrl = null;
		
		for (PdPersonDTO pdPersonDTO : clonedSession.getPdPersonDTOList()) {
			if(healthEnrollmentID == null || "".equals(healthEnrollmentID)) {
				healthEnrollmentID = pdPersonDTO.getHealthEnrollmentId() != null ? pdPersonDTO.getHealthEnrollmentId().toString() : null;
			}
			if(dentalEnrollmentID == null || "".equals(dentalEnrollmentID)) {
				dentalEnrollmentID = pdPersonDTO.getDentalEnrollmentId() != null ? pdPersonDTO.getDentalEnrollmentId().toString() : null;
			}
		}
			
		if(StringUtils.isNotBlank(healthEnrollmentID)) {
			EnrollmentResponse enrollmentResponse = planDisplayRestClient.findPlanIdAndOrderItemIdByEnrollmentId(healthEnrollmentID);
			
			if(enrollmentResponse != null && enrollmentResponse.getPlanId() != null && enrollmentResponse.getCmsPlanId() != null )	{
				subscriberZipcode = StringUtils.isNotBlank(enrollmentResponse.getSubscriberZip()) ? enrollmentResponse.getSubscriberZip() : null;
				subscriberCountycode = StringUtils.isNotBlank(enrollmentResponse.getSubscriberCountyCode()) ? enrollmentResponse.getSubscriberCountyCode() : null;
				healthPlanId = enrollmentResponse.getPlanId();
				clonedSession.setInsuranceType(InsuranceType.HEALTH);
				
				for(PdPersonDTO personDTO : clonedSession.getPdPersonDTOList())
				{
					personDTO.setZipCode(subscriberZipcode);
					personDTO.setCountyCode(subscriberCountycode);
				}
				
				String individualPlanStr = planDisplayRestClient.getPlans(clonedSession, true, null, null, null);
								
				Type type = new TypeToken<List<IndividualPlan>>() {}.getType();
				oldIndividualHealthPlans = (List<IndividualPlan>)platformGson.fromJson(individualPlanStr, type);	
			} 
		}
			
		if(StringUtils.isNotBlank(dentalEnrollmentID)) {
				EnrollmentResponse enrollmentResponse = planDisplayRestClient.findPlanIdAndOrderItemIdByEnrollmentId(dentalEnrollmentID);
				
				if(enrollmentResponse != null && enrollmentResponse.getPlanId() != null && enrollmentResponse.getCmsPlanId() != null) {
					subscriberZipcode = StringUtils.isNotBlank(enrollmentResponse.getSubscriberZip()) ? enrollmentResponse.getSubscriberZip() : null;
					subscriberCountycode = StringUtils.isNotBlank(enrollmentResponse.getSubscriberCountyCode()) ? enrollmentResponse.getSubscriberCountyCode() : null;
					dentalPlanId = enrollmentResponse.getPlanId();
					clonedSession.setInsuranceType(InsuranceType.DENTAL);
					
					for(PdPersonDTO personDTO : clonedSession.getPdPersonDTOList())
					{
						personDTO.setZipCode(subscriberZipcode);
						personDTO.setCountyCode(subscriberCountycode);
					}
					
					String individualPlanStr = planDisplayRestClient.getPlans(clonedSession, true, null, null, null);
					Type type = new TypeToken<List<IndividualPlan>>() {}.getType();
					oldIndividualDentalPlans = (List<IndividualPlan>)platformGson.fromJson(individualPlanStr, type);
			}
		}
		
		List<IndividualPlan> newIndividualHealthPlans = null;
		pdSession.setInsuranceType(InsuranceType.HEALTH);
		String individualPlanStr = planDisplayRestClient.getPlans(pdSession, true, null, null, null);
		Type type = new TypeToken<List<IndividualPlan>>() {}.getType();
		newIndividualHealthPlans = (List<IndividualPlan>)platformGson.fromJson(individualPlanStr, type);
		
		
		List<IndividualPlan> newIndividualDentalPlans = null;
		pdSession.setInsuranceType(InsuranceType.DENTAL);
		individualPlanStr = planDisplayRestClient.getPlans(pdSession, true, null, null, null);
		type = new TypeToken<List<IndividualPlan>>() {}.getType();
		newIndividualDentalPlans = (List<IndividualPlan>)platformGson.fromJson(individualPlanStr, type);
		
		boolean hasNewHealthPlans = compareOldNNewPlans(oldIndividualHealthPlans, newIndividualHealthPlans);
		boolean hasNewDentalPlans = compareOldNNewPlans(oldIndividualDentalPlans, newIndividualDentalPlans);
		
		if (!hasNewHealthPlans && !hasNewDentalPlans)
		{
			pdSession.getPdHouseholdDTO().setKeepOnly(YorN.Y);
		}
		else
		{
			String status = null;
			Float oldHealthPremium = getPlanPremium(oldIndividualHealthPlans,healthPlanId);
			Float newHealthPremium = getPlanPremium(oldIndividualDentalPlans,healthPlanId);
			Float oldDentalPremium = getPlanPremium(newIndividualHealthPlans,dentalPlanId);
			Float newDentalPremium = getPlanPremium(newIndividualDentalPlans,dentalPlanId);
			if ((oldHealthPremium != null && oldHealthPremium.equals(newHealthPremium)) && (oldDentalPremium != null && oldDentalPremium.equals(newDentalPremium)))
			{
				pdSession.getPdHouseholdDTO().setKeepOnly(YorN.N);
			}
			else
			{
				if(StringUtils.isNotBlank(healthEnrollmentID))
				{
					Map<String, Object> inputData = new HashMap<String, Object>();
					inputData.put("householdId", pdSession.getPdHouseholdDTO().getId());
					inputData.put("enrollmentId", healthEnrollmentID);
							 		 
					PlanDisplayRequest planDisplayRequest = new PlanDisplayRequest();
					planDisplayRequest.setInputData(inputData);
					status = planDisplayRestClient.sendAdminUpdate(planDisplayRequest);
				}
				if(StringUtils.isNotBlank(dentalEnrollmentID))
				{
					Map<String, Object> inputData = new HashMap<String, Object>();
				inputData.put("householdId", pdSession.getPdHouseholdDTO().getId());
					inputData.put("enrollmentId", dentalEnrollmentID);
						 		 
				PlanDisplayRequest planDisplayRequest = new PlanDisplayRequest();
				planDisplayRequest.setInputData(inputData);
					status = planDisplayRestClient.sendAdminUpdate(planDisplayRequest);
				}
				return "redirect:/enrollment/adminupdateconfirm?statusmessage="+status;
			}
		}
		return null;
	}
	
	private Float getPlanPremium(List<IndividualPlan> individualPlans, Integer dentalPlanId) {
		
		for(IndividualPlan individualPlan : individualPlans)
		{
			if(dentalPlanId.equals(individualPlan.getPlanId()))
			{
				return individualPlan.getPremium();
			}
		}
		return null;
	}

	public void processEnrollment(PdSession pdSession) {
		List<PdPersonDTO> pdPersonDTOList = pdSession.getPdPersonDTOList();		
		PdHouseholdDTO pdHouseholdDTO = pdSession.getPdHouseholdDTO();
		
		pdSession.setInitialHealthPlanAvailable(YorN.N);
		pdSession.setInitialDentalPlanAvailable(YorN.N);
		
		String healthEnrollmentID = null;
		String dentalEnrollmentID = null;	
		String healthSubscriberZipcode = null;
		String healthSubscriberCountycode = null;
		String dentalSubscriberZipcode = null;
		String dentalSubscriberCountycode = null;
		String changePlanAllowedHealth = null;
		String changePlanAllowedDental = null;
		
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		boolean isCAProfile = stateCode != null ? stateCode.equalsIgnoreCase("CA") : false;
		for (PdPersonDTO pdPersonDTO : pdPersonDTOList) {
			if(healthEnrollmentID == null || "".equals(healthEnrollmentID)) {
				healthEnrollmentID = pdPersonDTO.getHealthEnrollmentId() != null ? pdPersonDTO.getHealthEnrollmentId().toString() : null;
			}
			if(dentalEnrollmentID == null || "".equals(dentalEnrollmentID)) {
				dentalEnrollmentID = pdPersonDTO.getDentalEnrollmentId() != null ? pdPersonDTO.getDentalEnrollmentId().toString() : null;
			}
		}
			
		if(StringUtils.isNotBlank(healthEnrollmentID)) {
			EnrollmentResponse enrollmentResponse = planDisplayRestClient.findPlanIdAndOrderItemIdByEnrollmentId(healthEnrollmentID);
			
			if(enrollmentResponse != null && enrollmentResponse.getPlanId() != null && enrollmentResponse.getCmsPlanId() != null )	{
				String initialHealthCmsPlanId = enrollmentResponse.getCmsPlanId();
				String initialHealthSubscriberId = enrollmentResponse.getSubscriberMemberId();
				Float initialHealthPremium = enrollmentResponse.getGrossPremiumAmt();
				BigDecimal initialHealthSubsidy = BigDecimal.valueOf(enrollmentResponse.getGrossPremiumAmt()).subtract(BigDecimal.valueOf(enrollmentResponse.getNetPremiumAmt()));
				healthSubscriberZipcode = StringUtils.isNotBlank(enrollmentResponse.getSubscriberZip()) ? enrollmentResponse.getSubscriberZip() : null;
				healthSubscriberCountycode = StringUtils.isNotBlank(enrollmentResponse.getSubscriberCountyCode()) ? enrollmentResponse.getSubscriberCountyCode() : null;
				changePlanAllowedHealth = StringUtils.isNotBlank(enrollmentResponse.getChangePlanAllowed()) ? enrollmentResponse.getChangePlanAllowed() : null;

				/**
				 * HIX-115156, PM did not want to refactor initialHealthSubsidy logic.
				 * So we subtract stateSubsidy (if was used in previous net_premium calculation)
				 */
				if (enrollmentResponse.getStateSubsidyAmt() != null){
					BigDecimal initialStateSubsidy = enrollmentResponse.getStateSubsidyAmt();
					pdSession.setInitialStateSubsidy(initialStateSubsidy);
					initialHealthSubsidy = initialHealthSubsidy.subtract(initialStateSubsidy);
				}

				pdSession.setInitialHealthPremium(initialHealthPremium);
				pdSession.setInitialHealthSubsidy(initialHealthSubsidy.setScale(2,RoundingMode.HALF_UP).floatValue());
				pdSession.setInitialHealthSubscriberId(initialHealthSubscriberId);
				pdSession.setInsuranceType(InsuranceType.HEALTH);
				pdSession.setInitialHealthPlanId(Long.valueOf(enrollmentResponse.getPlanId()));
				pdSession.setInitialHealthCmsPlanId(initialHealthCmsPlanId);

				pdSession.setInitialMaxAPTC(enrollmentResponse.getMaxAptc());
				
				List<IndividualPlan> individualPlans = new ArrayList<IndividualPlan>();

				String isEligibleCatastrophic = "N";
				if(pdSession.getShowCatastrophicPlan())
				{
					isEligibleCatastrophic = "Y";
				}
				if(EnrollmentType.S.equals(pdHouseholdDTO.getEnrollmentType())){
					String coverageDate = DateUtil.dateToString(pdHouseholdDTO.getCoverageStartDate(), "yyyy-MM-dd");
					Plan planDetailObj = planDisplayRestClient.getPlanDetails(initialHealthCmsPlanId, coverageDate);
					if(pdHouseholdDTO.getCostSharing().toString()!=null && planDetailObj.getPlanHealth() != null && planDetailObj.getPlanHealth().getPlanLevel() != null ){
						if((!isCAProfile) || (isCAProfile && "SILVER".equalsIgnoreCase(planDetailObj.getPlanHealth().getPlanLevel())) ){
							String csrValue = pdHouseholdDTO.getCostSharing() != null ? pdHouseholdDTO.getCostSharing().toString() : StringUtils.EMPTY;
							initialHealthCmsPlanId = planDisplayRestClient.appendHiosPlanIdWithCSR(initialHealthCmsPlanId, csrValue, pdHouseholdDTO.getId().toString(), planDetailObj.getPlanHealth().getPlanLevel(), isEligibleCatastrophic);
							String newCostSharing = initialHealthCmsPlanId.substring((initialHealthCmsPlanId.length() - 2), initialHealthCmsPlanId.length());
							CostSharing costSharingChange = this.getCostSharing(newCostSharing);
							if(!costSharingChange.equals(pdHouseholdDTO.getCostSharing())){
								pdHouseholdDTO.setCostSharing(costSharingChange);
							}
						}
					}
				}
				
				if(EnrollmentType.A.equals(pdHouseholdDTO.getEnrollmentType()))
				{
					PdCrossWalkIssuerIdRequest crossWalkRequest = new PdCrossWalkIssuerIdRequest();
					crossWalkRequest.setZipcode(pdSession.getPdPersonDTOList().get(0).getZipCode());
					crossWalkRequest.setCountyCode(pdSession.getPdPersonDTOList().get(0).getCountyCode());
					crossWalkRequest.setCoverageDate(DateUtil.dateToString(pdHouseholdDTO.getCoverageStartDate(), "yyyy-MM-dd"));
					String csrValue = pdHouseholdDTO.getCostSharing() != null ? pdHouseholdDTO.getCostSharing().toString() : "";
					crossWalkRequest.setCsrValue(csrValue);
					PdSubscriberData pdSubscriberData = new PdSubscriberData();
					pdSubscriberData.setHouseholdId(pdHouseholdDTO.getId());
					pdSubscriberData.setInitialSubscriberId(initialHealthSubscriberId);
					pdSubscriberData.setInsuranceType(InsuranceType.HEALTH);
					pdSubscriberData.setSpecialEnrollmentFlowType(EnrollmentFlowType.KEEP);
					crossWalkRequest.setPdSubscriberData(pdSubscriberData);
					
					crossWalkRequest.setIsEligibleForCatastrophic(isEligibleCatastrophic);
					crossWalkRequest.setRenewalPlanId(initialHealthCmsPlanId);
					PdResponse response = planDisplayRestClient.getCrossWalkIssuerPlanId(crossWalkRequest);
					if(response != null)
					{
						if(response.getResponse()!= null)
						{
							initialHealthCmsPlanId = response.getResponse();
						}
					}
				}
				/*
				 * If enrollmentTYPE is A then get the crosswalkID
				 * put it as initialHealthCmsPlanId and let the flow confinue
				 */
				String individualPlanStr = planDisplayRestClient.getPlans(pdSession, false, initialHealthCmsPlanId, null, null);
								
				Type type = new TypeToken<List<IndividualPlan>>() {}.getType();
				individualPlans = (List<IndividualPlan>)platformGson.fromJson(individualPlanStr, type);

				if(individualPlans != null && !individualPlans.isEmpty() && individualPlans.size() == 1) {
					pdSession.setInitialHealthPlanAvailable(YorN.Y);
					savePdOrderItem(pdSession, individualPlans.get(0));
					if(EnrollmentType.A.equals(pdHouseholdDTO.getEnrollmentType()))
					{
						pdSession.setInitialHealthPlanId(Long.valueOf(individualPlans.get(0).getPlanId()));
					}
					Float subsidyForCalculations = pdHouseholdDTO.getMaxSubsidy();
					if(EnrollmentType.S.equals(pdHouseholdDTO.getEnrollmentType()) && pdHouseholdDTO.getAptcForKeep() != null)
					{
						subsidyForCalculations = pdHouseholdDTO.getAptcForKeep();
					}	
					else
					{
						subsidyForCalculations = pdHouseholdDTO.getMaxSubsidy();
					}
					
					if(individualPlans.get(0).getAptc() != 0 && subsidyForCalculations != null && !Float.valueOf(0).equals(subsidyForCalculations)){
						float remainingAptc = BigDecimal.valueOf(subsidyForCalculations).subtract(BigDecimal.valueOf(individualPlans.get(0).getAptc())).setScale(2,RoundingMode.HALF_UP).floatValue();
						pdHouseholdDTO.setHealthSubsidy(individualPlans.get(0).getAptc());
						
						if(!isCAProfile)
						{
						pdHouseholdDTO.setDentalSubsidy(new BigDecimal(Float.toString(remainingAptc)).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
						}
						planDisplayRestClient.updateHouseholdSubsidy(pdHouseholdDTO);
					}
				}					
			} 
		}
			
		if(StringUtils.isNotBlank(dentalEnrollmentID)) {
			if(YorN.N.equals(pdSession.getInitialHealthPlanAvailable()) && YorN.Y.equals(pdSession.getPdHouseholdDTO().getAutoRenewal())){
				pdSession.setInitialDentalPlanAvailable(YorN.N);
			}else{
				EnrollmentResponse enrollmentResponse = planDisplayRestClient.findPlanIdAndOrderItemIdByEnrollmentId(dentalEnrollmentID);
				
				if(enrollmentResponse != null && enrollmentResponse.getPlanId() != null && enrollmentResponse.getCmsPlanId() != null) {
					String initialDentalCmsPlanId = enrollmentResponse.getCmsPlanId();
					String initialDentalSubscriberId = enrollmentResponse.getSubscriberMemberId();
					Float initialDentalPremium = enrollmentResponse.getGrossPremiumAmt();
					Float initialDentalSubsidy = new BigDecimal(Float.toString(enrollmentResponse.getGrossPremiumAmt())).subtract(new BigDecimal(Float.toString(enrollmentResponse.getNetPremiumAmt()))).setScale(2,RoundingMode.HALF_UP).floatValue();
					changePlanAllowedDental = StringUtils.isNotBlank(enrollmentResponse.getChangePlanAllowed()) ? enrollmentResponse.getChangePlanAllowed() : null;
					dentalSubscriberZipcode = StringUtils.isNotBlank(enrollmentResponse.getSubscriberZip()) ? enrollmentResponse.getSubscriberZip() : null;
					dentalSubscriberCountycode = StringUtils.isNotBlank(enrollmentResponse.getSubscriberCountyCode()) ? enrollmentResponse.getSubscriberCountyCode() : null;
					
					pdSession.setInitialDentalPremium(initialDentalPremium);
					pdSession.setInitialDentalSubsidy(initialDentalSubsidy);
					pdSession.setInitialDentalSubscriberId(initialDentalSubscriberId);
					pdSession.setInsuranceType(InsuranceType.DENTAL);
					pdSession.setInitialDentalPlanId(Long.valueOf(enrollmentResponse.getPlanId()));
					/*
					 * If enrollmentTYPE is A then get the crosswalkID
					 * put it as initialHealthCmsPlanId and let the flow confinue
					 */
					if(EnrollmentType.A.equals(pdHouseholdDTO.getEnrollmentType()))
					{
						PdCrossWalkIssuerIdRequest crossWalkRequest = new PdCrossWalkIssuerIdRequest();
						crossWalkRequest.setZipcode(pdSession.getPdPersonDTOList().get(0).getZipCode());
						crossWalkRequest.setCountyCode(pdSession.getPdPersonDTOList().get(0).getCountyCode());
						crossWalkRequest.setCoverageDate(DateUtil.dateToString(pdHouseholdDTO.getCoverageStartDate(), "yyyy-MM-dd"));
					
						crossWalkRequest.setRenewalPlanId(initialDentalCmsPlanId);
						String isEligibleCatastrophic = "N";
						if(pdSession.getShowCatastrophicPlan())
						{
							isEligibleCatastrophic = "Y";
						}
						crossWalkRequest.setIsEligibleForCatastrophic(isEligibleCatastrophic);
						

						PdSubscriberData pdSubscriberData = new PdSubscriberData();
						pdSubscriberData.setHouseholdId(pdHouseholdDTO.getId());
						pdSubscriberData.setInitialSubscriberId(initialDentalSubscriberId);
						pdSubscriberData.setInsuranceType(InsuranceType.DENTAL);
						pdSubscriberData.setSpecialEnrollmentFlowType(EnrollmentFlowType.KEEP);
						crossWalkRequest.setPdSubscriberData(pdSubscriberData);
						PdResponse response = planDisplayRestClient.getCrossWalkIssuerPlanId(crossWalkRequest);
						if(response != null)
						{
							if(response.getResponse()!= null)
							{
								initialDentalCmsPlanId = response.getResponse();
							}
						}
					}
					List<IndividualPlan> individualPlans = new ArrayList<IndividualPlan>();
					String individualPlanStr = planDisplayRestClient.getPlans(pdSession, false, initialDentalCmsPlanId, null, null);
					Type type = new TypeToken<List<IndividualPlan>>() {}.getType();
					individualPlans = (List<IndividualPlan>)platformGson.fromJson(individualPlanStr, type);
										
					if(individualPlans != null && !individualPlans.isEmpty() && individualPlans.size() == 1) {
						pdSession.setInitialDentalPlanAvailable(YorN.Y);
						savePdOrderItem(pdSession, individualPlans.get(0));
						if(EnrollmentType.A.equals(pdHouseholdDTO.getEnrollmentType()))
						{
							pdSession.setInitialDentalPlanId(Long.valueOf(individualPlans.get(0).getPlanId()));
						}
						if(!isCAProfile)
						{
						pdHouseholdDTO.setDentalSubsidy(new BigDecimal(Float.toString(individualPlans.get(0).getAptc())).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
					}
				}
			}
		}
		}
		
		
			
		if(YorN.Y.equals(pdSession.getInitialHealthPlanAvailable()) && !ProductType.D.equals(pdSession.getProductType()) ) {
			if("Y".equalsIgnoreCase(changePlanAllowedHealth)){
				pdHouseholdDTO.setKeepOnly(YorN.N);
			}else if(getAddressChangeFlag(pdPersonDTOList, pdSession.getInitialHealthSubscriberId())){
				List<Integer> planIdsForCurrentZip = planDisplayRestClient.getListOfPlanIds(pdSession, null, null,InsuranceType.HEALTH);
				List<Integer> planIdsForOldZip = planDisplayRestClient.getListOfPlanIds(pdSession, healthSubscriberZipcode, healthSubscriberCountycode,InsuranceType.HEALTH);
				if(planIdsForCurrentZip != null && !planIdsForCurrentZip.isEmpty() && planIdsForOldZip != null && !planIdsForOldZip.isEmpty()) {
					pdHouseholdDTO.setKeepOnly(YorN.Y);
					for(Integer planId : planIdsForCurrentZip) {
						if(!planIdsForOldZip.contains(planId)) {
							pdHouseholdDTO.setKeepOnly(YorN.N);
							break;
						}
					}
				}
			}
		}
		
		if(YorN.Y.equals(pdSession.getInitialDentalPlanAvailable()) && ProductType.D.equals(pdSession.getProductType()) ) {
			if("Y".equalsIgnoreCase(changePlanAllowedDental)){
				pdHouseholdDTO.setKeepOnly(YorN.N);
			}else if(getAddressChangeFlag(pdPersonDTOList, pdSession.getInitialDentalSubscriberId())){
				List<Integer> planIdsForCurrentZip = planDisplayRestClient.getListOfPlanIds(pdSession, null, null,InsuranceType.DENTAL);
				List<Integer> planIdsForOldZip = planDisplayRestClient.getListOfPlanIds(pdSession, dentalSubscriberZipcode, dentalSubscriberCountycode,InsuranceType.DENTAL);
				if(planIdsForCurrentZip != null && !planIdsForCurrentZip.isEmpty() && planIdsForOldZip != null && !planIdsForOldZip.isEmpty()) {
					for(Integer planId : planIdsForCurrentZip) {
						if(!planIdsForOldZip.contains(planId)) {
							pdHouseholdDTO.setKeepOnly(YorN.N);
							break;
						}
					}
				}
			}
		}		
	}
	
	public void processPostShopping(PdSession pdSession, List<PdOrderItemDTO> preShoppingOrderItems) {
		
		PdHouseholdDTO pdHouseholdDTO = pdSession.getPdHouseholdDTO();
		pdSession.setInitialHealthPlanAvailable(YorN.N);
		pdSession.setInitialDentalPlanAvailable(YorN.N);
		ProductType productType = pdSession.getProductType();
		
		if(preShoppingOrderItems != null && !preShoppingOrderItems.isEmpty()){
			for(PdOrderItemDTO pdOrderItemDTO : preShoppingOrderItems){
				if((ProductType.A.equals(productType) || ProductType.H.equals(productType)) && (InsuranceType.HEALTH.equals(pdOrderItemDTO.getInsuranceType()))){
					String initialHealthPlanId = pdOrderItemDTO.getPlanId().toString();
					Float initialHealthPremium = pdOrderItemDTO.getPremiumBeforeCredit();
					Float initialHealthSubsidy = pdOrderItemDTO.getMonthlySubsidy();
					
					pdSession.setInitialHealthPremium(initialHealthPremium);
					pdSession.setInitialHealthSubsidy(initialHealthSubsidy);
					pdSession.setInitialStateSubsidy(pdOrderItemDTO.getMonthlyStateSubsidy());
					pdSession.setInsuranceType(InsuranceType.HEALTH);
					pdSession.setInitialHealthPlanId(pdOrderItemDTO.getPlanId());
					
					List<IndividualPlan> individualPlans = new ArrayList<IndividualPlan>();
					String individualPlanStr = planDisplayRestClient.getPlans(pdSession, false, null, initialHealthPlanId, null);
									
					Type type = new TypeToken<List<IndividualPlan>>() {}.getType();
					individualPlans = (List<IndividualPlan>)platformGson.fromJson(individualPlanStr, type);
	
					if(individualPlans != null && !individualPlans.isEmpty() && individualPlans.size() == 1) {
						//pdSession.setInitialHealthPlanId(Long.valueOf(individualPlans.get(0).getPlanId()));
						pdSession.setInitialHealthPlanAvailable(YorN.Y);
						savePdOrderItem(pdSession, individualPlans.get(0));
						
						if(individualPlans.get(0).getAptc() != 0 && pdHouseholdDTO.getMaxSubsidy() != null && pdHouseholdDTO.getMaxSubsidy() != 0){
							float remainingAptc = BigDecimal.valueOf(pdHouseholdDTO.getMaxSubsidy()).subtract(BigDecimal.valueOf(individualPlans.get(0).getAptc())).setScale(2,RoundingMode.HALF_UP).floatValue();
							pdHouseholdDTO.setHealthSubsidy(individualPlans.get(0).getAptc());
							String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
							boolean isCAProfile = stateCode != null ? stateCode.equalsIgnoreCase("CA") : false;
							if(!isCAProfile)
							{
							pdHouseholdDTO.setDentalSubsidy(new BigDecimal(Float.toString(remainingAptc)).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
							}
							planDisplayRestClient.updateHouseholdSubsidy(pdHouseholdDTO);
						}
					}					
				}
				if((ProductType.A.equals(productType) || ProductType.D.equals(productType)) && (InsuranceType.DENTAL.equals(pdOrderItemDTO.getInsuranceType()))){
					String initialDentalPlanId = pdOrderItemDTO.getPlanId().toString();
					Float initialDentalPremium = pdOrderItemDTO.getPremiumBeforeCredit();
					Float initialDentalSubsidy = pdOrderItemDTO.getMonthlySubsidy();
					
					pdSession.setInitialDentalPremium(initialDentalPremium);
					pdSession.setInitialDentalSubsidy(initialDentalSubsidy);
					pdSession.setInsuranceType(InsuranceType.DENTAL);
					pdSession.setInitialDentalPlanId(pdOrderItemDTO.getPlanId());
					
					List<IndividualPlan> individualPlans = new ArrayList<IndividualPlan>();
					String individualPlanStr = planDisplayRestClient.getPlans(pdSession, false, null, initialDentalPlanId, null);
					Type type = new TypeToken<List<IndividualPlan>>() {}.getType();
					individualPlans = (List<IndividualPlan>)platformGson.fromJson(individualPlanStr, type);
										
					if(individualPlans != null && !individualPlans.isEmpty() && individualPlans.size() == 1) {
						//pdSession.setInitialDentalPlanId(Long.valueOf(individualPlans.get(0).getPlanId()));
						pdSession.setInitialDentalPlanAvailable(YorN.Y);
						savePdOrderItem(pdSession, individualPlans.get(0));
						String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
						boolean isCAProfile = stateCode != null ? stateCode.equalsIgnoreCase("CA") : false;
						if(!isCAProfile)
						{
						pdHouseholdDTO.setDentalSubsidy(new BigDecimal(Float.toString(individualPlans.get(0).getAptc())).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
					}
				}
			}
		}
	}
	}
	
	public PdResponse processAutoRenewal(PdSession pdSession) {
		PdResponse pdResponse = new PdResponse();
		
		processEnrollment(pdSession);
		
		if(YorN.N.equals(pdSession.getInitialHealthPlanAvailable())) {
			pdResponse.startResponse();
			pdResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			pdResponse.setErrMsg("<application Id> " + pdSession.getPdHouseholdDTO().getApplicationId() + " : Failed as Health Plan id : "+pdSession.getInitialHealthPlanId()+" is not available for autorenewal case "+ pdSession.getPdHouseholdDTO().getShoppingId());
			pdResponse.endResponse();
		}else{
			pdResponse.startResponse();
			pdResponse.setResponse(pdSession.getPdHouseholdDTO().getId().toString());
			pdResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);			
			pdResponse.endResponse();
		}
		return pdResponse;
	}
	
    public void setCatastrophicEligibility(PdSession pdSession, List<PdPersonDTO> pdPersonDTOList) 
    {
    	if(pdPersonDTOList != null && !pdPersonDTOList.isEmpty()) 
    	{
    		Boolean catastrophicEligible = planDisplayUtil.checkIfHouseholdIsCatastrophicEligible(pdPersonDTOList, DateUtil.dateToString(pdSession.getPdHouseholdDTO().getCoverageStartDate(), "MM/dd/yyyy"));
    		pdSession.setShowCatastrophicPlan(catastrophicEligible);
    	}
    	else 
    	{
    		pdSession.setShowCatastrophicPlan(false);
    	}
    }
	
	public Boolean getAddressChangeFlag(List<PdPersonDTO> pdPersonDTOList, String subscriberId) {
		if(pdPersonDTOList != null && !pdPersonDTOList.isEmpty()) {
			for(PdPersonDTO pdPersonDTO : pdPersonDTOList) {
				if((StringUtils.isNotBlank(subscriberId) && subscriberId.equalsIgnoreCase(pdPersonDTO.getExternalId())) && 
						(StringUtils.isNotBlank(pdPersonDTO.getMaintenanceReasonCode()) && "43".equals(pdPersonDTO.getMaintenanceReasonCode()))) {
					return true;
				}	
			}
		}
		return false;
	}
	
	private CostSharing getCostSharing(String csrNumber){
		CostSharing costSharing = null;

		if("01".equalsIgnoreCase(csrNumber)) {
			costSharing = CostSharing.CS1;
		}
		else if("02".equalsIgnoreCase(csrNumber)) {
			costSharing = CostSharing.CS2;
		}
		else if("03".equalsIgnoreCase(csrNumber)) {
			costSharing = CostSharing.CS3;
		}
		else if("04".equalsIgnoreCase(csrNumber)) {
			costSharing = CostSharing.CS4;
		}
		else if("05".equalsIgnoreCase(csrNumber)) {
			costSharing = CostSharing.CS5;
		}
		else if("06".equalsIgnoreCase(csrNumber)) {
			costSharing = CostSharing.CS6;
		}
		return costSharing;
	}
	
	    
}
