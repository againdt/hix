package com.getinsured.eligibility.at.ref.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.iex.ssap.model.AccountTransferMigration;

@Repository
@Transactional(readOnly = true)
public interface IAccountTransferMigrationRepository extends JpaRepository<AccountTransferMigration, Long> {

	List<AccountTransferMigration> findByTransferId(String transferId);
	
	List<AccountTransferMigration> findByHouseholdCaseId(String householdCaseId);
	
	List<AccountTransferMigration> findByNewSsapApplicationId(Long newSsapApplicationId);
}