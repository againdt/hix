package com.getinsured.hix.batch.agent.service;

import java.util.List;

import com.getinsured.hix.platform.util.exception.GIException;

public interface AgentBatchService {
	
	/**
	 * Gets the list of Active Individuals to whom actionable notices were sent previous day.
	 * 	 
	 * @throws GIException
	 */
	void sendNotices() throws GIException;
	void setAgentBrokerAvailability() throws GIException;
}
