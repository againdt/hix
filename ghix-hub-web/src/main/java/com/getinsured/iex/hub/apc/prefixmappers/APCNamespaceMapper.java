package com.getinsured.iex.hub.apc.prefixmappers;

import java.util.HashMap;
import java.util.Map;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class APCNamespaceMapper extends NamespacePrefixMapper{
	private static final Map<String, String> PREFIXES = new HashMap<String, String>();
	
	static{
		PREFIXES.put("http://aptc.dsh.cms.gov/extension/1.0","ext");
		PREFIXES.put("http://hix.cms.gov/0.1/hix-core","hcore");
		
		PREFIXES.put("http://niem.gov/niem/appinfo/2.0","i");
		PREFIXES.put("http://niem.gov/niem/appinfo/2.1","i2");
		
		PREFIXES.put("http://niem.gov/niem/niem-core/2.0","nc");
		PREFIXES.put("http://niem.gov/niem/proxy/xsd/2.0","niem-xsd");
		
		PREFIXES.put("http://niem.gov/niem/structures/2.0","s");
		PREFIXES.put("http://hix.cms.gov/0.1/hix-ee","hee");
		
		PREFIXES.put("http://aptc.dsh.cms.gov/exchange/1.0","exch");
		PREFIXES.put("http://www.w3.org/2001/XMLSchema-instance","xsi");

	}
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		
		String prefix = PREFIXES.get(namespaceUri);
		
		if(prefix != null){
			return prefix;
		}
		else{
			return suggestion;
		}
	}
}






