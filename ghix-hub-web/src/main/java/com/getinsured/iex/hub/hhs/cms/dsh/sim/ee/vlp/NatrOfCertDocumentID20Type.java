
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NatrOfCertDocumentID20Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NatrOfCertDocumentID20Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}AlienNumber"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}NaturalizationNumber"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NatrOfCertDocumentID20Type", propOrder = {
    "alienNumber",
    "naturalizationNumber"
})
public class NatrOfCertDocumentID20Type {

    @XmlElement(name = "AlienNumber", required = true)
    protected String alienNumber;
    @XmlElement(name = "NaturalizationNumber", required = true)
    protected String naturalizationNumber;

    /**
     * Gets the value of the alienNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlienNumber() {
        return alienNumber;
    }

    /**
     * Sets the value of the alienNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlienNumber(String value) {
        this.alienNumber = value;
    }

    /**
     * Gets the value of the naturalizationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNaturalizationNumber() {
        return naturalizationNumber;
    }

    /**
     * Sets the value of the naturalizationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNaturalizationNumber(String value) {
        this.naturalizationNumber = value;
    }

}
