<%@ include file="../include.jsp" %>
<div id="appscr78" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page28.otherInsuranceFor" text="Other Insurance for" />
				<strong class="nameOfHouseHold"></strong>
			</h4>
		</div>
		<form id="appscr78Form" class="form-horizontal">
			<div class="control-group">
				<p>
					Is&nbsp;<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page28.eligibleForHealthCovergae" text="eligible for health coverage from any of the following? Select even if" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page28.isntCurrentlyEnrolled" text="isn't currently enrolled." />
				</p><!-- end of label -->
				<div class="margin5-l">
					<label class="checkbox">
						<input type="checkbox" id="appscr78Medicare" name="fedHealth" value="Medicare "><spring:message code="ssap.page28.medicare" text="Medicare" />
					</label>
					
					<label class="checkbox">
						<input type="checkbox" id="appscr78Tricare" name="fedHealth" value="TRICARE"><spring:message code="ssap.page28.tricare" text="TRICARE" />
					</label>
					
					<label class="checkbox">
						<input type="checkbox" id="appscr78PeaceCorps" name="fedHealth" value="PeaceCorps"><spring:message code="ssap.page28.peaceCorps" text="Peace Corps " />
					</label>
					
					<label class="checkbox">
						<input type="checkbox" name="fedHealth" id="appscr78OtherStateFHBP" value="Otherstateorfederalhealthbenefitprogram" checked="checked">
						<spring:message code="ssap.page28.otherStateOrFederal" text="Other state or federal health benefit program" />
					</label>
					<div class="appscr78OtherStateOrFederalHealthPlan">
						<div class="control-group">
							<label class="control-label">
								<spring:message code="ssap.page28.type" text="Type " />
							</label><!-- end of label -->
							<div class="controls">
								<input type="text"  id="appscr78TypeOfProgram" name="typeTxt" placeholder="<spring:message code='ssap.page28.type' text='Type' />" class="input-medium" />
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						
						<div class="control-group">
							<label class="control-label">
								<spring:message code="ssap.page28.nameOfProgram" text="Name of Program " />
							</label><!-- end of label -->
							<div class="controls">
								<input type="text" id="appscr78NameOfProgram" name="programTxt" placeholder="<spring:message code='ssap.page28.nameOfProgram' text='Name of Program' />" class="input-medium"/>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
					</div><!-- appscr78OtherStateOrFederalHealthPlan ends -->
					
					<label class="checkbox">
						<input type="checkbox" id="appscr78None" name="fedHealth" value="None of the above"><spring:message code="ssap.noneOfTheAbove" text="None of the above" />
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
		</form>
	</div>
	
	
	<div class="Main_ui_design hide">
		<div class="cobaappDarkWithoutMarginTop" style="display:none;">
			<label><spring:message code="ssap.is" text="Is" /> <span class="nameOfHouseHold"></span> <spring:message code="ssap.is" text="Is" /><spring:message code="ssap.page28.eligibleForHealthCovergae" text="eligible for health coverage from any of the following? Select even if" /><span
				class="nameOfHouseHold"></span> <spring:message code="ssap.page28.isntCurrentlyEnrolled" text="isn't currently enrolled." />
			</label>
		</div>
		<div class="inputCheckbox marginLeft11"  style="display:none;">
		<br>
			<input type="checkbox" name="fedHealth" value="Medicare"><spring:message code="ssap.page28.medicare" text="Medicare" />
			<br> <input type="checkbox" name="fedHealth" value="TRICARE"><spring:message code="ssap.page28.tricare" text="TRICARE" />
			<br> <input type="checkbox" name="fedHealth" value="PeaceCorps"><spring:message code="ssap.page28.peaceCorps" text="Peace Corps " /><br> <input type="checkbox" name="fedHealth" checked="checked"
				value="Otherstateorfederal health benefit program"><spring:message code="ssap.page28.otherStateOrFederal" text="Other state or federal health benefit program" /><br>
			<div class="appscr78OtherStateOrFederalHealthPlan">
			<label class="contentLabel noMargin"><spring:message code="ssap.page28.type" text="Type " /></label><br>
					<input type="text" name="typeTxt" id="typeTxt"><br>
					<label class="contentLabel noMargin"><spring:message code="ssap.page28.nameOfProgram" text="Name of Program " /></label> <br>
					 <input type="text" name="programTxt" id="programTxt" /></div>
			 <input type="checkbox" name="fedHealth"
				value="None of the above"><spring:message code="ssap.noneOfTheAbove" text="None of the above" />
		</div>
	</div>
</div>