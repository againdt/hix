package com.getinsured.hix.batch.plandisplay.writer;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.batch.plandisplay.exceptions.SolrNotAvailableException;


public class PlanDisplayDentalBenefitDataWriter implements ItemStreamWriter<SolrInputDocument>{

	private static Logger logger = LoggerFactory.getLogger(PlanDisplayDentalBenefitDataWriter.class);
	
	private int count = 0;
	
	@Autowired
	private HttpSolrServer solrServer;
	
	private ObjectOutputStream outputStream;

	public void write(List<? extends SolrInputDocument> items) throws Exception {
		long start = System.currentTimeMillis();
		logger.info("Received "+items.size()+" document for solr export");
		count += items.size();
		try
		{
			SolrPingResponse res =  solrServer.ping();
			logger.info("Solr Server Ping response ->" + res.getResponseHeader());
			if( res.getStatus() == 0)
			{
				for(SolrInputDocument tmp: items){
					solrServer.add(tmp);
				}
				solrServer.commit();
				logger.info("Posted "+count+" Records in "+(System.currentTimeMillis()-start)+" ms");
			}
		}
		catch(Exception e)
		{
			logger.error("Failed to communicate with solr server"+ e);
			throw new SolrNotAvailableException("Failed to communicate with SOLR", e);
		}
	}

	public HttpSolrServer getSolrServer() {
		return solrServer;
	}

	public void setSolrServer(HttpSolrServer solrServer) {
		this.solrServer = solrServer;
	}

	@Override
	public void close() throws ItemStreamException {
		logger.debug("Attemting to close the SOLR writer");
		if(this.outputStream != null){
			try {
				this.outputStream.flush();
				this.outputStream.close();
			} catch (IOException e) {
				logger.error("Error closing the offline file stream",e);
				throw new ItemStreamException("Close failed",e);
			}
		}
		//this.solrServer.shutdown();
	}

	@Override
	public void open(ExecutionContext arg0) throws ItemStreamException {
		if(this.solrServer != null){
			this.solrServer.setMaxRetries(3);//Default 0
			this.solrServer.setAllowCompression(true); // Default false, since TC server supports gzip, let's use that. If target server doesn't, this has no effect
		}
	}

	@Override
	public void update(ExecutionContext arg0) throws ItemStreamException {
//		if(this.solrServer != null){
//			try {
//				this.solrServer.ping();
//			} catch (SolrServerException | IOException e) {
//				logger.error("Ping failed for SOLR "+e.getMessage()+" Address:"+this.solrServer.getBaseURL());
//			}
//		}
	}
}
