package com.getinsured.hix.broker.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.DesignateBroker.Status;

public interface IDesignateBrokerRepository extends JpaRepository<DesignateBroker, Integer> {
	
	DesignateBroker findDesignateBrokerById(int id);

	DesignateBroker findBrokerByEmployerId(int employerId);

	long findByEmployerId(int employerId);

	@Modifying
	@Transactional
	@Query("delete from DesignateBroker db where db.brokerId = :brokerId")
	int deleteDesignateBrokerByBrokerId(@Param(BrokerConstants.BROKER_ID) int brokerId);

	DesignateBroker findBrokerByIndividualId(int individualId);

	DesignateBroker findBrokerByExternalEmployerId(int extEmployerId);

	DesignateBroker findBrokerByExternalIndividualId(int externalIndividualId);

	/**
	 * Retrieves DesignateBroker records against brokerId and status for
	 * external individuals.
	 */
	@Query("FROM DesignateBroker as an where an.brokerId = :brokerId and an.status = :status")
	List<DesignateBroker> findDesignateBrokerForExternalInd(@Param(BrokerConstants.BROKER_ID) int brokerId,
			@Param("status") DesignateBroker.Status status);

	@Query("FROM DesignateBroker as an where an.brokerId = :brokerId and an.status = :status and an.externalEmployerId != null")
	List<DesignateBroker> findDesignateBrokerForExternalEmployer(@Param(BrokerConstants.BROKER_ID) int brokerId,
			@Param("status") Status status);

	/**
	 * Retrieves DesignateBroker records against brokerId
	 */
	@Query("FROM DesignateBroker as an where an.brokerId = :brokerId")
	List<DesignateBroker> findDesigBrokerByAgentId(@Param(BrokerConstants.BROKER_ID) int brokerId);
	
	@Query("FROM DesignateBroker as an where an.brokerId = :brokerId and an.status in :listOfStatus")
	List<DesignateBroker> findActiveAndPendingDesignateBrokers(@Param(BrokerConstants.BROKER_ID) int brokerId,
			@Param("listOfStatus") List<DesignateBroker.Status> listOfStatus);

	/**
	 * Finds {@link DesignateBroker} based on external individual id and agent id
	 * coming from AHBX and status
	 * 
	 * @param brokerId
	 * @param externalIndividualId
	 * @param status
	 * @return {@link DesignateBroker}
	 */
	@Query("FROM DesignateBroker d where d.brokerId = :brokerId AND d.externalIndividualId = :externalIndividualId AND d.status = :status")
	DesignateBroker findDesigBrokerByAgentAndIndividualAndStatus(@Param(BrokerConstants.BROKER_ID) int brokerId,
			@Param("externalIndividualId") int externalIndividualId, @Param("status") Status status);
	
	
}