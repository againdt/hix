package com.getinsured.hix.entity.enrollmententity.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.entity.enrollmententity.repository.IPopulationLanguageRepository;
import com.getinsured.hix.entity.enrollmententity.service.PopulationLanguageService;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.PopulationLanguage;

@Service("populationLanguageService")
@Transactional
public class PopulationLanguageServiceImpl implements PopulationLanguageService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PopulationLanguageServiceImpl.class);
	
	@Autowired
	private IPopulationLanguageRepository populationLanguageRepository;

	/**
	 * @see PopulationLanguageService#findById(int)
	 */
	@Override
	public PopulationLanguage findById(int ethnicityId) {
		
		LOGGER.info("findById: START");
		LOGGER.info("findById: END");
		
		return populationLanguageRepository.findById(ethnicityId);
	}

	/**
	 * @see PopulationLanguageService#findByEntityId(int)
	 */
	@Override
	public List<PopulationLanguage> findByEntityId(int entityId) {
		
		LOGGER.info("findByEntityId: START");
		LOGGER.info("findByEntityId: END");
		
		return populationLanguageRepository.findByEntityId(entityId);
	}

	/**
	 * @see PopulationLanguageService#deleteAssociatedLanguages(int)
	 */
	@Override
	public int deleteAssociatedLanguages(int entityId) {
		
		LOGGER.info("deleteAssociatedLanguages: START");
		LOGGER.info("deleteAssociatedLanguages: END");
		
		return populationLanguageRepository.deleteAssociatedLanguages(entityId);
	}

	/**
	 * @see PopulationLanguageService#saveAssociatedLanguages(EnrollmentEntity,
	 *      List)
	 */
	@Override
	public void saveAssociatedLanguages(EnrollmentEntity enrollmentEntity,
			List<PopulationLanguage> populationLanguages) {
		
		LOGGER.info("saveAssociatedLanguages: START");
		
		populationLanguageRepository.deleteAssociatedLanguages(enrollmentEntity.getId());
		
		List<PopulationLanguage> updatedPopulationLanguages = new ArrayList<>();

		if (populationLanguages != null && !populationLanguages.isEmpty()) {
			for (PopulationLanguage language : populationLanguages) {
				language.setEntity(enrollmentEntity);
				
				updatedPopulationLanguages.add(language);
			}
			
			updatedPopulationLanguages = populationLanguageRepository.save(updatedPopulationLanguages);
		}
		
		LOGGER.info("saveAssociatedLanguages: END");
	}
}
