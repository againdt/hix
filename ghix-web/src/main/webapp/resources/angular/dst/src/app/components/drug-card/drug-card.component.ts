import { trigger, transition, style, animate } from '@angular/animations';
import { DataKeepService } from './../../services/data-keep.service';
import { FetchService } from './../../services/fetch.service';
import { DosageInfo, DrugDetail } from './../../models/data.model';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'drug-card',
    templateUrl: './drug-card.component.html',
    styleUrls: ['./drug-card.component.scss'],
    animations: [trigger('abc', [
        transition(':enter', [
            style({ opacity: 0 }),
            animate(500, style({ opacity: 1 }))
        ]),
        transition(':leave', [
            animate(500, style({ opacity: 0 }))
        ])
    ])]
})
export class DrugCardComponent implements OnInit {
    @Input() drug: DrugDetail;
    @Output() destoryed = new EventEmitter<DrugDetail>();


    public drugFairPrice: number;
    public genericFairPrice: number;
    public rxFairPrice: number;
    public rxGenericFairPrice: number;
    constructor(
        private fetch: FetchService,
        private dataKeep: DataKeepService
    ) { }

    public async ngOnInit() {
/*
        if (this.drug.drugNdc) {
            await this.fetch.fairPrice(this.drug.drugNdc).subscribe(val => {
                this.drug.drugFairPrice = val.toString();
            });
            await this.fetch.rxcui(this.drug.drugNdc).subscribe(val => {
                this.drug.drugRxCode = val.toString();
            });
        }
*/
        this.dataKeep.updatePersonalizedState("DRUG", "UPDATE", this.drug);

    }

    public destroy(drug: DrugDetail): void {
        this.destoryed.emit(this.drug);
       

    }
}
