package com.getinsured.hix.provider.service;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.hix.model.ProviderUpload;
import com.getinsured.hix.platform.util.DateUtil;

public class ProviderLoadStatusDTO {

	private int id;
	private String fileName;
	private Integer planYear;
	private String providerType;
	private String status;
	private String logs;
	private String ecmFileID;
	private String createdOn;

	public ProviderLoadStatusDTO() {
	}

	public ProviderLoadStatusDTO(int id, String fileName, Integer planYear, String providerType, String status, String logs, String ecmFileID, Date createdOn) {
		this.id = id;
		this.fileName = fileName;
		this.planYear = planYear;

		if (StringUtils.isNotBlank(providerType)) {
			this.providerType = ProviderUpload.getProviderTypeIndicatorOptionsList().get(ProviderUpload.PROVIDER_TYPE_INDICATOR.valueOf(providerType));
		}

		if (StringUtils.isNotBlank(status)) {
			this.status = ProviderUpload.getStatusOptionsList().get(ProviderUpload.STATUS.valueOf(status));
		}
		this.logs = logs;
		this.ecmFileID = ecmFileID;

		if (null != createdOn) {
			this.createdOn = DateUtil.dateToString(createdOn, "MMM dd, yyyy");
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getPlanYear() {
		return planYear;
	}

	public void setPlanYear(Integer planYear) {
		this.planYear = planYear;
	}

	public String getProviderType() {
		return providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLogs() {
		return logs;
	}

	public void setLogs(String logs) {
		this.logs = logs;
	}

	public String getEcmFileID() {
		return ecmFileID;
	}

	public void setEcmFileID(String ecmFileID) {
		this.ecmFileID = ecmFileID;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
}
