package com.getinsured.eligibility.at.resp.si.handler;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.mes.service.AtSpanProcessingService;
import com.getinsured.eligibility.at.ref.util.ExceptionUtil;
import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.resp.si.dto.TaxHouseholdMember;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.util.TSDate;

/**
 * EligibilityDecisionHandler to compare eligibilities.
 *
 * Take decision based on current and enrolled applications; if enrolled application found
 *
 * Take decision based current applications; if enrolled application not found
 *
 * EligibilityDecisionHandler updates Application status - {ER, EN and CL}
 *
 * @author Ekram
 *
 */
@Component
public class IdahoEligibilityDecisionHandler{

	private static final String POSSIBLE_DATA_ISSUE_ACTIVE_APP = "Possible Data issue : Invalid data found in ExchangeEligibilityStatus for Enrolled (Active) application - ";

	private static final String POSSIBLE_DATA_ISSUE_CURRENT_APP = "Possible Data issue : Invalid data found in ExchangeEligibilityStatus for Current application - ";

	private static final Logger LOGGER = Logger.getLogger(IdahoEligibilityDecisionHandler.class);

	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	AtSpanProcessingService atSpanProcessingService;

	@Autowired
	private ExceptionUtil exceptionUtil;
	
	private static final Set<ExchangeEligibilityStatus> VALID_EXCHANGE_ELIGIBILITY_STATUS_WITH_MEDICAID = EnumSet.of(ExchangeEligibilityStatus.APTC,
			ExchangeEligibilityStatus.APTC_CSR, ExchangeEligibilityStatus.QHP, ExchangeEligibilityStatus.CSR, ExchangeEligibilityStatus.MEDICAID);

	private static final Set<ExchangeEligibilityStatus> VALID_EXCHANGE_ELIGIBILITY_STATUS = EnumSet.of(ExchangeEligibilityStatus.APTC,
			ExchangeEligibilityStatus.APTC_CSR, ExchangeEligibilityStatus.QHP, ExchangeEligibilityStatus.CSR);
	
	public String processERP(Message<ERPResponse> message){

		ERPResponse erpResponse = (ERPResponse) message.getHeaders().get(EligibilityConstants.ERP_RESPONSE);

		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put(EligibilityConstants.SSAP_APPLICATION_ID, erpResponse.getApplicationID());

		try {
			boolean flag = preProcess(erpResponse);

			if (flag){
				resultMap.put(EligibilityConstants.ELIGIBILTY_DECISION_RESULT, EligibilityConstants.AUTO);
			} else {
				resultMap.put(EligibilityConstants.ELIGIBILTY_DECISION_RESULT, EligibilityConstants.MANUAL);
			}

		} catch (Exception e) {

			StringBuilder errorReason = new StringBuilder().append(EligibilityConstants.ERROR_PROCESSING_ERP_REQUEST_FOR_SSAP_APPLICATION_ID).
					append(erpResponse.getApplicationID()).append(EligibilityConstants.REASON).append(e.getMessage());

			LOGGER.error(errorReason.toString());
			resultMap.put(EligibilityConstants.ELIGIBILTY_DECISION_RESULT, EligibilityConstants.ERROR);
			resultMap.put(EligibilityConstants.ERROR_REASON, errorReason.toString());
			exceptionUtil.persistGiMonitorId(erpResponse.getSsapApplicationPrimaryKey(), e);
		} finally {
			resultMap.put(EligibilityConstants.ERP_RESPONSE, erpResponse);
		}

		return EligibilityUtils.marshal(resultMap);

	}


	private boolean preProcess(ERPResponse erpResponse){

		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumber(erpResponse.getApplicationID());

		if (ssapApplicationList.isEmpty()){
			throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES);
		}

		SsapApplication currentApplication = ssapApplicationList.get(0);

		SsapApplication enrolledApplication = ssapApplicationRepository.findLatestEnPnSsapApplicationForCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear());

		String mutipleEligibiltySpanEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_MULTIPLE_ELIGIBILITY_SPAN_CONFIG);
		if(mutipleEligibiltySpanEnabled.equalsIgnoreCase(Boolean.TRUE.toString())) {
			// close the previously ER applications
			atSpanProcessingService.closePreviousERApplications(currentApplication);
		}
		
		/** Check for Enrolled app...
		if found then do rest of the comparison
		else skip comparison
		 */
		// Set Application Status to ELIGIBILITY_RECEIVED
		updateCurrentAppToER(currentApplication, erpResponse.getAtResponseType());

		if (enrolledApplication != null) {
			LOGGER.debug("Enrolled (Active) Application found for - " + erpResponse.getApplicationID());
			compareAndProcess(currentApplication, enrolledApplication, erpResponse.getTaxHouseholdMemberList());
		} else {
			process(currentApplication);
		}

		return true;

	}

	private void updateCurrentAppToER(SsapApplication currentApplication, String atResponseType) {
		setApplicationStatus(currentApplication, ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
		setEligibilityReceivedDateAndType(currentApplication, atResponseType);
	}


	private void setEligibilityReceivedDateAndType(SsapApplication currentApplication, String atResponseType) {
		currentApplication.setEligibilityReceivedDate(new TSDate());
		currentApplication.setEligibilityResponseType(atResponseType);
		ssapApplicationRepository.save(currentApplication);
	}

	/**
	 * Process currentApplication.
	 * This is for applications NOT having enrolled application (active application).
	 * @param currentApplication
	 */
	private void process(SsapApplication currentApplication){

		if (!isExchangeEligible(currentApplication)){
			closeApplication(currentApplication);
			return;
		}

		ExchangeEligibilityStatus currentExchangeEligibilityStatus = currentApplication.getExchangeEligibilityStatus();
		
		String markMedicaidAppToER = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_MARK_MEDICAID_APP_TO_ER);
		Set<ExchangeEligibilityStatus> valid_exchange_eligibility_status = EligibilityConstants.TRUE.equalsIgnoreCase(markMedicaidAppToER)? VALID_EXCHANGE_ELIGIBILITY_STATUS_WITH_MEDICAID : VALID_EXCHANGE_ELIGIBILITY_STATUS;

		if (valid_exchange_eligibility_status.contains(currentExchangeEligibilityStatus)){
			// Allow Enrollment
			setAllowEnrollment(currentApplication, EligibilityConstants.Y);
		} else {
			// boundary condition...
			setAllowEnrollment(currentApplication, EligibilityConstants.N);
			closeApplication(currentApplication);
		}
	}


	private void closeApplication(SsapApplication ssapApplication) {
		setApplicationStatus(ssapApplication, ApplicationStatus.CLOSED.getApplicationStatusCode());
	}


	private boolean isExchangeEligible(SsapApplication currentApplication) {
		String markMedicaidAppToER = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_MARK_MEDICAID_APP_TO_ER);
		return (EligibilityConstants.TRUE.equalsIgnoreCase(markMedicaidAppToER) && currentApplication.getEligibilityStatus() == EligibilityStatus.AM) || currentApplication.getEligibilityStatus() == EligibilityStatus.AE || currentApplication.getEligibilityStatus() == EligibilityStatus.AX
				|| currentApplication.getEligibilityStatus() == EligibilityStatus.CAE || currentApplication.getEligibilityStatus() == EligibilityStatus.CAX;
	}

	/**
	 * Compares enrolledApplication with the currentApplication.
	 *
	 * @param currentApplication
	 * @param enrolledApplication
	 * @param atTaxHHList
	 */
	private void compareAndProcess(SsapApplication currentApplication, SsapApplication enrolledApplication, List<TaxHouseholdMember> atTaxHHList){


		if (!isExchangeEligible(currentApplication)){

			// Is anyone enrolled in enrolledApplication; landed here because of enrolled application
			// If YES, set allow enrollment to N and status to ER in current app and leave active app in EN state
			// Do not Close current App and active app

			setAllowEnrollment(currentApplication, EligibilityConstants.N);
			setApplicationStatus(currentApplication, ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
			return;
		}

		enrolledExchangeEligibilityStatusToNewStatus(currentApplication, enrolledApplication, atTaxHHList);

	}


	private void enrolledExchangeEligibilityStatusToNewStatus(SsapApplication currentApplication, SsapApplication enrolledApplication,
			List<TaxHouseholdMember> atTaxHHList) {

		ExchangeEligibilityStatus currentExchangeEligibilityStatus = currentApplication.getExchangeEligibilityStatus();
		ExchangeEligibilityStatus enrolledExchangeEligibilityStatus = enrolledApplication.getExchangeEligibilityStatus();

		LOGGER.debug(enrolledExchangeEligibilityStatus + " - "  + currentExchangeEligibilityStatus);

		switch (enrolledExchangeEligibilityStatus) {

		case NONE:
			//boundary condition... will this happen???
			throw new GIRuntimeException(POSSIBLE_DATA_ISSUE_ACTIVE_APP + enrolledExchangeEligibilityStatus);

		case QHP:
			//This is a case NF-F
			setAllowEnrollment(currentApplication, EligibilityConstants.Y);
			break;
		case APTC_CSR:
			handleAPTC_CSRToOthers(currentApplication, enrolledApplication, atTaxHHList, currentExchangeEligibilityStatus);
			break;

		case APTC:
			handleAPTCToOthers(currentApplication, enrolledApplication, atTaxHHList, currentExchangeEligibilityStatus);
			break;
		default:
			break;
		}
	}


	private void handleAPTCToOthers(SsapApplication currentApplication,
			SsapApplication enrolledApplication,
			List<TaxHouseholdMember> atTaxHHList,
			ExchangeEligibilityStatus currentExchangeEligibilityStatus) {

		switch (currentExchangeEligibilityStatus) {
		case APTC:
			handleCurrentAPTC(currentApplication, enrolledApplication, atTaxHHList);
			break;

		case APTC_CSR:
			setAllowEnrollment(currentApplication, EligibilityConstants.Y);
			break;

		case QHP:
			setAllowEnrollment(currentApplication, EligibilityConstants.Y);
			break;
			//throw new GIRuntimeException(POSSIBLE_DATA_ISSUE_CURRENT_APP + currentExchangeEligibilityStatus);

		case NONE:
			setAllowEnrollment(currentApplication, EligibilityConstants.Y);
			break;
			//throw new GIRuntimeException(POSSIBLE_DATA_ISSUE_CURRENT_APP + currentExchangeEligibilityStatus);
		case MEDICAID:
			String markMedicaidAppToER = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_MARK_MEDICAID_APP_TO_ER);
			if(EligibilityConstants.TRUE.equalsIgnoreCase(markMedicaidAppToER))
			{
				setAllowEnrollment(currentApplication, EligibilityConstants.Y);
			}
			break;
		default:
			break;
		}
	}


	private void handleAPTC_CSRToOthers(SsapApplication currentApplication,
			SsapApplication enrolledApplication,
			List<TaxHouseholdMember> atTaxHHList,
			ExchangeEligibilityStatus currentExchangeEligibilityStatus) {

		switch (currentExchangeEligibilityStatus) {

		case APTC_CSR:
			handleAPTC_CSRToAPTC_CSR(currentApplication, enrolledApplication, atTaxHHList);
			break;

		case APTC:
			handleCurrentAPTC(currentApplication, enrolledApplication, atTaxHHList);
			break;

		case QHP:
			setAllowEnrollment(currentApplication, EligibilityConstants.Y);
			break;
			//throw new GIRuntimeException(POSSIBLE_DATA_ISSUE_CURRENT_APP + currentExchangeEligibilityStatus);

		case NONE:
			setAllowEnrollment(currentApplication, EligibilityConstants.Y);
			break;
			//throw new GIRuntimeException(POSSIBLE_DATA_ISSUE_CURRENT_APP + currentExchangeEligibilityStatus);
		case MEDICAID:
			String markMedicaidAppToER = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_MARK_MEDICAID_APP_TO_ER);
			if(EligibilityConstants.TRUE.equalsIgnoreCase(markMedicaidAppToER))
			{
				setAllowEnrollment(currentApplication, EligibilityConstants.Y);
			}
			break;
		default:
			break;
		}
	}


	private void handleAPTC_CSRToAPTC_CSR(SsapApplication currentApplication,
			SsapApplication enrolledApplication,
			List<TaxHouseholdMember> atTaxHHList) {
		if (!StringUtils.equalsIgnoreCase(currentApplication.getCsrLevel(), enrolledApplication.getCsrLevel())){
			// Allow Enrollment
			LOGGER.debug("APTC_CSR-APTC_CSR + CSRLEVEL changed --> " + enrolledApplication.getCsrLevel() + " " + currentApplication.getCsrLevel());
			setAllowEnrollment(currentApplication, "Y");
		} else {
			processChangeInEligibilityIndicators(currentApplication, enrolledApplication, atTaxHHList);
		}
	}


	private void processChangeInEligibilityIndicators(SsapApplication currentApplication,
			SsapApplication enrolledApplication,
			List<TaxHouseholdMember> atTaxHHList) {
		/**
		 * New Member added - Allow Enrollment
		 *
		 * Change in APTC amount - Allow Enrollment
		 * No Change in APTC amount - Move Enrollment, close Active and make current Active
		 *
		 * */
		boolean isMemberAddedOrRemoved = isMemberAddedOrRemoved(enrolledApplication, atTaxHHList);

		if (isMemberAddedOrRemoved){
			LOGGER.debug("Member Added or Removed-Y ");
			setAllowEnrollment(currentApplication, "Y");
		} else {
			LOGGER.debug("Member Added or Removed-N");
			boolean isAPTCAmountChanged = isAPTCAmountChanged(currentApplication, enrolledApplication);

			if (isAPTCAmountChanged){
				LOGGER.debug("Member Added or Removed-N + APTCAmountChanged-Y");
				setAllowEnrollment(currentApplication, "Y");
			} else {
				LOGGER.debug("Member Added or Removed-N + APTCAmountChanged-N");
				handleEnrollment(currentApplication, enrolledApplication, atTaxHHList);
			}
		}
	}


	private boolean isMemberAddedOrRemoved(SsapApplication enrolledApplication,
			List<TaxHouseholdMember> atTaxHHList) {

		return false;
	}


	private void handleCurrentAPTC(SsapApplication currentApplication, SsapApplication enrolledApplication, List<TaxHouseholdMember> atTaxHHList) {

		processChangeInEligibilityIndicators(currentApplication, enrolledApplication, atTaxHHList);

	}


	private void handleEnrollment(SsapApplication currentApplication,
			SsapApplication enrolledApplication, List<TaxHouseholdMember> atTaxHHList) {
		/* This is handled in LceDecisionHandlerServiceImpl */
	}

	private void setApplicationStatus(SsapApplication ssapApplication, String applicationStatus){
		ssapApplication.setApplicationStatus(applicationStatus);
		ssapApplicationRepository.save(ssapApplication);

	}
	private void setAllowEnrollment(SsapApplication currentApplication, String isAllowEnrollment){

		LOGGER.debug("isAllowEnrollment - " + isAllowEnrollment );
		currentApplication.setAllowEnrollment(isAllowEnrollment);
		ssapApplicationRepository.save(currentApplication);

	}

	private boolean isAPTCAmountChanged(SsapApplication currentApplication,	SsapApplication enrolledApplication) {

		if (currentApplication.getMaximumAPTC() != null && enrolledApplication.getMaximumAPTC() != null){
			return currentApplication.getMaximumAPTC().compareTo(enrolledApplication.getMaximumAPTC()) == 0 ? false : true;
		}
		return true;
	}

}
