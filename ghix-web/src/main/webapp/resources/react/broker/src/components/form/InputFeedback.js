import React from 'react';
import classNames from 'classnames';

// Input feedback
const InputFeedback = ({ error, errorClasses }) =>
  error ? <div className={classNames(errorClasses)}>{error}</div> : null;

export default InputFeedback;
