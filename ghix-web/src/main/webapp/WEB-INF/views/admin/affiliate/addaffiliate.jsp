<%@page import="java.util.ArrayList"%>
<%@page import="antlr.collections.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.getinsured.hix.platform.util.*"   %>
<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 0px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

<style type="text/css">
.accordion-inner {
	padding: 10px 20px 0 20px;
}

.accordion-inner  h3 {
	margin-bottom: 15px;
	color: #ccc;
	border-bottom: solid 1px #e1e3e3;
}

.accordion-inner #sliderEmp,.accordion-inner #sliderDep {
	width: 95%;
	margin: 50px 20px 0px 20px;
}
.dob label {display: inline;}
</style>





<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/general.css" />" media="screen" />
<style>
.table th, .table th a {
	background:#666;
	color:#fff;
}
table th {
	height:22px;
}
</style>

<script type="text/javascript" src="<:url value="/resources/js/affiliate/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/affiliate/parsley.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/affiliate/highlight.min.js" />"></script>




<div class="">
<div class="row-fluid">
	<div class="gutter10">
        <div id="skip" class="hide"></div>
        <ul class="page-breadcrumb">
            <li><a href="#">&lt;  Back</a></li>
            <li><a href="#">Add Affiliate</a></li>
        </ul><!--page-breadcrumb ends-->
        <h1>Add Affiliate</h1>
	</div>
</div>

   <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
	</div>
	
	
	 <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:forEach items="${FieldError}" var="element"> 
			  <tr>
			    <td><p><c:out value="${element}"></c:out><p/></td>
			  </tr>
			</c:forEach>
			<br>
		</div>
	</div> 
	
	
	
<%-- <div class="header">
        <h4 class="span10">Company information</h4>
        <a class="btn btn-small" href="<c:url value="/admin/affiliate/manageaffiliate"/>"><spring:message  code="label.cancel"/></a> 
		<a id="save-affiliate" class="btn btn-primary btn-small" ><spring:message  code="label.save"/></a>
    </div> --%>
	<form class="form-horizontal" id="frmaddaffiliate" name="frmaddaffiliate" action="<c:url value="/admin/affiliate/addaffiliate" />" method="POST">
		<fieldset id="individual_member_0">
		<legend class="removemember" id="">Company Information</legend>
		<div class="left-col-w" style="width:470px; float:left;">
			<div class="control-group">
				<label for="companyname" class="control-label required">Company Name <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="companyName" name="companyName" value="${affiliate.companyName}" data-required="true" class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
			<div class="control-group">
				<label for="Address" class="control-label">Address <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="location.address1" name="location.address1" value="${affiliate.location.address1}" data-required="true"   class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
			<div class="control-group">
				<label for="City" class="control-label">City <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="location.city" name="location.city" value="${affiliate.location.city}" data-required="true"   class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
				<div class="control-group">
				<label class="control-label">Select State <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<select id="select-required" name="location.state"  data-required="true" class="parsley-validated parsley-error"  >
				    <option value="">Select State</option>
					<c:forEach var="stateName" items="${stateList}">
                    <option <c:if test="${state == stateName.code}">selected</c:if> value="<c:out value="${stateName.code}" />">
                    <c:out value="${stateName.name}" />
                    </option>
                   </c:forEach>
				</select>
			</div>
			
			<div class="control-group">
				<label for="Zip" class="control-label">Zip <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="location.zip" maxlength="5" name="location.zip" value="${affiliate.location.zip}" data-required="true" data-type="number" data-trigger="keyup" data-rangelength="[5,5]" data-rangelength-message="This value should be 5 digits" "class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
		</div>
		<div class="right-col-width"style="width:470px; float:right;">
			
		
			<div class="control-group">
				<label for="Website" class="control-label">Website <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="Website" name="Website"  value="${affiliate.website}" data-required="true" data-trigger="change" data-type="url" class="parsley-validated"  class="xlarge" size="60">
			</div>
			
			<div class="control-group">
				<label for="Accountexecutive" class="control-label">Account Executive <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="accountExecutive" name="accountExecutive" value="${affiliate.accountExecutive}"  data-required="true" class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
			<div class="control-group">
				<label for="email" class="control-label">Email <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="email" name="email" value="${affiliate.email}" data-trigger="change" data-required="true" data-type="email"  class="xlarge" size="60">
			</div>
			
			<div class="control-group">
				<label for="Phone" class="control-label">Phone <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="phone" name="phone" maxlength="10" value="${affiliate.phone}" data-required="Phone"  data-type="phone" class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
			<div class="control-group">
				<label for="affiliatemanager" class="control-label">Affiliate Manager <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="managerName" name="managerName" value="${affiliate.managerName}"  data-required="true" class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			<div class="control-group">
			<input type="submit" class="btn btn-primary btn-small" id="save-user" value="Save">
			<input type="button" class="btn btn-primary btn-small" id="cancel-affiliate" value="Cancel">
			</div>
			
		</div>
		
		</fieldset>
		
	
	</form>
</div>

<script type="text/javascript">
	  
	   $('#frmaddaffiliate').parsley( 'addListener', {
			onFormSubmit: function (isFormValid, event, ParsleyForm) {
				// if field is not visible, do not apply Parsley validation!
				if(isFormValid === "true"){
					var url='<c:url value="/admin/affiliate/addaffiliate" />';
					 $.post(url,function() {
						location.href='<c:url value="/admin/affiliate/addaffiliate" />'
					 });
				}
			} 
		}); 
		
	   
	   $('#cancel-affiliate').click(function (){
			  window.location='<c:url value="/admin/affiliate/manage"/>'
		  });

</script>

    