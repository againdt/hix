/**
 * 
 */
package com.getinsured.hix.account.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Tenant;

/**
 * @author gorai_k
 *
 */
public interface ITenantRepository extends JpaRepository<Tenant, Integer>, RevisionRepository<Tenant, Integer, Integer>{

	@Query("SELECT Tenant FROM Tenant Tenant WHERE Tenant.code= :code")
	Tenant getTenantByTenantCode(@Param("code") String code);
	
	@Query("SELECT Tenant.code,Tenant.name FROM Tenant Tenant")
	List<Tenant> getTenantCodeAndNameMap();
}
