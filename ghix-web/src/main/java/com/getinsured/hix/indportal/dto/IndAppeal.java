package com.getinsured.hix.indportal.dto;

public class IndAppeal {

	private String caseNumber;
	private String appealReason;
	private String appealSubtype;
	private String fileId;

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getAppealReason() {
		return appealReason;
	}

	public void setAppealReason(String appealReason) {
		this.appealReason = appealReason;
	}

	public String getAppealSubtype() {
		return appealSubtype;
	}

	public void setAppealSubtype(String appealSubtype) {
		this.appealSubtype = appealSubtype;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IndAppeal [caseNumber=");
		builder.append(caseNumber);
		builder.append(", appealReason=");
		builder.append(appealReason);
		builder.append(", appealSubtype=");
		builder.append(appealSubtype);
		builder.append(", fileId=");
		builder.append(fileId);
		builder.append("]");
		return builder.toString();
	}

}
