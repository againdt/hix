/**
 * 
 */
package com.getinsured.consumer.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.getinsured.consumer.exceptions.ConsumerServiceException;
import com.getinsured.consumer.util.GoodRxUtil;
import com.getinsured.hix.dto.consumer.goodrx.GoodRxResponseDTO;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author bhatt_s
 * 
 */
@Service("goodRxService")
public class GoodRxServiceImpl implements GoodRxService {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GoodRxServiceImpl.class);

	@Override
	public String getFairPrice(String args) {
		String queryString = createPairBasedQueryString(args);
		HttpGet request = new HttpGet(GoodRxService.FAIR_PRICE_SERVICE_ENDPOINT
				+ queryString);
		return executeHttpRequest(request);

	}

	@Override
	public String comparePrice(String args) {
		String queryString = createPairBasedQueryString(args);
		HttpGet request = new HttpGet(
				GoodRxService.COMPARE_PRICE_SERVICE_ENDPOINT + queryString);
		return executeHttpRequest(request);
	}

	@Override
	public String drugSearch(String args) {
		String queryString = createSimpleQueryString(args);
		HttpGet request = new HttpGet(
				GoodRxService.DRUG_SEARCH_SERVICE_ENDPOINT + queryString);
		return executeHttpRequest(request);
	}

	@Override
	public String getLowPrice(String args) {
		String queryString = createPairBasedQueryString(args);
		HttpGet request = new HttpGet(GoodRxService.LOW_PRICE_SERVICE_ENDPOINT
				+ queryString);
		return executeHttpRequest(request);
	}
	
	
	

	@Override
	public GoodRxResponseDTO search(String args) throws ConsumerServiceException {
		String response = drugSearch(args);
		GoodRxResponseDTO goodRxResp = null;
			try {
				goodRxResp = JacksonUtils.getJacksonObjectReaderForJavaType(GoodRxResponseDTO.class).readValue(response);
			} catch (IOException e) {
				throw new ConsumerServiceException("Exception occurred while converting drugsearch json to GoodRxResponseDTO, response=" + response + "\n" + e.getMessage());
			}
		return goodRxResp;
	}
	
	/**
	 * Right now only supporting name based details
	 */
	@Override
	public GoodRxResponseDTO details(String args) {
		String response = comparePrice("name=" + args);
		GoodRxResponseDTO goodRxResp = null;
		try{
			goodRxResp = JacksonUtils.getJacksonObjectReaderForJavaType(GoodRxResponseDTO.class).readValue(response);
		}catch(Exception ex){
			LOGGER.debug("Requested drug not found");
		}
		
		return goodRxResp;
	}

	public String executeHttpRequest(HttpGet request) {
		HttpResponse response;
		StringBuffer sb = new StringBuffer();
		HttpClient client = new DefaultHttpClient();
		BufferedReader rd = null;
		try {
			response = client.execute(request);
			rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}

		} catch (ClientProtocolException e) {
			LOGGER.error("Exception in executeHttpRequest : ", e);
		} catch (IOException e) {
			LOGGER.error("Exception in executeHttpRequest : ", e);
		}  finally {
			IOUtils.closeQuietly(rd);
 		}

		return sb.toString();
	}

	/**
	 * append API key to queryString
	 * 
	 * @param queryStr
	 */
	public void appendAPIKey(StringBuffer queryStr) {
		if (queryStr != null) {
			if (queryStr.length() > 1) {
				queryStr.append("&");
			}
			queryStr.append("api_key=");
			queryStr.append(GoodRxService.API_KEY);
		}
	}

	/**
	 * Creates a query string where input is like paramname=valuestring
	 * 
	 * @param args
	 * @return
	 */
	public String createPairBasedQueryString(String args) {
		// break key val pairs
		String[] keyVal = GoodRxUtil.parseArgs(args);
		if (keyVal == null) {
			return null;
		}
		StringBuffer queryString = new StringBuffer();
		boolean first = true;
		String[] keyvalpair;
		for (String pair : keyVal) {
			keyvalpair = GoodRxUtil.parseKeyVal(pair);
			if (keyvalpair != null) {
				if (first) {
					first = false;
				} else {
					queryString.append("&");
				}
				queryString.append(keyvalpair[0]);
				queryString.append("=");
				queryString.append(keyvalpair[1]);
			} else {
				LOGGER.error("INVALID args: " + pair);
			}
		}

		// append api key
		appendAPIKey(queryString);

		return appendSignature(queryString.toString());
	}

	/**
	 * Create a query string where input is just a string for example input for
	 * drug-search api. Further we append signature and other apikey
	 * 
	 * @return
	 */
	public String createSimpleQueryString(String args) {
		StringBuffer queryString = new StringBuffer();
		if (args == null || args.isEmpty()) {
			return null;
		}
		/**
		 * add input string as query
		 */
		queryString.append("query=");
		queryString.append(args);

		// append api key
		appendAPIKey(queryString);

		return appendSignature(queryString.toString());
		//return queryString.toString();
	}

	public String appendSignature(String query) {
		StringBuffer queryString = new StringBuffer();
		queryString.append(query);
		// create signature
		String signature = GoodRxUtil.createSignature(query);

		queryString.append("&");
		queryString.append("sig");
		queryString.append("=");
		queryString.append(signature);
		return queryString.toString();
	}
	

}
