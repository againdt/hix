<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>



<!-- end -->
<style type="text/css">


	.accordion-inner  {
		padding:10px 20px 0 20px;
	}

	.accordion-inner  h3 {
		margin-bottom:15px;
		color:#ccc;
		border-bottom:solid 1px #e1e3e3;	
	}
	.accordion-inner #sliderEmp, .accordion-inner #sliderDep{
		width:95%;
		margin:50px 20px 0px 20px;
	}
</style>

  

  
<div class="gutter10">
    
        <div class="row-fluid" id="titlebar">
       		<div class="gutter10" >
               <div class="span3">
                 <div class="subnav">
                     <ul class="nav nav-pills">
            
                      <li class="dropdown">
                        <a href="<c:url value="/shop/employer/plandisplay/coveragedates" />">Back</a>
                     </li>
                     </ul>
                 </div>
               </div>
          <div  class="span9">
           <h3 class="pull-left">Define a Contribution Level Towards Premiums</h3>
           
           </div>
		</div>
     </div><!--titlebar-->
     
    
    <div class="row-fluid">
     <div class="span3" id="sidebar">
     
    	 <div class="gutter10">
        	<p> 
Select the amount of money (say $300/month/employee) that you wish to contribute for each employee in your company. Your employees will there-after choose the plan that they want to. You will have little involvement in their coverage selection process thereafter. </p>
     	 </div>
      
       </div><!--sidebar-->      
       <div class="span9 columns pull-right" id="rightpanel">
     
        
        <!--PLANS-->


			<form class="form" id="cofrm" name="cofrm" 	action="<c:url value="/shop/employer/plandisplay/coverageoptions" />"	method="POST">
			<df:csrfToken/>

                        
			<div class="accordion" id="accordion2">
					<div class="accordion-group">
						<div class="controls gutter10">
							<label class="accordion-heading radio"> 
							<input type="radio" name="coverageType" id="coverageType" class="accordion-toggle" data-toggle="collapse"
								data-parent="#accordion2" data-target="#collapseOne" value="emp_defined_contribution"
								<c:if test="${empOrder.coverageType =='DEF_CONTRIB'}"> checked </c:if> />
								<strong> Defined Contribution Option </strong>
							</label>
						</div>
						<div id="collapseOne" class="accordion-body collapse in">
							<div class="accordion-inner">
								<p>Select this option if you wish to contribute the same
									amount of money (say $300/month/employee) for each employee in
									your company. Your employees will there-after choose the plan
									that they want to. You will have little involvement in this
									process thereafter.</p>

								<!-- inner accordian -->
								<div class="accordion" id="accordion3">
									<div class="accordion-group">
										<div class="controls gutter10">
											<label class="accordion-heading radio"> 
												<input type="radio" name="edcCoverDependents"id="edcCoverDependentsY" class="accordion-toggle form-inline" data-toggle="collapse"
												data-parent="#accordion3" data-target="#collapseOneA" value="y"
												<c:if test="${empOrder.coverageDept !='0.0' && empOrder.coverageType =='DEF_CONTRIB'}"> checked </c:if> />

												<strong> Cover Employees and Dependents</strong>
											</label>
										</div>
										<div id="collapseOneA" class="accordion-body collapse">
											<div class="accordion-inner">

												<p>Enter the monthly amount you can contribute</p>

												<div class="control-group span6">
													<label for="firstName" class="required control-label" for="fileInput">
														<div class="controls">
															&#36; <input type="text" name="edcEmpPremium" id="edcEmpPremium" class="input-mini" maxlength="5"
																<c:if test="${empOrder.coverageDept !='0.0' && empOrder.coverageType =='DEF_CONTRIB'}"> value="${empOrder.coverageEmployee}" </c:if> />
															<span>towards employee&#39;s premium</span>
															<div id="edcEmpPremium_error" class="help-inline"></div>
														</div> <!-- end of controls-->
													</label>
													<!-- end of label -->
												</div>
												<!-- end of control-group -->
												<div class="control-group span6">
													<label for="firstName" class="required control-label" for="fileInput">
														<div class="controls">
															&#36; <input type="text" name="edcDependentPremium" id="edcDependentPremium" class="input-mini" maxlength="5"
																<c:if test="${empOrder.coverageDept !='0.0' && empOrder.coverageType =='DEF_CONTRIB'}">  value="${empOrder.coverageDept}"  </c:if> />
															<span>towards dependent&#39;s premium</span>
															<div id="edcDependentPremium_error" class="help-inline"></div>
														</div> <!-- end of controls-->
													</label>
													<!-- end of label -->
												</div>
												<!-- end of control-group -->
											</div>
										</div>
									</div>


									<div class="accordion-group">
										<div class="controls gutter10">
											<label class="accordion-heading radio"> 
												<input type="radio" name="edcCoverDependents" id="edcCoverDependentsN" class="accordion-toggle"
												data-toggle="collapse" data-parent="#accordion3" data-target="#collapseOneB" value="n"
												<c:if test="${empOrder.coverageDept =='0.0' && empOrder.coverageType =='DEF_CONTRIB'}"> checked </c:if> />
												<strong>Cover Employees Only</strong>
											</label>
										</div>
										<div id="collapseOneB" class="accordion-body collapse">
											<div class="accordion-inner">
												<p>Enter the monthly amount you can contribute</p>
												<div class="control-group span6">
													<label for="firstName" class="required control-label" for="fileInput">
														<div class="controls">
															&#36; <input type="text" name="edcEmpPremiumOnly" id="edcEmpPremiumOnly" maxlength="5" class="input-mini"
																<c:if test="${empOrder.coverageDept =='0.0' && empOrder.coverageType =='DEF_CONTRIB'}"> value="${empOrder.coverageEmployee}" </c:if> />
															<span>towards employee&#39;s premium</span>
															<div id="edcEmpPremiumOnly_error" class="help-inline"></div>
														</div> <!-- end of controls-->
													</label>
													<!-- end of label -->
												</div>
												<!-- end of control-group -->
											</div>
										</div>
									</div>
								</div>
								<!-- end inner accordian -->
							</div>
						</div>
					</div>

					<div class="accordion-group">
						<div class="controls gutter10">
							<label class="accordion-heading radio"> 
								<input type="radio" name="coverageType" id="coverageType" class="accordion-toggle" data-toggle="collapse"
								data-parent="#accordion2" data-target="#collapseTwo" value="defined_benefits"
								<c:if test="${empOrder.coverageType =='DEF_BEN'}"> checked </c:if> />
								<strong>Defined Benefits</strong>
							</label>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
							<div class="accordion-inner">
								<p>Select this option if you wish to play an active role in
									helping your employees select health benefits. With this
									option, you will contribute a percentage of your employee's
									premiums, which may vary by employee. You will also select a
									set of health plans for your employees to choose from.</p>
								<div class="accordion" id="accordion4">
									<div class="accordion-group">
										<div class="controls gutter10">
											<label class="accordion-heading radio"> 
												<input type="radio" name="dbCoverDependents" id="dbCoverDependentsY" value="y" class="accordion-toggle form-inline" data-toggle="collapse"
												data-parent="#accordion4" data-target="#collapseTwoA"
												<c:if test="${empOrder.coverageDept !='0.0' && empOrder.coverageType =='DEF_BEN'}"> checked </c:if> />
												<strong> Cover Employees and Dependents</strong>
											</label>
										</div>
										<div id="collapseTwoA" class="accordion-body collapse">
											<div class="accordion-inner">
												<p>Enter the percentage of monthly premium you can
													contribute</p>
												<div class="control-group span6">
													<label for="firstName" class="required control-label" for="fileInput">
														<div class="controls">
															<input type="text" name="dbEmpPremium" id="dbEmpPremium" class="input-mini" maxlength="5"
																<c:if test="${empOrder.coverageDept !='0.0' && empOrder.coverageType =='DEF_BEN'}"> value="${empOrder.coverageEmployee}" </c:if>/> 
															<span>&#37; employee&#39;s premium </span>
															<div id="dbEmpPremium_error" class="help-inline"></div>
														</div> <!-- end of controls-->
													</label>
													<!-- end of label -->
												</div>
												<!-- end of control-group -->
												<div class="control-group span6">
													<label for="firstName" class="required control-label" for="fileInput">
														<div class="controls">
															<input type="text" name="dbDependentPremium" id="dbDependentPremium" class="input-mini"
																<c:if test="${empOrder.coverageDept !='0.0' && empOrder.coverageType =='DEF_BEN'}"> value="${empOrder.coverageDept}" </c:if> />
															<span>&#37; dependent&#39;s premium </span>
															<div id="dbDependentPremium_error" class="help-inline"></div>
														</div> <!-- end of controls-->
													</label>
													<!-- end of label -->
												</div>
												<!-- end of control-group -->
											</div>
										</div>
									</div>


									<div class="accordion-group">
										<div class="controls gutter10">
											<label class="accordion-heading radio"> 
											<input type="radio" name="dbCoverDependents" id="dbCoverDependentsN" value="n" class="accordion-toggle"
												data-toggle="collapse" data-parent="#accordion4" data-target="#collapseTwoB"
												<c:if test="${empOrder.coverageDept =='0.0' && empOrder.coverageType =='DEF_BEN'}"> checked </c:if> />
												<strong>Cover Employees Only</strong>
											</label>
										</div>
										<div id="collapseTwoB" class="accordion-body collapse">
											<div class="accordion-inner">
												<p>Enter the percentage of monthly premium you can contribute</p>
												<div class="control-group span6">
													<label for="firstName" class="required control-label"
														for="fileInput">
														<div class="controls">
															<input type="text" name="dbEmpPremiumOnly" id="dbEmpPremiumOnly" class="input-mini" maxlength="5"
																<c:if test="${empOrder.coverageDept =='0.0' && empOrder.coverageType =='DEF_BEN'}"> value="${empOrder.coverageEmployee}" </c:if> /> <span>&#37; employee&#39;s premium </span>
															<div id="dbEmpPremiumOnly_error" class="help-inline"></div>
														</div> <!-- end of controls-->
													</label>
													<!-- end of label -->
												</div>
												<!-- end of control-group -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<!-- end of profile -->

			<div class="form-actions">
				<div class="pull-right">
					<a class="btn" type="submit" onclick="formSubmit();">Save</a> 
					<a class="btn btn-primary" type="submit" onclick="formSubmit();">Next &rarr; Set Preferences</a>
				</div>
			</div>
		</form>

 </div><!--plan details-->
 <!--plan select-->
      </div>
      </div>

    <!--planSelect--> 

<!-- /container --> 


 <script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script> 

<script>
 $.validator.addMethod('validatePositiveNumber',
	    function (value) { 
	        return Number(value) >= 0;
	    }, 'Please Enter a positive number only.');
$.validator.addMethod('validatePercentageValue',
	    function (value) { 
    	    return ( parseInt(value) <= parseInt(100))
		}, 'Please Enter a number in range [0-100] only.');	    

var validator = $("#cofrm").validate({ 
	rules : {
		coverageType : {required : true},
		edcCoverDependents:{required:{depends: function(element){
							return ($('input:radio[name=coverageType]:checked').val() == 'emp_defined_contribution');}}},
		dbCoverDependents:{required:{depends: function(element){ 
							return ($('input:radio[name=coverageType]:checked').val() == 'defined_benefits');}}},
		edcEmpPremium :{ 
						required:{depends: function(element){
									return ($('input:radio[name=coverageType]:checked').val() == 'emp_defined_contribution' && $('input:radio[name=edcCoverDependents]:checked').val() == 'y');}},
						number: true,
						validatePositiveNumber : {depends: function(element){
							return ($('input:radio[name=coverageType]:checked').val() == 'emp_defined_contribution' && $('input:radio[name=edcCoverDependents]:checked').val() == 'y');}}
		},
		edcDependentPremium:{ 
						required:{depends: function(element){
									return ($('input:radio[name=coverageType]:checked').val() == 'emp_defined_contribution' && $('input:radio[name=edcCoverDependents]:checked').val() == 'y');}},
						number: true,
						validatePositiveNumber : {depends: function(element){
							return ($('input:radio[name=coverageType]:checked').val() == 'emp_defined_contribution' && $('input:radio[name=edcCoverDependents]:checked').val() == 'y');}}
		},
		edcEmpPremiumOnly :{ 
						required:{depends: function(element){
									return ($('input:radio[name=coverageType]:checked').val() == 'emp_defined_contribution' && $('input:radio[name=edcCoverDependents]:checked').val() == 'n');}},
						number: true,
						validatePositiveNumber : {depends: function(element){
							return ($('input:radio[name=coverageType]:checked').val() == 'emp_defined_contribution' && $('input:radio[name=edcCoverDependents]:checked').val() == 'n');}}
		},
		dbEmpPremium :{ 
						required:{depends: function(element){
								return ($('input:radio[name=coverageType]:checked').val() == 'defined_benefits' && $('input:radio[name=dbCoverDependents]:checked').val() == 'y');}},
						number: true,
						validatePositiveNumber : {depends: function(element){
							return ($('input:radio[name=coverageType]:checked').val() == 'defined_benefits' && $('input:radio[name=dbCoverDependents]:checked').val() == 'y');}},
						validatePercentageValue : {depends: function(element){
							return ($('input:radio[name=coverageType]:checked').val() == 'defined_benefits' && $('input:radio[name=dbCoverDependents]:checked').val() == 'y');}}
		},
		dbDependentPremium:{ 
						required:{depends: function(element){
								return ($('input:radio[name=coverageType]:checked').val() == 'defined_benefits' && $('input:radio[name=dbCoverDependents]:checked').val() == 'y');}},
						number: true,
						validatePositiveNumber : {depends: function(element){
							return ($('input:radio[name=coverageType]:checked').val() == 'defined_benefits' && $('input:radio[name=dbCoverDependents]:checked').val() == 'y');}},
						validatePercentageValue : {depends: function(element){
							return ($('input:radio[name=coverageType]:checked').val() == 'defined_benefits' && $('input:radio[name=dbCoverDependents]:checked').val() == 'y');}}
		},
		dbEmpPremiumOnly :{ 
						required:{depends: function(element){
								return ($('input:radio[name=coverageType]:checked').val() == 'defined_benefits' && $("input:radio[name=dbCoverDependents]:checked").val() == 'n');}},
						number: true,
						validatePositiveNumber : {depends: function(element){
							return ($('input:radio[name=coverageType]:checked').val() == 'defined_benefits' && $("input:radio[name=dbCoverDependents]:checked").val() == 'n');}},
						validatePercentageValue : {depends: function(element){
							return ($('input:radio[name=coverageType]:checked').val() == 'defined_benefits' && $("input:radio[name=dbCoverDependents]:checked").val() == 'n');}}
		}	
},		
messages : {
		coverageType : { required : "<span><em class='excl'>!</em>Please Select either Coverage Type</span>"},
		edcCoverDependents : { required : "<span><em class='excl'>!</em>Please Select either Defined Contribution Option</span>"},
		dbCoverDependents : { required : "<span><em class='excl'>!</em>Please Select either Defined Benefit Option</span>"},
		edcEmpPremium: { required : "<span><em class='excl'>!</em>Please Enter Employee Premium</span>",
						 number : "<span><em class='excl'>!</em>Please Enter Numbers Only</span>",
						 validatePositiveNumber : "<span><em class='excl'>!</em>Please Enter Positive Numbers Only</span>"},
		edcDependentPremium: { required : "<span><em class='excl'>!</em>Please Enter Dependent Premium</span>",
							   number : "<span><em class='excl'>!</em>Please Enter Numbers Only</span>",
							   validatePositiveNumber : "<span><em class='excl'>!</em>Please Enter Positive Numbers Only</span>"},
		edcEmpPremiumOnly: { required : "<span><em class='excl'>!</em>Please Enter Employee Only Premium</span>",
							 number : "<span><em class='excl'>!</em>Please Enter Numbers Only</span>",
							 validatePositiveNumber : "<span><em class='excl'>!</em>Please Enter Positive Numbers Only</span>"},
		dbEmpPremium: { required : "<span><em class='excl'>!</em>Please Enter Employee Premium</span>",
						number : "<span><em class='excl'>!</em>Please Enter Numbers Only</span>",
						validatePositiveNumber : "<span><em class='excl'>!</em>Please Enter Positive Numbers Only</span>",
						validatePercentageValue : "<span><em class='excl'>!</em>Please Enter Numbers in Range [0-100] Only</span>"},
		dbDependentPremium: { required : "<span><em class='excl'>!</em>Please Enter Dependent Premium</span>",
							  number : "<span><em class='excl'>!</em>Please Enter Numbers Only</span>",
							  validatePositiveNumber : "<span><em class='excl'>!</em>Please Enter Positive Numbers Only</span>",
							  validatePercentageValue : "<span><em class='excl'>!</em>Please Enter Numbers in Range [0-100] Only</span>"},
		dbEmpPremiumOnly: { required : "<span><em class='excl'>!</em>Please Enter Employee Only Premium</span>",
							number : "<span><em class='excl'>!</em>Please Enter Numbers Only</span>",
							validatePositiveNumber : "<span><em class='excl'>!</em>Please Enter Positive Numbers Only</span>",
							validatePercentageValue : "<span><em class='excl'>!</em>Please Enter Numbers in the Range [0-100] Only</span>"}
},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	} 
}); 

function formSubmit(){
	
	$('#cofrm').submit();
}

	$(document).ready(function(){
		 var ctype = "${empOrder.coverageType}";
		var premDept ="${empOrder.coverageDept}";
		
		if(ctype == 'DEF_CONTRIB'){
			$('#collapseOne').addClass("in");
			
			if(premDept != '0.0')
				$('#collapseOneA').addClass("in");
			else if(premDept == '0.0')
				$('#collapseOneB').addClass("in");
		}
		else if(ctype == 'DEF_BEN'){
			$('#collapseTwo').addClass("in");
			$('#collapseOne').removeClass("in");
			
			if(premDept != '0.0')
				$('#collapseTwoA').addClass("in");
			else if(premDept == '0.0')
				$('#collapseTwoB').addClass("in");
		}
		else $('#collapseOne').removeClass("in"); 
	});

</script>