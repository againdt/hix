package com.getinsured.hix.planmgmt.mapper;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.planmgmt.util.ServiceAreaDetails;
import com.getinsured.hix.planmgmt.util.ZipCounty;

public final class ServiceAreaMapper {
	
	private static final String PLAN_NAME_AND_NUMBER = "Plan Name and Number";
	private static final String START_DATE = "Start Date";
	private static final String END_DATE = "End Date";
	private static final String SERVICE_AREA_ID = "Service Area Id";
	private static final String ZIP_CODE = "ZIPCODES";
	private static final String COUNTY = "County";
	private static final int THREE = 3;
	private ServiceAreaMapper(){}
	
	public static  List<String[]> prepareServiceAreaListToExcel(ServiceAreaDetails serviceAreaDetails){

		List<String[]> serviceAreaDetailsList = new ArrayList<String[]>();
		String[] nameAndNumber = new String[THREE];
		nameAndNumber[0] = PLAN_NAME_AND_NUMBER;
		String[] startDate = new String[2];
		startDate[0]= START_DATE;
		String[] endDate = new String[2];
		endDate[0]= END_DATE;
		String[] serviceAreaId = new String[2];
		serviceAreaId[0] = SERVICE_AREA_ID;
		String[] zipCodeCountyHeader = new String[2];
		zipCodeCountyHeader[0] = ZIP_CODE;
		zipCodeCountyHeader[1] = COUNTY;
		
		nameAndNumber[1] = serviceAreaDetails.getName();
		nameAndNumber[2] = serviceAreaDetails.getIssuerPlanNumber();
		startDate[1] = serviceAreaDetails.getStartDate();
		endDate[1] = serviceAreaDetails.getEndDate();
		serviceAreaId[1] = serviceAreaDetails.getSerfServiceAreaId();
		serviceAreaDetailsList.add(nameAndNumber);
		serviceAreaDetailsList.add(startDate);
		serviceAreaDetailsList.add(endDate);
		serviceAreaDetailsList.add(serviceAreaId);
		serviceAreaDetailsList.add(zipCodeCountyHeader);
		List<ZipCounty> zipCountyList = serviceAreaDetails.getZipCounty();
		for(ZipCounty zipCounty: zipCountyList){
			String[] zipCodeCountyData = new String[2];
			zipCodeCountyData[0] = zipCounty.getZipCode();
			zipCodeCountyData[1] = zipCounty.getCounty();
			serviceAreaDetailsList.add(zipCodeCountyData);
		}
		return serviceAreaDetailsList;
	}

}
