package com.getinsured.ssap.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.ssap.model.ComparisonResult;
import com.getinsured.ssap.model.eligibility.EligibilityResponse;
import com.getinsured.ssap.model.eligibility.ResponseStatus;
import com.getinsured.ssap.model.eligibility.RunMode;
import com.getinsured.ssap.model.financial.IfsvResponse;
import com.getinsured.ssap.model.financial.nmec.NonEsiMecVerificationResponse;
import com.getinsured.ssap.model.hub.vlp.ApplicantVerificationStatus;
import com.getinsured.ssap.repository.ApplicantRepository;
import com.getinsured.ssap.util.JsonUtil;
import com.getinsured.ssap.util.SsapHelper;

/**
 * Class responsible for finalization of the SSAP.
 *
 * @author Yevgen Golubenko
 * @since 2019-09-20
 */
@Service("applicationFinalization")
public class ApplicationFinalizationImpl implements ApplicationFinalization {
  private static final Logger log = LoggerFactory.getLogger(ApplicationFinalizationImpl.class);

  private final EligibilityEngineIntegrationService eligibilityEngineIntegrationService;
  private final SsapComparisonService ssapComparisonService;
  private final IfsvService ifsvService;
  private final NonEsiMecService nmecService;
  private final SsapApplicationRepository ssapApplicationRepository;
  private final ApplicantRepository applicantRepository;
  private final OutboundAccountTransferService outboundAccountTransferService;
  private final RestTemplate restTemplate;
  private final GhixRestTemplate ghixRestTemplate;

  @Autowired
  public ApplicationFinalizationImpl(final EligibilityEngineIntegrationService eligibilityEngineIntegrationService,
                                     final SsapComparisonService ssapComparisonService,
                                     final IfsvService ifsvService,
                                     final NonEsiMecService nmecService,
                                     final SsapApplicationRepository ssapApplicationRepository,
                                     final ApplicantRepository applicantRepository,
                                     final OutboundAccountTransferService outboundAccountTransferService,
                                     final RestTemplate restTemplate,
                                     final GhixRestTemplate ghixRestTemplate) {
    this.eligibilityEngineIntegrationService = eligibilityEngineIntegrationService;
    this.ssapComparisonService = ssapComparisonService;
    this.ifsvService = ifsvService;
    this.nmecService = nmecService;
    this.ssapApplicationRepository = ssapApplicationRepository;
    this.applicantRepository = applicantRepository;
    this.outboundAccountTransferService = outboundAccountTransferService;
    this.restTemplate = restTemplate;
    this.ghixRestTemplate = ghixRestTemplate;
  }

  /**
   * {@inheritDoc}
   */
  @Async
  @Override
  @Transactional
  public void finalizeApplication(SingleStreamlinedApplication application, Long parentSsapApplicationId, String specialEnrollmentEvent,
                                  String specialEnrollmentDate, boolean csrOverride, AccountUser currentUser) {
    log.info("Finalizing application: applicationId: {}, application guid: {}, application type: {}, " +
        "CSR Override: {} special enrollment event: {}, special enrollment date: {}",
      application.getSsapApplicationId(), application.getApplicationGuid(), application.getApplicationType(),
      csrOverride, specialEnrollmentEvent, specialEnrollmentDate);

    final EligibilityResponse eligibilityResponse = eligibilityEngineIntegrationService.invokeEligibilityEngine(application, specialEnrollmentEvent, specialEnrollmentDate, RunMode.ONLINE);

    List<ComparisonResult> comparisonResults = new ArrayList<>();

    if (eligibilityResponse.getStatus() == ResponseStatus.success) {

      verifyNonQhpMembers(application, eligibilityResponse);

      // Check to see if current application is a clone of the previous one, once verifications are executed,
      // Need to set some kind of flag that even though parent ID does exist, we don't need to execute comparison logic again.
      if (parentSsapApplicationId != null && parentSsapApplicationId > 0L) {
        com.getinsured.iex.ssap.model.SsapApplication parentSsapApplication = ssapApplicationRepository.findOne(parentSsapApplicationId);
        if (parentSsapApplication != null) {
          log.info("We have parent SSAP Application ID: {}, need to call comparison API", parentSsapApplication.getId());
          SingleStreamlinedApplication parentApplication = JsonUtil.parseApplicationDataJson(parentSsapApplication);
          if (parentApplication != null) {
            comparisonResults = ssapComparisonService.compare(application, parentApplication);
            log.info("Got comparison results between {} and {} => {}", application.getSsapApplicationId(), parentApplication.getSsapApplicationId(), comparisonResults);
          } else {
            log.warn("Unable to retrieve parent application by id: {}", parentSsapApplicationId);
          }

          log.info("call Change automation flow for App Id {}", application.getSsapApplicationId());
          callChangeAutomation(currentUser, Long.parseLong(application.getSsapApplicationId()));
        }
      }

      if (!DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase("ID")) {

        if (eligibilityResponse.getMembers() != null && !eligibilityResponse.getMembers().isEmpty()) {
          boolean anyoneIsMedicaidChipEligible = eligibilityResponse.getMembers().stream().anyMatch(hhmr -> hhmr.isMedicaid() || hhmr.isChip());

          if (anyoneIsMedicaidChipEligible) {
            log.info("Eligibility engine responded that some members are Medicaid/CHIP eligible, invoking outbound account transfer to state.");
            outboundAccountTransferService.invokeOutboundAccountTransfer(SsapHelper.getApplicationId(application));
          }
        }

        // 1. IFSV should not be invoked for ID
        // 2. If we have no comparison results (meaning that there was no parent ssap id)
        // 3. If we did get comparison results and IFSV was included in result
        // only then run IFSV verifications.
        // TODO: Suresh, you can include the similar block in for verifications
        if (comparisonResults.isEmpty() || comparisonResults.contains(ComparisonResult.IFSV)) {
          log.info("Eligibility responded with successful status, calling IFSV service ");
          final IfsvResponse ifsvResponse = ifsvService.invoke(SsapHelper.getApplicationId(application));
          log.info("IFSV Response: {}", ifsvResponse);
        }

        if (comparisonResults.isEmpty() || comparisonResults.contains(ComparisonResult.NON_ESI_MEC)) {
          log.info("calling NON_ESI_MEC service ");
          final NonEsiMecVerificationResponse nmecResponse = nmecService.invoke(SsapHelper.getApplicationId(application));
          log.info("NON_ESI_MEC Response: {}", nmecResponse);
        }
      }

      try {
        if (!comparisonResults.isEmpty() && !comparisonResults.contains(ComparisonResult.NONE)) {
          log.info("Persist compare Results & Invoke SSAP Integration response: {}", comparisonResults);
          persistCompareResult(application, comparisonResults);
        }

        final String integrationResponse = restTemplate.postForObject(GhixEndPoints.SsapIntegrationEndpoints.SSAP_INTEGRATION_URL,
          application.getSsapApplicationId(), String.class);
        log.info("SSAP Integration response: {}", integrationResponse);
      } catch (Exception e) {
        log.error("Error while triggering SSAP integration: {}", e.getMessage());
        log.debug("Exception details", e);
      }
    } else {
      log.error("Eligibility response returned ERRORS:");
      eligibilityResponse.getErrors().forEach((k, v) -> {
        log.error("Eligibility response ERROR: {} => {}", k, v);
      });
    }

    log.info("Finalized application.");
  }

  /**
   * Looks at each household member's eligibility results, and if they are MEDICARE/CHIP eligible and have
   * not decided to shop for QHP, we mark all verifications as VERIFIED, because in case of MEDICARE/CHIP
   * eligibility, it's the state exchange that will do verifications for them.
   *
   * @param application {@link SingleStreamlinedApplication} object.
   */
  private void verifyNonQhpMembers(final SingleStreamlinedApplication application, final EligibilityResponse eligibilityResponse) {
    final List<SsapApplicant> ssapApplicants = applicantRepository.findBySsapApplicationId(SsapHelper.getApplicationId(application));

    final BigDecimal verificationId = new BigDecimal(0);
    final String verificationValue = ApplicantVerificationStatus.VERIFIED.getValue();

    // HIX-118046: If anyone is CHIP/Medicaid, we don't run any verifications on our side, we just set everything to VERIFIED
    eligibilityResponse.getMembers()
      .stream()
      .filter(eligibilityMemberResponse -> eligibilityMemberResponse.isMedicaid() || eligibilityMemberResponse.isChip())
      .forEach(householdMember -> {

        final SsapApplicant applicant = ssapApplicants.stream().filter(a ->
          a.getId() == householdMember.getApplicantId()).findFirst().orElse(null);

        boolean seeksQhp = false;

        if (applicant != null) {
          HouseholdMember hhm = application.getTaxHousehold().get(0).getHouseholdMember()
            .stream().filter(hh ->
              hh.getApplicantGuid().equals(applicant.getApplicantGuid())
            ).findFirst().orElse(null);
          seeksQhp = hhm != null && hhm.isSeeksQhp();
        }

        if (applicant != null && !seeksQhp) {
          log.info("Applicant id: {}, name: {} {}, is medicaid/chip (and not seeking QHP), verifying everything automatically",
            applicant.getId(), applicant.getFirstName(), applicant.getLastName());

          applicant.setVlpVerificationStatus(verificationValue);
          applicant.setVlpVerificationVid(verificationId);

          applicant.setIncomeVerificationStatus(verificationValue);
          applicant.setIncomeVerificationVid(verificationId);

          applicant.setSsnVerificationStatus(verificationValue);
          applicant.setSsnVerificationVid(verificationId);

          applicant.setMecVerificationStatus(verificationValue);
          applicant.setMecVerificationVid(verificationId);

          applicant.setDeathVerificationVid(verificationId);
          applicant.setDeathStatus(verificationValue);

          applicant.setNativeAmericanVerificationStatus(verificationValue);

          applicant.setNonEsiMecVerificationStatus(verificationValue);
          applicant.setNonEsiMecVerificationVid(verificationId.longValue());

          applicant.setIncarcerationVid(verificationId);
          applicant.setIncarcerationStatus(verificationValue);

          applicant.setCitizenshipImmigrationStatus(verificationValue);
          applicant.setCitizenshipImmigrationVid(verificationId);

          applicant.setResidencyStatus(verificationValue);
          applicant.setResidencyVid(verificationId);

          applicantRepository.save(applicant);
        }
      });
  }

  private void persistCompareResult(SingleStreamlinedApplication application, List<ComparisonResult> comparisonResults) {
    com.getinsured.iex.ssap.model.SsapApplication currentSsapApplication = ssapApplicationRepository.findOne(SsapHelper.getApplicationId(application));
    final SingleStreamlinedApplication applicationData = JsonUtil.parseApplicationDataJson(currentSsapApplication);
    if (applicationData != null) {
      final List<String> results = new ArrayList<>();
      comparisonResults.forEach(result -> results.add(result.name()));
      log.info("Verifications to be saved for further processing: {}", String.join(",", results));
      applicationData.getTaxHousehold().get(0).setRequiredVerifications(results);
      JsonUtil.setApplicationData(currentSsapApplication, applicationData);
      ssapApplicationRepository.save(currentSsapApplication);
    }
  }


  /**
   * Invoke Change automation flow for Application to Identify events and automate changes in enrollment (if any).
   *
   * @param user          {@link AccountUser} object.
   * @param applicationId SSAP id.
   */
  private void callChangeAutomation(final AccountUser user, final Long applicationId) {
    try {
      String url = GhixEndPoints.EligibilityEndPoints.APP_CHANGE_AUTOMATION + "/" + applicationId;
      ghixRestTemplate.exchange(
        url,
        user.getUsername(),
        HttpMethod.POST,
        MediaType.APPLICATION_JSON,
        String.class,
        null);
    } catch (RestClientException e) {
      log.error("REST Exception occurred while calling application change automation flow", e);
    } catch (Exception ex) {
      log.error("Exception occurred while calling application change automation flow: {}", ex.getMessage());
    }
  }
}

