<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>



    <link rel="stylesheet" type="text/css"  href="<c:url value="/resources/css/datepicker.css"/>" />
<!--old -->    
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>





<style type="text/css">

</style>
<div class="gutter10" id="main">
	<!-- <div class="container">	 -->

  <div class="row-fluid" id="titlebar">
       		<!-- <div class="gutter10" >
               <div class="span3">
                 <div class="subnav">
                     <ul class="nav nav-pills">
            
                      <li class="">
                        <a href="#">Back </a>
                     </li>
                     </ul>
                 </div>
               </div> -->
          <div  class="span9">
           <h3 class="pull-left">Select a Coverage Effective Date</h3>
           </div>
		</div>
     </div><!--titlebar-->

    <div class="row-fluid">
     <div class="span3" id="sidebar">
    	 <div class="gutter10">
        	<p>Please set the coverage start date, which should be at least 60 days from today to allow sufficient time for the enrollment process.
A notification will be sent to all of the employees on your roster before the start of the enrollment period.</p>
     	 </div>
       </div><!--sidebar-->      
       <form class="form" id="cdfrm" name="cdfrm" action="<c:url value="/shop/employer/plandisplay/coveragedates" />" method="POST">
      <df:csrfToken/>
       <div class="span9 columns pull-right" id="rightpanel">
             <div class="gutter10">
             
                        <div class="input-append date gutter10" id="startDate" data-date="${coverageDate}" data-date-format="mm/dd/yyyy">
                       		 <input class="span2" size="16" type="text" <c:if test="${coverageDate !=''}" > value="${coverageDate}" </c:if> name="startDate" id="startDate"  readonly="readonly" >
                       		 <span class="add-on"><i class="icon-calendar"></i></span>
            			</div>
						<p><div id="startDate_error"  class="help-inline"></div></p>
                        
                    <div class="form-actions">
                    	<div class="pull-right">
                       	 <a class="btn" type="submit" onclick="formSubmit();">Save</a>
                         <a class="btn btn-primary" type="submit" onclick="formSubmit();">Next &rarr; Select Contribution </a>
                    	 </div>
        			</div>
              </div><!--gutter10-->
       </div> <!--span9--> 
	</form>
   <!--  </div>container -->
    
  </div> <!--row-fluid--> 
   
 </div>
   

<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker.js" />"></script>


<script type="text/javascript">


$(document).ready(function(){
	
	$('#startDate').datepicker();
	
	$('.btn').tooltip(
		{
		placement:'top'
		});

});


function formSubmit(){
	
	$('#cdfrm').submit();
}

/* form validator*/	
$.validator.addMethod("validateStartDate", function(value, element) {
	
	var toDate  = new Date().getTime(); //current date in Milliseconds
	var stDate = $('input:text[name=startDate]').val(); //selected date val
	var days60 = (24*3600*60*1000); //60 days in Milliseconds
	return (new Date(stDate).getTime() > (toDate+days60));
}); 
var validator = $("#cdfrm").validate({ 
	rules : {
		startDate : {required : true, validateStartDate:true}
	},
	messages : {
		startDate : { required : "<span><em class='excl'>!</em> Please Select Coverage Start Date</span>",
			validateStartDate : "<span><em class='excl'>!</em> Please Select Valid Date (MINIMUM 60 DAYS FROM TODAY'S DATE)</span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	} 
});	
	
</script>