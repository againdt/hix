package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for last Name
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 *
 */
public class LastNameValidator extends Validator{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LastNameValidator.class);
	
	/** 
	 * Validations for last name
	 * String between 1-50 characters
	 * 
	 * Required : Yes
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String lastName = null;
		
		//Value is mandatory
		if(value == null){
			throw new InputDataValidationException(VLPServiceConstants.LAST_NAME_REQUIRED_ERROR_MESSAGE);
		}
		else{
			lastName = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(lastName.length() < VLPServiceConstants.LAST_NAME_MIN_LEN
				|| lastName.length() > VLPServiceConstants.LAST_NAME_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.LAST_NAME_LEN_ERROR_MESSAGE);
		}

	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
