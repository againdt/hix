package com.getinsured.iex.im.wso2;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.im.wso2.carbon.um.ws.service.AuthenticateResponse;
import com.getinsured.iex.im.wso2.wrappers.AuthenticateWrapper;
import com.getinsured.iex.im.wso2.wrappers.AuthenticationResponseWrapper;

public class AuthenticateServiceHandler extends AbstractServiceHandler {

	private AuthenticationResponseWrapper responseWrapper;

	@Override
	public Object getRequestpayLoad() throws HubServiceException,
			HubMappingException {
		JSONParser parser = new JSONParser();
		try {
			JSONObject authJsonrequest = (JSONObject) parser.parse(this.getJsonInput());
			String userName = (String) authJsonrequest.get("userName");
			String password = (String) authJsonrequest.get("password");
			AuthenticateWrapper authRequest = new AuthenticateWrapper();
			authRequest.setCredential(password);
			authRequest.setUserName(userName);
			return authRequest.getSystemRepresentation();
		} catch (ParseException e) {
			throw new HubServiceException("Failed to construct the payload from request input");
		}
	}

	@Override
	public String handleResponse(Object response) throws HubServiceException {
		if(!(response instanceof AuthenticateResponse)){
			throw new HubServiceException("Invalid response received expected "+AuthenticateResponse.class.getName()+" Received:"+response.getClass().getName());
		}
		responseWrapper= new AuthenticationResponseWrapper((AuthenticateResponse) response);
		boolean success = responseWrapper.isAuthenticated();
		return Boolean.toString(success);
	}

	@Override
	public RequestStatus getRequestStatus() {
		if(this.responseWrapper == null){
			return RequestStatus.FAILED;
		}
		return RequestStatus.SUCCESS;
	}

	

}
