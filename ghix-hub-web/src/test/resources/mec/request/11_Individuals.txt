{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload": {
    "taxHousehold": [
      {
        "householdMember": [
          {
            "dateOfBirth": "Apr 04, 1950 12:00:00 AM",
            "name": {
              "firstName": "Don",
              "middleName": "C",
              "lastName": "Johnson",
              "personNameSuffixText": ""
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "897581061"
            },
            "gender": "F",
            "householdContact": {
              "homeAddress": {
                "state": "AL"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/01/2013",
              "insurancePolicyExpirationDate": "12/31/2013"
            }
          },
          {
            "dateOfBirth": "Apr 04, 1950 12:00:00 AM",
            "name": {
              "firstName": "Don",
              "middleName": "C",
              "lastName": "Johnson",
              "personNameSuffixText": ""
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "578813959"
            },
            "gender": "F",
            "householdContact": {
              "homeAddress": {
                "state": "CA"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/01/2013",
              "insurancePolicyExpirationDate": "12/31/2013"
            }
          },
          {
            "dateOfBirth": "Apr 04, 1950 12:00:00 AM",
            "name": {
              "firstName": "Don",
              "middleName": "C",
              "lastName": "Johnson",
              "personNameSuffixText": ""
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "599785474"
            },
            "gender": "F",
            "householdContact": {
              "homeAddress": {
                "state": "AL"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/01/2013",
              "insurancePolicyExpirationDate": "12/31/2013"
            }
          },
          {
            "dateOfBirth": "Apr 04, 1950 12:00:00 AM",
            "name": {
              "firstName": "Don",
              "middleName": "C",
              "lastName": "Johnson",
              "personNameSuffixText": ""
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "150994958"
            },
            "gender": "F",
            "householdContact": {
              "homeAddress": {
                "state": "CA"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/01/2013",
              "insurancePolicyExpirationDate": "12/31/2013"
            }
          },
          {
            "dateOfBirth": "Apr 04, 1950 12:00:00 AM",
            "name": {
              "firstName": "Don",
              "middleName": "C",
              "lastName": "Johnson",
              "personNameSuffixText": ""
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "503316496"
            },
            "gender": "F",
            "householdContact": {
              "homeAddress": {
                "state": "AL"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/01/2013",
              "insurancePolicyExpirationDate": "12/31/2013"
            }
          },
          {
            "dateOfBirth": "Apr 04, 1950 12:00:00 AM",
            "name": {
              "firstName": "Don",
              "middleName": "C",
              "lastName": "Johnson",
              "personNameSuffixText": ""
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "603979795"
            },
            "gender": "F",
            "householdContact": {
              "homeAddress": {
                "state": "CA"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/01/2013",
              "insurancePolicyExpirationDate": "12/31/2013"
            }
          },
          {
            "dateOfBirth": "Apr 04, 1950 12:00:00 AM",
            "name": {
              "firstName": "Don",
              "middleName": "C",
              "lastName": "Johnson",
              "personNameSuffixText": ""
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "679477268"
            },
            "gender": "F",
            "householdContact": {
              "homeAddress": {
                "state": "AL"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/01/2013",
              "insurancePolicyExpirationDate": "12/31/2013"
            }
          },
          {
            "dateOfBirth": "Apr 04, 1950 12:00:00 AM",
            "name": {
              "firstName": "Don",
              "middleName": "C",
              "lastName": "Johnson",
              "personNameSuffixText": ""
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "248862041"
            },
            "gender": "F",
            "householdContact": {
              "homeAddress": {
                "state": "CA"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/01/2013",
              "insurancePolicyExpirationDate": "12/31/2013"
            }
          },
          {
            "dateOfBirth": "Apr 04, 1950 12:00:00 AM",
            "name": {
              "firstName": "Don",
              "middleName": "C",
              "lastName": "Johnson",
              "personNameSuffixText": ""
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "411041816"
            },
            "gender": "F",
            "householdContact": {
              "homeAddress": {
                "state": "AL"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/01/2013",
              "insurancePolicyExpirationDate": "12/31/2013"
            }
          },
          {
            "dateOfBirth": "Apr 04, 1950 12:00:00 AM",
            "name": {
              "firstName": "Don",
              "middleName": "C",
              "lastName": "Johnson",
              "personNameSuffixText": ""
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "327155739"
            },
            "gender": "F",
            "householdContact": {
              "homeAddress": {
                "state": "CA"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/01/2013",
              "insurancePolicyExpirationDate": "12/31/2013"
            }
          },
          {
            "dateOfBirth": "Apr 04, 1950 12:00:00 AM",
            "name": {
              "firstName": "Don",
              "middleName": "C",
              "lastName": "Johnson",
              "personNameSuffixText": ""
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "327155679"
            },
            "gender": "F",
            "householdContact": {
              "homeAddress": {
                "state": "AL"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/01/2013",
              "insurancePolicyExpirationDate": "12/31/2013"
            }
          }
        ]
      }
    ]
  }
}