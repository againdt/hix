package com.getinsured.ahbx.broker.service;

import com.getinsured.hix.model.DesignateBroker;

/**
 * Encapsulates service layer method calls for Broker Designation. 
 */
public interface DesignateService
{

	/**
	 * Saves designated broker with Pending status when employer/individual sends designate request.
	 * 
	 * @param designateBroker
	 */
	void saveDesignateBroker(DesignateBroker designateBroker);

	/**
	 * Calls the JPA repository and fetches the designateBroker record against the broker Id 
	 * and external employer Id 
	 * 
	 * @param brokerId 		the broker id
	 * @param externalEmployerId 	the external employer id
	 * @return the designateBroker record against the broker Id and external employer Id
	 */
	DesignateBroker findByBrokerIdAndExternalId(int brokerId, String role, int Id);

}
