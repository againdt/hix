<div id="lifeChangeEventMainDIV" class='right_contant_area backgroundWhite'>
<div id="pageContent">


<p class="heading">Select the life change event(s) you are applying for:</p>
<div id="lceList">
	<div><input class="appliedFormat" id="ms" type="checkbox"></input><label>Marital Status</label></div>
	<div><input class="appliedFormat" id="nod" type="checkbox"></input><label>Number Of Dependents</label></div>
	<div><input class="appliedFormat" id="lomec" type="checkbox"></input><label>Loss of minimum essential coverage</label></div>
	<div><input class="appliedFormat" id="incar" type="checkbox"></input><label>Incarceration</label></div>
	<div><input id="fifth" class="appliedFormat" type="checkbox"></input><label>Lawful Presence</label></div>
	<div><input id="sixth" class="appliedFormat" type="checkbox"></input><label>Change in residence/mailing address</label></div>
	<div><input class="appliedFormat" id="income" type="checkbox"></input><label>Income</label></div>
	<div><input id="eighth" class="appliedFormat" type="checkbox"></input><label>Gain Other minimum essential coverage</label></div>
	<div><input class="appliedFormat" id="other" type="checkbox"></input><label>Other</label></div>
</div>

<hr/>

<p class="heading">Individual and account information</p>
<div id="personalInfoDiv">
<div style="padding-bottom: 20px;" ><input id="mpAccount" type="checkbox"><label>Check this box if you do not have a 
marketplace account. If you are a new customer, please fill </label> <label id="labelText">out your name, email address, 
and date of birth below.</label></input></div>
<div id="infoContainer">
<ul class="individualUl">

<li><div class="labelDiv">
<label>
Legal First Name
<span class="asterisk">*</span>
</label>
</div> 
<div class="name-fields">
<input class="filler" id="lfname" type="text"/>
</div>
</li>

<li><div class="labelDiv">
<label>
Middle Name
<span class="asterisk">*</span>
</label>
</div> 
<div class="name-fields">
<input class="filler" id="fmname" type="text"/>
</div>
</li>

<li><div class="labelDiv">
<label>
Last Name
<span class="asterisk">*</span>
</label>
</div> 
<div class="name-fields">
<input class="filler" id="flname" type="text"/>
</div>
</li>

</ul></div>

<div id="infoContainer2">
<ul class="individualUl">

<li><div class="labelDiv">
<label>
Email Address

</label>
</div> 
<div class="name-fields">
<input class="filler" id="email" type="text"/>
</div>
</li>

<li><div class="labelDiv">
<label>
Date of Birth

</label>
</div> 
<div class="name-fields">
<input class="filler" id="dob" type="text"/>
</div>
</li>

<li><div class="labelDiv">
<label>
Social Security number (Optional)

</label>
</div> 
<div class="name-fields">
<input class="filler" id="ssn" type="text"/>
</div>
</li>

</ul></div>
<hr/>



<div style="display:none;" id="msDiv"> 
<p style="margin-top: 3%;" class="heading">Life Change Event : Marital Status</p>
<ul class="individualUl">
<li><div class="docDiv">
<label>
Date of Change
<span class="asterisk">*</span>
</label>
</div> 
<div class="name-fields">
<input class="filler" id="docMarriage" type="text"/>
</div>
</li>
</ul>
<div style="margin-left: 10px;">
	<input class="appliedFormat" id="marriage" type="checkbox"><label>Marriage</label></input>
	<input class="appliedFormat" id="civilUnion" type="checkbox"><label>Civil Union</label></input>
	<input class="appliedFormat" id="deathOfSpouse" type="checkbox"><label>Death Of Spouse</label></input>
	<input class="appliedFormat" id="divorceOrAnnulment" type="checkbox"><label>Divorce or Annulment</label></input><br>
	<input id="legalSeparation" class="appliedFormat" type="checkbox"><label>Legal Separation</label></input>	
</div>
</div>

<div style="display:none;" id="nodDiv"> 
<p style="margin-top: 3%;" class="heading">Life Change Event : Number of Dependants</p>
<ul class="individualUl">
<li><div class="docDiv">
<label>
Date of Change
<span class="asterisk">*</span>
</label>
</div> 
<div class="name-fields">
<input class="filler" id="docNumberOfDependants" type="text"/>
</div>
</li>
</ul>
<div>
	<input class="appliedFormat" id="birth" type="checkbox"><label>Birth</label></input>
	<input class="appliedFormat" id="adoption" type="checkbox"><label>Adoption or placement of adoption</label></input>
	<input class="appliedFormat" id="deathOfDependentChild" type="checkbox"><label>Death Of dependent child</label></input><br>
	<input class="appliedFormat" id="ageOff" type="checkbox"><label>Dependent child ages off (26 years or older)</label></input>
	<input id="gainOtherDependen" class="appliedFormat" type="checkbox"><label>Gain other dependent</label></input>	
</div>
</div>

<p style="display:none;" id="ditP" class="heading">Dependent Information Table</p>
<div style="display:none;" id="DepInfoTable">

  <h1 class="border-bottom">Dependent Information Table</h1>
  <p class="border-bottom">This table is for the addition of new people to the account or deletion of people from the account. Please specify whether or not
this is an addition or deletion from the account along with filling out the field associated with each individual. Please fill out the
supporting document, Worksheet E for person you are adding to your coverage.</p>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="20%" align="left" valign="top" class="border">Dependent Information</td>
    <td width="15%" align="left" valign="top" class="border">Legal Name (First, Middle, Last, & Suffix)</td>
    <td width="18%" align="left" valign="top" class="border">Sex</td>
    <td width="17%" align="left" valign="top" class="border">SSN</td>
    <td width="9%" align="left" valign="top" class="border">Date of Birth</td>
    <td width="21%" align="left" valign="top" class="border-bottom padding">Relationship to Account Holder</td>
  </tr>
  <tr>
    <td width="20%" align="left" valign="top" class="border">PERSON 1<br />
     <input name="" type="checkbox" value="" />Add <input name="" type="checkbox" value="" />Drop
    </td>
    <td width="15%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="18%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="17%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="9%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="21%" align="left" valign="top" class="border-bottom padding"><input name="" type="text" class="inputBG" /></td>
  </tr>
   <tr>
    <td width="20%" align="left" valign="top" class="border">PERSON 2<br />
     <input name="" type="checkbox" value="" />Add <input name="" type="checkbox" value="" />Drop
    </td>
    <td width="15%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="18%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="17%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="9%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="21%" align="left" valign="top" class="border-bottom padding"><input name="" type="text" class="inputBG" /></td>
  </tr>
   <tr>
    <td width="20%" align="left" valign="top" class="border">PERSON 3<br />
     <input name="" type="checkbox" value="" />Add <input name="" type="checkbox" value="" />Drop
    </td>
    <td width="15%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="18%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="17%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="9%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="21%" align="left" valign="top" class="border-bottom padding"><input name="" type="text" class="inputBG" /></td>
  </tr>
   <tr>
    <td width="20%" align="left" valign="top" class="border">PERSON 4<br />
     <input name="" type="checkbox" value="" />Add <input name="" type="checkbox" value="" />Drop
    </td>
    <td width="15%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="18%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="17%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="9%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="21%" align="left" valign="top" class="border-bottom padding"><input name="" type="text" class="inputBG" /></td>
  </tr>
   <tr>
    <td width="20%" align="left" valign="top" class="border">PERSON 5<br />
     <input name="" type="checkbox" value="" />Add <input name="" type="checkbox" value="" />Drop
    </td>
    <td width="15%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="18%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="17%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="9%" align="left" valign="top" class="border"><input name="" type="text" class="inputBG" /></td>
    <td width="21%" align="left" valign="top" class="border-bottom padding"><input name="" type="text" class="inputBG" /></td>
  </tr>

  <tr align="left" valign="top">
    <td class="border-right padding">
    <div class="DepInfotr">
    <strong>Relationship Type Suggestions.</strong><br />
You may write in other
relationships if
needed.
</div>
</td>
    <td class="border-right">
    <div class="DepInfotr">
      <ul class="individualUl">
         <li>Husband</li>
         <li>Wife</li>
         <li>Domestic Partner</li>
         <li>Mother</li>
         <li>Father</li>
         <li>Stepmother</li>
         <li>Stepfather</li>
         <li>Parent’s domestic partner</li>
         <li>Son</li>
         <li>Daughter</li>
         <li>Stepson</li>
      </ul>
      </div>
    </td>
    <td>
    <div class="DepInfotr">
      <ul class="individualUl">
         <li>Stepdaughter</li>
         <li>Child of domestic partner</li>
         <li>Brother</li>
         <li>Sister</li>
         <li>Stepbrother</li>
         <li>Stepsister</li>
         <li>Half brother</li>
         <li>Half sister</li>
         <li>Disabled Adult Dependent</li>
         <li>Unrelated</li>
      </ul>
      </div>
    </td>

  </tr>
</table>



</div>

<div style="display:none;" id="lomecDiv"> 
<p style="margin-top: 3%;" class="heading">Life Change Event : Loss of minimum essential coverage</p>
<ul class="individualUl">
<li><div class="docDiv">
<label>
Date of Change
<span class="asterisk">*</span>
</label>
</div> 
<div class="name-fields">
<input class="filler" id="docLossOfMEC" type="text"/>
</div>
</li>
</ul>
<div style="margin-left: 10px;">
	<input class="appliedFormat" id="lossOfCoverage" type="checkbox"><label>Loss of coverage through head of household (except due to non-payment)</label></input><br>
	<input class="appliedFormat" id="lossOfEmployerCoverage" type="checkbox"><label>Loss of employer sponsored coverage</label></input><br>
	<input class="appliedFormat" id="otherMEC" type="checkbox"><label>Eligibility for other minimum essential coverage ends (including Medicaid or CHP+)</label></input>
</div>
</div>

<div style="display:none;" id="incarDiv"> 
<p style="margin-top: 3%;" class="heading">Life Change Event : Incarceration</p>
<ul class="individualUl">
<li><div class="docDiv">
<label>
Date of Change
<span class="asterisk">*</span>
</label>
</div> 
<div class="name-fields">
<input class="filler" id="docIncarceration" type="text"/>
</div>
</li>
</ul>
<div>
	<input class="appliedFormat" id="incarcerated" name="incarceration" value="yes" type="radio"><input id="incarceratedName" type="text"/><label>			Is now Incarcerated</label></input><br>
	<input class="appliedFormat" id="noLongerIncarcerated" name="incarceration" value="no" type="radio"><input id="noIncarceratedName" type="text"/><label>			Is no longer incarcerated</label></input>
	</div>
</div>

<div style="display:none;" id="lpDiv"> 
<p style="margin-top: 3%;" class="heading">Life Change Event : Lawful Presence</p>
<ul class="individualUl">
<li><div class="docDiv">
<label>
Date of Change
<span class="asterisk">*</span>
</label>
</div> 
<div class="name-fields">
<input class="filler" id="docLawfulPresence" type="text"/>
</div>
</li>
</ul>
<div style="margin-left: 10px;">
	<input class="appliedFormat" id="gainOfCitizenship" type="checkbox"><label>Gain of Citizenship</label></input>
	<input class="appliedFormat" id="gainLawfulPresence" type="checkbox"><label>Gain Lawful Presence</label></input><br>
	<input class="appliedFormat" id="moreThan5Years" type="checkbox"><label>Now lawfully present more than 5 years</label></input>
</div>
</div>

<p style="display:none;margin-top: 3%;" id="worksheetP" class="heading">WORKSHEET</p>

<div style="display:none;" id="DepInfoTable3">


  <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
     <td>
        <ul class="individualUl">
           <li>Person Name <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
           <li>If THIS PERSON is not a U.S. citizen or U.S. national, do they have eligible immigration status?<br />
<input name="" type="checkbox" value="" /> <strong>Yes.</strong> Fill in their document type, ID number, and alien registration number below. <input name="" type="checkbox" value="" /> <strong>No.</strong></li><br>

           <li>a. Immigration document type:<input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
           <li>b. Document ID number:<input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
           <li>c. Alien registration number:<input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
           <li>If document type is a passport: Country of origin:<input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /><br>Expiration date (mm/dd/yyyy):<input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
           <li>Has THIS PERSON lived in the U.S. since 1996? <input name="" type="checkbox" value="" /> <strong>Yes</strong> <input name="" type="checkbox" value="" /> <strong>No</strong></li>
           <li>Is <strong>THIS PERSON,</strong> or their spouse or parent an honorably discharged veteran or an active-duty member of the U.S. military?
<input name="" type="checkbox" value="" /> <strong>Yes</strong> <input name="" type="checkbox" value="" /> <strong>No</strong></li>
           <li><strong>If Yes,</strong> name(s):<input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        </ul>
     </td>
  </tr>
  
</table>




</div>

<div style="display:none;" id="cirmaDiv">
<p style="margin-top: 3%;" class="heading">Life Change Event : Change in residence/mailing address</p> 
<ul class="individualUl">
<li><div class="docDiv">
<label>
Date of Change
<span class="asterisk">*</span>
</label>
</div> 
<div class="name-fields">
<input class="filler" id="docChangeInResidence" type="text"/>
</div>
</li>
</ul>
<div style="margin-left: 10px;">
	<input class="appliedFormat" id="changePhysicalAddress" type="checkbox"><label>Change Physical Address</label></input>
	<input class="appliedFormat" id="changeMailingAddress" type="checkbox"><label>Change Mailing Address</label></input>
	<input class="appliedFormat" id="movedToNewMexico" type="checkbox"><label>Moved to New Mexico</label></input>
</div>

<div id="infoContainer3">
<ul class="individualUl">

<li><div class="labelDiv">
<label>
Previous Address
</label>
</div> 
<div class="name-fields">
<input class="filler" id="previousAddress" type="text"/>
</div>
</li>
<li><div class="labelDiv">
<label>
Apartment or Suite Number
</label>
</div> 
<div class="name-fields">
<input class="filler" id="previousSuiteNumber" type="text"/>
</div>
</li>
<li><div class="labelDiv">
<label>
City
</label>
</div> 
<div class="name-fields">
<input class="filler" id="previousCity" type="text"/>
</div>
</li>
<li><div class="labelDiv">
<label>
Zip Code
</label>
</div> 
<div class="name-fields">
<input class="filler" id="previousZip" type="text"/>
</div>
</li>
</ul>
</div>


<div id="infoContainer4">
<ul class="individualUl">

<li><div class="labelDiv">
<label>
New Address
</label>
</div> 
<div class="name-fields">
<input class="filler" id="newAddress" type="text"/>
</div>
</li>
<li><div class="labelDiv">
<label>
Apartment or Suite Number
</label>
</div> 
<div class="name-fields">
<input class="filler" id="newSuiteNumber" type="text"/>
</div>
</li>
<li><div class="labelDiv">
<label>
City
</label>
</div> 
<div class="name-fields">
<input class="filler" id="newCity" type="text"/>
</div>
</li>
<li><div class="labelDiv">
<label>
Zip Code
</label>
</div> 
<div class="name-fields">
<input class="filler" id="newZip" type="text"/>
</div>
</li>
</ul>
</div>

</div>

<div style="display:none;" id="incomeDiv">
<p style="margin-top: 3%;" class="heading">Life Change Event : Income</p> 
<ul class="individualUl">
<li><div class="docDiv">
<label>
Date of Change
<span class="asterisk">*</span>
</label>
</div> 
<div class="name-fields">
<input class="filler" id="docIncome" type="text"/>
</div>
</li>
</ul>
<div>
	<input class="appliedFormat" id="changeInIncome" type="checkbox"><label>Change In Income</label></input>
	<input class="appliedFormat" id="noLongerWorking" type="checkbox"><label>No Longer working at an Employer</label></input>
	</div>
</div>

<p style="display:none;" id="incomeP" class="heading">Current Job & Income Information for THIS PERSON</p>

<div style="display:none;margin-top: 20px;" id="DepInfoTable4">

Person Name <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /><br /><br />
<div class="clear"></div>

<div class="EmploySkip">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left" valign="top"><input name="" type="checkbox" value="" /></td>
            <td align="left" valign="top"> <strong>Employed</strong><br />
            If currently employed, tell us
            about <strong>THIS PERSON’s</strong> income.
            Start with question 24..</td>
        </tr>
    </table>
</div>
<div class="EmploySkip">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left" valign="top"><input name="" type="checkbox" value="" /></td>
            <td align="left" valign="top"> <strong>Not employed</strong><br />
SKIP to question 32.</td>
        </tr>
    </table>
</div>
<div class="EmploySkip">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left" valign="top"><input name="" type="checkbox" value="" /></td>
            <td align="left" valign="top"><strong>Self-employed or have other income</strong><br />
SKIP to question 32.</td>
        </tr>
    </table>
</div>

<div class="clear"></div>

<div class="CurrenJob1">
    <h1>CURRENT JOB 1 for THIS PERSON:</h1>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="56%" align="left" valign="top" class="border">24. Employer name and address <input name="" type="text" class="inputBG" /></td>
    <td width="44%" align="left" valign="top" class="border-bottom padding">25. Employer phone number <input name="" type="text" class="inputBG" /></td>
  </tr>
   <tr>
    <td align="left" valign="top" class="border-right">26. Wages/tips (before taxes)<input name="" type="text" class="inputBG" />
    <div class="clear"></div>
     <div class="WagesBox">$ <input name="" type="text" class="inputBG2"style="border-bottom:1px solid #333" /></div>
     <div class="WagesBox">
      <ul class="individualUl">
         <li><input name="" type="checkbox" value="" /> Hourly</li>
         <li><input name="" type="checkbox" value="" /> Weekly</li>
         <li><input name="" type="checkbox" value="" /> Every 2 weeks</li>
      </ul>
     </div>
     <div class="WagesBox">
      <ul class="individualUl">
         <li><input name="" type="checkbox" value="" /> Twice a month</li>
         <li><input name="" type="checkbox" value="" /> Monthly</li>
         <li><input name="" type="checkbox" value="" /> Yearly</li>
      </ul>
     </div>

    </td>
    <td align="left" valign="top" class="padding">27. Average hours worked each WEEK <input name="" type="text" class="inputBG" /></td>
  </tr>
</table>

</div>

<div class="CurrenJob1">
    <h2>CURRENT JOB 2 for THIS PERSON:<span style="font-size:13px;"> (If THIS PERSON has more jobs and you need more space, attach another sheet of
paper.)</span></h2>	
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="56%" align="left" valign="top" class="border">28. Employer name and address <input name="" type="text" class="inputBG" /></td>
    <td width="44%" align="left" valign="top" class="border-bottom padding">29. Employer phone number <input name="" type="text" class="inputBG" /></td>
  </tr>
   <tr>
    <td align="left" valign="top" class="border-right">30. Wages/tips (before taxes)<input name="" type="text" class="inputBG" />
    <div class="clear"></div>
     <div class="WagesBox">$ <input name="" type="text" class="inputBG2"style="border-bottom:1px solid #333" /></div>
     <div class="WagesBox">
      <ul class="individualUl">
         <li><input name="" type="checkbox" value="" /> Hourly</li>
         <li><input name="" type="checkbox" value="" /> Weekly</li>
         <li><input name="" type="checkbox" value="" /> Every 2 weeks</li>
      </ul>
     </div>
     <div class="WagesBox">
      <ul class="individualUl">
         <li><input name="" type="checkbox" value="" /> Twice a month</li>
         <li><input name="" type="checkbox" value="" /> Monthly</li>
         <li><input name="" type="checkbox" value="" /> Yearly</li>
      </ul>
     </div>

    </td>
    <td align="left" valign="top" class="padding">31. Average hours worked each WEEK <input name="" type="text" class="inputBG" /></td>
  </tr>
</table>
</div>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>32. <strong>In the past year, did THIS PERSON:</strong> <input name="" type="checkbox" value="" /> Change jobs
     <input name="" type="checkbox" value="" />Stop working Start 
     <input name="" type="checkbox" value="" />working different hours
    <input name="" type="checkbox" value="" />Have a death in the family 
    <input name="" type="checkbox" value="" />Get married, legally separated, or divorced 
    <input name="" type="checkbox" value="" />Receive a wage or salary change
    <input name="" type="checkbox" value="" />None of these<br /><br /></td>
  </tr>
  <tr>
    <td class="border-top border-bottom">33. <strong>Is THIS PERSON</strong> a seasonal worker? <input name="" type="checkbox" value="" /><strong>Yes</strong> <input name="" type="checkbox" value="" /><strong>No</strong><br /><br /></td>
  </tr>
</table>

<div class="CurrenJob1">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
    <td width="36%" align="left" valign="top" class="border-right">a. Type of work<br />
    <input name="" type="text" class="inputBG3"style="border-bottom:1px solid #333" />

    </td>
    <td width="64%" align="left" valign="top" class="padding">b. How much gross income (profits before taxes, deductions,
or expenses are paid) will THIS PERSON receive from
this self-employment this month?<br />
 $<input name="" type="text" class="inputBG2" style="border-bottom:1px solid #333"/></td>
  </tr>
</table>
</div>


</div>

</div>

<div style="display:none;" id="gomecDiv">
<p style="margin-top: 3%;" class="heading">Life Change Event : Gain Other minimum essential coverage</p> 
<ul class="individualUl">
<li><div class="docDiv">
<label>
Date of Change
<span class="asterisk">*</span>
</label>
</div> 
<div class="name-fields">
<input class="filler" id="docOtherMEC" type="text"/>
</div>
</li>
</ul>
<div>
	<input class="appliedFormat" id="reached65" type="checkbox"><label>Reach the age of 65; or eligible for Medicare</label></input><br>
	<input class="appliedFormat" id="tricareEligible" type="checkbox"><label>Eligible for TriCare or Enrolled in Veteran’s Affair coverage</label></input><br>
	<input class="appliedFormat" id="newCoverageAvailable" type="checkbox"><label>Coverage newly available through spouse</label></input><br>
	<input class="appliedFormat" id="eligibilityForOtherMEC" type="checkbox"><label>Eligibility/Enrollment for other minimum essential coverage begins (including
Medicaid or CHP+)</label></input><br>
	<input class="appliedFormat" id="gainThroughEmployer" type="checkbox"><label>Gain affordable coverage through employer</label></input>
	</div>
</div>

<p id="hcP" style="display:none;" class="heading">Health Coverage</p>

<div style="display:none; margin-top: 15px;" id="DepInfoTable2">
  
<p class="border-bottom">Answer these questions for anyone who has gained other minimum essential coverage or whose employer sponsored
coverage has become unaffordable.</p>

  <p class="border-bottom">
  <strong>Is anyone enrolled in or eligible for health coverage now from the following?</strong><br />
  <input name="" type="checkbox" value="" /><strong>Yes.</strong> If Yes, check the type of coverage and write the person(s)’ name(s) next to the coverage.        <br><input name="" type="checkbox" value="" /> <strong>No.</strong>
  </p>

  <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="30%" class="border"><input name="" type="checkbox" value="" /> Medicaid</td>
    <td width="49%" valign="top" class="border">
    <div class="nameForm">
     <ul class="individualUl">
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
     </ul>
     </div>
    </td>
    <td width="21%" class="border-bottom"><input name="" type="checkbox" value="" /> Enrolled <br><input name="" type="checkbox" value="" /> Eligible</td>
  </tr>
  <tr>
    <td width="30%" class="border"><input name="" type="checkbox" value="" /> Child Health Plan Plus (CHP+)</td>
    <td width="49%" valign="top" class="border">
    <div class="nameForm">
     <ul class="individualUl">
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
     </ul>
     </div>
    </td>
    <td width="21%" class="border-bottom"><input name="" type="checkbox" value="" /> Enrolled <br><input name="" type="checkbox" value="" /> Eligible</td>
  </tr>
  <tr>
    <td width="30%" class="border"><input name="" type="checkbox" value="" /> Medicare</td>
    <td width="49%" valign="top" class="border">
    <div class="nameForm">
     <ul class="individualUl">
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Medicare claim number: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        Check for: <input name="" type="checkbox" value="" /> Part A <input name="" type="checkbox" value="" /> Part B <input name="" type="checkbox" value="" /> Part D<br />
        
If eligible but NOT enrolled in Part A, do you pay a
premium <input name="" type="checkbox" value="" /><strong>Yes</strong> <input name="" type="checkbox" value="" /> <strong>No</strong><br /><br />


Please include a copy of the front and back of the Medicare
card with this form if it is available.
     </ul>
     </div>
    </td>
    <td width="21%" class="border-bottom"><input name="" type="checkbox" value="" /> Enrolled <br><input name="" type="checkbox" value="" /> Eligible</td>
  </tr>
  <tr>
    <td width="30%" class="border"><input name="" type="checkbox" value="" /> TRICARE (Do not check if you have direct
care or Line of Duty)</td>
    <td width="49%" valign="top" class="border">
    <div class="nameForm">
     <ul class="individualUl">
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Policy number: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
     </ul>
     </div>
    </td>
    <td width="21%" class="border-bottom"><input name="" type="checkbox" value="" /> Enrolled <br><input name="" type="checkbox" value="" /> Eligible</td>
  </tr>
  <tr>
    <td width="30%" class="border"><input name="" type="checkbox" value="" /> VA Health Care Programs</td>
    <td width="49%" valign="top" class="border">
    <div class="nameForm">
     <ul class="individualUl">
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Policy number: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
     </ul>
     </div>
    </td>
    <td width="21%" class="border-bottom"><input name="" type="checkbox" value="" /> Enrolled <br><input name="" type="checkbox" value="" /> Eligible</td>
  </tr>
  <tr>
    <td width="30%" class="border"><input name="" type="checkbox" value="" /> Peace Corps</td>
    <td width="49%" valign="top" class="border">
    <div class="nameForm">
     <ul class="individualUl">
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
     </ul>
     </div>
    </td>
    <td width="21%" class="border-bottom"><input name="" type="checkbox" value="" /> Enrolled <br><input name="" type="checkbox" value="" /> Eligible</td>
  </tr>
  <tr>
    <td width="30%" class="border"><input name="" type="checkbox" value="" /> Medicaid</td>
    <td width="49%" valign="top" class="border">
    <div class="nameForm">
     <ul class="individualUl">
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <strong>If Yes,</strong> complete and include <strong>Worksheet A.</strong>
        <li>Name of health plan: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Policy number: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Start date of coverage or date the coverage could start<br />
         	(mm/dd/yyyy): <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Is this COBRA(i) coverage? <input name="" type="checkbox" value="" /><strong>Yes</strong> <input name="" type="checkbox" value="" /> <strong>No</strong></li> 
        <li><strong>If Yes</strong>, complete and include <strong>Worksheet A.</strong></li> 
        <li>Is this a retiree health plan? <input name="" type="checkbox" value="" /> <strong>Yes</strong> <input name="" type="checkbox" value="" /> <strong>No</strong></li> 
        <li><strong>If Yes,</strong> complete and include <strong>Worksheet A.</strong></li> 
        <li>If also eligible for Medicaid, do any members of this
household have access to group health insurance and
want help paying the monthly premium?<br /> <input name="" type="checkbox" value="" /> <strong>Yes</strong> <input name="" type="checkbox" value="" /> <strong>No</strong></li>   
     </ul>
     </div>
    </td>
    <td width="21%" class="border-bottom"><input name="" type="checkbox" value="" /> Enrolled <br><input name="" type="checkbox" value="" /> Eligible</td>
  </tr>
  <tr>
    <td width="30%" class="border"><input name="" type="checkbox" value="" /> Other</td>
    <td width="49%" valign="top" class="border">
    <div class="nameForm">
     <ul class="individualUl">
        <li>Name: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Name of health plan and/or policy type: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Start date of coverage or date the coverage could start<br />
(mm/dd/yyyy): <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
        <li>Policy number: <input name="" type="text" class="textBorder" style="border-bottom:1px solid #333" /></li>
     </ul>
     </div>
    </td>
    <td width="21%" class="border-bottom"><input name="" type="checkbox" value="" /> Enrolled <br><input name="" type="checkbox" value="" /> Eligible</td>
  </tr>
  
</table>




</div>

<div style="display:none;" id="otherDiv"> 
<p style="margin-top: 3%;" class="heading">Life Change Event : Other</p>
<ul class="individualUl">
<li><div class="docDiv">
<label>
Date of Change
<span class="asterisk">*</span>
</label>
</div> 
<div class="name-fields">
<input class="filler" id="docOther" type="text"/>
</div>
</li>
</ul>
<div>
	<input class="appliedFormat" id="erroneousEnrollment" type="checkbox"><label>Erroneous Enrollment by the Marketplace, a broker, or a Health Coverage Guide</label></input><br>
	<input class="appliedFormat" id="AIANStatus" type="checkbox"><label>Gain of American Indian/Alaska Native status</label></input><br>
	<input class="appliedFormat" id="lossOfEligibility" type="checkbox"><label>Loss of eligibility for the exemption to purchase health care coverage &nbsp; &nbsp; </label><br><label style="margin-left: 25px;font-weight:bold;">Exemption:</label></input><input type="text" style="margin-left: 5px; margin-top: 10px;" id="exemption"><br>
	<input class="appliedFormat" id="otherCircumstance" type="checkbox"><label>Other exceptional circumstances</label></input><br>
	<input class="appliedFormat" id="coverageUnaffordable" type="checkbox"><label>Employer sponsored coverage becomes unaffordable</label></input>
	</div>
</div>


</div>

</div>