import React, { Fragment } from 'react';


// Components

import UniversalInput from '../../../../lib/UniversalInput';

// Constants

import INPUT_TYPES from '../../../../constants/inputTypes';
import DROPDOWN_OPTIONS from '../../../../constants/dropdownOptions';
import VALIDATION_TYPES from '../../../../constants/validationTypes';



const PairsBlock = props => {

    const {
        coverageYear,
        family,
        onFieldChange,
        ...restProps
    } = props;

    const getPairs = () => {

        return family.pairs
            .filter(pair => pair.isPairAccordingToLaw && pair.isAnyOfSpousesFiler)
            .map((pair, idx) => {

                const {
                    pairId,
                    spouse1: {
                        spouse1fullName
                    },
                    spouse2: {
                        spouse2fullName,
                    },
                    doSpousesFileTogether,
                    doSpousesFileSeparately
                } = pair;

                const getValue = () => doSpousesFileTogether ? 'jointly' : doSpousesFileSeparately ? 'separately' : null;

                return (
                    <Fragment key={pairId}>

                        <UniversalInput
                            legend={{
                                legendText:
                                    <span>
                                        Do &nbsp;
                                        <strong>
                                            { spouse1fullName } &nbsp;
                                        </strong>
                                        and  &nbsp;
                                        <strong>
                                            { spouse2fullName } &nbsp;
                                        </strong>
                                        plan to file a joint federal income tax return for &nbsp;
                                        <strong>
                                            { coverageYear }
                                        </strong>
                                        ?
                                    </span>
                            }}
                            label={{
                                text: '',
                                labelClass: ['usa-width-one-whole', 'layout--02'],
                                ulClass: ['usa-width-one-whole margin-b-20'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                classes:['disp-styl-none'],
                                name: 'spousesChoice',
                                value: getValue(),
                                options: DROPDOWN_OPTIONS.FILING_JOINTLY_YES_NO,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: {},
                                fields: []
                            }}
                            inputActions={{
                                onFieldChange
                            }}
                            indexInArray={idx}
                            {...restProps}
                        />

                    </Fragment>
                );
            });
    };

    const isAnySpouseChecked = () => family.pairs.some(pair => pair.isAnyOfSpousesFiler);

    return isAnySpouseChecked() ?

        <div className="subsection">
            {
                getPairs()
            }
        </div> : null;
};

export default PairsBlock;
