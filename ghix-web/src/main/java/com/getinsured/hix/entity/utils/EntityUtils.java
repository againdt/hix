package com.getinsured.hix.entity.utils;

import static com.getinsured.hix.platform.util.GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_USER_ID;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityResponseDTO;
import com.getinsured.hix.dto.planmgmt.template.admin.niem.proxy.Date;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.platform.config.AEEConfiguration;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

/**
 * Entity Util class.
 * 
 * 
 */
@Component
public final class EntityUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(EntityUtils.class);
	private static final Object ASSISTERENROLLMENTENTITYADMIN = "ASSISTERENROLLMENTENTITYADMIN";
	private static final String USER_NOT_LOGGED_IN = "User not logged in";

	private static final long MAX = 9999999999L;
	private static final long MIN = 1000000000;
	public static final String HAS_PERMISSION_MODEL_ENTITY_VIEW_ASSISTER = "hasPermission(#model, 'ENTITY_VIEW_ASSISTER')";
	private static final String ASSISTER = "ASSISTER";
	private static final String FALSE = "false";
	private static final String TRUE = "true";
	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	@Autowired
	private UserService userService;
	
	@Autowired
	private EERestCallInvoker restClassCommunicator;

	@Autowired
	private ZipCodeService zipCodeService;
	
	@Autowired private Gson platformGson;
	@Autowired
	private LookupService lookupService;
	
	private EntityUtils(){}
	
	/**
	 * @see EntityUtils#marshal(GHIXResponse)
	 * 
	 *      Marshal and form XML response.
	 * 
	 * @param response
	 *            the response object to marshal
	 * @return XML String
	 */
	public static String marshal(GHIXResponse response) {
		XStream xstream = GhixUtils.getXStreamStaxObject();
		return xstream.toXML(response);
	}

	/**
	 * @see EntityUtils#convertUtilToSQLDate(Date)
	 * 
	 *      Converts java util date to java sql date
	 * 
	 * @param date
	 *            the util date to be converted into sql date
	 * @return java.sql.Date
	 */
	public static java.sql.Date convertUtilToSQLDate(java.util.Date date) {
		return new java.sql.Date(date.getTime());
	}

	/**
	 * @see EntityUtils#formatDate(String)
	 * 
	 *      Formats the given date into format defined in
	 *      BrokerConstants.DATE_FORMAT
	 * 
	 * @param date
	 *            the date to be formatted
	 * @return java.util.Date formatted date
	 */

	/**
	 * @see EntityUtils#isEmpty(String)
	 * 
	 *      Checks for null or blank String objects
	 * 
	 * @param value
	 *            the string to check
	 * @return true or false depending upon the condition
	 */
	public static boolean isEmpty(String value) {
		return (value != null && !"".equals(value)) ? false : true;
	}

	public static List<String> splitStringValues(String values, String splitBy) {
		List<String> items = null;
		items = Arrays.asList(values.split(splitBy));
		items.removeAll(Collections.singletonList(null));

		List<String> stringList = new ArrayList<String>();
		for (Iterator<String> iter = items.iterator(); iter.hasNext();) {
			String element = iter.next();
			if (!stringList.contains(element.trim())) {
				stringList.add(element.trim());
			}
		}
		return stringList;
	}

	/**
	 * Generates random number
	 * 
	 * @return long Random number
	 */
	public static synchronized long generateRandomCertificationNumber() {
		return (long) Math.round(Math.random() * (MAX - MIN + 1) + MIN);
	}

	/**
	 * Retrieves Registration Step next to current one
	 * 
	 * @param lastStepCompleted
	 * @return String representing Registration step next to current one
	 */
	public static String getNextRegistrationStep(String lastStepCompleted) {
		int index = 0;
		boolean isCAStateCode = EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE);
		List<String> stepList = new ArrayList<String>();
		stepList.add("ENTITY_INFO");
		stepList.add("POPULATION_SERVED");
		stepList.add("PRIMARY");
		stepList.add("SUBSITE");
		stepList.add("ENTITY_CONTACT");
		stepList.add("ASSISTER");
		stepList.add("DOCUMENT_UPLOAD");
		if(!isCAStateCode){
			stepList.add("PAYMENT_INFO");
		}
		stepList.add("REGISTRATION_STATUS");
		for (int i = 0; i < stepList.size(); i++) {
			if (stepList.get(i).equals(lastStepCompleted)) {
				index = i;
				break;
			}

		}
		index = index + 1;
		return stepList.get(index);
	}
	
	/**
	 * This method checks if an assiterId coming from request is associated with the logged in User
	 * @param assisterId
	 * @param user
	 * @param assisterResponseDTO
	 * @throws GIException
	 */
	public static void checkEntityAssisterAssociation(String assisterId, AccountUser user,
			AssisterResponseDTO assisterResponseDTO) throws GIException {
		
		if(user == null){
			LOGGER.error(USER_NOT_LOGGED_IN);
			throw new GIException(new InvalidUserException());
		}
		
		Set<UserRole> userRole = user.getUserRole();
		Set<String> roleName = new HashSet<String>();
		for (UserRole userRole2 : userRole) {
			roleName.add(userRole2.getRole().getName());
		}
		
		if(!roleName.contains(ASSISTER) && !roleName.contains(ASSISTERENROLLMENTENTITYADMIN) && assisterResponseDTO != null) {
			verifyEntityUserAssociation(assisterId, user, assisterResponseDTO);
		}
	}

	/**
	 * Method to verify the assiter's associated  user identifier with the logged in user account.
	 *  
	 * @param assisterId The assister identifier
	 * @param user The AcccountUser instance
	 * @param assisterResponseDTO The AssisterResponseDTO instance
	 * @throws GIException The GIException instance.
	 */
	private static void verifyEntityUserAssociation(String assisterId,
			AccountUser user, AssisterResponseDTO assisterResponseDTO)
			throws GIException {
		if(((assisterResponseDTO.getAssister() == null)|| 
				assisterResponseDTO.getAssister().getEntity() == null|| 
				assisterResponseDTO.getAssister().getEntity().getUser() == null) || user.getId() != assisterResponseDTO.getAssister().getEntity().getUser().getId()) {
				LOGGER.error("Error while processing Assister data. Invalid Assister ID.");
				throw new GIException("Access not allowed for "+((assisterId != null)?assisterId:""));
		}
	}	

	
	/**
	 * @param var The Object instance.
	 * @return String The resultant String.
	 */
	public static String initializeAndUnproxy(Object var) {
		String result = null;
        if (var == null) {
            return result;
        }

        Hibernate.initialize(var);
        if (var instanceof HibernateProxy) {
        	result = (String) ((HibernateProxy) var).getHibernateLazyInitializer()
                    .getImplementation();
        }
        else if (var instanceof String) {
        	result = (String)var;
        }
        else if (var instanceof LookupValue) {
        	result = ((LookupValue)var).getLookupValueLabel().trim();
        }
        return result;
    }
	
	/**
	 * @param value
	 * @return
	 */
	public static boolean isNullOrBlank(String value) {
		return value != null && !"".equals(value);
	}

	/**
	 * Method to fetch HouseHold/Consumer of a given identifier. 
	 * 
	 * @param userId The user identifier of the Household/Consumer.
	 * @return household The Household instance.
	 */
	
	public Household getHouseHoldByUserId(int userId){
		LOGGER.info("getHouseHoldByUserId : START");
		Household household = null;
		XStream xstream = null;
		LOGGER.debug("Rest call to get householeId starts");
		try{
			String response = ghixRestTemplate.postForObject(GET_HOUSEHOLD_BY_USER_ID, String.valueOf(userId), String.class);

			LOGGER.debug("rest call to get householeId ends");
			
			if(null == response) {
				LOGGER.warn("No Household record for given user identifier returned by ReST call.");
			}
			else {
				xstream = GhixUtils.getXStreamStaxObject();
				
				household = (Household) xstream.fromXML(response);
			}
		}
		catch(Exception ex){
			LOGGER.error("Rest call to find household failed", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getHouseHoldByUserId : END");
		return household;
	}
	
	/**
	 * Method to get currently logged in user. 
	 * 
	 * @return currUser The current AccountUser instance.
	 * @throws GIException The current GIException instance.
	 */	
	public AccountUser getLoggedInUser() throws GIException {
		LOGGER.info("getLoggedInUser :START");
		AccountUser currUser = null;
		try {
			currUser = userService.getLoggedInUser();
		} 
		catch (InvalidUserException e) {
			LOGGER.error("Exception occurred d while retrieving user information. Exception:" + e.getMessage(), e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			LOGGER.info("getLoggedInUser :END");
		}
		
		return currUser;
	}

	/**
	 * Method to retrieve EnrollmentEntity for given identifier.
	 * 
	 * @param entityId The entity identifier.
	 * @return enrollmentEntity The EnrollmentEntity instance.
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	public EnrollmentEntity getEnrollmentEntityById(int entityId) throws JsonProcessingException, IOException {
		EnrollmentEntity enrollmentEntity;
		EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequest.setModuleId(entityId);

		LOGGER.info("Retrieving Enrollment Entity REST Call Starts");
		String enrollmentEntityObjStr = restClassCommunicator.retrieveEnrollmentEntityObjectById(enrollmentEntityRequest);
		LOGGER.info("Retrieving Enrollment Entity Detail REST Call Ends");
		
		EnrollmentEntityResponseDTO enrollmentEntityResponse =   JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollmentEntityObjStr);
		enrollmentEntity = enrollmentEntityResponse.getEnrollmentEntity();
		return enrollmentEntity;
	}
	
	/**
	 * Method to populate County name list for a given State Code.
	 *  
	 * @return jsonData Stringified JSON of County name collection.
	 */
	public String loadCountiesServedData(){
		LOGGER.info("loadCountiesServedData : START");
		
		String jsonData = null;
		
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		
		Set<String> countiesServedlist = this.populateCountiesServedDataSet(stateCode);
		 
		
		jsonData = platformGson.toJson(countiesServedlist);
		
		LOGGER.info("loadCountiesServedData : END");

		return jsonData;
	}
	
	/**
	 * Method to populate Enrollment Entity served Counties data set.
	 * 
	 * @param stateCode The state code.
	 * @return countiesServedCollection The collection of counties served by Enrollment entity for the current State.
	 */
	public Set<String> populateCountiesServedDataSet(String stateCode){
		List<ZipCode> zipCodeVOList = null;  
		Set<String> countiesServedCollection = new TreeSet<String>();
		
		if(null == stateCode || stateCode.trim().isEmpty()) {
			LOGGER.info("State code is missing in the request.", stateCode);
		}
		else {
			zipCodeVOList = zipCodeService.getCountyListForState(stateCode);
			
			for(ZipCode currZipCode : zipCodeVOList){
				if(null != currZipCode && null != currZipCode.getCounty() && !currZipCode.getCounty().trim().isEmpty()) {
					countiesServedCollection.add(currZipCode.getCounty());
				}
			}
		}
		
		return countiesServedCollection;
	}
	
	public Household getHouseHoldById(int id){
		
		LOGGER.info("getHouseHoldById : STARTS");
		Household household = null;
		XStream xstream = null;
		ConsumerResponse consumerResponse = null;
		
		//get the household using rest call
		try{
			LOGGER.debug("Rest call to get householeObj starts");
			String response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID, String.valueOf(id), String.class);
			LOGGER.debug("rest call to get householeObj ends");
			xstream = GhixUtils.getXStreamStaxObject();
			if(response != null){
				consumerResponse = (ConsumerResponse) xstream.fromXML(response);
				household = consumerResponse.getHousehold();
			}
		}catch(Exception ex){
			LOGGER.error("Rest call to find householdObj failed", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}		
		LOGGER.info("getHouseHoldById : ENDS");
		return household;
	}

	/**
	 * Checks valid file extension
	 * 
	 * @param extension
	 * @return true/false
	 */
	public static boolean checkFileExtension(String extension){
		return getExtensions().contains(extension.toLowerCase());
	}
	
	public static List<String> getExtensions(){
		List<String> list = new ArrayList<String>();
		list.add("jpg");
		list.add("jpeg");
		list.add("gif");
		list.add("png");
		list.add("bmp");
		
		return list;
	}
	
	/**
	 * @see EntityUtils#checkXSS(String)
	 * 
	 * @param value
	 * 
	 * @return checks the presence of any script in file content
	 */
	public static boolean checkXSSImage(String value) {
		boolean isXSS = false;
		
		if(value.toUpperCase().contains("<script".toUpperCase()) || value.toUpperCase().contains("<meta".toUpperCase()) ||
				 value.toUpperCase().contains("<html".toUpperCase())){
			isXSS = true;
		} else {
			for (Pattern pattern : patterns){
				Matcher matcher = pattern.matcher(value);
				if(matcher.matches()){
					isXSS = true;
					break;
				} 
	        }
		}
		
		return isXSS;
	}
	
	private static Pattern[] patterns = new Pattern[]{
        // Script fragments
		Pattern.compile(".*<script.*", Pattern.CASE_INSENSITIVE),
		Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE),
        Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE),
        Pattern.compile("</script>", Pattern.CASE_INSENSITIVE),
        Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
       Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE),
        // vbscript:...
        Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE),
         Pattern.compile("alert\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        
        Pattern.compile("alert.*?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        //prompt(...)
        Pattern.compile("prompt\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        
        Pattern.compile("prompt.*?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
     };
	
	/**
	 * @see EntityUtils#getFileContent(MultipartFile)
	 * 
	 * @param file
	 * 
	 * @return file content in the form of String 
	 */
	public static String getFileContent(MultipartFile file) throws GIException{
        InputStream is = null;
        StringBuilder sb = new StringBuilder();
        final int BYTE_ARRAY_SIZE = 1024;
        
        try {
            is = file.getInputStream();
            byte content[] = new byte[2*BYTE_ARRAY_SIZE];
            int readCount = 0;
            while((readCount = is.read(content)) > 0){
            	sb.append(new String(content, 0, readCount-1));
            }
        } catch (Exception exception) {
        	LOGGER.error("Exception occurred in getFileContent().", exception);        	
        	throw new GIException(exception);
        } 
        finally {
        	IOUtils.closeQuietly(is);
        }
        
	  return sb.toString();
	}
	
	/**
	 * @see EntityUtils#formatSSN(String)
	 * 
	 *      removes all '.' from file name
	 * 
	 * @return formatted file name 
	 */
	public static String removeDots(String ssn){
		return ssn.replaceAll("[. ]+", "_");
	}
	/**
	 * This method checks whether send paper notices flag in gi_app_config table is "Y" or "N".
	 * Returns "true" when enrollmententities.counselors.allowMailNotices is "Y", otherwise returns "false".
	 * @param userLocal
	 * @return
	 */
	public static String isAllaowMailNotices(){
		String showPostalMailOption=FALSE;
		String IS_ALLOW_MAIL_NOTICES= DynamicPropertiesUtil.getPropertyValue(AEEConfiguration.AEEConfigurationEnum.ENROLLMENTENTITY_CEC_ALLOWMAILNOTICES);
		if( IS_ALLOW_MAIL_NOTICES!= null && IS_ALLOW_MAIL_NOTICES.equalsIgnoreCase("Y")){//userLocal.getActiveModuleName().equalsIgnoreCase(ASSISTER) &&
			showPostalMailOption=TRUE;
		}
		return  showPostalMailOption;
	}

	/**
	 * Returns the last element of a string
	 * 
	 * @param str
	 * @param delimeter
	 * @return String
	 */
	public static String getLastElement(String str, String delimeter){
		String[] array = str.split(delimeter);
		return array[array.length-1];
	}
	
	public static String removeLastElement(String str, String delimeter){
		return str.substring(0,str.lastIndexOf(delimeter));
	}
			
	/**
	 * Method to determine SortOrder.
	 * 
	 * @param request The current HttpServletRequest instance.
	 * @return sortOrder The sort order String.
	 */
	public static SortOrder determineSortOrder(HttpServletRequest request) {
		SortOrder order = SortOrder.ASC;
		LOGGER.info("determineSortOrder: START");
		final String CHANGE_ORDER = "changeOrder";
		String sortOrder =  (request.getParameter(EntityConstants.SORT_ORDER) == null) ?  EntityConstants.ASC : request.getParameter(EntityConstants.SORT_ORDER);
		sortOrder = (sortOrder.equals("")) ? EntityConstants.ASC : sortOrder;
		String changeOrder =  (request.getParameter(CHANGE_ORDER) == null) ?  FALSE : request.getParameter(CHANGE_ORDER);
		sortOrder = (changeOrder.equals(TRUE) ) ? ((sortOrder.equals(EntityConstants.ASC)) ? "DESC" : EntityConstants.ASC ): sortOrder;
		LOGGER.info("determineSortOrder: END");
		if("DESC".equals(sortOrder)){
			order = SortOrder.DESC;
		}
		return order;
	}
	
	public static boolean isEnrollmentEntityCertifiedAndActive(
			EnrollmentEntity enrollmentEntity) {
		if (enrollmentEntity != null
				&& enrollmentEntity.getRegistrationStatus() != null
				&& !(enrollmentEntity.getRegistrationStatus()
						.equalsIgnoreCase("Active".toString()))) {

			return true;
		}
		return false;
	}

	public static boolean isAssisterCertified(
			Assister assisterObj) {
		if (assisterObj != null
				&& assisterObj.getCertificationStatus() != null
				&& !(assisterObj.getCertificationStatus()
						.equalsIgnoreCase(Broker.certification_status.Certified
								.toString()))) {

			return true;
		}
		return false;
	}
	
	public boolean isEnrollmentEntityCertifiedAndActive(int id) throws GIRuntimeException, JsonProcessingException, IOException 
	{
		
		EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequest.setModuleId(id);
				
		// get enrollment entity details
		String entityObjStr = restClassCommunicator.retrieveEnrollmentEntityObjectByUserId(enrollmentEntityRequest);
		
		EnrollmentEntityResponseDTO enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityObjStr);
		EnrollmentEntity enrollmentEntity = enrollmentEntityResponse.getEnrollmentEntity();
		
		if (enrollmentEntity != null && enrollmentEntity.getRegistrationStatus() != null
				&& !(enrollmentEntity.getRegistrationStatus().equalsIgnoreCase("Active".toString()))) {

			return true;
		}
		return false;
	}
	
	public boolean isAssisterCertified(int id)
	{
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();	
		assisterRequestDTO.setModuleId(id);
		String assisterObjStr = restClassCommunicator.retrieveAssisterObjectByUserId(assisterRequestDTO);
		AssisterResponseDTO assisterResponseDTOFromUserId;
		try {
			assisterResponseDTOFromUserId = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterObjStr);
		} catch (Exception e) {
			throw new GIRuntimeException("Execption occured while feting assister details.");
		}
		
		Assister assisterObj = assisterResponseDTOFromUserId.getAssister();
		
		if (assisterObj != null && assisterObj.getCertificationStatus() != null && (assisterObj.getCertificationStatus()
				.equalsIgnoreCase(Broker.certification_status.Certified.toString()))) {

			return true;
		}
		return false;
	}
	
	public static boolean checkStateCode(String code){
		  String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		  return stateCode.equalsIgnoreCase(code);
	}
	
	public String checkLanguage(String language){
	    List<String> languageSpokenList = null;

	    languageSpokenList = lookupService.populateLanguageNames("");
	    
	    if(languageSpokenList!=null){
	    	if(languageSpokenList.contains(language)){
	    		return language;
	    	}
	    }
        return "";
	}
	
	public static Location getLocationFromDTO(LocationDTO locationDto){
		if(locationDto == null){
			return null;
		}
		Location l = new Location();
		l.setAddress1(locationDto.getAddressLine1());
		l.setAddress2(locationDto.getAddressLine2());
		l.setCity(locationDto.getCity());
		l.setState(locationDto.getState());
		l.setZip(locationDto.getZipcode());
		l.setCounty(locationDto.getCountyName());
		l.setCountycode(locationDto.getCountyCode());
		return l;
	}
}
