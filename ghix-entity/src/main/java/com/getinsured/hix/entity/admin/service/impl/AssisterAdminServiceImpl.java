package com.getinsured.hix.entity.admin.service.impl;

import static com.getinsured.hix.entity.assister.history.AssisterHistoryRendererImpl.ATTACHMENT;
import static com.getinsured.hix.entity.assister.history.AssisterHistoryRendererImpl.ATTACHMENT_COL;
import static com.getinsured.hix.entity.assister.history.AssisterHistoryRendererImpl.COMMENTS;
import static com.getinsured.hix.entity.assister.history.AssisterHistoryRendererImpl.COMMENT_COL;
import static com.getinsured.hix.entity.assister.history.AssisterHistoryRendererImpl.MODEL_NAME;
import static com.getinsured.hix.entity.assister.history.AssisterHistoryRendererImpl.NEW_STATUS;
import static com.getinsured.hix.entity.assister.history.AssisterHistoryRendererImpl.PREVIOUS_STATUS;
import static com.getinsured.hix.entity.assister.history.AssisterHistoryRendererImpl.REPOSITORY_NAME;
import static com.getinsured.hix.entity.assister.history.AssisterHistoryRendererImpl.STATUS_COL;
import static com.getinsured.hix.entity.assister.history.AssisterHistoryRendererImpl.UPDATED_DATE_COL;
import static com.getinsured.hix.entity.assister.history.AssisterHistoryRendererImpl.UPDATE_DATE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.entity.admin.repository.IEntityDocumentRepository;
import com.getinsured.hix.entity.admin.service.AssisterAdminService;
import com.getinsured.hix.entity.assister.history.AssisterHistoryRendererImpl;
import com.getinsured.hix.entity.assister.repository.IAssisterRepository;
import com.getinsured.hix.entity.util.EntityConstants;
import com.getinsured.hix.entity.util.EntityUtils;
import com.getinsured.hix.model.BrokerDocuments;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.platform.audit.service.DisplayAuditService;

/**
 * Implements {@link AssisterAdminService} to perform Entity Admin related
 * operations like find,save,update etc.
 */
@Service("assisterAdminService")
@Transactional
public class AssisterAdminServiceImpl implements AssisterAdminService, ApplicationContextAware {

	private static final String A_STATUS = "A.STATUS";

	private static final String LIKE = "LIKE";

	private static final int SIX = 6;

	private static final int FIVE = 5;

	private static final int FOUR = 4;

	private static final int THREE = 3;

	private static final String ACTIVITY_STATUS = "activityStatus";

	private static final String END_RECORD = "endRecord";

	private static final String START_LINE = "startLine";

	private static final String TO_DATE = "toDate";

	private static final String FROM_DATE = "fromDate";

	private static final String ENTITY_NAME = "entityName";

	private static final String CERTIFICATION_STATUS = "certificationStatus";

	private static final String ASSISTER_LAST_NAME = "assisterLastName";

	private static final String ASSISTER_FIRST_NAME = "assisterFirstName";
	
	private static final String ASSISTER_NUMBER = "assisterNumber";

	private static final Logger LOGGER = LoggerFactory.getLogger(AssisterAdminServiceImpl.class);

	private static final String IN_ACTIVE = "InActive";

	private static final String ACTIVE = "Active";
	
	private static final int SEVEN = 7;
	
	private static final int EIGHT = 8;
	
	@Autowired
	private ObjectFactory<DisplayAuditService> objectFactory;

	@Autowired
	private AssisterHistoryRendererImpl historyRendererService;

	@Autowired
	private IEntityDocumentRepository documentRepository;

	@PersistenceUnit
	private EntityManagerFactory emf;

	@Autowired
	private IAssisterRepository assisterRepository;

	private ApplicationContext applicationContext;

	private static final String DATE_STRING = "Date";
	private static final String NEW_CERTIFICATION_STATUS = "New Status";
	private static final String VIEW_COMMENT = "View Comment";
	private static final String VIEW_ATTACHMENT = "View Attachment";
	
	/**
	 * @see AssisterAdminService#loadAssisterStatusHistoryForAdmin(Integer)
	 * 
	 * @param assisterId
	 * 
	 * @return list of Assister Status History Map
	 */
	@Override
	public List<Map<String, Object>> loadAssisterStatusHistoryForAdmin(Integer assisterId) {
		

		LOGGER.info("loadAssisterStatusHistoryForAdmin: START");
		
		
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(UPDATED_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(STATUS_COL, NEW_CERTIFICATION_STATUS);
		requiredFieldsMap.put(COMMENT_COL, VIEW_COMMENT);
		requiredFieldsMap.put(ATTACHMENT_COL, VIEW_ATTACHMENT);

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(STATUS_COL);
		compareCols.add(COMMENT_COL);
		compareCols.add(ATTACHMENT_COL);
		//compareCols.add(UPDATED_DATE_COL);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(UPDATE_DATE);
		displayCols.add(PREVIOUS_STATUS);
		displayCols.add(NEW_STATUS);
		displayCols.add(COMMENTS);
		displayCols.add(ATTACHMENT);

		List<Map<String, Object>> data = null;
		try {
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(applicationContext);
			List<Map<String, String>> entityHistoryData = service.findRevisions(REPOSITORY_NAME, MODEL_NAME,
					requiredFieldsMap, assisterId);
			data = historyRendererService.processData(entityHistoryData, compareCols, displayCols);
		} catch (Exception e) {
			LOGGER.error("Exception occurred while loading Entity Status History" , e);
		}
		
		LOGGER.info("loadAssisterStatusHistoryForAdmin: END");
		
		return data;
	}

	/**
	 * @see AssisterAdminService#saveAssisterDocuments(BrokerDocuments)
	 * 
	 * @param assisterDocuments
	 *            {@link BrokerDocuments}
	 * 
	 * @return {@link BrokerDocuments}
	 */
	@Override
	public EntityDocuments saveAssisterDocuments(EntityDocuments assisterDocuments) {
		
		LOGGER.info("saveAssisterDocuments: START");
		LOGGER.info("saveAssisterDocuments: END");		
		
		return documentRepository.save(assisterDocuments);
	}

	/**
	 * @see AssisterAdminService#getAssisterDocumentsByRecordID(Integer)
	 * 
	 * @param assisterDocument
	 *            id
	 * 
	 * @return {@link BrokerDocuments}
	 */
	@Override
	public EntityDocuments getAssisterDocumentsByRecordID(Integer id) {
		
		LOGGER.info("getAssisterDocumentsByRecordID: START");
		LOGGER.info("getAssisterDocumentsByRecordID: END");
		
		return documentRepository.findOne(id);
	}

	/**
	 * @see AssisterAdminService#findAssister(Assister)
	 * 
	 * @param assister
	 *            {@link Assister}
	 * 
	 * @return list of Assisters
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Assister> findAssister(Assister assister) {
		
		LOGGER.info("findAssister: START");
		EntityManager em=null;
		List<Assister> assisterList = new ArrayList<Assister>();
		try {
			StringBuilder assisterFromEntity = new StringBuilder();
			assisterFromEntity
					.append("SELECT A.FIRST_NAME,A.LAST_NAME,E.ENTITY_NAME,A.CERTIFICATION_STATUS,A.ID,A.RECERTIFICATION_DATE FROM EE_ASSISTERS A,EE_ENTITIES E WHERE A.EE_ENTITY_ID =E.ID");

			EntityUtils.appendToSearchAssisterQuery(assisterFromEntity, "A.FIRST_NAME", assister.getFirstName(), ASSISTER_FIRST_NAME, LIKE, true, false);
			EntityUtils.appendToSearchAssisterQuery(assisterFromEntity, "A.LAST_NAME", assister.getLastName(), ASSISTER_LAST_NAME, LIKE, true, false);

			em = emf.createEntityManager();
			
			Query query = em.createNativeQuery(assisterFromEntity.toString());
			EntityUtils.setQueryParameter(query, ASSISTER_FIRST_NAME, assister.getFirstName(), true);
			EntityUtils.setQueryParameter(query, ASSISTER_LAST_NAME, assister.getLastName(), true);

			List<?> assisters = query.getResultList();
			createAssisterList(assisterList, assisters);
		} finally {
			if(em != null && em.isOpen()){
				em.close();
			}
		}
		LOGGER.info("findAssister: END");
		
		return assisterList;
	}

	private <E> void createAssisterList(List<Assister> assisterList, List<E> rsList) {
		
		LOGGER.info("createAssisterList: START");
		
		Iterator<E> rsIterator = rsList.iterator();

		while (rsIterator.hasNext()) {
			Object[] objArray = (Object[]) rsIterator.next();
			Assister a = new Assister();
			if (objArray[0] != null) {
				a.setFirstName(objArray[0].toString());

			}
			if (objArray[1] != null) {
				a.setLastName(objArray[1].toString());

			}
			if (objArray[2] != null) {
				EnrollmentEntity enrollmentEntity = new EnrollmentEntity();
				enrollmentEntity.setEntityName(objArray[2].toString());
				a.setEntity(enrollmentEntity);
			}
			if (objArray[THREE] != null) {
				a.setCertificationStatus(objArray[THREE].toString());
			}

			if (objArray[FOUR] != null) {
				a.setId(Integer.valueOf(objArray[FOUR].toString()));
			}
			if (objArray[FIVE] != null) {
				try {
					a.setReCertificationDate(EntityUtils.formatDate(new SimpleDateFormat(EntityConstants.DATE_FORMAT).format(new SimpleDateFormat(
							EntityConstants.DATE_TIME_FORMAT).parse(objArray[FIVE].toString()))));
				} catch (Exception exception) {
					LOGGER.error("Exception occured while formatting Date" , exception);
				}
			}
			assisterList.add(a);
		}

		LOGGER.info("createAssisterList: END");
	}

	/**
	 * @see AssisterAdminService#searchAssister(AssisterRequestDTO)
	 * 
	 * @param assisterRequestDTO
	 *            {@link AssisterRequestDTO}
	 * 
	 * @return list of Assisters
	 */
	@Override
	public List<Assister> searchAssister(AssisterRequestDTO assisterRequestDTO) {
		
		LOGGER.info("searchAssister: START");

		StringBuilder query = new StringBuilder();
		EntityManager em = null;
		List<Assister> assisterList = null;
		try{
			em = emf.createEntityManager();
			String sortBy = EntityUtils.isEmpty(assisterRequestDTO.getSortBy())?"FIRST_NAME":assisterRequestDTO.getSortBy();
			String sortOrder = EntityUtils.isEmpty(assisterRequestDTO.getSortOrder())?"ASC":assisterRequestDTO.getSortOrder();
	        
			if ("FIRST_NAME".equalsIgnoreCase(assisterRequestDTO.getSortBy()))
			{
				if (sortOrder.equalsIgnoreCase("ASC"))
				{
					sortBy = "FIRST_NAME, LAST_NAME"; 
				}
				else
				{
					sortBy = "FIRST_NAME DESC, LAST_NAME";
				}
			}
			
			query.append("SELECT ID,FIRST_NAME,LAST_NAME,ASSISTER_NUMBER,ENTITY_NAME,CERTIFICATION_STATUS,STATUS,RECERTIFICATION_DATE,LINE_NUMBER FROM "
					+ "(SELECT A.ID,A.FIRST_NAME,A.LAST_NAME,A.ASSISTER_NUMBER,E.ENTITY_NAME,A.CERTIFICATION_STATUS,A.STATUS,A.RECERTIFICATION_DATE, "
					+ " FROM EE_ASSISTERS A,EE_ENTITIES E WHERE A.EE_ENTITY_ID=E.ID");
			
			if(!EntityUtils.isEmpty(sortBy) && !EntityUtils.isEmpty(sortOrder)){
				query.insert(query.indexOf("A.RECERTIFICATION_DATE,") + "A.RECERTIFICATION_DATE,".length(), " ROW_NUMBER() OVER (ORDER BY " +sortBy+" "+sortOrder+ ") LINE_NUMBER ");
			}
			
			EntityUtils.appendToSearchAssisterQuery(query, "A.FIRST_NAME", assisterRequestDTO.getFirstName(), ASSISTER_FIRST_NAME, LIKE, true, false);
			EntityUtils.appendToSearchAssisterQuery(query, "A.LAST_NAME", assisterRequestDTO.getLastName(), ASSISTER_LAST_NAME, LIKE, true, false);
			EntityUtils.appendToSearchAssisterQuery(query, "A.ASSISTER_NUMBER", assisterRequestDTO.getAssisterNumber(), ASSISTER_NUMBER, LIKE, true, false);
			EntityUtils.appendToSearchAssisterQuery(query, "A.CERTIFICATION_STATUS", assisterRequestDTO.getCertificationStatus(), CERTIFICATION_STATUS, "=", false, false);

			if (assisterRequestDTO.getStatusInactive() == null || "".equals(assisterRequestDTO.getStatusInactive())) {
				EntityUtils.appendToSearchAssisterQuery(query, A_STATUS, assisterRequestDTO.getStatusactive(), ACTIVE, "=", false, false);
			}
			else if (assisterRequestDTO.getStatusactive() == null || "".equals(assisterRequestDTO.getStatusactive())) {
				EntityUtils.appendToSearchAssisterQuery(query, A_STATUS, assisterRequestDTO.getStatusInactive(), IN_ACTIVE, "=", false, false);
			}
			else {
				query.append(" AND A.STATUS IN (:"+ACTIVE+",:"+IN_ACTIVE+")");
			}

			EntityUtils.appendToSearchAssisterQuery(query, "E.ENTITY_NAME", assisterRequestDTO.getEntityName(), ENTITY_NAME, LIKE, true, false);
			
			appendDateConditionToQuery(assisterRequestDTO, query);

			query.append(" ORDER BY " +sortBy+" "+sortOrder+ " ) SUBQ1 WHERE LINE_NUMBER BETWEEN :" + START_LINE + " AND :" + END_RECORD);
			
			assisterList = new ArrayList<Assister>();
			Query searchQuery = em.createNativeQuery(query.toString());
			setQueryParameterForSearchAssister(assisterRequestDTO,searchQuery);
			List<?> list = searchQuery.getResultList();
			createAssistersList(assisterList, list);
			
		}catch(Exception e){
			LOGGER.error("Exception occurred while loading assisters", e);
		}finally{
			if(em != null && em.isOpen()){
				em.close();
			}
		}
	
		LOGGER.info("searchAssister: END");
		
		return assisterList;
	}

	/**
	 * @see AssisterAdminService#searchAssister(AssisterRequestDTO)
	 * 
	 * @param assisterRequestDTO
	 *            {@link AssisterRequestDTO}
	 * 
	 * @return list of Assisters
	 */
	@Override
		public List<Assister> searchAssisterForEntity(AssisterRequestDTO assisterRequestDTO) {
		
		LOGGER.info("searchAssister: START");
	
		StringBuilder query = new StringBuilder();
		EntityManager em = null;
		List<Assister> assisterList= null;
		try{
			em = emf.createEntityManager();
			String sortBy = EntityUtils.isEmpty(assisterRequestDTO.getSortBy())?"FIRST_NAME":assisterRequestDTO.getSortBy();
			String sortOrder = EntityUtils.isEmpty(assisterRequestDTO.getSortOrder())?"ASC":assisterRequestDTO.getSortOrder();
		
			if ("FIRST_NAME".equalsIgnoreCase(assisterRequestDTO.getSortBy()))
			{
				if (sortOrder.equalsIgnoreCase("ASC"))
				{
					sortBy = "FIRST_NAME, LAST_NAME"; 
				}
				else
				{
					sortBy = "FIRST_NAME DESC, LAST_NAME";
				}
			}
			
			query.append("SELECT ID,FIRST_NAME,LAST_NAME,ASSISTER_NUMBER,ENTITY_NAME,CERTIFICATION_STATUS,STATUS,RECERTIFICATION_DATE,CLIENT_COUNT,LINE_NUMBER FROM ");
			query.append("(SELECT Temp.ID,Temp.FIRST_NAME,Temp.LAST_NAME,Temp.ASSISTER_NUMBER,Temp.ENTITY_NAME,Temp.CERTIFICATION_STATUS,Temp.STATUS,Temp.RECERTIFICATION_DATE," 
			+"CLIENT_COUNT,ROW_NUMBER() OVER (ORDER BY " + sortBy + " " + sortOrder + ") as LINE_NUMBER ");
			
			query.append( " FROM (SELECT A.ID,A.FIRST_NAME,A.LAST_NAME,A.ASSISTER_NUMBER,E.ENTITY_NAME,A.CERTIFICATION_STATUS,A.STATUS,A.RECERTIFICATION_DATE, ");
			
			
			query.append("(SELECT COUNT(*) FROM EE_DESIGNATE_ASSISTERS DA WHERE DA.ASSISTER_ID = A.ID) ");
			query.append(" as CLIENT_COUNT ");
			query.append( " FROM EE_ASSISTERS A,EE_ENTITIES E WHERE A.EE_ENTITY_ID=E.ID");
			
			/*if(!EntityUtils.isEmpty(sortBy) && !EntityUtils.isEmpty(sortOrder)){
				query.insert(query.indexOf("A.RECERTIFICATION_DATE,") + "A.RECERTIFICATION_DATE,".length(), " ROW_NUMBER() OVER (ORDER BY " +sortBy+" "+sortOrder+ ") LINE_NUMBER ");
			}*/
			
			EntityUtils.appendToSearchAssisterQuery(query, "A.FIRST_NAME", assisterRequestDTO.getFirstName(), ASSISTER_FIRST_NAME, LIKE, true, false);
			EntityUtils.appendToSearchAssisterQuery(query, "A.LAST_NAME", assisterRequestDTO.getLastName(), ASSISTER_LAST_NAME, LIKE, true, false);
			EntityUtils.appendToSearchAssisterQuery(query, "A.ASSISTER_NUMBER", assisterRequestDTO.getAssisterNumber(), ASSISTER_NUMBER, LIKE, true, false);
			EntityUtils.appendToSearchAssisterQuery(query, "A.CERTIFICATION_STATUS", assisterRequestDTO.getCertificationStatus(), CERTIFICATION_STATUS, "=", false, false);
		
			if (assisterRequestDTO.getStatusInactive() == null || "".equals(assisterRequestDTO.getStatusInactive())) {
				EntityUtils.appendToSearchAssisterQuery(query, A_STATUS, assisterRequestDTO.getStatusactive(), ACTIVE, "=", false, false);
			}
			else if (assisterRequestDTO.getStatusactive() == null || "".equals(assisterRequestDTO.getStatusactive())) {
				EntityUtils.appendToSearchAssisterQuery(query, A_STATUS, assisterRequestDTO.getStatusInactive(), IN_ACTIVE, "=", false, false);
			}
			else {
				query.append(" AND A.STATUS IN (:"+ACTIVE+",:"+IN_ACTIVE+")");
			}
			query.append(" AND A.EE_ENTITY_ID="+ assisterRequestDTO.getEnrollmentEntityId());
			query.append(" ) Temp");
			EntityUtils.appendToSearchAssisterQuery(query, "E.ENTITY_NAME", assisterRequestDTO.getEntityName(), ENTITY_NAME, LIKE, true, false);
			
			appendDateConditionToQuery(assisterRequestDTO, query);
			query.append(" ) SUBQ3 WHERE LINE_NUMBER BETWEEN :" + START_LINE + " AND :" + END_RECORD); 
		
			assisterList = new ArrayList<Assister>();
			Query searchQuery = em.createNativeQuery(query.toString());
			setQueryParameterForNoOfAssisterBySearch(assisterRequestDTO,searchQuery);
			setQueryParameterForSearchAssister(assisterRequestDTO,searchQuery); 
			
			List<?> list = searchQuery.getResultList();
			createAssistersList(assisterList, list);
		
		}catch(Exception e){
			LOGGER.error("Exception occurred while loading assisters for entity", e);
		}finally{
			if(em != null && em.isOpen()){
				em.close();
			}
		}

		LOGGER.info("searchAssister: END");
		return assisterList;
	}
	
	public int findTotalNoOfAssistersForEntity(int enrollmentEntityId) {
		
		LOGGER.info("findAssistersForEnrollmentEntity: START");

		int recordCount = 0;
		EntityManager em = null;
		
		try{
			em = emf.createEntityManager();
			StringBuilder query = new StringBuilder();
			
			query.append("SELECT COUNT(*) FROM EE_ASSISTERS A WHERE A.EE_ENTITY_ID = :enrollmentEntityId");
			
			Query pQuery = em.createNativeQuery(query.toString());
			pQuery.setParameter("enrollmentEntityId", enrollmentEntityId);
			List list= pQuery.getResultList();
			
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object obj = iterator.next();
				if (obj != null) {
					recordCount = Integer.valueOf(obj.toString());
				}
				break;
			}
		}catch(Exception e){
			LOGGER.error("Exception occurred while loading total no of assisters", e);
		}finally{
			if(em != null && em.isOpen()){
				em.close();
			}
		}
		
		LOGGER.info("findAssistersForEnrollmentEntity: END");

		return recordCount;
	}

	/**
	 * @param assisterRequestDTO
	 * @param query
	 */
	private void appendDateConditionToQuery(
			AssisterRequestDTO assisterRequestDTO, StringBuilder query) {
		String fromDate = assisterRequestDTO.getFromDate();
		String toDate = assisterRequestDTO.getToDate();
		if (fromDate != null && !"".equals(fromDate)) {
			query.append(" AND A.RECERTIFICATION_DATE >=to_date(:" + FROM_DATE + ",'mm-dd-yyyy')");
		}
		if (toDate != null && !"".equals(toDate)) {
			query.append(" AND A.RECERTIFICATION_DATE <= to_date(:" + TO_DATE + ",'mm-dd-yyyy') + 1");
		}
	}
	
	private void setQueryParameterForSearchAssister(AssisterRequestDTO assisterRequestDTO,Query searchQuery ){
		
		Integer startRecord = assisterRequestDTO.getStartRecord();
		Integer startLine = startRecord + 1;
		Integer endRecord = startRecord + assisterRequestDTO.getPageSize();
		setQueryParameterForNoOfAssisterBySearch(assisterRequestDTO, searchQuery);
		searchQuery.setParameter(START_LINE,startLine);
		searchQuery.setParameter(END_RECORD,endRecord);
		
	}

	private void createAssistersList(List<Assister> assisterList, List rsList) {
		
		LOGGER.info("createAssistersList: START");
		
		Iterator rsIterator = rsList.iterator();
		while (rsIterator.hasNext()) {
			Object[] objArray = (Object[]) rsIterator.next();
			Assister a = new Assister();
			if (objArray[0] != null) {
				a.setId(Integer.valueOf(objArray[0].toString()));
			}
			if (objArray[1] != null) {
				a.setFirstName(objArray[1].toString());
			}
			if (objArray[2] != null) {
				a.setLastName(objArray[2].toString());
			}
			Long assisterNumber = objArray[THREE] != null || !objArray[THREE].toString().isEmpty() ? Long.valueOf(objArray[THREE].toString()) : 0;
			a.setAssisterNumber(assisterNumber);
			if (objArray[FOUR] != null) {
				EnrollmentEntity enrollmentEntity = new EnrollmentEntity();
				enrollmentEntity.setEntityName(objArray[FOUR].toString());
				a.setEntity(enrollmentEntity);
			}
			if (objArray[FIVE] != null) {
				a.setCertificationStatus(objArray[FIVE].toString());
			}
			if (objArray[SIX] != null) {
				a.setStatus(objArray[SIX].toString());
			}
			if (objArray[SEVEN] != null) {
				try {

					a.setReCertificationDate(EntityUtils.formatDate(new SimpleDateFormat(EntityConstants.DATE_FORMAT).format(new SimpleDateFormat(
							EntityConstants.DATE_TIME_FORMAT).parse(objArray[SEVEN].toString()))));

				} catch (Exception exception) {
					LOGGER.error("Exception occured while formatting Date" , exception);
				}
			}
			if (objArray[EIGHT] != null) {
				a.setClientCount(objArray[EIGHT].toString());
			}
			assisterList.add(a);
		}

		LOGGER.info("createAssistersList: END");
	}

	/**
	 * @see AssisterAdminService#totalNoOfAssistersBySearch(AssisterRequestDTO)
	 * 
	 * @param assisterRequestDTO
	 *            {@link AssisterRequestDTO}
	 * 
	 * @return no of Assisters by given search criteria
	 */
	@Override
	public int totalNoOfAssistersBySearch(AssisterRequestDTO assisterRequestDTO) {
		
		LOGGER.info("totalNoOfAssistersBySearch: START");

		StringBuilder query = new StringBuilder();
		EntityManager em = null;
		int count = 0;
		try{
			em = emf.createEntityManager();
			query.append("SELECT COUNT(*) from EE_ASSISTERS A,EE_ENTITIES E WHERE A.EE_ENTITY_ID=E.ID");

			EntityUtils.appendToSearchAssisterQuery(query, "A.FIRST_NAME", assisterRequestDTO.getFirstName(), ASSISTER_FIRST_NAME, LIKE, true, false);
			EntityUtils.appendToSearchAssisterQuery(query, "A.LAST_NAME", assisterRequestDTO.getLastName(), ASSISTER_LAST_NAME, LIKE, true, false);
			EntityUtils.appendToSearchAssisterQuery(query, "A.ASSISTER_NUMBER", assisterRequestDTO.getAssisterNumber(), ASSISTER_NUMBER, LIKE, true, false);
			EntityUtils.appendToSearchAssisterQuery(query, "A.CERTIFICATION_STATUS", assisterRequestDTO.getCertificationStatus(), CERTIFICATION_STATUS, "=", false, false);

			if (assisterRequestDTO.getStatusInactive() == null || "".equals(assisterRequestDTO.getStatusInactive())) {
				EntityUtils.appendToSearchAssisterQuery(query, A_STATUS, assisterRequestDTO.getStatusactive(), ACTIVE, "=", false, false);
			}
			else if (assisterRequestDTO.getStatusactive() == null || "".equals(assisterRequestDTO.getStatusactive())) {
				EntityUtils.appendToSearchAssisterQuery(query, A_STATUS, assisterRequestDTO.getStatusInactive(), IN_ACTIVE, "=", false, false);
			}
			else {
				query.append(" AND A.STATUS IN (:"+ACTIVE+",:"+IN_ACTIVE+")");
			}

			EntityUtils.appendToSearchAssisterQuery(query, "E.ENTITY_NAME", assisterRequestDTO.getEntityName(), ENTITY_NAME, LIKE, true, false);
			
			appendDateConditionToQuery(assisterRequestDTO, query);

			Query parameterizedQuery = em.createNativeQuery(query.toString());
			setQueryParameterForNoOfAssisterBySearch(assisterRequestDTO,parameterizedQuery);
			count = ((Number)parameterizedQuery.getSingleResult()).intValue();
			
		}catch(Exception e){
			LOGGER.error("Exception occurred while loading total no of assisters", e);
		}finally{
			if(em != null && em.isOpen()){
				em.close();
			}
		}
		
		LOGGER.info("totalNoOfAssistersBySearch: END");
		
		return count;
	}
	
	
	private void setQueryParameterForNoOfAssisterBySearch(AssisterRequestDTO assisterRequestDTO,Query searchQuery ){
		
		EntityUtils.setQueryParameter(searchQuery, ASSISTER_FIRST_NAME, assisterRequestDTO.getFirstName(), true);
		EntityUtils.setQueryParameter(searchQuery, ASSISTER_LAST_NAME, assisterRequestDTO.getLastName(), true);
		EntityUtils.setQueryParameter(searchQuery, ASSISTER_NUMBER, assisterRequestDTO.getAssisterNumber(), false);
		EntityUtils.setQueryParameter(searchQuery, CERTIFICATION_STATUS, assisterRequestDTO.getCertificationStatus(), false);

		String statusActive = assisterRequestDTO.getStatusactive();
		String statusInActive = assisterRequestDTO.getStatusInactive();

		if (assisterRequestDTO.getStatusInactive() == null || "".equals(assisterRequestDTO.getStatusInactive())) {
			EntityUtils.setQueryParameter(searchQuery, ACTIVE, assisterRequestDTO.getStatusactive(), false);
		}
		else if (assisterRequestDTO.getStatusactive() == null || "".equals(assisterRequestDTO.getStatusactive())) {
			EntityUtils.setQueryParameter(searchQuery, IN_ACTIVE, assisterRequestDTO.getStatusInactive(), false);
		}
		else {
			searchQuery.setParameter(ACTIVE,statusActive);
			searchQuery.setParameter(IN_ACTIVE,statusInActive);
		}
		EntityUtils.setQueryParameter(searchQuery, ENTITY_NAME, assisterRequestDTO.getEntityName(), true);

		String fromDate = assisterRequestDTO.getFromDate();
		String toDate = assisterRequestDTO.getToDate();
		if (fromDate != null && !"".equals(fromDate)) {
			searchQuery.setParameter(FROM_DATE,fromDate);
		}
		if (toDate != null && !"".equals(toDate)) {
			searchQuery.setParameter(TO_DATE,toDate);
		}

	}
	@Override
	public int findTotalNoOfAssisters() {
		
		LOGGER.info("findTotalNoOfAssisters: START");
		
		int recordCount = 0;
		EntityManager em = null;
		try{
			em = emf.createEntityManager();
			StringBuilder query = new StringBuilder();
			query.append("SELECT COUNT(*) FROM EE_ASSISTERS");

			List list = em.createNativeQuery(query.toString()).getResultList();

			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object obj = iterator.next();
				if (obj != null) {
					recordCount = Integer.valueOf(obj.toString());
				}
				break;
			}
		}catch(Exception e){
			LOGGER.error("Exception occurred while loading total no of assisters", e);
		}finally{
			if(em != null && em.isOpen()){
				em.close();
			}
		}

		LOGGER.info("findTotalNoOfAssisters: END");
		
		return recordCount;
	}

	@Override
	public int findAssistersByStatus(String activityStatus) {
		
		LOGGER.info("findAssistersByStatus: START");
		
		int recordCount = 0;
		EntityManager em = null;
		try{
			em = emf.createEntityManager();
			StringBuilder query = new StringBuilder();
			query.append("SELECT COUNT(*) FROM EE_ASSISTERS E WHERE E.STATUS = :" + ACTIVITY_STATUS);

			Query parameterizedQuery = em.createNativeQuery(query.toString());
			parameterizedQuery.setParameter(ACTIVITY_STATUS, activityStatus);
			List list = parameterizedQuery.getResultList();
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object obj = iterator.next();
				if (obj != null) {
					recordCount = Integer.valueOf(obj.toString());
				}
				break;
			}
		}catch(Exception e){
			LOGGER.error("Exception occurred while loading total no of assisters", e);
		}finally{
			if(em != null && em.isOpen()){
				em.close();
			}
		}
		
		LOGGER.info("findAssistersByStatus: END");
		
		return recordCount;
	}

	/**
	 * @see AssisterAdminService#searchAssister(AssisterRequestDTO)
	 * 
	 * @param recordId
	 *            record id of the Assister
	 * 
	 * @return {@link Assister}
	 */
	@Override
	public Assister getAssisterByRecordID(int recordId) {
		
		LOGGER.info("findAssistersByStatus: START");
		LOGGER.info("findAssistersByStatus: END");
		
		return assisterRepository.findById(recordId);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		
	}

}
