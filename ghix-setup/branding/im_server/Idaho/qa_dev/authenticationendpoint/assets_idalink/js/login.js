/**
 * Created by michael on 9/8/14.
 */
'use strict';

$(document).ready(function() {
	$('#loginForm').submit(function(form) {
		$('#username').val($('#username').val().toLowerCase());
	});

	$('.help-topic').click(function() {
		if($ (this).next().hasClass('open') ){
			$(this).next().removeClass('open');
		}
		else{
			$('.help-questions').removeClass('open');
			$(this).next().addClass('open');
		}
	});


	$('.help-toggle').click(function() {
		$('section.help').removeClass('hidden');
		$('.reEvalFormOverlay').removeClass('hidden');
		$('body').addClass('modal-open');
	});

	$('.reEvalFormOverlay').click(function() {
		$('section.help').addClass('hidden');
		$('.reEvalFormOverlay').addClass('hidden');
		$('body').removeClass('modal-open');
	});

	$('.close').click(function() {
		$('section.help').addClass('hidden');
		$('.reEvalFormOverlay').addClass('hidden');
		$('body').removeClass('modal-open')
	});
});