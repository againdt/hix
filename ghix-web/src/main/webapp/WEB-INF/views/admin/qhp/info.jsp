<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<div class="gutter10">
	<div class="row-fluid">
		<ul class="page-breadcrumb">
	       	<li><a href="javascript:history.back()">&lt; <spring:message  code="label.back"/></a></li>
	       	<li><a href="<c:url value="/admin/planmgmt/manageqhpplans"/>"><spring:message  code="label.healthplansTitle"/></a></li>
	        <li><a href="#"><spring:message code='label.addnewQHPapplication'/></a></li>
	        <li><spring:message  code='label.provideplandetails'/></li>
	    </ul><!--page-breadcrumb ends-->
	</div>

    	<div class="row-fluid">
            <div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><a name="skip"></a><spring:message  code='label.addnewQHPapplication'/></h4>
                </div>
                <!--  beginning of side bar -->
                <ol class="nav nav-list">
                    <li class="active"><a href="#"><spring:message  code='label.provideplandetails'/></a></li>
                    <li class="link"><a href="#"><spring:message  code='label.uploadplanbenefits'/></a></li>
                    <li class="link"><a href="#"><spring:message  code='label.uploadplanrates'/></a></li>
                    <li class="link"><a href="#"><spring:message  code='label.selectoraddprovidernetworks'/></a></li>
                    <!--<li><a href="#"><spring:message  code='label.addcostsharingvariations'/></a></li>-->
                    <li class="link"><a href="#"><spring:message  code='label.reviewandconfirm'/></a></li>
                </ol>
                <!-- end of side bar -->
            </div>
            <!-- end of span3 -->
		
		<div class="span9" id="rightpanel">
		<div class="header">
					<h4 class="margin0"><spring:message code='label.step1'/> <spring:message  code='label.provideplandetails'/></h4>
		</div>
			<form class="form-horizontal marginTop10" id="frmqhpinfo" name="frmqhpinfo"
				action="<c:url value="/admin/planmgmt/submitqhp" />" method="POST">
				<input type="hidden" id="id" name="id" value="${plan.id}"/>
				<df:csrfToken/>
					<div class="control-group">
						<label for="planName" class="required control-label"><spring:message  code='label.issuerName'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<div id="issuername" class="input">
								<!--<input id="issuername" name="issuer.name" class="input-xlarge" type="text">-->
								<div id="issuername_error" class="help-inline"></div>
								</div>
							</div> <!-- end of controls-->
					</div>
					<!-- end of control-group -->
					<div class="control-group">
						<label for="name" class="required control-label"><spring:message  code="label.planName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input id="name" name="name"  class="input-xlarge" type="text" value="${plan.name}" />
								<div id="name_error" class="help-inline"></div>
							</div> <!-- end of controls-->
					</div>
					<!-- end of control-group -->
					<div class="control-group">
						<label for="issuerPlanNumber" class="required control-label"><spring:message  code="label.planNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input id="issuerPlanNumber" name="issuerPlanNumber"   class="input-xlarge" type="text" value="${plan.issuerPlanNumber}">
								<div id="issuerPlanNumber_error" class="help-inline"></div>
							</div> <!-- end of controls-->
					</div>
					<!-- end of control-group -->
                    <div class="control-group">
                        <label for="market" class="required control-label"><spring:message  code="label.planMarket"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <select class="input-medium" name="market" id="market">									
									 <c:forEach var="qhpmarket" items="${qhpmarketList}">
								    	<option <c:if test="${plan.market==qhpmarket.key}">selected</c:if> value="${qhpmarket.key}">${qhpmarket.value}</option>
									</c:forEach>
								</select>
                                <div id="planMarket_error" class="help-inline"></div>
                            </div> <!-- end of controls-->
                    </div>
                    <!-- end of control-group -->
					<div class="control-group">
						<label for="planHealthplanLevel" class="required control-label"><spring:message  code="label.planLevel"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select class="input-medium" name="planHealth.planLevel" id="planHealthplanLevel">
									<option value=""><spring:message  code="label.choose"/></option>									
									<c:forEach var="qhplevel" items="${qhplevelList}">
								    	<option <c:if test="${plan.planHealth.planLevel==qhplevel.key}">selected</c:if> value="${qhplevel.key}">${qhplevel.value}</option>
									</c:forEach>
									
								</select>
                    			<input type="hidden" id="planLevelReadOnly" name="planLevelReadOnly" value="${plan.planHealth.planLevel}" />
								<div id="planHealthplanLevel_error" class="help-inline"></div>
							</div> <!-- end of controls-->
					</div>
					<div class="control-group">
						<label for="planHealthEhbCoveredLevel" class="required control-label"><spring:message  code="label.ehbCovered"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select class="input-medium" name="planHealth.ehbCovered" id="planHealthEhbCoveredLevel">
									<option value=""><spring:message  code="label.choose"/></option>									
									<c:forEach var="ehbCovered" items="${ehbCoveredList}">
								    	<option <c:if test="${plan.planHealth.ehbCovered==ehbCovered.key}">selected</c:if> value="${ehbCovered.key}">${ehbCovered.value}</option>
									</c:forEach>									
								</select>      
								<input type="hidden" id="planEhbCoveredReadOnly" name="planEhbCoveredReadOnly" value="${plan.planHealth.ehbCovered}" />              			
								<div id="planHealthEhbCoveredLevel_error" class="help-inline"></div>
							</div> <!-- end of controls-->
					</div>
					<!-- end of control-group -->
					<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.startDate}" var="stDate"/>
					<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.endDate}" var="enDate"/>
                    
                    <div class="control-group">
						<label for="hsa" class="required control-label"><spring:message  code="label.hsa"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select class="input-medium" id="hsa" name="hsa">
								<option value=""><spring:message  code="label.choose"/></option>									
								<c:forEach var="qhpHsa" items="${qhpHsaList}">
									<option <c:if test="${plan.hsa==qhpHsa.key}">selected</c:if> value="${qhpHsa.key}">${qhpHsa.value}</option>
								</c:forEach>
								</select>
								<div id="hsa_error" class="help-inline"></div>
							</div> 
					</div><!-- end of control-group -->
                    
					<div class="control-group">
						<label for="startDate" class="required control-label"><spring:message  code="label.planStartDate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" title="MM/dd/yyyy" class="datepick input-small" name="startDate" id="startDate" readonly="true" value="<c:out value="${stDate}"/>" />
								<div id="startDate_error" class="help-inline"></div>
							</div> <!-- end of controls-->
					</div>
					<!-- end of control-group -->
					<div class="control-group">
						<label for="endDate" class="control-label"><spring:message  code="label.planEndDate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" title="MM/dd/yyyy" class="datepick input-small" name="endDate" id="endDate" readonly="true" value="<c:out value="${enDate}"/>"/>
								<div id="endDate_error" class="help-inline"></div>
							</div> <!-- end of controls-->
					</div>					
					
					<!-- end of control-group -->
					<div class="control-group">
						<label for="formularlyId" class="required control-label"><spring:message  code="label.formularlyId"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input id="formularlyId" name="formularlyId"  class="input-xlarge" type="text" value="${plan.formularlyId}" />
								<div id="formularlyId_error" class="help-inline"></div>
							</div> <!-- end of controls-->
					</div>				
					<!-- end of control-group -->
					
					<div class="control-group">
						<label for="formularlyUrl" class="required control-label"><spring:message  code="label.formularlyURL"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input id="formularlyUrl" name="formularlyUrl"  class="input-xlarge" type="text" value="${plan.formularlyUrl}" />
								<div id="formularlyUrl_error" class="help-inline"></div>
							</div> <!-- end of controls-->
					</div>				
					<!-- end of control-group -->
										
					<!-- 
					Remove Max enrollment field 
					<div class="control-group"> 
						<label for="planName" class="required control-label"><spring:message  code="label.maximumEnrollment"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input id="maxEnrolee" name="maxEnrolee"  class="input-mini" maxlength="10" type='text' value="${plan.maxEnrolee}">
								<div id="maxEnrolee_error" class="help-inline"></div>
							</div> 
					</div>-->
					<input type="hidden" name="planHealth.acturialValueCertificate" id="planHealthacturialValueCertificate" value="${planHealthacturialValueCertificate}" />
					<!-- end of control-group -->
					
				</form>
					<form class="form-horizontal" id="frmCerti" name="frmCerti" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
						<div class="control-group">
							<label class="required control-label" for="certificates"><spring:message code="label.attach"/> <spring:message  code="label.valueCertificate"/></label>
								<div class="controls">
										<input type="file" id="certificates" name="certificates"  class="input-file">
										&nbsp; <button class="btn"><spring:message code="label.upload"/></button>
										<input type="hidden" name="fileType" value="certificates"/>
										<div id="certificatesFileName" class="help-inline">${acturialValueCertificateOrig}</div>
										<div id="planHealthacturialValueCertificate_error" class=""></div>
									
									<!--<input type="file" id="fileInput" class="input-file">
									&nbsp;
									<button class="btn">Upload</button>
									<div id="firstName_error" class="help-inline"></div>-->
								</div> <!-- end of controls-->
						</div>
						<!-- <div class="control-group">
							<label for="firstName" class="required control-label"
								for="fileInput">Plan Brochure</label>
								<div class="controls">
										<input type="file" id="planBrochure" name="planBrochure"  class="input-file">
										&nbsp; <button class="btn">Upload</button>
										<input type="hidden" name="fileType" value="certificates"/>
										<div id="planBrochureName" class="help-inline"> </div>
										<div id="planBrochure_error" class=""></div>
								</div> end of controls
						</div> -->
						
						<!-- <div class="control-group">
							<label for="firstName" class="required control-label"
								for="fileInput">Plan Brochure</label>
								<div class="controls">
										<input type="file" id="planbrochure" name="planbrochure"  class="input-file">
										&nbsp; <button class="btn">Upload</button>
										<input type="hidden" name="fileType" value="planbrochure"/>
										<div id="planbrochure" class="help-inline">${planbrochure}</div>
										<div id="planbrochure_error" class=""></div>
								</div> 
						</div>-->
						
						<div class="form-actions">
								<button type="button" title="<spring:message  code="label.saveAndContinue"/>" name="submitBtn" id="submitBtn" class="btn btn-primary" onclick="isExistingPlan()"> <spring:message  code='label.saveAndContinue'/> </button>
							</div>
					</form>
					<!-- end of control-group -->
					
			</div>
			<!-- end of span9 -->				
				
				<!-- end of form-actions -->
                </div>
	</div>

<!-- end of row-fluid -->

	
	
	<script type="text/javascript">
		$(function() {
			$('.datepick').each(function() {
				var ctx = "${pageContext.request.contextPath}";
				var imgpath = ctx+'/resources/images/calendar.gif';
				$(this).datepicker({
					showOn : "button",
					buttonImage : imgpath,
					buttonImageOnly : true,
					minDate : 0
				});
			});
		});
	</script>


<!--  end of row-fluid -->
<script type="text/javascript">

function  formSubmit()
{
	$("#frmqhpinfo").submit();
}
var oldPlanName='${plan.name}';
var oldIssuerId ='${plan.issuer.id}';
$(document).ready(function(){
	$.ajaxSetup({ cache: false }); // prevent Ajax caching in JQuery 
	
	getIssuerNames();
	
	var accreditationUpload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitAccreditationRequest,  // pre-submit callback 
	        success:       submitAccreditationResponse  // post-submit callback 
	    };  
    $('#frmCerti').ajaxForm(accreditationUpload);
    if(('${plan.id}' >0) && '${plan.planHealth.planLevel}' == 'Silver'  )
        {
    		$("#planHealthplanLevel").val('${plan.planHealth.planLevel}'); 
   			$("#planHealthplanLevel").attr("disabled", true);
        }
});


function submitAccreditationResponse(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	if(val[0] != 'Error')
	{	
		$("#certificatesFileName").text(val[1]);
		$("#planHealthacturialValueCertificate").val(val[2]);
		$("#planHealthacturialValueCertificate_error").val("");
	}
	else
	{
		$("#planHealthacturialValueCertificate_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
	}
}

function submitAccreditationRequest(formData, jqForm, options) { 
	var fileName = $('#certificates').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}

function getIssuerNames()
{
	var ctx = "${pageContext.request.contextPath}";
	var imgpath = ctx+'/resources/images/loader.gif';
	$("#issuername").html("<div class=\"issuerloader\"><img src=\"" + imgpath + "\" alt='Issuer loader'></div>");
	$.ajax({
		  type: 'GET',
		  url: "getissuers",
		  success: function(data) {
			 	$("#issuername").html("");
				$("#issuername").html(data);
				$("#issuerId").val('${plan.issuer.id}');
				
					var el = document.getElementById("issuerId");
					for(var i=0; i<el.options.length; i++) {
					  if ( el.options[i].value == '${plan.issuer.id}' ) {
					    el.selectedIndex = i;
					    break;
					  }
					}


            }	  
		});
}

function isExistingPlan()
{
	if($("#frmqhpinfo").valid())
	{
	var ctx = "${pageContext.request.contextPath}";
	if($('#issuerId').val() == oldIssuerId && oldPlanName == $('#name').val())
	{
		formSubmit();
	}
	else
	{
	$.ajax({
		  type: 'GET',
		  url: "isExistingPlan",
		  data:{"issuerId":$('#issuerId').val(),"name":$('#name').val(),"oldPlanName":oldPlanName},
		  success: function(data) {
			 	if(data=='true')
			 	{
			 		$("#name_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.message'/></span></label>");
					$("#name_error").attr('class','error help-inline');
			 	}
			 	else
			 	{
			 		formSubmit();
			 	}
				
            }	  
		});
	}
	}
}
</script>
<script type="text/javascript">
$.validator.addMethod("greaterThanToday", function(value, element) {
	var startdatevalue = '${currDate}';
	if (value == null || value.length <1)
		return true;
	return Date.parse(startdatevalue) <= Date.parse(value);
	});
	
$.validator.addMethod("endDate", function(value, element) {
var startdatevalue = $('#startDate').val();
if (value == null || value.length <1)
	return true;
return Date.parse(startdatevalue) < Date.parse(value);
});
$.validator.addMethod("numericRegex", function(value, element) {
    return this.optional(element) || /^[0-9]+$/i.test(value);
});
$.validator.addMethod("alphanumericRegex", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
});

$.validator.addMethod("planLevel", function(value, element) {
	return !(value == null || value.length <1);
});
$.validator.addMethod("ehbCovered", function(value, element) {
	return !(value == null || value.length <1);
});

$.validator.addMethod("maxlength", function (value, element, len) {
	   return value == "" || value.length <= len;
	});

var validator = $("#frmqhpinfo").validate({ 
	 ignore: "",
	rules : {issuername : { required : true},
		name : { required : true,
			maxlength : '200'},
		issuerPlanNumber : { required : true,
			maxlength : '30'},
		startDate : { required : true,greaterThanToday : true},
		endDate : { endDate : true,greaterThanToday : true},
		hsa : {	required : true},
		formularlyId : {required: true,alphanumericRegex:true,maxlength : '150'},
		formularlyUrl : {required: true,maxlength : '200',url : true},
		"planHealth.planLevel" : {planLevel : true},
		"planHealth.ehbCovered" : {ehbCovered : true},
		"planHealth.acturialValueCertificate" : { required : true}
		
	}, 
	messages : {
		issuername : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.issuerName'/></span>"},
		name : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.planName'/></span>",
			maxlength : "<span> <em class='excl'>!</em> <spring:message  code='label.planName'/> exceeds Maximum length.</span>"},
		issuerPlanNumber : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.planNumber'/></span>",
		maxlength : "<span> <em class='excl'>!</em> <spring:message  code='label.planName'/> exceeds Maximum length.</span>"},
		hsa : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showSelect'/> <spring:message  code='label.hsa'/></span>" },
		startDate : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.planStartDate'/></span>"
			,greaterThanToday : "<span> <em class='excl'>!</em><spring:message  code='label.planStartDate'/> <spring:message  code='alert.greaterThanToday'/></span>"},
		endDate : { endDate : "<span> <em class='excl'>!</em><spring:message  code='label.validateEndDate'/></span>"
			,greaterThanToday : "<span> <em class='excl'>!</em><spring:message  code='label.planEndDate'/> <spring:message  code='alert.greaterThanToday'/></span>"},
		formularlyId : {required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.formularlyId'/></span>"
			,alphanumericRegex:"<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='err.alphaNumericValue'/></span>"},
		formularlyUrl : {required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.formularlyURL'/></span>"
			,url:"<span><em class='excl'>!</em><spring:message  code='label.showEnter'/><spring:message  code='err.invalidURL'/></span>"},
		"planHealth.planLevel" : { planLevel : "<span> <em class='excl'>!</em><spring:message  code='label.showSelect'/><spring:message  code='label.planLevel'/></span>"},
		"planHealth.ehbCovered" : { ehbCovered : "<span> <em class='excl'>!</em><spring:message  code='label.showSelect'/><spring:message  code='label.ehbCovered'/></span>"},
		"planHealth.acturialValueCertificate" : { required : "<span> <em class='excl'>!</em>Upload <spring:message  code='label.valueCertificate'/></span>"}
		
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		if(elementId == 'planHealthacturialValueCertificate')
			{
			$("#" + elementId + "_error").html("");
			}
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
		
	} 
});
jQuery("#frmCerti").validate({});
</script>
