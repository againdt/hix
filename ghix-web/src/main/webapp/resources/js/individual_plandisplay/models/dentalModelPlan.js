(function (models) {
	models.dentalModelPlan = Backbone.Model.extend({
		
		urlRoot : "getDentalModelPlans",
		
		url : function () {
			if(this.id)
				return 'getDentalModelPlans/'+this.id+'?'+(Math.random()*10);
			else
				return 'getDentalModelPlans?'+(Math.random()*10);
		},
		
		//to specify default attributes for model
		defaults: {
			//attrName : default value
			"id"		: "",
			"planId"	: "",
			"name"		: "",
			"level"		: "",
			"premium"	: "",
			"premiumBeforeCredit" : "",
			"premiumAfterCredit" : "",
			"aptc" 			: "",
			"issuer"		: "",
			"issuerText"	: "",
			"networkType"	: "",
			"deductible"	: "",
			"groupId"		: "",
			"groupName"		: "",
			"groupMembers"	: "",
			"orderItemId"	: "",
			"providerLink" : "",
			"planDetailsByMember"	: "",
			"ISSUBSIDIZED" : "",
			"totalContribution" : "",
			"issuerLogo" : ""
		}
	}); 
})(App.models);