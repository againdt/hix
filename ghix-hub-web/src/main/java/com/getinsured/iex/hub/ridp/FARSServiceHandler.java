package com.getinsured.iex.hub.ridp;

import javax.xml.bind.JAXBElement;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.Parameter;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.messaging.GIJson;
import com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.exchange._1.ObjectFactory;
import com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.extension._1.ResponsePayloadType;
import com.getinsured.iex.hub.ridp.fars.wrapper.FarsRequestPayloadTypeWrapper;
import com.getinsured.iex.hub.ridp.fars.wrapper.FarsResponsePayloadTypeWrapper;
import com.getinsured.ridpv2.RIDPNamespaceMapper;

/**
 * Service handler for FARS Service
 * 
 * @author Nikhil Talreja
 * @since 20-Mar-2014
 *
 */
public class FARSServiceHandler extends AbstractServiceHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(FARSServiceHandler.class);
	private Jaxb2Marshaller marshaller;
	private static ObjectFactory factory = new ObjectFactory();
	
	public Object getRequestpayLoad() throws HubServiceException {
		return getPayLoadFromJSONInput("dshReferenceNumber",getJsonInput());
	}

	public Object getPayLoadFromJSONInput(String jsonSectionKey, String jsonStr) throws HubServiceException{
		
		FarsRequestPayloadTypeWrapper requestWrapper = new FarsRequestPayloadTypeWrapper();
		
		Object dhsRefNum = null;
		String dshReferenceNumber = null;
		try {
			GIJson mainJson = GIJson.getGIJson(jsonStr);
			dhsRefNum = mainJson.getValueForKey("dshReferenceNumber", jsonSectionKey);
			if(dhsRefNum == null){
				throw new HubServiceException("DHS Reference number is mandatory for FARS Service");
			}
			dshReferenceNumber = (String)dhsRefNum;
			Parameter target = this.getTargetParameter("dshReferenceNumber", FarsRequestPayloadTypeWrapper.class.getName());
			if(target != null){
				target.setValue(requestWrapper, dshReferenceNumber);
			}
		} catch (ParseException e) {
			logger.error("Invalid JSON:"+jsonStr);
			throw new HubServiceException(e);
		} catch (HubMappingException e) {
			throw new HubServiceException(e);
		}
		
		return factory.createRequest(requestWrapper.getRequest());
	}
	
	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		
		JAXBElement<ResponsePayloadType> resp= (JAXBElement<ResponsePayloadType>)responseObject;
		ResponsePayloadType payload = resp.getValue();
		
		String message = null;
		
		if(payload != null){
			FarsResponsePayloadTypeWrapper response = new FarsResponsePayloadTypeWrapper(payload);
			if(response != null){
				this.requestStatus = RequestStatus.SUCCESS;
				message = response.toJSONString();
				logger.debug("Response : " + message);
				if(response.getResponseMetadata() != null){
					this.setResponseCode(response.getResponseMetadata().getResponseCode());
				}
			}
		}
		
		return message;
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}
	
	@Override
	public String getServiceIdentifier() {
		return "H66.1";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.fars2.cms.dsh.ridp.exchange._1",new RIDPNamespaceMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.fars2.cms.dsh.ridp.exchange._1",new RIDPNamespaceMapper());
		}
		return this.marshaller;
	}

}
