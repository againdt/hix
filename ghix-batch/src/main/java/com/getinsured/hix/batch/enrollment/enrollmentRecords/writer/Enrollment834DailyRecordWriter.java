package com.getinsured.hix.batch.enrollment.enrollmentRecords.writer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.enrollmentRecords.IssuerFileList;
import com.getinsured.hix.enrollment.service.EnrollmentBatchService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;

@Component("Enrollment834DailyRecordWriter")
@Scope("step")
public class Enrollment834DailyRecordWriter implements ItemWriter<Enrollment>{
	private EnrollmentBatchService enrollmentBatchService ;
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment834DailyRecordWriter.class);
	
	private IssuerFileList issuerFileList;
	Date startDate=null;
	Date endDate=null;
	String carrierResendFlag=null;
	private ExecutionContext executionContext;
	private ExecutionContext stepContext;
	private String jobName;
	private long readStartTime;
	private Long jobExecutionId;
	
	int issuerSubList;
	private JobService jobService;
	public EnrollmentBatchService getEnrollmentBatchService() {
		return enrollmentBatchService;
	}

	public void setEnrollmentBatchService(EnrollmentBatchService enrollmentBatchService) {
		this.enrollmentBatchService = enrollmentBatchService;
	}
	
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) throws Exception {
		try{
			stepContext = stepExecution.getExecutionContext();
			jobExecutionId = stepExecution.getJobExecutionId();
			jobName=(String)stepExecution.getJobExecution().getJobInstance().getJobName();

			if(stepContext != null){
				issuerSubList= stepContext.getInt("issuerSubList");
				startDate = (Date) stepContext.get("startDate");
				endDate = 	(Date) stepContext.get("endDate");
				carrierResendFlag=(String) stepContext.get("carrierResendFlag");

			}

			executionContext = stepExecution.getJobExecution().getExecutionContext();
			LOGGER.info(Thread.currentThread().getName() + " Acquired the execution context in Writer");
			readStartTime = System.currentTimeMillis();
		}catch(Exception e){
			if(executionContext != null){
				executionContext.put("jobExecutionStatus", "failed");
				executionContext.put("errorMessage", e.getMessage());
			}
			throw e;
		}
	}

	@Override
	public void write(List<? extends Enrollment> enrollments) throws Exception {
		long writeStartTime = System.currentTimeMillis();
		LOGGER.info(Thread.currentThread().getName() + ": Time taken for reading " +  (enrollments == null?0:enrollments.size()) +  " enrollment records: "+ (writeStartTime - readStartTime)/1000  + " seconds and " + (writeStartTime - readStartTime)%1000 + " milliseconds");
		List<String> filePathList = new ArrayList<>();
		String batchJobStatus=null;
		
		 if(enrollments !=  null) {
			try{
				if(jobService != null && jobExecutionId != -1){
					batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
				}
				if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
					issuerFileList.deleteAllFiles();
					issuerFileList.clearFiles();
					throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
				}

				if(carrierResendFlag!=null && (carrierResendFlag.isEmpty() || carrierResendFlag.equalsIgnoreCase("null"))){
					carrierResendFlag=null;
				}
				String enrollmentType=null;
				if(jobName.equalsIgnoreCase(EnrollmentConstants.INDIVIDUAL_ENROLLMENT_BATCH_JOB)){
					enrollmentType=GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL;
				}else if(jobName.equalsIgnoreCase(EnrollmentConstants.SHOP_ENROLLMENT_BATCH_JOB)){
					enrollmentType=GhixConstants.ENROLLMENT_TYPE_SHOP;
				}
				
				if(issuerFileList == null){
					throw new GIException("FileMap not accessible");
				}
				
				@SuppressWarnings("unchecked")
				List<Enrollment> enrollmentList = (List<Enrollment>) enrollments;
				
				Enrollment e = enrollments.get(0);
				int issuerId = e.getIssuerId();
	
				LOGGER.info(Thread.currentThread().getName() + " Writer: Procesing Enrollment Record for issuer: " + issuerId);
				enrollmentBatchService.serveEnrollmentRecordJob(enrollmentList, issuerId, enrollmentType, startDate, endDate, carrierResendFlag, jobExecutionId, jobName, filePathList, issuerSubList);
				if(jobService != null && jobExecutionId != -1){
					batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
				}
				if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
					issuerFileList.deleteAllFiles();
					issuerFileList.clearFiles();
					throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
				}
				if(filePathList!=null && !filePathList.isEmpty()){
					issuerFileList.add(filePathList);
				}
			}
			catch(Exception e){
				if(filePathList!=null && filePathList.size()>0){
					issuerFileList.add(filePathList);
				}
				if(executionContext != null){
					executionContext.put("jobExecutionStatus", "failed");
					executionContext.put("errorMessage", e.getMessage());
				}
				LOGGER.error("Exception in Writer :"+ e.getMessage());
				throw e;
			}
		}
		long endTime = System.currentTimeMillis();
		LOGGER.info(Thread.currentThread().getName() + " : Time taken in Writer: "+ (endTime - writeStartTime)/1000  + " seconds and " + (endTime - writeStartTime)%1000 + " milliseconds");
	}

	public IssuerFileList getIssuerFileList() {
		return issuerFileList;
	}

	public void setIssuerFileList(IssuerFileList issuerFileList) {
		this.issuerFileList = issuerFileList;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
	
}