package com.getinsured.hix.eligibility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.eligibility.repository.IEligLeadRepository;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.screener.util.ScreenerUtil;
import com.getinsured.hix.ui.model.eligibility.prescreen.PhixRequest;

@Controller
public class InitialEligibilityController {
    private static final Logger LOGGER = Logger
            .getLogger(InitialEligibilityController.class);

    @Autowired
    private ZipCodeService zipCodeService;	

    @Autowired
    private UserService userService;

    @Autowired
    private ConsumerPortalUtil consumerUtil;

    @Autowired
    @Qualifier("screenerUtil")
    private ScreenerUtil phixUtil;
    
    @Autowired
    private IEligLeadRepository iEligLeadRepository;
    
    private static final String stateCodeConfig = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
    
    @RequestMapping(value = "preeligibility", method = RequestMethod.GET)
    public String getPreScreen(Model model, HttpServletRequest httpRequest) {
        return "iex/eligibility/checkernm";

    }
    @RequestMapping(value = "iex/initpreeligibility", method = RequestMethod.GET)
    @ResponseBody
    public PhixRequest initPreScreen(Model model, HttpServletRequest httpRequest) {
        PhixRequest phixRequest = new PhixRequest();
        try {
            AccountUser user  = userService.getLoggedInUser();
            Household household = null;
            EligLead eligLead = null;
            if(user == null){
            	Object leadId = httpRequest.getSession().getAttribute("leadId");
            	if(leadId != null){
            		eligLead = iEligLeadRepository.findOne(Long.parseLong(leadId.toString()));
            		phixRequest = phixUtil.createPhixRequest(eligLead,0);
            	}
            }
            
            //Initialize year to Current Coverage Year
            int year = TSCalendar.getInstance().get(Calendar.YEAR);
            String currYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
            if(StringUtils.isNumeric(currYear)) {
            	year = Integer.parseInt(currYear);
            }
            
            //HIX-64221 Support for 2016 Issuer Plan Preview
			if(user != null && user.getDefRole() != null
					&& (
							StringUtils.equalsIgnoreCase(user.getActiveModuleName(),"ISSUER_REPRESENTATIVE")
							|| StringUtils.equalsIgnoreCase(user.getActiveModuleName(),"ADMIN") 
							|| StringUtils.equalsIgnoreCase(user.getActiveModuleName(),"OPERATIONS") 
							|| StringUtils.equalsIgnoreCase(user.getActiveModuleName(),"ISSUER_ADMIN"))
					){
				phixRequest.setShowIncomeSection(false);
				
				if(httpRequest.getSession().getAttribute("EFFECTIVE_START_DATE") != null 
	    				&& StringUtils.isNotBlank(httpRequest.getSession().getAttribute("EFFECTIVE_START_DATE").toString())){
	    			String effectiveStartDateStr = httpRequest.getSession().getAttribute("EFFECTIVE_START_DATE").toString();
	    			
	    			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	    			LocalDate effectiveStartDate = new DateTime(formatter.parse(effectiveStartDateStr)).toLocalDate();
	    			year = effectiveStartDate.getYear();
	    		}
                phixRequest.setCoverageYear(year);
			}
			//HIX-70580 multi-year support for prescreener
			if(user != null && user.getDefRole() != null
					&& !StringUtils.equalsIgnoreCase(user.getActiveModuleName(),"ISSUER_REPRESENTATIVE")
					&& !StringUtils.equalsIgnoreCase(user.getActiveModuleName(),"ADMIN")
					&& !StringUtils.equalsIgnoreCase(user.getActiveModuleName(),"OPERATIONS")
					&& !StringUtils.equalsIgnoreCase(user.getActiveModuleName(),"ISSUER_ADMIN")){
                household = consumerUtil.getHouseholdRecord(user.getActiveModuleId());
                if(household!=null){
                eligLead = household.getEligLead();
	                
	                String coverageYear = httpRequest.getParameter("coverageYear");
	                if(StringUtils.isNumeric(coverageYear)) { 	
                	year = Integer.parseInt(coverageYear);
	                	String prevYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
	 
						if (StringUtils.isNumeric(prevYear) && year == Integer.parseInt(prevYear)) {
						boolean hasCutOffDatePassed = phixUtil.hasCutOffDatePassed();
						if(hasCutOffDatePassed){
								year = Integer.parseInt(currYear);
					}
                }
                }
                phixRequest = phixUtil.createPhixRequest(eligLead,year);
	                phixRequest.setCoverageYear(year);
            }
                else{
                	LOGGER.error("Getting null Household for active module id "+user.getActiveModuleId());
                }
            }
        } catch (InvalidUserException iue) {
            LOGGER.error("Error occured while retrieving loggedInUser. Returning an empty preeligibility model",iue);
        }catch(Exception ex){
            LOGGER.error("Error occured while constructing phixRequest. Returning an empty preeligibility model",ex);
        }

        return phixRequest;

    }
    
    @RequestMapping(value = "iex/validateZip", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Map<String,String>> validateZipCode(HttpServletRequest httpRequest) {

        Map<String, Map<String,String>> retMap = null;
        String zip = httpRequest.getParameter("zip");
        if (StringUtils.isNumeric(zip)) {
        	
        	//HIX-98256 changes
        	String stateCode = httpRequest.getParameter("stateCode");
        	if(StringUtils.isBlank(stateCode)) {
        		stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
        	}
           	
            List<ZipCode> records = zipCodeService.findByZipAndStateCode(zip, stateCode.trim());
            if (!CollectionUtils.isEmpty(records)) {
                retMap = new HashMap<>();
                // Adding state codes and counties to return map
                for (ZipCode zipCode : records) {
                    Map<String,String> values = new HashMap<String, String>();
                    values.put("countyCode", zipCode.getStateFips() + zipCode.getCountyFips());
                    values.put("stateCode", zipCode.getState());
                    values.put("globalStateCode", stateCode);
                    retMap.put(zipCode.getCounty(), values);
                }
            }
        }
        return retMap;
    }
    
    @RequestMapping(value = "preeligibility/session/ping",  method = RequestMethod.GET)
	@ResponseBody
	public String sessionPing(HttpServletRequest request, HttpServletResponse response) {
    	LOGGER.debug("session extended");
		return "" ;
	}
}
