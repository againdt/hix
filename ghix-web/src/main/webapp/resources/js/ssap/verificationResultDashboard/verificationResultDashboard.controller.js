(function(){
	'use strict';

	var app = angular.module("verificationResultDashboardApp");

	app.controller('verificationResultDashboardController', verificationResultDashboardController);

	verificationResultDashboardController.$inject = ['$http', 'fileUpload', '$window'];

	function verificationResultDashboardController($http, fileUpload, $window){
		var vm = this;

		vm.documentTypes = {};
		vm.files = {};
		vm.qleFiles = {};
		vm.init = init;
		vm.showVerificationModal = showVerificationModal;
		vm.showQleVerificationModal = showQleVerificationModal;
		vm.verificationDoc = verificationDoc;
		vm.overrideQleVerification = overrideQleVerification;
		vm.submitDoc = submitDoc;
		vm.submitQleDoc = submitQleDoc;
		vm.showDetail = showDetail;
		vm.switchCSR = switchCSR;
		vm.submitData = submitData;
		vm.getLegalPresenceVerification = getLegalPresenceVerification;
		vm.stateCode = $('#stateCode').val();
		vm.convertNumberToDate = convertNumberToDate;
		vm.showPage = false;

		function init(){
			vm.webJson = angular.fromJson($('#webJson').val());
			getLegalPresenceVerification(vm.webJson.mainApplicantList[0]);

			getQualifiedLifeEvents();

			//ssap verification
			for(var i = 0; i < vm.webJson.mainApplicantList.length; i++){
				for(var key in vm.webJson.mainApplicantList[i].documentCategoryStatusMap){
					if (key !== 'Legal Presence') {
						var submittedDocumentCount = 0;
						for (var j = 0; j < vm.webJson.mainApplicantList[i].documentCategoryStatusMap[key].documentCategoryDtlList.length; j++) {
							if (vm.webJson.mainApplicantList[i].documentCategoryStatusMap[key].documentCategoryDtlList[j].consumerDocument.accepted === 'SUBMITTED') {
								submittedDocumentCount++;
							}
						}

						if (submittedDocumentCount === 0) {
							vm.webJson.mainApplicantList[i].documentCategoryStatusMap[key].isInSubmitted = false;
						} else {
							vm.webJson.mainApplicantList[i].documentCategoryStatusMap[key].isInSubmitted = true;
						}
					}
				}
			}
		}


		function convertNumberToDate(input){
		   return  moment(input).format('MM/DD/YYYY');
        }
        function dateToMilliseconds(input) {
            return  moment(input).valueOf();
        }
		function getLegalPresenceVerification(memberApplicationDetails) {
			console.log('memberApplicationDetails', memberApplicationDetails);
			var url = '/hix/newssap/application/status/applicant/' + memberApplicationDetails.applicantID;
			var countryUrl = '/hix/hub/getCountryOfIssuanceList';
			var promise = $http.get(url);
            var promiseCountry = $http.get(countryUrl);

			promise.success(function (response) {
				vm.data = response;
				vm.actionResultList = vm.data.filter(function(member) {
				    return member.ssapApplicantId === memberApplicationDetails.applicantID;
				});
				vm.actionResult = vm.actionResultList[0].actionResult;
				vm.appID = vm.actionResultList[0].ssapApplicantId;
				vm.documentType = Object.keys(vm.data[0].vlpRequest.payload.DHSID);
                promiseCountry.success(function (response) {
                    vm.countryList = response;
                    vm.countries = vm.countryList.map(function(countryName){
                        return ({
                            name: countryName.countryName,
                            code: countryName.countryCode
                        });
                    });

                });
                var fetchCitizenshipImmigrationStatusUrl = '/hix/newssap/vlp/citizenshipImmigrationStatus/' + vm.actionResultList[0].id;
                promise = $http.get(fetchCitizenshipImmigrationStatusUrl);
                promise.success(function (response) {
                    vm.citizenshipImmigrationStatusData = response;
                    if (vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate) {
                        vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate = vm.convertNumberToDate(vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate);
                    }
                    if (vm.actionResult === 'VERIFY' || vm.actionResult === 'CHECK_CASE_RESOLUTION') {
                        vm.dhsidData = [];
                        var data = vm.data[0].vlpRequest.payload.DHSID;
                        for (var key in data) {
                            vm.dhsidData.push(data[key]);
                        }
                        vm.dhsidData.map(function(docType) {
                            if(docType.hasOwnProperty('ReceiptNumber')) {
                                var val = docType['ReceiptNumber'];
                                delete docType.ReceiptNumber;
                                docType.CardNumber = val;
                            }
                            vm.dhsid = docType;
                            return vm.dhsid;
                        });
                    }
                });
			});

		}

		function submitData(key, applicantId, expDate) {
			vm.loader = true;
            var data = {};
            var url = '/hix/newssap/application/vlp/update/' + vm.webJson.applicationID +'/'+ applicantId;

            var newDate = expDate;
            if (newDate && newDate.length > 0) {
                vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate = dateToMilliseconds(newDate);
            } else {
                vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate =
                    dateToMilliseconds(vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate);
            }
            data = vm.citizenshipImmigrationStatusData;
            var config = {
                headers: {'Content-Type': 'application/json'}
            };

            var promise = $http.post(url, data, config);
            promise.success(function(response){
                vm.loader = false;
                //vm.webJson.mainApplicantList[i].documentCategoryStatusMap[key].isDocumentCategoryVerified = true;
                vm.ajaxSuccess = response.message;
                $('#ajaxSuccessModal').modal();
                vm.init();

            });

            promise.error(function(){
                vm.loader = false;

                vm.ajaxError = 'We apologize for the technical issues. Please try again in sometime.';
                $('#ajaxErrorModal').modal();
            });
		}

		function getQualifiedLifeEvents(){
			var url = '/hix/ssap/applicant/events/' + vm.webJson.caseId;
			var promise = $http.get(url);

			promise.success(function(response){
				vm.showPage = true;
				vm.webJson.qualifiedLifeEvents = response;
				if(vm.webJson.qualifiedLifeEvents.status === '200'){

					var applicants = angular.copy(vm.webJson.qualifiedLifeEvents.applicants);
					//qle verification: setting applicant level flag
					for(var i = 0; i < applicants.length; i++){
						applicants[i].isAllEventsVerified = true;
						var notRequiredCount = 0;
						for(var j = 0; j< applicants[i].events.length; j++){
							if(applicants[i].events[j].validationStatus === 'INITIAL' || applicants[i].events[j].validationStatus === 'PENDING' || applicants[i].events[j].validationStatus === 'SUBMITTED' || applicants[i].events[j].validationStatus === 'REJECTED'){
								applicants[i].isAllEventsVerified = false;
							}else if(applicants[i].events[j].validationStatus === 'NOTREQUIRED'){
								notRequiredCount++
							}
						}


						if(notRequiredCount === applicants[i].events.length){
							applicants[i].isAllEventsNotRequired = true;
						}else{
							applicants[i].isAllEventsNotRequired = false;
						}
					}

					//don't show applicant whose all events are non-required 
					vm.webJson.qualifiedLifeEvents.applicants = applicants.filter(function(applicant){
						return applicant.isAllEventsNotRequired === false;
					});

				}else{
					vm.ajaxError = response;
					$('#ajaxErrorModal').modal();
				}

			});

			promise.error(function(response){
				vm.ajaxError = 'We apologize for the technical issues. Please try again in sometime.';
				$('#ajaxErrorModal').modal();
			});
		}

		function showVerificationModal(key, applicant){
			vm.key = key;
			vm.applicant = applicant;
			vm.qleVerificationModal = false;

			vm.overrideComment = '';
			$('#adminOverrideModal').modal();
		}

		function showQleVerificationModal(applicantEvent, applicant){
			vm.overrideApplicantEvent = applicantEvent;
			vm.overrideApplicant = applicant;
			vm.qleVerificationModal = true;

			vm.overrideComment = '';
			$('#adminOverrideModal').modal();
		}

		function verificationDoc(){
			vm.loader = true;

			var url = $('#subUrl').val();
			var data = {
				applicantID : vm.applicant.applicantID,
				documentCategory : vm.key.replace(/\s+/g, ""),
				overrideComments : vm.overrideComment,
				caseNumber : vm.webJson.caseId
			};

			var config = {
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj) {
					var str = [];
					for(var p in obj)
						str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					return str.join("&");
				}
			};

			var promise = $http.post(url, data, config);

			promise.success(function(response){
				vm.loader = false;
				if(response === 'true'){
					vm.applicant.documentCategoryStatusMap[vm.key].isDocumentCategoryVerified = true;
				}else{
					vm.ajaxError = 'We apologize for the technical issues. Please try again in sometime.';
					$('#ajaxErrorModal').modal();
				}
			});

			promise.error(function(){
				vm.loader = false;

				vm.ajaxError = 'We apologize for the technical issues. Please try again in sometime.';
				$('#ajaxErrorModal').modal();
			});

		}

        function showDetail(docpath) {
            var encodedDocPath = window.encodeURI(docpath);
            window.open("/hix/indportal/viewDocument?documentId=" + encodedDocPath,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
        }

        function switchCSR(ticketID) {
            var encodedticketID = window.encodeURI(ticketID);
            $window.location.href = "/hix/indportal/switchCSRToTicket/" + encodedticketID;
        }

		function overrideQleVerification(){
			vm.loader = true;

			var url = '/hix/ssap/admin/qle/override';
			var data = {
				applicantEventId : vm.overrideApplicantEvent.applicantEventId,
				overrideComments : vm.overrideComment,
				caseNumber : vm.webJson.caseId
			};

			var config = {
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj) {
			        var str = [];
			        for(var p in obj)
			        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			        return str.join("&");
			    }
			};

			var promise = $http.post(url, data, config);

			promise.success(function(response){
				vm.loader = false;
				if(response === '200'){
					vm.overrideApplicantEvent.validationStatus = 'OVERRIDDEN';

					var verifiedEventCount = 0;
					for(var j = 0; j< vm.overrideApplicant.events.length; j++){
						if(vm.overrideApplicant.events[j].validationStatus === 'OVERRIDDEN' || vm.overrideApplicant.events[j].validationStatus === 'VERIFIED'){
							verifiedEventCount++;
						}
					}

					if(verifiedEventCount === vm.overrideApplicant.events.length){
						vm.overrideApplicant.isAllEventsVerified = true;
					}

				}else{
					alert("Failed to override verification, Please try again later!");
				}

			});

			promise.error(function(){
				vm.loader = false;
				alert("Failed to override verification, Please try again later!");
			});
		}


		function submitDoc(applicantID, doc, firstIndex, secondIndex){

			vm.loader = true;

			var url = "/hix/ssapdocumentmgmt/documents/uploadDocumentAttachmentsAjax";

			//for upload file
			var file = '';
			var fileName = 'files[]';

			//for text input
			var fields = [
				{'name': 'case_number', 'data': vm.webJson.caseId},
				{'name': 'applicantionID', 'data': vm.webJson.applicationID},
				{'name': 'csrftoken', 'data': $('#tokid').val()}
			];

			if(applicantID === 'Authorized Representative Authorization'){
				file = vm.authorizedRepresentativeAuthorizationFile;
				fields = fields.concat([
					{'name': 'documentCategory', 'data': 'Authorized Representative Authorization'},
					{'name': 'documentType', 'data': 'Authorized Representative'},
					{'name': 'applicantID', 'data': vm.webJson.mainApplicantList[0].applicantID}
				]);
			}else{
				file = vm.files[firstIndex + '_' + secondIndex];
				fields = fields.concat([
					{'name': 'documentCategory', 'data': doc.documentCategoryName},
					{'name': 'documentType', 'data': vm.documentTypes[firstIndex + '_' + secondIndex]},
					{'name': 'applicantID', 'data': applicantID}

				]);
			}

			var selectedOption = fields.find(function(f) {
			    return f.name === "documentType";
            });
			if (selectedOption !== undefined && selectedOption !== null) {
				if (selectedOption.data.startsWith("--") && selectedOption.data.endsWith("--")) {
					vm.ajaxError = "Please select valid document type.";
					vm.loader = false;
					$('#ajaxErrorModal').modal();
					return;
				}
			}

			var promise = fileUpload.uploadFileAndFields(fileName, file, fields, url);
			promise.success(function (response) {
				vm.loader = false;

				if (Array.isArray(response)) {
					if (applicantID === 'Authorized Representative Authorization') {
					    var consumerDocuments = [];
					    for (var i = 0; i < response.length; i++) {
					        consumerDocuments.push(response[i].consumerDocument);
                        }
						vm.webJson.consumerDocuments = consumerDocuments;
					} else {
						doc.isInSubmitted = true;
						for (var i = 0; i < response.length; i++) {
							doc.documentCategoryDtlList[i] = response[i];
						}
					}
				} else {
					var errorMessageOneLocation = response.indexOf("Media type");
					var errorMessageTwoLocation = response.indexOf("Use one of these");
					if (errorMessageOneLocation == -1 && errorMessageTwoLocation == -1) {
						vm.ajaxError = "File could not be uploaded";
					} else if (errorMessageOneLocation == -1 && errorMessageTwoLocation != -1) {
						vm.ajaxError = response;
					} else if (errorMessageOneLocation != -1) {
						vm.ajaxError = "Your file could not be uploaded. Use one of these file types: BMP, GIF, JPG, JPEG, PNG, or PDF.";
					} else {
						vm.ajaxError = 'We apologize for the technical issues. Please try again in sometime.';
					}
					$('#ajaxErrorModal').modal();
				}
			});

			promise.error(function (response) {
				vm.loader = false;

				vm.ajaxError = 'We apologize for the technical issues. Please try again in sometime.';
				$('#ajaxErrorModal').modal();
			});
		}


		function submitQleDoc(thisEvent, thirdIndex, fourthIndex){
			vm.loader = true;
			var url = '/hix/ssap/applicant/event/uploadDocument';

			var eventLabel = "";
			if (thisEvent !== undefined && thisEvent.eventLabel !== undefined && thisEvent.eventLabel !== '') {
				eventLabel = thisEvent.eventLabel;
			}

			var fields = [
				{'name': 'applicantEventId', 'data': thisEvent.applicantEventId},
				{'name': 'eventId', 'data': thisEvent.eventId},
				{'name': 'eventLabel', 'data': eventLabel},
				{'name': 'documentType', 'data': 'QLE Validation Document'},
				{'name': 'caseNumber', 'data': vm.webJson.caseId + ''}
			];
			var file = vm.qleFiles[thirdIndex + '_' + fourthIndex];
			var fileName = 'document';

			var promise = fileUpload.uploadFileAndFields(fileName, file, fields, url);
			promise.success(function(response){
				vm.loader = false;

				if(Array.isArray(response)){
					thisEvent.uploadedDocumentList = response;
					thisEvent.validationStatus = 'SUBMITTED';
				}else{

					var errorMessageOneLocation = response.indexOf("Media type");
					var errorMessageTwoLocation = response.indexOf("Use one of these");
					if(errorMessageOneLocation == -1 && errorMessageTwoLocation == -1){
						vm.ajaxError    = "File could not be uploaded";
					}else if(errorMessageOneLocation == -1 && errorMessageTwoLocation != -1) {
						vm.ajaxError = response;
					}else if (errorMessageOneLocation != -1){
						vm.ajaxError = "Your file could not be uploaded. Use one of these file types: BMP, GIF, JPG, JPEG, PNG, or PDF.";
					}else{
						vm.ajaxError = 'We apologize for the technical issues. Please try again in sometime.';
					}

					$('#ajaxErrorModal').modal();
				}


			});

			promise.error(function(response){
				vm.loader = false;

				vm.ajaxError = 'We apologize for the technical issues. Please try again in sometime.';
				$('#ajaxErrorModal').modal();
			});
		}

	}
})();
