<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<html>

<body>

<div class="paddingR20">
<div id="header" class="gray"></div>
<div id="confirmdeletebox">
	<form class="form-vertical" id="validateconfirmdelete" name="validateconfirmdelete" action="deletecomment" method="POST" target="_parent">
		<input type="hidden" name="commentId" id="commentId" value="" />		
		
		<div class="profile">
			<div id="confirmationMessage" class="gutter10">
				<p><spring:message code="label.agent.employers.DelCommentConfMsg"/></p> 				
			</div>
			<div>
				<input name="submitRequest" id="submitRequest" type="button" onClick="confirmDelete();" value='<spring:message code="label.agent.employers.Delete"/>' class="btn btn-primary pull-right" />
				<input type="button" value='<spring:message code="label.brkCancel"/>' class="btn pull-left margin10-l" data-dismiss="modal"  onClick="closeWindow();"/>
			</div>
		</div>
	</form>	
</div>
</div>
</body>
</html>

<script type="text/javascript">

	$(document).ready(function() {
		var commentId = getParameterByName('commentId');		
		$('#commentId').val(commentId);
			
	});
	
	function getParameterByName( name ) //courtesy Artem
	{
	  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	  var regexS = "[\\?&]"+name+"=([^&#]*)";
	  var regex = new RegExp( regexS );
	  var results = regex.exec( window.location.href );
	  if( results == null )
	    return "";
	  else
	    return decodeURIComponent(results[1].replace(/\+/g, " "));
	}
			
	function confirmDelete() {					
		var commId = $('#commentId').val();
		var strPath =  "<%=  request.getContextPath()  %>";
		var pathURL=strPath+"/platform/web/comment/controller/deletecomment/" + commId;			
		//POST request for ajax call
		$.post(pathURL, {commentId : commId,csrftoken:'<df:csrfToken plainToken="true"/>'}, 
			function(data) {
				if(data == 'success'){
					//alert('Comment is deleted !');
				}
				else{		
					alert('Sorry your comment could not be deleted.');
				}
				window.parent.closeIFrame();
		});
	}
	function closeWindow()
	{
		window.parent.closeIFrame();
	}
</script>