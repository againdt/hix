package com.getinsured.iex.shop.mec.prefixmappers;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class ShopMECNamespaceMapper extends NamespacePrefixMapper{
	
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		
		if(namespaceUri != null &&
				namespaceUri.compareToIgnoreCase("http://gov.hhs.cms.hix.dsh.ee.nonesi_mec.ext") == 0){
			return "gov";
		}
		else{
			return suggestion;
		}
	}
}
