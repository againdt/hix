package com.getinsured.hix.batch.enrollment.terminator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

public class TerminateEnrlReconDiscrepancyReportJob  implements Tasklet{
	private static final Logger LOGGER = LoggerFactory.getLogger(TerminateEnrlReconDiscrepancyReportJob.class);
	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
		try{
			LOGGER.info("Inside TerminateEnrlReconDiscrepancyReportJob");
		}catch(Exception e){
			LOGGER.error("Error while terminating enrollment recon report job");
		}
		LOGGER.info("Terminate");
		return RepeatStatus.FINISHED;

	}
}
