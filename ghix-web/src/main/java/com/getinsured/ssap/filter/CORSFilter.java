package com.getinsured.ssap.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Enabling CORS support  - Access-Control-Allow-Origin
 *
 * Add this to web.xml to enable CORS
 *
 * <code>
 * <filter>
 *  <filter-name>cors</filter-name>
 *  <filter-class>com.getinsured.ssap.filter.CORSFilter</filter-class>
 * </filter>
 *
 * <filter-mapping>
 * <filter-name>cors</filter-name>
 *  <url-pattern>/*</url-pattern>
 * </filter-mapping>
 * </code>
 * @author Yevgen Golubenko
 * @since 1/4/19
 */
public class CORSFilter extends OncePerRequestFilter
{
  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException
  {
    response.addHeader("Access-Control-Allow-Origin", "*");

    if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
      // CORS "pre-flight" request
      response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
//			response.addHeader("Access-Control-Allow-Headers", "Authorization");
      response.addHeader("Access-Control-Allow-Headers", "Content-Type");
      response.addHeader("Access-Control-Max-Age", "1");
    }

    filterChain.doFilter(request, response);
  }
}
