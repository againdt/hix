package com.getinsured.eligibility.at.ref.service;

import com.getinsured.eligibility.at.ref.dto.APTCUpdateRequest;
import com.getinsured.hix.indportal.dto.AptcUpdate;

/**
 * @author chopra_s
 * 
 */
public interface APTCUpdateService {
	String process(APTCUpdateRequest request, AptcUpdate aptcUpdate);

	AptcUpdate populateAptcUpdate(APTCUpdateRequest request);
}
