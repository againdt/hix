package com.getinsured.iex.hub.consumers.hub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.rabbitmq.client.Channel;

public class IFSVQueueConsumer extends MessageProcessor implements ChannelAwareMessageListener {

	@Autowired
	private WebServiceTemplate ifsvServiceTemplate;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(IFSVQueueConsumer.class);

	
	
	private void invokeIFSV() throws HubMappingException,
			BridgeException, HubServiceException, InputDataValidationException {
		HubServiceBridge ifsvBridge = HubServiceBridge.getHubServiceBridge("IFSVRequest");
		ServiceHandler handler = ifsvBridge.getServiceHandler();
		handler.setJsonInput("{\"taxHousehold\":[{\"householdMember\":[{\"personId\":1,\"name\":{\"firstName\":\"Christian\",\"lastName\":\"Bale\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"006104444\"},\"taxFilerCategoryCode\":\"PRIMARY\"},{\"personId\":2,\"name\":{\"firstName\":\"Blooming\",\"lastName\":\"Dale\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"006105666\"},\"taxFilerCategoryCode\":\"DEPENDENT\"}]}],\"requestID\":\"1985\"}");
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction("IFSV");
		request.setEndpointOperationName("IFSV_VERIFICATION");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus("PENDING");
		this.executeRequest(request, ifsvBridge.getServiceHandler(), this.ifsvServiceTemplate);
	}
	@Override
	public void onMessage(Message message, Channel channel) throws BridgeException {
		String threadId = Thread.currentThread().getName();
		String routingKey = message.getMessageProperties().getReceivedRoutingKey();
		if(routingKey == null){
			throw new IllegalArgumentException("No routing key available for incoming IFSV message, don't know how to process it");
		}
		LOGGER.info("Thread[" + threadId + "]Routing Key:"
				+ routingKey
				+ " Received Message:" + new String(message.getBody())
				+ " from channel:" + channel.getChannelNumber());

		try {
			invokeIFSV();
		} catch (SoapFaultClientException e) {
			LOGGER.error("Request failed with Reason:"
					+ e.getFaultStringOrReason() + " message:"
					+e.getMessage()
					+ "And fault code:"+e.getFaultCode(),e);
		} catch (HubServiceException e) {
			LOGGER.info("Exception", e);
		} catch (HubMappingException e) {
			LOGGER.info("Exception", e);
		} catch (InputDataValidationException ie) {
			LOGGER.info("Exception", ie);
		}
	}
		
}
