<script type="text/ng-template" id="income-self.jsp">
  <div class="gutter10 jobIncome" ng-controller="incomeSelf">
    <h3><spring:message code="label.lce.selfemploymentincome"/></h3>
    <form class="form-horizontal" id="incomeSelf" name="incomeSelf" novalidate>
      <div class="control-group">
        <label class="control-label" for="typeOfRequest"><spring:message code="label.lce.typeofwork"/>:</label>
          <div class="controls">
            <input type="text" class="span5" placeholder="Consultant" name="typeOfWork" ng-model="householdMember.detailedIncome.selfEmploymentIncome.typeOfWork" required ng-trim>
          </div>
          <div class="controls margin5-t">
            <span class="validation-error-text-container" ng-show="submitted && incomeSelf.typeOfWork.$error.required"><spring:message code="label.lce.incomeself.error1"/></span>
          </div>
      </div>

      <spring:message code="label.lce.howmuchincomewill"/> {{householdMember.name.firstName}} {{householdMember.name.lastName}} <spring:message code="label.lce.getfromthis"/>
      <div class="control-group">
        <label class="control-label" for="typeOfRequest"></label>
          <div class="controls margin10-t">
            <div class="input-prepend">
              <span class="add-on">$</span>
              <input type="text" class="span10" name="monthlyNetIncome"  ng-model="householdMember.detailedIncome.selfEmploymentIncome.monthlyNetIncome" required numbers-Only>
            </div>
          </div>
          <div class="controls margin5-t">
            <span class="validation-error-text-container" ng-show="submitted && incomeSelf.monthlyNetIncome.$error.required"><spring:message code="label.lce.incomeself.error2"/></span>
          </div>
      </div>
    </form>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a ng-show="incomeSelf.$valid" class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
      <a ng-show="!incomeSelf.$valid" class="btn btn-primary" ng-click="submitted=true"><spring:message code="label.lce.next"/></a>
      {{jobIncome.$error}}
    </dl>
  </div>
</script>