package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.batch.ssap.renewal.service.VerificationService;
import com.getinsured.hix.batch.ssap.renewal.util.RenewalUtils;
import com.getinsured.hix.batch.ssap.renewal.util.VerificationPartitionerParams;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;

/**
 * Verification Partitioner class is used to get read SSAP Application IDs from database and do Partitioner.
 * 
 * @since August 12, 2019
 */
@Component("ssapVerificationPartitioner")
@Scope("step")
public class VerificationPartitioner implements Partitioner {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationPartitioner.class);

	private Long batchSize;
	private Long renewalYear;
	private String applicationStatus;
	private String verificationCommitInterval;
	private VerificationService verificationService;
	private VerificationPartitionerParams verificationPartitionerParams;
	private BatchJobExecutionService batchJobExecutionService;

	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {

		Map<String, ExecutionContext> partitionMap = null;
		StringBuffer errorMessage = new StringBuffer();

		try {

			if (!hasRunningBatchSizeOne()) {
				errorMessage.append(RenewalUtils.EMSG_RUNNING_BATCH);
				return partitionMap;
			}

			String defaultBatchSize = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_BATCHSIZE);
			renewalYear = Long.valueOf(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
			verificationPartitionerParams.setRenewalYear(renewalYear);

			if (null == batchSize || 0l == batchSize) {
				batchSize = Long.valueOf(defaultBatchSize);
			}

			if (!validateParams(errorMessage)) {
				return partitionMap;
			}

			List<Long> partitionerSsapApplicationIdList = null;
			List<String> applicationStatusList = new CopyOnWriteArrayList<String>();
			applicationStatusList.add(ApplicationStatus.SIGNED.getApplicationStatusCode());

			if (null != applicationStatus) {
				applicationStatusList.add(applicationStatus);
			}
			partitionerSsapApplicationIdList = verificationService.getSsapApplicationsByCoverageYearAndStatus(renewalYear, applicationStatusList, batchSize);

			if (CollectionUtils.isNotEmpty(partitionerSsapApplicationIdList)) {

				LOGGER.info("Number of SSAP Application-ID for Verification: {}", partitionerSsapApplicationIdList.size());
				partitionMap = new HashMap<String, ExecutionContext>();
				verificationPartitionerParams.clearSsapApplicationIdList();
				verificationPartitionerParams.addAllToSsapApplicationIdList(partitionerSsapApplicationIdList);

				int maxSsapApplicationsCommitInterval = 1;
				int size = partitionerSsapApplicationIdList.size();

				if (StringUtils.isNumeric(this.verificationCommitInterval)) {
					maxSsapApplicationsCommitInterval = Integer.valueOf(this.verificationCommitInterval.trim());
				}

				int numberOfSsapApplicationIdToCommit = size / maxSsapApplicationsCommitInterval;
				if (size % maxSsapApplicationsCommitInterval != 0) {
					numberOfSsapApplicationIdToCommit++;
				}

				int firstIndex = 0;
				int lastIndex = 0;

				for (int i = 0; i < numberOfSsapApplicationIdToCommit; i++) {
					firstIndex = i * maxSsapApplicationsCommitInterval;
					lastIndex = (i + 1) * maxSsapApplicationsCommitInterval;

					if (lastIndex > size) {
						lastIndex = size;
					}
					ExecutionContext value = new ExecutionContext();
					value.putInt("startIndex", firstIndex);
					value.putInt("endIndex", lastIndex);
					value.putInt("partition", i);
					partitionMap.put("partition - " + i, value);
				}
			}
			else {
				errorMessage.append("SSAP Application data is not found for Verification.");
			}
		}
		catch (Exception ex) {
			errorMessage.append("VerificationPartitioner failed to execute : ").append(ex.getMessage());
			LOGGER.error(errorMessage.toString(), ex);
		}
		finally {

			if (0 < errorMessage.length()) {
				verificationService.saveAndThrowsErrorLog(errorMessage.toString());
			}
		}
		return partitionMap;
	}

	private boolean validateParams(StringBuffer errorMessage) {

		boolean hasValidParams = true;

		if (null == batchSize || 0l >= batchSize) {
			errorMessage.append("Invalid Batch Size: ");
			errorMessage.append(batchSize);
			hasValidParams = false;
		}

		if (null == renewalYear || 2000 > renewalYear || 2099 < renewalYear) {

			if (!hasValidParams) {
				errorMessage.append(", ");
			}
			errorMessage.append("Invalid Current Coverage Year: ");
			errorMessage.append(renewalYear);
			hasValidParams = false;
		}

		if (null != applicationStatus) {

			if (null == ApplicationStatus.fromString(applicationStatus)) {

				if (!hasValidParams) {
					errorMessage.append(", ");
				}
				errorMessage.append("Invalid Application Status: ");
				errorMessage.append(applicationStatus);
				errorMessage.append(".");
				hasValidParams = false;
			}
		}
		if (!hasValidParams) {
			LOGGER.error(errorMessage.toString());
		}
		return hasValidParams;
	}

	/**
	 * Method is used to get Running Batch List.
	 */
	private boolean hasRunningBatchSizeOne() {

		boolean hasRunningBatchSizeOne = false;

		List<BatchJobExecution> batchExecutionList = batchJobExecutionService.findRunningJob(VerificationService.JOB_NAME);
		if (batchExecutionList != null && batchExecutionList.size() == 1) {
			hasRunningBatchSizeOne = true;
		}
		return hasRunningBatchSizeOne;
	}

	public Long getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(Long batchSize) {
		this.batchSize = batchSize;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public String getVerificationCommitInterval() {
		return verificationCommitInterval;
	}

	public void setVerificationCommitInterval(String verificationCommitInterval) {
		this.verificationCommitInterval = verificationCommitInterval;
	}

	public VerificationService getVerificationService() {
		return verificationService;
	}

	public void setVerificationService(VerificationService verificationService) {
		this.verificationService = verificationService;
	}

	public VerificationPartitionerParams getVerificationPartitionerParams() {
		return verificationPartitionerParams;
	}

	public void setVerificationPartitionerParams(VerificationPartitionerParams verificationPartitionerParams) {
		this.verificationPartitionerParams = verificationPartitionerParams;
	}

	public BatchJobExecutionService getBatchJobExecutionService() {
		return batchJobExecutionService;
	}

	public void setBatchJobExecutionService(BatchJobExecutionService batchJobExecutionService) {
		this.batchJobExecutionService = batchJobExecutionService;
	}
}
