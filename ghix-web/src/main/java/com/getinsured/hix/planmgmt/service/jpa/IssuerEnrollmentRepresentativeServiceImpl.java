package com.getinsured.hix.planmgmt.service.jpa;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberRequestDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.planmgmt.service.IssuerEnrollmentRepresentativeService;
import com.getinsured.hix.planmgmt.util.ExcelGenerator;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.google.gson.Gson;



@Service("issuerEnrollmentRepresentativeService")
@DependsOn("dynamicPropertiesUtil")
public class IssuerEnrollmentRepresentativeServiceImpl implements IssuerEnrollmentRepresentativeService {
	
	@Value("#{configProp['uploadPath']}") private String uploadPath;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerEnrollmentRepresentativeServiceImpl.class);
	
	private static final String ENROLLMENT_STATUS_ENROLLED = "Enrolled";
	private static final String ENROLLMENT_STATUS_CONFIRMED = "CONFIRM";
	private static final String ENROLLMENT_STATUS_ORDER_ED = "ORDER_ED";
	private static final String ENROLLMENT_STATUS_ORDER_CONFIRMED = "ORDER_CONFIRMED";
	
	@Autowired 
	private GhixRestTemplate ghixRestTemplate;
	@Autowired 
	private PlanMgmtUtil planMgmtUtils;
	@Autowired 
	private Gson platformGson;
	
	
	/* ##### implementation of prepare enrollee search criteria ##### */
	@Override
	public Map<String, Object> createEnrolleeSearchCriteria(HttpServletRequest request, Integer issuerId, boolean doPagination) {
		HashMap<String, Object> searchCriteria = new HashMap<String, Object>();
		try
		{
			int currentYear = TSCalendar.getInstance().get(Calendar.YEAR);
	
			String sortBy = (request.getParameter("sortBy") == null) ? "enrollment.id" : request.getParameter("sortBy");
			searchCriteria.put("sortBy", ((sortBy.equals("")) ? "enrollment.id" : sortBy));
	
			String sortOrder = (request.getParameter("sortOrder") == null) ? "DESC" : request.getParameter("sortOrder");
			sortOrder = (sortOrder.equals("")) ? "DESC" : sortOrder;
			
			String changeOrder = (request.getParameter("changeOrder") == null) ? "false" : request.getParameter("changeOrder");
			sortOrder = (changeOrder.equals("true")) ? ((sortOrder.equals("DESC")) ? "ASC" 	: "DESC") : sortOrder;
			request.removeAttribute("changeOrder");
			searchCriteria.put("sortOrder", sortOrder);
	
			if(doPagination){
				int pageNumber = (request.getParameter("pageNumber") != null) ? (Integer.parseInt(request.getParameter("pageNumber"))) : 0;
				searchCriteria.put("pageNumber", pageNumber);
				searchCriteria.put("pageSize", PlanMgmtConstants.TEN);
			}else{
				searchCriteria.put("pageNumber", 0);
				searchCriteria.put("pageSize", request.getParameter("totalrecords"));
			}
			
			searchCriteria.put("insuranceType", StringUtils.isNotBlank(request.getParameter("insuranceType")) ? (request.getParameter("insuranceType").equalsIgnoreCase("All") ? null : request.getParameter("insuranceType")) : null);
			searchCriteria.put("status", StringUtils.isNotBlank(request.getParameter("status")) ? (request.getParameter("status").equalsIgnoreCase("All") ? null : request.getParameter("status")) : null);
			searchCriteria.put("subscriberId", StringUtils.isNotBlank(request.getParameter("subscriberid")) ? request.getParameter("subscriberid").trim() : null);
			searchCriteria.put("last4DigitSSN", StringUtils.isNotBlank(request.getParameter("ssn")) ? request.getParameter("ssn").trim() : null);
			searchCriteria.put("dOBofSubscriber", StringUtils.isNotBlank(request.getParameter("subscriberdob")) ? request.getParameter("subscriberdob").trim() : null);
			searchCriteria.put("policyId", StringUtils.isNotBlank(request.getParameter("policyId")) ? request.getParameter("policyId").trim() : null);
			searchCriteria.put("name", StringUtils.isNotBlank(request.getParameter("name")) ? request.getParameter("name").trim() : null);
			
			if(StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATUS)) && request.getParameter(PlanMgmtConstants.STATUS).equals(ENROLLMENT_STATUS_ENROLLED)){
				searchCriteria.put(PlanMgmtConstants.STATUS, ENROLLMENT_STATUS_CONFIRMED); 
			}
			if(StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATUS)) && request.getParameter(PlanMgmtConstants.STATUS).equals(ENROLLMENT_STATUS_ORDER_ED)){
				searchCriteria.put(PlanMgmtConstants.STATUS, ENROLLMENT_STATUS_ORDER_CONFIRMED); 
			}
	
			searchCriteria.put("issuer", issuerId);
			searchCriteria.put("lookup", GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL);
			searchCriteria.put("applicableYear",  StringUtils.isNotBlank(request.getParameter("applicableYear")) ? request.getParameter("applicableYear") : String.valueOf(currentYear));
			searchCriteria.put("plannumber",  StringUtils.isNotBlank(request.getParameter("plannumber")) ? request.getParameter("plannumber").trim() : null);
			searchCriteria.put("isSubscriber", "true");
			
		}
		catch(Exception ex)
		{
			LOGGER.error("Error occurs inside createEnrolleeSearchCriteria() " , ex);
		}
		
		return searchCriteria;
	}
	
	
	/* ##### implementation of call enrollee search API ##### */
	@Override
	public EnrolleeResponse callSearchEnrolleeAPI(EnrollmentSubscriberRequestDTO enrollmentSubscriberRequestDTO, AccountUser loggedInUser){
		EnrolleeResponse enrolleeResponse = new EnrolleeResponse();
		try
		{
			ResponseEntity<String> res = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FETCH_ENROLLMENT_SUBSCRIBER_DATA, 
					loggedInUser.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentSubscriberRequestDTO));
			// call SEARCH_ENROLLEE_URL API of enrollment module
				
			if (res != null) { // if response is not empty
				enrolleeResponse = (EnrolleeResponse) platformGson.fromJson(res.getBody(), EnrolleeResponse.class);
			}
		}
		catch(Exception ex)
		{
			LOGGER.error("Error occurs inside callSearchEnrolleeAPI() " , ex);
		}
		return enrolleeResponse;
	}
	
	
	/* ##### implementation of prepare enrollment details API ##### */
	@Override
	public 	EnrollmentDataDTO callEnrollmentDetailsAPI(Integer enrollmentId, String userName){
		List<EnrollmentDataDTO> enrollmentDTOList  = new ArrayList<EnrollmentDataDTO> ();
		EnrollmentDataDTO enrollmentDataDTO = new EnrollmentDataDTO();
		
		List<Integer> idList = new ArrayList<Integer>();
		idList.add(enrollmentId);
					
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		enrollmentRequest.setIdList(idList);
	    enrollmentRequest.setPremiumDataRequired(true);
		EnrollmentResponse enrollmentResponse = null;
		
		try
		{
		    ResponseEntity<String> res = ghixRestTemplate.exchange(EnrollmentEndPoints.GET_ENROLLMENT_BY_ID_OR_HHCASEIDS, 
		    		userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest));
		    
			if(res != null){
				enrollmentResponse = platformGson.fromJson(res.getBody(), EnrollmentResponse.class);
				Map<String, List<EnrollmentDataDTO>> enrollmentDTOListMap  =  enrollmentResponse.getEnrollmentDataDtoListMap();
				if (enrollmentDTOListMap != null && !enrollmentDTOListMap.isEmpty()) {
					for ( String key : enrollmentDTOListMap.keySet() ) {
					    enrollmentDTOList = enrollmentDTOListMap.get(key);
					    if(enrollmentDTOList.size() > PlanMgmtConstants.ZERO){
					    	enrollmentDataDTO = enrollmentDTOList.get(PlanMgmtConstants.ZERO);
					    }
					}
				}	
			
			}
		}
		catch(Exception ex)
		{
			LOGGER.error("Error occurs inside callEnrollmentDetailsAPI() " , ex);
		}
		
		return enrollmentDataDTO;
	}
	
	
	/* ##### implementation of get Year list ##### */
	@Override
	public List<Integer> getYearsList(){
		List<Integer> yearList = new ArrayList<Integer>();
		try
		{
	      	int nextYear = TSCalendar.getInstance().get(Calendar.YEAR) + 1;
	      	int baseYear = planMgmtUtils.getBaseYear();

	      	int yearDiff = nextYear - baseYear;
	      	for(int i=0; i<=yearDiff; i++){
	      		yearList.add(nextYear - i);
	      	}	      	
		}
		catch(Exception ex){
			LOGGER.error("Exception occured in getYearsList: ", ex);
		}
		return yearList;
	}
	
	
	/* ##### implementation of download/export enrollee search result  ##### */
	@Override
	public void downloadEnrolleeData(HttpServletResponse response, List<EnrollmentSubscriberDTO> enrollmentSubscriberDTOList, String year){
		FileInputStream fis = null;
		FileOutputStream outFile = null;
		
		try{
			List<String[]> list = prepareEnrolleeDataForExcel(enrollmentSubscriberDTOList, year);
			String fileName = "EnrolleeData.xls";
			String filePath = uploadPath + File.separator + fileName;
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				HSSFWorkbook workBook = ExcelGenerator.createEnrollReportExcelSheet(list);
				outFile = new FileOutputStream(filePath);
				workBook.write(outFile);
				outFile.flush();
				outFile.close();
				
				response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
				response.setContentType("application/octet-stream");
				File file = new File(filePath);
				fis =  new FileInputStream(file);
				FileCopyUtils.copy(fis, response.getOutputStream());
				if(file.exists()){
					file.delete();
				}
				fis.close();
								
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("File downloaded successfully!!!");
				}	
					
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		}
		catch(Exception e)
		{
			LOGGER.error("Error occurs inside downloadEnrolleeData() ", e);
		}
		finally
		{
			 IOUtils.closeQuietly(fis);
			 IOUtils.closeQuietly(outFile);
		}
	}
	
	
	/* ##### private method to prepare data for enrollee search report ##### */
	private static  List<String[]> prepareEnrolleeDataForExcel(List<EnrollmentSubscriberDTO> enrollmentSubscriberDTOList, String year){
		List<String[]> enrolleeDetailsList = new ArrayList<String[]>();
		try
		{
			String[] reportDate = new String[2];
			reportDate[0] = "Report Date";
			reportDate[1] = DateUtil.dateToString(TSCalendar.getInstance().getTime(),"MM/dd/yyyy");
			enrolleeDetailsList.add(reportDate);
			
			String[] reportyear = new String[2];
			reportyear[0]= "Year";
			reportyear[1] = year;
			enrolleeDetailsList.add(reportyear);
			
			String[] reportEnrolleeDataHeader = new String[9];
			reportEnrolleeDataHeader[0] = "Subscriber";
			reportEnrolleeDataHeader[1] = "DOB";
			reportEnrolleeDataHeader[2] = "SSN";
			reportEnrolleeDataHeader[3] = "Policy ID";
			reportEnrolleeDataHeader[4] = "Plan Type";
			reportEnrolleeDataHeader[5] = "Plan Number";
			reportEnrolleeDataHeader[6] = "Enrollment Status";
			reportEnrolleeDataHeader[7] = "Effective Start Date";
			reportEnrolleeDataHeader[8] = "Subscriber ID";
			enrolleeDetailsList.add(reportEnrolleeDataHeader);
			
				
			String[] reportEnrolleeData = null; 
			for(EnrollmentSubscriberDTO enrollmentSubscriberDTO: enrollmentSubscriberDTOList){	
				reportEnrolleeData = new String[9];
				reportEnrolleeData[0] = enrollmentSubscriberDTO.getSubscriberFullName();
				reportEnrolleeData[1] = (enrollmentSubscriberDTO.getBirthDate() != null)  ? DateUtil.dateToString(enrollmentSubscriberDTO.getBirthDate(), "MM/dd/yyyy") : PlanMgmtConstants.EMPTY_STRING;
				reportEnrolleeData[2] =  enrollmentSubscriberDTO.getLastFourDigitSSN();
				reportEnrolleeData[3] =  enrollmentSubscriberDTO.getEnrollmentID().toString();
				reportEnrolleeData[4] =  enrollmentSubscriberDTO.getInsuranceTypeLookupLabel();
				reportEnrolleeData[5] =  enrollmentSubscriberDTO.getCMSPlanID();
				reportEnrolleeData[6] =  enrollmentSubscriberDTO.getEnrollmentStatusLookupLabel();
				reportEnrolleeData[7] =  (enrollmentSubscriberDTO.getEnrollmentBenefitStartDate() != null)?  DateUtil.dateToString(enrollmentSubscriberDTO.getEnrollmentBenefitStartDate(), "MM/dd/yyyy") : PlanMgmtConstants.EMPTY_STRING;;
				reportEnrolleeData[8] =  enrollmentSubscriberDTO.getExchgSubscriberIdentifier();
				enrolleeDetailsList.add(reportEnrolleeData);
			}
		}
		catch(Exception ex)
		{
			LOGGER.error("Exception occures inside prepareEnrolleeDataToExcel() " , ex);
		}
		
		return enrolleeDetailsList;
	}


	@Override
	public EnrollmentSubscriberRequestDTO createEnrollmentSubscriberRequest(HttpServletRequest request,
			Integer issuerId, boolean doPagination) {
		EnrollmentSubscriberRequestDTO enrollmentSubscriberRequestDTO = new EnrollmentSubscriberRequestDTO();
		try
		{
			int currentYear = TSCalendar.getInstance().get(Calendar.YEAR);
	
			String sortBy = (request.getParameter("sortBy") == null) ? "enrollmentID" : request.getParameter("sortBy");
			enrollmentSubscriberRequestDTO.setSortByField(sortBy.equals("") ? "enrollmentID" : sortBy);
	
			String sortOrder = (request.getParameter("sortOrder") == null) ? "DESC" : request.getParameter("sortOrder");
			sortOrder = (sortOrder.equals("")) ? "DESC" : sortOrder;
			
			String changeOrder = (request.getParameter("changeOrder") == null) ? "false" : request.getParameter("changeOrder");
			sortOrder = (changeOrder.equals("true")) ? ((sortOrder.equals("DESC")) ? "ASC" 	: "DESC") : sortOrder;
			request.removeAttribute("changeOrder");
			enrollmentSubscriberRequestDTO.setSortOrder(EnrollmentSubscriberRequestDTO.SortOrder.valueOf(sortOrder));
	
			if(doPagination){
				int pageNumber = request.getParameter("pageNumber") != null && StringUtils.isNumeric(request.getParameter("pageNumber"))
								? (Integer.parseInt(request.getParameter("pageNumber")))
								: 0;
				enrollmentSubscriberRequestDTO.setPageNumber(pageNumber);
				enrollmentSubscriberRequestDTO.setPageSize(PlanMgmtConstants.TEN);
			}else{
				enrollmentSubscriberRequestDTO.setPageNumber(0);
				enrollmentSubscriberRequestDTO.setPageSize(StringUtils.isNotBlank(request.getParameter("totalrecords")) && StringUtils.isNumeric(request.getParameter("totalrecords"))
								? Integer.parseInt(request.getParameter("totalrecords"))
								: null);
			}
			enrollmentSubscriberRequestDTO.setInsuranceTypeLookupCode(StringUtils.isNotBlank(request.getParameter("insuranceType")) ? (request.getParameter("insuranceType").equalsIgnoreCase("All") ? null : request.getParameter("insuranceType")) : null);
			enrollmentSubscriberRequestDTO.setEnrollmentStatus(StringUtils.isNotBlank(request.getParameter("status")) ? (request.getParameter("status").equalsIgnoreCase("All") ? null : request.getParameter("status")) : null);
			enrollmentSubscriberRequestDTO.setExchgSubscriberIdentifier(StringUtils.isNotBlank(request.getParameter("subscriberid")) ? request.getParameter("subscriberid").trim() : null);
			enrollmentSubscriberRequestDTO.setSSNLast4Digit(StringUtils.isNotBlank(request.getParameter("ssn")) ? request.getParameter("ssn").trim() : null);
			enrollmentSubscriberRequestDTO.setSubscriberDateOfBirth(request.getParameter("subscriberdob"));
			enrollmentSubscriberRequestDTO.setEnrollmentID(StringUtils.isNotBlank(request.getParameter("policyId"))  && StringUtils.isNumeric(request.getParameter("policyId"))
							? Integer.parseInt(request.getParameter("policyId"))
							: null);
			enrollmentSubscriberRequestDTO.setSubscriberName(StringUtils.isNotBlank(request.getParameter("name")) ? request.getParameter("name").trim() : null);
			
			if(StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATUS)) && request.getParameter(PlanMgmtConstants.STATUS).equals(ENROLLMENT_STATUS_ENROLLED)){
				enrollmentSubscriberRequestDTO.setEnrollmentStatus(ENROLLMENT_STATUS_CONFIRMED); 
			}
			if(StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATUS)) && request.getParameter(PlanMgmtConstants.STATUS).equals(ENROLLMENT_STATUS_ORDER_ED)){
				enrollmentSubscriberRequestDTO.setEnrollmentStatus(ENROLLMENT_STATUS_ORDER_CONFIRMED); 
			}
	
			enrollmentSubscriberRequestDTO.setIssuerID(issuerId);
			enrollmentSubscriberRequestDTO.setEnrollmentTypeLookupCode(GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL);
			enrollmentSubscriberRequestDTO.setCoverageYear(StringUtils.isNotBlank(request.getParameter("applicableYear")) ? request.getParameter("applicableYear") : String.valueOf(currentYear));
			enrollmentSubscriberRequestDTO.setCMSPlanID(StringUtils.isNotBlank(request.getParameter("plannumber")) ? request.getParameter("plannumber").trim() : null);
		}
		catch(Exception ex)
		{
			LOGGER.error("Error occurs inside createEnrolleeSearchCriteria() " , ex);
		}
		return enrollmentSubscriberRequestDTO;
	}
}
