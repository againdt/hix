<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<style type="text/css">

	.ui-autocomplete li {
		padding: 0 5px !important;
		width: 152px !important;
		word-break: break-all;
		word-wrap: break-word;
	}
	
body {
	padding-top: 40px;
	padding-bottom: 0px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

<style type="text/css">
.accordion-inner {
	padding: 10px 20px 0 20px;
}

.accordion-inner  h3 {
	margin-bottom: 15px;
	color: #ccc;
	border-bottom: solid 1px #e1e3e3;
}

.accordion-inner #sliderEmp,.accordion-inner #sliderDep {
	width: 95%;
	margin: 50px 20px 0px 20px;
}
.dob label {display: inline;}
</style>



<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/general.css" />" media="screen" />
<style>
.table th, .table th a {
	background:#666;
	color:#fff;
}
table th {
	height:22px;
}
</style>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<script type="text/javascript" src="<:url value="/resources/js/affiliate/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/affiliate/parsley.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/affiliate/highlight.min.js" />"></script>

<%@ page isELIgnored="false"%>

<div class="gutter10">	
    <div class="row-fluid">
        <ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/affiliate/manage" />">&lt; <spring:message  code="label.back"/></a></li>
            <li>Manage Affiliate</li>
        </ul><!--page-breadcrumb ends-->
            <h1>Manage Affiliate</h1>
    </div>	
    <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
   </div>
    
    <div class="row-fluid">
		<div id="sidebar" class="span3 ">
			<div class="gray">
            	<form class="form-vertical" id="frmsearchaffiliate" name="frmsearchaffiliate" action="<c:url value="/admin/affiliate/manage" />" method="POST">
                	<h4 class="margin0"><spring:message code='label.refineresult' /></h4>
                    <div class="gutter10 graybg">
                    	<div class="control-group">
                            <label for="affiliateName" class="control-label">Affiliate Name</label>
                            <div class="controls compnayAutoComp">
                                <input class="input-medium" type="text" value="${searchCriteria.affiliateName}"  name="affiliateName" id="affiliateName"  size="20"/>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        <div class="control-group">
                            <label for="company" class="control-label">Company</label>
                            <div class="controls compnayAutoComp">
                                <input class="input-medium" type="text" value="${searchCriteria.company}"  name="company" id="company"  size="20"/>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        <div class="control-group">
                            <label for="affiliateId" class="control-label">Affiliate ID</label>
                            <div class="controls">
                                <input class="input-medium" type="text" value="${searchCriteria.affiliateId}"  name="affiliateId" maxlength="10" id="affiliateId"  size="20"/>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        <div class="control-group">
                            <label for="status" class="control-label">Affiliate Status</label>
                            <div class="controls">
                                <c:set var="counter" value="0" />
                                 <select  name="affiliateStatus" id="affiliateStatus" class="input-medium">
                                    <option ${searchCriteria.affiliateStatus == 'ACTIVE' ? 'selected="selected"' : ''} value="ACTIVE" selected >Active</option>
                                    <option ${searchCriteria.affiliateStatus == 'INACTIVE' ? 'selected="selected"' : ''} value="INACTIVE">Inactive</option>
                                    <option ${searchCriteria.affiliateStatus == 'REGISTERED' ? 'selected="selected"' : ''} value="REGISTERED">Registered</option>
                                    <option ${searchCriteria.affiliateStatus == 'SUSPENDED' ? 'selected="selected"' : ''} value="SUSPENDED">Suspended</option>
                                    <option ${searchCriteria.affiliateStatus == 'MULTIPLE' ? 'selected="selected"' : ''} value="MULTIPLE">Multiple</option>
                                    <option ${searchCriteria.affiliateStatus == 'SELECT' ? 'selected="selected"' : ''} value="SELECT">Select</option>
                                </select>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
  
                        <div class="txt-center">
 						<input type="submit" class="btn" value="<spring:message  code='label.submit'/>" id="submit-parsely">
 						</div>
                        
                    </div>
                </form>
                <a href="<c:url value="/admin/affiliate/addaffiliate"/>" class="btn btn-primary span11" id="addAffiliate" name="addAffiliate"><spring:message  code='label.addAffiliate'/></a>
            </div>
		</div><!-- span3 ends-->
        
        <div class="span9 " id="rightpanel">
        	<form class="form-horizontal" id="frmsearchaffiliate" name="frmsearchaffiliate" method="POST">
            	<c:choose>
					<c:when test="${submit != '' || resultSize >= 1}">
                        <c:choose>
                            <c:when test="${fn:length(affiliate) > 0}">
                                <table class="table">
                                    <thead>
                                        <tr class="graydrkbg">
                                             <th scope="col"><dl:sort title="Company" sortBy="companyName" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col"><dl:sort title="Affiliate Name" sortBy="accountExecutive" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col"><dl:sort title="Affiliate ID" sortBy="affiliateId" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col"><dl:sort title="Affiliate Status" sortBy="affiliateStatus" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th></th>
                                        </tr>
                                    </thead>
                                    <c:forEach items="${affiliate}" var="affiliate">
                                            <tr>
                                            
                                                 <td>${affiliate.companyName}</td>
                                                <td>${affiliate.accountExecutive} </td>
                                                <td>${affiliate.affiliateId} </td>
                                                <td>${affiliate.affiliateStatus} </td>
                                                <td>
			                                        <div class="dropdown">
			                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cog"></i><i class="caret"></i></a>
			                                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			                                                <li><a href="<c:url value="/admin/affiliate/viewaffiliate/${affiliate.affiliateId}" />"><i class="icon-refresh"></i><spring:message  code='label.viewAffiliate'/></a></li>
			                                                <li><a href="<c:url value="/admin/affiliate/editaffiliate/${affiliate.affiliateId}" />"><i class="icon-pencil"></i><spring:message  code='label.editAffiliate'/></a></li>
			                                            </ul>
			                                        </div>
				                                 
				                                </td>
                                            </tr>
                                        </c:forEach>
                                </table>
                                <div class="center">
                                    <dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
                                </div>
                             </c:when>
                            <c:otherwise>
                               <h4 class="alert alert-info margin0"><spring:message  code='label.norecords'/></h4>
                            </c:otherwise>
                        </c:choose>	
		 			</c:when>
				</c:choose>
            </form>
		</div><!--span9 ends-->
	</div>	

    <div class="notes" style="display: none">
        <div class="row">
            <div class="span">

                <p>The prototype showcases three scenarios (A, B and C) dependent on
                    a particular Employer's eligibility to use the SHOP Exchange.</p>
            </div>
        </div>
    </div>

</div> <!-- row-fluid -->


<script type="text/javascript">
$(function() {
	$('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/calendar.gif';
		$(this).datepicker({
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true
		});
	});
});

function shiftbox(element,nextelement) {
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength) {
		nextelement = document.getElementById(nextelement);
		nextelement.focus();
	}
}


var company_list ='${companyList}';
var companyArr = company_list.slice(1,company_list.length-1);
var companyData = companyArr.split(',');
$( "#company" ).autocomplete({
    source: companyData
});


var affiliateName_list ='${affiliateNameList}';
var affiliateNameArr = affiliateName_list.slice(1,affiliateName_list.length-1);
var affiliateNameData = affiliateNameArr.split(',');
$( "#affiliateName" ).autocomplete({
    source: affiliateNameData
});

var affiliateId_list ='${affiliateIdList}';
var affiliateIdArr = affiliateId_list.slice(1,affiliateId_list.length-1);
var affiliateIdData = affiliateIdArr.split(',');
$( "#affiliateId" ).autocomplete({
    source: affiliateIdData
});

</script>

<script type="text/javascript">

	/*disable all the keypress except for numbers*/
	$('#affiliateId').keypress(function(e){
	  	if (window.event) { var charCode = window.event.keyCode; }
	    else if (e) { var charCode = e.which; }
	    else { return true; }
	    if (charCode > 31 && (charCode < 48 || charCode > 57)) { return false; }
	    return true;
	  
	});
</script>