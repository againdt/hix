package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpsav.ObjectFactory;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpsav.SubmitAgency3DHSResubRequestType;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;

/**
 * Wrapper Class for SubmitAgency3DHSResubRequestType
 * 
 * @author Nikhil Talreja
 * @since 06-Feb-2014
 */
public class SubmitAgency3DHSResubRequestTypeWrapper implements JSONAware{
	
	private static ObjectFactory factory = null;
	private SubmitAgency3DHSResubRequestType request = null;
	
	private String caseNumber;
	private String sevisId;
	private String requestedCoverageStartDate;
	private boolean fiveYearBarApplicabilityIndicator;
	private String requesterCommentsForHub;
	private String categoryCode;
	
	private static final String CASENUMBER_KEY = "CaseNumber";
	private static final String SEVIS_ID_KEY = "SEVISID";
	private static final String REQUESTEDCOVERAGESTARTDATE_KEY = "RequestedCoverageStartDate";
	private static final String FIVEYEARBARAPPLICABILITYINDICATOR_KEY = "FiveYearBarApplicabilityIndicator";
	private static final String REQUESTERCOMMENTSFORHUB_KEY = "RequesterCommentsForHub";
	private static final String CATEGORYCODE_KEY = "CategoryCode";
	
	public SubmitAgency3DHSResubRequestTypeWrapper() {
		factory = new ObjectFactory();
		request = factory.createSubmitAgency3DHSResubRequestType();
	}
	
	/** 
	 * Request specific validations
	 */
	public SubmitAgency3DHSResubRequestType getRequest() throws HubServiceException{
		
		if(this.request.getCaseNumber() == null){
			throw new HubServiceException("Case Number is required for SubmitAgency3DHSResubRequest");
		}
		
		if(this.request.getSEVISID() == null){
			throw new HubServiceException("SEVISID is required for SubmitAgency3DHSResubRequest");
		}
		
		if(this.request.getRequestedCoverageStartDate() == null){
			throw new HubServiceException("Requested Coverage Start Date is required for SubmitAgency3DHSResubRequest");
		}
		
		return request;
	}
	
	public void setRequest(SubmitAgency3DHSResubRequestType request) {
		this.request = request;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.request.setCaseNumber(caseNumber);
		this.caseNumber = caseNumber;
	}

	public String getSevisId() {
		return sevisId;
	}

	public void setSevisId(String sevisId) {
		this.request.setSEVISID(sevisId);
		this.sevisId = sevisId;
	}

	public String getRequestedCoverageStartDate() {
		return requestedCoverageStartDate;
	}

	public void setRequestedCoverageStartDate(String requestedCoverageStartDate) throws HubServiceException {
		
		try{
			this.request.setRequestedCoverageStartDate(VLPServiceUtil.stringToXMLDate(requestedCoverageStartDate, VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
		}
		catch (Exception e){
			throw new HubServiceException(e);
		}
		
		this.requestedCoverageStartDate = requestedCoverageStartDate;
	}

	public boolean isFiveYearBarApplicabilityIndicator() {
		return fiveYearBarApplicabilityIndicator;
	}

	public void setFiveYearBarApplicabilityIndicator(
			boolean fiveYearBarApplicabilityIndicator) {
		this.request.setFiveYearBarApplicabilityIndicator(fiveYearBarApplicabilityIndicator);
		this.fiveYearBarApplicabilityIndicator = fiveYearBarApplicabilityIndicator;
	}

	public String getRequesterCommentsForHub() {
		return requesterCommentsForHub;
	}

	public void setRequesterCommentsForHub(String requesterCommentsForHub) {
		this.requesterCommentsForHub = requesterCommentsForHub;
		this.request.setRequesterCommentsForHub(requesterCommentsForHub);
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
		this.request.setCategoryCode(categoryCode);
	}
	
	/**
	 * Converts the JSON String to SubmitAgency3DHSResubRequestTypeWrapper object
	 * 
	 * @param jsonString - String representation for SubmitAgency3DHSResubRequestTypeWrapper
	 * @return SubmitAgency3DHSResubRequestTypeWrapper Object 
	 */
	public static SubmitAgency3DHSResubRequestTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create SubmitAgency3DHSResubRequestTypeWrapper from null or empty input");
		}
		
		SubmitAgency3DHSResubRequestTypeWrapper wrapper = new SubmitAgency3DHSResubRequestTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		Object param = null;
		
		param = obj.get(CASENUMBER_KEY);
		if(param != null){
			wrapper.setCaseNumber((String)param);
		}
		
		param = obj.get(SEVIS_ID_KEY);
		if(param != null){
			wrapper.setSevisId((String)param);
		}
		
		param = obj.get(REQUESTEDCOVERAGESTARTDATE_KEY);
		if(param != null){
			try {
				wrapper.setRequestedCoverageStartDate((String)param);
			} catch (Exception e) {
				throw new HubServiceException(e);
			}
		}
		
		param = obj.get(FIVEYEARBARAPPLICABILITYINDICATOR_KEY);
		if(param != null){
			wrapper.setFiveYearBarApplicabilityIndicator((Boolean)param);
		}
		
		param = obj.get(REQUESTERCOMMENTSFORHUB_KEY);
		if(param != null){
			wrapper.setRequesterCommentsForHub((String)param);
		}
		
		param = obj.get(CATEGORYCODE_KEY);
		if(param != null){
			wrapper.setCategoryCode((String)param);
		}
		
		return wrapper;
		
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		obj.put(CASENUMBER_KEY, this.caseNumber);
		obj.put(SEVIS_ID_KEY, this.sevisId);
		obj.put(REQUESTEDCOVERAGESTARTDATE_KEY, this.requestedCoverageStartDate);
		obj.put(FIVEYEARBARAPPLICABILITYINDICATOR_KEY, this.fiveYearBarApplicabilityIndicator);
		obj.put(REQUESTERCOMMENTSFORHUB_KEY, this.requesterCommentsForHub);
		obj.put(CATEGORYCODE_KEY, this.categoryCode);
		return obj.toJSONString();

	}
	
	public ReverifyAgency3InitVerifRequestTypeWrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		
		ReverifyAgency3InitVerifRequestTypeWrapper wrapper = new ReverifyAgency3InitVerifRequestTypeWrapper();
		wrapper.setCaseNumber((String)jsonObj.get(CASENUMBER_KEY));
		wrapper.setSevisid((String)jsonObj.get(SEVIS_ID_KEY));
		wrapper.setRequestedCoverageStartDate((String)jsonObj.get(REQUESTEDCOVERAGESTARTDATE_KEY));
		wrapper.setFiveYearBarApplicabilityIndicator(Boolean.parseBoolean((String)jsonObj.get(FIVEYEARBARAPPLICABILITYINDICATOR_KEY)));
		wrapper.setRequesterCommentsForHub((String)jsonObj.get(REQUESTERCOMMENTSFORHUB_KEY));
		wrapper.setCategoryCode((String)jsonObj.get(CATEGORYCODE_KEY));
		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		
		obj.put(CASENUMBER_KEY, this.caseNumber);
		obj.put(SEVIS_ID_KEY, this.sevisId);
		obj.put(REQUESTEDCOVERAGESTARTDATE_KEY, this.requestedCoverageStartDate);
		obj.put(FIVEYEARBARAPPLICABILITYINDICATOR_KEY, this.fiveYearBarApplicabilityIndicator);
		obj.put(REQUESTERCOMMENTSFORHUB_KEY, this.requesterCommentsForHub);
		obj.put(CATEGORYCODE_KEY, this.categoryCode);

		return obj;
	}
	
}
