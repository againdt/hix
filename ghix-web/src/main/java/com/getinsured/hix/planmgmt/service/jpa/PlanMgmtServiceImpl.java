package com.getinsured.hix.planmgmt.service.jpa;

import static com.getinsured.hix.planmgmt.service.IssuerService.ACCREDITATION_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.ADD_INFO_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.AUTHORITY_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.BENEFITS_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.BROCHURE_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.CERTI_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.DISCLOSURES_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.DOT;
import static com.getinsured.hix.planmgmt.service.IssuerService.MARKETING_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.PLAN_SUPPORT_DOC_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.RATES_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.UNDERSCORE;
import static com.getinsured.hix.platform.util.DateUtil.StringToDate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;

import com.getinsured.hix.account.repository.ITenantPlanRepository;
import com.getinsured.hix.account.repository.ITenantRepository;
import com.getinsured.hix.admin.util.PlanTenantUtil;
import com.getinsured.hix.dto.planmgmt.FormularyDrugItemDTO;
import com.getinsured.hix.dto.planmgmt.FormularyDrugListResponseDTO;
import com.getinsured.hix.dto.planmgmt.FormularyPlanDTO;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.Formulary;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.Network;
import com.getinsured.hix.model.Network.NetworkType;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.PlanInsuranceType;
import com.getinsured.hix.model.Plan.PlanLevel;
import com.getinsured.hix.model.Plan.PlanStatus;
import com.getinsured.hix.model.PlanCompareReport;
import com.getinsured.hix.model.PlanDental;
import com.getinsured.hix.model.PlanDentalBenefit;
import com.getinsured.hix.model.PlanDentalCost;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.PlanHealthBenefit;
import com.getinsured.hix.model.PlanHealthCost;
import com.getinsured.hix.model.PlanRate;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.TenantPlan;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.dto.PlanLightResponseDTO;
import com.getinsured.hix.planmgmt.history.RatesHistoryRendererImpl;
import com.getinsured.hix.planmgmt.mapper.FormularyDrugMapper;
import com.getinsured.hix.planmgmt.mapper.PlanDentalMapper;
import com.getinsured.hix.planmgmt.mapper.PlanHealthMapper;
import com.getinsured.hix.planmgmt.mapper.PlanHealthSerRegMapper;
import com.getinsured.hix.planmgmt.mapper.PlanQDPSerRegMapper;
import com.getinsured.hix.planmgmt.mapper.ServiceAreaMapper;
import com.getinsured.hix.planmgmt.provider.service.ProviderNetworkService;
import com.getinsured.hix.planmgmt.querybuilder.PlanInfoQueryBuilder;
import com.getinsured.hix.planmgmt.repository.IFormularyRepository;
import com.getinsured.hix.planmgmt.repository.IIssuerRepository;
import com.getinsured.hix.planmgmt.repository.IIssuerRepresentativeRepository;
import com.getinsured.hix.planmgmt.repository.IPlanCompareReport;
import com.getinsured.hix.planmgmt.repository.IPlanDentalBenefitRepository;
import com.getinsured.hix.planmgmt.repository.IPlanDentalCostRepository;
import com.getinsured.hix.planmgmt.repository.IPlanDentalRepository;
import com.getinsured.hix.planmgmt.repository.IPlanHealthBenefitRepository;
import com.getinsured.hix.planmgmt.repository.IPlanHealthCostRepository;
import com.getinsured.hix.planmgmt.repository.IPlanHealthRepository;
import com.getinsured.hix.planmgmt.repository.IPlanRateRepository;
import com.getinsured.hix.planmgmt.repository.IPlanRepository;
import com.getinsured.hix.planmgmt.repository.IRegionRepository;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.planmgmt.service.email.IssuerPlanCertificationEmail;
import com.getinsured.hix.planmgmt.service.email.IssuerPlanDeCertificationEmail;
import com.getinsured.hix.planmgmt.service.email.PlanCertificationBulkStatusUpdateEmail;
import com.getinsured.hix.planmgmt.util.ExcelGenerator;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.planmgmt.util.ServiceAreaDetails;
import com.getinsured.hix.planmgmt.util.ZipCounty;
import com.getinsured.hix.platform.audit.service.DisplayAuditService;
import com.getinsured.hix.platform.audit.service.HistoryRendererService;
import com.getinsured.hix.platform.comment.service.CommentService;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.feature.GiFeature;
import com.getinsured.hix.platform.file.reader.FileReader;
import com.getinsured.hix.platform.file.reader.ReaderFactory;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notify.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixConstants.FileType;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints.AHBXEndPoints;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.ConjuctionType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;

@Service("planmgmtService")
@Repository
@Transactional
public class PlanMgmtServiceImpl implements PlanMgmtService, ApplicationContextAware {
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanMgmtServiceImpl.class);
	private static final String UNCHECKED = "unchecked";
	private static final String ISSUERNAME = "issuername";
	private static final String ISSUERID = "issuerid";
	private static final String PLAN_NUMBER = "planNumber";
	private static final String ISSUER_PLAN_NUMBER = "issuerPlanNumber";
	private static final String MARKET = "market";
	private static final String SORT_ORDER = "sortOrder";
	private static final String THE_STATUS = "status";
	private static final String FALSE_STRING = "false";
	private static final String TRUE_STRING = "true";
	private static final String DATE_STRING = "Date";
	private static final String FILE_STRING = "FILE";
	private static final String USER_STRING = "User";
	private static final String PLAN_LEVEL = "planLevel";
	private static final String PLAN_ID = "PlanId";
	private static final String VERIFIED = "verified";
	private static final String ISSUER_VERIFIED_STATUS = "issuerVerificationStatus";
	private static final String STATE = "state";
	private static final String ENROLLMENTAVAILABILITY = "enrollmentAvailability";
	private static final String ENROLLMENT_AVAILABILTY = "enrollmentAvail";

	private static final String EXCEPTION_STRING_1 = "Exception in : ";
	private static final String EXCEPTION_STRING_2 = ", Exception : ";
	private static final String BENEFIT_FILE = "Benefit File";
	private static final String PLAN_SBC_FILE = "Plan SBC";
	private static final String EXCHANGE_TYPE = "exType";
	private static final String TENANT = "tenant";
	private static final String LAST_UPDATE_TIMESTAMP = "lastUpdateTimestamp";
	
		
	// define AHBX plan attribute constants ENDS

	@Autowired private ObjectFactory<QueryBuilder> delegateFactory;
	@Autowired private IPlanRepository iPlanRepository;
	@Autowired private ITenantRepository iTenantRepository;
	@Autowired private ITenantPlanRepository iTenantPlanRepository;
	@Autowired private IPlanHealthRepository iPlanHealthRepository;
	@Autowired private IPlanDentalRepository iPlanDentalRepository;
	@Autowired private IPlanHealthBenefitRepository iPlanHealthBenfitRepository;
	@Autowired private IPlanDentalBenefitRepository iPlanDentalBenfitRepository;
	@Autowired private IPlanDentalCostRepository iPlanDentalCostRepository;
	@Autowired private IPlanHealthCostRepository iPlanHealthCostRepository;
	@Autowired private IIssuerRepository iIssuerRepository;
	@Autowired private IFormularyRepository iFormularyRepository;
	@Autowired private ProviderNetworkService providerNetworkService;
	@Autowired private PlanHealthSerRegMapper planHealthSerRegMapper;
	@Autowired private PlanQDPSerRegMapper planDentalSerRegMapper;
	@Autowired private UserService userService;
	@Autowired private ObjectFactory<DisplayAuditService> objectFactory;
	@Autowired private HistoryRendererService ratesHistoryRendererImpl;
	@Autowired private HistoryRendererService planHistoryRendererImpl;
	@Autowired private CommentTargetService commentTargetService;
	@Autowired private CommentService commentService;
	@Autowired private ContentManagementService ecmService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private IPlanRateRepository iPlanRateRepository;
	@PersistenceUnit private EntityManagerFactory emf;
	@Autowired private IPlanCompareReport iPlanCompareReport;
	//@Autowired private RoleService roleService;
	//@Autowired private EmailService emailService;
	@Autowired private PlanMgmtUtil planMgmtUtils;
	//@PersistenceContext private EntityManager em;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private IssuerService issuerService;
	@Autowired private IIssuerRepresentativeRepository representativeRepository;
	@Autowired private PlanCertificationBulkStatusUpdateEmail planCertificationBulkStatusUpdateEmail;
	@Autowired private IssuerPlanCertificationEmail issuerPlanCertificationEmail;
	@Autowired private IssuerPlanDeCertificationEmail issuerPlanDeCertificationEmail;
	@Autowired private NoticeService noticeService;
	
	
	private static final String DISPLAYTAG_ASC = "1";
	private static final String DISPLAYTAG_DESC = "2";
	private static final int TIME_OUT = 1800;
	public static final String SOURCE_DATE_FORMAT = "yyyy-MM-dd";
	public static final String DESTINATION_DATE_FORMAT = "MM/dd/yyyy";
	private static final String REMOVED = "successfully removed the tenants";
	private static final String DELFAILED = "deletion failed";
	private static final String effectiveDate = "EFF DATE";
	private static final String DOCUMENT_URL = "download/document?documentId=";
	private static final String NA = "NA";

	@Value("#{configProp['uploadPath']}") private String uploadPath;

	@Value("#{configProp['provider.uploadFilePath']}") private String uploadProviderPath;

	@Value("#{configProp['logoPath']}") private String logoPath;
	
	@Value("#{configProp['appUrl']}") private String appUrl;

	@Value(PlanMgmtConstants.DATABASE_TYPE_CONFIG)
	private String DB_TYPE;
	
	private static final String ALFRESCO = "ALFRESCO";
	

	@Override
	@Transactional(readOnly = true)
	public PlanDental getDentalPlan(int id) {
		return iPlanDentalRepository.findOne(id);
	}

	@Override
	@Transactional
	public <T> List<T> listData(Model model, HttpServletRequest request, Class<T> className, String insuranceType) {

		String planTableId = "planTable";
		Integer pageSize = 5;
		request.setAttribute("pageSize", pageSize);

		String pageParam = (new ParamEncoder(planTableId).encodeParameterName(TableTagParameters.PARAMETER_PAGE));
		String param = request.getParameter(pageParam);
		Integer startRecord = 0;
		if (param != null) {
			startRecord = (Integer.parseInt(param) - 1) * pageSize;
		}

		QueryBuilder<T> userQuery = delegateFactory.getObject();
		userQuery.buildObjectQuery(className);
	
		// --- Step 3 : Apply Sort (if needed).
		String sortParam = (new ParamEncoder(planTableId).encodeParameterName(TableTagParameters.PARAMETER_SORT));
		String sortValue = request.getParameter(sortParam);
		String orderParam = (new ParamEncoder(planTableId).encodeParameterName(TableTagParameters.PARAMETER_ORDER));
		String orderValue = request.getParameter(orderParam);
		if (DISPLAYTAG_ASC.equals(orderValue)) {
			userQuery.applySort(sortValue, SortOrder.ASC);
		} else if (DISPLAYTAG_DESC.equals(orderValue)) {
			userQuery.applySort(sortValue, SortOrder.DESC);
		}

		// --- Step 4 : Get the required data (fetched by Query builder from DB.
		List<T> users = userQuery.getRecords(startRecord, pageSize);
		model.addAttribute("resultSize", userQuery.getRecordCount().intValue());
		return users;
	}

	@Override
	@Transactional(readOnly = true)
	public Plan getPlan(int id) {
		return iPlanRepository.findOne(id);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Plan findPlanByIdAndIssuerId(int planId, int issuerId){
		return iPlanRepository.findPlanByIdAndIssuerId(planId, issuerId);
	}

	@Override
	@Transactional
	public Plan savePlan(Plan plan) throws InvalidUserException, GIException, IOException, ContentManagementServiceException {
		if (PlanInsuranceType.HEALTH.toString().equals(plan.getInsuranceType())) {
			PlanHealth planHealth = plan.getPlanHealth();
			if (plan.getPlanHealth().getId() > 0) {
				PlanHealth planHealth2 = iPlanHealthRepository.findOne(plan.getPlanHealth().getId());
				
			}
			if (planHealth.getPlanLevel() != null
					&& planHealth.getPlanLevel().equalsIgnoreCase(PlanLevel.SILVER.toString())
					&& planHealth.getParentPlanId() == 0
					&& planHealth.getCostSharing() == null) {
				planHealth.setCostSharing("CS1");
			}
				planHealth.setLastUpdatedBy(userService.getLoggedInUser().getId());
			
			planHealth.setPlan(plan);
			saveHealthFiles(planHealth);
		} else if (PlanInsuranceType.DENTAL.toString().equals(plan.getInsuranceType())) {
			PlanDental planDental = plan.getPlanDental();
			if (plan.getPlanDental().getId() > 0) {
				PlanDental planDental2 = iPlanDentalRepository.findOne(plan.getPlanDental().getId());
				if (planDental2 != null
						&& planDental.getEffectiveStartDate() != null) {
					planDental.setEffectiveStartDate(planDental2.getEffectiveStartDate());
				}
				if (planDental2 != null
						&& planDental.getEffectiveEndDate() != null) {
					planDental.setEffectiveEndDate(planDental2.getEffectiveEndDate());
				}
			}
				planDental.setLastUpdatedBy(userService.getLoggedInUser().getId());
			
			planDental.setPlan(plan);
			saveDentalFiles(planDental);
		}
		savePlanFiles(plan);
		// when plan end date is null set 31-Dec-next year of plan start year as
		// default end
		if (plan.getEndDate() == null) {
			Date endDate = null;
			Integer setdateval = 31;
			Calendar calendar = TSCalendar.getInstance();
			calendar.setTime(plan.getStartDate());
			Integer addyearval = calendar.get(Calendar.YEAR) + 1;

			Calendar calendar2 = TSCalendar.getInstance();
			calendar2.set(Calendar.YEAR, addyearval);
			calendar2.set(Calendar.DATE, setdateval);
			calendar2.set(Calendar.MONTH, Calendar.DECEMBER);
			endDate = calendar2.getTime();
			plan.setEndDate(endDate);
		}
		/*
		 * Commenting for SERFF changes
		 * plan.setCategory(PlanCategory.INDIVIDUAL.toString());
		 */
		plan.setLastUpdatedBy(userService.getLoggedInUser().getId());
		return iPlanRepository.save(plan);
	}

	private void savePlanFiles(Plan plan) throws GIException, IOException, ContentManagementServiceException {
		String fileId = null;
		if (!isFileUploadedOnDMS(plan.getBrochure())) {
			fileId = uploadAssociatedFiles(plan.getBrochure(), FileType.BROCHURE, Integer.toString(plan.getId()));
			if (null != fileId) {
				plan.setBrochure(fileId);
			}
		}

		if (!isFileUploadedOnDMS(plan.getSupportFile())) {
			fileId = uploadAssociatedFiles(plan.getSupportFile(), FileType.CERTIFICATES, Integer.toString(plan.getId()));
			if (null != fileId) {
				plan.setSupportFile(fileId);
			}
		}
	}

	private void saveDentalFiles(PlanDental planDental) throws GIException, IOException, ContentManagementServiceException {
		String fileId = null;
		if (!isFileUploadedOnDMS(planDental.getActurialValueCertificate())) {
			fileId = uploadAssociatedFiles(planDental.getActurialValueCertificate(), FileType.ACTUARIAL_CERTIFICATES, null);
			planDental.setActurialValueCertificate(fileId);
		}
		if (!isFileUploadedOnDMS(planDental.getBenefitFile())) {
			fileId = uploadAssociatedFiles(planDental.getBenefitFile(), FileType.BENEFITS_FILE, Integer.toString(planDental.getPlan().getId()));
			planDental.setBenefitFile(fileId);
		}
		if (!isFileUploadedOnDMS(planDental.getRateFile())) {
			fileId = uploadAssociatedFiles(planDental.getRateFile(), FileType.RATES_FILE, Integer.toString(planDental.getPlan().getId()));
			planDental.setRateFile(fileId);
		}
	}

	private void saveHealthFiles(PlanHealth planHealth) throws GIException, IOException, ContentManagementServiceException {
		String fileId = null;
		if (!isFileUploadedOnDMS(planHealth.getActurialValueCertificate())) {
			fileId = uploadAssociatedFiles(planHealth.getActurialValueCertificate(), FileType.ACTUARIAL_CERTIFICATES, null);
			planHealth.setActurialValueCertificate(fileId);
		}

		if (!isFileUploadedOnDMS(planHealth.getBenefitFile())) {
			fileId = uploadAssociatedFiles(planHealth.getBenefitFile(), FileType.BENEFITS_FILE, Integer.toString(planHealth.getPlan().getId()));
			planHealth.setBenefitFile(fileId);
		}

		if (!isFileUploadedOnDMS(planHealth.getRateFile())) {
			fileId = uploadAssociatedFiles(planHealth.getRateFile(), FileType.RATES_FILE, Integer.toString(planHealth.getPlan().getId()));
			planHealth.setRateFile(fileId);
		}
	}

	@Override
	public String uploadAssociatedFiles(String fileName, FileType fileType, String planId) throws GIException, IOException, ContentManagementServiceException {
		String fileURL = null;
		Map<String, String> additionalData = new HashMap<String, String>();
		additionalData.put(PLAN_ID, planId);
			String filePath = formDMSFilePath(fileType, additionalData);
			byte[] dataBytes = null;
			
				dataBytes = getFileContent(fileType, fileName);
				fileURL = ecmService.createContent(filePath, fileName, dataBytes
						, ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY, null);
				deleteLocalFile(fileType, fileName);
				// delete the file
			
		
		return fileURL;
	}

	private void deleteLocalFile(FileType fileType, String fileName) {
		String filePath = getFilePath(fileType, fileName);
		boolean isValidPath = planMgmtUtils.isValidPath(filePath);
		if(isValidPath){
			File localFile = new File(filePath);
			if (localFile.exists()) {
				localFile.delete();
			}
		}else{
			LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
		}
	}

	private byte[] getFileContent(FileType fileType, String fileName) throws IOException {
		FileInputStream fis = null;
		byte[] fileContent = null;
		
		try{
			String filePath = getFilePath(fileType, fileName);
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				fis =  new FileInputStream(filePath);
				fileContent = IOUtils.toByteArray(fis);
				fis.close();
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}			
		}catch(Exception e){
			LOGGER.error("Unable to read file : " + ExceptionUtils.getFullStackTrace(e));
		}finally{
			 IOUtils.closeQuietly(fis);
		}
		return fileContent;
	}

	@Override
	public String getFilePath(FileType fileType, String fileName) {
		String actualFilePath = null;
		try{
			boolean isValidPath = planMgmtUtils.isValidPath(uploadPath);
			if(isValidPath){
				if (FileType.ACTUARIAL_CERTIFICATES.equals(fileType)) {
					actualFilePath = uploadPath + File.separator + CERTI_FOLDER + File.separator + fileName;
				} else if (FileType.BENEFITS_FILE.equals(fileType)) {
					actualFilePath = uploadPath + File.separator + BENEFITS_FOLDER + File.separator + fileName;
				} else if (FileType.BROCHURE.equals(fileType)) {
					actualFilePath = uploadPath + File.separator + BROCHURE_FOLDER + File.separator + fileName;
				} else if (FileType.RATES_FILE.equals(fileType)) {
					actualFilePath = uploadPath + File.separator + RATES_FOLDER + File.separator + fileName;
				} else if (FileType.AUTHORITY.equals(fileType)) {
					actualFilePath = uploadPath + File.separator + AUTHORITY_FOLDER + File.separator + fileName;
				} else if (FileType.MARKETING.equals(fileType)) {
					actualFilePath = uploadPath + File.separator + MARKETING_FOLDER + File.separator + fileName;
				} else if (FileType.DISCLOSURES.equals(fileType)) {
					actualFilePath = uploadPath + File.separator + DISCLOSURES_FOLDER + File.separator + fileName;
				} else if (FileType.ACCREDITATION.equals(fileType)) {
					actualFilePath = uploadPath + File.separator + ACCREDITATION_FOLDER + File.separator + fileName;
				} else if (FileType.ADDITIONAL_INFO.equals(fileType)) {
					actualFilePath = uploadPath + File.separator + ADD_INFO_FOLDER + File.separator + fileName;
				} else if (FileType.ADDITIONAL_SUPPORT.equals(fileType)) {
					actualFilePath = uploadPath + File.separator + PLAN_SUPPORT_DOC_FOLDER + File.separator + fileName;
				} else if (FileType.BENEFITS_FILE.equals(fileType)) {
					actualFilePath = uploadPath + File.separator + BENEFITS_FOLDER + File.separator + fileName;
				} else if (FileType.CERTIFICATES.equals(fileType)) {
					actualFilePath = uploadPath + File.separator + PLAN_SUPPORT_DOC_FOLDER + File.separator + fileName;
				} else if (FileType.PROVIDER.equals(fileType)) {
					actualFilePath = uploadProviderPath + File.separator + fileName;
				}
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getFilePath: ", ex);
		}
		return actualFilePath;
	}

	@Override
	@SuppressWarnings(UNCHECKED)
	@Transactional
	public Plan savePlanBenefits(Plan plan, String benefitsFile) throws InvalidUserException, GIException, IOException, ContentManagementServiceException {
		
		FileReader<List<String>> ifpBenefitsReader = ReaderFactory.<List<String>> getReader(benefitsFile, ',', List.class);
		List<List<String>> data = ifpBenefitsReader.readData();
		PlanHealthBenefit planHealthBenefit = null;
		PlanDentalBenefit planDentalBenefit = null;

		if (plan.getInsuranceType().equals(PlanMgmtService.HEALTH)) {
			PlanHealthMapper planHealthMapper = new PlanHealthMapper();
			PlanHealth planHealth = planHealthMapper.mapData(data,
					ifpBenefitsReader.getColumns(), gethealthbenefitMap());
			if (planHealth != null) {

				// after uploaded benefits are mapped, set the start date and
				// end date of the benefits
				Map<String, PlanHealthBenefit> benefitsMap = planHealth.getPlanHealthBenefits();
				for (String key : benefitsMap.keySet()) {
					planHealthBenefit = benefitsMap.get(key);
					planHealthBenefit.setEffStartDate(plan.getStartDate());
					planHealthBenefit.setEffEndDate(plan.getEndDate());
				}

				planHealth.setActurialValueCertificate(plan.getPlanHealth().getActurialValueCertificate());
				planHealth.setPlanLevel(plan.getPlanHealth().getPlanLevel());
				planHealth.setEhbCovered(plan.getPlanHealth().getEhbCovered());
				planHealth.setId(plan.getPlanHealth().getId());
				planHealth.setCreationTimestamp(plan.getPlanHealth().getCreationTimestamp());
				planHealth.setCreated_by(plan.getPlanHealth().getCreated_by());
				planHealth.setBenefitFile(FilenameUtils.getName(benefitsFile));
				planHealth.setLastUpdatedBy(plan.getLastUpdatedBy());
				planHealth.setBenefitEffectiveDate(plan.getPlanHealth().getBenefitEffectiveDate());
				planHealth.setRateEffectiveDate(plan.getPlanHealth().getRateEffectiveDate());
				plan.setPlanHealth(planHealth);
				planHealth.setPlan(plan);
				savePlan(plan);
			}
		} else if (plan.getInsuranceType().equals(PlanMgmtService.DENTAL)) {
			PlanDentalMapper planDentalMapper = new PlanDentalMapper();
			PlanDental planDental = planDentalMapper.mapData(data, ifpBenefitsReader.getColumns(), getDentalbenefitMap());
			
			if (planDental != null) {
				// after uploaded benefits are mapped, set the start date and
				// end date of the benefits
				Map<String, PlanDentalBenefit> benefitsMap = planDental.getPlanDentalBenefits();
				for (String key : benefitsMap.keySet()) {
					planDentalBenefit = benefitsMap.get(key);
					planDentalBenefit.setEffStartDate(plan.getStartDate());
					planDentalBenefit.setEffEndDate(plan.getEndDate());
				}

				planDental.setActurialValueCertificate(plan.getPlanDental().getActurialValueCertificate());
				planDental.setPlanLevel(plan.getPlanDental().getPlanLevel());
				planDental.setId(plan.getPlanDental().getId());
				planDental.setCreationTimestamp(plan.getPlanDental().getCreationTimestamp());
				planDental.setCreatedBy(plan.getPlanDental().getCreatedBy());
				planDental.setBenefitFile(FilenameUtils.getName(benefitsFile));
				planDental.setBenefitEffectiveDate(plan.getPlanDental().getBenefitEffectiveDate());
				planDental.setRateEffectiveDate(plan.getPlanDental().getRateEffectiveDate());
				plan.setPlanDental(planDental);
				planDental.setPlan(plan);
				savePlan(plan);
			}
		}

		return plan;
	}

	@Override
	@Transactional
	public Plan uploadPlanBrochure(Plan plan, String brochureFile) throws InvalidUserException, GIException, IOException, ContentManagementServiceException {
		plan.setBrochure(FilenameUtils.getName(brochureFile));
		savePlan(plan);
		return plan;
	}

	@Override
	@SuppressWarnings(UNCHECKED)
	@Transactional
	public Plan editPlanBenefits(Plan plan, String benefitsFile, String benefitStartDate, String comments) throws InvalidUserException, GIException, IOException, ContentManagementServiceException {
		PlanHealthBenefit planHealthBenefit = null;
		PlanHealthBenefit oldPlanHealthBenefit = null;
		PlanDentalBenefit planDentalBenefit = null;
		PlanDentalBenefit oldPlanDentalBenefit = null;
		Date benefitEffStartDate = null;
		Date endDateForOldBenefits = null;
		Calendar cal = TSCalendar.getInstance();

		Integer userId = null;
		String userName = null;
		userId = userService.getLoggedInUser().getId();
		userName = userService.getLoggedInUser().getUserName();
		Integer cmId = saveComment(plan.getId(), comments, TargetName.PLAN_HEALTH, userId, userName);
		if (benefitStartDate != null && benefitStartDate.length() > 0) {
			benefitEffStartDate = StringToDate(benefitStartDate, "MM/dd/yyyy");
			// --- subtract one day ---
			cal.setTime(benefitEffStartDate);
			cal.add(Calendar.DATE, -1);
			endDateForOldBenefits = cal.getTime();
		}

		if (plan.getInsuranceType().equals(PlanMgmtService.HEALTH)) {
			if (benefitsFile != null && benefitsFile.length() > 0) {
				FileReader<List<String>> ifpBenefitsReader = ReaderFactory.<List<String>> getReader(benefitsFile, ',', List.class);
				List<List<String>> data = ifpBenefitsReader.readData();
				PlanHealthMapper planHealthMapper = new PlanHealthMapper();
				PlanHealth planHealth = planHealthMapper.mapData(data,
						ifpBenefitsReader.getColumns(), gethealthbenefitMap());
				if (planHealth != null) {
					// Change effective end date of (previous set of benefits) =
					// (effective date chosen by admin - 1 day).
					Map<String, PlanHealthBenefit> prevBenefitsMap = plan.getPlanHealth().getPlanHealthBenefits();
					for (String key : prevBenefitsMap.keySet()) {
						oldPlanHealthBenefit = prevBenefitsMap.get(key);
						// if (set of old benefits's end date) = (plan's end
						// date)
						if (plan.getEndDate().equals(oldPlanHealthBenefit.getEffEndDate())) {
							oldPlanHealthBenefit.setEffEndDate(endDateForOldBenefits);
						}
					}

					// after uploaded benefits are mapped, set the start date
					// and end date of the benefits
					Map<String, PlanHealthBenefit> benefitsMap = planHealth.getPlanHealthBenefits();
					for (String key : benefitsMap.keySet()) {
						planHealthBenefit = benefitsMap.get(key);
						planHealthBenefit.setEffStartDate(benefitEffStartDate);
						planHealthBenefit.setEffEndDate(plan.getEndDate());
					}

					// the current planHealth object is created by the mapper,
					// so it has only benefits data
					// we need to merge this object with the existing plan
					// health object/data and then save planHealth object
					planHealth.setId(plan.getPlanHealth().getId());
					planHealth.setActurialValueCertificate(plan.getPlanHealth().getActurialValueCertificate());
					planHealth.setPlanLevel(plan.getPlanHealth().getPlanLevel());
					planHealth.setEhbCovered(plan.getPlanHealth().getEhbCovered());
					planHealth.setCreationTimestamp(plan.getPlanHealth().getCreationTimestamp());
					planHealth.setCreated_by(plan.getPlanHealth().getCreated_by());
					planHealth.setBenefitFile(FilenameUtils.getName(benefitsFile));
					planHealth.setCostSharing(plan.getPlanHealth().getCostSharing());
					planHealth.setParentPlanId(plan.getPlanHealth().getParentPlanId());

					/*
					 * Commenting for SERFF changes
					 * planHealth.setPlanHealthServiceRegion
					 * (plan.getPlanHealth().getPlanHealthServiceRegion());
					 */
					planHealth.setRateFile(plan.getPlanHealth().getRateFile());
					planHealth.setStatus(plan.getPlanHealth().getStatus());
					planHealth.setBenefitEffectiveDate(benefitEffStartDate);
					planHealth.setCommentId(cmId);
					// set updated and upadtedby
					planHealth.setLastUpdatedBy(userId);

					plan.setPlanHealth(planHealth);
					planHealth.setPlan(plan);
					savePlan(plan);
				}
			}
		} else if (plan.getInsuranceType().equals(PlanMgmtService.DENTAL)) {
			if (benefitsFile != null && benefitsFile.length() > 0) {
				FileReader<List<String>> ifpBenefitsReader = ReaderFactory.<List<String>> getReader(benefitsFile, ',', List.class);
				List<List<String>> data = ifpBenefitsReader.readData();
				PlanDentalMapper planDentalMapper = new PlanDentalMapper();
				PlanDental planDental = planDentalMapper.mapData(data, ifpBenefitsReader.getColumns(), getDentalbenefitMap());
				if (planDental != null) {
					// Change effective end date of (previous set of benefits) =
					// (effective date chosen by admin - 1 day).
					Map<String, PlanDentalBenefit> prevBenefitsMap = plan.getPlanDental().getPlanDentalBenefits();
					for (String key : prevBenefitsMap.keySet()) {
						oldPlanDentalBenefit = prevBenefitsMap.get(key);
						// if (set of old benefits's end date) = (plan's end
						// date)
						if (plan.getEndDate().equals(oldPlanDentalBenefit.getEffEndDate())) {
							oldPlanDentalBenefit.setEffEndDate(endDateForOldBenefits);
						}
					}

					// after uploaded benefits are mapped, set the start date
					// and end date of the benefits
					Map<String, PlanDentalBenefit> benefitsMap = planDental.getPlanDentalBenefits();
					for (String key : benefitsMap.keySet()) {
						planDentalBenefit = benefitsMap.get(key);
						planDentalBenefit.setEffStartDate(benefitEffStartDate);
						planDentalBenefit.setEffEndDate(plan.getEndDate());
					}

					// the current planDental object is created by the mapper,
					// so it has only benefits data
					// we need to merge this object with the existing plan
					// health object/data and then save planDental object
					planDental.setId(plan.getPlanDental().getId());
					planDental.setActurialValueCertificate(plan.getPlanDental().getActurialValueCertificate());
					planDental.setPlanLevel(plan.getPlanDental().getPlanLevel());
					planDental.setCreationTimestamp(plan.getPlanDental().getCreationTimestamp());
					planDental.setCreatedBy(plan.getPlanDental().getCreatedBy());
					planDental.setBenefitFile(FilenameUtils.getName(benefitsFile));
					planDental.setCostSharing(plan.getPlanDental().getCostSharing());
					planDental.setParentPlanId(plan.getPlanDental().getParentPlanId());

					/*
					 * Commenting for SERFF changes
					 * planDental.setPlanDentalServiceRegion
					 * (plan.getPlanDental().getPlanDentalServiceRegion());
					 */
					planDental.setRateFile(plan.getPlanDental().getRateFile());
					planDental.setStatus(plan.getPlanDental().getStatus());
					planDental.setBenefitEffectiveDate(benefitEffStartDate);
					planDental.setCommentId(cmId);
					// set updated and upadtedby
					planDental.setLastUpdatedBy(userId);

					plan.setPlanDental(planDental);
					planDental.setPlan(plan);
					savePlan(plan);
				}
			}
		}
		return plan;
	}

	@Override
	public Integer saveComment(Integer id, String comments, TargetName targetName, Integer commentedBy, String commenterName) {
		CommentTarget target = commentTargetService.findByTargetIdAndTargetType(id.longValue(), targetName);
		if (target == null) {
			target = new CommentTarget();
			target.setTargetId(id.longValue());
			target.setTargetName(targetName);
			List<Comment> commentsList = new ArrayList<Comment>();
			target.setComments(commentsList);
		}

		Comment commentObj = new Comment();
		commentObj.setComentedBy(commentedBy);
		commentObj.setComment(comments);
		commentObj.setCommentTarget(target);
		commentObj.setCommenterName(commenterName);
		target.getComments().add(commentObj);

		commentObj = commentService.saveComment(commentObj);
		return commentObj.getId();
	}

	/*
	 * Keeping the time out for rates upload as 30 mins(meaning 1800 seconds).
	 */
	@Override
	@SuppressWarnings(UNCHECKED)
	@Transactional(timeout = TIME_OUT)
	public Plan savePlanRates(Plan plan, String rateFile) throws GIException, IOException, ContentManagementServiceException {
		FileReader<List<String>> ifpBenefitsReader = ReaderFactory.<List<String>> getReader(rateFile, ',', List.class);
		List<List<String>> data = ifpBenefitsReader.readData();

		if (plan.getInsuranceType().equals(PlanMgmtService.HEALTH)) {
			PlanHealth planHealth = plan.getPlanHealth();
				planHealthSerRegMapper.mapAndSaveData(data, ifpBenefitsReader.getColumns(), plan);
				planHealth.setRateFile(FilenameUtils.getName(rateFile));
				planHealth.setRateEffectiveDate(plan.getPlanHealth().getRateEffectiveDate());
				saveHealthPlan(planHealth);
			

		} else if (plan.getInsuranceType().equals(PlanMgmtService.DENTAL)) {
			PlanDental planDental = plan.getPlanDental();
				planDentalSerRegMapper.mapAndSaveData(data, ifpBenefitsReader.getColumns(), plan);
				planDental.setRateFile(FilenameUtils.getName(rateFile));
				planDental.setRateEffectiveDate(plan.getPlanDental().getRateEffectiveDate());
				saveDentalPan(planDental);
			
		}

		return plan;
	}

	private void updateEffectiveDates(List updatedRegions, Date startDate, Date endDate, String insuranceType) {
		if (PlanMgmtService.HEALTH.toString().equals(insuranceType)) {
			LOGGER.info("Inside If block");
		
		} else if (PlanMgmtService.DENTAL.toString().equals(insuranceType)) {
			LOGGER.info("Inside ELSE block");
			
		}

	}

	private void saveDentalPan(PlanDental planDental) throws GIException, IOException, ContentManagementServiceException {
		saveDentalFiles(planDental);
		iPlanDentalRepository.save(planDental);
	}

	private void saveHealthPlan(PlanHealth planHealth) throws GIException, IOException, ContentManagementServiceException {
		saveHealthFiles(planHealth);
		iPlanHealthRepository.save(planHealth);
	}

	@Override
	@Transactional
	public List<Plan> getPlanList(HttpServletRequest request, String insuranceType) {

		if(LOGGER.isDebugEnabled()){
		   LOGGER.debug("PERFORMANCE TESTING OLD METHOD : Fetching Plans stated - " + SecurityUtil.sanitizeForLogging(String.valueOf(new Timestamp(new TSDate().getTime()))));
		}
		
		List<Plan> planList = null;
		List<String> columnNames = new ArrayList<String>();
		List values = new ArrayList();
		List<DataType> dataTypes = new ArrayList<QueryBuilder.DataType>();
		List<ComparisonType> comparisonType = new ArrayList<QueryBuilder.ComparisonType>();
		List<ConjuctionType> conjunctionTypes = new ArrayList<QueryBuilder.ConjuctionType>();
		QueryBuilder<Plan> queryObj = delegateFactory.getObject();
		queryObj.buildObjectQuery(Plan.class);
		Integer pageSize = (Integer) request.getAttribute("pageSize");
		Integer startRecord = (Integer) request.getAttribute("startRecord");
		// Search criteria
		if (insuranceType != null && !insuranceType.equals("")) {
			columnNames.add("insuranceType");
			values.add(insuranceType);
			dataTypes.add(DataType.STRING);
			comparisonType.add(ComparisonType.EQUALS_CASE_IGNORE);
			conjunctionTypes.add(ConjuctionType.AND);
		}
		if ((request.getAttribute(ISSUERNAME) != null)
				&& !request.getAttribute(ISSUERNAME).toString().trim().isEmpty()) {
			String issuername = request.getAttribute(ISSUERNAME).toString();
			columnNames.add("issuer.name");
			values.add(issuername);
			dataTypes.add(DataType.STRING);
			comparisonType.add(ComparisonType.EQUALS);
			conjunctionTypes.add(ConjuctionType.AND);
		}
		if ((request.getAttribute(ISSUERID) != null)
				&& !request.getAttribute(ISSUERID).toString().trim().isEmpty()) {
			String issuerid = request.getAttribute(ISSUERID).toString();
			columnNames.add("issuer.id");
			values.add(Integer.parseInt(issuerid));
			dataTypes.add(DataType.NUMERIC);
			comparisonType.add(ComparisonType.EQ);
			conjunctionTypes.add(ConjuctionType.AND);
		}

		if ((request.getAttribute(PLAN_NUMBER) != null)
				&& !request.getAttribute(PLAN_NUMBER).toString().trim().isEmpty()) {
			String planNumber = request.getAttribute(PLAN_NUMBER).toString();
			columnNames.add(ISSUER_PLAN_NUMBER);
			values.add(planNumber.toUpperCase());
			dataTypes.add(DataType.STRING);
			comparisonType.add(ComparisonType.LIKE);
			conjunctionTypes.add(ConjuctionType.AND);
		}
		if ((request.getAttribute(MARKET) != null)
				&& !request.getAttribute(MARKET).toString().trim().isEmpty()) {
			String market = request.getAttribute(MARKET).toString();
			columnNames.add(MARKET);
			values.add(market);
			dataTypes.add(DataType.STRING);
			comparisonType.add(ComparisonType.EQUALS_CASE_IGNORE);
			conjunctionTypes.add(ConjuctionType.AND);
		}
		if ((request.getAttribute(ENROLLMENTAVAILABILITY) != null)
				&& !request.getAttribute(ENROLLMENTAVAILABILITY).toString().trim().isEmpty()) {
			String enrAvailability = request.getAttribute(ENROLLMENTAVAILABILITY).toString();
			columnNames.add(ENROLLMENT_AVAILABILTY);
			values.add(enrAvailability);
			dataTypes.add(DataType.STRING);
			comparisonType.add(ComparisonType.EQUALS_CASE_IGNORE);
			conjunctionTypes.add(ConjuctionType.AND);
		}
		if ((request.getAttribute(THE_STATUS) != null)
				&& !request.getAttribute(THE_STATUS).toString().trim().isEmpty()) {
			String planStatus = request.getAttribute(THE_STATUS).toString();
			columnNames.add(THE_STATUS);
			values.add(planStatus);
			dataTypes.add(DataType.STRING);
			comparisonType.add(ComparisonType.EQUALS_CASE_IGNORE);
			conjunctionTypes.add(ConjuctionType.AND);
		}
		if ((request.getAttribute(VERIFIED) != null)
				&& !request.getAttribute(VERIFIED).toString().trim().isEmpty()) {
			String verifiedStatus = request.getAttribute(VERIFIED).toString();
			columnNames.add(ISSUER_VERIFIED_STATUS);
			values.add(verifiedStatus);
			dataTypes.add(DataType.STRING);
			comparisonType.add(ComparisonType.EQUALS_CASE_IGNORE);
			conjunctionTypes.add(ConjuctionType.AND);
		}
		// FFM changes starts
		if (DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE).equals(GhixConstants.PHIX)) {
			if ((request.getAttribute(STATE) != null)
					&& !request.getAttribute(STATE).toString().trim().isEmpty()) {
				String state = request.getAttribute(STATE).toString();
				columnNames.add("state");
				values.add(state.toUpperCase());
				dataTypes.add(DataType.STRING);
				comparisonType.add(ComparisonType.LIKE);
				conjunctionTypes.add(ConjuctionType.AND);
			}

			if ((request.getAttribute(EXCHANGE_TYPE) != null)
					&& !request.getAttribute(EXCHANGE_TYPE).toString().trim().isEmpty()) {
				String exType = request.getAttribute(EXCHANGE_TYPE).toString();
				columnNames.add("exchangeType");
				values.add(exType.toUpperCase());
				dataTypes.add(DataType.STRING);
				comparisonType.add(ComparisonType.LIKE);
				conjunctionTypes.add(ConjuctionType.AND);
			}
			if ((request.getAttribute(TENANT) != null)
					&& !request.getAttribute(TENANT).toString().trim().isEmpty()) {
				String tenant = request.getAttribute(TENANT).toString();
				columnNames.add("tenantPlan.tenant.code");
				values.add(tenant.toUpperCase());
				dataTypes.add(DataType.STRING);
				comparisonType.add(ComparisonType.LIKE);
				conjunctionTypes.add(ConjuctionType.AND);
			}
		}
		columnNames.add("applicableYear");
		values.add(Integer.parseInt(request.getAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR).toString()));
		dataTypes.add(DataType.NUMERIC);
		comparisonType.add(ComparisonType.EQ);
		conjunctionTypes.add(ConjuctionType.AND);

		columnNames.add("isDeleted");
		values.add(Plan.IS_DELETED.N.toString());
		dataTypes.add(DataType.STRING);
		comparisonType.add(ComparisonType.EQUALS);
		conjunctionTypes.add(ConjuctionType.AND);
		// Change for replacing plan name with plan number column - start

		// Change for replacing plan name with plan number column - end

		// FFM changes ends

		// for plan levels search
		List<String> selectedLevels = new ArrayList<String>();
		Boolean applyPlanLevel = false;
		if (request.getParameter(PLAN_LEVEL) != null
				&& !request.getParameter(PLAN_LEVEL).isEmpty()) {
			selectedLevels.add(request.getParameter(PLAN_LEVEL).toString());
			applyPlanLevel = true;
		}

		if (insuranceType != null
				&& insuranceType.equalsIgnoreCase(PlanInsuranceType.HEALTH.toString())) {
			if (applyPlanLevel) {// if any one/multiple plan levels are selected
				queryObj.applyWhere("planHealth.planLevel", selectedLevels, DataType.LIST, ComparisonType.EQ);
			}
		}
		if (insuranceType != null
				&& insuranceType.equalsIgnoreCase(PlanInsuranceType.DENTAL.toString())) {
			if (applyPlanLevel) { // if any one/multiple plan levels are
									// selected
				queryObj.applyWhere("planDental.planLevel", selectedLevels, DataType.LIST, ComparisonType.EQ);
			}
			columnNames.add("planDental.parentPlanId");
			values.add(0);
			dataTypes.add(DataType.NUMERIC);
			comparisonType.add(ComparisonType.EQ);
			conjunctionTypes.add(ConjuctionType.AND);
		}

		queryObj.applyWhereClauses(columnNames, values, dataTypes, comparisonType, conjunctionTypes);

		List<String> sortColumnNames = new ArrayList<String>();
		SortOrder sortOrder = null;
		boolean hasDateTimeSort = false;

		// Sort criteria queryObj.applySort method override for changing the character case of the column values for sorting
		if ((request.getAttribute("sortBy") != null)
				&& !request.getAttribute("sortBy").toString().trim().isEmpty()) {

			String sortBy = request.getAttribute("sortBy").toString().trim();

			if (sortBy != null && !sortBy.isEmpty()) {

				if (sortBy.equalsIgnoreCase("level")) {

					if (insuranceType != null && insuranceType.equals(PlanMgmtService.DENTAL)) {
						sortColumnNames.add("planDental.planLevel");
					}
					else if (insuranceType != null && insuranceType.equals(PlanMgmtService.HEALTH)) {
						sortColumnNames.add("planHealth.planLevel");
					}
				}
				else if (sortBy.equalsIgnoreCase("name")) {
					sortColumnNames.add("name");
				}
				else if (sortBy.equalsIgnoreCase("issuer")) {
					sortColumnNames.add("issuer.name");
				}
				else if (sortBy.equalsIgnoreCase("issuer.name")) {
					sortColumnNames.add("issuer.name");
				}
				else if (sortBy.equalsIgnoreCase("submitedon") || sortBy.equalsIgnoreCase("createdOn")) {
					hasDateTimeSort = true;
					sortColumnNames.add("creationTimestamp");
				}
				else if (sortBy.equalsIgnoreCase("certifiedon")) {
					hasDateTimeSort = true;
					sortColumnNames.add("certifiedOn");
				}
				else if (sortBy.equalsIgnoreCase("planmarket")) {
					sortColumnNames.add(MARKET);
				}
				else if (sortBy.equalsIgnoreCase("planStatus")) {
					sortColumnNames.add(THE_STATUS);
				}
				else if (sortBy.equalsIgnoreCase("enrollment")) {
					sortColumnNames.add(ENROLLMENT_AVAILABILTY);
				}
				else if (sortBy.equalsIgnoreCase("verified")) {
					sortColumnNames.add(ISSUER_VERIFIED_STATUS);
				}
				else if (sortBy.equalsIgnoreCase(LAST_UPDATE_TIMESTAMP)) {
					hasDateTimeSort = true;
					sortColumnNames.add(LAST_UPDATE_TIMESTAMP);
				}
				// FFM changes starts
				else if (DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE).equals(GhixConstants.PHIX) &&
						sortBy.equalsIgnoreCase("state")) {
					sortColumnNames.add(STATE);
				}// FFM changes ends
				else {
					LOGGER.warn("Plan search sorting ignored, as sorting not supported for column ".intern() + sortBy);
				}
				sortOrder = SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString());
			}
			else {
				hasDateTimeSort = true;
				sortColumnNames.add(LAST_UPDATE_TIMESTAMP);
				sortOrder = SortOrder.DESC;
			}
		}
		else {
			hasDateTimeSort = true;
			sortColumnNames.add(LAST_UPDATE_TIMESTAMP);
			sortOrder = SortOrder.DESC;
		}
		sortColumnNames.add("issuerPlanNumber");

		if (hasDateTimeSort) {
			queryObj.applySortList(sortColumnNames, sortOrder, null);
		}
		else {
			queryObj.applySortList(sortColumnNames, sortOrder, "UPPER");
		}

		planList = queryObj.getRecords(startRecord, pageSize);
		request.setAttribute("resultSize", queryObj.getRecordCount().intValue());

		if (LOGGER.isDebugEnabled()) {
		   LOGGER.debug("PERFORMANCE TESTING OLD METHOD : Fetch Over - " + SecurityUtil.sanitizeForLogging(String.valueOf(new Timestamp(new TSDate().getTime()))));
		}  
		return planList;
	}

	//@Override
	//@Transactional
	/*public List<Plan> getPlanListOptimized(HttpServletRequest request, String insuranceType) {
		List<Plan> planList = null;

			LOGGER.info("PERFORMANCE TESTING : Fetching Plans stated - "
					+ new Timestamp(new TSDate().getTime()));

			StringBuilder queryObj = new StringBuilder();
			StringBuilder queryForCount = new StringBuilder();

			queryObj.append(" SELECT t.* FROM ( ");
			queryObj.append(" SELECT ROWNUM AS rn , t.* FROM (SELECT p.id, p.issuer_plan_number, p.name as planName, p.issuer_id, "
					+ " i.name as issuerName, phd.plan_level, p.market, p.last_update_timestamp, p.status, p.state, p.issuer_verification_status "
					+ " FROM plan p INNER JOIN issuers i ON  p.issuer_id = i.id ");

			queryForCount.append(" SELECT count(*) FROM plan p INNER JOIN issuers i ON  p.issuer_id = i.id ");

			// for plan levels search
			String selectedLevels = "";
			Boolean applyPlanLevel = false;
			if (request.getParameter(PLAN_LEVEL) != null
					&& !request.getParameter(PLAN_LEVEL).isEmpty()) {
				selectedLevels = request.getParameter(PLAN_LEVEL).toString();
				applyPlanLevel = true;
			}

			if (DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE).equals(GhixConstants.PHIX)) {
				queryObj.append(" INNER JOIN pm_tenant_plan tp on p.id = tp.plan_id INNER JOIN tenant t on t.id = tp.tenant_id ");
				queryForCount.append(" INNER JOIN pm_tenant_plan tp on p.id = tp.plan_id INNER JOIN tenant t on t.id = tp.tenant_id ");
			}
			
			// Apply inner join for plan health
			if (insuranceType != null
					&& insuranceType.equalsIgnoreCase(PlanInsuranceType.HEALTH.toString())) {
				queryObj.append(" INNER JOIN plan_health phd ON p.id = phd.plan_id ");
				queryForCount.append(" INNER JOIN plan_health phd ON p.id = phd.plan_id ");
			}

			// Apply inner join for plan dental
			if (insuranceType != null
					&& insuranceType.equalsIgnoreCase(PlanInsuranceType.DENTAL.toString())) {
				queryObj.append(" INNER JOIN plan_dental phd ON p.id = phd.plan_id ");
				queryForCount.append(" INNER JOIN plan_dental phd ON p.id = phd.plan_id ");
			}

			// where condition starts here
			queryObj.append(" WHERE ");
			queryForCount.append(" WHERE ");

			// if any one/multiple plan levels are selected
			if (applyPlanLevel) {
				queryObj.append(" phd.plan_level = '" + selectedLevels
						+ "' AND ");
				queryForCount.append(" phd.plan_level = '" + selectedLevels
						+ "' AND ");
			}

			if (insuranceType != null
					&& insuranceType.equalsIgnoreCase(PlanInsuranceType.DENTAL.toString())) {
				if (applyPlanLevel) {
					queryObj.append(" phd.plan_level = '" + selectedLevels
							+ "' AND ");
					queryForCount.append(" phd.plan_level = '" + selectedLevels
							+ "' AND ");
				}
				queryObj.append(" phd.parent_plan_id = " + 0 + " AND ");
				queryForCount.append(" phd.parent_plan_id = " + 0 + " AND ");
			}

			// to avoid deleted plans
			queryObj.append(" p.is_deleted='" + Plan.IS_DELETED.N.toString()
					+ "' ");
			queryForCount.append(" p.is_deleted='"
					+ Plan.IS_DELETED.N.toString() + "' ");

			// Search criteria
			if (insuranceType != null && !insuranceType.equals("")) {
				queryObj.append(" AND p.insurance_type = '" + insuranceType
						+ "' ");
				queryForCount.append(" AND p.insurance_type = '"
						+ insuranceType + "' ");
			}

			if ((request.getAttribute(ISSUERNAME) != null)
					&& !request.getAttribute(ISSUERNAME).toString().trim().isEmpty()) {
				queryObj.append(" AND i.name = '"
						+ request.getAttribute(ISSUERNAME).toString().trim()
						+ "' ");
				queryForCount.append(" AND i.name = '"
						+ request.getAttribute(ISSUERNAME).toString().trim()
						+ "' ");
			}

			if ((request.getAttribute(ISSUERID) != null)
					&& !request.getAttribute(ISSUERID).toString().trim().isEmpty()) {
				queryObj.append(" AND p.issuer_id = "
						+ Integer.parseInt(request.getAttribute(ISSUERID).toString())
						+ " ");
				queryForCount.append(" AND p.issuer_id = "
						+ Integer.parseInt(request.getAttribute(ISSUERID).toString())
						+ " ");
			}

			if ((request.getAttribute(PLAN_NUMBER) != null)
					&& !request.getAttribute(PLAN_NUMBER).toString().trim().isEmpty()) {
				queryObj.append(" AND p.issuer_plan_number = '"
						+ request.getAttribute(PLAN_NUMBER) + "' ");
				queryForCount.append(" AND p.issuer_plan_number = '"
						+ request.getAttribute(PLAN_NUMBER) + "' ");
			}

			if ((request.getAttribute(MARKET) != null)
					&& !request.getAttribute(MARKET).toString().trim().isEmpty()) {
				queryObj.append(" AND p.market = '"
						+ request.getAttribute(MARKET).toString() + "' ");
				queryForCount.append(" AND p.market = '"
						+ request.getAttribute(MARKET).toString() + "' ");
			}

			if ((request.getAttribute(THE_STATUS) != null)
					&& !request.getAttribute(THE_STATUS).toString().trim().isEmpty()) {
				queryObj.append(" AND p.status = '"
						+ request.getAttribute(THE_STATUS).toString() + "' ");
				queryForCount.append(" AND p.status = '"
						+ request.getAttribute(THE_STATUS).toString() + "' ");
			}

			if ((request.getAttribute(VERIFIED) != null)
					&& !request.getAttribute(VERIFIED).toString().trim().isEmpty()) {
				queryObj.append(" AND p.verified = '"
						+ request.getAttribute(VERIFIED).toString() + "' ");
				queryForCount.append(" AND p.verified = '"
						+ request.getAttribute(VERIFIED).toString() + "' ");
			}
			// FFM changes starts
			if (DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE).equals(GhixConstants.PHIX)) {
				if ((request.getAttribute(STATE) != null)
						&& !request.getAttribute(STATE).toString().trim().isEmpty()) {
					String state = request.getAttribute(STATE).toString();
					queryObj.append(" AND p.state = '" + state + "' ");
					queryForCount.append(" AND p.state = '" + state + "' ");
				}
			
				if ((request.getAttribute(TENANT) != null)
						&& !request.getAttribute(TENANT).toString().trim().isEmpty()) {
					String tenant = request.getAttribute(TENANT).toString();
					queryObj.append(" AND t.id = '" + tenant + "' ");
					queryForCount.append(" AND t.id = '" + tenant + "' ");
				}		
			}
			// Sort criteria
			// queryObj.applySort method override for changing the character
			// case of
			// the column values for sorting
			if ((request.getAttribute("sortBy") != null)
					&& !request.getAttribute("sortBy").toString().trim().isEmpty()) {
				String sortBy = request.getAttribute("sortBy").toString().trim();
				if (sortBy != null && !sortBy.isEmpty()) {
					if (sortBy.equalsIgnoreCase("level")) {
						queryObj.append(" order by p.enrollment_avail "
								+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString()));

					} else if (sortBy.equalsIgnoreCase("plannumber")) {
						queryObj.append(" order by p.issuer_plan_number "
								+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString()));

					} else if (sortBy.equalsIgnoreCase("name")) {
						queryObj.append(" order by p.name "
								+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString()));
					
					}
					else if (sortBy.equalsIgnoreCase("issuer.name")) {
						queryObj.append(" order by i.name "
								+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString()));

					} else if (sortBy.equalsIgnoreCase("submitedon")
							|| sortBy.equalsIgnoreCase("createdOn")) {
						queryObj.append(" order by p.creation_timestamp "
								+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString()));

					} else if (sortBy.equalsIgnoreCase("certifiedon")) {
						queryObj.append(" order by p.certified_on "
								+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString()));

					} else if (sortBy.equalsIgnoreCase("planmarket")) {
						queryObj.append(" order by p.market "
								+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString()));

					} else if (sortBy.equalsIgnoreCase("planStatus")) {
						queryObj.append(" order by p.status "
								+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString()));

					} else if (sortBy.equalsIgnoreCase("enrollment")) {
						queryObj.append(" order by p.enrollment_avail "
								+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString()));

					} else if (sortBy.equalsIgnoreCase(VERIFIED)) {
						queryObj.append(" order by p.issuer_verification_status "
								+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString()));

					} else if (sortBy.equalsIgnoreCase(STATE)) {
						// FFM changes starts
						if (DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE).equals(GhixConstants.PHIX)) {
							queryObj.append(" order by p.state "
									+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString()));
						}// FFM changes ends
					} else {
						queryObj.append(" order by "
								+ sortBy.toLowerCase()
								+ " "
								+ SortOrder.valueOf(request.getAttribute(SORT_ORDER).toString()));
					}
				} else {
					// default sort by created date
					queryObj.append(" order by p.creation_timestamp "
							+ SortOrder.DESC);
				}
			} else {
				// default sort by created date
				queryObj.append(" order by p.creation_timestamp "
						+ SortOrder.DESC);
			}

			Integer pageSize = (Integer) request.getAttribute("pageSize");
			Integer startRecord = (Integer) request.getAttribute("startRecord");

			queryObj.append(" ) t where rownum <= " + (startRecord + pageSize)
					+ " ) t ");
			queryObj.append(" WHERE rn > " + startRecord);

			EntityManager em = emf.createEntityManager();
			List rsList = em.createNativeQuery(queryObj.toString()).getResultList();
			Iterator rsIterator = rsList.iterator();

			LOGGER.info("Build query " + queryObj);

			planList = new ArrayList<Plan>();

			while (rsIterator.hasNext()) {
				Object[] objArray = (Object[]) rsIterator.next();
				Plan plan = new Plan();
				plan.setId(Integer.parseInt(objArray[1].toString()));
				plan.setIssuerPlanNumber(objArray[2].toString());
				plan.setName(objArray[3].toString());

				Issuer issuer = new Issuer();
				issuer.setId(Integer.parseInt(objArray[4].toString()));
				issuer.setName(objArray[5].toString());
				plan.setIssuer(issuer);

				if (insuranceType != null
						&& insuranceType.equalsIgnoreCase(PlanInsuranceType.HEALTH.toString())) {
					PlanHealth planHealth = new PlanHealth();
					if (objArray[6] != null) {
						planHealth.setPlanLevel(objArray[6].toString());
					} else {
						planHealth.setPlanLevel("");
					}
					plan.setPlanHealth(planHealth);
				}

				// Apply inner join for plan dental
				if (insuranceType != null
						&& insuranceType.equalsIgnoreCase(PlanInsuranceType.DENTAL.toString())) {
					PlanDental planDental = new PlanDental();
					if (objArray[6] != null) {
						planDental.setPlanLevel(objArray[6].toString());
					} else {
						planDental.setPlanLevel("");
					}
					plan.setPlanDental(planDental);
				}

				plan.setMarket(objArray[7].toString());

				Date date = DateUtil.StringToDate((objArray[8].toString()), REQUIRED_DATE_FORMAT);
				plan.setLastUpdateTimestamp(date);
				plan.setStatus(objArray[9].toString());
				plan.setState(objArray[10].toString());

				planList.add(plan);
			}

			int resultCount = 0;
			Object objCount = em.createNativeQuery(queryForCount.toString()).getSingleResult();
			LOGGER.info("Build query " + queryForCount);
			if (objCount != null) {
				resultCount = Integer.parseInt(objCount.toString());
			}

			// TODO: added for testing the above logic, below should be replaced
			// with the actual count
			request.setAttribute("resultSize", resultCount);
			// request.setAttribute("resultSize",
			// queryObj.getRecordCount().intValue());

		

		LOGGER.info("PERFORMANCE TESTING : Fetch over - "
				+ new Timestamp(new TSDate().getTime()));

		return planList;
	}*/


	@Override
	@Transactional(timeout = TIME_OUT)
	public void addCostSharingPlan(String planId, String costSharing, String certiFile, String benefitsFile) throws InvalidUserException, GIException, IOException, ContentManagementServiceException, CloneNotSupportedException {
		Object[] planId1 = iPlanHealthRepository.getPlanHealthIdByParentId(planId, costSharing);
		List<PlanRate> planRatesList = new ArrayList<PlanRate>();
		Plan newPlan = null;
		if (planId1 != null) {
			boolean hasCertiFile = false;
			PlanHealth planHealth2 = iPlanHealthRepository.findOne((Integer) planId1[0]);
			if (certiFile != null && certiFile.length() > 0) {
				planHealth2.setActurialValueCertificate(FilenameUtils.getName(certiFile));
				hasCertiFile = true;
			}
			if (benefitsFile != null && benefitsFile.length() > 0) {
				/*
				 * FileReader<PlanHealth> ifpBenefitsReader =
				 * ReaderFactory.<PlanHealth> getReader(benefitsFile,
				 * ',',PlanHealth.class); List<PlanHealth> benefits =
				 * ifpBenefitsReader.readData();
				 */
				FileReader<List<String>> ifpBenefitsReader = ReaderFactory.<List<String>> getReader(benefitsFile, ',', List.class);
				List<List<String>> data = ifpBenefitsReader.readData();
				PlanHealthMapper planHealthMapper = new PlanHealthMapper();
				PlanHealth planHealth1 = planHealthMapper.mapData(data,
						ifpBenefitsReader.getColumns(), gethealthbenefitMap());

				
				// --- maintain the ids to delete old benefits ---
				List<Integer> oldBenefits = new ArrayList<Integer>();
				for (String key : planHealth2.getPlanHealthBenefits().keySet()) {
					// oldBenefits.add(planHealth2.getPlanHealthBenefits().get(key));
					oldBenefits.add(planHealth2.getPlanHealthBenefits().get(key).getId());
				}

				// PlanHealth planHealth1 = benefits.get(0);
				planHealth1.setId(planHealth2.getId());
				planHealth1.setCostSharing(costSharing);
				planHealth1.setPlanLevel(planHealth2.getPlanLevel());
				planHealth1.setEhbCovered(planHealth2.getEhbCovered());
				planHealth1.setCreationTimestamp(planHealth2.getCreationTimestamp());
				planHealth1.setCreated_by(planHealth2.getCreated_by());
				planHealth1.setBenefitFile(FilenameUtils.getName(benefitsFile));
				Plan p = planHealth2.getPlan();
				planHealth1.setPlan(p);
				p.setPlanHealth(planHealth1);
				planHealth1.setParentPlanId(planHealth2.getParentPlanId());

				/* New Code added for PlanRate */

				// --- maintain the ids to delete old plan rate ---
				List<PlanRate> oldPlanRatesList = new ArrayList<PlanRate>();
				oldPlanRatesList = getPlanRate((Integer) planId1[1]);

				planRatesList = getPlanRate(Integer.parseInt(planId));
				List<PlanRate> newRates = new ArrayList<PlanRate>();

				if (planRatesList.size() > 0) {
					for (PlanRate planRate : planRatesList) {
						PlanRate newRate = planRate.clone();
						newRates.add(newRate);
						newRate.setPlan(p);
					}
					p.setPlanRate(newRates);
				}

				if (hasCertiFile) {
					planHealth1.setActurialValueCertificate(FilenameUtils.getName(certiFile));
				} else {
					planHealth1.setActurialValueCertificate(planHealth2.getActurialValueCertificate());
				}


				savePlan(planHealth1.getPlan());

				// --- delete old benefits ---
				for (int i : oldBenefits) {
					iPlanHealthBenfitRepository.delete(i);
				}

				// -- delete old plan rates ---------
				if (oldPlanRatesList.size() > 0) {
					for (PlanRate planRate2 : oldPlanRatesList) {
						iPlanRateRepository.delete(planRate2);
					}
				}

			} else if (hasCertiFile) {
				saveHealthPlan(planHealth2);
			}
		} else {
			Plan parentPlan = iPlanRepository.findOne(Integer.parseInt(planId));

				newPlan = parentPlan.clone();

			FileReader<List<String>> ifpBenefitsReader = ReaderFactory.<List<String>> getReader(benefitsFile, ',', List.class);
			List<List<String>> data = ifpBenefitsReader.readData();
			PlanHealthMapper planHealthMapper = new PlanHealthMapper();
			PlanHealth newPlanHealth = planHealthMapper.mapData(data,
					ifpBenefitsReader.getColumns(), gethealthbenefitMap());

			PlanHealth parentPlanHealth = parentPlan.getPlanHealth();
			newPlanHealth.setActurialValueCertificate(FilenameUtils.getName(certiFile));
			newPlanHealth.setPlanLevel(parentPlanHealth.getPlanLevel());
			newPlanHealth.setEhbCovered(parentPlanHealth.getEhbCovered());
			newPlanHealth.setCostSharing(costSharing);

			newPlanHealth.setBenefitFile(FilenameUtils.getName(benefitsFile));

			newPlanHealth.setRateFile(FilenameUtils.getName(parentPlanHealth.getRateFile()));
			planRatesList = getPlanRate(Integer.parseInt(planId));
			List<PlanRate> newRates = new ArrayList<PlanRate>();
			if (planRatesList.size() > 0) {
				for (PlanRate planRate : planRatesList) {
					PlanRate newRate = planRate.clone();
					newRates.add(newRate);
					newRate.setPlan(newPlan);
				}
				newPlan.setPlanRate(newRates);
			}

			newPlanHealth.setParentPlanId(Integer.parseInt(planId));
			// newPlanHealth.setPlanHealthServiceRegion(newRegions);
			newPlanHealth.setPlan(newPlan);
			newPlan.setPlanHealth(newPlanHealth);

			savePlan(newPlan);
		}
	}

	private List<PlanRate> getPlanRate(int planid) {
		List<PlanRate> parentPlanRateList = new ArrayList<PlanRate>();
		parentPlanRateList = iPlanRateRepository.getPlanRates(planid);
		return parentPlanRateList;
	}

	@Override
	@Transactional
	public Plan updatePlan(Plan plan) {
		Plan existingPlan = iPlanRepository.findOne(plan.getId());
		boolean isPlanEndDateChanged = (!existingPlan.getEndDate().equals(plan.getEndDate()));
		Date existingEndDate = existingPlan.getEndDate();
		copyPlanData(existingPlan, plan);
		List<Integer> childPlanIds = iPlanHealthRepository.getChildPlanIds(Integer.toString(plan.getId()));
		Plan childPlan = null;
		for (Integer childPlanId : childPlanIds) {
			childPlan = iPlanRepository.findOne(childPlanId);
			copyPlanData(childPlan, plan);
			iPlanRepository.save(childPlan);
		}
		if (isPlanEndDateChanged) {
			changeAssociatedEndDates(existingPlan, plan, existingEndDate);
		}
		return iPlanRepository.save(existingPlan);
	}

	@Override
	@Transactional
	public void updateNetworkURL(Integer planId, String networkURL, Integer userId) {
		if (null != planId && planId > 0 && StringUtils.isNotBlank(networkURL)) {
			try {
				Plan existingPlan = iPlanRepository.findOne(planId);	
				if(existingPlan!=null) {
					existingPlan.getNetwork().setNetworkURL(networkURL);
					existingPlan.setLastUpdatedBy(userId);
					iPlanRepository.save(existingPlan);
				}
			} catch (Exception e) {
				LOGGER.error("Error in updateNetworkURL() : " , e.getMessage());
			}
		} 				
	}
	
	@Override
	@Transactional
	public void updateFomularyURL(Integer planId, String formularyURL, Integer userId) {
		if (null != planId && planId > 0 && StringUtils.isNotBlank(formularyURL)) {
			try {
				Plan existingPlan = iPlanRepository.findOne(planId);
				Formulary formulary = null;
				if(existingPlan!=null) {
					if (StringUtils.isNotBlank(existingPlan.getFormularlyId())) {
						formulary = iFormularyRepository.findById(Integer.parseInt(existingPlan.getFormularlyId()));
						formulary.setFormularyUrl(formularyURL);
						iFormularyRepository.save(formulary);
					}
				}
			} catch (Exception e) {
				LOGGER.error("Error in updateFomularyURL() : " , e.getMessage());
			}
		} 				
	}
	
	private void changeAssociatedEndDates(Plan existingPlan, Plan plan, Date existingEndDate) {
		if (existingPlan.getInsuranceType().equals(PlanMgmtService.HEALTH)) {
			// ---- change benefits end date ----
			Map<String, PlanHealthBenefit> benefits = existingPlan.getPlanHealth().getPlanHealthBenefits();
			for (String key : benefits.keySet()) {
				PlanHealthBenefit benefit = benefits.get(key);
				if (benefit.getEffEndDate().equals(existingEndDate)) {
					benefit.setEffEndDate(plan.getEndDate());
				}
			}
			// ---- change rates end date ----
		
		} else if (existingPlan.getInsuranceType().equals(PlanMgmtService.DENTAL)) {
			// ---- change benefits end date ----
			Map<String, PlanDentalBenefit> benefits = existingPlan.getPlanDental().getPlanDentalBenefits();
			for (String key : benefits.keySet()) {
				PlanDentalBenefit benefit = benefits.get(key);
				if (benefit.getEffEndDate().equals(existingEndDate)) {
					benefit.setEffEndDate(plan.getEndDate());
				}
			}
			// ---- change rates end date ----
			
		}

	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public Plan updatePlanBenefits(Plan plan, String benefitsFile) throws InvalidUserException, GIException, IOException, ContentManagementServiceException {
		List<Integer> benefitIds = new ArrayList<Integer>();
		FileReader<List<String>> ifpBenefitsReader = ReaderFactory.<List<String>> getReader(benefitsFile, ',', List.class);
		List<List<String>> data = ifpBenefitsReader.readData();

		if (plan.getInsuranceType().equals(PlanMgmtService.HEALTH)) {
			PlanHealth existingPlanHealth = plan.getPlanHealth();
			Map<String, PlanHealthBenefit> benefitsMap = existingPlanHealth.getPlanHealthBenefits();

			/*
			 * Commenting for SERFF changes List<PlanHealthServiceRegion>
			 * regions = existingPlanHealth.getPlanHealthServiceRegion();
			 */
			PlanHealthBenefit benefit = null;
			PlanHealthMapper planHealthMapper = new PlanHealthMapper();
			PlanHealth planHealth = planHealthMapper.mapData(data,
					ifpBenefitsReader.getColumns(), gethealthbenefitMap());
			
			if (planHealth != null) {
				planHealth.setActurialValueCertificate(plan.getPlanHealth().getActurialValueCertificate());
				planHealth.setPlanLevel(plan.getPlanHealth().getPlanLevel());
				planHealth.setEhbCovered(plan.getPlanHealth().getEhbCovered());
				planHealth.setCreationTimestamp(plan.getPlanHealth().getCreationTimestamp());
				planHealth.setCreated_by(plan.getPlanHealth().getCreated_by());
				planHealth.setBenefitFile(FilenameUtils.getName(benefitsFile));
				planHealth.setBenefitEffectiveDate(plan.getPlanHealth().getBenefitEffectiveDate());
				planHealth.setRateEffectiveDate(plan.getPlanHealth().getRateEffectiveDate());
				planHealth.setLastUpdatedBy(plan.getLastUpdatedBy());
				plan.setPlanHealth(planHealth);
				planHealth.setPlan(plan);

				// deleting old benefits
				for (String key : benefitsMap.keySet()) {
					benefit = benefitsMap.get(key);
					benefitIds.add(benefit.getId());
				}

				for (Integer benefitId : benefitIds) {
					iPlanHealthBenfitRepository.delete(benefitId);
				}
				// deleting old health plan
				iPlanHealthRepository.delete(existingPlanHealth.getId());

				savePlan(plan);
			}
		} else if (plan.getInsuranceType().equals(PlanMgmtService.DENTAL)) {
			PlanDental existingPlanDental = plan.getPlanDental();
			Map<String, PlanDentalBenefit> benefitsMap = existingPlanDental.getPlanDentalBenefits();
			/*
			 * Commenting for SERFF changes List<PlanDentalServiceRegion>
			 * regions = existingPlanDental.getPlanDentalServiceRegion();
			 */
			PlanDentalBenefit benefit = null;
			PlanDentalMapper planDentalMapper = new PlanDentalMapper();
			PlanDental planDental = planDentalMapper.mapData(data, ifpBenefitsReader.getColumns(), getDentalbenefitMap());
			if (planDental != null) {
				planDental.setActurialValueCertificate(plan.getPlanDental().getActurialValueCertificate());
				planDental.setPlanLevel(plan.getPlanDental().getPlanLevel());
				planDental.setCreationTimestamp(plan.getPlanDental().getCreationTimestamp());
				planDental.setCreatedBy(plan.getPlanDental().getCreatedBy());
				planDental.setBenefitFile(FilenameUtils.getName(benefitsFile));
				planDental.setBenefitEffectiveDate(plan.getPlanDental().getBenefitEffectiveDate());
				planDental.setRateEffectiveDate(plan.getPlanDental().getRateEffectiveDate());
				plan.setPlanDental(planDental);
				planDental.setPlan(plan);

				// deleting old benefits
				for (String key : benefitsMap.keySet()) {
					benefit = benefitsMap.get(key);
					benefitIds.add(benefit.getId());
				}

				for (Integer benefitId : benefitIds) {
					iPlanDentalBenfitRepository.delete(benefitId);
				}
				// deleting old Dental plan
				iPlanDentalRepository.delete(existingPlanDental.getId());

				savePlan(plan);
			}
		}

		return plan;
	}

	@Override
	@Transactional
	public void updatePlanRates(Plan plan, String ratesFile) {
		
		List<Integer> childIds = iPlanHealthRepository.getChildPlanIds(Integer.toString(plan.getId()));
		childIds.add(plan.getId());
		for (Integer planId : childIds) {
			/*
			 * Commenting for SERFF changes updateRegions(planId,
			 * updatedRegions, ratesFile);
			 */
		}
	}

	@Override
	@Transactional
	public Map<String, String> getReviewScreenDetails(String parentPlanId) throws ContentManagementServiceException {
		List<Object[]> data = iPlanHealthRepository.getPlanHealthDetails(parentPlanId);
		Map<String, String> reviewData = new HashMap<String, String>();
		if (data == null) {
			return null;
		}
		String costSharing = null;
		String actCerti = null;
		String benefitFile = null;

		reviewData.put("hasActCerti1", FALSE_STRING);
		reviewData.put("hasBenefitFile1", FALSE_STRING);

		reviewData.put("hasActCerti2", FALSE_STRING);
		reviewData.put("hasBenefitFile2", FALSE_STRING);

		reviewData.put("hasActCerti3", FALSE_STRING);
		reviewData.put("hasBenefitFile3", FALSE_STRING);
		for (Object[] singleData : data) {
			actCerti = (String) singleData[0];
			benefitFile = (String) singleData[1];
			actCerti = getActualFileName(actCerti);
			benefitFile = getActualFileName(benefitFile);
			costSharing = (String) singleData[2];
			if ("CS4".equals(costSharing)) {
				if (actCerti != null && actCerti.length() > 0) {
					reviewData.put("actCerti1", actCerti);
					reviewData.put("hasActCerti1", TRUE_STRING);
				}

				if (benefitFile != null && benefitFile.length() > 0) {
					reviewData.put("benefitFile1", benefitFile);
					reviewData.put("hasBenefitFile1", TRUE_STRING);
				}
				reviewData.put("section1", "1");
			} else if ("CS5".equals(costSharing)) {

				if (actCerti != null && actCerti.length() > 0) {
					reviewData.put("actCerti2", actCerti);
					reviewData.put("hasActCerti2", TRUE_STRING);
				}

				if (benefitFile != null && benefitFile.length() > 0) {
					reviewData.put("benefitFile2", benefitFile);
					reviewData.put("hasBenefitFile2", TRUE_STRING);
				}
				reviewData.put("section2", "2");
			} else if ("CS6".equals(costSharing)) {

				if (actCerti != null && actCerti.length() > 0) {
					reviewData.put("actCerti3", actCerti);
					reviewData.put("hasActCerti3", TRUE_STRING);
				}

				if (benefitFile != null && benefitFile.length() > 0) {
					reviewData.put("benefitFile3", benefitFile);
					reviewData.put("hasBenefitFile3", TRUE_STRING);
				}
				reviewData.put("section3", "3");
			}
			
		}
		return reviewData;
	}

	@Override
	@Transactional
	public String getActualFileName(String fileName) throws ContentManagementServiceException {
		if (StringUtils.isNotBlank(fileName)) {
				fileName = ecmService.getContentById(fileName).getOriginalFileName();
				int lastIndex = fileName.lastIndexOf(DOT);
				if (lastIndex == -1) {
					lastIndex = fileName.length();
				}
				String ext = fileName.substring(lastIndex + 1);
				String fileNameWithUnderscore = fileName.substring(0, lastIndex);
				int lastUnderscore = fileNameWithUnderscore.lastIndexOf(UNDERSCORE);
				return fileNameWithUnderscore.substring(0, lastUnderscore)
						+ DOT + ext;
		}
		return null;
	}

	@Override
	@Transactional
	public void deletePlan(String planId, String costSharing) {
		List<Integer> childIds = null;
		if ("0".equals(costSharing)) {
			childIds = iPlanHealthRepository.getChildPlanIds(planId);
			LOGGER.info("Deleting child plans");
			childIds.add(Integer.valueOf(planId));
		} else if ("6".equals(costSharing)) {
			if(LOGGER.isDebugEnabled()){
			   LOGGER.debug("Deleting provider for plan " + SecurityUtil.sanitizeForLogging(String.valueOf(planId)));
			}
			Plan plan = iPlanRepository.findOne(Integer.parseInt(planId));
			plan.setNetwork(null);
			iPlanRepository.save(plan);
		} else {
			childIds = iPlanHealthRepository.getCostSharingPlanIds(planId, "CS"
					+ costSharing);
			// iPlanRepository.delete((Integer)childPlanId);
		}
		if (childIds != null) {
			for (Integer pId : childIds) {
				iPlanRepository.delete(pId);
			}
		}
	}

	@Override
	@Transactional(readOnly = true)
	public List<Plan> getAddedOrUpdatedPlans() {
		try {
			String startdatetime;
			String enddatetime;
			Calendar cal = TSCalendar.getInstance();
			cal.add(Calendar.DATE, -1);

			SimpleDateFormat startformatter = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
			SimpleDateFormat endformatter = new SimpleDateFormat("yyyy-MM-dd 23:59:59");

			startdatetime = startformatter.format(cal.getTime());
			enddatetime = endformatter.format(cal.getTime());

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date convertedStartDate = dateFormat.parse(startdatetime);
			Date convertedEndDate = dateFormat.parse(enddatetime);

			return iPlanRepository.getAddedOrUpdatedPlans(convertedStartDate, convertedEndDate);

		} catch (ParseException e) {
			LOGGER.error(EXCEPTION_STRING_1 + this.getClass()
					+ EXCEPTION_STRING_2 + e);
			return null;
		}

	}

	@Transactional
	private void copyPlanData(Plan copyTo, Plan copyFrom) {
		copyTo.setName(copyFrom.getName());
		copyTo.setIssuerPlanNumber(copyFrom.getIssuerPlanNumber());
		copyTo.setMarket(copyFrom.getMarket());
		/*
		 * Commenting for SERFF changes
		 * copyTo.setMaxEnrolee(copyFrom.getMaxEnrolee());
		 */
		copyTo.setStartDate(copyFrom.getStartDate());
		copyTo.setEndDate(copyFrom.getEndDate());
		copyTo.setHsa(copyFrom.getHsa());
		if (PlanInsuranceType.HEALTH.toString().equals(copyTo.getInsuranceType())) {
			copyPlanHealthData(copyTo, copyFrom);
		} else if (PlanInsuranceType.DENTAL.toString().equals(copyTo.getInsuranceType())) {
			copyPlanDentalData(copyTo, copyFrom);
		}
		Issuer issuer = iIssuerRepository.findOne(copyFrom.getIssuer().getId());
		copyTo.setIssuer(issuer);
	}

	private void copyPlanHealthData(Plan copyTo, Plan copyFrom) {
		copyTo.getPlanHealth().setActurialValueCertificate(copyFrom.getPlanHealth().getActurialValueCertificate());
		copyTo.getPlanHealth().setPlanLevel(copyFrom.getPlanHealth().getPlanLevel());
		copyTo.getPlanHealth().setEhbCovered(copyFrom.getPlanHealth().getEhbCovered());
	}

	private void copyPlanDentalData(Plan copyTo, Plan copyFrom) {
		copyTo.getPlanDental().setActurialValueCertificate(copyFrom.getPlanDental().getActurialValueCertificate());
		copyTo.getPlanDental().setPlanLevel(copyFrom.getPlanDental().getPlanLevel());
	}


	@Override
	@Transactional
	public void updateStatus(String planId, String loggedInUser, String status, String loggedInUserId, boolean updateEndDate, String certSuppDocFile, String comments, String issuerVerificationStatus) throws InvalidUserException {
		Integer userId = null;
		String userName = null;
			userId = userService.getLoggedInUser().getId();
			userName = userService.getLoggedInUser().getUserName();
		Integer cmId = 0;
		if (!StringUtils.isEmpty(comments)) {
			cmId = saveComment(Integer.parseInt(planId), comments, TargetName.PLAN, userId, userName);
		}

		Plan existingPlan = iPlanRepository.findOne(Integer.parseInt(planId));
		existingPlan.setSupportFile(certSuppDocFile);
		existingPlan.setStatus(status);
		existingPlan.setLastUpdatedBy(Integer.parseInt(loggedInUserId));
		if (issuerVerificationStatus != null) {
			existingPlan.setIssuerVerificationStatus(issuerVerificationStatus);
		}

		if (cmId != null && cmId != 0) {
			existingPlan.setCommentId(cmId);
		} else {
			existingPlan.setCommentId(null);
		}
		// when admin certify/recertify any plan 		
		if (STATUS_CERTIFIED.equalsIgnoreCase(status)) {
			existingPlan.setCertifiedby(loggedInUserId);
			existingPlan.setCertifiedOn(new TSDate());
			// on certification/de-certification of plan set Issuer Verification Status to Pending except PHIX profile
			if (!DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE).equalsIgnoreCase(GhixConstants.PHIX)) {				
				 existingPlan.setIssuerVerificationStatus(PlanMgmtService.ISSUER_VERIFICATION_STATUS_PENDING);
			}
		} 
		iPlanRepository.save(existingPlan);
	}

	@Override
	@Transactional
	public void updateProvider(String planId, String networkId) throws NumberFormatException, InvalidUserException {
		Plan existingPlan = iPlanRepository.findOne(Integer.parseInt(planId));
		Network network = null;
		List<Integer> childPlanIds = null;
		Plan childPlan = null;
		Integer userId = null;
			userId = userService.getLoggedInUser().getId();

			network = providerNetworkService.getNetwork(Integer.parseInt(networkId));

		if (network.getType().toString().equalsIgnoreCase("ppo")) {
			existingPlan.setNetworkType(NetworkType.PPO.toString());
		} else {
			existingPlan.setNetworkType(NetworkType.HMO.toString());
		}

		existingPlan.setNetwork(network);
		existingPlan.setLastUpdatedBy(userId);

		if (existingPlan.getInsuranceType().equals(PlanMgmtService.HEALTH)) {
			childPlanIds = iPlanHealthRepository.getChildPlanIds(planId);
		} else if (existingPlan.getInsuranceType().equals(PlanMgmtService.DENTAL)) {
			try {
				childPlanIds = iPlanDentalRepository.getChildPlanIds(Integer.parseInt(planId));
			} catch (Exception ex) {
				LOGGER.error(EXCEPTION_STRING_1 + this.getClass()
						+ EXCEPTION_STRING_2 + ex);
			}
		}

		if (childPlanIds != null) {
			for (Integer childPlanId : childPlanIds) {
				childPlan = iPlanRepository.findOne(childPlanId);
				childPlan.setNetwork(network);

				if (network.getType().toString().equalsIgnoreCase("ppo")) {
					childPlan.setNetworkType(NetworkType.PPO.toString());
				} else {
					childPlan.setNetworkType(NetworkType.HMO.toString());
				}
				childPlan.setLastUpdatedBy(userId);
				iPlanRepository.save(childPlan);
			}
		}
		iPlanRepository.save(existingPlan);
	}

	@Override
	@Transactional
	public Long getMatchingPlanCount(int issuerId, String name) {
		return iPlanRepository.getMatchingPlanCount(issuerId, name);
	}

	@Override
	@Transactional
	public void decertifyPlans(int issuerId) {
		iPlanRepository.decertifyPlans(issuerId);
	}

	@Override
	public String getSectionId(String costSharing) {
		String section = null;
		if ("CS4".equals(costSharing)) {
			section = "1";
		} else if ("CS5".equals(costSharing)) {
			section = "2";
		} else if ("CS6".equals(costSharing)) {
			section = "3";
		} else if ("CS4".equals(costSharing)) {
			section = "4";
		} else if ("CS5".equals(costSharing)) {
			section = "5";
		}
		return section;
	}

	@GiFeature("ahbx.planmgmt.ind04.enabled")
	@Override
	@Transactional
	public void syncPlanWithAHBX(int planId) {
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("==== Inside syncPlanWithAHBX() ====");
			}	
				
			List<Map<String, String>> listOfPlans = new ArrayList<Map<String, String>>();
			String insuranceType = PlanMgmtConstants.EMPTY_STRING;
			String planlevel = PlanMgmtConstants.EMPTY_STRING;
			Map<String, String> requestData = null;

			Plan plandata = iPlanRepository.findOne(planId);
			//Document requestDocument = DocumentHelper.createDocument();
			//Element requestElement = requestDocument.addElement("updatePlanInfo");
			insuranceType = plandata.getInsuranceType();
			if(insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
				planlevel = plandata.getPlanHealth().getPlanLevel();
			}else if(insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
				planlevel = plandata.getPlanDental().getPlanLevel();
			}
			
			requestData = new HashMap<String, String>();
			requestData.put(PlanMgmtConstants.AHBX_PLAN_ID, String.valueOf(plandata.getId()));
			requestData.put(PlanMgmtConstants.AHBX_ISSUER_ID, plandata.getIssuer().getHiosIssuerId());
			requestData.put(PlanMgmtConstants.AHBX_PLAN_LEVEL, planlevel);
			requestData.put(PlanMgmtConstants.AHBX_PLAN_MARKET, plandata.getMarket());
			requestData.put(PlanMgmtConstants.AHBX_PLAN_NAME, plandata.getName());
			requestData.put(PlanMgmtConstants.AHBX_PLAN_NETWORK_TYPE, plandata.getNetworkType());
			requestData.put(PlanMgmtConstants.AHBX_PLAN_RECORD_INDICATOR, (plandata.getLastUpdateTimestamp().equals(plandata.getCreationTimestamp())) ? "N" : "U");
			requestData.put(PlanMgmtConstants.AHBX_PLAN_TYPE, insuranceType);
			requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_START_DATE, PlanMgmtConstants.AHBX_DATE_FORMAT.format(plandata.getStartDate()));
			requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_END_DATE, PlanMgmtConstants.AHBX_DATE_FORMAT.format(plandata.getEndDate()));
			requestData.put(PlanMgmtConstants.AHBX_PLAN_HIOS_ID, plandata.getIssuerPlanNumber());
			requestData.put(PlanMgmtConstants.AHBX_PLAN_STATUS, plandata.getStatus());
			requestData.put(PlanMgmtConstants.AHBX_PLAN_YEAR, Integer.toString(plandata.getApplicableYear()));
			listOfPlans.add(requestData);
			
			callPlanSyncRestClient(listOfPlans); // call ahbx plan sync API
	}

	/* call AHBX API to sync list of plans when we do bulk plan certification */
	@GiFeature("ahbx.planmgmt.ind04.enabled")
	@Override
	public void syncPlanWithAHBXInBulk(List<Map<String, String>> listOfPlans){
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("==== Inside syncPlanWithAHBXInBulk() ====");
			LOGGER.info(" # of plan to sync : " +listOfPlans.size());
		}
		
		if(listOfPlans.size() > 0){
			callPlanSyncRestClient(listOfPlans); // call ahbx plan sync API
		}
	}
	
	
	private List<Map<String, String>> processPlanDataForAHBXCall(List<Map<String, String>> planDataList){
		List<Map<String, String>> listOfPlans = new ArrayList<Map<String, String>>();
		Map<String, String> requestData = null;
		String planLevel = null;
		String planMarket = null;
		String planType = null;
		
		for(Map<String, String> planData : planDataList){					
			planLevel = planData.get(PlanMgmtConstants.AHBX_PLAN_LEVEL);
			planLevel = planLevel.substring(0, 1).toUpperCase()	+ planLevel.substring(1).toLowerCase(); // example : <planLevel>Silver</planLevel>
			planMarket = planData.get(PlanMgmtConstants.AHBX_PLAN_MARKET);
			planMarket = (planMarket.equalsIgnoreCase(Plan.PlanMarket.INDIVIDUAL.toString())) ? "Individual" : "Group";
			planType = planData.get(PlanMgmtConstants.AHBX_PLAN_TYPE);
			planType = planType.substring(0, 1).toUpperCase() + planType.substring(1).toLowerCase(); 
			
			requestData = new HashMap<String, String>();
			requestData.put(PlanMgmtConstants.AHBX_PLAN_ID, planData.get(PlanMgmtConstants.AHBX_PLAN_ID));
			requestData.put(PlanMgmtConstants.AHBX_ISSUER_ID,	planData.get(PlanMgmtConstants.AHBX_ISSUER_ID));
			requestData.put(PlanMgmtConstants.AHBX_PLAN_LEVEL, planLevel);
			requestData.put(PlanMgmtConstants.AHBX_PLAN_MARKET, planMarket.toUpperCase());
			requestData.put(PlanMgmtConstants.AHBX_PLAN_NAME, planData.get(PlanMgmtConstants.AHBX_PLAN_NAME));
			requestData.put(PlanMgmtConstants.AHBX_PLAN_NETWORK_TYPE, planData.get(PlanMgmtConstants.AHBX_PLAN_NETWORK_TYPE).toUpperCase());
			requestData.put(PlanMgmtConstants.AHBX_PLAN_RECORD_INDICATOR, planData.get(PlanMgmtConstants.AHBX_PLAN_RECORD_INDICATOR));
			requestData.put(PlanMgmtConstants.AHBX_PLAN_TYPE, (planType.equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())) ? "Medical" : planType);
			requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_START_DATE, planData.get(PlanMgmtConstants.AHBX_PLAN_EFFCT_START_DATE));
			requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_END_DATE, planData.get(PlanMgmtConstants.AHBX_PLAN_EFFCT_END_DATE));
			requestData.put(PlanMgmtConstants.AHBX_PLAN_HIOS_ID, planData.get(PlanMgmtConstants.AHBX_PLAN_HIOS_ID));
			requestData.put(PlanMgmtConstants.AHBX_PLAN_STATUS, planData.get(PlanMgmtConstants.AHBX_PLAN_STATUS));
			requestData.put(PlanMgmtConstants.AHBX_PLAN_YEAR, planData.get(PlanMgmtConstants.AHBX_PLAN_YEAR));
			

			listOfPlans.add(requestData);
		}
		
		return listOfPlans;
	}

	
	@Override
	@SuppressWarnings(UNCHECKED)
	@Transactional(timeout = TIME_OUT)
	public Plan savePlanRatesEditMode(Plan plan, String rateFile, Date effectiveStartDate, String comments) throws InvalidUserException, GIException, IOException, ContentManagementServiceException {
		// --- Parse the rates file ---
		FileReader<List<String>> ifpBenefitsReader = ReaderFactory.<List<String>> getReader(rateFile, ',', List.class);
		List<List<String>> data = ifpBenefitsReader.readData();
		if (plan.getInsuranceType().equals(PlanMgmtService.HEALTH)) {
			editHealthRates(plan, rateFile, effectiveStartDate, comments, ifpBenefitsReader, data);
		} else if (plan.getInsuranceType().equals(PlanMgmtService.DENTAL)) {
			editDentalRates(plan, rateFile, effectiveStartDate, comments, ifpBenefitsReader, data);
		}
		return plan;
	}

	/**
	 * Edits plan rates of Plans of Insurance type Dental.
	 * @throws InvalidUserException 
	 */
	private void editDentalRates(Plan plan, String rateFile, Date effectiveStartDate, String comments, FileReader<List<String>> ifpBenefitsReader, List<List<String>> data) throws InvalidUserException {
		PlanDental planDental = plan.getPlanDental();
		/*
		 * Commenting for SERFF changes List<PlanDentalServiceRegion>
		 * updatedRegions = planDentalSerRegMapper.mapData(data,
		 * ifpBenefitsReader.getColumns());
		 */
		// --- fetch old regions ---

		/*
		 * Commenting for SERFF changes List<PlanDentalServiceRegion> oldRegions
		 * = planDental.getPlanDentalServiceRegion();
		 */

		Integer userId = null;
		String userName = null;
			userId = userService.getLoggedInUser().getId();
			planDental.setLastUpdatedBy(userId);

		Integer cmId = saveComment(plan.getId(), comments, TargetName.PLAN_DENTAL, userId, userName);
		planDental.setCommentId(cmId);

		planDental.setRateFile(FilenameUtils.getName(rateFile));
		planDental.setRateEffectiveDate(effectiveStartDate);

		
	}

	/**
	 * Edits plan rates of Plans of Insurance type health.
	 * @throws InvalidUserException 
	 * @throws ContentManagementServiceException 
	 * @throws IOException 
	 * @throws GIException 
	 */
	private void editHealthRates(Plan plan, String rateFile, Date effectiveStartDate, String comments, FileReader<List<String>> ifpBenefitsReader, List<List<String>> data) throws InvalidUserException, GIException, IOException, ContentManagementServiceException {
		PlanHealth planHealth = plan.getPlanHealth();
		/*
		 * Commenting for SERFF changes List<PlanHealthServiceRegion>
		 * updatedRegions = planHealthSerRegMapper.mapData(data,
		 * ifpBenefitsReader.getColumns());
		 */
		// --- fetch old regions ---
		

		Integer userId = null;
		String userName = null;
			userId = userService.getLoggedInUser().getId();
			planHealth.setLastUpdatedBy(userId);

		Integer cmId = saveComment(plan.getId(), comments, TargetName.PLAN_HEALTH, userId, userName);
		planHealth.setCommentId(cmId);

		planHealth.setRateFile(FilenameUtils.getName(rateFile));
		planHealth.setRateEffectiveDate(effectiveStartDate);

			saveHealthPlan(planHealth);
	}

	/**
	 * Form a region key for specified region as "healthId_regionId".
	 */
	private String getRegionKey(int planChildId, int regionId) {
		return planChildId + "_" + regionId;

	}

	

	@Override
	@Transactional(readOnly = true)
	public List<Map<String, Object>> loadPlanRatesHistory(Integer planId) {
		String insuranceType = iPlanRepository.getInsuranceType(planId);
		String repositoryName = null;
		String modelName = null;
		Integer planAssociateId = null;
		if (PlanInsuranceType.HEALTH.toString().equals(insuranceType)) {
			planAssociateId = iPlanHealthRepository.getPlanHealthId(planId);
			repositoryName = RatesHistoryRendererImpl.REPOSITORY_NAME;
			modelName = RatesHistoryRendererImpl.MODEL_NAME;
		} else if (PlanInsuranceType.DENTAL.toString().equals(insuranceType)) {
			planAssociateId = iPlanDentalRepository.getPlanDentalId(planId);
			repositoryName = RatesHistoryRendererImpl.DENTAL_REPOSITORY_NAME;
			modelName = RatesHistoryRendererImpl.DENTAL_MODEL_NAME;
		}

		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(PlanMgmtConstants.UPDATE_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.RATEFILE_COL, FILE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.USER_NAME_ALIAS, USER_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.RATE_EFFECTIVE_DATE_COL, effectiveDate);
		requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(PlanMgmtConstants.RATEFILE_COL);
		compareCols.add(PlanMgmtConstants.BENEFITFILE_COL);
		compareCols.add(PlanMgmtConstants.PLAN_SBC_COL);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		displayCols.add(PlanMgmtConstants.RATE_EFFECTIVE_DATE_INT);
		displayCols.add(PlanMgmtConstants.RATEFILE_INT);
		displayCols.add(PlanMgmtConstants.COMMENT_INT);
		List<Map<String, Object>> data = null;
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> planHealthData = service.findRevisions(repositoryName, modelName, requiredFieldsMap, planAssociateId);
			data = ratesHistoryRendererImpl.processData(planHealthData, compareCols, displayCols);
		return data;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Map<String, Object>> loadQDPRatesHistory(Integer planId) {

		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		List<Integer> displayCols = new ArrayList<Integer>();
		List<String> compareCols = new ArrayList<String>();

		Integer planAssociateId = iPlanDentalRepository.getPlanDentalId(planId);
		String repositoryName = RatesHistoryRendererImpl.DENTAL_REPOSITORY_NAME;
		String modelName = RatesHistoryRendererImpl.DENTAL_MODEL_NAME;

		requiredFieldsMap.put(PlanMgmtConstants.UPDATE_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.RATEFILE_COL, FILE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.USER_NAME_ALIAS, USER_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.RATE_EFFECTIVE_DATE_COL, effectiveDate);
		requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");

		compareCols.add(PlanMgmtConstants.RATEFILE_COL);
		compareCols.add(PlanMgmtConstants.RATE_EFFECTIVE_DATE_COL);

		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		displayCols.add(PlanMgmtConstants.RATE_EFFECTIVE_DATE_INT);
		displayCols.add(PlanMgmtConstants.RATEFILE_INT);
		displayCols.add(PlanMgmtConstants.COMMENT_INT);
		List<Map<String, Object>> data = null;
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> planHealthData = service.findRevisions(repositoryName, modelName, requiredFieldsMap, planAssociateId);
			data = ratesHistoryRendererImpl.processData(planHealthData, compareCols, displayCols);
		return data;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Map<String, Object>> loadPlanEnrollmentHistory(Integer planId, String role) {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, FILE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_COL,"");
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_BY_COL, USER_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, effectiveDate);
		requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");
		requiredFieldsMap.put(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL,"");
		
		List<String> compareCols = new ArrayList<String>();
		compareCols.add(PlanMgmtConstants.ENROLLMENT_AVAIL_COL);
		compareCols.add(planId.toString());
		if(role.equalsIgnoreCase(PlanMgmtConstants.ADMIN_ROLE)){
			compareCols.add(PlanMgmtConstants.ADMIN_ROLE);
		}else{
			compareCols.add(PlanMgmtConstants.ISSUER_ROLE);
		}
		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		displayCols.add(PlanMgmtConstants.EFFECTIVE_DATE_INT);
		displayCols.add(PlanMgmtConstants.ENROLLMENT_AVAIL_INT);
		displayCols.add(PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_INT);
		displayCols.add(PlanMgmtConstants.COMMENT_INT);
		displayCols.add(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL_VAL_INT);

		List<Map<String, Object>> data = null;
		DisplayAuditService service = objectFactory.getObject();
		service.setApplicationContext(appContext);
		List<Map<String, String>> planData = service.findRevisions(PlanMgmtConstants.REPOSITORY_NAME, PlanMgmtConstants.MODEL_NAME, requiredFieldsMap, planId);
		List<Map<String, String>> combinedPlanData = new ArrayList<Map<String, String>>(); 
		for (Map<String, String> map : planData) {
			combinedPlanData.add(map);
		}
		//HIX-103582: Add latest enrollAvailStatus, processData will remove duplicates if any
		Plan plan = iPlanRepository.findOne(planId);
		if(null != plan) {
			Map<String, String> planCertStatus = new HashMap<String, String>();
			String futureDate = null, effectiveDate = null;
			if(null != plan.getEnrollmentAvailEffDate()) { 
				effectiveDate = plan.getEnrollmentAvailEffDate().toString();
			} else {
				effectiveDate = "";
			}
			if(null != plan.getFutureErlAvailEffDate()) { 
				futureDate = plan.getFutureErlAvailEffDate().toString();
			} else {
				futureDate = "";
			}
			planCertStatus.put(PlanMgmtConstants.UPDATED_DATE_COL, plan.getLastUpdateTimestamp().toString());
			planCertStatus.put(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, plan.getEnrollmentAvail());
			planCertStatus.put(PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_COL,plan.getFutureEnrollmentAvail());
			planCertStatus.put(PlanMgmtConstants.UPDATED_BY_COL, "" + plan.getLastUpdatedBy());
			planCertStatus.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, effectiveDate);
			planCertStatus.put(PlanMgmtConstants.COMMENT_COL, "");
			planCertStatus.put(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL, futureDate);
			combinedPlanData.add(planCertStatus);
		}
		data = planHistoryRendererImpl.processData(combinedPlanData, compareCols, displayCols);
		return data;
	}

	// fetch QHP plan status history
	@Override
	@Transactional(readOnly = true)
	public List<Map<String, Object>> loadPlanStatusHistory(Integer planIdForStatusHistory) {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, "Enroll");
		requiredFieldsMap.put(PlanMgmtConstants.STATUS_COL, "STATUS");
		requiredFieldsMap.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, effectiveDate);
		requiredFieldsMap.put(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL, "");
		requiredFieldsMap.put(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL,"");
		requiredFieldsMap.put(PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_COL, "");

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(PlanMgmtConstants.STATUS_COL);
		compareCols.add(PlanMgmtConstants.ENROLLMENT_AVAIL_COL);
		compareCols.add(PlanMgmtConstants.EFFECTIVE_DATE_COL);
		compareCols.add(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL);
		compareCols.add(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS);
		//compareCols.add(FUTURE_ENROLLMENT_AVAIL_COL);
		//compareCols.add(FUTURE_ERL_AVAIL_DATE_COL);


		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.STATUS_INT);
		displayCols.add(PlanMgmtConstants.ENROLLMENT_AVAIL_INT);
		displayCols.add(PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_INT);
		displayCols.add(PlanMgmtConstants.EFFECTIVE_DATE_INT);
		//displayCols.add(FN_EFFECTIVE_START_DATE_VAL);
		displayCols.add(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_VAL_INT);
		displayCols.add(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL_VAL_INT);
		List<Map<String, Object>> data = null;
		DisplayAuditService service = objectFactory.getObject();
		service.setApplicationContext(appContext);
		List<Map<String, String>> planData = service.findRevisions(PlanMgmtConstants.REPOSITORY_NAME, PlanMgmtConstants.MODEL_NAME, requiredFieldsMap, planIdForStatusHistory);
		data = planHistoryRendererImpl.processData(planData, compareCols, displayCols);
		return data;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Map<String, Object>> loadPlanCertificationHistory(Integer planId, String role) {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.STATUS_FILE_COL, FILE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_BY_COL, USER_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.STATUS_COL, "STATUS");
		requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(PlanMgmtConstants.STATUS_COL);
		compareCols.add(planId.toString());
		if(role.equalsIgnoreCase(PlanMgmtConstants.ADMIN_ROLE)){
			compareCols.add(PlanMgmtConstants.ADMIN_ROLE);
		}else{
			compareCols.add(PlanMgmtConstants.ISSUER_ROLE);
		}
		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		displayCols.add(PlanMgmtConstants.STATUS_INT);
		displayCols.add(PlanMgmtConstants.STATUS_FILE_INT);
		displayCols.add(PlanMgmtConstants.COMMENT_INT);
		List<Map<String, Object>> data = null;
		DisplayAuditService service = objectFactory.getObject();
		service.setApplicationContext(appContext);
		List<Map<String, String>> planData = service.findRevisions(PlanMgmtConstants.REPOSITORY_NAME, PlanMgmtConstants.MODEL_NAME, requiredFieldsMap, planId);
		List<Map<String, String>> combinedPlanData = new ArrayList<Map<String, String>>(); 
		for (Map<String, String> map : planData) {
			combinedPlanData.add(map);
		}
		//HIX-103582: Add latest status, processData will remove duplicates if any
		Plan plan = iPlanRepository.findOne(planId);
		if(null != plan) {
			Map<String, String> planCertStatus = new HashMap<String, String>();
			planCertStatus.put(PlanMgmtConstants.UPDATED_DATE_COL, com.getinsured.hix.planmgmt.util.DateUtil.dateToString(plan.getLastUpdateTimestamp(), PlanMgmtConstants.TIMESTAMP_PATTERN1));
			planCertStatus.put(PlanMgmtConstants.STATUS_FILE_COL, "");
			planCertStatus.put(PlanMgmtConstants.UPDATED_BY_COL, "" + plan.getLastUpdatedBy());
			planCertStatus.put(PlanMgmtConstants.STATUS_COL, plan.getStatus());
			planCertStatus.put(PlanMgmtConstants.COMMENT_COL, "");
			combinedPlanData.add(planCertStatus);
		}
		data = planHistoryRendererImpl.processData(combinedPlanData, compareCols, displayCols);
		return data;
	}

	private List<Map<String, Object>> mergeLists(List<Map<String, Object>> firstList, List<Map<String, Object>> secondList) {
		List<Map<String, Object>> mergedList = new ArrayList<Map<String, Object>>();
		int i = 0;
		int j = 0;
		Map<String, Object> firstMap = null;
		Map<String, Object> secondMap = null;
		Object firstVal = null;
		Object secondVal = null;
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern(PlanMgmtConstants.TIMESTAMP_PATTERN1);
		if (firstList != null && secondList != null) {
			while (i < firstList.size() && j < secondList.size()) {
				firstMap = firstList.get(i);
				secondMap = secondList.get(j);

				firstVal = firstMap.get(PlanMgmtConstants.UPDATED_DATE_COL);
				secondVal = secondMap.get(PlanMgmtConstants.UPDATED_DATE_COL);
				
				if(firstVal != null && secondVal != null){
					Date firstDate = new TSDate();
					Date secondDate = new TSDate();
					boolean dateparsable = true;
					try {
						firstDate = sdf.parse(firstVal.toString());
						secondDate = sdf.parse(secondVal.toString());
					} catch (ParseException e) {
						dateparsable =false;
					}
					if(!dateparsable) {
						try {
							sdf.applyPattern("E MMM dd HH:mm:ss Z yyyy");
							firstDate = sdf.parse(firstVal.toString());
							secondDate = sdf.parse(secondVal.toString());
						}catch (ParseException e) {
							dateparsable =false;
						}
					}
					
					if ((firstDate).after(secondDate)) {					
						mergedList.add(firstMap);
						i++;
					} else {					
						mergedList.add(secondMap);
						j++;
					}
				}
			}
		}

		if (firstList != null) {
			while (i < firstList.size()) {
				mergedList.add(firstList.get(i));
				i++;
			}
		}

		if (secondList != null) {
			while (j < secondList.size()) {
				mergedList.add(secondList.get(j));
				j++;
			}
		}
		return mergedList;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Map<String, Object>> loadPlanHistory(Integer planId) {
		List<Map<String, Object>> planData = getPlanHistory(planId);
		//List<Map<String, Object>> planHealthData = getPlanAssociateHistory(planId);
		//List<Map<String, Object>> planRateData = loadPlanRateByEffectiveDate(planId);
		//List<Map<String, Object>> planMargedData = mergeLists(planHealthData, planData);
		//List<Map<String, Object>> rPlanMargedData = mergeLists(planMargedData, planRateData);
		/*Collections.sort(rPlanMargedData, new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH);
				Date dt1 = null;
				Date dt2 = null;
				try {
					dt1 = df.parse(o1.get("lastUpdateTimestamp").toString());
					dt2 = df.parse(o2.get("lastUpdateTimestamp").toString());
				} catch (ParseException e) {
					LOGGER.error("error",e);
				}
				return dt2.compareTo(dt1);
			}
		});*/
		return planData;
	}

	private List<Map<String, Object>> getPlanHistory(Integer planId) {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, "Enroll");
		requiredFieldsMap.put(PlanMgmtConstants.STATUS_COL, "STATUS");
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_BY_COL, USER_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, effectiveDate);
		requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");
		requiredFieldsMap.put(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL, "");
		requiredFieldsMap.put(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL, "");
		requiredFieldsMap.put(PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_COL,"");
		
		List<String> compareCols = new ArrayList<String>();
		compareCols.add(PlanMgmtConstants.ENROLLMENT_AVAIL_COL);
		compareCols.add(PlanMgmtConstants.STATUS_COL);
		compareCols.add(PlanMgmtConstants.EFFECTIVE_DATE_COL);
		compareCols.add(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL);
		compareCols.add(PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_COL);
		compareCols.add(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL);
		compareCols.add(planId.toString());		
		
		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		displayCols.add(PlanMgmtConstants.FN_ENROLLMENT_AVAIL_VAL_INT);
		displayCols.add(PlanMgmtConstants.FN_STATUS_VAL_INT);
		displayCols.add(PlanMgmtConstants.FN_DISPLAYVAL_INT);
		displayCols.add(PlanMgmtConstants.COMMENT_INT);
		displayCols.add(PlanMgmtConstants.FN_EFFECTIVE_START_DATE_VAL_INT);
		displayCols.add(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_VAL_INT);
		displayCols.add(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL_VAL_INT);
		displayCols.add(PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_INT);
		List<Map<String, Object>> data = null;
		DisplayAuditService service = objectFactory.getObject();
		service.setApplicationContext(appContext);
		List<Map<String, String>> planHealthData = service.findRevisions(PlanMgmtConstants.REPOSITORY_NAME, PlanMgmtConstants.MODEL_NAME, requiredFieldsMap, planId);
		data = planHistoryRendererImpl.processData(planHealthData, compareCols, displayCols);
		return data;
	}


	@Override
	@Transactional(readOnly = true)
	public List<Map<String, Object>> loadProviderHistory(Integer planId) {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_BY_COL, USER_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.PROVIDER_NETWORK_ID_COL, "Prov Id");

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(PlanMgmtConstants.PROVIDER_NETWORK_ID_COL);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.PROVIDER_NETWORK_NAME_INT);
		displayCols.add(PlanMgmtConstants.PROVIDER_NETWORK_TYPE_INT);
		displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		// displayCols.add(COMMENT);

		List<Map<String, Object>> data = null;
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> planData = service.findRevisions(PlanMgmtConstants.REPOSITORY_NAME, PlanMgmtConstants.MODEL_NAME, requiredFieldsMap, planId);
			data = planHistoryRendererImpl.processData(planData, compareCols, displayCols);
		return data;
	}

	@Override
	public List<Map<String, Object>> loadPlanBenefitsHistory(Integer planId, String role) {
		if (planId != null) {
			List<Map<String, Object>> planBrochureData = getPlanBrochureHistory(planId);
			List<Map<String, Object>> planEOCData = getPlanEOCHistory(planId, PlanMgmtConstants.ADMIN_ROLE);
			Plan givenPlan = this.getPlan(planId);
			List<Map<String, Object>> planData = new ArrayList<Map<String, Object>>();
			String planInsuranceType = givenPlan.getInsuranceType();
			if (planInsuranceType.equalsIgnoreCase(PlanInsuranceType.HEALTH.toString())) {
				Integer planHealthId = iPlanHealthRepository.getPlanHealthId(planId);
				planData = getHealthPlanBenefitAndSbcBrochure(planHealthId, planId, role);
			} else if (planInsuranceType.equalsIgnoreCase(PlanInsuranceType.DENTAL.toString())) {
				Integer planDentalId = iPlanDentalRepository.getPlanDentalId(planId);
				planData = getDentalPlanBenefitAndSbcBrochure(planDentalId, planId, role);
			}
			if(!(planEOCData.isEmpty())){
				planBrochureData.addAll(planEOCData);
			}
			return mergeLists(planData, planBrochureData);
		}
		return null;
	}
	
	private List<Map<String, Object>> getPlanEOCHistory(Integer planId, String role) {
		 Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		 requiredFieldsMap.put(PlanMgmtConstants.UPDATED_DATE_COL, DATE_STRING);
		 requiredFieldsMap.put(PlanMgmtConstants.EOC_DOC_UCM_ID_CAL, PlanMgmtConstants.PLAN_EOC_DOC_ID);
		 requiredFieldsMap.put(PlanMgmtConstants.EOC_DOC_NAME_COL, PlanMgmtConstants.PLAN_EOC_DOC_NAME);
		 requiredFieldsMap.put(PlanMgmtConstants.UPDATED_BY_COL, USER_STRING);
		 
		 List<String> compareCols = new ArrayList<String>();
		 compareCols.add(PlanMgmtConstants.EOC_DOC_UCM_ID_CAL);
		 compareCols.add(planId.toString());
		 if(role.equalsIgnoreCase(PlanMgmtConstants.ADMIN_ROLE)){
			 compareCols.add(PlanMgmtConstants.ADMIN_ROLE);
		 }else{
			 compareCols.add(PlanMgmtConstants.ISSUER_ROLE);
		 }
		 List<Integer> displayCols = new ArrayList<Integer>();
		 displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		 displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		 displayCols.add(PlanMgmtConstants.FN_EOC_DOC_VAL_INT);
		 displayCols.add(PlanMgmtConstants.FN_DISPLAYVAL_INT);
		 displayCols.add(PlanMgmtConstants.COMMENT_INT);
		 
		 List<Map<String, Object>> data = null;
		 List<Map<String, Object>> removeDataList = new ArrayList<Map<String, Object>>();
		 DisplayAuditService service = objectFactory.getObject();
		 service.setApplicationContext(appContext);
		 List<Map<String, String>> planHealthData = service.findRevisions(PlanMgmtConstants.REPOSITORY_NAME, PlanMgmtConstants.MODEL_NAME, requiredFieldsMap, planId);
		 data = planHistoryRendererImpl.processData(planHealthData, compareCols, displayCols);
		 if(!(data.isEmpty())){
			 for(Map<String, Object> checkReturnMap : data){
				 if(checkReturnMap.get(PlanMgmtConstants.DISPLAY_VAL).toString().isEmpty() && checkReturnMap.get(PlanMgmtConstants.DISPLAY_FIELD).equals(PlanMgmtConstants.PLAN_EOC_DOCUMENT)){
					 removeDataList.add(checkReturnMap);
				 }
			 }
			 data.removeAll(removeDataList);
		 }
		 return data;
	}

	private List<Map<String, Object>> getDentalPlanBenefitsHistory(Integer dentalId, Integer planId, String role) {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(PlanMgmtConstants.UPDATE_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.BENEFIT_EFFECTIVE_DATE_COL, "Benefit date");
		requiredFieldsMap.put(PlanMgmtConstants.USER_NAME_ALIAS, USER_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.BENEFITFILE_COL, BENEFIT_FILE);
		requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(PlanMgmtConstants.BENEFITFILE_COL);
		compareCols.add(planId.toString());
		if(role.equalsIgnoreCase(PlanMgmtConstants.ADMIN_ROLE)){
			compareCols.add(PlanMgmtConstants.ADMIN_ROLE);
		}else{
			compareCols.add(PlanMgmtConstants.ISSUER_ROLE);
		}

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		displayCols.add(PlanMgmtConstants.FN_BENEFITFILE_VAL_INT);
		displayCols.add(PlanMgmtConstants.BENEFIT_EFFECTIVE_DATE_INT);
		displayCols.add(PlanMgmtConstants.FN_DISPLAYVAL_RATEHISTORY_INT);
		displayCols.add(PlanMgmtConstants.COMMENT_INT);
		List<Map<String, Object>> data = null;
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> planDentalData = service.findRevisions(RatesHistoryRendererImpl.DENTAL_REPOSITORY_NAME, RatesHistoryRendererImpl.DENTAL_MODEL_NAME, requiredFieldsMap, dentalId);
			data = ratesHistoryRendererImpl.processData(planDentalData, compareCols, displayCols);
		return data;
	}

	private List<Map<String, Object>> getPlanBrochureHistory(Integer planId) {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.BROCHURE_UCM_ID_CAL, "Brochure");
		requiredFieldsMap.put(PlanMgmtConstants.BROCHURE_COL, "Brochure");
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_BY_COL, USER_STRING);

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(PlanMgmtConstants.BROCHURE_COL);
		compareCols.add(planId.toString());
		
		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		displayCols.add(PlanMgmtConstants.FN_BROCHURE_VAL_INT);
		displayCols.add(PlanMgmtConstants.FN_DISPLAYVAL_INT);
		displayCols.add(PlanMgmtConstants.COMMENT_INT);
		List<Map<String, Object>> data = null;
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> planHealthData = service.findRevisions(PlanMgmtConstants.REPOSITORY_NAME, PlanMgmtConstants.MODEL_NAME, requiredFieldsMap, planId);
			data = planHistoryRendererImpl.processData(planHealthData, compareCols, displayCols);
		return data;
	}

	/**
	 * Loads Benefits,Rates and Brochure uploads history for specified plan.
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Map<String, Object>> loadPlanHistoryForIssuer(Integer planId) {
		Plan givenPlan = this.getPlan(planId);
		String planInsuranceType = givenPlan.getInsuranceType();
		Integer planAssociateId = null;
		List<Map<String, Object>> planAssociateData = null;
		if (planInsuranceType.equalsIgnoreCase(PlanInsuranceType.HEALTH.toString())) {
			planAssociateId = iPlanHealthRepository.getPlanHealthId(planId);
		} else if (planInsuranceType.equalsIgnoreCase(PlanInsuranceType.DENTAL.toString())) {
			planAssociateId = iPlanDentalRepository.getPlanDentalId(planId);
		}
		List<Map<String, Object>> planData = getPlanBrochureHistory(planId);
		if(null != planAssociateId){
			planAssociateData = getPlanAssociateHistoryWithEffDate(planAssociateId, Enum.valueOf(PlanInsuranceType.class, planInsuranceType));
		}
		if(null != planAssociateData){
			return mergeLists(planAssociateData, planData);
		}else{
			return null;
		}
	}

	private List<Map<String, Object>> getPlanAssociateHistoryWithEffDate(Integer planAssociateId, PlanInsuranceType planInsuranceType) {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(PlanMgmtConstants.UPDATE_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.RATEFILE_COL, FILE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.USER_NAME_ALIAS, USER_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.BENEFITFILE_COL, BENEFIT_FILE);
		requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");
		requiredFieldsMap.put(PlanMgmtConstants.RATE_EFFECTIVE_DATE_COL, "");
		requiredFieldsMap.put(PlanMgmtConstants.BENEFIT_EFFECTIVE_DATE_COL, "");

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(PlanMgmtConstants.RATEFILE_COL);
		compareCols.add(PlanMgmtConstants.BENEFITFILE_COL);
		compareCols.add(PlanMgmtConstants.PLAN_SBC_COL);
		compareCols.add(planAssociateId.toString());

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		displayCols.add(PlanMgmtConstants.FN_RATEFILE_VAL_INT);
		displayCols.add(PlanMgmtConstants.FN_BENEFITFILE_VAL_INT);
		displayCols.add(PlanMgmtConstants.FN_DISPLAYVAL_RATEHISTORY_INT);
		displayCols.add(PlanMgmtConstants.COMMENT_INT);
		displayCols.add(PlanMgmtConstants.EFFECTIVE_DATE_COMBINED_INT);
		List<Map<String, Object>> data = null;

		String repositoryName = null;
		String modelName = null;
		if (PlanInsuranceType.HEALTH.equals(planInsuranceType)) {
			requiredFieldsMap.put(PlanMgmtConstants.PLAN_SBC_COL, PLAN_SBC_FILE);
			displayCols.add(PlanMgmtConstants.PLAN_SBC_VAL_INT);
			repositoryName = RatesHistoryRendererImpl.REPOSITORY_NAME;
			modelName = RatesHistoryRendererImpl.MODEL_NAME;

		} else if (PlanInsuranceType.DENTAL.equals(planInsuranceType)) {
			repositoryName = RatesHistoryRendererImpl.DENTAL_REPOSITORY_NAME;
			modelName = RatesHistoryRendererImpl.DENTAL_MODEL_NAME;
		}

			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> planHealthData = service.findRevisions(repositoryName, modelName, requiredFieldsMap, planAssociateId);
			data = ratesHistoryRendererImpl.processData(planHealthData, compareCols, displayCols);
		return data;
	}

	@Override
	public boolean updateEnrollmentAvail(String planId, Integer userId, String userName, String effectiveDate, String enrollmentAvail, String comments) throws GIException, ParseException {
		Integer cmId = 0;
		Date futureEnrlAailEffDate = null;
		// Added server side validation as a part of HIX-31861
		Plan existingPlan = getPlan(Integer.parseInt(planId));
		if (!StringUtils.isEmpty(comments)) {
			cmId = saveComment(Integer.parseInt(planId), comments, TargetName.PLAN, userId, userName);
		}
		if (existingPlan != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT_MMDDYYYY);
			Date effDate = dateFormat.parse(effectiveDate);
			Date currentDate = new TSDate();
			String strCurrentDate = DateUtil.dateToString(currentDate,PlanMgmtConstants.REQUIRED_DATE_FORMAT_MMDDYYYY);
			Date newcurrentDate = dateFormat.parse(strCurrentDate);
			futureEnrlAailEffDate = existingPlan.getFutureErlAvailEffDate();
			/*	modifying this condition for JIRA HIX-65796  */
			/*if(newcurrentDate.after(effDate) || (existingPlan.getFutureErlAvailEffDate() != null && !(effDate.compareTo(existingPlan.getFutureErlAvailEffDate()) <= 0))){
				 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			}*/
			if(effDate.before(newcurrentDate) || (futureEnrlAailEffDate != null && effDate.after(futureEnrlAailEffDate))){
				if(LOGGER.isDebugEnabled()){
				   LOGGER.debug("Checking date for validation newcurrentDate, effDate and futureEnrlAailEffDate respectively: " 
					+ SecurityUtil.sanitizeForLogging(String.valueOf(newcurrentDate)) +" :: "+ SecurityUtil.sanitizeForLogging(String.valueOf(effDate)) 
																	+" :: "+ SecurityUtil.sanitizeForLogging(String.valueOf(futureEnrlAailEffDate)));
				}
				throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			}
			/*	adding this condition for JIRA HIX-65796  */
			if(newcurrentDate.equals(effDate)){
				existingPlan.setEnrollmentAvail(enrollmentAvail);
				existingPlan.setEnrollmentAvailEffDate(effDate);
				existingPlan.setFutureEnrollmentAvail(null);
				existingPlan.setFutureErlAvailEffDate(null);
			}else{
				existingPlan.setFutureEnrollmentAvail(enrollmentAvail);
				existingPlan.setFutureErlAvailEffDate(effDate);
			}

			existingPlan.setLastUpdatedBy(userId);
			if (cmId != null && cmId != 0) {
				existingPlan.setCommentId(cmId);
			} else {
				existingPlan.setCommentId(null);
			}
			
			iPlanRepository.save(existingPlan);
			return true;
		}
		return false;
	}

	private String formDMSFilePath(FileType fileType, Map<String, String> additionalData) throws GIException {
		String dmsPath = null;
		String additionalString = null;
		switch (fileType) {
		case ACTUARIAL_CERTIFICATES:
			additionalString = additionalData.get(PLAN_ID);
			if (additionalString == null) {
				additionalString = "";
				// throw new
				// GIException("Unable to form DMS path. Key 'PlanId' not provided as additional Data to form correct path.");
			} else {
				//additionalString += GhixConstants.FRONT_SLASH;
				additionalString += File.separator;
			}
			//dmsPath = GhixConstants.PLAN_DOC_PATH + GhixConstants.FRONT_SLASH + additionalString + GhixConstants.CERTI_FOLDER;
			dmsPath = GhixConstants.PLAN_DOC_PATH + File.separator + additionalString + GhixConstants.CERTI_FOLDER;
			break;
		case BENEFITS_FILE:
			additionalString = additionalData.get(PLAN_ID);
			if (additionalString == null) {
				throw new GIException("Unable to form DMS path. Key 'PlanId' not provided as additional Data to form correct path.");
			} else {
				//additionalString += GhixConstants.FRONT_SLASH;
				additionalString += File.separator;
			}
			//dmsPath = GhixConstants.PLAN_DOC_PATH + GhixConstants.FRONT_SLASH + additionalString + GhixConstants.BENEFITS_FOLDER;
			dmsPath = GhixConstants.PLAN_DOC_PATH + File.separator + additionalString + GhixConstants.BENEFITS_FOLDER;
			break;
		case RATES_FILE:
			additionalString = additionalData.get(PLAN_ID);
			if (additionalString == null) {
				throw new GIException("Unable to form DMS path. Key 'PlanId' not provided as additional Data to form correct path.");
			} else {
				//additionalString += GhixConstants.FRONT_SLASH;
				additionalString += File.separator;
			}
			//dmsPath = GhixConstants.PLAN_DOC_PATH + GhixConstants.FRONT_SLASH + additionalString + GhixConstants.RATES_FOLDER;
			dmsPath = GhixConstants.PLAN_DOC_PATH + File.separator + additionalString + GhixConstants.RATES_FOLDER;
			break;
		case BROCHURE:
			additionalString = additionalData.get(PLAN_ID);
			if (additionalString == null) {
				additionalString = "";
				// throw new
				// GIException("Unable to form DMS path. Key 'PlanId' not provided as additional Data to form correct path.");
			} else {
				//additionalString += GhixConstants.FRONT_SLASH;
				additionalString += File.separator;
			}
			//dmsPath = GhixConstants.PLAN_DOC_PATH + GhixConstants.FRONT_SLASH + additionalString + GhixConstants.BROCHURE_FOLDER;
			dmsPath = GhixConstants.PLAN_DOC_PATH + File.separator + additionalString + GhixConstants.BROCHURE_FOLDER;
			break;
		case CERTIFICATES:
			additionalString = additionalData.get(PLAN_ID);
			if (additionalString == null) {
				additionalString = "";
				// throw new
				// GIException("Unable to form DMS path. Key 'PlanId' not provided as additional Data to form correct path.");
			} else {
				//additionalString += GhixConstants.FRONT_SLASH;
				additionalString += File.separator;
			}
			//dmsPath = GhixConstants.PLAN_DOC_PATH + GhixConstants.FRONT_SLASH + additionalString + GhixConstants.PLAN_SUPPORT_DOC_FOLDER;
			dmsPath = GhixConstants.PLAN_DOC_PATH + File.separator + additionalString + GhixConstants.PLAN_SUPPORT_DOC_FOLDER;
			break;
		case PROVIDER:
			dmsPath = GhixConstants.PROVIDER_DOC_PATH;
			break;
		}
		
		return dmsPath;
	}

	private boolean isFileUploadedOnDMS(String fileName) {
		boolean fileUploaded = true;
		if (fileName != null) {
			fileUploaded = fileName.startsWith("workspace://");
		}
		return true;
	}

	@Autowired private IRegionRepository iRegionRepository;
	private ApplicationContext appContext;

	@Override
	public long getZipCountForCounty(String county) {
		return iRegionRepository.getZipCountForCounty(county);
	}

	@Override
	public long getZipCountForCountyAndZip(String county, String zip) {
		return iRegionRepository.getZipCountForCountyAndZip(county, zip);
	}

	@Override
	public long getZipCountForZip(String zip) {
		return iRegionRepository.getZipCountForZip(zip);
	}

	@Override
	public Plan getRequiredPlan(int issuerID, String issuerPlanNumber) {
		Calendar today = TSCalendar.getInstance();   // This gets the current date and time.
		Integer applicableYear = today.get(Calendar.YEAR);  // This returns the year as an int.
		Plan plan = iPlanRepository.getRequiredPlan(issuerID, issuerPlanNumber, applicableYear);
		if (plan != null) {
			return plan;
		} else {
			return null;
		}
	}

	@Override
	public PlanHealth getRequiredPlanHealthObject(int issuerID, int planID, String csType) {
		PlanHealth planHealth = iPlanHealthRepository.getRequiredPlanHealthObject(issuerID, planID, csType);
		if (planHealth != null) {
			return planHealth;
		} else {
			return null;
		}
	}

	@Override
	@Transactional
	public String updateCsrAmount(Float csrAdvPayAmt, Integer issuerID, Integer planID, String csType) {
		PlanHealth planHealth = getRequiredPlanHealthObject(issuerID, planID, csType);
		planHealth.setCsrAdvPayAmt(csrAdvPayAmt);
		iPlanHealthRepository.save(planHealth);
		return GhixConstants.RESPONSE_SUCCESS;
	}

	private boolean callPlanSyncRestClient(List<Map<String, String>> listOfPlans) {
			List<Map<String, String>> processedListOfPlans = processPlanDataForAHBXCall(listOfPlans); // process plan data before AHBX call
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Call plan sync REST URL =========" + SecurityUtil.sanitizeForLogging(String.valueOf(AHBXEndPoints.SYNC_PLAN_DATA)));
			}	
			String apiResponse = null; 
			apiResponse = ghixRestTemplate.postForObject(AHBXEndPoints.SYNC_PLAN_DATA, processedListOfPlans, String.class);
			
			if(apiResponse != null && !"".equals(apiResponse)){ // if response is not empty
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug( "Plan sync response status " + apiResponse.toString());
				}	
			}
			
			return true;
	}
		

	@Override
	public String getActualLogoName(String encodedFileName) throws ContentManagementServiceException {
		String fileName = null;
		if (encodedFileName != null) {
				fileName = ecmService.getContentById(encodedFileName).getOriginalFileName();
		}

		return fileName;
	}

	@Override
	public String getLogoPath() {
		return logoPath;
	}

	@Override
	public List<PlanHealth> getPlanHealthInfo(int issuerID, int planID, String planLevelString) {
		List<PlanHealth> planHealthList = iPlanHealthRepository.getPlanHealthInfo(issuerID, planID, planLevelString);
		if (planHealthList != null) {
			return planHealthList;
		} else {
			return null;
		}
	}

	@Override
	public List<PlanDental> getPlanDentalInfo(int issuerID, int planID, String planLevelString) {
		List<PlanDental> planDentalList = iPlanDentalRepository.getPlanDentalInfo(issuerID, planID, planLevelString);
		if (planDentalList != null) {
			return planDentalList;
		} else {
			return null;
		}
	}

	@Override
	public Integer getEnrollmentCount(int planID) {
		List<String> enrollmentCount = new ArrayList<String>();
		enrollmentCount = iPlanHealthRepository.getEnrollmentCount(planID);
		if (enrollmentCount != null) {
			return enrollmentCount.size();
		} else {
			return 0;
		}
	}

	@Override
	public List<Plan> getPlanByIDMarket(List<Integer> planIdList, int issuerId, String market) {
		return iPlanRepository.getPlanByIDMarket(planIdList, issuerId, market);
	}

	@Override
	public List<Plan> getPlanByIDMarket(List<Integer> planIdList, String market) {
		return iPlanRepository.getPlanByIDMarket(planIdList, market);
	}

	@Override
	public Plan getPlanByIDMarket(Integer planid, String market) {
		Plan requiredPlan = iPlanRepository.getPlanByIDMarket(planid, market);
		if (requiredPlan != null) {
			return requiredPlan;
		} else {
			return null;
		}
	}

	@Override
	public List<Plan> getPlanByHIOSID(String HIOSID) {
		//Calendar today = TSCalendar.getInstance();   // This gets the current date and time.
		//Integer applicableYear = today.get(Calendar.YEAR);  // This returns the year as an int.
		List<Plan> planObjList = new ArrayList<Plan>();
		try{
			planObjList = iPlanRepository.getPlanByHIOSID(HIOSID);
		}catch(Exception ex){
			LOGGER.error("Exception occured in getPlanByHIOSID " + ex);
		}
		return planObjList;
	}

	@Override
	public List<PlanHealthBenefit> getPlanHelthBenefit(Integer planId) {
		return iPlanHealthBenfitRepository.getPlanHelthBenefitByPlanId(planId);
	}

	@Override
	public List<PlanDentalBenefit> getPlanDentalBenefit(Integer planId) {
		return iPlanDentalBenfitRepository.getPlanDentalBenefit(planId);
	}

	@Override
	public List<PlanDentalCost> getPlanDentalCost(Integer planId){
		return iPlanDentalCostRepository.getPlanDentalCostByPlanId(planId);
	}
	
	@Override
	public List<PlanHealthCost> getPlanHealthCost(Integer planId){
		return iPlanHealthCostRepository.getPlanHealthCostByPlanId(planId);
	}
	
	@Override
	public boolean updatePlanWithStatus(Integer planId, String status) {
		boolean opStatus = Boolean.FALSE;
		Plan plan = null;

		try {
			if (null == planId) {
				throw new IllegalArgumentException("The plan identifier is missing.");
			}

			if (null == status || status.trim().isEmpty()) {
				throw new IllegalArgumentException("Invalid status specified in call.");
			} else {
				status = status.trim();
			}

			plan = this.getPlan(planId);

			Validate.notNull(plan, "No Plan for given identifier found in database.");

			Map<PlanStatus, String> allowedPlanStatuses = plan.getPlanStatuses();

			Validate.notNull(allowedPlanStatuses, "No Plan statuses found.");
			Validate.notEmpty(allowedPlanStatuses, "Plan statuses are missing.");

			if (allowedPlanStatuses.containsKey(status)) {
				plan.setStatus(status);

				plan = this.updatePlan(plan);

				Validate.notNull(plan, "There is problem updating Plan with given status.");

				LOGGER.info("Plan updated successfully for specified status");
				opStatus = Boolean.TRUE;
			} else {
				LOGGER.warn("Invalid status '" + SecurityUtil.sanitizeForLogging(String.valueOf(status)) + "' specified for updating Plan status.");
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occurred while updating plan status. Exception:", ex);
			opStatus = Boolean.FALSE;
		}

		return opStatus;
	}

	@Override
	public List<Map<String, Object>> loadPlanRateGroupByEffectiveDate(Integer planId, String role) {
		List<Map<String, Object>> ratesList = new ArrayList<Map<String, Object>>();
		EntityManager em = null;
		
		StringBuilder downloadLinek = null;
		if (planId != null && planId > 0) {

			String buildquery = "SELECT to_char(pr.last_update_timestamp, 'YYYY-MM-DD') AS last_update_timestamp, to_char(pr.effective_start_date, 'YYYY-MM-DD') AS effective_start_date, to_char(pr.effective_end_date, 'YYYY-MM-DD') AS effective_end_date FROM pm_plan_rate pr WHERE "
					+ "pr.plan_id = "
					+ planId
					+ " GROUP BY to_char(pr.last_update_timestamp, 'YYYY-MM-DD'), to_char(pr.effective_start_date, 'YYYY-MM-DD'), to_char(pr.effective_end_date, 'YYYY-MM-DD') ORDER BY to_char(pr.last_update_timestamp, 'YYYY-MM-DD')";
         try{
		    em = emf.createEntityManager();
			List rsList = em.createNativeQuery(buildquery).getResultList();
			Iterator rsIterator = rsList.iterator();
			if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Build query " + SecurityUtil.sanitizeForLogging(String.valueOf(buildquery)));
			}
			while (rsIterator.hasNext()) {
				Object[] objArray = (Object[]) rsIterator.next();
				Map<String, Object> rateMap = new HashMap<String, Object>();
				rateMap.put("lastUpdateTimestamp", DateUtil.StringToDate(objArray[0].toString(), PlanMgmtConstants.RATEDATEFORMAT));
				rateMap.put("rateEffectiveDate", DateUtil.StringToDate(objArray[1].toString(), PlanMgmtConstants.RATEDATEFORMAT));
				rateMap.put("rateEffectiveEndDate", DateUtil.StringToDate(objArray[2].toString(), PlanMgmtConstants.RATEDATEFORMAT));
				rateMap.put("userName", "");
				rateMap.put("commentId", "");
				/*String downloadLinek = "<a href='/hix/admin/planmgmt/downloadrates/"
						+ planId
						+ "?"
						+ "effective_start_date="+StringUtils.replace(objArray[1].toString(), " ", "@")
						+ "&"
						+ "effective_end_date="+StringUtils.replace(objArray[2].toString(), " ", "@")
						+ "'>Download File</a>";*/

				if (role.equalsIgnoreCase(PlanMgmtConstants.ADMIN_ROLE)) {
					downloadLinek = new StringBuilder("<a href='/hix/admin/planmgmt/downloadrates/"); 
					downloadLinek.append(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(planId)));
				}
				else {
					downloadLinek = new StringBuilder("<a href='/hix/issuer/planmgmt/downloadrates/"); 
					downloadLinek.append(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(planId)));
				}
				downloadLinek.append("?");
				downloadLinek.append("effective_start_date=");
				downloadLinek.append(StringUtils.replace(objArray[1].toString(), " ", "@"));
				downloadLinek.append("&");
				downloadLinek.append("effective_end_date=");
				downloadLinek.append(StringUtils.replace(objArray[2].toString(), " ", "@"));
				downloadLinek.append("'>Download File</a>");

				rateMap.put("downloadLink", downloadLinek);
				ratesList.add(rateMap);
			}
		 }catch (Exception e) {
				LOGGER.error("ERROR in loadPlanRateGroupByEffectiveDate >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
				throw new GIRuntimeException("ERROR in loadPlanRateGroupByEffectiveDate >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
		 }finally{
				if(em !=null && em.isOpen()){
					em.clear();
					em.close();
				}
		 }
		}
		return ratesList;
	}

	@Override
	public List<Map<String, Object>> loadPlanHealthBenefitHistory(Integer planId, String role) {
		Integer planHealthId = iPlanHealthRepository.getPlanHealthId(planId);
		List<Map<String, Object>> planBrochureData = getPlanBrochureHistory(planId);
		List<Map<String, Object>> planBenefitData = getHealthPlanBenefitAndSbcBrochure(planHealthId, planId, role);
		return mergeLists(planBrochureData, planBenefitData);

	}

	private List<Map<String, Object>> getHealthPlanBenefitAndSbcBrochure(Integer planHealthId, Integer planId, String role) {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();

		requiredFieldsMap.put(PlanMgmtConstants.UPDATE_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.USER_NAME_ALIAS, USER_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");
		requiredFieldsMap.put(PlanMgmtConstants.BENEFITFILE_COL, BENEFIT_FILE);
		requiredFieldsMap.put(PlanMgmtConstants.BENEFIT_EFFECTIVE_DATE_COL, "");
		requiredFieldsMap.put(PlanMgmtConstants.PLAN_SBC_COL, PLAN_SBC_FILE);

		List<String> compareCols = new ArrayList<String>();

		compareCols.add(PlanMgmtConstants.PLAN_SBC_COL);
		compareCols.add(PlanMgmtConstants.BENEFITFILE_COL);
		compareCols.add(planId.toString());
		if(role.equalsIgnoreCase(PlanMgmtConstants.ADMIN_ROLE)){
			compareCols.add(PlanMgmtConstants.ADMIN_ROLE);
		}else{
			compareCols.add(PlanMgmtConstants.ISSUER_ROLE);
		}

		List<Integer> displayCols = new ArrayList<Integer>();

		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		displayCols.add(PlanMgmtConstants.FN_BENEFITFILE_VAL_INT);
		displayCols.add(PlanMgmtConstants.FN_DISPLAYVAL_RATEHISTORY_INT);
		displayCols.add(PlanMgmtConstants.COMMENT_INT);
		displayCols.add(PlanMgmtConstants.EFFECTIVE_DATE_COMBINED_INT);
		displayCols.add(PlanMgmtConstants.PLAN_SBC_VAL_INT);
		List<Map<String, Object>> data = null;

		DisplayAuditService service = objectFactory.getObject();
		service.setApplicationContext(appContext);
		List<Map<String, String>> planHealthData = service.findRevisions(RatesHistoryRendererImpl.REPOSITORY_NAME, RatesHistoryRendererImpl.MODEL_NAME, requiredFieldsMap, planHealthId);
		data = ratesHistoryRendererImpl.processData(planHealthData, compareCols, displayCols);

		return data;
	}

	private List<Map<String, Object>> getDentalPlanBenefitAndSbcBrochure(Integer planDentalId, Integer planId, String role) {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();

		requiredFieldsMap.put(PlanMgmtConstants.UPDATE_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.USER_NAME_ALIAS, USER_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");
		requiredFieldsMap.put(PlanMgmtConstants.BENEFITFILE_COL, BENEFIT_FILE);
		requiredFieldsMap.put(PlanMgmtConstants.BENEFIT_EFFECTIVE_DATE_COL, "");
		requiredFieldsMap.put(PlanMgmtConstants.PLAN_SBC_COL, PLAN_SBC_FILE);

		List<String> compareCols = new ArrayList<String>();

		compareCols.add(PlanMgmtConstants.PLAN_SBC_COL);
		compareCols.add(PlanMgmtConstants.BENEFITFILE_COL);
		compareCols.add(planId.toString());
		if(role.equalsIgnoreCase(PlanMgmtConstants.ADMIN_ROLE)){
			compareCols.add(PlanMgmtConstants.ADMIN_ROLE);
		}else{
			compareCols.add(PlanMgmtConstants.ISSUER_ROLE);
		}

		List<Integer> displayCols = new ArrayList<Integer>();

		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		displayCols.add(PlanMgmtConstants.FN_BENEFITFILE_VAL_INT);
		displayCols.add(PlanMgmtConstants.FN_DISPLAYVAL_RATEHISTORY_INT);
		displayCols.add(PlanMgmtConstants.COMMENT_INT);
		displayCols.add(PlanMgmtConstants.EFFECTIVE_DATE_COMBINED_INT);
		displayCols.add(PlanMgmtConstants.PLAN_SBC_VAL_INT);
		
		List<Map<String, Object>> data = null;

		DisplayAuditService service = objectFactory.getObject();
		service.setApplicationContext(appContext);
		List<Map<String, String>> planDentalData = service.findRevisions(RatesHistoryRendererImpl.DENTAL_REPOSITORY_NAME, RatesHistoryRendererImpl.DENTAL_MODEL_NAME, requiredFieldsMap, planDentalId);
		data = ratesHistoryRendererImpl.processData(planDentalData, compareCols, displayCols);

		return data;
	}
	
	@Override
	public List<Object[]> downloadPlanRate(Integer planId, String effectiveStartDate, String effectiveEndDate) throws IOException, ParseException {

		SimpleDateFormat sdfSource = new SimpleDateFormat(SOURCE_DATE_FORMAT);
		SimpleDateFormat sdfDestination = new SimpleDateFormat(DESTINATION_DATE_FORMAT);
		EntityManager em = null;

			String buildquery = "SELECT pra.rating_area, pr.min_age, pr.max_age, pr.tobacco, pr.rate FROM pm_plan_rate pr,pm_rating_area pra where pra.id=pr.rating_area_id and pr.plan_id="
					+ planId;

			if (!StringUtils.isBlank(effectiveStartDate)) {
				String[] startDatArr = effectiveStartDate.split("@");
				Date startDate = sdfSource.parse(startDatArr[0]);
				String effectStartDate = sdfDestination.format(startDate);
				buildquery = buildquery
						+ " AND pr.effective_start_date >= to_date('"
						+ effectStartDate + "','MM/dd/yyyy')";
			}

			if (!StringUtils.isBlank(effectiveEndDate)) {
				String[] endDatArr = effectiveEndDate.split("@");
				Date endDate = sdfSource.parse(endDatArr[0]);
				String effectEndDate = sdfDestination.format(endDate);
				buildquery = buildquery
						+ " AND pr.effective_end_date <= to_date('"
						+ effectEndDate + "','MM/dd/yyyy')";
			}

			buildquery = buildquery + "AND pr.is_deleted = 'N'";
			buildquery = buildquery
					+ " GROUP BY pra.rating_area, pr.min_age, pr.max_age, pr.tobacco, pr.rate ORDER BY pra.rating_area";
			
			if(LOGGER.isDebugEnabled()){
			   LOGGER.debug("build query : " + SecurityUtil.sanitizeForLogging(String.valueOf(buildquery)));
			}

			List rsList = null;
			try{
			  em = emf.createEntityManager();
			  rsList = em.createNativeQuery(buildquery).getResultList();
			}catch (Exception e) {
				LOGGER.error("ERROR in downloadPlanRate >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
				throw new GIRuntimeException("ERROR in downloadPlanRate >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			}finally{
				if(em !=null && em.isOpen()){
					em.clear();
					em.close();
				}
			}
			return rsList;
	}

	@Override
	public String getDocumentNameFromECMorUCM(String configurationType, String documentID, String filename) throws ContentManagementServiceException {
		if (ALFRESCO.equals(configurationType)) {
			return getActualFileName(documentID);
		} else {
			return filename;
		}
	}

	public byte[] getAttachment(String documentID) throws Exception {
		byte[] attachment = null;

			if (documentID != null) {
				attachment = ecmService.getContentDataById(documentID);
				if(LOGGER.isDebugEnabled()){
				   LOGGER.debug("AlfrescoContentManagementService.getContentDataById attachment bytes" + SecurityUtil.sanitizeForLogging(String.valueOf(attachment.length)));
				}   
			}

		return attachment;
	}


	@Override
	@Transactional
	public Map<Integer, Integer> getTenantCount(List<Integer> planIds) {

		// map with key as plan_id and value as the count of tenants
		Map<Integer, Integer> resultData = new HashMap<Integer, Integer>();
		    EntityManager em = null;
		    try{
				em = emf.createEntityManager();
				StringBuilder buildquery = new StringBuilder("SELECT plan_id,count(*) FROM PM_TENANT_PLAN where plan_id in (");
	
				buildquery.append(getListasString(planIds));
				buildquery.append(") group by plan_id");
				
				if(LOGGER.isDebugEnabled()){
				   LOGGER.debug("Build query getTenantCount " + SecurityUtil.sanitizeForLogging(String.valueOf(buildquery)));
				}
	
				List rsList = em.createNativeQuery(buildquery.toString()).getResultList();
				Iterator rsIterator = rsList.iterator();
	
				while (rsIterator.hasNext()) {
					Object[] objArray = (Object[]) rsIterator.next();
					resultData.put(Integer.parseInt(objArray[0].toString()), Integer.parseInt(objArray[1].toString()));
				}
		    }catch (Exception e) {
				LOGGER.error("ERROR in getTenantCount >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
				throw new GIRuntimeException("ERROR in getTenantCount >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			}finally{
				if(em !=null && em.isOpen()){
					em.clear();
					em.close();
				}
			}
		    
		return resultData;
	}

	/*
	 * @Override
	 * 
	 * @Transactional
	 */
	public String deleteTenants(Integer planId, String[] selectedTenants) {
		EntityManager em = null;
		EntityTransaction t = null;
		
		try {
			em = emf.createEntityManager();
			t = em.getTransaction();
			t.begin();
			StringBuilder buildquery = new StringBuilder("delete from PM_TENANT_PLAN where (tenant_id in (");
			buildquery.append("select id from tenant where code in (");
			buildquery.append(getCommaSeparatedStrings(selectedTenants));
			buildquery.append(") ) and plan_id in (");
			buildquery.append(planId);
			buildquery.append(") )");
			em.createNativeQuery(buildquery.toString()).executeUpdate();
			t.commit();
		} catch (Exception e) {
			LOGGER.error("Error in deleteTenants while deleting the tenants : ", e);
			t.rollback();
			return DELFAILED;
		} finally {
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		return REMOVED;
	}

	@Override
	@Transactional
	public List<PlanTenantUtil> addTenants(List<PlanTenantUtil> incomingData, String[] selectedTenants) {
		List<PlanTenantUtil> failedList = new ArrayList<PlanTenantUtil>();
		try {
			TenantPlan tenantPlan = null;
			Plan plan = new Plan();
			Tenant tenant = new Tenant();

			for (String s : selectedTenants) {
				for (PlanTenantUtil temp : incomingData) {
					plan = iPlanRepository.findOne(temp.getPlanId());
					tenant = iTenantRepository.getTenantByTenantCode(s);
					tenantPlan = iTenantPlanRepository.getTenantPlanByTenantId(tenant.getId(), plan.getId());
					if (null == tenantPlan) {
						tenantPlan = new TenantPlan();
						tenantPlan.setPlan(plan);
						tenantPlan.setTenant(tenant);
						tenantPlan.setCreatedDate(new TSDate());
						tenantPlan.setUpdatedDate(new TSDate());
						iTenantPlanRepository.save(tenantPlan);
					} else {
						// add the plan to failed list as already has the same
						// tenant
						failedList.add(temp);
					}

				}

			}

		} catch (Exception e) {
			LOGGER.error("Error in addTenants While adding the tenants : ", e);
			failedList.addAll(incomingData);
			return failedList;
		}
		return failedList;
	}

	/**
	 * Utility method to get comma separated plan_ids
	 * 
	 * @param planIds
	 * @return
	 */
	public String getListasString(List<Integer> planIds) {
		StringBuilder resultString = new StringBuilder("");
		int i = planIds.size();
		for (Integer j : planIds) {

			resultString.append(j.toString());
			if (i > 1) {
				resultString.append(",");
			}
			i--;
		}
		return resultString.toString();
	}

	/**
	 * Utility method to get comma separated tenants
	 * 
	 * @param selectedTenants
	 * @return
	 */
	public String getCommaSeparatedStrings(String[] selectedTenants) {
		StringBuilder resultString = new StringBuilder("");
		int i = selectedTenants.length;
		for (String s : selectedTenants) {
			resultString.append("'" + s + "'");
			if (i > 1) {
				resultString.append(",");
			}
			i--;
		}

		return resultString.toString();
	}

	@Override
	public List<Plan> getPlanList(int issuerID, String planType) {
		List<Plan> plans = new ArrayList<Plan>();
		EntityManager em = null;
		
		String buildquery = "";
		if (planType.equalsIgnoreCase("HEALTH")) {
			buildquery = "SELECT p.id,p.issuer_plan_number FROM plan p INNER JOIN plan_health ph ON p.id=ph.plan_id WHERE p.issuer_id="
					+ issuerID + " order by p.id";
		} else {
			buildquery = "SELECT p.id,p.issuer_plan_number FROM plan p INNER JOIN plan_dental pd ON p.id=pd.plan_id WHERE p.issuer_id="
					+ issuerID + " order by p.id";
		}
		if(LOGGER.isDebugEnabled()){
	       LOGGER.debug(" SQL === " + SecurityUtil.sanitizeForLogging(String.valueOf(buildquery)));
		}

		try{
			em = emf.createEntityManager();
			List rsList = em.createNativeQuery(buildquery).getResultList();
	
			if (rsList != null && rsList.size() > 0) {
				Iterator rsIterator = rsList.iterator();
				while (rsIterator.hasNext()) {
					Object[] objArray = (Object[]) rsIterator.next();
					Plan plan = new Plan();
					plan.setId(Integer.parseInt(objArray[0].toString()));
					plan.setIssuerPlanNumber(objArray[1].toString());
					plans.add(plan);
				}
				return plans;
			}
			return null;
		}catch (Exception e) {
			LOGGER.error("ERROR in getPlanList >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			throw new GIRuntimeException("ERROR in getPlanList >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		
	}
	
	@Override
	public Content getContent(String documentID) 
			throws Exception{
		Content content = null;
		
			if (documentID!=null) {
				content = ecmService.getContentById(documentID);
				if(LOGGER.isDebugEnabled()){
				   LOGGER.debug("AlfrescoContentManagementService.getContentById attachment bytes" + SecurityUtil.sanitizeForLogging(String.valueOf(content.getTitle())));
				}   
			}
		
		return content;
	}
	
	//for dynamic configuration
	public Map<String,String> gethealthbenefitMap()
	{
		String constname;
		String name;
		Map<String, String> healthBenefitMap = new HashMap<String, String>();
		for(PlanmgmtConfiguration.PlanmgmtConfigurationEnum pcE : PlanmgmtConfiguration.PlanmgmtConfigurationEnum.values()){
			 constname = pcE.name().endsWith(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY)?pcE.name().replace(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY,""):pcE.name();
			 name = pcE.getValue();
			if(name.contains(PlanMgmtConstants.PLANMGMT_HEALTHBENEFITS)){
				healthBenefitMap.put(constname,DynamicPropertiesUtil.getPropertyValue(name));
			}
		}
		return healthBenefitMap;
	}
	
	public Map<String,String> getDentalbenefitMap(){
		String constname;
		String name;
		Map<String, String> dentalBenefitMap = new HashMap<String, String>();
		
		for(PlanmgmtConfiguration.PlanmgmtConfigurationEnum pcE : PlanmgmtConfiguration.PlanmgmtConfigurationEnum.values()){
			 constname = pcE.name().endsWith(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY)?pcE.name().replace(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY,""):pcE.name();name = pcE.getValue();
			if(name.contains(PlanMgmtConstants.PLANMGMT_DENTALBENEFITS)){
				dentalBenefitMap.put(constname,DynamicPropertiesUtil.getPropertyValue(name));
			}
		}
		return dentalBenefitMap;
	}
	
	@Override
	public boolean updatePlanStatus(Integer planId, AccountUser user, String planStatus, String certSuppFile, String comments,Date deCertificationDate,Date enrollmentEndDate,String issuerVerificationStatus) throws ParseException {
			if(user == null){
				user = new AccountUser();
			}
			int cmId = 0;
			Plan existingPlan = iPlanRepository.findOne(planId);
			
			if(enrollmentEndDate != null && enrollmentEndDate.after(deCertificationDate)){
				throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			}
			existingPlan.setDeCertificationEffDate(deCertificationDate);
			existingPlan.setEnrollmentEndDate(enrollmentEndDate);
			
			if(StringUtils.isNotBlank(comments)) {
				cmId = saveComment(planId, comments, TargetName.PLAN, user.getId(), user.getUserName());
			}
			if((!Plan.PlanStatus.DECERTIFIED.toString().equals(planStatus)) || (deCertificationDate != null && DateUtils.isSameDay(deCertificationDate, new TSDate()))){
				existingPlan.setStatus(planStatus);
			}
			
			if(StringUtils.isNotBlank(issuerVerificationStatus)) {
				existingPlan.setIssuerVerificationStatus(issuerVerificationStatus);
			}
			if (cmId > 0) {
				existingPlan.setCommentId(cmId);
			} else {
				existingPlan.setCommentId(null);
			}
			if(STATUS_CERTIFIED.equalsIgnoreCase(planStatus)) {
				existingPlan.setCertifiedby(String.valueOf(user.getId()));
				existingPlan.setCertifiedOn(new TSDate());
				// on certification/de-certification of plan set Issuer Verification Status to Pending except PHIX profile
				if (!DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE).equalsIgnoreCase(GhixConstants.PHIX)) {				
					existingPlan.setIssuerVerificationStatus(PlanMgmtService.ISSUER_VERIFICATION_STATUS_PENDING);
				}
			}
			if(!Plan.PlanStatus.DECERTIFIED.toString().equals(planStatus)){
				existingPlan.setDeCertificationEffDate(null);
				existingPlan.setEnrollmentEndDate(null);
			}
			existingPlan.setSupportFile(certSuppFile);
			existingPlan.setLastUpdatedBy(user.getId());
			iPlanRepository.save(existingPlan);
		return true;
	}
	
	@Override
	public String getInsuranceType(List<Integer> planIdList) {
		String insuranceType = null;
		try{
			insuranceType = iPlanRepository.getInsuranceTypeForPlanList(planIdList);
		}catch(Exception ex){
			LOGGER.error("Exceptin in getInsuranceType @PlanReportServiceImpl: ", ex);
		}
		return insuranceType;
	}

	@Override
	public String savePlanReportInfo(PlanCompareReport planCompareReport) {
		try{
			iPlanCompareReport.save(planCompareReport);
		}catch(Exception ex){
			LOGGER.error("Exception Occured @savePlanReportInfo in PlanReportServiceImpl: ", ex);
		}
		return null;
	}
	
	public ServiceAreaDetails getServiceAreaDetails(Integer planId, Integer issuerId) {
		DateFormat inputDate = new SimpleDateFormat("yyyy-mm-DD");
		DateFormat outputDate = new SimpleDateFormat("mm/DD/yyyy");
		EntityManager em = null;
		
		StringBuilder buildQuery = new StringBuilder();
		buildQuery.append("SELECT P.name, p.issuer_plan_number, p.start_date, p.end_date, pisa.serff_service_area_id, psa.ZIP, psa.county ");
		buildQuery.append("FROM ");
		buildQuery.append("plan p, pm_service_area psa, pm_issuer_service_area pisa");
		buildQuery.append(" WHERE");
		if(null != issuerId &&  issuerId != 0) {
			buildQuery.append(" p.service_area_id = psa.service_area_id AND pisa.id =  p.service_area_id AND psa.is_deleted= 'N' AND p.id="+planId+" AND p.issuer_id=" +issuerId);
		} else {
			buildQuery.append(" p.service_area_id = psa.service_area_id AND pisa.id =  p.service_area_id AND psa.is_deleted= 'N' AND p.id="+planId);
		}

      try{
		em = emf.createEntityManager();
		List resultList = em.createNativeQuery(buildQuery.toString()).getResultList();
		List<ZipCounty> zipCountyList = new ArrayList<ZipCounty>();
		ServiceAreaDetails serviceArea= new ServiceAreaDetails();
		if(resultList.size() > PlanMgmtConstants.ZERO){
			for(int i=0; i < resultList.size(); i++){
				Object[] objArray = (Object[]) resultList.get(i);
				
				if(i == 0){
					try {
						ZipCounty zipCounty = new ZipCounty();
						serviceArea.setName((objArray[PlanMgmtConstants.ZERO] != null) ? objArray[PlanMgmtConstants.ZERO].toString() : "");
						serviceArea.setIssuerPlanNumber((objArray[PlanMgmtConstants.ONE] != null) ? objArray[PlanMgmtConstants.ONE].toString() : "");
						String[] startDate = (String[]) ((objArray[PlanMgmtConstants.TWO] != null) ? objArray[PlanMgmtConstants.TWO].toString().split(" ") : ""); 
						if(StringUtils.isNotBlank(startDate[PlanMgmtConstants.ZERO])){
							Date date = inputDate.parse(startDate[PlanMgmtConstants.ZERO]);
							serviceArea.setStartDate(outputDate.format(date));
						}else{
							serviceArea.setStartDate("");
						}
						
						String[] endDate = (String[]) ((objArray[PlanMgmtConstants.THREE] != null) ? objArray[PlanMgmtConstants.THREE].toString().split(" ") : "");
						if(StringUtils.isNotBlank(endDate[PlanMgmtConstants.ZERO])){
							Date date = inputDate.parse(endDate[PlanMgmtConstants.ZERO]);
							serviceArea.setEndDate(outputDate.format(date));
						}else{
							serviceArea.setEndDate("");
						}
						serviceArea.setSerfServiceAreaId((objArray[PlanMgmtConstants.FOUR] != null) ? objArray[PlanMgmtConstants.FOUR].toString() : "");
						zipCounty.setZipCode((objArray[PlanMgmtConstants.FIVE] != null) ? objArray[PlanMgmtConstants.FIVE].toString() : "0");
						zipCounty.setCounty((objArray[PlanMgmtConstants.SIX] != null) ? objArray[PlanMgmtConstants.SIX].toString() : "");
						zipCountyList.add(zipCounty);
					} catch (Exception e) {
						LOGGER.error("Exception Occured while parsing date: ", e);
					}
					
				}else{
					ZipCounty zipCounty = new ZipCounty();
					zipCounty.setZipCode((objArray[PlanMgmtConstants.FIVE] != null) ? objArray[PlanMgmtConstants.FIVE].toString() : "0");
					zipCounty.setCounty((objArray[PlanMgmtConstants.SIX] != null) ? objArray[PlanMgmtConstants.SIX].toString() : "");
					zipCountyList.add(zipCounty);
				}
			}
			serviceArea.setZipCounty(zipCountyList);
		}
      	return serviceArea;
      }
      catch (Exception e) {
			LOGGER.error("ERROR in getServiceAreaDetails >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			throw new GIRuntimeException("ERROR in getServiceAreaDetails >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
	}
	
	@Override
	public void downLoadServiceArea(ServiceAreaDetails serviceAreaDetails, HttpServletResponse response) throws IOException{
		FileInputStream fis = null;
		FileOutputStream outFile = null;
		
		try{
			List<String[]> list = ServiceAreaMapper.prepareServiceAreaListToExcel(serviceAreaDetails);
			String fileName = PlanMgmtConstants.SA_FILE_NAME;
			String filePath = uploadPath + File.separator + fileName;
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				HSSFWorkbook workBook = ExcelGenerator.createServiceAreaExcelSheet(list);
				outFile = new FileOutputStream(filePath);
				workBook.write(outFile);
				outFile.close();
				response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
				response.setContentType("application/octet-stream");
				File file = new File(filePath);
				fis =  new FileInputStream(file);
				FileCopyUtils.copy(fis, response.getOutputStream());
				if(file.exists()){
					file.delete();
				}
				fis.close();
				LOGGER.info("File downloaded successfully!!!");
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		}catch(Exception e){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_QHP_DETAIL_GET .getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}finally{
			 IOUtils.closeQuietly(fis);
			 IOUtils.closeQuietly(outFile);
		}
	}
	
	@Override
	@Transactional
	public void undoDecertification(Plan plan){
		iPlanRepository.save(plan);
	}
	
	/**
	 * Utility method to get comma separated plan_ids
	 * 
	 * @param planIds
	 * @return
	 */
	@Override
	public boolean modifyPlan(Plan plan) {
		try {
			Plan savedPlan = iPlanRepository.save(plan);
			if (savedPlan != null) {
				return true;
			} 
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	
	
	@Override
	public String findEnrollmentAvailablePlans(List<Plan> plansToUpdate){
		StringBuilder planNumbers = new StringBuilder();
		for(Plan plan: plansToUpdate){
			if(StringUtils.isNotBlank(plan.getFutureEnrollmentAvail())){
				planNumbers.append(plan.getIssuerPlanNumber());
				planNumbers.append(" ");
			}
		}
		return planNumbers.toString();
	}
	
	@Override
	public String findDeCertifiedPlans(List<Plan> plansToUpdate){
		StringBuilder planNumbers = new StringBuilder();
		Date toDate = new TSDate();
		for(Plan plan: plansToUpdate){
			if(plan.getDeCertificationEffDate() != null && plan.getDeCertificationEffDate().after(toDate)){
				planNumbers.append(plan.getIssuerPlanNumber());
				planNumbers.append(" ");
			}
		}
		return planNumbers.toString();
	}
	
	public List<Map<String, Integer>> getPlanYears(String insuranceType, Integer issuerId) {
		List<Map<String, Integer>> listOfplanyears = new ArrayList<Map<String, Integer>>();
		String buildquery = "";
		EntityManager em = null;
		
		if(issuerId != PlanMgmtConstants.ZERO){
			buildquery = "select distinct(applicable_year) from plan where insurance_type = '"+insuranceType+"' and is_deleted = 'N' and issuer_id = " + issuerId + "order by applicable_year desc";
		}else{
			buildquery = "select distinct(applicable_year) from plan where insurance_type = '"+insuranceType+"' and is_deleted = 'N' order by applicable_year desc";
		}
		if(LOGGER.isDebugEnabled()){
		   LOGGER.debug(" SQL === " + SecurityUtil.sanitizeForLogging(String.valueOf(buildquery)));
		}
		
		try{
			em = emf.createEntityManager();
			List rsList = em.createNativeQuery(buildquery).getResultList();
			if (rsList != null && rsList.size() > 0) {
				Iterator rsIterator = rsList.iterator();
				while (rsIterator.hasNext()) {
					BigDecimal objArray = (BigDecimal) rsIterator.next();
					Map<String, Integer> planYearMap = new HashMap<String, Integer>();
					planYearMap.put(PlanMgmtConstants.PLAN_YEARS_KEY, Integer.parseInt(objArray.toString()));
					listOfplanyears.add(planYearMap);
				}
			}
		}
		catch (Exception e) {
			LOGGER.error("ERROR getting applicable_year form plan table >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			throw new GIRuntimeException("ERROR getting applicable_year form plan table >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		
		
		return listOfplanyears;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Plan> getPlans(List<Integer> ids) {
		return iPlanRepository.getPlans(ids);
	}
	
	
	/*
	 * return PlanHealth Object
	 * @see com.getinsured.hix.planmgmt.service.PlanMgmtService#getPlanHealth(int)
	 */
	@Override
	public PlanHealth getPlanHealth(int id) {
		return iPlanHealthRepository.findOne(id);
	}

	/*
	 * return PlanDental Object
	 * @see com.getinsured.hix.planmgmt.service.PlanMgmtService#getPlanDental(int)
	 */
	@Override
	public PlanDental getPlanDental(int id) {
		return iPlanDentalRepository.findOne(id);
	}
	
	@Override
	public List<String[]> bulkUpdateCertificationStatus(String status, int lastUpdatedBy, String insuranceType, String issuerPlanNumber, 
    		String planLevel, String planMarket, String filterStatus, String isVerified, int planYear, int issuerId, String stateCode){
		List<String[]> updatedPlanList = null;
		try {
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Bulk updating plan certification information");
			}	
			int updatedCount = 0;			
			/*LOGGER.debug("Updating plans in bulk for criterias and updating info status, lastUpdatedBy, " + 
					"insuranceType, state, issuerPlanNumber, planLevel, planMarket, filterStatus, exchangeType, " + 
					"isVerified,planYear, issuerId " + status + " , "+ lastUpdatedBy + " , " + insuranceType + " , " + issuerPlanNumber + " , " + 
					planLevel + " , " + planMarket + " , " + filterStatus + " , " + isVerified + " , " + planYear + " , " + issuerId);*/
			
			if(insuranceType.equals(PlanMgmtConstants.HEALTH))						// if insurance type is HEALTH
			{
				if(!stateCode.equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_CA))		// if state exchange is ID/MN
				{
					if(status.equalsIgnoreCase(PlanMgmtConstants.CERTIFIED))		// if new plan status is CERTIFIED
					{
						// pull list of plans set for certification
						updatedPlanList = iPlanRepository.getBulkUpdatedQHPPlanList(insuranceType, issuerPlanNumber, planMarket, 
							filterStatus, isVerified, planYear, planLevel, issuerId);
						
						// update plan status as CERTIFIED and issuer verification status as VERIFIED
						updatedCount = iPlanRepository.bulkUpdateCertificationStatusAndVerificationStatusQHPPlans(status, lastUpdatedBy, 
								insuranceType, issuerPlanNumber, planMarket, filterStatus, isVerified, planYear, planLevel, issuerId);
						
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug(updatedPlanList.toString());
						}	
					} 
					else 
					{
						// update plan status as INCOMPLETE
						updatedCount = iPlanRepository.bulkUpdateCertificationStatusQHPPlans(status, lastUpdatedBy, 
								insuranceType, issuerPlanNumber, planMarket, filterStatus, isVerified, planYear, planLevel, issuerId);
					}
				}
				else if(stateCode.equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_CA))    // if state exchange is CA
				{
					// pull list of plans set for certification or Incomplete
					updatedPlanList = iPlanRepository.getBulkUpdatedQHPPlanList(insuranceType, issuerPlanNumber, planMarket, 
							filterStatus, isVerified, planYear, planLevel, issuerId);
					
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug(updatedPlanList.toString());
					}	

					if(status.equalsIgnoreCase(PlanMgmtConstants.CERTIFIED))    // if new plan status is CERTIFIED
					{
						// update plan status as CERTIFIED and issuer verification status as VERIFIED
						updatedCount = iPlanRepository.bulkUpdateCertificationStatusAndVerificationStatusQHPPlans(status, lastUpdatedBy, 
								insuranceType, issuerPlanNumber, planMarket, filterStatus, isVerified, planYear, planLevel, issuerId);
					}
					else 
					{
						// update plan status as INCOMPLETE
						updatedCount = iPlanRepository.bulkUpdateCertificationStatusQHPPlans(status, lastUpdatedBy, 
								insuranceType, issuerPlanNumber, planMarket, filterStatus, isVerified, planYear, planLevel, issuerId);
					}
				}
			}
			else if(insuranceType.equals(PlanMgmtConstants.DENTAL))						// if insurance type is DENTAL
			{
				if(!stateCode.equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_CA))		// if state exchange is ID
				{
					if(status.equalsIgnoreCase(PlanMgmtConstants.CERTIFIED))		// if new plan status is CERTIFIED
					{
						// pull list of plans set for certification
						updatedPlanList = iPlanRepository.getBulkUpdatedQDPPlanList(insuranceType, issuerPlanNumber, planMarket, 
							filterStatus, isVerified, planYear, planLevel, issuerId);
						
						// update plan status as CERTIFIED and issuer verification status as VERIFIED
						updatedCount = iPlanRepository.bulkUpdateCertificationStatusAndVerificationStatusQDPPlans(status, lastUpdatedBy, 
								insuranceType, issuerPlanNumber, planMarket, filterStatus, isVerified, planYear, planLevel, issuerId);
						
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug(updatedPlanList.toString());
						}	
					} 
					else 
					{
						// update plan status as INCOMPLETE
						updatedCount = iPlanRepository.bulkUpdateCertificationStatusQDPPlans(status, lastUpdatedBy, 
								insuranceType, issuerPlanNumber, planMarket, filterStatus, isVerified, planYear, planLevel, issuerId);
					}
				}
				else if(stateCode.equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_CA))    // if state exchange is CA
				{
					// pull list of plans set for certification or Incomplete
					updatedPlanList = iPlanRepository.getBulkUpdatedQDPPlanList(insuranceType, issuerPlanNumber, planMarket, 
							filterStatus, isVerified, planYear, planLevel, issuerId);
					
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug(updatedPlanList.toString());
					}	

					if(status.equalsIgnoreCase(PlanMgmtConstants.CERTIFIED))    // if new plan status is CERTIFIED
					{
						// update plan status as CERTIFIED and issuer verification status as VERIFIED
						updatedCount = iPlanRepository.bulkUpdateCertificationStatusAndVerificationStatusQDPPlans(status, lastUpdatedBy, 
								insuranceType, issuerPlanNumber, planMarket, filterStatus, isVerified, planYear, planLevel, issuerId);
					}
					else 
					{
						// update plan status as INCOMPLETE
						updatedCount = iPlanRepository.bulkUpdateCertificationStatusQDPPlans(status, lastUpdatedBy, 
								insuranceType, issuerPlanNumber, planMarket, filterStatus, isVerified, planYear, planLevel, issuerId);
					}
				}
			}
				
			// if unable to DB then nullify updatedPlanList to prevent send email notification / call IND 04
			if(updatedCount == 0){
				updatedPlanList = null;
			}

			if(LOGGER.isDebugEnabled()){
				LOGGER.debug( "Bulk updating plan certification information complete, total updated records : " + updatedCount);
			}
			
			return updatedPlanList;
		} catch (Exception e) {
			LOGGER.error("Error in bulk updating plan certification information", e);
			return updatedPlanList;
		}
	}
	
	@Override
	public boolean bulkUpdateEnrollmentAvbl(String enrollmentavail, Date enrollmentAvailEffdate, int lastUpdatedBy, String insuranceType, String issuerPlanNumber, 
    		String planLevel, String planMarket, String filterStatus, String isVerified, int planYear, int issuerId){
		try {
			LOGGER.debug("Bulk updating plan enrollment availability..");
			int updatedCount = 0;
			/*LOGGER.debug("Updating plans in bulk for criterias and updating info enrollmentavail, enrollmentAvailEffdate, lastUpdatedBy, " + 
					"insuranceType, state, issuerPlanNumber, planLevel, planMarket, filterStatus, exchangeType, " + 
					"isVerified,planYear, issuerId " + enrollmentavail + " , " + enrollmentAvailEffdate + " , " + lastUpdatedBy + " , " + 
					insuranceType + " , " + issuerPlanNumber + " , " + planLevel + " , " + planMarket + " , " + filterStatus + " , " + 
					isVerified + " , " + planYear + " , " + issuerId);*/
						     			
			SimpleDateFormat dateFormat = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT_MMDDYYYY);			
			Date currentDate = dateFormat.parse(dateFormat.format(new TSDate()));
			
			if(insuranceType.equals(PlanMgmtConstants.HEALTH)){
				if(enrollmentAvailEffdate.equals(currentDate)){
					updatedCount = iPlanRepository.bulkUpdateEnrollmentAvblQHPPlans(enrollmentavail, enrollmentAvailEffdate, lastUpdatedBy, 
							insuranceType, issuerPlanNumber, planMarket, filterStatus, isVerified, planYear, planLevel, issuerId);	
				} else {
					updatedCount = iPlanRepository.bulkUpdateFutureEnrollmentAvblQHPPlans(enrollmentavail, enrollmentAvailEffdate, lastUpdatedBy, 
							insuranceType, issuerPlanNumber, planMarket, filterStatus, isVerified, planYear, planLevel, issuerId);
				}
										
			}
			else {
				if(enrollmentAvailEffdate.equals(currentDate)){
					updatedCount = iPlanRepository.bulkUpdateEnrollmentAvblQDPPlans(enrollmentavail, enrollmentAvailEffdate, lastUpdatedBy, 
						insuranceType, issuerPlanNumber, planMarket, filterStatus, isVerified, planYear, planLevel, issuerId);
				} else {
					updatedCount = iPlanRepository.bulkUpdateFutureEnrollmentAvblQDPPlans(enrollmentavail, enrollmentAvailEffdate, lastUpdatedBy, 
							insuranceType, issuerPlanNumber, planMarket, filterStatus, isVerified, planYear, planLevel, issuerId);
				}
			}
			if(LOGGER.isDebugEnabled()){
			   LOGGER.debug("Bulk updating plan enrollment availability complete, total updated records : " + SecurityUtil.sanitizeForLogging(String.valueOf(updatedCount)));
			}   
			if (updatedCount == 0) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			LOGGER.error("Error in bulk updating plan enrollment availability..", e);
			return false;
		}
	}
	
	public Map<Integer, PlanLightResponseDTO> getPlansInfo(List<Integer> planIdList) {
		Map<Integer, PlanLightResponseDTO> planLightResponseDTOMap = new HashMap<Integer, PlanLightResponseDTO>();
		PlanLightResponseDTO planLightResponseDTO = null; 	
		EntityManager em = null;
		Object[] objArray = null;
		String deductibleCostInd = null;
		String deductibleCostFamily = null;
		
		// if request data elements are null throw error
		if(null == planIdList || planIdList.size() == 0) 
		{
			LOGGER.error("Invalid Input Parameters");
			planLightResponseDTO = new PlanLightResponseDTO();
			planLightResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			planLightResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			planLightResponseDTO.setErrMsg("Invalid request parameters.");
		} else {
			
		try {
				// get plan health info for given plan id
				String buildqueryForHealth = null;
				// if DB type is POSTGRESS
				if(DB_TYPE != null && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)){
					buildqueryForHealth = PlanInfoQueryBuilder.getHealthPlanInfoQueryForPostGres();
				}else{
					buildqueryForHealth = PlanInfoQueryBuilder.getHealthPlanInfoQuery();
				}
				
			try{
				em = emf.createEntityManager();
				Query query = em.createNativeQuery(buildqueryForHealth.toString());
				query.setParameter("param_planIdList", planIdList);
								
				if(LOGGER.isDebugEnabled()){
					LOGGER.info("Health SQL = " + buildqueryForHealth.toString());
				}
				
				List<?> rsList = query.getResultList();
				if(rsList != null) {
					Iterator<?> rsIterator = rsList.iterator();
					String[] benefitNames = null;
					String[] benefitValues = null;
				
					while (rsIterator.hasNext()) {
						planLightResponseDTO = new PlanLightResponseDTO();
						objArray = (Object[]) rsIterator.next();
						Integer planId = null;
						if(objArray[PlanMgmtConstants.ZERO] !=null){
							planId = Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString());
							planLightResponseDTO.setPlanId(planId);
						}
						if(planId != null) {
							if(objArray[PlanMgmtConstants.ONE] !=null){
								planLightResponseDTO.setPlanName(objArray[PlanMgmtConstants.ONE].toString());
							}
							if(objArray[PlanMgmtConstants.TWO] !=null){
								planLightResponseDTO.setNetworkType(objArray[PlanMgmtConstants.TWO].toString());
							}
							if(objArray[PlanMgmtConstants.THREE] !=null){
								planLightResponseDTO.setIssuerName(objArray[PlanMgmtConstants.THREE].toString());
							}	
							if(objArray[PlanMgmtConstants.FOUR] !=null){	
								planLightResponseDTO.setIssuerLogo(appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.FOUR].toString()));
							}
							if(objArray[PlanMgmtConstants.SIX] != null){
								deductibleCostInd = objArray[PlanMgmtConstants.SIX].toString();
							}else{
								deductibleCostInd =	NA;
							}
							planLightResponseDTO.setDeductible(deductibleCostInd);
							if(objArray[PlanMgmtConstants.SEVEN] != null){
								deductibleCostFamily = objArray[PlanMgmtConstants.SEVEN].toString();
							}else{
								deductibleCostFamily = NA;
							}
							planLightResponseDTO.setDeductibleFamily(deductibleCostFamily);
							planLightResponseDTO.setOfficeVisit(NA);
							planLightResponseDTO.setGenericMedications(NA);
							
							if(objArray[PlanMgmtConstants.EIGHT] != null && objArray[PlanMgmtConstants.NINE] != null){
								benefitNames = objArray[PlanMgmtConstants.EIGHT].toString().split(",");
								benefitValues = objArray[PlanMgmtConstants.NINE].toString().split(",");
								if(benefitNames != null && benefitValues != null && benefitValues.length == benefitNames.length) {
									for (int i = 0; i < benefitNames.length; i++) {
										if("PRIMARY_VISIT".equalsIgnoreCase(benefitNames[i])  ){
											planLightResponseDTO.setOfficeVisit(benefitValues[i]);
										}
										
										if("GENERIC".equalsIgnoreCase(benefitNames[i])  ){
											planLightResponseDTO.setGenericMedications(benefitValues[i]);
										}
									}
								}
							}
							planLightResponseDTOMap.put(planId, planLightResponseDTO);
						}
						planLightResponseDTO = null;
					}
				}
				// get dental plan info for given plan id
				String buildqueryForDental = PlanInfoQueryBuilder.getDentalPlanInfoQuery();
				
				Query query2 = em.createNativeQuery(buildqueryForDental.toString());
				query2.setParameter("param_planIdList", planIdList);
								
				if(LOGGER.isDebugEnabled()){
					LOGGER.info("dental SQL = " + buildqueryForDental.toString());
				}
					
				List<?> rsList2 = query2.getResultList();
				if(rsList2 != null) {
					Iterator<?> rsIterator2 = rsList2.iterator();		
				    objArray = null;
					deductibleCostInd = null;
					deductibleCostFamily = null;
	
					while (rsIterator2.hasNext()) {
						planLightResponseDTO = new PlanLightResponseDTO();
						objArray = (Object[]) rsIterator2.next();
						Integer planId = null;
						if(objArray[PlanMgmtConstants.ZERO] !=null){
							planId = Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString());
							planLightResponseDTO.setPlanId(planId);
						}
						if(planId != null) {
							if(objArray[PlanMgmtConstants.ONE] !=null){
								planLightResponseDTO.setPlanName(objArray[PlanMgmtConstants.ONE].toString());
							}
							if(objArray[PlanMgmtConstants.TWO] !=null){
								planLightResponseDTO.setNetworkType(objArray[PlanMgmtConstants.TWO].toString());
							}
							if(objArray[PlanMgmtConstants.THREE] !=null){
								planLightResponseDTO.setIssuerName(objArray[PlanMgmtConstants.THREE].toString());
							}	
							if(objArray[PlanMgmtConstants.FOUR] !=null){	
								planLightResponseDTO.setIssuerLogo(appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.FOUR].toString()));
							}
							if(objArray[PlanMgmtConstants.SIX] != null){
								deductibleCostInd = objArray[PlanMgmtConstants.SIX].toString();
							}else{
								deductibleCostInd =	NA;
							}
							planLightResponseDTO.setDeductible(deductibleCostInd);
							if(objArray[PlanMgmtConstants.SEVEN] != null){
								deductibleCostFamily = objArray[PlanMgmtConstants.SEVEN].toString();
							}else{
								deductibleCostFamily = NA;
							}
							planLightResponseDTO.setDeductibleFamily(deductibleCostFamily);
							
							// dental plans does not have those benefits. so set benefit value as NA 
							planLightResponseDTO.setOfficeVisit(NA);
							planLightResponseDTO.setGenericMedications(NA);
							
							planLightResponseDTOMap.put(planId, planLightResponseDTO);
						}
						planLightResponseDTO = null;
					}
				}
			 }catch(Exception e){
					LOGGER.error("Error in PlanMgmtServiceImpl getPlansInfo >>>>>>>>>>>>>>>>>>>>>>>>>>>" + e.getMessage());
					throw new GIRuntimeException("Error in PlanMgmtServiceImpl getPlansInfo >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			 }finally{
					if (em != null && em.isOpen()) {
						em.clear();
						em.close();
					}
			 }
				
		}catch(Exception ex){
				LOGGER.error("Got an exception to execute getPlansInfo()! Exception ==> "+ ex );
		}
			
	 }
	
		return planLightResponseDTOMap;
	}
	
	public String showSaveButtonForUpdate(String deCertificationStatus, String deCertificationEffDate){
		String showSaveButton = PlanMgmtConstants.TRUE_STRING;
		try{
			if((StringUtils.isNotEmpty(deCertificationStatus) && (Plan.PlanStatus.DECERTIFIED.toString().equalsIgnoreCase(deCertificationStatus)))
					|| StringUtils.isNotEmpty(deCertificationEffDate)){
				showSaveButton = PlanMgmtConstants.FALSE_STRING;
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in showSaveButton: " + ex);
		}
		return showSaveButton;
	}


	
	@Override
	@Transactional(readOnly = true)
	public List<Plan> getPlansData(List<Integer> ids) {
		return iPlanRepository.getPlansList(ids);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Plan> getPlanByIssuerIdandIssuerPlanNumber(int issuerId, String issuerPlanNumber) {
		List<Plan> planObjList =  new ArrayList<Plan>();
		try{
			planObjList = iPlanRepository.getPlanByIssuerIdandHIOSID(issuerId, issuerPlanNumber);
		}catch(Exception ex){
			LOGGER.error("Exception occured in getPlanByIssuerIdandIssuerPlanNumber: " +ex);
		}
		return planObjList;
	}
	
	
	// Method to send email notification to Issuer Representative. This function will invoke on all plan bulk update
	@Override
	public void sendNotificationEmailToIssuerRepresentative(List<String[]> updatedPlanList) throws NotificationTypeNotFound, InvalidUserException, NoticeServiceException {
		if(updatedPlanList != null){	
			AccountUser accountUser = null;
			Integer userId = null;
			String fileName = null;
			Map<String,Object> data = null;
			Issuer issuerObj = null;
			List<IssuerRepresentative> listIssuerRep  = null;
			Object[] temp = null;
			Map<Integer, Integer> filterUpdateIssuserList = new HashMap<Integer, Integer>();
			Integer updateIssuerId = null;
				
			// FILTER UPDATE updateIssuerList LIST TO GET UNIQUE ISSUER ID TO AVOID MULTIPLE EMAILS. REF JIRA HIX-85571  
			for (Object obj : updatedPlanList){
				temp = (Object[])obj;
				updateIssuerId = Integer.parseInt(temp[PlanMgmtConstants.FOURTEEN].toString());
				 if(filterUpdateIssuserList.get(updateIssuerId) != null){
					 filterUpdateIssuserList.put(updateIssuerId, filterUpdateIssuserList.get(updateIssuerId) + 1);
				 }else{
					 filterUpdateIssuserList.put(updateIssuerId, 1);
				 }
			}
				
			/* USE filterUpdateIssuserList LIST TO SEND NOTIFICATION TO ISSUER REP */
			for (Integer issuerId : filterUpdateIssuserList.keySet()) { 
				issuerObj = issuerService.getIssuerById(issuerId);
				listIssuerRep = representativeRepository.findByIssuerId(issuerObj.getId());
				data = new HashMap<String,Object>();
					
					for (int i = 0; i < listIssuerRep.size(); i++) { 
						if (listIssuerRep.get(i).getUserRecord() != null && PlanMgmtConstants.ACTIVE.equalsIgnoreCase(listIssuerRep.get(i).getUserRecord().getStatus())) {
							userId = listIssuerRep.get(i).getUserRecord().getId();
							accountUser = new AccountUser();
							if (null != userId && userId != 0) {
								accountUser = userService.findById(userId);
							}
							if (accountUser != null && !StringUtils.isBlank(accountUser.getEmail())) {
								planCertificationBulkStatusUpdateEmail.setIssuerObj(issuerObj);
								planCertificationBulkStatusUpdateEmail.setUser(accountUser);
								planCertificationBulkStatusUpdateEmail.setIssuerRepObj(listIssuerRep.get(i));
								planCertificationBulkStatusUpdateEmail.setNumberOfPlans(String.valueOf(filterUpdateIssuserList.get(issuerId)));
								fileName = PlanMgmtConstants.PLAN_CERTIFICATION_BULK_UPDATE_NOTIFICATION + TimeShifterUtil.currentTimeMillis() + PlanMgmtConstants.PDF_EXT;
								data = planCertificationBulkStatusUpdateEmail.getSingleData();
								noticeService.createNotice(PlanMgmtConstants.PLAN_CERTIFICATION_BULK_UPDATE_NOTIFICATION,  GhixLanguage.US_EN, data, 
										PlanMgmtConstants.PLAN_ECM_PDF_FILE_PATH, fileName, accountUser, null, GhixNoticeCommunicationMethod.Email);
								
								if(LOGGER.isDebugEnabled()){
									LOGGER.debug("Message stored in inbox successfully");
								}	
							} else {
								if(LOGGER.isDebugEnabled()){
									LOGGER.debug("Issuer Representative email address is blank or null");
								}	
							}
						} else {
							if(LOGGER.isDebugEnabled()){
								LOGGER.debug("No User Found for Issuer Representative : "+  listIssuerRep.get(i).getId());
							}	
						}
				}	
			}
		
		} // END IF LOOP
	}
	
	
	/*
	 * Method to send email notification to Issuer Representative. This function will invoke on page wise bulk update or single plan status update
	 * Changes for HIX-17561
	 */
	@Override
	public void sendNotificationEmailToIssuerRepresentative(Plan plan, String status) throws NotificationTypeNotFound, InvalidUserException, NoticeServiceException {
		Issuer issuerObj = issuerService.getIssuerById(plan.getIssuer().getId());
		List<IssuerRepresentative> listIssuerRep = representativeRepository.findByIssuerId(issuerObj.getId());
		Map<String,Object> data = new HashMap<String,Object>();
		
			for (int i = 0; i < listIssuerRep.size(); i++) {
				if (listIssuerRep.get(i).getUserRecord() != null && PlanMgmtConstants.ACTIVE.equalsIgnoreCase(listIssuerRep.get(i).getUserRecord().getStatus())) {
					Integer userId = listIssuerRep.get(i).getUserRecord().getId();
					AccountUser accountUser = new AccountUser();
					if (null != userId && userId != 0) {
						accountUser = userService.findById(userId);
					}
					if (accountUser != null && !StringUtils.isBlank(accountUser.getEmail())) {
						if(status.equalsIgnoreCase(Plan.PlanStatus.DECERTIFIED.toString())){
							issuerPlanDeCertificationEmail.setPlanObj(plan);
							issuerPlanDeCertificationEmail.setIssuerObj(issuerObj);
							issuerPlanDeCertificationEmail.setUser(accountUser);
							issuerPlanDeCertificationEmail.setIssuerRepObj(listIssuerRep.get(i));
							data = issuerPlanDeCertificationEmail.getSingleData();
							String fileName = PlanMgmtConstants.ISSUER_PLAN_DECERTIFICATION+TimeShifterUtil.currentTimeMillis() + PlanMgmtConstants.PDF_EXT;
							noticeService.createNotice(PlanMgmtConstants.PLAN_DE_CERTIFIED_NOTIFICATION,  GhixLanguage.US_EN, data,  PlanMgmtConstants.PLAN_ECM_PDF_FILE_PATH, fileName, accountUser, null, GhixNoticeCommunicationMethod.Email);
						} else {
							issuerPlanCertificationEmail.setPlanObj(plan);
							issuerPlanCertificationEmail.setIssuerObj(issuerObj);
							issuerPlanCertificationEmail.setUser(accountUser);
							issuerPlanCertificationEmail.setIssuerRepObj(listIssuerRep.get(i));
							String fileName = PlanMgmtConstants.ISSUER_PLAN_CERTIFICATION + TimeShifterUtil.currentTimeMillis() + PlanMgmtConstants.PDF_EXT;
							data = issuerPlanCertificationEmail.getSingleData();
							noticeService.createNotice(PlanMgmtConstants.PLAN_CERTIFIED_NOTIFICATION,  GhixLanguage.US_EN, data, PlanMgmtConstants.PLAN_ECM_PDF_FILE_PATH, fileName, accountUser, null, GhixNoticeCommunicationMethod.Email);
						}
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug("Message stored in inbox successfully");
						}	
	
					} else {
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug("Issuer Representative email address is blank or null");
						}	
					}
				} else {
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("No User Found for Issuer Representative : "+ listIssuerRep.get(i).getId());
					}	
				}
			}
	}
	
	
	@Override
	public void downLoadFormularyDrug(FormularyDrugListResponseDTO formularyDrugListResponseDTO,
			HttpServletResponse response) throws IOException {
		if(null != formularyDrugListResponseDTO) {
			List<String[]> drugList = FormularyDrugMapper.prepareFormularyDrugListToExcel(formularyDrugListResponseDTO);
			List<String[]> formularyPlanList = FormularyDrugMapper.prepareFormularyPlanListToExcel(formularyDrugListResponseDTO);
			String fileName = formularyDrugListResponseDTO.getIssuerHIOSId() + 
					"-" + formularyDrugListResponseDTO.getFormularyId() + 
					"-" + formularyDrugListResponseDTO.getApplicableYear() + 
					PlanMgmtConstants.FORMULARY_DRUG_FILE_NAME_SUFFIX;
			HSSFWorkbook workBook = ExcelGenerator.createFormularyDrugExcelSheet(drugList, formularyPlanList);
			FileOutputStream outFile = new FileOutputStream(uploadPath + File.separator + fileName);
			workBook.write(outFile);
			outFile.close();
			response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
			response.setContentType("application/octet-stream");
			File file = new File(uploadPath + File.separator + fileName);
			FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
			if(file.exists()){
				file.delete();
			}
		} else {
			LOGGER.error("downLoadFormularyDrug: Failed to get Formulary Drug List data..!!!");
		}
	}
	
	
	@Override
	public FormularyDrugListResponseDTO getFormularyDrugDetails(String applicbleYear, String issuerHiosId, String formularyId) {
		FormularyDrugListResponseDTO formularyDrugListDTO = new FormularyDrugListResponseDTO();
		int applicableYear = StringUtils.isNotBlank(applicbleYear) ? Integer.parseInt(applicbleYear) : 0;
		if(applicableYear > 0 && null != issuerHiosId && null != formularyId) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Getting Formulary Drug data for Year : " + applicableYear + " HIOS : "
						+ issuerHiosId + " FormularyId: " + formularyId);
			}
			formularyDrugListDTO.setApplicableYear(String.valueOf(applicableYear));
			formularyDrugListDTO.setFormularyId(formularyId);
			formularyDrugListDTO.setIssuerHIOSId(issuerHiosId);
			EntityManager entityManager = null;
			try {
				String buildquery = buildFormularyDrugListQuery();
				entityManager = emf.createEntityManager();
				Query query = entityManager.createQuery(buildquery.toString());
				query.setParameter("param_formulary_id", formularyId);
				query.setParameter("param_applicable_year", applicableYear);
				query.setParameter("param_issuer_id", issuerHiosId);
				List<?> rsList = query.getResultList();
				Iterator<?> rsIterator = rsList.iterator();
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Formulary Drug Data is fetched from db result size is " + rsList.size());
				}
				while (rsIterator.hasNext()) {
					Object[] objArray = (Object[]) rsIterator.next();
					FormularyDrugItemDTO formularyDrugItem = new FormularyDrugItemDTO();
					formularyDrugItem.setFormularyId(String.valueOf(objArray[PlanMgmtConstants.ZERO]));
					formularyDrugItem.setDrugTierLevel((Integer) objArray[PlanMgmtConstants.ONE]);
					formularyDrugItem.setDrugTierType1(String.valueOf(objArray[PlanMgmtConstants.TWO]));
					formularyDrugItem.setDrugTierType2(objArray[PlanMgmtConstants.THREE] == null ? "" : String.valueOf(objArray[PlanMgmtConstants.THREE]));
					formularyDrugItem.setRxcuiCode(String.valueOf( objArray[PlanMgmtConstants.FOUR]));
					formularyDrugItem.setAuthRequired(String.valueOf(objArray[PlanMgmtConstants.FIVE]));
					formularyDrugItem.setStepTherapyRequired(String.valueOf(objArray[PlanMgmtConstants.SIX]));
					formularyDrugListDTO.addFormularyDrugItemToList(formularyDrugItem);
				}
				buildquery = buildFormularyPlanListQuery();
				Query plansQuery = entityManager.createQuery(buildquery.toString());
				plansQuery.setParameter("param_formulary_id", formularyId);
				plansQuery.setParameter("param_applicable_year", applicableYear);
				plansQuery.setParameter("param_issuer_id", issuerHiosId);
				List<?> rsPlanList = plansQuery.getResultList();
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Formulary Plans Data is fetched from db result size is " + rsList.size());
				}
				rsIterator = rsPlanList.iterator();
				while (rsIterator.hasNext()) {
					Object[] objArray = (Object[]) rsIterator.next();
					if(null == formularyDrugListDTO.getIssuerName()) {
						formularyDrugListDTO.setIssuerName((String) objArray[PlanMgmtConstants.ZERO]);
					}
					FormularyPlanDTO formularyPlan = new FormularyPlanDTO();
					formularyPlan.setFormularyId(String.valueOf(objArray[PlanMgmtConstants.THREE]));
					formularyPlan.setIssuerPlanNumber(String.valueOf(objArray[PlanMgmtConstants.TWO]));
					formularyPlan.setPlanName(String.valueOf(objArray[PlanMgmtConstants.ONE]));
					formularyDrugListDTO.addFormularyPlanToList(formularyPlan);
				}
			} catch (Exception e) {
				LOGGER.error("Failed to create Formulary Drug List data. Exception: ", e);
			}
			finally
			{
				if(entityManager != null && entityManager.isOpen())
				{
					try {
						entityManager.close();
					}
					catch(IllegalStateException ex)
					{
						LOGGER.error("EntityManager is managed by application server", ex);
					}
				}
			}
		} else {
			LOGGER.error("Failed to get Formulary Drug data as params are invalid. Year : " + applicableYear + " HIOS : "
					+ issuerHiosId + " FormularyId: " + formularyId);
		}
		return formularyDrugListDTO;	
	}

	private String buildFormularyPlanListQuery() {
		StringBuilder buildquery = new StringBuilder(); 
		buildquery.append("SELECT i.name, p.name, p.issuerPlanNumber, f.formularyId "
				+ " FROM Plan p, Formulary f, Issuer i"
				+ " where i.id = p.issuer.id ");

		if (PlanMgmtConstants.POSTGRES.equalsIgnoreCase(DB_TYPE)) {
			buildquery.append(" and p.formularlyId = CAST (f.id AS string)");
		}
		else {
			buildquery.append(" and p.formularlyId = f.id");
		}

		buildquery.append(" and f.formularyId = :param_formulary_id"
				+ " and f.applicableYear = :param_applicable_year"
				+ " and i.hiosIssuerId = :param_issuer_id "
				+ " ORDER BY p.id");
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("build query : " + buildquery.toString());
		}
		return buildquery.toString();
	}

	private String buildFormularyDrugListQuery() {
		StringBuilder buildquery = new StringBuilder(); 
		buildquery.append("SELECT f.formularyId, pdt.drugTierLevel, pdt.drugTierType1, pdt.drugTierType2, pd.rxcui, pd.authRequired, pd.stepTherapyRequired "
				+ " FROM Drug pd, DrugTier pdt, Formulary f, Issuer i"
				+ " where i.id = f.issuer.id "
				+ " and f.drugList.id = pdt.drugList.id"
				+ " and pdt.id=pd.drugTier.id"
				+ " and f.formularyId = :param_formulary_id"
				+ " and f.applicableYear = :param_applicable_year"
				+ " and i.hiosIssuerId = :param_issuer_id "
				+ " ORDER BY pd.id");
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("build query : " + buildquery.toString());
		}
		return buildquery.toString();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext=applicationContext;
		
	}

	
}
