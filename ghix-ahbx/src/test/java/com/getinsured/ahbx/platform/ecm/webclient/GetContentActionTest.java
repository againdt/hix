package com.getinsured.ahbx.platform.ecm.webclient;

import static org.junit.Assert.assertNotNull;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.getinsured.hix.dto.platform.ecm.Document;
import com.getinsured.hix.dto.platform.ecm.Folder;
import com.getinsured.hix.dto.platform.ecm.Image;
import com.getinsured.hix.model.PlatformRequest;
import com.getinsured.hix.model.PlatformRequest.ECMRequest;
import com.getinsured.hix.model.PlatformResponse;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/applicationContext.xml")
public class GetContentActionTest {

	private static final Logger LOGGER = Logger.getLogger(GetContentActionTest.class);

	@Autowired private GetContentAction getContentAction;

	@Test
	public void testGetFileContent() throws ContentManagementServiceException {

		/**
		 * FIXME populate HashMap
		 */
		PlatformRequest platformRequest = new PlatformRequest();

		ECMRequest ecmRequest = platformRequest.new ECMRequest();
		ecmRequest.setDocId("1366269887560");
		platformRequest.setEcmRequest(ecmRequest);

		String ahbxResponse = getContentAction.getFileContent(platformRequest);

		/**
		 * UnMarshal and from PlatformResponse object
		 */
		XStream xstream = GhixUtils.getXStreamStaxObject();
		PlatformResponse platformResponse = (PlatformResponse) xstream.fromXML(ahbxResponse);

		/**
		 * If AHBX OEDQ call failed, throw Exception to the caller
		 */
		if ("FAILURE".equals(platformResponse.getStatus())){
			throw new ContentManagementServiceException("AHBX UCM call failed - " +  platformResponse.getErrMsg());
		}


		assertNotNull(platformResponse.getGiContent());

		if (platformResponse.getGiContent() instanceof Document){
			Document documentContent = (Document) platformResponse.getGiContent();
			assertNotNull(documentContent.getDocumentStream().getContent());
			LOGGER.info("documentContent found - " + new String(documentContent.getDocumentStream().getContent()));
		} else if (platformResponse.getGiContent() instanceof Image){
			Image imageContent = (Image) platformResponse.getGiContent();
			assertNotNull(imageContent.getImageStream());
			LOGGER.info("imageContent found - " + new String(imageContent.getImageStream()));
		} else if (platformResponse.getGiContent() instanceof Folder){
			Folder folderContent = (Folder) platformResponse.getGiContent();
			assertNotNull(folderContent.getParentId());
			LOGGER.info("folderContent found - " +new String(folderContent.getParentId()));
		}
	}

}
