<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/qdp.css" />" />

<div class="gutter10">
<div class="row-fluid">
    		<ul class="page-breadcrumb">
                <li><a href="#">&lt;<spring:message code="label.back"/></a></li>
                <li><a href="#"><spring:message code="label.plans"/></a></li>
                 <li><a href="#"><spring:message code='label.addNewQDP'/></a></li>
                <li><a href="#"><spring:message code="label.linkDentalPlanBenefits"/></a></li>
            </ul><!--page-breadcrumb ends-->
			<h1><a name="skip"></a><spring:message code='label.addNewQDP'/></h1>
</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
				<div class="header">
					<!--  beginning of side bar -->
					<h4 class="margin0"><spring:message code='label.steps'/></h4>
				</div>
					<ol class="nav nav-list">
						<li class="done"><a href="#"><spring:message code='label.linkDentalPlanDetails'/></a></li>
						<li class="active"><a href="#"><spring:message code='label.linkDentalPlanBenefits'/></a></li>
						<li class="link"><a href="#"><spring:message code='label.linkDentalPlanRates'/></a></li>
						<li class="link"><a href="#"><spring:message code='label.linkProviderNetwork'/></a></li>
						<li class="link"><a href="#"><spring:message code='label.reviewandconfirm'/></a></li>
					</ol>
					<!-- end of side bar -->
				
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
				<div class="header">
					<h4 class="margin0">2.<spring:message code='label.linkDentalPlanBenefits'/></h4>
				</div>
					<form class="form-horizontal" id="frmUploadBenefits" name="frmUploadBenefits" action="<c:url value="/admin/planmgmt/submitqdpbenefits" />" method="POST">
                    	<df:csrfToken/>
                    	<table class="table table-border-none marginL5">
							<tr>
								<td class="span3 txt-right"><spring:message code='label.issuer'/></td>
								<td class="span9"><strong>${issuerName}</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message code='label.planName'/></td>
								<td><strong>${planName}</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message code='label.planNumber'/></td>
								<td><strong>${planNumber}</strong></td>
							</tr>
						</table>
						<input type="hidden" id="benefitFile" name="benefitFile" /> 
						<input type="hidden" id="id" name="id" value="${planId}" /> 
						<input type="hidden" id="isEdit" name="isEdit" value="${isEdit}" />
						<input type="hidden" id="brochureFile" name="brochureFile" />
					</form>

					<form class="form-horizontal form-bottom" id="frmBenefits" name="frmBenefits" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
						<df:csrfToken/>
						<div class="box-gutter10">
							<div class="control-group">
								<label for="planBenefits" class="control-label"><spring:message code='label.titlePlanBenefits' /></label>
								<div class="controls">
									<span>
										<input type="file" id="benefits" name="benefits" class="input-file"> 
										&nbsp; <button type="submit" class="btn" onclick="validCsv('#benefits','#frmBenefits','#benefitFile_error')"><spring:message code='label.upload'/></button>
										&nbsp; <a href=<c:url value="/resources/sampleTemplates/qdp_benefits.csv" />><spring:message code='label.downloadTemplate'/></a> 
										<input type="hidden" name="fileType" value="benefits" />
									</span>
									<div id="benefitFile_error" class="">${benefitsFileError}</div>
									<div id="benefitsFileName" class="help"></div>

								</div>
								<!-- end of controls-->
							</div>
							<!-- end of control-group -->
						</div>
					</form>
					
					<form class="form-horizontal form-bottom" id="frmBrochure" name="frmBrochure" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
                    	<df:csrfToken/>
                    	<div class="control-group">
                            <label for="issuer_name" class="control-label"><spring:message code='label.planBrochure'/></label>
                            <div class="controls">
                                <input type="file" id="brochure" name="brochure" class="input-file"> 
                                &nbsp;&nbsp;<button type="submit" class="btn"><spring:message code='label.upload'/></button>
                                <input type="hidden" name="fileType" value="brochure" />
                                <div id="brochure_error" class="">${brochureFileError}</div>
                                <div id="brochureFileName" class="help"></div>
                            </div>
                        </div>
						
                        <div class="form-actions">
                            <button type="submit" title="<spring:message  code="label.saveAndContinue"/>" name="submitBtn" id="submitBtn" class="btn btn-primary paddingTB6" onclick="formSubmit()"><spring:message code='label.saveAndContinue'/></button>
                        </div>
                        
					</form>
			</div><!-- end of .span9 -->

		</div><!--  end of .row-fluid -->
	</div>

<script type="text/javascript">
function validCsv(fileElementId, formElementId, errorElementId) {
	var ext = $(fileElementId).val().split('.').pop().toLowerCase();
	var allow = new Array('csv');
	if(jQuery.inArray(ext, allow) == -1) {
	    $(errorElementId).html("<label class='error'><span> <em class='excl'>!</em><spring:message code='label.fileTypeError'/></span></label>");
	   return false;
	}else{
		$(formElementId).submit();
	  } 
}

function  formSubmit() {
	$("#benefitFile_error").html("");
	$("#brochure_error").html("");
	$("#frmUploadBenefits").submit();
}

$(document).ready(function() {
	$.ajaxSetup({ cache: false }); // prevent Ajax caching in JQuery
	var BenefitsUpload = {
		//target:        '#certiName',   // target element(s) to be updated with server response 
		beforeSubmit : submitBenefitsRequest, // pre-submit callback 
		success : submitBenefitsResponse
	// post-submit callback 
	};
	$('#frmBenefits').ajaxForm(BenefitsUpload);
	
	var brocureUpload = {
		beforeSubmit : submitBrocureRequest, // pre-submit callback 
		success : submitBrocureResponse
	};
	$('#frmBrochure').ajaxForm(brocureUpload);
	isFileReq();
});

function isFileReq() {
	if("false" == '${isEdit}') {
		$("#benefitFile").rules("add", "required");
	}	
}
function submitBenefitsResponse(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	if(val[0] != 'Error') {	
		$("#benefitsFileName").text(val[1]);
		$("#benefitFile").val(val[2]);
	} else {
		$("#benefitFile_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message code='label.fileSizeError'/></span></label>");
	}
		
}

function submitBenefitsRequest(formData, jqForm, options) {
	var fileName = $('#benefits').val();
	if (fileName.length > 0) {
	    return true;	
	} 
	return false;
}

function submitBrocureResponse(responseText, statusText, xhr, $form) {
	var val = responseText.split(",");
	if (val[0] != 'Error') {
		$("#brochureFileName").text(val[1]);
		$("#brochureFile").val(val[2]);
	} else {
		$("#brochure_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message code='label.fileSizeError'/></span></label>");
	}

}

function submitBrocureRequest(formData, jqForm, options) {
	var fileName = $('#brochure').val();
	if (fileName.length > 0) {
		return true;
	}
	return false;
}


	var validator = $("#frmUploadBenefits")
			.validate(
					{
						ignore : "",
						rules : {
							benefitFile : false
						},
						messages : {
							benefitFile : {
								required : "<span> <em class='excl'>!</em><spring:message code='error.benefitFileRequired'/></span>"
							}
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error").attr('class', 'error help-block');
						}
					});
	jQuery("#frmBenefits").validate({});

	$('.complete').each(
			function() {
				var completeStep = $(this).html();
				var replaceExpr = /[0-9]*\./gi;
				$(this).html(completeStep.replace(replaceExpr, '<i class="icon icon-ok"></i>'));
	});
</script>
