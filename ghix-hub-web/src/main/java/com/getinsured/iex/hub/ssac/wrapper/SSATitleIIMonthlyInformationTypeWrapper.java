package com.getinsured.iex.hub.ssac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ssac.extension.SSATitleIIMonthlyInformationType;

/**
 * Wrapper class for SSATitleIIMonthlyInformationType
 * 
 * @author Nikhil Talreja
 * @since 10-Mar-2014
 */
public class SSATitleIIMonthlyInformationTypeWrapper implements JSONAware {
	
	private String incomeMonthYear;
	private String benefitCreditedAmount;
	private String overpaymentDeductionAmount;
    private String priorMonthAccrualAmount;
    private String returnedCheckAmount;
    private boolean paymentInSuspenseIndicator;
    private String netMonthlyBenefitCreditedAmount;
    private String monthlyIncomeAmount;
	
	public SSATitleIIMonthlyInformationTypeWrapper(SSATitleIIMonthlyInformationType ssaTitleIIMonthlyInformationType){
		
		if(ssaTitleIIMonthlyInformationType == null){
			return;
		}
		
		this.incomeMonthYear = ssaTitleIIMonthlyInformationType.getIncomeMonthYear();
		
		if(ssaTitleIIMonthlyInformationType.getBenefitCreditedAmount() != null){
			this.benefitCreditedAmount = ssaTitleIIMonthlyInformationType.getBenefitCreditedAmount().toString();
		}
		
		if(ssaTitleIIMonthlyInformationType.getOverpaymentDeductionAmount() != null){
			this.overpaymentDeductionAmount = ssaTitleIIMonthlyInformationType.getOverpaymentDeductionAmount().toString();
		}
		
		if(ssaTitleIIMonthlyInformationType.getPriorMonthAccrualAmount() != null){
			this.priorMonthAccrualAmount = ssaTitleIIMonthlyInformationType.getPriorMonthAccrualAmount().toString();
		}
		
		if(ssaTitleIIMonthlyInformationType.getReturnedCheckAmount() != null){
			this.returnedCheckAmount = ssaTitleIIMonthlyInformationType.getReturnedCheckAmount().toString();
		}
		
		if(ssaTitleIIMonthlyInformationType.getPaymentInSuspenseIndicator() != null){
			this.paymentInSuspenseIndicator = ssaTitleIIMonthlyInformationType.getPaymentInSuspenseIndicator().isValue();
		}
		
		if(ssaTitleIIMonthlyInformationType.getNetMonthlyBenefitCreditedAmount() != null){
			this.netMonthlyBenefitCreditedAmount = ssaTitleIIMonthlyInformationType.getNetMonthlyBenefitCreditedAmount().toString();
		}
		
		if(ssaTitleIIMonthlyInformationType.getMonthlyIncomeAmount()!= null){
			this.monthlyIncomeAmount = ssaTitleIIMonthlyInformationType.getMonthlyIncomeAmount().toString();
		}
	}
	
	public String getIncomeMonthYear() {
		return incomeMonthYear;
	}

	public String getBenefitCreditedAmount() {
		return benefitCreditedAmount;
	}

	public String getOverpaymentDeductionAmount() {
		return overpaymentDeductionAmount;
	}

	public String getPriorMonthAccrualAmount() {
		return priorMonthAccrualAmount;
	}

	public String getReturnedCheckAmount() {
		return returnedCheckAmount;
	}

	public boolean getPaymentInSuspenseIndicator() {
		return paymentInSuspenseIndicator;
	}

	public String getNetMonthlyBenefitCreditedAmount() {
		return netMonthlyBenefitCreditedAmount;
	}

	public String getMonthlyIncomeAmount() {
		return monthlyIncomeAmount;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("IncomeMonthYear", this.incomeMonthYear);
		obj.put("BenefitCreditedAmount", this.benefitCreditedAmount);
		obj.put("OverpaymentDeductionAmount", this.overpaymentDeductionAmount);
		obj.put("PriorMonthAccrualAmount", this.priorMonthAccrualAmount);
		obj.put("ReturnedCheckAmount", this.returnedCheckAmount);
		obj.put("PaymentInSuspenseIndicator", this.paymentInSuspenseIndicator);
		obj.put("NetMonthlyBenefitCreditedAmount", this.netMonthlyBenefitCreditedAmount);
		obj.put("MonthlyIncomeAmount", this.monthlyIncomeAmount);
		return obj.toJSONString();
	}
	
}
