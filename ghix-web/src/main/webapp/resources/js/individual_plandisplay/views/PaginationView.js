(function (views) {
	views.Pagination = Backbone.View.extend({
		
		events : {
			'click a.first'        : 'gotoFirst',
			'click a.prev'         : 'gotoPrev',
			'click a.next'         : 'gotoNext',
			'click a.last'         : 'gotoLast',
			'click a.page'         : 'gotoPage'
		},
		
		el : '#pagination',
		initialize : function () {
			_.bindAll (this, 'render','gotoNext','gotoPrev','gotoNextItem','gotoPrevItem');
			var self = this;
			self.tmpl = _.template($('#tmpPagination').html());
			self.collection.bind('reset', this.render);

		},
		render : function () {
            var self = this;
            var info = self.collection.info();

            $('#prevItem').unbind('click');
            $('#nextItem').unbind('click');
            if(info.page != 1){
            	$('#prevItem').bind('click',this.gotoPrev);
            }
            if(info.page != info.lastPage){
            	$('#nextItem').bind('click',this.gotoNext);
            }
			
			var html = this.tmpl(info);
			$(this.el).html(html);
		},
		
		gotoFirst : function (e) {
			e.preventDefault();
			var self = this;
			self.collection.goTo(1);
			self.collection.preserveOtherOptions();
		},
		
		gotoPrev : function (e) {
			e.preventDefault();
			var self = this;
			self.collection.previousPage();
			self.collection.preserveOtherOptions();
			
		},
		
		gotoNext : function (e) {
			e.preventDefault();
			var self = this;
			self.collection.nextPage();
			self.collection.preserveOtherOptions();
		},
		
		gotoLast : function (e) {
			e.preventDefault();
			var self = this;
			self.collection.goTo(self.collection.information.lastPage);
			self.collection.preserveOtherOptions();
		},
		
		gotoPage : function (e) {
			e.preventDefault();
			var self = this;
			var page = $(e.target).text();
			self.collection.goTo(page.split(" ")[2]);
			self.collection.preserveOtherOptions();
		},
		
		gotoNextItem : function (e) {
			e.preventDefault();
			var self = this;
			var info = self.collection.info();
			var nextenabled = self.collection.stop == info.totalRecords ? false : true;
			if(nextenabled){
				self.collection.nextItem=1;
				self.collection.pager();
			}
			self.collection.preserveOtherOptions();
		},
		
		gotoPrevItem : function (e) {
			e.preventDefault();
			var self = this;
			var prevenabled = self.collection.start == 0 ? false : true;
			if(prevenabled){
				self.collection.prevItem=1;
				self.collection.pager();
			}
			self.collection.preserveOtherOptions();
		}
		
	});
})(App.views);