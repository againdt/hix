package com.getinsured.ahbx.delegation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.ws.http.HTTPException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.ahbx.delegation.delegatecode.DelegateAccessCodeRequestDTO;
import com.getinsured.ahbx.delegation.delegatecode.DelegateAccessCodeResponseDTO;
import com.getinsured.ahbx.delegation.delegatecode.RetrieveDelegationCodeResponse;
import com.getinsured.ahbx.util.AHBXUtils;
import com.getinsured.ahbx.util.GhixAhbxConstants;
import com.getinsured.hix.model.DelegationRequest;
import com.getinsured.hix.model.DelegationResponse;
import com.getinsured.hix.platform.webclient.SoapWebClient;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;


/**
 * The controller class that receives Rest requests from Issuer/Broker and Assister modules on their
 * registration. It then sends this information to AHBX by consuming their web services.
 */
@Controller
@RequestMapping(value = "/delegation")
public class DelegationWsController {

	@Autowired
	private WebServiceTemplate delegationWebServiceTemplate;

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private SoapWebClient<JAXBElement<RetrieveDelegationCodeResponse>> soapWebClient;

	private static final Logger LOGGER = Logger.getLogger(DelegationWsController.class);

	/**
	 * @return
	 * @throws HTTPException
	 *             test method written to test whether the call reaching to the controller.
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {
		LOGGER.info("Welcome to GHIX-AHBX Delegation Service module");

		return "Welcome to GHIX-AHBX SOAP Delegation Service module";
	}

	/**
	 * @return Method written to test the main AHBX delegation call
	 */
	@RequestMapping(value = "/testdelegationcode", method = RequestMethod.GET)
	@ResponseBody
	public String callAhbxIssuerResponse() {
		/* Map<String, Object> requestData = new HashMap<String, Object>(); */
		DelegationRequest delegationRequest = new DelegationRequest();
		delegationRequest.setRecordType("Issuer");
		delegationRequest.setRecordId(1);

		String verifyResponse = restTemplate.postForObject(
				"http://localhost:8080/ghix-ahbx/delegation/validation/delegationcode", delegationRequest, String.class);
		XStream xStream = GhixUtils.getXStreamStaxObject();
		DelegationResponse delegationResponse = (DelegationResponse) xStream.fromXML(verifyResponse);

		if (delegationResponse != null
				&& delegationResponse.getStatus().equalsIgnoreCase(GhixAhbxConstants.RESPONSE_SUCCESS)) {
			LOGGER.info("Received Response :" + delegationResponse.toString());
		}

		return delegationResponse.getErrMsg() != null ? delegationResponse.getErrMsg() : delegationResponse.getStatus();
	}

	/**
	 * This method is invoked to receive delegation code for AGENT/ISSUER and ASSISTER from AHBX
	 * 
	 * @param delegationRequest
	 *            dto containing request parameters
	 * @return xml response received from AHBX web service
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/validation/delegationcode", method = RequestMethod.POST)
	@ResponseBody
	public String getAHBXDelegationCode(@RequestBody DelegationRequest delegationRequest) {

		DelegationResponse delegationResponse = new DelegationResponse();
		RetrieveDelegationCodeResponse retrieveDelegationCodeResponse = null;

		if (delegationRequest != null) {
/*			LOGGER.info("IND54: Successfully received AssisterRequestDTO from ghix-web over rest call: "
					+ delegationRequest.toString());
*/
			try {

				delegationResponse.startResponse();

				// Creating and populating response object for AHBX call
				DelegateAccessCodeRequestDTO delegateAccessCodeRequestDTO = populateRequestDTO(delegationRequest);
				com.getinsured.ahbx.delegation.delegatecode.ObjectFactory objectFactory = new com.getinsured.ahbx.delegation.delegatecode.ObjectFactory();
				com.getinsured.ahbx.delegation.delegatecode.RetrieveDelegationCode retrieveDelegationCode = objectFactory
						.createRetrieveDelegationCode();

				List<DelegateAccessCodeRequestDTO> delegateCodeRequestList = new ArrayList<DelegateAccessCodeRequestDTO>();
				delegateCodeRequestList.add(delegateAccessCodeRequestDTO);
				retrieveDelegationCode.getArg0().addAll(delegateCodeRequestList);

				JAXBElement<com.getinsured.ahbx.delegation.delegatecode.RetrieveDelegationCode> requestElement = objectFactory
						.createRetrieveDelegationCode(retrieveDelegationCode);

				//LOGGER.debug("IND54: Created RetrieveDelegationCode request element for WS call : " + requestElement);

				// Invoking AHBX Web Service IND54
				delegationWebServiceTemplate.setDefaultUri(GhixAhbxConstants.WSDL_URL_IND54);			

				// webServiceTemplate
				 //.setDefaultUri("http://localhost:8088/mockDelegateAccessCodeEndPointImplServiceSoapBinding?wsdl");

				LOGGER.debug("IND54: Set following URI for WS client: " + GhixAhbxConstants.WSDL_URL_IND54);
				delegationResponse.setAppID(GhixAhbxConstants.WSDL_URL_IND54);

/*				LOGGER.info("IND54: Invoking Broker Details service. Sending following data to AHBX: "
						+ retrieveDelegationCode.toString());
*/
				// Retrieving delegation code from ahbx response
				//JAXBElement<RetrieveDelegationCodeResponse> ahbxResponse = (JAXBElement<RetrieveDelegationCodeResponse>) webServiceTemplate
						//.marshalSendAndReceive(requestElement);

				JAXBElement<RetrieveDelegationCodeResponse> ahbxResponse = soapWebClient.send(requestElement, GhixAhbxConstants.WSDL_URL_IND54, "IND54", delegationWebServiceTemplate);
				retrieveDelegationCodeResponse = ahbxResponse.getValue();

/*				LOGGER.debug("IND54: Retrieved RetrieveDelegationCodeResponse object from response: "
						+ retrieveDelegationCodeResponse.toString());
*/
				// Processing AHBX response
				if (retrieveDelegationCodeResponse != null && retrieveDelegationCodeResponse.getReturn() != null
						&& !retrieveDelegationCodeResponse.getReturn().isEmpty()) {

					// Assuming that only one record will be sent
					DelegateAccessCodeResponseDTO delegateAccessCodeResponseDTO = retrieveDelegationCodeResponse
							.getReturn().get(0);

					// Convert DelegateAccessCodeResponseDTO into DelegationResponse
					delegationResponse.setDelegationCode(delegateAccessCodeResponseDTO.getDelegationCode());
					delegationResponse.setRecordId(delegateAccessCodeResponseDTO.getRecordId());
					delegationResponse
							.setRecordType(delegateAccessCodeResponseDTO.getRecordType() != null ? delegateAccessCodeResponseDTO
									.getRecordType() : null);
					delegationResponse.setResponseCode(delegateAccessCodeResponseDTO.getResponseCode());
					delegationResponse.setResponseDescription(delegateAccessCodeResponseDTO.getResponseDescription());

/*					LOGGER.debug("IND54: Assister Delegate service call successful. Populated DelegationResponse to be sent to ghix-web: "
							+ delegationResponse.toString());
*/					
					delegationResponse.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);
					delegationResponse.setErrMsg("");
				}
				else {
					LOGGER.error("Received null response from AHBX");
					delegationResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
					delegationResponse.setErrMsg("No response or response is null");
				}
			}
			catch (WebServiceIOException webServiceIOException) {
				LOGGER.error("IND54: Exception occurred while invoking service " + webServiceIOException.getMessage());
				delegationResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				delegationResponse.setErrMsg(webServiceIOException.getMessage());
			}
			catch (Exception exception) {
				LOGGER.error("IND54: Exception occurred while invoking service" + exception.getMessage());
				delegationResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				delegationResponse.setErrMsg(exception.getMessage());
			}
		}
		else {
			LOGGER.error("IND54: Assister RequestDTO is null. Unable to send Assister Details to AHBX5");

			delegationResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			delegationResponse.setErrMsg(GhixAhbxConstants.INVALID_INPUT_MSG);
			delegationResponse.setErrCode(GhixAhbxConstants.INVALID_INPUT_CODE);
		}

		LOGGER.info("IND54: Marshalling and returning AssisterResponseDTO to ghix-web");

		delegationResponse.endResponse();

		// Marshaling the response before it is sent out to ghix-web
		return AHBXUtils.marshal(delegationResponse);
	}

	private DelegateAccessCodeRequestDTO populateRequestDTO(DelegationRequest delegationRequest) {
		DelegateAccessCodeRequestDTO delegateAccessCodeRequestDTO = new DelegateAccessCodeRequestDTO();

		delegateAccessCodeRequestDTO.setAddressLine1(delegationRequest.getAddressLineOne());
		delegateAccessCodeRequestDTO.setAddressLine2(delegationRequest.getAddressLineTwo());
		
		if(GhixAhbxConstants.AGENT.equalsIgnoreCase(delegationRequest.getRecordType()) && delegationRequest.getBusinessLegalName().length() > 50 )
		{
			delegateAccessCodeRequestDTO.setBusinessLegalName(delegationRequest.getBusinessLegalName().substring(0, 50));
		}else{
			delegateAccessCodeRequestDTO.setBusinessLegalName(delegationRequest.getBusinessLegalName());
		}
		
		if(delegationRequest.getCertificationId() != 0)
		{
			delegateAccessCodeRequestDTO.setCertificationId(delegationRequest.getCertificationId());
		}
		if(GhixAhbxConstants.APPROVEDADMINSTAFFL1.equalsIgnoreCase(delegationRequest.getRecordType()) || 
				(GhixAhbxConstants.APPROVEDADMINSTAFFL2.equalsIgnoreCase(delegationRequest.getRecordType()))){
			delegateAccessCodeRequestDTO.setCertificationStatusCd("AP");
		} else {
			delegateAccessCodeRequestDTO.setCertificationStatusCd("CE");
		}
		delegateAccessCodeRequestDTO.setCity(delegationRequest.getCity());
		delegateAccessCodeRequestDTO.setContactFirstName(delegationRequest.getContactFirstName());
		delegateAccessCodeRequestDTO.setContactLastName(delegationRequest.getContactLastName());
		delegateAccessCodeRequestDTO.setEmailId(delegationRequest.getEmailId());
		delegateAccessCodeRequestDTO.setEntityNumber(delegationRequest.getEntityNumber());
		delegateAccessCodeRequestDTO.setFirstName(delegationRequest.getAssisterFirstName());
		delegateAccessCodeRequestDTO.setFunctionalIdentifier(delegationRequest.getFunctionalId());
		delegateAccessCodeRequestDTO.setLastName(delegationRequest.getAssisterLastName());
		delegateAccessCodeRequestDTO.setPhoneNumber(delegationRequest.getPhoneNumber());
		delegateAccessCodeRequestDTO.setRecordId(delegationRequest.getRecordId());
		
		if(GhixAhbxConstants.AGENT.equalsIgnoreCase(delegationRequest.getRecordType()))
		{
			delegateAccessCodeRequestDTO.setRecordType("Agent");
			delegateAccessCodeRequestDTO.setAgentLicenseNumber(delegationRequest.getAgentLicenseNumber());
		}
		else if(GhixAhbxConstants.ASSISTER.equalsIgnoreCase(delegationRequest.getRecordType()))
		{
			delegateAccessCodeRequestDTO.setRecordType("Assister");
		}
		else if(GhixAhbxConstants.ISSUER.equalsIgnoreCase(delegationRequest.getRecordType()))
		{
			delegateAccessCodeRequestDTO.setRecordType("Issuer");
		}
		else if(GhixAhbxConstants.ISSUER_ENROLL_REP.equalsIgnoreCase(delegationRequest.getRecordType()))
		{
			delegateAccessCodeRequestDTO.setRecordType("ISSUER_ENROLL_REP");
		}
		else if(GhixAhbxConstants.APPROVEDADMINSTAFFL1.equalsIgnoreCase(delegationRequest.getRecordType()))
		{
			delegateAccessCodeRequestDTO.setRecordType("ApprovedAdminStaffL1");
		}
		else if(GhixAhbxConstants.APPROVEDADMINSTAFFL2.equalsIgnoreCase(delegationRequest.getRecordType()))
		{
			delegateAccessCodeRequestDTO.setRecordType("ApprovedAdminStaffL2");
		}
		
		//HIOS is not mandatory for some cases
		if(delegationRequest.getHiosIssuerId() > 0) {
			delegateAccessCodeRequestDTO.setHiosIssuerId(Long.valueOf(delegationRequest.getHiosIssuerId()));
		}
		
		delegateAccessCodeRequestDTO.setState(delegationRequest.getState());
		delegateAccessCodeRequestDTO.setStatusDate(delegationRequest.getCertificationDate());
		delegateAccessCodeRequestDTO.setZipcode(delegationRequest.getZipCode());

		//LOGGER.debug("IND54: DelegateAccessCodeRequestDTO successfully populated : " + delegateAccessCodeRequestDTO);

		return delegateAccessCodeRequestDTO;
	}
}
