(function() {
    'use strict';
    angular
        .module('reconciliationWorkbenchApp')
        .factory('searchPaginationService', searchPaginationService);

    searchPaginationService.$inject = ['$rootScope'];

    function searchPaginationService ($rootScope){
        return {
            subscribe: function(scope, callback) {
                var handler = $rootScope.$on('paginateDiscrepancy', callback);
                scope.$on('$destroy', handler);
            },

            notify: function(data) {
                $rootScope.$emit('paginateDiscrepancy', data);
            }
        };
    }
})();