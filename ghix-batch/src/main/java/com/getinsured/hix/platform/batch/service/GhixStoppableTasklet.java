package com.getinsured.hix.platform.batch.service;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.util.Assert;

/**
 */
public abstract class GhixStoppableTasklet implements Tasklet {
    private StepExecution stepExecution;
    private Long progressPercentage = null;

    @Override
    public final RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        this.stepExecution = chunkContext.getStepContext().getStepExecution();
        ActiveJobMonitor.started(this);
        try {
            return _execute(contribution, chunkContext);
        }
        finally {
            ActiveJobMonitor.finished(chunkContext.getStepContext().getStepExecution().getId());
        }
    }

    public StepExecution getStepExecution() {
        return stepExecution;
    }

    public void setProgressPercentage(long value) {
        Assert.isTrue(value >= 0);
        Assert.isTrue(value <= 100);
        progressPercentage = Long.valueOf(value);
    }

    public Long getProgressPercentage() {
        return progressPercentage;
    }

    public boolean hasSetProgress() {
        return progressPercentage != null;
    }

    /**
     * This method replaces Tasklet.execute(StepContribution contribution, ChunkContext chunkContext).
     * Implementation of this method should check stepExecution.isTerminationOnly() within proper period, and process
     * stop gracefully.
     *
     * @param contribution
     * @param chunkContext
     * @return
     * @throws Exception
     */
    protected abstract RepeatStatus _execute(StepContribution contribution, ChunkContext chunkContext) throws Exception;

    /**
     * SpringBatchJobStopper will update the status to STOPPED after notifying this tasklet to stop regardless of
     * this tasklet completed stopping or not. If the return value is less than 0, JobStopper will use default delay
     * value.
     *
     * @return seconds to wait before stop
     */
    public abstract long getStopDelaySeconds();
}
