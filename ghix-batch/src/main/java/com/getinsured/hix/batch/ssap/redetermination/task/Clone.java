package com.getinsured.hix.batch.ssap.redetermination.task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.eligibility.active.enrollment.service.ActiveEnrollmentService;
import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareApplicationDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicantEventService;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicantService;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationEventService;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.dto.LifeChangeEventDTO;
import com.getinsured.iex.lce.ChangedApplicant;
import com.getinsured.iex.lce.RequestParamDTO;
import com.getinsured.iex.lce.SepRequestParamDTO;
import com.getinsured.iex.lce.SepResponseParamDTO;
import com.getinsured.iex.lce.SepTransientDTO;
import com.getinsured.iex.ssap.HomeAddress;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.MailingAddress;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.ReferralUtil;
import com.google.gson.Gson;


/**
 * Spring batch Tasklet to execute Auto renewal of SSAP Applications. 
 * 
 * @author Sahay_B
 *  
 * GI_APP_CONFIG Properties used (iex.ssap.autorenewal.parentappyear, iex.ssap.autorenewal.renewalyear, global.open.enrollment.start.date
 * and global.open.enrollment.end.date)
 * 
 * This batch is expected to run once and leave all {iex.ssap.autorenewal.parentappyear} applications with an  {iex.ssap.autorenewal.renewalyear} 
 * Application in Conditionally eligible open state with cloned applicants and application events. 
 */
@Deprecated
@DependsOn("dynamicPropertiesUtil")
public class Clone extends StepExecutionListenerSupport implements Tasklet {
	
	private static final String SCHEDULED_EXECUTION = "scheduledExecution";
	private static final String RENEWAL_CLONE_BATCH = "RENEWAL_CLONE_BATCH";
	private static final String N = "N";
	private static final String EXADMIN_USERNAME = "exadmin@ghix.com";
	private static final String SSAP_RENEWAL_EVENT = "SSAP_RENEWAL_EVENT";
	private Logger LOGGER = LoggerFactory.getLogger(Clone.class);
	private static final String PROCESSING_RESULT = "processingResult";
	private static final String RENEWED_APPLICATION_ID = "renewedApplicationId";
	private static final String PARENT_APPLICATION_ID = "parentApplicationId";
	private static final String APPLICATION_EXIST_IN = "APPLICATION_EXIST_IN_";
	private static final String PROCESSING_TIME = "processingTime";
	
	private SsapCloneApplicationService ssapCloneApplicationService;
	private SsapCloneApplicantService ssapCloneApplicantService;
	private SsapCloneApplicantEventService ssapCloneApplicantEventService;
	private SsapCloneApplicationEventService ssapCloneApplicationEventService;
	private SsapApplicationRepository ssapApplicationRepository;
	private SsapApplicantRepository ssapApplicantRepository;
	private GhixRestTemplate ghixRestTemplate;
	private ThreadPoolTaskExecutor taskExecutor;
	private GIMonitorService giMonitorService;
	private ActiveEnrollmentService activeEnrollmentService;
	private UserService userService;
	private AccountUser user;
	private SsapJsonBuilder ssapJsonBuilder;
	private static volatile boolean isBatchRunning = false;
	

	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
				
		synchronized (this) {
			if(isBatchRunning) {
				throw new GIException("Batch is already running");
				//return RepeatStatus.FINISHED;
			}
			else {
				isBatchRunning = true;
			}
		}
		try {			
			Long coverageYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR));
			Long renewalYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
			String batchSizeStr = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_BATCHSIZE);
			String isOTREnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ONE_TOUCH_RENEWAL_YEAR);
			String scheduledExecution = chunkContext.getStepContext().getStepExecution().getJobParameters().getString(SCHEDULED_EXECUTION);
			if(scheduledExecution != null && scheduledExecution.equalsIgnoreCase("true".intern()) && (isOTREnabled == null || isOTREnabled.trim().equals(""))){
				LOGGER.info("OTR not enabled, skip scheduled execution.".intern());
				return RepeatStatus.FINISHED;
			}
			Long batchSize = null;
			batchSizeStr = chunkContext.getStepContext().getStepExecution().getJobParameters().getString("BATCH_SIZE", batchSizeStr);

			if (StringUtils.isNumeric(batchSizeStr)) {
				batchSize = Long.valueOf(batchSizeStr);
			}

			if (null == batchSize || 0l == batchSize) {
				throw new GIException("Invalid Batch Size");
			}

			LOGGER.info("Ssap Auto Renewal Batch job to auto renew {} application for {}".intern(),coverageYear,renewalYear);
			List<Long> parentApplicationId = null;
			try {

				List<String> applicationStatusList = new CopyOnWriteArrayList<String>();
				applicationStatusList.add("EN");

				if (isOTREnabled == null || isOTREnabled.trim().equals("")) {
					parentApplicationId = ssapCloneApplicationService.getEnrolledSsapApplicationsByCoverageYear(coverageYear, applicationStatusList, batchSize);
				}else{
					parentApplicationId = ssapCloneApplicationService.getOTRApplicationsByCoverageYear(coverageYear, applicationStatusList, batchSize);
				}
			} catch (Exception giExceotion) {
				giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50001".intern(), new Date(), this.getClass().getName(), giExceotion.getLocalizedMessage(), null,  ExceptionUtils.getFullStackTrace(giExceotion), GIRuntimeException.Component.BATCH.getComponent(), null);
			}
			
			if(parentApplicationId == null) {
				giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50002".intern(), new Date(), this.getClass().getName(), "No Application to Process for cloning".intern(), null, "No Application to Process for cloning".intern(), GIRuntimeException.Component.BATCH.getComponent(), null);
				return RepeatStatus.FINISHED;
			}
			Set<Future<Map<String,String>>> tasks = new HashSet<Future<Map<String,String>>>(parentApplicationId.size());
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Number of applications to clone {} for a maximum threads".intern(),parentApplicationId.size(), taskExecutor.getMaxPoolSize());
			}
			for (Long ssapApplicationId : parentApplicationId) {
				LOGGER.info("Processing Parent Application {} for renewal year {}",ssapApplicationId,renewalYear);
				try {
						tasks.add(taskExecutor.submit(new SsapCloneProcessor(ssapApplicationId, renewalYear))); //new task for one application
					} catch (Exception giExceotion) {
					try {
						giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50003".intern(), new Date(), this.getClass().getName(), giExceotion.getLocalizedMessage(), user, ExceptionUtils.getFullStackTrace(giExceotion), GIRuntimeException.Component.BATCH.getComponent(), null);
					}catch(Exception ex){
						LOGGER.error("Error Processing application", ex);
					}
					LOGGER.error("Error Processing application", giExceotion);
				}
			}
			
			waitForAllTaskToComplete(tasks);
			//log data
			ssapCloneApplicationService.logData(tasks,RENEWAL_CLONE_BATCH);
		} finally {
			isBatchRunning = false;
		}	

		return RepeatStatus.FINISHED;

	}

	/*
	 * Wait for all task to complete
	 */
	private void waitForAllTaskToComplete(Set<Future<Map<String,String>>> tasks) {
		boolean batchIsNotCompleted = true; 
		long currentNanoTime = System.nanoTime();
		long elapsedNanoTime = System.nanoTime();
		
		while(batchIsNotCompleted || (elapsedNanoTime/1000000000 >  60 * 15)) {
			boolean isAllTaskCompleted = true;
			for (Future<Map<String,String>> future : tasks) {
				if(!future.isDone()) {
					isAllTaskCompleted = false;
					break;
				}
			}
			batchIsNotCompleted = !isAllTaskCompleted;
			elapsedNanoTime = System.nanoTime() - currentNanoTime;
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Maximum threads active {}".intern(), taskExecutor.getActiveCount());
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				//ignore this
			}
		}
	}
	
	/**
	 * Check if there is an existing application for renewal year
	 * @param parentApplication - Application from previous year
	 * @param renewalYear
	 * @return
	 */
	private boolean noRenewalApplicationExist(SsapApplication parentApplication, Long renewalYear) {
		Long openApplicationCount = ssapCloneApplicationService.findOpenApplicationByCoverageYearAndCmrHouseoldId(parentApplication.getCmrHouseoldId(), renewalYear);
		if(openApplicationCount>0) {
			return false;
		}
		return true;
	}


	/*
	 *Processor to take one SsapApplication and clone for renewal
	 */
	private class SsapCloneProcessor implements Callable<Map<String,String>> {

		Long parentApplicationId = null;
		//SsapApplication currentApplication = null;
		Long renewalYear = null;
		
		public SsapCloneProcessor(Long application, Long renewalYear) {
			this.parentApplicationId = application;
			this.renewalYear = renewalYear;
		}


		@Override
		public Map<String,String> call() throws Exception {
			return cloneSsap(parentApplicationId, renewalYear);
		}

		private Map<String,String> cloneSsap(Long parentApplicationId, Long renewalYear) throws Exception {
			
			LOGGER.info("Started Processing application {}",parentApplicationId);		
			SsapApplication parentApplication = null;
			SsapApplication currentApplication = null;
			Map<String,String> result = new HashMap<String,String>();
			result.put(PARENT_APPLICATION_ID, String.valueOf(parentApplicationId));
			long time = System.currentTimeMillis();
			String processingResult = null; 
			String applicationSource = "CN";
			try {
				parentApplication = ssapApplicationRepository.findOne(parentApplicationId);
				
				if(noRenewalApplicationExist(parentApplication, renewalYear)){
					if(isEnrollmentActive(parentApplication)) { //Check if there is an active enrollment. If not put application in skipped status
						List<SsapApplicant> ssapApplicants;
						List<SsapApplicationEvent> ssapApplicationEvents;
						List<SsapApplicantEvent> ssapApplicantEvents;
						try {
							SepTransientDTO sepTransientDTO = new SepTransientDTO();
							SepRequestParamDTO sepRequestParamDTO = null;
							SepResponseParamDTO sepResponseParamDTO = new SepResponseParamDTO();
							String ssapJson = null;
							Map<String, Boolean> demographicFlagMap = new HashMap<String, Boolean>();
							Map<Long, List<String>> demographicEventsData = new HashMap<Long, List<String>>();
							List<Boolean> homeAddressChangeDemoFlags = new ArrayList<Boolean>();
							Date demographicEventDate = null;
							Date dobEventDate = null;
							
							ssapJson = parentApplication.getApplicationData();
							List<SsapApplicant> parentApplicants = ssapApplicantRepository
									.findBySsapApplication(parentApplication);
							sepRequestParamDTO = createSepRequestParamDTOForRenewal(parentApplication,
									parentApplicants);

							sepTransientDTO.setDemographicEventDate(demographicEventDate);
							sepTransientDTO.setDemographicEventsData(demographicEventsData);
							sepTransientDTO.setDemographicFlagMap(demographicFlagMap);
							sepTransientDTO.setHomeAddressChangeDemoFlags(homeAddressChangeDemoFlags);
							sepTransientDTO.setDobEventDate(dobEventDate);
							sepTransientDTO.setCmrHouseholdId(sepRequestParamDTO.getHouseholdId());
							sepTransientDTO.setEnrolledPersons(invokeEnrollmentApi(parentApplication.getId(),
									sepRequestParamDTO.getUserName(), parentApplicants));
							sepTransientDTO.setUserName(sepRequestParamDTO.getUserName());
							sepTransientDTO.setUserId(sepRequestParamDTO.getUserId());

							List<RequestParamDTO> events = sepRequestParamDTO.getEvents();

							updateDemographicMaps(demographicFlagMap);
							
							if(parentApplication.getRenewalStatus() != null && RenewalStatus.OTR.equals(parentApplication.getRenewalStatus())){
								applicationSource = "ON";
							}
							
							currentApplication = ssapCloneApplicationService.cloneSsapApplicationFromParent(ssapJson,
									currentApplication, parentApplication, sepRequestParamDTO, "OE", applicationSource,
									ApplicationStatus.SIGNED.getApplicationStatusCode(), renewalYear);
							ssapApplicants = ssapCloneApplicantService.cloneSsapApplicants(currentApplication, events,
									sepTransientDTO.getUserId(), sepResponseParamDTO,parentApplication,sepRequestParamDTO, new AtomicBoolean(false));
							ssapApplicationEvents = ssapCloneApplicationEventService.cloneSsapApplicationEvent(
									currentApplication, sepTransientDTO.getUserId(), SsapApplicationEventTypeEnum.OE);
							ssapApplicantEvents = ssapCloneApplicantEventService.cloneSsapApplicantEvents(
									currentApplication, ssapApplicationEvents.get(0), ssapApplicants, events,
									sepTransientDTO);

							ssapJson = updateMailingAddressSameAsHomeAddressIndicator(ssapJson, currentApplication);
							currentApplication.setApplicationData(ssapJson);
							ssapApplicationEvents.get(0).setSsapApplicantEvents(ssapApplicantEvents);
							currentApplication.setSsapApplicants(ssapApplicants);
							currentApplication.setSsapApplicationEvents(ssapApplicationEvents);
							// Story - HIX-86352 - update CSR Level
							ssapCloneApplicationService.updateCSRLevel(currentApplication);

							if (currentApplication != null) {
//								currentApplication.setRenewalStatus(RenewalStatus.STARTED);
								currentApplication = ssapCloneApplicationService
										.saveSsapSepApplication(currentApplication, true);
								if(LOGGER.isInfoEnabled()) {
									LOGGER.info("Cloned SSAP Application case number {} from Parent Application case number {}",currentApplication.getCaseNumber(),parentApplication.getCaseNumber());
								}
								parentApplication.setRenewalStatus(RenewalStatus.CLONED);
								ssapCloneApplicationService.saveSsapSepApplication(parentApplication, false);
								processingResult = "APPLICATION_CLONED".intern();
								result.put(RENEWED_APPLICATION_ID,String.valueOf(currentApplication.getId()));
							}

						} catch (Exception e) {
							LOGGER.error("Error while cloning application: {}",parentApplication.getId(), e);
							throw new GIException(50005,
									"Error while cloning application. " + parentApplication.getId() +  ExceptionUtils.getFullStackTrace(e),
									"High");
						}
					} else {
						parentApplication.setRenewalStatus(RenewalStatus.SKIPPED);
						ssapCloneApplicationService.saveSsapSepApplication(parentApplication, false);
						processingResult = "SKIPPED_ENROLLMENT_NOT_CONFRIM_PENDING";
					}
				}else{
					processingResult = APPLICATION_EXIST_IN+renewalYear;
				}
			} catch (GIException e) {
				parentApplication.setRenewalStatus(RenewalStatus.ERROR);
				try{
				ssapCloneApplicationService.saveSsapSepApplication(parentApplication, false);
				giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_"+ e.getErrorCode(), new Date(), this.getClass().getName(), ExceptionUtils.getFullStackTrace(e), user, parentApplication.getCaseNumber(), GIRuntimeException.Component.BATCH.getComponent(), null);
				}catch(Exception ex){
					LOGGER.error("Error Processing application case number" + parentApplication.getCaseNumber() + " after step " + parentApplication.getRenewalStatus(), e);
				}
				LOGGER.error("Error Processing application case number" + parentApplication.getCaseNumber() + " after step " + parentApplication.getRenewalStatus(), e);
				processingResult = RenewalStatus.ERROR.toString();
				result.put("exception",ExceptionUtils.getFullStackTrace(e));
			}catch (Exception e) {
				parentApplication.setRenewalStatus(RenewalStatus.ERROR); 
				try{
				ssapCloneApplicationService.saveSsapSepApplication(parentApplication, false);
					giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50001", new Date(), this.getClass().getName(),ExceptionUtils.getFullStackTrace(e), user, parentApplication.getCaseNumber(), GIRuntimeException.Component.BATCH.getComponent(), null);
				}catch(Exception ex){
					LOGGER.error("Error Processing application case number" + parentApplication.getCaseNumber() + " after step " + parentApplication.getRenewalStatus(), e);
				}
				LOGGER.error("Error Processing application case number" + parentApplication.getCaseNumber() + " after step " + parentApplication.getRenewalStatus(), e);
				processingResult = RenewalStatus.ERROR.toString();
				result.put("exception",ExceptionUtils.getFullStackTrace(e));
				
			}
			result.put(PROCESSING_TIME,String.valueOf(System.currentTimeMillis()-time));
			result.put(PROCESSING_RESULT,processingResult);
			
			LOGGER.info("Task completed for application " + parentApplication.getId());
			return result;
		}
		
		private String updateMailingAddressSameAsHomeAddressIndicator(
				String ssapJson,SsapApplication currentApplication) {
			SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
			
			for(TaxHousehold taxHousehold : singleStreamlinedApplication.getTaxHousehold()) {
				for(HouseholdMember houseHoldMember : taxHousehold.getHouseholdMember()) {
					if(houseHoldMember.getHouseholdContact() != null) {
						HomeAddress homeAddress = houseHoldMember.getHouseholdContact().getHomeAddress();
						MailingAddress mailingAddress = houseHoldMember.getHouseholdContact().getMailingAddress();
						houseHoldMember.getHouseholdContact().setMailingAddressSameAsHomeAddressIndicator(isHomeAddressSameAsMailingAddress(homeAddress, mailingAddress) ? true : false);
					}
				}
			}
			
			return ssapJsonBuilder.transformToJson(singleStreamlinedApplication);
			
		}


		private boolean isHomeAddressSameAsMailingAddress(
				HomeAddress homeAddress, MailingAddress mailingAddress) {
			if(homeAddress != null && mailingAddress != null) {
				return (
						nullCheckedValue(homeAddress.getStreetAddress1()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getStreetAddress1())) 
						&& nullCheckedValue(homeAddress.getStreetAddress2()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getStreetAddress2()))
						&& nullCheckedValue(homeAddress.getCity()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getCity()))
						&& nullCheckedValue(homeAddress.getCounty()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getCounty())) 
						&& nullCheckedValue(homeAddress.getCountyCode()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getCountyCode()))
						&& nullCheckedValue(homeAddress.getPostalCode()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getPostalCode()))
						&& nullCheckedValue(homeAddress.getState()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getState()))
						);
			}
			return true; 
		}
		
		private String nullCheckedValue(String value) {
			if(StringUtils.isBlank(value)) {
				return "";
			}
			return value;
		}


		/**
		 * Call Enrollment to check if there is an active enrollment 
		 * @param applicationId
		 * @return
		 * @throws GIException
		 * @throws RestClientException
		 * @throws JsonProcessingException
		 */
		private boolean isEnrollmentActive(SsapApplication parentApplication) throws GIException, RestClientException, JsonProcessingException {
			boolean isEnrollmentActive = false;
			try {
				isEnrollmentActive = activeEnrollmentService.isEnrollmentActive(parentApplication.getId(), EXADMIN_USERNAME);
			} catch (Exception e) {
				throw new GIException(50004, "Error while calling for enrollment API to determine isEnrollmentActive for " + parentApplication.getCaseNumber() + parentApplication.getId(), "High");
			}
			return isEnrollmentActive;
		}

		private SepRequestParamDTO createSepRequestParamDTOForRenewal(SsapApplication parentApplication, List<SsapApplicant> ssapApplicants) throws InvalidUserException {
			SepRequestParamDTO sepRequestParamDTO = new SepRequestParamDTO();
			
			if(parentApplication.getEsignDate() != null) {
				sepRequestParamDTO.setEsignDate(parentApplication.getEsignDate().toString());
			}
			
			sepRequestParamDTO.setEsignFirstName(parentApplication.getEsignFirstName());
			sepRequestParamDTO.setEsignLastName(parentApplication.getEsignLastName());
			sepRequestParamDTO.setEsignMiddleName(parentApplication.getEsignMiddleName());
			sepRequestParamDTO.setEvents(getEvents(ssapApplicants));
			sepRequestParamDTO.setHouseholdId(parentApplication.getCmrHouseoldId().longValue());
			sepRequestParamDTO.setSsapJSON(parentApplication.getApplicationData());
			if(userService.getLoggedInUser() != null) {
				sepRequestParamDTO.setUserId(new Long(userService.getLoggedInUser().getId()));
				sepRequestParamDTO.setUserName(userService.getLoggedInUser().getUsername());
			}
			else {
				sepRequestParamDTO.setUserId(1L);
				sepRequestParamDTO.setUserName(EXADMIN_USERNAME);
			}
			
			return sepRequestParamDTO;
		}

		private List<RequestParamDTO> getEvents(List<SsapApplicant> ssapApplicants) {
			List<RequestParamDTO> requestParamDTOList = new ArrayList<RequestParamDTO>();
			RequestParamDTO requestParamDTO = new RequestParamDTO();
			requestParamDTO.setDataChanged(true);
			requestParamDTO.setEventCategory(SSAP_RENEWAL_EVENT);
			requestParamDTO.setEventSubCategory(SSAP_RENEWAL_EVENT);
			requestParamDTO.setEventSubCategoryDate(new Date().toString());
			List<ChangedApplicant> changedApplicants = new  ArrayList<ChangedApplicant>();
			
			for (SsapApplicant ssapApplicant : ssapApplicants) {
				ChangedApplicant changedApplicant = new ChangedApplicant();
				changedApplicant.setEventDate(new Date().toString());
				changedApplicant.setIsChangeInZipCodeOrCounty(false);
				changedApplicant.setPersonId(ssapApplicant.getPersonId());
				changedApplicants.add(changedApplicant);
			}
			
			requestParamDTO.setChangedApplicants(changedApplicants);
			requestParamDTOList.add(requestParamDTO);
			return requestParamDTOList;
		}
		
		public Set<Long> invokeEnrollmentApi(long applicationId, String userName, List<SsapApplicant> parentApplicants) throws RestClientException, Exception {
			LifeChangeEventDTO lifeChangeEventDTO = new LifeChangeEventDTO();
			lifeChangeEventDTO.setOldApplicationId(applicationId);
			lifeChangeEventDTO.setUserName(userName);
			//String responseDTO = ghixRestServiceInvoker.invokeRestService(lifeChangeEventDTO, SsapEndPoints.LCE_GET_ENROLLED_APPLICANTS, HttpMethod.POST, GIRuntimeException.Component.LCE.toString());
			Set<Long> enrolledPersons = null;

			lifeChangeEventDTO = invokeEnrollmentApi(lifeChangeEventDTO);
			enrolledPersons = lifeChangeEventDTO.getPersonIds();
		
			return enrolledPersons;
		}
		
		protected LifeChangeEventDTO invokeEnrollmentApi(LifeChangeEventDTO lifeChangeEventDTO) throws Exception {
			List<EnrollmentShopDTO> enrollmentShopDTOs = null;
			EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
			Gson g = new Gson();
			enrolleeRequest.setSsapApplicationId(lifeChangeEventDTO.getOldApplicationId());
			//EnrolleeResponse enrolleeResponse = enrolleeService.findEnrolleeByApplicationid(enrolleeRequest);
			ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID_JSON, lifeChangeEventDTO.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, g.toJson(enrolleeRequest));
			 
			EnrolleeResponse enrolleeResponse = (EnrolleeResponse) g.fromJson(response.getBody(), EnrolleeResponse.class);
			
			if (null != enrolleeResponse & enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
				enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
				return extractEnrolledData(enrollmentShopDTOs, executeCompare(lifeChangeEventDTO.getOldApplicationId()), lifeChangeEventDTO);
			} else {
				throw new GIException("Unable to get Enrollment Plan Details. Error Details: " + enrolleeResponse.getErrCode() + ":" + enrolleeResponse.getErrMsg());
			}
		}
		
		private CompareMainDTO executeCompare(long enrolledApplicationId)
				throws Exception {
			CompareMainDTO compareMainDTO = null;

			final SsapApplication enrolledApplication = loadSsapApplicants(enrolledApplicationId);
			compareMainDTO = transformCompareDto(enrolledApplication);

			return compareMainDTO;
		}
		
		private LifeChangeEventDTO extractEnrolledData(List<EnrollmentShopDTO> enrollmentShopDTOs, CompareMainDTO compareMainDTO, LifeChangeEventDTO lifeChangeEventDTO) {
			Set<Long> personIds = new HashSet<>();

			for (EnrollmentShopDTO enrollmentShopDTO : enrollmentShopDTOs) {
				List<EnrolleeShopDTO> enrolleeShopDTO = enrollmentShopDTO.getEnrolleeShopDTOList();
				for (EnrolleeShopDTO enrolleeMembers : enrolleeShopDTO) {
					for (CompareApplicantDTO enrolledApplicant : compareMainDTO.getEnrolledApplication().getApplicants()) {
						if (enrolleeMembers.getExchgIndivIdentifier().equals(enrolledApplicant.getApplicantGuid())) {
							personIds.add(enrolledApplicant.getPersonId());
						}
					}
				}
			}
			
			lifeChangeEventDTO.setPersonIds(personIds);
			
			return lifeChangeEventDTO;
		}
		
		private CompareMainDTO transformCompareDto(SsapApplication enrolledApplication) {
			CompareMainDTO compareMainDTO = new CompareMainDTO();
			compareMainDTO.setEnrolledApplication(transformApplication(enrolledApplication));
			return compareMainDTO;
		}
		
		public CompareApplicationDTO transformApplication(SsapApplication ssapApplication) {
			final CompareApplicationDTO compareApplicationDTO = new CompareApplicationDTO();
			ReferralUtil.copyProperties(ssapApplication, compareApplicationDTO);
			transformApplicants(compareApplicationDTO, ssapApplication);
			return compareApplicationDTO;
		}
		
		private void transformApplicants(CompareApplicationDTO compareApplicationDTO, SsapApplication ssapApplication) {
			CompareApplicantDTO compareApplicantDTO;
			for (SsapApplicant ssapApplicant : ssapApplication.getSsapApplicants()) {
				compareApplicantDTO = new CompareApplicantDTO();
				ReferralUtil.copyProperties(ssapApplicant, compareApplicantDTO);
				compareApplicationDTO.addApplicant(compareApplicantDTO);
			}
		}
		
		private void updateDemographicMaps(Map<String, Boolean> demographicFlagMap) {
			
			demographicFlagMap.put(LifeChangeEventConstant.IS_DEMOGRAPHIC_CHANGE, false);
			demographicFlagMap.put(LifeChangeEventConstant.IS_DOB_CHANGE, false);
			demographicFlagMap.put(LifeChangeEventConstant.IS_OTHER_CHANGE, false);
			demographicFlagMap.put(LifeChangeEventConstant.IS_ADDRESS_DEMOGRAPHIC_CHANGE, false);
			
		}	
		
		
		private SsapApplication loadSsapApplicants(Long enApp) {
			return ssapApplicationRepository.findAndLoadApplicantsByAppId(enApp);
		}

	}

	public SsapApplicationRepository getSsapApplicationRepository() {
		return ssapApplicationRepository;
	}

	public void setSsapApplicationRepository(
			SsapApplicationRepository ssapApplicationRepository) {
		this.ssapApplicationRepository = ssapApplicationRepository;
	}

	public GhixRestTemplate getGhixRestTemplate() {
		return ghixRestTemplate;
	}

	public void setGhixRestTemplate(GhixRestTemplate ghixRestTemplate) {
		this.ghixRestTemplate = ghixRestTemplate;
	}


	public ThreadPoolTaskExecutor getTaskExecutor() {
		return taskExecutor;
	}


	public void setTaskExecutor(ThreadPoolTaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}


	public SsapApplicantRepository getSsapApplicantRepository() {
		return ssapApplicantRepository;
	}


	public void setSsapApplicantRepository(
			SsapApplicantRepository ssapApplicantRepository) {
		this.ssapApplicantRepository = ssapApplicantRepository;
	}

	public GIMonitorService getGiMonitorService() {
		return giMonitorService;
	}

	public void setGiMonitorService(GIMonitorService giMonitorService) {
		this.giMonitorService = giMonitorService;
	}

	public ActiveEnrollmentService getActiveEnrollmentService() {
		return activeEnrollmentService;
	}

	public void setActiveEnrollmentService(
			ActiveEnrollmentService activeEnrollmentService) {
		this.activeEnrollmentService = activeEnrollmentService;
	}

	public SsapCloneApplicationService getSsapCloneApplicationService() {
		return ssapCloneApplicationService;
	}

	public void setSsapCloneApplicationService(
			SsapCloneApplicationService ssapCloneApplicationService) {
		this.ssapCloneApplicationService = ssapCloneApplicationService;
	}

	public SsapCloneApplicantService getSsapCloneApplicantService() {
		return ssapCloneApplicantService;
	}

	public void setSsapCloneApplicantService(
			SsapCloneApplicantService ssapCloneApplicantService) {
		this.ssapCloneApplicantService = ssapCloneApplicantService;
	}

	public SsapCloneApplicantEventService getSsapCloneApplicantEventService() {
		return ssapCloneApplicantEventService;
	}

	public void setSsapCloneApplicantEventService(
			SsapCloneApplicantEventService ssapCloneApplicantEventService) {
		this.ssapCloneApplicantEventService = ssapCloneApplicantEventService;
	}

	public SsapCloneApplicationEventService getSsapCloneApplicationEventService() {
		return ssapCloneApplicationEventService;
	}

	public void setSsapCloneApplicationEventService(
			SsapCloneApplicationEventService ssapCloneApplicationEventService) {
		this.ssapCloneApplicationEventService = ssapCloneApplicationEventService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public SsapJsonBuilder getSsapJsonBuilder() {
		return ssapJsonBuilder;
	}

	public void setSsapJsonBuilder(SsapJsonBuilder ssapJsonBuilder) {
		this.ssapJsonBuilder = ssapJsonBuilder;
	}



	
}
