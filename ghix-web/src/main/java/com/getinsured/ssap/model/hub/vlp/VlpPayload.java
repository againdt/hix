package com.getinsured.ssap.model.hub.vlp;

import java.util.Date;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.getinsured.ssap.json.VlpPayloadDeserializer;
import com.getinsured.ssap.json.VlpPayloadSerializer;
import com.getinsured.ssap.model.hub.vlp.documents.DhsDocument;

/**
 * VLP Payload object. For serialization of this object
 * see {@link VlpPayloadSerializer}. The fields that will
 * be output by custom serializer is DIFFERENT from what you
 * see here.
 *
 * @author Yevgen Golubenko
 * @since 2/18/19
 */
@JsonSerialize(using = VlpPayloadSerializer.class)
@JsonDeserialize(using = VlpPayloadDeserializer.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class VlpPayload
{
  // Because we save this payload in db column, but we save what we send to DHS, and it's using different case
  private DhsDocument dhsDocument;
  private String firstName;
  private String lastName;
  private String middleName;
  private String aka;
  private Date dateOfBirth;
  private boolean requestSponsorDataIndicator;
  private boolean requestGrantDateIndicator;
  private boolean fiveYearBarApplicabilityIndicator;
  private String requesterCommentsForHub;
  private String casePOCFullName;
  private String casePOCPhoneNumber;

  public DhsDocument getDhsDocument()
  {
    return dhsDocument;
  }

  public void setDhsDocument(final DhsDocument dhsDocument)
  {
    this.dhsDocument = dhsDocument;
  }

  public String getFirstName()
  {
    return firstName;
  }

  public void setFirstName(final String firstName)
  {
    this.firstName = firstName;
  }

  public String getLastName()
  {
    return lastName;
  }

  public void setLastName(final String lastName)
  {
    this.lastName = lastName;
  }

  public String getMiddleName()
  {
    return middleName;
  }

  public void setMiddleName(final String middleName)
  {
    this.middleName = middleName;
  }

  public String getAka()
  {
    return aka;
  }

  public void setAka(final String aka)
  {
    this.aka = aka;
  }

  public Date getDateOfBirth()
  {
    return dateOfBirth;
  }

  public void setDateOfBirth(final Date dateOfBirth)
  {
    this.dateOfBirth = dateOfBirth;
  }

  public boolean isRequestSponsorDataIndicator()
  {
    return requestSponsorDataIndicator;
  }

  public void setRequestSponsorDataIndicator(final boolean requestSponsorDataIndicator)
  {
    this.requestSponsorDataIndicator = requestSponsorDataIndicator;
  }

  public boolean isRequestGrantDateIndicator()
  {
    return requestGrantDateIndicator;
  }

  public void setRequestGrantDateIndicator(final boolean requestGrantDateIndicator)
  {
    this.requestGrantDateIndicator = requestGrantDateIndicator;
  }

  public boolean isFiveYearBarApplicabilityIndicator()
  {
    return fiveYearBarApplicabilityIndicator;
  }

  public void setFiveYearBarApplicabilityIndicator(final boolean fiveYearBarApplicabilityIndicator)
  {
    this.fiveYearBarApplicabilityIndicator = fiveYearBarApplicabilityIndicator;
  }

  public String getRequesterCommentsForHub()
  {
    return requesterCommentsForHub;
  }

  public void setRequesterCommentsForHub(final String requesterCommentsForHub)
  {
    this.requesterCommentsForHub = requesterCommentsForHub;
  }

  public String getCasePOCFullName()
  {
    return casePOCFullName;
  }

  public void setCasePOCFullName(final String casePOCFullName)
  {
    this.casePOCFullName = casePOCFullName;
  }

  public String getCasePOCPhoneNumber()
  {
    return casePOCPhoneNumber;
  }

  public void setCasePOCPhoneNumber(final String casePOCPhoneNumber)
  {
    this.casePOCPhoneNumber = casePOCPhoneNumber;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", VlpPayload.class.getSimpleName() + "[", "]")
        .add("dhsDocument=" + dhsDocument)
        .add("firstName='" + firstName + "'")
        .add("lastName='" + lastName + "'")
        .add("middleName='" + middleName + "'")
        .add("aka='" + aka + "'")
        .add("dateOfBirth=" + dateOfBirth)
        .add("requestSponsorDataIndicator=" + requestSponsorDataIndicator)
        .add("requestGrantDateIndicator=" + requestGrantDateIndicator)
        .add("fiveYearBarApplicabilityIndicator=" + fiveYearBarApplicabilityIndicator)
        .add("requesterCommentsForHub='" + requesterCommentsForHub + "'")
        .add("casePOCFullName='" + casePOCFullName + "'")
        .add("casePOCPhoneNumber='" + casePOCPhoneNumber + "'")
        .toString();
  }
}
