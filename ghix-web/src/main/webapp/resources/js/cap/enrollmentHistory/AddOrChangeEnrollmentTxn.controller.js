(function() {
	"use strict";
	angular.module("enrollmentHistoryApp").controller('AddOrChangeEnrollmentTxnCtrl', AddOrChangeEnrollmentTxnCtrl);
	
	AddOrChangeEnrollmentTxnCtrl.$inject = ['$scope', '$http', '$timeout', '$state', '$stateParams', '$anchorScroll', 'enrollmentHistoryService'];
	
	function AddOrChangeEnrollmentTxnCtrl($scope, $http, $timeout, $state, $stateParams, $anchorScroll, enrollmentHistoryService){
		var vm = this;
		
		vm.enrollment = $stateParams.newEnrollment;
		vm.oldEnrollment = $stateParams.oldEnrollment;
		
		vm.isEditMode = $stateParams.isEditMode;
		vm.sendAsAddEnrollmentChange = sendAsAddEnrollmentChange;
		vm.validateAndSaveAddOrChangeTxn = validateAndSaveAddOrChangeTxn;
		vm.backToPremiumHistory = backToPremiumHistory;
		vm.updateEnrolleeStartDate = updateEnrolleeStartDate;
		vm.relationshipOrderBy = enrollmentHistoryService.relationshipOrderBy;
		vm.updateSubscriberTxnType = updateSubscriberTxnType;
		vm.setIsTermReasonNonPaymentFlag = setIsTermReasonNonPaymentFlag;
		vm.setTxnReason = setTxnReason;
		vm.validateLastPremPaidThroughDate = validateLastPremPaidThroughDate;
		
		_init();
		
		function _init(){
			if(vm.isEditMode){
				vm.enrollmentLevelTxnType = 'CHANGE';
				vm.isTermReasonNonPayment = false;
				vm.enrollmentLastPremPaidThroughDateUI = '';
				vm.enrollment.enrolleeDataDtoList.forEach(function(enrollee){
					enrollee.txnType = vm.enrollmentLevelTxnType;
					enrollee.txnReason = '29';
				})
			}else{
				vm.enrollment.enrollmentStartDateOriginal = vm.enrollment.enrollmentBenefitEffectiveStartDateUI;
				vm.isTermReasonNonPayment = false;
				sendAsAddEnrollmentChange();
			}
			
		}
		
		
		function sendAsAddEnrollmentChange(){			
			angular.forEach(vm.enrollment.enrolleeDataDtoList, function(value, key){
			value.enrolleeEffectiveEndDateUI = value.enrolleeEffectiveEndDateUI.replace(value.enrolleeEffectiveEndDateUI.substring(0,5), '12/31');
				
			});
			
			$anchorScroll();
		}
		
		

		function validateAndSaveAddOrChangeTxn() {
			if (vm.isEditMode == false) {
				sendAsAddTransaction();
			} else if (vm.isEditMode == true) {
				if(vm.enrollmentLevelTxnType == "TERM"){
					updateTransactionReasonForNonSubscribers();
				}
				sendAsChangeTransaction();
			}
		}


		function sendAsAddTransaction(){
			$scope.loader = true;
			var url = '/hix/crm/member/updateEnrollmentdetails';
			var data = {
					'id': vm.enrollment.enrollmentId,
					'householdId': vm.enrollment.houseHoldCaseId,
					'comment': vm.addOrChangeComment,
					'send834': vm.addOrChangeSendIn834,
					"benefitEffectiveDateStr": vm.enrollment.enrollmentBenefitEffectiveStartDateUI,
					"aptcAmount": vm.enrollment.aptcAmount != "" ? vm.enrollment.aptcAmount : null,
					"action":"ADD",
				    "members": []
			};
			angular.forEach(vm.enrollment.enrolleeDataDtoList, function(value, key){
				if(value.txnType == "true"){
				data.members.push({
					'exchgIndivIdentifier': value.memberId,
					'enrolleeId': value.enrolleeId
				});
				}
			});
			
			var promise = $http.post(url, data);
			
			promise.success(function(response){
				$scope.loader = false;
				if(response.errCode === 200){
					getUpdatedEnrollmentDetail(vm.enrollment.enrollmentId);
				}else{
					vm.isUpdatedSuccess = false;
					
					showAddOrChangeModal(response.errCode, response.errMsg);
				}
			});
			
			promise.error(function(response){
				$scope.loader = false;
				vm.isUpdatedSuccess = false;
				
				showAddOrChangeModal(response.errCode, response.errMsg);
			});	
		}
		
		function sendAsChangeTransaction(){
			$scope.loader = true;
			var url = '/hix/crm/member/updateEnrollmentdetails';
			var data = {
					'id': vm.enrollment.enrollmentId,
					'householdId': vm.enrollment.houseHoldCaseId,
					'comment': vm.addOrChangeComment,
					'send834': vm.addOrChangeSendIn834,
					'benefitEffectiveDate': null,
					'benefitEndDate': null,
					'benefitEffectiveDateStr': vm.enrollment.enrollmentBenefitEffectiveStartDateUI,
					'benefitEndDateStr': vm.enrollment.enrollmentBenefitEffectiveEndDateUI,
					'confirmIfNotConfirmed': false,
					'premiumPaidToDateEndStr' : enrollmentHistoryService.getUIDateFormat(vm.enrollmentLastPremPaidThroughDateUI),
					'premiumPaidToDateEnd' : null,
				    "members": []
			};
			angular.forEach(vm.enrollment.enrolleeDataDtoList, function(value, key){
				var genderLookupCode = '';
				
				if(value.genderLookupLabel.toUpperCase() === 'MALE'){
					genderLookupCode = 'M';
				}else if(value.genderLookupLabel.toUpperCase() === 'FEMALE'){
					genderLookupCode = 'F';
				}else{
					genderLookupCode = 'U';
				}
				
				
				data.members.push({
					'exchgIndivIdentifier': value.memberId,
					'enrolleeId': value.enrolleeId,
					'txnType': value.txnType,
					'txnReason': value.txnReason,
					'effectiveStartDateStr': value.enrolleeEffectiveStartDateUI, 
					'effectiveEndDateStr': value.enrolleeEffectiveEndDateUI,
					'firstName': value.firstName, 
					'lastName': value.lastName, 
					'middleName': value.middleName, 
					'ssn': value.taxIdentificationNumber, 
					'gender': genderLookupCode
				});
			});
			
			data.enrollmentPremiumDtoList = angular.copy(vm.enrollment.enrollmentPremiumDtoList);
			var promise = $http.post(url, data);
			
			promise.success(function(response){
				$scope.loader = false;
				if(response.errCode === 200){
					getUpdatedEnrollmentDetail(vm.enrollment.enrollmentId);
				}else{
					vm.isUpdatedSuccess = false;
					
					showAddOrChangeModal(response.errCode, response.errMsg);
				}
			});
			
			promise.error(function(response){
				$scope.loader = false;
				vm.isUpdatedSuccess = false;
				
				showAddOrChangeModal(response.errCode, response.errMsg);
			});	
		}
		
		
		function backToPremiumHistory(type){
			$('.modal-backdrop').remove();
			
			var isEditMode = false;
			
			if(type === 'back'){
				isEditMode = true;
			}
			else if (type === 'cancel'){
				vm.enrollment = vm.oldEnrollment;
			}
			if(type === 'back' || type === 'cancel' || type === true){
				$state.go('premiumHistory', {newEnrollment: vm.enrollment, oldEnrollment: vm.oldEnrollment, isEditMode: isEditMode, fromPage: 'addOrChangeEnrollmentTxn', isEnrollmentSaved: type});
			}
		}
		
		function updateEnrolleeStartDate(){

			$timeout(function() {
				if($scope.addOrChangeEnrollmentTxnForm.enrollmentBenefitEffectiveStartDateUI.$invalid === false){
					vm.enrollment.enrolleeDataDtoList.forEach(function(enrollee){
						var startDateUI = vm.enrollment.enrollmentBenefitEffectiveStartDateUI
						enrollee.enrolleeEffectiveStartDateUI = startDateUI;
					})
				}
			}, 0)
		}
		
		function getUpdatedEnrollmentDetail(enrollmentId){

			var data={
				encHouseholdId:$('#encHouseHoldId').val()
			};
			
			var url = '/hix/crm/member/enrollmentandenrolleedetails';

			var promise = $http.post(url, $.param(data), {
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformResponse: [function(data){return JSON.parse(data);}]
			});
			
			promise.success(function(response){
				if(response.errCode === 200){
					vm.isUpdatedSuccess = true;
					for (var key in response.enrollmentDataDtoListMap) {
						for(var i = 0; i < response.enrollmentDataDtoListMap[key].length; i++){
								if(enrollmentId === response.enrollmentDataDtoListMap[key][i].enrollmentId){
								
								vm.enrollment = response.enrollmentDataDtoListMap[key][i];
								
								vm.enrollment.enrollmentBenefitEffectiveStartDateUI = enrollmentHistoryService.getUIDateFormat(vm.enrollment.enrollmentBenefitEffectiveStartDate);
								vm.enrollment.enrollmentBenefitEffectiveEndDateUI = enrollmentHistoryService.getUIDateFormat(vm.enrollment.enrollmentBenefitEffectiveEndDate);
								vm.enrollment.enrollmentStatus = vm.enrollment.enrollmentStatus;
								angular.forEach(vm.enrollment.enrolleeDataDtoList, function(value, key){
									value.enrolleeEffectiveStartDateUI = enrollmentHistoryService.getUIDateFormat(value.enrolleeEffectiveStartDate);
									value.enrolleeEffectiveEndDateUI = enrollmentHistoryService.getUIDateFormat(value.enrolleeEffectiveEndDate);
		            			});
								
								
								angular.forEach(vm.enrollment.enrollmentPremiumDtoList, function(value, key){
		            				if(value.isFinancial === false){
		            					value.aptcAmount = 0;
		            				}
		            			});
								
							}
						}
					}
				}else{
					vm.isUpdatedSuccess = false;
				}
				vm.isEditMode = false;
				$anchorScroll();
				showAddOrChangeModal(response.errCode, response.errMsg);
			});
			
			promise.error(function(response){
				$scope.loader = false;
				vm.isUpdatedSuccess = false;
				showAddOrChangeModal(response.errCode, response.errMsg);
			});
		}
		
		function showAddOrChangeModal(errCode, errMsg){
			vm.addOrChangeResultMessage = enrollmentHistoryService.getEnrollmentEditToolErrorMessage(errCode, errMsg);
			if(errCode === 200){
				backToPremiumHistory(true);
			}
			else{
				$('#addOrChangeResultModal').modal({
					backdrop: 'static',
					keyboard: false
				});
				$scope.loader = false;
			}
		}
		
		function updateSubscriberTxnType(){
			vm.enrollment.enrolleeDataDtoList.forEach(function(enrollee){
				enrollee.txnType = vm.enrollmentLevelTxnType;
				if(vm.enrollmentLevelTxnType == "CHANGE"){
					enrollee.txnReason = "29";
				}
				if(vm.enrollmentLevelTxnType == "TERM"){
					enrollee.txnReason = "AI";
				}
//				else{
//					if(enrollee.relationshipToSubscriber === 'Self'){
//						enrollee.txnType = vm.enrollmentLevelTxnType;
//					}
//				}
			})
		}
		
		function setIsTermReasonNonPaymentFlag(){
			console.log('Set IsTermReasonNonPaymentFlag');
			vm.enrollmentLastPremPaidThroughDateUI="";
			vm.isTermReasonNonPayment = false;
			vm.enrollment.enrolleeDataDtoList.forEach(function(enrollee){
				if(enrollee.relationshipToSubscriber === 'Self' &&  enrollee.txnReason === '59'){
					vm.isTermReasonNonPayment = true;
					vm.addOrChangeSendIn834 = false;
				}
			})
		}
		
		function updateTransactionReasonForNonSubscribers(){
			var txnReason = 'AI'; 
			vm.enrollment.enrolleeDataDtoList.forEach(function(enrollee){
				if(enrollee.relationshipToSubscriber === 'Self'){
					txnReason = enrollee.txnReason;
				}
			})
			vm.enrollment.enrolleeDataDtoList.forEach(function(enrollee){
				enrollee.txnReason = txnReason ;
			})
		}
		
		function setTxnReason(enrollee){
			if(enrollee.txnType == "ADD"){
				enrollee.txnReason = "EC";	
			}else if(enrollee.txnType == "CHANGE"){
				enrollee.txnReason = "29";	
			}else{
				enrollee.txnReason = "AI";	
			}
		}
		
		function validateLastPremPaidThroughDate(){
		 console.log(vm.enrollmentLastPremPaidThroughDateUI);
		 var lastPremDate = stringToDate(vm.enrollmentLastPremPaidThroughDateUI)
		 if(null != lastPremDate && (lastPremDate < new Date(vm.enrollment.enrollmentBenefitEffectiveStartDateUI) || lastPremDate > new Date(vm.enrollment.enrollmentBenefitEffectiveEndDateUI))){
			 console.log("invalid");
			$scope.addOrChangeEnrollmentTxnForm.enrollmentLastPremPaidThroughDateUI.$setValidity('lastPremDateWithinCoverage', false);
		 } else {
			  console.log("valid");
			 $scope.addOrChangeEnrollmentTxnForm.enrollmentLastPremPaidThroughDateUI.$setValidity('lastPremDateWithinCoverage', true);
		 }
		}
		
		function stringToDate(_date){
			if(_date != undefined){	
				var month = parseInt(_date.substring(0,2))-1;
				var day = _date.substring(2,4)
				var year = _date.substring(4,8);
				return new Date(year,month,day);
			}
			return null;
		}		
	}
})();