<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page isELIgnored="false"%>

<div class="gutter10">
	<div class="row-fluid">
		<ul class="page-breadcrumb">
			<li><a href="#">&lt; <spring:message code="label.back" /></a></li>
			<li><a href="<c:url value="/ticketmgmt/ticket/ticketlist/" />"><spring:message code="lbl.viewTickets" /></a></li>
			<li><spring:message code="lbl_tk.summary" /></li>
		</ul>
		<div class="row-fluid">
			<h1>Ticket Pending Report</h1>
		</div>

		<div class="row-fluid " id="searchFilterParentDiv">
			<div class="span12 gray" id="rightpanel">
				<div class="header ">
					<h4 class="pull-left" id="pageTitle">Search Filters</h4>
					<a href="#" class="btn btn-small btn-primary pull-right" id='btnShowHide' onclick="javascript:showHideFilters()"
						data-toggle="modal"> + </a> <a class="pull-left"
						style="font-size: 14px; color: red" id="tkmErrorDiv"> <c:if
							test="${errorMsg != null && errorMsg != ''}">
							<p>
								<c:out value="${errorMsg}"></c:out>
							<p />
							<c:remove var="errorMsg" />
						</c:if>
					</a>
				</div>

				<!-- class="form-vertical gutter10 lightgray" -->
				<div id="searchFilterDiv" class="toggle hide">
					<form method="POST"	action="<c:url value="/ticketmgmt/ticket/pendingreport" />" id="ticketSearch" name="ticketSearch" class="form-vertical gutter10" autocomplete="off">
					<df:csrfToken/>
							<jsp:include page="ticketPendingReportFilter.jsp"></jsp:include>
					</form>
				</div>
			</div>
		</div>
		
		<div class="clearfix">
			<div class="row-fluid" id="rightpanel">
				<div class="span9" id="rightpanel">
					<div class="span12 gray" id="rightpanel">
	    				<div class="header ">
	    					<h4 class="pull-left" id="pageTitle"> Report </h4>
	    				</div>
    				</div>
					<div style="font-size: 14px; color: red" id="tkmErrorDiv">
						<c:if test="${errorMsg != null && errorMsg != ''}">
							<p>	<c:out value="${errorMsg}"></c:out>	</p>
							<c:remove var="errorMsg" />
						</c:if>
					</div>
					<div class="row-fluid">
						<div class="span12">
						<table class="table table-striped margin10-t">
						<thead>
						<tr class="header">
							<th style="width: 120px;"> Assignee Name </th>
							<th style="width: 200px;"> Workgroup(s) </th>
							<th style="width: 80px;"> Open Tickets </th>
							<th> 0-7 Days </th>
							<th> 8-14 Days </th>
							<th> 15-30 Days </th>
							<th> 31-45 Days </th>
							<th> 45+ Days </th>
						</tr>
						</thead>
						<tbody>
						<c:choose>
					    <c:when test="${recordSize >0}">
						<c:choose>
                            <c:when test="${fn:length(detailList) > 0}">
						<c:forEach items="${detailList}" var="data" varStatus="count">
							<tr>
								<td> <a href="<c:url value="/ticketmgmt/ticket/ticketlist?ticketStatus=Open&assignee=${data.assigneeName}&assigneeId=${data.assigneeId}" />" > ${data.assigneeName} </a>  </td>								
								<td>
									<c:forEach items="${fn:split(data.queueName, ',')}" var="queue" >
										<c:set var="initQueuetxt" value="${queue}"/>
											<c:choose>											 
												<c:when test="${fn:contains(initQueuetxt, 'Workgroup')}">
													<c:set var="finalQueuetxt" value="${fn:replace(initQueuetxt, 'Workgroup', '')}" />
												   	<c:out value="${finalQueuetxt}" />
												</c:when>
												<c:otherwise>
													<c:out value="${queue}" />
												</c:otherwise>
											</c:choose>
										 <br> 
									</c:forEach>  
								</td>
								<td> <a href="javascript:openTicket('Open','${data.assigneeName}','${data.assigneeId}')" hrefxxx="<c:url value="/ticketmgmt/ticket/ticketlist?ticketStatus=Open&assignee=${data.assigneeName}&assigneeId=${data.assigneeId}" />" > ${data.openCount } </td>
								<td> ${data.recBetwn_0_7} </td>
								<td> ${data.recBetwn_8_14} </td>
								<td> ${data.recBetwn_15_30} </td>
								<td> ${data.recBetwn_31_45} </td>
								<td> ${data.recAbove_45} </td>
							</tr>							
						</c:forEach>
						</c:when>
                            <c:otherwise>
                               <h4 class="alert alert-info margin0"><spring:message  code='label.norecords'/></h4>
                            </c:otherwise>
                        </c:choose>	
                        </c:when>
                        
                       
				</c:choose>
						
						</tbody>
						</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--  end of row-fluid -->
	</div>
	<!--  end of gutter10 -->
	</div>
	
	<script type="text/javascript">
	function showHideFilters() {
		$( "#searchFilterDiv" ).slideToggle( "slow", function() {
		    // Animation complete.
			if ($('#searchFilterDiv').css('display') == 'none') {
				$("#btnShowHide").text("+");
			} else {
				$("#btnShowHide").text("-");
			}
		 });
	}
	
	var availableAssignee = new Array();
	
	function fetchAssigneeList() {
		if(availableAssignee.length == 0) {
		var validateUrl = 'fetchassigneelist';
		var assigneeName = $("#assignee").val();
		var csrftoken = '${sessionScope.csrftoken}';

		if(assigneeName != '' && assigneeName != 'undefined'){
			
			if(assigneeName.length >=2){
								
				$.ajax({
					url : validateUrl,
					type : "POST",
					data : {
						assigneeName : assigneeName,
						csrftoken :csrftoken
					},
					success : function(response) {
						autoCompleteAssignee(response);
					},
					error : function(e) {
						console.log("Fetch Assignee List Response Error: "+response);
					}
				});		
			}	
		}
		
	  }	else {
		  
		  autoCompleteBox();
	  }

	}
	
	function autoCompleteAssignee(userListJSON) 
	{
		userListJSON = JSON.parse(userListJSON);
		var index = 0;
		for ( var key in userListJSON) {
			availableAssignee[index] = {
				label : userListJSON[key],
				idx : key
			};
			index++;
		}
		
		autoCompleteBox();
	}

	function autoCompleteBox() {
		$("#assignee").autocomplete({
			source : availableAssignee,
			select: function(event, ui) { AutoCompleteSelectHandler(event, ui)}
		});
	}
	$('#assignee').bind('focusout',function(){
		var element = getItemByLabel(availableAssignee, $(this).val())
		if(element != null){
			$("#assigneeId").val(element.idx);
		}else{
			$("#assigneeId").val(0);
		}	
		
	});
	
	function getItemByLabel(anArray, label) {
	    for (var i = 0; i < anArray.length; i += 1) {
	        if (anArray[i].label === label) {
	            return anArray[i];
	        }
	    }
	}
	
	function AutoCompleteSelectHandler(event, ui)
	  {        
	      $("#assigneeId").val(ui.item.idx);
	  }

var openTicket=function(a,b,c)
{
    $("#reportPoster #ticketStatus").val(a);
    $("#reportPoster #assignee").val(b);
    $("#reportPoster #assigneeId").val(c);
    $("#reportPoster").submit();
};
	</script>
<form id="reportPoster" method="POST" action="<c:url value="/ticketmgmt/ticket/ticketlist" />?fromPendingReport=true">
<input type="hidden" name="ticketStatus" id="ticketStatus" />
<input type="hidden" name="assignee" id="assignee" />
<input type="hidden" name="assigneeId" id="assigneeId" />
<df:csrfToken/>
</form>