import React, { Fragment, useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Components

import Button from '@cmsgov/design-system-core/dist/components/Button/Button';

// ActionCreators

import { hideModal } from '../../actions/actionCreators';


// Constants

import CONFIG from '../../.env';



const SessionRestorationModal = props => {

    const {
        modal: {
            modalOwner,
            data: logoutUrl,
            callbacks = []
        }
    } = props;

    const [ counter, setCounter ] = useState(30);


    useEffect(() => {
        const interval = setInterval(() => {
            setCounter(counter => counter - 1);
        }, 1000);
        return () => {
            clearInterval(interval);
        };
    }, []);

    const handleLeavingApplication = () => {
        window.location = logoutUrl;
    };

    const handleRestorationClick = () => {
        if(CONFIG.ENV.DEVELOPMENT) {
            props.actions.hideModal(modalOwner);
        } else {
            const [ restoreSession ] = callbacks;
            restoreSession();
        }
    };

    const isTimeOver = counter < 0;

    return (
        <Fragment>
            <div className="modal__header" id="modal__session_restoration">
                <h2>
                Your login session has expired.
                </h2>
            </div>
            <div className="modal__body txt-left">
                {
                    !isTimeOver ?
                        <Fragment>
                            <p>
                                You will be automatically logged out in ... &nbsp;
                                <strong>
                                    { counter }  &nbsp;
                                </strong>
                                seconds!
                            </p>
                            <p>
                                {'Click "Continue" to keep working with the application'}.
                            </p>
                        </Fragment> :
                        <Fragment>
                            <p>
                                You have been automatically logged out for security purposes.
                                You will have to log in again.
                            </p>
                        </Fragment>
                }

            </div>
            <div className='modal__controls'>
                {
                    !isTimeOver ?
                        <Button
                            className='modal-confirm-btn'
                            onClick={handleRestorationClick}
                        >
                            Continue
                        </Button> :
                        <Button
                            className='modal-confirm-btn'
                            onClick={handleLeavingApplication}
                        >
                            Go to Log in
                        </Button>
                }
            </div>
        </Fragment>
    );
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        hideModal
    }, dispatch)
});

export default connect(null, mapDispatchToProps)(SessionRestorationModal);