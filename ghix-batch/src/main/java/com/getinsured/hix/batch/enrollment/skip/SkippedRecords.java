package com.getinsured.hix.batch.enrollment.skip;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;


@Component("skippedRecords")
public class SkippedRecords {

	Map<String,String> skippedHouseholdMap=null;

	public SkippedRecords() {
		skippedHouseholdMap= new HashMap<>();
	}

	public synchronized void putToSkippedHouseholdMap(String key, String value){
		skippedHouseholdMap.put(key, value);
	}
	public synchronized void putAllToSkippedHouseholdMap(Map<String,String> failedHouseholdMap){
		this.skippedHouseholdMap.putAll(failedHouseholdMap);
	}	
	
	public Map<String,String> getSkippedHouseholdMap(){
		return skippedHouseholdMap;
	}
	
	public synchronized void clearSkippedHouseholdMap(){
		this.skippedHouseholdMap.clear();
	}	
}
