package com.getinsured.ssap.model;

import java.io.IOException;
import java.util.Date;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.ssap.json.DateConfiguration;

/**
 * POJO Describing save request.
 *
 * @author Yevgen Golubenko
 * @since 2019-01-04
 */
public class SaveRequest
{
  private String application;
  private SingleStreamlinedApplication ssapApplication;
  private long applicationId;
  private String currentPage;
  private boolean mailingAddressUpdated;
  private String sepEvent;
  private Date sepEventDate;
  private int coverageYear;
  private String applicationSource;
  private boolean csrOverride;
  private String remoteClientAddress;

  public String getRemoteClientAddress() {
    return remoteClientAddress;
  }

  public void setRemoteClientAddress(String remoteClientAddress) {
    this.remoteClientAddress = remoteClientAddress;
  }

  public boolean isCsrOverride() {
    return csrOverride;
  }

  public void setCsrOverride(boolean csrOverride) {
    this.csrOverride = csrOverride;
  }

  public String getApplication()
  {
    return application;
  }

  public void setApplication(String application)
  {
    this.application = application;
    DateConfiguration dc = new DateConfiguration();
    ObjectMapper mapper = dc.objectMapper();

    try
    {
      setSsapApplication(mapper.readValue(this.application, SingleStreamlinedApplication.class));
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }

  public SingleStreamlinedApplication getSsapApplication()
  {
    if(this.ssapApplication == null && this.application != null) {
      this.setApplication(this.application);
    }
    return ssapApplication;
  }

  public void setSsapApplication(final SingleStreamlinedApplication ssapApplication)
  {
    this.ssapApplication = ssapApplication;
  }

  public long getApplicationId()
  {
    return applicationId;
  }

  public void setApplicationId(long applicationId)
  {
    this.applicationId = applicationId;
  }

  public String getCurrentPage()
  {
    return currentPage;
  }

  public void setCurrentPage(final String currentPage)
  {
    this.currentPage = currentPage;
  }

  public boolean isMailingAddressUpdated()
  {
    return mailingAddressUpdated;
  }

  public void setMailingAddressUpdated(boolean mailingAddressUpdated)
  {
    this.mailingAddressUpdated = mailingAddressUpdated;
  }

  public String getSepEvent()
  {
    return sepEvent;
  }

  public void setSepEvent(String sepEvent)
  {
    this.sepEvent = sepEvent;
  }

  public Date getSepEventDate()
  {
    return sepEventDate;
  }

  public void setSepEventDate(Date sepEventDate)
  {
    this.sepEventDate = sepEventDate;
  }

  public int getCoverageYear()
  {
    return coverageYear;
  }

  public void setCoverageYear(int coverageYear)
  {
    this.coverageYear = coverageYear;
  }

  public String getApplicationSource()
  {
    return applicationSource;
  }

  public void setApplicationSource(String applicationSource)
  {
    this.applicationSource = applicationSource;
  }
}
