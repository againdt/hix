﻿define(function (require) {
    var button = new baseComponent();
    button.type = "button";
    button.getHTML = function (cgfObj) {
        var defCfg = {
            etype:"button"
        };
        cgfObj = $.extend(defCfg, cgfObj);
        var thisType = cgfObj.etype;
        var compData = cgfObj;
        var $button = $("<button type=\"" + thisType + "\" >" + compData.caption + "</input>");
        if (compData.click) {
            $button.attr("ng-click", compData.click);
        };
        $button.attr("id", compData.name);
        $button.addClass("btn");
        $button.addClass(compData.css);
        return $button[0].outerHTML;
    };
    return button;
});