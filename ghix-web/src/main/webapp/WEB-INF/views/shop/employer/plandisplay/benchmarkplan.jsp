<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld" prefix="util"%>

<script>
var ctx = "<%=request.getContextPath()%>";
</script>

<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/shopstyles.css"/>" />

<style>
.selected {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: none repeat scroll 0 0 rgb(236, 236, 236);
    background-image: none;
    background-repeat: repeat-x;
    border-color: rgb(230, 230, 230) rgb(230, 230, 230) rgb(191, 191, 191);
    border-image: none;
    border-style: solid;
    border-width: 1px;
    color: rgb(90, 90, 90);
    cursor: pointer;
    font-weight: normal;
    text-shadow: none;
	border-radius: 4px 4px 4px 4px;
	box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
	display: inline-block;
	line-height: 20px;
	text-align: center;
	vertical-align: middle;
}

.scroll-left-arrow-wrapper, .scroll-right-arrow-wrapper {
	width: 26px;
	background: #f3f3f3;
	padding: 82px 4px;
	cursor: pointer;
}
.scroll-left-arrow-wrapper {
	float: left;
	margin-left: -11px;
}
.scroll-right-arrow-wrapper {
	float: left;
	margin-right: -11px;
	border-left: solid 1px #e2e2e2;
}
.view-plans {
	width: 90.9%;
	margin: 0 32px;
}

.plans-wrap {
	float: left;
	background: url(/hix/resources/img/teeth.png) repeat-x left bottom;
	height: auto;
	position:relative;
	margin-bottom:20px;
}
h1.nextprev {
    color: #ffffff;
    font-size: 2.8em;
    line-height: 2.8em;
}
.scroll-left-arrow{
	margin-left:-4px;
}
.scroll-right-arrow {
    margin-left: -4px;
}
.plans-top-wrapper {
	z-index: 2;
	text-align:center;
	width:699px;
}

.plans-container {
	margin-top: -10px;
}
#planSelect div.plan:nth-child(4n) {
	border-right: 0px !important;
}
div.plans-top-wrapper.span6 {
	width:48.317948718%;
}
.scroll-left-arrow-wrapper {
	margin-left:0px;
}

.plans-wrap {
	width: 89.8%;
}
.plans-section-content .plan {
	float: left;
	width: 204px;
	position: relative;
}
#titlebar {
	margin-bottom: 0 !important;
}	
#rightpanel .pagination { margin: 0 !important;}
.table th, .table td, .view-plans .table td {
	border:0px;
	vertical-align:bottom;
}
.dedictible .table td {
	border-bottom: 1px solid #E2E2E2;
}
div.view-plans .plan:nth-child(2n) {
	border-left: 1px solid #E2E2E2;
	border-right: 1px solid #E2E2E2;
}
div.plans-section-content .plan {
	border-left: 1px solid #E2E2E2;
	
}



.highcharts-legend,.highcharts-axi {
	display: none !important;
	
}

</style>

<div class="gutter10" id="benchmark">
<div class="row-fluid">
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="accordion graysidebar" id="accordion2">
				<util:nav pageName="${page_name}"></util:nav>
			</div>
			<util:selection pageName="${page_name}"></util:selection>
			
			<div class="header margin10-t cahide phixhide">
				<h4>Quick Links</h4>
			</div>
			<ul class="nav nav-list cahide phixhide"> 
				<li><a href="<c:url value='/faq/employer'/>"><i class="icon-search"> </i><spring:message code = "label.learnMoreAboutReferencePlans"/></a></li>
			</ul> 
		</div>
		<!--sidebar-->
		<div class="span9" id="rightpanel">
			<h4 class="graydrkbg gutter10">
				<a href="<c:url value="/shop/employer/plandisplay/benchmarkinfo" />" class="btn"> <spring:message code = "label.back"/></a>
            	<span class="offset1"><spring:message code = "label.selectRefPlan"/></span>
            	<a href="#" class="btn btn-primary setBenchmarkPlan pull-right"> <spring:message  code="label.next"/> </a>
       		</h4>
            <div class="gutter10"><p><spring:message code = "label.setBaseLine"/></p></div>
		<div class="plans-top-wrapper">	
			<div id="planSelect" class="tabbedSections">				
					<div class="row-fluid" id="titlebar">
						<div class="gutter10">
							<div id="plansbar">
								<h3 class="pull-left txt-left span3" id="Nplans">
									<div id="totalPlanCount">${planCount}</div>
									<spring:message code = "label.plans"/>
								</h3>
								<!--PAGINATION  -->
								<div class="pagination pull-left">
									<ul id="pagination">
									</ul>
								</div>
								<!--SORT  -->
								<span id="sort">
								</span> &nbsp; &nbsp;
								<!-- pull-right -->
							</div>
							<!--plansbar-->
						</div>
						<!--gutter10-->
					</div>
					<!--titlebar-->

					<div class="scroll-left-arrow-wrapper">
						<div class="scroll-left-arrow">
							<h1 id="prevItem" class="nextprev"><i class="icon-chevron-left"></i></h1>
						</div>
					</div>
					<!-- MAIN PLAN SUMMARY SECTION  -->
					<div class="plans-header plans-section plans-wrap"
						id="plans-header">
						<div class="plans-header-content-wrapper">
							<div class="plans-section-content">
								<input type="hidden" name="benchmarkPlan" id="benchmarkPlan" value="${benchmarkPlan}" />
								<input type="hidden" name="maxEmployerCost" id="maxEmployerCost" value="" />
              					<input type="hidden" name="maxEmployeeCost" id="maxEmployeeCost" value="" />
              					
								<div id="summary" class="��well" style="background: none;">
								<h3 class="green"><spring:message code = "label.processingMsg"/></h3>
								<img src="<c:url value="/resources/img/loader_greendots.gif"/>" alt="loader dots">
								</div>
								
								<div id="summary_err" style="display: none;">
								<div class="alert-info margin30 gutter20">
								<p><spring:message code = "label.changePreferances"/></p>
								</div>
							</div>
							<!-- plans-section-content-->
						</div>
						<!--plans-header-content-wrapper-->
					</div>
					</div>
				</div>
					<!--plans-section plans-header-->
					<!-- PLAN NAVIGATION  -->
					<div class="scroll-right-arrow-wrapper">
						<div class="scroll-right-arrow">
							<h1 id="nextItem" class="nextprev"><i class="icon-chevron-right"></i></h1>
						</div>
					</div>
				
				<!-- End of top bar -->
				<table class="view-plans">
					<tbody>
						<tr>
							<td class="center-container row-fluid">
								<div class="plans-container">
									<div class="plans-section row-fluid" id="plans-cost">
										<div class="plans-section-header">
											<h4 data-toggle="collapse" data-target="#row1" class="collapsetoggle">
											<i class="icon-chevron-right"></i>
												<spring:message code = "label.monthlyCostToYou"/>  <small><spring:message code = "label.basedOnContriProposed"/> ${emplrContributionEmployee}% <spring:message code = "label.and"/> ${emplrContributionDependent}%  )</small> 
													<a href="javascript:void(0)" rel="tooltip" data-placement="top"
													data-original-title="<spring:message code = "label.costCaldesc1"/>"
													class="info"><div class="icon-question-sign"></div></a>
											</h4>
										</div>
										<!-- <div class="plans-section-info"></div> -->
										<div style="height: auto;" class="plans-section-content row-fluid in collapse" id="row1">
											<div id="employerCost"></div>
										</div>
										<!--plans-section-content-->
									</div>
									<!--plans-section row-fluid-->
									<div class="plans-section  row-fluid" id="plans-cost1">
										<div class="plans-section-header">
											<h4 data-toggle="collapse" data-target="#row2" class="collapsetoggle">
											<i class="icon-chevron-right"></i>
												<spring:message code = "label.costToEachEmp"/> <small>(<spring:message code = "label.basedOnContri"/> ${emplrContributionEmployee}% <spring:message code = "label.towardsEmp"/>)</small>
												<a href="javascript:void(0)" rel="tooltip" data-placement="top"
									data-original-title="<spring:message code = "label.costCaldesc2"/>" 
									class="info"><div class="icon-question-sign"></div></a> 
											</h4>
										</div>
										<!-- <div class="plans-section-info"></div> -->
										<div style="height: auto;" class="plans-section-content row-fluid in collapse" id="row2">
											<div id="employeeCost"></div>
											<!--plans-section-content-->
										</div>
									</div>
									<!--plans-section row-fluid-->
									<div class="plans-section dedictible row-fluid">
										<div class="plans-section-header">
										<h4 data-toggle="collapse" data-target="#row3" class="collapsetoggle">
									    </h4>
										</div>
										<!--plans-section-content-->
									</div>
									<!--plans-section row-fluid-->
								</div> <!--plans-container-->
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!--gutter10-->
		</div>
		<!--rightpanel-->
	</div>
	<!--row-fluid-->
</div>
<!--main-->
</div>

               <div id="missinginfo" class="modal hide fade"  tabindex="-1" role="dialog" aria-labelledby="childhomeaddress" aria-hidden="true">
                  <div class="modal-header"></div>
                  <div class="modal-body">
                    <h3 class="green"><spring:message code = "label.processingMsg"/></h3>
					<img src="<c:url value="/resources/img/loader_greendots.gif"/>" alt="loader dots">
                  </div>
                  <div class="modal-footer"></div>
                </div>
                <!-- /modal -->

<!--sample template for result item-->
<script type="text/html" id="resultItemTemplateSummary">
			<table class="plan plan-<@=planId@>">
			  <tbody>
			    <tr height='100px'>
                  <td class="vertical-align-center" align='center'>
					<div class="logowrap">	
						<@ if (issuer.length > 24) {@>
          					<img class="carrierlogo hide" src='<@=issuerLogo@>' alt="<@=issuer.substr(0,24)@>..."  onload="imgSize()" />
        				<@ } else { @>          
							<img class="carrierlogo hide" src='<@=issuerLogo@>' alt="<@=issuer@>" onload="imgSize()" />
        				<@ } @>
					</div>
				  </td>
				</tr>
				<tr>
                  <td class="planNameWrap">
				    <p><a tabindex="-1" href="#" data-placement="top" rel="tooltip" data-original-title="<@=name@>">
          			  <@ if (name.length > 60) {@>
          			    <@=name.substr(0,60)@>...
         				   <@ } else { @>
          				    <@=name@>
         				<@ } @></a></p>
			      </td>
			    </tr>
				<tr>
				  <td>
				    <h4 class="premium"><small><spring:message code = "label.avgEmpPrem"/> </small>$<@=indvPremium @> / <spring:message code = "label.month"/></h4></td>
				</tr>
				<tr>
				  <td>
                    <div class="actions">
	                  <a href="javascript:void(0)" class="btn btn-primary selectPlan" id="plan_<@=planId@>" name="selectPlan"><span class="aria-hidden"><spring:message code = "label.select"/> <@=name@> <spring:message code = "label.avgEmpPrem"/> is $<@=indvPremium @> per <spring:message code = "label.month"/></span> <spring:message code = "label.select"/>  </a>
					  <a href="javascript:void(0)" class="btn btn-primary active removePlan" id="removeplan_<@=planId@>" style="display:none;" name="removePlan"> <span class="aria-hidden"><spring:message code = "label.selected"/> <@=name@> <spring:message code = "label.avgEmpPrem"/> is $<@=indvPremium @> per <spring:message code = "label.month"/></span> <spring:message code = "label.selected"/></a>
                    </div>
                  </td>
			    </tr>
			  </tbody>
			</table>			
</script>
<script type="text/html" id="resultItemTemplateEmployerCost">
<div class="plan plan-<@=id @>">
    <table class="table ">
      <tbody>
        <tr>
          <td>
				<div id="Employer_<@=planId@>" style="min-width: 140px; min-height: 180px; margin: 0 auto"></div>
	  	  </td>
        </tr>
      </tbody>
    </table>
</div>
</script>
<script type="text/html" id="resultItemTemplateEmployeeCost">
<div class="plan plan-<@=id @>">
    <table class="table ">
      <tbody>
        <tr>
          <td>
				<div id="Employee_<@=planId@>" style="min-width: 140px; height: 170px; margin: 0 auto"></div>
	 	  </td>
        </tr>
      </tbody>
    </table>
</div>
</script>
<script type="text/html" id="tmpPagination">
			<@ if (page != 1) { @>
				<li><a href="#" class="prev" data-original-title=""><span aria-hidden="true">&#8249;</span> <span class="aria-hidden">Previous page</span></a></li>
			<@ } @>
			<@ _.each (pageSet, function (p) { @>
				<@ if (page == p) { @>
					<li class="active"><a href="#" class="page" data-original-title=""><span class='aria-hidden'>Page </span> <@= p @> <span class='aria-hidden'> Current Page </span></a></li>
				<@ } else { @>
					<li><a href="#" class="page" data-original-title="" id="<@=p@>"><span class='aria-hidden'>Page </span> <@= p @></a></li>
				<@ } @>
			<@ }); @>
			<@ if (lastPage != page) { @>
				<li><a href="#" class="next" data-original-title=""><span aria-hidden="true">&#8250;</span> <span class="aria-hidden">next page</span></a></li>
			<@ } @>
		
</script>

<script type="text/html" id="tmpSort">
<div class="dropdown">
	<a href="#" data-target="#" data-toggle="dropdown" role="button" id="sort_link" class="btn btn-small dropdown-toggle"><spring:message code = "label.sortBy"/><b class="caret"></b> <span class="aria-hidden"> Dropdown Menu. Press enter to open it and tab through its options</span></a>
	<ul class="dropdown-menu txt-left" id="sort_dropdown">
       <li><a href="#" class="sort_indvPremium_asc" id="indvPremium_asc"><spring:message code = "label.priceLowHigh"/></a></li>
       <li><a href="#" class="sort_indvPremium_desc" id="indvPremium_desc"><spring:message code = "label.priceHighLow"/></a></li>
       <li><a href="#" class="sort_issuer" id="issuerText"><spring:message code = "label.insurer"/><span id="issuerTextOrder"><span> <span class='aria-hidden'> End of dropdown</span></a></li>
    </ul>
</div>														
</script>
<script src="<c:url value="/resources/js/underscore-min.js" />"></script>
<script src="<c:url value="/resources/js/backbone-min.js" />"></script>
<script src="<c:url value="/resources/js/json2.js" />"></script>
<script src="<c:url value="/resources/js/plandisplay/showplans.js" />"></script>

<!--Backbone.Paginator-->
<script src="<c:url value="/resources/js/backbone-pagination.js" />"></script>

<!--Models/Collections-->
<script src="<c:url value="/resources/js/plandisplay/models/plan.js" />"></script>
<script	src="<c:url value="/resources/js/plandisplay/collections/PaginatedCollection.js" />"></script>

<!--Views-->
<script src="<c:url value="/resources/js/plandisplay/views/PlanView.js" />"></script>
<script src="<c:url value="/resources/js/plandisplay/views/PaginationView.js" />"></script>
<script	src="<c:url value="/resources/js/plandisplay/views/SortView.js" />">
</script>
<script type="text/javascript" src="<c:url value="/resources/js/highcharts.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/exporting.js" />"></script> 
<script>
$(document).ready(function() {
	$('.actions a').click(function(){
	    if($(this).text() == "Selected"){
	        $(this).removeClass('.btn');
	    }
	});
});
</script>	
<script type="text/javascript">
	$(window).load(function() {
		$('.view-plans').animate({
			opacity : 1,
			right : 0
		}, 650);
	});

	$(document).ready(function() {
	
		$.ajaxSetup({
			cache : false
		});

		$('body').tooltip({
		    selector: '[rel=tooltip]'
		});

				
		$('div#summary.well').find('.plans').css({
			"font-style" : "italic",
			"font-weight" : "bolder"
		});
		
			$('.plans-section-header').on('click show hide',
			function(e) {
				$(e.target).find('i').toggleClass('icon-chevron-right icon-chevron-down', 200);
			});
			
	});	
    
	function toggleSort() {
		$('#planSelect .dropdown').addClass('open');
	}
	
	function drawEmployerBar(barId, dependentCost,EmployeeCost,maxVal){
		chart = new Highcharts.Chart({
            chart: {renderTo: barId,
	            		type: 'column', 
	            		spacingBottom: 0, 
	            		marginBottom: 20, 
	            		marginTop: 5,
	            		events: {
		    	            load: function(event) {
		    	            	var id = $(event.target.container).attr('id')
		    	            	$('.plan .table').each(function(){
		    	            		var firstValue = $('#'+id).find('.highcharts-data-labels:first').find('text').find('tspan').text(),
										firstValueIE = $('#'+id).find('.highcharts-data-labels:first').find('span').text();/*used for IE browsers*/
		    	            		if (firstValue == '$0' || firstValueIE == '$0'){
		    	            			$('#'+id).find('.highcharts-data-labels:first').find('text').find('tspan').text('');
										$('#'+id).find('.highcharts-data-labels:first').find('span').text('');/*used for IE browsers*/
		    	            		}
		    	            	});
		    	            }
		    	        }  
            		},
			credits: { enabled: false },
			title: { text: '' },
			legend:  { enabled: false },
			exporting:{ buttons: {
                printButton:{ enabled:false },
                exportButton: { enabled:false }
              }
         	},
            xAxis: { categories: ['']},
			colors: [ '#99CC00','#74B33C' ],
            yAxis: {min: 0, max: maxVal,  title: { text: '' }, gridLineWidth: 0, gridLineColor: '#ffffff', labels: { enabled: false },
            	stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                },
                formatter: function() {
                	return formatCurrency(this.total);
                }
            }},
           
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b>'+
                        this.series.name +': '+ formatCurrency(this.y);
                }
            },
            plotOptions: {
                column: {stacking: 'normal', borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        formatter: function() {
                        	return formatCurrency(this.y);
                        }
                    }
                }
            },
            series: [{
                name: 'Dependents', shadow: false,
                data: [dependentCost]
            },  {
                name: 'Employee', shadow: false,
                data: [EmployeeCost]
            }]
        });
		
		//for accessibility		
		var planName= $(".plan-"+barId.replace(/^\D+/g,'')).find("a[data-original-title]").attr("data-original-title");  
		var HighChartSpan = $("#"+barId).find(".highcharts-stack-labels tspan");
		HighChartSpan.prepend("<span class='aria-hidden'>"+ planName +" Plan monthly costs you " + HighChartSpan.text() +", " + dependentCost + " for depentents and "+ EmployeeCost +"for employee</span>"); 
	}
	
	function drawEmployeeBar(barId, val,maxVal){
		  
		  chart = new Highcharts.Chart({
		    chart:   { 
		    			renderTo: barId, 
		    			type: 'column', 
		    			spacingBottom: 0, 
		    			marginBottom: 20, 
		    			marginTop: 5		    			
		    		},
		    credits: { enabled: false },
		    legend:  { enabled: false },
		    exporting:{ buttons: {
		                 printButton:{ enabled:false },
		                 exportButton: { enabled:false }
		               }
		          },
		    title: { text: '' },
		    xAxis: { categories: ['Employee Cost'] },
		    colors: ['#67c2c2'],
		    yAxis: { min: 0, max: maxVal, title: { text: '' }, gridLineWidth: 0, gridLineColor: '#ffffff', labels: { enabled: false } },
		    plotOptions: { 
		      column: { pointPadding: 0.5, borderWidth: 0, pointWidth: 56, colorByPoint: true, color: '#009999' }
		    },
		    tooltip: {
                formatter: function() {
                    return this.series.name +': '+ formatCurrency(this.y);
                }
            },
		    series: [{name: 'EmployeeCost', shadow: false, color: '#009999', data: [val], dataLabels: {
		    enabled: true,
		    formatter: function() {
		       return formatCurrency(this.y);
		    }
		 }    }]
		  });
		  
		  //for accessibility
		  var planName= $(".plan-"+barId.replace(/^\D+/g,'')).find("a[data-original-title]").attr("data-original-title");  
		  var HighChartSpan = $("#"+barId).find(".highcharts-data-labels tspan");
		  HighChartSpan.prepend("<span class='aria-hidden'>"+ planName +" Plan monthly costs each employee " + HighChartSpan.text() + "</span>"); 

		  
		}
	
	function formatCurrency(num) {
		num = parseFloat(num).toFixed(0);
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
		return (((sign)?'':'-') + '$' + num);
	}

	function imgSize(){
	    $(".carrierlogo").each(function(){
	        var width = $(this).width();
	        var height = $(this).height();
	        var maxWidth = 160;
			var maxHeight = 60;
			
			// Check if the current width is larger than the max
		    if(width > maxWidth){
		        ratio = maxWidth / width; // get ratio for scaling image
		        $(this).width(maxWidth); // Set new width
		        $(this).height((height * ratio)); // Scale height based on ratio
		        height = height * ratio; // Reset height to match scaled image
		    	width = width * ratio; // Reset width to match scaled image
		    }
			
			// Check if current height is larger than max
			if(height > maxHeight){                                     
			  ratio = maxHeight / height; // get ratio for scaling image
			  $(this).height(maxHeight); // Set new height
			  $(this).width((width * ratio)); // Scale width based on ratio    
			}
		    
			$(this).css('display','block');
	    });
	}
</script>