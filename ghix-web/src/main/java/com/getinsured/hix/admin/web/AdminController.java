package com.getinsured.hix.admin.web;

import static com.getinsured.hix.planmgmt.service.IssuerService.ACCREDITATION_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.ADD_INFO_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.AUTHORITY_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.BENEFITS_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.BROCHURE_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.CERTI_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.DISCLOSURES_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.MARKETING_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.PLAN_SUPPORT_DOC_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.RATES_FOLDER;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.getinsured.hix.account.dto.ManageUser;
import com.getinsured.hix.account.util.UserChangeEmailManager;
import com.getinsured.hix.admin.service.AccountLockNotificationEmail;
import com.getinsured.hix.admin.service.UsersNotificationService;
import com.getinsured.hix.batch.BatchNodeUtil;
import com.getinsured.hix.consumer.repository.IHouseholdRepository;
import com.getinsured.hix.dto.account.AccountUserDto;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.GhixNoticeUser;
import com.getinsured.hix.model.GhixNotificationType;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.model.NoticesElements;
import com.getinsured.hix.model.NoticesTypesElements;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.RoleProvisioningRule;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.model.VimoEncryptor;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.NoticesTypesElementsService;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.RoleAccessControl;
import com.getinsured.hix.platform.security.duo.DuoAdminApi;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserRoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.security.session.SessionTrackerService;
import com.getinsured.hix.platform.security.tokens.CSRFToken;
import com.getinsured.hix.platform.security.tokens.CSRFTokenTag;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.Utils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.ws_trust.saml.GhixModuleAccessToken;
import com.google.gson.Gson;

/**
 * Handles requests for the application home page.
 */
@Controller
public class AdminController {

	private static final String LOGED_USER = "logedUser";
	private static final String PLATFORM_00004 = "PLATFORM-00004";
	private static final String SORT_ORDER = "sortOrder";
	private static final String MODEL_SORT_ORDER = "sort_order";
	private static final String ERROR_MSG = "errorMsg";
	private static final String ACC_ACTIVATION_LINK = "accountActivationLink";
	private static final String EMAIL = "email";
	private static final String LAST_NAME = "lastName";
	private static final String FIRST_NAME = "firstName";
	private static final String CHANGE_ORDER = "changeOrder";
	private static final String SORT_BY = "sortBy";
	private static final String PAGE_SIZE = "pageSize";
	private static final String ON = "ON";
	private final Logger logger = LoggerFactory.getLogger(AdminController.class);
	private static final String NOTICETYPE = "NoticeType";
	private static final int LISTING_NUMBER = 10;
	private static final Calendar OPEN_ENDED = new GregorianCalendar(2099, Calendar.MARCH, 31);
	private static final byte[]TEMPLATE_REVIEW_ERROR = "Could Not Generate Template Review. Template might be corrupted.".getBytes();
	private static final String TEMPLATE_PREVIEW  = "Template_Preview";
	private static boolean isScimEnabled = GhixPlatformConstants.SCIM_ENABLED;	
	
	@Autowired private NoticeTypeRepository noticeTypeRepo;
	@Autowired private UserService userService;
	@Autowired private GhixRestTemplate restTemplate;
	@Autowired private RoleService roleService;
	@Autowired private UsersNotificationService usersNotificationService;
	@Autowired private AccountActivationService accountActivationService;
	@Autowired private ContentManagementService ecmService;
	@Autowired private NoticeService noticeService;
	@Autowired private AccountLockNotificationEmail accountLockNotificationEmail;
	@Autowired private VimoEncryptor vimoencryptor;//Temp var.Remove me while removing ghixUserPasswordDetails() method.
	@Autowired private NoticesTypesElementsService noticesTypesElementsService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private UserChangeEmailManager userChangeEmailManager;
	@Autowired private RoleAccessControl acessControl;
	@Autowired private UserRoleService userRoleService;
	@Autowired private SessionTrackerService sessionTracker;
	@Autowired private IHouseholdRepository householdRepository;
	@Autowired private AppEventService appEventService;
	@Autowired private LookupService lookupService;
	@Autowired private SCIMUserManager scimUserManager;
	@Autowired private BatchNodeUtil batchNodeUtil;
	@Value("#{configProp['notice_engine_type']}") private String noticeEngineType;

	
	@RequestMapping(value = "admin/noticetypelist", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_MANAGE_NOTICES')")
	public String noticeTypeList(Model model, HttpServletRequest request) {
		
		Map<String, Object> searchCriteria = this.createSearchCriteria(request);
		Map<String, Object> notificationListAndRecordCount = noticeTypeRepo.searchNotification(searchCriteria);
		List<NoticeType> notifications = (List<NoticeType>) notificationListAndRecordCount.get("notificationlist");

		Long iResultCt = (Long) notificationListAndRecordCount.get("recordCount");
		int iResultCount = iResultCt.intValue();

		model.addAttribute("page_title", "Getinsured Health Exchange: Notification Listing");

		model.addAttribute("noticeEngineType", noticeEngineType);

		if (searchCriteria.get("name") == null
				&& searchCriteria.get("user") == null
				&& searchCriteria.get("type") == null
				&& searchCriteria.get("method") == null) {
			searchCriteria.put("hideTerminated", NoticeType.NoticeTypeBooleanFlag.NO);
		}

		model.addAttribute("notificationlist", notifications);
		model.addAttribute("searchCriteria", searchCriteria);
		model.addAttribute("resultSize", iResultCount);
		model.addAttribute(PAGE_SIZE, LISTING_NUMBER);
		model.addAttribute("submit", "submit");
		model.addAttribute(SORT_ORDER, searchCriteria.get(SORT_ORDER));
		model.addAttribute("usersList", GhixNoticeUser.values());
		model.addAttribute("typeStatusList", GhixNotificationType.values());
		model.addAttribute("methodsList", GhixNoticeCommunicationMethod.values());
		
		return "admin/noticetypelist";
	}

	@RequestMapping(value = "admin/noticetype", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_CREATE_NOTICES')")
	public String createNoticeType(Model model, HttpServletRequest request) {
		logger.info("Create Notice page");

		model.addAttribute("noticeEngineType", noticeEngineType);

		model.addAttribute("page_title", "Getinsured Health Exchange: Create Notice Type");
		model.addAttribute("basepath", "../resources/ckeditor/");
		model.addAttribute("languageList", GhixLanguage.values());
		model.addAttribute("usersList", GhixNoticeUser.values());
		model.addAttribute("typeStatusList", GhixNotificationType.values());
		model.addAttribute("methodsList", GhixNoticeCommunicationMethod.values());
		model.addAttribute("id", 0);
		model.addAttribute("Mode", "Add");

		return "admin/noticetype";
	}

	@RequestMapping(value = "admin/noticetype", method = RequestMethod.POST)
	public String createNoticeType(@ModelAttribute("NoticeType") NoticeType noticeType, BindingResult result, Model model) {

		
		NoticeType noticeTypeobj = noticeTypeRepo.findById(noticeType.getId());
		if(noticeType.getId() > 0)
		{
			
			noticeType.setCreated(noticeTypeobj.getCreated());
		}
		if (noticeType.getTerminationDate() == null) {
			Date theEndDate = OPEN_ENDED.getTime();
			noticeType.setTerminationDate(theEndDate);
		}
		
		if (noticeType.getEmailClass() == null) {
			noticeType.setEmailClass(noticeTypeobj.getEmailClass());
		}
		
		if (noticeType.getEmailFrom() == null) {
			noticeType.setEmailFrom(noticeTypeobj.getEmailFrom());
		}
		
		if (noticeType.getEmailTo() == null) {
			noticeType.setEmailTo(noticeTypeobj.getEmailTo());
		}
		
		if (noticeType.getEmailSubject() == null) {
			noticeType.setEmailSubject(noticeTypeobj.getEmailSubject());
		}


		noticeTypeRepo.save(noticeType);
		return "redirect:noticetypelist";
		
	
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "admin/noticetype/{id}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_EDIT_NOTICES')")
	public String edit(@PathVariable("id") Integer id, Model model, HttpServletRequest request) {
		logger.info("Edit Notice type page ");
		
		model.addAttribute("page_title", "Getinsured Health Exchange: Edit Role");
		model.addAttribute("basepath", "../../resources/ckeditor/");
		model.addAttribute("languageList", GhixLanguage.values());
		NoticeType noticeType = noticeTypeRepo.findById(id);
		model.addAttribute("noticeEngineType", noticeEngineType);
		if (noticeType == null) {
			return "redirect:noticetypelist";
		}
		model.addAttribute("noticeType", noticeType);
		
		model.addAttribute("effectiveDate", DateUtil.dateToString(noticeType.getEffectiveDate(), GhixConstants.REQUIRED_DATE_FORMAT));
		if (noticeType.getTerminationDate().equals(OPEN_ENDED.getTime())) {
			model.addAttribute("terminationDate", "");
		} else {
			model.addAttribute("terminationDate", DateUtil.dateToString(noticeType.getTerminationDate(), GhixConstants.REQUIRED_DATE_FORMAT));
		}
		model.addAttribute("usersList", GhixNoticeUser.values());
		model.addAttribute("typeStatusList", GhixNotificationType.values());
		model.addAttribute("methodsList", GhixNoticeCommunicationMethod.values());
		model.addAttribute("Mode", "Edit");

		return "admin/noticetype";
	}

	@Value("#{configProp['uploadPath']}") private String uploadPath;
	@Autowired private PlanMgmtService planMgmtService;

	@RequestMapping(value = "/admin/filedownload", method = RequestMethod.GET)
	public ModelAndView downloadFile(@RequestParam(value = "document", required = false) String docName, @RequestParam(value = "docType", required = false) String docType, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String filePath = getFilePath(docType, docName);
		File file = new File(filePath);
		response.setContentType("application/octet-stream");
		response.setContentLength((int) file.length());
		String uploadedFileName = planMgmtService.getActualFileName(docName);
		response.setHeader("Content-Disposition", "attachment; filename=\""
				+ uploadedFileName + "\"");
		FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());

		return null;
	}

	private String getFilePath(String fileType, String fileName) {
		String actualFilePath = null;
		if ("certificates".equals(fileType)) {
			actualFilePath = uploadPath + File.separator + CERTI_FOLDER
					+ File.separator + fileName;
		} else if ("benefits".equals(fileType)) {
			actualFilePath = uploadPath + File.separator + BENEFITS_FOLDER
					+ File.separator + fileName;
		} else if ("brochures".equals(fileType)) {
			actualFilePath = uploadPath + File.separator + BROCHURE_FOLDER
					+ File.separator + fileName;
		} else if ("rates".equals(fileType)) {
			actualFilePath = uploadPath + File.separator + RATES_FOLDER
					+ File.separator + fileName;
		} else if ("authority".equals(fileType)) {
			actualFilePath = uploadPath + File.separator + AUTHORITY_FOLDER
					+ File.separator + fileName;
		} else if ("marketingReq".equals(fileType)) {
			actualFilePath = uploadPath + File.separator + MARKETING_FOLDER
					+ File.separator + fileName;
		} else if ("disclosures".equals(fileType)) {
			actualFilePath = uploadPath + File.separator + DISCLOSURES_FOLDER
					+ File.separator + fileName;
		} else if ("accreditation".equals(fileType)) {
			actualFilePath = uploadPath + File.separator + ACCREDITATION_FOLDER
					+ File.separator + fileName;
		} else if ("additionalInfo".equals(fileType)) {
			actualFilePath = uploadPath + File.separator + ADD_INFO_FOLDER
					+ File.separator + fileName;
		} else if ("certSuppDoc".equals(fileType)) {
			actualFilePath = uploadPath + File.separator
					+ PLAN_SUPPORT_DOC_FOLDER + File.separator + fileName;
		}

		return actualFilePath;
	}

	@RequestMapping(value = "/admin/managenotice", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ADMIN_MANAGE_NOTICES')")
	public String manageIssuer(@ModelAttribute(NOTICETYPE) NoticeType noticeTypeInfo, Model model, HttpServletRequest request) {
		Map<String, Object> searchCriteria = this.createSearchCriteria(request);
		Map<String, Object> notificationListAndRecordCount = noticeTypeRepo.searchNotification(searchCriteria);
		List<NoticeType> notifications = (List<NoticeType>) notificationListAndRecordCount.get("notificationlist");

		Long iResultCt = (Long) notificationListAndRecordCount.get("recordCount");
		int iResultCount = iResultCt.intValue();

		model.addAttribute("page_title", "Getinsured Health Exchange: Notification Listing");

		model.addAttribute("notificationlist", notifications);
		model.addAttribute("searchCriteria", searchCriteria);
		model.addAttribute("resultSize", iResultCount);
		model.addAttribute(PAGE_SIZE, LISTING_NUMBER);
		model.addAttribute("submit", "submit");
		model.addAttribute(SORT_ORDER, searchCriteria.get(SORT_ORDER));
		model.addAttribute("usersList", GhixNoticeUser.values());
		model.addAttribute("typeStatusList", GhixNotificationType.values());
		model.addAttribute("methodsList", GhixNoticeCommunicationMethod.values());
		return "admin/managenotice";

	}

	@RequestMapping(value = "/admin/noticetype/noticetype/checkIfExistNameAndLanguage")
	@ResponseBody
	public String checkIfExistNameAndLanguage2(@RequestParam String notificationName, @RequestParam GhixLanguage language) {
		return checkNameAndLang(notificationName, language);
	}

	private String checkNameAndLang(String notificationName, GhixLanguage language) {
		NoticeType noticeTypeobj = null;
		try {
			noticeTypeobj = noticeTypeRepo.findByNameAndLanguage(notificationName, language);
		} catch (NotificationTypeNotFound e) {
			logger.warn(e.getMessage());
		}
		if (noticeTypeobj != null) {
			return "EXIST";
		}
		return "NEW";
	}

	@RequestMapping(value = "/admin/noticetype/checkIfExistNameAndLanguage")
	@ResponseBody
	public String checkIfExistNameAndLanguage(@RequestParam String notificationName, @RequestParam GhixLanguage language) {
		return checkNameAndLang(notificationName, language);
	}

	private Map<String, Object> createSearchCriteria(HttpServletRequest request) {
		HashMap<String, Object> searchCriteria = new HashMap<String, Object>();

		String sortBy = (request.getParameter(SORT_BY) == null) ? "id"
				: request.getParameter(SORT_BY);
		searchCriteria.put(SORT_BY, ((sortBy.equals("")) ? "id" : sortBy));

		String sortOrder = (request.getParameter(SORT_ORDER) == null) ? SortOrder.DESC.toString()
				: request.getParameter(SORT_ORDER);
		sortOrder = (sortOrder.equals("")) ? SortOrder.DESC.toString() : sortOrder;

		String changeOrder = (request.getParameter(CHANGE_ORDER) == null) ? "false"
				: request.getParameter(CHANGE_ORDER);
		sortOrder = (changeOrder.equals("true")) ? ((sortOrder.equals(SortOrder.DESC.toString())) ? SortOrder.ASC.toString()
				: SortOrder.DESC.toString())
				: sortOrder;
		request.removeAttribute(CHANGE_ORDER);
		searchCriteria.put(SORT_ORDER, sortOrder);

		int startRecord = (request.getParameter("pageNumber") != null) ? (Integer.parseInt(request.getParameter("pageNumber")) - 1)
				* LISTING_NUMBER
				: 0;
		searchCriteria.put("startRecord", startRecord);
		searchCriteria.put(PAGE_SIZE, LISTING_NUMBER);

		String name = (request.getParameter("notificationName") == null) ? ""
				: request.getParameter("notificationName");
		String user = (request.getParameter("user") == null) ? ""
				: request.getParameter("user");
		String type = (request.getParameter("type") == null) ? ""
				: request.getParameter("type");
		String method = (request.getParameter("method") == null) ? ""
				: request.getParameter("method");
		String reqParameter = (request.getParameter("hideTerminatedValue") == null) ? "NO"
				: request.getParameter("hideTerminatedValue");
		searchCriteria.put("hideTerminated", reqParameter);

		if (!name.equals("")) {
			searchCriteria.put("name", name);
		}

		if (!user.equals("")) {
			searchCriteria.put("user", user);
		}
		if (!type.equals("")) {
			searchCriteria.put("type", type);
		}
		if (!method.equals("")) {
			searchCriteria.put("method", method);
		}

		return searchCriteria;
	}

//	
//	private boolean isAllowed(Model model, HttpServletRequest request) {
//		AccountUser user = null;
//		HashMap<String, Object> errorMsg = new HashMap<String, Object>();
//
//		try {
//			user = userService.getLoggedInUser();
//			if (!userService.isAdmin(user)) {
//				errorMsg.put("message", "User not authorized.");
//				model.addAttribute("authfailed", errorMsg.get("message"));
//				request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION", errorMsg);
//				return false;
//			}
//		} catch (InvalidUserException ex) {
//			logger.error("User not logged in");
//			errorMsg.put("message", "User not logged in.");
//			model.addAttribute("authfailed", errorMsg.get("message"));
//			request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION", errorMsg);
//			return false;
//		}
//		return true;
//	}

	

	
	/**
	 * Controller to land the add new user page. 
	 * Jira Refs: HIX-27181  refactored code from UserController
	 * @author Biswakalyan nayak
	 * @param  Model model, HttpServletRequest request
	 * @throws InvalidUserException 
	 * @since 01/29/2014
	 */
	
	@RequestMapping(value="/admin/user/add",method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_USERS')")
	public String loadAddNewUser(Model model, HttpServletRequest request) throws InvalidUserException{
		
		logger.info("load add user page.");
		List<Role> roles = roleService.findAllUserCanAdd();
		if(roles == null || roles.size() == 0){
			throw new AccessDeniedException("No roles found for current user, please check the configuration");
			//throw new GIRuntimeException("No roles found for current user, please cgeck the configuration");
		}
		model.addAttribute("roles", roles);
		
		if(StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE))){
			model.addAttribute("stateCode", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
		}		
		
		logger.info("Successfully loaded add user page.");

		
		return "admin/user/add";
	}
	
	/**
	 * Controller to add a new user. 
	 * Jira Refs: HIX-27181  refactored code from UserController
	 * @author Biswakalyan nayak
	 * @param  Model model, HttpServletRequest request
	 * @throws InvalidUserException 
	 * @since 01/29/2014
	 */
	@RequestMapping(value="/admin/user/add",method = RequestMethod.POST)
	@GiAudit(transactionName = "Add User page load",
	 eventType = EventTypeEnum.USER_ACCOUNTS,
	 eventName = EventNameEnum.CREATE_ACCOUNTS)
	@PreAuthorize("hasPermission(#model, 'MANAGE_USERS')")
	public String addNewUser(@ModelAttribute("ManageUser") @Validated(ManageUser.ManageUserData.class) ManageUser manageUser,
			 BindingResult result,Model model, HttpServletRequest request) throws  InvalidUserException{
		
		String roleId = request.getParameter("role");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
		Boolean isEmailActivation = Boolean.FALSE;
		String emailActivation = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EMAIL_ACTIVATION);
		
		if (StringUtils.isNotBlank(emailActivation)){
	        isEmailActivation = Boolean.parseBoolean(emailActivation.toLowerCase());
		}
		if(logger.isDebugEnabled()) {
			logger.debug("Initiating add new user flow for {} with email activation set to {}",stateCode,isEmailActivation);
		}
		if(result.hasErrors()){
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Server Side Validation error while adding user"));
			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
		
		String proposedUserName = request.getParameter(EMAIL).toLowerCase();
		AccountUser userNameObj = null;
		try {
			
			if(isScimEnabled) {
				if(logger.isDebugEnabled()) {
					logger.debug("Checking remote system if this user exists");
				}
				userNameObj = scimUserManager.findByEmail(proposedUserName);
			} else {
				userNameObj = userService.findByUserName(proposedUserName);
			}
		} catch (Exception e) {
			userNameObj = null;
		}
		
		if (userNameObj != null) {
			logger.error("User exists, can not continue with the add user flow");
			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
		
		String fName = request.getParameter(FIRST_NAME);
		String lName = request.getParameter(LAST_NAME);
		
		if(!Utils.isPureAscii(fName) || !Utils.isPureAscii(lName)) {
			logger.error("First name and Last name can only have alpha-numeric characters");
			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}

		try {
			Role role =  roleService.findById(Integer.parseInt(roleId));
			if (!isEmailActivation && "MN".equalsIgnoreCase(stateCode)) {
				AccountUser accountUser = new AccountUser();
				accountUser.setFirstName(fName);
				accountUser.setLastName(lName);
				accountUser.setEmail(request.getParameter(EMAIL));
				accountUser.setUserName(request.getParameter(EMAIL));
				accountUser.setPhone(request.getParameter("phone"));
				if(StringUtils.isNotBlank(request.getParameter("extnAppUserId"))) {
					accountUser.setExtnAppUserId(request.getParameter("extnAppUserId"));
				}
				if(logger.isDebugEnabled()) {
					logger.debug("Initiating local account creation without email activation");
				}
				accountUser = userService.createUser(accountUser, role.getName());
			} else {
				if(logger.isDebugEnabled()) {
					logger.debug("Initiating email based activation");
				}
				Map<String,String> adminUserDetails = new HashMap<String, String>(); 
				adminUserDetails.put("userRoleId", roleId);
				adminUserDetails.put("userStartDate", startDate);
				adminUserDetails.put("userEndDate", endDate);
				adminUserDetails.put(FIRST_NAME, fName);
				adminUserDetails.put(LAST_NAME, lName);
				adminUserDetails.put(EMAIL,request.getParameter(EMAIL));
				adminUserDetails.put("phone",request.getParameter("phone"));
				adminUserDetails.put("userName",request.getParameter(EMAIL));
				
				adminUserDetails.put("name", fName + " " + lName);
				adminUserDetails.put("exchangename",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
				adminUserDetails.put("emailType", "adminUserAccountActivationEmail");
				
				CreatedObject createdObject = new CreatedObject();
				createdObject.setObjectId(0);//default id set to 0 to avoid NPE
				createdObject.setEmailId(request.getParameter(EMAIL));
				createdObject.setRoleName(role.getName());
				createdObject.setPhoneNumbers(Arrays.asList(request.getParameter("phone")));
				createdObject.setFullName(fName + " " + lName);
				createdObject.setCustomeFields(adminUserDetails);
				createdObject.setFirstName(fName);
			    createdObject.setLastName(lName);
				CreatorObject creatorObject = new CreatorObject();
				AccountUser accountUser=userService.getLoggedInUser();
		
				creatorObject.setObjectId(accountUser.getId());
				creatorObject.setFullName(accountUser.getFullName());
				if(null!=accountUser.getUserRole() && null!=accountUser.getUserRole().iterator().next().getRole()){
					creatorObject.setRoleName(accountUser.getUserRole().iterator().next().getRole().getName());
				}
				String noOfExpirationsDays = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.USER);
				int expirationDays = !StringUtils.isEmpty(noOfExpirationsDays)?Integer.parseInt(noOfExpirationsDays):0;
				GiAuditParameterUtil.add(new GiAuditParameter("RoleName", role.getName()), new GiAuditParameter(EMAIL, 
						request.getParameter(EMAIL)));
				AccountActivation accountActivation =  accountActivationService.initiateActivationForCreatedRecord(createdObject, creatorObject, expirationDays);
				Map<String, String> mapEventParam = new HashMap<>();
				
				mapEventParam.put("User Role Created", role.getName());
			
				
				//event type and category
				LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode("APPEVENT_ACCOUNT","ACCOUNT_CREATED");
				EventInfoDto eventInfoDto = new EventInfoDto();
				eventInfoDto.setEventLookupValue(lookupValue);
				eventInfoDto.setModuleId(role.getId());// Here we dont get entry for module user and user object. So setting role id and role name
				eventInfoDto.setModuleName(role.getName());
				eventInfoDto.setModuleUserName(createdObject.getFullName());
				
				appEventService.record(eventInfoDto, mapEventParam);
		
				if (accountActivation != null){
					logger.info("Account activation link sent to the employee...");
					request.getSession().setAttribute(ACC_ACTIVATION_LINK, "Activation link sent to "
							+ request.getParameter(EMAIL));
					// do something useful with accountActivation object... if needed
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),
							new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Activation link sent to "+ request.getParameter(EMAIL)));
				}
			}
			if(logger.isInfoEnabled()) {
				logger.info("Successfully added a user");
			}
		} catch (Exception e) {
			logger.error("Error occured while adding user.");
			throw new GIRuntimeException("PLATFORM-00003",e,null,Severity.HIGH);
		}
		if(logger.isDebugEnabled()) {
			logger.debug("Done executing the add user flow, redirecting to user listing");
		}
		return "redirect:/admin/manage/users";
	}
	
	/**
	 * Controller to update the  users status for Ajax call.  
	 * Jira Refs: HIX-27181  refactored code from UserController
	 * @author Biswakalyan nayak
	 * @param  String status,int id, HttpServletResponse response
	 * @since 01/29/2014
	 * @return content 
	 */
	@RequestMapping(value = "/admin/manage/user/updateStatus", method = RequestMethod.POST)
	@ResponseBody
	public String changeUserStatus(@RequestParam String status,@RequestParam String id,
			HttpServletResponse response) {
		int confirmed=1;
		String changedStatus="";
		if(status.equalsIgnoreCase(AccountUser.user_status.Activate.toString())){
			confirmed=1;
			changedStatus="Active";
		}
		else {
			if(status.equalsIgnoreCase(AccountUser.user_status.Deactivate.toString())){
				confirmed=0;
				changedStatus="InActive";
			}
		}
		try{
			int decryptedId=Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(id));
			userService.changeUserStatus(confirmed,decryptedId);
			AccountUser user  = userService.findById(decryptedId);
			usersNotificationService.sendEmailNotification(null, user.getEmail(), "Status <b> "+changedStatus+"</b><br><br>", "Your account has been updated",user.getFullName());
		   }
		catch(NotificationTypeNotFound e){
			logger.error("Exception while sending mail to user for status change",e);
		}
		catch(Exception e){
			logger.error("Exception while changing user status",e);
		}
		
		return status;
	}
	
	/**
	 * Controller to get all users. 
	 * Jira Refs: HIX-27181  refactored code from UserController
	 * @author Paco Feng, Biswakalyan nayak
	 * @param  Model model, HttpServletRequest request
	 * @throws InvalidUserException 
	 * @since 08/12/2013
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/admin/manage/users",method = {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize("hasPermission(#model, 'MANAGE_USERS') ")
	public String manageUsers(Model model, HttpServletRequest request) throws InvalidUserException{
		logger.info("Load Manage user page.");
		try {
			List<AccountUser> userList = null;
			Long iResultCt = 1L;
			
			if(null!=request.getSession().getAttribute(ERROR_MSG)){
				model.addAttribute(ERROR_MSG, request.getSession().getAttribute(ERROR_MSG));
				request.getSession().removeAttribute(ERROR_MSG);
			}
			Map<String, Object> searchCriteria = this.createManageUserCriteria(request);
			Map<String, Object> userListAndRecordCount = userService.searchUsers(searchCriteria);
			//List<AccountUser> accountUsers = getActivationUsers(searchCriteria);
			// TODO catch exception if tkm is not deployed...
			userList = (List<AccountUser>) userListAndRecordCount.get("userList");
			iResultCt = (Long) userListAndRecordCount.get("recordCount");//+accountUsers.size();
			if (userList == null) {
				userList = new ArrayList<AccountUser>();
			}
			Sort sort = new Sort(Sort.Direction.ASC, "name");
			List<Role> roleTypeList = roleService.findAllRoleTypes(sort);
			
			
			// coomented out as part of HIX-98893
			/*
			//logic to hide issuer repr & issuer enroll repre from manage user 
			//HIX-97616
			Iterator<Role> i = roleTypeList.iterator();
			while (i.hasNext()) {
				Role o = i.next();
			    if("ISSUER_REPRESENTATIVE".equalsIgnoreCase(o.getName()) || "ISSUER_ENROLLMENT_REPRESENTATIVE".equalsIgnoreCase(o.getName())) {
			    	i.remove();
			    }
			}*/
			//Logic ends
			
			model.addAttribute("userList", userList);
			model.addAttribute("searchCriteria", searchCriteria);
			model.addAttribute("resultSize", iResultCt);
			model.addAttribute(PAGE_SIZE, searchCriteria.get(PAGE_SIZE));
			model.addAttribute(MODEL_SORT_ORDER, searchCriteria.get(SORT_ORDER));
			model.addAttribute("USER_TYPE_LIST", roleTypeList);
			model.addAttribute(LOGED_USER, userService.getLoggedInUser());
			
			if(null!=request.getSession().getAttribute(ACC_ACTIVATION_LINK)){
				model.addAttribute(ACC_ACTIVATION_LINK, request.getSession().getAttribute(ACC_ACTIVATION_LINK));
				request.getSession().removeAttribute(ACC_ACTIVATION_LINK);
			}
			logger.info("Successfully loaded Manage user page.");
		} catch (Exception ex) {
			logger.error("Error in loading users: "+ex.getMessage());
			throw new GIRuntimeException(PLATFORM_00004,ex,null,Severity.HIGH);
		}
		logger.info("Closing manage user page.");
		return "admin/manage/users";
	}
	
	private Map<String, Object> createManageUserCriteria(HttpServletRequest request) {
		HashMap<String, Object> searchCriteria = new HashMap<String, Object>();
		int numberOfListing = LISTING_NUMBER;
		String selectedPageSize = request.getParameter("pageSize");
		if (!StringUtils.isEmpty(selectedPageSize)) {
			numberOfListing = Integer.parseInt(selectedPageSize);
		}
		String sortBy = request.getParameter(SORT_BY);
		sortBy = StringUtils.isEmpty(sortBy) ? "id" : sortBy;
		searchCriteria.put(SORT_BY, sortBy);
		request.removeAttribute(CHANGE_ORDER);
		String sortOrder=getSortOrder(request);
		searchCriteria.put(SORT_ORDER, sortOrder);
		String strPageNum = request.getParameter("pageNumber");
		int iPageNum = StringUtils.isEmpty(strPageNum) ? 0 : (Integer
				.parseInt(strPageNum) - 1);
		
		searchCriteria.put(PAGE_SIZE, numberOfListing);
		
		
		int startRecord = iPageNum * numberOfListing;
		searchCriteria.put("startRecord", startRecord);
		
		String selectedUserType = request.getParameter("userType");
		if (!StringUtils.isEmpty(selectedUserType)) {
			searchCriteria.put("userType", selectedUserType);
		}
		
		//logic to hide roles as per HIX-97616
		List<String> skipUserTypyes = new ArrayList<String>();
		//skipUserTypyes.add("ISSUER_REPRESENTATIVE");
		//skipUserTypyes.add("ISSUER_ENROLLMENT_REPRESENTATIVE");
		searchCriteria.put("userTypeNotIn", skipUserTypyes);
		
		String selectedUserStatus = request.getParameter("userStatus");
		if (!StringUtils.isEmpty(selectedUserStatus)) {
			searchCriteria.put("userStatus", selectedUserStatus);
		}
		
		String lastUpdated = request.getParameter("lastUpdated");
		if (!StringUtils.isEmpty(lastUpdated)) {
			searchCriteria.put("lastUpdated", lastUpdated);
		}

		String userName = request.getParameter("userName");
		if (!StringUtils.isEmpty(userName)) {
			searchCriteria.put("userName", userName);
		}
		
		return searchCriteria;
	}
	
	private String getSortOrder(HttpServletRequest request) {
		String sortOrder = request.getParameter(SORT_ORDER);
		sortOrder = StringUtils.isEmpty(sortOrder) ? SortOrder.DESC.toString() : sortOrder;

		String changeOrder = request.getParameter(CHANGE_ORDER);
		changeOrder = StringUtils.isEmpty(changeOrder) ? "false" : changeOrder;

		if (changeOrder.equals("true")) {
			sortOrder = (sortOrder.equals(SortOrder.DESC.toString())) ? SortOrder.ASC.toString() : SortOrder.DESC.toString();
		}
		
		return sortOrder;
	}
	
	/**
	 * Controller to manage selected user's roles.
	 * Jira Refs: HIX-27181  refactored code from UserController
	 * @author venkata_tadepalli , Biswakalyan nayak
	 * @since 01/10/2014
	 * @param  Model model, HttpServletRequest request
	 * @return  String
	 * @throws GIRuntimeException 
	 * @see     
	 */
	@RequestMapping(value="/admin/manage/user/roles",method = RequestMethod.GET)
	@GiAudit(transactionName = "Go to Manage User role Page", eventType = EventTypeEnum.USER_ACCOUNTS, eventName = EventNameEnum.MODIFY_ACCOUNTS)
	@PreAuthorize("hasPermission(#model, 'MANAGE_USERS')")
	public String getManagedUserRoles(Model model, HttpServletRequest request) {
		//Added or modified for HIX-27184 and HIX-27305
		//Modified for HIX-51101
		int userId= Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(request.getParameter("userId")));
		logger.info("Manage User Roles for the user ::"+userId);
		AccountUser userToManage = null,logedUser = null ;
		List<UserRole> mangeUserRoleList= new ArrayList<UserRole>();
		Set<UserRole> userRolesOfManagedUser;
		Role defRoleOfManagedUser;
		try {
			logedUser = userService.getLoggedInUser();
			userToManage = userService.findById(userId);
			if(userToManage==null){
				String errMsg="Manage User Roles for the user ("+userId+") :: Not found";
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, errMsg));
				logger.error(errMsg);
				throw new GIRuntimeException(errMsg);
			}
			
			//Get data for the user to manage. default role, user roles
			defRoleOfManagedUser = userService.getDefaultRole(userToManage);
			userRolesOfManagedUser = userToManage.getUserRole();
			// Only privileged users
			if(defRoleOfManagedUser.getPrivileged()==1){
				mangeUserRoleList = roleService.findProvisioningOptionsForExistingRoles(userRolesOfManagedUser);
			}else{
				//non privilege user roles can't be added by user but admin/operation can de-active the role. We will show only the active roles. 
				//If any inactive role exist for any user no one can manage that. That user can add the role again to activate the role.
				for (UserRole userRole : userRolesOfManagedUser) {
					if(userRole.isActiveUserRole() && !userRole.isDefaultRole()){
						mangeUserRoleList.add(userRole);
					}
				}
			}
			logger.info("loaded User Roles for the user ::"+userId);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "loaded User Roles for the user id::"+userId));
		} catch (Exception ex) {
			logger.error("Error in loading User Roles: "+ex.getMessage());
			throw new GIRuntimeException(PLATFORM_00004,ex,null,Severity.HIGH);
		}
		model.addAttribute("mangeUserRoles",mangeUserRoleList);
		model.addAttribute("user", userToManage);
		model.addAttribute("defaultRole",defRoleOfManagedUser);
		model.addAttribute(LOGED_USER,logedUser );
		return "admin/manage/user/roles";
	}
	
	/**
	 * Controller to show user details.
	 * Jira Refs: HIX-27181  refactored code from UserController
	 * @author venkata_tadepalli , Biswakalyan nayak
	 * @since 01/29/2014
	 * @param  Model model, HttpServletRequest request
	 * @return  String
	 * @see     
	 */
	@RequestMapping(value="/admin/manage/user/details",method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_USERS')")
	public String viewUserDetail(Model model, HttpServletRequest request){
		logger.info("Loading user detail page.");
		try {
			int userId= Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(request.getParameter("userId")));
			AccountUser user = userService.findById(userId);
			boolean editDetails = this.checkUpdatesAllowed(user);
			boolean editRoles = this.checkIfRoleUpdateAllowed(user);
			model.addAttribute("editDetails", editDetails);
			model.addAttribute("editRoles", editRoles);
			model.addAttribute("user", user);
			model.addAttribute(LOGED_USER,userService.getLoggedInUser());
			if(StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE))){
				model.addAttribute("stateCode", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
			}
			logger.info("Successfully loaded user detail page.");
		} catch (Exception ex) {
			logger.error("Error in loading user details: "+ex.getMessage());
			throw new GIRuntimeException(PLATFORM_00004,ex,null,Severity.HIGH);
		}
		
		return "admin/manage/user/details";
	}
	
	private boolean checkIfRoleUpdateAllowed(AccountUser user) {
		try{
			this.userService.checkIfRoleUpdateAllowed(user);
		}catch(Exception e){
			return false;
		}
		return true;
	}

	private boolean checkUpdatesAllowed(AccountUser user) {
		try{
			this.userService.checkUpdatesAllowed(user);
		}catch(Exception e){
			return false;
		}
		return true;
	}
	
	@RequestMapping(value = "/admin/batchJobSchedules",method=RequestMethod.GET)
	public String launchJobSchedules(HttpServletRequest request, HttpServletResponse response) {
		logger.info("Redirecting to batch schedule page ");
		return ("batch/JobList"); 
	}
	
	@RequestMapping(value = "/admin/getJobList",method=RequestMethod.GET, produces="application/json")
	//	@PreAuthorize("hasPermission(#model, 'MANAGE_BATCH')")
	public ResponseEntity<String> getJobList(HttpServletRequest request, HttpServletResponse response) {
		StringBuffer urlBuff = null;
		GhixModuleAccessToken ghixModuleAccessToken = null;
		ResponseEntity<String> responseEntity = null;
		String urlParams = null;
		
		try {
			ghixModuleAccessToken = GhixModuleAccessToken.getModuleAccessToken();
			urlParams = ghixModuleAccessToken.toString();
			
			if(StringUtils.isNotEmpty(urlParams)) {
				urlBuff = new StringBuffer(GhixPlatformEndPoints.APPSERVER_URL).append("ghix-batch/getJobList?").append(urlParams);
				if(logger.isInfoEnabled()) {
					logger.info("BATCH REDIRECT URI: "+urlBuff.toString());
				}
				responseEntity = restTemplate.exchange(urlBuff.toString(), null, HttpMethod.GET, MediaType.APPLICATION_JSON, String.class, null);
				
			}
		} catch (Exception ex) {
			logger.error("ERR: WHILE REDIRECTING TO BATCH SSO: ",ex);
		}
		return responseEntity;
	}
	
	@RequestMapping(value = "/admin/scheduleJob",method=RequestMethod.POST, produces="application/json")
	//	@PreAuthorize("hasPermission(#model, 'MANAGE_BATCH')")
	public ResponseEntity<String> scheduleJob(HttpServletRequest request, HttpServletResponse response) {
		StringBuffer urlBuff = null;
		GhixModuleAccessToken ghixModuleAccessToken = null;
		ResponseEntity<String> responseEntity = null;
		String urlParams = null;
		
		try {
			ghixModuleAccessToken = GhixModuleAccessToken.getModuleAccessToken();
			urlParams = ghixModuleAccessToken.toString();
			
			String jobName = request.getParameter("jobName");
			String cronExp = request.getParameter("cronExp");
			
			String urlParams2 = "&jobName=" + jobName + "&cronExp=" + cronExp;
			
			if(StringUtils.isNotEmpty(urlParams)) {
				urlBuff = new StringBuffer(GhixPlatformEndPoints.APPSERVER_URL).append("ghix-batch/scheduleJob?").append(urlParams).append(urlParams2);
				if(logger.isInfoEnabled()) {
					logger.info("BATCH REDIRECT URI: "+urlBuff.toString());
				}
				responseEntity = restTemplate.exchange(urlBuff.toString(), null, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, null);
			}
		} catch (Exception ex) {
			logger.error("ERR: WHILE REDIRECTING TO BATCH SSO: ",ex);
		}
		return responseEntity;
	}
	@RequestMapping(value = "/admin/serffAdmin",method=RequestMethod.GET)
	//@PreAuthorize("hasPermission(#model, 'MANAGE_SERFF')")
	public void serffAdmin(HttpServletRequest request, HttpServletResponse response) {
		StringBuffer urlBuff = null;
		GhixModuleAccessToken ghixModuleAccessToken = null;
		String urlParams = null;
		
		try {
			ghixModuleAccessToken = GhixModuleAccessToken.getModuleAccessToken();
			urlParams = ghixModuleAccessToken.toString();
			if(StringUtils.isNotEmpty(urlParams)) {
				urlBuff = new StringBuffer(GhixPlatformEndPoints.APPSERVER_URL).append("exchange-hapi/account/user/login?").append(urlParams);
				if(logger.isInfoEnabled()) {
					logger.info("SERFF REDIRECT URI: "+urlBuff.toString());
				}
				response.sendRedirect(urlBuff.toString());
			}
		} catch (Exception ex) {
			logger.error("ERR: WHILE REDIRECTING TO SERFF SSO: ",ex);
		}
	}
	@RequestMapping(value = "/admin/batchjobs",method=RequestMethod.GET)
	//@PreAuthorize("hasPermission(#model, 'MANAGE_BATCH')")
	public void batchJobs(HttpServletRequest request, HttpServletResponse response) {
		StringBuffer urlBuff = null;
		GhixModuleAccessToken ghixModuleAccessToken = null;
		String urlParams = null;
		String server_url = request.getParameter("server_url");
		
		if(StringUtils.isEmpty(server_url)) {
			server_url = GhixPlatformEndPoints.APPSERVER_URL;
		}
		
		if(!server_url.endsWith("/")) {
			server_url += "/";
		}
		try {
			ghixModuleAccessToken = GhixModuleAccessToken.getModuleAccessToken();
			urlParams = ghixModuleAccessToken.toString();
			if(StringUtils.isNotEmpty(urlParams)) {
				urlBuff = new StringBuffer(server_url).append("ghix-batch/account/user/login?").append(urlParams);
				if(logger.isInfoEnabled()) {
					logger.info("BATCH REDIRECT URI: "+urlBuff.toString());
				}
				response.sendRedirect(urlBuff.toString());
			}
		} catch (Exception ex) {
			logger.error("ERR: WHILE REDIRECTING TO BATCH SSO: ",ex);
		}
		
	}
	
	@RequestMapping(value="/batch/getBatchNodes", method=RequestMethod.GET)
	@ResponseBody
	@PreAuthorize("hasPermission(#model, 'MANAGE_BATCH')")
	public String getBatchNodes(Model model, HttpServletRequest request) {
		JSONArray jsonNodes = batchNodeUtil.getBatchNodes();		
		return jsonNodes.toJSONString(); 
	}
	
	@RequestMapping(value="/batch/batchnodes", method=RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BATCH')")
	public String getNodesPage(Model model, HttpServletRequest request) {
		return "batch/batchnodes";
	}
	
	
	/** 
	 * Controller to edit selected user's detail.
	 * Jira Refs: HIX-27181 refactored code from UserController
	 * @author venkata_tadepalli , Biswakalyan nayak
	 * @since 01/10/2014
	 * @param  Model model, HttpServletRequest request
	 * @return  String
	 * @throws GIRuntimeException 
	 * @see     
	 */
	@RequestMapping(value="/admin/manage/user/edit",method = RequestMethod.POST)
	@GiAudit(transactionName = "Edit User Details", eventType = EventTypeEnum.USER_ACCOUNTS, eventName = EventNameEnum.ACTIVATE_ACCOUNT)
	@PreAuthorize("hasPermission(#model, 'MANAGE_USERS')")
	public String editUserDetails(@ModelAttribute("AccountUser")  @Validated(AccountUser.ManageEditAccountUser.class) AccountUser accountUserObj , BindingResult result,Model model,HttpServletRequest request,HttpServletResponse response)  {
		logger.debug("Edit User detail."); 
		model.addAttribute("page_title", "Getinsured Health Exchange: Edit User" );
		String errorMsg = null;
		StringBuffer emailMessage  = new StringBuffer();
		emailMessage.append("<p/>");
		Map<String,String> map = new HashMap<String, String>();
		List<GiAuditParameter> auditParameters = new ArrayList<GiAuditParameter>();
		
		String oldEmailAddress = null;
		String newEmailAddress = null;
		//GiAuditParameterUtil.add(new GiAuditParameter("User Id ", accountUserObj.getId()));	
		auditParameters.add(new GiAuditParameter("User Id ", accountUserObj.getId()));
		
		if(result.hasErrors()||!AccountUserDto.isValidUserStatus(accountUserObj.getStatus())){
			GiAuditParameterUtil.add(new GiAuditParameter("User Id ", accountUserObj.getId()));
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Server side validation error for edit user"));
			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
		try {
			if(accountUserObj.getStatus().equalsIgnoreCase("Active")){
				
				/*
				 * HIX-52548
				 * 
				 * The Locked checkbox needs to be evaluated
				 * only when status is Active
				 * 
				 * If checkbox is missing, user is either not active
				 * or unlocked. In this case, set confirmed as 1
				 * 
				 * If checkbox is present, set the value of confirmed to 
				 * 1: If checkbox is unchecked (User is unlocked)
				 * 0: If checkbox is checked (User is locked)
				 * 
				 */
				String accLockConfirm = request.getParameter("accountLocked");
				logger.debug((accLockConfirm==null)?"Found lockConfirm = NULL, setting user's confirm value to default as 1":("lockConfirm value = "+accLockConfirm));
				int accLockedVal = Integer.valueOf((accLockConfirm!=null)?accLockConfirm:"1");
				
				if(accLockedVal == 0){
					accountUserObj.setConfirmed(0);
				//	GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Account of Userid "+accountUserObj.getId()+" is Locked"));
					auditParameters.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
					auditParameters.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Account of Userid "+accountUserObj.getId()+" is Locked"));
				}
					
				//HIX-28891 user account lock/unlock
				if(accLockedVal == 1){
					accountUserObj.setConfirmed(1);
					accountUserObj.setRetryCount(0);
					accountUserObj.setSecQueRetryCount(0);
				//	GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Account of Userid "+accountUserObj.getId()+" is Unlocked"));
					auditParameters.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
					auditParameters.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Account of Userid "+accountUserObj.getId()+" is Unlocked"));
					logger.debug("Setting user's status to unlock.");
				}
				accountUserObj.setStatus("Active");
				//GiAuditParameterUtil.add(new GiAuditParameter("Account Lock Value", accLockedVal));
				auditParameters.add(new GiAuditParameter("Account Lock Value", accLockedVal));
			}
						
			if(accountUserObj.getStatus().equalsIgnoreCase("Inactive")){
				accountUserObj.setConfirmed(0);
				accountUserObj.setStatus("Inactive");
				//GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Account of Userid "+accountUserObj.getId()+" is Locked and status is Inactive"));
				auditParameters.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
				auditParameters.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Account of Userid "+accountUserObj.getId()+" is Locked and status is Inactive"));
			}
			
			
			
			Calendar startDate = prepareDBDate(request.getParameter("startDate"));
			Calendar endDate = prepareDBDate(request.getParameter("endDate"));
			
			if(null!=startDate){
				accountUserObj.setStartDate(startDate.getTime());
			}
			
			if(null!=endDate){
				accountUserObj.setEndDate(endDate.getTime());
			}
			
			if( !Utils.isPureAscii(request.getParameter("firstName")) || !Utils.isPureAscii(request.getParameter("lastName"))) {
				   logger.error("Invalid first name/last name provided while editing User details");
				   throw new GIRuntimeException("Invalid first name/ last name");
			   }
		
			AccountUser user  = userService.findById(accountUserObj.getId());
			
			/*
			 *  
			 * HIX-102236
			 * Audit Log entry if individual is unlocked from edit user screen.
			 * Conditions to check
			 * 1. User is "Individual"
			 * 2. User was Unlocked after getting locked because of exhausting reset password attempts. 
			 * To satisfy condition 2 check all of the following.
			 * a. accLockConfirm == null(Checkbox is unchecked)
			 * b. db account user's confirmed == 0
			 * c. db account user's status = active
			 * 
			 * */
			
			//Log parameter only when user is unlocked. So accLockConfirm should be NULL and prev confirmed flag should be 0, prev status should be active
			Set<UserRole> userRoles = user.getUserRole();
			if(userRoles != null && userRoles.size() > 0){
				for (UserRole userRole : userRoles) {
					if(userRole.isDefaultRole() && userRole.getRole().getLabel().equalsIgnoreCase("INDIVIDUAL")){
						if(request.getParameter("accountLocked") == null && user.getConfirmed() == 0 && user.getStatus().equalsIgnoreCase("active") )
						{
							auditParameters.add(new GiAuditParameter("Unlock User: USER_NAME=", accountUserObj.getFirstName() + " " + accountUserObj.getLastName() + ",EMAIL_ID=" + accountUserObj.getEmail()));
						}			
					}
				}
			}
			
			
			
			// Now check the permission
			this.userService.checkUpdatesAllowed(user);
			map.putAll(compareExistingUser(user,accountUserObj));
			for (Map.Entry<String, String> entry: map.entrySet()) {
				if(! "CurrentStatus".equalsIgnoreCase(entry.getKey()) && ! "NewStatus".equalsIgnoreCase(entry.getKey()) ) {
					emailMessage.append(entry.getKey()+" <b> " +entry.getValue()+"</b><br>");
				}
				
				if( "Status :".equals(entry.getKey() ) && null != map.get("CurrentStatus") ){
					auditParameters.add(new GiAuditParameter("Status changed for User :", user.getAuditLogName()));
					auditParameters.add(new GiAuditParameter("Role :", roleService.findRoleByUserId( user.getId() ).getLabel()  ) );
					auditParameters.add(new GiAuditParameter("Status changed from:", map.get("CurrentStatus")));
					auditParameters.add(new GiAuditParameter("Status changed to:", map.get("NewStatus")));
				}
				if(!("Phone: ").equals(entry.getKey())){
					auditParameters.add(new GiAuditParameter(entry.getKey(), entry.getValue()));
				}
			}
			
			oldEmailAddress = request.getParameter("userName").toLowerCase();
			newEmailAddress = user.getEmail().toLowerCase();
			
			if (!oldEmailAddress.equalsIgnoreCase(newEmailAddress)) {
				
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				errorMsg = "User's email and username cannot be different";
			//	GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Change email operation for user id "+accountUserObj.getId() +" is not allowed"));
				auditParameters.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE));
				auditParameters.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Change email operation for user id "+accountUserObj.getId() +" is not allowed as User's email and username cannot be different"));
			
				
			/*	if ("TRUE".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EMAIL_CHANGE_ENABLED))) {

					errorMsg = userChangeEmailManager
							.processEmailChangeRequest(oldEmailAddress,
									newEmailAddress);
					DuoAdminApiReturnStatus duoAdminApiReturnStatus = DuoAdminApi.removeDuoCheck(newEmailAddress); 
					if(duoAdminApiReturnStatus ==  DuoAdminApiReturnStatus.AccountRemoved) {
						GiAuditParameterUtil.add(new GiAuditParameter("userId ",   user.getId()),
								 new GiAuditParameter("Old userName ", newEmailAddress),
								 new GiAuditParameter("New userName ", oldEmailAddress),
								 new GiAuditParameter("Duo ac has been disabled for change of username", user.getFullName() )
						);
					}
					
				}

				else {
					response.setStatus(HttpServletResponse.SC_FORBIDDEN);
					errorMsg = "Change email operation is not allowed";
				//	GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Change email operation for user id "+accountUserObj.getId() +" is not allowed"));
					auditParameters.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE));
					auditParameters.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Change email operation for user id "+accountUserObj.getId() +" is not allowed"));
				}*/
			}

			if (logger.isInfoEnabled()) {
				logger.info("CHNG EMAIL: ERR: " + errorMsg);
			}
			user.setUserName(oldEmailAddress);
			user.setEmail(oldEmailAddress);
			user.setLastUpdatedBy(userService.getLoggedInUser().getId());
			user = userService.updateUser(user);
			
			List <GiAuditParameter> duoAuditParams =  this.userRoleService.toggleDuoAssociation(user);
			if(!duoAuditParams.isEmpty()){
				for(GiAuditParameter auditParameter : duoAuditParams ){
					GiAuditParameterUtil.add(auditParameter);
				}
			}
			
			if(!auditParameters.isEmpty()){
				for(GiAuditParameter auditParameter : auditParameters ){
					GiAuditParameterUtil.add(auditParameter);
				}
			}
			
			if(!emailMessage.toString().trim().equals("<p/>")){
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
				Boolean isEmailActivation = Boolean.FALSE;
				String emailActivation = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EMAIL_ACTIVATION);
				
				if (StringUtils.isNotBlank(emailActivation)){
			        isEmailActivation = Boolean.parseBoolean(emailActivation.toLowerCase());
				}
				if (!"MN".equalsIgnoreCase(stateCode) || isEmailActivation) {
					usersNotificationService.sendEmailNotification(null, user.getEmail(), emailMessage.toString()+"<br>", "Your account has been updated",user.getFullName());
				}
			}
			logger.debug("Edit User succesfull. ");
		
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Edit User successful for user id "+accountUserObj.getId()));
			
		} catch (NotificationTypeNotFound e) {
			logger.error("Error while sending mail");
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while sending email for user id"+accountUserObj.getId()));
			errorMsg = "Error while sending mail";
			request.getSession().setAttribute(ERROR_MSG, errorMsg);
		}catch (Exception ex) {
			logger.error("Error while editing user details: "+ex.getMessage());
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while updating user details"));
			throw new GIRuntimeException("PLATFORM-00003",ex,null,Severity.HIGH);
		} finally {
			if(!StringUtils.isEmpty(errorMsg) && !errorMsg.equalsIgnoreCase("Email changed successfully for user id "+accountUserObj.getId())) {
				request.getSession().setAttribute(ERROR_MSG, errorMsg);
			}
 		}
		return "redirect:/admin/manage/users";
	}
	
	private Map<String,String> compareExistingUser(AccountUser user,
			AccountUser accountUserObj) {
		Map<String,String> map = new HashMap<String, String>();
		
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		
		Household household = null;
		// Don't try to update the CMR household for MN as there will be multiple HH connected to a userAccount.
		if(!stateCode.equalsIgnoreCase("MN"))
			household= householdRepository.findByUserId(user.getId());
		boolean isHouseholdChanged = false;
		
		if(null!=user.getFirstName()){
			if(!user.getFirstName().equals(accountUserObj.getFirstName())){
				user.setFirstName(accountUserObj.getFirstName());
				if(null != household) {
					household.setFirstName(accountUserObj.getFirstName());
					isHouseholdChanged = true;
				}
				map.put("First Name: ", accountUserObj.getFirstName());
			}
		}
		else{
			user.setFirstName(accountUserObj.getFirstName());
			map.put("First Name: ", accountUserObj.getFirstName());
		}
			
		
		if(null!=user.getLastName()){
			if(!user.getLastName().equals(accountUserObj.getLastName())){
				user.setLastName(accountUserObj.getLastName());
				if(null != household) {
					household.setLastName(accountUserObj.getLastName());
					isHouseholdChanged = true;
				}
				map.put("Last Name: ", accountUserObj.getLastName());
			}
		}
		else{
			user.setLastName(accountUserObj.getLastName());
			map.put("Last Name: ", accountUserObj.getLastName());
		}
		
		if(null!=user.getEmail()){
			if(!user.getEmail().equals(accountUserObj.getEmail())){
				user.setEmail(accountUserObj.getEmail());
				map.put("Email: ", accountUserObj.getEmail());
			}

		}
		else{
			user.setEmail(accountUserObj.getEmail());
			map.put("Email: ", accountUserObj.getEmail());
		}
		
		if("MN".equalsIgnoreCase(stateCode)) {
			if(null!=user.getExtnAppUserId()){
				if(!user.getExtnAppUserId().equals(accountUserObj.getExtnAppUserId())){
					user.setExtnAppUserId(accountUserObj.getExtnAppUserId());
					map.put("ExtnAppUserId: ", accountUserObj.getExtnAppUserId());
				}
			}
			else{
				user.setExtnAppUserId(accountUserObj.getExtnAppUserId());
				map.put("ExtnAppUserId: ", accountUserObj.getExtnAppUserId());
			}
		}
		
		if(null!=user.getPhone()){
			if(!user.getPhone().equals(accountUserObj.getPhone())){
				user.setPhone(accountUserObj.getPhone());
				if(null != household) {
					household.setPhoneNumber(accountUserObj.getPhone());
					isHouseholdChanged = true;
				}
				map.put("Phone: ", accountUserObj.getPhone());
			}
			
		}
		else{
			user.setPhone(accountUserObj.getPhone());
			map.put("Phone: ", accountUserObj.getPhone());
		}
		
		if(isHouseholdChanged) {
			householdRepository.save(household);
		}
		
		if(user.getConfirmed()!=accountUserObj.getConfirmed()){
			user.setConfirmed(accountUserObj.getConfirmed());
			map.put("Status :", accountUserObj.getStatus().equalsIgnoreCase("Active") ? "Active" : "Inactive");
			
			// HIX-29631 Send Account Lock Notification
			if(!accountUserObj.getStatus().equalsIgnoreCase("Inactive")){
				try {
					sendAccountLockMail(user);
				} catch (NotificationTypeNotFound e) {
					logger.error(e.getMessage());
					logger.error("Account Lock e-mail cannot be sent - ", e);
				}
			}
		}
		
		//HIX-28891 account lock/unlock
		if(user.getConfirmed() == 1 && user.getRetryCount() != accountUserObj.getRetryCount()){
			user.setRetryCount(accountUserObj.getRetryCount());
			user.setSecQueRetryCount(accountUserObj.getSecQueRetryCount());
		}
 		//HIX-101651 To unlock account in CSR password reset flow.
		if(user.getConfirmed() == 1 && user.getSecQueRetryCount() != accountUserObj.getSecQueRetryCount()){
			user.setSecQueRetryCount(accountUserObj.getSecQueRetryCount());
		}
		if(!user.getStatus().equals(accountUserObj.getStatus())){
			map.put("CurrentStatus", user.getStatus() );
			map.put("NewStatus", accountUserObj.getStatus() );
			if(accountUserObj.getStatus().equalsIgnoreCase("Inactive")||accountUserObj.getStatus().equalsIgnoreCase("Inactive-dormant")){
				user.setConfirmed(0);
				sessionTracker.killUserActiveSession(accountUserObj.getId());
			}
			user.setStatus(accountUserObj.getStatus());
			map.put("Status :", accountUserObj.getStatus());
		}
		
		//Modified for HIX-27184 and HIX-27305.
		//Now start and end date are not mandatory.
		
		// Added Null check for HIX-30386
		if(user.getStartDate()!=null && accountUserObj.getStartDate()!= null){
			if(user.getStartDate().compareTo(accountUserObj.getStartDate())!=0){
				user.setStartDate(accountUserObj.getStartDate());
				map.put("Start Date: ", accountUserObj.getStartDate().toString());
			}
		}
		else if(null!=accountUserObj.getStartDate()){
			user.setStartDate(accountUserObj.getStartDate());
			map.put("Start Date: ", accountUserObj.getStartDate().toString());
		}
		
		if(user.getEndDate()!=null && accountUserObj.getEndDate()!= null){
			if(user.getEndDate().compareTo(accountUserObj.getEndDate())!=0){
				user.setEndDate(accountUserObj.getEndDate());
				map.put("End Date: ", accountUserObj.getEndDate().toString());
			}
		}
		else if (null!=accountUserObj.getEndDate()){
			user.setEndDate(accountUserObj.getEndDate());
			map.put("End Date: ", accountUserObj.getEndDate().toString());
		}
		return map;
	}
	
	/**
	 * HIX-29631
	 * Sends email for account UnLock
	 *
	 * @param userObj
	 *            {@link AccountUser}
	 * @throws NotificationTypeNotFound
	 *             Exception
	 */
	private void sendAccountLockMail(AccountUser userObj) throws NotificationTypeNotFound {
		logger.info("sendAccountLockMail : START");

		Map<String, Object> notificationContext = new HashMap<String, Object>();
		notificationContext.put("USER_OBJ", userObj);
		Map<String, String> tokens = accountLockNotificationEmail.getTokens(notificationContext);
		Map<String, String> emailData = accountLockNotificationEmail.getEmailData(notificationContext);
		Location location = null; //TODO: location has to be provided here 
		
		Notice noticeObj = accountLockNotificationEmail.generateEmail(accountLockNotificationEmail.getClass().getSimpleName(),location, emailData, tokens);
		if(noticeObj == null){
			throw new NotificationTypeNotFound("Failed to find the notification for location:"+accountLockNotificationEmail.getClass().getSimpleName());
		}
		logger.info(noticeObj.getEmailBody());
		accountLockNotificationEmail.sendEmail(noticeObj);
		logger.info("sendAccountLockMail : END");
	}
	
	private Calendar prepareDBDate(String parameter) {
			
		Calendar calendarDate = null;
		Date effectiveEndDate = null;
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		
		if(StringUtils.isEmpty(parameter)) {
			return calendarDate;
		}
		
		try {
			effectiveEndDate = formatter.parse(parameter);
		} catch (ParseException e) {
			logger.error(e.getMessage());
			return calendarDate;
		}

		calendarDate = TSCalendar.getInstance();
		calendarDate.setTime(effectiveEndDate);
		calendarDate.set(Calendar.HOUR_OF_DAY, calendarDate.getActualMaximum(Calendar.HOUR_OF_DAY));
		calendarDate.set(Calendar.MINUTE, calendarDate.getActualMaximum(Calendar.MINUTE));
		calendarDate.set(Calendar.SECOND, calendarDate.getActualMaximum(Calendar.SECOND));

		return calendarDate;
	}
	

	/**
	 * Controller to edit selected user's roles.
	 * Jira Refs: HIX-27181 refactored code from UserController
	 * @author Biswakalyan nayak
	 * @since 01/29/2014
	 * @param  Model model, HttpServletRequest request
	 * @return  String
	 * @see     
	 */
	
	public String editUserRole(Model model, HttpServletRequest request) {
		//Added for HIX-27184 and HIX-27305
		//Modified for HIX-49270,HIX-51924
		List<String> userNonDefRolesStr = new ArrayList<String>(),listRolesToUpdate;
		UserRole toUpdateUserRole;
		AccountUser user;
		HashMap<String,Object> editUserRoleData ;
		try {
			logger.debug("Edit User roles for user id:"+request.getParameter("id")); 
			//Get the role list which was checked in UI from request object. But 
			editUserRoleData = validateEditUserRoles(request);
			user  = (AccountUser)editUserRoleData.get("user");// userService.findById(Integer.parseInt(request.getParameter("id")));
			listRolesToUpdate = (List<String>)editUserRoleData.get("rolesToUpdate");
			//User exiting non default roles 
			Set<UserRole> userNonDefRoles =  userService.getNonDefaultUserRole(user);
			for (UserRole userRole : userNonDefRoles) {
				userNonDefRolesStr.add(userRole.getRole().getName());
			}
			
			/*Check each request parameter, if the name matches with any role present in role(All roles).
			 * If present insert/update to userRole table depending on already existing role or new role.
			 * */
			if(listRolesToUpdate!= null && !listRolesToUpdate.isEmpty()){
				Iterator<String>itrRolesToUpdate=listRolesToUpdate.iterator();
				while (itrRolesToUpdate.hasNext()) {
					String currRoleToUpdate = itrRolesToUpdate.next();
					
					toUpdateUserRole = userService.getUserRoleByRoleName(user,currRoleToUpdate);
					if(userNonDefRolesStr.contains(currRoleToUpdate) && !toUpdateUserRole.isActiveUserRole()){
						toUpdateUserRole.setIsActive(RoleService.ROLE_IS_ACTIVE_DEFAULT);
						userService.updateUserRole(user, toUpdateUserRole);
					}else{
						userService.addUserRole(user, currRoleToUpdate);
					}
				}
			}
			
			//Now remove all processed roles in above section. So non exiting non default role list need to marked as inactive
			userNonDefRolesStr.removeAll(listRolesToUpdate);
			/*We did not get any idea about the unchecked checkbox detail in request. 
			 * Bcoz the unchecked check box does not have any value in request. 
			 * So need to update the user roles to inactive which are not available in request.
			 * */
			GiAuditParameterUtil.add(new GiAuditParameter("userId ", user.getId()),
					new GiAuditParameter("List of roles to update", listRolesToUpdate));
			for (String nonDefRole : userNonDefRolesStr) {
				toUpdateUserRole = userService.getUserRoleByRoleName(user,nonDefRole);
				/*Check if the role is present in user existing role or not. If exist in user role then 
				 * Update the status to inactive.If the status is already inactive don't go for database hit.
				 * Skip and continue.
				 *  */
				if(toUpdateUserRole.isActiveUserRole()){
					toUpdateUserRole.setIsActive(RoleService.ROLE_IS_ACTIVE_NOT_DEFAULT);
					userService.updateUserRole(user, toUpdateUserRole);
				}
			}
			logger.debug("Edit User roles succesfull for user id:"+user.getId());
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Edit User roles succesfull for user id:"+user.getId()));
		}catch (GIRuntimeException e) {
			logger.error("Error occured while adding/modifing the role.");
			throw e;
		} 
		catch (Exception e) {
			logger.error("Error occured while adding/modifing the role.");
			throw new GIRuntimeException("PLATFORM-00002",e,null,Severity.HIGH);
		}
					
		return "redirect:/admin/manage/users";
	}

	private HashMap<String,Object> validateEditUserRoles(HttpServletRequest request){
		boolean isValid=true;
		HashMap<String,Object> editUserRoleData = new HashMap<String,Object>();
		//List<String> userNonDefRolesStr = new ArrayList<String>();
		List<String> lstRolesToUpdate = new ArrayList<String>();
		//UserRole toUpdateUserRole;
		
		// Validate the user's id
		AccountUser user  = userService.findById(Integer.parseInt(request.getParameter("id")));
		if(user!=null){
			editUserRoleData.put("user", user);
		}
		
		//get all roles to check the  request params.
		List<String> dbRoleNameList = roleService.findAllRoleName();
		
		// Validate the Role name and status received from the client
		Enumeration<String> paramEnumerator = request.getParameterNames();
		while (paramEnumerator.hasMoreElements()) {
			String roleNameToUpdate = paramEnumerator.nextElement();
			boolean isOn = ON.equalsIgnoreCase(request.getParameter(roleNameToUpdate));
			
			if(roleNameToUpdate.startsWith(GhixJasyptEncrytorUtil.GENC_TAG_START) && roleNameToUpdate.endsWith(GhixJasyptEncrytorUtil.GENC_TAG_END)) {
				roleNameToUpdate = ghixJasyptEncrytorUtil.decryptGencStringByJasypt(roleNameToUpdate);
			}
			
			if(dbRoleNameList.contains(roleNameToUpdate) && (isOn)){
				
				
				lstRolesToUpdate.add(roleNameToUpdate);
			}
		}
		
		//Set the value so that we can get the default empty list from this map.
		editUserRoleData.put("rolesToUpdate", lstRolesToUpdate);
		
		
		 
		  if(!isValid || editUserRoleData.isEmpty() ){
			  throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		  }
		return editUserRoleData;
		
	}
	/*
	 * END : Test RequestMapping(s) - by Venkata Tadepalli
	 */

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// System.out.println("START :: AdminController to test Ghix-SSo Test..");

		String response = "";
		String userName = "jscott@broker.com";
		// String ghixWsUrl =
		// "http://localhost:9090/ghix-enrollment/enrollment/searchenrollment";
		// //For POST
		String ghixWsUrl = "http://localhost:9090/ghix-enrollment/enrollment/welcome";// For
																						// GET
		GhixRestTemplate ghixRestTemplate = new GhixRestTemplate();

		Map<String, String> uriVariables = new HashMap<String, String>();
		uriVariables.put("orderId", 1 + "");
		uriVariables.put("pin_esig", "pin_esig");

		// _POST_RESP = customRestTemplate.exchange(ghixWsUrl, method,null,
		// String.class, uriVariables);

		// ResponseEntity responseEntity =
		// customRestTemplate.exchange(ghixWsUrl, userName,HttpMethod.POST,
		// MediaType.APPLICATION_XML,String.class, uriVariables);
		ResponseEntity responseEntity = ghixRestTemplate.exchange(ghixWsUrl, userName, HttpMethod.GET, MediaType.APPLICATION_JSON, String.class, uriVariables);
		response = (String) responseEntity.getBody();

		// System.out.println("Response From Service ::"+response);

	}
	
	@RequestMapping(value = "/admin/editTemplate/{id}", method = RequestMethod.GET )
	@PreAuthorize("hasPermission(#model, 'ADMIN_EDIT_NOTICES')")
	public String editTemplate(@PathVariable("id") Integer id, Model model, HttpServletRequest request) {
		NoticeType noticeType = noticeTypeRepo.findById(id);
        String templateContent=null;
	    
        String ecmContentId = noticeType.getExternalId();
        List<NoticesElements> elementsList = new ArrayList<NoticesElements>();
	    
		try {
			
			if (StringUtils.isEmpty(noticeType.getExternalId())) {
				ecmContentId = noticeService.getEcmContentId(noticeType);
			}
			
			InputStream inputStreamUrl = new ByteArrayInputStream(
					ecmService.getContentDataById(ecmContentId));
			
			templateContent = IOUtils.toString(inputStreamUrl, "UTF-8");
			List<NoticesTypesElements> noticesTypesElementsList =  noticesTypesElementsService.getAllElementsByNoticeType(noticeType);
			
			for (NoticesTypesElements noticesTypesElements : noticesTypesElementsList) {
				NoticesElements elements = new NoticesElements();
				elements = noticesTypesElements.getNoticesElements();
				elementsList.add(elements);
			}
			
		}
		catch (ContentManagementServiceException e) {
			logger.error("Error is getting template content at ECM service ",e);
			model.addAttribute(ERROR_MSG,"Error while getting template. "+e.getMessage()+".\n It can be case a that your template("+noticeType.getNotificationName()+") is not availble on ECM Server.");
		} catch (IOException e) {
			model.addAttribute(ERROR_MSG,"Error while getting template. "+e.getMessage());
			logger.error("Error in copying template content at ECM service ",e);
		}	
		model.addAttribute("template",templateContent);
		model.addAttribute("templateId",id);
		model.addAttribute("templateName",noticeType.getNotificationName());
		
		model.addAttribute("elementsList",elementsList);
		
		

		return "admin/editTemplate";

	}

	@SuppressWarnings("all")
	@RequestMapping(value = "/admin/prepareTemplateReview", method = {RequestMethod.POST,RequestMethod.GET})
	@PreAuthorize("hasPermission(#model, 'ADMIN_EDIT_NOTICES')")
	@ResponseBody
	public byte[] getNoticeTypeElements(
			@RequestParam String description,
			@RequestParam String templateId,
			HttpServletResponse response, HttpServletRequest request) {
		response.setContentType("application/pdf");
		Gson gson = new Gson();
		byte[] templatePreviewContent=null;
		try {
			Map templateContentMap = new HashMap();
			templateContentMap = gson.fromJson(StringEscapeUtils.unescapeHtml(templateId),
					templateContentMap.getClass());
			String updatedTemplate = (String) templateContentMap.get("encodedHtml");
			Map<String,Object> map = new HashMap<String, Object>();
			String newTaskObject = StringEscapeUtils.unescapeHtml(description);
			map = new Gson().fromJson(newTaskObject, map.getClass());
			templatePreviewContent = noticeService.createPreviewContent(updatedTemplate, map);
			request.getSession().setAttribute(TEMPLATE_PREVIEW, templatePreviewContent);
		} catch (Exception e) {
			logger.error("Error in Template Preview Generation: "+e.getMessage());
			templatePreviewContent=null;
		}
		
		return templatePreviewContent;

	}
	
	@SuppressWarnings("all")
	@RequestMapping(value = "/admin/returnTemplateReview", method = {RequestMethod.POST,RequestMethod.GET})
	@PreAuthorize("hasPermission(#model, 'ADMIN_EDIT_NOTICES')")
	@ResponseBody
	public void returnTemplateReview(HttpServletResponse response, HttpServletRequest request) {
		try {
			byte[] templatePreviewContent=TEMPLATE_REVIEW_ERROR;
			if(request.getSession().getAttribute(TEMPLATE_PREVIEW)!=null){
				templatePreviewContent = (byte[])request.getSession().getAttribute(TEMPLATE_PREVIEW);
				request.getSession().removeAttribute(TEMPLATE_PREVIEW);
			}
			response.setContentType("application/pdf");
			FileCopyUtils.copy(templatePreviewContent, response.getOutputStream());
			    
		} catch (Exception e) {
			logger.error("");
		}
	}
	
	
	@RequestMapping(value = "/admin/editTemplate", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_EDIT_NOTICES')")
	public void saveTemplate(
			@RequestParam(value = "description") String description,
			@RequestParam(value = "templateId") String templateId,
			HttpServletRequest request) {
		try {
			Gson gson = new Gson();
			@SuppressWarnings("rawtypes")
			Map map = new HashMap();
			map = gson.fromJson(StringEscapeUtils.unescapeHtml(description),
					map.getClass());
			String updatedTemplate = (String) map.get("encodedHtml");
			byte[] updatedTemplateBytes = updatedTemplate.getBytes();
			NoticeType noticeType = noticeTypeRepo.findById(Integer
					.parseInt(templateId));
			String ecmTemplateFolderPath = DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH);
			ecmTemplateFolderPath += noticeType.getTemplateLocation();
			String updatedExternalId =  ecmService.updateContent(noticeType.getExternalId(),
					noticeType.getTemplateLocation(), updatedTemplateBytes);
			logger.debug("Extern id Updated as "+updatedExternalId);
			noticeType.setExternalId(updatedExternalId);
			noticeType.setUpdated(new TSDate());
			noticeTypeRepo.save(noticeType);
			request.getSession().setAttribute(
					"Template_Updt_Msg",
					"Template (" + noticeType.getNotificationName()
							+ ") has been updated successfully ");
		} catch (Exception e) {
			logger.error("Error in updating template content at ECM service ",
					e);
			request.getSession().setAttribute("Template_Updt_Msg",
					"Error while updating " + e);
		}

	}
	
	@RequestMapping(value="/admin/user/privileges",method = RequestMethod.GET, produces="application/json")
	@GiAudit(transactionName = "Load Privileges", eventType = EventTypeEnum.PII_READ , eventName = EventNameEnum.ROLE_PROVISIONING)
	@PreAuthorize("hasPermission(#model, 'USER_PROVISIONING')")
	@ResponseBody
	public ResponseEntity<String> loadPrivileges(HttpServletResponse response, HttpServletRequest request){
		try {
			logger.debug("load privileges service");
			String token = null;
			String responseStr = this.acessControl.loadAllPrivileges(this.roleService.findAll());
				//Its a valid Response
			Object csrftokenObj = request.getSession().getAttribute(CSRFTokenTag.CSRF_PARAMETER_NAME);
			if(csrftokenObj == null){
				token = CSRFToken.getNewToken();
				request.getSession().setAttribute(CSRFTokenTag.CSRF_PARAMETER_NAME, token);
			}else{
				token = (String)csrftokenObj;
			}
			HashMap<String, String> csrfHeader = new HashMap<>(1);
			csrfHeader.put("csrftoken", token);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Retrieved Privileges successfully"));
			return Utils.makeResponse(responseStr, HttpStatus.OK, csrfHeader, MediaType.APPLICATION_JSON);
		} catch (Exception e) {
			logger.error("Error occured retrieving privileges",e);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error occured retrieving privileges"));
			return Utils.makeResponse("Error while retrieving privileges", HttpStatus.BAD_REQUEST, null, MediaType.APPLICATION_JSON);
		}
	}
	
	@RequestMapping(value="/admin/user/updatePrivileges",method = RequestMethod.POST, produces="application/json")
	@GiAudit(transactionName = "Update Privileges", eventType = EventTypeEnum.PII_WRITE , eventName = EventNameEnum.ROLE_PROVISIONING)
	@PreAuthorize("hasPermission(#model, 'USER_PROVISIONING')")
	@ResponseBody
	public ResponseEntity<String> updatePrivileges(HttpServletRequest request, @RequestBody String privJsonString){
		
		try {
			logger.debug("Received privilege update request:"+privJsonString);
			String token = null;
			HashMap<String, Role> allRolesMap = new HashMap<String, Role>();
			List<Role> allRoles = this.roleService.findAll();
			logger.debug("Retrieved "+allRoles.size()+" existing roles");
			GiAuditParameterUtil.add(new GiAuditParameter("privilege to be updated ", privJsonString),
					new GiAuditParameter("Existing role size retrieved ", allRoles.size()));
			for(Role tmp:allRoles){
				allRolesMap.put(tmp.getName(), tmp);
			}
			Object csrftokenObj = request.getSession().getAttribute(CSRFTokenTag.CSRF_PARAMETER_NAME);
			if(csrftokenObj == null){
				token = CSRFToken.getNewToken();
				request.getSession().setAttribute("csrftoken", token);
			}else{
				token = (String)csrftokenObj;
			}
			HashMap<String, String> csrfHeader = new HashMap<>(1);
			csrfHeader.put(CSRFTokenTag.CSRF_PARAMETER_NAME, token);
			List<RoleProvisioningRule> savedList = this.acessControl.saveOrUpdateAllPrivileges(privJsonString, allRolesMap);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Privilege role updated successfully"));
			return Utils.makeResponse(this.acessControl.getListAsJson(savedList),HttpStatus.OK,csrfHeader,MediaType.APPLICATION_JSON);
		} catch (Exception e) {
			logger.error("Error occured while updating privileges.",e);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error occured while updating privileges"));
			return Utils.makeResponse("Error while updating privileges", HttpStatus.BAD_REQUEST, null, MediaType.APPLICATION_JSON);
		}
	}
	
	@RequestMapping(value="/admin/manage/user/roles/edit", method=RequestMethod.POST)
	@GiAudit(transactionName = "Edit User Roles", eventType = EventTypeEnum.USER_ACCOUNTS, eventName = EventNameEnum.MODIFY_ACCOUNTS)
	@PreAuthorize("hasPermission(#model, 'MANAGE_USERS')")
	public String editUserRoles(Model model, HttpServletRequest request) {
		AccountUser user = userService.findById(Integer.parseInt(request
				.getParameter("id")));
		List<UserRole> actualUserRolesForEdit = null;
		List<GiAuditParameter> auditParameters = new ArrayList<GiAuditParameter>();
		
		// Validate the Role name and status received from the client
		Enumeration<String> paramEnumerator = request.getParameterNames();
		HashSet<String> browserReceivedRoleSet = new HashSet<String>();
		while (paramEnumerator.hasMoreElements()) {
			String roleNameToAdd = paramEnumerator.nextElement();
			boolean isOn = ON.equalsIgnoreCase(request
					.getParameter(roleNameToAdd));
			if (roleNameToAdd.startsWith(GhixJasyptEncrytorUtil.GENC_TAG_START) && roleNameToAdd.endsWith(GhixJasyptEncrytorUtil.GENC_TAG_END)) {
				roleNameToAdd = ghixJasyptEncrytorUtil
						.decryptGencStringByJasypt(roleNameToAdd);
				if (isOn) {
					logger.debug("Browser received role:" + roleNameToAdd);
					browserReceivedRoleSet.add(roleNameToAdd);
				}
			}
		}
		String existingRoleNameToEvaluate = null;
		actualUserRolesForEdit = this.userRoleService.findByUserForUpdate(user);
		Iterator<UserRole> userRoleCursor = actualUserRolesForEdit.iterator();
		UserRole existingUserRole = null;
		boolean isDefault = false;
		boolean isActive = false;
		try {
			int rolesRemovedCounter = 1;
			while (userRoleCursor.hasNext()) {
				existingUserRole = userRoleCursor.next();
				// Check if this role is received from the form submit
				existingRoleNameToEvaluate = existingUserRole.getRole()
						.getName();
				isDefault = existingUserRole.isDefaultRole();
				isActive = existingUserRole.isActiveUserRole();
				logger.debug("Evaluating existing user role:"
						+ existingRoleNameToEvaluate + " isDefault:"
						+ isDefault);
				if (browserReceivedRoleSet.contains(existingRoleNameToEvaluate)
						|| isDefault) {
					// User still has this role, no change //check if it is active
					logger.debug("User still has the role:"
							+ existingRoleNameToEvaluate
							+ " checking if it is active");
					if(isActive){
						browserReceivedRoleSet.remove(existingRoleNameToEvaluate);
					}else{
						logger.debug(existingRoleNameToEvaluate+" is not active,and received from the browser, should be added now");
					}
					continue;
				}
				// Reaching here means, user no longer has this role
				logger.debug("Removing role:"
						+ existingUserRole.getRole().getName());
				if (this.userRoleService.removeUserRoleFromUser(existingUserRole)) {
					sessionTracker.killUserActiveSession(existingUserRole.getUser().getId());
					/*GiAuditParameterUtil.add(new GiAuditParameter("userId ",
							user.getId()),new GiAuditParameter("userName ",
									user.getUserName()), new GiAuditParameter("Role removed "+rolesRemovedCounter,
									existingRoleNameToEvaluate));*/
					auditParameters.add(new GiAuditParameter("userId ", user.getId()));
					auditParameters.add(new GiAuditParameter("userName ", user.getUserName()));
					auditParameters.add(new GiAuditParameter("Role removed "+rolesRemovedCounter, existingRoleNameToEvaluate));
					rolesRemovedCounter++;
				} else {
					logger.error("Error occured while adding/modifing the role.");
					throw new GIRuntimeException(
							"Failed while removing the role");
				}
			}
			// Now add the new roles if any
			String roleToBeAdded = null;
			int rolesAddedCounter = 1;
			int totalArolesToBeAdded = browserReceivedRoleSet.size();
			logger.debug("Going to add:"+totalArolesToBeAdded+" roles received");
			if (totalArolesToBeAdded > 0) {
				Iterator<String> addRoleCursor = browserReceivedRoleSet
						.iterator();
				while (addRoleCursor.hasNext()) {
					roleToBeAdded = addRoleCursor.next();
					logger.debug("Adding role:"
							+ roleToBeAdded);
					this.userService.addUserRole(user, roleToBeAdded);
					/*GiAuditParameterUtil.add(new GiAuditParameter("userId ",
							user.getId()),new GiAuditParameter("userName ",
									user.getUserName()), new GiAuditParameter("Role Added "+rolesAddedCounter,
							roleToBeAdded));*/
					auditParameters.add(new GiAuditParameter("userId ", user.getId()));
					auditParameters.add(new GiAuditParameter("userName ", user.getUserName()));
					auditParameters.add(new GiAuditParameter("Role Added "+rolesAddedCounter, roleToBeAdded));
					rolesAddedCounter++;
				}
			}
			
			List <GiAuditParameter> duoAuditParams =  this.userRoleService.toggleDuoAssociation(user);
			if(!duoAuditParams.isEmpty()){
				for(GiAuditParameter auditParameter : duoAuditParams ){
					GiAuditParameterUtil.add(auditParameter);
				}
			}
			
			if(!auditParameters.isEmpty()){
				for(GiAuditParameter auditParameter : auditParameters ){
					GiAuditParameterUtil.add(auditParameter);
				}
			}
			
		} catch (Exception e) {
			logger.error("Error occured while adding/modifing the role.",e);
			throw new GIRuntimeException("PLATFORM-00002", e, null,
					Severity.HIGH);
		}
		return "redirect:/admin/manage/users";
	}
	
	@RequestMapping(value = "/admin/manage/user/getduostatus", method = RequestMethod.GET)
	@ResponseBody
	@PreAuthorize("hasPermission(#model, 'USER_PROVISIONING')")
	public String getDuoStatus(@RequestParam String userName) {
		JSONObject userDuo;
        userDuo = DuoAdminApi.retrieveUserByUsername(userName);
        if(null != userDuo ){
        	return userDuo.toString() ;
        }
		return "No data exist for user with email :"+userName;
	}

	
	private boolean pingServer(String serverName) throws IOException {
		boolean available = false;
		Socket t = null;
		 try {
		      InetAddress inetAddress = InetAddress.getByName(new URL(serverName).getHost());
		      
		      if(inetAddress != null && inetAddress.isReachable(5000))
		    	  available =  true;
		    }
		    catch (Exception e) {
		    	logger.info("Error in pingServer: ", e.getMessage());
		    }
		 return available;
		    }
	

	
	
	
}
