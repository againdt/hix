<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%> 
<style type="text/css">
	#sidebar .csrm-sidebar  li{border: 1px solid #E9E9E9;}
</style>
<div class="gutter10">
	<div class="row-fluid">
        <h1 class="span10 margin0 gutter20-b"><spring:message code="label.multiplier"/></h1>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message code="label.userfulInfo"/></h4>
       		</div>
       		<ul class="nav nav-list graybg csrm-sidebar">
	       		<li> 
		       		<spring:message code="label.csrInfo"/>
				</li> 
			</ul>
        </div>
		<div class="span9" id="rightpanel">
       		<form action="<c:url value="/admin/planmgmt/csrmultiplier/update"/>" name="editMultiplierForm" id="editMultiplierForm" method="post">
       			<df:csrfToken/>
       			<input type="hidden" name="csrMultiplier" id="csrMultiplier" value="${csrMultiplierTblId.id}">
				<display:table id="csrMultiplierTblId" name="csrMultipliers" list="csrMultipliers" sort="list" class="table table-condensed table-border-none table-striped">
		           <display:column titleKey="label.planCSRVariation" property="CSRVariation" sortable="false"/>
		           <display:column titleKey="label.metalTier" property="planLevel" sortable="false" />
		           <display:column titleKey="label.csrMultiplier" sortable="false">
		           		<input type="hidden" value="${csrMultiplierTblId.id}">
		           		<input type="text" name="multiplier" value="${csrMultiplierTblId.multiplier}" class="input-small txt-center">
		           </display:column>
				</display:table>
				<div class="control-group">
					<div class="span6 txt-center"><a class="btn btn-primary" role="button" href="<c:url value="/admin/planmgmt/csrmultiplier/manage"/>"><spring:message code="label.btnCancel"/></a></div>
					<div class="span6"><input type="submit" value="<spring:message code="label.btnSave"/>" class="btn btn-primary" onclick="collectData();"></div>
				</div> 
       		</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$.validator.addMethod("validMultiplier", function(value, element, param) { 
		var num = parseFloat(value);
		var pre = value.split(".");
		if(pre.length == 1 || pre[1].length > 2 || num >= 1){
			return false;
		} else {
			return true;
		}
	});
	
	$(document).ready(function(){
	    $("#editMultiplierForm").validate();
		$("input[name^=multiplier]").each(function() {
			$(this).rules("add", {number:true,required:true,validMultiplier:true,
			messages: {
				required: "<spring:message code='error.multiplier'/>",
				validMultiplier : "<spring:message code='error.invalidMultiplier'/>"
			}
	    });
	 });
	});
	
	function collectData(){
		var tableObj = document.getElementById("csrMultiplierTblId");
		var index = tableObj.rows.length;
		var multiplier = "";
		for(var i = 1; i < index; i++) {
			multiplier = multiplier + tableObj.rows[i].cells[2].children[0].value+"_"+tableObj.rows[i].cells[2].children[1].value+"@";
		}
		$("#csrMultiplier").val(multiplier.trim());
	}
</script>