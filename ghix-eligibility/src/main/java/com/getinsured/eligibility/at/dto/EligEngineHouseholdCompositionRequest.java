package com.getinsured.eligibility.at.dto;

/**
 * Request Object for the Eligibility Engine Household Composition API
 * 
 * @author Supreet Bhandari
 * @since 29 July, 2019
 *
 */
public class EligEngineHouseholdCompositionRequest {

	private long applicationId;
	private String requestId;

	public long getApplicationId() {
		return applicationId;
	}
	
	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public void setApplicationId(long applicationId) {
		this.applicationId = applicationId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(", applicationId=");
		builder.append(applicationId);
		builder.append(", requestId=");
		builder.append(requestId);
		builder.append("]");
		return builder.toString();
	}
}
