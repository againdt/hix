<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
<script type="text/javascript">
      $(document).ready(function() {
          $( "#doctor-city" ).autocomplete({
              source: '${pageContext. request. contextPath}/get_Location_list'
          });

      });
</script>
<style type="text/css">
	body {
	    background: none repeat scroll 0 0 #FFFFFF;
	}
	.bottom-b {
	    border-bottom: 1px solid #999999;
	}
	.container {
	    margin-top: -40px;
	    padding: 0 13px;
	    width: 97%;
	}
	form.form-horizontal .control-label {width: 75px;}
	form.form-horizontal .search-field .controls{margin-left: 80px;}
	form.form-horizontal #submit {margin-right: 55px;}
</style>
<body>
<div class="container">
        <c:choose>
        <c:when test="${domElement == 'doctors' || domElement == 'DOCTOR'}">
        	<div class="row-fluid gutter40-tb">
            	<h3 class="bottom-b"><spring:message code='label.findYourDoctor'/></h3>
        	</div>
	        <div class="row-fluid">
	        	<form class="form-horizontal" id="doctorForm" name="doctorForm" action="<c:url value="/provider/doctors/search" />" method="POST">
	                <!-- page header -->
	                <input type="hidden" name="defaultZip" value="${defaultZip}">
	                <div class="row-fluid search-field">
	                    <div class="control-group span5">
	                        <label class="control-label" for="doctor-name"><spring:message code='label.name'/></label>
	                        <div class="controls">
	                            <input type="text" id="doctor-name" name="doctorName" placeholder="Type your doctor's name">
	                        </div>
	                    </div>
	                    <div class="control-group span5">
	                        <label class="control-label" for="doctor-city"><spring:message code='label.near'/></label>
	                        <div class="controls">
	                            <input type="text" id="doctor-city" name="location" placeholder="Type city or zipcode">
	                        </div>
	                    </div>
	                </div>
	                <div class="row-fluid">
	                    <div class="control-group">
	                        <div id="submit" class="controls">
	                            <input type="submit" class="btn btn-primary pull-right" value="<spring:message code='label.search'/>" id="submit" name="submit" title="<spring:message code='label.search'/>">
	                        </div>
	                    </div><!-- end of control-group -->
	                </div>
	            </form>
	        </div>
	       </c:when>
	       <c:when test="${domElement == 'facilities'}">
        	<div class="row-fluid gutter40-tb">
            	<h3 class="bottom-b"><spring:message code='label.findYourFacility'/></h3>
        	</div>
	        <div class="row-fluid">
	        	<form class="form-horizontal" id="FacilityForm" name="facilityForm" action="<c:url value="/provider/facilities/search" />" method="POST">
	        		<input type="hidden" name="defaultZip" value="${defaultZip}">
	                <!-- page header -->
	                <div class="row-fluid search-field">
	                    <div class="control-group span5">
	                        <label class="control-label" for="doctor-name"><spring:message code='label.name'/></label>
	                        <div class="controls">
	                            <input type="text" id="doctor-name" name="facilityName" placeholder="Type your facility's name">
	                        </div>
	                    </div>
	                    <div class="control-group span5">
	                        <label class="control-label" for="doctor-city"><spring:message code='label.near'/></label>
	                        <div class="controls">
	                            <input type="text" id="doctor-city" name="location" placeholder="Type city or zipcode">
	                        </div>
	                    </div>
	                </div>
	                <div class="row-fluid">
	                    <div class="control-group">
	                        <div id="submit" class="controls">
	                            <input type="submit" class="btn btn-primary pull-right" value="<spring:message code='label.search'/>" id="submit" name="submit" title="<spring:message code='label.search'/>">
	                        </div>
	                    </div><!-- end of control-group -->
	                </div>
	            </form>
	        </div>
	       </c:when>
	       <c:otherwise>
	       	   <div class="row-fluid gutter40-tb">
            	   <h3 class="bottom-b"><spring:message code='label.findYourDentist'/></h3>
        	   </div>
		       <div class="row-fluid">
		        	<form class="form-horizontal" id="dentistForm" name="dentistForm" action="<c:url value="/provider/dentists/search" />" method="POST">
		                <!-- page header -->
		                <input type="hidden" name="defaultZip" value="${defaultZip}">
		                <div class="row-fluid search-field">
		                    <div class="control-group span5">
		                        <label class="control-label" for="facility-name"><spring:message code='label.name'/></label>
		                        <div class="controls">
		                            <input type="text" id="dentist-name" name="dentistName" placeholder="Type your doctor's name">
		                        </div>
		                    </div>
		                    <div class="control-group span5">
		                        <label class="control-label" for="facility-city"><spring:message code='label.near'/></label>
		                        <div class="controls">
		                            <input type="text" id="doctor-city" name="location" placeholder="Type city or zipcode">
		                        </div>
		                    </div>
		                </div>
		                <div class="row-fluid">
		                    <div class="control-group">
		                        <div id="submit" class="controls">
		                            <input type="submit" class="btn btn-primary pull-right" value="<spring:message code='label.search'/>" id="submit" name="submit" title="<spring:message code='label.search'/>">
		                        </div>
		                    </div><!-- end of control-group -->
		                </div>
		            </form>
		        </div>
	       </c:otherwise>
        </c:choose>
        <!-- end of row-fluid -->
</div>
 <script type="text/javascript">
 $('#doctor-name').val('Type your doctor\'s name');
 $('#doctor-name').css("color", "rgb(204, 190, 190)");
 $('#doctor-city').val('Type city or zipcode');
 $('#doctor-city').css("color", "rgb(204, 190, 190)");
 $('#dentist-name').val('Type your doctor\'s name');
 $('#dentist-name').css("color", "rgb(204, 190, 190)");
 $('#doctor-name').focus(function(){
	    if($(this).val() == 'Type your doctor\'s name'){
	        $(this).val('');
	    }
	}).blur(function(){
	    //check for empty input
	    if($(this).val() == ''){
	        $(this).val('Type your doctor\'s name');
	        $(this).css("color", "rgb(204, 190, 190)");
	    }
	});
 $('#doctor-city').focus(function(){
	    if($(this).val() == 'Type city or zipcode'){
	        $(this).val('');
	    }
	}).blur(function(){
	    //check for empty input
	    if($(this).val() == ''){
	        $(this).val('Type city or zipcode');
	        $(this).css("color", "rgb(204, 190, 190)");
	    }
	});
 $('#dentist-name').focus(function(){
	    if($(this).val() == 'Type your doctor\'s name'){
	        $(this).val('');
	    }
	}).blur(function(){
	    //check for empty input
	    if($(this).val() == ''){
	        $(this).val('Type your doctor\'s name');
	        $(this).css("color", "rgb(204, 190, 190)");
	    }
	});
 $(document).ready(function () {
     $("#doctor-name").keypress(function () {
         $("#doctor-name").css("color", "black");
         
     });
     $("#doctor-city").keypress(function () {
         $("#doctor-city").css("color", "black");
         
     });
     $("#dentist-name").keypress(function () {
         $("#dentist-name").css("color", "black");
         
     });
 });
 </script>
</body>
