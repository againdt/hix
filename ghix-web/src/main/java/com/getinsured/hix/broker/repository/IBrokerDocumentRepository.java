/**
 * 
 */
package com.getinsured.hix.broker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import com.getinsured.hix.model.BrokerDocuments;

/**
 * IBrokerDocumentRepository
 * 
 */
public interface IBrokerDocumentRepository extends RevisionRepository<BrokerDocuments, Integer, Integer>, JpaRepository<BrokerDocuments, Integer> {
	
	/**
	 * Fetches the BrokerDocuments record for the given document Id
	 * 
	 * @param docID
	 *            the document id
	 * @return the BrokerDocuments record for the given document Id
	 */
	BrokerDocuments findById(int docID);

}
