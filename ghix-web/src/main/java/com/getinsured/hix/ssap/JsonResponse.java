package com.getinsured.hix.ssap;

 
public class JsonResponse {
	
        private String status = null;
        private Object object = null;
        public String getStatus() {
               return status;
	    }
        public void setStatus(String status) {
	                this.status = status;
        }
		public Object getObject() {
			return object;
		}
		public void setObject(Object object) {
			this.object = object;
		}
 
	}
