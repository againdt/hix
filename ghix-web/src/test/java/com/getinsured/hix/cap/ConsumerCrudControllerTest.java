package com.getinsured.hix.cap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.getinsured.hix.account.web.UserControllerMockMvcTest.MockSecurityContext;
import com.getinsured.hix.platform.test.GhixBaseMvcTest;

public class ConsumerCrudControllerTest extends GhixBaseMvcTest {

	private MockMvc mockMvc;
	private MockHttpSession session = new MockHttpSession();
    @Resource
    private FilterChainProxy springSecurityFilterChain;
    
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void mySetUp() {

    	// setup mock MVC
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .addFilters(this.springSecurityFilterChain)
                .build();
        
//		UsernamePasswordAuthenticationToken principal = 
//		userController.getPrincipal("exadmin@ghix.com");
//		SecurityContextHolder.getContext().setAuthentication(principal);        

//		session.setAttribute(
//    		  HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, 
//    		  new MockSecurityContext(principal));
        
    }
    @Test
    public void viewAdd() throws Exception {
        mockMvc.perform(get("/cap/consumer/addconsumer")
                .session(session))
        .andExpect(status().isOk());
	}
	
}
