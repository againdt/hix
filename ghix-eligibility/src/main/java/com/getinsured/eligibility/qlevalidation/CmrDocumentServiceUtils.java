package com.getinsured.eligibility.qlevalidation;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.iex.ssap.repository.IConsumerDocumentRepository;

@Service("cmrDocumentServiceUtils")
public class CmrDocumentServiceUtils {
	
	public enum CmrTargetName {SSAP_APPLICANT_EVENTS}; 
	
	public enum AcceptedValue {Y, N}; 
	
	@Autowired private IConsumerDocumentRepository consumerDocumentRepository;
	
	private void updateCmrDocuments(CmrTargetName targetName, Long targetId, AcceptedValue acceptedValue, String comments){
		List<ConsumerDocument> consumerDocuments = consumerDocumentRepository.findByTargetNameAndTargetId(targetName.name(), targetId);
		
		boolean cmrDocUpdated = false;
		for (ConsumerDocument consumerDocument : consumerDocuments) {
			if (StringUtils.isEmpty(consumerDocument.getAccepted())){
				consumerDocument.setAccepted(acceptedValue.name());
				consumerDocument.setComments(comments);
				cmrDocUpdated = true;
			}
		}
		
		if (cmrDocUpdated){
			consumerDocumentRepository.save(consumerDocuments);
		}
	}
	
	public void updateCmrDocuments(CmrTargetName targetName, List<Integer> targetIds, AcceptedValue acceptedValue, String comments){
		
		for (Integer targetId : targetIds) {
			if (targetId != null){
				updateCmrDocuments(targetName, targetId.longValue(), acceptedValue, comments);
			}
		}
		
	}

}
