<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page import="com.getinsured.hix.platform.util.GhixPlatformConstants" %>
<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="author" content="" />
<%
String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
request.setAttribute("trackingCode",trackingCode);
String gtmContainerID = GhixPlatformConstants.GTM_CONTAINER_ID;
request.setAttribute("gtmContainerID",gtmContainerID);
%>
<title><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%></title>
 <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico"/>

 <link href="<c:url value="/resources/css/bootstrap-responsive.min.css" />" rel="stylesheet"  type="text/css" />
 <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet"  type="text/css" />
 <link href="<c:url value="/resources/css/formvalidation.css" />" rel="stylesheet"  type="text/css" />
 <link href="<c:url value="/resources/css/bootstrap-responsive.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
 <link href="<c:url value="/resources/css/bs3-responsive-classes.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
 <link href="<c:url value="/resources/css/ghixcustom.css" />" rel="stylesheet"  type="text/css" />
 <link href="/hix/resources/css/iframe.css" rel="stylesheet"  type="text/css" />

<!-- <link href="<c:url value="/resources/css/docs.css" />" rel="stylesheet"  type="text/css" />-->

 <link href="<c:url value="/resources/css/jquery-ui-1.8.16.custom.css" />" rel="stylesheet"  type="text/css" />
 <link href="<c:url value="/resources/css/jquery.ui.datepicker.css" />" rel="stylesheet"  type="text/css" />
 <link href='//fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
 <link href="<c:url value="/resources/css/accessibility.css" />" media="screen" rel="stylesheet" type="text/css" />


 <script type="text/javascript" src="<c:url value="/resources/js/jquery-1.7.min.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.16.custom.min.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/ghixcustom.js" />"></script>

 <!--[if lt IE 9]>
	<script type="text/javascript" src="<c:url value="/resources/js/html5.js" />"></script>
 <![endif]-->

 <!-- Google Tag Manager -->
<script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','${gtmContainerID}');
</script>

<!-- End Google Tag Manager -->

</head>
<body>
    <input type="hidden" name="exchangeName" id="exchangeName" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>">
    <input type="hidden" name="stateCode" id="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" />
	<div>
		<tiles:insertAttribute name="main" />
	</div>
    <!-- Entity Shell -->
    <c:if test="${not empty fn:trim(trackingCode)}">
    <script type="text/javascript">
    //need to put _gaq variable declaration to global scope
    var _gaq = _gaq || [];
    var gaPageName = null;
    var googleAnalyticsTrackingCodes = '${trackingCode}';
    var listOfGoogleAnalyticsTrackingCodes = googleAnalyticsTrackingCodes.split(',');
    function triggerGoogleTrackingEvent(eventData) {
      for (i = 0; i < listOfGoogleAnalyticsTrackingCodes.length; i++) {
          var updatedEventData = [];
          var namespacePrefix = i + "ga.";
          for (j = 0; j < eventData.length; j++) {
              var eventDataElement = eventData[j];
              if(j == 0) {
                  eventDataElement = namespacePrefix + eventData[j];
              }
              updatedEventData[j] = eventDataElement;
          }
          _gaq.push(updatedEventData);
      }
    }

     var errPage = false;
    $("img").each(function() {
        if ($(this).attr('src') && $(this).attr("src").slice(-13) == "404-error.png") {
           errPage = true;
        };
    });

    for (i = 0; i < listOfGoogleAnalyticsTrackingCodes.length; i++) {
        var domainName = window.location.hostname;
        domainName = domainName.replace('www.', '');
        var namespacePrefix = i + "ga.";
        _gaq.push([namespacePrefix + '_setAccount', listOfGoogleAnalyticsTrackingCodes[i]]);
        _gaq.push([namespacePrefix + '_setDomainName', domainName]);
        _gaq.push([namespacePrefix + '_setAllowLinker', true]);
        _gaq.push([namespacePrefix + '_addIgnoredRef', domainName]); // Stops Self Referrals

        if (errPage) {
            _gaq.push([namespacePrefix + '_trackPageview', '/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer]);
        } else {
            _gaq.push([namespacePrefix + '_trackPageview', location.pathname + location.search + location.hash]);
        }
    }

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

    </script>
    </c:if>
</body>
</html>
