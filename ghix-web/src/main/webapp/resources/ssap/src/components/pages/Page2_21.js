import React, { Fragment, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

// Components

import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';

// ActionCreators

import { changeData } from '../../actions/actionCreators';

// Utils

import cloneObject from '../../utils/cloneObject';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import { getCoverageProgramName } from '../../utils/getCoverageProgramName';

// Constants

import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import EMPTY_OBJECTS from '../../constants/emptyObjects';
import INPUT_TYPES from '../../constants/inputTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';
import { VALIDATION_ERROR_MESSAGES } from '../../constants/validationErrorMessages';




/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Additional Information - Member Pregnant Details ' Page
 *
 */
const Page2_21= props => {

    const {
        data,
        householdMembers,
        form: {
            Form: {
                isEligiblyForSave,
                isFormSubmitted
            }
        },
        isFirstVisit
    } = props;


    const [localState, setLocalState] = useState({
        noneOfTheAbove: isFirstVisit ?  false : householdMembers.every(member => !member.medicaidChipDenial)
    });

    const { noneOfTheAbove } = localState;

    const onFieldChange_member = (...args) => {

        let [ newData, indexInArray, , [ name, value ]  ] = args;

        setLocalState(state => {
            return {
                ...state,
                noneOfTheAbove: false
            };
        });
        const newState = {...data };

        newData = {
            ...newData,
            fiveYearBar: false
        };

        switch(true) {
        case name === 'medicaidChipDenial' && !value:
            newData = {
                ...newData,
                ...cloneObject(EMPTY_OBJECTS.MEDICAID_CHIP_DENIAL)
            };
            break;
        case name === 'medicaidDeniedDueToImmigration':
            newData.changeInImmigrationSince5Year = null;
            newData.changeInImmigrationSinceMedicaidDenial = null;
            break;
        case name === 'changeInImmigrationSince5Year':
            newData.changeInImmigrationSinceMedicaidDenial = null;
            break;
        }

        const valuesForWatch = [
            newData.medicaidDeniedDueToImmigration,
            newData.changeInImmigrationSince5Year,
            newData.changeInImmigrationSinceMedicaidDenial
        ];

        newData.fiveYearBar = newData.medicaidChipDenial && valuesForWatch.reduce((res, v, idx) => {
            if(v === false && idx === 0) {
                return false;
            }
            if(v === false) {
                return true;
            }
            return res;
        }, false);

        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray] = {...newData};
        props.actions.changeData(newState);
    };

    const onFieldChange_none_of_above = newData => {
        setLocalState(state => ({
            ...state,
            ...newData
        }));
        const newState = {...data };

        const updatedHouseholdMembers =
            householdMembers
                .map(member => {
                    member = {
                        ...member,
                        ...cloneObject(EMPTY_OBJECTS.MEDICAID_CHIP_DENIAL)
                    };
                    return member;
                });
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember = updatedHouseholdMembers;

        props.actions.changeData(newState);
    };

    const getHouseholdMembers = (householdMembers) => {

        const combinedMembers = [];

        const { noneOfTheAbove} = localState;

        householdMembers.forEach((member, i) => {

            const {
                name,
                medicaidChipDenial,
                medicaidDeniedDate,
                medicaidDeniedDueToImmigration,
                changeInImmigrationSince5Year,
                changeInImmigrationSinceMedicaidDenial,
                applyingForCoverageIndicator,
                citizenshipImmigrationStatus: {
                    citizenshipStatusIndicator
                }
            } = member;

            const memberName = `${normalizeNameOfPerson(name)}`;
            const fiveYearsAgo = moment().subtract(5, 'years').format('YYYY');

            combinedMembers.push(
                <Fragment key={`${name.firstName}_${i}`}>
                    { applyingForCoverageIndicator &&
                    <Fragment>
                        <ul className="usa-unstyled-list">
                            <li>
                                <UniversalInput
                                    label={{
                                        text: `${memberName}`
                                    }}
                                    inputData={{
                                        isInputRequired: false,
                                        type: INPUT_TYPES.CHECKBOX,
                                        name: 'medicaidChipDenial',
                                        classes:[''],
                                        checked: medicaidChipDenial,
                                        value: medicaidChipDenial,
                                        currentData: member,
                                        fields: [],
                                    }}
                                    inputActions={{
                                        onFieldChange: onFieldChange_member
                                    }}
                                    indexInArray={i}
                                />
                            </li>
                        </ul>
                        {/**********************************************************************************************************************
                         Begin - Were any of these people found not eligible for Medicaid or Nevada Check Up (CHIP) in the past 90 days? (YES)
                         ***********************************************************************************************************************/}
                        {
                            medicaidChipDenial &&
                            <Fragment>
                                <div className="gi-datepicker-layout--02">
                                    <UniversalInput
                                        id='medicaid_chip_denial_information'
                                        label={{
                                            labelClass: ['gi-datepicker-label'],
                                            text: `When was ${memberName} denied Medicaid or CHIP coverage?`,
                                            dataInstructions: 'medicaid_chip_denial_information',
                                            replacements: {
                                                'stateMedicaidProgramName': getCoverageProgramName('MEDICAID'),
                                                'stateChipProgramName': getCoverageProgramName('CHIP')
                                            }
                                        }}
                                        inputData={{
                                            inputClass: ['gi-datepicker'],
                                            isInputRequired: true,
                                            type: INPUT_TYPES.DATE_PICKER,
                                            name: 'medicaidDeniedDate',
                                            value: medicaidDeniedDate,
                                            placeholder: 'MM/DD/YYYY',
                                            validationType: VALIDATION_TYPES.DATE_VALID_CHIP_DENIAL_DATE,
                                            currentData: member,
                                            fields: []
                                        }}
                                        inputActions={{
                                            onFieldChange: onFieldChange_member
                                        }}
                                        errorClasses={['margin-l-20-ve', 'margin-b-10-ve']}
                                        indexInArray={i}
                                    />
                                </div>
                                { !citizenshipStatusIndicator &&
                                    <UniversalInput
                                        legend={{
                                            legendText:
                                            <span>
                                            Was&nbsp;
                                                <strong>{normalizeNameOfPerson(name)}&nbsp;</strong>
                                            found not eligible for Medicaid or CHIP based on immigration status since&nbsp;<strong>{fiveYearsAgo}</strong>?
                                            </span>
                                        }}
                                        label={{
                                            text: '',
                                            labelClass: ['usa-width-one-whole', 'layout--02'],
                                            ulClass:['usa-width-one-whole margin-b-20'],
                                            showRequiredIcon: true
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.RADIO_BUTTONS,
                                            name: 'medicaidDeniedDueToImmigration',
                                            value: medicaidDeniedDueToImmigration,
                                            options: DROPDOWN_OPTIONS.YES_NO,
                                            validationType: VALIDATION_TYPES.CHOSEN,
                                            validationErrorMessage: 'Please make your selection',
                                            currentData: member,
                                            fields: []
                                        }}
                                        inputActions={{
                                            onFieldChange: onFieldChange_member
                                        }}
                                        errorClasses={['margin-l-20-ve', 'margin-b-10-ve']}
                                        indexInArray={i}
                                    />
                                }

                                {
                                    medicaidDeniedDueToImmigration &&

                                    <UniversalInput
                                        legend={{
                                            legendText:
                                                <span>
                                                Has&nbsp;
                                                    <strong>{normalizeNameOfPerson(name)}&nbsp;</strong>
                                                had a change in their immigration status since <strong>{fiveYearsAgo}</strong>?
                                                </span>
                                        }}
                                        label={{
                                            text: '',
                                            labelClass: ['usa-width-one-whole', 'layout--02'],
                                            ulClass:['usa-width-one-whole margin-b-20'],
                                            showRequiredIcon: true
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.RADIO_BUTTONS,
                                            name: 'changeInImmigrationSince5Year',
                                            value: changeInImmigrationSince5Year,
                                            options: DROPDOWN_OPTIONS.YES_NO,
                                            validationType: VALIDATION_TYPES.CHOSEN,
                                            validationErrorMessage: 'Please make your selection',
                                            currentData: member,
                                            fields: []
                                        }}
                                        errorClasses={['margin-l-20-ve']}
                                        inputActions={{
                                            onFieldChange: onFieldChange_member
                                        }}
                                        indexInArray={i}
                                    />
                                }
                                {
                                    changeInImmigrationSince5Year &&
                                    <UniversalInput
                                        legend={{
                                            legendText:
                                                <span>
                                                Has&nbsp;
                                                    <strong>{normalizeNameOfPerson(name)}&nbsp;</strong>
                                                had a change in their immigration status since they were not found eligible for Medicaid or Nevada Check Up?
                                                </span>
                                        }}
                                        label={{
                                            text: '',
                                            labelClass: ['usa-width-one-whole', 'layout--02'],
                                            ulClass:['usa-width-one-whole margin-b-20'],
                                            showRequiredIcon: true
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.RADIO_BUTTONS,
                                            name: 'changeInImmigrationSinceMedicaidDenial',
                                            value: changeInImmigrationSinceMedicaidDenial,
                                            options: DROPDOWN_OPTIONS.YES_NO,
                                            validationType: VALIDATION_TYPES.CHOSEN,
                                            validationErrorMessage: 'Please make your selection',
                                            currentData: member,
                                            fields: []
                                        }}
                                        errorClasses={['margin-l-20-ve']}
                                        inputActions={{
                                            onFieldChange: onFieldChange_member
                                        }}
                                        indexInArray={i}
                                    />
                                }
                            </Fragment>
                        }
                        {/**********************************************************************************************************************
                         End - Were any of these people found not eligible for Medicaid or Nevada Check Up (CHIP) in the past 90 days? (NO)
                         ***********************************************************************************************************************/}
                    </Fragment>
                    }


                </Fragment>
            );

        });

        combinedMembers.push(
            <UniversalInput
                key={`None_of_above_${combinedMembers.length + 1}`}
                label={{
                    text: 'None of the Above',
                    isLabelRequired: false
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.CHECKBOX,
                    name: 'noneOfTheAbove',
                    checked: noneOfTheAbove,
                    value: noneOfTheAbove,
                    currentData: cloneObject(localState),
                    fields: [],
                }}
                inputActions={{
                    onFieldChange: onFieldChange_none_of_above
                }}
            />
        );
        return combinedMembers;
    };

    const checkTopLevelValidation = () => {
        return noneOfTheAbove || !!householdMembers.find(member => member.medicaidChipDenial);
    };

    return (
        <Fragment>
            <Form topLevelValidity={checkTopLevelValidation()}>
                <div id="fhMoreInfo" className="fade-in">
                    <div className="subsection">
                        <ValidationErrorMessage
                            isVisible={isFormSubmitted && !isEligiblyForSave}
                            messageText={VALIDATION_ERROR_MESSAGES.ANY_VALUE}
                            errorClasses={['margin-l-20-ve', 'margin-b-10-ve']}
                        />
                        <p className="required-true">
                            Were any of these people found not eligible for Medicaid or Nevada Check Up in the past 90 days?
                        </p>
                        {
                            getHouseholdMembers(householdMembers)
                        }
                    </div>
                </div>
            </Form>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page2_21.defaultProps = {
    name: 'Page2_21'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_21);
