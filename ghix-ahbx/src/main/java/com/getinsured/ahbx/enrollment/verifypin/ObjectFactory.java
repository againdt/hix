//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.05.16 at 02:33:28 PM IST 
//


package com.getinsured.ahbx.enrollment.verifypin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.accenture.ahbx.webservice.impl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _HBXException_QNAME = new QName("http://impl.webservice.ahbx.accenture.com/", "HBXException");
    private final static QName _VerifyEPin_QNAME = new QName("http://impl.webservice.ahbx.accenture.com/", "verifyEPin");
    private final static QName _VerifyEPinResponse_QNAME = new QName("http://impl.webservice.ahbx.accenture.com/", "verifyEPinResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.accenture.ahbx.webservice.impl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link VerifyEPin }
     * 
     */
    public VerifyEPin createVerifyEPin() {
        return new VerifyEPin();
    }

    /**
     * Create an instance of {@link VerifyESignaturePinResponseDTO }
     * 
     */
    public VerifyESignaturePinResponseDTO createVerifyESignaturePinResponseDTO() {
        return new VerifyESignaturePinResponseDTO();
    }

    /**
     * Create an instance of {@link VerifyEPinResponse }
     * 
     */
    public VerifyEPinResponse createVerifyEPinResponse() {
        return new VerifyEPinResponse();
    }

    /**
     * Create an instance of {@link VerifyESignaturePinRequestDTO }
     * 
     */
    public VerifyESignaturePinRequestDTO createVerifyESignaturePinRequestDTO() {
        return new VerifyESignaturePinRequestDTO();
    }

    /**
     * Create an instance of {@link HBXException }
     * 
     */
    public HBXException createHBXException() {
        return new HBXException();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HBXException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://impl.webservice.ahbx.accenture.com/", name = "HBXException")
    public JAXBElement<HBXException> createHBXException(HBXException value) {
        return new JAXBElement<HBXException>(_HBXException_QNAME, HBXException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyEPin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://impl.webservice.ahbx.accenture.com/", name = "verifyEPin")
    public JAXBElement<VerifyEPin> createVerifyEPin(VerifyEPin value) {
        return new JAXBElement<VerifyEPin>(_VerifyEPin_QNAME, VerifyEPin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyEPinResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://impl.webservice.ahbx.accenture.com/", name = "verifyEPinResponse")
    public JAXBElement<VerifyEPinResponse> createVerifyEPinResponse(VerifyEPinResponse value) {
        return new JAXBElement<VerifyEPinResponse>(_VerifyEPinResponse_QNAME, VerifyEPinResponse.class, null, value);
    }

}
