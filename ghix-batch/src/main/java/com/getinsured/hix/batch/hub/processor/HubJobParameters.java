package com.getinsured.hix.batch.hub.processor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;

public class HubJobParameters implements JobParametersIncrementer {
	
	private String run_ID = "runId";     
	
	public JobParameters getNext(JobParameters parameters){
		long id=0;        
		if (parameters==null || parameters.isEmpty()) {           
			id=1;         
		}
		else{ 
			id = parameters.getLong(run_ID,1L) + 1; 
        } 
		JobParametersBuilder builder = new JobParametersBuilder(parameters); 
		builder.addLong(run_ID, id).toJobParameters(); 
		DateFormat df = new SimpleDateFormat("MM/dd/yyy HH:mm:ss");
		builder.addString("EXECUTION_DATE", df.format(new Date()));  
		return builder.toJobParameters(); 
	}
}
