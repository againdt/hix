package com.getinsured.ssap.model.eligibility;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.enums.GenderEnum;
import com.getinsured.iex.ssap.financial.type.TaxFilingStatus;
import com.getinsured.iex.ssap.financial.type.TaxRelationship;
import com.getinsured.ssap.service.EligibilityEngineIntegrationServiceImpl;
import com.getinsured.ssap.util.BloodRelationshipCode;

/**
 * Describes house hold member request that is part of
 * {@link EligibilityRequest}.
 *
 * @author Yevgen Golubenko
 * @since 4/4/19
 */
public class HouseholdMemberRequest
{
  private long applicantId;

  @JsonFormat(pattern = EligibilityEngineIntegrationServiceImpl.API_DATE_FORMAT)
  private Date dateOfBirth;

  private String firstName;

  private String lastName;

  private GenderEnum gender;

  private boolean tobaccoUser;

  private boolean seekingCoverage;

  private boolean citizen;

  private boolean legalAlien;

  private boolean nativeAmerican;

  private boolean pregnant;

  @JsonProperty(value = "noOfBabies")
  private int numberOfBabies;

  private boolean incarcerated;

  private boolean disabled;

  private boolean deniedMedicaidChip;

  private boolean medicare;

  private boolean fiveYearBar;

  private boolean fosterCare;

  private int householdSizeMedicaid;

  private long householdIncomeMedicaid;

  private boolean hasESI;

  private boolean hasNonESI;

  private TaxFilingStatus taxFilingStatus = TaxFilingStatus.UNSPECIFIED;

  private TaxRelationship taxRelationship = TaxRelationship.UNSPECIFIED;

  private BloodRelationshipCode relationshipToPrimary;

  private boolean seeksQhp = true;

  /**
   * Flag indicates if this member is part of the primary tax household.
   * Primarily is used by eligibility engine integration to tell the engine
   * if subsidies needs to be calculated for this member.
   */
  private boolean primaryHousehold = true;

  /**
   * Flag indicates if member had a change in immigration in the last five years,
   * this is the question that is asked during SSAP if household member is not a citizen
   * and has answered section about eligible immigration document they have to proof lawful presence.
   */
  private boolean changeInImmigrationLastFiveYears = false;

  public long getApplicantId() {
    return applicantId;
  }

  public void setApplicantId(long applicantId) {
    this.applicantId = applicantId;
  }

  public Date getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public GenderEnum getGender() {
    return gender;
  }

  public void setGender(GenderEnum gender) {
    this.gender = gender;
  }

  public boolean isTobaccoUser() {
    return tobaccoUser;
  }

  public void setTobaccoUser(boolean tobaccoUser) {
    this.tobaccoUser = tobaccoUser;
  }

  public boolean isSeekingCoverage() {
    return seekingCoverage;
  }

  public void setSeekingCoverage(boolean seekingCoverage) {
    this.seekingCoverage = seekingCoverage;
  }

  public boolean isCitizen() {
    return citizen;
  }

  public void setCitizen(boolean citizen) {
    this.citizen = citizen;
  }

  public boolean isLegalAlien() {
    return legalAlien;
  }

  public void setLegalAlien(boolean legalAlien) {
    this.legalAlien = legalAlien;
  }

  public boolean isNativeAmerican() {
    return nativeAmerican;
  }

  public void setNativeAmerican(boolean nativeAmerican) {
    this.nativeAmerican = nativeAmerican;
  }

  public boolean isPregnant() {
    return pregnant;
  }

  public void setPregnant(boolean pregnant) {
    this.pregnant = pregnant;
  }

  public int getNumberOfBabies() {
    return numberOfBabies;
  }

  public void setNumberOfBabies(int numberOfBabies) {
    this.numberOfBabies = numberOfBabies;
  }

  public boolean isIncarcerated() {
    return incarcerated;
  }

  public void setIncarcerated(boolean incarcerated) {
    this.incarcerated = incarcerated;
  }

  public boolean isDisabled() {
    return disabled;
  }

  public void setDisabled(boolean disabled) {
    this.disabled = disabled;
  }

  public boolean isDeniedMedicaidChip() {
    return deniedMedicaidChip;
  }

  public void setDeniedMedicaidChip(boolean deniedMedicaidChip) {
    this.deniedMedicaidChip = deniedMedicaidChip;
  }

  public boolean isFiveYearBar() {
    return fiveYearBar;
  }

  public void setFiveYearBar(boolean fiveYearBar) {
    this.fiveYearBar = fiveYearBar;
  }

  public boolean isFosterCare() {
    return fosterCare;
  }

  public void setFosterCare(boolean fosterCare) {
    this.fosterCare = fosterCare;
  }

  public int getHouseholdSizeMedicaid() {
    return householdSizeMedicaid;
  }

  public void setHouseholdSizeMedicaid(int householdSizeMedicaid) {
    this.householdSizeMedicaid = householdSizeMedicaid;
  }

  public long getHouseholdIncomeMedicaid() {
    return householdIncomeMedicaid;
  }

  public void setHouseholdIncomeMedicaid(long householdIncomeMedicaid) {
    this.householdIncomeMedicaid = householdIncomeMedicaid;
  }

  public boolean isHasESI() {
    return hasESI;
  }

  public void setHasESI(boolean hasESI) {
    this.hasESI = hasESI;
  }

  public boolean isHasNonESI() {
    return hasNonESI;
  }

  public void setHasNonESI(boolean hasNonESI) {
    this.hasNonESI = hasNonESI;
  }

  public BloodRelationshipCode getRelationshipToPrimary() {
    return relationshipToPrimary;
  }

  public void setRelationshipToPrimary(BloodRelationshipCode relationshipToPrimary) {
    this.relationshipToPrimary = relationshipToPrimary;
  }

  public TaxFilingStatus getTaxFilingStatus()
  {
    return taxFilingStatus;
  }

  public void setTaxFilingStatus(TaxFilingStatus taxFilingStatus)
  {
    this.taxFilingStatus = taxFilingStatus;
  }

  public TaxRelationship getTaxRelationship() {
    return taxRelationship;
  }

  public void setTaxRelationship(TaxRelationship taxRelationship) {
    this.taxRelationship = taxRelationship;
  }

  public boolean isMedicare() {
    return medicare;
  }

  public void setMedicare(boolean medicare) {
    this.medicare = medicare;
  }

  public boolean isSeeksQhp() {
    return seeksQhp;
  }

  public void setSeeksQhp(boolean seeksQhp) {
    this.seeksQhp = seeksQhp;
  }


  /**
   * Returns boolean if this member is part of the primary tax household.
   * Primarily is used by eligibility engine integration to tell the engine
   * if subsidies needs to be calculated for this member.
   * @return {@code true} if this member part of primary tax household, {@code false} otherwise.
   */
  public boolean isPrimaryHousehold() {
    return primaryHousehold;
  }

  /**
   * Sets boolean indicating if this member is part of the primary tax household.
   * @param primaryHousehold boolean flag to set.
   */
  public void setPrimaryHousehold(boolean primaryHousehold) {
    this.primaryHousehold = primaryHousehold;
  }

  public boolean isChangeInImmigrationLastFiveYears() {
    return changeInImmigrationLastFiveYears;
  }

  public void setChangeInImmigrationLastFiveYears(boolean changeInImmigrationLastFiveYears) {
    this.changeInImmigrationLastFiveYears = changeInImmigrationLastFiveYears;
  }
}
