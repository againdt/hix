package com.getinsured.ssap.json;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonTokenId;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * Custom date deserializer is used by SSAP flow, to deserialize dates stored in different date formats.
 * <p>
 *   The need for this comes as the result of people storing dates in SSAP application data JSON in
 *   wrong formats. Some stored as ISO date, some are MMM dd, yyyy HH:mm:ss (e.g. Jul 01 2012, 11:04:32)
 *   some are stored in 12-hour time and some in 24-hour time formats.
 * </p>
 * @author Yevgen Golubenko
 * @since 1/5/19
 */
class CustomDateDeserializer extends JsonDeserializer<Date>
{
  private static final long serialVersionUID = -2275951539867772400L;
  private static final Logger log = LoggerFactory.getLogger(CustomDateDeserializer.class);

  /**
   * Implementation of our custom deserialization method.
   * <p>
   *   Currently it will parse dates from JSON if they are in following formats:
   *   <ul>
   *     <li><code>yyyy-MM-dd</code> - example: 2012-11-28</li>
   *     <li><code>MMM dd, yyyy HH:mm:ss</code> - example: Jul 01 1998, 12:03:18</li>
   *     <li><code>MMM dd, yyyy HH:mm:ss a</code> - example: Jul 01 1998, 12:03:18 AM</li>
   *     <li><code>MM/dd/yyyy</code> - example 12/28/1959</li>
   *     <li><code>{@code unix timestamp}</code> - example 1549524622</li>
   *   </ul>
   * </p>
   *
   * @param jp {@link JsonParser} instance of JSON parser that is passed to us.
   * @param ctxt {@link DeserializationContext} current parser context.
   * @return {@link Date} object if we could parse it, {@code null} otherwise.
   * @throws IOException if {@link IOException} is thrown.
   */
  @Override
  public Date deserialize(JsonParser jp, DeserializationContext ctxt)
      throws IOException
  {
    if (jp.getText() != null && jp.getText().length() > 0)
    {
      if(jp.getCurrentTokenId() == JsonTokenId.ID_NUMBER_INT)
      {
        try
        {
          long timestamp = Long.parseLong(jp.getText());
          Date date = new Date(timestamp);
          log.debug("Parsed date from unix timestamp value: {} => {}", jp.getText(), date.toString());
          return date;
        }
        catch (Exception ex)
        {
          log.warn("Could not parse {} as unix timestamp to date", jp.getText());
        }
      }
      else if (jp.getCurrentTokenId() == JsonTokenId.ID_STRING)
      {
        try
        {
          return DateUtils.parseDate(jp.getText(), new String[]{
              "yyyy-MM-dd",
              "MMM dd, yyyy HH:mm:ss",
              "MMM dd, yyyy HH:mm:ss a",
              "MM/dd/yyyy"
          });
        }
        catch (Exception e)
        {
          log.warn("Could not parse date value using any of the known date formats. Value was: {}, error: {}", jp.getText(), e.getMessage());
        }
      }
    }

    return null;
  }
}
