package com.getinsured.hix.admin.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import com.getinsured.hix.dto.enrollment.EnrollmentPlanResponseDTO;
import com.getinsured.hix.platform.util.SecurityUtil;
@Controller
public class ProcessDataForReport {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessDataForReport.class);
	private static final String REQUIRED_DATE_FORMAT="yyyy-MM-dd";
	private static final Integer MONTH_DEC=-1;
	private static final Integer YEARLY_DEC=-5;
	private static final String INDIVIDUAL_CONST="Individual";
	
	/*	SET QUARTER,MONTH AND YEAR FOR TIME PERIOD */
	public void setTimePeriodsAndValues(Model model,String periodID,List<EnrollmentPlanResponseDTO> enrollmentList){
		Date currentDate= new TSDate();
		Calendar calendar = TSCalendar.getInstance();
		calendar.setTime(currentDate);
		
		LOGGER.debug("First Fundtion PeriodID: " + SecurityUtil.sanitizeForLogging(String.valueOf(periodID)) 
						+" and enrollmentList: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentList.size())));
		
		if(periodID!=null && periodID.equalsIgnoreCase("Monthly")){
			int i=1;
			calendar.add(Calendar.YEAR, MONTH_DEC);
			Date previousDate = calendar.getTime();
			
    		SimpleDateFormat sdfDestination = new SimpleDateFormat(REQUIRED_DATE_FORMAT);
    		String newCurrentDate = sdfDestination.format(currentDate);
    		String newPreviousDate = sdfDestination.format(previousDate);
			
			LocalDate lastYearDate = new LocalDate(newPreviousDate);
			LocalDate todayYearDate = new LocalDate(newCurrentDate); 
			 
			 while(lastYearDate.isBefore(todayYearDate)){
				 StringBuilder stringBuilder=new StringBuilder("monthlyPeriod");
				 String resultantString=stringBuilder.append(String.valueOf(i++)).toString();
				 lastYearDate = lastYearDate.plus(Period.months(1));
			     model.addAttribute(resultantString, lastYearDate.toString("MMM-yyyy"));
			 }
			
		}

		if(periodID!=null && periodID.equalsIgnoreCase("Yearly")){
	        int year = calendar.get(Calendar.YEAR);
			model.addAttribute("monthlyPeriod1", year);
			model.addAttribute("monthlyPeriod2", year-1);
			model.addAttribute("monthlyPeriod3", year-2);
			model.addAttribute("monthlyPeriod4", year-3);
			model.addAttribute("monthlyPeriod5", year-4);
		}
		
		if(periodID!=null && periodID.equalsIgnoreCase("Quarterly")){
			int year = calendar.get(Calendar.YEAR);
			model.addAttribute("monthlyPeriod1", "Q1/"+year);
			model.addAttribute("monthlyPeriod2", "Q2/"+year);
			model.addAttribute("monthlyPeriod3", "Q3/"+year);
			model.addAttribute("monthlyPeriod4", "Q4/"+year);
		}
	}
	
	/*	Collect required data for displaying colors in GRAPH	*/
	public void setTimePeriodsValues(Model model,String periodID,List<String> planLevelEnrollList,String planLevelString){
		//LOGGER.info("Second Fundtion PeriodID: " + periodID +" ,planLevelString: " + planLevelString +" and listSize "+ planLevelEnrollList.size());
		if(periodID!=null && periodID.equalsIgnoreCase("Monthly") && planLevelEnrollList!=null && planLevelEnrollList.size()>0){
			setColorSeperation(model, planLevelString, planLevelEnrollList);
		}
		if(periodID!=null && periodID.equalsIgnoreCase("Yearly") && planLevelEnrollList!=null && planLevelEnrollList.size()>0){
			setColorSeperation(model, planLevelString, planLevelEnrollList);
		}
		if(periodID!=null && periodID.equalsIgnoreCase("Quarterly") && planLevelEnrollList!=null && planLevelEnrollList.size()>0){
			setColorSeperation(model, planLevelString, planLevelEnrollList);
		}
	}
	
	/*	Create an Varible like: timeValuePlatinum1,timeValuePlatinum2 and add value for this variable to show the graph	*/
	public void setColorSeperation(Model model,String planLevelString,List<String> planLevelEnrollList){
		if(planLevelString!=null && planLevelString.equalsIgnoreCase("platinum") && planLevelEnrollList.size()>0){
			for(int i=0;i<planLevelEnrollList.size();i++){
				StringBuilder stringBuilder=new StringBuilder("timeValuePlatinum");
				String resultantString=stringBuilder.append(String.valueOf(i+1)).toString();
				model.addAttribute(resultantString, planLevelEnrollList.get(i));
			}
		}else if(planLevelString!=null && (planLevelString.equalsIgnoreCase("gold") || planLevelString.equalsIgnoreCase("high")) && planLevelEnrollList.size()>0){
			for(int i=0;i<planLevelEnrollList.size();i++){
				StringBuilder stringBuilder=new StringBuilder("timeValueGold");
				String resultantString=stringBuilder.append(String.valueOf(i+1)).toString();
				model.addAttribute(resultantString, planLevelEnrollList.get(i));
			}
		}else if(planLevelString!=null && (planLevelString.equalsIgnoreCase("silver") || planLevelString.equalsIgnoreCase("low")) && planLevelEnrollList.size()>0){
			for(int i=0;i<planLevelEnrollList.size();i++){
				StringBuilder stringBuilder=new StringBuilder("timeValueSilver");
				String resultantString=stringBuilder.append(String.valueOf(i+1)).toString();
				model.addAttribute(resultantString, planLevelEnrollList.get(i));
			}
		}else if(planLevelString!=null && planLevelString.equalsIgnoreCase("bronze") && planLevelEnrollList.size()>0){
			for(int i=0;i<planLevelEnrollList.size();i++){
				StringBuilder stringBuilder=new StringBuilder("timeValueBronze");
				String resultantString=stringBuilder.append(String.valueOf(i+1)).toString();
				model.addAttribute(resultantString, planLevelEnrollList.get(i));
			}
		}else if(planLevelString!=null && planLevelString.equalsIgnoreCase("catastrophic") && planLevelEnrollList.size()>0){
			for(int i=0;i<planLevelEnrollList.size();i++){
				StringBuilder stringBuilder=new StringBuilder("timeValueCatastrophic");
				String resultantString=stringBuilder.append(String.valueOf(i+1)).toString();
				model.addAttribute(resultantString, planLevelEnrollList.get(i));
			}
		}
	}
	
	public List<String> getResultantGraph(List<String> blankspaceList,String periodID){
		int MONTH_COUNT=12;
		Integer count=0;
		List<String> returnList=new ArrayList<String>();
		if(periodID!=null && periodID.equalsIgnoreCase("Monthly")){
			for(int k=0;k<blankspaceList.size();k++){
				if(k<MONTH_COUNT){
					returnList.add("");
				}
				if(k==MONTH_COUNT){
					count=0;
				}
				if(!blankspaceList.get(k).equals("") && k<MONTH_COUNT){
					LOGGER.debug("Index: " + SecurityUtil.sanitizeForLogging(String.valueOf(k)));
					if(returnList.get(k)!=null && !returnList.get(k).isEmpty()){
						count=Integer.parseInt(returnList.get(k));
					}
					returnList.remove(k);
					if(!blankspaceList.get(k).equals("")){
						count++;
						returnList.add(k,String.valueOf(count));
					}
					count=0;
					//returnList.add(k,blankspaceList.get(k));
				}
				if(!blankspaceList.get(k).equals("") && k>=MONTH_COUNT){
					int remind=k%MONTH_COUNT;
					LOGGER.debug("remind: " + SecurityUtil.sanitizeForLogging(String.valueOf(remind)));
					if(remind==0){
						if(returnList.get(remind)!=null && !returnList.get(remind).isEmpty()){
							count=Integer.parseInt(returnList.get(remind));
						}
						returnList.remove(remind);
						if(!blankspaceList.get(k).equals("")){
							count++;
							returnList.add((remind),String.valueOf(count));
						}
						count=0;
						//returnList.add((remind),blankspaceList.get(k));
					}else{
						if(returnList.get(remind)!=null && !returnList.get(remind).isEmpty()){
							count=Integer.parseInt(returnList.get(remind));
						}
						returnList.remove(remind);
						if(!blankspaceList.get(k).equals("")){
							count++;
							returnList.add((remind),String.valueOf(count));
						}
						count=0;
						//returnList.add((remind-1),blankspaceList.get(k));
					}
				}
				
			}
		}
		return returnList;
	}
	
	public Map<String,String> setTimePeriods(String periodID){
		Map<String,String> tempTimeMap=new HashMap<String,String>();
		Date currentDate= new TSDate();
		Calendar calendar = TSCalendar.getInstance();
		calendar.setTime(currentDate);

		if(periodID!=null){
			if(periodID.equalsIgnoreCase("Monthly") || periodID.equalsIgnoreCase("Quarterly")){
				calendar.add(Calendar.YEAR, MONTH_DEC);
			}else if(periodID.equalsIgnoreCase("Yearly")){
				calendar.add(Calendar.YEAR, YEARLY_DEC);
			}
			Date previousDate = calendar.getTime();
			
			SimpleDateFormat sdfDestination = new SimpleDateFormat(REQUIRED_DATE_FORMAT);
			String newCurrentDate = sdfDestination.format(currentDate);
			String newPreviousDate = sdfDestination.format(previousDate);
			
			LocalDate lastYearDate = new LocalDate(newPreviousDate);
			lastYearDate=lastYearDate.plus(Period.months(1));
			LocalDate todayYearDate = new LocalDate(newCurrentDate);
			
			/* FUNCTIONALITY REVERSE AS PER THE NAME */
			//tempTimeMap.put("CURRENT_DATE",todayYearDate.toString());
			//tempTimeMap.put("REQUIRED_PREVIOUS_DATE",lastYearDate.toString());
			
			tempTimeMap.put("REQUIRED_PREVIOUS_DATE",lastYearDate.toString());
			tempTimeMap.put("CURRENT_DATE",todayYearDate.toString());

		}else{
			calendar.add(Calendar.YEAR, MONTH_DEC);
			Date previousDate = calendar.getTime();
			
			SimpleDateFormat sdfDestination = new SimpleDateFormat(REQUIRED_DATE_FORMAT);
			String newCurrentDate = sdfDestination.format(currentDate);
			String newPreviousDate = sdfDestination.format(previousDate);
			
			LocalDate lastYearDate = new LocalDate(newPreviousDate);
			lastYearDate=lastYearDate.plus(Period.months(1));
			LocalDate todayYearDate = new LocalDate(newCurrentDate);
			
			/* FUNCTIONALITY REVERSE AS PER THE NAME */
			//tempTimeMap.put("CURRENT_DATE",todayYearDate.toString());
			//tempTimeMap.put("REQUIRED_PREVIOUS_DATE",lastYearDate.toString());
			
			tempTimeMap.put("CURRENT_DATE",lastYearDate.toString());
			tempTimeMap.put("REQUIRED_PREVIOUS_DATE",todayYearDate.toString());

		}

		return tempTimeMap;
	}
	
	/*	Convert region name to rating region	*/
	public Integer getRatingRegionValue(String regionName){
		Integer areaRating=0;
		if(regionName!=null){
			String rating=regionName.replaceAll("[a-zA-Z ]", "");
			areaRating=Integer.parseInt(rating);
		}
		return areaRating;
	}
	
	/*	FI-Individual, 24-Shop	*/
	public String getRequiredMarketType(String marketType){
		String convertedMarketType=null;
		if(marketType!=null){
			if(marketType.equalsIgnoreCase(INDIVIDUAL_CONST)){
				convertedMarketType="FI";
			}else{
				convertedMarketType="24";
			}
		}
		return convertedMarketType;
	}

}
