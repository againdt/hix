/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.batch.enrollment.util.EnrlReconTestDataCreationThread;
import com.getinsured.hix.enrollment.repository.IEnrolleeRelationshipRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPremiumRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentPremium;
import com.getinsured.hix.model.enrollment.ReconDetailCsvDto;
import com.getinsured.hix.model.enrollment.ReconSummeryCsvDto;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;

/**
 * @author negi_s
 *
 */
@Service("enrlReconTestDataCreationService")
@Transactional
public class EnrlReconTestDataCreationServiceImpl implements EnrlReconTestDataCreationService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconTestDataCreationServiceImpl.class);
	
	@Autowired (required=true) 
	private IEnrollmentRepository enrollmentRepository;
	@Autowired (required=true) 
	private IEnrolleeRepository enrolleeRepository;
	@Autowired (required=true) 
	private IEnrolleeRelationshipRepository enrolleeRelationshipRepository;
	@Autowired(required = true)
	private IEnrollmentPremiumRepository enrollmentPremiumRepository;
	
	public static final String SPOE_ID = "          ";
	public static final String RECON_HEADER = "Record Code|Trading Partner ID|SPOE ID|Tenant ID|HIOS ID|QHPID Lookup Key|Issuer Extract Date|Issuer Extract Time|QI First Name|QI Middle Name|QI Last Name|QI Birth Date|QI Gender|QI Social Security Number (SSN)|Subscriber Indicator|Individual Relationship Code|Exchange-Assigned Subscriber ID|Exchange-Assigned Member ID|Issuer-Assigned Subscriber ID|Issuer-Assigned Member ID|Exchange-Assigned Policy Number|Issuer-Assigned Policy ID|Residential Address Line 1|Residential Address Line 2|Residential City Name|Residential State Code|Residential ZIP Code|Mailing Address Line 1|Mailing Address Line 2|Mailing Address City|Mailing Address State Code|Mailing Address ZIP Code|Residential County Code|Rating Area|Telephone Number|Tobacco Use Code|QHP Identifier|Benefit Start Date|Benefit End Date|Applied APTC Amount|Applied APTC Effective Date|Applied APTC End Date|CSR Amount|CSR Effective Date|CSR End Date|Total Premium Amount|Total Premium Effective Date|Total Premium End Date|Individual Premium Amount|Individual Premium Effective Date|Individual Premium End Date|Initial Premium Paid Status|Issuer-Assigned Record Trace Number|Coverage Year|Paid Through Date|End of Year Termination Indicator|Agent/Broker Name|Agent/Broker NPN";

	@Override
	public void generateTestDataForIssuer(List<String> hiosIssuerIdList, int coverageYear, JobService jobService, long jobExecutionId) throws Exception {
		for(String hiosIssuerId : hiosIssuerIdList){
			LOGGER.info("Generating RECON file for hiosIssuerId :: " + hiosIssuerId);
			String batchJobStatus=null;
			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
			List<Integer> enrollmentIdList = enrollmentRepository.getEnrollmentIdByIssuerAndYearForTestData(
					getIssuerIdByHiosIssuerId(hiosIssuerId), String.valueOf(coverageYear));
			List<ReconDetailCsvDto> reconDetailList = new ArrayList<>();
			Date batchRunDate = new Date();
			try{
				reconDetailList = populateDataDto(enrollmentIdList, batchRunDate, hiosIssuerId, coverageYear);
			}catch(Exception e){
				LOGGER.error("Data generation failed", e);
			}
			createReconFile(reconDetailList, hiosIssuerId, batchRunDate, coverageYear);
		}
	}
	
	@Override
	public List<ReconDetailCsvDto> populateDataForEnrollment(Integer enrollmentId, Date batchRunDate,
			String hiosIssuerId, int coverageYear) {
		List<ReconDetailCsvDto> reconDetailList = new ArrayList<>();
		try{
			Enrollment enrollment = enrollmentRepository.findById(enrollmentId);
			List<Enrollee> enrolleeList = enrolleeRepository.getEnrolleesForEnrollmentID(enrollmentId);
			Enrollee subscriber = getSubscriberEnrollee(enrolleeList);
			for(Enrollee enrollee : enrolleeList){
				if(enrollee.getId() != subscriber.getId()){
					ReconDetailCsvDto reCsvDto = populateEnrolleeDetails(enrollee, enrollment, hiosIssuerId, subscriber, coverageYear, batchRunDate);
					LOGGER.info(reCsvDto.toCsv('|'));
					reconDetailList.add(reCsvDto);
				}else{
					populateSubscriberDetails(reconDetailList, enrollee, enrollment, hiosIssuerId, subscriber, coverageYear, batchRunDate);
				}
			}
		}catch(Exception e){
			LOGGER.error("Error populating DTO hence skipping, enrollment ID :: " + enrollmentId, e);
		}
		return reconDetailList;
	}

	/**
	 * Populates ReconDetailCsvDto list from enrollment data
	 * @param enrollmentIdList
	 * @param batchRunDate
	 * @param hiosIssuerId
	 * @param coverageYear
	 * @return List of ReconDetailCsvDto
	 * @throws InterruptedException
	 */
	private List<ReconDetailCsvDto> populateDataDto(List<Integer> enrollmentIdList, Date batchRunDate, String hiosIssuerId, int coverageYear) throws InterruptedException {
		List<ReconDetailCsvDto> reconDetailList = new ArrayList<>();
		List<Callable<List<ReconDetailCsvDto>>> taskList = new ArrayList<>();
		if (null != enrollmentIdList && !enrollmentIdList.isEmpty()) {
			Integer maxThreadPoolSize = EnrollmentConstants.TEN;
			String maxThreadPoolSizeStr = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_TEST_DATA_CREATION_THREAD_POOLSIZE);
			if(maxThreadPoolSizeStr !=null && !maxThreadPoolSizeStr.isEmpty()){
				maxThreadPoolSize=Integer.valueOf(maxThreadPoolSizeStr.trim());
			}
			ExecutorService executor = Executors.newFixedThreadPool(maxThreadPoolSize);
			int size = enrollmentIdList.size();
			Integer maxEnrollmentsPerPartition = 2000;
			String maxEnrollmentsPerPartitionStr = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_TEST_DATA_CREATION_ENRL_COUNT);
			if(maxEnrollmentsPerPartitionStr !=null && !maxEnrollmentsPerPartitionStr.isEmpty()){
				maxEnrollmentsPerPartition=Integer.valueOf(maxEnrollmentsPerPartitionStr.trim());
			}
			int numberOfPartitions = size / maxEnrollmentsPerPartition;
			if (size % maxEnrollmentsPerPartition != 0) {
				numberOfPartitions++;
			}
			int firstIndex = 0;
			int lastIndex = 0;
			for (int i = 0; i < numberOfPartitions; i++) {
				firstIndex = i * maxEnrollmentsPerPartition;
				lastIndex = (i + 1) * maxEnrollmentsPerPartition;
				if (lastIndex > size) {
					lastIndex = size;
				}
				List<Integer> subList = enrollmentIdList.subList(firstIndex, lastIndex);
						taskList.add(new EnrlReconTestDataCreationThread(subList,
								hiosIssuerId, coverageYear,batchRunDate, this));
			}
			executor.invokeAll(taskList).stream().map(future -> {
				try {
					return future.get();
				} catch (Exception ex) {
					LOGGER.error("Exception occurred in populateDataDto : " + ex.getMessage());
				}
				return null;
			}).filter(p -> p != null).forEach(e -> reconDetailList.addAll(e));
			executor.shutdown();
		}
		return reconDetailList;
	}

	/**
	 * Populates subscriber details from enrollment data
	 * @param reconDetailList
	 * @param enrollee
	 * @param enrollment
	 * @param hiosIssuerId
	 * @param subscriber
	 * @param coverageYear
	 * @param batchRunDate
	 */
	private void populateSubscriberDetails(List<ReconDetailCsvDto> reconDetailList, Enrollee enrollee, Enrollment enrollment, String hiosIssuerId, Enrollee subscriber, int coverageYear, Date batchRunDate) {
		List<EnrollmentPremium> enrollmentPremiumList = enrollmentPremiumRepository.findNotNullPremiumByEnrollmentId(enrollment.getId());
		if(null !=  enrollmentPremiumList && !enrollmentPremiumList.isEmpty()){
			Float grossPremiumAmount = (float) -1;
			Float aptcAmount = (float) -1;
			Float slcspPremiumAmount = (float) -1;
			Calendar startDate = null;
			ReconDetailCsvDto reCsvDto = null;
			for (int i = 0; i < enrollmentPremiumList.size(); i++) {

				EnrollmentPremium enrollmentPremium = enrollmentPremiumList.get(i);

				if (grossPremiumAmount == -1 && aptcAmount == -1 && slcspPremiumAmount == -1) {

					grossPremiumAmount = enrollmentPremium.getGrossPremiumAmount();
					aptcAmount = enrollmentPremium.getAptcAmount();
					slcspPremiumAmount = enrollmentPremium.getSlcspPremiumAmount();
					startDate = EnrollmentUtils.dateToCalendar(enrollment.getBenefitEffectiveDate());
					
					if(enrollmentPremiumList.size() == 1){
						reCsvDto = populateEnrolleeDetails(enrollee, enrollment, hiosIssuerId, subscriber, coverageYear, batchRunDate);
						setAmountFields(reCsvDto, enrollment, enrollmentPremium, startDate.getTime(), true);
						reconDetailList.add(reCsvDto);
					}
					continue;
				}

				if (checkIfPremiumChange(grossPremiumAmount, aptcAmount,
						slcspPremiumAmount, enrollmentPremium)) {
					reCsvDto = populateEnrolleeDetails(enrollee, enrollment, hiosIssuerId, subscriber, coverageYear, batchRunDate);
					setAmountFields(reCsvDto, enrollment, enrollmentPremiumList.get(i - 1), startDate.getTime(), false);
					reconDetailList.add(reCsvDto);
					grossPremiumAmount = enrollmentPremium.getGrossPremiumAmount();
					aptcAmount = enrollmentPremium.getAptcAmount();
					slcspPremiumAmount = enrollmentPremium.getSlcspPremiumAmount();
					startDate = new GregorianCalendar(enrollmentPremium.getYear(),enrollmentPremium.getMonth()-1, 1);
				}

				if (i == (enrollmentPremiumList.size() - 1)) {
					reCsvDto = populateEnrolleeDetails(enrollee, enrollment, hiosIssuerId, subscriber, coverageYear, batchRunDate);
					setAmountFields(reCsvDto, enrollment, enrollmentPremiumList.get(i), startDate.getTime(), true);
					reconDetailList.add(reCsvDto);
				}
			}
		}else{
			ReconDetailCsvDto reCsvDto = populateEnrolleeDetails(enrollee, enrollment, hiosIssuerId, subscriber, coverageYear, batchRunDate);
			setAmountFields(reCsvDto, enrollment);
			reconDetailList.add(reCsvDto);
		}
	}

	/**
	 * Sets amount fields into subscriber DTO
	 * @param reCsvDto
	 * @param enrollment
	 * @param enrollmentPremium
	 * @param startDate
	 * @param isLast
	 */
	private void setAmountFields(ReconDetailCsvDto reCsvDto, Enrollment enrollment,
			EnrollmentPremium enrollmentPremium, Date startDate, boolean isLast) {
		Date endDate = null;
		if(isLast){
			endDate = enrollment.getBenefitEndDate();
		}else{
			Calendar monthLastDay = new GregorianCalendar(enrollmentPremium.getYear(),enrollmentPremium.getMonth()-1, 1);
			monthLastDay.set(Calendar.DAY_OF_MONTH, monthLastDay.getActualMaximum(Calendar.DAY_OF_MONTH));
			endDate = monthLastDay.getTime();
		}
		if(null !=enrollmentPremium.getAptcAmount()){
			reCsvDto.setAppliedAptcAmount(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, enrollmentPremium.getAptcAmount()));
			reCsvDto.setAppliedAptcEffectiveDate(DateUtil.dateToString(startDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
			reCsvDto.setAppliedAptcEndDate(DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
		}
		if(null != enrollment.getCsrMultiplier() && enrollment.getCsrMultiplier().compareTo(0.0f) != 0 && null != enrollmentPremium.getGrossPremiumAmount()){
			reCsvDto.setCsrAmount(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, EnrollmentUtils
					.roundFloat(enrollmentPremium.getGrossPremiumAmount() * enrollment.getCsrMultiplier(), 2)));
			reCsvDto.setCsrEffectiveDate(DateUtil.dateToString(startDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
			reCsvDto.setCsrEndDate(DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
		}
		reCsvDto.setTotalPremiumAmount(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, enrollmentPremium.getGrossPremiumAmount()));
		reCsvDto.setTotalPremiumEffectiveDate(DateUtil.dateToString(startDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
		reCsvDto.setTotalPremiumEndDate(DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
	}
	
	/**
	 * Sets amount fields into subscriber DTO
	 * @param reCsvDto
	 * @param enrollment
	 */
	private void setAmountFields(ReconDetailCsvDto reCsvDto, Enrollment enrollment) {
		Date endDate = enrollment.getBenefitEndDate();
		Date startDate = enrollment.getBenefitEffectiveDate();
		if(null !=enrollment.getAptcAmt()){
			reCsvDto.setAppliedAptcAmount(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, enrollment.getAptcAmt()));
			reCsvDto.setAppliedAptcEffectiveDate(DateUtil.dateToString(startDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
			reCsvDto.setAppliedAptcEndDate(DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
		}
		if(null != enrollment.getCsrMultiplier() && null != enrollment.getGrossPremiumAmt()){
			reCsvDto.setCsrAmount(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, EnrollmentUtils
					.roundFloat(enrollment.getGrossPremiumAmt() * enrollment.getCsrMultiplier(), 2)));
			reCsvDto.setCsrEffectiveDate(DateUtil.dateToString(startDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
			reCsvDto.setCsrEndDate(DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
		}
		reCsvDto.setTotalPremiumAmount(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, enrollment.getGrossPremiumAmt()));
		reCsvDto.setTotalPremiumEffectiveDate(DateUtil.dateToString(startDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
		reCsvDto.setTotalPremiumEndDate(DateUtil.dateToString(endDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
	}

	/**
	 * Populates ReconDetailCsvDto with enrollee data
	 * @param enrollee
	 * @param enrollment
	 * @param hiosIssuerId
	 * @param subscriber
	 * @param coverageYear
	 * @param batchRunDate
	 * @return ReconDetailCsvDto
	 */
	private ReconDetailCsvDto populateEnrolleeDetails(Enrollee enrollee, Enrollment enrollment, String hiosIssuerId, Enrollee subscriber, int coverageYear, Date batchRunDate) {
		ReconDetailCsvDto reCsvDto = new ReconDetailCsvDto();
		reCsvDto.setRecordCode("01");
		reCsvDto.setTradingPartnerId(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.TradingPartnerID));
		reCsvDto.setSpoeId(SPOE_ID);
		reCsvDto.setTenantId("ID0");
		reCsvDto.setHiosId(hiosIssuerId);
		reCsvDto.setQhpidLookupKey(enrollment.getCMSPlanID() != null ? enrollment.getCMSPlanID().substring(0,10) : null);
		reCsvDto.setIssuerExtractDate(DateUtil.dateToString(batchRunDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
		reCsvDto.setIssuerExtractTime(DateUtil.dateToString(batchRunDate, "HHmmssSS"));
		//*******Qualified Individual (QI) Information
		reCsvDto.setQiFirstName(enrollee.getFirstName());
		reCsvDto.setQiMiddleName(enrollee.getMiddleName());
		reCsvDto.setQiLastName(enrollee.getLastName());
		reCsvDto.setQiBirthDate(
				enrollee.getBirthDate() != null ? DateUtil.dateToString(enrollee.getBirthDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD) : null);
		reCsvDto.setQiGender(enrollee.getGenderLkp() != null ? enrollee.getGenderLkp().getLookupValueCode() : null);
		reCsvDto.setQiSocialSecurityNumber(enrollee.getTaxIdNumber());
		reCsvDto.setSubscriberIndicator("N");
		if(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
			reCsvDto.setSubscriberIndicator("Y");
			if(null != enrollment.getAgentBrokerName()){
				reCsvDto.setAgentBrokerName(EnrollmentUtils.removeExtraSpaces(enrollment.getAgentBrokerName()).trim());
			}
			reCsvDto.setAgentBrokerNpn(enrollment.getBrokerTPAAccountNumber1());
			reCsvDto.setPaidThroughDate(enrollee.getLastPremiumPaidDate() != null ? DateUtil.dateToString(enrollee.getLastPremiumPaidDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD) : null);
			if(null != enrollment.getEnrollmentStatusLkp() && EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())){
				reCsvDto.setInitialPremiumPaidStatus("C");
			}else if(null != enrollment.getEnrollmentConfirmationDate()){
				reCsvDto.setInitialPremiumPaidStatus("Y");
			}else{
				reCsvDto.setInitialPremiumPaidStatus("N");
			}
		}
		EnrolleeRelationship relationShipLkp = enrolleeRelationshipRepository.getRelationshipBySourceEndTargetId(enrollee.getId(), subscriber.getId());
		reCsvDto.setIndividualRelationshipCode(relationShipLkp != null ? relationShipLkp.getRelationshipLkp().getLookupValueCode() : null);
		//********************Identifying Information
		reCsvDto.setExchangeAssignedSubscriberId(enrollment.getExchgSubscriberIdentifier());
		reCsvDto.setExchangeAssignedMemberId(enrollee.getExchgIndivIdentifier());
		reCsvDto.setIssuerAssignedSubscriberId(enrollment.getIssuerSubscriberIdentifier());
		reCsvDto.setIssuerAssignedMemberId(enrollee.getIssuerIndivIdentifier());
		reCsvDto.setExchangeAssignedPolicyNumber(String.valueOf(enrollment.getId()));
		reCsvDto.setIssuerAssignedPolicyId(String.valueOf(enrollment.getId())); // Setting enrollment Id as Health coverage policy ID in Enrollee is optional
		//***********************Residential Address Information
		Location residence = enrollee.getHomeAddressid();
		if(null != residence ){
			reCsvDto.setResidentialAddressLine1(StringUtils.trim(residence.getAddress1()));
			reCsvDto.setResidentialAddressLine2(StringUtils.trim(residence.getAddress2()));
			reCsvDto.setResidentialCityName(StringUtils.trim(residence.getCity()));
			reCsvDto.setResidentialStateCode(StringUtils.trim(residence.getState()));
			reCsvDto.setResidentialZipCode(StringUtils.trim(residence.getZip()));
			reCsvDto.setResidentialCountyCode(StringUtils.trim(residence.getCountycode()));
		}
		//***********************Mailing Address Information
		Location mailingAddress = enrollee.getMailingAddressId();	
		if(null != mailingAddress ){
			reCsvDto.setMailingAddressLine1(StringUtils.trim(mailingAddress.getAddress1()));
			reCsvDto.setMailingAddressLine2(StringUtils.trim(mailingAddress.getAddress2()));
			reCsvDto.setMailingAddressCity(StringUtils.trim(mailingAddress.getCity()));
			reCsvDto.setMailingAddressStateCode(StringUtils.trim(mailingAddress.getState()));
			reCsvDto.setMailingAddressZipCode(StringUtils.trim(mailingAddress.getZip()));
		}
		//**************************Other Demographic Information						
//		reCsvDto.setResidentialCountyCode(StringUtils.trim(residence.getCountycode()));
		reCsvDto.setRatingArea(EnrollmentUtils.getFormatedRatingArea(enrollee.getRatingArea()));
		reCsvDto.setTelephoneNumber(enrollee.getPrimaryPhoneNo());
		reCsvDto.setTobaccoUseCode("2");
		if(enrollee.getTobaccoUsageLkp()!=null && enrollee.getTobaccoUsageLkp().getLookupValueCode().equalsIgnoreCase("T")){
			reCsvDto.setTobaccoUseCode("1");
		}
		reCsvDto.setQhpIdentifier(enrollment.getCMSPlanID());
		reCsvDto.setBenefitStartDate(enrollee.getEffectiveStartDate() != null ? DateUtil.dateToString(enrollee.getEffectiveStartDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD) : null);
		reCsvDto.setBenefitEndDate(enrollee.getEffectiveEndDate() != null ? DateUtil.dateToString(enrollee.getEffectiveEndDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD) : null);
		reCsvDto.setCoverageYear(String.valueOf(coverageYear));
		if(null != enrollee.getTotalIndvResponsibilityAmt()){
			reCsvDto.setIndividualPremiumAmount(String.format(EnrollmentConstants.DECIMAL_POINT_TWO,enrollee.getTotalIndvResponsibilityAmt()));
			reCsvDto.setIndividualPremiumEffectiveDate(enrollee.getEffectiveStartDate() != null ? DateUtil.dateToString(enrollee.getEffectiveStartDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD) : null);
			reCsvDto.setIndividualPremiumEndDate(enrollee.getEffectiveEndDate() != null ? DateUtil.dateToString(enrollee.getEffectiveEndDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD) : null);
		}
	
		return reCsvDto;
	}

	/**
	 * Writes CSV file from a list of ReconDetailCsvDto
	 * @param reconDetailList
	 * @param hiosIssuerId
	 * @param batchRunDate
	 * @param coverageYear 
	 */
	private void createReconFile(List<ReconDetailCsvDto> reconDetailList, String hiosIssuerId, Date batchRunDate, int coverageYear) {
		if(null !=reconDetailList && !reconDetailList.isEmpty()){
			Writer writer = null;
			StringBuilder reconFilePathBuilder = new StringBuilder();
			reconFilePathBuilder
					.append(DynamicPropertiesUtil.getPropertyValue(
							EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_OUT_JOB_PATH))
					.append(File.separator).append(EnrollmentConstants.ReportType.CARRIERRECON.toString())
					.append(File.separator).append(EnrollmentConstants.RECON_TEST_DATA_FOLDER);
			String reconFilePath = reconFilePathBuilder.toString();
			try{
				ReconSummeryCsvDto summaryDto = getSummaryRecord(reconDetailList, hiosIssuerId, batchRunDate);
				EnrollmentUtils.createDirectory(reconFilePath);
				String reconFileName = "from_"+hiosIssuerId+"_INDV_MONTHLYRECON_"+coverageYear+"_"+DateUtil.dateToString(batchRunDate, "yyyyMMddHHmmss")+".IN";
				writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(reconFilePath+ File.separatorChar+reconFileName), "utf-8"));
				writer.write(RECON_HEADER+"\n");
				for(ReconDetailCsvDto dto: reconDetailList){
					writer.write(dto.toCsv('|')+"\n");
				}
				writer.write(summaryDto.toCsv('|'));
			}catch(Exception e){
				LOGGER.error("Exception in creating Reconciliation file :: " + e.getMessage(), e);
			}finally{
				if(writer != null){
					try {
						writer.close();
					} catch (IOException e) {
						LOGGER.error("Exception closing writer :: " + e.getMessage(), e);
					}
				}
			}
		}
	}
	
	/**
	 * Creates summary record from a list of ReconDetailCsvDto
	 * @param reconDetailList
	 * @param hiosIssuerId
	 * @param batchRunDate
	 * @return ReconSummeryCsvDto
	 */
	private ReconSummeryCsvDto getSummaryRecord(List<ReconDetailCsvDto> reconDetailList, String hiosIssuerId,
			Date batchRunDate) {
		ReconSummeryCsvDto summaryDto = new ReconSummeryCsvDto();
		int totalRecords = reconDetailList.size();
		int totalSubscribers = 0;
		int totalDependentMembers = 0;
		BigDecimal totalPremiumAmount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal totalAptcAmount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
		summaryDto.setRecordCode("02");
		summaryDto.setTradingPartnerId(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.TradingPartnerID));
		summaryDto.setSpoeId(SPOE_ID);
		summaryDto.setTenantId("ID0");
		summaryDto.setHiosId(hiosIssuerId);
		summaryDto.setQhpidLookupKey(reconDetailList.get(totalRecords-1).getQhpidLookupKey());
		summaryDto.setIssuerExtractDate(DateUtil.dateToString(batchRunDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
		for(ReconDetailCsvDto dto : reconDetailList){
			if("Y".equalsIgnoreCase(dto.getSubscriberIndicator())){
				totalSubscribers++;
				if(NumberUtils.isNumber(dto.getTotalPremiumAmount())){
					totalPremiumAmount = totalPremiumAmount.add(new BigDecimal(dto.getTotalPremiumAmount()));
				}
				if(NumberUtils.isNumber(dto.getAppliedAptcAmount())){
					totalAptcAmount = totalAptcAmount.add(new BigDecimal(dto.getAppliedAptcAmount()));
				}
			}else{
				totalDependentMembers++;
			}
		}
		summaryDto.setTotalNumberOfRecords(String.valueOf(totalRecords));
		summaryDto.setTotalNumberOfSubscribers(String.valueOf(totalSubscribers));
		summaryDto.setTotalNumberOfDependentMembers(String.valueOf(totalDependentMembers));
		summaryDto.setTotalPremiumAmount(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, totalPremiumAmount));
		summaryDto.setTotalAppliedAptcAmount(String.format(EnrollmentConstants.DECIMAL_POINT_TWO, totalAptcAmount));
		return summaryDto;
	}

	/**
	 * Premium change check method
	 * @param grossPremiumAmount
	 * @param aptcAmount
	 * @param slcspPremiumAmount
	 * @param enrollmentPremium
	 * @return boolean 
	 */
	private boolean checkIfPremiumChange(Float grossPremiumAmount, Float aptcAmount, Float slcspPremiumAmount, EnrollmentPremium enrollmentPremium) {
		boolean premiumChange = false;
		if (null != grossPremiumAmount && null == enrollmentPremium.getGrossPremiumAmount()) {
			return true;
		}
		if (null != aptcAmount && null == enrollmentPremium.getAptcAmount()) {
			return true;
		}
		/*if (null != slcspPremiumAmount & null == enrollmentPremium.getSlcspPremiumAmount()) {
			return true;
		}*/

		// =============================================================

		if (null == grossPremiumAmount && null != enrollmentPremium.getGrossPremiumAmount()) {
			return true;
		}
		if (null == aptcAmount && null != enrollmentPremium.getAptcAmount()) {
			return true;
		}
		/*if (null == slcspPremiumAmount & null != enrollmentPremium.getSlcspPremiumAmount()) {
			return true;
		}*/

		// =============================================================

		if (null != grossPremiumAmount && null != enrollmentPremium.getGrossPremiumAmount() && Float.compare(grossPremiumAmount, enrollmentPremium.getGrossPremiumAmount()) != 0 ) {
			return true;
		}
		if (null != aptcAmount && null != enrollmentPremium.getAptcAmount() &&  Float.compare(aptcAmount, enrollmentPremium.getAptcAmount()) != 0) {
			return true;
		}
		/*if (null != slcspPremiumAmount & null != enrollmentPremium.getSlcspPremiumAmount()) {
			return Float.compare(slcspPremiumAmount, enrollmentPremium.getSlcspPremiumAmount()) == 0 ? false : true;
		}
		 */
		return premiumChange;
	}
	

	/**
	 * Get issuer ID from HiosIssuerId
	 * @param hiosIssuerId
	 * @return Issuer ID
	 */
	private Integer getIssuerIdByHiosIssuerId(String hiosIssuerId) {
		Integer issuerId = null;
		if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)) {
			issuerId = enrollmentRepository.getIssuerIdByHiosIssuerIdPg(hiosIssuerId);
		}else if("ORACLE".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
			issuerId = enrollmentRepository.getIssuerIdByHiosIssuerIdOracle(hiosIssuerId);
		}
		return issuerId;
	}
	

	/**
	 * Gets subscriber from a list of enrollees
	 * @param enrolleeList
	 * @return subscriber enrollee
	 */
	private Enrollee getSubscriberEnrollee(List<Enrollee> enrolleeList) {
		Enrollee subscriber = null;
		if(enrolleeList != null && enrolleeList.size() > 0){
			for(Enrollee enrollee : enrolleeList){
				if(enrollee.getPersonTypeLkp().getLookupValueCode() != null  && enrollee.getPersonTypeLkp().getLookupValueCode().equals("SUBSCRIBER")){
					subscriber = enrollee;
					break;
				}
			}
		}
		return subscriber;
	}


}
