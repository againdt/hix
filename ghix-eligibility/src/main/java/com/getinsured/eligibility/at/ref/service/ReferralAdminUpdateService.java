package com.getinsured.eligibility.at.ref.service;

import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
/**
 * @author wadkar_p
 * 
 */
public interface ReferralAdminUpdateService {
	
	 void executeAdminUpdate(CompareMainDTO compareMainDTO) ;
	 void executeNonFinancialAdminUpdate(CompareMainDTO compareMainDTO);

}
