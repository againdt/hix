package com.getinsured.eligibility.verification.rulesets;


import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.verification.service.VerificationRulesService;
import com.getinsured.eligibility.verification.util.SSAPConstants;
import com.getinsured.eligibility.verification.util.SSAPUtils;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Encapsulates Income Verification Rule methods
 */
@Component
public class IncomeVerificationRule {

	private static final Logger LOGGER = LoggerFactory.getLogger(IncomeVerificationRule.class);
	
	@Autowired
	private VerificationRulesService verificationRulesService;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	
	public static void main(String[] asdf) throws Exception {
		IncomeVerificationRule income = new IncomeVerificationRule();
		
		income.verifyIncome(SSAPUtils.loadSSAPJSON(), null);
	}
	
	/**
	 * This rule will verify Income Status of Household Members
	 * 
	 * @param ssapJson
	 * @param hubResponseJson
	 * @throws Exception 
	 */
	public String verifyIncome(String ssapJson, String hubResponseJson) throws Exception {
		LOGGER.info("Verify Income Rule : Starts");
		
		String status = null;
		JsonObject householdMember = null;
		String houseHoldIncome;
		String irsIncome = null;
		float houseHoldIncomeAmount;
		float iRSIncome;
		String personId;
		JsonArray householdMembers;
		boolean personChecked = false;
		String compatibility  = SSAPConstants.FALSE;
		JsonObject singleStreamlinedApplication;

		try {
			singleStreamlinedApplication = SSAPUtils.getSingleStreamlinedApplication(ssapJson);
			householdMembers = SSAPUtils.getFilteredHouseholdMembers(singleStreamlinedApplication);
			
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				householdMember = iterator.next().getAsJsonObject();
				personId = householdMember.get(SSAPConstants.PERSON_ID).getAsString();
				
				try {
					if(!SSAPUtils.isEmpty(hubResponseJson)){
						houseHoldIncome = SSAPUtils.getHouseholdIncome(singleStreamlinedApplication);
						
						if(!SSAPUtils.isEmpty(houseHoldIncome)){
							irsIncome = SSAPUtils.getIRSIncome(hubResponseJson);
							if(!SSAPUtils.isEmpty(irsIncome)){
								houseHoldIncomeAmount = Float.valueOf(houseHoldIncome);
								iRSIncome = Float.valueOf(irsIncome);
								if(houseHoldIncomeAmount >= iRSIncome){
									status = SSAPConstants.VERIFIED;
								} else {
									if(SSAPUtils.checkIRSCompatibilityRange(houseHoldIncomeAmount, iRSIncome)){
										status = SSAPConstants.VERIFIED;
										compatibility = SSAPConstants.TRUE;
									} else {
										status = SSAPConstants.NOT_VERIFIED;
									}
								}
							} else {
								status = SSAPConstants.NOT_VERIFIED;
							}
						} else {
							status = SSAPConstants.NOT_VERIFIED;
						}
						personChecked = true;
					} else {
						status = SSAPConstants.PENDING;
					}
					
					verificationRulesService.updateVerificationStatus(Long.valueOf(SSAPUtils.getApplicationIdFromSsapJson(singleStreamlinedApplication)),
							Long.valueOf(personId), status, SSAPConstants.INCOME_VERIFICATION_RULE);
					
					if(personChecked)break;
				} catch(Exception exception){
					LOGGER.error("Exception occurred while Verifying Income : Person : "+personId,exception);
					String rule = SSAPConstants.RULES.INCOME_VERIFICATION_RULE.toString();
					giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
							hubResponseJson, null, null, null, rule), rule));
				}
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Verifying Income ", exception);
			throw exception;
		}

		LOGGER.info("Verify Income Rule : Ends ");

		return compatibility;
	}
}
