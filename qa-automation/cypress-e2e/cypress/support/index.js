/// <reference types="cypress" />


// ***********************************************************
// This example support/index.html is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'
import './constants'
import './user'
import './XML/at_datagen'
import './XML/um_schema'
import './XML/at_schema'
import './XML/digest'
import './XML/at_constants'
import './XML/at_functions'
import './XML/at_commands'
import './UI/member_portal_functions'
import './UI/enrollment'
import './UI/disenrollment'

// Alternatively you can use CommonJS syntax:
// require('./commands')

Cypress.Cookies.debug(false, { verbose: false })
Cypress.Cookies.defaults({
  whitelist: [
    'JSESSIONID', 
    'SESSION', 
    'SERVERIDSCREENERAPI',
    'SERVERIDWEB',
    'SERVERIDSVC'
  ]
})

Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
    expect(err.message).exist
        //.to.include('resizeimage is not defined' || undefined)
  return false
})

const addContext = require('mochawesome/addContext')

Cypress.on('test:after:run', (test, runnable) => {
  if (test.state === 'failed') {
    //TODO: figure out screenshots links

    // const screenshotFileName = `${runnable.parent.title} -- ${test.title} (failed).png`
    // addContext({ test }, `assets/screenshots/${Cypress.spec.name}/${screenshotFileName}`)

    //Video link for failed test steps
    addContext({ test }, `assets/videos/${Cypress.spec.name}.mp4`)
  }
})

