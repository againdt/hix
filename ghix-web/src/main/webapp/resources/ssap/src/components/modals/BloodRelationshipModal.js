import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from '@cmsgov/design-system-core/dist/components/Button/Button';


// Action creators

import { hideModal } from '../../actions/actionCreators';

// Utils

import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';



const BloodRelationshipModal = props => {

    const {
        headOfHousehold: {
            name : headOfHouseholdName
        },
        householdMembers,
        modal: {
            modalOwner,
            data : listOfHouseholdMembers,
            callbacks
        }
    } = props;

    const handleHideClick = () => {
        props.actions.hideModal(modalOwner);
    };


    const handleConfirmationClick = () => {
        householdMembers.forEach(member => {
            if(listOfHouseholdMembers.find(listedMember => listedMember.applicantGuid === member.applicantGuid)) {
                member.applyingForCoverageIndicator = false;
            }
        });
        callbacks();
        props.actions.hideModal(modalOwner);
    };

    return (
        <Fragment>
            <h4 className='h4-not-highlighted'/>
            <div>
                <p>
                    {
                        `Some member(s) of your household are over the age of 26,
                        and therefore cannot be listed as a dependent of ${normalizeNameOfPerson(headOfHouseholdName)}.`
                    }
                </p>
                <p>
                    {
                        `This household member(s) will need to apply separately and will
                        be marked as "not seeking coverage" in this application:`
                    }
                </p>
                <p>
                    <b>
                        {
                            `(If you have a dependent who is disabled and is 25 years or older,
                            please select either "Parent/Caretaker" or "Court Appointed Guardian"
                            as your relationship to that person.)`
                        }
                    </b>
                </p>
                {
                    listOfHouseholdMembers.map((member, i) => {
                        const {
                            name,
                            name: {
                                firstName
                            }
                        } = member;
                        return (
                            <p key={`${firstName}_${i}`}>
                                {
                                    normalizeNameOfPerson(name)
                                }
                            </p>
                        );
                    })
                }
                <div>
                    <Button
                        className='modal-cancel-btn usa-button-secondary'
                        onClick={handleHideClick}
                    >
                        Cancel
                    </Button>
                    <Button
                        className='modal-confirm-btn'
                        onClick={handleConfirmationClick}
                    >
                        OK
                    </Button>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    headOfHousehold: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember[0],

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        hideModal,
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(BloodRelationshipModal);