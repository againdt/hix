package com.getinsured.hix.batch.enrollment.xmlEnrollments;

import org.apache.log4j.Logger;
import org.springframework.batch.core.annotation.OnSkipInProcess;
import org.springframework.batch.core.annotation.OnSkipInRead;
import org.springframework.batch.core.annotation.OnSkipInWrite;

public class EnrollmentRecordListener {
	final static Logger LOGGER = Logger.getLogger("ENROLLMENT_LOGGER");
	
	private String getExceptionMessage(Throwable t, StringBuilder builder){
		Throwable cause = null;
		builder.append(t.getMessage());
		cause = t.getCause();
		if(cause != null){
			return getExceptionMessage(cause, builder);
		}
		return builder.toString();
	}
	
	@OnSkipInRead
	public void logValidationError(Throwable t){
		StringBuilder builder = new StringBuilder();
		String error = this.getExceptionMessage(t, builder);
		LOGGER.error(error);
	}
	
	@OnSkipInWrite
	public void logWriteError(Object obj, Throwable t){
		StringBuilder builder = new StringBuilder();
		String error = this.getExceptionMessage(t, builder);
		LOGGER.error(error);
	}
	
	@OnSkipInProcess
	public void logWriteProcess(Object obj, Throwable t){
		StringBuilder builder = new StringBuilder();
		String error = this.getExceptionMessage(t, builder);
		LOGGER.error(error);
	}

}
