package com.getinsured.eligibility.at.ref.service;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.service.EligibilityNotificationService;
import com.getinsured.eligibility.redetermination.service.RenewalNoticeService;
import com.getinsured.eligibility.ssap.integration.notices.AgedOutDisenrollmentNoticeService;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

@Component("referralNotificationService")
@Scope("singleton")
public class ReferralNotificationServiceImpl implements ReferralNotificationService {

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;
	@Autowired
	private ReferralQENotificationService referralQENotificationService;
	@Autowired
	private ReferralAccountActivationService referralAccountActivationService;
	@Autowired
	private AgedOutDisenrollmentNoticeService agedOutDisenrollmentNoticeService;
	@Autowired
	private EligibilityNotificationService eligibilityNotificationService;
	@Autowired 
	RenewalNoticeService renewalNoticeService;

	private static final Logger LOGGER = Logger.getLogger(ReferralNotificationServiceImpl.class);
	
	private static final String IS_HEALTH_DISENROLLED = "healthDisenroled";
	private static final String IS_DENTAL_DISENROLLED = "dentalDisenrolled";
	private static final String TERMINATION_DATE = "terminationDate";

	private static final Set<String> VALID_NOTICES = new HashSet<>(Arrays.asList("DemographicChange", "SEPEvent", "FinancialLCE", "HouseHoldEligibilityLoss", /*"NoChange",*/ "SpecialEnrollmentPeriodDenied", "QualifyingEvent", "SEPKeepOnlyEvent",
	        "QEWithAutoLinking", "ReferralActivationWithoutAutoLinkingEmail", "AgeOutNotice", "PassiveEnrollmentNotice", "ApplicationEligibilityResults", 
	        "UpdatedApplicationEligibilityResults", "AdditionalDocumentsToVerify","EE057-E020ChangeActionRequiredNotice", "EE057-E022ChangeActionRequiredNotice", 
	        "DenialNotificationEmailNotice","EE032ConversionNonFinToFin","EE060AutomationNotice", "EE033NonFinToFinManual", "OEWithAutoLinking", "SuccessAutoRenewalNotice",
			"FailedAutoRenewalNotice", "EE018FCSPlanChange", "EE018FRTRatingAreaChange", "EE061LCEDocumentRequiredNotice", "AccountMigrationEmail"));

	private String generateNotice(String caseNumber, String noticeName, long enrolledAppId, Map<String, String> noticeData) throws Exception {

		String ecmId = null;
		switch (noticeName) {
			case "DemographicChange":
				ecmId = referralLceNotificationService.generateDemographicChangeNotice(caseNumber);
				break;
			case "SEPEvent":
				ecmId = referralLceNotificationService.generateSEPEventNotice(caseNumber);
				break;
			case "FinancialLCE":
				ecmId = referralLceNotificationService.generateFinancialLCENotice(caseNumber);
				break;
			case "HouseHoldEligibilityLoss":
				ecmId = referralLceNotificationService.generateEligibilityLossNotice(caseNumber);
				break;
			case "SpecialEnrollmentPeriodDenied":
				ecmId = referralLceNotificationService.generateSEPDenialNotice(caseNumber);
				break;
			case "QualifyingEvent":
				ecmId = referralLceNotificationService.generateQEPEventNotice(caseNumber);
				break;
			case "SEPKeepOnlyEvent":
				ecmId = referralLceNotificationService.generateSEPEventKeepOnlyNotice(caseNumber);
				break;

			case "QEWithAutoLinking":
				ecmId = referralQENotificationService.generateQEWithAutoLinkingNotice(caseNumber);
				break;

			case "ReferralActivationWithoutAutoLinkingEmail":
			case "ReferralAccountActivationEmail":
				ecmId = referralAccountActivationService.retriggerAccountActivation(caseNumber, noticeName);
				break;

			case "AgeOutNotice":
				ecmId = agedOutDisenrollmentNoticeService.processAgeOutDependents(caseNumber);
				break;

			case "ApplicationEligibilityResults":
				ecmId = eligibilityNotificationService.generateEligibilityNotificationInInbox(caseNumber);
				break;

			case "UpdatedApplicationEligibilityResults":
				ecmId = eligibilityNotificationService.generateEligibilityNotificationUpdatedInInbox(caseNumber);
				break;

			case "EE052AutomationAddRemove":
				ecmId = referralLceNotificationService.generateAddRemoveAutomationNotice(caseNumber);
				break;

			case "APTCAmountChangedNotice":
				ecmId = referralLceNotificationService.generateAPTCChangeNotice(caseNumber);
				break;

			case "EE019LceDisenrollNotification":
				ecmId = referralLceNotificationService.generateHHLostEligibilityNoticewithNonQhpIndicators(caseNumber, enrolledAppId);
				break;

			case "EE058NonFinancialConversionWithAPTCAndCSR":
				ecmId = referralLceNotificationService.generateNonFinConversionNoticeWithAptcCsrIneligibile(caseNumber);
				break;

			case "EE056NonFinancialConversionWithAPTCOnly":
				ecmId = referralLceNotificationService.generateNonFinConversionNoticeWithAptcOnlyIneligibile(caseNumber);
				break;

			case "EE057-E020ChangeActionRequiredNotice":
				ecmId = referralLceNotificationService.generateChangeActionNotice(caseNumber, null, true);
				break;

			case "EE057-E022ChangeActionRequiredNotice":
				ecmId = referralLceNotificationService.generateChangeActionNotice(caseNumber, null, false);
				break;
			case "DenialNotificationEmailNotice":
				ecmId = referralLceNotificationService.generateDenialNotice(caseNumber);
				break;
			case "EE032ConversionNonFinToFin":
				ecmId = referralLceNotificationService.generateNonFinToFinConversionNotice(caseNumber);
				break;
			case "EE033NonFinToFinManual":
				ecmId = referralLceNotificationService.generateNonFinToFinConversionNoticeManual(caseNumber);
				break;
			case "OEWithAutoLinking":
				ecmId = referralQENotificationService.generateOEWithAutoLinkingNotice(caseNumber);
				break;
			case "SuccessAutoRenewalNotice":
				ecmId = renewalNoticeService.triggerRenewalNotice(caseNumber, "Renewal Notice", "SuccessAutoRenewalNotice");
				break;
			case "FailedAutoRenewalNotice":
				ecmId = renewalNoticeService.triggerRenewalNotice(caseNumber, "Renewals Notice: Plan not available", "FailedAutoRenewalNotice");
				break;
				
			case "EE018FCSPlanChange":
				if (null != noticeData)
				{
					boolean isHealthDisenrolled = false;
					boolean isDentalDisenrolled = false;
					Date terminationDate = null;
					if("Y".equalsIgnoreCase(noticeData.get(IS_HEALTH_DISENROLLED)) )
					{
						isHealthDisenrolled = true;
					}
					
					if("Y".equalsIgnoreCase(noticeData.get(IS_DENTAL_DISENROLLED)) )
					{
						isDentalDisenrolled = true;
					}
					if(null != noticeData.get(TERMINATION_DATE))
					{
						terminationDate = ReferralUtil.convertStringToDate(noticeData.get(TERMINATION_DATE), ReferralConstants.DB_DATE_FORMAT);
					}
					ecmId = referralLceNotificationService.generateCSPlanChangeNotice(caseNumber, isHealthDisenrolled, isDentalDisenrolled, terminationDate);
				}
				break;
			case "EE018FRTRatingAreaChange":
				boolean isHealthDisenrolled = false;
				boolean isDentalDisenrolled = false;
				Date terminationDate = null;
				if("Y".equalsIgnoreCase(noticeData.get(IS_HEALTH_DISENROLLED)) )
				{
					isHealthDisenrolled = true;
				}
				
				if("Y".equalsIgnoreCase(noticeData.get(IS_DENTAL_DISENROLLED)) )
				{
					isDentalDisenrolled = true;
				}
				if(null != noticeData.get(TERMINATION_DATE))
				{
					terminationDate = ReferralUtil.convertStringToDate(noticeData.get(TERMINATION_DATE), ReferralConstants.DB_DATE_FORMAT);
				}
				ecmId = referralLceNotificationService.generateRatingAreaChangeNotice(caseNumber, isHealthDisenrolled, isDentalDisenrolled, terminationDate); 
				break;	
			case "EE060AutomationNotice":
				ecmId = referralLceNotificationService.generateAutomationNoticeEE060(caseNumber);
			case "EE061LCEDocumentRequiredNotice":
				ecmId = referralLceNotificationService.generateLceDocumentRequiredNotice(caseNumber);
			default:
				break;
		}

		return ecmId;
	}

	@Override
	public ReferralNoticeResponse retriggerNotice(ReferralNoticeRequest referralNoticeRequest) {

		ReferralNoticeResponse response = new ReferralNoticeResponse();
		response.setCaseNumber(referralNoticeRequest.getCaseNumber());

		if (!VALID_NOTICES.contains(referralNoticeRequest.getNoticeName())) {
			response.setResult("retriggering not supported for notice name - " + referralNoticeRequest.getNoticeName());
		} else {
			try {
				String ecmId = generateNotice(referralNoticeRequest.getCaseNumber(), referralNoticeRequest.getNoticeName(), referralNoticeRequest.getEnrolledApplicationId(), referralNoticeRequest.getNoticeData());
				response.setResult(ecmId);
			} catch (Exception e) {
				StringBuilder message = new StringBuilder().append("Unable to generate pdf for caseNumber - ").append(referralNoticeRequest.getCaseNumber()).append(" - noticeName ").append(referralNoticeRequest.getNoticeName()).append(" - ")
				        .append(e);
				response.setResult(message.toString());
				LOGGER.error(message.toString());
			}
		}
		return response;
	}

}
