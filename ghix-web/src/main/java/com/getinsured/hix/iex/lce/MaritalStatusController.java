package com.getinsured.hix.iex.lce;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;




/**
 * This controller serves request for 
 * Life change event Marital status. 
 * @author patel_vj
 *
 */
@Controller
@RequestMapping("/iex/lce/maritalstatus")
public class MaritalStatusController {
	
	private static final String COMPLETED_URL = "/completed";
	private static final String OTHER_URL = "/other";
	private static final String INCOME_SUMMARY_URL = "/incomesummary";
	private static final String OTHER_INCOME_URL = "/otherincome";
	private static final String ALIMONY_INCOME_URL = "/alimonyincome";
	private static final String FARMING_OR_FISHING_INCOME_URL = "/farmingorfishingincome";
	private static final String RENTAL_OR_ROYAL_INCOME_URL = "/rentalorroyalincome";
	private static final String INVESTMENT_INCOME_URL = "/investmentincome";
	private static final String REDIRECT = "redirect:";
	private static final String CAPITAL_GAINS_URL = "/capitalgains";
	private static final String RETIREMENT_PENSION_URL = "/retirementpension";
	private static final String UNEMPLOYENT_INCOME = "/unemployentincome";
	private static final String SOCIAL_SECURITY_BENEFITS_INCOME_URL = "/socialsecuritybenefitsincome";
	private static final String SELF_EMPLOYMENT_INCOME = "/selfemploymentincome";
	private static final String JOB_INCOME = "/jobincome";
	private static final String INCOME_URL = "/income";
	private static final String ADDRESS_URL = "/address";
	private static final String ETHNICITY_AND_RACE = "/ethnicityandrace";
	private static final String CITIZENSHIP_URL = "/citizenship";
	private static final String HOUSEHOLD_INFORMATION_URL = "/householdinformation";
	private static final String DATE_OF_CHANGE_URL = "/dateofchange";
	private static final String MARITAL_STATUS_CONTROLLER_TILES_URL="/iex/lifechangeevent/maritalstatus";
	
	/**
	 * handles date change get request and forward to date of change view
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = DATE_OF_CHANGE_URL, method = RequestMethod.GET)
	public String viewDateOfChange(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+DATE_OF_CHANGE_URL;
	}
	
	/**
	 * handles date of change post request and redirect to view household information  
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/savedateofchange", method = RequestMethod.POST)
	public String submitDateOfChange(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+HOUSEHOLD_INFORMATION_URL;
	}
	
	/**
	 * handles household information get request and redirects to view page
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = HOUSEHOLD_INFORMATION_URL, method = RequestMethod.GET)
	public String viewHouseholdInfromation(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+HOUSEHOLD_INFORMATION_URL;
	}
	
	
	@RequestMapping(value = "/savehouseholdinformation", method = RequestMethod.POST)
	public String submitHouseholdInfromation(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+CITIZENSHIP_URL;
	}
	
	
	@RequestMapping(value = CITIZENSHIP_URL, method = RequestMethod.GET)
	public String viewCitizenship(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+CITIZENSHIP_URL;
	}
	
	
	@RequestMapping(value = "/savecitizenship", method = RequestMethod.POST)
	public String submitCitizenship(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+ETHNICITY_AND_RACE;
	}
	
	@RequestMapping(value = ETHNICITY_AND_RACE, method = RequestMethod.GET)
	public String viewEthnicityAndRace(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+ETHNICITY_AND_RACE;
	}
		
	@RequestMapping(value = "/saveethnicityandrace", method = RequestMethod.POST)
	public String submitEthnicityAndRace(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+ADDRESS_URL;
	}
	
	@RequestMapping(value = ADDRESS_URL, method = RequestMethod.GET)
	public String viewAddress(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+ADDRESS_URL;
	}
	
	@RequestMapping(value = "/saveaddress", method = RequestMethod.POST)
	public String submitAddress(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+INCOME_URL;
	}

	@RequestMapping(value = INCOME_URL, method = RequestMethod.GET)
	public String viewIncome(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+INCOME_URL;
	}
	
	@RequestMapping(value = "/saveincome", method = RequestMethod.POST)
	public String submitIncome(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+JOB_INCOME;
	}
	
	@RequestMapping(value = JOB_INCOME, method = RequestMethod.GET)
	public String viewJobIncome(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+JOB_INCOME;
	}
	
	@RequestMapping(value = "/savejobincome", method = RequestMethod.POST)
	public String submitJobIncome(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+SELF_EMPLOYMENT_INCOME;
	}
	
	@RequestMapping(value = SELF_EMPLOYMENT_INCOME, method = RequestMethod.GET)
	public String viewSelfEmploymentIncome(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+SELF_EMPLOYMENT_INCOME;
	}
	
	@RequestMapping(value = "/saveselfemploymentincome", method = RequestMethod.POST)
	public String submitSelfEmploymentIncome(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+SOCIAL_SECURITY_BENEFITS_INCOME_URL;
	}

	@RequestMapping(value = SOCIAL_SECURITY_BENEFITS_INCOME_URL, method = RequestMethod.GET)
	public String viewSocialSecurityBenefitsIncome(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+SOCIAL_SECURITY_BENEFITS_INCOME_URL;
	}
	
	@RequestMapping(value = "/savesocialsecuritybenefitsincome", method = RequestMethod.POST)
	public String submitSocialSecurityBenefitsIncome(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+UNEMPLOYENT_INCOME;
	}

	@RequestMapping(value = UNEMPLOYENT_INCOME, method = RequestMethod.GET)
	public String viewUnemployentIncome(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+UNEMPLOYENT_INCOME;
	}
	
	@RequestMapping(value = "/saveunemployentincome", method = RequestMethod.POST)
	public String submitUnemployentIncome(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+RETIREMENT_PENSION_URL;
	}
	
	@RequestMapping(value = RETIREMENT_PENSION_URL, method = RequestMethod.GET)
	public String viewRetirementPension(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+RETIREMENT_PENSION_URL;
	}
	
	@RequestMapping(value = "/saveretirementpension", method = RequestMethod.POST)
	public String submitRetirementPension(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+CAPITAL_GAINS_URL;
	}

	@RequestMapping(value = CAPITAL_GAINS_URL, method = RequestMethod.GET)
	public String viewCapitalGains(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+CAPITAL_GAINS_URL;
	}
	
	@RequestMapping(value = "/savecapitalgains", method = RequestMethod.POST)
	public String submitCapitalGains(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+INVESTMENT_INCOME_URL;
	}

	@RequestMapping(value = INVESTMENT_INCOME_URL, method = RequestMethod.GET)
	public String viewInvestmentIncome(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+INVESTMENT_INCOME_URL;
	}
	
	@RequestMapping(value = "/saveinvestmentincome", method = RequestMethod.POST)
	public String submitInvestmentIncome(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+RENTAL_OR_ROYAL_INCOME_URL;
	}

	@RequestMapping(value = RENTAL_OR_ROYAL_INCOME_URL, method = RequestMethod.GET)
	public String viewRentalOrRoyalIncome(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+RENTAL_OR_ROYAL_INCOME_URL;
	}
	
	@RequestMapping(value = "/saverentalorroyalincome", method = RequestMethod.POST)
	public String submitRentalOrRoyalIncome(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+FARMING_OR_FISHING_INCOME_URL;
	}

	@RequestMapping(value = FARMING_OR_FISHING_INCOME_URL, method = RequestMethod.GET)
	public String viewFarmingOrFishingIncome(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+FARMING_OR_FISHING_INCOME_URL;
	}
	
	@RequestMapping(value = "/savefarmingorfishingincome", method = RequestMethod.POST)
	public String submitFarmingOrFishingIncome(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+ALIMONY_INCOME_URL;
	}
	
	@RequestMapping(value = ALIMONY_INCOME_URL, method = RequestMethod.GET)
	public String viewAlimonyIncome(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+ALIMONY_INCOME_URL;
	}
	
	@RequestMapping(value = "/savealimonyincome", method = RequestMethod.POST)
	public String submitAlimonyIncome(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+OTHER_INCOME_URL;
	}
	
	@RequestMapping(value = OTHER_INCOME_URL, method = RequestMethod.GET)
	public String viewOtherIncome(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+OTHER_INCOME_URL;
	}
	
	@RequestMapping(value = "/saveotherincome", method = RequestMethod.POST)
	public String submitOtherIncome(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+INCOME_SUMMARY_URL;
	}
	
	@RequestMapping(value = INCOME_SUMMARY_URL, method = RequestMethod.GET)
	public String viewIncomeSummary(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+INCOME_SUMMARY_URL;
	}
	
	@RequestMapping(value = "/saveincomesummary", method = RequestMethod.POST)
	public String submitIncomeSummary(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+OTHER_URL;
	}
	
	@RequestMapping(value = OTHER_URL, method = RequestMethod.GET)
	public String viewOther(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+OTHER_URL;
	}
	
	@RequestMapping(value = "/saveother", method = RequestMethod.POST)
	public String submitOther(Model model,HttpServletRequest request){
		return REDIRECT+MARITAL_STATUS_CONTROLLER_TILES_URL+COMPLETED_URL;
	}
	
	@RequestMapping(value = COMPLETED_URL, method = RequestMethod.GET)
	public String viewCompleted(Model model,HttpServletRequest request){
		return MARITAL_STATUS_CONTROLLER_TILES_URL+COMPLETED_URL;
	}
	
	

}
