<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript">
var isPlanPresent = true;
var isSilverPlanPresent = '${isSilverPlan}';
var isCSPlan1Present = '${section1 == "1"}';
var isCSPlan2Present = '${section2 == "2"}';
var isCSPlan3Present = '${section3 == "3"}';
var isProviderPresent = true;
</script>

<div class="gutter10">   
<div class="row-fluid">
        <div class="span3" id="sidebar">
        	<div class="header">
	        	<!--  beginning of side bar -->
                <h4 class="margin0"><a name="skip"></a><spring:message  code='label.addnewQHPapplication'/></h4>
            </div>
	            <ol class="nav nav-list">
                    <li class="done"><a href="#"><spring:message  code='label.provideplandetails'/></a></li>
                    <li class="done"><a href="#"><spring:message  code='label.uploadplanbenefits'/></a></li>
                    <li class="done"><a href="#"><spring:message  code='label.uploadplanrates'/></a></li>
                    <li class="done"><a href="#"><spring:message  code='label.selectoraddprovidernetworks'/></a></li>
                    <!--<li><a href="#"><spring:message  code='label.addcostsharingvariations'/></a></li>-->
                    <li class="active"><a href="#"><spring:message  code='label.reviewandconfirm'/></a></li>
                </ol>
	        	<!-- end of side bar -->
        	
        </div><!-- end of span3 -->
        
        <div class="span9" id="rightpanel">
        <div class="header">
		            <h4 class="margin0"> <spring:message code='label.step6'/> <spring:message  code='label.reviewandconfirm'/></h4>
		           </div>
					<div class="row-fluid">
						<h4 class="noBackground review"><spring:message code='label.attachmentMsg'/></h4>
						<table class="table">
							<thead>
								<tr>
									<td class="span4 txt-right"><spring:message  code='label.issuerName'/></td>
									<td><strong>${issuerName}</strong></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="txt-right"><spring:message  code="label.planName"/></td>
									<td><strong>${planName}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.planNumber"/></td>
									<td><strong>${planNumber}</strong></td>
								</tr>
							</tbody>
						</table>
					</div>
					<br />
					<div class="row-fluid">
						<table class="table">
							<thead>
								<tr class="alert alert-info">
									<th colspan="2" class="txt-right" scope="col"><h4 class="noBackground pull-left DocumentTxt">${planName}</h4>
									<img id="loadingImg"  src="<c:url value="/resources/images/loader.white.gif" />" alt="loader"/>
									<button class="btn" onclick="deleteSilverPlan(0)">
											<i class="icon-trash"></i><spring:message  code="label.btnDelete"/>
										</button>
										<button class="btn" onclick="updateSilverPlan(0)">
											<i class="icon-pencil"></i><spring:message  code="button.label"/>
										</button></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="span4 txt-right"><spring:message  code="label.linkProviderNetwork"/></td>
									<td><strong>${netName}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.planStartDate"/></td>
									<td><strong>${startDate}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.planEndDate"/></td>
									<td><strong>${endDate}</strong></td>
								</tr>
								<tr>
								<!--  No longer exists
								 <tr>
									<td class="txt-right"><spring:message  code="label.maximumEnrollment"/></td>
									<td><strong>${maxEnrolee}</strong></td>
								</tr>-->
								<tr>
									<td class="txt-right"><spring:message  code="label.valueCertificate"/></td>
									<td><strong>${actCerti}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.planBenefitData"/></td>
									<td><strong>${benefitFile}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.rateByZip"/></td>
									<td><strong>${rateFile}</strong></td>
								</tr>
							</tbody>
						</table>
					</div>
					<br />
					<c:choose>
						<c:when test='${isSilverPlan == true}'>
					<div class="row-fluid">
						<table  class="table">
							<thead>
								<tr class="alert alert-info">
									<th colspan="2" class="silverPlan1 txt-right" scope="col"><h4 class="noBackground pull-left DocumentTxt">${planName} <spring:message code="label.planCodeName1"/></h4>
									<img id="loadingImg2"  src="<c:url value="/resources/images/loader.white.gif" />" alt="loader"/>
									<button class="btn" onclick="deleteSilverPlan(1)">
											<i class="icon-trash"></i><spring:message  code="label.btnDelete"/>
										</button>
										<button class="btn" onclick="updateSilverPlan(1)">
											<i class="icon-pencil"></i><spring:message  code="button.label"/>
										</button></th>
									<th class="addSilverPlan1" scope="col"><button class="btn" onclick="updateSilverPlan(1)">
											<i class="icon-plus"></i><spring:message  code="label.addSivlverPln"/> 
										</button></th>
								</tr>
							</thead>
							<tbody class="silverPlan1">
								<tr>
									<td class="span4 txt-right"><spring:message  code="label.valueCertificate"/></td>
									<td><strong>${actCerti1}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.planBenefitData"/></td>
									<td><strong>${benefitFile1}</strong></td>
								</tr>
							</tbody>
						</table>
					</div>
					<br />
					<div class="row-fluid">
						<table id="silverPlan2" class="table">
							<thead>
								<tr class="alert alert-info">
									<th colspan="2" class="silverPlan2 txt-right" scope="col"><h4 class="noBackground pull-left DocumentTxt">${planName} <spring:message code="label.planCodeName2"/></h4>
									<img id="loadingImg3"  src="<c:url value="/resources/images/loader.white.gif" />" alt="loader"/>
									<button class="btn" onclick="deleteSilverPlan(2)">
											<i class="icon-trash"></i><spring:message  code="label.btnDelete"/>
										</button>
										<button class="btn" onclick="updateSilverPlan(2)">
											<i class="icon-pencil"></i><spring:message  code="button.label"/>
										</button></th>
									<th class="addSilverPlan2"><button class="btn" onclick="updateSilverPlan(2)">
											<i class="icon-plus"></i><spring:message  code="label.addSivlverPln"/>
										</button></th>
								</tr>
							</thead>
							<tbody class="silverPlan2">
								<tr>
									<td class="span4 txt-right"><spring:message  code="label.valueCertificate"/></td>
									<td><strong>${actCerti2}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.planBenefitData"/></td>
									<td><strong>${benefitFile2}</strong></td>
								</tr>
							</tbody>
						</table>
					</div>
					<br />
					<div class="row-fluid">
						<table id="silverPlan3" class="table">
							<thead>
								<tr class="alert alert-info">
									<th colspan="2" class="silverPlan3 txt-right" scope="col"><h4 class="noBackground pull-left DocumentTxt">${planName} <spring:message code="label.planCodeName3"/></h4>
									<img id="loadingImg4"  src="<c:url value="/resources/images/loader.white.gif" />" alt="loader"/>
									<button class="btn" onclick="deleteSilverPlan(3)">
											<i class="icon-trash"></i><spring:message  code="label.btnDelete"/>
										</button>
										<button class="btn" onclick="updateSilverPlan(3)">
											<i class="icon-pencil"></i><spring:message  code="button.label"/>
										</button></th>
									<th class="addSilverPlan3" scope="col"><button class="btn" onclick="updateSilverPlan(3)">
											<i class="icon-plus"></i><spring:message  code="label.addSivlverPln"/>
										</button></th>
								</tr>
							</thead>
							<tbody class="silverPlan3">
								<tr>
									<td class="span4 txt-right"><spring:message  code="label.valueCertificate"/></td>
									<td><strong>${actCerti3}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.planBenefitData"/></td>
									<td><strong>${benefitFile3}</strong></td>
								</tr>
							</tbody>
						</table>
					</div>
					</c:when>
					</c:choose>
					<br />
					<div class="row-fluid">
						<table class="table">
							<thead>
								<tr class="alert alert-info">
									<th colspan="2" class="silverPlan6 txt-right" scope="col"><h4 class="noBackground pull-left DocumentTxt">${netName}</h4>
									<button class="btn" onclick="deleteSilverPlan(6)" >
											<i class="icon-trash"></i><spring:message  code="label.btnDelete"/>
										</button>
										<button class="btn" onclick="updateSilverPlan(6)">
											<i class="icon-pencil"></i><spring:message  code="button.label"/>
										</button></th>
									<th class="addSilverPlan6" scope="col"><button class="btn" onclick="updateSilverPlan(6)">
											<i class="icon-trash"></i><spring:message  code="label.addSivlverPln"/>
										</button></th>
								</tr>
							</thead>
							<tbody class="silverPlan6">
								<tr>
									<td class="span4 txt-right"><spring:message  code="label.providerNetworkType"/></td>
									<c:if test="${fn:toUpperCase(netType) == 'HMO'}">
										<th><spring:message code='label.hmo' javaScriptEscape='true'/></th>
									</c:if>	
									<c:if test="${fn:toUpperCase(netType) == 'PPO'}">
										<th><spring:message code='label.ppo' javaScriptEscape='true'/></th>
									</c:if>	
								</tr>
								<!--<tr>
									<td>Network Adequacy</td>
									<td>Adequacy_Name1.pdf</td>
								</tr>
								<tr>
									<td>Provider Database</td>
									<td>Provider_Name1.xls</td>
								</tr>-->
							</tbody>
						</table>
					</div>
 					<form class="form-stacked" id="frmSubmit" name="frmSubmit" action="<c:url value="/admin/planmgmt/editSilverPlan" />" method="post" enctype="multipart/form-data">
                      <input type="hidden" name="secion" id="secion"/>
                      <input type="hidden" name="planId" id="planId" value="${planId}"/>
                      <df:csrfToken/>
                     </form>
 					<form class="form-stacked" id="frmDelete" name="frmDelete" action="<c:url value="/admin/planmgmt/deletePlan" />" method="post" enctype="multipart/form-data">
                      <input type="hidden" name="secionId" id="secionId"/>
                      <input type="hidden" name="planId" id="planId" value="${planId}"/>
                      <df:csrfToken/>
                     </form>
 					<form class="form-stacked" id="frmChangeState" name="frmChangeState" action="<c:url value="/admin/planmgmt/submitqhpstatus" />" method="post" enctype="multipart/form-data">
                      <input type="hidden" name="planId" id="planId" value="${planId}"/>
                      <df:csrfToken/>
                     </form>



				<div class="form-actions">
					<button class="btn" onClick="window.print()">
						<i class="icon-print"></i><spring:message code="label.btnPrint"/> 
					</button>
					<a href='<c:url value="/admin/planmgmt/pdfconversion?planId=${planId}"/>'><button class="btn"><spring:message code="label.btnSaveAsPdf"/></button></a>
					<!--<button class="btn">
						<i class="icon-plus-sign"></i> Add More QHPs
					</button>-->
					<img id="loadingImg1" align="right"  src="<c:url value="/resources/images/loader.white.gif" />" alt="loader"/>
					<button type="submit" id="submit1" name="submit1" class="btn btn-primary pull-right" onClick="changeState()">
						 <spring:message  code="label.btnSubmit"/>
					</button>
					<!--<input type="submit" name="submit" id="submit" value="Submit" title="Submit"
						class="btn btn-primary pull-right" />-->
				</div>
				<!-- end of form-actions -->
		</div><!-- end of span9 -->
        <!-- assister registration -->
<div id="confirmBox" class="hide">
<div class="modal-body message"></div>
</div>
</div>
</div>
<!--<div id="modalBox">
    <img id="loadingImg"  src="<c:url value="/resources/images/loader.white.gif" />" alt="loader"/>
</div>-->
<!--  end of row-fluid -->

<script type="text/javascript">
function savePageAsPDF() { 
	   var pURL = "http://savepageaspdf.pdfonline.com/pdfonline/pdfonline.asp?cURL=" + escape(document.location.href); 
	  window.open(pURL, "PDFOnline", "scrollbars=yes, resizable=yes,width=640, height=480,menubar,toolbar,location");
} 
function  formSubmit()
{
	//if($("#optionsCheckbox").is(":checked"))
	{
		$("#frmUploadBenefits").submit();
	}
}
function changeState()
{
	//if($("#optionsCheckbox").is(":checked"))
	{
		$("#frmChangeState").submit();
	}
}
$(document).ready(function(){
	//getIssuerNames();
	$('#loadingImg').hide();
	$('#loadingImg1').hide();
	$('#loadingImg2').hide();
	$('#loadingImg3').hide();
	$('#loadingImg4').hide();
	var deletePlans = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitDeletionRequest,  // pre-submit callback 
	        success:       submitDeletionResponse  // post-submit callback 
	    };
    $('#frmDelete').ajaxForm(deletePlans);

    var changeStatus = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitChangeStateRequest,  // pre-submit callback 
	        success:       submitChangeStateResponse  // post-submit callback 
	    };
    $('#frmChangeState').ajaxForm(changeStatus);
});


function submitDeletionResponse(responseText, statusText, xhr, $form)  {
	var ctx = "${pageContext.request.contextPath}";
	if(statusText=='success' && responseText != 'success')
		{window.location.replace(ctx + responseText);}
	updateSections();
	
    	
}

function submitDeletionRequest(formData, jqForm, options) {
	return true;
}

function submitChangeStateResponse(responseText, statusText, xhr, $form)  {	
	doSubmit("<spring:message code='label.info'/>", "");
}

function submitChangeStateRequest(formData, jqForm, options) {
	$('#loadingImg1').show();
	return true;
}
</script>

<script type="text/javascript">
function doConfirm(msg, section)
{
    var confirmBox = $("#confirmBox");
    confirmBox.find(".message").text(msg);
    confirmBox.dialog
    ({
      autoOpen: true,
      modal: true,
	  resizable:false,
	  draggable: false,
      buttons:
        {
          'Yes': function () {
            $(this).dialog('close');
            deletePlan(section);
            //$(this).find(".yes").click(yesFn);
          },
          'No': function () {
            $(this).dialog('close');
            noFn;
            //$(this).find(".no").click(noFn);
          }
        }
    });
}
function doSubmit(msg, section)
{
    var confirmBox = $("#confirmBox");
    confirmBox.find(".message").text(msg);
    confirmBox.dialog
    ({
      autoOpen: true,
      modal: true,
      buttons:
        {
          'OK': function () {
            $(this).dialog('close');
            var ctx = "${pageContext.request.contextPath}/admin/planmgmt/manageqhpplans";
            window.location.replace(ctx);
            //deletePlan(section);
            //$(this).find(".yes").click(yesFn);
          }
        }
    });
}
 function deletePlan(section)
 {
	 if(section == 0)
		 {
		 $('#loadingImg').show();
		 //showModal
		 }
	 if(section == 1)
	{
		 $('#loadingImg2').show();
		isCSPlan1Present=false;
	}
	else if(section == 2)
	{
		 $('#loadingImg3').show();
		isCSPlan2Present=false;
	}
	else if(section == 3)
	{
		 $('#loadingImg4').show();
		isCSPlan3Present=false;
	}
	else if(section == 6)
	{
		isProviderPresent=false;
	}
	 $('#secionId').val(section);
		$('#frmDelete').submit();
	//$('#silverPlan'+section).hide();
	//$('#addSilverPlan'+section).show();
	updateSubmit();
	}

function updateSilverPlan(section)
{
	$('#secion').val(section);
	$('#frmSubmit').submit();
}

function deleteSilverPlan(section)
{
	doConfirm("<spring:message code='label.confirmation'/>",section);
}


function updateSubmit()
{
	var enableSubmit = false;
	if (isPlanPresent && isProviderPresent)
	{
		if (isSilverPlanPresent== 'true' )
		{
			if (isCSPlan1Present == 'true' && isCSPlan2Present == 'true' && isCSPlan3Present == 'true')
				{
					enableSubmit = true;
				}
		}
		else
		{
			enableSubmit = true;
		}
	}
	if (enableSubmit == true)
	{
		$("#submit1").removeAttr("disabled");
	}
	else
	{
		$("#submit1").attr("disabled", "disabled");
	}
}

function updateSections()
{	
	if (isSilverPlanPresent == 'true')
	{
		if (isCSPlan1Present == 'true')
		{
			 $('#loadingImg2').hide();
			showNHideElement('silverPlan1','addSilverPlan1');
		}else
		{
			showNHideElement('addSilverPlan1','silverPlan1');
		}
		
		if(isCSPlan2Present == 'true')
		{
			 $('#loadingImg3').hide();
			showNHideElement('silverPlan2','addSilverPlan2');
		}
		else
		{
			showNHideElement('addSilverPlan2','silverPlan2');
		}
		
		if (isCSPlan3Present == 'true')
		{
			 $('#loadingImg4').hide();
			showNHideElement('silverPlan3','addSilverPlan3');
		}
		else
		{
			showNHideElement('addSilverPlan3','silverPlan3');
		}
	}
	if (isProviderPresent)
	{
		showNHideElement('silverPlan6','addSilverPlan6');
	}
	else
	{
		showNHideElement('addSilverPlan6','silverPlan6');
	}
}

function showNHideElement(showElement,hideElement)
{
	$('.'+hideElement).hide();
	$('.'+showElement).show();
}

$(document).ready(function(){
	updateSections();		
	updateSubmit();
});

</script>	
