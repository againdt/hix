package com.getinsured.hix.planmgmt.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;

import com.getinsured.hix.admin.util.PlanTenantUtil;
import com.getinsured.hix.dto.planmgmt.FormularyDrugListResponseDTO;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.IssuerVerificationStatus;
import com.getinsured.hix.model.Plan.PlanInsuranceType;
import com.getinsured.hix.model.Plan.PlanStatus;
import com.getinsured.hix.model.PlanCompareReport;
import com.getinsured.hix.model.PlanDental;
import com.getinsured.hix.model.PlanDentalBenefit;
import com.getinsured.hix.model.PlanDentalCost;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.PlanHealthBenefit;
import com.getinsured.hix.model.PlanHealthCost;
import com.getinsured.hix.planmgmt.dto.PlanLightResponseDTO;
import com.getinsured.hix.planmgmt.util.ServiceAreaDetails;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notify.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.util.GhixConstants.FileType;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Any "field" you declare in an interface will be a static final field
 * 
 */
public interface PlanMgmtService {

	public static final	String HEALTH = PlanInsuranceType.HEALTH.toString();
	public static final	String VISION = PlanInsuranceType.VISION.toString();
	public static final	String DENTAL = PlanInsuranceType.DENTAL.toString();
	public static final	String LIFE = PlanInsuranceType.LIFE.toString();
	public static final	String AME = PlanInsuranceType.AME.toString();
	public static final	String STM = PlanInsuranceType.STM.toString();
	public static final	String MEDICARE = PlanInsuranceType.MEDICARE.toString();

	public static final	String STATUS_INCOMPLETE = PlanStatus.INCOMPLETE.toString();
	public static final	String STATUS_LOADED = PlanStatus.LOADED.toString();
	public static final	String STATUS_CERTIFIED = PlanStatus.CERTIFIED.toString();
	public static final	String STATUS_DECERTIFIED = PlanStatus.DECERTIFIED.toString();
	public static final	String ISSUER_VERIFICATION_STATUS_VERIFIED = IssuerVerificationStatus.VERIFIED.toString();
	public static final	String ISSUER_VERIFICATION_STATUS_PENDING = IssuerVerificationStatus.PENDING.toString();

	PlanDental getDentalPlan(int id);

	Plan getPlan(int id);

	Plan findPlanByIdAndIssuerId(int planId, int issuerId);
	
	Plan savePlan(Plan plan) throws InvalidUserException, GIException, IOException, ContentManagementServiceException;

	Plan savePlanBenefits(Plan plan, String benefitsFile) throws InvalidUserException, GIException, IOException, ContentManagementServiceException;

	Plan savePlanRates(Plan plan, String ratesFile) throws GIException, IOException, ContentManagementServiceException;

	// int getPendingPlanCtr(HttpServletRequest request, String insuranceType);

	<T> List<T> listData(Model model, HttpServletRequest request, Class<T> className, String insuranceType);

	List<Plan> getAddedOrUpdatedPlans() throws ParseException;

	List<Plan> getPlanList(HttpServletRequest request, String insuranceType);

	//List<Plan> getPlanListOptimized(HttpServletRequest request, String insuranceType);

	void addCostSharingPlan(String planId, String costSharing, String certiFile, String benefitsFile) throws InvalidUserException, GIException, IOException, ContentManagementServiceException, CloneNotSupportedException;

	Plan updatePlan(Plan plan);

	Plan updatePlanBenefits(Plan plan, String benefitsFile) throws InvalidUserException, GIException, IOException, ContentManagementServiceException;

	Plan editPlanBenefits(Plan plan, String benefitsFile, String benefitStartDate, String comments) throws InvalidUserException, GIException, IOException, ContentManagementServiceException;

	Plan uploadPlanBrochure(Plan plan, String brochureFile) throws InvalidUserException, GIException, IOException, ContentManagementServiceException;

	void updatePlanRates(Plan plan, String ratesFile);

	void deletePlan(String planId, String costSharing);

	void updateStatus(String planId, String loggedInUser, String status, String loggedInUserId, boolean updateEndDate, String certSuppDocFile, String comments, String issuerVerificationStatus) throws InvalidUserException;

	void updateProvider(String planId, String status) throws InvalidUserException, NumberFormatException, Exception;

	void syncPlanWithAHBX(int planId);
	
	void syncPlanWithAHBXInBulk(List<Map<String, String>> listOfPlans);

	Map<String, String> getReviewScreenDetails(String parentPlanId) throws ContentManagementServiceException;

	String getActualFileName(String changedFileName) throws ContentManagementServiceException;

	Long getMatchingPlanCount(int issuerId, String name);

	void decertifyPlans(int issuerId);

	String getSectionId(String costSharing);

	/*Commenting for SERFF changes	
	PlanLife getLifePlan(int id);
	void saveLifePlan(PlanLife planLife);
	PlanMedicare getMedicarePlan(int id);
	PlanSTM getSTMPlan(int id);
	void saveSTMPlan(PlanSTM planSTM);
	PlanAME getAMEPlan(int id);
	void saveAMEPlan(PlanAME planAME);*/

	Plan savePlanRatesEditMode(Plan plan, String rateFile, Date effectiveStartDate, String comments) throws InvalidUserException, GIException, IOException, ContentManagementServiceException;

	List<Map<String, Object>> loadPlanStatusHistory(Integer planIdForStatusHistory);

	List<Map<String, Object>> loadPlanRatesHistory(Integer planId);

	List<Map<String, Object>> loadQDPRatesHistory(Integer planId);

	List<Map<String, Object>> loadPlanEnrollmentHistory(Integer planId, String role);

	List<Map<String, Object>> loadPlanCertificationHistory(Integer planId, String role);

	List<Map<String, Object>> loadPlanHistory(Integer planId);

	List<Map<String, Object>> loadProviderHistory(Integer planId);

	List<Map<String, Object>> loadPlanBenefitsHistory(Integer planId, String role);

	Integer saveComment(Integer id, String comments, TargetName targetName, Integer commentedBy, String commenterName);

	List<Map<String, Object>> loadPlanHistoryForIssuer(Integer planId);

	boolean updateEnrollmentAvail(String planId, Integer userId, String userName, String effectiveDate, String enrollmentAvail, String comments) throws GIException, ParseException;

	String getFilePath(FileType fileType, String fileName);

	String uploadAssociatedFiles(String fileName, FileType fileType, String planId) throws GIException, IOException, ContentManagementServiceException;

	long getZipCountForCounty(String county);

	long getZipCountForCountyAndZip(String county, String zip);

	long getZipCountForZip(String zip);

	String getActualLogoName(String encodedFileName) throws ContentManagementServiceException;

	String getLogoPath();

	// New Method Added by Sandip for CMS implementation

	Plan getRequiredPlan(int issuerID, String issuerPlanNumber);

	PlanHealth getRequiredPlanHealthObject(int issuerID, int planID, String csType);

	List<PlanHealth> getPlanHealthInfo(int issuerID, int planID, String planLevelString);

	List<PlanDental> getPlanDentalInfo(int issuerID, int planID, String planLevelString);

	String updateCsrAmount(Float csrAdvPayAmt, Integer issuerID, Integer planID, String csType);

	Integer getEnrollmentCount(int planID);

	Plan getPlanByIDMarket(Integer id, String market);

	List<Plan> getPlanByHIOSID(String HIOSID);

	List<PlanHealthBenefit> getPlanHelthBenefit(Integer planId);

	List<PlanDentalBenefit> getPlanDentalBenefit(Integer planId);

	/*	Commenting for SERFF changes
	List<PlanHealthServiceRegion> getAllServiceRegionsForPlanHealth(Integer planHealthId);*/

	boolean updatePlanWithStatus(Integer planId, String status);

	List<Map<String, Object>> loadPlanRateGroupByEffectiveDate(Integer planId, String role);

	List<Map<String, Object>> loadPlanHealthBenefitHistory(Integer planId, String role);

	List<Object[]> downloadPlanRate(Integer planId, String effectiveStartDate, String effectiveEndDate) throws IOException, ParseException;

	String getDocumentNameFromECMorUCM(String documentConfigurationType, String documentID, String filename) throws ContentManagementServiceException;

	byte[] getAttachment(String documentID) throws Exception;

	Map<Integer, Integer> getTenantCount(List<Integer> planIds);

	String deleteTenants(Integer planId, String[] selectedTenants);

	List<PlanTenantUtil> addTenants(List<PlanTenantUtil> incomingData, String[] selectedTenants);

	List<Plan> getPlanList(int issuerID, String planType);

	List<Plan> getPlanByIDMarket(List<Integer> planIdList, int issuerId, String market);

	List<Plan> getPlanByIDMarket(List<Integer> planIdList, String market);
	
	 Content getContent(String documentID) throws Exception;
	 
	 boolean updatePlanStatus(Integer planId, AccountUser user, String planStatus, String certSuppFile, String comments,Date deCertificationDate,Date enrollmentEndDate,String issuerVerificationStatus) throws ParseException;
	 
	 List<String[]> bulkUpdateCertificationStatus(String status, int lastUpdatedBy, String insuranceType, String issuerPlanNumber, 
	    		String planLevel, String planMarket, String filterStatus, String isVerified, int planYear, int issuerId, String stateCode);
	 
	 boolean bulkUpdateEnrollmentAvbl(String enrollmentavail, Date enrollmentAvailEffdate, int lastUpdatedBy, String insuranceType, String issuerPlanNumber, 
	    		String planLevel, String planMarket, String filterStatus, String isVerified, int planYear, int issuerId);
	//kunal's
	 List<PlanDentalCost> getPlanDentalCost(Integer planId);
	 List<PlanHealthCost> getPlanHealthCost(Integer planId);
	
	String getInsuranceType(List<Integer> planIdList);
	String savePlanReportInfo(PlanCompareReport planCompareReport);
	
	ServiceAreaDetails  getServiceAreaDetails(Integer planId, Integer issuerId);
	
	void downLoadServiceArea(ServiceAreaDetails serviceAreaDetails, HttpServletResponse response) throws IOException;
	
	void undoDecertification(Plan plan);
	
	boolean modifyPlan(Plan plan);
	
	String findEnrollmentAvailablePlans(List<Plan> plansToUpdate);
	
	String findDeCertifiedPlans(List<Plan> plansToUpdate);
		
	List<Map<String, Integer>> getPlanYears(String insuranceType, Integer issuerId);
	
	public List<Plan> getPlans(List<Integer> ids);
	
	PlanHealth getPlanHealth(int id);
	
	PlanDental getPlanDental(int id);
	
	public List<Plan> getPlansData(List<Integer> ids);
	
	public Map<Integer,PlanLightResponseDTO> getPlansInfo(List<Integer> planIds);
		
	public String showSaveButtonForUpdate(String deCertificationStatus, String deCertificationEffDate);
	
	public List<Plan> getPlanByIssuerIdandIssuerPlanNumber(int issuerId, String issuerPlanNumber);
	
	public void sendNotificationEmailToIssuerRepresentative(List<String[]> issuerList) throws NotificationTypeNotFound, InvalidUserException, NoticeServiceException;
	
	public void sendNotificationEmailToIssuerRepresentative(Plan plan, String status) throws NotificationTypeNotFound, InvalidUserException, NoticeServiceException;
	
	FormularyDrugListResponseDTO getFormularyDrugDetails(String decryptedApplicableYear, String decryptedIssuerHiosId,
			String decryptedFormularyId);

	void downLoadFormularyDrug(FormularyDrugListResponseDTO formularyDrugListResponseDTO, HttpServletResponse response) throws IOException;
	
	void updateNetworkURL(Integer planId, String networkURL, Integer userId);
	
	void updateFomularyURL(Integer planId, String networkURL, Integer userId);

}
