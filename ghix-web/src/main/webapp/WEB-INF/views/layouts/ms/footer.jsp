<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page import="com.getinsured.hix.platform.util.*"   %>
<%@page import="com.getinsured.hix.util.GhixConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<jsp:useBean id="date" class="java.util.Date" />
<fmt:formatDate var="now" value="${date}" pattern="yyyy" />  
<%!
	String currStateExchangeType = null;
%>
<%
 currStateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
%>
		 
<footer class="container" role="contentinfo">
 <div class="row-fluid">

					<div class="pull-left">
						<ul class="nav nav-pills">
							<li id="copyrights">&copy;${now} <%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%></li>
							<li><a href="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL)%>" class="margin20-l">Privacy Policy</a></li>
						</ul>
					</div>
                    <div class="pull-right">
                    <c:set var="assisterRoleName" ><encryptor:enc value="assisterenrollmententity" isurl="true"/> </c:set>
                    	<sec:authorize access="!isAuthenticated()">
	                    	<ul class="nav nav-pills">
	                    		<% if("Individual".equalsIgnoreCase(currStateExchangeType) || "Both".equalsIgnoreCase(currStateExchangeType)) { %>
	                    			<li><a href="<c:url value="/account/signup/${assisterRoleName}"/>">Enrollment Entities</a></li>
	                    		<% } %>
	                        	<!--<li><a href="/hix/shop/employer/paperapplication">Submitting a paper application</a></li>-->
	                    		<li><a href="/hix/account/user/login">Administrators</a></li>
	                    		<li><a href="/hix/account/user/login">Issuers</a></li>
	                    	</ul>
                    	</sec:authorize>
                    	<ul class="nav nav-list pull-right hide" id="build">
  <li style="white-space: nowrap;"><a href="#">Branch Name: <%= GhixPlatformConstants.BUILD_BRANCH_NAME %></a></li>
     						<li style="white-space: nowrap;"><a href="#">Build Number: <%= GhixPlatformConstants.BUILD_NUMBER %></a></li>
                    		<li style="white-space: nowrap;"><a href="#">Build Date: <%= GhixPlatformConstants.BUILD_DATE %></a></li>
        				</ul>
                    
	                    <ul class="nav nav-list span6 ">
	                       <li><a id="callSupport" href="#"></a></li>
	                      <%--  <spring:message code="label.call"/>  --%>  
	                      <%--GhixConfiguration.BRANDNAME> Health Exchange Helpline 1-800-123-1234</a></li>--%
	                      <%--   <li><a id="" href="#"><spring:message code="label.chat"/></a></li> --%>   
	                    </ul>
                  </div>
                    <!--  
                    <div class="span1" id="social">
                   
                       <a href="#" id="twitter">Twitter</a>
                        <a href="#" id="fb">Facebook</a>
                        
                    </div>
               -->
            </div>
            
<!--      		<div class="row-fluid" style="border-top:solid 1px #ccc;margin-top:20px;"> -->
<%--  				<p class="span6" id="copyrights">&copy;<small> <%=GhixConfiguration.BRANDNAME%> <spring:message code="label.footerbrand"/> ${now}</small></p> --%>
<!--      			<ul class="nav nav-list pull-right" id="build"> -->
<%--      				<li style="white-space: nowrap;"><a href="#">Build Number: <%= GhixPlatformConstants.BUILD_NUMBER %></a></li> --%>
<%--                     <li style="white-space: nowrap;"><a href="#">Build Date: <%= GhixPlatformConstants.BUILD_DATE %></a></li> --%>
<!--         		</ul> -->
<!--         	</div> -->
                       
	 </footer><!-- footer --> 


