<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<META http-equiv="Cache-Control" content="max-age=0" />
<META http-equiv="Cache-Control" content="no-cache,no-store,must-revalidate" />
<META HTTP-EQUIV="Expires" CONTENT="Mon, 22 Jul 2002 11:12:01 GMT">
<META http-equiv="Pragma" content="no-cache" />
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/modal-zipcode.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/modal-zipcode-utils.js" />"></script>

<link href="<gi:cdnurl value="/resources/css/chosen.css" />" rel="stylesheet" type="text/css" media="screen,print">
<link href="<gi:cdnurl value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script src="<gi:cdnurl value="/resources/js/jquery.validate.min.js" />" type="text/javascript"></script>
<link href="<gi:cdnurl value="/resources/css/autoSuggest.css" />" media="screen" rel="stylesheet" type="text/css" />
<script src="<gi:cdnurl value="/resources/js/jquery.autoSuggest.js" />" type="text/javascript"></script>
<script src="<gi:cdnurl value="/resources/js/jquery.autoSuggest.minified.js" />" type="text/javascript"></script>

<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/chosen.jquery.js" />"></script>


 <script type="text/javascript">
  $(document).ready(function(){
	  var config = {
		      '.chosen-select'           : {},
		      '.chosen-select-deselect'  : {allow_single_deselect:true},
		      '.chosen-select-no-single' : {disable_search_threshold:10},
		      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		      '.chosen-select-width'     : {width:"95%"}
		    }
		    for (var selector in config) {
		      $(selector).chosen(config[selector]);
		    }
  });

	$(document).ready(function() {
		// disable autocomplete
		if (document.getElementsByTagName) {
			var inputElements = null;
			var elemIds = ["address1", "address2", "city", "state", "zip"];
			
			for(i=0; elemIds[i]; i++) {
				inputElements = document.getElementById(elemIds[i]);
				
				for (j=0; inputElements[j]; j++) {							
					if (inputElements[j].className && (inputElements[j].className.indexOf("disableAutoComplete") != -1)) {
						inputElements[j].setAttribute("autocomplete","off");
					}
				}
			}
		}
	});

</script>

<div class="gutter10">
<%-- <div class="row-fluid">
	<div class="page-header">
		<c:choose>
          	          <c:when test="${enrollmentEntity.registrationStatus=='Incomplete'}">
          			    <h1><spring:message code="label.entity.step"/> 5: <spring:message code="label.assister.assisters"/></h1>
          		      </c:when>
          		      <c:otherwise>
          		       <h1><spring:message code="label.assister.assisters"/></h1>
          		      </c:otherwise>
          		    </c:choose>
	</div>
</div> --%>


		<div class="row-fluid margin20-t">
		
		<jsp:include page="../leftNavigationMenu.jsp" />
    
     	<div class="span9" id="rightpanel">
     		<div id="section">
     	            <div class="header">
     	            	<h4><spring:message code="label.entity.step"/> 5: <spring:message code="label.assister.assisters"/></h4>
     	            </div>
     	            <div class="gutter10">
     	            
     	         <c:choose>
					<c:when test="${CA_STATE_CODE}">
						<c:if test="${loggedUser =='entityAdmin' || enrollmentEntity.registrationStatus=='Incomplete'}">
							<div class="control-group">
								<p><spring:message code="label.assister.inThisSecULLProInfAbtOrgAss"/></p>
	            			 		<input class="btn btn-primary" id="addAssister" onClick="window.location.href='<c:url value="/entity/assister/addassister"/>'" type="button" value="<spring:message code="label.assister.addAssister"/>"> 
							</div>
						</c:if>
					</c:when>
					<c:otherwise>
						<c:choose>          
	         	 			<c:when test="${loggedUser!='entityAdmin' && enrollmentEntity.registrationStatus=='Pending'}">                      				
	             			</c:when>
	             			<c:otherwise>
	             				<div class="control-group">
	             					<p><spring:message code="label.assister.inThisSecULLProInfAbtOrgAss"/></p>
	             			 		<input class="btn btn-primary" id="addAssister" onClick="window.location.href='<c:url value="/entity/assister/addassister"/>'" type="button" value="<spring:message code="label.assister.addAssister"/>"> 
	             				</div>
	             			</c:otherwise>
	             		</c:choose>	
					</c:otherwise>
				</c:choose> 

            	<%-- <div class="control-group">  
	              <table class="table table-border-none">
						<tr>
							<c:choose>          
         	 					<c:when test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus!='Incomplete' || loggedUser =='entityAdmin' && enrollmentEntity.registrationStatus!='Incomplete'}">                      				
             					</c:when>
             					<c:otherwise>				                     
				                	<td><a class="btn" id="assisterBack" onClick="window.location.href='/hix/entity/enrollmententity/entitycontactinfo'"><spring:message code="label.assister.back"/></a></td>
				                	<td width="50"><a class="btn btn-primary" id="assisterDone" onClick="window.location.href='/hix/entity/enrollmententity/documententity'"><spring:message code="label.assister.done"/> </a></td> 
					        	</c:otherwise>
				        	</c:choose>
				        </tr>	
				   </table>	           
	             </div> --%>
	             
	             	<div class="clear form-actions">
	             		<c:choose>          
       	 					<c:when test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus!='Incomplete' || loggedUser =='entityAdmin' && enrollmentEntity.registrationStatus!='Incomplete'}"></c:when>
	           				<c:otherwise>				                     

				        		 <input name="back" class="btn" id="assisterBack" onClick="window.location.href='<c:url value="/entity/enrollmententity/entitycontactinfo" />'" type="button" value='<spring:message code="label.assister.back"/>'>
								 <input name="done" class="btn btn-primary pull-right" id="assisterDone" onClick="window.location.href='<c:url value="/entity/enrollmententity/documententity" />'" type="button" value="<spring:message code="label.assister.done"/>"> 
				        	</c:otherwise>
			        	</c:choose>        
	             	</div>
	             
	            <c:if test="${fn:length(assisterList) != 0}"> 
	            	<div class="header">
		             <h4 id="assisterHeader"><spring:message code="label.assister.assisterRoster"/></h4>
		        	</div>
		            	<div id="assisterTable" class="control-group">
		          			<div id="section">
		          				<div class="gutter10">		 			
						         <table class="table margin20-t" id="siteList">
						         <thead>
						         	<tr>				                     
				                      <th><spring:message code="label.assister.name"/></th>	
				                      <th><spring:message code="label.assister.site"/></th>
				                      <th>&nbsp;</th>
				                    </tr>	
				                    </thead>			                  
				                  	<tbody>
						               	<c:forEach items="${assisterList}" var="assister">
						               	 <c:set var="encAssisterId" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
						                    <tr>
						                        <td><a href="<c:url value="/entity/assister/viewassisterinformation?assisterId=${encAssisterId}"/>">
						                       			<c:out value="${assister.firstName} ${assister.lastName}" escapeXml="false"/>
						                        	</a>
						                        </td>			                    	
						                  		<td><c:out value="${assister.primaryAssisterSite.siteLocationName}" escapeXml="false"/></td>
						                 		<td>
						                 			<c:choose>
														<c:when test="${CA_STATE_CODE}">
															<c:if test="${loggedUser =='entityAdmin' || enrollmentEntity.registrationStatus=='Incomplete'}">
																<div class="dropdown" align="right">						                 		
										                			<a class="dropdown-toggle" data-toggle="dropdown" href="#" ><i class="icon-cog"></i><i class="caret"></i></a>
																	<ul class="dropdown-menu pull-right"><li><a id ="editId" href="<c:url value="/entity/assister/registration?assisterId=${encAssisterId}"/>" class="offset1">
																	<i class="icon-pencil"></i><spring:message code="label.assister.edit"/></a></li></ul>
																</div>
															</c:if>
														</c:when>
														<c:otherwise>
															<c:choose>          
										         	 			<c:when test="${loggedUser!='entityAdmin' && enrollmentEntity.registrationStatus=='Pending'}">                      				
										             			</c:when>
										             			<c:otherwise>
										             				<div class="dropdown" align="right">						                 		
										                 				<a class="dropdown-toggle" data-toggle="dropdown" href="#" ><i class="icon-cog"></i><i class="caret"></i></a>
																		<ul class="dropdown-menu pull-right"><li><a id ="editId" href="<c:url value="/entity/assister/registration?assisterId=${encAssisterId}"/>" class="offset1">
																		<i class="icon-pencil"></i><spring:message code="label.assister.edit"/></a></li></ul>
																	</div>
										             			</c:otherwise>
										             		</c:choose>	
														</c:otherwise>
													</c:choose> 					                 										
												</td>
						                  </tr>
					                  	</c:forEach>
				                 </tbody>
				             </table>
				             </div>
						     </div>
						</div>
	            </c:if>
 	
				<form class="form-horizontal gutter10 entityAddressValidation" style="display:none" id="frmAssister" enctype="multipart/form-data" name="frmAssister" action="registration" method="POST" onsubmit="return submitMyForm($('#frmAssister') , $('#SaveAssister'));"  autocomplete="off">
				<df:csrfToken/>
				
					<input type="hidden" id="assisterId" name="assisterId"  value="<encryptor:enc value="${assister.id}"/>" />
					<input type="hidden" name="assisterLanguageId" name="assisterLanguageId" value="${assisterLanguages.id}"/>
						<div class="header">
							<h4 class="pull-left"><spring:message code="label.assister.newAssisterForm"/></h4>
						</div>
						<div class="control-group">
						</div>
						<div class="control-group">
							<label for="firstName" class="required control-label"><spring:message code="label.assister.Firstname"/><img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
			                  <input type="text" name="firstName" id="firstName" value="${assister.firstName}" class="input-xlarge" size="20" maxlength="50"/>
			                  <div id="firstName_error"></div>
			                </div>
						 </div>
						
						<div class="control-group">
							<label for="lastName" class="required control-label"><spring:message code="label.assister.Lastname"/><img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
			                  <input type="text" name="lastName" id="lastName" value="${assister.lastName}" class="input-xlarge" size="20" maxlength="50"/>
			                  <div id="lastName_error"></div>
			                </div>
						 </div>
						  
						 <div class="control-group">
			                <label class="control-label" for="emailAddress"><spring:message code="label.assister.emailAddress"/><img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
			                <div class="controls">
			                  <input type="text" name="emailAddress" id="emailAddress" value="${assister.emailAddress}" class="input-xlarge" size="30" placeholder="<spring:message code="label.assister.asscompany@email.com"/>"/>
			                	<div id="emailAddress_error"></div>
			                </div>
		             	 </div>
						 
						<div class="control-group phone-group">
							<label for="primaryPhone1" class="required control-label"><spring:message code="label.assister.EnterPrimaryPhoneNumber"/><img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /> <span class="aria-hidden"><spring:message  code="label.firstPhone"/></span></label>
							<label for="primaryPhone2" class="aria-hidden"><spring:message  code="label.assister.EnterPrimaryPhoneNumber"/> <spring:message  code="label.secondPhone"/></label>
							<label for="primaryPhone3" class="aria-hidden"><spring:message  code="label.assister.EnterPrimaryPhoneNumber"/> <spring:message  code="label.thirdPhone"/></label>
							<div class="controls">
								<input type="text" name="primaryPhone1" id="primaryPhone1" value="${primaryPhone1}" maxlength="3" placeholder="xxx" class="area-code input-mini" />
								<input type="text" name="primaryPhone2" id="primaryPhone2" value="${primaryPhone2}" maxlength="3" placeholder="xxx" class="input-mini"/>
								<input type="text" name="primaryPhone3" id="primaryPhone3" value="${primaryPhone3}" maxlength="4" placeholder="xxxx" class="input-small" />
								<input type="hidden" name="primaryPhoneNumber" id="primaryPhoneNumber" value="" />      
								<div id="primaryPhone3_error"></div>
								<div id="primaryPhone1_error"></div>
							</div>
						</div>
						<div class="control-group phone-group">
							<label for="secondaryPhone1" class="control-label"><spring:message code="label.assister.EnterSecondaryPhoneNumber"/> <span class="aria-hidden"><spring:message  code="label.firstPhone"/></span></label>
							<label for="secondaryPhone2" class="aria-hidden"><spring:message  code="label.assister.EnterSecondaryPhoneNumber"/> <spring:message  code="label.secondPhone"/></label>
							<label for="secondaryPhone3" class="aria-hidden"><spring:message  code="label.assister.EnterSecondaryPhoneNumber"/> <spring:message  code="label.thirdPhone"/></label>
							<div class="controls">
								<input type="text" name="secondaryPhone1" id="secondaryPhone1" value="${secondaryPhone1}" maxlength="3" placeholder="xxx" class="area-code input-mini" />
								<input type="text" name="secondaryPhone2" id="secondaryPhone2" value="${secondaryPhone2}" maxlength="3" placeholder="xxx" class="input-mini" />
								<input type="text" name="secondaryPhone3" id="secondaryPhone3" value="${secondaryPhone3}" maxlength="4" placeholder="xxxx" class="input-small" />
								<input type="hidden" name="secondaryPhoneNumber" id="secondaryPhoneNumber" value="" />
								<div id="secondaryPhone1_error"></div>
								<div id="secondaryPhone3_error"></div>
							</div>
						</div>
						
						 <div class="control-group">
						 	<fieldset>
								<legend class="control-label"><spring:message code="label.assister.Howwouldthispersonliketobecontacted"/> <span class="aria-hidden"><spring:message code="label.entity.required"/></span> <img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></legend>
								<div class="controls">
										<label class="radio inline" for="email"> 
											<c:choose>
												<c:when test="${assister.communicationPreference == 'email'}">
													<input type="radio" name="communicationPreference" id="communicationPreferenceEmail" value="email" checked />
												</c:when>
												<c:otherwise>
													<input type="radio" name="communicationPreference" id="communicationPreferenceEmail" value="email" checked/>
												</c:otherwise>
											</c:choose> 
											<spring:message code="label.assister.emailAddress"/>
										</label> <br>
									
										<label class="radio inline" for="communicationPreferenceEmailPhone"> 
											<c:choose>
												<c:when test="${assister.communicationPreference == 'phone'}">
													<input type="radio" name="communicationPreference" id="communicationPreferenceEmailPhone" value="phone" checked />
												</c:when>
												<c:otherwise>
													<input type="radio" name="communicationPreference" id="communicationPreferenceEmailPhone" value="phone" />
												</c:otherwise>
											</c:choose> 
											<spring:message code="label.assister.SelectPrimaryPhone"/>
										</label> <br>
									
										<label class="radio inline" for="communicationPreferenceMail"> 
											<c:choose>
												<c:when test="${assister.communicationPreference == 'mail'}">
													<input type="radio" name="communicationPreference" id="communicationPreferenceMail" value="mail" checked />
												</c:when>
												<c:otherwise>
													<input type="radio" name="communicationPreference" id="communicationPreferenceMail" value="mail" />
												</c:otherwise>
											</c:choose> 
											<spring:message code="label.assister.Mail"/>
										</label>
								<div id="communicationPreference_error"></div>
								</div>
							</fieldset>
						</div>
						
						
						<c:choose>
						 						<c:when test="${showPostalMailOption == 'true'}">
						 								<div class="control-group" >
						 						</c:when>
						 						<c:otherwise>
						 								<div class="control-group" style="visibility: hidden;display: none;">
						 						</c:otherwise>
						 	</c:choose>
							
						 		<fieldset>
						 				<legend for="postalMail" class="control-label"> <spring:message code="label.assister.receiveNotices"/> </legend>  
						 			
						 				<div class="controls">
						 					<c:choose>
						 						<c:when test="${assister.postalMail!=null && assister.postalMail == 'Y'}">
						 								<input type="checkbox" name="postalMail" value="Y" checked="checked" id="postalMail" />
						 						</c:when>
						 						<c:otherwise>
						 								<input type="checkbox" name="postalMail" value="Y"  id="postalMail" />
						 						</c:otherwise>
						 					</c:choose>
						 						
						 						<div id="postalMail_error"></div>
						 						<div> <spring:message code="label.assister.alwaysReceiveEmail"/></div>
						 				</div>
						 		</fieldset>
							</div>
						
						 <div class="control-group">
						 	<fieldset class="idMisMatchWithoutUnderScore">
									<legend for="isAssisterCertified" class="required control-label"><spring:message code="label.assister.isThisAssisterCertified?"/></legend>
									<div class="controls">
										<c:if test="${assister.certificationNumber!=null && assister.certificationNumber != ''}">
											<label><input type="radio" name="isAssisterCertified" value="No" id="selectNo" onclick="$('#certification').hide()"><spring:message code="label.assister.No"/></label>
											<label><input type="radio"	name="isAssisterCertified" value="Yes" id="selectYes" onclick="$('#certification').show()" checked><spring:message code="label.assister.Yes"/></label>
											<!-- <input type="radio" name="isAssisterCertified" disabled="disabled" value="No" id="selectNo" onclick="$('#certification').hide()">No<br>
											<input type="radio"	name="isAssisterCertified" disabled="disabled" value="Yes" id="selectYes" onclick="$('#certification').show()" checked>Yes -->
										</c:if>
										
										<c:if test="${assister.certificationNumber==null || assister.certificationNumber == ''}">
											<!-- <input type="radio" name="isAssisterCertified" value="No" id="selectNo" onclick="$('#certification').hide()" checked>No<br>
											<input type="radio"	name="isAssisterCertified" value="Yes" id="selectYes" onclick="$('#certification').show()">Yes -->
											<label><input type="radio" name="isAssisterCertified" value="No" id="selectNo" onclick="$('#certification').hide()" checked><spring:message code="label.assister.No"/></label>
											<label><input type="radio"	name="isAssisterCertified" value="Yes" id="selectYes" onclick="$('#certification').show()"><spring:message code="label.assister.Yes"/></label>
										</c:if>
									</div>
								</fieldset>
						</div>
						
						<div id="certification" style="display: none" class="control-group">
							<label for="certificationNumber" class="required control-label"><spring:message code="label.assister.AssisterCertification"/># <img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
								<c:choose>
									<c:when test="${assister.certificationNumber != null && assister.certificationNumber != ''}">
										<input type="text" name="certificationNumber" id="certificationNumber" maxlength="10" value="<fmt:formatNumber minIntegerDigits="10" value="${assister.certificationNumber}" groupingUsed="FALSE"/>" class="input-xlarge" size="30" />
									</c:when>
									<c:otherwise>
										<input type="text" name="certificationNumber" id="certificationNumber" maxlength="10" value="<fmt:formatNumber minIntegerDigits="10" value="${assister.certificationNumber}" groupingUsed="FALSE"/>" class="input-xlarge" size="30" />
									</c:otherwise>
								</c:choose> 
					 			<div id="certificationNumber_error"></div>
							</div>
						</div>
						
						<%-- <div class="control-group">
							<label for="businessLegalName" class="required control-label" id="businessLegalName">Business Legal Name<img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
			                  <input type="text" name="businessLegalName" id="businessLegalName" value="${assister.businessLegalName}" class="input-xlarge" size="20" maxlength="50"/>
			                  <div id="businessLegalName_error"></div>
			                </div>
						</div> --%>
						
						<div class="control-group">
								<label for="primarySite" class="required control-label"><spring:message code="label.assister.priAssisterSite"/><img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
								<%-- <label for="state" class="control-label"><spring:message code="label.assister.priAssisterSite"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label> --%>
								<div class="controls">
									<select  id="primarySite" name="primarySite" path="sitelist" class="ie8">
										<option value=""><spring:message code="label.assister.select"/></option>
										<c:forEach var="primary" items="${primarySitelist}">
											<option id="${primary.id}" 
												<c:if test="${primary.id == assister.primaryAssisterSite.id}"> selected="selected"</c:if> value="<c:out value="${primary.id}" />"> ${primary.siteLocationName}
											</option>
										</c:forEach>
									</select> 
	<%-- 								<input type="hidden" id="primaryAssisterSite_id" name="primaryAssisterSite_id" value="${priSite}">
	 --%>								<div id="primarySite_error"></div>
								</div>
						</div>
		
						<div class="control-group">
								<label for="secondarySite" class="control-label"><spring:message code="label.assister.secAssisterSite"/></label>
								<div class="controls">
									<select  id="secondarySite" name="secondarySite" path="sitelist" class="ie8">
										<option value=""><spring:message code="label.assister.select"/></option>
										<c:forEach var="secondary" items="${secondarySitelist}">
											<option id="${secondary.id}"
												<c:if test="${secondary.id == assister.secondaryAssisterSite.id}">selected="selected"</c:if>
													value="<c:out value="${secondary.id}" />">
													${secondary.siteLocationName}
											</option>	
																		
										</c:forEach>									
									</select> 
	<%-- 								<input type="hidden" id="secondaryAssisterSite_id" name="secondaryAssisterSite_id" value="${secSite}">
	 --%>							</div>
						</div>
		
						<div class="header">
							<h4 class="pull-left"><spring:message code="label.assister.mailingAddress"/></h4>
						</div>
						
						<div class="control-group">
						</div>
						<div class="control-group">
							<label for="address1" class="control-label required"><spring:message code="label.assister.streetAddress"/><img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">							
								<input type="text" placeholder="<spring:message code="label.address1placeholder"/>" name="mailingLocation.address1" id="address1" value="${assister.mailingLocation.address1}" class="input-xlarge" size="30" maxlength="100"  autocomplete="off"/>
								<input type="hidden" id="address1_hidden" name="address1_hidden" value="${assister.mailingLocation.address1}">
								<div id="address1_error"></div>
							</div>	
						</div>	
						
						<div class="control-group">
							<label for="address2" class="control-label"><spring:message code="label.assister.suite"/></label>
							<div class="controls">
								<input type="text" placeholder="<spring:message code="label.address2placeholder"/>" name="mailingLocation.address2" id="address2" value="${assister.mailingLocation.address2}" class="input-xlarge" size="30" maxlength="100" autocomplete="off"/>
								<input type="hidden" id="address2_hidden" name="address2_hidden" value="${assister.mailingLocation.address2}">
							</div>
						</div>
						 
						 <div class="control-group">
								<label for="city" class="control-label"><spring:message code="label.assister.city"/><img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
								<div class="controls">
									<c:choose>
										<c:when test="${CA_STATE_CODE}">
											<input type="text" placeholder="<spring:message code="label.cityplaceholder"/>" name="mailingLocation.city" id="city" value="${assister.mailingLocation.city}" class="input-xlarge" size="30" maxlength="30"  autocomplete="off"/>
										</c:when>
										<c:otherwise>
									<input type="text" placeholder="<spring:message code="label.cityplaceholder"/>" name="mailingLocation.city" id="city" value="${assister.mailingLocation.city}" class="input-xlarge" size="30" maxlength="100"  autocomplete="off"/>
										</c:otherwise>
									</c:choose>
									<input type="hidden" id="city_hidden" name="city_hidden" value="${assister.mailingLocation.city}">
								<div id="city_error"></div>
								</div>	
						</div>	
						 
						<div class="control-group">
							<label for="state" class="control-label"><spring:message code="label.assister.state"/><img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
								<select  id="state" name="mailingLocation.state" path="statelist" class="input-medium"  autocomplete="off">
									<option value=""><spring:message code="label.assister.select"/></option>
									<c:forEach var="state" items="${statelist}">
										<option id="${state.code}" <c:if test="${state.code == assister.mailingLocation.state}"> selected="selected"</c:if>  value="<c:out value="${state.code}" />">
											<c:out value="${state.name}" />
										</option>
									</c:forEach>
								</select>
								<input type="hidden" id="state_hidden" name="state_hidden" value="${assister.mailingLocation.state}">
								<div id="state_error"></div>
							</div>	
						</div>	
						 <div class="control-group">
							<label for="zip" class="control-label"><spring:message code="label.assister.zipCode"/><img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
								<input type="text" placeholder="" name="mailingLocation.zip" id="zip" value="${assister.mailingLocation.zip}" class="input-mini entityZipCode" maxlength="5"  autocomplete="off" />
								<input type="hidden" id="zip_hidden" name="zip_hidden" value="${assister.mailingLocation.zip}">	
								<div id="zip_error"></div>
							</div>
						</div>	
					
						<div class="header">
							<h4 class="pull-left"><spring:message code="label.assister.profileInformation"/></h4>
						</div>
						<div class="control-group margin20-t">
							<fieldset>
								<legend class="control-label"><spring:message code="label.assister.spokenLanguagesSupported"/> <span class="aria-hidden"><spring:message code="label.entity.required"/></span> <img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></legend>
								<div class="controls">
									<c:forEach var="language" items="${languageSpokenList}">
										<label class="aria-hidden" for="spoken_${language.lookupValueLabel}">${language.lookupValueLabel}</label>
					                      <input type="checkbox" name="spoken" id="spokenLanguagesId"  value="${language.lookupValueLabel}" ${fn:contains(assisterLanguages.spokenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' }>
					                      <c:out value="${language.lookupValueLabel}" /><br>
				                  	</c:forEach> 
				                  	<div class="selectPlugin">
					                    <input type="checkbox" name="otherSpokenLanguageCheckbox" id="otherSpokenLanguageCheckbox"  value="${otherSpokenLanguage}" ${otherSpokenLanguage!=null ? 'checked="checked"' : '' }> <spring:message code="label.entity.other"/>&nbsp;&nbsp; 
				                      	<select data-placeholder="<spring:message code="label.selectsomeoptions"/>" id="otherSpokenLanguage" name="otherSpokenLanguage"  class="chosen-select" multiple style="width:350px;" ></select>
				                      	<input type="hidden" id="spokenLanguages" name="spokenLanguages" value="" />
				                      	
				                      	
				                      	<div id="spokenLanguagesId_error"></div> 
				                      	<div id="otherSpokenLanguage_error"></div>
				                      	<div id="otherSpokenLanguageCheckbox_error"></div>
			                      	</div>
								</div>	
							</fieldset>
						</div>	
						
						<div class="control-group">
							<fieldset>
								<legend class="control-label"><spring:message code="label.assister.writtenLanguagesSupported"/> <span class="aria-hidden"><spring:message code="label.entity.required"/></span> <img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></legend>
								<div class="controls">
										<c:forEach var="language" items="${languageWrittenList}">
											<label class="aria-hidden" for="written_${language.lookupValueLabel}">${language.lookupValueLabel}</label>
						                      <input type="checkbox" id="writtenLanguagesId" name="written" value="${language.lookupValueLabel}" ${fn:contains(assisterLanguages.writtenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' }>
				                     		  <c:out value="${language.lookupValueLabel}" /><br>
					                  	</c:forEach> 
					                   <div class="selectPlugin">
						                    <input type="checkbox" name="otherWrittenLanguageCheckbox" id="otherWrittenLanguageCheckbox"  value="${otherWrittenLanguage}" ${otherWrittenLanguage!=null ? 'checked="checked"' : '' }> <spring:message code="label.entity.other"/>&nbsp;&nbsp; 
					                      	<select data-placeholder="<spring:message code="label.selectsomeoptions"/>" id="otherWrittenLanguage" name="otherWrittenLanguage"  class="chosen-select" multiple style="width:350px;" ></select>
											<input type="hidden" id="writtenLanguages" name="writtenLanguages" value="" />
											
											<div id="writtenLanguagesId_error"></div> 
											<div id="otherWrittenLanguage_error"></div>
											<div id="otherWrittenLanguageCheckbox_error"></div>
										</div>
								</div>
							</fieldset>	
						</div>	
		              
						<div class="control-group">
							<label for="education" class="control-label"><spring:message code="label.assister.education"/><img src="<gi:cdnurl value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
								<select  id="education" name="education" path="educationlist" class="input-large">
									<option value=""><spring:message code="label.assister.select"/></option>
									<c:forEach var="educationVar" items="${educationlist}">
										<option id="${educationVar}" <c:if test="${educationVar == assister.education}"> selected="selected"</c:if>  value="<c:out value="${educationVar}" />">
											<c:out value="${educationVar}" />
										</option>
									</c:forEach>
								</select>
								<input type="hidden" id="education_hidden" name="education_hidden" value="${assister.education}">
								<div id="education_error"></div>
							</div>	
						</div>	
						
						<div class="control-group">
								<label class="control-label" for="fileInput"><spring:message code="label.assister.uploadPhoto"/></label>
								<div class="controls paddingT5">
									<input type="file" class="input-file" id="fileInput" name="fileInput">&nbsp;
									<input type="hidden" id="fileInput_Size" name="fileInput_Size" value="1">
								<div>
					                <spring:message code="label.uploadDocumentCaption"/>
					            </div>
					                <div id="fileInput_error"></div>
								</div>									
								
						</div>
						 <input type="hidden" name="languagesSpokenCheck" id="languagesSpokenCheck" value="0" />
		 				 <input type="hidden" name="languagesWrittenCheck" id="languagesWrittenCheck" value="0" />
						<input type="hidden" name="mailingLocation.lat" id="lat" value="${assister.mailingLocation.lat != null ? assister.mailingLocation.lat : 0.0}" />
						<input type="hidden" name="mailingLocation.lon" id="lon" value="${assister.mailingLocation.lon != null ? assister.mailingLocation.lon : 0.0}" />
						<input type="hidden" name="mailingLocation.rdi" id="rdi" value="${assister.mailingLocation.rdi != null ? assister.mailingLocation.rdi : ''}" />
						<input type="hidden" name="mailingLocation.county" id="county" value="${assister.mailingLocation.county}" />	
						<div class="form-actions">
							<input type="button" name="SaveAssister" id="SaveAssister" onClick="javascript:validateForm();" value="<spring:message code="label.assister.SaveAssister"/>" class="btn btn-primary" />							
						</div>
				</form>
				
				</div>
				</div>
			</div>	
		</div>
	</div>
	<input id="tokid" type="hidden" value="${sessionScope.csrftoken}" />
	<c:url value="/assister/checkEmail" var="theUrltocheckEmail">  
		 
	</c:url>

<script type="text/javascript">
function ie8Trim(){
	if(typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function() {
        	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
        };
	}
}	
$(document).ready(function() {
	$("#assisters").removeClass("link");
	$("#assisters").addClass("active");

	var isShow ='${isShow}';
	if(isShow=="true") {
		$("#frmAssister").show();
	}  else {	
		$("#frmAssister").hide();
	}
	
	var isCertified ='${isCertified}';
	if(isCertified=="true") {
		$("#certification").show();
	}  else {	
		$("#certification").hide();
	}
	
});


$(function() {
	$("#SaveAssister").click(function(){
		ie8Trim();
		var spokenVal ="";
		var writtenVal="";
		$.each($("input[name='spoken']:checked"), function() {
			var spokenValcurrent = $(this).attr('value');
			if(spokenVal == ""){ 
				spokenVal=spokenValcurrent;
			} else {
				spokenVal=spokenVal+","+spokenValcurrent;
			}
		});
		$.each($("input[name='written']:checked"), function() {
			var  writtenValcurrent = $(this).attr('value');
			if(writtenVal == ""){ 
				writtenVal=writtenValcurrent;
			} else {
				writtenVal=writtenVal+","+writtenValcurrent;
			}
		});
		otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
		if(otherSpokenLanguage != "" &&  otherSpokenLanguage !=null){ 
			if(spokenVal != ""){ 
				spokenVal = spokenVal+","+otherSpokenLanguage;
			}
			else{ 
				spokenVal =otherSpokenLanguage;
			}
		}
		
		otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
		if(otherWrittenLanguage != "" &&  otherWrittenLanguage !=null){ 
			if(writtenVal != ""){ 
				writtenVal = writtenVal+","+otherWrittenLanguage;
			}
			else{ 
				writtenVal =otherWrittenLanguage;
			}
		}
		
		 $("#spokenLanguages").val(spokenVal);
		 $("#writtenLanguages").val(writtenVal);
		 //adding code for subsite id selected by enrollment entity representative during add assister operation 
		 var selectedSiteId=$('#secondarySite').val();
		 $("#secondaryAssisterSite_id").val(selectedSiteId);
	});
});

jQuery.validator.addMethod("OtherSpokenLanguageCheck", function(value, element, param) {
	ie8Trim();
	otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
	if(otherSpokenLanguage != ""){ 
		var languages='${languageNames}';
		languages=languages.replace("[","");
		languages=languages.replace("]","");
		languages=languages.replace(" ","");
		var languagesArray=languages.split(',');
		
		var found = false;
		for (i = 0; i < languagesArray.length && !found; i++) {
			languagesTocompare=languagesArray[i].trim();
		if (languagesTocompare.toLowerCase().match(otherSpokenLanguage.toLowerCase())) {			
			  found = true;
		  }
		}
		if(found){
			return false;
		}
		else
			{
				return true;
			}
	}
	else
	{
		if($("#otherSpokenLanguageCheckbox").attr("checked")=='checked' && otherSpokenLanguage =="")
		{					
				return false;
		}
		else
			{
				return true;
			}
	}	
});
jQuery.validator.addMethod("OtherWrittenLanguageCheck", function(value, element, param) {
	ie8Trim();
	otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
	if(otherWrittenLanguage != ""){ 
		var languages='${languageWrittenNames}';
		languages=languages.replace("[","");
		languages=languages.replace("]","");
		languages=languages.replace(" ","");
		var languagesArray=languages.split(',');
		
		var found = false;
		for (i = 0; i < languagesArray.length && !found; i++) {
		languagesTocompare=languagesArray[i].trim();
		 if (languagesTocompare.toLowerCase().match(otherWrittenLanguage.toLowerCase())) {	
			  found = true;
		  }
		}  
		if(found){
			return false;
		}
		else
			{
				return true;
			}
	
	}
	else
	{	
		if($("#otherWrittenLanguageCheckbox").attr("checked")=='checked' && otherWrittenLanguage =="")
		{
			
			return false;
		}
		else
			{
				return true;
			}
	}
});

jQuery.validator.addMethod("SpokenLanguageCheck", function(value, element, param) {
	ie8Trim();
	var fields = $("input[name='spoken']").serializeArray(); 
    if (fields.length == 0) 
    { 
        otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
    	if(otherSpokenLanguage == null || otherSpokenLanguage == ""){ 
    		return false;
    	}
    } 
	return true;
});

jQuery.validator.addMethod("otherSpokenLanguageCheckboxCheck", function(value, element, param) {
	ie8Trim();
	
	otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
	if (otherSpokenLanguage != null ) 
    { 
	    if((otherSpokenLanguage != null || otherSpokenLanguage != "") && !($("#otherSpokenLanguageCheckbox").attr("checked")=='checked' )){
	    		return false;
	    	}
    } 
	return true;
});

jQuery.validator.addMethod("otherSpokenLanguageSelectCheck", function(value, element, param) {
	ie8Trim();
	
	temp = $("#otherSpokenLanguageCheckbox").attr("checked")=='checked';
	if (temp != false ) 
    { 
	    if((otherSpokenLanguage == null || otherSpokenLanguage == "") && ($("#otherSpokenLanguageCheckbox").attr("checked")=='checked' )){
	    		return false;
	    	}
    } 
	return true;
}); 

jQuery.validator.addMethod("WrittenLanguageCheck", function(value, element, param) {
	ie8Trim();
	var fields = $("input[name='written']").serializeArray(); 
    if (fields.length == 0) 
    { 
        otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
    	if(otherWrittenLanguage  == null || otherWrittenLanguage == ""){ 
    		return false;
    	}
    } 
	return true;
});

jQuery.validator.addMethod("otherWrittenLanguageCheckboxCheck", function(value, element, param) {
	ie8Trim();
	otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
	if (otherWrittenLanguage != null) 
    { 
    	if((otherWrittenLanguage != null || otherWrittenLanguage != "") && !($("#otherWrittenLanguageCheckbox").attr("checked")=='checked' )){
    			return false;
    	}
    } 
	return true;
});

jQuery.validator.addMethod("otherWrittenLanguageSelectCheck", function(value, element, param) {
	ie8Trim();
	
	temp = $("#otherWrittenLanguageCheckbox").attr("checked")=='checked';
	if (temp != false ) 
    { 
	    if((otherWrittenLanguage == null || otherWrittenLanguage == "") && ($("#otherWrittenLanguageCheckbox").attr("checked")=='checked' )){
	    		return false;
	    	}
    } 
	return true;
}); 

jQuery.validator.addMethod("PhotoUploadCheck", function(value, element, param) {
	ie8Trim();
      var file = $('input[type="file"]').val();
      var exts = ['jpg','jpeg','gif','png','bmp'];
      if ( file ) {
        var get_ext = file.split('.');
        get_ext = get_ext.reverse();

        if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
          return true;
        } else {
          return false;
        }
     }
     return true;
});


var validator = $("#frmAssister").validate({ 
	onkeyup: false,
	onclick: false,
	rules : {
		firstName : {required : true},
		lastName : {required : true},			
		emailAddress : { required : true,email: true},
		primaryPhone1 :{numberStartsWithZeroCheck : true},
		secondaryPhone1 :{numberStartsWithZeroCheck : true},
		primaryPhone3 : {primaryphonecheck : true},	
		secondaryPhone3 : {secondaryphonecheck : true},
		communicationPreference : { required : true},
		"mailingLocation.address1" : { required: true},
		"mailingLocation.city" : { required: true},
		"mailingLocation.state" : { required: true},
		"mailingLocation.zip" : {required: true, MailingZipCodecheck: true, digits: true},
		primarySite : {required: true},
		certificationNumber : { CertificationNoCheck: true},
		spoken : {SpokenLanguageCheck: true},
		written : {WrittenLanguageCheck: true},
		otherSpokenLanguage : {OtherSpokenLanguageCheck: true, languagesSpokenCheck: true},
		otherWrittenLanguage : {OtherWrittenLanguageCheck: true ,languagesWrittenCheck : true},
		education : { required: true},
		otherSpokenLanguageCheckbox : {otherSpokenLanguageCheckboxCheck : true, otherSpokenLanguageSelectCheck : true},
		otherWrittenLanguageCheckbox : {otherWrittenLanguageCheckboxCheck : true, otherWrittenLanguageSelectCheck : true},
		fileInput : {PhotoUploadCheck: true, sizeCheck: true} 
		},
	messages : {
		firstName: { required : "<span> <em class='excl'>!</em><spring:message code='label.entityValidateFirstName' javaScriptEscape='true'/></span>"},
         lastName: { required : "<span> <em class='excl'>!</em><spring:message code='label.entityValidateLastName' javaScriptEscape='true'/></span>"},
		emailAddress: { required : "<span> <em class='excl'>!</em><spring:message code='label.entityValidatePleaseEnterValidEmail' javaScriptEscape='true'/></span>",
	    email : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidEmail'/></span>"},
	    primaryPhone1 : {numberStartsWithZeroCheck :  "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>" },
	    secondaryPhone1 : {numberStartsWithZeroCheck :  "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>" },
	    primaryPhone3: { primaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNo' javaScriptEscape='true'/></span>"},
	    secondaryPhone3: { secondaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSecondaryPhoneNo' javaScriptEscape='true'/></span>" },
		communicationPreference: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCommunicationPreference' javaScriptEscape='true'/></span>"},
		"mailingLocation.address1" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterMailingAddress' javaScriptEscape='true'/></span>"},
		"mailingLocation.city" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterCity' javaScriptEscape='true'/></span>"},
		"mailingLocation.state" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseSelectState' javaScriptEscape='true'/></span>"},
		"mailingLocation.zip" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterZipCode' javaScriptEscape='true'/></span>",
		MailingZipCodecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidZipCode' javaScriptEscape='true'/></span>"},
		primarySite : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryAssisterSite' javaScriptEscape='true'/></span>"},
		certificationNumber : {CertificationNoCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateCertificationNo' javaScriptEscape='true'/></span>"},
 		spoken : {SpokenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSpokenLanguage' javaScriptEscape='true'/></span>"},
 		written : {WrittenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateWrittenLanguage' javaScriptEscape='true'/></span>"}, 
 		otherSpokenLanguage : {OtherSpokenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateOtherSpokenLanguage' javaScriptEscape='true'/></span>",
 			 languagesSpokenCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterLanguageSpokenFromDropDown' javaScriptEscape='true'/></span>"},
 		otherWrittenLanguage : {OtherWrittenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateOtherWrittenLanguage' javaScriptEscape='true'/></span>",
 			languagesWrittenCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterLanguageWrittenFromDropDown' javaScriptEscape='true'/></span>"},
 		education : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterEducation' javaScriptEscape='true'/></span>"},
 		fileInput : {PhotoUploadCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePhoto' javaScriptEscape='true'/></span>",
 					sizeCheck : "<span> <em class='excl'>!</em><spring:message  code='label.brkvalidatePleaseSelectFileWithSizeLessThan5MB' javaScriptEscape='true'/></span>"},
 		otherSpokenLanguageCheckbox : {otherSpokenLanguageCheckboxCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectothercheckbox'/></span>",
			otherSpokenLanguageSelectCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectlangforother'/></span>"},
		otherWrittenLanguageCheckbox : {otherWrittenLanguageCheckboxCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectothercheckboxforwritten'/></span>",
			otherWrittenLanguageSelectCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectlangforotherwritten'/></span>"}
 	}
	,
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});

jQuery.validator.addMethod("alphaNumeric", function(value, element, param) {
	ie8Trim();
	 return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
	 
});

jQuery.validator.addMethod("primaryphonecheck", function(value, element, param) {
	ie8Trim();
	primphone1 = $("#primaryPhone1").val().trim(); 
	primphone2 = $("#primaryPhone2").val().trim(); 
	primphone3 = $("#primaryPhone3").val().trim(); 
 
    var a=(/^[0-9]*$/.test(primphone1)&& /^[0-9]*$/.test(primphone2)&&/^[0-9]*$/.test(primphone3));
	
	if(!a){
		return false;
	}
	if( (primphone1 == "" || primphone2 == "" || primphone3 == "")  || (isNaN(primphone1)) || (primphone1.length < 3 ) || (isNaN(primphone2)) || (primphone2.length < 3 ) || (isNaN(primphone3)) || (primphone3.length < 4 )  )
	{
		return false;
	}
	else
	{
		$("#primaryPhoneNumber").val(primphone1 + primphone2 + primphone3);
		return true;
	}	
} );


jQuery.validator.addMethod("secondaryphonecheck", function(value, element, param) {
	ie8Trim();
	secondaryphone1 = $("#secondaryPhone1").val().trim(); 
	secondaryphone2 = $("#secondaryPhone2").val().trim(); 
	secondaryphone3 = $("#secondaryPhone3").val().trim(); 
	var a=(/^[0-9]*$/.test(secondaryphone1)&& /^[0-9]*$/.test(secondaryphone2)&&/^[0-9]*$/.test(secondaryphone3));

	if(secondaryphone1 == "" && secondaryphone2 == "" && secondaryphone3 == ""){
		return true;
	} 
	if(!a){
		return false;
	}
	else if(isNaN(secondaryphone1) || secondaryphone1.length < 3  || isNaN(secondaryphone2) || secondaryphone2.length < 3  || isNaN(secondaryphone3) || secondaryphone3.length < 4 ){
		return false;
	} else {
		$("#secondaryPhoneNumber").val(secondaryphone1 + secondaryphone2 + secondaryphone3);
		return true;
	}
} );

// function shiftbox(element,nextelement){
// 	ie8Trim();
// 	maxlength = parseInt(element.getAttribute('maxlength'));
// 	if(element.value.length == maxlength){
// 		nextelement = document.getElementById(nextelement);
// 		nextelement.focus();
// 	}
// }

jQuery.validator.addMethod("MailingZipCodecheck", function(value, element, param) {
	ie8Trim();
	zip = $("#zip").val().trim(); 
	if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || (zip == "00000"))){ 
		return false; 
	}
	return true;
});

jQuery.validator.addMethod("CertificationNoCheck", function(value, element, param) {
	ie8Trim();
	var isChecked = jQuery("input[name=isAssisterCertified]:checked").val();
	
	if(isChecked == "Yes" ){
		
		certificationNo = $("#certificationNumber").val().trim();
		if((certificationNo == "")  || !isPositiveInteger(certificationNo) || (isNaN(certificationNo)) || (value.length < 10 ) || (value == "0000000000"))
		{ 
			return false; 
		}
		return true;
	}
});

jQuery.validator.addMethod("numberStartsWithZeroCheck", function(value, element, param) {
	ie8Trim();
	if((value.length == 0)) {
		return true;
	}

	var firstChar = value.charAt(0);
	if(firstChar == 0) {
			return false;
	} else{
	    return true;
	}
	
});

var secret = $('.secret');
secret.hide();
$('input:radio[name="assisterCertified"]').change(function(){
    if ($(this).is(':checked') && $(this).val() === 'certifiedYes') {
       secret.fadeToggle();
    }
    else{
      secret.hide();
      }
  });
  


$('#address1').focusin(function() {
	
			if(($('#address2').val())==="Address Line 2"){
				$('#address2').val('');
			}
		
		
	
});

function split(val) {
    return val.split(/,\s*/);
}
function extractLast(term) {
    return split(term).pop();
}

 $(document).ready(function() {
	 $("#locationAndHours").removeClass("link");
		$("#locationAndHours").removeClass("active2");
   	     
    $( ".otherLanguages").autocomplete({
        source: function (request, response) {
            $.getJSON("${pageContext. request. contextPath}/getOtherLanaguageList", {
                term: extractLast(request.term)
            }, response);
        },
        search: function () {
            // custom minLength
            var term = extractLast(this.value);
            if (term.length < 1) {
                return false;
            }
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
 });
 
 $('#otherWrittenLanguage').focusout(function() {
     county=$("#otherWrittenLanguage").val().trim();
     var test= (county).substring((county).length-1,(county).length);
	 if(test==","){
		 var index = (county).substring(0,(county).length-1);
         $('#otherWrittenLanguage').val(index);
	 }
	 else{
		  }
     
  });

 $('#otherSpokenLanguage').focusout(function() {
     county=$("#otherSpokenLanguage").val().trim();
     var test= (county).substring((county).length-1,(county).length);
	 if(test==","){
		 var index = (county).substring(0,(county).length-1);
         $('#otherSpokenLanguage').val(index);
	 }
	 else{
		  }
  });
 
 $('#otherWrittenLanguage').focusout(function() {
     county=$("#otherWrittenLanguage").val().trim();
     var test= (county).substring((county).length-1,(county).length);
	 if(test==","){
		 var index = (county).substring(0,(county).length-1);
         $('#otherWrittenLanguage').val(index);
	 }
	 else{
		  }
     
  });

 $('#otherSpokenLanguage').focusout(function() {
     county=$("#otherSpokenLanguage").val().trim();
     var test= (county).substring((county).length-1,(county).length);
	 if(test==","){
		 var index = (county).substring(0,(county).length-1);
         $('#otherSpokenLanguage').val(index);
	 }
	 else{
		  }
  });
 
 $(function(){
	 $('#fileInput').change(function(){
			var rv = -1; // Return value assumes failure.
			 if (navigator.appName == 'Microsoft Internet Explorer')
			 {
			    var ua = navigator.userAgent;
			    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			    if (re.exec(ua) != null)
			       rv = parseFloat( RegExp.$1 );
			 }
			 if(!(rv <= 9.0 && navigator.appName == 'Microsoft Internet Explorer')){
			 var f=this.files[0];
			if((f.size > 5242880)||(f.fileSize > 5242880)){
				$("#fileInput_Size").val(0);
			}
			else{
				$("#fileInput_Size").val(1);	
			} 
			}
			else{
				$("#fileInput_Size").val(1);	
			} 

		});
	});
 
 jQuery.validator.addMethod("sizeCheck", function(value, element, param) { 
	  ie8Trim();
	  if($("#fileInput_Size").val()==1){
		  return true;
	  }
	  else{
		  document.getElementById('fileInput').value=null;
		  $("#fileInput_Size").val(1);
		  return false;
	  }
});
 
 
 jQuery.validator.addMethod("languagesSpokenCheck", function(value, element, param) {
		if(value == '') {
			return true;
		}
		checkLanguageSpokenForSite();
		return $("#languagesSpokenCheck").val() == 1 ? false : true;
		});
		
	function checkLanguageSpokenForSite(){
		
		$.get('registration/checkLanguagesSpokenForAsister',
		{otherSpokenLanguage: $("#otherSpokenLanguage").val()},
		            function(response){
		                    if(response == true){
		                            $("#languagesSpokenCheck").val(0);
									
		                    }else{
		                            $("#languagesSpokenCheck").val(1);
									
		                    }
		            }
		        );
		}
	
	jQuery.validator.addMethod("languagesWrittenCheck", function(value, element, param) {
		if(value == '') {
			return true;
		}
		checkLanguageWrittenForSite();
		return $("#languagesWrittenCheck").val() == 1 ? false : true;
		});
		
	function checkLanguageWrittenForSite(){
		
		$.get('registration/checkLanguagesWrittenForAssister',
		{otherWrittenLanguage: $("#otherWrittenLanguage").val()},
		            function(response){
		                    if(response == true){
		                            $("#languagesWrittenCheck").val(0);
									
		                    }else{
		                            $("#languagesWrittenCheck").val(1);
									
		                    }
		            }
		        );
		}

	function submitMyForm(frm , control) {
		
		if ( frm.valid() == true ) { 
			return disableControl(control);
		} else { 
			return false;
		}
	}

	function disableControl(control) {

		if ( control != null ) {
			
			control.attr('disabled','disabled');
		}
		
		return true;
	}
	
	function isPositiveInteger(s)
	{
	    return /^\d+$/.test(s);
	}
	var theUrltocheckEmail = '<c:out value="${theUrltocheckEmail}"/>';
	function validateForm() {
		if($("#frmAssister").validate().form() )
			checkExistingEmail();
		else return false;
	}
	
	function isInvalidCSRFToken(xhr) {
	    var rv = false;
	    if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {                   
	    	alert($('Session is invalid').text());
	           rv = true;
	    }
	    return rv;
	}
	
	function checkExistingEmail() {
		var validateUrl = theUrltocheckEmail;
		$("#SaveAssister").attr("disabled", true);
		$.ajax({
			url : validateUrl,
			type : "POST",
			data : {
				newEmail : $("#emailAddress").val(),
				oldEmail : '${assister.emailAddress}',
				csrftoken : $('#tokid').val() ,
			},
			success : function(response) {
				if(isInvalidCSRFToken(response)){
					$("#SaveAssister").attr("disabled", false);
					return; 
				}                                  
	                
				
				if (response) {
					error = "<label class='error' generated='true'><span> <em class='excl'>!</em><spring:message code='label.validateEmailCheckID' javaScriptEscape='true'/></span></label>";
					$('#emailAddress_error').html(error);
					$("#SaveAssister").attr("disabled", false);
					return false;
				} else {
					$("#frmAssister").submit();
				}
			},

		}); 
	}
	
</script>	
<script type="text/javascript">
 //load the jquery chosen plugin 
	function loadLanguages() {
		$("#otherSpokenLanguage").html('');
		var respData = $.parseJSON('${languagesList}');
		var counties='${otherSpokenLanguage}';
		for ( var key in respData) {
	    	var isSelected = false;
		     if(counties!=null){
			      isSelected = checkLanguages(respData[key]);
		      }
		      if(isSelected){
		    	  $('#otherSpokenLanguage').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#otherSpokenLanguage').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }
	     
	     $('#otherSpokenLanguage').trigger("liszt:updated");
	   }

	function checkLanguages(county){
	     var counties='${otherSpokenLanguage}';
	     var countiesArray=counties.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i];
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;
			    	
			     }
			 }
		   return found;
    }
	
	$(document).ready(function() {
		loadLanguages();
	});
	

</script>
<script type="text/javascript">
 //load the jquery chosen plugin 
	function loadLanguagesForWritten() {
		$("#otherWrittenLanguage").html('');
		var respData = $.parseJSON('${languagesList}');
		var counties='${otherWrittenLanguage}';
	    for ( var key in respData) {
	    	var isSelected = false;
		     if(counties!=null){
			      isSelected = checkLanguage(respData[key]);
		      }
		      if(isSelected){
		    	  $('#otherWrittenLanguage').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#otherWrittenLanguage').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }
	    $('#otherWrittenLanguage').trigger("liszt:updated");
	   
	   }

	function checkLanguage(county){
	     var counties='${otherWrittenLanguage}';
	     var countiesArray=counties.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i];
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;
			    	
			     }
			 }
		   return found;
    }
	
	$(document).ready(function() {
		loadLanguagesForWritten();
		$("#selectNo").click(function() {
			   var textBox=document.getElementById('certificationNumber');
			   textBox.value ='';
			 });
	});
	
</script>