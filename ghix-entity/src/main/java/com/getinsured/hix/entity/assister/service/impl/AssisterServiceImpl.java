package com.getinsured.hix.entity.assister.service.impl;

import static com.getinsured.hix.entity.assister.history.AssisterActivityStatusHistoryRendererImpl.COMMENTS;
import static com.getinsured.hix.entity.assister.history.AssisterActivityStatusHistoryRendererImpl.COMMENT_COL;
import static com.getinsured.hix.entity.assister.history.AssisterActivityStatusHistoryRendererImpl.MODEL_NAME;
import static com.getinsured.hix.entity.assister.history.AssisterActivityStatusHistoryRendererImpl.NEW_STATUS;
import static com.getinsured.hix.entity.assister.history.AssisterActivityStatusHistoryRendererImpl.PREVIOUS_STATUS;
import static com.getinsured.hix.entity.assister.history.AssisterActivityStatusHistoryRendererImpl.REPOSITORY_NAME;
import static com.getinsured.hix.entity.assister.history.AssisterActivityStatusHistoryRendererImpl.STATUS_COL;
import static com.getinsured.hix.entity.assister.history.AssisterActivityStatusHistoryRendererImpl.UPDATED_DATE_COL;
import static com.getinsured.hix.entity.assister.history.AssisterActivityStatusHistoryRendererImpl.UPDATE_DATE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.entity.AssisterExportDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.entity.admin.service.AssisterIndTriggerService;
import com.getinsured.hix.entity.assister.history.AssisterActivityStatusHistoryRendererImpl;
import com.getinsured.hix.entity.assister.repository.IAssisterDesignateRepository;
import com.getinsured.hix.entity.assister.repository.IAssisterNumberRepository;
import com.getinsured.hix.entity.assister.repository.IAssisterRepository;
import com.getinsured.hix.entity.assister.repository.IExternalIndividualForAssisterRepository;
import com.getinsured.hix.entity.assister.service.AssisterService;
import com.getinsured.hix.entity.enrollmententity.service.EnrollmentEntityService;
import com.getinsured.hix.entity.util.EntityConstants;
import com.getinsured.hix.entity.util.EntityUtils;
import com.getinsured.hix.model.ExternalIndividual;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.AssisterNumber;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityLocation;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.platform.audit.service.DisplayAuditService;

/**
 * Implements {@link AssisterService} to perform Assister related operations
 * like find,save,update etc.
 */
@Service("assisterService")
@Transactional
public class AssisterServiceImpl implements AssisterService , ApplicationContextAware{

	private static final int SIX = 6;
	private static final int FIVE = 5;
	private static final int FOUR = 4;
	private static final int THREE = 3;
	private static final int THIRTEEN = 13;
	private static final int ELEVEN = 11;
	private static final String WHERE = "WHERE";
	private static final String FROM = "FROM";
	private static final Logger LOGGER = LoggerFactory.getLogger(AssisterServiceImpl.class);
	private static final int SEVEN = 7;
	private static final int EIGHT = 8;
	private static final int NINE = 9;
	private static final int TEN = 10;
	private static final String A_STATUS = "A.STATUS";
	 		
    private static final String ENTITY_NAME = "entityName";

    private static final String CERTIFICATION_STATUS = "certificationStatus";

    private static final String ASSISTER_LAST_NAME = "assisterLastName";

    private static final String ASSISTER_FIRST_NAME = "assisterFirstName"; 
    private static final String IN_ACTIVE = "InActive";
    private static final String ACTIVE = "Active";
	
	@Autowired
	private IAssisterRepository assisterRepository;

	@Autowired
	private IAssisterNumberRepository assisterNumberRepository;

	@Autowired
	private IExternalIndividualForAssisterRepository externalIndividualForAssisterRepository;

	@Autowired
	private AssisterActivityStatusHistoryRendererImpl activityHistoryRendererService;

	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;

	@Autowired
	private EnrollmentEntityService enrollmentEntityService;

	@Autowired
	private ObjectFactory<DisplayAuditService> objectFactory;

	@Autowired
	private IAssisterDesignateRepository designateAssister;
	
	@Autowired
	private AssisterIndTriggerService assisterIndTriggerService;
	
	@Autowired
	private EntityUtils entityUtils;
	private ApplicationContext context;

	private static final String DATE_STRING = "Date";
	private static final String NEW_ACTIVITY_STATUS = "New Status";
	private static final String VIEW_COMMENT = "View Comment";
	private static final String FIRST_NAME = "firstName";
	private static final String LAST_NAME = "lastName";
	private static final String ELIGIBILITY_STATUS = "status";
	private static final String DESIG_STATUS = "desigStatus";
	private static final String ASSISTER_ID = "assisterId";
	private static final String FROM_DATE = "fromDate";
	private static final String TO_DATE = "toDate";
	private static final String START_LINE = "startLine";
	private static final String END_RECORD= "endRecord";
	private static final String LIKE = "LIKE";
	private static final String ENROLLMENT_ENTITY_ID = "enrollmentEntityId";
	/**
	 * @see AssisterService#findById(int)
	 * 
	 * @param assisterId
	 * 
	 * @return {@link Assister}
	 */
	@Override
	@Transactional(readOnly = true)
	public Assister findById(int assisterId) {

		LOGGER.info("findById: START");
		LOGGER.info("findById: END");

		return assisterRepository.findById(assisterId);
	}

	/**
	 * @see AssisterService#update(Assister)
	 * 
	 * @param assister
	 *            {@link Assister}
	 * @param isAHBXInfo
	 * 
	 * @return {@link Assister}
	 */
	@Override
	@Transactional
	public Assister update(Assister assister, boolean isAHBXInfo) {

		LOGGER.info("update: START");

		LOGGER.info("Assister update method invoked");

		Assister existingAssister = assisterRepository.findById(assister.getId());

		if (existingAssister != null) {
			existingAssister = assisterRepository.save(assister);
		}
		
		//HIX-86664- Feature returns null if INDs are not enabled or property doesn't exist.
		Assister tmpExistingAssister = assisterIndTriggerService.triggerInd54(existingAssister);
		if(tmpExistingAssister!=null){
			existingAssister = tmpExistingAssister;
		}

		LOGGER.info("update: END");

		return existingAssister;
	}

	/**
	 * @see AssisterService#saveAssister(Assister)
	 * 
	 * @param assister
	 *            {@link Assister}
	 * @param isAHBXInfo
	 * 
	 * @return {@link Assister}
	 */
	@Override
	@Transactional
	public Assister saveAssister(Assister assister) {

		LOGGER.info("saveAssister: START");

		Assister existingAssister = assisterRepository.save(assister);
		// Generate Assister Number if does not exist
		AssisterNumber assisterNumberObj = assisterNumberRepository.findByAssisterId(assister.getId());
		if(assisterNumberObj == null){
			existingAssister = generateAssisterNumber(existingAssister);
			existingAssister = assisterRepository.save(existingAssister);
		}
		
		
		// Trigger IND35
		try {
			LOGGER.info("Send Assister Information call starts");
			assisterIndTriggerService.triggerInd35(assister);
			LOGGER.info("Send Assister Information call ends. ");
		} catch (Exception ex) {
			LOGGER.error(
					"Error while sending Assister information to AHBX : ",
					ex);
		}

		LOGGER.info("saveAssister: END");
		return existingAssister;
	}

	/**
	 * @see AssisterService#updateAssisterInformation(Assister, boolean)
	 * 
	 * @param assister
	 *            {@link Assister}
	 * @param isAHBXInfo
	 * 
	 * @return {@link Assister}
	 */
	@Override
	@Transactional
	public Assister updateAssisterInformation(Assister assister, boolean isAHBXInfo) {

		LOGGER.info("updateAssisterInformation: START");

		Assister existingAssister = assisterRepository.findById(assister.getId());

		// Generate Assister Number if does not exist
		existingAssister = generateAssisterNumber(existingAssister);
		existingAssister = assisterRepository.save(setAssister(existingAssister, assister, isAHBXInfo));

		// Trigger IND35
		try {
			LOGGER.info("Send Assister Information call starts");
			assisterIndTriggerService.triggerInd35(existingAssister);
			LOGGER.info("Send Assister Information call ends. ");
		} catch (Exception ex) {
			LOGGER.error(
					"Error while sending Assister information to AHBX : ",
					ex);
		}

				
		LOGGER.info("updateAssisterInformation: END");

		return existingAssister;
	}

	/**
	 * Sets {@link Assister} to the existing Assister
	 * 
	 * @param existingAssister
	 *            {@link Assister}
	 * 
	 * @param newAssister
	 *            {@link Assister}
	 * 
	 * @return {@link Assister}
	 */
	private Assister setAssister(Assister existingAssister, Assister newAssister, boolean isCertificationNumberUpdate) {

		LOGGER.info("setAssister: START");

		if (newAssister.getUser() != null) {
			existingAssister.setUser(newAssister.getUser());
		}
		existingAssister.setFirstName(newAssister.getFirstName());
		existingAssister.setLastName(newAssister.getLastName());
		existingAssister.setBusinessLegalName(newAssister.getBusinessLegalName());
		existingAssister.setPrimaryPhoneNumber(newAssister.getPrimaryPhoneNumber());
		if(newAssister.getSecondaryPhoneNumber()!=null){
		existingAssister.setSecondaryPhoneNumber(newAssister.getSecondaryPhoneNumber());
		}
		existingAssister.setEmailAddress(newAssister.getEmailAddress());
		if(newAssister.getCommunicationPreference()!=null){
		existingAssister.setCommunicationPreference(newAssister.getCommunicationPreference());
		}
		if(newAssister.getPrimaryAssisterSite()!=null){ 
		existingAssister.setPrimaryAssisterSite(newAssister.getPrimaryAssisterSite());
		}
		if(newAssister.getSecondaryAssisterSite()!=null){
		existingAssister.setSecondaryAssisterSite(newAssister.getSecondaryAssisterSite());
		}
		existingAssister.setUpdatedBy(newAssister.getUpdatedBy());
		existingAssister.setPostalMail(newAssister.getPostalMail());
		EntityLocation existingLocation = existingAssister.getMailingLocation();
		existingLocation.setAddress1(newAssister.getMailingLocation().getAddress1());
		existingLocation.setAddress2(newAssister.getMailingLocation().getAddress2());
		existingLocation.setCity(newAssister.getMailingLocation().getCity());
		existingLocation.setState(newAssister.getMailingLocation().getState());
		existingLocation.setZip(newAssister.getMailingLocation().getZip());
		existingLocation.setLon(newAssister.getMailingLocation().getLon());
		existingLocation.setLat(newAssister.getMailingLocation().getLat());
		existingLocation.setCounty(newAssister.getMailingLocation().getCounty());
		existingLocation.setRdi(newAssister.getMailingLocation().getRdi());
		existingAssister.setMailingLocation(existingLocation);

		existingAssister.setEducation(newAssister.getEducation());
		
		if (newAssister.getCertificationNumber() != null) {
			existingAssister.setCertificationNumber(newAssister.getCertificationNumber());
		} else if(!isCertificationNumberUpdate) {
			existingAssister.setCertificationNumber(null);
		}
		

		LOGGER.info("setAssister: END");

		return existingAssister;
	}

	/**
	 * Retrieves {@link AssisterNumber} if exists, else generate assister number
	 * and set into {@link Assister} object
	 * 
	 * @param assister
	 *            {@link Assister}
	 * @return {@link Assister}
	 */
	private Assister generateAssisterNumber(Assister assister) {

		LOGGER.info("generateAssisterNumber: START");

		AssisterNumber assisterNumberObj = assisterNumberRepository.findByAssisterId(assister.getId());
		if (assisterNumberObj == null) {
			LOGGER.debug("Generating unique assister number for Assister.");
			AssisterNumber assisterNumber = new AssisterNumber();
			assisterNumber.setAssisterId(assister.getId());
			assisterNumberObj = assisterNumberRepository.saveAndFlush(assisterNumber);
		}
		assister.setAssisterNumber(assisterNumberObj.getId());

		LOGGER.info("generateAssisterNumber: END");

		return assister;
	}

	/**
	 * @see AssisterService#findByEnrollmentEntity(EnrollmentEntity)
	 * 
	 * @param enrollmentEntity
	 *            {@link EnrollmentEntity}
	 * @return {@link List<Assister>}
	 */
	@Override
	public List<Assister> findByEnrollmentEntity(EnrollmentEntity enrollmentEntity) {

		LOGGER.info("findByEnrollmentEntity: START");
		LOGGER.info("findByEnrollmentEntity: END");

		return assisterRepository.findByEntity(enrollmentEntity);
	}

	/**
	 * Finds the List of DesignateAssisters by given assister id
	 * 
	 * @param assisterId
	 *            the given Assister's Id
	 * @return the list of DesignateAssisters for the given assister
	 */
	public List<DesignateAssister> findDesignateAssisterByAssisterId(int assisterId) {
		
		LOGGER.info("findDesignateAssisterByAssisterId: START");
		LOGGER.info("findDesignateAssisterByAssisterId: END");

		return designateAssister.findByAssisterId(assisterId);
	}

	/**
	 * @see AssisterService#findByUserId(Integer)
	 * 
	 * @param assisterId
	 *            id of the Assister
	 * @return {@link Assister}
	 */
	@Override
	public Assister findByUserId(Integer userId) {

		LOGGER.info("findByUserId: START");
		LOGGER.info("findByUserId: END");

		return assisterRepository.findByUserId(userId);
	}

	/**
	 * @see AssisterService#findAssisterById(int)
	 * 
	 * @param assisterId
	 *            id of the Assister
	 * @return {@link Assister}
	 */
	@Override
	public Assister findAssisterById(int assisterId) {

		LOGGER.info("findAssisterById: START");
		LOGGER.info("findAssisterById: END");

		return assisterRepository.findById(assisterId);
	}

	/**
	 * @see AssisterService#getAssisterByRecordID(int)
	 * 
	 * @param recordId
	 *            record id of the Assister
	 * @return {@link Assister}
	 */
	@Override
	public Assister getAssisterByRecordID(int recordId) {

		LOGGER.info("getAssisterByRecordID: START");
		LOGGER.info("getAssisterByRecordID: END");

		return assisterRepository.findById(recordId);
	}

	@Override
	public List<Map<String, Object>> loadAssisterActivityStatusHistoryForAdmin(Integer assisterId) {

		LOGGER.info("loadAssisterActivityStatusHistoryForAdmin: START");

		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(UPDATED_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(STATUS_COL, NEW_ACTIVITY_STATUS);
		requiredFieldsMap.put(COMMENT_COL, VIEW_COMMENT);

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(STATUS_COL);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(UPDATE_DATE);
		displayCols.add(PREVIOUS_STATUS);
		displayCols.add(NEW_STATUS);
		displayCols.add(COMMENTS);

		List<Map<String, Object>> data = null;
		try {
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(context);
			List<Map<String, String>> entityHistoryData = service.findRevisions(REPOSITORY_NAME, MODEL_NAME,
					requiredFieldsMap, assisterId);
			data = activityHistoryRendererService.processData(entityHistoryData, compareCols, displayCols);
		} catch (Exception e) {
			LOGGER.error("Exception occurred while loading Assister Status History", e);
		}

		LOGGER.info("loadAssisterActivityStatusHistoryForAdmin: END");

		return data;
	}

	/**
	 * @see AssisterService#searchAssisters(EnrollmentEntityRequestDTO)
	 * 
	 * @param enrollmentEntityRequestDTO
	 *            contains search parameters
	 * @return {@link List<Assister>}
	 */
	@Override
	public List<Assister> searchAssisters(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("searchAssisters: START");

		StringBuilder query = new StringBuilder();
		EntityManager em =null;
		EnrollmentEntity enrollmentEntity = null;
		int enrollmentEntityId = 0;
		List<Assister> assisters = new ArrayList<Assister>();
		
		try {
			em = entityManagerFactory.createEntityManager();
			
			String zip = enrollmentEntityRequestDTO.getZipCode();
			String language = enrollmentEntityRequestDTO.getLanguage();
			Integer startRecord = enrollmentEntityRequestDTO.getStartRecord();
			Integer startLine = startRecord + 1;
			Integer endRecord = startRecord + enrollmentEntityRequestDTO.getPageSize();
			String sortBy = EntityUtils.isEmpty(enrollmentEntityRequestDTO.getSortBy())?"FIRST_NAME":enrollmentEntityRequestDTO.getSortBy();
			String sortOrder = EntityUtils.isEmpty(enrollmentEntityRequestDTO.getSortOrder())?"ASC":enrollmentEntityRequestDTO.getSortOrder();
			
			query.append("SELECT ID,FIRST_NAME,LAST_NAME,LINE_NUMBER FROM ("
					+ "SELECT A.ID,A.FIRST_NAME,A.LAST_NAME,ROW_NUMBER() OVER (ORDER BY A.ID ASC) LINE_NUMBER "
					+ "ROW_NUMBER() OVER (ORDER BY A.ID ASC) LINE_NUMBER FROM EE_ASSISTERS A ");

			if(!EntityUtils.isEmpty(sortBy) && !EntityUtils.isEmpty(sortOrder)){
				query.insert(query.indexOf("A.FIRST_NAME,") + "A.FIRST_NAME,".length(), " ROW_NUMBER() OVER (ORDER BY " +sortBy+" "+sortOrder+ ") LINE_NUMBER ");
			}
			
			String entityName = enrollmentEntityRequestDTO.getEntityName();
			if (entityName != null && !"".equals(entityName)) {
				enrollmentEntity = enrollmentEntityService.findEnrollmentEntityByEntityName(entityName);
				if (enrollmentEntity != null) {
					enrollmentEntityId = enrollmentEntity.getId();
				}
				query.insert(query.lastIndexOf(FROM) + FROM.length(), " EE_ENTITY E, ");
				query.insert(query.indexOf(WHERE) + WHERE.length(), " E.ID=A.EE_ENTITY_ID AND A.EE_ENTITY_ID = :"+ ENROLLMENT_ENTITY_ID);
				
			}

			addLanguageConditionToQuery(query, language);
			addZipCodeConditionToQuery(query, zip);

			//query.append(") WHERE LINE_NUMBER BETWEEN " + startLine + " AND " + endRecord);
			query.append(" ORDER BY :" +sortBy+" "+sortOrder+ " ) SUBQ1 WHERE LINE_NUMBER BETWEEN :" + START_LINE + " AND :" + END_RECORD);
			
			Query pQuery = em.createNativeQuery(query.toString());
			pQuery.setParameter(ENROLLMENT_ENTITY_ID, enrollmentEntityId);
			pQuery.setParameter(START_LINE, startLine);
			pQuery.setParameter(END_RECORD, endRecord);		
			List list = pQuery.getResultList();
			//List list = em.createNativeQuery(query.toString()).getResultList();

			createAssisterList(assisters, list);
			
		} finally {
			if(em != null && em.isOpen()){
				em.close();
			}
		}

		LOGGER.info("searchAssisters: END");

		return assisters;
	}

	/**
	 * @param query
	 * @param language
	 */
	private void addLanguageConditionToQuery(StringBuilder query, String language) {
		if (language != null && !"".equals(language)) {
			query.insert(query.lastIndexOf(FROM) + FROM.length(), " EE_ASSISTER_LANGUAGES AL, ");
			query.insert(query.indexOf(WHERE) + WHERE.length(), " A.ID=AL.EE_ASSISTER_ID AND ");
			query.append(" AND UPPER(AL.SPOKEN_LANGUAGES) LIKE UPPER('%" + language.trim() + "%')");
		}
	}

	/**
	 * @param query
	 * @param zip
	 */
	private void addZipCodeConditionToQuery(StringBuilder query, String zip) {
		if (zip != null) {
			query.insert(query.lastIndexOf(FROM) + FROM.length(), " EE_LOCATIONS L, ");
			query.insert(query.indexOf(WHERE) + WHERE.length(), " A.MAILING_LOCATION_ID=L.ID AND ");
			query.append(" AND L.ZIP LIKE ('" + zip + "%')");
		}
	}

	private void createAssisterList(List<Assister> assisters, List rsList) {

		LOGGER.info("createAssisterList: START");

		Iterator rsIterator = rsList.iterator();

		while (rsIterator.hasNext()) {
			Object[] objArray = (Object[]) rsIterator.next();
			Assister a = new Assister();

			if (objArray[0] != null) {
				a.setId(Integer.valueOf(objArray[0].toString()));
			}
			if (objArray[1] != null) {
				a.setFirstName(objArray[1].toString());

			}
			if (objArray[2] != null) {
				a.setLastName(objArray[2].toString());
			}
			assisters.add(a);
		}

		LOGGER.info("createAssisterList: END");
	}

	/**
	 * @see AssisterService#findAssistersByEEId(Integer)
	 * 
	 * @param enrollmentEntityId
	 *            enrollment entity Id
	 * @return {@link Assister}
	 */
	@Override
	public List<Assister> findAssistersByEEId(Integer enrollmentEntityId) {

		LOGGER.info("findAssistersByEEId: START");
		LOGGER.info("findAssistersByEEId: END");

		return assisterRepository.findByEntityId(enrollmentEntityId);
	}

	/**
	 * @see AssisterService#findByEnrollmentEntityAndSite(Integer, Integer)
	 * 
	 * @param enrollmentEntityId
	 *            Enrollment Entity ID
	 * @return {@link List<Assister>}
	 */
	@Override
	public List<Assister> findByEnrollmentEntityAndSite(Integer enrollmentEntityId, Integer siteId) {

		LOGGER.info("findByEnrollmentEntityAndSite: START");
		LOGGER.info("findByEnrollmentEntityAndSite: END");

		return assisterRepository.findByEntityIdAndSiteId(enrollmentEntityId, siteId);
	}

	@Override
	public ExternalIndividual findIndividualInfo(Integer individualId) {

		//LOGGER.info("findByEnrollmentEntityAndSite: START");
		//abAhbxRestCallInvoker.retrieveAndSaveExternalIndividualDetail(individualId);
		//LOGGER.info("findByEnrollmentEntityAndSite: END");

		return externalIndividualForAssisterRepository.findByIndividualCaseId(individualId);
	}

	@Override
	public AssisterResponseDTO searchIndividualForAssister(Map<String, Object> searchCriteria, String desigStatus,
			Integer startRecord, Integer pageSize) {

		LOGGER.info("searchIndividualForAssister: START");

		EntityManager em = null;

		Query query = null;
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		
		try {
			em = entityManagerFactory.createEntityManager();
			
			// Get search criteria
			String individualFirstName = (String) searchCriteria.get("individualFirstName");
			String individualLastName = (String) searchCriteria.get("individualLastName");
			String status = (String) searchCriteria.get("status");
			Integer assisterId = (Integer) searchCriteria.get("assisterId");
			String fromDate = (String) searchCriteria.get("fromDate");
			String toDate = (String) searchCriteria.get("toDate");
			
			List<Map<String, Object>> individualList = new ArrayList<Map<String, Object>>();

			String queryStr = createIndividualSearchForAssisterQuery(individualFirstName, individualLastName, status, assisterId, fromDate, toDate, false);
			
			query = em.createNativeQuery(queryStr);
			
			setParamatersForIndividualSearchForAssisterQuery(query, individualFirstName, individualLastName, status, assisterId, fromDate, toDate, desigStatus);
			
			query.setFirstResult(startRecord);
			query.setMaxResults(pageSize);
			
			List resultlist = query.getResultList();
			
			int totalCount = 0;
			
			if ((startRecord.intValue() == 0 || startRecord.intValue() == 1) && (resultlist != null && resultlist.size() < pageSize) ) {
				totalCount = resultlist.size();
			}
			else {
				String countQueryStr = createIndividualSearchForAssisterQuery(individualFirstName, individualLastName, status, assisterId, fromDate, toDate, true);
				
				query = em.createNativeQuery(countQueryStr);
				
				setParamatersForIndividualSearchForAssisterQuery(query, individualFirstName, individualLastName, status, assisterId, fromDate, toDate, desigStatus);
				
				List totalCountList = query.getResultList();
				
				if (totalCountList != null && totalCountList.size() > 0){
					totalCount = Integer.parseInt(totalCountList.get(0) + "");
				}
			}
			
			if(null != resultlist) {
				createIndividualListForEnrollmentEntity(individualList, resultlist);
			}
			
			assisterResponseDTO.setAssisterEntityHistory(individualList);
			assisterResponseDTO.setNoOfIndividualsForAssister(totalCount);
			
		} finally {
			if(em != null && em.isOpen()){
				em.close();
			}
		}

		LOGGER.info("searchIndividualForAssister: END");

		return assisterResponseDTO;
	}

	/**
	 * 
	 * method to create individual search query for assister
	 
	 * @param individualFirstName
	 * @param individualLastName
	 * @param status
	 * @param assisterId
	 * @param fromDate
	 * @param toDate
	 * @param desigStatus
	 * @param isTotalCountQuery
	 * @return
	 */
	private String createIndividualSearchForAssisterQuery(String individualFirstName, String individualLastName, String status, Integer assisterId, String fromDate, String toDate, boolean isTotalCountQuery)
	{
		StringBuilder query = new StringBuilder();
		
		if (isTotalCountQuery)
		{
			query.append("SELECT COUNT(*) FROM "
					+ " CMR_HOUSEHOLD I, EE_DESIGNATE_ASSISTERS D WHERE I.ID = D.INDIVIDUAL_ID");
	
		}
		else
		{
			query.append("SELECT I.FIRST_NAME,I.LAST_NAME,D.LAST_UPDATE_TIMESTAMP,I.STATUS ,I.ID,I.FAMILY_SIZE,I.HOUSEHOLD_INCOME FROM "
					+ " CMR_HOUSEHOLD I, EE_DESIGNATE_ASSISTERS D WHERE I.ID = D.INDIVIDUAL_ID AND D.STATUS = :desigStatus");
		}
		
		if (assisterId != null) {
			query.append(" AND D.ASSISTER_ID= :" + ASSISTER_ID );
		}

		EntityUtils.appendToSearchAssisterQuery(query, "I.FIRST_NAME", individualFirstName, FIRST_NAME, LIKE, true, false);
		EntityUtils.appendToSearchAssisterQuery(query, "I.LAST_NAME", individualLastName, LAST_NAME, LIKE, true, false);
		EntityUtils.appendToSearchAssisterQuery(query, "I.STATUS", status, ELIGIBILITY_STATUS, "=", true, false);
		
		if (fromDate != null && !"".equals(fromDate)) {
			query.append(" AND D.LAST_UPDATE_TIMESTAMP >=to_date(:" + FROM_DATE + ",'mm-dd-yy')");
		}
		if (toDate != null && !"".equals(toDate)) {
			query.append(" AND D.LAST_UPDATE_TIMESTAMP < to_date(:" + TO_DATE + ",'mm-dd-yy')");
		}
			
		if (!isTotalCountQuery)
		{
			query.append(" ORDER BY D.ID " );
		}
		
		return query.toString();
	}
	
	/**
	 * method to set parameters to individual search query for assister
	 * 
	 * @param query
	 * @param individualFirstName
	 * @param individualLastName
	 * @param status
	 * @param assisterId
	 * @param fromDate
	 * @param toDate
	 * @param desigStatus
	 */
	private void setParamatersForIndividualSearchForAssisterQuery(Query query, String individualFirstName, String individualLastName, String status, Integer assisterId, String fromDate, String toDate, String desigStatus)
	{
				
		if (assisterId != null) {
			query.setParameter(ASSISTER_ID, assisterId );
		}

		EntityUtils.setQueryParameter(query, DESIG_STATUS, desigStatus, false);
		EntityUtils.setQueryParameter(query, FIRST_NAME, individualFirstName, true);
		EntityUtils.setQueryParameter(query, LAST_NAME, individualLastName, true);
		EntityUtils.setQueryParameter(query, ELIGIBILITY_STATUS, status, false);
		EntityUtils.setQueryParameter(query, FROM_DATE, fromDate, false);
		EntityUtils.setQueryParameter(query, TO_DATE, toDate, false);

	}

	/**
	 * @ to set Individual List
	 */
	private void createIndividualListForEnrollmentEntity(List<Map<String, Object>> individualList, List list) {

		LOGGER.info("createIndividualListForEnrollmentEntity: START");

		Iterator rsIterator = list.iterator();
		while (rsIterator.hasNext()) {
			Object[] objArray = (Object[]) rsIterator.next();
			Map<String, Object> individual = new HashMap<String, Object>();
			// set individual name
			if ((objArray[0] != null) && (objArray[1] != null)) {
				individual.put("individualName", objArray[0].toString().concat(" ").concat(objArray[1].toString()));
			}
			// set date
			if (objArray[2] != null) {
				String dateStr = objArray[2].toString();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date convertedDate = null;
				try {
					convertedDate = dateFormat.parse(dateStr);
				} catch (ParseException e) {
					LOGGER.error("Error while parsing date  : ", e);
				}
				individual.put("updated", convertedDate);
			}
			if ((objArray[THREE] != null)) {
				individual.put("status", objArray[THREE].toString());
			}
			if ((objArray[FOUR] != null)) {
				individual.put("individualcaseid", objArray[FOUR].toString());
			}
			if ((objArray[FIVE] != null)) {
				individual.put("familySize", objArray[FIVE].toString());
			}
			if ((objArray[SIX] != null)) {
				individual.put("houseHoldIncome", objArray[SIX].toString());
			}
			individualList.add(individual);
		}

		LOGGER.info("createIndividualListForEnrollmentEntity: END");
	}

	@Override
	public DesignateAssister getDesignateAssister(Integer individualId) {

		LOGGER.info("getDesignateAssister: START");
		LOGGER.info("getDesignateAssister: END");

		return designateAssister.findByIndividualId(individualId);
	}

	/**
	 * @see AssisterService#searchAssistersByEntityAndSite(AssisterRequestDTO)
	 * 
	 * @param AssisterRequestDTO
	 *            contains Assister search parameters
	 * @return {@link List<Assister>}
	 */
	@Override
	public List<Assister> searchAssistersByEntityAndSite(AssisterRequestDTO assisterRequestDTO) {

		LOGGER.info("searchAssistersByEntityAndSite: START");

		StringBuilder query = new StringBuilder();
		EntityManager em = null;

		List<Assister> assisters = new ArrayList<Assister>();

		try {
			 em = entityManagerFactory.createEntityManager();
			
			 Integer startRecord = assisterRequestDTO.getStartRecord();
				Integer startLine = startRecord + 1;
				Integer endRecord = startRecord + assisterRequestDTO.getPageSize();

				Integer enrollmentEntityId = assisterRequestDTO.getEnrollmentEntityId();
				Integer siteId = assisterRequestDTO.getSiteId();

				query.append("SELECT ID,FIRST_NAME,LAST_NAME,ADDRESS1,ADDRESS2,CITY,STATE,ZIP,COUNTY,EE_ENTITY_ID,EMAIL_ADDRESS,PRIMARY_PHONE_NUMBER,LINE_NUMBER,SITE_ID FROM ("
						+ "SELECT A.ID,A.FIRST_NAME,A.LAST_NAME,L.ADDRESS1,L.ADDRESS2,L.CITY,L.STATE,L.ZIP,L.COUNTY,A.EE_ENTITY_ID,"
						+ "A.EMAIL_ADDRESS,A.PRIMARY_PHONE_NUMBER,ROW_NUMBER() OVER (ORDER BY A.LAST_NAME, A.FIRST_NAME) LINE_NUMBER,S.ID AS SITE_ID "
						+ "FROM EE_ASSISTERS A INNER JOIN  EE_SITES S ON  A.PRIMARY_ASSISTER_SITE_ID = S.ID OR A.SECONDARY_ASSISTER_SITE_ID = S.ID LEFT OUTER JOIN  EE_LOCATIONS L ON  S.PHYSICAL_LOCATION_ID = L.ID   WHERE  ");
				
				query.append(" A.EE_ENTITY_ID =:enrollmentEntityId"
						+ " AND S.ID = :siteId"+ " AND A.CERTIFICATION_STATUS = 'Certified' AND A.STATUS = 'Active'  ");
				query.append(" ) SUBQ2 ");
				query.append(" WHERE LINE_NUMBER BETWEEN :" + START_LINE + " AND :" + END_RECORD);

				Query pQuery= em.createNativeQuery(query.toString());
				pQuery.setParameter("enrollmentEntityId", enrollmentEntityId);
				pQuery.setParameter("siteId", siteId);
				pQuery.setParameter(START_LINE,startLine);
				pQuery.setParameter(END_RECORD, endRecord);
				
				List list = pQuery.getResultList();

				setAssisterList(assisters, list);
			 
		} finally {
			if(em != null && em.isOpen()){
				em.close();
			}

		}

		LOGGER.info("searchAssistersByEntityAndSite: END");

		return assisters;
	}

	/**
	 * @see AssisterService#totalAssistersByEntityAndSite(AssisterRequestDTO)
	 * 
	 * @param AssisterRequestDTO
	 *            contains Assister search parameters
	 * @return no of Assisters
	 */
	@Override
	public int totalAssistersByEntityAndSite(AssisterRequestDTO assisterRequestDTO) {

		LOGGER.info("totalAssistersByEntityAndSite: START");

		StringBuilder query = new StringBuilder();
		EntityManager em = null;
		List list = null;
		try {
			em = entityManagerFactory.createEntityManager();
			
			Integer enrollmentEntityId = assisterRequestDTO.getEnrollmentEntityId();
			Integer siteId = assisterRequestDTO.getSiteId();

			query.append("SELECT ID,FIRST_NAME,LAST_NAME,ADDRESS1,ADDRESS2,CITY,STATE,ZIP,COUNTY,EE_ENTITY_ID,EMAIL_ADDRESS,PRIMARY_PHONE_NUMBER,LINE_NUMBER,SITE_ID FROM ("
					+ "SELECT A.ID,A.FIRST_NAME,A.LAST_NAME,L.ADDRESS1,L.ADDRESS2,L.CITY,L.STATE,L.ZIP,L.COUNTY,A.EE_ENTITY_ID,"
					+ "A.EMAIL_ADDRESS,A.PRIMARY_PHONE_NUMBER,ROW_NUMBER() OVER (ORDER BY A.ID ASC) LINE_NUMBER,S.ID AS SITE_ID "
					+ "FROM EE_ASSISTERS A INNER JOIN  EE_SITES S ON A.PRIMARY_ASSISTER_SITE_ID = S.ID OR  A.SECONDARY_ASSISTER_SITE_ID = S.ID LEFT OUTER JOIN  EE_LOCATIONS L ON S.PHYSICAL_LOCATION_ID = L.ID   WHERE  ");
			
			query.append(" A.EE_ENTITY_ID = :enrollmentEntityId "
					+ " AND S.ID = :siteId" +" AND A.CERTIFICATION_STATUS = 'Certified' AND A.STATUS = 'Active' ");

			query.append(") SUBQ3 ");
			Query pQuery= em.createNativeQuery(query.toString());
			pQuery.setParameter("enrollmentEntityId", enrollmentEntityId);
			pQuery.setParameter("siteId", siteId);
			list = pQuery.getResultList();
			
		} finally {
			if(em != null && em.isOpen()){
				em.close();
			}

		}
		
		LOGGER.info("totalAssistersByEntityAndSite: END");

		return list.size();
	}

	private void setAssisterList(List<Assister> assisters, List rsList) {

		LOGGER.info("setAssisterList: START");

		Iterator rsIterator = rsList.iterator();

		while (rsIterator.hasNext()) {
			Object[] objArray = (Object[]) rsIterator.next();
			Assister a = new Assister();
			EnrollmentEntity entity = new EnrollmentEntity();
			EntityLocation location = new EntityLocation();
			Site site = new Site();

			setAssisterDetail(objArray, a, location);
			if (objArray[NINE] != null) {
				entity.setId(Integer.valueOf(objArray[NINE].toString()));
			}
			if (objArray[TEN] != null) {
				a.setEmailAddress(objArray[TEN].toString());
			}
			if (objArray[ELEVEN] != null) {
				a.setPrimaryPhoneNumber(objArray[ELEVEN].toString());
			}
			if (objArray[THIRTEEN] != null) {
				site.setId(Integer.valueOf(objArray[THIRTEEN].toString()));
			}
			a.setEntity(entity);
			//Setting Physical Location of Entity's Primary/Subsite to Assister's mailing location
			a.setMailingLocation(location);
			a.setPrimaryAssisterSite(site);
			assisters.add(a);
		}

		LOGGER.info("setAssisterList: END");
	}

	/**
	 * @param objArray
	 * @param a
	 * @param location
	 */
	private void setAssisterDetail(Object[] objArray, Assister a,
			EntityLocation location) {
		if (objArray[0] != null) {
			a.setId(Integer.valueOf(objArray[0].toString()));
		}
		if (objArray[1] != null) {
			a.setFirstName(objArray[1].toString());
		}
		if (objArray[2] != null) {
			a.setLastName(objArray[2].toString());
		}
		if (objArray[THREE] != null) {
			location.setAddress1(objArray[THREE].toString());
		}
		if (objArray[FOUR] != null) {
			location.setAddress2(objArray[FOUR].toString());
		}
		if (objArray[FIVE] != null) {
			location.setCity(objArray[FIVE].toString());
		}
		if (objArray[SIX] != null) {
			location.setState(objArray[SIX].toString());
		}
		if (objArray[SEVEN] != null) {
			location.setZip(objArray[SEVEN].toString());
		}
		if (objArray[EIGHT] != null) {
			location.setCounty(objArray[EIGHT].toString());
		}
	}

	/**
	 * @see AssisterService#getAssisterByUserId(AssisterRequestDTO)
	 * 
	 * @param userId
	 * @return no of Assister
	 */
	@Override
	public Assister getAssisterByUserId(Integer userId) {

		LOGGER.info("getAssisterByUserId: START");
		LOGGER.info("getAssisterByUserId: END");

		return assisterRepository.findByUserId(userId);
	}
	
	@Override
	public Assister getAssisterRecordByUserId(Integer userId) {

		LOGGER.info("getAssisterByUserId: START");
		LOGGER.info("getAssisterByUserId: END");

		return assisterRepository.findAssisterRecordByUserId(userId);
	}

	/**
	 * Finds Assister Number
	 * 
	 * @param assisterId
	 * @return no of AssisterNumber
	 */
	@Override
	public AssisterNumber findAssisterNumberByAssisterId(int assisterId) {

		LOGGER.info("findAssisterNumberByAssisterId: START");
		LOGGER.info("findAssisterNumberByAssisterId: END");

		return assisterNumberRepository.findByAssisterId(assisterId);
	}
	
	@Override
	public int findByAssisterEnrollmentEntity(AssisterRequestDTO assisterRequestDTO, int entityId) {
		
		LOGGER.info("findAssistersForEnrollmentEntity: START");
		LOGGER.info("searchAssister: START");
		
		StringBuilder query = new StringBuilder();
		List<?> list=null;
		EntityManager em = null;
		int recordCount =0;
		
		try {
			em = entityManagerFactory.createEntityManager();
			String sortBy = assisterRequestDTO.getSortBy();
			String sortOrder = assisterRequestDTO.getSortOrder();
			
				query.append("SELECT ID,FIRST_NAME,LAST_NAME,ENTITY_NAME,CERTIFICATION_STATUS,STATUS,RECERTIFICATION_DATE,CLIENT_COUNT,LINE_NUMBER FROM ");
				query.append("(SELECT Temp.ID,Temp.FIRST_NAME,Temp.LAST_NAME,Temp.ENTITY_NAME,Temp.CERTIFICATION_STATUS,Temp.STATUS,Temp.RECERTIFICATION_DATE," 
				+"CLIENT_COUNT,ROW_NUMBER() OVER (ORDER BY " + sortBy + " " + sortOrder + ") as LINE_NUMBER ");
				
				query.append( " FROM (SELECT A.ID,A.FIRST_NAME,A.LAST_NAME,E.ENTITY_NAME,A.CERTIFICATION_STATUS,A.STATUS,A.RECERTIFICATION_DATE, ");
				
				query.append("(SELECT COUNT(*) FROM EE_DESIGNATE_ASSISTERS DA WHERE DA.ASSISTER_ID = A.ID) ");
				query.append(" as CLIENT_COUNT ");
				query.append( " FROM EE_ASSISTERS A,EE_ENTITIES E WHERE A.EE_ENTITY_ID=E.ID");
			
				EntityUtils.appendToSearchAssisterQuery(query, "A.FIRST_NAME", assisterRequestDTO.getFirstName(), ASSISTER_FIRST_NAME, LIKE, true, false);
				EntityUtils.appendToSearchAssisterQuery(query, "A.LAST_NAME", assisterRequestDTO.getLastName(), ASSISTER_LAST_NAME, LIKE, true, false);
				EntityUtils.appendToSearchAssisterQuery(query, "A.CERTIFICATION_STATUS", assisterRequestDTO.getCertificationStatus(), CERTIFICATION_STATUS, "=", false, false);
			
				if (assisterRequestDTO.getStatusInactive() == null || "".equals(assisterRequestDTO.getStatusInactive())) {
					EntityUtils.appendToSearchAssisterQuery(query, A_STATUS, assisterRequestDTO.getStatusactive(), ACTIVE, "=", false, false);
				}
				else if (assisterRequestDTO.getStatusactive() == null || "".equals(assisterRequestDTO.getStatusactive())) {
					EntityUtils.appendToSearchAssisterQuery(query, A_STATUS, assisterRequestDTO.getStatusInactive(), IN_ACTIVE, "=", false, false);
				}
				else {
					query.append(" AND A.STATUS IN (:"+ACTIVE+",:"+IN_ACTIVE+")");
				}
				query.append(" AND A.EE_ENTITY_ID="+ assisterRequestDTO.getEnrollmentEntityId());
				query.append(" ) Temp");
				EntityUtils.appendToSearchAssisterQuery(query, "E.ENTITY_NAME", assisterRequestDTO.getEntityName(), ENTITY_NAME, LIKE, true, false);
				
				appendDateConditionToQuery(assisterRequestDTO, query);
				query.append(" ) SUBQ4 "); 
				
		
			List<Assister> assisterList = new ArrayList<Assister>();
			Query pQuery = em.createNativeQuery(query.toString());
			setQueryParameterForNoOfAssisterBySearch(assisterRequestDTO,pQuery);
			 list= pQuery.getResultList();
			createAssistersList(assisterList, list);
			
		} finally {
			if(em != null && em.isOpen()){
				em.close();
			}
		}
	
		LOGGER.info("findAssistersForEnrollmentEntity: END");

		return recordCount = list.size();
	}
	
	/**
	 * @param assisterRequestDTO
	 * @param query
	 */
	private void appendDateConditionToQuery(
			AssisterRequestDTO assisterRequestDTO, StringBuilder query) {
		String fromDate = assisterRequestDTO.getFromDate();
		String toDate = assisterRequestDTO.getToDate();
		if (fromDate != null && !"".equals(fromDate)) {
			query.append(" AND A.RECERTIFICATION_DATE >=to_date(:" + FROM_DATE + ",'mm-dd-yy')");
		}
		if (toDate != null && !"".equals(toDate)) {
			query.append("AND A.RECERTIFICATION_DATE <= to_date(:" + TO_DATE + ",'mm-dd-yy') + 1");
		}
	}
	
	private void setQueryParameterForNoOfAssisterBySearch(AssisterRequestDTO assisterRequestDTO,Query searchQuery ){
		
		EntityUtils.setQueryParameter(searchQuery, ASSISTER_FIRST_NAME, assisterRequestDTO.getFirstName(), true);
		EntityUtils.setQueryParameter(searchQuery, ASSISTER_LAST_NAME, assisterRequestDTO.getLastName(), true);
		EntityUtils.setQueryParameter(searchQuery, CERTIFICATION_STATUS, assisterRequestDTO.getCertificationStatus(), false);

		String statusActive = assisterRequestDTO.getStatusactive();
		String statusInActive = assisterRequestDTO.getStatusInactive();

		if (assisterRequestDTO.getStatusInactive() == null || "".equals(assisterRequestDTO.getStatusInactive())) {
			EntityUtils.setQueryParameter(searchQuery, ACTIVE, assisterRequestDTO.getStatusactive(), false);
		}
		else if (assisterRequestDTO.getStatusactive() == null || "".equals(assisterRequestDTO.getStatusactive())) {
			EntityUtils.setQueryParameter(searchQuery, IN_ACTIVE, assisterRequestDTO.getStatusInactive(), false);
		}
		else {
			searchQuery.setParameter(ACTIVE,statusActive);
			searchQuery.setParameter(IN_ACTIVE,statusInActive);
		}
		EntityUtils.setQueryParameter(searchQuery, ENTITY_NAME, assisterRequestDTO.getEntityName(), true);

		String fromDate = assisterRequestDTO.getFromDate();
		String toDate = assisterRequestDTO.getToDate();
		if (fromDate != null && !"".equals(fromDate)) {
			searchQuery.setParameter(FROM_DATE,fromDate);
		}
		if (toDate != null && !"".equals(toDate)) {
			searchQuery.setParameter(TO_DATE,toDate);
		}

	}
	
	private void createAssistersList(List<Assister> assisterList, List rsList) {
		
		LOGGER.info("createAssistersList: START");
		
		Iterator rsIterator = rsList.iterator();
		while (rsIterator.hasNext()) {
			Object[] objArray = (Object[]) rsIterator.next();
			Assister a = new Assister();
			if (objArray[0] != null) {
				a.setId(Integer.valueOf(objArray[0].toString()));
			}
			if (objArray[1] != null) {
				a.setFirstName(objArray[1].toString());
			}
			if (objArray[2] != null) {
				a.setLastName(objArray[2].toString());
			}
			if (objArray[THREE] != null) {
				EnrollmentEntity enrollmentEntity = new EnrollmentEntity();
				enrollmentEntity.setEntityName(objArray[THREE].toString());
				a.setEntity(enrollmentEntity);
			}
			if (objArray[FOUR] != null) {
				a.setCertificationStatus(objArray[FOUR].toString());
			}
			if (objArray[FIVE] != null) {
				a.setStatus(objArray[FIVE].toString());
			}
			if (objArray[SIX] != null) {
				try {

					a.setReCertificationDate(EntityUtils.formatDate(new SimpleDateFormat(EntityConstants.DATE_FORMAT).format(new SimpleDateFormat(
							EntityConstants.DATE_TIME_FORMAT).parse(objArray[SIX].toString()))));

				} catch (Exception exception) {
					LOGGER.error("Exception occured while formatting Date" , exception);
				}
			}
			if (objArray[SEVEN] != null) {
				a.setClientCount(objArray[SEVEN].toString());
			}
			assisterList.add(a);
		}

		LOGGER.info("createAssistersList: END");
	}
	
	public List<Assister> findByEmailAddress(String emailAddress){
		
		return assisterRepository.findByEmailAddress(emailAddress);
		
	}
	
	@Override
	public List<AssisterExportDTO> getAssisterExportList()  
	{
		LOGGER.info("getAssisterExportList : START");
		List<AssisterExportDTO> assisterExportList = null;
		
		List<Object[]> assisterExportListObj = assisterRepository.getAssisterExportList();
		
		if(assisterExportListObj!=null){
			assisterExportList = new ArrayList<>();
			
			Iterator rsIterator = assisterExportListObj.iterator();
			while (rsIterator.hasNext()) {
				Object[] objArray = (Object[]) rsIterator.next();
				AssisterExportDTO assisterExportDTO = new AssisterExportDTO();
				if (objArray[0] != null) {
					assisterExportDTO.setId(Integer.valueOf(objArray[0].toString()));
				}
				if (objArray[1] != null) {
					assisterExportDTO.setFirstName(objArray[1].toString());
				}
				if (objArray[2] != null) {
					assisterExportDTO.setLastName(objArray[2].toString());
				}
				if (objArray[3] != null) {
					assisterExportDTO.setEntityName(objArray[3].toString());
				}
				if (objArray[4] != null) {
					assisterExportDTO.setAssisterNumber(Long.valueOf(objArray[4].toString()));
				}
				if (objArray[5] != null) {
					try {
						assisterExportDTO.setRecertificationDate(EntityUtils.formatDate(new SimpleDateFormat(EntityConstants.DATE_FORMAT).format(new SimpleDateFormat(
								EntityConstants.DATE_TIME_FORMAT).parse(objArray[5].toString()))));
						} catch (Exception exception) {
						LOGGER.error("Exception occured while formatting Date" , exception);
					}
				}
				if (objArray[6] != null) {
					assisterExportDTO.setCertificationStatus(objArray[6].toString());
				}
				if (objArray[7] != null) {
					assisterExportDTO.setAddress1(objArray[7].toString());
				}
				if (objArray[8] != null) {
					assisterExportDTO.setAddress2(objArray[8].toString());
				}
				if (objArray[9] != null) {
					assisterExportDTO.setCity(objArray[9].toString());
				}
				if (objArray[10] != null) {
					assisterExportDTO.setState(objArray[10].toString());
				}
				if (objArray[11] != null) {
					assisterExportDTO.setZip(objArray[11].toString());
				}
				if (objArray[12] != null) {
					assisterExportDTO.setPrimaryPhoneNumber(objArray[12].toString());
				}
				if (objArray[13] != null) {
					assisterExportDTO.setEmailAddress(objArray[13].toString());
				}
				if (objArray[14] != null) {
					assisterExportDTO.setPrefMethodOfCommunication(objArray[14].toString());
				}
				if (objArray[15] != null) {
					assisterExportDTO.setPendingRequestsCount(Integer.valueOf(objArray[15].toString()));
				}
				if (objArray[16] != null) {
					assisterExportDTO.setActiveRequestsCount(Integer.valueOf(objArray[16].toString()));
				}
				if (objArray[17] != null) {
					assisterExportDTO.setInactiveRequestsCount(Integer.valueOf(objArray[17].toString()));
				}
				
				assisterExportList.add(assisterExportDTO);
			}
		}
		
		LOGGER.info("assisterExportListObj.size :"+(assisterExportListObj!=null? assisterExportListObj.size():null) );
		return assisterExportList;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context= applicationContext;
		
	}
}
