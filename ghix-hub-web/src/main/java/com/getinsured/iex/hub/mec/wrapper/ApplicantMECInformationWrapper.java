package com.getinsured.iex.hub.mec.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ApplicantMECInformationType;
import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.InsuranceApplicantResponseType;
import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.MonthlyPremiumAmountType;

public class ApplicantMECInformationWrapper implements JSONAware{

	private InsuranceApplicantResponseWrapper applicantResponseWrapper;
	private MonthlyPremiumAmountTypeWrapper monthlyPremiumAmountTypeWrapper;
	
	public ApplicantMECInformationWrapper(ApplicantMECInformationType mecInfo) {
		
		if(mecInfo != null){
			mecInfo.getInconsistencyIndicator().isValue();
			InsuranceApplicantResponseType applicantReponse = mecInfo.getInsuranceApplicantResponse();
			
			if(applicantReponse != null)
			{
				this.applicantResponseWrapper = new InsuranceApplicantResponseWrapper(applicantReponse);
			}
			
			MonthlyPremiumAmountType monthlyPremiumAmountType = mecInfo.getMonthlyPremiumAmount();
			
			if(monthlyPremiumAmountType != null)
			{
				this.monthlyPremiumAmountTypeWrapper = new MonthlyPremiumAmountTypeWrapper(monthlyPremiumAmountType);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ApplicantResponse", this.applicantResponseWrapper);
		obj.put("MonthlyPremiumAmount", this.monthlyPremiumAmountTypeWrapper);
		return obj.toJSONString();
	}

}
