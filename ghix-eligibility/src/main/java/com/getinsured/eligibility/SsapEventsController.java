package com.getinsured.eligibility;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.at.resp.service.SsapApplicationService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.dto.qep.QepEventRequest;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.ssap.service.SsapEventsService;

@Controller
@RequestMapping("/SsapEventsController")

public class SsapEventsController {
	@Autowired SsapApplicationRepository ssapApplicationRepository;
	@Autowired SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired SsapApplicantRepository ssapApplicantRepository;
	@Autowired SsapEventsService ssapEventsService;
	@Autowired private SsapApplicationService ssapApplicationService;
	private static Logger LOGGER = Logger.getLogger(SsapEventsController.class);

	@RequestMapping(value = "/processEvents", method = RequestMethod.POST)
	@ResponseBody
	public String processEvents(@RequestBody QepEventRequest qepEventRequest) {
		Long appId = qepEventRequest.getSsapApplicationId();
		try {
			LOGGER.info("invoking creation of events for application id: " + appId);
			if(ssapApplicationService.checkIsEditApplicationFlow(appId)){
				LOGGER.info("checkIsEditApplicationFlow: true");
				// TODO: If it's edit application, why we set flag to false?
				ssapApplicationService.updateIsEditApplicationFlag(qepEventRequest.getCaseNumber(), false);
				LOGGER.info("finished checkIsEditApplicationFlow: true");
				return "success";
			}
			LOGGER.info("calling createSsapEvents()");
			ssapEventsService.createSsapEvents(qepEventRequest);
			ssapEventsService.triggerQleValidation(qepEventRequest.getCaseNumber(), qepEventRequest.getUserId());
			LOGGER.info("finished createSsapEvents()");
		}
		catch(Exception e){
			LOGGER.error("Error creating SSAP Events: " + e.getMessage(), e);
			LOGGER.info("Exception from processEvents()");
			throw new GIRuntimeException(e);
		}

		LOGGER.info("returning from processEvents()");
		return "success";
	}
	
	
}
