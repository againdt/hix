package com.getinsured.eligibility.at.ref.service.migration;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.iex.ssap.model.AccountTransferMigration;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

@Component("oeReferralProcessingMigrationService")
public class OeReferralProcessingMigrationService {
	private static final Logger LOGGER = Logger.getLogger(OeReferralProcessingMigrationService.class);

	@Autowired
	@Qualifier("referralProcessingMigrationService")
	private ReferralProcessingMigrationService referralProcessingMigrationService;

	@Autowired
	@Qualifier("referralSsapCmrLinkMigrationService")
	private ReferralSsapCmrLinkMigrationService referralSsapCmrLinkMigrationService;
	
	@Autowired
	private MigrationUtil migrationUtil;

	public String processReferral(AccountTransferRequestDTO accountTransferRequest) {
		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("OeReferralMigrationProcessingService processReferral starts ");
			referralResponse.getData().put(ReferralProcessingConstants.KEY_APPLICATIONS_WITH_SAME_ID, accountTransferRequest.getApplicationsWithSameId());

			ssapApplicationId = referralProcessingMigrationService.executeOeReferral(accountTransferRequest);
			referralResponse.getData().put(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID, ssapApplicationId);

			final String useExternalId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_HOUSEHOLD_CASE_ID_ENABLE);
			if(null != useExternalId && "TRUE".equalsIgnoreCase(useExternalId)) {
				List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes =accountTransferRequest.getAccountTransferRequestPayloadType().getInsuranceApplication().getApplicationIdentification();
				String hhCaseId = null ;
				for(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType : iTypes) {
					if(  ReferralConstants.HOUSE_HOLD_CASE_ID.equalsIgnoreCase( tIdentificationType.getIdentificationCategoryText( ).getValue()  )   ) {
						hhCaseId = tIdentificationType.getIdentificationID().getValue();
						break;
					}
				}
				referralResponse.getData().put(ReferralConstants.HOUSE_HOLD_CASE_ID, hhCaseId);
				
				migrationUtil.updateATMigrationByTransferId(accountTransferRequest, ssapApplicationId, false, hhCaseId, null, AccountTransferMigration.StatusType.APPLICATION_CREATED.getValue());
			}
			
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_CREATING_REFERRAL_APPLICATION + accountTransferRequest.getGiwsPayloadId());
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_CREATING_REFERRAL_APPLICATION).append(accountTransferRequest.getGiwsPayloadId()).append(ReferralProcessingConstants.REASON)
			        .append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			migrationUtil.persistGiMonitorId(ssapApplicationId, e);
		} finally {
			referralResponse.getData().put(ReferralProcessingConstants.KEY_GI_WS_PAYLOAD_ID, accountTransferRequest.getGiwsPayloadId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_COMPARED_TO_APPLICATION_ID, accountTransferRequest.getCompareToApplicationId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_ACCOUNT_TRANSFER_CATEGORY, accountTransferRequest.getAccountTransferCategory());
		}

		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("OeReferralMigrationProcessingService processReferral ends - Response is - " + response);

		return response;
	}

	public String processLinkCmr(Message<String> message) {

		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("OeReferralMigrationProcessingService processLinkCmr starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());

			referralResponse.getData().putAll(input.getData());
			String hhCaseId = (String) input.getData().get(ReferralConstants.HOUSE_HOLD_CASE_ID)    ;
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			final Map<String, Object> mpData = referralSsapCmrLinkMigrationService.executeOELinking((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID),hhCaseId);
			referralResponse.getData().put(ReferralProcessingConstants.REFERRAL_AUTOLINKING, ReferralUtil.convertToBoolean(mpData.get(ReferralProcessingConstants.AUTO_LINK)));
			referralResponse.getData().put(ReferralProcessingConstants.KEY_REFERRAL_CMR_MULTIPLE, ReferralUtil.convertToBoolean(mpData.get(ReferralProcessingConstants.MULTIPLE_CMR)));
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR, ReferralUtil.convertToBoolean(mpData.get(ReferralProcessingConstants.NON_FINANCIAL_CMR)));
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR_ID, ReferralUtil.convertToInt(mpData.get(ReferralProcessingConstants.NON_FINANCIAL_CMR_ID)));

			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_LINK_CMR + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LINK_CMR).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_LINK_CMR);
			migrationUtil.persistGiMonitorId(ssapApplicationId, e);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("OeReferralMigrationProcessingService processLinkCmr ends - Response is - " + response);

		return response;
	}
}
