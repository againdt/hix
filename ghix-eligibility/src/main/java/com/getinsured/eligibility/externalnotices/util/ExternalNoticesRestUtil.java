package com.getinsured.eligibility.externalnotices.util;

import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.model.ExternalNoticeRequest;
import com.getinsured.hix.platform.util.exception.GIException;

@Component
public class ExternalNoticesRestUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalNoticesRestUtil.class);
	private RestTemplate restTemplate;
	
	public static String EXTERNAL_NOTICES_API_ENDPOINT;
	public static String EXTERNAL_NOTICES_API_BASIC_AUTH_USER;
	public static String EXTERNAL_NOTICES_API_BASIC_AUTH_PASSWORD;

	@Value("#{configProp['external.notices.api.endpoint']}")
	public void setExternalNoticesApiEndPoint(String externalNoticesApiEndPoint) {
		EXTERNAL_NOTICES_API_ENDPOINT = externalNoticesApiEndPoint;
	}
	@Value("#{configProp['external.notices.api.username']}")
	public void setExternalNoticesApiUserName(String userName) {
		EXTERNAL_NOTICES_API_BASIC_AUTH_USER = userName;
	}
	@Value("#{configProp['external.notices.api.password']}")
	public void setExternalNoticesApiUserPassword(String password) {
		EXTERNAL_NOTICES_API_BASIC_AUTH_PASSWORD = password;
	}

	@Autowired
	public ExternalNoticesRestUtil(RestTemplate restTemplate) {
		super();
		this.restTemplate = restTemplate;
	}
	public String postToExternalSystem(ExternalNoticeRequest externalNoticeRequest) throws GIException {
		ResponseEntity<String> externalNoticeResponse = null;
		if (externalNoticeRequest != null) {
			try {
				String plainCreds = EXTERNAL_NOTICES_API_BASIC_AUTH_USER+":"+EXTERNAL_NOTICES_API_BASIC_AUTH_PASSWORD;
				byte[] plainCredsBytes = plainCreds.getBytes();
				byte[] base64CredsBytes = Base64.getMimeEncoder().encode(plainCredsBytes);
				String base64Creds = new String(base64CredsBytes);

				HttpHeaders headers = new HttpHeaders();
				headers.add("Authorization", "Basic " + base64Creds);
				headers.setContentType(MediaType.APPLICATION_JSON);
				HttpEntity<Object> requestEntity = new HttpEntity<>(externalNoticeRequest, headers);
				externalNoticeResponse = restTemplate.exchange(EXTERNAL_NOTICES_API_ENDPOINT, HttpMethod.POST, requestEntity, String.class);
			} catch (Exception e) {
				LOGGER.error("Exception occured in Rest call for External System: ", e);
				throw new GIException(e);
			}
		}
		return externalNoticeResponse.getBody();
	}

}
