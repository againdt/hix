package com.getinsured.eligibility.lce.service;

import java.util.List;

import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;

public interface LifeChangeEventApplicationEventService {

	List<SsapApplicationEvent> cloneSsapApplicationEvent(SsapApplication currentApplication, Long userId, SsapApplicationEventTypeEnum eventType) throws Exception;
}
