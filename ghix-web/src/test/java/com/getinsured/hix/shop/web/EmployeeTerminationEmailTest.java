package com.getinsured.hix.shop.web;

import com.getinsured.hix.BaseTest;
import com.jcraft.jsch.IO;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.w3c.tidy.Tidy;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.xhtmlrenderer.pdf.ITextRenderer;

public class EmployeeTerminationEmailTest extends BaseTest
{
	private String notesContent = null;

	@Test
	public void readHtmlFile()
	{
		InputStream inputStream = null;
		try
		{
			/*InputStream inputStream = this
					.getClass()
					.getClassLoader()
					.getResourceAsStream("EmployeeTerminationConfirmation.html");*/
			String inputFile = "src/main/resources/EmployeeTerminationConfirmation.html";
			BufferedInputStream mybuffer = null;
			DataInputStream datainput = null;
			inputStream =	new FileInputStream(inputFile); 
			
			mybuffer = new BufferedInputStream(inputStream);
			datainput = new DataInputStream(mybuffer);
			StringBuffer stringBuffer = new StringBuffer();
			while (datainput.available() != 0)
			{
				stringBuffer.append(datainput.readLine());
			}
			readTemplate(stringBuffer.toString());
			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}finally{
			IOUtils.closeQuietly(inputStream);
		}
	}

	public void readTemplate(String templateData)
	{
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();
		Template tmpl = null;
		try
		{
			stringLoader.putTemplate("EmployeeTerminationConfirmation",	templateData);
			templateConfig.setTemplateLoader(stringLoader);
			tmpl = templateConfig.getTemplate("EmployeeTerminationConfirmation");
			tmpl.process(getReplacableData(), sw);
			textSectionConverter(sw.toString());
			generatePdf();
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public Map<String, String> getReplacableData()
	{
		String signature = "<img src="+'"'+"image/signature.jpg"+'"'+ " alt="+'"'+'"'+ " />";
		SimpleDateFormat simpleFormatter = new SimpleDateFormat("MMMMM DD, yyyy");
		Map<String, String> tokens = new HashMap<String, String>();
		tokens.put("SystemDate", simpleFormatter.format(new java.util.Date()));
		tokens.put("EmployeeName", "Kuldeep Sharma");
		tokens.put("EmployerName", "Xoriant Solutions");
		tokens.put("EnrolledPlanName", "Ghix_Health_01");
		tokens.put("IssuerName", "United Health Insurance");
		tokens.put("TerminationDate", "02/01/2013");
		tokens.put("ExchangeSignature", signature);
		tokens.put("ExchangePhone", "1800554400");
		tokens.put("EmployerPhone", "022-30437208");
		return tokens;
	}

	public void generatePdf() throws Exception
	{
		OutputStream os1 = null;
		OutputStream os = null;
		notesContent = "" + notesContent + "";
		StringWriter writer = new StringWriter();
		Tidy tidy = new Tidy();
		tidy.setTidyMark(false);
		tidy.setDocType("omit");
		tidy.setXHTML(true);
		tidy.setInputEncoding("utf-8");
		tidy.setOutputEncoding("utf-8");
		tidy.parse(new StringReader(notesContent), writer);
		writer.close();
		notesContent = writer.toString();
		String outputFile1 = "src/main/resources/EmployeeTermination.xhtml";
        
		try{
			os1 = new FileOutputStream(outputFile1);
	        os1.write(notesContent.getBytes());
	        
	        String inputFile = "src/main/resources/EmployeeTermination.xhtml";
	        String url = new File(inputFile).toURI().toURL().toString();
	        String outputFile = "src/main/resources/Employee_termination.pdf";
	        os = new FileOutputStream(outputFile);
	        
	        ITextRenderer renderer = new ITextRenderer();
	        renderer.setDocument(url);
	        renderer.layout();
	        renderer.createPDF(os);
		}finally{
			IOUtils.closeQuietly(os);
			IOUtils.closeQuietly(os1);
		}
   		System.out.println("Succesfully Generated PDF ");
	}

	public void textSectionConverter(String notesContent)
	{
		this.notesContent = notesContent;
	}
}
