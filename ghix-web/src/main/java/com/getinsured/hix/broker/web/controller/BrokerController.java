package com.getinsured.hix.broker.web.controller;

import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.hix.agency.utils.AgencyUtils;
import com.getinsured.hix.broker.BrokerConfirmationEmail;
import com.getinsured.hix.broker.BrokerProfileUpdateEmail;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.agency.AgencyInformationDto;
import com.getinsured.hix.dto.broker.BrokerAvailabilityDTO;
import com.getinsured.hix.dto.broker.BrokerConnectHoursDTO;
import com.getinsured.hix.dto.broker.BrokerInformationDTO;
import com.getinsured.hix.dto.broker.BrokerParticipationDTO;
import com.getinsured.hix.dto.broker.DesignateBrokerDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentCountDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.dto.shop.EmployeeDTO;
import com.getinsured.hix.dto.shop.EmployerDTO;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.model.*;
import com.getinsured.hix.model.DesignateBroker.Status;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.*;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.jsoup.helper.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.imageio.ImageIO;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.Map.Entry;

/**
 * Handles requests for the application home page.
 */
@SuppressWarnings("unused")
@Controller
public class BrokerController {
	private static final String COMMA_EXCEPTION_IS = ", Exception is: ";
	private static final int MAX_COMMENT_LENGTH = 4000;
	private static final int FILE_INPUT_MAX_SIZE = 5242880;

	private static final int TWELVE = 12;

	private static final int EIGHT = 8;

	private static final int SEVEN = 7;

	private static final int FOUR = 4;

	private static final int THREE = 3;

	private static final int ZERO = 0;

	private static final int MINUS_THIRTY = -30;

	private static final String TARGET_NAME = "target_name";

	private static final String TARGET_ID = "target_id";

	private static final String REDIRECT = "redirect:";

	private static final String INVALID_USER_EXCEPTION_OCCURRED = "Invalid User Exception occurred";

	private static final String IGNORING_PAYMENT_INFO_CURRENTLY = "Ignoring PaymentInfo currently.";

	private static final String HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER = "hasPermission(#model, 'BROKER_VIEW_BROKER')";

	private static final String HAS_PERMISSION_MODEL_BROKER_ADD_BROKER = "hasPermission(#model, 'BROKER_ADD_BROKER')";

	private static final String BROKER_VIEW_PROFILE = "broker/viewprofile";

	private static final String STATE_EXCHANGE_TYPE = "stateExchangeType";

	private static final String VALIDATE_ADDRESS = "validateAddress";

	private static final String SIZE_EXCEED = "sizeExceed";

	private static final String FALSE = "false";

	private static final String TRUE2 = "true";

	private static final String PHONE3 = "phone3";

	private static final String PHONE2 = "phone2";

	private static final String PHONE1 = "phone1";

	private static final String SWITCH_TO_MODULE_ID = "switchToModuleId";

	private static final String IS_USER_SWITCH_TO_OTHER_VIEW = "isUserSwitchToOtherView";

	private static final String BRK_NAME = "brkName";

	private static final String BROKER_OBJECT_FOR_LEFT_NAV = "brokerObjectForLeftNav";

	private static final String PAYMENT_OBJ_ID = "paymentObjId";

	private static final String CANCEL = "cancel";

	private static final String MAILING_LOCATION_OBJ = "mailingLocationObj";

	private static final String LOCATION_OBJ = "locationObj";

	private static final String LIST_OF_STATES = "statelist";

	private static final String LOCATION_TYPE = "locationType";

	private static final String VALIDATED_MAILING_LOCATION = "validatedMailingLocation";

	private static final String VALIDATED_BUSINESS_LOCATION = "validatedBusinessLocation";

	private static final String BROKER_MODULE = "broker";

	private static final String BROKER_ROLE = "BROKER";

	private static final String PAGE_TITLE = "page_title";

	private static final String UPDATEPROFILE = "updatedProfile";

	private static final String PAYMENT_METHODS_OBJ = "paymentMethodsObj";

	private static final String PAYMENT_ID = "paymentId";

	private static final String ERROR_MESSAGE = "errorMsg";

	private static final String APPLICATION_ERROR = "Application Error ::Please contact system admin";
	
	private static final String EMAIL = "email";

	private List<String> languages = null;

	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerController.class);

	private static final String APP_JSON_CONTENT = "application/json";

	private static final String LANGUAGES_LIST = "languagesList";
	private static final String EDUCATION_LIST = "educationList";
	private static final String BROKER_ID = "brokerId";
	private static final String EMPLOYER_CASE_DETAILS = "redirect:/broker/employer/employercase/";
	private static final String ACTIVATION_RESULT = "?activationLinkResult=";
	public static final Integer PAGE_SIZE = 10;
	private static final String LAST_VISITED_URL = "lastVisitedURL";
	private static final String ACTIVATION_LINK_RESULT =  "activationLinkResult";
	private static final String FAILURE = "FAILURE";
	private static final XStream XSTREAM = GhixUtils.getXStreamStaxObject();
	private static final String ENCRYPTED_BROKER_ID = "encryptedBrokerId";
	private String BROKER_ADMIN = "BROKER_ADMIN";
	private String ADMIN = "ADMIN";
	
	@Autowired private Gson platformGson;
	@Autowired
	private LookupService lookupService;

	@Autowired
	private BrokerConfirmationEmail brokerConfirmationEmail;
	@Autowired
	private BrokerProfileUpdateEmail brokerProfileUpdateEmail;
	@Autowired
	private BrokerService brokerService;
	@Autowired
	private DesignateService designateService;

	@Autowired
	private UserService userService;
	@Autowired
	private IUserRepository userRepository;
	@Autowired
	private SCIMUserManager scimUserManager;
	@Autowired
	private RoleService roleService;

	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	@Autowired
	private ZipCodeService zipCodeService;

	@Autowired private MessageSource messageSource;


	@Autowired private AccountActivationService accountActivationService;


	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

	@Autowired
	private BrokerMgmtUtils brokerMgmtUtils;

	@Autowired
	private SmartValidator smartValidator;

	@Autowired
	private EERestCallInvoker restClassCommunicator;
	
	@Autowired
	private AgencyUtils agencyUtils;
	/**
	 * To display Broker's Dashboard page
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for Broker Dashboard
	 */
	@RequestMapping(value = "/broker/dashboard", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	public String dashboard(HttpServletRequest request, Model model) {
		LOGGER.info("dashboard : START");
		String json = "[{\"planLevel\":\"NoPlan\",\"enrollmentCount\":0}]";

		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				LOGGER.info("userActiveModuleName: "+ user.getActiveModuleName());
			}
		} catch (InvalidUserException e) {
			LOGGER.error(INVALID_USER_EXCEPTION_OCCURRED, e);
		}
		Broker brokerObj = null;
		if (isUserSwitch(request)) {
			brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String) request.getSession().getAttribute(
					SWITCH_TO_MODULE_ID)));
		} else {
			Validate.notNull(user);

			brokerObj = brokerService.findBrokerByUserId(user.getId());
			if(brokerObj != null && user!= null && user.getActiveModuleId()==ZERO ){
				user.setActiveModuleId(brokerObj.getId());
			}
			//HIX-110852: Resetting session values to default value based on logged role.
			if(userService.hasUserRole(user, RoleService.BROKER_ROLE)){
				LOGGER.info("logged role: "+ RoleService.BROKER_ROLE);
				user.setActiveModuleName(RoleService.BROKER_ROLE);
				user.setPersonatedRoleName(RoleService.BROKER_ROLE);
				request.getSession().setAttribute("userActiveRoleName",RoleService.BROKER_ROLE.toLowerCase());
				request.getSession().setAttribute("switchToModuleName", RoleService.BROKER_ROLE.toLowerCase());
				request.getSession().setAttribute("designatedBrokerProfile", brokerObj);
			}else if(userService.hasUserRole(user, RoleService.AGENCY_MANAGER)) {
				LOGGER.info("logged role: "+ RoleService.AGENCY_MANAGER);
				user.setActiveModuleName(RoleService.AGENCY_MANAGER);
				user.setPersonatedRoleName(RoleService.AGENCY_MANAGER);
				request.getSession().setAttribute("userActiveRoleName",RoleService.AGENCY_MANAGER.toLowerCase());
				request.getSession().setAttribute("switchToModuleName", RoleService.AGENCY_MANAGER.toLowerCase());
			}
		}

		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
			String ahbxSwitchAccountUrl = GhixConstants.SWITCH_URL_IND66;
			model.addAttribute(BrokerConstants.SWITCH_ACC_URL, ahbxSwitchAccountUrl);
			model.addAttribute(ENCRYPTED_BROKER_ID, ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(brokerObj.getId())));
			
			if(userService.hasUserRole(user, RoleService.ADMIN_ROLE) || userService.hasUserRole(user, RoleService.BROKER_ADMIN_ROLE) || userService.hasUserRole(user, RoleService.OPERATIONS_ROLE)){
				model.addAttribute("isAdminUser", true);
			}else{
				model.addAttribute("isAdminUser", false);
			}
			
			//HIX-111361: Resetting session values to default value based on logged role.
			if(userService.hasUserRole(user, RoleService.BROKER_ROLE)){
				LOGGER.info("logged role CA: "+ RoleService.BROKER_ROLE);
				user.setActiveModuleName(RoleService.BROKER_ROLE);
				user.setActiveModuleId(brokerObj.getId());
				user.setPersonatedRoleName(RoleService.BROKER_ROLE);
				request.getSession().setAttribute("userActiveRoleName",RoleService.BROKER_ROLE.toLowerCase());
				request.getSession().setAttribute("switchToModuleName", RoleService.BROKER_ROLE.toLowerCase());
				request.getSession().setAttribute("designatedBrokerProfile", brokerObj);
			}else if(userService.hasUserRole(user, RoleService.AGENCY_MANAGER)) {
				LOGGER.info("logged role CA: "+ RoleService.AGENCY_MANAGER);
				user.setActiveModuleName(RoleService.AGENCY_MANAGER);
				user.setPersonatedRoleName(RoleService.AGENCY_MANAGER);
				request.getSession().setAttribute("userActiveRoleName",RoleService.AGENCY_MANAGER.toLowerCase());
				request.getSession().setAttribute("switchToModuleName", RoleService.AGENCY_MANAGER.toLowerCase());
				request.getSession().setAttribute("designatedBrokerProfile", brokerObj);
			}
		}
		
		Validate.notNull(brokerObj);

		//set state exchange type in session
		setStateExchangeTypeInSession(request);
		enrollmentRequest.setAssisterBrokerId(brokerObj.getId());
		enrollmentRequest.setRole(EnrollmentRequest.Role.AGENT);
		enrollmentRequest.setStartDate(DateUtil.addToDate(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT, MINUS_THIRTY));
		enrollmentRequest.setEndDate(DateUtil.addToDate(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT, +1));

		XStream xstream = GhixUtils.getXStreamStaxObject();
		String requestXML = GhixUtils.xStreamHibernateXmlMashaling().toXML(enrollmentRequest);
		long startTime = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("ReST call start."+ startTime);
		String restResponse = ghixRestTemplate.postForObject(
				EnrollmentEndPoints.FIND_ENROLLMENT_PLANLEVEL_COUNT_BY_BROKER, requestXML, String.class);
		long endTime = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("ReST call end."+ endTime);
		LOGGER.info("Enrollment api total duration:"+ (endTime - startTime));
		EnrollmentResponse response = (EnrollmentResponse) xstream.fromXML(restResponse);

		if (response != null && response.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
			List<EnrollmentCountDTO> enrollmentCountDTOList = response.getEnrollmentCountDTO();
			json =  platformGson.toJson(enrollmentCountDTOList);
		}
		request.getSession().setAttribute(LAST_VISITED_URL, "/broker/dashboard");
		LOGGER.debug("EnrollmentResponse is converted to JSON :");

		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
		model.addAttribute("ID_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE));
		model.addAttribute("NV_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE));
		model.addAttribute("broker", brokerObj);
		
		request.getSession().setAttribute("json", json);

		LOGGER.info("dashboard : END");

		return "broker/dashboard";
	}

    @RequestMapping(value = "/broker/helpondemand", method = RequestMethod.GET)
    @PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
    public String helpOnDemand(HttpServletRequest request, Model model,
                        @RequestParam(value = "showconfirmation", required = false) boolean showconfirmation) {
        LOGGER.info("helpOnDemand::going to help on demand JSP");

        model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: View Help On Demand");
        Broker brokerObj = null;
        try {
            AccountUser user = userService.getLoggedInUser();
            decryptRequestParam(request);
            LOGGER.info("helpOnDemand - broker Id - attribute -  {}", request.getAttribute(BROKER_ID));
            if (isUserSwitch(request)) {
                if(null != request.getAttribute(BROKER_ID)){
                    brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String)request.getAttribute(BROKER_ID)));
                }else if(null != request.getParameter(BROKER_ID)){
                    brokerObj = brokerService.getBrokerDetail(Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(request.getParameter(BROKER_ID))));
                }else {
                    brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String) request.getSession().getAttribute(
                            SWITCH_TO_MODULE_ID)));
                }

            } else if(null != request.getParameter(BROKER_ID)){
                brokerObj = brokerService.getBrokerDetail(Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(request.getParameter(BROKER_ID))));
            }else if(null != request.getAttribute(BROKER_ID)){
                brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String)request.getAttribute(BROKER_ID)));
            } else {
                brokerObj = brokerService.findBrokerByUserId(user.getId());
            }

            if (brokerObj == null) {
                brokerObj = new Broker();
                brokerObj.setUser(user);
            }

            agencyUtils.compareAgencyId(user, brokerObj);

            if (brokerObj.getContactNumber() != null) {
                model.addAttribute(PHONE1, brokerObj.getContactNumber().substring(ZERO, THREE));
                model.addAttribute(PHONE2, brokerObj.getContactNumber().substring(FOUR, SEVEN));
                model.addAttribute(PHONE3, brokerObj.getContactNumber().substring(EIGHT, TWELVE));
            }

            model.addAttribute(BROKER_MODULE, brokerObj);
            model.addAttribute(LOCATION_OBJ, brokerObj.getLocation());
            model.addAttribute(MAILING_LOCATION_OBJ, brokerObj.getMailingLocation());
            model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
            model.addAttribute("ca_statecode", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));

            if (!StringUtils.isBlank(request.getParameter(CANCEL))
                    && request.getParameter(CANCEL).trim().equalsIgnoreCase(TRUE2)) {
                model.addAttribute(UPDATEPROFILE, false);
            }

            getPaymentId(model, request, brokerObj,user);

            //Shuffled code to prevent change in attached object.HIX-102613
            // adds space after comma for clients served and product experties
            String clientsServed = brokerObj.getClientsServed();
            clientsServed= addSpaceToField(clientsServed);
            brokerObj.setClientsServed(clientsServed);

            String productExperties = brokerObj.getProductExpertise();
            productExperties = addSpaceToField(productExperties);
            brokerObj.setProductExpertise(productExperties);

            if(null == brokerObj.getUser()){
                //This condition is for newly created brokers (through agency) which doesn't have user account
                AccountUser acUser= new AccountUser();
                acUser.setFirstName(brokerObj.getFirstName());
                acUser.setLastName(brokerObj.getLastName());
                brokerObj.setUser(acUser);
            }

            request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);

            if (showconfirmation) {
                request.getSession().setAttribute(UPDATEPROFILE, TRUE2);
            } else {
                request.getSession().setAttribute(UPDATEPROFILE, FALSE);
            }
        } catch (InvalidUserException ex) {
            request.getSession().setAttribute(UPDATEPROFILE, FALSE);
            LOGGER.error(INVALID_USER_EXCEPTION_OCCURRED, ex);
            throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
        }catch(GIRuntimeException giexception){
            LOGGER.error("GIRuntimeException occured while viewing Brokers profile : ", giexception);
            throw giexception;
        } catch(Exception exception){
            LOGGER.error("Exception occured while viewing Brokers profile  : ", exception);
            throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
        }


        LOGGER.info("helpOnDemand : END");

        return "broker/helpondemand";
    }

    @RequestMapping(value = "/broker/participationDetails", method = RequestMethod.GET, produces = "application/json")
    //@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
    public @ResponseBody String getParticipationDetails(HttpServletRequest request, Model model) {
        BrokerConnectResponse brokerConnectResponse = new BrokerConnectResponse();
        brokerConnectResponse.setResponseCode(404);
        String response = platformGson.toJson(brokerConnectResponse);

        try {
            LOGGER.debug("getParticipationDetails::getting user and getting participation");
            AccountUser user = userService.getLoggedInUser();
            Map<String, Integer> urlVariables = new HashMap<>();
            //urlVariables.put("brokerId", user.getId());
            response = ghixRestTemplate.getForObject(GhixEndPoints.BrokerServiceEndPoints.GET_SET_PARTICIPATION_DETAILS + "/" + user.getActiveModuleId(), String.class, urlVariables);
            LOGGER.debug("getParticipationDetails::response from serivce = {}", response);
        } catch(Exception ex) {
            LOGGER.error("getParticipationDetails::error getting households", ex);
        }

        return response;
    }

    @RequestMapping(value = "/broker/participationDetails", method = RequestMethod.POST, produces = "application/json")
    //@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
    public @ResponseBody String setParticipationDetails(HttpServletRequest request, Model model, @RequestBody BrokerParticipationDTO brokerParticipation) {
        BrokerConnectResponse brokerConnectResponse = new BrokerConnectResponse();
        brokerConnectResponse.setResponseCode(404);
        String response = platformGson.toJson(brokerConnectResponse);

        try {
            LOGGER.debug("setParticipationDetails::getting user and getting participation");
            AccountUser user  = userService.getLoggedInUser();
            if(user != null) {
                LOGGER.debug("setParticipationDetails::user id = {}", user.getId());
                response = ghixRestTemplate.postForObject(GhixEndPoints.BrokerServiceEndPoints.GET_SET_PARTICIPATION_DETAILS + "/" + user.getActiveModuleId(), brokerParticipation, String.class);
                LOGGER.debug("setParticipationDetails::response from serivce = {}", response);
            } else {
                LOGGER.debug("setParticipationDetails::user was null");
            }
        } catch(Exception ex) {
            LOGGER.error("setParticipationDetails::error getting households", ex);
        }

        return response;
    }

    @RequestMapping(value = "/broker/hours", method = RequestMethod.GET, produces = "application/json")
    //@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
    public @ResponseBody String getBrokerConnectDetails(HttpServletRequest request, Model model) {
        BrokerConnectResponse brokerConnectResponse = new BrokerConnectResponse();
        brokerConnectResponse.setResponseCode(404);
        String response = platformGson.toJson(brokerConnectResponse);

        try {
            LOGGER.debug("getBrokerConnectDetails::getting user and connect hours");
            AccountUser user  = userService.getLoggedInUser();
            Map<String, Integer> urlVariables = new HashMap<>();
            //urlVariables.put("brokerId", user.getId());
            response = ghixRestTemplate.getForObject(GhixEndPoints.BrokerServiceEndPoints.GET_SET_CONNECT_HOURS + "/" + user.getActiveModuleId(), String.class, urlVariables);
            LOGGER.debug("getBrokerConnectDetails::response from serivce = {}", response);
        } catch(Exception ex) {
            LOGGER.error("getBrokerConnectDetails::error getting broker connect details", ex);
        }

        return response;
    }

    @RequestMapping(value = "/broker/hours", method = RequestMethod.POST, produces = "application/json")
    //@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
    public @ResponseBody String setBrokerConnectDetails(HttpServletRequest request, Model model, @RequestBody BrokerConnectHoursDTO brokerConnectHoursDTO) {
        BrokerConnectResponse brokerConnectResponse = new BrokerConnectResponse();
        brokerConnectResponse.setResponseCode(404);
        String response = platformGson.toJson(brokerConnectResponse);

        try {
            LOGGER.debug("setBrokerConnectDetails::brokerConnectHoursDTO = {}", brokerConnectHoursDTO);
            if(brokerConnectHoursDTO != null) {
                LOGGER.debug("setBrokerConnectDetails::getting user and connect hours");
                AccountUser user = userService.getLoggedInUser();
                if(user != null) {
                    LOGGER.debug("setBrokerAvailability::user id = {}", user.getId());
                    response = ghixRestTemplate.postForObject(GhixEndPoints.BrokerServiceEndPoints.GET_SET_CONNECT_HOURS + "/" + user.getActiveModuleId(), brokerConnectHoursDTO, String.class);
                    LOGGER.debug("setBrokerConnectDetails::response from serivce = {}", response);
                } else {
                    LOGGER.debug("setBrokerAvailability::user was null");
                }
            } else {
                LOGGER.debug("setBrokerConnectDetails::brokerConnectHoursDTO null");
            }
        } catch(Exception ex) {
            LOGGER.error("setBrokerConnectDetails::error getting broker connect details", ex);
        }

        return response;
    }

    @RequestMapping(value = "/broker/available", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String setBrokerAvailability(HttpServletRequest request, Model model, @RequestBody BrokerAvailabilityRequest brokerAvailabilityRequest) {
        BrokerConnectResponse brokerConnectResponse = new BrokerConnectResponse();
        brokerConnectResponse.setResponseCode(404);
        String response = platformGson.toJson(brokerConnectResponse);
        try {
            LOGGER.debug("setBrokerAvailability::setting available to = {}", brokerAvailabilityRequest.getAvailable());
            if(brokerAvailabilityRequest != null) {
                LOGGER.debug("setBrokerAvailability::getting user and connect hours");
                AccountUser user = userService.getLoggedInUser();
                if(user != null) {
                    LOGGER.debug("setBrokerAvailability::user id = " + user.getId());
                    response = ghixRestTemplate.postForObject(GhixEndPoints.BrokerServiceEndPoints.SET_BROKER_AVAILABILITY + "/" + user.getActiveModuleId(), brokerAvailabilityRequest.getAvailable(), String.class);
                    LOGGER.debug("setBrokerAvailability::response from serivce = {}", response);
                } else {
                    LOGGER.debug("setBrokerAvailability::user was null");
                }
            }
        } catch(Exception ex) {
            LOGGER.error("setBrokerAvailability::error getting broker connect details", ex);
        }

        return response;
    }

	private void setStateExchangeTypeInSession(HttpServletRequest request) {
		boolean isEmployerFlowAllowed = false;
		boolean isIndividualFlowAllowed = false;
		String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
		String enableConsumerSignup = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ENABLE_CONSUMER_SIGNUP);

		if(null != stateExchangeType) {
			stateExchangeType = stateExchangeType.trim();

			if("BOTH".equalsIgnoreCase(stateExchangeType)) {
				isEmployerFlowAllowed = true;
				isIndividualFlowAllowed = true;
			}
			else if("INDIVIDUAL".equalsIgnoreCase(stateExchangeType)){
				isIndividualFlowAllowed = true;
			}
			else if("SHOP".equalsIgnoreCase(stateExchangeType)){
				isEmployerFlowAllowed = true;
			}
		}
		request.setAttribute("enableConsumerSignup", enableConsumerSignup);
		request.getSession().setAttribute("isEmployerFlowAllowed", isEmployerFlowAllowed);
		request.getSession().setAttribute("isIndividualFlowAllowed", isIndividualFlowAllowed);
	}

	/**
	 * Registration URL for Broker
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/broker/registrationconfirm", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	public String registrationConfirm(HttpServletRequest request){

		LOGGER.info("registrationConfirm : START");

		// Enable the menu
		userService.enableRoleMenu(request, BROKER_ROLE);

		LOGGER.info("registrationConfirm : END");

		return "redirect:/broker/certificationapplication";

	}

	/**
	 * To get password confirmation screen
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/broker/passwordconfirm", method = RequestMethod.GET)
//	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_BROKER')")
	public String passwordConfirm() {

		LOGGER.info("passwordConfirm : START");

		LOGGER.info("passwordConfirm : END");

		return "broker/passwordconfirm";
	}

	/**
	 * To get password setup screen
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation
	 */
/*	@RequestMapping(value = "/broker/passwordsetup", method = RequestMethod.GET)
	//@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_BROKER')")
	public String brokerPasswordSetup(Model model, HttpServletRequest request) {

		LOGGER.info("brokerPasswordSetup : START");

		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Broker Registeration Password Setup");
		model.addAttribute("userId", request.getParameter("user"));
		LOGGER.info("brokerPasswordSetup : END");
		return "broker/passwordsetup";
	}*/

	/**
	 * To get broker notifications page
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/broker/notifications", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	public String brokerNotifications(Model model, HttpServletRequest request) {

		LOGGER.info("brokerNotifications : START");

		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Broker Notifications");
		model.addAttribute("userId", request.getParameter("user"));

		LOGGER.info("brokerNotifications : END");

		return "broker/notifications";
	}

	/**
	 * To save broker password
	 *
	 * @param user
	 *            {@link AccountUser}
	 * @param result
	 *            {@link BindingResult}
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/broker/passwordsetup", method = RequestMethod.POST)
	@GiAudit(transactionName = "User Password Change",
	 eventType = EventTypeEnum.PASSWORD_RESET,
	 eventName = EventNameEnum.PII_WRITE)
	public String brokerPasswordSave(@ModelAttribute("AccountUser") AccountUser user, BindingResult result,
			Model model, HttpServletRequest request) {

		LOGGER.info("brokerPasswordSave : START");
		String tmp = (String) request.getSession().getAttribute("errorMsg");
		if(tmp != null){
			request.getSession().removeAttribute("errorMsg");
		}
		try {
			/*// MSHIX-735 : Check the logged in user before changing the password.
				AccountUser loggedInUser = userService.getLoggedInUser();
				if(loggedInUser.getId() != user.getId()) {
					LOGGER.error("Invalid user for changing password.");
					return "redirect:/";
				} else {
					userService.savePassword(user.getId(), user.getPassword());
				}*/

			int userId  =user.getId();
             //token = vimoencryptor.decrypt(token);

	         AccountUser checkUser = userService.findById(userId);
	         if(checkUser==null){
	                   throw new InvalidUserException("Invalid to rest the password-in user");
	          }
	         if(GhixPlatformConstants.SCIM_ENABLED) {
	        	boolean passwordUpdated = scimUserManager.changePasswordByAdmin(checkUser, user.getPassword());
	        	
	        	if(passwordUpdated == false){
					request.getSession().setAttribute("errorMsg", "Failed to reset the password for user: " + checkUser.getEmail());

	        	}else{
	        	
		        	//Invalidate the password recovery token upon successful password change for SSO
		        	checkUser.setPasswordRecoveryToken(null);
		        	checkUser.setPassword("PASSWORD_NOT_IN_USE");
		 			checkUser.setPasswordRecoveryTokenExpiration(null);
		 			checkUser.setLastUpdatedBy(userId);
		 			checkUser.setPasswordLastUpdatedTimeStamp(new TSDate());
		 			GiAuditParameterUtil.add( new GiAuditParameter("PASSWORD_SET_BY_ADMIN_FOR", checkUser.getUsername()));
		 			userRepository.save(checkUser);
	        	}
	         } else {
	        	 userService.changePasswordAdmin(checkUser.getId(), user.getPassword());
	         }

		} catch (InvalidUserException e) {
			LOGGER.error("Invalid logged-in user", e);
		} catch (GIException e) {
			LOGGER.error("PASSWORD CHNG FAILED", e);
			if("30000".equalsIgnoreCase(e.getMessage())) {
				request.getSession().setAttribute("errorMsg", messageSource.getMessage("label.wso2msgforpasswordhistoryplugin", null, LocaleContextHolder.getLocale()));
			}
		}

		LOGGER.info("brokerPasswordSave : END");

		return "redirect:/broker/passwordconfirm";
	}

	/**
	 * Retrieves edit profile page
	 *
	 * @param model
	 *            {@link Model}
	 * @param selectedAdrs
	 *            {@link Location}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to broker edit profile page
	 */
	@RequestMapping(value = "/broker/editprofile", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_EDIT_BROKER')")
	public String editprofile(Model model,
			@RequestParam(value = "selectedAdrs", required = false) Location selectedAdrs, HttpServletRequest request) {
		LOGGER.info("editprofile : START");

		try{
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Edit Broker Profile");
		Location validatedBusinessLocation = (Location) request.getSession().getAttribute(VALIDATED_BUSINESS_LOCATION);
		Location validatedMailingLocation = (Location) request.getSession().getAttribute(VALIDATED_MAILING_LOCATION);
		Broker brokerObj = null;
		AccountUser user = null;
		try {
			decryptRequestParam(request);
			 user = userService.getLoggedInUser();

			 if(null != request.getAttribute("brokerId")){
					brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String)request.getAttribute("brokerId")));
			 }else if (isUserSwitch(request)) {
				 
					brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String) request.getSession().getAttribute(
							SWITCH_TO_MODULE_ID)));
				
			} else {
				Validate.notNull(user);
				brokerObj = brokerService.findBrokerByUserId(user.getId());
			}

			if (brokerObj == null) {
				brokerObj = new Broker();
				brokerObj.setUser(user);
				model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());

			} else {
				model.addAttribute(LANGUAGES_LIST, loadLanguagesData());
				model.addAttribute(BROKER_MODULE, brokerObj);

				if (validatedBusinessLocation != null) {
					model.addAttribute(LOCATION_OBJ, validatedBusinessLocation);
				} else {
					model.addAttribute(LOCATION_OBJ, brokerObj.getLocation());
				}

				if (validatedMailingLocation != null) {
					model.addAttribute(MAILING_LOCATION_OBJ, validatedMailingLocation);
				} else {
					model.addAttribute(MAILING_LOCATION_OBJ, brokerObj.getMailingLocation());
				}
				model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
				model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
				setContactNumber(model, brokerObj);
			}
		} catch (InvalidUserException ex) {
			LOGGER.error(INVALID_USER_EXCEPTION_OCCURRED, ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);


		}

		getPaymentId(model, request, brokerObj,user);


		if (request.getSession().getAttribute(SIZE_EXCEED) != null) {
			String sizeExceed = (String) request.getSession().getAttribute(SIZE_EXCEED);
			model.addAttribute(SIZE_EXCEED, sizeExceed);
			request.getSession().removeAttribute(SIZE_EXCEED);
		} else {
			model.addAttribute(SIZE_EXCEED, "0");
		}
		request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);

		model.addAttribute(STATE_EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE));
		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));

		model.addAttribute(EDUCATION_LIST, loadEducationData());
		
		if(null == brokerObj.getUser()){
			AccountUser acTmpUser= new AccountUser();
			acTmpUser.setFirstName(brokerObj.getFirstName());
			acTmpUser.setLastName(brokerObj.getLastName());
			brokerObj.setUser(acTmpUser);
		}
		
		}catch(GIRuntimeException giexception){
            LOGGER.error("GIRuntimeException occured while retreiving edit profile page : ", giexception);
            throw giexception;
		} catch(Exception exception){
            LOGGER.error("Exception occured while retreiving edit profile page of broker : ", exception);
            throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
     }


		LOGGER.info("editprofile: END");
		return "broker/editprofile";

	}

	/**
	 * Setting broker contact number to model
	 *
	 * @param model
	 *            {@link Model}
	 * @param brokerObj
	 *            {@link Broker}
	 */
	private void setContactNumber(Model model, Broker brokerObj) {

		LOGGER.info("setContactNumber : START");

		if (brokerObj.getContactNumber() != null) {
			model.addAttribute(PHONE1, brokerObj.getContactNumber().substring(ZERO, THREE));
			model.addAttribute(PHONE2, brokerObj.getContactNumber().substring(FOUR, SEVEN));
			model.addAttribute(PHONE3, brokerObj.getContactNumber().substring(EIGHT, TWELVE));
		}

		LOGGER.info("setContactNumber : END");
	}


	/**
	 * To redirect to broker home screen
	 *
	 * @param model
	 *            {@link Model}
	 * @return URL for navigation to home page
	 */
	@RequestMapping(value = "/broker/home", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	public String brokerHome(Model model) {

		LOGGER.info("brokerHome : START");

		LOGGER.info("Broker Home");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Broker Home");

		LOGGER.info("brokerHome : END");

		return "broker/home";
	}


	/**
	 * Saves broker edited profile
	 *
	 * @param broker
	 *            {@link Broker}
	 * @param locationObj
	 *            {@link Location}
	 * @param result
	 *            {@link BindingResult}
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param file
	 *            {@link CommonsMultipartFile}
	 * @return URL for navigation to broker view profile page
	 * @throws CloneNotSupportedException
	 */
	@RequestMapping(value = "/broker/editprofile", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'BROKER_EDIT_BROKER')")
	public String profilesubmit(@ModelAttribute(BROKER_MODULE) Broker broker,BindingResult bindingResult,
			@ModelAttribute(LOCATION_OBJ) Location locationObj, @RequestParam(value = "state_hidden", required = false) String state_hidden,
			BindingResult result, Model model,
			HttpServletRequest request) {
		LOGGER.info("profilesubmit : START");
		Broker updatedBroker = null;
		
		try{
		if (broker.getLocation() == null) {
			return "redirect:/broker/editprofile";
		}
		decryptRequestParam(request);
		// Check for XSS attack
		checkXSSAttack(broker);

		Broker brkObj = (Broker) broker.clone();
		AccountUser user = null;
		String firstName = null;
		String lastName = "";
		
		String brkName = request.getParameter(BRK_NAME);
		String[] fullName = brkName.split("\\s+");
		firstName = fullName[ZERO];

		if (fullName.length > 1) {
			lastName = fullName[1];
		}

		try {
			user = userService.getLoggedInUser();
			Validate.notNull(user);
			brkObj.setUser(user);
			Broker brokerObj = null;
			
			if(null != request.getAttribute("brokerId")){
				brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String)request.getAttribute("brokerId")));
			}else if (isUserSwitch(request)) {
				brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String) request.getSession().getAttribute(
						SWITCH_TO_MODULE_ID)));
			} else {
				brokerObj = brokerService.findBrokerByUserId(user.getId());
			}
			if(!"Y".equalsIgnoreCase((String)request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)) && !userService.hasUserRole(user, RoleService.AGENCY_MANAGER )   ){
				if(brokerObj != null && brokerObj.getId() != 0 && !BrokerConstants.INCOMPLETE.equals(brokerObj.getCertificationStatus())){
					if(!brokerMgmtUtils.isBrokerCertified(brokerObj.getUser().getId(), false)){
						throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
					}
				}
			}
			if (brokerObj == null) {
				brokerObj = new Broker();
				brokerObj.setUser(user);
			}
			
			if(broker.getLocation()!=null && StringUtils.isEmpty(broker.getLocation().getState())){
				broker.getLocation().setState(state_hidden);
			}
			
			brokerObj.setLocation(broker.getLocation());

			brokerObj.setEducation(brkObj.getEducation());
			brokerObj.setTraining(brkObj.getTraining());
			validateLanguagesSpoken(brkObj);
			brokerObj.setLanguagesSpoken(brkObj.getLanguagesSpoken());
			brokerObj.setProductExpertise(brkObj.getProductExpertise());
			brokerObj.setClientsServed(brkObj.getClientsServed());
			brokerObj.setYourWebSite(brkObj.getYourWebSite());
			brokerObj.setYourPublicEmail(brkObj.getYourPublicEmail());
			
			if(BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
				String newEmailAddress = request.getParameter(EMAIL);
				if(newEmailAddress != null && !newEmailAddress.trim().isEmpty()){
					brokerObj.setYourPublicEmail(newEmailAddress);
				}
			}
			
			//Fetching first 4000 char for About me text;
			String aboutMe = brkObj.getAboutMe();
			int beginIndex = 0;
			int endIndex = aboutMe.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  aboutMe.length();
			aboutMe = aboutMe.substring(beginIndex, endIndex);
					
			brokerObj.setAboutMe(aboutMe);
			
			//Prevent update of firstName and lastName in agency portal HIX-102633
			if(null == brokerObj.getAgencyId()){
				setBrokerFirstNameLastName(firstName, lastName, brokerObj);	
			}

			LOGGER.debug("Broker Profile submit for given ID ::  ");

			validateBrokerUpdateProfile(bindingResult, brokerObj);

			updatedBroker = brokerService.saveBrokerWithLocation(brokerObj);

			getPaymentId(model, request, brokerObj,user);

			request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);
		} catch (InvalidUserException invalidUserException) {
			LOGGER.error("Invalid User Exception occurred : ", invalidUserException);
			throw new GIRuntimeException(invalidUserException, ExceptionUtils.getFullStackTrace(invalidUserException), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		removeSessionAttributes(request);
		request.getSession().setAttribute(UPDATEPROFILE, FALSE);

		model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
		}catch(CloneNotSupportedException cloneNotSupported){
            LOGGER.error("CloneNotSupportedException occured while saving broker edited profile : ", cloneNotSupported);
            throw new GIRuntimeException(cloneNotSupported,ExceptionUtils.getFullStackTrace(cloneNotSupported),Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}catch(GIRuntimeException giexception){
            LOGGER.error("GIRuntimeException occured while saving broker edited profile : ", giexception);
            throw giexception;
		} catch(Exception exception){
            LOGGER.error("Exception occured while saving broker edited profile : ", exception);
            throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}


		LOGGER.info("profilesubmit : END");

		return "redirect:/broker/viewprofile?brokerId="+ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(updatedBroker.getId()));

	}

	private void validateBrokerUpdateProfile(BindingResult bindingResult,
			Broker brokerObj) {
		List<Object> validationGroups = new ArrayList<Object>();
		validationGroups.add(Broker.BrokerProfile.class);
		smartValidator.validate(brokerObj,bindingResult, validationGroups.toArray());

		if(bindingResult.hasErrors()){
			Exception ex=new GIException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			throw new GIRuntimeException(ex , ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

	}

	/**
	 * This method removes attributes  from  session scope.
	 * @param request
	 */
	private void removeSessionAttributes(HttpServletRequest request) {
		request.getSession().removeAttribute(VALIDATE_ADDRESS);
		request.getSession().removeAttribute(LOCATION_TYPE);
		request.getSession().removeAttribute(VALIDATED_BUSINESS_LOCATION);
		request.getSession().removeAttribute(VALIDATED_MAILING_LOCATION);
	}

	/**
	 * This methods adds payment id to model if this is not for NM or MS code.
	 * @param model
	 * @param request
	 * @param brokerObj
	 */
	private PaymentMethods getPaymentId(Model model, HttpServletRequest request, Broker brokerObj, AccountUser user) {
		int paymentId = ZERO;
		PaymentMethods paymentMethodsObj = null;
		if((userService.hasUserRole(user, RoleService.AGENCY_MANAGER) )  || RoleService.AGENCY_MANAGER.equalsIgnoreCase(user.getActiveModuleName())  ){
			model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isAgencyPaymentEnabled());
			model.addAttribute("isAgencyManager", true);
			model.addAttribute("isAdminStaffL2", false);
			AgencyInformationDto response;
			if(isUserSwitch(request)) {
				response =  ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+brokerObj.getAgencyId(),AgencyInformationDto.class);
				
			}else {
				response = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/getAgencyForBroker/"+user.getId(),AgencyInformationDto.class);	
			}
			
			if(null != response){
				model.addAttribute("agency", response);	
			}
			
			if(brokerObj.getUser() == null || brokerObj.getUser().getId() != user.getId()) {
				model.addAttribute("viewOtherBrokerInfo", true);
			}else {
				model.addAttribute("viewSelfBrokerInfo", true);
			}
			
		}else if((userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2) )  || RoleService.APPROVED_ADMIN_STAFF_L2.equalsIgnoreCase(user.getActiveModuleName())){
			model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isAgencyPaymentEnabled());
			model.addAttribute("isAgencyManager", false);
			model.addAttribute("isAdminStaffL2", true);
			AgencyInformationDto response = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/getAgencyForBroker/"+user.getId(),AgencyInformationDto.class);	
			if(null != response){
				model.addAttribute("agency", response);	
			}
			
			if(brokerObj.getUser() == null || brokerObj.getUser().getId() != user.getId()) {
				model.addAttribute("viewOtherBrokerInfo", true);
			}else {
				model.addAttribute("viewSelfBrokerInfo", true);
			}
		}else if (BrokerMgmtUtils.isPaymentEnabled()) {
			LOGGER.info("Payment Info");
			paymentMethodsObj = brokerService.getPaymentMethodDetailsForBroker(brokerObj.getId(),
					PaymentMethods.ModuleName.BROKER);

			if (paymentMethodsObj != null) {
				paymentId = paymentMethodsObj.getId();
			}

			request.getSession().setAttribute(PAYMENT_OBJ_ID, paymentId);
			model.addAttribute(PAYMENT_ID, paymentId);
			model.addAttribute("paymentMethodsObj", paymentMethodsObj);
			model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isPaymentEnabled());
			model.addAttribute("isAgencyManager", false);
			model.addAttribute("isAdminStaffL2", false);
		}
		
		return paymentMethodsObj;
	}

	/**
	 * When request is not for NM and MS states and first-name and last name is not null, broker object is being set with first name and last name.
	 * @param firstName
	 * @param lastName
	 * @param brokerObj
	 */
	private void setBrokerFirstNameLastName(String firstName, String lastName, Broker brokerObj) {
		if (firstName != null && lastName != null) {
			
			if(null == brokerObj.getUser()){
				brokerObj.setFirstName(firstName.trim());
				brokerObj.setLastName(lastName.trim());
			}else{
				brokerObj.getUser().setFirstName(firstName.trim());
				brokerObj.getUser().setLastName(lastName.trim());
			}
			
		}
	}

	/**
	 * Settings broker photo in Broker object
	 *
	 * @param file
	 *            {@link CommonsMultipartFile}
	 * @param brokerObj
	 *            {@link Broker}
	 */
	/*private void setPhoto(CommonsMultipartFile file, Broker brokerObj) {
		byte[] fileContent = null;
		if (file != null && file.getSize() > ZERO) {

			try {
				InputStream inputStream = file.getInputStream();

				fileContent = IOUtils.toByteArray(inputStream);

				if (fileContent.length > ZERO) {
					brokerObj.setPhoto(fileContent);
				}
			} catch (IOException ex) {
				LOGGER.error("IOException occured while uploading the photo for Broker", ex);
			}
			brokerObj.setPhoto(fileContent);
		} else {
			if (brokerObj.getPhoto() != null) {
				fileContent = brokerObj.getPhoto();
				brokerObj.setPhoto(fileContent);
			}
		}
	}*/

	private void checkXSSAttack(Broker broker) {
		/** Check for XSS Attack - Starts */
		broker.setComments(XssHelper.stripXSS(broker.getComments()));
		broker.setAboutMe(XssHelper.stripXSS(broker.getAboutMe()));
		broker.setAlternatePhoneNumber(XssHelper.stripXSS(broker.getAlternatePhoneNumber()));
		broker.setBusinessContactPhoneNumber(XssHelper.stripXSS(broker.getBusinessContactPhoneNumber()));
		broker.setCompanyName(XssHelper.stripXSS(broker.getCompanyName()));
		broker.setContactNumber(XssHelper.stripXSS(broker.getContactNumber()));
		broker.setFaxNumber(XssHelper.stripXSS(broker.getFaxNumber()));
		broker.setLanguagesSpoken(XssHelper.stripXSS(broker.getLanguagesSpoken()));
		broker.setLicenseNumber(XssHelper.stripXSS(broker.getLicenseNumber()));
		broker.setYourWebSite(XssHelper.stripXSS(broker.getYourWebSite()));

		if (broker.getLocation() != null) {
			broker.getLocation().setAddress1(XssHelper.stripXSS(broker.getLocation().getAddress1()));
			broker.getLocation().setAddress2(XssHelper.stripXSS(broker.getLocation().getAddress2()));
			broker.getLocation().setCity(XssHelper.stripXSS(broker.getLocation().getCity()));
			if (broker.getLocation().getZip() != null && broker.getLocation().getZip().trim().length() > ZERO) {
				String zip = XssHelper.stripXSS(String.valueOf(broker.getLocation().getZip()));
				broker.getLocation().setZip(!StringUtils.isEmpty(zip) ? zip : null);
			}
		}
		if (broker.getMailingLocation() != null) {
			broker.getMailingLocation().setAddress1(XssHelper.stripXSS(broker.getMailingLocation().getAddress1()));
			broker.getMailingLocation().setAddress2(XssHelper.stripXSS(broker.getMailingLocation().getAddress2()));
			broker.getMailingLocation().setCity(XssHelper.stripXSS(broker.getMailingLocation().getCity()));
			if (broker.getMailingLocation().getZip() != null
					&& broker.getMailingLocation().getZip().trim().length() > ZERO) {
				String zip = XssHelper.stripXSS(String.valueOf(broker.getMailingLocation().getZip()));
				broker.getMailingLocation().setZip(!StringUtils.isEmpty(zip) ? zip : null);
			}
		}
		/** Check for XSS Attack - Ends */
	}

	/**
	 * To send confirmation email
	 *
	 * @param userId
	 *            User id
	 * @throws MessagingException
	 *             Exception occurred while messaging
	 */
	public void sendConfirmationEmail(int userId) throws MessagingException {

		LOGGER.info("sendConfirmationEmail : START");

		AccountUser userObj = userService.findById(userId);
		brokerConfirmationEmail.setUserObj(userObj);
		Notice noticeObj = null;
		try {
			noticeObj = brokerConfirmationEmail.generateEmail();
			brokerConfirmationEmail.sendEmail(noticeObj);
		} catch (NotificationTypeNotFound e) {
			LOGGER.error("Notification type \"ConfirmationEmail\" not found.Email can not be sent.", e);
		}

		LOGGER.info("sendConfirmationEmail : END");
	}

	/**
	 * To send profile update email
	 *
	 * @param userId
	 *            User id
	 * @throws MessagingException
	 *             Exception occurred while messaging
	 */
	public void sendProfileUpdateEmail(int userId) throws MessagingException {

		LOGGER.info("sendProfileUpdateEmail : START");

		AccountUser userObj = userService.findById(userId);
		brokerProfileUpdateEmail.setUserObj(userObj);
		Notice noticeObj = null;
		try {
			noticeObj = brokerProfileUpdateEmail.generateEmail();

			brokerProfileUpdateEmail.sendEmail(noticeObj);
		} catch (NotificationTypeNotFound e) {
			LOGGER.error("Notification type \"ProfileUpdateEmail\" not found.Email can not be sent.", e);
		}

		LOGGER.info("sendProfileUpdateEmail : END");
	}

	/**
	 * Broker's view profile
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param showconfirmation
	 *            Flag to show profile creation confirmation popup
	 * @return URL for navigation to view profile page
	 */
	@RequestMapping(value = "/broker/viewprofile", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	public String viewprofile(Model model, HttpServletRequest request,
			@RequestParam(value = "showconfirmation", required = false) boolean showconfirmation){

		LOGGER.info("viewprofile : START");

		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: View Broker Profile");
		Broker brokerObj = null;
		try {
			AccountUser user = userService.getLoggedInUser();
			decryptRequestParam(request);
			LOGGER.info("viewprofile - broker Id - attribute -  "+ String.valueOf(request.getAttribute("brokerId")));
			if (isUserSwitch(request)) {
				 if(null != request.getAttribute("brokerId")){
						brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String)request.getAttribute("brokerId")));
					}else if(null != request.getParameter("brokerId")){
						brokerObj = brokerService.getBrokerDetail(Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(request.getParameter("brokerId"))));
					}else {
						brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String) request.getSession().getAttribute(
								SWITCH_TO_MODULE_ID)));
					}
				
			} else if(null != request.getParameter("brokerId")){
				brokerObj = brokerService.getBrokerDetail(Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(request.getParameter("brokerId"))));
			}else if(null != request.getAttribute("brokerId")){
				brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String)request.getAttribute("brokerId")));
			} else {
				brokerObj = brokerService.findBrokerByUserId(user.getId());
			}

			if (brokerObj == null) {
				brokerObj = new Broker();
				brokerObj.setUser(user);
			}
			
			agencyUtils.compareAgencyId(user, brokerObj);
			
			if (brokerObj.getContactNumber() != null) {
				model.addAttribute(PHONE1, brokerObj.getContactNumber().substring(ZERO, THREE));
				model.addAttribute(PHONE2, brokerObj.getContactNumber().substring(FOUR, SEVEN));
				model.addAttribute(PHONE3, brokerObj.getContactNumber().substring(EIGHT, TWELVE));
			}

			model.addAttribute(BROKER_MODULE, brokerObj);
			model.addAttribute(LOCATION_OBJ, brokerObj.getLocation());
			model.addAttribute(MAILING_LOCATION_OBJ, brokerObj.getMailingLocation());
			model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
			model.addAttribute("ca_statecode", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));

			if (!StringUtils.isBlank(request.getParameter(CANCEL))
					&& request.getParameter(CANCEL).trim().equalsIgnoreCase(TRUE2)) {
				model.addAttribute(UPDATEPROFILE, false);
			}

			getPaymentId(model, request, brokerObj,user);
			
			//Shuffled code to prevent change in attached object.HIX-102613
			// adds space after comma for clients served and product experties
			String clientsServed = brokerObj.getClientsServed();
			clientsServed= addSpaceToField(clientsServed);
			brokerObj.setClientsServed(clientsServed);

			String productExperties = brokerObj.getProductExpertise();
			productExperties = addSpaceToField(productExperties);
			brokerObj.setProductExpertise(productExperties);
			
			if(null == brokerObj.getUser()){
				//This condition is for newly created brokers (through agency) which doesn't have user account
				AccountUser acUser= new AccountUser();
				acUser.setFirstName(brokerObj.getFirstName());
				acUser.setLastName(brokerObj.getLastName());
				brokerObj.setUser(acUser);
			}

			request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);

			if (showconfirmation) {
				request.getSession().setAttribute(UPDATEPROFILE, TRUE2);
			} else {
				request.getSession().setAttribute(UPDATEPROFILE, FALSE);
			}
		} catch (InvalidUserException ex) {
			request.getSession().setAttribute(UPDATEPROFILE, FALSE);
			LOGGER.error(INVALID_USER_EXCEPTION_OCCURRED, ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}catch(GIRuntimeException giexception){
            LOGGER.error("GIRuntimeException occured while viewing Brokers profile : ", giexception);
            throw giexception;
		} catch(Exception exception){
            LOGGER.error("Exception occured while viewing Brokers profile  : ", exception);
            throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
     }


		LOGGER.info("viewprofile : END");

		return BROKER_VIEW_PROFILE;

	}

	/**
	 * This method takes comma seperated string and adds space after each comma.
	 * @param productExperties
	 * @return
	 */
	private String addSpaceToField(String field) {
		if (field != null) {
			List<String> fieldList = EntityUtils.splitStringValues(field, ",");
			StringBuilder fieldStrBuffer = new StringBuilder();
			for (String productExpertiesListStr : fieldList) {
				fieldStrBuffer.append(productExpertiesListStr);
				fieldStrBuffer.append(",");
				fieldStrBuffer.append(" ");
			}
			return fieldStrBuffer.substring(ZERO, fieldStrBuffer.length() - 2);
		}
		return field;
	}

	/**
	 * To view edit profile page
	 *
	 * @param model
	 *            {@link Model}
	 * @return URL to show edit profile page
	 */
	@RequestMapping(value = "/broker/viewprofile", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	public String profile(Model model) {

		LOGGER.info("profile : START");

		LOGGER.info("View Profile ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: View Agent Profile");

		LOGGER.info("profile : END");

		return "broker/editprofile";
	}

	/**
	 * To retrieve broker profile photo
	 *
	 * @return Broker profile photo
	 */
	@RequestMapping(value = "/broker/photo", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	@ResponseBody
	public byte[] downloadPhoto(HttpServletRequest request, HttpServletResponse response) {

		LOGGER.info("downloadPhoto : START");
		String contentType;
		byte[] content = null;
		BrokerPhoto brokerPhoto = null;
		ByteArrayInputStream is = null;
		ByteArrayOutputStream byteArrayOutputStream = null;

		try {

			AccountUser user = userService.getLoggedInUser();
			brokerPhoto = brokerService.getBrokerPhotoByUserId(user.getId());

				if(brokerPhoto!=null && brokerPhoto.getPhoto()!=null && !EntityUtils.isEmpty(brokerPhoto.getMimeType())){
						is = new ByteArrayInputStream(brokerPhoto.getPhoto());
						BufferedImage bufferedImage = ImageIO.read(is);
						byteArrayOutputStream = new ByteArrayOutputStream();

						contentType = EntityUtils.getLastElement(brokerPhoto.getMimeType(), BrokerConstants.SLASH);

						if(EntityUtils.checkFileExtension(contentType)){
							ImageIO.write(bufferedImage, contentType, byteArrayOutputStream);
				            content = byteArrayOutputStream.toByteArray();
						}

				response.setContentType(brokerPhoto.getMimeType());
			}
		} catch (InvalidUserException ex) {
			LOGGER.error(INVALID_USER_EXCEPTION_OCCURRED, ex);
		} catch (IOException e) {
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			IOUtils.closeQuietly(is);
			IOUtils.closeQuietly(byteArrayOutputStream);
		}

		LOGGER.info("downloadPhoto : END");

		  return brokerPhoto !=  null ? brokerPhoto.getPhoto() : null;

	}


	/**
	 * Saves broker's build profile while broker registration flow
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param broker
	 *            {@link Broker}
	 * @param file
	 *            {@link CommonsMultipartFile}
	 * @param redirectAttrs
	 *            {@link RedirectAttributes}
	 * @return URL for navigation to payment information page for CA and
	 *         certification application page for NM
	 * @throws CloneNotSupportedException
	 */
	@RequestMapping(value = "/broker/buildprofile", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_ADD_BROKER)
	public String submitBuildProfile(Model model, HttpServletRequest request,
			@ModelAttribute(BROKER_MODULE) Broker broker,BindingResult bindingResult,
			RedirectAttributes redirectAttrs) {
		LOGGER.info("submitBuildProfile : START");
		AccountUser user ;
		int paymentId = ZERO;
		Broker brokerObj = null;
		String refParam="";
		try{
		decryptRequestParam(request);
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Build Your Profile");

		/** Check for XSS Attack - Starts */
		checkXssAttack(broker);
		/** Check for XSS Attack - Ends */

		Broker brkObj = (Broker) broker.clone();

		request.getSession().setAttribute(UPDATEPROFILE, FALSE);

		try {
			user = userService.getLoggedInUser();
			if(null == request.getAttribute("brokerId")){
				brkObj.setUser(user);
			}
			

			if(null != request.getAttribute("brokerId")){
				brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String)request.getAttribute("brokerId")));
			}else if (request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW) != null
                    && request.getSession().getAttribute(SWITCH_TO_MODULE_ID) != null
                    && (  (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)).equalsIgnoreCase("Y")) {
				 
					brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String) request.getSession().getAttribute(SWITCH_TO_MODULE_ID)));	
				
            }else {
					brokerObj = brokerService.findBrokerByUserId(user.getId());
			}

			if (brokerObj == null) {
				brokerObj = new Broker();
				brokerObj.setUser(user);
			}
			validateLanguagesSpoken(brkObj);

			validateBrokerRegSecondStep(bindingResult, brkObj);

			populateAndSaveBroker(brokerObj, brkObj);

			if(userService.hasUserRole(user, RoleService.AGENCY_MANAGER)  && null == request.getAttribute("brokerId")){
				LOGGER.info("Agency flow submitting profile page");
				String agencyId = String.valueOf(agencyUtils.retrieveAgencyMgrForBrokerId(user));
				request.getSession().setAttribute(UPDATEPROFILE, FALSE);
				return "redirect:/agency#/documentupload";
			}else{
				if(  (  (userService.hasUserRole(user, RoleService.AGENCY_MANAGER) || RoleService.AGENCY_MANAGER.equalsIgnoreCase(user.getActiveModuleName()) ) && !BrokerMgmtUtils.isAgencyPaymentEnabled())|| !BrokerMgmtUtils.isPaymentEnabled() ){
					LOGGER.info(IGNORING_PAYMENT_INFO_CURRENTLY);
					request.getSession().setAttribute(UPDATEPROFILE, FALSE);
					// Condition to verify whether broker exists in Brokers
					// table
					// then change Broker status to pending
					brokerObj.setStatusChangeDate(new TSDate());
					brokerObj.setLastUpdatedBy(userService.getLoggedInUser().getId());
					brokerService.saveBrokerCertification(brokerObj, null);
					request.getSession().setAttribute(PAYMENT_OBJ_ID, ZERO);
					request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);


					redirectAttrs.addFlashAttribute("isFinancialInfoAdded", Boolean.FALSE);

					request.getSession().setAttribute(UPDATEPROFILE, TRUE2);
					LOGGER.info("submitBuildProfile : END");
					if(null != request.getAttribute("brokerId")){
						Map<String,String> paramMap= new HashMap<String,String>(3);
				        paramMap.put("brokerId", (String) request.getAttribute("brokerId") );
				        String encryptedValue = GhixAESCipherPool.encryptParameterMap(paramMap)  ;
						refParam = "&ref="+encryptedValue;	
					}
					return "redirect:/broker/certificationstatus?showconfirmation=true"+refParam;
				}else{
					getPaymentId(model, request, brokerObj,user);
				}
				
			}

			request.getSession().setAttribute(PAYMENT_OBJ_ID, paymentId);
			request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);

			model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isPaymentEnabled());
		} catch (Exception ex) {
			LOGGER.error("Exception occured while submitting the Agent's build profile page", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);

		}

		// This condition is added so that page is redirected in case on any exceptions.
		if(userService.hasUserRole(user, RoleService.BROKER_ROLE)){
			if(BrokerMgmtUtils.isPaymentEnabled()){
				return "redirect:/broker/paymentinfo";
			}else {
				request.getSession().setAttribute(UPDATEPROFILE, FALSE);
				// Condition to verify whether broker exists in Brokers table
				// then change Broker status to pending
				if (brokerObj != null) {
					brokerObj.setStatusChangeDate(new TSDate());
					brokerService.saveBrokerCertification(brokerObj, null);
					request.getSession().setAttribute(PAYMENT_OBJ_ID, ZERO);
					request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);
				}
				redirectAttrs.addFlashAttribute("isFinancialInfoAdded", Boolean.FALSE);
				request.getSession().setAttribute(UPDATEPROFILE, TRUE2);
			}
		}
		
		}catch(CloneNotSupportedException clonenotSupportedException){
            LOGGER.error("CloneNotSupportedException occured while saving build profile in submitBuildProfile : ", clonenotSupportedException);
            throw new GIRuntimeException(clonenotSupportedException, ExceptionUtils.getFullStackTrace(clonenotSupportedException), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}catch(GIRuntimeException giruntimeexception){
            LOGGER.error("Exception occured while saving build profile in submitBuildProfile : ", giruntimeexception);
            throw giruntimeexception;
		}catch(Exception ex)
		{
			LOGGER.error("Exception occured while saving build profile : ", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("submitBuildProfile : END");
		if(null != request.getAttribute("brokerId")){
			Map<String,String> paramMap= new HashMap<String,String>(3);
	        paramMap.put("brokerId", (String) request.getAttribute("brokerId") );
	        String encryptedValue = GhixAESCipherPool.encryptParameterMap(paramMap)  ;
			refParam = "&ref="+encryptedValue;	
		}
		
		return "redirect:/broker/certificationapplication?showconfirmation=true"+refParam;
	}

	private void validateBrokerRegSecondStep(BindingResult bindingResult,
			Broker brkObj) throws Exception {
		List<Object> validationGroups = new ArrayList<Object>();
		validationGroups.add(Broker.BrokerProfile.class);
		smartValidator.validate(brkObj, bindingResult, validationGroups.toArray());
		if(bindingResult.hasErrors()){
			throw new Exception(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
	}

	/**
	 * This method updates broker object fetched from DB with values submitted by user.
	 * This updated object again saved to database.
	 * @param brokerObj
	 * @param brkObj
	 */
	private void populateAndSaveBroker(Broker brokerObj, Broker brkObj) {
		brokerObj.setClientsServed(brkObj.getClientsServed());
		brokerObj.setLanguagesSpoken(brkObj.getLanguagesSpoken());
		brokerObj.setProductExpertise(brkObj.getProductExpertise());
		brokerObj.setEducation(brkObj.getEducation());
		//Fetching first 4000 char for Aomment text;
		String aboutMe = brkObj.getAboutMe();
		int beginIndex = 0;
		int endIndex = aboutMe.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  aboutMe.length();
		aboutMe = aboutMe.substring(beginIndex, endIndex);
				
		brokerObj.setAboutMe(aboutMe);
		brokerObj.setYourWebSite(brkObj.getYourWebSite());
		brokerObj.setYourPublicEmail(brkObj.getYourPublicEmail());
		//This new column has been added for agency flow. 
		//This is being called while Creating broker, Creating new broker through Agency, Creating Agency Managers broker,
		//Perticularlt its being called while saving Profile information of broker.
		if(null == brokerObj.getStatus() && brokerObj.getAgencyId()!= null){
			brokerObj.setStatus("Active");	
		}
		
		brokerService.saveBrokerWithLocation(brokerObj);
	}

	/**
	 * This method checks for XSS Attack.
	 * It takes broker object populated from user-site, replaces values if it contains any executable statements(escaping).
	 * This broker object then used to clone and new broker object is being saved to DB
	 * @param broker
	 */
	protected void checkXssAttack(Broker broker) {
		broker.setComments(XssHelper.stripXSS(broker.getComments()));
		broker.setAboutMe(XssHelper.stripXSS(broker.getAboutMe()));
		broker.setAlternatePhoneNumber(XssHelper.stripXSS(broker.getAlternatePhoneNumber()));
		broker.setBusinessContactPhoneNumber(XssHelper.stripXSS(broker.getBusinessContactPhoneNumber()));
		broker.setCompanyName(XssHelper.stripXSS(broker.getCompanyName()));
		broker.setContactNumber(XssHelper.stripXSS(broker.getContactNumber()));
		broker.setFaxNumber(XssHelper.stripXSS(broker.getFaxNumber()));
		broker.setLanguagesSpoken(XssHelper.stripXSS(broker.getLanguagesSpoken()));
		broker.setLicenseNumber(XssHelper.stripXSS(broker.getLicenseNumber()));
		broker.setYourWebSite(XssHelper.stripXSS(broker.getYourWebSite()));
		broker.setYourPublicEmail(XssHelper.stripXSS(broker.getYourPublicEmail()));

		checkLocationXssAttack(broker);

		checkMailingLocationXssAttack(broker);
	}

	/**
	 * This method checks and removes XSS attack in brokers mailing location object.
	 * @param broker
	 */
	private void checkMailingLocationXssAttack(Broker broker) {
		if (broker.getMailingLocation() != null) {
			broker.getMailingLocation().setAddress1(XssHelper.stripXSS(broker.getMailingLocation().getAddress1()));
			broker.getMailingLocation().setAddress2(XssHelper.stripXSS(broker.getMailingLocation().getAddress2()));
			broker.getMailingLocation().setCity(XssHelper.stripXSS(broker.getMailingLocation().getCity()));

			if (broker.getMailingLocation().getZip() != null
					&& broker.getMailingLocation().getZip().trim().length() > ZERO) {
				String zip = XssHelper.stripXSS(String.valueOf(broker.getMailingLocation().getZip()));
				broker.getMailingLocation().setZip(!zip.isEmpty() ? zip : null);
			}

			if((broker.getMailingLocation().getLat()==0.0)&&(broker.getMailingLocation().getLon()==0.0)){
				ZipCode zipCode=zipCodeService.findByZipCode(broker.getMailingLocation().getZip().toString());
				if(zipCode!=null){
					broker.getMailingLocation().setLat(zipCode.getLat());
					broker.getMailingLocation().setLon(zipCode.getLon());
				}
			}

		}
	}

	/**
	 * This method check and removes Xss attack in broker location object.
	 * @param broker
	 */
	private void checkLocationXssAttack(Broker broker) {
		if (broker.getLocation() != null) {
			broker.getLocation().setAddress1(XssHelper.stripXSS(broker.getLocation().getAddress1()));
			broker.getLocation().setAddress2(XssHelper.stripXSS(broker.getLocation().getAddress2()));
			broker.getLocation().setCity(XssHelper.stripXSS(broker.getLocation().getCity()));

			if (broker.getLocation().getZip() != null && broker.getLocation().getZip().trim().length() > ZERO) {
				String zip = XssHelper.stripXSS(String.valueOf(broker.getLocation().getZip()));
				broker.getLocation().setZip(!zip.isEmpty() ? zip : null);
				setLatLong(broker);
			}
		}
	}

	private void setLatLong(Broker broker) {
				if((broker.getLocation().getLat()==0.0)&&(broker.getLocation().getLon()==0.0)){
					ZipCode zipCode=zipCodeService.findByZipCode(broker.getLocation().getZip().toString());
					if(zipCode!=null){
						broker.getLocation().setLat(zipCode.getLat());
						broker.getLocation().setLon(zipCode.getLon());
					}
				}
	}

	/**
	 * To show update profile page
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to update profile page
	 */
	@RequestMapping(value = "/broker/profileupdated", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_ADD_BROKER)
	public String profileupdated(Model model) {

		LOGGER.info("profileupdated : START");
		model.addAttribute("exchangeName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)); 
		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
		AccountUser user;
		try {
			user = userService.getLoggedInUser();
			if(userService.hasUserRole(user, RoleService.AGENCY_MANAGER)){
				model.addAttribute("isAgencyManager", true);
			}else {
				model.addAttribute("isAgencyManager", false);
			}
		} catch (InvalidUserException e) {
			model.addAttribute("isAgencyManager", false);
			e.printStackTrace();
		}
		
		LOGGER.info("profileupdated : END");

		return "broker/profileupdated";

	}

	/**
	 * Validate spoken languages
	 *
	 * @param brkObj
	 *            {@link Broker}
	 */
	private void validateLanguagesSpoken(Broker brkObj) {

		LOGGER.info("validateLanguagesSpoken : START");

		if (brkObj.getLanguagesSpoken() != null && !brkObj.getLanguagesSpoken().isEmpty()) {
			String ls = brkObj.getLanguagesSpoken();
			ls = ls.trim();
			if (ls.endsWith(",")) {
				ls = ls.substring(ZERO, ls.length() - 1);
				brkObj.setLanguagesSpoken(ls);
			}

			List<String> items = null;
			items = Arrays.asList(brkObj.getLanguagesSpoken().split(","));
			items.removeAll(Collections.singletonList(null));

			List<String> newList = new ArrayList<String>();
			for (Iterator<String> iter = items.iterator(); iter.hasNext();) {
				String element = iter.next();
				if (!newList.contains(element.trim())) {
					newList.add(element.trim());
				}
			}

			String newString = "";
			for (Iterator<String> it = newList.iterator(); it.hasNext();) {
				newString += it.next();
				if (it.hasNext()) {
					newString += ", ";
				}
			}
			brkObj.setLanguagesSpoken(newString);
		}

		LOGGER.info("validateLanguagesSpoken : END");
	}

	/**
	 * To display build your profile page to Broker while registration
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param broker
	 *            {@link Broker}
	 * @return URL for navigation to build profile page
	 * @throws CloneNotSupportedException
	 */
	@RequestMapping(value = "/broker/buildprofile", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	public String buildProfile(Model model, HttpServletRequest request, @ModelAttribute(BROKER_MODULE) Broker broker) {

		LOGGER.info("buildProfile : START");
		decryptRequestParam(request);
		try{
		Broker brkObj = (Broker) broker.clone();
		AccountUser user = new AccountUser();
		Broker brokerObj = null;
		Broker brokerDetailObj;


			if(request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW) != null)
			{
				if("Y".equalsIgnoreCase((String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)) &&
						request.getSession().getAttribute(SWITCH_TO_MODULE_ID) != null)
				{
					if(null != request.getAttribute("brokerId")){
						//This is a case where agency-manager creates broker account.
						brokerDetailObj = brokerService.getBrokerDetail( Integer.parseInt((String)request.getAttribute("brokerId")));
					}else {
						brokerDetailObj = brokerService.getBrokerDetail(Integer.parseInt(request.getSession().getAttribute(SWITCH_TO_MODULE_ID).toString()));
					}
						
						if(brokerDetailObj != null) {
							user = brokerDetailObj.getUser();
						} else {
							throw new InvalidUserException("Invalid switchToModuleId provided.");
						}
				}
			} else {
				user = userService.getLoggedInUser();
			}
			
			if(null != request.getAttribute("brokerId")){
				//This is a case where agency-manager creates broker account.
				brokerObj = brokerService.getBrokerDetail( Integer.parseInt((String)request.getAttribute("brokerId")));
			}else{
				brkObj.setUser(user);
				brokerObj = brokerService.findBrokerByUserId(user.getId());	
			}
			

			if (brokerObj == null) {
				brokerObj = new Broker();
				brokerObj.setUser(user);
			}

		model.addAttribute(LANGUAGES_LIST, loadLanguagesData());
		model.addAttribute(BROKER_MODULE, brokerObj);

		int paymentId = ZERO;

		if(BrokerMgmtUtils.isPaymentEnabled()) {
			PaymentMethods paymentMethodsObj = brokerService.getPaymentMethodDetailsForBroker(brokerObj.getId(),
					PaymentMethods.ModuleName.BROKER);

			if (paymentMethodsObj != null) {
				paymentId = paymentMethodsObj.getId();
			}

			request.getSession().setAttribute(PAYMENT_OBJ_ID, paymentId);
		}

		model.addAttribute(PAYMENT_ID, paymentId);
		request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);

		
		model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isPaymentEnabled());
		model.addAttribute(STATE_EXCHANGE_TYPE,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE));

		model.addAttribute(EDUCATION_LIST, loadEducationData());
		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
		
		if(userService.hasUserRole(user, RoleService.AGENCY_MANAGER) || RoleService.AGENCY_MANAGER.equalsIgnoreCase( userService.getLoggedInUser().getActiveModuleName())){
			model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isAgencyPaymentEnabled());
			model.addAttribute("isAgencyManager", true);
			model.addAttribute("activePage", "agentProfile"); 
			model.addAttribute("showOKicon", true);
			
			//If Agency Manager has status other than InComplete OR Certified .. then redirect to below URL.
			
			String jsonStr=ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+ brokerObj.getAgencyId() , String.class);
			ObjectReader objRd=JacksonUtils.getJacksonObjectReaderForJavaType(AgencyInformationDto.class);
			AgencyInformationDto agencyInfo=objRd.readValue(jsonStr);
			if(agencyInfo.getCertificationStatus()!=null && !( "CERTIFIED".equalsIgnoreCase(agencyInfo.getCertificationStatus().getValue())  || "INCOMPLETE".equalsIgnoreCase(agencyInfo.getCertificationStatus().getValue())  ) ){
				String refParam="";
				if(null != request.getAttribute("brokerId")){
					Map<String,String> paramMap= new HashMap<String,String>(3);
			        paramMap.put("brokerId", (String) request.getAttribute("brokerId") );
			        String encryptedValue = GhixAESCipherPool.encryptParameterMap(paramMap)  ;
					refParam = "?ref="+encryptedValue;	
				}
				return  "redirect:/broker/viewprofile"+refParam;
					
			}
			
			
		}else if(userService.hasUserRole(user, RoleService.BROKER_ROLE)){
			model.addAttribute("isAgencyManager", false);
		}
		
		}catch(InvalidUserException invalidUserException)
		{
			LOGGER.error("InvalidUserException occured while displaying profile page : ", invalidUserException);
			throw new GIRuntimeException(invalidUserException, ExceptionUtils.getFullStackTrace(invalidUserException), Severity.HIGH,
					GIRuntimeException.Component.AEE.toString(), null);

		}catch(CloneNotSupportedException cloneNotSupported)
		{
			LOGGER.error("CloneNotSupportedException occured while displaying profile page : ", cloneNotSupported);
			throw new GIRuntimeException(cloneNotSupported, ExceptionUtils.getFullStackTrace(cloneNotSupported), Severity.HIGH,GIRuntimeException.Component.AEE.toString(), null);
		}catch(GIRuntimeException giruntimeexception){
            LOGGER.error("GIRuntimeException occured while saving build profile in buildProfile method : ", giruntimeexception);
            throw giruntimeexception;
		}
		catch(Exception ex){
			LOGGER.error("Exception occured while displaying profile page : ", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH,GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("buildProfile : END");

		return "broker/buildprofile";

	}
	
	private void decryptRequestParam(HttpServletRequest request) {
		if(null !=request.getParameter("ref")){
			String reference = request.getParameter("ref");
			LOGGER.info("decryptRequestParam :  ref : "+ reference);
			Map<String, String> encParams = GhixAESCipherPool.decryptParameterMap(reference);
		    for(Entry<String, String> entrySet:encParams.entrySet()){
			  request.setAttribute( entrySet.getKey(), entrySet.getValue()); 
			  LOGGER.info("decryptRequestParam :  key : "+  entrySet.getKey() +", value : "+entrySet.getValue());
		    }
		}
	}

	/**
	 * Method to load Agent's Education Data.
	 *
	 * @return educationDataMap The Agent's education code,label Map.
	 */
	private Map<String, String> loadEducationData() {
		LOGGER.info("loadEducationData : START");

		List<LookupValue> educationLVList = null;
		Map<String, String> educationDataMap = null;

		try {
			educationLVList = lookupService.getLookupValueList("AGENT_EDUCATION");

			if(null == educationLVList || educationLVList.isEmpty()) {
				LOGGER.warn("No education Lookup value data found in repository.");
			}
			else {
				educationDataMap = new HashMap<String, String>();

				for(LookupValue educationLV: educationLVList) {
					educationDataMap.put(educationLV.getLookupValueCode(), educationLV.getLookupValueLabel());
				}
			}
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred while loading Agent's education data.", ex);
		}
		finally {
			LOGGER.info("loadEducationData : END");
		}

		return educationDataMap;
	}

	/**
	 * Handles the view payment information of Broker
	 *
	 * @param model
	 * @param request
	 * @param paymentId
	 * @return URL for navigation to appropriate manage Broker page
	 */
	@RequestMapping(value = "/broker/viewpaymentinfo", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_PAYMENTINFO')")
	public String viewpaymentinfo(Model model, HttpServletRequest request) {

		LOGGER.info("viewpaymentinfo : START");

		PaymentMethods paymentMethodsObj = null;

		if (BrokerMgmtUtils.isPaymentEnabled()) {
			LOGGER.info("View Broker Payment Info ");
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: View Broker Payment Info");
			Broker brokerObj = new Broker();
			try {
				AccountUser user = userService.getLoggedInUser();
				if (isUserSwitch(request)) {
 	 	 	 		brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String) request.getSession().getAttribute(
 	 	 	 				SWITCH_TO_MODULE_ID)));
 	 	 	 	} else {
 	 	 	 		brokerObj = brokerService.findBrokerByUserId(user.getId());
 	 	 	 	}
				if (brokerObj == null) {
					brokerObj = new Broker();
					brokerObj.setUser(user);
				}

				model.addAttribute(BROKER_MODULE, brokerObj);
				if (!StringUtils.isBlank(request.getParameter(CANCEL))
						&& request.getParameter(CANCEL).trim().equalsIgnoreCase(TRUE2)) {
					model.addAttribute(UPDATEPROFILE, false);
				}
				request.getSession().setAttribute(UPDATEPROFILE, FALSE);
				paymentMethodsObj  = getPaymentId(model, request, brokerObj,user);
				model.addAttribute(LIST_OF_STATES, brokerMgmtUtils.filterStates());
				request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);
				model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isPaymentEnabled());
			} catch (InvalidUserException ex) {
				LOGGER.error(INVALID_USER_EXCEPTION_OCCURRED, ex);
			}
			if (paymentMethodsObj != null) {
				model.addAttribute(PAYMENT_METHODS_OBJ, paymentMethodsObj);
				LOGGER.info("viewpaymentinfo: END");
				return "broker/viewpaymentinfo";
			} else {
				LOGGER.info("viewpaymentinfo: END");
				return "broker/paymentinfo";
			}
		}
		else {
			LOGGER.info(IGNORING_PAYMENT_INFO_CURRENTLY);
			LOGGER.info("viewpaymentinfo: END");
			return BROKER_VIEW_PROFILE;
		}
	}

	/**
	 * Handles the Submit payment information of Broker
	 *
	 * @param paymentMethod
	 * @param request
	 * @param redirectUrl
	 * @param paymentId
	 * @return URL for navigation to appropriate manage Broker page
	 */
	@RequestMapping(value = "/broker/paymentinfo", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'BROKER_EDIT_PAYMENTINFO')")
	public String saveBrokerPaymentInfo(Model model, @ModelAttribute(PAYMENT_METHODS_OBJ) PaymentMethods paymentMethod,
			@RequestParam(value = "paymentId", required = false) Integer paymentId,
			@RequestParam(value = "redirectTo", required = false) String redirectUrl,
			@ModelAttribute(BROKER_MODULE) Broker broker, HttpServletRequest request, RedirectAttributes redirectAttrs) {

		LOGGER.info("saveBrokerPaymentInfo : START");
		String redirectToUrl = null;

		if (BrokerMgmtUtils.isPaymentEnabled()) {
			LOGGER.info("New Broker Payment Info");
			request.getSession().setAttribute(UPDATEPROFILE, FALSE);
			//boolean isFinancialInfoAdded = false;
			BankInfo bankInfo = null;
			FinancialInfo financialInfo = null;
			PaymentMethods paymentMethodsObj = null;
			PaymentMethods paymentMethods=null;
			try {
				Broker brkObj = (Broker) broker.clone();
				AccountUser user = userService.getLoggedInUser();
				brkObj.setUser(user);
				Broker brokerObj = null;
				if (isUserSwitch(request)) {
					brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String) request.getSession().getAttribute(
							SWITCH_TO_MODULE_ID)));
				}else{
					brokerObj = brokerService.findBrokerByUserId(user.getId());
				}
				if (brokerObj == null) {
					brokerObj = new Broker();
					brokerObj.setUser(user);
				}

				// get the PaymentMethods, FinancialInfo, BankInfo which is
				// already
				// exists, if not exists, create one.
				 paymentMethodsObj = getPaymentMethod(brokerObj);

				financialInfo = getFinancialInfo(paymentMethodsObj);

				bankInfo = getBankInfo(financialInfo);

				populateFinancialInfo(paymentMethod, bankInfo, financialInfo, brokerObj);

				if(null != financialInfo) {
					if(null == financialInfo.getFirstName() || financialInfo.getFirstName().trim().isEmpty()) {
						financialInfo.setFirstName(user.getFirstName());
					}

					if(null == financialInfo.getLastName() || financialInfo.getLastName().trim().isEmpty()) {
						financialInfo.setLastName(user.getLastName());
					}

					if(null == financialInfo.getEmail() || financialInfo.getEmail().trim().isEmpty()) {
						financialInfo.setEmail(user.getEmail());
					}

					if(null == financialInfo.getContactNumber() || financialInfo.getContactNumber().isEmpty()) {
						financialInfo.setContactNumber(user.getPhone());
					}
				}

				// setting the BankInfo object to the FinancialInfo object
				financialInfo.setBankInfo(bankInfo);

				// setting the FinancialInfo object to the PaymentMethods object
				paymentMethodsObj.setFinancialInfo(financialInfo);

				paymentMethodsObj.setPaymentType(paymentMethod.getPaymentType());

				if(null == paymentMethod.getPaymentType()) {
					paymentMethodsObj.setPaymentMethodName(StringUtils.EMPTY);
				}
				else {
					paymentMethodsObj.setPaymentMethodName(paymentMethod.getPaymentType().toString());
				}

				//Moved this in method getPaymentMethod
				/*if (paymentMethodsObj.getStatus() == null) {
					paymentMethodsObj.setStatus(PaymentStatus.Active);
				}*/

				paymentMethodsObj.setIsDefault(PaymentMethods.PaymentIsDefault.Y);
				paymentMethodsObj.setModuleId(brokerObj.getId());
				paymentMethodsObj.setModuleName(PaymentMethods.ModuleName.BROKER);

				LOGGER.info("Retrieving Save Payment Detail REST Call Starts");
 	 	 	 	String paymentMethodsStr = restClassCommunicator.saveBrokerPaymentDetails(paymentMethodsObj);
 	 	 	 	LOGGER.info("Retrieving Save Payment Detail REST Call Ends");

 	 	 	 	PaymentMethodResponse paymentMethodResponse = (PaymentMethodResponse) XSTREAM.fromXML(paymentMethodsStr);

 	 	 	 	if(paymentMethodResponse.getStatus().equals(GhixConstants.RESPONSE_SUCCESS)){
 	 	 	 	paymentMethods = paymentMethodResponse.getPaymentMethods();
 	 	 	 	}else{
 	 	 	 	String paymentErrorMsg = paymentMethodResponse.getErrMsg();
 	 	 	 	request.getSession().setAttribute("paymentErrorMsg", paymentErrorMsg);
 	 	 	 	LOGGER.error("Error while saving  Broker Payment Details");
 	 	 	 	return  "redirect:/broker/paymentinfo?paymentInfoValidated=true";
 	 	 	 	//need to throw exception.
 	 	 	 	}
				// Called to update the status from Incomplete to Pending
				// saving the PaymentMethods info into the payment_methods table
				LOGGER.info("Financial Information is saved");
				//isFinancialInfoAdded = (paymentMethods != null) ? true : false;
				//paymentId = paymentMethods.getId();
				if (paymentMethods != null) { // If condition (isFinancialInfoAdded is removed to reduce cyclomatic complexity)
					// added to verify whether broker exists in Brokers table and paymentInfo is successfully added
					brokerObj.setStatusChangeDate(new TSDate());
					brokerService.saveBrokerCertification(brokerObj, paymentMethods);// (isFinancialInfoAdded is removed to reduce cyclomatic complexity)
					request.getSession().setAttribute(PAYMENT_OBJ_ID, paymentMethodsObj.getId());
					request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);
				}
				model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isPaymentEnabled());
			}
			catch (Exception ex) {
				LOGGER.error("Financial Information is invalid", ex);
				request.getSession().setAttribute(UPDATEPROFILE, FALSE);
			}

			redirectAttrs.addFlashAttribute("isFinancialInfoAdded", paymentMethods != null);

			redirectToUrl = getRedirectURL(redirectUrl, request);
		}
		else {
			LOGGER.info("saveBrokerPaymentInfo : END");
			redirectToUrl =  BROKER_VIEW_PROFILE;
		}

		LOGGER.info("saveBrokerPaymentInfo : END");

		return redirectToUrl;
	}

	/**
	 * This method returns redirect url depending on the contains of argument redirectUrl
	 * @param redirectUrl
	 * @param request
	 * @return redirectToUrl
	 */
	private String getRedirectURL(String redirectUrl,HttpServletRequest request) {
		String redirectToUrl=null;
		if ("addpaymentinfo".equalsIgnoreCase(redirectUrl)) {
			request.getSession().setAttribute(UPDATEPROFILE, TRUE2);
			redirectToUrl = "redirect:/broker/certificationapplication?showconfirmation=true";
		} else if ("editpaymentinfo".equalsIgnoreCase(redirectUrl)) {
			redirectToUrl = "redirect:/broker/viewpaymentinfo";
		}
		return redirectToUrl;
	}

	/**
	 * This method populates financial info depending upon payment type.
	 * @param paymentMethod
	 * @param bankInfo
	 * @param financialInfo
	 */
	private void populateFinancialInfo(PaymentMethods paymentMethod,
			BankInfo bankInfo, FinancialInfo financialInfo, Broker brokerObj) {
		if (PaymentMethods.PaymentType.CHECK.equals(paymentMethod.getPaymentType())) {
			financialInfo.setPaymentType(com.getinsured.hix.model.FinancialInfo.PaymentType.CHECK);

			// adding the payment address related information to the
			// financialInfo object
			financialInfo.setAddress1(paymentMethod.getFinancialInfo().getAddress1());
			financialInfo.setAddress2(paymentMethod.getFinancialInfo().getAddress2());
			financialInfo.setCity(paymentMethod.getFinancialInfo().getCity());
			financialInfo.setState(paymentMethod.getFinancialInfo().getState());
			financialInfo.setZipcode(paymentMethod.getFinancialInfo().getZipcode());

			// removing the bank related information from the BankInfo
			// object
			bankInfo.setAccountNumber(null);
			bankInfo.setBankName(null);
			bankInfo.setRoutingNumber(null);
			bankInfo.setAccountType(null);
			bankInfo.setNameOnAccount(null);
			paymentMethod.setPaymentType(PaymentMethods.PaymentType.CHECK);
	 	 	paymentMethod.setPaymentMethodName(PaymentMethods.PaymentType.CHECK.toString());
		} else {

			// Changing EFT to ACH as per HIX-HIX-13866HIX-13866
			financialInfo.setPaymentType(com.getinsured.hix.model.FinancialInfo.PaymentType.ACH);

			// adding the bank related information to the BankInfo
			// object
			String accountNumber = paymentMethod.getFinancialInfo().getBankInfo().getAccountNumber();
			if(accountNumber != null && !accountNumber.equalsIgnoreCase(bankInfo.getAccountNumber())){
				bankInfo.setAccountNumber(paymentMethod.getFinancialInfo().getBankInfo().getAccountNumber());
			}

			bankInfo.setBankName(paymentMethod.getFinancialInfo().getBankInfo().getBankName());
			bankInfo.setRoutingNumber(paymentMethod.getFinancialInfo().getBankInfo().getRoutingNumber());
			bankInfo.setAccountType(paymentMethod.getFinancialInfo().getBankInfo().getAccountType());
			bankInfo.setNameOnAccount(paymentMethod.getFinancialInfo().getBankInfo().getNameOnAccount());

			// removing the payment address related information from the
			// financialInfo object
			Location location = new Location();
			location.setAddress1(brokerObj.getLocation().getAddress1());
			location.setAddress2(brokerObj.getLocation().getAddress2());
			location.setCity(brokerObj.getLocation().getCity());
			location.setState(brokerObj.getLocation().getState());
			location.setZip(brokerObj.getLocation().getZip());
			financialInfo.setLocation(location);

 	 	 	paymentMethod.setPaymentType(PaymentMethods.PaymentType.ACH);
 	 	 	paymentMethod.setPaymentMethodName(PaymentMethods.PaymentType.ACH.toString());
		}
	}

	/**
	 * If financialInfo contains noy null BankInfo, method returns BankInfo.
	 * If financialInfo contains null BankInfo, new BankInfo is returned.
	 * @param financialInfo
	 * @return
	 */
	private BankInfo getBankInfo(FinancialInfo financialInfo) {
		BankInfo bankInfo=financialInfo.getBankInfo();

		return (bankInfo != null) ? bankInfo : new BankInfo();
	}

	/**
	 * If paymentMethodsObj object contains not null FinancialInfo, method returns FinancialInfo.
	 * If paymentMethodsObj contains null FinancialInfo, new FinancialInfo is returned.
	 * @param paymentMethodsObj
	 * @return
	 */
	private FinancialInfo getFinancialInfo(PaymentMethods paymentMethodsObj) {
		FinancialInfo financialInfo=paymentMethodsObj.getFinancialInfo();
		return (financialInfo != null) ? financialInfo: new FinancialInfo();
	}

	/**
	 * If paymentId is not null, PaymentMethods is acquired from paymentMethodService and that is returned.
	 * If paymentId is null OR PaymentMethods returned from paymentMethodService is null then new PaymentMethods is returned.
	 * @param paymentId
	 * @return
	 */
	private PaymentMethods getPaymentMethod(Broker brokerObj) {
		PaymentMethods payMethod=null;
		if(brokerObj != null){
	 	 	 	payMethod = brokerService.getPaymentMethodDetailsForBroker(brokerObj.getId(),
	 	 	 	PaymentMethods.ModuleName.BROKER);
		}
		if(payMethod == null){
			payMethod=new PaymentMethods();
		}
		if (payMethod.getStatus() == null) {
			payMethod.setStatus(PaymentStatus.Active);
		}

		return payMethod;
	}

	/**
	 * Handles the add payment information of Broker
	 *
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate manage Broker page
	 */
	@RequestMapping(value = "/broker/paymentinfo", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_PAYMENTINFO')")
	public String paymentInfo(Model model, HttpServletRequest request) {
		LOGGER.info("paymentInfo : START");

		if (BrokerMgmtUtils.isPaymentEnabled()) {
			LOGGER.info("Broker Payment Information ");
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Broker Payment Information");
			Broker brokerObj = new Broker();
			String paymentErrorFlag = request.getParameter("paymentInfoValidated");
			model.addAttribute("paymentInfoValidated", paymentErrorFlag);
			try {
				AccountUser user = userService.getLoggedInUser();
				if (isUserSwitch(request)) {
					brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String) request.getSession().getAttribute(
							SWITCH_TO_MODULE_ID)));
				}else{
					brokerObj = brokerService.findBrokerByUserId(user.getId());
				}
				if (brokerObj == null) {
					brokerObj = new Broker();
					brokerObj.setUser(user);
				}
				model.addAttribute(BROKER_MODULE, brokerObj);
				if (!StringUtils.isBlank(request.getParameter(CANCEL))
						&& request.getParameter(CANCEL).trim().equalsIgnoreCase(TRUE2)) {
					model.addAttribute(UPDATEPROFILE, false);
				}
				PaymentMethods paymentMethodObj = new PaymentMethods();
				paymentMethodObj = brokerService.getPaymentMethodDetailsForBroker(brokerObj.getId(),
						PaymentMethods.ModuleName.BROKER);
				model.addAttribute(PAYMENT_METHODS_OBJ, paymentMethodObj);
				model.addAttribute(LIST_OF_STATES, brokerMgmtUtils.filterStates());
				request.getSession().setAttribute(PAYMENT_OBJ_ID,
						paymentMethodObj != null ? paymentMethodObj.getId() : ZERO);
				request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);
				model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isPaymentEnabled());
			} catch (InvalidUserException ex) {
				LOGGER.error(INVALID_USER_EXCEPTION_OCCURRED, ex);
			}

			LOGGER.info("paymentInfo : END");

			return "broker/paymentinfo";
		}
		else {
			LOGGER.info(IGNORING_PAYMENT_INFO_CURRENTLY);
			LOGGER.info("paymentInfo : END");
			return BROKER_VIEW_PROFILE;
		}
	}

	/**
	 * To retrieve spoken language list for Broker
	 *
	 * @param query
	 *            String of inputs entered by user
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return JSON string of Language list
	 *
	 */
	@RequestMapping(value = "broker/buildprofile/getlanguage", method = RequestMethod.GET, headers = "Accept=*/*")
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	@ResponseBody
	public String getLanguageList(@RequestParam("matched") String query, HttpServletResponse response) {

		LOGGER.info("getLanguageList : START");

		LOGGER.info("Getting the language spoken list ");

		List<String> languageList = lookupService.populateLanguageNames(query);

		if (null == languageList) {
			languages = new ArrayList<String>();
		} else {
			languages = languageList;
		}

		LOGGER.info("getLanguageList: END");
		return getLanguageSpokenList(query, response);

	}

	/**
	 * Get spoken language list
	 *
	 * @param query
	 *            String of inputs entered by user
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return Language JSON string
	 */
	private String getLanguageSpokenList(String query, HttpServletResponse response) {

		LOGGER.info("getLanguageSpokenList : START");
		response.setContentType(APP_JSON_CONTENT);

		String jsonData = null;
		String country = null;
		String lowerCaseQuery = query.toLowerCase();
		List<String> matched = new ArrayList<String>();
		for (int i = ZERO; i < languages.size(); i++) {
			country = languages.get(i).toLowerCase();
			if (country.startsWith(lowerCaseQuery)) {
				matched.add(languages.get(i));
			}
		}

		LOGGER.info("getLanguageSpokenList : END");
		jsonData = platformGson.toJson(matched);

		return jsonData;
	}

	/**
	 * Method to validate languages entered by broker
	 *
	 * @param model
	 *            The Model instance.
	 * @param languagesSpoken
	 *            The spoken language.
	 * @return Boolean <code>true</code> if language allowed else
	 *         <code>false</code>.
	 */
	@RequestMapping(value = "/broker/buildProfile/checkLanguagesSpoken")
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	@ResponseBody
	public Boolean checkLanguagesSpoken(@RequestParam String languagesSpoken) {

		LOGGER.info("checkLanguagesSpoken : START");

		LOGGER.info("spoken language validation start here");

		List<String> languageList = null;
		List<String> countiesServedlist = null;

		languageList = lookupService.populateLanguageNames("");

		if (null == languageList) {
			languageList = new ArrayList<String>();
		}

		String languagesSpokenTrimmed = languagesSpoken.trim();
		countiesServedlist = Arrays.asList(languagesSpokenTrimmed.split(","));
		Validate.notNull(countiesServedlist);
		boolean returnVal = false;
		for (String countiesStr : countiesServedlist) {
			if (languageList.contains(countiesStr.trim())) {
				returnVal = true;
			} else {
				returnVal = false;
				break;
			}
		}

		LOGGER.info("checkLanguagesSpoken : END");

		return returnVal;

	}

	private String loadLanguagesData(){
        LOGGER.info("getLanguageList : START");
        String jsonData = null;

	    List<String> languageSpokenList = null;

	    languageSpokenList = lookupService.populateLanguageNames("");
        LOGGER.info("getLanguageList : END");
        jsonData = platformGson.toJson(languageSpokenList);

        return jsonData;
	}

	//@RequestMapping(value = "/broker/employee/dashboard", method ={RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ACCOUNT_SWITCH_EMPLOYEE')")
	public String switchToEmployeeView(HttpServletRequest request) {
		 if(request.getSession().getAttribute("checkDilog")==null){
	    		String checkDilog= request.getParameter("checkAgentView");
	    		request.getSession().setAttribute("checkDilog", checkDilog);
	    	}
		return "forward:/account/user/switchUserRole";
	}

	/**
	 * Handles the GET and POST request for displaying new comment pop-up iframe for employee.
	 *
	 * @param target_id The employees id for whom this comment is.
	 * @param target_name For employee target name should be "EMPLOYEE".
	 * @param employeeName Name of employee to show on pop-up box.
	 * @return request The current HttpServletRequest instance.
	 * @throws GIException
	 */
	//@RequestMapping(value = "/broker/employee/newcomment", method = {
			//RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_ADD_COMMENT')")
	public String addNewComment(
			@RequestParam(value = TARGET_ID, required = false) String encryptedId,
			@RequestParam(value = TARGET_NAME, required = false) String encryptedTargetName,
			@RequestParam(value = "employeeName", required = false) String employeeName,
			Model model, HttpServletRequest request) throws GIException {
		LOGGER.info("employee new-comment : START");
		String targetId = null;
		String targetName=null;
		try{
			targetId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			targetName = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedTargetName);
			model.addAttribute(PAGE_TITLE,
					"Getinsured Health Exchange:Employee New-comment page");
			model.addAttribute(TARGET_ID, targetId);
			model.addAttribute(TARGET_NAME, targetName);
			model.addAttribute("employeeName", employeeName);
		}
		catch(Exception e){
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in broker." + e);
		}

		LOGGER.info("employee new-comment : END ");
		return "broker/employee/addnewcomment";
	}


	/**
	 * Handles the GET request for loading comments screen for broker.
	 *
	 * @param brokerId The current broker's identifier.
	 * @param request The current HttpServletRequest instance.
	 * @param model The current Model instance.
	 * @return String The view name.
	 * @throws InvalidUserException The InvalidUserException instance.
	 */
	@RequestMapping(value = "/broker/comments/{encBrokerId}/{encryptedId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_COMMENT')")
	public String getComments(@PathVariable("encBrokerId") String encBrokerId, @PathVariable("encryptedId") String encEmployeeId, HttpServletRequest request, Model model) throws InvalidUserException {
		LOGGER.info("broker - getComments : START ");
		EmployeeDTO currEmployee =  null;
		String decryptBrkId = null;
		String decryptEmployeeId = null;
		int brokerId, employeeId;

		try {
			decryptBrkId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			decryptEmployeeId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encEmployeeId);

			brokerId = Integer.parseInt(decryptBrkId);
			employeeId = Integer.parseInt(decryptEmployeeId);
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Comments");


			/** Fetches current broker information. */
			Broker brokerObj = brokerService.getBrokerDetail(brokerId);

			/** Fetches current employee information. */

			XStream xStream = GhixUtils.getXStreamStaxObject();
			String restResponse = ghixRestTemplate.getForObject(
					GhixEndPoints.ShopEndPoints.GET_EMPLOYEE_DETAILS+"?employeeId="+employeeId, String.class);// employeeService.findById(employeeId);
			ShopResponse shopResponse=(ShopResponse) xStream.fromXML(restResponse);

			currEmployee = (EmployeeDTO)shopResponse.getResponseData().get("Employee");

			Validate.notNull(brokerObj, "The broker record not found in repository.");
			Validate.notNull(currEmployee, "The employee record not found in repository.");

			model.addAttribute(BROKER_MODULE, brokerObj);
			model.addAttribute("objEmployee", currEmployee);

			model.addAttribute("exchangePhone", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			model.addAttribute("exchangeName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			request.getSession().setAttribute(LAST_VISITED_URL, "/broker/comments/"+brokerId+"/"+employeeId);
		}
		catch (Exception exception) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in broker." + exception);
		}

		LOGGER.info("broker - getComments : END ");
		return "broker/comments";
	}


	/**
	 * Handles the POST request for designating employer when Agent tries to add Employer on behalf of Employer.
	 *
	 * @param request
	 * @param employerId
	 * @param response
	 */
	@RequestMapping(value = "/broker/designateEmployer", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'BROKER_DESIGNATE_EMPLOYER')")
	@ResponseBody
	public void designateEmployer(@RequestBody String employerRequestStr) {
		LOGGER.info("designateEmployer: START ");
		XStream xstream = GhixUtils.getXStreamStaxObject();
		DesignateBrokerDTO designateBrokerDTO = (DesignateBrokerDTO)xstream.fromXML(employerRequestStr);
		DesignateBroker designateBroker = new DesignateBroker();
		AccountUser user = null;

		try {
			user = userService.getLoggedInUser();
		} catch (InvalidUserException ex) {
			LOGGER.error(INVALID_USER_EXCEPTION_OCCURRED, ex);
		}

		Validate.notNull(user);

		Broker brokerObj = brokerService.findBrokerByUserId(user.getId());
		designateBroker.setEmployerId(designateBrokerDTO.getEmployerId());
		designateBroker.setStatus(Status.Active);
		designateBroker.setBrokerId(brokerObj.getId());
		designateBroker.setShow_switch_role_popup('Y');
		try {
			designateService.saveDesignateBroker(designateBroker);
		}
		catch(GIRuntimeException giexception){
			LOGGER.error("Exception ocurred while designating Employer.", giexception);
            throw giexception;
		}
		LOGGER.info("designateEmployer: END ");
	}

	/**
	 * Handles the GET/POST request for sending activation link.
	 *
	 * @param id
	 * @throws GIException
	 */
	@RequestMapping(value = "/broker/sendActivationLink/{encryptedId}", method = {RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_ADD_BROKER)
	public String sendActivationLink(@PathVariable("encryptedId") String encryptedId, HttpServletRequest request) throws GIException{
		LOGGER.info("Sending Email to employer for aend activation link: " );
		String activationLinkResult = null;
		StringBuilder redirectUrl = new StringBuilder();
		String decryptedId = null;
		int id;
		AccountUser user = new AccountUser();
		try {
			user = userService.getLoggedInUser();
		 } catch (InvalidUserException ex) {
			LOGGER.error(INVALID_USER_EXCEPTION_OCCURRED, ex);
		 }
		try {
			decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			id = Integer.parseInt(decryptedId);
			XStream xStream = GhixUtils.getXStreamStaxObject();

			String restResponse = ghixRestTemplate.getForObject(
						GhixEndPoints.ShopEndPoints.GET_EMPLOYER_DETAILS+"?employerId="+id, String.class);// employeeService.findById(employeeId);
			ShopResponse shopResponse=(ShopResponse)xStream.fromXML(restResponse);
			EmployerDTO employer= (EmployerDTO)shopResponse.getResponseData().get("Employer");
			Broker brokerObj = null;
			 if (request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW) != null
						&& request.getSession().getAttribute(SWITCH_TO_MODULE_ID) != null
						&& ((String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)).equalsIgnoreCase("Y")) {

					brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String) request.getSession().getAttribute(SWITCH_TO_MODULE_ID)));
				} else {
					brokerObj = brokerService.findBrokerByUserId(user.getId());
				}

			 activationLinkResult = brokerService.generateEmployerActivationLink( employer,brokerObj );

			 redirectUrl.append(EMPLOYER_CASE_DETAILS);
			 redirectUrl.append(encryptedId);

			 if(activationLinkResult != null) {

				 redirectUrl.append(ACTIVATION_RESULT);
				 redirectUrl.append(activationLinkResult);
			 }
		}catch(NumberFormatException nfe){
			String errMsg = "Failed to decrypt employerId. "+COMMA_EXCEPTION_IS+nfe;
	    	LOGGER.error(errMsg);
	    	throw new GIException("Error occured in sendActivationLink. "+errMsg);
		}
		catch (Exception e) {
			LOGGER.error("Error occured while generating Employer Email Account Activation Link : "+e.getMessage(),e);
		}
		return redirectUrl.toString();
	}

	/**
	 * Handles the request for fetching the Agent/Broker information by EmployerID
	 *
	 * @param employerId
	 * @return brokerObj
	 */
	@RequestMapping(value = "/broker/brokerinformation", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_ADD_BROKER)
    @ResponseBody
    public String brokerInformation(@RequestBody String employerRequestStr) {
           LOGGER.info("brokerInformation: START ");
           XStream xstream = GhixUtils.getXStreamStaxObject();
           BrokerInformationDTO brokerInformationDTO = (BrokerInformationDTO)xstream.fromXML(employerRequestStr);
           DesignateBroker designateBroker = designateService.findBrokerByEmployerId(brokerInformationDTO.getEmployerId());
           Broker brokerObj = brokerService.getBrokerDetail(designateBroker.getBrokerId());
           LOGGER.info("brokerInformation: END ");
           return GhixUtils.xStreamHibernateXmlMashaling().toXML(brokerObj);
	}

	/**
	 * CODE FROM SHOP MODULE
	 * @throws GIException
	 *
	 */
	//@RequestMapping(value = "/broker/employer/newcomment", method = {
			//RequestMethod.GET, RequestMethod.POST })
	public String addNewCommentEmployer(
			@RequestParam(value = "encryptedId", required = false) String encryptedId,
			@RequestParam(value = TARGET_NAME, required = false) String target_name,
			@RequestParam(value = "employerName", required = false) String employerName,
			Model model, HttpServletRequest request) throws GIException {
		LOGGER.info("employer contact info page ");
		String decryptedId = null;
		int targetId;
		try{
			decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			targetId = Integer.parseInt(decryptedId);
			model.addAttribute(PAGE_TITLE,
					"Getinsured Health Exchange:Employer Details Page");
			model.addAttribute(TARGET_ID, targetId);
			model.addAttribute(TARGET_NAME, target_name);
		}catch(NumberFormatException nfe){
			String errMsg = "Failed to decrypt target_id. " +COMMA_EXCEPTION_IS+nfe;
	    	LOGGER.error(errMsg);
	    	throw new GIException("Error occured in addNewCommentEmployer. "+errMsg);
		}
		return "broker/employer/addnewcomment";
	}

	//@RequestMapping(value = "/broker/employer/viewemployerdetail/{encryptedId}", method = {
			//RequestMethod.GET, RequestMethod.POST })
	public String viewemployerdetail(@PathVariable("encryptedId") String encryptedId,
			Model model, HttpServletRequest request) {
		LOGGER.info("employer contact info page ");
		model.addAttribute(PAGE_TITLE,
				"Getinsured Health Exchange:Employer Details Page");
		Integer totalEmp = ZERO;
		int employerId;
		DesignateBroker designateBroker = null;
		String decrypyedEmpId = null;
		XStream xStream = GhixUtils.getXStreamStaxObject();

		try{
			decrypyedEmpId =  ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			employerId = Integer.parseInt(decrypyedEmpId);

			String restResponse = ghixRestTemplate.getForObject(
					GhixEndPoints.ShopEndPoints.GET_EMPLOYER_DETAILS+"?employerId="+employerId, String.class);// employeeService.findById(employeeId);
			ShopResponse shopResponse=(ShopResponse)xStream.fromXML(restResponse);
			EmployerDTO employerObj=(EmployerDTO)shopResponse.getResponseData().get("Employer");


			designateBroker = designateService.findBrokerByEmployerId(employerId);

			AccountUser user = null;
			// List<ModuleUser> moduleUserList =
			// moduleUserService.findByModuleIdModuleName(employerId,
			// ModuleUserService.EMPLOYER_MODULE);
			List<ModuleUser> moduleUserList = userService.getModuleUsers(
					employerId, ModuleUserService.EMPLOYER_MODULE);
			if (moduleUserList != null && !moduleUserList.isEmpty()) {
				ModuleUser mu = moduleUserList.get(ZERO);
				user = mu.getUser();
			}

			List<EmployeeDTO> employees = (List<EmployeeDTO>)shopResponse.getResponseData().get("Employees");

			if (employees != null && !employees.isEmpty()) {
				totalEmp = employees.size();
			}
			model.addAttribute("user", user);
			if (moduleUserList != null) {
				model.addAttribute("moduleuserlist", moduleUserList);
			}
			model.addAttribute("employer", employerObj);

			String status = designateBroker.getStatus() != null ? designateBroker.getStatus().toString() : "";
			model.addAttribute("status", status);
			model.addAttribute("totalEmp", totalEmp);
			model.addAttribute(BROKER_ID, designateBroker.getBrokerId());
		}catch(NumberFormatException nfe){
			String errMsg = "Failed to decrypt EmployerId in viewemployerdetail method. "+COMMA_EXCEPTION_IS+nfe;
	    	LOGGER.error(errMsg);
	    	throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}catch(GIRuntimeException giexception){
            LOGGER.error("Error occured in viewemployerdetail in viewemployerdetail method: ", giexception);
            throw giexception;
		}catch(Exception e){
			LOGGER.error("Error occured in viewemployerdetail, Exception is: "+e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return "broker/employer/viewemployerdetail";
	}

	//@RequestMapping(value = "/broker/employer/employercase/{encryptedId}", method = RequestMethod.GET)
	public String getEmployerCaseDetail(@PathVariable("encryptedId") String encryptedId,
			Model model,HttpServletRequest request) {

		String decryptedEmpId = null;
		int empId;
		try{
			decryptedEmpId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			empId = Integer.parseInt(decryptedEmpId);
			boolean validBroker=validateBroker(empId, request);

			if(!validBroker){
				return REDIRECT+getDashboardPage(request);
			}

			String loginUrl = getEmployerDetails(empId, model);
			String activationLinkResult = request.getParameter(ACTIVATION_LINK_RESULT);
			model.addAttribute(ACTIVATION_LINK_RESULT, activationLinkResult);
			if (loginUrl != null) {
				return loginUrl;
			}
			request.getSession().setAttribute(LAST_VISITED_URL, "/broker/employer/employercase/"+encryptedId);

		}catch(NumberFormatException nfe){
			String errMsg = "Error in decrypting EmployerID, exception is: "+nfe;
			LOGGER.error(errMsg);
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);

		}catch(InvalidUserException ex){
			LOGGER.error(INVALID_USER_EXCEPTION_OCCURRED, ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);

		}catch(GIRuntimeException giexception){
            LOGGER.error("Exception occured while fetching employee Information : ", giexception);
            throw giexception;
		}catch(Exception exception){
            LOGGER.error("Exception occured while fetching employee Information in getEmployerCaseDetail method: ", exception);
            throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		return "broker/employer/employercasedetails";
	}

	private String getDashboardPage(HttpServletRequest request) {
		String roleName=(String) request.getSession().getAttribute("switchToModuleName");
		if(roleName == null || "".equals(roleName)) {
			try {
				AccountUser user = userService.getLoggedInUser();
				roleName = user.getCurrentUserRole();
			} catch (InvalidUserException e) {
				LOGGER.error("Unable to get currently logged in user when trying to retrieve dashboard URL {}, returning root URL.", e.getMessage());
				return "/";
			}
		}

		Role activeRole = roleService.findRoleByName(StringUtils.upperCase( roleName));
		if(BROKER_ROLE.equalsIgnoreCase(StringUtils.upperCase( roleName))){
			request.getSession().removeAttribute(LAST_VISITED_URL);
			//This if has been added to keep last_visited functionality of Agent switch to employer.
		}
		return activeRole.getLandingPage();
	}

	protected boolean validateBroker(Integer id, HttpServletRequest request)
			throws InvalidUserException {
		boolean validBroker=false;
		String switchToModuleName = (String) request.getSession().getAttribute("switchToModuleName");
		if("broker".equalsIgnoreCase(switchToModuleName)){
			//if logged in user is broker... then check if this broker is designated to same broker...or send broker to defaultLanding page
			AccountUser user = userService.getLoggedInUser();
			//if user is came here with switching then get broker with active-module-id
			Broker broker;
			if("Y".equalsIgnoreCase((String)request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW))){
				broker=brokerService.getBrokerDetail(user.getActiveModuleId());
			}else{
				 broker=brokerService.findBrokerByUserId(user.getId());
			}

			DesignateBroker designateBroker=designateService.findBrokerByEmployerId(id);
			if(broker ==null || designateBroker==null){
				validBroker=false;
			}else if(   broker.getId()!=designateBroker.getBrokerId() || "InActive".equalsIgnoreCase(designateBroker.getStatus().toString())  ){
				validBroker=false;
			}else{
				validBroker=true;
			}

		}

		return validBroker;
	}

	/**
	 * This method has been implemented to redirect user to his dashboard page ..
	 * this method gets call from masthead.jsp "Dashboard" link
	 * @return
	 */
	@RequestMapping(value = "account/user/getDashBoard", method = RequestMethod.GET)
	public String redirectToDashboard(HttpServletRequest request){
		return REDIRECT+getDashboardPage(request);
	}

	//@RequestMapping(value = "broker/employer/employercomments/{encryptedId}", method = RequestMethod.GET)
	public String getEmployerComments(@PathVariable("encryptedId") String encryptedId,
			HttpServletRequest request, Model model) throws GIException {
		String decryptedId = null;
		int id;
		try{
			decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			id = Integer.parseInt(decryptedId);
			boolean validBroker=validateBroker(id, request);
			if(!validBroker){
				request.getSession().removeAttribute(LAST_VISITED_URL);
				return REDIRECT+getDashboardPage(request);
			}
			String loginUrl = getEmployerDetails(id, model);
			if (loginUrl != null) {
				return loginUrl;
			}
			request.getSession().setAttribute("employerId", id);
			request.getSession().setAttribute(LAST_VISITED_URL, "/broker/employer/employercomments/"+encryptedId);
		}catch(NumberFormatException nfe){
			String errMsg = "Error occured in decrypting employerId. "+COMMA_EXCEPTION_IS+nfe;
			LOGGER.error(errMsg);
			throw new GIException("Error occured in getEmployerComments. "+errMsg);
		}catch(InvalidUserException ex){
			LOGGER.error(INVALID_USER_EXCEPTION_OCCURRED, ex);
			throw new GIException(ex.getMessage());
		}

		return "broker/employer/employercomments";
	}

	//@RequestMapping(value = "/broker/employer/employercontactinfo/{encryptedId}", method = RequestMethod.GET)
	public String getEmployerContactInfo(Model model,HttpServletRequest request,
			@PathVariable(value = "encryptedId") String encryptedId) throws InvalidUserException {
		LOGGER.info("View Employer Contact Info");
		String id = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
		boolean validBroker=validateBroker(Integer.valueOf(id), request);
		if(!validBroker){
			request.getSession().removeAttribute(LAST_VISITED_URL);
			return REDIRECT+getDashboardPage(request);
		}
		String loginUrl = getEmployerDetails(Integer.valueOf(id), model);
		if (loginUrl != null) {
			return loginUrl;
		}
		request.getSession().setAttribute(LAST_VISITED_URL, "/broker/employer/employercontactinfo/"+encryptedId);
		return "broker/employer/employercontactinfo";
	}

	private String getEmployerDetails(Integer id, Model model) throws InvalidUserException {
		LOGGER.info("Inside Get Employer Details");
		String errorMsg = "Employer does not exist.";
		int employerId = id;
		String avgSal = "N/A";
		int totalEmp = ZERO;
		DesignateBroker designateBroker = null;
		/*
		 * NOTES:  The following block had removed for HIX-10970.
		 * While /shop/** is not protected , getting the logged in user is invalid.
		 * If the method required to have secured access then use @PreAuthorize("hasPermission(#model, < role name >)") before method signature.
		 */
		XStream xStream = GhixUtils.getXStreamStaxObject();

		String restResponse = ghixRestTemplate.getForObject(
				GhixEndPoints.ShopEndPoints.GET_EMPLOYER_DETAILS+"?employerId="+employerId, String.class);// employeeService.findById(employeeId);

		ShopResponse  shopResponse=(ShopResponse)xStream.fromXML(restResponse);
		EmployerDTO employer=(EmployerDTO)shopResponse.getResponseData().get("Employer");


		if (employer != null){
			errorMsg="";
			if(employer.getAverageSalary() != null ){
				avgSal = employer.getAverageSalary();

				if(avgSal.length() > ZERO && !avgSal.contains("$")){
					avgSal = "$" + avgSal;
				}
			}
			List<EmployeeDTO> employees =(List<EmployeeDTO>)shopResponse.getResponseData().get("Employees");

			totalEmp = getTotalEmployees( employees);

			designateBroker = designateService.findBrokerByEmployerId(employerId);

	 	 	model.addAttribute("employer", employer);

		}

		model.addAttribute("totalEmp", totalEmp);
		model.addAttribute("avgSal", avgSal);
		model.addAttribute("errorMsg", errorMsg);
		// Below code is added to check if employer has activated the account.If it is activated then
		// do not show the sendActivation link.

		Validate.notNull(designateBroker);

		AccountActivation accountActivation =  accountActivationService.getAccountActivationByCreatedObjectId(employerId, "EMPLOYER",designateBroker.getBrokerId(),"BROKER");
		boolean employerActivated =true;
		if(accountActivation == null || accountActivation.getStatus().equals(AccountActivation.STATUS.NOTPROCESSED)){
			employerActivated= false;
		}

		model.addAttribute("employerActivated", Boolean.toString(employerActivated));
		return null;
	}

	/**
	 * This method takes list of employees of employer and counts total number of employees.
	 * Method doesn't count number of employees which are deleted or terminated.
	 * @param totalEmp
	 * @param employees
	 * @return
	 */
	private int getTotalEmployees( List<EmployeeDTO> employees) {
		int totalEmp=ZERO;
		String currentEmployeeStatus;
		if(employees==null){
			return totalEmp;
		}
		for(Employee currEmployee: employees){
				if( (null != currEmployee) && (null != currEmployee.getStatus())){
					currentEmployeeStatus = currEmployee.getStatus().toString().trim();

					if(currentEmployeeStatus.isEmpty() ||  currentEmployeeStatus.equals(Employee.Status.DELETED.toString()) || currentEmployeeStatus.equals(Employee.Status.TERMINATED.toString())) {
						continue;
					}
					totalEmp += 1;
				}
		}
		return totalEmp;
	}

	/**
	 * Handles the POST request for uploading the Broker's photo.
	 *
	 * @param request
	 * @param fileInput
	 * @return URL for navigation to edit certification status page.
	 * @throws IOException
	 * @throws GIException
	 */
	@RequestMapping(value = "/broker/uploadphoto/{encBrokerId}", method = RequestMethod.POST)
	@ResponseBody
	public void uploadPhoto(@RequestParam(value = "fileInputPhoto", required = false) MultipartFile fileInput,
			HttpServletRequest request, @PathVariable("encBrokerId") String encBrokerId, HttpServletResponse response,
			@ModelAttribute(BROKER_MODULE) Broker broker) throws IOException {

		LOGGER.info("uploadPhoto: START ");
		String returnString = "SUCCESS";
		response.setContentType("text/plain");
		PrintWriter out  = null;
		String decryptBrokerId = null;
		int brokerId;
		String extension = "";
		InputStream is = null;
	    try {
	    	decryptBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
	    	brokerId = Integer.parseInt(decryptBrokerId);
			out  = response.getWriter();
			Validate.notNull(fileInput);
			is = fileInput.getInputStream();
			BufferedImage image = ImageIO.read(is);

			if(image == null) {
				returnString = "Your file could not be uploaded. Use one of these file types: BMP, GIF, JPG, JPEG, PNG.";
				out.print(returnString);
				return;
			}
			
			if(!checkIfCurrentUserOrAdmin(brokerId)) {
				returnString = "ACCESSDENIED";
				throw new AccessDeniedException("Permission Denied. Invalid object access requested");
			}

			if((fileInput !=null) && (fileInput.getSize() > ZERO) && (fileInput.getSize() < FILE_INPUT_MAX_SIZE)){
				extension = FilenameUtils.getExtension(fileInput.getOriginalFilename());

				returnString = updatePhotoResult(fileInput,
						brokerId, extension);
					}
			else
			{
				returnString = "SIZE_FAILURE";
			}
			out.print(returnString);
	    }catch(AccessDeniedException ex){
	    	LOGGER.error("Access denied exception.");
	    	throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} catch (NumberFormatException nfe){
	    	String errMsg = "Failed to decrypt Broker id in uploadPhoto method. "+COMMA_EXCEPTION_IS+nfe;
	    	LOGGER.error(errMsg);
	    	LOGGER.info("uploadPhoto Method : END ");
	    	throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
	    }catch(GIRuntimeException giexception){
            LOGGER.error("Failed to upload broker photo: ", giexception);
            throw giexception;
		}catch (Exception ex) {
			returnString = FAILURE;
			if(out !=null) {
				out.print(returnString);
			}
			LOGGER.error("Failed to upload", ex);
			LOGGER.info("uploadPhoto: END ");
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally{
			IOUtils.closeQuietly(is);
			IOUtils.closeQuietly(out);
		}
	}
	
	
	
	private boolean checkIfCurrentUserOrAdmin(int brokerId) throws InvalidUserException {
		if(userService.hasUserRole(userService.getLoggedInUser(),
				 ADMIN) || userService.hasUserRole(userService.getLoggedInUser(),  BROKER_ADMIN)) {
			return true;
		}
		else {
			Broker broker = brokerService.findBrokerByUserId(userService.getLoggedInUser().getId());
			if(broker != null && broker.getId() == brokerId) {
  				return true;
			}else if(userService.hasUserRole(userService.getLoggedInUser(), RoleService.AGENCY_MANAGER)){
				Broker ceatedNewBroker = brokerService.getBrokerDetail(brokerId);
				if(broker.getAgencyId().intValue() == ceatedNewBroker.getAgencyId().intValue()){
					return true;
				}
			} 
		}
		return false;
	}

	private String updatePhotoResult(MultipartFile fileInput,
			 int brokerId, String extension)
			throws Exception {
		Integer docId = ZERO;
		String returnString="SUCCESS";
		String fileContent;
		if(EntityUtils.checkFileExtension(extension)){
			fileContent = EntityUtils.getFileContent(fileInput);
			if(!EntityUtils.isEmpty(fileContent) && !EntityUtils.checkXSSImage(fileContent)){
				docId = brokerService.uploadPhoto(brokerId,fileInput);
				if(docId == ZERO) {
					returnString = FAILURE;
				}
			} else {
				returnString = FAILURE;
				throw new Exception("Possible XSS Attack : Invalid input found on the page.");
			}
		} else {
			returnString = FAILURE;
			throw new GIException("Invalid File Extension. This file type is not supported.");
		}
		return returnString;
	}

	/**
	 * Handles the Get request for downloading the Broker's photo.
	 *
	 * @param id
	 * @param request
	 * @return photo in Bytes array.
	 * @throws GIException
	 */
	@RequestMapping(value = "/broker/photo/{encBrokerId}", method = RequestMethod.GET, produces = "image/jpeg; charset=utf-8")
	@ResponseBody
	public byte[] downloadPhotoById(@PathVariable("encBrokerId") String encBrokerId, HttpServletRequest request, HttpServletResponse response) throws GIException {

		LOGGER.info("downloadPhotoById: START");
		String decryptBrokerId = null;
		BrokerPhoto brokerPhoto = null;
		String contentType;
		byte[] content = null;
		int brokerId;
		ByteArrayOutputStream byteArrayOutputStream = null;
		ByteArrayInputStream byteArrayInputStream = null;
		String mimeType;
		
		try{
			decryptBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
	    	brokerId = Integer.parseInt(decryptBrokerId);
			brokerPhoto = brokerService.getBrokerPhotoById(brokerId);
			
			if(brokerPhoto!=null && brokerPhoto.getPhoto()!=null && !EntityUtils.isEmpty(brokerPhoto.getMimeType())){
				mimeType = brokerPhoto.getMimeType();
				byteArrayInputStream = new ByteArrayInputStream(brokerPhoto.getPhoto());
				BufferedImage bufferedImage = ImageIO.read(byteArrayInputStream);
				byteArrayOutputStream = new ByteArrayOutputStream();

				contentType = EntityUtils.getLastElement(mimeType, BrokerConstants.SLASH);

				if(EntityUtils.checkFileExtension(contentType)){
					ImageIO.write(bufferedImage, contentType, byteArrayOutputStream);
		            content = byteArrayOutputStream.toByteArray();
				}

				int count = StringUtils.countMatches(mimeType, BrokerConstants.SLASH);
				if(count>1){
					mimeType = EntityUtils.removeLastElement(mimeType, BrokerConstants.SLASH);
				}
					
				response.setContentType(mimeType);
				response.setHeader("Content-Type", mimeType);
			}
		}catch (NumberFormatException nfe){
	    	String errMsg = "Failed to decrypt Broker id. "+COMMA_EXCEPTION_IS+nfe;
	    	LOGGER.error(errMsg);
	    	LOGGER.info("downloadPhotoById Method: END ");
	    	throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
	    }
		catch(GIRuntimeException giexception){
            LOGGER.error("Failed to downloadPhotoById: ", giexception);
            throw giexception;
		}catch (Exception ex) {

			LOGGER.error("Failed to download broker Photo by id", ex);
			LOGGER.info("downloadPhotoById: END ");
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			IOUtils.closeQuietly(byteArrayInputStream);
			IOUtils.closeQuietly(byteArrayOutputStream);
		}

		LOGGER.info("downloadPhotoById: END");
		return content;
	}
	/**
	 * This methods takes request object as a parameter and checks whether user has switched to other user OR not by using some session values
	 * @param request
	 * @return
	 */
	private boolean isUserSwitch( HttpServletRequest request){
		boolean returnValue=false;

		returnValue=(request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW) != null) && (request.getSession().getAttribute(SWITCH_TO_MODULE_ID) != null)
				&& (((String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)).equalsIgnoreCase("Y"));
		LOGGER.info("request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW): "+ request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW));
		LOGGER.info("request.getSession().getAttribute(SWITCH_TO_MODULE_ID): "+ request.getSession().getAttribute(SWITCH_TO_MODULE_ID));
		LOGGER.info("request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW): "+ request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW));

		return returnValue;
	}

}
