package com.getinsured.consumer.service;

import java.util.Date;

class NoteMetaData {
	private String enteredBy;
	private Date date;
	private String time;

	public NoteMetaData() {

	}

	public NoteMetaData(String enteredBy, Date date, String time) {
		this.enteredBy = enteredBy;
		this.date = date;
		this.time = time;
	}

	public String getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(String enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
}