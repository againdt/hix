<%@ include file="../include.jsp" %>
 
<div id="appscr54" style="display: none;">
	<div class="row-fluid">
		<div class="page-title">
			<h5 class="titleForPages"><spring:message code="ssap.page4.whoIsHelpingYou" text="Who is Helping You?" /></h5>
		</div>
		
		<form method="POST" action="#" class="form-horizontal gutter10" id="authorized-representative-contact-information-form" data-parsley-excluded=":hidden">
			<div id="helpQuestionDiv" class="hide">
				<div class="header">
					<h4><spring:message code="ssap.page4.getAssistance" /></h4>
				</div>
				<p class="gutter10">
					<spring:message code="ssap.page4.anyoneHelpingThisApplication" /> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png">
				</p>

				<div class="control-group margin10-l">
					<label for="authorizedRepresentativeHelpYes" class="capitalize-none">
						<input type="radio" name="authorizedRepresentativeHelp" id="authorizedRepresentativeHelpYes" value="yes" onchange="displayAuthorizedRepresenatitive();" data-parsley-errors-container="#authorizedRepresentativeHelpDivErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />"/>
						<spring:message code="ssap.page4.authorizedRepresentative" />
					</label>
					<label for="authorizedRepresentativeHelpNo" class="capitalize-none">
						<input type="radio" name="authorizedRepresentativeHelp" id="authorizedRepresentativeHelpNo" value="no"  onchange="displayAuthorizedRepresenatitive();" />
						<spring:message code="ssap.page4.noOneHelping" />
					</label>
					<div id="authorizedRepresentativeHelpDivErrorDiv"></div>
				</div><!-- control-group ends -->
				
				<p><spring:message code="ssap.page4.helpText" /></p>
			</div><!-- helpQuestionDiv end-->
			
			
			<div id="brokerDiv" class="hide">
				<h4><spring:message code="ssap.page4.whoIsHelpingYouHealthBroker" text="Health Insurance Broker" /></h4>
				
				<%-- <p class="gutter10">
					<spring:message code="ssap.page4.whoIsHelpingYouHealthBrokerAuth" text="We show that you have authorized the following health insurance broker:" />
				</p> --%>
				
				<div class="control-group margin5-l">
					<label for="brokerName54" class="control-label">
						<spring:message code="ssap.page4.whoIsHelpingYouHealthBrokerName" text="Health Insurance Broker Name" />
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="brokerName54" name="brokerName54" class="input-medium" disabled />
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group margin5-l">
					<label for="brokerID54" class="control-label">
						<spring:message code="ssap.page4.whoIsHelpingYouHealthBrokerId" text="Health Insurance Broker ID" />
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="brokerID54" name="brokerID54" class="input-medium" disabled />
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
			</div>
			
			
			
			<div id="guideDiv" class="hide">
				<h4><spring:message code="ssap.page4.whoIsHelpingHealthCoverageGuide" text="Health Coverage Guide" /></h4>
				
				<%-- <p class="gutter10">
					<spring:message code="ssap.page4.whoIsHelpingHealthCoverageGuideAuth" text="We show that you have authorized the following health coverage guide:" />
				</p> --%>
				
				<div class="control-group margin5-l">
					<label for="guideName54" class="control-label">
						<spring:message code="ssap.page4.whoIsHelpingHealthCoverageGuideName" text="Health Coverage Guide Name" />
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="guideName54" name="guideName54" disabled />
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group margin5-l">
					<label for="guideID54"  class="control-label">
						<spring:message code="ssap.page4.whoIsHelpingHealthCoverageGuideID" text="Health Coverage Guide ID:" />
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="guideID54" name="guideID54" class="input-medium" disabled />
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<%-- <p class="gutter10">
					<spring:message code='ssap.page4.whoIsHelpingYouToFindHealthBroker' text='To find a new health insurance broker' /> <spring:message code="ssap.page4.whoIsHelpingYouCleckHere" text="click here." />
				</p> --%>
			</div><!-- guideDiv ends -->
			
			
			<div id="authorizedRepresentativeDiv" class="hide">
				<h4><spring:message code="ssap.page4.whoIsHelpingYouAuthorizedRepresentative" text="Authorized Representative" /></h4>
				<p class="gutter10">
					<spring:message code="ssap.page4.whoIsHelpingYouAuthorizedRepresentativeText1" /> 
					<spring:message code="ssap.page4.whoIsHelpingYouAuthorizedRepresentativeText2" />
				</p>
				<fieldset>
				<legend class="gutter10">
					<strong>
						<spring:message code="ssap.page4.whoIsHelpingYouSomeoneElseAuthorizedRepresentative" text="Do you want to name someone as your authorized representative?" />
					</strong>
				</legend>
			
				<div class="control-group margin10-l">
					<label for="displayAuthRepresentativeInfo">
						<input type="radio" id="displayAuthRepresentativeInfo" name="authorizedRepresentativeIndicator" value="yes" onchange="displayAuthInfo();" data-parsley-errors-container="#displayAuthRepresentativeInfoErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" />
						<spring:message code="ssap.page4.whoIsHelpingYouAuthRepreIndicatorYes" text="Yes" />
					</label>
					<label for="hideAuthRepresentativeInfo">
						<input type="radio" id="hideAuthRepresentativeInfo" name="authorizedRepresentativeIndicator" value="no" checked  onchange="displayAuthInfo();"/>
						<spring:message code="ssap.page4.whoIsHelpingYouAuthRepreIndicatorNo" text="No" />
					</label>
					<div id="displayAuthRepresentativeInfoErrorDiv"></div>
				</div><!-- control-group ends -->
			</fieldset>
				<div id="authPerInfoPage54" class="hide">
					<h4>
						<span class="appscr54_authorizedName"><spring:message code="ssap.page4.whoIsHelpingYouAuthRepre" text="Authorized Representative" /></span>&nbsp;
						<spring:message code="ssap.page4.whoIsHelpingYouAuthRepreContactInfo" text="Contact Information" />
					</h4>
					
					<div class="control-group">
						<label for="authorizedFirstName" class="required control-label">
							<spring:message code='ssap.firstName' text='First Name' /> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png">
						</label><!-- end of label -->
						<div class="controls">
							<input type="text" placeholder="<spring:message code='ssap.firstName' text='First Name' />" class="input-large" onkeyup="setAuthorizedName()" id="authorizedFirstName" name="authorizedFirstName" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.firstNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.firstNameRequired'/>" />
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label for="authorizedMiddleName" class="control-label">
							<spring:message code='ssap.middleName' text='Middle Name' />
						</label><!-- end of label -->
						<div class="controls">
							<input type="text" placeholder="<spring:message code='ssap.middleName' text='Middle Name' />" class="input-large" onkeyup="setAuthorizedName()" id="authorizedMiddleName" name="authorizedMiddleName" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.middleNameValid'/>"/>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label for="authorizedLastName" class="required control-label">
							<spring:message code='ssap.lastName' text='Last Name' /> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png">
						</label><!-- end of label -->
						<div class="controls">
							<input type="text" placeholder="<spring:message code='ssap.lastName' text='Last Name' />" class="input-large" onkeyup="setAuthorizedName()" id="authorizedLastName" name="authorizedLastName" data-parsley-pattern="^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$"  data-parsley-pattern-message="<spring:message code='ssap.error.lastNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.lastNameRequired'/>" />
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label for="authorizedSuffix" class="control-label">
							<spring:message code="ssap.suffix" text="Suffix" />
						</label><!-- end of label -->
						<div class="controls">
							<select class="input-medium" id="authorizedSuffix" name="authorizedSuffix">
								<option value=""><spring:message code="ssap.suffix" text="Suffix" /></option>
								<option value="Sr."><spring:message code="ssap.Sr" text="Sr." /></option>
								<option value="Jr."><spring:message code="ssap.Jr" text="Jr." /></option>
								<option value="III"><spring:message code="ssap.III" text="III" /></option>
								<option value="IV"><spring:message code="ssap.IV" text="IV" /></option>
							</select>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label for="authorizedEmailAddress" class="required control-label">
							<spring:message code="ssap.page4.whoIsHelpingYouAuthRepreEmailAddress" text="Email Address" /> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" >
						</label><!-- end of label -->
						<div class="controls">
							<input type="text" placeholder="<spring:message code='ssap.page4.whoIsHelpingYouAuthRepreEmailAddress' text='Email Address' />" class="input-large" id="authorizedEmailAddress" name="authorizedEmailAddress"  data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.emailRequired'/>" data-parsley-pattern="^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$"  data-parsley-pattern-message="<spring:message code='ssap.error.emailValid'/>" data-parsley-length="[5, 100]" data-parsley-length-message="<spring:message code='ssap.error.emailValid'/>" />
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<h4>
						<span class="appscr54_authorizedName"><spring:message code="ssap.page4.whoIsHelpingYouAuthRepre" text="Authorized Representative" /></span>&nbsp;
						<spring:message code="ssap.page4.whoIsHelpingYouAuthRepreHomeAddress" text="Home Address" />
					</h4>
					
					<div class="control-group">
						<label for="authorizedAddress1" class="required control-label">
							<spring:message code='ssap.address1' text='Address 1' />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label><!-- end of label -->
						<div class="controls">
							<input type="text" id="authorizedAddress1" name="authorizedAddress1" class="input-large" placeholder="<spring:message code='ssap.address1' text='Address 1' />" data-parsley-pattern="^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{5,75}$"  data-parsley-pattern-message="<spring:message code='ssap.error.addressValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.addressRequired'/>" />
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label for="authorizedAddress2" class="control-label">
							<spring:message code='ssap.address2' text='Address 2' />
						</label><!-- end of label -->
						<div class="controls">
							<input type="text" id="authorizedAddress2" name="authorizedAddress2" class="input-large" placeholder="<spring:message code='ssap.address2' text='Address 2' />" data-parsley-pattern="^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{5,75}$"  data-parsley-pattern-message="<spring:message code='ssap.error.addressValid'/>"/>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label for="authorizedCity" class="required control-label">
							<spring:message code='ssap.city' text='City' />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label><!-- end of label -->
						<div class="controls">
							<input type="text" id="authorizedCity" name="authorizedCity" class="input-large" placeholder="<spring:message code='ssap.city' text='City' />"  data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.cityRequired'/>"  data-parsley-pattern="^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$"  data-parsley-pattern-message="<spring:message code='ssap.error.cityValid'/>" data-parsley-length="[1, 100]" data-parsley-length-message="<spring:message code='ssap.error.cityValid'/>" />
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label for="authorizedZip" class="required control-label">
							<spring:message code='ssap.zip' text='Zip' />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label><!-- end of label -->
						<div class="controls">
							<input type="text" id="authorizedZip" name="authorizedZip" class="input-large" placeholder="<spring:message code='ssap.zip' text='Zip' />" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.zipRequired'/>" data-parsley-pattern="^[0-9]{5}(\-[0-9]{4})?$"  data-parsley-pattern-message="<spring:message code='ssap.error.zipValid'/>" />
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label for="authorizedState" class="required control-label">
							<spring:message code="ssap.State" text="State" />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label><!-- end of label -->
						<div class="controls">
							<select data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.stateRequired'/>" name="authorizedState" id="authorizedState" class="input-large">
								<option value=''><spring:message code="ssap.State" text="State" /></option>
								<option value='AL'><spring:message code="ssap.state.Albama" text="Alabama" /></option>
								<option value='AK'><spring:message code="ssap.state.Alaska" text="Alaska" /></option>
								<option value='AS'><spring:message code="ssap.state.AmericanSamoa" text="American Samoa" /></option>
								<option value='AZ'><spring:message code="ssap.state.Arizona" text="Arizona" /></option>
								<option value='AR'><spring:message code="ssap.state.Arkansas" text="Arkansas" /></option>
								<option value='CA'><spring:message code="ssap.state.California" text="California" /></option>
								<option value='CO'><spring:message code="ssap.state.Colorado" text="Colorado" /></option>
								<option value='CT'><spring:message code="ssap.state.Connecticut" text="Connecticut" /></option>
								<option value='DE'><spring:message code="ssap.state.Delaware" text="Delaware" /></option>
								<option value='DC'><spring:message code="ssap.state.DistrictOfColumbia" text="District of Columbia" /></option>
								<option value='FL'><spring:message code="ssap.state.Florida" text="Florida" /></option>
								<option value='GA'><spring:message code="ssap.state.Georgia" text="Georgia" /></option>
								<option value='GU'><spring:message code="ssap.state.Guam" text="Guam" /></option>
								<option value='HI'><spring:message code="ssap.state.Hawaii" text="Hawaii" /></option>
								<option value='ID'><spring:message code="ssap.state.Idaho" text="Idaho" /></option>
								<option value='IL'><spring:message code="ssap.state.Illinois" text="Illinois" /></option>
								<option value='IN'><spring:message code="ssap.state.Indiana" text="Indiana" /></option>
								<option value='IA'><spring:message code="ssap.state.Iowa" text="Iowa" /></option>
								<option value='KS'><spring:message code="ssap.state.Kansas" text="Kansas" /></option>
								<option value='KY'><spring:message code="ssap.state.Kentucky" text="Kentucky" /> </option>
								<option value='LA'><spring:message code="ssap.state.Louisiana" text="Louisiana" /></option>
								<option value='ME'><spring:message code="ssap.state.Maine" text="Maine" /></option>
								<option value='MD'><spring:message code="ssap.state.Maryland" text="Maryland" /></option>
								<option value='MA'><spring:message code="ssap.state.Massachusetts" text="Massachusetts" /></option>
								<option value='MI'><spring:message code="ssap.state.Michigan" text="Michigan" /></option>
								<option value='MN'><spring:message code="ssap.state.Minnesota" text="Minnesota" /></option>
								<option value='MS'><spring:message code="ssap.state.Mississippi" text="Mississippi" /></option>
								<option value='MO'><spring:message code="ssap.state.Missouri" text="Missouri" /></option>
								<option value='MT'><spring:message code="ssap.state.Montana" text="Montana" /></option>
								<option value='NE'><spring:message code="ssap.state.Nebraska" text="Nebraska" /></option>
								<option value='NV'><spring:message code="ssap.state.Nevada" text="Nevada" /></option>
								<option value='NH'><spring:message code="ssap.state.NewHampshire" text="New Hampshire" /></option>
								<option value='NJ'><spring:message code="ssap.state.NewJersey" text="New Jersey" /></option>
								<option value='NM'><spring:message code="ssap.state.NewMexico" text="New Mexico" /></option>
								<option value='NY'><spring:message code="ssap.state.NewYork" text="New York" /></option>
								<option value='NC'><spring:message code="ssap.state.NorthCarolina" text="North Carolina" /></option>
								<option value='ND'><spring:message code="ssap.state.NorthDakota" text="North Dakota" /></option>
								<option value='MP'><spring:message code="ssap.state.NorthernMarianasIslands" text="Northern Marianas Islands" /></option>
								<option value='OH'><spring:message code="ssap.state.Ohio" text="Ohio" /></option>
								<option value='OK'><spring:message code="ssap.state.Oklahoma" text="Oklahoma" /></option>
								<option value='OR'><spring:message code="ssap.state.Oregon" text="Oregon" /></option>
								<option value='PA'><spring:message code="ssap.state.Pennsylvania" text="Pennsylvania" /></option>
								<option value='PR'><spring:message code="ssap.state.PuertoRico" text="Puerto Rico" /></option>
								<option value='RI'><spring:message code="ssap.state.RhodeIsland" text="Rhode Island" /></option>
								<option value='SC'><spring:message code="ssap.state.SouthCarolina" text="South Carolina" /></option>
								<option value='SD'><spring:message code="ssap.state.SouthDakota" text="South Dakota" /></option>
								<option value='TN'><spring:message code="ssap.state.Tennessee" text="Tennessee" /></option>
								<option value='TX'><spring:message code="ssap.state.Texas" text="Texas" /></option>
								<option value='UT'><spring:message code="ssap.state.Utah" text="Utah" /></option>
								<option value='VT'><spring:message code="ssap.state.Vermont" text="Vermont" /></option>
								<option value='VA'><spring:message code="ssap.state.Virginia" text="Virginia" /></option>
								<option value='VI'><spring:message code="ssap.state.VirginIslands" text="Virgin Islands" /></option>
								<option value='WA'><spring:message code="ssap.state.Washington" text="Washington" /></option>
								<option value='WV'><spring:message code="ssap.state.WestVirginia" text="West Virginia" /></option>
								<option value='WI'><spring:message code="ssap.state.Wisconsin" text="Wisconsin" /></option>
								<option value='WY'><spring:message code="ssap.state.Wyoming" text="Wyoming" /></option>
							</select>
							<span class="errorMessage"></span>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<h4>
						<span class="appscr54_authorizedName"><spring:message code="ssap.page4.whoIsHelpingYouAuthRepre" text="Authorized Representative" /></span>&nbsp;
						<spring:message code="ssap.page4.whoIsHelpingYouAuthReprePhone" text="Phone" />
					</h4>
					
					<div class="control-group">
						<label for="appscr54_phoneNumber" class="control-label">
							<spring:message code="ssap.cellPhone" text="Cell" />
						</label><!-- end of label -->
						<div class="controls">
							<div class="pull-left">
								<input type="text" class="input-medium phoneNumber" placeholder="(xxx) xxx-xxxx" id="appscr54_phoneNumber" name="appscr54_phoneNumber" data-parsley-pattern="^\(\d{3}\) \d{3}-\d{4}$" data-parsley-errors-container="#appscr54_phoneNumberErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumValid'/>"/>
							</div>
							<div class="pull-left margin5-l" style="visibility:hidden">
								<input type="text" class="input-mini phoneNumberExt" id="appscr54_ext" name="appscr54_ext" placeholder="<spring:message code='ssap.phoneExt' text='Ext.' />" data-parsley-pattern="^\d{4}$" data-parsley-errors-container="#appscr54_extErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumExtValid'/>"/>
							</div>
							<div id="appscr54_phoneNumberErrorDiv"></div>
							<div id="appscr54_extErrorDiv"></div>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label for="appscr54_homePhoneNumber" class="control-label">
							<spring:message code="ssap.homePhone" text="Home" />
						</label><!-- end of label -->
						<div class="controls">
							<div class="pull-left">
								<input type="text" class="input-medium phoneNumber" placeholder="(xxx) xxx-xxxx" id="appscr54_homePhoneNumber" name="appscr54_homePhoneNumber" data-parsley-pattern="^\(\d{3}\) \d{3}-\d{4}$" data-parsley-errors-container="#appscr54_homePhoneNumberErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumValid'/>"/>
							</div>
							<div class="pull-left margin5-l">
								<input type="text" class="input-mini phoneNumberExt" id="appscr54_homeExt" name="appscr54_homeExt" placeholder="<spring:message code='ssap.phoneExt' text='Ext.' />" data-parsley-pattern="^\d{4}$" data-parsley-errors-container="#appscr54_homeExtErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumExtValid'/>"/>
							</div>
							<div id="appscr54_homePhoneNumberErrorDiv"></div>
							<div id="appscr54_homeExtErrorDiv"></div>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label for="appscr54_workPhoneNumber" class="control-label">
							<spring:message code="ssap.workPhone" text="Work" />
						</label><!-- end of label -->
						<div class="controls">
							<div class="pull-left">
								<input type="text" class="input-medium phoneNumber" placeholder="(xxx) xxx-xxxx" id="appscr54_workPhoneNumber" name="appscr54_workPhoneNumber" data-parsley-pattern="^\(\d{3}\) \d{3}-\d{4}$" data-parsley-errors-container="#appscr54_workPhoneNumberErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumValid'/>"/>
							</div>
							<div class="pull-left margin5-l">
								<input type="text" class="input-mini phoneNumberExt" id="appscr54_workExt" placeholder="<spring:message code='ssap.phoneExt' text='Ext.' />" name="appscr54_workExt" data-parsley-pattern="^\d{4}$" data-parsley-errors-container="#appscr54_workExtErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumExtValid'/>"/>
							</div>
							<div id="appscr54_workPhoneNumberErrorDiv"></div>
							<div id="appscr54_workExtErrorDiv"></div>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<p class="gutter10">
						<spring:message code="ssap.page4.whoIsHelpingYouPartOfOrganization" text="Is this person part of an organization helping you apply for health insurance? " />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</p>
					
					<div class="control-group">
						<label for="cmpnyNameAndOrgRadio54_1" class="control-label">
							 <input type="radio" name="isPersonHelpingyou4ApplingHealthInsurance" value="yes" onchange="nameOfCompanyAndOrg54();" id="cmpnyNameAndOrgRadio54_1" data-parsley-errors-container = "#isPersonHelpingyou4ApplingHealthInsuranceErrorDiv"data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field'/>" />
							 <spring:message code="ssap.page4.whoIsHelpingYouPartOfOrganizationYes" text="Yes" />
						</label>
						<label for="cmpnyNameAndOrgRadio54_2" class="control-label">
							<input type="radio" name="isPersonHelpingyou4ApplingHealthInsurance" value="no" onchange="nameOfCompanyAndOrg54();" id="cmpnyNameAndOrgRadio54_2"  />
							<spring:message code="ssap.page4.whoIsHelpingYouPartOfOrganizationNo" text="No" />
						</label>
						<div id ="isPersonHelpingyou4ApplingHealthInsuranceErrorDiv"></div>
					</div><!-- control-group ends -->
					
					<div id="cmpnyNameAndOrgDiv" class="hide">
						<div class="control-group">
							<label for="authorizeCompanyName" class="control-label">
								<spring:message code="ssap.page4.whoIsHelpingYouCompanyName" text="Company Name" />
							</label><!-- end of label -->
							<div class="controls">
								<input type="text" class="input-medium" id="authorizeCompanyName" name="authorizeCompanyName" placeholder="<spring:message code='ssap.page4.whoIsHelpingYouCompanyName' text='Company Name' />" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.companyRequired'/>"  data-parsley-length="[8, 256]" data-parsley-length-message="<spring:message code='ssap.error.companyValid'/>"/>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						
						<div class="control-group">
							<label for="authorizeOrganizationId" class="control-label">
								<spring:message code="ssap.page4.whoIsHelpingYouCOrganizationId" text="Organization ID" />
							</label><!-- end of label -->
							<div class="controls">
								 <input type="text" class="input-medium" id="authorizeOrganizationId" name="authorizeOrganizationId" placeholder="<spring:message code='ssap.page4.whoIsHelpingYouCOrganizationId' text='Organization ID' />" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.organizationRequired'/>"  data-parsley-length="[1, 20]" data-parsley-length-message="<spring:message code='ssap.error.organizationValid'/>" />
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
					</div>
					<p class="gutter10">
						<spring:message code="ssap.page4.whoIsHelpingYouSomeoneYourAuthRepText1" text="To make someone your authorized representative, " />
						<strong class="primaryContactName camelCaseName"></strong>
						<spring:message code="ssap.page4.whoIsHelpingYouSomeoneYourAuthRepText2" text="needs to sign here or provider proof of a legal reason that" /> 
						<strong class="appscr54_authorizedName"><spring:message code="ssap.page4.whoIsHelpingYouAuthRepre" text="Authorized Representative" /></strong>
						<spring:message code="ssap.page4.whoIsHelpingYouSomeoneYourAuthRepText3" text="can represent" /> <strong class="primaryContactName camelCaseName"></strong>.
					</p>
					<fieldset class="gutter10">
						<legend>
							<spring:message code="ssap.page4.whoIsHelpingYouSelectYourOptionBelow" text="Select an option below" />
						</legend>					
						<div class="control-group clearfix">
							<label for="makeOtherizedRepresentativeSignature" class="radio">
								<input type="radio" name="makeOtherizedRepresentative" id="makeOtherizedRepresentativeSignature" value="signature" onclick="displaySigniture();" data-parsley-errors-container ="#makeOtherizedRepresentativeSignatureErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field'/>"/>
								<spring:message code="ssap.page4.whoIsHelpingYouSignature" text="Signature" />
							</label>
							<div for="authorizeSignature" class="hide radio" id="signitureLabel">
								<input type="text" id="authorizeSignature" name="authorizeSignature" class="input-medium" 
								placeHolder="<spring:message code='ssap.page4.whoIsHelpingYouTypeSignature' text='Type Signature' />" 
								data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.signitureRequired'/>"  
								data-parsley-length="[1, 45]" data-parsley-length-message="<spring:message code='ssap.error.signitureValid'/>"/>
							</div>
						</div>
					</fieldset><!-- control-group ends -->
				
				
					<div class="control-group clearfix">

								<div for="makeOtherizedRepresentativeLater" class="pull-left margin10-l radio">
									<input name="makeOtherizedRepresentative" id="makeOtherizedRepresentativeLater" type="radio" 
									aria-label="Authorize later option" value="later" onclick="displaySigniture();"/>
								</div>
								<p class="pull-left span11 margin0 capitalize-none">
									<spring:message code='ssap.page4.whoIsHelpingYouSubmitDocument' text='Submit document later for proof. You will have the opportunity to submit documentation at the end of the application or from the my Documents page in My Account.' />
								</p>
						
						<div id="makeOtherizedRepresentativeSignatureErrorDiv"></div>
					</div><!-- control-group ends -->
				</div><!-- hide end -->
			</div>
			
		
		
			
			
			<%-- <p class="gutter10">
				<spring:message code="ssap.page4.whoIsHelpingYouToFindHealthBroker" text="To find a new health insurance broker" /> <spring:message code="ssap.page4.whoIsHelpingYouCleckHere" text="click here." />
			</p>
			 --%>

		</form>
	</div>
</div>

