package com.getinsured.hix.admin.web;

public class CsvFormatBean {
	private String regionName=null;
	private String maxAge=null;
	private String minAge=null;
	private String tobaco=null;
	private String premimumpmpm=null;
	
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getMaxAge() {
		return maxAge;
	}
	public void setMaxAge(String maxAge) {
		this.maxAge = maxAge;
	}
	public String getMinAge() {
		return minAge;
	}
	public void setMinAge(String minAge) {
		this.minAge = minAge;
	}
	public String getTobaco() {
		return tobaco;
	}
	public void setTobaco(String tobaco) {
		this.tobaco = tobaco;
	}
	public String getPremimumpmpm() {
		return premimumpmpm;
	}
	public void setPremimumpmpm(String premimumpmpm) {
		this.premimumpmpm = premimumpmpm;
	}
}
