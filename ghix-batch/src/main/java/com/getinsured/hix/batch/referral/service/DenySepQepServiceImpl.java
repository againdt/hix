/**
 *
 */
package com.getinsured.hix.batch.referral.service;


import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import java.util.List;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;


import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.hix.batch.referral.ReferralBatchResponse;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration.IEXConfigurationEnum;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.google.gson.Gson;
import org.springframework.http.HttpStatus;
import com.getinsured.iex.util.IndividualPortalUtil;

/**
 * @author Deepa
 *
 */
@Service("denySepQepService")
@DependsOn("dynamicPropertiesUtil")
public class DenySepQepServiceImpl implements DenySepQepService {

	private static final Logger LOGGER = Logger.getLogger(DenySepQepServiceImpl.class);

	private static final int IDX_SSAP_ZERO = 0;
	private static final int IDX_SSAP_ONE = 1;
	private static final int IDX_SSAP_FOUR = 4;
	
	private static int enrollmentGracePeriod;

	private static int numberOfenrollmentDays = 60;
	
	private final String BATCH_SEP_QEP_DENIAL = "BATCH_SEP_QEP_DENIAL";
	private final String APPLICATION = "application";
	private final String CASENUMBER = "casenumber";
	

	@Value("#{configProp['database.type']}")
	private String dbType;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired private IUserRepository iUserRepository;
	
	@Autowired private Gson platformGson;
	
	@Autowired
	private IntegrationLogService integrationLogService;
	
	@Autowired
	private IndividualPortalUtil individualPortalUtil;
	
	@PostConstruct
	public void doPost() {
		enrollmentGracePeriod = 9;
		String enrollmentGracePeriodstr = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
		if (!StringUtils.isEmpty(enrollmentGracePeriodstr)) {
			enrollmentGracePeriod = Integer.parseInt(enrollmentGracePeriodstr);
		}
	}

	@Override
	public void overDueSepQepDenial() {
		List<Object[]> ssapApplicationDTOList = null;
		ReferralBatchResponse batchResponse = new ReferralBatchResponse();
		String response = GhixConstants.RESPONSE_SUCCESS;

		try {
			batchResponse.setStartTime(new Date());
			batchResponse.setBatchName(BATCH_SEP_QEP_DENIAL);
			DateTime curJodaTime = new DateTime();
			curJodaTime = curJodaTime.minusDays(enrollmentGracePeriod + numberOfenrollmentDays);
			curJodaTime = setJodaTime(curJodaTime);
			DateTime currentDateTime = new DateTime();
			currentDateTime = setJodaTime(currentDateTime);
			Date creationWindowDate = curJodaTime.toDate();
			Date currentDate = currentDateTime.toDate();
			if(dbType.equalsIgnoreCase("Oracle")){
				ssapApplicationDTOList = ssapApplicationRepository.findOverDueSEPQEP_Oracle(new Timestamp(creationWindowDate.getTime()),new Timestamp(currentDate.getTime()));
			}else if(dbType.equalsIgnoreCase("POSTGRESQL")){
				ssapApplicationDTOList = ssapApplicationRepository.findOverDueSEPQEP_Postgres(new Timestamp(creationWindowDate.getTime()),new Timestamp(currentDate.getTime()));
			} else {
				LOGGER.error("Unknown db type found in configuration.properties: " + dbType );
				throw new GIRuntimeException("Unknown db type found in configuration.properties: " + dbType);
			}
			
			processBatchDenial(ssapApplicationDTOList, batchResponse);
		} catch (Exception ex) {
			response = GhixConstants.RESPONSE_FAILURE;
			LOGGER.error("ERR: WHILE PROCESSING OVER DUE SEP QEP DENIAL: ", ex);
		} finally {
			batchResponse.setEndTime(new Date());
			logData(batchResponse, response, BATCH_SEP_QEP_DENIAL);
		}
	}
	
	private DateTime setJodaTime(DateTime time){
		DateTime localtime = null;
		localtime = time.withMinuteOfHour(0);
		localtime = localtime.withSecondOfMinute(0);
		localtime = localtime.withMillisOfSecond(0);
		return localtime;
	}
	/**
	 * @param ssapApplicationDTOList
	 */
	private void processBatchDenial(List<Object[]> ssapApplicationDTOList, ReferralBatchResponse batchResponse) {

		if (null != ssapApplicationDTOList && !ssapApplicationDTOList.isEmpty()) {
			AccountUser user = getUser();
			final Integer userId = user.getId();
			final String userName = user.getUserName();
			for (Object[] ssapApplicationDTO : ssapApplicationDTOList) {
				String coverageYear = ssapApplicationDTO[IDX_SSAP_FOUR] != null ? ssapApplicationDTO[IDX_SSAP_FOUR].toString() : null;
				String currentCovergeYear = DynamicPropertiesUtil.getPropertyValue(IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
				if (!coverageYear.equals(currentCovergeYear) || (coverageYear.equals(currentCovergeYear)
						&& !individualPortalUtil.isInsideOEWindow(new Integer(currentCovergeYear)))) {
					processApplication(userId, userName, ssapApplicationDTO, batchResponse);
				}
			}
		}
	}
	
	/**
	 * @param userId
	 * @param ssapApplicationDTO
	 */
	private void processApplication(Integer userId, String userName, Object[] ssapApplicationDTO, ReferralBatchResponse batchResponse) {
		Long applicationId = 1L;
		String caseNumber = null;
		
		boolean response = false;
		try {
			applicationId = ((Long) ssapApplicationDTO[IDX_SSAP_ZERO]).longValue();
			caseNumber =   ssapApplicationDTO[IDX_SSAP_ONE]!=null?   ssapApplicationDTO[IDX_SSAP_ONE].toString():null;
			response = denyApplication(caseNumber, userId,userName);
		} catch (Exception ex) {
			response = false;
			LOGGER.error("ERR: WHILE DENYING OVER DUE SEP QEP APPLICATION: ", ex);
		} finally {
			populateBatchResponse(applicationId, caseNumber, response, batchResponse);
		}
	}
	
	private boolean denyApplication(final String caseNumber, final Integer userId, final String userName) {
		boolean response = false;
		Object[] ssapApplicationDTO = ssapApplicationRepository.findApplicationByCaseNumberAndERStatus(caseNumber);
		ResponseEntity<Object> responseEntity = null;
		Long applicationId = 0L;
		if(ssapApplicationDTO!=null && ssapApplicationDTO.length!=0) {
			applicationId = ((Long) ssapApplicationDTO[IDX_SSAP_ZERO]).longValue();
		}
		if(applicationId!=0L) {
			responseEntity = ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"/ssap/application/"+caseNumber+"/deny", userName, HttpMethod.POST, MediaType.APPLICATION_JSON, Object.class, userId);
		}
		if(applicationId==0L) {
			response = false;
		}
		else if(responseEntity==null || (responseEntity!=null && HttpStatus.OK!=responseEntity.getStatusCode())) {
			LOGGER.error("Unable to deny for " + caseNumber);
			throw new GIRuntimeException("UNABLE TO DENY FOR SSAP ID:" + caseNumber);
		} else {
			 LinkedHashMap<String, Object>  responseMap = (LinkedHashMap<String, Object>) responseEntity.getBody();
			 response = (Boolean) responseMap.get("present");
		}
		return response;
	}
	
	private void populateBatchResponse(Long applicationId, String caseNumber, boolean response, ReferralBatchResponse batchResponse) {
		Map<String, String> successFailureApps = new HashMap<String, String>();

		successFailureApps.put(APPLICATION, String.valueOf(applicationId));
		successFailureApps.put(CASENUMBER, caseNumber);
		
        if(response){
			batchResponse.getSuccessfullApplications().add(successFailureApps);
		}else {
			batchResponse.getFailedApplications().add(successFailureApps);
		}
	}
	
	/**
	 *
	 * @return
	 */
	private AccountUser getUser() {
		AccountUser updateUser = iUserRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
		return updateUser;
	}
	
	private void logData(ReferralBatchResponse message, String status, String serviceName) {
		integrationLogService.save(platformGson.toJson(message), serviceName, status, null, null);
	}

}
