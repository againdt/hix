package com.getinsured.iex.hub.vlp.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.ArrayOfEmpAuthDataType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.EmpAuthDataType;
import com.getinsured.iex.hub.platform.HubServiceException;

/**
 * Wrapper Class for ArrayOfEmpAuthDataType
 * 
 * @author Vishaka Tharani
 * @since  18-Feb-2014 
 * 
 */
public class ArrayOfEmpAuthDataTypeWrapper implements JSONAware{
	
	private List<EmpAuthDataTypeWrapper> empAuthDataTypewrapperList;
	private static final String EMP_AUTH_DATA_KEY = "empAuthData";
	
	public ArrayOfEmpAuthDataTypeWrapper(ArrayOfEmpAuthDataType arrayOfEmpAuthDataType) throws HubServiceException{
		if(arrayOfEmpAuthDataType == null){
			throw new HubServiceException("No sponsorshipData provided");
		}
		
		List<EmpAuthDataType> empAuthDataTypeList = arrayOfEmpAuthDataType.getEmpAuthData();
		if(empAuthDataTypeList != null && !empAuthDataTypeList.isEmpty())
		{
			empAuthDataTypewrapperList = new ArrayList<EmpAuthDataTypeWrapper>();
			for(EmpAuthDataType empAuthDataType : empAuthDataTypeList){
				EmpAuthDataTypeWrapper empAuthDataTypeWrapper = new EmpAuthDataTypeWrapper(empAuthDataType); 
				empAuthDataTypewrapperList.add(empAuthDataTypeWrapper);
			}
		}
		
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		JSONArray empAuthData = null;
		
		if(empAuthDataTypewrapperList != null && !empAuthDataTypewrapperList.isEmpty()){
			 empAuthData = new JSONArray();
		}
		for(EmpAuthDataTypeWrapper data : this.empAuthDataTypewrapperList){
			empAuthData.add(data.toJSONString());
		}
		obj.put(EMP_AUTH_DATA_KEY,empAuthData);
		return obj.toJSONString();
	}
}
