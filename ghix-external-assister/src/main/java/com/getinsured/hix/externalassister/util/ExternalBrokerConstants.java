package com.getinsured.hix.externalassister.util;

import org.springframework.stereotype.Component;

@Component
public final class ExternalBrokerConstants {
	
	public static final int ERROR_CODE_200 = 200;
    public static final int ERROR_CODE_201 = 201;
    public static final int ERROR_CODE_202 = 202;
    public static final int ERROR_CODE_203 = 203;
    public static final int ERROR_CODE_204 = 204;
    public static final int ERROR_CODE_205 = 205;
    public static final int ERROR_CODE_206 = 206;
    public static final int ERROR_CODE_207 = 207;
    public static final int ERROR_CODE_208 = 208;
    public static final int ERROR_CODE_209 = 209;
    public static final int ERROR_CODE_210 = 210;
    public static final int ERROR_CODE_211 = 211;
    public static final int ERROR_CODE_212 = 212;
    
    
    public static final String ERR_MSG_REQUEST_IS_NULL = "Invalid input: Request is Null";
    public static final String ERR_MSG_IN_DESIGNATING_BROKER_API = "Failed to designate or dedesignate the external broker";
    public static final String ERR_MSG_IN_GET_ASSISTER_DETAIL = "Failed to get assister detail for the external broker id";
    public static final String ERR_MSG_IN_HOUSEHOLDS_FOR_ASSISTER = "Failed to get households for the assister";
    public static final String ACTION_ADD="Add";
    public static final String ACTION_REMOVE="Remove";
    public static final String ENROLLMENT_TYPE_INDIVIDUAL = "FI";
    public static final String EXTERNAL_ASSISTER= "BROKER";
    public static final String AGENT_ROLE = "Agent";
    public static final String MSG_ENROLLMENT_UPDATE_SUCCESS = "Enrollment updated successfully";
    
    public static final String JSON_DATE_FORMAT = "MMM dd, yyyy hh:mm:ss a";

}
