<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/secure-url.tld" prefix="surl"%>
<%@ page import="java.util.HashMap" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.model.Broker"%>
<%
	String CA_STATE_CODE_STR = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	Boolean brokerConnectEnabled = Boolean.valueOf(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.IEX_BROKER_CONNECT_ENABLED));
	Broker designatedBrokerProfile = (Broker) (request.getAttribute("designatedBrokerProfile"));
%>
<c:set var="CA_STATE_CODE_STR" value="<%=CA_STATE_CODE_STR%>" />
<c:set var="designatedBrokerProfile" value="<%=designatedBrokerProfile%>" />
<c:set var="brokerConnectEnabled" value="<%=brokerConnectEnabled%>" />
<!-- #sidebar -->

<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 span3" id="sidebar">
	<div class="header">
		<h4>
			<spring:message code="label.brkSteps" />
		</h4>
	</div>
	<ul class="nav nav-list">
		<jsp:useBean id="ecyptedModuleParamsMap" class="java.lang.String" />
		<c:choose>
			<c:when test="${sessionScope.brokerObjectForLeftNav != null && sessionScope.brokerObjectForLeftNav.certificationStatus != null && sessionScope.brokerObjectForLeftNav.certificationStatus !='' && sessionScope.brokerObjectForLeftNav.certificationStatus != 'Incomplete'}">
				<c:if test="${isAgencyManager || isAdminStaffL2}">
					<jsp:useBean id="moduleParamsMap" class="java.util.HashMap" />
					<c:set target="${moduleParamsMap}" property="brokerId" value="${sessionScope.brokerObjectForLeftNav.id}" />
					<surl:secureUrl paramMap="${moduleParamsMap}" var="refValue" />
					<c:set var="ecyptedModuleParamsMap" value="${refValue }" />
				</c:if>
				<li id="brkCertInfo"><a href="/hix/broker/viewcertificationinformation?ref=${ecyptedModuleParamsMap}">
					<spring:message code="label.brkBrokerInformation" /></a></li>
				<li id="brkProfile"><a href="/hix/broker/viewprofile?ref=${ecyptedModuleParamsMap}">
					<spring:message code="label.profile" /></a></li>
				<c:if test="${fn:toLowerCase(CA_STATE_CODE_STR) != 'ca'}">
					<c:if test="${isPaymentEnabled}">
						<li id="brkPaymentInfo"><a href="/hix/broker/viewpaymentinfo">
							<spring:message code="label.brkPaymentInformation" /></a></li>
					</c:if>
				</c:if>
			</c:when>
			<c:when test="${sessionScope.brokerObjectForLeftNav != null && sessionScope.brokerObjectForLeftNav.certificationStatus != null && sessionScope.brokerObjectForLeftNav.certificationStatus !='' && sessionScope.brokerObjectForLeftNav.certificationStatus == 'Incomplete'}">
				<c:choose>
					<c:when test="${sessionScope.brokerObjectForLeftNav.contactNumber != null}">
						<c:if test="${isAgencyManager}">
							<jsp:useBean id="moduleParamsMapLocal" class="java.util.HashMap" />
							<c:set target="${moduleParamsMapLocal}" property="brokerId" value="${sessionScope.brokerObjectForLeftNav.id}" />
							<surl:secureUrl paramMap="${moduleParamsMapLocal}" var="refValue" />
							<c:set var="ecyptedModuleParamsMap" value="${refValue }" />
							<li id="brkCertInfo"><a href="/hix/broker/certificationapplication?ref=${ecyptedModuleParamsMap}"><i class="icon-ok"></i>
								<spring:message code="label.brkBrokerInformation" /></a></li>
						</c:if>
						<c:if test="${not isAgencyManager}">
							<li id="brkCertInfo"><a href="#"><i class="icon-ok"></i>
								<spring:message code="label.brkBrokerInformation" /></a></li>
						</c:if>
					</c:when>
					<c:otherwise>
						<li id="brkCertInfo"><a href="#">1. &nbsp;
							<spring:message code="label.brkBrokerInformation" /></a></li>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${sessionScope.brokerObjectForLeftNav.clientsServed != null || sessionScope.brokerObjectForLeftNav.productExpertise != null || sessionScope.brokerObjectForLeftNav.education != null || sessionScope.brokerObjectForLeftNav.aboutMe != null }">
						<c:if test="${isAgencyManager}">
							<jsp:useBean id="moduleParamsMapLocalObj" class="java.util.HashMap" />
							<c:set target="${moduleParamsMapLocalObj}" property="brokerId" value="${sessionScope.brokerObjectForLeftNav.id}" />
							<surl:secureUrl paramMap="${moduleParamsMapLocalObj}" var="refValue" />
							<c:set var="ecyptedModuleParamsMap" value="${refValue }" />
							<li id="brkProfile"><a href="/hix/broker/buildprofile?ref=${ecyptedModuleParamsMap }"><i class="icon-ok"></i>
								<spring:message code="label.profile" /></a></li>
						</c:if>
						<c:if test="${not isAgencyManager}">
							<li id="brkProfile"><a href="#"><i class="icon-ok"></i>
								<spring:message code="label.profile" /></a></li>
						</c:if>
					</c:when>
					<c:otherwise>
						<li id="brkProfile"><a href="#">2. &nbsp;
							<spring:message code="label.profile" /></a></li>
					</c:otherwise>
				</c:choose>
				<c:if test="${isPaymentEnabled}">
					<c:choose>
						<c:when test="${sessionScope.paymentObjId != 0}">
							<li id="brkPaymentInfo"><a href="#"><i class="icon-ok"></i>
								<spring:message code="label.paymentinfo" /></a></li>
						</c:when>
						<c:otherwise>
							<li id="brkPaymentInfo"><a href="#">3. &nbsp;
								<spring:message code="label.paymentinfo" /></a></li>
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${sessionScope.brokerObjectForLeftNav.contactNumber != null}">
						<li id="brkCertInfo"><a href="/hix/broker/viewcertificationinformation"><i class="icon-ok"></i>
							<spring:message code="label.brkBrokerInformation" /></a></li>
					</c:when>
					<c:otherwise>
						<li id="brkCertInfo"><a href="#">1. &nbsp;
							<spring:message code="label.brkBrokerInformation" /></a></li>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${sessionScope.brokerObjectForLeftNav.clientsServed != null || sessionScope.brokerObjectForLeftNav.productExpertise != null || sessionScope.brokerObjectForLeftNav.education != null || sessionScope.brokerObjectForLeftNav.aboutMe != null }">
						<li id="brkProfile"><a href="/hix/broker/viewprofile"><i class="icon-ok"></i>
							<spring:message code="label.profile" /></a></li>
					</c:when>
					<c:otherwise>
						<li id="brkProfile"><a href="#">2. &nbsp;
							<spring:message code="label.profile" /></a></li>
					</c:otherwise>
				</c:choose>
				<c:if test="${isPaymentEnabled}">
					<c:choose>
						<c:when test="${sessionScope.paymentObjId != 0}">
							<li id="brkPaymentInfo"><a href="#"><i class="icon-ok"></i>
								<spring:message code="label.paymentinfo" /></a></li>
						</c:when>
						<c:otherwise>
							<li id="brkPaymentInfo"><a href="#">3. &nbsp;
								<spring:message code="label.paymentinfo" /></a></li>
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:otherwise>
		</c:choose>
		<c:if test="${sessionScope != null && sessionScope.brokerObjectForLeftNav != null && sessionScope.brokerObjectForLeftNav.certificationStatus != null && sessionScope.brokerObjectForLeftNav.certificationStatus != '' && sessionScope.brokerObjectForLeftNav.certificationStatus != 'Incomplete'}">
			<li id="brkCertStatus"><a href="<c:url value="/broker/certificationstatus?ref=${ecyptedModuleParamsMap}" />" >
				<spring:message code="label.brkCertificationStatus" /></a></li>
			<c:if test="${fn:toLowerCase(CA_STATE_CODE_STR) == 'ca'}">
				<jsp:useBean id="moduleParamsMapTwo" class="java.util.HashMap" />
				<c:set target="${moduleParamsMapTwo}" property="brokerId" value="${sessionScope.brokerObjectForLeftNav.id}" />
				<surl:secureUrl paramMap="${moduleParamsMapTwo}" var="refValue" />
				<c:set var="ecyptedModuleParamsMap" value="${refValue }" />
				<li id="brkStatus"><a href="<c:url value="/admin/broker/activitystatus?ref=${ecyptedModuleParamsMap}" />">
					<spring:message code="label.brkActivityStatus" /></a></li>
			</c:if>
		</c:if>
	</ul>
	<c:choose>
		<c:when test="${designatedBrokerProfile != null && designatedBrokerProfile.certificationStatus == 'Certified' && brokerConnectEnabled}">
			<div class="header">
				<h4>Broker Connect</h4>
			</div>
			<ul class="nav nav-list">
				<li id="brkHelpOnDemand"><a href="<c:url value="/broker/helpondemand#participation" />" >Participation Information</a></li>
				<li id="brkConnect"><a href="<c:url value="/broker/helpondemand#availability" />" >Availability</a></li>
			</ul>
		</c:when>
	</c:choose>
	<!-- 		<div class="margin10-t">
                 <p class="alert alert-info"><spring:message code="label.brkPleaseInformation"/></p>
             </div> -->
</div>
