/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.util.Date;
import java.util.List;

import org.springframework.batch.admin.service.JobService;

import com.getinsured.hix.model.enrollment.ReconDetailCsvDto;

/**
 * Enrollment Reconciliation test data file creation service
 * @author negi_s
 * @since 08/03/2017
 */
public interface EnrlReconTestDataCreationService {
	
	/**
	 * Generates test CSV file from enrollment data
	 * @param hiosIssuerId
	 * @param year
	 * @param jobService
	 * @param jobExecutionId 
	 * @throws Exception 
	 */
	void generateTestDataForIssuer(List<String> hiosIssuerId, int year, JobService jobService, long jobExecutionId) throws Exception;
	
	/**
	 * Populates ReconDetailCsvDto with enrollment data
	 * @param enrollmentId
	 * @param batchRunDate
	 * @param hiosIssuerId
	 * @param coverageYear
	 * @return ReconDetailCsvDto 
	 */
	List<ReconDetailCsvDto> populateDataForEnrollment(Integer enrollmentId, Date batchRunDate,
			String hiosIssuerId, int coverageYear);
}
