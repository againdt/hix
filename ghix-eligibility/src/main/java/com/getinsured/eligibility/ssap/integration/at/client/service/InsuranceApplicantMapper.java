package com.getinsured.eligibility.ssap.integration.at.client.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.repository.IEligibilityProgram;
import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.ActivityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.IncarcerationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.APTCCalculationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.APTCEligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.CHIPEligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.CSRAdvancePaymentType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.CSREligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.DisenrollmentActivityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.EligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ExchangeEligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantESIAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsurancePolicyType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.LawfulPresenceStatusType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.MedicaidEligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.MedicaidMAGIEligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.MedicaidNonMAGIEligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ReferralActivityStatusType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ReferralActivityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_pm.InsurancePlanType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.AbsentParentOrSpouseCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.AbsentParentOrSpouseCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.DisenrollmentReasonCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.DisenrollmentReasonCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InsurancePlanVariantCategoryAlphaCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InsuranceSourceCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InsuranceSourceCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityStatusCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityStatusCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.VerificationCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ProperNameTextType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ReferenceType;
import com.getinsured.iex.ssap.enums.OtherStateOrFederalProgramType;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.google.gson.Gson;

@Component
public class InsuranceApplicantMapper {
	
    @Value("#{configProp['hsd.personNoFixedAddress']}")
	private String personNoFixedAddress;

    private static Logger lOGGER = Logger.getLogger(InsuranceApplicantMapper.class);
    
    @Autowired private IEligibilityProgram eligibilityProgramRepository;
    @Autowired private SsapApplicantRepository ssapApplicant;
    @Autowired private Gson platformGson;
    @Autowired private CoverageStartDateService coverageStartDateService;
    @Autowired private GhixRestTemplate ghixRestTemplate;
    
    private static final String APTC_ELIGIBILITY_TYPE = "APTCEligibilityType";
    private static final String CSR_ELIGIBILITY_TYPE = "CSREligibilityType";
    private static final String EXCHANGE_ELIGIBILITY_TYPE = "ExchangeEligibilityType";
    private static final String MEDICAID_ELIGIBILITY_TYPE = "MedicaidEligibilityType";
    private static final String CHIP_ELIGIBILITY_TYPE = "CHIPEligibilityType";
    private static final String MEDICAID_MAGI_ELIGIBILITY_TYPE = "MedicaidMAGIEligibilityType";
    private static final String MEDICAID_NON_MAGI_ELIGIBILITY_TYPE = "MedicaidNonMAGIEligibilityType";
    private static final String ASSESSED_CHIP_ELIGIBILITY_TYPE = "AssessedCHIPEligibilityType";
    private static final String ASSESSED_MEDICAID_MAGI_ELIGIBILITY_TYPE = "AssessedMedicaidMAGIEligibilityType";
    private static final String ASSESSED_MEDICAID_NON_MAGI_ELIGIBILITY_TYPE = "AssessedMedicaidNonMAGIEligibilityType";

    private static final String EE_PACKAGE_NAME = "com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.";
    
    private static final Map<String, String> CSR_MAP = new HashMap<>();
    
    static{
		CSR_MAP.put("CS2","OpenToIndiansBelow300PercentFPL");
		CSR_MAP.put("CS3","OpenToIndiansAbove300PercentFPL");
		CSR_MAP.put("CS4","73PercentActuarialVarianceLevelSilverPlanCSR");
		CSR_MAP.put("CS5","87PercentActuarialVarianceLevelSilverPlanCSR");
		CSR_MAP.put("CS6","94PercentActuarialVarianceLevelSilverPlanCSR");
	}
    
	public InsuranceApplicantType createInsuranceApplicants(JSONObject houseHold, Map<Long, Map<String, VerificationMetadataType>> verificationMetadatas, Long ssapId, BigDecimal aptcAmount) throws GIException {
		
		InsuranceApplicantType insuranceApplicantType = null;
		try {
			if ((java.lang.Boolean) houseHold.get("applyingForCoverageIndicator")) {
				insuranceApplicantType = AccountTransferUtil.insuranceApplicationObjFactory.createInsuranceApplicantType();
                String personId = houseHold.get("personId").toString();
                Map<String, VerificationMetadataType> personVerificationMetadata = verificationMetadatas.get(Long.valueOf(personId));
				// medicaidInsurance:requestHelpPayingMedicalBillsLast3MonthsIndicator
		        if((JSONObject)((JSONObject)houseHold.get("healthCoverage")).get("medicaidInsurance") != null){
                	insuranceApplicantType.setInsuranceApplicantRecentMedicalBillsIndicator(
                		AccountTransferUtil.addBoolean(
                				((JSONObject)((JSONObject)houseHold.get("healthCoverage"))
                						.get("medicaidInsurance")).get("requestHelpPayingMedicalBillsLast3MonthsIndicator")));
                }
                
				insuranceApplicantType.setInsuranceApplicantTemporarilyLivesOutsideApplicationStateIndicator(
						AccountTransferUtil.addBoolean(((JSONObject) houseHold.get("otherAddress")).get("livingOutsideofStateTemporarilyIndicator")));
				
				insuranceApplicantType.setInsuranceApplicantFixedAddressIndicator(AccountTransferUtil.addBoolean(Boolean.valueOf(this.personNoFixedAddress)));
				
				insuranceApplicantType.setInsuranceApplicantParentCaretakerIndicator(
						AccountTransferUtil.addBoolean(((JSONObject)houseHold.get("parentCaretakerRelatives")).get("mainCaretakerOfChildIndicator")));
				
				// TODO. This is on Hold. Abhinav will re visit and let us know.
				/*if(((JSONObject)((JSONObject)((JSONArray)((JSONObject)houseHold.get("healthCoverage")).get("currentEmployer")).get(0))
						  .get("currentEmployerInsurance")).get("willBeEnrolledInEmployerPlanIndicator") != null 
						  && AccountTransferUtil.checkBoolean(((JSONObject)((JSONObject)((JSONArray)((JSONObject)houseHold.get("healthCoverage")).get("currentEmployer")).get(0))
								  .get("currentEmployerInsurance")).get("willBeEnrolledInEmployerPlanIndicator"))){
				  EnrollmentCodeType enrollmentCodeType = AccountTransferUtil.hixTypeFactory.createEnrollmentCodeType();
				  enrollmentCodeType.setValue(EnrollmentCodeSimpleType.PLANNING_TO_ENROLL);
				  insuranceApplicantESIAssociationType.getInsuranceApplicantESIPlannedEnrollmentStatusCode().add(enrollmentCodeType);
				}*/
				
				JSONObject healthCoverage = (JSONObject) houseHold.get("healthCoverage");
				JSONObject currentOtherInsurance = (JSONObject)healthCoverage.get("currentOtherInsurance");
				List<JSONObject> otherStateOrFederalPrograms = (List<JSONObject>) currentOtherInsurance.get("otherStateOrFederalPrograms");
				
				if(otherStateOrFederalPrograms.size() > 0){
					boolean addNonEsiInsurancePolicyFlag = false;
					for (int i = 0; i < otherStateOrFederalPrograms.size(); i++) {
						JSONObject otherStateOrFederalProgram = (JSONObject) otherStateOrFederalPrograms.get(i);
						if(otherStateOrFederalProgram.get("eligible") != null && otherStateOrFederalProgram.get("name") != null){
							String name = (String )otherStateOrFederalProgram.get("name");
							Boolean eligible = (boolean) otherStateOrFederalProgram.get("eligible");
							if(OtherStateOrFederalProgramType.OTHER_COVERAGE.getLabel().equals(name) && eligible == true){
								addNonEsiInsurancePolicyFlag = true;
							}
						}
					}
					
					if(addNonEsiInsurancePolicyFlag == true && healthCoverage.get("otherInsurance") != null) {
						JSONObject currentOtherInsuranceSelected = (JSONObject)healthCoverage.get("otherInsurance");
			            if(currentOtherInsuranceSelected != null) {
			            	InsurancePolicyType  insurancePolicyType  = this.addNonEsiInsurancePolicy(healthCoverage);
			            	if(insurancePolicyType != null){
			            		insuranceApplicantType.getInsuranceApplicantNonESIPolicy().add(insurancePolicyType);
			            	}
			            }
					}
				}
				insuranceApplicantType.getInsuranceApplicantNonESICoverageIndicator().add(this.createInsuranceApplicantNonESICoverageIndicator(healthCoverage, personVerificationMetadata));
				
				JSONArray currentEmployer = (JSONArray) healthCoverage.get("currentEmployer");
				
				if(currentEmployer.size() > 0){
					insuranceApplicantType.getInsuranceApplicantESIAssociation().add(this.createInsuranceApplicantESIAssociation(currentEmployer, createESIEligibleIndicator(healthCoverage,personVerificationMetadata)));
				}
			
				insuranceApplicantType.setInsuranceApplicantCoverageDuringPreviousSixMonthsIndicator(AccountTransferUtil.addBoolean(healthCoverage.get("haveBeenUninsuredInLast6MonthsIndicator")));
				String disenrollmentReasonCode = (String) ((JSONObject) healthCoverage.get("chpInsurance")).get("reasonInsuranceEndedOther");
				if (StringUtils.isNotBlank(disenrollmentReasonCode)) {
					insuranceApplicantType.getInsuranceApplicantNonESIPolicy().add(addDisenrollmentActivity(disenrollmentReasonCode));
				}
				insuranceApplicantType.getInsuranceApplicantIncarceration().add(this.createIncarcerationType(houseHold, personVerificationMetadata));

				//immigration
				JSONObject citizenshipImmigrationStatus = (JSONObject) houseHold.get("citizenshipImmigrationStatus");
                if(null != citizenshipImmigrationStatus && !((Boolean)citizenshipImmigrationStatus.get("citizenshipAsAttestedIndicator"))) {
				    LawfulPresenceStatusType lawfulPresenceStatusType = new LawfulPresenceMapper().createLawfulPresenceStatusType(citizenshipImmigrationStatus);
				
    				// Set ELIGIBLE_IMMIGRATION_STATUS verification metadata if available.
    				VerificationMetadataWrapper.setMetadata(personVerificationMetadata, lawfulPresenceStatusType, VerificationCategoryCodeSimpleType.ELIGIBLE_IMMIGRATION_STATUS);
                    insuranceApplicantType.setInsuranceApplicantLawfulPresenceStatus(lawfulPresenceStatusType);
                }

				JSONObject specialCircumstances = (JSONObject) houseHold.get("specialCircumstances");
				// everInFosterCareIndicator
				if ((java.lang.Boolean) specialCircumstances.get("everInFosterCareIndicator")) {

					insuranceApplicantType.setInsuranceApplicantFosterCareIndicator(AccountTransferUtil.addBoolean(specialCircumstances.get("everInFosterCareIndicator")));
					insuranceApplicantType.setInsuranceApplicantAgeLeftFosterCare(AccountTransferUtil.createNumericType((java.lang.Integer) specialCircumstances.get("ageWhenLeftFosterCare")));
					insuranceApplicantType.setInsuranceApplicantFosterCareState(AccountTransferUtil.createUSStateCodeType((java.lang.String) specialCircumstances.get("fosterCareState")));
				}
				
				insuranceApplicantType.setInsuranceApplicantHadMedicaidDuringFosterCareIndicator(AccountTransferUtil.addBoolean(specialCircumstances.get("gettingHealthCareThroughStateMedicaidIndicator")));

				insuranceApplicantType.setInsuranceApplicantBlindnessOrDisabilityIndicator(AccountTransferUtil.addBoolean(houseHold.get("disabilityIndicator")));
				
				insuranceApplicantType.setInsuranceApplicantLongTermCareIndicator(AccountTransferUtil.addBoolean(houseHold.get("livingArrangementIndicator")));
				
				if(healthCoverage.get("eligibleITU") != null){
	               	insuranceApplicantType.setInsuranceApplicantEligibleITUServicesIndicator(AccountTransferUtil.addBoolean(healthCoverage.get("eligibleITU")));
	            }
				if(healthCoverage.get("receivedITU") != null){
					insuranceApplicantType.setInsuranceApplicantReceivedITUServicesIndicator(AccountTransferUtil.addBoolean(healthCoverage.get("receivedITU")));
				}
				insuranceApplicantType.setInsuranceApplicantFixedAddressIndicator(AccountTransferUtil.addBoolean(true));
				
				if(healthCoverage.get("absentParent")!= null){
					AbsentParentOrSpouseCodeType absentParentOrSpouseCodeType = AccountTransferUtil.hixTypeFactory.createAbsentParentOrSpouseCodeType();
					absentParentOrSpouseCodeType.setValue((AbsentParentOrSpouseCodeSimpleType) healthCoverage.get("absentParent"));
					insuranceApplicantType.setInsuranceApplicantAbsentParentOrSpouseCode(absentParentOrSpouseCodeType);
				}
				if(healthCoverage.get("stateHealthBenefit")!= null){
					insuranceApplicantType.setInsuranceApplicantStateBenefitsThroughPublicEmployeeIndicator(AccountTransferUtil.addBoolean(healthCoverage.get("stateHealthBenefit")));
				}
				
				String applicantGuid = (String) houseHold.get("applicantGuid");
				com.getinsured.iex.ssap.model.SsapApplicant ssapApplicantObj = ssapApplicant.findBySsapApplicantGuidAndSsapApplicationId(applicantGuid, ssapId);
				
				HashMap<String,EligibilityType> eligibilityTypeMap = new HashMap<String,EligibilityType>();
				
				this.setApctCsrExchEligibility(insuranceApplicantType, ssapApplicantObj.getId(), eligibilityTypeMap, aptcAmount);
				
				insuranceApplicantType.setReferralActivity(this.createReferralActivityType(ssapApplicantObj, insuranceApplicantType, eligibilityTypeMap));
			}
		} catch (Exception e) {
			lOGGER.error("Exeption while creating InsuranceApplicant element",e);
			throw new GIException(e);
		}
		
		return insuranceApplicantType;
	}
	
	private InsuranceApplicantType setApctCsrExchEligibility(InsuranceApplicantType insuranceApplicantType, Long ssapApplicantId, HashMap<String,EligibilityType> eligibilityTypeMap, BigDecimal aptcAmount){

		List<EligibilityProgram> eligibilityProgramList = eligibilityProgramRepository.getApplicantEligibilities(ssapApplicantId);
		
		/*ghix-eligibility/eligibility/api/getAptcRatio
		POST
		AptcRatioRequest
		AptcRatioResponse*/
		
		
		boolean exchangeEligibilityFlag = false; 

		if(eligibilityProgramList.size() > 0){
			List<EligibilityType> eligibilityTypeList = new ArrayList<EligibilityType>();
			boolean hasAssessedCHIPEligibilityType = hasEligibilityTypePresent(eligibilityProgramList,ASSESSED_CHIP_ELIGIBILITY_TYPE);
			boolean hasAssessedMedicaidMAGIEligibilityType = hasEligibilityTypePresent(eligibilityProgramList,ASSESSED_MEDICAID_MAGI_ELIGIBILITY_TYPE);
			boolean hasAssessedMedicaidNonMAGIEligibilityType = hasEligibilityTypePresent(eligibilityProgramList,ASSESSED_MEDICAID_NON_MAGI_ELIGIBILITY_TYPE);
			for (EligibilityProgram eligibilityProgram : eligibilityProgramList) {
				if(APTC_ELIGIBILITY_TYPE.equals(eligibilityProgram.getEligibilityType()) && eligibilityProgram.getEligibilityIndicator() != null){
					APTCEligibilityType aptcEligibilityType = this.createAptcEligibilityType(eligibilityProgram, aptcAmount);
					eligibilityTypeList.add(aptcEligibilityType);
					eligibilityTypeMap.put(APTC_ELIGIBILITY_TYPE, aptcEligibilityType);
				}else if(CSR_ELIGIBILITY_TYPE.equals(eligibilityProgram.getEligibilityType()) && eligibilityProgram.getEligibilityIndicator() != null){
					CSREligibilityType csrEligibilityType = this.createCSREligibilityType(eligibilityProgram);
					eligibilityTypeList.add(csrEligibilityType);
					eligibilityTypeMap.put(CSR_ELIGIBILITY_TYPE, csrEligibilityType);
				}else if(EXCHANGE_ELIGIBILITY_TYPE.equals(eligibilityProgram.getEligibilityType())){
					ExchangeEligibilityType exchangeEligibilityType = this.createExchangeEligibilityType(eligibilityProgram, ssapApplicantId);
					eligibilityTypeList.add(exchangeEligibilityType);
					exchangeEligibilityFlag = true;
					eligibilityTypeMap.put(EXCHANGE_ELIGIBILITY_TYPE, exchangeEligibilityType);
				}else if(MEDICAID_ELIGIBILITY_TYPE.equals(eligibilityProgram.getEligibilityType()) && eligibilityProgram.getEligibilityIndicator() != null && !hasAssessedMedicaidMAGIEligibilityType){
					MedicaidMAGIEligibilityType medicaidMAGIEligibilityType = this.createMedicaidMAGIEligibilityType(eligibilityProgram);
					eligibilityTypeList.add(medicaidMAGIEligibilityType);
					eligibilityTypeMap.put(MEDICAID_ELIGIBILITY_TYPE, medicaidMAGIEligibilityType);
				}else if(CHIP_ELIGIBILITY_TYPE.equals(eligibilityProgram.getEligibilityType()) && eligibilityProgram.getEligibilityIndicator() != null && !hasAssessedCHIPEligibilityType){
					CHIPEligibilityType cHIPEligibilityType = this.createCHIPEligibilityType(eligibilityProgram);
					eligibilityTypeList.add(cHIPEligibilityType);
					eligibilityTypeMap.put(CHIP_ELIGIBILITY_TYPE, cHIPEligibilityType);
				}else if(MEDICAID_MAGI_ELIGIBILITY_TYPE.equals(eligibilityProgram.getEligibilityType()) && eligibilityProgram.getEligibilityIndicator() != null && !hasAssessedMedicaidMAGIEligibilityType){
					MedicaidMAGIEligibilityType medicaidMAGIEligibilityType = this.createMedicaidMAGIEligibilityType(eligibilityProgram);
					eligibilityTypeList.add(medicaidMAGIEligibilityType);
					eligibilityTypeMap.put(MEDICAID_MAGI_ELIGIBILITY_TYPE, medicaidMAGIEligibilityType);
				}else if(MEDICAID_NON_MAGI_ELIGIBILITY_TYPE.equals(eligibilityProgram.getEligibilityType()) && eligibilityProgram.getEligibilityIndicator() != null && !hasAssessedMedicaidNonMAGIEligibilityType){
					MedicaidNonMAGIEligibilityType medicaidNonMAGIEligibilityType = this.createMedicaidNonMAGIEligibilityType(eligibilityProgram);
					eligibilityTypeList.add(medicaidNonMAGIEligibilityType);
					eligibilityTypeMap.put(MEDICAID_NON_MAGI_ELIGIBILITY_TYPE, medicaidNonMAGIEligibilityType);
				}else if(ASSESSED_CHIP_ELIGIBILITY_TYPE.equals(eligibilityProgram.getEligibilityType()) && eligibilityProgram.getEligibilityIndicator() != null){
					CHIPEligibilityType AssessedCHIPEligibilityType = this.createCHIPEligibilityType(eligibilityProgram);
					eligibilityTypeList.add(AssessedCHIPEligibilityType);
					eligibilityTypeMap.put(CHIP_ELIGIBILITY_TYPE, AssessedCHIPEligibilityType);
				}else if(ASSESSED_MEDICAID_MAGI_ELIGIBILITY_TYPE.equals(eligibilityProgram.getEligibilityType()) && eligibilityProgram.getEligibilityIndicator() != null){
					MedicaidMAGIEligibilityType medicaidMAGIEligibilityType = this.createMedicaidMAGIEligibilityType(eligibilityProgram);
					eligibilityTypeList.add(medicaidMAGIEligibilityType);
					eligibilityTypeMap.put(MEDICAID_MAGI_ELIGIBILITY_TYPE, medicaidMAGIEligibilityType);
				}else if(ASSESSED_MEDICAID_NON_MAGI_ELIGIBILITY_TYPE.equals(eligibilityProgram.getEligibilityType()) && eligibilityProgram.getEligibilityIndicator() != null){
					MedicaidNonMAGIEligibilityType medicaidNonMAGIEligibilityType = this.createMedicaidNonMAGIEligibilityType(eligibilityProgram);
					eligibilityTypeList.add(medicaidNonMAGIEligibilityType);
					eligibilityTypeMap.put(MEDICAID_NON_MAGI_ELIGIBILITY_TYPE, medicaidNonMAGIEligibilityType);
				}
			}
			insuranceApplicantType.setEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility(eligibilityTypeList);
		}
		
		List<EligibilityType> eligibilityTypeFinalList = insuranceApplicantType.getEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility();
		if(exchangeEligibilityFlag == true){
			return insuranceApplicantType;
		}else{
			if(exchangeEligibilityFlag == false){
				ExchangeEligibilityType exchangeEligibilityType = this.createExchangeEligibilityType(new EligibilityProgram(), ssapApplicantId);
				eligibilityTypeFinalList.add(exchangeEligibilityType);
			}
			insuranceApplicantType.setEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility(eligibilityTypeFinalList);
		}
		return insuranceApplicantType;
	}
	
	private boolean hasEligibilityTypePresent(List<EligibilityProgram> eligibilityProgramList,
			String assessedChipEligibilityType) {
		for (EligibilityProgram eligibilityProgram : eligibilityProgramList) {
			if(assessedChipEligibilityType.equals(eligibilityProgram.getEligibilityType())){
				return true;
			}
		}
		return false;
	}

	private APTCEligibilityType createAptcEligibilityType(EligibilityProgram eligibilityProgram, BigDecimal aptcAmount){
		APTCEligibilityType aptcEligibilityType = new APTCEligibilityType();
		
		DateRangeType aptcEligibilityDateRange = AccountTransferUtil.niemCoreFactory.createDateRangeType();

		Date startDateDb = eligibilityProgram.getEligibilityStartDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date startDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType aptcEligibilityStartdateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(startDateDb!=null){
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(startDateDb));
		}else{
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		aptcEligibilityStartdateType.setDate(startDateDbAtBasic);
		aptcEligibilityDateRange.setStartDate(aptcEligibilityStartdateType);
		
		Date endDateDb = eligibilityProgram.getEligibilityEndDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date endDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType aptcEligibilityEnddateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(endDateDb!=null){
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(endDateDb));
		}else{
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		aptcEligibilityEnddateType.setDate(endDateDbAtBasic);
		aptcEligibilityDateRange.setEndDate(aptcEligibilityEnddateType);
		
		aptcEligibilityType.setEligibilityDateRange(aptcEligibilityDateRange);
		
		Date eligibilityActivationDateDb = eligibilityProgram.getEligibilityDeterminationDate();
		if(eligibilityActivationDateDb != null){
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.DateTime eligibilityActivationDateDbAtBasic = AccountTransferUtil.basicFactory.createDateTime();
			DateType eligibilityActivationdateType = AccountTransferUtil.niemCoreFactory.createDateType();
			
			eligibilityActivationDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(eligibilityActivationDateDb));
			eligibilityActivationdateType.setDateTime(eligibilityActivationDateDbAtBasic);
			
			ActivityType eligibilityDetermination = AccountTransferUtil.hixCoreFactory.createActivityType();
			eligibilityDetermination.setActivityDate(eligibilityActivationdateType);
			aptcEligibilityType.setEligibilityDetermination(eligibilityDetermination);
		}else{
			aptcEligibilityType.setEligibilityDetermination(null);
		}
		
		//TODO: We need confirmation for reasontext
		TextType textType = AccountTransferUtil.niemCoreFactory.createTextType();
		textType.setValue("999");
		aptcEligibilityType.setEligibilityReasonText(textType);
		
		aptcEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(eligibilityProgram.getEligibilityIndicator()));
		if(eligibilityProgram.getEligibilityIndicator() != null && Boolean.valueOf(eligibilityProgram.getEligibilityIndicator()) == Boolean.TRUE && aptcAmount != null){
			AmountType amountType = AccountTransferUtil.niemCoreFactory.createAmountType();
			APTCCalculationType aptcCalculationType  = AccountTransferUtil.insuranceApplicationObjFactory.createAPTCCalculationType();
			if(eligibilityProgram.getId() != null){
				amountType.setValue(aptcAmount);
				aptcCalculationType.setAPTCMaximumAmount(amountType);
				aptcEligibilityType.setAPTC(aptcCalculationType);
				aptcEligibilityType.getAPTC().setAPTCMaximumAmount(aptcCalculationType.getAPTCMaximumAmount());
			}else{
				amountType.setValue(null);
				aptcCalculationType.setAPTCMaximumAmount(amountType);
				aptcEligibilityType.setAPTC(aptcCalculationType);
				aptcEligibilityType.getAPTC().setAPTCMaximumAmount(null);
			}
		}
		
		
		if(eligibilityProgram.getEligibilityIndicator() != null){
			aptcEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(Boolean.valueOf(eligibilityProgram.getEligibilityIndicator())));
		}else{
			aptcEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(Boolean.FALSE));
		}
		StringBuffer id =  new StringBuffer(eligibilityProgram.getEligibilityType());
		id.append("_").append(String.valueOf(eligibilityProgram.getSsapApplicant().getId()));
		aptcEligibilityType.setId(id.toString());
		
		return aptcEligibilityType;
	}
	
	private CSREligibilityType createCSREligibilityType(EligibilityProgram eligibilityProgram){
		CSREligibilityType csrEligibilityType = new CSREligibilityType();
		
		DateRangeType csrEligibilityDateRange = AccountTransferUtil.niemCoreFactory.createDateRangeType();

		Date startDateDb = eligibilityProgram.getEligibilityStartDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date startDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType csrEligibilityStartdateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(startDateDb!=null){
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(startDateDb));
		}else{
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		csrEligibilityStartdateType.setDate(startDateDbAtBasic);
		csrEligibilityDateRange.setStartDate(csrEligibilityStartdateType);

		Date endDateDb = eligibilityProgram.getEligibilityEndDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date endDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType csrEligibilityEnddateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(endDateDb!=null){
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(endDateDb));
		}else{
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		csrEligibilityEnddateType.setDate(endDateDbAtBasic);
		csrEligibilityDateRange.setEndDate(csrEligibilityEnddateType);

		csrEligibilityType.setEligibilityDateRange(csrEligibilityDateRange);
		
		Date eligibilityActivationDateDb = eligibilityProgram.getEligibilityDeterminationDate();
		if(eligibilityActivationDateDb != null){
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.DateTime eligibilityActivationDateDbAtBasic = AccountTransferUtil.basicFactory.createDateTime();
			DateType eligibilityActivationdateType = AccountTransferUtil.niemCoreFactory.createDateType();
			
			eligibilityActivationDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(eligibilityActivationDateDb));
			eligibilityActivationdateType.setDateTime(eligibilityActivationDateDbAtBasic);
			
			ActivityType eligibilityDetermination = AccountTransferUtil.hixCoreFactory.createActivityType();
			eligibilityDetermination.setActivityDate(eligibilityActivationdateType);
			csrEligibilityType.setEligibilityDetermination(eligibilityDetermination);
		}else{
			csrEligibilityType.setEligibilityDetermination(null);
		}
		
		TextType textType = AccountTransferUtil.niemCoreFactory.createTextType();
		textType.setValue("999");
		csrEligibilityType.setEligibilityReasonText(textType);
		
		if(eligibilityProgram.getEligibilityIndicator() != null){
			csrEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(Boolean.valueOf(eligibilityProgram.getEligibilityIndicator())));
		}else{
			csrEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(Boolean.FALSE));
		}
		
		if(eligibilityProgram.getId() != null && CSR_MAP.get(eligibilityProgram.getSsapApplicant().getCsrLevel())!= null){
			InsurancePlanVariantCategoryAlphaCodeType insurancePlanVariantCategoryAlphaCodeType = AccountTransferUtil.hixTypeFactory.createInsurancePlanVariantCategoryAlphaCodeType();
			insurancePlanVariantCategoryAlphaCodeType.setValue(CSR_MAP.get(eligibilityProgram.getSsapApplicant().getCsrLevel()));
			 
			CSRAdvancePaymentType csrAdvancePaymentType = AccountTransferUtil.insuranceApplicationObjFactory.createCSRAdvancePaymentType();
			csrAdvancePaymentType.setCSRCategoryAlphaCode(insurancePlanVariantCategoryAlphaCodeType);
			csrEligibilityType.setCSRAdvancePayment(csrAdvancePaymentType);
			csrEligibilityType.getCSRAdvancePayment().setCSRCategoryAlphaCode(insurancePlanVariantCategoryAlphaCodeType);
		}/*else{
			InsurancePlanVariantCategoryAlphaCodeType insurancePlanVariantCategoryAlphaCodeType = AccountTransferUtil.hixTypeFactory.createInsurancePlanVariantCategoryAlphaCodeType();
			insurancePlanVariantCategoryAlphaCodeType.setValue(null);
			 
			CSRAdvancePaymentType csrAdvancePaymentType = AccountTransferUtil.insuranceApplicationObjFactory.createCSRAdvancePaymentType();
			csrAdvancePaymentType.setCSRCategoryAlphaCode(insurancePlanVariantCategoryAlphaCodeType);
			csrEligibilityType.setCSRAdvancePayment(csrAdvancePaymentType);
			csrEligibilityType.getCSRAdvancePayment().setCSRCategoryAlphaCode(null);
		}*/
		
		StringBuffer id =  new StringBuffer(eligibilityProgram.getEligibilityType());
		id.append("_").append(String.valueOf(eligibilityProgram.getSsapApplicant().getId()));
		csrEligibilityType.setId(id.toString());
		return csrEligibilityType;
	}
	
	private ExchangeEligibilityType createExchangeEligibilityType(EligibilityProgram eligibilityProgram, Long ssapApplicantId){
		ExchangeEligibilityType exchangeEligibilityType = new ExchangeEligibilityType();
		
		DateRangeType exchangeEligibilityDateRange = AccountTransferUtil.niemCoreFactory.createDateRangeType();
		
		Date startDateDb = eligibilityProgram.getEligibilityStartDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date startDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType exchangeEligibilityStartdateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(startDateDb!=null){
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(startDateDb));
		}else{
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		exchangeEligibilityStartdateType.setDate(startDateDbAtBasic);
		exchangeEligibilityDateRange.setStartDate(exchangeEligibilityStartdateType);

		Date endDateDb = eligibilityProgram.getEligibilityEndDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date endDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType exchangeEligibilityEnddateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(endDateDb!=null){
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(endDateDb));
		}else{
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		exchangeEligibilityEnddateType.setDate(endDateDbAtBasic);
		exchangeEligibilityDateRange.setEndDate(exchangeEligibilityEnddateType);
		exchangeEligibilityType.setEligibilityDateRange(exchangeEligibilityDateRange);
		
		Date eligibilityActivationDateDb = eligibilityProgram.getEligibilityDeterminationDate();
		if(eligibilityActivationDateDb != null){
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.DateTime eligibilityActivationDateDbAtBasic = AccountTransferUtil.basicFactory.createDateTime();
			DateType eligibilityActivationdateType = AccountTransferUtil.niemCoreFactory.createDateType();
			
			eligibilityActivationDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(eligibilityActivationDateDb));
			eligibilityActivationdateType.setDateTime(eligibilityActivationDateDbAtBasic);
			ActivityType eligibilityDetermination = AccountTransferUtil.hixCoreFactory.createActivityType();
			eligibilityDetermination.setActivityDate(eligibilityActivationdateType);
			exchangeEligibilityType.setEligibilityDetermination(eligibilityDetermination);
		}else{
			exchangeEligibilityType.setEligibilityDetermination(null);
		}
		if(eligibilityProgram.getId() != null){
			TextType textType = AccountTransferUtil.niemCoreFactory.createTextType();
			textType.setValue("999");
			exchangeEligibilityType.setEligibilityReasonText(textType);
			exchangeEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(Boolean.valueOf(eligibilityProgram.getEligibilityIndicator())));
		}else{
			exchangeEligibilityType.setEligibilityReasonText(null);
			exchangeEligibilityType.setEligibilityIndicator(null);
		}
		if(org.apache.commons.lang3.StringUtils.isBlank(eligibilityProgram.getEligibilityType())){
			StringBuffer id =  new StringBuffer(EXCHANGE_ELIGIBILITY_TYPE);
			id.append("_").append(String.valueOf(ssapApplicantId));
			exchangeEligibilityType.setId(id.toString());
		}else{
			StringBuffer id =  new StringBuffer(eligibilityProgram.getEligibilityType());
			id.append("_").append(String.valueOf(ssapApplicantId));
			exchangeEligibilityType.setId(id.toString());
		}
		return exchangeEligibilityType;
	}
	
	private MedicaidMAGIEligibilityType createMedicaidMAGIEligibilityType(EligibilityProgram eligibilityProgram){
		MedicaidMAGIEligibilityType medicaidMAGIEligibilityType = new MedicaidMAGIEligibilityType();
		
		DateRangeType exchangeEligibilityDateRange = AccountTransferUtil.niemCoreFactory.createDateRangeType();
		
		Date startDateDb = eligibilityProgram.getEligibilityStartDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date startDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType exchangeEligibilityStartdateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(startDateDb!=null){
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(startDateDb));
		}else{
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		exchangeEligibilityStartdateType.setDate(startDateDbAtBasic);
		exchangeEligibilityDateRange.setStartDate(exchangeEligibilityStartdateType);

		Date endDateDb = eligibilityProgram.getEligibilityEndDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date endDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType exchangeEligibilityEnddateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(endDateDb!=null){
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(endDateDb));
		}else{
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		exchangeEligibilityEnddateType.setDate(endDateDbAtBasic);
		exchangeEligibilityDateRange.setEndDate(exchangeEligibilityEnddateType);
		medicaidMAGIEligibilityType.setEligibilityDateRange(exchangeEligibilityDateRange);
		
		Date eligibilityActivationDateDb = eligibilityProgram.getEligibilityDeterminationDate();
		if(eligibilityActivationDateDb != null){
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.DateTime eligibilityActivationDateDbAtBasic = AccountTransferUtil.basicFactory.createDateTime();
			DateType eligibilityActivationdateType = AccountTransferUtil.niemCoreFactory.createDateType();
			
			eligibilityActivationDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(eligibilityActivationDateDb));
			eligibilityActivationdateType.setDateTime(eligibilityActivationDateDbAtBasic);
			ActivityType eligibilityDetermination = AccountTransferUtil.hixCoreFactory.createActivityType();
			eligibilityDetermination.setActivityDate(eligibilityActivationdateType);
			medicaidMAGIEligibilityType.setEligibilityDetermination(eligibilityDetermination);
		}else{
			medicaidMAGIEligibilityType.setEligibilityDetermination(null);
		}
		if(eligibilityProgram.getId() != null){
			medicaidMAGIEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(Boolean.valueOf(eligibilityProgram.getEligibilityIndicator())));
		}else{
			medicaidMAGIEligibilityType.setEligibilityIndicator(null);
		}
		StringBuffer id =  new StringBuffer(MEDICAID_MAGI_ELIGIBILITY_TYPE);
		id.append("_").append(String.valueOf(eligibilityProgram.getSsapApplicant().getId()));
		medicaidMAGIEligibilityType.setId(id.toString());
		
		return medicaidMAGIEligibilityType;
	}
	private CHIPEligibilityType createCHIPEligibilityType(EligibilityProgram eligibilityProgram){
		CHIPEligibilityType chipEligibilityType = new CHIPEligibilityType();
				
		DateRangeType exchangeEligibilityDateRange = AccountTransferUtil.niemCoreFactory.createDateRangeType();
		
		Date startDateDb = eligibilityProgram.getEligibilityStartDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date startDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType exchangeEligibilityStartdateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(startDateDb!=null){
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(startDateDb));
		}else{
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		exchangeEligibilityStartdateType.setDate(startDateDbAtBasic);
		exchangeEligibilityDateRange.setStartDate(exchangeEligibilityStartdateType);

		Date endDateDb = eligibilityProgram.getEligibilityEndDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date endDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType exchangeEligibilityEnddateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(endDateDb!=null){
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(endDateDb));
		}else{
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		exchangeEligibilityEnddateType.setDate(endDateDbAtBasic);
		exchangeEligibilityDateRange.setEndDate(exchangeEligibilityEnddateType);
		chipEligibilityType.setEligibilityDateRange(exchangeEligibilityDateRange);
		
		Date eligibilityActivationDateDb = eligibilityProgram.getEligibilityDeterminationDate();
		if(eligibilityActivationDateDb != null){
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.DateTime eligibilityActivationDateDbAtBasic = AccountTransferUtil.basicFactory.createDateTime();
			DateType eligibilityActivationdateType = AccountTransferUtil.niemCoreFactory.createDateType();
			
			eligibilityActivationDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(eligibilityActivationDateDb));
			eligibilityActivationdateType.setDateTime(eligibilityActivationDateDbAtBasic);
			ActivityType eligibilityDetermination = AccountTransferUtil.hixCoreFactory.createActivityType();
			eligibilityDetermination.setActivityDate(eligibilityActivationdateType);
			chipEligibilityType.setEligibilityDetermination(eligibilityDetermination);
		}else{
			chipEligibilityType.setEligibilityDetermination(null);
		}
		if(eligibilityProgram.getId() != null){
			chipEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(Boolean.valueOf(eligibilityProgram.getEligibilityIndicator())));
		}else{
			chipEligibilityType.setEligibilityIndicator(null);
		}
		StringBuffer id =  new StringBuffer(CHIP_ELIGIBILITY_TYPE);
		id.append("_").append(String.valueOf(eligibilityProgram.getSsapApplicant().getId()));
		chipEligibilityType.setId(id.toString());
		
		return chipEligibilityType;
	}
	
	private MedicaidEligibilityType createMedicaidEligibilityType(EligibilityProgram eligibilityProgram){
		MedicaidEligibilityType medicaidEligibilityType = new MedicaidEligibilityType();
				
		DateRangeType exchangeEligibilityDateRange = AccountTransferUtil.niemCoreFactory.createDateRangeType();
		
		Date startDateDb = eligibilityProgram.getEligibilityStartDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date startDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType exchangeEligibilityStartdateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(startDateDb!=null){
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(startDateDb));
		}else{
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		exchangeEligibilityStartdateType.setDate(startDateDbAtBasic);
		exchangeEligibilityDateRange.setStartDate(exchangeEligibilityStartdateType);

		Date endDateDb = eligibilityProgram.getEligibilityEndDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date endDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType exchangeEligibilityEnddateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(endDateDb!=null){
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(endDateDb));
		}else{
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		exchangeEligibilityEnddateType.setDate(endDateDbAtBasic);
		exchangeEligibilityDateRange.setEndDate(exchangeEligibilityEnddateType);
		medicaidEligibilityType.setEligibilityDateRange(exchangeEligibilityDateRange);
		
		Date eligibilityActivationDateDb = eligibilityProgram.getEligibilityDeterminationDate();
		if(eligibilityActivationDateDb != null){
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.DateTime eligibilityActivationDateDbAtBasic = AccountTransferUtil.basicFactory.createDateTime();
			DateType eligibilityActivationdateType = AccountTransferUtil.niemCoreFactory.createDateType();
			
			eligibilityActivationDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(eligibilityActivationDateDb));
			eligibilityActivationdateType.setDateTime(eligibilityActivationDateDbAtBasic);
			ActivityType eligibilityDetermination = AccountTransferUtil.hixCoreFactory.createActivityType();
			eligibilityDetermination.setActivityDate(eligibilityActivationdateType);
			medicaidEligibilityType.setEligibilityDetermination(eligibilityDetermination);
		}else{
			medicaidEligibilityType.setEligibilityDetermination(null);
		}
		if(eligibilityProgram.getId() != null){
			medicaidEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(Boolean.valueOf(eligibilityProgram.getEligibilityIndicator())));
		}else{
			medicaidEligibilityType.setEligibilityIndicator(null);
		}
		StringBuffer id =  new StringBuffer(eligibilityProgram.getEligibilityType());
		id.append("_").append(String.valueOf(eligibilityProgram.getSsapApplicant().getId()));
		medicaidEligibilityType.setId(id.toString());
		
		return medicaidEligibilityType;
	}

	private MedicaidNonMAGIEligibilityType createMedicaidNonMAGIEligibilityType(EligibilityProgram eligibilityProgram){
		MedicaidNonMAGIEligibilityType medicaidMAGIEligibilityType = new MedicaidNonMAGIEligibilityType();
		
		DateRangeType exchangeEligibilityDateRange = AccountTransferUtil.niemCoreFactory.createDateRangeType();
		
		Date startDateDb = eligibilityProgram.getEligibilityStartDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date startDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType exchangeEligibilityStartdateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(startDateDb!=null){
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(startDateDb));
		}else{
			startDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		exchangeEligibilityStartdateType.setDate(startDateDbAtBasic);
		exchangeEligibilityDateRange.setStartDate(exchangeEligibilityStartdateType);

		Date endDateDb = eligibilityProgram.getEligibilityEndDate();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date endDateDbAtBasic = AccountTransferUtil.basicFactory.createDate();
		DateType exchangeEligibilityEnddateType = AccountTransferUtil.niemCoreFactory.createDateType();
		if(endDateDb!=null){
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(endDateDb));
		}else{
			endDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(new Date()));
		}
		exchangeEligibilityEnddateType.setDate(endDateDbAtBasic);
		exchangeEligibilityDateRange.setEndDate(exchangeEligibilityEnddateType);
		medicaidMAGIEligibilityType.setEligibilityDateRange(exchangeEligibilityDateRange);
		
		Date eligibilityActivationDateDb = eligibilityProgram.getEligibilityDeterminationDate();
		if(eligibilityActivationDateDb != null){
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.DateTime eligibilityActivationDateDbAtBasic = AccountTransferUtil.basicFactory.createDateTime();
			DateType eligibilityActivationdateType = AccountTransferUtil.niemCoreFactory.createDateType();
			
			eligibilityActivationDateDbAtBasic.setValue(AccountTransferUtil.dateToXMLGregorianCalendar(eligibilityActivationDateDb));
			eligibilityActivationdateType.setDateTime(eligibilityActivationDateDbAtBasic);
			ActivityType eligibilityDetermination = AccountTransferUtil.hixCoreFactory.createActivityType();
			eligibilityDetermination.setActivityDate(eligibilityActivationdateType);
			medicaidMAGIEligibilityType.setEligibilityDetermination(eligibilityDetermination);
		}else{
			medicaidMAGIEligibilityType.setEligibilityDetermination(null);
		}
		if(eligibilityProgram.getId() != null){
			medicaidMAGIEligibilityType.setEligibilityIndicator(AccountTransferUtil.addBoolean(Boolean.valueOf(eligibilityProgram.getEligibilityIndicator())));
		}else{
			medicaidMAGIEligibilityType.setEligibilityIndicator(null);
		}
		StringBuffer id =  new StringBuffer(MEDICAID_NON_MAGI_ELIGIBILITY_TYPE);
		id.append("_").append(String.valueOf(eligibilityProgram.getSsapApplicant().getId()));
		medicaidMAGIEligibilityType.setId(id.toString());
		
		return medicaidMAGIEligibilityType;
	}
	
	private InsurancePolicyType addDisenrollmentActivity(String disenrollmentReasonCode){
		InsurancePolicyType insurancePolicyType = AccountTransferUtil.insuranceApplicationObjFactory.createInsurancePolicyType();
		DisenrollmentActivityType disenrollmentActivityType = AccountTransferUtil.insuranceApplicationObjFactory.createDisenrollmentActivityType();
		DisenrollmentReasonCodeType disenrollmentReasonCodeType = AccountTransferUtil.hixTypeFactory.createDisenrollmentReasonCodeType();
		disenrollmentReasonCodeType.setValue(DisenrollmentReasonCodeSimpleType.fromValue(disenrollmentReasonCode));
		disenrollmentActivityType.setDisenrollmentActivityReasonCode(disenrollmentReasonCodeType);
		insurancePolicyType.setDisenrollmentActivity(disenrollmentActivityType);
		return insurancePolicyType;
	}
	
	private com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean createESIEligibleIndicator(JSONObject healthCoverage,
			Map<String, VerificationMetadataType> personVerificationMetadata) {
		
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean esiEligibleIndicator = AccountTransferUtil.basicFactory.createBoolean();
		if (healthCoverage.get("isOfferedHealthCoverageThroughJobIndicator") != null) {
			esiEligibleIndicator.setValue((java.lang.Boolean) healthCoverage.get("isOfferedHealthCoverageThroughJobIndicator"));
           if(personVerificationMetadata != null) {
                VerificationMetadataType esiMecMetadata = personVerificationMetadata.get(VerificationCategoryCodeSimpleType.ESI_MEC.value());
                if(esiMecMetadata != null) {
                    esiEligibleIndicator.getMetadata().add(esiMecMetadata);
                }
            }
		}
		
		return esiEligibleIndicator;
		
	}

	private IncarcerationType createIncarcerationType(JSONObject houseHold, Map<String, VerificationMetadataType> personVerificationMetadata ){
		
		IncarcerationType incarcerationType = AccountTransferUtil.hixCoreFactory.createIncarcerationType();
	
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean incarcerationIndicator = AccountTransferUtil.basicFactory.createBoolean();
		Object isIncarerationStatusProvidedByApplicant = ((JSONObject)houseHold.get("incarcerationStatus")).get("incarcerationStatusIndicator");
		
		if(isIncarerationStatusProvidedByApplicant != null){
			incarcerationIndicator.setValue(Boolean.valueOf(isIncarerationStatusProvidedByApplicant.toString()));	
	        if(personVerificationMetadata != null) {
	            VerificationMetadataType incarcerationMetadata = personVerificationMetadata.get(VerificationCategoryCodeSimpleType.INCARCERATION_STATUS.value());
	            if(incarcerationMetadata != null) {
	                incarcerationIndicator.getMetadata().add(incarcerationMetadata);
	            }
	        }
	    }
		
		if(incarcerationIndicator != null) {
			incarcerationType.setIncarcerationIndicator(incarcerationIndicator);
		}
		return incarcerationType;
	}
	
	private ReferralActivityType createReferralActivityType(SsapApplicant ssapApplicantObj, InsuranceApplicantType insuranceApplicantType, HashMap<String, EligibilityType> eligibilityTypeMap){
		ReferralActivityType referralActivityType = AccountTransferUtil.insuranceApplicationObjFactory.createReferralActivityType();
		
		DateType refActivityDateType= AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.DateTime refActivityDate = AccountTransferUtil.basicFactory.createDateTime();
		//TODO source of this date.It is not system Date
		refActivityDate.setValue(AccountTransferUtil.getSystemDateTime());
		//refActivityDate.setValue(value);
		refActivityDateType.setDateTime(refActivityDate);
		referralActivityType.setActivityDate(refActivityDateType);	
		
		IdentificationType referralActivityIdentificationType =AccountTransferUtil.niemCoreFactory.createIdentificationType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String referralActivityId = AccountTransferUtil.basicFactory.createString();
		referralActivityId.setValue(ssapApplicantObj.getApplicantGuid());
		referralActivityIdentificationType.setIdentificationID(referralActivityId);
		referralActivityType.setActivityIdentification(referralActivityIdentificationType);
		
		ReferralActivityStatusCodeType referralActivityStatusCodeType = AccountTransferUtil.hixTypeFactory.createReferralActivityStatusCodeType();
		ReferralActivityStatusType referralActivityStatusType = AccountTransferUtil.insuranceApplicationObjFactory.createReferralActivityStatusType();
		referralActivityStatusCodeType.setValue(ReferralActivityStatusCodeSimpleType.ACCEPTED);
		referralActivityStatusType.setReferralActivityStatusCode(referralActivityStatusCodeType);
		referralActivityType.setReferralActivityStatus(referralActivityStatusType);
		
		List<EligibilityProgram>  eligibilityPrograms= eligibilityProgramRepository.getAssessedChipAndMedicaidEligibilityByApplicant(ssapApplicantObj.getSsapApplication().getId(), ssapApplicantObj.getId());
		if(eligibilityPrograms.size() > 0){
			ReferralActivityStatusCodeType referralActivityIniStatusCodeType = AccountTransferUtil.hixTypeFactory.createReferralActivityStatusCodeType();
			ReferralActivityStatusType referralActivityIniStatusType = AccountTransferUtil.insuranceApplicationObjFactory.createReferralActivityStatusType();
			referralActivityIniStatusCodeType.setValue(ReferralActivityStatusCodeSimpleType.INITIATED);
			referralActivityIniStatusType.setReferralActivityStatusCode(referralActivityIniStatusCodeType);
			referralActivityType.setReferralActivityStatus(referralActivityIniStatusType);
		}
		
		List<ReferenceType> referenceTypeList = new ArrayList<ReferenceType>();
		for(Map.Entry<String, EligibilityType> entry : eligibilityTypeMap.entrySet())
		{
				ReferenceType referenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
				referenceType.setRef(entry.getValue());
				referenceTypeList.add(referenceType);
		}
		referralActivityType.setReferralActivityEligibilityReasonReference(referenceTypeList);
		return referralActivityType;
	}

	private com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean createInsuranceApplicantNonESICoverageIndicator(JSONObject healthCoverage, Map<String, VerificationMetadataType> personVerificationMetadata ){
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean nonESICoverageFlag = AccountTransferUtil.basicFactory.createBoolean();
		
		// Extract and set the non-ESI coverage details based on otherInsuranceIndicator and currentOtherInsurance tags from Ssap json
		if(null != healthCoverage.get("otherInsuranceIndicator") && (java.lang.Boolean)healthCoverage.get("otherInsuranceIndicator")) {
		    // Set the value of the non-ESI coverage indicator as true because some other insurance type is selected by the user 
		    nonESICoverageFlag.setValue(true);
		} else {
		    nonESICoverageFlag.setValue(false);
		}
	    if(personVerificationMetadata != null) {
            VerificationMetadataType nonEsiMecMetadata = personVerificationMetadata.get(VerificationCategoryCodeSimpleType.NON_ESI_MEC.value());
            if(nonEsiMecMetadata != null) {
                nonESICoverageFlag.getMetadata().add(nonEsiMecMetadata);
            }
        }
		
        return nonESICoverageFlag;
	}
	
	private InsurancePolicyType addNonEsiInsurancePolicy(JSONObject healthCoverage){
		// if true, take "otherInsurance" set otherStateOrFederalProgramName = otherInsurance->insuranceName, 
		//otherStateOrFederalProgramType=otherInsurance->limitedBenefit == true ? UnspecifiedLimitedCoverage : UnspecifiedFullCoverage
		JSONObject OtherInsurance = (JSONObject)healthCoverage.get("otherInsurance");
		InsurancePolicyType nonEsiInsurancePolicyType = AccountTransferUtil.insuranceApplicationObjFactory.createInsurancePolicyType();
		if(OtherInsurance != null){
			 InsurancePlanType otherInsurancePlan = AccountTransferUtil.hixPMFactory.createInsurancePlanType();
	         ProperNameTextType planName = AccountTransferUtil.niemCoreFactory.createProperNameTextType();
	         planName.setValue(OtherInsurance.get("insuranceName").toString());
	         otherInsurancePlan.setInsurancePlanName(planName);
        	 nonEsiInsurancePolicyType.setInsurancePlan(otherInsurancePlan);

        	 if(OtherInsurance.get("policyNumber") != null){
	     		 IdentificationType nonESIPolicyNumberIdentificationType =AccountTransferUtil.niemCoreFactory.createIdentificationType();
	     		 com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String nonESIPolicyId = AccountTransferUtil.basicFactory.createString();
	     		 nonESIPolicyId.setValue(OtherInsurance.get("policyNumber").toString());
	     		 nonESIPolicyNumberIdentificationType.setIdentificationID(nonESIPolicyId);
	     		 nonEsiInsurancePolicyType.setInsurancePolicyIdentification(nonESIPolicyNumberIdentificationType);
        	 }
         	
        	 InsuranceSourceCodeType otherInsuranceCodeType = AccountTransferUtil.hixTypeFactory.createInsuranceSourceCodeType();
        	 Boolean OtherInsuranceLimitedBenefit = (Boolean) OtherInsurance.get("limitedBenefit");
		     if(OtherInsuranceLimitedBenefit == true){
	        	 otherInsuranceCodeType.setValue(InsuranceSourceCodeSimpleType.fromValue(InsuranceSourceCodeSimpleType.UNSPECIFIED_LIMITED_COVERAGE.value())); 
	         }else{
	        	 otherInsuranceCodeType.setValue(InsuranceSourceCodeSimpleType.fromValue(InsuranceSourceCodeSimpleType.UNSPECIFIED_FULL_COVERAGE.value())); 
	         }
	         nonEsiInsurancePolicyType.setInsurancePolicySourceCode(otherInsuranceCodeType);
		     return nonEsiInsurancePolicyType;
		}
		return null;
	}
	private InsuranceApplicantESIAssociationType createInsuranceApplicantESIAssociation(JSONArray currentEmployer, com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean esiEligibleIndicator) throws ParseException, DatatypeConfigurationException {
		InsuranceApplicantESIAssociationType insuranceApplicantESIAssociationType = AccountTransferUtil.insuranceApplicationObjFactory.createInsuranceApplicantESIAssociationType();
		insuranceApplicantESIAssociationType.setInsuranceApplicantESIEligibleIndicator(esiEligibleIndicator);
	
		DateRangeType esiPlannedCoveragedateRangeType =AccountTransferUtil.niemCoreFactory.createDateRangeType();
		DateType esiPlannedCoveragedateType =AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date esiPlannedCoverageDate = AccountTransferUtil.basicFactory.createDate();
		String wbeiepd = (java.lang.String) ((JSONObject) ((JSONObject) currentEmployer.get(0)).get("currentEmployerInsurance")).get("willBeEnrolledInEmployerPlanDate");
		if (!StringUtils.isBlank(wbeiepd)) {
			esiPlannedCoverageDate.setValue(AccountTransferUtil.stringToXMLGregorianCalendar(wbeiepd));					
			esiPlannedCoveragedateType.setDate(esiPlannedCoverageDate);
			esiPlannedCoveragedateRangeType.setStartDate(esiPlannedCoveragedateType);
			insuranceApplicantESIAssociationType.getInsuranceApplicantESIPlannedCoverageDateRange().add(esiPlannedCoveragedateRangeType);
		}
		return insuranceApplicantESIAssociationType;
	}
}
