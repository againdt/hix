package com.getinsured.hix.ssap;


import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.hix.account.service.LocationService;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.dto.entity.AssisterDesignateResponseDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.client.ResourceCreatorImpl;
import com.getinsured.iex.client.SsapApplicationAccessor;
import com.getinsured.iex.client.SsapApplicationRequest;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.dto.qep.QepEventRequest;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;

public class Util {
	private static final int DIVIDE_YEARLY = 12;
	private static final int MULTIPLY_BI_WEEKLY = 2;
	private static final int MULTIPLY_WEEKLY = 4;
	private static final int MULTIPLY_DAILY = 30;
	private static final int MULTIPLY_HOURLY = 720;
	static final String SIGN_AND_SAVE_PAGE_ID="appscr85";
	static final String FAMILY_HOUSEHOLD_SECTION_LAST_PAGE="appscr67";
	static Pattern patternForNumberInLink = Pattern.compile("\\d+");
	@Autowired private SsapApplicationAccessor ssapApplicationAccessorImpl;
	@Autowired private UserService userService;
	@Autowired private BrokerService brokerService;
	@Autowired private DesignateService designateService;
	@Autowired private LocationService locationService;
	@Autowired private EERestCallInvoker restClassCommunicator;
	@Autowired private RestTemplate restTemplate;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private ResourceCreatorImpl resourceCreatorImpl;
	
	private static Logger utilityLogger = Logger.getLogger(Util.class);
	
	public static Double calculateIncome(Double income,String frequency){
		
		Double monthlyIncome = new Double(0);
		
		if(frequency.equalsIgnoreCase("HOURLY")){
			monthlyIncome = income * MULTIPLY_HOURLY;
		}
		else if(frequency.equalsIgnoreCase("DAILY")){
			monthlyIncome = income * MULTIPLY_DAILY;
		}
		else if(frequency.equalsIgnoreCase("WEEKLY")){
			monthlyIncome = income * MULTIPLY_WEEKLY;	
		}
		else if(frequency.equalsIgnoreCase("EVERY_TWO_WEEKS")){
			monthlyIncome = income * MULTIPLY_BI_WEEKLY;
		}
		else if(frequency.equalsIgnoreCase("TWICE_A_MONTH")){
			monthlyIncome = income * MULTIPLY_BI_WEEKLY ;
		}
		else if(frequency.equalsIgnoreCase("MONTHLY")){
			monthlyIncome = income;
		}
		else if(frequency.equalsIgnoreCase("ANNUALLY")||frequency.equalsIgnoreCase("ONE_TIME_ONLY")){
			monthlyIncome = income/DIVIDE_YEARLY;
		}
		
		return monthlyIncome;
	}
	

	public static long getNumerIDAtEndOfLink(String link){
		
		long returnvalue = 0;
		Matcher matcher = patternForNumberInLink.matcher(link);
		while (matcher.find( ))
		{
			returnvalue = Long.parseLong(matcher.group());     
		                
		}
		return returnvalue;
	}
	
	public static long getIDFromLink(Object links) {
		
		long returnvalue = 0;
	
		JSONObject jsonObjectLinks = new JSONObject((Map<String,String>)links);
		Map<String,String> self = (Map<String,String>)jsonObjectLinks.get("self");
		String selfHref= (self.get("href")==null)?"": self.get("href");
		URI myUri = null;
		if("".equals(selfHref)) {
			throw new GIRuntimeException("ERROR IN UTIL.getIDFromLink");
		}
		else {
			myUri = URI.create(selfHref);
			myUri.getHost();
			myUri.getPath();
			myUri.getPort();
			
		}
		
		returnvalue = getNumerIDAtEndOfLink(myUri.getPath());
		return returnvalue;
	}	
	
	public static String deriveSsapApplicationStatus(String currentPage,Map<String,SsapApplicantResource> applicantMap){
		String status =  ApplicationStatus.OPEN.getApplicationStatusCode();//"P"
		if (SIGN_AND_SAVE_PAGE_ID.equalsIgnoreCase(currentPage)) {
			status =  ApplicationStatus.SIGNED.getApplicationStatusCode();//"C";
		}
		return status;
	}
	
	/** Get the coverage year
	 * @return coverage year in yyyy format
	 * 
	 */
	public int getCoverageYear() {
		// TBD: The logic to derrive the coverage year
		// The coverage year is to be set from a common method for 2016 enrollments 
		// as there can be LCEs for 2015 and applications for 2016
		return 2015;
	}
	
	/**
	 * get the application in the form of applicationResource
	 * @param caseNumber
	 */
	public SsapApplicationResource getApplicationByCaseNumber(String caseNumber) {
		try {
			return ssapApplicationAccessorImpl.getSsapApplicationsByCaseNumberId(caseNumber);
		}
		catch(Exception e) {
			utilityLogger.error("Error gettign the application for the case number:", e);
			return null;
		}
	}
	
	/**
	 * get the application in the form of applicationResource
	 * @param coverageYear 
	 * @param houseHoldId
	 * @return 
	 */
	public  List<SsapApplicationResource> getApplicationByCmrHouseHoldId(long cmrHouseholdId, long coverageYear) {
		try {
			return ssapApplicationAccessorImpl.getSsapApplicationsForCoverageYear(cmrHouseholdId, coverageYear);
		}
		catch(Exception e) {
			utilityLogger.error("Error gettign the application for the cmrHouseholdId:", e);
			return null;
		}
	}
	
	/**
	 * get the application in the form of applicationResource
	 * @param coverageYear 
	 * @param houseHoldId
	 * @return 
	 */
	public  boolean isNewApplicationCreationApplicable(long cmrHouseholdId, long coverageYear) {
		boolean isApplicable = true;
		try {
			 List<SsapApplicationResource> listSsapApplicationResource = getApplicationByCmrHouseHoldId(cmrHouseholdId, coverageYear);
			 String applicationStatus = "";
			 for(SsapApplicationResource objSsapApplicationResource : listSsapApplicationResource){
				 applicationStatus = objSsapApplicationResource.getApplicationStatus();
				 if(StringUtils.equals("OP", applicationStatus) || StringUtils.equals("UC", applicationStatus) || 
						 StringUtils.equals("SG", applicationStatus) || StringUtils.equals("SU", applicationStatus) || 
						 StringUtils.equals("ER", applicationStatus)){
					 return false;
				 }
				 if(StringUtils.equals("EN", applicationStatus)){
					 long applicationId = getIDFromLink(objSsapApplicationResource.get_links());
					 if(checkForActiveEnrollment(applicationId)) {
						 return false;
					 }
					 else {
						 objSsapApplicationResource.setApplicationStatus("CL");
						 closeSsapApplicationByApplicationId(applicationId, objSsapApplicationResource);
					 }
				 }
			 }
		}
		catch(Exception e) {
			utilityLogger.error("Error in checking for active applications for the cmrHouseholdId:", e);
			return false;
		}
		return isApplicable;
	}
	
	private void closeSsapApplicationByApplicationId(Long ssapApplicationId, SsapApplicationResource objSsapApplicationResource) throws Exception {
		SsapApplicationResource ssapApplication = resourceCreatorImpl.updateSsapApplicationById(ssapApplicationId, objSsapApplicationResource);
	}

	public Broker getAssignedBroker() {
		try {
			int individualId = userService.getLoggedInUser().getActiveModuleId();
			DesignateBroker designateBroker = designateService.findBrokerByIndividualId(individualId);
			if(designateBroker != null && "Active".equalsIgnoreCase(designateBroker.getStatus().toString())) {
				return brokerService.getBrokerDetail(designateBroker.getBrokerId());
				
			}
			return null;
		}
		catch (Exception e) {
			utilityLogger.error("Error in getting the assigned broker", e);
			return null;
		}
	}
	
	public Assister getAssignedAssister() {
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		AssisterDesignateResponseDTO assisterDesignateResponseDTO = null;
		try {
			int individualId = userService.getLoggedInUser().getActiveModuleId();
			
			assisterRequestDTO.setIndividualId(individualId);
			assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
			if(assisterDesignateResponseDTO!=null){
				return assisterDesignateResponseDTO.getAssister();
			}
			
			
			
		} catch (InvalidUserException ex) {
			utilityLogger.error("User not logged in", ex);
		} catch (GIException e) {
			utilityLogger.error("Error retrieving the Assister", e);
		} 
		
		return null;
	}
	
	/**
	 * Method to retrieve Assister designation detail by individual identifier.
	 * 
	 * @param assisterRequestDTO The AssisterRequestDTO instance.
	 * @return assisterDesignateResponseDTO The AssisterDesignateResponseDTO instance.
	 * @throws Exception 
	 */
	private AssisterDesignateResponseDTO retrieveAssisterDesignationDetailByIndividualId(
			AssisterRequestDTO assisterRequestDTO) throws GIException {
		AssisterDesignateResponseDTO assisterDesignateResponseDTO = null;
		utilityLogger.info("REST call start to the GHIX-ENITY module to retreive Assister Designation details.");
		try{
			assisterDesignateResponseDTO = restClassCommunicator.retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
		
		if(assisterDesignateResponseDTO != null && assisterDesignateResponseDTO.getResponseCode()==EntityConstants.RESPONSE_CODE_FAILURE){				
				throw new GIException("Error in retrieving designatedAssister");
		}	
		
		utilityLogger.info("REST call Ends to the GHIX-ENITY module.");
		}
		catch(Exception ex){
			utilityLogger.error("Exception in retrieving designatedAssister details, REST response code is: "+ex+
					((assisterDesignateResponseDTO!=null)?"assisterDesignateResponseDTO is NOT NULL":"assisterDesignateResponseDTO is null"));
			throw new GIException("Exception in retrieving designatedAssister details." + ex.getMessage());
		}
		return assisterDesignateResponseDTO;
	}
	

	public long getApplicationId(SsapApplicationResource ssapApplicationResource) {
		long ssapId = 0;
		
		Object links = ssapApplicationResource.get_links();
		JSONObject jsonObjectLinks = new JSONObject((Map<String,String>)links);
		Map<String,String> self = (Map<String,String>)jsonObjectLinks.get("self");
		String selfHref= (self.get("href")==null)?"": self.get("href");
		
		if("".equals(selfHref)) {
			return ssapId;
		}
		else {
			URI myUri = URI.create(selfHref);
			ssapId = Long.parseLong(myUri.getPath().replace(GhixEndPoints.SsapEndPoints.RELATTIVE_SSAP_APPLICATION_DATA_URL, ""));
		}
		return ssapId;
		
	}
	
	public Map<String,String> getPrimaryApplicantName(List<HouseholdMember> members) {
		Map<String,String> primaryApplicantName = new HashMap<String,String>();
		for(HouseholdMember member: members) {
			if(member.getPersonId() ==1) {
				primaryApplicantName.put(SsapConstants.PRIMARY_APPLICANT_FIRST_NAME, member.getName()==null?"":member.getName().getFirstName());
				primaryApplicantName.put(SsapConstants.PRIMARY_APPLICANT_LAST_NAME, member.getName()==null?"":member.getName().getLastName());
				break;
			}
		}
		
		return primaryApplicantName;
	}
	
	/**
	 * get the application source. if the CSR or any one is helping to create the application the source is captured as the input
	 * @param request
	 * @param ssapApplicationResource
	 * @return
	 * @throws GIException
	 */
	public String getApplicationSource(HttpServletRequest request, SsapApplicationResource ssapApplicationResource) throws GIException {
		try {
			
			if(request.getSession().getAttribute(SsapConstants.CSR_OVER_RIDE) != null && 
					"Y".equalsIgnoreCase(request.getSession().getAttribute(SsapConstants.CSR_OVER_RIDE).toString())) {
				return ssapApplicationResource.getSource();
			}
			if("INDIVIDUAL".equalsIgnoreCase(userService.getLoggedInUser().getDefRole().getName())){
				return ssapApplicationResource.getSource();
				
			}
			else {
				// someone is helping the consumer and the application source is captured in the input
				return request.getParameter(SsapConstants.APPLICATION_SOURCE) != null? request.getParameter(SsapConstants.APPLICATION_SOURCE) :"ON";
			}
		}
		catch(InvalidUserException ivue) {
			utilityLogger.error("Exception accessing the logged in user", ivue);
			throw new GIException(ivue);
		}
		catch (Exception e) {
			utilityLogger.error("Exception accessing the logged in user", e);
			return "ON";
		}
	}
	
	public Location saveMailingLocation(List<HouseholdMember> members, Map<String, SsapApplicantResource> applicantMap ) {
		Location mailingLocation;

		String primaryApplicantGuid = getPrimaryApplicantGuid(members);
		if(applicantMap !=null && applicantMap.containsKey(primaryApplicantGuid)){
			mailingLocation = locationService.findById( applicantMap.get(primaryApplicantGuid).getMailiingLocationId()==null?0: 
				applicantMap.get(primaryApplicantGuid).getMailiingLocationId().intValue());
			if(mailingLocation == null) {
				mailingLocation = new Location();
			}
		}
		else {
			mailingLocation = new Location();
		}
		mailingLocation.setAddress1(members.get(0).getHouseholdContact().getMailingAddress().getStreetAddress1());
		mailingLocation.setAddress2(members.get(0).getHouseholdContact().getMailingAddress().getStreetAddress2());
		mailingLocation.setCity(members.get(0).getHouseholdContact().getMailingAddress().getCity());
		mailingLocation.setState(members.get(0).getHouseholdContact().getMailingAddress().getState());
		mailingLocation.setZip(members.get(0).getHouseholdContact().getMailingAddress().getPostalCode());
		mailingLocation.setCounty(members.get(0).getHouseholdContact().getMailingAddress().getCounty());
		locationService.save(mailingLocation);
		return mailingLocation;
	}
	
	public Location savePrimaryLocation(List<HouseholdMember> members, Map<String, SsapApplicantResource> applicantMap ) {
		Location primaryLocation = null;
		String primaryApplicantGuid = getPrimaryApplicantGuid(members);
		if(applicantMap !=null && applicantMap.containsKey(primaryApplicantGuid)){
			primaryLocation = locationService.findById(applicantMap.get(primaryApplicantGuid).getOtherLocationId()==null?0:
				applicantMap.get(primaryApplicantGuid).getOtherLocationId().intValue());
			if(primaryLocation == null) {
				primaryLocation = new Location();
			}
		}
		else {
			primaryLocation = new Location();
		}
		primaryLocation.setAddress1(members.get(0).getHouseholdContact().getHomeAddress().getStreetAddress1());
		primaryLocation.setAddress2(members.get(0).getHouseholdContact().getHomeAddress().getStreetAddress2());
		primaryLocation.setCity(members.get(0).getHouseholdContact().getHomeAddress().getCity());
		primaryLocation.setState(members.get(0).getHouseholdContact().getHomeAddress().getState());
		primaryLocation.setZip(members.get(0).getHouseholdContact().getHomeAddress().getPostalCode());
		primaryLocation.setCounty(members.get(0).getHouseholdContact().getHomeAddress().getCounty());
		locationService.save(primaryLocation);
		return primaryLocation;
	}
	
	public String getPrimaryApplicantGuid(List<HouseholdMember> members) {
		String applicantGuid = "";
		for(HouseholdMember member:members) {
			if(member.getPersonId()==1) {
				applicantGuid = member.getApplicantGuid();
				break;
			}
		}
		return applicantGuid;
	}
	
	public boolean canProcessDeleteApplicant(SingleStreamlinedApplication singleStreamlinedApplication)  {
		try {
			if(singleStreamlinedApplication.getApplicationGuid() != null && !"".equals(singleStreamlinedApplication.getApplicationGuid())) {
				SsapApplicationResource ssapApplicationResource = getApplicationByCaseNumber(singleStreamlinedApplication.getApplicationGuid());
				if(userService.getLoggedInUser().getActiveModuleId() != ssapApplicationResource.getCmrHouseoldId().intValueExact()) {
					return false;
				}
				
				if(!(ApplicationStatus.OPEN.getApplicationStatusCode().equals(ssapApplicationResource.getApplicationStatus()) ||
						ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equals(ssapApplicationResource.getApplicationStatus()) ||
						ApplicationStatus.SUBMITTED.getApplicationStatusCode().equals(ssapApplicationResource.getApplicationStatus()) ||
						ApplicationStatus.SIGNED.getApplicationStatusCode().equals(ssapApplicationResource.getApplicationStatus()))) {
					return false;
				}
			}
		}
		catch (InvalidUserException e){
			utilityLogger.error("Exception accessing the logged in user while checking the permissions to delete and hence returning false", e);
			return false;
		}
		catch (Exception ex){
			utilityLogger.error("Exception occured while checking the permissions to delete and hence returning false", ex);
			return false;
		}
		return true;
	}
	
	public Integer getMaxpersonId(String caseNumber) throws GIException{
		// set the initial value to 1 as by default the primary contact will be created
		// from the user account
		Integer maxPersonId = 1;
		
		if (caseNumber== null || "".equals(caseNumber)) {
			return maxPersonId;
		}
		
		try{
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
			requestHeaders.setAccept(Collections.singletonList(new MediaType("application","json")));
			SsapApplicationRequest ssapApplicationRequest = new SsapApplicationRequest();
			ssapApplicationRequest.setCaseNumber(caseNumber);
			HttpEntity<?> entity = new HttpEntity<>(ssapApplicationRequest,requestHeaders);
			ResponseEntity<Integer> response = restTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL + "/application/getmaxpersonid", 
					HttpMethod.POST, entity, Integer.class);
			if(response != null && response.getBody() != null){
				maxPersonId = response.getBody();
			}
		}
		catch (Exception e) {
			utilityLogger.error("Exception in getting the max person id for caseNumber:", e);
			throw new GIException("Exception occured in processing");
		}
		
		return maxPersonId;
	}
	
	/**
	 * @param sourceSuffix
	 * @return
	 */
	public String getMatchingSuffixValue(String sourceSuffix) {
		String targetSuffix = "";
		//The list values are not same between RIDP and SSAP
		
		if("SR".equalsIgnoreCase(sourceSuffix)) {
			targetSuffix = "Sr.";
		}
		else if("JR".equalsIgnoreCase(sourceSuffix)) {
			targetSuffix = "Jr.";
		}
		else if("III".equalsIgnoreCase(sourceSuffix)) {
			targetSuffix = "III";
		}
		else if("IV".equalsIgnoreCase(sourceSuffix)) {
			targetSuffix = "IV";
		}
		return targetSuffix;
	}
	
	/**
	 * checks if there is any active enrollment. The application status can be EN 
	 * but the enrollment might be terminated
	 * @param applicationId
	 * @return
	 */
	public boolean checkForActiveEnrollment(long applicationId){
		boolean hasActiveEnrollment = false;
		try {
			AccountUser user =userService.getLoggedInUser(); 
			hasActiveEnrollment = (ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.IS_ACTIVE_HEALTH_ENROLLMENT_FOR_APP_ID, 
					user.getUsername(),HttpMethod.POST,MediaType.APPLICATION_JSON, Boolean.class, applicationId)).getBody();
		} catch (Exception e) {
			utilityLogger.error("Unable to get active enrollments",e);
		}
		return hasActiveEnrollment;
	}
	
	public QepEventRequest createQepEventRequest(long ssapApplicationId, String qepEvent, String qepEventDate, String caseNumber, String applicationType) throws InvalidUserException {
		QepEventRequest qepEventRequest = new QepEventRequest();
		qepEventRequest.setCaseNumber(caseNumber);
		qepEventRequest.setSsapApplicationId(ssapApplicationId);
		qepEventRequest.setQepEvent(qepEvent);
		qepEventRequest.setQepEventDate(qepEventDate);
		qepEventRequest.setApplicationType(applicationType);
		qepEventRequest.setUserId(userService.getLoggedInUser().getId());
		
		if(SsapApplicationEventTypeEnum.QEP.name().equalsIgnoreCase(applicationType)){
			qepEventRequest.setChangeType("QUALIFYING_EVENT");
		} else if(SsapApplicationEventTypeEnum.OE.name().equalsIgnoreCase(applicationType)) {
			qepEventRequest.setChangeType("OE");
		}
		return qepEventRequest;
	}
	
}
