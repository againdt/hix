(function() {
    'use strict';

    angular.module('fileModelDirectiveModule', []).directive('fileModel', fileModel);
    
    fileModel.$inject = ['$parse'];
    
    function fileModel($parse){
        var directive = {
            restrict: 'A',
            require : 'ngModel',
	        link: function(scope, element, attrs, ngModel) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;
                
                function checkSize(fileSize){
	        		return attrs.maxFileSize >= fileSize
	        	}
                
                element.bind('change click', function(){
                	scope.$apply(function(){
	                    modelSetter(scope, element[0].files[0]);
	                    
	                    if(attrs.maxFileSize!== undefined){
	                    	if(element[0].files[0] !== undefined){
		                    	var isSizeValid =checkSize(element[0].files[0].size);
		                    	
		                    	ngModel.$dirty = true;
		                    	ngModel.$pristine = false;
		                    	ngModel.$setValidity('maxFileSize', isSizeValid);
		                    	
		                    }else{
		                    	ngModel.$setValidity('maxFileSize', true);
		                    }
	                    }  
	                });
                });
            }
        };
        return directive;
    }



})();