<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>
$(document).ready(function() {
	var scope = angular.element(document.getElementById("additionalinfo")).scope();
	if (scope) {
		scope.$apply(function() {
			scope['additionalinfo'] = {title: '<spring:message code="indportal.tobacco.additionalInfoNeeded" javaScriptEscape="true"/>'};
		});
	}
});
</script>
<script type="text/ng-template" id="additionalinfo">
    <div spinning-loader="loader"></div>
    <div class="well ng-scope" ng-init="showApplicantDetails()">
        <div ng-if="loader === false">
            <div class="header row-fluid ng-scope">
                <h4><spring:message code="indportal.tobacco.additionalInfoNeeded"/></h4>
            </div>
            <div class="well ng-scope">
                <div class="alert alert-error" ng-if="coverageYearOver">
                    <spring:message code="indportal.portalhome.esdRequired"/>
                </div>
                <div class="well-step">
                    <div class="margin20-lr">
                        <spring:message code="indportal.tobacco.needDetails"/>
                        <p class="alert alert-info"><strong><spring:message code="indportal.tobacco.tobaccoUse"/>:</strong> <spring:message code="indportal.tobacco.tobaccoUseContent"/></p>
                        <p class="alert alert-info"><strong><spring:message code="indportal.tobacco.hardshipExempt"/>:</strong> <spring:message code="indportal.tobacco.hardshipExemptContent"/></p>

                        <div><input type="checkbox" name="sc3" ng-model="hardship.exemption" ng-change="updateHardship()" class="ng-pristine ng-valid" id="exemptionCheckbox">
                            <label for="exemptionCheckbox"><strong><spring:message code="indportal.tobacco.qualifyHardshipQ"/></label>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div id="submitTobacco">
                            <form name="exemptForm" class="form-horizontal ng-pristine ng-valid">

                                <div class="gutter20">
                                    <div class="header row-fluid ng-scope" ng-if="additionalDetails.length>0">
                                        <h4><strong><spring:message code="indportal.tobacco.eligibleMembers"/></h4>
                                        <div ng-if="!hasSelectedTobaccoUse" class="alert alert-error">
                                            <label><spring:message code="indportal.tobacco.fillInTobaccoUse"/></label>
                                        </div>
                                    </div>
                                    <table class="table head" ng-if="additionalDetails.length>0">
                                        <tbody>
                                        <tr>
                                            <th class="exemptCell" scope="col"><spring:message code="indportal.tobacco.household.members"/></th>
                                            <!-- <th class="exemptCell txt-center" scope="col" ><a href="javascript:void(0)" data-toggle="tooltip" rel="tooltip" data-placement="bottom" data-original-title="<strong><spring:message code="indportal.tobacco.seekCoverage"/></strong><br>Select for each household member seeking coverage." data-html="true" tabindex="-1"><spring:message code="indportal.tobacco.seekCoverage"/></a></th> -->
                                            <th class="exemptCell txt-center" scope="col"><a href="javascript:void(0)" data-toggle="tooltip" rel="tooltip" data-placement="bottom" data-original-title="<strong><spring:message code="indportal.tobacco.tobaccoUse"/></strong><br><spring:message code="indportal.tobacco.check.tooltip"/>" data-html="true" tabindex="-1"><spring:message code="indportal.tobacco.tobaccoUse"/></a><font color="red">*</font></th>
                                            <th class="exemptCell txt-center" scope="col" for="exemptionNumber" ng-if="hardship.exemption" width="80px"><spring:message code="indportal.tobacco.hardshipExemptNum"/><font color="red">*</font></th>
                                        </tr>

                                        <tr ng-repeat="applicant in additionalDetails"  ng-if="additionalDetails.length>0">
                                            <th scope="row" class="exemptCell">{{applicant.name}}</th>
                                            <!-- <td class="exemptCell">
                                                <div class="no-checkedselector">
                                                    <div class="toggle-container center">
                                                        <input type="checkbox" name="sc1" ng-model="applicant.applyingForCoverage" value="true" class="ng-pristine ng-valid" ng-disabled=true>
                                                    </div>
                                                </div>
                                            </td> -->
                                            <td class="exemptCell">
                                                <div>
                                                    <div class="no-checkedselector">
                                                        <div class="f f-center">
                                                            <label for="tobaccoUser-no"><input type="radio" ng-model="applicant.isTobaccoUser" ng-disabled="!applicant.enableTobacco" id="tobaccoUser-no" ng-value="false"> <spring:message code="indportal.portalhome.no"/></label>&nbsp;
                                                            <label for="tobaccoUser-yes"><input type="radio" ng-model="applicant.isTobaccoUser" ng-disabled="!applicant.enableTobacco" id="tobaccoUser-yes" ng-value="true"> <spring:message code="indportal.portalhome.yes"/></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td ng-if="hardship.exemption" class="exemptCell">
                                                <div >
                                                    <div class="no-checkedselector">
                                                        <div class="toggle-container center">
                                                            <input type="text" name="sc3" class="exempt span12" ng-model="applicant.exemptionNumber" class="ng-pristine ng-valid" maxlength="10" size="10" id="exemptionNumber">
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <div>
                                        <div><spring:message code="indportal.tobacco.privacyNoticeTxt"/></div>
                                        <div>
                                            <div name="disclaimer" id="disclaimer" class="box-disclaimer">
<span class="list-title"><spring:message code="indportal.tobacco.privacyNotice"/></span>
                                            </p>
<spring:message code="indportal.tobacco.disclaimer1"/>
                                            </p>
<spring:message code="indportal.tobacco.disclaimer2"/>
</p>
<spring:message code="indportal.tobacco.disclaimer3"/>
</p>
<spring:message code="indportal.tobacco.disclaimer4"/>
                                            </div>
                                        </div>
                                        <br><br>
                                    </div>
                                    <div class="pull-left">
                                        <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="goHome()"><spring:message code="indportal.portalhome.godashboard" javaScriptEscape="true"/></a>
                                    </div>
                                    <div class="pull-right" ng-if="additionalDetails.length>0 && !coverageYearOver">
                                        <a href="javascript:void(0)" class="btn btn-primary"  ng-click=enrollAfterTobacoo() ng-disabled="!exemptForm.$valid"><spring:message code="indportal.tobacco.saveContinue"/></a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <!-- Modal for failing submission of additional info-->
                <div ng-show= "modalForm.techIssue">
                    <div modal-show="modalForm.techIssue" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedSubmissionModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h3 id="failedSubmissionModal"><spring:message code="indportal.portalhome.technicalissue"/></h3>
                                </div>
                                <div class="modal-body">
                                    <p><spring:message code="indportal.tobacco.plsComeBack"/></p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.portalhome.close"/></button>
                                </div>
                            </div><!-- /.modal-content-->
                        </div> <!-- /.modal-dialog-->
                    </div>
                </div>
                <!-- Modal for failing submission of additional info-->

                <!-- Modal for Filling in Exemption Number-->
                <div ng-if= "hardship.fillExemptNum">
                    <div modal-show="hardship.fillExemptNum" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ExemptNumberModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h3 id="ExemptNumberModal"><spring:message code="indportal.tobacco.exemptNo"/></h3>
                                </div>
                                <div class="modal-body">
                                    <p><spring:message code="indportal.tobacco.fillInExemptNo"/></p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.portalhome.close"/></button>
                                </div>
                            </div><!-- /.modal-content-->
                        </div> <!-- /.modal-dialog-->
                    </div>
                </div>
                <!-- Modal for Seeking Coverage-->

                <!-- Modal-->
                <div ng-show="showDialog" modal-show="showDialog" class="modal fade ng-scope ng-hide bigger-modal" tabindex="-1" role="dialog" aria-labelledby="financialHelpModal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><span aria-hidden="true">x</span><span class="aria-hidden">close</span></span><span class="aria-hidden">close</span></button>
                                <h3 id="financialHelpModal"><spring:message code="indportal.portalhome.financialhelp" javaScriptEscape="true"/></h3>
                            </div>
                            <div class="modal-body">
                                <p><spring:message code="indportal.portalhome.doyouwanttoapplyforfinancialhelp" javaScriptEscape="true"/></p>
                                <p><spring:message code="indportal.tobacco.stepsFinancialApp"/></p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn pull-left" data-dismiss="modal">Cancel</button>
                                <button class="btn btn-primary" data-dismiss="modal" ng-click="financialHelpPath()"><spring:message code="indportal.tobacco.goToFinApp"/></button>
                                <button class="btn btn-primary" data-dismiss="modal" ng-click="showApplication(modelattrs.caseNumber)"><spring:message code="indportal.portalhome.nothanks"/></button>
                            </div>
                        </div><!-- /.modal-content-->
                    </div> <!-- /.modal-dialog-->
                </div>
            </div>
        </div>
    </div>
</script>