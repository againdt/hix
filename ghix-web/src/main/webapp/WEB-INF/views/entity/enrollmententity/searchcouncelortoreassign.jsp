<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<link href="/hix/resources/css/chosen.css" rel="stylesheet" type="text/css" media="screen,print">
<script src="../resources/js/jquery.autoSuggest.minified.js" type="text/javascript"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-tooltip.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<script src="../resources/js/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<c:url value="/entity/enrollmententity/searchCEC" var="theUrl">  
	 	<c:param name="${df:csrfTokenParameter()}">
	      <df:csrfToken plainToken="true"/>     
	    </c:param>
	 </c:url>
	 <c:url value="/entity/enrollmententity/reassignindividualsToCEC" var="reAssignUrl">  
	 	<c:param name="${df:csrfTokenParameter()}">
	      <df:csrfToken plainToken="true"/>     
	    </c:param>
</c:url>
<style>#alert.modal{left: 3%;
    margin: 0 auto;
    top: 30%;
    width: 90%;}</style>

<div class="gray">
	<h4><spring:message code="label.assister.reassignLightboxTitle"/></h4>
</div>

<form class="form-vertical gutter10" id="searchCE" name="searchCE"  action="<c:url value="/entity/enrollmententity/reassignIndividuals"/>" method="POST">
<df:csrfToken/>
<input type="hidden" id="pageNumber" name="pageNumber" value="1">
<table class="table table-border-none">
		<tbody>
		<tr>
			<td><label class="control-label" for="cecEmailAddress"><spring:message code="label.assister.counselorEmail"/></label></td>
			<td><input class="span" type="text" id="cecEmailAddress" name="cecEmailAddress" value="" placeholder="" /></td>
		</tr>
		<tr>
			<td><label class="control-label" for="cecFirstOrLastName"><spring:message code="label.assister.counselorName"/></label></td>
			<td><input class="span" type="text" id="cecFirstOrLastName" name="cecFirstOrLastName" value="" placeholder="" /></td>
		</tr>
		<tr>
			<td><label for="primarySite" class="required control-label"><spring:message code="label.assister.counselorPrimarySite"/></label></td>
			<td><select  id="primarySite" name="primarySite" path="sitelist" class="ie8">
										<option value=""><spring:message code="label.assister.select"/></option>
										<c:forEach var="primary" items="${primarySitelist}">
											<option id="${primary.id}" value="<c:out value="${primary.id}" />"> ${primary.siteLocationName}</option>
										</c:forEach>
									</select>									
			</td>
		</tr>
		<tr>
		</tr>
		</tbody>
</table>					
<div class="form-actions">
	<span class="pull-right">		
		<input type="submit" id="btnSearch" name="btnSearch" value="Search" class="btn" />				
	</span>
</div>
<c:if test="${showCECtable == 'Y'}">  

<table id="CEC_table" class="table table-striped table-bordered">
<thead>
   <tr class="header">
   					<th scope="col" class="header nmhide" style="width: 175px;"></th>
               		<th>Enrollment Counselor Name</th>
              		<th>Email</th>
               		<th>Primary Site</th>
           		</tr>
           		<c:forEach items="${CECList}" var="entry">
           		<tr>
           			<td><input id='selectCEC' type='radio' name='selectCEC' value="${entry.id}"></td>
           			<td>${entry.firstName} ${entry.lastName}</td><td>${entry.emailAddress}</td>
           			<td>${entry.primaryAssisterSite.siteLocationName}</td>
           		</tr>
           		</c:forEach>
           	</thead>
</table>

<c:choose>
		<c:when test="${fn:length(CECList) > 0 || resultSize > 0}">					
			<div class="pagination">
				<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
			</div>
		</c:when>
		<c:otherwise>								
			<div class="alert alert-info">No matching records found.</div>
		</c:otherwise>
</c:choose>

<div class="form-actions">
	<span class="pull-right">		
		<input type="button" id="btnReassign" name="btnReassign" value="Re-assign" class="btn" />				
	</span>
</div>	
</c:if>			                                                          
</form>

<script type="text/javascript">

$(document).ready(function() {
	
	function closeIFrame() {
		$("#imodal").remove();
		parent.location.reload();		
	}
	
	 $("#btnReassign").on('click', function(e) {
			e.preventDefault();			
			var selectCECId = $('input:radio[name=selectCEC]:checked').val();
			var reAssigned = '<c:out value="${reAssignUrl}"/>';
			var csrfValue= '';
			
			if (reAssigned.indexOf('csrftoken' + "=") >= 0){
		        var prefix = reAssigned.substring(0, reAssigned.indexOf('csrftoken'));
		        var suffix = reAssigned.substring(reAssigned.indexOf('csrftoken'));
		        suffix = suffix.substring(suffix.indexOf("=") + 1);
		        csrfValue = suffix;
		        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")+1) : "";
		        reAssigned = prefix + suffix;
		    }
			
			if(selectCECId == undefined){
				alert("Please select enrollment counselor");
			}
			else {
				$.ajax({
					url : reAssigned,				
					type : 'POST',
					data : {				
						selectCEC : selectCECId	,
						'csrftoken' : csrfValue
					},				
					dataType : "text",
					success : function(data,xhr) {
						if(isInvalidCSRFToken(xhr))                                  
		                    return; 
						if(data != "")
							{
								var splitdata = data.split(',');							
								$('<div id="alert" class="modal stack-modal"><div class="modal-body txt-center"><iframe id="alert" src="#" style="overflow-x:hidden;width:0;border:0;margin:0;padding:0;height:25px;"></iframe><spring:message code="label.assister.ReassignConfirmationMessage_1"/>' +
										splitdata[0] + ' <spring:message code="label.assister.ReassignConfirmationMessage_2"/> '+splitdata[1]+'! <br/><br/><button class="btn" data-dismiss="modal" aria-hidden="true" onClick="window.parent.closeIFrame();">OK</button><br/></div></div>')
										.modal({backdrop:"static",keyboard:"false"});
								
							}
					},
					error : function(data) {							
						alert("Failed to get response");
					},
				});
		}			
});
});

	 function isInvalidCSRFToken(xhr) {
		    var rv = false;
		    if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {                   
		           alert($('Session is invalid').text());
		           //console.log("Session is invalid");
		           rv = true;
		    }
			return rv;
		} 
</script>

