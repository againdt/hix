package com.getinsured.ssap.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.codec.digest.HmacUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.ssap.model.hub.vlp.*;
import com.getinsured.ssap.model.hub.vlp.documents.*;
import com.getinsured.ssap.model.hub.vlp.response.*;
import com.getinsured.ssap.util.SSLUtil;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link VlpIntegrationImpl} class.
 *
 * @author Yevgen Golubenko
 * @since 2/7/19
 */
@RunWith(MockitoJUnitRunner.class)
public class VlpIntegrationImplTest {
  private static final Logger log = LoggerFactory.getLogger(VlpIntegrationImplTest.class);
  private static final String CASE_POC_FULL_NAME = "Sally Q Test";
  private static final String CASE_POC_PHONE = "5555551234";
  private static final String CASE_POC_PHONE_EXT = "1234";
  private static final String TEST_SERVER_URL = "https://7ntk685owc.execute-api.us-gov-west-1.amazonaws.com/nv/ghix/";
  private static final String TEST_SERVER_API_KEY = "4c8g907556ce4f609fff13f57a7fdee8";
  private static final String TEST_SERVER_API_SECRET = "f1f0524f-15c1-4094-b76d-6b1fd1t7g39d";
  private static final String TEST_SERVER_CMS_PARTNER_ID = "SSHIX_NV2DEV";

  @Mock
  private GhixRestTemplate ghixRestTemplate;

  @Mock
  private SsapApplicationRepository ssapApplicationRepository;

  @Mock
  private Properties configProp;

  private VlpIntegrationImpl vlpIntegration;

  private ObjectMapper om = new ObjectMapper();

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    om.enable(SerializationFeature.INDENT_OUTPUT);

    SSLUtil.turnOffSslChecking();
    RestTemplate restTemplate = restTemplate();

    when(ghixRestTemplate.getForObject(anyString(), any(Class.class))).thenAnswer(invocation -> {
      final String url = invocation.getArgument(0, String.class);
      final Class clazz = invocation.getArgument(1, Class.class);
      HttpHeaders headers = getAuthHeaders();
      HttpEntity entity = new HttpEntity(headers);
      //HttpEntity<?> requestHttpEntity = new HttpEntity<>(null, getAuthHeaders());
      //return restTemplate.getForObject(url, clazz);
      return restTemplate.exchange(url, HttpMethod.GET, entity, clazz);
    });

    when(ghixRestTemplate.postForObject(anyString(), any(Class.class), any(Class.class))).thenAnswer(invocation -> {
      final String url = invocation.getArgument(0, String.class);

      Object requestObject = invocation.getArgument(1, Object.class);

      if(requestObject instanceof VlpRequest) {
        VlpRequest vlpRequest = (VlpRequest)requestObject;
        setCasePocFields(vlpRequest);

        System.out.println("VLP Request: " + om.writeValueAsString(vlpRequest));
        HttpEntity<?> requestHttpEntity = new HttpEntity<>(vlpRequest, getAuthHeaders());
        requestObject = requestHttpEntity;
      } else if(requestObject instanceof VlpReverifyRequest) {
        VlpReverifyRequest vlpReverifyRequest = (VlpReverifyRequest) requestObject;
        System.out.println("VLP Reverify Request " + om.writeValueAsString(vlpReverifyRequest));
        HttpEntity<?> requestHttpEntity = new HttpEntity<>(vlpReverifyRequest, getAuthHeaders());
        requestObject = requestHttpEntity;
      } else if(requestObject instanceof VlpResubmitWithSevisRequest) {
        VlpResubmitWithSevisRequest vlpResubmitWithSevisRequest = (VlpResubmitWithSevisRequest) requestObject;
        System.out.println("VLP Resubmit with SEVIS id Request: " + om.writeValueAsString(vlpResubmitWithSevisRequest));
        HttpEntity<?> requestHttpEntity = new HttpEntity<>(vlpResubmitWithSevisRequest, getAuthHeaders());
        requestObject = requestHttpEntity;
      }
//      } else if(requestObject instanceof VlpStepTwoResolutionRequest) {
//        VlpStepTwoResolutionRequest vlpStepTwoResolutionRequest = (VlpStepTwoResolutionRequest) requestObject;
//        System.out.println("VLP Check Step 2 Case Resolution Request: " + om.writeValueAsString(vlpStepTwoResolutionRequest));
//        HttpEntity<?> requestHttpEntity = new HttpEntity<>(vlpStepTwoResolutionRequest, getAuthHeaders());
//        requestObject = requestHttpEntity;
//      }

      final Class response = invocation.getArgument(2, Class.class);

      return restTemplate.postForObject(url, requestObject, response);
    });

    when(configProp.getProperty("ghixHubIntegrationURL")).thenReturn(TEST_SERVER_URL);

    vlpIntegration = new VlpIntegrationImpl(ghixRestTemplate, configProp);
    vlpIntegration.postConstruct();

    om.enable(SerializationFeature.INDENT_OUTPUT);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void verifyI94_Valid() {
    I94Document document = new I94Document();
    document.setI94Number("60060015469");

    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    // Person ID 79756
    vlpPayload.setDateOfBirth(getDate(1983, 11, 15));
    vlpPayload.setFirstName("Tonya");
    vlpPayload.setLastName("Landers");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-94 Document not valid!");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setClientIp(getClientIp());
    vlpRequest.setPayload(vlpPayload);

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);

    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpAction.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyI327_Valid() {
    I327Document document = new I327Document();
    document.setAlienNumber("660015540");

    VlpPayload vlpPayload = new VlpPayload();

    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDateOfBirth(getDate(1983, 11, 15));
    vlpPayload.setFirstName("Jerome");
    vlpPayload.setLastName("Donaldson");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);

    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-327 Document not valid!");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());


    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpAction.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyI327_fromSoapUI_ResubmitWithSEVIS() {
    I327Document document = new I327Document();
    document.setAlienNumber("666778084");

    VlpPayload vlpPayload = new VlpPayload();

    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber("7036922890");
    vlpPayload.setDateOfBirth(getDate(1974, 10, 8));
    vlpPayload.setFirstName("Rose");
    vlpPayload.setLastName("Aiden");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequesterCommentsForHub("VAWA");

    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-327 Document not valid!");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());


    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpAction.NONE, vlpResponse.getActionResult());
    Assert.assertEquals("Next step should be resubmit with sevisid", VlpActionResult.RESUBMIT_WITH_SEVISID, vlpResponse.getActionResult());

    document.setSevisId("6060015514");
    document.setCaseNumber(vlpResponse.getIndividualDetails().get(0).getCaseNumber());
    vlpPayload.setDhsDocument(document);
    vlpRequest.setPayload(vlpPayload);

    VlpResponse resubmitWithSevisIdResponse = vlpIntegration.resubmitWithSevisId(vlpRequest);
    Assert.assertNotNull("Resubmit with SEVIS ID response is null", resubmitWithSevisIdResponse);
  }

  @Test
  public void verifyI327_fromSoapUI_ResubmitWithSEVIS_NextStep() {
    I327Document document = new I327Document();
    document.setAlienNumber("666778084");

    VlpPayload vlpPayload = new VlpPayload();

    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber("7036922890");
    vlpPayload.setDateOfBirth(getDate(1974, 10, 8));
    vlpPayload.setFirstName("Rose");
    vlpPayload.setLastName("Aiden");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequesterCommentsForHub("VAWA");

    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-327 Document not valid!");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());


    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpAction.NONE, vlpResponse.getActionResult());
    Assert.assertEquals("Next step should be resubmit with sevisid", VlpActionResult.RESUBMIT_WITH_SEVISID, vlpResponse.getActionResult());

    String caseNumber = vlpResponse.getIndividualDetails().get(0).getCaseNumber();
    Assert.assertNotNull("Case Number was not returned", caseNumber);

    document.setCaseNumber(caseNumber);
    document.setSevisId("6060015514");

    vlpPayload.setDhsDocument(document);
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpRequest.setCasePOCPhoneNumber(CASE_POC_PHONE);

    VlpResponse resubmitWithSevisIdResponse = vlpIntegration.resubmitWithSevisId(vlpRequest);
    Assert.assertNotNull("Resubmit with Sevis Id response is null", resubmitWithSevisIdResponse);

    IndividualDetails individualDetails = resubmitWithSevisIdResponse.getIndividualDetails().get(0);
    Assert.assertEquals(5, individualDetails.getEligibilityStatementCode());
    Assert.assertEquals(VlpActionResult.CHECK_CASE_RESOLUTION, resubmitWithSevisIdResponse.getActionResult());
  }

  @Test
  public void verifyI327_NotValid() {
    I327Document document = new I327Document();
    document.setAlienNumber("660015490");

    VlpPayload vlpPayload = new VlpPayload();

    // 79803, row 55
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDateOfBirth(getDate(1983, 11, 15));
    vlpPayload.setFirstName("Kevin");
    vlpPayload.setLastName("Allen");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);

    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-327 Document not valid!");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);

    // TODO: Should be: ResponseCode.VALIDATION_FAILED, ValidationErrorCode.CASE_NUMBER_REQUIRED
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpAction.NONE, vlpResponse.getActionResult());
  }


  @Test
  public void verifyI551_Valid() {
    I551Document document = new I551Document();
    document.setAlienNumber("660015518");
    document.setReceiptNumber("OCM6060015518");

    // Person ID 79759
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("Peter");
    vlpPayload.setLastName("Medina");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1983,11,15));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-551 Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpAction.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyI571_Valid() {
    I571Document document = new I571Document();
    document.setAlienNumber("660015499");

    // Person ID 79768
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("Della");
    vlpPayload.setLastName("Gallagher");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1983,11,15));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-571 Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpAction.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyI766_Valid() {
    I766Document document = new I766Document();
    document.setAlienNumber("660015562");
    document.setReceiptNumber("OCM6060015562");
    document.setDocExpirationDate(getDate(2019, 9, 12));

    // Person ID 79696
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("JORDAN");
    vlpPayload.setLastName("ROBLES");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1946,1,14));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-766 Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyI766_2_Valid() {
    I766Document document = new I766Document();
    document.setAlienNumber("660015498");
    document.setReceiptNumber("OCM6060015498");
    document.setDocExpirationDate(getDate(2020, 8, 8));

    // Person ID 79697
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("JAKE");
    vlpPayload.setLastName("RAMOS");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1963,7,14));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-766 2nd Test Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyUnexpiredForeignPassport_Valid() {
    UnexpiredForeignPassportDocument document = new UnexpiredForeignPassportDocument();
    document.setPassportNumber("600060015505");
    document.setCountryOfIssuance("STL");
    document.setDocExpirationDate(getDate(2020, 7,3));

    // Person ID 79748
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("EMMA");
    vlpPayload.setLastName("GOODWIN");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1952,11,26));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("Unexpired Foreign Passport Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyUnexpiredForeignPassport_2_Valid() {
    UnexpiredForeignPassportDocument document = new UnexpiredForeignPassportDocument();
    document.setPassportNumber("600060015437");
    document.setCountryOfIssuance("TUR");
    document.setDocExpirationDate(getDate(2020, 9,28));

    // Person ID 79752
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("Lilian");
    vlpPayload.setLastName("Moody");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1983,11,15));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("Unexpired Foreign Passport Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyUnexpiredForeignPassport_3_Valid() {
    UnexpiredForeignPassportDocument document = new UnexpiredForeignPassportDocument();
    document.setPassportNumber("600060015543");
    document.setCountryOfIssuance("RWA");
    document.setDocExpirationDate(getDate(2019, 7,21));

    // Person ID 79754
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("Samantha");
    vlpPayload.setLastName("Tanner");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1983,11,15));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("Unexpired Foreign Passport Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);

    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyMachineReadableImmigrationVisa_Valid() {
    MachineReadableImmigrantVisaDocument document = new MachineReadableImmigrantVisaDocument();
    document.setAlienNumber("660015508");
    document.setPassportNumber("600060015508");
    document.setCountryOfIssuance("GRD");

    // Person ID 79753
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("Jerome");
    vlpPayload.setLastName("Henderson");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1983,11,15));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("Machine Readable Immigration Visa Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyDS2019_Valid() {
    Ds2019Document document = new Ds2019Document();
    document.setSevisId("6060015514");

    // Person ID 79755
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("Andres");
    vlpPayload.setLastName("Mandelay");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1983,11,15));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("DS2019 Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyNaturalizationCertificate_Valid() {
    // State-Reorg-v37-Conversion-H92 Step1Payloads-092118-143942_v18_modified.xslx
    // row 114
    NaturalizationCertificateDocument document = new NaturalizationCertificateDocument();
    document.setAlienNumber("660015456");
    document.setNaturalizationNumber("600060015456");

    // Person ID 79866
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("Anita");
    vlpPayload.setLastName("Polk");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1983,11,15));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("Naturalization Certificate Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);

    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyI94InPassport_Valid()
  {
    I94InPassportDocument document = new I94InPassportDocument();
    document.setI94Number("60060015509");
    document.setPassportNumber("600060015509");
    document.setCountryOfIssuance("VEN");
    document.setDocExpirationDate(getDate(2020, 7, 6));

    // Person ID 79745
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("CHARLOTTE");
    vlpPayload.setLastName("STANLEY");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1982, 7, 3));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if (!vlpPayload.getDhsDocument().isValid())
    {
      Assert.fail("I-94 In Passport Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);

    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyI94InPassport_Valid_2() {
    I94InPassportDocument document = new I94InPassportDocument();
    document.setI94Number("46191684624");
    document.setVisaNumber("62843119");
    document.setPassportNumber("8844697399");
    document.setCountryOfIssuance("KOR");
    document.setDocExpirationDate(getDate(2019,1,1));

    // Person ID 79745
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("Salvador");
    vlpPayload.setLastName("Ayala");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1974,10,3));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);

    vlpPayload.setCasePOCFullName("DHS Tester");
    vlpPayload.setCasePOCPhoneNumber("5555555555");
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-94 In Passport Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);

    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyTemporaryI551Stamp_Valid() {
    TemporaryI551StampDocument document = new TemporaryI551StampDocument();
    document.setAlienNumber("660015567");

    // Person ID 79764
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("Kiera");
    vlpPayload.setLastName("Ahmed");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1983,11,15));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("Temporary I-551 Stamp In Passport Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyI20_Valid() {
    I20Document document = new I20Document();
    document.setSevisId("6060015514");

    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("CHARLOTTE");
    vlpPayload.setLastName("STANLEY");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1982,7,2));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-20 Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void verifyOtherAlien_Valid() {
    OtherAlienDocument document = new OtherAlienDocument();
    document.setAlienNumber("738812313");
    document.setDocDescReq("Notice of Action");

    // Person ID 79764
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("LAYLA");
    vlpPayload.setLastName("SCHWARTZ");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1967,4,9));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("Other Alien Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }


  @Test
  public void verifyOtherI94_Valid() {
    // H92-Step 1 VLPv37 E2E Test Data row 8
    OtherI94Document document = new OtherI94Document();
    document.setI94Number("82946778266");
    document.setDocDescReq("Letter Granting Asylum");

    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("Thaddeus");
    vlpPayload.setLastName("Delaney");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1982,11,17));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);

    // Next 3 lines are differs from the most.
    vlpPayload.setRequestGrantDateIndicator(false);
    vlpPayload.setCasePOCFullName("DHS Tester");
    vlpPayload.setCasePOCPhoneNumber("5555555555");

    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("Other I-94 Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);

    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void testStep1ToStep1a() {
    log.info("Testing e2e flow step 1 => step 1a Arrival/Departure Record in Foreign Passport (I-94)");
    I94InPassportDocument document = new I94InPassportDocument();
    document.setI94Number("60060015509");
    document.setPassportNumber("600060015509");
    document.setCountryOfIssuance("VEN");
    document.setDocExpirationDate(getDate(2020,7,6));

    // Person ID 79745
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("CHARLOTTE");
    vlpPayload.setLastName("STANLEY");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1982,7,3));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-94 In Passport Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);

    VlpActionResult actionResult = VlpActionResult.get(vlpResponse);

    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, actionResult);

    Assert.assertEquals("Next step should be Re-Submit With SEVIS", VlpActionResult.RESUBMIT_WITH_SEVISID, actionResult);

    /*
     *
     * Re-submit with SEVISID
     *
     */
    document.setCaseNumber(vlpResponse.getIndividualDetails().get(0).getCaseNumber());
    document.setSevisId("6060015514"); // Invalid sevis from another test.

    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-94 In Passport Document with SEVISID not valid");
    }

    vlpRequest.setPayload(vlpPayload);
    vlpResponse = vlpIntegration.resubmitWithSevisId(vlpRequest);

    /*
    {
      "IntialVerificationResponseTypeResponseSet": {
        "EligStatementCd":22,
        "USCitizenCode":"X",
        "NonCitLastName":"STANLEY",
        "NonCitCoaCode":"F1",
        "WebServSftwrVer":"37",
        "QualifiedNonCitizenCode":"N",
        "FiveYearBarMetCode":"X",
        "LawfulPresenceVerifiedCode":"Y",
        "NonCitMiddleName":"STANLEY",
        "CaseNumber":"6000060015509OC",
        "NonCitBirthDate":"1982-07-03",
        "EligStatementTxt":"Temporary Resident - Temporary Employment Authorized",
        "NonCitFirstName":"CHARLOTTE",
        "FiveYearBarApplyCode":"X"
      },
      "ResponseMetadata":{
        "ResponseCode":"HS000000",
        "ResponseDescriptionText":"Successful.",
        "TDSResponseDescriptionText":"Successful."
      }
     }
     */
    Assert.assertNotNull("No VlpResponse after re-submit with sevisid call", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
    Assert.assertEquals("USCitizenCode should be " + MultiCode.X, MultiCode.X, vlpResponse.getIndividualDetails().get(0).getUsCitizenCode());
    Assert.assertEquals("Lawful presence should be verified", MultiCode.Y, vlpResponse.getIndividualDetails().get(0).getLawfulPresenceVerifiedCode());
  }

  @Test
  public void testStep1ToStep1a_2() {
    // Arrival/Departure Record in Foreign Passport (I-94)
    I94InPassportDocument document = new I94InPassportDocument();
    document.setI94Number("46191684624");
    document.setVisaNumber("62843119");
    document.setPassportNumber("8844697399");
    document.setCountryOfIssuance("KOR");
    document.setDocExpirationDate(getDate(2019,1,1));
    document.setCaseNumber("12345");

    // Person ID 79745
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("Salvador");
    vlpPayload.setLastName("Ayala");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1974,10,3));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setDhsDocument(document);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-94 In Passport Document not valid");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);

    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());

    Assert.assertEquals("Next step should be re-submit with sevisid", VlpActionResult.RESUBMIT_WITH_SEVISID, vlpResponse.getActionResult());

    // Resubmitting with SEVISID and CaseNumber
    document.setSevisId("6060015514"); // Invalid sevis from another test.
    document.setCaseNumber(vlpResponse.getIndividualDetails().get(0).getCaseNumber());
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-94 In Passport Document with SEVISID not valid");
    }

    vlpRequest.setPayload(vlpPayload);
    vlpResponse = vlpIntegration.resubmitWithSevisId(vlpRequest);

    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
  }

  @Test
  public void testGetCaseStatus() {
    String caseNumber = "6000060015509OC";
    /*
    {"payload": {"AKA": "Tonya Landers",
    "DHSID": {"I94DocumentID":
    {"SEVISID": null,
    "I94Number": "60060015469",
     "CaseNumber": null,
     "DocExpirationDate": null}},
     "LastName": "Landers", "FirstName": "Tonya", "MiddleName": null, "DateOfBirth": "1980-04-25",
     "CasePOCFullName": "Tonya Landers", "CasePOCPhoneNumber": "5419999999",
     "RequesterCommentsForHub": null, "RequestGrantDateIndicator": true,
     "RequestSponsorDataIndicator": true, "FiveYearBarApplicabilityIndicator": true}, "clientIp": null, "documentType": "I94Document", "applicationId": 7450}
     */
    I94Document document = new I94Document();
    document.setI94Number("60060015469");

    document.setCaseNumber("6000060015509OC"); // New

    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);

    vlpPayload.setDateOfBirth(getDate(1983, 11, 15));
    vlpPayload.setFirstName("Tonya");
    vlpPayload.setLastName("Landers");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setDhsDocument(document);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-94 Document not valid!");
    }

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setClientIp(getClientIp());
    vlpRequest.setPayload(vlpPayload);

    CaseStatusResponse caseStatusResponse = vlpIntegration.caseStatus(document.getCaseNumber());
    Assert.assertNotNull("Vlp Response was null for given case status", caseStatusResponse);
  }

  @Test
  public void step1aReverification_Mon_Wed_Fri() {
    Calendar cal = Calendar.getInstance();
    int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

    if( dayOfWeek == Calendar.MONDAY ||
      dayOfWeek == Calendar.WEDNESDAY ||
      dayOfWeek == Calendar.FRIDAY)
    {
      System.out.println("Executing test case because today is MON, WED or FRI");
    } else {
      System.out.println("Test case passes because today is not MON, WED or FRI");
      return;
    }

    I766Document document = new I766Document();
    document.setAlienNumber("660011846");
    document.setReceiptNumber("OCM6060011846");;
    document.setDocExpirationDate(getDate(2014, 12, 21));

    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setDateOfBirth(getDate(1967,4, 9));
    vlpPayload.setFirstName("LAYLA");
    vlpPayload.setLastName("SCHWARTZ");
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setDhsDocument(document);

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setDocumentType(document.getDocumentType());
    vlpRequest.setCmsPartnerId(getCmsPartnerId());
    vlpRequest.setClientIp(getClientIp());
    vlpRequest.setApplicationId(getApplicationId());

    vlpRequest.setPayload(vlpPayload);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-766 Document is not valid");
    }

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    try {
      System.out.format("Verify response: %s%n", om.writeValueAsString(vlpResponse));
    } catch (JsonProcessingException e) {
      Assert.fail(e.getMessage());
    }

    Assert.assertNotNull("vlpResponse is null", vlpResponse);
    Assert.assertEquals(vlpResponse.getActionResult(), VlpActionResult.REVERIFY);
    String caseNumber = vlpResponse.getIndividualDetails().get(0).getCaseNumber();
    Assert.assertNotNull("Case number is missing from response", caseNumber);

    vlpRequest.getPayload().getDhsDocument().setCaseNumber(caseNumber);
    vlpRequest.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpRequest.setCasePOCFullName(CASE_POC_FULL_NAME);

    VlpResponse reverifyResponse = vlpIntegration.reverify(vlpRequest);

    try {
      System.out.format("Re-Verify response: %s%n", om.writeValueAsString(reverifyResponse));
    } catch (JsonProcessingException e) {
      Assert.fail(e.getMessage());
    }

    Assert.assertNotNull("reverify response is null", reverifyResponse);
    Assert.assertNotEquals(reverifyResponse.getActionResult(), VlpActionResult.RETRY);
    Assert.assertEquals(reverifyResponse.getActionResult(), VlpActionResult.CHECK_CASE_RESOLUTION);

    System.out.format("Test case complete: %s%n", reverifyResponse.getActionResult());

    VlpResponse checkCaseResolutionResponse = vlpIntegration.getStepTwoCaseResolution(vlpRequest);
    Assert.assertNotNull("Check case resolution response is null", checkCaseResolutionResponse);
    Assert.assertEquals("Action result is unexpected", checkCaseResolutionResponse.getActionResult(), VlpActionResult.RETRY);
  }

  @Test
  public void step1aReverification_Tue_Thu() {
    Calendar cal = Calendar.getInstance();
    int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

    if( dayOfWeek == Calendar.TUESDAY ||
      dayOfWeek == Calendar.THURSDAY)
    {
      System.out.println("Executing test case because today is TUE or THU");
    } else {
      System.out.println("Test case passes because today is not TUE or THU");
      return;
    }

    I766Document document = new I766Document();
    document.setAlienNumber("660011846");
    document.setReceiptNumber("OCM6060011846");;
    document.setDocExpirationDate(getDate(2014, 12, 21));

    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setDateOfBirth(getDate(1975,10, 16));
    vlpPayload.setFirstName("Kenny");
    vlpPayload.setLastName("Ethersonstcedez");
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setDhsDocument(document);

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setDocumentType(document.getDocumentType());
    vlpRequest.setCmsPartnerId(getCmsPartnerId());
    vlpRequest.setClientIp(getClientIp());
    vlpRequest.setApplicationId(getApplicationId());

    vlpRequest.setPayload(vlpPayload);

    if(!vlpPayload.getDhsDocument().isValid()) {
      Assert.fail("I-766 Document is not valid");
    }

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);

    try {
      System.out.format("Verify response: %s%n", om.writeValueAsString(vlpResponse));
    } catch (JsonProcessingException e) {
      Assert.fail(e.getMessage());
    }

    Assert.assertNotNull("vlpResponse is null", vlpResponse);
    Assert.assertEquals(vlpResponse.getActionResult(), VlpActionResult.REVERIFY);
    String caseNumber = vlpResponse.getIndividualDetails().get(0).getCaseNumber();
    Assert.assertNotNull("Case number is missing from response", caseNumber);

    vlpRequest.getPayload().getDhsDocument().setCaseNumber(caseNumber);
    vlpRequest.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpRequest.setCasePOCFullName(CASE_POC_FULL_NAME);
    document.setAlienNumber("660011846");
    document.setReceiptNumber("OCM6060011846");

    VlpResponse reverifyResponse = vlpIntegration.reverify(vlpRequest);

    try {
      System.out.format("Re-Verify response: %s%n", om.writeValueAsString(reverifyResponse));
    } catch (JsonProcessingException e) {
      Assert.fail(e.getMessage());
    }

    Assert.assertNotNull("reverify response is null", reverifyResponse);
    Assert.assertNotEquals(reverifyResponse.getActionResult(), VlpActionResult.RETRY);
    Assert.assertEquals(reverifyResponse.getActionResult(), VlpActionResult.CHECK_CASE_RESOLUTION);

    VlpResponse stepTwoCaseResolution = vlpIntegration.getStepTwoCaseResolution(vlpRequest);

    try {
      System.out.format("Step 2 case resolution response: %s%n", om.writeValueAsString(stepTwoCaseResolution));
    } catch (JsonProcessingException e) {
      Assert.fail(e.getMessage());
    }


    Assert.assertNotNull("Case status response is null", stepTwoCaseResolution);
  }

  @Test
  public void testStep2CaseResolutionMapping() throws IOException {
    String json = "{\"RetriveStep2ResponseTypeResponseSet\":{}," +
      "\"ResponseMetadata\":{\"ResponseCode\":\"HE010043\"," +
      "\"ResponseDescriptionText\":\"Additional (Step 2) or Third (Step 3) verification results for the requested case are not available at this time.\"}}";
    VlpResponse vlpResponse = om.readValue(json, VlpResponse.class);

    Assert.assertNotNull(vlpResponse);
    Assert.assertNotNull(vlpResponse.getResponseMetadata());
  }

  @Test
  public void testUnsupportedDocumentType() {
    UnsupportedDocument document = new UnsupportedDocument();

    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setFirstName("Jerome");
    vlpPayload.setLastName("Henderson");
    vlpPayload.setAka(getAka(vlpPayload));
    vlpPayload.setDateOfBirth(getDate(1983,11,15));
    vlpPayload.setFiveYearBarApplicabilityIndicator(true);
    vlpPayload.setRequestGrantDateIndicator(true);
    vlpPayload.setCasePOCFullName(CASE_POC_FULL_NAME);
    vlpPayload.setCasePOCPhoneNumber(CASE_POC_PHONE);
    vlpPayload.setDhsDocument(document);


    Assert.assertTrue("Unsupported Document is valid should return true", vlpPayload.getDhsDocument().isValid());

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(getApplicationId());
    vlpRequest.setPayload(vlpPayload);
    vlpRequest.setClientIp(getClientIp());

    VlpResponse vlpResponse = vlpIntegration.verify(vlpRequest);
    Assert.assertNotNull("There is no VLP Response received", vlpResponse);
    Assert.assertNotEquals("Communication error response should not happen", ResponseCode.COMMUNICATION_ERROR, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Response code is NONE which is invalid", ResponseCode.NONE, vlpResponse.getResponseCode());
    Assert.assertNotEquals("Next step should not be NOTHING", VlpActionResult.NONE, vlpResponse.getActionResult());
    Assert.assertEquals("Unsupported Document should not pass verification and require DMI", VlpActionResult.VALIDATION_FAILED, vlpResponse.getActionResult());
  }

  private Date getDate(int year, int month, int day) {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.YEAR, year);
    cal.set(Calendar.MONTH, month - 1);
    cal.set(Calendar.DAY_OF_MONTH, day - 1);

    return cal.getTime();
  }

  private String getAka(VlpPayload vlpPayload) {
    return String.format("%s %s", vlpPayload.getFirstName(), vlpPayload.getLastName());
  }

  private String getClientIp() {
    return "10.0.0.7";
  }

  private RestTemplate restTemplate() {
    RestTemplate restTemplate = new RestTemplate();
    return restTemplate;
  }

  /**
   * Returns Authorization headers that we need to order to talk to AWS.
   * @return {@link HttpHeaders} with Authorization.
   */
  private HttpHeaders getAuthHeaders() {
    final HttpHeaders headers = new HttpHeaders();
    final String authKey = TEST_SERVER_CMS_PARTNER_ID + ":" + HmacUtils.hmacSha256Hex(TEST_SERVER_API_SECRET.getBytes(), TEST_SERVER_API_KEY.getBytes());
    final String secretValue = Base64.getEncoder().encodeToString(authKey.getBytes(StandardCharsets.UTF_8));
    headers.set("Authorization", "Basic " + secretValue);
    return headers;
  }

  private long getApplicationId() {
    // yb@yopmail.com test user on id6dev.
    return 555_555_555l; // 525050l
  }

  private void setCasePocFields(VlpRequest request) {
    request.setCmsPartnerId(getCmsPartnerId());
    request.getPayload().setCasePOCFullName(getCasePocFullName());
    request.getPayload().setCasePOCPhoneNumber(getCasePOCPhone());
  }

  private String getCmsPartnerId() {
    return "SSHIX_NV2DEV";
  }

  private String getCasePocFullName() {
    return "DHS Tester";
  }

  private String getCasePOCPhone() {
    return "5555555555";
  }
}
