package com.getinsured.hix.batch.enrollment.terminator;

import java.util.Arrays;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.skip.EnrollmentIRSOut;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.JiraUtil;
//import com.getinsured.hix.batch.util.GhixBatchConstants;

public class TerminateEnrollmentMonthlyIRSOutboundJob  implements Tasklet{
	private static final Logger LOGGER = LoggerFactory.getLogger(TerminateEnrollmentMonthlyIRSOutboundJob.class);
	private EnrollmentIRSOut enrollmentIRSOut;
	private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;
	
	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
		try{
			
			if(enrollmentIRSOut!=null && enrollmentIRSOut.getSkippedHouseholdMap()!=null && !enrollmentIRSOut.getSkippedHouseholdMap().isEmpty()){
				//StepExecution stepExecution=chunkContext.getStepContext().getStepExecution();
					logBug(enrollmentIRSOut,chunkContext.getStepContext().getStepExecution().getJobExecutionId());
				LOGGER.error("TerminateEnrollmentMonthlyIRSOutboundJob :: The Households for which IRS Monthly report failed are "+enrollmentIRSOut.getSkippedHouseholdMap());
			}
			
		}catch(Exception e){
			LOGGER.error("Error while logging skip and success details "+EnrollmentConstants.ENROLLMENT_MONTHLY_OUT_JOB + " :: "+e.toString());
		}finally{
			
		}
		LOGGER.info("Terminate");
		return RepeatStatus.FINISHED;

}
	private void logBug(EnrollmentIRSOut enrollmentIRSOut, Long executionId) {
		String environment = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTENVIRONMENT);
		Boolean isJiraEnabled = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENABLE_JIRA_CREATION));
		StringBuilder descriptionstringBuilder = new StringBuilder();
         for (Entry<String, String> entry : enrollmentIRSOut.getSkippedHouseholdMap().entrySet()) {
                 descriptionstringBuilder.append(" Record skipped While running IRS out batch : "+ entry.getKey()+"\n"
                                  +" Exception Stack : "+ entry.getValue());
                 descriptionstringBuilder.append("\n");
         }
         
         String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
		 String fixVersion = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_FIX_VERSION);
			
		 if(environment!=null && environment.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_ENVIRONMENT_PRODUCTION) && isJiraEnabled){
			 JiraUtil.logBug(Arrays.asList(enrollmentComponent), 
    		         Arrays.asList(fixVersion), 
        		         descriptionstringBuilder.toString(), 
        		         "Record skipped While running IRS out batch : "+ executionId,
        		         null);
	}
		enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.IRS,
				 "Record skipped While running IRS out batch : " + executionId, descriptionstringBuilder.toString());
	}
	public EnrollmentIRSOut getEnrollmentIRSOut() {
		return enrollmentIRSOut;
	}
	public void setEnrollmentIRSOut(EnrollmentIRSOut enrollmentIRSOut) {
		this.enrollmentIRSOut = enrollmentIRSOut;
	}
	/**
	 * @return the enrollmentGIMonitorUtil
	 */
	public EnrollmentGIMonitorUtil getEnrollmentGIMonitorUtil() {
		return enrollmentGIMonitorUtil;
	}
	/**
	 * @param enrollmentGIMonitorUtil the enrollmentGIMonitorUtil to set
	 */
	public void setEnrollmentGIMonitorUtil(EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		this.enrollmentGIMonitorUtil = enrollmentGIMonitorUtil;
	}
}
