import React from 'react';
import SummaryItem from './SummaryItem';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import helperFunctions from '../../utils/helperFunctions';
import SubHeader from '../headers/SubHeader';

// Components

// Utils


const IncomeInformationSummary = props => {
    const {
        coverageYear,
        householdMembers,
        buttonContainer: ButtonContainer,
        PAGES
    } = props;

    return householdMembers.map((hhm , idx)=> {

        const {
            name,
            annualTaxIncome: {
                projectedIncome,
                reportedIncome
            },
        } = hhm;

        return (
            <div className='summary__block' key={`${name.firstName}_${idx}`}>
                <div className="usa-grid usa-width-one-whole summary__header">
                    <SubHeader
                        text={
                            <span>{ normalizeNameOfPerson(name)}</span>
                        }
                        headerClassName={'usa-heading-alt'}
                        additionalElement={(
                            <ButtonContainer
                                type="editButton_secondary"
                                args={[idx, PAGES.INCOME_SOURCES_PAGE]}
                            />
                        )}
                        additionalElementClassName={'header-btn'}
                    />
                </div>

                <SummaryItem
                    category={`Yearly income in ${coverageYear}`}
                    value={ helperFunctions.getIncomeValueForRender(projectedIncome, reportedIncome) }
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label',
                        divClass2: 'summary__value'
                    }}
                />

            </div>

        );
    });
};

export default IncomeInformationSummary;
