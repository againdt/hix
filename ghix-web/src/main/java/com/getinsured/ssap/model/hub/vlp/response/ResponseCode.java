package com.getinsured.ssap.model.hub.vlp.response;

import java.util.Arrays;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Response codes for VLP.
 * <p>
 *   <ul>
 *     <li>Successful response code would be staring with HS followed by 6 digits</li>
 *     <li>Failure response code will start with HE followed by 6 digits</li>
 *   </ul>
 * </p>
 * <p>
 *   Sample success code:
 *   <ul>
 *     <li>HS000000</li>
 *   </ul>
 *
 *   Sample error codes:
 *   <ul>
 *     <li>HE003000 - Validation Failed (see {@link ValidationErrorCode} for details)</li>
 *     <li>HE001079 - Called method is not authorized for use by this agency</li>
 *   </ul>
 * </p>
 * @see ValidationErrorCode for more detailed information about what caused validation failure.
 * @author Yevgen Golubenko
 * @since 2/17/19
 */
public enum ResponseCode
{
  NONE("NONE", -1000, "No response code"),
  COMMUNICATION_ERROR("COMMUNICATION_ERROR", -999, "Communication error with the HUB endpoint"),
  EXCEPTION("EXCEPTION", -998, "Exception occurred during HUB call, see message for details"),
  SUCCESS("HS000000", 0, "Successful"),
  VALIDATION_FAILED("HE003000", -1, "Validation Failed"),
  ALIEN_OR_I94_MISSING("HE001020", -2, "Alien Number or I-94 Number must exist"),
  ALIEN_AND_I94_TOGETHER("HE001021", -3, "Cannot have both Alien and I-94 Number."),
  DOCUMENT_EXPIRATION_IS_NULL("HE001161", -4, "Required Document Expiration Date is null"),
  HIRE_DATE_IS_NULL("HE001033", -5, "Required Hire Date is null"),
  CLOSE_STATUS_IS_NULL("HE001035", -6, "Required Close Status is null"),
  INVALID_DOCTYPE_FORMAT("HE001043", -7, "Invalid format for DocType"),
  INVALID_DOCDESCREQ_FORMAT("HE001044", -8, "Invalid format for DocDesqReq"),
  INVALID_BENEFIT_CODE_FOR_USER("HE001047", -9, "Invalid Benefit Code for user"),
  INVALID_DOC_EXP_DATE("HE001049", -10, "Invalid Document Expiration Date"),
  INVALID_USERFIELD_FORMAT("HE001052", -11, "Invalid format for User Field"),
  NO_PERMISSION_TO_ACCESS_THIS_CASE("HE001055", -12, "No permission to access this case"),
  DOC_OTHER_DESC_CANNOT_HAVE_VAL("HE001056", -13, "Doc Other Description cannot have a value for selected Document Type"),
  METHOD_NOT_AUTHORIZED("HE001079", -14, "Called method is not authorized for use by this agency"),
  CASE_DOESNT_EXIST_OR_NO_PRIVILEGE("HE001090", -15, "Case doesn't exist, or you have no correct privilege to access it"),
  REQUEST_SPONSOR_DATA_INDICATOR_MUST_BE_FALSE("HE001202", -16, "Request Sponsor Data Indicator must be false"),
  REQUEST_GRANT_DATE_INDICATOR_MUST_BE_FALSE("HE001203", -17, "Request Grant Date Data Indicator must be false"),
  INVALID_CARD_NUMBER_FORMAT("HE001197", -18, "Card number must be 3 alphabetic characters followed by 10 digits"),
  PASSPORT_NUMBER_IS_NULL("HE001100", -19, "Required Passport Number is null"),
  CASE_RESULT_NOT_AVAILABLE("HE010043", -20, "Additional (Step 2) or Third (Step 3) verification results for the requested case are not available at this time")
  ;

  private String name;
  private int code;
  private String message;

  /**
   * Constructs new {@code ResponseCode} with given code name/number.
   *
   * @param name name of this code.
   * @param code integer representation of this code.
   * @param message optional message text for this code.
   */
  ResponseCode(String name, int code, String message)
  {
    this.name = name;
    this.code = code;
    this.message = message;
  }

  /**
   * Returns a name, such as HEXXXXXX for invalid codes, or HS000000
   * for successful code.
   * @return name of the response code.
   */
  public String getName()
  {
    return name;
  }

  public void setName(final String name)
  {
    this.name = name;
  }

  /**
   * Returns numeric value for this response code, such as 0 for success,
   * or < 0 for various errors.
   * @return code for this response.
   */
  public int getCode()
  {
    return code;
  }

  public void setCode(final int code)
  {
    this.code = code;
  }

  /**
   * Returns a short message text associated with this reponse code.
   * @return short message text for this response.
   */
  public String getMessage()
  {
    return message;
  }

  public void setMessage(final String message)
  {
    this.message = message;
  }

  /**
   * Takes code name and returns {@link ResponseCode} enum.
   * <p>
   *   This used by deserializer, to parse code names such as HE003000, HS000000, etc.
   *   and turn them into appropriate enum.
   * </p>
   * @param name code name.
   * @return {@link ResponseCode}.
   */
  @JsonCreator
  public static ResponseCode fromJsonName(String name) {
    Optional<ResponseCode> responseCodeOptional = Arrays.stream(ResponseCode.values()).filter(rc -> rc.getName().equals(name)).findFirst();
    return responseCodeOptional.orElse(ResponseCode.NONE);

  }
}
