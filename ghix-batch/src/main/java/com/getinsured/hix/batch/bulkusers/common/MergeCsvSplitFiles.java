package com.getinsured.hix.batch.bulkusers.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MergeCsvSplitFiles {

	private static final Logger logger = LoggerFactory.getLogger(MergeCsvSplitFiles.class);
	private String csvUserStatusHeader;
	private String consolidatedCsvFile;
	private String csvJobsOutputFileDirectory;
	private String csvJobsOutputFileNameExpr;

	public void mergeFiles(String jobProcessId) throws IOException {

		String thisLine = "";
		
		ArrayList<String> files = CommonUtil.getListOfFileNames(csvJobsOutputFileDirectory, csvJobsOutputFileNameExpr+ "*.csv");
		logger.info("Started Merging CSV Jobs Output Files. jobProcessId : "+jobProcessId+"  csvUserStatusHeader : "+csvUserStatusHeader);

		String consolidatedFileName = consolidatedCsvFile + CommonUtil.dateToString() + ".csv";
		
		if (files != null && files.size() > 0) {
			logger.info("Final Consolidated CSV File Name  : "+consolidatedFileName +"     jobProcessId : "+jobProcessId);
			BufferedWriter consolidatedCsvWriter = new BufferedWriter(new FileWriter(new File(consolidatedFileName)));
			BufferedReader readerHugeData;
			consolidatedCsvWriter.write(csvUserStatusHeader + "\n");
			
			logger.info("Total no. of CSV jobs files are " + files.size()+"    jobProcessId : "+jobProcessId);
			for (int i = 0; i < files.size(); i++) {
				readerHugeData = new BufferedReader(
						new InputStreamReader(new FileInputStream(files.get(i))));
				int count = 0;
				logger.info("Reading data from CSV jobs output files and consolidated filename is " + files.get(i)+" jobProcessId : "+jobProcessId);
				 
				while ((thisLine = readerHugeData.readLine()) != null) {
					if (count != 0) {
						consolidatedCsvWriter.write(thisLine + "\n");
					}
					count++;
				}
				consolidatedCsvWriter.flush();
				count = 0;
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				readerHugeData.close();
			}
			consolidatedCsvWriter.close();
			logger.info("Completed merging CSV jobs output files.  jobProcessId : "+jobProcessId);
			
		} else {
			logger.info("No files are found to merge csv jobs output files.  jobProcessId : "+jobProcessId);
		}
		
		
	}

	public String getCsvUserStatusHeader() {
		return csvUserStatusHeader;
	}

	public void setCsvUserStatusHeader(String csvUserStatusHeader) {
		this.csvUserStatusHeader = csvUserStatusHeader;
	}

	public String getConsolidatedCsvFile() {
		return consolidatedCsvFile;
	}

	public void setConsolidatedCsvFile(String consolidatedCsvFile) {
		this.consolidatedCsvFile = consolidatedCsvFile;
	}

	public String getCsvJobsOutputFileDirectory() {
		return csvJobsOutputFileDirectory;
	}

	public void setCsvJobsOutputFileDirectory(String csvJobsOutputFileDirectory) {
		this.csvJobsOutputFileDirectory = csvJobsOutputFileDirectory;
	}

	public String getCsvJobsOutputFileNameExpr() {
		return csvJobsOutputFileNameExpr;
	}

	public void setCsvJobsOutputFileNameExpr(String csvJobsOutputFileNameExpr) {
		this.csvJobsOutputFileNameExpr = csvJobsOutputFileNameExpr;
	}

}
