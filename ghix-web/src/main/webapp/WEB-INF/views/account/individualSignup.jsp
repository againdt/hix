<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="gutter10">
	<div class="row-fluid">
		<h1>
			Shop For Health Insurance 
		</h1>
	</div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
			<h4 class="margin0">Did You Know?</h4>
			</div>
			<div class="gutter10">
				<p>The Federal insurance
				Marketplace will ask you identify
				proofing questions and then ask
				your detailed questions to
				determine your subsidy eligibility.</p>
				<%-- <h4 class="margin0">Steps</h4>
				<ol class="nav nav-list">
					<li class="active"><a href="#">Basic information</a></li>
					<c:if test="${IS_SECURITY_QUESTIONS_REQUIRED == 'TRUE'}">
					<li><a href="#">Security challenges</a></li>
					</c:if>
				</ol> --%>
			</div>
		</div>
		<!-- end of span3 -->


		<!-- start of span9 --->
		<div class="span9" id="rightpanel">
			<form class="form-horizontal" id="frmuserreg" name="frmuserreg"
				action="<c:url value='/account/signup'/>" method="POST">
				<!-- Hard coding Firstname and lastname for individual signup -->
				<!-- <input type="hidden" name="firstName" id = "firstName" value="default" />
				<input type="hidden" name="lastName" id = "lastName" value="default" /> -->
				<div class="header">
					<h4 class="margin0">Account Creation</h4>
				</div>
				<div class="gutter10">
					<p>Let's create an account real quick before we send you off to get your subsidy determination on the
					Federal Marketplace. This will be prefilled there and will also be used so who you are when you come
					back to select plans with us!</p>
				
					
					<div class="control-group">
						<label for="firstName" class="required control-label"><spring:message
								code="label.name" /> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<!-- end of label -->
						<div class="controls">
							<input type="text" name="firstName" id="firstName" value=""
								class="input-xlarge margin10-b" size="20" onkeypress="resetErrorMsg(this)" placeholder="First Name"/>
							<div id="firstName_error" class="help-inline"></div>
							<input type="text" name="lastName" id="lastName" value=""
								class="input-xlarge" size="20" onkeypress="resetErrorMsg(this)" placeholder="Last Name"/>
							<div id="lastName_error" class="help-inline"></div>
						</div>
						<!-- end of controls-->
					</div>
					<!-- end of control-group -->

					<%-- <div class="control-group">
						<label for="lastName" class="required control-label"><spring:message
								code="label.userLastName" /><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<!-- end of label -->
						<div class="controls"> --%>
							<!-- <input type="text" name="lastName" id="lastName" value=""
								class="input-xlarge" size="30" onkeypress="resetErrorMsg(this)" placeholder="Last Name"/>
							<div id="lastName_error" class="help-inline"></div> -->
						<!-- </div>
						end of controls
					</div> -->
					
					 <div class="control-group">
						<label for="phone" class="required control-label"><spring:message
								code="label.phone" /><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<%--<div class="controls">
							<input type="text" maxlength="10" name="phone" id="phone"
								value="" class="input-xlarge" size="30" onkeypress="resetErrorMsg(this)"/>
							<div id="phone_error" class="help-inline"></div>
						</div>--%>
						<!-- end of controls-->
					 
							<div class="controls">								
								<input type="text" class="span2 inline input-mini" name="phone1" id="phone1" maxlength="3" />
								
								<input type="text" class="span2 inline input-mini" name="phone2" id="phone2" maxlength="3" />
								
								<input type="text" class="span2 inline input-mini" name="phone3" id="phone3" maxlength="4"/> 
								<span id="phone3_error"></span>
						<div id="phone_error" class="help-inline"></div>
					</div>
					</div>
					<div class="control-group">
						<label for="email" class="required control-label"><spring:message
								code="label.userEmail" /><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<!-- end of label -->
						<div class="controls">
							<!-- <input type="email" name="email" id="email" value="" class="input-xlarge" size="30" onblur="checkEmail()" />-->
							<input type="email" name="email" id="email" value="" class="input-xlarge" size="30"/>							
							<div id="email_error" class="help-inline"></div>
						</div>
						<!-- end of controls-->
					</div>
					
					
					<!-- end of control-group -->
					<div class="control-group">
						<h4>Set Password</h4>
					</div>
					<div class="control-group">
						<label for="password" class="required control-label"><spring:message
								code="label.password" /><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="password" name="password" id="password" value=""
								class="input-xlarge" size="30" onkeypress="resetErrorMsg(this)"/>
							<div id="password_error" class=""></div>
						</div>
						<!-- end of controls-->
					</div>
					<!-- end of control-group -->

					<div class="control-group">
						<label for="confirmPassword" class="required control-label"><spring:message
								code="label.userConfirmPassword" /> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="password" name="confirmPassword"
								id="confirmPassword" value="" class="input-xlarge" size="30" onkeypress="resetErrorMsg(this)"/>
							<div id="confirmPassword_error" class=""></div>

						</div>
					</div>
					
					<input type="hidden" name="userNameCheck" id="userNameCheck"
						value="" /> <input type="hidden"
						name="defUserRoleName" id="defUserRoleName"
						value="${defUserRoleName}" />
						
					<input type="hidden" name="activationId" id="activationId" value="${activationId}" />
				</div>
				
				<div class="form-actions">
					<input type="button" name="submitbtn" id="submitbtn" onClick="javascript:validateForm();" value="<spring:message  code='label.signupBtn'/>" title="<spring:message  code='label.signupBtn'/>"
						class="btn btn-primary" />
				</div>
			</form>
		</div> <!-- end of span9 -->
		
	</div> <!-- end of row-fluid -->
</div> <!-- end of gutter10-->

<script type="text/javascript">

function resetErrorMsg(element) {
	var elementId = element.id;
	
	$("#" + elementId + "_error").html('');
}
jQuery.validator.addMethod("passwordcheck", function(value,
		element, param) {
	password = $("#password").val();
	confirmed = $("#confirmPassword").val();

	if (password != confirmed) {
		return false;
	}
	return true;
});

jQuery.validator.addMethod("validatepassword", function(value,
		element, param) {
	password = $("#password").val();

	//var re = /^[A-Za-z0-9!@#$%^&*()_]{8,20}$/;
	//var re = /(?=^.{8,20}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=^.*[^\s].*$).*$/; 
	//var re = /(?=^.{8,20}$)(?=.*[a-z])(?=.*[0-9])(?=^.*[^\s].*$).*$/; 
	var re = /(?=^.{8,20}$)(?=.*[a-zA-Z])(?=.*[0-9])(?!.*\s).*$/; 

	if (!re.test(password)) {
		return false;
	}
	return true;

});

var validator = $("#frmuserreg")
.validate(
		{
			rules : {
				firstName : {
					required : true
				},
				lastName : {
					required : true
				},
				email : {
					required : true
					},
				userName : {
					required : true,
					userNameCheck : true
				},
				password : { 
					required : true,
					validatepassword:true 
				},
				confirmPassword : { 
					required : true, 
					passwordcheck : true
				},
				phone : {
					required : true,
					number : true,
					minlength : 10
				}
			},
			messages : {
				firstName : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstName' javaScriptEscape='true'/></span>"
				},
				lastName : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateLastName' javaScriptEscape='true'/></span>"
				},
				email : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateEmail' javaScriptEscape='true'/></span>"
					},
				password: { 
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatePassword' javaScriptEscape='true'/></span>",
					validatepassword : "<span> <em class='excl'>!</em><spring:message code='label.validatePasswordLength' javaScriptEscape='true'/></span>"										
				},
				confirmPassword: { 
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatePasswordConfirm' javaScriptEscape='true'/></span>",
					passwordcheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePasswordMatch' javaScriptEscape='true'/></span>"
				},
				phone: {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNo' javaScriptEscape='true'/></span>",
					number : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNo' javaScriptEscape='true'/></span>",
					minlength : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNo' javaScriptEscape='true'/></span>"
				}
			},
			onkeyup : false,
			errorClass : "error",
			errorPlacement : function(error, element) {
				var elementId = element.attr('id');
				error
						.appendTo($("#" + elementId
								+ "_error"));
				$("#" + elementId + "_error").attr('class',
						'error help-inline');
			},
			
		});
function validateForm()
{
	checkExistingEmail();
}

function isInvalidCSRFToken(xhr) {
    var rv = false;
    if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {                   
    	alert($('Session is invalid').text());
           rv = true;
    }
    return rv;
}

function checkExistingEmail()
{
	//var validateUrl = 'signup/checkEmail';
	var validateUrl = "<c:url value='/account/signup/checkEmail'/>";
	var csrftoken = '${sessionScope.csrftoken}';
	$.ajax({
		url : validateUrl,
		type : "POST",
		data : {
			email : $("#email").val(),
			csrftoken : csrftoken
		},
		success: function(response)
		{
			if(isInvalidCSRFToken(response)){
				return false; 
			}  
			
			if(!response)
			{
				error = "<label class='error' generated='true'><span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.validateEmailCheck' javaScriptEscape='true'/></span></label>";
				$('#email_error').html(error);
				return false;
			}
			else
			{
				$("#frmuserreg").submit();
			}
		},
		
	});
}
</script>
