<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js"></script>
<div class="gutter10">
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
			<ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/manageissuer"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
        	<li><spring:message  code="pgheader.issuerDetails"/></li>
        </ul><!--page-breadcrumb ends-->
	</div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
			</div>
			<div class="span9">
				<h1>${issuerObj.name}</h1>
			</div>
		</div>
	</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.issuerAbout"/></h4>
                </div>
                
                <jsp:include page="issuerDetailsLeftNav.jsp">
        				<jsp:param name="pageName" value="paymentInfo"/>
				</jsp:include>
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="pull-left"><spring:message  code="pgheader.issuerInfo"/></h4>
                    <a class="btn btn-primary btn-small pull-right margin5-lr" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
                    <a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/issuerPaymentInfo/${encIssuerId}"/>"><spring:message  code="label.cancel"/></a> 
                </div>
				<div class="gutter10">
				<form class="form-horizontal" id="frmEditIssuerPaymentDetails" name="frmEditIssuerPaymentDetails" action="<c:url value="/admin/issuer/details/saveIssuerPayment/${encIssuerId}"/>" method="POST">
					<df:csrfToken/>
					<%-- <input type="hidden" id="id" name="id" value="<encryptor:enc value="${issuerPaymentInfo.id}"></encryptor:enc>"/> --%>
					<div class="profile">
						<table class="table table-border-none">	
							<tbody>						
								<tr>
									<td class="txt-right span4"><label for="securityCertName"><spring:message  code="label.securityCertName"/></label></td>
									<td>
										<input type="text" name="securityCertName" id="securityCertName" class="input-large" value="${issuerPaymentInfo.securityCertName}" maxlength="1020"/>
	                            		<div id="securityCertName" class=""></div>
									</td>
								</tr>	
					
								<tr>
									<td class="txt-right span4"><label for="keyStoreFileLocation"><spring:message  code="label.keyStoreFileLocation"/></label></td>
									<td>
										<input type="text" name="keyStoreFileLocation" id="keyStoreFileLocation" class="input-large" value="${issuerPaymentInfo.keyStoreFileLocation}" maxlength="1020"/>
	                            		<div id="keyStoreFileLocation" class=""></div>
									</td>
								</tr>		
								 <tr>
									<td class="txt-right span4"><label for="privateKeyName"><spring:message  code="label.privateKeyName"/></label></td>
									<td>
										<input type="text" name="privateKeyName" id="privateKeyName" class="input-large" value="${issuerPaymentInfo.privateKeyName}" maxlength="1020"/>
	                            		<div id="privateKeyName" class=""></div>
									</td>
								</tr>	
								<tr>
									<td class="txt-right span4"><label for="password"><spring:message  code="label.password"/></label></td>
									<td>
										<input type="password" name="password" id="password" class="input-large" value="${issuerPaymentInfo.password}" maxlength="1020" />
	                            		<div id="password" class=""></div>
									</td>
								</tr>	
								<tr>
									<td class="txt-right span4"><label for="passwordSecuredKey"><spring:message  code="label.passwordSecuredKey"/></label></td>
									<td>
										<input type="text" name="passwordSecuredKey" id="passwordSecuredKey" class="input-large" value="${issuerPaymentInfo.passwordSecuredKey}" maxlength="1020"/>
	                            		<div id="passwordSecuredKey" class=""></div>
									</td>
								</tr>	
										
								<tr>
									<td class="txt-right"><label for="securityDnsName"><spring:message  code="label.securityDnsName"/></label></td>
									<td>
										<input type="text" name="securityDnsName" id="securityDnsName" class="input-large" value="${issuerPaymentInfo.securityDnsName}" maxlength="1020"/>
	                            		<div id="securityDnsName" class=""></div>
									</td>
								</tr>
								<tr>
									<td class="txt-right"><label for="securityAddress"><spring:message  code="label.securityAddress"/></label></td>
									<td>
										<input type="text" name="securityAddress" id="securityAddress" class="input-large" value="${issuerPaymentInfo.securityAddress}" maxlength="1020"/>
	                            		<div id="securityAddress" class=""></div>
									</td>
								</tr>
								
								<tr>
									<td class="txt-right"><label for="securityKeyInfo"><spring:message  code="label.securityKeyInfo"/></label></td>
									<td>
										<input type="text" name="securityKeyInfo" id="securityKeyInfo" class="input-large" value="${issuerPaymentInfo.securityKeyInfo}" maxlength="1020"/>
	                            		<div id="securityKeyInfo" class=""></div>
									</td>
								</tr>
								<tr>
									<td class="txt-right"><label for="issuerAuthURL"><spring:message  code="label.issuerAuthURL"/></label></td>
									<td>
										<input type="text" name="issuerAuthURL" id="issuerAuthURL" class="input-large" value="${issuerPaymentInfo.issuerAuthURL}" maxlength="1020"/>
	                            		<div id="issuerAuthURL" class=""></div>
									</td>
								</tr>
								
                       	</table>
					</div>			
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
		</div>

<script type="text/javascript">var phixFlag = '${exchangeType}';

var flagValue = true;
if(phixFlag == 'PHIX'){	
	flagValue = false;
}else {	
	flagValue = true;
}

var error = '${erorr}';

if(error == 'true'){
	$('#frmEditIssuerPaymentDetails').html("Error Occured");
}
function submitForm(){
	 $("#frmEditIssuerPaymentDetails").submit(); 
} 
var validator = $("#frmEditIssuerPaymentDetails").validate({ 
rules : {
	keyStoreFileLocation : { maxlength : 1020},
	privateKeyName : { maxlength : 1020}	,
	password : { maxlength : 1020}	,
	passwordSecuredKey : { maxlength : 1020}	,
	securityDnsName : { maxlength : 1020}	,
	securityAddress : { maxlength : 1020}	,
	securityKeyInfo : { maxlength : 1020}	,
	issuerAuthURL : { maxlength : 1020}	
	
},
messages : {
	
	keyStoreFileLocation: { 
			maxlength : "<span><em class='excl'>!</em><spring:message  code='err.validIssuerPaymentInfo' javaScriptEscape='true'/></span>"
	},
	privateKeyName: { 
			maxlength : "<span><em class='excl'>!</em><spring:message  code='err.validIssuerPaymentInfo' javaScriptEscape='true'/></span>"
	},	
	password: { 
			maxlength : "<span><em class='excl'>!</em><spring:message  code='err.validIssuerPaymentInfo' javaScriptEscape='true'/></span>"
	},	
	passwordSecuredKey: { 
			maxlength : "<span><em class='excl'>!</em><spring:message  code='err.validIssuerPaymentInfo' javaScriptEscape='true'/></span>"
	},	
	securityDnsName: { 
			maxlength : "<span><em class='excl'>!</em><spring:message  code='err.validIssuerPaymentInfo' javaScriptEscape='true'/></span>"
	},	
	securityAddress: { 
			maxlength : "<span><em class='excl'>!</em><spring:message  code='err.validIssuerPaymentInfo' javaScriptEscape='true'/></span>"
	},	
	securityKeyInfo: { 
			maxlength : "<span><em class='excl'>!</em><spring:message  code='err.validIssuerPaymentInfo' javaScriptEscape='true'/></span>"
	},	
	issuerAuthURL: { 
			maxlength : "<span><em class='excl'>!</em><spring:message  code='err.validIssuerPaymentInfo' javaScriptEscape='true'/></span>"
	}	
		
},
errorClass: "error",
errorPlacement: function(error, element) {
	var elementId = element.attr('id');
	error.appendTo( $("#" + elementId + "_error"));
	$("#" + elementId + "_error").attr('class','error');
} 
});

</script>		
	
		