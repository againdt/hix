<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>

<link href="resources/css/slideshow-css.css" rel="stylesheet"  type="text/css" />
<!--  <script type="text/javascript" src="resources/js/slideshow.js"></script>-->
<style>
body {
padding-top:25px;
}

.clear-left {
clear:left;
}

.img-polaroid {
margin:10px;
}

figcaption {
font-size:80%;
font-weight:bold;
text-align:center;
}

figcaption span {
display:block;
line-height:10px;
}

#navhelp .navbar-inner {
margin-top:15px;
}

h3 {
font-size: em
}

#menu li {
    line-height: 32px;
}

.container #menu ul li.menuItem {
	padding:10px 0;
	text-transform: uppercase;
}
.container #menu ul {
	background: #f7f7f7;
	border:1px solid #e8e8e8;
	background-image: none;
}
.container #menu ul li a {
	font-weight: normal;
}
.container #menu ul li.act a {
	background: #4c7a9d;
	padding:10px 20px;
	color: white;
	border-radius: 5px;
	border:1px solid #4b7b9a;
}

</style>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="ecm" uri="/WEB-INF/tld/ecm.tld"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<div class="row-fluid" role="main">

<%
	String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
%>
<c:set var="stateExchangeType" value="<%=stateExchangeType%>" />




			<div id="gallery">
			<c:if test="${'both'.equalsIgnoreCase(stateExchangeType) || 'individual'.equalsIgnoreCase(stateExchangeType)}">
				<div class="hero-unit">
				<c:set var="individualRoleName" ><encryptor:enc value="individual" isurl="true"/> </c:set>
						<h2>Individuals &amp; Families</h2>
						<p><spring:message code="label.individualsCopy"/></p>
						<div>
							<a class="btn btn-primary large" href="<c:url value="/preeligibility" />"><spring:message code="label.shop"/></a>
							<a class="btn btn-primary large" href="<c:url value="/account/signup/${individualRoleName}" />"><spring:message code="label.firsttimehereInvidual"/></a>
						</div>
						<img src="<c:url value="/resources/images/family.png" />" width="" height="" alt="Individuals and Families" title="Individuals and Families"  />
				</div>
				</c:if>
				
				<c:if test="${'both'.equalsIgnoreCase(stateExchangeType) || 'shop'.equalsIgnoreCase(stateExchangeType)}">
				<div id="slides">
					<div class="slide">
					
					
						<div id="menu">
							<ul>
								<li class="menuItem"><a href=""><spring:message code="label.employers"/></a></li>
							</ul>
						</div>
						<c:set var="employerRoleName" ><encryptor:enc value="employer" isurl="true"/> </c:set>
						<img src="<c:url value="/resources/images/NMHIX-shop-img_employers.png" />" width="" height="" alt="Employers" title="Employers"  />
						<span class="left">
							<h2><spring:message code="label.employerHeader"/></h2>
							<p><spring:message code="label.employerText"/></p>
							<p><a class="btn btn-primary large" href="<c:url value="/account/signup/${employerRoleName}" />"><spring:message code="label.firsttimehereEmployer"/></a></p>
						</span>
					
						
					</div>
					
					<div class="slide">
						<div id="menu">
							<ul>
								<li class="menuItem"><a href=""><spring:message code="label.employees"/></a></li>
							</ul>
						</div>
						<img src="<c:url value="/resources/images/NMHIX-shop-img_employees.jpg" />" alt="Employee" title="Employee" />
						<span class="left">
							<h2><spring:message code="label.employeeHeader"/></h2>							
							<p><spring:message code="label.employeeText"/></p>
                            <p><a class="btn btn-primary large" href="<c:url value="/account/user/login" />"><spring:message code="label.signin"/></a>
						</span>
					</div>
					
					<div class="slide">
						<div id="menu">
							<ul>
								<li class="menuItem"><a href=""><spring:message code="label.broker"/></a></li>
								<!-- <li class="menuItem"><a href="">Doctors &amp; Hospitals</a></li> -->
								<%-- <li class="menuItem last"><a  href=""><spring:message code="label.insurers"/></a></li>--%>
							</ul>
						</div>
						<img src="<c:url value="/resources/images/NMHIX-shop-img_agents.png" />" alt="Agents" title="Agents" />
						<span class="left">
							<c:set var="brokerRoleName" ><encryptor:enc value="broker" isurl="true"/> </c:set>
							<h2><spring:message code="label.brokerHeader"/></h2>							
							<p id="agentsText"><spring:message code="label.brokerText"/></p>
							<p><a class="btn btn-primary large" href="<c:url value="/account/signup/${brokerRoleName}" />"><spring:message code="label.firsttimehereAgent"/></a></p>
<%-- 							<p class="small"><spring:message code="label.alreadymember"/> <a href="<c:url value="account/user/login" />"><spring:message code="label.signin"/></a></p> --%>
						</span>
				 
						
					</div>
					
				</div>
				</c:if>	
					<%-- <div class="slide">
					<img src="<c:url value="/resources/images/Eli-insurers-L.jpg" />" width="465" height="274" alt="A couple filling out a form" title="A couple filling out a form"  /> 
						<span class="left">
							<h2><spring:message code="label.insurers"/></h2>
							<p><spring:message code="label.insurerstext"/> </p>
							<p><a class="btn btn-primary large" href="<c:url value="/planmgmt/carrier/signup" />">First time here? Sign Up</a> 
							<p class="small"><spring:message code="label.alreadymember"/> <a href="<c:url value="account/user/login" />"><spring:message code="label.signin"/></a></p>
						</span>			   	
					</div>
					 --%>
				</div>
				
	 	 	</div>
	
		<!-- Example row of columns -->
		

			
<!-- 		<button type="button" data-toggle="modal" data-target="#myModal">Launch modal</button> -->
	    <div id="myModal" class="modal hide fade">
		    <div class="modal-header">
		   		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		    	<h3>&nbsp;</h3>
		    </div>
		    <div class="modal-body">
		    	<h3 class="alert alert-info"><spring:message code="label.featureNotAvailable"/></h3>
		    </div>
		    <div class="modal-footer">
			    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.close"/></a>
		    </div>
	    </div>		

	
<script type="text/javascript">
		//$('#myModal').modal("hide")
</script>
