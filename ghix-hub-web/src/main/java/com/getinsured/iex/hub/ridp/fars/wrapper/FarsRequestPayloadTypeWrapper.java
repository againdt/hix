package com.getinsured.iex.hub.ridp.fars.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.extension._1.ObjectFactory;
import com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.extension._1.RequestPayloadType;


/**
 * This is the wrapper class for RequestPayloadType
 * 
 * @author Nikhil Talreja
 * @since 20-Mar-2014
 *
 */
public class FarsRequestPayloadTypeWrapper implements JSONAware{
	
	
	private RequestPayloadType request;
	
	private String dshReferenceNumber;
	
	public static final String DHS_REF_NUM_KEY = "DSHReferenceNumber";
	
	public FarsRequestPayloadTypeWrapper(){
		ObjectFactory factory = new ObjectFactory();
		this.request = factory.createRequestPayloadType();
	}
	
	public String getDshReferenceNumber() {
		return dshReferenceNumber;
	}

	public void setDshReferenceNumber(String dshReferenceNumber) {
		this.dshReferenceNumber = dshReferenceNumber;
		this.request.setDSHReferenceNumber(dshReferenceNumber);
	}

	public RequestPayloadType getRequest() throws HubServiceException{
		
		if(this.request.getDSHReferenceNumber() == null){
			throw new HubServiceException("DHS Reference Number is mandatory for FARS Service");
		}
		
		return request;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(DHS_REF_NUM_KEY, this.dshReferenceNumber);
		return obj.toJSONString();
	}

}
