

import { instructionsActionTypes } from '../constants/actionTypes';

const initialState = {};

export default (state = initialState, action) => {
    switch(action.type) {
    case instructionsActionTypes.SET_INSTRUCTIONS:
        return action.instructions;
    case instructionsActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};