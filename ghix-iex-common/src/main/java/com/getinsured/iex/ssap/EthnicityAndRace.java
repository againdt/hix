
package com.getinsured.iex.ssap;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class EthnicityAndRace {

	@Expose
    private List<Ethnicity> ethnicity = new ArrayList<Ethnicity>();
	@Expose
	private List<Race> race = new ArrayList<Race>();
	@Expose
    private Boolean hispanicLatinoSpanishOriginIndicator;
   
   
    public Boolean getHispanicLatinoSpanishOriginIndicator() {
        return hispanicLatinoSpanishOriginIndicator;
    }

    public List<Ethnicity> getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(List<Ethnicity> ethnicity) {
		this.ethnicity = ethnicity;
	}

	public List<Race> getRace() {
		return race;
	}

	public void setRace(List<Race> race) {
		this.race = race;
	}

	public void setHispanicLatinoSpanishOriginIndicator(Boolean hispanicLatinoSpanishOriginIndicator) {
        this.hispanicLatinoSpanishOriginIndicator = hispanicLatinoSpanishOriginIndicator;
    }

	   
}
