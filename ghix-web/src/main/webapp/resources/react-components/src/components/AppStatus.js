import React from 'react';
import ReactTooltip from 'react-tooltip';
import { FormattedMessage } from 'react-intl';

function AppStatus({ data, onGetApplicationResults }) {

    const { applicants, applicationYear, status, link } = data;
    let tRef = null;

    const onKeyUpEvent = (e, tooltipRef) => {
        // check for Esc key
        if (e.keyCode === 27) {
            hideTooltip(e, tooltipRef);
        }
    }

    const hideTooltip = (e, tooltipRef) => {
        ReactTooltip.hide(tooltipRef);
    }

    const showTooltip = (e, tooltipRef) => {
        ReactTooltip.show(tooltipRef);
    }

    const getMembers = () => {
        const tooltipContent = applicants && applicants.map((applicant => {
            return <div key={applicant.memberId}>
                {applicant.firstName} {applicant.middleName} {applicant.lastName} {applicant.nameSuffix}
            </div>
        }));

        return <div>
            <a data-tip
                ref = {ref => tRef = ref}
               data-for="application_status_member_count_tooltip"
               className="ds-u-font-size--small ds-text ds-u-measure--wide ds-u-color--muted ds-u-text-decoration--underline cursor--pointer"
               id="application_status_member_count"
            >
                <FormattedMessage id="application_status.for.members" values={{noofmembers: applicants ? applicants.length : 0}} />
            </a>
            <div tabIndex="0" onFocus={(e) => showTooltip(e, tRef)} onBlur={(e) => hideTooltip(e, tRef)} onKeyUp={(e) => onKeyUpEvent(e, tRef)}>
                <ReactTooltip
                    place="left"
                    type="dark"
                    effect="solid"
                    role="tooltip"
                    id="application_status_member_count_tooltip"
                    className="ds-u-text-transform--capitalize"
                >
                    <div>{tooltipContent}</div>
                </ReactTooltip>
            </div>
        </div>

    };

    return (
        <div id="auto-app-status">
            <div className="ds-u-margin--2">
                <div className="ds-l-row">
                    <h3 className="ds-h6 ds-u-font-weight--semibold" id="application_status_header">
                        <FormattedMessage id="application_status.header"/>
                    </h3>
                </div>
                <div className="ds-l-row ds-u-font-size--small ds-u-margin--1">
                    <div className="ds-l-xs-col--12 ds-l-sm-col--12 ds-l-md-col--5 ds-l-lg-col--5 ds-l-xl-col--5">
                        <div style={{ 'marginBottom': '0' }} className="ds-text"
                             id="application_status_year"
                        >
                            <FormattedMessage id="application_status.application_year" values={{applicationYear}} />
                        </div>
                        {applicants && applicants.length > 0 && getMembers()}
                    </div>
                    <div className="ds-l-xs-col--12 ds-l-sm-col--12 ds-l-md-col--4 ds-l-lg-col--4 ds-l-xl-col--4">
                        <p className="ds-u-text ds-u-measure--wide" id="application_status_value">
                            <FormattedMessage id={status}/>
                        </p>
                    </div>
                    <div className="ds-l-xs-col--12 ds-l-sm-col--12 ds-l-md-col--3 ds-l-lg-col--3 ds-l-xl-col--3">
                        <div className="ds-u-text-decoration--underline">
                            <a href="javascript:void(0)"
                               id="application_status_link"
                               onClick={() => {onGetApplicationResults(data)}}
                            >
                                <FormattedMessage id={link}/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AppStatus;