<script type="text/ng-template" id="income-capitalgains.jsp">
  <div class="gutter10 jobIncome" ng-controller="incomeCapGains" >
    <h3><spring:message code="label.lce.capitalgains"/></h3>
    <form class="form-horizontal" id="captitalGainsIncome" name="captitalGainsIncome" novalidate>
      <spring:message code="label.lce.howmuchdoes"/> {{householdMember.name.firstName}} {{householdMember.name.lastName}} <spring:message code="label.lce.expecttogetfromcapitalgain"/>
      <div class="control-group">
        <label class="control-label" for="typeOfRequest"></label>
          <div class="controls margin10-t">
            <div class="input-prepend">
              <span class="add-on">$</span>
              <input type="text" class="span4 margin10-r" name="capitalGainsIncomeAmount" ng-model="householdMember.detailedIncome.capitalGains.annualNetCapitalGains" required numbers-Only ng-trim />
              <select class="span5"  name="capitalGainsIncomeFrequency" ng-model="householdMember.detailedIncome.capitalGains.incomeFrequency" required>
                <option><spring:message code="label.lce.weekly"/>Weekly</option>
			  	      <option><spring:message code="label.lce.biweekly"/>Bi-Weekly</option>
				        <option><spring:message code="label.lce.monthly"/>Monthly</option>
				        <option><spring:message code="label.lce.bimonthly"/>Bi-Monthly</option>
				        <option><spring:message code="label.lce.yearly"/>Yearly</option>
				        <option><spring:message code="label.lce.onceonly"/>Once Only</option>
              </select>
            </div>
          </div>
         <div class="controls margin5-t">
           <span class="validation-error-text-container" ng-show="submitted && captitalGainsIncome.capitalGainsIncomeAmount.$error.required"><spring:message code="label.incomecapitalgainserror1"/></span>
        </div>
         <div class="controls margin5-t">
           <span class="validation-error-text-container" ng-show="submitted && captitalGainsIncome.capitalGainsIncomeFrequency.$error.required"><spring:message code="label.incomecapitalgainserror2"/></span>
        </div>
      </div>
    </form>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a ng-show="captitalGainsIncome.$valid" class="btn btn-primary" ng-click="next();"><spring:message code="label.lce.next"/></a>
      <a ng-show="!captitalGainsIncome.$valid" class="btn btn-primary" ng-click="submitted=true;"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>