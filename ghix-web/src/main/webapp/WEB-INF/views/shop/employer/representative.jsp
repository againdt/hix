<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<script type="text/javascript">
	function populateDate(zipCode){
		var subUrl = '<c:url value="representative">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';
		$.ajax({
		  url: subUrl,
		  type: "GET",
		  data: {op : 'GETZIPDATA', zipCode : zipCode},
		  success: function(data,xhr){
	        if(isInvalidCSRFToken(xhr))
	          return;
			  var arr = data.split(" # ");
			  $('#city').val(data);
		  }
		});
	}
	
	function shiftbox(element,nextelement){
		maxlength = parseInt(element.getAttribute('maxlength'));
		if(element.value.length == maxlength){
	   		nextelement = document.getElementById(nextelement);
	   		nextelement.focus();
		}
	}
	</script>

<div class="row">
	<div class="span4">
		<h2>Employer Representative</h2>
		<p>You may designate an Human Relations employee, or a licensed
			insurance broker, to continue the Exchange process on your behalf. To
			do so, you must provide the name and contact information for your
			authorized representative to the Exchange. You must also provide a
			copy of your statement of authorization on your company letterhead,
			signed by you along with your title.</p>
	</div>
	<div class="span12">
		<form class="form-stacked" id="frmcontact" name="frmcontact" action="representative" method="post">
			<df:csrfToken/>
			<fieldset>
				<legend>Statement of Authorization</legend>
				<h4>I do hereby designate</h4>
				<div class="clearfix">
					<dt id="representType-label">&#160;</dt>
					<ul class="inputs-list">
						<li>
						<label for="representType-Employee"><input type="radio" name="representType" id="representType-Employee" value="Employee"> Employee</label>
						<label for="representType-Navigator"><input type="radio" name="representType" id="representType-Navigator" value="Navigator"> Navigator/Broker</label></li>
					</ul>
				</div>
				<div class="clearfix">
					<label>Company Name: Devals Pvt Ltd</label>
				</div>
				<div class="clearfix">
					<label for="firstName" class="required">First Name of Representative</label>
					<div class="input">
						<input type="text" name="firstName" id="firstName" value="" class="xlarge" size="60">
					</div>
				</div>
				<div class="clearfix">
					<label for="lastName" class="required">Last Name of Representative</label>
					<div class="input">
						<input type="text" name="lastName" id="lastName" value="" class="xlarge" size="60">
					</div>
				</div>
				<div class="clearfix">
					<label for="title" class="required">Title or Designation 
						<a href="#" data-original-title="Title" data-content="What is your title or designation." rel="popover"><span class="label">?</span></a>
					</label>
					<div class="input">
						<input type="text" name="title" id="title" value="" class="xlarge">
					</div>
				</div>
				<div class="clearfix">
					<label for="email" class="required">Email Address 
						<a href="#" data-original-title="Email Address" data-content="This email address will be used for logging into your account." rel="popover"><span class="label">?</span></a>
					</label>
					<div class="input">
						<input type="text" name="email" id="email" value="" class="xlarge" size="30">
					</div>
				</div>
				<div class="clearfix">
					<label for="phone1" class="required">Primary Contact Number
						<a href="#" data-original-title="Primary Number" data-content="What is your primary phone number." rel="popover"> <span class="label">?</span></a>
					</label>
					<div class="input">
						<input type="text" name="phone1" id="phone1" value="" size="3" maxlength="3" class="micro" onKeyUp="shiftbox(this,&#39;phone2&#39;)"> 
						<input type="text" name="phone2" id="phone2" value="" size="3" maxlength="3" class="micro" onKeyUp="shiftbox(this,&#39;phone3&#39;)"> 
						<input type="text" name="phone3" id="phone3" value="" size="4" maxlength="4" class="micro">
					</div>
				</div>
				<div class="clearfix">
					<label for="address" class="required">Address 
						<a href="#" data-original-title="Company Address" data-content="Your exact company address as it appears on the EIN." rel="popover"><span class="label">?</span></a>
					</label>
					<div class="input">
						<input type="text" name="address" id="address" value="" class="xlarge" size="30">
					</div>
				</div>
				<div class="clearfix row">
					<div class="span3">
						<label for="city" class="required">City</label>
						<div class="input">
							<input type="text" name="city" id="city" value="St. Paul" class="medium" size="30">
						</div>
					</div>
					<div class="span4">
						<label for="zip" class="required">Zip Code</label>
						<div class="input">
							<input type="text" name="zip" id="zip" value="55155" class="mini" size="5" onBlur="populateDate(this.value)" maxlength="5">
						</div>
					</div>
				</div>
				<div class="clearfix">
					<h4>as my authorized representative and point of contact for
						purposes of conducting company transactions with the Minnesota
						Health Insurance Exchange.</h4>
				</div>
			</fieldset>

			<div class="actions">
				<label id="optionsCheckboxes">Your Electronic Signature</label>
				<div class="clearfix">
					<label class="required" for="legal_name">Please enter your
						full legal name below.</label>
					<div class="input">

						<input type="text" name="fullName" id="fullName" value="" class="xlarge" size="60">&nbsp; &nbsp; 
						<input type="submit" name="continue" id="continue" value="Continue" class="btn primary"  title="Continue">&nbsp; OR &nbsp;
						<button type="button" class="btn danger" id="noattest" name="noattest" onclick="window.location='/shop/employer_index/regemployees';">Skip this Step</button>
						<br />
					</div>
				</div>
				<div class="clearfix">
					<a href="#" class="btn">Save as PDF</a>
				</div>
			</div>
		</form>

	</div>

	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">
				<p>If the Employer chooses to authorize an employee or a
					Navigator/Broker, a new user account is being created for this
					representative. The Employer may remove any Authorized
					Representatives using the Account Administration page accessible
					from the Employer Dashboard.</p>
			</div>
		</div>
	</div>

</div>
