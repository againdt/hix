<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<!-- The following stylesheet link has already been moved to shell.jsp. 
	If you are working on this page try removing the stylesheet link.
	If the page works fine without it, just delete the entire line along with this comment. -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<%@ page isELIgnored="false"%>

<div class="gutter10">

	<div class="row-fluid">

		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p>
					<c:out value="${errorMsg}"></c:out>
				<p />
			</c:if>
			<br>
		</div>

		
			<h1>
				Employees &nbsp;<font size="4%"><small> <fmt:formatNumber
							type="number" value="${totalNoOfEmp}" /> total employees
						</small></font> 
			</h1>
	
	</div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
				<h4>
				<spring:message code="label.searchBy" />
				<a class="pull-right" href="../../crm/employee/searchemployee"><spring:message
						code="label.brkResetAll" /></a>
				</h4>
			</div>
			<form class="form-vertical gutter10" method="POST"
				action="<c:url value="/crm/employee/searchemployee" />"
				id="employeeSearch" name="employeeSearch"
				class="form-vertical gutter10 lightgray" novalidate="novalidate">
			              <df:csrfToken/>
				<!-- <input type="hidden" name="PAGE_VISIT_COUNT" id="PAGE_VISIT_COUNT" value="true" /> -->

				<div class="control-group name-group">
					<label for="lastName" class="required control-label"><spring:message
							code="label.employeeLastAndFirstName" /></label> <label for="firstName"
						class="required hidden"></label>
					<div class="controls">
						<input class="input-small" type="text" name="lastName"
							id="lastName" value="${searchCriteria.lastName}" maxlength="100"
							placeholder="last" /> <input class="input-small" type="text"
							name="firstName" id="firstName"
							value="${searchCriteria.firstName}" maxlength="100"
							placeholder="first" />
					</div>
				</div>

				<div class="control-group">
					<label class="control-label"><spring:message
							code="label.employerName" /><a href="#" rel="tooltip"
						data-placement="top"
						data-original-title="Please enter first 3 characters of Employer Name for auto complete option 
								"
						class="info"><i class="icon-question-sign customMargin"></i></a></label>

					<div class="controls">
						<input class="input-medium" type="text"
							placeholder="Employer Name" name="employerName" id="employerName"
							value="${searchCriteria.employerName}" maxlength="400"
							onkeypress="javascript:onEmpNameChangeEvent();">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label"><spring:message
							code="label.employeeAddress" /></label>
					<div class="controls">
						<input class="input-medium" type="text" placeholder="Address"
							name="employeeAddress" id="employeeAddress" maxlength="1020"
							value="${searchCriteria.employeeAddress}">
					</div>
				</div>

				<div class="control-group">
					<label for="zipCode" class="control-label required"><spring:message
							code='label.columnHeaderZip' /></label>
					<div class="controls">
						<input class="input-small" type="text" placeholder="zip"
							name="zipCode" id="zipCode" value="${searchCriteria.zipCode}"
							maxlength="5">
						<div id="zipCode_error"></div>
					</div>
				</div>
				<div class="control-group">
					<label for="contactNumber" class="control-label required"><spring:message
							code="label.phone" /></label>
					<div class="controls">
						<input class="input-medium" type="text" placeholder="Phone Number"
							name="contactNumber" id="contactNumber"
							value="${searchCriteria.contactNumber}" maxlength="10">
						<div id="contactNumber_error"></div>
					</div>
				</div>
				<div class="control-group">
					<label for="ssn" class="control-label required"><spring:message
							code="label.ssnLast4Digit" /></label>
					<div class="controls">
						<input class="input-medium" type="text" placeholder="SSN"
							name="ssn" id="ssn" value="${searchCriteria.ssn}" maxlength="4">
						<div id="ssn_error"></div>
					</div>
				</div>

				<div class="txt-center">
					<input type="submit" class="btn"
						value="<spring:message  code='label.go'/>"
						title="<spring:message  code='label.go'/>" id="filter-flow"
						name="filter-flow">
				</div>
			</form>
		</div>
		<div class="span9" id="rightpanel">
			<form class="form-horizontal" id="frmSearchEmployee"
				name="frmSearchEmployee"
				action="<c:url value="/crm/employee/searchemployee" />"
				method="POST">
              			<df:csrfToken/>

				<c:choose>
					<c:when test="${not empty onPageLoad}">
						<h4 class="alert alert-info">
							<spring:message code="label.filterSearchCriteria" />
						</h4>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${resultSize >= 1}">
								<c:choose>
									<c:when test="${fn:length(employeeList) > 0}">
										<table class="table table-striped">
											<thead>
												<tr class="graydrkbg ">
													<spring:message code='label.columnHeaderName'
														var="employeeName" />
													
													<th class="sortable" scope="col"><dl:sort
															title="${employeeName}" sortBy="name"
															sortOrder="${searchCriteria.sortOrder}"></dl:sort> <c:choose>
															<c:when
																test="${searchCriteria.sortOrder == 'ASC' && searchCriteria.sortBy == 'name'}">
																<img
																	src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
																	alt="State sort ascending" />
															</c:when>
															<c:when
																test="${searchCriteria.sortOrder == 'DESC' && searchCriteria.sortBy == 'name'}">
																<img
																	src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
																	alt="State sort descending" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose></th>

													<th><spring:message code='label.columnHeaderAddress' /></th>
													<th><spring:message code='label.columnHeaderZip' /></th>
													<th><spring:message code='label.columnHeaderPhone' /></th>
													<th><spring:message code='label.ssnLast4Digit' /></th>
												</tr>
											</thead>

											<c:forEach items="${employeeList}" var="employee"
												varStatus="vs">
												<tr>
													
													
													<td><a
														href="<c:url value="/crm/crmemployee/details/${employee.id}"/>">${employee.name}</a></td>
													<td>${employee.employeeDetailslocationaddress1}</td>
													<td>${employee.employeeDetailslocationzip}</td>
													<td>${employee.employeeDetailscontactNumber}</td>
													<td>
														<%-- ${employee.employeeDetailsssn} --%> <c:if
															test="${not empty employee.employeeDetailsssn}">
															<c:set var="myValue">${employee.employeeDetailsssn}</c:set>
															<c:set var="ssnLength">${fn:length(myValue)}</c:set>
													${fn:substring(employee.employeeDetailsssn, ssnLength-4, ssnLength)}
												</c:if>
													</td>
												</tr>
											</c:forEach>
										</table>
										<div class="pagination txt-center">
											<dl:paginate resultSize="${resultSize + 0}"
												pageSize="${pageSize + 0}" />
										</div>
									</c:when>
								</c:choose>
							</c:when>
							<c:otherwise>
								<h4 class="alert alert-info">
									<spring:message code='label.norecords' />
								</h4>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</form>
		</div>
	</div>

	<div class="row-fluid">
		<div class="notes" style="display: none">
			<div class="row">
				<div class="span">
					<p>The prototype showcases three scenarios (A, B and C)
						dependant on a particular Employer's eligibility to use the SHOP
						Exchange.</p>
				</div>
			</div>
		</div>

	</div>
	<!-- row-fluid -->
</div>

<script type="text/javascript">
	var validator = $("#employeeSearch")
			.validate(
					{
						rules : {
							'contactNumber' : {
								minlength : 10,
								number : true
							},
							'zipCode' : {
								minlength : 5,
								number : true
							},
							'ssn' : {
								number : true
							}
						},
						messages : {
							'contactNumber' : {
								minlength : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNoFilterLength' javaScriptEscape='true'/></span>",
								number : "<span> <em class='excl'>!</em><spring:message code='label.validateNumericFilterValue' javaScriptEscape='true'/></span>"
							},
							'zipCode' : {
								minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateZipFilterLength' javaScriptEscape='true'/></span>",
								number : "<span> <em class='excl'>!</em><spring:message code='label.validateNumericFilterValue' javaScriptEscape='true'/></span>"
							},
							'ssn' : {
								number : "<span> <em class='excl'>!</em><spring:message code='label.validateNumericFilterValue' javaScriptEscape='true'/></span>"
							}
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							if ($("#" + elementId + "_error").html() != null) {
								$("#" + elementId + "_error").html('');
							}
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error")
									.attr('class', 'error');
						}
					});
</script>

<script>
	/* $(document).ready(function() {
	 fetchEmployerList();
	 }); */
	var empFilter = "";
	var availableEmployer = new Array();
	$(function() {
		$('.datepick').each(function() {
			var ctx = "${pageContext.request.contextPath}";
			var imgpath = ctx + '/resources/images/calendar.gif';
			$(this).datepicker({
				showOn : "button",
				buttonImage : imgpath,
				buttonImageOnly : true
			});
		});
	});

	function fetchEmployerList() {
		$("#requester").empty();
		var validateUrl = '<c:url value="/crm/employer/fetchemployerlist"></c:url>';
				var employerName = $("#employerName").val();

		$.ajax({
			url : validateUrl,
			type : "POST",
			data : {
				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				empName : employerName
			},
			success : function(response, xhr) {
			    if(isInvalidCSRFToken(xhr))                                  
			      return;
				autoCompleteEmployer(response);
			},
			error : function(e) {
				alert("Failed to Add subtype");
			},
		});

	}

	function onEmpNameChangeEvent() {
		var txtvalue = $("#employerName").val();
		var len = txtvalue.length;

		if (len >= 1 && empFilter != txtvalue.substring(0, 1)) {
			empFilter = txtvalue;
			fetchEmployerList();
		}
		autoCompleteBox();
	}

	function autoCompleteEmployer(userListJSON) {
		userListJSON = JSON.parse(userListJSON);
		var index = 0;
		for ( var key in userListJSON) {
			availableEmployer[index] = {
				label : userListJSON[key],
				idx : key
			};
			index++;
		}
	}

	function autoCompleteBox() {
		$("#employerName").autocomplete({
			source : availableEmployer,
			change : function(event, ui) {
				if (ui.item == null)
					$("#employerName").val('');
			}
		});
	}
</script>

<script type="text/javascript">
	$('.info').tooltip();
</script>