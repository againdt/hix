package com.getinsured.hix.batch.ssap.renewal.util;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.stereotype.Component;

@Component("qhpReportPartitionerParams")
public class QhpReportPartitionerParams {

	// CopyOnWriteArrayList<Integer> enrollmentIdList = null;
	List<Integer> enrollmentIdList = null;

	public QhpReportPartitionerParams() {
		this.enrollmentIdList = new CopyOnWriteArrayList<Integer>();
	}

	public List<Integer> getEnrollmentIdList() {
		return enrollmentIdList;
	}

	public synchronized void clearEnrollmentIdList() {
		this.enrollmentIdList.clear();
	}

	public synchronized void addAllToEnrollmentIdList(List<Integer> enrollmentIdList) {
		this.enrollmentIdList.addAll(enrollmentIdList);
	}

	public synchronized void addToEnrollmentIdList(Integer enrollmentId) {
		this.enrollmentIdList.add(enrollmentId);
	}
}
