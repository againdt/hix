<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/displaytagstyles.css" />">

<style>
	.width70 {width: 70px!important;}
	.width75 {width: 75px!important;}
	.width150 {width: 150px!important;}
	.width160 {width: 160px!important;}
</style>

<div class="gutter10">
<form class="form-horizontal" id="frmEnrollmentLog" name="frmEnrollmentLog" action="enrollmentlogreport" method="POST">
	<df:csrfToken/>

		<div class="row-fluid">
			<h1>Monthly Enrollment Log</h1>
		</div>	

		<div class="row-fluid">
		<div class="span12" id="rightpanel">
			<div class="header margin5-b">
				<h4 class="pull-left">Refine Results</h4>	
			</div>
			<div class="row-fluid">
				<div class="span2 margin20-lr">
	             	<div class="control-group">
						<label class="control-label required" for="fromDate" style="text-align: left">Start Date </label>
						<div class="input-append date date-picker" id="date" data-date-format="mm-dd-yyyy"  data-date="" >
							<input class="input-small" type="text"  name="fromDate" id="fromDate" value="${searchParams.FROMDATE}" /> 
							<span class="add-on"><i class="icon-calendar"></i></span>
						</div>	
						<span id="fromDate_error"></span>
					 	<!-- <input title="MM/DD/YYYY" type = "text" class="datepick input-small" name="fromDate" id="mystartdate" value="${searchCriteria.fromDate}" onChange="setdate();"/>-->					<!-- 	<div id="mystartdate_error"></div> -->
					</div>
				</div>
				<div class="span2 margin20-lr">		
					<div class="control-group">
						<label class="control-label required" for="toDate" style="text-align: left">End Date </label>
						<div class="input-append date date-picker" id="date" data-date-format="mm-dd-yyyy"  >
							<input class="input-small" type="text" name="toDate" id="toDate" value="${searchParams.TODATE}" /> 
							<span class="add-on"><i class="icon-calendar"></i></span>
						</div>
						<span id="toDate_error"></span>
					</div>
	           	</div>
				<div class="span2 margin20-lr">		
					<div class="control-group">
						<label class="control-label required" for="employeeName" style="text-align: left">Employee Name </label>
						<input class="input-small" type="text"  name="employeeName" id="employeeName" value="${searchParams.EMPLOYEENAME}" />
					</div>
	           	</div> 
	           	<div class="txt-center margin20-t">		
					<div class="control-group">
						<input type="submit" name="submitBtn" id="submitBtn" Value="Search" title="Search" class="btn btn-primary">
					</div>
	           	</div>    
	        </div>    
	 
            <c:if test="${not empty logReport}">	
	             <div id="logPanel" style="position:relative; margin: 10px 2px 20px 0;">
					<display:table name="logReport" pagesize="${pageSize}" list="logReport" requestURI="" sort="list" class="displaytagtable table">
			           
			           <display:column property="eventDate" titleKey="label.onlydate"  format="{0,date,MMM dd, yyyy}" sortable="true" class="width75"  />
			           <display:column property="employeeName" titleKey="label.employeename" sortable="true" class="width160"/>
			           <display:column property="enrolleeName" titleKey="label.enrollee" sortable="true" class="width160" />
			           <display:column property="eventType" titleKey="label.eventtype" sortable="true" class="width160" />
			           <display:column property="eventReason" titleKey="label.eventreason" sortable="true" class="width150" />
			           <display:column property="coverageStartDate" titleKey="label.coveragestartdate" sortable="true" class="width70" />
			           <display:column property="coverageEndDate" titleKey="label.coverageenddate" sortable="true" class="width70" />
			           <display:setProperty name="paging.banner.placement"  value="bottom" />
			           <display:setProperty name="paging.banner.some_items_found" value=''/>
			           <display:setProperty name="paging.banner.all_items_found" value=''/>
			           <display:setProperty name="paging.banner.group_size" value='50'/>
	                   <display:setProperty name="paging.banner.onepage" value=''/>
			           <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
			           <display:setProperty name="paging.banner.page.selected" value='<li class="active"><a href="#">{0}</a></li>'/>
			           <display:setProperty name="paging.banner.first" value='<span class="pagelinks"><div class="pagination center pull-left"><ul><li></li><li>{0}</li><li><a href="{3}">Next &gt;</a></li></ul></div></span>'/>
					   <display:setProperty name="paging.banner.last" value='<span class="pagelinks"><div class="pagination center pull-left"><ul><li><a href="{2}">&lt; Prev</a></li><li>{0}</li><li></li></ul></div></span>'/>
					   <display:setProperty name="paging.banner.full" value='<span class="pagelinks"><div class="pagination center pull-left"><ul><li><a href="{2}">&lt; Prev</a></li><li>{0}</li><li><a href="{3}">Next &gt;</a></li></ul></div></span>'/>
				</display:table>
				
				<span class="gutter10 span3">[ Total :  ${enrollmentLogSize} ]</span>
				</div>
					
			</c:if>
			
		</div>	
		</div>
		<c:if test="${empty logReport}">
					<h4><div class="control-group">
						No records found!
					</div></h4>
		</c:if>
		<c:if test="${not empty logReport}">
	        <div class="txt-center">		
				<div class="control-group">
					<input type="button" name="printButton" id="printButton" Value="Print" title="Print" onclick="javascript:callPrint();" class="btn btn-primary">
				</div>
	        </div> 
        </c:if>                 
	</form>
</div>
		
<script type="text/javascript">
$('.date-picker').datepicker({
    autoclose: true,
    format: 'mm-dd-yyyy'
});
/* $(function() {
			$('.datepick').each(function() {
				var ctx = "${pageContext.request.contextPath}";
				var imgpath = ctx+'/resources/images/calendar.gif';
				$(this).datepicker({
					showOn : "button",
					buttonImage : imgpath,
					buttonImageOnly : true
				});
			});
		});
 */		
 
 jQuery.validator.addMethod("checkDuration", function (value, element, param) {
	 var effectiveStartDate = new Date($("#fromDate").val());
	 effectiveStartDate.setHours(0, 0, 0, 0);
	 var effectiveEndDate = new Date($("#toDate").val());
	 effectiveEndDate.setHours(0, 0, 0, 0);
	 if(new Date(effectiveStartDate) > new Date(effectiveEndDate)){
		 return false;
	 }
	 return true;
}); 

 var validator = $('#frmEnrollmentLog').validate({ 
	 	onkeyup: false,
		onclick: false,
		onfocusout: false,
		onSubmit: true,
		rules : {
			'fromDate': {required: true},
			'toDate': {required: true,checkDuration: true}
		},
		messages : {
			fromDate: { 
				required: "<span> <em class='excl'>!</em><spring:message code='label.selectFromDt' javaScriptEscape='true'/></span>"
			},
			toDate: {
				required: "<span> <em class='excl'>!</em><spring:message code='label.selectToDt' javaScriptEscape='true'/></span>",
				checkDuration : "<span> <em class='excl'>!</em><spring:message code='label.toDateValidate' javaScriptEscape='true'/></span>" 
			}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
			$('#fromDate').unbind('focus');
			$('#toDate').unbind('focus');
		} 
	});

 
	function callPrint() {
		var contentId = "logPanel";
		var prtContent = document.getElementById(contentId);
		var WinPrint = window.open('', '', 'letf=100,top=100,width=600,height=600');
		var newHtml=prtContent.innerHTML;
		newHtml=newHtml.replace('<table class="displaytagtable">','<table border="1"><h4>Monthly Enrollment Log</h4>');
		while(newHtml.indexOf('<span class="offScreen">Sortable</span>') != -1){
			newHtml=newHtml.replace('<span class="offScreen">Sortable</span>','');	
		}
		if(newHtml.indexOf('<span class="pagelinks">') != -1){
			newHtml=newHtml.substring(0,newHtml.indexOf('<span class="pagelinks">'));	
		}
		
		WinPrint.document.write(newHtml);
		WinPrint.document.close();
		WinPrint.focus();
		
		setTimeout(function(){WinPrint.print()},800);	
		//WinPrint.close() 

	}
	
</script>		

<style>
	.sss{
		max-width:600px !important;
		overflow-x:auto;
	}
</style>