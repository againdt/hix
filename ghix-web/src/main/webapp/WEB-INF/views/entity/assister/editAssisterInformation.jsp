<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script type="text/javascript" src="<c:url value="/resources/js/addressutils.js" />"></script>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
	
<script type="text/javascript">
	function highlightThis(val){
		
		var currUrl = window.location.href;
		var newUrl="";
		/* Check if Query String parameter is set or not */
		if(currUrl.indexOf("?", 0) > 0) { /* If Yes */
			if(currUrl.indexOf("?lang=", 0) > 0) {/* Check if locale is already set without querystring param  */
				 newUrl = "?lang="+val;
			} else if(currUrl.indexOf("&lang=", 0) > 0) { /*  Check if locale is already set with querystring param  */
				newUrl = currUrl.substring(0, currUrl.length-2)+val; 
			} else { 
				newUrl = currUrl + "&lang="+val;
			}
		} else { /* If No  */
			newUrl = currUrl + "?lang="+val;
		}
		window.location = newUrl;
	}
</script>

				<div class="gutter10-lr">
					<div class="l-page-breadcrumb hide">
						<!--start page-breadcrumb -->
						<div class="row-fluid">
							<ul class="page-breadcrumb">
								<li><a href="#">&lt; <div class="control-group"><label class="control-label" for="firstName"><spring:message code="label.assister.back"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/manageassister" />"><div class="control-group"><label class="control-label" for="firstName"><spring:message code="label.assister.assisters"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/manageassister" />"><div class="control-group"><label class="control-label" for="firstName"><spring:message code="label.assister.manage"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/viewassisterinformation?assisterId=${assister.id}" />">${assister.firstName} ${assister.lastName}</a></li>
							</ul>
							<!--page-breadcrumb ends-->
						</div><!-- end of .row-fluid -->
					</div><!--l-page-breadcrumb ends-->
					
					
					<div class="row-fluid">
						<h1><a name="skip"></a>${assister.firstName} ${assister.lastName}</h1>
					</div>
					<div class="row-fluid">
						<jsp:include page="/WEB-INF/views/entity/leftNavigationMenu.jsp" />
						<div class="span9">
							<form class="form-horizontal" id="frmupdateassisterinformation"  name="frmupdateassisterinformation" method="post" action="editassisterinformation">
								<input type="hidden" id="assisterId" name="assisterId" value="${assister.id}" />
			
								<div class="header">
									<h4 class="pull-left">Assister Information</h4>
									<a class="btn btn-small pull-right" href="<c:url value="/entity/assisteradmin/viewassisterinformation?assisterId=${assister.id}" />">Cancel</a>
								</div>
								<div class="gutter10">
										<div class="control-group">
										<label class="control-label" for="firstName"><spring:message code="label.assister.Firstname"/></label>
												 <div class="controls">
												 <input type="text" name="firstName" id="firstName" value="${assister.firstName}" class="input-xlarge" size="30" maxlength="50"/>
												</div>
												</div>
											
											
												<div class="control-group">
												<label class="control-label" for="lastName"><spring:message code="label.assister.Lastname"/>
												</label><div class="controls">
												<input type="text" name="lastName" id="lastName" value="${assister.lastName}" class="input-xlarge" size="30" maxlength="50"/>
											</div>
											</div>
											
												<div class="control-group">
												<label class="control-label" for="firstName"><spring:message code="label.assister.phoneNumber"/></label>
										<div class="controls">
										<label class="aria-hidden" for="primaryPhone1">Primary Phone Number First Three digits</label>
													<input type="text" name="primaryPhone1" id="primaryPhone1" value="${primaryPhone1}" maxlength="3" placeholder="xxx" class="area-code input-mini"  />
													<label class="aria-hidden" for="primaryPhone2">Primary Phone Number Second Three digits</label>
													<input type="text" name="primaryPhone2" id="primaryPhone2" value="${primaryPhone2}" maxlength="3" placeholder="xxx" class="input-mini" />
													<label class="aria-hidden" for="primaryPhone3">Primary Phone Number Last Four digits</label>
													<input type="text" name="primaryPhone3" id="primaryPhone3" value="${primaryPhone3}" maxlength="4" placeholder="xxxx" class="input-small" />
													<input type="hidden" name="primaryPhoneNumber" id="primaryPhoneNumber" value="" />      
													<div id="primaryPhone3_error"></div>
												</div>
												</div>
											
											
												<div class="control-group"><label class="control-label" for="emailAddress"><spring:message code="label.assister.emailAddress"/>
												</label><div class="controls"><input type="text" name="emailAddress" id="emailAddress" value="${assister.emailAddress}" class="input-xlarge" size="30" placeholder="company@email.com"/>
			                						<div id="emailAddress_error"></div>
			                						</div></div>
			                					
											
											
												<div class="control-group"><label class="control-label" for="businessLegalName"><spring:message code="label.assister.businessLegalName"/>
												</label><div class="controls"><input type="text" name="businessLegalName" id="businessLegalName" value="${assister.businessLegalName}" class="input-xlarge" size="30" maxlength="50"/>
			                						<div id="businessLegalName_error"></div>
			                						</div>
			                				
								</div>
					<div class="gutter10">
					<div class="header">
						<h4><spring:message code="label.assister.mailingAddress"/></h4>
					</div>
						
								
									<div class="control-group"><label class="control-label" for="address1"><spring:message code="label.assister.streetAddress1"/>
									</label><div class="controls"><input type="text" placeholder="Street Name, P.O. Box, Company, c/o" name="mailingLocation.address1" id="address1" value="${assister.mailingLocation.address1}" class="input-xlarge" size="30" maxlength="25"/>
										<input type="hidden" id="address1_hidden" name="address1_hidden" value="${assister.mailingLocation.address1}">
										<div id="address1_error"></div>
										</div>
										</div>
									
								
								
								<c:if test="${assister.mailingLocation.address2!=null}">
									
										<div class="control-group"><label class="control-label" for="address2"><spring:message code="label.assister.streetAddress2"/>
										
											</label><div class="controls"><input type="text" placeholder="Apt, Suite, Unit, Bldg, Floor, etc" name="mailingLocation.address2" id="address2" value="${assister.mailingLocation.address2}" class="input-xlarge" size="30" maxlength="25"/>
										<input type="hidden" id="address2_hidden" name="address2_hidden" value="${assister.mailingLocation.address2}">
									</div></div>
								</c:if>
								
								
									<div class="control-group"><label class="control-label" for="city"><spring:message code="label.assister.city"/>
									
										</label><div class="controls"><input type="text" placeholder="City, Town" name="mailingLocation.city" id="city" value="${assister.mailingLocation.city}" class="input-xlarge" size="30" maxlength="15"/>
										<input type="hidden" id="city_hidden" name="city_hidden" value="${assister.mailingLocation.city}">
										<div id="city_error"></div>
									</div></div>
								
								
								
									<div class="control-group"><label class="control-label" for="state"><spring:message code="label.assister.state"/></label>
									<div class="controls">
										<select size="1" id="state" name="mailingLocation.state" path="statelist" class="input-medium">
										<option value="">Select</option>
										<c:forEach var="state" items="${statelist}">
											<option id="${state.code}" <c:if test="${state.code == assister.mailingLocation.state}"> selected="selected"</c:if>  value="<c:out value="${state.code}" />">
												<c:out value="${state.name}" />
											</option>
										</c:forEach>
										</select>
										<input type="hidden" id="state_hidden" name="state_hidden" value="${assister.mailingLocation.state}">
										<div id="state_error"></div>
										</div></div>
											
								
									<div class="control-group">
									<label class="control-label" for="zip_mailing"><spring:message code="label.assister.zipCode"/></label>
									<div class="controls"><input type="text" placeholder="" name="mailingLocation.zip" id="zip" value="${assister.mailingLocation.zip}" class="input-mini zipCode" maxlength="5"	 />
										<input type="hidden" id="zip_hidden" name="zip_hidden" value="${assister.mailingLocation.zip}">
										<div id="zip_error"></div>
									</div>
									</div>
						<div class="form-actions noBackground">
							<input type="submit" name="SaveAssister" id="SaveAssister" value="Save" class="btn btn-primary" />
						</div>
						</div>
						<input type="hidden" name="mailingLocation.lat" id="lat" value="${assister.mailingLocation.lat != null ? assister.mailingLocation.lat : 0.0}" />
						<input type="hidden" name="mailingLocation.lon" id="lon" value="${assister.mailingLocation.lon != null ? assister.mailingLocation.lon : 0.0}" />
						<input type="hidden" name="mailingLocation.rdi" id="rdi" value="${assister.mailingLocation.rdi != null ? assister.mailingLocation.rdi : ''}" />
					<input type="hidden" name="mailingLocation.county" id="county" value="${assister.mailingLocation.county}" />
					</form>
				</div>
			</div>
		</div>
		
		
<script type="text/javascript">
function ie8Trim(){
	if(typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function() {
        	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
        };
	}
}
$(document).ready(function(){		
	$("#assisterInformation").removeClass("link");
	$("#assisterInformation").addClass("active");
});

var validator = $("#frmupdateassisterinformation").validate({ 
	onkeyup: false,
	onclick: false,
	rules : {
		firstName : {required : true},
		lastName : {required : true},
		businessLegalName : {required : true},
		emailAddress : { required : true,email: true},
		primaryPhone3 : {primaryphonecheck : true},
		"mailingLocation.address1" : { required: true},
		"mailingLocation.city" : { required: true},
		"mailingLocation.state" : { required: true},
		"mailingLocation.zip" : {required: true, MailingZipCodecheck: true}
	},
	messages : {
		firstName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstName' javaScriptEscape='true'/></span>"},
		lastName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateLastName' javaScriptEscape='true'/></span>"},
		businessLegalName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateBusinessLegalName' javaScriptEscape='true'/></span>"},
		emailAddress: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEmailAddress' javaScriptEscape='true'/></span>",
	    	email : "<span> <em class='excl'>!</em>Please enter valid email.</span>"},
		primaryPhone3: { primaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNo' javaScriptEscape='true'/></span>"},

		"mailingLocation.address1" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"},
		"mailingLocation.city" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
		"mailingLocation.state" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateState' javaScriptEscape='true'/></span>"},
		"mailingLocation.zip" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateZip' javaScriptEscape='true'/></span>",
		MailingZipCodecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateZipSyntax' javaScriptEscape='true'/></span>"}
	}
	,
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});

jQuery.validator.addMethod("primaryphonecheck", function(value, element, param) {
	ie8Trim();
	primphone1 = $("#primaryPhone1").val().trim(); 
	primphone2 = $("#primaryPhone2").val().trim(); 
	primphone3 = $("#primaryPhone3").val().trim(); 

	if( (primphone1 == "" || primphone2 == "" || primphone3 == "")  || (primphone1 != parseInt(primphone1) || isNaN(primphone1)) || (primphone1.length < 3 ) || (primphone2 != parseInt(primphone2) || isNaN(primphone2)) || (primphone2.length < 3 ) || (primphone3 != parseInt(primphone3) || isNaN(primphone3)) || (primphone3.length < 4 )  )
	{
		return false;
	}
	else
	{
		$("#primaryPhoneNumber").val(primphone1 + primphone2 + primphone3);
		return true;
	}	
} );


// function shiftbox(element,nextelement){
// 	ie8Trim();
// 	maxlength = parseInt(element.getAttribute('maxlength'));
// 	if(element.value.length == maxlength){
// 		nextelement = document.getElementById(nextelement);
// 		nextelement.focus();
// 	}
// }

jQuery.validator.addMethod("MailingZipCodecheck", function(value, element, param) {
	ie8Trim();
	zip = $("#zip").val().trim(); 
	if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || (zip == "00000") )){ 
		return false; 
	}
	return true;
});

</script>	
		
		<script type="text/javascript">
if(!NREUMQ.f){NREUMQ.f=function(){NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:")+"//"+"d1ros97qkrwjf5.cloudfront.net/42/eum/rum.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-3.newrelic.com","18650c3d6e","1192004","ZVVQN0FQWRBZABBfDFwfeDBjHmAmek4teCUdRlsGREIYD1kaC0MXQR9BC1ZdW01SEBQ=",0,194,new Date().getTime(),"","","","",""]);
		
$('.zipCode').focusout(function(e) {
	if ($(this).val().length == 5){	
		
		var address1_e='address1'; var address2_e='address2'; var city_e= 'city'; var state_e='state'; var zip_e='zip';
		var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
		
		var model_address1 = address1_e + '_hidden' ;
		var model_address2 = address2_e + '_hidden' ;
		var model_city = city_e + '_hidden' ;
		var model_state = state_e + '_hidden' ;
		var model_zip = zip_e + '_hidden' ;
		
		var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
		if(($('#'+ address1_e).val() != "Street Name, P.O. Box, Company, c/o")&&($('#'+ city_e).val() != "City, Town")&&($('#'+ state_e).val() != "State")&&($('#'+ zip_e).val() != '')){
			if(($('#'+ address2_e).val())==="Apt, Suite, Unit, Bldg, Floor, etc"){
				$('#'+ address2_e).val('');
			}	
			viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
				$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
				idsText);	
		}
		
	}
});	

$('#address1').focusin(function() {
	
	if(($('#address2').val())==="Address Line 2"){
		$('#address2').val('');
	}



});

</script>
