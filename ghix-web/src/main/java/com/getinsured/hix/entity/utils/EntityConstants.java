package com.getinsured.hix.entity.utils;

public final class EntityConstants {
	private EntityConstants() {
	}

	public static final String DATE_FORMAT = "MM/dd/yyyy";

	public static final int RESPONSE_CODE_SUCCESS = 200;
	public static final int RESPONSE_CODE_FAILURE = 404;
	public static final int INVALID_INPUT_CODE = 4001;
	
	public static final String STATUS_CERTIFIED = "Certified";
	public static final String STATUS_PENDING = "Pending";
	public static final String NAVIGATION_MENU_MAP = "navigationmenumap";
	public static final String STATUS_INCOMPLETE = "Incomplete";
	public static final String RESPONSE_SUCCESS = "SUCCESS";
	public static final String RESPONSE_FAILURE = "FAILURE";
	public static final String SORT_ORDER = "sortOrder";
	public static final String SORT_BY = "sortBy";
	public static final String ASC = "ASC";
	public static final String CA_STATE_CODE = "CA";
	public static final String ID_STATE_CODE = "ID";
	public static final String NV_STATE_CODE = "NV";
	public static final String GLOBAL_STATE_CODE = "ID_STATE_CODE";
	public static final String GLOBAL_STATE_CODE1 = "NV_STATE_CODE";
}
