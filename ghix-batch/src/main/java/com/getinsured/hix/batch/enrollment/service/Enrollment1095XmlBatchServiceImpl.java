/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.batch.enrollment.util.Enrollment1095Xml;
import com.getinsured.hix.batch.enrollment.util.Enrollment1095Factory;
import com.getinsured.hix.batch.enrollment.util.Enrollment1095XmlWrapper;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
@Service("enrollment1095XmlBatchServiceImpl")
@Transactional
public class Enrollment1095XmlBatchServiceImpl implements Enrollment1095XmlBatchService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095XmlBatchServiceImpl.class);
	
	@Autowired private Enrollment1095Factory enrollment1095Factory;

	/* (non-Javadoc)
	 * @see com.getinsured.hix.batch.enrollment.service.Enrollment1095XmlBatchService#processXmlForEnrollment1095(java.util.List, java.lang.Integer)
	 */
	@Override
	public boolean processXmlForEnrollment1095(List<Integer> enrollment1095Ids, Integer docSeqId, String wipFolder, Map<Integer, String> recordSequenceIdMap, String batchCategoryCode, Enrollment1095XmlWrapper enrollment1095XmlWrapper) throws GIException {
		// Start Processing
		boolean status = true;
		try{
			if(null != enrollment1095Ids && !enrollment1095Ids.isEmpty()){
				Enrollment1095Xml enrollment1095Xml= enrollment1095Factory.getOutboundInstance(enrollment1095XmlWrapper.getCoverageYear());
				enrollment1095Xml.generateXml(enrollment1095Ids, docSeqId, wipFolder, recordSequenceIdMap, enrollment1095XmlWrapper, batchCategoryCode);
				/*Form1095ATransmissionUpstreamType annual1095TransmissionObj = new Form1095ATransmissionUpstreamType();
				StringBuilder enrollmentIds = new StringBuilder();
				Map<String, List<Enrollment1095>> successSkipMap = getUpstreamReportDetails(annual1095TransmissionObj.getForm1095AUpstreamDetail(), enrollment1095Ids, recordSequenceIdMap, enrollmentIds, enrollment1095XmlWrapper);
				String generatedFileName = null;
				if(annual1095TransmissionObj != null && !annual1095TransmissionObj.getForm1095AUpstreamDetail().isEmpty()){
					generatedFileName = generateIrsXml(annual1095TransmissionObj, docSeqId, wipFolder);
					enrollmentIds.deleteCharAt(enrollmentIds.length()-1);
					logEnrollmentOut1095(enrollmentIds.toString(), generatedFileName, enrollment1095XmlWrapper);
				}
				updateStagingTable(successSkipMap, generatedFileName, docSeqId, recordSequenceIdMap, batchCategoryCode, enrollment1095XmlWrapper);*/
			}
		}catch(Exception e){
			LOGGER.debug("Error creating XML inside Enrollment1095XmlBatchServiceImpl.processXmlForEnrollment1095()");
			throw new GIException("Error creating XML ::", e);
		}
		return status;
	}
}
