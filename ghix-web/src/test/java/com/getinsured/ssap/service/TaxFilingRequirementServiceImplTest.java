package com.getinsured.ssap.service;

import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.getinsured.ssap.model.financial.TaxFilingRequirement;

/**
 * Unit tests for {@link TaxFilingRequirementServiceImpl}
 *
 * @author Yevgen Golubenko
 * @since 2019-06-10
 */
@RunWith(MockitoJUnitRunner.class)
public class TaxFilingRequirementServiceImplTest {

  @Mock
  private TaxFilingRequirementServiceImpl taxFilingRequirementService;

  @Before
  public void setUp() throws Exception {
    taxFilingRequirementService = new TaxFilingRequirementServiceImpl();
    taxFilingRequirementService.postConstruct();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void getTaxFilingRequirementsByYear() {
    List<TaxFilingRequirement> taxFilingRequirements = taxFilingRequirementService.getTaxFilingRequirementsByYear(2018);
    Assert.assertNotNull("List of TaxFilingRequirement is null", taxFilingRequirements);
    Assert.assertFalse("List of TaxFilingRequirements is empty", taxFilingRequirements.isEmpty());
  }

  @Test
  public void getTaxFilingRequirements() {
    Map<Integer, List<TaxFilingRequirement>> taxIntegerListMap = taxFilingRequirementService.getTaxFilingRequirements();
    Assert.assertNotNull("Map of Year=>List<TaxFilingRequirement> is null", taxIntegerListMap);
    Assert.assertFalse("Map of Year=>List<TaxFilingRequirement> is empty", taxIntegerListMap.isEmpty());
  }
}
