

import exceptionReducer, { initialState } from '../exceptionReducer';
import { exceptionActionTypes } from '../../constants/actionTypes';

describe('Test for exceptionReducer', () => {

    const anotherAction = {
        type: 'ANOTHER_ACTION',
        payload: []
    };

    const setExceptionAction = {
        type: exceptionActionTypes.SET_EXCEPTION,
        exceptionType: 'Exception_1',
        exceptionPages: [ 12 ]
    };

    const previousState = {
        exceptionType: 'Exception_35',
        exceptionPages: [ 77 ]
    };

    const removeExceptionAction = {
        type: exceptionActionTypes.REMOVE_EXCEPTION
    };

    const updateExceptionAction = {
        type: exceptionActionTypes.UPDATE_EXCEPTION,
        exceptionPages: [ 100 ]
    };

    const clearStateAction = {
        type: exceptionActionTypes.CLEAR_STATE
    };

    const falseValues = [
        null,
        false
    ];

    const wrongValues_notAnArray = [
        23,
        '43346',
        () => {},
        {},
        true
    ];

    const wrongValues_anyButNotNumber = [
        [ '43346' ],
        [ null ],
        [ () => {} ],
        [ {} ],
        [ undefined ],
        [ true ]
    ];

    it('This reducer hasn\'t been invoked', () => {
        expect(exceptionReducer(initialState, anotherAction)).toEqual(initialState);
    });

    wrongValues_notAnArray.forEach(v => {
        it('Should throw an error for exceptionPages (not an array)', () => {
            expect(() => {
                exceptionReducer(initialState, {
                    ...setExceptionAction,
                    exceptionPages: v
                });
            }).toThrow('Not an array has been passed in exceptionPages prop in exceptionReducer');
        });
    });

    wrongValues_anyButNotNumber.forEach(v => {
        it('Should throw an error for exceptionPages (not an number in array)', () => {
            expect(() => {
                exceptionReducer(initialState, {
                    ...setExceptionAction,
                    exceptionPages: v
                });
            }).toThrow('Exception page has not been provided as a number');
        });
    });

    falseValues.forEach(v => {
        it('Should throw an error for false types', () => {
            expect(() => {
                exceptionReducer(initialState, {
                    ...setExceptionAction,
                    exceptionPages: v
                });
            }).toThrow('One of false types was provided in exceptionPages prop in exceptionReducer');
        });
    });

    it('Should set exception', () => {
        expect(exceptionReducer(initialState, setExceptionAction)).toEqual({
            exceptionType: setExceptionAction.exceptionType,
            exceptionPages: setExceptionAction.exceptionPages
        });
    });

    it('Should remove exception', () => {
        expect(exceptionReducer(previousState, removeExceptionAction)).toEqual(initialState);
    });

    it('Should update exception', () => {
        expect(exceptionReducer(previousState, updateExceptionAction)).toEqual({
            exceptionType: previousState.exceptionType,
            exceptionPages: updateExceptionAction.exceptionPages
        });
    });

    it('Should clear state', () => {
        expect(exceptionReducer({ welcomeText: 'Hi there!' }, clearStateAction)).toEqual(initialState);
    });


});