package com.getinsured.iex.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import com.getinsured.hix.model.LookupValue;


/**
 * The persistent class for the SSAP_APPLICATION_EVENTS database table.
 * 
 */
public class SsapApplicationEvent implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Timestamp coverageStartDate;
	private Timestamp coverageEndDate;
	private String verification;
	private String actionEnabled;
	private Timestamp createdTimeStamp;
	private Timestamp eventDate;
	private Timestamp reportStartDate;
	private Timestamp reportEndDate;
	private Timestamp enrollmentStartDate;
	private Timestamp enrollmentEndDate;
	private LookupValue changeEventType;
	private SsapApplication ssapApplication;

	public SsapApplicationEvent() {
	}



	public Timestamp getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(Timestamp coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public Timestamp getCoverageEndDate() {
		return coverageEndDate;
	}

	public void setCoverageEndDate(Timestamp coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}

	public String getVerification() {
		return verification;
	}

	public void setVerification(String verification) {
		this.verification = verification;
	}

	public String getActionEnabled() {
		return actionEnabled;
	}

	public void setActionEnabled(String actionEnabled) {
		this.actionEnabled = actionEnabled;
	}

	public Timestamp getCreatedTimeStamp() {
		return createdTimeStamp;
	}

	public void setCreatedTimeStamp(Timestamp createdTimeStamp) {
		this.createdTimeStamp = createdTimeStamp;
	}

	public Timestamp getEventDate() {
		return eventDate;
	}

	public void setEventDate(Timestamp eventDate) {
		this.eventDate = eventDate;
	}

	public Timestamp getReportStartDate() {
		return reportStartDate;
	}

	public void setReportStartDate(Timestamp reportStartDate) {
		this.reportStartDate = reportStartDate;
	}

	public Timestamp getReportEndDate() {
		return reportEndDate;
	}

	public void setReportEndDate(Timestamp reportEndDate) {
		this.reportEndDate = reportEndDate;
	}

	public Timestamp getEnrollmentStartDate() {
		return enrollmentStartDate;
	}

	public void setEnrollmentStartDate(Timestamp enrollmentStartDate) {
		this.enrollmentStartDate = enrollmentStartDate;
	}

	public Timestamp getEnrollmentEndDate() {
		return enrollmentEndDate;
	}

	public void setEnrollmentEndDate(Timestamp enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}

	public LookupValue getChangeEventType() {
		return changeEventType;
	}

	public void setChangeEventType(LookupValue changeEventType) {
		this.changeEventType = changeEventType;
	}


/*
	public SsapApplication getSsapApplication() {
		return ssapApplication;
	}



	public void setSsapApplication(SsapApplication ssapApplication) {
		this.ssapApplication = ssapApplication;
	}*/


	public SsapApplication getSsapApplication() {
		return ssapApplication;
	}



	public void setSsapApplication(SsapApplication ssapApplication) {
		this.ssapApplication = ssapApplication;
	}



	@Override
	public String toString() {
		return "SsapApplicationEvent [coverageStartDate=" + coverageStartDate
				+ ", coverageEndDate=" + coverageEndDate + ", verification="
				+ verification + ", actionEnabled=" + actionEnabled
				+ ", createdTimeStamp=" + createdTimeStamp + ", eventDate="
				+ eventDate + ", reportStartDate=" + reportStartDate
				+ ", reportEndDate=" + reportEndDate + ", enrollmentStartDate="
				+ enrollmentStartDate + ", enrollmentEndDate="
				+ enrollmentEndDate + ", changeEventType=" + changeEventType
				+ ", ssapApplication=" + ssapApplication + "]";
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}

}