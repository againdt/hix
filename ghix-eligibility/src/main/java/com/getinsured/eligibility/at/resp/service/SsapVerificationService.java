package com.getinsured.eligibility.at.resp.service;

import java.util.List;

import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapVerification;

public interface SsapVerificationService {

	List<SsapVerification> saveOrUpdateSsapVerification(Long ssapApplicantId,
			List<SsapVerification> ssapVerificationList, Integer giWsPayloadId);


	List<SsapVerification> findBySsapApplicantId(Long ssapApplicantId);


	int updateVerificationStatus(Long ssapApplicantId);

	boolean isApplicantVerified(String isFinancialFlag, SsapApplicant applicant, HouseholdMember member);

}
