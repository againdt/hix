package com.getinsured.hix.indportal.dto.dm.application;

public class ContactInformation {
	 private String email;
	 private PhoneNumber primaryPhoneNumber;
	 private PhoneNumber secondaryPhoneNumber;
	 private String mobileNotificationPhoneNumber;


	 // Getter Methods 

	 public String getEmail() {
	  return email;
	 }

	 public PhoneNumber getPrimaryPhoneNumber() {
	  return primaryPhoneNumber;
	 }

	 // Setter Methods 

	 public void setEmail(String email) {
	  this.email = email;
	 }

	 public void setPrimaryPhoneNumber(PhoneNumber primaryPhoneNumberObject) {
	  this.primaryPhoneNumber = primaryPhoneNumberObject;
	 }
 
	public String getMobileNotificationPhoneNumber() {
		return mobileNotificationPhoneNumber;
	}

	public void setMobileNotificationPhoneNumber(String mobileNotificationPhoneNumber) {
		this.mobileNotificationPhoneNumber = mobileNotificationPhoneNumber;
	}

	public PhoneNumber getSecondaryPhoneNumber() {
		return secondaryPhoneNumber;
	}

	public void setSecondaryPhoneNumber(PhoneNumber secondaryPhoneNumber) {
		this.secondaryPhoneNumber = secondaryPhoneNumber;
	}
	}
