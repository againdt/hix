package com.getinsured.iex.hub.vci.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.AnnualCompensationInformationType;
import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.AnnualCompensationType;

/**
 * Wrapper class for
 * com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.AnnualCompensationInformationType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class AnnualCompensationTypeWrapper implements JSONAware{
	
	
	private List<AnnualCompensationInformationTypeWrapper> annualCompensationInformation;
	
    private static final String COMPENSATION_KEY = "AnnualCompensationInformation";
    
	public AnnualCompensationTypeWrapper(AnnualCompensationType annualCompensationType){
		
		if(annualCompensationType == null){
			return;
		}
		
		for(AnnualCompensationInformationType compensation : annualCompensationType.getAnnualCompensationInformation()){
			getAnnualCompensationInformation().add(new AnnualCompensationInformationTypeWrapper(compensation));
		}
		
	}
	
	public List<AnnualCompensationInformationTypeWrapper> getAnnualCompensationInformation() {
        if (annualCompensationInformation == null) {
            annualCompensationInformation = new ArrayList<AnnualCompensationInformationTypeWrapper>();
        }
        return this.annualCompensationInformation;
    }
	
	@SuppressWarnings("unchecked")
   	public String toJSONString() {
   		JSONObject obj = new JSONObject();
   		
   		JSONArray array = new JSONArray();
   		for(AnnualCompensationInformationTypeWrapper compensation : this.annualCompensationInformation){
   			array.add(compensation);
   		}
   		obj.put(COMPENSATION_KEY, array);
   		
   		return obj.toJSONString();
   	}
	
}
