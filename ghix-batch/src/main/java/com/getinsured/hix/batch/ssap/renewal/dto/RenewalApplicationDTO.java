package com.getinsured.hix.batch.ssap.renewal.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RenewalApplicationDTO {
	private String healthRenewalStatus;
	private String dentalRenewalStatus;
	private String oeStartDate;
	private String oeEndDate;
	private String renewalYear;
	private Map<Long,List<String>> healthFallOutReasonCodesByMember;
	private Map<Long,List<String>> dentalFallOutReasonCodesByMember;
	private Map<Long,String> healthGroupReasonCodes;
	private GenerateNoticeEnum generateNoticeFor;
	private String caseNumber;
	private Set<String> healthCodes;
	private Set<String> dentalCodes;
	
	public String getHealthRenewalStatus() {
		return healthRenewalStatus;
	}

	public void setHealthRenewalStatus(String healthRenewalStatus) {
		this.healthRenewalStatus = healthRenewalStatus;
	}

	public String getDentalRenewalStatus() {
		return dentalRenewalStatus;
	}

	public void setDentalRenewalStatus(String dentalRenewalStatus) {
		this.dentalRenewalStatus = dentalRenewalStatus;
	}

	public String getOeStartDate() {
		return oeStartDate;
	}

	public void setOeStartDate(String oeStartDate) {
		this.oeStartDate = oeStartDate;
	}

	public String getOeEndDate() {
		return oeEndDate;
	}

	public void setOeEndDate(String oeEndDate) {
		this.oeEndDate = oeEndDate;
	}

	public String getRenewalYear() {
		return renewalYear;
	}

	public void setRenewalYear(String renewalYear) {
		this.renewalYear = renewalYear;
	}

	public Map<Long, List<String>> getHealthFallOutReasonCodesByMember() {
		return healthFallOutReasonCodesByMember;
	}

	public void setHealthFallOutReasonCodesByMember(Map<Long, List<String>> healthFallOutReasonCodesByMember) {
		this.healthFallOutReasonCodesByMember = healthFallOutReasonCodesByMember;
	}

	public Map<Long, List<String>> getDentalFallOutReasonCodesByMember() {
		return dentalFallOutReasonCodesByMember;
	}

	public void setDentalFallOutReasonCodesByMember(Map<Long, List<String>> dentalFallOutReasonCodesByMember) {
		this.dentalFallOutReasonCodesByMember = dentalFallOutReasonCodesByMember;
	}

	public Map<Long,String> getHealthGroupReasonCodes() {
		return healthGroupReasonCodes;
	}

	public void setHealthGroupReasonCodes(Map<Long,String> healthGroupReasonCodes) {
		this.healthGroupReasonCodes = healthGroupReasonCodes;
	}

	public GenerateNoticeEnum getGenerateNoticeFor() {
		return generateNoticeFor;
	}

	public void setGenerateNoticeFor(GenerateNoticeEnum generateNoticeFor) {
		this.generateNoticeFor = generateNoticeFor;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	
	public void prepareHealthReasonCodes() {
		Set<String> healthCodes = new HashSet<String>();

		for (List<String> reasonCodes : this.healthFallOutReasonCodesByMember.values()) {
			for (String code : reasonCodes) {
				healthCodes.add(code.substring(code.indexOf(",") + 1));
			}
		}

		this.healthCodes = healthCodes;
	}
	
    public void preapreDentalReasonCodes() {
    	Set<String> dentalCodes = new HashSet<String>();

		for (List<String> reasonCodes : this.dentalFallOutReasonCodesByMember.values()) {
			for (String code : reasonCodes) {
				dentalCodes.add(code.substring(code.indexOf(",") + 1));
			}
		}

		this.dentalCodes = dentalCodes;
	}
    
	public String constructCodesInHTMLFormat(Set<String> codes) {
		
		if(codes == null || codes.size() == 0) 
			return "";
		
		String reasonCodes = "";
		
		for (String code : codes) {
			reasonCodes = reasonCodes + " <li> " + code + " </li> ";
		}

		return "<ul>" + reasonCodes + "</ul>";
	}

	public Set<String> getHealthCodes() {
		return healthCodes;
	}

	public void setHealthCodes(Set<String> healthCodes) {
		this.healthCodes = healthCodes;
	}

	public Set<String> getDentalCodes() {
		return dentalCodes;
	}

	public void setDentalCodes(Set<String> dentalCodes) {
		this.dentalCodes = dentalCodes;
	}
}