package com.getinsured.hix.batch.enrollment.util;

import java.io.File;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;

import com.getinsured.hix.batch.enrollment.service.EnrlPopulateExtEnrollmentService;

public class EnrlPopulateExtEnrollmentJobThread implements Callable<String> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlPopulateExtEnrollmentJobThread.class);
	
	private String filePath = null;
	private Long jobId = null;
	private JobService jobService = null;
	private StepExecution stepExecution = null;
	private File archivePath = null;
	private EnrlPopulateExtEnrollmentService enrlPopulateExtEnrollmentService;
	
	public EnrlPopulateExtEnrollmentJobThread(String filePath, 
			                                 Long jobId, 
			                                 JobService jobService, 
			                                 StepExecution stepExecution,
			                                 File archivePath,
			                                 EnrlPopulateExtEnrollmentService enrlPopulateExtEnrollmentService) {
		super();
		this.filePath = filePath;
		this.jobId = jobId;
		this.jobService = jobService;
		this.stepExecution = stepExecution;
		this.archivePath = archivePath;
		this.enrlPopulateExtEnrollmentService = enrlPopulateExtEnrollmentService;
	}

	@Override
	public String call() throws Exception {
		try {
			 return enrlPopulateExtEnrollmentService.populateExtEnrollmentData(filePath, jobId, jobService, stepExecution, archivePath);
		} catch (Exception e) {
			LOGGER.error("Exception occurred in EnrollmentReconciliationJobThread: ", e);
		}
		return null;
	}

}
