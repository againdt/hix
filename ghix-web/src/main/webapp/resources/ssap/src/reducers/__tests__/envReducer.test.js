

import envReducer, { initialState } from '../envReducer';
import { headerFooterLinkTypes } from '../../constants/actionTypes';

describe('Test for envReducer', () => {

    const anotherAction = {
        type: 'ANOTHER_ACTION',
        payload: []
    };

    const setEnvironmentAction = {
        type: headerFooterLinkTypes.SET_ENVIRONMENTS,
        headerLinks: {
            value: 'New environment values are here'
        }
    };

    const clearStateAction = {
        type: headerFooterLinkTypes.CLEAR_STATE
    };

    it('This reducer hasn\'t been invoked', () => {
        expect(envReducer(initialState, anotherAction)).toEqual(initialState);
    });

    it('Should set new environment value', () => {
        expect(envReducer(initialState, setEnvironmentAction)).toEqual(setEnvironmentAction.headerLinks);
    });

    it('Should clear state', () => {
        expect(envReducer({ welcomeText: 'Hi there!' }, clearStateAction)).toEqual(initialState);
    });
});