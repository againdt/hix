import React, {Fragment, useState} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Button from '@cmsgov/design-system-core/dist/components/Button/Button';
import FormForModal from '../../components/pages/FormForModal';
import UniversalInput from '../../lib/UniversalInput';
import {hideModal} from '../../actions/actionCreators';
import INPUT_TYPES from '../../constants/inputTypes';
import normalizeAddress from '../../utils/normalizeAddress';
import STATES_LIST from '../../constants/statesList';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

// Components

// Action creators

// Lists


const ValidateAddressModal = props => {

    const {
        modal: {
            modalOwner,
            data: {
                input: {
                    zipcode,
                    addressLine1,
                    state,
                    city
                },
                suggestions: serverSuggestions
            },
            callbacks
        },
        headOfHousehold
    } = props;


    const handleHideClick = () => {
        props.actions.hideModal(modalOwner);
    };

    // Here we create local state for user address values ("You entered" on UI):

    const [ userInput, checkUserInput ] = useState({
        address: normalizeAddress({
            postalCode: zipcode,
            streetAddress1: addressLine1,
            state: STATES_LIST.find(st => st.stateCode === state).stateName,
            city: city
        }),
        values: {
            postalCode: zipcode,
            streetAddress1: addressLine1,
            state: state,
            city: city
        },
        isUserInputChecked: false
    });

    // Here we create local state for API suggestions ("We found" on UI):

    const [ machineSuggestions, checkSuggestion] = useState(
        serverSuggestions ? serverSuggestions.map((suggestion, i) => ({
            address: normalizeAddress({
                postalCode: suggestion.zipcode,
                streetAddress1: suggestion.addressLine1,
                state: STATES_LIST.find(state => state.stateCode === suggestion.state).stateName,
                city: suggestion.city
            }),
            values: {
                postalCode: suggestion.zipcode,
                streetAddress1: suggestion.addressLine1,
                state: suggestion.state,
                city: suggestion.city
            },
            [`isSuggestion${i}Checked`]: false
        })) : []
    );

    /**
     *
     * @param {Object} newData - updated (checked) machine suggestion
     *
     * Here we check/uncheck needed machine suggestion and if we have one of them checked
     * we uncheck rest of machine suggestions and user input address as well
     */
    const onFieldChange_suggestions = newData => {
        checkSuggestion(machineSuggestions.map((suggestion, i) =>
            suggestion.address === newData.address ?
                newData :
                {
                    ...suggestion,
                    [`isSuggestion${i}Checked`]: false
                }
        ));
        if(machineSuggestions.find((suggestion, i) => suggestion[`isSuggestion${i}Checked`])) {
            checkUserInput(userInput => ({
                ...userInput,
                isUserInputChecked: false
            }));
        }
    };

    /**
     *
     * @param {Object} newData - updated (checked) user input
     *
     * Here we check/uncheck user input address, and if it is checked we uncheck all machine suggestions
     */
    const onFieldChange_userInput = newData => {

        checkUserInput(state => ({
            ...state,
            ...newData
        }));

        if(userInput.isUserInputChecked && machineSuggestions.length !== 0) {
            checkSuggestion(suggestions => suggestions.map((suggestion, i) => ({
                ...suggestion,
                [`isSuggestion${i}Checked`]: false
            })));
        }
    };

    /**
     * Here we resolve situation:
     *
     * 1. To save suggestion if our user checked it
     *  or
     * 2. Not to save if nothing was checked
     *
     */
    const handleConfirmationClick = () => {
        const checkedMachineSuggestion = machineSuggestions.find((suggestion, i) => suggestion[`isSuggestion${i}Checked`]);

        const valueToSave =
            userInput.isUserInputChecked ?
                userInput.values :                      // If User agrees with address he/she entered in Form on Page
                checkedMachineSuggestion ?
                    checkedMachineSuggestion.values :   // If User chose one of suggestions
                    null;                               // None of inputs being checked

        if(valueToSave) {
            const newData = { ...headOfHousehold };
            newData.householdContact.homeAddress = {
                ...newData.householdContact.homeAddress,
                ...valueToSave
            };
            callbacks(newData); // Here we invoke callback that we passed from Page via Redux Store
            handleHideClick();
        } else {
            handleHideClick();
        }
    };



    return (
        <Fragment>
            <div className="modal__header" id="modal__validate__address">
                <h2>Address not found</h2>
                <div className="modal__btn--close">
                    <a href="#" onClick={handleHideClick}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                </div>
            </div>
            <div className="modal__body txt-left">
                <FormForModal>
                    <Fragment>
                        <div className="modal__subheading">
                            <h3>You Entered</h3>
                            <div className="modal__address__list">
                                <UniversalInput
                                    label={{
                                        labelClasses: ['ui', 'gi-checkbox'],
                                        text: userInput.address
                                    }}
                                    inputData={{
                                        isInputRequired: false,
                                        type: INPUT_TYPES.CHECKBOX,
                                        name: 'isUserInputChecked',
                                        checked: userInput.isUserInputChecked,
                                        value: userInput.isUserInputChecked,
                                        currentData: userInput,
                                        fields: []
                                    }}
                                    inputActions={{
                                        onFieldChange: onFieldChange_userInput
                                    }}
                                />
                            </div>
                        </div>
                        {
                            machineSuggestions && machineSuggestions.length === 0  &&
                            <div className='usa-alert usa-alert-error gi__alert--error margin-t-20' role='alert'>
                                <div className='usa-alert-body'>
                                    <p className='usa-alert-text'>
                                        The address you entered is not in the postal database. Please check it for accuracy. <br/>
                                        Click OK to proceed with the address you entered.
                                    </p>
                                </div>
                            </div>
                        }
                    </Fragment>

                    {/******************************************
                     Begin - If #machineSuggestions (not empty)
                     ******************************************/}

                    {
                        machineSuggestions && machineSuggestions.length !== 0  &&

                        <Fragment>
                            <div className="modal__subheading margin-t-20">
                                <h3>We Found</h3>
                                {
                                    machineSuggestions && machineSuggestions.map((suggestion, i) => {
                                        return (
                                            <div className="modal__address__list" key={`${suggestion.address}_${i}`}>
                                                <UniversalInput
                                                    label={{
                                                        labelClasses: ['ui', 'gi-checkbox'],
                                                        text: suggestion.address
                                                    }}
                                                    inputData={{
                                                        isInputRequired: false,
                                                        type: INPUT_TYPES.CHECKBOX,
                                                        name: `isSuggestion${i}Checked`,
                                                        checked: suggestion[`isSuggestion${i}Checked`],
                                                        value: suggestion[`isSuggestion${i}Checked`],
                                                        currentData: suggestion,
                                                        fields: []
                                                    }}
                                                    inputActions={{
                                                        onFieldChange: onFieldChange_suggestions
                                                    }}
                                                />
                                            </div>
                                        );
                                    })
                                }
                            </div>
                            <div className='usa-alert usa-alert-error gi__alert--error margin-t-20' role='alert'>
                                <div className='usa-alert-body'>
                                    <p className='usa-alert-text'>
                                        Select the address we found in the postal database and click OK to proceed or click Cancel to edit the address.
                                    </p>
                                </div>
                            </div>
                        </Fragment>
                    }

                    {/******************************************
                     End - If #machineSuggestions (not empty)
                     ******************************************/}
                </FormForModal>
            </div>
            <div className="modal__footer">
                <div className="usa-grid controls">
                    <div className="usa-offset-one-third txt-left">
                        <Button
                            className='modal__btn--cancel usa-button-secondary'
                            onClick={handleHideClick}
                        >
                            Cancel
                        </Button>
                        <Button
                            className='modal__btn--confirm'
                            onClick={handleConfirmationClick}
                        >
                            OK
                        </Button>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    headOfHousehold: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember[0]
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        hideModal,
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ValidateAddressModal);
