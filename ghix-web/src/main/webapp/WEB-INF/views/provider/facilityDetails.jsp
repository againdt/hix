<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<script src="//maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<style type="text/css">
#detail {
    padding-top: 0 !important;
	background: #fff;
}
.border-b {
	border-bottom: 1px solid #E5E5E5;
}
.border-t {
	border-top: 1px solid #E5E5E5;
}
.container {
	margin-top: -40px;
	padding: 0 13px;
	width: 97%;
}
#profile-info h1 {margin-top: 0;}
#profile-info dl {margin: 10px 0 0;}
#profile-info dt, #profile-info dd {line-height: 24px;}
	
#profile-info dl dt{margin-right: 3px; width: auto; font-weight: normal;}
#profile-info dd {font-weight: bold;}
#profile-info .dl-horizontal dd {margin-left: 15px;}
address {margin-bottom: 0;}
</style>
<script type="text/javascript">
function insertData(){
	var idvalue = '${facility.id}';
	var addId= '${facilityAddressObj.id}';
	var name = '${facility.provider.name}';
	var speca = '${specialties}';
	var line1 = '${facilityAddressObj.address1}';
	var city = '${facilityAddressObj.city}';
	var state = '${facilityAddressObj.state}';
	var zipcode = '${facilityAddressObj.zip}';
	var contactNum ='${facilityAddressObj.phoneNumber}';
	var email =null;
	var qual = null;
	
	var existingData = []; 
	if(parent.document.getElementById("facilities").value!=""){
		existingData = JSON.parse(parent.document.getElementById("facilities").value);
	}
	var jsondata = {id: idvalue ,name: name,qualification: qual,specialties: speca ,contactNumber: contactNum ,email: email ,address:[{id:addId,line1:line1,zip:zipcode,city:city,state:state}]};
	existingData.push(jsondata);
	parent.document.getElementById("facilities").value = JSON.stringify(existingData); 
	window.parent.displayProviders("facilities");
	
	$('div').remove('.modal bigModal in');
	window.parent.closeModal();
 }
</script>
<script type="text/javascript">

 $(document).ready(function(){
    var locations = ${mapString};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(locations[0][0],locations[0][1]),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marker;
	for (i = 0; i <= locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][0],locations[i][1]),
        map: map    
      });
      marker.set("type", "point");
      marker.set("id", 1);
    };
}); 

</script>
<body id="detail">
    <div class="gutter10">
    	<div class="row-fluid">
    		<a class="btn" href="javascript:history.go(-1)"><spring:message code='label.back'/></a>
    		<a class="btn btn-primary" href="/hix/provider/searchproviders?domElement=facilities&zipcode=${defaultZip}"><spring:message code='label.searchAgain'/></a>
        </div>
    </div>
    <div class="gutter10-lr">
    	<form class="form-horizontal" id="frmdetail" name="frmdetail" action="/plandisplay/preferences"  method="POST">
        	<div class="row-fluid border-t">
        	<div class="gutter10 span7">
            	<div id="profile-info" class="row-fluid">
                	<h1>${facility.provider.name}</h1>
    				<div class="doc-info">
    					
    					<address>
    					<c:forEach var="facilityAddresses" items="${facility.facilityAddress}">
    						${facilityAddresses.address1}, ${facilityAddresses.state} ${facilityAddresses.zip} <br>
    					</c:forEach>
    					<br><a href="mailto:#">${facility.websiteUrl}</a></address>
    				</div>
                </div><!--end profile-info-->
            </div> 
            <div class="span5 gutter10-tb">
    			<div id="map" style="width: 220px; height: 205px;"></div>
    		</div>
           	</div><!--end row-fluid-->
            <div class="row-fluid gutter10-tb border-t">
            	<a class="btn btn-primary" href="#" onClick="insertData();" ><spring:message code='label.addToList'/></a>
            </div>
        </form>
    </div>
    <!--footer-->
</body>