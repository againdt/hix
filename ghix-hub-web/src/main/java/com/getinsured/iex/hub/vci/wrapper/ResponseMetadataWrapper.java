package com.getinsured.iex.hub.vci.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.hix._0_1.hix_core.ObjectFactory;
import com.getinsured.iex.hub.vci.gov.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.TextType;


/**
 * Wrapper com.getinsured.iex.hub.vci.gov.cms.hix._0_1.hix_core.ResponseMetadataType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class ResponseMetadataWrapper implements JSONAware{
	
	private ResponseMetadataType responseMetadataType;
	
	private String responseCode;
	private String responseDescriptionText;
	private String tdsResponseDescriptionText;
    
    public ResponseMetadataWrapper(ResponseMetadataType responseMetadataType){
    	
    	if(responseMetadataType == null){
    		return;
    	}
    	
    	ObjectFactory factory = new ObjectFactory();
    	this.responseMetadataType = factory.createResponseMetadataType();
    	
    	TextType value = responseMetadataType.getResponseCode();
    	this.responseCode = value != null ? value.getValue() : null;
    	this.responseMetadataType.setResponseCode(value);
    	
    	value = responseMetadataType.getResponseDescriptionText();
    	this.responseDescriptionText = value != null ? value.getValue() : null;
    	this.responseMetadataType.setResponseDescriptionText(value);
    	
    	value = responseMetadataType.getTDSResponseDescriptionText();
    	this.tdsResponseDescriptionText = value != null ? value.getValue() : null;
    	this.responseMetadataType.setTDSResponseDescriptionText(value);
    }
    
    public ResponseMetadataType getResponseMetadataType(){
    	return responseMetadataType;
    }
    
	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescriptionText() {
		return responseDescriptionText;
	}

	public void setResponseDescriptionText(String responseDescriptionText) {
		this.responseDescriptionText = responseDescriptionText;
	}

	public String getTdsResponseDescriptionText() {
		return tdsResponseDescriptionText;
	}

	public void setTdsResponseDescriptionText(String tdsResponseDescriptionText) {
		this.tdsResponseDescriptionText = tdsResponseDescriptionText;
	}
    
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseCode", this.responseCode);
		obj.put("ResponseDescriptionText", this.responseDescriptionText);
		obj.put("TDSResponseDescriptionText", this.tdsResponseDescriptionText);
		return obj.toJSONString();
	}
	
}
