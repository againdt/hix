export interface PersonalizedState {
    doctor: {
        visitFrequency: string,
        doctorSearch: ProviderInfo[],
        nearZipcode: string
    },
    drug: {
        monthlyPrescription: string,
        drugSearch: DrugDetail[],
    }

}

export interface DrugInfo {
    drugID: string;
    drugName: string;
    drugType: string;
}

export interface DosageInfo {
    drugDosageID: string;
    drugDosageName: string;
    drugRxCui: string;
    strength: string;
    isCommonDosage: boolean;
    genericDosageName: string;
    genericDosageID: string;

}
export interface RxcuiInfo {
    genericName: string;
    genericRxcui: string;
    genericDosage: string;
    drugType: string;
}

export interface DrugDetail {
    drugID: string;
    drugName: string;
    drugType: string;
    drugDosage: string; // drugDosageName
    drugRxCode: string; // rx
    genericID: string;
    genericName: string;
    genericDosage: string; //genericDosageName
    genericRxCode: string; // rx generic
}

// the unfiltered data from the endpoints
export interface ProviderFullData {
    acceptingNewPatients: null;
    accessibilityCodes: null;
    distanceFromUserLocation: number;
    groupKey: null;
    hospitalAffiliation: null;
    identifiers: {};
    languages: null;
    locationId: null;
    medicalGroupAffiliation: null;
    name: string;
    networkId: null;
    networkTierId: null;

    providerType: 'doctor' | 'facility';
    providerTypeIndicator: string;
    specialty: string;
    specialties: {};
    strenuus_id: string;
    sanctionStatus: string;
    providerAddress: ProviderAddress[];
}


// the data to send to server
export interface ProviderInfo {
    id: string;
    name: string;
    specialty: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    providerType: "doctor" | "facility";
    locationId: string;
}




export interface ProviderAddress {
    addLine2: null,
    addline1: string,
    city: string,
    distanceFromUserLocation: number,
    fax: string,
    lat: number,
    lon: number,
    phone: string[]
    prac_location: string,
    proximity: number,
    state: string,
    zip: string
}




export interface HouseHold {
    zipCode: string,
    noOfMembers: number,
    coverageYear: number,
    householdId: string,
    planPremiumList: [{
        hiosId: string,
        netPremium: number
    }],
    members: [{
        id: string,
        age: number,
        gender: "M" | "F"
    }],
    pdPreferencesDTO: {
        medicalUse: "LEVEL1" | "LEVEL2" | "LEVEL3" | "LEVEL4",
        prescriptionsUse: "LEVEL1" | "LEVEL2" | "LEVEL3" | "LEVEL4",
        providers: ProviderInfo[],
        prescriptions: DrugDetail[],
        pdHouseholdId: number
    }

}
