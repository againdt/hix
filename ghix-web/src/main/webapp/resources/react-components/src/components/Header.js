import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { SketchPicker } from 'react-color';
import { instanceOf } from 'prop-types';
import axios from 'axios';
import { withCookies, Cookies } from 'react-cookie';

import 'react-datepicker/dist/react-datepicker.css';

class Header extends Component {

	  constructor (props) {
		    super(props)
		    this.state = {
		      startDate: moment(),
				showEnrollButton:'N',
				firstName: '',
				lastName: '',
				persons: [],
				jSessionID: ''
				
		    };

		    console.log("window.reactApp - " + JSON.stringify(window.reactApp.user.name));
		    if(this.reactApp) {
		    	this.setState({myuserInfo : window.reactApp });
		    }
		    
		    this.handleChange = this.handleChange.bind(this);
		  }
	  	  
//	  componentWillMount() {
//		    const { cookies } = this.props;
//		    
//		    this.setState({
//		    	jSessionID: cookies.get('JSESSIONID'),
//		    });
//		  }
	  
		componentDidMount(){
    		this.setState({
        		showEnrollButton: document.getElementById("showEnrollButton").value,
				firstName : document.getElementById("firstName").value,
				lastName : document.getElementById("lastName").value,
    		});
    	    axios.get(`https://jsonplaceholder.typicode.com/users`)
    	      .then(res => {
    	        const persons = res.data;
    	        this.setState({ persons });
    	      })
			}
		 
		  handleChange(date) {
		    this.setState({
		      startDate: date
		    });
		  }
		  
  render() {
	console.log("this.props.sayHello - " + this.props.welcome) ; 
    return (
      <div>
        <h1>My First React App!</h1>
        
        <ul>
        	{ this.state.persons.map(person => <li>{person.name}</li>)}
        </ul>
        
        <SketchPicker />
        <div><strong>User Info - {this.props.user.name}</strong></div>
        
		<div><strong>Show Enroll Button - {this.state.showEnrollButton}</strong></div>
		<div><strong>First Name - {this.state.firstName}</strong></div>
		<div><strong>Last Name - {this.state.lastName}</strong></div>
		<button onClick={() => this.props.welcome('hello')}>Say Hello</button>
		<div><strong>JSESSIONID - {this.state.jSessionID}</strong></div>
		<div><strong>myuserInfo - {window.reactApp.user.email}</strong></div>
        <DatePicker
        selected={this.state.startDate}
        onChange={this.handleChange}
    />
      </div>
    );
  }
}

export default Header;