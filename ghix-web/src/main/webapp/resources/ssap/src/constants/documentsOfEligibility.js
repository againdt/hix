
// Constants

import INPUT_TYPES from '../constants/inputTypes';
import VALIDATION_TYPES from '../constants/validationTypes';
import { VALIDATION_ERROR_MESSAGES } from '../constants/validationErrorMessages';

const DETAILS_INPUTS = {
    alienNumber: {
        name: 'alienNumber',
        type: INPUT_TYPES.REGULAR_INPUT,
        text: 'Alien Number',
        isInputRequired: true,
        placeholder: 'Alien Number',
        validationType: VALIDATION_TYPES.ALIEN_NUMBER
    },
    cardNumber: {
        name: 'cardNumber',
        type: INPUT_TYPES.REGULAR_INPUT,
        text: 'Card Number',
        isInputRequired: true,
        placeholder: 'Card Number',
        validationType: VALIDATION_TYPES.I_551
    },
    documentExpirationDate: {
        name: 'documentExpirationDate',
        type: INPUT_TYPES.DATE_PICKER,
        text: 'Passport Expiration Date',
        isInputRequired: false,
        placeholder: 'Passport Expiration Date',
        validationType: VALIDATION_TYPES.DATE
    },
    foreignPassportOrDocumentNumber: {
        name: 'foreignPassportOrDocumentNumber',
        type: INPUT_TYPES.REGULAR_INPUT,
        text: 'Passport Number',
        isInputRequired: true,
        placeholder: 'Passport Number',
        validationType: VALIDATION_TYPES.PASSPORT,
    },
    foreignPassportCountryOfIssuance: {
        name: 'foreignPassportCountryOfIssuance',
        type: INPUT_TYPES.DROPDOWN,
        text: 'Foreign Passport Country of Issuance',
        isInputRequired: true,
        options: [],
        validationType: VALIDATION_TYPES.CHOSEN,
        validationErrorMessage: VALIDATION_ERROR_MESSAGES.COUNTRY_OF_ISSUANCE
    },
    visaNumber: {
        name: 'visaNumber',
        type: INPUT_TYPES.REGULAR_INPUT,
        text: 'Visa Number',
        isInputRequired: false,
        placeholder: 'Visa Number',
        validationType: VALIDATION_TYPES.VISA
    },
    i94Number: {
        name: 'i94Number',
        type: INPUT_TYPES.REGULAR_INPUT,
        text: 'I-94 Number',
        isInputRequired: true,
        placeholder: 'I-94 Number',
        validationType: VALIDATION_TYPES.I_94
    },
    sevisId: {
        name: 'sevisId',
        type: INPUT_TYPES.REGULAR_INPUT,
        text: 'SEVIS ID Number',
        isInputRequired: false,
        placeholder: 'SEVIS ID Number',
        validationType: VALIDATION_TYPES.SEVIS_ID
    },
    documentDescription: {
        name: 'documentDescription',
        type: INPUT_TYPES.REGULAR_INPUT,
        text: 'Document description',
        isInputRequired: true,
        placeholder: 'Document description',
        validationType: VALIDATION_TYPES.DOCUMENT_DESCRIPTION
    }
};

const documentsOfEligibility = [
    {
        name: 'I551Indicator',
        text: 'Permanent Resident Card “Green Card”, I−551',
        details: [
            {...DETAILS_INPUTS.alienNumber},
            {...DETAILS_INPUTS.cardNumber},
            {
                ...DETAILS_INPUTS.documentExpirationDate,
                text: 'Expiration Date'
            }
        ]
    },
    {
        name: 'TemporaryI551StampIndicator',
        text: 'Temporary I−551 Stamp (on passport or I−94, I−94A)',
        details: [
            {...DETAILS_INPUTS.alienNumber},
            {...DETAILS_INPUTS.foreignPassportOrDocumentNumber},
            {...DETAILS_INPUTS.foreignPassportCountryOfIssuance},
            {...DETAILS_INPUTS.documentExpirationDate}
        ]
    },
    {
        name: 'MachineReadableVisaIndicator',
        text: 'Machine Readable Immigrant Visa (With Temporary I-551 Language)',
        details: [
            {...DETAILS_INPUTS.alienNumber},
            {...DETAILS_INPUTS.foreignPassportOrDocumentNumber},
            {...DETAILS_INPUTS.foreignPassportCountryOfIssuance},
            {...DETAILS_INPUTS.documentExpirationDate},
            {...DETAILS_INPUTS.visaNumber}
        ]
    },
    {
        name: 'I766Indicator',
        text: 'Employment Authorization Card (EAD, I-766)',
        details: [
            {
                ...DETAILS_INPUTS.alienNumber,
                validationType: VALIDATION_TYPES.ALIEN_NUMBER
            },
            {...DETAILS_INPUTS.cardNumber},
            {
                ...DETAILS_INPUTS.documentExpirationDate,
                isInputRequired: true,
                text: 'Expiration Date'
            }
        ]
    },
    {
        name: 'I94Indicator',
        text: 'Arrival/Departure Record (I-94, I-94A)',
        details: [
            {...DETAILS_INPUTS.i94Number},
            {...DETAILS_INPUTS.sevisId},
            {...DETAILS_INPUTS.documentExpirationDate}
        ]
    },
    {
        name: 'I94InPassportIndicator',
        text: 'Arrival/Departure Record in Foreign Passport (I-94)',
        details: [
            {...DETAILS_INPUTS.i94Number},
            {...DETAILS_INPUTS.foreignPassportOrDocumentNumber},
            {...DETAILS_INPUTS.foreignPassportCountryOfIssuance},
            {
                ...DETAILS_INPUTS.documentExpirationDate,
                isInputRequired: true
            },
            {...DETAILS_INPUTS.visaNumber},
            {...DETAILS_INPUTS.sevisId}
        ]
    },
    {
        name: 'UnexpiredForeignPassportIndicator',
        text: 'Foreign Passport',
        details: [
            {
                ...DETAILS_INPUTS.i94Number,
                isInputRequired: false
            },
            {...DETAILS_INPUTS.foreignPassportOrDocumentNumber},
            {...DETAILS_INPUTS.foreignPassportCountryOfIssuance},
            {
                ...DETAILS_INPUTS.documentExpirationDate,
                isInputRequired: true
            },
            {...DETAILS_INPUTS.sevisId}
        ]
    },
    {
        name: 'I327Indicator',
        text: 'Reentry Permit (I-327)',
        details: [
            {...DETAILS_INPUTS.alienNumber},
            {
                ...DETAILS_INPUTS.documentExpirationDate,
                text: 'Expiration Date'
            }
        ]
    },
    {
        name:'I571Indicator',
        text: 'Refugee Travel Document (I-571)',
        details: [
            {...DETAILS_INPUTS.alienNumber},
            {
                ...DETAILS_INPUTS.documentExpirationDate,
                text: 'Expiration Date'
            }
        ]
    },
    {
        name: 'I20Indicator',
        text: 'Certificate of Eligibility for Nonimmigrant (F-1) Student Status (I-20)',
        details: [
            {
                ...DETAILS_INPUTS.sevisId,
                isInputRequired: true
            },
            {
                ...DETAILS_INPUTS.i94Number,
                isInputRequired: false
            },
            {...DETAILS_INPUTS.foreignPassportOrDocumentNumber},
            {...DETAILS_INPUTS.foreignPassportCountryOfIssuance},
            {...DETAILS_INPUTS.documentExpirationDate}
        ]
    },
    {
        name: 'DS2019Indicator',
        text: 'Certificate of Eligibility for Exchange Visitor (J-1) Status (DS2019)',
        details: [
            {
                ...DETAILS_INPUTS.sevisId,
                isInputRequired: true
            },
            {
                ...DETAILS_INPUTS.i94Number,
                isInputRequired: false
            },
            {
                ...DETAILS_INPUTS.foreignPassportOrDocumentNumber,
                isInputRequired: false
            },
            {...DETAILS_INPUTS.documentExpirationDate}
        ]
    },
    {
        name: 'I797Indicator',
        text: 'Notice of Action, I-797',
        details: []
    },
    {
        name: 'OtherDocumentTypeIndicator',
        text: 'Other status',
        details: [
            {
                ...DETAILS_INPUTS.alienNumber,
                isInputRequired: false
            },
            {
                ...DETAILS_INPUTS.foreignPassportOrDocumentNumber,
                isInputRequired: false
            },
            { ...DETAILS_INPUTS.sevisId },
            {
                ...DETAILS_INPUTS.documentExpirationDate ,
                isInputRequired: false
            },
            { ...DETAILS_INPUTS.documentDescription }
        ]
    },
    {
        name: 'noneOfThese',
        text: 'None of these',
        details: []
    }
];

export default documentsOfEligibility;