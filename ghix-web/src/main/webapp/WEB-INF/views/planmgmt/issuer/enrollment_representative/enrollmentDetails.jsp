<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>

<script type="text/javascript" src="<gi:cdnurl value="/resources/js/preserveImgAspectRatio.js" />"></script>

<%@ page isELIgnored="false"%>
<div class="row-fluid">
	<div class="span3" id="sidebar">
        <a href='<c:url value="/planmgmt/issuer/enrollmentrep/enrollments" />' class="btn btn-primary"><spring:message code='label.backtoEnrollmentList'/></a>       
	</div>
	<!-- end of span3 -->
	<div class="span9" id="rightpanel">	
		<div class="well-step enrollment-history">
		    <div class="row-fluid">
		      <div class="span5 center">
		      	<img src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>
		        <p><small>${issuerObj.name}</small></p>
		      </div>
		
		 	  <div class="span7">
		          <h5 class="center">${fn:toUpperCase(enrollmentDataDTO.planName)}</h5>
		      </div>
		    </div>
			 
			<div class="row-fluid margin10-t">
		      <table class="table table-condensed span5">
		        <tr>
		          <th><spring:message code='label.enrollmentStatus'/>:</th>
		          <td>${enrollmentDataDTO.enrollmentStatus}</td>
		        </tr>
		        <tr>
		          <th><spring:message code='label.confirmationDate'/>:</th>
		          <td>${enrollmentDataDTO.enrollmentConfirmationDate}</td>
		        </tr>
		      </table>
		       
		      <table class="table table-condensed span7">
		        <tr>
		          <th><spring:message code='label.effectiveDate'/>:</th> 
		          <td>
		          <fmt:formatDate type="DATE" value="${enrollmentDataDTO.enrollmentBenefitEffectiveStartDate}" pattern="MM/dd/yyyy" />	          	 
		          	 - 
		          <fmt:formatDate type="DATE" value="${enrollmentDataDTO.enrollmentBenefitEffectiveEndDate}" pattern="MM/dd/yyyy" />	 
		          </td>
		        </tr>
		        <tr>
		        <td><a href="#" id="viewGrid"><spring:message code='label.premiumandAPTCGrid'/></a></td>
		        </tr>
		      </table>
			</div>
			 
		    <div class="row-fluid">
		      <table class="table table-condensed span5">
		        <caption class="table-caption"><spring:message code='label.monthlyPayment'/></caption>
		        <tr>
		          <th><spring:message code='label.premiumAmount'/>:</th>
		          <td>$<fmt:formatNumber type="number" minFractionDigits="2" value="${enrollmentDataDTO.grossPremiumAmt}"/></td>    
		        </tr> 
		          
				<tr>
		          <th><spring:message code='label.electedAPTCAmount'/>:</th>
		          <td><c:if test="${enrollmentDataDTO.aptcAmount != null}">$<fmt:formatNumber type="number" minFractionDigits="2" value="${enrollmentDataDTO.aptcAmount}" /></c:if></td>
		        </tr>
                  <tr>
                      <th><c:if test="${isStateSubsidyEnabled && enrollmentDataDTO.stateSubsidyAmount != null}"><spring:message code='label.electedSSAmount'/>:</c:if></th>
                      <td><c:if test="${isStateSubsidyEnabled && enrollmentDataDTO.stateSubsidyAmount != null}">$<fmt:formatNumber type="number" minFractionDigits="2" value="${enrollmentDataDTO.stateSubsidyAmount}" /></c:if></td>
                  </tr>
				<tr>
				  <th><spring:message code='label.netPremium'/>:</th>
				  <td>$<fmt:formatNumber type="number" minFractionDigits="2" value="${enrollmentDataDTO.netPremiumAmt}" /></td> 
		        </tr>               
		      </table>
		
		      <table class="table table-condensed span7">
		        <caption class="table-caption"><spring:message code='label.enrollmentIDs'/></caption>
		        <tr><th><spring:message code='label.exchangeAssignedPolicyID'/>:</th><td>${enrollmentDataDTO.enrollmentId}</td></tr>        
		        <tr><th><spring:message code='label.transactionID'/>:</th><td>${enrollmentDataDTO.transactionId}</td></tr>      
		        <tr><th><spring:message code='label.planHIOSID'/>:</th><td>${enrollmentDataDTO.CMSPlanID}</td></tr>     
		      </table>
		    </div>
			
			 <div class="row-fluid">
			    <table class="table table-condensed span5">
			      <tr>
			        <th><spring:message code='label.submittedDate'/>:</th> <td> <fmt:formatDate type="DATE" value="${enrollmentDataDTO.creationTimestamp}" pattern="MM/dd/yyyy" /></td><td></td>
			      </tr>    
			      <tr>
			        <th><spring:message code='label.APTCEffectiveDate'/>:</th> <td> <fmt:formatDate type="DATE" value="${enrollmentDataDTO.aptcEffectuveDate}" pattern="MM/dd/yyyy" /></td><td></td>
			      </tr>    
			      <tr>
			        <th><spring:message code='label.lastUpdateDate'/>:</th> <td> <fmt:formatDate type="DATE" value="${enrollmentDataDTO.lastUpdateDate}" pattern="MM/dd/yyyy" /></td>
			       <td></td>
			      </tr>       
			    </table>
			</div>
			    
		    <div id="enrollee" >
		    	<c:choose>
	            	<c:when test="${fn:length(enrollmentDataDTO.enrolleeDataDtoList) > 0}">
	            		<c:set var="selfCtr" value="0"/>
	            		<c:set var="spouseCtr" value="0"/>
	            		<c:set var="dependentCtr" value="0"/>
	            		<c:forEach items="${enrollmentDataDTO.enrolleeDataDtoList}" var="enrollee">
	            			<c:if test="${fn:toUpperCase(enrollee.relationshipToSubscriber) == 'SELF'}">
	            				<c:set var="selfCtr" value="${selfCtr +1}"/>
	            			</c:if>
	            			<c:if test="${fn:toUpperCase(enrollee.relationshipToSubscriber) == 'SPOUSE'}">
	            				<c:set var="spouseCtr" value="${spouseCtr +1}"/>
	            			</c:if>
	            			<c:if test="${fn:toUpperCase(enrollee.relationshipToSubscriber) != 'SELF' && fn:toUpperCase(enrollee.relationshipToSubscriber) != 'SPOUSE'}">
	            				<c:set var="dependentCtr" value="${dependentCtr +1}"/>
	            			</c:if>
	            		</c:forEach>	 	
				    	<h5><spring:message code='label.enrollees'/> ( ${selfCtr} <spring:message code='label.primary'/>, ${spouseCtr} <spring:message code='label.spouse'/>, ${dependentCtr} <spring:message code='label.dependent'/> )</h5>
						<table class="table table-condensed">
						  <thead>
				            <tr>
				              <th><spring:message code='label.type'/></th>
				              <th><spring:message code='label.nameOnly'/></th>
				              <th><spring:message code='label.gender'/></th>
				              <th><spring:message code='label.DOB'/></th>
				              <th><spring:message code='label.benefitEffectiveDate'/></th>
				              <th><spring:message code='label.memberId'/></th>
				            </tr>
				          </thead>
				
				          <tbody>
				          	 <c:forEach items="${enrollmentDataDTO.enrolleeDataDtoList}" var="enrollee">
				          	 <tr>
					              <td>
					              		${enrollee.relationshipToSubscriber}
					              </td>
					              <td>
					              		${enrollee.firstName} ${enrollee.middleName} ${enrollee.lastName}
					              </td>
					              <td>
					              		${enrollee.genderLookupLabel}
					              </td>
					              <td>
					              		<fmt:formatDate type="DATE" value="${enrollee.birthDate}" pattern="MM/dd/yyyy" />
					              </td>
					              <td>
					              	<fmt:formatDate type="DATE" value="${enrollee.enrolleeEffectiveStartDate}" pattern="MM/dd/yyyy" />
						             	- 
					              	<fmt:formatDate type="DATE" value="${enrollee.enrolleeEffectiveEndDate}" pattern="MM/dd/yyyy" />
					              </td>
					              <td>
					              		${enrollee.memberId}
					              </td>             
				             </tr>
				             </c:forEach>
				          </tbody>
				      	</table>
		      		</c:when>
				</c:choose>
		   </div>
		</div> <!-- end enrollment data -->
	</div> <!-- end right panel -->
</div> 


	<div id="premiumAndAPTCGrid" class="modal hide fade in" style="display:none;">
	  	<div class="modal-header"><spring:message code='label.premiumandAPTCGrid'/>
	    	<a id="closeEnrollmentDialog" class="close" data-dismiss="modal" data-original-title="">x</a>
	        <span id="planIdCountEnroll"></span> 
	    </div>
	    <div class="modal-body">
	       <table class="table table-condensed">
	       		<tr>
	       			<th><spring:message code='label.month'/></th>
	       			<th><spring:message code='label.grossPremium'/></th>
	       			<th><spring:message code='label.APTC'/></th>
	       			<th><spring:message code='label.netPremium'/></th>
                    <th><c:if test="${isStateSubsidyEnabled}"><spring:message code='label.CAPS'/></c:if></th>
	       			<th><spring:message code='label.applicationType'/></th>
	       		</tr>
	       		<c:choose>
	       			<c:when test="${fn:length(enrollmentDataDTO.enrollmentPremiumDtoList) > 0}">
		       			<c:forEach items="${enrollmentDataDTO.enrollmentPremiumDtoList}" var="enrollmentPremium">	
			          	<tr>
			            	<td>
			            	<c:choose>
			            		<c:when test="${enrollmentPremium.month == 1}">January</c:when>
			            		<c:when test="${enrollmentPremium.month == 2}">February</c:when>
			            		<c:when test="${enrollmentPremium.month == 3}">March</c:when>
			            		<c:when test="${enrollmentPremium.month == 4}">April</c:when>
			            		<c:when test="${enrollmentPremium.month == 5}">May</c:when>
			            		<c:when test="${enrollmentPremium.month == 6}">June</c:when>
			            		<c:when test="${enrollmentPremium.month == 7}">July</c:when>
			            		<c:when test="${enrollmentPremium.month == 8}">August</c:when>
			            		<c:when test="${enrollmentPremium.month == 9}">September</c:when>
			            		<c:when test="${enrollmentPremium.month == 10}">October</c:when>
			            		<c:when test="${enrollmentPremium.month == 11}">November</c:when>
			            		<c:when test="${enrollmentPremium.month == 12}">December</c:when>
			            	</c:choose>
			            	</td>
			            	<td><c:if test="${enrollmentPremium.grossPremiumAmount != null }">$<fmt:formatNumber type="number" minFractionDigits="2" value="${enrollmentPremium.grossPremiumAmount}" /></c:if></td>
			            	<td><c:if test="${enrollmentPremium.aptcAmount != null }">$<fmt:formatNumber type="number" minFractionDigits="2" value="${enrollmentPremium.aptcAmount}" /></c:if></td>
			            	<td><c:if test="${enrollmentPremium.netPremiumAmount != null }">$<fmt:formatNumber type="number" minFractionDigits="2" value="${enrollmentPremium.netPremiumAmount}" /></c:if></td>
                            <td><c:if test="${isStateSubsidyEnabled && enrollmentPremium.stateSubsidyAmount != null}">$<fmt:formatNumber type="number" minFractionDigits="2" value="${enrollmentPremium.stateSubsidyAmount}" /></c:if></td>
			            	<td>
			            		<c:choose>
			            			<c:when test="${enrollmentPremium.financial == true}"><spring:message code='label.financial'/></c:when>
			            			<c:otherwise><spring:message code='label.nonFinancial'/></c:otherwise>
			            		</c:choose>
			            	</td>	
			            </tr>  
		           		</c:forEach>   
		        	</c:when>
	       		</c:choose>                              
	        </table>
	      </div>
	</div> 
	 	
<script type="text/javascript">

$(document).ready(function(){
	
	$("#viewGrid").click(function(){
		$("#premiumAndAPTCGrid").modal("show"); 
	});
});

</script>
