package com.getinsured.hix.serff.service.templates;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanDocumentsJob;
import com.getinsured.hix.serff.repository.templates.ISerffPlanDocumentsJobRepository;
import com.getinsured.hix.serff.repository.templates.ISerffPlanHealthRepository;
import com.getinsured.hix.serff.repository.templates.ISerffPlanRepository;

@Service("PlanDocumentsJobService")
@Repository
@Transactional
public class PlanDocumentsJobServiceImpl implements PlanDocumentsJobService {

	private static final Logger LOGGER = Logger
			.getLogger(PlanDocumentsJobServiceImpl.class);
	private static final String CLASS_NAME = "PlanDocumentsJobServiceImpl";

	@Autowired
	private ISerffPlanRepository iPlanRepository;

	@Autowired
	private ISerffPlanHealthRepository iPlanHealthRepository;

	@Autowired
	private ISerffPlanDocumentsJobRepository iPlanDocumentsJobRepository;

	@Override
	public int persistPlanDocumentRecord(PlanDocumentsJob documentJob) {
		LOGGER.debug(CLASS_NAME + ". Saving Plan Document Job. : "
				+ documentJob.getIssuerPlanNumber());
		PlanDocumentsJob documentJobInserted = iPlanDocumentsJobRepository
				.save(documentJob);
		LOGGER.debug(CLASS_NAME + ". Plan Document Job id : "
				+ documentJobInserted.getId());
		return documentJobInserted.getId();
	}

	@Override
	public Long getPlanCount(@Param("planId") String planId) {
		LOGGER.debug(CLASS_NAME + ". Getting Plan Count by planissuerNumber."
				+ planId);
		return iPlanRepository.getPlanCount(planId);
	}

	@Override
	public Long getPlanHealthCount(@Param("planId") String planId) {
		LOGGER.debug(CLASS_NAME
				+ ". Getting Plan Helath Count by planissuerNumber." + planId);
		return iPlanHealthRepository.getPlanHealthCount(planId);
	}

	@Override
	public void updatePlanDocuments(Map<String, String[]> planIdBrochureMap) {

		if (null != planIdBrochureMap) {
			for (String issuerPlanNumber : planIdBrochureMap.keySet()) {

				String[] planBrochureValues = planIdBrochureMap
						.get(issuerPlanNumber);

				LOGGER.debug(CLASS_NAME
						+ ". Issuer planNumber to Update in database : "
						+ issuerPlanNumber);
				List<Plan> planList = iPlanRepository
						.findByIsDeletedAndIssuerPlanNumberStartingWith(
								Plan.IS_DELETED.N.toString(), issuerPlanNumber);
				LOGGER.debug(CLASS_NAME
						+ ". Plan list fetched from database with size : "
						+ ((null != planList) ? planList.size() : null));
				LOGGER.debug(CLASS_NAME
						+ ". PlanBrochureValues : Brochure id - "
						+ planBrochureValues[0] + " , Brochure name - "
						+ planBrochureValues[1] + " , SBC id - "
						+ planBrochureValues[2] + " , SBC name - "
						+ planBrochureValues[3]);

				if (null != planList) {
					for (Plan plan : planList) {					
							if (null != planBrochureValues[0]
									&& null != planBrochureValues[1]) {
								plan.setBrochureUCmId(planBrochureValues[0]);
								LOGGER.debug(CLASS_NAME
										+ ". Setting brochure id "
										+ planBrochureValues[0]
										+ " for plan with id : " + plan.getId());
								plan.setBrochureDocName(planBrochureValues[1]);
								LOGGER.debug(CLASS_NAME
										+ ". Setting brochure name "
										+ planBrochureValues[1]
										+ " for plan with id : " + plan.getId());
							}
							
							//Null check for plan health object.	
							if (null != plan.getPlanHealth()) {
								if (null != planBrochureValues[2]
										&& null != planBrochureValues[3]) {
									plan.getPlanHealth().setSbcUcmId(
											planBrochureValues[2]);
									LOGGER.debug(CLASS_NAME
											+ ". Setting sbc id for plan "
											+ planBrochureValues[2]
											+ " for plan with id : " + plan.getId());
									plan.getPlanHealth().setSbcDocName(
											planBrochureValues[3]);
									LOGGER.debug(CLASS_NAME
											+ ". Setting sbc name for plan  "
											+ planBrochureValues[3]
											+ " for plan with id : " + plan.getId());
								}
							}
						}					
					}

				// Save whole updated list.
				iPlanRepository.save(planList);
			}
		}

	}

}
