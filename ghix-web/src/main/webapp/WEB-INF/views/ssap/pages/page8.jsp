	<%@ include file="../include.jsp" %>
<div id="appscr58"  style="display:none;">
	<div class="row-fluid">
		<div class="page-title">
			<h5 class="titleForPages"><spring:message code="ssap.page8.whoIsAppllyingForSummary" text="Who is Applying for Health Insurance Summary" /></h5>
		</div>
		<h4><spring:message code="ssap.page8.whoIsAppllyingForYouAre" text="You are applying for health insurance for these people" /></h4>
		
		<form method="POST" action="#" class="form-horizontal" data-parsley-excluded=":hidden">
			<div id="appscr58HouseholdForm">
				
			</div>
		</form>
		<div id="addAnotherIndividualButton">
			<a  href="javascript:showIndividualDiv();" class="btn btn-primary"><spring:message code="ssap.page8.addAnotherPerson" text="Add Another Person" /></a>
		</div> 
		
		<div id="modalDeleteApplicant" class="modal hide fade">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			  <h3><spring:message code="ssap.deleteApplicant.modalHeading" text="Delete Applicant" /></h3>
			</div>
			<div class="modal-body">
			  <p><spring:message code="ssap.deleteApplicant.confirmationMsg" text="Are you sure you want to remove this person from the application?" /></p>			        
			</div>
			<div class="modal-body">
			  <a href="#" data-dismiss="modal" class="btn btn-primary" id="btnDeleteApplicantNo" onClick="javascript:cancelDeleteApplicant()">CANCEL</a>
			  <a href="#" onClick="javascript:deleteApplicantConfirmed()" class="btn btn-primary" id="btnDeleteApplicantYes">YES</a>
			</div>
		</div>
		
	</div>
<div class="Main_ui_design">

           <!--   <div class="Hr_ui_design"><div class="Hr_ui_design1"></div> </div> --> 
           
          <div id="addAnotherIndividual" style="display:none">
          <div style="width:20px;height:100px"></div>
<div class="input_tetx_area">
<p class="cobaapp_title_ui"><spring:message code="ssap.page8.addIndividual" text="Add Individual" /> </p>
</div>
<div class="input_tetx_area">
<label class="cobaapp blackColorLbl" for="anotherIndividualFirstName">
<spring:message code="ssap.page8.addIndividualName" text="Name" />
<span class="mandatoryFields">*</span>
</label>
</div>
<div class="input_tetx_area">
<div class="input_page_auto">
<input id="anotherIndividualFirstName" name="anotherIndividualFirstName" class="input_row_area" type="text" placeholder="First Name" onclick="removeValidationFirstName(1)" onkeyup="removeValidationFirstName(1); setName(1)">
<label for="anotherIndividualMiddleName" class="aria-hidden"><spring:message code="ssap.page8.addIndividualName" text="Name" /></label>
<input id="anotherIndividualMiddleName" name="anotherIndividualMiddleName" class="input_row_area" type="text" placeholder="Middle Name" onclick="removeValidationLastName(1)" onkeyup="removeValidationLastName(1); setName(1)">
<label for="anotherIndividualLastName" class="aria-hidden"><spring:message code="ssap.page8.addIndividualName" text="Name" /></label>
<input id="anotherIndividualLastName" name= "anotherIndividualLastName" class="input_row_area" type="text" placeholder="Last Name" onkeyup="setName(1)">
<select id="anotherIndividualSuffix" class="select_row_options" name="anotherIndividualSuffix" aria-label="Individual Name Suffix">
				<option value=""><spring:message code="ssap.suffix" text="Suffix" /></option>
				<option value="Jr."><spring:message code="ssap.JR" text="Jr." /></option>
				<option value="Sr."><spring:message code="ssap.SR" text="Sr." /></option>
				<option value="III"><spring:message code="ssap.III" text="III" /></option>
				<option value="IV"><spring:message code="ssap.IV" text="IV" /></option>
</select>
</div>
</div>
<div class="input_tetx_area">
<label class="cobaapp blackColorLbl" for="anotherIndividualDOB">
<spring:message code="ssap.dateOfBirth" text="Date of Birth" />
<span class="mandatoryFields">*</span>
</label>
</div>
<div class="input_tetx_area">
<div class="input_page_auto">
<input id="anotherIndividualDOB" name="anotherIndividualDOB" class="input_row_area" type="text" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" onclick="removeValidationDob(1)" onkeyup="removeValidationDob(1)">
</div>
</div>

<div class="control-group">
	<fieldset>
		<legend class="Heading61">
			<spring:message code="ssap.page8.addIndividualSex" text="Sex :" />
		</legend>
	<div class="value61">
	<label class="radio inline" for="genderMale">
		<input type="radio" name="anotherIndividualgender" id="genderMale" checked="checked" value="male"><spring:message code="ssap.page8.addIndividualMale" text="Male" />
	</label>
	<label class="radio inline" for="appscr61GenderFemale">
		<input type="radio"  name="anotherIndividualgender" id="appscr61GenderFemale" value="female"><spring:message code="ssap.page8.addIndividualFemale" text="Female" />
	</label>
	</div>
	</fieldset>
</div>

<div class="input_tetx_area control-group">

<fieldset>
<legend class="cobaapp blackColorLbl">
Has
<span id="anotherIndividualName" class="applicantName57Bold"><spring:message code="ssap.page8.addIndividualTestPerson" text="Test Person" /></span>
<spring:message code="ssap.page8.addIndividualTabacco" text="used tobacco products in the last 12 months?" />
</legend>
<div class="isTobaccoused">
<label for="anotherIndividualTobaccoYes" class="radio inline">
	<input id="anotherIndividualTobaccoYes" type="radio" value="yes" name="anotherIndividualTobacco">
<spring:message code="ssap.page8.addIndividualTabaccoYes" text="Yes" />
</label>
<label for="anotherIndividualTobaccoNo" class="radio inline">
<input id="anotherIndividualTobaccoNo" type="radio" value="no" checked="checked" name="anotherIndividualTobacco">
<spring:message code="ssap.page8.addIndividualTabaccoNo" text="No" />
</label>
</div>
</fieldset>
</div>

<div class="addIndividualButtonStyle">
<a style="color:white;text-decoration:none" href="javascript:addAnotherIndividual();"><spring:message code="ssap.page8.addIndividual" text="Add Individual" /></a>
</div>
</div> 

</div>


</div>


<div id="coverageMinimumAlert" class="modal hide fade" tabindex="-1" role="dialog">
	<div class="modal-header">
	</div>
	<div class="modal-body">
		<p><spring:message code="ssap.coverageMinimum.atLeastOneApplicant"/></p>
		<p><spring:message code="ssap.coverageMinimum.clickCancel"/></p>
		<p><spring:message code="ssap.coverageMinimum.clickDashboard"/></p>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="ssap.coverageMinimum.close"/></button>
		<button class="btn btn-primary" onclick="saveData();goToDashboard();"><spring:message code="ssap.coverageMinimum.dashboard"/></button>
	</div>
</div>