package com.getinsured.hix.batch.provider;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

public class ProviderRecordWriter implements ItemWriter<SolrInputDocument> {
	private static final Logger LOGGER = Logger.getLogger(ProviderRecordWriter.class);
	private int port=8080;
	@Autowired private HttpSolrServer solrServer;
	private HttpSolrServer solrServerToUse;
	private String paramSolrURL;
	private String providerCore;

	public HttpSolrServer getSolrServer() {
		return solrServer;
	}

	public void setSolrServer(HttpSolrServer solrServer) {
		this.solrServer = solrServer;
	}

	public String getParamSolrURL() {
		return paramSolrURL;
	}

	public void setParamSolrURL(String paramSolrURL) {
		this.paramSolrURL = paramSolrURL;
	}

	public String getProviderCore() {
		return providerCore;
	}

	public void setProviderCore(String providerCore) {
		this.providerCore = providerCore;
	}

	public ProviderRecordWriter() {
		LOGGER.debug("Invoked ProviderRecordWriter()");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void write(List<? extends SolrInputDocument> recordList) throws Exception {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Record List is empty: " + CollectionUtils.isEmpty(recordList));
			LOGGER.debug("Parameter Solr Server URL: " + paramSolrURL);
			LOGGER.debug("Provider Core: " + providerCore);
		}

		if (null == solrServerToUse) {

			if (StringUtils.isNotBlank(paramSolrURL) && StringUtils.isNotBlank(providerCore)) {
				solrServerToUse = new HttpSolrServer(paramSolrURL + providerCore);
			}
			else {
				solrServerToUse = solrServer;
			}

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("SOLR Server URL To Use " + solrServerToUse.getBaseURL());
			}
		}
		solrServerToUse.add((Collection<SolrInputDocument>) recordList);
		LOGGER.debug("Solr commit");
		solrServerToUse.commit();
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
