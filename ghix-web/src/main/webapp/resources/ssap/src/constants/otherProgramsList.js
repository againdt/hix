export const OTHER_PROGRAMS_LIST =  [
    {
        'name': 'CHIP',
        'type': 'CHIP',
        'eligible': false
    },
    {
        'name': 'COBRA Coverage',
        'type': 'COBRA_COVERAGE',
        'eligible': false
    },
    {
        'name': 'Medicaid',
        'type': 'MEDICAID',
        'eligible': false
    },
    {
        'name': 'Medicare',
        'type':'MEDICARE',
        'eligible': false,
    },
    {
        'name': 'Peace Corps',
        'type': 'PEACE_CORPS',
        'eligible': false
    },
    {
        'name': 'Retire Health Benefits',
        'type': 'RETIRE_HEALTH_BENEFITS',
        'eligible': false
    },
    {
        'name': 'TRICARE',
        'type': 'TRICARE',
        'eligible': false
    },
    {
        'name': 'Veterans Affairs (VA) Health Care Program',
        'type': 'VETERANS_AFFAIRS_HCP',
        'eligible': false
    },
    {
        'name': 'Other Coverage',
        'type': 'OTHER_COVERAGE',
        'eligible': false
    },
    {
        'name': 'None Of the Above',
        'type': 'NONE',
        'eligible': false
    }
];