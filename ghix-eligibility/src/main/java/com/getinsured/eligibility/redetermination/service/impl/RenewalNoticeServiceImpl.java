package com.getinsured.eligibility.redetermination.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.redetermination.service.RenewalNoticeService;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.google.gson.Gson;

@Service
public class RenewalNoticeServiceImpl implements RenewalNoticeService {
	
	private static final String EXCHANGE_FULL_NAME = "exchangeFullName";	
	private static final String STATE_NAME = "exgStateName";
	private static final String COUNTRY_NAME = "countryName";
	private static final String EXCHANGE_URL = "exchangeURL";
	private static final String EXCHANGE_PHONE = "exchangePhone";
	private static final String EXCHANGE_NAME = "exgName";
	private static final String EXCHANGE_ADDRESS_ONE = "exchangeAddress1";
	private static final String EXCHANGE_ADDRESS_TWO = "exchangeAddress2";
	private static final String EXCHANGE_CITY_NAME = "exgCityName";
	private static final String EXCHANGE_ZIP = "zip";
	private static final String PRIMARY_APPLICANT_NAME = "primaryApplicantName";
	private static final String EXCHANGE_ADDRESS_EMAIL = "exchangeAddressEmail";
	private static final String USER_NAME = "userName";
	private static final String SPECIAL_ENROLLMENT_END_DATE = "SpecialEnrollmentEndDate";
	private static final String DATE_FORMAT_FOR_NOTICE ="MMM dd, yyyy";
	private static final String EMPTY_STRING="";
	private static final String ES = "ES";
	private static final String LOCAL_ES = "es";
	private static final String DATE_FORMAT = "MMMM dd, YYYY";
	private static final String SPANISH_DATE = "spanishDate";
	private static final String ENGLISH_DATE = "englishDate";
	private static final String TODAYS_DATE = "todaysDate";
	
	private static final String CURRENT_YEAR = "currentYear";
	private static final String CURRENT_COVERAGE_YEAR = "currentCoverageYear";
	private static final String PREVIOUS_COVERAGE_YEAR = "previousCoverageYear";
	private static final String ANNONYMOUS_SHOOPING_START_DATE = "annonymousShoopingStartDate";
	private static final String EFFECTIVE_DATE = "effectiveDate";
	private static final String MMMM_D_YYYY = "MMMM d, yyyy";
	private static final Logger LOGGER = LoggerFactory.getLogger(RenewalNoticeServiceImpl.class);

	@Autowired private Gson platformGson;
	@Autowired
	SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	PreferencesService preferencesService;
	@Autowired
	SsapCloneApplicationService ssapCloneApplicationService;
	@Autowired
	GhixRestTemplate ghixRestTemplate;
	@Autowired
	NoticeService noticeService;
	
	
	@Override
	public String triggerRenewalNotice(String caseNumber, String templateName, String fileName) {
		List<SsapApplication> applications = ssapApplicationRepository.findByCaseNumber(caseNumber);
		if (applications != null && applications.size() > 0) {
			return generate(applications.get(0),templateName,fileName);
		}
		
		return null;
	}

	private String generate(SsapApplication ssapApplication, String templateName, String fileName) {
		LOGGER.info("Sending Notification : Starts");
		Map<String, Object> individualTemplateData = new HashMap<String, Object>();
		List<String> sendToEmailList = new ArrayList<String>();;
		DateFormat dateFormat = null;
		DateFormat dateFormatForSpanish = null;
		int cmrHousehold = ssapApplication.getCmrHouseoldId().intValue();
		String relativePath = "cmr/"+cmrHousehold+"/";
		try {
			individualTemplateData.put(LifeChangeEventConstant.NOTIFICATION_SEND_DATE, new TSDate());
			individualTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);
			individualTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
			individualTemplateData.put(STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
			individualTemplateData.put(COUNTRY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
			individualTemplateData.put(EXCHANGE_URL, GhixEndPoints.GHIXWEB_SERVICE_URL);			
			individualTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			individualTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			
			individualTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
			individualTemplateData.put(EXCHANGE_ADDRESS_TWO , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_2));
			individualTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
			individualTemplateData.put(EXCHANGE_ZIP , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
			
			//initialize the date formatter
			dateFormat = new SimpleDateFormat(DATE_FORMAT);
			dateFormatForSpanish = new SimpleDateFormat(DATE_FORMAT,new Locale(LOCAL_ES, ES));
			//set English and Spanish date into template data
			Date todaysDate = new TSDate(); 
			individualTemplateData.put(ENGLISH_DATE ,dateFormat.format(todaysDate));			
			individualTemplateData.put(TODAYS_DATE ,dateFormat.format(todaysDate));			
			individualTemplateData.put(SPANISH_DATE ,dateFormatForSpanish.format(todaysDate));			
			
			//get name of primary applicant
			String name = getName(ssapApplication);
			individualTemplateData.put(PRIMARY_APPLICANT_NAME , name);
			individualTemplateData.put(USER_NAME , name);
			individualTemplateData.put(EXCHANGE_ADDRESS_EMAIL , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
			individualTemplateData.put(SPECIAL_ENROLLMENT_END_DATE, getSepEndDate());
			if("Renewal Notice".equalsIgnoreCase(templateName)){
				individualTemplateData.put(EFFECTIVE_DATE, getCoverageDateForNotice(ssapApplication));
			}else{
				individualTemplateData.put(EFFECTIVE_DATE, "");
			}
			
			individualTemplateData.put(ANNONYMOUS_SHOOPING_START_DATE,getShoppingDate());
			individualTemplateData.put(PREVIOUS_COVERAGE_YEAR,DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR));
			individualTemplateData.put(CURRENT_COVERAGE_YEAR,DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
			Calendar cal = TSCalendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			individualTemplateData.put(CURRENT_YEAR,String.valueOf(year));
			individualTemplateData.put("exchangeFAX", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			//Populating Household details from Preferences
			Location location = new Location();
			PreferencesDTO preferencesDTO = preferencesService.getPreferences(cmrHousehold, false);
			location = getLocationFromDTO(preferencesDTO.getLocationDto());
			String emailId = preferencesDTO.getEmailAddress();
			sendToEmailList.add(emailId);
			
			individualTemplateData.put("ssapApplicationId", ssapApplication.getId());
			
			fileName = fileName + TimeShifterUtil.currentTimeMillis() + LifeChangeEventConstant.PDF;
			LOGGER.info("Sending "  + templateName + "for household " + cmrHousehold); 
			Notice notice = noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, individualTemplateData, relativePath, fileName,
					LifeChangeEventConstant.INDIVIDUAL,cmrHousehold, sendToEmailList, individualTemplateData.get(EXCHANGE_FULL_NAME) + "", name ,location , preferencesDTO.getPrefCommunication());
			return notice.getEcmId();
		} catch(Exception exception){
			LOGGER.error("Error sending notification for " + templateName + " for household " + cmrHousehold, exception);
		}
		
		LOGGER.info("Sending Notification : Ends");
		return null;
		
	}
	
	private String getCoverageDateForNotice(SsapApplication ssapApplication) {
		Date coverageStartDate = null;
		try {
			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentRequest.setSsapApplicationId(ssapApplication.getId());
			ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.ENROLLMENT_URL+"enrollment/getenrollmentdatabyssapapplicationid", "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest));
			EnrollmentResponse enrollmentResponse = (EnrollmentResponse) platformGson.fromJson(response.getBody(), EnrollmentResponse.class);
			coverageStartDate = getBenefitStartDate(enrollmentResponse);
			if(coverageStartDate != null){
				DateFormat df = new SimpleDateFormat(MMMM_D_YYYY);
				return df.format(coverageStartDate);
			}
		} catch (Exception e) {
			LOGGER.error("Error occured while fetching enrollment for application.");
			throw new GIRuntimeException("Exception occuered while fetching enrollment details", e);
		}
		return null;
	}

	
	private Date getBenefitStartDate(EnrollmentResponse enrollmentResponse){
		List<EnrollmentDataDTO> enrollmentDataDTOs = enrollmentResponse.getEnrollmentDataDTOList();
		Set<String> validEnrollmentStatus = new HashSet<String>();
		validEnrollmentStatus.add("CONFIRM");
		validEnrollmentStatus.add("PENDING");
		validEnrollmentStatus.add("TERM");
		List<EnrollmentDataDTO> filteredEnrollment = new ArrayList<EnrollmentDataDTO>();
		EnrollmentDataDTO enrollment = null;
		//sort to consider latest enrollments for age out.
		Comparator<EnrollmentDataDTO> cmp = new Comparator<EnrollmentDataDTO>(){
		     public int compare(EnrollmentDataDTO enrollment1, EnrollmentDataDTO enrollment2){
		          return enrollment2.getCreationTimestamp().compareTo(enrollment1.getCreationTimestamp());
		     }
		};
		
		
		
		//filter health and dental enrollment
		for(EnrollmentDataDTO enrollmentDataDTO : enrollmentDataDTOs){
			if(validEnrollmentStatus.contains(enrollmentDataDTO.getEnrollmentStatus())){
				filteredEnrollment.add(enrollmentDataDTO);
			}
		}
		
		if(!filteredEnrollment.isEmpty() ){
			if(filteredEnrollment.size()>1){
				Collections.sort(filteredEnrollment,cmp);
			}
			
			if(filteredEnrollment.get(0).getEnrollmentBenefitEffectiveEndDate().after(new TSDate())){
				enrollment = filteredEnrollment.get(0);
				return enrollment.getEnrollmentBenefitEffectiveStartDate();
			}
				
			
			
		}
		
		return null; 
	}
	
	public Location getLocationFromDTO(LocationDTO locationDto){
		if(locationDto == null){
			return null;
		}
		Location l = new Location();
		l.setAddress1(locationDto.getAddressLine1());
		l.setAddress2(locationDto.getAddressLine2());
		l.setCity(locationDto.getCity());
		l.setState(locationDto.getState());
		l.setZip(locationDto.getZipcode());
		l.setCounty(locationDto.getCountyName());
		l.setCountycode(locationDto.getCountyCode());
		return l;
	}
	
	private String getName(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getFirstName() + " " + ssapApplicant.getLastName();
			}
		}

		return null;
	}
	
	public String getShoppingDate(){
		int year = Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR));
		DateFormat df = new SimpleDateFormat(MMMM_D_YYYY);
		return df.format(getDate(15,Calendar.OCTOBER,year));
	}
	
	public Date getDate(int day,int month, int year){
		Calendar date = TSCalendar.getInstance();
	    date.set(Calendar.DAY_OF_MONTH, day);
	    date.set(Calendar.YEAR, year);
		date.set(Calendar.MONTH, month);
		return date.getTime();

	}
	
	public String getSepEndDate(){
		int year = Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR));
		DateFormat df = new SimpleDateFormat(MMMM_D_YYYY);
		return df.format(getDate(15,Calendar.DECEMBER,year));
	}

}
