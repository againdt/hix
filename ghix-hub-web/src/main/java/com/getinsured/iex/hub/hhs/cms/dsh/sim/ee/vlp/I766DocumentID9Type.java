
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for I766DocumentID9Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="I766DocumentID9Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}AlienNumber"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}ReceiptNumber"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}DocExpirationDate"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "I766DocumentID9Type", propOrder = {
    "alienNumber",
    "receiptNumber",
    "docExpirationDate"
})
public class I766DocumentID9Type {

    @XmlElement(name = "AlienNumber", required = true)
    protected String alienNumber;
    @XmlElement(name = "ReceiptNumber", required = true)
    protected String receiptNumber;
    @XmlElement(name = "DocExpirationDate", required = true)
    protected XMLGregorianCalendar docExpirationDate;

    /**
     * Gets the value of the alienNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlienNumber() {
        return alienNumber;
    }

    /**
     * Sets the value of the alienNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlienNumber(String value) {
        this.alienNumber = value;
    }

    /**
     * Gets the value of the receiptNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * Sets the value of the receiptNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptNumber(String value) {
        this.receiptNumber = value;
    }

    /**
     * Gets the value of the docExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDocExpirationDate() {
        return docExpirationDate;
    }

    /**
     * Sets the value of the docExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDocExpirationDate(XMLGregorianCalendar value) {
        this.docExpirationDate = value;
    }

}
