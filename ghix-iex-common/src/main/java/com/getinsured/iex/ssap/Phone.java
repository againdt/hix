package com.getinsured.iex.ssap;

import com.getinsured.iex.util.ReferralUtil;
import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class Phone {

	@Expose
	private String phoneExtension;
	@Expose
	private String phoneNumber;
	@Expose
	private String phoneType;

	public Phone() {
	}

	public Phone(String phoneType) {
		this.phoneType = phoneType;
	}

	public String getPhoneExtension() {
		return phoneExtension;
	}

	public void setPhoneExtension(String phoneExtension) {
		this.phoneExtension = phoneExtension;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		String phoneNum = ReferralUtil.getPhoneNumberWithoutFormat(phoneNumber);
		this.phoneNumber = phoneNum;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

}
