package com.getinsured.hix.batch.externalnotices.service;

import java.util.List;

import com.getinsured.hix.model.ExternalNotice;

public interface ExternalNoticesFailureReportService {
	
	public void processFailureRecords(List<ExternalNotice> externalNotices);

}
