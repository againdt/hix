import API from '../services/API';
import { ActionTypes, spinnerActionTypes, messagesActionTypes, languageActionTypes } from '../constants/constants.js';

/*************************

 # Async action creators

 *************************/

export const fetchDashboardModel = url => async dispatch => {
    dispatch(startSpinner('Loading ...'));
    const { data, error } = await API.getData(url);
    if (error) {
        dispatch(stopSpinner());
        dispatch(setError(error));

    } else {
        dispatch(stopSpinner());
        dispatch(setDashboardModel(data));
    }
};

export const fetchMultiHousehold = url => async dispatch => {
    dispatch(startSpinner('Loading ...'));
    const { data, error } = await API.getData(url);
    if (error) {
        dispatch(stopSpinner());
        dispatch(setError(error));

    } else {
        dispatch(stopSpinner());
        dispatch(setMultihousehold(data));
    }
};

export const fetchVerificationData = url => async dispatch => {
    dispatch(startSpinner('Loading ...'));
    const { data, error } = await API.getData(url);
    if (error) {
        dispatch(stopSpinner());
        dispatch(setError(error));

    } else {
        dispatch(stopSpinner());
        dispatch(setVerificationData(data));
    }
};

export const fetchIssuers = url => async dispatch => {
    dispatch(startSpinner('Loading ...'));
    const { data, error } = await API.getData(url);
    if (error) {
        dispatch(stopSpinner());
        dispatch(setError(error));

    } else {
        dispatch(stopSpinner());
        dispatch(setIssuers(data));
    }
};


export const submitVerificationData = (...args) => async dispatch => {
    dispatch(startSpinner('Loading ...'));
    const { data, error } = await API.postData(...args);
    if (error) {
        dispatch(stopSpinner());
        dispatch(setError(error));

    } else {
        dispatch(stopSpinner());
        dispatch(updateVerificationData('validationStatus', data.validationStatus ));
    }
};

/*************************

 # Sync action creators

 *************************/
export const switchPageContainer = payload => ({
    type: ActionTypes.CHANGE_PAGE_CONTAINER,
    payload
});

export const setDashboardModel = (dashboardModel) => ({
    type: ActionTypes.RECEIVED_DASHBOARD_INFO_SUCCESS,
    payload: dashboardModel
});

export const setMultihousehold = (isMultiHousehold) => ({
    type: ActionTypes.RECEIVED_MUTLTIHOUSEHOLD_SUCCESS,
    payload: isMultiHousehold
});

export const setError = errorData => ({
    type: ActionTypes.FETCHING_ERROR,
    payload: errorData
});

export const setVerificationData = (verificationData) => ({
    type: ActionTypes.RECEIVED_VERIFICATION_DATA_SUCCESS,
    payload: verificationData
});

export const setIssuers = (issuersList) => ({
    type: ActionTypes.RECEIVED_ISSUERS_DATA_SUCCESS,
    payload: issuersList
});

export const updateVerificationData = (inputType, value) => ({
    type: ActionTypes.UPDATED_VERIFICATION_DATA,
    payload: {inputType, value}
});

export const selectApplicantIndex = (index) => ({
    type: ActionTypes.SELECT_APPLICANT_INDEX,
    payload: index
});

export const startSpinner = message => ({
    type: spinnerActionTypes.START_SPINNER,
    message
});

export const stopSpinner = () => ({
    type: spinnerActionTypes.STOP_SPINNER
});

export const setMessages = messages => ({
    type: messagesActionTypes.SET_MESSAGES,
    messages
});

export const setLanguage = language => ({
    type: languageActionTypes.SET_LANGUAGE,
    language
});