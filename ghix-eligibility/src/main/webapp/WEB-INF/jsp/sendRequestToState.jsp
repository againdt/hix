<%@page import="java.util.Properties"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.getinsured.eligibility.util.EligibilityUtils"%>
<%!private static ApplicationContext ctx = null;%>
<%!private static Properties prop = null;%>
<%
	if (ctx == null) {
		ctx = org.springframework.web.servlet.support.RequestContextUtils.getWebApplicationContext(request, application);
	}
	if (prop == null) {
		prop = (Properties) ctx.getBean("configProp");
	}
%>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<title>Submit Application to State</title>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript">
$(function() {
		$("#submitToState").bind("click", function() {
			callSubmit();
		});
	});

function callSubmit()
{
	var data = $("#stateRequestId").val();
	
	if(data == "")
	{
		alert("Please enter a value.");
		$("#stateRequestId").focus();
		return;
	}
  // make changes later to access the rp property and this value  (serverIpAddress) should be passed as an argument
  var serverIpAddress = $("#stateServerId option:selected").val();
  var url = "http://"+serverIpAddress+"/ghix-eligibility-rp/accounttransfer";
  $('#submitToState').attr('disabled','disabled');

  $.ajax({
	  type: "POST",
	  url: url,
	  data: data,
	  dataType: "text/xml",
	  contentType: "text/xml; charset=\"utf-8\"",
	  cache:false
  }).always(function( jqXHR ) {
		$("#stateRequestId").val("");
		$('#submitToState').removeAttr('disabled');
		alert("Processing Complete.\nFollowing is the response returned :-\n" + jqXHR.responseText);
	});
  
  /*
  Purposely Commented as the response is non xml so alwayz failure is returning, thts why alwayz method is used.
  .done(function( data, textStatus, jqXHR ) {alert("success");alert(data);alert(textStatus);alert(jqXHR.responseText);})
  .fail(function( jqXHR, textStatus, errorThrown ) {alert("failure");alert(errorThrown);alert(textStatus);alert(jqXHR.responseText);});
  */  
  
}
</script>
</head>

<body>
<%
final String fileData = EligibilityUtils.readFile();
%>
<b>Submit XML Application to State</b>
<form method="POST" action="--WEBBOT-SELF--">
<table border="0" width="100%">
  <tr>
    <td width="100%">
    <p><textarea rows="30" name="S1" cols="80" id="stateRequestId"><%=fileData%></textarea></p>
    </td>
  </tr>
  <tr>
    <td width="100%"><select size="1" name="D1" id="stateServerId">
    	<%
		final String[] serverIpAddress = (prop.getProperty("eligibility.ipaddress.state")+"").split(",");
    	for (int ii = 0 ; ii < serverIpAddress.length; ii++)
    	{
    	%>
			<option value="<%=serverIpAddress[ii]%>"><%=serverIpAddress[ii]%></option>
        <%
    	}
    	%>


      </select></td>
  </tr>
  <tr>
    <td width="100%"><input type="reset" value="Reset" name="B2"> 
    <input type="button" value="Submit to State" name="B1" id="submitToState"></td>
  </tr>
</table>
</form>

<p><font face="Courier New" size="2">Note: This accepts Application XML as input
and create entry in GI_WS_PAYLOAD table and submits application to state. Also
updates response in table. 
<br>1. Helps to test ERP (Eligibility Result Processor) 
<br>2. Helps to test State integration</font></p>

</body>

</html>
