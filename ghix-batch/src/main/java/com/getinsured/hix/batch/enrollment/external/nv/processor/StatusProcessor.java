package com.getinsured.hix.batch.enrollment.external.nv.processor;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.core.io.Resource;

import com.getinsured.hix.batch.enrollment.service.EnrlPopulateExtEnrollmentService;

public class StatusProcessor implements Tasklet {

	private EnrlPopulateExtEnrollmentService enrlPopulateExtEnrollmentService;
	private String fileName;
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		Long jobId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
		int idx = fileName.indexOf(':');
		fileName = fileName.substring(idx+1);
		Integer fileId = enrlPopulateExtEnrollmentService.createSummary(fileName, jobId, false);
		chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put("fileId", fileId);
		chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put("fileName", fileName);
		//System.out.println("-----------------job started ---------"+jobId);
		return null;
	}


	public void setEnrlPopulateExtEnrollmentService(EnrlPopulateExtEnrollmentService enrlPopulateExtEnrollmentService) {
		this.enrlPopulateExtEnrollmentService = enrlPopulateExtEnrollmentService;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
