<%@ taglib prefix="audit" uri="/WEB-INF/tld/audit-view.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
			
       <div class="gutter10-lr">
		  <div class="l-page-breadcrumb hide">
			<!--start page-breadcrumb -->
			<div class="row-fluid">
				<ul class="page-breadcrumb">
					<li><a href="javascript:history.back()">&lt; <spring:message
								code="label.back" /></a></li>
					<li><a href="<c:url value="/entity/entityadmin/managelist"/>"><spring:message
								code="label.entities" /></a></li>
					<li><spring:message code="label.entity.manage"/></li>
					<li>${enrollmentEntity.entityName}</li>
				</ul><!--page-breadcrumb ends-->
			</div>
			<!-- end of .row-fluid -->
		</div>
		

	<div class="row-fluid">
		
	<jsp:include page="/WEB-INF/views/entity/leftNavigationMenu.jsp" />
				
	<div class="span9" id="rightpanel">	
	 <form class="form-horizontal" id="frmupdatecertification"  enctype="multipart/form-data" name="frmupdatecertification" action="<c:url value="/entity/entityadmin/updateregistrationstatus"/>"  method="POST">
	 <df:csrfToken/>
			<input type="hidden" id="entityId" name="entityId"  value="<encryptor:enc value="${enrollmentEntity.id}"/>"/>
			<input type="hidden" id="documentId" name="documentId" value="<encryptor:enc value="${uploadedDocument}"/>"/>
							
				<div class="row-fluid">									
<!-- 					<div class="header"> -->
<%-- 						<h4 class="span10"><spring:message code="label.brkCertificationStatus"/></h4> --%>
<%-- 							<a class="btn btn-small" type="button"  href="/hix/admin/broker/certificationstatus/${broker.id}"><spring:message code="label.brkCancel"/></a> --%>
<!-- 					</div> -->
					<div class="header">
						<h4><spring:message code="label.RegistrationStatus"/></h4>
						<c:set var="encrytdEnrollmentEntityid"><encryptor:enc value="${enrollmentEntity.id}" isurl="true"/> </c:set>
					</div>
					<table class="table table-border-none verticalThead">
						<%-- <thead>
							<tr>
								<th class="span3"><spring:message code="label.RegistrationStatus"/></th>
								<th class="span6"><a class="btn btn-small pull-right" type="button" href="/hix/entity/entityadmin/registrationstatus/${enrollmentEntity.id}"><spring:message code="label.brkCancel"/></a></th>
							</tr>
						</thead> --%>
						<!-- header class for accessibility -->
								<tbody>
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.RegistrationStatus"/></th>
										<td><strong>${enrollmentEntity.registrationStatus}</strong></td>
									</tr>
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.EntityNumber"/></th>
										<td><strong>${enrollmentEntity.entityNumber}</strong></td>
									</tr>
									<tr>
										<c:choose>
								            <c:when test="${enrollmentEntity.registrationStatus=='Registered' || enrollmentEntity.registrationStatus=='Active' }">
								              <th>
								              	<table style="width: 100%;">
									              	<tr align="right">
									              		<td align="right" class="txt-right"><spring:message code="label.RenewalDate"/></td>
									              	</tr>
								              		<tr>
								              			<td>&nbsp;</td>
								              		</tr>
								              	</table>
								              </th>
								              <td>
								              	<table>
								              		<tr>
								              			<td>
								              				  <strong><fmt:formatDate value= "${enrollmentEntity.registrationRenewalDate}" pattern="MM/dd/yyyy"/></strong><a href="#" onClick="changeDate()">&nbsp; <spring:message code="label.assister.change"/><span class="aria-hidden"><spring:message code="label.RenewalDate"/></span></a>
								              			</td>
								              		</tr>
								              		<tr>
								              			<td>
								              				<div class="input-append date date-picker" id="date" data-date="" style="display:inline">
												             	<input class="span10" type="text" name="newRegistrationRenewalDate" id="newRegistrationRenewalDate" value= "<fmt:formatDate value= '${newEnrollmentEntity.registrationRenewalDate}' pattern="MM-dd-yyyy"/>"   pattern="mm-dd-yyyy"/> <span class="add-on"><i class="icon-calendar"></i></span>	
																<div id="newRegistrationRenewalDate_error"></div>
															</div>
								              			</td>
								              		</tr>
								              	</table>
												</td>								           
											</c:when>
										     <c:otherwise>
										           <th class="span4 txt-right" scope="row"><spring:message code="label.RenewalDate"/></th>
										           <td><strong><spring:message code="label.entity.nA"/></strong></td>
											</c:otherwise>
										</c:choose>
			
									</tr>
<!-- 									<tr> -->
<!-- 										<td class="span4 txt-right">Entity Registration Number</td> -->
<!-- 										<td><strong>1200042014</strong></td> -->
<!-- 									</tr> -->
									<tr>
										<td class="span4 txt-right"><label for="registrationStatus"><spring:message code="label.NewStatus"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label></td>
										<td>
										<select  size="1"  id="registrationStatus" name="registrationStatus" class="input-medium">
											<option value=""><spring:message code="label.entity.select"/></option>
											<c:forEach items="${statuslist}" var= "value">
												<option value="${value}" <c:if test="${newEnrollmentEntity.registrationStatus == value}"></c:if>>${value}</option> 
											</c:forEach>
										</select>
										<div id="registrationStatus_error"></div>
										</td>
									</tr>
									<tr>
										<td class="span4 txt-right"><label for="comments"><spring:message code="label.entity.coment"/></label></td>										
										<td><textarea name="comments" id="comments" class="input-xlarge" rows="3" maxlength="4000"></textarea></td>
									</tr>
									
									<tr>
			                   			<td class="txt-right"><label for="fileInput"><spring:message code="label.brkUploadSupportingDocs"/></label></td>
			                   			<td>
			                   				<div>
			                   					<input type="file" class="input-file" id="fileInput" name="fileInput">
			                   					<input type="hidden" id="fileInput_Size" name="fileInput_Size" value="1"/>	
				                   				<input type="button" id="btnUploadDoc" name="btnUploadDoc" value="<spring:message code="label.assister.upload"/>" class="btn" />
				                   				<div id="uploadedFileDiv"><label id="fileName">${fileName}</label></div>
			                   				</div>
			                   				<div>
 	 												<spring:message code="label.uploadDocumentCaption"/>
 											</div>
			                   			</td>
	                  				</tr>		
	                  																													
							</tbody>
					</table>
					<div class="form-actions">
						<a class="btn cancel-btn" type="button" href="/hix/entity/entityadmin/registrationstatus/${encrytdEnrollmentEntityid}" title="<spring:message code="label.brkCancel"/>"><spring:message code="label.brkCancel"/></a>
						<input type="submit" class="btn btn-primary" name="update" id="update" value="<spring:message code="label.entity.submit"/>" title="<spring:message code="label.entity.submit"/>" />
						<input type="hidden" id="fileToUpload" name="fileToUpload" value="fileInput"/>	
					</div>
					<input type="hidden" name="formAction" id="formAction" value="" />							
				</div>			
			
			</form>		
			<!-- Showing comments -->			 					
			<!-- end of span9 -->			
		
		<div class="gutter10">				
					<display:table id="enrollmentRegisterStatusHistory" name="enrollmentRegisterStatusHistory" list="enrollmentRegisterStatusHistory" requestURI="" sort="list" class="table" >
				           <display:column property="updatedOn" titleKey="label.entity.date"  format="{0,date,MM/dd/yyyy}" sortable="false" />
				           <display:column property="previousRegistrationStatus" titleKey="label.entity.previousStatus" sortable="false"/>
				           <display:column property="newRegistrationStatus" titleKey="label.NewStatus" sortable="false"/>
				           <display:column property="comments" titleKey="label.assister.comment" sortable="false"/>
				           <display:column titleKey="label.assister.viewAttachment" > 
				                <c:choose>
				                    <c:when test="${enrollmentRegisterStatusHistory.documentId!=null}">	
				                    <c:set var="encryteddocumentId"><encryptor:enc value="${enrollmentRegisterStatusHistory.documentId}" isurl="true"/> </c:set>
			                              <a href="#" onClick="showdetail('${encryteddocumentId}');" id="edit_${enrollmentRegisterStatusHistory.documentId}"><spring:message code="label.assister.viewAttachment"/></a> 
			                        </c:when>
			                        <c:otherwise>	
			                            <spring:message code="label.assister.noAttachment"/>
			                        </c:otherwise>
			                   </c:choose> 
				           </display:column>	
					</display:table>					
		</div><!-- end of .gutter10 -->
		</div>
</div>		
	<div id="modalBox" class="modal hide fade">
	<div class="modal-header">
		<a class="close" data-dismiss="modal" data-original-title="">x</a>
		<h3 id="myModalLabel"><spring:message code="label.assister.viewComment"/></h3>
	</div>
	<div class="modal-body">
		<p id ="commentDet"><spring:message code="labe.assister.dataData"/></p>
	</div>
	
		<div class="modal-footer">
			<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message code="label.assister.close"/></a>
	  	</div>
    </div>
</div>
	<script type="text/javascript">
		$(function(){$('.datepick').each(function(){$(this).datepicker({showOn:"button",buttonImage:"../resources/images/calendar.gif",buttonImageOnly:true});});});
		
		$('.date-picker').datepicker({
	        autoclose: true,
	        format: 'mm-dd-yyyy'
		});

		$(document).ready(function() {
			$("#registerStatus").removeClass("link");
			$("#registerStatus").addClass("active");
			$("#date").hide();
			$("#uploadedFileDiv").hide();
			
			//var button_id_names= '#btnUploadDoc';
			
			
			
		}); //(document).ready -- close
		
		$("#btnUploadDoc").click(function(){
			
			if($('#frmupdatecertification').validate().form()){
				$(modalHTML).remove();
					//$('#fileToUpload').val('fileInput');
				var uploadPhoto = $(this).attr('id'),
					file = $('#fileInput').val(),
					modalHTML = $('<div class="modal popup-address" id="fileUpload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-rel="uploadPhoto" data-dismiss="modal">x</button></div></div><div class="modal-body gutter10" style="max-height:470px;"><h4 class="uploadText"></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-rel="uploadPhoto" data-dismiss="modal"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});					
				
				if(file!=""){
						
					var hiddenVar='fileInput_Size';
					
					if(document.getElementById(hiddenVar).value==1){
						$("#btnUploadDoc").attr('disabled','disabled');
						//
						
						$('#frmupdatecertification').ajaxSubmit({
							url: "<c:url value='/entity/entityadmin/uploaddocument'/>",
							beforeSend:function(){
								$('.modal-header, .modal-body, .modal-footer').hide();
								$(modalHTML).append('<div class="txt-center"><img src="/hix/resources/images/loader.gif" width="100" height="100" alt="Required!" /></div>');
							},
							success: function(responseText){
								
								var val = responseText.split("|");  /* For IE*/
								
								//$("#btnUploadDoc").removeAttr('disabled');
								if(val[2]==0){
									/* console.log('1111', $(modalHTML)); */
									/*failed to upload file*/
									hideContentBeforeLoad();
									$(modalHTML).find('h4').html('<spring:message code="label.failedtouoload"/>').attr('data-val','false');
									
										/* $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">x</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.failedtouoload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
										document.getElementById('fileInput').value=null; */
									} 
									else if(val[2]==-1){
										/* console.log('222'); */
										/*file less than 5mb*/
										hideContentBeforeLoad();
										$(modalHTML).find('h4').html('<spring:message code="label.filelessthan5MB"/>').attr('data-val','false');
										
										/* $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">x</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
										document.getElementById('fileInput').value=null; */
									}
									else if(val[0]==""){
										/* console.log('333'); */
										/*select each file less than 5MB*/
										hideContentBeforeLoad();
										$(modalHTML).find('h4').html('<spring:message code="label.selecteachfilelessthan5MB"/>').attr('data-val','false');
										
										/* $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">x</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.selecteachfilelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"}); */	
									}
									else{
										/* console.log('444'); */
										/*file uploaded successfully*/
										if(val[0]=='fileInput'){
											$("#documentId").val(val[2]);
										 }
										hideContentBeforeLoad();
										/* console.log(uploadPhoto,'uploadPhoto') */
										$(modalHTML).find('h4').html('<spring:message code="label.fileuploadedsuccessfully"/>').attr({"data-val":"true", "data-rel":uploadPhoto});
										
										//$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">x</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.fileuploadedsuccessfully"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
									}
							},
							error : function(responseText){   /* For Firefox*/
								$("#btnUploadDoc").removeAttr('disabled');
								var parsedJSON = eval(responseText);
								var val = parsedJSON.responseText.split("|");
								//alert("error function value: ",val);
								if(val[2]==0){
									/* console.log('5555'); */
									/*failed tp upload*/
									hideContentBeforeLoad();
									$(modalHTML).find('h4').html('<spring:message code="label.failedtouoload"/>').attr('data-val','false');
									
									/* $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">x</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.failedtouoload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
									document.getElementById('fileInput').value=null; */
									} 
								  else if(val[2]==-1){
									  /* console.log('6666'); */
									  	/*file less than 5mb*/
									  	
									  	hideContentBeforeLoad();
										$(modalHTML).find('h4').html('<spring:message code="label.filelessthan5MB"/>').attr('data-val','false');
										/* $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">x</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
										document.getElementById('fileInput').value=null; */
								   }
								  else if(val[0]==""){
									 /*  console.log('7777'); */
									 	/*select each file less than 5MB*/
									  	hideContentBeforeLoad();
										$(modalHTML).find('h4').html('<spring:message code="label.selecteachfilelessthan5MB"/>').attr('data-val','false');
										
										/*$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">x</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.selecteachfilelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});*/	
									}
									else{
										/* console.log('888'); */
										/*file uploaded successfully*/
										hideContentBeforeLoad();
										$(modalHTML).find('h4').html('<spring:message code="label.selectfilebeforeupload"/>').attr('data-val','false');
										/*$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">x</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.fileuploadedsuccessfully"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});*/
									}
							}
						});
					}
					
					else if(document.getElementById(hiddenVar).value==0) {
						//alert("Please select a file with size less than 5 MB");
						/* console.log('99999'); */
						hideContentBeforeLoad();
						$(modalHTML).find('h4').html('<spring:message code="label.filelessthan5MB"/>').attr('data-val','false');
										
						/* $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">x</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
						$("#btnUploadDoc").removeAttr('disabled');	
						document.getElementById('fileInput').value=null; */
					}
					
					else if(document.getElementById(hiddenVar).value==-1){
						/* console.log('1000000'); */
						hideContentBeforeLoad();
						$(modalHTML).find('h4').html('<spring:message code="label.selectfilegreaterthanzero"/>').attr('data-val','false');
						
						//alert("Please select a file with size greater than zero");
						/* $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">x</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.selectfilegreaterthanzero"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
						$("#btnUploadDoc").removeAttr('disabled'); */
					}
				}
				else{
					//alert("Please select a file before upload");
					/* console.log('aaaaaa'); */
					hideContentBeforeLoad();
					$(modalHTML).find('h4').html('<spring:message code="label.selectfilebeforeupload"/>').attr('data-val','false');
					
						/* $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">x</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.selectfilebeforeupload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
						$("#btnUploadDoc").removeAttr('disabled'); */
					}
					return false; 
					
				} // $('#frmupdatecertification').validate().form() -- close
				
			});	// button click close
			
		function hideContentBeforeLoad(){
			$('#crossClose').focus();
			$("#btn_UploadPhoto").removeAttr('disabled');
			$('.modal-header, .modal-body, .modal-footer').show();
			$('.txt-center').remove();
			$('#crossClose').focus();
		}
		
		function getComment(comments)
		{					
				
			    $('#commentDet').html("<p> Loading Comment...</p>");
				comments =comments.replace(/#/g,'<br/>'); //Added to remove new line break marker with HTML equivalent br				
				comments=comments.replace(/apostrophes/g,"'"); //Added to replace regex apostrophes with '
				$('#commentDet').html(comments);			
		}
		
		var validator = $("#frmupdatecertification").validate({ 
			 rules : {
				 registrationStatus : {required : true},
				 newRegistrationRenewalDate :{newRegistrationRenewalDate : true},
			 },
			 messages : {
				 registrationStatus : { required : "<span><em class='excl'>!</em><spring:message  code='label.validateRegistrationStatus' javaScriptEscape='true'/></span>"},
			    newRegistrationRenewalDate : { newRegistrationRenewalDate : "<span><em class='excl'>!</em><spring:message  code='label.validateRegistrationRenewalDate' javaScriptEscape='true'/></span>"},
			 },
			 errorClass: "error",
			 errorPlacement: function(error, element) {
			 var elementId = element.attr('id');
			 error.appendTo( $("#" + elementId + "_error"));
			 $("#" + elementId + "_error").attr('class','error help-inline');
			}
		});			

		 function showdetail(documentId) {
			 var rv = -1; // Return value assumes failure.
			 if (navigator.appName == 'Microsoft Internet Explorer')
			 {
			    var ua = navigator.userAgent;
			    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			    if (re.exec(ua) != null)
			       rv = parseFloat( RegExp.$1 );
			 }
			 if(rv <= 8.0 && navigator.appName == 'Microsoft Internet Explorer'){
				 $('<div class="modal popup-address" id="addressIFrame"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><h4><spring:message  code='label.entiy.upgradeBrowser' javaScriptEscape='true'/></h4><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">x</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 0px;"><spring:message  code='label.entiy.upgradeBrowserMessage' javaScriptEscape='true'/></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
				 } else {
					var  contextPath =  "<%=request.getContextPath()%>";
					 var documentUrl = contextPath + "/entity/entityadmin/viewAttachment?encrypteddocumentId="+documentId;
				      window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
				}
			}	

		 
 function changeDate(){
	 
	 document.getElementById('date').style.display='inline';
}
 jQuery.validator.addMethod("newRegistrationRenewalDate", function(value, element, param) {
	  var current=new Date();
	  if(value != null){
		if(Date.parse(current) > Date.parse(value)){
			return false;
		}  
	  }
		return true;
 });
 $(function(){
	 $('#fileInput').change(function(){
			var rv = -1; // Return value assumes failure.
			 if (navigator.appName == 'Microsoft Internet Explorer')
			 {
			    var ua = navigator.userAgent;
			    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			    if (re.exec(ua) != null)
			       rv = parseFloat( RegExp.$1 );
			 }
			 if(!(rv <= 9.0 && navigator.appName == 'Microsoft Internet Explorer')){
			 var f=this.files[0];
			if((f.size > 5242880)||(f.fileSize > 5242880)){
				$("#fileInput_Size").val(0);
			}
			else{
				$("#fileInput_Size").val(1);	
			} 
			}
			else{
				$("#fileInput_Size").val(1);	
			} 
		});
	});	
</script>
