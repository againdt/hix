<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<%@ page isELIgnored="false"%>

<jsp:useBean id="now" class="java.util.Date" />
<div class="gutter10">
	<div class="row-fluid">
		<h1>Make A Payment</h1>
	</div>
	<div class="row-fluid">
	<div class="span3" id="sidebar">
		<h4>Steps</h4>
			<ul class="nav nav-list">
				<li>View Invoice & Pay</li>
				<li class="active">Review Payment</li>
				<li>Confirm Payment</li>
			</ul>
	</div>

	<div class="span9">
	<!-- <h4 class="graydrkbg ">Please review your payment details</h4> -->

	<form class="form-horizontal gutter10" id="fromreviewpayment" name="fromreviewpayment" action="processpayment" method="POST">
	<df:csrfToken/>
		<input type="hidden" value="<encryptor:enc value = "${employerid}"/>" name="employerid" id="employerid" />
		<input type="hidden" value="<encryptor:enc value = "${invoice.id}"/>" name="invoiceid" id="invoiceid" />
		<input type="hidden" value="<encryptor:enc value = "${paymentdetail.id}"/>" name="id" id="paymentid" />
		<input type="hidden" value="${enablePartialPayment}" name="enablePartialPayment" id="enablePartialPayment" />
		<input type="hidden" value="${minimumPaymentThreshold}" name="minimumPaymentThreshold" id="minimumPaymentThreshold" />
		<input type="hidden" value="${invoice.totalAmountDue}" name="totalAmountDue" id="totalAmountDue" />
		<c:choose>
			<c:when test="${fn:length(paymentmethods) > 0}">
				<div id="directBank">
					<div class="control-group">
						<h5>Please review your payment details</h5>
					</div>
					<div class="control-group">
						<label class="control-label required " style="text-align: left">Pay Towards Invoice</label>
						<div class="controls">
						 <label class="control-label" style="text-align: left">${invoice.invoiceNumber}</label>
						 </div>
					</div>
					<div class="control-group">
					<label class="control-label required " style="text-align: left">Payment Amount</label>
					<div class="controls">
					<label class="control-label" style="text-align: left">$	${invoice.totalAmountDue}</label>
					</div>
					</div>
					<div class="control-group">
						<label class="control-label required" style="text-align: left">Payment Date</label>
						<div class="controls">
						<label class="control-label" style="text-align: left">
						<fmt:formatDate value="${now}" pattern="MM/dd/yyyy" /></label>
						</div>
					</div>
					<div class="control-group">
					<label class="control-label required" style="text-align: left">Payment Method</label>
					<div class="controls">
					
						<table>
							<c:forEach items="${paymentmethods}" var="activepaymentdetail">
							<tr style="white-space: nowrap">
								<td width="20" align="left">
								<c:choose>
									<c:when test="${activepaymentdetail.isDefault == 'Y'}">
									 <%-- id="paymenttype+${paymentmethods.id}" --%> 
										<input type="radio" name="paymenttype" value="<encryptor:enc value = "${activepaymentdetail.id}"/>" checked="checked">
									</c:when>
									<c:otherwise>
									<!-- onChange="selectedPaymentMethods(this); -->
										<input type="radio" name="paymenttype" value="<encryptor:enc value = "${activepaymentdetail.id}"/>" ">
									</c:otherwise>
								</c:choose>
								</td>
								<td width="50" align="center" style="text-align: left">${activepaymentdetail.paymentMethodName}</td>
								<!-- <td width="20"> -->
								<%-- <c:choose>
								<c:when test='${not empty activepaymentdetail.financialInfo.bankInfo}'>
									<a class="ttclass" rel="tooltip" href="#" data-original-title="
									<table>
										<tr><td>Bank Name:</td> <td>${activepaymentdetail.financialInfo.bankInfo.bankName}</td></tr>
									    <tr><td>Account No:</td> <td>${activepaymentdetail.financialInfo.bankInfo.accountNumber}</td></tr>
									    <tr><td>Routing No:</td> <td>${activepaymentdetail.financialInfo.bankInfo.routingNumber}</td></tr>
									    <tr><td>Account Type:</td> <td>${activepaymentdetail.financialInfo.bankInfo.accountType}</td></tr></table>">
										<i class="icon-question-sign"></i>
									</a>
								</c:when>
								<c:when test='${not empty activepaymentdetail.financialInfo.creditCardInfo}'>
								<a class="ttclass" rel="tooltip" href="#" data-original-title="
								<table>
									<tr><td>Name On Card:</td><td>${activepaymentdetail.financialInfo.creditCardInfo.nameOnCard}</td></tr>
									<tr><td>Card No:</td><td>${activepaymentdetail.financialInfo.creditCardInfo.cardNumber}</td></tr>
									<tr><td>Card Type:</td><td>
									<c:if test="${activepaymentdetail.financialInfo.creditCardInfo.cardType  != null && activepaymentdetail.financialInfo.creditCardInfo.cardType  == 'visa'}">
									<c:out value="Visa"/></c:if>
									<c:if test="${activepaymentdetail.financialInfo.creditCardInfo.cardType != null && activepaymentdetail.financialInfo.creditCardInfo.cardType  == 'mastercard'}">
									<c:out value="Master Card" />
									</c:if>
									<c:if test="${activepaymentdetail.financialInfo.creditCardInfo.cardType  != null && activepaymentdetail.financialInfo.creditCardInfo.cardType  == 'americanexpress'}">
									<c:out value="American Express" />
									</c:if></td>
									</tr>
									</table>">
									<i class="icon-question-sign"></i></a>
								</c:when>
								<c:otherwise>
								<a class="ttclass" rel="tooltip" href="#" data-original-title="
								<table>
									<tr><td>Address:</td><td>${activepaymentdetail.financialInfo.address1}</td></tr>
									<tr><td>City:</td><td>${activepaymentdetail.financialInfo.city}</td></tr>
									<tr><td>State:</td><td>${activepaymentdetail.financialInfo.state}</td></tr>
									<tr><td>ZipCode:</td><td>${activepaymentdetail.financialInfo.zipcode} </td></tr></table>">
								<i class="icon-question-sign"></i>
								</a>
								</c:otherwise>
								</c:choose> --%>
								<!-- </td> -->
								</tr>
								</c:forEach>
								</table>
					
					</div>
					</div>
					
					<div class="control-group" id="errorDiv" style="display: none;">
						<label class="control-label required" style="text-align: left">Error</label>
						<div class="controls">
						<label class="control-label" style="text-align: left" id="errorLabel"></label>
						</div>
					</div>
					
					<div class="control-group" id="bankDiv" style="display: none;">
						<table>
							<tr>
								<th><label class="control-label required" style="text-align: left">Account Number</label></th>
								<th><label class="control-label required" style="text-align: left">Account Type</label></th>
								<th><label class="control-label required" style="text-align: left">Routing Number</label></th>
						</tr>
						<tr>
							<td><label class="control-label" style="text-align: left" id="accountNoLabel"></label></td>
							<td><label class="control-label" style="text-align: left" id="accountTypeLabel"></label></td>
							<td><label class="control-label" style="text-align: left" id="routingNoLabel"></label></td>
						</tr>
						
						</table>
					</div>
					
					<div class="control-group" id="creditCardDiv" style="display: none;">
						<table>
							<tr>
								<th><label class="control-label required" style="text-align: left">Card Number</label></th>
								<th><label class="control-label required" style="text-align: left">Card Type</label></th>
								<th><label class="control-label required" style="text-align: left">Expiration Month</label></th>
								<th><label class="control-label required" style="text-align: left">Expiration Year</label></th>
							</tr>
							<tr>
								<td><label class="control-label" style="text-align: left" id="cardNoLabel"></label></td>
								<td><label class="control-label" style="text-align: left" id="cardTypeLabel"></label></td>
								<td><label class="control-label" style="text-align: left" id="expMonthLabel"></label></td>
								<td><label class="control-label" style="text-align: left" id="expYearLabel"></td>
							</tr>
						</table>
					</div>
					
					
		<div class="control-group" align="center">
			<table>
				<tr>
					<td></td>
					<td><div id="payment_radio_error"></div>
					</td>
				</tr>
			</table>
		</div>
		<div class="control">
			<p>
				<%-- NOTE: The payment to this invoice will be dated
				<fmt:formatDate value="${now}" pattern="MM/dd/yyyy" />
				. Credit card payments will be processed immediately; bank
				withdrawals will be usually processed within 3 business days. --%>
				NOTE: By checking the box below, you are authorizing us to withdraw funds
				 from the above bank account for the amount of the invoice as of today's date.
				 Bank withdrawals will usually be processed with 3-4 business days.
			</p>
		</div>
		
		<!-- <div class="control-group">
			<label class="control-label required" for="thresholdPayment"
				style="text-align: left" id="thresholdPaymentLabel">Amount</label>
			<div class="controls">
				<input type="text" id="thresholdPayment"
					name="thresholdPayment">
				<div id="thresholdPayment_error"></div>
			</div>
		</div> -->
		<div class="controls">
			<label class="checkbox"> <input type="checkbox"
				name="authorise" id="authorise"> Yes, I authorize this
				transaction.
			</label>
			<div id="authorize_error"></div>
		</div>

	</div>


	<!-- end: this only shows when you come from 'edit payment' -->

	<div class="form-actions">
		<a class="btn btn-primary"
			href="<c:url value="/shop/employer/makepayment"/>" id="back">Back</a> <input
			type="button" name="submitBtn" id="submitBtn"
			onClick="javascript:validateForm();" Value="Make Payment"
			class="btn btn-primary"> <a class="btn btn-primary"
			href="<c:url value="/shop/employer/makepayment"/>" id="cancel">Cancel Payment</a>
	</div>
	</c:when>
	<c:otherwise>
		<h4 class="alert alert-info">There is no active payments available
			please add active payment method.</h4>
	</c:otherwise>
	</c:choose>
	</form>
</div>
</div>


<div class="row-fluid">
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">
				<p>The prototype showcases three scenarios (A, B and C)
					dependant on a particular Employer's eligibility to use the SHOP
					Exchange.</p>
			</div>
		</div>
	</div>
</div>
</div>
<!-- row-fluid -->
<script type="text/javascript">
$(document).ready(function() {
	//alert("hi");
//alert( jQuery( 'input[name=paymenttype][checked=checked]' ).val() );
var id = jQuery( 'input[name=paymenttype][checked=checked]' ).val();
if(id !=null && id != '')
{
	fetchPaymentMethod(id);
}
});

var validator = $("#fromreviewpayment")
.validate(
		{
			rules : {
				'thresholdPayment' : {
					required : true,
					number : true
				}
			},
			messages : {
				'thresholdPayment' : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateThresholdAmt' javaScriptEscape='true'/></span>",
					number : "<span> <em class='excl'>!</em><spring:message code='label.validateThresholdNumber' javaScriptEscape='true'/></span>"
				}
			},
				errorClass : "error",
				errorPlacement : function(error, element) {
					var elementId = element.attr('id');
					error.appendTo($("#" + elementId + "_error"));
					$("#" + elementId + "_error")
							.attr('class', 'error');
				}
			});
</script>
<script>

function validateForm() {
	var authoriseflag = false;
	var paymenttypeflag = false;
	
	if(null != '${enablePartialPayment}' && null != '${minimumPaymentThreshold}' && '${enablePartialPayment}' =='YES' && '${minimumPaymentThreshold}' !='100')
    {
    	var amt = document.getElementById("minimumPaymentThreshold").value;
    	var payableAmt = document.getElementById("totalAmountDue").value;
		var percentCalc= payableAmt*amt/100;
    	if(null != '${thresholdPayment}' && $("#thresholdPayment").val() < percentCalc)
		{
			error = "<label class='error' generated='true'><span><em class='excl'>!</em>Amount less than minimum allowed amount.</span></label>";
			$('#thresholdPayment_error').html(error);
			return false;
		}
    }
	
	 if (!$("input[name='paymenttype']:checked").val()) {
	    	error = "<label class='error' generated='true'><span><em class='excl'>!</em>Please select a Payment Method.</span></label>";
			$('#payment_radio_error').html(error);
			return false;
   	}
   	else {
   		 paymenttypeflag = true;
   	}
	
	
	
    if($("#authorise").is(':checked')) {
    	authoriseflag = true;
    }
	else {
		error = "<label class='error' generated='true'><span><em class='excl'>!</em>Please check Authorize Transaction check box.</span></label>";
		$('#authorize_error').html(error);
		return false;
	}
    $("#submitBtn").attr("disabled","disabled");
    if(authoriseflag && paymenttypeflag){
    	$("#fromreviewpayment").submit();
    	$('#back').bind('click', false);
		$('#cancel').bind('click', false);		
	}
}

$('.ttclass').tooltip();
</script>
<script type="text/javascript">

if(null != '${enablePartialPayment}' && null != '${minimumPaymentThreshold}')
{
	//if('${enablePartialPayment}' == 'YES' && '${minimumPaymentThreshold}' == '100')
	if(('${enablePartialPayment}' == 'YES' && '${minimumPaymentThreshold}' == '100') || '${enablePartialPayment}' == 'NO')
	{
		thresholdPayment.style.display = "none";
		thresholdPaymentLabel.style.display="none";
	}
	else
	{
		thresholdPayment.style.display = "";
		thresholdPaymentLabel.style.display="";
	}
}
</script>
<script>
var paymentMethodId = null;
$("input[name='paymenttype']").click(function() {
	paymentMethodId = this.value;
	fetchPaymentMethod(paymentMethodId);
    /* alert(category); */
});

/* function selectedPaymentMethods()
{
	//alert("in paymentMethod: "+ this.value);	
}

$('radio[name="paymenttype"]').click(function(){
    var paymentMethodId=this.value;
    alert(paymentMethodId);
  });
 */

function fetchPaymentMethod(paymentId)
{
	//alert("Received Payment id: "+paymentId);
	
	var validateUrl = '<c:url value="/shop/employer/fetchpaymentmethod"> <c:param name="${df:csrfTokenParameter()}"> <df:csrfToken plainToken="true" /> 	</c:param> </c:url>';

	$.ajax({
		url : validateUrl,
		type : "POST",
		data : {
			paymentMethodId : paymentId
		},
		success: function(response, xhr)
		{
			if(isInvalidCSRFToken(xhr))	    				
				return;
			if(response != '')
			{
				populatePaymentDetails(response);
			}
			else
			{
				alert("Received invalid response");
			}
		}
	});
}

function populatePaymentDetails(response)
{
	var newresponse = JSON.parse(response); 
	//alert(newresponse.ERROR);
	if(newresponse.ERROR != null && newresponse.ERROR != '')
	{
		//alert(newresponse.PAYMENT_TYPE);
		document.getElementById("errorLabel").innerHTML = newresponse.ERROR;
		document.getElementById('errorDiv').style.display = "block";
		document.getElementById('bankDiv').style.display = "none";
		document.getElementById('creditCardDiv').style.display = "none";
	}
	if(newresponse.PAYMENT_TYPE == 'BANK')
	{
		//alert("BANK");
		document.getElementById("accountNoLabel").innerHTML = newresponse.ACCOUNT_NO;
		if(newresponse.ACCOUNT_TYPE == 'C')
		{
			document.getElementById("accountTypeLabel").innerHTML = 'Checking';
		}
		else
		{
			document.getElementById("accountTypeLabel").innerHTML = 'Saving';
		}
		
		document.getElementById("routingNoLabel").innerHTML = newresponse.ROUTING_NUMBER;

		document.getElementById('errorDiv').style.display = "none";
		document.getElementById('bankDiv').style.display = "block";
		document.getElementById('creditCardDiv').style.display = "none";
	}
	else if(newresponse.PAYMENT_TYPE == 'CREDITCARD')
	{
		//alert("CREDIT Card"+newresponse.PAYMENT_TYPE);
		document.getElementById("cardNoLabel").innerHTML = newresponse.CARD_NUMBER;
		document.getElementById("cardTypeLabel").innerHTML = newresponse.CARD_TYPE;
		document.getElementById("expMonthLabel").innerHTML = newresponse.EXPIRATION_MONTH;
		document.getElementById("expYearLabel").innerHTML = newresponse.EXPIRATION_YEAR;
		
		document.getElementById('errorDiv').style.display = "none";
		document.getElementById('bankDiv').style.display = "none";
		document.getElementById('creditCardDiv').style.display = "block";
	}
		
}

function isInvalidCSRFToken(xhr){
	var rv = false;
	if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
		alert($('Session is invalid').text());
		rv = true;
	}
	return rv;
}

</script>

<script type = "text/javascript" >
   function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>

