package com.getinsured.hix.batch.enrollment.service;

public interface ResendInd20Service {
	
	public void sendIndvPSDetails(long jobExecutionId) throws Exception;

}
