package com.getinsured.eligibility.at.ref.service;

import com.getinsured.hix.model.consumer.Household;
import com.getinsured.iex.ssap.model.SsapApplicant;

/**
 * @author chopra_s
 * 
 */
public interface ReferralRidpService {
	String RIDP_VERIFIED_YES = "Y";

	String RIDP_VERIFIED_NO = "N";
	
	int PRIMARY_PERSON = 1;

	int executeRidp(  SsapApplicant ssapApplicant, int userId, Household household);
}