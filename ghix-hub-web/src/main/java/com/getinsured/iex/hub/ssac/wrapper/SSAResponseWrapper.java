package com.getinsured.iex.hub.ssac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ssac.extension.SSAIncarcerationInformationType;
import com.getinsured.iex.hub.ssac.extension.SSAQuartersOfCoverageType;
import com.getinsured.iex.hub.ssac.extension.SSAResponseType;
import com.getinsured.iex.hub.ssac.extension.SSATitleIIMonthlyIncomeType;
import com.getinsured.iex.hub.ssac.extension.SSATitleIIYearlyIncomeType;
import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Boolean;

public class SSAResponseWrapper implements JSONAware {

	private boolean ssnVerified;
	private boolean personCitizenIndicator;
	private boolean personIncarcerationIndicator;
	private SSAIncarcerationInformationWrapper ssaIncarcerationWrapper;
	private String deathConfmCode;
	private boolean qtrCoverageIndicator;
	private SSAQuartersOfCoverageWrapper qtrsOfcoverage;
	private boolean titleIIAnnualIncomeIndicator;
	private SSATitleIIYearlyIncomeWrapper titleIIYearlyIncome;
	private boolean titleIImonthlyIncomeIndicator;
	private SSATitleIIMonthlyIncomeWrapper titleIIMonthlyIncome;

	public SSAResponseWrapper(SSAResponseType v) {
		
		if(v == null){
			return;
		}
		
		this.ssnVerified = checkBoolean(v.getSSNVerificationIndicator());

		this.personCitizenIndicator = checkBoolean(v.getPersonUSCitizenIndicator());
			
		this.personIncarcerationIndicator = checkBoolean(v.getPersonIncarcerationInformationIndicator());

		if(this.personIncarcerationIndicator){
			SSAIncarcerationInformationType ssaIncarcerationType = v.getSSAIncarcerationInformation();
			this.ssaIncarcerationWrapper = new SSAIncarcerationInformationWrapper(ssaIncarcerationType);
		}
			
		this.deathConfmCode = v.getDeathConfirmationCode() !=null ? v.getDeathConfirmationCode().value() : null;
		
		this.qtrCoverageIndicator = checkBoolean(v.getSSAQuartersOfCoverageInformationIndicator());

		if(this.qtrCoverageIndicator){
			SSAQuartersOfCoverageType quatersOfcoverage = v.getSSAQuartersOfCoverage();
			this.qtrsOfcoverage = new SSAQuartersOfCoverageWrapper(quatersOfcoverage);
		}
		
		this.titleIIAnnualIncomeIndicator = checkBoolean(v.getSSATitleIIAnnualIncomeInformationIndicator());

		if(this.titleIIAnnualIncomeIndicator){
			SSATitleIIYearlyIncomeType titleIIYearlyIncomeType = v.getSSATitleIIYearlyIncome();
			this.titleIIYearlyIncome = new SSATitleIIYearlyIncomeWrapper(titleIIYearlyIncomeType); 
		}
			
		this.titleIImonthlyIncomeIndicator = checkBoolean(v.getSSATitleIIMonthlyIncomeInformationIndicator());

		if(this.titleIImonthlyIncomeIndicator){
			SSATitleIIMonthlyIncomeType titleIIMonthlyIncomeType = v.getSSATitleIIMonthlyIncome();
			this.titleIIMonthlyIncome = new SSATitleIIMonthlyIncomeWrapper(titleIIMonthlyIncomeType);
		}
		
	}

	public boolean isSsnVerified() {
		return ssnVerified;
	}

	public boolean isPersonCitizenIndicator() {
		return personCitizenIndicator;
	}

	public boolean isPersonIncarcerationIndicator() {
		return personIncarcerationIndicator;
	}

	public String getDeathConfmCode() {
		return deathConfmCode;
	}

	public boolean isQtrCoverageIndicator() {
		return qtrCoverageIndicator;
	}

	public SSAIncarcerationInformationWrapper getSsaIncarcerationWrapper() {
		return ssaIncarcerationWrapper;
	}
	
	public SSAQuartersOfCoverageWrapper getQtrsOfcoverage() {
		return qtrsOfcoverage;
	}

	public boolean isTitleIIAnnualIncomeIndicator() {
		return titleIIAnnualIncomeIndicator;
	}

	public SSATitleIIYearlyIncomeWrapper getTitleIIYearlyIncome() {
		return titleIIYearlyIncome;
	}

	public boolean isTitleIIMonthlyIncomeIndicator() {
		return titleIImonthlyIncomeIndicator;
	}

	public SSATitleIIMonthlyIncomeWrapper getTitleIImonthlyIncome() {
		return titleIIMonthlyIncome;
	}
	
	private boolean checkBoolean(Boolean b){
		return (b != null ? b.isValue() : false);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("SSNVerificationIndicator", this.isSsnVerified());
		obj.put("DeathConfirmationCode", this.deathConfmCode);
		obj.put("PersonUSCitizenIndicator", this.personCitizenIndicator);
		obj.put("PersonIncarcerationInformationIndicator", this.personIncarcerationIndicator);
		obj.put("SSATitleIIMonthlyIncomeInformationIndicator", this.titleIImonthlyIncomeIndicator);
		obj.put("SSATitleIIAnnualIncomeInformationIndicator", this.titleIIAnnualIncomeIndicator);
		obj.put("SSAQuartersOfCoverageInformationIndicator", this.qtrCoverageIndicator);
		obj.put("SSAIncarcerationInformation", this.ssaIncarcerationWrapper);
		obj.put("SSATitleIIMonthlyIncome", this.titleIIMonthlyIncome);
		obj.put("SSATitleIIYearlyIncome", this.titleIIYearlyIncome);
		obj.put("SSAQuartersOfCoverage", this.qtrsOfcoverage);
		return obj.toJSONString();
	}

}
