<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>

<c:set var="calheersEnv" value='<%=GhixConstants.CALHEERS_ENV%>'></c:set>

<input type="hidden" id="providerSearchConfig" name="providerSearchConfig" value="${providerSearchConfig}"/>
<input type="hidden" id="benefitQuestionsConfig" name="benefitQuestionsConfig" value="${benefitQuestionsConfig}"/>
<input type="hidden" id="prescriptionSearchConfig" name="prescriptionSearchConfig" value="${prescriptionSearchConfig}"/>
<input type="hidden" id="stateCode" name="stateCode" value="${stateCode}"/>
<input type="hidden" id="loggedInUser" name="loggedInUser" value="${loggedInUser}"/>
<input type="hidden" id="isIndividual" name="isIndividual" value="${isIndividual}"/>
<link rel="stylesheet" href="<c:url value="/resources/css/chosen.css" />" type="text/css">
<link rel="stylesheet" href="<c:url value="/resources/css/tiles.css" />" type="text/css">


<script type="text/javascript">
  var plan_display = new Array();
  plan_display['pd.label.trackingEvent.selectProvider'] = "<spring:message code='pd.label.trackingEvent.selectProvider' javaScriptEscape='true'/>";
  plan_display['pd.label.doctor.title'] = "<spring:message code='pd.label.doctor.title' javaScriptEscape='true'/>";
  plan_display['pd.label.doctor.dmd'] = "<spring:message code='pd.label.doctor.dmd' javaScriptEscape='true'/>";
  plan_display['pd.label.noResultsFound'] = "<spring:message code='pd.label.noResultsFound' javaScriptEscape='true'/>";
  plan_display['pd.label.noResultsFoundDrugs'] = "<spring:message code='pd.label.noResultsFoundDrugs' javaScriptEscape='true'/>";
  plan_display['pd.label.noResults'] = "<spring:message code='pd.label.noResults' javaScriptEscape='true'/>";
  plan_display['pd.label.providerName_1'] = "<spring:message code='pd.label.preferences.providerName.1'/>".replace('&eacute;', 'é');
  plan_display['pd.label.providerName_2'] = "<spring:message code='pd.label.preferences.providerName.2'/>";
  plan_display['pd.label.providerName_3'] = "<spring:message code='pd.label.preferences.providerName.3'/>";
  plan_display['pd.label.preferences.providerDoctor'] = "<spring:message code='pd.label.preferences.providerDoctor'/>";
  plan_display['pd.label.preferences.providerDentist'] = "<spring:message code='pd.label.preferences.providerDentist'/>";
  plan_display['pd.label.preferences.providerHospital'] = "<spring:message code='pd.label.preferences.providerHospital'/>";
  plan_display['pd.label.preferences.selectDosage'] = "<spring:message code='pd.label.selectYourDosage'/>";
  plan_display['pd.label.preferences.availableAs'] = "<spring:message code='pd.label.preferences.availableAs'/>";
  plan_display['pd.label.preferences.thirtyDaySupply'] = "<spring:message code='pd.label.preferences.thirtyDaySupply'/>";
  plan_display['pd.label.preferences.drugAlreadySelected'] = "<spring:message code='pd.label.preferences.drugAlreadySelected'/>";
  plan_display['pd.label.preferences.genericName'] = "<spring:message code='pd.label.preferences.genericName'/>";
  plan_display['pd.label.preferences.fiveDosageAdded'] = "<spring:message code='pd.label.preferences.fiveDosageAdded'/>";
  plan_display['pd.label.preferences.remove'] = "<spring:message code='pd.label.preferences.remove'/>";
  plan_display['pd.label.preferences.removeDosageFor'] = "<spring:message code='pd.label.preferences.removeDosageFor'/>";
  plan_display['pd.label.preferences.dosageSelectionRemoved'] = "<spring:message code='pd.label.preferences.dosageSelectionRemoved'/>";
</script>
<style>
.skip-to-view-header {
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
}
</style>



<!--  <link rel="stylesheet" href="<c:url value="/resources/css/phix-responsive.css" />" type="text/css"> -->
<form class="form" id="preferencesForm" name="preferencesForm" action="<c:url value="/private/preferences" />" method="POST">
	<input type="hidden" id="providersJson" name="providersJson" value='<c:if test="${providersList.size() gt 0}">${providersJson}</c:if>'/>
	<input type="hidden" id="prescriptionSearchRequest" name="prescriptionSearchRequest" value='${prescriptionRequestList}'/>
	<input type="hidden" name="insuranceType" id="insuranceType" value="${insuranceType}" />
	<df:csrfToken/>


	<div class="gutter70">
        <c:if test="${loggedInUser == null && stateCode == 'CA'}">
            <a href="https://${calheersEnv}/lw-shopandcompare/" class="cp-link border-b"><i class="icon-caret-left"></i> <spring:message code="pd.label.title.backToShopAndCompare"/></a>
        </c:if>
        <c:if test="${loggedInUser != null && stateCode == 'CA' && isIndividual}">
            <a href="<c:url value="/indportal#"/>" class="cp-link border-b"><i class="icon-caret-left"></i> <spring:message code="pd.label.title.backTodashboard"/></a>
		</c:if>
	<div class="row-fluid" id="backTodashboard">
		<c:if test="${loggedInUser != null && stateCode == 'MN' && userActiveRoleName != 'issuer_representative'}">
			<a href="<c:url value="/indportal#"/>" class="cp-link border-b"><i class="icon-caret-left"></i> <spring:message code="pd.label.title.backTodashboard"/></a>
		</c:if>
	</div>
	<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
	<div class="row-fluid" id="titlebar">
		<h1><spring:message code="pd.label.title.message28"/></h1>
	</div>
	<div class="row-fluid margin10-t">
		<!--rightpanel-->
        <div id="rightpanel">
            <div class="header pref-header skip-to-view-header">
				<h2>
            		<spring:message code="pd.label.title.message29"/>: (<span class="currentSlide">1</span>/<span class="totalSlides"></span>)
            	</h2>
				<div class="button-controls">
      			<c:if test="${stateCode != 'CA' && stateCode != 'ID'}">
					<input id="skipButton"
                        class="btn btn-primary width-xs-button margin10-b gtm_skip_to_view"
                        onclick="window.dataLayer.push({'event': 'preferencesEvent', 'eventCategory': 'Plan Selection - Pref-Provider Search', 'eventAction': 'Navigational Button Click', 'eventLabel': '<spring:message code="pd.label.commontext.skipToViewPlans"/>'});"
                        type="submit"
                        title="<spring:message code="pd.label.commontext.skipToViewPlans"/>"
                        value="<spring:message code="pd.label.commontext.skipToViewPlans"/>" />
            	</c:if> 
            	<c:if test="${stateCode == 'ID'}">
					<input id="skipButton"
                        class="btn width-xs-button margin10-b gtm_skip_to_view"
                        onclick="window.dataLayer.push({'event': 'preferencesEvent', 'eventCategory': 'Plan Selection - Pref-Provider Search', 'eventAction': 'Navigational Button Click', 'eventLabel': '<spring:message code="pd.label.commontext.skipToViewPlans"/>'});"
                        type="submit"
                        title="<spring:message code="pd.label.commontext.skipToViewPlans"/>"
                        value="<spring:message code="pd.label.commontext.skipToViewPlans"/>" />
            	</c:if>          	
	</div>
            </div>

            <div id="myPereferences" role="region" aria-label="<spring:message code="pd.label.title.message29"/>" class="carousel slide" data-interval="false">
            <div class="gutter10">
			  <div class="carousel-inner">

			  	<div class="item" data-google-tracking-page="Pref - Doctor Search" id="providerSearchItem">
					<div class="fieldset">
						<div class="control-group">
							<span id="ariaSearchFor"><spring:message code="pd.label.preferences.q4.1"/></span>
							<div class="dropdown" style="display:inline">
								<a class="dropdown-toggle btn" aria-haspopup="true" id="providerTypeId" data-toggle="dropdown" href="#" aria-live="assertive" aria-labelledby="ariaSearchFor providerType ariaToSee">
									<span id="providerType">
										<spring:message code="pd.label.preferences.providerDoctor"/>
									</span>
									<i class="icon-caret-down"></i>
								</a>
								<ul id="providerTypeDropdown" class="dropdown-menu">
									<li data-item="doctor">
										<a tabindex="0">
											<i class="icon-check-sign"></i>
											<span class="providerType" data-provider-search-type="doctor"><spring:message code="pd.label.preferences.providerDoctor"/></span>
											<span class="aria-hidden">selected</span>
										</a>
									</li>

									<c:if test="${stateCode != 'CT' && stateCode != 'ID'}">
										<li data-item="dentist">
											<a tabindex="0">
												<i class="icon-check-sign hide"></i>
												<span class="providerType" data-provider-search-type="dentist"><spring:message code="pd.label.preferences.providerDentist"/></span>
												<span class="aria-hidden">Please press enter to select</span>
											</a>
										</li>
									</c:if>

									<c:if test="${stateCode != 'CT'}">
										<li data-item="hospital">
											<a tabindex="0">
												<i class="icon-check-sign hide"></i>
												<span class="providerType" data-provider-search-type="hospital"><spring:message code="pd.label.preferences.providerHospital"/></span>
												<span class="aria-hidden">Please press enter to select</span>
											</a>
										</li>
									</c:if>
								</ul>
							</div>
							<span id="ariaToSee">
								<spring:message code="pd.label.preferences.q4.2"/>
								<span class="aria-hidden">Press enter to open menu, then use up or down arrow key to change provider type. You can add up to 5 providers.</span>
							</span>
							<small class="userProfileForm__hint--small"><spring:message code="pd.label.preferences.providerUpTo5"/></small>
						</div>

						<div class="control-group">
							<input
								id="providerName"
                                class="providerName1 gtm_doctor_search_focus"
								type="text"
								placeholder="<spring:message code="pd.label.preferences.providerName.1"/>"
								aria-label="<spring:message code="pd.label.preferences.providerName.1"/>">

							<select class="input-large gtm-providerDistance" id="providerDistance" name="providerDistance" aria-label="within miles">
								<option value="1"><spring:message code="pd.label.preferences.radius1"/></option>
								<option value="2"><spring:message code="pd.label.preferences.radius2"/></option>
								<option selected value="5"><spring:message code="pd.label.preferences.radius5"/></option>
								<option value="10"><spring:message code="pd.label.preferences.radius10"/></option>
								<option value="20"><spring:message code="pd.label.preferences.radius20"/></option>
								<option value="30"><spring:message code="pd.label.preferences.radius30"/></option>
								<option value="50"><spring:message code="pd.label.preferences.radius50"/></option>
								<option value="100"><spring:message code="pd.label.preferences.radius100"/></option>
							</select>
							&nbsp;&nbsp;<spring:message code="pd.label.preferences.q4.3"/>&nbsp;&nbsp;

								<span aria-live="assertive">
									<input type="text" id="providerZipcode" name="providerZipcode" class="input-mini required" maxlength="5" value="${zipCode == null? '' : zipCode}" pattern="\d+" onpaste="return false" placeholder="<spring:message code="pd.label.preferences.q4.4"/>" aria-label="<spring:message code="pd.label.preferences.q4.4"/>">
								</span>
								<input type="hidden" id="providerZipcode_hidden" name="providerZipcode_hidden" value="${zipCode}">

							<div id="providerSelectedList" class="chzn-container chzn-container-multi row-fluid"  style="min-height: 50px">
								<ul class="chzn-choices providerSelectedListUl" id="providerSelectedListUl" aria-live="rude" aria-relevant="all">
									<c:if test="${providersList.size() gt 0}">
										<c:forEach var="provider" items="${providersList}">

											<li class="search-choice" id="${provider.id}">
												<div class='chzn-header'>
													<c:choose>
														<c:when test="${provider.providerType == 'hospital'}">
															<spring:message code="pd.label.preferences.providerHospital"/>
														</c:when>
														<c:when test="${provider.providerType == 'dentist'}">
															<spring:message code="pd.label.preferences.providerDentist"/>
														</c:when>
														<c:when test="${provider.providerType == 'doctor'}">
															<spring:message code="pd.label.preferences.providerDoctor"/>
														</c:when>
													</c:choose>
													<span><a href="javascript:void(0)" class="search-choice-close" aria-label="Remove ${provider.name}">x</a></span>
												</div>
												<div class="chzn-details">
													<b>
														<c:if test="${provider.providerType == 'doctor' && fn:indexOf(provider.name, 'Dr') != 0 && stateCode == 'CA'}">
															<spring:message code="pd.label.doctor.title"/>
														</c:if>
														<span class="txt-capitalize">${provider.name}</span>
														<!-- <c:if test="${provider.providerType == 'dentist'}"><spring:message code="pd.label.doctor.dmd"/></c:if> -->
													</b></br>

														<c:if test="${provider.specialty != ''}">${provider.specialty}<br></c:if>
														<c:if test="${provider.phone != ''}">${provider.phone}<br></c:if>
														<c:if test="${provider.address != ''}">${provider.address}<br></c:if>
														${provider.city}, ${provider.state} ${provider.zip}
												</div>

											</li>
										</c:forEach>
									</c:if>
								</ul>
							</div>
                            <c:if test="${stateCode != 'ID'}">
    							<p class="alert alert-info margin10-t"><spring:message code="pd.label.preferences.note1"/></p>
    							<p class="small">
    								<spring:message code="pd.label.preferences.note2"/>

    								<c:if test="${stateCode == 'CT' || stateCode == 'MN'}">
    									<spring:message code="pd.label.anonymousAlertInfo" />
    			                  	</c:if>
    		                  	</p>
                            </c:if>
                            <c:if test="${stateCode == 'ID'}">
                                <p class="alert alert-error supporting_err_msg">
                                    <spring:message code="pd.label.noResultsFoundSupportingMsg"/>
                                </p>
                                <p class="alert alert-info margin10-t"><spring:message code="pd.label.preferences.note2"/></p>
                            </c:if>
						</div>

						<div class="button-controls">
							<c:if test="${stateCode != 'CA' && stateCode != 'ID'}">
								<a href="<c:url value="/preeligibility#/" />" class="btn pull-left margin10-b">
									<i class="icon-caret-left"></i> <spring:message code="pd.label.only.back" />
								</a>
							</c:if>

							<c:if test="${stateCode != 'CA'}">
								<button class="btn margin10-b width-xs-button reset"><spring:message code="pd.label.title.message30"/></button>
							</c:if>

							<a
								id="providerSearchNext"
								href="#myPereferences"
								data-slide="next"
								title="<spring:message code="pd.label.next"/>"
                                class="btn btn-primary pull-right margin10-b gtm_doctor_search_next">
                    			<spring:message code="pd.label.next"/>
                    			<i class="icon-caret-right"></i>
                   			</a>
						</div>

					</div>

				</div>



			    <div class="item" data-google-tracking-page="Pref - Medical Service" id="medicalServiceItem">
                    <fieldset>
                        <legend class="margin10-b">
                            <spring:message code="pd.label.preferences.q1.1"/>&nbsp;<a href="#" data-placement="bottom" rel="tooltip" role="tooltip" data-original-title="<spring:message code="pd.label.preferences.tooltip1.3"/>"><spring:message code="pd.label.preferences.q1.2"/></a> <spring:message code="pd.label.preferences.q1.4"/>
                        </legend>
                        <div class="control-group">
                            <div class="span8">
                                <div class="controls radio-ctrls">
                                    <label class="radio" for="sixmonths">
                                    <input type="radio" class="pref gtm_medical_service_usage" name="doctorvisit" id="sixmonths"
                                    value="LEVEL1" <c:if test="${medicalVal == 'LEVEL1'}">checked="checked"</c:if>>
                                    <spring:message code="pd.label.title.1-2Times"/></label>

                                </div>
                                <div class="controls radio-ctrls">
                                    <label class="radio" for="threemonths">
                                    <input type="radio" class="pref gtm_medical_service_usage" name="doctorvisit" id="threemonths"
                                    value="LEVEL2" <c:if test="${medicalVal == 'LEVEL2'}">checked="checked"</c:if>>
                                    <spring:message code="pd.label.title.3-4Times"/></label>

                                </div>
                                <div class="controls radio-ctrls">
                                    <label class="radio" for="everymonth">
                                    <input type="radio" class="pref gtm_medical_service_usage" name="doctorvisit" id="everymonth"
                                    value="LEVEL3" <c:if test="${medicalVal == 'LEVEL3'}">checked="checked"</c:if>>
                                    <spring:message code="pd.label.title.5-11Times"/></label>

                                </div>
                                <div class="controls radio-ctrls">
                                    <label class="radio" for="everytwoweeks">
                                    <input type="radio" class="pref gtm_medical_service_usage" name="doctorvisit" id="everytwoweeks"
                                    value="LEVEL4" <c:if test="${medicalVal == 'LEVEL4'}">checked="checked"</c:if>>
                                    <spring:message code="pd.label.title.moreThan12Times"/></label>

                                </div>
                            </div>
                        </div>
                    </fieldset>

					<c:if test="${stateCode == 'CT' || stateCode == 'MN'}">
	                	<p class="small margin30-t"><spring:message code="pd.label.anonymousAlertInfo" /></p>
                  	</c:if>

                  	<div class="button-controls">
                  		<c:choose>
                  			<c:when test="${loggedInUser == null && stateCode != 'CA'}">
                  				<a href="<c:url value="/preeligibility#/" />" class="btn pull-left margin10-b">
									<i class="icon-caret-left"></i>
									<spring:message code="pd.label.only.back" />
								</a>
                  			</c:when>
                  			<c:when test="${loggedInUser != null && stateCode != 'CA'}">
                  				<c:choose>
	                  				<c:when test="${userActiveRoleName == 'issuer_representative' || userActiveRoleName == 'issuer' || userActiveRoleName == 'issuer_admin'}">
										<a href="<c:url value="/preeligibility#/" />" class="btn pull-left margin10-b">
											<i class="icon-caret-left"></i><spring:message code="pd.label.only.back" />
										</a>
	                  				</c:when>
	                  				<c:otherwise>
	                  					<a href="<c:url value="/indportal#"/>" class="btn pull-left margin10-b">
	                  						<i class="icon-caret-left"></i> <spring:message code="pd.label.title.backTodashboard"/>
	                  					</a>
	                  				</c:otherwise>
                  				</c:choose>
                  			</c:when>

							<c:otherwise>
								<a
									id="medicalServiceBack"
									href="#myPereferences"
									data-slide="prev"
									class="btn pull-left margin10-b gtm_medical_service_back"
									title="<spring:message code="pd.label.only.back" />">
									<i class="icon-caret-left"></i>
									<spring:message code="pd.label.only.back" />
								</a>

							</c:otherwise>
						</c:choose>


						<c:if test="${stateCode != 'CA'}">
							<button class="btn margin10-b width-xs-button reset"><spring:message code="pd.label.title.message30"/></button>
						</c:if>

						<a
							id="medicalUsageNext"
							href="#myPereferences"
							data-slide="next"
							class="btn btn-primary pull-right margin10-b gtm_medical_service_next"
							title="<spring:message code="pd.label.next"/>">

	              			<spring:message code="pd.label.next"/>
	              			<i class="icon-caret-right"></i>
	              		</a>
                  	</div>
			    </div>


			    <div class="item" data-google-tracking-page="Pref - Prescription Drug" >
                  	<fieldset>
	                    <legend class="margin10-b">
		                    <spring:message code="pd.label.preferences.q2.1"/>&nbsp;<a href="#" data-placement="bottom" rel="tooltip" role="tooltip" data-original-title="<spring:message code="pd.label.preferences.tooltip2.3"/>"><spring:message code="pd.label.preferences.q2.2"/></a> <spring:message code="pd.label.preferences.q2.4"/>
	                    </legend>
	                    <div class="control-group">
                            <div class="span8">
		                      	<div class="controls radio-ctrls">
		                      		<label class="radio" for="none">
		                          		<input type="radio" class="pref gtm_prescription_search_usage" name="prescription" id="none" value="LEVEL1"  <c:if test="${drugVal == 'LEVEL1'}">checked="checked"</c:if>> <spring:message code="pd.label.title.0-2"/>
                                  </label>
                              </div>
		                      	<div class="controls radio-ctrls">
		                  			<label class="radio" for="onetofour">
		                          		<input type="radio" class="pref gtm_prescription_search_usage" name="prescription" id="onetofour" value="LEVEL2" <c:if test="${drugVal == 'LEVEL2'}">checked="checked"</c:if>> <spring:message code="pd.label.title.3-4"/>
                                    </label>
                                </div>
		                      	<div class="controls radio-ctrls">
		                      		<label class="radio" for="fivetoten">
		                          		<input type="radio" class="pref gtm_prescription_search_usage" name="prescription"  id="fivetoten" value="LEVEL3"  <c:if test="${drugVal == 'LEVEL3'}">checked="checked"</c:if>> <spring:message code="pd.label.title.5-11"/>
                              </label>
                              </div>
		                      	<div class="controls radio-ctrls">
		                      		<label class="radio" for="morethan10">
		                           		<input type="radio" class="pref gtm_prescription_search_usage" name="prescription"  id="morethan10" value="LEVEL4" <c:if test="${drugVal == 'LEVEL4'}">checked="checked"</c:if>> <spring:message code="pd.label.title.12OrMore"/>
                                    </label>
                              </div>
                            </div>
	                    </div>
                 	</fieldset>

                 	<c:if test="${stateCode == 'CT' || stateCode == 'MN'}">
						<p class="small"><spring:message code="pd.label.anonymousAlertInfo" /></p>
                  	</c:if>

					<div class="button-controls">
						<a
							href="#myPereferences"
							data-slide="prev"
							class="btn pull-left margin10-b gtm_prescription_drug_back"
							title="<spring:message code="pd.label.only.back" />">
							<i class="icon-caret-left"></i>
							<spring:message code="pd.label.only.back" />
						</a>

						<c:if test="${stateCode != 'CA'}">
							<button class="btn margin10-b width-xs-button reset"><spring:message code="pd.label.title.message30"/></button>
						</c:if>

						<a
							id="prescriptionNext"
							href="#myPereferences"
							data-slide="next"
							class="btn btn-primary pull-right margin10-b gtm_prescription_drug_next"
							title="<spring:message code="pd.label.next"/>">

	              			<spring:message code="pd.label.next"/>
	              			<i class="icon-caret-right"></i>
	              		</a>
	              		<button id="prescriptionSubmit" class="btn btn-primary width-xs-button pull-right gtm_prescription_submit hide"> <spring:message code="pd.label.commontext.viewPlans"/> </button>
                  	</div>
				</div>


				<div class="item" data-google-tracking-page="Pref - Prescription Search" id="prescriptionSearchItem" data-automation-id="drug-search-item">
                  <div class="fieldset">
                    <div class="control-group" style="min-height: 150px">
                    	<label for="drugName">
	                    	<spring:message code="pd.label.preferences.drugSearch" />
	                    </label>
                    	<input
                            type="text"
                            id="drugName"
                            class="input-xlarge margin10-t gtm_prescription_search"
                            data-automation-id="drug-name"
                            placeholder="<spring:message code="pd.label.forExample"/>, Lipitor <spring:message code="pd.label.or"/> Atorvastatin">
                    	<c:if test="${drugSearchAPIConfig != 'GI'}">
	                   		<img src="<c:url value="/resources/img/goodrx.png"/>" alt="goodrx logo" style="opacity:0.5;"><br>
						</c:if>

                    	<div id="drugDosagesContainer" data-automation-id="drug-dosages-container" aria-live="rude" aria-relevant="additions"></div>

                    	<div id="selectedDrugsContainer" data-automation-id="selected-drugs-container" class="margin20-b row-fluid" aria-live="rude" aria-relevant="additions removals"></div>
                    </div>

                    <p class="alert alert-info">
	                 	<spring:message code="pd.label.preferences.drugSearchNote"/>
	                </p>
					<c:if test="${stateCode != 'ID'}">
						<p class="small">
							<spring:message code="pd.label.anonymousAlertInfo"/>
						</p>
					</c:if>

					<div class="button-controls clearfix">
						<a
							href="#myPereferences"
							data-automation-id="drug-search-back"
							data-slide="prev"
							title="<spring:message code="pd.label.only.back"/>"
							class="btn pull-left margin10-b gtm_prescription_search_back"
                            onclick="window.dataLayer.push({'event': 'preferencesEvent', 'eventCategory': 'Plan Selection - Pref-Prescription Search', 'eventAction': 'Navigational Button Click', 'eventLabel': '<spring:message code="pd.label.only.back"/>'});"
                            >

							<i class="icon-caret-left"></i>
							<spring:message code="pd.label.only.back"/>
						</a>
						<c:if test="${stateCode != 'CA'}">
							<button class="btn margin10-b width-xs-button reset" data-automation-id="drug-search-reset"  ><spring:message code="pd.label.title.message30"/></button>
						</c:if>

						<a
							id="prescriptionSearchNext"
							data-automation-id="drug-search-next"
							href="#myPereferences"
							data-slide="next"
							class="btn btn-primary pull-right margin10-b gtm_prescription_search_next"
                            onclick="window.dataLayer.push({'event': 'preferencesEvent', 'eventCategory': 'Plan Selection - Pref-Prescription Search', 'eventAction': 'Navigational Button Click', 'eventLabel': '<spring:message code="pd.label.next"/>'});"
							title="<spring:message code="pd.label.next"/>">

							<spring:message code="pd.label.next"/>
							<i class="icon-caret-right"></i>
						</a>

						<button
                            id="prescriptionSearchSubmit"
                            class="btn btn-primary gtm_prescription_search_submit pull-right hide">
                            onclick="window.dataLayer.push({'event': 'preferencesEvent', 'eventCategory': 'Plan Selection - Pref-Prescription Search', 'eventAction': 'Navigational Button Click', 'eventLabel': '<spring:message code="pd.label.commontext.viewPlans"/>'});"
                            <spring:message code="pd.label.commontext.viewPlans"/>
                        </button>
					</div>
                 </div>

				</div>




				<div class="item" id="benefitQuestionsItem">
			    <!-- <h5>Benefit Preferences</h5> -->
                  <fieldset>
                    <legend class="margin10-b">
	                    <spring:message code="pd.label.preferences.q3.1"/>
	 					<span class="alert alert-info margin10-t block"><strong><spring:message code="pd.label.preferences.q3.2"/></strong>:&nbsp;<spring:message code="pd.label.preferences.note"/></span>
					</legend>
                    <div class="control-group benefits clearfix">
                        <div class="controls clearfix">
                   		 <p class="benefits-error alert alert-error hide"> <i class="icon-warning-sign"></i> <spring:message code="pd.label.title.message31"/></p>
                   	  </div>

                      <div class="span6">
						<div class="controls">
	                    	<label class="checkbox">
	                        	<input
                                    type="checkbox"
                                    class="child gtm_benefits"
                                    name="benefits"
                                    id="pediatric"
                                    data-filter-label="<spring:message code="pd.label.title.childrensDental"/>"
                                    value="Pediatric dental" <c:if test="${fn:contains(benefits, 'Pediatric dental')}">checked="checked"</c:if> >
	                        	 <a data-original-title="<p><spring:message code="pd.label.commontext.benefits.pediatricDental.description"/></p>" data-html="true" rel="tooltip" role="tooltip" data-placement="right" href="#"><spring:message code="pd.label.title.childrensDental"/></a>
	                        </label>
						</div>
					  </div>
					  <c:choose>
		            		<c:when test="${stateCode=='MN'}">
		            			<div class="span5">
								<div class="controls">
			                    	<label class="checkbox">
			                        	<input
                                            type="checkbox"
                                            name="benefits"
                                            class="gtm_benefits"
                                            id="hsaEligible"
                                            data-filter-label="<spring:message code="pd.label.title.hsaEligible"/>"
                                            value="HSA Eligible" <c:if test="${fn:contains(benefits, 'HSA Eligible')}">checked="checked"</c:if> >
			                        		<a data-original-title="<p><spring:message code="pd.label.script.networkType.hsa"/></p>" data-html="true" rel="tooltip" role="tooltip" data-placement="top" href="#"><spring:message code="pd.label.title.hsaEligible"/></a>
			                        </label>
								</div>
			                </div>
		            		</c:when>
		            		<c:otherwise>
		            			<div class="span5">
		                    		<div class="controls">
				                		<label class="checkbox">
				                    		<input
                                                type="checkbox"
                                                name="benefits"
                                                class="gtm_benefits"
                                                data-filter-label="<spring:message code="pd.label.title.acupuncture"/>"
                                                value="<spring:message code="pd.label.commontext.benefits.accupuncture"/>"
                                                id="acupuncture" <c:if test="${fn:contains(benefits, 'Acupuncture')}">checked="checked"</c:if>>
                                                <spring:message code="pd.label.title.acupuncture"/>
				                    </label>
								</div>
			                 </div>
		            		</c:otherwise>
		            </c:choose>


                    </div>
                  </fieldset>

					<c:if test="${stateCode == 'CT' || stateCode == 'MN'}">
						<p class="small">
                  			<spring:message code="pd.label.anonymousAlertInfo" />
                  		</p>
					</c:if>

					<div class="button-controls clearfix">
						<a
							href="#myPereferences"
							data-slide="prev"
							class="btn pull-left margin10-b"
							title="<spring:message code="pd.label.only.back"/>">

							<i class="icon-caret-left"></i>
							<spring:message code="pd.label.only.back"/>
						</a>

						<c:if test="${stateCode!='CA'}">
							<button class="btn margin10-b width-xs-button reset"><spring:message code="pd.label.title.message30"/></button>
						</c:if>

						<button id="benefitSubmit" class="btn btn-primary width-xs-button pull-right gtm_benefit_submit"> <spring:message code="pd.label.commontext.viewPlans"/> </button>
					</div>
			    </div>
			  </div>
			  </div>
			  <!-- Carousel nav -->
			</div>
           </div> <!-- /#rightpanel -->
          </div>
		</div>
          <!--gutter10-->
          </div>
        <!--main-->
</form>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script src="<c:url value="/resources/js/preferences.js" />"></script>
<script>
$(document).ready(function () {

	setPageTitle('<spring:message code="pd.label.title.message28"/>');
    
    // Data layer variable to be pushed at the end of shell.jsp
    dl_pageCategory = 'Preferences';

    $('.supporting_err_msg').hide();
	$('#myPereferences').carousel({ wrap: false, interval: false});
    function getParameterByName(key) {
      key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
      var match = location.search.match(new RegExp("[?&]" + key + "=([^&]+)(&|$)"));
      var slide = match && decodeURIComponent(match[1].replace(/\+/g, " "));
      if (Math.floor(slide) == slide && $.isNumeric(slide))
          return parseInt(slide);
      else
          return 0;
    }

    var slideNumber = getParameterByName('slide');

    $('#myPereferences').carousel(slideNumber);

    // $('.radio').each(function(){
    //     $
    // });

	$("#preferencesForm").validate({
		rules: {
			providerZipcode : {
				required: true,
				digits: true,
				zipCodecheck: true,
				maxlength: 5
			}
		},
		messages: {
			providerZipcode: "<span id='providerZipcode_error'> <em class='excl'>!</em><spring:message code='label.validateZipSyntax' javaScriptEscape='true'/></span>"
		}
	});
    $("#providerDistance").change(function(){
        var selectedRadius = $(this).find("option:selected").text();
        window.dataLayer.push({
            'event': 'providerSearchEvent',
            'eventCategory': 'Plan Selection - Provider Search',
            'eventAction': 'Radius Selection',
            'eventLabel': selectedRadius
        });
    });

	jQuery.validator.addMethod("zipCodecheck", function(value, element, param) {
		zip = $('#providerZipcode').val().trim();
		if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || (zip == '00000') )){
			return false;
		}
		return true;
	});

	$(".js-skip").click(function(e) {
		e.preventDefault();
		var slideNum = $("#myPereferences .item.active").index() + 1;
		if(slideNum == "1") {
			$('#myPereferences').carousel(2);
		}
		if(slideNum == "2") {
			$('#myPereferences').carousel(2);
		}
		if(slideNum == "3") {
			window.location.href = '/hix/private/planselection?insuranceType=HEALTH';
		}
	});
});
</script>
