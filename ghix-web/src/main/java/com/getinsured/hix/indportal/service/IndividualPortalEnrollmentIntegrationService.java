package com.getinsured.hix.indportal.service;

/**
 * @author Sunil Desu
 *
 */
public interface IndividualPortalEnrollmentIntegrationService {
	
	public boolean hasActiveEnrollment(String caseNumber);

}
