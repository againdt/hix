<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<style type="text/css">
h3 {
	line-height: 25px;
	margin: 0
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$('.info').tooltip();
	});

	function gotoparent() {
		window.top.location.href = document.getElementById("redirectLink").value;
	}
</script>

<form name="adminorderconfirm" id="adminorderconfirm">
	<input type="hidden" name="redirectLink" id="redirectLink"
		value="${redirectLink}">
	<div class="gutter40" id="rightpanel">
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(enrErrorMsg,'SUCCESS')}">
		<div class="gutter10">
			<h3>
				<spring:message code='enroll.orderConfirm.confirm' />
			</h3>
			<p>
				<spring:message code='enroll.adminConfirm.value' />
			</p>
		</div>


			</c:when>
			<c:otherwise>
				<div class="gutter10" style="font-weight: bold; font-size: medium; color: red;">
					<p>
						<spring:message code='enroll.adminConfirm.error' />
					<p />
					<p>
						Time: <c:out value="${enrErrorTime}"></c:out>
					</p>
					<br>
				</div>
			</c:otherwise>
		</c:choose>


		<%-- 
			The following code is been removed as per Jan's mail on 3 sep 2013 
			"Let's remove the Continue button. "
			HIX-17088
			
			
			<div class="form-actions margin-top50 form-horizontal">
				<a href="#" class="btn"><spring:message
					code='enroll.orderConfirm.overView' /></a>
			</div> 
		
		--%>

		<%-- HIX-36118 Add Continue Button to Admin Update Confirmation Page--%>
		<a href="#" title="<spring:message code='enroll.orderConfirm.overView' />" onclick="gotoparent()" class="btn btn-primary pull-right">
			<spring:message code='enroll.orderConfirm.overView' />
		</a>
	</div>

</form>