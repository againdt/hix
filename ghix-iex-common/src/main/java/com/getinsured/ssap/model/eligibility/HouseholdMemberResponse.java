package com.getinsured.ssap.model.eligibility;

import java.util.Date;
import java.util.StringJoiner;

/**
 * Describes household member response.
 *
 * @author Yevgen Golubenko
 * @since 4/4/19
 */
public class HouseholdMemberResponse
{
  private long applicantId;
  private boolean aptcEligible;
  private boolean csrEligible;

  private String csrLevel;
  private Date csrEligibilityStartDate;
  private Date csrEligibilityEndDate;

  private boolean chip;
  private Date chipEligibilityStartDate;
  private Date chipEligibilityEndDate;

  private boolean magiDetermination;

  private boolean medicaid;

  private boolean exchangeEligible;
  private Date exchangeEligibilityStartDate;
  private Date exchangeEligibilityEndDate;

  private String memberIneligibilityReason;
  private String memberDecisionPath;

  public long getApplicantId() {
    return applicantId;
  }

  public void setApplicantId(long applicantId) {
    this.applicantId = applicantId;
  }

  public boolean isAptcEligible() {
    return aptcEligible;
  }

  public void setAptcEligible(boolean aptcEligible) {
    this.aptcEligible = aptcEligible;
  }

  public boolean isCsrEligible() {
    return csrEligible;
  }

  public void setCsrEligible(boolean csrEligible) {
    this.csrEligible = csrEligible;
  }

  public String getCsrLevel() {
    return csrLevel;
  }

  public void setCsrLevel(String csrLevel) {
    this.csrLevel = csrLevel;
  }

  public Date getCsrEligibilityStartDate() {
    return csrEligibilityStartDate;
  }

  public void setCsrEligibilityStartDate(Date csrEligibilityStartDate) {
    this.csrEligibilityStartDate = csrEligibilityStartDate;
  }

  public Date getCsrEligibilityEndDate() {
    return csrEligibilityEndDate;
  }

  public void setCsrEligibilityEndDate(Date csrEligibilityEndDate) {
    this.csrEligibilityEndDate = csrEligibilityEndDate;
  }

  public boolean isChip() {
    return chip;
  }

  public void setChip(boolean chip) {
    this.chip = chip;
  }

  public Date getChipEligibilityStartDate() {
    return chipEligibilityStartDate;
  }

  public void setChipEligibilityStartDate(Date chipEligibilityStartDate) {
    this.chipEligibilityStartDate = chipEligibilityStartDate;
  }

  public Date getChipEligibilityEndDate() {
    return chipEligibilityEndDate;
  }

  public void setChipEligibilityEndDate(Date chipEligibilityEndDate) {
    this.chipEligibilityEndDate = chipEligibilityEndDate;
  }

  /**
   * If {@link #medicaid} eligibility is based on the Income,
   * this will return {@code true}. If it's based on other information,
   * it will return {@code false}.
   * @return boolean if {@link #medicaid} is based on the Income or not.
   */
  public boolean isMagiDetermination() {
    return magiDetermination;
  }

  public void setMagiDetermination(boolean magiDetermination) {
    this.magiDetermination = magiDetermination;
  }

  public boolean isMedicaid() {
    return medicaid;
  }

  public void setMedicaid(boolean medicaid) {
    this.medicaid = medicaid;
  }

  public boolean isExchangeEligible() {
    return exchangeEligible;
  }

  public void setExchangeEligible(boolean exchangeEligible) {
    this.exchangeEligible = exchangeEligible;
  }

  public Date getExchangeEligibilityStartDate() {
    return exchangeEligibilityStartDate;
  }

  public void setExchangeEligibilityStartDate(Date exchangeEligibilityStartDate) {
    this.exchangeEligibilityStartDate = exchangeEligibilityStartDate;
  }

  public Date getExchangeEligibilityEndDate() {
    return exchangeEligibilityEndDate;
  }

  public void setExchangeEligibilityEndDate(Date exchangeEligibilityEndDate) {
    this.exchangeEligibilityEndDate = exchangeEligibilityEndDate;
  }

  public String getMemberIneligibilityReason() {
    return memberIneligibilityReason;
  }

  public void setMemberIneligibilityReason(String memberIneligibilityReason) {
    this.memberIneligibilityReason = memberIneligibilityReason;
  }

  public String getMemberDecisionPath() {
    return memberDecisionPath;
  }

  public void setMemberDecisionPath(String memberDecisionPath) {
    this.memberDecisionPath = memberDecisionPath;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", HouseholdMemberResponse.class.getSimpleName() + "[", "]")
        .add("applicantId=" + applicantId)
        .add("aptcEligible=" + aptcEligible)
        .add("csrEligible=" + csrEligible)
        .add("csrLevel='" + csrLevel + "'")
        .add("csrEligibilityStartDate=" + csrEligibilityStartDate)
        .add("csrEligibilityEndDate=" + csrEligibilityEndDate)
        .add("chip=" + chip)
        .add("chipEligibilityStartDate=" + chipEligibilityStartDate)
        .add("chipEligibilityEndDate=" + chipEligibilityEndDate)
        .add("magiDetermination=" + magiDetermination)
        .add("medicaid=" + medicaid)
        .add("exchangeEligible=" + exchangeEligible)
        .add("exchangeEligibilityStartDate=" + exchangeEligibilityStartDate)
        .add("exchangeEligibilityEndDate=" + exchangeEligibilityEndDate)
        .add("memberIneligibilityReason='" + memberIneligibilityReason + "'")
        .add("memberDecisionPath='" + memberDecisionPath + "'")
        .toString();
  }
}
