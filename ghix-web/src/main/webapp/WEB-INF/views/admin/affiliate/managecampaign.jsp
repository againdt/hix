<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<script type="text/javascript" src="<:url value="/resources/js/affiliate/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/affiliate/parsley.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/affiliate/highlight.min.js" />"></script>

<%@ page isELIgnored="false"%>

<style type="text/css">
	.ui-autocomplete li {
		padding: 0 5px !important;
		width: 152px !important;
		word-break: break-all;
		word-wrap: break-word;
	}
</style>
<div class="gutter10">	
    <div class="row-fluid">
        <ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li>Manage Campaign</li>
        </ul><!--page-breadcrumb ends-->
            <h1>Manage Campaign</h1>
    </div>	
    
    <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
   </div>
    
    <div class="row-fluid">
		<div id="sidebar" class="span3 ">
			<div class="gray">
            	<form class="form-vertical" id="frmsearchCampaign" name="frmsearchCampaign" action="<c:url value="/admin/affiliate/managecampaign" />" method="POST">
                	<h4 class="margin0"><spring:message code='label.refineresult' /></h4>
                    <div class="gutter10 graybg">
                    	<div class="control-group">
                            <label for="campaignName" class="control-label">Campaign Name</label>
                            <div class="controls compnayAutoComp">
                                <input class="input-medium" type="text" value="${searchCriteria.campaignName}" data-maxlength="64" name="campaignName" id="campaignName"  size="20"/>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                    	 <div class="control-group"> 
                             <label for="affiliateId" class="control-label">Affiliate ID</label>
                             <div class="controls"> 
                                <input class="input-medium" type="text" value="${searchCriteria.affiliateId}" maxlength="10" data-type="number" name="affiliateId" id="affiliateId"  size="20"/>
                            </div> <!--end of controls -->
                         </div><!--end of control-group -->
                        <div class="control-group">
                            <label for="companyName" class="control-label">Company Name</label>
                            <div class="controls compnayAutoComp">
                                <input class="input-medium" type="text" value="${searchCriteria.company}" data-maxlength="64" name="company" id="company"  size="20"/>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                         <div class="control-group">
                            <label for="startDate" class="control-label">Start Date</label>
                            <div class="controls">
                                <input type="text" title="mm/dd/yyyy" value="${searchCriteria.startDate}" class="datepick input-medium"   name="startDate" id="startDate" />
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                        <div class="control-group">
                            <label for="endDate" class="control-label">End Date</label>
                            <div class="controls">
                                <input type="text" title="mm/dd/yyyy" value="${searchCriteria.endDate}" class="datepick input-medium" name="endDate" id="endDate" />
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                                               
                       <div class="control-group">
                            <label for="amount" class="control-label">Amount</label>
                            <div class="controls">
                                <input class="input-medium" type="text" value="${searchCriteria.amount}" data-type="number" name="amount" id="amountValue" maxlength="9" size="20"/>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                        <div class="txt-center">
 						<input type="submit" class="btn" value="<spring:message  code='label.submit'/>">
 						</div>
                        
                    </div>
                </form>
            </div>
		</div><!-- span3 ends-->
        
        <div class="span9 " id="rightpanel">
        	<form class="form-horizontal" id="frmsearchCampaign" name="frmsearchCampaign" method="POST">
            	<c:choose>
					<c:when test="${submit != '' || resultSize >= 1}">
                        <c:choose>
                            <c:when test="${fn:length(campaignList) > 0}">
                                <table class="table">
                                    <thead>
                                        <tr class="graydrkbg">
                                             <th scope="col"><dl:sort title="Campaign Name" sortBy="campaign.campaignName" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
<%--                                              <th scope="col"><dl:sort title="Affiliate Id" sortBy="affiliate.affiliateId" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th> --%>
                                             <th scope="col"><dl:sort title="Company Name" sortBy="affiliate.companyName" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col"><dl:sort title="Start Date" sortBy="campaign.startDate" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col"><dl:sort title="End Date" sortBy="campaign.endDate" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col"><dl:sort title="Amount" sortBy="campaign.amount" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th></th>
                                        </tr>
                                    </thead>
                                    <c:forEach items="${campaignList}" var="campaign">
                                            <tr>
                                           		 <td>${campaign.campaigncampaignName}</td>
<%--                                                 <td>${campaign.affiliateaffiliateId} </td> --%>
                                                <td>${campaign.affiliatecompanyName}</td>
                                                <td>${campaign.campaignstartDate} </td>
                                                 <td>${campaign.campaignendDate} </td>
                                                <td>${campaign.campaignamount}</td>
                                                 <td>
			                                        <div class="dropdown">
			                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cog"></i><i class="caret"></i></a>
			                                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			                                                <li><a href="<c:url value="/admin/affiliate/viewcampaign/${campaign.affiliateflowId}/${campaign.affiliateaffiliateId}" />"><i class="icon-refresh"></i><spring:message  code='label.viewCampaign'/></a></li>
			                                                <li><a href="<c:url value="/admin/affiliate/editcampaign/${campaign.affiliateflowId}/${campaign.affiliateaffiliateId}" />"><i class="icon-pencil"></i><spring:message  code='label.editCampaign'/></a></li>
			                                            </ul>
			                                        </div>
				                                 
				                                </td>
                                            </tr>
                                        </c:forEach>
                                </table>
                                <div class="center">
                                    <dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
                                </div>
                             </c:when>
                            <c:otherwise>
                               <h4 class="alert alert-info margin0"><spring:message  code='label.norecords'/></h4>
                            </c:otherwise>
                        </c:choose>	
		 			</c:when>
				</c:choose>
            </form>
		</div><!--span9 ends-->
	</div>	

    <div class="notes" style="display: none">
        <div class="row">
            <div class="span">

                <p>The prototype showcases three scenarios (A, B and C) dependent on
                    a particular Employer's eligibility to use the SHOP Exchange.</p>
            </div>
        </div>
    </div>

</div> <!-- row-fluid -->


<script type="text/javascript">
$(document).ready(function(){
	$("#startDate").datepicker({
	showOn : "button",
	buttonImage : "${pageContext.request.contextPath}"+ '/resources/images/calendar.gif',
	buttonImageOnly : true,
	buttonText: "",
	numberOfMonths: 1,
	onSelect: function(selected) {
	$("#endDate").datepicker("option","minDate", selected)
	}
	});
	$("#endDate").datepicker({
	showOn : "button",
	buttonImage : "${pageContext.request.contextPath}"+ '/resources/images/calendar.gif',
	buttonImageOnly : true,
	buttonText: "",
	numberOfMonths: 1,
	onSelect: function(selected) {
	$("#startDate").datepicker("option","maxDate", selected)
	}
	}); 
	});
	
$('#frmsearchCampaign').parsley( 'addListener', {
	onFormSubmit: function (isFormValid, event, ParsleyForm) {
		// if field is not visible, do not apply Parsley validation!
		if(isFormValid === "true"){
			var url='<c:url value="/admin/affiliate/managecampaign" />';
			 $.post(url,function() {
				location.href='<c:url value="/admin/affiliate/managecampaign" />'
			 });
		}
	} 
});

$('#frmsearchCampaign').submit(function(){
	var re = /^\d{0,6}(\.\d{0,2})?$/;
	if (document.getElementById('amountValue').value.match(re)) {
		$('#amountValue').parent().find('.requiredError').remove();
	} else {
		$('#amountValue').parent().find('.requiredError').remove();
		$('#amountValue').parent().append('<p class="requiredError">Amount should be in form of xxxxxx.xx</p>');
		return false;
	}
});

var company_list ='${companyList}';
var companyArr = company_list.slice(1,company_list.length-1);
var companyData = companyArr.split(',');
$( "#company" ).autocomplete({
    source: companyData
});


var campaignName_list ='${campaignNameList}';
var campaignNameArr = campaignName_list;
var campaignNameData = campaignNameArr.split('|');
$( "#campaignName" ).autocomplete({
    source: campaignNameData
});

var affiliateId_list ='${affiliateIdList}';
var affiliateIdArr = affiliateId_list.slice(1,affiliateId_list.length-1);
var affiliateIdData = affiliateIdArr.split(',');
$( "#affiliateId" ).autocomplete({
    source: affiliateIdData
});

/*disable all the keypress except for numbers*/
$('#affiliateId').keypress(function(e){
  	if (window.event) { var charCode = window.event.keyCode; }
    else if (e) { var charCode = e.which; }
    else { return true; }
    if (charCode > 31 && (charCode < 48 || charCode > 57)) { return false; }
    return true;
  
});

</script>