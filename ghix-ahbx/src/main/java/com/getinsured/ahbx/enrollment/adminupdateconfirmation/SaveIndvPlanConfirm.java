//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.04.28 at 12:16:18 PM IST 
//


package com.getinsured.ahbx.enrollment.adminupdateconfirmation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for saveIndvPlanConfirm complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="saveIndvPlanConfirm">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="individualPlanConfirmationRequest" type="{http://impl.webservice.ahbx.accenture.com/}individualPlanConfirmationRequestDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "saveIndvPlanConfirm", propOrder = {
    "individualPlanConfirmationRequest"
})
public class SaveIndvPlanConfirm {

    protected IndividualPlanConfirmationRequestDTO individualPlanConfirmationRequest;

    /**
     * Gets the value of the individualPlanConfirmationRequest property.
     * 
     * @return
     *     possible object is
     *     {@link IndividualPlanConfirmationRequestDTO }
     *     
     */
    public IndividualPlanConfirmationRequestDTO getIndividualPlanConfirmationRequest() {
        return individualPlanConfirmationRequest;
    }

    /**
     * Sets the value of the individualPlanConfirmationRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndividualPlanConfirmationRequestDTO }
     *     
     */
    public void setIndividualPlanConfirmationRequest(IndividualPlanConfirmationRequestDTO value) {
        this.individualPlanConfirmationRequest = value;
    }

}
