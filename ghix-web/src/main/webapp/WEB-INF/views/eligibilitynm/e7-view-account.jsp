
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>View account</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link type="text/css" rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<link rel="stylesheet" href="../resources/css/eligibility.css" type="text/css">



<div class="row-fluid" id="main">
    <div class="row-fluid" id="titlebar">
      <div class=" span3">
         <a href="e1-1-start-app" class="btn btn-link">Back </a> 
      </div>
      <div class="span9 pull-left">
        <h1>Your Account Dashboard</h1>
      </div>
    </div>
    
    <!--titlebar-->
    <div class="row-fluid">
      <div class="span3 gutter10" id="sidebar">
      <div class="gutter10">
        <ul class="nav nav-tabs nav-stacked"  data-spy="affix">
          <li class="active"><a href="#">Overview <i class="icon-chevron-right pull-right"></i></a></li>
          <li><a href="e7-enrollment-history">Enrollment History <i class="icon-chevron-right pull-right"></i></a></li>
          <li><a href="#">Household Profile<i class="icon-chevron-right pull-right"></i></a></li>
          <li><a href="e8-changes-in-circumstance">Change in Circumstance <i class="icon-chevron-right"></i></a></li>
          <li><a href="#">Inbox <i class="icon-chevron-right pull-right"></i></a></li>
          <li><a href="#">Account Settings <i class="icon-chevron-right pull-right"></i></a></li>
        </ul>
        </div><!-- gutter10 -->
      </div>
      <!--sidebar-->
      <div class="span9" id="rightpanel">
        <div class="margin-right"> <!-- gutter10 -->
        <h3>Account Overview</h3>

<div class="alert">
  <a href="e4-household-income"><button type="button" title="x" class="close" data-dismiss="alert">x</button> <button class=" btn btn-small  pull-right margin-left">Complete</button></a>
  You started your application for financial assistance, but have not submitted it. Your application will expire if you do not submit it before December 7, 2014.
</div>

<div class="alert">
  <a href="e1-1-start-app"><button type="button" title="x" class="close" data-dismiss="alert">x</button><button class=" btn btn-small  pull-right">Continue</button></a>
  Please make sure to complete the eligibility redetermination process before the annual open enrollment period ends on December 7, 2014.
</div>

<div class="alert">
  <button type="button" title="x" class="close" data-dismiss="alert">x</button> <button class=" btn btn-small  pull-right">More Info</button>
  Your verification period ends on March 7, 2015. Please provide supporting documentation regarding your incarceration status before then or risk being disenrolled or suspended from coverage.
</div>

  <p>Your account dashboard contains all of your application, eligibility and health coverage information. Check here for updates from the New Mexico Exchange and to report any changes that may affect your eligibility.</p>

      <!--
        <h5>2014 Enrollment Progress</h5>
        <p>Your application is complete. Stan Jacobs, Janet Jacobs and Joe Jacobs have been determined eligible for health coverage and help paying for health coverage. However, coverage can't begin until they have selected a health plan.</p>
        <img src="../resources/eligibilitynm/img/progress.png" alt="Progress bar">

        <form class="form-vertical">
          <div class="well">
            Your Current Eligibility Determination <a href="e9-confirmation" class="btn"><i class="icon-file"></i></a><button class="btn offset3">Complete Enrollment</button>
          </div>
        </form>
      -->

    <!--gutter10-->
  </div>
  <!--rightpanel-->
</div>
<!--row-fluid-->
</div>
<!--main-->
</div>


<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr.custom.js"></script>
<script src="../resources/eligibilitynm/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr-2.0.6.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/waypoints.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/jquery.multiselect.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/ghixcustom.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-modal.js"></script>
<script>
$(document).ready(function(){
$('.qualify').hide();
$('#qualify-coverage').modal('hide');
$('#sidebar').affix();
$("input[name='immigration']").change(function(){
	$('.immigration').fadeToggle(this.value == "Yes");
});
$('.date').datepicker();
});
</script>

