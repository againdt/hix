package com.getinsured.hix.dst;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.SecurityConfiguration;

@Controller
@RequestMapping(value="/dst")
public class DstController 
{

	private static final Logger LOGGER = LoggerFactory.getLogger(DstController.class);
	
	@Value("#{configProp}")
	private Properties properties;

	@RequestMapping(value="/index", method = RequestMethod.GET)
	public String showIndex(final HttpServletRequest request, final Model model) {
		
		String logo = "";
		if(StringUtils.isNotEmpty(DynamicPropertiesUtil.getPropertyValue("ui.BrandFile"))){
			logo = DynamicPropertiesUtil.getPropertyValue("ui.BrandFile");
		}
		model.addAttribute("logo", logo);
		
		String brandName = "";
		if(StringUtils.isNotEmpty(DynamicPropertiesUtil.getPropertyValue("ui.BrandName"))){
			brandName = DynamicPropertiesUtil.getPropertyValue("ui.BrandName");
		}
		model.addAttribute("brandName", brandName);
		
		String favicon = "";
		if(StringUtils.isNotEmpty(DynamicPropertiesUtil.getPropertyValue("ui.faviconIcon"))){
			favicon = DynamicPropertiesUtil.getPropertyValue("ui.faviconIcon");
		}
		model.addAttribute("favicon", favicon);
		
		String GTMContainerID = "";
		if(StringUtils.isNotEmpty(properties.getProperty("GTMContainerID"))){
			GTMContainerID = properties.getProperty("GTMContainerID").trim();
		}		
		model.addAttribute("GTMContainerID", GTMContainerID);
		
		String ghixEnvironment = "PROD";
		if(StringUtils.isNotEmpty(properties.getProperty("ghixEnvironment"))){
			ghixEnvironment = properties.getProperty("ghixEnvironment").trim();
		}
		
		String dstCancelRedirectUrl = "";
		if(StringUtils.isNotEmpty(DynamicPropertiesUtil.getPropertyValue("planSelection.dstCancelRedirectUrl"))){
			dstCancelRedirectUrl = DynamicPropertiesUtil.getPropertyValue("planSelection.dstCancelRedirectUrl");
		}
		
		String dstSubmitRedirectUrl = "";
		if(StringUtils.isNotEmpty(DynamicPropertiesUtil.getPropertyValue("planSelection.dstSubmitRedirectUrl"))){
			dstSubmitRedirectUrl = DynamicPropertiesUtil.getPropertyValue("planSelection.dstSubmitRedirectUrl");
		}
		
		if(!"PROD".equalsIgnoreCase(ghixEnvironment) && StringUtils.isNotBlank(request.getParameter("redirectDomain"))){
			dstCancelRedirectUrl = request.getParameter("redirectDomain")+"/ReturnToHPFOnCancel";
			dstSubmitRedirectUrl = request.getParameter("redirectDomain")+"/ReturnToHPFOnSuccess";
		}
		if(StringUtils.isNotBlank(request.getParameter("appflow"))){
			if(dstCancelRedirectUrl.contains("?")){
				dstCancelRedirectUrl += "&appflow="+request.getParameter("appflow");
			}else{
				dstCancelRedirectUrl += "?appflow="+request.getParameter("appflow");
			}
			if(dstSubmitRedirectUrl.contains("?")){
				dstSubmitRedirectUrl += "&appflow="+request.getParameter("appflow");
			}else{
				dstSubmitRedirectUrl += "?appflow="+request.getParameter("appflow");
			}
			
		}
		model.addAttribute("dstCancelRedirectUrl", dstCancelRedirectUrl);
		model.addAttribute("dstSubmitRedirectUrl", dstSubmitRedirectUrl);
		
		String clientInactivityThreshold = "";
		if(StringUtils.isNotEmpty(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.CLIENT_INACTIVITY_THRESHOLD))){
			clientInactivityThreshold = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.CLIENT_INACTIVITY_THRESHOLD);
		}
		model.addAttribute("clientInactivityThreshold", clientInactivityThreshold);
		
		String popupThresholdValue = "";
		if(StringUtils.isNotEmpty(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.SESSION_POPUP_THRESHOLD))){
			popupThresholdValue = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.SESSION_POPUP_THRESHOLD);
		}
		model.addAttribute("popupThresholdValue", popupThresholdValue);
		
		return "dst/index";
	}

}
