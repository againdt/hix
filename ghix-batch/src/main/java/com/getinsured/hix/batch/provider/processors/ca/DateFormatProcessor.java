package com.getinsured.hix.batch.provider.processors.ca;

import com.getinsured.hix.batch.provider.InvalidOperationException;
import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatProcessor implements ProviderDataFieldProcessor {
    private ValidationContext context;
    private int index;
    private Logger logger = LoggerFactory.getLogger(DateFormatProcessor.class);

    @Override
    public void setIndex(int idx) {
        this.index = idx;
    }

    @Override
    public int getIndex() {
        return this.index;
    }

    @Override
    public void setValidationContext(ValidationContext validationContext) {
        this.context = validationContext;
    }

    @Override
    public ValidationContext getValidationContext() {
        return this.context;
    }

    @Override
    public Object process(Object objToBeValidated) throws ProviderValidationException, InvalidOperationException {
        String format = context.getNamedConstraintField("format");
        if (format == null) {
            throw new ProviderValidationException("Attribute 'format' required for this field. Please check your metadata file.");
        }

        Date date;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            sdf.setLenient(false);
            String dateToBeValidated = (String) objToBeValidated;
            date = sdf.parse(dateToBeValidated);
        } catch (ParseException e) {
            throw new ProviderValidationException("Date validation failed. Given date does match the set format.");
        }

        return date;
    }
}
