package com.getinsured.hix.batch.rrv.writer;

import com.getinsured.eligibility.model.RrvSubmission;
import com.getinsured.eligibility.rrv.service.RrvService;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class RrvInboundWriter implements ItemWriter<RrvSubmission> {

    @Autowired
    RrvService rrvService;

    @Override
    public void write(List<? extends RrvSubmission> list) throws Exception {
        rrvService.getVerificationResult(list);
    }

    public RrvService getRrvService() {
        return rrvService;
    }

    public void setRrvService(RrvService rrvService) {
        this.rrvService = rrvService;
    }
}