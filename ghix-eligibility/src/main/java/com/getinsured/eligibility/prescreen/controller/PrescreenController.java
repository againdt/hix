/**
 * 
 */
package com.getinsured.eligibility.prescreen.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.prescreen.repository.IPrescreenDataRepository;
import com.getinsured.eligibility.prescreen.service.EligibilityPrescreenService;
import com.getinsured.hix.model.prescreen.PrescreenData;
import com.getinsured.hix.model.prescreen.calculator.FplRequest;
import com.getinsured.hix.model.prescreen.calculator.FplResponse;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenDTO;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenRequest;
import com.knappsack.swagger4springweb.annotation.ApiExclude;

/**
 * This is the entry point for clients. From Controller we call service to get
 * the things done.
 * 
 * @author Nikhil Talreja
 * @since 19 April, 2013
 */
@Controller
@RequestMapping("/eligibility")
public class PrescreenController {
	
	private static final Logger LOGGER = Logger.getLogger(PrescreenController.class);
	
	@Autowired private EligibilityPrescreenService eligibilityPrescreenService;
	@Autowired private IPrescreenDataRepository iPrescreenDataRepository;
	
	/**
	 * This method calculates APTC for a household
	 * This is a RESTful service for eligibility Prescreen
	 * 
	 * @author Nikhil Talreja
	 * @since April 19, 2013
	 * @param aptcRequest
	 *            Houshehold for which APTC is to be calculated
	 * @return double - APTC value
	 */
	@ApiExclude
	@RequestMapping(value = "/calculateAptc", method = RequestMethod.POST)
	@ResponseBody
	public PrescreenRequest calculateAptc(@RequestBody PrescreenRequest aptcRequest){
		LOGGER.info("Calling Calculate APTC service");
		return eligibilityPrescreenService.calculateAptc(aptcRequest);
	}
	
	/**
	 * This method computes CSR
	 * 
	 * This is a RESTful service for eligibility Prescreen
	 * 
	 * @author Nikhil Talreja
	 * @since April 19, 2013
	 * @param fpl
	 *            percentage for which the CSR has to be evaluated
	 * @return csr
	 */
	@ApiExclude
	@RequestMapping(value = "/computeCSR", method = RequestMethod.POST)
	@ResponseBody
	public String computeCSR(@RequestBody double fpl){
		LOGGER.info("Calling Compute CSR service");
		return eligibilityPrescreenService.computeCSR(fpl);
	}
	
	/**
	 * This method calculates MAGI
	 * 
	 * This is a RESTful service for eligibility Prescreen
	 * 
	 * @author Nikhil Talreja
	 * @since April 19, 2013
	 * 
	 * @param prescreenHousehold
	 * @param prescreenDeductions
	 * @return magiValue which is the calculated MAGI income value
	 */
	@ApiExclude
	@RequestMapping(value = "/calculateMagi", method = RequestMethod.POST)
	@ResponseBody
	public double calculateMagi(@RequestBody PrescreenRequest aptcRequest){
		LOGGER.info("Calling Calculate MAGI service");
		return eligibilityPrescreenService.calculateMagi(aptcRequest);
	}
	
	/**
	 * This is the method for the calculation of FPL.
	 * 
	 * This is a RESTful service for eligibility Prescreen
	 * 
	 * @author Nikhil Talreja
	 * @since April 19, 2013
	 * 
	 * @param fplRequest
	 *            is the request object which contains the required parameters
	 *            for FPL Calculation. State, family size and family income are
	 *            required and cannot be {@code null}.
	 * 
	 * @return fplResponse is the response object which contains
	 *         <ol>
	 *         <li>the percentage value of FPL
	 *         <li>error message, which is populated if the processing results
	 *         in an error condition
	 *         </ol>
	 */
	@ApiExclude
	@RequestMapping(value = "/calculateFpl", method = RequestMethod.POST)
	@ResponseBody
	public FplResponse calculateFpl(@RequestBody FplRequest fplRequest){
		LOGGER.info("Calling Calculate MAGI service");
		return eligibilityPrescreenService.calculateFpl(fplRequest);
	}
	
	@ApiExclude
	@RequestMapping(value = "/savePrescreenRecord", method = RequestMethod.POST)
	@ResponseBody
	public PrescreenData savePrescreenRecord(@RequestBody PrescreenData record){
		return eligibilityPrescreenService.savePrescreenRequestRecord(record);
	}
	
	@ApiExclude
	@RequestMapping(value = "/getPrescreenRecord", method = RequestMethod.POST)
	@ResponseBody
	public PrescreenData getPrescreenRecord(@RequestBody long id){
		return eligibilityPrescreenService.getPrescreenRequestRecord(id);
	}
	
	@ApiExclude
	@RequestMapping(value = "/getPrescreenRecordBySessionId", method = RequestMethod.POST)
	@ResponseBody
	public PrescreenData getPrescreenRecordBySessionId(@RequestBody String sessionId){
		return eligibilityPrescreenService.getPrescreenRecordBySessionId(sessionId);
	}
	
	@ApiExclude
	@RequestMapping(value = "/adminHome", method = RequestMethod.GET)
	public String getAdminPage(Model model) {
		LOGGER.info("Fetching admin home page");
		return "admin/eligibility/adminHome";
	}
	
	@ApiExclude
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String getMainPage(Model model) {
		LOGGER.info("Fetching admin main page");
		return "admin/eligibility/main";
	}
	
	@ApiExclude
	@RequestMapping(value = "/getData", method = RequestMethod.GET)
	public String getPrescreenData(Model model) {
		LOGGER.info("Fetching Prescreen Records");
		model.addAttribute("result", iPrescreenDataRepository.findAll());
		return "admin/eligibility/getData";
	}
	
	/**
	 * This method calculates APTC(Product Version) for a household
	 * This is a RESTful service for eligibility Prescreen
	 * 
	 * @author Sunil Desu
	 * @since June 06, 2013
	 * @param aptcRequest
	 *            Houshehold for which APTC is to be calculated
	 * @return double - APTC value
	 */
	@ApiExclude
	@RequestMapping(value = "/getAptc", method = RequestMethod.POST)
	@ResponseBody
	public PrescreenDTO getAptc(@RequestBody PrescreenDTO aptcRequest){
		LOGGER.info("Calling Calculate APTC service for Product version.");
		return eligibilityPrescreenService.getAptc(aptcRequest);
	}
}
