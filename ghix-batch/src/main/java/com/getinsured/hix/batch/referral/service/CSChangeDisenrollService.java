/**
 * 
 */
package com.getinsured.hix.batch.referral.service;

/**
 * @author Biswakesh
 *
 */
public interface CSChangeDisenrollService {
	
	void processCSPlanChange();
	
	void processCSGeoLocaleChange();

}
