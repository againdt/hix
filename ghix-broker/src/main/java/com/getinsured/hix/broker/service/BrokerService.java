package com.getinsured.hix.broker.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.broker.BrokerContactDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.BrokerCallPrioritization;
import com.getinsured.hix.model.BrokerConnect;
import com.getinsured.hix.model.BrokerConnectHours;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * Encapsulates service layer method calls for Broker
 */
public interface BrokerService {

	/**
	 * Finds the broker by given id
	 * 
	 * @param brokerId
	 *            the given broker id
	 * @return the existing broker if found
	 */
	Broker findById(int brokerId);

	/**
	 * Generates partial record for the given broker from third party and return
	 * the generated sequence id
	 * 
	 * @param broker
	 *            object with partial broker information like license no,state
	 *            ein etc.
	 * @return the partially generated broker object
	 */
	Integer generateBrokerRecordId(Broker broker);

	/**
	 * Updates the Broker table with the given broker object
	 * 
	 * @param broker
	 *            object to update existing record
	 * @return the updated broker object
	 */
	Broker update(Broker broker);
	
	/**
	 * Finds the broker by given license no
	 * 
	 * @param licenseNo
	 *            the given license no
	 * @return the existing broker if found
	 */
	Broker findByLicenseNo(String licenseNo);
	
	/**
	 * Retrieves broker by broker id
	 * 
	 * @param id
	 *            broker id
	 * @return {@link Broker}
	 */
	Broker getBrokerDetail(int id);
	
	/**
	 * Retrieves broker by user id
	 * 
	 * @param id
	 *            user id
	 * @return {@link Broker}
	 */
	Broker getBrokerDetailByUserId(int userId);

	List<BrokerContactDTO> findBrokerListByStatus();
	
	void saveBroker(Broker broker);
	
	Broker findByAgencyManagerInfo(long agencyId);
	
	public List<Broker> findByAgencyManagers(long agencyId);

	EntityResponseDTO getBrokerAssisterDetailsByHouseholdId(Integer householdId);

	BrokerConnect getBrokerConnectDetails(Broker broker);

	BrokerConnect addBrokerConnectDetails(BrokerConnect brokerConnect);

	BrokerConnect updateBrokerConnectDetails(BrokerConnect brokerConnect);

	List<BrokerConnectHours> getBrokerConnectHours(BrokerConnect brokerConnect);

	List<BrokerConnectHours> addBrokerConnectHours(List<BrokerConnectHours> brokerConnectHours);

	List<BrokerConnectHours> updateBrokerConnectHours(List<BrokerConnectHours> brokerConnectHours);

	BrokerConnect updateBrokerAvailability(Integer brokerId, boolean availability);

	List<String> searchAvailableBrokers(BrokerCallPrioritization brokerCallPrioritization);
}