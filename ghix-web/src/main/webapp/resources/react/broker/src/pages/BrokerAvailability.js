import React, { Fragment } from 'react';

import UpdatePhoneNumber from '../components/UpdatePhoneNumber';
import AvailabilityHours from '../components/AvailabilityHours';

const BrokerAvailability = () => {
  return (
    <Fragment>
      <AvailabilityHours />
      <UpdatePhoneNumber />
    </Fragment>
  );
};

export default BrokerAvailability;
