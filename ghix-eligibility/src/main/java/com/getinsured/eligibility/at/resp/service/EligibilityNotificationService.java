package com.getinsured.eligibility.at.resp.service;

import com.getinsured.eligibility.notification.dto.SsapApplicationEligibilityDTO;


public interface EligibilityNotificationService {

	SsapApplicationEligibilityDTO getApplicationData(String caseNumber);

	String generateEligibilityNotificationInInbox(String caseNumber) throws Exception;

	String generateEligibilityNotificationInInbox(String caseNumber, String noticeTemplateName) throws Exception;

	String generateEligibilityNotificationUpdatedInInbox(String caseNumber)
			throws Exception;
}
