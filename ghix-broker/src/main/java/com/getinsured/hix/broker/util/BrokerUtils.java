package com.getinsured.hix.broker.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.dto.enrollment.EnrollmentBrokerUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.PaymentIsDefault;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixRestServiceInvoker;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.GhixEndPoints.FinanceServiceEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

/**
 * Broker Util class.
 * 
 * 
 */
@Component
public final class BrokerUtils {

	private BrokerUtils(){
		
	}
	
	private static final Logger LOGGER = Logger.getLogger(BrokerUtils.class);
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private GhixRestServiceInvoker ghixRestServiceInvoker;
	
	@Autowired private Gson platformGson;
	private static Pattern pattern;
	private static Matcher matcher;

	/**
	 * Validate email with regular expression
	 * 
	 * @param email
	 *            email for validation
	 * @return true valid email, false invalid email
	 */
	public static boolean validate(final String email) {
		pattern = Pattern.compile(BrokerConstants.EMAIL_PATTERN);
		matcher = pattern.matcher(email);

		return matcher.matches();
	}

	/**
	 * @see BrokerUtils#marshal(GHIXResponse)
	 * 
	 *      Marshal and form XML response.
	 * 
	 * @param response
	 *            the response object to marshal
	 * @return XML String
	 */
	public static String marshal(GHIXResponse response) {
		XStream xstream = GhixUtils.getXStreamStaxObject();
		return xstream.toXML(response);
	}

	/**
	 * @see BrokerUtils#convertUtilToSQLDate(Date)
	 * 
	 *      Converts java util date to java sql date
	 * 
	 * @param date
	 *            the util date to be converted into sql date
	 * @return java.sql.Date
	 */
	public static java.sql.Date convertUtilToSQLDate(java.util.Date date) {
		return new java.sql.Date(date.getTime());
	}

	/**
	 * @see BrokerUtils#formatDate(String)
	 * 
	 *      Formats the given date into format defined in
	 *      BrokerConstants.DATE_FORMAT
	 * 
	 * @param date
	 *            the date to be formatted
	 * @return java.util.Date formatted date
	 */
	public static Date formatDate(String date) throws GIException {
		Date formattedDate;
		SimpleDateFormat format = new SimpleDateFormat(
				BrokerConstants.DATE_FORMAT);
		try {
			formattedDate = format.parse(date);
		} catch (ParseException parseException) {
			throw new GIException(parseException);
		}
		return formattedDate;
	}

	/**
	 * @see BrokerUtils#isEmpty(String)
	 * 
	 *      Checks for null or blank String objects
	 * 
	 * @param value
	 *            the string to check
	 * @return true or false depending upon the condition
	 */
	public static boolean isEmpty(String value) {
		return (value != null && !"".equals(value)) ? false : true;
	}

	/**
	 * @see BrokerUtils#checkLengthOfNumber(Integer, int)
	 * 
	 *      Checks the length of a given number with given length
	 * 
	 * @param num
	 *            the number to check
	 * @param length
	 *            the length of the number to check
	 * @return true or false depending upon the condition
	 */
	public static boolean checkLengthOfNumber(Integer num, int length) {
		return String.valueOf(num).length() == length ? true : false;
	}

	/**
	 * @see BrokerUtils#checkLengthOfString(String, int)
	 * 
	 *      Checks the length of a given string with given length
	 * 
	 * @param str
	 *            the string to check
	 * @param length
	 *            the length of the number to check
	 * @return true or false depending upon the condition
	 */
	public static boolean checkLengthOfString(String str, int length) {
		return str.length() == length ? true : false;
	}

	/**
	 * @see BrokerUtils#isNumeric(String)
	 * 
	 *      Checks the string if it is a number
	 * 
	 * @param str
	 *            the string to check
	 * @return true or false depending upon the condition
	 */
	public static boolean isNumeric(String str) {
		try {
			Integer.parseInt(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
	
	public static boolean isZero(int number){
		if(number != 0){
			return false;
		} 
			return true;
	}
	
	/**
	 * @see BrokerUtils#appendZeros(String, int)
	 * 
	 *      Checks the length of a given string with given length and append leading zeros
	 * 
	 * @param str
	 *            the string to check
	 * @param length
	 *            the length of the number to check
	 * @return original string with leading zeros
	 */
	public static String appendZeros(String str, int length) {
		int numberLength;
		StringBuilder numberWithZeros = new StringBuilder(); 
		if(str.length()!=length){
			numberLength = length - str.length();
			for(int i=0;i<numberLength;i++){
				numberWithZeros.append(0);
			}
		}
		return numberWithZeros.append(str).toString();
	}
	
	public static boolean checkStateCode(String code){
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		return stateCode.equalsIgnoreCase(code);
	}
	
	public EnrollmentResponse updateEnrollmentDetails(EnrollmentBrokerUpdateDTO enrollmentBrokerUpdateDTO, Integer individualId, Integer recordId, String roleType, String actionFlag) {
		LOGGER.info("Update Enrollment Start : Individual ID : "+individualId+" : Role Type : "+roleType+" : Action : "+actionFlag);
		
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		String response = null;
		EnrollmentResponse enrollmentResponse = null;
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentRequest.class);
		
		enrollmentBrokerUpdateDTO.setHouseHoldCaseId(null);
	 	enrollmentBrokerUpdateDTO.setExchgIndividualIdentifier(String.valueOf(String.valueOf(individualId)));
		enrollmentBrokerUpdateDTO.setRoleType(roleType);
		enrollmentBrokerUpdateDTO.setAssisterBrokerId(recordId);
		enrollmentBrokerUpdateDTO.setAddremoveAction(actionFlag);
		enrollmentBrokerUpdateDTO.setMarketType(BrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL);
		enrollmentBrokerUpdateDTO.setBrokerTPAFlag(null);
		enrollmentRequest.setEnrollmentBrokerUpdateData(enrollmentBrokerUpdateDTO);
		
		try {
			LOGGER.info("Enrollment call Start. enrollmentBrokerUpdateDTO->  "+enrollmentBrokerUpdateDTO);
			LOGGER.info("GhixEndPoints.EnrollmentEndPoints.UPDATE_ENROLLMENT_BROKER_DETAILS:"+GhixEndPoints.EnrollmentEndPoints.UPDATE_ENROLLMENT_BROKER_DETAILS);
			response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.UPDATE_ENROLLMENT_BROKER_DETAILS,
					"exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON,
					String.class, writer.writeValueAsString(enrollmentRequest)).getBody();

			LOGGER.info("Update Enrollment API Response : " + SecurityUtil.sanitizeForLogging(response));
			
			if (null != response) {
				 enrollmentResponse = platformGson.fromJson(response, EnrollmentResponse.class);
				 if(enrollmentResponse != null	&& enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
					LOGGER.info("Failed to update enrollment:  Error Code : " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentResponse.getErrCode())) + " Error Message : " + SecurityUtil.sanitizeForLogging(enrollmentResponse.getErrMsg()));
					//throw new GIRuntimeException(null, null, Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
				}
			}
		} catch(GIRuntimeException giexception) {
			LOGGER.error("Exception occured while updating enrollment details : ", giexception);
			throw giexception;
		}
		catch (Exception e) {
			LOGGER.error("Exception occured while updating enrollment details : ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return enrollmentResponse;
	}
	
	public PaymentMethods getPaymentMethodDetailsForBroker(int moduleId, PaymentMethods.ModuleName moduleName) {
		LOGGER.info("getPaymentMethodDetailsForBroker : START");
		PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
 	 	paymentMethodRequestDTO.setModuleID(moduleId);
 	 	paymentMethodRequestDTO.setModuleName(moduleName);
 	 	paymentMethodRequestDTO.setPaymentMethodStatus(PaymentStatus.Active);
 	 	paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentIsDefault.Y);
 	 	LOGGER.info("Retrieving Payment Detail REST Call Starts");
 	 	String paymentMethodsStr = ghixRestServiceInvoker.invokeRestService(paymentMethodRequestDTO, FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
 	 	LOGGER.info("Retrieving Payment Detail REST Call Ends");
 	 	XStream xstream = GhixUtils.getXStreamStaxObject();
 	 	PaymentMethodResponse paymentMethodResponse = (PaymentMethodResponse) xstream
 	 	.fromXML(paymentMethodsStr);
 	 	PaymentMethods paymentMethods = null;
 	 	Map<String, Object> paymentMethodMap = paymentMethodResponse.getPaymentMethodMap();
 	 	if(paymentMethodMap != null && paymentMethodMap.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY) != null){
 	 	List<PaymentMethods> paymentMethodList = (List<PaymentMethods>)paymentMethodMap.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
 	 	paymentMethods = paymentMethodList != null ? paymentMethodList.get(0) : null;
 	 	}
 	 	
 	 	if(paymentMethods != null && paymentMethods.getPaymentType() == PaymentMethods.PaymentType.ACH){
 	 	String paymentMethodsString = ghixRestTemplate.getForObject(FinanceServiceEndPoints.FIND_PAYMENT_METHOD_PCI+paymentMethods.getId(), String.class);
 	 	PaymentMethodResponse  paymentMethodPCIResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsString);
 	 	return paymentMethodPCIResponse.getPaymentMethods();
 	 	}
		LOGGER.info("getPaymentMethodDetailsForBroker : END");
		return paymentMethods;
	}
}
