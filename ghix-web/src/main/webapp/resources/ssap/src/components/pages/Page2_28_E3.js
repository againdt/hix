import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Components

import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';

// ActionCreators

import { changeData } from '../../actions/actionCreators';

// Utils

import bitWiseOperatorTools from '../../utils/bitWiseOperatorTools';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Constants

import MASKS from '../../constants/masksConstants';
import INPUT_TYPES from '../../constants/inputTypes';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import VALIDATION_TYPES from '../../constants/validationTypes';
import {VALIDATION_ERROR_MESSAGES} from '../../constants/validationErrorMessages';




const Page2_28_E3 = props => {

    const {
        buttonContainer: ButtonContainer,
        currentHhm: {
            name,
            flagBucket: {
                taxHouseholdCompositionFlags
            }
        },
        data,
        form: {
            Form: {
                isFormSubmitted
            }
        },
        hhmIndex,
        householdMembers: {
            length : hhmsQty
        }
    } = props;

    const {
        taxHouseholdComposition: taxHouseholdCompositionMasks,
        taxHouseholdComposition: {
            isMarriedFlags,
            isSpouseOnApplicationFlags
        }
    } = MASKS;

    const indicatorsValues = bitWiseOperatorTools
        .convertFlagsToValues(taxHouseholdCompositionFlags, taxHouseholdCompositionMasks);

    const {
        isMarried,
        isSpouseOnApplication
    } = indicatorsValues;

    const onFieldChange = (...args) => {
        const [ , indexInArray, , [ name, value ] ] = args;

        const newState = { ...data };

        const updateFlags = payload => {
            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[indexInArray]
                .flagBucket
                .taxHouseholdCompositionFlags = bitWiseOperatorTools.getUpdatedFlags(
                    taxHouseholdCompositionFlags,
                    payload,
                );
        };

        switch(name) {
        case 'isMarried':
            updateFlags([
                [ isMarriedFlags, value ],
                !value ? [ isSpouseOnApplicationFlags, null ] : []
            ]); break;
        case 'isSpouseOnApplication':
            updateFlags([
                [ isSpouseOnApplicationFlags, value ]
            ]); break;

        }

        props.actions.changeData(newState);
    };

    const getButtons = isSpouseOnApplication => {

        switch(true) {
        case isSpouseOnApplication === true:
            return (
                <Fragment>
                    <p>
                        Select &nbsp;
                        <strong>
                            { normalizeNameOfPerson(name) }
                        </strong>
                        {'\'s spouse.'}
                    </p>
                    <ButtonContainer type="fixRelationshipButton" args={[]}/>
                </Fragment>
            );
        case isSpouseOnApplication === false || (isMarried && hhmsQty === 1):
            return (
                <Fragment>
                    <p className="required-true">
                        Enter &nbsp;
                        <strong>
                            { normalizeNameOfPerson(name) } &nbsp;
                        </strong>
                        {'spouse\'s information.'}
                    </p>
                    <ButtonContainer type="updateInformationButton" args={[]}/>
                </Fragment>
            );
        default:
            return null;
        }
    };

    const checkTopLevelValidation = () => isMarried !== true;

    return (
        <div id="fhMaritalStatus" className="fade-in">
            <Form topLevelValidity={checkTopLevelValidation()}>
                <ValidationErrorMessage
                    isVisible={isFormSubmitted && !checkTopLevelValidation() && isMarried === true}
                    messageText={VALIDATION_ERROR_MESSAGES.PROVIDE_SPOUSE}
                    errorClasses={['margin-l-20-ve']}
                />
                <UniversalInput
                    legend={{
                        legendText:
                            <span>
                                Is &nbsp;
                                <strong>
                                    {normalizeNameOfPerson(name)}&nbsp;
                                </strong>
                                married ?
                            </span>
                    }}
                    label={{
                        text: '',
                        labelClass: ['usa-width-one-whole', 'layout--02'],
                        ulClass:['usa-width-one-whole margin-b-20'],
                        showRequiredIcon: true
                    }}
                    inputData={{
                        isInputRequired: true,
                        type: INPUT_TYPES.RADIO_BUTTONS,
                        name: 'isMarried',
                        value: isMarried,
                        options: DROPDOWN_OPTIONS.YES_NO,
                        validationType: VALIDATION_TYPES.CHOSEN,
                        currentData: {},
                        fields: ['']
                    }}
                    inputActions={{
                        onFieldChange
                    }}
                    indexInArray={hhmIndex}
                    errorClasses={['margin-l-0 margin-l-20-ve']}
                />

                {
                    isMarried && hhmsQty > 1 &&

                    <UniversalInput
                        legend={{
                            legendText:
                                <span>
                                    Who is  &nbsp;
                                    <strong>
                                        {normalizeNameOfPerson(name)}{'\'s'}&nbsp;
                                    </strong>
                                    spouse ?
                                </span>
                        }}
                        label={{
                            text: '',
                            labelClass: ['usa-width-one-whole', 'layout--02'],
                            ulClass:['usa-width-one-whole margin-b-20'],
                            showRequiredIcon: true
                        }}
                        inputData={{
                            isInputRequired: true,
                            type: INPUT_TYPES.RADIO_BUTTONS,
                            name: 'isSpouseOnApplication',
                            value: isSpouseOnApplication,
                            options: DROPDOWN_OPTIONS.SPOUSE_ON_APPLICATION,
                            validationType: VALIDATION_TYPES.CHOSEN,
                            currentData: {},
                            fields: ['']
                        }}
                        inputActions={{
                            onFieldChange
                        }}
                        indexInArray={hhmIndex}
                        errorClasses={['margin-l-0 margin-l-20-ve']}
                    />
                }
            </Form>
            {
                getButtons(isSpouseOnApplication)
            }
        </div>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page2_28_E3.defaultProps = {
    name: 'Page2_28_E3'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_28_E3);
