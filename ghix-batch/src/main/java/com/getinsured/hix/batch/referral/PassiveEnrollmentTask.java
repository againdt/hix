package com.getinsured.hix.batch.referral;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.batch.repository.PassiveEnrollmentDataRepository;
import com.getinsured.hix.enrollment.PassiveEnrollment;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.AuthorizedRepresentative;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.CitizenshipImmigrationStatus;
import com.getinsured.iex.ssap.ContactPreferences;
import com.getinsured.iex.ssap.EligibleImmigrationDocumentType;
import com.getinsured.iex.ssap.HomeAddress;
import com.getinsured.iex.ssap.HouseholdContact;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.MailingAddress;
import com.getinsured.iex.ssap.Name;
import com.getinsured.iex.ssap.OtherAddress;
import com.getinsured.iex.ssap.Phone;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.SocialSecurityCard;
import com.getinsured.iex.ssap.SpecialCircumstances;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.iex.util.SsapUtil;

public class PassiveEnrollmentTask extends StepExecutionListenerSupport implements Tasklet {



    private static final String CONTACT_VIA_MAIL = "MAIL";

	private static final String CONTACT_VIA_EMAIL = "EMAIL";

	private static final String HOME_PHONE_TYPE = "HOME";

	private static final int SSN_LENGTH = 9;

	private static final String FEMALE = "F";

    private static final String MALE = "M";

    private static final String SSAP_FEMALE_GENDER = "Female";

    private static final String SSAP_MALE_GENDER = "Male";
    
    private static final String SPOUSE_RELATION = "01";

    private static final String SELF_RELATION = "18";
    
    private static final String CHILD_RELATION = "19";

    private static final String OTHER_RELATION = "G8";

    private static final String UNKNOWN = "unknown";

    private static final String SSAPAPPLICANT_GUID_KEY = "SSAP_APPLICANT_DISPLAY_ID";
    
    private static final String SSAPAPPLICATION_GUID_KEY = "SSAP_APPLICATION_DISPLAY_ID";
	
	private static final String PROCESSED = "P";
    
	private static final String VALIDATE_MODE = "VALIDATE";
	
	private static final String PROCESSING_MODE = "PROCESS";
	
	private final DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    
	private static final Logger LOGGER = Logger.getLogger(PassiveEnrollmentTask.class);

	@SuppressWarnings("serial")
    private static final List<String> VALID_RELATIONS = new ArrayList<String>() {
	    {
	        add(SPOUSE_RELATION);
	        add(SELF_RELATION);
	        add(CHILD_RELATION);
	        add(OTHER_RELATION);
	    }
	};
	
    @SuppressWarnings("serial")
    private static final Map<String, String> BLOOD_RELATIONSHIP_MAPPING = new HashMap<String, String>() {
        {
            //18 -> Self, 19 -> Child, 01 -> Spouse, 14 -> Sibling, G8 -> Other, 03 -> Parent
            put("18-19", "03"); //Self   - Child  => Parent
            put("18-01", "01"); //Self   - Spouse => Spouse
            put("18-G8", "G8"); //Self   - Other  => Other
            put("18-18", "18"); //Self   - Self   => Self
            put("01-18", "01"); //Spouse - Self   => Spouse
            put("01-19", "03"); //Spouse - Child  => Parent
            put("01-G8", "G8"); //Spouse - Other  => Other
            put("01-01", "01"); //Spouse - Spouse => Spouse
            put("19-19", "14"); //Child  - Child  => Sibling
            put("19-01", "19"); //Child  - Spouse => Child
            put("19-18", "19"); //Child  - Self   => Child
            put("19-G8", "G8"); //Child  - Other  => Other
        }
    };

	private static final int PRIMARY_PERSON_ID = 1;

	private static final String EXCHANGE_STATE_NAME = "Idaho";
    
	private static final String EXCHANGE_STATE_CODE = "ID";

	private static final String TOBACCO_USER = "1";
	
	private static final String NON_TOBACCO_USER = "2";

	private static final String VALIDATED = "V";

	private static final String ERROR = "E";
	
    @Autowired
    PassiveEnrollmentDataRepository passiveEnrollmentDataRepository;
    
    @Autowired
    private SsapUtil ssapUtil;
    
    private RestTemplate restTemplate;
    
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
    
    @Autowired
    @Qualifier("zipCodeService")
    private ZipCodeService zipCodeService;

    private String batchMode;
    
    private Long batchId;
    
	@Override
	public void beforeStep(StepExecution stepExecution) {
		JobParameters jobParameters = stepExecution.getJobParameters();
		if(StringUtils.isNotBlank(jobParameters.getString("mode")) && (VALIDATE_MODE.equals(jobParameters.getString("mode")) || PROCESSING_MODE.equals(jobParameters.getString("mode")))) {
			batchMode = jobParameters.getString("mode");
		}
		
		if(StringUtils.isNotBlank(jobParameters.getString("batchId"))) {
			try {
				batchId = Long.valueOf(jobParameters.getString("batchId"));	
			} catch(Exception e) {
				LOGGER.error("Error setting batchId for initiating job");
			}
		}
	}

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
    	String errorMessage = null;
    	try {
	    	if(batchMode != null && batchId != null) {
	            List<PassiveEnrollment> unprocessedRecords = passiveEnrollmentDataRepository.getAllRecordsForBatchIdAndRecordProcessingStatus(batchId, getRecordingProcessingStatus(batchMode));
	            if(validIssuerAssignedPolicyId(unprocessedRecords)) {
	            	if(null != unprocessedRecords && !unprocessedRecords.isEmpty()) {
	            		Map<String, List<PassiveEnrollment>> enrollmentsToProcess = groupEnrollments(unprocessedRecords);
	            		processEnrollments(enrollmentsToProcess);
	            	} else {
	            		errorMessage = "No passive enrollments to process";
	            	}
	            } else {
	            	errorMessage = "Blank Issuer Assigned policy id's found, terminating.";
	            }
	    	} else {
	    		errorMessage = "Mandatory input parameters 'mode' and 'batchId' not provided or invalid. (Valid values for mode = [VALIDATE, PROCESS], for batchId use valid batch id from PASSIVE_ENROLLMENT_DATA table)";
	    	}
        } catch (Exception e) {
        	errorMessage = "PassiveEnrollment Job received an exception";
        	LOGGER.error(errorMessage, e);
        } finally {
        	if(null != errorMessage) {
        		// Not logged because this will show up on the UI and is also logged in the database tables for spring batch
        		throw new GIRuntimeException(errorMessage);
        	}
        }
        return RepeatStatus.FINISHED;
    }

    private String getRecordingProcessingStatus(String batchMode) {
    	String status = null;
		if(VALIDATE_MODE.equals(batchMode)) {
			status = "N";
		} else if(PROCESSING_MODE.equals(batchMode)) {
			status = "V";
		}
		return status;
	}

	private boolean validIssuerAssignedPolicyId(List<PassiveEnrollment> enrollments) {
    	boolean allValidIssuerAssignedPolicyId = true;
    	for (PassiveEnrollment enrollee : enrollments) {
			if(StringUtils.isBlank(enrollee.getIssuerAssignedPolicyId())) {
				allValidIssuerAssignedPolicyId = false;
				enrollee.setStatusReason("Blank Issuer Assigned Policy Id");
				enrollee.setRecordProcessingStatus(ERROR);
			}
		}
    	passiveEnrollmentDataRepository.save(enrollments);
		return allValidIssuerAssignedPolicyId;
	}

	private void processEnrollments(Map<String, List<PassiveEnrollment>> applicationsToProcess) {
        Collection<List<PassiveEnrollment>> allEnrollments = applicationsToProcess.values();
        Date currentDate = new Date();
        
        // Iterate over all the enrollments
        for (List<PassiveEnrollment> enrollment : allEnrollments) {
            try {
                // Now process all the applicants in one application
                SingleStreamlinedApplication ssap = new SingleStreamlinedApplication();
                ssap.setApplicationGuid(ssapUtil.getNextSequenceFromDB(SSAPAPPLICATION_GUID_KEY));
                ssap.setApplyingForFinancialAssistanceIndicator(false);
				ssap.setApplicationStartDate(currentDate);
                ssap.setApplicationSignatureDate(currentDate);
                List<TaxHousehold> taxHouseholds = ssap.getTaxHousehold();
                TaxHousehold taxHousehold = new TaxHousehold();
                taxHouseholds.add(taxHousehold);
                List<HouseholdMember> householdMembers = taxHousehold.getHouseholdMember();
                HouseholdMember primaryHouseholdMember = null;
                int personIdCounter = 2;
                boolean enrollmentHasFailures = false;
                
                for (PassiveEnrollment enrollee : enrollment) {
                    HouseholdMember householdMember = new HouseholdMember();
                    householdMembers.add(householdMember);
                    HouseholdContact householdContact = householdMember.getHouseholdContact();
                    householdMember.setApplyingForCoverageIndicator(true);
                    householdMember.setUnder26Indicator(true);
                    
                    
                    if("Y".equals(enrollee.getSubscriberIndicator())) {
                        householdMember.setPersonId(PRIMARY_PERSON_ID);
                        householdMember.setHouseholdContactIndicator(true);
                        householdMember.setApplicantGuid(ssapUtil.getNextSequenceFromDB(SSAPAPPLICANT_GUID_KEY));
                        enrollee.setPersonId(PRIMARY_PERSON_ID);
                        ssap.setPrimaryTaxFilerPersonId(PRIMARY_PERSON_ID);
                        if(null == primaryHouseholdMember) {
                            primaryHouseholdMember = householdMember;
                        } else {
                            enrollee.getErrors().add("More than one primary subscribers found.");
                        }
                    } else {
                        householdMember.setPersonId(personIdCounter);
                        householdMember.setHouseholdContactIndicator(false);
                        householdMember.setApplicantGuid(ssapUtil.getNextSequenceFromDB(SSAPAPPLICANT_GUID_KEY));
                        enrollee.setPersonId(personIdCounter);
                        personIdCounter++;
                    }
                    
                    populateNameGenderSSN(enrollee, householdMember);
    
                    boolean validResidentialAddressSet = populateResidentialAddress(enrollee, householdMember);
                    
                    populateContactPreferences(enrollee, householdContact, validResidentialAddressSet);
                    
                    populatePhone(enrollee, householdContact);
                    
                    populateNativeAmerican(enrollee, householdMember);
                    
                    populateTobaccoUsage(enrollee, householdMember);
                    
                    populateDefaultsForMissingData(ssap);
                    
                    if(!enrollee.getErrors().isEmpty()) {
                        enrollmentHasFailures = true;
                    }
                }
                
                if(null != primaryHouseholdMember) {
                    if(populateBloodRelations(primaryHouseholdMember, enrollment)){
                        enrollmentHasFailures = true;
                    }
                	
                	populateLivesAtOtherAddress(primaryHouseholdMember, ssap);
                } else {
                	enrollmentHasFailures = true;
                	enrollment.get(0).getErrors().add("No primary subscriber found.");
                }
                
                if(!enrollmentHasFailures) {
                	if(PROCESSING_MODE.equals(batchMode)) {
                		processSsapApplication(ssap, enrollment);
                	} 
                	
                	if(VALIDATE_MODE.equals(batchMode)) {
                		updateProcessingStatus(enrollment);
                	} 
                	
                } else {
                	handleErrorsForEnrollment(enrollment);
                }
                
            } catch (Exception ex) {
            	String enrollmentRecordIssuerAssignedPolicyId = UNKNOWN;
            	try {
            		if(null != enrollment && !enrollment.isEmpty()) {
            			enrollmentRecordIssuerAssignedPolicyId = enrollment.get(0).getIssuerAssignedPolicyId();
            		}
            		LOGGER.error("Error processing enrollment record: " + enrollmentRecordIssuerAssignedPolicyId, ex);
            		handleErrorsForEnrollment(enrollment);
            	} catch(Exception saveException) {
            		LOGGER.error("Error saving exception for processing of enrollment record: " + enrollmentRecordIssuerAssignedPolicyId, saveException);
            	}
            }
        }
    }

    private void updateProcessingStatus(List<PassiveEnrollment> enrollment) {
        for (PassiveEnrollment enrollee : enrollment) {
            enrollee.setRecordProcessingStatus(VALIDATED);
        }
        passiveEnrollmentDataRepository.save(enrollment);
	}
    
    private void populateContactPreferences(PassiveEnrollment enrollee, HouseholdContact householdContact, boolean validResidentialAddressSet) {
    	// Try to set mailing address from the mailing address fields first
    	boolean validMailingAddressSet = populateMailingAddress(enrollee, householdContact);
    	ContactPreferences contactPreferences = householdContact.getContactPreferences();
    	
    	// Set email address if provided
    	boolean validEmailSet = populateEmail(enrollee, householdContact);
    	
    	// If valid email is set then set the contact preferences to email otherwise set it to mailing address if provided.
    	// In case mailing address is not provided, use the residential address as mailing address
    	if(validEmailSet) {
    		contactPreferences.setPreferredContactMethod(CONTACT_VIA_EMAIL);
    		if(!validMailingAddressSet && validResidentialAddressSet) {
    			populateMailingAddressFromHomeAddress(householdContact);
    		}
    	} else {
    		if(validMailingAddressSet) {
    			contactPreferences.setPreferredContactMethod(CONTACT_VIA_MAIL);
    		} else if(validResidentialAddressSet) {
    			populateMailingAddressFromHomeAddress(householdContact);
    			householdContact.setMailingAddressSameAsHomeAddressIndicator(true);
    			contactPreferences.setPreferredContactMethod(CONTACT_VIA_MAIL);
    		}
    	}
	}

	private void populateMailingAddressFromHomeAddress(HouseholdContact householdContact) {
		HomeAddress homeAddress = householdContact.getHomeAddress();
		MailingAddress mailingAddress = householdContact.getMailingAddress();
		mailingAddress.setStreetAddress1(homeAddress.getStreetAddress1());
		mailingAddress.setStreetAddress2(homeAddress.getStreetAddress2());
		mailingAddress.setCity(homeAddress.getCity());
		mailingAddress.setState(homeAddress.getState());
		mailingAddress.setPostalCode(homeAddress.getPostalCode());
		mailingAddress.setPrimaryAddressCountyFipsCode(homeAddress.getPrimaryAddressCountyFipsCode());
		mailingAddress.setCounty(homeAddress.getCounty());
		mailingAddress.setCountyCode(homeAddress.getCountyCode());
	}

	private boolean populateEmail(PassiveEnrollment enrollee, HouseholdContact householdContact) {
		boolean validEmailSet = false;
		if(StringUtils.isNotBlank(enrollee.getSubscriberEmailAddress())) {
			householdContact.getContactPreferences().setEmailAddress(enrollee.getSubscriberEmailAddress());
			validEmailSet = true;
		}
		return validEmailSet;
	}

	private void populateTobaccoUsage(PassiveEnrollment enrollee, HouseholdMember householdMember) {
    	if(StringUtils.isNotBlank(enrollee.getTobaccoStatus()) && TOBACCO_USER.equals(enrollee.getTobaccoStatus())) {
    		householdMember.setTobaccoUserIndicator(true);
    	} else if(StringUtils.isNotBlank(enrollee.getTobaccoStatus()) && NON_TOBACCO_USER.equals(enrollee.getTobaccoStatus())) {
    		householdMember.setTobaccoUserIndicator(false);
    	} else {
    		enrollee.getErrors().add("Invalid Tobacco usage indicator (valid values are 1 or 2)");
    	}
	}

	private void populateNativeAmerican(PassiveEnrollment enrollee, HouseholdMember householdMember) {
    	SpecialCircumstances specialCircumstances = householdMember.getSpecialCircumstances();
    	if(StringUtils.isNotBlank(enrollee.getQhpIdentifier()) && (enrollee.getQhpIdentifier().endsWith("02") || enrollee.getQhpIdentifier().endsWith("03"))) {
    		specialCircumstances.setAmericanIndianAlaskaNativeIndicator(true);
    	} else {
    		specialCircumstances.setAmericanIndianAlaskaNativeIndicator(false);
    	}
	}

	private void populatePhone(PassiveEnrollment enrollee, HouseholdContact householdContact) {
    	if(StringUtils.isNotBlank(enrollee.getTelephoneNumber())) {
    		Phone phone = householdContact.getPhone();
    		phone.setPhoneNumber(enrollee.getTelephoneNumber());
    		phone.setPhoneType(HOME_PHONE_TYPE);
    	}
	}

	private void processSsapApplication(SingleStreamlinedApplication ssap, List<PassiveEnrollment> enrollment) {
        String ssapId = null;

        try {
            ssapId = restTemplate.postForObject(GhixEndPoints.EligibilityEndPoints.PASSIVE_ENROLLMENT_PROCESSOR_URL, ssapJsonBuilder.transformToJson(ssap), String.class);
        } catch (Exception e) {
        	String errorMessage = "Error creating Ssap application for enrollment for issuer assigned policy id: " + enrollment.get(0).getIssuerAssignedPolicyId();
            handleErrorsForEnrollment(enrollment, errorMessage);
            LOGGER.error("Error in processing ssap", e);
        }
        
        if(null != ssapId) {
            for (PassiveEnrollment enrollee : enrollment) {
                enrollee.setRecordProcessingStatus(PROCESSED);
				enrollee.setSsapApplicationId(Long.valueOf(ssapId));
				LOGGER.info("Processing enrollee record: " + enrollee.getIssuerAssignedPolicyId() + " successful");
            }
            passiveEnrollmentDataRepository.save(enrollment);
        }
    }

    private void handleErrorsForEnrollment(List<PassiveEnrollment> enrollment, String errorMessage) {
        for (PassiveEnrollment enrollee : enrollment) {
            enrollee.setStatusReason(errorMessage);
            enrollee.setRecordProcessingStatus(ERROR);
        }
        passiveEnrollmentDataRepository.save(enrollment);
    }

    private void handleErrorsForEnrollment(List<PassiveEnrollment> enrollment) {
        for (PassiveEnrollment enrollee : enrollment) {
        	if(!enrollee.getErrors().isEmpty()) {
        		enrollee.setStatusReason(enrollee.getErrors().toString());
        	} else {
        		enrollee.setStatusReason("Failed due to error processing other records in enrollment");	
        	}
        	enrollee.setRecordProcessingStatus(ERROR);
        }
        passiveEnrollmentDataRepository.save(enrollment);
    }

    private boolean populateBloodRelations(HouseholdMember primaryHouseholMember, List<PassiveEnrollment> enrollment) {
        List<BloodRelationship> bloodRelationships = primaryHouseholMember.getBloodRelationship();
        boolean enrollmentHasFailures = false;
        for (PassiveEnrollment enrollee : enrollment) {
            
            if("1".equals(enrollee.getRelationToSubscriberInd())) {
                enrollee.setRelationToSubscriberInd(SPOUSE_RELATION);
            }
            
            if(!VALID_RELATIONS.contains(enrollee.getRelationToSubscriberInd())) {
                enrollee.getErrors().add("Invalid relation to subscriber: " + enrollee.getRelationToSubscriberInd());
                enrollmentHasFailures = true;
            }
            
            for (PassiveEnrollment secondaryEnrollee : enrollment) {
                if("1".equals(secondaryEnrollee.getRelationToSubscriberInd())) {
                	secondaryEnrollee.setRelationToSubscriberInd(SPOUSE_RELATION);
                }
                if("Y".equals(secondaryEnrollee.getSubscriberIndicator())) {
                    BloodRelationship relation = new BloodRelationship();
                    relation.setIndividualPersonId(enrollee.getPersonId().toString());
                    relation.setRelatedPersonId(secondaryEnrollee.getPersonId().toString());
                    relation.setRelation(enrollee.getRelationToSubscriberInd());
                    bloodRelationships.add(relation);
                } else if(enrollee.getPersonId() != secondaryEnrollee.getPersonId()) {
                    BloodRelationship relation = new BloodRelationship();
                    relation.setIndividualPersonId(enrollee.getPersonId().toString());
                    relation.setRelatedPersonId(secondaryEnrollee.getPersonId().toString());
                    relation.setRelation(BLOOD_RELATIONSHIP_MAPPING.get(enrollee.getRelationToSubscriberInd() + "-" + secondaryEnrollee.getRelationToSubscriberInd()));
                    bloodRelationships.add(relation);
                } else {
                    // This is the self - relation i.e. relation with self because code should enter
                    // this condition when enrollee's/secondaryEnrollee's IssuerAssignedMemberId are 
                    // same which means it is the self relation.
                    BloodRelationship relation = new BloodRelationship();
                    relation.setIndividualPersonId(enrollee.getPersonId().toString());
                    relation.setRelatedPersonId(secondaryEnrollee.getPersonId().toString());
                    relation.setRelation(SELF_RELATION);  
                    bloodRelationships.add(relation);
                }
            }
        }
        
        return enrollmentHasFailures;
    }

    private void populateNameGenderSSN(PassiveEnrollment enrollee, HouseholdMember householdMember) {
        
        Name name = householdMember.getName();
        
        if(StringUtils.isNotBlank(enrollee.getFirstName())) {
            name.setFirstName(enrollee.getFirstName());
        } else {
        	enrollee.getErrors().add("First name should not be blank");
        }
        
        if(StringUtils.isNotBlank(enrollee.getMiddleName())) {
            name.setMiddleName(enrollee.getMiddleName());
        }
        
        if(StringUtils.isNotBlank(enrollee.getLastName())) {
            name.setLastName(enrollee.getLastName());
        } else {
        	enrollee.getErrors().add("Last name should not be blank");
        }
        
        if(null != enrollee.getDob()) {
            try {
            	Date dateOfBirth = dateFormat.parse(enrollee.getDob());
            	if(new DateTime(enrollee.getDob()).isBeforeNow()) {
            		enrollee.getErrors().add("Date of birth should not be less than current date");
            	} else {
            		householdMember.setDateOfBirth(dateOfBirth);
            	}
			} catch (ParseException e) {
				String errorMessage = "Date of birth " + enrollee.getDob() + " not in format YYYYMMDD for enrollee: " + enrollee.getId();
				enrollee.getErrors().add(errorMessage);
				LOGGER.error(errorMessage);
			}
        } else {
        	enrollee.getErrors().add("Date of birth should not be null or blank");
        }
        
        if(StringUtils.isNotBlank(enrollee.getGender())) {
            householdMember.setGender(convertGender(enrollee.getGender()));
        } else {
        	enrollee.getErrors().add("Gender should not be blank");
        }
        
        if(StringUtils.isNotBlank(enrollee.getSsn()) && 
        		StringUtils.isNumeric(enrollee.getSsn()) && 
        		(enrollee.getSsn().trim().length() == SSN_LENGTH)) {
            SocialSecurityCard socialSecurityCard = householdMember.getSocialSecurityCard(); 
            socialSecurityCard.setSocialSecurityCardHolderIndicator(true);
            socialSecurityCard.setSocialSecurityNumber(enrollee.getSsn());
        } else if(StringUtils.isNotBlank(enrollee.getSsn())) {
            householdMember.getSocialSecurityCard().setSocialSecurityNumber(enrollee.getSsn());
            enrollee.getErrors().add("Invalid SSN");
        }
    }

    private boolean populateMailingAddress(PassiveEnrollment enrollee, HouseholdContact householdContact) {
    	boolean validMailingAddressSet = false;
    	
    	if(StringUtils.isNotBlank(enrollee.getMailingStreetAddress1()) && 
    			StringUtils.isNotBlank(enrollee.getMailingAddressCity()) &&
    			StringUtils.isNotBlank(enrollee.getMailingAddressState()) &&
    			StringUtils.isNotBlank(enrollee.getMailingAddressZipCode())) {
    		
    		if(validStateAndZipCode(enrollee.getMailingAddressState(), enrollee.getMailingAddressZipCode())) {
    			householdContact.getMailingAddress().setStreetAddress1(enrollee.getMailingStreetAddress1());
    			householdContact.getMailingAddress().setCity(enrollee.getMailingAddressCity());
    			householdContact.getMailingAddress().setState(enrollee.getMailingAddressState());
    			householdContact.getMailingAddress().setPostalCode(truncateZipCode(enrollee.getMailingAddressZipCode()));
    			
    			if(StringUtils.isNotBlank(enrollee.getMailingStreetAddress2())) {
    				householdContact.getMailingAddress().setStreetAddress2(enrollee.getResidentialStreetAddress2());
    			}
    			validMailingAddressSet = true;
    		} else {
    			enrollee.getErrors().add("Invalid state and zip code for mailing address");
    		}
    	}
    	return validMailingAddressSet;
    }

    private boolean validStateAndZipCode(String stateCode, String zipCode) {
		boolean validStateAndZipCode = false;
		try {
			validStateAndZipCode = zipCodeService.validateZipByState(stateCode, zipCode);
		} catch(Exception e) {
			LOGGER.error("Invalid state and zip code", e);
		}
		return validStateAndZipCode;
	}

	private boolean populateResidentialAddress(PassiveEnrollment enrollee, HouseholdMember householdMember) {
		boolean validResidentialAddressSet = false;
    	HouseholdContact householdContact = householdMember.getHouseholdContact();
    	
    	if(StringUtils.isBlank(enrollee.getResidentialStreetAddress1()) || 
    			StringUtils.isBlank(enrollee.getResidentialCity()) ||
    			StringUtils.isBlank(enrollee.getResidentialState()) ||
    			StringUtils.isBlank(enrollee.getResidentialCountyCode()) ||
    			StringUtils.isBlank(enrollee.getResidentialZipCode())) {
    		enrollee.getErrors().add("Residential address should have address1, city, state, zipcode and county fips code");
    	} else {
	        householdContact.getHomeAddress().setStreetAddress1(enrollee.getResidentialStreetAddress1());
	        householdContact.getHomeAddress().setCity(enrollee.getResidentialCity());
	        householdContact.getHomeAddress().setState(enrollee.getResidentialState());
	        householdContact.getHomeAddress().setPrimaryAddressCountyFipsCode(enrollee.getResidentialCountyCode());
	        householdContact.getHomeAddress().setPostalCode(truncateZipCode(enrollee.getResidentialZipCode()));
	        householdContact.setHomeAddressIndicator(true);

	        if(StringUtils.isNotBlank(enrollee.getResidentialStreetAddress2())) {
	        	householdContact.getHomeAddress().setStreetAddress2(enrollee.getResidentialStreetAddress2());
	        }
	        
	        validResidentialAddressSet = populateCountyName(enrollee.getErrors(), householdContact.getHomeAddress());
	        
	        if(PRIMARY_PERSON_ID == householdMember.getPersonId() && 
	        		EXCHANGE_STATE_CODE.equals(StringUtils.isNotBlank(enrollee.getResidentialState()))) {
	        	enrollee.getErrors().add("Primary applicant must be from " + EXCHANGE_STATE_NAME);
	        	validResidentialAddressSet = false;
	        }
    	}
    	return validResidentialAddressSet;
    }

    private String truncateZipCode(String zipCode) {
		if(StringUtils.isNotBlank(zipCode) && zipCode.length() > 5) {
			return zipCode.substring(0,5);
		}
		return zipCode;
	}

	private String convertGender(String gender) {
        String ssapGender = null;
        if(StringUtils.isNotBlank(gender)) {
            if(MALE.equals(gender)) {
                ssapGender = SSAP_MALE_GENDER;
            } else if(FEMALE.equals(gender)) {
                ssapGender = SSAP_FEMALE_GENDER;
            }
        }
        return ssapGender;
    }

    private Map<String, List<PassiveEnrollment>> groupEnrollments(List<PassiveEnrollment> unprocessedRecords) {
        Map<String, List<PassiveEnrollment>> groupedApplicants = new HashMap<>(); 
        for (PassiveEnrollment passiveEnrollment : unprocessedRecords) {
            List<PassiveEnrollment> applicantsForCurrentRecord = groupedApplicants.get(passiveEnrollment.getIssuerAssignedPolicyId());
            if(null == applicantsForCurrentRecord) {
                applicantsForCurrentRecord = new ArrayList<PassiveEnrollment>();
                groupedApplicants.put(passiveEnrollment.getIssuerAssignedPolicyId(), applicantsForCurrentRecord);
            }
            applicantsForCurrentRecord.add(passiveEnrollment);
        }
        return groupedApplicants;
    }
    
    private boolean populateCountyName(List<String> errors, Address address) {
    	boolean validCountyNameSet = false;
        if (address != null) {
            String countyCode = address.getPrimaryAddressCountyFipsCode(); // 5 digit code as the CSV has 5 digit county code
            String zip = address.getPostalCode();
            String state = address.getState();

            if (StringUtils.isNotBlank(countyCode) && StringUtils.isNotBlank(zip) && StringUtils.isNotBlank(state)) {
                String countyName = zipCodeService.findCountyNameByZipStateAndCountyCD(zip, state, countyCode.substring(2));
                if(StringUtils.isNotBlank(countyName)) {
                	address.setCounty(countyName);
                	validCountyNameSet = true;
                }
            }
            
            if(!validCountyNameSet) {
                errors.add("Invalid residential address: state(" + state +"), zip("+ zip +") and countycode(" + countyCode + ") provided are invalid");
            }
            
        } else {
            errors.add("Invalid or null residential address");
        }
        return validCountyNameSet;
    }
    
    private void populateLivesAtOtherAddress(HouseholdMember primaryHouseholdMember, SingleStreamlinedApplication singleStreamlinedApplication) {
        final List<HouseholdMember> householdMemberList = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
        final int size = ReferralUtil.listSize(householdMemberList);
        primaryHouseholdMember.setLivesWithHouseholdContactIndicator(true);
        if (size > 1) // No Need to check if there is one primary
        {
            boolean blnCheck = false;
            boolean blnLivesOther = false;
            Address otherAddres = null;
            for (int i = 0; i < size; i++) {
                if (householdMemberList.get(i).getPersonId().compareTo(1) != 0) {

                    blnCheck = false;
                    blnLivesOther = false;
                    otherAddres = null;

                    if (primaryHouseholdMember.getHouseholdContact().getHomeAddress() != null && householdMemberList.get(i).getHouseholdContact().getHomeAddress() != null) {
                        blnCheck = ReferralUtil.compareAddressData(primaryHouseholdMember.getHouseholdContact().getHomeAddress(), householdMemberList.get(i).getHouseholdContact().getHomeAddress());
                    }

                    if (blnCheck) {
                        blnLivesOther = false;
                    } else if (householdMemberList.get(i).getHouseholdContact().getHomeAddress() != null && householdMemberList.get(i).getHouseholdContact().getHomeAddress().getPostalCode() != null) {
                        blnLivesOther = true;
                        otherAddres = householdMemberList.get(i).getHouseholdContact().getHomeAddress();
                    } else {
                        if (primaryHouseholdMember.getHouseholdContact().getMailingAddress() != null && householdMemberList.get(i).getHouseholdContact().getMailingAddress() != null) {
                            blnCheck = ReferralUtil.compareAddressData(primaryHouseholdMember.getHouseholdContact().getMailingAddress(), householdMemberList.get(i).getHouseholdContact().getMailingAddress());
                        }

                        if (blnCheck) {
                            blnLivesOther = false;
                        } else if (householdMemberList.get(i).getHouseholdContact().getMailingAddress() != null && householdMemberList.get(i).getHouseholdContact().getMailingAddress().getPostalCode() != null) {
                            blnLivesOther = true;
                            otherAddres = householdMemberList.get(i).getHouseholdContact().getMailingAddress();
                        }
                    }

                    householdMemberList.get(i).setLivesWithHouseholdContactIndicator(!blnLivesOther);
                    
                    if (blnLivesOther) {
                        householdMemberList.get(i).setLivesAtOtherAddressIndicator(blnLivesOther);
                        if (householdMemberList.get(i).getOtherAddress() == null) {
                            householdMemberList.get(i).setOtherAddress(new OtherAddress());
                        }
                        otherAddres.setCounty(otherAddres.getPrimaryAddressCountyFipsCode());
                        otherAddres.setCountyCode(otherAddres.getPrimaryAddressCountyFipsCode());
                        otherAddres.setPrimaryAddressCountyFipsCode(null);
                        
                        householdMemberList.get(i).getOtherAddress().setAddress(otherAddres);
                    }
                }
            }
        }
    }
    
    private void populateDefaultsForMissingData(SingleStreamlinedApplication ssap) {
    	// Set the authorized representative with blank name for SSAp edit functionality
    	AuthorizedRepresentative representative = ssap.getAuthorizedRepresentative();
    	Name name = new Name();
    	name.setFirstName("");
    	name.setMiddleName("");
    	name.setLastName("");
    	name.setSuffix("");
    	representative.setName(name);
    	
    	// Set the immigration document types for all the applicants for SSAp edit functionality
    	for (HouseholdMember member : ssap.getTaxHousehold().get(0).getHouseholdMember()) {
    		CitizenshipImmigrationStatus citizenshipImmigrationStatus  = member.getCitizenshipImmigrationStatus();
    		citizenshipImmigrationStatus.setEligibleImmigrationDocumentSelected("");
    		List<EligibleImmigrationDocumentType> eligibleImmigrationDocumentTypes = citizenshipImmigrationStatus.getEligibleImmigrationDocumentType();
    		EligibleImmigrationDocumentType eligibleImmigrationDocumentType = new EligibleImmigrationDocumentType();
    		eligibleImmigrationDocumentType.setI20Indicator(Boolean.FALSE);
    		eligibleImmigrationDocumentType.setI327Indicator(Boolean.FALSE);
    		eligibleImmigrationDocumentType.setI551Indicator(Boolean.FALSE);
    		eligibleImmigrationDocumentType.setI571Indicator(Boolean.FALSE);
    		eligibleImmigrationDocumentType.setI766Indicator(Boolean.FALSE);
    		eligibleImmigrationDocumentType.setI797Indicator(Boolean.FALSE);
   			eligibleImmigrationDocumentType.setI94Indicator(Boolean.FALSE);
   			eligibleImmigrationDocumentType.setI94InPassportIndicator(Boolean.FALSE);
   			eligibleImmigrationDocumentType.setMachineReadableVisaIndicator(Boolean.FALSE);
   			eligibleImmigrationDocumentType.setOtherDocumentTypeIndicator(Boolean.FALSE);
   			eligibleImmigrationDocumentType.setTemporaryI551StampIndicator(Boolean.FALSE);
   			eligibleImmigrationDocumentType.setUnexpiredForeignPassportIndicator(Boolean.FALSE);
   			eligibleImmigrationDocumentTypes.add(eligibleImmigrationDocumentType);
		} 
	}

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public PassiveEnrollmentDataRepository getPassiveEnrollmentDataRepository() {
		return passiveEnrollmentDataRepository;
	}

	public void setPassiveEnrollmentDataRepository(PassiveEnrollmentDataRepository passiveEnrollmentDataRepository) {
		this.passiveEnrollmentDataRepository = passiveEnrollmentDataRepository;
	}

	public SsapUtil getSsapUtil() {
		return ssapUtil;
	}

	public void setSsapUtil(SsapUtil ssapUtil) {
		this.ssapUtil = ssapUtil;
	}

	public SsapJsonBuilder getSsapJsonBuilder() {
		return ssapJsonBuilder;
	}

	public void setSsapJsonBuilder(SsapJsonBuilder ssapJsonBuilder) {
		this.ssapJsonBuilder = ssapJsonBuilder;
	}

	public ZipCodeService getZipCodeService() {
		return zipCodeService;
	}

	public void setZipCodeService(ZipCodeService zipCodeService) {
		this.zipCodeService = zipCodeService;
	}
}
