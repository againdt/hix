package com.getinsured.hix.batch.provider;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.getinsured.hix.platform.util.GhixConstants;

public class ProviderFieldsMetadata extends HashMap<String, FieldMetadata> {

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	public static final int FACILITY=0;
	public static final int INDIVIDUAL=1;
	private ArrayList<FieldMetadata> derivedFields;
	private Logger logger = LoggerFactory.getLogger(ProviderFieldsMetadata.class);
	private HashMap<String, String> texonomyCodeMap;
	private HashMap<String, String> spcialityCodeMap;
	private HashMap<String, String> zipCodeMap;

	private ProviderFieldsMetadata() {

	}

	public static ProviderFieldsMetadata getMetadataMap()
			throws InvalidOperationException, ParserConfigurationException, SAXException, IOException {
		ProviderFieldsMetadata map = new ProviderFieldsMetadata();
		//keep the xml file in the conf directory
		String enclarityDataDir = GhixConstants.ENCLARITY_METADATA_DIR;
//		String texonomyFileName = GhixConstants.PROVIDER_TEXONOMY_FILE_NAME;
//		String zipCodeFileName = GhixConstants.PROVIDER_ZIPCODE_FILE;
		String fieldMetaDataFileName = GhixConstants.PROVIDER_FIELDS_METADATA;
		
		File enclarityDbDir = new File(enclarityDataDir);
		if(!enclarityDbDir.exists() || !enclarityDbDir.isDirectory()){
			throw new IOException("Data directory:"+enclarityDataDir+" doesn't exists or its not a directory");
		}
		//map.loadTexonomyAndSpecialityCodes(enclarityDbDir, texonomyFileName);
		//map.loadZipCodesCodes(enclarityDbDir,zipCodeFileName);//we need to verify this. zipcodes can be looked up from solr
		map.loadFieldMetadata(enclarityDbDir,fieldMetaDataFileName);
		return map;
	}
	
	//jdk 1.8. it's using jaxb 2.2. validate if this code is compliant to jaxb2.2. one method may not be supported from old version of jaxb 2.1 
	private void loadFieldMetadata(File enclarityDbDir, String fieldMetaDataFileName)
			throws ParserConfigurationException, SAXException, IOException,
			InvalidOperationException {
		InputStream metadaStream = new FileInputStream(new File(enclarityDbDir,
				fieldMetaDataFileName));
		DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();
		Document doc = docBuilder.parse(metadaStream);
		NodeList nodeList = doc.getElementsByTagName("field");
		int totalFields = nodeList.getLength();
		FieldMetadata fieldMetadata;
		Node field;
		for (int i = 0; i < totalFields; i++) {
			field = nodeList.item(i);
			fieldMetadata = extractFieldMetaData(field);
			logger.debug("Extracted Metadata:" + fieldMetadata.toString());
			if (fieldMetadata.getType().equalsIgnoreCase("derived")) {
				if (derivedFields == null) {
					derivedFields = new ArrayList<FieldMetadata>();
				}
				derivedFields.add(fieldMetadata);
			} else {
				this.put(fieldMetadata.getName(), fieldMetadata);
			}
		}
		logger.info("Done with loading metadata:" + this.size());
		metadaStream.close();
	}
	
	private FieldMetadata extractFieldMetaData(Node field)
			throws InvalidOperationException {
		FieldMetadata md = null;
		ProviderDataFieldProcessor processor;
		String tmp;
		String tmpVal;
		NamedNodeMap field_attr = field.getAttributes();
		int attrLen = field_attr.getLength();
		if (attrLen > 0) {
			md = new FieldMetadata();
		}
		for (int i = 0; i < field_attr.getLength(); ++i) {
			Node attr = field_attr.item(i);
			tmp = attr.getNodeName();
			tmpVal = attr.getNodeValue();
			if (tmp.equalsIgnoreCase("name")) {
				md.setName(tmpVal);
			} else if (tmp.equalsIgnoreCase("isMandatory")) {
				md.setMendatory(Boolean.valueOf(tmpVal));
			} else if (tmp.equalsIgnoreCase("type")) {
				md.setType(tmpVal);
			} else if (tmp.equalsIgnoreCase("hasMultipleValues")) {
				md.setHasMultipleValues(Boolean.valueOf(tmpVal));
			} else if (tmp.equalsIgnoreCase("scope")) {
				md.setScope(tmpVal);
			} else if (tmp.equalsIgnoreCase("id")) {
				md.setIdentifier(tmpVal);
			} else if (tmp.equalsIgnoreCase("solr_aware")) {
				md.setSolrAware(Boolean.valueOf(tmpVal));
			} else if (tmp.equalsIgnoreCase("validationRequired")) {
				md.setValidationRequired(Boolean.valueOf(tmp));
			} else {
				logger.warn("Unknown field attribute [" + tmp + "=\"" + tmpVal
						+ "\"] found, don't know how to handle this");
			}
		}
		// Now check for Processors
		NodeList childNodes = field.getChildNodes();
		Node childNode;
		int childs = childNodes.getLength();
		for (int i = 0; i < childs; i++) {
			childNode = childNodes.item(i);
			if (childNode.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}
			tmp = childNode.getNodeName();
			if (tmp.equalsIgnoreCase("processor")) {
				processor = extractProcessorFromNode(childNode);
				md.addProcessor(processor);
			}
			// getElementsByTagName("processor");
		}
		if (md == null) {
			logger.error("Failed to build field metadata");
		}
		return md;
	}
	
	private ProviderDataFieldProcessor extractProcessorFromNode(
			Node processorNode) throws InvalidOperationException {
		ProviderDataFieldProcessor processor = null;
		ValidationContext context = null;
		int index = -1;
		String processorName = null;
		String tmp = null;
		String tmpVal = null;
		Exception ex = null;
		NamedNodeMap processor_attr = processorNode.getAttributes();
		try {
			for (int i = 0; i < processor_attr.getLength(); ++i) {
				Node attr = processor_attr.item(i);
				tmp = attr.getNodeName();
				tmpVal = attr.getNodeValue();
				if (tmp.equalsIgnoreCase("index")) {
					index = Integer.valueOf(tmpVal);
				} else if (tmp.equalsIgnoreCase("name")) {
					processorName = tmp;
					logger.debug("Loading processor:" + tmpVal);
					processor = (ProviderDataFieldProcessor) (Class
							.forName(tmpVal).newInstance());
				} else {
					logger.warn("Unknown processor attribute [" + tmp + "=\""
							+ tmpVal
							+ "\"] found, don't know how to handle this");
				}
			}
			if (index == -1) {
				throw new InvalidOperationException(
						"No index found for processor [" + processorName + "] ");
			}
			processor.setIndex(index);
		} catch (ClassNotFoundException ce) {
			ClassLoader cl = ClassLoader.getSystemClassLoader();
			URL[] urls = ((URLClassLoader) cl).getURLs();
			for (URL url : urls) {
				logger.info(url.getFile());
			}
			ex = ce;
		} catch (IllegalAccessException ie) {
			ex = ie;
		} catch (InstantiationException ine) {
			ex = ine;
		} finally {
			if (ex != null) {
				logger.error(ex.getMessage(), ex);
				throw new InvalidOperationException(ex.getMessage(), ex);
			}
		}
		// Reaching here means we not have the processor. now extract the
		// context and constraints
		NodeList contextNodeChilds = processorNode.getChildNodes();
		int len = contextNodeChilds.getLength();
		Node contextNode = null;
		Node tmpNode = null;
		for (int c = 0; c < len; c++) {
			tmpNode = contextNodeChilds.item(c);
			if (tmpNode.getNodeType() != Node.ELEMENT_NODE) {
				tmpNode = null;
				continue;
			}
			if (tmpNode.getNodeName().equalsIgnoreCase("context")) {
				contextNode = tmpNode;
			}

		}
		if (contextNode != null) {
			context = extractValidationContext(contextNode);
		} else {
			logger.warn("No validation context found, providing default");
			context = new ValidationContext();
		}
		context.addContextInfo("texonomies", texonomyCodeMap);
		context.addContextInfo("specialities", spcialityCodeMap);
		context.addContextInfo("zipcodeMap", zipCodeMap);
		processor.setValidationContext(context);
		return processor;
	}
	
	/*
	 * <processor index="1" name=
	 * "com.getinsured.hix.batch.provider.processors.StringLengthProcessor.class"
	 * > <context> <constraint max="60" onfail="warn"></constraint> </context>
	 * </processor>
	 */
	private ValidationContext extractValidationContext(Node contextNode)
			throws InvalidOperationException {
		NodeList constraintNodes = null;
		Node tmp = null;
		Node tmpNode = null;
		Node tmpAttr = null;
		String tmpStr = null;
		FieldConstraints fc = null;
		int constraintIndex = -1;
		ValidationContext vc = new ValidationContext();
		NamedNodeMap contextAttrMap = contextNode.getAttributes();
		int conextAttrs = contextAttrMap.getLength();
		for (int m = 0; m < conextAttrs; m++) {
			tmp = contextAttrMap.item(m);
			vc.addContextInfo(tmp.getNodeName(), tmp.getNodeValue());
		}
		tmp = null;
		constraintNodes = contextNode.getChildNodes();
		int len = constraintNodes.getLength();
		if (len == 0) {
			logger.debug("No constraints available from the metada file");
		}
		for (int i = 0; i < len; i++) {
			tmp = constraintNodes.item(i);
			if (tmp.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}
			if (tmp.getNodeName().equalsIgnoreCase("constraint")) {
				fc = new FieldConstraints();
				NamedNodeMap x = tmp.getAttributes();
				tmpNode = x.getNamedItem("index");
				if (tmpNode == null) {
					throw new InvalidOperationException(
							"Index parameter missing from constraint");
				}
				tmpStr = tmpNode.getNodeValue();
				constraintIndex = Integer.valueOf(tmpStr);
				fc.setIndex(constraintIndex);
				for (int j = 0; j < x.getLength(); j++) {
					tmpAttr = x.item(j);
					fc.addConstraintAttribute(tmpAttr.getNodeName(),
							tmpAttr.getNodeValue());
				}
				vc.addFieldConstraint(fc);
			}
		}
		return vc;
	}
	
	
	private void loadTexonomyAndSpecialityCodes(File enclarityDbDir, String texonomyFileName)
			throws InvalidOperationException, IOException {
		// getClass().getClassLoader();
		InputStream x = new FileInputStream(new File(enclarityDbDir,
				texonomyFileName));
		texonomyCodeMap = new HashMap<String, String>();
		spcialityCodeMap = new HashMap<String, String>();
		if (x != null) {
			BufferedReader br = new BufferedReader(new InputStreamReader(x));
			String str = null;
			String tmp = null;
			String[] record;
			while ((str = br.readLine()) != null) {
				if (str.startsWith("#")) {
					continue;
				}
				record = str.split(",");
				tmp = texonomyCodeMap.get(record[2]);
				if (tmp != null && !tmp.equalsIgnoreCase(record[0])) {
					tmp += "," + record[0];
					logger.debug("Multiple specialities found for texonomy code:"
							+ record[2] + " Description :" + tmp);
				} else {
					tmp = record[0];
				}
				texonomyCodeMap.put(record[2].trim(), tmp);
				spcialityCodeMap.put(record[1].trim(), record[0].trim());
				logger.debug("Populated Texonomy and Speciality Records: Description["
						+ record[0]
						+ "] Speciality ["
						+ record[1]
						+ "] Texonomy[" + record[2] + "]");
			}
			br.close();
		}
	}

	//we need ca zipcodes and counties. if they don't send us, escalate it to Dawn. solr has all the zipcodes
	//solr  has 2 yr old data. it's better if get the file from CA
	private void loadZipCodesCodes(File enclarityDbDir, String zipCodeFileName)
			throws InvalidOperationException, IOException {
		InputStream x = new FileInputStream(new File(enclarityDbDir,
				zipCodeFileName));
		zipCodeMap = new HashMap<String, String>();
		if (x != null) {
			BufferedReader br = new BufferedReader(new InputStreamReader(x));
			String latLon = null;
			String zip = null;
			String[] record;
			String str = null;
			while ((str = br.readLine()) != null) {
				if (str.startsWith("#")) {
					continue;
				}
				record = str.split(",", 2);
				zip = record[0];
				latLon = record[1];
				if (latLon.endsWith(",")) {
					latLon = latLon.substring(0, latLon.length() - 2);
				}
				zipCodeMap.put(zip.trim(), latLon);
				logger.debug("Populated Zip Records: Zip code[" + record[0]
						+ "] LatLon [" + record[1] + "]");
			}
			br.close();
		}
	}
	
	public String getTexonomyDescription(String taxonomyCode){
		if(this.texonomyCodeMap != null){
			return this.texonomyCodeMap.get(taxonomyCode);
		}
		return null;
	}
	
	public String getSpecialityDescription(String specialityCode){
		if(this.spcialityCodeMap != null){
			return this.spcialityCodeMap.get(specialityCode);
		}
		return null;
	}
	
	public String getZipLatLonCoordinates(String zipCode){
		if(this.zipCodeMap != null){
			return this.zipCodeMap.get(zipCode);
		}
		return null;
	}

	public ArrayList<FieldMetadata> getDerivedFields() {
		if(this.derivedFields == null){
			this.derivedFields = new ArrayList<FieldMetadata>();
		}
		return this.derivedFields;
	}
}
/*
for phase1 - once the user enters the zipcode, just go to db to get lati and longi. don't need to go to solr to get that data as it's old.
 */ 
