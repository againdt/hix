package com.getinsured.hix.indportal.dto.dm.application;

public class DocumentedAnnualIncome {
	 private Float incomeAmount;
	 private Integer householdSize;
	 private String effectiveStartDate;
	 private String effectiveEndDate;


	 // Getter Methods 

	 public Float getIncomeAmount() {
	  return incomeAmount;
	 }

	 public Integer getHouseholdSize() {
	  return householdSize;
	 }

	 public String getEffectiveStartDate() {
	  return effectiveStartDate;
	 }

	 public String getEffectiveEndDate() {
	  return effectiveEndDate;
	 }

	 // Setter Methods 

	 public void setIncomeAmount(Float incomeAmount) {
	  this.incomeAmount = incomeAmount;
	 }

	 public void setHouseholdSize(Integer householdSize) {
	  this.householdSize = householdSize;
	 }

	 public void setEffectiveStartDate(String effectiveStartDate) {
	  this.effectiveStartDate = effectiveStartDate;
	 }

	 public void setEffectiveEndDate(String effectiveEndDate) {
	  this.effectiveEndDate = effectiveEndDate;
	 }
	}