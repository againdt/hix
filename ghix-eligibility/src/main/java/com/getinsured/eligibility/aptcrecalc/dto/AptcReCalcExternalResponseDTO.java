package com.getinsured.eligibility.aptcrecalc.dto;

public class AptcReCalcExternalResponseDTO {

	 Response response;


	 // Getter Methods 

	 public Response getResponse() {
	  return response;
	 }

	 // Setter Methods 

	 public void setResponse(Response responseObject) {
	  this.response = responseObject;
	 }
}
