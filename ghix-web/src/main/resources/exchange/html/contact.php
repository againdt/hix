<?php
require_once ('thrive-app/util/Vimo_Session.php');
include_once ("thrive-app/util/session_proc.php");
include_once ("thrive-ws/htdocs/thrive/backbuttonchk.php");
include_once ("thrive-app/biz/TW_Contact.php");
include_once ("thrive-ws/htdocs/thrive/backbuttonchk.php");

$module = $contactEmailSentStatus = $blankStr = "";	

if (isset ( $_POST ["formSubmitted"] )) {	 	
	
	$formErr = null;
	$cobj_TW_Contact = new TW_Contact ();			
	// validate the form
	if ($cobj_TW_Contact->contactValidate ( & $formErr )) {		
		// CAPTCHA validation bellow
		/*
		$captchaValid = false;
		$secCode = isset ( $_POST ['secCode'] ) ? trim ( $_POST ['secCode'] ) : "";
		$captChaInSess = Vimo_Session::getData ( 'CAPTCHA_SECURITY_CODE' );
		if ($secCode === $captChaInSess) {
			$captchaValid = true;
			Vimo_Session::setData ( 'CAPTCHA_SECURITY_CODE', $blankStr );
		} else {
			$formErr = "<p>Sorry the security code is invalid! Please try it again!</p>";
		}
		*/
		// if CAPTHCHA validation is valid then only proceed bellow
		
		$captchaValid = true;
		$contactToEmail = "contact@getinsured.com";
		
		if ($captchaValid) {
			if ($contactToEmail != "") {
				$cobj_TW_Contact->setRecipientEmailAddress ( $contactToEmail );
			}
			// add feedback details, and send the email
			if ($cobj_TW_Contact->addContactDetails ()) {
				$contactEmailSentStatus = true;				
			}
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Getinsured Health Exchange</title>
<link href='https://fonts.googleapis.com/css?family=Rokkitt:400,700' rel='stylesheet' type='text/css'>
<link href="../assets/css/bootstrap.css" rel="stylesheet">
<link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="../assets/css/styles.css" rel="stylesheet">
<link rel="stylesheet" href="../assets/css/font-awesome.css">

<link rel="shortcut icon" href="../assets/images/favicon.ico" type="image/x-icon" />
</head>
<style>
.top {
	margin-top: 80px;
}
ul {
	margin: 0 0 10px 0;
	list-style: none;
}
.gutter {
	padding-left: 20px;
}
</style>

<body>

<!--<div class="sideCallOut">
        <a href="#" alt="Request a Demo" class="btn">Request a Demo</a>
    </div>--> 
<div class="navbar-wrapper"> 
  <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
  <div class="navContainer">
    <div class="navbar-inverse">
      <div class="navbar-inner"> <a class="brand" href="home.html" alt="Getinsured Logo"><img src="../assets/images/getinsured_logo.png" alt="Getinsured Logo"></a> 
        <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
        <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        
        <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
        <div class="nav-collapse collapse">
          <ul class="nav pull-right nav-pills">
            <li class=" dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Features <i class="icon-caret-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="features.html"><strong>Overview <i class=" icon-angle-right"></i></strong></a></li>
                <li class="divider">&nbsp;</li>
                <li><a href="features/plan-selection.html">Plan Selection</a></li>
                    <li><a href="features/additional-plans.html">Ancillary Plans</a></li>
                    <li><a href="features/shop.html">Employer Exchange (SHOP)</a></li>
                    <li><a href="features/broker.html">Broker/Navigator Management</a></li>
                    <li><a href="features/carrier.html">Plan Management</a></li>
                	<li><a href="features/eligibility.html">Eligibility</a></li>                   
                    <li><a href="features/enrollment.html">Enrollment</a></li>
                    <li><a href="features/financial.html">Financial Management</a></li>
                    <li><a href="features/call-center.html">Call Center Operations</a></li>
                    <li><a href="features/marketing.html">Marketing Outreach</a></li>
                <!--<li><a href="features/external-interfaces.html">External Interfaces</a></li>-->
              </ul>
            </li>
            <li class=" dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Our Solutions<i class="icon-caret-down"></i></a>
            	<ul class="dropdown-menu">
                  <li><a href="exchange.html"><strong>Overview <i class=" icon-angle-right"></i></strong></a></li>
                  <li class="divider">&nbsp;</li>
                  <li><a class="" href="solutions/government.html">Government</a></li>
                  <li><a href="solutions/carriers.html">Carriers</a></li>
                  <li><a href="solutions/partners.html">Partners</a></li>
            	</ul>
            </li>
            <li><a href="why.html">Why Getinsured</a></li>
            <li><a href="about.html">About</a></li>
            <li class="active"><a href="contact.php">Contact</a></li>
          </ul>
        </div>
        <!--/.nav-collapse --> 
      </div>
      <!-- /.navbar-inner --> 
    </div>
    <!-- /.navbar --> 
  </div>
</div>
<div class="wrapper">
  <div class="container-fluid gutter25">
    <div class="top">
      <div class="fullBg wrap">
        <div class="row-fluid "> 
          <h1>Contact Us</h1>
          </div>
        <div class="row-fluid">
        	 <?php if ($contactEmailSentStatus != "") { ?>
        	<div class="alert alert-info span9">
            	Thank you for contacting Getinsured.com. We'll be in touch shortly about your request.
  			</div>
  			<?php } else { ?>
  			<form class="form-horizontal" name="contactForm" id="contactForm" method="post" action="contact.php">            
              <div class="span9 box container">
              <input type="hidden" name="formSubmitted" value="true" />
                <h2>Get more information</h2>
                <div class="control-group">
                  <label class="control-label">Name</label>
                  <div class="controls">
                    <input type="text" class="input-large" placeholder="" name="contact_name" />
                  </div>
                </div>
                <!-- /.control-group -->
                
                <div class="control-group">
                  <label class="control-label">Company</label>
                  <div class="controls">
                    <input type="text" class="input-large" placeholder=""  name="contact_company" />
                  </div>
                </div>
                <!-- /.control-group -->
                
                <div class="control-group">
                  <label class="control-label">Email Address</label>
                  <div class="controls">
                    <input type="text" class="input-large" placeholder="" name="contact_email" />
                  </div>
                </div>
                <!-- /.control-group -->
                
                <div class="control-group">
                  <label class="control-label">Phone Number</label>
                  <div class="controls">
                    <input type="text" class="input-medium" placeholder="" name="contact_phone_number" />
                  </div>
                </div>
                <!-- /.control-group -->
                
                <div class="control-group">
                  <label class="control-label">City</label>
                  <div class="controls">
                    <input type="text" class="input-medium" placeholder="" name="contact_city" />
                  </div>
                </div>
                <!-- /.control-group -->
                
                <div class="control-group">
                  <label class="control-label">State</label>
                  <div class="controls">
                    <input type="text" class="input-mini" placeholder="" name="contact_state" />
                  </div>
                </div>
                <!-- /.control-group -->
                
                <div class="control-group">
                  <label class="control-label">How can we help?</label>
                  <div class="controls">
                    <select name="contact_how_help">
                      <option>&nbsp;</option> 
                      <option>State-Based Exchange</option>
                      <option>Private Exchange</option>
                      <option>Partnership Inquiries</option>
                      <option>Media/Press Inquiries</option>
                      <option>Other...</option>
                    </select>
                  </div>
                </div>
                <!-- /.control-group -->
                
                <div class="control-group">
                  <label class="control-label">Message</label>
                  <div class="controls">
                    <textarea name="contact_message"></textarea>
                  </div>
                </div>
                <!-- /.control-group -->
                
                <div class="control-group">
                  <div class="controls"> <a class="btn submit">Submit</a> </div>
                </div>
                <!-- /.control-group --> 
                
              </div>
              <!-- /.span6 -->
              </form>
            <!-- ./form --> 
			<?php } ?>
              <div class="span3">
                <div class="gutter">
                	<div class="bottom25">
                  <h3>Getinsured</h3>
                  <p> 2595 East Bayshore Road<br />
                    Suite 250 <br />
                    Palo Alto, CA<br />
                  </p>
                  <ul>
                    <!--<li><i class="icon-phone"></i> 800-987-2232<br />
                    </li>-->
                    <li><a href="mailto: contact@getinsured.com"><i class="icon-envelope-alt"></i> contact@getinsured.com</a></li>
                    <li><a href="https://twitter.com/getinsuredcom" target="_blank"><i class="icon-twitter-sign"></i> Twitter</a></li>
                    <li><a href="https://www.facebook.com/GetInsured" target="_blank"><i class="icon-facebook-sign"></i> Facebook</a></li>
                  </ul>
                  </div>
                  
                  <div class="bottom25">
                  <h3>Press Inquiries</h3>
                  <p>
                  	<strong>Jason Alyesh</strong><br />
                    Director of Media Relations<br />
                    650-618-4611<br />
                    <a href="mailto: media@getinsured.com"><i class="icon-envelope-alt"></i> media@getinsured.com</a>
                  </p>
                  </div>
                  
                </div>
              </div>
              <!-- /.span6 -->
              
            
        </div>
        <!-- /.row-fluid--> 
        
      </div>
      <!-- /.container-fluid --> 
      
    </div>
  </div>
  <!-- /.main -->
  <div class="push"></div>
</div>
<!--/.wrapper-->
<div class="footer">
  <div class="footerContainer container-fluid row-fluid">
    <div class="span3">
      <ul>
        <li>&copy; 2013 Getinsured</li>
        <li><a href="privacy.html">Privacy</a></li>
        <li><a href="https://www.getinsured.com/legal.php" target="_blank">Legal</a></li>
      </ul>
    </div>
    <!-- ./span3 -->
    
    <div class="span3">
      <ul>
        <li><a href="about.html">About</a></li>
        <li><a href="team.html">Meet the Team</a></li>
        <li><a href="contact.php">Contact</a></li>
        <li><a href="contact.php">Request a Demo</a></li>
      </ul>
    </div>
    <!-- ./span3 -->
    
    <div class="span3">
      <ul>
        <li><a href="features.html">Features</a></li>
        <li><a href="exchange.html">Our Solutions</a></li>
        <li><a href="why.html">Why Getinsured</a></li>
        <li><a href="http://www.getinsured.com" target="_blank">Getinsured.com</a></li>
      </ul>
    </div>
    <!-- ./span3 -->
    
    <div class="span3">
      <ul class="">
        
        <li class="pull-left"><a href="contact.php"><i class="icon-envelope-alt icon-large icon-2x"></i></a></li>
        <li class="pull-left"><a href="https://twitter.com/getinsuredcom" target="_blank"><i class="icon-twitter-sign icon-large icon-2x"></i></a></li>
        <li class="pull-left"><a href="https://www.facebook.com/GetInsured" target="_blank"><i class="icon-facebook-sign icon-large icon-2x"></i></a></li>
      </ul>
    </div>
    <!-- ./span3 --> </div>
  <!-- ./footerContainer --> 
</div>
<!-- /.footer --> 

<script src="../assets/js/jquery.js"></script> 
<script src="../assets/js/bootstrap-transition.js"></script> 
<script src="../assets/js/bootstrap-alert.js"></script> 
<script src="../assets/js/bootstrap-modal.js"></script> 
<script src="../assets/js/bootstrap-dropdown.js"></script> 
<script src="../assets/js/bootstrap-scrollspy.js"></script> 
<script src="../assets/js/bootstrap-tab.js"></script> 
<script src="../assets/js/bootstrap-tooltip.js"></script> 
<script src="../assets/js/bootstrap-popover.js"></script> 
<script src="../assets/js/bootstrap-button.js"></script> 
<script src="../assets/js/bootstrap-collapse.js"></script> 
<script src="../assets/js/bootstrap-carousel.js"></script> 
<script src="../assets/js/bootstrap-typeahead.js"></script> 
<script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
	  
	  
    </script> 
<script src="../assets/js/holder/holder.js"></script>
  <script>
    // very simple to use!
    $(document).ready(function() {
	  
	  // hide alert	  
	//  $('.alert').hide();
	  
	  //submit function for the alert
	  $('.submit').click(function(){		  	
		  //$('.alert').show();
		  //$('.form-horizontal').hide();
		  document.contactForm.submit();		  
		  });
    });
	
  </script>
  
</body>
</html>
