package com.getinsured.ssap.model.hub.vlp.documents;

import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * Certificate of Citizenship document.
 *
 * @author Yevgen Golubenko
 * @since 2/15/19
 */
public class CertificateOfCitizenshipDocument extends DhsDocument
{
  public CertificateOfCitizenshipDocument()
  {
    super.setDocumentType(DocumentType.CERTIFICATE_OF_CITIZENSHIP);
  }

  @JsonProperty("AlienNumber")
  private String alienNumber;

  @JsonProperty("CitizenshipNumber")
  private String citizenshipNumber;

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isValid()
  {
    return notBlank(this.alienNumber) && notBlank(this.citizenshipNumber);
  }

  /**
   * {@inheritDoc }
   */
  @Override
  public DhsDocument from(final CitizenshipDocument citizenshipDocument)
  {
    final CertificateOfCitizenshipDocument document = new CertificateOfCitizenshipDocument();

    document.setAlienNumber(validValue(citizenshipDocument.getAlienNumber()));
    document.setCitizenshipNumber(validValue(citizenshipDocument.getForeignPassportOrDocumentNumber()));

    return document;
  }

  public String getAlienNumber()
  {
    return alienNumber;
  }

  public void setAlienNumber(final String alienNumber)
  {
    this.alienNumber = alienNumber;
  }

  public String getCitizenshipNumber()
  {
    return citizenshipNumber;
  }

  public void setCitizenshipNumber(final String citizenshipNumber)
  {
    this.citizenshipNumber = citizenshipNumber;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", CertificateOfCitizenshipDocument.class.getSimpleName() + "[", "]")
        .add("alienNumber='" + alienNumber + "'")
        .add("citizenshipNumber='" + citizenshipNumber + "'")
        .toString();
  }
}
