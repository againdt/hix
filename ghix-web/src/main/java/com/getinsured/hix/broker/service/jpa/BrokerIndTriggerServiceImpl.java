package com.getinsured.hix.broker.service.jpa;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.broker.repository.IBrokerRepository;
import com.getinsured.hix.broker.service.BrokerIndTriggerService;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.dto.broker.BrokerBOBSearchParamsDTO;
import com.getinsured.hix.dto.broker.BrokerBobResponseDTO;
import com.getinsured.hix.dto.broker.BrokerDTO;
import com.getinsured.hix.dto.entity.DesignateEntityDTO;
import com.getinsured.hix.dto.entity.EntityRequestDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Broker.certification_status;
import com.getinsured.hix.model.DelegationRequest;
import com.getinsured.hix.model.DelegationResponse;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.platform.feature.GiFeature;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.jpa.UserServiceImpl;
import com.getinsured.hix.platform.util.GhixEndPoints.AHBXEndPoints;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.thoughtworks.xstream.XStream;

@Service("brokerIndTriggerService")
public class BrokerIndTriggerServiceImpl implements BrokerIndTriggerService {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private IBrokerRepository brokerRepository;
	
//	private static final String aeeIndEnabled = AEEConfiguration.AEEConfigurationEnum.AEE_IND_IS_ENABLED != null ? String.valueOf(AEEConfiguration.AEEConfigurationEnum.AEE_IND_IS_ENABLED) : "N";
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@GiFeature("ahbx.aee.ind35.enabled")
	@Override
	public void triggerInd35(Broker broker, PaymentMethods paymentMethods) {
		// TODO Auto-generated method stub
				LOGGER.info("Start of Ind35 call");
				if(broker == null){
					throw new GIRuntimeException("Exception occured while processing IND 35");
				}
				List<EntityRequestDTO> entityRequestDTOList = new ArrayList<EntityRequestDTO>();
				EntityRequestDTO entityRequestDTO = populateBrokerDto(broker, paymentMethods);
				entityRequestDTOList.add(entityRequestDTO);

				try {
					LOGGER.info("IND35: Sending Broker Details to Ghix-AHBX");

					String strEntityRequestDTOList = GhixUtils.xStreamHibernateXmlMashaling().toXML(entityRequestDTOList);

					String wsResponse = restTemplate.postForObject(GhixPlatformEndPoints.AHBXEndPoints.SEND_ENTITY_DETAIL,
							strEntityRequestDTOList, String.class);

					LOGGER.info("IND35: Successfully sent Broker Details over Rest call to Ghix-Ahbx.");

				} catch (Exception exception) {
					LOGGER.error("IND35: Send Broker Details Web Service failed: Exception is :", exception);
				}
				LOGGER.info("end of Ind35 call");
	}
	
	/**
	 * Populates {@link BrokerDTO} from {@link Broker} to send it to AHBX
	 * through REST API
	 *
	 * @param broker
	 * @return {@link BrokerDTO}
	 */
	private EntityRequestDTO populateBrokerDto(Broker broker, PaymentMethods paymentMethods) {
		EntityRequestDTO entityRequestDTO = new EntityRequestDTO();

		entityRequestDTO.setRecordId(broker.getId());
		entityRequestDTO.setRecordType("Agent");
		entityRequestDTO.setRecordIndicator("Update");

		entityRequestDTO.setFirstName(broker.getUser() !=null?broker.getUser().getFirstName():broker.getFirstName());
		entityRequestDTO.setLastName(broker.getUser() !=null?broker.getUser().getLastName():broker.getLastName());

		entityRequestDTO.setBusinessLegalName(broker.getCompanyName());

		// Applying as quick fix to HIX-10371
		entityRequestDTO.setFunctionalIdentifier(new Long(broker.getBrokerNumber()));
		entityRequestDTO.setFederalEIN(broker.getFederalEIN());

		if (broker.getMailingLocation() != null) {
			entityRequestDTO.setAddressLine1(broker.getLocation().getAddress1());
			entityRequestDTO.setAddressLine2(broker.getLocation().getAddress2());
			entityRequestDTO.setCity(broker.getLocation().getCity());
			entityRequestDTO.setState(broker.getLocation().getState());
			entityRequestDTO.setZipCode(String.valueOf(broker.getLocation().getZip()));
		} else {
			entityRequestDTO.setAddressLine1("");
			entityRequestDTO.setAddressLine2("");
			entityRequestDTO.setCity("");
			entityRequestDTO.setState("");
			entityRequestDTO.setZipCode("");
		}

		String contactNumber = "";
		if (broker.getContactNumber() != null && broker.getContactNumber().contains("-")) {
			contactNumber = broker.getContactNumber().replaceAll("-", "");
		} else {
			contactNumber = broker.getContactNumber();
		}
		entityRequestDTO.setPhoneNumber(contactNumber);
		entityRequestDTO.setEmail(broker.getPersonalEmailAddress());

		entityRequestDTO.setAgentLicenseNum(broker.getLicenseNumber()); // !=
																		// null
																		// ?
																		// broker.getLicenseNumber().substring(0,
																		// 6) :
																		// "");
		populateCertificationInformation(broker, entityRequestDTO);

		populatePaymentMethod(paymentMethods, entityRequestDTO);
		
		if(broker.getChangeType()!=null){
			entityRequestDTO.setChangeType(broker.getChangeType());
		}

		return entityRequestDTO;
	}
	
	/**
	 * @param broker
	 * @param entityRequestDTO
	 */
	private void populateCertificationInformation(Broker broker,
			EntityRequestDTO entityRequestDTO) {
		entityRequestDTO.setCertiStatusCode(broker.getCertificationStatus());
		if (broker.getCertificationDate() != null) {
			entityRequestDTO.setCertiStartDate(formatDate(broker.getCertificationDate()));
		}
		if (broker.getStatusChangeDate() != null) {
			entityRequestDTO.setStatusDate(formatDate(broker.getStatusChangeDate()));
		}

		// If broker is certified then, setting certification end date as
		// certification date + 1 year, else set current date as certification
		// end date
		if (broker.getCertificationStatus() != null
				&& broker.getCertificationStatus().toString()
						.equalsIgnoreCase(certification_status.Certified.toString())) {
			Calendar date = TSCalendar.getInstance();
			date.setTime(broker.getCertificationDate());
			date.add(Calendar.YEAR, 1);
			Date newdate = date.getTime();
			entityRequestDTO.setCertiEndDate(formatDate(newdate));
			entityRequestDTO.setCertificationNumber(broker.getCertificationNumber());
			entityRequestDTO.setCertiRenewalDate(formatDate(broker.getReCertificationDate()));
		} else {
			entityRequestDTO.setCertiEndDate(formatDate(new TSDate()));
			entityRequestDTO.setCertiRenewalDate(formatDate(new TSDate()));
		}
	}
	
	/**
	 * Method to populate current EntityRequestDTO with payment method information.
	 *
	 * @param paymentMethods The current PaymentMethods instance.
	 * @param entityRequestDTO The current EntityRequestDTO instance.
	 */
	private void populatePaymentMethod(PaymentMethods paymentMethods,
			EntityRequestDTO entityRequestDTO) {
		// If payment type is EFT/ACH, setting flag to true. Else, false
		if (paymentMethods != null && "ACH".equals(paymentMethods.getPaymentType().name())) {
			LOGGER.debug("Payment Method is ACH. Setting Direct Deposit Flag to True");
			entityRequestDTO.setDirectDepositFlag("True");
		} else {
			LOGGER.debug("Payment Method is not ACH. Setting Direct Deposit Flag to False");
			entityRequestDTO.setDirectDepositFlag("False");
		}

		if (paymentMethods != null && paymentMethods.getFinancialInfo() != null
				&& paymentMethods.getFinancialInfo().getBankInfo() != null) {

			BankInfo bankInfo = paymentMethods.getFinancialInfo().getBankInfo();

			entityRequestDTO.setBankRoutingNumber(bankInfo.getRoutingNumber());
			entityRequestDTO.setAcctHolderName(bankInfo.getNameOnAccount());
			entityRequestDTO.setBankAcctNumber(bankInfo.getAccountNumber());

			// Checking if account type is 'C' or 'S' to set hard-coded values
			// in DTO
			String accountType = null;
			if (bankInfo.getAccountType() != null) {
				if ("C".equalsIgnoreCase(bankInfo.getAccountType())) {
					accountType = "Checking";
				} else if ("S".equalsIgnoreCase(bankInfo.getAccountType())) {
					accountType = "Savings";
				}
			}
			entityRequestDTO.setBankAcctType(accountType);
		}
	}
	
	@GiFeature("ahbx.aee.ind47.enabled")
	@Override
	public EntityResponseDTO triggerInd47(DesignateBroker designateBroker) {
		DesignateEntityDTO designateEntityDTO = populateEntityBrokerDto(designateBroker);
		EntityResponseDTO entityResponseDTO = new EntityResponseDTO();

		/*
		 * Currently logging response received from AHBX. TODO Need to log
		 * response in db when requirement comes
		 */

		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For Broker : ").append(designateBroker.getId()).append(" : ");

		try {
			LOGGER.warn("IND47: Sending Broker Designation details to Ghix-AHBX . Designate Broker ID : "+designateBroker.getId());

			String wsResponse = restTemplate.postForObject(AHBXEndPoints.SEND_ENTITY_DESIGNATION_DETAIL,
					designateEntityDTO, String.class);
			LOGGER.warn("IND47: wsResponse : "+wsResponse);
			if(!isEmpty(wsResponse)){
				LOGGER.warn("IND47: wsResponse is not empty");
				entityResponseDTO = (EntityResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(wsResponse);
				LOGGER.warn("IND47: entityResponseDTO :"+entityResponseDTO);
			}

			LOGGER.warn("IND47: Successfully sent Broker Designation details over Rest call to Ghix-Ahbx.");

		} catch (Exception exception) {
			LOGGER.error("IND47: Send Broker Designation Web Service failed. Exception is :" , exception);
		}
		
		return entityResponseDTO;
	}
	
	/**
	 * Populates {@link DesignateEntityDTO} from {@link DesignateEntityDTO} to
	 * send it to AHBX through REST API
	 * 
	 * @param designateBroker
	 * @return {@link DesignateEntityDTO}
	 */
	private DesignateEntityDTO populateEntityBrokerDto(DesignateBroker designateBroker) {
		DesignateEntityDTO designEntityDto = new DesignateEntityDTO();

		designEntityDto.setBrokerAssisterId(designateBroker.getBrokerId());

		// Set them to default values
		designEntityDto.setEmployerId(0);
		designEntityDto.setId(0);
		designEntityDto.setStatus(designateBroker.getStatus().toString());

		// Adding record type as per latest AHBX WSDL update to differentiate if
		// request
		// is associated with agent or assister
		designEntityDto.setRecordType(BrokerConstants.AGENT);

		if (designateBroker.getExternalEmployerId() != null && !"".equals(designateBroker.getExternalEmployerId())) {
			designEntityDto.setEmployerId(designateBroker.getExternalEmployerId());
			designEntityDto.setRole(RoleService.EMPLOYER_ROLE);
		} else if (designateBroker.getExternalIndividualId() != null
				&& !"".equals(designateBroker.getExternalIndividualId())) {
			designEntityDto.setIndividualId(designateBroker.getExternalIndividualId());
			designEntityDto.setRole(RoleService.INDIVIDUAL_ROLE);
		}

		return designEntityDto;
	}

	
	private String formatDate(Date dateInput) {
		String date = null;
		if(dateInput!=null){
			date = new SimpleDateFormat("MMddyyyy").format(dateInput);
		}
		return date;
	}

	
	/**
	 * Checks for null or blank String objects
	 * 
	 * @param value
	 *            the string to be compared
	 * @return boolean true/false depending on whether the value is empty or not
	 */
	public static boolean isEmpty(String value) {

		return (value != null && !"".equals(value)) ? false : true;
	}
	
	@GiFeature("ahbx.aee.ind72.enabled")
	@Override
	public BrokerBobResponseDTO triggerInd72(BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO) {
		 
		BrokerBobResponseDTO brokerBobResponseDTO = null;

		try {
			LOGGER.info("IND72: Sending request to Ghix-AHBX for fetching pending/active.in-active individuals. : "+brokerBOBSearchParamsDTO.getRecordId());
			

			String wsResponse = restTemplate.postForObject(AHBXEndPoints.GET_EXTERNAL_INDIVIDUAL_BOB_LIST, brokerBOBSearchParamsDTO, String.class);
			
			if(!isEmpty(wsResponse)){
				brokerBobResponseDTO	 = (BrokerBobResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(wsResponse);
			}
			
			LOGGER.info("IND72: Successfully fetched individual information.");

		} catch (Exception exception) {
			LOGGER.error("IND72: Fetch individual details Service failed. Exception is :" , exception);
		}
		
		return brokerBobResponseDTO;
	}
	
	@GiFeature("ahbx.aee.ind73.enabled")
	@Override
	public BrokerBobResponseDTO triggerInd73(BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO) {
		 
		BrokerBobResponseDTO brokerBobResponseDTO = null;

		try {
			LOGGER.info("IND73: Sending request to Ghix-AHBX for fetching Household/Eligibility Details : "+brokerBOBSearchParamsDTO.getRecordId());
			

			String wsResponse = restTemplate.postForObject(AHBXEndPoints.GET_EXTERNAL_INDIVIDUAL_HOUSEHOLD_ELIGIBILITY_DETAIL, brokerBOBSearchParamsDTO, String.class);
			
			if(!isEmpty(wsResponse)){
				brokerBobResponseDTO	 = (BrokerBobResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(wsResponse);
			}
			
			LOGGER.info("IND73 : Successfully fetched individual information.");

		} catch (Exception exception) {
			LOGGER.error("IND73 : Fetch household/eligibility details Service failed. Exception is :" , exception);
		}
		
		return brokerBobResponseDTO;
	}
	
	@GiFeature("ahbx.aee.ind54.enabled")
	@Override
	public void triggerInd54(Broker broker) {
		String wsResponse = null;
		
		try {
			DelegationRequest delegationRequest = populateDelegationRequestDTO(broker);

			LOGGER.info("IND54: Sending Broker Information to AHBX : Broker ID : "+broker.getId());

			wsResponse = restTemplate.postForObject(AHBXEndPoints.DELEGATIONCODE_RESPONSE, delegationRequest,
					String.class);
			
			DelegationResponse delegationResponse = (DelegationResponse) GhixUtils
					.xStreamHibernateXmlMashaling().fromXML(wsResponse);

			String delegationCode = null;
			if (delegationResponse != null
					&& delegationResponse.getResponseCode() == 200) {
				delegationCode = delegationResponse.getDelegationCode();
				Broker existingBroker = brokerRepository.findById(broker.getId());
				existingBroker.setDelegationCode(delegationCode);
				brokerRepository.save(existingBroker);

				LOGGER.debug("Successfully persisted the delegation code into DB");
			}

			LOGGER.info("IND54: Successfully sent Assister Information over Rest call to Ghix-Ahbx : "+wsResponse);

		} catch (Exception exception) {
			LOGGER.error("IND54: Send Assister Information Web Service failed. Exception is :" , exception);
		}
	}
	
	private DelegationRequest populateDelegationRequestDTO(Broker broker) {

		DelegationRequest delegationRequest = new DelegationRequest();

		delegationRequest.setRecordId(broker.getId());
		
		delegationRequest.setContactFirstName(broker.getUser() !=null?broker.getUser().getFirstName():broker.getFirstName());
		delegationRequest.setContactLastName(broker.getUser() !=null?broker.getUser().getLastName():broker.getLastName());

		delegationRequest.setBusinessLegalName(broker.getCompanyName());
		delegationRequest.setAddressLineOne(broker.getLocation() != null ? broker.getLocation().getAddress1() : null);
		delegationRequest.setAddressLineTwo(broker.getLocation() != null ? broker.getLocation().getAddress2() : null);
		delegationRequest.setCity(broker.getLocation() != null ? broker.getLocation().getCity()	: null);
		delegationRequest.setState(broker.getLocation() != null ? broker.getLocation().getState() : null);
		delegationRequest.setZipCode(broker.getLocation() != null ? String.valueOf(broker.getLocation().getZip()) : null);
		delegationRequest.setFunctionalId(broker.getBrokerNumber());
		delegationRequest.setAssisterFirstName(broker.getFirstName());
		delegationRequest.setAssisterLastName(broker.getLastName());
		delegationRequest.setCertificationStausCode(broker.getCertificationStatus());
		delegationRequest.setCertificationDate(broker.getStatusChangeDate() != null ? formatDate(
				BrokerConstants.DATE_FORMAT_PLAIN, broker.getStatusChangeDate()) : null);
		
		if(StringUtils.isNotEmpty(broker.getContactNumber())) {
			delegationRequest.setPhoneNumber(Long.parseLong(getOnlyDigits(broker.getContactNumber())));
		}
		
		delegationRequest.setAgentLicenseNumber(broker.getLicenseNumber());
		delegationRequest.setEmailId(broker.getPersonalEmailAddress());
		delegationRequest.setCertificationId(broker.getCertificationNumber());
		delegationRequest.setRecordType(BrokerConstants.AGENT_ROLE);
		
		return delegationRequest;
	}
	
	public static String formatDate(String dateFormat, Date dateInput) {

		return dateInput != null ? new SimpleDateFormat(dateFormat).format(dateInput) : null;
	}
	
	public static String getOnlyDigits(String s) {
	    Pattern pattern = Pattern.compile("[^0-9]");
	    Matcher matcher = pattern.matcher(s);
	    String number = matcher.replaceAll("");
	    return number;
	 }
}
