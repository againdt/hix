<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- eligiblity info controller -->

<script type="text/javascript">
agentEntityApp.controller('eligibilityInfoCtrl', function($scope, $http) {
	$scope.isEligibilityDataLoaded = false;
    $scope.$on('eligibilityInfoEvent', function(e, data) {      	
    	$http({
            url: '/hix/broker/HouseholdEligibilityInformation',
            method: "POST",
            data: data,
            headers: {'Content-Type': 'application/json',"csrftoken": $('#tokid').val()}
        }).success(function (data, status, headers, config) {
              if (data != 'null'){
            	  $scope.eligiblityInfoData = data;
            	  $scope.isEligibilityDataLoaded = true;
                  angular.element('#eligibilityInfolModal').modal('show');
              }
        }).error(function (data, status, headers, config) {
            alert("error")
        });
    });
});
</script>

<!-- eligibility info popup -->
<div id="eligibilityInfolModal" class="modal" style="display:none" ng-controller="eligibilityInfoCtrl">
	<div class="modal-header">
		  <p class="pull-left"><spring:message code="label.bobHouseholdEligibilityInfo"/></p>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	</div>
	 <div class="modal-body">
	 <div class="row-fluid" ng-hide="eligiblityInfoData">
			  <div class="span12 text-center">
			  		<img src="/hix/resources/images/loader.gif" alt="loading" width="40px" />
			  </div>
		</div>
	 	<div ng-show="eligiblityInfoData">
		 	<h3 class="modalMessage"><spring:message code="label.bobHouseholdEligibilityfor"/> {{eligiblityInfoData.firstName}} {{eligiblityInfoData.lastName}}</h3>
		 		<div class="row-fluid">
			  <div class="span4 text-right"> 		<spring:message code="label.bobEligibilityStatus"/>:	  </div>
			  <div class="span8"> 		{{eligiblityInfoData.eligibilityStatus}}  </div>
			</div>
			<div class="row-fluid">
				  <div class="span4 text-right"> 		<spring:message code="label.bobAdvPremiumTaxCredit"/>:	  </div>
				  <div class="span8" ng-if="eligiblityInfoData.advancedPremiumTaxCredit"> 		{{eligiblityInfoData.advancedPremiumTaxCredit | currency}} <spring:message code="label.permonth"/>
								{{(eligiblityInfoData.advancedPremiumTaxCredit*12) | currency}} <spring:message code="label.peryear"/></div>
			</div>
			<c:if test="${ID_STATE_CODE || NV_STATE_CODE}">
			<div class="row-fluid">	
				  <div class="span4 text-right"> 		<spring:message code="label.bobCostSharingReduction"/>:	  </div>
				  <div class="span8"> 		{{eligiblityInfoData.costSharingReduction}}  </div>
			</div>
			</c:if>
			<h3 class="modalMessage"><spring:message code="label.bobApplicantEligibility"/></h3>
		  		<table class="householdDataTable table table-striped margin10-t"  >
					<thead>
						<tr>
							<th><spring:message code="label.bobHouseholdName"/></th>
							<th><spring:message code="label.bobEligibilityStatus"/></th>
							<c:if test="${CA_STATE_CODE}">
								<th><spring:message code="label.bobMedicalEligibility"/></th>
							</c:if>
							<th><spring:message code="label.bobAdvancePremiumTax"/></th>
							<th><spring:message code="label.bobCostSharingReduction"/></th>
		
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="data in eligiblityInfoData.applicantEligibility ">
							<td>{{data.firstName}} {{data.lastName}}</td>
							<td>{{data.eligibilityStatus}}</td>
							<c:if test="${CA_STATE_CODE}">
								<td>{{data.medicalEligibilityStatus}}</td>
							</c:if>
							<td>{{data.advancedPremiumTaxCredit}}</td>
							<td>{{data.costSharingReduction}}</td>
		
						</tr>
					</tbody>			
				</table>	
		</div>			
	</div>
</div>