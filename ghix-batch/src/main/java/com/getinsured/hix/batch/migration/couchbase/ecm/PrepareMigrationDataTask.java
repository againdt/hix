package com.getinsured.hix.batch.migration.couchbase.ecm;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.getinsured.hix.batch.migration.couchbase.ecm.DataConfig.MigrationStepEnum;

public class PrepareMigrationDataTask extends BaseEcmCouchMigrationClass {
	private static final String ENCOUNTERED_EXCEPTION_IN_THE_PROCESS_OF_PREPARING_THE_DATABASE_FOR_MIGRATION = "Encountered Exception in the process of preparing the database for Migration";

	private static final String PROCESS_OF_PREPARING_THE_DATABASE_FOR_MIGRATION_COMPLETED_SUCCESSFULLY = "Process of preparing the database for Migration completed successfully";

	private static final String INITIATE_THE_PROCESS_OF_PREPARING_THE_DATABASE_FOR_MIGRATION = "Initiate the process of preparing the database for Migration";

	private static final Logger LOGGER = LoggerFactory.getLogger(PrepareMigrationDataTask.class);

	private JdbcTemplate jdbcTemplate;

	public PrepareMigrationDataTask() {
		super(MigrationStepEnum.IDENTIFY);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	void startExecution() throws Exception {
		Connection con = null;
		try {
			LOGGER.info(INITIATE_THE_PROCESS_OF_PREPARING_THE_DATABASE_FOR_MIGRATION);
			con = jdbcTemplate.getDataSource().getConnection();
			con.setAutoCommit(true);
			for (CouchbaseECMMigrationDTO tableDTO : DataConfig.migrationTables.values()) {
				LOGGER.info(
						"Clearing from Table - " + tableDTO.getTableName() + ", Column - " + tableDTO.getColumnName());
				clearData(tableDTO, con);
			}

			for (CouchbaseECMMigrationDTO tableDTO : DataConfig.migrationTables.values()) {
				LOGGER.info(
						"Inserting in Table - " + tableDTO.getTableName() + ", Column - " + tableDTO.getColumnName());
				prepareData(tableDTO, con);
			}

			LOGGER.info("Identify Duplicate ecm ids");
			handleDuplicateEcm(con);

			LOGGER.info(PROCESS_OF_PREPARING_THE_DATABASE_FOR_MIGRATION_COMPLETED_SUCCESSFULLY);
		} catch (Exception e) {
			LOGGER.error(ENCOUNTERED_EXCEPTION_IN_THE_PROCESS_OF_PREPARING_THE_DATABASE_FOR_MIGRATION, e);
			throw e;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e2) {

				}
			}
		}

	}
	
	private void handleDuplicateEcm(Connection con) throws SQLException {

		String updateStatus = "Update " + DataConfig.COUCH_MIGRATION + " SET " + DataConfig.MIGRATION_STATUS +" ='" 
		+ DataConfig.MigrationStatusEnum.DUPLICATE.name() + "'"
		+" WHERE " + DataConfig.MIGRATION_STATUS + " = '" + DataConfig.MigrationStatusEnum.NOTSTARTED.name() + "'";
		
		int count = JDBCUtil.executeUpdate(con, updateStatus);
		
		LOGGER.info("HANDLE DUPLICATE STEP 1-" + count + " RECORDS OF COUCH_MIGRATION TABLE ARE UPDATED AS DUPLICATE");
		
		updateStatus = "Update " + DataConfig.COUCH_MIGRATION + " SET " + DataConfig.MIGRATION_STATUS +" = '" 
		+ DataConfig.MigrationStatusEnum.NOTSTARTED.name() + "'"
		+ " WHERE " + DataConfig.MIGRATION_ID + " IN (select min("+ DataConfig.MIGRATION_ID + ") FROM " 
		+ DataConfig.COUCH_MIGRATION
		+" WHERE " + DataConfig.MIGRATION_STATUS + " = '" + DataConfig.MigrationStatusEnum.DUPLICATE.name() + "'"
		+ " GROUP BY " + DataConfig.EXISTING_ECM_ID + ")" ;
		
		count = JDBCUtil.executeUpdate(con, updateStatus);
		LOGGER.info("HANDLE DUPLICATE STEP 2-" + count + " RECORDS OF COUCH_MIGRATION TABLE ARE UPDATED AS NOTSTARTED");
	}


//	private void handleDuplicateEcm(Connection con) throws SQLException {
//
//		List<Map<String, String>> data = fetchDuplicateRecords(con);
//		int iSize = CouchMigrationUtil.listSize(data);
//		LOGGER.info(iSize + " - Ecm records having duplicate records");
//		if (iSize == 0) {
//
//			return;
//		}
//		StringBuilder st = new StringBuilder(50);
//
//		for (Map<String, String> map : data) {
//
//			st.append("update ").append(DataConfig.COUCH_MIGRATION).append(" set MIGRATION_STATUS = '")
//					.append(DataConfig.MigrationStatusEnum.DUPLICATE.name()).append("' where MIGRATION_ID != ")
//					.append(map.get(DataConfig.DB_MIGRATION_ID)).append(" and ").append("  TABLE_NAME = '")
//					.append(map.get(DataConfig.TABLE_NAME)).append("' and COLUMN_NAME = '")
//					.append(map.get(DataConfig.COLUMN_NAME)).append("' and EXISTING_ECM_ID = '")
//					.append(map.get(DataConfig.ECM_ID)).append("' and MIGRATION_STATUS in ('")
//					.append(DataConfig.MigrationStatusEnum.NOTSTARTED.name()).append("')");
//
//			LOGGER.info("Update Migration status for records - Table - " + map.get(DataConfig.TABLE_NAME)
//					+ ", Column - " + map.get(DataConfig.COLUMN_NAME) + ", EcmId - " + map.get(DataConfig.ECM_ID)
//					+ ", Migration Id - " + map.get(DataConfig.DB_MIGRATION_ID) + ", Query - \n " + st.toString());
//
//			final int uCount = JDBCUtil.executeUpdate(con, st.toString());
//			LOGGER.info(uCount + " Records Updated with status " + DataConfig.MigrationStatusEnum.DUPLICATE.name());
//			st.setLength(0);
//		}
//
//	}

//	private List<Map<String, String>> fetchDuplicateRecords(Connection con) throws SQLException {
//		StringBuilder st = new StringBuilder(50);
//
//		st.append("select count(o.MIGRATION_ID),o.TABLE_NAME,o.COLUMN_NAME,o.EXISTING_ECM_ID,")
//				.append("(select min(i.MIGRATION_ID) from COUCH_MIGRATION i where i.TABLE_NAME = o.TABLE_NAME and i.COLUMN_NAME = o.COLUMN_NAME ")
//				.append(" and i.EXISTING_ECM_ID = o.EXISTING_ECM_ID and migration_status not in ('")
//				.append(DataConfig.MigrationStatusEnum.DUPLICATE.name()).append("')) from ")
//				.append(DataConfig.COUCH_MIGRATION)
//				.append(" o group by o.TABLE_NAME,o.COLUMN_NAME,o.EXISTING_ECM_ID having count(o.MIGRATION_ID) > 1 order by 2,3");
//
//		return JDBCUtil.records(con, st.toString(), new String[] { DataConfig.COUNT_OF_REC, DataConfig.TABLE_NAME,
//				DataConfig.COLUMN_NAME, DataConfig.ECM_ID, DataConfig.DB_MIGRATION_ID });
//	}

	private void prepareData(CouchbaseECMMigrationDTO tableDTO, Connection con) throws SQLException {
		StringBuilder st = new StringBuilder(100);
		st.append("INSERT INTO ").append(DataConfig.COUCH_MIGRATION)
				.append("(MIGRATION_ID,TABLE_NAME,COLUMN_NAME,PRIMARY_ID,EXISTING_ECM_ID,CREATION_TIMESTAMP,LAST_UPDATED_TIMESTAMP) SELECT ")
				.append("COUCH_MIGRATION_SEQ.NEXTVAL,'").append(tableDTO.getTableName()).append("','")
				.append(tableDTO.getColumnName()).append("',").append(tableDTO.getPrimaryIdColumn()).append(",")
				.append(tableDTO.getColumnName()).append(",CURRENT_TIMESTAMP,CURRENT_TIMESTAMP").append(" from ")
				.append(tableDTO.getTableName()).append(" where ").append(tableDTO.getColumnName())
				.append(" is not null and NVL(").append(tableDTO.getColumnName()).append(", 'NULL') != 'NULL' and  (")
				.append(tableDTO.getColumnName())
				.append(" like 'workspace:%') and 0 = (select count(x.MIGRATION_ID) from ")
				.append(DataConfig.COUCH_MIGRATION).append(" x where x.PRIMARY_ID = ")
				.append(tableDTO.getPrimaryIdColumn()).append(" and x.TABLE_NAME = '").append(tableDTO.getTableName())
				.append("' and x.COLUMN_NAME = '").append(tableDTO.getColumnName()).append("')");

		LOGGER.info("Insert data from Table - " + tableDTO.getTableName() + ", Column - " + tableDTO.getColumnName()
				+ ", Query - \n " + st.toString());

		int countOfRecords = JDBCUtil.executeUpdate(con, st.toString());
		LOGGER.info("Total Records - " + countOfRecords);
		st = null;
	}

	private void clearData(CouchbaseECMMigrationDTO tableDTO, Connection con) throws SQLException {
		StringBuilder st = new StringBuilder(100);
		st.append("delete from ").append(DataConfig.COUCH_MIGRATION).append(" where TABLE_NAME = '")
				.append(tableDTO.getTableName()).append("' and COLUMN_NAME = '").append(tableDTO.getColumnName())
				.append("' and MIGRATION_STATUS in ('").append(DataConfig.MigrationStatusEnum.NOTSTARTED.name())
				.append("','").append(DataConfig.MigrationStatusEnum.DUPLICATE.name()).append("')");
		LOGGER.info("Clear data from Table - " + tableDTO.getTableName() + ", Column - " + tableDTO.getColumnName()
				+ ", Query - \n " + st.toString());
		int countOfRecords = JDBCUtil.executeUpdate(con, st.toString());
		LOGGER.info("Total Records - " + countOfRecords);
		st = null;
	}

}
