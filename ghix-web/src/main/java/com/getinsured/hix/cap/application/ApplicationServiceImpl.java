package com.getinsured.hix.cap.application;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.hix.dto.cap.ProgramEligibilityDTO;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;
import com.getinsured.hix.enrollment.util.CreateDTOBean;
import com.getinsured.hix.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.indportal.IndividualPortalConstants;
import com.getinsured.hix.indportal.IndividualPortalUtility;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.iex.dto.qep.QepEventRequest;
import com.getinsured.iex.request.ssap.SsapApplicationRequest;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.ssap.repository.EligibilityProgramRepository;
import com.getinsured.ssap.util.EligibilityConstants;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;

@Service("applicationService")
public class ApplicationServiceImpl implements ApplicationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationServiceImpl.class);

	private static final String NO = "No";
	public static final String DATE_FORMAT = "MM/dd/yyyy";
	public static final String DATE_FORMAT_APPLICANT = "MMM dd, yyyy HH:mm:ss.S";
	private static final String YES = "Yes";
	private static final String Y = "Y";
	private static final String N = "N";
	public static final String CITIZENSHIP_IMMIGRATION_STATUS = "citizenshipImmigrationStatus";
	public static final String CITIZENSHIP_STATUS_INDICATOR = "citizenshipStatusIndicator";
	public static final String SINGLE_STREAMLINED_APPLICATION = "singleStreamlinedApplication";
	public static final String HOUSEHOLD_MEMBER = "householdMember";
	public static final String TAX_HOUSEHOLD = "taxHousehold";
	public static final String SSAP_APPLICANT = "ssapApplicants";
	public static final String NAME = "name";
	public static final String DATEOFBIRTH = "dateOfBirth";
	public static final String HOME_ADDRESS = "homeAddress";
	public static final String STATE = "state";
	public static final String MAILING_ADDRESS = "mailingAddress";
	public static final String BLOOD_RELATIONSHIPS = "bloodRelationship";
	public static final String INDIVIDUAL_PERSONID = "individualPersonId";
	public static final String RELATED_PERSONID = "relatedPersonId";
	public static final String RELATION = "relation";
	public static final String FIRSTNAME = "firstName";
	public static final String LASTNAME = "lastName";
	public static final String GENDER = "gender";
	public static final String HOUSEHOLD_CONTACT = "householdContact";
	public static final String SOCIAL_SECURITY_CARD = "socialSecurityCard";
	public static final String SOCIAL_SECURITY_NUMBER = "socialSecurityNumber";
	public static final String SEEKING_COVERAGE = "applyingForCoverageIndicator";
	public static final String FIRST_NAME_ON_SSN_CARD = "firstNameOnSSNCard";
	public static final String LAST_NAME_ON_SSN_CARD = "lastNameOnSSNCard";
	public static final String PERSONID = "personId";
	public static final String NAME_ON_SSN_CARD_INDICATOR = "nameSameOnSSNCardIndicator";
	public static final Integer PAGE_SIZE = 10;
	public static final String ELIGIBILITYTYPE_TRUE = "TRUE";
	public static final String ELIGIBILITYTYPE_FALSE = "FALSE";
	public static final String CS2 = "CS2";
	public static final String CS3 = "CS3";
	public static final String CS4 = "CS4";
	public static final String CS5 = "CS5";
	public static final String CS6 = "CS6";

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Autowired
	private EligibilityProgramRepository eligibilityProgramRepository;

	@Autowired
	private CreateDTOBean createDTOBean;

	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	@Autowired
	private UserService userService;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
	private Gson platformGson;

	@Autowired
	private IndividualPortalUtility individualPortalUtility;

	@Value("#{configProp['appUrl']}")
	private String appUrl;

	public SsapApplication getSsapApplication(Long applicationId) {
		return ssapApplicationRepository.findOne(applicationId);
	}

	public ProgramEligibilityDTO getApplication(ProgramEligibilityDTO application) throws Exception {

		try {
			// Get Applicants details
			if (application != null && application.getSsapApplicationId() != 0) {
				getApplicantDetails(application);
			}
		} catch (Exception exception) {
			LOGGER.error("Exception while fetching Eligibility Information : " + exception.getMessage());
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.CAP.toString(), null);
		}

		return application;
	}

	private void getApplicantDetails(ProgramEligibilityDTO application) {
		try {

			List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplicationIdAndApplyingForCoverage(application.getSsapApplicationId(), Y);

			List<SsapApplicantDTO> applicants = new ArrayList<SsapApplicantDTO>();
			for (SsapApplicant applicant : ssapApplicants) {

				SsapApplicantDTO ssapApplicantDTO = new SsapApplicantDTO();
				ssapApplicantDTO.setFirstName(applicant.getFirstName());
				ssapApplicantDTO.setLastName(applicant.getLastName());
				ssapApplicantDTO.setId(applicant.getId());
				ssapApplicantDTO.setCsrLevel(applicant.getCsrLevel());
				ssapApplicantDTO.setNativeAmericanFlag(applicant.getNativeAmericanFlag());
				getProgramEligibilityDetails(ssapApplicantDTO);

				String nativeAmericanFlag = ssapApplicantDTO.getNativeAmericanFlag();
				String csrLevel = ssapApplicantDTO.getCsrLevel();

				if (N.equalsIgnoreCase(nativeAmericanFlag) && (CS2.equalsIgnoreCase(csrLevel) || CS3.equalsIgnoreCase(csrLevel))) {
					ssapApplicantDTO.setCsrLevel(NO);
				}
				applicants.add(ssapApplicantDTO);

				if (Y.equalsIgnoreCase(application.getFinancialAssistanceFlag())) {
					BigDecimal maximumAPTC = application.getMaximumAPTC();

					if (null == maximumAPTC || BigDecimal.ZERO.compareTo(maximumAPTC) == 1) {
						application.setMaximumAPTC(BigDecimal.ZERO);
					}
				}
				application.setApplicantEligibility(applicants);
			}
		} catch (Exception exception) {
			LOGGER.error("Exception while fetching Applicant details : " + exception.getMessage());
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	private void getProgramEligibilityDetails(SsapApplicantDTO ssapApplicantDTO) throws ParseException {
		List<EligibilityProgram> eligibilities = eligibilityProgramRepository.findBySsapApplicantId(ssapApplicantDTO.getId());

		for (EligibilityProgram eligibility : eligibilities) {
			if (eligibility.getEligibilityType().equalsIgnoreCase(EligibilityConstants.CSR_ELIGIBILITY_TYPE) && eligibility.getEligibilityIndicator().equalsIgnoreCase(ELIGIBILITYTYPE_TRUE)) {
				ssapApplicantDTO.setCsrEligibility(YES);
			} else if (eligibility.getEligibilityType().equalsIgnoreCase(EligibilityConstants.CSR_ELIGIBILITY_TYPE) && eligibility.getEligibilityIndicator().equalsIgnoreCase(ELIGIBILITYTYPE_FALSE)) {
				ssapApplicantDTO.setCsrEligibility(NO);
			}

			if (eligibility.getEligibilityType().equalsIgnoreCase(EligibilityConstants.APTC_ELIGIBILITY_TYPE) && eligibility.getEligibilityIndicator().equalsIgnoreCase(ELIGIBILITYTYPE_TRUE)) {
				ssapApplicantDTO.setAptcEligibility(YES);
			} else if (eligibility.getEligibilityType().equalsIgnoreCase(EligibilityConstants.APTC_ELIGIBILITY_TYPE) && eligibility.getEligibilityIndicator().equalsIgnoreCase(ELIGIBILITYTYPE_FALSE)) {
				ssapApplicantDTO.setAptcEligibility(NO);
			}

			if (eligibility.getEligibilityType().equalsIgnoreCase(EligibilityConstants.ASSESSED_CHIP_ELIGIBILITY_TYPE) && eligibility.getEligibilityIndicator().equalsIgnoreCase(ELIGIBILITYTYPE_TRUE)) {
				ssapApplicantDTO.setChipEligibility(YES);
			} else if (eligibility.getEligibilityType().equalsIgnoreCase(EligibilityConstants.ASSESSED_CHIP_ELIGIBILITY_TYPE) && eligibility.getEligibilityIndicator().equalsIgnoreCase(ELIGIBILITYTYPE_FALSE)) {
				ssapApplicantDTO.setChipEligibility(NO);
			}

			if (eligibility.getEligibilityType().equalsIgnoreCase(EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE) && eligibility.getEligibilityIndicator().equalsIgnoreCase(ELIGIBILITYTYPE_TRUE)) {
				ssapApplicantDTO.setExchangeEligibility(YES);
			} else if (eligibility.getEligibilityType().equalsIgnoreCase(EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE) && eligibility.getEligibilityIndicator().equalsIgnoreCase(ELIGIBILITYTYPE_FALSE)) {
				ssapApplicantDTO.setExchangeEligibility(NO);
			}

			if ((eligibility.getEligibilityType().equalsIgnoreCase(EligibilityConstants.ASSESSED_MEDICAID_MAGI_ELIGIBILITY_TYPE) && eligibility.getEligibilityIndicator().equalsIgnoreCase(ELIGIBILITYTYPE_TRUE)) || (eligibility.getEligibilityType().equalsIgnoreCase(EligibilityConstants.ASSESSED_MEDICAID_NON_MAGI_ELIGIBILITY_TYPE) && eligibility.getEligibilityIndicator().equalsIgnoreCase(ELIGIBILITYTYPE_TRUE))) {
				ssapApplicantDTO.setMedicaidEligibility(YES);
			} else if ((eligibility.getEligibilityType().equalsIgnoreCase(EligibilityConstants.ASSESSED_MEDICAID_MAGI_ELIGIBILITY_TYPE) && eligibility.getEligibilityIndicator().equalsIgnoreCase(ELIGIBILITYTYPE_FALSE)) || (eligibility.getEligibilityType().equalsIgnoreCase(EligibilityConstants.ASSESSED_MEDICAID_NON_MAGI_ELIGIBILITY_TYPE) && eligibility.getEligibilityIndicator().equalsIgnoreCase(ELIGIBILITYTYPE_FALSE))) {
				ssapApplicantDTO.setMedicaidEligibility(NO);
			}
		}

		if (null == ssapApplicantDTO.getCsrEligibility()) {
			ssapApplicantDTO.setCsrEligibility(NO);
		}

		if (null == ssapApplicantDTO.getCsrLevel() || "N/A".equalsIgnoreCase(ssapApplicantDTO.getCsrLevel())) {
			ssapApplicantDTO.setCsrLevel(NO);
		}

		if (null == ssapApplicantDTO.getAptcEligibility()) {
			ssapApplicantDTO.setAptcEligibility(NO);
		}

		if (null == ssapApplicantDTO.getChipEligibility()) {
			ssapApplicantDTO.setChipEligibility(NO);
		}

		if (null == ssapApplicantDTO.getExchangeEligibility()) {
			ssapApplicantDTO.setExchangeEligibility(NO);
		}

		if (null == ssapApplicantDTO.getMedicaidEligibility()) {
			ssapApplicantDTO.setMedicaidEligibility(NO);
		}
	}

	public String overrideProgramEligibility(ProgramEligibilityDTO programEligibilityDTO, String newApplicationCaseNumber, String oldApplicationCaseNumber, String coverageYear, HttpServletRequest request) {
		String response = "failure";
		SsapApplication newApplication = ssapApplicationRepository.findByCaseNumber(newApplicationCaseNumber);
		SsapApplication oldApplication = ssapApplicationRepository.findByCaseNumber(oldApplicationCaseNumber);

		//Step2 - Restore seekQHP and Current Page
		restoreSeekQHPAndCurrentPage(newApplication, oldApplication);

		// Step3 - Override Program Eligibility.
		response = saveProgramEligibilityDetails(programEligibilityDTO, newApplication, oldApplication, coverageYear);

		// Step4 - Application Rollup
		applicationRollUp(newApplication);

		// Step 5 - Event creation
		individualPortalUtility.logIndividualAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_OVERRIDE_PROGRAM_ELIG, null, request);

		return response;
	}

	private String saveProgramEligibilityDetails(ProgramEligibilityDTO programEligibilityDTO, SsapApplication newApplication, SsapApplication oldApplication, String coverageYear) {
		boolean exchangeEligibility = false;
		boolean csrEligibility = false;
		boolean aptcEligibility = false;
		String status = "failure";

		try {
			String eliStrtDate[] = programEligibilityDTO.getEligibilityStartDate().split("/");
			Date date = new SimpleDateFormat("yyyy-MM-dd").parse(eliStrtDate[2] + "-" + eliStrtDate[0] + "-" + eliStrtDate[1]);
			DateFormat simpleDateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
			Date eligibilityStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(date));

			// Getting last date of year
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, Integer.parseInt(coverageYear));
			calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
			Date eligibilityEndDate = calendar.getTime();
			date = simpleDateFormat.parse(eligibilityEndDate.toString());
			eligibilityEndDate = new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(date));

			Date currentDate = new TSDate();

			List<com.getinsured.iex.ssap.model.SsapApplicant> oldApplicantIds = ssapApplicantRepository.findBySsapApplicationId(oldApplication.getId());

			for (com.getinsured.iex.ssap.model.SsapApplicant oldApplicant : oldApplicantIds) {
				exchangeEligibility = false;
				csrEligibility = false;
				aptcEligibility = false;

				List<EligibilityProgram> eligibilities = eligibilityProgramRepository.findBySsapApplicantId(oldApplicant.getId());
				com.getinsured.iex.ssap.model.SsapApplicant currentApplicant = ssapApplicantRepository.findByApplicantGuidAndAppId(oldApplicant.getApplicantGuid(), newApplication.getId());
				for (EligibilityProgram eligibility : eligibilities) {

					SsapApplicantDTO applicantDTOForUpdate = null;
					EligibilityProgram clonedEligibility = new EligibilityProgram();

					List<SsapApplicantDTO> applicantsForUpdate = programEligibilityDTO.getApplicantEligibility();
					for (SsapApplicantDTO applicantForUpdate : applicantsForUpdate) {
						if (oldApplicant.getId() == applicantForUpdate.getId()) {
							applicantDTOForUpdate = new SsapApplicantDTO();
							applicantDTOForUpdate = applicantForUpdate;
							if (CS2.equalsIgnoreCase(applicantDTOForUpdate.getCsrLevel()) || CS3.equalsIgnoreCase(applicantDTOForUpdate.getCsrLevel()) || CS4.equalsIgnoreCase(applicantDTOForUpdate.getCsrLevel()) || CS5.equalsIgnoreCase(applicantDTOForUpdate.getCsrLevel()) || CS6.equalsIgnoreCase(applicantDTOForUpdate.getCsrLevel())) {
								applicantDTOForUpdate.setCsrEligibility(YES);
							} else {
								applicantDTOForUpdate.setCsrEligibility(NO);
							}
						}
					}
					createDTOBean.copyProperties(clonedEligibility, eligibility);
					if (null != applicantDTOForUpdate) {

						if (EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE.equalsIgnoreCase(eligibility.getEligibilityType())) {
							clonedEligibility.setEligibilityIndicator(applicantDTOForUpdate.getExchangeEligibility().equalsIgnoreCase(YES) ? ELIGIBILITYTYPE_TRUE : ELIGIBILITYTYPE_FALSE);
							exchangeEligibility = true;
						} else if (EligibilityConstants.CSR_ELIGIBILITY_TYPE.equalsIgnoreCase(eligibility.getEligibilityType())) {
							clonedEligibility.setEligibilityIndicator(applicantDTOForUpdate.getCsrEligibility().equalsIgnoreCase(YES) ? ELIGIBILITYTYPE_TRUE : ELIGIBILITYTYPE_FALSE);
							currentApplicant.setCsrLevel(applicantDTOForUpdate.getCsrLevel());
							csrEligibility = true;
						} else if (EligibilityConstants.APTC_ELIGIBILITY_TYPE.equalsIgnoreCase(eligibility.getEligibilityType())) {
							clonedEligibility.setEligibilityIndicator(applicantDTOForUpdate.getAptcEligibility().equalsIgnoreCase(YES) ? ELIGIBILITYTYPE_TRUE : ELIGIBILITYTYPE_FALSE);
							aptcEligibility = true;
						}
						clonedEligibility.setEligibilityStartDate(eligibilityStartDate);
						clonedEligibility.setEligibilityEndDate(eligibilityEndDate);
					}

					clonedEligibility.setId(null);
					clonedEligibility.setSsapApplicant(currentApplicant);
					eligibilityProgramRepository.save(clonedEligibility);
				}

				SsapApplicantDTO applicantForInsert = null;
				List<SsapApplicantDTO> applicants = programEligibilityDTO.getApplicantEligibility();
				for (SsapApplicantDTO tempDTO : applicants) {
					if (oldApplicant.getId() == tempDTO.getId()) {
						applicantForInsert = new SsapApplicantDTO();
						applicantForInsert = tempDTO;
						if (CS2.equalsIgnoreCase(applicantForInsert.getCsrLevel()) || CS3.equalsIgnoreCase(applicantForInsert.getCsrLevel()) || CS4.equalsIgnoreCase(applicantForInsert.getCsrLevel()) || CS5.equalsIgnoreCase(applicantForInsert.getCsrLevel()) || CS6.equalsIgnoreCase(applicantForInsert.getCsrLevel())) {
							applicantForInsert.setCsrEligibility(YES);
						} else {
							applicantForInsert.setCsrEligibility(NO);
						}
					}
				}
				if (null != applicantForInsert) {
					EligibilityProgram programEligibilityForInsert = null;
					if (!csrEligibility && YES.equalsIgnoreCase(applicantForInsert.getCsrEligibility())) {
						programEligibilityForInsert = new EligibilityProgram();
						programEligibilityForInsert.setEligibilityType(EligibilityConstants.CSR_ELIGIBILITY_TYPE);
						programEligibilityForInsert.setEligibilityIndicator(applicantForInsert.getCsrEligibility().equalsIgnoreCase(YES) ? ELIGIBILITYTYPE_TRUE : ELIGIBILITYTYPE_FALSE);// TRUE, FALSE
						programEligibilityForInsert.setEligibilityStartDate(eligibilityStartDate);// if null then current date
						programEligibilityForInsert.setEligibilityEndDate(eligibilityEndDate);// getEligibilityStartDate + 60 days
						programEligibilityForInsert.setEligibilityDeterminationDate(currentDate);// current date

						currentApplicant.setCsrLevel(applicantForInsert.getCsrLevel());
						programEligibilityForInsert.setId(null);
						programEligibilityForInsert.setSsapApplicant(currentApplicant);
						eligibilityProgramRepository.save(programEligibilityForInsert);
					}
					if (!exchangeEligibility && YES.equalsIgnoreCase(applicantForInsert.getExchangeEligibility())) {
						programEligibilityForInsert = new EligibilityProgram();
						programEligibilityForInsert.setEligibilityType(EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE);
						programEligibilityForInsert.setEligibilityIndicator(applicantForInsert.getExchangeEligibility().equalsIgnoreCase(YES) ? ELIGIBILITYTYPE_TRUE : ELIGIBILITYTYPE_FALSE);// TRUE, FALSE
						programEligibilityForInsert.setEligibilityStartDate(eligibilityStartDate);// if null then current date
						programEligibilityForInsert.setEligibilityEndDate(eligibilityEndDate);// getEligibilityStartDate + 60 days
						programEligibilityForInsert.setEligibilityDeterminationDate(currentDate);// current date
						programEligibilityForInsert.setId(null);
						programEligibilityForInsert.setSsapApplicant(currentApplicant);
						eligibilityProgramRepository.save(programEligibilityForInsert);
					}
					if (!aptcEligibility && YES.equalsIgnoreCase(applicantForInsert.getAptcEligibility()) && oldApplication.getFinancialAssistanceFlag().equalsIgnoreCase(Y)) {
						programEligibilityForInsert = new EligibilityProgram();
						programEligibilityForInsert.setEligibilityType(EligibilityConstants.APTC_ELIGIBILITY_TYPE);
						programEligibilityForInsert.setEligibilityIndicator(applicantForInsert.getAptcEligibility().equalsIgnoreCase(YES) ? ELIGIBILITYTYPE_TRUE : ELIGIBILITYTYPE_FALSE);// TRUE, FALSE
						programEligibilityForInsert.setEligibilityStartDate(eligibilityStartDate);// if null then current date
						programEligibilityForInsert.setEligibilityEndDate(eligibilityEndDate);// getEligibilityStartDate + 60 days
						programEligibilityForInsert.setEligibilityDeterminationDate(currentDate);// current date
						programEligibilityForInsert.setId(null);
						programEligibilityForInsert.setSsapApplicant(currentApplicant);
						eligibilityProgramRepository.save(programEligibilityForInsert);
					}
				}
			}

			// 1. if it is financial flow and APTC > 0 then save to ssap_applicatioin table

			if (oldApplication.getFinancialAssistanceFlag().equalsIgnoreCase(Y)) {
				BigDecimal maximumAPTC = programEligibilityDTO.getMaximumAPTC();
				if (null == maximumAPTC || BigDecimal.ZERO.compareTo(maximumAPTC) == 1) {
					newApplication.setMaximumAPTC(BigDecimal.ZERO);
				} else {
					newApplication.setMaximumAPTC(programEligibilityDTO.getMaximumAPTC());
				}
				ssapApplicationRepository.save(newApplication);
			}
			// 2. create ssap_applicant_event as OTHER

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String currentDateStr = dateFormat.format(currentDate);
			startEventCreation(newApplication.getId(), "OVERRIDE_PROGRAM_ELIGIBILITY", currentDateStr, newApplication.getCaseNumber(), newApplication.getApplicationType());

			// 3. update ssap_application.applicaion_status to ER 

			AccountUser currentUser = null;
			currentUser = userService.getLoggedInUser();
			SsapApplicationRequest updateStatusRequest = new SsapApplicationRequest();
			updateStatusRequest.setUserId(null != currentUser ? new BigDecimal(currentUser.getId()) : null);
			updateStatusRequest.setCaseNumber(newApplication.getCaseNumber());
			updateStatusRequest.setApplicationStatus(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());

			LOGGER.info("Updating Application Status to {} for SSAP id: {}", ApplicationStatus.ELIGIBILITY_RECEIVED, newApplication.getId());
			ghixRestTemplate.postForObject(GhixEndPoints.EligibilityEndPoints.SSAP_APP_STATUS_UPDATE, updateStatusRequest, Void.class);
			status = "success";
		} catch (Exception e) {
			LOGGER.error("Exception occurred while overriding eligibilities : {}", e.getMessage());
			status = "failure";
		}
		return status;//success or failure to show in popup
	}

	private void startEventCreation(final long ssapApplicationId, final String qepEvent, final String qepEventDate, final String caseNumber, final String applicationType) {
		try {
			QepEventRequest qepEventRequest = createQepEventRequest(ssapApplicationId, qepEvent, qepEventDate, caseNumber, applicationType);
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(QepEventRequest.class);
			String response = ghixRestTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL + "SsapEventsController/processEvents", writer.writeValueAsString(qepEventRequest), String.class);
			LOGGER.debug("Got response from {} => {}", GhixEndPoints.ELIGIBILITY_URL + "SsapEventsController/processEvents", response);
		} catch (InvalidUserException e) {
			LOGGER.error("Invalid User Exception while creating QepEvents for ssap application id: " + ssapApplicationId + "; event name: " + qepEvent, e);
			throw new GIRuntimeException(e);
		} catch (Exception e) {
			LOGGER.error("Error in creating QepEvents for ssap application id: " + ssapApplicationId + "; event name: " + qepEvent, e);
			throw new GIRuntimeException("Error in creating QepEvents for ssap application id: " + ssapApplicationId, e);
		}
	}

	private QepEventRequest createQepEventRequest(long ssapApplicationId, String qepEvent, String qepEventDate, String caseNumber, String applicationType) throws InvalidUserException {
		QepEventRequest qepEventRequest = new QepEventRequest();
		qepEventRequest.setCaseNumber(caseNumber);
		qepEventRequest.setSsapApplicationId(ssapApplicationId);
		qepEventRequest.setQepEvent(qepEvent);
		qepEventRequest.setQepEventDate(qepEventDate);
		qepEventRequest.setApplicationType(applicationType);
		qepEventRequest.setUserId(userService.getLoggedInUser().getId());

		if (SsapApplicationEventTypeEnum.QEP.name().equalsIgnoreCase(applicationType)) {
			qepEventRequest.setChangeType("QUALIFYING_EVENT");
		} else if (SsapApplicationEventTypeEnum.OE.name().equalsIgnoreCase(applicationType)) {
			qepEventRequest.setChangeType("OE");
		}
		return qepEventRequest;
	}

	private void restoreSeekQHPAndCurrentPage(SsapApplication newApplication, SsapApplication oldApplication) {
		SingleStreamlinedApplication oldSsapApplicationData = ssapJsonBuilder.transformFromJson(oldApplication.getApplicationData());
		SingleStreamlinedApplication newSsapApplicationData = ssapJsonBuilder.transformFromJson(newApplication.getApplicationData());
		if (null != oldSsapApplicationData) {
			newSsapApplicationData.setCurrentPage(oldSsapApplicationData.getCurrentPage());
			List<HouseholdMember> newHouseholdMembers = newSsapApplicationData.getTaxHousehold().get(0).getHouseholdMember();
			List<HouseholdMember> oldHouseholdMembers = oldSsapApplicationData.getTaxHousehold().get(0).getHouseholdMember();

			for (HouseholdMember oldHouseholdMember : oldHouseholdMembers) {
				copyOverSeeksQHP(oldHouseholdMember, newHouseholdMembers);
			}
		}
		newApplication.setApplicationData(ssapJsonBuilder.transformToJson(newSsapApplicationData));
		newApplication.setCurrentPageId(oldApplication.getCurrentPageId());
		ssapApplicationRepository.save(newApplication);
	}

	private void copyOverSeeksQHP(HouseholdMember houseHoldMember, List<HouseholdMember> householdMembers) {
		for (HouseholdMember member : householdMembers) {
			if (houseHoldMember.getApplicantGuid().equals(member.getApplicantGuid())) {
				member.setSeeksQhp(houseHoldMember.isSeeksQhp());
				break;
			}
		}
	}

	private void applicationRollUp(SsapApplication newApplication) {
		final List<SsapApplicant> applicantList = newApplication.getSsapApplicants();

		//Setting Exchange Eligibility Status and CSR for Application
		if (Y.equalsIgnoreCase(newApplication.getNativeAmerican()) && N.equalsIgnoreCase(newApplication.getFinancialAssistanceFlag())) {
			newApplication.setExchangeEligibilityStatus(ExchangeEligibilityStatus.QHP);
			newApplication.setCsrLevel(CS3);
		}

		//Setting Eligibility Status for Application
		newApplication.setEligibilityStatus(EligibilityStatus.CAE);

		//Setting CSR level for Application
		if (applicantList.size() > 0) {
			newApplication.setCsrLevel(getLowestCSRLevel(applicantList));
		} else {
			newApplication.setCsrLevel(null);
		}
		//Setting Eligibility Status
		for (SsapApplicant applicant : applicantList) {
			if (applicant != null) {
				final List<EligibilityProgram> eligibilityPrograms = applicant.getEligibilityProgram();
				applicant.setEligibilityStatus(ExchangeEligibilityStatus.NONE.name());
				for (EligibilityProgram programEligibility : eligibilityPrograms) {
					if ((EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE.equalsIgnoreCase(programEligibility.getEligibilityType())) && ELIGIBILITYTYPE_TRUE.equalsIgnoreCase(programEligibility.getEligibilityIndicator())) {
						applicant.setEligibilityStatus(ExchangeEligibilityStatus.QHP.name());
						break;
					}
				}
			}
		}
		//Update Application
		ssapApplicationRepository.save(newApplication);
	}

	private String getLowestCSRLevel(final List<SsapApplicant> applicantList) {
		List<String> csrLevels = applicantList.stream().map(SsapApplicant::getCsrLevel).distinct().collect(Collectors.toList());
		return Stream.of("CS1", "CS4", "CS5", "CS6", "CS3", "CS2").filter(csrLevels::contains).findFirst().orElse(null);
	}
}
