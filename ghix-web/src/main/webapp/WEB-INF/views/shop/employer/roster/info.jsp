<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/employer-registration-util.tld" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript"> window.history.forward(); </script>
<style> 
	input#file {
		position: absolute;
	}
</style>
<body onunload=""></body>
<!--CONFIRM UPLOAD LIGHTBOX  -->
			<div id="confirmupload" class="modal hide fade"   tabindex="-1" role="dialog" aria-labelledby="confirmuploadLabel" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" onclick="document.location.reload(true);" aria-hidden="true">�</button>
					<h3 id="confirmuploadLabel">Upload CSV?</h3>
				</div>
				<div class="modal-body" id="confirmuploadBody">
				Uploading information for employees already on your Employee list will cause duplicates. Continue uploading list ?
				</div>
				<div class="modal-footer">
					<button class="btn" onclick="document.location.reload(true);" data-dismiss="modal" aria-hidden="true" id="confirmuploadCancel"><spring:message  code='label.cancel'/></button>
					<button class="btn btn-primary" id="confirmuploadButton"><spring:message  code='label.ok'/></button>
				</div>
			</div>
			
<div class="gutter10">
<div class="row-fluid">
	<div class="span9">
		<h1 id="skip"><spring:message  code="label.employerroasterinfoh1"/></h1>
	</div>
</div>
<div class="row-fluid">
	<div class="span3" id="sidebar">
		<util:nav pageName="Add Employees" navStepNo="2"></util:nav>
	</div>
	<!--sidebar-->
	<div class="span9" id="rightpanel">
		<div class="row-fluid">
		<div class="header">
			<h4><spring:message  code="label.employerroasterinfoh2"/></h4>
			</div>
			<div class="gutter10 clearfix">
				<p><spring:message  code="label.employerroasterinfoh3"/></p>
				<p>Don't forget to add yourself, even if you're the business owner.</p>
				<!-- <h4>Employee Information Requirements</h4>
				<ul class="column clearfix">
					<li>Name</li>
					<li>Worksite address</li>
					<li>Estimated annual wages</li>
					<li>Average hours worked per week</li>
					<li>Hire date</li>
					<li>Birth date</li>
					<li>Gender</li>
					<li>Tax ID number</li>
					<li>Email address</li>
					<li>Phone number</li>
					<li>Home address</li>
					<li>Tobacco use</li>
					<li>Native American status</li>
					<li>Marital status</li>
					<li>Number of children</li>
				</ul> -->
				<p><a href="#out-of-state" data-toggle="modal"><spring:message  code="label.employerroasterinfoh4"/></a></p>
				<p><a href="#part-time-employee" data-toggle="modal"><spring:message  code="label.employerroasterinfoh5"/></a></p>
				<form id="frmuploademployees" name="frmuploademployees" action="info" method="post"  enctype="multipart/form-data">
					<df:csrfToken/>
					<div class="form-actions">
						<c:if test="${status == 'NEW' }">							
							<a href="<c:url value="/resources/sampleTemplates/Template for Employee Upload.csv"/>"><spring:message  code="label.employerdownloadcsv"/></a>							
							<p></p>		
							<div class="controls pull-left">
								<label  tabindex="0" class="btn btn-primary" for="file" data-role="button" data-inline="true" data-mini="true" data-corners="false"><spring:message  code="label.employeruploademplist"/></label>
								<input id="file" type="file" onchange="confirmupload('<c:out value='${employeescount}'/>')" name="file" multiple data-role="button" data-inline="true" data-mini="true" data-corners="false"  class="hiddenInput hiddenInputBrowse"/>														
							</div>
						</c:if>
						<div class="pull-right">
							<input type="button" name="submitButton" id="submitButton" onclick="javascript:window.location.href ='addemployee' " class="btn btn-primary" value="<spring:message  code="label.addEmployee"/>">
						</div>
					</div>
				</form>
			</div>
		</div>
		<!--gutter10-->
	</div>
	<!-- rightpanel -->
</div>
<!-- row-fluid -->
</div>

<!--out-of-state Modal -->
<div id="out-of-state" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="outOfState" aria-hidden="true">
  <div class="modal-header noBorder">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="outOfState">&nbsp;</h3>
  </div>
  <div class="modal-body">
    <p><spring:message  code="label.employerroasterinfoh6"/> </p>
    <p><spring:message  code="label.employerroasterinfoh7"/></p>
  </div>
</div>
<!--part-time-employee Modal -->
<div id="part-time-employee" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="partTimeEmployee" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h3 id="partTimeEmployee">&nbsp;</h3>
  </div>
  <div class="modal-body">
    <p><spring:message  code="label.employerroasterinfoh8"/></p>
  </div>
</div>

<div id="employeeRosterError" class="" style="display:none;" tabindex="-1" role="dialog" aria-hidden="true"  title="Import Failed">
  	<div id="errorBody" class="modal-body gutter0">
     	<p>The employee list could not be imported due to errors in the following areas:</p>
     	<p id="errorMsg"></p>
     	<br>
     	<p>Please fix the errors and try again.</p>
    </div>
</div>

<script type="text/javascript">
function confirmupload(empcount){
	$('#confirmupload').modal('hide');
	if(empcount > 0){
	$('#confirmuploadButton').bind('click', function() {
		document.getElementById("frmuploademployees").submit();
    });
	$('#confirmupload').modal('show');
 }else{
	 document.getElementById("frmuploademployees").submit();
 }
}
$(document).ready(function(){
	var errorlist = ${errorlist};
	if(errorlist != null){
		var errorString = "";
			for(var i=0; i < errorlist.length; i++){
				errorString += "- " + errorlist[i] + "<br>";
			};	
		setTimeout(function(){
			$("#errorMsg").append(errorString);
			$("#employeeRosterError").dialog({
				modal: true,
				resizable: false,
				draggable: false,
				minHeight: 300,
				minWidth: 500,
				buttons: {
		        	"Ok": function() {
		            	$( this ).dialog( "close" );
		         	},
		        },
		        create:function () {
		            $(this).parents('.ui-dialog').find('.ui-dialog-buttonpane button').addClass('btn-primary');
		        }
			});
		}, 100);
	};
});
</script>
