package com.getinsured.ssap.util;

import java.util.*;

import org.springframework.core.io.ClassPathResource;

import com.getinsured.iex.ssap.*;
import com.getinsured.iex.ssap.enums.GenderEnum;
import com.getinsured.iex.ssap.enums.OtherStateOrFederalProgramType;
import com.getinsured.iex.ssap.financial.EmployerSponsoredCoverage;
import com.getinsured.iex.ssap.financial.Expense;
import com.getinsured.iex.ssap.financial.Income;
import com.getinsured.iex.ssap.financial.TaxHouseholdComposition;
import com.getinsured.iex.ssap.financial.type.*;

/**
 * Provides methods to build various household compositions.
 * @author Yevgen Golubenko
 * @since 6/14/19
 */
public class FamilyBuilder {
  public static List<HouseholdMember> buildHusbandWifeAndTwoKidsMarriedFilingJointly() {
    List<HouseholdMember> household = new ArrayList<>();

    HouseholdMember husband = new HouseholdMember();
    HouseholdMember wife = new HouseholdMember();
    HouseholdMember child = new HouseholdMember();
    HouseholdMember otherChild = new HouseholdMember();

    husband.setPersonId(1);
    wife.setPersonId(2);
    child.setPersonId(3);
    otherChild.setPersonId(4);

    husband.setAddressId(1);
    wife.setAddressId(1);
    child.setAddressId(1);
    otherChild.setAddressId(1);

    TaxHouseholdComposition thcHusband = new TaxHouseholdComposition();
    TaxHouseholdComposition thcWife = new TaxHouseholdComposition();
    TaxHouseholdComposition thcChild = new TaxHouseholdComposition();
    TaxHouseholdComposition thcOtherChild = new TaxHouseholdComposition();

    thcHusband.setTaxRelationship(TaxRelationship.FILER);
    thcWife.setTaxRelationship(TaxRelationship.DEPENDENT);
    thcChild.setTaxRelationship(TaxRelationship.DEPENDENT);
    thcOtherChild.setTaxRelationship(TaxRelationship.DEPENDENT);

    thcHusband.setTaxFiler(true);
    thcHusband.setClaimingDependents(true);
    thcHusband.setMarried(true);
    thcHusband.setLivesWithSpouse(true);
    thcHusband.setTaxFilingStatus(TaxFilingStatus.FILING_JOINTLY);

    thcWife.setTaxFiler(false);
    thcWife.setClaimingDependents(false);
    thcWife.setMarried(true);
    thcWife.setLivesWithSpouse(true);
    thcWife.setTaxDependent(true);
    thcWife.setTaxFilingStatus(TaxFilingStatus.UNSPECIFIED);

    thcChild.setTaxFiler(false);
    thcChild.setClaimingDependents(false);
    thcChild.setMarried(false);
    thcChild.setLivesWithSpouse(false);
    thcChild.setTaxDependent(true);
    thcChild.setTaxFilingStatus(TaxFilingStatus.UNSPECIFIED);

    thcOtherChild.setTaxFiler(false);
    thcOtherChild.setClaimingDependents(false);
    thcOtherChild.setMarried(false);
    thcOtherChild.setLivesWithSpouse(false);
    thcOtherChild.setTaxDependent(true);
    thcOtherChild.setTaxFilingStatus(TaxFilingStatus.UNSPECIFIED);

    thcHusband.getClaimerIds().add(0);
    thcWife.getClaimerIds().add(0);
    thcChild.getClaimerIds().add(1);
    thcChild.getClaimerIds().add(2);
    thcOtherChild.getClaimerIds().add(1);
    thcOtherChild.getClaimerIds().add(2);

    husband.setTaxHouseholdComposition(thcHusband);
    wife.setTaxHouseholdComposition(thcWife);
    child.setTaxHouseholdComposition(thcChild);
    otherChild.setTaxHouseholdComposition(thcOtherChild);

    husband.getBloodRelationship().add(createBloodRelationshipForMembers(husband.getPersonId(), husband.getPersonId(), BloodRelationshipCode.SELF));
    husband.getBloodRelationship().add(createBloodRelationshipForMembers(husband.getPersonId(), wife.getPersonId(), BloodRelationshipCode.SPOUSE));
    husband.getBloodRelationship().add(createBloodRelationshipForMembers(husband.getPersonId(), child.getPersonId(), BloodRelationshipCode.PARENT_OF_CHILD));
    husband.getBloodRelationship().add(createBloodRelationshipForMembers(husband.getPersonId(), otherChild.getPersonId(), BloodRelationshipCode.PARENT_OF_CHILD));

    husband.getBloodRelationship().add(createBloodRelationshipForMembers(wife.getPersonId(), wife.getPersonId(), BloodRelationshipCode.SELF));
    husband.getBloodRelationship().add(createBloodRelationshipForMembers(wife.getPersonId(), husband.getPersonId(), BloodRelationshipCode.SPOUSE));
    husband.getBloodRelationship().add(createBloodRelationshipForMembers(wife.getPersonId(), child.getPersonId(), BloodRelationshipCode.PARENT_OF_CHILD));
    husband.getBloodRelationship().add(createBloodRelationshipForMembers(wife.getPersonId(), otherChild.getPersonId(), BloodRelationshipCode.PARENT_OF_CHILD));

    husband.getBloodRelationship().add(createBloodRelationshipForMembers(child.getPersonId(), child.getPersonId(), BloodRelationshipCode.SELF));
    husband.getBloodRelationship().add(createBloodRelationshipForMembers(child.getPersonId(), husband.getPersonId(), BloodRelationshipCode.CHILD));
    husband.getBloodRelationship().add(createBloodRelationshipForMembers(child.getPersonId(), wife.getPersonId(), BloodRelationshipCode.CHILD));
    husband.getBloodRelationship().add(createBloodRelationshipForMembers(child.getPersonId(), otherChild.getPersonId(), BloodRelationshipCode.SIBLING_BROTHER_SISTER));

    husband.getBloodRelationship().add(createBloodRelationshipForMembers(otherChild.getPersonId(), otherChild.getPersonId(), BloodRelationshipCode.SELF));
    husband.getBloodRelationship().add(createBloodRelationshipForMembers(otherChild.getPersonId(), husband.getPersonId(), BloodRelationshipCode.CHILD));
    husband.getBloodRelationship().add(createBloodRelationshipForMembers(otherChild.getPersonId(), wife.getPersonId(), BloodRelationshipCode.CHILD));
    husband.getBloodRelationship().add(createBloodRelationshipForMembers(otherChild.getPersonId(), child.getPersonId(), BloodRelationshipCode.SIBLING_BROTHER_SISTER));


    ArrayList<Income> hIncomes = new ArrayList<>();
    ArrayList<Income> wIncomes = new ArrayList<>();
    ArrayList<Income> cIncomes = new ArrayList<>();
    ArrayList<Income> ocIncomes = new ArrayList<>();

    Income hIncome = new Income();
    hIncome.setType(IncomeType.JOB);
    hIncome.setFrequency(Frequency.YEARLY);
    hIncome.setAmount(1_500_00L);
    hIncomes.add(hIncome);

    Income wIncome = new Income();
    wIncome.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    wIncome.setFrequency(Frequency.YEARLY);
    wIncome.setAmount(2_000_00L);
    wIncomes.add(wIncome);

    Income cIncome = new Income();
    cIncome.setType(IncomeType.SELF_EMPLOYMENT);
    cIncome.setFrequency(Frequency.YEARLY);
    cIncome.setAmount(1_000_00L);
    cIncomes.add(cIncome);

    Income ocIncome = new Income();
    ocIncome.setType(IncomeType.SELF_EMPLOYMENT);
    ocIncome.setFrequency(Frequency.YEARLY);
    ocIncome.setAmount(3_500_00L);
    ocIncomes.add(ocIncome);

    husband.setIncomes(hIncomes);
    wife.setIncomes(wIncomes);
    child.setIncomes(cIncomes);
    otherChild.setIncomes(ocIncomes);

    husband.setDateOfBirth(new Date(2019-1900- 64, Calendar.JANUARY, 1));
    wife.setDateOfBirth(new Date(2019-1900-    35, Calendar.JANUARY, 1));
    child.setDateOfBirth(new Date(2019-1900-   19, Calendar.JANUARY,1));
    otherChild.setDateOfBirth(new Date(2019-1900-   18, Calendar.JANUARY, 1));

    household.add(husband);
    household.add(wife);
    household.add(child);
    household.add(otherChild);

    return household;
  }

  // https://docs.google.com/spreadsheets/d/1Cqi1WyU9VGCFkOqxiVE93cZxTTLuOA4tIxi3Ll5OwRA/edit?ts=5d01413f&pli=1#gid=288665812
  public static List<HouseholdMember> buildTestCase_H_Household() {
    final List<HouseholdMember> members = new ArrayList<>(4);

    final HouseholdMember primary = new HouseholdMember();
    final HouseholdMember spouse = new HouseholdMember();
    final HouseholdMember dependentA = new HouseholdMember();
    final HouseholdMember dependentB = new HouseholdMember();

    // Person Id
    primary.setPersonId(1);
    spouse.setPersonId(2);
    dependentA.setPersonId(3);
    dependentB.setPersonId(4);

    // Guids
    primary.setApplicantGuid("1001");
    spouse.setApplicantGuid("2001");
    dependentA.setApplicantGuid("3001");
    dependentB.setApplicantGuid("4001");

    // Address Id
    primary.setAddressId(1);
    spouse.setAddressId(1);
    dependentA.setAddressId(1);
    dependentB.setAddressId(1);

    // Genders
    primary.setGender(GenderEnum.MALE.value());
    spouse.setGender(GenderEnum.FEMALE.value());
    dependentA.setGender(GenderEnum.FEMALE.value());
    dependentB.setGender(GenderEnum.MALE.value());

    setName(primary, "Primary", "Contact");
    setName(spouse, "Spouse", "A");
    setName(dependentA, "Dependent", "A");
    setName(dependentB, "Dependent", "b");

    // DOB's
    primary.setDateOfBirth(new Date(63, Calendar.JANUARY, 1));
    spouse.setDateOfBirth(new Date(67, Calendar.JANUARY, 1));
    dependentA.setDateOfBirth(new Date(108, Calendar.JANUARY, 1));
    dependentB.setDateOfBirth(new Date(100, Calendar.JANUARY, 1));

    // Relationships
    primary.getBloodRelationship().add(createBloodRelationshipForMembers(primary.getPersonId(), primary.getPersonId(), BloodRelationshipCode.SELF));
    primary.getBloodRelationship().add(createBloodRelationshipForMembers(primary.getPersonId(), spouse.getPersonId(), BloodRelationshipCode.SPOUSE));
    primary.getBloodRelationship().add(createBloodRelationshipForMembers(primary.getPersonId(), dependentA.getPersonId(), BloodRelationshipCode.PARENT_OF_CHILD));
    primary.getBloodRelationship().add(createBloodRelationshipForMembers(primary.getPersonId(), dependentB.getPersonId(), BloodRelationshipCode.PARENT_OF_CHILD));

    primary.getBloodRelationship().add(createBloodRelationshipForMembers(spouse.getPersonId(), spouse.getPersonId(), BloodRelationshipCode.SELF));
    primary.getBloodRelationship().add(createBloodRelationshipForMembers(spouse.getPersonId(), primary.getPersonId(), BloodRelationshipCode.SPOUSE));
    primary.getBloodRelationship().add(createBloodRelationshipForMembers(spouse.getPersonId(), dependentA.getPersonId(), BloodRelationshipCode.PARENT_OF_CHILD));
    primary.getBloodRelationship().add(createBloodRelationshipForMembers(spouse.getPersonId(), dependentB.getPersonId(), BloodRelationshipCode.PARENT_OF_CHILD));

    primary.getBloodRelationship().add(createBloodRelationshipForMembers(dependentA.getPersonId(), dependentA.getPersonId(), BloodRelationshipCode.SELF));
    primary.getBloodRelationship().add(createBloodRelationshipForMembers(dependentA.getPersonId(), primary.getPersonId(), BloodRelationshipCode.CHILD));
    primary.getBloodRelationship().add(createBloodRelationshipForMembers(dependentA.getPersonId(), spouse.getPersonId(), BloodRelationshipCode.CHILD));
    primary.getBloodRelationship().add(createBloodRelationshipForMembers(dependentA.getPersonId(), dependentB.getPersonId(), BloodRelationshipCode.SIBLING_BROTHER_SISTER));

    primary.getBloodRelationship().add(createBloodRelationshipForMembers(dependentB.getPersonId(), dependentB.getPersonId(), BloodRelationshipCode.SELF));
    primary.getBloodRelationship().add(createBloodRelationshipForMembers(dependentB.getPersonId(), primary.getPersonId(), BloodRelationshipCode.CHILD));
    primary.getBloodRelationship().add(createBloodRelationshipForMembers(dependentB.getPersonId(), spouse.getPersonId(), BloodRelationshipCode.CHILD));
    primary.getBloodRelationship().add(createBloodRelationshipForMembers(dependentB.getPersonId(), dependentA.getPersonId(), BloodRelationshipCode.SIBLING_BROTHER_SISTER));

    // Applicant/Non-Applicant
    primary.setApplyingForCoverageIndicator(true);
    spouse.setApplyingForCoverageIndicator(true);
    dependentA.setApplyingForCoverageIndicator(true);
    dependentB.setApplyingForCoverageIndicator(false);

    // SSN's
    final SocialSecurityCard primarySsn = new SocialSecurityCard();
    final SocialSecurityCard spouseSsn = new SocialSecurityCard();
    final SocialSecurityCard dependentASsn = new SocialSecurityCard();
    final SocialSecurityCard dependentBSsn = new SocialSecurityCard();
    primarySsn.setSocialSecurityNumber("555553576");
    spouseSsn.setSocialSecurityNumber("555553577");
    dependentASsn.setSocialSecurityNumber("555553578");

    // Marital Status
    primary.setMarriedIndicator(true);
    spouse.setMarriedIndicator(true);
    dependentA.setMarriedIndicator(false);
    dependentB.setMarriedIndicator(false);

    // Parent/Caretaker/Relative
    // TODO: fill

    // American Indian / Alaska Native
    final AmericanIndianAlaskaNative primaryAiAn = new AmericanIndianAlaskaNative();
    final AmericanIndianAlaskaNative spouseAiAn = new AmericanIndianAlaskaNative();
    final AmericanIndianAlaskaNative dependentAAiAn = new AmericanIndianAlaskaNative();
    final AmericanIndianAlaskaNative dependentBAiAn = new AmericanIndianAlaskaNative();

    primaryAiAn.setMemberOfFederallyRecognizedTribeIndicator(true);
    spouseAiAn.setMemberOfFederallyRecognizedTribeIndicator(false);
    dependentAAiAn.setMemberOfFederallyRecognizedTribeIndicator(false);
    dependentBAiAn.setMemberOfFederallyRecognizedTribeIndicator(false);

    primary.setAmericanIndianAlaskaNative(primaryAiAn);
    spouse.setAmericanIndianAlaskaNative(null);
    dependentA.setAmericanIndianAlaskaNative(dependentAAiAn);
    dependentB.setAmericanIndianAlaskaNative(dependentBAiAn);

    // Relationship and living situation
    primary.setLivesWithHouseholdContactIndicator(true);
    spouse.setLivesWithHouseholdContactIndicator(true);
    dependentA.setLivesWithHouseholdContactIndicator(true);
    dependentB.setLivesWithHouseholdContactIndicator(true);

    // Citizenship / Immigration Status
    final CitizenshipImmigrationStatus primaryCitizenship = new CitizenshipImmigrationStatus();
    primaryCitizenship.setCitizenshipStatusIndicator(true);
    primary.setCitizenshipImmigrationStatus(primaryCitizenship);

    final CitizenshipImmigrationStatus spouseCitizenship = new CitizenshipImmigrationStatus();
    spouseCitizenship.setCitizenshipStatusIndicator(false);
    spouseCitizenship.setEligibleImmigrationStatusIndicator(true);
    spouseCitizenship.setLivedIntheUSSince1996Indicator(true);

    final CitizenshipImmigrationStatus dependentACitizenship = new CitizenshipImmigrationStatus();
    dependentACitizenship.setCitizenshipStatusIndicator(true);
    dependentA.setCitizenshipImmigrationStatus(dependentACitizenship);

    // Tax Filing Status
    final TaxHouseholdComposition primaryTaxHhComposition = new TaxHouseholdComposition();
    primaryTaxHhComposition.setTaxFiler(true);
    primaryTaxHhComposition.setClaimingDependents(true);
    primaryTaxHhComposition.setLivesWithSpouse(true);
    primaryTaxHhComposition.setMarried(true);
    primaryTaxHhComposition.setTaxFilingStatus(TaxFilingStatus.FILING_JOINTLY);
    primaryTaxHhComposition.setTaxRelationship(TaxRelationship.FILER);
    primaryTaxHhComposition.getClaimerIds().add(0); // Tax Filer itself, not claimed by anybody
    primary.setTaxHouseholdComposition(primaryTaxHhComposition);

    final TaxHouseholdComposition spouseTaxHhComposition = new TaxHouseholdComposition();
    spouseTaxHhComposition.setTaxFiler(false);
    spouseTaxHhComposition.setClaimingDependents(true);
    spouseTaxHhComposition.setLivesWithSpouse(true);
    spouseTaxHhComposition.setMarried(true);
    spouseTaxHhComposition.setTaxDependent(false);
    spouseTaxHhComposition.setTaxFilingStatus(TaxFilingStatus.FILING_JOINTLY);
    spouseTaxHhComposition.setTaxRelationship(TaxRelationship.FILER);
    spouseTaxHhComposition.getClaimerIds().add(0); // Tax Filer itself, not claimed by anybody
    spouse.setTaxHouseholdComposition(spouseTaxHhComposition);

    final TaxHouseholdComposition dependentATaxHhComposition = new TaxHouseholdComposition();
    dependentATaxHhComposition.setTaxFiler(false);
    dependentATaxHhComposition.setClaimingDependents(false);
    dependentATaxHhComposition.setLivesWithSpouse(false);
    dependentATaxHhComposition.setMarried(false);
    dependentATaxHhComposition.setTaxDependent(true);
    dependentATaxHhComposition.setTaxFilingStatus(TaxFilingStatus.UNSPECIFIED);
    dependentATaxHhComposition.setTaxRelationship(TaxRelationship.DEPENDENT);
    dependentATaxHhComposition.getClaimerIds().add(1); // Each parent is claimant of this person
    dependentATaxHhComposition.getClaimerIds().add(2); // Each parent is claimant of this person
    dependentA.setTaxHouseholdComposition(dependentATaxHhComposition);

    final TaxHouseholdComposition dependentBTaxHhComposition = new TaxHouseholdComposition();
    dependentBTaxHhComposition.setTaxFiler(false);
    dependentBTaxHhComposition.setClaimingDependents(false);
    dependentBTaxHhComposition.setLivesWithSpouse(false);
    dependentBTaxHhComposition.setMarried(false);
    dependentBTaxHhComposition.setTaxDependent(true);
    dependentBTaxHhComposition.setTaxFilingStatus(TaxFilingStatus.UNSPECIFIED);
    dependentBTaxHhComposition.setTaxRelationship(TaxRelationship.DEPENDENT);
    dependentBTaxHhComposition.getClaimerIds().add(1); // Each parent is claimant of this person
    dependentBTaxHhComposition.getClaimerIds().add(2); // Each parent is claimant of this person
    dependentB.setTaxHouseholdComposition(dependentBTaxHhComposition);


    // Incomes
    List<Income> primaryIncomes = new ArrayList<>();
    List<Expense> primaryExpenses = new ArrayList<>();

    Income primaryJob1 = new Income();
    Income primaryJob2 = new Income();
    Income primaryJob3 = new Income();

    primaryJob1.setAmount(18_600_88L);
    primaryJob1.setType(IncomeType.JOB);
    primaryJob1.setFrequency(Frequency.YEARLY);
    primaryJob1.setSourceName("Primary Job 1 (18600.88 yearly)");
    primaryJob1.setTribalAmount(100_00L * 12L);

    primaryJob2.setAmount(5_159_17L);
    primaryJob2.setType(IncomeType.JOB);
    primaryJob2.setFrequency(Frequency.YEARLY);
    primaryJob2.setSourceName("Primary Job 2 (5159.17 yearly)");

    primaryJob3.setAmount(99_37L);
    primaryJob3.setType(IncomeType.JOB);
    primaryJob3.setFrequency(Frequency.WEEKLY);
    primaryJob3.setSourceName("Primary Job 3 (99.37 weekly)");

    primaryIncomes.add(primaryJob1);
    primaryIncomes.add(primaryJob2);
    primaryIncomes.add(primaryJob3);

    // primary -> Expenses
    Expense primaryExpense1 = new Expense();
    primaryExpense1.setType(ExpenseType.ALIMONY);
    primaryExpense1.setAmount(197_77);
    primaryExpense1.setFrequency(Frequency.MONTHLY);

    primaryExpenses.add(primaryExpense1);

    primary.setIncomes(primaryIncomes);
    primary.setExpenses(primaryExpenses);

    List<Income> spouseIncomes = new ArrayList<>();
    Income spouseJob1 = new Income();
    spouseJob1.setAmount(18_600_00L);
    spouseJob1.setType(IncomeType.JOB);
    spouseJob1.setFrequency(Frequency.YEARLY); // TODO: Year?
    spouseJob1.setSourceName("Spouse Job 1 (18600.00 yearly)");

    spouseIncomes.add(spouseJob1);
    spouse.setIncomes(spouseIncomes);

    // Medicaid/CHIP Denial
    primary.setMedicaidDenied(false);
    spouse.setMedicaidDenied(false);
    dependentA.setMedicaidDenied(false);

    // Non-MAGI Questions
    // Nothing

    // Full-Time Student Questions
    // Nothing

    // Incarceration Questions
    IncarcerationStatus primaryIncarcerated = new IncarcerationStatus();
    primaryIncarcerated.setIncarcerationStatusIndicator(false);
    primaryIncarcerated.setIncarcerationAsAttestedIndicator(false);
    primary.setIncarcerationStatus(primaryIncarcerated);

    IncarcerationStatus spouseIncarcerated = primaryIncarcerated;
    spouse.setIncarcerationStatus(spouseIncarcerated);

    IncarcerationStatus dependentAIncarcerated = spouseIncarcerated;
    dependentA.setIncarcerationStatus(dependentAIncarcerated);

    // Pregnancy Questions
    SpecialCircumstances spouseSpecialCircumstances = new SpecialCircumstances();
    spouseSpecialCircumstances.setPregnantIndicator(false);
    spouse.setSpecialCircumstances(spouseSpecialCircumstances);

    SpecialCircumstances dependentASpecialCircumstances = spouseSpecialCircumstances;
    dependentA.setSpecialCircumstances(dependentASpecialCircumstances);

    // Foster Care Questions
    // Nothing

    // COBRA or other coverage
    CurrentOtherInsurance spouseCurrentOtherInsurance = new CurrentOtherInsurance();
    List<OtherStateOrFederalProgram> spouseOtherStateOrFederalPrograms = new ArrayList<>();
    OtherStateOrFederalProgram spouseOtherStateOrFederalProgram = new OtherStateOrFederalProgram();
    spouseOtherStateOrFederalProgram.setType(OtherStateOrFederalProgramType.TRICARE);
    spouseOtherStateOrFederalProgram.setName(OtherStateOrFederalProgramType.TRICARE.name());
    spouseOtherStateOrFederalProgram.setEligible(true);
    spouseOtherStateOrFederalPrograms.add(spouseOtherStateOrFederalProgram);
    spouseCurrentOtherInsurance.setOtherStateOrFederalPrograms(spouseOtherStateOrFederalPrograms);
    HealthCoverage spouseHealthCoverage = new HealthCoverage();
    spouseHealthCoverage.setCurrentOtherInsurance(spouseCurrentOtherInsurance);
    spouse.setHealthCoverage(spouseHealthCoverage);

    // Employer Sponsored Coverage
    EmployerSponsoredCoverage employerSponsoredCoverage = new EmployerSponsoredCoverage();
    employerSponsoredCoverage.setEmployerPremium(800_00L);
    employerSponsoredCoverage.setEmployerPremiumFrequency(Frequency.MONTHLY);
    employerSponsoredCoverage.setMemberName("Primary Member Name");
    primary.setEmployerSponsoredCoverage(Collections.singletonList(employerSponsoredCoverage));

    // Legal Attestations

    // Life Changes (SEPs)
    // Nothing

    // Address
    HomeAddress homeAddress = new HomeAddress();
    homeAddress.setAddressId(1);
    homeAddress.setCounty("Clark");
    homeAddress.setCity("Las Vegas");
    homeAddress.setState("NV");
    homeAddress.setPostalCode("89109");
    homeAddress.setStreetAddress1("3001 S Las Vegas Blvd");
    homeAddress.setCountyCode("32003");
    homeAddress.setPrimaryAddressCountyFipsCode("32003");

    MailingAddress mailingAddress = new MailingAddress();
    mailingAddress.setCounty(homeAddress.getCounty());
    mailingAddress.setCity(homeAddress.getCity());
    mailingAddress.setState(homeAddress.getState());
    mailingAddress.setPostalCode(homeAddress.getPostalCode());
    mailingAddress.setStreetAddress1(homeAddress.getStreetAddress1());
    mailingAddress.setCountyCode(homeAddress.getCountyCode());
    mailingAddress.setPrimaryAddressCountyFipsCode(homeAddress.getPrimaryAddressCountyFipsCode());

    HouseholdContact hhContact = new HouseholdContact();
    hhContact.setHomeAddress(homeAddress);
    hhContact.setMailingAddress(mailingAddress);

    primary.setHouseholdContact(hhContact);
    spouse.setHouseholdContact(hhContact);
    dependentA.setHouseholdContact(hhContact);
    dependentB.setHouseholdContact(hhContact);

    // Add all and return
    members.addAll(Arrays.asList(primary, spouse, dependentA, dependentB));
    return members;
  }

  private static void setName(HouseholdMember member, String dependent, String lastName) {
    Name n = new Name();
    n.setFirstName(dependent);
    n.setLastName(lastName);
    member.setName(n);
  }

  // https://docs.google.com/spreadsheets/d/1Cqi1WyU9VGCFkOqxiVE93cZxTTLuOA4tIxi3Ll5OwRA/edit?ts=5d01413f&pli=1#gid=288665812
  public static List<HouseholdMember> buildTestCase_E_Household() {
    final List<HouseholdMember> members = new ArrayList<>();
    final HouseholdMember grandpa = new HouseholdMember();
    final HouseholdMember son = new HouseholdMember();
    final HouseholdMember grandChild = new HouseholdMember();

    // Person Id
    grandpa.setPersonId(1);
    son.setPersonId(2);
    grandChild.setPersonId(3);

    // Names
    setName(grandpa, "Grandpa", "Ee");
    setName(son, "Sonny", "Ee");
    setName(grandChild, "Baby", "Ee");

    // Address ids
    grandpa.setAddressId(1);
    son.setAddressId(2);
    grandChild.setAddressId(2);


    // Genders
    grandpa.setGender(GenderEnum.MALE.value());
    son.setGender(GenderEnum.MALE.value());
    grandChild.setGender(GenderEnum.FEMALE.value());

    // DOB's
    grandpa.setDateOfBirth(new Date(54, Calendar.JANUARY, 1));
    son.setDateOfBirth(new Date(93, Calendar.JANUARY, 1));
    grandChild.setDateOfBirth(new Date(112, Calendar.JANUARY, 1));


    // Relationships

    grandpa.getBloodRelationship().add(createBloodRelationshipForMembers(grandpa.getPersonId(), grandpa.getPersonId(), BloodRelationshipCode.SELF));
    grandpa.getBloodRelationship().add(createBloodRelationshipForMembers(grandpa.getPersonId(), son.getPersonId(), BloodRelationshipCode.PARENT_OF_CHILD));
    grandpa.getBloodRelationship().add(createBloodRelationshipForMembers(grandpa.getPersonId(), grandChild.getPersonId(), BloodRelationshipCode.GRANDPARENT));

    grandpa.getBloodRelationship().add(createBloodRelationshipForMembers(son.getPersonId(), son.getPersonId(), BloodRelationshipCode.SELF));
    grandpa.getBloodRelationship().add(createBloodRelationshipForMembers(son.getPersonId(), grandpa.getPersonId(), BloodRelationshipCode.CHILD));
    grandpa.getBloodRelationship().add(createBloodRelationshipForMembers(son.getPersonId(), grandChild.getPersonId(), BloodRelationshipCode.PARENT_OF_CHILD));

    grandpa.getBloodRelationship().add(createBloodRelationshipForMembers(grandChild.getPersonId(), grandChild.getPersonId(), BloodRelationshipCode.SELF));
    grandpa.getBloodRelationship().add(createBloodRelationshipForMembers(grandChild.getPersonId(), son.getPersonId(), BloodRelationshipCode.CHILD));
    grandpa.getBloodRelationship().add(createBloodRelationshipForMembers(grandChild.getPersonId(), grandpa.getPersonId(), BloodRelationshipCode.GRANDCHILD));

    // Applicant/Non-Applicant
    grandpa.setApplyingForCoverageIndicator(true);
    son.setApplyingForCoverageIndicator(true);
    grandChild.setApplyingForCoverageIndicator(true);

    // SSN's
    final SocialSecurityCard primarySsn = new SocialSecurityCard();
    final SocialSecurityCard spouseSsn = new SocialSecurityCard();
    final SocialSecurityCard dependentASsn = new SocialSecurityCard();
    final SocialSecurityCard dependentBSsn = new SocialSecurityCard();
    primarySsn.setSocialSecurityNumber("555553576");
    spouseSsn.setSocialSecurityNumber("555553577");
    dependentASsn.setSocialSecurityNumber("555553578");

    // Marital Status
    grandpa.setMarriedIndicator(false);
    son.setMarriedIndicator(false);
    grandChild.setMarriedIndicator(false);

    // Parent/Caretaker/Relative
    // TODO: fill

    // American Indian / Alaska Native
    final AmericanIndianAlaskaNative grandpaAiAn = new AmericanIndianAlaskaNative();
    final AmericanIndianAlaskaNative sonAiAn = new AmericanIndianAlaskaNative();
    final AmericanIndianAlaskaNative grandChildAiAn = new AmericanIndianAlaskaNative();

    grandpaAiAn.setMemberOfFederallyRecognizedTribeIndicator(false);
    sonAiAn.setMemberOfFederallyRecognizedTribeIndicator(false);
    grandChildAiAn.setMemberOfFederallyRecognizedTribeIndicator(false);

    grandpa.setAmericanIndianAlaskaNative(grandpaAiAn);
    son.setAmericanIndianAlaskaNative(sonAiAn);
    grandChild.setAmericanIndianAlaskaNative(grandChildAiAn);


    // Citizenship / Immigration Status
    final CitizenshipImmigrationStatus citizen = new CitizenshipImmigrationStatus();
    citizen.setCitizenshipStatusIndicator(true);

    grandpa.setCitizenshipImmigrationStatus(citizen);
    son.setCitizenshipImmigrationStatus(citizen);
    grandChild.setCitizenshipImmigrationStatus(citizen);

    // Relationship and living situation
    grandpa.setLivesWithHouseholdContactIndicator(true); // Grandpa himself at 1 Broadway
    son.setLivesWithHouseholdContactIndicator(false); // Father with son at 2 Broadway
    grandChild.setLivesWithHouseholdContactIndicator(false); // Son with Father at 2 Broadway

    // Tax Filing Status
    final TaxHouseholdComposition grandpaTaxHhComposition = new TaxHouseholdComposition();
    grandpaTaxHhComposition.setTaxFiler(true);
    grandpaTaxHhComposition.setClaimingDependents(true);
    grandpaTaxHhComposition.setLivesWithSpouse(false);
    grandpaTaxHhComposition.setMarried(false);
    /*
      Tax filer, files own return, claims child and grandchild as dependents
     */
    grandpaTaxHhComposition.setTaxFilingStatus(TaxFilingStatus.HEAD_OF_HOUSEHOLD);
    grandpaTaxHhComposition.setTaxRelationship(TaxRelationship.FILER);
    grandpaTaxHhComposition.getClaimerIds().add(0);

    grandpa.setTaxHouseholdComposition(grandpaTaxHhComposition);

    final TaxHouseholdComposition sonTaxHhComposition = new TaxHouseholdComposition();
    sonTaxHhComposition.setTaxFiler(false);
    sonTaxHhComposition.setClaimingDependents(false);
    sonTaxHhComposition.setLivesWithSpouse(false);
    sonTaxHhComposition.setMarried(false);
    sonTaxHhComposition.setTaxDependent(true);
    sonTaxHhComposition.setClaimedOutsideHousehold(false);
    sonTaxHhComposition.setTaxFilingStatus(TaxFilingStatus.UNSPECIFIED);
    sonTaxHhComposition.setTaxRelationship(TaxRelationship.DEPENDENT);
    sonTaxHhComposition.getClaimerIds().add(1);

    son.setTaxHouseholdComposition(sonTaxHhComposition);

    final TaxHouseholdComposition grandChildTaxHhComposition = new TaxHouseholdComposition();
    grandChildTaxHhComposition.setTaxFiler(false);
    grandChildTaxHhComposition.setClaimingDependents(false);
    grandChildTaxHhComposition.setLivesWithSpouse(false);
    grandChildTaxHhComposition.setMarried(false);
    grandChildTaxHhComposition.setTaxDependent(true);
    grandChildTaxHhComposition.setClaimedOutsideHousehold(false);
    grandChildTaxHhComposition.setTaxFilingStatus(TaxFilingStatus.UNSPECIFIED);
    grandChildTaxHhComposition.setTaxRelationship(TaxRelationship.DEPENDENT);
    grandChildTaxHhComposition.getClaimerIds().add(1);

    grandChild.setTaxHouseholdComposition(grandChildTaxHhComposition);

    // Incomes
    List<Income> grandpaIncomes = new ArrayList<>();

    Income grandpaJob = new Income();
    Income grandpaRetirement = new Income();

    grandpaJob.setAmount(1_500_00L);
    grandpaJob.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    grandpaJob.setFrequency(Frequency.MONTHLY);

    grandpaRetirement.setAmount(1_000_00L);
    grandpaRetirement.setType(IncomeType.RETIREMENT);
    grandpaRetirement.setFrequency(Frequency.MONTHLY);

    grandpaIncomes.addAll(Arrays.asList(grandpaJob, grandpaRetirement));
    grandpa.setIncomes(grandpaIncomes);

    List<Income> sonIncomes = new ArrayList<>();
    Income sonIncome = new Income();
    sonIncome.setAmount(600_00L);
    sonIncome.setType(IncomeType.SELF_EMPLOYMENT);
    sonIncome.setFrequency(Frequency.MONTHLY);
    sonIncomes.add(sonIncome);
    son.setIncomes(sonIncomes);

    // Address
    HomeAddress homeAddress = new HomeAddress();
    homeAddress.setAddressId(2);
    homeAddress.setCounty("Clark");
    homeAddress.setCity("Las Vegas");
    homeAddress.setState("NV");
    homeAddress.setPostalCode("89109");
    homeAddress.setStreetAddress1("3001 S Las Vegas Blvd");
    homeAddress.setCountyCode("32003");
    homeAddress.setPrimaryAddressCountyFipsCode("32003");

    MailingAddress mailingAddress = new MailingAddress();
    mailingAddress.setCounty(homeAddress.getCounty());
    mailingAddress.setCity(homeAddress.getCity());
    mailingAddress.setState(homeAddress.getState());
    mailingAddress.setPostalCode(homeAddress.getPostalCode());
    mailingAddress.setStreetAddress1(homeAddress.getStreetAddress1());
    mailingAddress.setCountyCode(homeAddress.getCountyCode());
    mailingAddress.setPrimaryAddressCountyFipsCode(homeAddress.getPrimaryAddressCountyFipsCode());

    HouseholdContact hhContact = new HouseholdContact();
    hhContact.setHomeAddress(homeAddress);
    hhContact.setMailingAddress(mailingAddress);

    HomeAddress primaryHomeAddress = new HomeAddress();
    primaryHomeAddress.setAddressId(1);
    primaryHomeAddress.setCounty("Clark");
    primaryHomeAddress.setCity("Las Vegas");
    primaryHomeAddress.setState("NV");
    primaryHomeAddress.setPostalCode("89109");
    primaryHomeAddress.setStreetAddress1("1 S Las Vegas Blvd");
    primaryHomeAddress.setCountyCode("32003");
    primaryHomeAddress.setPrimaryAddressCountyFipsCode("32003");

    MailingAddress primaryMailingAddress = new MailingAddress();
    primaryMailingAddress.setCounty(primaryHomeAddress.getCounty());
    primaryMailingAddress.setCity(primaryHomeAddress.getCity());
    primaryMailingAddress.setState(primaryHomeAddress.getState());
    primaryMailingAddress.setPostalCode(primaryHomeAddress.getPostalCode());
    primaryMailingAddress.setStreetAddress1(primaryHomeAddress.getStreetAddress1());
    primaryMailingAddress.setCountyCode(primaryHomeAddress.getCountyCode());
    primaryMailingAddress.setPrimaryAddressCountyFipsCode(primaryHomeAddress.getPrimaryAddressCountyFipsCode());

    HouseholdContact hhContactPrimary = new HouseholdContact();
    hhContactPrimary.setHomeAddress(primaryHomeAddress);
    hhContactPrimary.setMailingAddress(primaryMailingAddress);

    grandpa.setHouseholdContact(hhContactPrimary);
    son.setHouseholdContact(hhContact);
    grandChild.setHouseholdContact(hhContact);

    // Add and return
    members.addAll(Arrays.asList(grandpa, son, grandChild));
    return members;
  }

  /**
   * Builds sample {@link SingleStreamlinedApplication} taken from the JSON (from database column).
   * @param jsonId file id to build from, e.g. sample-(jsonId).json
   * @return {@link SingleStreamlinedApplication}.
   */
  public static SingleStreamlinedApplication sampleApplicationFromJson(final int jsonId) {
    final ClassPathResource classPathResource = new ClassPathResource(String.format("/households/sample-%d.json", jsonId));
    final String json = JsonUtil.readFile(classPathResource);
    final SingleStreamlinedApplication application = JsonUtil.getSingleStreamlinedApplicationFromJson(json);
    return application;
  }

  public static SingleStreamlinedApplication sampleApplicationFromJson(final String jsonFile) {
    final ClassPathResource classPathResource = new ClassPathResource(String.format("/households/%s", jsonFile));
    final String json = JsonUtil.readFile(classPathResource);
    final SingleStreamlinedApplication application = JsonUtil.getSingleStreamlinedApplicationFromJson(json);
    return application;
  }

  public static BloodRelationship createBloodRelationshipForMembers(int forWhoPersonId, int toWhoPersonId, BloodRelationshipCode code) {
      BloodRelationship br = new BloodRelationship();
      br.setIndividualPersonId(String.valueOf(forWhoPersonId));
      br.setRelatedPersonId(String.valueOf(toWhoPersonId));
      br.setRelation(code.getCode());
      return br;
  }
}
