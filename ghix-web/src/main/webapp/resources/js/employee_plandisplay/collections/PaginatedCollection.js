(function (collections, pagination, model) {
	collections.Plans = Backbone.Collection.extend({
		model : model,
		url : 'getEmployeePlans?'+(Math.random()*10),
		
		/**
		 * @param resp the response returned by the server
		 * @returns (Array) plans
		 */
		parse : function (resp) {
			var plans = resp;
			return plans;
		}
	});
	
	_.extend(collections.Plans.prototype, pagination);
})(App.collections, App.backbone.Pagination, App.models.Plan);