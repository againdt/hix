package com.getinsured.hix.tkm.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.tkm.ReportDto;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.tkm.tkmException.TKMException;

/**
 * 
 * @author Sharma_k
 *
 */
public interface TicketListService 
{
	TkmTickets addNewTicket(TkmTickets tkmTickets) throws TKMException;

	TkmTickets updateTicket(String ticNumber, int tickid);
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	TkmTickets findTkmTicketsById(int id) throws TKMException;
	
	/**
	 * 
	 * @param integer 
	 * @return
	 * @throws TKMException 
	 */
	List<String> getDistinctSubject(Integer integer) throws TKMException;
	
	/**
	 * @author Sharma_k
	 * @param ticketId
	 * @param comment
	 * @return
	 * @throws TKMException 
	 */
	TkmTickets saveComments(int ticketId, String comment) throws TKMException;
	
	List<TkmTickets>  getTicketByUser(List<Integer> queueId,Integer userId) throws TKMException;
	
	/**
	 * 
	 * @param ticketId
	 * @param status
	 * @param userId 
	 * @return
	 * @throws TKMException 
	 */
	TkmTickets updateStatus(Integer ticketId, String status, Integer userId, String comments) throws TKMException ;

	/**
	 * 
	 * @param tkmTickets
	 * @return
	 * @throws TKMException 
	 */
	TkmTickets saveEditedTicket(TkmTickets tkmTickets) throws TKMException;

	List<TkmTickets> searchTicketsByCriteria(TkmTicketsRequest ticketRequest) throws TKMException;

	boolean setUpdator(Integer ticketId, Integer updatorId) throws TKMException;

	/**
	 * @author hardas_d
	 * @return List<String>
	 * @return
	 * @throws TKMException 
	 */
	List<String> getDistinctNumbers() throws TKMException;

	/**
	 * @author hardas_d
	 * @return List<String>
	 * @return
	 */
	List<AccountUser> getDistinctRequesters() throws TKMException;
	Map<String, Object> searchTicketsFromTicketAndTask(Map<String, Object> searchCriteria) throws TKMException;
	
	/**
	 * @author kaul_s
	 * @return List<TkmTickets>
	 * @return
	 */
	List<TkmTickets> getTkmTicketsByActivitiTaskId(String activitiTaskId) throws TKMException;
	
	List<ReportDto> getAgeingReport(Map<String, Object> reportCriteria) throws TKMException;
	
	/**
	 * @author yadav_sa
	 * @return List<Object[]> List Of Tkm Ticket Priorities and corresponding count.
	 * @return
	 */
	List<Object[]> getTkmTicketsForPieReport(String type) throws TKMException;

	long getCntByModuleAndRoleId(int moduleId, int roleId);
	
	TkmTickets findByTicketNumber(String ticNum);
	
	void generateCsvForTickets();

	Household getNameCreatedFor(int moduleId) throws TKMException;

	//List<Household> getHouseholdNamesForCreatedFor(String userName) throws TKMException;

	String getCreatedForIndv(String userText);

	String getCreatedForUser(String roleId, String userText);

	List<Household> getHouseholdByuserId(int houseHoldUserId);
	
	TkmTickets findTkmTicketsById(Integer ticketId, boolean statusDisplayFlag) throws TKMException;
	
	boolean archiveTicket(Integer ticketId,Integer archivedBy);

	Map<String, Object> searchTicketsWithNativeQuery(Map<String, Object> searchCriteria);

	List<AccountUser> getRequesters(String userText);	
	
	void saveTicketEvent(Integer ticketId, Integer eventId, String subModuleName);

	TkmTickets findTicketWithEventId(Integer subModuleId);
	
	TkmTickets findTicketWithEventIdAndName(Integer subModuleId, String subModuleName);
}
