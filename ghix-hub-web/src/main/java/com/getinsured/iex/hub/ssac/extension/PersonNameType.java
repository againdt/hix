
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * A data type for a combination of names and/or titles by which a person is known.
 * 
 * <p>Java class for PersonNameType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonNameType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}PersonGivenName" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}PersonMiddleName" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}PersonSurName"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonNameType", propOrder = {
    "personGivenName",
    "personMiddleName",
    "personSurName"
})
public class PersonNameType
    extends ComplexObjectType
{

    @XmlElement(name = "PersonGivenName")
    protected RestrictedGivenNameType personGivenName;
    @XmlElement(name = "PersonMiddleName")
    protected RestrictedMiddleNameType personMiddleName;
    @XmlElement(name = "PersonSurName", required = true)
    protected RestrictedSurNameType personSurName;

    /**
     * Gets the value of the personGivenName property.
     * 
     * @return
     *     possible object is
     *     {@link RestrictedGivenNameType }
     *     
     */
    public RestrictedGivenNameType getPersonGivenName() {
        return personGivenName;
    }

    /**
     * Sets the value of the personGivenName property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestrictedGivenNameType }
     *     
     */
    public void setPersonGivenName(RestrictedGivenNameType value) {
        this.personGivenName = value;
    }

    /**
     * Gets the value of the personMiddleName property.
     * 
     * @return
     *     possible object is
     *     {@link RestrictedMiddleNameType }
     *     
     */
    public RestrictedMiddleNameType getPersonMiddleName() {
        return personMiddleName;
    }

    /**
     * Sets the value of the personMiddleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestrictedMiddleNameType }
     *     
     */
    public void setPersonMiddleName(RestrictedMiddleNameType value) {
        this.personMiddleName = value;
    }

    /**
     * Gets the value of the personSurName property.
     * 
     * @return
     *     possible object is
     *     {@link RestrictedSurNameType }
     *     
     */
    public RestrictedSurNameType getPersonSurName() {
        return personSurName;
    }

    /**
     * Sets the value of the personSurName property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestrictedSurNameType }
     *     
     */
    public void setPersonSurName(RestrictedSurNameType value) {
        this.personSurName = value;
    }

}
