package com.getinsured.ssap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.iex.ssap.model.SsapApplicant;

/**
 * Eligibility program repository.
 *
 * @author Yevgen Golubenko
 * @since 5/8/19
 */
public interface EligibilityProgramRepository extends JpaRepository<EligibilityProgram, Long> {
	/**
	 * Deletes {@link EligibilityProgram} records by given SsapApplicant.
	 *
	 * @param applicant {@link SsapApplicant} object.
	 */
	void deleteBySsapApplicant(final SsapApplicant applicant);

	EligibilityProgram findByEligibilityTypeAndEligibilityIndicatorAndSsapApplicantId(String eligibilityType, String eligibilityIndicator, Long ssapApplicantId);

	List<EligibilityProgram> findBySsapApplicantId(long id);
}
