package com.getinsured.eligibility.lce.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;

import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareApplicationDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.lce.service.LifeChangeEventApplicantEventService;
import com.getinsured.eligibility.lce.service.LifeChangeEventApplicantService;
import com.getinsured.eligibility.lce.service.LifeChangeEventApplicationEventService;
import com.getinsured.eligibility.lce.service.LifeChangeEventApplicationService;
import com.getinsured.eligibility.lce.task.LifeChangeEventTask;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixRestServiceInvoker;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.dto.LifeChangeEventDTO;
import com.getinsured.iex.dto.SsapPreferencesDTO;
import com.getinsured.iex.lce.ChangedApplicant;
import com.getinsured.iex.lce.RequestParamDTO;
import com.getinsured.iex.lce.SepRequestParamDTO;
import com.getinsured.iex.lce.SepResponseParamDTO;
import com.getinsured.iex.lce.SepTransientDTO;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.ssap.service.SsapService;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.LifeChangeEventUtil;
import com.getinsured.iex.util.ReferralUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.thoughtworks.xstream.XStream;
@Controller
@RequestMapping("/iex/lce")
public class LifeChangeEventController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LifeChangeEventController.class);
	
	private final String APPLICATION_DATA_SAVED_SUCCESSFULLY = "Application data saved successfully.";
	private final String ERROR_EXCEPTION_OCCURED_WHILE_SAVING_APPLICATION = "Error : Exception occured while saving Application.";
	private final String MESSAGE = "message";
	private final String FAILURE = "failure";
	private final String STATUS = "status";
	private final String SUCCESS = "success";
	private final String LCE ="LCE";
	private final String REASON ="AddTaxDependentReason : ";
	private final String ADD_DEPENDENT ="ADD_DEPENDENT(S)";
	@Autowired
	private GhixRestServiceInvoker ghixRestServiceInvoker;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	@Autowired
	private LifeChangeEventTask lifeChangeEventTask;
	@Autowired
	private LifeChangeEventApplicationService lifeChangeEventApplicationService;
	@Autowired
	private LifeChangeEventApplicantService lifeChangeEventApplicantService;
	@Autowired
	private LifeChangeEventApplicantEventService lifeChangeEventApplicantEventService;
	@Autowired
	private LifeChangeEventApplicationEventService lifeChangeEventApplicationEventService;
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private SsapService ssapService;
	@Autowired
	private ILocationRepository iLocationRepository;
	@Autowired 
	private Gson platformGson;
	

	@RequestMapping(value = "/savessapapplication", method = RequestMethod.POST)
	@ResponseBody
	public SepResponseParamDTO saveSSAPApplication(@RequestBody SepRequestParamDTO sepRequestParamDTO) throws Exception {

		LOGGER.info("Save SSAP Application : Starts");
		
		String ssapJson = null;
		SsapApplication parentApplication;
		SsapApplication currentApplication = new SsapApplication();
		JsonObject jsonObject = new JsonObject();
		Map<String, Boolean> demographicFlagMap = new HashMap<String, Boolean>();
		Map<Long, List<String>> demographicEventsData = new HashMap<Long, List<String>>();
		List<Boolean> homeAddressChangeDemoFlags = new ArrayList<Boolean>();
		Date demographicEventDate = null;
		Date dobEventDate = null;
		SepResponseParamDTO sepResponseParamDTO = new SepResponseParamDTO();
		sepResponseParamDTO.setIsNameChangedForPrimary(false);
		SepTransientDTO sepTransientDTO = new SepTransientDTO(); 
		try {		
			ssapJson = sepRequestParamDTO.getSsapJSON();
			parentApplication = lifeChangeEventApplicationService.getEnrolledSsapApplicationByMaxIdAndCmrHouseoldId(new BigDecimal(sepRequestParamDTO.getHouseholdId()),sepRequestParamDTO.getCoverageYear());
			if(lifeChangeEventApplicationService.isValidLCEApplication(parentApplication) && !LifeChangeEventUtil.isEmpty(ssapJson)) {
				
				sepTransientDTO.setParentApplication(parentApplication);
				sepTransientDTO.setUserId(sepRequestParamDTO.getUserId());						
				sepTransientDTO.setCurrentApplication(currentApplication);
				List<RequestParamDTO> events = sepRequestParamDTO.getEvents();
				
				updateDemographicMaps(events, demographicFlagMap, demographicEventsData, homeAddressChangeDemoFlags, demographicEventDate, dobEventDate);
				sepTransientDTO.setDemographicEventDate(demographicEventDate);
				sepTransientDTO.setDemographicEventsData(demographicEventsData);
				sepTransientDTO.setDemographicFlagMap(demographicFlagMap);
				sepTransientDTO.setHomeAddressChangeDemoFlags(homeAddressChangeDemoFlags);
				sepTransientDTO.setDobEventDate(dobEventDate);
				sepTransientDTO.setCmrHouseholdId(sepRequestParamDTO.getHouseholdId());
				sepTransientDTO.setEnrolledPersons(invokeEnrollmentApi(parentApplication.getId(), sepRequestParamDTO.getUserName()));
				sepTransientDTO.setUserName(sepRequestParamDTO.getUserName());
				
				currentApplication = lifeChangeEventApplicationService.cloneSsapApplicationFromParent(ssapJson, currentApplication, parentApplication, sepRequestParamDTO, LifeChangeEventConstant.SEP, LifeChangeEventConstant.ON, ApplicationStatus.SIGNED.getApplicationStatusCode(), parentApplication.getCoverageYear());
				List<SsapApplicant> ssapApplicants = lifeChangeEventApplicantService.cloneSsapApplicants(currentApplication, events, sepTransientDTO.getUserId(), sepResponseParamDTO);
				List<SsapApplicationEvent> ssapApplicationEvents = lifeChangeEventApplicationEventService.cloneSsapApplicationEvent(currentApplication, sepTransientDTO.getUserId(), SsapApplicationEventTypeEnum.SEP);
				List<SsapApplicantEvent> ssapApplicantEvents = lifeChangeEventApplicantEventService.cloneSsapApplicantEvents(currentApplication, ssapApplicationEvents.get(0), ssapApplicants, events, sepTransientDTO);
				ssapApplicationEvents.get(0).setSsapApplicantEvents(ssapApplicantEvents);
				currentApplication.setSsapApplicants(ssapApplicants);
				currentApplication.setSsapApplicationEvents(ssapApplicationEvents);
				jsonObject.addProperty(STATUS, SUCCESS);
				jsonObject.addProperty(MESSAGE, APPLICATION_DATA_SAVED_SUCCESSFULLY);
				
				if(currentApplication != null) {
					currentApplication = lifeChangeEventApplicationService.saveSsapSepApplication(currentApplication, true);
					
					List<SsapApplicantEvent> ssapApplicantEventList=lifeChangeEventApplicantEventService.getSsapApplicantEvents(currentApplication.getId(), ADD_DEPENDENT);
					
					String taxDependentReason=sepRequestParamDTO.getAddTaxDependentReason();
					String commenterName=sepRequestParamDTO.getFirstName()+" "+sepRequestParamDTO.getLastName();
					long commenterId=sepRequestParamDTO.getUserId();
					lifeChangeEventApplicantEventService.saveAddTaxDepReason(ssapApplicantEventList,LCE ,REASON + taxDependentReason ,commenterName,commenterId);
					
					sepTransientDTO.setCurrentApplication(currentApplication);
				}
				
				updateMailingAddress(sepRequestParamDTO, ssapApplicants,sepRequestParamDTO.getUserId());
				
				String invokeResult = invokeSsapIntegration(sepTransientDTO, events);
				if(invokeResult.equalsIgnoreCase(LifeChangeEventConstant.SEP_DENIED)){
					jsonObject.addProperty(STATUS, LifeChangeEventConstant.SEP_DENIED);
				}
			}
			else {
				throw new GIException("Not a valid status to file LCE application.");
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occured in Save SSAP Application method", exception);
			jsonObject.addProperty(STATUS, FAILURE);
			jsonObject.addProperty(MESSAGE, ERROR_EXCEPTION_OCCURED_WHILE_SAVING_APPLICATION);
			giWSPayloadRepository.saveAndFlush(LifeChangeEventUtil.createGIWSPayload(exception, ssapJson, LifeChangeEventConstant.ENDPOINT_OPERATION_SAVE));
		} 
		
		 //reset();
		LOGGER.info("Save SSAP Application : Ends");
		sepResponseParamDTO.setJsonObject(jsonObject.toString());
		return sepResponseParamDTO;
	}

	private void updateMailingAddress(SepRequestParamDTO sepRequestParamDTO, List<SsapApplicant> ssapApplicants,Long userId) {
		//update mailing address
		boolean isMailingAddressChangeEvent = false;
		for(RequestParamDTO requestParamDTO :  sepRequestParamDTO.getEvents()){
			if(requestParamDTO.getEventSubCategory().equals(LifeChangeEventConstant.DEMOGRAPHICS_MAILING_ADDRESS_CHANGE)){
				isMailingAddressChangeEvent = true;
				break;
			}
		}
		
		if(isMailingAddressChangeEvent){
		SsapPreferencesDTO ssapPreferencesDTO = new SsapPreferencesDTO();
		ssapPreferencesDTO.setHouseholdId(sepRequestParamDTO.getHouseholdId().intValue());
		ssapPreferencesDTO.setUpdatedBy(userId.intValue());
		ssapPreferencesDTO.setUpdateMailingAddress(true);
		ssapPreferencesDTO.setUpdatePreferences(false);
		for(SsapApplicant ssapApplicant:ssapApplicants){
			if(ssapApplicant.getPersonId() == 1){
				ssapPreferencesDTO.setPrefContactLocation(iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue()));
				break;
			}
		}
		ssapService.updatePreferences(ssapPreferencesDTO);
	}
	}
		
	private void updateDemographicMaps(List<RequestParamDTO> events,
			Map<String, Boolean> demographicFlagMap,
			Map<Long, List<String>> demographicEventsData, List<Boolean> homeAddressChangeDemoFlags, Date demographicEventDate, Date dobEventDate) {
		
		demographicFlagMap.put(LifeChangeEventConstant.IS_DEMOGRAPHIC_CHANGE, false);
		demographicFlagMap.put(LifeChangeEventConstant.IS_DOB_CHANGE, false);
		demographicFlagMap.put(LifeChangeEventConstant.IS_OTHER_CHANGE, false);
		demographicFlagMap.put(LifeChangeEventConstant.IS_ADDRESS_DEMOGRAPHIC_CHANGE, false);
		
		for (RequestParamDTO requestParamDTO : events) {
			for (ChangedApplicant changedApplicant : requestParamDTO.getChangedApplicants()) {
				initDemographicChange(requestParamDTO.getEventCategory(), requestParamDTO.getEventSubCategory(), changedApplicant.getPersonId(), LifeChangeEventUtil.getDate(changedApplicant.getEventDate()), changedApplicant.getIsChangeInZipCodeOrCounty(), demographicFlagMap, demographicEventsData, homeAddressChangeDemoFlags, demographicEventDate, dobEventDate);
			}
		}
		
	}

	private void initDemographicChange(String eventCategory, String eventName, Long personId, Date eventDate, Boolean isChangeInZipCodeOrCounty, Map<String, Boolean> demographicFlagMap, 
			Map<Long, List<String>> demographicEventsData, List<Boolean> homeAddressChangeDemoFlags, Date demographicEventDate, Date dobEventDate){
		
		if(LifeChangeEventConstant.DEMOGRAPHICS.equals(eventCategory)){
			if(LifeChangeEventUtil.isDobEvent(eventName)){
				demographicFlagMap.put(LifeChangeEventConstant.IS_DOB_CHANGE, true);
				dobEventDate = eventDate; 
			} else {
				demographicFlagMap.put(LifeChangeEventConstant.IS_DEMOGRAPHIC_CHANGE, true);
				addDemographicEventData(personId, eventName, demographicEventsData);
				demographicEventDate = eventDate;
			}
		} 
		else if (LifeChangeEventConstant.ADDRESS_CHANGE.equals(eventCategory)) {
			boolean isAddressDemoChange = isAddressChangeDemographicEvent(personId, eventName, isChangeInZipCodeOrCounty);
			homeAddressChangeDemoFlags.add(isAddressDemoChange);
			isAddressDemoChange = computeIsAddressDemoChange(homeAddressChangeDemoFlags);
			if(isAddressDemoChange){
				demographicFlagMap.put(LifeChangeEventConstant.IS_ADDRESS_DEMOGRAPHIC_CHANGE, isAddressDemoChange);
				addDemographicEventData(personId, eventName, demographicEventsData);
				demographicEventDate = eventDate;
			}
		}
		else {
			if(!LifeChangeEventConstant.OTHER.equals(eventName)){
				demographicFlagMap.put(LifeChangeEventConstant.IS_OTHER_CHANGE, true);
			}
		}
	}

	private boolean computeIsAddressDemoChange(List<Boolean> homeAddressChangeDemoFlags) {
		Boolean isAddressDemoChange = true;
		if(homeAddressChangeDemoFlags != null && homeAddressChangeDemoFlags.size() > 0) {
			  for (Boolean homeAddressDemoFlag : homeAddressChangeDemoFlags) {
				if(!homeAddressDemoFlag) {
					isAddressDemoChange = false;	
					break;
				}
			  }
		}
		return isAddressDemoChange;
	}
	
	private boolean isAddressChangeDemographicEvent(Long personId, String name, Boolean isChangeInZipCodeOrCounty) {
		if(LifeChangeEventConstant.ADDRESS_CHANGE_WITHIN_STATE.equals(name)) {
			if(personId != LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID) {
				return true;
			}
			else {
				if(!isChangeInZipCodeOrCounty) {
					return true;
				}
			}
		}
		return false;
	}

	private void addDemographicEventData(Long personId, String eventName, Map<Long, List<String>> demographicEventsData){
		List<String> events = demographicEventsData.get(personId);
		if(null==events){
			events = new ArrayList<>();
		}
		events.add(eventName);
		demographicEventsData.put(personId, events);
	}
	
	public Set<Long> invokeEnrollmentApi(long applicationId, String userName) throws RestClientException, Exception {
		LifeChangeEventDTO lifeChangeEventDTO = new LifeChangeEventDTO();
		lifeChangeEventDTO.setOldApplicationId(applicationId);
		lifeChangeEventDTO.setUserName(userName);
		//String responseDTO = ghixRestServiceInvoker.invokeRestService(lifeChangeEventDTO, SsapEndPoints.LCE_GET_ENROLLED_APPLICANTS, HttpMethod.POST, GIRuntimeException.Component.LCE.toString());
		lifeChangeEventDTO = invokeEnrollmentApi(lifeChangeEventDTO);
		Set<Long> enrolledPersons = lifeChangeEventDTO.getPersonIds();
		if(lifeChangeEventDTO!=null && GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(lifeChangeEventDTO.getStatus())){
			
		}
		return enrolledPersons;
	}
	
	protected LifeChangeEventDTO invokeEnrollmentApi(LifeChangeEventDTO lifeChangeEventDTO) throws Exception {
		XStream xstream = GhixUtils.getXStreamStaxObject();
		List<EnrollmentShopDTO> enrollmentShopDTOs = null;
		EnrolleeResponse enrolleeResponse;
		EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
		enrolleeRequest.setSsapApplicationId(lifeChangeEventDTO.getOldApplicationId());
		ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID, lifeChangeEventDTO.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrolleeRequest));
		enrolleeResponse = (EnrolleeResponse) xstream.fromXML(response.getBody());
		
		if (null != enrolleeResponse && null != enrolleeResponse.getStatus() && enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
			enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
			return extractEnrolledData(enrollmentShopDTOs, executeCompare(lifeChangeEventDTO.getOldApplicationId()), lifeChangeEventDTO);
		} else {
			throw new GIException("Unable to get Enrollment Plan Details. Error Details: " + enrolleeResponse.getErrCode() + ":" + enrolleeResponse.getErrMsg());
		}
	}
	
	private CompareMainDTO executeCompare(long enrolledApplicationId)
			throws Exception {
		CompareMainDTO compareMainDTO = null;

		final SsapApplication enrolledApplication = loadSsapApplicants(enrolledApplicationId);
		compareMainDTO = transformCompareDto(enrolledApplication);

		return compareMainDTO;
	}
	
	private LifeChangeEventDTO extractEnrolledData(List<EnrollmentShopDTO> enrollmentShopDTOs, CompareMainDTO compareMainDTO, LifeChangeEventDTO lifeChangeEventDTO) {
		Set<Long> personIds = new HashSet<>();

		for (EnrollmentShopDTO enrollmentShopDTO : enrollmentShopDTOs) {
			List<EnrolleeShopDTO> enrolleeShopDTO = enrollmentShopDTO.getEnrolleeShopDTOList();
			for (EnrolleeShopDTO enrolleeMembers : enrolleeShopDTO) {
				for (CompareApplicantDTO enrolledApplicant : compareMainDTO.getEnrolledApplication().getApplicants()) {
					if (enrolleeMembers.getExchgIndivIdentifier().equals(enrolledApplicant.getApplicantGuid())) {
						personIds.add(enrolledApplicant.getPersonId());
					}
				}
			}
		}
		
		lifeChangeEventDTO.setPersonIds(personIds);
		
		return lifeChangeEventDTO;
	}
	
	private CompareMainDTO transformCompareDto(SsapApplication enrolledApplication) {
		CompareMainDTO compareMainDTO = new CompareMainDTO();
		compareMainDTO.setEnrolledApplication(transformApplication(enrolledApplication));
		return compareMainDTO;
	}
	
	public CompareApplicationDTO transformApplication(SsapApplication ssapApplication) {
		final CompareApplicationDTO compareApplicationDTO = new CompareApplicationDTO();
		ReferralUtil.copyProperties(ssapApplication, compareApplicationDTO);
		transformApplicants(compareApplicationDTO, ssapApplication);
		return compareApplicationDTO;
	}
	
	private void transformApplicants(CompareApplicationDTO compareApplicationDTO, SsapApplication ssapApplication) {
		CompareApplicantDTO compareApplicantDTO;
		for (SsapApplicant ssapApplicant : ssapApplication.getSsapApplicants()) {
			compareApplicantDTO = new CompareApplicantDTO();
			ReferralUtil.copyProperties(ssapApplicant, compareApplicantDTO);
			compareApplicationDTO.addApplicant(compareApplicantDTO);
		}
	}
	
	private SsapApplication loadSsapApplicants(Long enApp) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(enApp);
	}
	
	private String invokeSsapIntegration(SepTransientDTO sepTransientDTO, List<RequestParamDTO> events) throws Exception {
		return lifeChangeEventTask.process(sepTransientDTO, events, sepTransientDTO.getUserName(), sepTransientDTO.getDemographicFlagMap(), sepTransientDTO.getDemographicEventsData(), sepTransientDTO.getHomeAddressChangeDemoFlags());
	}
	
}