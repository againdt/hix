<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<form name="employeeViewForm" id="employeeViewForm"
	action="<c:url value="/broker/employer/dashboard"/>"
	novalidate="novalidate">
	<input type="hidden" name="switchToModuleName"
		id="switchToModuleName" value="employer" /> <input type="hidden"
		name="switchToModuleId" id="switchToModuleId" value="${employer.id}" />
	<input type="hidden" name="switchToResourceName"
		id="switchToResourceName" value="${employer.name}" />
	
<div id="viewempModal" class="modal hide fade" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">&times;</button>
		<h3 id="myModalLabel">
			<spring:message code="label.agent.employers.ViewEmployerAccount" />
		</h3>
	</div>
	<div class="modal-body">
		<p>
			<spring:message code="label.agent.employers.EmpAccViewconfMsg1" />
			&nbsp;${employer.name}
			<spring:message code="label.agent.employers.EmpAccViewconfMsg2" />
		</p>
		<p>
			<spring:message code="label.agent.employers.ProceedtoEmployerView" />
		</p>
	</div>
	<div class="modal-footer">
		<label class="checkbox span6 pull-left txt-left"> 
		<input type="checkbox" name="showPopupInFuture" />
		<spring:message code="label.agent.employers.DontShowMsgAgain" />
		</label>
		<button class="btn" data-dismiss="modal">
			<spring:message code="label.brkCancel" />
		</button>
		<button class="btn btn-primary" type="submit">
			<spring:message code="label.agent.employers.EmployerView" />
		</button>
	</div>
</div>
</form>