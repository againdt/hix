<%@page import="java.util.ArrayList"%>
<%@page import="antlr.collections.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.getinsured.hix.platform.util.*"   %>
<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 0px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

<style type="text/css">
.accordion-inner {
	padding: 10px 20px 0 20px;
}

.accordion-inner  h3 {
	margin-bottom: 15px;
	color: #ccc;
	border-bottom: solid 1px #e1e3e3;
}

.accordion-inner #sliderEmp,.accordion-inner #sliderDep {
	width: 95%;
	margin: 50px 20px 0px 20px;
}
.dob label {display: inline;}
</style>





<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/general.css" />" media="screen" />
<style>
.table th, .table th a {
	background:#666;
	color:#fff;
}
table th {
	height:22px;
}
</style>

<script type="text/javascript" src="<:url value="/resources/js/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/parsley.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/highlight.min.js" />"></script>
<form class="form-vertical" id="frmviewflow" name="frmviewflow" action="<c:url value="/admin/affiliate/viewflow" />" method="POST">

<div class="row-fluid">
	<div class="gutter10">
        <div id="skip" class="hide"></div>
        <ul class="page-breadcrumb">
            <li><a href="/hix/admin/affiliate/manage">&lt;  Back</a></li>
            <li><a href="/hix/admin/affiliate/manage">Manage Affiliate</a></li>
            <li><a href="/hix/admin/affiliate/viewaffiliate/${affiliate.affiliateId}">View Affiliate</a></li>
            <li><a href="#">View Flow</a></li>
        </ul><!--page-breadcrumb ends-->
        <h1>View Flow Information</h1>
	</div>
</div>

<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
</div>
<div class="gutter10">
	<div class="row-fluid"></div>

	<table class="table table-condensed table-border-none">
		<tbody>
			<tr>
				<td class="txt-right">Flow Name</td>
				<td><strong>${affiliateflow.flowName}</strong></td>
				
				<td class="txt-right">Status</td>
				<td><strong>${affiliateflow.flowStatus}</strong></td>
				
				<td class="txt-right ">Custom Field 1</td>
				<td><strong>${affiliateflow.customField1}</strong></td>
			</tr>
			<tr>
				
				<td class="txt-right ">Type of Traffic</td>
				<td><strong>${affiliateflow.trafficType}</strong></td>
				
				<td class="txt-right ">IVR Number</td>
				<td><strong>${affiliateflow.ivrNumber}</strong></td>
				
				<td class="txt-right ">Custom Field 2</td>
				<td><strong>${affiliateflow.customField2}</strong></td>
			</tr>
			<tr>
				
				
				<td class="txt-right ">Web Pricing Model</td>
				<td><strong>${affiliateflow.webPricingModel}</strong></td>
				
				<td class="txt-right ">Web Payout</td>
				<td>
				<c:if test="${affiliateflow.webPayoutType == 'FIXED' }">
	            <strong>  $ ${affiliateflow.webPayoutvalue}</strong>
	            </c:if>
	            <c:if test="${affiliateflow.webPayoutType == 'PERCENTAGE' }">
	            <strong>${affiliateflow.webPayoutvalue} % </strong>
	             </c:if>
				</td>
				
				<td class="txt-right ">Custom Field 3</td>
				<td><strong>${affiliateflow.customField3}</strong></td>
				
			</tr>
			<tr>
				
				<td class="txt-right ">Call Pricing Model</td>
				<td><strong>${affiliateflow.callPricingModel}</strong></td>
				
				<td class="txt-right ">Call Payout</td>
				<td>
				<c:if test="${affiliateflow.callPayoutType == 'FIXED_PAYOUT' }">
	              <strong>$ ${affiliateflow.callPayoutvalue}</strong>
	            </c:if>
	            <c:if test="${affiliateflow.callPayoutType == 'PERCENTAGE' }">
	               <strong>${affiliateflow.callPayoutvalue} %</strong>
	            </c:if>
				
				</td>
				
				<td class="txt-right ">Custom Field 4</td>
				<td><strong>${affiliateflow.customField4}</strong></td>
				
			</tr>
				<tr>
				
				
				<td class="txt-right aff-greetings">Inbound Greetings</td>
				<td class="viewTD"><strong>${affiliateflow.inboundGreeting}</strong></td>
				
				<td class="txt-right aff-greetings">Business Comments</td>
				<td class="viewTD"><strong>${affiliateflow.businessTermsConditons}</strong></td>
				
				<td class="txt-right aff-greetings">Outbound Greetings</td>
				<td class="viewTD"><strong>${affiliateflow.outboundGreeting}</strong></td>
				
			</tr>
		</tbody>
	</table>
	<!--row-fluid-->
</div>
</form>
<!--gutter10-->
<script type="text/javascript">

	   $('#frmviewflow').parsley( 'addListener', {
			onFormSubmit: function (isFormValid, event, ParsleyForm) {
				// if field is not visible, do not apply Parsley validation!
				if(isFormValid === "true"){
					var url='<c:url value="/admin/affiliate/viewflow" />';
					 $.post(url,function() {
						location.href='<c:url value="/admin/affiliate/viewflow" />'
					 });
				}
			} 
		}); 
		

</script>

    