package com.getinsured.hix.entity.web.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.broker.service.jpa.ConsumerComposite;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.entity.AssisterExportDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.dto.entity.AssisterSearchParameters;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityResponseDTO;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.entity.service.EntityService;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.AssisterLanguages;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.config.AEEConfiguration;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.iex.household.service.PreferencesService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import com.thoughtworks.xstream.XStream;

@Controller
public class AssisterAdminController {
	private static final String ASSISTER_CERTIFICATION_DATE = "assisterCertificationDate";
	private static final String UPLOADED_DOCUMENT_CHECK = "uploadedDocumentCheck";
	private static final String HAS_PERMISSION_MODEL_EEADMIN_MANAGE_ASSISTER = "hasPermission(#model, 'EEADMIN_MANAGE_ASSISTER')";
	private static final String HAS_PERMISSION_MODEL_EEADMIN_EDIT_ASSISTER = "hasPermission(#model, 'EEADMIN_EDIT_ASSISTER')";
	private static final String SORT_BY_COL = "sortByCol";
	private static final String DESC = "DESC";
	private static final String FALSE = "false";
	private static final String TRUE = "true";
	private static final String CHANGE_ORDER = "changeOrder";
	private static final String ASC = "ASC";
	private static final String SORT_ORDER = "sortOrder";
	private static final String FIRST_NAME = "FIRST_NAME";
	private static final String SORT_BY = "sortBy";
	private static final String ASSISTER_ID = "assisterId";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AssisterAdminController.class);
	private static final String PAGE_TITLE = "page_title";
	private static final String ASSISTER_LIST = "assisterList";
	private static final String ASSISTER = "assister";
	private static final String ASSISTER_LANGUAGES = "assisterLanguages";
	private static final String STATUS_HISTORY = "assisterRegisterStatusHistory";
	private static final String LIST_OF_STATES = "statelist";
	private static final String SHOW_POSTAL_MAIL="showPostalMailOption";
	private static final String RESULT_SIZE = "resultSize";
	private static final int PAGE_SIZE = 10;
	private static final String RETRIEVING_ASSISTER_DETAIL_REST_CALL_RESPONSE = "Retrieving Assister Detail REST Call Ends ";
	private static final String RETRIEVING_ASSISTER_DOCUMENT_DETAILS_REST_CALL_STARTS = "Retrieving Assister Document Details REST Call Starts";
	private static final String RETRIEVING_ASSISTER_DOCUMENT_DETAILS_REST_CALL_ENDS = "Retrieving Assister Document Details REST Call Ends";
	private static final String EDIT_ASSISTER_INFO = "Edit Assister Information";
	private static final int ZERO = 0;
	private static final int THREE = 3;
	private static final int SIX = 6;
	private static final int TEN = 10;
	private static final String ASSISTER_MENU = "assisterMenu";
	private static final String MENU = "menu";
	private static final String LIST_OF_EDUCATIONS = "educationlist";
	private static final String ENROLLMENTENTITY = "enrollmentEntity";
	private static final String LIST_OF_PRIMARY_SITES = "primarySitelist";
	private static final String LIST_OF_SECONDARY_SITES = "secondarySitelist";
	private static final String IS_CERTIFICATION_CERTIFIED = "isCertified";
	private static final String LANGUAGE_NAMES = "languageNames";
	private static final String OTHER_SPOKEN_LANGUAGE = "otherSpokenLanguage";
	private static final String OTHER_WRITTEN_LANGUAGE = "otherWrittenLanguage";
	private static final String FILE_INPUT = "fileInput";
	private static final String TOTAL_ASSISTERS_BY_SEARCH = "totalassistersbysearch";
	private static final String TOTAL_ASSISTERS = "totalassisters";
	private static final String ASSISTERS_UP_FOR_RENEWAL = "assistersupforrenewal";
	private static final String INACTIVE_ASSISTERS = "inactiveassisters";
	private static final String PAGE_NUMBER = "pageNumber";
	private static final String LANGUAGE_WRITTEN_NAMES = "languageWrittenNames";
	private static final String ENTITY_LANGUAGE_SPOKEN = "ENTITY_LANGUAGE_SPOKEN";
	private static final String ENTITY_LANGUAGE_WRITTEN = "ENTITY_LANGUAGE_WRITTEN";
	private static final String LIST_OF_LANGUAGES_SPOKEN = "languageSpokenList";
	private static final String LIST_OF_LANGUAGES_WRITTEN = "languageWrittenList";
	private static final String NAVIGATION_MENU_MAP = "navigationmenumap";
	private static final String ASSISTER_CERTIFICATION_STATUS = "ASSISTER_CERTIFICATION_STATUS";
	private static final String STATUSLIST = "statusList";
	private static final String MANAGE_ASSISTER = "Manage Assister";
	private static final String RETRIVINGASSISTERDETAILRESTCALLSTARTS = "Retrieving Assister Detail REST Call Starts : ";
	private static final String FILENAME = "fileName";
	private static final String LANGUAGES_LIST = "languagesList";
	private static final long UPLOAD_DOCUMENT_ALLOWED_SIZE = 5242880;
	private static String lastVisitedURL = "entity/assisteradmin/certificationstatus";
	//private static final String ASSISTER_CASE_DETAILS = "redirect:/" + LAST_VISITED_URL + "?assisterId=";
	private static final String ACTIVATION_RESULT = "&activationLinkResult=";
	private static final String ACTIVITY_STATUS_HISTORY = "assisterActivityStatusHistory";
	private static final String HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER = "hasPermission(#model, 'ASSISTER_EDIT_ASSISTER')";
	public static final String EXCEPTION_OCCURRED = "Exception occured.";
	public static final String ENTITY_ID = "entityId";
	private static final String CURRDATE = "currDate";
	private AccountUser user;
	private static final String NOTIFICATION_TEMPLATE_NAME = "IndividualDedesignationTemplateEnrollmentCounselor";
	private static final String EXCHANGE_NAME = "exchangeName";
	private static final String EXCHANGE_FULL_NAME = "exchangeFullName";
	private static final String EXCHANGE_ADDRESS_LINE_ONE = "exchangeAddressLineOne";
	private static final String STATE_NAME = "stateName";
	private static final String COUNTRY_NAME = "countryName";
	private static final String EXCHANGE_URL = "exchangeURL";
	private static final String EXCHANGE_PHONE = "exchangePhone";
	private static final String EXCHANGE_ADDRESS_ONE = "exchangeAddress1";
	private static final String EXCHANGE_CITY_NAME = "cityName";
	private static final String EXCHANGE_PIN_CODE = "pinCode";
	private static final String ENROLLMENTENTITY_NAME = "EntityName";
	private static final String NOTIFICATION_SEND_DATE = "notificationDate";
	public static final String PRIMARYSITE = "PRIMARY";
	private static final String ASSISTER_NAME = "counselorName";
	public static final String REASON_FOR_DE_DESIGNATION = "ReasonForDeDesignation";
	private static final String SEARCH_CRITERIA = "searchCriteria";
	private static final String INDIVIDUAL_NOTIFICATION = "individualnotification_";
	private static final String INDIVIDUAL_MODULE = "individual";
	private static final String PDF = ".pdf";
	private static final String ECMRELATIVEPATH = "brokercertnotifications";
	private static final int TWO = 2;
	
	@Autowired private Gson platformGson;
	@Autowired
	private EERestCallInvoker restClassCommunicator;
	@Autowired
	private LookupService lookupService;
	@Autowired
	private UserService userService;

	@Autowired
	private EntityService entityService;
	
	@Autowired 
	private AccountActivationService accountActivationService;
	
	@Autowired 
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

	@Autowired
	private BrokerMgmtUtils brokerMgmtUtils;
	
	@Autowired
	private NoticeService noticeService;
	
	@Autowired
	private SmartValidator validator;
	
	@Autowired
	PreferencesService preferencesService;
	
	/**
	 * Get Method to retrieve the list of all the assisters on manage assisters
	 * page. Parameter Model model Parameter request returns associated page
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/entity/assisteradmin/manageassister", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_MANAGE_ASSISTER)
	public String manageAssister(Model model, HttpServletRequest request) throws GIRuntimeException {

		LOGGER.info("manageAssister: START");

		LOGGER.info("Inside Manage Assister");
		Assister assister = new Assister();
		try {
			if(!isEntityAdmin()) {
				return "redirect:/entity/entityadmin/displayassisters"; 
			}
			
			Integer pageSize = PAGE_SIZE;
			String requestPageSize = request.getParameter("pageSize");
			if(!StringUtils.isBlank(requestPageSize)){
				pageSize = Integer.parseInt(requestPageSize);
			}
			request.setAttribute("pageSize", pageSize);
			request.setAttribute("reqURI", "list");
			String param = request.getParameter(PAGE_NUMBER);
			Integer startRecord = 0;
			int currentPage = 1;
			if (param != null) {
				startRecord = (Integer.parseInt(param) - 1) * pageSize;
				currentPage = Integer.parseInt(param);
			}
			model.addAttribute(PAGE_TITLE,
					"Getinsured Health Exchange: Manage Assister");
			AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setPageSize(pageSize);
			assisterRequestDTO.setStartRecord(startRecord);
			assisterRequestDTO.setAssister(assister);
			assisterRequestDTO.setAdmin(true);
			String sortBy =  (request.getParameter(SORT_BY) == null) ?  FIRST_NAME : request.getParameter(SORT_BY);
			String sortOrder = determineSortOrderForCriteria(request);

			request.removeAttribute(CHANGE_ORDER);
			
			model.addAttribute(SORT_ORDER, sortOrder);
			model.addAttribute(SORT_BY_COL, sortBy);
			
			assisterRequestDTO.setSortBy(sortBy);
			assisterRequestDTO.setSortOrder(sortOrder);
			
			AssisterResponseDTO assisterResponseDTO = retrieveSearchAssisterList(assisterRequestDTO);
		
			populateAssisterCertificationsStatuses(model);
			
			model.addAttribute(ASSISTER_LIST,
					assisterResponseDTO.getListOfAssisters());
			if (!assisterResponseDTO.getListOfAssisters().isEmpty()) {
				model.addAttribute(RESULT_SIZE, assisterResponseDTO
						.getListOfAssisters().size());
			} else {
				model.addAttribute(RESULT_SIZE, 0);
			}
			model.addAttribute("currentPage", currentPage);
			request.getSession().setAttribute("assistersearchparam", assister);
			request.getSession().removeAttribute(MENU);
			request.getSession().setAttribute(MENU, ASSISTER_MENU);

			model.addAttribute(TOTAL_ASSISTERS,
					assisterResponseDTO.getTotalAssisters());

			model.addAttribute(ASSISTERS_UP_FOR_RENEWAL,
					assisterResponseDTO.getAssistersUpForRenewal());
			model.addAttribute(INACTIVE_ASSISTERS,
					assisterResponseDTO.getInactiveAssisters());

			model.addAttribute(TOTAL_ASSISTERS_BY_SEARCH,
					assisterResponseDTO.getTotalAssistersBySearch());
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			model.addAttribute(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retreiving list of assisters : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retreiving list of all assisters: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("manageAssister: END");

		return "entity/assisteradmin/manageassister";
	}

	/**
	 * Method to populate Assister Certification statuses.
	 * 
	 * @param model The model instance.
	 */
	private void populateAssisterCertificationsStatuses(Model model) {
		List<LookupValue> statusValuelist = lookupService.getLookupValueList(ASSISTER_CERTIFICATION_STATUS);
		List<String> statuslist = new ArrayList<String>();

		for (LookupValue val : statusValuelist) {
			statuslist.add(val.getLookupValueLabel());
			model.addAttribute(STATUSLIST, statuslist);
		}
	}

	/**
	 * @param assisterRequestDTO
	 * @return
	 * @throws IOException 
	 */
	private AssisterResponseDTO retrieveSearchAssisterList( AssisterRequestDTO assisterRequestDTO) throws IOException {
		
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class);

		String requestJSON = writer.writeValueAsString(assisterRequestDTO);

		String response = restClassCommunicator.retrieveSearchAssisterList(requestJSON);
		
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class);
		AssisterResponseDTO assisterResponseDTO = reader.readValue(response );
		
		return assisterResponseDTO;
	}

	/**
	 * Handles the reset all functionality
	 * 
	 * @param result
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate Manage list page
	 */
	@RequestMapping(value = "/entity/assisteradmin/resetall", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_MANAGE_ASSISTER)
	public String resetAll(Model model) throws GIRuntimeException{

		LOGGER.info("resetAll: START");

		try {
			model.addAttribute("assisterSearchParameters",
					new AssisterSearchParameters());
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while reseting Assister Search Parameters : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while reseting : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("resetAll: END");

		return "redirect:/entity/assisteradmin/manageassister";
	}
	
	/**
	 * Method 'clearUploadComponents' to clear intermediate upload data.
	 * 
	 * @param request
	 *            The HttpServletRequest instance.
	 * @return String The status of operation.
	 */
	/*@RequestMapping(value = "/entity/assisteradmin/resetuploads", method = RequestMethod.POST)
	@ResponseBody
	private String clearUploadComponents(HttpServletRequest request) {
		String STATUS_SUCCESS = "success";
		String STATUS_FAILURE = "failure";

		try {
			this.clearSessionAttributes(request);
		} catch (Exception ex) {
			LOGGER.error("Exception occurred while clearing upload components. Exception:" + ex.getMessage(), ex);
			return STATUS_FAILURE;
		}

		return STATUS_SUCCESS;
	}*/
	
	/**
	 * Method 'clearSessionAttributes' to clear session attributes .
	 * 
	 * @param request
	 *            The HttpServletRequest instance.
	 * @return request The HttpServletRequest instance.
	 */
	/*private void clearSessionAttributes(HttpServletRequest request) {
		if (null != request.getSession()) {
			request.getSession().removeAttribute(FILENAME);
		}
	}*/

	/**
	 * Get Method and Post Method to retrieve the list of assisters based on
	 * Search Criteria Parameter Model model Parameter request returns
	 * associated page
	 * 
	 * @param assister
	 * @param enrollmentEntity
	 * @param request
	 * @param model
	 * @param pFirstName
	 * @param pLastName
	 * @param pEntityName
	 * @param pFromDate
	 * @param pToDate
	 * @param pCertificationStatus
	 * @param pStatusActive
	 * @param pStatusInactive
	 * @return
	 * @throws GIRuntimeException 
	 */
	@RequestMapping(value = "/entity/assisteradmin/manageassister/listforpagination", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_MANAGE_ASSISTER)
	public String assisterList(
			HttpServletRequest request,
			Model model,
			@RequestParam(value = "pFirstName", required = false) String pFirstName,
			@RequestParam(value = "pLastName", required = false) String pLastName,
			@RequestParam(value = "pAssisterNumber", required = false) String pAssisterNumber,
			@RequestParam(value = "pEntityName", required = false) String pEntityName,
			@RequestParam(value = "pFromDate", required = false) String pFromDate,
			@RequestParam(value = "pToDate", required = false) String pToDate,
			@RequestParam(value = "pCertificationStatus", required = false) String pCertificationStatus,
			@RequestParam(value = "pStatusActive", required = false) String pStatusActive,
			@RequestParam(value = "pStatusInactive", required = false) String pStatusInactive) throws GIRuntimeException {

		LOGGER.info("assisterList: START");

		try {
		LOGGER.info(MANAGE_ASSISTER);
		
		if(!isEntityAdmin()) {
			return "redirect:/entity/entityadmin/displayassisters"; 
		}

		model.addAttribute(PAGE_TITLE,
				"Getinsured Health Exchange: Manage Assister");
		Integer pageSize = PAGE_SIZE;
		String requestPageSize = request.getParameter("pageSize");
		if(!StringUtils.isBlank(requestPageSize)){
			pageSize = Integer.parseInt(requestPageSize);
		}
		request.setAttribute("pageSize", pageSize);
		request.setAttribute("reqURI", "list");
		String param = request.getParameter(PAGE_NUMBER);
		Integer startRecord = 0;
		int currentPage = 1;
		List<Assister> listOfAssisters = new ArrayList<Assister>();
		String sortBy = null;
		String sortOrder = null;

		if (param != null) {
			startRecord = (Integer.parseInt(param) - 1) * pageSize;
			currentPage = Integer.parseInt(param);
			model.addAttribute(PAGE_NUMBER, param);
		} else {
			model.addAttribute(PAGE_NUMBER, 1);
		}

		
			Assister assister = populateAssisster(pFirstName, pLastName, pCertificationStatus);

			AssisterRequestDTO assisterRequestDTO = populateAssisterRequestDTO(
					pFirstName, pLastName, pAssisterNumber, pEntityName, pFromDate, pToDate,
					pCertificationStatus, pStatusActive, pStatusInactive,
					pageSize, startRecord, assister);
			
			sortBy = request.getParameter(SORT_BY);
			
			if(!EntityUtils.isEmpty(sortBy)){
				sortOrder =  determineSortOrderForCriteria(request);	

				request.removeAttribute(CHANGE_ORDER);
			} else {
				sortBy = request.getParameter(SORT_BY_COL);
				sortOrder = request.getParameter("sortOrderCol");
			}
			
			assisterRequestDTO.setSortBy(sortBy);
			assisterRequestDTO.setSortOrder(sortOrder);
			assisterRequestDTO.setAdmin(true);
			AssisterResponseDTO assisterResponseDTO = retrieveSearchAssisterList(assisterRequestDTO);
			listOfAssisters = assisterResponseDTO.getListOfAssisters();

			// listOfAssisters.addAll(listOfAssistersbyEntity);
			model.addAttribute(ASSISTER_LIST, listOfAssisters);
			
			populateAssisterCertificationsStatuses(model);

			populateModelSessionWithAssisterCount(request, model, assisterResponseDTO);

			request.getSession().setAttribute("menu", ASSISTER_MENU);
			
			populateModelForAssisterList(model, currentPage, sortBy, sortOrder, assister, assisterResponseDTO);
			
			AssisterSearchParameters assisterSearchParameters = setAssisterSearchParameters(
					pFirstName, pLastName, pAssisterNumber, pEntityName, pCertificationStatus,
					pFromDate, pToDate, pStatusActive, pStatusInactive);
			model.addAttribute("assisterSearchParameters",
					assisterSearchParameters);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			model.addAttribute(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		} catch(InvalidUserException invalidUserException)
		{
			LOGGER.error("InvalidUserException occured while retrieving the list of assisters based on  Search Criteria  : ", invalidUserException);
			throw new GIRuntimeException(invalidUserException, ExceptionUtils.getFullStackTrace(invalidUserException), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving list of assisters based on  Search Criteria : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving the list of assisters based on  Search Criteria  : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("assisterList: END");

		return "entity/assisteradmin/manageassister";
	}

	/**
	 * @param pFirstName
	 * @param pLastName
	 * @param pCertificationStatus
	 * @return
	 */
	private Assister populateAssisster(String pFirstName, String pLastName,
			String pCertificationStatus) {
		Assister assister = new Assister();
		assister.setFirstName(pFirstName);
		assister.setLastName(pLastName);
		assister.setCertificationStatus(pCertificationStatus);
		return assister;
	}

	/**
	 * @param request
	 * @param model
	 * @param assisterResponseDTO
	 */
	private void populateModelSessionWithAssisterCount(
			HttpServletRequest request, Model model,
			AssisterResponseDTO assisterResponseDTO) {
		if (!assisterResponseDTO.getListOfAssisters().isEmpty()) {
			model.addAttribute(RESULT_SIZE, assisterResponseDTO
					.getListOfAssisters().size());
			request.getSession().setAttribute(RESULT_SIZE,
					assisterResponseDTO.getListOfAssisters().size());
		} else {
			model.addAttribute(RESULT_SIZE, 0);
			request.getSession().setAttribute(RESULT_SIZE, 0);
		}
	}

	/**
	 * @param model
	 * @param currentPage
	 * @param sortBy
	 * @param sortOrder
	 * @param assister
	 * @param assisterResponseDTO
	 */
	private void populateModelForAssisterList(Model model, int currentPage,
			String sortBy, String sortOrder, Assister assister,
			AssisterResponseDTO assisterResponseDTO) {
		model.addAttribute("currentPage", currentPage);

		model.addAttribute(ASSISTER, assister);

		model.addAttribute(TOTAL_ASSISTERS,
				assisterResponseDTO.getTotalAssisters());

		model.addAttribute(ASSISTERS_UP_FOR_RENEWAL,
				assisterResponseDTO.getAssistersUpForRenewal());
		model.addAttribute(INACTIVE_ASSISTERS,
				assisterResponseDTO.getInactiveAssisters());

		model.addAttribute(TOTAL_ASSISTERS_BY_SEARCH,
				assisterResponseDTO.getTotalAssistersBySearch());
		
		model.addAttribute(SORT_ORDER, sortOrder);
		model.addAttribute(SORT_BY_COL, sortBy);
	}

	/**
	 * @param pFirstName
	 * @param pLastName
	 * @param pEntityName
	 * @param pFromDate
	 * @param pToDate
	 * @param pCertificationStatus
	 * @param pStatusActive
	 * @param pStatusInactive
	 * @param pageSize
	 * @param startRecord
	 * @param assister
	 * @return
	 */
	private AssisterRequestDTO populateAssisterRequestDTO(String pFirstName,
			String pLastName, String pAssisterNumber, String pEntityName, String pFromDate,
			String pToDate, String pCertificationStatus, String pStatusActive,
			String pStatusInactive, Integer pageSize, Integer startRecord,
			Assister assister) {
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setAssister(assister);
		assisterRequestDTO.setPageSize(pageSize);
		assisterRequestDTO.setStartRecord(startRecord);
		assisterRequestDTO.setFirstName(pFirstName);
		assisterRequestDTO.setLastName(pLastName);
		assisterRequestDTO.setAssisterNumber(pAssisterNumber);
		assisterRequestDTO.setEntityName(pEntityName);
		assisterRequestDTO.setCertificationStatus(pCertificationStatus);
		assisterRequestDTO.setToDate(pToDate);
		assisterRequestDTO.setFromDate(pFromDate);
		assisterRequestDTO.setStatusactive(pStatusActive);
		assisterRequestDTO.setStatusInactive(pStatusInactive);
		return assisterRequestDTO;
	}

	/**
	 * Get Method and Post Method to retrieve the list of assisters based on
	 * Search Criteria Parameter Model model Parameter request returns
	 * associated page
	 * 
	 * @param assister
	 * @param enrollmentEntity
	 * @param request
	 * @param model
	 * @param firstName
	 * @param lastName
	 * @param entityName
	 * @param certificationStatus
	 * @param statusActive
	 * @param fromDate
	 * @param toDate
	 * @param statusActive
	 * @param statusInactive
	 * @return
	 * @throws InvalidUserException 
	 */
	@RequestMapping(value = "/entity/assisteradmin/manageassister/list", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_MANAGE_ASSISTER)
	public String assisterListForPagination(
			@ModelAttribute("assister") Assister assister,
			HttpServletRequest request,	Model model,
			@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "assisterNumber", required = false) String assisterNumber, 
			@RequestParam(value = "entityName", required = false) String entityName,
			@RequestParam(value = "fromDate", required = false) String fromDate,
			@RequestParam(value = "toDate", required = false) String toDate,
			@RequestParam(value = "certificationStatus", required = false) String certificationStatus,
			@RequestParam(value = "statusActive", required = false) String statusActive,
			@RequestParam(value = "statusInActive", required = false) String statusInActive) throws GIRuntimeException {

		LOGGER.info("assisterListForPagination: START");

		try{
		LOGGER.info(MANAGE_ASSISTER);
		
		if(!isEntityAdmin()) {
			return "redirect:/entity/entityadmin/displayassisters"; 
		}

		model.addAttribute(PAGE_TITLE,
				"Getinsured Health Exchange: Manage Assister");
		Integer pageSize = PAGE_SIZE;
		String requestPageSize = request.getParameter("pageSize");
		if(!StringUtils.isBlank(requestPageSize)){
			pageSize = Integer.parseInt(requestPageSize);
		}
		request.setAttribute("pageSize", pageSize);
		request.setAttribute("reqURI", "list");
		String param = request.getParameter(PAGE_NUMBER);
				
		String sortBy =  (request.getParameter(SORT_BY) == null) ?  FIRST_NAME : request.getParameter(SORT_BY);
		String sortOrder = determineSortOrderForCriteria(request);

		request.removeAttribute(CHANGE_ORDER);
		
		model.addAttribute(SORT_ORDER,sortOrder);
		
		Integer startRecord = 0;
		int currentPage = 1;
		List<Assister> listOfAssisters = new ArrayList<Assister>();

		if (param != null) {
			startRecord = (Integer.parseInt(param) - 1) * pageSize;
			currentPage = Integer.parseInt(param);
		}

		
			AssisterRequestDTO assisterRequestDTO = populateAssisterRequestDTOForAssisterListForPagination(
					assister, firstName, lastName, assisterNumber, entityName, fromDate,
					toDate, certificationStatus, statusActive, statusInActive,
					pageSize, sortBy, sortOrder, startRecord);

			AssisterResponseDTO assisterResponseDTO = retrieveSearchAssisterList(assisterRequestDTO);
			listOfAssisters = assisterResponseDTO.getListOfAssisters();

			// listOfAssisters.addAll(listOfAssistersbyEntity);
			model.addAttribute(ASSISTER_LIST, listOfAssisters);
			
			populateAssisterCertificationsStatuses(model);

			populateModelSessionWithAssisterCount(request, model,
					assisterResponseDTO);
			model.addAttribute("currentPage", currentPage);
			model.addAttribute(ASSISTER, assister);
			request.getSession().setAttribute("menu", ASSISTER_MENU);

			model.addAttribute(TOTAL_ASSISTERS,
					assisterResponseDTO.getTotalAssisters());

			model.addAttribute(ASSISTERS_UP_FOR_RENEWAL,
					assisterResponseDTO.getAssistersUpForRenewal());
			model.addAttribute(INACTIVE_ASSISTERS,
					assisterResponseDTO.getInactiveAssisters());

			model.addAttribute(TOTAL_ASSISTERS_BY_SEARCH,
					assisterResponseDTO.getTotalAssistersBySearch());

			AssisterSearchParameters assisterSearchParameters = setAssisterSearchParameters(
					firstName, lastName, assisterNumber, entityName, certificationStatus,
					fromDate, toDate, statusActive, statusInActive);
			model.addAttribute("assisterSearchParameters",
					assisterSearchParameters);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			model.addAttribute(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		} catch(InvalidUserException invalidUserException){
			LOGGER.error("InvalidUserException occured while retrieving the list of assisters based on search criteria for pagination : ", invalidUserException);
			throw new GIRuntimeException(invalidUserException, ExceptionUtils.getFullStackTrace(invalidUserException), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving the list of assisters based on search criteria for pagination : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving list of assisters based on search criteria for pagination : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("assisterListForPagination: END");

		return "entity/assisteradmin/manageassister";
	}

	/**
	 * Method to populate AssisterRequestDTO For AssisterListForPagination operation.
	 * 
	 * @param assister The Assister instance.
	 * @param firstName The first name.
	 * @param lastName The last name.
	 * @param entityName The entity name.
	 * @param fromDate The from date.
	 * @param toDate The to date.
	 * @param certificationStatus The current certification status.
	 * @param statusActive The status active value.
	 * @param statusInActive The status inactive value.
	 * @param pageSize The page size.
	 * @param sortBy The sort by value.
	 * @param sortOrder The sort order value.
	 * @param startRecord The start record value.
	 * 
	 * @return assisterRequestDTO The updated AssisterRequestDTO instance
	 */
	private AssisterRequestDTO populateAssisterRequestDTOForAssisterListForPagination(
			Assister assister, String firstName, String lastName, String assisterNumber,
			String entityName, String fromDate, String toDate,
			String certificationStatus, String statusActive,
			String statusInActive, Integer pageSize, String sortBy,
			String sortOrder, Integer startRecord) {
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setSortBy(sortBy);
		assisterRequestDTO.setSortOrder(sortOrder);
		assisterRequestDTO.setAssister(assister);
		assisterRequestDTO.setPageSize(pageSize);
		assisterRequestDTO.setStartRecord(startRecord);
		assisterRequestDTO.setFirstName(firstName);
		assisterRequestDTO.setLastName(lastName);
		assisterRequestDTO.setAssisterNumber(assisterNumber); 
		assisterRequestDTO.setEntityName(entityName);
		assisterRequestDTO.setCertificationStatus(certificationStatus);
		assisterRequestDTO.setToDate(toDate);
		assisterRequestDTO.setFromDate(fromDate);
		assisterRequestDTO.setStatusactive(statusActive);
		assisterRequestDTO.setStatusInactive(statusInActive);
		assisterRequestDTO.setAdmin(true);
		return assisterRequestDTO;
	}

	/**
	 * Method to determine the Sort Order for Criteria. 
	 * 
	 * @param request The HttpServletRequest instance.
	 * @return sortOrder The sort order value.
	 */
	private String determineSortOrderForCriteria(HttpServletRequest request) {
		String sortOrder =  (request.getParameter(SORT_ORDER) == null) ?  ASC : request.getParameter(SORT_ORDER);
		sortOrder = ("".equals(sortOrder)) ? ASC : sortOrder;
		String changeOrder =  (request.getParameter(CHANGE_ORDER) == null) ?  FALSE : request.getParameter(CHANGE_ORDER);
		sortOrder = (changeOrder.equals(TRUE) ) ? ((sortOrder.equals(DESC)) ? ASC : DESC ): sortOrder;	
		return sortOrder;
	}

	@RequestMapping(value = "/entity/assisteradmin/certificationstatus", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_MANAGE_ASSISTER)
	public String getCertificationStatus(Model model,
			HttpServletRequest request,
			@RequestParam(ASSISTER_ID) String encryAssisterId) throws GIRuntimeException {

		LOGGER.info("getCertificationStatus: START");
		try {
		String assisterId = encryAssisterId !=null && !"".equals(encryAssisterId)?ghixJasyptEncrytorUtil.decryptStringByJasypt(encryAssisterId):null;
		
			Assister assister = null;
			user = userService.getLoggedInUser();
			if (assisterId != null && !EntityUtils.isEmpty(assisterId)) {
				AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
				assisterRequestDTO.setId(Integer.valueOf(assisterId));
				
				AssisterResponseDTO assisterResponseDTO = getAssisterDetail(assisterRequestDTO);
				
				Validate.notNull(assisterResponseDTO);
				 
				EntityUtils.checkEntityAssisterAssociation(assisterId, user, assisterResponseDTO);
				assister = assisterResponseDTO.getAssister();
				
				model.addAttribute(ASSISTER, assister);

				List<Map<String, Object>> assisterCertStatusHistory = null;
				if (null != assister) {
					AssisterRequestDTO enrollmentEntityRequestDTO = new AssisterRequestDTO();
					enrollmentEntityRequestDTO.setModuleId(assister.getId());
					LOGGER.info("Retrieving Enrollment Entity History Detail REST Call starts");
					String enrollmentEntityHistoryStr = restClassCommunicator.retrieveAssisterAdminDetailsById(enrollmentEntityRequestDTO);
					LOGGER.info("Retrieving Enrollment Entity History Detail REST Call Ends");
					
					ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class);
					AssisterResponseDTO assisterResponseDTOLocal = reader.readValue(enrollmentEntityHistoryStr );
					
					assisterCertStatusHistory = assisterResponseDTOLocal.getAssisterEntityHistory();
					
				}
				model.addAttribute(STATUS_HISTORY, assisterCertStatusHistory);
			}
			assisterNavigationMap(request, assister, model);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured in getCertificationStatus method : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured in getCertificationStatus method  : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getCertificationStatus: END");

		return "entity/assisteradmin/certificationstatus";
	}

	/**
	 * Method to get AssisterDetail.
	 * 
	 * @param assisterRequestDTO The AssisterRequestDTO instance.
	 * @return assisterResponseDTO The AssisterResponseDTO instance.
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private AssisterResponseDTO getAssisterDetail(AssisterRequestDTO assisterRequestDTO) throws JsonProcessingException, IOException {
		LOGGER.debug(RETRIVINGASSISTERDETAILRESTCALLSTARTS);
		String assisterDetailsResponse = restClassCommunicator.getAssisterDetail(assisterRequestDTO);
		AssisterResponseDTO assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
		 LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_RESPONSE);
		return assisterResponseDTO;
	}

	private AssisterSearchParameters setAssisterSearchParameters(
			String firstName, String lastName, String assisterNumber, String entityName, 
			String certificationStatus, String fromDate, String toDate,
			String statusActive, String statusInActive) {

		LOGGER.info("setAssisterSearchParameters: START");

		AssisterSearchParameters assisterSearchParameters = new AssisterSearchParameters();
		assisterSearchParameters.setFirstName(firstName);
		assisterSearchParameters.setLastName(lastName);
		assisterSearchParameters.setAssisterNumber(assisterNumber);
		assisterSearchParameters.setEntityName(entityName);
		assisterSearchParameters.setCertificationStatus(certificationStatus);
		assisterSearchParameters.setFromDate(fromDate);
		assisterSearchParameters.setToDate(toDate);
		assisterSearchParameters.setStatusActive(statusActive);
		assisterSearchParameters.setStatusInActive(statusInActive);

		LOGGER.info("setAssisterSearchParameters: END");

		return assisterSearchParameters;
	}

	private void assisterNavigationMap(HttpServletRequest request, Assister assister, Model model) throws InvalidUserException, JsonProcessingException, IOException {

		LOGGER.info("assisterNavigationMap: START");
		
		Map<String, Boolean> map = null;
		if (assister != null) {
			map = getLeftNavigationMenuMap(assister);
			model.addAttribute("assisterActivated", Boolean.toString(isAccountActivated(assister.getId())));
		} else {
			map = new HashMap<String, Boolean>();
		}

		Map<String, Boolean> navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(NAVIGATION_MENU_MAP);

		if (navigationMenuMap != null) {
			map.putAll(navigationMenuMap);
		}

		lastVisitedURL = request.getRequestURI().substring(request.getContextPath().length());
		request.getSession().setAttribute(EntityConstants.NAVIGATION_MENU_MAP, map);
		LOGGER.info("assisterNavigationMap: END");
	}

	private boolean isAccountActivated(Integer assisterId) throws JsonProcessingException, IOException {
		boolean assisterActivated =true;
		
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setId(assisterId);
		
		AssisterResponseDTO assisterResponseDTO = getAssisterDetail(assisterRequestDTO);
		Assister assister = assisterResponseDTO.getAssister();
		
		AccountActivation accountActivation =  accountActivationService.getAccountActivationByCreatedObjectId(assister.getId(), GhixRole.ASSISTER.getRoleName(), assister.getEntity().getId(), GhixRole.ENROLLMENTENTITY.getRoleName());
		
		if((accountActivation == null || accountActivation.getStatus().equals(AccountActivation.STATUS.NOTPROCESSED)) && (assister.getCertificationNumber() != null
				&& !"".equals(assister.getCertificationNumber())))
		{
			assisterActivated = false;
		}
		return assisterActivated;
	}

	/**
	 * Get Method to display assister status
	 *
	 * @param model
	 * @param request
	 * @param encryAssisterId
	 * @param documentId
	 * @throws GIRuntimeException 
	 */
	@RequestMapping(value = "/entity/assisteradmin/editcertificationstatus", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ASSISTER)
	public String editCertificationStatus(
			Model model,
			HttpServletRequest request,
			@RequestParam(ASSISTER_ID) String encryAssisterId,
			@RequestParam(value = "documentId", required = false) String documentId) throws GIRuntimeException {
		LOGGER.info("editCertificationStatus: START");
		try {
		String assisterId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryAssisterId);
		
			user = userService.getLoggedInUser();
			LOGGER.info("Inside Edit CertificationStatus");
			Assister assister = null;
			List<LookupValue> statuslist = lookupService
					.getLookupValueList(ASSISTER_CERTIFICATION_STATUS);
			if (assisterId != null && !EntityUtils.isEmpty(assisterId)) {
				AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
				assisterRequestDTO.setId(Integer.valueOf(assisterId));
				
				AssisterResponseDTO assisterResponseDTO = getAssisterDetail(assisterRequestDTO);
				
				assister = assisterResponseDTO.getAssister();
				Validate.notNull(assister);
				EntityUtils.checkEntityAssisterAssociation(assisterId, user, assisterResponseDTO);
				
				model.addAttribute(ASSISTER, assister);
				model.addAttribute(STATUSLIST, statuslist);
				model.addAttribute(CURRDATE, (new TSDate()).getTime());
				if(null != assister.getCertificationDate()){
					model.addAttribute(ASSISTER_CERTIFICATION_DATE,assister.getCertificationDate().getTime());
				}else{
					model.addAttribute(ASSISTER_CERTIFICATION_DATE,0L);
				}
				
				
				// Get the broker details from broker audit table
				List<Map<String, Object>> assisterCertStatusHistory = null;
				if (assister != null) {
					AssisterRequestDTO enrollmentEntityRequestDTO = new AssisterRequestDTO();
					enrollmentEntityRequestDTO.setModuleId(assister.getId());
					LOGGER.info("Retrieving Enrollment Entity History Detail REST Call starts");
					String enrollmentEntityHistoryStr = restClassCommunicator.retrieveAssisterAdminDetailsById(enrollmentEntityRequestDTO);
					LOGGER.info("Retrieving Enrollment Entity History Detail REST Call Ends");
					
					ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class);
					AssisterResponseDTO assisterResponseDTOLocal =  reader.readValue(enrollmentEntityHistoryStr );
							 
					assisterCertStatusHistory = assisterResponseDTOLocal.getAssisterEntityHistory();
				}
				model.addAttribute(STATUS_HISTORY, assisterCertStatusHistory);
			}
			assisterNavigationMap(request, assister, model);
			Assister assisterInSession = (Assister) request.getSession()
					.getAttribute(ASSISTER);
			String fileName = (String) request.getSession().getAttribute(
					FILENAME);
			if (assisterInSession != null) {
				model.addAttribute("newAssister", assisterInSession);
				model.addAttribute("uploadedDocument", documentId);
				model.addAttribute(FILENAME, fileName);
			}
			if ((request.getSession().getAttribute(UPLOADED_DOCUMENT_CHECK) != null)
					&& ("SIZE_FAILURE".equals(request.getSession().getAttribute(UPLOADED_DOCUMENT_CHECK)))) {
				model.addAttribute(UPLOADED_DOCUMENT_CHECK, "SIZE_FAILURE");
			} 
			request.getSession().removeAttribute(ASSISTER);
			request.getSession().removeAttribute(FILENAME);
			request.getSession().setAttribute("fileUploadId",Integer.valueOf(assisterId));
			request.getSession().removeAttribute(UPLOADED_DOCUMENT_CHECK);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while editing certification status : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while editing certification status of Assister: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("editCertificationStatus: END");

		return "entity/assisteradmin/editcertificationstatus";
	}

	/**
	 * Handles the Post for editing assister certification status
	 * 
	 * @param model
	 * @param bindingResult
	 * @param assister
	 * @param assisterId
	 * @param certificationRenewalDate
	 * @return URL for navigation to View Assister Information page
	 * @throws GIRuntimeException 
	 */
	@RequestMapping(value = "/entity/assisteradmin/editcertificationstatus", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ASSISTER)
	public String editCertificationStatusSubmit(
			@ModelAttribute(ASSISTER) Assister assister,BindingResult bindingResult,
			@RequestParam(value = "certificationRenewalDate", required = false) String certificationRenewalDate,
			@RequestParam(value = "certificationStartDate", required = false) String certificationStartDate,
			@RequestParam(value = ASSISTER_ID, required = false) String assisterId) throws GIRuntimeException {

		LOGGER.info("editCertificationStatusSubmit: START");
		List<ConsumerComposite> houseHoldObjList = new ArrayList<ConsumerComposite>();
		boolean sendMail = false;
		try{
		LOGGER.info("Inside EditCertificationSubmit method");
		validateAssisterCertification(assister, bindingResult);
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		if (assister != null) {
			if (assisterId != null && !"".equals(assisterId)) {
				assister.setId(Integer.parseInt(assisterId));
			}
			assister.setStatusChangeDate(new TSDate());
			assister.setDocumentId(assister.getDocumentId());
			
			if(!EntityUtils.isEmpty(certificationRenewalDate) && !EntityUtils.isEmpty(certificationStartDate)){
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				Date reCertificationDate = dateFormat.parse(certificationRenewalDate);
				Date certStartDate = dateFormat.parse(certificationStartDate);
				assister.setReCertificationDate(reCertificationDate);
				assister.setCertificationDate(certStartDate);
			}
			//get assister details
			assisterRequestDTO.setAssister(assister);
			assisterRequestDTO.setId(assister.getId());
			AssisterResponseDTO assisterResponseDTO = getAssisterDetail(assisterRequestDTO);
			Assister assisterObj = assisterResponseDTO.getAssister();
			//get current active individuals for assister
			if(!EntityUtils.checkStateCode("CA")){
				
				String statusvalues =  DynamicPropertiesUtil.getPropertyValue(AEEConfiguration.AEEConfigurationEnum.ENROLLMENTENTITY_CEC_DEDESIGNATIONSTATUS);
				String[] cecStatusValues = statusvalues!=null? statusvalues.split(","): new String[]{};
				
				if (assister.getCertificationStatus() != null) {
					for(String statusFromProperty:cecStatusValues){
						if(assister.getCertificationStatus().equalsIgnoreCase(statusFromProperty)){
							sendMail=true;
						}
					}
				}
				
				if(sendMail){
					Map<String, Object> inividualAPIRequestMap = new HashMap<String, Object>();
			        inividualAPIRequestMap.put(BrokerConstants.MODULE_NAME, BrokerConstants.ASSISTER);
			        inividualAPIRequestMap.put(BrokerConstants.STATUS, BrokerConstants.STATUS_ACTIVE);
			        inividualAPIRequestMap.put(BrokerConstants.MODULE_ID, assisterObj.getId());
			        inividualAPIRequestMap.put(BrokerConstants.SENDER_OBJ, assisterObj);
			        Integer startRecord = 0;
			        inividualAPIRequestMap.put(BrokerConstants.START_PAGE, startRecord);
					Map<String, Object> searchCriteriaMap = new HashMap<String, Object>();
					searchCriteriaMap.put(SEARCH_CRITERIA, inividualAPIRequestMap);
					houseHoldObjList = new ArrayList<ConsumerComposite>();
					//List<String> individualEmailAdList = new ArrayList<String>();
					Map<String, Object> houseHoldMap = brokerMgmtUtils.retrieveConsumerList(searchCriteriaMap);
					if( houseHoldMap != null && houseHoldMap.size() > 0 ) {
						houseHoldObjList = (List<ConsumerComposite>) houseHoldMap.get(BrokerConstants.HOUSEHOLDLIST);
					}
				}
			}
			//update the certification status i
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class);

			
			String requestJSON = writer.writeValueAsString(assisterRequestDTO);
			
			 
			String assisterDetailsResponse = restClassCommunicator.saveAssisterStatus(requestJSON);
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class);
			
			assisterResponseDTO = reader.readValue(assisterDetailsResponse );
			 

			//send notice
			if(!EntityUtils.checkStateCode("CA")){
				String fileNameForNotification = null;
				//if assister account not activated - send activation link and certification change notice

				if (!isAccountActivated(assister.getId()) && "Certified".equals(assister.getCertificationStatus())) {
					String redirectUrl= generateLinkAndRedirect(assister.getId(), "/entity/assisteradmin/certificationstatus");
					createCECtatusChangeNotice(assister, assisterObj);
					return redirectUrl;
				}
				//if assister certification status is changed - send certification change notice
				if (!assisterObj.getCertificationStatus().equalsIgnoreCase(assister.getCertificationStatus())) {
					createCECtatusChangeNotice(assister, assisterObj);
				}
				
				//sent not to individuals if needed.
				if (sendMail) 
					{
						LOGGER.info("send CEC termination notice: START");
						// Get the list of Individuals associated with this Enrollment counselor
						String noticeTemplateName = NOTIFICATION_TEMPLATE_NAME;
						String exchangeName = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
			            Map<String, Object> assisterTemplateData = new HashMap<String, Object>();
			     		String assistername = assisterObj.getFirstName() +" "+assisterObj.getLastName();
			     		assisterTemplateData.put(ASSISTER_NAME, assistername);
			     		assisterTemplateData.put(ENROLLMENTENTITY_NAME, assisterObj.getEntity().getEntityName());
			     		
			     		SimpleDateFormat sdf = new SimpleDateFormat("MMMMMMMMM dd, yyyy");
			     		assisterTemplateData.put(NOTIFICATION_SEND_DATE, sdf.format(new TSDate()));
			     		assisterTemplateData.put(REASON_FOR_DE_DESIGNATION, "Your Counselor "+assistername+ " is no longer affiliated with the "+exchangeName);
			     		
			     		// tokens required for the notification template
			     		assisterTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
			     		assisterTemplateData.put(EXCHANGE_ADDRESS_LINE_ONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
			     		assisterTemplateData.put(STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
			     		assisterTemplateData.put(COUNTRY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
			     		assisterTemplateData.put(EXCHANGE_URL, GhixEndPoints.GHIXWEB_SERVICE_URL);
			     		assisterTemplateData.put("EXCHANGE_URL", GhixEndPoints.GHIXWEB_SERVICE_URL);// need this token to match with template. Fix the template also to make tokens consistent
			     		assisterTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			     		// HIX-20774
			     		
			     		assisterTemplateData.put(EXCHANGE_NAME, exchangeName);
			     	
			     		assisterTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
			     		assisterTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
			     		assisterTemplateData.put(EXCHANGE_PIN_CODE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
			     		assisterTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);
			     		
			     		Map<String, String> templateDataModified = getTemplateDataMap(assisterTemplateData);
			     		for(ConsumerComposite houseHoldObj:houseHoldObjList)
						{	
			     			PreferencesDTO  preferencesDTO  =  preferencesService.getPreferences((int)houseHoldObj.getId(), false);
			     			Location location = EntityUtils.getLocationFromDTO(preferencesDTO.getLocationDto());
			     			List<String> individualEmailAdList = new ArrayList<String>();
			     			individualEmailAdList.add(preferencesDTO.getEmailAddress());	
			     			fileNameForNotification = INDIVIDUAL_NOTIFICATION + TimeShifterUtil.currentTimeMillis() + PDF;
								noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, assisterTemplateData, ECMRELATIVEPATH, fileNameForNotification, INDIVIDUAL_MODULE, 
											houseHoldObj.getId(), individualEmailAdList, assisterObj.getUser().getFullName(),
											(houseHoldObj.getFirstName() + " " + houseHoldObj.getLastName()),location,preferencesDTO.getPrefCommunication());																																													
						}
			     		LOGGER.info("send CEC termination notice: END");
					}
			}
		} else {
			LOGGER.error("Assister is Null in editCertificationStatusSubmit method");
		}
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while editing certification status post method : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving edited certification status post method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("editCertificationStatusSubmit: END");
		
		
		return "redirect:/entity/assisteradmin/certificationstatus?assisterId="
				+ ghixJasyptEncrytorUtil.encryptStringByJasypt(assisterId);
	}

	private void validateAssisterCertification(Assister assister,
			BindingResult bindingResult) {
		if(assister.getCertificationStatus()!= null && !assister.getCertificationStatus().isEmpty()){
			if("certified".equalsIgnoreCase(assister.getCertificationStatus())){
				validator.validate(assister,bindingResult,Assister.AssisterCertStatusGroup.class);
				if (bindingResult.hasErrors()){
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				}
			}
		}else{
			 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
	}
	/**
	 * @param assister
	 * @param assisterObj
	 */
	private void createCECtatusChangeNotice(Assister assister,
			Assister assisterObj) {
		
		LOGGER.info("createCECtatusChangeNotice : Starts");
		
		String priorStatus = assisterObj.getCertificationStatus();
		assisterObj.setCertificationStatus(assister.getCertificationStatus());
		assisterObj.setComments(assister.getComments());
		assisterObj.setStatusChangeDate(new TSDate());			
		try {
			entityService.createCECStatusChangeNotice(assisterObj, priorStatus);
		} catch (Exception e) {
			LOGGER.error("Error generating notice for entity certification status change", e);
		}
		LOGGER.info("createCECtatusChangeNotice : Ends");
	}
	
	/**
	 * Handles the Get for editing assister information
	 * 
	 * @param model
	 * @param encryAssisterId
	 * @param request
	 * @return URL for navigation to View Assister Information page
	 * @throws GIRuntimeException 
	 */
	@RequestMapping(value = "/entity/assister/editassisterinformation", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER)
	public String editAssisterInformation(Model model, HttpServletRequest request,
			@RequestParam(value = ASSISTER_ID, required = false) String encryAssisterId) 
					throws GIRuntimeException {

		LOGGER.info("editAssisterInformation: START");
		
		try {
		String assisterId =encryAssisterId!=null? ghixJasyptEncrytorUtil.decryptStringByJasypt(encryAssisterId):null;
		
		LOGGER.info(EDIT_ASSISTER_INFO);
		model.addAttribute(PAGE_TITLE, EDIT_ASSISTER_INFO);
		model.addAttribute("isascflag", true);
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		AssisterResponseDTO assisterResponseDTO = null;
		List<Site> listOfSites = new ArrayList<Site>();
		String showPostalMailOption=FALSE;
		
			user = userService.getLoggedInUser();
			
			showPostalMailOption=EntityUtils.isAllaowMailNotices();
			
			EnrollmentEntity enrollmentEntity = null;
			
			Validate.notNull(assisterId);
			
			assisterRequestDTO.setId(Integer.valueOf(assisterId));

			assisterResponseDTO = getAssisterDetail(assisterRequestDTO);
			
			LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_RESPONSE);
			EntityUtils.checkEntityAssisterAssociation(assisterId, user, assisterResponseDTO);
			Assister assister = assisterResponseDTO.getAssister();
			model.addAttribute(ASSISTER, assister);
			model.addAttribute(ENROLLMENTENTITY, assister.getEntity());
			model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
			model.addAttribute(SHOW_POSTAL_MAIL,showPostalMailOption);
			String languageDetails =  loadLanguages();
			
			model.addAttribute(LANGUAGES_LIST, languageDetails);
			
			setContactNumber(model, assister);
			
			if (assisterId != null) {
				//if (enrollmentEntity == null) {
					enrollmentEntity = assister.getEntity();
				//}

				determineWorkSitesForAnEnrollmentEntity(model, listOfSites,
						enrollmentEntity);

				List<LookupValue> listOfLanguagesSpoken = populateEntitySpokenLanguages();
				
				model.addAttribute(LIST_OF_LANGUAGES_SPOKEN, listOfLanguagesSpoken);

				List<LookupValue> listOfLanguagesWritten = populateEntityWrittenLanguages();
				
				model.addAttribute(LIST_OF_LANGUAGES_WRITTEN, listOfLanguagesWritten);

				model.addAttribute(LIST_OF_STATES,
						new StateHelper().getAllStates());

				model.addAttribute(LIST_OF_EDUCATIONS, getEducationList());

				if (assister.getCertificationNumber() != null
						&& !"".equals(assister.getCertificationNumber())) {
					model.addAttribute(IS_CERTIFICATION_CERTIFIED, TRUE);
				}
				AssisterResponseDTO response = setAssisterAndLanguages(model,
						Integer.valueOf(assisterId));
				AssisterLanguages assisterLanguages = response
						.getAssisterLanguages();
				setOtherSpokenLanguage(assisterLanguages, listOfLanguagesSpoken, model);
				setOtherWrittenLanguage(assisterLanguages, listOfLanguagesWritten, model);

				// Set language names for other language text box checking
				setLanguageNames(listOfLanguagesSpoken, listOfLanguagesWritten, model);
				assisterNavigationMap(request, assister, model);
			}
		} catch (GIException giException) {
			LOGGER.error("GIException occured while editing assister information : ", giException);
			throw new GIRuntimeException(giException, ExceptionUtils.getFullStackTrace(giException), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while editing assister information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while edit assister information page: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("editAssisterInformation: END");

		return "entity/assister/editassisterinformation";
	}

	/**
	 * Method to determine Work Sites for an EnrollmentEntity.
	 * 
	 * @param model The Model instance.
	 * @param listOfSites The collection of work sites.
	 * @param enrollmentEntity The EnrollmentEntity instance.
	 */
	private void determineWorkSitesForAnEnrollmentEntity(Model model, List<Site> listOfSites, EnrollmentEntity enrollmentEntity) {
		List<Site> listOfPrimarySites;
		List<Site> listOfSecondarySites;
		// Fetching Assister's Primary Site
		listOfPrimarySites = getListOfEnrollmentEntitySitesBySiteType(
				enrollmentEntity, Site.site_type.PRIMARY.toString());

		// Fetching Assister's Secondary/Sub-Sites
		listOfSecondarySites = getListOfEnrollmentEntitySitesBySiteType(
				enrollmentEntity, Site.site_type.SUBSITE.toString());

		listOfSites.addAll(listOfPrimarySites);
		model.addAttribute(LIST_OF_PRIMARY_SITES, listOfSites);
		listOfSites.addAll(listOfSecondarySites);
		model.addAttribute(LIST_OF_SECONDARY_SITES, listOfSites);
	}

	/**
	 * Method to retrieve EnrollmentEntity.
	 * 
	 * @param assister The Assister instance.
	 * @return enrollmentEntity The EnrollmentEntity instance.
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private EnrollmentEntity retrieveEnrollmentEntity(Assister assister) throws JsonProcessingException, IOException {
		EnrollmentEntity enrollmentEntity;
		EnrollmentEntityRequestDTO enrollmentEntityRequest = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequest.setModuleId(assister.getEntity()
				.getId());
		
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = retrieveEnrollmentEntityObjectById(enrollmentEntityRequest);
		
		enrollmentEntity = enrollmentEntityResponseDTO
				.getEnrollmentEntity();
		return enrollmentEntity;
	}

	/**
	 * Method to retrieve EnrollmentEntity For User identifier.
	 * 
	 * @param entityId The entity identifier.
	 * @return enrollmentEntity The EnrollmentEntity instance.
	 * @throws InvalidUserException The InvalidUserException instance.
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 * @throws NumberFormatException The NumberFormatException instance.
	 */
	private EnrollmentEntity retrieveEnrollmentEntityForUserId(String entityId) throws InvalidUserException, JsonProcessingException, IOException {
		int userId;
		EnrollmentEntity enrollmentEntity = null;
		
		if (!isEntityAdmin()) {				
			if (user != null) {
				userId = user.getId();
				enrollmentEntity = getEnrollmentEntityByUserId(userId);
			}
		} else {
			//if role is admin then retirve the user id from rest call
			//GET_ENROLLMENTENTITY_DETAILS_BY_ID
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setModuleId(Integer.parseInt(entityId));
			
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = retrieveEnrollmentEntityObjectById(enrollmentEntityRequestDTO);
			
			if (enrollmentEntityResponseDTO.getEnrollmentEntity().getUser() != null) {
				userId = enrollmentEntityResponseDTO.getEnrollmentEntity().getUser().getId();
			}
		}
		return enrollmentEntity;
	}

	/**
	 * Method to populate the collection of languages those which can be written by an entity.
	 * 
	 * @return listOfLanguagesWritten The collection of languages those which can be written by an entity.
	 */
	private List<LookupValue> populateEntityWrittenLanguages() {
		// Fetching List of Languages
		LookupValue langWritten=null;
		List<LookupValue> listOfLanguagesWritten = lookupService.getLookupValueList(ENTITY_LANGUAGE_WRITTEN);
		
		for(LookupValue lookupValue:listOfLanguagesWritten){
			if("English".equalsIgnoreCase(lookupValue.getLookupValueLabel())){
				langWritten=lookupValue;
				listOfLanguagesWritten.remove(lookupValue);
			    break;
			}
		}
		
		Collections.sort(listOfLanguagesWritten);
		Collections.reverse(listOfLanguagesWritten);
		if(langWritten!=null){
			listOfLanguagesWritten.add(0,langWritten);
		}
		return listOfLanguagesWritten;
	}

	/**
	 * Method to populate the collection of languages those which can be spoken by an entity.
	 * 
	 * @return listOfLanguagesSpoken The collection of languages those which can be spoken by an entity.
	 */
	private List<LookupValue> populateEntitySpokenLanguages() {
		// Fetching List of Languages
		LookupValue langSpoken=null;
		List<LookupValue> listOfLanguagesSpoken = lookupService.getLookupValueList(ENTITY_LANGUAGE_SPOKEN);

		for(LookupValue value:listOfLanguagesSpoken){
			if("English".equalsIgnoreCase(value.getLookupValueLabel())){
				langSpoken=value;
				listOfLanguagesSpoken.remove(value);
				break;
			}
		}
		
		Collections.sort(listOfLanguagesSpoken);
		Collections.reverse(listOfLanguagesSpoken);
		
		if(langSpoken!=null){
			listOfLanguagesSpoken.add(0,langSpoken);
		}
		return listOfLanguagesSpoken;
	}

	/**
	 * Method to retrieve EnrollmentEntity instance for given identifier.
	 * @param enrollmentEntityRequestDTO The EnrollmentEntityResponseDTO instance.
	 * @return enrollmentEntityResponseDTO The EnrollmentEntityResponseDTO instance.
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private EnrollmentEntityResponseDTO retrieveEnrollmentEntityObjectById(
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) throws JsonProcessingException, IOException {
		String enrollEntityResponse = restClassCommunicator.retrieveEnrollmentEntityObjectById(enrollmentEntityRequestDTO);
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO =  JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollEntityResponse);
		return enrollmentEntityResponseDTO;
	}

	/**
	 * Handles the Post for editing assister information
	 * 
	 * @param model
	 * @param assisterBindingResult
	 * @param assister
	 * @param assisterId
	 * @param assisterLanguageId
	 * @param primarySite
	 * @param secondarySite
	 * @param assisterLanguages
	 * @param languageBindingResult
	 * @param request
	 * @param entityId
	 * @return URL for navigation to View Assister Information page
	 * @throws GIRuntimeException 
	 */
	@RequestMapping(value = "/entity/assister/editassisterinformation", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER)
	public String updateAssisterInformation(
			Model model,
			@ModelAttribute(ASSISTER) Assister assister,BindingResult assisterBindingResult,
			@RequestParam(value = ASSISTER_ID, required = false) String assisterId,
			@RequestParam(value = "assisterLanguageId", required = false) String assisterLanguageId,
			@RequestParam(value = "primarySite", required = false) String primarySite,
			@RequestParam(value = "secondarySite", required = false) String secondarySite,
			@ModelAttribute(ASSISTER_LANGUAGES)@Validated AssisterLanguages assisterLanguages,BindingResult languageBindingResult,
			@RequestParam(value = FILE_INPUT, required = false) CommonsMultipartFile fileInput,
			HttpServletRequest request,@RequestParam(value=ENTITY_ID,required=false)String entityId) throws GIRuntimeException{

		LOGGER.info("updateAssisterInformation: START");

		AssisterResponseDTO assisterResponseDTO = null;
		try {
		LOGGER.info(EDIT_ASSISTER_INFO);
		
		/** Check for XSS Attack - Starts */
		checkXSSAttackForUpdateAssisterInformation(assister, assisterLanguages);		
		/** Check for XSS Attack - Ends */

		model.addAttribute(PAGE_TITLE, EDIT_ASSISTER_INFO);
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
	
		EnrollmentEntity enrollmentEntity = null;
		AssisterResponseDTO tempAssister = null;
		int userId = 0;
		
			if (!isEntityAdmin()) {
				user = userService.getLoggedInUser();
				if (user != null) {
					userId = user.getId();
					enrollmentEntity = getEnrollmentEntityByUserId(userId);
					EntityUtils.checkEntityAssisterAssociation(assisterId.toString(), user, getAssisterResponseDTO(assister, assisterId,
							assisterRequestDTO));
				}
			} else {
				tempAssister = getAssisterResponseDTO(assister, assisterId, assisterRequestDTO);
				enrollmentEntity = tempAssister.getAssister().getEntity();
			}
			
			String isAssisterCertified="No";
			String[] isCertifiedCheckBoxValue = request.getParameterValues("isAssisterCertified");
			if("Yes".equals(isCertifiedCheckBoxValue[0])){
				assister.setCertificationNumber(assister.getCertificationNumber());
				isAssisterCertified="Yes";
			}
			else{
				assister.setCertificationNumber(null);
			}
			
			populateAssisterRequestDTOWithAssiterInformation(assister, assisterId, assisterLanguageId, primarySite, secondarySite, assisterLanguages, entityId,
					assisterRequestDTO, enrollmentEntity, userId);

			//validate after setting primary site and secondary site
			 validateAssisterInfoUpdate(assisterBindingResult,languageBindingResult,assister,isAssisterCertified);
			
			// Saving Assister details
			assisterResponseDTO = saveAssisterDetails(assisterRequestDTO);

			// saving assister photo
			if (assisterResponseDTO != null && assisterResponseDTO.getAssister() != null)
			{
				entityService.saveAssisterPhoto(assisterResponseDTO.getAssister().getId(), fileInput);
			}
			
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while updating assister information post : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while editing assister information post : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		Validate.notNull(assisterResponseDTO);
		Validate.notNull(assisterResponseDTO.getAssister());		
		
		LOGGER.info("updateAssisterInformation: END");
		
		return "redirect:/entity/assisteradmin/viewassisterinformation?assisterId="
				+ ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(assisterResponseDTO.getAssister().getId()))+ "&entityId=" + ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(assisterResponseDTO.getAssister().getEntity().getId()));
	}

	private void validateAssisterInfoUpdate(BindingResult assisterBindingResult,BindingResult languageBindingResult, Assister assister,String isAssisterCertified) {

		if(!EntityUtils.isEmpty(isAssisterCertified) && "Yes".equalsIgnoreCase(isAssisterCertified)){
			validator.validate(assister,assisterBindingResult,Assister.AddAssisterByEntity.class,Assister.AssisterCerticationNumber.class,Assister.AssisterInfoGroup.class);
		}else{
			validator.validate(assister,assisterBindingResult,Assister.AddAssisterByEntity.class,Assister.AssisterInfoGroup.class);
		}
		if(assisterBindingResult.hasErrors() || languageBindingResult.hasErrors()){
			LOGGER.info("ADD : Assister Registration Ends");
			  throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
		return;
	}
	
	private void validateAssisterProfileUpdate(BindingResult assisterBindingResult,BindingResult languageBindingResult, Assister assister) {
			validator.validate(assister,assisterBindingResult,Assister.AddAssisterByEntity.class);
		if(assisterBindingResult.hasErrors() || languageBindingResult.hasErrors()){
			LOGGER.info("ADD : Assister Registration Ends");
			 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
		return ;
	}
	
	/**
	 * Method to populate AssisterRequestDTO with Assister information.
	 * 
	 * @param assister The Assister instance.
	 * @param assisterId The assister identifier
	 * @param assisterLanguageId The Assister language identifier.
	 * @param primarySite The primarySite.
	 * @param secondarySite The secondarySite.
	 * @param assisterLanguages The current AssisterLanguages.
	 * @param entityId The entity identifier.
	 * @param assisterRequestDTO The current AssisterRequestDTO instance.
	 * @param enrollmentEntity The current EnrollmentEntity instance.
	 * @param userId The current user identifier.
	 * @throws GIException The GIException instance
	 */
	private void populateAssisterRequestDTOWithAssiterInformation(Assister assister, String assisterId,
			String assisterLanguageId, String primarySite,
			String secondarySite, AssisterLanguages assisterLanguages,
			String entityId, AssisterRequestDTO assisterRequestDTO,
			EnrollmentEntity enrollmentEntity, int userId) throws GIException {
		try {
			assister.setId(Integer.parseInt(assisterId));
			assisterRequestDTO.setAssister(assister);
			assisterRequestDTO.setEnrollmentEntityId(Integer.parseInt(entityId));
			assisterRequestDTO.setUserId(userId);
			
			// Setting Assister Languages Id to update Languages (Required in
			// case
			// of only Edit Operation)
			if (assisterLanguageId != null && !"".equals(assisterLanguageId)) {
				assisterLanguages.setId(Integer.parseInt(assisterLanguageId));
			}
	
			// Fetching selected Primary Site object and associating with
			// Assister
			if (primarySite != null && !"".equals(primarySite)) {
				assister.setPrimaryAssisterSite(getSiteById(Integer
						.parseInt(primarySite)));
			}
	
			// Fetching selected Secondary/Sub Site object and associating with
			// Assister
			if (secondarySite != null && !"".equals(secondarySite)) {
				assister.setSecondaryAssisterSite(getSiteById(Integer
						.parseInt(secondarySite)));
			}
			else{
				assister.setSecondaryAssisterSite(null);
			}
			// Associating EE to Assister
			assister.setEntity(enrollmentEntity);
			assisterRequestDTO.setAssister(assister);
			assisterRequestDTO.setAssisterLanguages(assisterLanguages);
		}
		catch(NumberFormatException exNF){
			LOGGER.error("Exception occurrend while populating AssiterRequestDTO. Exception:" +exNF.getMessage());
			throw new GIException(exNF);
		}
	}

	/**
	 * Method to get AssisterResponseDTO.
	 * @param assister The Assister instance.
	 * @param assisterId The Assister identifier.
	 * @param tempAssister The AssisterResponseDTO instance.
	 * @return
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */

	private AssisterResponseDTO getAssisterResponseDTO(Assister assister, String assisterId, AssisterRequestDTO assisterRequestDTO) throws JsonProcessingException, IOException {
		AssisterResponseDTO tempAssister = null;
		
		if(assisterId != null) {
			assisterRequestDTO.setId(Integer.parseInt(assisterId));
		}
		else if(assister != null) {
			assisterRequestDTO.setId(assister.getId());
		}

		tempAssister = getAssisterDetail(assisterRequestDTO);
		
		return tempAssister;
	}

	/**
	 * Method to check for XSS attack in assister information update operation.
	 * 
	 * @param assister The Assister instance.
	 * @param assisterLanguages The AssisterLanguages instance.
	 */
	private void checkXSSAttackForUpdateAssisterInformation(Assister assister, AssisterLanguages assisterLanguages) {
		
		assister.setComments(XssHelper.stripXSS(assister.getComments()));
		assister.setBusinessLegalName(XssHelper.stripXSS(assister
				.getBusinessLegalName()));
		assister.setEmailAddress(XssHelper.stripXSS(assister.getEmailAddress()));
		assister.setFirstName(XssHelper.stripXSS(assister.getFirstName()));
		assister.setLastName(XssHelper.stripXSS(assister.getLastName()));
		assister.setPrimaryPhoneNumber(XssHelper.stripXSS(assister
				.getPrimaryPhoneNumber()));
		assister.setSecondaryPhoneNumber(XssHelper.stripXSS(assister
				.getSecondaryPhoneNumber()));

		if (assister.getCertificationNumber() != null) {
			String certNo = XssHelper.stripXSS(String.valueOf(assister
					.getCertificationNumber()));
			assister.setCertificationNumber(!StringUtils.isEmpty(certNo) ? Long
					.parseLong(certNo) : null);
		}

		if (assister.getMailingLocation() != null) {
			assister.getMailingLocation().setAddress1(
					XssHelper.stripXSS(assister.getMailingLocation()
							.getAddress1()));
			assister.getMailingLocation().setAddress2(
					XssHelper.stripXSS(assister.getMailingLocation()
							.getAddress2()));
			assister.getMailingLocation()
					.setCity(
							XssHelper.stripXSS(assister.getMailingLocation()
									.getCity()));

			String zip = XssHelper.stripXSS(String.valueOf(assister
					.getMailingLocation().getZip()));
			assister.getMailingLocation().setZip(!StringUtils.isEmpty(zip) ? zip : null);
		}

		if (assisterLanguages != null) {
			// Adding check for Assister languages
			assisterLanguages.setSpokenLanguages(XssHelper
					.stripXSS(assisterLanguages.getSpokenLanguages()));
			assisterLanguages.setWrittenLanguages(XssHelper
					.stripXSS(assisterLanguages.getWrittenLanguages()));
		}
	}

	/**
	 * Handles the Post for updating assister details
	 * 
	 * @param model
	 * @param assisterId
	 * @return URL for navigation to View Assister Information page
	 * @throws GIRuntimeException 
	 */
	@RequestMapping(value = "/entity/assisteradmin/updateassisterdetail", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ASSISTER)
	public String updateAssisterDetail(
			Model model,
			@RequestParam(value = ASSISTER_ID, required = false) String assisterId) throws GIRuntimeException {

		LOGGER.info("updateAssisterDetail: START");

		
		try{
			

		LOGGER.info(MANAGE_ASSISTER);
		model.addAttribute(PAGE_TITLE, MANAGE_ASSISTER);
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		AssisterResponseDTO assisterResponseDTO = null;
		String assisterDetailsResponse = null;

		if (assisterId != null && !"".equals(assisterId)) {
		

				assisterRequestDTO.setId(Integer.valueOf(assisterId));
				ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class);
				String requestXML = writer.writeValueAsString(assisterRequestDTO);

				LOGGER.debug("Updating Assister Detail REST Call Starts.");
				assisterDetailsResponse = restClassCommunicator
						.updateAssisterDetail(requestXML);
				assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
				LOGGER.debug("Updating Assister Detail REST Call Ends.");
			
		}

		if(null != assisterResponseDTO) {
			model.addAttribute(ASSISTER, assisterResponseDTO.getAssister());
			model.addAttribute(ASSISTER_LANGUAGES,
				assisterResponseDTO.getAssisterLanguages());
		}
	} catch(GIRuntimeException giexception){
		LOGGER.error("GIRuntimeException occured while updating assister details: ", giexception);
		throw giexception;
	} catch(Exception exception){
		LOGGER.error("Exception occured while updating assister details : ", exception);
		throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
	}
		LOGGER.info("updateAssisterDetail: END");

		return "entity/assisteradmin/viewassisterprofile";
	}

	/**
	 * Handles the Assister view page
	 * 
	 * @param model
	 * @param request
	 * @param encryAssisterId
	 * @return URL for navigation to View Assister Information page
	 * @throws InvalidUserException 
	 * @throws GIException 
	 */
	@RequestMapping(value = "/entity/assisteradmin/viewassisterinformation", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ASSISTER_EDIT_ASSISTER')")
	public String viewAssisterInformation(
			Model model,
			@RequestParam(value = ASSISTER_ID, required = false) String encryAssisterId,
			HttpServletRequest request) throws GIRuntimeException {

		LOGGER.info("viewAssisterInformation: START");

		try{
			
			LOGGER.info("View Assister Information");
			String assisterId = encryAssisterId !=null && !"".equals(encryAssisterId)?ghixJasyptEncrytorUtil.decryptStringByJasypt(encryAssisterId):null;
			model.addAttribute(PAGE_TITLE,
					"Getinsured Health Exchange Assister : Assister Information");
			AssisterResponseDTO assisterResponseDTO = null;
			AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
	
			if (assisterId != null && !"".equals(assisterId)) {
				assisterRequestDTO.setId(Integer.valueOf(assisterId));
	
				assisterResponseDTO = getAssisterDetail(assisterRequestDTO);
			}

			Validate.notNull(assisterResponseDTO);
			
			Assister assister = assisterResponseDTO.getAssister();
			EntityUtils.checkEntityAssisterAssociation(assisterId, userService.getLoggedInUser(), assisterResponseDTO);
			model.addAttribute(ASSISTER, assister);
			model.addAttribute(ENROLLMENTENTITY, assister.getEntity());
			AssisterLanguages assisterLanguages =assisterResponseDTO.getAssisterLanguages();
			
			if(null != assisterLanguages) {
				String writtenLang=assisterLanguages.getWrittenLanguages();
				String writtenStr = "";
				if(writtenLang !=null){
					String[] writtenLangList= writtenLang.split(",");
					StringBuilder writtenBuilder=new StringBuilder();
					for(String writtenLangStr:writtenLangList){
			
							writtenBuilder.append(writtenLangStr);
							writtenBuilder.append(", ");
					}
						writtenStr =writtenBuilder.toString();
						if(!EntityUtils.isEmpty(writtenStr)){
						writtenStr=writtenStr.substring(0,writtenStr.length()-TWO);	
						
					}
				}
				
				String spokenLang=assisterLanguages.getSpokenLanguages();
				String spokenStr = "";
				if(spokenLang != null){
					String[] spokenLangList= spokenLang.split(",");
					StringBuilder spokenBuilder=new StringBuilder();
					for(String spokenLangStr:spokenLangList){
		
						spokenBuilder.append(spokenLangStr);
						spokenBuilder.append(", ");
					}
						spokenStr =spokenBuilder.toString();
						if(!EntityUtils.isEmpty(spokenStr)){
							spokenStr=spokenStr.substring(0,spokenStr.length()-TWO);	
						
					}
				}
			     assisterLanguages.setWrittenLanguages(writtenStr);
			     assisterLanguages.setSpokenLanguages(spokenStr);
			}
		
			String showPostalMailOption=FALSE;
			showPostalMailOption=EntityUtils.isAllaowMailNotices();
	     
			model.addAttribute(SHOW_POSTAL_MAIL,showPostalMailOption);
			model.addAttribute(ASSISTER_LANGUAGES,assisterLanguages);
			assisterNavigationMap(request, assister, model);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing assister page : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while getting view assister page : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("viewAssisterInformation: END");

		return "entity/assisteradmin/viewassisterinformation";
	}

	private Map<String, Boolean> getLeftNavigationMenuMap(Assister assister) throws GIRuntimeException{

		LOGGER.info("getLeftNavigationMenuMap: START");

		AssisterResponseDTO assisterResponseDTO = null;

		Map<String, Boolean> map = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setAssister(assister);
		String response = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class);
			String requestXML = writer.writeValueAsString(assisterRequestDTO);

			LOGGER.debug("Populate Left Navigation Map REST Call Starts.");
			response = restClassCommunicator
					.assisterPopulateNavigationMenuMap(requestXML);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(response);
			map = assisterResponseDTO.getNavigationMenuMap();
			LOGGER.debug("Populate Left Navigation Map REST Call Ends.");
		} catch (Exception exception) {
			LOGGER.error(
					"Exception occurred while Populating Left Navigation Map : ",
					exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getLeftNavigationMenuMap: END");

		return map;
	}

	/**
	 * Handles the Assister Profile View page
	 * 
	 * @param model
	 * @param request
	 * @param encryptedId
	 * @return URL for navigation to View Assister Information page
	 * @throws GIException 
	 * @throws InvalidUserException 
	 */
	@RequestMapping(value = "/entity/assisteradmin/viewassisterprofile", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER)
	public String viewAssisterProfile(
			Model model,
			@RequestParam(value = ASSISTER_ID, required = false) String encryptedId,
			HttpServletRequest request) throws GIRuntimeException {

		LOGGER.info("viewAssisterProfile: START");

		try{
			

		LOGGER.info("View Assister Information");
		String assisterId = "";
		if(encryptedId != null && ! "".equals(encryptedId)){
			assisterId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
		}	
		model.addAttribute(PAGE_TITLE,
				"Getinsured Health Exchange Assister : Assister Information");
		AssisterResponseDTO assisterResponseDTO = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		Assister assister = null;
		if (assisterId != null && !"".equals(assisterId)) {
			assisterRequestDTO.setId(Integer.valueOf(assisterId));

			assisterResponseDTO = getAssisterDetail(assisterRequestDTO);
			
			assister = assisterResponseDTO.getAssister();
			
			model.addAttribute(ASSISTER, assister);
			model.addAttribute(ASSISTER_LANGUAGES,
					assisterResponseDTO.getAssisterLanguages());
			setContactNumber(model, assister);
		}
		Validate.notNull(assister);
		EntityUtils.checkEntityAssisterAssociation(assisterId, userService.getLoggedInUser(), assisterResponseDTO);
		
		assisterNavigationMap(request, assister, model);
		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing assister profile page : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while getting view assister profile page : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("viewAssisterProfile: END");

		return "entity/assisteradmin/viewassisterprofile";
	}

	@RequestMapping(value = "/entity/assisteradmin/photo/{encrytedAssisterIdforURL}", method = RequestMethod.GET, produces = "image/jpeg; charset=utf-8")
	@ResponseBody
	public byte[] downloadPhotoById(
			@PathVariable("encrytedAssisterIdforURL") String encrytedAssisterIdforURL,
			HttpServletResponse response) throws GIRuntimeException{

		LOGGER.info("downloadPhotoById: START");

		byte[] assisterPhoto = null;
		
		
		try {
			Integer assisterId=Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(encrytedAssisterIdforURL));
			LOGGER.info("Retrieving Assister Photo by Assister Id : "+assisterId);
			
			assisterPhoto = entityService.getAssisterPhotoById(assisterId);
			if(null != assisterPhoto){
				response.setContentType(getContentTypeFromBytes(assisterPhoto));
			}
			LOGGER.debug("Retrieved Assister Photo successfully by Assister Id : ");
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retreiving Assister Photo : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retreiving Assister Photo successfully : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("downloadPhotoById: END");

		return assisterPhoto;
	}

	private void setContactNumber(Model model, Assister assister) {

		LOGGER.info("setContactNumber: START");

		if (assister.getPrimaryPhoneNumber() != null && (!"".equals(assister.getPrimaryPhoneNumber()))) {
			model.addAttribute("primaryPhone1", assister
					.getPrimaryPhoneNumber().substring(ZERO, THREE));
			model.addAttribute("primaryPhone2", assister
					.getPrimaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute("primaryPhone3", assister
					.getPrimaryPhoneNumber().substring(SIX, TEN));
		}
		if (assister.getSecondaryPhoneNumber() != null && (!"".equals(assister.getSecondaryPhoneNumber()))) {
			model.addAttribute("secondaryPhone1", assister
					.getSecondaryPhoneNumber().substring(ZERO, THREE));
			model.addAttribute("secondaryPhone2", assister
					.getSecondaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute("secondaryPhone3", assister
					.getSecondaryPhoneNumber().substring(SIX, TEN));
		}

		LOGGER.info("setContactNumber: END");
	}

	/**
	 * Handles the POST request for Edit Broker information
	 * 
	 * @param request
	 * @param brokerId
	 * @param model
	 * @param assister
	 * @param addSupportDoc
	 * @param fileToUpload
	 * @param assisterId
	 * @param documenttype
	 * @return URL for navigation to edit certification status page.
	 * @throws GIException 
	 */
	@RequestMapping(value = "/entity/assisteradmin/uploaddocument", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ASSISTER)
	public String uploadAssisterApplicationDocuments(
			@ModelAttribute(ASSISTER) Assister assister,
			@RequestParam(value = FILE_INPUT, required = false) MultipartFile addSupportDoc,
			@RequestParam(value = "fileToUpload", required = false) String fileToUpload,
			@RequestParam(value = ASSISTER_ID, required = false) Integer assisterId,
			@RequestParam(value = "registrationStatus", required = false) String documenttype,
			HttpServletRequest request) throws GIRuntimeException {

		LOGGER.info("uploadAssisterApplicationDocuments: START");
		LOGGER.info("uploadEntityApplicationDocuments: START");
		
		int documentId = 0;
		try{
		Validate.notNull(assister);
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		assisterResponseDTO.setAssister(assister);
		EntityUtils.checkEntityAssisterAssociation(assisterId.toString(), userService.getLoggedInUser(), assisterResponseDTO );
		
		if ((addSupportDoc == null) || (addSupportDoc.getSize() > UPLOAD_DOCUMENT_ALLOWED_SIZE)) {
			Integer idRequest = (Integer) request.getSession().getAttribute("fileUploadId");
			request.getSession().setAttribute(UPLOADED_DOCUMENT_CHECK, "SIZE_FAILURE");
			return "redirect:/entity/assisteradmin/editcertificationstatus?assisterId="+idRequest;
		}
		LOGGER.debug("Retrieving Assister Document Details REST Call  : ");

		/** Check for XSS Attack - Starts */
		assister.setComments(XssHelper.stripXSS(assister.getComments()));
		/** Check for XSS Attack - Ends */

		
		if (null != fileToUpload) {
			documentId = uploadFile(fileToUpload, addSupportDoc, assisterId,
					documenttype);
		}
		request.getSession().setAttribute(ASSISTER, assister);
		request.getSession().setAttribute(FILENAME,
				addSupportDoc.getOriginalFilename());
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured in uploadAssisterApplicationDocuments : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while uploadAssisterApplicationDocuments : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			
		}

		LOGGER.info("uploadAssisterApplicationDocuments: END");

		return "redirect:/entity/assisteradmin/editcertificationstatus?assisterId="
				+ assisterId + "&documentId=" + documentId;

	}
	
	// uploading the issuer application documents
	/**
	 * Handles the POST request for Upload document
	 * 
	 * @param request
	 * @param brokerId
	 * @param model
	 * @param assister
	 * @param addSupportDoc
	 * @param encryAssisterId
	 * @param documenttype
	 * @param fileToUpload
	 * @return URL for navigation to edit certification status page.
	 * @throws GIRuntimeException 
	 */
	@RequestMapping(value = "/entity/assisteradmin/uploaddocument/{encryAssisterId}", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ASSISTER)
	public void uploadAssisterApplicationDocumentsWithID(
			@ModelAttribute(ASSISTER) Assister assister,
			@RequestParam(value = FILE_INPUT, required = false) MultipartFile addSupportDoc,
			@RequestParam(value = "fileToUpload", required = false) String fileToUpload,
			@PathVariable("encryAssisterId") String encryAssisterId,
			@RequestParam(value = "registrationStatus", required = false) String documenttype,
			HttpServletRequest request, HttpServletResponse response) throws GIRuntimeException {
		
		LOGGER.info("uploadAssisterApplicationDocumentsWithID: START");
		
		try{
		Validate.notNull(assister);
		int assisterId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryAssisterId));
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		assisterResponseDTO.setAssister(assister);
		EntityUtils.checkEntityAssisterAssociation(assister.getId() + "", userService.getLoggedInUser(), assisterResponseDTO);

		String returnString = "";
		
		if (fileToUpload != null && fileToUpload.equalsIgnoreCase(FILE_INPUT)) {
			if (UPLOAD_DOCUMENT_ALLOWED_SIZE > addSupportDoc.getSize()) {
				Integer docId = uploadFile(fileToUpload, addSupportDoc, assisterId,
						documenttype);
				returnString = fileToUpload + "|" + addSupportDoc.getOriginalFilename() + "|" + docId;
			} else {
				returnString = fileToUpload + "|" + addSupportDoc.getOriginalFilename() + "|" + -1;
			}
			Map<String, MultipartFile> documents = new HashMap<String, MultipartFile>();
			documents.put(FILE_INPUT, addSupportDoc);
			request.getSession().setAttribute(FILE_INPUT, documents);
		}		
		
		/** Check for XSS Attack - Starts */
		assister.setComments(XssHelper.stripXSS(assister.getComments()));
		/** Check for XSS Attack - Ends */
		
		response.setContentType("text/html");
		response.getWriter().write(returnString);
		response.flushBuffer();
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while uploading the issuer application documents : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while uploading the issuer application documents : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("uploadAssisterApplicationDocumentsWithID: END");
		return;
	}

	/**
	 * Handles the POST request for Edit Broker information
	 * 
	 * @param documentId
	 * @param response
	 * @return URL for navigation to edit certification status page.
	 * @throws ContentManagementServiceException
	 * @throws IOException
	 */
	@RequestMapping(value = "/entity/assisteradmin/viewAttachment", method = RequestMethod.GET)
	@ResponseBody
	@PreAuthorize("hasPermission(#model, 'EEADMIN_VIEW_ASSISTER')")
	public void viewAttachment(@RequestParam Integer documentId, HttpServletResponse response)
			throws GIRuntimeException {

		LOGGER.info("viewAttachment: START");

		EntityDocuments entityDocuments = null;
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class);
		try {
			if (documentId != null) {
				AssisterRequestDTO enrollmentEntityRequest = new AssisterRequestDTO();
				enrollmentEntityRequest.setModuleId(documentId);
				LOGGER.info(RETRIEVING_ASSISTER_DOCUMENT_DETAILS_REST_CALL_STARTS);
				String assisterResDocJSONStr  = restClassCommunicator.retrieveAssisterDocumentsObjectById(enrollmentEntityRequest);
				LOGGER.info(RETRIEVING_ASSISTER_DOCUMENT_DETAILS_REST_CALL_ENDS);

				
				AssisterResponseDTO assisterResponseDTOObj =  reader.readValue(assisterResDocJSONStr );
				
				entityDocuments = assisterResponseDTOObj.getAssisterDocuments();

			}
			byte[] attachment = null;

			if (entityDocuments != null) {
				AssisterRequestDTO enrollmentEntityRequest = new AssisterRequestDTO();
				enrollmentEntityRequest.setAssisterType(entityDocuments
						.getDocumentName());
				LOGGER.info(RETRIEVING_ASSISTER_DOCUMENT_DETAILS_REST_CALL_STARTS);
				String attachmentStr = restClassCommunicator.retrieveAssisterDocumentsfromECMbyId(enrollmentEntityRequest);
				LOGGER.info(RETRIEVING_ASSISTER_DOCUMENT_DETAILS_REST_CALL_ENDS);

				 
				AssisterResponseDTO enrollmentEntityResponse = reader.readValue (attachmentStr);
				
				attachment = enrollmentEntityResponse.getAttachment();
			} else {
				attachment = "No Document is attached".getBytes();
			}

			if (entityDocuments != null) {
				response.setContentType(entityDocuments.getMimeType().replaceAll("[\n\r]", ""));

				response.addHeader(
						"Content-Disposition",
						"attachment; filename="
								+ entityDocuments.getOrgDocumentName().replaceAll("[\n\r]", ""));

			} else {
				response.setContentType("text/plain");

				response.setContentLength(attachment.length);
			}
			FileCopyUtils.copy(attachment, response.getOutputStream());

		} catch (IOException iOException) {
			LOGGER.error("Unable to show attachment", iOException);
			try {
				FileCopyUtils.copy("UNABLE TO SHOW ATTACHMENT.PLEASE CONTACT ADMIN".getBytes(), response.getOutputStream());
			} catch (IOException ioexception) {
				//e.printStackTrace();
				LOGGER.error("Exception occured in IOException block in viewAttachment  : ", ioexception);
				throw new GIRuntimeException(ioexception, ExceptionUtils.getFullStackTrace(ioexception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing attachment : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing attachments of assister : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("viewAttachment: END");

	}

	// uploading the files into their specific folders
	private int uploadFile(String fileToUpload, MultipartFile file,
			int assisterId, String documenttype) {

		LOGGER.info("uploadFile: START");

		int documentId1 = 0;
		String documentId = null;
		AssisterRequestDTO enrollmentEntityRequestDTO = new AssisterRequestDTO();
		AssisterResponseDTO enrollmentEntityResponse = null;
		AssisterRequestDTO enrollmentEntityRequest = new AssisterRequestDTO();

		try {
			if (assisterId != 0) {

				enrollmentEntityRequest.setModuleId(assisterId);
				enrollmentEntityResponse = retrieveAssisterObjectById(enrollmentEntityRequest);
				enrollmentEntityResponse.getAssister();
			}

			// creating a new file name with the time stamp
			StringBuilder fileNameBuilder = new StringBuilder();
			fileNameBuilder.append(FilenameUtils.getBaseName(file
					.getOriginalFilename()));
			fileNameBuilder.append(GhixConstants.UNDERSCORE);
			fileNameBuilder.append(TimeShifterUtil.currentTimeMillis());
			fileNameBuilder.append(GhixConstants.DOT);
			fileNameBuilder.append(FilenameUtils.getExtension(file
					.getOriginalFilename()));

			// upload file to DMS
			enrollmentEntityRequestDTO.setAttachment(file.getBytes());
			enrollmentEntityRequestDTO.setModuleId(assisterId);
			enrollmentEntityRequestDTO.setAssisterType(fileNameBuilder
					.toString());


			enrollmentEntityResponse = saveAssistersDocumentsOnECM(enrollmentEntityRequestDTO);
			
			documentId = enrollmentEntityResponse.getDocument();

			MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
			String mimeType = mimeTypesMap.getContentType(file
					.getOriginalFilename());

			EntityDocuments entityDocs = new EntityDocuments();
			entityDocs.setCreatedBy(userService.getLoggedInUser().getEmail());
			entityDocs.setCreatedDate(new TSDate());
			entityDocs.setDocumentName(documentId);
			entityDocs.setDocumentType(documenttype);
			entityDocs.setMimeType(mimeType);
			entityDocs.setOrgDocumentName(file.getOriginalFilename());

			enrollmentEntityRequestDTO.setEntityDocuments(entityDocs);
			
			enrollmentEntityResponse = saveAssisterDocument(enrollmentEntityRequestDTO);

			EntityDocuments entityDocuments = enrollmentEntityResponse
					.getAssisterDocuments();

			documentId1 = entityDocuments.getID();

		} catch (Exception exception) {
			LOGGER.error("Fail to upload Assister Document " , exception);
		}

		LOGGER.info("uploadFile: END");

		return documentId1;
	}

	/**
	 * Method to retrieve Assister instance by identifier.
	 * 
	 * @param assisterRequest The AssisterRequestDTO instance.
	 * @return assisterResponse The AssisterResponseDTO instance.
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private AssisterResponseDTO retrieveAssisterObjectById(AssisterRequestDTO assisterRequest) throws JsonProcessingException, IOException {
		String assisterObjStr;
		AssisterResponseDTO assisterResponse;
		
		LOGGER.info(RETRIEVING_ASSISTER_DOCUMENT_DETAILS_REST_CALL_STARTS);

		// Retrieving Assister Details
		assisterObjStr = restClassCommunicator.retrieveAssisterObjectById(assisterRequest);
		LOGGER.info(RETRIEVING_ASSISTER_DOCUMENT_DETAILS_REST_CALL_ENDS);
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class);
		
		assisterResponse =  reader.readValue(assisterObjStr);
		return assisterResponse;
	}

	/**
	 * Method to store Assister documents on ECM.
	 * 
	 * @param assisterRequestDTO The AssisterRequestDTO instance.
	 * @return assisterResponseDTO The AssisterResponseDTO instance.
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private AssisterResponseDTO saveAssistersDocumentsOnECM(AssisterRequestDTO assisterRequestDTO) throws JsonProcessingException, IOException {
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class);
		LOGGER.info(RETRIEVING_ASSISTER_DOCUMENT_DETAILS_REST_CALL_STARTS);
		// Saving Assister Document on ECM
		String strAssisterResponseDTO = restClassCommunicator.saveAssisterDocumentsOnECM(assisterRequestDTO);
		LOGGER.info(RETRIEVING_ASSISTER_DOCUMENT_DETAILS_REST_CALL_ENDS);
		AssisterResponseDTO assisterResponseDTO = reader.readValue(strAssisterResponseDTO);
		return assisterResponseDTO;
	}

	/**
	 * Method to make ReST Call to store Assister Document.
	 * 
	 * @param assisterRequestDTO The AssisterRequestDTO instance.
	 * @return assisterResponseDTO The AssisterResponseDTO instance.
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private AssisterResponseDTO saveAssisterDocument(AssisterRequestDTO assisterRequestDTO) throws JsonProcessingException, IOException {
		AssisterResponseDTO assisterResponseDTO;
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class);
		LOGGER.info(RETRIEVING_ASSISTER_DOCUMENT_DETAILS_REST_CALL_STARTS);
		String strAssisterResponseDTO = restClassCommunicator.saveAssisterDocument(assisterRequestDTO);
		LOGGER.info(RETRIEVING_ASSISTER_DOCUMENT_DETAILS_REST_CALL_ENDS);
		assisterResponseDTO = reader.readValue(strAssisterResponseDTO);
		return assisterResponseDTO;
	}

	private List<Site> getListOfEnrollmentEntitySitesBySiteType(
			EnrollmentEntity enrollmentEntity, String siteType) throws GIRuntimeException {

		LOGGER.info("getListOfEnrollmentEntitySitesBySiteType: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntity);
		enrollmentEntityRequestDTO.setSiteType(siteType);

		try {
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.debug("Retrieving Enrollment Entity List of Sites REST Call Starts :");
			String listOfSitesResponse = restClassCommunicator
					.getListOfEnrollmentEntitySitesBySiteType(requestJSON);
			enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(listOfSitesResponse);
			LOGGER.debug("Retrieving Enrollment Entity List of Sites REST Call Ends :");
		}catch(GIRuntimeException giexception){
			LOGGER.error(
					"GIRuntimeException occurred while Retrieving Enrollment Entity List of Sites : ",
					giexception);
			throw giexception;
		} catch (Exception exception) {
			LOGGER.error(
					"Exception occurred while Retrieving Enrollment Entity List of Sites in getListOfEnrollmentEntitySitesBySiteType method : ",
					exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getListOfEnrollmentEntitySitesBySiteType: END");

		return enrollmentEntityResponseDTO.getSiteList();
	}

	/**
	 * Verifies if logged in user is with role ENTITY_ADMIN
	 * 
	 * @return boolean true/false
	 * @throws InvalidUserException
	 *             exception
	 */
	private boolean isEntityAdmin() throws InvalidUserException {

		return userService.hasUserRole(userService.getLoggedInUser(),
				RoleService.ENTITY_ADMIN_ROLE);
	}

	private List<String> getEducationList() {

		LOGGER.info("getEducationList: START");

		List<String> educationList = new ArrayList<String>();
		educationList.add("Up to 8th Grade");
		educationList.add("Some High School");
		educationList.add("High School Graduate");
		educationList.add("Some College");
		educationList.add("College Graduate");
		educationList.add("Two Year Associate Degree");
		educationList.add("Inapplicable/Not Ascertained");

		LOGGER.info("getEducationList: END");

		return educationList;
	}

	private void setLanguageNames(List<LookupValue> listOfLanguagesSpoken,
			List<LookupValue> listOfLanguagesWritten, Model model) {

		LOGGER.info("setLanguageNames: START");

		List<String> listOfNames = new ArrayList<String>();
		List<String> listWrittenNames = new ArrayList<String>();
		for (LookupValue language : listOfLanguagesSpoken) {
			listOfNames.add(language.getLookupValueLabel());
		}
		for (LookupValue language : listOfLanguagesWritten) {
			listWrittenNames.add(language.getLookupValueLabel());
		}
		model.addAttribute(LANGUAGE_NAMES, listOfNames);
		model.addAttribute(LANGUAGE_WRITTEN_NAMES, listWrittenNames);

		LOGGER.info("setLanguageNames: END");
	}

	private EnrollmentEntity getEnrollmentEntityByUserId(
			Integer enrollmentEntityUserId) throws GIRuntimeException {

		LOGGER.info("getEnrollmentEntityByUserId: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = null;

		try {
			enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setModuleId(enrollmentEntityUserId);

			LOGGER.debug("Retrieving Enrollment Entity By User Id REST Call Starts : ");
			String enrollmentEntityResponse = restClassCommunicator
					.retrieveEnrollmentEntityObjectByUserId(enrollmentEntityRequestDTO);
			enrollmentEntityResponseDTO =JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollmentEntityResponse);
			LOGGER.debug("Retrieving Enrollment Entity By User Id REST Call Ends :");
		}catch(GIRuntimeException giexception){
			LOGGER.error(
					"GIRuntimeException occurred while Retrieving Enrollment Entity By User Id : ",
					giexception);
			throw giexception;
		}catch (Exception exception) {
			LOGGER.error(
					"Exception occurred while Retrieving Enrollment Entity By User Id in getEnrollmentEntityByUserId method: ",
					exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getEnrollmentEntityByUserId: END");

		return enrollmentEntityResponseDTO.getEnrollmentEntity();
	}

	private AssisterResponseDTO setAssisterAndLanguages(Model model,
			Integer assisterID) throws JsonProcessingException, IOException {

		LOGGER.info("setAssisterAndLanguages: START");

		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setId(assisterID);

		AssisterResponseDTO assisterResponseDTO = getAssisterDetail(assisterRequestDTO);

		model.addAttribute(ASSISTER, assisterResponseDTO.getAssister());
		model.addAttribute(ASSISTER_LANGUAGES,
				assisterResponseDTO.getAssisterLanguages());

		setContactNumber(model, assisterResponseDTO.getAssister());

		LOGGER.info("setAssisterAndLanguages: END");

		return assisterResponseDTO;
	}

	private void setOtherSpokenLanguage(AssisterLanguages assisterLanguages,
			List<LookupValue> listOfLanguagesSpoken, Model model) {

		LOGGER.info("setOtherSpokenLanguage: START");

		String[] spokenLanguages = assisterLanguages.getSpokenLanguages()
				.split(",");
		List<String> listOfNames = new ArrayList<String>();
		for (LookupValue language : listOfLanguagesSpoken) {
			listOfNames.add(language.getLookupValueLabel());
		}
		StringBuilder strSpoken = new StringBuilder();
		for (String language : spokenLanguages) {
			if (!"".equals(language) && !listOfNames.contains(language)) {
				if (strSpoken.length() == 0) {
					strSpoken.append(language);
				} else {
					strSpoken.append(",");
					strSpoken.append(language);
				}
			}
		}
		if (strSpoken.length() != 0) {
			model.addAttribute(OTHER_SPOKEN_LANGUAGE, strSpoken);
		} else {
			model.addAttribute(OTHER_SPOKEN_LANGUAGE, null);
		}

		LOGGER.info("setOtherSpokenLanguage: END");
	}

	private void setOtherWrittenLanguage(AssisterLanguages assisterLanguages,
			List<LookupValue> listOfLanguagesWritten, Model model) {

		LOGGER.info("setOtherWrittenLanguage: START");

		String[] writtenLanguages = assisterLanguages.getWrittenLanguages()
				.split(",");
		List<String> listOfNames = new ArrayList<String>();
		StringBuilder strWritten = new StringBuilder();
		for (LookupValue language : listOfLanguagesWritten) {
			listOfNames.add(language.getLookupValueLabel());
		}
		for (String language : writtenLanguages) {
			if (!"".equals(language) && !listOfNames.contains(language)) {
				if (strWritten.length() == 0) {
					strWritten.append(language);
				} else {
					strWritten.append(",");
					strWritten.append(language);
				}
			}
		}
		if (strWritten.length() != 0) {
			model.addAttribute(OTHER_WRITTEN_LANGUAGE, strWritten);
		} else {
			model.addAttribute(OTHER_WRITTEN_LANGUAGE, null);
		}

		LOGGER.info("setOtherWrittenLanguage: END");
	}

	private Site getSiteById(int siteId) throws GIRuntimeException{

		LOGGER.info("getSiteById: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();

		Site site = new Site();
		site.setId(siteId);

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setSite(site);

		try {
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.debug("Retrieving Site By ID REST Call Starts : ");
			String listOfSitesResponse = restClassCommunicator
					.getSiteById(requestJSON);
			enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(listOfSitesResponse);
			LOGGER.debug("Retrieving Site By ID REST Call Ends : ");
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occurred while Retrieving Site By ID : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occurred while Retrieving Site By ID in getSiteById method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getSiteById: END");

		return enrollmentEntityResponseDTO.getSite();
	}

	private AssisterResponseDTO saveAssisterDetails(
			AssisterRequestDTO assisterRequestDTO) throws GIRuntimeException {

		LOGGER.info("saveAssisterDetails: START");

		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		String request = null;
		String assisterDetailsResponse = null;
		Assister assister = null;
		try {
			
			//get logged in user
			user= userService.getLoggedInUser();
			//get assister object
			assister = assisterRequestDTO.getAssister();
			//check for update or new record operation
			if(assister.getId() == 0){
				// new record
				assister.setCreatedBy(user.getId());
			}else{
				//update
				assister.setUpdatedBy(user.getId());
			}

			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class);
			request = writer.writeValueAsString(assisterRequestDTO);

			LOGGER.debug("Save Assister Detail REST Call Starts :");
			assisterDetailsResponse = restClassCommunicator
					.saveAssisterDetails(request);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
			LOGGER.debug("Save Assister Detail REST Call Ends :");
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving assister details : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving assister details : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("saveAssisterDetails: END");

		return assisterResponseDTO;
	}

	/**
	 * Method to validate languages entered by Assister
	 * 
	 * @param otherSpokenLanguage
	 * @return Boolean <code>true</code> if language allowed else
	 *         <code>false</code>.
	 */
	@RequestMapping(value = "/entity/assister/editassisterinformation/checkLanguagesSpokenForAsister")
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ASSISTER)
	public Boolean checkLanguagesSpokenForAssister(@RequestParam String otherSpokenLanguage) {

		LOGGER.info("checkLanguagesSpokenForAssister: START");
		LOGGER.info("spoken language validation start here");

		List<String> languageList = null;
		List<String> countiesServedlist = new ArrayList<String>();
		String otherSpokenLanguageWithoutSpace = null;
		languageList = lookupService.populateLanguageNames("");

		if (null == languageList) {
			languageList = new ArrayList<String>();
		}

		otherSpokenLanguageWithoutSpace = otherSpokenLanguage.trim();
		countiesServedlist = Arrays.asList(otherSpokenLanguageWithoutSpace
				.split(","));
		for (String countiesStr : countiesServedlist) {
			if (!languageList.contains(countiesStr.trim())) {

				LOGGER.info("checkLanguagesSpokenForAssister: END");

				return false;
			}
		}

		LOGGER.info("checkLanguagesSpoken: END");

		return true;
	}

	/**
	 * Method to validate languages entered by Assister
	 * 
	 * @param otherWrittenLanguage
	 *            The other written language.
	 * @return Boolean <code>true</code> if language allowed else
	 *         <code>false</code>.
	 */
	@RequestMapping(value = "/entity/assister/editassisterinformation/checkLanguagesWrittenForAssister")
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ASSISTER)
	public Boolean checkLanguagesWrittenForAssister(@RequestParam String otherWrittenLanguage) {
		LOGGER.info("checkLanguagesWrittenForAssister: START");
		LOGGER.info("spoken language validation start here");

		List<String> languageList = null;
		List<String> countiesServedlist = new ArrayList<String>();
		String otherWrittenLanguageWithoutSpace = null;
		languageList = lookupService.populateLanguageNames("");

		if (null == languageList) {
			languageList = new ArrayList<String>();
		}

		otherWrittenLanguageWithoutSpace = otherWrittenLanguage.trim();
		countiesServedlist = Arrays.asList(otherWrittenLanguageWithoutSpace
				.split(","));
		for (String countiesStr : countiesServedlist) {
			if (!languageList.contains(countiesStr.trim())) {

				LOGGER.info("checkLanguagesWrittenForAssister: END");

				return false;
			}
		}

		LOGGER.info("checkLanguagesWrittenForAssister: END");

		return true;

	}
	
	/**
	 * Handles the GET/POST request for sending activation link.
	 *
	 * @param id
	 * @throws GIException 
	 */
	@RequestMapping(value = "/entity/assisteradmin/sendactivationlink/{id}", method = {RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_EEADMIN_EDIT_ASSISTER)
	public String sendActivationLink(@PathVariable("id") String encryptedAssisterId) throws GIException{
		LOGGER.info("Sending Email to employer for aend activation link: " );
		Integer assisterId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedAssisterId));
		return generateLinkAndRedirect(assisterId, null);
	}
	
	/* Get Method to display assister status for view only */
	/**
	 * Get Method to display assister status for view only.
	 *
	 * @param model
	 * @param request
	 * @param encryAssisterId
	 * @throws GIRuntimeException 
	 */
	@RequestMapping(value = "/entity/entityadmin/assisterstatus", method = RequestMethod.GET)
	@PreAuthorize(EntityUtils.HAS_PERMISSION_MODEL_ENTITY_VIEW_ASSISTER)
	public String displayassisterStatus(Model model, HttpServletRequest request,
			@RequestParam(ASSISTER_ID) String encryAssisterId) throws GIRuntimeException {

		LOGGER.info("displayassisterStatus: START");
		try {
		String assisterId = encryAssisterId != null && !"".equals(encryAssisterId)?ghixJasyptEncrytorUtil.decryptStringByJasypt(encryAssisterId):null;
		
			user = userService.getLoggedInUser();
			LOGGER.info("Inside displayassisterStatus");

			if (assisterId != null && !EntityUtils.isEmpty(assisterId)) {

				AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();

				assisterRequestDTO.setId(Integer.valueOf(assisterId));


				String assisterDetailsResponse = restClassCommunicator
						.retrieveAssistersDetailsForExchangeAdmin(assisterRequestDTO);

				AssisterResponseDTO assisterResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
				EntityUtils.checkEntityAssisterAssociation(assisterId, user, assisterResponseDTO);
				Assister assister = assisterResponseDTO.getAssister();

				List<Map<String, Object>> assisterActivityStatusHistory = null;
				if (assister != null) {
					AssisterRequestDTO enrollmentEntityRequestDTO = new AssisterRequestDTO();
					enrollmentEntityRequestDTO.setModuleId(assister.getId());
					
					String assisterResponseJSONStr = restClassCommunicator.retrieveAssisterActivityDetailsById(enrollmentEntityRequestDTO);
					
					ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterResponseDTO.class);
					AssisterResponseDTO assisterResponseDTOLocal = reader.readValue(assisterResponseJSONStr );
					
					assisterActivityStatusHistory = assisterResponseDTOLocal.getAssisterEntityHistory();

				}
				model.addAttribute(ASSISTER, assister);
				model.addAttribute(ACTIVITY_STATUS_HISTORY, assisterActivityStatusHistory);
				LOGGER.info("Display assister activity Status ends here");
				assisterNavigationMap(request, assister, model);
			}

		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while displaying assister status : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while displaying assister status in displayassisterStatus : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("displayassisterStatus: END");

		return "entity/entityadmin/assisterstatus";
	}


	private String generateLinkAndRedirect(Integer id, String redirectURL)
			throws GIException {
		StringBuilder redirectUrl = new StringBuilder();
			
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setId(id);
		AssisterResponseDTO response;
		try {
			response = getAssisterDetail(assisterRequestDTO);
		} catch (Exception e) {
			throw new GIRuntimeException("Exception occured while fecthing assister details.");
		} 
		Assister assister = response.getAssister();
		String activationLinkResult = entityService.generateAssisterActivationLink(assister);
		
		if(redirectURL != null) {
			lastVisitedURL = redirectURL;
		}
		 
		 redirectUrl.append("redirect:" + lastVisitedURL + "?assisterId=");
		 redirectUrl.append(ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(assister.getId())));
		 
		 if(activationLinkResult != null) {				 
			 redirectUrl.append(ACTIVATION_RESULT);
			 redirectUrl.append(activationLinkResult);	
		 }
		return redirectUrl.toString();
	}		
	
	private String loadLanguages(){
		JsonArray languageJsonArr = null;
        LOGGER.info("getLanguageList : START");
        String jsonLangData = "";
         
	    List languageSpokenList = null;
		try{ 
	    languageSpokenList = lookupService.populateLanguageNames("");
	    languageJsonArr = new JsonArray();
	    for(int i=0;i<languageSpokenList.size();i++)
	    {
	    	String lan = EntityUtils.initializeAndUnproxy(languageSpokenList.get(i));
	    	if(lan!=null){
	    		languageJsonArr.add(new JsonPrimitive(lan));
	    	}
	    }
	    
	    jsonLangData = platformGson.toJson(languageJsonArr);
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while loading languages : ", giexception);
			throw giexception;
		}
		catch(Exception exception)
		{
			LOGGER.error("Exception occured while loading languages in loadLanguages method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
        LOGGER.info("getLanguageList : END");
        return jsonLangData;
	}
	
	/*
	 * Get Method to display assister profile for editing purpose providing user
	 * option to select Certified Status
	 */

	@RequestMapping(value = "/entity/assister/editassisterprofile", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER)
	public String displayssisterStatusForEdit(
			Model model, @RequestParam(value = ASSISTER_ID, required = false) String assisterId,
			@RequestParam(value=ENTITY_ID,required=false)String entityId) throws GIRuntimeException
	{

		LOGGER.info("displayssisterProfileForEdit: START");
		try {
		 assisterId=assisterId != null && !"".equals(assisterId)? ghixJasyptEncrytorUtil.decryptStringByJasypt(assisterId):null;
		 entityId= ghixJasyptEncrytorUtil.decryptStringByJasypt(entityId);
	
		model.addAttribute(PAGE_TITLE, EDIT_ASSISTER_INFO);
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		AssisterResponseDTO assisterResponseDTO = null;
		String showPostalMailOption=FALSE;
		List<Site> listOfSites = new ArrayList<Site>();
		
			AccountUser currentUser = userService.getLoggedInUser();
			showPostalMailOption=EntityUtils.isAllaowMailNotices();
			EnrollmentEntity enrollmentEntity = retrieveEnrollmentEntityForUserId(entityId);
			
			Validate.notNull(assisterId);
			
			assisterRequestDTO.setId(Integer.valueOf(assisterId));

			assisterResponseDTO = getAssisterDetail(assisterRequestDTO);
			
			LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_RESPONSE );
			EntityUtils.checkEntityAssisterAssociation(assisterId, currentUser, assisterResponseDTO);
			Assister assister = assisterResponseDTO.getAssister();
			model.addAttribute(ASSISTER, assister);
			model.addAttribute(ENROLLMENTENTITY, assister.getEntity());
			model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
			model.addAttribute(SHOW_POSTAL_MAIL,showPostalMailOption);
			String languageDetails =  loadLanguages();
			
			model.addAttribute(LANGUAGES_LIST, languageDetails);
			
			setContactNumber(model, assister);
			
			if (assisterId != null) {
				if (enrollmentEntity == null) {
					enrollmentEntity = retrieveEnrollmentEntity(assister);
				}

				determineWorkSitesForAnEnrollmentEntity(model, listOfSites, enrollmentEntity);

				List<LookupValue> listOfLanguagesSpoken = populateEntitySpokenLanguages();
				
				model.addAttribute(LIST_OF_LANGUAGES_SPOKEN, listOfLanguagesSpoken);

				List<LookupValue> listOfLanguagesWritten = populateEntityWrittenLanguages();
				
				model.addAttribute(LIST_OF_LANGUAGES_WRITTEN, listOfLanguagesWritten);

				model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());

				model.addAttribute(LIST_OF_EDUCATIONS, getEducationList());

				AssisterResponseDTO response = setAssisterAndLanguages(model, Integer.valueOf(assisterId));
				AssisterLanguages assisterLanguages = response.getAssisterLanguages();
				setOtherSpokenLanguage(assisterLanguages, listOfLanguagesSpoken, model);
				setOtherWrittenLanguage(assisterLanguages, listOfLanguagesWritten, model);
				
				// Set language names for other language text box checking
				setLanguageNames(listOfLanguagesSpoken, listOfLanguagesWritten, model);
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured in displayssisterStatusForEdit : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while getting displayssisterStatusForEdit: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("editAssisterInformation: END");

		LOGGER.info("displayssisterProfileForEdit: END");

		return "entity/assister/editassisterprofile";
	}
	
	/*
	 * Post Method to update assister profile information for Certified CEC
	 */
	@RequestMapping(value = "/entity/assister/editassisterprofile", method = RequestMethod.POST)
	public String updateAssisterProfileInformation(
			Model model,
			@ModelAttribute(ASSISTER) Assister assister,BindingResult assisterBindingResult,
			@RequestParam(value = ASSISTER_ID, required = false) String assisterId,
			@RequestParam(value = "assisterLanguageId", required = false) String assisterLanguageId,
			@ModelAttribute(ASSISTER_LANGUAGES)@Validated  AssisterLanguages assisterLanguages,BindingResult languageBindingResult,
			@RequestParam(value = FILE_INPUT, required = false) MultipartFile fileInput,
			@RequestParam(value=ENTITY_ID,required=false)String entityId) throws GIRuntimeException {

		LOGGER.info("updateAssisterProfileInformation: START");

		LOGGER.info(EDIT_ASSISTER_INFO);
		AssisterResponseDTO assisterResponseDTO = null;
		
		try {
		/** Check for XSS Attack - Starts */
		checkXSSAttackForUpdateAssisterInformation(assister, assisterLanguages);		
		/** Check for XSS Attack - Ends */

		model.addAttribute(PAGE_TITLE, EDIT_ASSISTER_INFO);
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
	
		EnrollmentEntity enrollmentEntity = null;
		int userId = 0;
		
				user = userService.getLoggedInUser();
				if (user != null) {
					userId = user.getId();
					enrollmentEntity = getEnrollmentEntityByUserId(userId);
				}

				// Setting Assister Id to update Registration Information (Required in
				// case of only Edit Operation)
				if (EntityUtils.isNullOrBlank(assisterId)) {
					assister.setId(Integer.parseInt(assisterId));
				}

				// Setting Assister Languages Id to update Languages (Required in case
				// of only Edit Operation)
				if (EntityUtils.isNullOrBlank(assisterLanguageId)) {
					assisterLanguages.setId(Integer.parseInt(assisterLanguageId));
				}
				
			assister.setId(Integer.parseInt(assisterId));
			assisterRequestDTO.setAssister(assister);
			assisterRequestDTO.setEnrollmentEntityId(Integer.parseInt(entityId));
			assisterRequestDTO.setUserId(userId);
			
			// Setting Assister Languages Id to update Languages (Required in
			// case
			// of only Edit Operation)
			if (assisterLanguageId != null && !"".equals(assisterLanguageId)) {
				assisterLanguages.setId(Integer.parseInt(assisterLanguageId));
			}

			// Associating EE to Assister
			assister.setEntity(enrollmentEntity);
			assisterRequestDTO.setAssister(assister);
			assisterRequestDTO.setAssisterLanguages(assisterLanguages);
			assisterRequestDTO.setCertificationNumberUpdate(true);
			//validate after setting primary site and secondary site
			validateAssisterProfileUpdate(assisterBindingResult,languageBindingResult,assister);//Passing "No" will not check validation for certification number.
			
			// Saving Assister details
			assisterResponseDTO = saveAssisterDetails(assisterRequestDTO);

			// saving assister photo
			if (assisterResponseDTO != null && assisterResponseDTO.getAssister() != null)
			{
				entityService.saveAssisterPhoto(assisterResponseDTO.getAssister().getId(), fileInput);
			}
			
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while updating assister profile information for Certified CEC : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while updating assister profile information in updateAssisterProfileInformation method", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		Validate.notNull(assisterResponseDTO);
		Validate.notNull(assisterResponseDTO.getAssister());		
		
		LOGGER.info("updateAssisterProfileInformation: END");

		return "redirect:/entity/assisteradmin/viewassisterprofile?assisterId="
				+ ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(assisterResponseDTO.getAssister().getId()))+ "&entityId=" + ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(assisterResponseDTO.getAssister().getEntity().getId()));
	}

	

	private Map<String, String> getTemplateDataMap(Map<String, Object> templateData) {
		Map<String, String> templateDataModified = new HashMap<String, String>();
	    try{
		Iterator<Entry<String, Object>> it = templateData.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
	        if(pairs.getValue() != null) {
	        	templateDataModified.put(pairs.getKey(), pairs.getValue().toString());
	        }
	    }
	    }catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured in getTemplateDataMap : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured in getTemplateDataMap : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		return templateDataModified;
	}	
	
	private String getContentTypeFromBytes(byte[] b) throws IOException,Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        java.io.InputStream is = null;
        String contentType = null;
        ByteArrayInputStream bais = null;
        
		try {
			//is = fileInput.getInputStream();
	        //byte[] b = new byte[100];
	        //int read = is.read(b, 0, b.length);
	        baos.write(b);
	        baos.close();
	        bais = new ByteArrayInputStream(baos.toByteArray());
			contentType = URLConnection.guessContentTypeFromStream(bais);
			
			if(contentType == null) {
				contentType = "application/octet-stream";
			}
		} 
		catch (Exception e) { 
			LOGGER.debug("Error finding mime type from content");
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			IOUtils.closeQuietly(baos);
			IOUtils.closeQuietly(bais);
			IOUtils.closeQuietly(is);
		}
		
		return contentType;
	}	
	
	@RequestMapping(value = "/entity/assisteradmin/exportAssisterList", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'EEADMIN_MANAGE_ASSISTER')")
	@ResponseBody
	public void exportAssisterListAsExcel(HttpServletRequest request, HttpServletResponse response, Model model) {
		LOGGER.info("exportAssisterListAsExcel: START ");
		final char CHAR_NEW_LINE = '\n', CHAR_COMMA = ',', CHAR_HYPHEN = '-';
		StringBuffer strWriter;
		
		AssisterResponseDTO assisterResponseDTO = null;

		try {
			String assisterExportResponse = restClassCommunicator.retrieveAssisterExportList();
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterExportResponse);
			
			if(assisterResponseDTO!=null && assisterResponseDTO.getAssisterExportDTOList()!=null && assisterResponseDTO.getAssisterExportDTOList().size()>0){
				StringBuffer address = null;
				strWriter = new StringBuffer();
				strWriter.append("CEC First Name,CEC Last Name,Entity Name,CEC Number,CEC Renewal Date,Certification Status,Mailing Address,Primary Phone Number,CEC Email Address,Preferred Method of Communication,Number of Current Active Delegations,Number of Current Pending Delegations,Number of Current Inactive Delegations");
				strWriter.append(CHAR_NEW_LINE);

				List<AssisterExportDTO> assisterExportDTOs = assisterResponseDTO.getAssisterExportDTOList();

				for(AssisterExportDTO assisterExportDTO: assisterExportDTOs) {
					strWriter.append(StringEscapeUtils.escapeCsv(assisterExportDTO.getFirstName())).append(CHAR_COMMA);
					strWriter.append(StringEscapeUtils.escapeCsv(assisterExportDTO.getLastName())).append(CHAR_COMMA);
					strWriter.append(StringEscapeUtils.escapeCsv(assisterExportDTO.getEntityName())).append(CHAR_COMMA);
					
					strWriter.append(assisterExportDTO.getAssisterNumber()).append(CHAR_COMMA);
					
					if(null != assisterExportDTO.getRecertificationDate()) {
				       strWriter.append(StringEscapeUtils.escapeCsv(assisterExportDTO.getRecertificationDate()+""));
				    }
					strWriter.append(CHAR_COMMA);
					
					if(StringUtils.isNotBlank(assisterExportDTO.getCertificationStatus())) {
						strWriter.append(assisterExportDTO.getCertificationStatus());
					}
					strWriter.append(CHAR_COMMA);
					
					address = new StringBuffer();

					if(StringUtils.isNotBlank(assisterExportDTO.getAddress1())) {
						address.append(assisterExportDTO.getAddress1());
					}

					if(StringUtils.isNotBlank(assisterExportDTO.getAddress2())) {
						address.append((", ")).append(assisterExportDTO.getAddress2());
					}

					final String STR_SPACE = " ";
					if(StringUtils.isNotBlank(assisterExportDTO.getCity())) {
						address.append(STR_SPACE).append(assisterExportDTO.getCity());
					}

					if(StringUtils.isNotBlank(assisterExportDTO.getState())) {
						address.append(STR_SPACE).append(assisterExportDTO.getState());
					}

					if(StringUtils.isNotBlank(assisterExportDTO.getZip())) {
						address.append(STR_SPACE).append(assisterExportDTO.getZip());
					}
					
					strWriter.append(StringEscapeUtils.escapeCsv(address.toString()));
					strWriter.append(CHAR_COMMA);

					if(StringUtils.isNotBlank(assisterExportDTO.getPrimaryPhoneNumber())) {
						strWriter.append(assisterExportDTO.getPrimaryPhoneNumber().substring(0, 3)).append(CHAR_HYPHEN);
						strWriter.append(assisterExportDTO.getPrimaryPhoneNumber().substring(3, 6)).append(CHAR_HYPHEN);
						strWriter.append(assisterExportDTO.getPrimaryPhoneNumber().substring(6, 10));
					}
					strWriter.append(CHAR_COMMA);
					
					if(StringUtils.isNotBlank(assisterExportDTO.getEmailAddress())) {
						strWriter.append(assisterExportDTO.getEmailAddress());
					}
					strWriter.append(CHAR_COMMA);

					if(StringUtils.isNotBlank(assisterExportDTO.getPrefMethodOfCommunication())) {
						strWriter.append(assisterExportDTO.getPrefMethodOfCommunication());
					}
					strWriter.append(CHAR_COMMA);

					strWriter.append(assisterExportDTO.getPendingRequestsCount()).append(CHAR_COMMA);
					strWriter.append(assisterExportDTO.getActiveRequestsCount()).append(CHAR_COMMA);
					strWriter.append(assisterExportDTO.getInactiveRequestsCount());
					
					strWriter.append(CHAR_NEW_LINE);
				}
			} else {
				strWriter = new StringBuffer("No Assisters found");
				response.setContentLength(strWriter.toString().length());
			}

			strWriter.trimToSize();
			response.setContentType("text/csv");
			response.addHeader("Content-Disposition", "attachment; filename=Download_CEC_List.csv");
			FileCopyUtils.copy(strWriter.toString().getBytes(), response.getOutputStream());
		}
		catch(GIRuntimeException runtimeException) {
			LOGGER.error("Runtime exception occurred while exporting Assister list as Excel", runtimeException);
			throw runtimeException;
		}
		catch(Exception exception) {
			LOGGER.error("Exception occurred while exporting Assister list as Excel", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			LOGGER.info("exportAssisterListAsExcel: END ");
		}
	}
}
