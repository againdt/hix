package com.getinsured.hix.batch.enrollment.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.slcsp.service.BenchMarkPremiumService;
import com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO;
import com.getinsured.hix.enrollment.repository.IEnrollmentPremiumRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.model.enrollment.EnrollmentPremium;
import com.getinsured.iex.ssap.repository.AptcHistoryRepository;

@Service("enrlPopulateSLCSPAmtForNFAppService")
@Transactional
public class EnrlPopulateSLCSPAmtForNFAppServiceImpl implements EnrlPopulateSLCSPAmtForNFAppService {
	
	private IEnrollmentRepository iEnrollmentRepository;
	
	private BenchMarkPremiumService benchMarkPremiumService;
	
	private IEnrollmentPremiumRepository iEnrollmentPremiumRepository;
	
	private AptcHistoryRepository aptcHistoryRepository;

	@Autowired
	public EnrlPopulateSLCSPAmtForNFAppServiceImpl(IEnrollmentRepository iEnrollmentRepository,
			BenchMarkPremiumService benchMarkPremiumService, IEnrollmentPremiumRepository iEnrollmentPremiumRepository, AptcHistoryRepository aptcHistoryRepository) {
		this.iEnrollmentRepository = iEnrollmentRepository;
		this.benchMarkPremiumService = benchMarkPremiumService;
		this.iEnrollmentPremiumRepository = iEnrollmentPremiumRepository;
		this.aptcHistoryRepository = aptcHistoryRepository;
	}

	@Override
	public void processSLCSPAmountForNFApp(List<Long> ssapApplicationIdList, String coverageYear) {
		List<EnrollmentMemberDataDTO> enrollmentMemberDataDTOList = iEnrollmentRepository.getSSAPandEnrlIdbySsapId(ssapApplicationIdList,coverageYear);
		getBenchMarkPremiumAmt(enrollmentMemberDataDTOList);
	}

	@Override
	public void processSLCSPAmountForNFApp(String coverageYear) {
		List<EnrollmentMemberDataDTO> enrollmentMemberDataDTOList = iEnrollmentRepository.getSSAPApplicationIdsForNFEnrollment(coverageYear);
		getBenchMarkPremiumAmt(enrollmentMemberDataDTOList);
	}
	
	private void getBenchMarkPremiumAmt(List<EnrollmentMemberDataDTO> enrollmentMemberDataDTOList) {
		Float slcspAmt = null;
		Map<Long, Float> ssapSLCSPMap = new HashMap<Long, Float>();
		if (enrollmentMemberDataDTOList != null && !enrollmentMemberDataDTOList.isEmpty()) {
			for (EnrollmentMemberDataDTO enrollmentMemberDataDto : enrollmentMemberDataDTOList) {
				if (enrollmentMemberDataDto.getSsapApplicationID() != null && enrollmentMemberDataDto.getEnrollmentID() != null) {
					boolean isAPTCHistoryUpdate = false;
					Long ssapApplicationId = enrollmentMemberDataDto.getSsapApplicationID();
					Integer enrollmentId = enrollmentMemberDataDto.getEnrollmentID();
					if (!ssapSLCSPMap.containsKey(ssapApplicationId)) {
						slcspAmt = benchMarkPremiumService.getSLCSPAmountForApplicationId(ssapApplicationId);
						ssapSLCSPMap.put(ssapApplicationId, slcspAmt);
					} else {
						slcspAmt = ssapSLCSPMap.get(ssapApplicationId);
						isAPTCHistoryUpdate = true;
					}
					if (slcspAmt != null) {
						iEnrollmentRepository.updateSLCSPAmtForEnrollment(slcspAmt, enrollmentId);
						if (!isAPTCHistoryUpdate) {
							aptcHistoryRepository.updateSLCSPAmtForSSAPApplication(BigDecimal.valueOf(slcspAmt).setScale(2, BigDecimal.ROUND_HALF_UP),ssapApplicationId);
						}
					}
					List<EnrollmentPremium> enrollmentPremiumList = iEnrollmentPremiumRepository.findByEnrollmentId(enrollmentId);
					if (slcspAmt != null && enrollmentPremiumList != null && !enrollmentPremiumList.isEmpty()) {
						for (EnrollmentPremium enrollmentPremium : enrollmentPremiumList) {
							if (enrollmentPremium.getNetPremiumAmount() != null) {
								enrollmentPremium.setSlcspPremiumAmount(slcspAmt);
							}
						}
						iEnrollmentPremiumRepository.save(enrollmentPremiumList);
					}
				}
			}
		}
	}

}
