package com.getinsured.hix.indportal.dto.dm.application;

public class Family {
	private Integer babyDueQuantity;
	private Boolean claimsDependentIndicator;
	 private Boolean claimingTaxFilerNotOnApplicationIndicator;
	 private Boolean fosterCareIndicator;
	 private Integer fosterCareEndAge;
	 private Boolean medicaidDuringFosterCareIndicator;
	 private String fosterCareState;
	 private Boolean medicaidFamilyNotProvidedIndicator;
	private Boolean taxFilerNotProvidedIndicator;
	 private Boolean parentCaretakerIndicator;
	 private Boolean pregnancyIndicator;
	 private Boolean livesWithCustodialParentNotOnApplicationIndicator;
	 private Boolean studentStatus;
	 private Boolean parentLivesInStudentStateIndicator;
	 private Boolean studentsParentLivingInExchangeStateIndicator;
	 private Boolean absentParentIndicator;
	 private Boolean taxDependentIndicator;
	 private Boolean taxFilerIndicator;
	 private Boolean resideWithBothParentIndicator;
	 private String taxReturnFilingStatusType;
	 private Boolean liveWithParentOrSiblingIndicator;
	 private Boolean attestedHeadOfHouseholdIndicator;


	 // Getter Methods 

	 public Boolean getClaimsDependentIndicator() {
	  return claimsDependentIndicator;
	 }

	 public Boolean getFosterCareIndicator() {
	  return fosterCareIndicator;
	 }

	 public Boolean getTaxFilerNotProvidedIndicator() {
	  return taxFilerNotProvidedIndicator;
	 }

	 public Boolean getParentCaretakerIndicator() {
	  return parentCaretakerIndicator;
	 }

	 public Boolean getPregnancyIndicator() {
	  return pregnancyIndicator;
	 }

	 public Boolean getLivesWithCustodialParentNotOnApplicationIndicator() {
	  return livesWithCustodialParentNotOnApplicationIndicator;
	 }

	 public Boolean getStudentStatus() {
	  return studentStatus;
	 }

	 public Boolean getStudentsParentLivingInExchangeStateIndicator() {
	  return studentsParentLivingInExchangeStateIndicator;
	 }

	 public Boolean getAbsentParentIndicator() {
	  return absentParentIndicator;
	 }

	 public Boolean getTaxDependentIndicator() {
	  return taxDependentIndicator;
	 }

	 public Boolean getTaxFilerIndicator() {
	  return taxFilerIndicator;
	 }

	 public Boolean getResideWithBothParentIndicator() {
	  return resideWithBothParentIndicator;
	 }

	 public String getTaxReturnFilingStatusType() {
	  return taxReturnFilingStatusType;
	 }

	 public Boolean getLiveWithParentOrSiblingIndicator() {
	  return liveWithParentOrSiblingIndicator;
	 }

	 // Setter Methods 

	 public void setClaimsDependentIndicator(Boolean claimsDependentIndicator) {
	  this.claimsDependentIndicator = claimsDependentIndicator;
	 }

	 public void setFosterCareIndicator(Boolean fosterCareIndicator) {
	  this.fosterCareIndicator = fosterCareIndicator;
	 }

	 public void setTaxFilerNotProvidedIndicator(Boolean taxFilerNotProvidedIndicator) {
	  this.taxFilerNotProvidedIndicator = taxFilerNotProvidedIndicator;
	 }

	 public void setParentCaretakerIndicator(Boolean parentCaretakerIndicator) {
	  this.parentCaretakerIndicator = parentCaretakerIndicator;
	 }

	 public void setPregnancyIndicator(Boolean pregnancyIndicator) {
	  this.pregnancyIndicator = pregnancyIndicator;
	 }

	 public void setLivesWithCustodialParentNotOnApplicationIndicator(Boolean livesWithCustodialParentNotOnApplicationIndicator) {
	  this.livesWithCustodialParentNotOnApplicationIndicator = livesWithCustodialParentNotOnApplicationIndicator;
	 }

	 public void setStudentStatus(Boolean studentStatus) {
	  this.studentStatus = studentStatus;
	 }

	 public void setStudentsParentLivingInExchangeStateIndicator(Boolean studentsParentLivingInExchangeStateIndicator) {
	  this.studentsParentLivingInExchangeStateIndicator = studentsParentLivingInExchangeStateIndicator;
	 }

	 public void setAbsentParentIndicator(Boolean absentParentIndicator) {
	  this.absentParentIndicator = absentParentIndicator;
	 }

	 public void setTaxDependentIndicator(Boolean taxDependentIndicator) {
	  this.taxDependentIndicator = taxDependentIndicator;
	 }

	 public void setTaxFilerIndicator(Boolean taxFilerIndicator) {
	  this.taxFilerIndicator = taxFilerIndicator;
	 }

	 public void setResideWithBothParentIndicator(Boolean resideWithBothParentIndicator) {
	  this.resideWithBothParentIndicator = resideWithBothParentIndicator;
	 }

	 public void setTaxReturnFilingStatusType(String taxReturnFilingStatusType) {
	  this.taxReturnFilingStatusType = taxReturnFilingStatusType;
	 }

	 public void setLiveWithParentOrSiblingIndicator(Boolean liveWithParentOrSiblingIndicator) {
	  this.liveWithParentOrSiblingIndicator = liveWithParentOrSiblingIndicator;
	 }

	 public Boolean getClaimingTaxFilerNotOnApplicationIndicator() {
		return claimingTaxFilerNotOnApplicationIndicator;
	}

	public void setClaimingTaxFilerNotOnApplicationIndicator(Boolean claimingTaxFilerNotOnApplicationIndicator) {
		this.claimingTaxFilerNotOnApplicationIndicator = claimingTaxFilerNotOnApplicationIndicator;
	}

	 public Integer getBabyDueQuantity() {
		return babyDueQuantity;
	}

	public void setBabyDueQuantity(Integer babyDueQuantity) {
		this.babyDueQuantity = babyDueQuantity;
	}

	public Boolean getAttestedHeadOfHouseholdIndicator() {
		return attestedHeadOfHouseholdIndicator;
	}

	public void setAttestedHeadOfHouseholdIndicator(Boolean attestedHeadOfHouseholdIndicator) {
		this.attestedHeadOfHouseholdIndicator = attestedHeadOfHouseholdIndicator;
	}

	 public Boolean getMedicaidDuringFosterCareIndicator() {
		return medicaidDuringFosterCareIndicator;
	}

	public void setMedicaidDuringFosterCareIndicator(Boolean medicaidDuringFosterCareIndicator) {
		this.medicaidDuringFosterCareIndicator = medicaidDuringFosterCareIndicator;
	}

	public Boolean getMedicaidFamilyNotProvidedIndicator() {
		return medicaidFamilyNotProvidedIndicator;
	}

	public void setMedicaidFamilyNotProvidedIndicator(Boolean medicaidFamilyNotProvidedIndicator) {
		this.medicaidFamilyNotProvidedIndicator = medicaidFamilyNotProvidedIndicator;
	}

	public Integer getFosterCareEndAge() {
		return fosterCareEndAge;
	}

	public void setFosterCareEndAge(Integer fosterCareEndAge) {
		this.fosterCareEndAge = fosterCareEndAge;
	}

	public String getFosterCareState() {
		return fosterCareState;
	}

	public void setFosterCareState(String fosterCareState) {
		this.fosterCareState = fosterCareState;
	}

	public Boolean getParentLivesInStudentStateIndicator() {
		return parentLivesInStudentStateIndicator;
	}

	public void setParentLivesInStudentStateIndicator(Boolean parentLivesInStudentStateIndicator) {
		this.parentLivesInStudentStateIndicator = parentLivesInStudentStateIndicator;
	}
}
