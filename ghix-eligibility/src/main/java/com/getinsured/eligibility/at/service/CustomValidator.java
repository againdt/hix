package com.getinsured.eligibility.at.service;

import java.util.List;

import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;

/**
 * @author chopra_s
 *
 */
public interface CustomValidator {
	
	String PRIMARYTAXFILER = "PrimaryTaxFiler";
	
	String EE_PACKAGE_NAME = "com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.";

	String MSG_IDENTIFICATION_ID = "HE001112 - Applicant IdentificationId not found in the AT Request for Applicant - {0}.";

	String MSG_FINANCIAL_APP = "HE001113 - AT request is not a financial application.";

	String MSG_TAX_RETURN = "HE001114 - AT request cannot have more than 1 Tax Return.";

	String MSG_APTC_CSR_MISSING = "HE001115 - Either APTC or CSR Eligibility missing for one or more insuranceApplicantType.";

	String MSG_MANDATORY_FIELD_APPLICANT = "HE001116 - {0} not found in the AT Request for Applicant - {1}.";

	String MSG_SSN_DUPLICATE = "HE001117 - SSN cannot be duplicate for Household members in the AT Request.";

	String MSG_RELATIONSHIP = "HE001118 - Relationship with primary contact not found for some Household members in the AT Request.";

	String MSG_MANDATORY_FIELD = "HE001119 - {0} not found in the AT Request.";

	String MSG_INVALID_DATE = "HE0011110 - {0} not valid in the AT Request.";

	String MSG_INVALID_DATA = "HE0011111 - {0} not valid in the AT Request for Applicant - {1}.";

	String MSG_ZIPCODE = "HE0011112 - Address not valid in AT Request.";

	String MSG_ID_DUPLICATE = "HE0011113 - IdentificationId cannot be duplicate for Household members in the AT Request.";

	String MSG_APTC_FINAL_AMOUNT = "HE0011114 - APTC Maximum Amount is not available for a primary tax filer on an APTC eligible household.";

	String MSG_NON_NATIVE_AMR_CSR ="HE0011115 - Multiple invalid CSR levels (non native american) found on tax household with values -";

	String MSG_NATIVE_AMR_CSR ="HE0011116 - Multiple invalid CSR levels (native american) found on tax household with values -";

	String MSG_MISSING_RELATIONSHIP="HE0011117 - All the relationships for household members are not available in AT Request";

	String MSG_REASON_TEXT_INVALID="HE0011118 - Eligibility Reason text is not valid in AT Request for Applicant - {0}.";

	String MSG_CSR_APTC_INVALID="HE0011119 - APTC Eligibility value cannot be false if CSR Eligibility is true Applicant - {0}.";

	String MSG_EVENT_QHP_MANDATORY = "HE00111120 - Either ExtendedApplicantEvent or ExtendedApplicantNonQHP is required for Extended Applicant in the AT Request for Applicant - {0}.";

	String MSG_EVENT_REPORT_DATE_DIFFERENCE = "HE00111121 - Difference of ExtendedApplicantEventDate and ExtendedApplicantReportDate should be less than or equal to sixty days for Applicant - {0}.";

	String MSG_CODE_DUPLICATE = "HE00111122 - {0} cannot be duplicate for the Applicant - {1} in the AT Request.";

	String MSG_INVALID_DATE_APPLICANT = "HE00111123 - {0} not valid in the AT Request for Applicant - {1}.";

	String MSG_APTC_AND_CSR_ELIGIBILITY_START_DATE_IS_NOT_FIRST_OF_A_MONTH = "HE00111124 - APTC and CSR eligibility start dates for all members should be first of a month.";

	String MSG_MULTIPLE_APTC_AND_CSR_ELIGIBILITY_END_DATES_FOUND = "HE00111125 - APTC and CSR eligibility end dates for all members should match.";

	String MSG_MULTIPLE_APTC_ELIGIBILITY_START_DATES_FOUND = "HE00111126 - APTC eligibility start dates for all members should match.";
	
	String MSG_MULTIPLE_CSR_ELIGIBILITY_START_DATES_FOUND = "HE00111136 - CSR eligibility start dates for all members should match.";

	String MSG_APTC_AND_CSR_ELIGIBILITY_START_DATE_IS_OUTSIDE_COVERAGE_YEAR = "HE00111127 - APTC and CSR eligibility start dates for all members should be for the coverage year - {0} .";

	String MSG_APTC_AND_CSR_ELIGIBILITY_END_DATE_IS_OUTSIDE_COVERAGE_YEAR = "HE00111128 - APTC and CSR eligibility end dates for all members should be for the coverage year - {0} .";
	
	String MSG_CSR_MEDICAID_INVALID = "HE00111129 - Medicaid Eligibility value cannot be true if CSR Eligibility is true Applicant - {0}.";
	
	String MSG_CSR_CHIP_INVALID = "HE00111130 - Chip Eligibility value cannot be true if CSR Eligibility is true Applicant - {0}.";
	
	String MSG_EXCHANGE_ELIG_MISSING_CA = "HE00111131 - Exchange Eligibility missing for one or more insuranceApplicantType.";

	String MSG_NO_PROVISION_AUTO_REP = "Warning : {0} not valid in the AT Request or {0} is not provisioned - {1}.";
	
	String MSG_NO_OR_INVALID_ELIGIBILITY_SPANS = "HE00111132 - {0} are required and should be an integer greater than 0.";
	
	String MSG_INVALID_COUNTY = "HE00111133 - Location County Code can not be empty and must contain exactly 3 digits.";
	
	String MSG_INVALID_ASSISTER_TYPE_ORG_NAME = "HE00111134 - Assister:OrganizationName is invalid - must be Broker, Navigator or Certified Application Counselor.";
	
	String MSG_MISSING_ELIGIBILITY_SPANS = "HE00111135 - Missing spans. Received {0}.";
	
	String MSG_MISSING_COVERAGE_SEEKING_INSURANCE_APPLICANT = "HE00111136 - Coverage seeking member not present as an InsuranceApplicant. Applicant - {0}.";

	public List<String> validate(AccountTransferRequestPayloadType request);
}
