

export default condition => {
    const resolve = condition => {
        let output = [];
        Object.entries(condition).forEach(c => {
            const [ key, value ] = c;
            if(key !== 'type') {
                if(value instanceof Object) {
                    output.push(resolve(value));
                } else {
                    output.push(value);
                }
            }
        });
        switch(condition.type) {
        case 'AND':
            return output.every(v => v);
        case 'OR':
            return output.some(v => v);
        }
    };
    return resolve(condition);
};