package com.getinsured.hix.broker.service;

import java.util.List;
import java.util.Map;
import com.getinsured.hix.dto.employees.EmployeeDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.DesignateBroker.Status;
import com.getinsured.hix.platform.security.exception.InvalidUserException;

/**
 * Designate Broker Service
 */
public interface DesignateService {

	/**
	 * Saves designated broker with Pending status when employer/individual
	 * sends designate request.
	 *
	 * @param designateBroker
	 * @return 
	 */
	EntityResponseDTO saveDesignateBroker(DesignateBroker designateBroker);


	/**
	 * Finds designated broker id by employer id
	 *
	 * @param employerId
	 *            Employer ID
	 * @return long broker id
	 */
	long findByEmployerId(int employerId);

	/**
	 * Finds {@link DesignateBroker}
	 *
	 * @param broker
	 *            {@link Broker}
	 * @param startRecord
	 *            used for pagination
	 * @param pageSize
	 *            page size
	 * @return {@link Map} of {@link DesignateBroker}
	 */
	Map<String, Object> findDesignatedBroker(DesignateBroker broker,
			Integer startRecord, Integer pageSize);

	/**
	 * Finds {@link DesignateBroker}
	 *
	 * @param broker
	 *            {@link Broker}
	 * @param startRecord
	 *            used for pagintaion
	 * @param pageSize
	 *            page size
	 * @param empStatus
	 *            current status
	 * @return {@link Map} of {@link DesignateBroker}
	 */
	Map<String, Object> findDesignatedBroker(DesignateBroker broker,
			Integer startRecord, Integer pageSize, String empStatus);

	/**
	 * Finds {@link DesignateBroker} by employer id
	 *
	 * @param employerId
	 *            Employer id
	 * @return {@link DesignateBroker}
	 */
	DesignateBroker findBrokerByEmployerId(int employerId);

	/**
	 * Finds {@link Employer} based on search criteria
	 *
	 * @param searchCriteria
	 *            search criteria
	 * @param empStatus
	 *            employer status
	 * @return {@link Map} of {@link Employer}
	 */
	Map<String, Object> findEmployers(Map<String, Object> searchCriteria,
			String empStatus);
	
	/**
	 * De-designates {@link Employer} for {@link Broker}
	 *
	 * @param brokerId
	 *            broker ID
	 * @return Number of de-designated employers
	 */
	int deDesignateEmployersForBroker(int brokerId);

	/**
	 * Finds {@link DesignateBroker} by individual id
	 *
	 * @param individualId
	 *            User ID
	 * @return {@link DesignateBroker}
	 */
	DesignateBroker findBrokerByIndividualId(int individualId);

	/**
	 * Finds individuals based on search criteria
	 *
	 * @param searchCriteria
	 *            search criteria
	 * @param desigStatus
	 *            status of individual
	 * @return {@link Map} of individuals
	 */
	Map<String, Object> findIndividuals(Map<String, Object> searchCriteria,
			String desigStatus);

	/**
	 * Finds employees based on search criteria
	 *
	 * @param searchCriteria
	 *            search criteria
	 * @param desigStatus
	 *            status of Employer
	 * @return {@link List} of employee DTO objects
	 */
	List<EmployeeDTO> findEmployees(Map<String, Object> searchCriteria,
			String desigStatus);

	/**
	 * Finds
	 * @param extEmployerId
	 */
	DesignateBroker findBrokerByExternalEmployerId(int extEmployerId);

	/**
	 *
	 * @param extIndividualId
	 * @return
	 */
	DesignateBroker findBrokerByExternalIndividualId(int extIndividualId);

	/**
	 * Updates user preferences flag for switch pop-up message in Designate_Broker table
	 *
	 * @param showPopupInFuture Value 'Y' or 'N' as per user selection
	 * @param designateBrokerObj {@link DesignateBroker}
	 */
	void updateUserPreference(Character showPopupInFuture,
			DesignateBroker designateBrokerObj);


	/**
	 * Finds {@link DesignateBroker} by employer id
	 *
	 * @param employerId
	 *            Employer id
	 * @return {@link DesignateBroker}
	 */
	List<DesignateBroker> findActiveIndividualForBroker(int brokerId, DesignateBroker.Status status);
	/**
	 * Finds {@link DesignateBroker} by brokerid
	 * @param id
	 * @return List<@link DesignateBroker>
	 */
	List<DesignateBroker> findDesigBrokerByAgentId(int brokerId);
	
	Integer deDesignateIndividualForBroker(DesignateBroker designateBroker) throws InvalidUserException;


	List<DesignateBroker> findActiveAndPendingDesignateBrokers(int brokerId, List<Status> listOfStatus);
}
