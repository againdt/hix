package com.getinsured.iex.ssap.financial.type;

/**
 * Describes income types.
 *
 * @author Yevgen Golubenko
 * @since 4/1/19
 */
public enum IncomeType {
  /**
   * Special income type to differentiate income
   * from other incomes, if user of this income is
   * Alaskan Indian or American Native.
   * @deprecated UI not going to leverage this income type.
   */
  AI_AN(false),

  /**
   * Received Alimony income.
   */
  ALIMONY(false),

  /**
   * Income from Capital Gains.
   */
  CAPITAL_GAIN(false),

  /**
   * Farming or Fishing income.
   */
  FARMING_FISHING(true),

  /**
   * Investment income.
   */
  INVESTMENT(false),

  /**
   * Regular Job income.
   */
  JOB(true),

  /**
   * Other income.
   */
  OTHER(false),

  /**
   * Pension income.
   */
  PENSION(false),

  /**
   * Rental or Royalty income.
   */
  RENTAL_ROYALTY(false),

  /**
   * Retirement income.
   */
  RETIREMENT(false),

  /**
   * Scholarship income.
   */
  SCHOLARSHIP(false),

  /**
   * Self Employment income.
   */
  SELF_EMPLOYMENT(true),

  /**
   * Social Security Disability income.
   */
  SOCIAL_SECURITY_DISABILITY(false),

  /**
   * Unemployment income.
   */
  UNEMPLOYMENT(false),

  /**
   * Not specified income type,
   * default.
   */
  UNSPECIFIED(false);

  private boolean earned;

  IncomeType(boolean earnedIncome) {
    this.earned = earnedIncome;
  }

  public boolean isEarned() {
    return this.earned;
  }
}
