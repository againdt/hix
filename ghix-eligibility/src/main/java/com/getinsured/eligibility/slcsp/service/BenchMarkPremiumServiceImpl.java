package com.getinsured.eligibility.slcsp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.slcsp.util.BenchMarkPremiumRestUtil;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitResponse;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.iex.ssap.HouseholdContact;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Service
public class BenchMarkPremiumServiceImpl implements BenchMarkPremiumService {
	
	private SsapJsonBuilder ssapJsonBuilder;
	
	private BenchMarkPremiumRestUtil benchMarkPremiumRestUtil;
	
	private SsapApplicationRepository ssapApplicationRepository;
	
	private static final Logger LOGGER = Logger.getLogger(BenchMarkPremiumService.class);

	@Autowired
	public BenchMarkPremiumServiceImpl(SsapJsonBuilder ssapJsonBuilder,
			BenchMarkPremiumRestUtil benchMarkPremiumRestUtil, SsapApplicationRepository ssapApplicationRepository) {
		this.ssapJsonBuilder = ssapJsonBuilder;
		this.benchMarkPremiumRestUtil = benchMarkPremiumRestUtil;
		this.ssapApplicationRepository = ssapApplicationRepository;
	}

	@Override
	public Float getSLCSPAmount(SsapApplication ssapapplication) {
		
		try {

			Float slcspAmount = null;
			HouseholdContact householdContact = null;
			List<HouseholdMember> houseHoldMembers = null;
			SingleStreamlinedApplication ssapApplicationData;
			
			ssapApplicationData = ssapJsonBuilder.transformFromJson(ssapapplication.getApplicationData());
			houseHoldMembers = ssapApplicationData.getTaxHousehold().get(0).getHouseholdMember();
			householdContact = houseHoldMembers.get(0).getHouseholdContact();
			Long ssapApplicationId = ssapapplication.getId();

			PlanRateBenefitRequest planRateBenefitRequest = new PlanRateBenefitRequest();
			Map<String, Object> cRequestParams = new HashMap<>();
			List<SsapApplicant> ssapApplicantList = ssapapplication.getSsapApplicants();
			List<Map<String, String>> censusData = new ArrayList<Map<String, String>>();

			for (SsapApplicant ssapApplicant : ssapApplicantList) {

				if (ssapApplicant.getEligibilityStatus() != null && ssapApplicant.getApplyingForCoverage() != null
						&& ssapApplicant.getEligibilityStatus().equalsIgnoreCase("QHP")
						&& ssapApplicant.getApplyingForCoverage().equalsIgnoreCase("Y")) {

					Map<String, String> memberData = new HashMap<>();

					if (ssapApplicant.getBirthDate() != null) {
						memberData.put("dob", DateUtil.dateToString(ssapApplicant.getBirthDate(),
								EligibilityConstants.SHORT_DATE_FORMAT));
					}

					if (ssapApplicant.getTobaccouser() != null
							&& ssapApplicant.getTobaccouser().equalsIgnoreCase("Y")) {
						memberData.put("tobacco", "y");
					} else {
						memberData.put("tobacco", "n");
					}

					String zip = householdContact.getHomeAddress().getPostalCode() != null
							? householdContact.getHomeAddress().getPostalCode()
							: householdContact.getMailingAddress().getPostalCode();
					String countyCode = householdContact.getHomeAddress().getPrimaryAddressCountyFipsCode() != null
							? householdContact.getHomeAddress().getPrimaryAddressCountyFipsCode()
							: householdContact.getMailingAddress().getPrimaryAddressCountyFipsCode();

					memberData.put("zip", zip);
					memberData.put("countycode", countyCode);

					if (ssapApplicant.getRelationship() != null) {
						memberData.put("relation", ssapApplicant.getRelationship());
					} else {
						memberData.put("relation", "G8");
					}

					memberData.put("id", ssapApplicant.getApplicantGuid());
					censusData.add(memberData);
				}
				
			}
			
			cRequestParams.put("censusData", censusData);

			if(ssapapplication.getStartDate() != null) {
				cRequestParams.put("coverageStartDate", DateUtil.dateToString(ssapapplication.getStartDate(), EligibilityConstants.SHORT_DATE_FORMAT));
			} else {
				cRequestParams.put("coverageStartDate", null);
			}

			planRateBenefitRequest.setRequestParameters(cRequestParams);

			PlanRateBenefitResponse response = null;
			if (censusData != null && !censusData.isEmpty()) {
				response = benchMarkPremiumRestUtil.getBenchmarkPlanBenefitRate(planRateBenefitRequest);
			} else {
				LOGGER.error("Ignoring benchmark premium call as Census Data received is NULL or EMPTY for ssapID: " + ssapApplicationId);
			}

			if (response != null && response.getStatus().equalsIgnoreCase("SUCCESS")) {
				
				Map<String, Object> responseData = response.getResponseData();
				
				if (responseData != null && responseData.get("benchMarkPre") != null) {
					slcspAmount = Float.parseFloat((String) responseData.get("benchMarkPre"));
				} else {
					LOGGER.info("No EHB Amount returned for HH");
				}			
				return slcspAmount;
				
			} else {
				LOGGER.info("Failure returned by benchmark API");
			}
			
		} catch (Exception e) {
			LOGGER.error("Exception caught in benchmark API: ",e);
		}		
		return null;
	}

	@Override
	public Float getSLCSPAmountForApplicationId(Long ssapApplicationId) {
		Float slcspAmt = null;
		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);
		if (ssapApplications != null && !ssapApplications.isEmpty()) {
			slcspAmt = getSLCSPAmount(ssapApplications.get(0));
		}
		return slcspAmt;
	}
	
}
