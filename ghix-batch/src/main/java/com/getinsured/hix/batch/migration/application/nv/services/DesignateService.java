package com.getinsured.hix.batch.migration.application.nv.services;

import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Designate Broker Service to designate / de-designate employers / individuals
 * 
 * @author kanthi_v
 * 
 */
public interface DesignateService {

	/**
	 * Designates broker for external individual id
	 * 
	 * @param brokerId
	 *            Identification number for agent
	 * @param extIndividualId
	 *            AHBX individual id
	 * @return {@link DesignateBroker}
	 */
	DesignateBroker designateIndividualForBroker(String brokerNpn, String emailId) throws GIException;

	DesignateBroker designateIndividualForBrokerByCaseId(String brokerNpn, String caseId, Integer userId, String firstName, String lastName) throws GIException;
	
	Integer findUserIdByName(String userName);
	
	void deleteDesignatedBrokerByCaseId(String householdCaseId);
	
}
