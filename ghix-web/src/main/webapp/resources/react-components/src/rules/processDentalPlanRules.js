/**
 * Created by sellathurai_s on 11/16/18.
 */

import {Engine} from 'json-rules-engine';
import rulesData from './json/dentalPlanRules.json';
import RulesUtil from './rulesUtil';

class ProcessDentalPlanRules {

    constructor(facts){
        // console.log("Inside Process Plan Rules.");
        this.engine = new Engine();
        this.facts = facts;
        this.rulesData = rulesData;
        this.rulesUtil = new RulesUtil();
    }

    getDefaultOutput(applicationStatus) {
        switch (applicationStatus){
            case 'NO':
            case 'OP':
            case 'ER':
            case 'EN':
            case 'PN':
                return {
                    "applicationStatus": "NO",
                    "enrolledInDental": "N/A",
                    "period": "N/A",
                    "favoritePlanAvailable":"N"
                };
                break;

            default:
                break;
        }
        return {
            "applicationStatus": "NO",
            "enrolledInDental": "N/A",
            "period": "N/A",
            "favoritePlanAvailable":"N"
        };
    }

    executeRules() {
        return new Promise((resolve, reject) => {

            // console.log("Inside executeRules.");

            for (let i = 0; i < this.rulesData.length; i++) {
                this.engine.addRule({
                    conditions: {
                        any: [{
                            all: this.rulesUtil.createRule(this.rulesData[i].rules)
                        }]
                    },
                    event: this.rulesUtil.createEvent(this.rulesData[i].result)
                });
            }

            let displayData = [];
            this.engine
                .run(this.facts)
                .then(events => { // run() returns events with truthy conditions
                    if (events && events.length > 0) {
                        displayData = events.map(item =>
                            { return item.params }
                        );
                        resolve(displayData);
                    } else {
                        // console.log("Default State: " + JSON.stringify(this.getDefaultOutput("ER")));

                        let returnData = {
                            "type": "SINGLE_COL",
                            "col1": "plans.dental_no_plans",
                            "col2": "",
                            "col3": ""
                        };

                        displayData.push(returnData);
                        // console.log("No matching records");
                        resolve(displayData);
                    }
                });

        });
    }
};


export { ProcessDentalPlanRules as default};