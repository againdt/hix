import conditions from './smFurtherAction/smConditions';
import states from './smFurtherAction/smStates';
import transitions from './smFurtherAction/transitions/default/smTransitions';
import MNtransitions from './smFurtherAction/transitions/mn/smTransitions';
import CAtransitions from './smFurtherAction/transitions/ca/smTransitions';
import NVtransitions from './smFurtherAction/transitions/nv/smTransitions';

const getSMFurtherActionRules = (state) => {
    // smConditions and smStates are shared across all states.
    // smTransitions are state specific.
    // Currently only MN and CA has unique requirements. All other states will fall back to default.
    let result = {
        states: states,
        conditions: conditions
    };

    switch (state) {
        case 'MN': {
            result.transitions = MNtransitions;
            break;
        }
        case 'CA': {
            result.transitions = CAtransitions;
            break;
        }
        case 'NV': {
            result.transitions = NVtransitions;
            break;
        }
        default: {
            result.transitions = transitions;
        }
    }

    return result;
};

export default getSMFurtherActionRules;