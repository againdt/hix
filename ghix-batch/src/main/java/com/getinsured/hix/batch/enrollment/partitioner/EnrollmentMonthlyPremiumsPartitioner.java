package com.getinsured.hix.batch.enrollment.partitioner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.service.EnrollmentMonthlyPremiumsBatchService;
import com.getinsured.hix.batch.enrollment.skip.EnrollmentMonthlyPremiumParams;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.util.exception.GIException;

@Component("enrollmentMonthlyPremiumsPartitioner")
@Scope("step")
public class EnrollmentMonthlyPremiumsPartitioner implements Partitioner {
	private StepExecution stepExecution;
	
	private String commitInterval;
	private String strYear;
	private String enrollmentIds;
	private String fillEmptyPremiumOnly;
	private Integer applicableYear;
	private String fillOnlySLCSP;
	private JobService jobService;
	private EnrollmentMonthlyPremiumParams enrollmentMonthlyPremiumParams;
	private EnrollmentMonthlyPremiumsBatchService enrollmentMonthlyPremiumsBatchService;
	long jobExecutionId = -1;
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyPremiumsPartitioner.class);
	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();
		ExecutionContext executionContext = null;
		if((null == strYear || strYear.isEmpty()) && (null == enrollmentIds || enrollmentIds.isEmpty())){
			LOGGER.error("EnrollmentMonthlyPremiumsPartitioner failed to execute :: Please provide year or an enrollment ID list");
			throw new RuntimeException("EnrollmentMonthlyPremiumsPartitioner failed to execute :: Please provide an year or a pipe separated enrollment ID list") ;
		}
		
		try{
			if(null != enrollmentMonthlyPremiumParams){
				enrollmentMonthlyPremiumParams.resetFields();
			}
			setApplicableMonthYear();
			if(stepExecution!=null){
				executionContext=stepExecution.getJobExecution().getExecutionContext();
				if(executionContext!=null){
					executionContext.put("year", applicableYear);
				}
				jobExecutionId=stepExecution.getJobExecution().getId();
			}
			List<Integer> enrollmentIdList= new ArrayList<Integer>();
			if(fillOnlySLCSP!=null){
				if(!fillOnlySLCSP.equalsIgnoreCase(EnrollmentConstants.FILL_ONLY_SLCSP_ALL) && !fillOnlySLCSP.equalsIgnoreCase(EnrollmentConstants.FILL_ONLY_SLCSP_MISSING_MONTHS)){
					throw new RuntimeException("EnrollmentMonthlyPremiumsPartitioner failed to execute :: Please provide All OR MissingMonths for property FillOnlySLCSP") ;
				}
				if(null != enrollmentIds && !enrollmentIds.isEmpty() && enrollmentIds.split("\\|").length > 0 && enrollmentIds.matches("[0-9|]*")){
					String[] enrlArray = enrollmentIds.split("\\|");
					for(String enrollmentId : enrlArray){
						if(NumberUtils.isDigits(enrollmentId.trim())){
							enrollmentIdList.add(Integer.valueOf(enrollmentId.trim()));
						}
					}
				}else if(applicableYear > 0){
					enrollmentIdList= enrollmentMonthlyPremiumsBatchService.getEmptySLCSPEnrollmentIdsForYear(applicableYear);
				}
				
			}else{
				if(null != enrollmentIds && !enrollmentIds.isEmpty() && enrollmentIds.split("\\|").length > 0 && enrollmentIds.matches("[0-9|]*")){
					String[] enrlArray = enrollmentIds.split("\\|");
					for(String enrollmentId : enrlArray){
						if(NumberUtils.isDigits(enrollmentId.trim())){
							enrollmentIdList.add(Integer.valueOf(enrollmentId.trim()));
						}
					}
				}else if(applicableYear > 0){
					// Get enrollment Id list from database
					if(fillEmptyPremiumOnly != null && fillEmptyPremiumOnly.equalsIgnoreCase("Y")){
						
						enrollmentIdList =  enrollmentMonthlyPremiumsBatchService.getDeltaEnrollmentIdsForYear(applicableYear);
						
					}else{
						
						enrollmentIdList =  enrollmentMonthlyPremiumsBatchService.getEnrollmentIdsForYear(applicableYear);
					}
					
				}else{
					LOGGER.error("EnrollmentMonthlyPremiumsPartitioner failed to execute :: Please provide valid year or an enrollment ID list");
					throw new RuntimeException("EnrollmentMonthlyPremiumsPartitioner failed to execute :: Please provide an year or a pipe separated enrollment ID list") ;
				}
			}
			
			
			
			if(enrollmentIdList!=null && !enrollmentIdList.isEmpty()&& enrollmentIdList.size()>0){
				enrollmentMonthlyPremiumParams.addAllToEnrollmentIdList(enrollmentIdList);
				int maxEnrollmentsPerPartition = 1;
				int size = enrollmentIdList.size();
				if(this.commitInterval !=null && !this.commitInterval.isEmpty()){
					maxEnrollmentsPerPartition=Integer.valueOf(this.commitInterval.trim());
				}
				int numberOfPartitions = size / maxEnrollmentsPerPartition;
				if (size % maxEnrollmentsPerPartition != 0) {
					numberOfPartitions++;
				}
				int firstIndex = 0;
				int lastIndex = 0;
				for (int i = 0; i < numberOfPartitions; i++) {
					firstIndex = i * maxEnrollmentsPerPartition;
					lastIndex = (i + 1) * maxEnrollmentsPerPartition;
					if (lastIndex > size) {
						lastIndex = size;
					}
					ExecutionContext value = new ExecutionContext();
					value.putInt("startIndex", firstIndex);
					value.putInt("endIndex", lastIndex);
					value.putInt("partition",  i);
					partitionMap.put("partition - " + i, value);
					LOGGER.debug("EnrollmentMonthlyPremiumsPartitioner : partition():: partition map entry end for partition :: "+i);

				}
				
				
				 String batchJobStatus=null;
					if(jobService != null && jobExecutionId != -1){
						try{
						batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
						}catch(Exception e){
							LOGGER.error("Failed to get job status :: "+e.getMessage());
						}
					}
					
					if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
						throw new RuntimeException(EnrollmentConstants.BATCH_STOP_MSG);
					}
			}

		}catch(Exception e){
			LOGGER.error("EnrollmentMonthlyPremiumsPartitioner failed to execute : "+ e.getMessage(), e);
			throw new RuntimeException("EnrollmentMonthlyPremiumsPartitioner failed to execute : " + e.getMessage()) ;
		}
		return partitionMap;
	}

	private void setApplicableMonthYear() throws GIException{
		int intYear = 0;
		Calendar calObj = Calendar.getInstance();
		int currentYear = calObj.get(Calendar.YEAR);
		if(NumberUtils.isNumber(strYear)){
			intYear = Integer.parseInt(strYear);
			if(intYear <= currentYear){
				applicableYear = intYear;
			}
		}
	}
	
	public String getStrYear() {
		return strYear;
	}

	public void setStrYear(String strYear) {
		this.strYear = strYear;
	}
	/**
	 * @return the commitInterval
	 */
	public String getCommitInterval() {
		return commitInterval;
	}
	/**
	 * @param commitInterval the commitInterval to set
	 */
	public void setCommitInterval(String commitInterval) {
		this.commitInterval = commitInterval;
	}
	/**
	 * @return the enrollmentIds
	 */
	public String getEnrollmentIds() {
		return enrollmentIds;
	}
	/**
	 * @param enrollmentIds the enrollmentIds to set
	 */
	public void setEnrollmentIds(String enrollmentIds) {
		this.enrollmentIds = enrollmentIds;
	}

	public String getFillEmptyPremiumOnly() {
		return fillEmptyPremiumOnly;
	}

	public void setFillEmptyPremiumOnly(String fillEmptyPremiumOnly) {
		this.fillEmptyPremiumOnly = fillEmptyPremiumOnly;
	}

	/**
	 * @return the enrollmentMonthlyPremiumParams
	 */
	public EnrollmentMonthlyPremiumParams getEnrollmentMonthlyPremiumParams() {
		return enrollmentMonthlyPremiumParams;
	}

	/**
	 * @param enrollmentMonthlyPremiumParams the enrollmentMonthlyPremiumParams to set
	 */
	public void setEnrollmentMonthlyPremiumParams(EnrollmentMonthlyPremiumParams enrollmentMonthlyPremiumParams) {
		this.enrollmentMonthlyPremiumParams = enrollmentMonthlyPremiumParams;
	}

	/**
	 * @return the enrollmentMonthlyPremiumsBatchService
	 */
	public EnrollmentMonthlyPremiumsBatchService getEnrollmentMonthlyPremiumsBatchService() {
		return enrollmentMonthlyPremiumsBatchService;
	}

	/**
	 * @param enrollmentMonthlyPremiumsBatchService the enrollmentMonthlyPremiumsBatchService to set
	 */
	public void setEnrollmentMonthlyPremiumsBatchService(
			EnrollmentMonthlyPremiumsBatchService enrollmentMonthlyPremiumsBatchService) {
		this.enrollmentMonthlyPremiumsBatchService = enrollmentMonthlyPremiumsBatchService;
	}

	public String getFillOnlySLCSP() {
		return fillOnlySLCSP;
	}

	public void setFillOnlySLCSP(String fillOnlySLCSP) {
		this.fillOnlySLCSP = fillOnlySLCSP;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public StepExecution getStepExecution() {
		return stepExecution;
	}

	public void setStepExecution(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}

	
	
}
