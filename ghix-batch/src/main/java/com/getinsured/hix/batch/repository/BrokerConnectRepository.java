package com.getinsured.hix.batch.repository;

import com.getinsured.hix.model.BrokerConnect;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BrokerConnectRepository extends JpaRepository<BrokerConnect, Integer> {
    @Query("FROM BrokerConnect WHERE status = 'ACTIVE'")
    List<BrokerConnect> findActiveBrokers();
}
