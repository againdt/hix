import React, {Fragment} from 'react';
import {FormattedMessage} from 'react-intl';
import {connect} from 'react-redux';
import classnames from 'classnames';

const SummaryItem = props => {
    const {
        additionalElement,
        additionalElementClassName,
        category,
        value,
        valueCode,
        classes2,
        textCode,
    } = props;

    const reviewDocumentClass = classnames({
        'review-document': value instanceof Array,
        'review-document-two-columns': !(value instanceof Array)
    });
    const {
        spanClass,
        spanClass1,
        divClass1,
        divClass2
    } = classes2 ? classes2 : {};

    return (
        <div
            className={`usa-width-one-whole ${reviewDocumentClass}`}
        >
            {
                !(value instanceof Array) ?
                    (
                        <Fragment>
                            <div className={divClass1 ? divClass1 : 'usa-width-one-forth'}>
                                {textCode &&
                                    <FormattedMessage id={`${textCode}`} />
                                }
                                {!textCode && category}
                                {!textCode && (category === null || category.length < 1)
                                }
                            </div>
                            <div
                                className={divClass2 ? divClass2 : 'usa-width-one-forth'}
                            >
                                <span>
                                    {value}
                                </span>
                            </div>
                        </Fragment>
                    ) :
                    (
                        <Fragment>
                            <span
                                className={spanClass1 ? spanClass1 : spanClass ? spanClass : 'usa-width-one-forth'}
                            >
                                {textCode &&
                                    <FormattedMessage id={`${textCode}`} />
                                }
                                {!textCode && category}
                            </span>
                            {valueCode && valueCode.map(el =>
                                <span
                                    key={el}
                                    className={spanClass ? spanClass : 'usa-width-one-forth'}
                                >
                                    {el &&
                                        <FormattedMessage id={`${el}`} />
                                    }
                                    {!el &&
                                        <>&nbsp;</>
                                    }
                                </span>
                            )}
                            {!valueCode && value.map(el =>
                                <span
                                    key={el}
                                    className={spanClass ? spanClass : 'usa-width-one-forth'}
                                >
                                    {el}
                                    {!el &&
                                        <>&nbsp;</>
                                    }
                                </span>
                            )}
                            {
                                additionalElement &&

                                <div className={additionalElementClassName}>
                                    {
                                        additionalElement
                                    }
                                </div>
                            }
                        </Fragment>

                    )
            }
        </div>
    );
};

const mapStateToProps = (state) => ({
    messages: state.messages
});

export default connect(mapStateToProps)(SummaryItem);
