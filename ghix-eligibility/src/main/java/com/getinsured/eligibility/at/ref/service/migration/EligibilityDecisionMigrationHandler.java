package com.getinsured.eligibility.at.ref.service.migration;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.resp.si.dto.TaxHouseholdMember;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.util.TSDate;

/**
 * EligibilityDecisionHandler to compare eligibilities.
 *
 * Take decision based on current and enrolled applications; if enrolled application found
 *
 * Take decision based current applications; if enrolled application not found
 *
 * EligibilityDecisionHandler updates Application status - {ER, EN and CL}
 *
 * @author Ekram
 *
 */
@Component
@DependsOn("dynamicPropertiesUtil")
public class EligibilityDecisionMigrationHandler{

	private static final String POSSIBLE_DATA_ISSUE_ACTIVE_APP = "Possible Data issue : Invalid data found in ExchangeEligibilityStatus for Enrolled (Active) application - ";

	private static final String POSSIBLE_DATA_ISSUE_CURRENT_APP = "Possible Data issue : Invalid data found in ExchangeEligibilityStatus for Current application - ";

	private static final Logger LOGGER = Logger.getLogger(EligibilityDecisionMigrationHandler.class);

	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private SsapApplicantRepository ssapApplicantRepository;

	@Qualifier("idahoEligibilityDecisionMigrationHandler")
	@Autowired private IdahoEligibilityDecisionMigrationHandler idahoEligibilityDecisionHandler;

	private String STATE_CODE;

	@PostConstruct
	public void createStateContext() {
		STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	}

	private static final Set<ExchangeEligibilityStatus> VALID_EXCHANGE_ELIGIBILITY_STATUS = EnumSet.of(ExchangeEligibilityStatus.APTC,
			ExchangeEligibilityStatus.APTC_CSR, ExchangeEligibilityStatus.QHP);

	public String processERP(Message<ERPResponse> message){

		return idahoEligibilityDecisionHandler.processERP(message);
		
		/*if ("ID".equals(STATE_CODE)){
			return idahoEligibilityDecisionHandler.processERP(message);
		}*/

		/*ERPResponse erpResponse = (ERPResponse) message.getHeaders().get(EligibilityConstants.ERP_RESPONSE);

		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put(EligibilityConstants.SSAP_APPLICATION_ID, erpResponse.getApplicationID());

		try {
			boolean flag = preProcess(erpResponse);

			if (flag){
				resultMap.put(EligibilityConstants.ELIGIBILTY_DECISION_RESULT, EligibilityConstants.AUTO);
			} else {
				resultMap.put(EligibilityConstants.ELIGIBILTY_DECISION_RESULT, EligibilityConstants.MANUAL);
			}

		} catch (Exception e) {

			StringBuilder errorReason = new StringBuilder().append(EligibilityConstants.ERROR_PROCESSING_ERP_REQUEST_FOR_SSAP_APPLICATION_ID).
					append(erpResponse.getApplicationID()).append(EligibilityConstants.REASON).append(e);

			LOGGER.error(errorReason.toString());
			resultMap.put(EligibilityConstants.ELIGIBILTY_DECISION_RESULT, EligibilityConstants.ERROR);
			resultMap.put(EligibilityConstants.ERROR_REASON, errorReason.toString());
		} finally {
			resultMap.put(EligibilityConstants.ERP_RESPONSE, erpResponse);
		}

		return EligibilityUtils.marshal(resultMap);*/

	}


	private boolean preProcess(ERPResponse erpResponse){

		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumber(erpResponse.getApplicationID());

		if (ssapApplicationList.isEmpty()){
			throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES);
		}

		SsapApplication currentApplication = ssapApplicationList.get(0);

		SsapApplication enrolledApplication = ssapApplicationRepository.findLatestEnPnSsapApplicationForCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear());

		/** Check for Enrolled app...
		if found then do rest of the comparison
		else skip comparison
		 */
		// Set Application Status to ELIGIBILITY_RECEIVED
		updateCurrentAppToER(currentApplication, erpResponse.getAtResponseType());

		if (enrolledApplication != null) {
			LOGGER.debug("Enrolled (Active) Application found for - " + erpResponse.getApplicationID());
			compareAndProcess(currentApplication, enrolledApplication, erpResponse.getTaxHouseholdMemberList());
		} else {
			process(currentApplication);
		}

		return true;

	}

	private void updateCurrentAppToER(SsapApplication currentApplication, String atResponseType) {
		setApplicationStatus(currentApplication, "ER");
		setEligibilityReceivedDateAndType(currentApplication, atResponseType);
	}


	private void setEligibilityReceivedDateAndType(SsapApplication currentApplication, String atResponseType) {
		currentApplication.setEligibilityReceivedDate(new TSDate());
		currentApplication.setEligibilityResponseType(atResponseType);
		ssapApplicationRepository.save(currentApplication);
	}

	/**
	 * Process currentApplication.
	 * This is for applications NOT having enrolled application (active application).
	 * @param currentApplication
	 */
	private void process(SsapApplication currentApplication){

		if (!isExchangeEligible(currentApplication)){

			// Close current App
			// Show link to Medicaid Agency
			if (currentApplication.getEligibilityStatus() == EligibilityStatus.CAM){
				// DO NOT CLOSE the app for CAM case, enable enrollment..
				// Allow Enrollment
				setAllowEnrollment(currentApplication, EligibilityConstants.Y);
				return;
			}
			closeApplication(currentApplication);
			return;
		}

		ExchangeEligibilityStatus currentExchangeEligibilityStatus = currentApplication.getExchangeEligibilityStatus();


		if (VALID_EXCHANGE_ELIGIBILITY_STATUS.contains(currentExchangeEligibilityStatus)){
			// Allow Enrollment
			setAllowEnrollment(currentApplication, EligibilityConstants.Y);
		} else {
			// boundary condition...
			setAllowEnrollment(currentApplication, EligibilityConstants.N);
			closeApplication(currentApplication);
		}
	}


	private void closeApplication(SsapApplication ssapApplication) {
		setApplicationStatus(ssapApplication, "CL");
	}


	private boolean isExchangeEligible(SsapApplication currentApplication) {
		return currentApplication.getEligibilityStatus() == EligibilityStatus.AE || currentApplication.getEligibilityStatus() == EligibilityStatus.AX
				|| currentApplication.getEligibilityStatus() == EligibilityStatus.CAE || currentApplication.getEligibilityStatus() == EligibilityStatus.CAX;
	}

	/**
	 * Compares enrolledApplication with the currentApplication.
	 *
	 * @param currentApplication
	 * @param enrolledApplication
	 * @param atTaxHHList
	 */
	private void compareAndProcess(SsapApplication currentApplication, SsapApplication enrolledApplication, List<TaxHouseholdMember> atTaxHHList){


		if (!isExchangeEligible(currentApplication)){

			// Is anyone enrolled in enrolledApplication
			// If YES, Dis-enroll and Close enrolledApplication
			// Close current App
			// Show link to Medicaid Agency

			closeApplication(enrolledApplication);
			closeApplication(currentApplication);

			return;
		}

		enrolledExchangeEligibilityStatusToNewStatus(currentApplication, enrolledApplication, atTaxHHList);

	}


	private void enrolledExchangeEligibilityStatusToNewStatus(SsapApplication currentApplication, SsapApplication enrolledApplication,
			List<TaxHouseholdMember> atTaxHHList) {

		ExchangeEligibilityStatus currentExchangeEligibilityStatus = currentApplication.getExchangeEligibilityStatus();
		ExchangeEligibilityStatus enrolledExchangeEligibilityStatus = enrolledApplication.getExchangeEligibilityStatus();

		LOGGER.debug(enrolledExchangeEligibilityStatus + " - "  + currentExchangeEligibilityStatus);

		switch (enrolledExchangeEligibilityStatus) {

		case NONE:
			//boundary condition... will this happen???
			throw new GIRuntimeException(POSSIBLE_DATA_ISSUE_ACTIVE_APP + enrolledExchangeEligibilityStatus);

		case QHP:
			handleQHPToOthers(currentApplication, enrolledApplication, atTaxHHList, currentExchangeEligibilityStatus);
			break;

		case APTC_CSR:
			handleAPTC_CSRToOthers(currentApplication, enrolledApplication, atTaxHHList, currentExchangeEligibilityStatus);
			break;

		case APTC:
			handleAPTCToOthers(currentApplication, enrolledApplication, atTaxHHList, currentExchangeEligibilityStatus);
			break;

		default:
			break;
		}
	}


	private void handleAPTCToOthers(SsapApplication currentApplication,
			SsapApplication enrolledApplication,
			List<TaxHouseholdMember> atTaxHHList,
			ExchangeEligibilityStatus currentExchangeEligibilityStatus) {

		switch (currentExchangeEligibilityStatus) {
		case APTC:
			handleCurrentAPTC(currentApplication, enrolledApplication, atTaxHHList);
			break;

		case APTC_CSR:
			setAllowEnrollment(currentApplication, EligibilityConstants.Y);
			break;

		case QHP:
			callMaintenance834();
			break;

		case NONE:
			throw new GIRuntimeException(POSSIBLE_DATA_ISSUE_CURRENT_APP + currentExchangeEligibilityStatus);

		default:
			break;
		}
	}


	private void handleAPTC_CSRToOthers(SsapApplication currentApplication,
			SsapApplication enrolledApplication,
			List<TaxHouseholdMember> atTaxHHList,
			ExchangeEligibilityStatus currentExchangeEligibilityStatus) {

		switch (currentExchangeEligibilityStatus) {

		case APTC_CSR:
			handleAPTC_CSRToAPTC_CSR(currentApplication, enrolledApplication, atTaxHHList);
			break;

		case APTC:
			handleCurrentAPTC(currentApplication, enrolledApplication, atTaxHHList);
			break;

		case QHP:
			callMaintenance834();
			break;

		case NONE:
			throw new GIRuntimeException(POSSIBLE_DATA_ISSUE_CURRENT_APP + currentExchangeEligibilityStatus);

		default:
			break;
		}
	}


	private void handleAPTC_CSRToAPTC_CSR(SsapApplication currentApplication,
			SsapApplication enrolledApplication,
			List<TaxHouseholdMember> atTaxHHList) {
		if (!StringUtils.equalsIgnoreCase(currentApplication.getCsrLevel(), enrolledApplication.getCsrLevel())){
			// Allow Enrollment
			LOGGER.debug("APTC_CSR-APTC_CSR + CSRLEVEL changed --> " + enrolledApplication.getCsrLevel() + " " + currentApplication.getCsrLevel());
			setAllowEnrollment(currentApplication, "Y");
		} else {
			LOGGER.debug("APTC_CSR-APTC_CSR + CSRLEVEL NOT changed -->" + enrolledApplication.getCsrLevel() + " " + currentApplication.getCsrLevel());
			boolean isIndividualEligibilityChanged = isIndividualEligibilityChanged(enrolledApplication, atTaxHHList);

			if (isIndividualEligibilityChanged){
				// Allow Enrollment
				LOGGER.debug("APTC_CSR-APTC_CSR + CSRLEVEL NOT changed + IndividualEligibility-Y");
				setAllowEnrollment(currentApplication, "Y");
			} else {
				LOGGER.debug("APTC_CSR-APTC_CSR + CSRLEVEL NOT changed + IndividualEligibility-N");
				boolean isAPTCAmountChanged = isAPTCAmountChanged(currentApplication, enrolledApplication);

				if (isAPTCAmountChanged){
					// Call Maintenance 834 interface
					LOGGER.debug("APTC_CSR-APTC_CSR + CSRLEVEL NOT changed + IndividualEligibility-N + APTCAmountChanged-Y");
					callMaintenance834();
				} else {
					LOGGER.debug("APTC_CSR-APTC_CSR + CSRLEVEL NOT changed + IndividualEligibility-N + APTCAmountChanged-N");
					handleEnrollment(currentApplication, enrolledApplication, atTaxHHList);
				}

			}
		}
	}


	private void handleQHPToOthers(SsapApplication currentApplication,
			SsapApplication enrolledApplication,
			List<TaxHouseholdMember> atTaxHHList,
			ExchangeEligibilityStatus currentExchangeEligibilityStatus) {

		switch (currentExchangeEligibilityStatus) {

		case QHP:
			handleQHPToQHP(currentApplication, enrolledApplication, atTaxHHList);
			break;

		case APTC:
		case APTC_CSR:
			setAllowEnrollment(currentApplication, EligibilityConstants.Y);
			break;

		case NONE:
			throw new GIRuntimeException(POSSIBLE_DATA_ISSUE_CURRENT_APP + currentExchangeEligibilityStatus);

		default:
			break;
		}
	}


	private void handleQHPToQHP(SsapApplication currentApplication,
			SsapApplication enrolledApplication,
			List<TaxHouseholdMember> atTaxHHList) {
		boolean isIndividualEligibilityChanged = isIndividualEligibilityChanged(enrolledApplication, atTaxHHList);

		if (isIndividualEligibilityChanged){
			// Allow Enrollment
			LOGGER.debug("QHP-QHP + IndividualEligibility-Y");
			setAllowEnrollment(currentApplication, "Y");
		} else {
			LOGGER.debug("QHP-QHP + IndividualEligibility-N");
			handleEnrollment(currentApplication, enrolledApplication, atTaxHHList);
		}
	}


	private void handleCurrentAPTC(SsapApplication currentApplication, SsapApplication enrolledApplication, List<TaxHouseholdMember> atTaxHHList) {
		boolean isIndividualEligibilityChanged = isIndividualEligibilityChanged(enrolledApplication, atTaxHHList);

		if (isIndividualEligibilityChanged){
			// Allow Enrollment
			LOGGER.debug("APTC_CSR-APTC   or    APTC-APTC   + IndividualEligibility-Y  ");
			setAllowEnrollment(currentApplication, "Y");
		} else {
			LOGGER.debug("APTC_CSR-APTC   or    APTC-APTC   + IndividualEligibility-N  ");
			boolean isAPTCAmountChanged = isAPTCAmountChanged(currentApplication, enrolledApplication);

			if (isAPTCAmountChanged){
				// Call Maintenance 834 interface
				LOGGER.debug("APTC_CSR-APTC   or    APTC-APTC   + IndividualEligibility-N + APTCAmountChanged-Y");
			} else {
				LOGGER.debug("APTC_CSR-APTC   or    APTC-APTC   + IndividualEligibility-N + APTCAmountChanged-N");
				handleEnrollment(currentApplication, enrolledApplication, atTaxHHList);
			}
		}
	}


	private void handleEnrollment(SsapApplication currentApplication,
			SsapApplication enrolledApplication, List<TaxHouseholdMember> atTaxHHList) {
		// Move enrollment from enrolled application to current application
		moveEnrollment();
		closeApplication(enrolledApplication);
		// Update current application to ENROLLED
		updateCurrentAppToEN(currentApplication);
	}

	private void updateCurrentAppToEN(SsapApplication currentApplication) {
		setApplicationStatus(currentApplication, "EN");
	}


	private void moveEnrollment() {
		// TODO: Demographic changes...
		LOGGER.debug("Calling Enrollment API - Move enrollment from enrolled application to current application");
	}

	private void callMaintenance834(){
		LOGGER.debug("Calling Enrollment API - Maintenance 834 interface");
	}

	private void setApplicationStatus(SsapApplication ssapApplication, String applicationStatus){

		LOGGER.debug("setApplicationStatus - " + applicationStatus );
		ssapApplication.setApplicationStatus(applicationStatus);
		ssapApplicationRepository.save(ssapApplication);

	}
	private void setAllowEnrollment(SsapApplication currentApplication, String isAllowEnrollment){

		LOGGER.debug("isAllowEnrollment - " + isAllowEnrollment );
		currentApplication.setAllowEnrollment(isAllowEnrollment);
		ssapApplicationRepository.save(currentApplication);

	}

	private boolean isAPTCAmountChanged(SsapApplication currentApplication,	SsapApplication enrolledApplication) {

		if (currentApplication.getMaximumAPTC() != null && enrolledApplication.getMaximumAPTC() != null){
			return currentApplication.getMaximumAPTC().compareTo(enrolledApplication.getMaximumAPTC()) == 0 ? false : true;
		}
		return true;
	}


	private boolean isIndividualEligibilityChanged(SsapApplication enrolledApplication, List<TaxHouseholdMember> atTaxHHList) {

		List<com.getinsured.iex.ssap.model.SsapApplicant> enrolledApplicants = ssapApplicantRepository.findBySsapApplication(enrolledApplication);

		int enrolledCount = 0;
		for (SsapApplicant enrolledApplicant : enrolledApplicants) {
			if (StringUtils.equalsIgnoreCase(enrolledApplicant.getApplyingForCoverage(), EligibilityConstants.Y)){
				enrolledCount++;
			}
		}

		if (enrolledCount != atTaxHHList.size()){
			return true;
		}

		// TODO: compare each member based for demographic changes

		return false;
	}
}
