package com.getinsured.hix.agency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.agency.assistant.AgencyAssistant;

@Repository
public interface IAgencyAssistantRepository extends RevisionRepository<AgencyAssistant, Integer, Integer>, JpaRepository<AgencyAssistant, Integer>{
	

	AgencyAssistant findById(Long id);
	
	AgencyAssistant findByAgencyId(Long agencyId); 
	
	AgencyAssistant findByPersonalEmailAddress(String personalEmailAddress);
	
	AgencyAssistant findByUserId(int userId); 

}
