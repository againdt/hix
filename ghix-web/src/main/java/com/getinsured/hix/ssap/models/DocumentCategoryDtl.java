package com.getinsured.hix.ssap.models;

import com.getinsured.hix.model.ConsumerDocument;

public class DocumentCategoryDtl {

	private String documentName;
	private boolean isDocumentVerified;
	private String alfrescoLink;
	private String documentType;
	private boolean isVerified;
	private ConsumerDocument consumerDocument;
	private String encryptedID;
	private String ticketNumber;
	private String encryptedDocPath;
	private boolean showDocument;

	public DocumentCategoryDtl(ConsumerDocument consumerDocument){
		this.consumerDocument = consumerDocument;
	}
	
	public ConsumerDocument getConsumerDocument() {
		return consumerDocument;
	}

	public void setConsumerDocument(ConsumerDocument consumerDocument) {
		this.consumerDocument = consumerDocument;
	}

	
	public DocumentCategoryDtl(String documentName,String documentType){
		this.documentName = documentName;
		this.documentType = documentType;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public boolean isDocumentVerified() {
		return isDocumentVerified;
	}
	public void setDocumentVerified(boolean isDocumentVerified) {
		this.isDocumentVerified = isDocumentVerified;
	}
	public String getAlfrescoLink() {
		return alfrescoLink;
	}
	public void setAlfrescoLink(String alfrescoLink) {
		this.alfrescoLink = alfrescoLink;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public boolean isVerified() {
		return isVerified;
	}

	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public String getEncryptedID() {
		return encryptedID;
	}

	public void setEncryptedID(String encryptedID) {
		this.encryptedID = encryptedID;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getEncryptedDocPath() {
		return encryptedDocPath;
	}

	public void setEncryptedDocPath(String encryptedDocPath) {
		this.encryptedDocPath = encryptedDocPath;
	}

  public boolean showDocument() {
    return showDocument;
  }

  public void setShowDocument(boolean showDocument) {
    this.showDocument = showDocument;
  }
}
