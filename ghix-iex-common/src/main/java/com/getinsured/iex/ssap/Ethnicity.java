package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

public class Ethnicity {

	@Expose
	private String label;
	@Expose
	private String code;
	@Expose
	private String otherLabel;
	
	private boolean comparedLogic;

	public boolean isComparedLogic() {
		return comparedLogic;
	}

	public void setComparedLogic(boolean comparedLogic) {
		this.comparedLogic = comparedLogic;
	}


	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOtherLabel() {
		return otherLabel;
	}

	public void setOtherLabel(String otherLabel) {
		this.otherLabel = otherLabel;
	}
}
