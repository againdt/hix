<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<div class="ps-compare__plan-tiles" id="mainSummaryCmp">
    <div class="ps-compare__placeholder"></div>
</div>

<script type="text/html" id="mainSummaryCmpTpl">
    <div class="ps-compare__plan-tile" tabindex="0" aria-label="You are on plan <@=name@>">
        <!-- tile header -->
		<@ if($('#stateCode').val() != "CA") { @>
			<div class="cp-tile__header">
				<a
					href="#"
					rel="tooltip"
					data-html="true"
					data-placement="bottom"
					data-original-title="<spring:message code="pd.tooltip.summary.estimatedCostDetail"/>">
					<spring:message code="pd.label.title.expenseEstimate"/>
        	   	</a>

				<@ if(expenseEstimate == "Lower Cost"){ @>
					<span class="ps-detail__tile-estimate ps-detail__tile-estimate--lower"><span><spring:message code="pd.label.low"/></span></span>
				<@ } else if(expenseEstimate == "Average"){ @>
					<span class="ps-detail__tile-estimate ps-detail__tile-estimate--average"><span><spring:message code="pd.label.medium"/></span></span>
				<@ } else if(expenseEstimate == "Pricey"){ @>
					<span class="ps-detail__tile-estimate ps-detail__tile-estimate--higher"><span><spring:message code="pd.label.high"/></span></span>
				<@ } @>
			</div>
		<@ } @>
		<!-- tile header end-->

        <!-- tile body -->
        <div class="cp-tile__body">
            <!-- logo img -->
            <div id="detail_<@=id@>" class="cp-tile__img-link cp-tile__img-link-white detail">
                <@ if(issuer.length > 24) { @>
                    <img src="<@=issuerLogo@>" alt="<@=issuer.substr(0,24)@>..." class="cp-tile__img">
                <@ } else { @>
                    <img src="<@=issuerLogo@>" alt="<@=issuer@>" class="cp-tile__img">
                    <!-- <div class="planimg" id="detail_<@=id@>"><img id="detail_<@=id@>" class="planimg carrierlogo hide" src='<@=issuerLogo@>' alt="<@=issuer@>" onload="resizeimage(this)" /></div> -->
                <@ } @>
            </div>
            <!-- logo img end-->

            <!-- plan name -->
            <div class="cp-tile__plan-name">
                <a class="detail" href="#" id="detail_<@=id@>" data-placement="bottom" rel="tooltip" role="tooltip" data-original-title="<@=name@>" aria-label="<@=name@>">
                    <@ if(name.length > 24) { @>
                        <@=name.substr(0,24)@>...
                    <@ } else {@>
                        <@=name@>
                    <@ } @>
                </a>
            </div>
            <!-- plan name end-->


            <!-- metal tier -->
            <div>
                <@ if(level === 'PLATINUM') {@>
                    <span class="cp-tile__metal-tier cp-tile__metal-tier--platinum"><spring:message code="pd.label.filterby.platinum"/> &nbsp;<@=networkType@></span>
                <@ } else if(level === 'GOLD') { @>
                    <span class="cp-tile__metal-tier cp-tile__metal-tier--gold"><spring:message code="pd.label.filterby.gold"/> &nbsp;<@=networkType@></span>
                <@ } else if(level === 'SILVER') { @>
                    <span class="cp-tile__metal-tier cp-tile__metal-tier--silver"><spring:message code="pd.label.filterby.silver"/> &nbsp;<@=networkType@></span>
                <@ } else if(level === 'BRONZE') {@>
                    <span class="cp-tile__metal-tier cp-tile__metal-tier--bronze"><spring:message code="pd.label.filterby.bronze"/> &nbsp;<@=networkType@></span>
                <@ } else if(level === 'CATASTROPHIC') {@>
                    <span class="cp-tile__metal-tier cp-tile__metal-tier--catastrophic"><spring:message code="pd.label.filterby.catastropic"/> &nbsp;<@=networkType@></span>
                <@ } else {@>
                    <span class="cp-tile__metal-tier cp-tile__metal-tier--bronze"><@=level@> &nbsp;<@=networkType@></span>
                <@ } @>

				<@ var planNum = issuerPlanNumber.substr(issuerPlanNumber.length - 2, issuerPlanNumber.length) @>
				<@ if(planNum !== '00' && planNum !== '01') {@>
					<span class="cp-tile__metal-tier cp-tile__metal-tier--csr cp-tile__metal-tier--csr-right ps-csr-show hide"><spring:message code="pd.label.title.csr"/></span>
				<@ } @>
            </div>
            <!-- metal tier end-->
			
			<!-- current plan -->
			<@ if ($("#enrollmentType").val() == 'A' && $("#initialHealthPlanId").val() == planId){ @>
				<div>
					<span class="cp-tile__current-plan">
						<spring:message code="pd.label.title.yourCurrentPlan"/>
					</span>
				</div>
			<@ } @>
			<!-- current plan end -->

            <!-- premium -->
            <div class="cp-tile__premium">
				<span class="cp-tile__premium-amount">$<@=premiumAfterCredit.toFixed(2) @></span>
				<span class="cp-tile__premium-month"><spring:message code="pd.label.title.premiumPerMonth"/></span>
			</div>
            <!-- premium end-->

            <!-- tax credit -->
            <c:if test="${exchangeType != 'OFF' && (maxAptc > 0 || maxStateSubsidy > 0)}">
                <@ var totalCredit = 0; @>
                <@ if (aptc > 0 || $('#stateCode').val() === 'CA') {@>
                <@ totalCredit = totalCredit + (+aptc.toFixed(2)); @>
                <@ } @>
                <@ if(stateSubsidy){ @>
                <@    totalCredit = totalCredit + (+stateSubsidy.toFixed(2)); @>
                <@ } @>
                <div class="cp-tile__tax-credit">
                <@ if (totalCredit !== 0 && stateSubsidy !== null) { @>
                    <a
                    href="#"
                    rel="tooltip"
                    role="tooltip"
                    data-html="true"
                    data-placement="bottom"
                    data-original-title="<table>
                                            <tr>
                                                <td><spring:message code="pd.label.commontext.federal" htmlEscape="false"/>
                                                <td>=</td>
                                                <td>$<@=aptc && aptc.toFixed(2)@><td/>
                                            </tr>
                                            <tr>
                                                <td><spring:message code="pd.label.commontext.state" htmlEscape="false"/>
                                                <td>=</td>
                                                <td>$<@=stateSubsidy && stateSubsidy.toFixed(2)@><td/>
                                            <tr>
                                                <td><spring:message code="pd.label.commontext.total" htmlEscape="false"/>
                                                <td>=</td>
                                                <td>$<@=totalCredit && totalCredit.toFixed(2)@><td/>
                                            </tr>
                                         </table>">
                        <spring:message arguments="<@=totalCredit.toFixed(2)@>" code="pd.label.commontext.afterTaxCredit" htmlEscape="false"/>
                    </a>

                <@ } else { @>
                    <spring:message arguments="<@=totalCredit.toFixed(2)@>" code="pd.label.commontext.afterTaxCredit" htmlEscape="false"/>
                <@ } @>
                </div>
            </c:if>
            <!-- tax credit end -->

        </div>
        <!-- tile body ends -->

        <!-- tile footer -->
        <div class="ps-compare__tile-footer">
            <@ if ($('#stateCode').val() != "CT" && ($("#flowType").val() == 'PLANSELECTION' || $("#flowType").val() == 'PREELIGIBILITY')){ @>
				<@ if ( planId == 0) { @>
				<span id="cart_<@=planId@>" class="btn btn-primary ps-detail__tile-add addToCart ps-prev_year-btn" title="<spring:message code="pd.label.title.prevYearPlan"/>">
                    <spring:message code="pd.label.title.prevYearPlan"/>
                </span>
				<@ } else { @>
				<a href="#" id="cart_<@=planId@>" class="btn btn-primary ps-detail__tile-add addToCart" title="<spring:message code="pd.label.title.add"/>">
                    <spring:message code="pd.label.title.add"/>
                    <i class="icon-shopping-cart"></i>
                </a>
                <@ } @>
            <@ } @>
		</div>
        <!-- tile footer ends -->
    </div>

    <!-- <div class="plan-details">
        <div class="remove"><i class="icon-remove-sign"></i></div>
        <div class="tile-header">
        <@ if ($('#stateCode').val() != "CT" && ($("#flowType").val() == 'PLANSELECTION' || $("#flowType").val() == 'PREELIGIBILITY')){ @>
        <div><a class="addToCart btn btn-small ie10" title="<spring:message code='pd.label.title.addToCart'/>" href="#" id="cart_<@=planId@>">
        <span><spring:message code="pd.label.title.addToCart" htmlEscape="true"/></span> <i class="icon-shopping-cart"></i>
        </a></div>
        <@ } @>

        <a class="removeCompare" href="#" id="removeChk_<@=id@>"> <spring:message code="pd.label.title.removeFromComapre"/></a>
        </div>
        </div>
    </div> -->
</script>


<div id="benefitHead" style="display:none">
  <div class="accordion benefits" role="tablist">
    <div class="accordion-group"><!-- Summary -->
      <div class="accordion-heading"  aria-controls="collapseOne" >
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseOne" aria-expanded="true">
          <i class="icon-chevron-down"></i> <spring:message code="pd.label.title.summary"/>
        </a>
      </div>
      <div id="collapseOne" class="accordion-body collapse in" aria-expanded="true">
        <div class="accordion-inner">
          <div class="row-fluid" id="summaryCompare">
            <div class="u-flex-display" id="smartScoreRow">
              <div class="u-flex-25">
              	<c:if test="${gpsVersion == 'V1'}">
              	  <p><spring:message code="pd.label.title.getInsuredPlanScore"/></p>
              	</c:if>
              </div>
            </div>

            <c:if test="${stateCode != 'MN' }">
                <div class="u-flex-display" id="estimatedCost">
                  <div class="u-flex-25">
                    <p><a href="#" rel="tooltip" data-html="true" data-placement="right" data-original-title="<spring:message code="pd.tooltip.summary.estimatedCostDetail" htmlEscape="true"/>">
                    <spring:message code="pd.label.title.expenseEstimate"/></a></p>
                  </div>
                </div>
            </c:if>

            <div class="u-flex-display" id="providerSearch">
              <div class="u-flex-25">
                <p><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.summary.providersLink" htmlEscape="true"/>"><spring:message code="pd.label.summary.link5"/> </a></p>
              </div>
            </div>
            <div class="u-flex-display" id="productTypeCompare">
              <div class="u-flex-25">
                <p><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.summary.producttype" htmlEscape="true"/>"><spring:message code="pd.label.commontext.productType"/> </a></p>
              </div>
            </div>
            <c:if test="${exchangeType != 'OFF' && PGRM_TYPE == 'Individual'}">
            <div class="u-flex-display" id="discounts">
              <div class="u-flex-25"><p><a data-placement="right" rel="tooltip" href="#" class="dotted" data-original-title="<spring:message code="pd.label.tooltip.summary.discount" htmlEscape="true"/>"><spring:message code="pd.label.summary.link6"/></a></p></div>
            </div></c:if>

            <div class="u-flex-display" id="hsaType">
 	 	 	 <div class="u-flex-25">
 	 	 	 	<p>
 	 	 	 		<c:if test="${stateCode != 'ID'}">
	 	 	 	 		<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.title.hsaQualifiedtooltip" htmlEscape="true"/>">
	 	 	 	 	</c:if>
	 	 	 		<spring:message code="pd.label.commontext.hsatype"/>
	 	 	 	 	<c:if test="${stateCode != 'ID'}">	
	 	 	 	 		</a>
	 	 	 	 	</c:if>
 	 	 	 	</p>
			 </div>
 	 	 	</div>
 	 	 	<c:if test="${showNetworkTransparencyRating == 'ON'}">
 	 	 	<div class="u-flex-display" id="networkTransparencyRating">
 	 	 	 <div class="u-flex-25">
 	 	 	 	<p><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.tooltip.network" htmlEscape="true"/>"><spring:message code="pd.label.network"/> </a></p>
			 </div>
 	 	 	</div>
            </c:if>
 	 	 	<c:if test="${showQualityRating == 'YES'}">
            <div class="u-flex-display" id="qualityRating">
 	 	 	 <div class="u-flex-25">
 	 	 	 	<p><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.qualityRating.tooltip" htmlEscape="true"/>"><spring:message code="pd.label.tooltip.qualityRatings.1"/> </a></p>
			 </div>
 	 	 	</div>
            </c:if>
          </div>
        </div>
      </div>
    </div>  <!-- Summary -->


    <!-- Sample Care Cost starts -->
    <%-- <c:if test="${showSBCScenarios == 'ON'}">
    <div class="accordion-group">
      <div class="accordion-heading"  aria-controls="collapseSampleCareCosts" >
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseSampleCareCosts" aria-expanded="true">
          <i class="icon-chevron-down"></i> <spring:message code="pd.label.title.SampleCareCosts"/>
        </a>
      </div>
      <div id="collapseSampleCareCosts" class="accordion-body collapse in" aria-expanded="true">
        <div class="accordion-inner">
          <div class="row-fluid">
            <div class="u-flex-display" id="havingBabyCompare">
              <div class="u-flex-25">
				<p>
              	  <spring:message code="pd.health.label.havingABaby"/>
				</p>
              </div>
            </div>
            <div class="u-flex-display" id="havingDiabetesCompare">
              <div class="u-flex-25">
              	<p>
              	  <spring:message code="pd.health.label.havingDiabetes"/>
             	</p>
              </div>
            </div>
            <div class="u-flex-display" id="treatmentFractureCompare">
              <div class="u-flex-25">
              	<p>
              	  <spring:message code="pd.health.label.treatmentOfASimpleFracture"/>
              	</p>  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </c:if> --%>
    <!-- Sample Care cost ends -->

    <!-- Doctors and Facilities starts -->
	<c:if test="${providerSearchConfig == 'ON'}">
		<div class="accordion-group">
			<div class="accordion-heading">
				<a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseDoctorsFacilities" aria-expanded="true">
					<i class="icon-chevron-down"></i> <spring:message code="pd.label.title.doctorsandfacilities"/>
				</a>
			</div>
			<div id="collapseDoctorsFacilities" class="accordion-body collapse in" aria-expanded="true">
	        	<div class="accordion-inner">

	            		<div id="providerNames"></div>

	            <!-- Heat Map -->
	            <c:if test="${providerMapConfig == 'ON'}">
	            <div class="row-fluid">
    	   				<div class="u-flex-display" id="providerHeatMap">
	              			<div class="u-flex-25"><label for="providerDistance"><spring:message code="pd.provider.label.physiciansIn"/></label>
	              			<select class="input-medium" id="providerDistance" name="providerDistance" onchange="App.views.compareView.comparePlans();">
	              				<option value="1"><spring:message code="pd.label.preferences.radius1"/></option>
								<option value="2"><spring:message code="pd.label.preferences.radius2"/></option>
								<option value="5"><spring:message code="pd.label.preferences.radius5"/></option>
								<option selected value="10"><spring:message code="pd.label.preferences.radius10"/></option>
								<option value="20"><spring:message code="pd.label.preferences.radius20"/></option>
								<option value="30"><spring:message code="pd.label.preferences.radius30"/></option>
								<option value="50"><spring:message code="pd.label.preferences.radius50"/></option>
								<option value="100"><spring:message code="pd.label.preferences.radius100"/></option>
							</select> <spring:message code="pd.provider.label.of"/> ${zipcode}</div>
	            		</div>
	            	</div>
	            </c:if>
				</div> <%-- .accordion-inner --%>
			</div> <%-- .accordion-body --%>
		</div> <%-- .accordion-group --%>
	</c:if>
<!-- Doctors and Facilities ends -->
    <c:choose>
    <c:when test="${simplifiedDeductible == 'ON' }">
    <div class="accordion-group">
      <div class="accordion-heading"  aria-controls="collapseTwo" >
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseTwo" aria-expanded="true">
          <i class="icon-chevron-down"></i> <spring:message code="pd.label.commontext.deductible"/> &amp; <spring:message code="pd.label.icon.outOfPocket"/>&nbsp;&nbsp;
            (<spring:message code="pd.label.title.inNetwork"/>)
        </a>
      </div>
      <div id="collapseTwo" class="accordion-body collapse in" aria-expanded="true">
        <div class="accordion-inner">
          <div class="row-fluid">
            <div class="u-flex-display" id="simplifiedDeductible">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.Deductible"/></p>
              </div>
            </div>
            <div class="u-flex-display" id="simplifiedSeparateDrugDeductible">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.separate.drugdeductible"/></p>
              </div>
            </div>
            <div class="u-flex-display" id="simplifiedOOPMax">
              <div class="u-flex-25">
                <p>
                	<c:if test="${stateCode != 'CA'}">
	                	<a
							href="#"
							rel="tooltip"
							role="tooltip"
							data-placement="right"
							data-html="true"
							data-original-title="<spring:message code="pd.tooltip.oopmax"/>" aria-label="<spring:message code="pd.label.oopmax"/> help text:<spring:message code="pd.tooltip.oopmax"/> help text finished">
					</c:if>			
                	<spring:message code="pd.label.deductible.oopmax"/>
                	<c:if test="${stateCode != 'CA'}">	
                		</a>
                	</c:if>	
                </p>
              </div>
            </div>

            <c:if test="${stateCode == 'CA'}">
	           <div class="u-flex-display" id="simplifiedMaxCostPerPrescription">
	              <div class="u-flex-25">
	               <p><spring:message code="pd.label.maxCostPerPrescription"/></p>
	             </div>
	           </div>

	           <div class="u-flex-display" id="simplifiedOtherDeductible">
	            <div class="u-flex-25">
	              <p><spring:message code="pd.label.deductible.otherDeductibles"/></p>
	            </div>
	          </div>

         	 </c:if>
        </div>
      </div>
    </div>
    </div>
    </c:when>
    <c:otherwise>
    <div class="accordion-group">
      <div class="accordion-heading">
        <a class="accordion-toggle"  data-toggle="collapse" data-parent="#benefitHead" href="#collapseTwo" aria-expanded="false">
          <i class="icon-chevron-down"></i> <spring:message code="pd.label.commontext.deductible"/> &amp; <spring:message code="pd.label.icon.outOfPocket"/>&nbsp;&nbsp;
          (<spring:message code="pd.label.title.inNetwork"/>)
        </a>
      </div>
      <div id="collapseTwo" class="accordion-body collapse in" aria-expanded="false">
        <div class="accordion-inner">
          <div class="row-fluid">
            <div class="u-flex-display" id="planDeductible1">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.deductible.link1"/> (<spring:message code="pd.label.commontext.individual"/>)</p>
              </div>
            </div>
            <div class="u-flex-display" id="planDeductible2">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.deductible.link2"/> (<spring:message code="pd.label.commontext.individual"/>)</p>
              </div>
            </div>
            <div class="u-flex-display" id="planDeductible3">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.deductible.link1"/> (<spring:message code="pd.label.commontext.family"/>) </p>
              </div>
            </div>
            <div class="u-flex-display" id="planDeductible4">
              <div class="u-flex-25">
               <p><spring:message code="pd.label.deductible.link2"/> (<spring:message code="pd.label.commontext.family"/>)</p>
             </div>
           </div>
           <div class="u-flex-display" id="planDeductible5">
            <div class="u-flex-25">
              <p><spring:message code="pd.label.deductible.link3"/> (<spring:message code="pd.label.commontext.individual"/>) </p>
            </div>
          </div>
          <div class="u-flex-display" id="planDeductible6">
            <div class="u-flex-25">
              <p><spring:message code="pd.label.deductible.link4"/> (<spring:message code="pd.label.commontext.individual"/>)</p>
            </div>
          </div>
          <div class="u-flex-display" id="planDeductible7">
            <div class="u-flex-25">
              <p><spring:message code="pd.label.deductible.link5"/> (<spring:message code="pd.label.commontext.individual"/>) </p>
            </div>
          </div>
          <div class="u-flex-display" id="planDeductible8">
            <div class="u-flex-25">
              <p><spring:message code="pd.label.deductible.link6"/> (<spring:message code="pd.label.commontext.individual"/>)</p>
            </div>
          </div>
          <div class="u-flex-display" id="planDeductible9">
            <div class="u-flex-25">
              <p><spring:message code="pd.label.deductible.link3"/> (<spring:message code="pd.label.commontext.family"/>) </p>
            </div>
          </div>
          <div class="u-flex-display" id="planDeductible10">
            <div class="u-flex-25">
              <p><spring:message code="pd.label.deductible.link4"/> (<spring:message code="pd.label.commontext.family"/>)</p>
            </div>
          </div>
          <div class="u-flex-display" id="planDeductible11">
            <div class="u-flex-25">
              <p><spring:message code="pd.label.deductible.link5"/> (<spring:message code="pd.label.commontext.family"/>) </p>
            </div>
          </div>
          <div class="u-flex-display" id="planDeductible12">
            <div class="u-flex-25">
              <p><spring:message code="pd.label.deductible.link6"/> (<spring:message code="pd.label.commontext.family"/>)</p>
            </div>
          </div>
          <div class="u-flex-display" id="planDeductibleOther">
            <div class="u-flex-25">
              <p><spring:message code="pd.label.deductible.linkOther"/></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    </c:otherwise>
    </c:choose>
    <div class="accordion-group">
      <div class="accordion-heading"  aria-controls="collapseThree" >
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseThree" aria-expanded="false">
          <i class="icon-chevron-right"></i> <spring:message code="pd.label.icon.doctorVisit"/>
        </a>
      </div>
      <div id="collapseThree" class="accordion-body collapse"  aria-expanded="false">
        <div class="accordion-inner">
          <div class="row-fluid">

            <div class="u-flex-display" id="doctorVisit1">
              <div class="u-flex-25">
                <p>
					<c:if test="${stateCode != 'ID' }">                
                		<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.doctorVisit.link1tooltip" htmlEscape="false"/>">
                	</c:if>
                	<spring:message code="pd.label.doctorVisit.link1"/>
                	<c:if test="${stateCode != 'ID' }"> 
                		</a>
                	</c:if>
                </p>
              </div>
            </div>
            <div class="u-flex-display" id="doctorVisit2">
              <div class="u-flex-25">
                <p>	
                	<c:if test="${stateCode != 'ID' }">					
                		<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.doctorVisit.link2tooltip" htmlEscape="false"/>">
                	</c:if>
                	<spring:message code="pd.label.doctorVisit.link2"/>
                	<c:if test="${stateCode != 'ID' }">
                		</a>  
                	</c:if>              
				</p>
              </div>
            </div>

            <c:if test="${stateCode != 'CT' }">
	            <div class="u-flex-display" id="doctorVisit3">
	              <div class="u-flex-25">
	                <p>
	                	<c:if test="${stateCode != 'ID' }">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.doctorVisit.link3tooltip" htmlEscape="false"/>">
						</c:if>
						<spring:message code="pd.label.doctorVisit.link3"/>
						<c:if test="${stateCode != 'ID' }">
							</a>
						</c:if>
	                </p>
	              </div>
	            </div>
            </c:if>

            <div class="u-flex-display" id="doctorVisit4">
              <div class="u-flex-25">
                <p>
                	<c:if test="${stateCode != 'ID' }">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.doctorVisit.link4tooltip" htmlEscape="false"/>">
					</c:if>
					<spring:message code="pd.label.doctorVisit.link4"/>
					
					<c:if test="${stateCode != 'ID' }">
						</a>
					</c:if>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="accordion-group">
      <div class="accordion-heading"  aria-controls="collapseFour" >
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseFour" aria-expanded="false">
          <i class="icon-chevron-right"></i> <spring:message code="pd.label.icon.tests"/>
        </a>
      </div>
      <div id="collapseFour" class="accordion-body collapse" aria-expanded="false">
        <div class="accordion-inner">
          <div class="row-fluid">
          	<c:if test="${stateCode != 'MN' }">
	            <div class="u-flex-display" id="test1">
	              <div class="u-flex-25">
	                <spring:message code="pd.label.tests.link1"/>
	              </div>
	            </div>
            </c:if>
            <div class="u-flex-display" id="test2">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.tests.link2"/></p>
              </div>
            </div>
            <div class="u-flex-display" id="test3">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.tests.link3"/> </p>
              </div>
            </div>
          </div>
        </div>
      </div>
	</div>

      <div class="accordion-group">
        <div class="accordion-heading"  aria-controls="collapseFive" >
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseFive" aria-expanded="false">
            <i class="icon-chevron-right"></i> <spring:message code="pd.label.icon.drugs"/>
          </a>
        </div>
        <div id="collapseFive" class="accordion-body collapse" aria-expanded="false">
          <div class="accordion-inner">
            <div class="row-fluid">
            <div class="u-flex-display" id="drug1">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.drugs.link1"/></p>
              </div>
            </div>
            <div class="u-flex-display" id="drug2">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.drugs.link2"/></p>
              </div>
            </div>
            <div class="u-flex-display" id="drug3">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.drugs.link3"/></p>
              </div>
            </div>
            <div class="u-flex-display" id="drug4">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.drugs.link4"/> </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>

      <div class="accordion-group">
        <div class="accordion-heading"  aria-controls="collapseSix" >
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseSix" aria-expanded="false">
            <i class="icon-chevron-right"></i>  <spring:message code="pd.label.icon.outpatient"/>
          </a>
        </div>
        <div id="collapseSix" class="accordion-body collapse" aria-expanded="false">
          <div class="accordion-inner">
            <div class="row-fluid">
            
            <c:if test="${stateCode != 'MN' }">
	            <div class="u-flex-display" id="outpatient1">
	              <div class="u-flex-25">
	                <spring:message code="pd.label.outpatient.link1"/>
	              </div>
	            </div>
			</c:if>
			
            <c:if test="${stateCode != 'CT' }">
	            <div class="u-flex-display" id="outpatient2">
	              <div class="u-flex-25">
	                <spring:message code="pd.label.outpatient.link2"/>
	              </div>
	            </div>
				<c:if test="${stateCode != 'MN' && stateCode != 'NV'}">
		            <div class="u-flex-display" id="outpatientServicesOfficeVisits">
		              <div class="u-flex-25">
		                <spring:message code="pd.label.outpatient.officeVisits"/>
		              </div>
		            </div>
	            </c:if>
            </c:if>
          </div>
        </div>
      </div>
      </div>

      <div class="accordion-group">
        <div class="accordion-heading"  aria-controls="collapseSeven" >
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseSeven" aria-expanded="false">
            <i class="icon-chevron-right"></i> <spring:message code="pd.label.icon.er"/> &amp; <spring:message code="pd.label.icon.urgentCare"/>
          </a>
        </div>
        <div id="collapseSeven" class="accordion-body collapse" aria-expanded="false">
          <div class="accordion-inner">
            <div class="row-fluid">
              <div class="u-flex-display" id="urgent1">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.urgentCare.link1"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="urgent2">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.urgentCare.link2"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="urgent3">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.urgentCare.link3"/></p>
                </div>
              </div>

              <c:if test="${stateCode != 'CT' && stateCode != 'MN' && stateCode != 'NV'}">
              	<div class="u-flex-display" id="urgentProfessionalFee">
                	<div class="u-flex-25">
                  		<p><spring:message code="pd.label.urgentCare.professionalFee"/></p>
                	</div>
              	</div>
              </c:if>

            </div>
          </div>
        </div>
      </div>

      <div class="accordion-group">
        <div class="accordion-heading"  aria-controls="collapseEight" >
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseEight" aria-expanded="false">
            <i class="icon-chevron-right"></i> <spring:message code="pd.label.icon.hospital"/>
          </a>
        </div>
        <div id="collapseEight" class="accordion-body collapse" aria-expanded="false">
          <div class="accordion-inner">
            <div class="row-fluid">
              <div class="u-flex-display" id="hospital1">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.hospital.link1"/></p>
                </div>
              </div>

              <c:if test="${stateCode != 'CT' }">
	              <div class="u-flex-display" id="hospital2">
	                <div class="u-flex-25">
	                  <p><spring:message code="pd.label.hospital.link2"/></p>
	                </div>
	              </div>
              </c:if>

            </div>
          </div>
        </div>
      </div>

	<c:if test="${stateCode != 'CT' }">
      <div class="accordion-group">
        <div class="accordion-heading"  aria-controls="collapseNine" >
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseNine" aria-expanded="false">
            <i class="icon-chevron-right"></i> <spring:message code="pd.label.icon.health"/>
          </a>
        </div>
        <div id="collapseNine" class="accordion-body collapse" aria-expanded="false">
          <div class="accordion-inner">
            <div class="row-fluid">
              <div class="u-flex-display" id="mentalHealth1">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.mentalHealth.link1"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="mentalHealth2">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.mentalHealth.link2"/></p>
                </div>
              </div>
              <c:if test="${stateCode != 'MN' && stateCode != 'NV'}">
	              <div class="u-flex-display" id="mentalHealthInpatientProfFee">
	                <div class="u-flex-25">
	                  <p><spring:message code="pd.label.mentalHealth.inpatientProfFee"/></p>
	                </div>
	              </div>
              </c:if>
              <div class="u-flex-display" id="mentalHealth3">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.mentalHealth.link3"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="mentalHealth4">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.mentalHealth.link4"/></p>
                </div>
              </div>
              <c:if test="${stateCode != 'MN' && stateCode != 'NV'}">
	              <div class="u-flex-display" id="mentalHealthSubDisorderInpProfFee">
	                <div class="u-flex-25">
	                  <p><spring:message code="pd.label.mentalHealth.subsDisorderInpProfFee"/></p>
	                </div>
	              </div>
              </c:if>
            </div>
          </div>
        </div>
      </div>

      <div class="accordion-group">
        <div class="accordion-heading"  aria-controls="collapseTen" >
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseTen" aria-expanded="false">
            <i class="icon-chevron-right"></i> <spring:message code="pd.label.icon.pregnancy"/>
          </a>
        </div>
        <div id="collapseTen" class="accordion-body collapse" aria-expanded="false">
          <div class="accordion-inner">
            <div class="row-fluid">
            	<c:if test="${stateCode != 'MN'}">	
	            	<div class="u-flex-display" id="pregnancy1">
	                	<div class="u-flex-25">
	                  		<p><spring:message code="pd.label.pregnancy.link1"/></p>
	                	</div>
	              	</div>
				</c:if>
              <div class="u-flex-display" id="pregnancy2">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.pregnancy.link2"/></p>
                </div>
              </div>
              <c:if test="${stateCode != 'MN' && stateCode != 'NV'}">	
	              <div class="u-flex-display" id="pregnancyInpatientProfFee">
	                <div class="u-flex-25">
	                  <p><spring:message code="pd.label.pregnancy.inpatientProfFee"/></p>
	                </div>
	              </div>
              </c:if>
            </div>
          </div>
        </div>
      </div>

      <div class="accordion-group">
        <div class="accordion-heading"  aria-controls="collapseElven" >
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseEleven" aria-expanded="false">
            <i class="icon-chevron-right"></i> <spring:message code="pd.label.icon.otherSpecialNeeds"/>
          </a>
        </div>
        <div id="collapseEleven" class="accordion-body collapse" aria-expanded="false">
          <div class="accordion-inner">
            <div class="row-fluid">
              <div class="u-flex-display" id="specialNeed1">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.otherSpecialNeeds.link1"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="specialNeed2">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.otherSpecialNeeds.link2"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="specialNeed3">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.otherSpecialNeeds.link3"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="specialNeed4">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.otherSpecialNeeds.link4"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="specialNeed5">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.otherSpecialNeeds.link5"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="specialNeed6">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.otherSpecialNeeds.link6"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="specialNeed7">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.otherSpecialNeeds.link7"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="specialNeed8">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.otherSpecialNeeds.link8"/></p>
                </div>
              </div>
              <c:if test="${stateCode != 'MN' }">
	              <div class="u-flex-display" id="specialNeed9">
	                <div class="u-flex-25">
	                  <p><spring:message code="pd.label.otherSpecialNeeds.link9"/></p>
	                </div>
	              </div>
              </c:if>
              <div class="u-flex-display" id="specialNeed10">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.otherSpecialNeeds.link10"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="specialNeed11">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.otherSpecialNeeds.link11"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="specialNeed12">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.otherSpecialNeeds.link12"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="specialNeed13">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.otherSpecialNeeds.link13"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="specialNeed14">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.otherSpecialNeeds.link14"/></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="accordion-group">
        <div class="accordion-heading"  aria-controls="collapseTwelve" >
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseTwelve" aria-expanded="false">
            <i class="icon-chevron-right"></i> <spring:message code="pd.label.icon.children.vision"/>
          </a>
        </div>
        <div id="collapseTwelve" class="accordion-body collapse" aria-expanded="false">
          <div class="accordion-inner">
            <div class="row-fluid">
              <div class="u-flex-display" id="childrensVision1">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.vision.link1"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="childrensVision2">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.vision.link2"/></p>
                </div>
              </div>
              <c:if test="${stateCode == 'MN'}">
              <div class="u-flex-display" id="hearingAids">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.hearing.link1"/></p>
                </div>
              </div>
              </c:if>
            </div>
          </div>
        </div>
      </div>
	</c:if>

      <div class="accordion-group">
        <div class="accordion-heading"  aria-controls="collapseThirteen" >
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseThirteen" aria-expanded="false">
            <i class="icon-chevron-right"></i> <spring:message code="pd.label.icon.children.dental"/>
          </a>
        </div>
        <div id="collapseThirteen" class="accordion-body collapse" aria-expanded="false">
          <div class="accordion-inner">
            <div class="row-fluid">
              <div class="u-flex-display" id="childrensDental1">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.link1"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="childrensDental2">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.link2"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="childrensDental3">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.link3"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="childrensDental4">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.link4"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="childrensDental5">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.link5"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="childrensDental6">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.link6"/></p>
                </div>
              </div>

              <c:if test="${stateCode != 'CT' && stateCode != 'MN'}">
	              <div class="u-flex-display" id="childrensDental7">
	                <div class="u-flex-25">
	                  <p><spring:message code="pd.label.children.dental.link7"/></p>
	                </div>
	              </div>
              </c:if>
			  <c:if test="${stateCode != 'MN' }">
	              <div class="u-flex-display" id="childrensDental8">
	                <div class="u-flex-25">
	                  <p><spring:message code="pd.label.children.dental.link8"/></p>
	                </div>
	              </div>
              </c:if>	
              
              <div class="u-flex-display" id="childrensDental9">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.link9"/></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--
<script type="text/html" id="mainSummaryCmpTpl">
  <div class="plan-details">
  <div class="remove"><i class="icon-remove-sign"></i></div>
    <div class="tile-header">
    <@ if ($('#stateCode').val() != "CT" && ($("#flowType").val() == 'PLANSELECTION' || $("#flowType").val() == 'PREELIGIBILITY')){ @>
        <div><a class="addToCart btn btn-small ie10" title="<spring:message code='pd.label.title.addToCart'/>" href="#" id="cart_<@=planId@>">
            <span><spring:message code="pd.label.title.addToCart" htmlEscape="true"/></span> <i class="icon-shopping-cart"></i>
        </a></div>
    <@ } @>
    <@ if (issuer.length > 24) {@>
        <div class="planimg"><img class="carrierlogo hide" src='<@=issuerLogo@>' alt="<@=issuer.substr(0,18)@>..." onload="resizeimage(this)" /></div>
      <@ } else { @>
        <div class="planimg"><img class="carrierlogo hide" src='<@=issuerLogo@>' alt="<@=issuer@>" onload="resizeimage(this)" /></div>
      <@ } @>
      <p><a class="detail" href="#" id="detail_<@=id@>" data-placement="bottom" rel="tooltip" data-original-title="<@=name@>">
        <@ if (name.length > 24) {@>
          <@=name.substr(0,24)@>...
        <@ } else { @>
          <@=name@>
        <@ } @>
      </a></p>
	<small><@=level@> &nbsp; <@=networkType@></small>
      <div class="payment">
        <h3>$<@=premiumAfterCredit.toFixed(2) @></h3><span>/month</span><br>
		<@ if($('#stateCode').val() == "CA") { @>
		<p class="plan-options__tile__notes"><spring:message code="pd.label.title.aftertax1"/> $<@=aptc.toFixed(2)@> <spring:message code="pd.label.title.aftertax2"/></p></td>
		 <@ }  @>
		<a class="removeCompare" href="#" id="removeChk_<@=id@>"> <spring:message code="pd.label.title.removeFromComapre"/></a>
      </div>
    </div>
  </div>
</script>
-->

<script type="text/html" id="smartScoreTpl">
<@ if($("#gpsVersion").val() == 'V1' && SMART_SCORE != '0') { @>
  <div class="plan-details">
    <@ if(SMART_SCORE >=80){ @>
      <div class="tile best">
		<a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.green" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
    <@ } else if(SMART_SCORE >=60){ @>
      <div class="tile ok">
		<a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.yellow" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">

    <@ } else { @>
      <div class="tile poor">
 		<a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.red" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
  <@ } @>

    <div class="score">
	  <div class="circle-wrapper">
	  	<div class="center-circle"></div>
	    <div class="slice">
	      <div class="pie score_<@=SMART_SCORE@> pie-color"></div>
	    </div>
	    <div class="circle-core center-circle">
	      <div class="circle-num"><@=SMART_SCORE@></div>
	    </div>
	  </div>
	</div>
  </a></div>
</div>
<@ } @>
</script>

<script type="text/html" id="estimatedCostTpl">
  <div class="plan-details u-flex-25-gs">
	<@ if($('#stateCode').val() != "ID") { @>
	  <@ if(EXPENSE_ESTIMATE != null && EXPENSE_ESTIMATE != ""){@>
	    <a href="#" class="expense" rel="tooltip" data-html="true" data-placement="top" data-original-title="<spring:message arguments="<@=parseFloat(ANNUAL_PREMIUM).toFixed(2)@>,<@=parseFloat(EXPENSE_ESTIMATE-ANNUAL_PREMIUM).toFixed(2)@>, <@=parseFloat(EXPENSE_ESTIMATE).toFixed(2)@>" code="pd.tooltip.summary.dynamicCostDetail"/>">
            	$<@=parseFloat(EXPENSE_ESTIMATE).toFixed(2)@>
        </a>
	  <@ } else { @>
	    <p><spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/></p>
	  <@ } @>
	<@ } else { @>
	  <@ if(EXPENSE_ESTIMATE == "Lower Cost"){ @>
      	<span class="best"><spring:message code="pd.label.low"/> <i class="icon-flag"></i>
  	  <@ } else if(EXPENSE_ESTIMATE == "Average"){ @>
    	<span class="ok"><spring:message code="pd.label.average"/> <i class="icon-flag"></i>
  	  <@ } else if(EXPENSE_ESTIMATE == "Pricey"){ @>
    	<span class="poor"><spring:message code="pd.label.high"/> <i class="icon-flag"></i>
 	  <@ } else if(EXPENSE_ESTIMATE === null || EXPENSE_ESTIMATE === "" ||  EXPENSE_ESTIMATE === " " ){ @>
		<span><spring:message code="pd.label.commontext.notAvailable"/></span>
  	  <@ } else { @>
      	<@=EXPENSE_ESTIMATE@>
      <@ } @>
	<@ } @>
  </div>
</script>

<script type="text/html" id="hsaTypeCompareTpl">
 	 <div class="plan-details u-flex-25-gs">
	 	 <@=HSA@>
 	 </div>
</script>
<script type="text/html" id="networkTransparencyCompareTpl">
<c:if test="${showNetworkTransparencyRating == 'ON'}">
 	 <div class="plan-details u-flex-25-gs">
	 	<@ if(networkTransparencyRating === null){ @>
			<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
		<@ } else {@>
           	<@ if(networkTransparencyRating.toUpperCase() === 'BASIC'){@>
           		<a href="#" rel="tooltip" data-placement="right" data-html="true" data-original-title="<spring:message code="pd.tooltip.network.basic" htmlEscape="true"/>"><spring:message code="pd.network.basic" htmlEscape="true"/></a>
           	<@ } else if(networkTransparencyRating.toUpperCase() === 'STANDARD') { @>
           		<a href="#" rel="tooltip" data-placement="right" data-html="true" data-original-title="<spring:message code="pd.tooltip.network.standard" htmlEscape="true"/>"><spring:message code="pd.network.standard" htmlEscape="true"/></a>
           	<@ } else if(networkTransparencyRating.toUpperCase() === 'BROAD') { @>
           		<a href="#" rel="tooltip" data-placement="right" data-html="true" data-original-title="<spring:message code="pd.tooltip.network.broad" htmlEscape="true"/>"><spring:message code="pd.network.broad" htmlEscape="true"/></a>
           	<@ } @>
        <@ } @>
 	 </div>
</c:if>
</script>

<script type="text/html" id="qualityRatingTpl">
	<c:if test="${showQualityRating == 'YES'}">
 	 	<div class="plan-details u-flex-25-gs">
			<@ if (issuerQualityRating1 != null) { @>
				<@= populateQualityRatingsHtml(issuerQualityRating1)@>
			<@ } else { @>
				<div class="noStarsMessage">
					<c:if test="${stateCode == 'CA'}">
						<a data-placement="top" rel="tooltip" role="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.qualityRatings.9"/>" aria-label="<spring:message code="pd.label.tooltip.qualityRatings.9"/>">
					</c:if>
					<spring:message code="pd.label.tooltip.qualityRatings.7"/>
					<c:if test="${stateCode == 'CA'}">
						</a>
					</c:if>
				</div>
			<@ } @>

 	 	</div>
	</c:if>
</script>

<script type="text/html" id="productTypeTpl">
 <@ if($(".smartScoreVal").val() >= 80) { @>
 	      <div class="plan-details best u-flex-25-gs">
 	 <@ } else if($(".smartScoreVal").val() >= 60) { @>
 	      <div class="plan-details ok u-flex-25-gs">
 	 <@ } else { @>
    	   <div class="plan-details poor u-flex-25-gs">
  <@ } @>
	<@ if(NETWORK_TYPE == 'PPO') { @> <a data-placement="top" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.ppo" htmlEscape="true"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'HMO') { @><a data-placement="top" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.hmo" htmlEscape="true"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'POS') { @><a data-placement="top" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.pos" htmlEscape="true"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'DHMO') { @><a data-placement="top" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.dhmo" htmlEscape="true"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'DPPO') { @><a data-placement="top" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.dppo" htmlEscape="true"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'HSA') { @><a data-placement="top" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.hsa" htmlEscape="true"/>"><@=NETWORK_TYPE@></a>
    <@ } else { @> <@=NETWORK_TYPE@> <@ } @>
  </div>
</script>
<script type="text/html" id="discountTpl">
   <@ if($(".smartScoreVal").val() >= 80) { @>
 	      <div class="plan-details best">
 	 <@ } else if($(".smartScoreVal").val() >= 60) { @>
 	      <div class="plan-details ok">
 	 <@ } else { @>
    	   <div class="plan-details poor">
  <@ } @>
	<@ if($('#exchangeType').val() != 'OFF') {@>
      <@ if($('#csr').val() != null && $('#csr').val() != "" && $('#csr').val() != "CS1") { @>
		<@ if(($('#csr').val() == "CS2" || $('#csr').val() == "CS3") && LEVEL != 'CATASTROPHIC'  ) { @>
		  <p><spring:message code="pd.label.script.specialSavings" htmlEscape="true"/></p>
		<@ } else if(($('#csr').val() == "CS4" || $('#csr').val() == "CS5"  || $('#csr').val() == "CS6") && LEVEL == 'SILVER'  ) { @>
		  <p><spring:message code="pd.label.script.specialSavings" htmlEscape="true"/></p>
		<@ } else { @>
          <p><spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/></p>
      	<@ } @>
      <@ } else { @>
        <p><spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/></p>
      <@ } @>
    <@ } @>
  </div>
</script>

<script type="text/html" id="havingBabyTpl">
 	 <div class="plan-details u-flex-25-gs">
	 	 <@ if(TOTAL_COST !== null && TOTAL_COST !== "") { @>
	<p><span>$<@=TOTAL_COST@></span></p>
<@ } else { @>
	  <small><spring:message code="pd.label.commontext.notAvailable" /></small>
    <@ } @>
 	 </div>
</script>
<script type="text/html" id="havingDiabetesTpl">
 	 <div class="plan-details u-flex-25-gs">
	 	 <@ if(TOTAL_COST !== null && TOTAL_COST !== "") { @>
	<p><span>$<@=TOTAL_COST@></span></p>
<@ } else { @>
	  <small><spring:message code="pd.label.commontext.notAvailable" /></small>
    <@ } @>
 	 </div>
</script>
<script type="text/html" id="treamentFractureTpl">
 	 <div class="plan-details u-flex-25-gs">
	 	 <@ if(TOTAL_COST !== null && TOTAL_COST !== "") { @>
	<p><span>$<@=TOTAL_COST@></span></p>
<@ } else { @>
	  <small><spring:message code="pd.label.commontext.notAvailable" /></small>
    <@ } @>
 	 </div>
</script>
<script type="text/html" id="deductibleIndTpl">
   <@ if($(".smartScoreVal").val() >= 80) { @>
 	      <div class="plan-details best u-flex-25-gs">
 	 <@ } else if($(".smartScoreVal").val() >= 60) { @>
 	      <div class="plan-details ok u-flex-25-gs">
 	 <@ } else { @>
    	   <div class="plan-details poor u-flex-25-gs">
  <@ } @>

	<@ if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() == "1") {@>

		<@ if(DEDUCTIBLE != null && DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != ""){@>
      		<p>$<@=parseFloat(DEDUCTIBLE.inNetworkInd).toFixed()@></p>
    	<@ } else if(DEDUCTIBLE != null && DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != ""){ @>
        	<p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkInd).toFixed()@></p>
		<@ } else { @>
      		<p><spring:message code="pd.label.commontext.notApp" htmlEscape="true"/></p>
    	<@ } @>

	<@} if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1" ){ @>

		<@ if(DEDUCTIBLE != null && DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != ""){@>
      		<p>$<@=parseFloat(DEDUCTIBLE.inNetworkFlyPerPerson).toFixed()@></p>
    	<@ } else if(DEDUCTIBLE != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != ""){ @>
        	<p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFlyPerPerson).toFixed()@></p>
		<@ } else { @>
      		<p><spring:message code="pd.label.commontext.notApp" htmlEscape="true"/></p>
    	<@ } @>

	<@} @>
  </div>
</script>

<script type="text/html" id="deductibleFlyTpl">
   <@ if($(".smartScoreVal").val() >= 80) { @>
 	      <div class="plan-details best u-flex-25-gs">
 	 <@ } else if($(".smartScoreVal").val() >= 60) { @>
 	      <div class="plan-details ok u-flex-25-gs">
 	 <@ } else { @>
    	   <div class="plan-details poor u-flex-25-gs">
  <@ } @>
    <@ if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1" ){ @>
      <@ if(DEDUCTIBLE != null && DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != ""){ @>
        <p>$<@=parseFloat(DEDUCTIBLE.inNetworkFly).toFixed()@></p>
      <@ } else if(DEDUCTIBLE != null && DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != ""){ @>
        <p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFly).toFixed()@></p>
      <@ } else { @>
        <p><spring:message code="pd.label.commontext.notApp" htmlEscape="true"/></p>
      <@ } @>
	<@ } else { @>
 	  <p><spring:message code="pd.label.commontext.notApplicable" htmlEscape="true"/></p>
    <@ } @>
  </div>
</script>

<script type="text/html" id="benefitTpl">
  <@ if($(".smartScoreVal").val() >= 80) { @>
    <div class="plan-details u-flex-25-gs best">
  <@ } else if($(".smartScoreVal").val() >= 60) { @>
    <div class="plan-details u-flex-25-gs ok">
  <@ } else { @>
    <div class="plan-details u-flex-25-gs poor">
  <@ } @>
  <@ if(BENEFIT != null && BENEFIT.displayVal !=null && BENEFIT.displayVal != "") { @>
	<small><@=BENEFIT.displayVal@></small>
  <@ } else{@><small><spring:message code='pd.label.commontext.notAvailable' htmlEscape='true'/></small><@}@>
  <@ if(( EXPLANATION!=null && (EXPLANATION.displayVal != null && EXPLANATION.displayVal != "" )) && ( EXCLUSION!=null && (EXCLUSION.displayVal != null && EXCLUSION.displayVal != "" ))) {@>
	<p><small><a href="#" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=EXPLANATION.displayVal@>,<@=EXCLUSION.displayVal@>" code="pd.tooltip.title.explanationAndExclusions" htmlEscape="false"/>"><spring:message code="pd.label.title.additionalInformation"/></a></small></p>
  <@} else if(( EXPLANATION!=null && (EXPLANATION.displayVal == null || EXPLANATION.displayVal == "") ) && ( EXCLUSION!=null && (EXCLUSION.displayVal != null && EXCLUSION.displayVal != "" ))) {@>
	<p><small><a href="#" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=EXCLUSION.displayVal@>" code="pd.tooltip.title.limitsAndExclusions" htmlEscape="false"/>"><spring:message code="pd.label.title.additionalInformation"/></a></small></p>
  <@} else if(( EXPLANATION!=null && (EXPLANATION.displayVal != null && EXPLANATION.displayVal != "" )) && ( EXCLUSION!=null && (EXCLUSION.displayVal == null || EXCLUSION.displayVal == "" ))) {@>
	<p><small><a href="#" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=EXPLANATION.displayVal@>" code="pd.tooltip.title.benefitExplanation" htmlEscape="false"/>"><spring:message code="pd.label.title.additionalInformation"/></a></small></p>
  <@} else {@>
  <@} @>
</script>

<script type="text/html" id="deductibleTpl">
  <div class="plan-details">
  <p>
	<@ if(Object.keys(DEDUCTIBLES).length > 0) { @>
      <@ for(DEDUCTIBLE_NAME in DEDUCTIBLES) { @>
      <@ if(DEDUCTIBLES[DEDUCTIBLE_NAME] != null) { @>
      	<@ var DEDUCTIBLE = DEDUCTIBLES[DEDUCTIBLE_NAME]; @>
	  	<span><@=DEDUCTIBLE_NAME@> : </span>
		<@ if((DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != "") ||(DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != "") ||(DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != "") ||(DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != "") || (DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != "") || (DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != "")){ @>

			<@ if($('#PERSON_COUNT').val() == 1){ @>

				<@ if(DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != ""){ @>
					<span>$<@=parseFloat(DEDUCTIBLE.inNetworkInd).toFixed()@> <spring:message code='pd.label.commontext.individualSmall' htmlEscape='true'/></span>
				<@ } else if(DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != ""){ @>
            		<span>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkInd).toFixed()@> <spring:message code='pd.label.commontext.individualSmall' htmlEscape='true'/></span>
          		<@ } @>

			<@} else if($('#PERSON_COUNT').val() > 1){ @>

				<@ if(DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != ""){ @>
					<span>$<@=parseFloat(DEDUCTIBLE.inNetworkFlyPerPerson).toFixed()@> <spring:message code='pd.label.commontext.individualSmall' htmlEscape='true'/></span>
				<@ } else if(DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != ""){ @>
            		<span>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFlyPerPerson).toFixed()@> <spring:message code='pd.label.commontext.individualSmall' htmlEscape='true'/></span>
          		<@ } @>

				<@ if(DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != "" && DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != "" && DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != ""){@>
              		<span>,&nbsp;</span>
          		<@ } else if(DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != "" && DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != "" && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != ""){@>
          	  		<span>,&nbsp;</span>
          		<@ } @>

          		<@ if(DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != ""){@>
              		<span>$<@=parseFloat(DEDUCTIBLE.inNetworkFly).toFixed()@> <spring:message code='pd.label.commontext.familySmall' htmlEscape='true'/></span>
            	<@ } else if(DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != ""){ @>
              		<span>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFly).toFixed()@> <spring:message code='pd.label.commontext.familySmall' htmlEscape='true'/></span>
            	<@ } @>

			<@ } @>
		<@ } else { @>
    	  <spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
    	<@ } @>
      	<br>
      <@ } @>
    <@ } @>
    <@ } else { @>
      <spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
    <@ } @>
  </p>
  </div>
</script>

<script type="text/html" id="simplifiedDeductibleTpl">
<div class="plan-details u-flex-25-gs">
	<@ var inNetworkDeductible = false; @>

	<@ if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() == "1" && DEDUCTIBLE != null && DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != ""){@>
		<@ inNetworkDeductible = true; @>
		<p>$<@=parseFloat(DEDUCTIBLE.inNetworkInd).toFixed()@> (<spring:message code="pd.label.commontext.individual" />)</p>
	<@ } @>

	<@ if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1"){ @>

		<@ if(DEDUCTIBLE != null && DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != ""){@>
			<@ inNetworkDeductible = true; @>
			<p>$<@=parseFloat(DEDUCTIBLE.inNetworkFlyPerPerson).toFixed()@> (<spring:message code="pd.label.commontext.individual" />)</p>
		<@ } @>

		<@ if(DEDUCTIBLE != null && DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != ""){@>
			<@ inNetworkDeductible = true; @>
			<p>$<@=parseFloat(DEDUCTIBLE.inNetworkFly).toFixed()@> (<spring:message code="pd.label.commontext.family" />)</p>
        <@ } @>
	<@ } @>

	<@ if(inNetworkDeductible == false) { @>

		<@ if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() == "1" && DEDUCTIBLE != null && DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != ""){ @>
			<@ inNetworkDeductible = true; @>
			<p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkInd).toFixed()@> (<spring:message code="pd.label.commontext.individual" />)</p>
	  	<@ } @>

		<@ if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1"){ @>

			<@ if(DEDUCTIBLE != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != ""){ @>
				<@ inNetworkDeductible = true; @>
				<p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFlyPerPerson).toFixed()@> (<spring:message code="pd.label.commontext.individual" />)</p>
	  		<@ } @>

			<@ if(DEDUCTIBLE != null && DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != ""){ @>
				<@ inNetworkDeductible = true; @>
				<p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFly).toFixed()@> (<spring:message code="pd.label.commontext.family" />)</p>
			<@ } @>
	  	<@ } @>
	<@ } @>

	<@ if(inNetworkDeductible == false && $('#stateCode').val() != 'CT') { @>
		<p><spring:message code="pd.label.commontext.includedInDeductible" htmlEscape="true"/></p>
	<@ } @>

	<@ if(inNetworkDeductible == false && $('#stateCode').val() == "CT") { @>
		<p><spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/></p>
	<@ } @>
</div>

</script>


<script type="text/html" id="simplifiedOtherDeductibleTpl">
<div class="plan-details u-flex-25-gs">
	<p>
 	  <@ if(Object.keys(DEDUCTIBLES).length > 0) { @>
	    <@ for(DEDUCTIBLE_NAME in DEDUCTIBLES) { @>
		  <@ if(DEDUCTIBLES[DEDUCTIBLE_NAME] != null) { @>
			<@ var DEDUCTIBLE = DEDUCTIBLES[DEDUCTIBLE_NAME]; @>
			<@ if(DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != ""){@>
  		    <span><@=DEDUCTIBLE_NAME@> : </span>
        	  <span>$<@=parseFloat(DEDUCTIBLE.inNetworkInd).toFixed()@></span>
      		<@ } @>
      		<br>
		  <@ } @>
		<@ } @>
	  <@ } else { @>
		<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
	  <@ } @>
	</p>
  </div>

</script>

<script type="text/html" id="maxCoinseForSpecialtyDrugsTpl">
  <div class="plan-details u-flex-25-gs"><p>
    <@ if( MAX_COINSE_FOR_SPECIALTY_DRUGS!=null && MAX_COINSE_FOR_SPECIALTY_DRUGS != "") { @>
     $<@=MAX_COINSE_FOR_SPECIALTY_DRUGS@>
    <@ } else { @>
		<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
	  <@ } @>
  </p></div>

</script>

<script type="text/html" id="providerNamesTpl">
	<@if(doctors && doctors!=""){@>
		<@ _.each (doctors, function (doctor) { @>
			<div class="row-fluid">
			  <div class="u-flex-display" id="doc_<@=doctor.providerId@>">
	        	<div class="u-flex-25 planCompare__benefits__label col-xs-3 col-sm-3">
	              <@ if (doctor.providerType == "doctor") {@>
					<i class='user-md'></i>&nbsp;
					<@ if (doctor.providerName.indexOf('Dr.') != 0 && $('#stateCode').val() === 'CA') {@>
						<spring:message code="pd.label.doctor.title"/>&nbsp;
					<@ } @>
					<@=doctor.providerName@>
          		  <@ } else if (doctor.providerType == "dentist") {@>
					<i class='user-md'></i>&nbsp;<@=doctor.providerName@>
                      <@ } else { @>
					<i class='hospital'></i>&nbsp;<@=doctor.providerName@>
                <@ } @>
      				  &nbsp; <a href="preferences" class="margin10-l"><spring:message code="pd.label.title.edit" htmlEscape="true"/></a></br>
          		  <span><@=doctor.providerSpecialty@> </br>
		  		  <@if(doctor.providerPhone != ""){@>
                <@=doctor.providerPhone@><br>
		  		  <@}@>
                <@=doctor.providerAddress@><br>
					      <@=doctor.providerCity@>, <@=doctor.providerState@> <@=doctor.providerZip@>
	            </div>
			  </div>
			</div>
		<@ }); @>
	<@ } else { @>
		<div class="row-fluid" id="providers">
		  <div class="u-flex-display margin10-tb">
			<div class="u-flex-25 planCompare__benefits__label col-xs-3 col-sm-3">
	            <a href="preferences" class="margin10-l"><spring:message code="pd.label.title.checkDoctor"/></a>
	        </div>
		  </div>
		</div>
	<@ } @>
</script>

<script type="text/html" id="drugNamesTpl">
<c:if test="${prescriptionSearchConfig == 'ON'}">
    <@ if (prescriptionResponseList != null) { @>
      <@ _.each (prescriptionResponseList, function (prescription) { @>
		  <div class="u-flex-display drugName" id="drug_<@=prescription.drugID.replace(/[^A-Z0-9]+/ig, '')@>">
			  <div class="u-flex-25">
		  		<a href="#" data-placement="right" rel="tooltip" data-original-title="<@=prescription.drugDosage@>">
				  <@=prescription.drugName@> <@if(prescription.drugType!=null){@>(<@=prescription.drugType@>)<@}@>
				</a>
			  </div>
		  </div>

 		  <@if(prescription.genericID != null && prescription.genericID != "" && prescription.genericDosage != null && prescription.genericDosage != ""){@>
		  	<@ var genericDrugName = prescription.genericDosage.split(' ')[0]; @>
		  	<@ genericDrugName = genericDrugName.charAt(0).toUpperCase() + genericDrugName.slice(1); @>
			<div class="u-flex-display" id="generic_<@=prescription.genericID.replace(/[^A-Z0-9]+/ig, '')@>">
			  <div class="u-flex-25">
				<a href="#" data-placement="right" rel="tooltip" data-original-title="<@=prescription.genericDosage@>">
				  <@=prescription.drugName@> (Generic: <@=genericDrugName@>)
				</a>
			  </div>
		 	</div>
		  <@ } @>
	  <@});@>
	<@}@>
</c:if>
</script>

<script type="text/html" id="providerSearchLinkTpl">
  <div class="plan-details u-flex-25-gs">
       <@ if( PROVIDER_LINK && PROVIDER_LINK != "") { @>
         <a href="<@=PROVIDER_LINK@>" target="_blank"> <spring:message code="pd.label.info.yesDir"/></a>
       <@ } else { @>
		 <spring:message code="pd.label.info.noDir"/>
       <@ } @>
  </div>
</script>

<script type="text/html" id="providerHeatMapCompareTpl">
<div class="plan-details u-flex-25-gs">
       <div id="providerHeat_<@=PLAN_ID@>_<@=PROVIDER_NETWORK_KEY@>" class="heat-num"></div>
       <button id="viewMap_<@=PLAN_ID@>_<@=PROVIDER_NETWORK_KEY@>" data-toggle="modal" data-target="#map-modal" class="map__button btn btn-small"><spring:message code="pd.provider.buttons.viewMap" htmlEscape="true"/></button>
</div>
</script>

<script type="text/html" id="providerSearchInfoTplCompare">
	<@if(doctors && doctors!=""){@>
		<@ _.each (doctors, function (doctor) { @>
			<@var  detailElementId =planId +"_"+doctor.providerId; @>
			<@var networkStatusClassHTML="";@>
			<@var id="#" +"doc_"+ doctor.providerId;  @>

			<@if(doctor.networkStatus=="outNetWork"){@>
				<@ networkStatusClassHTML="<i class='NA'></i>&nbsp;<spring:message code='pd.label.outNetwork'/>"  ;@>
			<@}else if(doctor.networkStatus=="inNetWork"){@>
				<@ networkStatusClassHTML="<i class='GOOD'></i>&nbsp;<spring:message code='pd.label.inNetwork' htmlEscape='true'/>"  ;@>
			<@}else{@>
				<@ networkStatusClassHTML="<i class='UNKNOWN'></i>&nbsp;<spring:message code='pd.label.unknownAffiliation' htmlEscape='true'/>"   ;@>
			<@}@>

			<@if($('#'+detailElementId).length==0){@>
				<@$(id).append( '<div class="plan-details u-flex-25-gs removeProvidersInfo" id='+detailElementId+'>'+networkStatusClassHTML + '</div>')@>
			<@}@>
		<@ }); @>
	<@ } @>
</script>
<script type="text/html" id="drugSearchInfoTplCompare">
<c:if test="${prescriptionSearchConfig == 'ON'}">
	<@ if (prescriptionResponseList != null) { @>
      <@ _.each (prescriptionResponseList, function (prescription) { @>
		  <@var  detailElementId =planId +"_drug_"+prescription.drugID.replace(/[^A-Z0-9]+/ig, ''); @>
		  <@var networkStatusClassHTML="";@>
		  <@var id="#" +"drug_"+ prescription.drugID.replace(/[^A-Z0-9]+/ig, '');  @>
		  <@if(prescription.isDrugCovered=="Y"){@>
		    <@ networkStatusClassHTML="<a href='#' data-placement='bottom' rel='tooltip' data-original-title='<spring:message arguments='"+prescription.authRequired+","+prescription.stepTherapyRequired+"' code='pd.label.drugCovered'/>'><i class='GOOD'></i></a>&nbsp;"+prescription.drugCost;@>
		  <@}else if(prescription.isDrugCovered=="U"){@>
		   	<@ networkStatusClassHTML="<span><spring:message code='pd.label.commontext.covInfoNotAvailable'/></span>";@>
		  <@}else{@>
		    <@ networkStatusClassHTML="<a href='#' data-placement='bottom' rel='tooltip' data-original-title='<spring:message code='pd.label.notCovered' htmlEscape='true'/>'><span class='aria-hidden'><spring:message code='pd.label.notCovered'/></span><i class='icon-ban-circle'></i></a>";@>
		  <@}@>

		  <@if($('#'+detailElementId).length==0){@>
			<@$(id).append( '<div class="plan-details u-flex-25-gs removeDrugsInfo" id='+detailElementId+'>'+networkStatusClassHTML + '</div>')@>
		  <@}@>

		  <@if(prescription.genericID != null && prescription.genericID != ""){@>
		  	<@var  genericdetailElementId =planId +"_generic_"+prescription.genericID.replace(/[^A-Z0-9]+/ig, ''); @>
		  	<@var genericnetworkStatusClassHTML="";@>
		  	<@var genericid="#" +"generic_"+ prescription.genericID.replace(/[^A-Z0-9]+/ig, '');  @>
			<@if(prescription.isGenericCovered=="Y"){@>
				<@ genericnetworkStatusClassHTML="<a href='#' data-placement='bottom' rel='tooltip' data-original-title='<spring:message arguments='"+prescription.genericAuthRequired+","+prescription.genericStepTherapyRequired+"' code='pd.label.drugCovered'/>'><i class='GOOD'></i></a>&nbsp;"+prescription.genericCost;@>
			<@}else if(prescription.isGenericCovered=="U"){@>
		   		<@ genericnetworkStatusClassHTML="<span><spring:message code='pd.label.commontext.covInfoNotAvailable'/></span>";@>
			<@}else{@>
				<@ genericnetworkStatusClassHTML="<a href='#' data-placement='bottom' rel='tooltip' data-original-title='<spring:message code='pd.label.notCovered' htmlEscape='true'/>'><span class='aria-hidden'><spring:message code='pd.label.notCovered'/></span><i class='icon-ban-circle'></i></a>";@>
			<@}@>

			<@if($('#'+genericdetailElementId).length==0){@>
				<@$(genericid).append( '<div class="plan-details u-flex-25-gs removeDrugsInfo" id='+genericdetailElementId+'>'+genericnetworkStatusClassHTML + '</div>')@>
			<@}@>
		<@ } @>
	  <@ }); @>
	<@ } @>
</c:if>
</script>
