package com.getinsured.eligibility.at.ref.si;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.mes.service.QueueProcessingService;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.ref.dto.EnrollmentAttributesDTO;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerService;
import com.getinsured.eligibility.at.ref.service.ReferralDemographicService;
import com.getinsured.eligibility.at.ref.service.ReferralLCECompareService;
import com.getinsured.eligibility.at.ref.service.ReferralProcessingService;
import com.getinsured.eligibility.at.ref.service.ReferralSsapCmrLinkService;
import com.getinsured.eligibility.at.ref.service.ReferralTriggerAdminupdateService;
import com.getinsured.eligibility.at.ref.util.ExceptionUtil;
import com.getinsured.eligibility.at.resp.si.dto.ApplicationExtension;
import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.AssisterType;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.timeshift.TimeShifterUtil;

/**
 * @author chopra_s
 * 
 */
@Component("lceReferralProcessingService")
@Scope("singleton")
public class LceReferralProcessingService {
	private static final Logger LOGGER = LoggerFactory.getLogger(LceReferralProcessingService.class);

	@Autowired
	@Qualifier("referralProcessingService")
	private ReferralProcessingService referralProcessingService;

	@Autowired
	@Qualifier("referralDemographicService")
	private ReferralDemographicService referralDemographicService;

	@Autowired
	@Qualifier("referralTriggerAdminupdateService")
	private ReferralTriggerAdminupdateService referralTriggerAdminupdateService;

	@Autowired
	@Qualifier("referralSsapCmrLinkService")
	private ReferralSsapCmrLinkService referralSsapCmrLinkService;

	@Autowired
	@Qualifier("referralLCECompareService")
	private ReferralLCECompareService referralLCECompareService;

	@Autowired
	@Qualifier("lceDemoHandlerService")
	private LceProcessHandlerService lceDemoHandlerService;

	@Autowired
	@Qualifier("lceDemoDobHandlerService")
	private LceProcessHandlerService lceDemoDobHandlerService;

	@Autowired
	@Qualifier("lceElgLostHandlerService")
	private LceProcessHandlerService lceElgLostHandlerService;
	
	@Autowired
	@Qualifier("lceElgLostPostHandlerService")
	private LceProcessHandlerService lceElgLostPostHandlerService;

	@Autowired
	@Qualifier("lceAptcOnlyHandlerService")
	private LceProcessHandlerService lceAptcOnlyHandlerService;

	@Autowired
	@Qualifier("lceCitizenshipHandlerService")
	private LceProcessHandlerService lceCitizenshipHandlerService;

	@Autowired
	@Qualifier("lceRelationshipHandlerService")
	private LceProcessHandlerService lceRelationshipHandlerService;

	@Autowired
	@Qualifier("lceAllChangesHandlerService")
	private LceProcessHandlerService lceAllChangesHandlerService;

	@Autowired
	@Qualifier("lceNoChangesHandlerService")
	private LceProcessHandlerService lceNoChangesHandlerService;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private ExternalAssisterService externalAssisterService;
	
	@Autowired
	private QueueProcessingService queueProcessingService;

	@Autowired
	private ExceptionUtil exceptionUtil;
	
	public String processReferral(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		AccountTransferRequestDTO accountTransferRequest = null;
		long ssapApplicationId = 0;
		try {
			LOGGER.info("LceReferralProcessingService processReferral starts ");
			
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
			referralResponse.getData().putAll(input.getData());
			
			// Retrieve request object from message which was set in step routing step.
			accountTransferRequest = (AccountTransferRequestDTO)referralResponse.getData().get(ReferralProcessingConstants.ACCOUNT_TRANSFER_REQUEST_DTO);
			
			if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
				TimeShifterUtil.initializeTimeContext(accountTransferRequest);
			}
			
			referralResponse.getData().put(ReferralProcessingConstants.KEY_APPLICATIONS_WITH_SAME_ID, accountTransferRequest.getApplicationsWithSameId());
			
			// Get Organization Identification ID from Authorized Representative which is used as External Applicant ID in users
			referralResponse.getData().put(ReferralProcessingConstants.KEY_AUTH_REPS_ORGA_IDEN_ID, referralProcessingService.getOrganizationIdentification(accountTransferRequest.getAccountTransferRequestPayloadType()));

			ssapApplicationId = referralProcessingService.executeLceReferral(accountTransferRequest);
			referralResponse.getData().put(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID, ssapApplicationId);

			// for MN if Assister tag is passed then set it in the map
			final String hasLinkUserStr =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_LINK_USER);
			if (null != hasLinkUserStr && "TRUE".equalsIgnoreCase(hasLinkUserStr)) {
				if (null != accountTransferRequest.getAccountTransferRequestPayloadType().getAssister()) {
					referralResponse.getData().put(ReferralConstants.ASSISTER_INFO,
							accountTransferRequest.getAccountTransferRequestPayloadType().getAssister());
				}
			}

			// set the HouseholdCaseId in ReferralResponse 
			referralProcessingService.setExternalHouseholdCaseIdInReferralResponse(referralResponse,accountTransferRequest);
			
			// set mes procecssing required fields in referral response 
			referralResponse.getData().put(ReferralProcessingConstants.SPAN_INFO_DTO,queueProcessingService.populateATQueueInfoRequestDTO(accountTransferRequest));
						
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_CREATING_REFERRAL_APPLICATION + accountTransferRequest.getGiwsPayloadId());
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_CREATING_REFERRAL_APPLICATION).append(accountTransferRequest.getGiwsPayloadId()).append(ReferralProcessingConstants.REASON)
			        .append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		} finally {
			Map<String, Object> data = referralResponse.getData();
			data.put(ReferralProcessingConstants.KEY_GI_WS_PAYLOAD_ID, accountTransferRequest.getGiwsPayloadId());
			data.put(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID, accountTransferRequest.getEnrolledApplicationId());
			data.put(ReferralProcessingConstants.KEY_COMPARED_TO_APPLICATION_ID, accountTransferRequest.getCompareToApplicationId());
			data.put(ReferralProcessingConstants.KEY_ACCOUNT_TRANSFER_CATEGORY, accountTransferRequest.getAccountTransferCategory());
			// Remove request object from message as further steps won't need it. It will be recreated when required.
			data.remove(ReferralProcessingConstants.ACCOUNT_TRANSFER_REQUEST_DTO);
		}

		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LceReferralProcessingService processReferral ends - Response is - " + response);

		return response;
	}

	public String processDemoCompare(Message<String> message) {

		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("LceReferralProcessingService processDemoCompare starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());

			referralResponse.getData().putAll(input.getData());
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			
			final CompareMainDTO compareMainDTO = referralDemographicService.executeCompare((long) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID),
			        (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));

			referralResponse.getData().put(ReferralProcessingConstants.KEY_DEMO_COMPARE_ADMINUPDATE, compareMainDTO.getEnrolledApplication().isHasAdminUpdate());

			 
			
			List <String > memberGuidList = new ArrayList<String >();
			boolean invokeMemberLevelUpdate = true;
			for (CompareApplicantDTO applicantDTO :compareMainDTO.getEnrolledApplication().getApplicants()) {
				int groupChangeInfo = checkForGroupABChanges(applicantDTO,memberGuidList);
				
				if(groupChangeInfo == 2 || groupChangeInfo == 3) {
					invokeMemberLevelUpdate = false;
					break;//Break and call Demo for all
				}
			}
			
			if(invokeMemberLevelUpdate && !memberGuidList.isEmpty() ) {
				referralResponse.getData().put( ReferralProcessingConstants.MEMBER_GUID_LIST , memberGuidList);
			}
			
			
			/* Modification of IND57 */
			Address updatedMailingAddress = checkMailingAddressUpdate(compareMainDTO);
			if (updatedMailingAddress != null){
				referralResponse.getData().put(ReferralProcessingConstants.UPDATED_MAILING_ADDRESS, updatedMailingAddress);
			} else {
				referralResponse.getData().put(ReferralProcessingConstants.UPDATED_MAILING_ADDRESS, new Address());
			}

			/* Modification of IND57 */
			
			
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_DEMOGRAPHIC_COMPARE + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_DEMOGRAPHIC_COMPARE).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_DEMOGRAPHIC);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LceReferralProcessingService processDemoCompare ends - Response is - " + response);

		return response;
	}
	
	private Address checkMailingAddressUpdate(CompareMainDTO compareMainDTO) {

		for (CompareApplicantDTO applicantDTO : compareMainDTO.getEnrolledApplication().getApplicants()) {
			if (applicantDTO.isAdminUpdate()) {
				return checkMailingAddress(applicantDTO);
			}
		}
		return null;
		
	}
	
	

	private Address checkMailingAddress(CompareApplicantDTO applicantDTO) {
		if (applicantDTO.getCompareApplicationDTO().isHasAddressChanged() && applicantDTO.getPersonId() == 1L) {
			if (applicantDTO.isMailAddressChanged()) {
				return applicantDTO.getMailingAddress();
			}
		}
		return null;
		
	}

	public String executeAdminUpdate(Message<String> message) {

		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("LceReferralProcessingService executeAdminUpdate starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());

			referralResponse.getData().putAll(input.getData());
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			referralTriggerAdminupdateService.triggerAdminupdate((long) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID), (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (Boolean) input
			        .getData().get(ReferralProcessingConstants.KEY_DEMO_COMPARE_ADMINUPDATE), 
			        (Address) input
			        .getData().get(ReferralProcessingConstants.UPDATED_MAILING_ADDRESS),  (List<String>) input.getData().get(ReferralProcessingConstants.MEMBER_GUID_LIST));

			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_ADMIN_UPDATE + input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_ADMIN_UPDATE).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_ADMIN_UPDATE);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LceReferralProcessingService executeAdminUpdate ends - Response is - " + response);

		return response;
	}
	
/**
 * //IF Group A Changes- Return 01 =1
		//If Group B Changes-Return  10 =2
		//If Group AB Changes Return 11 = 3
		//For Group A - FN|LN|MN|Gender|SSN 
		//For Group B - Mailing Address|Email | Phone Number 
 * @param applicantDTO
 * @param changeInfoMap
 * @return
 */
	private int checkForGroupABChanges(CompareApplicantDTO applicantDTO,  List <String> guidList) {
		 
		int returnValue = 0;
		if( applicantDTO.isAdminUpdate()) {
			returnValue = returnValue | 1;
			guidList.add(applicantDTO.getApplicantGuid() );
		}
		
		// we check person type here as we need to consider PTF change as HH level change
		if(applicantDTO.getPersonType()== null)  {
			if(applicantDTO.getPersonId()==1 && applicantDTO.isPrimaryHHLvlChange()) {
				returnValue = returnValue | 2;
			}
		} else if((applicantDTO.getPersonType().getPersonType().contains(SsapApplicantPersonType.PC.toString()) 
			|| applicantDTO.getPersonType().getPersonType().contains(SsapApplicantPersonType.PTF.toString()))
			&&  applicantDTO.isPrimaryHHLvlChange() ) {
			returnValue = returnValue | 2;
		}
		return returnValue;
	}

	public String processLinkCmr(Message<String> message) {

		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("LceReferralProcessingService processLinkCmr starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());

			referralResponse.getData().putAll(input.getData());
			
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			String authRepId = null;
			
			if(input.getData().get(ReferralProcessingConstants.KEY_AUTH_REPS_ORGA_IDEN_ID)!=null) {
				authRepId = input.getData().get(ReferralProcessingConstants.KEY_AUTH_REPS_ORGA_IDEN_ID).toString();
			}
			String hhCaseId = (String) input.getData().get(ReferralConstants.HOUSE_HOLD_CASE_ID);
			
			final boolean blnFlag = referralSsapCmrLinkService.executeLinking((long) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID)
					, (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID),authRepId,hhCaseId);
			
			if (blnFlag) {
				
				// call External Assister Designate/De-designate API
				final String hasLinkUserStr =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_LINK_USER);
				if(null != hasLinkUserStr && "TRUE".equalsIgnoreCase(hasLinkUserStr)){
					AssisterType assisterInfo = (AssisterType) input.getData().get(ReferralConstants.ASSISTER_INFO);
					final SsapApplication enrolledApplication = ssapApplicationRepository.findOne((long) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID));
					if(null != assisterInfo) {
						externalAssisterService.callExternalAssisterDesignateAPI(assisterInfo,enrolledApplication.getCmrHouseoldId().intValue());	
					} else {
						externalAssisterService.callExternalAssisterDeDesignateAPI(enrolledApplication.getCmrHouseoldId().intValue());
					}
					
				}
				
				referralResponse.getData().put(ReferralProcessingConstants.REFERRAL_AUTOLINKING, true);
				referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
				referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_LINK_CMR + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
				referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);
			} else {
				referralResponse.setMessage(ReferralProcessingConstants.ERROR_EXECUTING_LINK_CMR);
				referralResponse.setErrorCode(ReferralResponse.ERROR_LINK_CMR);
			}

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LINK_CMR).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_LINK_CMR);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LceReferralProcessingService processLinkCmr ends - Response is - " + response);

		return response;
	}

	public String processLceCompare(Message<String> message) {
		ERPResponse erpResponse = (ERPResponse) message.getHeaders().get(EligibilityConstants.ERP_RESPONSE);

		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put(EligibilityConstants.SSAP_APPLICATION_ID, erpResponse.getApplicationID());

		try {
			LOGGER.info("LceReferralProcessingService processLceCompare starts between " + erpResponse.getSsapApplicationPrimaryKey() + " and " + erpResponse.getCompareEnrolledApplicationId());
			final CompareMainDTO compareMainDTO = referralLCECompareService.executeCompare(erpResponse.getSsapApplicationPrimaryKey(), erpResponse.getCompareEnrolledApplicationId());
			erpResponse.setEnrolledApplicationAttributes(compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes());
			resultMap.put(ReferralProcessingConstants.LCECOMPARE_PROCESSING_RESULT, EligibilityConstants.AUTO);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LCE_COMPARE).append(erpResponse.getApplicationID()).append(EligibilityConstants.REASON).append(e.getMessage());
			LOGGER.error(errorReason.toString());
			resultMap.put(ReferralProcessingConstants.LCECOMPARE_PROCESSING_RESULT, EligibilityConstants.ERROR);
			resultMap.put(EligibilityConstants.ERROR_REASON, errorReason.toString());
			exceptionUtil.persistGiMonitorId(erpResponse.getSsapApplicationPrimaryKey(), e);
		} finally {
			resultMap.put(EligibilityConstants.ERP_RESPONSE, erpResponse);
		}
		final String response = EligibilityUtils.marshal(resultMap);

		LOGGER.info("LceReferralProcessingService processLceCompare ends - Response is - " + response);

		return response;
	}

	public String processLceDemoHandler(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("LceReferralProcessingService processLceDemoHandler starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
			referralResponse.getData().putAll(input.getData());
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			lceDemoHandlerService.execute(createLCEProcessRequestDTO((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (long) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID),
			        (ApplicationExtension) input.getData().get(ReferralProcessingConstants.KEY_APPLICATION_EXTENSION), (EnrollmentAttributesDTO) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_ATTRIBUTES)));
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_LCE_DEMO_HANDLER + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LCE_DEMO_HANDLER).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_LCE_DEMO_HANDLER);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LceReferralProcessingService processLceDemoHandler ends - Response is - " + response);

		return response;
	}

	public String processLceDemoDobHandler(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("LceReferralProcessingService processLceDemoDobHandler starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
			referralResponse.getData().putAll(input.getData());
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			lceDemoDobHandlerService.execute(createLCEProcessRequestDTO((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (long) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID),
			        (ApplicationExtension) input.getData().get(ReferralProcessingConstants.KEY_APPLICATION_EXTENSION), (EnrollmentAttributesDTO) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_ATTRIBUTES)));
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_LCE_DEMO_DOB_HANDLER + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LCE_DEMO_DOB_HANDLER).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_LCE_DEMO_DOB_HANDLER);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LceReferralProcessingService processLceDemoDobHandler ends - Response is - " + response);

		return response;
	}

	public String processLceElgLostHandler(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("LceReferralProcessingService processLceElgLostHandler starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
			referralResponse.getData().putAll(input.getData());
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			LCEProcessRequestDTO lceProcessRequestDTO = createLCEProcessRequestDTO((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (long) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID),
			        (ApplicationExtension) input.getData().get(ReferralProcessingConstants.KEY_APPLICATION_EXTENSION), (EnrollmentAttributesDTO) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_ATTRIBUTES));
			lceElgLostHandlerService.execute(lceProcessRequestDTO);
			
			final boolean isExchangeEligRequired = "TRUE".equalsIgnoreCase( DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ELIGIBILITY_ENABLE)) ? true :false;
			if(!isExchangeEligRequired){ // FA to NFA conversion can happen at this place only when exchange eligibility config is false.
				lceElgLostPostHandlerService.execute(lceProcessRequestDTO);	
			}
			
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_LCE_ELG_LOST_HANDLER + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LCE_ELG_LOST_HANDLER).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_LCE_ELG_LOST_HANDLER);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LceReferralProcessingService processLceElgLostHandler ends - Response is - " + response);

		return response;
	}

	public String processLceAptcOnlyHandler(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("LceReferralProcessingService processLceAptcOnlyHandler starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
			referralResponse.getData().putAll(input.getData());
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			lceAptcOnlyHandlerService.execute(createLCEProcessRequestDTO((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (long) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID),
			        (ApplicationExtension) input.getData().get(ReferralProcessingConstants.KEY_APPLICATION_EXTENSION), (EnrollmentAttributesDTO) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_ATTRIBUTES)));
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_LCE_APTC_ONLY_HANDLER + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LCE_APTC_ONLY_HANDLER).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_LCE_APTC_ONLY_HANDLER);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LceReferralProcessingService processLceAptcOnlyHandler ends - Response is - " + response);

		return response;
	}

	public String processLceCitizenshipHandler(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("LceReferralProcessingService processLceCitizenshipHandler starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
			referralResponse.getData().putAll(input.getData());
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			lceCitizenshipHandlerService.execute(createLCEProcessRequestDTO((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (long) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID),
			        (ApplicationExtension) input.getData().get(ReferralProcessingConstants.KEY_APPLICATION_EXTENSION), (EnrollmentAttributesDTO) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_ATTRIBUTES)));
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_LCE_CITIZENSHIP_HANDLER + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LCE_CITIZENSHIP_HANDLER).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_LCE_CITIZENSHIP_HANDLER);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LceReferralProcessingService processLceCitizenshipHandler ends - Response is - " + response);

		return response;
	}

	public String processLceRelationshipHandler(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("LceReferralProcessingService processLceRelationshipHandler starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
			referralResponse.getData().putAll(input.getData());
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			lceRelationshipHandlerService.execute(createLCEProcessRequestDTO((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (long) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID),
			        (ApplicationExtension) input.getData().get(ReferralProcessingConstants.KEY_APPLICATION_EXTENSION), (EnrollmentAttributesDTO) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_ATTRIBUTES)));
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_LCE_RELATIONSHIP_HANDLER + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LCE_RELATIONSHIP_HANDLER).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_LCE_RELATIONSHIP_HANDLER);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LceReferralProcessingService processLceRelationshipHandler ends - Response is - " + response);

		return response;
	}

	public String processLceAllChangesHandler(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("LceReferralProcessingService processLceAllChangesHandler starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
			referralResponse.getData().putAll(input.getData());
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			lceAllChangesHandlerService.execute(createLCEProcessRequestDTO((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (long) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID),
			        (ApplicationExtension) input.getData().get(ReferralProcessingConstants.KEY_APPLICATION_EXTENSION), (EnrollmentAttributesDTO) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_ATTRIBUTES)));
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_LCE_ALL_CHANGES_HANDLER + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LCE_ALL_CHANGES_HANDLER).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_LCE_ALL_CHANGES_HANDLER);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LceReferralProcessingService processLceAllChangesHandler ends - Response is - " + response);

		return response;
	}

	public String processLceNoChangesHandler(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("LceReferralProcessingService processLceNoChangesHandler starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
			referralResponse.getData().putAll(input.getData());
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			lceNoChangesHandlerService.execute(createLCEProcessRequestDTO((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (long) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID),
			        (ApplicationExtension) input.getData().get(ReferralProcessingConstants.KEY_APPLICATION_EXTENSION), (EnrollmentAttributesDTO) input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_ATTRIBUTES)));
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_LCE_NO_CHANGES_HANDLER + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LCE_NO_CHANGES_HANDLER).append(ReferralProcessingConstants.REASON).append(e.getMessage());
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_LCE_NO_CHANGES_HANDLER);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LceReferralProcessingService processLceNoChangesHandler ends - Response is - " + response);

		return response;
	}

	private LCEProcessRequestDTO createLCEProcessRequestDTO(long currentApplicationId, long enrolledApplicationId, ApplicationExtension applicationExtension, EnrollmentAttributesDTO enrolledApplicationAttributes) {
		return new LCEProcessRequestDTO(currentApplicationId, enrolledApplicationId, applicationExtension, enrolledApplicationAttributes);
	}
	
	public String processApplication(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		AccountTransferRequestDTO accountTransferRequest = null;
		long ssapApplicationId = 0;
		try {
			LOGGER.info("LceReferralProcessingService processReferral starts ");
			
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
			referralResponse.getData().putAll(input.getData());
			
			// Retrieve request object from message which was set in step routing step.
			accountTransferRequest = (AccountTransferRequestDTO)referralResponse.getData().get(ReferralProcessingConstants.ACCOUNT_TRANSFER_REQUEST_DTO);
			
			if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
				TimeShifterUtil.initializeTimeContext(accountTransferRequest);
}
			
			referralResponse.getData().put(ReferralProcessingConstants.KEY_APPLICATIONS_WITH_SAME_ID, accountTransferRequest.getApplicationsWithSameId());
			
			// Get Organization Identification ID from Authorized Representative which is used as External Applicant ID in users
			//referralResponse.getData().put(ReferralProcessingConstants.KEY_AUTH_REPS_ORGA_IDEN_ID, referralProcessingService.getOrganizationIdentification(accountTransferRequest.getAccountTransferRequestPayloadType()));

			ssapApplicationId = referralProcessingService.executeLceATDto(accountTransferRequest);
			referralResponse.getData().put(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID, accountTransferRequest.getCompareToApplicationId());

			// set the HouseholdCaseId in ReferralResponse 
			referralProcessingService.setExternalHouseholdCaseIdInReferralResponse(referralResponse,accountTransferRequest);
			
			// set mes procecssing required fields in referral response 
			referralResponse.getData().put(ReferralProcessingConstants.SPAN_INFO_DTO,queueProcessingService.populateATQueueInfoRequestDTO(accountTransferRequest));
						
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_CREATING_REFERRAL_APPLICATION + accountTransferRequest.getGiwsPayloadId());
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_CREATING_REFERRAL_APPLICATION).append(accountTransferRequest.getGiwsPayloadId()).append(ReferralProcessingConstants.REASON)
			        .append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		} finally {
			Map<String, Object> data = referralResponse.getData();
			data.put(ReferralProcessingConstants.KEY_GI_WS_PAYLOAD_ID, accountTransferRequest.getGiwsPayloadId());
			data.put(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID, accountTransferRequest.getEnrolledApplicationId());
			data.put(ReferralProcessingConstants.KEY_COMPARED_TO_APPLICATION_ID, accountTransferRequest.getCompareToApplicationId());
			data.put(ReferralProcessingConstants.KEY_ACCOUNT_TRANSFER_CATEGORY, accountTransferRequest.getAccountTransferCategory());
			// Remove request object from message as further steps won't need it. It will be recreated when required.
			data.remove(ReferralProcessingConstants.ACCOUNT_TRANSFER_REQUEST_DTO);
		}

		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("LceReferralProcessingService processReferral ends - Response is - " + response);

		return response;
	}
}
