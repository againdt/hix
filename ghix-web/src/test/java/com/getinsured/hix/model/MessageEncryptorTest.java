package com.getinsured.hix.model;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageEncryptorTest {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageEncryptorTest.class);
	
	@Test
	public void encryptTest() {
		LOGGER.info("============ Encrypt Test Begin =============");

		//VimoEncryptor ve = (VimoEncryptor) GHIXApplicationContext.getBean("vimoencryptor");
		try{
			
			String [] testStrings = {"small string", 
					"RSA is an algorithm for public-key cryptography that is based on the presumed" +
					" difficulty of factoring large integers, the factoring problem. RSA stands for Ron Rivest, " +
					"Adi Shamir and Leonard Adleman, who first publicly described it in 1977. Clifford Cocks, " +
					"an English mathematician, had developed an equivalent system in 1973, but it was classified until 1997"};
			
			for(int i=0; i<testStrings.length; i++){
				String encryptString = MessageEncryptor.encrypt(testStrings[i]);
				
				LOGGER.info("Original String : " + testStrings[i]);
				LOGGER.info("Original String length : " + testStrings[i].length());
				LOGGER.info("Encrypted String : " + encryptString);
				LOGGER.info("Encrypted String length : " + encryptString.length());
				LOGGER.info("Encryption Ratio : " + encryptString.length()/testStrings[i].length());
				
				String decryptString = MessageEncryptor.decrypt(encryptString);
				LOGGER.info("Decrypted String : " + decryptString+"\n");
			}
		}
		catch(Exception e){
			LOGGER.error("Exception has occured",e);
		}
		
		LOGGER.info("============ Encrypt Test End =============");
	}

}
