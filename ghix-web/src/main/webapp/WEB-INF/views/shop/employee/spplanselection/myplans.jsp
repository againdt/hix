<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/employee-dashboard-util" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/shop-enrollment-data" prefix="enrollData"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />


<div class="gutter10">
  <h1>Welcome Back,&nbsp;<span id="user-name">${employeeApplication.name}</span></h1>
  <div class="row-fluid">
    
    <div class="span3" id="sidebar">
      <util:myStuff employeeApplication="${employeeApplication}"></util:myStuff>
      <util:quickLinks employeeApplication="${employeeApplication}"></util:quickLinks>
    </div><!--/span3-->
    
    <div class="span9" id="rightpanel">
    
      <div class="header">  
        <h4>My Plans</h4>        
      </div>

      <div class="gutter10">
      <form class="form-horizontal" id="formmyplans" name="formmyplans" action="<c:url value='/shop/employee/spplanselection/submitmyplans'/>" method="post" >
	      <df:csrfToken/>
      	<input type="hidden" name="applicationId" id="applicationId" value="${employeeApplication.id}"/>
        <div class="row-fluid">
        	<c:if test="${employeeApplication.status=='ENROLLED' }">
            <p>Following is the information about plans selected by you during open enrollment.</p>
            </c:if> 
            <c:if test="${employeeApplication.status!='ENROLLED' }">
            <p>Your household is currently not enrolled in any health or dental plans.</p>
            </c:if>
            <input type="hidden" name="selectedPlan" id="selectedPlan" value=""/>
           <img class="para hide" src="/hix/resources/img/onepixel.png">              
        </div>

        <enrollData:view employeeApplication="${employeeApplication}"></enrollData:view>  
	  </form>
    </div>
  </div>
  </div><!--/row-fluid-->    
</div><!--/main .gutter10-->
<c:if test="${!isEnrollmentEnded }">
<jsp:include page="../waive_employer_coverage.jsp" >
	<jsp:param name="employeeApplicationId" value="${employeeApplication.id}" />
</jsp:include>
</c:if>
<script type="text/javascript">
$(".inactive").click(function(e) {
	e.preventDefault();
});


  $("#viewBenifitDetails ,.viewBenifitDetails").live('click',function(e) {
		$('#plandetails').remove();
		e.preventDefault();
		var href = $(this).attr('href') + "?iframe=no";
		if (href.indexOf('#') != 0) {
		  $(
			  '<div id="plandetails" class="modal bigModal"><div class="searchModal-header gutter10-lr"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class=""><iframe id="search" src="' + href + '" class="searchModal-body"></iframe></div></div>').modal();
		}
	});


$(document).keyup(function(e) {
	if (e.keyCode == 27) {
		$('#plandetails').modal('hide');
	}  
});


function imgSize(){
    $(".carrierlogo").each(function(){
        var width = $(this).width();
        var height = $(this).height();
        var maxWidth = 160;
		var maxHeight = 60;
		
		// Check if the current width is larger than the max
	    if(width > maxWidth){
	        ratio = maxWidth / width; // get ratio for scaling image
	        $(this).width(maxWidth); // Set new width
	        $(this).height((height * ratio)); // Scale height based on ratio
	        height = height * ratio; // Reset height to match scaled image
	    	width = width * ratio; // Reset width to match scaled image
	    }
		
		// Check if current height is larger than max
		if(height > maxHeight){                                     
		  ratio = maxHeight / height; // get ratio for scaling image
		  $(this).height(maxHeight); // Set new height
		  $(this).width((width * ratio)); // Scale width based on ratio    
		}
	    
		$(this).css('display','block');
    });
}


function selectPlan(insuranceType){
	
	if(insuranceType=='HEALTH'){
		if($('#terminateHealth').is(':checked')){
			$('#terminateHealthLink').removeAttr('disabled');
			if($('#terminateDental').length > 0){
				$('#terminateDental').prop('checked', true);	
			}
				
		}
		else{
			if($('#terminateDental').length > 0){
				$('#terminateDental').prop('checked', false);
			}
		}
		
	}
	else if(insuranceType=='DENTAL'){
		if($('#terminateHealth').is(':checked')){
			$('#terminateDental').prop('checked', true);	
		}else{
			if($('#terminateDental').is(':checked')){ 
				$('#terminateHealthLink').attr('disabled','disabled');
			}
			else{
				$('#terminateHealthLink').removeAttr('disabled');
			}
			
		}
	}
}


function terminateCoverage(insuranceType){
	if($('#terminateDental').length > 0){
		if(!$('#terminateHealth').is(':checked') && $('#terminateDental').is(':checked') && insuranceType=='HEALTH'){
			return false;
		}
		if(!$('#terminateHealth').is(':checked') && !$('#terminateDental').is(':checked')){
			alert('Please Select atleast one plan to terminate');
			return false;
		}	
	}
	else{
		if(!$('#terminateHealth').is(':checked')){
			alert('Please Select atleast one plan to terminate');
			return false;
		}
	}
	
	
	$('#selectedPlan').val(insuranceType);
	$('#formmyplans').submit();
}


</script>

<script>
	$(document).ready(function(){
		$('span.terminationPageTooltip').hide();
		var the_checkbox = $('#terminateHealth');
		the_checkbox.on('change',function () {
		    if ($(this).is(':checked')) {
		    	$('span.terminationPageTooltip').show();
// 		        $('input#terminateHealth').addClass('terminationPageTooltip');
		    } else {
		    	$('span.terminationPageTooltip').hide();
// 		    	$('input#terminateHealth').removeClass('terminationPageTooltip');
		    }
		});
	});
</script>
