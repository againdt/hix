package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;


import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr3r.RetrieveStep3CaseResolutionResponseType;

public class RetrieveStep3CaseResolutionResponseTypeWrapper implements JSONAware{

	private RetrieveStep3CaseResolutionResponseType retrieveStep3CaseResolutionResponseType;
	
	private RetrieveStep3ResponseMetadataWrapper responseMetadata;
	private String lawfulPresenceVerifiedCode;
	private RetrieveStep3CaseResolutionResponseSetTypeWrapper retrieveStep3ResponseTypeResponseSet;
	
	public RetrieveStep3CaseResolutionResponseTypeWrapper(RetrieveStep3CaseResolutionResponseType retrieveStep3CaseResolutionResponseType){
		
		if(retrieveStep3CaseResolutionResponseType == null){
			return;
		}
		
		RetrieveStep3ResponseMetadataWrapper responseMetadataWrapper = new RetrieveStep3ResponseMetadataWrapper(retrieveStep3CaseResolutionResponseType.getResponseMetadata());
		this.responseMetadata = responseMetadataWrapper;
		
//		this.lawfulPresenceVerifiedCode = intialVerificationResponseType.getLawfulPresenceVerifiedCode();
		
		RetrieveStep3CaseResolutionResponseSetTypeWrapper retrieveStep3CaseResolutionResponseSetTypeWrapper = 
				new RetrieveStep3CaseResolutionResponseSetTypeWrapper(retrieveStep3CaseResolutionResponseType.getRetrieveStep3CaseResolutionResponseSet());
		
		this.retrieveStep3ResponseTypeResponseSet = retrieveStep3CaseResolutionResponseSetTypeWrapper;
	}

	public RetrieveStep3CaseResolutionResponseType getRetrieveStep3CaseResolutionResponseType() {
		return retrieveStep3CaseResolutionResponseType;
	}

	public void setRetrieveStep3CaseResolutionResponseType(
			RetrieveStep3CaseResolutionResponseType retrieveStep3CaseResolutionResponseType) {
		this.retrieveStep3CaseResolutionResponseType = retrieveStep3CaseResolutionResponseType;
	}

	public RetrieveStep3ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(RetrieveStep3ResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}

	public String getLawfulPresenceVerifiedCode() {
		return lawfulPresenceVerifiedCode;
	}

	public void setLawfulPresenceVerifiedCode(String lawfulPresenceVerifiedCode) {
		this.lawfulPresenceVerifiedCode = lawfulPresenceVerifiedCode;
	}

	public RetrieveStep3CaseResolutionResponseSetTypeWrapper getRetrieveStep3CaseResolutionResponseSet() {
		return retrieveStep3ResponseTypeResponseSet;
	}

	public void setRetrieveStep3CaseResolutionResponseSet(
			RetrieveStep3CaseResolutionResponseSetTypeWrapper retrieveStep3CaseResolutionResponseSetTypeWrapper) {
		this.retrieveStep3ResponseTypeResponseSet = retrieveStep3CaseResolutionResponseSetTypeWrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.responseMetadata != null){
			obj.put("ResponseMetadata",this.responseMetadata);
		}
		
		if(this.lawfulPresenceVerifiedCode != null){
			obj.put("LawfulPresenceVerifiedCode",this.lawfulPresenceVerifiedCode);
		}
		
		if(this.retrieveStep3ResponseTypeResponseSet != null){
			obj.put("RetriveStep3ResponseTypeResponseSet",this.retrieveStep3ResponseTypeResponseSet);
		}

		return obj.toJSONString();
	}

}
