<script type="text/ng-template" id="appeals">

<div ng-controller="contactUsCtrl">  
 <div class="header">
    <h4><spring:message code="indportal.eligibilitysummary.submitanappeal" javaScriptEscape="true"/></h4>
  </div>


  <div id="submitAppeal" >
  <div ng-hide="appealPostResult">
    
      <div class="gutter20">
        <p><spring:message code="indportal.appeals.ifyoudontagree" javaScriptEscape="true"/></p>
        <p><spring:message code="indportal.appeals.youmayreceive" javaScriptEscape="true"/></p>
        <p class="margin20-b"><spring:message code="indportal.appeals.ingeneralwenotify" javaScriptEscape="true"/></p>
        <p><spring:message code="indportal.appeals.orifyouwereincorrectly" javaScriptEscape="true"/></p>
         <div class="margin10-b">
          <div class="header"><h4><spring:message code="indportal.appeals.fileyourappeal" javaScriptEscape="true"/></h4></div>
        </div>
        <div class="gutter20">
		<p class="margin20-b"><spring:message code="indportal.appeals.request" javaScriptEscape="true"/></p>
          <p class="alert alert-info"><strong class="margin10-r"><spring:message code="indportal.appeals.step1" javaScriptEscape="true"/>:</strong> <a onclick="openInNewTab('https://www.yourhealthidaho.org/wp-content/uploads/2014/10/Your-Health-Idaho-Appeal-Request-Form_rev.10092014.pdf')" href="javscript:void(0)" target="_blank"> <spring:message code="indportal.eligibilitysummary.downloadyourappealform" javaScriptEscape="true"/></a></p>
		  <input type="hidden" name="appealReason" ng-model="appealForm.appealReason" id="" value="Qhp">
          <p class="alert alert-info">
            <strong class="margin10-r"><spring:message code="indportal.appeals.step2" javaScriptEscape="true"/>:</strong> <spring:message code="indportal.eligibilitysummary.uploadyourappealform" javaScriptEscape="true"/>
            <div class="control-group">
							 <spring:message code="indportal.contactus.eachfileyouupload" javaScriptEscape="true"/>
              <label class="control-label" for="inputEmail"><spring:message code="indportal.eligibilitysummary.uploadform" javaScriptEscape="true"/>:</label>
              <div class="controls">
                <input type="hidden" name="appealFileUploaded" ng-model="appealForm.appealFileUploaded" value="asd">
                <button class="btn btn-small" trigger-add-file> <spring:message code="indportal.contactus.choosefile" javaScriptEscape="true"/> </button>
                <input type="file" class="custom-file-input" ng-file-select="selectAppealFile($files, $event)" ng-hide="true" ng-model="file">
                <button class="btn btn-small" ng-disabled="!appealfileSelected" ng-click="cancelAppealfile()"><i class="icon-remove"></i> <spring:message code="indportal.contactus.cancel" javaScriptEscape="true"/> </button>
                <span>{{appealfileSelected.name}}<span>

              </div>
            </div>
          </p>
        </div>
      </div>
    
    <div class="form-actions form-horisontal">
      <a href="#/" class="btn btn-secondary"><spring:message code="indportal.eligibilitysummary.gobacktobashboard" javaScriptEscape="true"/></a>
      <a href="" ng-click="formAppeal.$invalid || !appealfileSelected || submitAppeal()" class="btn btn-primary pull-right" ng-disabled="formAppeal.$invalid || !appealfileSelected"><spring:message code="indportal.appeals.submitappeal" javaScriptEscape="true"/></a>
    </div>

<div spinning-loader="loader"></div>

		<!-- Oversized File Modal-->
		<div ng-show= "oversizedFileD">
		      <div modal-show="oversizedFileD" class="modal fade">
		      <div class="modal-dialog">
		        <div class="modal-content">
		          <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		            <h3 id="myModalLabel"><spring:message code="indportal.contactus.oversized" javaScriptEscape="true"/></h3>
		          </div>
		          <div class="modal-body">
		          <p><spring:message code="indportal.contactus.oversizedcontent" javaScriptEscape="true"/></p>
		          </div>
		          <div class="modal-footer">
		            <button class="btn pull-left" data-dismiss="modal""><spring:message code="indportal.eligibilitysummary.close" javaScriptEscape="true"/></button>
		          </div>
		        </div><!-- /.modal-content-->
		      </div> <!-- /.modal-dialog-->
		     </div>
		</div>
		<!-- Oversized File Modal-->

			<!-- Wrong File Modal-->
		<div ng-show= "wrongFileTypeDialog">
		      <div modal-show="wrongFileTypeDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="wrongFileTypeModal" aria-hidden="true">
		      <div class="modal-dialog">
		        <div class="modal-content">
		          <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		            <h3 id="wrongFileTypeModal"><spring:message code="indportal.contactus.wrongfiletype" javaScriptEscape="true"/></h3>
		          </div>
		          <div class="modal-body">
		          <p><spring:message code="indportal.contactus.wrongfiletypecontent" javaScriptEscape="true"/></p>
		          </div>
		          <div class="modal-footer">
		            <button class="btn pull-left" data-dismiss="modal""><spring:message code="indportal.eligibilitysummary.close" javaScriptEscape="true"/></button>
		          </div>
		        </div><!-- /.modal-content-->
		      </div> <!-- /.modal-dialog-->
		     </div>
		</div>
		<!-- Wrong File Modal-->

</div>
<!-- GOOD -->
<div id="appealAckSuccess" aria-labelledby="appealAckSuccessLabel" ng-if="appealPostResult=='postsuccess'">
    <h3 id="contactRequestAckSuccessLabel"><spring:message code="indportal.contactus.submittedsuccessfully" javaScriptEscape="true"/></h3>
    <p><spring:message code="indportal.contactus.yourtickethasbeengenerated" javaScriptEscape="true"/></p>
    <a href="/hix/indportal" class="btn btn-primary"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></a>
</div>

<!-- BAD -->
<div id="appealAckFailure" aria-labelledby="appealAckFailureLabel" ng-if="appealPostResult=='postfailure'">
    <h3 id="contactRequestAckFailureLabel"><spring:message code="indportal.contactus.submissionfailed" javaScriptEscape="true"/></h3>
    <p><spring:message code="indportal.contactus.pleasetryagain" javaScriptEscape="true"/></p>
    <a href="/hix/indportal" class="btn btn-primary"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></a>
</div>



  </div>
</div>

</script>