import React from 'react';

// Components

import AddEmployerCoverageModal from '../components/modals/AddEmployerCoverageDetailsModal';
import AddIncomeOrDeductionSourceModal from '../components/modals/AddIncomeOrDeductionSourceModal';
import BloodRelationshipModal from '../components/modals/BloodRelationshipModal';
import FallbackUIModal from '../components/modals/FallbackUIModal';
import InformLawfullyPresentRequiredForCoverageModal from '../components/modals/InformLawfullyPresentRequiredForCoverageModal.js';
import InformMedicareEligibleModal from '../components/modals/InformMedicareEligibleModal.js';
import InformToAddIncomeSourceModal from '../components/modals/InformToAddIncomeSourceModal.js';
import LackOfAddressesModal from '../components/modals/LackOfAddressesModal';
import LocateAssistanceModal from '../components/modals/LocateAssistanceModal';
import MandatorySeekingForCoverageModal from '../components/modals/MandatorySeekingForCoverageModal';
import MultiTaxHouseholdModal from '../components/modals/MultiTaxHouseholdModal';
import OtherAddressModal from '../components/modals/OtherAddressModal';
import PreEligibilityModal from '../components/modals/PreElegibilityModal';
import QualifyingEventDetailsModal from '../components/modals/QualifyingEventDetailsModal';
import SessionRestorationModal from '../components/modals/SessionRestorationModal';
import ValidateAddressModal from '../components/modals/ValidateAddressModal';

// Constants

import MODAL_TYPES from '../constants/modalTypes';


/**
*
* @param {Object} props - contains "modal" props from Redux Store
* @returns {React.Component} - modal layout and needed content for Modal
*
*/
const ModalContainer = props => {

    const {
        modal,
        modal: {
            content,
            zIndex
        },
        ...restProps
    } = props;

    const getModal = () => {
        switch (content) {

        case MODAL_TYPES.ADD_EMPLOYER_COVERAGE_DETAIL_MODAL:
            return <AddEmployerCoverageModal {...{ modal, ...restProps }}/>;

        case MODAL_TYPES.ADD_INCOME_OR_DEDUCTION_SOURCE_MODAL:
            return <AddIncomeOrDeductionSourceModal {...{ modal, ...restProps }}/>;

        case MODAL_TYPES.BLOOD_RELATIONSHIP_MODAL:
            return <BloodRelationshipModal {...{ modal, ...restProps }}/>;

        case MODAL_TYPES.INFORM_MEDICARE_ELIGIBLE_MODAL:
            return <InformMedicareEligibleModal {...{ modal, ...restProps }}/>;

        case MODAL_TYPES.INFORM_LAWFULLY_PRESENT_REQUIRED_FOR_COVERAGE_MODAL:
            return <InformLawfullyPresentRequiredForCoverageModal {...{ modal, ...restProps }}/>;

        case MODAL_TYPES.INFORM_TO_ADD_INCOME_SOURCE_MODAL:
            return <InformToAddIncomeSourceModal {...{ modal, ...restProps }}/>;

        case MODAL_TYPES.LACK_OF_ADDRESS_MODAL:
            return <LackOfAddressesModal {...{ modal, ...restProps }}/>;

        case MODAL_TYPES.LOCATE_ASSISTANCE_MODAL:
            return <LocateAssistanceModal {...{ modal, ...restProps }}/>;

        case MODAL_TYPES.MANDATORY_SEEKING_FOR_COVERAGE_MODAL:
            return <MandatorySeekingForCoverageModal {...{ modal, ...restProps }}/>;

        case MODAL_TYPES.MULTI_TAX_HOUSEHOLD_MODAL:
            return <MultiTaxHouseholdModal {...{ modal, ...restProps }}/>;

        case MODAL_TYPES.OTHER_ADDRESS_MODAL:
            return <OtherAddressModal {...{  modal, ...restProps }}/>;

        case MODAL_TYPES.PRE_ELIGIBILITY_MODAL:
            return <PreEligibilityModal {...{  modal, ...restProps }}/>;

        case MODAL_TYPES.QUALIFYING_EVENT_DETAILS_MODAL:
            return <QualifyingEventDetailsModal {...{ modal, ...restProps }}/>;

        case MODAL_TYPES.SESSION_EXPIRING_MODAL:
            return <SessionRestorationModal {...{ modal, ...restProps }}/>;

        case MODAL_TYPES.VALIDATE_ADDRESS_MODAL:
            return <ValidateAddressModal {...{ modal, ...restProps }}/>;

        default:
            return <FallbackUIModal {...{ modal, ...restProps }}/>;
        }
    };

    return (
        <div
            className="modal__layout"
            style={{
                zIndex
            }}
        >
            <div className="modal__content">
                {
                    getModal()
                }
            </div>
        </div>
    );
};

export default ModalContainer;
