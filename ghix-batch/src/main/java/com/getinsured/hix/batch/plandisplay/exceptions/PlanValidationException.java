package com.getinsured.hix.batch.plandisplay.exceptions;

public class PlanValidationException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public PlanValidationException() {
		// TODO Auto-generated constructor stub
	}

	public PlanValidationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PlanValidationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public PlanValidationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/***
	 * Commenting out as this constructor is available on JDK 1.7 onwards only
	public ProviderValidationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}
	*/

}
