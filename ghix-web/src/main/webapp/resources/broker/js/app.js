
var agentEntityApp = angular.module('agentEntityApp',['agentEntityApp.services']);
var sortChanged = 'false';
var defaultDescription = 'Enter the following primary consumer information:\n'+
		'Application ID:\n'+
		"Applicant's Last Name:\n"+
		"Applicant's Date of Birth:\n"+
		"Last 4 digits of the Applicant's SSN:\n\n"+
		'Enter the following issuer and plan information:\n'+
		'Issuer Name:\n'+
		'Consumer Plan:\n\n'+
		'Describe the situation with as much detail as possible:\n\n'+
		'Please enter the steps that you have taken so far to assist the consumer with the above issue (if any);\n\n';

angular.module('agentEntityApp.services', [])
  .factory('agentEntityService', function($http) {
    var aeAPI = {};
    aeAPI.getAgentEntityData = function(params) {
    	params.totalIndividualCount = totalCountAtClient;

     var localCSRF = {csrftoken:$('#tokid').val()};
     return $http({
        method: 'POST',
        url: '/hix/broker/bookofbusinessforpagination?desigStatus=Active',
        params:params,
        headers: localCSRF
      });
    };
    return aeAPI;
 });

agentEntityApp.controller('searchFilterCtrl', function($scope) {
	$scope.filters = {
			  firstName:"",
		      lastName:"",
		      applicationType:"",
		      issuerval:"",
		      currentStatus:"",
		      nextStep:"",
		      coverageYear:"",
		      dueDate:""
	};
	$scope.isSliderOpen = false;
	$scope.isAnyFilterApplied = false;
	$scope.searchFilterSlider = function(){
		$scope.isSliderOpen = !$scope.isSliderOpen;
		$( ".searchPanel" ).slideToggle( "slow" );
		if ($scope.isSliderOpen) {
			$( ".searchPanel" ).attr("aria-label", "searchbar expaned");
		} else {
			$( ".searchPanel" ).attr("aria-label", "searchbar collapsed")
		}
	}

	$scope.removeFilter= function (clickEvent){
		 var el = $(clickEvent.target);
		 clickEvent.stopPropagation();
		 $("#"+ el.attr("filterName")).val("");
		 $scope.$parent.loadData('simpleLoading');
		 el.closest(".filterContainer").remove();
	};
	$scope.applyFilter= function (clickEvent){
		$scope.$parent.successCallback = function(){
			$scope.searchFilterSlider();
			$scope.isAnyFilterApplied = true;
		};
		$scope.$parent.pageNo = 1 ;
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$scope.$parent.loadData('simpleLoading',true);
	}
});
agentEntityApp.controller('searchSortingCtrl', function($scope) {
	$scope.exportExcelConfirmationContainer = 'exportExcelConfirmationForm';

	$scope.exportExcelConfirmation = function(eventFromUI){
		var modal = angular.element('#exportExcelPopupModal');
		setTimeout(function() {
			var hrefStr = $(eventFromUI.currentTarget).attr('href');
			if(hrefStr != '#'){
				$("#exportExcel").attr("action",hrefStr);
			}

			 modal.modal('show');
			 $(document).on('submit','#exportExcel',function(){
				 	modal.modal('hide');
			});
        });
	};

	 $("#sortDataDD").on("change",function(){
		 sortChanged = 'true';
		 $scope.$parent.pageNo = 1;
		 $scope.$parent.loadData('simpleLoading');
	 });
});
agentEntityApp.controller('angentEntityCtrl', function($scope,$http,agentEntityService) {
	var flag=true;
	$scope.contacts=[];

	$scope.isDataLoaded=false;
	$scope.dialogTitle = "";
	$scope.totalRecord = 0;
	$scope.pageNo = 1;
	$scope.count = 0;

	$scope.params = {};
	$scope.successCallback= function(){};
	$scope.isLazyLoadigRequired = true;

	$scope.modalContainer ='switchModalConfirmation';
	$scope.contactExchangeContainer ='saveIndTicketForm';
	$scope.ticketSuccessContainer='saveTicketSuccessForm';
	$scope.requestType = {reqSubTypeModel:'',requestTypeModel:'',priorityModel:'Medium', subjectModel:'', descriptionModel:defaultDescription};
	$scope.accountAlreadyCreated ='accountAlreadyCreated';
	$scope.resendActivationEmailToInd ='resendActivationEmailToInd';
	$scope.resendActEmailSuccess ='resendActEmailSuccess';
	$scope.markAsInActiveContainer ='markAsInActiveModalConfirmation';

	//this for making header scrollable
	$("#masthead").css("position","absolute");

	//This is to persist sortby value
	if(stateCode){
		$("#sortDataDD").val("FIRST_NAME_ASC");
	}else{
		$("#sortDataDD").val("DUE_DATE_ASC");
	}


	$scope.indActLinkModle = {email : "", phone1 : "", phone2 : "",phone3 : ""};
	$scope.loadissuers = function (){


		$http({	method: 'GET',
	        	url: '/hix/broker/loadIssuers'
	      }).success(function (data, status, headers, config) {


	    	 $scope.QHPIssuerNames = data.QHPIssuerNames;
	    	 $scope.QDPIssuerNames = data.QDPIssuerNames;
	    	 $scope.bothQHPAndQDPIssuerNames = data.bothQHPAndQDPIssuerNames;
		});

	};
	$scope.loadissuers();



	var  localFilters={
	      firstName:'',
	      lastName:'',
	      applicationType:'',
	      issuerval: '',
	      currentStatus:'',
	      nextStep:'',
	      dueDate:'',
	      currentStatus:'',
	      coverageYear:''
	     };

	$scope.loadData = function(loadingType, fromFilter){
		var issuervalVar='';

		if($("#issuer :selected").parent().attr("label")){
			issuervalVar= $("#issuer :selected").parent().attr("label") + '_'+$("#issuer").val();
		} else if($("#issuer").val()){
			if(stateCode){
			issuervalVar = $("#issuer").val();
		}
		}

		if(fromFilter){
			 localFilters={
				      firstName:$("#firstName").val(),
				      lastName:$("#lastName").val(),
				      applicationType:$("#applicationType").val(),
				      issuerval: issuervalVar,
				      currentStatus:$("#currentStatus").val(),
				      nextStep:$("#nextStep").val(),
				      dueDate:$("#dueDate").val(),
				      enrollmentStatus :$("#enrollmentStatus").val(),
				      currentStatus:$("#currentStatus").val(),
				      coverageYear:$("#coverageYear").val()
				     };
		}


		var loaderType = (loadingType === 'lazyLoading') ? 'lazyLoader' : 'loader';
		$("."+loaderType).show();
		var restService = function(){


			$scope.params = {
				     pageNumber:$scope.pageNo,
				     sortBy: $("#sortDataDD").val(),
				     isSortChanged : sortChanged ,
				     filters:localFilters
				   };

			agentEntityService.getAgentEntityData($scope.params).success(function (data, status, headers, config) {
				sortChanged = 'false';
				if(data.brokerBobDetailsDTO != undefined ){
					//console.log( data.brokerBobDetailsDTO.length)
					if ( data.brokerBobDetailsDTO.length > 0){
						$(".gridDataContainer").show();
						$(".noResultMsg").hide();
						//$scope.contacts = (loadingType === 'lazyLoading') ? $scope.contacts.concat(data.brokerBobDetailsDTO) : data.brokerBobDetailsDTO; $scope.pageNo = 1;
						if (loadingType === 'lazyLoading') {
							if(data.count <= 10){
								$scope.contacts = data.brokerBobDetailsDTO;
							}else{
								$scope.contacts = $scope.contacts.concat(data.brokerBobDetailsDTO);
							}
						} else {
							$scope.contacts = data.brokerBobDetailsDTO;
							$scope.pageNo = 1;
						}
						totalCountAtClient = data.count;
						$scope.count = data.brokerBobDetailsDTO.length;
						$scope.totalRecord = data.count;
						if($scope.totalRecord >0){
							$("#exportCSVLink").show();
						}else{
							$("#exportCSVLink").hide();
						}
						//$scope.contacts = $scope.contacts.concat(data.brokerBobDetailsDTO);
						$scope.isDataLoaded=true;
						flag = true;
						$scope.isDataLoaded=true;
						$scope.successCallback();
					}

				} else {
					$scope.contacts = {};
					$scope.totalRecord = data.count;
					$(".noResultMsg").show();
					$(".gridDataContainer").hide();
					if($scope.totalRecord >0){
						$("#exportCSVLink").show();
					}else{
						$("#exportCSVLink").hide();
					}
				}
				$("."+loaderType).hide();
			});

		}
		restService();
	};
	//$scope.loadData();


	// modal windwo for action  options
	$scope.getModalWindow = function(clickEvent){

		$scope.dialogTitle = $(clickEvent.target).html();
		$( "#dialog-box" ).dialog({
		  modal: true,
		  title:$scope.dialogTitle,
		  buttons: {
			Ok: function() {
			  $( this ).dialog( "close" );
			}
		  }
		});
	};

	// check is there any record to load
	$scope.isLazyLoadigRequired = function(clickEvent){
		var maxPageRecords= $scope.totalRecord/10;
		  maxPageRecords = maxPageRecords*10;
		  if( ($scope.totalRecord % 10) > 0){
		   maxPageRecords = maxPageRecords +10;
		  }
		  if ((($scope.pageNo +  1) * 10) <= maxPageRecords){
			return true;
		} else {
			return false;
		}
	};
	$scope.markInactive = function(event,contactData){
		$http({	method: 'GET',
	        	url: '/hix/broker/decline/individual/'+contactData.encryptedIndividualId +'?desigStatus=Active'
	      }).success(function (data, status, headers, config) {
	    	  if(data=='ok'){
	    		  $scope.pageNo = 1;
	    		  $scope.loadData("simpleLoading");
	    	  }
	    	  $(event.target).prop('disabled', false);
	    	  var modal = angular.element('#viewIndmarkAsInActiveModal');
	    	  modal.modal('hide');
		});
		$(event.target).prop('disabled', true);

	};

	$scope.markInactivePopup = function(contactData){
		$scope.modalContactData =contactData;
		var modal = angular.element('#viewIndmarkAsInActiveModal');
		setTimeout(function() {
			 modal.modal('show');


        });

	};

	$scope.redirectToCASwitch = function(contactData){
		window.location.href=contactData.existURL;
	};

	$scope.switchRole = function(contactData){
		$scope.modalContactData =contactData;
		if(stateCode){
			var recordTypeNumber = 3 ;
			if(recordTypeVar == 'broker' || recordTypeVar == 'agency_manager'){
				recordTypeNumber=2;
			}
			var existURL = $( '#switchForm' ).attr( 'action' );

			if (existURL.indexOf('ahbxId' + "=") >= 0)
		    {
		        var prefix = existURL.substring(0, existURL.indexOf('ahbxId'));
		        var suffix = existURL.substring(existURL.indexOf('ahbxId'));
		        suffix = suffix.substring(suffix.indexOf("=") + 1);
		        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
		        existURL = prefix + 'ahbxId' + "=" + contactData.encryptedIndividualId + '&recordType=' + recordTypeNumber    + suffix;

		    } else {
			    if (existURL.indexOf("?") < 0)
			    	existURL += "?" + 'ahbxId' + "=" + contactData.encryptedIndividualId + '&recordType=' + recordTypeNumber;
			    else
			    	existURL += "&" + 'ahbxId' + "=" + contactData.encryptedIndividualId + '&recordType=' + recordTypeNumber;
		    }
			if("N" == contactData.showSwitchRolePopup ){
				window.location.href = existURL;
			}else{
				$scope.modalContactData.existURL = existURL;
				var modal = angular.element('#viewindModal');
				setTimeout(function() {
					 modal.modal('show');
	            });
			}

			return true;
		}


		if("N" == contactData.showSwitchRolePopup ){
			var frm = angular.element('#switchForm');
			 setTimeout(function() {
				 frm.submit();
             });
		}else{
			var modal = angular.element('#viewindModal');
			setTimeout(function() {
				 modal.modal('show');
            });
		}

	};

	//household modal controller
	$scope.showHouseHoldInfo = function(data){
		 $scope.$broadcast ('hoseHoldInfoEvent' ,data);
	}

	//household modal controller
	$scope.showEligiblityInfo = function(data){
		 $scope.$broadcast ('eligibilityInfoEvent',data);
	}

	//resendActivationEmaild modal controller
	$scope.resendActivationEmail = function(contactData){
		$scope.modalContactData =contactData;
		$http({	method: 'GET',
	        	url: '/hix/individual/checkIndividualAccount/'+ contactData.encryptedIndividualId
	      }).success(function (data, status, headers, config) {
	    	  if(data=='Activated'){
	    		  var modal = angular.element('#resendFailureModal');
	  			  setTimeout(function() {
	  				 modal.modal('show');
	              });
	    	  }else if(data=='unActivated'){
	    		  if($scope.modalContactData.phoneNumber){
	    			  $scope.indActLinkModle.phone1 = $scope.modalContactData.phoneNumber.substring(0, 3);
		    		  $scope.indActLinkModle.phone2 = $scope.modalContactData.phoneNumber.substring(3, 6);
		    		  $scope.indActLinkModle.phone3 = $scope.modalContactData.phoneNumber.substring(6, 10);
	    		  }else{
	    			  $scope.indActLinkModle.phone1 = "";
		    		  $scope.indActLinkModle.phone2 = "";
		    		  $scope.indActLinkModle.phone3 = "";
	    		  }

	    		  $scope.indActLinkModle.email = $scope.modalContactData.emailAddress;
	    		  $scope.showInvalidEmailError=false;
	    		  var modal = angular.element('#resendActivationModal');
	  			  setTimeout(function() {
	  				 modal.modal('show');
	              });
	    	  }
		});

	};

	$scope.createTicketPopup = function(contactData){
		$scope.requestType={reqSubTypeModel:'', requestTypeModel:'', priorityModel:'Medium', subjectModel:'', descriptionModel:defaultDescription};
		$scope.modalContactData =contactData;
		//ticket popup
		$http({	method: 'GET',
        	url: '/hix/broker/contactYHIPopup/'+ contactData.ssapApplicationId
      }).success(function (data, status, headers, config) {
    	  if(null != data){
    		  var applicationData = data;
    		  if(applicationData.birthDateString!=null){
    			  $scope.modalContactData.birthDateString = applicationData.birthDateString;
    		  }else{
    			  $scope.modalContactData.birthDateString = "";
    		  }
    		  if(applicationData.ssn!=null){
    			  $scope.modalContactData.ssn = applicationData.ssn;
    		  }else{
    			  $scope.modalContactData.ssn = "";
    		  }

    		  if($scope.modalContactData.issuerName == null){
    			  $scope.modalContactData.issuerName = "";
    		  }
    		  if($scope.modalContactData.planName == null){
    			  $scope.modalContactData.planName = "";
    		  }
    	  }
    	  $scope.requestType.descriptionModel = 'Enter the following primary consumer information:\n'+
  		'Application ID: '+ $scope.modalContactData.ssapApplicationId +'\n'+
  		"Applicant's Last Name: "+ $scope.modalContactData.lastName +"\n"+
  		"Applicant's Date of Birth: "+$scope.modalContactData.birthDateString +"\n"+
  		"Last 4 digits of the Applicant's SSN: "+ $scope.modalContactData.ssn +"\n\n"+
  		'Enter the following issuer and plan information: \n'+
  		'Issuer Name: '+ $scope.modalContactData.issuerName +'\n'+
  		'Consumer Plan: '+ $scope.modalContactData.planName +'\n\n'+
  		'Describe the situation with as much detail as possible:\n\n'+
  		'Please enter the steps that you have taken so far to assist the consumer with the above issue (if any);\n\n';
  			var modal = angular.element('#contactExchnagePopupModal');
  			setTimeout(function() {
  				 modal.modal('show');
              });
	});

	};

	$scope.commentPopup = function(data){
		window.location ="/hix/individual/individualcomments/"+data.encryptedIndividualId;
	};

	if(!stateCode){
	  $http({ method: 'GET',
	         url: '/hix/broker/ticket/create'
	       }).success(function (data, status, headers, config) {
	        if(null != data){
	         var jsonResponse = data;
	         $scope.categoryRes = jsonResponse.category;
	         $scope.priorityArray = jsonResponse.priority;

	        }
	  });
	}

	$scope.changeSubCategory= function(subCat){
		$scope.subTypeArray=$scope.categoryRes[subCat];
	};

	$scope.submitTicketInfo = function(){
	 	var transform = function(data){
		        return $.param(data);
		    }

		    $http.post('/hix/broker/ticket/create/'+$scope.modalContactData.encryptedIndividualId +'/'+encFromModule, $scope.requestType, {
		        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',"csrftoken": $('#tokid').val()},
		        transformRequest: transform
		    }).success(function(responseData) {
		    	if(responseData.indexOf("application-error") != -1 || responseData.indexOf("Application Error") != -1){
		    		alert('Error occured');
	        	}else{
	        		$scope.ticketNumber = responseData;
	        		var modal = angular.element('#contactExchnagePopupModal');
	    			//setTimeout(function() {
	    			modal.modal('hide');

	    			var modalTkm = angular.element('#ticketSuccessPopupModal');

	    			modalTkm.modal('show');
	               // });

	        	}
		    });

	};


	$( document ).ready(function() {
		$(window).scroll(function() {
			var _scrollAmount = $(window).scrollTop(), _windowHeight =  $(window).height(),  _documentHeight = $(document).height() - 100 ;
			if(_scrollAmount + _windowHeight > _documentHeight ) {
				//console.log($(window).scrollTop() + $(window).height() +"==================="+ $(document).height())
				if (flag && $scope.isLazyLoadigRequired())
				{
					flag = false;
					$scope.pageNo = $scope.pageNo + 1;
					$scope.loadData('lazyLoading');

				}
			}
		});
		//var sticky = new Waypoint.Sticky({ element: $('#panel')[0]});
		$scope.loadData();
	});

	$scope.sendIndividualActivationLink = function(){
		var localContactData = $scope.modalContactData;
		var localPhoneAndemail = angular.copy($scope.indActLinkModle);
		var transform = function(data){
	        return $.param(data);
	    }

	    $http.post('/hix/broker/sendindividualactivationlink/'+$scope.modalContactData.encryptedIndividualId, $scope.indActLinkModle, {
	        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',"csrftoken": $('#tokid').val()},
	        transformRequest: transform
	    }).success(function(responseData) {
        	if(responseData=='linkSent'){
        		localContactData.phoneNumber = localPhoneAndemail.phone1+localPhoneAndemail.phone2+localPhoneAndemail.phone3;
        		localContactData.emailAddress = localPhoneAndemail.email;
        		var modal = angular.element('#resendActivationModal');
	  			modal.modal('hide');
	  			var modal = angular.element('#resendSuccessModal');
	  			setTimeout(function() {
	  				modal.modal('show');
	  			});
	    	}
        	if(responseData=='userExists'){
        		$scope.showInvalidEmailError=true;
	    	}
	    	if(responseData=='Invalied input'){
	    		  alert("Invalied input");
	    	}
	    });

	};

});
