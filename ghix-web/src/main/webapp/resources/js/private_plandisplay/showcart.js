(function(){
	_.templateSettings = {
		    interpolate: /\<\@\=(.+?)\@\>/gim,
		    evaluate: /\<\@(.+?)\@\>/gim
		};
	window.App = {};
	App.collections = {};
	App.models = {};
	App.views = {};
	App.backbone = {};
	
	// Defer initialization until doc ready.
	$(function(){
		var cartCollection = new App.collections.Carts();
		var cartModel = new App.models.Cart();
		App.views.carts = new App.views.Carts({collection:cartCollection,cartModel:cartModel});
	});

})();