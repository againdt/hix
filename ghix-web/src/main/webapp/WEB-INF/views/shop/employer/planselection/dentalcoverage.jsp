<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script>
	var employerIndvContribution = 50; 
	var employerDepeContribution = 0;
	var minDentalPremium = ${minPremiumDental};
	var maxDentalPremium = ${maxPremiumDental};
	var defaultEmployerIndvContribution = 0; 
</script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="<c:url value='/resources/js/shop/eps-common.js' />"></script>

<script type="text/javascript">
function populateEmployerCost(){
	var contributionEmployee = parseInt($("#contributionTowardsEmployeesDental").val()) * parseInt('${employeeCount}');
	var contributionDependent= parseInt($("#contributionTowardsDependentsDental").val()) * parseInt('${dependentCount}');

	contributionEmployee = isNaN(contributionEmployee) ? 0 : contributionEmployee;
	contributionDependent = isNaN(contributionDependent) ? 0 : contributionDependent;
	$("#yourCostAmount").html(parseInt(contributionEmployee)+parseInt(contributionDependent));
}
</script>
<div class="gutter10">  	
	<div class="row-fluid margin30-t">
	    <div class="span3" id="sidebar">
	    	<div class="header">
	      		<h4>Coverage Setup</h4>
	      	</div>
	      	<ul class="nav nav-list">
	      		<li><a href="<c:url value='/shop/employer/planselection/gettingstarted' />"><i class="icon-ok"></i> Getting Started</a></li>
	      		<li><a href="<c:url value='/shop/employer/planselection/healthcoverage' />"><i class="icon-ok"></i> Health Coverage Setup</a></li>
	      		<li class="active"><a href="#">3 Dental Coverage Setup</a></li>
	      		<li><a href="#">4 Review and Confirm</a></li>
	      	</ul>

	      	<div class="header margin30-t">
	      		<h4>Quick Links</h4>
	      	</div>
	      	<ul class="nav nav-list">
	      		<li><a href="<c:url value='/broker/search' />" id="findbroker"><i class="icon-search"></i> Find an Agent</a></li>
	      	</ul>
	    </div>
    
	    <div class="span9" id="rightpanel">
	    	<div class="header">
	    		<h4 class="pull-left">Dental Coverage Setup</h4>
	    		<a href="#"  onclick="submitForm();" class="btn btn-small btn-primary pull-right dentalCoverageSetupActionNext">Next</a>
	    		<a href="<c:url value="/shop/employer/planselection/healthcoverage" />" class="btn btn-small pull-right dentalCoverageSetupActionBackReturn">Back</a>
	    	</div>
	    	<div class="gutter10">
		    	<form class="form-horizontal" id="dentalcoverageform" name="dentalcoverageform" action="dentalcoverage" method="post">
				<df:csrfToken/>

					<div class="gutter10" id="coverageSetupSuggestions">
						<div class="row-fluid margin20-b">
							<p class="span10">We have made some initial suggestions for your dental care setup options.  Feel free to make changes and see how it affects your monthly cost.</p>
							<div class="span2 img-polaroid" id="yourCostWrapper">
								<ul>
									<li>Your Cost</li>
									<li class="loadingImg"><img src="img/loadingGif.gif" alt=""></li>
									<li class="amount"><span id="yourCostAmount">$0</span> monthly</li>
								</ul>
							</div>
						</div><!-- .row-fluid -->

						<div class="accordion" id="coverageSetupAccordion">
							<div class="accordion-group margin10-b">
						    	<div class="accordion-heading">
						    		<span class="pull-left">Your Contribution Amount per Employee</span>
						      		<a class="accordion-toggle pull-right btn btn-small" data-toggle="collapse" data-parent="#coverageSetupAccordion" href="#collapseOne" data-text-swap="Close">Change</a>
						      		<span class="pull-right contributionTowardsEmployeesDentalResult">0</span>
						    	</div>
						    	<div id="collapseOne" class="accordion-body collapse">
						      		<div class="accordion-inner">
						      			<p class="margin10-t">Monthly premiums for dental plans available to your employees range from <strong>$${minPremiumDental}-$${maxPremiumDental}</strong> per month.</p>
						        		<p>Select the dollar amount you are willing to contribute towards each employee's monthly dental insurance premium.  The portion you don't cover will be paid by your employees.</p>
						        		
						        		<div class="slider-home row-fluid">
											<div class="sliderTowardsEmployeesDental span9"></div>
    										<div class="span3 contributionTowardsEmployeesDentalResult">0</div>  
    										<input type="hidden" id="contributionTowardsEmployeesDental" name="contributionTowardsEmployeesDental"  value="${employeeContribution}"  />
										</div>	
						      		</div>
						    	</div>
						  	</div>

						  	<div class="accordion-group margin10-b">
						    	<div class="accordion-heading">
						    		<span class="pull-left">Your Contribution Amount per Dependent</span>
						      		<a class="accordion-toggle pull-right btn btn-small" data-toggle="collapse" data-parent="#coverageSetupAccordion" href="#collapseTwo" data-text-swap="Close">Change</a>
						      		<span class="pull-right contributionTowardsDependentsDentalResult">0</span>
						    	</div>
						    	<div id="collapseTwo" class="accordion-body collapse">
						      		<div class="accordion-inner">
						      			<p class="margin10-t">Monthly premiums for dental plans available to the dependents of your employees range from <strong>$${minPremiumDental}-$${maxPremiumDental}</strong> per month.</p>
						        		<p>Select the dollar amount you are willing to contribution towards the monthly dental insurance premium of each dependent of your employees.  The portion you don't cover will be paid by your employees.</p>

						        		<div class="slider-home row-fluid">
											<div class="sliderTowardsDependentsDental span9"></div>
    										<div class="span3 contributionTowardsDependentsDentalResult">0</div>  
    										<input type="hidden" id="contributionTowardsDependentsDental" name="contributionTowardsDependentsDental"  value="${dependentContribution}"  />
										</div>
						      		</div>
						    	</div>
						  	</div>
						</div><!-- #accordion -->
					</div>

					<div class="form-actions">
						<a href="#" class="btn btn-primary btn-large dentalCoverageSetupActionNext" onclick="submitForm();">These look good to me &gt;&gt;</a>
					</div>
				</form>
	    	</div><!-- .gutter10 -->
		</div><!-- .span9 #rightpanel -->
	</div><!-- .row-fluid -->
</div><!-- .gutter10 -->


<script>
$(document).ready(function(){

	    $( ".contributionTowardsEmployeesDentalResult").text('${employeeContribution}');
	 	$( ".contributionTowardsDependentsDentalResult" ).text('${dependentContribution}');
	
		$(".sliderTowardsEmployeesDental").slider('value', '${employeeContribution}');
		$(".sliderTowardsDependentsDental").slider('value', '${dependentContribution}');
		
	$('.ui-slider-handle').on('click', function(){
		$('li.amount').hide();
		$('li.loadingImg').show().delay(1000).hide(0);
		$('li.amount').delay(1000).show(0);
	});

});	

function submitForm(){
	$('#dentalcoverageform').submit();
}

function openFindBrokerModal(){
	$('#findbroker').modal('show');
}
 $(function() {
	 
	 $('#findbroker').click(function(e){
			e.preventDefault();
			var href = $(this).attr('href');
			if (href.indexOf('#') != 0)
				openFindBrokerDialog(href);
		});
	 
     $('#addempdiv :a').attr('disabled', 'disabled'); 
});
 
 $('.modalClose').live('click',function(){
		$('#modal, .modal-backdrop').remove();
		window.location.reload();
	});

$(document).keyup(function(e) {
	if (e.keyCode == 27) {
		$('#modal').remove();
	}
});
</script>