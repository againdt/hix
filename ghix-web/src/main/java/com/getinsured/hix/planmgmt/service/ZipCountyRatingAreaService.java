package com.getinsured.hix.planmgmt.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.AdminDocument;
import com.getinsured.hix.model.ZipCountyRatingArea;

public interface ZipCountyRatingAreaService {
	Integer getAllCountyAreaRatingCount();

	boolean saveZipCountyRatingArea(ZipCountyRatingArea zipCountyRatingArea);

	void removeAllContentsFromZipCounty();

	String saveFile(String fileLocation, MultipartFile mfile, String suggestedFileName);

	List<AdminDocument> getAllAdminDocument(String documentType);

	boolean saveDocument(AdminDocument adminDocument);
}
