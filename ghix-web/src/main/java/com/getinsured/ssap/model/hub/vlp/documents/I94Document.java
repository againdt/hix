package com.getinsured.ssap.model.hub.vlp.documents;

import java.util.Date;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * I-94 Document.
 *
 * @author Yevgen Golubenko
 * @since 2/15/19
 */
public class I94Document extends DhsDocument
{
  public I94Document()
  {
    super.setDocumentType(DocumentType.I94);
  }

  @JsonProperty("I94Number")
  private String i94Number;

  @JsonProperty("SEVISID")
  private String sevisId;

  @JsonFormat(pattern = DATE_FORMAT)
  @JsonProperty("DocExpirationDate")
  private Date docExpirationDate;

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isValid()
  {
    return notBlank(this.i94Number);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @SuppressWarnings("Duplicates")
  public DhsDocument from(final CitizenshipDocument citizenshipDocument)
  {
    final I94Document document = new I94Document();

    document.setSevisId(getSevisId(citizenshipDocument));
    document.setI94Number(validValue(citizenshipDocument.getI94Number()));
    document.setDocExpirationDate(citizenshipDocument.getDocumentExpirationDate());

    return document;
  }

  public String getI94Number()
  {
    return i94Number;
  }

  public void setI94Number(final String i94Number)
  {
    this.i94Number = i94Number;
  }

  public String getSevisId()
  {
    return sevisId;
  }

  public void setSevisId(final String sevisId)
  {
    this.sevisId = sevisId;
  }

  public Date getDocExpirationDate()
  {
    return docExpirationDate;
  }

  public void setDocExpirationDate(final Date docExpirationDate)
  {
    this.docExpirationDate = docExpirationDate;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", I94Document.class.getSimpleName() + "[", "]")
        .add("i94Number='" + i94Number + "'")
        .add("sevisId='" + sevisId + "'")
        .add("docExpirationDate=" + docExpirationDate)
        .toString();
  }
}
