package com.getinsured.hix.batch.migration.couchbase.ecm;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.getinsured.hix.batch.migration.couchbase.ecm.DataConfig.MigrationStepEnum;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.platform.couchbase.CouchBaseUtil;
import com.getinsured.hix.platform.couchbase.dto.CouchBinary;
import com.getinsured.hix.platform.couchbase.service.CouchBucketService;
import com.getinsured.hix.platform.ecm.CMISSessionUtil;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.couchbase.dto.CouchEcmDocument;

public class MigrateDocumentsTask extends BaseEcmCouchMigrationClass {

	private static final String ENCOUNTERED_EXCEPTION_IN_THE_PROCESS_OF_MIGRATING_DOCUMENTS_FROM_ALFRESCO_TO_COUCHBASE = "Encountered Exception in the process of migrating documents from Alfresco to Couchbase";

	private static final String PROCESS_OF_MIGRATING_DOCUMENTS_FROM_ALFRESCO_TO_COUCHBASE_COMPLETED_SUCCESSFULLY = "Process of migrating documents from Alfresco to Couchbase completed successfully";

	private static final String INITIATE_THE_PROCESS_OF_MIGRATING_DOCUMENTS_FROM_ALFRESCO_TO_COUCHBASE = "Initiate the process of migrating documents from Alfresco to Couchbase";

	private static final Logger LOGGER = LoggerFactory.getLogger(MigrateDocumentsTask.class);

	private CouchBucketService couchBucketService;

	private ContentManagementService ecmService;

	private JdbcTemplate jdbcTemplate;

	public MigrateDocumentsTask() {
		super(MigrationStepEnum.COPY);
	}

	public void setCouchBucketService(CouchBucketService couchBucketService) {
		this.couchBucketService = couchBucketService;
	}

	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	void startExecution() throws Exception {
		Connection con = null;
		try {
			LOGGER.info(INITIATE_THE_PROCESS_OF_MIGRATING_DOCUMENTS_FROM_ALFRESCO_TO_COUCHBASE);
			con = jdbcTemplate.getDataSource().getConnection();
			for (CouchbaseECMMigrationDTO tableDTO : DataConfig.migrationTables.values()) {
				LOGGER.info("Migrating documents from Table - " + tableDTO.getTableName() + ", Column - "
						+ tableDTO.getColumnName());
				int maxPrimaryRecordId = extractMaximumPrimaryRecordId(con, tableDTO.getTableName(),
						tableDTO.getColumnName(), DataConfig.MigrationStatusEnum.NOTSTARTED);
				LOGGER.info("Maximum Primary Id for Table - " + tableDTO.getTableName() + ", Column - "
						+ tableDTO.getColumnName() + " is - " + maxPrimaryRecordId);
				if (maxPrimaryRecordId != 0) {
					startDocumentMigration(con, tableDTO.getTableName(), tableDTO.getColumnName(),
							tableDTO.getCategory(), tableDTO.getSubCategory(), maxPrimaryRecordId);
				}
			}
			//handleDuplicateEcm(con);
			LOGGER.info(PROCESS_OF_MIGRATING_DOCUMENTS_FROM_ALFRESCO_TO_COUCHBASE_COMPLETED_SUCCESSFULLY);
		} catch (Exception e) {
			LOGGER.error(ENCOUNTERED_EXCEPTION_IN_THE_PROCESS_OF_MIGRATING_DOCUMENTS_FROM_ALFRESCO_TO_COUCHBASE, e);
			throw e;
		} finally {
			if (con != null) {
				try {
					con.setAutoCommit(true);
				} catch (Exception e2) {

				}

				try {
					con.close();
				} catch (Exception e2) {

				}
			}
		}

	}

//	private List<Map<String, String>> fetchDuplicateRecords(Connection con) throws SQLException {
//		StringBuilder st = new StringBuilder(50);
//
//		st.append("select distinct o.TABLE_NAME,o.COLUMN_NAME,o.EXISTING_ECM_ID,")
//				.append("(select min(i.MIGRATION_ID) from COUCH_MIGRATION i where i.TABLE_NAME = o.TABLE_NAME and i.COLUMN_NAME = o.COLUMN_NAME ")
//				.append(" and i.EXISTING_ECM_ID = o.EXISTING_ECM_ID and migration_status not in ('")
//				.append(DataConfig.MigrationStatusEnum.DUPLICATE.name()).append("')) from ")
//				.append(DataConfig.COUCH_MIGRATION).append(" o where migration_status in ('")
//				.append(DataConfig.MigrationStatusEnum.DUPLICATE.name()).append("')");
//
//		return JDBCUtil.records(con, st.toString(), new String[] { DataConfig.TABLE_NAME, DataConfig.COLUMN_NAME,
//				DataConfig.ECM_ID, DataConfig.DB_MIGRATION_ID });
//	}

//	private void handleDuplicateEcm(Connection con) throws SQLException {
//		con.setAutoCommit(true);
//		List<Map<String, String>> data = fetchDuplicateRecords(con);
//		int iSize = CouchMigrationUtil.listSize(data);
//		LOGGER.info(iSize + " - Ecm records having duplicate records");
//		if (iSize == 0) {
//			return;
//		}
//		StringBuilder st = new StringBuilder(50);
//
//		for (Map<String, String> map : data) {
//			if (!StringUtils.isEmpty(map.get(DataConfig.DB_MIGRATION_ID))) {
//				Map<String, String> mainRecord = migratedRecordDetails(con, map.get(DataConfig.DB_MIGRATION_ID));
//				if (mainRecord != null) {
//					st.append("update ").append(DataConfig.COUCH_MIGRATION).append(" set MIGRATION_STATUS = '")
//							.append(migratedStatus(mainRecord.get("1"))).append("', COUCHBASE_ID = ")
//							.append(migratedCouchbaseId(mainRecord.get("2"))).append(" where TABLE_NAME = '")
//							.append(map.get(DataConfig.TABLE_NAME)).append("' and COLUMN_NAME = '")
//							.append(map.get(DataConfig.COLUMN_NAME)).append("' and EXISTING_ECM_ID = '")
//							.append(map.get(DataConfig.ECM_ID)).append("' and MIGRATION_STATUS in ('")
//							.append(DataConfig.MigrationStatusEnum.DUPLICATE.name()).append("')");
//
//					LOGGER.info("Update Migration status for records - Table - " + map.get(DataConfig.TABLE_NAME)
//							+ ", Column - " + map.get(DataConfig.COLUMN_NAME) + ", EcmId - "
//							+ map.get(DataConfig.ECM_ID) + ", Query - \n " + st.toString());
//
//					final int uCount = JDBCUtil.executeUpdate(con, st.toString());
//					LOGGER.info(
//							uCount + " Records Updated with status " + DataConfig.MigrationStatusEnum.DUPLICATE.name());
//					st.setLength(0);
//				}
//			}
//		}
//	}

//	private String migratedCouchbaseId(String couchId) {
//		return StringUtils.isEmpty(couchId) ? null : "'" + couchId + "'";
//	}
//
//	private String migratedStatus(String preStatus) {
//		if (DataConfig.MigrationStatusEnum.COUCHBASEDONE.name().equals(preStatus)
//				|| DataConfig.MigrationStatusEnum.MIGRATIONDONE.name().equals(preStatus)) {
//			return DataConfig.MigrationStatusEnum.COUCHBASEDONE.name();
//		} else {
//			return preStatus;
//		}
//	}

//	private Map<String, String> migratedRecordDetails(Connection con, String string) throws SQLException {
//		return JDBCUtil.recordByColumnId(con, "select MIGRATION_STATUS, COUCHBASE_ID from " + DataConfig.COUCH_MIGRATION
//				+ " where MIGRATION_ID = " + string, 2);
//	}

	private void startDocumentMigration(Connection con, String tableName, String columnName, String category,
			String subCategory, int maxPrimaryRecordId) throws Exception {
		Pagination paging = new Pagination(maxPrimaryRecordId);
		List<Map<String, String>> data;
		int iSize;
		while (paging.isNext()) {
			LOGGER.info("Migrate records for Table - " + tableName + ", Column - " + columnName + " between - "
					+ paging.getStartRecordNumber() + " and " + paging.getEndRecordNumber());
			data = fetchRecordsToMigrate(con, tableName, columnName, paging.getStartRecordNumber(),
					paging.getEndRecordNumber());
			iSize = CouchMigrationUtil.listSize(data);

			if (iSize == 0) {
				continue;
			}
			LOGGER.info(iSize + " - records found for Table - " + tableName + ", Column - " + columnName + " between - "
					+ paging.getStartRecordNumber() + " and " + paging.getEndRecordNumber());
			migrateDocument(con, tableName, columnName, category, subCategory, data);
		}

	}

	private List<Map<String, String>> fetchRecordsToMigrate(Connection con, String tableName, String columnName,
			int startRecordNumber, int endRecordNumber) throws SQLException {
		StringBuilder st = new StringBuilder(50);
		st.append("select MIGRATION_ID, PRIMARY_ID, EXISTING_ECM_ID from ").append(DataConfig.COUCH_MIGRATION)
				.append(" where TABLE_NAME = '").append(tableName).append("' and COLUMN_NAME = '").append(columnName)
				.append("' and MIGRATION_STATUS in ('").append(DataConfig.MigrationStatusEnum.NOTSTARTED.name())
				.append("')").append(" and PRIMARY_ID between ").append(startRecordNumber).append(" and ")
				.append(endRecordNumber);

		return JDBCUtil.records(con, st.toString(),
				new String[] { DataConfig.MIGRATION_ID, DataConfig.PRIMARY_ID, DataConfig.EXISTING_ECM_ID });
	}

	private void migrateDocument(Connection con, String tableName, String columnName, String category,
			String subCategory, List<Map<String, String>> data) throws Exception {

		Stream.of(data).forEach((l) -> {
			l.parallelStream().forEach((m) -> {
				try {
					String couchBaseId = fetchAndPutInCouchbase(m, tableName, columnName, category, subCategory);
					updateMigrationtable(con, couchBaseId, m, tableName, columnName);
				} catch (Exception e){
					throw new RuntimeException(e);
				}
			});
		});

		/*
		String couchBaseId = null;

		for (Map<String, String> map : data) {
			couchBaseId = fetchAndPutInCouchbase(map, tableName, columnName, category, subCategory);
			updateMigrationtable(con, couchBaseId, map, tableName, columnName);
		}*/

	}

	@SuppressWarnings("unused")
	private void updateMigrationtable(Connection con, String couchBaseId, Map<String, String> map, String tableName,
			String columnName) throws Exception {
		try {
			con.setAutoCommit(false);
			StringBuilder st1 = new StringBuilder(50);
			StringBuilder st2 = new StringBuilder(50);

			if (CouchMigrationUtil.isValidString(couchBaseId)) {
				st1.append("update ").append(DataConfig.COUCH_MIGRATION).append(" set MIGRATION_STATUS = '")
						.append(DataConfig.MigrationStatusEnum.COUCHBASEDONE.name()).append("', COUCHBASE_ID = '")
						.append(couchBaseId).append("' where MIGRATION_ID = ").append(map.get(DataConfig.MIGRATION_ID));
				st2.append("update ").append(DataConfig.COUCH_MIGRATION).append(" set MIGRATION_STATUS = '")
						.append(DataConfig.MigrationStatusEnum.COUCHBASEDONE.name()).append("', COUCHBASE_ID = '")
						.append(couchBaseId).append("' where EXISTING_ECM_ID = '")
						.append(map.get(DataConfig.EXISTING_ECM_ID)).append("'").append(" and MIGRATION_STATUS = '")
						.append(DataConfig.MigrationStatusEnum.DUPLICATE.name()).append("'");
			} else {
				st1.append("update ").append(DataConfig.COUCH_MIGRATION).append(" set MIGRATION_STATUS = '")
						.append(DataConfig.MigrationStatusEnum.ECMFILENOTFOUND.name()).append("' where MIGRATION_ID = ")
						.append(map.get(DataConfig.MIGRATION_ID));
				st2.append("update ").append(DataConfig.COUCH_MIGRATION).append(" set MIGRATION_STATUS = '")
						.append(DataConfig.MigrationStatusEnum.ECMFILENOTFOUND.name()).append("' where EXISTING_ECM_ID = '")
						.append(map.get(DataConfig.EXISTING_ECM_ID)).append("'")
						.append(" and MIGRATION_STATUS = '").append(DataConfig.MigrationStatusEnum.DUPLICATE.name())
						.append("'");
			}

			int[] ireturn = JDBCUtil.executeBatchUpdate(con, new String[] { st1.toString(), st2.toString() });

			con.commit();
			st1 = null;
			st2 = null;
		} catch (Exception e) {
			try {
				con.rollback();
			} catch (Exception e2) {
				/** TODO should we eat the exception */
			}
		}
	}

	@SuppressWarnings("unused")
	private String fetchAndPutInCouchbase(Map<String, String> map, String tableName, String columnName, String category,
			String subCategory) throws Exception {
		String couchBaseId = null;
		try {
			Content content = ecmService.getContentById(map.get(DataConfig.EXISTING_ECM_ID));

			if (content == null) {
				return null;
			}

			byte[] contentData = ecmService.getContentDataById(map.get(DataConfig.EXISTING_ECM_ID));
			if (contentData == null || contentData.length == 0) {
				return null;
			}

			CouchEcmDocument document = formDocument(content, map.get(DataConfig.PRIMARY_ID),
					map.get(DataConfig.MIGRATION_ID), tableName, columnName, category, subCategory, contentData.length);

			final String documentId = couchBucketService.createDocument(document);

			final String[] documentContentIds = couchBucketService.createBinaryDocument(contentData,
					document.getBinaryMetaData().getContentLink());

			couchBaseId = documentId;
			

		} catch (Exception e) {
			LOGGER.error("==> Error from ECM ==> Exception -  ", e);
		}

		return couchBaseId;
	}

	private CouchEcmDocument formDocument(Content content, String primaryId, String migrationId, String tableName,
			String columnName, String category, String subCategory, int length) {

		CouchEcmDocument document = new CouchEcmDocument();
		document.setOriginalFileName(content.getOriginalFileName());
		document.setRelativePath(document.getRelativePath());
		document.getMetaData().setCategory(category);
		document.getMetaData().setSubCategory(subCategory);
		document.getMetaData().setCreationDate(content.getCreationDate());
		document.getMetaData().setModifiedDate(content.getModifiedDate());
		document.getMetaData().setCreatedBy("migrationadmin");
		document.getMetaData().setModifiedBy("migrationadmin");

		Map<String, String> mp = new HashMap<>();
		mp.put(DataConfig.ECM_ID, content.getContentId());
		mp.put(DataConfig.TABLE_NAME, tableName);
		mp.put(DataConfig.COLUMN_NAME, columnName);
		mp.put(DataConfig.DB_MIGRATION_ID, migrationId);
		mp.put(DataConfig.DB_PRIMARY_ID, primaryId);
		document.setCustomMetaData(mp);

		CouchBinary couchBinary = new CouchBinary();
		couchBinary.setFileName(content.getOriginalFileName());
		couchBinary.setFileSize(length);
		couchBinary.setHasContent(true);
		couchBinary.setDocType(CMISSessionUtil.getType(content.getOriginalFileName()));
		couchBinary.setMimeType(CMISSessionUtil.getMime(content.getOriginalFileName()) + "; charset=UTF-8");
		couchBinary.setNumberOfParts(CouchBaseUtil.numberOfParts(couchBinary.getFileSize()));
		document.setBinaryMetaData(couchBinary);
		return document;
	}

}
