import React, {Fragment} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import {changeData} from '../../actions/actionCreators';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import INPUT_TYPES from '../../constants/inputTypes';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';

// Components

// ActionCreators

// Utils

// Constants


/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Additional Information - Member Enrolled In Other Health Insurance
 *
 */
const Page4_62 = props => {

    const { data, taxHouseHold, householdMembers } = props;

    const taxFilerPersonID = taxHouseHold.primaryTaxFilerPersonId;

    if(taxFilerPersonID === 0) {
        return null;
    }

    const {
        reconciledAptc
    } = taxHouseHold;

    const {
        name
    } = householdMembers[taxFilerPersonID - 1];

    const onFieldChange = newData => {
        const newState = { ...data };
        newState.singleStreamlinedApplication.taxHousehold[0] = {
            ...newData,
            failureToReconcile: !newData.reconciledAptc
        };
        props.actions.changeData(newState);
    };

    return (
        <Fragment>
            <Form>
                <div id="aiReconciledAptc" className="fade-in">
                    <div className="subsection">
                        <UniversalInput
                            legend={{
                                legendText:
                                    <div>
                                        Did&nbsp;
                                        <strong>{normalizeNameOfPerson(name)}&nbsp;</strong>
                                        <a
                                            className="usa-external_link"
                                            href="https://help.nevadahealthlink.com/hc/en-us/articles/360028568612-How-to-reconcile-your-premium-tax-credit"
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        ><span className="font-weight-normal">reconcile premium tax credits</span></a>
                                        &nbsp;on their tax return for past years?
                                    </div>
                            }}
                            label={{
                                text: '',
                                labelClass: ['usa-width-one-whole', 'layout--02'],
                                ulClass:['usa-width-one-whole margin-b-20'],
                                showRequiredIcon: false
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                name: 'reconciledAptc',
                                value: reconciledAptc,
                                options: DROPDOWN_OPTIONS.RECONCILE_YES_NO,
                                currentData: taxHouseHold,
                                fields: []
                            }}
                            inputActions={{
                                onFieldChange: onFieldChange
                            }}
                            errorClasses={['margin-l-20-ve', 'margin-b-10-ve']}
                        />
                    </div>
                </div>
            </Form>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,

    taxHouseHold: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0],

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page4_62.defaultProps = {
    name: 'Page4_62'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page4_62);
