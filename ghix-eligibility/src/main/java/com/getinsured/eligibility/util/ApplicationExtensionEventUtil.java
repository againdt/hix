package com.getinsured.eligibility.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;

import com.getinsured.eligibility.at.resp.si.dto.ApplicantDetails;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.resp.si.dto.ApplicationExtension;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExtendedApplicantEventCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExtendedApplicantNonQHPCodeSimpleType;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralQualifyingEventCode;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
public final class ApplicationExtensionEventUtil {

	private ApplicationExtensionEventUtil() {
	}

	private static final long SIXTY_DAYS = 60l;

	private static final Set<String> APPLICANT_EXTENSION_DENIAL_EVENT = new HashSet<String>(Arrays.asList(ExtendedApplicantEventCodeSimpleType.CHANGE_IN_CITIZENSHIP.value(), ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEC.value(),ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEDICAID.value(),ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEC_PRESUMPTIVELY_VERIFIED.value(),
	        ExtendedApplicantEventCodeSimpleType.BIRTH.value(), ExtendedApplicantEventCodeSimpleType.ADOPTION.value(), ExtendedApplicantEventCodeSimpleType.MARRIAGE.value(), ExtendedApplicantEventCodeSimpleType.ADD_DEPENDENT.value(),
	        ExtendedApplicantEventCodeSimpleType.MOVED_INTO_STATE.value(), ExtendedApplicantEventCodeSimpleType.CHANGE_IN_ADDRESS.value(),ExtendedApplicantEventCodeSimpleType.BIRTH_OR_ADOPTION.value(),ExtendedApplicantEventCodeSimpleType.OTHER_ELIGIBILITY_CHANGE.value()));

	public static final Set<String> FUTURE_DATED_LCE_EVENTS = new HashSet<String>(Arrays.asList(ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEC.value(),
			ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEDICAID.value(),ExtendedApplicantEventCodeSimpleType.GAIN_OF_MEDICAID.value(),
			ExtendedApplicantEventCodeSimpleType.REMOVE_DEPENDENT.value(), ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEC_PRESUMPTIVELY_VERIFIED.value(),
			ExtendedApplicantEventCodeSimpleType.MARRIAGE.value(), ExtendedApplicantEventCodeSimpleType.INCOME_CHANGE.value()));

	private final static Map<String, ExtendedApplicantEventCodeSimpleType[]> STATUS_EVENT_MAP = new HashMap<String, ExtendedApplicantEventCodeSimpleType[]>() {
		private static final long serialVersionUID = -9136402578487499707L;
		{
			put(ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS.value(), new ExtendedApplicantEventCodeSimpleType[] { ExtendedApplicantEventCodeSimpleType.CHANGE_IN_CITIZENSHIP });
			put(ApplicantStatusEnum.ADD_NEW_ELIGIBILE.value(), new ExtendedApplicantEventCodeSimpleType[] { ExtendedApplicantEventCodeSimpleType.BIRTH, ExtendedApplicantEventCodeSimpleType.ADOPTION, ExtendedApplicantEventCodeSimpleType.MARRIAGE,
			        ExtendedApplicantEventCodeSimpleType.ADD_TAX_DEPENDENT, ExtendedApplicantEventCodeSimpleType.ADD_DEPENDENT, ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEC,ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEDICAID, ExtendedApplicantEventCodeSimpleType.MOVED_INTO_STATE,ExtendedApplicantEventCodeSimpleType.CHANGE_IN_CITIZENSHIP,
			        ExtendedApplicantEventCodeSimpleType.BIRTH_OR_ADOPTION,ExtendedApplicantEventCodeSimpleType.OTHER_ELIGIBILITY_CHANGE,ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEC_PRESUMPTIVELY_VERIFIED });
			put(ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE.value(), new ExtendedApplicantEventCodeSimpleType[] { ExtendedApplicantEventCodeSimpleType.BIRTH, ExtendedApplicantEventCodeSimpleType.ADOPTION, ExtendedApplicantEventCodeSimpleType.MARRIAGE,
			        ExtendedApplicantEventCodeSimpleType.ADD_TAX_DEPENDENT, ExtendedApplicantEventCodeSimpleType.ADD_DEPENDENT, ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEC,ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEDICAID, ExtendedApplicantEventCodeSimpleType.MOVED_INTO_STATE,ExtendedApplicantEventCodeSimpleType.CHANGE_IN_CITIZENSHIP,
			        ExtendedApplicantEventCodeSimpleType.BIRTH_OR_ADOPTION,ExtendedApplicantEventCodeSimpleType.OTHER_ELIGIBILITY_CHANGE,ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEC_PRESUMPTIVELY_VERIFIED });
			put(ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE.value(), new ExtendedApplicantEventCodeSimpleType[] { ExtendedApplicantEventCodeSimpleType.REMOVE_DEPENDENT, ExtendedApplicantEventCodeSimpleType.DIVORCE_OR_ANULLMENT,
					ExtendedApplicantEventCodeSimpleType.OTHER_ELIGIBILITY_CHANGE,ExtendedApplicantEventCodeSimpleType.GAIN_OF_MEC,ExtendedApplicantEventCodeSimpleType.GAIN_OF_MEDICAID});
			put(ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE.value(), new ExtendedApplicantEventCodeSimpleType[] { ExtendedApplicantEventCodeSimpleType.REMOVE_DEPENDENT, ExtendedApplicantEventCodeSimpleType.DIVORCE_OR_ANULLMENT,ExtendedApplicantEventCodeSimpleType.OTHER_ELIGIBILITY_CHANGE
					,ExtendedApplicantEventCodeSimpleType.GAIN_OF_MEC,ExtendedApplicantEventCodeSimpleType.GAIN_OF_MEDICAID});
			put(ApplicantStatusEnum.UPDATED_ZIP_COUNTY.value(), new ExtendedApplicantEventCodeSimpleType[] { ExtendedApplicantEventCodeSimpleType.CHANGE_IN_ADDRESS,ExtendedApplicantEventCodeSimpleType.MOVED_INTO_STATE });
			put(ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT.value(), new ExtendedApplicantEventCodeSimpleType[] { ExtendedApplicantEventCodeSimpleType.INCOME_CHANGE });
			put(ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL.value(), new ExtendedApplicantEventCodeSimpleType[] { ExtendedApplicantEventCodeSimpleType.INCOME_CHANGE });
		}
	};

	private final static Map<String, ExtendedApplicantNonQHPCodeSimpleType[]> STATUS_QHP_EVENT_MAP = new HashMap<String, ExtendedApplicantNonQHPCodeSimpleType[]>() {
		private static final long serialVersionUID = 1116750927096870725L;
		{
			put(ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE.value(), new ExtendedApplicantNonQHPCodeSimpleType[] { ExtendedApplicantNonQHPCodeSimpleType.DEATH, ExtendedApplicantNonQHPCodeSimpleType.INCARCERATION,
			        ExtendedApplicantNonQHPCodeSimpleType.CHANGE_IN_LEGAL_PRESENCE, ExtendedApplicantNonQHPCodeSimpleType.MOVED_OUT_OF_STATE, 
			        ExtendedApplicantNonQHPCodeSimpleType.DIVORCE_OR_ANULLMENT,ExtendedApplicantNonQHPCodeSimpleType.GAIN_OF_MEC,ExtendedApplicantNonQHPCodeSimpleType.REMOVE_DEPENDENT });
			put(ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE.value(), new ExtendedApplicantNonQHPCodeSimpleType[] { ExtendedApplicantNonQHPCodeSimpleType.DEATH, ExtendedApplicantNonQHPCodeSimpleType.INCARCERATION,
			        ExtendedApplicantNonQHPCodeSimpleType.CHANGE_IN_LEGAL_PRESENCE, ExtendedApplicantNonQHPCodeSimpleType.MOVED_OUT_OF_STATE, 
			        ExtendedApplicantNonQHPCodeSimpleType.DIVORCE_OR_ANULLMENT,ExtendedApplicantNonQHPCodeSimpleType.GAIN_OF_MEC,ExtendedApplicantNonQHPCodeSimpleType.REMOVE_DEPENDENT });
		}
	};

	public final static Map<String, String> QEP_EVENT_NAME_MAP = new HashMap<String, String>() {
		private static final long serialVersionUID = -9136402578487499807L;
		{
			put(ExtendedApplicantEventCodeSimpleType.CHANGE_IN_CITIZENSHIP.value(), ReferralQualifyingEventCode.CHANGE_IN_CITIZENSHIP);
			put(ExtendedApplicantEventCodeSimpleType.MOVED_INTO_STATE.value(), ReferralQualifyingEventCode.MOVED_INTO_STATE);
			put(ExtendedApplicantEventCodeSimpleType.DIVORCE_OR_ANULLMENT.value(), ReferralQualifyingEventCode.DIVORCE);
			put(ExtendedApplicantEventCodeSimpleType.BIRTH.value(), ReferralQualifyingEventCode.BIRTH);
			put(ExtendedApplicantEventCodeSimpleType.ADOPTION.value(), ReferralQualifyingEventCode.ADOPTION);
			put(ExtendedApplicantEventCodeSimpleType.REMOVE_DEPENDENT.value(), ReferralQualifyingEventCode.REMOVE_DEPENDENT);
			put(ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEC.value(), ReferralQualifyingEventCode.LOST_OTHER_MIN_ESSENTIAL_COVERAGE);
			put(ExtendedApplicantEventCodeSimpleType.PRIMARY_IN_ANOTHER_APPLICATION_INCARCERATED.value(), ReferralQualifyingEventCode.PRIMARY_IN_ANOTHER_APPLICATION_INCARCERATED);
			put(ExtendedApplicantEventCodeSimpleType.PRIMARY_IN_ANOTHER_APPLICATION_DIED.value(), ReferralQualifyingEventCode.PRIMARY_IN_ANOTHER_APPLICATION_DIED);
			put(ExtendedApplicantEventCodeSimpleType.EXEMPTION_CANCELLED.value(), ReferralQualifyingEventCode.EXEMPTION_CANCELLED);
			put(ExtendedApplicantEventCodeSimpleType.ADD_DEPENDENT.value(), ReferralQualifyingEventCode.CHANGE_IN_HOUSEHOLD_SIZE);
			put(ExtendedApplicantEventCodeSimpleType.MARRIAGE.value(), ReferralQualifyingEventCode.MARRIAGE);
			put(ExtendedApplicantEventCodeSimpleType.CHANGE_IN_ADDRESS.value(), ReferralQualifyingEventCode.CHANGE_IN_ADDRESS);
			put(ExtendedApplicantEventCodeSimpleType.INCOME_CHANGE.value(), ReferralQualifyingEventCode.INCOME_CHANGE_CSR);
			put(ExtendedApplicantEventCodeSimpleType.BIRTH_OR_ADOPTION.value(),ReferralQualifyingEventCode.BIRTH_OR_ADOPTION);
			put(ExtendedApplicantEventCodeSimpleType.OTHER_ELIGIBILITY_CHANGE.value(),ReferralQualifyingEventCode.OTHER_ELIGIBILITY_CHANGE);
			put(ExtendedApplicantEventCodeSimpleType.LOSS_OF_MEC_PRESUMPTIVELY_VERIFIED.value(),ReferralQualifyingEventCode.LOSS_OF_MEC_PRESUMPTIVELY_VERIFIED);
			put(ReferralConstants.QEP_WITHOUT_QLE,ReferralConstants.QEP_WITHOUT_QLE);
		}
	};

	public static ApplicantDetails retrieveFromPayload(String externalApplicantId, ApplicationExtension applicationExtension) {
		ApplicantDetails applicantRet = null;
		if (applicationExtension != null && ReferralUtil.listSize(applicationExtension.getExtendedApplicant()) != ReferralConstants.NONE) {
			for (ApplicantDetails applicantDetails : applicationExtension.getExtendedApplicant()) {
				if (StringUtils.equalsIgnoreCase(externalApplicantId, applicantDetails.getIdentificationId())) {
					applicantRet = applicantDetails;
					break;
				}
			}
		}
		return applicantRet;
	}

	public static Date defaultApplicantReportDate() {
		return TSDateTime.getInstance().withTime(0, 0, 0, 0).toDate();
	}

	public static Date defaultApplicantEventDate() {
		return TSDateTime.getInstance().withTime(0, 0, 0, 0).toDate();
	}

	public static ApplicantEvent retrieveEvent(ApplicantDetails payloadApplicant, String value) {
		ApplicantEvent event = null;
		if (ReferralUtil.listSize(payloadApplicant.getApplicantEvents()) != ReferralConstants.NONE) {
			for (ApplicantEvent applicantEvent : payloadApplicant.getApplicantEvents()) {
				if (StringUtils.equalsIgnoreCase(value, applicantEvent.getCode())) {
					event = applicantEvent;
				}
			}
		}
		return event;
	}

	public static ApplicantEvent retrieveEvent(ApplicantDetails payloadApplicant, ExtendedApplicantEventCodeSimpleType[] eventCodes) {
		ApplicantEvent event = null;
		if (ReferralUtil.listSize(payloadApplicant.getApplicantEvents()) != ReferralConstants.NONE) {
			final int size = ReferralUtil.arraySize(eventCodes);
			outer: for (int i = 0; i < size; i++) {
				for (ApplicantEvent applicantEvent : payloadApplicant.getApplicantEvents()) {
					if (StringUtils.equalsIgnoreCase(eventCodes[i].value(), applicantEvent.getCode())) {
						event = applicantEvent;
						break outer;
					}
				}
			}

		}
		return event;
	}

	public static List<ApplicantEvent> retrieveEvents(ApplicantDetails payloadApplicant, ExtendedApplicantEventCodeSimpleType[] eventCodes, ExtendedApplicantNonQHPCodeSimpleType[] qhpEventCodes) {
		List<ApplicantEvent> event = new ArrayList<ApplicantEvent>();
		if (ReferralUtil.listSize(payloadApplicant.getApplicantEventsNonQHP()) != ReferralConstants.NONE) {
			final int size = ReferralUtil.arraySize(qhpEventCodes);
			for (int i = 0; i < size; i++) {
				for (ApplicantEvent applicantEvent : payloadApplicant.getApplicantEventsNonQHP()) {
					if (StringUtils.equalsIgnoreCase(qhpEventCodes[i].value(), applicantEvent.getCode())) {
						event.add(applicantEvent);
						break;
					}
				}
			}
		}
		if (ReferralUtil.listSize(payloadApplicant.getApplicantEvents()) != ReferralConstants.NONE) {
			final int size = ReferralUtil.arraySize(eventCodes);
			for (int i = 0; i < size; i++) {
				for (ApplicantEvent applicantEvent : payloadApplicant.getApplicantEvents()) {
					if (StringUtils.equalsIgnoreCase(eventCodes[i].value(), applicantEvent.getCode())) {
						event.add(applicantEvent);
						break;
					}
				}
			}
		}
		return event;
	}

	public static List<ApplicantEvent> retrieveEvents(ApplicantDetails payloadApplicant) {
		List<ApplicantEvent> event = new ArrayList<ApplicantEvent>();
		if (ReferralUtil.listSize(payloadApplicant.getApplicantEvents()) != ReferralConstants.NONE) {
			for (ApplicantEvent applicantEvent : payloadApplicant.getApplicantEvents()) {
				event.add(applicantEvent);
			}
		}
		return event;
	}

	public static Map<String, ApplicantEvent> populateApplicantEventFromExtension(List<SsapApplicant> ssapApplicants, ApplicationExtension applicationExtension) {
		Map<String, ApplicantEvent> data = new HashMap<String, ApplicantEvent>();
		ApplicantDetails applicantFromPayload;
		ApplicantEvent changeEvent;
		ExtendedApplicantEventCodeSimpleType[] eventCodes;
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			eventCodes = STATUS_EVENT_MAP.get(ssapApplicant.getStatus());
			if (eventCodes == null) {
				continue;
			}
			applicantFromPayload = ApplicationExtensionEventUtil.retrieveFromPayload(ssapApplicant.getExternalApplicantId(), applicationExtension);
			if (null != applicantFromPayload) {
				changeEvent = ApplicationExtensionEventUtil.retrieveEvent(applicantFromPayload, eventCodes);
				if (null != changeEvent) {
					data.put(ssapApplicant.getApplicantGuid(), changeEvent);
				}
			}

		}
		return data;
	}

	public static Map<String, List<ApplicantEvent>> populateApplicantEventsFromExtension(List<SsapApplicant> ssapApplicants, ApplicationExtension applicationExtension, SsapApplicant primaryApplicant) {
		Map<String, List<ApplicantEvent>> data = new HashMap<String, List<ApplicantEvent>>();
		ApplicantDetails applicantFromPayload;
		ExtendedApplicantEventCodeSimpleType[] eventCodes;
		ExtendedApplicantNonQHPCodeSimpleType[] qhpEventCodes;
		final ApplicantDetails primaryFromPayload = ApplicationExtensionEventUtil.retrieveFromPayload(primaryApplicant.getExternalApplicantId(), applicationExtension);
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			eventCodes = STATUS_EVENT_MAP.get(ssapApplicant.getStatus());
			qhpEventCodes = STATUS_QHP_EVENT_MAP.get(ssapApplicant.getStatus());
			if (eventCodes == null && qhpEventCodes == null) {
				continue;
			}
			if (eventToPickFromPrimary(ssapApplicant.getStatus())) {
				applicantFromPayload = primaryFromPayload;
			} else {
				applicantFromPayload = ApplicationExtensionEventUtil.retrieveFromPayload(ssapApplicant.getExternalApplicantId(), applicationExtension);
			}
			if (null != applicantFromPayload) {
				data.put(ssapApplicant.getApplicantGuid(), ApplicationExtensionEventUtil.retrieveEvents(applicantFromPayload, eventCodes, qhpEventCodes));
			}
		}
		return data;
	}
	
	public static Map<String, Boolean> populateInvalidApplicantEventsFromExtension(List<SsapApplicant> ssapApplicants, ApplicationExtension applicationExtension, SsapApplicant primaryApplicant) {
		Map<String, Boolean> applicantWithInvalidExtensionEvents = new HashMap<String, Boolean>();
		ApplicantDetails applicantFromPayload;
		ExtendedApplicantEventCodeSimpleType[] eventCodes;
		ExtendedApplicantNonQHPCodeSimpleType[] qhpEventCodes;
		final ApplicantDetails primaryFromPayload = ApplicationExtensionEventUtil.retrieveFromPayload(primaryApplicant.getExternalApplicantId(), applicationExtension);
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			eventCodes = STATUS_EVENT_MAP.get(ssapApplicant.getStatus());
			qhpEventCodes = STATUS_QHP_EVENT_MAP.get(ssapApplicant.getStatus());
			if (eventCodes == null && qhpEventCodes == null) {
				continue;
			}
			if (eventToPickFromPrimary(ssapApplicant.getStatus())) {
				applicantFromPayload = primaryFromPayload;
			} else {
				applicantFromPayload = ApplicationExtensionEventUtil.retrieveFromPayload(ssapApplicant.getExternalApplicantId(), applicationExtension);
			}
			if (null != applicantFromPayload) {
				List<ApplicantEvent> retrievedValidEvents = ApplicationExtensionEventUtil.retrieveEvents(applicantFromPayload, eventCodes, qhpEventCodes);
				
				if (ReferralUtil.listSize(retrievedValidEvents) != ReferralConstants.NONE){
					applicantWithInvalidExtensionEvents.put(ssapApplicant.getApplicantGuid(), false);
				} else {
					applicantWithInvalidExtensionEvents.put(ssapApplicant.getApplicantGuid(), true);
				}
			}
		}
		return applicantWithInvalidExtensionEvents;
	}

	private static boolean eventToPickFromPrimary(String status) {
		final ApplicantStatusEnum applicantStatus = ApplicantStatusEnum.fromValue(status);
		return ((applicantStatus == ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT) || (applicantStatus == ApplicantStatusEnum.UPDATED_ZIP_COUNTY));
	}

	/**
	 * get the maximum non qhp event for the applicant when dhw sends more than 1 event based on event date. Usually this should not happen. Handled for future if it happens
	 */

	public static Map<String, ApplicantEvent> populateMaxApplicantEventforApplicant(List<SsapApplicant> currentSsapApplicants, ApplicationExtension currentApplicationExtension, List<String> enrolledApplicantsGuid) {

		Map<String, ApplicantEvent> data = new HashMap<String, ApplicantEvent>();
		ApplicantDetails applicantExtensionFromPayload;
		List<ApplicantEvent> nonQhpEvents;
		List<ApplicantEvent> simpleEvents;
		for (SsapApplicant ssapApplicant : currentSsapApplicants) {
			if (enrolledApplicantsGuid.contains(ssapApplicant.getApplicantGuid())) {
				applicantExtensionFromPayload = ApplicationExtensionEventUtil.retrieveFromPayload(ssapApplicant.getExternalApplicantId(), currentApplicationExtension);
				if (null != applicantExtensionFromPayload) {
					nonQhpEvents = applicantExtensionFromPayload.getApplicantEventsNonQHP();
					if (ReferralUtil.listSize(nonQhpEvents) != ReferralConstants.NONE) {
						final ApplicantEvent nonQhpEvent = fetchMaxNonQhpEventBasedonDate(nonQhpEvents);
						data.put(ssapApplicant.getApplicantGuid(), nonQhpEvent);
					} else {
						// fetch the simple event and not qhp code 
						simpleEvents = applicantExtensionFromPayload.getApplicantEvents();
						final ApplicantEvent simpleEvent = fetchMaxNonQhpEventBasedonDate(simpleEvents);
						data.put(ssapApplicant.getApplicantGuid(), simpleEvent);
					}
				}
			}
		}
		return data;

	}

	public static ApplicantEvent fetchMaxNonQhpEventBasedonDate(List<ApplicantEvent> nonQhpEvents) {

		Collections.sort(nonQhpEvents, Collections.reverseOrder((event1, event2) -> event1.getEventDate().compareTo(event2.getEventDate())));

		return nonQhpEvents.get(0);
	}

	public static ApplicantEvent fetchMinNonQhpEvent(List<ApplicantEvent> nonQhpEvents) {

		Collections.sort(nonQhpEvents, (event1, event2) -> event1.getEventDate().compareTo(event2.getEventDate()));

		return nonQhpEvents.get(0);
	}

	public static ApplicantEvent calculateMaxEventDateApplicantEvent(Map<String, ApplicantEvent> applicantEventMap) {
		final List<ApplicantEvent> applicantEvents = new ArrayList<ApplicantEvent>(applicantEventMap.values());
		if (ReferralUtil.listSize(applicantEvents) != ReferralConstants.NONE) {
			return ApplicationExtensionEventUtil.fetchMaxNonQhpEventBasedonDate(applicantEvents);
		} else {
			return defaultApplicationEvent();
		}
	}

	private static ApplicantEvent defaultApplicationEvent() {
		ApplicantEvent applicationEvent = new ApplicantEvent();
		applicationEvent.setEventDate(new TSDate());
		return applicationEvent;
	}

	public static ApplicantEvent calculateMinEventDateApplicantEvent(Map<String, ApplicantEvent> applicantEventMap) {
		final List<ApplicantEvent> applicantEvents = new ArrayList<ApplicantEvent>(applicantEventMap.values());
		if (ReferralUtil.listSize(applicantEvents) != ReferralConstants.NONE) {
			return ApplicationExtensionEventUtil.fetchMinNonQhpEvent(applicantEvents);
		} else {
			return defaultApplicationEvent();
		}
	}

	private static int calculateGracePeriod() {
		int gracePeriod = 9;
		final String gracePeriodstr = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
		if (StringUtils.isNumeric(gracePeriodstr)) {
			gracePeriod = Integer.parseInt(gracePeriodstr);
		}
		return gracePeriod;
	}

	public static boolean checkDenial(SsapApplicant ssapApplicant, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		boolean blnIsDenial = false;
		final ApplicantStatusEnum status = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
		if (ReferralConstants.SEP_DENIAL_APPLICANT_STATUS.contains(status)) {
			final List<ApplicantEvent> events = applicantExtensionEvents.get(ssapApplicant.getApplicantGuid());
			if (ReferralUtil.listSize(events) != ReferralConstants.NONE) {
				final ApplicantEvent event = events.get(0);
				if (checkEventisForDenial(event)) {
					blnIsDenial = checkDenialEventDifference(event);
					if (blnIsDenial) {
						return blnIsDenial;
					}
				}

				blnIsDenial = isFutureEventDenied(event);
			}
		}
		return blnIsDenial;
	}

	public static boolean isFutureEventDenied(ApplicantEvent event) {
		boolean denied = false;
		int gracePeriod = calculateGracePeriod();
		if (checkEventisForFuture(event) && checkDenialFutureEvent(event, gracePeriod)) {
			denied = true;
		}
		return denied;
	}

	private static boolean checkDenialFutureEvent(ApplicantEvent event, int gracePeriod) {
		return futureEventDenied(event.getEventDate(), gracePeriod);
	}

	public static boolean futureEventDenied(Date eventDate, int gracePeriod) {
		final DateTime currentDate = new DateTime(new TSDate());
		DateTime eventCheckDate = new DateTime(eventDate.getTime());
		final DateTime eventCheckCurrentDate = currentDate.plusDays(gracePeriod + (int) SIXTY_DAYS);
		// System.out.println("eventCheckCurrentDate : " +eventCheckCurrentDate);
		// System.out.println("eventCheckDate : " + eventCheckDate);
		return eventCheckCurrentDate.isBefore(eventCheckDate);
	}

	public static boolean checkEventisForFuture(ApplicantEvent event) {
		return FUTURE_DATED_LCE_EVENTS.contains(event.getCode());
	}

	public static ApplicantEvent applicantEventForAutoQEP(SsapApplicant primaryApplicant, ApplicationExtension applicationExtension) {
		ApplicantEvent applicantEvent = null;
		final ApplicantDetails primaryFromPayload = ApplicationExtensionEventUtil.retrieveFromPayload(primaryApplicant.getExternalApplicantId(), applicationExtension);
		if (primaryFromPayload != null) {
			final List<ApplicantEvent> changeEventList = ApplicationExtensionEventUtil.retrieveEvents(primaryFromPayload);

			if (ReferralUtil.collectionSize(changeEventList) == ReferralConstants.ONE && checkEventisForQEP(changeEventList.get(0))) {
				applicantEvent = changeEventList.get(0);
			}
		}
		return applicantEvent;
	}

	public static boolean checkSingleEventDenial(SsapApplicant ssapApplicant, Map<String, ApplicantEvent> applicantExtensionEvents) {
		boolean blnIsDenial = false;
		final ApplicantStatusEnum status = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
		if (ReferralConstants.SEP_DENIAL_APPLICANT_STATUS.contains(status)) {
			final ApplicantEvent event = applicantExtensionEvents.get(ssapApplicant.getApplicantGuid());
			if (null != event) {
				if (checkEventisForDenial(event)) {
					return checkDenialEventDifference(event);
				}
			}

		}
		return blnIsDenial;
	}

	private static boolean checkDenialEventDifference(ApplicantEvent event) {
		return (Math.abs(ReferralUtil.dayDifference(event.getEventDate(), event.getReportDate())) > (SIXTY_DAYS + calculateGracePeriod()));
	}

	private static boolean checkEventisForDenial(ApplicantEvent event) {
		return APPLICANT_EXTENSION_DENIAL_EVENT.contains(event.getCode());
	}

	private static boolean checkEventisForQEP(ApplicantEvent event) {
		return QEP_EVENT_NAME_MAP.containsKey(event.getCode());
	}

}
