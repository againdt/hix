package com.getinsured.hix.batch.enrollment.skip;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component("houseHolds")
public class HouseHoldIds {
	List<String> houseHoldIdList=null;
	boolean isGeneratedXMLValid = Boolean.TRUE;

	public HouseHoldIds() {
		houseHoldIdList= new ArrayList<String>();
		isGeneratedXMLValid = Boolean.TRUE;
	}
	
	
	
	/**
	 * @return the isGeneratedXMLValid
	 */
	public boolean isGeneratedXMLValid() {
		return isGeneratedXMLValid;
	}



	/**
	 * @param isGeneratedXMLValid the isGeneratedXMLValid to set
	 */
	public synchronized void setGeneratedXMLValid(boolean isGeneratedXMLValid) {
		this.isGeneratedXMLValid = isGeneratedXMLValid;
	}



	public synchronized void addAllToHouseHoldIdList(List<String> householdIdlist){
		this.houseHoldIdList.addAll( householdIdlist);
	}
	
	public synchronized void addToHouseHoldIdList(String householdId){
		this.houseHoldIdList.add( householdId);
	}
	
	public List<String> getHouseHoldIdList(){
		return houseHoldIdList;
	}
	public synchronized void clearHouseHoldIdList(){
		this.houseHoldIdList.clear();
	}
}
