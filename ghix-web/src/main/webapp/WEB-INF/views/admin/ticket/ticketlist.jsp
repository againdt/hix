<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page isELIgnored="false"%>
<jsp:include page="archiveTicket.jsp">
	<jsp:param value="" name=""/>
</jsp:include>
<jsp:include page="cancelTicket.jsp">
	<jsp:param value="" name=""/>
</jsp:include>
<jsp:include page="reassignTicket.jsp">
	<jsp:param value="" name=""/>
</jsp:include>
<jsp:include page="completeTicket.jsp">
	<jsp:param name="ticketId" value="${param.ticketId}"/>
</jsp:include>
<jsp:include page="completeVerifyTicket.jsp">
	<jsp:param name="ticketId" value="${param.ticketId}"/>
</jsp:include>
<jsp:include page="reopenTicket.jsp">
	<jsp:param value="" name=""/>
</jsp:include>
<jsp:include page="restartTicket.jsp">
	<jsp:param value="" name=""/>
</jsp:include>
<%-- <jsp:include page="verifyDocument.jsp">
	<jsp:param value="" name=""/>
</jsp:include> --%>

<!-- <script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script> -->
<script type="text/javascript">

var showAndHideRowNum = 0;

$(function(){

    $(".customMultiDD").multiselect();
    
});

</script>
<style type="text/css">
#searchFilterDiv  .multiSelectControlContainer-ticket-search button {
 margin-top:0px;
 background:none;
 width:225px !important;
 white-space:nowrap;
}

#searchFilterDiv  .multiSelectControlContainer button {
 margin-top:0px;
 background:none;
 width:150px !important;
 white-space:nowrap;
}
.ui-multiselect-menu {
 width:250px !important;
}

.ui-widget-header a {
 font-weight:normal;
 
}
	.TDsubject{
		max-width:175px;
		word-wrap: break-word;
	}
	.ui-autocomplete{
	height: 125px !important;
	overflow-y: scroll;
	overflow-x: hidden;width: 220px !important;
	font-family:Helvetica,Arial,sans-serif;
	font-size:13px;
	}
[name=' ticketId '] {
    display: none;
}
</style>

<div class="gutter10">
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/ticketmgmt/ticket/createpage" />"><spring:message  code="label.createTicket"/></a></li>
            <li><spring:message  code="lbl.viewTickets"/></li>
        </ul><!--page-breadcrumb ends-->
		<h1><div>
				<span id="tktCountHeader"><spring:message  code="header.tickets"/> 
					<small>
						<span id="lblTicketCount"></span> 
						<spring:message  code="header.totalTickets"/><span class="hide" id="lblTicketCounts"><spring:message  code="header.text_s"/></span>
					</small>
				</span>
				
				<!-- HIX-37405 -->
<!-- 				<div class="controls pull-right btn btn-primary btn-small margin10-r nmhide mshide" style="white-space:nowrap;"> -->
<!-- 					<div class="dropdown"> -->
<!-- 						<a href="/page.html" data-target="#" data-toggle="dropdown" role="button" id="dLabel" class="dropdown-toggle"> <i class="icon-cog"></i> <b class="caret"></b></a> -->
<!-- 						<ul id="action_0" aria-labelledby="dLabel" role="menu" class="dropdown-menu pull-right"> -->
<%-- 							<li><a href="<c:url value="/ticketmgmt/ticket/createpage" />"><i class="icon-pencil"></i><spring:message  code="link.addNew"/></a></li> --%>
<!-- 								<li><a href="#" class=""><i class="icon-exchange"></i>Close</a></li> -->
<%-- 							<li class="navmenu disabled"><a href="#" class=""><i class=" icon-refresh "></i> <spring:message  code="link.archive"/></a></li> --%>
<!-- 						</ul> -->
<!-- 					</div>  -->
<!-- 				</div> -->

				<div class="controls pull-right" id="createTicketDiv">
	<%-- 				<c:if test="${stateExchenge != null && stateExchenge != 'SHOP' }"> --%>
	<%-- 					<a href="#" class="btn btn-small btn-primary pull-right"  id='fff' onclick="javascript:showUploadDiv()" data-toggle="modal"><spring:message  code="label.addDocumentType"/></a> --%>
	<%--     		    </c:if> --%>
					<c:if test="${addTicketPermission != null && addTicketPermission == true }">	
						<a href="<c:url value="/ticketmgmt/ticket/createpage"/>" class="btn btn-primary pull-right" id="addNewTicket" name="addNewTicket" ><spring:message  code="label.addNewTicket"/></a>
					</c:if>					
				</div>
			</div>
		</h1>
	</div>
		
    <div class="row-fluid " id="searchFilterParentDiv">
    	<div class="span12 gray" id="rightpanel">
    		<div class="header ">
    			<h4 class="pull-left" id="pageTitle"> Search Filters</h4>
    			<a href="#" class="btn btn-small pull-right"  id='btnShowHide' onclick="javascript:showHideFilters()" data-toggle="modal"> + </a>
              <a class="pull-left" style="font-size: 14px; color: red; padding-left:20px; padding-top:10px;" id="tkmErrorDiv">
					<c:if test="${errorMsg != null && errorMsg != ''}">
						<p><c:out value="${errorMsg}"></c:out><p/>
						<c:remove var="errorMsg"/>
					</c:if>
				</a>
		</div>
        
        <!-- class="form-vertical gutter10 lightgray" -->
        <div id="searchFilterDiv" class="toggle hide row-fluid" >
        <form method="POST" action="<c:url value="/ticketmgmt/ticket/list" />" id="ticketSearch" name="ticketSearch" class="form-vertical gutter10" novalidate="novalidate">
        <df:csrfToken/>
        <input type ="hidden" name="isSimplifiedView" id="isSimplifiedView" value="${isSimplifiedView}" >
        	<c:choose>
    			<c:when test="${isSimplifiedView}">
                   <jsp:include page="ticketListSimplifiedFilter.jsp"></jsp:include>
                </c:when>
          		<c:otherwise>
        		   <jsp:include page="ticketListFilter.jsp"></jsp:include>
          		</c:otherwise>
            </c:choose>
        </form>
        </div>
     </div>
  </div>
         <div id="ticketListNgApp" class="row-fluid" ng-controller="ticketListAppController">	
         	
	        <div class="span12">
            <form  method="POST">
            <df:csrfToken/>
            <h4 ng-if="tickets.length == 0" class="alert alert-info">
                                <spring:message code='label.norecords' />
            </h4>
            <div ng-if="tickets.length > 0">
            	<table class="table table-striped margin10-t">
             			<thead>
             				<tr class="header">
             					<th ng-cloak ng-repeat="column in columns" ng-class="{sortable:column.sortable}" scope="col" style="width:{{column.width}}">
             						<span ng-cloak ng-switch on="column.sortable">
             							<span ng-switch-when="true"><a href="#" ng-click="dosort(column)" ng-cloak>{{column.title}}</a></span>
             							<span ng-switch-default ng-cloak>{{column.title}}</span>
             						</span>
             					</th>
             					<th><button class="btn btn-small" ng-click="exportToExcel()" title="Export to Excel"><i class="icon-download-alt"></i></button></th>
             				</tr>
             			</thead>
             			<tbody>
             				<tr ng-cloak ng-repeat="ticket in tickets" class="row_{{ticket.id}}" >

             					<td ng-cloak ng-repeat="column in columns">
             						<span ng-cloak ng-if="ticket[column.name] != undefined" ng-switch on="column.name">
             							<span ng-cloak ng-switch-when="subject">
             								<span>
             									<a href="<c:url value="/ticketmgmt/ticket/ticketdetail/" />{{ticket.encryptedId}}" ng-cloak>{{ticket.number}}</a>
             									<a href="<c:url value="/ticketmgmt/ticket/ticketdetail/" />{{ticket.encryptedId}}" ng-cloak>{{ticket.subject}}</a>
             								</span>
             							</span>
             							<span ng-cloak ng-switch-default>{{ticket[column.name]}}</span>
             						</span>
             					</td>
             					<input type="hidden" class="jsRow_{{ticket.id}}" id="ticketListEncodedId-{{ticket.encryptedId}}"/>
	             					<td class="jsRow_{{ticket.id}}" style="white-space:nowrap" id="ticketTaskStatusId-{{ticket.id}}">
	             						<div class="controls">
	             							<div class="dropdown">
	             								<a href="#" data-target="#" data-toggle="dropdown" role="button" id="dLabel" class="dropdown-toggle"> <i class="icon-cog"></i> <b class="caret"></b>
												<span aria-hidden="true" class="hide"> Dropdown Menu. Press enter to open it and tab through its options</span></a>
												<ul role="menu" class="dropdown-menu pull-right">
													<li class="navmenu" ng-if="ticket.allowView">
														<a href="<c:url value="/ticketmgmt/ticket/ticketdetail/" />{{ticket.encryptedId}}" class=""><i class="icon-eye-open"></i><spring:message code="text_tkt.viewDetails"/></a>
													</li>
	                                                <li class="navmenu" ng-if="ticket.allowEdit">
	                                                    <a href="{{urls.edit}}{{ticket.encryptedId}}" class=""><i class="icon-pencil"></i><spring:message code="option_tktTask.editSummary"/></a>
	                                                </li>
	                                                <li class="navmenu" ng-if="ticket.allowCancel">
	                                                    <a href="javascript:cancelFunction('{{ticket.encryptedId}}')" class=""><i class="icon-remove"></i><spring:message code="lbl.cancelTicket"/></a>
	                                                </li>
	                                                <li class="navmenu" ng-if="ticket.allowMarkAsComplete">
	                                                    <a href="javascript:completeTicket('{{ticket.encryptedId}}', '{{ticket.ticketCategory}}')" class=""><i class="icon-refresh"></i><spring:message code="option_tktTask.markAsComplete"/></a>
	                                                </li>
	                                                <li class="navmenu" ng-if="ticket.allowClaim">
	                                                    <a href="javascript:completeTicket('{{ticket.encryptedId}}', '{{ticket.ticketCategory}}')" class=""><i class="icon-refresh"></i><spring:message code="option_tktTask.claim"/></a>
	                                                </li>
	                                                <li class="navmenu" ng-if="ticket.allowReassign">
	                                                    <a href="javascript:reassignFunction('{{ticket.encryptedId}}')" class=""><i class="icon-exchange"></i><spring:message code="option_tktTask.reAssignTkt"/></a>
	                                                    <input type="hidden" id="encAssignTicketid_{{ticket.encryptedId}}" value="{{ticket.encryptedId}}" />
	                                                </li>
	                                                <li class="navmenu" ng-if="ticket.allowAssign">
	                                                    <a href="javascript:assignFunction('{{ticket.encryptedId}}')"  class=""><i class="icon-exchange"></i>Assign Ticket</a>
	                                                    <input type="hidden" id="encAssignTicketid_{{ticket.encryptedId}}" value="{{ticket.encryptedId}}" />
	                                                </li>
	                                                <li class="navmenu" ng-if="ticket.allowReopen">
	                                                    <a href="javascript:reopenTicketFunction('{{ticket.encryptedId}}')" class=""><i class="icon-folder-open"></i>Reopen</a>
	                                                </li>
	                                                <li class="navmenu" ng-if="ticket.allowRestart">
	                                                    <a href="javascript:restartTicketFunction('{{ticket.encryptedId}}')" class=""><i class="icon-refresh"></i>Restart</a>
	                                                </li>
	                                                <li class="navmenu" ng-if="ticket.allowArchive">
	                                                    <a href="javascript:archiveFunction({{ticket.id}},'{{ticket.encryptedId}}')" class=""><i class="icon-archive"></i><spring:message code="option_tktTask.archiveTkt"/></a>
	                                                </li>
												</ul>
	             							</div>
	             						</div>
	             					</td>
             				</tr>
             			</tbody>
             	</table>
             	<ui-pagination size="pageSize" total="recordCount" page="currentPage" callback="'goToPage'" ></ui-pagination>
            </div>
            
        </form>
		</div>
		<div style="display:none;" class="span1">
                {{isArchivedAllowed=${isArchivedAllowed}}}
                {{isSimplifiedViewStr='${isSimplifiedView}'}}
                {{isAssignAllowed=${isAssignAllowed}}}
         		{{urls.view='<c:url value="/ticketmgmt/ticket/ticketdetail/" />'}}
         		{{urls.edit='<c:url value="/ticketmgmt/ticket/edit/"/>'}}
                {{urls.service='<c:url value="/ticketmgmt/ticket/list/"/>'}}
        </div>
    </div><!--  end of row-fluid -->
</div>

<script type="text/javascript" src="<c:url value="/resources/js/cap/ticketSearchList.js" />"></script>

<script type="text/javascript">
    /* pre populate data (HIX-57281)*/
    var sessionStorageVarName = "ticketListSearchVarsV1";
    /* pre populate data (HIX-57281)*/
    

var availableUsers = new Array();
function showHideFilters() {
	$( "#searchFilterDiv" ).slideToggle( "slow", function() {
	    // Animation complete.
		if ($('#searchFilterDiv').css('display') == 'none') {
			$("#btnShowHide").text("+");
		} else {
			$("#btnShowHide").text("-");
		}
	 });
}

function showUploadDiv()
{
	$('#tkmErrorDiv').html("");
	$('#uploadDialog').modal('show');
	checkDocumentType();
	
	}
	
function isInvalidCSRFToken(xhr){
		var rv = false;
		if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
			alert($('Session is invalid').text());
		rv = true;
		}
		return rv;
	}	
	
function findtktArray(queueArray, filterArray){
	
	$('#ticketQueues li').each(function(){
		if($(this).is('.selected.tktqueue')){
			var selectedQueue = $(this).attr("name");
			queueArray.push(selectedQueue);
		}	
		if($(this).is('.selected.tktArray')){
			var selectedFilter = $(this).attr("name");
			filterArray.push(selectedFilter);
		} 				
	});
}	

var finalurl = "";

function populateValues(prePopulate) {
    
    var subject = $("#ticketSubject").val();
    var comments = $("#ticketComments").val();
    var reqbyName = $("#requestedByName").val();
    var reqby = $("#requestedBy").val();
    var priority;
    var assigneeName = $("#assignee").val();
    var assigneeId = $("#assigneeId").val();
    var tktNo = $("#ticketNumber").val();
    var submittedBefore = $("#submittedBefore").val();
    var submittedAfter = $("#submittedAfter").val();
    var ticketStatus = $("#ticketStatus").val();
    var taskStatus = $("#taskStatus").val();
    var requester = $( "#requester" ).val();
    var roleId = $( "#roleId" ).val();
    var userRole = $( "#userRole" ).val();
    var archiveFlagValue = ($("#archiveFlagValue:checked").length > 0)? true:false;
    var queue;
    var tktStatus;
    var multipleTicketStatus = [];
    var multipleQueue = [];
    var multiplePriority = [];
    var multipleTicketStatusTxt=[];
  if (document.getElementById("ticketStatus") != null) {
        if (document.getElementById("ticketStatus").tagName === 'SELECT') {
            $('#ticketStatus option[selected]').each(function (i, selected) {
                multipleTicketStatus[i] = $(selected).val();
                multipleTicketStatusTxt.push($(selected).val());
            });
        } else if (document.getElementById("ticketStatus").tagName === 'INPUT') {
            tktStatus = $("#ticketStatus").val();
        };

    }; 

    if(getQueryString().ticketStatus != undefined)
    {
        ticketStatus=getQueryString().ticketStatus;
    };

    

    if (document.getElementById("ticketQueues").tagName === 'SELECT') {
        $('#ticketQueues :selected').each(function (i, selected) {
            multipleQueue[i] = $(selected).val();
        });
        queue = multipleQueue.join(",");
    } else if (document.getElementById("ticketQueues").tagName === 'INPUT') {
        queue = $("#ticketQueues").val();
    }
	if (document.getElementById("ticketPriority") != null ){
	    if (document.getElementById("ticketPriority").tagName === 'SELECT') {
	        $('#ticketPriority :selected').each(function (i, selected) {
	        	multiplePriority[i] = $(selected).val();
	        });
	        priority = multiplePriority.join(",");
	    } else if (document.getElementById("ticketQueues").tagName === 'INPUT') {
	    	priority = $("#ticketPriority").val();
	    }
	};

    if(getQueryString().ticketPriority != undefined)
    {
        priority=getQueryString().ticketPriority;
    };
    

    
    var assignedToMe = false;
    var unClaimedTicket = false;
    if (document.getElementById("isSimplifiedView").value === 'true') {
        var queueArray = [];
        var filterArray = [];
        findtktArray(queueArray, filterArray);
        queue = queueArray.join(",");

        assignedToMe = filterArray.indexOf("assignedToMe") != -1;
        unClaimedTicket = filterArray.indexOf("unClaimedTicket") != -1;
        archiveFlagValue = filterArray.indexOf("archiveFlagValue") != -1; 
    }

    var criteria = {
        "ticketSubject": subject,
        "ticketComments": comments,
        "requestedByName": reqbyName,
        "requestedBy": reqby,
        "ticketQueues": queue,
        "ticketPriority": priority,
        "assignee": assigneeName,
        "assigneeId": assigneeId,
        "ticketNumber": tktNo,
        "submittedBefore": submittedBefore,
        "submittedAfter": submittedAfter,
        "ticketStatus": ticketStatus,
        "taskStatus": taskStatus,
        "assignedToMe": assignedToMe,
        "unClaimedTicket": unClaimedTicket,
	"roleId" : roleId,
	 "requester" : requester,
	 "userRole" : userRole,
	 "archiveFlagValue": archiveFlagValue
    };
    if(multipleTicketStatusTxt.length >0)
    {
        tktStatus=multipleTicketStatusTxt.join(",");
    };

    if(getQueryString().period != undefined)
    {
        criteria.period=getQueryString().period;
    };
    
    /* pre populate data (HIX-57281)*/
    if (typeof (Storage) !== "undefined") {
        var dataToSave = {};
        $.extend(dataToSave, criteria);
        dataToSave.isSimplifiedView = $("#isSimplifiedView").val();
        dataToSave.ticketQueues = criteria.ticketQueues;
        dataToSave.taskStatus = $('#taskStatus :selected').html();
        dataToSave.ticketPriority = criteria.ticketPriority;
        dataToSave.ticketStatus = ticketStatus;
        dataToSave.archiveFlagValue = archiveFlagValue;
        if (document.getElementById("isSimplifiedView").value === 'true') {
            dataToSave.ticketQueues=queue;
        };
        dataToSave.prePopulate = prePopulate;
        sessionStorage[sessionStorageVarName] = JSON.stringify(dataToSave);
    };

    return criteria;
    /* pre populate data (HIX-57281)*/
};
var GetParameterByName = function (url, name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};
var sortBy="";
var sortOrder="";
$("#ticketSearch").submit(function (e) {

    if($("#userRole").val() !="")
    {
        if($("#requester").val()=="")
        {
            return;
        };
    };

    $('#tkmErrorDiv').hide("slow");
    var criteria;
    if ($('#searchFilterParentDiv').css("display") == "none") {
        criteria = populateValues(false);
    } else {
        criteria = populateValues(true);
    };

    criteria.pageNumber = defPage;
    criteria.sortBy=sortBy;
    criteria.sortOrder=sortOrder;

    if (typeof (Storage) !== "undefined") {
        sessionStorage[sessionStorageVarName + "page"] = defPage;
    };

    criteria.${df:csrfTokenParameter()}='<df:csrfToken plainToken="true" />';

    var validateUrl = $(this).attr("action");
    
        $('body').prepend('<div class="Ajaxloader"></div>').css('overflow','hidden');
                $.ajax({
                     url : validateUrl,
                        type : "POST",
                        data : criteria,
                        success: function(response)
                        {
                                //$("#ticketResult").html("");
                                //$("#ticketResult").html(response);
                                //populateTktCount();

                                removeAjaxLoaderImage();

                                angular.element("#ticketListNgApp").scope().setData(response);
                                try{
                                    angular.element("#ticketListNgApp").scope().$apply();
                                }catch(e){};                                
                        },      
                        error: function(e)
                        {
                                alert("Failed to Fetch Tickets Data");
                                removeAjaxLoaderImage();
                        }
                });
                e.preventDefault();
                
		});

		
		function removeAjaxLoaderImage(){
		        $('.Ajaxloader').remove();
		        $('body').css('overflow','auto');       
		}
		
		
		function checkActionInUrl(url) {
		        
		        if((url.indexOf("?")) != -1) {
		                
		                if((url.indexOf("action=search")) != -1) {
		                        
		                        finalurl = url;
		                } else {
		                        
		                        finalurl = url + "&action=search";
		                }
		                
		        } else {
		                finalurl = url+"?action=search";
		        }
		        
		        
		}
		function goToPage(pageNumber)
        {
            defPage=pageNumber;
            if (typeof (Storage) !== "undefined") {
                sessionStorage[sessionStorageVarName + "page"] = pageNumber;
            };
            $("#ticketSearch").submit();

        };
		function goToPage__(url){
		        
		    checkActionInUrl(url);
		    $('#tkmErrorDiv').hide();
		    var pgNumber = GetParameterByName(url, "pageNumber");
		    if (typeof (Storage) !== "undefined") {
		        sessionStorage[sessionStorageVarName + "page"] = pgNumber;
		    };
		        
		        $('body').prepend('<div class="Ajaxloader"></div>').css('overflow','hidden');
		        $.ajax({
		                url : finalurl,
		                type : "GET",
		                
		                success: function(response)
		                {
		                        $("#ticketResult").html("");
		                        $("#ticketResult").html(response);
		                         populateTktCount();
		                         removeAjaxLoaderImage();
		                },      
		                error: function(e)
		                {
		                        alert("Failed to Fetch Tickets Data");
		                        removeAjaxLoaderImage();        
		                }
		        });
		}
		
		function  sorting_old(url){
		        
		        checkActionInUrl(url);
		        
		        $('body').prepend('<div class="Ajaxloader"></div>').css('overflow','hidden');
		    $.ajax({
		           url : finalurl,
		           type : "GET",
		           success: function(response)
		           {
		                  $("#ticketResult").html("");
		                  $("#ticketResult").html(response);
		                  
		                  populateTktCount();
		                  removeAjaxLoaderImage();
		           },     
		           error: function(e)
		           {
		                  alert("Failed to Fetch Tickets Data");
		                  removeAjaxLoaderImage();
		           }
		    });
		}
		
		
		function populateTktCount() {
		    
		        var tktCount =  $("#totTickets").val();
		        var countHeader = '<spring:message  code="header.tickets"/><small>&nbsp;' + tktCount + '&nbsp;<spring:message  code="header.totalTickets"/>';
		    
		    if(tktCount > 1) { 
		        
		        countHeader = countHeader + '<spring:message  code="header.text_s"/>';
		    }
		    countHeader = countHeader + '</small>';
		    
		    $("#tktCountHeader").html("");
		    
		    $("#tktCountHeader").html(countHeader);
		}



$('.date-picker').datepicker({
    autoclose: true,
    dateFormat: 'MM/DD/YYYY'
});
	
	var isSubmittedAfterDate = function(startDateStr, endDateStr) {
	    var inDate = new Date(startDateStr),
	        eDate = new Date(endDateStr);
	    if(inDate > eDate) {
	        return false;
	    }else{
	    	return true;
	    }
	
	};
	
	jQuery.validator.addMethod("isSubmittedAfterDate", function(value, element) {
	    return isSubmittedAfterDate($('#submittedAfter').val(), value);
	}, "<span> <em class='excl'>!</em><spring:message code='label.validateBeforeAndAfterDate' javaScriptEscape='true'/></span>");
	
	var validator = $("#ticketSearch")
	.validate(
			{
		rules : {
			'submittedAfter' : {date: true},
			'submittedBefore' : {date: true, isSubmittedAfterDate: true}
			
		},
		messages : {
			'submittedAfter' : { date:  "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>"},
			'submittedBefore' : { date:  "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>"}
		     
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			if($("#" + elementId + "_error").html() != null)
			{
				$("#" + elementId + "_error").html('');
			}
			error.appendTo($("#" + elementId + "_error"));
			$("#" + elementId + "_error")
					.attr('class', 'error');
		} 
	});

$('#submittedBefore').keypress(function(e){
  	if (window.event) { var charCode = window.event.keyCode; }
    else if (e) { var charCode = e.which; }
    else { return true; }
    if (charCode > 31 && (charCode > 48 || charCode < 57)) { return false; }
    return true;
  
});

$('#submittedAfter').keypress(function(e){
  	if (window.event) { var charCode = window.event.keyCode; }
    else if (e) { var charCode = e.which; }
    else { return true; }
    if (charCode > 31 && (charCode > 48 || charCode < 57)) { return false; }
    return true;
  
});

$(document).ready(function()  {
	
    var prePopulate = true;

    if("referrer" in document)
    {
        if(document.referrer.indexOf("login") > 0)
        {
            if (typeof (Storage) !== "undefined") {
                sessionStorage[sessionStorageVarName + "page"] = 1;
                sessionStorage[sessionStorageVarName] = undefined;
            };
        };
    };

    if ((window.location.href.indexOf("period") > -1 ) || (document.referrer.indexOf("ticket/pendingreport") > 0 || getQueryString().fromAgeingReports=="true")) {
        prePopulate = false;
        $('#searchFilterParentDiv').hide();
        $('#createTicketDiv').hide();
        if (typeof (Storage) !== "undefined") {
            sessionStorage[sessionStorageVarName + "page"] = defPage;
        };
        var criteria = populateValues(prePopulate);
    };
    $("input").on("change", function () {
        defPage = 1;
    });
    $("select").on("change", function () {
        defPage = 1;
    });
	
	
    /*var selectedTask = '${searchCriteria.ticketStatus}';
    selectedTask==""?selectedTask='["Any"]':selectedTask;
    var selectedTaskArray = JSON.parse(selectedTask);
    
    var multiselectobject=document.getElementById("ticketStatus")
    if(multiselectobject != null){
    	for (var i=0; i<multiselectobject.length; i++){
            for(var j=0; j< selectedTaskArray.length; j++){
                    if(selectedTaskArray[j] == multiselectobject.options[i].value){
                            multiselectobject.options[i].selected="selected";
                    }
            }
        }	
    }*/

    if (String(window.location.href).indexOf("resetflag=") > 0) {
    	if (typeof (Storage) !== "undefined") {
            sessionStorage[sessionStorageVarName + "page"] = 1;
            sessionStorage[sessionStorageVarName] = undefined;
        };
    };
    
    /* pre populate data (HIX-57281)*/    
    if (String(window.location.href).indexOf("persist=") > 0) {
        prePopulate = true;
    }else{
        if (document.referrer.indexOf("ticket/ticketlist") > 0) {
            prePopulate = false;
            var criteria = populateValues(true);
        };
    };
    if (typeof (Storage) !== "undefined") {
        if ((sessionStorage[sessionStorageVarName] != undefined && sessionStorage[sessionStorageVarName] != "undefined") && prePopulate==true) {
            var criteria = sessionStorage[sessionStorageVarName];
            criteria = JSON.parse(criteria);

            showHideFilters();

            $("#ticketSubject").val(criteria.ticketSubject);
            $("#ticketComments").val(criteria.ticketComments);
            $("#requestedByName").val(criteria.requestedByName);
            $("#requestedBy").val(criteria.requestedBy);

            $("#assignee").val(criteria.assignee);
            $("#assigneeId").val(criteria.assigneeId);

            $("#ticketNumber").val(criteria.ticketNumber);
            $("#submittedBefore").val(criteria.submittedBefore);
            $("#submittedAfter").val(criteria.submittedAfter);
            $("#isSimplifiedView").val(criteria.isSimplifiedView);
            $("#archiveFlagValue").attr("checked",criteria.archiveFlagValue);

            $("#userRole").val(criteria.userRole);
            
            if($("#userRole").val()!="") {
            	$("#requester").val(criteria.requester).removeAttr("disabled");
            }
            
            if(criteria.roleId !="")
            {
                $("#roleId").val(criteria.roleId);
                $("#requester").val(criteria.requester).removeAttr("disabled");
            };

            $(".date-picker").datepicker("update");

            $("#ticketQueues>option").each(function (idx, opt) {
                var $opt = $(opt);
                if ( $opt.attr('value').length > 0){
                	if (criteria.ticketQueues.indexOf($opt.attr('value'))  >= 0 ) {
                        $opt.attr("selected", "selected");
                    };
                }
                
            });
            $("#taskStatus>option").each(function (idx, opt) {
                var $opt = $(opt);
                if ($opt.html() == criteria.taskStatus) {
                    $opt.attr("selected", "selected");
                };
            });
            $("#ticketPriority>option").each(function (idx, opt) {
                var $opt = $(opt);
                if ( $opt.attr('value').length > 0){
                	if (criteria.ticketPriority.indexOf($opt.attr('value'))  >= 0 ) {
                        $opt.attr("selected", "selected");
                    };
                }
                
            });
            $("#ticketStatus option[selected]").removeAttr("selected");
            $("#ticketStatus>option").each(function (idx, opt) {
                var $opt = $(opt);
                var statusArr=String(criteria.ticketStatus).split(",");
                statusArr.forEach(function(vval){
                    if(vval==$opt.html())
                    {
                        $opt.attr("selected", "selected");
                    };
                });              
            });
            if (criteria.prePopulate == false) {
                $('#searchFilterParentDiv').hide();
                $('#createTicketDiv').hide();
            };

            if(criteria.isSimplifiedView=="true")
            {
                var queue=criteria.ticketQueues;
                var qArray=String(queue).split(",");
                var mainH=$("#ticketQueues");
                $.each(qArray,function(idx,obj){
                    mainH.find("[name='"+obj+"']").addClass("selected");
                });

                for(var nKey in criteria)
                {
                    if(criteria[nKey]==true)
                    {
                        mainH.find("[name='"+nKey+"']").addClass("selected");
                    };
                };
            };
            
            var criteria = populateValues(criteria.prePopulate);
            defPage = sessionStorage[sessionStorageVarName+"page"]
           
        }else{
            showHideFilters();
        };
    }else{
    	showHideFilters();
    };
     $("#ticketSearch").submit();
    /* pre populate data (HIX-57281)*/
    $(".customMultiDD").multiselect('refresh');

});
var defPage = 1;

$(function() {	
	
	// add multiple select / deselect functionality
    $("#selectall").click(function () {
          $('.case').attr('checked', this.checked);
    });
 
    // if all checkbox are selected, check the selectall checkbox
    // and viceversa
    $(".case").click(function(){
 
        if($(".case").length == $(".case:checked").length) {
            $("#selectall").attr("checked", "checked");
        } else {
            $("#selectall").removeAttr("checked");
        }
 
    });
});


function getTicketTaskData(id)
{
	var hideId = "hide"+ id;
	//alert("displaying"+hideId);
	
	var validateUrl = '<c:url value="tickettasklist"></c:url>';
	if(showAndHideRowNum != id)
	{
		hideRow("hide"+showAndHideRowNum);
		showAndHideRowNum = id;
	$.ajax({
		url : validateUrl,
		type : "POST",
		data : {
			${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
			ticketId : id
		},
		success: function(response,xhr)
		{
			if(isInvalidCSRFToken(xhr))
				return;
			$('#hideId').attr('style', 'display:block');
			$("#excelDataTable"+id).empty();
			buildHtmlTable(response, id);
			showAndHideRow(hideId);
   			},
  			error: function(e)
  			{
   				alert("Failed to Fetch Data");
   			}
	});
	}
	else
	{
		hideRow(hideId);
		showAndHideRowNum = 0;
	}
}

function buildHtmlTable(response, id) 
{
	var newresponse = JSON.parse(response); 
	var myList = newresponse;

	 //var myList = response;
    if(myList != '')
    {
    	//alert("MyList :"+myList);
	 var columns = addAllColumnHeaders(myList, id);
    //alert(columns);
        for (var i = 0 ; i < myList.length ; i++) {
            var row$ = $('<tr/>');
            for (var colIndex = 0 ; colIndex < columns.length ; colIndex++) {
                var cellValue = myList[i][columns[colIndex]];
    
                if (cellValue == null) { cellValue = ""; }
    
                row$.append($('<td/>').html(cellValue));
            }
            $("#excelDataTable"+id).append(row$);
        }
    }
    else
    {
    	//alert(response);
    	var row$ = $('<tr/>');
    	var message = '<h4 class="alert alert-info">Currently there are no task available for this ticket </h4>';
    	
    	row$.append($('<td/>').html(message));
    	$("#excelDataTable"+id).append(row$);
    }
}

function showAndHideRow(id) 
{
	
	var hideId = id;
	
	if( document.getElementById(hideId).style.display=='none')
	{
		document.getElementById(hideId).style.display = '';
	}
	else
	{
		document.getElementById(hideId).style.display = 'none';
	}
}
function hideRow(id)
{
	
	if(id != 'hide0')
	{
		document.getElementById(id).style.display = 'none';
	}
}


function addAllColumnHeaders(myList, id)
{
    var columnSet = [];
    var headerTr$ = $('<tr/>');

    for (var i = 0 ; i < myList.length ; i++) {
        var rowHash = myList[i];
        for (var key in rowHash) {
            /* alert(key); */
            if ($.inArray(key, columnSet) == -1){
                columnSet.push(key);
                headerTr$.append($('<th/>').html(key));
            }
        }
    }
    $("#excelDataTable"+id).append(headerTr$);

    return columnSet;
}
	
	var empFilter = "";

	var availableAssignee = new Array();
	var availableSubjects = new Array();
	var availableTicketNumbers = new Array();
	var availableRequesters = new Array();
	

	$('.info').tooltip();

	function fetchAssigneeList() {
		if(availableAssignee.length == 0) {
		
		var validateUrl = '<c:url value="fetchassigneelist"></c:url>';
		var assigneeName = $("#assignee").val();
        
		$.ajax({
			url : validateUrl,
			type : "POST",
			data : {
				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				assigneeName : assigneeName
			},
			success : function(response,xhr) {
					if(isInvalidCSRFToken(xhr))
							return;
				autoCompleteAssignee(response);
			},
			error : function(e) {
				alert("Failed to search Assignee.");
			},
		});
	  }	else {
		  
		  autoCompleteBox();
	  }

	}
	
	function autoCompleteAssignee(userListJSON) 
	{
		userListJSON = JSON.parse(userListJSON);
		var index = 0;
		for ( var key in userListJSON) {
			availableAssignee[index] = {
				label : userListJSON[key],
				idx : key
			};
			index++;
		}
		
		autoCompleteBox();
	}

	function autoCompleteBox() {
		$("#assignee").autocomplete({
			source : availableAssignee,
			select: function(event, ui) { AutoCompleteSelectHandler(event, ui)}
		});
	}
	$('#assignee').bind('focusout',function(){
		var element = getItemByLabel(availableAssignee, $(this).val())
		if(element != null){
			$("#assigneeId").val(element.idx);
		}else{
			$("#assigneeId").val(0);
		}	
		
	});

	function getItemByLabel(anArray, label) {
	    for (var i = 0; i < anArray.length; i += 1) {
	        if (anArray[i].label === label) {
	            return anArray[i];
	        }
	    }
	}
	
	function AutoCompleteSelectHandler(event, ui)
	  {        
	      $("#assigneeId").val(ui.item.idx);
	  }
	
	function notifyCancelResponse(status, ticketid) {
		if(status == true) {
			var rowId = '#ticketListEncodedId-'+ticketid;
            var scope=angular.element(rowId).scope();
            scope.ticket.status="Canceled";
            scope.statusChanged(scope.ticket);
            try{scope.$apply();}catch(e){};
            
		} else {
			alert("Ticket cancellation failed");
		}
	}
	function cancelFunction(ticketid)
	{
		$('#tkmErrorDiv').html("");
		document.getElementById("ticketId").value = ticketid;
		$("#confirmCancelDlg").modal('show');
	}
	function archiveFunction(ticketid,encTicketid)
	{
		$('#tkmErrorDiv').html("");
		document.getElementById("ticketId").value = ticketid;
		document.getElementById("archiveEncTicketid").value = encTicketid
		$("#confirmArchiveDlg").modal('show');
		$("#crossCloseArchive").attr("disabled",false); 	
		$("#archiveCanelButton").attr("disabled",false); 	
		$("#archiveSubmit").attr("disabled",false); 	
		$('#tkmErrorDiv').html("");
	}
	function reassignFunction(ticketid)
	{
		document.getElementById("ticketId").value = ticketid;
		divpopulate();
	}
	function assignFunction(ticketid)
	{
		document.getElementById("ticketId").value = ticketid;
		divpopulate('Assign');
	}

	function completeTicket(ticketid, ticketCategory) {
		// Any Verify Document Ticket Type should go to getTicketActiveTaskDataVerifyDoc
        if (ticketCategory && ticketCategory === "Document Verification") {
			document.getElementById("ticketIdVerify").value = ticketid;
			document.getElementById("ticketId").value = ticketid;
			$('#validateVerifyComments').val('Y');
			getTicketActiveTaskDataVerifyDoc();
		} else {
            document.getElementById("ticketId").value = ticketid;
            $('#validateComments').val('N');
            getTicketActiveTaskData();
        }
    }
	
	
	function notifyClaimResponse() {
        var ticketid=document.getElementById("ticketId").value;
        var rowId = '#ticketListEncodedId-'+ticketid;
            var scope=angular.element(rowId).scope();
            scope.ticket.taskStatus="Claimed";
            scope.ticket.status="Open";
            scope.statusChanged(scope.ticket);
            try{scope.$apply();}catch(e){};
            $('.Ajaxloader').remove();
            $('body').css('overflow','auto');   
			/*var rowId = '#completeTicket-'+ticketId;
			$(rowId).find( "a" ).text("  Mark as complete").prepend('<i class="icon-refresh"></i>');
			
			var rowId1 = '#ticketTaskStatusId-'+ticketId;
			$(rowId1).text("Open");
			$('.Ajaxloader').remove();
			$('body').css('overflow','auto');	
			*/
	}
	
	function notifyCompleteResponse() {
	    //window.location.reload(true);
	    var turl=window.location.href;
	    var startT="";
	    if(String(turl).indexOf("?")>0)
	    {
	        startT="&";
	    }else{
	        startT="?";
	    };
	    if(String(turl).indexOf("persist=")>0)
	    {

	    }else{
	        turl +=startT + "persist=true";
	    };
	    turl=turl.replace("resetflag=","rsetflag=");
	    window.location.href=turl;
	}
	
	function fetchTicketSubjects(){
		
		if(availableSubjects.length == 0) {
			
			var validateUrl = '<c:url value="/ticketmgmt/ticket/ticketSubject"></c:url>';
			availableSubjects.length = 0;
			
				$.ajax({
					url : validateUrl,
					type : "POST",
					data : {
						${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />"
					},
					success: function(response,xhr){
					     if(isInvalidCSRFToken(xhr))	    				
						   return;
						if(response == "") {
							$("#roleId_error").val("Failed to search Subject.");
						} else {
		       				populateSubject(response);
						}
		       		},
		      		error: function(e){
		       			alert("Failed to search Subject.");
		       		},
				});
			
		} else {
			
			autoCompleteSubject();
		}
	}
	
	function populateSubject(subjectListJSON) {
		subjectListJSON = JSON.parse(subjectListJSON);
		var index=0;
		for (var key in subjectListJSON) {
			availableSubjects[index]={label:subjectListJSON[key], idx:key};
			index++;		
	    }
		
		autoCompleteSubject();
	}
	
	function autoCompleteSubject(){ $( "#ticketSubject" ).autocomplete({
	      source: availableSubjects,
	      select: function(event, ui) { AutoCompleteSelectHandler(event, ui)}
	    });
	function AutoCompleteSelectHandler(event, ui)
	  {               
	      $("#ticketSubject").val(ui.item.idx);
	  }
	}
	
	function fetchTicketNumbers(){
		
		if(availableTicketNumbers.length == 0) {
			var validateUrl =  '<c:url value="/ticketmgmt/ticket/ticketNumber"></c:url>';
			availableTicketNumbers.length = 0;
			
				$.ajax({
					url : validateUrl,
					type : "POST",
					data : {
						${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />"
					},
					success: function(response){
						if(response == "") {
							$("#roleId_error").val("Failed to search Ticket Number.");
						} else {
		       				populateTicketNumbers(response);
						}
		       		},
		      		error: function(e){
		       			alert("Failed to search Ticket Number");
		       		},
				});
			
		} else {
			
			autoCompleteTicketNumbers();
		}
	}
	
	function populateTicketNumbers(ticketNumbersListJSON) {
		ticketNumbersListJSON = JSON.parse(ticketNumbersListJSON);
		var index=0;
		for (var key in ticketNumbersListJSON) {
			availableTicketNumbers[index]={label:ticketNumbersListJSON[key], idx:key};
			index++;		
	    }
		
		autoCompleteTicketNumbers();
	}
	
	function autoCompleteTicketNumbers(){ $( "#ticketNumber" ).autocomplete({
	      source: availableTicketNumbers,
	      select: function(event, ui) { AutoCompleteSelectHandler(event, ui)}
	    });
	function AutoCompleteSelectHandler(event, ui)
	  {               
	      $("#ticketNumber").val(ui.item.idx);
	  }
	}
	
	function fetchRequesters(){
		
		var userName = $("#requestedByName").val();		
		if(userName.length >= 3) {
				
			availableRequesters = [];
			var validateUrl =  '<c:url value="/ticketmgmt/ticket/ticketRequesters"></c:url>';
			
            $('.requestedByNameLoader').show();
				$.ajax({
					url : validateUrl,
					type : "POST",
					data : {
						userText : userName,
						${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />"
					},
					success: function(response){
						if(response == "") {
							
							$("#roleId_error").val("Failed to search user.");
							
						} else {
		       				if(response == "") {
                        $('.requestedByNameLoader').hide();
                        $( "#requestedByName" ).attr( "placeholder","<spring:message  code="label.descriptionRequesterNotPresent"/>");
                            } else {
                                $('.requestedByNameLoader').hide();
                                populateRequesters1(response);
                                    autoCompleteRequesters();
                                    $("#requestedByName").data("autocomplete").search($("#requestedByName").val());
                                    }
						}
		       		},
		      		error: function(e){
		       			alert("Failed to search user.");
		       		},
				})

		} 
	}
	
	function populateRequesters1(ticketRequestersListJSON) {
		ticketRequestersListJSON = JSON.parse(ticketRequestersListJSON);
		var index=0;
		for (var key in ticketRequestersListJSON) {
			availableRequesters[index]={label:ticketRequestersListJSON[key], idx:key};
			index++;		
			
	    }
	}
	var isAutoCompleteRequesterCreated=false;
	function autoCompleteRequesters(){ 
		if(isAutoCompleteRequesterCreated){
			return;
		};
		$("#requestedByName").on("focus",function(){
			$("#requestedByName").data("autocomplete").search($("#requestedByName").val());			
		});
		isAutoCompleteRequesterCreated=true;
		$( "#requestedByName" ).autocomplete({
            minLength : 0,
	      source:  function (request, response) {
                        var term = $.ui.autocomplete.escapeRegex(request.term)
                            , startsWithMatcher = new RegExp("^" + term, "i")
                            , startsWith = $.grep(availableRequesters, function(value) {
                                return startsWithMatcher.test(value.label || value.value || value);
                            })
                            , containsMatcher = new RegExp(term, "i")
                            , contains = $.grep(availableRequesters, function (value) {
                                return $.inArray(value, startsWith) < 0 &&
                                    containsMatcher.test(value.label || value.value || value);
                            });

                            response(startsWith.concat(contains));
                 },
	      select: function(event, ui) { AutoCompleteSelectHandler(event, ui)}
	    });
	$('#requestedByName').bind('focusout',function(){	
		var element = getItemByLabel(availableRequesters, $(this).val())
		if(element != null){
			$("#requestedBy").val(element.idx);
		}else{
			$("#requestedBy").val(0);
		}	
		
	});
	function AutoCompleteSelectHandler(event, ui)
	  {               
	      $("#requestedBy").val(ui.item.idx);
	  }
	
	$("#requestedByName").data("autocomplete").search($("#requestedByName").val());
	}
	var availableCompanyList=[];
	function checkUserRole() {
		
		availableUsers.length =0;
		 availableCompanyList.length = 0;
		$('#companyDiv').attr('style', 'display:none');
		
		$("#company").empty();
		$("#company").val("");
 		
		
		$("#requester").val("");
		$("#roleId").val("");
		$("#requester").empty();
		$("#roleId_error").val("");
		
		$("#moduleId").val('');
		
		var validateUrl = "";
		var elt = document.getElementById("userRole");
		var userRole = elt.options[elt.selectedIndex].text;
		
		if(userRole == "Employer") {
		 $('#companyDiv').attr('style', 'display:block');
		   $( "#requester" ).attr( "placeholder","");
		   validateUrl = '<c:url value="/ticketmgmt/ticket/employers"></c:url>';
		   
		   getCompanyList(validateUrl, 'EMPLOYER');
		  } else if(userRole == "Issuer") {
		   $('#companyDiv').attr('style', 'display:block');
		   validateUrl = '<c:url value="/ticketmgmt/ticket/issuers" ></c:url>';
		   
		   getCompanyList(validateUrl, 'ISSUER');
		  } else if(userRole == "Employee") {
		   $( "#requester" ).attr( "placeholder","");
		   $('#companyDiv').attr('style', 'display:block');
		   validateUrl = '<c:url value="/ticketmgmt/ticket/employers" ></c:url>';
			
			getCompanyList(validateUrl, "EMPLOYER");
		} /* else { // commented because the requesters will be populated only when user types in three starting letters of the first or last name of the user
			getRequesters();
		} */
		if(userRole=="Select")
			{
		    $("#requester").prop("disabled",true).prop("required",false);
		    $("#requester_error").html("");
		    
			}else{
		    $("#requester").prop('disabled', false).prop("required",true);

		}
	}
	

	function getCompanyList(url, userRole) {
		$('.companyLoader').show();
		$( "#company" ).attr( "placeholder","");
		
		$.ajax({
			url : url,
			type : "POST",
			data : {
				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				roleName : userRole
			},
			success: function(response,xhr){
				if(isInvalidCSRFToken(xhr))	    				
					return;
				if(response == "") {
					$('.companyLoader').hide();
					$( "#company" ).attr( "placeholder","<spring:message  code="label.descriptionForCompanyNotPresent"/>");
					$("#company_error").val("Failed to search Company.");
				} else {
					$('.companyLoader').hide();
       				populateCompany(response);
				}
       		},
      		error: function(e){
       			alert("Failed to search Company.");
       		},
		});
	}
	
 function populateCompany(companyMapJSON) {
	 companyMapJSON = JSON.parse(companyMapJSON);
		var index=0;
		availableUsers.length =0;
		 availableCompanyList.length = 0; 
		 if(jQuery.isEmptyObject(companyMapJSON)){
				$( "#company" ).attr( "placeholder","<spring:message  code="label.descriptionForCompanyNotPresent"/>");
				
			}else{
				$( "#company" ).attr( "placeholder","<spring:message  code="label.descriptionForCompany"/>");
		for (var key in companyMapJSON) {
			availableCompanyList[index]={label:companyMapJSON[key], idx:key};
			index++;		
	    }
	   }
		 autoCompleteCompanyMap();  
	}
 function autoCompleteCompanyMap(){
	 $( "#company" ).focus(function() {
	    	$(this).data("autocomplete").search($(this).val());
 	}).autocomplete({
			minLength : 0,
			source: function (request, response) {
		            var term = $.ui.autocomplete.escapeRegex(request.term)
		                , startsWithMatcher = new RegExp("^" + term, "i")
		                , startsWith = $.grep(availableCompanyList, function(value) {
		                    return startsWithMatcher.test(value.label || value.value || value);
		                })
		                , containsMatcher = new RegExp(term, "i")
		                , contains = $.grep(availableCompanyList, function (value) {
		                    return $.inArray(value, startsWith) < 0 &&
		                        containsMatcher.test(value.label || value.value || value);
		                });

         			response(startsWith.concat(contains));
    		 },
	     	 select: function(event, ui) { AutoCompleteSelectHandler(event, ui); },
		  	 change: function( event, ui ) {if(ui.item==null) $("#moduleId").val('');}
	   	});
	 function AutoCompleteSelectHandler(event, ui) {               
		 $("#moduleId").val(ui.item.idx);
		 // getRequesters(); //// commented because the requesters will be populated only when user types in three starting letters of the first or last name of the user
		 $("#company_error").html('');
	  	}
    }
 
 
 
 function getCreatedFor(){
	  if($("#requester").val().length>=3){
		 //$("#requester").empty();
		$("#roleId_error").val("");
		
		var validateUrl = '<c:url value="/ticketmgmt/ticket/createdFor"></c:url>';
		var userRole = $("#userRole").val();
		
		var elt = document.getElementById("userRole");
		var userRoleName = elt.options[elt.selectedIndex].text;
			var userName = $("#requester").val();
		availableUsers.length = 0;
		$( "#requester" ).attr( "placeholder","");
		if(userRole!=''){
			$('.requesterLoader').show();
			$.ajax({
				url : validateUrl,
				type : "POST",
				data : {
					${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
					roleId : userRole, // IND ROLE ID
					moduleName : userRoleName, // 'INDIVIDUAL'
					userText : userName // 'USer text for name'
				},
				success: function(response,xhr){
					if(isInvalidCSRFToken(xhr))	    				
						return;
						 //$("#requester").empty();
					if(response == "") {
						$('.requesterLoader').hide();
						$( "#requester" ).attr( "placeholder","<spring:message  code="label.descriptionRequesterNotPresent"/>");
						$("#roleId_error").val("Failed to search Created For.");
					} else {
						$('.requesterLoader').hide();
	       				populateRequesters(response);
		       				autoCompleteBox1();
		       				$("#requester").data("autocomplete").search($("#requester").val());
					}
	       		},
	      		error: function(e){
					$('.requesterLoader').hide();
	       			alert("Failed to search Created For.");
	       		},
			});
		}
	  }
	}
	

	function populateRequesters(userListJSON){
		userListJSON = JSON.parse(userListJSON);
		var index=0;
		if(jQuery.isEmptyObject(userListJSON)){
			$( "#requester" ).attr( "placeholder","<spring:message  code="label.descriptionRequesterNotPresent"/>");
			
		}else{
			$( "#requester" ).attr( "placeholder","<spring:message  code="label.descriptionRequester"/>");
			for (var key in userListJSON) {
				availableUsers[index]={label:userListJSON[key], idx:key};
				index++;		
		    }
		}
	}
	
	function isInvalidCSRFToken(xhr){
		var rv = false;
		if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
		  alert($('Session is invalid').text());
		  rv = true;
		 }
		return rv;
	}


	var autoCompleteBoxCreate=false;
		function autoCompleteBox1() { 
			if(autoCompleteBoxCreate){return;}
			autoCompleteBoxCreate=true;
			$("#requester").on("focus",function(){
				$("#requester").data("autocomplete").search($("#requester").val());			
			});
	    	$("#requester").autocomplete({
				minLength : 0,
				source: function (request, response) {
			            var term = $.ui.autocomplete.escapeRegex(request.term)
			                , startsWithMatcher = new RegExp("^" + term, "i")
			                , startsWith = $.grep(availableUsers, function(value) {
			                    return startsWithMatcher.test(value.label || value.value || value);
			                })
			                , containsMatcher = new RegExp(term, "i")
			                , contains = $.grep(availableUsers, function (value) {
			                    return $.inArray(value, startsWith) < 0 &&
			                        containsMatcher.test(value.label || value.value || value);
			                });

	            			response(startsWith.concat(contains));
	       		 },
		     	 select: function(event, ui) { AutoCompleteSelectHandler(event, ui); },
			  	 change: function( event, ui ) {if(ui.item==null) $("#roleId").val('');}
		   	});
		
			function AutoCompleteSelectHandler(event, ui) {               
		      $("#roleId").val(ui.item.idx);
		      
		       $("#requester_error").html('');
		       $("#roleId_error").html('');
		  	}
		}
		
		function dataExportToExcel(){
			var csrf = $("#csrftoken").val();
			$("#csrftoken").val('');
			var data = $("#ticketSearch :input[value!='']").serialize()
			$("#csrftoken").val(csrf);
			var archiveFlagValue = ($("#archiveFlagValue:checked").length > 0)? true:false;
			$(".modalMessage").hide();
			$(".ajaxTextLoader").show();
			window.location.assign(
					"${pageContext.request.contextPath}"+'/ticketmgmt/ticket/listreport?csvData='+data+'&archiveFlagValue='+archiveFlagValue
		   );
		   	$(".modalMessage").show();
			$(".ajaxTextLoader").hide();
			$("#exportToExcelModal").modal('hide');
		}

function notifyReassignResponse(status, ticketId,assignee,queueName,action) {
	if(action == true){
        var encTicketId  =  document.getElementById("encAssignTicketid_"+ticketId).value;
        var redirectUrl ='<c:url value="/ticketmgmt/ticket/ticketdetail/'+encTicketId+'"></c:url>';
        window.location.href = redirectUrl;
    }
    else{
        if(status == true) {
            var rowId = '#ticketQueueName-'+ticketId;
            $(rowId).text(queueName);
        } else {
            alert("Ticket Reassign failed");
        }   
    }
}		

var getQueryString=function()
{
    var getQuery=window.location.search;
    getQuery=String(getQuery).replace(/\?/ig,"");
    var queryArray=getQuery.split("&");

    var retObj={
        period:'${searchCriteria.period}'==''?undefined:'${searchCriteria.period}',
        ticketPriority:'${searchCriteria.ticketPriority}'==''?undefined:'${searchCriteria.ticketPriority}',
    };

    queryArray.forEach(function(obj){
        var valSplit=String(obj).split("=");
        retObj[valSplit[0]]=unescape(valSplit[1]);
    });

    if(retObj.ticketPriority !=undefined)
    {
        retObj.ticketPriority = String(retObj.ticketPriority).replace("[","");
        retObj.ticketPriority = String(retObj.ticketPriority).replace("]","");
    };

    if(retObj.fromAgeingReports=="true")
    {
        retObj.ticketStatus="Open,UnClaimed";
    };
    if(retObj.fromPendingReport=="true")
    {
        retObj.ticketStatus="Open";
    }

    return retObj;

};

</script>

<div id="exportToExcelModal" class="modal" style="display:none">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	</div>
	 <div class="modal-body">
      <h3 class="modalMessage">Export search results to CSV file?</h3>
      <h4 class="modalMessage">(Only the first 1000 rows will be exported)</h4>
	  <h3 class="ajaxTextLoader hide">Export in process ...  </h3>

		  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="btnExportData" onclick="dataExportToExcel()">Export</button>
      </div>
</div>
