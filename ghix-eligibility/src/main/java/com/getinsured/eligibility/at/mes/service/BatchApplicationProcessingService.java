package com.getinsured.eligibility.at.mes.service;

public interface BatchApplicationProcessingService {

	void executeQueuedApplication();
}