<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<script>
	$(document).ready(function() {
		var scope = angular.element(document.getElementById("customGrouping")).scope();
		if (scope) {
			scope.$apply(function() {
				scope['customGrouping'] = {title: '<spring:message code="indportal.customGrouping.customGrouping" javaScriptEscape="true"/>'};
			});
		}
	});
</script>
<script type="text/ng-template" id="customGrouping">
		<div class="ps-detail well ng-scope" ng-init="showCustomGrouping()">
			<ul class="dashboard_tabs clearfix" data-tabgroup="first-tab-group">
				<li> <a href="javascript:void(0)" tabindex="0" role="button" ng-keyup="checkToggleGroupsKeyEvents($event, 'Health')" ng-click="toggleGroups('Health')" id='tab-0' ng-class="{'active': showHealthGroups}" ><spring:message code="indportal.customGrouping.shopHealthPlans" /></a></li>
				<li> <a href="javascript:void(0)" tabindex="0" role="button" ng-keyup="checkToggleGroupsKeyEvents($event, 'Dental')" ng-click="toggleGroups('Dental')" id='tab-1' ng-class="{'active': showDentalGroups}" ><spring:message code="indportal.customGrouping.shopDentalPlans" /></a></li>
			</ul>

	<div class="well step group-header" id="healthGroups" ng-show="showHealthGroups">
		<div ng-show="isAnyGroupEnrolled" id="aid_enrolled_grp">
            <div class="margin20-b" ng-if="isRenewalApplication && isHealthRenewalGroupAvailable">
				<spring:message code="indportal.customGrouping.renewChangeGroup1" /> <a style="text-decoration: underline;" href="javascript:void(0)" ng-click="showAppNotToConsider('HEALTH')"><spring:message code="indportal.customGrouping.renewChangeGroup2" /></a>
			</div>
			<div class="ps-detail__group">
				<div class="ps-detail__header ps-detail__header--group">
					<span ng-if="enrolledMembersCount==1 && !allGroupsRenewal" id="aid_enrolledHeader"><spring:message code="indportal.customGrouping.enrolled" /> ( {{enrolledMembersCount}} <spring:message code="indportal.customGrouping.member" />)</span> 
					<span ng-if="enrolledMembersCount>1 && !allGroupsRenewal" id="aid_enrolledHeader"><spring:message code="indportal.customGrouping.enrolled" /> ( {{enrolledMembersCount}} <spring:message code="indportal.customGrouping.members" />)</span>
					<span ng-if="enrolledMembersCount==1 && allGroupsRenewal" id="aid_enrolledHeader">{{ previousCoverageYear }} Enrollments ( {{enrolledMembersCount}} <spring:message code="indportal.customGrouping.member" />)</span> 
					<span ng-if="enrolledMembersCount>1 && allGroupsRenewal" id="aid_enrolledHeader">{{ previousCoverageYear }} Enrollments ( {{enrolledMembersCount}} <spring:message code="indportal.customGrouping.members" />)</span>
					 
					<i id="health_toggle_icon" class="toggle icon-chevron-up" ng-keyup="checkSlideDivEvents($event, 'health')" ng-click="slideDiv('health')" tabindex="0"></i >
				</div>
				<div id="aid_health_enrolled_grps" class="toggle-div">
					<div class="well-step ps-detail__group-body">
						<div ng-if="healthGroups.length>0" ng-repeat="group in healthGroups" >
							<div ng-if="isAllEnrolled(group)" id="{{'aid_enrolled_grp_' + $index}}">
								<div class="row-fluid">
									<p ng-if="showAddAPTCMessage(group) || showAddStateSubsidyMessage(group)">
										<span ng-if="showAddAPTCMessage(group)">
										    <spring:message code="indportal.customGrouping.APTC" />
										    <span>{{formatAmount(group.maxAptc)}} <spring:message code="indportal.customGrouping.perMonth" /></span>
										</span>
								        <span ng-if="showAddStateSubsidyMessage(group)">
								            <span ng-if="showAddAPTCMessage(group)">
								                <spring:message code="indportal.customGrouping.stateSubsidyMsg1" />
								            </span>
								            <spring:message code="indportal.customGrouping.stateSubsidyMsg2" />
								            <span>{{formatAmount(group.maxStateSubsidy)}} <spring:message code="indportal.customGrouping.perMonth" /></span>
								        </span>
								        <span> <spring:message code="indportal.customGrouping.APTCMsg" /></span>
									</p>

									<div ng-if="!showAddAPTCMessage(group) && !showAddStateSubsidyMessage(group)">
										<div ng-if="(group.hasAPTCEligibleMembers || group.hasStateSubsidyEligibleMembers) && !group.isRenewal" >
											<span><spring:message code="indportal.customGrouping.APTCSuccess" /> </span>
											<span ng-if="group.electedAptc" id="{{'aid_elected_aptc_lbl_' + $index}}">
													<spring:message code="indportal.customGrouping.APTCMsg2" />
													<strong><span id="{{'aid_elected_aptc_value_' + $index}}"> {{ formatAmount(group.electedAptc) }} <spring:message code="indportal.customGrouping.perMonth" /></span ></strong>
													<span ng-if="group.hasAPTCEligibleMembers && group.hasStateSubsidyEligibleMembers && group.electedStateSubsidy" >
														<spring:message code="indportal.customGrouping.stateSubsidyMsg1" />
													</span>
											</span>

											<span ng-if="group.electedStateSubsidy" id="{{'aid_elected_ss_lbl_' + $index}}">
											    <spring:message code="indportal.customGrouping.stateSubsidyMsg2" />
                                                <strong>
                                                    <span id="{{'aid_elected_ss_value_' + $index}}"> {{ formatAmount(group.electedStateSubsidy) }} <spring:message code="indportal.customGrouping.perMonth" /></span >
                                                </strong>
											</span>
											<span ng-if="(group.electedAptc > 0 || group.electedStateSubsidy > 0)">
											<spring:message code="indportal.customGrouping.APTCMsg3" />
											</span>
										</div>

										<div ng-if="(group.hasAPTCEligibleMembers || group.hasStateSubsidyEligibleMembers) && group.isRenewal" >
											<span ng-if="group.maxAptc" id="{{'aid_max_aptc_lbl_' + $index}}">
													<spring:message code="indportal.customGrouping.APTCMsgRenewal" /> {{ coverageYear }}: 
													<strong><span id="{{'aid_max_aptc_value_' + $index}}"> {{ formatAmount(group.maxAptc) }} <spring:message code="indportal.customGrouping.perMonth" /></span ></strong>
													<span ng-if="group.hasAPTCEligibleMembers && group.hasStateSubsidyEligibleMembers && group.maxStateSubsidy" >
														<spring:message code="indportal.customGrouping.stateSubsidyMsg1" />
													</span>
											</span>
											<span ng-if="group.maxStateSubsidy" id="{{'aid_max_ss_lbl_' + $index}}">
											    <spring:message code="indportal.customGrouping.stateSubsidyMsgRenewal" /> {{ coverageYear }}: 
                                                <strong>
                                                    <span id="{{'aid_max_ss_value_' + $index}}"> {{ formatAmount(group.maxStateSubsidy) }} <spring:message code="indportal.customGrouping.perMonth" /></span >
                                                </strong>
											</span>
										</div>

										<div ng-if="!(group.hasAPTCEligibleMembers || group.hasStateSubsidyEligibleMembers) && !group.isRenewal">
										<span id="{{'aid_enrolled_success_' + $index}}"><spring:message code="indportal.customGrouping.APTCSuccess" /></span>
									    </div>

								    </div>
								</div>
								<div class="row-fluid">
									<div class="col-xs-12 col-sm-5 col-md-4 col-lg-4 gutter10">
										<div ng-repeat="member in group.memberList" class="member-spacing">
											<span ng-show="member.healthEnrollmentId !== undefined" >
												<input type="checkbox" class="ps-form__check-input groupCheck" id="{{member.id+group.id}}" checked style="display: none">
												<input type="checkbox" checked="" disabled="" ng-if="group.isRenewal">
												<i class="icon-ok icon-large enrolled" ng-if="!group.isRenewal"></i>
													<strong class="member-info">
														{{member.firstName}} <span ng-if="member.middleName !== undefined"> {{member.middleName}} </span> {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
													</strong>
												
											</span>
										</div>
									</div>

									<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8 gutter10">
										<div ng-if="group.groupCsr != null && group.groupCsr != 'CS1'" id="{{'aid_members_qualify_' + $index}}"><spring:message code="indportal.customGrouping.groupCsr" /></div>

										<%--<div ng-if="group.maxAptc > 0">Total Advance Premium Tax Credit for this group: <strong>$ {{group.maxAptc}} </strong></div>--%>
										<div class="margin10-tb">
											<div id="{{'aid_issuer_name' + $index}}"> {{group.issuerName}} </div>
											<div id="{{'aid_plan_name' + $index}}"> {{group.planName}} </div>
		<div ng-if="group.netPremium"><span id="{{'aid_net_premium' + $index}}" > {{formatAmount(group.netPremium)}} <spring:message code="indportal.customGrouping.perMonth" /></span></div>
										</div>
										<div>
											<div ng-show="group.validCoverageSDate && !group.isRenewal">
                                                <button class="btn btn-primary btn_res btn-small pull-left" id="{{'aid_btn_disenroll' + $index}}" ng-click="disEnrollPlan('Health', group.coverageDate, group.encryptedGroupEnrollmentId, true)"><spring:message code="indportal.customGrouping.disenroll" /></button>
											</div>
											<div ng-show="showCancelCoverageButton(group) && !group.isRenewal">
                                                <button class="btn btn-primary btn_res btn-small pull-left" style="width: max-content;" id="{{'aid_btn_cancel_coverage' + $index}}" ng-click="disEnrollPlan('Health', group.coverageDate, group.encryptedGroupEnrollmentId, false)"><spring:message code="indportal.customGrouping.canceCoverage" /></button>
											</div>
											<div ng-if="!group.appState.isBlank">
																								<button  class="btn btn_res btn-primary btn-small pull-right" id="{{'aid_btn_finalize_changeplan' + $index}}" ng-click="changePlan('HEALTH', group.id, group)">
																									<span ng-if="group.appState.isFinalizeEnrollment"><spring:message code="indportal.customGrouping.finalizeEnrollment" /></span>
																									<span ng-if="group.appState.isFinalizePlan"><spring:message code="indportal.customGrouping.finalizePlan" /></span>
																									<span ng-if="group.appState.isChangePlan"><spring:message code="indportal.customGrouping.changePlan" /></span>
																									<span ng-if="group.appState.isRenewOrChangePlan"><spring:message code="indportal.customGrouping.renewOrChangePlan" /></span>
																								</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div ng-if="showHealthGroups" style="margin-top: 10px;" id="aid_unenrolled_grps">
			<div ng-if="UnEnrolledhealthGroups.length > 0  && isAllMembersAddedToOtherGroups()">
				<div style="margin-top: 8px; margin-bottom: 8px;" id="aid_uneg_whom_are_you_shopping"><strong><spring:message code="indportal.customGrouping.whomYouAreShopping" /></strong></div>

				<div ng-show="stateCode !== 'MN' && hasAIMembers">
					<span style="margin-bottom: 15px" id="aid_uneg_mix_ai_an_member"> <spring:message code="indportal.customGrouping.aiAnMember1" /></span>
				</div>

				<div ng-show="stateCode !== 'MN' && !hasAIMembers">
					<span style="margin-bottom: 15px" id="aid_uneg_no_mix_ai_an_member"> <spring:message code="indportal.customGrouping.aiAnMember2" /></span>
				</div>

				<div ng-show="stateCode === 'MN' && hasAIMembers">
					<span style="margin-bottom: 15px" id="aid_uneg_no_mix_ai_an_member"> <spring:message code="indportal.customGrouping.aiAnMember2" /></span>
					<p><spring:message code="indportal.customGrouping.aiAnMember3" /></p>
					<p><spring:message code="indportal.customGrouping.aiAnMember4" /></p>
					<p><spring:message code="indportal.customGrouping.aiAnMember5" /></p>
				</div>
				<div ng-show="stateCode === 'MN' && !hasAIMembers">
						<span style="margin-bottom: 15px" id="aid_uneg_mix_ai_an_member"> <spring:message code="indportal.customGrouping.aiAnMember1" /></span>
				</div>

				<div style="margin-top: 10px;">
					<label class="ps-form__check-input" id="aid_uneg_shop_members" > <strong> <spring:message code="indportal.customGrouping.shopForThese" /></strong> </label>
				</div>
				<div ng-if="healthGroups.length>0" ng-repeat="group in UnEnrolledhealthGroups" style="font-size: 14px;">
					<div ng-if="isAnyMembersUnEnrolled(group) && !areAllMembersAddedToGroups(group)" id="{{ 'aid_unenrolled_grp_' + $index }}">
						<div class="flex-container" id="{{'aid_uneg_members_' + $index}}">
							<div class="flex-child-1 member-info-container">
								<div ng-if="group.memberList != null && group.memberList.length > 0" ng-repeat="member in group.memberList" class="member-spacing">
									<div  ng-show="member.healthEnrollmentId === undefined" class="ps-form__check" >
											<input type="checkbox" class="ps-form__check-input groupCheck" id="{{member.id+group.id}}" ng-check="member.isChecked" ng-model="member.isChecked" ng-change="optionToggled(member, group, UnEnrolledhealthGroups)" value="{{member}}" ng-disabled="member.enrollmentId !== null && member.enrollmentId !=='' && member.enrollmentId !=undefined ? true : false">
											<label for="{{member.id+group.id}}" class="ps-form__check-input">
												<strong class="member-info">
													{{member.firstName}} <span ng-if="member.middleName !== undefined"> {{member.middleName}} </span> {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
												</strong>
											</label>
									</div>
								</div>
							</div>

							<div class="flex-child-2">
								<div ng-if="group.groupCsr != null && group.groupCsr != 'CS1'">
								    <spring:message code="indportal.customGrouping.csrMsg" />
								</div>
								<div ng-if="group.maxAptc > 0">
								    <spring:message code="indportal.customGrouping.totalAPTC" />
								    <strong>
								        <span id="{{'aid_uneg_maxaptc_' + $index}}"> {{formatAmount(group.maxAptc)}} <spring:message code="indportal.customGrouping.perMonth" /></span>
								    </strong>
								</div>
								<div ng-if="group.maxStateSubsidy && group.maxStateSubsidy > 0">
                                    <spring:message code="indportal.customGrouping.totalStateSubsidy" />
                                    <strong>
                                        <span id="{{'aid_uneg_maxstatesubsidy_' + $index}}"> {{formatAmount(group.maxStateSubsidy)}} <spring:message code="indportal.customGrouping.perMonth" /></span>
                                    </strong>
                                </div>
								<div style="margin-top: 10px; margin-right: 10px;">
									<div id="{{'aid_uneg_group_' + $index}}" > {{group.issuerName}} </div>
									<div id="{{'aid_uneg_planname_' + $index}}"> {{group.planName}} </div>
									<div ng-if="group.netPremium">
										<span id="{{'aid_uneg_net_premium_' + $index}}"> {{formatAmount(group.netPremium)}} <spring:message code="indportal.customGrouping.perMonth" /></span>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="margin20-t">
					<div>
						<a href="javascript:void(0)" class="btn btn_res btn-primary pull-left" data-dismiss="modal" ng-click="goHome()"><spring:message code="indportal.portalhome.godashboard" javaScriptEscape="true"/></a>
					</div>
					<div ng-if="UnEnrolledhealthGroups.selectedMembersCount > 0">
						<a href="javascript:void(0)" class="btn btn_res btn-primary pull-right" id="aid_uneg_shop_for_members" ng-click="showHealthPlansButtonDisabled==true || (validateGroup() === true && enrollAfterCustomGrouping('HEALTH', UnEnrolledhealthGroups[0].id, healthGroups, false))" ><spring:message code="indportal.customGrouping.shopHealthPlans" />
							<span ng-if="UnEnrolledhealthGroups.selectedMembersCount > 1"> ({{UnEnrolledhealthGroups.selectedMembersCount}} <spring:message code="indportal.customGrouping.members2" />)</span>
							<span ng-if="UnEnrolledhealthGroups.selectedMembersCount === 1"> ({{UnEnrolledhealthGroups.selectedMembersCount}} <spring:message code="indportal.customGrouping.member" />)</span> </a>
					</div>

					<div ng-if="!UnEnrolledhealthGroups.selectedMembersCount || UnEnrolledhealthGroups.selectedMembersCount === 0">
						<a href="javascript:void(0)" class="btn btn_res btn-primary pull-right" id="aid_uneg_shop_with_existing_members" ng-click="showHealthPlansButtonDisabled==true || (validateGroup() === true && enrollAfterCustomGrouping('HEALTH', UnEnrolledhealthGroups[0].id, healthGroups, false))" disabled="true"><spring:message code="indportal.customGrouping.shopHealthPlans" /></a>
					</div>
				</div>
			</div>
		</div>

		<div ng-show="showHealthGroups && addMembersGroups && addMembersGroups.length > 0" style="margin-top: 21px; border: 1px solid lightgrey; border-radius: 5px;" class="" id="multiGrp">
			<div class="ps-detail__header ps-detail__header--group" style="background-color: rgb(240, 240, 240); border-bottom: none;">
				<spring:message code="indportal.customGrouping.selectOption" />
			</div>
			<div ng-if="stateCode === 'MN'">		
				<div>		
					<spring:message code="indportal.customGrouping.addMembers.warning1" />		
				</div>		
				<div>		
					<spring:message code="indportal.customGrouping.addMembers.warning2" />		
				</div>		
			</div>

			<div ng-repeat="groupSet in addMembersGroups" style="font-size: 14px;">

				<div ng-if="groupSet.AddedInEnrolledGroup" style="border: 1px solid lightgray; margin: 10px; padding: 10px; border-radius: 5px;" id="{{ 'aid_multi_enrolled_grp_' + $index}}">

						<div style="margin-top: 10px;">
							<label class="ps-form__check-input" id="{{ 'aid_mng_grp_add_' + $index}}" > <strong> <spring:message code="indportal.customGrouping.addMembers" /> </strong> </label>
						</div>
						<div style="margin-top: 10px;">
							<label class="ps-form__check-input" id="{{ 'aid_mngd_grp_' + $index}}" > {{getUnEnrolledMembers(groupSet.AddedInEnrolledGroup)}} <spring:message code="indportal.customGrouping.sameLevel" /></label>
						</div>

						<div class="flex-container" style="border: none; margin-left: 10px;">
							<div class="flex-child-1">
								<div ng-if="groupSet.AddedInEnrolledGroup.memberList != null && groupSet.AddedInEnrolledGroup.memberList.length > 0" ng-repeat="member in groupSet.AddedInEnrolledGroup.memberList" class="member-spacing">
									<div ng-show="member.healthEnrollmentId !== undefined" id="{{ 'aid_mngd_grp_members_enr_' + $parent.$index}}">
										<input type="checkbox" class="ps-form__check-input groupCheck" id="{{member.id+group.id}}" checked style="display: none">
										<input type="checkbox" checked="" disabled="" ng-if="groupSet.AddedInEnrolledGroup.isRenewal">
											<i class="icon-ok icon-large enrolled" ng-if="!groupSet.AddedInEnrolledGroup.isRenewal"></i>
											<strong class="member-info">
												{{member.firstName}} <span ng-if="member.middleName !== undefined"> {{member.middleName}} </span> {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
											</strong>
									</div>

									<div  ng-show="member.healthEnrollmentId === undefined" class="ps-form__check" id="{{ 'aid_mngd_grp_members_nen_' + $parent.$index}}">
											<input
												type="checkbox"
												ng-checked="applicationType !== 'SEP' && $parent.$parent.$index === 0 ? true: false"
												class="ps-form__check-input groupCheck" id="{{member.id+groupSet.AddedInEnrolledGroup.id}}"
												ng-model="member.isChecked"
												ng-change="optionToggled(member, groupSet.AddedInEnrolledGroup, groupSet.AddedInEnrolledGroup, 'SINGLE')"
												value="{{member}}"
												ng-disabled="member.enrollmentId !== null && member.enrollmentId !=='' && member.enrollmentId !=undefined ? true : false">
											<label class="ps-form__check-input">
												<strong class="member-info">
													{{member.firstName}} <span ng-if="member.middleName !== undefined"> {{member.middleName}} </span> {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
												</strong>
											</label>
									</div>
								</div>
							</div>

							<div class="flex-child-2">
								<div style="margin-top: 10px; margin-right: 10px;">
									<div id="{{ 'aid_mngd_issuer_name_' + $index}}"> {{groupSet.AddedInEnrolledGroup.issuerName}} </div>
									<div id="{{ 'aid_mngd_plan_name_' + $index}}"> {{groupSet.AddedInEnrolledGroup.planName}} </div>
									<div id="{{ 'aid_mngd_net_premium_' + $index}}" ng-if="groupSet.AddedInEnrolledGroup.netPremium">{{formatAmount(groupSet.AddedInEnrolledGroup.netPremium)}} <spring:message code="indportal.customGrouping.perMonth" /></div>
								</div>

								<div id="{{ 'aid_mngd_add_engrp_' + $index}}" ng-if="groupSet.AddedInEnrolledGroup.groupCsr != null && groupSet.AddedInEnrolledGroup.groupCsr != 'CS1'"><spring:message code="indportal.customGrouping.csrMsg" /></div>
		                        <div id="{{ 'aid_mngd_add_engrp_aptc' + $index}}" ng-if="groupSet.AddedInEnrolledGroup.maxAptc > 0">
		                            <spring:message code="indportal.customGrouping.totalAPTC" />
		                            <strong>{{ formatAmount(getMaxAPTC(groupSet.AddedInEnrolledGroup)) }} <spring:message code="indportal.customGrouping.perMonth" /></strong>
		                        </div>
		                        <div id="{{ 'aid_mngd_add_engrp_ss' + $index}}" ng-if="groupSet.AddedInEnrolledGroup.maxStateSubsidy > 0">
                                    <spring:message code="indportal.customGrouping.totalStateSubsidy" />
                                    <strong>{{ formatAmount(getMaxStateSubsidy(groupSet.AddedInEnrolledGroup)) }} <spring:message code="indportal.customGrouping.perMonth" /></strong>
                                </div>

							</div>

						</div>

						<div style="margin-left: 10px; display: flex; flex-direction:row-reverse;">
								<div ng-if="groupSet.AddedInEnrolledGroup.selectedMembersCount > 0">
									<a href="javascript:void(0)"  id="{{'aid_mngd_add_engrp_shop_' + $index}}" class="btn btn-primary" ng-click="(validateGroup() === true && addToExistingGroup('HEALTH', groupSet.AddedInEnrolledGroup.id, groupSet.AddedInEnrolledGroup, false))"><span ng-if="!groupSet.AddedInEnrolledGroup.isRenewal"><spring:message code="indportal.customGrouping.addToPlan" /></span>
										<span ng-if="groupSet.AddedInEnrolledGroup.isRenewal"><spring:message code="indportal.customGrouping.renewPlan" /></span>
										<span ng-if="groupSet.AddedInEnrolledGroup.selectedMembersCount > 1 && !groupSet.AddedInEnrolledGroup.isRenewal"> ( {{groupSet.AddedInEnrolledGroup.selectedMembersCount}} Members)</span> 
										<span ng-if="groupSet.AddedInEnrolledGroup.selectedMembersCount === 1 && !groupSet.AddedInEnrolledGroup.isRenewal"> ( {{groupSet.AddedInEnrolledGroup.selectedMembersCount}} Member)</span></a>								
								</div>

								<div ng-if="groupSet.AddedInEnrolledGroup.selectedMembersCount  === 0">
									<a href="javascript:void(0)" id="{{'aid_mngd_add_engrp_add_shop_' + $index}}" class="btn btn-primary" disabled="true">
										<span ng-if="!groupSet.AddedInEnrolledGroup.isRenewal"><spring:message code="indportal.customGrouping.addToPlan" /></span>
										<span ng-if="groupSet.AddedInEnrolledGroup.isRenewal"><spring:message code="indportal.customGrouping.renewPlan" /></span>
									</a>								
								</div>
						</div>

				</div>
			</div>

			<div style="display: flex; justify-content: center; margin: 10px;"> <span style="
			   height: 32px;
			   width: 32px;
			   font-size: 12px;
			   border: 2px solid #69696959;
			   border-radius: 21px;
			   background: lightgrey; display: flex; justify-content: center;flex-direction: column; text-align: center;"><spring:message code="indportal.customGrouping.or" /></span>
			</div>

			<div ng-repeat="groupSet in addMembersGroups" style="font-size: 14px;" id="{{ 'aid_un_mngd_' + $index}}">

				<div ng-if="canShowGroup(groupSet.enrollmentGroup,addMembersGroups, $index)" style="border: 1px solid lightgray; margin: 10px; padding: 10px; border-radius: 5px;">

						<div style="margin-top: 10px;">
							<label id="{{ 'aid_un_sho_health_plans_' + $index}}" class="ps-form__check-input"> <strong><spring:message code="indportal.customGrouping.shopNewHealthPlan" /></strong></label>
						</div>

						<div class="flex-container" style="border: none; margin-left: 10px;">
							<div class="flex-child-1">
								<div ng-if="groupSet.enrollmentGroup.memberList != null && groupSet.enrollmentGroup.memberList.length > 0" ng-repeat="member in groupSet.enrollmentGroup.memberList" class="member-spacing">

									<div  ng-show="member.healthEnrollmentId === undefined" class="ps-form__check" id="{{ 'aid_un_sho_member_' + $parent.$index}}" >
											<input type="checkbox" class="ps-form__check-input groupCheck" id="{{member.id+groupSet.enrollmentGroup.id}}" ng-model="member.isChecked" ng-change="optionToggled(member, groupSet.enrollmentGroup, groupSet.enrollmentGroup, 'SINGLE')" value="{{member}}" ng-disabled="member.enrollmentId !== null && member.enrollmentId !=='' && member.enrollmentId !=undefined ? true : false">
											<label class="ps-form__check-input">
												<strong class="member-info">
													{{member.firstName}} <span ng-if="member.middleName !== undefined"> {{member.middleName}} </span> {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
												</strong>
											</label>
									</div>
								</div>
							</div>

							<div class="flex-child-2">
								<div id="{{ 'aid_un_group_csr_' + $index}}" ng-if="groupSet.enrollmentGroup.groupCsr != null && groupSet.enrollmentGroup.groupCsr != 'CS1'"><spring:message code="indportal.customGrouping.csrMsg" /></div>
								<div id="{{ 'aid_un_max_aptc_' + $index}}" ng-if="groupSet.enrollmentGroup.maxAptc > 0">
								    <spring:message code="indportal.customGrouping.totalAPTC" />
								    <strong>{{ formatAmount(getMaxAPTC(groupSet.enrollmentGroup)) }} <spring:message code="indportal.customGrouping.perMonth" /></strong>
								</div>
								<div id="{{ 'aid_un_max_aptc_' + $index}}" ng-if="groupSet.enrollmentGroup.maxStateSubsidy > 0">
                                    <spring:message code="indportal.customGrouping.totalStateSubsidy" />
                                    <strong>{{ formatAmount(getMaxStateSubsidy(groupSet.enrollmentGroup)) }} <spring:message code="indportal.customGrouping.perMonth" /></strong>
                                </div>
								<div style="margin-top: 10px; margin-right: 10px;">
									<div id="{{ 'aid_un_issuer_name_' + $index}}" > {{groupSet.enrollmentGroup.issuerName}} </div>
									<div id="{{ 'aid_un_plan_name_' + $index}}"> {{groupSet.enrollmentGroup.planName}} </div>
									<div id="{{ 'aid_un_sho_net_premium_' + $index}}" ng-if="groupSet.enrollmentGroup.netPremium">{{ formatAmount(groupSet.enrollmentGroup.netPremium)}} <spring:message code="indportal.customGrouping.perMonth" /></div>
								</div>
							</div>
						</div>

						<div style="margin-left: 10px; display: flex; flex-direction:row-reverse;">
								<a href="javascript:void(0)" id="{{ 'aid_un_enroll_shop_btn_' + $index}}" class="btn btn-primary" ng-disabled="groupSet.enrollmentGroup.selectedMembersCount < 1" ng-click="showHealthPlansButtonDisabled==true || (validateGroup() === true && enrollAfterCustomGrouping('HEALTH', groupSet.enrollmentGroup.id, healthGroups, false))">
									<spring:message code="indportal.customGrouping.shopForHealthPlans" />
									<span ng-if="groupSet.enrollmentGroup.selectedMembersCount > 1"> ( {{groupSet.enrollmentGroup.selectedMembersCount}} Members)</span>
									<span ng-if="groupSet.enrollmentGroup.selectedMembersCount === 1"> ( {{groupSet.enrollmentGroup.selectedMembersCount}} Member)</span>
								</a>
						</div>


				</div>
		</div>

		</div>

		<div ng-if="healthGroups.length<=0">
			<p class="alert alert-info" id="aid_no_health_group"><spring:message code="indportal.customGrouping.noHealthGroup" /></p>
		</div>

	</div>

	<div class="well step group-header" id="dentalGroups_enr" ng-show="showDentalGroups && isAnyDentalEnrollmentsAvailable">
		<div class="margin20-b" ng-if="isDentalRenewalGroupAvailable">
				<spring:message code="indportal.customGrouping.dentalChangeGroup1" /> <a style="text-decoration: underline;" href="javascript:void(0)" ng-click="showAppNotToConsider('DENTAL')"><spring:message code="indportal.customGrouping.dentalChangeGroup2" /></a>
			</div>
		<div class="ps-detail__header ps-detail__header--group" style="background-color: rgb(240, 240, 240); border-bottom: none;">
			<span ng-if="enrolledDentalMembersCount==1 && !isDentalRenewalGroupAvailable" id="aid_enrolledHeader_enr"><spring:message code="indportal.customGrouping.enrolled" /> ( {{enrolledDentalMembersCount}} <spring:message code="indportal.customGrouping.member" />)</span>
			<span ng-if="enrolledDentalMembersCount>1 && !isDentalRenewalGroupAvailable" id="aid_enrolledHeader_enr"><spring:message code="indportal.customGrouping.enrolled" /> ( {{enrolledDentalMembersCount}} <spring:message code="indportal.customGrouping.members" />)</span>
			<span ng-if="enrolledDentalMembersCount==1 && isDentalRenewalGroupAvailable" id="aid_enrolledHeader_enr">{{ previousCoverageYear }} Enrollments ( {{enrolledDentalMembersCount}} <spring:message code="indportal.customGrouping.member" />)</span>
			<span ng-if="enrolledDentalMembersCount>1 && isDentalRenewalGroupAvailable" id="aid_enrolledHeader_enr">{{ previousCoverageYear }} Enrollments ( {{enrolledDentalMembersCount}} <spring:message code="indportal.customGrouping.members" />)</span>
			<i id="dental_toggle_icon" class="toggle icon-chevron-up" ng-keyup="checkSlideDivEvents($event, 'dental')" ng-click="slideDiv('dental')" tabindex="0"></i >
		</div>

		  	<div ng-if="dentalGroups.length>0" ng-repeat="group in dentalGroups" class="well-step ps-detail__group-body" id="{{ 'aid_dental_grps_nrg_' + $index}}" style="margin:0px;border-radius:0px">
                <div  class="row-fluid">
                <div  class="col-xs-12 col-sm-5 col-md-4 col-lg-4 gutter10">
					<div ng-if="group.memberList != null && group.memberList.length > 0" ng-repeat="member in group.memberList" class="member-spacing">

						<div ng-show="member.dentalEnrollmentId !== undefined" class="ps-form__check" id="{{ 'aid_dental_grps_member_enr_nrg_' + $parent.$index}}">
							<input type="checkbox" aria-label="{{member.firstName}} {{member.middleName}} {{member.lastName}}" class="ps-form__check-input groupCheck" id="{{member.id}}" checked style="display: none" >
							<input type="checkbox" checked="" disabled="" ng-if="group.isRenewal" tabindex="0">
							<i class="icon-ok icon-large enrolled" ng-if="!group.isRenewal"></i>
								<strong class="member-info">
									{{member.firstName}} <span ng-if="member.middleName !== undefined"> {{member.middleName}} </span> {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
								</strong>
							
						</div>
					</div>
				</div>
				<div  class="col-xs-12 col-sm-7 col-md-8 col-lg-8 gutter10">

					<div id="{{ 'aid_dental_grps_max_aptc_all_enr_' + $index}}" ng-if="group.electedAptc > 0 && !group.isRenewal"><spring:message code="indportal.customGrouping.totalAPTC" /> <span>$</span>{{group.electedAptc}}</div>
					<div id="{{ 'aid_dental_grps_max_aptc_all_enr_' + $index}}" ng-if="group.maxAptc > 0 && group.isRenewal"><spring:message code="indportal.customGrouping.APTCMsgRenewal" /> {{ coverageYear }}:  {{formatAmount(group.maxAptc)}} <spring:message code="indportal.customGrouping.perMonth" /></div>

					<div class="margin10-tb">
						<div id="{{ 'aid_dental_issuer_name_all_enr_' + $index}}"> {{group.issuerName}} </div>
						<div id="{{ 'aid_dental_plan_name_all_enr_' + $index}}"> {{group.planName}} </div>
						<div id="{{ 'aid_dental_net_premium_all_enr_' + $index}}" ng-if="group.netPremium">{{ formatAmount(group.netPremium) }} <spring:message code="indportal.customGrouping.perMonth" /></div>
					</div>

					<div ng-show="group.validCoverageSDate && !group.isRenewal">
						<button class="btn btn-primary btn_res btn-small pull-left" id="{{ 'aid_dental_btn_disenroll_nrg_' + $index}}" ng-click="disEnrollPlan('Dental', group.coverageDate, group.encryptedGroupEnrollmentId, true)"><spring:message code="indportal.customGrouping.disenroll" /></button>
					</div>
					<div ng-show="showCancelCoverageButton(group) && !group.isRenewal">
						<button class="btn btn-primary btn_res btn-small pull-left" style="width: max-content;" id="{{ 'aid_dental_btn_cancel_coverage_nrg_' + $index}}" ng-click="disEnrollPlan('Dental', group.coverageDate, group.encryptedGroupEnrollmentId, false)"><spring:message code="indportal.customGrouping.canceCoverage" /></button>
					</div>
					<div ng-if="!group.appState.isBlank">
						<button class="btn btn-primary btn_res btn-small pull-right" id="{{ 'aid_dental_btn_change_finalize_nrg_' + $index}}" href="javascript:void(0)" ng-click="changePlan('DENTAL', group.id, group)">
							<span ng-if="group.appState.isFinalizeEnrollment"><spring:message code="indportal.customGrouping.finalizeEnrollment" /></span>
							<span ng-if="group.appState.isFinalizePlan"><spring:message code="indportal.customGrouping.finalizePlan" /></span>
							<span ng-if="group.appState.isChangePlan"><spring:message code="indportal.customGrouping.changePlan" /></span>
							<span ng-if="group.appState.isRenewOrChangePlan"><spring:message code="indportal.customGrouping.renewOrChangePlan" /></span>
						</button>
					</div>
				</div>
            </div>
		  	</div>

		<div ng-if="dentalGroups.length<=0">
			<p id="aid_dental_no_dental_grp_nrg_" class="alert alert-info"><spring:message code="indportal.customGrouping.noDentalGroup" /></p>
		</div>

	</div>

		<div class="well step group-header" id="dentalGroups" ng-show="showDentalGroups && !isAllMembersEnrolledInDental">
		<div>
			<label id="aid_shop_for_dental" class="ps-form__check-input"> <strong><spring:message code="indportal.customGrouping.shopForThese" /> </strong> </label>
		</div>

		<div ng-if="isStateCA"><label id="aid_dental_kid_msg_0" ><spring:message code="indportal.customGrouping.kidMsg0"/></label></div>
		<div ng-if="isStateCA"> <label id="aid_dental_kid_msg_1" ><spring:message code="indportal.customGrouping.kidMsg1" /> </label></div>
		<div ng-if="isStateCA"> <label id="aid_dental_kid_msg_2"><spring:message code="indportal.customGrouping.kidMsg2" /></label></div>

		  	<div ng-if="dentalGroups.length>0" ng-repeat="group in dentalGroups" class="flex-container" id="{{ 'aid_dental_grps_' + $index}}">
				<div  class="flex-child-1 member-info-container">
					<div ng-if="group.memberList != null && group.memberList.length > 0" ng-repeat="member in group.memberList" class="member-spacing">

						<div ng-show="member.dentalEnrollmentId !== undefined" class="ps-form__check" id="{{ 'aid_dental_grps_member_enr_' + $parent.$index}}">
							<input type="checkbox" aria-label="{{member.firstName}} {{member.middleName}} {{member.lastName}}" class="ps-form__check-input groupCheck" id="{{member.id}}" checked style="display: none" >
							<input type="checkbox" checked="" disabled="" ng-if="group.isRenewal">
							<i class="icon-ok icon-large enrolled" ng-if="!group.isRenewal"></i>
								<strong class="member-info">
									{{member.firstName}} <span ng-if="member.middleName !== undefined"> {{member.middleName}} </span> {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
								</strong>
							</i>
						</div>

						<div ng-show="member.dentalEnrollmentId === undefined" class="ps-form__check" id="{{ 'aid_dental_grps_member_nenr_' + $parent.$index}}">
							<input type="checkbox" class="ps-form__check-input groupCheck" id="{{member.id}}" ng-model="member.isChecked" ng-change="dentalOptionToggled(group)" value="{{member}}" ng-disabled= "isDentalDisabled(member)" >
							<label for="{{member.id}}" class="ps-form__check-input">
								<strong class="member-info">
									{{member.firstName}} <span ng-if="member.middleName !== undefined"> {{member.middleName}} </span> {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
								</strong>
							</label>
						</div>

					</div>
				</div>
				<div  class="flex-child-2">
					<div id="{{ 'aid_dental_grps_max_aptc_' + $index}}" ng-if="group.maxAptc > 0 && group.showAptcDentalMessage"><spring:message code="indportal.customGrouping.totalAPTC" /> {{formatAmount(group.maxAptc)}} <spring:message code="indportal.customGrouping.perMonth" /></div>
					<div style="margin-top: 10px; margin-right: 10px;">
						<div id="{{ 'aid_dental_issuer_name_' + $index}}"> {{group.issuerName}} </div>
						<div id="{{ 'aid_dental_plan_name_' + $index}}"> {{group.planName}} </div>
							<div id="{{ 'aid_dental_net_premium_' + $index}}" ng-if="group.netPremium">{{formatAmount(group.netPremium)}} <spring:message code="indportal.customGrouping.perMonth" /></div>
					</div>
				</div>
		  	</div>

		<div ng-if="dentalGroups.length<=0">
			<p id="aid_dental_no_dental_grp" class="alert alert-info"><spring:message code="indportal.customGrouping.noDentalGroup" /></p>
		</div>

		<div class="" ng-if="dentalGroups.length>0">
			<a href="javascript:void(0)" id="aid_dental_shop_btn_dental_" class="btn btn_res btn-primary pull-right" ng-click="showDentalPlansButtonDisabled ==true || enrollAfterCustomGrouping('DENTAL', dentalGroups[0].id, dentalGroups, false)" ng-hide="showDentalPlansButtonDisabled">
				<span ng-if="!isDentalRenewalGroupAvailable"><spring:message code="indportal.customGrouping.shopDentalPlans" /></span>
				<span ng-if="isDentalRenewalGroupAvailable"><spring:message code="indportal.customGrouping.renewPlan" /></span>
			</a>
		</div>
	</div>

</div>


		<div ng-show="pendingAppDialog_unused">
			 <div modal-show="pendingAppDialog_unused" id="aid_pending_app_dialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="pendingAppModal" aria-hidden="true">
				<div class="modal-dialog">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button"  class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					  <h3 id="pendingAppModal" style="font-size: 18px;border: none; z-index: 1101;"><spring:message code="indportal.pendingAppModal.header" /></h3>
					</div>
					<div class="modal-body">
						<spring:message code="indportal.pendingAppModalForCancel.body" />
					</div>
					<div class="modal-footer">
					  <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.ok" /></a>
					</div>
				  </div><!-- /.modal-content-->
				</div> <!-- /.modal-dialog-->
			 </div>
		</div>

		<!-- Confirm changing application to NOT_TO_CONSIDER -->
			<div ng-show="confirmNotToConsider">
				<div modal-show="confirmNotToConsider" id="aid_app_not_consider_dialog" class="modal fade customGroupingFailure" tabindex="-1" role="dialog" aria-labelledby="failedFormingGroups" aria-hidden="true">
					 <div class="modal-dialog">
						<div class="modal-content">
								<div id="failedFormingGroups" class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title"><spring:message code="indportal.customGrouping.customGrouping" /></h4>
								</div>
								<div class="modal-body">
											  <span ng-if="renewalTabType === 'DENTAL'">
          											<p><spring:message code="indportal.customGrouping.dentalnotconsider" /></p>
          									  </span>
										      <span ng-if="renewalTabType === 'HEALTH'">
          											<p><spring:message code="indportal.customGrouping.healthnotconsider" /></p>
          									  </span>
								</div>
								<div class="modal-footer">			       		         
          						  <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="appNotToConsider(renewalTabType)" class=""><spring:message code="indportal.portalhome.ok" /></a>
								  <a href="#" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.cancel" /></a>
						</div>
					   </div><!-- /.modal-content-->
					 </div> <!-- /.modal-dialog-->
				</div>
			</div>
		<!-- Confirm changing application to NOT_TO_CONSIDER -->

		<!-- Modal for failing of forming custom groups -->
			<div ng-show= "customGroupingFailure">
				<div modal-show="customGroupingFailure" id="aid_custom_grouping_failure_dialog" class="modal fade customGroupingFailure" tabindex="-1" role="dialog" aria-labelledby="failedFormingGroups" aria-hidden="true">
					 <div class="modal-dialog">
						<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title"><spring:message code="indportal.customGrouping.customGrouping" /></h4>
								</div>
								<div class="modal-body">
									<p ng-if="errorAppNotToConsider"><spring:message code="indportal.customGrouping.error.appNotToConsider"/></p>
									<p ng-if="errorValidateGroup"><spring:message code="indportal.customGrouping.error.validateGroup"/></p>
									<p ng-if="errorDentalKids"><spring:message code="indportal.customGrouping.error.dentalKids"/></p>
									<p ng-if="errorDentalAdults"><spring:message code="indportal.customGrouping.error.dentalAdult"/></p>
									<p ng-if="!errorAppNotToConsider && !errorValidateGroup && !errorDentalKids && !errorDentalAdults">{{customGroupingFailureMsg}}</p>
								</div>
								<div class="modal-footer">
						<button class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.portalhome.close"/></button>
						</div>
					   </div><!-- /.modal-content-->
					 </div> <!-- /.modal-dialog-->
				</div>
			</div>
		<!-- Modal for failing of forming custom groups -->

		<!-- Modal to enroll in a new plan after the current plan is terminated -->
<div ng-show= "shopForNewPlanFailure">
				<div modal-show="shopForNewPlanFailure" id="aid_custom_grouping_failure_dialog" class="modal fade customGroupingFailure" tabindex="-1" role="dialog" aria-labelledby="failedFormingGroups" aria-hidden="true">
					<div class="modal-content">
							<div class="modal-body">
								<spring:message code="indportal.customGrouping.shopForNewPlanMsg"/>								
							</div>
							<div class="modal-footer">
								<button class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.portalhome.cancel"/></button>
								<button class="btn" ng-click="shopForNewPlan()" data-dismiss="modal" >
									<spring:message code="indportal.customGrouping.shopForPlans"/>
								</button>
							</div>
				   </div><!-- /.modal-content-->
				</div>
			</div>
		<!-- Modal to enroll in a new plan after the current plan is terminated -->

		<!-- Modal for failing of multi custom groups -->
				<div ng-show= "multiGroupFailure" id="aid_multi_group_failure_dialog" modal-show="multiGroupFailure" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedFormingGroups" aria-hidden="true" data-keyboard="false" data-backdrop="static">
					<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" ng-click="undoSelection()" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body">
								<p ng-if="multiGroupFailureChangeRecommended && !multiGroupFailureNonStandardNotAllowed"><spring:message code="indportal.customGrouping.error.changeRecommended" /></p>
								<p ng-if="multiGroupFailureChangeRecommendedNativeAmerican && !multiGroupFailureNonStandardNotAllowed"><spring:message code="indportal.customGrouping.error.changeRecommendedNativeAmerican" /></p>
								<p ng-if="multiGroupFailureNotAllowed && !multiGroupFailureChangeRecommended && !multiGroupFailureChangeRecommendedNativeAmerican"><spring:message code="indportal.customGrouping.error.enroll" /></p>
								<p ng-if="multiGroupFailureNonStandardNotAllowed && !multiGroupFailureNotAllowed"><spring:message code="indportal.customGrouping.error.nonStandardGroupEnroll" /></p>
								<p ng-if="!multiGroupFailureNonStandardNotAllowed && !multiGroupFailureNotAllowed"><spring:message code="indportal.customGrouping.error.proceed" /></p>
							</div>
							<div class="modal-footer">


								<button class="btn" ng-click="undoSelection()" ng-if="!multiGroupFailureNotAllowed && !multiGroupFailureNonStandardNotAllowed">
									<spring:message code="indportal.customGrouping.cancel" />
								</button>
								<button class="btn"  data-dismiss="modal" ng-if="!multiGroupFailureNotAllowed && !multiGroupFailureNonStandardNotAllowed">
									<spring:message code="indportal.customGrouping.continue" />
								</button>
								<button class="btn"  ng-click="undoSelection()" ng-if="multiGroupFailureNotAllowed || multiGroupFailureNonStandardNotAllowed">
									<spring:message code="indportal.customGrouping.continue" />
								</button>
							</div>
				   </div><!-- /.modal-content-->
				</div>
		<!-- Modal for failing of forming custom groups -->


		<!-- Modal for failing of multi custom groups -->
		<div ng-show= "dentalHasHealthEnrollemnts" id="aid_has_dental_enrollments_dialog" modal-show="dentalHasHealthEnrollemnts" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedFormingGroups" aria-hidden="true">
			<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="shop_popup_close" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<p><spring:message code="indportal.customGrouping.error.dentalNoHealthPlan" /></p>
					</div>
					<div class="modal-footer">
						<button class="btn" id="start_dental_enroll" ng-click="toggleTabs('Dental')"><spring:message code="indportal.customGrouping.startDentalEnrollment" /></button>
						<button class="btn" id="continue_health_enroll" ng-click="toggleTabs('Health')"><spring:message code="indportal.customGrouping.continuesHealthEnrollment" /></button>
					</div>

		   </div><!-- /.modal-content-->
		</div>
		<!-- Modal for failing of forming custom groups -->


		<!-- Modal to encourage consumers to enroll in health plans prior to dental -->
		<div ng-show="healthPriorDentalModal" modal-show="healthPriorDentalModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedFormingGroups" aria-hidden="true">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" id="shop_popup_close" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
			<div class="modal-body">
				<p><spring:message code="indportal.customGrouping.healthEnrollPriorDental" /></p>
			</div>
			<div class="modal-footer">
				<button class="btn" id="continueWithHealth" ng-click="toggleTabs('Health')"><spring:message code="indportal.customGrouping.continueWithHealth" /></button>
				<button class="btn" id="StartDentalShopping" ng-click="toggleTabs('Dental')"><spring:message code="indportal.customGrouping.startDentalShopping" /></button>
			</div>

			</div><!-- /.modal-content-->
		</div>
		<!-- Modal to encourage consumers to enroll in health plans prior to dental -->

<!-- Pending app dialog -->
<div ng-show="pendingAppDialog">
 <div modal-show="pendingAppDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="pendingAppModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="pendingAppModal" style="font-size: 18px;border: none; z-index: 1101;"><spring:message code="indportal.pendingAppModal.header" /></h3>
        </div>
        <div class="modal-body">
			<spring:message code="indportal.pendingAppModal.body" />
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.cancel" /></a>
        </div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- Pending app dialog -->

		<!-- Disenroll Modal-->
		<div ng-show="disenrollDialog" id="aid_disenroll_dialog">
		 <div modal-show="disenrollDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disenrollModal" aria-hidden="true">
			<div class="modal-dialog">
			  <div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancelDisenroll()">&times;</button>
					<h3 id="disenrollModal">
							<span ng-if="enrollmentAction"><spring:message code="indportal.plansummary.voluntarydisenrollmentreasons" javaScriptEscape="true"/></span>
							<span ng-if="!enrollmentAction"><spring:message code="indportal.plansummary.cancelcoverage" javaScriptEscape="true"/></span>
						</h3>
				</div>
				<div class="modal-body ">
				  <form class="form-horizontal">
					<h5 ng-if="enrollmentAction" class="margin10-l" id="subtitle"><strong><spring:message code="indportal.plansummary.whyareyoudisenrolling" javaScriptEscape="true"/></strong></h5>
					<h5 ng-if="!enrollmentAction" class="margin10-l" id="subtitle"><strong><spring:message code="indportal.plansummary.cancel.whyareyoudisenrolling" javaScriptEscape="true"/></strong></h5>

					<div class="margin30-l">

					  <label class="radio">
						  <input type="radio" name="disenrollmentreason" id="disaffordability" value="CANNOT_AFFORD_PAYMENT" checked="" required="" class="" ng-model="checked">
						  <spring:message code="indportal.plansummary.icantafford" />
					  </label>
					  <br>
					  <label class="radio" >
						  <input type="radio" name="disenrollmentreason" id="disnothappywithplan" value="NO_PROPER_SERVICE" required="" class="" ng-model="checked">
						  <spring:message code="indportal.plansummary.imnothappywith" />
						  <span ng-if="stateCode !== 'CA' && stateCode !== 'MN'">
								<span ng-if="typeOfPlan == 'Dental'"><spring:message code="indportal.plansummary.dental" javaScriptEscape="true"/></span>
								<span ng-if="typeOfPlan == 'Health'"><spring:message code="indportal.plansummary.health" javaScriptEscape="true"/></span>
								<span ng-if="typeOfPlan == 'both'"><spring:message code="indportal.plansummary.health" javaScriptEscape="true"/> and <spring:message code="indportal.plansummary.dental" javaScriptEscape="true"/></span>
								<spring:message code="indportal.plansummary.plan" javaScriptEscape="true"/>
						  </span>
					  </label>
					  <br>
					  <label class="radio">
						  <input type="radio" name="disenrollmentreason" id="outOfnetwork" value="PHY_OUT_OF_NETWORK" required="" class="" ng-model="checked">
						  <spring:message code="indportal.plansummary.phyOutOfNetwork" />
					  </label>
					  <br>
					  <label class="radio">
						  <input type="radio" name="disenrollmentreason" id="agentError" value="AGENT_ERROR" class="" ng-model="checked">
						  <spring:message code="indportal.plansummary.agentError"/>
					  </label>
					  <br>
					  <label class="radio">
						  <input type="radio" name="disenrollmentreason" id="other" value="OTHER" class="" ng-model="checked">
						  <spring:message code="indportal.plansummary.other"/>
					  </label>

					</div>
				   <div id="otherDisenrollReason" ng-if="checked=='OTHER'">
					  <div class="margin30-l">
						<input type="text" name="tellusmore" id="tellusmore" class="span11" ng-model="$parent.otherReason"/>
					  </div>
				   </div>
				  </form>
					  <div ng-if="stateCode !== 'MN'" class="margin5-l margin5-r margin20-t alert alert-info margin0-b">
					  <p><spring:message code="indportal.plansummary.otherReasonText"/></p>
					  </div>
				</div>
				<div class="modal-footer">
				  <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal" ng-click="cancelDisenroll()"><spring:message code="indportal.portalhome.cancel" /></a>
				  <a href="javascript:void(0);" class="btn btn-primary"  ng-if="!checked" ng-click="disenrollmentReason()" class=""><spring:message code="indportal.customGrouping.skipAndContinue" /></a>
				  <a href="javascript:void(0);" class="btn btn-primary"  ng-if="checked" ng-click="disenrollmentReason()" class=""><spring:message code="indportal.portalhome.continue" /></a>
				</div>
			  </div><!-- /.modal-content-->
			</div> <!-- /.modal-dialog-->
		   </div>
		</div>
<!-- Disenroll Modal-->

<!-- Are You Sure Modal-->
<div ng-show="areYourSureDialog">
 <div modal-show="areYourSureDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disenrollCheckModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
				<div  id="disenrollCheckModal" class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<span ng-if="typeOfPlan == 'Dental' && enrollmentAction"><h3><spring:message code="indportal.plansummary.doyouwanttocontinue.dental" javaScriptEscape="true"/></h3></span>
					<span ng-if="typeOfPlan == 'Health' && enrollmentAction"><h3><spring:message code="indportal.plansummary.doyouwanttocontinue" javaScriptEscape="true"/></h3></span>
					<span ng-if="typeOfPlan == 'both' && enrollmentAction"><h3><spring:message code="indportal.plansummary.doyouwanttocontinueboth" javaScriptEscape="true"/></h3></span>
					<span ng-if="typeOfPlan == 'Dental' && !enrollmentAction"><h3><spring:message code="indportal.plansummary.cancel.doyouwanttocontinue.dental" javaScriptEscape="true"/></h3></span>
					<span ng-if="typeOfPlan == 'Health' && !enrollmentAction"><h3><spring:message code="indportal.plansummary.cancel.doyouwanttocontinue.health" javaScriptEscape="true"/></h3></span>
					<span ng-if="typeOfPlan == 'both' && !enrollmentAction"><h3><spring:message code="indportal.plansummary.doyouwanttocontinueboth" javaScriptEscape="true"/></h3></span>
				</div>
        <div class="modal-body gutter20">
		  <span ng-if="typeOfPlan == 'Health'">
          	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll" /></p>
          </span>
					<span ng-if="stateCode !== 'MN' && typeOfPlan == 'Dental'">
          	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenrolldental" /></p>
		 	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll2dental" /></p>
          </span>
					<span ng-if="stateCode === 'MN' && typeOfPlan == 'Dental'">
						<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll" javaScriptEscape="true"/></p>
						<p><spring:message code="indportal.plansummary.ifyoucanaffordbut" javaScriptEscape="true"/></p>
					</span>
		  <span ng-if="stateCode !== 'MN' && typeOfPlan == 'both'">
          	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenrollboth" /></p>
		 	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll2both" /> <a href="https://www.coveredca.com/individuals-and-families/getting-covered/special-enrollment/qualifying-life-events/" target="_blank"><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll2_1" /></a></p>
		  	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll3both" /></p>
          </span>
		  <span ng-if="typeOfPlan == 'Health' && dentaldetails && dentaldetails.disEnroll && dentaldetails.electedAptc > 0">
			<p><spring:message code="indportal.plansummary.aptcwarning" /></p>
		  </span>
          <div class="margin20-t alert alert-info margin0-b" ng-if="typeOfPlan == 'Health' || typeOfPlan == 'both'">
            <spring:message code="indportal.plansummary.ifyoucanaffordbut" />
						<br><br>
						<span ng-if="stateCode !== 'MN'">
			<spring:message code="indportal.plansummary.thefeecontent4" /> <a href="<spring:message code='indportal.plansummary.yhiExemptionsUrl'/>" target="_blank"> <spring:message code="indportal.plansummary.yhiExemptionsUrl"/></a>
						</span>
          </div>
        </div>
        <div ng-if="stateCode !== 'MN'" class="modal-footer">
          <a href="#" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.no" /></a>
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="goToDisenrollDateModal()" class=""><spring:message code="indportal.portalhome.yes" /></a>
				</div>
				<div ng-if="stateCode === 'MN'" class="modal-footer">
					<a href="#" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
					<a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="goToDisenrollDateModal()" class=""><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
				</div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- Are You Sure Modal-->
<!-- Choosing Disenrollment Date Modal-->
<div ng-show="disenrollDateDialog.step">
 <div modal-show="disenrollDateDialog.step" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disenrollCheckModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="disenrollDateModal" ng-if="disenrollDateDialog.step == 1"><spring:message code="indportal.plansummary.selectTerminationDate"/></h3>
		  <h3 id="ConfirmDisenrollDateModal" ng-if="disenrollDateDialog.step == 2"><spring:message code="indportal.plansummary.confirmEndDate"/></h3>
		  <h3 id="ConfirmSubmitDisenrollDateModal" ng-if="disenrollDateDialog.step == 3"><spring:message code="indportal.plansummary.confirmation"/></h3>
        </div>
        <div class="modal-body gutter20" ng-if="disenrollDateDialog.step == 1">
		  <form class="form-horizontal">
            <div class="margin30-l">
							<label class="radio margin5-b">
                  <input type="radio" name="disenrollmentdate" checked="" required="" class="" ng-model="disenrollDateDialog.date" value="CURRENT_MONTH" ng-click="saveSelectedDate('CURRENT_MONTH')">
									<spring:message code="indportal.plansummary.disenrollmentDates.lastDayCurrentMonth"/>
							</label>
							<label class="radio margin5-b">
                  <input type="radio" name="disenrollmentdate" checked="" required="" class="" ng-model="disenrollDateDialog.date" value="NEXT_MONTH" ng-click="saveSelectedDate('NEXT_MONTH')">
									<spring:message code="indportal.plansummary.disenrollmentDates.lastDayNextMonth"/>
							</label>
							<label class="radio margin5-b">
                  <input type="radio" name="disenrollmentdate" checked="" required="" class="" ng-model="disenrollDateDialog.date" value="MONTH_AFTER_NEXT_MONTH" ng-click="saveSelectedDate('MONTH_AFTER_NEXT_MONTH')">
									<spring:message code="indportal.plansummary.disenrollmentDates.lastDayAfterMonth"/>
              </label>
            </div>

		  </form>
		  <div class="margin20-t alert alert-info margin0-b"><spring:message code="indportal.plansummary.needSpecificTermDate"/></div>
        </div>

        <div class="modal-body gutter20" ng-if="disenrollDateDialog.step == 2">
          <p>
						<span ng-if="typeOfPlan === 'Health'"><spring:message code="indportal.plansummary.customGrouping.chosenEndDate.health"/></span>
						<span ng-if="typeOfPlan === 'Dental'"><spring:message code="indportal.plansummary.customGrouping.chosenEndDate.dental"/></span>
						<span ng-if="typeOfPlan === 'both'"><spring:message code="indportal.plansummary.customGrouping.chosenEndDate.both"/></span>
					</p>

		  <div class="margin20-t margin0-b disenroll-confirm-box">
			<!--<p><spring:message code="indportal.plansummary.from"/></p>-->
			<div ng-if="typeOfPlan == 'Health'"><img class="pull-left" ng-src="{{disenrollGroup.logoUrl}}" alt="{{disenrollGroup.issuerName}} logo"></div>
			<div ng-if="typeOfPlan == 'Dental'"><img class="pull-left" ng-src="{{disenrollGroup.logoUrl}}" alt="{{disenrollGroup.issuerName}} logo"></div>
			<div ng-if="typeOfPlan == 'both'"><img class="pull-left" ng-src="{{disenrollGroup.logoUrl}}" alt="{{disenrollGroup.issuerName}} logo">
			<img class="pull-right" ng-src="{{disenrollGroup.logoUrl}}" alt="{{disenrollGroup.issuerName}} logo"></div>
          </div>
        </div>
        <div class="modal-body gutter20" ng-if="disenrollDateDialog.step == 3">
		  <div class="margin20-t alert alert-info margin0-b disenroll-confirm-box">
					<p ng-if="typeOfPlan === 'Health'">
						<spring:message code="indportal.plansummary.disenrollConfirmContent.health"/>
					</p>
					<p ng-if="typeOfPlan === 'Dental'">
						<spring:message code="indportal.plansummary.disenrollConfirmContent.dental"/>
					</p>
					<p ng-if="typeOfPlan === 'both'">
						<spring:message code="indportal.plansummary.disenrollConfirmContent.both"/>
					</p>
          </div>
		  <div class="margin20-t alert alert-info margin0-b">
			<spring:message code="indportal.plansummary.note"/>:
			<ul>
				<li><spring:message code="indportal.plansummary.disenrollConfirmPt1"/></li>
				<li ng-if="stateCode === 'MN'"><spring:message code="indportal.plansummary.disenrollConfirmPt2"/></li>
			</ul>
		  </div>
        </div>

        <div class="modal-footer" ng-if="disenrollDateDialog.step == 1">
					<a ng-if="stateCode === 'MN'" href="#" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
          <button class="btn btn-primary" ng-click="disenrollDateDialog.step = 2" ng-disabled="!disenrollDateDialog.date"><spring:message code="indportal.portalhome.continue"/></button>
        </div>
        <div class="modal-footer" ng-if="disenrollDateDialog.step == 2">
          <button class="btn btn-secondary" ng-click="disenrollDateDialog.step = 1"><spring:message code="indportal.plansummary.updateTermDate"/></button>
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" class="" ng-click="disenrollDateDialog.step = 3"><spring:message code="indportal.portalhome.continue"/></a>
        </div>
        <div class="modal-footer" ng-if="disenrollDateDialog.step == 3">
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" class="" ng-click="submitDisenrollment()"><spring:message code="indportal.contactus.submit"/></a>
        </div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- Choosing Disenrollment Date Modal-->

<!-- SubmitDisenroll-->
<div ng-show="submitDisenrollmentDialog">
 <div modal-show="submitDisenrollmentDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="submitDisenrollModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 id="submitDisenrollModal">
						<span ng-if="enrollmentAction"> <spring:message code="indportal.plansummary.voluntarydisenrollmentrequestsubmitted" javaScriptEscape="true"/></span>
						<span ng-if="!enrollmentAction"> <spring:message code="indportal.plansummary.cancelcoveragerequest" javaScriptEscape="true"/></span>
					</h3>
        </div>
				<div class="modal-body gutter20">
					<p>
						<span ng-if="stateCode !== 'MN' && enrollmentAction"> <spring:message code="indportal.plansummary.wehavereviewed" javaScriptEscape="true"/></span>
						<span ng-if="stateCode === 'MN' && enrollmentAction"> <spring:message code="indportal.plansummary.wehavereviewedcancelcoverage" javaScriptEscape="true"/></span>
						<span ng-if="!enrollmentAction"> <spring:message code="indportal.plansummary.wehavereviewedcancelcoverage" javaScriptEscape="true"/></span>
					</p>
					<!--<p><spring:message code="indportal.plansummary.youwillbecovered" javaScriptEscape="true"/></p>-->
					<p ng-if="stateCode !== 'MN'"><spring:message code="indportal.plansummary.ifyouhaveanyquestions" javaScriptEscape="true"/></p>
				</div>
        <div class="modal-footer">
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="goHome()"><spring:message code="indportal.portalhome.godashboard" /></a>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
</div>
<!-- SubmitDisenroll-->
<!-- SubmitDisenroll Failure Modal-->
<div ng-show="submitDisenrollmentFailureDialog">
 <div modal-show="submitDisenrollmentFailureDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedSumissionModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="failedSumissionModal"><spring:message code="indportal.plansummary.technicalissues" /></h3>
        </div>
        <div class="modal-body gutter20">
          <p><spring:message code="indportal.plansummary.cannotsubmitrequest" /></p>
          <p><spring:message code="indportal.plansummary.ifyouhaveanyquestions" /></p>
        </div>
        <div class="modal-footer">
          <a class="btn btn-secondary" data-dismiss="modal" href="/#"><spring:message code="indportal.portalhome.cancel" /></a>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
  </div>
</div>
<!-- SubmitDisenroll Failure Modal-->
</script>
