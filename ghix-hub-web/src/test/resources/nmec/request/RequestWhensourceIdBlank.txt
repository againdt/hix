{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload": {
    "taxHousehold": [
      {
        "householdMember": [
          {
            "dateOfBirth": "01/25/1970",
            "name": {
              "firstName": "John",
              "middleName": "D",
              "lastName": "Fox"
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "222222204"
            },
            "gender": "M",
            "householdContact": {
              "homeAddress": {
                "state": "AZ"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "05/01/2014",
              "insurancePolicyExpirationDate": "12/31/2014"
            }
          }
        ]
      }
    ]
  }
}