/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.EnrollmentSftpFileTransferDTO;
import com.getinsured.hix.enrollment.repository.IEnrollmentCmsFileTransferLogRepository;
import com.getinsured.hix.enrollment.service.CmsSftpNotificationService;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.enrollment.util.EnrollmentSftpUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.model.enrollment.EnrollmentCmsFileTransferLog;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * @author negi_s
 *
 */
@Service("cmsSftpService")
public class CmsSftpServiceImpl implements CmsSftpService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CmsSftpServiceImpl.class);
	
	public static enum CMS_INBOUND_FUNC_CODE {
		SBMR, SBMS, EPSEXT, SI820, ERRSBM, SBEDDR
	}
	
	public static enum IRS_INBOUND_FUNC_CODE {
		EOMACK, EOMNAK, EOMOUT
	}
	
	public static enum PLR_INBOUND_FUNC_CODE {
		SDBO
	}
	
	public static enum ANNUAL_INBOUND_FUNC_CODE {
		EOYACK, EOYNAK, EOYOUT
	}
	
/*	private static final String CMS_DROP_PATH = "/inbound30";
	private static final String CMS_PICKUP_PATH = "/outbound30";*/

	@Autowired
	private BatchJobExecutionService batchJobExecutionService;
	@Autowired
	private IEnrollmentCmsFileTransferLogRepository enrollmentCmsFileTransferLogRepository;
	@Autowired
	private CmsSftpNotificationService cmsSftpNotificationService;
	@Autowired 
	private Gson platformGson;
	@Autowired 
	private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;

	private static Type listType = new TypeToken<ArrayList<EnrollmentSftpFileTransferDTO>>() {}.getType();
	private static String CMS_SFTP_PASSWORD;
	private static String EXCHG_SFTP_PASSWORD;
	private static final String ENC_START_TAG = "ENC(";
	private static final String ENC_END_TAG = ")";
	private static final String SPACE = " ";
	
	@Value("#{configProp['enrollment.cms.sftp.url']}")
	private String CMS_SFTP_URL;
	@Value("#{configProp['enrollment.cms.sftp.port']}")
	private String CMS_SFTP_PORT;
	@Value("#{configProp['enrollment.cms.sftp.outbound.path']}")
	private String CMS_SFTP_OUTBOUND_PATH;
	@Value("#{configProp['enrollment.cms.sftp.inbound.path']}")
	private String CMS_SFTP_INBOUND_PATH;
	@Value("#{configProp['enrollment.cms.sftp.username']}")
	private String CMS_SFTP_USERNAME;
	
	@Value("#{configProp['enrollment.cms.sftp.password']}")
	public void setCmsSftpPassword(String sftpPassword){
		CMS_SFTP_PASSWORD = convertPropertyValue(sftpPassword);
	}
	
	@Value("#{configProp['enrollment.exchg.sftp.url']}")
	private String EXCHG_SFTP_URL;
	@Value("#{configProp['enrollment.exchg.sftp.port']}")
	private String EXCHG_SFTP_PORT;
	@Value("#{configProp['enrollment.exchg.sftp.folderPath']}")
	private String EXCHG_SFTP_FOLDERPATH;
	@Value("#{configProp['enrollment.exchg.sftp.username']}")
	private String EXCHG_SFTP_USERNAME;
	
	@Value("#{configProp['enrollment.exchg.sftp.password']}")
	private void setExchgSftpPassword(String sftpPassword){
		EXCHG_SFTP_PASSWORD = convertPropertyValue(sftpPassword);
	}

	@Override
	public List<BatchJobExecution> getRunningBatchList(String jobName) {
		LOGGER.info("Inside CmsSftpOutboundServiceImpl :: getRunningBatchList() ");
		return batchJobExecutionService.findRunningJob(jobName);
	}

	@Override
	public void uploadFilesForReportType(String reportType, Long batchExecutionId) throws GIException {
		LOGGER.info("Inside CmsSftpServiceImpl :: uploadFilesForReportType() ");
//		String sourceLocation = "E:/SFTPUpload";
//		String hostLocation = "/opt/batch/ghixhome/ghix-docs/enrollment/SftpUpload";
//		Map<String, String> sftpParameters = getSftpTestParameters();
		String sourceLocation= getSourcePathFromType(reportType);
//		String hostLocation = CMS_DROP_PATH;
		//String hostLocation =  DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.CMS_SFTP_INBOUND_PATH);
		Map<String, String> sftpParameters = getSftpParameters();
		if(null != sourceLocation && null != CMS_SFTP_INBOUND_PATH && new File(sourceLocation).exists()){
			CMS_SFTP_INBOUND_PATH = CMS_SFTP_INBOUND_PATH.trim();
			String response = EnrollmentSftpUtil.uploadFilesByExtension(sftpParameters, sourceLocation, CMS_SFTP_INBOUND_PATH, EnrollmentConstants.FILE_TYPE_IN, enrollmentGIMonitorUtil);
			List<EnrollmentSftpFileTransferDTO> fileTransferList = platformGson.fromJson(response, listType);
			List<EnrollmentCmsFileTransferLog> transferLogList = logSftpTransfers(fileTransferList, batchExecutionId,
					reportType, sourceLocation, CMS_SFTP_INBOUND_PATH);
			// Copy successful files to YHI folder
			if (!EnrollmentCmsFileTransferLog.ReportType.ANNUAL.toString().equalsIgnoreCase(reportType)
					&& EnrollmentConstants.STATE_CODE_ID.equalsIgnoreCase(EnrollmentConfiguration.returnStateCode())) {
				copyToExchgFolder(transferLogList, reportType, EnrollmentConstants.TRANSFER_DIRECTION_OUT, batchExecutionId);
			}
			// Clear successful files from folder
			clearSourceFolder(transferLogList);
			sendYhiAlertEmail(transferLogList, reportType, EnrollmentConstants.TRANSFER_DIRECTION_OUT);
		}else{
			throw new GIException("Source location doesn't exist");
		}
	}

	private String getSourcePathFromType(String reportType) {
		StringBuilder sourceBuilder = EnrollmentUtils
				.getReportingBasePathBuilderByTypeAndDirection(reportType, EnrollmentConstants.TRANSFER_DIRECTION_OUT)
				.append(File.separator).append(EnrollmentConstants.VALID_FOLDER);
				
		return sourceBuilder.toString();
	}

	private Map<String, String> getSftpParameters() {
		Map<String, String> sftpParameters = new HashMap<String, String>();
		sftpParameters.put(EnrollmentSftpUtil.SFTP_HOST, CMS_SFTP_URL);
		sftpParameters.put(EnrollmentSftpUtil.SFTP_USERNAME, CMS_SFTP_USERNAME);
		sftpParameters.put(EnrollmentSftpUtil.SFTP_PASSWORD, CMS_SFTP_PASSWORD);
		sftpParameters.put(EnrollmentSftpUtil.SFTP_PORT, CMS_SFTP_PORT);
		return sftpParameters;
	}
	
	private Map<String, String> getSftpTestParameters() {
		Map<String, String> sftpParameters = new HashMap<String, String>();
		sftpParameters.put(EnrollmentSftpUtil.SFTP_HOST, "idmainqa.ghixqa.com");
		sftpParameters.put(EnrollmentSftpUtil.SFTP_USERNAME, "app");
		sftpParameters.put(EnrollmentSftpUtil.SFTP_PASSWORD, "ghix123#");
		sftpParameters.put(EnrollmentSftpUtil.SFTP_PORT, "22");
		return sftpParameters;
	}

	@Override
	public List<EnrollmentCmsFileTransferLog> logSftpTransfers(List<EnrollmentSftpFileTransferDTO> fileTransferList, Long batchExecutionId,
			String reportType, String sourceLocation, String hostLocation) {
		List<EnrollmentCmsFileTransferLog> transferLogList = new ArrayList<EnrollmentCmsFileTransferLog>();
		for (EnrollmentSftpFileTransferDTO dto : fileTransferList) {
			EnrollmentCmsFileTransferLog transferLog = new EnrollmentCmsFileTransferLog();
			transferLog.setFileName(dto.getFileName());
			transferLog.setFileSize(Long.parseLong(dto.getFileSize()));
			transferLog.setReportType(reportType);
			if(null != dto.getTransferType()){
				transferLog.setDirection(EnrollmentCmsFileTransferLog.Direction.valueOf(dto.getTransferType()).toString());
			}
			transferLog.setErrorDescription(dto.getErrorMsg());
			transferLog.setStatus(dto.getTransferStatus());
			transferLog.setSourceDirectory(sourceLocation);
			transferLog.setTargetDirectory(hostLocation);
			transferLog.setBatchExecutionId(batchExecutionId);
			transferLogList.add(transferLog);
			try {
				enrollmentCmsFileTransferLogRepository.saveAndFlush(transferLog);
			} catch (Exception e) {
				LOGGER.error("Error saving log", e);
			}
		}
		return transferLogList;
	}
	
	/**
	 * Cleans the successfully transferred files from the source
	 * @param transferLogList
	 */
	private void clearSourceFolder(List<EnrollmentCmsFileTransferLog> transferLogList) {
		for(EnrollmentCmsFileTransferLog fileInfo : transferLogList){
			if(EnrollmentConstants.SUCCESS.equalsIgnoreCase(fileInfo.getStatus())){
				try {
					FileUtils.forceDelete(new File(fileInfo.getSourceDirectory()+File.separator+fileInfo.getFileName()));
				} catch (Exception e) {
					LOGGER.error("Unable to delete file : "+fileInfo.getFileName(), e);
				}
			}
		}
	}
	
	@Override
	public void downloadFilesForReportType(String reportType, Long batchExecutionId) throws GIException {
		LOGGER.info("Inside CmsSftpServiceImpl :: downloadFilesForReportType() ");
		String targetLocation = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(reportType, EnrollmentConstants.TRANSFER_DIRECTION_IN).toString();
//		String hostLocation = CMS_PICKUP_PATH;
		//String hostLocation = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.CMS_SFTP_OUTBOUND_PATH);
		Map<String, String> sftpParameters = getSftpParameters();
//		String targetLocation = "E:/SFTPDownload";
//		String hostLocation = "/opt/batch/ghixhome/ghix-docs/enrollment/CMSXML/Outbound/valid";
//		Map<String, String> sftpParameters = getSftpTestParameters();
		
		if(null != CMS_SFTP_OUTBOUND_PATH && null != targetLocation && new File(targetLocation).exists()){
			CMS_SFTP_OUTBOUND_PATH = CMS_SFTP_OUTBOUND_PATH.trim();
			String partnerId = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.TradingPartnerID);
			switch(reportType){
			case "CMS":
				String defaultTargetLocation = targetLocation;
				for (CMS_INBOUND_FUNC_CODE codeEnum : CMS_INBOUND_FUNC_CODE.values()) {
					targetLocation = defaultTargetLocation;
					if (CMS_INBOUND_FUNC_CODE.EPSEXT == codeEnum || CMS_INBOUND_FUNC_CODE.ERRSBM == codeEnum
							|| CMS_INBOUND_FUNC_CODE.SBEDDR == codeEnum) {
						StringBuilder suffix = new StringBuilder();
						suffix.append(File.separatorChar).append(EnrollmentConstants.ARCHIVE_FOLDER)
								.append(File.separatorChar).append(codeEnum).append(File.separatorChar)
								.append(batchExecutionId);
						targetLocation += suffix.toString();
					}
					downloadFilesByPattern(reportType, sftpParameters, CMS_SFTP_OUTBOUND_PATH, targetLocation,partnerId, codeEnum.toString(), batchExecutionId);
				}
				break;
			case "IRS":
				for(IRS_INBOUND_FUNC_CODE codeEnum : IRS_INBOUND_FUNC_CODE.values()){
					downloadFilesByPattern(reportType, sftpParameters, CMS_SFTP_OUTBOUND_PATH, targetLocation,partnerId, codeEnum.toString(),batchExecutionId);
				}
				break;
			case "PLR":
				for(PLR_INBOUND_FUNC_CODE codeEnum : PLR_INBOUND_FUNC_CODE.values()){
					downloadFilesByPattern(reportType, sftpParameters, CMS_SFTP_OUTBOUND_PATH, targetLocation,partnerId, codeEnum.toString(),batchExecutionId);
				}
				break;
			case "ANNUAL":
				for(ANNUAL_INBOUND_FUNC_CODE codeEnum : ANNUAL_INBOUND_FUNC_CODE.values()){
					downloadFilesByPattern(reportType, sftpParameters, CMS_SFTP_OUTBOUND_PATH, targetLocation,partnerId, codeEnum.toString(),batchExecutionId);
				}
				break;
			}
		}else{
			throw new GIException("Source location doesn't exist");
		}
			
	}

	/**
	 * Download files as per partner ID and the function code
	 * @param reportType
	 * @param sftpParameters
	 * @param hostLocation
	 * @param targetLocation
	 * @param partnerId
	 * @param funcCode
	 * @param batchExecutionId
	 */
	private void downloadFilesByPattern(String reportType, Map<String, String> sftpParameters, String hostLocation, String targetLocation, String partnerId, String funcCode, Long batchExecutionId) {
		String pattern = partnerId + "." + funcCode;
		String response = EnrollmentSftpUtil.downloadFilesByExtensionAndPattern(sftpParameters, hostLocation, targetLocation, EnrollmentConstants.FILE_TYPE_OUT, pattern, enrollmentGIMonitorUtil);
		List<EnrollmentSftpFileTransferDTO> fileTransferList = platformGson.fromJson(response, listType);
		List<EnrollmentCmsFileTransferLog> transferLogList = logSftpTransfers(fileTransferList, batchExecutionId, reportType, hostLocation, targetLocation);
		List<String> fileListToDelete = new ArrayList<String>();
		for(EnrollmentCmsFileTransferLog fileInfo : transferLogList){
			if(EnrollmentConstants.SUCCESS.equalsIgnoreCase(fileInfo.getStatus())){
				fileListToDelete.add(fileInfo.getFileName());
			}
		}
		if (!EnrollmentCmsFileTransferLog.ReportType.ANNUAL.toString().equalsIgnoreCase(reportType)
				&& EnrollmentConstants.STATE_CODE_ID.equalsIgnoreCase(EnrollmentConfiguration.returnStateCode())) {
			copyToExchgFolder(transferLogList, reportType, EnrollmentConstants.TRANSFER_DIRECTION_IN, batchExecutionId);
		}
		EnrollmentSftpUtil.deleteByFilename(sftpParameters, fileListToDelete, hostLocation, enrollmentGIMonitorUtil);
		sendYhiAlertEmail(transferLogList, reportType, EnrollmentConstants.TRANSFER_DIRECTION_IN);
	}
	

	/**
	 * Copy files to internal YHI folder
	 * @param transferLogList
	 * @param reportType
	 * @param direction
	 * @param batchExecutionId 
	 */
	private void copyToExchgFolder(List<EnrollmentCmsFileTransferLog> transferLogList, String reportType,
			String direction, Long batchExecutionId) {
		String srcFilePath = null;
		boolean isInbound = EnrollmentConstants.TRANSFER_DIRECTION_IN.equalsIgnoreCase(direction);
		Map<String, String> sftpParameters = getExchgSftpParameters();
		String basePath =	EnrollmentUtils.getExchgReportingBasePathBuilderByTypeAndDirection(reportType, direction, EXCHG_SFTP_FOLDERPATH).toString();
		List<String> fileSrcPathList = new ArrayList<String>();
		for(EnrollmentCmsFileTransferLog fileInfo : transferLogList){
			if(EnrollmentConstants.SUCCESS.equalsIgnoreCase(fileInfo.getStatus())){
				srcFilePath = fileInfo.getSourceDirectory();
				if(isInbound) {
					srcFilePath = fileInfo.getTargetDirectory();
				} 						
				fileSrcPathList.add(srcFilePath + File.separator + fileInfo.getFileName());
			}
		}
		try {
			String response = EnrollmentSftpUtil.uploadFilesByFileNames(sftpParameters, fileSrcPathList, basePath, enrollmentGIMonitorUtil);
			List<EnrollmentSftpFileTransferDTO> yhifileTransferList = platformGson.fromJson(response, listType);
			for(EnrollmentSftpFileTransferDTO yhiDto : yhifileTransferList){
				List<EnrollmentCmsFileTransferLog> transferLogListByName  = enrollmentCmsFileTransferLogRepository.getTransferLogByFileNameAndExecutionId(yhiDto.getFileName(), batchExecutionId);
				if(null != transferLogListByName && !transferLogListByName.isEmpty() && null != transferLogListByName.get(0) ){
					EnrollmentCmsFileTransferLog transferLog = transferLogListByName.get(0);
					transferLog.setExchgTransferStatus(yhiDto.getTransferStatus());
					transferLog.setExchgFailureMsg(yhiDto.getErrorMsg());
					enrollmentCmsFileTransferLogRepository.saveAndFlush(transferLog);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Unable to copy files to YHI directory: "+ fileSrcPathList, e);
		}

	}
	
	private Map<String, String> getExchgSftpTestParameters() {
		Map<String, String> sftpParameters = new HashMap<String, String>();
		sftpParameters.put(EnrollmentSftpUtil.SFTP_HOST, "idnextqa.ghixqa.com");
		sftpParameters.put(EnrollmentSftpUtil.SFTP_USERNAME, EXCHG_SFTP_USERNAME);
		sftpParameters.put(EnrollmentSftpUtil.SFTP_PASSWORD, EXCHG_SFTP_PASSWORD);
		sftpParameters.put(EnrollmentSftpUtil.SFTP_PORT, "22");
		return sftpParameters;
	}
	
	public Map<String, String> getExchgSftpParameters() {
		Map<String, String> sftpParameters = new HashMap<String, String>();
		sftpParameters.put(EnrollmentSftpUtil.SFTP_HOST, EXCHG_SFTP_URL);
		sftpParameters.put(EnrollmentSftpUtil.SFTP_USERNAME, EXCHG_SFTP_USERNAME);
		sftpParameters.put(EnrollmentSftpUtil.SFTP_PASSWORD, EXCHG_SFTP_PASSWORD);
		sftpParameters.put(EnrollmentSftpUtil.SFTP_PORT, EXCHG_SFTP_PORT);
		return sftpParameters;
	}

	/**
	 * Send YHI alert mail after each upload and download
	 * @param transferLogList
	 * @param reportType
	 * @param transferDirection
	 */
	private synchronized void sendYhiAlertEmail(List<EnrollmentCmsFileTransferLog> transferLogList, String reportType,
			String transferDirection) {
		
		Map<String, String> emailDataMap=new HashMap<>();
		Calendar cal = Calendar.getInstance();
		String direction = null;
		StringBuffer subjectBuilder = new StringBuffer();
		emailDataMap.put("reportType", reportType);
		switch(transferDirection){
		case EnrollmentConstants.TRANSFER_DIRECTION_OUT:
			emailDataMap.put("transferDirection", "Outbound");
			emailDataMap.put("direction", "sent to");
			emailDataMap.put("subjectDirection", "sent");	
			direction =  "Outbound";
			break;
		case EnrollmentConstants.TRANSFER_DIRECTION_IN:
			emailDataMap.put("transferDirection", "Inbound");
			emailDataMap.put("direction", "received from");
			emailDataMap.put("subjectDirection", "received");			
			direction =  "Inbound";
			break;
		}
		emailDataMap.put("month", String.format("%02d", cal.get(Calendar.MONTH)+1));
		emailDataMap.put("year", String.valueOf(cal.get(Calendar.YEAR)));
		emailDataMap.put("date", cal.getTime().toString());
		subjectBuilder.append(emailDataMap.get("reportType")).append(SPACE).append(emailDataMap.get("transferDirection"))
				.append(SPACE).append("for").append(SPACE).append(String.format("%02d", cal.get(Calendar.MONTH) + 1)).append("/")
				.append(String.valueOf(cal.get(Calendar.YEAR))).append(SPACE)
				.append(emailDataMap.get("subjectDirection"));
		String tdPrefix = "<td style='border:1px solid #000;'>";
		String tdPostfix = "</td>";
		boolean sendMail = false;
		
		StringBuffer tableContentBuilder = new StringBuffer();
		for(EnrollmentCmsFileTransferLog fileInfo : transferLogList){
			if(EnrollmentConstants.SUCCESS.equalsIgnoreCase(fileInfo.getStatus())){
				tableContentBuilder.append("<tr>");
				tableContentBuilder.append(tdPrefix).append(fileInfo.getFileName()).append(tdPostfix);
				tableContentBuilder.append(tdPrefix).append(fileInfo.getFileSize()).append(tdPostfix);
				tableContentBuilder.append(tdPrefix).append(reportType).append(tdPostfix);
				tableContentBuilder.append(tdPrefix).append(direction).append(tdPostfix);
				tableContentBuilder.append(tdPrefix).append(String.format("%02d", cal.get(Calendar.MONTH) + 1))
						.append("/").append(cal.get(Calendar.YEAR)).append(tdPostfix);
				tableContentBuilder.append("</tr>");
				sendMail = true;
			}
		}
		emailDataMap.put("tableContent", tableContentBuilder.toString());
		emailDataMap.put("Subject", subjectBuilder.toString());
		if(sendMail){
			try {
				cmsSftpNotificationService.sendYhiAlertEmail(emailDataMap);
			} catch (GIException e) {
				LOGGER.debug("Error sending staging email notification :: ", e);
			}
		}
	}
	
	private String convertPropertyValue(final String originalValue){
		if(StringUtils.isNotBlank(originalValue) && 
				originalValue.startsWith(ENC_START_TAG) && originalValue.endsWith(ENC_END_TAG)){
			try {
				return GhixAESCipherPool.decrypt(originalValue.substring(ENC_START_TAG.length(),
						(originalValue.length() - ENC_END_TAG.length())));
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| InvalidKeySpecException | NoSuchPaddingException
					| InvalidAlgorithmParameterException
					| UnsupportedEncodingException | IllegalBlockSizeException
					| BadPaddingException e) {
				throw new GIRuntimeException("CMS SFTP: Failed to decrypt the property ["+originalValue+"]"+e.getMessage(),e);
			}
		}
		return originalValue;
	}

	@Override
	public void checkConnection(){
		try {
			EnrollmentSftpUtil.checkConnection(getSftpParameters());	
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
}
