<!-- <script type="text/javascript" language="JavaScript">
  $(document).ready(function() {
    if (parseInt($('#dependents').text()) < 0) {
      alert("You can not have negative depedents to add")
    }
  })
</script> -->

<script type="text/ng-template" id="numofdep-add.jsp">
  <div class="gutter10" ng-controller="noOfDependentsAdd"  ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold">
    <spring:message code="label.lce.numofdep.p1"/>
    <div class="gutter10">
      <div class="gutter10">
        <table id="" class="table table-striped">
            <tbody>
  	          <tr>
  	            <td><spring:message code="label.lce.firstname"/></td>
  	            <td><spring:message code="label.lce.lastname"/></td>
  	            <td><spring:message code="label.lce.relationship"/></td>
  	            <td><spring:message code="label.lce.seekingcoverage"/>?</td>
  	          </tr>
  	          <tr ng-repeat="householdMember in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
  	          <td>{{householdMember.name.firstName}}</td>
  	          <td>{{householdMember.name.lastName}}</td>
  	          <td>
  	            <div ng-repeat="relationship in householdMember.bloodRelationship">
  					      {{getRelation(relationship.relationshipCode)}}
  					    </div>
  	          </td>
  						<td>
  						  <label class="radio inline" for="applyingforhealthcare">
  						    <input id="seekingCoverageTrue{{$index}}" name="seekingCoverage{{$index}}" type="radio" value="true" ng-value="true" ng-model="jsonObject.singleStreamlinedApplication.taxHousehold[$parent.$index].householdMember[$index].applyingForCoverageIndicator" />
                  <label for="seekingCoverageTrue{{$index}}"><spring:message code="label.lce.yes"/></label>
  						  </label>
  						  <label class="radio inline" for="applyingforhealthcare">
  						    <input id="seekingCoverageFalse{{$index}}" name="seekingCoverage{{$index}}" type="radio" value="false" ng-value="false" ng-model="jsonObject.singleStreamlinedApplication.taxHousehold[$parent.$index].householdMember[$index].applyingForCoverageIndicator" />
                  <label for="seekingCoverageFalse{{$index}}" ><spring:message code="label.lce.no"/></label>
  						  </label>
  						</td>
  	        </tr>
          </tbody>
        </table>
        <div class="gutter10" style="background: #F7F7F7;">
          <span class="margin20-l margin10-t margin20-r">
            <spring:message code="label.lce.specifythenumberofdependentsyouwantoadd"/>:
          </span>
          <input type="text" class="span1 margin20-l margin10-t" ng-model="numDependents"></input>
        </div>
      </div>
    </div>

    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a class="btn btn-primary" ng-click="addDep(numDependents); next()"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>