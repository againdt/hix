package com.getinsured.iex.shop.mec.wrapper;

import java.text.ParseException;

import javax.xml.datatype.DatatypeConfigurationException;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.DatePeriodType;

public class DatePeriodTypeWrapper implements JSONAware{

	private String startDate;
    private String endDate;
    private static final String DATE_FORMAT = "MM/dd/yyyy";
	public DatePeriodTypeWrapper(DatePeriodType datePeriodType) throws HubServiceException{
		if(datePeriodType != null){
			try {
				startDate =  PlatformServiceUtil.xmlDateToString(datePeriodType.getStartDate(), DATE_FORMAT);
				endDate =  PlatformServiceUtil.xmlDateToString(datePeriodType.getEndDate(), DATE_FORMAT);
			} catch (ParseException | DatatypeConfigurationException e) {
				throw new HubServiceException(e.getMessage(), e);
			}
		}
		
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("StartDate", this.startDate);
		obj.put("EndDate", this.endDate);
		return obj.toJSONString();
	}
}
