module.exports = function(grunt) {

	  // Project configuration.
	  grunt.initConfig({

	    // Build the site using grunt-includes
	    includes: {
	      build: {
	        cwd: 'notificationTemplate/mn/staticTemplates',   // change the cwd according to the static template directory added
	        src: [ '*.html' ],
	        dest: 'notificationTemplate/mn/',    // change destination dir according to the state 
	        options: {
	          flatten: true
	        }
	      }
	    }
	  });

	  // Load plugins used by this task gruntfile
	  grunt.loadNpmTasks('grunt-includes');
	  //grunt.loadNpmTasks('grunt-contrib-clean');

	  // Task definitions
	  grunt.registerTask('build', ['includes']);
	  grunt.registerTask('default', ['build']);
	};