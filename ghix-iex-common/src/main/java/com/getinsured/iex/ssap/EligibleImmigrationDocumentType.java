
package com.getinsured.iex.ssap;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author chopra_s
 */
public class EligibleImmigrationDocumentType {

  @JsonProperty("DS2019Indicator")
  @SerializedName("DS2019Indicator")
  @Expose
  private Boolean dS2019Indicator = false;

  @JsonProperty("UnexpiredForeignPassportIndicator")
  @SerializedName("UnexpiredForeignPassportIndicator")
  @Expose
  private Boolean unexpiredForeignPassportIndicator = false;

  @JsonProperty("TemporaryI551StampIndicator")
  @SerializedName("TemporaryI551StampIndicator")
  @Expose
  private Boolean temporaryI551StampIndicator = false;

  @JsonProperty("I551Indicator")
  @SerializedName("I551Indicator")
  @Expose
  private Boolean i551Indicator = false;

  @JsonProperty("MachineReadableVisaIndicator")
  @SerializedName("MachineReadableVisaIndicator")
  @Expose
  private Boolean machineReadableVisaIndicator = false;

  @JsonProperty("OtherDocumentTypeIndicator")
  @SerializedName("OtherDocumentTypeIndicator")
  @Expose
  private Boolean otherDocumentTypeIndicator = false;

  @JsonProperty("I94InPassportIndicator")
  @SerializedName("I94InPassportIndicator")
  @Expose
  private Boolean i94InPassportIndicator = false;

  @JsonProperty("I571Indicator")
  @SerializedName("I571Indicator")
  @Expose
  private Boolean i571Indicator = false;

  @JsonProperty("I766Indicator")
  @SerializedName("I766Indicator")
  @Expose
  private Boolean i766Indicator = false;

  @JsonProperty("I20Indicator")
  @SerializedName("I20Indicator")
  @Expose
  private Boolean i20Indicator = false;

  @JsonProperty("I94Indicator")
  @SerializedName("I94Indicator")
  @Expose
  private Boolean i94Indicator = false;

  @JsonProperty("I797Indicator")
  @SerializedName("I797Indicator")
  @Expose
  private Boolean i797Indicator = false;

  @JsonProperty("I327Indicator")
  @SerializedName("I327Indicator")
  @Expose
  private Boolean i327Indicator = false;

  @Expose
  private boolean noneOfThese = false;

  public Boolean getDS2019Indicator() {
    return dS2019Indicator;
  }

  public void setDS2019Indicator(Boolean dS2019Indicator) {
    this.dS2019Indicator = dS2019Indicator;
  }

  public Boolean getUnexpiredForeignPassportIndicator() {
    return unexpiredForeignPassportIndicator;
  }

  public void setUnexpiredForeignPassportIndicator(Boolean unexpiredForeignPassportIndicator) {
    this.unexpiredForeignPassportIndicator = unexpiredForeignPassportIndicator;
  }

  public Boolean getTemporaryI551StampIndicator() {
    return temporaryI551StampIndicator;
  }

  public void setTemporaryI551StampIndicator(Boolean temporaryI551StampIndicator) {
    this.temporaryI551StampIndicator = temporaryI551StampIndicator;
  }

  public Boolean getI551Indicator() {
    return i551Indicator;
  }

  public void setI551Indicator(Boolean i551Indicator) {
    this.i551Indicator = i551Indicator;
  }

  public Boolean getMachineReadableVisaIndicator() {
    return machineReadableVisaIndicator;
  }

  public void setMachineReadableVisaIndicator(Boolean machineReadableVisaIndicator) {
    this.machineReadableVisaIndicator = machineReadableVisaIndicator;
  }

  public Boolean getOtherDocumentTypeIndicator() {
    return otherDocumentTypeIndicator;
  }

  public void setOtherDocumentTypeIndicator(Boolean otherDocumentTypeIndicator) {
    this.otherDocumentTypeIndicator = otherDocumentTypeIndicator;
  }

  public Boolean getI94InPassportIndicator() {
    return i94InPassportIndicator;
  }

  public void setI94InPassportIndicator(Boolean i94InPassportIndicator) {
    this.i94InPassportIndicator = i94InPassportIndicator;
  }

  public Boolean getI571Indicator() {
    return i571Indicator;
  }

  public void setI571Indicator(Boolean i571Indicator) {
    this.i571Indicator = i571Indicator;
  }

  public Boolean getI766Indicator() {
    return i766Indicator;
  }

  public void setI766Indicator(Boolean i766Indicator) {
    this.i766Indicator = i766Indicator;
  }

  public Boolean getI20Indicator() {
    return i20Indicator;
  }

  public void setI20Indicator(Boolean i20Indicator) {
    this.i20Indicator = i20Indicator;
  }

  public Boolean getI94Indicator() {
    return i94Indicator;
  }

  public void setI94Indicator(Boolean i94Indicator) {
    this.i94Indicator = i94Indicator;
  }

  public Boolean getI797Indicator() {
    return i797Indicator;
  }

  public void setI797Indicator(Boolean i797Indicator) {
    this.i797Indicator = i797Indicator;
  }

  public Boolean getI327Indicator() {
    return i327Indicator;
  }

  public void setI327Indicator(Boolean i327Indicator) {
    this.i327Indicator = i327Indicator;
  }

  public boolean isNoneOfThese() {
    return noneOfThese;
  }

  public void setNoneOfThese(boolean noneOfThese) {
    this.noneOfThese = noneOfThese;
  }
}
