package com.getinsured.hix.batch.referral;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.context.annotation.DependsOn;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@DependsOn("dynamicPropertiesUtil")
public abstract class BaseLCEReminderNotificationTask extends StepExecutionListenerSupport implements Tasklet {
	public Date targetDate;
	public Long applicationId;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BaseLCEReminderNotificationTask.class);


	@Override
	public void beforeStep(StepExecution stepExecution) {
		JobParameters jobParameters = stepExecution.getJobParameters();
		String targetDateStr = jobParameters.getString("target_date");
		targetDate = targetDateStr != null ? getDate(targetDateStr) : new Date();
		if (targetDate == null) {
			throw new GIRuntimeException("Target Date is not valid - "+targetDateStr);
		}
		String applicationIdStr = jobParameters.getString("application_id");
		if (applicationIdStr != null) {
			applicationId = getApplicationId(applicationIdStr);
			if (applicationId == null) {
				throw new GIRuntimeException("SSAP Application Id is not valid -"+applicationIdStr);
			}
		} else {
			applicationId = null;
		}
	}

	public Date getDate(String startDatestr) {
		if (StringUtils.isNotBlank(startDatestr)) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				return formatter.parse(startDatestr);
			} catch (Exception e) {
				LOGGER.error("Jb Parameter Target Date is invalid ");
				return null;
			}
		}
		return null;

	}

	public Long getApplicationId(String AppIdstr) {
		if (StringUtils.isNotBlank(AppIdstr)) {
			try {
				return new Long(AppIdstr);
			} catch (Exception e) {
				LOGGER.error("Jb Parameter Application Id is invalid ");
				return null;
			}
		}
		return null;

	}
}
