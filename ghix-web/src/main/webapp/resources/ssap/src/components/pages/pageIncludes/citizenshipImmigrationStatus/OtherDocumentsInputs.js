import React, { Fragment } from 'react';
import _ from 'lodash';

// Components

import UniversalInput from '../../../../lib/UniversalInput';

// Constants

import INPUT_TYPES from '../../../../constants/inputTypes';
import { OTHER_IMMIGRATION_DOCUMENTS } from '../../../../constants/otherImmigrationDocuments';



const OtherDocumentsInputs = props => {

    const {
        otherImmigrationDocumentType,
        currentData,
        ...restProps
    } = props;

    const cleanedRestProps = _.omit(restProps, 'children');

    const getContent = () => {
        return OTHER_IMMIGRATION_DOCUMENTS.map(ev => {

            const {
                text,
                name
            } = ev;

            const value = name === 'noneOfTheseOther' ?
                otherImmigrationDocumentType.noneOfThese :
                otherImmigrationDocumentType[name];

            return (
                <UniversalInput
                    key={text}
                    label={{
                        text: text
                    }}
                    inputData={{
                        isInputRequired: false,
                        type: INPUT_TYPES.CHECKBOX,
                        name: name,
                        classes:['disp-styl'],
                        checked: value,
                        value: value,
                        currentData: currentData,
                        fields: ['citizenshipImmigrationStatus', 'otherImmigrationDocumentType']
                    }}
                    {...cleanedRestProps}
                />
            );
        });
    };

    return (
        <Fragment>
            { getContent() }
        </Fragment>
    );
};

export default OtherDocumentsInputs;