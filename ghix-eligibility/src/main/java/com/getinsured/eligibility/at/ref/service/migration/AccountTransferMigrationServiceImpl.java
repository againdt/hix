package com.getinsured.eligibility.at.ref.service.migration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.dto.AccountTransferResponseDTO;
import com.getinsured.eligibility.at.ref.common.AccountTransferCategoryEnum;
import com.getinsured.eligibility.at.ref.dto.ApplicationsWithSameIdDTO;
import com.getinsured.eligibility.at.resp.service.SsapApplicantService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.AtResponseTypeEnum;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferResponsePayloadType;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

@Service
@DependsOn("dynamicPropertiesUtil")
@Transactional
public class AccountTransferMigrationServiceImpl extends BaseDeterminationClassMigration implements AccountTransferMigrationService  {
	
	private static final Logger LOGGER = Logger.getLogger(AccountTransferMigrationServiceImpl.class);
	
	private static final String PROCESS_INITIAL_REFERRAL = "/processOEReferralMigration";
	private static final String PROCESS_QE_REFERRAL = "/processQEReferralMigration";
	private static final String PROCESS_LCE_REFERRAL = "/processLceReferralMigration";
	
	@Autowired 
	private GIWSPayloadService giwsPayloadService;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private SsapApplicantService ssapApplicantService;
	@Autowired
	private MigrationUtil migrationUtil;
	
	@Value("#{configProp['ghixEligibilityServiceURL']}")
	private String erpSvcUrl;
	
	@Override
	public void process(AccountTransferRequestPayloadType request, AccountTransferResponseDTO result, String enrollmentIds) throws Exception {
		GIWSPayload giwsPayload = giwsPayloadService.save(result.getGiwsPayload());
		Integer giwsPayloadId = giwsPayload.getId();

		AccountTransferResponsePayloadType response = result.getAccountTransferResponsePayloadType();
		String responseCode = response.getResponseMetadata().getResponseCode().getValue();

		if (StringUtils.equalsIgnoreCase(responseCode, EligibilityConstants.HS000000)){
			migrationUtil.createAccountTransferMigration(request, enrollmentIds);
			invokeReferralProcess(populateAccountTransferDTO(request, giwsPayloadId));
		}
	}
	
	private void invokeReferralProcess(AccountTransferRequestDTO accountTransferRequestDTO) {
		LOGGER.info("Invoking Referral Process : " + accountTransferRequestDTO.getProcessToBeExecuted());
		ghixRestTemplate.exchange(erpSvcUrl + accountTransferRequestDTO.getProcessToBeExecuted(), ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_XML, String.class, accountTransferRequestDTO);
	}
	
	private AccountTransferRequestDTO populateAccountTransferDTO(AccountTransferRequestPayloadType request, Integer giwsPayloadId){
		
		final long coverageYear = ReferralUtil.extractCoverageYear(request.getInsuranceApplication().getApplicationExtension());
		
		AccountTransferRequestDTO accountTransferRequestDTO = new AccountTransferRequestDTO();
		accountTransferRequestDTO.setAccountTransferRequestPayloadType(request);
		accountTransferRequestDTO.setGiwsPayloadId(giwsPayloadId);
		accountTransferRequestDTO.setFullDetermination(true);
		accountTransferRequestDTO.setNotificationType(ReferralConstants.WITHOUT_LINK);
		accountTransferRequestDTO.setCoverageYear(coverageYear);
		accountTransferRequestDTO.setAtResponseType(AtResponseTypeEnum.UPDATED.toString());
		
		List<SsapApplication> ssapApplications = null ;
		
		final String useExternalId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_HOUSEHOLD_CASE_ID_ENABLE);
		if(null != useExternalId && "TRUE".equalsIgnoreCase(useExternalId)) {
			List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes =request.getInsuranceApplication().getApplicationIdentification();
			String hhCaseId = null ;
			for(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType : iTypes) {
				if(  ReferralConstants.HOUSE_HOLD_CASE_ID.equalsIgnoreCase( tIdentificationType.getIdentificationCategoryText( ).getValue()  )   ) {
					hhCaseId = tIdentificationType.getIdentificationID().getValue();
					break;
				}
			}
			ssapApplications = ssapApplicantService.findSsapApplicationForHHCaseId(hhCaseId);
		}
		
		processLogicToDetermine(accountTransferRequestDTO, ssapApplications);
		
		populateProcessToBeExecuted(accountTransferRequestDTO);
		 
		return accountTransferRequestDTO;
	}
	
	void processLogicToDetermine(AccountTransferRequestDTO accountTransferRequestDTO, List<SsapApplication> ssapApplications) {
		List<SsapApplication> applicationsToBeClosed = new ArrayList<SsapApplication>();
		List<SsapApplication> applicationsThatAreEnrolled = new ArrayList<SsapApplication>();
		List<SsapApplication> otherYearApplications = new ArrayList<SsapApplication>();
		List<SsapApplication> sameYearApplications = new ArrayList<SsapApplication>();
		List<ApplicationsWithSameIdDTO> applicationsWithSameId = new ArrayList<ApplicationsWithSameIdDTO>();
		Set<BigDecimal> enrolledAppCmrIds = new HashSet<>();
		Set<BigDecimal> allAppCmrIds = new HashSet<>();
		boolean isLCETrue = false;
		boolean bCheck;
		boolean hasEnrolledApplication = false;
		boolean isQEDetermined = false;
		long enrolledApplicationId = 0l;
		
		for (SsapApplication ssapApplication : ssapApplications) {
			ApplicationsWithSameIdDTO fieldData = populateDataInDto(ssapApplication);
			if (ReferralUtil.convertToInt(ssapApplication.getCmrHouseoldId()) != ReferralConstants.NONE) {
				allAppCmrIds.add(ssapApplication.getCmrHouseoldId());
			}
			if (ssapApplication.getCoverageYear() == accountTransferRequestDTO.getCoverageYear()) {
				sameYearApplications.add(ssapApplication);
				if (ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus()) ||
						ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus())	) {
					bCheck = isEnrollmentActive(ssapApplication);
					fieldData.setEnrolledActive(bCheck);
					if (bCheck) {
						hasEnrolledApplication = true;
						isLCETrue = true;
						isQEDetermined = false;
						applicationsThatAreEnrolled.add(ssapApplication);
						enrolledAppCmrIds.add(ssapApplication.getCmrHouseoldId());
					} else {
						if (!hasEnrolledApplication) {
							isQEDetermined = true;
						}
					}
				} else if (!ApplicationStatus.CLOSED.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus())
				        && !ApplicationStatus.CANCELLED.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus())) {
					applicationsToBeClosed.add(ssapApplication);
				}
			} else {
				otherYearApplications.add(ssapApplication);
			}
			applicationsWithSameId.add(fieldData);
		}

		accountTransferRequestDTO.setApplicationsWithSameId(applicationsWithSameId);

		if (ReferralUtil.listSize(applicationsToBeClosed) != ReferralConstants.NONE) {
			closeApplications(applicationsToBeClosed);
		}

		if (isLCETrue) {
			enrolledApplicationId = retrieveComparedApplicationId(applicationsThatAreEnrolled, enrolledAppCmrIds);
			LOGGER.info("Lce Comparison to be done with Enrolled Application  - " + enrolledApplicationId);
			
			migrationUtil.updateATMigrationByTransferId(accountTransferRequestDTO, enrolledApplicationId, true, null, null, null);
			
			accountTransferRequestDTO.setEnrolledApplicationId(enrolledApplicationId);
			accountTransferRequestDTO.setCompareToApplicationId(enrolledApplicationId);
			accountTransferRequestDTO.setLCE(true);
			accountTransferRequestDTO.setAccountTransferCategory(AccountTransferCategoryEnum.LCE.value());
		} else if (isQEDetermined || isQEPeriod(accountTransferRequestDTO)) {
			accountTransferRequestDTO.setQE(true);
			accountTransferRequestDTO.setAccountTransferCategory(AccountTransferCategoryEnum.QE.value());
			if (ReferralUtil.listSize(sameYearApplications) != ReferralConstants.NONE) {
				accountTransferRequestDTO.setCompareToApplicationId(retrieveComparedToApplication(sameYearApplications, accountTransferRequestDTO.getCoverageYear()));
				LOGGER.info("QE Comparison to be done with Application  - " + accountTransferRequestDTO.getCompareToApplicationId());
			}
		} else {
			accountTransferRequestDTO.setAccountTransferCategory(AccountTransferCategoryEnum.OE.value());
			if (ReferralUtil.collectionSize(allAppCmrIds) <= ReferralConstants.ONE && isOEPeriod(accountTransferRequestDTO)) {
				long coverageYear = accountTransferRequestDTO.getCoverageYear();
				List<SsapApplication> tmpYearApplications = new ArrayList<SsapApplication>();
				
				if (ReferralUtil.listSize(sameYearApplications) != ReferralConstants.NONE) {
					coverageYear = accountTransferRequestDTO.getCoverageYear();
					tmpYearApplications.addAll(sameYearApplications);
				} else if (ReferralUtil.listSize(otherYearApplications) != ReferralConstants.NONE) {
					coverageYear = accountTransferRequestDTO.getCoverageYear() - ReferralConstants.ONE;
					tmpYearApplications.addAll(otherYearApplications);
				}
				
				accountTransferRequestDTO.setCompareToApplicationId(retrieveComparedToApplication(tmpYearApplications, coverageYear));
				LOGGER.info("OE Comparison to be done with Application  - " + accountTransferRequestDTO.getCompareToApplicationId());
			}
		}
	}
	
	private void populateProcessToBeExecuted(AccountTransferRequestDTO accountTransferRequestDTO) {
		if (accountTransferRequestDTO.isLCE()) {
			LOGGER.info(PROCESS_LCE_REFERRAL);
			accountTransferRequestDTO.setProcessToBeExecuted(PROCESS_LCE_REFERRAL);
		} else if (accountTransferRequestDTO.isQE()) {
			LOGGER.info(PROCESS_QE_REFERRAL);
			accountTransferRequestDTO.setProcessToBeExecuted(PROCESS_QE_REFERRAL);
		} else {
			LOGGER.info(PROCESS_INITIAL_REFERRAL);
			accountTransferRequestDTO.setProcessToBeExecuted(PROCESS_INITIAL_REFERRAL);
		}
	}
}
