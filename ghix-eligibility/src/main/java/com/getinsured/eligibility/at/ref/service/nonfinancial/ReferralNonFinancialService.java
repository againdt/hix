package com.getinsured.eligibility.at.ref.service.nonfinancial;

import com.getinsured.eligibility.at.ref.dto.NFProcessDTO;

/**
 * @author chopra_s
 * 
 */
public interface ReferralNonFinancialService {
	boolean execute(NFProcessDTO nfProcessDTO) throws Exception;
}
