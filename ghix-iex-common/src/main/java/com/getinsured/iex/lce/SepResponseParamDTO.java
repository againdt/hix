package com.getinsured.iex.lce;

import com.google.gson.JsonObject;

public class SepResponseParamDTO {

	private String jsonObject;
	private Boolean isNameChangedForPrimary;
	private String primaryFirstName;
	private String primaryLastName;
	private String primaryMiddleName;
	
	public String getJsonObject() {
		return jsonObject;
	}
	public void setJsonObject(String jsonObject) {
		this.jsonObject = jsonObject;
	}
	public Boolean getIsNameChangedForPrimary() {
		return isNameChangedForPrimary;
	}
	public void setIsNameChangedForPrimary(Boolean isNameChangedForPrimary) {
		this.isNameChangedForPrimary = isNameChangedForPrimary;
	}
	public String getPrimaryFirstName() {
		return primaryFirstName;
	}
	public void setPrimaryFirstName(String primaryFirstName) {
		this.primaryFirstName = primaryFirstName;
	}
	public String getPrimaryLastName() {
		return primaryLastName;
	}
	public void setPrimaryLastName(String primaryLastName) {
		this.primaryLastName = primaryLastName;
	}
	public String getPrimaryMiddleName() {
		return primaryMiddleName;
	}
	public void setPrimaryMiddleName(String primaryMiddleName) {
		this.primaryMiddleName = primaryMiddleName;
	}

}
