package com.getinsured.ssap.model.financial.nmec;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.ssap.model.hub.vlp.response.ResponseMetadata;

/**
 * Describes NMEC error message details.
 *
 * @author Suresh Kancherla
 * @since 2019-08-19
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NonEsiMecErrorMessageDetail {
  @JsonProperty("XPathContent")
  private String xPathContent;

  @JsonProperty("ResponseMetadata")
  private ResponseMetadata responseMetadata;

  public String getxPathContent() {
    return xPathContent;
  }

  public void setxPathContent(String xPathContent) {
    this.xPathContent = xPathContent;
  }

  public ResponseMetadata getResponseMetadata() {
    return responseMetadata;
  }

  public void setResponseMetadata(ResponseMetadata responseMetadata) {
    this.responseMetadata = responseMetadata;
  }

  @Override
  public String toString() {
    return "NonEsiMecErrorMessageDetail{" +
        "xPathContent='" + xPathContent + '\'' +
        ", responseMetadata=" + responseMetadata +
        '}';
  }
}
