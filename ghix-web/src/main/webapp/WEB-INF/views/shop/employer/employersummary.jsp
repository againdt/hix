<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld" %>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/style.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

	<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != 'false'}">
				<c:out value="${errorMsg}"></c:out>
			</c:if>
		</div>
	</div>
	

<!--  Latest UI -->
	
<div class="gutter10">

	<c:if test="${message != null}">
	<div class="errorblock alert alert-info">
		<p>${message}</p>
	</div>
	</c:if>

		<h1>${employer.name} <small>&nbsp;Contact Name: ${employer.contactFirstName} ${employer.contactLastName}</small></h1>
	<div class="row-fluid">
			<div class="span3">
				<div class="gray">
					<h4 class="margin0">About this Employer</h4>
						<ul class="nav nav-list">
	<%-- 			                  <li class="active"><a href="<c:url value="/shop/employer/employercasesummary/${employer.id}"/>">Summary</a></li> --%>
	<%-- 			                  <li><a href="<c:url value="/shop/employer/employercasecontactinfo/${employer.id}"/>">Contact Info</a></li> --%>
							<li class="active"><a href="#">Summary</a></li>
		                 	<li><a href="/hix/shop/employer/employercontactinfo/${employer.id}">Contact Info</a></li>
		                 	<li>
		                  		<c:url value="/inbox/secureInboxSearch" var="searchURL">
								   <c:param name="searchText" value="${employer.name}"/>
								</c:url>
								<a href="${searchURL}">Messages</a>
							</li>
		                  <!-- 	<li><a href="/hix/shop/employer/composemessage/${employer.id}">Notifications</a></li> -->
		                  	<li><a href="/hix/shop/employer/employercomments/${employer.id}">Comments</a></li>
		                </ul>
				</div>
				<br>
				<div class="gray">
					<h4 class="margin0"><i class="icon-cog icon-white"></i> Actions</h4>
						<ul class="nav nav-list">
		                  <li><a href="#viewempModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> View Employer Account</a></li>
		                   <li><a href="#new-msg" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();">Compose Message</a></li>
		                  <li><a href="#"><i class="icon-comment"></i> New Comment</a></li>
		                </ul>
				</div>
		</div>	
		<!-- Modal -->
		
		<div id="rightpanel" class="span9">
		
			<form class="form-horizontal" id="" name="" action="" method="POST">				
				<df:csrfToken/>
				<table class="table">
					<thead>
						<tr class="graydrkbg">
							<th class="span3"><strong>Summary</strong></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="txt-right">Business Name</td>
							<td><strong>${employer.name}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">EIN</td>
							<td><strong>${employer.federalEIN}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Corporate Type</td>
							<td><strong>${employer.orgType}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Total Employees</td>
							<td><strong>${employer.totalEmployees}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Average Salary</td>
							<td><strong>${avgSal}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Locations</td>
							<td><strong>${fn:length(employer.locations)}</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Primary Worksite Address</td>
							<td><strong>
													${employer.locations[0].location.address1}  <br />
													${employer.locations[0].location.address2} <br />
													${employer.locations[0].location.city} <br />
													${employer.locations[0].location.state} <br />
													${employer.locations[0].location.zip} 
													</strong></td>
						</tr>
						<tr>
							<td class="txt-right">Worksite Address 2</td>
							<td><strong>
													${employer.locations[1].location.address1}  <br />
													${employer.locations[1].location.address2} <br />
													${employer.locations[1].location.city} <br />
													${employer.locations[1].location.state} <br />
													${employer.locations[1].location.zip} 
													</strong>
						</tr>
						<tr>
							<td class="txt-right">Benchmark Plan</td>
							<td><strong></strong></td>
						</tr>
						<tr>
							<td class="txt-right">Eligibility Status</td>
							<td><strong></strong></td>
						</tr>
					</tbody>
				</table>
		</form>	

		</div>
		</div>	
	</div>	
		
		 
		<!-- Modal -->
		<div id="viewempModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 id="myModalLabel">View Employer Account</h3>
			</div>
			<div class="modal-body">
			<p>Clicking &#34;Employer View&#34; will take you to the Employer&#39;s portal for &#34; &#60;${employer.name}&#62; &#34;.
				Through this portal you will be able to take actions on behalf of this employer, such as view and edit employee list, fill out employer eligibility, etc.
			</p>
			<p>
				Proceed to Employer view?
			</p>
			</div>
			<div class="modal-footer">
				<label class="checkbox pull-left">
					<input type="checkbox" value="">
					Don&#39;t show this message again.
				</label>
			<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				 <a class="btn btn-primary""  type="button"  href="<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>">Employer View</a>
			
			</div>
		</div>
<!--  Latest UI -->