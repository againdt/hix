﻿define(function (required) {
    var textbox = new baseComponent();
    textbox.type = "textbox";
    textbox.getHTML = function (cgfObj) {
        var defCfg = {
            etype: "text",
            colRatio:"1:1",
            editable: true,
            visible: true,
            disabled: false,
            requiredMessage: cgfObj.caption +" required"
        };
        cgfObj = $.extend(defCfg, cgfObj);
        var compData = cgfObj;       
        var $retobj;
        var retStr = "";
        if (compData.editable) {
            $retobj = this.getEditable(compData);
            retStr = $retobj[0].outerHTML;
        } else {
            $retobj = this.getNonEditable(compData);
            retStr = $retobj[0].outerHTML;
        };
        return retStr;
    };
    textbox.getNonEditable=function(compData)
    {
        var maxAtEachSide = 12;
        var ratioArra = compData.colRatio.split(":");
        var totoRat = Number(ratioArra[0]) + Number(ratioArra[1]);
        var eachSize = Math.ceil(maxAtEachSide / totoRat);
        if (eachSize < 1) { eachSize = 1 };

        var leftSide = Math.ceil(eachSize * Number(ratioArra[0]));
        var rightSide = Math.ceil(eachSize * Number(ratioArra[1]));
        var $retobj = $("<div class=\"row-fluid\"></div>");
        $retobj.append("<div class=\"span" + leftSide + " noedit_label\" style=\"text-align:right;\"><strong>" + compData.caption + "</strong></div>");
        $retobj.append("<div class=\"span" + rightSide + " noedit_value\" style=\"text-align:left;\">{{" + compData.name + "}}</div>");
        if (compData.visible == false) {
            $retobj.addClass("hide");
        };
        return $retobj;
    },
    textbox.getEditable = function (compData) {
        var thisType = compData.etype;
        var $retobj = $("<div class=\"control-group\"></div>");
        var $label = $("<label class=\"control-label\" for=\"" + compData.name + "\">" + compData.caption + "</label>");
        $retobj.append($label);
        if (compData.required && compData.editable == true) {
            $label.append("<img src=\"" + baseURL + "../../images/requiredAsterix.png\" width=\"10\" height=\"10\" alt=\"Required!\" aria-hidden=\"true\">");
        };
        var $controls = $("<div class=\"controls\"></div>");
        $retobj.append($controls);

        var $textbox = $("<input ng-model=\"" + compData.name + "\" type=\"" + thisType + "\" ></input>");
        $textbox.attr("id", compData.name);
        $textbox.attr("name", compData.name);
        $textbox.addClass("k-textbox");
        $textbox.attr("placeholder", compData.caption);

        $controls.append($textbox);

        if (compData.required) {
            $textbox.attr("required", "required");
            var $errorHolder = "<span for=\"" + compData.name + "\" ng-show=\"runValidate && ##formName##." + compData.name + ".$error.required\" class=\"error\"><label class=\"error\">" + compData.requiredMessage + "</label></span>";
            $controls.append($errorHolder);
        };
        if (compData.disabled) {
            $textbox.attr("disabled", "disabled");
        };
        if (compData.visible == false) {
            $retobj.addClass("hide");
        };
        if (compData.onChange) {
            $retobj.attr("ng-change", compData.onChange);
        };
        
        
       
        return $retobj;
    };
    return textbox;
});