package com.getinsured.ssap.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.LocalDate;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.getinsured.eligibility.enums.ApplicationSource;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import com.getinsured.hix.account.service.LocationService;
import com.getinsured.hix.dto.eligibility.PersonTypeDTO;
import com.getinsured.hix.dto.eligibility.PersonTypeRequest;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.indportal.enums.SsapApplicationTypeEnum;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.ssap.Util;
import com.getinsured.iex.client.ResourceCreatorImpl;
import com.getinsured.iex.client.SsapApplicationRequest;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.dto.SsapApplication;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.ssap.*;
import com.getinsured.iex.ssap.enums.OtherStateOrFederalProgramType;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.util.IexEnumAdapterFactory;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.SsapUtil;
import com.getinsured.ssap.exception.InvalidStateException;
import com.getinsured.ssap.exception.InvalidStateType;
import com.getinsured.ssap.exception.VerificationRequiredException;
import com.getinsured.ssap.exception.VerificationType;
import com.getinsured.ssap.model.SaveRequest;
import com.getinsured.ssap.repository.ApplicantRepository;
import com.getinsured.ssap.util.BloodRelationshipCode;
import com.getinsured.ssap.util.HouseholdUtil;
import com.getinsured.ssap.util.JsonUtil;
import com.getinsured.ssap.util.SsapHelper;
import com.getinsured.timeshift.TSLocalTime;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Implementation of {@link SingleStreamlinedApplicationService}.
 *
 * <p>
 * <b>Note</b>, a lot of these code blocks are copy-pasted from old junk codebase,
 * for now I've cleaned it as much as possible, but since there is NOT a single
 * document, page, email, or note that describes how this all should work, I cannot
 * clean it up completely. Old code is obscure, illogical and totally out of touch
 * with this world. The amount of junk is mind boggling. So as I learn more I'll try
 * to revisit this and rewrite it as much as possible.
 * </p>
 *
 * @author Yevgen Golubenko
 * @since 2018-12-21
 */
@Service("singleStreamlinedApplicationService")
public class SingleStreamlinedApplicationServiceImpl implements SingleStreamlinedApplicationService {
  private static final Logger log = LoggerFactory.getLogger(SingleStreamlinedApplicationServiceImpl.class);

  private static final int FAMILY_HOUSEHOLD_SECTION_LAST_PAGE = 50;
  private static final String SSAP_APPLICANT_DISPLAY_ID = "SSAP_APPLICANT_DISPLAY_ID";


  private final GhixRestTemplate ghixRestTemplate;
  private final RestTemplate restTemplate;
  private final UserService userService;
  private final SsapUtil ssapUtil;
  private final ResourceCreatorImpl resourceCreatorImpl; // TODO: Get rid of this
  private final LocationService locationService;
  private final ZipCodeService zipCodeService;
  private final SsapApplicationRepository ssapApplicationRepository;
  private final ApplicantRepository applicantRepository;
  private final LookupService lookupService;
  private final EventCreationService eventCreationService;
  private final Gson platformGson;
  private final ApplicationFinalization applicationFinalization;

  @Autowired
  public SingleStreamlinedApplicationServiceImpl(final GhixRestTemplate ghixRestTemplate,
                                                 final RestTemplate restTemplate,
                                                 final UserService userService,
                                                 final SsapUtil ssapUtil,
                                                 final ResourceCreatorImpl resourceCreatorImpl,
                                                 final LocationService locationService,
                                                 final ZipCodeService zipCodeService,
                                                 final SsapApplicationRepository ssapApplicationRepository,
                                                 final LookupService lookupService,
                                                 final ApplicantRepository applicantRepository,
                                                 final EventCreationService eventCreationService,
                                                 final ApplicationFinalization applicationFinalization
  )
  {
    this.ghixRestTemplate = ghixRestTemplate;
    this.restTemplate = restTemplate;
    this.userService = userService;
    this.ssapUtil = ssapUtil;
    this.resourceCreatorImpl = resourceCreatorImpl;
    this.locationService = locationService;
    this.zipCodeService = zipCodeService;
    this.ssapApplicationRepository = ssapApplicationRepository;
    this.lookupService = lookupService;
    this.applicantRepository = applicantRepository;
    this.eventCreationService = eventCreationService;
    this.applicationFinalization = applicationFinalization;

    final GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.registerTypeAdapterFactory(new IexEnumAdapterFactory());
    gsonBuilder.serializeNulls();
    gsonBuilder.setDateFormat(ReferralConstants.JSON_DATE_FORMAT); // "MMM dd, yyyy hh:mm:ss a"
    gsonBuilder.serializeSpecialFloatingPointValues();

    this.platformGson = gsonBuilder.create();
  }

  private AccountUser getCurrentUser() throws InvalidUserException {
    final AccountUser currentUser;
    try {
      currentUser = userService.getLoggedInUser();

      if (currentUser == null) {
        throw new InvalidUserException();
      }

      return currentUser;
    } catch (InvalidUserException iue) {
      throw new InvalidUserException("Could not get currently logged in user");
    }
  }

  private int getCoverageYear(SingleStreamlinedApplication application) {
    if (application != null && application.getCoverageYear() > 0) {
      if (application.getCoverageYear() > 0) {
        try {
          return Math.toIntExact(application.getCoverageYear());
        } catch (Exception e) {
          log.error("Unable to parse supplied coverage year from JSON: {}, error: {}", application.getCoverageYear(), e.getMessage());
        }
      }
    }

    LocalDate now = TSLocalTime.getLocalDateInstance();
    return now.getYear();
  }

  /**
   * {@inheritDoc }
   */
  @Override
  public SingleStreamlinedApplication save(SingleStreamlinedApplication application,
                                           boolean csrOverride,
                                           String applicationSource,
                                           String sepEventType,
                                           Date sepEventDate) throws Exception {
    final AccountUser currentUser = getCurrentUser();

    Household household = null;
    boolean ridpVerified;

    try {
      if (csrOverride && SsapHelper.getApplicationId(application) != 0) {
        com.getinsured.iex.ssap.model.SsapApplication ssapApplication = ssapApplicationRepository.findByCaseNumber(application.getApplicationGuid());
        if (ssapApplication != null) {
          household = getHouseholdByHouseholdId(ssapApplication.getCmrHouseoldId());
        }
      } else {
        household = getHouseholdForUser(currentUser);
      }
    } catch (Exception e) {
      log.error("Unable to fetch household for currently logged-in user", e);
    }

    if (application == null) {
      log.error("Application JSON is invalid or null, if you see that JSON is sent, make sure data types are correct (e.g. objects are objects and enums are real enums, etc.)");
      throw new InvalidStateException("Application JSON that we have received is invalid or null (data type issue?)", InvalidStateType.CANT_UPDATE_APPLICANT_INFO);
    }

    if (household == null || application.getTaxHousehold() == null || application.getTaxHousehold().size() == 0) {
      throw new InvalidStateException("Application doesn't contain any (tax) households",
        InvalidStateType.APPLICATION_HAS_NO_TAX_HOUSEHOLDS);
    } else {
      ridpVerified = "Y".equalsIgnoreCase(household.getRidpVerified());

      if (!ridpVerified && !csrOverride && application.isFinalized()) {
        throw new VerificationRequiredException(application.getApplicationGuid(), VerificationType.RIDP);
      }
    }

    List<HouseholdMember> householdMemberList = setApplicationGuidAndExternalId(household.getId(), application.getTaxHousehold().get(0).getHouseholdMember());

    updateApplicantPersonType(application.getTaxHousehold());

    Date now = new TSDate();
    Timestamp nowTs = new Timestamp(now.getTime());

    String ssapApplicationLink;
    SsapApplication ssapApplication = null;

    // New application
    if (SsapHelper.getApplicationId(application) == 0) {
      boolean canCreateNew = isNewApplicationCreationAllowed(currentUser, getCoverageYear(application));

      if (!canCreateNew) {
        throw new InvalidStateException("Application already started for this household and coverage year",
          InvalidStateType.APPLICATION_ALREADY_STARTED_FOR_THIS_HOUSEHOLD_AND_YEAR);
      }

      ssapApplication = new SsapApplication();
      ssapApplication.setCaseNumber(getNextGUID());
      ssapApplication.setApplicationStatus(ApplicationStatus.OPEN.getApplicationStatusCode());
      ssapApplication.setApplicationType(application.getApplicationType());
      ssapApplication.setCreationTimestamp(nowTs);
      ssapApplication.setLastUpdateTimestamp(nowTs);
      ssapApplication.setStartDate(nowTs);
      ssapApplication.setEligibilityStatus(EligibilityStatus.PE.toString());
      ssapApplication.setExchangeEligibilityStatus(ExchangeEligibilityStatus.NONE.toString());
      ssapApplication.setCreatedBy(new BigDecimal(currentUser.getId()));
      ssapApplication.setLastUpdatedBy(ssapApplication.getCreatedBy());
      ssapApplication.setSource(ApplicationSource.ONLINE_APPLICATION.getApplicationSourceCode());
      ssapApplication.setCoverageYear(getCoverageYear(application));
      ssapApplication.setFinancialAssistanceFlag(getYNForBoolean(application.getApplyingForFinancialAssistanceIndicator()));

      if (currentUser.getActiveModuleId() == 0) {
        ssapApplication.setCmrHouseoldId(null);
      } else {
        ssapApplication.setCmrHouseoldId(new BigDecimal(currentUser.getActiveModuleId()));
      }

      application.setApplicationGuid(ssapApplication.getCaseNumber());
      application.setSsapApplicationId("0"); // 0 always because we are inside of if(appid == 0)

      Map<String, SingleStreamlinedApplication> jsonMap = new HashMap<>();
      jsonMap.put("singleStreamlinedApplication", application);
      String appDataJson = platformGson.toJson(jsonMap);

      ssapApplication.setNativeAmerican(getYNForBoolean(isAnyoneAlaskaNativeOrAmericanIndian(application)));
      ssapApplication.setApplicationData(appDataJson);

      ssapApplicationLink = resourceCreatorImpl.saveSsapApplication(ssapApplication);

      updateNativeAmericanFlag(application);

      if (!"".equals(ssapApplicationLink)) {
        long newApplicationId = Util.getNumerIDAtEndOfLink(ssapApplicationLink);
        application.setSsapApplicationId(String.valueOf(newApplicationId));

        jsonMap.put("singleStreamlinedApplication", application);
        appDataJson = platformGson.toJson(jsonMap);
        ssapApplication.setApplicationData(appDataJson);

        resourceCreatorImpl.updateSsapApplication(ssapApplication, newApplicationId);
      }

      eventCreationService.logIndividualAppEvent(EventCreationServiceImpl.APPLICATION_CREATED,
        SsapHelper.getApplicationId(application), application.getApplyingForFinancialAssistanceIndicator() == Boolean.TRUE);
    } else {
      // Existing application
      com.getinsured.iex.ssap.model.SsapApplication existingSsapApplication = null;
      try {
        existingSsapApplication = getSsapApplicationByCaseNumber(application.getApplicationGuid());
      } catch (Exception e) {
        log.error("Unable to fetch existing SSAP by GUID: {}, error: {}", application.getApplicationGuid(), e.getMessage());
        log.error("Exception", e);
      }

      if (existingSsapApplication == null) {
        throw new InvalidStateException("Unable to find SSAP for given application GUID/CaseNumber",
          InvalidStateType.NO_APPLICATION_FOUND_FOR_GIVEN_GUID_OR_CASENUMBER);
      }

      if (!csrOverride && (currentUser.getActiveModuleId() != existingSsapApplication.getCmrHouseoldId().intValueExact())) {
        throw new InvalidStateException("Permission denied to modify application, are you trying to modify application that belongs to someone else?",
          InvalidStateType.PERMISSION_DENIED_LOGGEDIN_USER_ID_AND_HOUSEHOLD_ID_DONT_MATCH);
      }

      if (csrOverride) {
        if (ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equals(existingSsapApplication.getApplicationStatus())) {
          throw new InvalidStateException("Application with status of Enrolled or Active cannot be modified by CSR",
            InvalidStateType.ENROLLED_OR_ACTIVE_SSAP_CANNOT_BE_MODIFIED_BY_CSR);
        }
      }

      updateNativeAmericanFlag(application);

      ssapApplication = new SsapApplication();

      // TODO: set/getApplciationStatus is misspelled
      ssapApplication.setApplicationStatus(existingSsapApplication.getApplicationStatus());
      ssapApplication.setCreationTimestamp(existingSsapApplication.getCreationTimestamp());
      ssapApplication.setStartDate(existingSsapApplication.getStartDate());
      ssapApplication.setEligibilityStatus(existingSsapApplication.getEligibilityStatus().name());
      ssapApplication.setExchangeEligibilityStatus(existingSsapApplication.getExchangeEligibilityStatus().name());
      ssapApplication.setCmrHouseoldId(existingSsapApplication.getCmrHouseoldId());
      ssapApplication.setCaseNumber(existingSsapApplication.getCaseNumber());
      ssapApplication.setCurrentPageId(String.valueOf(application.getCurrentPage()));
      ssapApplication.setEligibilityReceivedDate(existingSsapApplication.getEligibilityReceivedDate());
      ssapApplication.setEhbAmount(existingSsapApplication.getEhbAmount());
      ssapApplication.setCsrLevel(existingSsapApplication.getCsrLevel());
      ssapApplication.setNativeAmerican(getYNForBoolean(isAnyoneAlaskaNativeOrAmericanIndian(application)));
      ssapApplication.setMaximumAPTC(existingSsapApplication.getMaximumAPTC());
      ssapApplication.setElectedAPTC(existingSsapApplication.getElectedAPTC());
      ssapApplication.setAvailableAPTC(existingSsapApplication.getAvailableAPTC());
      ssapApplication.setLastUpdateTimestamp(nowTs);
      ssapApplication.setCreatedBy(existingSsapApplication.getCreatedBy());
      ssapApplication.setLastUpdatedBy(new BigDecimal(currentUser.getId()));
      ssapApplication.setAllowEnrollment(existingSsapApplication.getAllowEnrollment());
      ssapApplication.setEligibilityResponseType(existingSsapApplication.getEligibilityResponseType());
      ssapApplication.setExternalApplicationId(existingSsapApplication.getExternalApplicationId());
      ssapApplication.setExemptHouseHold(existingSsapApplication.getExemptHousehold());
      ssapApplication.setCoverageYear(new Long(existingSsapApplication.getCoverageYear()).intValue());
      ssapApplication.setSource(existingSsapApplication.getSource());
      ssapApplication.setApplicationType(existingSsapApplication.getApplicationType());
      ssapApplication.setFinancialAssistanceFlag(getYNForBoolean(application.getApplyingForFinancialAssistanceIndicator()));
      ssapApplication.setParentSsapApplicationId(existingSsapApplication.getParentSsapApplicationId() != null ? existingSsapApplication.getParentSsapApplicationId().longValue() : null);

      if (application.isFinalized()) {
        HouseholdMember primaryMember = HouseholdUtil.getPrimaryTaxHouseholdMember(application.getTaxHousehold());

        if (primaryMember.getName() != null) {
          // TODO: On the UI we check if they are e-signed. Perhaps here double-check is needed as well.
          ssapApplication.setEsignFirstName(primaryMember.getName().getFirstName());
          ssapApplication.setEsignLastName(primaryMember.getName().getLastName());
        } else {
          // TODO: Is this even valid?
          ssapApplication.setEsignFirstName("");
          ssapApplication.setEsignLastName("");
        }

        ssapApplication.setEsignDate(nowTs);

        if (!csrOverride && !GhixRole.INDIVIDUAL.name().equals(currentUser.getDefRole().getName()) && applicationSource != null) {
          ssapApplication.setSource(applicationSource);
        }

        // For now, set everybody who is not part of Primary Tax Household as not seeking coverage
        // After OEP 2019, we will update this to give them medicaid/aptc/chip etc.
//        List<HouseholdMember> nonPrimaryTaxHouseholdMembers = HouseholdUtil.getNonPrimaryTaxHouseholdMembers(application);
//        nonPrimaryTaxHouseholdMembers.forEach(hhm -> {
//          // hhm.setApplyingForCoverageIndicator(Boolean.FALSE);
//          application.getTaxHousehold().get(0).getHouseholdMember().forEach(ahhm -> {
//            if(ahhm.getPersonId().equals(hhm.getPersonId())) {
//              ahhm.setApplyingForCoverageIndicator(Boolean.FALSE);
//              log.info("Household member is not part of primary tax household: {} setting as not seeking coverage", hhm.getPersonId());
//            }
//          });
//        });
      }

      // Update SsapApplication DTO with new JSON
      ssapApplication = JsonUtil.setApplicationData(ssapApplication, application);

      if (csrOverride || ApplicationStatus.OPEN.getApplicationStatusCode().equals(existingSsapApplication.getApplicationStatus())) {
        ssapApplicationLink = resourceCreatorImpl.updateSsapApplication(ssapApplication, SsapHelper.getApplicationId(application));
      } else {
        throw new InvalidStateException("Application was already e-signed and cannot be modified again",
          InvalidStateType.APPLICATION_ALREADY_ESIGNED);
      }
    }

    if (application.getApplyingForFinancialAssistanceIndicator() != null && application.getApplyingForFinancialAssistanceIndicator() == Boolean.TRUE &&
      FAMILY_HOUSEHOLD_SECTION_LAST_PAGE == application.getCurrentPage()) {
      application.setMecCheckStatus(mecCheckIntegration(application.getApplicationGuid()));
    }

    List<BloodRelationship> bloodRelationships = ssapUtil.getBloodRelationship(householdMemberList);
    AtomicBoolean updateApplicants = new AtomicBoolean(false);
    Map<String, SsapApplicantResource> applicantMap = resourceCreatorImpl.getApplicantsWithGuidAsKey(SsapHelper.getApplicationId(application));
    List<SsapApplicant> applicantList = applicantRepository.findBySsapApplicationId(SsapHelper.getApplicationId(application));
    Location mailingLocation = saveMailingLocation(householdMemberList, applicantMap);
    Location homeLocation = savePrimaryLocation(householdMemberList, applicantMap);

    // Compare existing applicant list with current SSAP household members and remove any old ones.
    if (applicantList != null && !applicantList.isEmpty()) {
      List<SsapApplicant> removedApplicants = new ArrayList<>();

      applicantList.forEach(applicant -> {
        Optional<HouseholdMember> member = householdMemberList.stream().filter(hhm -> hhm.getApplicantGuid() != null &&
          hhm.getApplicantGuid().equals(applicant.getApplicantGuid())).findFirst();

        if (!member.isPresent()) {
          log.info("Removing applicant for SSAP id: {}, id: {}, applicant guid: {}",
            application.getSsapApplicationId(), applicant.getId(), applicant.getApplicantGuid());

          removedApplicants.add(applicant);
          log.info("Applicant ID: {}", applicant.getId());
        }
      });

      if (!removedApplicants.isEmpty()) {
        if(isPossibleToRemoveApplicants(application.getApplicationStatus())) {
          markApplicantForDeletion(SsapHelper.getApplicationId(application), removedApplicants);
        }
        else {
          log.warn("Tried to delete {} applicants, but current application status does not allow it", removedApplicants.size());
        }
      }
    }

    // TODO: Start of applicant creation/modification for this SSAP
    for (HouseholdMember householdMember : householdMemberList) {
      // Hard STOP if there is no personId, nothing will work down the stream.
      if (householdMember.getPersonId() == null) {
        throw new GIRuntimeException("Household Member doesn't have person id! ssap id: " + application.getSsapApplicationId() + "; member: " + householdMember.getName());
      }

      SsapApplicantResource applicant;
      if (applicantMap != null && applicantMap.containsKey(householdMember.getApplicantGuid())) {
        applicant = applicantMap.get(householdMember.getApplicantGuid());
        updateApplicants.set(true);
        applicant.setPersonId(Long.valueOf(householdMember.getPersonId()));

        if(applicant.getExternalApplicantId() == null && householdMember.getExternalId() != null) {
          applicant.setExternalApplicantId(householdMember.getExternalId());
        } else if(applicant.getExternalApplicantId() != null && householdMember.getExternalId() == null) {
          householdMember.setExternalId(applicant.getExternalApplicantId());
        }
      } else {
        applicant = new SsapApplicantResource();
        if(householdMember.getApplicantGuid() != null) {
        	applicant.setApplicantGuid(householdMember.getApplicantGuid());
        }else {
        	applicant.setApplicantGuid(getNextGUID());
        	// Once new GUID is created, save it in the JSON as well.
        	householdMember.setApplicantGuid(applicant.getApplicantGuid());
        }
        applicant.setCreationTimestamp(nowTs);
        applicant.setPersonId(Long.valueOf(householdMember.getPersonId()));
        updateApplicants.set(false);
      }

      assert applicant.getApplicantGuid() != null :
        "Applicant GUID is null: " + applicant.getFirstName() + " " + applicant.getLastName() + " [id: " + applicant.getId() + "]";

      if(applicant.getApplicantGuid() == null) {
        throw new GIRuntimeException("Applicant GUID is null: " + applicant.getFirstName() + " " + applicant.getLastName() + " [id: " + applicant.getId() + "]");
      }

      if (bloodRelationships != null && !bloodRelationships.isEmpty()) {
        applicant.setRelationship(getApplicantRelation(bloodRelationships, applicant.getPersonId()));
      }

      applicant.setSsapApplication(ssapApplicationLink);

      if (householdMember.getSocialSecurityCard() != null) {
        applicant.setSsn(householdMember.getSocialSecurityCard().getSocialSecurityNumber());
      }

      if (householdMember.getName() != null) {
        applicant.setFirstName(householdMember.getName().getFirstName());
        applicant.setLastName(householdMember.getName().getLastName());
        applicant.setMiddleName(householdMember.getName().getMiddleName());
        applicant.setNameSuffix(householdMember.getName().getSuffix());
      }

      applicant.setGender(householdMember.getGender());

      if (householdMember.getMarriedIndicator() == Boolean.TRUE) {
        applicant.setMarried("true");
      } else {
        applicant.setMarried("false");
      }
      applicant.setApplyingForCoverage(getYNForBoolean(householdMember.getApplyingForCoverageIndicator()));
      applicant.setHouseholdContactFlag(getYNForBoolean(householdMember.getHouseholdContactIndicator()));

      if (householdMember.getHouseholdContact() != null) {
        if (householdMember.getHouseholdContact().getPhone() != null) {
          applicant.setMobilePhoneNumber(householdMember.getHouseholdContact().getPhone().getPhoneNumber());
        }

        if (householdMember.getHouseholdContact().getOtherPhone() != null) {
          applicant.setPhoneNumber(householdMember.getHouseholdContact().getOtherPhone().getPhoneNumber());
        }

        if (householdMember.getHouseholdContact().getContactPreferences() != null) {
          applicant.setEmailAddress(householdMember.getHouseholdContact().getContactPreferences().getEmailAddress());
        }
      }

      applicant.setBirthDate(householdMember.getDateOfBirth());
      // applicant.setExternalApplicantId(householdMember.getExternalId());
      applicant.setLastUpdateTimestamp(nowTs);
      // TODO: typo
      applicant.setMailiingLocationId(new BigDecimal(mailingLocation.getId()));
      applicant.setOtherLocationId(new BigDecimal(homeLocation.getId()));

      if (householdMember.getAmericanIndianAlaskaNative() != null) {
        applicant.setNativeAmericanFlag(
          getYNForBoolean(householdMember.getAmericanIndianAlaskaNative().getMemberOfFederallyRecognizedTribeIndicator())
        );
      } else if (householdMember.getSpecialCircumstances() != null) {
        applicant.setNativeAmericanFlag(
          getYNForBoolean(householdMember.getSpecialCircumstances().getAmericanIndianAlaskaNativeIndicator())
        );
      }

      // Get Person Type
      applicant.setPersonType(householdMember.getApplicantPersonType().getPersonType());

      if (updateApplicants.get()) {
        try {
          boolean updatedApplicant = updateSsapApplicantInfo(applicant);

          if (!updatedApplicant) {
            throw new Exception("Applicant cannot be updated, returned null after updateSsapApplicant method call");
          }
          log.info("Updated applicant information. guid {}", applicant.getApplicantGuid());
        } catch (Exception updateException) {
          throw new InvalidStateException("Unable to update applicant information for given SSAP: " +
            updateException.getMessage(),
            InvalidStateType.CANT_UPDATE_APPLICANT_INFO);
        }
      } else {
        try {
          String newApplicantLink = resourceCreatorImpl.createSsapApplicant(applicant, ssapApplicationLink);
          if (newApplicantLink != null) {
            log.info("[create] Created new applicant: {}", newApplicantLink);
          } else {
            log.warn("[create] unable to create new applicant, after call newApplicantLink is null");
          }
        } catch (Exception createException) {
          throw new InvalidStateException("Unable to create applicant information for given SSAP: " +
            createException.getMessage(),
            InvalidStateType.CANT_UPDATE_APPLICANT_INFO);
        }
      }
    }
    // TODO: End of applicant creation/modification for this SSAP

    if (application.isFinalized()) {

      updateHouseholdLocationId(household, mailingLocation);

      if (sepEventDate == null) {
        sepEventDate = application.getApplicationStartDate();

        if (sepEventDate == null) {
          sepEventDate = now;
        }
      }

      if (sepEventType == null) {
        sepEventType = "";
      }

      String sepEventDateStr = new SimpleDateFormat("yyyy-MM-dd").format(sepEventDate);

      final Long parentSsapApplicationId = ssapApplication.getParentSsapApplicationId() != null ? ssapApplication.getParentSsapApplicationId() : 0L;

      log.info("Launching finalization of the SSAP as async.");
      application.setApplicationStatus(ApplicationStatus.SIGNED);
      ssapApplication.setApplicationStatus(ApplicationStatus.SIGNED.getApplicationStatusCode());
      JsonUtil.setApplicationData(ssapApplication, application);
      resourceCreatorImpl.updateSsapApplication(ssapApplication, SsapHelper.getApplicationId(application));

      applicationFinalization.finalizeApplication(application, parentSsapApplicationId, sepEventType, sepEventDateStr, csrOverride, currentUser);
    }

    return application;
  }

  private boolean isPossibleToRemoveApplicants(final ApplicationStatus applicationStatus) {
    // login from old code
    if(!(ApplicationStatus.OPEN.getApplicationStatusCode().equals(applicationStatus.getApplicationStatusCode()) ||
      ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equals(applicationStatus.getApplicationStatusCode()) ||
      ApplicationStatus.SUBMITTED.getApplicationStatusCode().equals(applicationStatus.getApplicationStatusCode()) ||
      ApplicationStatus.SIGNED.getApplicationStatusCode().equals(applicationStatus.getApplicationStatusCode()))) {
      return false;
    }

    return true;
  }

  private boolean markApplicantForDeletion(final long applicationId, final List<SsapApplicant> applicants) {
    final String eligibilityUrl = String.format("%sapplication/deleteSsapApplicationApplicant", GhixEndPoints.ELIGIBILITY_URL);

    try {
      final List<Boolean> results = new ArrayList<>(applicants.size());

      applicants.forEach(applicant -> {
        final SsapApplicationRequest ssapApplicationRequest = new SsapApplicationRequest();
        ssapApplicationRequest.setApplicantGuidToRemove(applicant.getApplicantGuid());
        ssapApplicationRequest.setSsapApplicationId(applicationId);

        log.info("Marking applicant for deletion, ssap id: {}, applicant id: {}, guid: {}",
          applicationId,
          applicant.getId(),
          applicant.getApplicantGuid()
        );

        boolean res = ghixRestTemplate.postForObject(eligibilityUrl, ssapApplicationRequest, Boolean.class);
        results.add(res);

        if(!res) {
          log.error("Unable to mark applicant for deletion, ssap id: {}, applicant id: {}, guid: {}",
            applicationId,
            applicant.getId(),
            applicant.getApplicantGuid());
        }
      });

      return results.size() == applicants.size();
    }
    catch(Exception e) {
      log.error("Error while calling: " + eligibilityUrl, e);
    }

    return false;
  }

  private void updateHouseholdLocationId(Household household, Location location){
    String url = GhixEndPoints.ELIGIBILITY_URL +  "/application/updateHouseholdLocation/" + household.getId();
    ghixRestTemplate.postForObject(url, location, String.class);
  }

  private void updateApplicantPersonType(List<TaxHousehold> taxHouseholds) {
    final TaxHousehold taxHousehold = taxHouseholds.get(0);
    final PersonTypeRequest personTypeRequest = new PersonTypeRequest();
    personTypeRequest.setPrimaryTaxFilerId(String.valueOf(taxHousehold.getPrimaryTaxFilerPersonId()));
    personTypeRequest.setList(new ArrayList<>());

    taxHousehold.getHouseholdMember().forEach(hhm -> {
      // We don't have a notion of primary contact, UI keeps first person as the applicant
      // that started SSAP, for now this if will be sufficient, if anything changes this if() can be updated.
      if(hhm.getPersonId() == 1) {
        personTypeRequest.setPrimaryApplicantId(String.valueOf(hhm.getPersonId()));
      }

      PersonTypeDTO personTypeDTO = new PersonTypeDTO();
      personTypeDTO.setApplyingForCoverage(hhm.getApplyingForCoverageIndicator() != null && hhm.getApplyingForCoverageIndicator() == Boolean.TRUE);
      personTypeDTO.setPersonId(String.valueOf(hhm.getPersonId()));
      personTypeRequest.getList().add(personTypeDTO);
    });

    if(log.isDebugEnabled()) {
      ObjectMapper om = new ObjectMapper();
      om.enable(SerializationFeature.INDENT_OUTPUT);
      try {
        log.debug("Request for person type: {}", om.writeValueAsString(personTypeRequest));
      } catch (JsonProcessingException e) {
        log.error("exception while logging person type request: {}", e.getMessage());
      }
    }

    final String url = GhixEndPoints.ELIGIBILITY_URL + "/referral/getPersonType";
    final Map<?,?> responseMap;

    try {
      responseMap = ghixRestTemplate.postForObject(url, personTypeRequest, Map.class);

      if(log.isDebugEnabled()) {
        ObjectMapper om = new ObjectMapper();
        om.enable(SerializationFeature.INDENT_OUTPUT);
        try {
          log.debug("Response of personType request: {}", om.writeValueAsString(responseMap));
        } catch (JsonProcessingException e) {
          log.error("exception while logging person type response: {}", e.getMessage());
        }
      }

      if(responseMap != null) {
        taxHousehold.getHouseholdMember().forEach(hhm -> {
          String personIdString = String.valueOf(hhm.getPersonId());
          String personTypeString = String.valueOf(responseMap.get(personIdString));
          SsapApplicantPersonType personType = SsapApplicantPersonType.valueOf(personTypeString);
          hhm.setApplicantPersonType(personType);
          log.info("Setting HHM with person id: {} to person type: {}", hhm.getPersonId(), personType);
        });
      }
    } catch(Exception e) {
      log.error("Unable to determinate person type, request for API failed: {}", e.getMessage());
      log.error("person type request exception", e);
    }
  }

  /**
   * Updates native american flag for each household member.
   * The reason for this, is because new React SSAP is using different flag
   * in comparison to what DMI page, or old SSAP was using as per new requirements.
   *
   * @param application {@link SingleStreamlinedApplication} object.
   */
  private void updateNativeAmericanFlag(final SingleStreamlinedApplication application) {
    final TaxHousehold taxHousehold = application.getTaxHousehold().get(0);
    final List<HouseholdMember> members = taxHousehold.getHouseholdMember();
    members.stream().filter(hhm -> hhm.getAmericanIndianAlaskaNative() != null && hhm.getAmericanIndianAlaskaNative().getMemberOfFederallyRecognizedTribeIndicator() == Boolean.TRUE)
      .map(HouseholdMember::getSpecialCircumstances).forEach(specialCircumstances -> {
      if (specialCircumstances == null) {
        specialCircumstances = new SpecialCircumstances();
      }
      specialCircumstances.setAmericanIndianAlaskaNativeIndicator(Boolean.TRUE);
    });
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public SingleStreamlinedApplication save(SaveRequest saveRequest) throws Exception {
    return save(saveRequest.getSsapApplication(),
      saveRequest.isCsrOverride(),
      saveRequest.getApplicationSource(),
      saveRequest.getSepEvent(),
      saveRequest.getSepEventDate());
  }

  /**
   * TODO: This is copied from old code
   *
   * @param link href string
   * @return number at the eno of given {@code link}
   * @deprecated Dont use this crap
   */
  private long getNumberIDAtEndOfLink(String link) {
    long returnValue = 0;
    Pattern patternForNumberInLink = Pattern.compile("\\d+");
    Matcher matcher = patternForNumberInLink.matcher(link);
    while (matcher.find()) {
      returnValue = Long.parseLong(matcher.group());

    }
    return returnValue;
  }

  // TODO: copied from ghix-iex-commons
  private boolean updateSsapApplicantInfo(final SsapApplicantResource applicant) throws Exception {
    try {
      Object links = applicant.get_links();
      JSONObject jsonObjectLinks = new JSONObject((Map<String, String>) links);
      Map<String, String> self = (Map<String, String>) jsonObjectLinks.get("self");
      String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICANTS_DATA_URL + "{id}";
      String selfHref = (self.get("href") == null) ? "" : self.get("href");
      if ("".equals(selfHref)) {
        throw new Exception("The end point to update the applicant is incorrect or not defined");
      }
      long ssapApplicantId = getNumberIDAtEndOfLink(selfHref);
      Map<String, Object> uriParams = new HashMap<>();
      uriParams.put("id", ssapApplicantId);
      ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapApplicantResource.class);
      String jsonString = writer.writeValueAsString(applicant);
      HttpHeaders requestHeaders = new HttpHeaders();
      List<MediaType> mediaTypeList = new ArrayList<>();
      mediaTypeList.add(MediaType.APPLICATION_JSON);
      requestHeaders.setAccept(mediaTypeList);
      requestHeaders.setContentType(MediaType.APPLICATION_JSON);
      HttpEntity<?> requestEntity = new HttpEntity<Object>(jsonString, requestHeaders);

      /*
      exchangeWithParameterizedType(String url, String userName, HttpMethod httpMethod, MediaType contentType,
        ParameterizedTypeReference<T> responseType, Map<String, ?> inputVariables, boolean isTenantContext, String tenantUserName)
       */
      ResponseEntity<Resource<SsapApplicantResource>> updated =
        restTemplate.exchange(url, HttpMethod.PUT, requestEntity, new ParameterizedTypeReference<Resource<SsapApplicantResource>>() {
        }, uriParams);
      if (!updated.getStatusCode().is4xxClientError() && !updated.getStatusCode().is5xxServerError()) {
        return true;
      }
    } catch (IOException jme) {
      log.error("Exception in writing value as a string", jme);
      throw new Exception(jme);
    } catch (Exception e) {
      log.error("Exception occurred while updating the applicant", e);
      throw new Exception(e);
    }

    return false;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public SingleStreamlinedApplication getApplicationByApplicationId(final long applicationId) throws Exception {
    log.info("Fetching SSAP by application id: {}", applicationId);
    if (applicationId <= 0) {
      return createNewSingleStreamlinedApplication(null);
    }

    com.getinsured.iex.ssap.model.SsapApplication ssapApplication = ssapApplicationRepository.findOne(applicationId);

    if (ssapApplication == null) {
      throw new InvalidStateException("Application with given id not found.",
        InvalidStateType.APPLICATION_NOT_FOUND_FOR_GIVEN_ID);
    }

    Household household;

    try {
      AccountUser currentUser = getCurrentUser(); // this will throw exception if null/not logged in.
      household = getHouseholdForUser(currentUser);

      if (household != null && ssapApplication.getCmrHouseoldId() != null &&
        ssapApplication.getCmrHouseoldId().intValue() != household.getId()) {
        // If we are not just ordinary INDIVIDUAL user role, deny looking at someone else's SSAP.
        if (GhixRole.INDIVIDUAL.name().equals(currentUser.getDefRole().getName())) {
          throw new Exception("Currently logged in user does not have permission to view this application");
        }
      }
    } catch (Exception e) {
      throw new InvalidStateException(e.getMessage(),
        InvalidStateType.PERMISSION_DENIED_LOGGEDIN_USER_ID_AND_HOUSEHOLD_ID_DONT_MATCH);
    }

    SingleStreamlinedApplication application = JsonUtil.parseApplicationDataJson(ssapApplication);

    if (application != null) {
      setApplicationTypeFlag(application, ssapApplication.getApplicationType());

      ApplicationStatus applicationStatus = ApplicationStatus.fromString(ssapApplication.getApplicationStatus());
      if (applicationStatus != null) {
        application.setApplicationStatus(applicationStatus);
      }

      application.setEligibilityStatus(ssapApplication.getEligibilityStatus());
      application.setCoverageYear(ssapApplication.getCoverageYear());
      application.setExternalApplicationId(ssapApplication.getExternalApplicationId());
      application.setExemptHousehold(ssapApplication.getExemptHousehold());

      if (household != null) {
        application.setRidpVerified("Y".equals(household.getRidpVerified()));
      }

      List<TaxHousehold> taxHouseholds = application.getTaxHousehold();
      if (taxHouseholds != null && !taxHouseholds.isEmpty()) {
        TaxHousehold taxHousehold = taxHouseholds.get(0);
        List<HouseholdMember> members = taxHousehold.getHouseholdMember();
        HouseholdMember primaryMember = members.stream().filter(m -> m.getPersonId() == 1).findFirst().orElse(null);

        if (primaryMember != null) {
          if (primaryMember.getAddressId() == 0) {
            primaryMember.setAddressId(1);
          }

          if (primaryMember.getHouseholdContact() == null) {
            primaryMember.setHouseholdContact(new HouseholdContact());
          } else if (primaryMember.getHouseholdContact().getHomeAddress() == null) {
            primaryMember.getHouseholdContact().setHomeAddress(new HomeAddress());
          }

          Address address = primaryMember.getHouseholdContact().getHomeAddress();

          if (address.getAddressId() == 0) {
            address.setAddressId(1);
            List<Address> otherAddresses = taxHousehold.getOtherAddresses();
            Address existingPrimaryAddress = otherAddresses.stream().filter(a -> a.getAddressId() == 1).findFirst().orElse(null);
            if (existingPrimaryAddress == null) {
              otherAddresses.add(address);
            } else {
              existingPrimaryAddress.setAddressId(1);
            }
          }
        } else {
          log.error("Unable to set primary's addressId because could not find primary contact by personId == 1, ssap id: {}", applicationId);
        }
      }

      log.info("Returning SSAP by id: {}", applicationId);
      return application;
    }

    log.warn("Unable to find SSAP by id: {}, returning null", applicationId);
    return null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<SsapApplicationResource> getSsapApplicationsForYear(final int coverageYear) throws Exception {
    final AccountUser currentUser = getCurrentUser();
    return getSsapApplicationsForCoverageYear(currentUser.getActiveModuleId(), coverageYear);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public com.getinsured.iex.ssap.model.SsapApplication getNewSsapApplicationForHousehold(Household household, int coverageYear) throws Exception {

    log.info("Creating new SSAP stub for given household and coverage year: {}", coverageYear);
    final AccountUser currentUser = getCurrentUser(); // this will throw InvalidUserException if not present

    if (household == null) {
      household = getHouseholdForUser(currentUser);
    }

    if (household == null) {
      throw new Exception("Could not get household for current user");
    }

    Date now = new TSDate();
    Timestamp nowTs = new Timestamp(now.getTime());

    com.getinsured.iex.ssap.model.SsapApplication newApp = new com.getinsured.iex.ssap.model.SsapApplication();
    newApp.setCmrHouseoldId(new BigDecimal(currentUser.getActiveModuleId()));
    newApp.setApplicationStatus(ApplicationStatus.OPEN.getApplicationStatusCode());
    newApp.setStartDate(nowTs);
    newApp.setEligibilityStatus(EligibilityStatus.PE);
    newApp.setExchangeEligibilityStatus(ExchangeEligibilityStatus.NONE);
    newApp.setCreatedBy(newApp.getCmrHouseoldId());
    newApp.setSource(ApplicationSource.ONLINE_APPLICATION.getApplicationSourceCode());
    newApp.setCoverageYear(coverageYear);

    SingleStreamlinedApplication application = createNewSingleStreamlinedApplication(household);
    Map<String, SingleStreamlinedApplication> jsonMap = new HashMap<>();
    jsonMap.put("singleStreamlinedApplication", application);
    String appDataJson = platformGson.toJson(jsonMap);

    newApp.setApplicationData(appDataJson);

    return newApp;
  }

  /**
   * {@inheritDoc}
   */
  public SingleStreamlinedApplication createNewSingleStreamlinedApplication(Household household) throws Exception {
    log.info("Creating new SingleStreamlinedApplication() JSON stub.");

    // If we didn't pass household, try to get it.
    if (household == null) {
      AccountUser currentUser = getCurrentUser();
      household = getHouseholdForUser(currentUser);

      if (household == null) {
        throw new Exception("Could not get household for current user");
      }
    }

    Date now = new TSDate();
    Timestamp nowTs = new Timestamp(now.getTime());

    SingleStreamlinedApplication application = new SingleStreamlinedApplication();
    application.setManuallyCreated(true);
    application.setApplicationDate(nowTs);
    application.setSsapApplicationId("0");
    application.setIsRidpVerified("Y".equals(household.getRidpVerified()));
    application.setApplicationStartDate(now);
    application.setApplicationStatus(ApplicationStatus.OPEN);
    setApplicationTypeFlag(application, null);

    Name authorizedRepName = new Name();
    AuthorizedRepresentative authorizedRepresentative = new AuthorizedRepresentative();
    authorizedRepresentative.setName(authorizedRepName);

    application.setAuthorizedRepresentative(authorizedRepresentative);

    List<TaxHousehold> taxHouseholds = getTaxHouseholdForNewApplication(household);
    application.setTaxHousehold(taxHouseholds);

    return application;
  }

  private void setApplicationTypeFlag(SingleStreamlinedApplication application, String currentFlag) {

    // TODO: Check for Open enrollment date range and if outside, set to QLE?
    final String oeStartDateStr = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE);
    final String oeEndDateStr = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE);

    if (!"".equals(oeStartDateStr) && !"".equals(oeEndDateStr)) {
      try {
        Date startDate = DateUtils.parseDate(oeStartDateStr, "MM/dd/yyyy", "yyyy-MM-dd");
        Date endDate = DateUtils.parseDate(oeEndDateStr, "MM/dd/yyyy", "yyyy-MM-dd");

        LocalDate now = TSLocalTime.getLocalDateInstance();
        LocalDate startOEP = LocalDate.fromDateFields(startDate);
        LocalDate endOEP = LocalDate.fromDateFields(endDate);

        // TODO: This grace period calculation is taken from old code. Not sure why it's 9 days by default.
//        int enrollmentGracePeriod = 9;
//        String enrollmentGracePeriodStr = DynamicPropertiesUtil.getPropertyValue(
//            GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
//
//        if (StringUtils.isNumeric(enrollmentGracePeriodStr)) {
//          enrollmentGracePeriod = Integer.parseInt(enrollmentGracePeriodStr);
//        }
//
//        // Add grace period?
//        endOEP = endOEP.plusDays(enrollmentGracePeriod);

        if (now.isBefore(startOEP) || now.isAfter(endOEP)) {
          log.info("Current date is out of OEP range: {} and {}, setting application type of QEP", startOEP, endOEP);
          application.setApplicationType(SsapApplicationTypeEnum.QEP.name());
        } else {
          if (currentFlag != null && !"".equals(currentFlag)) {
            application.setApplicationType(currentFlag);
          } else {
            application.setApplicationType(SsapApplicationTypeEnum.OE.name());
          }
        }
      } catch (ParseException e) {
        log.error("Unable to parse OEP start/end dates: start: {}, end: {}, error: {}", oeStartDateStr, oeEndDateStr, e.getMessage());
      }
    }

    if (currentFlag != null && !"".equals(currentFlag)) {
      application.setApplicationType(currentFlag);
    } else {
      application.setApplicationType(SsapApplicationTypeEnum.OE.name());
    }
    log.info("OE start/end date is not configured in state configuration properties, setting applicationType to: {}",
      application.getApplicationType());
  }

  private List<TaxHousehold> getTaxHouseholdForNewApplication(Household household) {
    List<TaxHousehold> taxHouseholds = new ArrayList<>();
    TaxHousehold taxHousehold = new TaxHousehold();

    List<HouseholdMember> householdMembers = new ArrayList<>();
    HouseholdMember householdMember = new HouseholdMember();
    householdMember.setApplicantPersonType(SsapApplicantPersonType.PC_PTF);
    householdMember.setDateOfBirth(household.getBirthDate());

    Name name = new Name();
    name.setFirstName(household.getFirstName());
    name.setLastName(household.getLastName());
    name.setMiddleName(household.getMiddleName());
    name.setSuffix(household.getNameSuffix());
    householdMember.setName(name);

    HouseholdContact contact = new HouseholdContact();
    Phone phone = new Phone();
    phone.setPhoneNumber(household.getPhoneNumber());
    contact.setPhone(phone);

    Location hhLocation = household.getPrefContactLocation();

    HomeAddress homeAddress = new HomeAddress();
    MailingAddress mailingAddress = new MailingAddress();

    if (hhLocation != null) {

      homeAddress.setStreetAddress1(hhLocation.getAddress1());
      homeAddress.setStreetAddress2(hhLocation.getAddress2());
      homeAddress.setCity(hhLocation.getCity());
      homeAddress.setState(hhLocation.getState());
      homeAddress.setPostalCode(hhLocation.getZip());
      homeAddress.setCounty(hhLocation.getCounty());
      homeAddress.setCountyCode(hhLocation.getCountycode());
      homeAddress.setPrimaryAddressCountyFipsCode(homeAddress.getCountyCode());

      mailingAddress.setStreetAddress1(homeAddress.getStreetAddress1());
      mailingAddress.setStreetAddress2(homeAddress.getStreetAddress2());
      mailingAddress.setCity(homeAddress.getCity());
      mailingAddress.setState(homeAddress.getState());
      mailingAddress.setPostalCode(homeAddress.getPostalCode());
      mailingAddress.setCounty(homeAddress.getCounty());
      mailingAddress.setCountyCode(homeAddress.getCountyCode());
      mailingAddress.setPrimaryAddressCountyFipsCode(homeAddress.getCountyCode());

      if (hhLocation.getState() != null && hhLocation.getCounty() != null) {
        final ZipCode zipCode = zipCodeService.findByZipCode(household.getZipCode());
        if (zipCode != null) {
          // county codes on UI are fetched from API that maps state fips + county fips, e.g. { "06081": "San Mateo"}
          // TODO: do we mimic this behaviour here or require UI to pre-select county based on the name?
          final String uiCountyCode = zipCode.getStateFips() + zipCode.getCountyFips();
          homeAddress.setCountyCode(uiCountyCode);
          homeAddress.setPrimaryAddressCountyFipsCode(uiCountyCode);
          mailingAddress.setCountyCode(uiCountyCode);
        }
        // We can add fips here, for example, if needed.
      }

      contact.setMailingAddressSameAsHomeAddressIndicator(false);
    }

    /*
     * Set addressId to be 1, for personId=>addressId matrix.
     */
    homeAddress.setAddressId(1);
    householdMember.setAddressId(1);
    taxHousehold.setOtherAddresses(Collections.singletonList(homeAddress));

    contact.setHomeAddress(homeAddress);
    contact.setMailingAddress(mailingAddress);

    ContactPreferences contactPreferences = new ContactPreferences();
    contactPreferences.setEmailAddress(household.getEmail());

    if (household.getPrefContactMethod() != null && household.getPrefContactMethod() > 0) {
      LookupValue prefContactMethodValue = lookupService.findLookupValuebyId(household.getPrefContactMethod());

      if (prefContactMethodValue != null) {
        contactPreferences.setPreferredContactMethod(prefContactMethodValue.getLookupValueCode());
      }
    }

    if (household.getPrefSpokenLang() != null && household.getPrefSpokenLang() > 0) {
      final LookupValue prefSpokenValue = lookupService.findLookupValuebyId(household.getPrefSpokenLang());
      if (prefSpokenValue != null) {
        contactPreferences.setPreferredSpokenLanguage(prefSpokenValue.getLookupValueCode());
      }
    }

    if (household.getPrefWrittenLang() != null && household.getPrefWrittenLang() > 0) {
      final LookupValue prefWrittenValue = lookupService.findLookupValuebyId(household.getPrefWrittenLang());
      if (prefWrittenValue != null) {
        contactPreferences.setPreferredWrittenLanguage(prefWrittenValue.getLookupValueCode());
      }
    }

    contact.setContactPreferences(contactPreferences);

    householdMember.setHouseholdContact(contact);

    SocialSecurityCard ssnCard = new SocialSecurityCard();

    if (household.getSsn() != null && !household.getSsn().trim().equals("")) {
      ssnCard.setSocialSecurityNumber(household.getSsn());
      ssnCard.setFirstNameOnSSNCard(household.getFirstName());
      ssnCard.setMiddleNameOnSSNCard(household.getMiddleName());
      ssnCard.setLastNameOnSSNCard(household.getLastName());
      ssnCard.setSuffixOnSSNCard(household.getNameSuffix());
      ssnCard.setSocialSecurityCardHolderIndicator(true);
    }

    householdMember.setSocialSecurityCard(ssnCard);
    List<BloodRelationship> bloodRelationships = new ArrayList<>();
    BloodRelationship bloodRelationship = new BloodRelationship();
    bloodRelationship.setRelation(BloodRelationshipCode.SELF.getCode());
    bloodRelationship.setIndividualPersonId(String.valueOf(1));
    bloodRelationship.setRelatedPersonId(String.valueOf(1));
    bloodRelationships.add(bloodRelationship);

    householdMember.setPersonId(1);

    householdMember.setBloodRelationship(bloodRelationships);
    List<EligibleImmigrationDocumentType> eligibleImmigrationDocumentTypeList = new ArrayList<>();
    EligibleImmigrationDocumentType eligibleImmigrationDocumentType = new EligibleImmigrationDocumentType();
    eligibleImmigrationDocumentTypeList.add(eligibleImmigrationDocumentType);
    CitizenshipImmigrationStatus citizenshipImmigrationStatus = new CitizenshipImmigrationStatus();
    citizenshipImmigrationStatus.setEligibleImmigrationDocumentType(eligibleImmigrationDocumentTypeList);

    householdMember.setCitizenshipImmigrationStatus(citizenshipImmigrationStatus);

    // Health Coverage
    final HealthCoverage healthCoverage = new HealthCoverage();
    final CurrentOtherInsurance currentOtherInsurance = new CurrentOtherInsurance();
    final List<OtherStateOrFederalProgram> otherStateOrFederalPrograms = new ArrayList<>();

    for (OtherStateOrFederalProgramType t : OtherStateOrFederalProgramType.values()) {
      OtherStateOrFederalProgram p = new OtherStateOrFederalProgram();
      p.setName(t.getLabel());
      p.setType(t);
      otherStateOrFederalPrograms.add(p);
    }

    currentOtherInsurance.setOtherStateOrFederalPrograms(otherStateOrFederalPrograms);
    healthCoverage.setCurrentOtherInsurance(currentOtherInsurance);
    householdMember.setHealthCoverage(healthCoverage);

    householdMembers.add(householdMember);

    taxHousehold.setHouseholdMember(householdMembers);

    taxHouseholds.add(taxHousehold);

    return taxHouseholds;
  }

  private Household getHouseholdForUser(AccountUser accountUser) {
    int householdId = accountUser.getActiveModuleId();

    if (householdId == 0) {
      return null;
    }

    return fetchHousehold(householdId);
  }

  private Household getHouseholdByHouseholdId(final BigDecimal householdId) {
    if (householdId == null) {
      return null;
    }

    return fetchHousehold(householdId.longValue());
  }

  private Household fetchHousehold(final long householdId) {
    final String url = String.format("%s/consumer/householdbyhouseholdid/%s", GhixEndPoints.CONSUMER_SERVICE_URL, householdId);
    log.info("Fetching household from ghix-consumer-svc: {}", url);
    Household household = null;

    try {
      household = ghixRestTemplate.getForObject(url, Household.class);
    } catch (Exception e) {
      log.error("Exception occurred while fetching household by id", e);
    }

    return household;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<SsapApplicationResource> getSsapApplicationsForCoverageYear(long householdId, int coverageYear) {
    final String url =
      String.format("%sapplication/applications/coverageyearandcmrhousehold?cmrHouseoldId=%d&coverageYear=%d",
        GhixEndPoints.ELIGIBILITY_URL, householdId, coverageYear);
    // TODO: code is plain junk, takes cmrHouseoldId instead of cmrHouseholdId, just wasted 30 minutes
    //       trying to figure out why it's not returning correct stuff.

    return ghixRestTemplate.getForObject(url, new ParameterizedTypeReference<List<SsapApplicationResource>>() {
    });
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isNewApplicationCreationAllowed(AccountUser user, int coverageYear) {
    boolean allowed = true;
    List<SsapApplicationResource> applicationResources = getSsapApplicationsForCoverageYear(user.getActiveModuleId(), coverageYear);

    Set<String> appStatuses = getStatusCodesForIneligibleNewApplication();

    for (SsapApplicationResource applicationResource : applicationResources) {
      if (appStatuses.contains(applicationResource.getApplicationStatus())) {
        allowed = false;
      } else if (applicationResource.getApplicationStatus().equals(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode())) {
        boolean activeEnrollment = isActiveEnrollment(user, applicationResource.getApplicationId());

        if (activeEnrollment) {
          allowed = false;
        } else {
          applicationResource.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
          closeSsapApplicationByApplicationId(applicationResource.getApplicationId(), applicationResource);
        }
      }
    }
    return allowed;
  }

  /**
   * Returns list of {@link ApplicationStatus} codes that are ineligible for creating new SSAP.
   *
   * @return list of application status codes.
   */
  private Set<String> getStatusCodesForIneligibleNewApplication() {
    Set<String> appStatuses = new HashSet<>();
    appStatuses.add(ApplicationStatus.OPEN.getApplicationStatusCode());
    appStatuses.add(ApplicationStatus.UNCLAIMED.getApplicationStatusCode());
    appStatuses.add(ApplicationStatus.SIGNED.getApplicationStatusCode());
    appStatuses.add(ApplicationStatus.SUBMITTED.getApplicationStatusCode());
    appStatuses.add(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
    return appStatuses;
  }

  private void closeSsapApplicationByApplicationId(Long ssapApplicationId, SsapApplicationResource objSsapApplicationResource) {
    try {
      resourceCreatorImpl.updateSsapApplicationById(ssapApplicationId, objSsapApplicationResource);
    } catch (Exception e) {
      log.error("Unable to close SSAP", e);
    }
  }

  /**
   * Checks if given application id is in active enrollment.
   *
   * @param user          {@link AccountUser} object.
   * @param applicationId application id.
   * @return true if application is in active enrollment and not terminated.
   */
  private boolean isActiveEnrollment(AccountUser user, long applicationId) {
    boolean hasActiveEnrollment = false;
    try {
      hasActiveEnrollment = (ghixRestTemplate.exchange(
        GhixEndPoints.EnrollmentEndPoints.IS_ACTIVE_HEALTH_ENROLLMENT_FOR_APP_ID,
        user.getUsername(),
        HttpMethod.POST,
        MediaType.APPLICATION_JSON,
        Boolean.class,
        applicationId)).getBody();
    } catch (RestClientException e) {
      log.error("Exception occurred while checking for active enrollment", e);
    }
    return hasActiveEnrollment;
  }
  
  /**
   * Sets Application GUID and External ID for each given {@link HouseholdMember}.
   * <p>
   * If there is an already existing user that has the same first name, last name, dob, gender and consumer id,
   * it looks up that existing record for applicant guid and external app id, otherwise creates new GUID for given
   * household member.
   * </p>
   *
   * @param householdId household id.
   * @param members     list of {@link HouseholdMember}
   * @return updated list of {@link HouseholdMember}
   */
  private List<HouseholdMember> setApplicationGuidAndExternalId(int householdId, List<HouseholdMember> members) {
	  List<String> alreadyAssignedAppids = new ArrayList<>();
	  members.stream()
      .filter(m -> m.getApplicantGuid() != null && !m.getApplicantGuid().trim().isEmpty())
      .forEach(householdMember -> {
    	  alreadyAssignedAppids.add(householdMember.getApplicantGuid());
      });
	  
	  members.stream()
      .filter(m -> m.getApplicantGuid() == null || m.getApplicantGuid().trim().equals(""))
      .forEach(householdMember -> {
        Map<String, Object> memberData = new HashMap<>();

        if (householdMember.getName() != null) {
          memberData.put(SsapUtil.FIRST_NAME, householdMember.getName().getFirstName());
          memberData.put(SsapUtil.LAST_NAME, householdMember.getName().getLastName());
        }

        if (householdMember.getSocialSecurityCard() != null) {
          memberData.put(SsapUtil.SOCIAL_SECURITY_NUMBER, householdMember.getSocialSecurityCard().getSocialSecurityNumber());
        }

        memberData.put(SsapUtil.CONSUMER_ID, householdId);
        memberData.put(SsapUtil.BIRTH_DATE, householdMember.getDateOfBirth());
        memberData.put(SsapUtil.GENDER, householdMember.getGender());

        Map<String, String> applicantIds = null;
        try {
          applicantIds = ssapUtil.getApplicantIds(memberData);
        } catch (Exception e) {
          log.error("Exception occurred while getting application GUID and external applicant id for member of household", e);
        }

        if (applicantIds == null || applicantIds.isEmpty()) {
          householdMember.setApplicantGuid(getNextGUID());
        } else if(alreadyAssignedAppids.contains(applicantIds.get(SsapUtil.APPLICANT_GUID))) {
      	  householdMember.setApplicantGuid(getNextGUID());
        } else {
          householdMember.setApplicantGuid(
            applicantIds.getOrDefault(SsapUtil.APPLICANT_GUID, getNextGUID())
          );

          householdMember.setExternalId(
            applicantIds.get(SsapUtil.EXTERNAL_APPLICANT_ID)
          );
        }
      });

    return members;
  }

  /**
   * Returns database record (ssap_applications) by given case number.
   *
   * @param caseNumber
   * @return
   */
  @Override
  public com.getinsured.iex.ssap.model.SsapApplication getSsapApplicationByCaseNumber(final String caseNumber) {
    return ssapApplicationRepository.findByCaseNumber(caseNumber);
  }

  private String mecCheckIntegration(String caseNumber) {
    /* It always returns N in old code */
    return "N";
  }

  // TODO: this copied from old code.
  private Location saveMailingLocation(List<HouseholdMember> members, Map<String, SsapApplicantResource> applicantMap) {
    Location mailingLocation;
    String primaryApplicantGuid = getPrimaryApplicantGuid(members);
    if (applicantMap != null && applicantMap.containsKey(primaryApplicantGuid)) {
      mailingLocation = locationService.findById(applicantMap.get(primaryApplicantGuid).getMailiingLocationId() == null ? 0 :
        applicantMap.get(primaryApplicantGuid).getMailiingLocationId().intValue());
      if (mailingLocation == null) {
        mailingLocation = new Location();
      }
    } else {
      mailingLocation = new Location();
    }
    mailingLocation.setAddress1(members.get(0).getHouseholdContact().getMailingAddress().getStreetAddress1());
    mailingLocation.setAddress2(members.get(0).getHouseholdContact().getMailingAddress().getStreetAddress2());
    mailingLocation.setCity(members.get(0).getHouseholdContact().getMailingAddress().getCity());
    mailingLocation.setState(members.get(0).getHouseholdContact().getMailingAddress().getState());
    mailingLocation.setZip(members.get(0).getHouseholdContact().getMailingAddress().getPostalCode());
    mailingLocation.setCounty(members.get(0).getHouseholdContact().getMailingAddress().getCounty());
    locationService.save(mailingLocation);
    return mailingLocation;
  }

  private Location savePrimaryLocation(List<HouseholdMember> members, Map<String, SsapApplicantResource> applicantMap) {
    Location primaryLocation;
    String primaryApplicantGuid = getPrimaryApplicantGuid(members);
    if (applicantMap != null && applicantMap.containsKey(primaryApplicantGuid)) {
      primaryLocation = locationService.findById(applicantMap.get(primaryApplicantGuid).getOtherLocationId() == null ? 0 :
        applicantMap.get(primaryApplicantGuid).getOtherLocationId().intValue());
      if (primaryLocation == null) {
        primaryLocation = new Location();
      }
    } else {
      primaryLocation = new Location();
    }
    primaryLocation.setAddress1(members.get(0).getHouseholdContact().getHomeAddress().getStreetAddress1());
    primaryLocation.setAddress2(members.get(0).getHouseholdContact().getHomeAddress().getStreetAddress2());
    primaryLocation.setCity(members.get(0).getHouseholdContact().getHomeAddress().getCity());
    primaryLocation.setState(members.get(0).getHouseholdContact().getHomeAddress().getState());
    primaryLocation.setZip(members.get(0).getHouseholdContact().getHomeAddress().getPostalCode());
    primaryLocation.setCounty(members.get(0).getHouseholdContact().getHomeAddress().getCounty());
    locationService.save(primaryLocation);
    return primaryLocation;
  }

  private String getNextGUID() {
    return ssapUtil.getNextSequenceFromDB(SSAP_APPLICANT_DISPLAY_ID);
  }

  private String getPrimaryApplicantGuid(List<HouseholdMember> householdMemberList) {
    Optional<HouseholdMember> hhmOptional = householdMemberList.stream().filter(householdMember -> householdMember.getPersonId() == 1).findFirst();

    if (hhmOptional.isPresent()) {
      HouseholdMember primaryMember = hhmOptional.get();
      return primaryMember.getApplicantGuid();
    }

    return null;
  }

  /**
   * If anyone in the household is Alaska Native/American Indian returns {@code true}.
   *
   * @param application {@link SingleStreamlinedApplication} object.
   * @return true if anyone is AN/AI in household for this application.
   */
  private boolean isAnyoneAlaskaNativeOrAmericanIndian(final SingleStreamlinedApplication application) {
    // TODO: Do we need to only consider primary tax household if any members in there have this status?
    if (application != null && application.getTaxHousehold() != null && !application.getTaxHousehold().isEmpty()) {
      final List<HouseholdMember> householdMembers = application.getTaxHousehold().get(0).getHouseholdMember();
      return householdMembers.stream().anyMatch(hhm -> hhm.getAmericanIndianAlaskaNative() != null && hhm.getAmericanIndianAlaskaNative().getMemberOfFederallyRecognizedTribeIndicator() == Boolean.TRUE);
    } else {
      log.error("Cannot find out if anyone on SSAP [id: {}] is AN/AI, didn't pass null checks.", application != null ? application.getSsapApplicationId() : null);
    }

    return false;
  }

  private String getApplicantRelation(List<BloodRelationship> bloodRelationship, Long personId) {
    if (bloodRelationship != null && bloodRelationship.size() > 0) {
      for (BloodRelationship relation : bloodRelationship) {
        String individualPersonId = relation.getIndividualPersonId();
        String relatedPersonId = relation.getRelatedPersonId();
        if (StringUtils.isNotEmpty(individualPersonId) && individualPersonId.equals(personId.toString())
          && StringUtils.isNotEmpty(relatedPersonId) && "1".equals(relatedPersonId)) {
          return relation.getRelation();
        }
      }
    }
    return null;
  }

  private String getYNForBoolean(final Boolean b) {
    return b != null && b ? "Y" : "N";
  }

  /**
   * {@inheritDoc }
   */
  @Override
  public List<Map<String, String>> populateCountiesForState(final String stateCode) {
    if (stateCode == null) {
      return new ArrayList<>();
    }

    List<ZipCode> zipCodeList = zipCodeService.getCountyListForState(stateCode);
    List<Map<String, String>> counties = new ArrayList<>(zipCodeList.size());

    return filterCountyList(counties, zipCodeList);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Map<String, String>> populateCountiesForStateAndZipCode(final String stateCode, final String zipCode) {
    if (stateCode == null || zipCode == null) {
      return new ArrayList<>();
    }

    List<ZipCode> zipCodeList = zipCodeService.getCountyListForZipAndState(zipCode, stateCode);
    List<Map<String, String>> counties = new ArrayList<>(zipCodeList.size());

    return filterCountyList(counties, zipCodeList);
  }

  /**
   * Filters county list to make sure that there is no null or empty county names.
   *
   * @param counties    list of counties to populate.
   * @param zipCodeList list of {@link ZipCode} objects to filter.
   * @return filtered list.
   */
  private List<Map<String, String>> filterCountyList(final List<Map<String, String>> counties, final List<ZipCode> zipCodeList) {
    if (zipCodeList != null) {
      zipCodeList.stream().filter(z -> z != null && z.getCounty() != null && !z.getCounty().trim().isEmpty()).forEach(z -> {
        Map<String, String> m = new HashMap<>(1);
        m.put(z.getCountyCode(), z.getCounty());
        if (!counties.contains(m)) {
          counties.add(m);
        }
      });
    }

    return counties;
  }

}
