
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.ProperNameTextType;


/**
 * 
 *                 A data type created simply to restrict the length of middle name, such as the parts of a PersonMiddleName.
 *             
 * 
 * <p>Java class for RestrictedMiddleNameType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RestrictedMiddleNameType">
 *   &lt;simpleContent>
 *     &lt;restriction base="&lt;http://niem.gov/niem/niem-core/2.0>ProperNameTextType">
 *     &lt;/restriction>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RestrictedMiddleNameType")
public class RestrictedMiddleNameType
    extends ProperNameTextType
{


}
