package com.getinsured.eligibility.at.ref.service;

import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;


/**
 * @author chopra_s
 * 
 */
public interface LceProcessHandlerService {

	String ERROR_MOVE_ENROLLMENT = "An error occurred while invoking Move Enrollment API for Application Id - ";

	String ERROR_LCE_NOTIFICATION = "An error occurred while sending the LCE notification";
	
	String ERROR_PLAN_AVAILABLE = "No response returned from Plan availability API for Application Id - ";
	
	String APP_EVENT_TYPE = "APPEVENT_APPLICATION";

	void execute(LCEProcessRequestDTO lceProcessRequestDTO);

}
