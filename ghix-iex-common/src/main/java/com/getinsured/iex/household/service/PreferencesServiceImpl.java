/**
 * 
 */
package com.getinsured.iex.household.service;

import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.iex.household.repository.HouseholdRepository;

/**
 * @author vishwanath_s
 *
 */
@Service
public class PreferencesServiceImpl implements PreferencesService{

	private static final Logger LOGGER = Logger
			.getLogger(PreferencesServiceImpl.class);
	
	public static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";
	
	@Autowired 
	private LookupService lookupService;
	
	@Autowired
	private HouseholdRepository householdRepository;
	
	@Override
	public PreferencesDTO getPreferences(Integer householdId, boolean editMode) {
		LOGGER.info("PreferencesService : getPreferences "); 

		PreferencesDTO communicationPreferenceDTO = null;

		Household household = householdRepository.findOne(householdId);
		if (null == household){
		 	LOGGER.error("ConsumerService.populatePreferences - Cannot find a household record with id " 
		 					+ SecurityUtil.sanitizeForLogging(""+householdId));
		} else {
			communicationPreferenceDTO = populatePreferences(household, editMode);
		}
		
		return communicationPreferenceDTO;
	}
	
	/**
	 * Populate Communication Preference page details
	 * @param activeModuleId
	 * @param editMode
	 * @return
	 */
	private PreferencesDTO populatePreferences(Household household, boolean editMode) {
		LOGGER.info("PopulatePreferences - Start");
		PreferencesDTO communicationPreferenceDTO = new PreferencesDTO();

		if (household.getPrefSpokenLang() != null) {
			communicationPreferenceDTO.setPrefSpokenLang(getLookupValueLabel(household.getPrefSpokenLang()));
		}
		if (household.getPrefWrittenLang() != null) {
			communicationPreferenceDTO.setPrefWrittenLang(getLookupValueLabel(household.getPrefWrittenLang()));
		}
		
		if (household.getPrefContactMethod() != null) {
				try {
					communicationPreferenceDTO.setPrefCommunication(GhixNoticeCommunicationMethod.valueOf(getLookupValueLabel(household.getPrefContactMethod())));
				} catch (IllegalArgumentException | NullPointerException ex) {
					LOGGER.error("Invalid Preferred Communication Method found");
				}
			}

		communicationPreferenceDTO.setEmailAddress(household.getEmail());
		communicationPreferenceDTO.setPhoneNumber(household.getPhoneNumber());
		Location location = household.getPrefContactLocation();

		if (location != null) {
			LocationDTO locationDto = new LocationDTO();
			locationDto.setAddressLine1(location.getAddress1());
			locationDto.setAddressLine2(location.getAddress2());
			locationDto.setCity(location.getCity());
			locationDto.setState(location.getState());
			locationDto.setZipcode(location.getZip());
			locationDto.setCountyName(location.getCounty());
			locationDto.setCountyCode(location.getCountycode());
			communicationPreferenceDTO.setLocationDto(locationDto);
		}

		// Populate Date of birth only during registration
		if (!editMode && household.getBirthDate() != null) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat(SHORT_DATE_FORMAT);
				formatter.setLenient(false);
				communicationPreferenceDTO.setDob(formatter.format(household.getBirthDate()));
			} catch (Exception ex) {
				LOGGER.error("ConsumerService.populatePreferences Date Of Birth Format error:", ex);
			}
		}
		communicationPreferenceDTO.setTextMe(null==household.getTextMe()?false:household.getTextMe());
		communicationPreferenceDTO.setMobileNumberVerified(null==household.getMobileNumberVerified()?false:household.getMobileNumberVerified());
		
		communicationPreferenceDTO.setOptInPaperless1095(null==household.getOptInPaperless1095()?false:household.getOptInPaperless1095());
		communicationPreferenceDTO.setPaperless1095Date(household.getPaperless1095Date());
		communicationPreferenceDTO.setPaperless1095User(household.getPaperless1095User());

		communicationPreferenceDTO.setEditMode(editMode);
		
		LOGGER.info("PopulatePreferences - END");
        return communicationPreferenceDTO;
	}
	
	private String getLookupValueLabel(Integer lookupValueId){
		String lookupValueLabel = null;
		LookupValue lookupValue = lookupService.findLookupValuebyId(lookupValueId);
		if(null!=lookupValue && StringUtils.isNotBlank(lookupValue.getLookupValueLabel())){
			lookupValueLabel = lookupValue.getLookupValueLabel();
		}
		return lookupValueLabel;
	}
	
}
