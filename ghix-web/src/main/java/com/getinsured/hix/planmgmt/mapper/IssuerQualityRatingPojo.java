package com.getinsured.hix.planmgmt.mapper;

import java.util.Date;


public class IssuerQualityRatingPojo {
	private int issuerId=0;
	private String effectiveDate;
	private String globalRating;
	private String notExistIssuer;
	private String companyLegal;
	private String HIOSId;
	private String errorFlag;
	private String hiosPlanId;
	private Integer createdBy;
	private Integer lastUpdatedBy;
	private Date lastUpdateTimestamp;
	private Date creationTimestamp;
	private String globalRatingError;
	
	/********** NON CA STATE EXCHANGE QUALITY RATING PROPERTIES ***********/
	
	private String accessRating;
	private String gettingTimelyAppointmentscare;
	private String gettingCarewithYourPersonalDoctor;
	private String stayingHealthyRating;
	private String checkingForCancer;
	private String helpForHealthyBehaviors;
	private String wellnessCareForChildren;
	private String planServiceRating;
	private String planCustomerService;
	private String gettingClaimsPaid;
	private String shoppingServicestoFindAffordableMedicalCare;
	private String gettinganInterpreterWhenSeeingTheDoctor;
	private String memberComplaintsAppeals;
	private String clinicalCareRating;
	private String gettingtheRighSafeCare;
	private String diabetesCare;
	private String heartCare;
	private String mentalHealthCare;
	private String respiratoryCare;
	private String maternityCare;
	
	/********** CA STATE EXCHANGE QUALITY RATING PROPERTIES ***********/
	
	private String summaryClinicalQualityManagement;
	private String clinicalEffectiveness;
	private String asthmaCare;
	private String behavioralHealth;
	private String cardiovascularCare;
	private String domainPatientSafety;
	private String patientSafety;
	private String prevention;
	private String maternalCare;
	private String stayingHealthyAdult;
	private String stayingHealthyChild;
	private String summaryEnrolleeExperience;
	private String domainAccessToCare;
	private String accessToCare;
	private String domainCareCoordination;
	private String careCoordination;
	private String domainDoctorAndCare;
	private String doctorAndCare;
	private String summaryPlanEfficiencyAffordabilityAndManagement;
	private String efficiencyAndAffordability;
	private String efficientCare;
	private String planService;
	private String enrolleeExperienceWithHealthPlan;
	/**
	 * @return the issuer
	 */
	public int getIsuerId() {
		return issuerId;
	}
	/**
	 * @param issuer the issuer to set
	 */
	public void setIsuerId(int issuer) {
		this.issuerId = issuer;
	}
	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}
	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	/**
	 * @return the globalRating
	 */
	public String getGlobalRating() {
		return globalRating;
	}
	/**
	 * @param globalRating the globalRating to set
	 */
	public void setGlobalRating(String globalRating) {
		this.globalRating = globalRating;
	}
	/**
	 * @return the notExistIssuer
	 */
	public String getNotExistIssuer() {
		return notExistIssuer;
	}
	/**
	 * @param notExistIssuer the notExistIssuer to set
	 */
	public void setNotExistIssuer(String notExistIssuer) {
		this.notExistIssuer = notExistIssuer;
	}
	/**
	 * @return the companyLegal
	 */
	public String getCompanyLegal() {
		return companyLegal;
	}
	/**
	 * @param companyLegal the companyLegal to set
	 */
	public void setCompanyLegal(String companyLegal) {
		this.companyLegal = companyLegal;
	}
	/**
	 * @return the hIOSId
	 */
	public String getHIOSId() {
		return HIOSId;
	}
	/**
	 * @param hIOSId the hIOSId to set
	 */
	public void setHIOSId(String hIOSId) {
		HIOSId = hIOSId;
	}
	/**
	 * @return the errorFlag
	 */
	public String getErrorFlag() {
		return errorFlag;
	}
	/**
	 * @param errorFlag the errorFlag to set
	 */
	public void setErrorFlag(String errorFlag) {
		this.errorFlag = errorFlag;
	}
	/**
	 * @return the hiosPlanId
	 */
	public String getHIOSPlanId() {
		return hiosPlanId;
	}
	/**
	 * @param hIOSPlanId the hiosPlanId to set
	 */
	public void setHIOSPlanId(String hiosPlanId) {
		this.hiosPlanId = hiosPlanId;
	}
	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	/**
	 * @return the lastUpdateTimestamp
	 */
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	/**
	 * @param lastUpdateTimestamp the lastUpdateTimestamp to set
	 */
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	/**
	 * @return the creationTimestamp
	 */
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	/**
	 * @param creationTimestamp the creationTimestamp to set
	 */
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	/**
	 * @return the globalRatingError
	 */
	public String getGlobalRatingError() {
		return globalRatingError;
	}
	/**
	 * @param globalRatingError the globalRatingError to set
	 */
	public void setGlobalRatingError(String globalRatingError) {
		this.globalRatingError = globalRatingError;
	}
	/**
	 * @return the accessRating
	 */
	public String getAccessRating() {
		return accessRating;
	}
	/**
	 * @param accessRating the accessRating to set
	 */
	public void setAccessRating(String accessRating) {
		this.accessRating = accessRating;
	}
	/**
	 * @return the gettingTimelyAppointmentscare
	 */
	public String getGettingTimelyAppointmentscare() {
		return gettingTimelyAppointmentscare;
	}
	/**
	 * @param gettingTimelyAppointmentscare the gettingTimelyAppointmentscare to set
	 */
	public void setGettingTimelyAppointmentscare(
			String gettingTimelyAppointmentscare) {
		this.gettingTimelyAppointmentscare = gettingTimelyAppointmentscare;
	}
	/**
	 * @return the gettingCarewithYourPersonalDoctor
	 */
	public String getGettingCarewithYourPersonalDoctor() {
		return gettingCarewithYourPersonalDoctor;
	}
	/**
	 * @param gettingCarewithYourPersonalDoctor the gettingCarewithYourPersonalDoctor to set
	 */
	public void setGettingCarewithYourPersonalDoctor(
			String gettingCarewithYourPersonalDoctor) {
		this.gettingCarewithYourPersonalDoctor = gettingCarewithYourPersonalDoctor;
	}
	/**
	 * @return the stayingHealthyRating
	 */
	public String getStayingHealthyRating() {
		return stayingHealthyRating;
	}
	/**
	 * @param stayingHealthyRating the stayingHealthyRating to set
	 */
	public void setStayingHealthyRating(String stayingHealthyRating) {
		this.stayingHealthyRating = stayingHealthyRating;
	}
	/**
	 * @return the checkingForCancer
	 */
	public String getCheckingForCancer() {
		return checkingForCancer;
	}
	/**
	 * @param checkingForCancer the checkingForCancer to set
	 */
	public void setCheckingForCancer(String checkingForCancer) {
		this.checkingForCancer = checkingForCancer;
	}
	/**
	 * @return the helpForHealthyBehaviors
	 */
	public String getHelpForHealthyBehaviors() {
		return helpForHealthyBehaviors;
	}
	/**
	 * @param helpForHealthyBehaviors the helpForHealthyBehaviors to set
	 */
	public void setHelpForHealthyBehaviors(String helpForHealthyBehaviors) {
		this.helpForHealthyBehaviors = helpForHealthyBehaviors;
	}
	/**
	 * @return the wellnessCareForChildren
	 */
	public String getWellnessCareForChildren() {
		return wellnessCareForChildren;
	}
	/**
	 * @param wellnessCareForChildren the wellnessCareForChildren to set
	 */
	public void setWellnessCareForChildren(String wellnessCareForChildren) {
		this.wellnessCareForChildren = wellnessCareForChildren;
	}
	/**
	 * @return the planServiceRating
	 */
	public String getPlanServiceRating() {
		return planServiceRating;
	}
	/**
	 * @param planServiceRating the planServiceRating to set
	 */
	public void setPlanServiceRating(String planServiceRating) {
		this.planServiceRating = planServiceRating;
	}
	/**
	 * @return the planCustomerService
	 */
	public String getPlanCustomerService() {
		return planCustomerService;
	}
	/**
	 * @param planCustomerService the planCustomerService to set
	 */
	public void setPlanCustomerService(String planCustomerService) {
		this.planCustomerService = planCustomerService;
	}
	/**
	 * @return the gettingClaimsPaid
	 */
	public String getGettingClaimsPaid() {
		return gettingClaimsPaid;
	}
	/**
	 * @param gettingClaimsPaid the gettingClaimsPaid to set
	 */
	public void setGettingClaimsPaid(String gettingClaimsPaid) {
		this.gettingClaimsPaid = gettingClaimsPaid;
	}
	/**
	 * @return the shoppingServicestoFindAffordableMedicalCare
	 */
	public String getShoppingServicestoFindAffordableMedicalCare() {
		return shoppingServicestoFindAffordableMedicalCare;
	}
	/**
	 * @param shoppingServicestoFindAffordableMedicalCare the shoppingServicestoFindAffordableMedicalCare to set
	 */
	public void setShoppingServicestoFindAffordableMedicalCare(
			String shoppingServicestoFindAffordableMedicalCare) {
		this.shoppingServicestoFindAffordableMedicalCare = shoppingServicestoFindAffordableMedicalCare;
	}
	/**
	 * @return the gettinganInterpreterWhenSeeingTheDoctor
	 */
	public String getGettinganInterpreterWhenSeeingTheDoctor() {
		return gettinganInterpreterWhenSeeingTheDoctor;
	}
	/**
	 * @param gettinganInterpreterWhenSeeingTheDoctor the gettinganInterpreterWhenSeeingTheDoctor to set
	 */
	public void setGettinganInterpreterWhenSeeingTheDoctor(
			String gettinganInterpreterWhenSeeingTheDoctor) {
		this.gettinganInterpreterWhenSeeingTheDoctor = gettinganInterpreterWhenSeeingTheDoctor;
	}
	/**
	 * @return the memberComplaintsAppeals
	 */
	public String getMemberComplaintsAppeals() {
		return memberComplaintsAppeals;
	}
	/**
	 * @param memberComplaintsAppeals the memberComplaintsAppeals to set
	 */
	public void setMemberComplaintsAppeals(String memberComplaintsAppeals) {
		this.memberComplaintsAppeals = memberComplaintsAppeals;
	}
	/**
	 * @return the clinicalCareRating
	 */
	public String getClinicalCareRating() {
		return clinicalCareRating;
	}
	/**
	 * @param clinicalCareRating the clinicalCareRating to set
	 */
	public void setClinicalCareRating(String clinicalCareRating) {
		this.clinicalCareRating = clinicalCareRating;
	}
	/**
	 * @return the gettingtheRighSafeCare
	 */
	public String getGettingtheRighSafeCare() {
		return gettingtheRighSafeCare;
	}
	/**
	 * @param gettingtheRighSafeCare the gettingtheRighSafeCare to set
	 */
	public void setGettingtheRighSafeCare(String gettingtheRighSafeCare) {
		this.gettingtheRighSafeCare = gettingtheRighSafeCare;
	}
	/**
	 * @return the diabetesCare
	 */
	public String getDiabetesCare() {
		return diabetesCare;
	}
	/**
	 * @param diabetesCare the diabetesCare to set
	 */
	public void setDiabetesCare(String diabetesCare) {
		this.diabetesCare = diabetesCare;
	}
	/**
	 * @return the heartCare
	 */
	public String getHeartCare() {
		return heartCare;
	}
	/**
	 * @param heartCare the heartCare to set
	 */
	public void setHeartCare(String heartCare) {
		this.heartCare = heartCare;
	}
	/**
	 * @return the mentalHealthCare
	 */
	public String getMentalHealthCare() {
		return mentalHealthCare;
	}
	/**
	 * @param mentalHealthCare the mentalHealthCare to set
	 */
	public void setMentalHealthCare(String mentalHealthCare) {
		this.mentalHealthCare = mentalHealthCare;
	}
	/**
	 * @return the respiratoryCare
	 */
	public String getRespiratoryCare() {
		return respiratoryCare;
	}
	/**
	 * @param respiratoryCare the respiratoryCare to set
	 */
	public void setRespiratoryCare(String respiratoryCare) {
		this.respiratoryCare = respiratoryCare;
	}
	/**
	 * @return the maternityCare
	 */
	public String getMaternityCare() {
		return maternityCare;
	}
	/**
	 * @param maternityCare the maternityCare to set
	 */
	public void setMaternityCare(String maternityCare) {
		this.maternityCare = maternityCare;
	}
	/**
	 * @return the summaryClinicalQualityManagement
	 */
	public String getSummaryClinicalQualityManagement() {
		return summaryClinicalQualityManagement;
	}
	/**
	 * @param summaryClinicalQualityManagement the summaryClinicalQualityManagement to set
	 */
	public void setSummaryClinicalQualityManagement(
			String summaryClinicalQualityManagement) {
		this.summaryClinicalQualityManagement = summaryClinicalQualityManagement;
	}
	/**
	 * @return the clinicalEffectiveness
	 */
	public String getClinicalEffectiveness() {
		return clinicalEffectiveness;
	}
	/**
	 * @param clinicalEffectiveness the clinicalEffectiveness to set
	 */
	public void setClinicalEffectiveness(String clinicalEffectiveness) {
		this.clinicalEffectiveness = clinicalEffectiveness;
	}
	/**
	 * @return the asthmaCare
	 */
	public String getAsthmaCare() {
		return asthmaCare;
	}
	/**
	 * @param asthmaCare the asthmaCare to set
	 */
	public void setAsthmaCare(String asthmaCare) {
		this.asthmaCare = asthmaCare;
	}
	/**
	 * @return the behavioralHealth
	 */
	public String getBehavioralHealth() {
		return behavioralHealth;
	}
	/**
	 * @param behavioralHealth the behavioralHealth to set
	 */
	public void setBehavioralHealth(String behavioralHealth) {
		this.behavioralHealth = behavioralHealth;
	}
	/**
	 * @return the cardiovascularCare
	 */
	public String getCardiovascularCare() {
		return cardiovascularCare;
	}
	/**
	 * @param cardiovascularCare the cardiovascularCare to set
	 */
	public void setCardiovascularCare(String cardiovascularCare) {
		this.cardiovascularCare = cardiovascularCare;
	}
	/**
	 * @return the domainPatientSafety
	 */
	public String getDomainPatientSafety() {
		return domainPatientSafety;
	}
	/**
	 * @param domainPatientSafety the domainPatientSafety to set
	 */
	public void setDomainPatientSafety(String domainPatientSafety) {
		this.domainPatientSafety = domainPatientSafety;
	}
	/**
	 * @return the patientSafety
	 */
	public String getPatientSafety() {
		return patientSafety;
	}
	/**
	 * @param patientSafety the patientSafety to set
	 */
	public void setPatientSafety(String patientSafety) {
		this.patientSafety = patientSafety;
	}
	/**
	 * @return the prevention
	 */
	public String getPrevention() {
		return prevention;
	}
	/**
	 * @param prevention the prevention to set
	 */
	public void setPrevention(String prevention) {
		this.prevention = prevention;
	}
	/**
	 * @return the maternalCare
	 */
	public String getMaternalCare() {
		return maternalCare;
	}
	/**
	 * @param maternalCare the maternalCare to set
	 */
	public void setMaternalCare(String maternalCare) {
		this.maternalCare = maternalCare;
	}
	/**
	 * @return the stayingHealthyAdult
	 */
	public String getStayingHealthyAdult() {
		return stayingHealthyAdult;
	}
	/**
	 * @param stayingHealthyAdult the stayingHealthyAdult to set
	 */
	public void setStayingHealthyAdult(String stayingHealthyAdult) {
		this.stayingHealthyAdult = stayingHealthyAdult;
	}
	/**
	 * @return the stayingHealthyChild
	 */
	public String getStayingHealthyChild() {
		return stayingHealthyChild;
	}
	/**
	 * @param stayingHealthyChild the stayingHealthyChild to set
	 */
	public void setStayingHealthyChild(String stayingHealthyChild) {
		this.stayingHealthyChild = stayingHealthyChild;
	}
	/**
	 * @return the summaryEnrolleeExperience
	 */
	public String getSummaryEnrolleeExperience() {
		return summaryEnrolleeExperience;
	}
	/**
	 * @param summaryEnrolleeExperience the summaryEnrolleeExperience to set
	 */
	public void setSummaryEnrolleeExperience(String summaryEnrolleeExperience) {
		this.summaryEnrolleeExperience = summaryEnrolleeExperience;
	}
	/**
	 * @return the domainAccessToCare
	 */
	public String getDomainAccessToCare() {
		return domainAccessToCare;
	}
	/**
	 * @param domainAccessToCare the domainAccessToCare to set
	 */
	public void setDomainAccessToCare(String domainAccessToCare) {
		this.domainAccessToCare = domainAccessToCare;
	}
	/**
	 * @return the accessToCare
	 */
	public String getAccessToCare() {
		return accessToCare;
	}
	/**
	 * @param accessToCare the accessToCare to set
	 */
	public void setAccessToCare(String accessToCare) {
		this.accessToCare = accessToCare;
	}
	/**
	 * @return the domainCareCoordination
	 */
	public String getDomainCareCoordination() {
		return domainCareCoordination;
	}
	/**
	 * @param domainCareCoordination the domainCareCoordination to set
	 */
	public void setDomainCareCoordination(String domainCareCoordination) {
		this.domainCareCoordination = domainCareCoordination;
	}
	/**
	 * @return the careCoordination
	 */
	public String getCareCoordination() {
		return careCoordination;
	}
	/**
	 * @param careCoordination the careCoordination to set
	 */
	public void setCareCoordination(String careCoordination) {
		this.careCoordination = careCoordination;
	}
	/**
	 * @return the domainDoctorAndCare
	 */
	public String getDomainDoctorAndCare() {
		return domainDoctorAndCare;
	}
	/**
	 * @param domainDoctorAndCare the domainDoctorAndCare to set
	 */
	public void setDomainDoctorAndCare(String domainDoctorAndCare) {
		this.domainDoctorAndCare = domainDoctorAndCare;
	}
	/**
	 * @return the doctorAndCare
	 */
	public String getDoctorAndCare() {
		return doctorAndCare;
	}
	/**
	 * @param doctorAndCare the doctorAndCare to set
	 */
	public void setDoctorAndCare(String doctorAndCare) {
		this.doctorAndCare = doctorAndCare;
	}
	/**
	 * @return the summaryPlanEfficiencyAffordabilityAndManagement
	 */
	public String getSummaryPlanEfficiencyAffordabilityAndManagement() {
		return summaryPlanEfficiencyAffordabilityAndManagement;
	}
	/**
	 * @param summaryPlanEfficiencyAffordabilityAndManagement the summaryPlanEfficiencyAffordabilityAndManagement to set
	 */
	public void setSummaryPlanEfficiencyAffordabilityAndManagement(
			String summaryPlanEfficiencyAffordabilityAndManagement) {
		this.summaryPlanEfficiencyAffordabilityAndManagement = summaryPlanEfficiencyAffordabilityAndManagement;
	}
	/**
	 * @return the efficiencyAndAffordability
	 */
	public String getEfficiencyAndAffordability() {
		return efficiencyAndAffordability;
	}
	/**
	 * @param efficiencyAndAffordability the efficiencyAndAffordability to set
	 */
	public void setEfficiencyAndAffordability(String efficiencyAndAffordability) {
		this.efficiencyAndAffordability = efficiencyAndAffordability;
	}
	/**
	 * @return the efficientCare
	 */
	public String getEfficientCare() {
		return efficientCare;
	}
	/**
	 * @param efficientCare the efficientCare to set
	 */
	public void setEfficientCare(String efficientCare) {
		this.efficientCare = efficientCare;
	}
	/**
	 * @return the planService
	 */
	public String getPlanService() {
		return planService;
	}
	/**
	 * @param planService the planService to set
	 */
	public void setPlanService(String planService) {
		this.planService = planService;
	}
	/**
	 * @return the enrolleeExperienceWithHealthPlan
	 */
	public String getEnrolleeExperienceWithHealthPlan() {
		return enrolleeExperienceWithHealthPlan;
	}
	/**
	 * @param enrolleeExperienceWithHealthPlan the enrolleeExperienceWithHealthPlan to set
	 */
	public void setEnrolleeExperienceWithHealthPlan(
			String enrolleeExperienceWithHealthPlan) {
		this.enrolleeExperienceWithHealthPlan = enrolleeExperienceWithHealthPlan;
	}
		
}
