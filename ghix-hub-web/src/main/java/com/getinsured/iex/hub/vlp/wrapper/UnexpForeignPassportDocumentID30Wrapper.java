package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.UnexpForeignPassportDocumentID30Type;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;

/**
 * Wrapper Class for UnexpForeignPassportDocumentID30Type
 * 
 * @author Nikhil Talreja
 * @since  20-Jan-2014 
 * 
 */
public class UnexpForeignPassportDocumentID30Wrapper implements JSONAware {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UnexpForeignPassportDocumentID30Wrapper.class);
	
	private ObjectFactory factory;
	private UnexpForeignPassportDocumentID30Type unexpForeignPassportDocumentID30Type;
	
	private String i94Number;
	private String passportNumber;
	private String countryOfIssuance;
	private String sevisid;
	private String expiryDate;

	private static final String I94_NUMBER_KEY = "I94Number";
	private static final String PASSPORT_NUMBER_KEY = "PassportNumber";
	private static final String COUNTRY_KEY = "CountryOfIssuance";
	private static final String SEVIS_ID_KEY = "SEVISID";
	private static final String EXPIRY_DATE_KEY = "DocExpirationDate";
	

	public UnexpForeignPassportDocumentID30Wrapper(){
		this.factory = new ObjectFactory();
		this.unexpForeignPassportDocumentID30Type = factory.createUnexpForeignPassportDocumentID30Type();
	}
	
	public void setI94Number(String i94Number) throws HubServiceException {
		
		if(i94Number == null){
			throw new HubServiceException("No i94 number provided");
		}
		
		this.i94Number = i94Number;
		this.unexpForeignPassportDocumentID30Type.setI94Number(this.i94Number);
	}
	
	public String getI94Number() {
		return i94Number;
	}
	
	public void setPassportNumber(String passportNumber) throws HubServiceException{
		
		if(passportNumber == null){
			throw new HubServiceException("No Passport number provided");
		}
		
		this.passportNumber = passportNumber;
		this.unexpForeignPassportDocumentID30Type.setPassportNumber(passportNumber);
	}
	
	public String getPassportNumber() {
		return passportNumber;
	}
	
	public void setCountryOfIssuance(String countryOfIssuance) throws HubServiceException{
		
		if(countryOfIssuance == null){
			throw new HubServiceException("No Country of Issuance provided");
		}
		
		this.countryOfIssuance = countryOfIssuance;
		this.unexpForeignPassportDocumentID30Type.setCountryOfIssuance(countryOfIssuance);
	}
	
	public String getCountryOfIssuance() {
		return countryOfIssuance;
	}
	
	public void setSevisid(String sevisid) throws HubServiceException{

		if(sevisid == null){
			throw new HubServiceException("No SEVIS ID provided");
		}
		
		this.sevisid = sevisid;
		this.unexpForeignPassportDocumentID30Type.setSEVISID(this.sevisid);
		
	}
	
	public String getSevisid() {
		return sevisid;
	}

	public void setExpiryDate(String expiryDate) {
		
		try{
			this.unexpForeignPassportDocumentID30Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			this.expiryDate = expiryDate;
		}catch(Exception e){
			LOGGER.error("Exception occured while setting I94 Unexp Foreing Passport Document Expiration date",e);
		}
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}

	
	public UnexpForeignPassportDocumentID30Type getUnexpForeignPassportDocumentID30Type(){
		return this.unexpForeignPassportDocumentID30Type;
	}
	
	/**
	 * Converts the JSON String to UnexpForeignPassportDocumentID30Wrapper object
	 * 
	 * @param jsonString - String representation for UnexpForeignPassportDocumentID30Wrapper
	 * @return UnexpForeignPassportDocumentID30Wrapper Object
	 */
	public static UnexpForeignPassportDocumentID30Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create UnexpForeignPassportDocumentID30Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		UnexpForeignPassportDocumentID30Wrapper wrapper = new UnexpForeignPassportDocumentID30Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(I94_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.i94Number = (String)tmpObj;
			wrapper.unexpForeignPassportDocumentID30Type.setI94Number(wrapper.i94Number);
		}
		
		tmpObj = obj.get(PASSPORT_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.passportNumber = (String)tmpObj;
			wrapper.unexpForeignPassportDocumentID30Type.setPassportNumber(wrapper.passportNumber);
		}
	
		tmpObj = obj.get(COUNTRY_KEY);
		if(tmpObj != null){
			wrapper.countryOfIssuance = (String)tmpObj;
			wrapper.unexpForeignPassportDocumentID30Type.setCountryOfIssuance(wrapper.countryOfIssuance);
		}
		
		tmpObj = obj.get(SEVIS_ID_KEY);
		if(tmpObj != null){
			wrapper.sevisid = (String)tmpObj;
			wrapper.unexpForeignPassportDocumentID30Type.setSEVISID(wrapper.sevisid);
		}
		
		tmpObj = obj.get(EXPIRY_DATE_KEY);
		if(tmpObj != null){
			wrapper.expiryDate = (String)tmpObj;
			try {
				wrapper.unexpForeignPassportDocumentID30Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating UnexpForeignPassportDocumentID30Wrapper from JSON, Invalid expiration date",e);
			}
		}
		
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(I94_NUMBER_KEY, this.i94Number);
		obj.put(PASSPORT_NUMBER_KEY, this.passportNumber);
		obj.put(COUNTRY_KEY, this.countryOfIssuance);
		obj.put(SEVIS_ID_KEY, this.sevisid);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj.toJSONString();
	}
	
	public static UnexpForeignPassportDocumentID30Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		UnexpForeignPassportDocumentID30Wrapper wrapper = new UnexpForeignPassportDocumentID30Wrapper();
		wrapper.setI94Number((String) jsonObj.get(I94_NUMBER_KEY));
		wrapper.setSevisid((String) jsonObj.get(SEVIS_ID_KEY));
		wrapper.setPassportNumber((String) jsonObj.get(PASSPORT_NUMBER_KEY));
		wrapper.setCountryOfIssuance((String) jsonObj.get(COUNTRY_KEY));
		wrapper.setExpiryDate((String) jsonObj.get(EXPIRY_DATE_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(I94_NUMBER_KEY, this.i94Number);
		obj.put(PASSPORT_NUMBER_KEY, this.passportNumber);
		obj.put(COUNTRY_KEY, this.countryOfIssuance);
		obj.put(SEVIS_ID_KEY, this.sevisid);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj;
	}
	
}
