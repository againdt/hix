/**
 * Created by sellathurai_s on 9/19/18.
 */

import moment from 'moment';

class RulesUtil {

    constructor() {
    }

    isTrue(field) {
        return (field && (field.toLowerCase() === "y" || field.toLowerCase() === "yes" || field === true));
    }

    createEvent(json) {
        var event = {};
        event.type = json.type;
        event.params = json.params;
        return event;
    }

    createRule(json) {
        var length = Object.keys(json).length;
        var rules = [];
        for(var i = 0; i < length; i++){
            var rule = {};
            rule.fact = Object.keys(json)[i];
            rule.operator = 'equal';
            rule.value = json[rule.fact];
            rules.push(rule);
        }

        return rules;
    }
};

export default RulesUtil;
