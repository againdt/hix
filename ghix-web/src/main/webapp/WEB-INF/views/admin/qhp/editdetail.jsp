<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
<c:set var="encPlanId" ><encryptor:enc value="${plan.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
			<ul class="page-breadcrumb">
                <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
                <li><a href="#"><spring:message code="label.plans"/></a></li>
                <li><a href="<c:url value="/admin/planmgmt/manageqhpplans"/>"><spring:message code="label.manageQHPS"/></a></li>
                <li><a href="<c:url value="/admin/planmgmt/viewqhpdetail/${encPlanId}"/>"><spring:message code="label.linkPlanDetails"/></a></li>
                <li><spring:message code="label.linkPlanDetails"/></li>
            </ul><!--page-breadcrumb ends-->
			<h1><a name="skip"></a><img class="resize-img" src="<c:url value="/admin/issuer/company/profile/logo/hid/${plan.issuer.hiosIssuerId}"/>"/>${plan.name}</h1>
		</div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message code="label.aboutPlan"/></h4>
       		</div>
       		<!--  beginning of side bar -->
	            <ul class="nav nav-list graybg">
     				<li><a href="<c:url value="/admin/planmgmt/viewqhpdetail/${encPlanId}" />"><spring:message  code='label.linkPlanDetails'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpbenefits/${encPlanId}"/>"><spring:message  code='label.linkPlanBenefits'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhprates/${encPlanId}" />"><spring:message  code='label.linkPlanRates'/></a></li>
                    <c:if test="${displayProviderNetwork == 'YES'}">
                    	<li><a href="<c:url value="/admin/planmgmt/viewqhpprovidernetwork/${encPlanId}" />"><spring:message  code='label.linkProviderNetwork'/></a></li>
					</c:if>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpenrollmentavail/${encPlanId}"/>"><spring:message  code='label.linkEnrollmentAvail'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpcertification/${encPlanId}" />"><spring:message  code='label.linkCertificationStatus'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhphistory/${encPlanId}"/>"><spring:message  code='label.linkPlanHistory'/></a></li>
			   </ul>
				<br /><br />
				<c:if test="${displayAction == 'YES'}">
					<h4 class="margin0"><spring:message code='label.action'/></h4>
					<ul class="nav nav-list graybg">
						<li class="active"><a href="<c:url value="/admin/planmgmt/editqhpdetail/${encPlanId}" />"><i class="icon icon-pencil"></i> <spring:message  code='label.linkEditPlanDetails'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpbenefits/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanBenefits'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhprates/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanRates'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpprovidernetwork/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditProviderNetwork'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpenrollmentavail/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditEnrollmentAvail'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpcertification/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditCertificationStatus'/></a></li>
					</ul>
				</c:if>
        </div><!-- end of span3 -->
		
		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="pull-left"><spring:message code='label.linkEditPlanDetails'/></h4>
				<a class="btn btn-small" title="<spring:message  code="label.btnCancel"/>" href="<c:url value="/admin/planmgmt/viewqhpdetail/${encPlanId}"></c:url>"><spring:message code='label.btnCancel'/></a> 
				<a class="btn btn-primary btn-small" title="<spring:message  code="label.btnSave"/>" href="#" onclick="isExistingPlan();"><spring:message code='label.btnSave'/></a>
			</div>
			<form class="form-horizontal" id="frmqhpEdit" name="frmqhpEdit" action="<c:url value="/admin/planmgmt/editqhpdetail" />" method="POST">
				<df:csrfToken/>
				<table class="table table-border-none">
					<!-- <div class="well well-small">Plan Details
					<div id="edit-button" class="pull-right">
						<div class="pull-right">
							<a href='<c:url value="/admin/planmgmt/viewqhpdetail/${plan.id}"></c:url>' class="btn">Cancel</a>
							<a href='#' class="btn btn-primary" onclick="isExistingPlan();">Submit</a>
						</div>
					</div>-->
					<tr>
						<td class="txt-right span3"><label for="issuer"><spring:message  code='label.issuerName'/></label></td>
						<td><div id="issuername" class="input">
								
							</div><div id="issuername_error" class="help-inline"></div></td>
					</tr>
					<tr>
						<td class="txt-right"><label for="plan_name"><spring:message code='label.planName'/></label></td>
						<td><strong>${plan.name}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><label for="plan_number"><spring:message code='label.planNumber'/></label></td>
						<td><strong>${plan.issuerPlanNumber}</strong>
						<input type="hidden" id="issuerPlanNumber" name="issuerPlanNumber" value="<encryptor:enc value="${plan.issuerPlanNumber}"></encryptor:enc>"/>
						<div id="issuerPlanNumber_error" class="help-inline"></div></td>
					</tr>
					<tr>
						<td class="txt-right"><label for="plan_market"><spring:message code="label.planMarket"/></label></td>
						<td>
							<select class="input-medium" name="market" id="market">
								<c:forEach var="qhpmarket" items="${qhpmarketList}">
									<option <c:if test="${plan.market==qhpmarket.key}">selected</c:if> value="${qhpmarket.key}">${qhpmarket.value}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.planLevel"/></td>
						<td><select class="input-medium" name="planHealth.planLevel" id="planHealthplanLevel" disabled="disabled">
								<option value="">Choose</option>
								<c:forEach var="qhplevel" items="${qhplevelList}">
									<option <c:if test="${plan.planHealth.planLevel==qhplevel.key}">selected</c:if> value="${qhplevel.key}">${qhplevel.value}</option>						    		
								</c:forEach>
								
							</select>
							<input type="hidden" id="planLevelReadOnly" name="planLevelReadOnly" value="${plan.planHealth.planLevel}" /></td> 
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.ehbCovered"/></td>
						<td><select class="input-medium" name="planHealth.ehbCovered" id="planHealthEhbCoveredLevel">								
								<c:forEach var="ehbCovered" items="${ehbCoveredList}">
								    <option <c:if test="${plan.planHealth.ehbCovered==ehbCovered.key}">selected</c:if> value="${ehbCovered.key}">${ehbCovered.value}</option>
								</c:forEach>	
								
							</select>
							<input type="hidden" id="planEhbCoveredReadOnly" name="planEhbCoveredReadOnly" value="${plan.planHealth.ehbCovered}" /> 
					</tr>
					<tr>
						<td class="txt-right"><label for="plan_HSA"><spring:message  code="label.hsa"/></label></td>
						<td>
							<select class="input-medium" id="hsa" name="hsa">								
								<c:forEach var="qhpHsa" items="${qhpHsaList}">
									<option <c:if test="${plan.hsa==qhpHsa.key}">selected</c:if> value="${qhpHsa.key}">${qhpHsa.value}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.startDate}" var="stDate"/>
					<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.endDate}" var="enDate"/>
					<tr>
						<td class="txt-right"><label for="plan_start"><spring:message  code="label.planStartDate"/></label></td>
						<td><input type="text" title="MM/dd/yyyy" class="datepick input-small" name="startDate" id="startDate" readonly="true" value="<c:out value="${stDate}"/>" /></td>
					</tr>
					<tr>
						<td class="txt-right"><label for="plan_end"><spring:message  code="label.planEndDate"/></label></td>
						<td><input type="text" title="MM/dd/yyyy" class="datepick input-small" name="endDate" id="endDate"  readonly="true" value="<c:out value="${enDate}"/>"/><div id="endDate_error" class="help-inline"></div></td>
						
					</tr>
					<tr>
						<td class="txt-right"><label for="formularlyId"><spring:message  code="label.formularlyId"/></label></td>
						<td><strong>${plan.formularlyId}</strong>
					</tr>
					<tr>
						<td class="txt-right"><label for="formularlyUrl"><spring:message  code="label.formularlyURL"/></label></td>
						<td><strong>${plan.formularlyUrl}</strong>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.enrollAvail"/></td>
						<td><strong>${enrollAvailability}</strong> <a href="<c:url value="/admin/planmgmt/editqhpenrollmentavail/${encPlanId}" />"><spring:message  code="button.label"/>edit</a><input type="hidden" name="planHealth.acturialValueCertificate" id="planHealthacturialValueCertificate" value="${planHealthacturialValueCertificate}" /></td>
					</tr><tr><td>
				<input type="hidden" id="rateFile" name="rateFile"/>
			
				<input type="hidden" id="id" name="id" value="<encryptor:enc value="${plan.id}"></encryptor:enc>"/>
				<input type="hidden" id="name" name="name" value="<encryptor:enc value="${plan.name}"></encryptor:enc>"/>
				<input type="hidden" id="maxEnrolee" name="maxEnrolee" value="<encryptor:enc value="${plan.maxEnrolee}"></encryptor:enc>"/>
				<input type="hidden" id="isEdit" name="isEdit" value="${isEdit}"/></td><td></td>
					</tr>
				</table>
			</form>
			<%-- <form class="form-horizontal form-bottom" id="frmCerti" name="frmCerti" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
				<table class="table table-condensed table-border-none">
				<tr>
					<td class="txt-right"><label for="actuarialCertificate"><spring:message code="label.valueCertificate"/></label></td>
					<td><input type="file" id="certificates" name="certificates"  class="input-file">
						&nbsp; <button class="btn"><spring:message code="label.upload"/></button>
						<input type="hidden" name="fileType" value="certificates"/>
						<div id="certificatesFileName" class="help-inline">${acturialValueCertificateOrig}</div>
							<div id="planHealthacturialValueCertificate_error" class=""></div></td>		
						</tr>
				</table>
				</form> --%>
		</div><!-- end of .span9 -->
	</div><!--  end of .row-fluid -->
</div><!--  end of .gutter -->

<script type="text/javascript">
$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	})
	$('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/calendar.gif';
		$(this).datepicker({
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true
		});
	});
	
	getIssuerNames();
	
	var accreditationUpload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitAccreditationRequest,  // pre-submit callback 
	        success:       submitAccreditationResponse  // post-submit callback 
	    };  
    $('#frmCerti').ajaxForm(accreditationUpload);
    if(('${plan.id}' >0) && '${plan.planHealth.planLevel}' == 'Silver'  )
        {
    		$("#planHealthplanLevel").val('${plan.planHealth.planLevel}'); 
   			$("#planHealthplanLevel").attr("disabled", true);
        }
});

function submitAccreditationResponse(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	if(val[0] != 'Error')
	{	
		$("#certificatesFileName").text(val[1]);
		$("#planHealthacturialValueCertificate").val(val[2]);
		$("#planHealthacturialValueCertificate_error").val("");
	}
	else
	{
		$("#planHealthacturialValueCertificate_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
	}
}

function submitAccreditationRequest(formData, jqForm, options) { 
	var fileName = $('#certificates').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}


	function getIssuerNames() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/loader.gif';
		$("#issuername").html("<div class=\"issuerloader\"><img src=\"" + imgpath + "\" alt='Issuer loader'></div>");
		$.ajax({
			  type: 'GET',
			  url: "../getissuers",
			  success: function(data) {
			 	$("#issuername").html("");
				$("#issuername").html(data);
				$("#issuerId").val('${plan.issuer.id}');
				
				var el = document.getElementById("issuerId");
				for(var i=0; i<el.options.length; i++) {
				  if ( el.options[i].value == '${plan.issuer.id}' ) {
				    el.selectedIndex = i;
				    break;
				  }
				}
	           }
		});
}
</script>

<script type="text/javascript">

$.validator.addMethod("endDate", function(value, element) {
var startdatevalue = $('#startDate').val();
if (value == null || value.length <1)
	return true;
return Date.parse(startdatevalue) < Date.parse(value);
});
$.validator.addMethod("numericRegex", function(value, element) {
    return this.optional(element) || /^[0-9]+$/i.test(value);
});
$.validator.addMethod("planLevel", function(value, element) {
	return !(value == null || value.length <1);
});

$.validator.addMethod("maxlength", function (value, element, len) {
	   return value == "" || value.length <= len;
	});

var validator = $("#frmqhpEdit").validate({ 
	 ignore: "",
	rules : {issuername : { required : true},
		issuerPlanNumber : { required : true,
			maxlength : '30'},
		startDate : { required : true},
		endDate : { endDate : true},
		maxEnrolee : {	numericRegex : true},
		"planHealth.planLevel" : {planLevel : true},
		"planHealth.acturialValueCertificate" : { required : true}
	}, 
	messages : {
		issuername : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.issuerName'/></span>"},
		name : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.planName'/></span>",
			maxlength : "<span> <em class='excl'>!</em> <spring:message  code='label.planName'/> exceeds Maximum length.</span>"},
		issuerPlanNumber : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.planNumber'/></span>",
		maxlength : "<span> <em class='excl'>!</em> <spring:message  code='label.planName'/> exceeds Maximum length.</span>"},
		maxEnrolee : { numericRegex : "<span> <em class='excl'>!</em><spring:message  code='label.maximumEnrollment'/> <spring:message  code='label.planmaxEnrollment'/></span>" },
		startDate : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.planStartDate'/></span>"},
		endDate : { endDate : "<span> <em class='excl'>!</em><spring:message  code='label.validateEndDate'/></span>"},
		"planHealth.planLevel" : { planLevel : "<span> <em class='excl'>!</em><spring:message  code='label.showSelect'/><spring:message  code='label.planLevel'/></span>"},
		"planHealth.acturialValueCertificate" : { required : "<span> <em class='excl'>!</em>Upload <spring:message  code='label.valueCertificate'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		if(elementId == 'planHealthacturialValueCertificate')
			{
			$("#" + elementId + "_error").html("");
			}
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
		
	} 
});
jQuery("#frmCerti").validate({});
</script>

<script type="text/javascript">

function  formSubmit()	{
	$("#frmqhpEdit").submit();
}

var oldPlanName='${plan.name}';
var oldIssuerId ='${plan.issuer.id}';
function isExistingPlan() {
	if($("#frmqhpEdit").valid()) 	{
		var ctx = "${pageContext.request.contextPath}";
		if($('#issuerId').val() == oldIssuerId && oldPlanName == $('#name').val()) {
			formSubmit();
		} else {
			$.ajax({
			  type: 'GET',
			  url: "../isExistingPlan",
			  data:{"issuerId":$('#issuerId').val(),"name":$('#name').val(),"oldPlanName":oldPlanName},
			  success: function(data) {
				 	if(data=='true')
				 	{
				 		$("#issuername_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message code='label.alert'/></span></label>");
						$("#issuername_error").attr('class','error help-inline');
				 	}
				 	else
				 	{
				 		formSubmit();
				 	}
					
	            }	  
			});
		}
	}
}
</script>
