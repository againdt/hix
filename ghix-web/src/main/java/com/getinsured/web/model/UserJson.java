package com.getinsured.web.model;

import com.getinsured.hix.model.AccountUser;

/**
 * User JSON object to be send to environment.
 * <p>
 *   Constructor takes {@link AccountUser} and only gets required fields
 *   that are needed for the {@link com.getinsured.web.rest.EnvironmentController} environment api.
 * </p>
 * @author Yevgen Golubenko
 * @since 2/15/19
 */
public class UserJson {
  private long userId;
  private String firstName;
  private String lastName;
  private String role;
  private String landingPage;
  private String userName;
  private int activeModuleId;
  private String activeModuleName;

  public UserJson(AccountUser user) {
    this.userId = user.getId();
    this.firstName = user.getFirstName();
    this.lastName = user.getLastName();
    this.role = user.getDefRole().getName();
    this.landingPage = user.getDefRole().getLandingPage();
    this.userName = user.getUserName();
    this.activeModuleId = user.getActiveModuleId();
    this.activeModuleName = user.getActiveModuleName();
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getLandingPage() {
    return landingPage;
  }

  public void setLandingPage(String landingPage) {
    this.landingPage = landingPage;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public int getActiveModuleId() {
    return activeModuleId;
  }

  public void setActiveModuleId(int activeModuleId) {
    this.activeModuleId = activeModuleId;
  }

  public String getActiveModuleName() {
    return activeModuleName;
  }

  public void setActiveModuleName(String activeModuleName) {
    this.activeModuleName = activeModuleName;
  }
}
