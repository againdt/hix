package com.getinsured.consumer.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.consumer.Household;


@Repository
@Transactional
public interface IHouseholdRepository extends JpaRepository<Household, Integer>{

	
	/**
	 * Fetches the Household for a given User
	 * @param userId 
	 * @return Household associated with that user
	 */
	@Query("FROM Household hh where hh.user.id = :userId")
	Household findByUserId(@Param("userId") Integer userId); 
	
	/**
	 * Fetches Household given a elig_lead_id
	 * @param eligLeadId
	 * @return
	 */
	@Query("FROM Household hh where hh.eligLead.id = :eligLeadId")
	Household findByEligLeadId(@Param("eligLeadId") Long eligLeadId);
	
	@Query("FROM Household hh where hh.giHouseholdId = :giHouseholdId")
	Household findByGIHouseholdId(@Param("giHouseholdId") String giHouseholdId);
	
	@Query("SELECT hh FROM Household as hh where hh.id IN (:hhIdList) ")
	List<Household> findHouseholds(@Param("hhIdList") List<Integer> hhIdList);
	
	@Query("FROM Household hh where hh.householdCaseId = :caseId")
	Household findByCaseId(@Param("caseId") String caseId);
}
