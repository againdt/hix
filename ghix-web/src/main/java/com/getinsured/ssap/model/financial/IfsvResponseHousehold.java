package com.getinsured.ssap.model.financial;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Describes IFSV response household details.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-12
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IfsvResponseHousehold {
  @JsonProperty("ApplicantVerification")
  List<IfsvApplicantVerification> applicantVerification;

  public List<IfsvApplicantVerification> getApplicantVerification() {
    return applicantVerification;
  }

  public void setApplicantVerification(List<IfsvApplicantVerification> applicantVerification) {
    this.applicantVerification = applicantVerification;
  }
}
