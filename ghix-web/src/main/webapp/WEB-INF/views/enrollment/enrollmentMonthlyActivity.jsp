<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
    <head>
     <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">--%>
    <!--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">-->
    <link rel="stylesheet" href="/hix/resources/css/ui-grid.css" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
    <link rel="stylesheet" href="/hix/resources/css/workbench.css">
    </head>

    <body ng-app="reconciliationWorkbenchApp">


    <%--<div class="container">--%>
    <%--<div class="row">--%>
    <%--</div>--%>

    <div id="exTab2" style="margin-bottom: 10px;">

    <ul class="nav nav-tabs" id="myTab">
        <li class="active">
            <a  ui-sref="home" >Monthly Activity</a>
        </li>
        <li>
            <a ui-sref="search" >Search</a>
        </li>
        <%--<li>--%>
            <%--<a ui-sref="detail" >Discrepancy Detail</a>--%>
        <%--</li>--%>
    </ul>

    <!--<ul class="nav nav-tabs">-->
    <!--<li class="active">-->
    <!--<a  href="#/home" data-toggle="tab">Monthly Activity</a>-->
    <!--</li>-->
    <!--<li>-->
    <!--<a href="#/search" data-toggle="tab">Search</a>-->
    <!--</li>-->
    <!--</ul>-->

    <!--<div class="tab-content ">-->
    <!--<div class="tab-pane">-->
    <!--<div ui-view></div>-->
    <!--</div>-->
    <!--</div>-->
    </div>

    <!--<ul class="list-inline header-tabs">-->
    <!--<li><a class="header-selected"href="#"><h3><i class="font-awesome-color" aria-hidden="true"></i> Monthly Activity</h3></a></li>-->
    <!--<li><a class="header-not-selected" ui-sref="search"><h3><i class="ont-awesome-color" aria-hidden="true"></i> Search</h3></a></li>-->
    <!--</ul>-->

    <input id="populateCarrierNMonthJson" name="populateCarrierNMonthJson" type="hidden" value='${populateCarrierNMonthJson}'/>
    <input id="monthlyReconDataJson" name="monthlyReconDataJson" type="hidden" value='${monthlyReconDataJson}'/>
    <input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>
    <input id="redirectEnrollmentId" name="redirectEnrollmentId" type="hidden" value="${redirectEnrollmentId}"/>

    <div ui-view></div>
    <%--</div>--%>

    <script src="/hix/resources/js/d3.v2.min.js"></script>
    <script src="/hix/resources/js//d3.v4.min.js"></script>
    <script src="/hix/resources/js/angular-touch.js"></script>
    <script src="/hix/resources/js/angular-animate.js"></script>
    <script src="/hix/resources/js/angular-ui-router.min.js"></script>
    <script src="/hix/resources/js/moment.js"></script>
    <script src="/hix/resources/js/d3.tip.v0.6.3.js"></script>
    <script src="/hix/resources/js/d3-scale-chromatic.v0.3.min.js"></script>
    <script src="/hix/resources/js/ui-grid.js"></script>
    <script src="/hix/resources/js/spring-security-csrf-token-interceptor.js"></script>
    <script src="/hix/resources/js/amplify.min.js"></script>

    <script src="/hix/resources/js/enrollment/app.js"></script>
    <script src="/hix/resources/js/enrollment/components/dataStorage.js"></script>

    <script src="/hix/resources/js/enrollment/components/animateNumber.directive.js"></script>
    <script src="/hix/resources/js/enrollment/components/months.directive.js"></script>
    <script src="/hix/resources/js/enrollment/components/issuers.directive.js"></script>
    <script src="/hix/resources/js/enrollment/components/years.directive.js"></script>

    <script src="/hix/resources/js/enrollment/services/reconSearch.service.js"></script>
    <script src="/hix/resources/js/enrollment/services/searchNotifying.service.js"></script>
    <script src="/hix/resources/js/enrollment/services/discrepancyDetail.service.js"></script>
    <script src="/hix/resources/js/enrollment/services/issuerNotifying.service.js"></script>
    <script src="/hix/resources/js/enrollment/services/workbench.service.js"></script>
    <script src="/hix/resources/js/enrollment/services/issuerDetails.service.js"></script>
    <script src="/hix/resources/js/enrollment/services/notifying.service.js"></script>
    <script src="/hix/resources/js/enrollment/services/searchPagination.service.js"></script>
    <script src="/hix/resources/js/enrollment/services/clearSearchResults.service.js"></script>


    <script src="/hix/resources/js/enrollment/monthlyactivity/issuersActivity.controller.js"></script>
    <script src="/hix/resources/js/enrollment/monthlyactivity/workbench.controller.js"></script>
    <script src="/hix/resources/js/enrollment/monthlyactivity/issuerActivity.controller.js"></script>

    <script src="/hix/resources/js/enrollment/search/searchResults.controller.js"></script>
    <script src="/hix/resources/js/enrollment/search/reconSearch.controller.js"></script>

    <script src="/hix/resources/js/enrollment/activitydetail/discrepancyDetail.controller.js"></script>

    <script src="/hix/resources/js/enrollment/components/pieChart.directive.js"></script>
    <script src="/hix/resources/js/enrollment/components/trendChart.directive.js"></script>
    <script src="/hix/resources/js/enrollment/components/barCharHorizontal.directive.js"></script>
    <script src="/hix/resources/js/enrollment/components/stackedChart.directive.js"></script>
    <script src="/hix/resources/js/enrollment/components/barChart.directive.js"></script>




    <script>
    $(document).ready(function() {
    $('#myTab li:first').trigger( "click" );

    $(".nav-tabs > li").click(function () {
    $(".nav-tabs > li").removeClass("active");
    // $(".tab").addClass("active"); // instead of this do the below
    $(this).addClass("active");
    });
    });

    var csrfToken = {
    getParam: '${df:csrfTokenParameter()}=<df:csrfToken plainToken="true"/>',
    paramName: '${df:csrfTokenParameter()}',
    paramValue: '<df:csrfToken plainToken="true"/>'
    };
    </script>


    </body>
    </html>