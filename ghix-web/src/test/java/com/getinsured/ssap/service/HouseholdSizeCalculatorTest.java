package com.getinsured.ssap.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.ssap.util.FamilyBuilder;

/**
 * Unit tests for {@link HouseholdSizeCalculator}.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-18
 */
public class HouseholdSizeCalculatorTest {

  @Test
  public void calculateSize() {
    SingleStreamlinedApplication application = new SingleStreamlinedApplication();
    List<TaxHousehold> taxHouseholds = new ArrayList<>();
    TaxHousehold taxHousehold = new TaxHousehold();
    taxHousehold.setHouseholdMember(FamilyBuilder.buildTestCase_H_Household());

    List<BloodRelationship> bloodRelationshipList = new ArrayList<>();

    for(HouseholdMember m : taxHousehold.getHouseholdMember()) {
      bloodRelationshipList.addAll(m.getBloodRelationship());
    }
    taxHousehold.getHouseholdMember().get(0).setBloodRelationship(bloodRelationshipList);

    taxHouseholds.add(taxHousehold);
    application.setTaxHousehold(taxHouseholds);

    List<HouseholdMember> members = application.getTaxHousehold().get(0).getHouseholdMember();
    int primaryTaxFilerPersonId = application.getTaxHousehold().get(0).getPrimaryTaxFilerPersonId();
    int householdSize = HouseholdSizeCalculator.calculateSize(members, primaryTaxFilerPersonId);
    Assert.assertEquals("Household size should be 4", 4, householdSize);
    System.out.format("HH Size: %s %s household size is: %d %n", members.get(0).getName().getFirstName(), members.get(0).getName().getLastName(), householdSize);
  }

  @Test
  public void calculateMagiSize_testCase_E() {
    SingleStreamlinedApplication application = new SingleStreamlinedApplication();
    List<TaxHousehold> taxHouseholds = new ArrayList<>();
    TaxHousehold taxHousehold = new TaxHousehold();
    taxHousehold.setHouseholdMember(FamilyBuilder.buildTestCase_E_Household());

    taxHouseholds.add(taxHousehold);
    application.setTaxHousehold(taxHouseholds);
    application.setCoverageYear(2019L);

    List<HouseholdMember> members = application.getTaxHousehold().get(0).getHouseholdMember();

    int grandFatherHouseholdSize = HouseholdSizeCalculator.getMagiSize(members, members.get(0), Math.toIntExact(application.getCoverageYear()),1);
    Assert.assertEquals(members.get(0).getName().getFirstName() + " " +
        members.get(0).getName().getLastName() + " household size is incorrect", 3, grandFatherHouseholdSize);

    int fatherHouseholdSize = HouseholdSizeCalculator.getMagiSize(members, members.get(1), Math.toIntExact(application.getCoverageYear()),1);
    Assert.assertEquals(members.get(1).getName().getFirstName() + " " +
        members.get(1).getName().getLastName() +
        " household size is incorrect", 3, fatherHouseholdSize);

    int childHouseholdSize = HouseholdSizeCalculator.getMagiSize(members, members.get(2), Math.toIntExact(application.getCoverageYear()),1);
    Assert.assertEquals(members.get(2).getName().getFirstName() + " " +
        members.get(2).getName().getLastName() + " household size is incorrect", 2, childHouseholdSize);

    System.out.format("Test Case E: %s %s household size is: %d %n", members.get(0).getName().getFirstName(), members.get(0).getName().getLastName(), grandFatherHouseholdSize);
    System.out.format("Test Case E: %s %s household size is: %d %n", members.get(1).getName().getFirstName(), members.get(1).getName().getLastName(), fatherHouseholdSize);
    System.out.format("Test Case E: %s %s household size is: %d %n", members.get(2).getName().getFirstName(), members.get(2).getName().getLastName(), childHouseholdSize);

    Assert.assertEquals("Test Case E Grandfathers household size should be 3", 3, grandFatherHouseholdSize);
    Assert.assertEquals("Test Case E Fathers household size should be 3", 3, fatherHouseholdSize);
    Assert.assertEquals("Test Case E Grandchild household size should be 3", 2, childHouseholdSize);

    List<HouseholdMember> grandFatherHouseholdMembers = HouseholdSizeCalculator.getMagiHousehold(members, members.get(0), Math.toIntExact(application.getCoverageYear()), 1);
    List<HouseholdMember> fatherHouseholdMembers = HouseholdSizeCalculator.getMagiHousehold(members, members.get(1), Math.toIntExact(application.getCoverageYear()), 1);
    List<HouseholdMember> childHouseholdMembers = HouseholdSizeCalculator.getMagiHousehold(members, members.get(2), Math.toIntExact(application.getCoverageYear()), 1);

    System.out.format("Test Case E: %s %s household size is: %d %n", members.get(0).getName().getFirstName(), members.get(0).getName().getLastName(), grandFatherHouseholdMembers.size());
    System.out.format("Test Case E: %s %s household size is: %d %n", members.get(1).getName().getFirstName(), members.get(1).getName().getLastName(), fatherHouseholdMembers.size());
    System.out.format("Test Case E: %s %s household size is: %d %n", members.get(2).getName().getFirstName(), members.get(2).getName().getLastName(), childHouseholdMembers.size());

    Assert.assertEquals("Test Case E Grandfathers household size should be 3", 3, grandFatherHouseholdMembers.size());
    Assert.assertEquals("Test Case E Fathers household size should be 3", 3, fatherHouseholdMembers.size());
    Assert.assertEquals("Test Case E Grandchild household size should be 2", 2, childHouseholdMembers.size());
  }

  @Test
  public void calculateMagiSize_testCase_H() {
    SingleStreamlinedApplication application = new SingleStreamlinedApplication();
    List<TaxHousehold> taxHouseholds = new ArrayList<>();
    TaxHousehold taxHousehold = new TaxHousehold();
    taxHousehold.setHouseholdMember(FamilyBuilder.buildTestCase_H_Household());

    taxHouseholds.add(taxHousehold);
    application.setTaxHousehold(taxHouseholds);
    application.setCoverageYear(2019L);

    List<HouseholdMember> members = application.getTaxHousehold().get(0).getHouseholdMember();

    int primaryTaxHouseholdSize = HouseholdSizeCalculator.getMagiSize(members, members.get(0), Math.toIntExact(application.getCoverageYear()), 1);
    Assert.assertEquals("Primary tax household size incorrect", 4, primaryTaxHouseholdSize);

    int spouseTaxHouseholdSize = HouseholdSizeCalculator.getMagiSize(members, members.get(1), Math.toIntExact(application.getCoverageYear()), 1);
    Assert.assertEquals("Spouse tax household size incorrect", 4, spouseTaxHouseholdSize);

    int dependentATaxHouseholdSize = HouseholdSizeCalculator.getMagiSize(members, members.get(2), Math.toIntExact(application.getCoverageYear()),1);
    Assert.assertEquals("Dependent A tax household size incorrect", 4, dependentATaxHouseholdSize);

    int dependentBTaxHouseholdSize = HouseholdSizeCalculator.getMagiSize(members, members.get(3), Math.toIntExact(application.getCoverageYear()), 1);
    Assert.assertEquals("Dependent B tax household size incorrect", 4, dependentBTaxHouseholdSize);

    System.out.format("Test Case H: %s %s household size is: %d %n", members.get(0).getName().getFirstName(), members.get(0).getName().getLastName(), primaryTaxHouseholdSize);
    System.out.format("Test Case H: %s %s household size is: %d %n", members.get(1).getName().getFirstName(), members.get(1).getName().getLastName(), spouseTaxHouseholdSize);
    System.out.format("Test Case H: %s %s household size is: %d %n", members.get(2).getName().getFirstName(), members.get(2).getName().getLastName(), dependentATaxHouseholdSize);
    System.out.format("Test Case H: %s %s household size is: %d %n", members.get(2).getName().getFirstName(), members.get(3).getName().getLastName(), dependentBTaxHouseholdSize);

    List<HouseholdMember> primaryFatherHouseholdMembers = HouseholdSizeCalculator.getMagiHousehold(members, members.get(0), Math.toIntExact(application.getCoverageYear()), 1);
    List<HouseholdMember> spouseHouseholdMembers = HouseholdSizeCalculator.getMagiHousehold(members, members.get(1), Math.toIntExact(application.getCoverageYear()), 1);
    List<HouseholdMember> dependentAHouseholdMembers = HouseholdSizeCalculator.getMagiHousehold(members, members.get(2), Math.toIntExact(application.getCoverageYear()), 1);
    List<HouseholdMember> dependentBHouseholdMembers = HouseholdSizeCalculator.getMagiHousehold(members, members.get(3), Math.toIntExact(application.getCoverageYear()), 1);

    System.out.format("Test Case H: %s %s household size is: %d %n", members.get(0).getName().getFirstName(), members.get(0).getName().getLastName(), primaryFatherHouseholdMembers.size());
    System.out.format("Test Case H: %s %s household size is: %d %n", members.get(1).getName().getFirstName(), members.get(1).getName().getLastName(), spouseHouseholdMembers.size());
    System.out.format("Test Case H: %s %s household size is: %d %n", members.get(2).getName().getFirstName(), members.get(2).getName().getLastName(), dependentAHouseholdMembers.size());
    System.out.format("Test Case H: %s %s household size is: %d %n", members.get(3).getName().getFirstName(), members.get(3).getName().getLastName(), dependentBHouseholdMembers.size());

    Assert.assertEquals("Test Case H Primary household size should be 4", 4, primaryFatherHouseholdMembers.size());
    Assert.assertEquals("Test Case H Spouse household size should be 4", 4, spouseHouseholdMembers.size());
    Assert.assertEquals("Test Case H Dependent A household size should be 4", 4, dependentAHouseholdMembers.size());
    Assert.assertEquals("Test Case H Dependent B household size should be 4", 4, dependentBHouseholdMembers.size());
  }

  @Test
  public void householdSizeFromSampleJson_1() {
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson(1);
    Assert.assertNotNull("Could not get SingleStreamlinedApplication from JSON id 1", application);
    Assert.assertFalse("Tax household is empty", application.getTaxHousehold().isEmpty());
    Assert.assertFalse("Tax household -> household members is empty", application.getTaxHousehold().get(0).getHouseholdMember().isEmpty());
    List<HouseholdMember> householdMembers = application.getTaxHousehold().get(0).getHouseholdMember();
    int primaryTaxFilerPersonId = application.getTaxHousehold().get(0).getPrimaryTaxFilerPersonId();
    int householdSize = HouseholdSizeCalculator.calculateSize(householdMembers, primaryTaxFilerPersonId);
    long householdIncome = HouseholdIncomeCalculator.calculateTotalHouseholdIncome(householdMembers);

    System.out.format("Total Household Size: %d, Income: %d%n", householdSize, householdIncome);
    householdMembers.forEach(hhm -> {
      int magiHhSize = HouseholdSizeCalculator.getMagiSize(householdMembers, hhm, Math.toIntExact(application.getCoverageYear()), 1);
      long magiIncome = HouseholdIncomeCalculator.calculateMagi(householdMembers, hhm, Math.toIntExact(application.getCoverageYear()), 1);
      System.out.format("%s %s => MAGI Household Size: %d, Income: %d%n", hhm.getName().getFirstName(), hhm.getName().getLastName(), magiHhSize, magiIncome);
    });
  }
}
