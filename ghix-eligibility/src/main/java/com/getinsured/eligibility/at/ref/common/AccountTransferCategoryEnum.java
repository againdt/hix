package com.getinsured.eligibility.at.ref.common;

/**
 * @author chopra_s
 * 
 */
public enum AccountTransferCategoryEnum {
	OE, LCE, QE, RENEWAL;
	
	public String value() {
		return name();
	}

	public static AccountTransferCategoryEnum fromValue(String v) {
		AccountTransferCategoryEnum data = null;
		for (AccountTransferCategoryEnum c : AccountTransferCategoryEnum.class.getEnumConstants()) {
			if (c.value().equals(v)) {
				data = c;
				break;
			}
		}
		return data;
	}

	@Override
	public String toString() {
		return value();
	}

}
