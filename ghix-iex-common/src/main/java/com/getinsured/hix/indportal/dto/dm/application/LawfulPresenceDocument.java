package com.getinsured.hix.indportal.dto.dm.application;

public class LawfulPresenceDocument {
	
	 private boolean noIdentifiersProvidedIndicator;
	 private String documentExpirationDate;
	 private String alienNumber;
	 private String employmentAuthorizationCategoryIdentifier;
	 private String sevisId;
	 private String i94Number;
	 private String passportNumber;
	 private String issuingCountry;
	 private String cardNumber;
	 private String naturalizationCertificateNumber;
	 private String citizenShipNumber;
	 private DocumentAlternateName documentAlternateName;
	 private String otherDocumentTypeText;


	 // Getter Methods 

	 public boolean getNoIdentifiersProvidedIndicator() {
	  return noIdentifiersProvidedIndicator;
	 }

	 // Setter Methods 

	 public void setNoIdentifiersProvidedIndicator(boolean noIdentifiersProvidedIndicator) {
	  this.noIdentifiersProvidedIndicator = noIdentifiersProvidedIndicator;
	 }

	public String getDocumentExpirationDate() {
		return documentExpirationDate;
	}

	public void setDocumentExpirationDate(String documentExpirationDate) {
		this.documentExpirationDate = documentExpirationDate;
	}

	public String getAlienNumber() {
		return alienNumber;
	}

	public void setAlienNumber(String alienNumber) {
		this.alienNumber = alienNumber;
	}

	public String getEmploymentAuthorizationCategoryIdentifier() {
		return employmentAuthorizationCategoryIdentifier;
	}

	public void setEmploymentAuthorizationCategoryIdentifier(String employmentAuthorizationCategoryIdentifier) {
		this.employmentAuthorizationCategoryIdentifier = employmentAuthorizationCategoryIdentifier;
	}

	public String getSevisId() {
		return sevisId;
	}

	public void setSevisId(String sevisId) {
		this.sevisId = sevisId;
	}

	public String getI94Number() {
		return i94Number;
	}

	public void setI94Number(String i94Number) {
		this.i94Number = i94Number;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getIssuingCountry() {
		return issuingCountry;
	}

	public void setIssuingCountry(String issuingCountry) {
		this.issuingCountry = issuingCountry;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getNaturalizationCertificateNumber() {
		return naturalizationCertificateNumber;
	}

	public void setNaturalizationCertificateNumber(String naturalizationCertificateNumber) {
		this.naturalizationCertificateNumber = naturalizationCertificateNumber;
	}

	public String getCitizenShipNumber() {
		return citizenShipNumber;
	}

	public void setCitizenShipNumber(String citizenShipNumber) {
		this.citizenShipNumber = citizenShipNumber;
	}

	public DocumentAlternateName getDocumentAlternateName() {
		return documentAlternateName;
	}

	public void setDocumentAlternateName(DocumentAlternateName documentAlternateName) {
		this.documentAlternateName = documentAlternateName;
	}

	public String getOtherDocumentTypeText() {
		return otherDocumentTypeText;
	}

	public void setOtherDocumentTypeText(String otherDocumentTypeText) {
		this.otherDocumentTypeText = otherDocumentTypeText;
	}
	}
