package com.getinsured.ssap.model.hub.vlp.documents;

import java.util.Date;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * I327 Document.
 *
 * Always call {@link #isValid()} to verify that all required fields are present.
 *
 * @author Yevgen Golubenko
 * @since 2/14/19
 */
public class I327Document extends DhsDocument
{
  public I327Document() {
    super.setDocumentType(DocumentType.I327);
  }

  @JsonProperty("AlienNumber")
  private String alienNumber;

  @JsonProperty("SEVISID")
  private String sevisId;

  @JsonFormat(pattern = DATE_FORMAT)
  @JsonProperty("DocExpirationDate")
  private Date docExpirationDate;

  /**
   * {@inheritDoc}
   */
  public boolean isValid() {
    return notBlank(this.alienNumber);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public DhsDocument from(final CitizenshipDocument citizenshipDocument)
  {
    final I327Document document = new I327Document();

    document.setAlienNumber(validValue(citizenshipDocument.getAlienNumber()));
    document.setDocExpirationDate(citizenshipDocument.getDocumentExpirationDate());
    document.setSevisId(validValue(citizenshipDocument.getSevisId()));

    return document;
  }

  public String getAlienNumber()
  {
    return alienNumber;
  }

  public void setAlienNumber(final String alienNumber)
  {
    this.alienNumber = alienNumber;
  }

  public Date getDocExpirationDate()
  {
    return docExpirationDate;
  }

  public void setDocExpirationDate(final Date docExpirationDate)
  {
    this.docExpirationDate = docExpirationDate;
  }

  public String getSevisId() {
    return sevisId;
  }

  public void setSevisId(String sevisId) {
    this.sevisId = sevisId;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", I327Document.class.getSimpleName() + "[", "]")
      .add("alienNumber='" + alienNumber + "'")
      .add("sevisId='" + sevisId + "'")
      .add("docExpirationDate=" + docExpirationDate)
      .toString();
  }
}
