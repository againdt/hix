package com.getinsured.eligibility.at.ref.common;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.getinsured.eligibility.at.ref.dto.EnrollmentAttributesDTO;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventServiceImpl;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.plan.service.ApplicantPlanDto;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterResponse;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExtendedApplicantEventCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExtendedApplicantNonQHPCodeSimpleType;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
public final class LceAllChangesHandlerDetermination {

	private static final Logger LOGGER = Logger.getLogger(LceAllChangesHandlerDetermination.class);

	private LceAllChangesHandlerDetermination() {
	}

	private final static Set<ApplicantStatusEnum> EVENT_CHECK_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE, ApplicantStatusEnum.ADD_NEW_ELIGIBILE, ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE,
	        ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE, ApplicantStatusEnum.UPDATED_ZIP_COUNTY, ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS);
	
	private static final int CHANGE_IN_CSR_LEVEL_INDEX = 0;
	private static final int ADD_INDEX = 1;
	private static final int REMOVE_INDEX = 2;
	private static final int UPDATED_ZIP_COUNTY_INDEX = 3;
	private static final int CHANGE_IN_CITIZENSHIP_STATUS_INDEX = 4;
	private static final int CHANGE_IN_BLOOD_RELATION_INDEX = 5;
	private static final int UPDATED_DOB_INDEX = 6;
	
	private static final int CHANGE_SIZE = 7;

	public enum AllChangesHandlerType {
		SINGLE, MIXED, MANUAL, DENIAL, PURE_REMOVE, CS_CHANGE, PURE_CS_CHANGE;
	}

	public static AllChangesHandlerType handlerToInvoke(SsapApplication currentApplication, Map<String, List<ApplicantEvent>> applicantExtensionEvents, EnrollmentAttributesDTO enrolledApplicationAttributes, Map<String, Boolean> applicantWithInvalidExtensionEvents) {
		String automateFlow = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CHANGE_TO_AUTOMATE);
		final List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();

		int[] changeCountDecisionTable = populateChangeCountDecisionTable(ssapApplicants);
		
		if (LOGGER.isInfoEnabled()){
			LOGGER.info("CHANGE COUNT DECISION TABLE - " +  Arrays.toString(changeCountDecisionTable));
		}
		
		if (runCsChangeRulesAndDetermineAutomation(changeCountDecisionTable, enrolledApplicationAttributes, currentApplication, applicantExtensionEvents, applicantWithInvalidExtensionEvents)){
			LOGGER.info("CSR Automation detected, preparing for IND71 call.");
			if (isPureCsrChange(changeCountDecisionTable)){
				return AllChangesHandlerType.PURE_CS_CHANGE;
			}
			return AllChangesHandlerType.CS_CHANGE;
		}
		
		if (checkPureRemove(changeCountDecisionTable, ssapApplicants, applicantExtensionEvents, applicantWithInvalidExtensionEvents)){
			// Is enrolled for health check is removed to allow dis-enrolling not eligible members from ONLY DENTAL enrollments as well.
			//if (!checkIfCatastrophicPlan(enrolledApplicationAttributes) && SsapEnrolleeHandler.isEnrolledForHealth(enrolledApplicationAttributes)){
			LOGGER.info("Multiple remove applicants determined and proceed with automation.");
			if(StringUtils.isNotBlank(automateFlow) && automateFlow.contains("MemberRemoval")){
				return AllChangesHandlerType.PURE_REMOVE;
			}else {
				return AllChangesHandlerType.MANUAL;
			}
		}
		
		if (checkMultipleAddRemove(ssapApplicants)) {
			LOGGER.info("Multiple add/remove applicants determined.");
			return AllChangesHandlerType.MANUAL;
		} else {
			LOGGER.info("Compute determination handler.");
			AllChangesHandlerType allChangesHandlerType = determineHandler(ssapApplicants, applicantExtensionEvents);
			if (AllChangesHandlerType.SINGLE.equals(allChangesHandlerType) && !SsapEnrolleeHandler.isEnrolledForHealth(enrolledApplicationAttributes)) {
				LOGGER.info("Change Handler to " + AllChangesHandlerType.MIXED + " as Only Dental found");
				allChangesHandlerType = AllChangesHandlerType.MIXED;
			}

			if (AllChangesHandlerType.SINGLE.equals(allChangesHandlerType) && checkIfCatastrophicPlan(enrolledApplicationAttributes)) {
				LOGGER.info("Change Handler to " + AllChangesHandlerType.MIXED + " as Catastrophic Plan found");
				allChangesHandlerType = AllChangesHandlerType.MIXED;
			}

			return allChangesHandlerType;
		}
	}
	
	private static boolean runCsChangeRulesAndDetermineAutomation(int[] changeCountDecisionTable, EnrollmentAttributesDTO enrolledApplicationAttributes, SsapApplication currentApplication, Map<String, List<ApplicantEvent>> applicantExtensionEvents, Map<String, Boolean> applicantWithInvalidExtensionEvents){
		
		boolean decision = false;
		//StringUtils.equalsIgnoreCase(ReferralConstants.CS1, enrolledApplicationAttributes.getCostSharing()) ||
		//Removed this OR condition as part of HIX-106303
		if (isCsrChangeDetected(changeCountDecisionTable)){
			if ( (enrolledApplicationAttributes.getHealthPlanId() == 0 &&  enrolledApplicationAttributes.getDentalEnrollmentId() == 0)
					||  checkIfCatastrophicPlan(enrolledApplicationAttributes)
					|| isNonAutomationChange(changeCountDecisionTable) || isAddRemoveDetected(changeCountDecisionTable)
					|| isAddAddressDetected(changeCountDecisionTable) || isRemoveAddressDetected(changeCountDecisionTable)){
				decision = false;
			} else if (isSingleAddDetected(changeCountDecisionTable)) {
				decision = checkIfAddEventExists(currentApplication.getSsapApplicants(), applicantExtensionEvents);
			} else if (isRemovesDetected(changeCountDecisionTable)){
				decision = checkIfRemoveEventExists(currentApplication.getSsapApplicants(), applicantExtensionEvents, applicantWithInvalidExtensionEvents);
			} else if (isAddressChangeDetected(changeCountDecisionTable)){
				decision = checkIfAddressChangeEventExists(currentApplication.getSsapApplicants(), applicantExtensionEvents);
			} else if (isPureCsrChange(changeCountDecisionTable)) {
				decision = true;
			}
		}
		
		return decision;
		
	}
	
	private static boolean isNonAutomationChange(int[] changeCountDecisionTable) {
		return changeCountDecisionTable[CHANGE_IN_CITIZENSHIP_STATUS_INDEX] != 0
				|| changeCountDecisionTable[CHANGE_IN_BLOOD_RELATION_INDEX] != 0 || changeCountDecisionTable[UPDATED_DOB_INDEX] != 0;
	}
	
	private static boolean isSingleAddDetected(int[] changeCountDecisionTable) {
		return !isNonAutomationChange(changeCountDecisionTable) && changeCountDecisionTable[ADD_INDEX] == 1 && changeCountDecisionTable[REMOVE_INDEX] == 0
				&& changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] == 0;
	}
	
	private static boolean isRemovesDetected(int[] changeCountDecisionTable) {
		return !isNonAutomationChange(changeCountDecisionTable) && changeCountDecisionTable[ADD_INDEX] == 0 && changeCountDecisionTable[REMOVE_INDEX] != 0
				&& changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] == 0;
	}
	
	private static boolean isAddressChangeDetected(int[] changeCountDecisionTable) {
		return !isNonAutomationChange(changeCountDecisionTable) && changeCountDecisionTable[ADD_INDEX] == 0 && changeCountDecisionTable[REMOVE_INDEX] == 0
				&& changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] != 0;
	}
	
	private static boolean isAddRemoveDetected(int[] changeCountDecisionTable) {
		return !isNonAutomationChange(changeCountDecisionTable) && changeCountDecisionTable[ADD_INDEX] != 0 && changeCountDecisionTable[REMOVE_INDEX] != 0
				&& changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] == 0;
	}
	
	private static boolean isAddAddressDetected(int[] changeCountDecisionTable) {
		return !isNonAutomationChange(changeCountDecisionTable) && changeCountDecisionTable[ADD_INDEX] != 0 && changeCountDecisionTable[REMOVE_INDEX] == 0
				&& changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] != 0;
	}
	
	private static boolean isRemoveAddressDetected(int[] changeCountDecisionTable) {
		return !isNonAutomationChange(changeCountDecisionTable) && changeCountDecisionTable[ADD_INDEX] == 0 && changeCountDecisionTable[REMOVE_INDEX] != 0
				&& changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] != 0;
	}
	
	private static boolean isPureCsrChange(int[] changeCountDecisionTable) {
		return !isNonAutomationChange(changeCountDecisionTable) 
				&& changeCountDecisionTable[ADD_INDEX] == 0  && changeCountDecisionTable[REMOVE_INDEX] == 0 
				&& changeCountDecisionTable[CHANGE_IN_CSR_LEVEL_INDEX] != 0
				&& changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] == 0;
	}

	private static boolean isCsrChangeDetected(int[] changeCountDecisionTable) {
		return changeCountDecisionTable[CHANGE_IN_CSR_LEVEL_INDEX] != 0;
	}

	private static int[] populateChangeCountDecisionTable(final List<SsapApplicant> ssapApplicants) {
		int[] changeCountDecisionTable = new int[CHANGE_SIZE];
		ApplicantStatusEnum temp;
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			temp = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
			if (ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL == temp){
				changeCountDecisionTable[CHANGE_IN_CSR_LEVEL_INDEX] = ++changeCountDecisionTable[CHANGE_IN_CSR_LEVEL_INDEX];
			} else if (ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE == temp || ApplicantStatusEnum.ADD_NEW_ELIGIBILE == temp ) {
				changeCountDecisionTable[ADD_INDEX] = ++changeCountDecisionTable[ADD_INDEX];
			} else if (ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE == temp || ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE == temp ) {
				changeCountDecisionTable[REMOVE_INDEX] = ++changeCountDecisionTable[REMOVE_INDEX];
			} else if (ApplicantStatusEnum.UPDATED_ZIP_COUNTY == temp){
				changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] = ++changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX];
			} else if (ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS == temp){
				changeCountDecisionTable[CHANGE_IN_CITIZENSHIP_STATUS_INDEX] = ++changeCountDecisionTable[CHANGE_IN_CITIZENSHIP_STATUS_INDEX];
			} else if (ApplicantStatusEnum.CHANGE_IN_BLOOD_RELATION == temp) {
				changeCountDecisionTable[CHANGE_IN_BLOOD_RELATION_INDEX] = ++changeCountDecisionTable[CHANGE_IN_BLOOD_RELATION_INDEX];
			} else if (ApplicantStatusEnum.UPDATED_DOB == temp) {
				changeCountDecisionTable[UPDATED_DOB_INDEX] = ++changeCountDecisionTable[UPDATED_DOB_INDEX];
			}
		}
		
		return changeCountDecisionTable;
	}

	private static boolean checkIfCatastrophicPlan(EnrollmentAttributesDTO enrolledApplicationAttributes) {
		return SsapEnrolleeHandler.isEnrolledForHealth(enrolledApplicationAttributes) && Plan.PlanLevel.CATASTROPHIC.toString().equals(enrolledApplicationAttributes.getHealthPlanLevel());
	}

	// Renamed to checkMultipleAddRemove to checkMultipleAdd
	private static boolean checkPureRemove(int[] changeCountDecisionTable, List<SsapApplicant> ssapApplicants, Map<String, List<ApplicantEvent>> applicantExtensionEvents, Map<String, Boolean> applicantWithInvalidExtensionEvents) {
		boolean pureRemove = isRemovesDetected(changeCountDecisionTable) && !isCsrChangeDetected(changeCountDecisionTable);
		
		if (pureRemove){
			pureRemove = checkIfRemoveEventExists(ssapApplicants, applicantExtensionEvents, applicantWithInvalidExtensionEvents);
		}
		
		return pureRemove;
	}
	
	/**
	 * This is invoked for Single Add case only
	 */
	private static boolean checkIfAddEventExists(List<SsapApplicant> ssapApplicants, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		int noOfAdds = 0;
		boolean automate = false;
		for (SsapApplicant applicant : ssapApplicants) {
			ApplicantStatusEnum statusEnum = ApplicantStatusEnum.fromValue(applicant.getStatus());
			List<ApplicantEvent> payloadEvent = applicantExtensionEvents.get(applicant.getApplicantGuid());
			
			if (ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(statusEnum)){
				noOfAdds++;
				if (ReferralUtil.listSize(payloadEvent) == 1) {
					boolean isValid = true;
					for (ApplicantEvent applicantEvent : payloadEvent) {
						String eventCode = null;
						ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromStringValue(applicantEvent.getCode());
						if (eventName != null) {
							eventCode = eventName.name();
						}
						
						if (eventCode != null){	/* If remove event found then add to list */
							Map<String, SepEvents> sepAddEvents = SsapApplicationEventServiceImpl.getSepAddEvents();
							if (ReferralUtil.mapSize(sepAddEvents) > 0){
								SepEvents sepEvent = sepAddEvents.get(eventCode);
								if (sepEvent != null){
									automate = true;
								}
							} else {
								LOGGER.error("No sepAddEvents found from cache. Dont proceed with automation.");
								isValid = false; 
								automate = false;
								break;
							}
						}
					}
					
					if (!isValid){
						break;
					}
				}
			}
			
		}
		if (noOfAdds > 1){ /* safety check for single add */
			automate = false;
		}
		
		return automate;
	}
	
	/**
	 * This is invoked for Address change case only
	 */
	private static boolean checkIfAddressChangeEventExists(List<SsapApplicant> ssapApplicants, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		
		boolean automate = false;
		for (SsapApplicant applicant : ssapApplicants) {
			ApplicantStatusEnum statusEnum = ApplicantStatusEnum.fromValue(applicant.getStatus());
			List<ApplicantEvent> payloadEvent = applicantExtensionEvents.get(applicant.getApplicantGuid());
			
			if (ReferralConstants.ADDRESS_CHANGE.contains(statusEnum)){
				
				if (ReferralUtil.listSize(payloadEvent) == 1) {
					boolean isValid = true;
					for (ApplicantEvent applicantEvent : payloadEvent) {
						String eventCode = null;
						ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromStringValue(applicantEvent.getCode());
						if (eventName != null) {
							eventCode = eventName.name();
						}
						
						if (eventCode != null){	/* If address change event found then add to list */
							Map<String, SepEvents> sepOtherEvents = SsapApplicationEventServiceImpl.getSepOtherEvents();
							if (ReferralUtil.mapSize(sepOtherEvents) > 0){
								SepEvents sepEvent = sepOtherEvents.get(eventCode);
								if (sepEvent != null){
									automate = true;
								}
							} else {
								LOGGER.error("No sepOtherEvents found from cache. Dont proceed with automation.");
								isValid = false; 
								automate = false;
								break;
							}
						}
					}
					
					if (!isValid){
						break;
					}
				}
			}
			
		}
				
		return automate;
    }
	
	private static boolean checkIfRemoveEventExists(List<SsapApplicant> ssapApplicants, Map<String, List<ApplicantEvent>> applicantExtensionEvents, Map<String, Boolean> applicantWithInvalidExtensionEvents) {
		
		Map<String, List<ApplicantEvent>> removedMemberEventsMap = new HashMap<>();
		Set<Integer> typesOfEvents  = new HashSet<>();
		
		boolean proceed = collectRemovingMemberAndEvents(ssapApplicants, applicantExtensionEvents, removedMemberEventsMap, typesOfEvents, applicantWithInvalidExtensionEvents);
		
		if (!proceed){
			return false;
		}
		
		boolean automate = false;
		if (ReferralUtil.mapSize(removedMemberEventsMap) > 0){
			int zeroEvent = 0;
			int oneEvent = 0;

			boolean noMultipleEventsFoundPerApplicant = true;
			
			for (String applicantGuid : removedMemberEventsMap.keySet()) {
				List<ApplicantEvent> events = removedMemberEventsMap.get(applicantGuid);
				
				if (ReferralUtil.listSize(events) > 1){
					noMultipleEventsFoundPerApplicant = false;
					break;
				} else if (ReferralUtil.listSize(events) == 0){
					zeroEvent++;
				} else if (ReferralUtil.listSize(events) == 1) {
					oneEvent++;
				}
			}
			
			if (noMultipleEventsFoundPerApplicant){
				if (zeroEvent == removedMemberEventsMap.size()){
					automate = true;
				} else if (oneEvent == removedMemberEventsMap.size()){
					automate = true;
				}
				else{
					automate = true;
					applicantExtensionEvents =  new HashMap<String, List<ApplicantEvent>>();
				}
				if (automate){
					if (typesOfEvents.size() > 1){ /* Automate if the indicators/events evaluate to the same Type */
						automate = false;
					}
				}
			}
			
		}
			
		return automate;
	}

	private static boolean collectRemovingMemberAndEvents(List<SsapApplicant> ssapApplicants,
			Map<String, List<ApplicantEvent>> applicantExtensionEvents,
			Map<String, List<ApplicantEvent>> removedMemberEventsMap, Set<Integer> typesOfEvents, 
			Map<String, Boolean> applicantWithInvalidExtensionEvents) {
		for (SsapApplicant applicant : ssapApplicants) {
			ApplicantStatusEnum statusEnum = ApplicantStatusEnum.fromValue(applicant.getStatus());
			List<ApplicantEvent> payloadEvent = applicantExtensionEvents.get(applicant.getApplicantGuid());
			boolean invalidEventFound = false;
			if (applicantWithInvalidExtensionEvents.containsKey(applicant.getApplicantGuid())){
				invalidEventFound = applicantWithInvalidExtensionEvents.get(applicant.getApplicantGuid());
			}
			if (ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(statusEnum)){
				//as part of HIX-112522 we need to do auto remove even we get invalid event
				//if (invalidEventFound){
					//return false; /* Invalid events found, go back to normal processing */
				//}
				
				List<ApplicantEvent> memberEvents = new ArrayList<>();
				if (ReferralUtil.listSize(payloadEvent) != 0) {
					for (ApplicantEvent applicantEvent : payloadEvent) {
						String eventCode = null;
						ExtendedApplicantNonQHPCodeSimpleType nonQHPEventName = ExtendedApplicantNonQHPCodeSimpleType.fromStringValue(applicantEvent.getCode());
						if (nonQHPEventName != null) {
							eventCode = nonQHPEventName.name();
						}
						
						ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromStringValue(applicantEvent.getCode());
						if (eventName != null) {
							eventCode = eventName.name();
						}
						
						if (eventCode != null){	/* If remove event found then add to list */
							Map<String, SepEvents> sepRemoveEvents = SsapApplicationEventServiceImpl.getSepRemoveEvents();
							if (ReferralUtil.mapSize(sepRemoveEvents) > 0){
								SepEvents sepEvent = sepRemoveEvents.get(eventCode);
								if (sepEvent != null){
									typesOfEvents.add(sepEvent.getType());
									memberEvents.add(applicantEvent);
								} else {
									//LOGGER.error("Invalid remove event found. Dont proceed with automation.");
									//as part of HIX-112522 we need to do auto remove even we get invalid event
									return true;
								}
							} else {
								LOGGER.error("No sepRemoveEvents found from cache. Dont proceed with automation.");
								return false;
							}
							
						}
					}
				}
				removedMemberEventsMap.put(applicant.getApplicantGuid(), memberEvents);
			}
			
		}
		return true;
	}
	
	private static boolean checkMultipleAddRemove(List<SsapApplicant> ssapApplicants) {
		boolean blnManual = false;
		boolean blnFound = false;
		ApplicantStatusEnum temp;
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			temp = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
			if (ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(temp) || ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(temp)) {
				if (blnFound) {
					blnManual = true;
					break;
				}
				blnFound = true;
			}
		}
		return blnManual;
	}

	private static AllChangesHandlerType determineHandler(List<SsapApplicant> ssapApplicants, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		boolean blnSingleCheckFlag = false;
		boolean blnManual = false;
		boolean blnDenial = false;
		final String askQleOEPFlag =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ASK_QLE_IN_OEP);
		
		AllChangesHandlerType returnHandler = AllChangesHandlerType.MANUAL;
		ApplicantStatusEnum temp;
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			temp = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
			if (null == temp || ReferralConstants.NO_EFFECT.contains(temp)) {
				continue;
			}
			if (!blnSingleCheckFlag && (ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(temp) || 
					ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(temp)  ||  ReferralConstants.ADDRESS_CHANGE.contains(temp))){
					
				boolean blnCheck = checkIfEventExists(ssapApplicant, applicantExtensionEvents);
				
				if (!blnManual && blnCheck) {
					blnSingleCheckFlag = true;
					if (!blnDenial) {
						if (ApplicationExtensionEventUtil.checkDenial(ssapApplicant, applicantExtensionEvents)) {
							returnHandler = AllChangesHandlerType.DENIAL;
							blnDenial = true;
						} else {
							returnHandler = AllChangesHandlerType.SINGLE;
						}
					}
				} else if(askQleOEPFlag.equalsIgnoreCase(Boolean.FALSE.toString()) && isOpenEnrollment(ssapApplicant.getSsapApplication().getCoverageYear()) && isOepEsd(ssapApplicant.getSsapApplication().getStartDate())){
					if (!blnManual) {
						blnSingleCheckFlag = true;
						returnHandler = AllChangesHandlerType.SINGLE;
					}
				} else {
					returnHandler = AllChangesHandlerType.MANUAL;
					blnManual = true;
				}
			} else if (ReferralConstants.ALL_CHANGES_APPLICANT_STATUS.contains(temp)) {
				if (!ReferralConstants.APTC_CHANGED.contains(temp)) {
					blnSingleCheckFlag = true;
				}

				if (EVENT_CHECK_APPLICANT_STATUS.contains(temp)) {
					boolean blnCheck = checkIfEventExists(ssapApplicant, applicantExtensionEvents);
					if (!blnManual && blnCheck) {
						if (!blnDenial) {
							if (ApplicationExtensionEventUtil.checkDenial(ssapApplicant, applicantExtensionEvents)) {
								returnHandler = AllChangesHandlerType.DENIAL;
								blnDenial = true;
							} else {
								returnHandler = AllChangesHandlerType.MIXED;
							}
						}
					} else if(askQleOEPFlag.equalsIgnoreCase(Boolean.FALSE.toString()) && isOpenEnrollment(ssapApplicant.getSsapApplication().getCoverageYear()) && isOepEsd(ssapApplicant.getSsapApplication().getStartDate())){
						if (!blnManual) {
							returnHandler = AllChangesHandlerType.MIXED;
						}
					} else {
						returnHandler = AllChangesHandlerType.MANUAL;
						blnManual = true;
					}
				} else if (!blnManual && !blnDenial) {
					if (!ReferralConstants.APTC_CHANGED.contains(temp) || !blnSingleCheckFlag) {
						returnHandler = AllChangesHandlerType.MIXED;
					}
				}
			}
		}
		return returnHandler;
	}

	private static boolean checkIfEventExists(SsapApplicant ssapApplicant, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		List<ApplicantEvent> events = applicantExtensionEvents.get(ssapApplicant.getApplicantGuid());
		if (ReferralUtil.listSize(events) != ReferralConstants.NONE) {
			if (checkIfAddEventAndTaxDependent(ssapApplicant, events)) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	private static boolean checkIfAddEventAndTaxDependent(SsapApplicant ssapApplicant, List<ApplicantEvent> events) {
		if (ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(ssapApplicant.getStatus()))) {
			if (taxDependentEvent(events)) {
				return true;
			}
		}
		return false;
	}

	private static boolean taxDependentEvent(List<ApplicantEvent> events) {
		ApplicantEvent applicantEvent;
		for (Iterator<ApplicantEvent> iterator = events.iterator(); iterator.hasNext();) {
			applicantEvent = iterator.next();
			if (ExtendedApplicantEventCodeSimpleType.ADD_TAX_DEPENDENT.value().equals(applicantEvent.getCode())) {
				return true;
			}
		}
		return false;
	}

	public static boolean proceedSingleProcess(List<SsapApplicant> ssapApplicants, LCEProcessRequestDTO lceProcessRequestDTO, PlanAvailabilityAdapterResponse planResponse) {
		boolean blnCheck = checkSubscriberChanged(lceProcessRequestDTO, planResponse);
		if (!blnCheck) {
			return false;
		}
		return hasDentalEnrolleeRemoved(ssapApplicants, lceProcessRequestDTO, planResponse);
	}

	private static boolean hasDentalEnrolleeRemoved(List<SsapApplicant> ssapApplicants, LCEProcessRequestDTO lceProcessRequestDTO, PlanAvailabilityAdapterResponse planResponse) {
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			if (ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(ssapApplicant.getStatus()))) {
				final boolean isDentalEnrollee = checkDentalEnrollee(ssapApplicant.getApplicantGuid(), lceProcessRequestDTO.getEnrolledApplicationAttributes().getDentalEnrollees());
				if (isDentalEnrollee && ReferralUtil.mapSize(planResponse.getDentalPlanDisplayMap()) == ReferralConstants.NONE) {
					return false;
				}
				break;
			}
		}
		return true;
	}

	private static boolean checkDentalEnrollee(String applicantGuid, List<String> dentalEnrollees) {
		boolean bCheck = false;
		final int size = ReferralUtil.listSize(dentalEnrollees);
		for (int i = 0; i < size; i++) {
			if (StringUtils.equalsIgnoreCase(applicantGuid, dentalEnrollees.get(i))) {
				bCheck = true;
				break;
			}
		}
		return bCheck;
	}

	private static boolean checkSubscriberChanged(LCEProcessRequestDTO lceProcessRequestDTO, PlanAvailabilityAdapterResponse planResponse) {
		final String healthSubcriber = lceProcessRequestDTO.getEnrolledApplicationAttributes().getHealthSubscriberId();
		final String compareHealthSubscriber = fetchSubscriber(planResponse.getHealthPlanDisplayMap());
		boolean blnCompare = StringUtils.equalsIgnoreCase(healthSubcriber, compareHealthSubscriber);
		if (!blnCompare) {
			return false;
		}
		final String dentalSubcriber = lceProcessRequestDTO.getEnrolledApplicationAttributes().getDentalSubscriberId();
		final String compareDentalSubscriber = fetchSubscriber(planResponse.getDentalPlanDisplayMap());
		return StringUtils.equalsIgnoreCase(dentalSubcriber, compareDentalSubscriber);
	}

	private static String fetchSubscriber(Map<String, ApplicantPlanDto> planDisplayMap) {
		String s = null;
		for (String applicantGuid : planDisplayMap.keySet()) {
			ApplicantPlanDto temp = planDisplayMap.get(applicantGuid);
			if (temp.getSubscriberFlag() == YorN.Y) {
				s = String.valueOf(temp.getMemberGUID());
				break;
			}
		}
		return s;
	}

	public static boolean hasAddMember(List<SsapApplicant> ssapApplicants) {
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			if (ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(ssapApplicant.getStatus()))) {
				return true;
			}
		}
		return false;
	}

	public static boolean isOpenEnrollment(long coverageYear) {
		final String currentDate = ReferralUtil.formatDate(ReferralUtil.currentDate(), ReferralConstants.DEFDATEPATTERN);
		String oeStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE);
		String oeEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE);
		String currentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
		String previousOEStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_START_DATE);
		String previousOEEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_END_DATE);
		if (currentCoverageYear.equals(String.valueOf(coverageYear))) {
			return ((ReferralUtil.compareDate(ReferralUtil.convertStringToDate(oeStartDate), ReferralUtil.convertStringToDate(currentDate))) >= 0 && (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(oeEndDate), ReferralUtil.convertStringToDate(currentDate)) <= 0));
		} else {
			return ((ReferralUtil.compareDate(ReferralUtil.convertStringToDate(previousOEStartDate), ReferralUtil.convertStringToDate(currentDate))) >= 0 && (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(previousOEEndDate), ReferralUtil.convertStringToDate(currentDate)) <= 0));
		}
	}
	
	
	public static boolean isOepEsd(Timestamp applicationESDDate) {
		final String applicationEsdDate = ReferralUtil.formatDate(new Date(applicationESDDate.getTime()), ReferralConstants.DEFDATEPATTERN);
		String oeESDDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_OEP_EFFECTIVE_START_DATE);
		return ReferralUtil.compareDate(ReferralUtil.convertStringToDate(oeESDDate), ReferralUtil.convertStringToDate(applicationEsdDate)) == 0;
	}

}
