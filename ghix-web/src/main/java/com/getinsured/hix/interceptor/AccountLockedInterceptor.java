/**
 * 
 */
package com.getinsured.hix.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixPlatformConstants;

/**
 * @author Biswakesh
 * 
 */
public class AccountLockedInterceptor extends HandlerInterceptorAdapter {

	private static final Logger LOGGER = Logger
			.getLogger(AccountLockedInterceptor.class);

	private static final String SSO_ACCOUNT_LOCKED_URL = GhixConstants.APP_URL
			+ "/account/user/ssoaccountlocked";

	private static boolean isScimEnabled = GhixPlatformConstants.SCIM_ENABLED;

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String requestURI = request.getRequestURI();
		boolean preHandleStatus = true;
		if(isScimEnabled && requestURI.indexOf("ssoaccountlocked") < 0 && requestURI.indexOf("logout") < 0 && requestURI.indexOf("unlock") < 0) {
			Authentication auth = SecurityContextHolder.getContext()
					.getAuthentication();
			Object loggedInUser = (auth != null) ? auth.getPrincipal() : null;
			if (loggedInUser instanceof AccountUser) {
				AccountUser user = (AccountUser) loggedInUser;

					int confirmed = user.getConfirmed();
					String status = user.getStatus();
					if (confirmed == 0
							|| (null != status && status.length() > 0 && (status
									.equalsIgnoreCase("Inactive") || status
									.equalsIgnoreCase("Inactive-dormant")))) {
						request.getSession().setAttribute(
								GhixPlatformConstants.USER_NAME, user.getEmail());
						response.sendRedirect(SSO_ACCOUNT_LOCKED_URL);
						preHandleStatus = false;
					}
			}

		}
		return preHandleStatus;
	}
}
