package com.getinsured.hix.saml;

import com.getinsured.hix.dto.planmgmt.IssuerPaymentInfoResponse;

import gov.ca.calheers.carrierpayment.beans.CarrierPayment;

public interface SamlPaymentProviderService {
	
	boolean isApplicable(String stateCode, String issuerName);
	String processPaymentRequest(CarrierPayment carrierPayment, IssuerPaymentInfoResponse issuerPaymentInfoResponse);
}
