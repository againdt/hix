var app = angular.module('manageProviderListApp', ['ui.grid', 'ui.bootstrap', 'ui.grid.resizeColumns', 'ui.grid.pagination']);

app.controller('ManageProviderListCtrl', ['$scope', 'ManageProviderListService', 'uiGridConstants', function ($scope, ManageProviderListService, uiGridConstants) {

	$scope.resetFilter = function(logsData) {
		$("#facilityFilter").val("");
		$("#specialityFilter").val("");
		$("#networkIdFilter").val("");
	};

	$scope.colDef = [];
	$scope.PAGE_NUMBER = 1;

	var paginationOptions = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	};

	var displayName = {
		facilities : 'Facilities',
		specialities : 'Specialities',
		inNetworkIds : 'In Network Ids',
		unknownNetworkIds : 'Unknown Network Ids'
	};

	var setPropertiesFn = function (elem, key) {

        switch (key) {
			case 'facilities':
			case 'specialities':
			case 'inNetworkIds':
			case 'unknownNetworkIds':
				elem.displayName = displayName[key];
        }
	};

	$scope.getFacilityReport = function(logsData) {

		ManageProviderListService.getReport($scope.PAGE_NUMBER, paginationOptions.pageSize, $("#facilityFilter").val(), $("#specialityFilter").val(), $("#networkIdFilter").val()).success(function(data) {
//			debugger;
			if (data) {
				$scope.gridOptions.paginationCurrentPage = $scope.PAGE_NUMBER;
				$scope.gridOptions.data = data.content;
				$scope.gridOptions.totalItems = data.totalElements;
			}
			else {
				$scope.gridOptions.data.length = 0;
				$scope.gridOptions.totalItems = 0;
			}
		});
	};

	var excludeFields = ['groupKey'];

	ManageProviderListService.getReport(paginationOptions.pageNumber, paginationOptions.pageSize, $("#facilityFilter").val(), $("#specialityFilter").val(), $("#networkIdFilter").val()).success(function(data) {
//		debugger;

		if (data && data.size != 0) {

			$scope.gridOptions.data = data.content;

			angular.forEach(data.content[0], function(value, key) {

				if (!excludeFields.includes(key)) {
					var elem = null;

					if (key == 'facilities' || key == 'specialities')  {
						elem = {name:key, width: 250};
					}
					else if (key == 'unknownNetworkIds' || key == 'inNetworkIds')  {
						elem = {name:key, width: 340};
					}
					else {
						elem = {name:key, width: 150};	// Default width
					}
					setPropertiesFn(elem, key);
					$scope.colDef.push(elem);
				}
			});
			$scope.gridOptions.totalItems = data.totalElements;
		}
		else {
			$scope.gridOptions.data.length = 0;
			$scope.gridOptions.totalItems = 0;
		}
	});

	$scope.gridOptions = {
		paginationPageSizes: [10],
		paginationPageSize: paginationOptions.pageSize,
		useExternalPagination: true,
		columnDefs:  $scope.colDef,
		enableGridMenu: false,
		enableFiltering: false,
		enableSorting: true,
		// exporterPdfFilename: 'download.pdf',

		onRegisterApi: function(gridApi) {
			$scope.gridApi = gridApi;

			gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
				paginationOptions.pageNumber = newPage;
				paginationOptions.pageSize = pageSize;

				ManageProviderListService.getReport(newPage, pageSize, $("#facilityFilter").val(), $("#specialityFilter").val(), $("#networkIdFilter").val()).success(function(data) {
//					debugger;
					$scope.gridOptions.data = data.content;
					$scope.gridOptions.totalItems = data.totalElements;
				});
			});
		}
	};
}]);

app.service('ManageProviderListService',['$http', function ($http) {

	function getReport(pageNumber, size, facilityFilter, specialityFilter, networkIdFilter) {
//		debugger;
		pageNumber = pageNumber > 0 ? pageNumber - 1 : 0;

		if (!facilityFilter) {
			facilityFilter = "";
		}

		if (!specialityFilter) {
			specialityFilter = "";
		}

		if (!networkIdFilter) {
			networkIdFilter = "";
		}
		return  $http({
			method: 'GET',
			url: '/hix/admin/provider/getFacilityDataList?page=' + pageNumber + '&size=' + size + '&facilityFilter=' + facilityFilter + '&specialityFilter=' + specialityFilter + '&networkIdFilter=' + networkIdFilter
		});
	}

	return {
		getReport:getReport
	};
}]);
