<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>

<%
String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");


if(request.getParameter("lang") == null && session.getAttribute("lang") == null){
	session.setAttribute("lang","en");
}else if(request.getParameter("lang") != null && request.getParameter("lang") != session.getAttribute("lang")   ){
	session.setAttribute("lang", request.getParameter("lang"));
}

%>
<c:set var="userActiveRoleName" value="<%=userActiveRoleName%>" />

<input type="hidden" value="<%=session.getAttribute("lang")%>" id="lang">

<div class="navbar navbar-fixed-top navbar-inverse" id="masthead" role="banner">
  <div class="navbar-inner">
    <div class="container">
		<ul class="nav">
			<li>
        			<a href="#" title="Link open in new window or tab" onclick="openInNewTab('<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL)%>')"><img class="brand" src="<gi:cdnurl value="/resources/img/" /><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE)%>"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/></a>
			</li>
      	</ul>
      	
      	<div class="nav-collapse collapse pull-right">
			<ul class="nav pull-right">
				<li><a href="#rightpanel" name="skip" class="skip-nav" accesskey="n">Skip to Main Content</a></li>
				<li><a href="https://www.accesshealthct.com/AHCT/DisplayCreateUserAccount.action"><spring:message code="label.homePage.masthead.enrollNow" /></a></li>
				<li><a href="#" id="language-select">English</a></li>
				<c:if test="${userActiveRoleName != null && userActiveRoleName != 'null' && userActiveRoleName != ''}">
					<li><a href="<gi:cdnurl value="/account/user/logout" />">Logout</a></li>
				</c:if>
			</ul>
		</div>
    </div>  
  </div> 
</div>
