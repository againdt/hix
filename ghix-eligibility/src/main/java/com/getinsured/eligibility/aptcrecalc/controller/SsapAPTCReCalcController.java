package com.getinsured.eligibility.aptcrecalc.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.aptcrecalc.dto.Request;
import com.getinsured.eligibility.aptcrecalc.dto.Response;
import com.getinsured.eligibility.aptcrecalc.service.SsapAptcReCalcService;
import com.getinsured.eligibility.at.resp.service.AptcHistoryService;
import com.getinsured.eligibility.at.resp.service.SsapApplicationService;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.AptcHistory;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;

@Controller
@RequestMapping(value="/ssapaptcrecalc")
public class SsapAPTCReCalcController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SsapAPTCReCalcController.class);
	
	private static final String KEEP_APTC_AMOUNT = "keepAptcAmount";
	private static final String SHOP_APTC_AMOUNT = "shopAptcAmount";
	@Autowired private Gson platformGson;
	@Autowired
	private SsapApplicationService ssapApplicationService;
	@Autowired
	private SsapAptcReCalcService ssapAptcReCalcService;
	@Autowired
	private AptcHistoryService aptcHistoryService;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private AptcHistoryRepository aptcHistoryRepository;
	@Autowired private GIMonitorService giMonitorService;
	
	private static final String INPUT_COVERAGE_START_DATE = "coverageStartDate";
	private static final String INPUT_EXTERNAL_CASE_ID = "externalCaseId";
	private static final String INPUT_ENROLLMENT_YEAR_KEY = "enrollmentYear";
	private static final String ERROR_CODE_201 = "201";
	private static final String SUCEES_CODE_200 = "200";
	private static final String ERROR_CODE_203 = "203";
	private static final String ERROR_CODE_204 = "204";
	private static final String ERROR_CODE_201_DESCRIPTION = "No record found from the given search criteria.";
	private static final String ERR_CODE_KEY = "errCode";
	private static final String ERR_CODE_DESCRIPTION_KEY = "errCodeDescription";
	private static final String GI_MONITOR_ID = "monitorId";
	private static final String REFERRAL_ERR01 = "REFERRAL_ERR01";
	
	//coverage start date,enrollment year, external case id
		@SuppressWarnings("unchecked")
		@RequestMapping(value = "/fetchLatestAPTCAmount", method = RequestMethod.POST)
		@ResponseBody
		public String fetchLatestAPTCAmount(@RequestBody Map<String, String> inputData) {
			String aptcRecalc = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_AHBX_APTC_RECALC_ENABLE);
			Map<String,String> response = new HashMap<String,String>();
			response.put(ERR_CODE_KEY, SUCEES_CODE_200);
			LOGGER.info("iex.AT.aptcRecalc.enable = {}", aptcRecalc);
			if("ON".equalsIgnoreCase(aptcRecalc)){
				boolean isValidRequest = this.isValidRequestForAhbxApiCall(inputData);
				LOGGER.info("isValidRequest = {}", isValidRequest);
				if(isValidRequest){
					LOGGER.info("INPUT_EXTERNAL_CASE_ID = {}", inputData.get(INPUT_EXTERNAL_CASE_ID));
					Date coverageStartDate = parseCoverageStartDate(inputData, response);
					if(coverageStartDate != null){
						try{
							this.proceedForAhbxCall(inputData, response, coverageStartDate);	
						}
						catch(Exception e){
							response.put(ERR_CODE_KEY, ERROR_CODE_204);
							response.put(ERR_CODE_DESCRIPTION_KEY, "Error in AHBX invocation.");
							LOGGER.error("Error in AHBX invocation.",e.getMessage());
						}
					}
					else{
						response.put(ERR_CODE_KEY, ERROR_CODE_203);
						response.put(ERR_CODE_DESCRIPTION_KEY, "Expected format of Coverage Start Date is MM/dd/yyyy.");
					}
				}
				else{
					
					response.put(ERR_CODE_KEY, ERROR_CODE_203);
					response.put(ERR_CODE_DESCRIPTION_KEY, "External Case Id, Enrollment Year and Coverage Start Date are mandatory fields.");
				}
			}
			LOGGER.info("Response Code = {}", response.get(ERR_CODE_KEY));
			LOGGER.info("Response DESCRIPTION = {}", response.get(ERR_CODE_DESCRIPTION_KEY));
			
			return platformGson.toJson(response);
		}

	private void proceedForAhbxCall(Map<String, String> inputData, Map<String, String> response,
			Date coverageStartDate) throws Exception{
		LOGGER.info(" Inside proceedForAhbxCall method ");
		try{
			Long coverageYear = Long.parseLong(inputData.get(INPUT_ENROLLMENT_YEAR_KEY));
			List<SsapApplication> ssapApplications = ssapApplicationService.findByHHCaseIdAndCoverageYear(inputData.get(INPUT_EXTERNAL_CASE_ID),coverageYear);
			if(ssapApplications != null && ssapApplications.size() > 0){
				SsapApplication latestSsapApplication =  ssapApplications.get(0);
				LOGGER.info(" latestSsapApplication Id {} ", latestSsapApplication.getId());
				AptcHistory aptcHistory = aptcHistoryService.getLatestByApplicationId(latestSsapApplication.getId());
				if(aptcHistory != null && aptcHistory.getEligStartDate() != null && aptcHistory.getEligStartDate().compareTo(coverageStartDate) != 0){
					LOGGER.info(" Calling  callAhbxReCalcApi API ");
					Response responseFromAhbx = ssapAptcReCalcService.callAhbxReCalcApi(inputData,latestSsapApplication);
					if(responseFromAhbx != null ){
						LOGGER.info(" responseFromAhbx status {} ", responseFromAhbx.getStatus());
						if("SUCCESS".equalsIgnoreCase(responseFromAhbx.getStatus())){
							this.saveAptcAmount(response, coverageStartDate, latestSsapApplication, aptcHistory,responseFromAhbx);
						}
						else{
							response.put(ERR_CODE_KEY, responseFromAhbx.getErrorCode());
							response.put(ERR_CODE_DESCRIPTION_KEY, "Error Code received from AHBX.");
							GIRuntimeException ex = new GIRuntimeException("Error Code received from AHBX.");
							GIMonitor monitorObj = giMonitorService.saveOrUpdateErrorLog(REFERRAL_ERR01, new TSDate(),ex.getClass().getName(),ExceptionUtils.getStackTrace(ex), null);
							response.put(GI_MONITOR_ID, String.valueOf(monitorObj.getId()));
							LOGGER.error("Error Code received from AHBX",responseFromAhbx.getErrorCode());
						}
					}
					else{
						LOGGER.error("RECEIVED NULL RESPONSE FOR AHBX APTC RECALC API");
					}
				}
			}
			else{
				response.put(ERR_CODE_KEY, ERROR_CODE_201);
				response.put(ERR_CODE_DESCRIPTION_KEY, ERROR_CODE_201_DESCRIPTION);
			}
		}catch(Exception e){
			GIMonitor monitorObj = giMonitorService.saveOrUpdateErrorLog(REFERRAL_ERR01, new TSDate(), e.getClass().getName(), ExceptionUtils.getStackTrace(e), null);
			response.put(GI_MONITOR_ID, String.valueOf(monitorObj.getId()));
			throw e;
		}
	}

	private void saveAptcAmount(Map<String, String> response, Date coverageStartDate,
			SsapApplication latestSsapApplication, AptcHistory aptcHistory, Response responseFromAhbx) {
		LOGGER.info(" Inside saveAptcAmount method ");
		AptcHistory reCalcAptcHistory = aptcHistory;
		reCalcAptcHistory.setId(0);
		reCalcAptcHistory.setIsRecalc("Y");
		if(responseFromAhbx.getKeepAptcAmount() != null){
			reCalcAptcHistory.setEffectiveDate(coverageStartDate);
			reCalcAptcHistory.setEligStartDate(coverageStartDate);
			latestSsapApplication.setMaximumAPTC(new BigDecimal(responseFromAhbx.getKeepAptcAmount()).setScale(2, BigDecimal.ROUND_HALF_UP));
			reCalcAptcHistory.setAptcAmount(new BigDecimal(responseFromAhbx.getKeepAptcAmount()).setScale(2, BigDecimal.ROUND_HALF_UP));
		}
		
		if(responseFromAhbx.getShopAptcAmount() != null){
			reCalcAptcHistory.setEffectiveDate(coverageStartDate);
			reCalcAptcHistory.setEligStartDate(coverageStartDate);
			latestSsapApplication.setShopAptcAmnt(new BigDecimal(responseFromAhbx.getShopAptcAmount()).setScale(2, BigDecimal.ROUND_HALF_UP));
			reCalcAptcHistory.setShopAptcAmnt(new BigDecimal(responseFromAhbx.getShopAptcAmount()).setScale(2, BigDecimal.ROUND_HALF_UP));	
		}

		if(responseFromAhbx.getKeepStateSubsidyAmount() != null){
			reCalcAptcHistory.setEffectiveDate(coverageStartDate);
			reCalcAptcHistory.setStateSubsidyStartDate(coverageStartDate);
			latestSsapApplication.setMaximumStateSubsidy(new BigDecimal(responseFromAhbx.getKeepStateSubsidyAmount()));
			reCalcAptcHistory.setStateSubsidyAmt(new BigDecimal(responseFromAhbx.getKeepStateSubsidyAmount()));
		}

		if(responseFromAhbx.getShopStateSubsidyAmount() != null){
			reCalcAptcHistory.setEffectiveDate(coverageStartDate);
			reCalcAptcHistory.setStateSubsidyStartDate(coverageStartDate);
			latestSsapApplication.setShopStateSubsidyAmnt(new BigDecimal(responseFromAhbx.getShopStateSubsidyAmount()));
			reCalcAptcHistory.setStateSubsidyAmt(new BigDecimal(responseFromAhbx.getShopStateSubsidyAmount()));
		}


		ssapApplicationRepository.save(latestSsapApplication);
		aptcHistoryRepository.save(aptcHistory);
		
		response.put(ERR_CODE_KEY, SUCEES_CODE_200);
	}

	private Date parseCoverageStartDate(Map<String, String> inputData, Map<String, String> response) {
			Date coverageStartDate=null;
			if(inputData.containsKey(INPUT_COVERAGE_START_DATE) && inputData.get(INPUT_COVERAGE_START_DATE) != null){
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				try {
					coverageStartDate = sdf.parse(inputData.get(INPUT_COVERAGE_START_DATE));
					}catch (ParseException e) {
						
					}
				
			}
			return coverageStartDate;
	}
		
	private boolean isValidRequestForAhbxApiCall(Map<String,String> inputData){
			
		if(inputData != null && 
			inputData.containsKey(INPUT_EXTERNAL_CASE_ID) && inputData.get(INPUT_EXTERNAL_CASE_ID) != null && 
			inputData.containsKey(INPUT_ENROLLMENT_YEAR_KEY) && inputData.get(INPUT_ENROLLMENT_YEAR_KEY)!= null &&
			inputData.containsKey(INPUT_COVERAGE_START_DATE) && inputData.get(INPUT_COVERAGE_START_DATE) != null){
				
			return Boolean.TRUE;
		}
		else{
			return Boolean.FALSE;
		}
		
	}
	
	/*
	 * This API is only used for generating mock response while calling from callAhbxReCalcApi as mock request.
	 */
	@RequestMapping(value = "/testAhbx", method = RequestMethod.PUT)
	@ResponseBody
	public String testAhbxApi(@RequestBody String inputJson){
		String response = "";
		try{
			
			Request aptcRequest= platformGson.fromJson(inputJson, Request.class);
			Response responseDetails = new Response();
			responseDetails.setKeepAptcAmount("153.72");
			responseDetails.setShopAptcAmount("150.00");
			responseDetails.setStatus("SUCCESS");
			response = platformGson.toJson(responseDetails);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return response;
	}
	
	
	 
}
