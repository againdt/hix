package com.getinsured.eligibility.at.ref.service.migration;

import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.service.ReferralEligibilityDecisionService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.resp.si.dto.ApplicationExtension;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.model.SepEvents.Source;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.timeshift.util.TSDate;

@Component("referralEligibilityDecisionMigrationService")
public class ReferralEligibilityDecisionMigrationServiceImpl implements ReferralEligibilityDecisionService {
	private static final Logger LOGGER = Logger.getLogger(ReferralEligibilityDecisionMigrationServiceImpl.class);
	
	private static final String OTHER_ELIGIBILITY_CHANGE = "OTHER_ELIGIBILITY_CHANGE";

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("referralSEPMigrationService")
	private ReferralSEPMigrationService referralSEPMigrationService;

	@Autowired
	private IUserRepository userRepository;
	
	@Autowired
	@Qualifier("qleValidationMigrationService")
	private QLEValidationService qleValidationService;
	
	@Autowired private MigrationUtil migrationUtil;
	@Autowired private ApplicationExtensionEventMigrationUtil applicationExtensionEventMigrationUtil;
	
	@Override
	@ReferralTransactionAnno
	public boolean execute(long applicationId, boolean cmrAutoLinking) {
		boolean blnComplete = false;
		try {
			LOGGER.info("ReferralEligibilityDecision for Referral application " + applicationId);

			final SsapApplication application = ssapApplicationRepository.findOne(applicationId);

			if (application == null) {
				LOGGER.error(ReferralConstants.NO_SSAP_FOUND + applicationId);
				throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + applicationId);
			}

			final boolean checkEligibility = isApplicationDenied(application);

			if (checkEligibility) {
				closeSsapApplication(application);
				blnComplete = true;
			} else {
				if (!cmrAutoLinking) {
					// set status back to UC to work activation flow...
					setUnclaimedStatus(application);
				}
				blnComplete = cmrAutoLinking;
			}

			LOGGER.info("ReferralEligibilityDecision for Referral application Ends");
		} catch (Exception e) {
			throw e;
		}
		return blnComplete;
	}

	@Override
	@ReferralTransactionAnno
	public boolean execute(long applicationId, ApplicationExtension applicationExtension, boolean cmrAutoLinking) {
		boolean blnComplete = false;
		try {
			LOGGER.info("ReferralEligibilityDecision for Referral application " + applicationId);

			final SsapApplication application = ssapApplicationRepository.findOne(applicationId);

			if (application == null) {
				LOGGER.error(ReferralConstants.NO_SSAP_FOUND + applicationId);
				throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + applicationId);
			}
			final boolean checkEligibility = isApplicationDenied(application);

			if (checkEligibility) {
				closeSsapApplication(application);
				blnComplete = true;
			} else {
				if (!cmrAutoLinking) {
					// set status back to UC to work activation flow...
					setUnclaimedStatus(application);
				}
				blnComplete = processQEAutomation(application, applicationExtension, cmrAutoLinking);
			}
			LOGGER.info("ReferralEligibilityDecision for Referral application Ends, blnComplete - " + blnComplete);
		} catch (Exception e) {
			throw e;
		}
		return blnComplete;
	}

	private boolean processQEAutomation(SsapApplication application, ApplicationExtension applicationExtension, boolean cmrAutoLinking) {
		boolean blnComplete = false;
		// check for Automation
		final SsapApplicant primaryApplicant = retrievePrimaryApplicant(application);
		final ApplicantEvent applicantEvent = ApplicationExtensionEventUtil.applicantEventForAutoQEP(primaryApplicant, applicationExtension);
		if (applicantEvent != null) {
			// create Application Event
			if("IncomeChange".equalsIgnoreCase(applicantEvent.getCode()) && applicantEvent.getEventDate().after(new TSDate())){
				applicantEvent.setEventDate(new TSDate());
			}
			createAutoQEPEvent(application, applicantEvent);
			AccountUser updateUser = userRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
			qleValidationService.runValidationEngine(application.getCaseNumber(),  Source.EXTERNAL, updateUser.getId());
			// if denial
			if (referralSEPMigrationService.checkForDenial(application, applicantEvent.getEventDate()) || applicationExtensionEventMigrationUtil.isFutureEventDenied(application, applicantEvent)) {
				qleValidationService.denySep(application.getCaseNumber(),updateUser.getId());
				blnComplete = true;
			} else {
				blnComplete = cmrAutoLinking;
			}
		} else { 
			Timestamp activityDate = migrationUtil.getActivityDate(application.getId());
			if(activityDate!=null){
				referralSEPMigrationService.persistQEPEventforApplicantandApplication(application, activityDate, OTHER_ELIGIBILITY_CHANGE);
			} else {
				referralSEPMigrationService.persistQEPEventforApplicantandApplication(application, new TSDate(), OTHER_ELIGIBILITY_CHANGE);
			}
			blnComplete = cmrAutoLinking;
		}
		return blnComplete;
	}
	
	private void createAutoQEPEvent(SsapApplication application, ApplicantEvent applicantEvent) {
		final String eventCode = ApplicationExtensionEventUtil.QEP_EVENT_NAME_MAP.get(applicantEvent.getCode());
		referralSEPMigrationService.persistQEPEventforApplicantandApplication(application, applicantEvent.getEventDate(), eventCode);
	}

	private boolean isApplicationDenied(SsapApplication application) {
		return EligibilityStatus.DE.equals(application.getEligibilityStatus());
	}

	private void closeSsapApplication(SsapApplication ssapApplication) {
		LOGGER.info("Close Application id " + ssapApplication.getId());
		ssapApplication.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
		ssapApplicationRepository.save(ssapApplication);
	}

	private void setUnclaimedStatus(SsapApplication ssapApplication) {
		ssapApplication.setApplicationStatus(ApplicationStatus.UNCLAIMED.getApplicationStatusCode());
		ssapApplicationRepository.save(ssapApplication);
	}

	private SsapApplicant retrievePrimaryApplicant(SsapApplication currentApplication) {
		for (SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()) {
			if (ssapApplicant.getPersonId() == ReferralConstants.PRIMARY) {
				return ssapApplicant;
			}
		}
		return null;
	}

}