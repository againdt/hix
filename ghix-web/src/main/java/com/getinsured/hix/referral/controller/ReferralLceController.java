package com.getinsured.hix.referral.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.model.SepEvents.Gated;
import com.getinsured.eligibility.referral.ui.dto.LceActivityDTO;
import com.getinsured.eligibility.referral.ui.dto.SepEventDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.referral.exception.ReferralServiceException;
import com.getinsured.hix.referral.service.ReferralActivationService;
import com.getinsured.hix.referral.validator.SepEventsValidator;
import com.getinsured.iex.dto.QleApplicantsDto;
import com.getinsured.iex.dto.QleEventDto;
import com.getinsured.iex.dto.QleResponeDto;
import com.getinsured.iex.ssap.model.SsapApplicantEvent.ApplicantEventValidationStatus;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;

/**
 * 
 * @author raguram_p
 * 
 */

@Controller
@RequestMapping("/referral")
@DependsOn("dynamicPropertiesUtil")
public class ReferralLceController {

	@Autowired
	@Qualifier("referralActivationService")
	private ReferralActivationService referralActivationService;

	@Autowired
	private UserService userService;

	@Autowired
	@Qualifier("sepEventsValidator")
	private SepEventsValidator sepEventsValidator;

	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired
	GhixRestTemplate ghixRestTemplate;

	@Autowired private Gson platformGson;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralLceController.class);

	private static final String REDIRECT_ACCOUNT_USER_LOGINFAILED = "redirect:/account/user/loginfailed";

	private enum Events {
		ADD, REMOVE, QUALIFYING_EVENT, OTHER
	}

	private AccountUser isAllowed(Model model, HttpServletRequest request) {

		AccountUser user = null;
		HashMap<String, Object> errorMsg = new HashMap<String, Object>();

		try {
			user = userService.getLoggedInUser();
			if (user == null) {
				errorMsg.put("message", "User not authorized.");
				model.addAttribute("authfailed", errorMsg.get("message"));
				request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION", errorMsg);
			}
		} catch (InvalidUserException ex) {
			LOGGER.error("User not logged in");
			errorMsg.put("message", "User not logged in.");
			model.addAttribute("authfailed", errorMsg.get("message"));
			request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION", errorMsg);
		}
		return user;
	}

	@GiAudit(transactionName = "SEP-QEP Events", eventType = EventTypeEnum.MODIFY_PII_DATA, eventName = EventNameEnum.FINANCIAL_APP)
	@RequestMapping(value = { "/lce/events/applicants/{casenumber}" }, method = RequestMethod.GET)
	public String fetchLceEventsforApplicants(@PathVariable("casenumber") String casenumber, Model model, HttpServletRequest request) {

		AccountUser user = isAllowed(model, request);

		if (user == null) {
			LOGGER.warn("User not authorized");
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User not authorized"));
			return REDIRECT_ACCOUNT_USER_LOGINFAILED;
		}

		Map<String, List<SepEvents>> sepEvents = populateSepEventsandQualifyingEvents();
		model.addAttribute("sepEventsAdd", sepEvents.get(Events.ADD.name()));
		model.addAttribute("sepEventsRemove", sepEvents.get(Events.REMOVE.name()));
		model.addAttribute("sepEventsOther", sepEvents.get(Events.OTHER.name()));
		model.addAttribute("qualifyEvents", sepEvents.get(Events.QUALIFYING_EVENT.name()));
		
		model.addAttribute("serverDate", ReferralUtil.formatDate(new TSDate(), ReferralConstants.UI_DATE_FORMAT));
		try {
			String decryptedCaseNumber = toDecrypt(casenumber);
			LOGGER.debug("Inside processLceCheck");

			LceActivityDTO lceActivity = referralActivationService.fetchApplicantsforLceEvents(decryptedCaseNumber);
			GiAuditParameterUtil.add(new GiAuditParameter("lceActivity", lceActivity), new GiAuditParameter("Case Number", decryptedCaseNumber));

			if (lceActivity != null && lceActivity.isApplicationEventExists()) {
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Invalid Phase : Application Event Exists "));
				return "referral/invalidphase";
			}
			
			if (lceActivity != null) {
				lceActivity.setUserId(user.getId());				
				model.addAttribute("lceActivity", lceActivity);
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
				return "referral/lce/events";
			} else {
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Case number is not valid "));
				throw new GIRuntimeException("Case number is not valid - " + casenumber);
			}
		} catch (Exception e) {
			LOGGER.error("Error while fetching lce applicants  " + e.getMessage());
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.LCE_CHECK_EXCEPTION.getId(), e, null, Severity.HIGH);
		}
	}

	@GiAudit(transactionName = "SEP-QEP Events", eventType = EventTypeEnum.MODIFY_PII_DATA, eventName = EventNameEnum.FINANCIAL_APP)
	@RequestMapping(value = { "/lce/events/applicants/{casenumber}" }, method = RequestMethod.POST)
	public String updateSepEventsforApplicantandApplication(@PathVariable("casenumber") String casenumber, @ModelAttribute("lceActivity") LceActivityDTO lceActivity, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes,
	        BindingResult bindingResult) {
		try {

			Map<String, String> selectedEvents = new HashMap<String, String>();
			Map<String, String> selectedEventDates = new HashMap<String, String>();
			Map<String, String> allowFutureDate = new HashMap<String, String>();

			final HttpSession httpSession = request.getSession();

			if (httpSession.getAttribute("isEnrolledStatus") != null) {
				httpSession.removeAttribute("isEnrolledStatus");
			}
			GiAuditParameterUtil.add(new GiAuditParameter("lceActivity", lceActivity));
			sepEventsValidator.validate(lceActivity, bindingResult);
			if (bindingResult.hasErrors()) {
				LOGGER.info("SEP-QEP Event Submission has errors " + bindingResult.getAllErrors());
				httpSession.setAttribute("lceAllErrors", bindingResult.getAllErrors());
				populateSelectedValues(lceActivity, selectedEvents, selectedEventDates, allowFutureDate);
				httpSession.setAttribute("selectedEvents", selectedEvents);
				httpSession.setAttribute("selectedEventDates", selectedEventDates);
				httpSession.setAttribute("allowFutureDate", allowFutureDate);
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, " Life Change Reporting Request Has Validation Errors "));
				return "redirect:/referral/lce/events/applicants/" + casenumber;
			}
			
			Map<String, String> result = referralActivationService.updateSepEventsforApplicantandApplications(lceActivity);

			GiAuditParameterUtil.add(new GiAuditParameter(ReferralConstants.PROCESS_FLOW, result.get(ReferralConstants.PROCESS_FLOW)));
			if (result.get(ReferralConstants.STATUS_KEY).equals(ReferralConstants.SUCCESS + "")) {
				if (result.get(ReferralConstants.RESULT_KEY) != null) {
					model.addAttribute("endDate", result.get(ReferralConstants.RESULT_KEY));
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
					//return "referral/lce/updated";
					/**
					 * check whether we can put the application in Auto flow or not
					 * if the events is qualifying event - not needed to auto flow
					 * if the events is gated - not needed to auto flow
					 * So only the events that are not gated will result in autoflow
					 */
					boolean autoFlowEnabled = checkForAutomationFlow(lceActivity);
					LOGGER.info("Auto Flow enabled: "+autoFlowEnabled+" for lceActivity: "+lceActivity);
					if(autoFlowEnabled) {
						// if yes then we can automate the events for processing
						automateEventsProcessing(toDecrypt(casenumber));	
					}					
					return "redirect:/indportal";
				} else {
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE,
					        " Unable to process Life Change Reporting request - SsapApplicationEvent not saved "));
					throw new GIRuntimeException("Unable to process life change reporting request - SsapApplicationEvent not saved for " + toDecrypt(casenumber));
				}
			} else {
				if (result.get(ReferralConstants.STATUS_KEY).equals(ReferralConstants.STATUS_NOT_VALID + "")) {
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, " Application/Applicant Event Status not valid "));
					return "referral/lce/failure";
				} else {
					final String reason = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ONLINE1_DENIAL_REASON_ENGLISH);
					model.addAttribute("denialReason", reason);
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, " SEP denined with " + reason));
					return "referral/lce/denied";
				}
			}

		} catch (Exception e) {
			LOGGER.error("Error_Msg @ processLceCheck  " + e.getMessage());
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.LCE_CHECK_EXCEPTION.getId(), e, null, Severity.HIGH);
		}

	}

	/**
	 * This method will check for the non gated events and also for qualifying events
	 *  to determine whether the auto flow is to be triggered or not
	 * @param lceActivityDTO
	 * @return
	 */
	private boolean checkForAutomationFlow(LceActivityDTO lceActivityDTO) {
		LOGGER.info("inside checkForAutomationFlow lceActivityDTO : "+lceActivityDTO);
		boolean isAutoFlowEnabled = false;
		try {			
			
			Map<String, List<SepEvents>> sepEvents = referralActivationService.fetchSepAndQEPEvents();

			List<SepEvents> sepEventsOtherList = sepEvents.get(Events.OTHER.name());
	
			Map<String, SepEvents> sepEventsOther =filterRecordsByChangeType(Events.OTHER.name(),sepEventsOtherList);
			
			//check for LCE application
			if(!(lceActivityDTO != null && lceActivityDTO.getQualifyingEventMap() != null && lceActivityDTO.getQualifyingEventMap().keySet().size() > 0)) {
				// then application is SEP so check for non gated events
				// Change of Address map
				if (!lceActivityDTO.getChangeOfAddressMembersMap().isEmpty()) {
					SepEvents sepEvent = sepEventsOther.get("CHANGE_IN_ADDRESS");
					if(null!= sepEvent && sepEvent.getGated()== Gated.N) {
						isAutoFlowEnabled = true;
					}
				}
				
				if (!lceActivityDTO.getChangeOfDobMembersMap().isEmpty()) {
					SepEvents sepEvent = sepEventsOther.get("DOB_CHANGE");
					if(null!= sepEvent && sepEvent.getGated()== Gated.N) {
						isAutoFlowEnabled = true;
					}
				}
				
				if (!lceActivityDTO.getHouseholdChangeMembersMap().isEmpty()) {
					SepEvents sepEvent = sepEventsOther.get(lceActivityDTO.getHouseholdChangeReason() == null ? "OTHER" : lceActivityDTO.getHouseholdChangeReason());
					if(null!= sepEvent && sepEvent.getGated()== Gated.N) {
						isAutoFlowEnabled = true;
					}
				}
				
				/**
				 * Step 3 - Added Members
				 */
				List<String> addedMembersGatedType = new ArrayList<>();
				for (String addedApplicantGuid : lceActivityDTO.getAddedMembersMap().keySet()) {
					SepEventDTO addedApplicantSepEventDTO = lceActivityDTO.getAddedMembersMap().get(addedApplicantGuid);
					addedMembersGatedType.add(addedApplicantSepEventDTO.getIsGated());
				}
				
				// check for multiple gated or not gated events - even if one is gated checkautomation will not be true
				if(!addedMembersGatedType.isEmpty()) {
					boolean containsGated = addedMembersGatedType.contains(Gated.Y.toString());
					LOGGER.info("containsGated: added "+containsGated);
					if(!containsGated) {
						isAutoFlowEnabled = true;
					}					
				}
				
				/**
				 * Step 4 - Remove Members
				 */
				List<String> removedMembersGatedType = new ArrayList<>();
				for (String removedApplicantGuid : lceActivityDTO.getRemovedMembersMap().keySet()) {
					SepEventDTO removedApplicantSepEventDTO = lceActivityDTO.getRemovedMembersMap().get(removedApplicantGuid);
					removedMembersGatedType.add(removedApplicantSepEventDTO.getIsGated());
				}
				
				// check for multiple gated or not gated events - even if one is gated checkautomation will not be true
				if(!removedMembersGatedType.isEmpty()) {
					boolean containsGatedRemovedMap = removedMembersGatedType.contains(Gated.Y.toString());
					if(!containsGatedRemovedMap) {
						isAutoFlowEnabled = true;
					}						
				}
				
				/**
				 * Step 5 - No Change Members
				 */
				List<String> noChangeMembersGatedType = new ArrayList<>();
				for (String noChangeApplicantGuid : lceActivityDTO.getNoChangeMembersMap().keySet()) {					
					//SepEvents sepEvent = sepEventsOther.get("OTHER");
					SepEventDTO noChangeSepEventDTO = lceActivityDTO.getNoChangeMembersMap().get(noChangeApplicantGuid);
					if(null != noChangeSepEventDTO.getIsGated()) {
						noChangeMembersGatedType.add(noChangeSepEventDTO.getIsGated());	
					}
				}
				
				// check for multiple gated or not gated events - even if one is gated checkautomation will not be true
				if(!noChangeMembersGatedType.isEmpty()) {
					boolean containsGatedNoChangeMap = noChangeMembersGatedType.contains(Gated.Y.toString());
					if(!containsGatedNoChangeMap) {
						isAutoFlowEnabled = true;
					}	
				}
				
			}		
			
		}catch (Exception e) {
			LOGGER.error("Error at checkAutoFlow", e);			
		}
		return isAutoFlowEnabled;
		
	}
	
	private Map<String, SepEvents> filterRecordsByChangeType(final String changeType, List<SepEvents> allFinRecords) {
		Map<String, SepEvents> changeTypeResults = new LinkedHashMap<String, SepEvents>();
		for (SepEvents sepEvent : allFinRecords) {
			if (changeType.equals(sepEvent.getChangeType())) {
				changeTypeResults.put(sepEvent.getName(), sepEvent);
			}
		}		
		return changeTypeResults;
	}


	/**
	 * This method will fetch all the events by application 
	 * Iterate through it and automate the flow
	 * @param casenumber
	 */
	private void automateEventsProcessing(String casenumber) {
		try {
			AccountUser accountUser = userService.getLoggedInUser();
			// we will reach here when consumer confirms life event and events selected are only not gated
			// hence pass event type as n for the automation
			String eventType = "n";
			String result  = ghixRestTemplate.getForObject(GhixEndPoints.ELIGIBILITY_URL + "ssap/retrieveEventsByApplication/"+accountUser.getActiveModuleId()+"/"+casenumber+"/"+eventType,String.class);
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("automateEventsProcessing retrieveEventsByApplication result: {} for active module ID : {}  case number : {} eventType: {} ",
						result,accountUser.getActiveModuleId(),casenumber,eventType);	
			}			
			QleResponeDto qleResponeDto = platformGson.fromJson(result, QleResponeDto.class);
			// fetch the applicantEventid to process
			String applicantEventId = fetchApplicantEventIdToProcess(qleResponeDto);
			if(null != applicantEventId) {
				com.getinsured.hix.dto.ssap.SsapApplicantEvent sae = new com.getinsured.hix.dto.ssap.SsapApplicantEvent();
				sae.setId(Long.parseLong(applicantEventId));//qleRequestDto.getApplicantEventId()
				sae.setLastUpdatedUserId((int)accountUser.getId());//qleRequestDto.getUserId().intValue()
				sae.setStatus(ApplicantEventValidationStatus.NOTREQUIRED);//qleRequestDto.getUserName()
				ResponseEntity<String> resultAuto = ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"/ssap/applicantevent/"+applicantEventId, accountUser.getUsername(), HttpMethod.PUT, MediaType.APPLICATION_JSON, String.class, sae);
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("Auto flow triggered for eventid: {} and response from the api is {}",applicantEventId,resultAuto);	
				}
				
			}else {
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("Auto flow is not triggered as applicantEventid is null");	
				}				
			}
			
		}catch (Exception ex) {			
			LOGGER.error("Exception occured while automateEventsProcessing: "+casenumber,ex);
		}		
	
	}

	/**
	 * This method will be used to fetch the applicant event id for which the auto process will start 
	 * for not gated event selected	
	 * @param qleResponeDto
	 * @return
	 */
	private String fetchApplicantEventIdToProcess(QleResponeDto qleResponeDto) {
		String applicantEventIdToProcess = null;
		List<String> applicantIdList = new ArrayList();
		for(QleApplicantsDto dto : qleResponeDto.getApplicants()) {
			for(QleEventDto qleEventDto : dto.getEvents()) {
				// create a list with applicant id ignoring the eventNAme as OTHER and eventCategory as OTHER 
				String eventName = qleEventDto.getEventName();
				String eventCategory = qleEventDto.getEventCategory();
				if(!(eventName.equalsIgnoreCase("OTHER") && eventCategory.equalsIgnoreCase("OTHER"))) {
					/**
					 * event other would be ignored for the initial applicant
					 * and make an event list and trigger processing for the first element 
					 */
					String applicantEventId = qleEventDto.getApplicantEventId();
					applicantIdList.add(applicantEventId);
				}					
			}
		}
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("applicantIdList made for processing: "+applicantIdList);	
		}
		
		
		if(!applicantIdList.isEmpty()) {
			applicantEventIdToProcess = applicantIdList.get(0);
		}
		return applicantEventIdToProcess;
	}

	private Map<String, List<SepEvents>> populateSepEventsandQualifyingEvents() {
		Map<String, List<SepEvents>> sepEvents = referralActivationService.fetchSepAndQEPEvents();

		List<SepEvents> sepEventsAdd = sepEvents.get(Events.ADD.name());
		List<SepEvents> sepEventsRemove = sepEvents.get(Events.REMOVE.name());
		List<SepEvents> sepEventsOther = sepEvents.get(Events.OTHER.name());
		List<SepEvents> qualifyEvents = sepEvents.get(Events.QUALIFYING_EVENT.name());

		if (sepEventsAdd == null || sepEventsAdd.isEmpty() || sepEventsRemove == null || sepEventsRemove.isEmpty() || sepEventsOther == null || sepEventsOther.isEmpty() || qualifyEvents == null || qualifyEvents.isEmpty()) {
			throw new GIRuntimeException("Unable to fetch SEP and QEP Events from sep lookup table - Please contact admin.");
		}

		return sepEvents;
	}

	private void populateSelectedValues(LceActivityDTO lceActivity, Map<String, String> selectedEvents, Map<String, String> selectedEventDates, Map<String, String> allowFutureDate) {

		if (lceActivity.getAddedMembersMap() != null) {
			for (String addedApplicantGuid : lceActivity.getAddedMembersMap().keySet()) {
				if (lceActivity.getAddedMembersMap().get(addedApplicantGuid).getName() != null && lceActivity.getAddedMembersMap().get(addedApplicantGuid).getName().trim().length() > 0) {
					selectedEvents.put("addedMembersMap[" + addedApplicantGuid + "].name", lceActivity.getAddedMembersMap().get(addedApplicantGuid).getName());
				}
				if (lceActivity.getAddedMembersMap().get(addedApplicantGuid).getEventDate() != null && lceActivity.getAddedMembersMap().get(addedApplicantGuid).getEventDate().trim().length() > 0) {
					selectedEventDates.put("addedMembersMap[" + addedApplicantGuid + "].eventDate", lceActivity.getAddedMembersMap().get(addedApplicantGuid).getEventDate());
				}
				
				SepEventDTO sepEvent = lceActivity.getAddedMembersMap().get(addedApplicantGuid);
				allowFutureDate.put("addedMembersMap[" + addedApplicantGuid + "].allowFutureEvent", sepEvent.getAllowFutureEvent());
			}
		}
		if (lceActivity.getRemovedMembersMap() != null) {
			for (String addedApplicantGuid : lceActivity.getRemovedMembersMap().keySet()) {
				if (lceActivity.getRemovedMembersMap().get(addedApplicantGuid).getName() != null && lceActivity.getRemovedMembersMap().get(addedApplicantGuid).getName().trim().length() > 0) {
					selectedEvents.put("removedMembersMap[" + addedApplicantGuid + "].name", lceActivity.getRemovedMembersMap().get(addedApplicantGuid).getName());
				}
				if (lceActivity.getRemovedMembersMap().get(addedApplicantGuid).getEventDate() != null && lceActivity.getRemovedMembersMap().get(addedApplicantGuid).getEventDate().trim().length() > 0) {
					selectedEventDates.put("removedMembersMap[" + addedApplicantGuid + "].eventDate", lceActivity.getRemovedMembersMap().get(addedApplicantGuid).getEventDate());
				}
				SepEventDTO sepEvent = lceActivity.getRemovedMembersMap().get(addedApplicantGuid);
				allowFutureDate.put("removedMembersMap[" + addedApplicantGuid + "].allowFutureEvent", sepEvent.getAllowFutureEvent());
			}
		}
		if (lceActivity.getHouseholdChangeMembersMap() != null) {
			for (String addedApplicantGuid : lceActivity.getHouseholdChangeMembersMap().keySet()) {
				if (lceActivity.getHouseholdChangeMembersMap().get(addedApplicantGuid).getName() != null && lceActivity.getHouseholdChangeMembersMap().get(addedApplicantGuid).getName().trim().length() > 0) {
					selectedEvents.put("householdChangeReason", lceActivity.getHouseholdChangeMembersMap().get(addedApplicantGuid).getName());
				}
				if (lceActivity.getHouseholdChangeEventDate() != null && lceActivity.getHouseholdChangeEventDate().trim().length() > 0) {
					selectedEventDates.put("householdChangeEventDate", lceActivity.getHouseholdChangeEventDate());
				}
			}
		}
		if (lceActivity.getChangeOfAddressMembersMap() != null) {
			selectedEventDates.put("zipCountyChangeEventDate", lceActivity.getZipCountyChangeEventDate());
		}
		if (lceActivity.getQualifyingEventMap() != null) {
			if (lceActivity.getQualifyEventSelected() != null && lceActivity.getQualifyEventSelected().trim().length() > 0) {
				selectedEvents.put("qualifyEventSelected", lceActivity.getQualifyEventSelected());
			}
			if (lceActivity.getQualifyEventDate() != null && lceActivity.getQualifyEventDate().trim().length() > 0) {
				selectedEventDates.put("qualifyEventDate", lceActivity.getQualifyEventDate());
			}
			if (lceActivity.getQualifyEventAllowFuture() != null && lceActivity.getQualifyEventAllowFuture().trim().length() > 0) {
			allowFutureDate.put("allowFutureEvent", lceActivity.getQualifyEventAllowFuture());
			}
			
		}
	}

	/*
	 * private String toEncrypt(final int idToEncrypt) { return ghixJasyptEncrytorUtil.encryptStringByJasypt("" + idToEncrypt); }
	 */
	private String toDecrypt(String strToDecrypt) {
		return ghixJasyptEncrytorUtil.decryptStringByJasypt(strToDecrypt);
	}

}
