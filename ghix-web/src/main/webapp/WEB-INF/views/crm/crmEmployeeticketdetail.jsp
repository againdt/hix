<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page isELIgnored="false"%>

<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<script type="text/javascript">
$('.my-link').click(function(e) { e.preventDefault(); }); 
</script>
<div class="gutter10">
<div class="row-fluid">	
	
    	<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/crm/employee/searchemployee"/>">Employees</a></li>
			<li>Ticket History</li>
		</ul><!--page-breadcrumb ends-->
                <div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
		<h1>
		<span class="span3">${employee.name}</span> <small class="span9">Contact Name:
			${employee.name} </small>
	</h1>
    </div><!--  end of row-fluid -->
    <div class="row-fluid">
    	<div class="span3" id="sidebar">		
    	<div class="header">
            <h4>About This Employee</h4>
            </div>
             <ul class="nav nav-list">
				
				<li><a href="/hix/crm/crmemployee/details/${employeeId}">Basic Information</a></li>
				<li><a href="/hix/crm/crmemployee/comments/${employeeId}">Comments</a></li>
				<li class="active navmenu"><a href="/hix/crm/crmemployee/ticket/history/${employeeId}">Ticket History</a></li>
				<li class="navmenu"><a href="/hix/crm/crmemployee/securityQuestions/${employeeId}">Security Questions</a></li>
		
			</ul>
            <br />
            <h4 class="margin0"><i class="icon-cog icon-white"></i>&nbsp;Actions</h4>
            <ul class="nav nav-list"> 
            <c:choose>
			<c:when test="${employee.status == 'TERMINATED'}">
			<li class="navmenu disabled"><a href="#" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employee Account</a></li>
			</c:when>
			<c:otherwise>
			<c:if test="${checkDilogEmp == null}"> 
			<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employee Account</a></li>
			</c:if>
			<c:if test="${checkDilogEmp != null}">  
			<li class="navmenu"><a href="/hix/account/user/switchUserRole?switchToModuleName=employee&switchToModuleId=${employeeId}&switchToResourceName=${employee.name}" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employee Account</a></li>
			</c:if>
			<li class="navmenu"><a href="<c:url value="/crm/crmemployee/passwordreset"/>/${employeeId}?sourcePage=ticket/history" role="button" data-toggle="modal"><i class="icon-envelope"></i><spring:message  code='label.sendPasswordResetLink'/></a></li>
			</c:otherwise>
			</c:choose>
             <!--    <li class="navmenu disabled"><a href="#"><i class="icon-envelope"></i>Compose Message</a></li>
                <li class="navmenu disabled"><a href="#"><i class="icon-comment"></i>New Comment</a></li> -->
            </ul>
		</div>
    	
        <div class="span9" id="rightpanel">		
            
                <div class="header">
                    <h4 class="span10">Ticket Summary</h4>
             
                </div>
                <div class="row-fluid">
                	<form method="POST" action="#" name="frmCrmTicketDetailInfo" id="frmCrmTicketDetailInfo" class="form-horizontal">
                		<df:csrfToken/>
                		<table class="table table-border-none verticalThead">							
								<tbody><tr>
									<th class="txt-right span4">Task</th>
									<td><strong>${tkmTicketsObj.subject} </strong></td>
								</tr>
								<tr>
									<th class="txt-right span4">Ticket Type</th>
									<td><strong>${tkmTicketsObj.tkmWorkflows.type}</strong></td>
								</tr>							
								<tr>
									<th class="txt-right">Requestor</th>
									<td><strong>${tkmTicketsObj.role.firstName} ${tkmTicketsObj.role.lastName}</strong></td>
								</tr>
								<tr>
									<th class="txt-right">Plan Name</th>
									<td><strong>${tkmTicketsObj.tkmWorkflows.description}</strong>
									</td>
								</tr>
								<tr>
									<th class="txt-right">Created On</th>
									<td><strong>${tkmTicketsObj.created}</strong></td>
								</tr>
								<tr>
									<th class="txt-right">Description</th>
									<td><strong>${tkmTicketsObj.description}</strong></td>
								</tr>
								
							</tbody>
						</table>
                	
                	

                    <%--     <div class="control-group margin0">
                            <label class="control-label">Task</label>
                            <div class="controls paddingT5"><strong>${tkmTicketsObj.subject} </strong></div>
                        </div><!--control-group ends-->
                        <div class="control-group margin0">
                            <label class="control-label">Ticket Type</label>
                            <div class="controls paddingT5"><strong>${tkmTicketsObj.tkmWorkflows.type}</strong></div>
                        </div><!--control-group ends-->
                        <div class="control-group margin0">
                            <label class="control-label">Requestor</label>
                            <div class="controls paddingT5"><strong>${tkmTicketsObj.role.firstName}</strong></div>
                        </div><!--control-group ends-->
                        <div class="control-group margin0">
                            <label class="control-label">Plan Name</label>
                            <div class="controls paddingT5"><strong>${tkmTicketsObj.tkmWorkflows.description}</strong></div>
                        </div><!--control-group ends-->
                        <div class="control-group margin0">
                            <label class="control-label">Created On</label>
                            <div class="controls paddingT5"><strong>${tkmTicketsObj.created}</strong></div>
                        </div><!--control-group ends-->
                        <div class="control-group margin0">
                            <label class="control-label">Description</label>
                            <div class="controls paddingT5"><strong>${tkmTicketsObj.description}</strong></div>
                        </div><!--control-group ends-->
                        <div class="control-group margin0">
                            <label class="control-label">&nbsp;</label>
                            <div class="controls paddingT5">
	                           	<a href="<c:url value="/admin/ticketmgmt/editticket/${tkmTicketsObj.id}"/>" class="btn btn-primary">Edit<i class="icon-ok"></i></a>
                            </div>
                        </div> --%><!--control-group ends-->
                    </form>
                </div>
        </div>
    </div><!--  end of row-fluid -->
</div>    	<!--  end of gutter10 -->

<!-- Modal -->
<form name="dialogForm" id="dialogForm" action="<c:url value="/crm/employee/home" />" novalidate="novalidate">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left">View Employee Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					Clicking "Employee View" will take to the Employee's portal for ${employee.name}.<br/>
					Through this portal you will be able to take actions on behalf of the employee.<br/>
					Proceed to Employee view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
  <input class="pull-left"  type="checkbox" id="checkEmployeeView" name="checkEmployeeView"  > 
    <div class="pull-left">&nbsp; Don't show this message again.</div>
    <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button class="btn btn-primary" type="submit">Employee View</button>
    <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="employee" />
	<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="${employeeId}" />
	<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${employee.name}" />
    <!-- a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>
</form>
<script type="text/javascript">
function saveComment(ticketId)
{
	var validateUrl = '<c:url value="/admin/ticketmgmt/savecomment">
						<c:param name="${df:csrfTokenParameter()}"> 
						<df:csrfToken plainToken="true" />
						</c:param></c:url>';
	var commentData = $("#comment_text").val();
	$.ajax({
		type: "POST",
		url: validateUrl,
		data: {ticketId : ticketId,
			comment : commentData},
		success: function(response, xhr)
		{
	        if(isInvalidCSRFToken(xhr))                                  
    	      return;

			if(response)
			{
				$('#markCompleteDialog').modal('hide');
				$('#markItComplete a').removeAttr('href')
			}
		}
		});
}

</script>