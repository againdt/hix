package com.getinsured.hix.admin.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.Announcement;
import com.getinsured.hix.model.AnnouncementRole;

public interface AnnouncementService {
	Announcement findById(Integer id);

	Announcement save(Announcement announcement);

	Announcement findByName(String name);

	List<AnnouncementRole> setAnnouncementRoleList(Announcement announcement,
			String[] roles);

	void deleteAnnouncement(Integer id);

	Announcement editAnnouncement(Announcement announcementFormObj);

	Map<String, Object> searchAnnouncement(Map<String, Object> searchCriteria);

	List<Announcement> findAll();

	List<String> viewAnnouncement();
	
	public List<Announcement> getActiveAnnoucements(String moduleName);

}