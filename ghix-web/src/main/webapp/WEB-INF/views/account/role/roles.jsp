<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url value="/account/role/roles/records" var="recordsUrl"/>
<c:url value="/account/role/roles/download" var="downloadUrl"/>
<c:url value="/account/role/roles/download/token" var="downloadTokenUrl"/>
<c:url value="/account/role/roles/download/progress" var="downloadProgressUrl"/>

<link rel="stylesheet" type="text/css" media="screen" href='<c:url value="/resources/css/redmond/jquery-ui-1.8.18.custom.css"/>'/>
<link rel="stylesheet" type="text/css" media="screen" href='<c:url value="/resources/css/ui.jqgrid-4.3.1.css"/>'/>

<script type='text/javascript' src='<c:url value="/resources/js/grid.locale-en-4.3.1.js"/>'></script>
<script type='text/javascript' src='<c:url value="/resources/js/jquery.jqGrid.min.4.3.1.js"/>'></script>

<script type='text/javascript'>
	$(function() {
		$("#grid").jqGrid({
		   	url:'${recordsUrl}',
			datatype: 'json',
			mtype: 'GET',
		   	colNames:['Id', 'Name', 'Description', 'Landing Page', 'Created', 'Updated'],
		   	colModel:[
		   		{name:'id',index:'id', width:55, hidden:true},
		   		{name:'name',index:'name', width:100},
		   		{name:'description',index:'description', width:100},
		   		{name:'landingPage',index:'landingPage', width:100},
		   		{name:'created',index:'created', width:100},
		   		{name:'updated',index:'updated', width:100},
		   	],
		   	postData: {},
		   	rowList:[10,20,40,60],
		   	height: 240,
		   	autowidth: true,
			rownumbers: true,
		   	pager: '#pager',
		    viewrecords: true,
		    caption:"Records",
		    emptyrecords: "Role records",
		    loadonce: true,
		    sortable: true,
		    loadComplete: function() {},
		    jsonReader : {
		        root: "rows",
		        page: "page",
		        total: "total",
		        records: "records",
		        repeatitems: false,
		        cell: "cell",
		        id: "id"
		    }
		});

		$("#grid").jqGrid('navGrid','#pager',
				{edit:false, add:false, del:false, search:true},
				{}, {}, {}, {}
		);
		
		$("#grid").navButtonAdd('#pager',
				{ 	caption:"Pdf", 
					buttonicon:"ui-icon-arrowreturn-1-s", 
					onClickButton: downloadPdf,
					position: "last", 
					title:"", 
					cursor: "pointer"
				} 
			);
		
		$("#grid").navButtonAdd('#pager',
				{ 	caption:"Excel", 
					buttonicon:"ui-icon-arrowreturn-1-s", 
					onClickButton: downloadXls,
					position: "last", 
					title:"", 
					cursor: "pointer"
				} 
			);
		
	});

	function downloadXls() {download('xls');}
	
	function downloadPdf() {download('pdf');}
	
	function download(type) {
		// Retrieve download token
		// When token is received, proceed with download
		$.get('${downloadTokenUrl}', function(response) {
			// Store token
			var token = response.message[0];
			
			// Show progress dialog
			$('#msgbox').text('Processing download...');
			$('#msgbox').dialog( 
					{	title: 'Download',
						modal: true,
						buttons: {"Close": function()  {
							$(this).dialog("close");} 
						}
					});
			
			// Start download
			window.location = '${downloadUrl}'+'?token='+token+'&type='+type;

			// Check periodically if download has started
			var frequency = 1000;
			var timer = setInterval(function() {
				$.get('${downloadProgressUrl}', {token: token}, 
						function(response) {
							// If token is not returned, download has started
							// Close progress dialog if started
							if (response.message[0] != token) {
								$('#msgbox').dialog('close');
								clearInterval(timer);
							}
					});
			}, frequency);
			
		});
	}
</script>
  
<div id='jqgrid'>
	<table id='grid'></table>
	<div id='pager'></div>
</div>
	
<div id='msgbox' title='' style='display:none'></div>