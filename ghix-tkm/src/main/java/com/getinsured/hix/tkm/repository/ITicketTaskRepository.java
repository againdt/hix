package com.getinsured.hix.tkm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.TkmTicketTasks;

public interface ITicketTaskRepository extends RevisionRepository<TkmTicketTasks, Integer, Integer>, JpaRepository<TkmTicketTasks, Integer>
{
	
	@Query("SELECT t FROM TkmTicketTasks t WHERE t.ticket.id= :id order by t.created ASC")
	List<TkmTicketTasks> getTkmTicketTaskList(@Param("id") int ticketId);
	
	@Query("SELECT t FROM TkmTicketTasks t WHERE t.taskId= :id ORDER BY t.updated ASC")
	List<TkmTicketTasks> getTkmTicketTasksHistory(@Param("id") int taskId);
	
	@Query("SELECT t FROM TkmTicketTasks t WHERE t.processInstanceId= :processInstanceId")
	List<TkmTicketTasks> findProcessInstanceTaskList(@Param("processInstanceId") String processInstanceId);
	
	@Query("SELECT t.ticket.id FROM TkmTicketTasks t WHERE t.taskId= :taskId")
	Integer getTicketIdForTask(@Param("taskId") Integer taskId);

	@Query("SELECT t FROM TkmTicketTasks t WHERE t.activitiTaskId= :activitiTaskId")
	TkmTicketTasks getTaskForActivitiTaskTaskId(@Param("activitiTaskId")String activitiTaskId);
	 
	@Query("SELECT count(*) FROM TkmTicketTasks t")
	long getTicketTaskCount();
	
	@Query("SELECT t FROM TkmTicketTasks t WHERE (LOWER(t.assignee.firstName ||' '|| t.assignee.lastName) LIKE LOWER(:assigneeName) || '%')" +
			"AND t.ticket.id in (SELECT DISTINCT(t1.ticket.id) FROM TkmTicketTasks t1)")
	List<TkmTicketTasks> getTkmticketByAssignee(@Param("assigneeName") String assigneeName);
	
	@Query("SELECT t.ticket.id FROM TkmTicketTasks t where t.queue.id in(:id) AND t.status = :status")
	List<Integer>getTkmTicketIdByQueueAndTask(@Param("id") List<Integer> queueId,@Param("status") String status);

	TkmTicketTasks findByProcessInstanceIdAndActivitiTaskId(
			String processInstanceId, String activitiTaskId);

	@Query("SELECT tt.assignee.id, t.status, tt.taskName, tt.status " +
			"FROM TkmTicketTasks tt, TkmTickets t " +
			"WHERE tt.ticket.id= :id  AND t.id = tt.ticket.id AND tt.id  = (select task.id from TkmTicketTasks task where task.created = (select max(t1.created) from TkmTicketTasks t1 where t1.ticket.id = :id)) ")
	List<Object> getDetailsForQuickActionBar(@Param("id") Integer ticketId);
	
	@Query("SELECT t.assignee.id,concat(t.assignee.firstName,' ',t.assignee.lastName) from TkmTicketTasks t WHERE (LOWER(t.assignee.firstName ||' '|| t.assignee.lastName) LIKE LOWER(:assigneeName) || '%')" )
	List<Object[]> getTkmUserInfoByAssignee(@Param("assigneeName") String assigneeName);
}
