import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactLoading from 'react-loading';
import { FormattedMessage } from 'react-intl';
import moment from 'moment';
import ReactModal from 'react-modal';

import AppStatus from '../components/AppStatus';
import FurtherAction from '../components/FurtherAction';
import HealthPlan from '../components/HealthPlan';
import HouseHold from '../components/HouseHold';
import WarningMessages from '../components/WarningMsgs/WarningMessages';
import {
    goToSSAPStart,
    stateSpecificAHBXRedirect,
    applyForCostSavings,
    defaultReportAChange
} from '../utils/stateSpecificUtils';
import useDashboardInfo from '../hooks/useDashboardInfo';
import { switchPageContainer, fetchMultiHousehold } from '../actions/actionCreators';
import {
    getFurtherActionData,
    isEnrolledInDental,
    isEnrolledInHealth
} from '../stateMachineRules/processDashboardModel';
import { PAGE_CONTAINERS } from '../constants/constants';
import { APP_STATUSES, APP_STATUS_MESSAGES, APP_STATUS_LINKS } from '../constants/dashboardModelTypes';
import './DashboardExt.css';

ReactModal.setAppElement('#myReactID');

//TODO: Refactor state specific logic

function DashboardExt({ stateCode, defaultRoleName, pageContainerName, actions, isMultiHousehold }) {
    let [currentTabIndex, setCurrentTabIndex] = useState(null);
    let [enrollCaseNumber, setEnrollCaseNumber] = useState(null);
    let [showModal, setShowModal] = useState(false);
    let [showOldSSAPModal, setShowOldSSAPModal] = useState(false);
    let [reportAChangeModal, setReportAChangeModal] = useState(false);
    let [closedStateAlertModal, setClosedStateAlertModal] = useState(false);
    let [QHPInelibleModal, setQHPInelibleModal] = useState(false);
    let [oldSSAPParams, setOldSSAPParams] = useState([]);
    let { yearlyData, dashboardModel } = useDashboardInfo({ userId: '1' });
    let [changePlansSelected, setChangePlansSelected] = useState(false);
    let [ssapApplicationId, setSsapApplicationId] = useState(null);

  
    window.dataLayer.push({
      'pageCategory': 'Dashboard',
    });

    useEffect(() => {
        localStorage.setItem('ssapApplicationId', '');
        if (dashboardModel && dashboardModel.configuration) {
            localStorage.setItem('sepEndDate', dashboardModel.configuration.sepEndDate);
            localStorage.setItem('serverDate', dashboardModel.configuration.serverDate);
        }
        if (yearlyData.length > 0) {
            let defaultTabIndex = yearlyData.length - 1;
            let defaultEnrollCaseNumber = yearlyData[defaultTabIndex] && yearlyData[defaultTabIndex].caseNumber;
            localStorage.setItem('tabs', JSON.stringify(dashboardModel.tabs));

            // default currentTabIndex
            if (!currentTabIndex) {
                setCurrentTabIndex(defaultTabIndex);
            }

            // default enrollCaseNumber
            if (!enrollCaseNumber) {
                localStorage.setItem('enrollCaseNumber', defaultEnrollCaseNumber);
                setEnrollmentCaseNumber(defaultEnrollCaseNumber);
            }
        }
    }, [yearlyData]);

    useEffect(() => {
        actions.fetchMultiHousehold('indportal/multiHousehold');
    }, [pageContainerName]);

    /**
     * Sets enrollCaseNumber to localStorage and sets attribute for dom element
     * @param value - enrollment case number
     */
    const setEnrollmentCaseNumber = (value) => {
        const ENROLL_CASE_NUMBER = 'enrollCaseNumber';
        document.getElementById(ENROLL_CASE_NUMBER).setAttribute("value", value);
        localStorage.setItem(ENROLL_CASE_NUMBER, value);
    };

    /**
     * Returns true only if ALL enrollments creation date is within this month.
     * @param enrollments
     * @param serverDate
     */
    const isAllEnrollmentCreationDateWithinThisMonth = (enrollments, serverDate) => {
        // We should ignore term enrollments whose coverage end date has passed.
        const filteredEnrollments = enrollments.filter(enrollment => {
            let { enrollmentStatus, coverageEndDate } = enrollment;
            return enrollmentStatus !== 'TERM';
        });

        return filteredEnrollments.every((enrollment) => {
            let { enrollmentCreationDate } = enrollment;
            return (moment(enrollmentCreationDate, "MM-DD-YYYY").isSame(moment(serverDate, "MM-DD-YYYY"), 'month'));
        })
    };

    /**
     * Retrieve application status in an interval
     * @param ssapApplicationId
     */
    const storeSsapApplicationId = (ssapApplicationId) => {
        if (ssapApplicationId !== null && ssapApplicationId !== undefined) {
            setSsapApplicationId(ssapApplicationId);
            localStorage.setItem('ssapApplicationId', ssapApplicationId);
        }
    };

    /**
     * Handles 'Report a Change' link across all states.
     * @param data - getDashboardModel for the selected tab (year)
     */
    const reportAChange = (data, ssapFlowVersion) => {
        const { isFinancial, caseNumber, applicationStatus, applicationYear, isInsideOEWindow } = data;
        switch (stateCode) {
            case('CA'): {
                stateSpecificAHBXRedirect();
                break;
            }

            case('MN'): {
                window.open('https://www.mnsure.org/current-customers/manage-account/report-change/private.jsp', "_blank");
                break;
            }

            case('ID'): {
                if (isFinancial) {
                    setReportAChangeModal(true);
                } else {
                    defaultReportAChange(caseNumber, applicationYear, isInsideOEWindow, ssapFlowVersion, applicationStatus);
                }

                break;
            }

            default: {
                defaultReportAChange(caseNumber, applicationYear, isInsideOEWindow, ssapFlowVersion, applicationStatus);
            }
        }
    };

    /**
     * Set active class for a selected Dashboard tab.
     * @param index - selected tab
     * @returns {string}
     */
    const getActiveClass = (index) => {
        return (currentTabIndex === index) ? 'active' : '';
    };

    /**
     * Show/hide tab section.
     * @param index - selected tab
     * @returns {string}
     */
    const getShowClass = (index) => {
        return (currentTabIndex === index) ? 'show' : 'hide';
    };

    /**.
     * CTA modal. Prevents users from redirecting to custom grouping page
     */
    const handleCloseModal = () => {
        setShowModal(false);
    };

    /**.
     * CTA modal. Prevents users from redirecting to custom grouping page
     */
    const handleQHPInelibleModalCloseModal = () => {
        setQHPInelibleModal(false);
    };

    /**
     * YHI only old SSAP modal for financial/nonfinancial flow.
     */
    const handleOldSSAPCloseModal = () => {
        setShowOldSSAPModal(false);
    };

    /**
     * YHI only modal for 'Report a Change' link.
     */
    const handleReportAChangeCloseModal = () => {
        setReportAChangeModal(false);
    };

    /**
     * Closed app state alert modal.
     */
    const handleAlertClosedAppStateCloseModal = () => {
        setClosedStateAlertModal(false);
    };

    /**
     * Updates state and enrollment case number once a new tab was selected.
     * @param curTabIndex
     * @param caseNumber
     * @param coverageYear
     */
    const selectTabHandler = (curTabIndex, caseNumber, coverageYear) => {
        $('#coverageYear').val(coverageYear);
        setCurrentTabIndex(curTabIndex);
        setEnrollmentCaseNumber(caseNumber);
    };

    /**
     * check if application state is CLOSED.
     * @param ssapApplicationId
     */
    const checkAppState = async (ssapApplicationId) => {
        try {
            let response = await fetch(`/hix/indportal/getapplicationstatusbyid/${ssapApplicationId}`);
            const data = await response.json();
            if (data.applicationStatus === APP_STATUSES.CL) {
                setClosedStateAlertModal(true);
                return true;
            }
            return false;
        } catch (err) {
            console.error(`In DashboardExt: checkAppState(): error in calling API = `);
            console.info(err);
            return false;
        }
    }

    /**
     * Adds dynamic values for paragraph text
     * @param model
     * @param config
     * @param messageId
     * @returns {*}
     */
    const processMessages = (model, config, messageId) => {
        const currentDate = config.serverDate ? moment(config.serverDate, 'MM-DD-YYYY') : moment();
        const hasDental = isEnrolledInDental(model.enrollments, config);
        const hasHealth = isEnrolledInHealth(model.enrollments, config);

        return <FormattedMessage id={messageId.props ? messageId.props.id : messageId}
                                 values={{
                                     OEPStartDate: config.oepStartDate,
                                     OEPEndDate: config.oepEndDate,
                                     ishealth: hasHealth ?
                                         <FormattedMessage id="further_action.coverage_status.health"/> : "",
                                     isdental: hasHealth && hasDental ?
                                         <FormattedMessage id="further_action.coverage_status.and_dental"/>
                                         : (hasDental ?
                                             <FormattedMessage id="further_action.coverage_status.dental"/> : ""),
                                     previousYear: currentDate.year() - 1,
                                     upcomingyear: currentDate.year() + 1,
                                     QLElink: <FormattedMessage
                                         id="further_action.coverage_status.qualifying_life_event"/>,
                                     currentyear: currentDate.year(),
                                     isFinancial: model.isFinancial ?
                                         <FormattedMessage id="further_action.coverage_status.and_financial"/> : ""
                                 }}/>
    };

    /**
     * Runs state machine to get an object {btnText, paragraphText, link} and processes paragraphText to get dynamic values.
     * @param model
     * @param config
     * @returns {{btnText, processedParagraphText: *, link}}
     */
    const getFurtherActionProcessedData = (model, config) => {
        model = { ...model, defaultRoleName };

        const smFurtherActionsRes = getFurtherActionData(model, config, stateCode);
        const processedParagraphText = processMessages(model, config, smFurtherActionsRes.paragraphText);
        return {
            ...smFurtherActionsRes,
            processedParagraphText
        }
    };

    /**
     * Checks if none of the members of a HH are eligible for QHP.
     * @param applicants
     * @returns {*}
     */
    const checkQHPInelibleMembers = applicants => {
        return applicants && applicants.length > 0 && applicants.every(applicant => {
            return !applicant.exchangeEligible || !applicant.seeksQHP;
        });
    };

    /**
     * CTA button logic for FurtherAction component. Process link destination.
     * @param tab
     * @param config
     * @param link
     * @returns {boolean}
     */
    const getFurtherActions = (tab, config, link) => {
        const tobbacoEnabled = config.tobbacoEnabled ? config.tobbacoEnabled : 'ON';
        const hardshipEnabled = config.hardshipEnabled ? config.hardshipEnabled : 'ON';
        const ssapFlowVersion = config.ssapFlowVersion ? config.ssapFlowVersion : '1.0';
        const { caseNumber, applicationStatus, applicationYear, applicationType, ssapApplicationId, encyptedCaseNumber, enrollments, isInsideOEWindow } = tab;
        const { serverDate } = config;

        window.caseNumber = tab.caseNumber;
        window.coverageYear = tab.applicationYear;

        switch (link) {
            case "START_NEW_APPLICATION":
                switch (stateCode) {
                    case 'CA': {
                        stateSpecificAHBXRedirect();
                        break;
                    }
                    case ('ID'): {
                        setOldSSAPParams([caseNumber, applicationYear, isInsideOEWindow, ssapFlowVersion, applicationStatus]);
                        setShowOldSSAPModal(true);
                        break;
                    }
                    default: {
                        goToSSAPStart(caseNumber, applicationYear, isInsideOEWindow, ssapFlowVersion, applicationStatus);
                    }
                }

                break;

            case "RESUME_APPLICATION":
                switch (stateCode) {
                    case 'CA': {
                        stateSpecificAHBXRedirect();
                        break;
                    }
                    case 'MN': {
                        window.open('https://www.mnsure.org/new-customers/apply/index.jsp', "_blank");
                        break;
                    }
                    default: {
                        goToSSAPStart(caseNumber, applicationYear, isInsideOEWindow, ssapFlowVersion, applicationStatus);
                    }
                }

                break;

            case "GROUPING_SCREEN":
                storeSsapApplicationId(ssapApplicationId);
                checkAppState(ssapApplicationId).then((isShowAlert) => {
                    if (!isShowAlert) {
                        setChangePlansSelected(true);
                        if(checkQHPInelibleMembers(tab.applicants)) {
                            setQHPInelibleModal(true);
                        } else {
                            if (tobbacoEnabled === 'ON' || hardshipEnabled === 'ON') {
                                window.location = "./indportal#/additionalinfo";
                            } else {
                                window.location = "./indportal#/customGrouping";
                            }
                        }

                    }
                });
                break;

            case 'GROUPING_SCREEN_IF_ALL_ENROLLMENTS_NOT_WITHIN_THIS_MONTH':
                storeSsapApplicationId(ssapApplicationId);
                checkAppState(ssapApplicationId).then((isShowAlert) => {
                    if (!isShowAlert) {
                        setChangePlansSelected(true);
                        if (isAllEnrollmentCreationDateWithinThisMonth(enrollments, serverDate)) {
                            setShowModal(true);
                            return false;
                        } else {
                            if(checkQHPInelibleMembers(tab.applicants)) {
                                setQHPInelibleModal(true);
                            } else {
                                if (tobbacoEnabled === 'ON' || hardshipEnabled === 'ON') {
                                    window.location = "./indportal#/additionalinfo";
                                } else {
                                    window.location = "./indportal#/customGrouping";
                                }
                            }
                        }
                    }
                });
                break;
            case "REPORT_CHANGE":
                reportAChange(tab, ssapFlowVersion);
                break;

            case "REPORT_LIFE_EVENT_SHOP":
                reportAChange(tab, ssapFlowVersion);
                break;

            case "UPLOAD_DOCUMENTS":
                window.location = `/hix/ssap/verificationResultDashboard?caseNumber=${caseNumber}`;
                break;

            case "CONTINUE":
                if (stateCode === 'MN') {
                    window.open('https://www.mnsure.org/documents/index.jsp', '_blank');
                }
                break;

            case "CONTACT_US":
                if (stateCode === 'MN') {
                    window.open('https://www.mnsure.org/help/contact-us/index.jsp', '_blank');
                }
                break;

            case "CONFIRM_EVENT":
                storeSsapApplicationId(ssapApplicationId);
                checkAppState(ssapApplicationId).then((isShowAlert) => {
                    if (!isShowAlert) {
                        setChangePlansSelected(true);
                        window.location = `/hix/referral/lce/events/applicants/${encyptedCaseNumber}`; 
                    }
                });
                break;

            case "VERIFICATION":
                actions.switchPageContainer(PAGE_CONTAINERS.VERIFICATION);
            default:
                break;

        }
    };

    /**
     * Depending on the ApplicationStatus handles what link and text to show.
     * @returns {boolean}
     * @param model
     */
    const getAppStatusData = (model) => {
        const { applicationStatus = 'NO_STATUS', applicants, applicationYear } = model;
        const appStatusData = { applicants, applicationYear };

        switch (applicationStatus) {
            case 'NO_STATUS':
            case 'SU':
            case 'CC':
            case 'CL':
            case 'SG': {
                appStatusData.status = APP_STATUS_MESSAGES.NOT_STARTED;
                appStatusData.link = APP_STATUS_LINKS.START_APP;
                break;
            }
            case 'OP': {
                appStatusData.status = APP_STATUS_MESSAGES.IN_PROGRESS;
                appStatusData.link = APP_STATUS_LINKS.CONTINUE_APP;
                break;
            }
            case 'ER':
            case 'QU': {
                appStatusData.status = APP_STATUS_MESSAGES.COMPLETE;
                appStatusData.link = APP_STATUS_LINKS.VIEW_APP;
                break;
            }
            case 'PN': {
                appStatusData.status = APP_STATUS_MESSAGES.COMPLETE;
                appStatusData.link = APP_STATUS_LINKS.VIEW_APP;
                break;
            }
            case 'EN': {
                appStatusData.status = APP_STATUS_MESSAGES.COMPLETE;
                appStatusData.link = APP_STATUS_LINKS.VIEW_APP;
                break;
            }
            case 'UC': {
                appStatusData.status = APP_STATUS_MESSAGES.NOT_STARTED;
                appStatusData.link = APP_STATUS_LINKS.START_APP;
                break;
            }

            default: {
                appStatusData.status = APP_STATUS_MESSAGES.NOT_STARTED;
                appStatusData.link = APP_STATUS_LINKS.START_APP;
                break;
            }
        }

        return appStatusData;

    };

    const getApplicationResults = (model, configuration, link) => {
        const { caseNumber, applicationStatus, applicationYear, applicationType, isInsideOEWindow } = model;
        const { ssapFlowVersion } = configuration;
        window.caseNumber = caseNumber;

        switch (link) {
            case APP_STATUS_LINKS.START_APP:
                switch (stateCode) {
                    case ('CA'): {
                        stateSpecificAHBXRedirect();
                        break;
                    }
                    case ('MN'): {
                        window.open('https://www.mnsure.org/help/contact-us/index.jsp', "_blank");
                        break;
                    }
                    case ('ID'): {
                        setOldSSAPParams([caseNumber, applicationYear, isInsideOEWindow, ssapFlowVersion, applicationStatus]);
                        setShowOldSSAPModal(true);
                        break;
                    }
                    default: {
                        goToSSAPStart(null, applicationYear, isInsideOEWindow, ssapFlowVersion, applicationStatus);
                    }
                }
                break;

            case APP_STATUS_LINKS.CONTINUE_APP:
                switch (stateCode) {
                    case ('CA'): {
                        stateSpecificAHBXRedirect();
                        break;
                    }
                    case ('MN'): {
                        window.open('https://www.mnsure.org/help/contact-us/index.jsp', "_blank");
                        break;
                    }
                    default: {
                        goToSSAPStart(caseNumber, applicationYear, isInsideOEWindow, ssapFlowVersion, applicationStatus);
                    }
                }
                break;

            case APP_STATUS_LINKS.VIEW_APP:
                window.location = `/hix/indportal#/applications`;
                break;

            default:
                break;
        }
    };


    if (!yearlyData.length) {
        const loaderInlineStyles = {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100vh'
        };
        return (
            <div>
                <div style={loaderInlineStyles}>
                    <ReactLoading type="spin" color="#2c94a3" height="170px" width="170px"/>
                </div>
            </div>
        )
    } else {
        let tabs = [];
        let tabContents = [];
        yearlyData.forEach((item, index) => {
            tabs.push(
                <li key={item.applicationStatusInfo.applicationYear}>
                    <a href="javascript:void(0)"  onClick={() => {
                        selectTabHandler(index, item.caseNumber, item.applicationStatusInfo.applicationYear)
                    }}
                       id={`tab-${index}`}
                       className={getActiveClass(index)}
                    >
                        {item.applicationStatusInfo.applicationYear}
                    </a>
                </li>);

            tabContents.push(
                <div
                    id={`section-tab-${index}`}
                    key={`section-tab-${index}`}
                    className={getShowClass(index)}
                >
                    <div className="ds-u-fill--white"
                         id={item.applicationStatusInfo.applicationYear + "_tab_content"}
                    >

                        <WarningMessages
                            tabData={dashboardModel.tabs[index]}
                            stateCode={stateCode}
                            config={dashboardModel.configuration}
                            prevTabYear={dashboardModel.tabs[index-1] ? dashboardModel.tabs[index-1].applicationYear : null}/>

                        <FurtherAction
                            data={getFurtherActionProcessedData(dashboardModel.tabs[index], dashboardModel.configuration)}
                            onClickHandler={(el) => {
                                getFurtherActions(dashboardModel.tabs[index], dashboardModel.configuration, el.data.link)
                            }}
                        />

                        <div className="ds-u-margin--2">
                            <div className="ds-h5"
                                 id={item.applicationStatusInfo.applicationYear + "_dashboard_overview"}>
                                <FormattedMessage id="dashboard.overview"/>
                            </div>

                            <AppStatus
                                data={getAppStatusData(dashboardModel.tabs[index])}
                                onGetApplicationResults={(el) => {
                                    getApplicationResults(dashboardModel.tabs[index], dashboardModel.configuration, el.link)
                                }}
                            />
                            <hr/>

                            <HouseHold
                                data={item}
                                onReportChange={() => {
                                    reportAChange(dashboardModel.tabs[index], dashboardModel.configuration.ssapFlowVersion)
                                }}
                                isMultiHousehold={isMultiHousehold}/>

                            <hr/>
                            <div id="auto-health-plans">
                                <HealthPlan
                                    stateCode={stateCode}
                                    key={`id-${index}`}
                                    type={item.healthPlans.type}
                                    planType={item.healthPlans.planType}
                                    singleColData={item.healthPlans.singleColData}
                                    multiColData={item.healthPlans.multiColData}
                                    isInsideOEWindow={dashboardModel.tabs[index].isInsideOEWindow}
                                    applicationStatus={item.applicationStatusInfo.applicationStatus}
                                    applicationYear={item.applicationStatusInfo.applicationYear}
                                    ssapFlowVersion={item.ssapFlowVersion ? item.ssapFlowVersion : '1.0'}
                                    config={dashboardModel.configuration}
                                />

                            </div>
                            <hr/>
                            <div id="auto-dental-plans">
                                <HealthPlan
                                    stateCode={stateCode}
                                    key={`id-${index}`}
                                    type={item.dentalPlans.type}
                                    planType={item.dentalPlans.planType}
                                    singleColData={item.dentalPlans.singleColData}
                                    multiColData={item.dentalPlans.multiColData}
                                    isInsideOEWindow={dashboardModel.tabs[index].isInsideOEWindow}
                                    applicationStatus={item.applicationStatusInfo.applicationStatus}
                                    applicationYear={item.applicationStatusInfo.applicationYear}
                                    ssapFlowVersion={item.ssapFlowVersion}
                                    config={dashboardModel.configuration}
                                />
                            </div>
                        </div>
                    </div>
                </div>);
        });
        return (
            <div>
                <div className="wrapper">
                    <ul className="tabs dashboard_tabs clearfix" data-tabgroup="first-tab-group">
                        {tabs}
                    </ul>
                    <section id="first-tab-group" className="tabgroup">
                        {tabContents}
                        <ReactModal isOpen={showModal}
                                    onRequestClose={handleCloseModal}
                                    className="react-modal modal"
                                    overlayClassName="react-overlay">
                            <div className="modal-body react-modal-body">
                                <p>
                                    <FormattedMessage id="further_action.modal.warning_msg"/>
                                </p>
                            </div>
                            <div className="modal-footer react-modal-footer">
                                <button onClick={handleCloseModal}
                                        className="ds-c-button ds-c-button--primary ds-u-margin--1">
                                    <FormattedMessage id="dashboard.ok"/>
                                </button>
                            </div>
                        </ReactModal>

                        {/*Modal for YHI only - old SSAP*/}
                        <ReactModal isOpen={showOldSSAPModal}
                                    onRequestClose={handleOldSSAPCloseModal}
                                    className="react-modal modal old-ssap-modal"
                                    overlayClassName="react-overlay">
                            <div className="modal-header">
                                <button type="button"
                                        className="close"
                                        data-dismiss="modal"
                                        aria-hidden="true"
                                        onClick={handleOldSSAPCloseModal}
                                >
                                    &times;
                                </button>
                                <h3>
                                    <FormattedMessage id="application_status.old_ssap_modal_header"/>
                                </h3>
                            </div>
                            <div className="modal-body react-modal-body">
                                <p>
                                    <FormattedMessage id="application_status.old_ssap_modal_paragraph"/>
                                </p>
                            </div>
                            <div className="modal-footer">
                                <button className="btn pull-left" onClick={handleOldSSAPCloseModal}>
                                    <FormattedMessage id="application_status.old_ssap_modal_cancel"/>
                                </button>

                                <button className="btn btn-primary" onClick={applyForCostSavings}>
                                    <FormattedMessage id="application_status.old_ssap_modal_btn1"/>
                                </button>
                                <button className="btn btn-primary"
                                        onClick={() => goToSSAPStart.apply(null, oldSSAPParams)}>
                                    <FormattedMessage id="application_status.old_ssap_modal_btn2"/>
                                </button>

                            </div>
                        </ReactModal>

                        {/*Modal for YHI only - Report a Change*/}
                        <ReactModal isOpen={reportAChangeModal}
                                    onRequestClose={handleReportAChangeCloseModal}
                                    className="react-modal modal"
                                    overlayClassName="react-overlay">
                            <div className="modal-header">
                                <button type="button"
                                        className="close"
                                        data-dismiss="modal"
                                        aria-hidden="true"
                                        onClick={handleReportAChangeCloseModal}
                                >
                                    &times;
                                </button>
                                <h3>
                                    <FormattedMessage id="household_section.report_a_change.header"/>
                                </h3>
                            </div>
                            <div className="modal-body react-modal-body">
                                <p>
                                    <FormattedMessage id="household_section.report_a_change.paragraph1"/>
                                </p>
                                <div className="alert alert-info">
                                    <p>
                                        <FormattedMessage id="household_section.report_a_change.paragraph2"/>
                                    </p>
                                    <ul>
                                        <li>
                                            <FormattedMessage id="household_section.report_a_change.e-forms"
                                                              values={{
                                                                  changeReportForm: <a
                                                                      href="http://healthandwelfare.idaho.gov"
                                                                      target="_blank">
                                                                      <FormattedMessage
                                                                          id="household_section.change_report_form"/>
                                                                  </a>
                                                              }}
                                            />
                                        </li>
                                        <li>
                                            <FormattedMessage id="household_section.report_a_change.email"
                                                              values={{
                                                                  idahoEmail: <a target="_blank"
                                                                                 href="mailto:mybenefits@dhw.idaho.gov">mybenefits@dhw.idaho.gov</a>
                                                              }}
                                            />
                                        </li>
                                        <li>
                                            <FormattedMessage id="household_section.report_a_change.phone"/>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <div className="modal-footer">
                                <button className="btn pull-left" onClick={handleReportAChangeCloseModal}>
                                    <FormattedMessage id="dashboard.close"/>
                                </button>

                                <button className="btn btn-primary" onClick={handleReportAChangeCloseModal}>
                                    <FormattedMessage id="dashboard.ok"/>
                                </button>

                            </div>
                        </ReactModal>

                        {/* Modal alert when application is closed - Confirm Event, Shop for Plans */}
                        <ReactModal isOpen={closedStateAlertModal}
                                    onRequestClose={handleAlertClosedAppStateCloseModal}
                                    className="react-modal modal"
                                    overlayClassName="react-overlay">
                            <div className="modal-header">
                                <button type="button"
                                        className="close"
                                        data-dismiss="modal"
                                        aria-hidden="true"
                                        onClick={handleAlertClosedAppStateCloseModal}
                                >
                                    &times;
                                </button>
                                <h3>
                                    <FormattedMessage id="appstatus.closed.header"/>
                                </h3>
                            </div>
                            <div className="modal-body react-modal-body">
                                <p>
                                    <FormattedMessage id="appstatus.closed.warning"
                                                    values={{
                                                        dashboardLink: <a
                                                            href="/hix/indportal">
                                                            <FormattedMessage
                                                                id="dashboard.back"/>
                                                        </a>
                                                    }}
                                    />
                                </p>
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-primary" onClick={handleAlertClosedAppStateCloseModal}>
                                    <FormattedMessage id="dashboard.ok"/>
                                </button>

                            </div>
                        </ReactModal>

                        {/* Modal alert when none of the members of a HH are eligible for QHP */}
                        <ReactModal isOpen={QHPInelibleModal}
                                    onRequestClose={handleQHPInelibleModalCloseModal}
                                    className="react-modal modal"
                                    overlayClassName="react-overlay">
                            <div className="modal-header">
                                <button type="button"
                                        className="close"
                                        data-dismiss="modal"
                                        aria-hidden="true"
                                        onClick={handleQHPInelibleModalCloseModal}
                                >
                                    &times;
                                </button>
                            </div>
                            <div className="modal-body react-modal-body">
                                <p>
                                    <FormattedMessage id="further_action.modal.QHP_ineligible_msg"/>
                                </p>
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-primary" onClick={handleQHPInelibleModalCloseModal}>
                                    <FormattedMessage id="dashboard.ok"/>
                                </button>

                            </div>
                        </ReactModal>
                    </section>
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({ app, pageContainer, isMultiHousehold }) => {
    const { stateCode, defaultRoleName } = app;
    const { pageContainerName } = pageContainer;
    return {
        stateCode,
        defaultRoleName,
        pageContainerName,
        isMultiHousehold
    }
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        switchPageContainer,
        fetchMultiHousehold
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardExt);
