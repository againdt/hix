var houseHoldNumberValue;
var haveDataFlag = false;


var disabilityFlag = true;
var i_cover_adoptionFlag = true;
var i_nativeAmericonFlag = true;
var i_cover_fosterFlag = true;
var i_deceasedFlag = true;
var isPregnantFlag = true;
var tribeCode = "";

function fillValuesToFields(currentPage1)
{	
	if(currentPage1 == "appscr52")
	{
		setValuesToappscr53Mongodb();
	}
	else if(currentPage1 == "appscr53")
	{
		setValuesToappscr54Mongodb();
	}
	else if(currentPage1 == "appscr54")
	{
		setData55();
	}
	else if(currentPage1 == "appscr55")
	{
		setValuesToappscr57Mongodb();
	}
	else if(currentPage1 == "appscr57")
	{
		Appscr58DetailJson();
	}

	else if(currentPage1 == "appscr59A")
	{

		//if(haveData && HouseHoldRepeat<=houseHoldNumberValue){
			updateAppscr60FromJSON(HouseHoldRepeat);

		//}
	}
	else if(currentPage1 == "appscr59B")
	{

		//if(haveData && HouseHoldRepeat<=houseHoldNumberValue){
			updateAppscr60FromMongodb(HouseHoldRepeat);

		//}
	}

	else if(currentPage1 == "appscr60")
	{
		haveDataFlag = true;

		if(useJSON == true){
			appscr67EditController(nonFinancialHouseHoldDone);
		}else {
			if(nonFinancialHouseHoldDone<=houseHoldNumberValue)
				appscr67EditController(nonFinancialHouseHoldDone);
		}

		haveDataFlag = false;
	}
	else if(currentPage1 == "appscr61"){
		//var rowId =($('#houseHoldTRNo').val()).substring(9);
		var rowId = getCurrentApplicantID();
		  appscr61FromJSON(rowId);
	}
	else if(currentPage1 == "appscr62Part1"){
		if(useJSON == true){
		  appscr63FromJSON(nonFinancialHouseHoldDone);
		}
	}
	else if(currentPage1 == "appscr64")
	{
		if(haveData == true)
			appscr65FromMongodb();
	}
	else if(currentPage1 == "appscr65")
	{		
		//setAddressDetailsNonfinancial();
		appscr66FromMongodbJson();		
		//appscr66NoneOfThese();
	}
	else if(currentPage1 == "appscr66")
	{		
		populateRelationship();
	}
	
	/*else if(currentPage1 == "appscrBloodRel")
	{		
		showHouseHoldContactSummary();
	}*/
	
	else if(currentPage1 == "appscr67")
	{
		//appscrSNAPfromMongodb();
	}
	else if(currentPage1 == "appscrSNAP")
	{
		appscr68FromMongodb();
	}
	else if(currentPage1 == "appscr68")
	{
		haveDataFlag = true;
		if(useJSON == true){
			appscr74EditController(financialHouseHoldDone);
		}else {
			if(financialHouseHoldDone<=houseHoldNumberValue)
				appscr74EditController(financialHouseHoldDone);
		}
		haveDataFlag = false;
	}
	else if(currentPage1 == "appscr70")
	{
		haveDataFlag = true;
		if(useJSON == true){
			appscr74EditController(financialHouseHoldDone);
		}else {
			if(financialHouseHoldDone<=houseHoldNumberValue)
				appscr74EditController(financialHouseHoldDone);
		}
		haveDataFlag = false;
	}
	else if(currentPage1 == "appscr69")
	{

		if(useJSON == true){
			appscr69FromMongodbJSON(financialHouseHoldDone);
		}

	}
	else if(currentPage1 == "appscr75B")
	{
		appscr76FromMongodb();
	}
	else if(currentPage1 == "appscr75A"){
		if(useJSON == true){
			appscr76FromMongodb();
		}
	}
	else if(currentPage1 == "appscr76P1"){
		if(useJSON == true){
			appscr76FromMongodb();
		}
	}
	else if(currentPage1 == "appscr76P2"){
		if(useJSON == true){
			appscr76FromMongodb();
		}
	}
	else if(currentPage1 == "appscr77Part1"){
		if(useJSON == true){
			appscr76FromMongodb();
		}
	}
	else if(currentPage1 == "appscr77Part2")
	{
		fillDataAppscr78ToTable();
		if(useJSON == true){
			appscr76FromMongodb();
		}
	}
	else if(currentPage1 == "appscr78")
	{
		fillValuesAppscr79FromMongodb();
	}
	else if(currentPage1 == "appscr80")
	{
		fillValuesAppscr81FromMongodb();
	}
	
	else if(currentPage1 == "appscr83")
	{
		generateReviewSummaryReport();
	}	
	
}

function setValuesToappscr53Mongodb()
{

	if(useJSON == true){
		setValuesToappscr53JSON();
		return;
	}

	/*var addressSameFlag = true;
	var householdAccountHolder = true;

	$('#firstName').val($('#appscr57FirstNameTD1').text());
	$('#middleName').val($('#appscr57MiddleNameTD1').text());
	$('#lastName').val($('#appscr57LastNameTD1').text());

	var temp = $('#appscr57SuffixTD1').text();
	if(temp == "Suffix")
		$('#appscr53Suffix').val('0');
	else
		$('#appscr53Suffix').val(temp);

	$('#dateOfBirth').val($('#appscr57DOBTD1').text());
	$('#dobVerificationStatus').val($('#appscr57DOBVerificationTD1').text());
	$('#emailAddress').val($('#appscr57emailAddressTD1').text());

	$('#home_addressLine1').val($('#appscr57HouseHoldhome_addressLine1TD1').text());
	$('#home_addressLine2').val($('#appscr57HouseHoldhome_addressLine2TD1').text());
	$('#home_primary_city').val($('#appscr57HouseHoldhome_primary_cityTD1').text());
	$('#home_primary_zip').val($('#appscr57HouseHoldhome_primary_zipTD1').text());
	$('#home_primary_state').val($('#appscr57HouseHoldhome_primary_stateTD1').text());
	$('#home_primary_county').val($('#appscr57HouseHoldhome_primary_countyTD1').text());

	$('#mailing_addressLine1').val($('#appscr57HouseHoldmailing_addressLine1TD1').text());
	$('#mailing_addressLine2').val($('#appscr57HouseHoldmailing_addressLine2TD1').text());
	$('#mailing_primary_city').val($('#appscr57HouseHoldmailing_primary_cityTD1').text());
	$('#mailing_primary_zip').val($('#appscr57HouseHoldmailing_primary_zipTD1').text());
	$('#mailing_primary_state').val($('#appscr57mailing_primary_stateTD1').text());
	$('#mailing_primary_county').val($('#appscr57HouseHoldmailing_primary_countyTD1').text());

	$('#first_phoneNo').val($('#appscr57houseHoldPhoneFirstTD1').text());
	$('#first_ext').val($('#appscr57houseHoldPhoneExtFirstTD1').text());
	$('#first_phoneType').val($('#appscr57houseHoldPhoneTypeFirstTD1').text());
	$('#second_phoneNo').val($('#appscr57houseHoldPhoneSecondTD1').text());
	$('#second_ext').val($('#appscr57houseHoldPhoneExtSecondTD1').text());
	$('#second_phoneType').val($('#appscr57houseHoldPhoneTypeSecondTD1').text());
	$('#preffegred_spoken_language').val($('#appscr57houseHoldSpokenLanguageTD1').text());
	$('#preffered_written_language').val($('#appscr57houseHoldWrittenLanguageTD1').text());

	if(!($('#appscr57FirstNameTD1').text() == firstNameDB))
		householdAccountHolder = false;
	if(!($('#appscr57MiddleNameTD1').text() == middleNameDB))
		householdAccountHolder = false;
	if(!($('#appscr57LastNameTD1').text() == lastNameDB))
		householdAccountHolder = false;
	if(!($('#appscr57emailAddressTD1').text() == emailDB))
		householdAccountHolder = false;
	if(!($('#appscr57houseHoldPhoneFirstTD1').text() == phoneDB))
		householdAccountHolder = false;

	document.getElementById('checkIsAccountHolder').checked = householdAccountHolder;


	if(!($('#appscr57HouseHoldhome_addressLine1TD1').text() == $('#appscr57HouseHoldmailing_addressLine1TD1').text()))
		addressSameFlag = false;
	if(!($('#appscr57HouseHoldhome_addressLine2TD1').text() == $('#appscr57HouseHoldmailing_addressLine2TD1').text()))
		addressSameFlag = false;
	if(!($('#appscr57HouseHoldhome_primary_cityTD1').text() == $('#appscr57HouseHoldmailing_primary_cityTD1').text()))
		addressSameFlag = false;
	if(!($('#appscr57HouseHoldhome_primary_zipTD1').text() == $('#appscr57HouseHoldmailing_primary_zipTD1').text()))
		addressSameFlag = false;
	if(!($('#appscr57HouseHoldhome_primary_stateTD1').text() == $('#appscr57mailing_primary_stateTD1').text()))
		addressSameFlag = false;
	if(!($('#appscr57HouseHoldhome_primary_countyTD1').text() == $('#appscr57HouseHoldmailing_primary_countyTD1').text()))
		addressSameFlag = false;

		document.getElementById('mailingAddressIndicator').checked = addressSameFlag;

		addressIsSame();
	
	if(houseHoldNumberValue>1){
		$('#wantToGetHelpPayingHealthInsurance_id2').prop("checked", true);
	}else if(ssapApplicationType != "NON_FINANCIAL"){
		$('#wantToGetHelpPayingHealthInsurance_id1').prop("checked", true);	
	}

	showOrHideOptionDiv();*/
}

function setValuesToappscr54Mongodb()
{

	if(useJSON == true){
		setValuesToappscr54JSON();
		return;
	}
	/*var temp = "";

	$('#brokerName54').val($('#appscr54BrokerNameTD1').text());
	$('#brokerID54').val($('#appscr54BrokerIDTD1').text());

	temp = $('#appscr54authRepIndicatorTD1').text();
	if(temp == "true")
		$('#displayAuthRepresentativeInfo').prop("checked", true);
	else
		$('#hideAuthRepresentativeInfo').prop("checked", true);

	$('#authorizedFirstName').val($('#appscr54authRepFirstNameTD1').text());
	$('#authorizedMiddleName').val($('#appscr54authRepMiddleNameTD1').text());
	$('#authorizedLastName').val($('#appscr54authRepLastNameTD1').text());

	$('#authorizedEmailAddress').val($('#appscr54authRepEmailTD1').text());

	$('#authorizedAddress1').val($('#appscr54authRepStreetAddr1TD1').text());
	$('#authorizedAddress2').val($('#appscr54authRepStreetAddr2TD1').text());
	$('#authorizedCity').val($('#appscr54authRepCityTD1').text());

	$('#authorizedState').val($('#appscr54authRepStateTD1').text());
	$('#authorizedZip').val($('#appscr54authRepPostalCodeTD1').text());

	$('#appscr54_phoneNumber').val($('#appscr57authRepPhoneNoTD1').text());
	$('#authorizedPhoneType').val($('#appscr57authRepPhoneTypeTD1').text());
	$('#appscr54_ext').val($('#appscr57authRepPhoneExtensionTD1').text());

	$('#authorizeCompanyName').val($('#appscr57authRepCompanyNameTD1').text());
	$('#authorizeOrganizationId').val($('#appscr57authRepOrganizationIDTD1').text());*/

}

function setValuesToappscr57Mongodb()
{
	if(useJSON == true){
		setValuesToappscr57JSON();
		return;
	}
	/*//var household = $('#noOfPeople53').val();
	for(var i=1; i<=houseHoldNumberValue; i++)
	{
		var temp = "";
		if(i==1)
		{
			temp = $('#appscr57TobaccoTD'+i).text();

			if(temp == "yes")
				$('#appscr57SexYes'+i).prop("checked", true);
			else
				$('#appscr57SexNo'+i).prop("checked", true);

			temp = $('#appscr57SexTD'+i).text();
			if(temp == "male")
				$('#appscr57Sexmale'+i).prop("checked", true);
			else
				$('#appscr57SexFemale'+i).prop("checked", true);

			continue;
		}

		$('#appscr57FirstName'+i).val($('#appscr57FirstNameTD'+i).text());
		$('#appscr57MiddleName'+i).val($('#appscr57MiddleNameTD'+i).text());
		$('#appscr57LastName'+i).val($('#appscr57LastNameTD'+i).text());
		$('#appscr57Suffix'+i).val($('#appscr57SuffixTD'+i).text());
		$('#appscr57DOB'+i).val($('#appscr57DOBTD'+i).text());
		$('#appscr57DOBVerificationStatus'+i).val($('#appscr57DOBVerificationTD'+i).text());

		temp = $('#appscr57TobaccoTD'+i).text();
		if(temp == "yes")
			$('#appscr57SexYes'+i).prop("checked", true);
		else
			$('#appscr57SexNo'+i).prop("checked", true);

		temp = $('#appscr57SexTD'+i).text();
		if(temp == "male")
			$('#appscr57SexMale'+i).prop("checked", true);
		else
			$('#appscr57SexFemale'+i).prop("checked", true);

		$('#applicantName57'+i).text($('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text());
	}*/
}

function updateAppscr60FromMongodb(i)
{

	if(useJSON == true){
		updateAppscr60FromJSON(i);
		return;
	}

	/*var temp = $('#appscr57isMarriedTD'+i).text();
	//alert(temp+"    "+i);
	if(temp == "true"){

		$('#marriedIndicatorYes').prop("checked", true);
		isMarried();
	}
	else{

		$('#marriedIndicatorNo').prop("checked", true);
		isNotMarried();
	}

	temp = $('#appscr57spouseIdTD'+i).text();
	if(temp != "false")
	{
		temp =trimSpaces($('#appscr57spouseIdTD'+i).text());
		temp = temp.substring(9);
		//alert("Individual : "+temp);
		$('#householdContactSpouse'+temp).prop("checked", true);
	}


	temp = $('#appscr57payJointlyTD'+i).text();
	if(temp == "true"){

		$('#planOnFilingJointFTRIndicatorYes').prop("checked", true);
		householdPayJointly();
	}
	else{

		$('#planOnFilingJointFTRIndicatorNo').prop("checked", true);
		householdNotPayJointly();
	}


	temp = $('#appscr57isTaxFilerTD'+i).text();
	if(temp == "true")
		$('#planToFileFTRIndicatorYes').prop("checked", true);
	else
		$('#planToFileFTRIndicatorNo').prop("checked", true);


	temp = $('#appscr57isLiveWithSpouseTD'+i).text();
	if(temp == "true")
		$('#liveWithSpouseIndicatorYes').prop("checked", true);
	else
		$('#liveWithSpouseIndicatorNo').prop("checked", true);


	temp = $('#appscr57isClaimDependantsTD'+i).text();
	if(temp == "true"){
		$('#householdHasDependantYes').prop("checked", true);
	}
	else{
		$('#householdHasDependantNo').prop("checked", true);
	}

	changeDependentDivView60A();

	var countRow = 1;
	$('#claimDependantsRow'+i+' td').each(function(){
		//alert("claimDependantsRow"+i);

		var id = "";
		if(countRow%2 == 0)
		{
			id = $(this).text().substring(9);
			document.getElementById('householdContactDependant'+id).checked = true;
			//alert(id);
		}

		countRow++;
	});*/
}


function appscr65FromMongodb()
{

	if(useJSON == true){
		appscr65FromMongodbJSON();
		return;
	}
	/*$('#appscr57HouseHoldPersonalInformation tr').each(function( i ) {

				var pureNameOfUser='';

				$("td", this).each(function( j) {
					if(j<3)
						pureNameOfUser+= $(this).text();
					 if(j<2)
						 pureNameOfUser+='_';
					  });

				nameWithSpace= getNameRemovedBySpace(pureNameOfUser);

					var actualName=getNameByTRIndex(i);

				liveAtOtherAddress = $('#appscr57liveAtOtherAddressIndicatorTD'+(i+1)).text();

				//alert(liveAtOtherAddress+"      "+actualName);

				if(i>0 && liveAtOtherAddress == "true")
				{

					document.getElementById('householdContactAddress'+actualName).checked = true;
					checkUncheck(actualName);

					$('#applicant_or_non-applican_address_1'+actualName).val($('#appscr57HouseHoldhome_addressLine1TD'+(i+1)).text());
					$('#applicant_or_non-applican_address_2'+actualName).val($('#appscr57HouseHoldhome_addressLine2TD'+(i+1)).text());
					$('#city'+actualName).val($('#appscr57HouseHoldhome_primary_cityTD'+(i+1)).text());
					$('#zip'+actualName).val($('#appscr57HouseHoldhome_primary_zipTD'+(i+1)).text());
					$('#applicant_or_non-applican_state'+actualName).val($('#appscr57HouseHoldhome_primary_stateTD'+(i+1)).text());
					//alert($('#appscr57HouseHoldhome_primary_stateTD'+(i+1)).text());
				}

			});
*/
}


function appscr66NoneOfThese(){
	$('#disabilityNoneOfThese').prop('checked',disabilityFlag);
	$('#assistanceServicesNoneOfThese').prop('checked',i_cover_adoptionFlag);
	$('#nativeNoneOfThese').prop('checked',i_nativeAmericonFlag);
	$('#personfosterCareNoneOfThese').prop('checked',i_cover_fosterFlag);
	$('#deceasedPersonNoneOfThese').prop('checked',i_deceasedFlag);
	//document.getElementById('disabilityNoneOfThese').checked = disabilityFlag;
	//document.getElementById('assistanceServicesNoneOfThese').checked = i_cover_adoptionFlag;
	//document.getElementById('nativeNoneOfThese').checked = i_nativeAmericonFlag;
	//document.getElementById('personfosterCareNoneOfThese').checked = i_cover_fosterFlag;
	//document.getElementById('deceasedPersonNoneOfThese').checked = i_deceasedFlag;
	if($('#pregnancyNoneOfThese').html() != undefined){
		$('#pregnancyNoneOfThese').prop('checked',isPregnantFlag);
		//document.getElementById('pregnancyNoneOfThese').checked = isPregnantFlag;
	}
}


function appscr66FromMongodb()
{
	if(useJSON == true){
		appscr66FromMongodbJson();
		return;
	}
	
	/*var disability;
	var i_cover_adoption;
	var i_nativeAmericon;
	var i_cover_foster;
	var i_deceased;
	var isPregnant;
	var fatusCount;
	var isPersonGettingHealthCare;

	for(var i=1; i<=houseHoldNumberValue; i++)
	{
		disability = $('#appscr57HouseHoldDisability'+i).text();
		i_cover_adoption = $('#appscr57HouseHoldAssistanceServices'+i).text();
		i_nativeAmericon = $('#appscr57HouseHoldNative'+i).text();
		i_cover_foster = $('#appscr57HouseHoldPersonfosterCare'+i).text();
		i_deceased = $('#appscr57HouseHoldDeceased'+i).text();
		isPregnant = $('#appscr57HouseHoldPregnant'+i).text();
		fatusCount = $('#appscr57HouseHoldFetusCount'+i).text();
		isPersonGettingHealthCare = $('#appscr57HouseHoldIsPersonGettingHealthCare'+i).text();


		if(disability == "yes")
			document.getElementById('disability'+i).checked = true;
		else
			document.getElementById('disability'+i).checked = false;

		if(disability == "yes" && disabilityFlag)
			disabilityFlag = false;

		if(i_cover_adoption == "yes")
			document.getElementById('assistanceServices'+i).checked = true;
		else
			document.getElementById('assistanceServices'+i).checked = false;

		if(i_cover_adoption == "yes" && i_cover_adoptionFlag)
			i_cover_adoptionFlag = false;

		if(i_nativeAmericon == "yes")
			document.getElementById('native'+i).checked = true;
		else
			document.getElementById('native'+i).checked = false;

		if(i_nativeAmericon == "yes" && i_nativeAmericonFlag)
			i_nativeAmericonFlag = false;

		if(i_cover_foster == "yes")
			document.getElementById('personfosterCare'+i).checked = true;
		else
			document.getElementById('personfosterCare'+i).checked = false;

		if(i_cover_foster == "yes" && i_cover_fosterFlag)
			i_cover_fosterFlag = false;

		if(i_deceased == "yes")
			document.getElementById('deceased'+i).checked = true;
		else
			document.getElementById('deceased'+i).checked = false;

		if(i_deceased == "yes" && i_deceasedFlag)
			i_deceasedFlag = false;

		if($('#appscrHouseHoldgender57TD'+i).text() == "female")
		{
			if(isPregnant == "yes")
			{
				document.getElementById('pregnancy'+i).checked = true;
				getExpectedInPregnancy();
				$('#number-of-babies').val(fatusCount);
			}
			else
			{
				if(document.getElementById('pregnancy'+i)!=null){
					document.getElementById('pregnancy'+i).checked = false;
				}

			}
		}


		if(isPregnant == "yes" && isPregnantFlag)
			isPregnantFlag = false;

		if(isPersonGettingHealthCare == "yes")
			$('#isPersonGettingHealthCareYes'+i).prop("checked", true);
		else
			$('#isPersonGettingHealthCareNo'+i).prop("checked", true);

	}
*/
	//alert(i_deceasedFlag);
}


function appscrSNAPfromMongodb()
{
	var temp = $('#appscr57personCashBenefits1').text();

	if(temp == "yes" || temp == "Yes")
		$('#cashBenefitsYes').prop("checked", true);
	else
		$('#cashBenefitsNo').prop("checked", true);


	for (var i = 1; i <= houseHoldNumberValue; i++)
	{
		temp = $('#appscr57Identification'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('identification'+i).checked = true;
		else
			document.getElementById('identification'+i).checked = false;


		temp = $('#appscr57IdentificationVerification'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('identificationVerification'+i).checked = true;
		else
			document.getElementById('identificationVerification'+i).checked = false;


		temp = $('#appscr57Food'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('food'+i).checked = true;
		else
			document.getElementById('food'+i).checked = false;


		temp = $('#appscr57FoodVerification'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('foodVerification'+i).checked = true;
		else
			document.getElementById('foodVerification'+i).checked = false;


		temp = $('#appscr57IsStriker'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('isStriker'+i).checked = true;
		else
			document.getElementById('isStriker'+i).checked = false;


		temp = $('#appscr57Having_IPV'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('having_IPV'+i).checked = true;
		else
			document.getElementById('having_IPV'+i).checked = false;


		temp = $('#appscr57ResidencyVerified'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('residencyVerified'+i).checked = true;
		else
			document.getElementById('residencyVerified'+i).checked = false;


		temp = $('#appscr57DataSSI'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('dataSSI'+i).checked = true;
		else
			document.getElementById('dataSSI'+i).checked = false;


		temp = $('#appscr57DrugFellow'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('drugFellow'+i).checked = true;
		else
			document.getElementById('drugFellow'+i).checked = false;


		temp = $('#appscr57DependentCareExpenseCost1').text();
		if(temp != "0.0" || temp != "0")
		{
			$('#dependentCareExpenseCost').val(temp);

			temp = $('#appscr57DependentCareExpenseFrequency1').text();
			$('#dependentCareExpenseFrequency').val(temp);
		}

		temp = $('#appscr57ChildSupportCost1').text();
		if(temp != "0.0" || temp != "0")
		{
			$('#childSupportCost').val(temp);

			temp = $('#appscr57ChildSupportFrequency1').text();
			$('#childSupportFrequency').val(temp);
		}

		temp = $('#appscr57MedicalCost1').text();
		if(temp != "0.0" || temp != "0")
		{
			$('#medicalCost').val(temp);

			temp = $('#appscr57MedicalFrequency1').text();
			$('#medicalFrequency').val(temp);
		}

		temp = $('#appscr57ShelterAndUtilityExpenseCost1').text();
		if(temp != "0.0" || temp != "0")
		{
			$('#shelterAndUtilityExpenseCost').val(temp);

			temp = $('#appscr57ShelterAndUtilityExpenseFrequency1').text();
			$('#shelterAndUtilityExpenseFrequency').val(temp);
		}

		temp = $('#appscr57HeatingAndCoolingIndicator1').text();
		if(temp == "yes" || temp == "Yes")
			$('#hAndCYes').prop("checked", true);
		else
			$('#hAndCNo').prop("checked", true);
	}
}


function appscr68FromMongodb()
{

}

function setValuesToappscr53JSON()
{

	var addressSameFlag = true;
	var householdAccountHolder = true;
	
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];

	$('#firstName').val(trimSpaces(householdMemberObj.name.firstName));
	$('#middleName').val(trimSpaces(householdMemberObj.name.middleName));
	$('#lastName').val(trimSpaces(householdMemberObj.name.lastName));

	var temp = householdMemberObj.name.suffix;

	if(temp == "Suffix")
		$('#appscr53Suffix').val('0');
	else
		$('#appscr53Suffix').val(temp);

	//$('#appscr53Suffix').val(householdMemberObj.name.suffix);



	$('#dateOfBirth').val(UIDateformat(householdMemberObj.dateOfBirth));
	$('#dobVerificationStatus').val($('#appscr57DOBVerificationTD1').text());
	$('#emailAddress').val(householdMemberObj.householdContact.contactPreferences.emailAddress);


	$('#home_addressLine1').val(trimSpaces(householdMemberObj.householdContact.homeAddress.streetAddress1));
	$('#home_addressLine2').val(trimSpaces(householdMemberObj.householdContact.homeAddress.streetAddress2));
	$('#home_primary_city').val(trimSpaces(householdMemberObj.householdContact.homeAddress.city));
	$('#home_primary_zip').val(trimSpaces(householdMemberObj.householdContact.homeAddress.postalCode));
	$('#home_primary_state').val(householdMemberObj.householdContact.homeAddress.state);

	//$('#home_primary_county').val(householdMemberObj.householdContact.homeAddress.county);
	var homeAddressCounty = householdMemberObj.householdContact.homeAddress.county;
	var homeAddressCountyCode = householdMemberObj.householdContact.homeAddress.primaryAddressCountyFipsCode;

	if(homeAddressCounty != null && homeAddressCounty != "") {
		$("#home_primary_county").append("<option value='" + homeAddressCountyCode + "' selected>" + homeAddressCounty+ "</option>");
	}


	$('#mailing_addressLine1').val(trimSpaces(householdMemberObj.householdContact.mailingAddress.streetAddress1));
	$('#mailing_addressLine2').val(trimSpaces(householdMemberObj.householdContact.mailingAddress.streetAddress2));
	$('#mailing_primary_city').val(trimSpaces(householdMemberObj.householdContact.mailingAddress.city));
	$('#mailing_primary_zip').val(trimSpaces(householdMemberObj.householdContact.mailingAddress.postalCode));
	$('#mailing_primary_state').val(householdMemberObj.householdContact.mailingAddress.state);

	$('#mailing_primary_county').val(householdMemberObj.householdContact.mailingAddress.county);

	var mailingAddressCounty = householdMemberObj.householdContact.homeAddress.county;

	if(mailingAddressCounty != null && mailingAddressCounty != "") {
		$("#mailing_primary_county").append("<option value='" + mailingAddressCounty + "' selected>" + mailingAddressCounty + "</option>");
	}


	$('#first_phoneNo').val(householdMemberObj.householdContact.phone.phoneNumber);
	$('#first_ext').val(householdMemberObj.householdContact.phone.phoneExtension);	
	$('#first_homePhoneNo').val(householdMemberObj.householdContact.otherPhone.phoneNumber);
	$('#first_homeExt').val(householdMemberObj.householdContact.otherPhone.phoneExtension);	
	
	$('#first_phoneType').val($('#appscr57houseHoldPhoneTypeFirstTD1').text());
	$('#second_phoneNo').val(householdMemberObj.householdContact.otherPhone.phoneNumber);
	$('#second_ext').val(householdMemberObj.householdContact.otherPhone.phoneExtension);
	$('#second_phoneType').val($('#appscr57houseHoldPhoneTypeSecondTD1').text());
	$('#preffegred_spoken_language').val(householdMemberObj.householdContact.contactPreferences.preferredSpokenLanguage);
	$('#preffered_written_language').val(householdMemberObj.householdContact.contactPreferences.preferredWrittenLanguage);	
	
	//mask the phone number after retrieve from JSON
	$('.phoneNumber').mask('(000) 000-0000');
	
	if(householdMemberObj.householdContact.contactPreferences.preferredContactMethod == "EMAIL"){
		$('#email').prop("checked", true);
	}
	
	if(householdMemberObj.householdContact.contactPreferences.preferredContactMethod == "POSTAL MAIL"){
		$('#inTheEmail').prop("checked", true);
	}
	
	if(householdMemberObj.householdContact.contactPreferences.preferredContactMethod == "EMAIL | POSTAL MAIL"){
		$('#inTheEmail').prop("checked", true);
		$('#email').prop("checked", true);
	}

	if(!($('#appscr57FirstNameTD1').text() == firstNameDB))
		householdAccountHolder = false;
	if(!($('#appscr57MiddleNameTD1').text() == middleNameDB))
		householdAccountHolder = false;
	if(!($('#appscr57LastNameTD1').text() == lastNameDB))
		householdAccountHolder = false;
	if(!($('#appscr57emailAddressTD1').text() == emailDB))
		householdAccountHolder = false;
	if(!($('#appscr57houseHoldPhoneFirstTD1').text() == phoneDB))
		householdAccountHolder = false;

	//document.getElementById('checkIsAccountHolder').checked = householdAccountHolder;


	if(!($('#appscr57HouseHoldhome_addressLine1TD1').text() == $('#appscr57HouseHoldmailing_addressLine1TD1').text()))
		addressSameFlag = false;
	if(!($('#appscr57HouseHoldhome_addressLine2TD1').text() == $('#appscr57HouseHoldmailing_addressLine2TD1').text()))
		addressSameFlag = false;
	if(!($('#appscr57HouseHoldhome_primary_cityTD1').text() == $('#appscr57HouseHoldmailing_primary_cityTD1').text()))
		addressSameFlag = false;
	if(!($('#appscr57HouseHoldhome_primary_zipTD1').text() == $('#appscr57HouseHoldmailing_primary_zipTD1').text()))
		addressSameFlag = false;
	if(!($('#appscr57HouseHoldhome_primary_stateTD1').text() == $('#appscr57mailing_primary_stateTD1').text()))
		addressSameFlag = false;
	if(!($('#appscr57HouseHoldhome_primary_countyTD1').text() == $('#appscr57HouseHoldmailing_primary_countyTD1').text()))
		addressSameFlag = false;

		document.getElementById('mailingAddressIndicator').checked = addressSameFlag;

		addressIsSame();

		if(houseHoldNumberValue>1){
			$('#wantToGetHelpPayingHealthInsurance_id2').prop("checked", true);
		}else if(ssapApplicationType != "NON_FINANCIAL"){
			$('#wantToGetHelpPayingHealthInsurance_id1').prop("checked", true);	
		}
		
	showOrHideOptionDiv();
}

function setValuesToappscr54JSON()
{

	var helpType = webJson.singleStreamlinedApplication.helpType;
	var temp = "";	
	$("input[name=authorizedRepresentativeIndicator]").prop("checked",false);	
	if(helpType == "BROKER"){
		$("#helpQuestionDiv").hide();
		$("#brokerDiv, #authorizedRepresentativeDiv").show();
		
		brokerObj=webJson.singleStreamlinedApplication.broker;

		$('#brokerName54').val(brokerObj.brokerName);
		$('#brokerID54').val(brokerObj.brokerFederalTaxIdNumber);
		
		
	
		
	}else if(helpType == "ASSISTER"){
		$("#helpQuestionDiv").hide();
		$("#guideDiv, #authorizedRepresentativeDiv").show();
		
		assisterObj=webJson.singleStreamlinedApplication.assister;
		
		$('#guideID54').val(assisterObj.assisterID);
		$('#guideName54').val(assisterObj.assisterName);
		
	}else{
		$("#helpQuestionDiv").show();
		$("#brokerDiv, #authorizedRepresentativeDiv,#guideDiv, #authorizedRepresentativeDiv").hide();
		$("input[name=authorizedRepresentativeHelp]").prop("checked",false);
		if(webJson.singleStreamlinedApplication.getHelpIndicator == true){
			$("#authorizedRepresentativeHelpYes").prop("checked",true);
			$("#authorizedRepresentativeDiv").show();
			
		}else if(webJson.singleStreamlinedApplication.getHelpIndicator == false){
			$("#authorizedRepresentativeHelpNo").prop("checked",true);
		}
	}
	
	if(webJson.singleStreamlinedApplication.getHelpIndicator == true){
		temp = webJson.singleStreamlinedApplication.authorizedRepresentativeIndicator;
		
		if(temp === true){
			$('#displayAuthRepresentativeInfo').prop("checked", true);
			displayAuthInfo();
			
			AuthorizedRepresentativeObj = webJson.singleStreamlinedApplication.authorizedRepresentative;

			$('#authorizedFirstName').val(AuthorizedRepresentativeObj.name.firstName);
			$('#authorizedMiddleName').val(AuthorizedRepresentativeObj.name.middleName);
			$('#authorizedLastName').val(AuthorizedRepresentativeObj.name.lastName);

			$('#authorizedEmailAddress').val(AuthorizedRepresentativeObj.emailAddress);

			$('#authorizedAddress1').val(AuthorizedRepresentativeObj.address.streetAddress1);
			$('#authorizedAddress2').val(AuthorizedRepresentativeObj.address.streetAddress2);
			$('#authorizedCity').val(AuthorizedRepresentativeObj.address.city);

			$('#authorizedState').val(AuthorizedRepresentativeObj.address.state);
			$('#authorizedZip').val(AuthorizedRepresentativeObj.address.postalCode);
			
			
			$('#appscr54_phoneNumber').val(AuthorizedRepresentativeObj.phone[0].phoneNumber);
			$('#appscr54_ext').val(AuthorizedRepresentativeObj.phone[0].phoneExtension);
			
			$('#appscr54_homePhoneNumber').val(AuthorizedRepresentativeObj.phone[1].phoneNumber);
			$('#appscr54_homeExt').val(AuthorizedRepresentativeObj.phone[1].phoneExtension);
			
			$('#appscr54_workPhoneNumber').val(AuthorizedRepresentativeObj.phone[2].phoneNumber);
			$('#appscr54_workExt').val(AuthorizedRepresentativeObj.phone[2].phoneExtension);
			
			//mask the phone number after retrieve from JSON
			$('.phoneNumber').mask('(000) 000-0000');
			
			temp = AuthorizedRepresentativeObj.partOfOrganizationIndicator;
			if(temp === true){
				$("#cmpnyNameAndOrgRadio54_1").prop("checked",true);
				nameOfCompanyAndOrg54();
				
				$('#authorizeCompanyName').val(AuthorizedRepresentativeObj.companyName);
				$('#authorizeOrganizationId').val(AuthorizedRepresentativeObj.organizationId);
			}else if(temp === false){
				$("#cmpnyNameAndOrgRadio54_2").prop("checked",true);
				nameOfCompanyAndOrg54();
			}
			
			temp = AuthorizedRepresentativeObj.signatureisProofIndicator;
			if(temp === true){
				$("#makeOtherizedRepresentativeSignature").prop("checked",true);
				$("#authorizeSignature").val(AuthorizedRepresentativeObj.signature);
			}else if(temp === false){
				$("#makeOtherizedRepresentativeLater").prop("checked",true);
			}
			
			
			if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == 'signature'){
				AuthorizedRepresentativeObj.signatureisProofIndicator = true;
			}else if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == 'later'){
				AuthorizedRepresentativeObj.signatureisProofIndicator = false;			
			}
			
		}else if(temp === false){
			$('#hideAuthRepresentativeInfo').prop("checked", true);
			displayAuthInfo();
		}
	}
	
	

}


function setValuesToappscr55JSON(){
	//page5
	householdMemberObj=webJson.singleStreamlinedApplication;
	$("input[name=ApplyingForhouseHold]").prop("checked",false);
	if(householdMemberObj.applyingForhouseHold == 'houseHoldContactOnly')
		$('#ApplyingForhouseHoldOnly').prop("checked", true);
	else if(householdMemberObj.applyingForhouseHold == 'otherFamilyMember')
		$('#ApplyingForhouseHoldMember').prop("checked", true);
	else if(householdMemberObj.applyingForhouseHold == 'otherExcludingHousehold')
		$('#ApplyingForhouseHoldOther').prop("checked", true);
	
	if(ssapApplicationType == "NON_FINANCIAL"){
		householdMemberObj.applyingForFinancialAssistanceIndicator = false;
	}
	$("input[name=wantToGetHelpPayingHealthInsurance]").prop("checked",false);
	if(householdMemberObj.applyingForFinancialAssistanceIndicator == true)
		$('#wantToGetHelpPayingHealthInsurance_id1').prop("checked", true);
	else if(householdMemberObj.applyingForFinancialAssistanceIndicator == false)
		$('#wantToGetHelpPayingHealthInsurance_id2').prop("checked", true);
	
	showOrHideOptionDiv();
}



function setValuesToappscr57JSON()
{
	//var household = $('#noOfPeople53').val();

	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];

	noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	//addAnotherIndividual
	//noOfHouseHold
	$('#appscr57HouseholdForm').html('');
	for(var i=1; i<=noOfHouseHoldmember; i++){
		noOfHouseHold = i;
		addAnotherIndividual(i);
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		$('#appscr57FirstName'+i).val(householdMemberObj.name.firstName);
		$('#appscr57MiddleName'+i).val(householdMemberObj.name.middleName);
		$('#appscr57LastName'+i).val(householdMemberObj.name.lastName);
		$('#appscr57Suffix'+i).val(householdMemberObj.name.suffix);
		$('#appscr57DOB'+i).val(UIDateformat(householdMemberObj.dateOfBirth));		
		
		$('#applicantName57' + i).text(getNameByPersonId(i));
		$('#applicantTitle57' + i).text(getNameByPersonId(i));
		
		
		
		temp = householdMemberObj.tobaccoUserIndicator;
		if(temp == true)
			$('#appscr57SexYes'+i).prop("checked", true);
		else
			$('#appscr57SexNo'+i).prop("checked", true);

		if(householdMemberObj.applyingForCoverageIndicator == true)
			$('#appscr57checkseekingcoverage'+i).prop("checked", true);
		else
			$('#appscr57checkseekingcoverage'+i).prop("checked", false);
		//$('#appscr57DOBVerificationStatus'+i).val($('#appscr57DOBVerificationTD'+i).text());

		/*temp = $('#appscr57TobaccoTD'+i).text();
		if(temp == "yes")
			$('#appscr57SexYes'+i).prop("checked", true);
		else
			$('#appscr57SexNo'+i).prop("checked", true);

		temp = $('#appscr57SexTD'+i).text();
		if(temp == "male")
			$('#appscr57SexMale'+i).prop("checked", true);
		else
			$('#appscr57SexFemale'+i).prop("checked", true);*/


	}


	/*for(var i=1; i<=houseHoldNumberValue; i++)
	{
		var temp = "";
		if(i==1)
		{
			temp = $('#appscr57TobaccoTD'+i).text();

			if(temp == "yes")
				$('#appscr57SexYes'+i).prop("checked", true);
			else
				$('#appscr57SexNo'+i).prop("checked", true);

			temp = $('#appscr57SexTD'+i).text();
			if(temp == "male")
				$('#appscr57Sexmale'+i).prop("checked", true);
			else
				$('#appscr57SexFemale'+i).prop("checked", true);

			continue;
		}

		$('#appscr57FirstName'+i).val($('#appscr57FirstNameTD'+i).text());
		$('#appscr57MiddleName'+i).val($('#appscr57MiddleNameTD'+i).text());
		$('#appscr57LastName'+i).val($('#appscr57LastNameTD'+i).text());
		$('#appscr57Suffix'+i).val($('#appscr57SuffixTD'+i).text());
		$('#appscr57DOB'+i).val($('#appscr57DOBTD'+i).text());
		$('#appscr57DOBVerificationStatus'+i).val($('#appscr57DOBVerificationTD'+i).text());

		temp = $('#appscr57TobaccoTD'+i).text();
		if(temp == "yes")
			$('#appscr57SexYes'+i).prop("checked", true);
		else
			$('#appscr57SexNo'+i).prop("checked", true);

		temp = $('#appscr57SexTD'+i).text();
		if(temp == "male")
			$('#appscr57SexMale'+i).prop("checked", true);
		else
			$('#appscr57SexFemale'+i).prop("checked", true);

		$('#applicantName57'+i).text($('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text());
	}*/
}

function updateAppscr60FromJSON(i)
{

	if(useJSON == true){
		var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];

		if(householdMemberObj.planToFileFTRIndicator === true){
			$("#planToFileFTRIndicatorYes").prop("checked",true);
			planToFileYes();		
		}else if(householdMemberObj.planToFileFTRIndicator === false){
			$("#planToFileFTRIndicatorNo").prop("checked",true);
			planToFileNo();
		}else if(householdMemberObj.planToFileFTRIndicator === null){
			$("input[name=planToFileFTRIndicator]").prop("checked",false);
			
			//hide all sequence questions
			$("#marriedIndicatorDiv, #householdSpouseMainDiv, #willClaimDependents, #householdDependentMainDiv, #dependentDetailDiv_inputFields, #willBeClaimed, #appscr60ClaimedFilerDiv").hide();
		}
		
		//update married
		if(householdMemberObj.marriedIndicator === true){
			$("#marriedIndicatorYes").prop("checked",true);
			isMarried();		
			
			
			//update joint tax file
			if(householdMemberObj.planToFileFTRJontlyIndicator === true){
				$("#planOnFilingJointFTRIndicatorYes").prop("checked",true);
				householdPayJointly();
			}else if(householdMemberObj.planToFileFTRJontlyIndicator === false){
				$("#planOnFilingJointFTRIndicatorNo").prop("checked",true);
				householdNotPayJointly();
			}else if(householdMemberObj.planToFileFTRJontlyIndicator === null){
				$("input[name=planOnFilingJointFTRIndicator]").prop("checked",false);
				householdNotPayJointly();
			}
			
			//update spouse
			getSpouse(i);
			if(householdMemberObj.taxFiler.spouseHouseholdMemberId > 0){
				$("#householdContactSpouse" + householdMemberObj.taxFiler.spouseHouseholdMemberId).prop("checked",true);
				
			}else if(householdMemberObj.taxFiler.spouseHouseholdMemberId == 0){
				$("#householdSpouseDiv input[name=householdContactSpouse]").prop("checked",false);
			}		
			setSpouse();
			changeSpouseDivView60A();		
			
			
			//update living with spouse
			if(householdMemberObj.taxFiler.liveWithSpouseIndicator === true){
				$("#liveWithSpouseIndicatorYes").prop("checked",true);
			}else if(householdMemberObj.taxFiler.liveWithSpouseIndicator === false){
				$("#liveWithSpouseIndicatorNo").prop("checked",true);
			}else if(householdMemberObj.taxFiler.liveWithSpouseIndicator === null){
				$("input[name=liveWithSpouseIndicator]").prop("checked",false);
			}
			
			
		}else if(householdMemberObj.marriedIndicator === false){
			$("#marriedIndicatorNo").prop("checked",true);
			isNotMarried();
			
			$("#householdSpouseMainDiv input").prop("checked",false);
		}else if(householdMemberObj.marriedIndicator === null){
			$("input[name=marriedIndicator]").prop("checked",false);
			isNotMarried();
			
			$("#householdSpouseMainDiv input").prop("checked",false);
		}
		
		
		//update "claim dependents"
		if(householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator === true){
			$("#householdHasDependantYes").prop("checked",true);
			
			//update "claim dependents" check box
			for(var j=0; j<webJson.singleStreamlinedApplication.taxFilerDependants.length; j++){
				$("#householdContactDependant" + webJson.singleStreamlinedApplication.taxFilerDependants[j]).prop("checked",true);
			}
			
		}else if(householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator === false){
			$("#householdHasDependantNo").prop("checked",true);
		}else if(householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator === null){
			$("input[name=householdHasDependant]").prop("checked",false);
		}
		changeDependentDivView60A();
		
		//update "be claimed"
		if(householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === true){
			$("#doesApplicantHasTaxFillerYes").prop("checked",true);
			
			$("#householdContactFiler" + webJson.singleStreamlinedApplication.primaryTaxFilerPersonId).prop("checked",true);
			
		}else if(householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === false){
			$("#doesApplicantHasTaxFillerNo").prop("checked",true);
			
			$("#householdSpouseDiv input[name=householdContactFiler]").prop("checked",false);
		}else if(householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === null){
			$("input[name=doesApplicantHasTaxFiller]").prop("checked",false);
			$("#householdSpouseDiv input[name=householdContactFiler]").prop("checked",false);
		}	
		doesFilerHasTaxFiller();
	}
}

function appscr65FromMongodbJSON()
{	
	var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	var none = true;
	var nonenull = false;
	
	 updatePageOtherAddress();
	// populateDataToRelations();
	 setAddressDetailsToSpan();
	 populateRelationship();
	 
	 var houseHoldotherAddressNoneOfTheAbove = true;

	 
	 for(var i=2;i<=noOfHouseHoldmember;i++){
		 householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		 
		 if (householdMemberObj.livesAtOtherAddressIndicator == true){	
			 
			//if one of them is checked, set houseHoldotherAddressNoneOfTheAbove to false
			houseHoldotherAddressNoneOfTheAbove = false;
			 
			$('#householdContactAddress' + i).prop('checked',true);
			checkUncheck(i);
		 	$('#applicant_or_non-applican_address_1'+i).val(householdMemberObj.otherAddress.address.streetAddress1);
			$('#applicant_or_non-applican_address_2'+i).val(householdMemberObj.otherAddress.address.streetAddress2);
			$('#city'+i).val(householdMemberObj.otherAddress.address.city);
			$('#applicant_or_non-applican_state'+i).val(householdMemberObj.otherAddress.address.state);
			$('#zip'+i).val(householdMemberObj.otherAddress.address.postalCode);
			getCountyListByZipCode(i, householdMemberObj.otherAddress.address.county);
			if(householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator == true){
				$('#TemporarilyLivingOutsideIndicatorYes'+i).prop("checked",true);
				$('#inNMDiv'+i).show();
				
				$('#applicant2city'+i).val(householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.city);
				$('#applicant_or_non-applican_stateTemp'+i).val(householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.state);
				$('#applicant-2-zip'+i).val(householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode);
				
			}else{
				$('#TemporarilyLivingOutsideIndicatorNo'+i).prop("checked",true);
				$('#inNMDiv'+i).hide();
			}
		}else{
			 $('#householdContactAddress' + i).prop('checked',false);
			 checkUncheck(i);
		}
	 }
	 
	 if(houseHoldotherAddressNoneOfTheAbove){
		 $('#HouseHoldotherAddressNoneOfTheAbove').prop("checked", true);
	 }
	 
	/*for(var y=1;y<=noOfHouseHoldmember;y++){
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[y-1];		
		if (y!=1 && householdMemberObj.livesAtOtherAddressIndicator == true){
			none = false;
		}
		if (y!=1 && householdMemberObj.livesAtOtherAddressIndicator != null){
			nonenull = true;
		}
		if(y!=1 && householdMemberObj.otherAddress.address.streetAddress1 !="") //this applicant is living at other address
			{
			
				var name = householdMemberObj.name.firstName + "_" + householdMemberObj.name.middleName + "_" + householdMemberObj.name.lastName;
				//none = false;
				$('#householdContactAddress'+name).prop("checked", true);
				checkUncheck(name);
				$('#applicant_or_non-applican_address_1'+name).val(householdMemberObj.otherAddress.address.streetAddress1);
				$('#applicant_or_non-applican_address_2'+name).val(householdMemberObj.otherAddress.address.streetAddress2);				
				$('#city'+name).val(householdMemberObj.otherAddress.address.city);				
				$('#applicant_or_non-applican_state'+name +" option:selected").val(householdMemberObj.otherAddress.address.state)
				$('#zip'+name).val(householdMemberObj.otherAddress.address.postalCode);
				//householdMemberObj.otherAddress.address.state = $('#applicant_or_non-applican_state'+name +" option:selected").val();
				$('#inNMDiv'+name).hide();
							
				if(householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator == true){
					$('#TemporarilyLivingOutsideIndicatorYes').prop("checked", true);
					$('#applicant2city'+name).val(householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.city);
					$('#applicant_2_state'+name + " option:selected").text(householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.state);
					$('#applicant-2-zip'+name).val(householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode);
					$('#inNMDiv'+name).show();
				} else if (householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator == false){
					$('#TemporarilyLivingOutsideIndicatorNo').prop("checked", false);
					$('#applicant2city'+name).val("");
					$('#applicant_2_state'+name + " option:selected").text("");
					$('#applicant-2-zip'+name).val("");					
				}
			}
		
	}	*/
	
	/*if(none == true && haveData == true){
		$('#HouseHoldotherAddressNoneOfTheAbove').prop("checked", true);
		otherAddressNoneOfTheAbove();
	}
	
	if(nonenull == true && haveData == false){
		$('#HouseHoldotherAddressNoneOfTheAbove').prop("checked", true);
		otherAddressNoneOfTheAbove();
	}*/
	
}

function appscr69FromMongodbJSON(i){
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];

	var haveAnyIcome = false;
	$("#Financial_Job").prop('checked', false);
	if(householdMemberObj.detailedIncome.jobIncomeIndicator == true){	
		$("#Financial_Job").prop('checked', true);
		haveAnyIcome = true;
	}
	$("#Financial_Self_Employment").prop('checked', false);
	if(householdMemberObj.detailedIncome.selfEmploymentIncomeIndicator == true){
		$("#Financial_Self_Employment").prop('checked', true);
		haveAnyIcome = true;
	}
	$("#Financial_SocialSecuritybenefits").prop('checked', false);
	if(householdMemberObj.detailedIncome.socialSecurityBenefitIndicator == true){
		$("#Financial_SocialSecuritybenefits").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_Unemployment").prop('checked', false);
	if(householdMemberObj.detailedIncome.unemploymentBenefitIndicator == true){
		$("#Financial_Unemployment").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_Retirement_pension").prop('checked', false);
	if(householdMemberObj.detailedIncome.retirementOrPensionIndicator == true){
		$("#Financial_Retirement_pension").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_Capitalgains").prop('checked', false);
	if(householdMemberObj.detailedIncome.capitalGainsIndicator == true){
		$("#Financial_Capitalgains").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_RentalOrRoyaltyIncome").prop('checked', false);
	if(householdMemberObj.detailedIncome.rentalRoyaltyIncomeIndicator == true){
		$("#Financial_RentalOrRoyaltyIncome").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_FarmingOrFishingIncome").prop('checked', false);
	if(householdMemberObj.detailedIncome.farmFishingIncomeIndictor == true){
		$("#Financial_FarmingOrFishingIncome").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_AlimonyReceived").prop('checked', false);
	if(householdMemberObj.detailedIncome.alimonyReceivedIndicator == true){
		$("#Financial_AlimonyReceived").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_InvestmentIncome").prop('checked', false);
	if(householdMemberObj.detailedIncome.investmentIncomeIndicator == true){
		$("#Financial_InvestmentIncome").prop('checked', true);
		haveAnyIcome = true;
	}
	
	$("#Financial_OtherIncome").prop('checked', false);
	if(householdMemberObj.detailedIncome.otherIncomeIndicator == true){
		$("#Financial_OtherIncome").prop('checked', true);
		haveAnyIcome = true;
	}
	
	if(haveAnyIcome == true){
		$("#Financial_None").prop('checked', false);
		$("#appscr70radios4IncomeYes").prop('checked', true);
		$('#checkBoxes_div').show(); 
		$('#checkBoxes_divlbl').show();
		
		showDiductionsFields();
		
		$("#incomeSourceDiv").show();
		
		$('#incomesStatus'+i).text("Yes");

	}else{
		$("#Financial_NoneFinancial_None").prop('checked', true);
		$('#incomesStatus'+i).text("No");
	}

	//$("#Financial_OtherIncome").prop('checked', false);

	//financial_DetailsArray[0] = 'Financial_NoneFinancial_None';
	//financial_DetailsArray[11] = 'Financial_OtherIncome';
	if(haveAnyIcome == false){
	$.each(incomesecion, function(key, value) {	    
	    if(value == true)
		{	    	
	    	$("#"+key).prop('checked', true);
		}
	});
	}

}

function appscr61FromJSON(i){	
	var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	var houseHoldname = getNameByPersonId(i);
	$('.nameOfHouseHold').html(""+houseHoldname);
	$('input[name=docType]:radio:checked').prop("checked", false);
	$('input[name=documentType]:radio:checked').prop("checked", false);
	$('input[name=naturalizedCitizenshipIndicator]:radio:checked').prop("checked", false);
	$('input[name=livesInUS]:radio:checked').prop("checked", false);
	$('input[name=honourably-veteran]:radio:checked').prop("checked", false);	 
	$(".immigrationDocType").hide();
	var temp = householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator;
	if(temp == false){
		  //$('input[name=UScitizen]:radio').prop('checked', false);
		  $("#UScitizenIndicatorNo").prop("checked", true);
		  isUSCitizen();
	}else if(temp == true){
		  //$('input[name=UScitizen]:radio').prop('checked', true); suneel 05/09/2014
		  $("#UScitizenIndicatorYes").prop("checked", true);
		  isUSCitizen();
		  	
	}else if(temp == null){
			$("#UScitizenIndicatorYes").prop("checked", true);
			isUSCitizen();
			$('input[name=UScitizen]:radio:checked').prop("checked", false);
	}	 
			
	if(householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false){
			//$('input[name=naturalizedCitizenshipIndicator]:radio').prop('checked', false);
		$("#naturalizedCitizenshipIndicatorNo").prop("checked", true);				 
	}else if(householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == true){
				// $('input[name=naturalizedCitizenshipIndicator]:radio').prop('checked', true); // suneel 05/09/2014
		$("#naturalizedCitizenshipIndicatorYes").prop("checked", true);
	}
	isNaturalizedCitizen();
	
	
	var exchangeName = $('#exchangeName').val();

	if(exchangeName === "NMHIX"){
		householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator = true;
		$('#naturalCitizen_eligibleImmigrationStatus').prop('checked',true).prop('disabled',true);
	}else if(exchangeName === "Your Health Idaho"){
		if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true){
			$('#naturalCitizen_eligibleImmigrationStatus').prop('checked',true);
		}else{
			$('#naturalCitizen_eligibleImmigrationStatus').prop('checked',false);
		}
		
	}
	

	
	
	showOrHideNaturalCitizenImmigrationStatus();		 
			/*Retreving Data for immigration depending upon Document Type*/
			
			 if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I551Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I551Indicator =="true")
			 	{
				 	$("#permcarddetails").prop("checked", true); // suneel 05/09/2014
				 	$("#permcarddetailsdiv").show(); // Suneel 05/09/2014
				 	
				 	$('#permcarddetailsAlignNumber').val("");
				 	$('#permcarddetailsppcardno').val("");
				 	$('#permcarddetailsdateOfExp').val("");
				 	
					$('#permcarddetailsAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
					$('#permcarddetailsppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
					$('#permcarddetailsdateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));

			 	}
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].TemporaryI551StampIndicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].TemporaryI551StampIndicator =="true")
			 	{
				 	$("#tempcarddetails").prop("checked", true); // suneel 05/09/2014
				 	$("#tempcarddetailsdiv").show(); // Suneel 05/09/2014
				 	
				 	$('#tempcarddetailsAlignNumber').val("");
				 	$('#tempcarddetailsppcardno').val("");
				 	$('#tempcarddetailsPassportexpdate').val("");
				 	
					$('#tempcarddetailsAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
					$('#tempcarddetailsppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
					
					$('#tempcarddetailsCounty').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance);
					
					$('#tempcarddetailsPassportexpdate').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
			 	}
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].MachineReadableVisaIndicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].MachineReadableVisaIndicator =="true")
			 	{
				 	$("#machinecarddetails").prop("checked", true); // suneel 05/09/2014
				 	$("#machinecarddetailsdiv").show(); // Suneel 05/09/2014
				 	
				 	$('#machinecarddetailsAlignNumber').val("");
				 	$('#machinecarddetailsppcardno').val("");
				 	$('#machinecarddetailsppvisano').val("");
				 	
					$('#machinecarddetailsAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
					$('#machinecarddetailsppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
					$('#machinecarddetailsppvisano').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.visaNumber);
					$('#machinecarddetailsCounty').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance);
					$('#machinecarddetailsdateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));					
				}

			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I766Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I766Indicator =="true")
			 	{
				 	$("#empauthcarddetails").prop("checked", true); // suneel 05/09/2014
				 	$("#empauthcarddetailsdiv").show(); // Suneel 05/09/2014
				 	
					$('#empauthcarddetailsAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
					$('#empauthcarddetailsppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
					$('#empauthcarddetailsdateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				 }
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I94Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I94Indicator =="true")
			 	{
				 	$("#arrivaldeparturerecord").prop("checked", true); // suneel 05/09/2014
				 	$("#arrivaldeparturerecorddiv").show(); // Suneel 05/09/2014
				 	
				  $('#arrivaldeparturerecordI94no').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				  $('#arrivaldeparturerecordSevisID').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				  $('#arrivaldeparturerecorddateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				 }
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I94InPassportIndicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I94InPassportIndicator =="true")
			 	{
				 	$("#arrivaldeprecordForeign").prop("checked", true); // suneel 05/09/2014
				 	$("#arrivaldeprecordForeigndiv").show(); // Suneel 05/09/2014
				 	
				  $('#arrivaldeprecordForeignI94no').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				  $('#arrivaldeprecordForeignppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
				  $('#arrivaldeprecordForeignCounty').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance);
				  $('#arrivaldeprecordForeigndateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				  $('#arrivaldeprecordForeignppvisano').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.visaNumber);
				  $('#arrivaldeprecordForeignSevisID').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				 }
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I797Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I797Indicator =="true")
			 	{
				 	$('#NoticeOfAction').prop("checked", true); // suneel 05/09/2014
				 	$('#NoticeOfActiondiv').show(); // Suneel 05/09/2014

				 	$('#NoticeOfActionAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
				 	$('#NoticeOfActionI94').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				 }
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].UnexpiredForeignPassportIndicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].UnexpiredForeignPassportIndicator =="true")
			 	{
				 
				 	$('#ForeignppI94').prop("checked", true); // suneel 05/09/2014
				 	$('#ForeignppI94div').show(); // Suneel 05/09/2014
				 
				  $('#ForeignppI94AlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				  $('#ForeignppI94ppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
				  $('#ForeignppI94County').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance);
				  $('#ForeignppI94dateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				  $('#ForeignppI94SevisID').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				 }
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I327Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I327Indicator =="true")
			 	{
				 	$('#rentrypermit').prop("checked", true); // suneel 05/09/2014
				 	$('#rentrypermitdiv').show(); // Suneel 05/09/2014
				 
				  $('#rentrypermitAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
				  $('#rentrypermitdateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				 }
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I571Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I571Indicator =="true")
			 	{
				 	$('#refugeetraveldocI-571').prop("checked", true); // suneel 05/09/2014
				 	$('#refugeetraveldocI-571div').show(); // Suneel 05/09/2014
				 
				  $('#refugeetraveldocI-571AlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
				  $('#refugeetraveldocI-571dateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				 }
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I20Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I20Indicator =="true")
			 	{
				 	$('#certificatenonimmigrantF1-Student-I20').prop("checked", true); // suneel 05/09/2014
				 	$('#certificatenonimmigrantF1-Student-I20div').show(); // Suneel 05/09/2014
				 
				  $('#certificatenonimmigrantF1-Student-I20SevisID').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				  $('#certificatenonimmigrantF1-Student-I20I94no').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				  $('#certificatenonimmigrantF1-Student-I20ppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
				  $('#certificatenonimmigrantF1-Student-I20dateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				  $('#certificatenonimmigrantF1-Student-I20County').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance);
				 }
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].DS2019Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].DS2019Indicator =="true")
			 	{
				 	$('#certificatenonimmigrantJ1-Student-DS2019').prop("checked", true); // suneel 05/09/2014
				 	$('#certificatenonimmigrantJ1-Student-DS2019div').show(); // Suneel 05/09/2014
				 
				  $('#certificatenonimmigrantJ1-Student-DS2019SevisID').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				  $('#certificatenonimmigrantJ1-Student-DS2019I94no').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				  $('#certificatenonimmigrantJ1-Student-DS2019ppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
				  $('#certificatenonimmigrantJ1-Student-DS2019dateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				 }
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].OtherDocumentTypeIndicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].OtherDocumentTypeIndicator =="true")
			 	{
				 $('#docType_other').prop("checked", false); // suneel 05/09/2014
				 	$('#otherDocStatusOnPage62P1').hide();
				 	
				 	$('#docType_other').prop("checked", true); // suneel 05/09/2014
				 	$('#otherDocStatusOnPage62P1').show(); // Suneel 05/09/2014
				 
				 	$('#DocAlignNumber').val("");
				 	$('#DocI-94Number').val("");
				 	$('#passportDocNumber').val("");
				 	$('#passportExpDate').val("");
				 	$('#sevisIDNumber').val("");
				 	$('#documentDescription').val("");
				 	
				  $('#DocAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
				  $('#DocI-94Number').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				  $('#passportDocNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
				  $('#passportExpDate').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				  $('#sevisIDNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				  $('#documentDescription').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentDescription);
				 }
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected == "NaturalizationCertificate")
			 	{
				 	$('#naturalizedCitizenNaturalizedIndicator').prop("checked", true); 
				 	$('.appscr62Part1HideandShowFillingDetails').hide();
				 	
				 	
				 	$('#divNaturalizationCertificate').show(); 
				 
				 	$('#DocAlignNumber').val("");
				 	$('#DocI-94Number').val("");
				 	$('#passportDocNumber').val("");
				 	$('#passportExpDate').val("");
				 	$('#sevisIDNumber').val("");
				 	$('#documentDescription').val("");
				 	$('#naturalizationAlignNumber').val("");
				 	$('#naturalizationCertificateNumber').val("");
				 	
				 	$('#naturalizationAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
				 	$('#naturalizationCertificateNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				 	
				  
				 }
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected == "CitizenshipCertificate")
			 	{
				 	$('#naturalizedCitizenNaturalizedIndicator2').prop("checked", true); 
				 	$('.appscr62Part1HideandShowFillingDetails').hide();			 	
				 	
				 	$('#divCitizenshipCertificate').show(); 
				 
				 	$('#DocAlignNumber').val("");
				 	$('#DocI-94Number').val("");
				 	$('#passportDocNumber').val("");
				 	$('#passportExpDate').val("");
				 	$('#sevisIDNumber').val("");
				 	$('#documentDescription').val("");
				 	$('#citizenshipAlignNumber').val("");
				 	$('#appscr62p1citizenshipCertificateNumber').val("");
				 	
				 	$('#citizenshipAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
				 	$('#appscr62p1citizenshipCertificateNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				 	
				  
				 }
			 else {				 
				 $('#otherDocStatusOnPage62P1').hide(); // Suneel 05/09/2014
			 }


			 // living in US since 1962
			 var livedInUs = householdMemberObj.citizenshipImmigrationStatus.livedIntheUSSince1996Indicator; 
			 if(livedInUs == true) {
				 $('#62Part_livesInUS').prop("checked", true); // suneel 05/09/2014
				 
			 }
			 else if(livedInUs == false) {
				 $('#62Part_livesInUSNo').prop("checked", true); // suneel 05/09/2014
			 }
			 
			 var honourableVeteran = householdMemberObj.citizenshipImmigrationStatus.honorablyDischargedOrActiveDutyMilitaryMemberIndicator;
			if(honourableVeteran == true || honourableVeteran == 'true') {				
				$('#62Part_honourably-veteran').prop("checked", true); // suneel 05/09/2014
			} else if(honourableVeteran == false || honourableVeteran == 'false') {
				$('#62Part_honourably-veteranNo').prop("checked", true); // suneel 05/09/2014
			}

			// Other documents
			$('#haveTheseDocuments').show();
			$('#otherDocORR').prop('checked', false);
			$('#otherDocORR-under18').prop('checked', false);
			$('#Cuban-or-HaitianEntrant').prop('checked', false);
			$('#docRemoval').prop('checked', false);
			$('#adminOrderbyDeptofHomelanSecurity').prop('checked', false);
			$('#americanSamaoresident').prop('checked', false);
			
			if(householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.ORRCertificationIndicator){
				$('#otherDocORR').prop('checked', true);
			}
			
			if(householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.ORREligibilityLetterIndicator){
				$('#otherDocORR-under18').prop('checked', true);
			}
			if(householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.CubanHaitianEntrantIndicator){
				$('#Cuban-or-HaitianEntrant').prop('checked', true);
			}
			if(householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.WithholdingOfRemovalIndicator){
				$('#docRemoval').prop('checked', true);
			}
			if(householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.AmericanSamoanIndicator){
				$('#americanSamaoresident').prop('checked', true);
			}
			if(householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.StayOfRemovalIndicator){
				$('#adminOrderbyDeptofHomelanSecurity').prop('checked', true);
			}
			

		// Name different on document
			var nameSameonDocument = householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameSameOnDocumentIndicator;
			$('#sameNameInfoPage62P2').show(); // Suneel 05/09/2014
			
			//$('#62Part2_UScitizenYes').prop("checked", true); // suneel 05/09/2014
			if(nameSameonDocument == true) {
				$('#62Part2_UScitizenYes').prop("checked", true); // suneel 05/09/2014
				
				$("#documentFirstName").val("");
				$("#documentMiddleName").val("");
				$("#documentLastName").val("");
				$("#documentSuffix").val("");
				
			}
			else if(nameSameonDocument == false){
				$('#62Part2_UScitizen').prop("checked", true); // suneel 05/09/2014
				
				$("#documentFirstName").val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.firstName);
				$("#documentMiddleName").val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.middleName);
				$("#documentLastName").val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.lastName);
				$("#documentSuffix").val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.suffix);
				
			}else{
				$('input[name=UScitizen62]').prop("checked", false);
			}

			displayOrHideSameNameInfoPage62();

		
	
}

function appscr63FromJSON(i){	
	
	
	var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	$("input[name=appscr63AtLeastOneChildUnder19Indicator]").prop("checked",false);
	if(householdMemberObj.parentCaretakerRelatives.mainCaretakerOfChildIndicator == true){
		$("#appscr63AtLeastOneChildUnder19Yes").prop("checked", true);	 	
	 	parentCareTakerHideShow();
	} else if(householdMemberObj.parentCaretakerRelatives.mainCaretakerOfChildIndicator == false) {
		$("#appscr63AtLeastOneChildUnder19No").prop("checked", true);	 	
	 	parentCareTakerHideShow();
	}
	
	
	//householdMemberObj.parentCaretakerRelatives.personId[0].personid =1 ;	
	$("input[name=takeCareOf]").prop("checked",false);
	if(householdMemberObj.parentCaretakerRelatives.anotherChildIndicator == true){
		$("#takeCareOf_anotherChild").prop("checked", true);
		method4AnotherChildCheckBox();
	 	
	}
	
	$('#parent-or-caretaker-firstName').val(householdMemberObj.parentCaretakerRelatives.name.firstName);
	$('#parent-or-caretaker-middleName').val(householdMemberObj.parentCaretakerRelatives.name.middleName);
	$('#parent-or-caretaker-lastName').val(householdMemberObj.parentCaretakerRelatives.name.lastName);
	$('#parent-or-caretaker-suffix').val(householdMemberObj.parentCaretakerRelatives.name.suffix);
	$('#parent-or-caretaker-dob').val(UIDateformat(householdMemberObj.parentCaretakerRelatives.dateOfBirth));
	$('#applicant-relationship').val(householdMemberObj.parentCaretakerRelatives.relationship);
	updateFullNameOfOtherChild();
	$("input[name=birthOrAdoptiveParents]").prop("checked",false);
	if(householdMemberObj.parentCaretakerRelatives.liveWithAdoptiveParentsIndicator == true){
		$("#appscr63BirthOrAdoptiveParentsYes").prop("checked", true);
	} else if (householdMemberObj.parentCaretakerRelatives.liveWithAdoptiveParentsIndicator == false){
		$("#appscr63BirthOrAdoptiveParentsNo").prop("checked", true);
	}
	
}

function appscr66FromMongodbJson(){
	
	/*pageValidation();*/
	
	/*if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){ 
		return;
	}*/
	
	var disability;
	var i_cover_adoption;
	var i_nativeAmericon;
	var i_cover_foster;
	var i_deceased;
	var isPregnant;
	var fatusCount;
	var isPersonGettingHealthCare;
	var noneOftheseDisabilityIndicator;
	
	var noAmericanIndianAlaskaNative = true;
	
	noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;		

	for ( var i = 1; i <= noOfHouseHoldmember; i++) {
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];		
		disability = householdMemberObj.disabilityIndicator;
		i_nativeAmericon = householdMemberObj.specialCircumstances.americanIndianAlaskaNativeIndicator;
		i_cover_foster = householdMemberObj.specialCircumstances.fosterChild;
		isPregnant = householdMemberObj.specialCircumstances.pregnantIndicator;
		
		if(i_nativeAmericon && noAmericanIndianAlaskaNative){
			noAmericanIndianAlaskaNative = false;
		}
		
		try{
			
			if(disability == true)
				document.getElementById('disability'+i).checked = true;
			else
				document.getElementById('disability'+i).checked = false;

			if(disability == true && disabilityFlag)
				disabilityFlag = false;

			if(i_cover_adoption == true)
				document.getElementById('assistanceServices'+i).checked = true;
			else
				document.getElementById('assistanceServices'+i).checked = false;

			if(i_cover_adoption == true && i_cover_adoptionFlag)
				i_cover_adoptionFlag = false;

			if(i_nativeAmericon == true){
				document.getElementById('native'+i).checked = true;
				nativeOnChange();
				if(householdMemberObj.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator == true){
					$('#AmericonIndianQuestionRadioYes'+i).prop("checked", true);
					checkAmericonIndianQuestion(i);
					$('#americonIndianQuestionSelect'+i).val(householdMemberObj.americanIndianAlaskaNative.state);
					objTribdNameDropdown = $('#tribeName'+i);
					tribeCode = householdMemberObj.americanIndianAlaskaNative.tribeName;
					getAmericanAlaskaTribeList(householdMemberObj.americanIndianAlaskaNative.state,objTribdNameDropdown);																	
				}else{
					$('#AmericonIndianQuestionRadioNo'+i).prop("checked", true);
				}
			}
			else
				document.getElementById('native'+i).checked = false;

			if(i_nativeAmericon == true && i_nativeAmericonFlag)
				i_nativeAmericonFlag = false;
			else 
				i_nativeAmericonFlag = true;
			
			if(i_cover_foster == true)
				document.getElementById('personfosterCare'+i).checked = true;
			else
				document.getElementById('personfosterCare'+i).checked = false;

			if(i_cover_foster == true && i_cover_fosterFlag)
				i_cover_fosterFlag = false;

			if(i_deceased == true)
				document.getElementById('deceased'+i).checked = true;
			else
				document.getElementById('deceased'+i).checked = false;

			if(i_deceased == true && i_deceasedFlag)
				i_deceasedFlag = false;
			
			
			var strGender = householdMemberObj.gender;
			if(strGender.toLowerCase() == "female")
			{
				if(isPregnant == true)
				{
					document.getElementById('pregnancy'+i).checked = true;
					//getExpectedInPregnancy();
					//$('#number-of-babies'+i).val(householdMemberObj.specialCircumstances.numberBabiesExpectedInPregnancy);
				}
				else
				{
					if(document.getElementById('pregnancy'+i)!=null){
						document.getElementById('pregnancy'+i).checked = false;
					}

				}
			}


			if(isPregnant == true && isPregnantFlag)
				isPregnantFlag = false;

			if(isPersonGettingHealthCare == true)
				$('#isPersonGettingHealthCareYes'+i).prop("checked", true);
			else
				$('#isPersonGettingHealthCareNo'+i).prop("checked", true);
			
		}catch(e){
			//console.log(e);
		}
		
		
	/*	if(householdMemberObj.healthCoverage.currentOtherInsurance.medicareEligibleIndicator == true){
			$('#appscr78Medicare'+i).prop("checked", true);
		}
		
		if(householdMemberObj.healthCoverage.currentOtherInsurance.tricareEligibleIndicator == true){
			$('#appscr78Tricare'+i).prop("checked", true);
		}
		
		if(householdMemberObj.healthCoverage.currentOtherInsurance.peaceCorpsIndicator == true){
			$('#appscr78PeaceCorps'+i).prop("checked", true);
		}
		
		if(householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramIndicator == true){
			$('#appscr78OtherStateFHBP'+i).prop("checked", true);
		}
		
		if(householdMemberObj.healthCoverage.currentOtherInsurance.nonofTheAbove == true){
			$('#appscr78None'+i).prop("checked", true);
		}*/
		
	}
	
	$('#disabilityNoneOfThese').prop('checked',false);
	$('#assistanceServicesNoneOfThese').prop('checked',false);
	$('#nativeNoneOfThese').prop('checked',false);
	$('#personfosterCareNoneOfThese').prop('checked',false);
	$('#deceasedPersonNoneOfThese').prop('checked',false);
	
	/*
	document.getElementById('disabilityNoneOfThese').checked = false;
	document.getElementById('assistanceServicesNoneOfThese').checked = false;
	document.getElementById('nativeNoneOfThese').checked = false;
	document.getElementById('personfosterCareNoneOfThese').checked = false;
	document.getElementById('deceasedPersonNoneOfThese').checked = false;
	*/
	
	
/*	if(noneOftheseDisabilityIndicator){ // have to check this for none of the above disability javed
		//document.getElementById('disabilityNoneOfThese').checked = true;
		$('#disabilityNoneOfThese').prop('checked',true);
	}
	if(noneOftheseAssistanceServices){
		//document.getElementById('assistanceServicesNoneOfThese').checked = true;
		$('#assistanceServicesNoneOfThese').prop('checked',true)
	}
	if(noneOftheseAmericanIndianAlaskaNativeIndicator){
		//document.getElementById('nativeNoneOfThese').checked = true;
		$('#nativeNoneOfThese').prop('checked',true);
	}
	if(noneOftheseEverInFosterCareIndicator){
		//document.getElementById('personfosterCareNoneOfThese').checked = true;
		$('#personfosterCareNoneOfThese').prop('checked',true);
	}
	if(noneoftheseDeceased){
		//document.getElementById('deceasedPersonNoneOfThese').checked = true;
		$('#deceasedPersonNoneOfThese').prop('checked',true);
	}	
	getExpectedInPregnancy();
	if(noneofthesePregnancy){
		//document.getElementById('pregnancyNoneOfThese').checked = true;
		$('#pregnancyNoneOfThese').prop('checked',true);
	}*/
	
	
	
	//Is XXX eligible for health coverage from any of the following? Select even if bb bbb isnt currently enrolled:
	var noOfHouseHold = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	
	for(var i=1;i<=noOfHouseHold;i++){
		var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		
		if(householdMemberObj.healthCoverage.otherInsuranceIndicator === false){
			$("#appscr78None"+i).prop("checked",true);
		}else if(householdMemberObj.healthCoverage.otherInsuranceIndicator === true){
			$("input:radio[name=fedHealth"+ i +"]").each(function(){
				if($(this).val() == householdMemberObj.healthCoverage.currentOtherInsurance.currentOtherInsuranceSelected){
					$(this).prop("checked",true);
				}
				
				if(householdMemberObj.healthCoverage.currentOtherInsurance.currentOtherInsuranceSelected == "Other"){
					otherInsuranceCheckbox(i);
					$("#appscr78TypeOfProgram" + i).val(householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramType);
					$("#appscr78NameOfProgram" + i).val(householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramName);
				}
			});
		}
		
	}
	/*if (haveData == true) {
		appscr66NoneOfThese();
	}*/
}

function appscr85FromMongodbJSON(){
	//agreement
	var parimaryContactObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
	if(webJson.singleStreamlinedApplication.consentAgreement === true){
		$('#useIncomeDataIndicatorYes').prop('checked',true);
		updateTaxReturnPeriod();
	}else if(webJson.singleStreamlinedApplication.consentAgreement === false){
		$('#useIncomeDataIndicatorNo').prop('checked',true);
		updateTaxReturnPeriod();
		$('#taxReturnPeriod' + webJson.singleStreamlinedApplication.numberOfYearsAgreed).prop('checked',true);
		
	}
}

	function UIDateformat(dt){
		if(trimSpaces(dt)=="") return dt;
		if(dt==undefined) return "";
		var formattedDate = new Date(dt);
		var dd = formattedDate.getDate();
		var mm =  formattedDate.getMonth();
		mm += 1;  // JavaScript months are 0-11
		var yyyy = formattedDate.getFullYear();
		if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm}
		formatteddate = mm + "/" + dd + "/" + yyyy;
		return formatteddate;
	
	}

	function DBDateformat(dt){
		if(trimSpaces(dt)=="") return null;
		if(dt==undefined) return null;
		
		var monthNames = new Array("January", "February", "March", 
				"April", "May", "June", "July", "August", "September", 
				"October", "November", "December");
		
		var MonthNameShortLength = 3; //0 for full month name

		var arrDate = dt.split('/');
		var monthName = "";
		var monthNumber = arrDate[0].replace(/^0+/, '');		
		if (MonthNameShortLength == 0){
			month = monthNames[monthNumber-1];
		} else {
			month = monthNames[monthNumber-1].substring(0, MonthNameShortLength);
		}
		var formattedDate = month + " " +  arrDate[1] + ", " + arrDate[2] + " 12:00:00 AM" ;		
		return formattedDate;		

	}
	
	function getDecimalNumber(num){
		if(num == null || trimSpaces(num)=="" ) return null;	
		
		if(num == 0 ) return 0; 
		
		return parseFloat(Number(num).toFixed(2));
		 
	}
	
	function getIntegerNumber(num){
		if(num == null || trimSpaces(num)=="" ) return null;	
		
		if(num == 0 ) return 0; 
		
		if(num == parseInt(num)){
			return parseInt(num) ;
		}else {
			return null;
		}
		
		
		 
	}
	
	function Moneyformat(num) {
		if(num == null || trimSpaces(num)=="" ) return num;			
		if(num == 0 ) return 0;
	    var p = Number(num).toFixed(2).split(".");	    
	    return p[0].split("").reduceRight(function(acc, num, i, orig) {
	        if ("-" === num && 0 === i) {
	            return num + acc;
	        }
	        var pos = orig.length - i - 1
	        return  num + (pos && !(pos % 3) ? "," : "") + acc;
	    }, "") + (p[1] ? "." + p[1] : "");
	}
	
	
	function checkHelpType(){
		var helpType = webJson.singleStreamlinedApplication.helpType;
		if(helpType == "BROKER"){
			$("#brokerDiv, #authorizedRepresentativeDiv").show();
		}else if(helpType == "ASSISTER"){
			$("#guideDiv, #authorizedRepresentativeDiv").show();
		}else{
			$("#helpQuestionDiv").show();
		}
	}	
