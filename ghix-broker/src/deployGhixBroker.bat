@ECHO OFF
rem set /p projPath="Enter your Project Path : "
rem set /p jbossPath="Enter your JBOSS Path : "
echo.
cls

set projPath=E:\VIMO\NewWorkspace
set jbossPath=E:\JBoss\jboss-eap-6.0

set dirPath=%projPath%\ghix\ghix-broker
set targetPath=%projPath%\ghix\ghix-broker\target
set deployPath=%jbossPath%\standalone\deployments

cd /D %dirPath%
call mvn -DskipTests clean install

copy %targetPath%\ghix-broker.war %deployPath%

pause