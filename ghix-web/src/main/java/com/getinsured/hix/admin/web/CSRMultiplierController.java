package com.getinsured.hix.admin.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.getinsured.hix.model.PlanCsrMultiplier;
import com.getinsured.hix.planmgmt.service.CSRMultiplierService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;

@Controller
public class CSRMultiplierController {

	@Autowired private CSRMultiplierService csrMultiplierService;
	private static final String CSR_MULTIPLIERS ="csrMultipliers"; 
	
	@RequestMapping(value = "/admin/planmgmt/csrmultiplier/manage", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_QHP_PLANS')")
	public String manageCSRMultiplier(Model model) {
		try{
			List<PlanCsrMultiplier>	csrMultipliers = csrMultiplierService.getCSRMultiplierList();
			model.addAttribute(CSR_MULTIPLIERS, csrMultipliers);
		}catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.MANAGE_CSR_MULTIPLIER, null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/planmgmt/csrmultiplier/manage";
	}
	
	@RequestMapping(value = "/admin/planmgmt/csrmultiplier/edit", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_QHP_PLANS')")
	public String editCSRMultiplier(Model model) {
		try{
			List<PlanCsrMultiplier>	csrMultipliers = csrMultiplierService.getCSRMultiplierList();
			model.addAttribute(CSR_MULTIPLIERS, csrMultipliers);
		}catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_CSR_MULTIPLIER, null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/planmgmt/csrmultiplier/edit";
	}
	
	@RequestMapping(value = "/admin/planmgmt/csrmultiplier/update", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'EDIT_PLAN_DETAILS')")
	public String updateCSRMultiplier(Model model,@RequestParam("csrMultiplier") String csrMultiplier) {
		try{
			csrMultiplierService.updateCSRMultiplier(csrMultiplier);
		}catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.UPDATE_CSR_MULTIPLIER, null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "redirect:/admin/planmgmt/csrmultiplier/manage";
	}
	
	@RequestMapping(value = "/admin/planmgmt/csrmultiplier/history", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'EDIT_PLAN_DETAILS')")
	public String cSRMultiplierHistory(Model model,HttpServletRequest request) {
		try{
			List<Map<String, Object>> csrMultipliers = csrMultiplierService.getCSRMultiplierHistory();
			model.addAttribute(CSR_MULTIPLIERS, csrMultipliers);
			model.addAttribute("pageSize", GhixConstants.PAGE_SIZE);
			model.addAttribute("resultSize", csrMultipliers.size());
		}catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.CSR_MULTIPLIER_HISTORY, null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/planmgmt/csrmultiplier/history";
	}
}
