var imgPath = '<img src="/hix/resources/images/requiredAsterix.png" width=\"10\" height=\"10\" alt=\"\"/> <span class="aria-hidden">required</span>';
var Dependent = {
	line_index:    1,
	dependent_row:
		'<div id="employee_dependent_CN" class="gutter10">\n' +
		  '<h4 class="lightgray"> DEPENDENT_TYPE <button type="button" class="close" data-dismiss="modal" onclick="Dependent.closeBlock(CN);"><span class="aria-hidden">Close DEPENDENT_TYPE</span> <span aria-hidden="true">×</span></button> </h4>\n' +
		  '<input type="hidden" id="dependent_CN_type" name="employeeDetails[CN].type" value="" />\n' +
		  '<input type="hidden" id="dependent_CN_id" name="employeeDetails[CN].id" value="" />\n' +
		      '<div class="control-group">\n' +
				'<label class="control-label" for="dependent_CN_firstName">First Name'+imgPath+'</label>\n' +
				'<div class="controls">\n' +
					'<input type="text" name="employeeDetails[CN].firstName" id="dependent_CN_firstName"/>\n' +
					'<span id="dependent_CN_firstName_error"></span>\n' +
				'</div>\n' +
			'</div>\n' +
			'<div class="control-group">\n' +
				'<label class="control-label" for="dependent_CN_lastName">Last Name'+imgPath+'</label>\n' +
				'<div class="controls">\n' +
					'<input type="text" id="dependent_CN_lastName" name="employeeDetails[CN].lastName" />\n' +
					'<span id="dependent_CN_lastName_error"></span>\n' +
				'</div>\n' +
			'</div>\n' +
			'<div class="control-group">\n' +
				'<label class="control-label" for="dependent_CN_dob">Date of Birth'+imgPath+'</label>\n' +
				'<div class="controls" id="dob_CN_control">\n' +
					'<input title="MM/DD/YYYY" class="datepick input-small"  placeholder="MM/DD/YYYY" id="dependent_CN_dob" name="employeeDetails[CN].dob" />\n' +
					'<span id="dependent_CN_dob_error"></span>\n' +
				'</div>\n' +
			'</div>\n' +
			'<div class="control-group">\n' +
				'<label class="control-label" for="dependent_CN_location">Home Address</label>\n' +
				'<div class="controls">\n' +
					'<select class="selecthomeaddress" name="employeeDetails[CN].location.id" id="dependent_CN_location"></select>\n' +
					'<a id="worksiteLink" tabindex="0" class="gutter10" onclick="javascript:Dependent.showHomeAddress(\'dependent_CN_location\')"><span><i class="icon-plus-sign"></i> Add new Address</span></a>\n' +
				'</div>\n' +
			'</div>\n' +
		'</div>\n',
		
     add_dependent: function(id,type,imagepath, firstName, lastName, dob, locationid, location){
		var row = this.dependent_row;
		row = row.replace(/CN/g, this.line_index);
		row = row.replace(/DEPENDENT_TYPE/g,type);
		if(type == "SPOUSE"){
			$('#employeeSpouseDiv').append(row);
		}else{
			$('#employeeChildDiv').append(row);
			var childsCount1 = $('#childCount').val();
			childsCount1++;
			$('#childCount').val(childsCount1);
		}
		$('#dependent_'+this.line_index+'_dob').datepicker({
			showOn : "button",
			buttonImage : imagepath,
			buttonImageOnly : true
		});
		$('#dependent_'+this.line_index+'_type').val(type);
		$('#dependent_'+this.line_index+'_id').val(id);
		
		if( $('#dependent_'+this.line_index+'_type').val() == "SPOUSE"){
			$("#buttonSpouse").hide();
		}
		
		jQuery('#dependent_'+this.line_index+'_firstName').rules("add", { required: true,namecheck:true, messages: {
																		 required: "<span><em class='excl'>!</em>Please enter a first name.</span>",
																		 namecheck:   "<span> <em class='excl'>!</em>Please enter a valid first name.</span>"} } );
		jQuery('#dependent_'+this.line_index+'_lastName').rules("add", { required: true,namecheck:true, messages: {
																		 required: "<span><em class='excl'>!</em>Please enter a last name.</span>",
																		 namecheck:   "<span> <em class='excl'>!</em>Please enter a valid last name.</span>"} } );
		jQuery('#dependent_'+this.line_index+'_dob').rules("add", { required: true,  date: true, dobcheck: true,  
																	messages: { required: "<span><em class='excl'>!</em>Please enter a date of birth.</span>",
																				date:  "<span> <em class='excl'>!</em>Please enter a valid date of birth.</span>",
																		        dobcheck:  "<span> <em class='excl'>!</em>Please enter a valid date of birth.</span>"
																			   }
																   } 
															);
		$('#dependent_'+this.line_index+'_firstName').val(firstName);
		$('#dependent_'+this.line_index+'_lastName').val(lastName);
		$('#dependent_'+this.line_index+'_dob').val(dob);
		if(location != ''){
			$('#dependent_'+this.line_index+'_location').prepend('<option value="'+locationid+'" selected>'+location+'</option>');
		}

		for (var key in locationsObj) {
			if(locationid != key){
				$('#dependent_'+this.line_index+'_location').prepend('<option value="'+key+'" >'+locationsObj[key]+'</option>');
			}
		}
		
		//if($("#employeeStatus").val() == 'ENROLLED'){
		var formattedDate = '';
		if(dob != ''){
            var newDate = new Date(dob);
            formattedDate = monthNames[newDate.getMonth()] + ' ' + newDate.getDate() + ', ' + newDate.getFullYear();
		}
			$('#dob_'+this.line_index+'_control').html(formattedDate);
		//}
		
		this.line_index++;
		 $('#footerbuttons').show();
	},
	
	showHomeAddress: function(elementid) {
		Dependent.resetHomeAddress();
		$('#homeaddress').val(elementid);
		$('#worksite').modal({
			keyboard: false,
			show: true
		});
	},
	
	resetHomeAddress: function(){
		$('#homeaddress').val('');
		$('#address2').val('');
		$('#address1').val('');
		$('#city').val('');
		$('#state').find('option[0]').attr("selected",true);
		$('#zip').val('');
	},
	
	closeBlock: function(index){
		if(confirm('Are you sure you want to delete this record ?')){
			if( $('#dependent_'+index+'_type').val() == "SPOUSE" ){
				$("#buttonSpouse").show();
			}
			else{
				var childsCount2 = $('#childCount').val();
				childsCount2--;
				$('#childCount').val(childsCount2);
			}
			$('#employee_dependent_'+index).remove();
			
		}
	}	 
};
