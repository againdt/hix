package com.getinsured.ssap.util;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;

/**
 * @author Yevgen Golubenko
 * @since 9/16/19
 */
public class HouseholdUtilTest {

  @Test
  public void getEligibilityRequestFromApplication_ssap_4_tax_filers() {
    SingleStreamlinedApplication singleStreamlinedApplication = FamilyBuilder.sampleApplicationFromJson("3primary2nonprimary.json");

    List<HouseholdMember> primaryHouseholdMembers = HouseholdUtil.getPrimaryTaxHouseholdMembers(singleStreamlinedApplication);
    Assert.assertNotNull(primaryHouseholdMembers);
    Assert.assertEquals("Primary Household size should be 3", 3, primaryHouseholdMembers.size());

    List<HouseholdMember> nonPrimaryHouseholdMembers = HouseholdUtil.getNonPrimaryTaxHouseholdMembers(singleStreamlinedApplication);
    Assert.assertNotNull(nonPrimaryHouseholdMembers);
    Assert.assertEquals("Primary Household size should be 2", 2, nonPrimaryHouseholdMembers.size());
  }
}
