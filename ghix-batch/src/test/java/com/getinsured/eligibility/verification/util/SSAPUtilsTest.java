package com.getinsured.eligibility.verification.util;

import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.util.SsapBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Unit tests for SSAP Utils.
 *
 * @author Yevgen Golubenko
 * @since 3/27/19
 */
@RunWith(MockitoJUnitRunner.class)
public class SSAPUtilsTest {
  private ObjectMapper om = new ObjectMapper();

  @Test
  public void getSingleStreamlinedApplication() throws Exception {
    SingleStreamlinedApplication application = SsapBuilder.sampleApplicationFromJson("ssap-1-MedicaidChipEligible.json");
    Assert.assertNotNull(application);
    String applicationJson = SsapBuilder.getJsonFromFile("ssap-1-MedicaidChipEligible.json");
    JsonObject singleStreamlinedApplication = SSAPUtils.getSingleStreamlinedApplication(applicationJson);
    Assert.assertNotNull(singleStreamlinedApplication);

    JsonArray householdMembersOld = SSAPUtils.getHouseholdMembers(singleStreamlinedApplication);
    process(householdMembersOld);
    Assert.assertEquals(4, householdMembersOld.size());
    JsonArray filteredHouseholdMembers = SSAPUtils.getFilteredHouseholdMembers(singleStreamlinedApplication);

    Assert.assertNotNull(filteredHouseholdMembers);
    Assert.assertEquals(3, filteredHouseholdMembers.size());
    process(filteredHouseholdMembers);
  }

  public static void process(JsonArray householdMembers) {

    Iterator<JsonElement> iterator = householdMembers.iterator();
    JsonObject householdMember = null;
    System.out.println("\n---");
    while (iterator.hasNext()) {
      householdMember = iterator.next().getAsJsonObject();
      String personId = householdMember.get(SSAPConstants.PERSON_ID).getAsString();
      System.out.println("person id: " + personId);
    }
  }
}
