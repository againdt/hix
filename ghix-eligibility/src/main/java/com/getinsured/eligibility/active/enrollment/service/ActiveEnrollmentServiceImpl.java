package com.getinsured.eligibility.active.enrollment.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.getinsured.eligibility.renewal.util.RenewalConstants;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentByMemberListRequestDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentWithMemberDataDTO;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.RenewalConfiguration;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.repository.EnrollmentReferralRepository;
import com.getinsured.timeshift.util.TSDate;
import com.thoughtworks.xstream.XStream;

@Service("activeEnrollmentService")
public class ActiveEnrollmentServiceImpl implements ActiveEnrollmentService {

	private static final int IDX_ENROLLEMNT_SEVEN = 7;
	private static final String DENTAL_PLAN = "dental";
	public static final String HEALTH_PLAN = "health";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ActiveEnrollmentServiceImpl.class);

	@Autowired private GhixRestTemplate ghixRestTemplate;
	
	@Autowired private EnrollmentReferralRepository enrollmentReferralRepository;
	
	@Autowired
	private GIWSPayloadService giwsPayloadService;
	
	
	private static final int IDX_ENROLLEMNT_ZERO = 0;
	private static final int IDX_ENROLLEMNT_ONE = 1;
	private static final int IDX_ENROLLEMNT_TWO = 2;
	private static final int IDX_ENROLLEMNT_THREE = 3;
	private static final int IDX_ENROLLEMNT_FOUR = 4;
	private static final int IDX_ENROLLEMNT_FIVE = 5;
	private static final int IDX_ENROLLEMNT_SIX = 6;
	

	@Override
	public boolean isEnrollmentActive(Long applicationId, String username) {

		validateIncomingRequest(applicationId, username);

		boolean isEnrollmentActive = false;
		try {
			isEnrollmentActive =  (ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.IS_ACTIVE_HEALTH_ENROLLMENT_FOR_APP_ID,
					username,
					HttpMethod.POST,
					MediaType.APPLICATION_JSON,
					Boolean.class,
					applicationId)).getBody();
		} catch (Exception e) {
			String errorMsg = "Exception occurred while getting Active Enrollment Details for ssap application id - " + applicationId + " - " + ExceptionUtils.getFullStackTrace(e);
			LOGGER.error(errorMsg);
			throw new GIRuntimeException(errorMsg);
		}

		return isEnrollmentActive;
	}
	
	
	@Override
	public boolean isEnrollmentActiveCheck(Long applicationId, String username) {

		validateIncomingRequest(applicationId, username);
		LOGGER.info("Calling isEnrollmentActiveCheck : " + applicationId);
		boolean isEnrollmentActive = false;
		try {
			isEnrollmentActive =  (ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.IS_ACTIVE_HEALTH_ENROLLMENT_FOR_APP_ID_NO_LOOKBACK,
					username,
					HttpMethod.POST,
					MediaType.APPLICATION_JSON,
					Boolean.class,
					applicationId)).getBody();
		} catch (Exception e) {
			String errorMsg = "Exception occurred while getting Active Enrollment Details for ssap application id - " + applicationId + " - " + ExceptionUtils.getFullStackTrace(e);
			LOGGER.error(errorMsg);
			throw new GIRuntimeException(errorMsg);
		}
		LOGGER.info("isEnrollmentActiveCheck for " + applicationId + " = " + isEnrollmentActive);
		return isEnrollmentActive;
	}
	
	@Override
	public boolean isConfirmedEnrollment(Long applicationId, String username)
			throws GIException {
		return isConfirmedEnrollment(applicationId, getValidActiveEnrollmentStatusList(), username);
	}
	

	@Override
	public boolean isConfirmedEnrollment(Long applicationId, List<String> validEnrollmentStatus, 
			String username) throws GIException {
		return isConfirmedEnrollment(this.getEnrollmentForApplication(applicationId, username), validEnrollmentStatus);
	}


	@Override
	public EnrollmentDataDTO getEnrollmentForApplication(Long applicationId, String username) throws GIException {
		validateIncomingRequest(applicationId, username);
		List<EnrollmentDataDTO> enrollmentDataDTOs = getEnrollmentDataBySsapApplicationId(applicationId);
		if(enrollmentDataDTOs != null) {
			if(enrollmentDataDTOs.size()>1){
				Collections.sort(enrollmentDataDTOs, new Comparator<EnrollmentDataDTO>(){
				     public int compare(EnrollmentDataDTO enrollmentId1, EnrollmentDataDTO enrollmentId2){
				         return Long.valueOf(enrollmentId2.getCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getCreationTimestamp().getTime()));
				     }
				});
			}
			
			return enrollmentDataDTOs.get(0);
		}
		
		return null;
		
	}
	
	@Override
	public List<EnrollmentDataDTO> getEnrollmentDataBySsapApplicationId(Long ssapApplicationId) {
		List<EnrollmentDataDTO> enrollmentDataDTOList = null;
		List<Object[]> enrollmentDataList = enrollmentReferralRepository.getEnrollmentDataBySsapApplicationId(ssapApplicationId);
		if (enrollmentDataList != null && !enrollmentDataList.isEmpty()) {
			enrollmentDataDTOList = new ArrayList<EnrollmentDataDTO>();
			for (Object[] enrollmentData : enrollmentDataList) {
				EnrollmentDataDTO enrollmentDataDTO = new EnrollmentDataDTO();
				enrollmentDataDTO.setEnrollmentId((Integer) enrollmentData[IDX_ENROLLEMNT_ZERO]);
				enrollmentDataDTO.setEnrollmentStatus((String) enrollmentData[IDX_ENROLLEMNT_ONE]);
				enrollmentDataDTO.setPlanId((Integer) enrollmentData[IDX_ENROLLEMNT_TWO]);
				enrollmentDataDTO.setCMSPlanID((String) enrollmentData[IDX_ENROLLEMNT_THREE]);
				enrollmentDataDTO.setEnrollmentBenefitEffectiveStartDate((Date) enrollmentData[IDX_ENROLLEMNT_FOUR]);
				enrollmentDataDTO.setEnrollmentBenefitEffectiveEndDate((Date) enrollmentData[IDX_ENROLLEMNT_FIVE]);
				enrollmentDataDTO.setCreationTimestamp((Date) enrollmentData[IDX_ENROLLEMNT_SIX]);
				enrollmentDataDTO.setAptcAmount((Float)enrollmentData[IDX_ENROLLEMNT_SEVEN]);
				enrollmentDataDTOList.add(enrollmentDataDTO);
			}
		}
		return enrollmentDataDTOList;

	}
	
	@Override
	public EnrollmentShopDTO getLatestEnrollmentApplication(Long applicationId, String username) throws GIException {
		validateIncomingRequest(applicationId, username);
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		List<EnrollmentShopDTO> enrollmentShopDTOs = null;
		EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
		enrolleeRequest.setSsapApplicationId(applicationId);
		//EnrolleeResponse enrolleeResponse = enrolleeService.findEnrolleeByApplicationid(enrolleeRequest);
		ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID, username, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrolleeRequest));
		EnrolleeResponse enrolleeResponse = (EnrolleeResponse) xstream.fromXML(response.getBody());
		
		enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();

		List<EnrollmentShopDTO> enrollmentForHealth = new ArrayList<EnrollmentShopDTO>();
		List<EnrollmentShopDTO> enrollmentForDental = new ArrayList<EnrollmentShopDTO>();
		if(enrollmentShopDTOs != null) {
			for (EnrollmentShopDTO enrollmentShopDto : enrollmentShopDTOs) {
				if(enrollmentShopDto.getPlanType().equalsIgnoreCase(HEALTH_PLAN)){
					enrollmentForHealth.add(enrollmentShopDto);
				}else if(enrollmentShopDto.getPlanType().equalsIgnoreCase(DENTAL_PLAN)){
					enrollmentForDental.add(enrollmentShopDto);
				}
			}
				
			if(enrollmentForHealth.size()>1){
				Collections.sort(enrollmentForHealth, new Comparator<EnrollmentShopDTO>(){
				     public int compare(EnrollmentShopDTO enrollmentId1, EnrollmentShopDTO enrollmentId2){
				         return Long.valueOf(enrollmentId2.getEnrollmentCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getEnrollmentCreationTimestamp().getTime()));
				     }
	
				});
			}
			
			
			if(enrollmentForHealth!= null && !enrollmentForHealth.isEmpty()){
				return enrollmentForHealth.get(0);
			}
		}
		
		return null;
		
	}	
	

	private void validateIncomingRequest(Long applicationId, String username) {

		StringBuilder errorMsg = new StringBuilder();

		if (StringUtils.isEmpty(username)){
			errorMsg.append("username is empty. ");
		}

		if (applicationId == null || applicationId == 0){
			errorMsg.append("applicationId is null or 0.");
		}

		if (!StringUtils.isEmpty(errorMsg.toString())){
			throw new GIRuntimeException(errorMsg.toString());
		}

	}
	
	public boolean isConfirmedEnrollment(EnrollmentDataDTO enrollmentDataDTO, List<String> validEnrollmentStatus) throws GIException{
		
		if(enrollmentDataDTO != null && validEnrollmentStatus.contains(enrollmentDataDTO.getEnrollmentStatus())) {
			if(enrollmentDataDTO.getEnrollmentBenefitEffectiveEndDate().after(new TSDate())) {
				return true;
			}
		}
		
		return false;		
	}
	

	private List<String> getValidActiveEnrollmentStatusList() {
		List<String> validActiveEnrollmentStatus = new ArrayList<String>();
		validActiveEnrollmentStatus.add("CONFIRM");
		return validActiveEnrollmentStatus;
	}

	
	@Override
	public List<EnrollmentWithMemberDataDTO> getEnrollmentByMembers(EnrollmentByMemberListRequestDTO requestDto,Long ssapApplicationId) throws GIException 
	{
		List<EnrollmentWithMemberDataDTO> dataList = null;
		ObjectMapper mapper = new ObjectMapper();
		String requestPayload = null;
		String responsePayload = null;
		String responseCode = null;
		String exceptionTrace = null;
		String status = RenewalConstants.FAILURE;
		String endPointUrl = GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_BY_MEMBER_ID_LIST;
		try 
		{
			requestPayload = mapper.writeValueAsString(requestDto);
			LOGGER.info("getEnrollmentByMembers request : " + requestPayload);
			ResponseEntity<String> response = ghixRestTemplate.exchange(endPointUrl, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, requestDto);
			if(response != null && response.getBody() != null) 
			{
				responsePayload = response.getBody();
				LOGGER.info("getEnrollmentByMembers response  = " + responsePayload);
				responseCode = String.valueOf(response.getStatusCode());
				status = RenewalConstants.SUCCESS;
				Map<String, List<EnrollmentWithMemberDataDTO>> enrollmentByMembersMap = mapper.readValue(responsePayload, LinkedHashMap.class);
				if(enrollmentByMembersMap != null && enrollmentByMembersMap.containsKey(RenewalConstants.KEY_ENROLLMENTS))
				{
					dataList = enrollmentByMembersMap.get(RenewalConstants.KEY_ENROLLMENTS);
					String json = mapper.writeValueAsString(dataList);
					CollectionType typeReference = TypeFactory.defaultInstance().constructCollectionType(List.class, EnrollmentWithMemberDataDTO.class);
					List<EnrollmentWithMemberDataDTO> data = mapper.readValue(json, typeReference);
					dataList = data;
				}
			}
		} 
		catch (Exception e) {
			exceptionTrace = ExceptionUtils.getFullStackTrace(e);
		}
		finally
		{
			
			String persistPayload = DynamicPropertiesUtil.getPropertyValue(RenewalConfiguration.RenewalConfigurationEnum.PERSIST_TOCONSIDER_PAYLOAD);
			if(RenewalConstants.Y.equalsIgnoreCase(persistPayload))
			{
				GIWSPayload giwsPayload = new GIWSPayload();
		        giwsPayload.setSsapApplicationId(ssapApplicationId);
		        giwsPayload.setCreatedTimestamp(new TSDate());
		        giwsPayload.setEndpointFunction(RenewalConstants.SSAP_TO_CONSIDER_BATCH);
		        giwsPayload.setEndpointOperationName("getEnrollmentByMembers");
		        giwsPayload.setEndpointUrl(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_BY_MEMBER_ID_LIST);
		        giwsPayload.setRequestPayload(requestPayload);
		        giwsPayload.setResponsePayload(responsePayload);
		        giwsPayload.setStatus(status);
		        giwsPayload.setResponseCode(responseCode);
		        giwsPayload.setExceptionMessage(exceptionTrace);
		        giwsPayloadService.save(giwsPayload);
			}
		}
		
		return dataList;
	}	
	
	
	
}
