package com.getinsured.eligibility.rrv.controller;

import com.getinsured.eligibility.rrv.service.RrvService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class RrvController {

    private static Logger logger = LoggerFactory.getLogger(RrvController.class);

    private final RrvService rrvService;

    @Autowired
    RrvController(RrvService rrvService){
        this.rrvService = rrvService;
    }

    @RequestMapping(value = "/checkStatusOfPendingSubmissions/{coverageYear}", method = RequestMethod.GET)
    public ResponseEntity<String> checkStatusOfPendingSubmissions(@PathVariable("coverageYear") long coverageYear){

        logger.info("Check Status of Pending RRV Submissions for {}", coverageYear);

        rrvService.checkStatusOfPendingSubmissions(coverageYear);

        return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
    }

    @RequestMapping(value = "/rrvSubmission/{coverageYear}/{applicationStatus}", method = RequestMethod.GET)
    public ResponseEntity<String> rrvSubmission(@PathVariable("coverageYear") long coverageYear,
                                                @PathVariable("applicationStatus") String applicationStatus){

        logger.info("RRV Submission for {}", coverageYear);

        int page = 0;
        int pageSize = 5000;

        Page<Long> ssapApplicationPage;
        long batchId;

        try {
            batchId = rrvService.getBatchId();
        } catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        do {
            //Paginating based on Applications that match the criteria to not cut off applicants from the same household
            ssapApplicationPage = rrvService.findAllEligibleSsapApplication(coverageYear, applicationStatus, new PageRequest(page, pageSize));

            List<Long> applicationIdList = ssapApplicationPage.getContent();

            rrvService.process(applicationIdList, batchId, coverageYear);

            page++;
        } while (!ssapApplicationPage.isLast());


        return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
    }
}
