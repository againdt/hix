package com.getinsured.hix.batch.enrollment.partitioner;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.skip.EnrollmentAdminEffectuation;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;

@Component("enrollmentAdminEffectuationPartitioner")
@Scope("step")
public class EnrollmentAdminEffectuationPartitioner  implements Partitioner {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAdminEffectuationPartitioner.class);
	private EnrollmentAdminEffectuation enrollmentAdminEffectuation;
	private static final String CSV = "csv";
	private String enrollmentAdminEffFilePath;

	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		/*
		 * move all files from source to wip folder
		 * put each file in each partitioner 
		 * read CSV file update enrollment.
		 */
		String enrollmentAdminEffectuationCSVPath;
		
		if (EnrollmentConfiguration.isCaCall()) {
			enrollmentAdminEffectuationCSVPath = enrollmentAdminEffFilePath;
		} else {
			enrollmentAdminEffectuationCSVPath = DynamicPropertiesUtil.getPropertyValue(
					EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_ADMIN_EFFECTUATION_CSV_PATH);
		}
		
		//String enrollmentAdminEffectuationCSVPath  = "D:\\vino\\Effectuation";
		String wIPFolderName = "WIP_"+new Date().getTime();
		String wIPFolder =  enrollmentAdminEffectuationCSVPath+ File.separator + wIPFolderName;
		
		enrollmentAdminEffectuation.setWipFolderPath(wIPFolder);
		enrollmentAdminEffectuation.setWipFolderName(wIPFolderName);
		
		try {
			EnrollmentUtils.moveFiles(enrollmentAdminEffectuationCSVPath, wIPFolder,CSV, null);
		}catch(Exception e){
			LOGGER.error("EnrollmentAdminEffectuationPartitioner failed to Move file from  : "+ enrollmentAdminEffectuationCSVPath +" To :" +wIPFolder , e);
			}
		
		File[] allCSVFileForProcesing = new File(wIPFolder).listFiles();
		
		Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();
		
		if(allCSVFileForProcesing != null && allCSVFileForProcesing.length > 0){
			for (File file : allCSVFileForProcesing) {
	            if(file.isFile() && file.getName().toLowerCase().endsWith(EnrollmentConstants.FILE_TYPE_CSV)){
	        	   ExecutionContext context = new ExecutionContext();
	        	   context.put("fileResource", file.toPath().toString());
	           	   context.put("wIPFolderDir", wIPFolder);
	        	   partitionMap.put(file.getName(), context);
	           }
	         }
		}
		
		return partitionMap;
	}

	public EnrollmentAdminEffectuation getEnrollmentAdminEffectuation() {
		return enrollmentAdminEffectuation;
	}

	public void setEnrollmentAdminEffectuation(
			EnrollmentAdminEffectuation enrollmentAdminEffectuation) {
		this.enrollmentAdminEffectuation = enrollmentAdminEffectuation;
	}
	
	public String getEnrollmentAdminEffFilePath() {
		return enrollmentAdminEffFilePath;
	}

	public void setEnrollmentAdminEffFilePath(String enrollmentAdminEffFilePath) {
		this.enrollmentAdminEffFilePath = enrollmentAdminEffFilePath;
	}

}
