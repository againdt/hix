<script type="text/ng-template" id="lossofmin.jsp">
<div class="gutter10" ng-controller="lossOfMinimumCoverageController">
    <h4>
        <spring:message code="label.lce.famHouseholdSum" />
    </h4>
    <div>
        <div>
            <table id="" class="table table-striped" ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold" role="presentation">
                <tbody>
                    <tr>
                        <td>
                            <spring:message code="label.lce.firstname" />
                        </td>
                        <td>
                            <spring:message code="label.lce.lastname" />
                        </td>
                        <td>
                            <spring:message code="label.lce.relationship" />
                        </td>
                        <td>
                            <spring:message code="label.lce.seekingcoverage" />?
                        </td>
                        <td>
							<spring:message code="label.lce.losscoverage" />?
                             <a class="ttclass" rel="tooltip" href="javascript:void(null);" data-toggle="tooltip" data-placement="right" title="<spring:message code="label.lce.lossMECTooltip"/>">
                      			<i class="icon-question-sign"></i>
                    		</a>
                        </td>
                    </tr>
                    <tr ng-repeat="(fIndex, householdMember)  in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
                        <td>{{householdMember.name.firstName}}</td>
                        <td>{{householdMember.name.lastName}}</td>
                        <td>
                            {{getRelationShipWithPrimaryHousehold(householdMember.personId)}}
                        </td>
                        <td>
                            <span ng-if="householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.yes" /></span>
                            <span ng-if="!householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.no" /></span>
                        </td>
                        <td>
							<div ng-if="householdMember.applyingForCoverageIndicator || !enableLossOfMEC(householdMember.personId)">-- </div>
                            <a class="btn btn-primary btn-small" ng-if="!householdMember.applyingForCoverageIndicator && enableLossOfMEC(householdMember.personId)" ng-click='toggleModal(householdMember,getOriginalIndex( householdMember,taxHousehold.householdMember));'>
                                <spring:message code="label.lce.edit" />
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- MODAL THAT POPS UP WHEN USER WANT TO EDIT  -->
            <modal-dialog-static-backdrop show='modalShown' ng-model="modalHouseholdMember">
                <p class="alert alert-info">
                    <spring:message code="label.lce.youhavereported"/> {{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.lastName}} <spring:message code="label.lce.lossMECp1"/>
                </p>
                <form class="form-horizontal" name="lossMinForm" novalidate>
                    <label class="control-label" for="dateOfChange">
                    	<spring:message code="label.lce.dateLossCoverage" />
                    <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                    </label>
                    <div class="controls margin20-t">
                        <div id="lossMinCalenderDiv" class="input-append ng-scope" datetimez="" ng-model="eventInfoArrayTemp[originalIndex].changeInLossDate" >
                            <input data-format="MM/dd/yyyy" type="text" id="dateOfChange{{$index}}" ng-model="eventInfoArrayTemp[originalIndex].changeInLossDate" name="lossMinChangeDate" class="span9"  required placeholder="MM/DD/YYYY" check-after="59">
                            <span class="add-on" ng-click='appalyZindex()'>
                            <i class="icon-calendar"></i>
                            </span>
                        </div>
                        <div class="margin10-l">
                            <span class="validation-error-text-container" ng-if="submitted && lossMinForm.lossMinChangeDate.$error.required">Please select date.</span>
							<span class="validation-error-text-container" ng-if="submitted && lossMinForm.lossMinChangeDate.$error.checkAfter"><spring:message code="label.lce.futureDateAllowed60"/></span>
                        </div>
                    </div>
                    <div class="control-group margin30-t">
                        <h4>
                            <spring:message code="label.lce.selectcriteriaLossMEC" />
                        </h4>
                        <div>
                            <label class="radio-inline">
                                <input type="radio" ng-change="eventInfoArrayTemp[originalIndex].changedLossOption=true" name="criteria" value="{{lossMinEventArray[0].name}}" ng-model="eventInfoArrayTemp[originalIndex].lossEventSubCategory" required>
                                <spring:message code="label.lce.lossthruheadofhousehold" />
                                <br> 
                            </label>
                        </div>
                        <div>
                            <label class="radio-inline">
                                <input type="radio" ng-change="eventInfoArrayTemp[originalIndex].changedLossOption=true" name="criteria" value="{{lossMinEventArray[1].name}}" ng-model="eventInfoArrayTemp[originalIndex].lossEventSubCategory" required>
                                <spring:message code="label.lce.lossthruemployer" />
                            </label>
                        </div>
                        <div>
                            <label class="radio-inline">
                            <input type="radio" ng-change="eventInfoArrayTemp[originalIndex].changedLossOption=true" name="criteria" value="{{lossMinEventArray[2].name}}" ng-model="eventInfoArrayTemp[originalIndex].lossEventSubCategory" required>
                            <spring:message code="label.lce.lossthruPublic"/>
                            </label>
                        </div>
                        <span class="validation-error-text-container span7" style="display:block" ng-if="submitted && (lossMinForm.criteria.$error.required)">Please select one option.</span>
                    </div>
                </form>
                <dl class="dl-horizontal pull-right">
                    <a class="btn btn-secondary" ng-click="toggleModal()"><spring:message code="lce.cancelbutton" /></a>
                    <a ng-if="lossMinForm.$valid" class="btn btn-primary" ng-click="modalOk(modalHouseholdMember)"><spring:message code="label.lce.continue" /></a>
                    <a ng-if="!lossMinForm.$valid" class="btn btn-secondary" ng-click="$parent.submitted=true"><spring:message code="label.lce.continue" /></a>
                </dl>
            </modal-dialog-static-backdrop>
        </div>
    </div>
    <dl class="dl-horizontal pull-right">
        <a class="btn btn-secondary" ng-click="back()" ng-if="!(lifeEventCounter >= 0 || counter >= 1)">
            <spring:message code="label.lce.back" />
        </a>
        <a class="btn btn-primary" ng-click="updateDates();next();">
            <spring:message code="label.lce.next" />
        </a>
    </dl>
</div>
</script>

