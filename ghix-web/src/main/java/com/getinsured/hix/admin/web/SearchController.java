package com.getinsured.hix.admin.web;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Predicate;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.hix.account.service.LocationService;
import com.getinsured.hix.broker.BrokerConfirmationEmail;
import com.getinsured.hix.broker.repository.IBrokerCertificationNumberRepository;
import com.getinsured.hix.broker.service.BrokerIndTriggerService;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.broker.service.jpa.ConsumerComposite;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.account.AccountUserDto;
import com.getinsured.hix.dto.account.QuestionAnswer;
import com.getinsured.hix.dto.agency.AgencyDetailsListDTO;
import com.getinsured.hix.dto.agency.AgencyInformationDto;
import com.getinsured.hix.dto.agency.AgencySiteDto;
import com.getinsured.hix.dto.agency.SearchAgencyDto;
import com.getinsured.hix.dto.broker.BrokerExportDTO;
import com.getinsured.hix.dto.broker.BrokerExportResponseDTO;
import com.getinsured.hix.dto.broker.BrokerSiteLocationDTO;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountActivation.STATUS;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Broker.certification_status;
import com.getinsured.hix.model.BrokerCertificationNumber;
import com.getinsured.hix.model.BrokerDocuments;
import com.getinsured.hix.model.BrokerResponse;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.TkmTicketsResponse;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.config.AEEConfiguration;
import com.getinsured.hix.platform.config.AEEConfiguration.AEEConfigurationEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.ecm.MimeConfig;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserRoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.TicketMgmtEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.iex.household.service.PreferencesService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.thoughtworks.xstream.XStream;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

/*
 * Handles requests for the application home page.
 */
@SuppressWarnings("unused")
@Controller
public class SearchController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SearchController.class);

	private static final String PAGE_SIZE = "pageSize";
	private static final String CURRENT_PAGE = "currentPage";
	private static final String BROKERSEARCHPARAM = "brokersearchparam";
	private static final String RESULT_SIZE = "resultSize";
	private static final String BROKERS_LIST = "brokerslist";
	private static final String RECORD_COUNT = "recordCount";
	private static final String BROKER_LIST = "brokerlist";
	private static final String STATUS_SUCCESS = "success";
	private static final String STATUS_FAILURE = "failure";
	private static final String DOCUMENT_ID_SUPPORTING = "documentIdSupporting";
	private static final String DOCUMENT_ID_E_O_DECL_PAGE = "documentIdEODeclPage";
	private static final String DOCUMENT_ID_CONTRACT = "documentIdContract";
	private static final String NEW_BROKER = "newBroker";
	private static final String UPLOADED_DOCUMENT_SUPPORTING = "uploadedDocument_Supporting";
	private static final String UPLOADED_DOCUMENT_E_O_DECL_PAGE = "uploadedDocument_EODeclPage";
	private static final String UPLOADED_DOCUMENT_CONTRACT = "uploadedDocument_Contract";
	private static final String FILE_NAME_E_O_DECL_PAGE = "fileName_EODeclPage";
	private static final String FILE_NAME_CONTRACT = "fileName_Contract";
	private static final String FILE_NAME = "fileName";
	private static final String STATUS_LIST = "statuslist";
	private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
	private static final String PAGE_TITLE = "page_title";
	private static final String BROKER = "broker";
	private static final String LOCATION_OBJ = "locationObj";
	private static final String MAILING_LOCATION_OBJ = "mailingLocationObj";
	private static final String CERTIFICATION_STATUS = "certification_status";
	private static final String LIST_OF_STATES = "statelist";
	private static final String LANGUAGES_LIST = "languagesList";
	private static final String UPDATEPROFILE = "updatedProfile";
	private static final String STATUS_HISTORY = "brokerStatusHistory";
	private static final String DATE_FORMAT = "MM-dd-yyyy";
	private static final String BROKER_NAME = "BrokerName";
	private static final String BROKER_COMPANY_ADDRESS = "BrokerCompanyAddress";
	private static final String NEW_CERT_STATUS = "newCertificationStatus";
	private static final String PRIOR_CERT_STATUS = "priorCertificationStatus";
	private static final String STAT_COMMENT_CHANGE = "StatusChangeComment";
	private static final String BROKER_CERT_NOTIFICATION = "brokercertnotification_";
	private static final String PDF = ".pdf";
	private static final String ECMRELATIVEPATH = "brokercertnotifications";
	private static final String NOTIFICATION_TEMPLATE_NAME = "NMBrokerCertificationNotification";
	//HIX-42075-START
	private static final String NOTIFICATION_TEMPLATE_NAME_WITH_POSTALMAIL = "nmBrokerCertificationNotificationWithPostalMail";
	//HIX-42075-END
	private static final String CURRENT_UPLOAD = "currentUpload";
	private static final String UPLOAD_FLOW = "uploadflow";
	private static final String NOTIFICATION_SEND_DATE = "notificationDate";
	private static final String EMAIL = "email";

	private static final String EXCHANGE_FULL_NAME = "exchangeFullName";
	private static final String EXCHANGE_ADDRESS_LINE_ONE = "exchangeAddressLineOne";
	private static final String STATE_NAME = "stateName";
	private static final String STATE_CODE = "stateCode";
	private static final String COUNTRY_NAME = "countryName";
	private static final String EXCHANGE_URL = "exchangeURL";
	private static final String EXCHANGE_PHONE = "exchangePhone";
	//HIX-20774
	private static final String EXCHANGE_NAME = "exchangeName";
	private static final String EXCHANGE_ADDRESS_ONE = "exchangeAddress1";
	private static final String EXCHANGE_CITY_NAME = "cityName";
	private static final String EXCHANGE_PIN_CODE = "pinCode";
	private static final Integer PAGE_SIZE_COUNT=10;
	private static final Integer UPLOAD_LIMIT= 5000001;
	//HIX-41564
	private static final String CURRDATE = "currDate";
	private static final String REQUIRED_FORMAT="MM/dd/yyyy";
	//HIX-42075-START
	private static final String ALL_MAIL_NOTICES="allowMailNotice";

	private static final String FIRST_NAME="firstName";
	private static final String LAST_NAME="lastName";
	private static final String FULL_NAME="fullName";
	private static final String MAILING_ADDRESS1="addressLine1";
	private static final String MAILING_ADDRESS2="addressLine2";
	private static final String MAILING_CITY="cityName";
	private static final String MAILING_STATE="stateCode";
	private static final String MAILING_ZIP="pinCode";

	//HIX-42075-END
	private static final String INDIVIDUAL_NOTIFICATION = "individualnotification_";
	private static final String INDIVIDUAL_MODULE = "individual";
	private static final String SEARCH_CRITERIA = "searchCriteria";
	private static final String BROKER_CERTIFICATION_DATE = "brokerCertificationDate";
	private static final int MAX_COMMENT_LENGTH = 4000;
	private static final String NA = "N/A";
	private static final String LIST_OF_AGENCY_SITES = "agencySites";
	
	private static final String IS_USER_SWITCH_TO_OTHER_VIEW = "isUserSwitchToOtherView";
	private static final String SWITCH_TO_MODULE_ID = "switchToModuleId";
	private static final String Y = "Y";

	//HIX-113524
	private static String EXPIRATION_DAYS_STR;
	private static final String CONSUMER_ACCOUNT_ACTIVATION_EMAIL = "consumerAccountActivationEmail";
	private static final String ADMIN_ACCOUNT_ACTIVATION_EMAIL = "adminUserAccountActivationEmail";
	private static final String ADMINISTRATOR = "Administrator";

	@Autowired private Gson platformGson;
	@Autowired
	private BrokerService brokerService;
	@Autowired
	private ContentManagementService ecmService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserRoleService userRoleService;
	@Autowired
	private IBrokerCertificationNumberRepository brokerCertificationNumberRepository;
	@Autowired
	private NoticeService noticeService;
	@Autowired 
	private ZipCodeService zipCodeService;
	@Autowired
	private PreferencesService preferencesService;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired private ApplicationContext appContext;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

	@Autowired
	private BrokerMgmtUtils brokerMgmtUtils;

	@Autowired
	private DesignateService designateService;

	@Autowired
	private BrokerConfirmationEmail notificationAgent;

	@Autowired
	SmartValidator smartValidator;
	
	@Autowired
	private LookupService lookupService;
	
	@Autowired 
	private BrokerIndTriggerService brokerIndTriggerService;
	
	@Autowired 
	private RoleService roleService;
	
	@Autowired
	LocationService locationService;
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired
	private AccountActivationService accountActivationService;
	
	
	/**
	 * Handles the Manage Agent request by Admin
	 *
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate manage Agent page
	 */
	@RequestMapping(value = "admin/broker/manage", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String manageBroker(Model model, HttpServletRequest request) {
		LOGGER.info("manageBroker: START");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Manage Agent");
		String tableId = "brokerTable";
		Integer pageSize = PAGE_SIZE_COUNT;
		String requestPageSize = request.getParameter("pageSize");
		//if request page size is not empty
		if(!StringUtils.isBlank(requestPageSize)){
			pageSize = Integer.parseInt(requestPageSize);
		}
		request.setAttribute(PAGE_SIZE, pageSize);
		request.setAttribute("reqURI", "list");
		String param = request.getParameter("pageNumber");

		Integer startRecord = 0;
		int currentPage = 1;
		if (null != param) {
			startRecord = (Integer.parseInt(param) - 1) * pageSize;
			currentPage = Integer.parseInt(param);

		}

		Broker brkObj = new Broker();
		String sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) ?  BrokerConstants.CONTACT_NAME : request.getParameter(BrokerConstants.SORT_BY);
		
		String screenpopParam = request.getParameter("ANI");
		
		if(null!=screenpopParam && screenpopParam.length()>0) {
			
			populateBrokerContactNumbers(brkObj, screenpopParam);
			
		}
	 	SortOrder sortOrder = EntityUtils.determineSortOrder(request);

		Map<String, Object> brokerListAndRecordCount = brokerService.findBroker(brkObj, startRecord, pageSize, sortBy, sortOrder);
		List<Broker> brokers = (List<Broker>) brokerListAndRecordCount.get(BROKER_LIST);
		Long iResultCt = (Long) brokerListAndRecordCount.get(RECORD_COUNT);
		int iResultCount = 0;
		if(iResultCt != null){
			iResultCount = iResultCt.intValue();
		}
		
		if(null!=brkObj.getContactNumber() && brkObj.getContactNumber().length()>0) {
			brkObj.setContactNumber(clearContactNumberFormatting(brkObj.getContactNumber()));
		}
		
		model.addAttribute(BROKERS_LIST, brokers);
		model.addAttribute(RESULT_SIZE, iResultCount);
		model.addAttribute(CURRENT_PAGE, currentPage);
		request.getSession().setAttribute(BROKERSEARCHPARAM, brkObj);
		model.addAttribute(STATUS_LIST, getStatusList());
		model.addAttribute(EntityConstants.SORT_ORDER, sortOrder);
		model.addAttribute(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));	
		LOGGER.info("manageBroker: END");
		return "admin/broker/manage";
	}

	/**
	 * Handles the Broker's list page on Manage Admin tab
	 *
	 * @param broker
	 * @param result
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate Broker list page
	 * @throws GIRuntimeException 
	 */
	@RequestMapping(value = "admin/broker/manage/list", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String brokerList(@ModelAttribute(BROKER) Broker broker, BindingResult result, Model model,
			HttpServletRequest request) throws GIRuntimeException {
		LOGGER.info("brokerList : START ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Manage Broker");
		Broker brkObj = null;
		String sessionUse = request.getParameter("usesession");
		try {
			
			Predicate<Broker> phoneNumberPresent = b -> b!=null && b.getContactNumber()!=null && b.getContactNumber().length()>0;
			Predicate<Broker> licenseNumberPresent = b -> b!=null && b.getLicenseNumber()!=null && b.getLicenseNumber().length()>0;
			
			try
			{
				if (sessionUse != null && "yes".equalsIgnoreCase(sessionUse)) {
					brkObj = (Broker) request.getSession().getAttribute(BROKERSEARCHPARAM);
					if (brkObj == null) {
						brkObj = (Broker) broker.clone();
					}
				} else {
					brkObj = (Broker) broker.clone();
				}
				
				if(phoneNumberPresent.test(brkObj)) {
					populateBrokerContactNumbers(brkObj, brkObj.getContactNumber());
				}
				
				if(licenseNumberPresent.test(brkObj)) {
					brkObj.setNpn(brkObj.getLicenseNumber());
				}
				
			}catch(CloneNotSupportedException cns) {
				LOGGER.error("Exception occured while cloning the broker object : ", cns);
				throw new GIRuntimeException(cns, ExceptionUtils.getFullStackTrace(cns), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			} 
			// certification_status status = brkObj.getCertificationStatus();
			model.addAttribute(PAGE_TITLE, " Getinsured Health Exchange: Manage Broker");
			String tableId = "brokerTable";
			Integer pageSize = PAGE_SIZE_COUNT;
			String requestPageSize = request.getParameter("pageSize");
			//if request page size is not empty
			if(!StringUtils.isBlank(requestPageSize)){
				pageSize = Integer.parseInt(requestPageSize);
			}
			request.setAttribute(PAGE_SIZE, pageSize);
			request.setAttribute("reqURI", "list");
			String param = request.getParameter("pageNumber");
			Integer startRecord = 0;
			int currentPage = 1;
			if (param != null) {
				startRecord = (Integer.parseInt(param) - 1) * pageSize;
				currentPage = Integer.parseInt(param);
			}
			
			String sortBy =  (request.getParameter(EntityConstants.SORT_BY) == null) ?  BrokerConstants.CONTACT_NAME : request.getParameter(EntityConstants.SORT_BY);
			SortOrder sortOrder = EntityUtils.determineSortOrder(request);
			
			Map<String, Object> brokerListAndRecordCount = brokerService.findBroker(brkObj, startRecord, pageSize, sortBy, sortOrder);
			
			List<Broker> brokers = (List<Broker>) brokerListAndRecordCount.get(BROKER_LIST);
			Long iResultCt = (Long) brokerListAndRecordCount.get(RECORD_COUNT);
			int iResultCount = 0;
			if(iResultCt != null){
				iResultCount = iResultCt.intValue();
			}
			model.addAttribute(BROKERS_LIST, brokers);
			model.addAttribute(RESULT_SIZE, iResultCount);
			model.addAttribute(CURRENT_PAGE, currentPage);

			// This list is added into model to persist check-box value after search
			// is performed by user.
			List<String> selectedStatusList = new ArrayList<String>();
			if (!BrokerMgmtUtils.isEmpty(brkObj.getCertificationStatus())) {
				// If multiple status are selected, then split and add those into a
				// list, else if single status is selected, add it to a list
				// directly.
				if (brkObj.getCertificationStatus().contains(BrokerConstants.COMMA)) {
					selectedStatusList = Arrays.asList(brkObj.getCertificationStatus().split(BrokerConstants.COMMA));
				} else {
					selectedStatusList.add(brkObj.getCertificationStatus());
				}
			}
			
			if(phoneNumberPresent.test(brkObj)) {
				brkObj.setContactNumber(clearContactNumberFormatting(brkObj.getContactNumber()));
			}
			
			AccountUser user = userService.getLoggedInUser();
			request.getSession().setAttribute("loggedInRole",userService.hasUserRole(user, RoleService.AGENCY_MANAGER));
			model.addAttribute("selectedStatusList", selectedStatusList);
			request.getSession().setAttribute(BROKERSEARCHPARAM, brkObj);
			model.addAttribute(STATUS_LIST, getStatusList());
			model.addAttribute(EntityConstants.SORT_ORDER, sortOrder);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			model.addAttribute(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
			LOGGER.info("brokerList : END ");
		}
		catch(GIRuntimeException giexception){
			LOGGER.error("Exception occured while fetching Agents : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching Agents : ", exception	);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		return "admin/broker/manage";
	}
	
	private Broker populateBrokerContactNumbers(Broker brkObj,String contactNumber) {
		
		brkObj.setContactNumber(contactNumber);
		brkObj.setBusinessContactPhoneNumber(contactNumber);
		brkObj.setAlternatePhoneNumber(contactNumber);
		
		return brkObj;
	}
	
	private String formatContactNumber(String contactNumber) {
		
		return contactNumber.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
		
	}
	
	private String clearContactNumberFormatting(String contactNumber) {
		
		return contactNumber.replaceAll("-", "");
		
	}

	private List<String> getStatusList() {
		List<String> statusList = new ArrayList<String>();
		statusList.add("Pending");
		statusList.add("Withdrawn");
		statusList.add("Certified");
		statusList.add("Eligible");
		statusList.add("Denied");
		statusList.add("Terminated-Vested");
		statusList.add("Terminated-For-Cause");
		statusList.add("Deceased");
		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
			statusList.add("Suspended");
		}
		return statusList;
	}

	/**
	 * Handles the Broker's Certification Status.
	 *
	 * @param broker
	 * @param result
	 * @param model
	 * @param request
	 * @return URL for navigation to update certification status page.
	 */
	@RequestMapping(value = "admin/broker/updatecertificationstatus", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_EDIT_BROKER')")
	public String getcertificationstatus(Model model, HttpServletRequest request) throws GIRuntimeException {
		LOGGER.info("getcertificationstatus : START ");
		if (null == request.getParameter(UPLOAD_FLOW) || request.getParameter(UPLOAD_FLOW).trim().isEmpty()
				|| "n".equalsIgnoreCase(request.getParameter(UPLOAD_FLOW).trim())) {

			try {

				this.clearSessionAttributes(request);

			} catch(GIRuntimeException giexception) {
				LOGGER.error("Exception occurred while clearing session variables : ", giexception);
				throw giexception;
			} catch(Exception exception) {
				LOGGER.error("Exception occurred while clearing session variables : ", exception);
				throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			} 
		}
		LOGGER.info("getcertificationstatus : END ");
		return "admin/broker/updatecertificationstatus";
	}

	/**
	 * Handles the GET request for Broker's Edit Certification Status.
	 *
	 * @param model
	 * @param id
	 * @param request
	 * @return URL for navigation to edit certification status page.
	 */
	@RequestMapping(value = "/admin/broker/editcertificationstatus/{encBrokerId}", method = { RequestMethod.GET,RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String updatecertificationstatus(Model model, HttpServletRequest request, @PathVariable("encBrokerId") String encBrokerId) throws GIRuntimeException
	{
		LOGGER.info("updatecertificationstatus : START ");
		Broker brokerObj = null;
		String decryptedBrokerId = null;
		int id;
		try {
			decryptedBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			id = Integer.parseInt(decryptedBrokerId);
			brokerObj = brokerService.getBrokerDetail(id);
			model.addAttribute(LOCATION_OBJ, brokerObj.getLocation());
			model.addAttribute(STATUS_LIST, getStatusList());
			//model.addAttribute(CURRDATE, DateUtil.dateToString(new TSDate(), REQUIRED_FORMAT));
			model.addAttribute(CURRDATE, (new TSDate()).getTime());
			if(null != brokerObj.getCertificationDate()){
				model.addAttribute(BROKER_CERTIFICATION_DATE,brokerObj.getCertificationDate().getTime());
			}else{
				model.addAttribute(BROKER_CERTIFICATION_DATE,0L);
			}
			// Get the broker details from broker audit table
			List<Map<String, Object>> brokerCertStatusHistory = brokerService.loadBrokerStatusHistory(id);
			model.addAttribute(STATUS_HISTORY, brokerCertStatusHistory);

			// Getting Broker object from session after file upload
			Broker brokerInSession = (Broker) request.getSession().getAttribute(BROKER);

			String fileNameEODeclPage = (String) request.getSession().getAttribute(FILE_NAME_E_O_DECL_PAGE);
			String fileNameContract = (String) request.getSession().getAttribute(FILE_NAME_CONTRACT);
			String fileName = (String) request.getSession().getAttribute(FILE_NAME);
			if (brokerInSession != null) {
				// Setting new Broker object in session to set certification
				// status and comments added by user while uploading document
				model.addAttribute(NEW_BROKER, brokerInSession);

				if (null != request.getParameter(DOCUMENT_ID_E_O_DECL_PAGE)
						&& !request.getParameter(DOCUMENT_ID_E_O_DECL_PAGE).isEmpty()) {
					model.addAttribute(UPLOADED_DOCUMENT_E_O_DECL_PAGE, request.getParameter(DOCUMENT_ID_E_O_DECL_PAGE));
					request.getSession().setAttribute(UPLOADED_DOCUMENT_E_O_DECL_PAGE,
							request.getParameter(DOCUMENT_ID_E_O_DECL_PAGE));
				}

				if (null != request.getParameter(DOCUMENT_ID_CONTRACT)
						&& !request.getParameter(DOCUMENT_ID_CONTRACT).isEmpty()) {
					model.addAttribute(UPLOADED_DOCUMENT_CONTRACT, request.getParameter(DOCUMENT_ID_CONTRACT));
					request.getSession().setAttribute(UPLOADED_DOCUMENT_CONTRACT,
							request.getParameter(DOCUMENT_ID_CONTRACT));
				}

				if (null != request.getParameter(DOCUMENT_ID_SUPPORTING)
						&& !request.getParameter(DOCUMENT_ID_SUPPORTING).isEmpty()) {
					model.addAttribute(UPLOADED_DOCUMENT_SUPPORTING, request.getParameter(DOCUMENT_ID_SUPPORTING));
					request.getSession().setAttribute(UPLOADED_DOCUMENT_SUPPORTING,
							request.getParameter(DOCUMENT_ID_SUPPORTING));
				}
			}

			model.addAttribute(BROKER, brokerObj);
			model.addAttribute(FILE_NAME_E_O_DECL_PAGE, fileNameEODeclPage);
			model.addAttribute(FILE_NAME_CONTRACT, fileNameContract);
			model.addAttribute(FILE_NAME, fileName);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			request.getSession().removeAttribute(BROKER);
			
			List<Broker> brokers =   brokerService.findCompleteBrokersByLicenseNumber(brokerObj.getLicenseNumber());
			model.addAttribute("sameBrokersList", brokers);
			 
			
			setEncryptedUserNameInModel(model, brokerObj);
		} catch(GIRuntimeException giexception) {
			LOGGER.error("Cannot edit Broker's Certification Status : ", giexception);
			throw giexception;
		} catch(Exception exception) {
			LOGGER.error("Cannot edit Broker's Certification Status : ", exception);			
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}    
		LOGGER.info("updatecertificationstatus : END ");
		return "admin/broker/updatecertificationstatus";
	}

	/**
	 * Handles the GET request for Broker's Details.
	 *
	 * @param broker
	 * @param id
	 * @param model
	 * @param request
	 * @return URL for navigation to edit certification status page.
	 */
	@RequestMapping(value = "/broker/viewdetail/{id}/{currentpage}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String viewDetail(Model model, HttpServletRequest request, @PathVariable("id") Integer id,
			@PathVariable("currentpage") Integer currentpage) throws GIRuntimeException {
		LOGGER.info("viewDetail: START ");

		model.addAttribute(PAGE_TITLE, " Getinsured Health Exchange: Broker Details");
		try{
			Broker brokerObj = brokerService.getBrokerDetail(id);
			model.addAttribute(BROKER, brokerObj);
			model.addAttribute("locationObj", brokerObj.getLocation());
			model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
			model.addAttribute("brkpage", currentpage);
			model.addAttribute(CURRENT_PAGE, currentpage);
			LOGGER.info("viewDetail: END ");
		}
		catch(GIRuntimeException giexception) {
			LOGGER.error("Cannot view Broker's Details : ", giexception);
			throw giexception;
		} catch(Exception exception) {
			LOGGER.error("Cannot view Broker's Details : ", exception);			
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		return "broker/viewdetail";
	}

	/**
	 * Handles the POST request for Broker's Details.
	 *
	 * @param broker
	 * @param result
	 * @param model
	 * @param request
	 * @return URL for navigation to edit certification status page.
	 */
	@RequestMapping(value = "/broker/viewdetail", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String brokerDetail(@ModelAttribute(BROKER) Broker broker, BindingResult result, Model model,
			HttpServletRequest request, @RequestParam(value = "fileInput", required = false) MultipartFile fileInput) 
					throws GIRuntimeException {

		LOGGER.info("brokerDetail: START ");

		/** Check for XSS Attack - Starts */
		broker.setComments(XssHelper.stripXSS(broker.getComments()));
		/** Check for XSS Attack - Ends */

		model.addAttribute(PAGE_TITLE, " Getinsured Health Exchange: Broker Details");
		try{
			Broker myBroker = brokerService.getBrokerDetail(broker.getId());
			model.addAttribute(BROKER, myBroker);
			model.addAttribute("locationObj", myBroker.getLocation());
			model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
			model.addAttribute("brkpage", request.getParameter("currentpage"));
			model.addAttribute(CURRENT_PAGE, request.getParameter("currentpage"));
			LOGGER.info("brokerDetail: END ");
		}
		catch(GIRuntimeException giexception) {
			LOGGER.error("Cannot view Broker's Details : ", giexception);
			throw giexception;
		} catch(Exception exception) {
			LOGGER.error("Cannot view Broker's Details : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}  
		return "broker/viewdetail";
	}
	
	@RequestMapping(value = "/admin/broker/updatebrokercertificationstatus", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	@ResponseBody
	public String updateBrokerCertificationStatus(@ModelAttribute(BROKER) Broker broker, BindingResult bindingResult,
			HttpServletRequest request, @RequestParam("brokerId") String brokerId,
			@RequestParam(value = "newStartDate", required = false) String newStartDate,
			@RequestParam(value = "newEndDate", required = false) String newEndDate,
			@RequestParam(value = "fileInput", required = false) CommonsMultipartFile fileInput)
			throws GIRuntimeException {

		String status = BrokerConstants.RESPONSE_SUCCESS;

		try {
			Validate.notNull(broker);
			broker.setComments(XssHelper.stripXSS(broker.getComments()));
			validateBrokerCerification(bindingResult, newStartDate, newEndDate, broker);

			updateCertificationStatus(Integer.valueOf(brokerId), broker.getComments(), newStartDate, newEndDate,
					broker.getCertificationStatus(), request);
		} catch (GIRuntimeException giexception) {
			LOGGER.error("Cannot update Broker's Certification Status : ", giexception);
			status = BrokerConstants.RESPONSE_FAILURE;
		} catch (Exception exception) {
			LOGGER.error("Cannot update Broker's Certification Status: ", exception);
			status = BrokerConstants.RESPONSE_FAILURE;
		}
		return status;
	}
	/**
	 * Handles the POST request for Broker's update Certification status
	 *
	 * @param broker
	 * @param result
	 * @param fileInput
	 * @param brokerId
	 * @param certificationStatus
	 * @return URL for navigation to edit certification status page.
	 * @throws GIRuntimeException 
	 */
	@RequestMapping(value = "/admin/broker/updatecertificationstatus", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String updatecertificationstatus(@ModelAttribute(BROKER) Broker broker,BindingResult bindingResult, Model model,
			HttpServletRequest request, @RequestParam("brokerId") String brokerId,
			@RequestParam(value="brokerIds", required = false) List<String> brokerIds,
			@RequestParam(value = "newStartDate", required = false) String newStartDate,
			@RequestParam(value = "newEndDate", required = false) String newEndDate,
			@RequestParam(value = "fileInput", required = false) CommonsMultipartFile fileInput) throws GIRuntimeException {

		LOGGER.info("updatecertificationstatus: START ");
		String encryptedBrkId = null;
		
		try {
			Validate.notNull(broker);
			broker.setComments(XssHelper.stripXSS(broker.getComments()));
			validateBrokerCerification(bindingResult, newStartDate, newEndDate,broker);
			
			if(brokerIds!=null){
				for(String brokerID:brokerIds){
					updateCertificationStatus(Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(brokerID)), broker.getComments(), newStartDate, newEndDate, broker.getCertificationStatus(), request);
				}
			} else {
				updateCertificationStatus(Integer.valueOf(brokerId), broker.getComments(), newStartDate, newEndDate, broker.getCertificationStatus(), request);
			}
			
			encryptedBrkId = ghixJasyptEncrytorUtil.encryptStringByJasypt(brokerId);
			
			model.addAttribute(STATUS_LIST, getStatusList());
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));

			request.getSession().removeAttribute(FILE_NAME_E_O_DECL_PAGE);
			request.getSession().removeAttribute(FILE_NAME_CONTRACT);
			request.getSession().removeAttribute(FILE_NAME);
			request.getSession().removeAttribute(CURRENT_UPLOAD);
		} 
		catch(GIRuntimeException giexception) {
			LOGGER.error("Cannot update Broker's Certification Status : ", giexception);
			throw giexception;
		} catch(Exception exception) {
			LOGGER.error("Cannot update Broker's Certification Status: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("updatecertificationstatus: END ");
		return "redirect:/admin/broker/certificationstatus/" + encryptedBrkId;
	}
	
	private void updateCertificationStatus(int brokerId, String comment, String newStartDate, String newEndDate, String certificationStatus, HttpServletRequest request){
		AccountUser objUser = null;
		String priorCertificationStatus=null;
		String documentId = null;
		List<String> sendToEmailList = null;
		
		try {
			Broker brokerObj = brokerService.getBrokerDetail(brokerId);
			
			AccountUser user = userService.getLoggedInUser();
			
			Validate.notNull(brokerObj);
			
			brokerObj.setLocation(brokerObj.getLocation());
			brokerObj.setComments(comment);

			objUser = brokerObj.getUser();
			brokerObj.setUser(objUser);
			priorCertificationStatus=brokerObj.getCertificationStatus();
			if (null != request.getParameter("documentId_EODeclPage")
					&& !request.getParameter("documentId_EODeclPage").toString().trim().isEmpty()) {
				documentId = request.getParameter("documentId_EODeclPage").toString().trim();
				if (BrokerMgmtUtils.isEmpty(documentId)
						|| (!EntityUtils.isEmpty(documentId) && "0".equalsIgnoreCase(documentId))) {
					brokerObj.setEoDeclarationDocumentId(null);
				} else {
					brokerObj.setEoDeclarationDocumentId(Integer.valueOf(documentId));
				}
			}

			if (null != request.getParameter("documentId_Contract")
					&& !request.getParameter("documentId_Contract").toString().trim().isEmpty()) {
				documentId = request.getParameter("documentId_Contract").toString().trim();

				if (BrokerMgmtUtils.isEmpty(documentId)
						|| (!EntityUtils.isEmpty(documentId) && "0".equalsIgnoreCase(documentId))) {
					brokerObj.setContractDocumentId(null);
				} else {
					brokerObj.setContractDocumentId(Integer.valueOf(documentId));
				}
			}

			if (null != request.getParameter("documentId_Supporting")
					&& !request.getParameter("documentId_Supporting").toString().trim().isEmpty()) {
				documentId = request.getParameter("documentId_Supporting").toString().trim();

				if (BrokerMgmtUtils.isEmpty(documentId)
						|| (!EntityUtils.isEmpty(documentId) && "0".equalsIgnoreCase(documentId))) {
					brokerObj.setSupportingDocumentId(null);
				} else {
					brokerObj.setSupportingDocumentId(Integer.valueOf(documentId));
				}
			}

			// If current status and actual status is not same, then do status
			// related changes in object.
			if (brokerObj.getCertificationStatus() != null
					&& !brokerObj.getCertificationStatus().equalsIgnoreCase(certificationStatus)) {
				brokerObj.setCertificationStatus(certificationStatus);
				brokerObj.setStatusChangeDate(new TSDate());

				if ( ( certificationStatus != null)
						&& (certificationStatus
								.equalsIgnoreCase(certification_status.Certified.toString()) )  && (!EntityUtils.isEmpty(newStartDate) && !EntityUtils.isEmpty(newEndDate))  ) {

					SimpleDateFormat dateFormat = new SimpleDateFormat(REQUIRED_FORMAT);
					Date startDate = dateFormat.parse(newStartDate);
					Date endDate = dateFormat.parse(newEndDate);

					brokerObj.setCertificationDate(startDate);
					brokerObj.setReCertificationDate(endDate);
					brokerObj.setCertificationNumber(generateBrokerCertificationNumber(brokerObj));
				}
				if (certificationStatus
						.equalsIgnoreCase(certification_status.TerminatedVested.toString())
						|| certificationStatus
						.equalsIgnoreCase(certification_status.TerminatedForCause.toString())) {
					brokerObj.setDeCertificationDate(new TSDate());
				}
				brokerObj.setLastUpdatedBy(user.getId());
				// Save the object and trigger IND35
				Broker updatedBrokerObj = brokerService.saveBrokerWithLocation(brokerObj);

				if(!BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					String noticeTemplateName = NOTIFICATION_TEMPLATE_NAME;
					Map<String, Object> brokerTemplateData = new HashMap<String, Object>();
					brokerTemplateData.put(BROKER_NAME, objUser.getFirstName() + " " + objUser.getLastName());
					brokerTemplateData.put(PRIOR_CERT_STATUS, priorCertificationStatus);
					StringBuilder brokerCompanyAddress = new StringBuilder("");

					if(null != brokerObj  &&  null != brokerObj.getLocation() ) {

						brokerCompanyAddress.append(brokerObj.getLocation().getAddress1() + ",<br/>");

						if(null != brokerObj.getLocation().getAddress2() && !brokerObj.getLocation().getAddress2().isEmpty()) {
							brokerCompanyAddress.append(brokerObj.getLocation().getAddress2() + ",<br/>");
						}

						brokerCompanyAddress.append(brokerObj.getLocation().getCity() + ",&nbsp;");
						brokerCompanyAddress.append(brokerObj.getLocation().getState() + " ");
						brokerCompanyAddress.append(brokerObj.getLocation().getZip());
					}

					//brokerTemplateData.put(BROKER_COMPANY_ADDRESS, brokerObj.getCompanyName() + "<br/>" + brokerCompanyAddress.toString());
					brokerTemplateData.put(BROKER_COMPANY_ADDRESS , brokerCompanyAddress.toString());
					brokerTemplateData.put(NEW_CERT_STATUS, certificationStatus);

					SimpleDateFormat sdf = new SimpleDateFormat("MMMMMMMMM dd, yyyy");
					brokerTemplateData.put(NOTIFICATION_SEND_DATE, sdf.format(new TSDate()));
					brokerTemplateData.put(STAT_COMMENT_CHANGE, comment);
					String fileName = BROKER_CERT_NOTIFICATION + TimeShifterUtil.currentTimeMillis() + PDF;

					// tokens required for the notification template
					brokerTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
					brokerTemplateData.put(EXCHANGE_ADDRESS_LINE_ONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
					brokerTemplateData.put(STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
					brokerTemplateData.put(COUNTRY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
					brokerTemplateData.put(EXCHANGE_URL, GhixEndPoints.GHIXWEB_SERVICE_URL);
					brokerTemplateData.put("EXCHANGE_URL", GhixEndPoints.GHIXWEB_SERVICE_URL);// need this token to match with template. Fix the template also to make tokens consistent
					brokerTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
					// HIX-20774

					brokerTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));

					brokerTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
					brokerTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
					brokerTemplateData.put(EXCHANGE_PIN_CODE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
					brokerTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);

					Map<String, String> templateDataModified = getTemplateDataMap(brokerTemplateData);
					sendToEmailList = new LinkedList<String>();
					sendToEmailList.add(objUser.getEmail());
					//HIX-42075-START
					if(brokerObj.getPostalMailEnabled()!=null && brokerObj.getPostalMailEnabled().equalsIgnoreCase("Y") && DynamicPropertiesUtil.getPropertyValue(AEEConfigurationEnum.AGENT_ALLOWMAILNOTICES).equalsIgnoreCase("Y"))
					{
						brokerTemplateData.put(FIRST_NAME, objUser.getFirstName());
						brokerTemplateData.put(LAST_NAME, objUser.getLastName());
						brokerTemplateData.put(FULL_NAME, objUser.getFirstName()+" "+objUser.getLastName());
						brokerTemplateData.put(MAILING_ADDRESS1, brokerObj.getMailingLocation().getAddress1());
						if(!StringUtils.isEmpty( brokerObj.getMailingLocation().getAddress2()))
						{
							brokerTemplateData.put(MAILING_ADDRESS2,brokerObj.getMailingLocation().getAddress2());
						}else
						{
							brokerTemplateData.put(MAILING_ADDRESS2,"");
						}
						brokerTemplateData.put(MAILING_CITY, brokerObj.getMailingLocation().getCity());
						brokerTemplateData.put(MAILING_STATE, brokerObj.getMailingLocation().getState());
						brokerTemplateData.put(MAILING_ZIP, brokerObj.getMailingLocation().getZip());
						String noticeTemplateNameWithPostalMail = NOTIFICATION_TEMPLATE_NAME_WITH_POSTALMAIL;
						noticeService.createModuleNotice(noticeTemplateNameWithPostalMail, GhixLanguage.US_EN, brokerTemplateData, ECMRELATIVEPATH, fileName, BROKER, brokerId, sendToEmailList, "Exchange Admin", objUser.getFullName(),brokerObj.getMailingLocation(),GhixNoticeCommunicationMethod.Mail);
						//HIX-42075-END
					}else
					{
						noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, brokerTemplateData, ECMRELATIVEPATH, fileName, BROKER, brokerId, sendToEmailList, "Exchange Admin", objUser.getFullName(),brokerObj.getMailingLocation(),GhixNoticeCommunicationMethod.Email);
					}

					//Check for only Individuals/Both
					String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);

					if( null != stateExchangeType ) {

						stateExchangeType = stateExchangeType.trim();

						if(stateExchangeType.equalsIgnoreCase(BrokerConstants.BOTH) || stateExchangeType.equalsIgnoreCase(BrokerConstants.INDIVIDUAL))
						{
							// Check the certification status.
							// if it is TerminatedVested / TerminatedForCause / Deceased

							String agentStatus[] = DynamicPropertiesUtil.getPropertyValue(AEEConfiguration.AEEConfigurationEnum.AGENT_STATUS).split(",");						
							noticeTemplateName = BrokerConstants.INDIVIDUAL_NOTIFICATION_TEMPLATE_NAME;
							if ((brokerObj.getCertificationStatus() != null))
							{
								if(agentStatus.length > 0)
								{

									for(String agentstat : agentStatus) {
										{
											if(brokerObj.getCertificationStatus().toString().equalsIgnoreCase(agentstat))
											{
												StringBuilder reasonForDesignation = new StringBuilder();											
												reasonForDesignation.append(brokerObj.getUser().getFirstName());
												reasonForDesignation.append(" ");
												reasonForDesignation.append(brokerObj.getUser().getLastName());
												reasonForDesignation.append(" is no longer affiliated with ");
												reasonForDesignation.append(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));


												brokerTemplateData.put(BrokerConstants.BROKER_BUSINESS_NAME, brokerObj.getUser().getFirstName() + " " + brokerObj.getUser().getLastName());
												brokerTemplateData.put(BrokerConstants.REASON_FOR_DE_DESIGNATION,reasonForDesignation.toString());
												brokerTemplateData.put(BrokerConstants.NOTIFICATION_DATE, sdf.format(new TSDate()));

												Map<String, Object> searchCriteriaMap = new HashMap<String, Object>();
												searchCriteriaMap.put(BrokerConstants.MODULE_NAME, BrokerConstants.SENDER_BROKER_MODULE);
												searchCriteriaMap.put(BrokerConstants.SENDER_OBJ, brokerObj);											
												searchCriteriaMap.put(BrokerConstants.MODULE_ID, brokerObj.getId());
												searchCriteriaMap.put(BrokerConstants.STATUS, new StringBuilder().append(BrokerConstants.STATUS_PENDING).append(",").append(BrokerConstants.STATUS_ACTIVE).toString());
												Integer startRecord = 0;
												searchCriteriaMap.put(BrokerConstants.START_PAGE, startRecord);
												Map<String, Object> inividualAPIRequestMap = new HashMap<String, Object>();
												inividualAPIRequestMap.put(SEARCH_CRITERIA, searchCriteriaMap);

												List<ConsumerComposite> houseHoldObjList = new ArrayList<ConsumerComposite>();
												

												Map<String, Object> houseHoldMap = brokerMgmtUtils.retrieveConsumerList(inividualAPIRequestMap);
												String fileNameForNotification = null;
												if( houseHoldMap != null && houseHoldMap.size() > 0 ) {
													houseHoldObjList = (List<ConsumerComposite>) houseHoldMap.get(BrokerConstants.HOUSEHOLDLIST);
												}
												
												List<DesignateBroker.Status> list = new ArrayList<>();
												list.add(DesignateBroker.Status.Pending);
												list.add(DesignateBroker.Status.Active);

												// retrieve a list of designated broker records using brokerId
												List<DesignateBroker> desigBrokerList = designateService.findActiveAndPendingDesignateBrokers(brokerObj.getId(), list);
												for(DesignateBroker designateBroker : desigBrokerList)
												{
													if(BrokerConstants.STATUS_ACTIVE.equalsIgnoreCase(designateBroker.getStatus().toString())){
														brokerMgmtUtils.updateEnrollmentDetails(brokerObj, BrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL,
																user.getUsername(),new Long(designateBroker.getIndividualId()), BrokerConstants.AGENT_ROLE,
																BrokerConstants.REMOVEACTIONFLAG);												
													}
													
													//Make that broker to individual association inactive.
													designateService.deDesignateIndividualForBroker(designateBroker);
												}
												// Get the list of Individuals associated with this agent
												for(ConsumerComposite houseHoldObj:houseHoldObjList)
												{	
													PreferencesDTO  preferencesDTO  =  preferencesService.getPreferences((int)houseHoldObj.getId(), false);
													List<String> individualEmailAdList = new ArrayList<String>();
													individualEmailAdList.add(preferencesDTO.getEmailAddress());
													fileNameForNotification = INDIVIDUAL_NOTIFICATION + TimeShifterUtil.currentTimeMillis() + PDF;
													noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, brokerTemplateData, ECMRELATIVEPATH, fileNameForNotification, INDIVIDUAL_MODULE, 
															houseHoldObj.getId(), individualEmailAdList, brokerObj.getUser().getFullName(),
															(houseHoldObj.getFirstName() + " " + houseHoldObj.getLastName()),getLocationFromDTO(preferencesDTO.getLocationDto()),preferencesDTO.getPrefCommunication());
												}	

												break;
											}
										}
									}
								}
							}	
						}
					}
				} else {
					if ( (!certificationStatus.contains(certification_status.Certified.toString()) && !certification_status.Incomplete
									.toString().equalsIgnoreCase(certificationStatus)) && !certification_status.Suspended.toString().equalsIgnoreCase(certificationStatus)) {
						List<DesignateBroker> desigBrokerList = designateService.findDesigBrokerByAgentId(brokerObj.getId());
						for(DesignateBroker designateBroker : desigBrokerList){
							if(DesignateBroker.Status.Active.equals(designateBroker.getStatus())){
								brokerMgmtUtils.updateEnrollmentDetails(brokerObj, BrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL,
										user.getUsername(),new Long(designateBroker.getExternalIndividualId()), BrokerConstants.AGENT_ROLE,
										BrokerConstants.REMOVEACTIONFLAG);
								//De-designating individual
								designateService.deDesignateIndividualForBroker(designateBroker);
							}else if(DesignateBroker.Status.Pending.equals(designateBroker.getStatus())){
								//De-designating individual
								designateService.deDesignateIndividualForBroker(designateBroker);
							}
						}
					}
					
					if (updatedBrokerObj != null && updatedBrokerObj.getAgencyId()!=null && updatedBrokerObj.getAgencyId()!=0 && (updatedBrokerObj.getUser()==null || !userService.hasUserRole(updatedBrokerObj.getUser(), RoleService.AGENCY_MANAGER ))) {
						if(updatedBrokerObj.getCertificationStatus().toString().contains(certification_status.Certified.toString())){
							brokerIndTriggerService.triggerInd54(updatedBrokerObj);
						}
					}
				}
			}else {
				// As status has not been changed, save related information
				// but
				// do not trigger IND35
				if (  (certificationStatus != null)  
						&& (certificationStatus
								.equalsIgnoreCase(certification_status.Certified.toString())) && (!EntityUtils.isEmpty(newStartDate) && !EntityUtils.isEmpty(newEndDate))   ) {

					SimpleDateFormat dateFormat = new SimpleDateFormat(REQUIRED_FORMAT);
					Date startDate = dateFormat.parse(newStartDate);
					Date endDate = dateFormat.parse(newEndDate);

					brokerObj.setCertificationDate(startDate);
					brokerObj.setReCertificationDate(endDate);
				}
				brokerService.saveBrokerWithLocation(brokerObj);
			}
		} catch(GIRuntimeException giexception) {
			LOGGER.error("Cannot update Broker's Certification Status : ", giexception);
			throw giexception;
		} catch(Exception exception) {
			LOGGER.error("Cannot update Broker's Certification Status: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	private Location getLocationFromDTO(LocationDTO locationDto){
		if(locationDto == null){
			return null;
		}
		Location l = new Location();
		l.setAddress1(locationDto.getAddressLine1());
		l.setAddress2(locationDto.getAddressLine2());
		l.setCity(locationDto.getCity());
		l.setState(locationDto.getState());
		l.setZip(locationDto.getZipcode());
		l.setCounty(locationDto.getCountyName());
		l.setCountycode(locationDto.getCountyCode());
		return l;
	}
	
	private void validateBrokerCerification(BindingResult bindingResult,
			String newStartDate, String newEndDate, Broker brkObj) throws Exception {
		List<Object> validationGroups = new ArrayList<Object>();
		validationGroups.add( Broker.BrokerCerificationChange.class);

		if(( brkObj.getCertificationStatus() != null)&& (brkObj.getCertificationStatus().toString().equalsIgnoreCase(certification_status.Certified.toString()) )){
			SimpleDateFormat dateFormat = new SimpleDateFormat(REQUIRED_FORMAT);
			try{
				brkObj.setCertificationDate(dateFormat.parse(newStartDate)) ;
			}catch(ParseException parseException){
				brkObj.setCertificationDate(null) ;
			}
			try{
				brkObj.setReCertificationDate(dateFormat.parse(newEndDate));
			}catch(ParseException parseException){
				brkObj.setReCertificationDate(null);
			}

			Date midNightToday;
			try {
				midNightToday = dateFormat.parse(dateFormat.format(new TSDate()));
				if( !(dateFormat.parse(newEndDate).getTime() >= midNightToday.getTime() )){
					throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION); 
				}
			} catch (Exception e) {

				throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION); 
			}

			validationGroups.add(Broker.BrokerCertifiedStatus.class);
		}
		smartValidator.validate(brkObj,bindingResult, validationGroups.toArray());

		if(bindingResult.hasErrors()){
			throw new Exception(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);	
		}
	}

	/**
	 * Handles the POST request for view Broker's Certification Information
	 *
	 * @param broker
	 * @param request
	 * @param result
	 * @param model
	 * @return URL for navigation to edit certification status page.
	 */
	@RequestMapping(value = "/admin/broker/viewcertificationinformation", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'BROKER_EDIT_BROKER')")
	public String submitadmincertificationinformation(@ModelAttribute(BROKER) Broker broker, BindingResult result,
			Model model, HttpServletRequest request) {
		LOGGER.info("submitadmincertificationinformation: START ");
		LOGGER.info("submitadmincertificationinformation: END ");

		return "redirect:/admin/broker/certificationapplication";
	}

	/**
	 * Handles the GET request for view Broker's Certification Information
	 *
	 * @param model The current Model instance.
	 * @param request The HttpServletRequest instance.
	 * @param isCertified The isCertified flag.
	 * @param id The current broker identifier.
	 * @return URL for navigation to edit certification status page.
	 * @throws GIException 
	 */
	@RequestMapping(value = "/admin/broker/viewcertificationinformation/{encBrokerId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String viewcertificationinformation(Model model, HttpServletRequest request,
			@RequestParam(value = "isCertified", required = false) String isCertified, @PathVariable("encBrokerId") String encBrokerId) 
					throws GIRuntimeException {

		LOGGER.info("viewcertificationinformation: START ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: View Certification Information");
		Broker brokerObj = null;
		String decryptedBrokerId = null;
		int id;
		try{
			decryptedBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			id = Integer.parseInt(decryptedBrokerId);

			brokerObj = brokerService.getBrokerDetail(id);
			
			Validate.notNull(brokerObj);
			//HIX-42075 -Start	

			model.addAttribute(ALL_MAIL_NOTICES,DynamicPropertiesUtil.getPropertyValue(AEEConfigurationEnum.AGENT_ALLOWMAILNOTICES));
			//HIX-42075 -End	
			model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
			if(!BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
				brokerObj = formatFederalEIN(brokerObj);
			}
			model.addAttribute(BROKER, brokerObj);
			model.addAttribute(STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));

			if(null != brokerObj) {
				model.addAttribute(LOCATION_OBJ, brokerObj.getLocation());
				model.addAttribute(MAILING_LOCATION_OBJ, brokerObj.getMailingLocation());

				this.populateSwitchResourceName(model, brokerObj);

				getContactNumber(model, brokerObj);

				if (brokerObj.getLocation() != null && brokerObj.getMailingLocation() != null) {
					int comparisonResult = brokerObj.getLocation().compareTo(brokerObj.getMailingLocation());
					model.addAttribute("locationMatching", comparisonResult == 0 ? "Y" : "N");
				}
			}
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			
			//null != brokerObj.getUser() && -- Updated condition for view only (HIX-107742)
			if( null != brokerObj.getAgencyId()   ) {
				model.addAttribute("ROLECHANGEALLOWED","TRUE");
				if (userService.hasUserRole(brokerObj.getUser(), RoleService.AGENCY_MANAGER)) {
					model.addAttribute("CURRENTROLE","AGENCYMANAGER");
				}else {
					model.addAttribute("CURRENTROLE","BROKER");
				}
			}
			
			setEncryptedUserNameInModel(model, brokerObj);
		}catch(GIRuntimeException giexception) {			
			String errMsg = "Decrypted BrokerId's value is: "+decryptedBrokerId+".Failed to view certification information.Exception is  "+giexception;
			LOGGER.error(errMsg);
			LOGGER.info("viewcertificationinformation: END ");			
			throw giexception;
		}	
		catch(NumberFormatException nfe) {
			String errMsg = "Failed to decrypt BrokerId. Decrypted BrokerId's value is: "+decryptedBrokerId+", Exception is: "+nfe;
			LOGGER.error(errMsg);
			LOGGER.info("viewcertificationinformation: END ");
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);  		    
		}
		catch(Exception exception) {
			String errMsg = "Decrypted BrokerId's value is: "+decryptedBrokerId+".Failed to view certification information.Exception is  "+exception;
			LOGGER.error(errMsg);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}  		
		LOGGER.info("viewcertificationinformation: END ");
		return "admin/broker/viewcertificationinformation";
	}

	/**
	 * Handles the POST request to resend Broker's Account Activation Link
	 *
	 * @param model The current Model instance.
	 * @param request The current HttpServletRequest instance.
	 * @return URL for navigation to viewProfile page.
	 * @throws GIRuntimeException 
	 */
	@RequestMapping(value = "/admin/broker/resendactivationlink", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String resendActivationLink(Model model, HttpServletRequest request) throws GIRuntimeException
	{		
		
		LOGGER.info("resendActivationLink: START ");
		String decryptBrokerId = null;
		int id;
		String status = BrokerConstants.RESPONSE_FAILURE;
		
		try{
			
			String encBrokerId = request.getParameter("brokerid");
			
			if(!StringUtils.isEmpty(encBrokerId)) {
				
				decryptBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
				id = Integer.parseInt(decryptBrokerId);
				Broker brokerObj = brokerService.getBrokerDetail(id);
				
				if (null != brokerObj) {
					
					final int expiryDays = StringUtils.isNumeric(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.USER)) ?	
							Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.USER)) : 30 ;

					EXPIRATION_DAYS_STR = String.valueOf(expiryDays);

					Map<String, String> consumerActivationDetails = new HashMap<String, String>();
					consumerActivationDetails.put("consumerName", brokerObj.getFirstName() + " " + brokerObj.getLastName());
					consumerActivationDetails.put("exchangeName", EXCHANGE_NAME);
					consumerActivationDetails.put("exchangePhone", EXCHANGE_PHONE);
					consumerActivationDetails.put("exchangeURL", EXCHANGE_URL);
					consumerActivationDetails.put("emailType", ADMIN_ACCOUNT_ACTIVATION_EMAIL);
					consumerActivationDetails.put("expirationDays", EXPIRATION_DAYS_STR);
					consumerActivationDetails.put("name", brokerObj.getFirstName() + " " + brokerObj.getLastName());

					CreatedObject createdObject = new CreatedObject();
					createdObject.setObjectId(brokerObj.getId());
					createdObject.setEmailId(brokerObj.getYourPublicEmail());
					createdObject.setRoleName(GhixRole.BROKER.toString());
					createdObject.setPhoneNumbers(Arrays.asList(brokerObj.getContactNumber().replaceAll("-", "")));
					createdObject.setFullName(brokerObj.getFirstName() + " " + brokerObj.getLastName());
					createdObject.setFirstName(brokerObj.getFirstName());
					createdObject.setLastName(brokerObj.getLastName());
					createdObject.setCustomeFields(consumerActivationDetails);

					CreatorObject creatorObject = new CreatorObject();
					creatorObject.setObjectId(0);
					creatorObject.setFullName(ADMINISTRATOR);
					creatorObject.setRoleName(GhixRole.ADMIN.toString());
					
					
					AccountActivation accountActivation = accountActivationService.getAccountActivationByCreatedObjectIdAndCreatorObjectId(
							createdObject.getObjectId(), createdObject.getRoleName(),creatorObject.getObjectId(), creatorObject.getRoleName(), STATUS.NOTPROCESSED);

					if (accountActivation != null) {
						accountActivation = accountActivationService.resendActivationMail(createdObject, creatorObject,expiryDays, accountActivation);
					}
					else // 3. If the activation object is null, then call initiateActivationForCreatedRecord method to create a new object
					{
						accountActivation = accountActivationService.initiateActivationForCreatedRecord(createdObject,creatorObject, expiryDays);
					}
					
					if (accountActivation != null) {
						status = BrokerConstants.RESPONSE_SUCCESS;
					}
					
				}
			}
			
		}catch(GIRuntimeException giexception) {
			String errMsg = "Failed to resend activation mail. Decrypted BrokerId's value is: "+decryptBrokerId+", Exception is: "+giexception;
			LOGGER.error(errMsg);
			LOGGER.info("resendActivationLink: END ");		
		}catch(Exception e) {
			String errMsg = "Failed to resend activation mail. Decrypted BrokerId's value is: "+decryptBrokerId+", Exception is: "+e;
			LOGGER.error(errMsg);
			LOGGER.info("resendActivationLink: END ");
		}
		LOGGER.info("resendActivationLink: END ");
		return status;
		
	}

	private Broker formatFederalEIN(Broker broker){
		if(broker!=null && broker.getFederalEIN()!=null){
			StringBuilder sb = new StringBuilder();
			String federalEIN = broker.getFederalEIN();
			sb.append("***-**-"+federalEIN.substring(federalEIN.length()-4, federalEIN.length()));
			broker.setFederalEIN(sb.toString());			
		}
		return broker;
	}


	/**
	 * Handles the POST request for Broker's Certification Status
	 *
	 * @param broker
	 * @param request
	 * @param result
	 * @param model
	 * @return URL for navigation to edit certification status page.
	 */
	@RequestMapping(value = "/admin/broker/certificationstatus", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String submitadmincertificationstatus(@ModelAttribute(BROKER) Broker broker, BindingResult result,
			Model model, HttpServletRequest request) {

		LOGGER.info("submitadmincertificationstatus: START ");
		LOGGER.info("submitadmincertificationstatus: END ");
		return "admin/broker/certificationstatus";
	}

	/**
	 * Handles the GET request for Broker's Certification Status
	 *
	 * @param model The current Model instance
	 * @param request The current HttpServletRequest instance.
	 * @param id The current broker identifier.
	 * @return URL for navigation to edit certification status page.
	 * @throws GIException 
	 */
	@RequestMapping(value = "/admin/broker/certificationstatus/{encBrokerId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String certificationstatus(Model model, HttpServletRequest request, @PathVariable("encBrokerId") String encBrokerId)
			throws GIRuntimeException {

		LOGGER.info("certificationstatus: START ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Certification Status");
		Broker brokerObj = null;
		String decryptedBrokerId = null;
		int id;
		try{
			decryptedBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			id = Integer.parseInt(decryptedBrokerId);

			brokerObj = brokerService.getBrokerDetail(id);
			if (brokerObj != null) {
				this.populateSwitchResourceName(model, brokerObj);
				model.addAttribute(CERTIFICATION_STATUS, brokerObj.getCertificationStatus());
			}
			model.addAttribute(BROKER, brokerObj);

			// get the broker details from broker audit table
			List<Map<String, Object>> brokerCertStatusHistory = brokerService.loadBrokerStatusHistory(id);
			model.addAttribute(STATUS_HISTORY, brokerCertStatusHistory);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			
			setEncryptedUserNameInModel(model, brokerObj);
		}catch(GIRuntimeException giexception) {			
			String errMsg = "Decrypted BrokerId's value is: "+decryptedBrokerId+".Failed to view certification status.Exception is "+giexception;
			LOGGER.error(errMsg);
			LOGGER.info("certificationstatus: END ");		
			throw giexception;
		}	
		catch(NumberFormatException nfe){
			String errMsg = "Failed to decrypt BrokerId. Decrypted BrokerId's value is: "+decryptedBrokerId+", Exception is: "+nfe;
			LOGGER.error(errMsg);
			LOGGER.info("certificationstatus: END ");
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		catch(Exception e) {			
			String errMsg = "Decrypted BrokerId's value is: "+decryptedBrokerId+".Failed to view certification status.Exception is "+e;
			LOGGER.error(errMsg);
			LOGGER.info("certificationstatus: END ");
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} 		
		LOGGER.info("certificationstatus: END ");
		return "admin/broker/certificationstatus";
	}

	/**
	 * Handles the POST request for Broker's View Profile
	 *
	 * @param broker
	 * @param request
	 * @param result
	 * @param model
	 * @return URL for navigation to edit certification status page.
	 */
	@RequestMapping(value = "/admin/broker/viewprofile", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String submitBrokerProfile(@ModelAttribute(BROKER) Broker broker, BindingResult result, Model model,
			HttpServletRequest request) {

		LOGGER.info("submitBrokerProfile: START ");
		LOGGER.info("submitBrokerProfile: END ");
		return "admin/broker/viewprofile";
	}

	/**
	 * Handles the GET request for Broker's View Profile
	 *
	 * @param model The current Model instance.
	 * @param request The current HttpServletRequest instance.
	 * @param id The current broker identifier.
	 * @return URL for navigation to edit certification status page.
	 * @throws GIException 
	 */
	@RequestMapping(value = "/admin/broker/viewprofile/{encBrokerId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String viewprofile(Model model, HttpServletRequest request, @PathVariable("encBrokerId") String encBrokerId) throws GIRuntimeException
	{		
		LOGGER.info("viewprofile: START ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: View Broker Profile");
		Broker brokerObj = null;
		String decryptBrokerId = null;
		int brokerId;
		try{
			decryptBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			brokerId = Integer.parseInt(decryptBrokerId);
			brokerObj = brokerService.getBrokerDetail(brokerId);

			if(null != brokerObj) {
				String productExperties = brokerObj.getProductExpertise();
				if (productExperties != null) {
					List<String> productExpertiesList = EntityUtils.splitStringValues(productExperties, ",");
					StringBuffer productExpertiesStrBuffer = new StringBuffer();
					for (String productExpertiesListStr : productExpertiesList) {
						productExpertiesStrBuffer.append(productExpertiesListStr);
						productExpertiesStrBuffer.append(",");
						productExpertiesStrBuffer.append(" ");
					}
					productExperties = productExpertiesStrBuffer.substring(0, productExpertiesStrBuffer.length() - 2);
				}
				brokerObj.setProductExpertise(productExperties);

				this.populateSwitchResourceName(model, brokerObj);

				model.addAttribute(BROKER, brokerObj);
				model.addAttribute(LOCATION_OBJ, brokerObj.getLocation());
				model.addAttribute(MAILING_LOCATION_OBJ, brokerObj.getMailingLocation());
				model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
				model.addAttribute("ca_statecode", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
				model.addAttribute(STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
			}

			if (   (!StringUtils.isBlank(request.getParameter("cancel")))&& (request.getParameter("cancel").trim().equalsIgnoreCase("true"))   ) {
				model.addAttribute(UPDATEPROFILE, false);
			}

			setEncryptedUserNameInModel(model, brokerObj);
		}catch(GIRuntimeException giexception) {
			String errMsg = "Failed to view Agent Profile. Decrypted BrokerId's value is: "+decryptBrokerId+", Exception is: "+giexception;
			LOGGER.error(errMsg);
			LOGGER.info("viewprofile: END ");		
			throw giexception;
		}	
		catch(NumberFormatException nfe) {
			String errMsg = "Failed to decrypt Broker id. Decrypted BrokerId's value is: "+decryptBrokerId+", Exception is: "+nfe;
			LOGGER.error(errMsg);
			LOGGER.info("viewprofile: END ");
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		catch(Exception e) {
			String errMsg = "Failed to view Agent Profile. Decrypted BrokerId's value is: "+decryptBrokerId+", Exception is: "+e;
			LOGGER.error(errMsg);
			LOGGER.info("viewprofile: END ");
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("viewprofile: END ");
		return "admin/broker/viewprofile";
	}
	
	/**
	 * Handles the GET request for Broker's View Profile
	 * 
	 * @param broker
	 * @param request
	 * @param id
	 * @param model
	 * @return URL for navigation to edit certification status page.
	 */
	@RequestMapping(value = "/admin/broker/editprofile/{encBrokerId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String editprofile(Model model, HttpServletRequest request, @PathVariable("encBrokerId") String encBrokerId) throws GIRuntimeException{
		LOGGER.info("editprofile: START ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: View Broker Profile");
		String decryptBrokerId = null;
		int id;
		
		try{
			decryptBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			id = Integer.parseInt(decryptBrokerId);
			Broker brokerObj = brokerService.getBrokerDetail(id);
			model.addAttribute(BROKER, brokerObj);
			model.addAttribute(LOCATION_OBJ, brokerObj.getLocation());
			model.addAttribute(MAILING_LOCATION_OBJ, brokerObj.getMailingLocation());
			model.addAttribute(LANGUAGES_LIST, loadLanguagesData());
			model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
	
			if (   (!StringUtils.isBlank(request.getParameter("cancel")))&& (request.getParameter("cancel").trim().equalsIgnoreCase("true"))   ) {
				model.addAttribute(UPDATEPROFILE, false);
			}
			
			if (brokerObj.getContactNumber() != null) {
				model.addAttribute("phone1", brokerObj.getContactNumber().substring(0, 3));
				model.addAttribute("phone2", brokerObj.getContactNumber().substring(4, 7));
				model.addAttribute("phone3", brokerObj.getContactNumber().substring(8, 12));
			}
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			
			setEncryptedUserNameInModel(model, brokerObj);
		}catch(GIRuntimeException giexception) {
			String errMsg = "Failed to view Agent Profile. Decrypted BrokerId's value is: "+decryptBrokerId+", Exception is: "+giexception;
			LOGGER.error(errMsg);
			LOGGER.info("editprofile: END ");		
			throw giexception;
		}	
		catch(NumberFormatException nfe) {
			String errMsg = "Failed to decrypt Broker id. Decrypted BrokerId's value is: "+decryptBrokerId+", Exception is: "+nfe;
			LOGGER.error(errMsg);
			LOGGER.info("editprofile: END ");
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		catch(Exception e) {
			String errMsg = "Failed to view Agent Profile. Decrypted BrokerId's value is: "+decryptBrokerId+", Exception is: "+e;
			LOGGER.error(errMsg);
			LOGGER.info("editprofile: END ");
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("editprofile: END ");
		return "admin/broker/editprofile";
	}
	
	@RequestMapping(value = "/admin/broker/checkEmailAvailable/{encBrokerId}", method = RequestMethod.GET)
	@ResponseBody
	public String checkUsernameAvailable( @RequestParam(value = "userName", required = true) String userName, @PathVariable("encBrokerId") String encBrokerId) throws GIRuntimeException{
		String available= "false";
		String decryptBrokerId = null;
		int id;
		
		try{
			decryptBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			id = Integer.parseInt(decryptBrokerId);
			Broker brokerObj = brokerService.getBrokerDetail(id);
			
			AccountUser existingUser = userService.findByEmail(userName.toLowerCase());
			
			if( existingUser == null ||  (existingUser.getId()== brokerObj.getUser().getId())    ){
				available = "true";
			}
			
		}catch(Exception e) {
				String errMsg = "Failed to view Agent Profile. Decrypted BrokerId's value is: "+decryptBrokerId+", Exception is: "+e;
				LOGGER.error(errMsg);
		}
		LOGGER.info("checkExistingUsername : END ");
		 
		return available;
	}
	
	private void setEncryptedUserNameInModel(Model model, Broker brokerObj){
		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
			if(brokerObj!=null && brokerObj.getUser()!=null && !EntityUtils.isEmpty(brokerObj.getUser().getUsername())){
	            model.addAttribute(BrokerConstants.ENCRYPTED_USER_NAME, ghixJasyptEncrytorUtil.encryptStringByJasypt(brokerObj.getUser().getUserName()));
	        }
		}
	}
	
	@RequestMapping(value = "/admin/broker/updateprofile/{encBrokerId}", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String editProfile(@ModelAttribute(BROKER) Broker broker, Model model,
			HttpServletRequest request, @PathVariable("encBrokerId") String encBrokerId) throws InvalidUserException {
		
		LOGGER.info("editProfile: START ");
		String decryptBrokerId = null;
		int id;
		
		try{
			decryptBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			id = Integer.parseInt(decryptBrokerId);
			Broker brokerObj = brokerService.getBrokerDetail(id);
			if(brokerObj != null) {
				brokerObj.setClientsServed(broker.getClientsServed());
				brokerObj.setProductExpertise(broker.getProductExpertise());
				brokerObj.setYourWebSite(broker.getYourWebSite());
				
				//For brokers attached to agency, dont have facility to update address from profile page
				if(null == brokerObj.getAgencyId()){
					brokerObj.setLocation(broker.getLocation());	
				}
				
				validateLanguagesSpoken(broker);
				brokerObj.setLanguagesSpoken(broker.getLanguagesSpoken());
				
				String newEmailAddress = request.getParameter(EMAIL);
				if(newEmailAddress != null && !newEmailAddress.trim().isEmpty()){
					brokerObj.setYourPublicEmail(newEmailAddress);
				}
				
				//Fetching first 4000 char for About me text;
				String aboutMe = broker.getAboutMe();
				int beginIndex = 0;
				int endIndex = aboutMe.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  aboutMe.length();
				aboutMe = aboutMe.substring(beginIndex, endIndex);
				brokerObj.setAboutMe(aboutMe);
				/*
				 * =========================================================
				 * HIX-33269 
				 * ALM: 10962 - SHOP - Profile pg in Edit Mode [AGN, AM]: 
				 * Agent and Agent Manager is unable to edit the email on the Profile page because the field is missing in edit mode
				 */
				
				/*if(brokerObj.getUser() != null && newEmailAddress != null && !newEmailAddress.trim().isEmpty()){
					brokerObj.getUser().setEmail(newEmailAddress);
					LOGGER.debug("editProfile : updated new email address: "+newEmailAddress+", for the brokerID: "+brokerObj.getId());
				}*/
				
				/*
				 * =========================================================
				 */
				brokerService.saveBrokerWithLocation(brokerObj);
			}
		}catch(GIRuntimeException giexception) {
			String errMsg = "Failed to view Agent Profile. Decrypted BrokerId's value is: "+decryptBrokerId+", Exception is: "+giexception;
			LOGGER.error(errMsg);
			LOGGER.info("editprofile: END ");		
			throw giexception;
		}	
		catch(NumberFormatException nfe) {
			String errMsg = "Failed to decrypt Broker id. Decrypted BrokerId's value is: "+decryptBrokerId+", Exception is: "+nfe;
			LOGGER.error(errMsg);
			LOGGER.info("editprofile: END ");
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		catch(Exception e) {
			String errMsg = "Failed to view Agent Profile. Decrypted BrokerId's value is: "+decryptBrokerId+", Exception is: "+e;
			LOGGER.error(errMsg);
			LOGGER.info("editprofile: END ");
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("editProfile: END ");
		return "redirect:/admin/broker/viewprofile/" + encBrokerId;
	}

	/**
	 * Handles the GET/POST request for Edit Broker information
	 *
	 * @param request
	 * @param id
	 * @param model
	 * @return URL for navigation to edit certification status page.
	 * @throws GIException 
	 */
	@RequestMapping(value = "/admin/broker/certificationapplication/{encBrokerId}", method = { RequestMethod.GET,
			RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String certification(Model model, HttpServletRequest request,
			@RequestParam(value = "isCertified", required = false) String isCertified, @PathVariable("encBrokerId") String encBrokerId) throws GIRuntimeException {
		LOGGER.info("certification: START ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Certification Application");
		String decryptedBrokerId = null;
		Broker brokerObj = null;
		int id;
		try{
			//HIX-42075-START
			model.addAttribute(ALL_MAIL_NOTICES,DynamicPropertiesUtil.getPropertyValue(AEEConfigurationEnum.AGENT_ALLOWMAILNOTICES));
			//HIX-42075-END
			decryptedBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			id = Integer.parseInt(decryptedBrokerId);
			brokerObj = brokerService.getBrokerDetail(id);
			request.getSession().setAttribute("designatedBrokerProfile", brokerObj);
			model.addAttribute(LIST_OF_STATES, brokerMgmtUtils.filterStates());
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			model.addAttribute(STATE_CODE,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
			setModelAttributes(model, brokerObj);
			request.getSession().setAttribute("designateBroker", null);
			
			setEncryptedUserNameInModel(model, brokerObj);
			
			if(brokerObj.getAgencyId()!=null){
				List<AgencySiteDto> agencySiteDtos = null;
				ResponseEntity<List<AgencySiteDto>> response = restTemplate.exchange(GhixEndPoints.GHIX_BROKER_URL+"agency/site/list/"+brokerObj.getAgencyId(), HttpMethod.GET, null, new ParameterizedTypeReference<List<AgencySiteDto>>() {
	            });
				if(response != null){
					agencySiteDtos = response.getBody();
				}
				model.addAttribute(LIST_OF_AGENCY_SITES, agencySiteDtos);
				
				JsonArray jsonArray = new JsonArray();
				for (Iterator iterator = agencySiteDtos.iterator(); iterator.hasNext();) {
					AgencySiteDto agencySiteDto = (AgencySiteDto) iterator.next();
					
					JsonObject jsonObject = new JsonObject();
					jsonObject.addProperty("id", agencySiteDto.getId());
					jsonObject.addProperty("siteType", agencySiteDto.getSiteType());
					
					if(agencySiteDto.getLocation()!= null) {
						jsonObject.addProperty("address1", agencySiteDto.getLocation().getAddress1());
						jsonObject.addProperty("address2", agencySiteDto.getLocation().getAddress2());
						jsonObject.addProperty("city", agencySiteDto.getLocation().getCity());
						jsonObject.addProperty("state", agencySiteDto.getLocation().getState());
						jsonObject.addProperty("zip", agencySiteDto.getLocation().getZip());
						jsonObject.addProperty("lat", agencySiteDto.getLocation().getLat());
						jsonObject.addProperty("lon", agencySiteDto.getLocation().getLon());
					}else {
						String stringNull = null;
						jsonObject.addProperty("address1",stringNull);
						jsonObject.addProperty("address2",stringNull);
						jsonObject.addProperty("city", stringNull);
						jsonObject.addProperty("state", stringNull);
						jsonObject.addProperty("zip", stringNull);
						jsonObject.addProperty("lat", stringNull);
						jsonObject.addProperty("lon",stringNull);
					}
					
					
					jsonArray.add(jsonObject);
				}
				
				model.addAttribute(LIST_OF_AGENCY_SITES+"JsonString",jsonArray.toString() );
				
				if(brokerObj.getUser() != null) {
					if(null != brokerObj.getCertificationStatus()  && !"Incomplete".equalsIgnoreCase(brokerObj.getCertificationStatus() )) {
						
						//Allowed only if Role is other than InComplete
						model.addAttribute("ROLECHANGEALLOWED","TRUE");
						
						//If role is other than Certified then Show "NO SERVICE" warning.
						if(!"Certified".equalsIgnoreCase(brokerObj.getCertificationStatus())) {
							model.addAttribute("NOSERVICE","TRUE");
						}
						
						if (userService.hasUserRole(brokerObj.getUser(), RoleService.AGENCY_MANAGER)) {
							model.addAttribute("CURRENTROLE","AGENCYMANAGER");
							int amCount = brokerService.getAgencyManagerCount(brokerObj.getAgencyId(), RoleService.AGENCY_MANAGER);
							if(amCount == 1) {
								model.addAttribute("AMWONTAVAILABLE","TRUE");
							}
						}else {
							model.addAttribute("CURRENTROLE","BROKER");
						}
					}   
				}
				
			}else {
				model.addAttribute(LIST_OF_AGENCY_SITES+"JsonString","[]" );
			}
			
		}catch(GIRuntimeException giexception) {
			String errMsg = "Failed to decrypt BrokerId. Decrypted BrokerId's value is: "+decryptedBrokerId+", Exception is: "+giexception;
			LOGGER.error(errMsg);
			LOGGER.info("certification:  END ");		
			throw giexception;
		}	
		catch(NumberFormatException nfe){
			String errMsg = "Failed to decrypt BrokerId. Decrypted BrokerId's value is: "+decryptedBrokerId+", Exception is: "+nfe;
			LOGGER.error(errMsg);
			LOGGER.info("certification: END ");
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		catch(Exception e) {
			String errMsg = "Failed to decrypt BrokerId. Decrypted BrokerId's value is: "+decryptedBrokerId+", Exception is: "+e;
			LOGGER.error(errMsg);
			LOGGER.info("certification:  END ");
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("certification: END ");
		return "admin/broker/certificationapplication";
	}

	/**
	 * Handles the GET request for loading comments screen for broker.
	 *
	 * @param id The comment identifier.
	 * @param request The current HttpServletRequest.
	 * @param model The current Model instance.
	 * @return String The view name.
	 * @throws InvalidUserException The InvalidUserException instance.
	 * @throws GIException 
	 */
	@RequestMapping(value = "/admin/broker/comments/{encBrokerId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String getComments(@PathVariable("encBrokerId") String encBrokerId, HttpServletRequest request, Model model)
			throws GIRuntimeException {
		LOGGER.info("broker - getComments : START ");
		String decryptedBrokerId = null;
		int id;
		try {
			decryptedBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			id = Integer.parseInt(decryptedBrokerId);
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Comments");
			Broker brokerObj = brokerService.getBrokerDetail(id);

			if(null != brokerObj) {
				this.populateSwitchResourceName(model, brokerObj);
			}

			model.addAttribute(BROKER, brokerObj);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			
			setEncryptedUserNameInModel(model, brokerObj);
		}
		catch(GIRuntimeException giexception) {
			String errMsg = "Failed to decrypt BrokerId. Decrypted BrokerId's value is: "+decryptedBrokerId+", Exception is: "+giexception;
			LOGGER.error(errMsg);
			LOGGER.info("broker - getComments : END ");	
			throw giexception;
		}
		catch (Exception e) {
			LOGGER.error("Error in broker - getComments: "+e);
			LOGGER.info("viewprofile: END ");
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("broker - getComments : END ");
		return "admin/broker/comments";
	}

	/**
	 * Handles the GET/POST request for saving comments screen for broker.
	 *
	 * @param target_id
	 * @param target_name
	 * @param employerName
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/admin/broker/newcomment", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String addNewComment(@RequestParam(value = "target_id", required = false) Integer target_id,
			@RequestParam(value = "target_name", required = false) String target_name,
			@RequestParam(value = "brokerName", required = false) String brokerName, Model model,
			HttpServletRequest request) throws GIRuntimeException {

		LOGGER.info("broker - addNewComment : START");
		try {
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Comments");
			model.addAttribute("target_id", target_id);
			model.addAttribute("target_name", target_name);
		} catch(GIRuntimeException giexception) {
			LOGGER.error("Error in broker - addNewComment: ", giexception);
			throw giexception;
		} catch(Exception e) {
			LOGGER.error("Error in broker - addNewComment: ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("broker - addNewComment : END");
		return "/admin/broker/addnewcomment";
	}

	/**
	 * Handles GET request for displaying security questions
	 *
	 * @param model The current Model instance.
	 * @param id The security question identifier.
	 * @return String The view name.
	 */
	/*@RequestMapping(value = "/admin/broker/securityquestions/{encBrokerId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	public String getSecurityQuestions(Model model, @PathVariable(value = "encBrokerId") String encBrokerId)
			throws GIRuntimeException {

		LOGGER.info("broker - getSecurityQuestions : START");
		String decryptedBrokerId = null;
		int id;
		try {
			decryptedBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			id = Integer.parseInt(decryptedBrokerId);
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Security Questions");
			Broker brokerObj = brokerService.getBrokerDetail(id);

			if(null != brokerObj) {
				this.populateSwitchResourceName(model, brokerObj);
				List<QuestionAnswer> brokerQstnAnsList = AccountUserDto.getUserSecurityQuestionAnswers(brokerObj.getUser());
				model.addAttribute("userSecQstnAnsList",brokerQstnAnsList);
			}
			model.addAttribute(BROKER, brokerObj);

		} catch(GIRuntimeException giexception) {
			LOGGER.error("Error in displaying getSecurityQuestions: ", giexception);
			throw giexception;
		} catch(Exception e) {
			LOGGER.error("Error in displaying getSecurityQuestions: ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} 
		LOGGER.info("broker - getSecurityQuestions : END");
		return "admin/broker/securityquestions";
	}*/

	/**
	 * Handles GET/POST request for displaying ticket history
	 *
	 * @param id The current broker identifier.
	 * @param model The current Model instance
	 * @param request The current HttpServletRequest instance.
	 * @return String The view name.
	 */
	@RequestMapping(value = "/admin/broker/tickethistory/{encBrokerId}", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_ADD_TICKET')")
	public String getBrokerTicketHistory(@PathVariable("encBrokerId") String encBrokerId, Model model)
			throws GIRuntimeException {

		LOGGER.info("broker - getBrokerTicketHistory : START");
		String decryptedBrokerId = null;
		int id;
		List<TkmTickets> ticketHistoryList = null;

		try {
			decryptedBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			id = Integer.parseInt(decryptedBrokerId);
			Broker brokerObj = brokerService.getBrokerDetail(id);

			if(null != brokerObj) {
				ticketHistoryList = getTkmTicketsForBroker(brokerObj.getUser());
				this.populateSwitchResourceName(model, brokerObj);
			}

			model.addAttribute(BROKER, brokerObj);
			model.addAttribute("ticketHistoryList", ticketHistoryList);
			model.addAttribute(PAGE_SIZE, GhixConstants.PAGE_SIZE);

		} catch(GIRuntimeException giexception) {
			LOGGER.error("Error in broker - getBrokerTicketHistory: ", giexception);
			throw giexception;
		} catch(Exception e) {
			LOGGER.error("Error in broker - getBrokerTicketHistory: ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}   
		LOGGER.info("broker - getBrokerTicketHistory : END");
		return "admin/broker/tickethistory";
	}

	/**
	 * Retrieves all the tickets for the user from the ticket mgmt module
	 *
	 * @param userObj
	 * @return ticketsList
	 * @throws Exception
	 */
	private List<TkmTickets> getTkmTicketsForBroker(AccountUser userObj) throws GIRuntimeException {

		LOGGER.info("broker - getTkmTicketsForBroker : START");

		XStream xstream = GhixUtils.getXStreamStaxObject();
		List<TkmTickets> ticketsList = null;
		TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();

		try {
			List<UserRole> userRoleList = userRoleService.findByUser(userObj);
			tkmTicketsRequest.setRequester(userObj.getId());
			for (UserRole userRoleObj : userRoleList) {
				tkmTicketsRequest.setUserRoleId(userRoleObj.getRole().getId());
				break;
			}
			String postResp = restTemplate.postForObject(GhixEndPoints.TicketMgmtEndPoints.GETCONSUMERTICKETLIST,
					tkmTicketsRequest, String.class);
			TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(postResp);
			ticketsList = tkmTicketsResponse.getTkmTicketsList();
		} catch(GIRuntimeException giexception) {
			LOGGER.error("Error in fetching the ticket list for the employer: ", giexception);
			throw giexception;
		} catch(Exception e) {
			LOGGER.error("Error in fetching the ticket list for the employer: ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} 			
		LOGGER.info("broker - getTkmTicketsForBroker : END");

		return ticketsList;

	}

	/**
	 * Handles GET/POST request for displaying the ticket details.
	 *
	 * @param model
	 * @param request
	 * @param id
	 * @param brokerId
	 * @return
	 */
	@RequestMapping(value = "/admin/broker/ticketdetail/{id}", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_ADD_TICKET')")
	public String getBrokerTicketDetail(Model model, HttpServletRequest request, @PathVariable("id") Integer id,
			@RequestParam("brokerId") Integer brokerId) throws GIRuntimeException {

		LOGGER.info("broker - getBrokerTicketDetail : START");

		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();

		try {

			Broker brokerObj = brokerService.getBrokerDetail(brokerId);
			// fetch the ticket data by ticketId from the ticket management
			// module.
			String postResp = restTemplate.getForObject(
					TicketMgmtEndPoints.SEARCH_TKMTICKETS_ID + id,
					String.class);
			TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(postResp);
			TkmTickets tkmTicketsObj = tkmTicketsResponse.getTkmTickets();
			model.addAttribute(BROKER, brokerObj);
			model.addAttribute("tkmTicketsObj", tkmTicketsObj);

		} catch(GIRuntimeException giexception) {
			LOGGER.error("Error in broker - getBrokerTicketDetail: ", giexception);
			throw giexception;

		} catch(Exception e) {
			LOGGER.error("Error in broker - getBrokerTicketDetail: ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} 	
		LOGGER.info("broker - getBrokerTicketDetail : END");

		return "admin/broker/ticketdetail";
	}

	/**
	 * Handles the POST request for Edit Broker information
	 *
	 * @param request
	 * @param brokerId
	 * @param model
	 * @return URL for navigation to edit certification status page.
	 */
	@RequestMapping(value = "admin/broker/certificationapplication", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'BROKER_ADD_BROKER')")
	public String brokerCertificationSubmit(@ModelAttribute(BROKER) Broker broker,BindingResult bindingResult,
			@RequestParam(value = "licrenewaldate", required = false) String licenseRenewalDate,
			@RequestParam(value = "brokerId", required = false) Integer brokerId, Model model,
			HttpServletRequest request) throws GIRuntimeException {

		LOGGER.info("brokerCertificationSubmit: START ");
		if (   (broker.getLocation() != null && broker.getMailingLocation() != null)&& (broker.getMailingLocation().getState() == null && broker.getLocation().getState() != null)   ) {

			broker.getMailingLocation().setState(broker.getLocation().getState());

		}

		String certificationStatus = request.getParameter("certificationStatus");
		String firstName = "";
		String lastName = "";
		String encryptedBrkId = null;
		UserRole ur =null ;
		
		try {
			Broker brokerObj = null;
			encryptedBrkId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(brokerId));
			brokerObj = brokerService.getBrokerDetail(brokerId);
			String phone = request.getParameter("phone1") + "-" + request.getParameter("phone2") + "-"
					+ request.getParameter("phone3");
			brokerObj.setContactNumber(phone);
			brokerObj.setAlternatePhoneNumber(broker.getAlternatePhoneNumber());
			brokerObj.setBusinessContactPhoneNumber(broker.getBusinessContactPhoneNumber());
			if (broker.getYourWebSite() != null && !StringUtils.isEmpty(broker.getYourWebSite())) {
				brokerObj.setYourWebSite(broker.getYourWebSite());
			}
			brokerObj.setFaxNumber(broker.getFaxNumber());
			brokerObj.setPostalMailEnabled(broker.getPostalMailEnabled());
			setBrokerName(broker, firstName, lastName, brokerObj);
			brokerObj.setCommunicationPreference(broker.getCommunicationPreference());//This line has been added to resolve bug HIX-17094
			brokerObj.setNpn(broker.getNpn());
			if (brokerObj.getCertificationStatus() == null) {
				brokerObj.setCertificationStatus(certification_status.Pending.toString());
				brokerObj.setApplicationDate(new TSDate());
				brokerObj.setStatusChangeDate(new TSDate());
			}

			if (brokerObj.getCertificationStatus() != null
					&& certification_status.Certified.equals(brokerObj.getCertificationStatus())) {
				brokerObj.setCertificationNumber(generateBrokerCertificationNumber(brokerObj));
			}
			setBrokerObj(broker, brokerObj, request);
			if (!EntityUtils.isEmpty(licenseRenewalDate)) {
				SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
				Date licenseRenDate = dateFormat.parse(licenseRenewalDate);
				brokerObj.setLicenseRenewalDate(licenseRenDate);
			}

			validateBrokerRegFirstStep(bindingResult, brokerObj);
			brokerObj.setLastUpdatedBy(userService.getLoggedInUser().getId());
			
			brokerObj.setPersonalEmailAddress(broker.getPersonalEmailAddress());
			
			if(null != brokerObj.getAgencyId() && null != brokerObj.getUser() && null != request.getParameter("allotedRole")) {
				if(userService.hasUserRole(brokerObj.getUser(), RoleService.AGENCY_MANAGER) 
					&&	"ROLEBROKER".equalsIgnoreCase(request.getParameter("allotedRole"))) {
					ur = userService.getUserRoleByRoleName(brokerObj.getUser(), RoleService.AGENCY_MANAGER);
					
					Role rle = roleService.findRoleByName(RoleService.BROKER_ROLE);
					ur.setRole(rle);
					brokerObj.setChangeType("AgencyManagerToAgent");
					//Set role to Broker
				} else if("ROLEAGENCYMANAGER".equalsIgnoreCase(request.getParameter("allotedRole")) && userService.hasUserRole(brokerObj.getUser(), RoleService.BROKER_ROLE) ) {
					//Set Role to agency Manager
					ur = userService.getUserRoleByRoleName(brokerObj.getUser(), RoleService.BROKER_ROLE);
					Role rle = roleService.findRoleByName(RoleService.AGENCY_MANAGER);
					ur.setRole(rle);
					brokerObj.setChangeType("AgentToAgencyManager");
				}

				if(null != ur) {
					userRoleService.saveUserRole(ur);
				}
			}
			
			brokerService.saveBrokerWithLocation(brokerObj);

			model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());
			model.addAttribute(LOCATION_OBJ, brokerObj.getLocation());
			model.addAttribute(MAILING_LOCATION_OBJ, brokerObj.getMailingLocation());
			model.addAttribute(BROKER, brokerObj);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			request.getSession().setAttribute("designatedBrokerProfile", brokerObj);

			// request.getSession().setAttribute("broker", brokerObj);
		} catch(GIRuntimeException giexception) {
			LOGGER.error("Error while saving Broker details ");
			String errMsg = "Failed to encrypyt BrokerId. Encrypted BrokerId's value is: "+encryptedBrkId+", Exception is: "+giexception;
			LOGGER.error(errMsg);
			LOGGER.info("broker - getComments : END ");		    	
			throw giexception;
		} catch(Exception e) {
			LOGGER.error("Error while saving Broker details : ", e);
			LOGGER.info("brokerCertificationSubmit: END ");
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} 			
		LOGGER.info("brokerCertificationSubmit: END ");
		return "redirect:/admin/broker/viewcertificationinformation/" + encryptedBrkId;
	}

	private void validateBrokerRegFirstStep(BindingResult bindingResult,
			Broker brokerObj) throws Exception {
		List<Object> validationGroups = new ArrayList<Object>();
		validationGroups.add(Broker.BrokerRegistration.class);

		if(brokerObj.getBusinessContactPhoneNumber()!=null && !brokerObj.getBusinessContactPhoneNumber().trim().isEmpty())
		{
			validationGroups.add(Broker.BrokerBusinessContact.class);
		}

		if(brokerObj.getAlternatePhoneNumber()!=null && !brokerObj.getAlternatePhoneNumber().trim().isEmpty())
		{
			validationGroups.add(Broker.BrokerAlternateContact.class);
		}
		if("Fax".equalsIgnoreCase(brokerObj.getCommunicationPreference()) || (brokerObj.getFaxNumber()!=null && !brokerObj.getFaxNumber().trim().isEmpty())){
			validationGroups.add(Broker.BrokerFaxContact.class);
		}
		smartValidator.validate(brokerObj,bindingResult, validationGroups.toArray());

		if(bindingResult.hasErrors()){
			throw new Exception(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION); 	
		}
	}

	public static void fillRequestParamInModel(HttpServletRequest request,Model model){
		Enumeration<String> enumerationOfRequestParmeter = request.getParameterNames();
		while(enumerationOfRequestParmeter.hasMoreElements()){
			String parameterName = enumerationOfRequestParmeter.nextElement();
			model.addAttribute(parameterName,request.getParameter(parameterName));

		}
	}
	/**
	 * Retrieves {@link BrokerCertificationNumber} if exists, else generate
	 * broker certification number and set into {@link Broker} object
	 *
	 * @param broker
	 *            {@link Broker}
	 */
	private long generateBrokerCertificationNumber(Broker broker) {
		BrokerCertificationNumber brokerCertificatonNumberObj = brokerCertificationNumberRepository
				.findByBrokerId(broker.getId());
		if (brokerCertificatonNumberObj == null) {
			logger.info("Generating unique broker certification number for Broker : " + broker.getId());
			BrokerCertificationNumber brokerCertificationNumber = new BrokerCertificationNumber();
			brokerCertificationNumber.setBrokerId(broker.getId());
			brokerCertificatonNumberObj = brokerCertificationNumberRepository.saveAndFlush(brokerCertificationNumber);
			logger.info("Broker Certification Number : " + brokerCertificatonNumberObj.getId());
		}
		return brokerCertificatonNumberObj.getId();
	}

	private void setBrokerName(Broker broker, String firstName, String lastName, Broker brokerObj) {
		String fname = firstName;
		String lname = lastName;
		if (broker != null && broker.getUser()!=null) {
			fname = broker.getUser().getFirstName();
			lname = broker.getUser().getLastName();
		}

		if (fname != null && lname != null && brokerObj.getUser()!=null) {
			brokerObj.getUser().setFirstName(fname.trim());
			brokerObj.getUser().setLastName(lname.trim());
		}else if(fname != null && lname != null) {
			brokerObj.setFirstName(fname.trim());
			brokerObj.setLastName(lname.trim());
		}
	}

	private void setBrokerObj(Broker broker, Broker brokerObj, HttpServletRequest request) {

		if (broker != null) {
			
			if(null != brokerObj.getAgencyId()){
						
				String selectedAgencySiteId = ghixJasyptEncrytorUtil.decryptStringByJasypt(request.getParameter("select_agencyLocation"));
				BrokerSiteLocationDTO brokerSiteLocationDTO = new BrokerSiteLocationDTO();
				if(selectedAgencySiteId!=null){
						brokerSiteLocationDTO.setId(brokerObj.getId());
						brokerSiteLocationDTO.setSiteId(Long.parseLong(selectedAgencySiteId));
						String response = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/broker/location",brokerSiteLocationDTO,String.class);
						
						ObjectReader objRd=JacksonUtils.getJacksonObjectReaderForJavaType(BrokerResponse.class);
						BrokerResponse agencyInfo;
						try {
							agencyInfo = objRd.readValue(response);
						} catch (Exception e) {
							LOGGER.error("Exception occurred while converting String to JSOn Object", e);
							throw new GIRuntimeException(e , ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
						}  
						
						if(agencyInfo.getResponseCode()==200 ){
							Location loc=locationService.findById(agencyInfo.getLocationId());
							brokerObj.setLocation(loc);
						}
						
				}
			}else if (broker.getLocation() != null) {

				if((broker.getLocation().getLat()==0.0)&&(broker.getLocation().getLon()==0.0)){ 
					ZipCode zipCode=zipCodeService.findByZipCode(broker.getLocation().getZip().toString()); 
					if(zipCode!=null){ 
						broker.getLocation().setLat(zipCode.getLat());   
						broker.getLocation().setLon(zipCode.getLon());   
					} 
				} 
				brokerObj.setLocation(broker.getLocation());
			}
			
			
			
			if (broker.getMailingLocation() != null) {

				if((broker.getMailingLocation().getLat()==0.0)&&(broker.getMailingLocation().getLon()==0.0)){ 
					ZipCode zipCode=zipCodeService.findByZipCode(broker.getMailingLocation().getZip().toString()); 
					if(zipCode!=null){ 
						broker.getMailingLocation().setLat(zipCode.getLat());    
						broker.getMailingLocation().setLon(zipCode.getLon());            
					} 
				}
				if( null == broker.getMailingLocation().getState() && null != request.getParameter("state_mailing_hidden")){
					broker.getMailingLocation().setState(request.getParameter("state_mailing_hidden"));
				}
				
				if(broker.getMailingLocation().getAddress1()!= null) {
					broker.getMailingLocation().setAddress1(URLDecodeString(broker.getMailingLocation().getAddress1()));
				}
				if(broker.getMailingLocation().getAddress2()!= null) {
					broker.getMailingLocation().setAddress2(URLDecodeString(broker.getMailingLocation().getAddress2()));
				}
				if(broker.getMailingLocation().getCity()!= null) {
					broker.getMailingLocation().setCity(URLDecodeString(broker.getMailingLocation().getCity()));
				}
				
				
				brokerObj.setMailingLocation(broker.getMailingLocation());
			}
			brokerObj.setCompanyName(broker.getCompanyName());
			brokerObj.setLicenseNumber(broker.getLicenseNumber());

			if(!broker.getFederalEIN().contains("*")){
				brokerObj.setFederalEIN(broker.getFederalEIN());
			}			
		}
	}
	private String   URLDecodeString(String urlEncryptedString)   {
		String result = urlEncryptedString;
		result = StringEscapeUtils.unescapeHtml4(urlEncryptedString);
		return result;
	}
	private void setModelAttributes(Model model, Broker brokerObj) {
		if (brokerObj != null) {
			if(!BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
				brokerObj = formatFederalEIN(brokerObj);
			}
			model.addAttribute(BROKER, brokerObj);
			model.addAttribute(LOCATION_OBJ, brokerObj.getLocation());
			model.addAttribute(MAILING_LOCATION_OBJ, brokerObj.getMailingLocation());
			getContactNumber(model, brokerObj);
			if (brokerObj.getLocation() != null && brokerObj.getMailingLocation() != null) {
				int comparisonResult = brokerObj.getLocation().compareTo(brokerObj.getMailingLocation());
				model.addAttribute("locationMatching", comparisonResult == 0 ? "Y" : "N");
			}
		}
	}

	private void getContactNumber(Model model, Broker brokerObj) {
		if (brokerObj.getContactNumber() != null && brokerObj.getContactNumber().length()==12) {
			model.addAttribute("phone1", brokerObj.getContactNumber().substring(0, 3));
			model.addAttribute("phone2", brokerObj.getContactNumber().substring(4, 7));
			model.addAttribute("phone3", brokerObj.getContactNumber().substring(8, 12));
		}else{
			logger.info("SearchController -getContactNumber(): Unsupported format for the Contact Number: "+((brokerObj.getContactNumber()!=null)?brokerObj.getContactNumber():"NULL Value"));
		}
		if (brokerObj.getAlternatePhoneNumber() != null && brokerObj.getAlternatePhoneNumber().length()==12) {
			model.addAttribute("alternatePhone1", brokerObj.getAlternatePhoneNumber().substring(0, 3));
			model.addAttribute("alternatePhone2", brokerObj.getAlternatePhoneNumber().substring(4, 7));
			model.addAttribute("alternatePhone3", brokerObj.getAlternatePhoneNumber().substring(8, 12));
		}else{
			logger.info("SearchController - getContactNumber(): Unsupported format for the Alternate Phone Number: "+((brokerObj.getAlternatePhoneNumber()!=null)?brokerObj.getAlternatePhoneNumber():"NULL Value"));
		}
		if (brokerObj.getBusinessContactPhoneNumber() != null && brokerObj.getBusinessContactPhoneNumber().length()==12) {
			model.addAttribute("businessContactPhone1", brokerObj.getBusinessContactPhoneNumber().substring(0, 3));
			model.addAttribute("businessContactPhone2", brokerObj.getBusinessContactPhoneNumber().substring(4, 7));
			model.addAttribute("businessContactPhone3", brokerObj.getBusinessContactPhoneNumber().substring(8, 12));
		}else{
			logger.info("SearchController - getContactNumber(): Unsupported format for the Business Contact Phone Number: "+((brokerObj.getBusinessContactPhoneNumber()!=null)?brokerObj.getBusinessContactPhoneNumber():"NULL Value"));
		}
		if (brokerObj.getFaxNumber() != null && brokerObj.getFaxNumber().length()==12) {
			model.addAttribute("faxNumber1", brokerObj.getFaxNumber().substring(0, 3));
			model.addAttribute("faxNumber2", brokerObj.getFaxNumber().substring(4, 7));
			model.addAttribute("faxNumber3", brokerObj.getFaxNumber().substring(8, 12));
		}else{
			logger.info("SearchController - getContactNumber(): Unsupported format for the Fax Number: "+((brokerObj.getFaxNumber()!=null)?brokerObj.getFaxNumber():"NULL Value"));
		}
	}

	/**
	 * Handles the POST request for Edit Broker information
	 *
	 * @param request
	 * @param brokerId
	 * @param model
	 * @return URL for navigation to edit certification status page.
	 * @throws ContentManagementServiceException
	 * @throws IOException
	 */
	@RequestMapping(value = "/admin/broker/viewAttachment", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	@ResponseBody
	public void viewAttachment(@RequestParam String encDocumentId, Model model, HttpServletResponse response)
			throws GIRuntimeException {
		LOGGER.info("viewAttachment: START ");
		BrokerDocuments brokerDocuments = null;
		String decryptesDocId = null;
		Integer documentId;
		try {
			decryptesDocId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encDocumentId);
			documentId = Integer.parseInt(decryptesDocId);
			if (documentId != null) {
				brokerDocuments = brokerService.getListofDocuments(documentId);
			}
			byte[] attachment = null;

			if (brokerDocuments != null) {
				attachment = ecmService.getContentDataById(brokerDocuments.getDocumentName());
			} else {
				attachment = "No Document is attached".getBytes();
			}

			if (brokerDocuments != null) {
				response.setContentType(brokerDocuments.getMimeType());

				response.addHeader("Content-Disposition",
						"attachment; filename=" + brokerDocuments.getOrgDocumentName());

			} else {
				response.setContentType("text/plain");

				response.setContentLength(attachment.length);
			}
			FileCopyUtils.copy(attachment, response.getOutputStream());

		} catch(GIRuntimeException giexception) {			
			LOGGER.error("Error while view Attachment ");
			String errMsg = "Failed to encrypyt document Id. Decrypted document Id value is: "+decryptesDocId+", Exception is: "+giexception;
			LOGGER.error(errMsg);
			LOGGER.info("viewAttachment: END ");	    	
			throw giexception;
		}	
		catch (ContentManagementServiceException e) {
			logger.error("Unable to show attachment", e);
			try {
				FileCopyUtils.copy("UNABLE TO SHOW ATTACHMENT.PLEASE CONTACT ADMIN".getBytes(), response.getOutputStream());
			} catch (IOException ioe) {
				LOGGER.info("viewcertificationinformation: END ");
				throw new GIRuntimeException(ioe, ExceptionUtils.getFullStackTrace(ioe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}	
			LOGGER.info("viewcertificationinformation: END ");
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);

		}
		catch (IOException ioException) {
			logger.error("Unable to show attachment", ioException);
			LOGGER.info("viewcertificationinformation: END ");
			throw new GIRuntimeException(ioException, ExceptionUtils.getFullStackTrace(ioException), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}	
		catch(NumberFormatException nfe) {
			String errMsg = "Failed to decrypt BrokerId. Decrypted BrokerId's value is: "+decryptesDocId+", Exception is: "+nfe;
			LOGGER.error(errMsg);
			LOGGER.info("viewcertificationinformation: END ");
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		catch(Exception exception) {
			String errMsg = "Decrypted BrokerId's value is: "+decryptesDocId+".Failed to view certification information.Exception is  "+exception;
			LOGGER.error(errMsg);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("viewAttachment: END ");
	}

	// uploading the files into their specific folders
	private int uploadFile(String fileToUpload, String folderName, MultipartFile file, int brokerId,
			String documenttype, HttpServletRequest request) throws GIRuntimeException {

		Broker brokerObj = null;
		if (brokerId != 0) {
			brokerObj = brokerService.findBrokerByUserId(brokerId);
		}
		int documentId1 = 0;
		String documentId = null;
		try {

			// creating a new file name with the time stamp
			String modifiedFileName = FilenameUtils.getBaseName(file.getOriginalFilename()) + GhixConstants.UNDERSCORE
					+ TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT
					+ FilenameUtils.getExtension(file.getOriginalFilename());
			// upload file to DMS
			documentId = ecmService.createContent(GhixConstants.BROKER_DOC_PATH + "/" + brokerId + "/" + folderName,
					modifiedFileName, file.getBytes()
					, ECMConstants.Broker.DOC_CATEGORY, ECMConstants.Broker.BROKER_DOCUMENTS, null);
			String createdBy = userService.getLoggedInUser().getUserName();

			MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
			String mimeType = mimeTypesMap.getContentType(file.getOriginalFilename());
			documentId1 = brokerService.saveDocument(createdBy, documentId, documenttype, mimeType,
					file.getOriginalFilename());

		}catch (InvalidUserException ex) {
			LOGGER.error("Invalid User Exception occurred", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} 
		catch(GIRuntimeException giexception) {
			LOGGER.error("Error while uploading the file "+giexception);			
			LOGGER.info("uploadFile: END ");	    	
			throw giexception;
		} catch(Exception e) {
			LOGGER.error("Error while uploading the file ", e);
			LOGGER.info("uploadFile: END ");
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		return documentId1;
	}

	/**
	 * Method 'clearUploadComponents' to clear intermediate upload data.
	 *
	 * @author chalse_v
	 * @param request
	 *            The HttpServletRequest instance.
	 * @return String The status of operation.
	 */
	@RequestMapping(value = "/admin/broker/resetuploads", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'BROKER_DELETE_BROKER')")
	@ResponseBody
	private void clearUploadComponents(HttpServletRequest request,HttpServletResponse response) throws GIRuntimeException {
		try {
			this.clearSessionAttributes(request);
			response.setContentType("text/html");
			response.getWriter().write(STATUS_SUCCESS);
			response.flushBuffer();	
		} catch (Exception e) {
			logger.error("Exception occurred while clearing upload components. Exception:" , e);			
			LOGGER.info("uploadFile: END ");
			try {
				response.setContentType("text/html");
				response.getWriter().write(STATUS_FAILURE);
				response.flushBuffer();	
			} catch (IOException ioException) {
				logger.error("Exception occurred while clearing upload components. Exception:" ,ioException);					
				throw new GIRuntimeException(ioException, ExceptionUtils.getFullStackTrace(ioException), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} 
	}

	// uploading the issuer application documents
	@RequestMapping(value = "/admin/broker/upload/{encBrokerId}", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'BROKER_ADD_BROKER')")
	@ResponseBody
	public void uploadBrokerApplicationDocuments(Model model, HttpServletRequest request,
			@RequestParam(value = "fileToUpload", required = false) String fileToUpload,
			@RequestParam(value = "fileInputEODeclPage", required = false) MultipartFile fileInputEODeclPage,
			@RequestParam(value = "fileInputContract", required = false) MultipartFile fileInputContract,
			@RequestParam(value = "fileInput", required = false) MultipartFile fileInput,
			@PathVariable("encBrokerId") String encBrokerId, HttpServletResponse response) throws GIRuntimeException {

		String returnString = "";
		String decryptedBrokerId = null;
		int brokerId;
		try{
			decryptedBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			brokerId = Integer.parseInt(decryptedBrokerId);

			if (fileToUpload != null) {
				String mimeType = null;
				if (fileToUpload.equalsIgnoreCase("fileInputEODeclPage")) {
					if (UPLOAD_LIMIT > fileInputEODeclPage.getSize()) {
						Integer docId = uploadFile(fileToUpload, "ADD_SUPPORT_FOLDER", fileInputEODeclPage, brokerId, null,
								request);
						returnString = fileToUpload + "|" + fileInputEODeclPage.getOriginalFilename() + "|" + docId;
					} else {
						returnString = fileToUpload + "|" + fileInputEODeclPage.getOriginalFilename() + "|" + -1;
					}
					Map<String, MultipartFile> documents = new HashMap<String, MultipartFile>();
					documents.put("fileInputEODeclPage", fileInputEODeclPage);
					request.getSession().setAttribute("authorityDocumentInfoMap", documents);
				} else if (fileToUpload.equalsIgnoreCase("fileInputContract")) {
					if (UPLOAD_LIMIT > fileInputContract.getSize()) {
						Integer docId = uploadFile(fileToUpload, "ADD_SUPPORT_FOLDER", fileInputContract, brokerId, null,
								request);
						returnString = fileToUpload + "|" + fileInputContract.getOriginalFilename() + "|" + docId;
					} else {
						returnString = fileToUpload + "|" + fileInputContract.getOriginalFilename() + "|" + -1;
					}
					Map<String, MultipartFile> documents = new HashMap<String, MultipartFile>();
					documents.put("fileInputContract", fileInputContract);
					request.getSession().setAttribute("fileInputContract", documents);
				} else if (fileToUpload.equalsIgnoreCase("fileInput")) {
					if (UPLOAD_LIMIT > fileInput.getSize()) {
						Integer docId = uploadFile(fileToUpload, "ADD_SUPPORT_FOLDER", fileInput, brokerId, null, request);
						returnString = fileToUpload + "|" + fileInput.getOriginalFilename() + "|" + docId;
					} else {
						returnString = fileToUpload + "|" + fileInput.getOriginalFilename() + "|" + -1;
					}
					Map<String, MultipartFile> documents = new HashMap<String, MultipartFile>();
					documents.put("fileInput", fileInput);
					request.getSession().setAttribute("fileInput", documents);
				}

			}
			response.setContentType("text/html");
			response.getWriter().write(returnString);
			response.flushBuffer();

		}catch(GIRuntimeException giexception) {
			LOGGER.error("Error while uploading the Broker's ApplicationDocuments "+giexception);			
			LOGGER.info("uploadBrokerApplicationDocuments: END ");
			response.setContentType("text/html");
			try {
				returnString = "FAILURE";
				response.getWriter().write(returnString);
				response.flushBuffer();
			} catch (IOException e) {

				throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}

			return;
		} 
		catch(NumberFormatException nfe){
			String errMsg = "Failed to decrypt BrokerId. Decrypted BrokerId's value is: "+decryptedBrokerId+", Exception is: "+nfe;
			LOGGER.error(errMsg);
			LOGGER.info("uploadBrokerApplicationDocuments: END ");	    
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		catch(Exception e) {
			LOGGER.error("Error while uploading the Broker's ApplicationDocuments ", e);
			LOGGER.info("uploadBrokerApplicationDocuments: END ");
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}	
		return;
	}

	/**
	 * Method 'clearSessionAttributes' to clear session attributes .
	 *
	 * @param request
	 *            The HttpServletRequest instance.
	 * @return request The HttpServletRequest instance.
	 */
	private void clearSessionAttributes(HttpServletRequest request) {
		if (null != request.getSession()) {
			request.getSession().removeAttribute(FILE_NAME_E_O_DECL_PAGE);
			request.getSession().removeAttribute(FILE_NAME_CONTRACT);
			request.getSession().removeAttribute(FILE_NAME);

			request.getSession().removeAttribute(CURRENT_UPLOAD);

			request.getSession().removeAttribute(UPLOADED_DOCUMENT_E_O_DECL_PAGE);
			request.getSession().removeAttribute(UPLOADED_DOCUMENT_CONTRACT);
			request.getSession().removeAttribute(UPLOADED_DOCUMENT_SUPPORTING);
		}
	}

	/**
	 * Method 'switchToBrokerView' to switch role from admin(csr) to broker
	 *
	 * @param model The current Model instance.
	 * @param request The current HttpServletRequest instance.
	 * @return String The view name.
	 */
	@RequestMapping(value = "/admin/broker/dashboard", method ={RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ACCOUNT_SWITCH_BROKER')")
	public String switchToBrokerView(Model model, HttpServletRequest request) {
		LOGGER.info("switchToBrokerView: START ");

		if(request.getSession().getAttribute("checkDilog")==null){
			String checkDilog= request.getParameter("checkAgentView");
			request.getSession().setAttribute("checkDilog", checkDilog);
		}
		
		if ( null != request.getParameter("showPopupInFuture")) {
			request.getSession().setAttribute("showPopupInFutureForAgency", request.getParameter("showPopupInFuture"));
		}
		
		String switchToModuleName = request.getParameter("switchToModuleName");
		String switchToModuleId = request.getParameter("switchToModuleId");
		String switchToResourceName = request.getParameter("switchToResourceName");
		
		HashMap<String,String> paramMap= new HashMap<String,String>(3);
		paramMap.put("switchToModuleName", switchToModuleName);
		paramMap.put("switchToModuleId", switchToModuleId);
		paramMap.put("switchToResourceName", switchToResourceName);
		
		LOGGER.info("switchToBrokerView: END ");
		return "forward:/account/user/switchUserRole?ref="+GhixAESCipherPool.encryptParameterMap(paramMap);
	}

	/**
	 * Method to populate the to switch resource's full name.
	 *
	 * @param model The current Model instance.
	 * @param brokerObj The current Broker instance.
	 * @return model The current updated or non-updated Model instance.
	 */
	private Model populateSwitchResourceName(Model model, Broker brokerObj) throws GIRuntimeException {
		LOGGER.info("populateSwitchResourceName: START ");
		AccountUser user = null;

		if(null != model && (null != brokerObj && null != brokerObj.getUser())) {

			user = brokerObj.getUser();

			if(null != user.getFullName() && !user.getFullName().trim().isEmpty()) {
				try {
					model.addAttribute(BrokerConstants.SWITCH_TO_RESOURCE_FULL_NAME_ENCODED, URLEncoder.encode(user.getFullName().trim(), BrokerConstants.ENCODING_UTF_8));
					model.addAttribute(BrokerConstants.SWITCH_TO_RESOURCE_FULL_NAME_UNENCODED, user.getFullName().trim());
				}
				catch (UnsupportedEncodingException e) {
					LOGGER.error("Exception occurred while encoding broker fullname.", e);
					throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
				}
			}
			else {
				try {
					model.addAttribute(BrokerConstants.SWITCH_TO_RESOURCE_FULL_NAME_ENCODED, URLEncoder.encode(brokerObj.getUser().getFirstName().trim(), BrokerConstants.ENCODING_UTF_8));
					model.addAttribute(BrokerConstants.SWITCH_TO_RESOURCE_FULL_NAME_UNENCODED, brokerObj.getUser().getFirstName().trim());
				}
				catch (UnsupportedEncodingException e) {
					LOGGER.error("Exception occurred while encoding broker name.", e);
					throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
				}
			}			
		}else if(null!=brokerObj && null!=brokerObj.getFirstName()) {
			
			StringBuffer fullName = new StringBuffer();
			if (brokerObj.getFirstName() != null
					&& brokerObj.getFirstName().trim().length() > 0) {
				fullName.append(brokerObj.getFirstName().trim());
			}

			if (brokerObj.getLastName() != null
					&& brokerObj.getLastName().trim().length() > 0) {
				fullName.append(" ").append(brokerObj.getLastName().trim());
			}
			
			try {
				model.addAttribute(BrokerConstants.SWITCH_TO_RESOURCE_FULL_NAME_ENCODED, URLEncoder.encode(fullName.toString(), BrokerConstants.ENCODING_UTF_8));
				model.addAttribute(BrokerConstants.SWITCH_TO_RESOURCE_FULL_NAME_UNENCODED,fullName.toString());
			}
			catch (UnsupportedEncodingException e) {
				LOGGER.error("Exception occurred while encoding broker fullname.", e);
				throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}
			
		}
		LOGGER.info("populateSwitchResourceName: END ");
		return model;
	}

	private String getTemplateContentWithTokensReplaced(Map<String, Object> tokens, String location ) throws GIRuntimeException {

		InputStream inputStreamUrl = null;
		String templateContent = "";
		try {
			Resource resource = appContext.getResource("classpath:"+location);
			inputStreamUrl = resource.getInputStream();
			templateContent = IOUtils.toString(inputStreamUrl, "UTF-8");
		} catch (Exception e) {
			LOGGER.error("Error reading content from  - " + location , e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally{
			IOUtils.closeQuietly(inputStreamUrl);
		}
		
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();	
		Template tmpl = null;
		String responseData = null;
		
		try {
			stringLoader.putTemplate("welcomeEmail", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			tmpl = templateConfig.getTemplate("welcomeEmail");
			tmpl.process(tokens, sw);
			responseData = sw.toString();
		} catch (Exception e) {
			LOGGER.error("Exception ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			IOUtils.closeQuietly(sw);
		}
		return responseData;

	}

	private Map<String, String> getTemplateDataMap(Map<String, Object> templateData) {
		Map<String, String> templateDataModified = new HashMap<String, String>();
		Iterator<Entry<String, Object>> it = templateData.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			if(pairs.getValue() != null) {
				templateDataModified.put(pairs.getKey(), pairs.getValue().toString());
			}
		}
		return templateDataModified;
	}
	
	private String loadLanguagesData(){
        LOGGER.info("getLanguageList : START");
        String jsonData = null;

	    List<String> languageSpokenList = null;

	    languageSpokenList = lookupService.populateLanguageNames("");
        LOGGER.info("getLanguageList : END");
        jsonData = platformGson.toJson(languageSpokenList);

        return jsonData;
	}
	
	/**
	 * Validate spoken languages
	 *
	 * @param brkObj
	 *            {@link Broker}
	 */
	private void validateLanguagesSpoken(Broker brkObj) {

		LOGGER.info("validateLanguagesSpoken : START");

		if (brkObj.getLanguagesSpoken() != null && !brkObj.getLanguagesSpoken().isEmpty()) {
			String ls = brkObj.getLanguagesSpoken();
			ls = ls.trim();
			if (ls.endsWith(",")) {
				ls = ls.substring(0, ls.length() - 1);
				brkObj.setLanguagesSpoken(ls);
			}

			List<String> items = null;
			items = Arrays.asList(brkObj.getLanguagesSpoken().split(","));
			items.removeAll(Collections.singletonList(null));

			List<String> newList = new ArrayList<String>();
			for (Iterator<String> iter = items.iterator(); iter.hasNext();) {
				String element = iter.next();
				if (!newList.contains(element.trim())) {
					newList.add(element.trim());
				}
			}

			String newString = "";
			for (Iterator<String> it = newList.iterator(); it.hasNext();) {
				newString += it.next();
				if (it.hasNext()) {
					newString += ", ";
				}
			}
			brkObj.setLanguagesSpoken(newString);
		}

		LOGGER.info("validateLanguagesSpoken : END");
	}
	
	@RequestMapping(value = "/admin/broker/manage/exportAgentList", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'MANAGE_BROKER')")
	@ResponseBody
	public void exportAgentListAsExcel(HttpServletRequest request, HttpServletResponse response, Model model) {
		LOGGER.info("exportAgentListAsExcel: START ");
		final char CHAR_$ = '$', CHAR_NEW_LINE = '\n', CHAR_COMMA = ',', CHAR_HYPHEN = '-';
		Broker broker;
		StringBuffer strWriter;
		BrokerExportResponseDTO brokerExportResponseDTO;
		try{
			brokerExportResponseDTO = brokerService.getExportAgentList();
			
			if (null == brokerExportResponseDTO) {
				strWriter = new StringBuffer("No Agents found in Export Agent List");
				strWriter.trimToSize();
				response.setContentType("text/plain");

				response.setContentLength(strWriter.toString().length());
			}else{
				StringBuffer address = null;
				strWriter = new StringBuffer();
				strWriter.append("Agent First Name,Agent Last Name,Agent Business Name,Agent FEIN,Agent License Number,License Renewal date,Certification Status,Business Address,Primary Phone number,Agent Email Address,Preferred Method of Communication,Number of Current Active Delegations,Number of Current Pending Delegations,Number of Current Inactive Delegations");
				strWriter.append(CHAR_NEW_LINE);

				List<BrokerExportDTO> brokerExportList = brokerExportResponseDTO.getBrokerExportDTO();

				Validate.notNull(brokerExportList);
				Validate.notEmpty(brokerExportList);
				
				for(BrokerExportDTO brokerExportDTO: brokerExportList) {
					if(null != brokerExportDTO) {
						strWriter.append(StringEscapeUtils.escapeCsv(brokerExportDTO.getFirstName())).append(CHAR_COMMA);
						strWriter.append(StringEscapeUtils.escapeCsv(brokerExportDTO.getLastName())).append(CHAR_COMMA);
						strWriter.append(StringEscapeUtils.escapeCsv(brokerExportDTO.getCompanyName())).append(CHAR_COMMA);
						strWriter.append(StringEscapeUtils.escapeCsv(brokerExportDTO.getFederalEIN())).append(CHAR_COMMA);
						strWriter.append(StringEscapeUtils.escapeCsv(brokerExportDTO.getLicenseNumber())).append(CHAR_COMMA);
						
						if(null != brokerExportDTO.getLicenseRenewalDate()) {
						       strWriter.append(StringEscapeUtils.escapeCsv(brokerExportDTO.getLicenseRenewalDate()+""));
					    }else{
					    	strWriter.append(NA);
					    }
						strWriter.append(CHAR_COMMA);
						
						strWriter.append(StringEscapeUtils.escapeCsv(brokerExportDTO.getCertificationStatus())).append(CHAR_COMMA);
						
						address = new StringBuffer();

						if(StringUtils.isNotBlank(brokerExportDTO.getAddress1())) {
							address.append(brokerExportDTO.getAddress1());
						}

						if(StringUtils.isNotBlank(brokerExportDTO.getAddress2())) {
							address.append((", ")).append(brokerExportDTO.getAddress2());
						}

						final String STR_SPACE = " ";
						if(StringUtils.isNotBlank(brokerExportDTO.getCity())) {
							address.append(STR_SPACE).append(brokerExportDTO.getCity());
						}

						if(StringUtils.isNotBlank(brokerExportDTO.getState())) {
							address.append(STR_SPACE).append(brokerExportDTO.getState());
						}

						if(StringUtils.isNotBlank(brokerExportDTO.getZip())) {
							address.append(STR_SPACE).append(brokerExportDTO.getZip());
						}

						strWriter.append(StringEscapeUtils.escapeCsv(address.toString()));
						strWriter.append(CHAR_COMMA);

						if(StringUtils.isNotBlank(brokerExportDTO.getContactNumber())) {
							if(brokerExportDTO.getContactNumber().contains("-")){
								strWriter.append(brokerExportDTO.getContactNumber());
							} else {
								strWriter.append(brokerExportDTO.getContactNumber().substring(0, 3)).append(CHAR_HYPHEN);
								strWriter.append(brokerExportDTO.getContactNumber().substring(3, 6)).append(CHAR_HYPHEN);
								strWriter.append(brokerExportDTO.getContactNumber().substring(6, 10));
							}
						}
						strWriter.append(CHAR_COMMA);

						if(StringUtils.isNotBlank(brokerExportDTO.getEmail())) {
							strWriter.append(brokerExportDTO.getEmail());
						}
						strWriter.append(CHAR_COMMA);

						strWriter.append(StringEscapeUtils.escapeCsv(brokerExportDTO.getPrefMethodOfCommunication())).append(CHAR_COMMA);
						strWriter.append(brokerExportDTO.getActiveRequestsCount()).append(CHAR_COMMA);
						strWriter.append(brokerExportDTO.getPendingRequestsCount()).append(CHAR_COMMA);
						strWriter.append(brokerExportDTO.getInactiveRequestsCount());
												
						strWriter.append(CHAR_NEW_LINE);
					}
				}
			}
			strWriter.trimToSize();
			response.setContentType("text/csv");
			response.addHeader("Content-Disposition", "attachment; filename=Download_Agent_List.csv");

			FileCopyUtils.copy(strWriter.toString().getBytes(), response.getOutputStream());
		}
		catch(GIRuntimeException runtimeException) {
			LOGGER.error("Runtime exception occurred while exporting Agent list as Excel", runtimeException);
			throw runtimeException;
		}
		catch(Exception exception) {
			LOGGER.error("Runtime exception occurred while exporting Agent list as Excel", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			LOGGER.info("exportAgentListAsExcel: END ");
		}
	}
	
	/**
	 * Handles the Broker's list page on Manage Admin tab
	 *
	 * @param broker
	 * @param result
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate Broker list page
	 * @throws GIRuntimeException 
	 */
	@RequestMapping(value = "admin/agency/manage", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'MANAGE_AGENCY')")
	public String manageAgency(Model model, HttpServletRequest request) throws GIRuntimeException {
		LOGGER.info("manageAgency : START ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Manage Agency");
		try {	
			AccountUser user = userService.getLoggedInUser();
			request.getSession().setAttribute("isAdminRole",userService.hasUserRole(user, RoleService.BROKER_ADMIN_ROLE));
			LOGGER.info("brmanageAgencyokerList : END ");
		}
		catch(GIRuntimeException giexception){
			LOGGER.error("Exception occured while fetching Agents : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while fetching Agents : ", exception	);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		return "admin/agency/manageagency";
	}
	
	/**
	 * Display Manage Agencies
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return response
	 */
	@ResponseBody
	@RequestMapping(value = {"admin/agency/managelist"}, method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'MANAGE_AGENCY')")
	public AgencyDetailsListDTO manageAgenciesList(@RequestBody SearchAgencyDto searchAgencyDto, HttpServletRequest request){
		AgencyDetailsListDTO agencyDetailsListDTO = null;
		try {
			agencyDetailsListDTO = brokerService.findAgency(searchAgencyDto);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching agencies list - ",ex);
		}
		return agencyDetailsListDTO;
	}
	
	@ResponseBody
	@RequestMapping(value = {"admin/broker/role/{encUserId}"}, method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'VIEW_AGENT_ROLE')")
	public String findUserRole(@PathVariable("encUserId") String encUserId, HttpServletRequest request){
		String name = "";
		String userId = null;
		
		try {
			if(encUserId!=null && !encUserId.isEmpty()){
				userId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encUserId);
				if(userId !=null && !userId.isEmpty() && StringUtils.isNumeric(userId)){
					Role role = roleService.findRoleByUserId(Integer.valueOf(userId));
					if(role!=null){
						name = role.getName();
					}
				}
			}
		} catch(NumberFormatException nfe) {
			String errMsg = "Failed to decrypt UserId. Decrypted UserId's value is: "+encUserId+", Exception is: "+nfe;
			LOGGER.error(errMsg);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching agent role - ",ex);
		}
		return name;
	}
	
	private void decryptRequestParam(HttpServletRequest request) {
		if(null !=request.getParameter("ref")){
			String reference = request.getParameter("ref");
			Map<String, String> encParams = GhixAESCipherPool.decryptParameterMap(reference);
		    for(Entry<String, String> entrySet:encParams.entrySet()){
			  request.setAttribute( entrySet.getKey(), entrySet.getValue()); 
		    }
		}
	}
	 
	@RequestMapping(value = {"/admin/broker/activitystatus","/admin/broker/activitystatus/{encBrokerId}"}, method = RequestMethod.GET)
	public String findBrokerActivityStatus(@PathVariable("encBrokerId") Optional<String> encBrokerId, HttpServletRequest request, Model model){
		String brokerId = null;
		Broker broker = null;
		
		try {
			AccountUser user = userService.getLoggedInUser();
			
			if(encBrokerId.isPresent()){
				brokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId.get());
			} else if(null !=request.getParameter("ref")){
				decryptRequestParam(request);
				brokerId = (String) request.getAttribute("brokerId");
			} else if(user.getActiveModuleId()!=0){
				brokerId = String.valueOf(user.getActiveModuleId());
			} 
			
			broker = brokerService.getBrokerDetail(Integer.parseInt(brokerId));
			
			List<Map<String, Object>> brokerActivityStatusHistory = brokerService.loadBrokerActivityStatusHistory(Integer.parseInt(brokerId));
			
			model.addAttribute("broker", broker);
			model.addAttribute("brokerActivityStatusHistory", brokerActivityStatusHistory);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			request.getSession().setAttribute(BrokerConstants.BROKER_OBJECT_FOR_LEFT_NAV, broker);
			
			if (userService.hasUserRole(user, RoleService.AGENCY_MANAGER)|| RoleService.AGENCY_MANAGER.equalsIgnoreCase(user.getActiveModuleName())){
				model.addAttribute("isAgencyManager", true);
				model.addAttribute("isAdminStaffL2", false);
				if(broker.getUser() == null || broker.getUser().getId() != user.getId()) {
					model.addAttribute("viewOtherBrokerInfo", true);
				}else {
					model.addAttribute("viewSelfBrokerInfo", true);
				}
			}else if (userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2)|| RoleService.APPROVED_ADMIN_STAFF_L2.equalsIgnoreCase(user.getActiveModuleName())){
				model.addAttribute("isAgencyManager", false);
				model.addAttribute("isAdminStaffL2", true);
				if(broker.getUser() == null || broker.getUser().getId() != user.getId()) {
					model.addAttribute("viewOtherBrokerInfo", true);
				}else {
					model.addAttribute("viewSelfBrokerInfo", true);
				}
			}else {
				model.addAttribute("isAgencyManager", false);
				model.addAttribute("isAdminStaffL2", true);
			}
			
			if(userService.hasUserRole(user, RoleService.ADMIN_ROLE) || userService.hasUserRole(user, RoleService.BROKER_ADMIN_ROLE) || userService.hasUserRole(user, RoleService.OPERATIONS_ROLE)){
                    model.addAttribute("isAdminUser", true);
            }else{
                    model.addAttribute("isAdminUser", false);
            }
			
		} catch (Exception exception) {
			LOGGER.error("Exception occured while loading broker activity status history - ",exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return "broker/viewstatusinformation" ;
	}
	
	@RequestMapping(value = {"/admin/broker/activitystatus"}, method = RequestMethod.POST)
	public String updateBrokerActivityStatus(
			@RequestParam(value = "encBrokerId", required = true) String encBrokerId,
			@RequestParam(value = "activityStatus", required = true) String status,
			@RequestParam(value = "comment", required = false) String comment,
			HttpServletRequest request, Model model){
		String brokerId;
		Broker broker = null;
		
		try {
			AccountUser user = userService.getLoggedInUser();
			AgencyInformationDto agencyInformationDto = null;
			
			if(request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW) != null
					&& request.getSession().getAttribute(SWITCH_TO_MODULE_ID) != null
					&& ((String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)).equalsIgnoreCase(Y)) {
				
				agencyInformationDto =   ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+request.getSession().getAttribute(SWITCH_TO_MODULE_ID), AgencyInformationDto.class);
				
			}else {
				agencyInformationDto = brokerService.findAgencyByLoggedInUser(user);	
			}
			
			if(agencyInformationDto!=null && agencyInformationDto.getCertificationStatus()!=null && !"CERTIFIED".equalsIgnoreCase(agencyInformationDto.getCertificationStatus().toString())){
				throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
			}
			
			brokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			broker = brokerService.getBrokerDetail(Integer.parseInt(brokerId));
			
			if(StringUtils.isNotEmpty(comment)){
				Comment comments = brokerService.saveComments(broker.getId(), comment);
				if(comments!=null){
					broker.setCommentsId(Long.valueOf(comments.getId()));
				}
			} else {
				broker.setCommentsId(null);
			}
			broker.setStatus(status);
			broker.setLastUpdatedBy(user.getId());
			broker = brokerService.saveBrokerWithLocation(broker);
		} catch (Exception exception) {
			LOGGER.error("Exception occured while saving broker activity status - ",exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return "redirect:/admin/broker/activitystatus/" + encBrokerId;
	}
	
	@RequestMapping(value = "/admin/agency/dashboard", method ={RequestMethod.GET, RequestMethod.POST })
	//@PreAuthorize("hasPermission(#model, 'ACCOUNT_SWITCH_AGENCY')")
	public String switchToAgencyView(Model model, HttpServletRequest request) {
		LOGGER.info("switchToAgencyView: START ");

		if(request.getSession().getAttribute("checkDilog")==null){
			String checkDilog= request.getParameter("checkAgencyView");
			request.getSession().setAttribute("checkDilog", checkDilog);
		}
		String switchToModuleName = request.getParameter("switchToModuleName");
		String switchToModuleId = request.getParameter("switchToModuleId");
		String switchToResourceName = request.getParameter("switchToResourceName");
		
		HashMap<String,String> paramMap= new HashMap<String,String>(3);
		paramMap.put("switchToModuleName", switchToModuleName);
		paramMap.put("switchToModuleId", switchToModuleId);
		paramMap.put("switchToResourceName", switchToResourceName);
		
		LOGGER.info("switchToAgencyView: END ");
		return "forward:/account/user/switchUserRole?ref="+GhixAESCipherPool.encryptParameterMap(paramMap);
	}

}
