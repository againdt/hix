import React from 'react';

// Components

import UniversalInput from '../../../../lib/UniversalInput';
import ValidationErrorMessage from '../../../notifications/ValidationErrorMessage';

// Utils

import normalizeNameOfPerson from '../../../../utils/normalizeNameOfPerson';
import { getHhmByPersonId } from '../../../../utils/getHhmByPersonId';

// Constants

import INPUT_TYPES from '../../../../constants/inputTypes';
import { VALIDATION_ERROR_MESSAGES } from '../../../../constants/validationErrorMessages';




const PrimaryFilerBlock = props => {
    const {
        family,
        householdMembers,
        onFieldChange,
        primaryTaxFilerPersonId,
        isFormSubmitted,
        isFormOnTopLevelValid,
        ...restProps
    } = props;

    const getPrimaryTaxFilerMessage = () => {

        const getPrimaryTaxFilerName = () => primaryTaxFilerPersonId ? normalizeNameOfPerson(getHhmByPersonId(primaryTaxFilerPersonId, householdMembers).name) : 'no Primary Tax Filer';

        return (
            <span>
                Primary Tax Filer on the application is &nbsp;
                <strong>
                    { getPrimaryTaxFilerName() }
                </strong>
            </span>
        );
    };

    const getChosenFilersList = () => {

        return family.filers.filter(filer => filer.isFilingTaxes).map(filer => {

            if(Array.isArray(filer.filerPersonId)) {

                return filer.filerPersonId.map((...args) => {

                    const [ , idx ] = args;
                    const {
                        filerPersonId,
                        filerFullName,
                        filerIndex
                    } = filer;

                    const isPrimaryTaxFiler = primaryTaxFilerPersonId === filerPersonId[idx];

                    return (
                        <UniversalInput
                            key={filerPersonId[idx]}
                            label={{
                                text: filerFullName.split(' and ')[idx]
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.CHECKBOX,
                                name: 'isPrimaryTaxFiler',
                                classes: [''],
                                checked: isPrimaryTaxFiler,
                                value: isPrimaryTaxFiler,
                                currentData: { },
                                fields: [],
                            }}
                            inputActions={{
                                onFieldChange
                            }}
                            indexInArray={filerIndex[idx]}
                            {...restProps}
                        />
                    );
                });
            }


            const {
                filerPersonId,
                filerFullName,
                filerIndex
            } = filer;

            const isPrimaryTaxFiler = primaryTaxFilerPersonId === filerPersonId;

            return (
                <UniversalInput
                    key={filerPersonId}
                    label={{
                        text: filerFullName
                    }}
                    inputData={{
                        isInputRequired: false,
                        type: INPUT_TYPES.CHECKBOX,
                        name: 'isPrimaryTaxFiler',
                        classes: [''],
                        checked: isPrimaryTaxFiler,
                        value: isPrimaryTaxFiler,
                        currentData: { },
                        fields: [],
                    }}
                    inputActions={{
                        onFieldChange
                    }}
                    indexInArray={filerIndex}
                    {...restProps}
                />
            );
        });
    };

    return family.filersQty < 2 ?

        (primaryTaxFilerPersonId === 0 ?
            null :
            <div className="usa-alert usa-alert-info alert--info">
                { getPrimaryTaxFilerMessage() }
            </div>
        ) :

        <div className="subsection">
            <ValidationErrorMessage
                isVisible={isFormSubmitted && !isFormOnTopLevelValid && !(family.filersQty > 1 ? primaryTaxFilerPersonId !== 0 : true)}
                messageText={VALIDATION_ERROR_MESSAGES.PRIMARY_TAX_FILER}
                errorClasses={['margin-l-20-ve', 'margin-b-10']}
            />
            <p className="required-true">
                Please select which of the tax filers below should be considered the primary applicant for this application (if filing a joint return, this would be the Primary Tax Filer)
            </p>
            {
                getChosenFilersList()
            }
        </div>;
};

export default PrimaryFilerBlock;