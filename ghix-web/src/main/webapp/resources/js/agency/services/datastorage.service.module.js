(function() {
    'use strict';

    angular
        .module('dataStorageServiceModule',[])
        .factory('dataStorageService', dataStorageService);

    dataStorageService.$inject = ['$rootScope'];

    function dataStorageService($rootScope) {
        return {
            setItem: function(key, value){
                if (!amplify.store.types.sessionStorage )
                    amplify.store(key, value);
                else
                    amplify.store.sessionStorage(key, value);
            },

            getItem: function(key){
                if (!amplify.store.types.sessionStorage )
                    return amplify.store(key);
                else
                    return amplify.store.sessionStorage(key);
            }
        };
    }
})();



