package com.getinsured.hix.batch.enrollment.skip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component("enrollmentMonthlyPremiumParams")
public class EnrollmentMonthlyPremiumParams {
	List<Integer> enrollmentIdList=null;
	Map<Integer,String> skippedEnrollmentMap=null;

	public EnrollmentMonthlyPremiumParams() {
		enrollmentIdList= new ArrayList<Integer>();
		skippedEnrollmentMap = new HashMap<Integer,String>();
	}
	
	public synchronized void addAllToEnrollmentIdList(List<Integer> enrollmentIdList){
		this.enrollmentIdList.addAll(enrollmentIdList);
	}
	
	public synchronized void addToEnrollmentIdList(Integer enrollmentId){
		this.enrollmentIdList.add(enrollmentId);
	}
	
	public List<Integer> getEnrollmentIdList(){
		return enrollmentIdList;
	}
	public synchronized void clearEnrollmentIdList(){
		this.enrollmentIdList.clear();
	}
	
	public synchronized void putToSkippedEnrollmentMap(Integer key, String value){
		skippedEnrollmentMap.put(key, value);
	}
	public synchronized void putAllToSkippedEnrollmentMap(Map<Integer,String> failedEnrollmentMap){
		this.skippedEnrollmentMap.putAll(failedEnrollmentMap);
	}	
	
	public Map<Integer,String> getSkippedEnrollmentMap(){
		return skippedEnrollmentMap;
	}
	
	public synchronized void clearSkippedEnrollmentMap(){
		this.skippedEnrollmentMap.clear();
	}	
	
	public synchronized void resetFields(){
		this.enrollmentIdList.clear();
		this.skippedEnrollmentMap.clear();
	}
}
