package com.getinsured.ssap.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.dto.qep.QepEventRequest;
import com.getinsured.iex.util.LifeChangeEventConstant;

/**
 * Service wrapper for creating app events.
 * @author Yevgen Golubenko
 * @since 2019-06-04
 */
@Service("eventCreationService")
public class EventCreationServiceImpl implements EventCreationService {
  private static final Logger log = LoggerFactory.getLogger(EventCreationServiceImpl.class);

  // Application event constants.
  private static final String QUALIFYING_EVENT = "QUALIFYING_EVENT";
  private static final String APPEVENT_APPLICATION = "APPEVENT_APPLICATION";
  public static final String APPLICATION_CREATED = "APPLICATION_CREATED";
  public static final String APPLICATION_SUBMITTED = "APPLICATION_SUBMITTED";

  private final HIXHTTPClient hIXHTTPClient;
  private final UserService userService;
  private final AppEventService appEventService;
  private final LookupService lookupService;

  @Autowired
  public EventCreationServiceImpl(final HIXHTTPClient hIXHTTPClient, final UserService userService,
                                  final AppEventService appEventService, final LookupService lookupService) {
    this.hIXHTTPClient = hIXHTTPClient;
    this.userService = userService;
    this.appEventService = appEventService;
    this.lookupService = lookupService;
  }

  // TODO: Refactor this copy/paste junk
  @Override
  public void startEventCreation(final long ssapApplicationId, final String qepEvent, final String qepEventDate, final String caseNumber, final String applicationType) {
    try {
      QepEventRequest qepEventRequest = createQepEventRequest(ssapApplicationId, qepEvent, qepEventDate, caseNumber, applicationType);
      ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(QepEventRequest.class);

      String response = hIXHTTPClient.getPOSTData(GhixEndPoints.ELIGIBILITY_URL + "SsapEventsController/processEvents", writer.writeValueAsString(qepEventRequest), "application/json");
      log.debug("Got response from {} => {}", GhixEndPoints.ELIGIBILITY_URL + "SsapEventsController/processEvents", response);
    } catch (InvalidUserException e) {
      log.error("Invalid User Exception while creating QepEvents for ssap application id: " + ssapApplicationId + "; event name: " + qepEvent, e);
      throw new GIRuntimeException(e);
    } catch(Exception e) {
      log.error("Error in creating QepEvents for ssap application id: " + ssapApplicationId +"; event name: " + qepEvent, e);
      throw new GIRuntimeException("Error in creating QepEvents for ssap application id: " + ssapApplicationId, e);
    }
  }

  // TODO: Refactor this copy/paste junk
  @Override
  public void logIndividualAppEvent(String eventType, long applicationId, boolean financial) {
    final String APPLICATION_ID = "Application ID";
    final String APPLICATION_TYPE = "Application Type";
    final String NON_FINANCIAL_APPLICATION = "Non Financial Application";
    final String FINANCIAL_APPLICATION = "Financial Application";

    try {
      //event type and category
      LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(APPEVENT_APPLICATION, eventType);
      EventInfoDto eventInfoDto = new EventInfoDto();

      Map<String, String> mapEventParam = new HashMap<>();
      mapEventParam.put(APPLICATION_ID, String.valueOf(applicationId));
      mapEventParam.put(APPLICATION_TYPE, financial ? FINANCIAL_APPLICATION : NON_FINANCIAL_APPLICATION);

      final AccountUser user = getCurrentUser();
      eventInfoDto.setModuleId(user.getActiveModuleId());
      eventInfoDto.setModuleName(user.getActiveModuleName().toUpperCase());
      eventInfoDto.setEventLookupValue(lookupValue);

      appEventService.record(eventInfoDto, mapEventParam);
    }catch (InvalidUserException ex) {
      log.error("Invalid User Exception while getting currently logged in user", ex);
      throw new GIRuntimeException(ex);
    } catch (Exception e) {
      log.error("Exception occurred while log Application event" + e.getMessage(), e);
      throw new GIRuntimeException(e);
    }
  }

  // TODO: Refactor this copy/paste junk
  private QepEventRequest createQepEventRequest(final long ssapApplicationId, final String qepEvent,
                                                final String qepEventDate, final String caseNumber,
                                                final String applicationType) throws InvalidUserException {
    AccountUser accountUser = null;
    try {
      accountUser = getCurrentUser();
      
    } catch(InvalidUserException e) {
      log.error("Invalid user exception occurred: {}", e.getMessage());
    }

    QepEventRequest qepEventRequest = new QepEventRequest();
    qepEventRequest.setCaseNumber(caseNumber);
    qepEventRequest.setSsapApplicationId(ssapApplicationId);
    qepEventRequest.setQepEvent(qepEvent);
    qepEventRequest.setQepEventDate(qepEventDate);
    qepEventRequest.setApplicationType(applicationType);
    if(accountUser != null){
    	qepEventRequest.setUserId(accountUser.getId());
    }
    else if(accountUser == null){
    	 final AccountUser exAdminUser = userService.findByEmail(LifeChangeEventConstant.EXADMIN_USERNAME);
         if(exAdminUser != null) {
           log.info("Event creation has no user context, called from probably batch/back-office process, using {} user instead", LifeChangeEventConstant.EXADMIN_USERNAME);
           accountUser = exAdminUser;
           qepEventRequest.setUserId(accountUser.getId());
         }
    }

    if (SsapApplicationEventTypeEnum.QEP.name().equalsIgnoreCase(applicationType)) {
      qepEventRequest.setChangeType(QUALIFYING_EVENT);
    } else if (SsapApplicationEventTypeEnum.OE.name().equalsIgnoreCase(applicationType)) {
      qepEventRequest.setChangeType(SsapApplicationEventTypeEnum.OE.name());
    }
    return qepEventRequest;
  }

  /**
   * Retrieves currently logged in user or if that fails, retrieves exadmin user.
   * @return logged in user or exadmin user.
   * @throws InvalidUserException if could not get any of: currently logged in or exadmin user.
   */
  private AccountUser getCurrentUser() throws InvalidUserException {
    AccountUser currentUser = null;

    try {
      currentUser = userService.getLoggedInUser();

    } catch(InvalidUserException e) {
      log.info("Event creation has no user context, called from probably batch/back-office process, using {} user instead", LifeChangeEventConstant.EXADMIN_USERNAME);
    }

    if(currentUser == null) {
      final AccountUser exAdminUser = userService.findByEmail(LifeChangeEventConstant.EXADMIN_USERNAME);

      if(exAdminUser != null) {
        currentUser = exAdminUser;
        currentUser.setActiveModuleName(RoleService.ADMIN_ROLE);
      }else {
        log.error("Unable to retrieve currently logged in user or admin user: {}", LifeChangeEventConstant.EXADMIN_USERNAME);
        throw new InvalidUserException("Unable to retrieve currently logged in user, or admin user: " + LifeChangeEventConstant.EXADMIN_USERNAME);
      }
    }

    return currentUser;
  }
}
