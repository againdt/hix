import React, {Fragment} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';
import {changeData} from '../../actions/actionCreators';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import bitWiseTools from '../../utils/bitWiseOperatorTools';
import INPUT_TYPES from '../../constants/inputTypes';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';

import VALIDATION_TYPES from '../../constants/validationTypes';
import MASKS from '../../constants/masksConstants';
// ActionCreators


// Utils

// Constants
/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Additional Information - Member Enrolled In Other Health Insurance
 *
 */
const Page4_66 = props => {

    const {
        data,
        form: {
            Form: {
                isFormSubmitted
            }
        },
        familyStatus,
        household,
        householdMembers,
        hhmIndex : member,
        currentHhm
    } = props;
    const {
        applyingForCoverageIndicator,
        personId,
        healthCoverage: {
            eligibleITU,
            receivedITU,
            unpaidBill,
            absentParent
        },
        americanIndianAlaskaNative: {
            memberOfFederallyRecognizedTribeIndicator
        },
        name
    } = currentHhm;
    const {
        taxHouseholdComposition: taxHouseholdCompositionMasks,
        taxHouseholdComposition: {
            taxFilerFlags
        }
    } = MASKS;

    const onFieldChange = (newData) => {
        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member] = {...newData};
        props.actions.changeData(newState);
    };

    const onFieldChange_needChildCoverage = (...args) => {
        const [, indexInArray, , [, value] ] = args;

        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray].flagBucket.taxHouseholdCompositionFlags = bitWiseTools
            .getUpdatedFlags(householdMembers[indexInArray].flagBucket.taxHouseholdCompositionFlags,
                [[taxFilerFlags, value]]);
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member].healthCoverage.coveredDependentChild = familyStatus.relatedPersonsIds.children.every(child => {
            const {
                flagBucket: {
                    taxHouseholdCompositionFlags
                }
            } = householdMembers[child-1];
            return bitWiseTools.convertFlagsToValues(taxHouseholdCompositionFlags, taxHouseholdCompositionMasks).taxFiler;
        });
        props.actions.changeData(newState);
    };


    const childrenHasHealthCoverage = (familyStatus) => {
        if(!familyStatus.relatedPersonsIds.children ||  household.primaryTaxFilerPersonId !== personId) return null;

        const filteredChildren = familyStatus.relatedPersonsIds.children.filter(ch => !householdMembers[ch - 1].applyingForCoverageIndicator);

        return filteredChildren.length > 0 ?
            <div className="usa-width-one-whole gi-component gi-checkbox ">
                <fieldset className="usa-fieldset-inputs usa-sans">
                    <legend className="required-true">Do Children listed below currently have health coverage?</legend>
                    <ul className="usa-unstyled-list">
                        {
                            filteredChildren.map((child, i) => {
                                const {
                                    name,
                                    flagBucket: {
                                        taxHouseholdCompositionFlags
                                    }
                                } = householdMembers[child-1];

                                let value = bitWiseTools.convertFlagsToValues(taxHouseholdCompositionFlags, taxHouseholdCompositionMasks).taxFiler;

                                return (
                                    <li key={`${child}_${i}`}>
                                        <UniversalInput
                                            label={{
                                                text: normalizeNameOfPerson(name)
                                            }}
                                            inputData={{
                                                isInputRequired: true,
                                                type: INPUT_TYPES.CHECKBOX,
                                                name: 'coveredDependentChild',
                                                validationType: VALIDATION_TYPES.CHECKED,
                                                classes: [''],
                                                checked: value,
                                                value: value,
                                                currentData: {},
                                                fields: []
                                            }}
                                            inputActions={{
                                                onFieldChange: onFieldChange_needChildCoverage
                                            }}
                                            indexInArray={child-1}
                                            errorClasses={['margin-l-20-ve']}
                                        />
                                    </li>
                                );

                            })
                        }
                    </ul>
                </fieldset>
            </div> : null;

    };

    const isMemberTheChildWithoutParent = (()=> {
        const analyzeIfNotAllParents = value => !Array.isArray(value) ? true : value.length < 2;
        return applyingForCoverageIndicator && !familyStatus.currentHhm.isOlderThanChildAge && analyzeIfNotAllParents(familyStatus.relatedPersonsIds.parents);
    })();


    const checkTopLevelValidation = () => {
        return true;
    };
    return (
        <Fragment>
            <Form topLevelValidity={checkTopLevelValidation()}>
                <ValidationErrorMessage
                    isVisible={isFormSubmitted && !checkTopLevelValidation()}
                    messageText='Please select the employer that will offer health coverage'
                    errorClasses={['margin-l-0']}
                />
                <div id="aiStateHealthBenefit" className="fade-in">
                    <div className="subsection">
                        {
                            applyingForCoverageIndicator && memberOfFederallyRecognizedTribeIndicator &&
                            <Fragment>
                                <UniversalInput
                                    legend={{
                                        legendText:
                                            <span>
                                                Is&nbsp;
                                                <strong>{normalizeNameOfPerson(name)}</strong>&nbsp;
                                                eligible to get health services from the Indian Health Service,
                                                a tribal health program, or an urban Indian health program or through referral from one of these programs?
                                            </span>
                                    }}
                                    label={{
                                        text: '',
                                        labelClass: ['usa-width-one-whole', 'layout--02'],
                                        ulClass: ['usa-width-one-whole'],
                                        showRequiredIcon: true
                                    }}
                                    inputData={{
                                        isInputRequired: true,
                                        type: INPUT_TYPES.RADIO_BUTTONS,
                                        name: 'eligibleITU',
                                        value: eligibleITU,
                                        options: DROPDOWN_OPTIONS.YES_NO,
                                        validationType: VALIDATION_TYPES.CHOSEN,
                                        currentData: householdMembers[member],
                                        fields: ['healthCoverage']

                                    }}
                                    inputActions={{
                                        onFieldChange: onFieldChange
                                    }}
                                    errorClasses={['margin-l-20-ve']}
                                />
                                <UniversalInput
                                    legend={{
                                        legendText:
                                            <span>
                                            Has&nbsp;
                                                <strong>{normalizeNameOfPerson(name)}</strong>&nbsp;
                                                ever gotten a health service from the Indian Health Service,
                                            a tribal health program, or urban Indian health program
                                            or through a referral from one of these programs?
                                            </span>
                                    }}
                                    label={{
                                        text: '',
                                        labelClass: ['usa-width-one-whole', 'layout--02'],
                                        ulClass: ['usa-width-one-whole'],
                                        showRequiredIcon: true
                                    }}
                                    inputData={{
                                        isInputRequired: true,
                                        type: INPUT_TYPES.RADIO_BUTTONS,
                                        name: 'receivedITU',
                                        value: receivedITU,
                                        options: DROPDOWN_OPTIONS.YES_NO,
                                        validationType: VALIDATION_TYPES.CHOSEN,
                                        currentData: householdMembers[member],
                                        fields: ['healthCoverage']

                                    }}
                                    inputActions={{
                                        onFieldChange: onFieldChange
                                    }}
                                    errorClasses={['margin-l-20-ve']}
                                />
                            </Fragment>

                        }
                        {
                            applyingForCoverageIndicator  &&

                            <UniversalInput
                                legend={{
                                    legendText:
                                        <span className="required-true">
                                            Would&nbsp;
                                            <strong>{normalizeNameOfPerson(name)}</strong>&nbsp;
                                            like help paying for medical bills from the last 3 months?
                                        </span>
                                }}
                                label={{
                                    text: '',
                                    labelClass: ['usa-width-one-whole', 'layout--02'],
                                    ulClass: ['usa-width-one-whole'],
                                    showRequiredIcon: false
                                }}
                                inputData={{
                                    isInputRequired: true,
                                    type: INPUT_TYPES.RADIO_BUTTONS,
                                    name: 'unpaidBill',
                                    value: unpaidBill,
                                    options: DROPDOWN_OPTIONS.YES_NO,
                                    validationType: VALIDATION_TYPES.CHOSEN,
                                    currentData: householdMembers[member],
                                    fields: ['healthCoverage']

                                }}
                                inputActions={{
                                    onFieldChange: onFieldChange
                                }}
                                errorClasses={['margin-l-20-ve']}
                            />
                        }
                        {
                            childrenHasHealthCoverage(familyStatus)
                        }
                        {
                            isMemberTheChildWithoutParent &&

                            <UniversalInput
                                legend={{
                                    legendText:
                                        <span>
                                            Does <strong>{normalizeNameOfPerson(name)}</strong>&nbsp; have a parent living outside the home?
                                        </span>
                                }}
                                label={{
                                    text: '',
                                    labelClass: ['usa-width-one-whole', 'layout--02'],
                                    ulClass: ['usa-width-one-whole'],
                                    showRequiredIcon: true
                                }}
                                inputData={{
                                    isInputRequired: true,
                                    type: INPUT_TYPES.RADIO_BUTTONS,
                                    name: 'absentParent',
                                    value: absentParent,
                                    options: DROPDOWN_OPTIONS.YES_NO,
                                    validationType: VALIDATION_TYPES.CHOSEN,
                                    currentData: householdMembers[member],
                                    fields: ['healthCoverage']

                                }}
                                inputActions={{
                                    onFieldChange: onFieldChange
                                }}
                                errorClasses={['margin-l-20-ve']}
                            />
                        }

                    </div>
                </div>
            </Form>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,

    household: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0],

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    page: state.page
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page4_66.defaultProps = {
    name: 'Page4_66'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page4_66);
