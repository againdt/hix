<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="org.springframework.context.MessageSource"%>
<%@page import="org.springframework.beans.factory.annotation.Autowired"%>
<%@page import="com.getinsured.hix.platform.util.DisplayUtil"%>
<%@page import="com.getinsured.hix.platform.config.SecurityConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>

<div class="gutter10">
	<h1>Unlock Your Account</h1>

	<div class="row-fluid">
    	<%
    	WebApplicationContext webAppContext = RequestContextUtils.getWebApplicationContext(request);
    	MessageSource messageSource = (MessageSource)webAppContext.getBean("messageSource");
    	String passwordPolicyRegex = "";
    	String errorMsg = "";
    	if(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_PASSWORD_POLICY_ACTIVE)=="Y"){
    		passwordPolicyRegex = DisplayUtil.replaceDynamicValuesInMessageSource(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX));
    		errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexity", null, LocaleContextHolder.getLocale()));
    	}%>
    	
		<div class="span3" id="sidebar">
		<label for="passwordPolicyRegex" aria-hidden="true" class="aria-hidden">passwordPolicyRegex</label>
			<input type="hidden" id="passwordPolicyRegex" name="passwordPolicyRegex" value="<%=passwordPolicyRegex%>"/>	   
	        <label for="passwordPolicyErrorMsg" aria-hidden="true" class="aria-hidden">passwordPolicyErrorMsg</label>
	        <input type="hidden" id="passwordPolicyErrorMsg" name="passwordPolicyErrorMsg" value="<%=errorMsg%>"/>	
        
			<div class="header">
				<h4 class="margin0">You should know</h4>
			</div>
			<ul class="nav nav-list">
				<li>To unlock your account you will need your email, answers to
					security questions and certain miscellaneous personal information.</li>
			</ul>
		</div>

		<div class="span9" id="rightpanel">		
			<div class="header">
				<c:choose>
					<c:when test="${STEP_NO != null}">
						<h4><c:out value="${STEP_NO}"></c:out></h4>
					</c:when>
					<c:otherwise>
						<h4>Step 5: Change your password</h4>
					</c:otherwise>
				</c:choose>
			</div>
			
			<%-- <c:choose>
				<c:when test="${not empty isAdminRole}">
					<div class="row-fluid"  id="member-portal">		  	
						<div class="well-step">
							<div class="row-fluid">
								<div id="step-progress">
									<div id="progress-bar">
										<ul>
											<li id="step-one" class="step-desc active-state step4"><span class="s-no">1</span></li>
											<li id="step-two" class="step-desc active-state step4"><span class="s-no">2</span></li>
											<li id="step-three" class="step-desc active-state step4"><span class="s-no">3</span></li>
											<li id="step-four" class="step-desc active-state step4"><span class="s-no">4</span></li>
										</ul>
									</div>
								</div>
							</div>
						</div> 
					</div>
				</c:when>
				<c:otherwise>
					<div class="row-fluid"  id="member-portal">		  	
						<div class="well-step">
							<div class="row-fluid">
								<div id="step-progress">
									<div id="progress-bar">
										<ul>
											<li id="step-one" class="step-desc active-state step5"><span class="s-no">1</span></li>
											<li id="step-two" class="step-desc active-state step5"><span class="s-no">2</span></li>
											<li id="step-three" class="step-desc active-state step5"><span class="s-no">3</span></li>
											<li id="step-four" class="step-desc active-state step5"><span class="s-no">4</span></li>
											<li id="step-five" class="step-desc active-state step5"><span class="s-no">5</span></li>
										</ul>
									</div>
									<!--/End Progress-->
								</div>
								<!--/End step-progress-->
							</div>
							<!--/End row-fluid-->
						</div>
					<!-- End .well-step -->
					</div>
				</c:otherwise>
			</c:choose> --%>
				
			<form class="form-horizontal gutter10" id="frmChangePassword" autocomplete="off" name="frmChangePassword" action="<c:url value="/account/unlock/password"/>" method="POST">

				<df:csrfToken/>
				<div class="control-group">
					<label for="" newPassword"" class="control-label">New
						Password</label>
					<div class="controls">
						<input type="password" id="newPassword" autocomplete="off" name="newPassword">
						<div id="newPassword_error"></div>
					</div>
					
				</div>

				<div class="control-group">
					<label for="" confirmPassword"" class="control-label">Confirm
						Password</label>
					<div class="controls">
						<input type="password" id="confirmPassword" autocomplete="off" name="confirmPassword">
						<div id="confirmPassword_error"></div>
					</div>
					
				</div>
				
				<div class="control-group">
					<input type="hidden" id="passwordcheck" name="passwordcheck" value=""/>
					<div id="passwordcheck_error"></div>
				</div>

				<div class="control-group">
					<div class="controls">
						<input type="button" name="submitbtn" id="submitbtn" onClick="javascript:checkUserPassword();" value="Unlock Account" 	class="btn btn-primary btn-large" />
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="modal hide fade" id="myModal" style="display: none">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
	</div>
	<div class="modal-body">
		<p>${message}</p>
	</div>
	<div class="modal-footer">
		<c:choose>
			<c:when test="${error == true}">
				<button class="btn btn-primary" data-dismiss="modal">OK</button>
			</c:when>
			<c:otherwise>
				<a class="btn btn-primary" type="button"
					href="<c:url value="/account/user/login"/>">OK</a>
			</c:otherwise>
		</c:choose>
	</div>
</div>

<script type="text/javascript">
	
	$(document).ready(function() {
		var isShow ='${isShow}';
		if(isShow=="true") {
			$('#myModal').modal('show');
		} 
	});
	
	var validator = $("#frmChangePassword").validate({ 
		onkeyup: false,
		onclick: false,
		rules : {
			newPassword : {required : true, validatepassword:true },
			confirmPassword : {required : true, confirmPasswordCheck: true},			
			},
		messages : {
			newPassword: { required : "<span> <em class='excl'>!</em>New password cannot be empty</span>",
								validatepassword : "<span> <em class='excl'>!</em> "+$("#passwordPolicyErrorMsg").val()+" </span>"},
			confirmPassword: { required : "<span> <em class='excl'>!</em>Confirm password cannot be empty</span>", 
								confirmPasswordCheck : "<span> <em class='excl'>!</em>New password and confirm password does not match</span>"},
	 	}
		,
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		} 
	});
	
	jQuery.validator.addMethod("confirmPasswordCheck", function(value, element, param) {
		confirmpassword = $("#confirmPassword").val().trim(); 
		newpassword = $("#newPassword").val().trim(); 
		
		if(newpassword!=confirmpassword){
			return false;
		} 
		return true;
	});

	jQuery.validator.addMethod("validatepassword", function(value,
			element, param) {
		password = $("#newPassword").val();
		var re = RegExp(($("#passwordPolicyRegex").val()));///(?=^.{8,20}$)(?=.*[a-zA-Z])(?=.*[0-9])(?!.*\s).*$/; 
		if (!re.test(password)) {
			return false;
		}
		return true;

	});

	function checkUserPassword()
	{
		var csrftoken;
		csrftoken= '${sessionScope.csrftoken}';
		
		if($("#frmChangePassword").validate().form()){
			
			var validateUrl = "<c:url value='/account/signup/unlockUserPasswordLoggedInUser'/>";
			$.ajax({
				url : validateUrl,
				type : "POST",
				data : {
					password : $("#newPassword").val(), action : 'USER_FROM_SESSION', csrftoken :csrftoken
				},
				success: function(response)
				{
					if (response!="")
					{
						$('#newPassword_error').html("<label class='error'><span> <em class='excl'>!</em>"+response+"</span></label>");
						$('#newPassword_error').attr('class','error help-inline');
						/*$("#passwordPolicyErrorMsg").html(response)  ;
						document.getElementById('passwordPolicyErrorMsg').value = response;
						*/return false;
					}
					else{
						

								$('#newPassword_error').html("");
								$('#newPassword_error').removeAttr("class");
								$("#frmChangePassword").submit();  	
						
					}
				},
				
			});
		}
	}

		
</script>

<style>
/* .modal{
		text-align: center;
	} */
</style>

