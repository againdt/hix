package com.getinsured.ssap.model.hub.vlp.response;

import java.util.Date;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.ssap.model.hub.vlp.documents.DhsDocument;

/**
 * @author Yevgen Golubenko
 * @since 2/27/19
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IndividualDetails {

  @JsonProperty("LawfulPresenceVerifiedCode")
  private MultiCode lawfulPresenceVerifiedCode;

  @JsonProperty("CaseNumber")
  private String caseNumber;

  @JsonProperty("AgencyAction")
  private String agencyAction;

  @JsonProperty("NonCitFirstName")
  private String nonCitizenFirstName;

  @JsonProperty("NonCitLastName")
  private String nonCitizenLastName;

  @JsonProperty("NonCitMiddleName")
  private String nonCitizenMiddleName;

  @JsonProperty("NonCitProvOfLaw")
  private String nonCitizenProvOfLaw;

  @JsonProperty("USCitizenCode")
  private MultiCode usCitizenCode;

  @JsonProperty("NonCitCoaCode")
  private String nonCitizenCoaCode;

  @JsonProperty("EligStatementCd")
  private int eligibilityStatementCode;

  @JsonProperty("ArrayOfSponsorshipData")
  private Object arrayOfSponsorshipData; // TODO: SponsorshipData[]

  @JsonProperty("NonCitCountryCitCd")
  private String nonCitizenCountryCitCd;

  @JsonProperty("NonCitCountryBirthCd")
  private String nonCitizenCountryBirthCd;

  @JsonFormat(pattern = DhsDocument.DATE_FORMAT)
  @JsonProperty("NonCitEadsExpireDate")
  private Date nonCitizenEadsExpireDate;

  @JsonProperty("QualifiedNonCitizenCode")
  private MultiCode qualifiedNonCitizenCode;

  @JsonFormat(pattern = DhsDocument.DATE_FORMAT)
  @JsonProperty("NonCitEntryDate")
  private Date nonCitizenEntryDate;

  @JsonFormat(pattern = DhsDocument.DATE_FORMAT)
  @JsonProperty("NonCitAdmittedToDate")
  private Date nonCitizenAdmittedToDate;

  @JsonProperty("NonCitAdmittedToText")
  private String nonCitizenAdmittedToText;

  @JsonProperty("WebServSftwrVer")
  private String webServiceSoftwareVersion;

  @JsonProperty("FiveYearBarMetCode")
  private MultiCode fiveYearBarMetCode;

  @JsonProperty("SponsorDataFoundIndicator")
  private boolean sponsorDataFoundIndicator;

  @JsonProperty("SponsorshipReasonCd")
  private String sponsorshipReasonCd;

  @JsonFormat(pattern = DhsDocument.DATE_FORMAT)
  @JsonProperty("NonCitBirthDate")
  private Date nonCitizenBirthDate;

  @JsonProperty("EligStatementTxt")
  private String eligibilityStatementText;

  @JsonProperty("FiveYearBarApplyCode")
  private MultiCode fiveYearBarApplyCode;

  @JsonFormat(pattern = DhsDocument.DATE_FORMAT)
  @JsonProperty("GrantDate")
  private Date grantDate;

  @JsonProperty("GrantDateReasonCd")
  private String grantDateReasonCd;

  @JsonProperty("IAVTypeTxt")
  private String iavTypeText;

  @JsonProperty("IAVTypeCode")
  private InvalidDataTypeCode iavTypeCode;

  @JsonFormat(pattern = DhsDocument.DATE_FORMAT)
  @JsonProperty("AdmittedToDate")
  private Date admittedToDate;

  @JsonProperty("AdmittedToText")
  private String admittedToText;

  @JsonProperty("ResponseMetadata")
  private ResponseMetadata responseMetadata;

  @JsonProperty("ArrayOfErrorResponseMetadata")
  private Object errorResponseMetadata;

  public MultiCode getLawfulPresenceVerifiedCode()
  {
    return lawfulPresenceVerifiedCode;
  }

  public void setLawfulPresenceVerifiedCode(final MultiCode lawfulPresenceVerifiedCode)
  {
    this.lawfulPresenceVerifiedCode = lawfulPresenceVerifiedCode;
  }

  public String getCaseNumber()
  {
    return caseNumber;
  }

  public void setCaseNumber(final String caseNumber)
  {
    this.caseNumber = caseNumber;
  }

  public String getAgencyAction()
  {
    return agencyAction;
  }

  public void setAgencyAction(final String agencyAction)
  {
    this.agencyAction = agencyAction;
  }

  public String getNonCitizenFirstName()
  {
    return nonCitizenFirstName;
  }

  public void setNonCitizenFirstName(final String nonCitizenFirstName)
  {
    this.nonCitizenFirstName = nonCitizenFirstName;
  }

  public String getNonCitizenLastName()
  {
    return nonCitizenLastName;
  }

  public void setNonCitizenLastName(final String nonCitizenLastName)
  {
    this.nonCitizenLastName = nonCitizenLastName;
  }

  public String getNonCitizenMiddleName()
  {
    return nonCitizenMiddleName;
  }

  public void setNonCitizenMiddleName(final String nonCitizenMiddleName)
  {
    this.nonCitizenMiddleName = nonCitizenMiddleName;
  }

  public String getNonCitizenProvOfLaw()
  {
    return nonCitizenProvOfLaw;
  }

  public void setNonCitizenProvOfLaw(final String nonCitizenProvOfLaw)
  {
    this.nonCitizenProvOfLaw = nonCitizenProvOfLaw;
  }

  public MultiCode getUsCitizenCode()
  {
    return usCitizenCode;
  }

  public void setUsCitizenCode(final MultiCode usCitizenCode)
  {
    this.usCitizenCode = usCitizenCode;
  }

  public String getNonCitizenCoaCode()
  {
    return nonCitizenCoaCode;
  }

  public void setNonCitizenCoaCode(final String nonCitizenCoaCode)
  {
    this.nonCitizenCoaCode = nonCitizenCoaCode;
  }

  public int getEligibilityStatementCode()
  {
    return eligibilityStatementCode;
  }

  public void setEligibilityStatementCode(final int eligibilityStatementCode)
  {
    this.eligibilityStatementCode = eligibilityStatementCode;
  }

  public Object getArrayOfSponsorshipData()
  {
    return arrayOfSponsorshipData;
  }

  public void setArrayOfSponsorshipData(final Object arrayOfSponsorshipData)
  {
    this.arrayOfSponsorshipData = arrayOfSponsorshipData;
  }

  public String getNonCitizenCountryCitCd()
  {
    return nonCitizenCountryCitCd;
  }

  public void setNonCitizenCountryCitCd(final String nonCitizenCountryCitCd)
  {
    this.nonCitizenCountryCitCd = nonCitizenCountryCitCd;
  }

  public String getNonCitizenCountryBirthCd()
  {
    return nonCitizenCountryBirthCd;
  }

  public void setNonCitizenCountryBirthCd(final String nonCitizenCountryBirthCd)
  {
    this.nonCitizenCountryBirthCd = nonCitizenCountryBirthCd;
  }

  public Date getNonCitizenEadsExpireDate()
  {
    return nonCitizenEadsExpireDate;
  }

  public void setNonCitizenEadsExpireDate(final Date nonCitizenEadsExpireDate)
  {
    this.nonCitizenEadsExpireDate = nonCitizenEadsExpireDate;
  }

  public MultiCode getQualifiedNonCitizenCode()
  {
    return qualifiedNonCitizenCode;
  }

  public void setQualifiedNonCitizenCode(final MultiCode qualifiedNonCitizenCode)
  {
    this.qualifiedNonCitizenCode = qualifiedNonCitizenCode;
  }

  public Date getNonCitizenEntryDate()
  {
    return nonCitizenEntryDate;
  }

  public void setNonCitizenEntryDate(final Date nonCitizenEntryDate)
  {
    this.nonCitizenEntryDate = nonCitizenEntryDate;
  }

  public Date getNonCitizenAdmittedToDate()
  {
    return nonCitizenAdmittedToDate;
  }

  public void setNonCitizenAdmittedToDate(final Date nonCitizenAdmittedToDate)
  {
    this.nonCitizenAdmittedToDate = nonCitizenAdmittedToDate;
  }

  public String getNonCitizenAdmittedToText()
  {
    return nonCitizenAdmittedToText;
  }

  public void setNonCitizenAdmittedToText(final String nonCitizenAdmittedToText)
  {
    this.nonCitizenAdmittedToText = nonCitizenAdmittedToText;
  }

  public String getWebServiceSoftwareVersion()
  {
    return webServiceSoftwareVersion;
  }

  public void setWebServiceSoftwareVersion(final String webServiceSoftwareVersion)
  {
    this.webServiceSoftwareVersion = webServiceSoftwareVersion;
  }

  public MultiCode getFiveYearBarMetCode()
  {
    return fiveYearBarMetCode;
  }

  public void setFiveYearBarMetCode(final MultiCode fiveYearBarMetCode)
  {
    this.fiveYearBarMetCode = fiveYearBarMetCode;
  }

  public boolean isSponsorDataFoundIndicator()
  {
    return sponsorDataFoundIndicator;
  }

  public void setSponsorDataFoundIndicator(final boolean sponsorDataFoundIndicator)
  {
    this.sponsorDataFoundIndicator = sponsorDataFoundIndicator;
  }

  public String getSponsorshipReasonCd()
  {
    return sponsorshipReasonCd;
  }

  public void setSponsorshipReasonCd(final String sponsorshipReasonCd)
  {
    this.sponsorshipReasonCd = sponsorshipReasonCd;
  }

  public Date getNonCitizenBirthDate()
  {
    return nonCitizenBirthDate;
  }

  public void setNonCitizenBirthDate(final Date nonCitizenBirthDate)
  {
    this.nonCitizenBirthDate = nonCitizenBirthDate;
  }

  public String getEligibilityStatementText()
  {
    return eligibilityStatementText;
  }

  public void setEligibilityStatementText(final String eligibilityStatementText)
  {
    this.eligibilityStatementText = eligibilityStatementText;
  }

  public MultiCode getFiveYearBarApplyCode()
  {
    return fiveYearBarApplyCode;
  }

  public void setFiveYearBarApplyCode(final MultiCode fiveYearBarApplyCode)
  {
    this.fiveYearBarApplyCode = fiveYearBarApplyCode;
  }

  public Date getGrantDate()
  {
    return grantDate;
  }

  public void setGrantDate(final Date grantDate)
  {
    this.grantDate = grantDate;
  }

  public String getGrantDateReasonCd()
  {
    return grantDateReasonCd;
  }

  public void setGrantDateReasonCd(final String grantDateReasonCd)
  {
    this.grantDateReasonCd = grantDateReasonCd;
  }

  public String getIavTypeText()
  {
    return iavTypeText;
  }

  public void setIavTypeText(final String iavTypeText)
  {
    this.iavTypeText = iavTypeText;
  }

  public InvalidDataTypeCode getIavTypeCode()
  {
    return iavTypeCode;
  }

  public void setIavTypeCode(final InvalidDataTypeCode iavTypeCode)
  {
    this.iavTypeCode = iavTypeCode;
  }

  public Date getAdmittedToDate()
  {
    return admittedToDate;
  }

  public void setAdmittedToDate(final Date admittedToDate)
  {
    this.admittedToDate = admittedToDate;
  }

  public String getAdmittedToText()
  {
    return admittedToText;
  }

  public void setAdmittedToText(final String admittedToText)
  {
    this.admittedToText = admittedToText;
  }

  public ResponseMetadata getResponseMetadata()
  {
    return responseMetadata;
  }

  public void setResponseMetadata(final ResponseMetadata responseMetadata)
  {
    this.responseMetadata = responseMetadata;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", IndividualDetails.class.getSimpleName() + "[", "]")
        .add("lawfulPresenceVerifiedCode=" + lawfulPresenceVerifiedCode)
        .add("caseNumber='" + caseNumber + "'")
        .add("agencyAction='" + agencyAction + "'")
        .add("nonCitizenFirstName='" + nonCitizenFirstName + "'")
        .add("nonCitizenLastName='" + nonCitizenLastName + "'")
        .add("nonCitizenMiddleName='" + nonCitizenMiddleName + "'")
        .add("nonCitizenProvOfLaw='" + nonCitizenProvOfLaw + "'")
        .add("usCitizenCode=" + usCitizenCode)
        .add("nonCitizenCoaCode='" + nonCitizenCoaCode + "'")
        .add("eligibilityStatementCode=" + eligibilityStatementCode)
        .add("arrayOfSponsorshipData=" + arrayOfSponsorshipData)
        .add("nonCitizenCountryCitCd='" + nonCitizenCountryCitCd + "'")
        .add("nonCitizenCountryBirthCd='" + nonCitizenCountryBirthCd + "'")
        .add("nonCitizenEadsExpireDate=" + nonCitizenEadsExpireDate)
        .add("qualifiedNonCitizenCode=" + qualifiedNonCitizenCode)
        .add("nonCitizenEntryDate=" + nonCitizenEntryDate)
        .add("nonCitizenAdmittedToDate=" + nonCitizenAdmittedToDate)
        .add("nonCitizenAdmittedToText='" + nonCitizenAdmittedToText + "'")
        .add("webServiceSoftwareVersion='" + webServiceSoftwareVersion + "'")
        .add("fiveYearBarMetCode=" + fiveYearBarMetCode)
        .add("sponsorDataFoundIndicator=" + sponsorDataFoundIndicator)
        .add("sponsorshipReasonCd='" + sponsorshipReasonCd + "'")
        .add("nonCitizenBirthDate=" + nonCitizenBirthDate)
        .add("eligibilityStatementText='" + eligibilityStatementText + "'")
        .add("fiveYearBarApplyCode=" + fiveYearBarApplyCode)
        .add("grantDate=" + grantDate)
        .add("grantDateReasonCd='" + grantDateReasonCd + "'")
        .add("iavTypeText='" + iavTypeText + "'")
        .add("iavTypeCode=" + iavTypeCode)
        .add("admittedToDate=" + admittedToDate)
        .add("admittedToText='" + admittedToText + "'")
        .add("responseMetadata=" + responseMetadata)
        .add("errorResponseMetadata=" + errorResponseMetadata)
        .toString();
  }
}
