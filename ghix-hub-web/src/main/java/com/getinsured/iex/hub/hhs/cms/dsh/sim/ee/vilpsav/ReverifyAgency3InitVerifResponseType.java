
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReverifyAgency3InitVerifResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReverifyAgency3InitVerifResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}ReverifyAgency3InitVerifResponseSet" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}ResponseMetadata"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReverifyAgency3InitVerifResponseType", propOrder = {
    "reverifyAgency3InitVerifResponseSet",
    "responseMetadata"
})
public class ReverifyAgency3InitVerifResponseType {

    @XmlElement(name = "ReverifyAgency3InitVerifResponseSet")
    protected ReverifyAgency3InitVerifResponseSetType reverifyAgency3InitVerifResponseSet;
    @XmlElement(name = "ResponseMetadata", required = true)
    protected ResponseMetadataType responseMetadata;

    /**
     * Gets the value of the reverifyAgency3InitVerifResponseSet property.
     * 
     * @return
     *     possible object is
     *     {@link ReverifyAgency3InitVerifResponseSetType }
     *     
     */
    public ReverifyAgency3InitVerifResponseSetType getReverifyAgency3InitVerifResponseSet() {
        return reverifyAgency3InitVerifResponseSet;
    }

    /**
     * Sets the value of the reverifyAgency3InitVerifResponseSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReverifyAgency3InitVerifResponseSetType }
     *     
     */
    public void setReverifyAgency3InitVerifResponseSet(ReverifyAgency3InitVerifResponseSetType value) {
        this.reverifyAgency3InitVerifResponseSet = value;
    }

    /**
     * Gets the value of the responseMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMetadataType }
     *     
     */
    public ResponseMetadataType getResponseMetadata() {
        return responseMetadata;
    }

    /**
     * Sets the value of the responseMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMetadataType }
     *     
     */
    public void setResponseMetadata(ResponseMetadataType value) {
        this.responseMetadata = value;
    }

}
