(function() {
'use strict';

	angular.module('transferConsumerDelegationsCtrl', [])
	.controller('TransferConsumerDelegationsCtrl', TransferConsumerDelegationsCtrl);

	TransferConsumerDelegationsCtrl.$inject = ['$stateParams', '$http', '$location', '$state', 'StatusService'];

    function TransferConsumerDelegationsCtrl($stateParams, $http, $location, $state, StatusService) {
    	var vm = this;

    	var SEARCH_AGENT_URL = '/hix/agency/searchagents';
    	var REASSIGN_AGENT_URL = '/hix/agency/agentbulktransfer';
    	var AGENT_SITE_URL = '/hix/agency/site/list';
    	var EXPORT_AGENT_URL = '/hix/agency/export/';

    	vm.init = init;
		var switchAccUrl = $('#switchAccUrl').val();
    	vm.searchAgent = searchAgent;
    	vm.reassignAgent = reassignAgent;
		vm.startApplication = startApplication;
    	vm.agencyCertificationStatus = StatusService.CertificationStatus;
		vm.isNewApplication = $location.search().newApplication;
    	function init(){
			if(vm.agencyCertificationStatus !== 'CERTIFIED') {
				$state.go("viewagentlist");
			}
    		if($stateParams.delegatedIndividuals === undefined){
    			vm.delegatedIndividuals = $stateParams.delegatedIndividuals;
    		}else if(Array.isArray($stateParams.delegatedIndividuals)){
    			vm.delegatedIndividuals = $stateParams.delegatedIndividuals;
    		}else{
    			vm.delegatedIndividuals = [$stateParams.delegatedIndividuals];
    		}


    		vm.delegatedAgent = $stateParams.delegatedAgent
    		vm.flow = $stateParams.flow;

    		getSites();

    	}

    	function getSites(){
    		vm.loader = true;
    		var promise = $http.get(AGENT_SITE_URL);

			promise.success(function(response){
				vm.loader = false;
				vm.sites = [];
				if(response) {
					response.forEach(function(site){
						vm.sites.push(site.siteLocationName);
					});
				}
    		});

			promise.error(function(response){
				vm.loader = false;
			});
    	}

    	function searchAgent(){
    		vm.loader = true;
    		var agentRequestData = {
				firstName: vm.agentFirstName,
				lastName: vm.agentLastName,
				pageNumber: vm.pageNumber,
				sortBy: null,
				sortOrder: null,
				email: vm.agentEmail,
				siteName: vm.agentSiteName,
				licenseNumber: vm.licenseNumber
    		}

    		var promise = $http.post(SEARCH_AGENT_URL, agentRequestData);

			promise.success(function(response){
				vm.loader = false;
    			vm.agentsData = response;
    			getPrimarySite();

    		});

			promise.error(function(response){
				vm.loader = false;
			});
    	}


    	function getPrimarySite(){
    		vm.agentsData.brokers.forEach(function(agent){
    			agent.primarySite = '';

				if(agent.location.address1 !== null){
					agent.primarySite += agent.location.address1 + ' ';
				}

				if(agent.location.address2 !== null){
					agent.primarySite += agent.location.address2 + ' ';
				}

				if(agent.location.city !== null){
					agent.primarySite += agent.location.city + ', ';
				}

				if(agent.location.state !== null){
					agent.primarySite += agent.location.state + ' ';
				}

				if(agent.location.zip !== null){
					agent.primarySite += agent.location.zip;
				}


			})
    	}

		function startApplication(id) {
			var brokerId = id;
			var existURL = switchAccUrl+'&_pageLabel=individualHomePage&ahbxId=&ahbxUserType=1&recordId='+brokerId+'&recordType=2&newAppInd=Y&lang=en_US';
			window.location.href = existURL;
		}

    	function reassignAgent(){
    		vm.loader = true;

    		var reassignRequestData = {
    			agentIds: null,
    			targetAgentId: vm.targetAgent.brokerId,
    			action: 'Transfer',
    			individualIds: null
    		}

    		if(vm.delegatedIndividuals !== undefined){
    			//delegate one or a set individuals
    			var delegatedIndividuals = [];

    			vm.delegatedIndividuals.forEach(function(individual){
        			delegatedIndividuals.push({
        				agentId: individual.brokerId,
        				individualId: individual.individualId,
        				designationStatus: individual.status,
        			});
        		})

        		reassignRequestData.individualIds = delegatedIndividuals;

    		}else{
    			//delegate all individuals
    			reassignRequestData.agentIds = [vm.delegatedAgent.encryptedId];
    		}

    		var promise = $http.post(REASSIGN_AGENT_URL, reassignRequestData);

			promise.success(function(response){
				vm.loader = false;

				vm.agentName = vm.targetAgent.firstName + ' ' + vm.targetAgent.lastName;
    			if(response === 'Failure'){
    				$('#transferError').modal();
    			}else{
    				$('#transferSuccess').modal({
    					backdrop: 'static',
    					keyboard: false
    				});
    			}

    		});

			promise.error(function(response){
				vm.loader = false;
				$('#transferError').modal();
			});
    	}
    }

})();
