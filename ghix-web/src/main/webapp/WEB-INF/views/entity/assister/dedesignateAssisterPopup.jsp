<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<html>
	<body>
		<div id="header" class="gray"></div>
		<div id="dedesignateAssisterBox">
			<form class="form-vertical" id="dedesignateAssisterForm" name="dedesignateAssisterForm" method="POST" target="_parent">
				<input type="hidden" name="assisterId" id="assisterId" value="<encryptor:enc value="${assisterId}" isurl="true"/>">
								
				<div class="profile">
					<div id="confirmationMessage">
						<h4 class="hide jsConfirmHeading">
						<spring:message  code="label.brkconfirmmessage1"/> ${assisterName} <spring:message  code="label.brkconfirmmessage2"/>
						</h4><br/>
						<p>
						<spring:message  code="label.brkconfirm1"/> ${assisterName} <%-- <spring:message  code="label.brkconfirm2"/> --%>
						</p><br/>
					</div>
						<input name="submitFromLightbox" id="submitFromLightbox" type="button" onClick="dedesignateAssister();" value="<spring:message  code='label.brkConfirm'/>" title="<spring:message  code='label.brkConfirm'/>" class="btn btn-primary pull-right" />					
						<input type="button"  class="btn pull-left" data-dismiss="modal" onClick="window.parent.closeLightboxOnCancel();" value="<spring:message  code='label.entity.cancel'/>" title="<spring:message  code='label.entity.cancel'/>" />	

				</div>
			</form>	
		</div>
	</body>
</html>

<script type="text/javascript">	
$(function (){
	var confirmHeading = $(".jsConfirmHeading").text();
	  var headerMessage = '<h4>' + confirmHeading + '<button type="button" class="modalClose close" aria-hidden="true">x</button></h4>';
	 window.parent.$(".jsValidateDedesignateModal").find(".modal-header").html(headerMessage);
	});
	function dedesignateAssister() 
	{			
		$.ajax({
	        type: "GET",
	        url: "dedesignateAssister/"+$("#assisterId").val(),
	        success: function(response){	
	        	if(response == 'success')
        		{       	
	        		window.parent.parent.location.reload();     
        		}	        	
	        },	        
		});
	}		
	
</script>
