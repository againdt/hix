package com.getinsured.iex.ssap.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.renewal.util.RenewalConstants;
import com.getinsured.iex.ssap.model.RenewalApplicant;

@RestResource(path = "RenewalApplicant")
public interface RenewalApplicantRepository extends JpaRepository<RenewalApplicant, Long> {
	
	@Query("select ra from RenewalApplicant ra where ra.ssapApplicationId = :ssapApplicationId and ra.renewalStatus in ('APP_ERROR','TO_CONSIDER') ")
	List<RenewalApplicant> findBySsapApplicationIdAndRenewalStatus(@Param("ssapApplicationId") Long ssapApplicationId);

	@Query("select ra from RenewalApplicant ra where ra.ssapApplicationId = :applicationId")
	List<RenewalApplicant> findBySsapApplicationId(@Param("applicationId") long applicationId);
	
	@Modifying	
	@Transactional
	@Query(nativeQuery = true, value = "DELETE FROM RENEWAL_APPLICANTS WHERE SSAP_APPLICATION_ID = :applicationId")
	void deleteBySsapApplicationId(@Param("applicationId") Long applicationId);

	@Modifying
	@Transactional
	@Query("UPDATE RenewalApplicant SET renewalStatus = :setRenewalStatus, groupRenewalStatus = :setRenewalStatus, "
			+ "groupReasonCode = :setReasonCode "
			+ "WHERE ssapApplicationId = :ssapApplicationId AND UPPER(planType) = UPPER(:planType)")
	int updateApplicantRenewalStatusByApplIdAndPlanType(@Param("setRenewalStatus") RenewalStatus setRenewalStatus,
			@Param("setReasonCode") Integer setReasonCode, @Param("ssapApplicationId") Long ssapApplicationId,
			@Param("planType") String planType);

	@Query("from RenewalApplicant where memberId = :memberId and ssapApplicationId = :ssapApplicationId and UPPER(planType) = :planType")
	RenewalApplicant findByMemberIdAndPlanType(@Param("memberId") String memberId, @Param("planType") String planType,@Param("ssapApplicationId") long ssapApplicationId);
	
	@Modifying
	@Transactional
	@Query("UPDATE RenewalApplicant SET renewalStatus = :setRenewalStatus, groupRenewalStatus = :setRenewalStatus, lastUpdateTimestamp = :lastUpdateTimestamp, "
			+ "groupReasonCode = :setReasonCode "
			+ "WHERE ssapApplicationId = :ssapApplicationId")
	int updateRenewalStatusByApplicationId(@Param("setRenewalStatus") RenewalStatus setRenewalStatus,
			@Param("setReasonCode") Integer setReasonCode, @Param("ssapApplicationId") long ssapApplicationId,
			@Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp);
	
	@Query("FROM RenewalApplicant WHERE renewalStatus = :renewalStatus AND ssapApplicationId = :ssapApplicationId AND UPPER(planType) = :planType")
	List<RenewalApplicant> findMembersByPlanType(@Param("renewalStatus") RenewalStatus renewalStatus,@Param("planType") String planType,@Param("ssapApplicationId") long ssapApplicationId);
	
	@Query("select distinct enrollmentId, groupReasonCode from RenewalApplicant where ssapApplicationId = :ssapApplicationId and planType = 'Health' and enrollmentId is not null and groupReasonCode is not null")
	public List<Object[]> getEnrollmentWithGroupReasonCodes(@Param("ssapApplicationId") Long ssapApplicationId);
	
	@Query("select distinct enrollmentId, memberId, memberFalloutReasonCode from RenewalApplicant where ssapApplicationId = :ssapApplicationId and planType = :planType and enrollmentId is not null and memberFalloutReasonCode is not null")
	public List<Object[]> getEnrollmentWithMembersAndFallOutReasonCodesDetail(@Param("ssapApplicationId") Long ssapApplicationId, @Param("planType") String planType);
	
	
	@Query("FROM RenewalApplicant WHERE renewalStatus = :renewalStatus AND ssapApplicationId = :ssapApplicationId AND UPPER(planType) = :planType AND MEMBER_FALLOUT_REASON_CODE = :reasonCode")
	List<RenewalApplicant> findNewMembersByPlanType(@Param("renewalStatus") RenewalStatus renewalStatus,@Param("planType") String planType,@Param("ssapApplicationId") long ssapApplicationId,@Param("reasonCode") long reasonCode);
	
}
