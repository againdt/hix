<%@ include file="../include.jsp" %>
<div id="appscr68" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page18.income" text="Income" />
			</h4>
		</div>
		
		<div class="control-group">
			<div class="alert alert-info">
				<p class="margin10-t">	
					<spring:message code="ssap.page18.weAskForCurrentInformation" text="We ask for current information for everyone in your family and household to make sure you get the most benefits possible." /> <spring:message code="ssap.page18.ifSpousesHaveJoint" text="If spouses have joint income, only list it once." />
				</p>
			</div>
			<div class="gutter10">
				<p>
					<em>
						<spring:message code="ssap.page18.allFieldsOnThisIncome" text="All fields on this income section are required unless otherwise indicated." />  
					</em>
				</p>
				<ul class="checklist gutter10">
					<li><spring:message code="ssap.page18.payStubs" text="Pay stubs" /></li>
					<li><spring:message code="ssap.page18.w2Forms" text="W-2 Forms" /> </li>
					<li><spring:message code="ssap.page18.informationAboutAnyOther" text="Information about any other income you get" />  </li>
				</ul>	
			</div>
		</div>
		
		
		<div class="control-group margin30-t hide">
			<p>
				<strong>
					<img alt="Time estimate" src="<c:url value="/resources/images/icons/2icon1.png" />" class="arrow_ui_design" />
					<spring:message code="ssap.page18.etimatedTimeFor" text="Estimated time for this section" />:<spring:message code="ssap.page18.5Minutes" text="5 Minutes" /> &ndash; <spring:message code="ssap.page18.10Minutes" text="10 Minutes" />
				</strong>
			</p>
		</div>
	</div>
</div>