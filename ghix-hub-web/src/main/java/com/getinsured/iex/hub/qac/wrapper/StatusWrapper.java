package com.getinsured.iex.hub.qac.wrapper;

import java.text.ParseException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_core.StatusType;
import com.getinsured.iex.hub.qac.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.iex.hub.qac.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.hub.qac.niem.niem.proxy.xsd._2.Boolean;
import com.getinsured.iex.hub.qac.niem.niem.proxy.xsd._2.Date;

public class StatusWrapper implements JSONAware {

	private boolean status;
	private String stDate;
	private String endDate;
	private static final Logger LOGGER = LoggerFactory.getLogger(StatusWrapper.class);
	
	private String getDateString(DateType dateType){
		String dateStr = null;
		
		if(dateType != null){
			Date xsdDate = (Date) dateType.getDateRepresentation().getValue();
			XMLGregorianCalendar xmlDate = xsdDate.getValue();
			try {
				dateStr = PlatformServiceUtil.xmlDateToString(xmlDate, "yyyy-MM-dd");
			} catch (ParseException e) {
				LOGGER.info("Invalid date",e);
			} catch (DatatypeConfigurationException e) {
				LOGGER.info("Invalid date",e);
			}
		}
		return dateStr;
	}
	
	public StatusWrapper(StatusType status){
		Boolean x = status.getStatusIndicator();
		this.status = x.isValue();
		DateRangeType dtRange = status.getStatusValidDateRange();
		if(dtRange != null)
		{
			DateType date = dtRange.getStartDate();
			if(date != null){
				this.stDate = getDateString(date);
			}
			
			date = dtRange.getEndDate();
			if(date != null){
				this.endDate = getDateString(date);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("StatusIndicator", this.status);
		
		if(this.stDate != null){
			obj.put("StartDate", this.stDate);
		}
		
		if(this.endDate != null){
			obj.put("EndDate", this.endDate);
		}
		return obj.toJSONString();
	}

}
