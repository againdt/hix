package com.getinsured.hix.indportal.dto.dm.application;

import java.util.ArrayList;

public class BestSEP {
	 private String status;
	 private String statusReason;
	 private String startDate;
	 private String endDate;
	 private String sepType;
	 private ArrayList < String > associatedMemberIdentifiers;
	

	private Boolean subjectToSviIndicator;


	 // Getter Methods 

	 public String getStatus() {
	  return status;
	 }

	 public String getStatusReason() {
	  return statusReason;
	 }

	 public String getStartDate() {
	  return startDate;
	 }

	 public String getEndDate() {
	  return endDate;
	 }

	 public String getSepType() {
	  return sepType;
	 }

	 public Boolean getSubjectToSviIndicator() {
	  return subjectToSviIndicator;
	 }

	 // Setter Methods 

	 public void setStatus(String status) {
	  this.status = status;
	 }

	 public void setStatusReason(String statusReason) {
	  this.statusReason = statusReason;
	 }

	 public void setStartDate(String startDate) {
	  this.startDate = startDate;
	 }

	 public void setEndDate(String endDate) {
	  this.endDate = endDate;
	 }

	 public void setSepType(String sepType) {
	  this.sepType = sepType;
	 }

	 public void setSubjectToSviIndicator(Boolean subjectToSviIndicator) {
	  this.subjectToSviIndicator = subjectToSviIndicator;
	 }
	 public ArrayList<String> getAssociatedMemberIdentifiers() {
			return associatedMemberIdentifiers;
		}

		public void setAssociatedMemberIdentifiers(ArrayList<String> associatedMemberIdentifiers) {
			this.associatedMemberIdentifiers = associatedMemberIdentifiers;
		}
	}
