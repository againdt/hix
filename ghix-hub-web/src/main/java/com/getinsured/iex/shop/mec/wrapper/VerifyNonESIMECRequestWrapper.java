package com.getinsured.iex.shop.mec.wrapper;

import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.NonESIMECRequestType;
import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.ObjectFactory;


public class VerifyNonESIMECRequestWrapper {

	private NonESIMECRequestType request = null;
	
	private NonESIMECIndividualTypeWrapper nonESIMECIndividualTypeWrapper;
	
	public VerifyNonESIMECRequestWrapper(){
		ObjectFactory objectFactory = new ObjectFactory();
		request = objectFactory.createNonESIMECRequestType();
	}
	
	public NonESIMECRequestType getSystemRepresentation()
	{
		if(nonESIMECIndividualTypeWrapper != null){
			request.setNonESIMECindividualInformation(nonESIMECIndividualTypeWrapper.getSystemRepresentation());
		}
		return request;
	}

	public void setNonESIMECIndividualTypeWrapper(NonESIMECIndividualTypeWrapper nonESIMECIndividualTypeWrapper){
		this.nonESIMECIndividualTypeWrapper = nonESIMECIndividualTypeWrapper;
	}
	
}
