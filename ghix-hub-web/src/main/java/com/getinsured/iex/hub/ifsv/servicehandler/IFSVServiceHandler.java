package com.getinsured.iex.hub.ifsv.servicehandler;

import java.util.Iterator;

import javax.xml.bind.JAXBElement;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.exchange._1.ObjectFactory;
import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.ErrorMessageSetType;
import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.IFSVResponsePayloadType;
import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.IRSResponseType;
import com.getinsured.iex.hub.ifsv.prefixmappers.IFSVNamespaceMapper;
import com.getinsured.iex.hub.ifsv.wrapper.ErrorMessageSetTypeWrapper;
import com.getinsured.iex.hub.ifsv.wrapper.IFSVApplicantTypeWrapper;
import com.getinsured.iex.hub.ifsv.wrapper.IFSVRequestPayloadTypeWrapper;
import com.getinsured.iex.hub.ifsv.wrapper.IRSResponseTypeWrapper;
import com.getinsured.iex.hub.ifsv.wrapper.PersonTypeWrapper;
import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ssap.iex.HouseholdMember;
import com.getinsured.iex.hub.platform.ssap.iex.SSAP;
import com.getinsured.iex.hub.platform.ssap.iex.SSAPTaxHouseHold;

/**
 * Service handler for IFSV Service
 * 
 * @author - Nikhil Talreja
 * @since - 27-Feb-2014
 */
public class IFSVServiceHandler extends AbstractServiceHandler {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IFSVServiceHandler.class);
	
	private static final String REQUEST_ID = "requestID";
	
	private static final int MAX_APPLICANTS = 99;
	private Jaxb2Marshaller marshaller;
	private ObjectFactory factory = new ObjectFactory();
	
	@Override
	public Object getRequestpayLoad() throws HubServiceException {
		
		/*JSONObject obj = new JSONObject();
		JSONArray personsArray = new JSONArray();
		PersonTypeWrapper primary = new PersonTypeWrapper();
		primary.setPersonGivenName("Nikhil");
		primary.setPersonMiddleName("P");
		primary.setPersonSurName("Talreja");
		primary.setPersonSSNIdentification("123456789");
		
		IFSVApplicantTypeWrapper primaryApplicant = new IFSVApplicantTypeWrapper();
		primaryApplicant.setPerson(primary);
		primaryApplicant.setTaxFilerCategoryCode("PRIMARY");
		personsArray.add(primaryApplicant);
		
		PersonTypeWrapper dependent = new PersonTypeWrapper();
		dependent.setPersonGivenName("Nikhil");
		dependent.setPersonMiddleName("middle");
		dependent.setPersonSurName("Talreja Jr.");
		dependent.setPersonSSNIdentification("223456789");
		
		IFSVApplicantTypeWrapper dependentApplicant = new IFSVApplicantTypeWrapper();
		dependentApplicant.setPerson(dependent);
		dependentApplicant.setTaxFilerCategoryCode("DEPENDENT");
		personsArray.add(dependentApplicant);
		obj.put("requestID", this.objectIdentifier);
		obj.put("applicants", this.jsonInput);*/
		
		try{
			//return getPayLoadFromJSONInput("taxHousehold", this.jsonInput);
			return getPayLoadFromJSONInput();
		}catch(HubMappingException he){
			LOGGER.error("Failed to generate the payload",he);
			throw new HubServiceException("Failed to generate the payload:",he);
		}
	}
	
	private SSAP validateInputJSON() throws HubMappingException {
		try{
			return new SSAP(getJsonInput());
		}catch(ParseException  ex){
			listJSONKeys();
			throw new HubMappingException("Failed to process input data ["+ex.getMessage()+"]", ex);
		}
	}
	
	private Object getPayLoadFromJSONInput() throws HubMappingException, HubServiceException{
		
		SSAP ssap = validateInputJSON();
		
		HouseholdMember houseHoldMember = null;
		SSAPTaxHouseHold taxHousehold = null;
		
		Iterator<SSAPTaxHouseHold> householdCursor = ssap.houseHoldIterator();
		
		IFSVRequestPayloadTypeWrapper payLoad = new IFSVRequestPayloadTypeWrapper();
		
		if(householdCursor == null){
			throw new HubServiceException("No household information provided");
		}
		
		while(householdCursor.hasNext()){
			
			taxHousehold = householdCursor.next();
			Iterator<HouseholdMember> memberCursor = taxHousehold.iterator();
			
			if(memberCursor == null){
				throw new HubServiceException("No member information provided");
			}
			
			while(memberCursor.hasNext()){
				houseHoldMember = memberCursor.next();
				PersonTypeWrapper person = new PersonTypeWrapper();
				this.extractSSAPData(houseHoldMember.getSsnInfo(), person);
				this.extractSSAPData(houseHoldMember, person);
				IFSVApplicantTypeWrapper applicant = new IFSVApplicantTypeWrapper();
				applicant.setPerson(person);
				this.extractSSAPData(houseHoldMember, applicant);
				payLoad.getIFSVApplicant().add(applicant);
			}
			
			if(payLoad.getIFSVApplicant().isEmpty() || payLoad.getIFSVApplicant().size() > MAX_APPLICANTS){
				throw new HubServiceException("Mimimum one applicant and Maximum 99 applicants expected for IFSV Service");
			}
			
		}
		
		payLoad.setRequestID(getRequestId());
		return factory.createIFSVRequest(payLoad.getRequest());
	}
	
	private String getRequestId() throws HubServiceException, HubMappingException{
		
		/*
		 * Temporary logic to get request Id 
		 * Once we have this in SSAP JSON, this code needs to be removed
		 */
		if(getJsonInput() != null){
			JSONParser parser = new JSONParser();
			try {
				JSONObject input = (JSONObject)parser.parse(getJsonInput());
				Object requestObject = input.get(REQUEST_ID);
				this.handleInputParameter(REQUEST_ID, requestObject);
				return (requestObject.toString());
			} catch (ParseException e) {
				LOGGER.error("Invalid JSON: "+ getJsonInput());
				return null;
			}
		}
		return null;
	}

	/*public Object getPayLoadFromJSONInput(String jsonSectionKey, String jsonStr) throws HubServiceException, HubMappingException{
		
		LOGGER.info("received JSON String: "+jsonStr);
		JAXBElement<IFSVRequestPayloadType> payloadElement = null;
		HubServiceException hse;
		JSONObject obj = null;
		JSONParser parser = new JSONParser();
		try {
			obj = (JSONObject)parser.parse(jsonStr);
		} catch (ParseException e) {
			LOGGER.error("Invalid JSON:"+jsonStr);
			hse = new HubServiceException("Failed to parse JSON input", e);
			throw hse;
		}
		
		IFSVRequestPayloadTypeWrapper payLoad = new IFSVRequestPayloadTypeWrapper();
		Parameter requestIdParameter = null;
		
		//Request Id validation
		if(obj.get(REQUEST_ID) != null){
			String namespace = IFSVRequestPayloadTypeWrapper.class.getName();
			requestIdParameter = this.getTargetParameter(REQUEST_ID, namespace);
			if(requestIdParameter == null){
				LOGGER.error("Invalid request Id :");
				throw new HubMappingException("Failed to find parameter mapping for input:requestID for namespace:"+namespace);
			}
			requestIdParameter.setValue(payLoad, obj.get(REQUEST_ID));
			payLoad.setRequestID(obj.get(REQUEST_ID).toString());
		}
		else{
			throw new HubServiceException("Request ID expected for IFSV Service");
		}
		
		JSONArray applicants = (JSONArray) obj.get(jsonSectionKey);
		if(applicants == null || applicants.size() == 0 || applicants.size() > MAX_APPLICANTS){
			throw new HubServiceException("Mimimum one applicant and Maximum 99 applicants expected for IFSV Service");
		}
		JSONObject ifsvApplicant = null;
		JSONArray ifsvApplicantData = null;
		
		IFSVApplicantTypeWrapper applicant;
		for(int i = 0; i < applicants.size(); i ++){
			ifsvApplicant = (JSONObject)applicants.get(i);
			ifsvApplicantData = ((JSONArray)ifsvApplicant.get("householdMember"));
			if(ifsvApplicantData.isEmpty()){
				LOGGER.error("Invalid JSON:"+jsonStr);
				throw new HubServiceException("Invalid Applicant data input, no applicant attributes found");
			}
			applicant = processApplicantData(ifsvApplicantData);
			
			payLoad.getIFSVApplicant().add(applicant);
			//payLoad.setRequestID("12345");
		}
		try{
			payloadElement = factory.createIFSVRequest(payLoad.getRequest());
		}
		catch(Exception e){
			LOGGER.error("Failed to get Payload for IFSV service",e);
		}
		return payloadElement;
	}*/

	/*@SuppressWarnings("unchecked")
	private IFSVApplicantTypeWrapper processApplicantData(JSONArray ifsvApplicantData) throws HubMappingException, HubServiceException {
		
		IFSVApplicantTypeWrapper applicant = null;

		PersonTypeWrapper person = new PersonTypeWrapper();
		Iterator<Map.Entry<String, Object>> cursor = ifsvApplicantData.iterator();

		
		JSONObject requestData = null;
		JSONObject ssnObj = null;
		Parameter target = null;
		Set<Map.Entry<String, Object>> individualRequestData = null;
		
		applicant = new IFSVApplicantTypeWrapper();
		
		while(cursor.hasNext()){
			
			requestData = (JSONObject)cursor.next();
			individualRequestData = ((JSONObject)requestData.get("name")).entrySet();
			
			if(individualRequestData.size() == 0){
				throw new HubServiceException("Invalid request data input, no applicant attributes found");
			}
			
			person = handleNameKey(individualRequestData);
			
			ssnObj = (JSONObject) requestData.get("socialSecurityCard");
			if(ssnObj != null){
				String ssnVerificationFlag = (String) ssnObj.get("socialSecurityNumber");
				target = this.getTargetParameter("socialSecurityNumber", PersonTypeWrapper.class.getName());
				if(target != null){
					target.setValue(person, ssnVerificationFlag);
				}
			}
			//Tax filer category code validation
			String taxFilerCategoryCodeObj = (String) requestData.get(IFSVApplicantTypeWrapper.TAX_FILER_CATEGORY_CODE_KEY);
			if(taxFilerCategoryCodeObj != null){
				target = this.getTargetParameter(IFSVApplicantTypeWrapper.TAX_FILER_CATEGORY_CODE_KEY, IFSVApplicantTypeWrapper.class.getName());
				if(target == null){
					LOGGER.error("Invalid Tax filer category code :");
					throw new HubMappingException("Failed to find parameter mapping for input:Tax filer category code for namespace:"+PersonTypeWrapper.class.getName());
				}
				else{
					target.setValue(applicant, taxFilerCategoryCodeObj);
				}
			}
			else{
				throw new HubServiceException("Tax filer category code is expected for every applicant");
			}
		}
		
		applicant.setPerson(person);
		return applicant;
	}
	
	private PersonTypeWrapper handleNameKey(Set<Entry<String, Object>> individualRequestData) throws HubMappingException{
		
		String namespace = PersonTypeWrapper.class.getName();
		PersonTypeWrapper person = new PersonTypeWrapper();
		
		Iterator<Map.Entry<String, Object>> cursor = individualRequestData.iterator();
		Map.Entry<String, Object> me = null;
		String paramName = null;
		Parameter applicantParameter = null;
		while(cursor.hasNext()){
			me = cursor.next();
			paramName = me.getKey();
			applicantParameter = this.getTargetParameter(paramName, namespace);
			if(applicantParameter == null){
				LOGGER.warn("No target parameter found for: "+paramName+" in namespace:"+namespace+" Ignoring parameter");
				continue;
			}
			applicantParameter.setValue(person, me.getValue());
		}
		return person;
		
	}*/
	
	@Override
	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {	
		JAXBElement<IFSVResponsePayloadType> response = (JAXBElement<IFSVResponsePayloadType>)(responseObject);
		IFSVResponsePayloadType resoponsePayload = response.getValue();
		
		/**
		 * Response from IFSV service can be of two types
		 * 1. ErrorMessageSetType
		 * 2. IRSResponseType
		 */
		JAXBElement<?> responseData = resoponsePayload.getResponse();
		Object actualResponse = responseData.getValue();
		
		LOGGER.debug("Response type " + actualResponse.getClass().getName());
		
		if(actualResponse instanceof ErrorMessageSetType){
			ErrorMessageSetTypeWrapper responseWrapper = new ErrorMessageSetTypeWrapper((ErrorMessageSetType)actualResponse);
			this.requestStatus = RequestStatus.HUB_ERROR_RESPONSE;
			return responseWrapper.toJSONString();
		}
		else if(actualResponse instanceof IRSResponseType){
			IRSResponseTypeWrapper responseWrapper = new IRSResponseTypeWrapper((IRSResponseType)actualResponse);
			this.requestStatus = RequestStatus.SUCCESS;
			return responseWrapper.toJSONString();
		}
		else{
			throw new HubServiceException("Unexpected response type received : " + actualResponse.getClass().getName() +" is unkownn to this service");
		}
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}
	
	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return "H09T";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.exchange._1",new IFSVNamespaceMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.exchange._1",new IFSVNamespaceMapper());
		}
		return this.marshaller;
	}
}
