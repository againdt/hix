import React, {Fragment, useEffect, useState} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Form from './Form';
import NamesOnDocumentInputs from './pageIncludes/common/names/NamesOnDocumentInputs';
import OtherDocumentsInputs from './pageIncludes/citizenshipImmigrationStatus/OtherDocumentsInputs';
import UniversalInput from '../../lib/UniversalInput';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';
import {changeData, clearFormSubmission, fetchCountries} from '../../actions/actionCreators';
import cloneObject from '../../utils/cloneObject';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import CONFIG from '../../.env';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import ELIGIBILITY_DOCUMENTS from '../../constants/documentsOfEligibility';
import EMPTY_OBJECTS from '../../constants/emptyObjects';
import ENDPOINTS from '../../constants/endPointsList';
import INPUT_TYPES from '../../constants/inputTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';
import {OTHER_IMMIGRATION_DOCUMENTS} from '../../constants/otherImmigrationDocuments';
import {VALIDATION_ERROR_MESSAGES} from '../../constants/validationErrorMessages';

// Components

// ActionCreators

// Utils

// Constants


/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Citizenship and Immigrant status' Page
 *
 */
const Page2_09 = props => {

    const {
        countries,
        data,
        data: {
            singleStreamlinedApplication: {
                applyingForFinancialAssistanceIndicator
            }
        },
    } = props;

    const {
        hhmIndex : member,
        currentHhm
    } = props;

    const {
        citizenshipImmigrationStatus : {
            citizenshipStatusIndicator,
            citizenshipDocument: {
                alienNumber,
                sevisId,
                foreignPassportOrDocumentNumber,
                nameOnDocument:
                    {
                        middleName,
                        lastName,
                        firstName,
                        suffix
                    },
                nameSameOnDocumentIndicator,
            },
            lawfulPresenceIndicator,
            otherImmigrationDocumentType,
            naturalizedCitizenshipIndicator,
            livedIntheUSSince1996Indicator,
            eligibleImmigrationDocumentSelected
        },
        name,
        gender
    } = currentHhm;

    const currentData = data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember[member];

    const documents = data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember[member]
        .citizenshipImmigrationStatus
        .eligibleImmigrationDocumentType[0];

    const {
        noneOfThese
    } = documents;

    const {
        form: {
            Form: {
                isFormSubmitted
            }
        }
    } = props;

    // Preparing for Local State Eligibility Documents to handle if one of them is checked or not

    const prepareDocumentList = () => {
        const documents = data
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .citizenshipImmigrationStatus
            .eligibleImmigrationDocumentType[0];

        return ELIGIBILITY_DOCUMENTS.map(doc => ({
            documentStatus: documents && documents[doc.name] || false,
            documentName: doc.name,
            documentText: doc.text,
            documentDetails: doc.details.map(d => {
                if(d.name === 'foreignPassportCountryOfIssuance') {
                    d.options = DROPDOWN_OPTIONS.COUNTRIES(countries);
                }
                if(documents.OtherDocumentTypeIndicator
                    && d.name === 'documentExpirationDate'
                ) {
                    d.isInputRequired = foreignPassportOrDocumentNumber !== null && foreignPassportOrDocumentNumber !== '';
                    d.isToggleReRegistration = foreignPassportOrDocumentNumber !== null && foreignPassportOrDocumentNumber !== '';
                }
                return d;
            })
        }));
    };

    // Local State of Functional Component

    const [ localState, setLocalState ] = useState({
        documents: prepareDocumentList(),
        citizenshipCertificate: eligibleImmigrationDocumentSelected === 'CitizenshipCertificate',
        naturalizationCertificate: eligibleImmigrationDocumentSelected === 'NaturalizationCertificate'
    });

    // Here we make API call on mounting Component in DOM-structure to get countries list

    useEffect(() => {
        if(CONFIG.ENV.PRODUCTION) {
            props.countries.length === 0 &&props.actions.fetchCountries(`${window.location.origin}${ENDPOINTS.COUNTRIES_ENDPOINT}`);
        }
    }, []);

    const cleanUpCertificateRelatedData = data => {
        return data
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .citizenshipImmigrationStatus
            .citizenshipDocument = cloneObject(EMPTY_OBJECTS.CITIZENSHIP_DOCUMENT);
    };

    useEffect(() => {
        if(localState.citizenshipCertificate) {
            const newState = {...data };
            const currentValue = newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[member]
                .citizenshipImmigrationStatus
                .eligibleImmigrationDocumentSelected;

            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[member]
                .citizenshipImmigrationStatus
                .eligibleImmigrationDocumentSelected = 'CitizenshipCertificate';
            if(currentValue !== 'CitizenshipCertificate') {
                cleanUpCertificateRelatedData(newState);
            }
            props.actions.changeData(newState);
        }
    }, [localState.citizenshipCertificate]);

    useEffect(() => {
        if(localState.naturalizationCertificate) {
            const newState = {...data };
            const currentValue = newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[member]
                .citizenshipImmigrationStatus
                .eligibleImmigrationDocumentSelected;

            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[member]
                .citizenshipImmigrationStatus
                .eligibleImmigrationDocumentSelected = 'NaturalizationCertificate';
            if(currentValue !== 'NaturalizationCertificate') {
                cleanUpCertificateRelatedData(newState);
            }
            props.actions.changeData(newState);
        }
    }, [localState.naturalizationCertificate]);

    useEffect(() => {
        !naturalizedCitizenshipIndicator && props.actions.clearFormSubmission('Form');
    }, [naturalizedCitizenshipIndicator]);

    // Updating some properties in JSON

    const onFieldChange = (...args) => {

        const [ newData, , , [ name, value ] ] = args;

        const {
            citizenshipStatusIndicator,
            honorablyDischargedOrActiveDutyMilitaryMemberIndicator
        } = newData.citizenshipImmigrationStatus;

        if(citizenshipStatusIndicator) {
            newData.citizenshipImmigrationStatus = {
                ...cloneObject(EMPTY_OBJECTS.CITIZENSHIP_STATUS),
                honorablyDischargedOrActiveDutyMilitaryMemberIndicator,
                citizenshipStatusIndicator: true
            };
        }

        if(OTHER_IMMIGRATION_DOCUMENTS.some(d => d.name === name)) {
            if(name === 'noneOfTheseOther') {
                if(value) {
                    newData.citizenshipImmigrationStatus.otherImmigrationDocumentType = {
                        ...EMPTY_OBJECTS.OTHER_IMMIGRATION_DOCUMENTS,
                        noneOfThese: value
                    };
                }
            } else {
                newData.citizenshipImmigrationStatus.otherImmigrationDocumentType = {
                    ...newData.citizenshipImmigrationStatus.otherImmigrationDocumentType,
                    noneOfThese: value ?
                        false :
                        newData
                            .citizenshipImmigrationStatus
                            .otherImmigrationDocumentType
                            .every(d => !d)
                };
            }
        }



        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member] = newData;
        props.actions.changeData(newState);
    };

    const onFieldChange_naturalized = newData => {

        const {
            honorablyDischargedOrActiveDutyMilitaryMemberIndicator
        } = newData.citizenshipImmigrationStatus;

        setLocalState(state => ({
            ...state,
            citizenshipCertificate: false,
            naturalizationCertificate: false
        }));
        if(!newData.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator) {
            newData.citizenshipImmigrationStatus = {
                ...cloneObject(EMPTY_OBJECTS.CITIZENSHIP_STATUS),
                honorablyDischargedOrActiveDutyMilitaryMemberIndicator,
                citizenshipStatusIndicator: true,
                naturalizedCitizenshipIndicator: false,
                eligibleImmigrationDocumentSelected: null
            };
        }
        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member] = newData;
        props.actions.changeData(newState);
    };

    // Updating checking certificate inputs

    const onFieldChange_certificate = (...args) => {
        const [ newData, , , [ name ] ] = args;
        if(name !== 'naturalizationCertificate' && name !== 'citizenshipCertificate') {
            const newState = {...data };
            newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member] = newData;
            props.actions.changeData(newState);
        }
        setLocalState(state => ({
            ...state,
            ...newData,
        }));
    };

    const onFieldChange_check_if = newData => {

        const {
            honorablyDischargedOrActiveDutyMilitaryMemberIndicator
        } = newData.citizenshipImmigrationStatus;

        if(!newData.citizenshipImmigrationStatus.lawfulPresenceIndicator) {
            newData.citizenshipImmigrationStatus = {
                ...cloneObject(EMPTY_OBJECTS.CITIZENSHIP_STATUS),
                honorablyDischargedOrActiveDutyMilitaryMemberIndicator,
                citizenshipStatusIndicator: false,
                naturalizedCitizenshipIndicator: false,
                lawfulPresenceIndicator: false
            };
        }
        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member] = newData;
        props.actions.changeData(newState);
    };

    // Updating status of eligibility document

    const onFieldChange_document = newData => {

        // Here comments should be added

        let docForChange = [];
        Object.entries(newData).forEach(doc => {
            const [name, value] = doc;
            const neededDoc = localState.documents.find(doc => doc.documentName === name);
            if (neededDoc) {
                if (neededDoc.documentStatus === value || (neededDoc.documentStatus === true && value === false)) {
                    newData[name] = false;
                } else {
                    docForChange = doc;
                    newData[name] = true;
                }
            }
        });

        setLocalState(state => ({
            ...state,
            documents: state.documents.map(doc => ({
                ...doc,
                documentStatus: docForChange && (doc.documentName === docForChange[0]) ? docForChange[1] : false
            }))
        }));

        const newState = {...data};
        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .citizenshipImmigrationStatus
            .citizenshipDocument = cloneObject(EMPTY_OBJECTS.CITIZENSHIP_DOCUMENT);

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .citizenshipImmigrationStatus
            .eligibleImmigrationDocumentType[0] = newData;

        props.actions.changeData(newState);
    };

    const checkTopLevelValidation = () => {
        if(naturalizedCitizenshipIndicator) {
            return localState.citizenshipCertificate || localState.naturalizationCertificate;
        } else if(lawfulPresenceIndicator) {
            return !localState.documents.every(document => document.documentStatus === false);
        } else {
            return true;
        }
    };

    return (
        <div id="fhCitizenship" className="fade-in">
            <Form topLevelValidity={checkTopLevelValidation()}>
                <Fragment>
                    <div className="subsection">
                        <UniversalInput
                            legend={{
                                legendText:
                                    <span>
                                            Is &nbsp;
                                        <strong>
                                            {normalizeNameOfPerson(name)} &nbsp;
                                        </strong>
                                        a U.S. citizen or U.S. national?
                                    </span>
                            }}
                            label={{
                                text: DROPDOWN_OPTIONS.YES_NO,
                                labelClass: ['usa-width-one-whole', 'layout--02'],
                                ulClass: ['usa-width-one-whole'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                name: 'citizenshipStatusIndicator',
                                value: citizenshipStatusIndicator,
                                options: DROPDOWN_OPTIONS.YES_NO,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: currentData,
                                fields: ['citizenshipImmigrationStatus']

                            }}
                            inputActions={{
                                onFieldChange
                            }}
                            errorClasses={['margin-l-0 margin-l-20-ve']}
                        />

                        {
                            citizenshipStatusIndicator === false &&

                                <Fragment>
                                    <div className="usa-width-one-whole margin-t-20">
                                        <UniversalInput
                                            label={{
                                                text:
                                                    <span>
                                                        Check if &nbsp;
                                                        <strong>
                                                            {normalizeNameOfPerson(name)} &nbsp;
                                                        </strong>
                                                        has eligible immigration status
                                                    </span>
                                            }}
                                            inputData={{
                                                isInputRequired: false,
                                                type: INPUT_TYPES.CHECKBOX,
                                                classes:[],
                                                name: 'lawfulPresenceIndicator',
                                                checked: lawfulPresenceIndicator || false,
                                                value: lawfulPresenceIndicator || false,
                                                currentData: currentData,
                                                fields: ['citizenshipImmigrationStatus']
                                            }}
                                            inputActions={{
                                                onFieldChange: onFieldChange_check_if
                                            }}
                                        />
                                    </div>
                                    {/*****************************************
                                     Begin - If #lawfulPresenceIndicator (Yes)
                                     ******************************************/}

                                    {
                                        lawfulPresenceIndicator &&

                                        <Fragment>
                                            <legend className="usa-width-one-whole layout--02 margin-tb-20-0 required-true">
                                                Please select a document type
                                            </legend>
                                            <ValidationErrorMessage
                                                isVisible={isFormSubmitted && !checkTopLevelValidation()}
                                                messageText='Please select a document type'
                                                errorClasses={['margin-l-0']}
                                            />
                                            <div className="usa-width-one-whole">
                                                <fieldset className="usa-fieldset-inputs usa-sans">
                                                    <ul className="usa-unstyled-list">
                                                        {
                                                            prepareDocumentList().map(statusDocument => {

                                                                return (
                                                                    <Fragment
                                                                        key={statusDocument.documentName}
                                                                    >
                                                                        <li className="margin-tb-10">
                                                                            <UniversalInput

                                                                                label={{
                                                                                    text: statusDocument.documentText,
                                                                                    labelClass: ['usa-width-one-whole', 'layout--02']
                                                                                }}
                                                                                inputData={{
                                                                                    isInputRequired: false,
                                                                                    type: INPUT_TYPES.RADIO_CHECKBOXES,
                                                                                    name: statusDocument.documentName,
                                                                                    checked: documents[statusDocument.documentName] || false,
                                                                                    value: documents[statusDocument.documentName] || false,
                                                                                    currentData: documents,
                                                                                    fields: [],
                                                                                    radioCheckbox: false
                                                                                }}
                                                                                inputActions={{
                                                                                    onFieldChange: onFieldChange_document
                                                                                }}
                                                                                indexInArray={member}
                                                                            />

                                                                            {/*****************************************
                                                                             Begin - If certain document is checked (true)
                                                                             ******************************************/}

                                                                            {
                                                                                documents[statusDocument.documentName] ?

                                                                                    <Fragment>
                                                                                        {
                                                                                            statusDocument.documentDetails.map(input => {

                                                                                                const {
                                                                                                    isInputRequired,
                                                                                                    isToggleReRegistration,
                                                                                                    name,
                                                                                                    options,
                                                                                                    placeholder,
                                                                                                    text,
                                                                                                    type,
                                                                                                    validationType,
                                                                                                    validationErrorMessage
                                                                                                } = input;

                                                                                                return (
                                                                                                    <UniversalInput
                                                                                                        key={name}
                                                                                                        label={{
                                                                                                            text: text
                                                                                                        }}
                                                                                                        inputData={{
                                                                                                            isInputRequired: isInputRequired,
                                                                                                            placeholder: placeholder,
                                                                                                            type: INPUT_TYPES[type],
                                                                                                            name: name,
                                                                                                            value: currentData.citizenshipImmigrationStatus.citizenshipDocument[name],
                                                                                                            checked: currentData.citizenshipImmigrationStatus.citizenshipDocument[name],
                                                                                                            options: options,
                                                                                                            validationType: validationType,
                                                                                                            validationErrorMessage: validationErrorMessage,
                                                                                                            currentData: currentData,
                                                                                                            fields: ['citizenshipImmigrationStatus', 'citizenshipDocument'],
                                                                                                            isToggleReRegistration: isToggleReRegistration
                                                                                                        }}
                                                                                                        inputActions={{
                                                                                                            onFieldChange
                                                                                                        }}
                                                                                                        indexInArray={member}
                                                                                                    />
                                                                                                );
                                                                                            })
                                                                                        }
                                                                                    </Fragment> : null
                                                                            }

                                                                            {/*****************************************
                                                                             End - If certain document is checked (true)
                                                                             ******************************************/}
                                                                        </li>
                                                                    </Fragment>
                                                                );
                                                            })
                                                        }
                                                    </ul>
                                                </fieldset>
                                            </div>

                                            <div className="usa-width-one-whole">
                                                <fieldset className="usa-fieldset-inputs usa-sans gi-checkbox margin-tb-20-10">
                                                    <legend>Does <strong>{normalizeNameOfPerson(name)}</strong> also have any of these documents? (Select all that apply)</legend>
                                                    <OtherDocumentsInputs
                                                        {...{
                                                            otherImmigrationDocumentType,
                                                            currentData,
                                                            inputActions: {
                                                                onFieldChange
                                                            }
                                                        }}
                                                    />
                                                </fieldset>
                                            </div>

                                            {
                                                !noneOfThese &&

                                                <UniversalInput
                                                    legend={{
                                                        legendText:
                                                            <span>
                                                                Is &nbsp;
                                                                <strong>
                                                                    { normalizeNameOfPerson(name) }  &nbsp;
                                                                </strong>
                                                                {`the same name that appears on ${gender === 'male' ? 'his' : 'her'} document?`}
                                                            </span>
                                                    }}
                                                    label={{
                                                        text: DROPDOWN_OPTIONS.YES_NO,
                                                        labelClass: ['usa-width-one-whole', 'layout--02'],
                                                        ulClass: ['usa-width-one-whole'],
                                                        showRequiredIcon: true
                                                    }}
                                                    inputData={{
                                                        isInputRequired: true,
                                                        type: INPUT_TYPES.RADIO_BUTTONS,
                                                        name: 'nameSameOnDocumentIndicator',
                                                        value: nameSameOnDocumentIndicator,
                                                        options: DROPDOWN_OPTIONS.YES_NO,
                                                        validationType: VALIDATION_TYPES.CHOSEN,
                                                        currentData: currentData,
                                                        fields: ['citizenshipImmigrationStatus', 'citizenshipDocument']
                                                    }}
                                                    errorClasses={['margin-l-20-ve margin-b-10-ve']}
                                                    inputActions={{
                                                        onFieldChange
                                                    }}
                                                />

                                            }

                                            {
                                                nameSameOnDocumentIndicator === false &&

                                                <NamesOnDocumentInputs
                                                    {...{
                                                        firstName,
                                                        middleName,
                                                        lastName,
                                                        suffix,
                                                        currentData: currentData,
                                                        inputActions: {
                                                            onFieldChange
                                                        }
                                                    }}
                                                />
                                            }

                                            {
                                                applyingForFinancialAssistanceIndicator &&
                                                <Fragment>
                                                    <UniversalInput
                                                        legend={{
                                                            legendText:
                                                                <span>
                                                                    Has &nbsp;
                                                                    <strong>
                                                                        {normalizeNameOfPerson(name)}
                                                                    </strong>
                                                                    {'\'s primary residence been in the U.S. since 1996?'}
                                                                </span>
                                                        }}
                                                        label={{
                                                            text: DROPDOWN_OPTIONS.YES_NO,
                                                            labelClass: ['usa-width-one-whole', 'layout--02', 'margin-t-20'],
                                                            ulClass: ['usa-width-one-whole'],
                                                            showRequiredIcon: true
                                                        }}
                                                        inputData={{
                                                            isInputRequired: true,
                                                            type: INPUT_TYPES.RADIO_BUTTONS,
                                                            name: 'livedIntheUSSince1996Indicator',
                                                            value: livedIntheUSSince1996Indicator,
                                                            options: DROPDOWN_OPTIONS.YES_NO,
                                                            validationType: VALIDATION_TYPES.CHOSEN,
                                                            currentData: currentData,
                                                            fields: ['citizenshipImmigrationStatus']
                                                        }}
                                                        errorClasses={['margin-l-0']}
                                                        inputActions={{
                                                            onFieldChange
                                                        }}
                                                    />
                                                </Fragment>
                                            }

                                        </Fragment>
                                    }

                                    {/*****************************************
                                     End - If #lawfulPresenceIndicator (Yes)
                                     ******************************************/}

                                </Fragment>
                        }


                        {/**********************************************
                         Begin - If #citizenshipStatusIndicator (false)
                         ***********************************************/}

                        {
                            citizenshipStatusIndicator === true &&

                            <Fragment>
                                <UniversalInput
                                    legend={{
                                        legendText:
                                            <span>
                                                Is &nbsp;
                                                <strong>
                                                    {normalizeNameOfPerson(name)} &nbsp;
                                                </strong>
                                                a naturalized citizen?
                                            </span>
                                    }}
                                    label={{
                                        text: DROPDOWN_OPTIONS.YES_NO,
                                        labelClass: ['usa-width-one-whole', 'layout--02'],
                                        ulClass: ['usa-width-one-whole'],
                                        showRequiredIcon: true
                                    }}
                                    inputData={{
                                        isInputRequired: true,
                                        type: INPUT_TYPES.RADIO_BUTTONS,
                                        name: 'naturalizedCitizenshipIndicator',
                                        value: naturalizedCitizenshipIndicator,
                                        options: DROPDOWN_OPTIONS.YES_NO,
                                        validationType: VALIDATION_TYPES.CHOSEN,
                                        currentData: currentData,
                                        fields: ['citizenshipImmigrationStatus']

                                    }}
                                    inputActions={{
                                        onFieldChange: onFieldChange_naturalized
                                    }}
                                    errorClasses={['margin-l-20-ve margin-b-10-ve']}
                                />
                                {/***************************************************
                                 Begin - If #naturalizedCitizenshipIndicator (true)
                                 ***************************************************/}

                                {

                                    naturalizedCitizenshipIndicator === true &&
                                        <Fragment>
                                            <ValidationErrorMessage
                                                isVisible={isFormSubmitted && !checkTopLevelValidation()}
                                                messageText='Please select any of these certificates'
                                                errorClasses={['margin-l-20-ve margin-b-10-ve']}
                                            />
                                            <UniversalInput
                                                label={{
                                                    text: 'Naturalization Certificate',
                                                    labelClass: ['usa-width-one-whole', 'layout--02']
                                                }}
                                                legend={{
                                                    legendText: 'Please select a document type'
                                                }}
                                                inputData={{
                                                    isInputRequired: true,
                                                    type: INPUT_TYPES.RADIO_CHECKBOXES,
                                                    classes:['disp-styl'],
                                                    name: 'naturalizationCertificate',
                                                    checked: localState.naturalizationCertificate,
                                                    value: localState.naturalizationCertificate,
                                                    currentData: {
                                                        naturalizationCertificate: localState.naturalizationCertificate,
                                                        citizenshipCertificate: false
                                                    },
                                                    fields: [],
                                                    radioCheckbox: false
                                                }}
                                                inputActions={{
                                                    onFieldChange: onFieldChange_certificate
                                                }}
                                            />

                                            {/***************************************************
                                             Begin - If #naturalizationCertificateIndicator (Yes)
                                             ***************************************************/}

                                            {
                                                localState.naturalizationCertificate && (
                                                    <Fragment>
                                                        <UniversalInput
                                                            label={{
                                                                text: 'Alien Number',
                                                                labelClasses: []
                                                            }}
                                                            inputData={{
                                                                isInputRequired: true,
                                                                type: INPUT_TYPES.REGULAR_INPUT,
                                                                helperClasses: [],
                                                                name: 'alienNumber',
                                                                value: alienNumber,
                                                                placeholder: 'Alien number',
                                                                validationType: VALIDATION_TYPES.ALIEN_NUMBER,
                                                                currentData: currentData,
                                                                fields: ['citizenshipImmigrationStatus', 'citizenshipDocument']
                                                            }}
                                                            inputActions={{
                                                                onFieldChange: onFieldChange_certificate
                                                            }}
                                                        />
                                                        <UniversalInput
                                                            label={{
                                                                text: 'Naturalization Number',
                                                                labelClasses: []
                                                            }}
                                                            inputData={{
                                                                isInputRequired: true,
                                                                type: INPUT_TYPES.REGULAR_INPUT,
                                                                name: 'sevisId',
                                                                helperClasses: [],
                                                                value: sevisId,
                                                                placeholder: 'Naturalization Number',
                                                                validationType: VALIDATION_TYPES.CERTIFICATE_NUMBER,
                                                                validationErrorMessage: VALIDATION_ERROR_MESSAGES.NATURALIZATION_NUMBER,
                                                                currentData: currentData,
                                                                fields: ['citizenshipImmigrationStatus', 'citizenshipDocument']
                                                            }}
                                                            inputActions={{
                                                                onFieldChange: onFieldChange_certificate
                                                            }}
                                                        />
                                                    </Fragment>
                                                )
                                            }

                                            {/***************************************************
                                             End - If #naturalizationCertificateIndicator (Yes)
                                             ***************************************************/}

                                            <UniversalInput
                                                label={{
                                                    text: 'Certificate of Citizenship'
                                                }}
                                                inputData={{
                                                    isInputRequired: false,
                                                    classes:['disp-styl'],
                                                    type: INPUT_TYPES.RADIO_CHECKBOXES,
                                                    name: 'citizenshipCertificate',
                                                    checked: localState.citizenshipCertificate,
                                                    value: localState.citizenshipCertificate,
                                                    currentData: {
                                                        citizenshipCertificate: localState.citizenshipCertificate,
                                                        naturalizationCertificate: false
                                                    },
                                                    fields: [],
                                                    radioCheckbox: false
                                                }}
                                                inputActions={{
                                                    onFieldChange: onFieldChange_certificate
                                                }}
                                            />

                                            {/************************************************
                                             Begin - If #citizenshipAsAttestedIndicator (Yes)
                                             *************************************************/}

                                            {
                                                localState.citizenshipCertificate && (
                                                    <Fragment>
                                                        <UniversalInput
                                                            label={{
                                                                text: 'Alien Number',
                                                                labelClasses: []
                                                            }}
                                                            inputData={{
                                                                isInputRequired: true,
                                                                type: INPUT_TYPES.REGULAR_INPUT,
                                                                name: 'alienNumber',
                                                                helperClasses: [],
                                                                value: alienNumber,
                                                                placeholder: 'Alien number',
                                                                validationType: VALIDATION_TYPES.ALIEN_NUMBER,
                                                                currentData: currentData,
                                                                fields: ['citizenshipImmigrationStatus', 'citizenshipDocument']
                                                            }}
                                                            inputActions={{
                                                                onFieldChange: onFieldChange_certificate
                                                            }}
                                                        />
                                                        <UniversalInput
                                                            label={{
                                                                text: 'Citizenship Certificate Number',
                                                                labelClasses: []
                                                            }}
                                                            inputData={{
                                                                isInputRequired: true,
                                                                type: INPUT_TYPES.REGULAR_INPUT,
                                                                name: 'sevisId',
                                                                helperClasses: [],
                                                                value: sevisId,
                                                                placeholder: 'Citizenship Certificate Number',
                                                                validationType: VALIDATION_TYPES.CERTIFICATE_NUMBER,
                                                                validationErrorMessage: VALIDATION_ERROR_MESSAGES.CITIZENSHIP_NUMBER,
                                                                currentData: currentData,
                                                                fields: ['citizenshipImmigrationStatus', 'citizenshipDocument']
                                                            }}
                                                            inputActions={{
                                                                onFieldChange: onFieldChange_certificate
                                                            }}
                                                        />
                                                    </Fragment>
                                                )
                                            }

                                            {/************************************************
                                             End - If #citizenshipAsAttestedIndicator (Yes)
                                             *************************************************/}

                                        </Fragment>
                                }
                            </Fragment>
                        }

                        {/**********************************************
                         End - If #citizenshipStatusIndicator (false)
                         ***********************************************/}

                    </div>
                </Fragment>
            </Form>
        </div>
    );
};

const mapStateToProps = state => ({
    countries: state.countries,
    data: state.data,
    form: state.form
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        clearFormSubmission,
        fetchCountries
    }, dispatch)
});

Page2_09.defaultProps = {
    name: 'Page2_09'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_09);
