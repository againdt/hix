<%@page session="true" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.getinsured.hix.util.GhixConfiguration"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>

<%
  String themeName = (String) request.getSession().getAttribute("switchToThemeName");
  if(StringUtils.isBlank(themeName)){
	  themeName="default";
  }
  themeName = DynamicPropertiesUtil.getPropertyValue(GhixConfiguration.UIConfigurationEnum.ACCENT_LESS_FILE)+"-"+themeName+".css";
// 	themeName = "theme-"+themeName+".css"; 
	

String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");

	/*
	 * START:: HIX-10725 - Program Menu Links for Inbox, account manintenance 
	 */
	 String myAccountUrl= GhixConstants.MY_ACCOUNT_URL;
	 String myInboxUrl= GhixConstants.MY_INBOX_URL;
	 String userName =  (String) request.getSession().getAttribute("USER_NAME");
	 String logoutUrl = request.getContextPath()+"/account/user/logout";

	 
	if(StringUtils.isBlank(myAccountUrl)){
		myAccountUrl="#";
	}
	
	if(StringUtils.isNotBlank(myInboxUrl) && StringUtils.isNotBlank(userName)) {
		myInboxUrl = myInboxUrl + "?languageCode=" + session.getAttribute("lang") ;
	}else{
		myInboxUrl="#";
	}
	
	if(StringUtils.isBlank(logoutUrl)){
		logoutUrl="#";
	}
	
	System.out.println("myAccountUrl ::"+ myAccountUrl);
	System.out.println("myInboxUrl ::"+ myInboxUrl);

	/*
	 * END:: HIX-10725 - Program Menu Links for Inbox, account manintenance 
	 */
	 
	
%>
<c:set var="userActiveRoleName" value="<%=userActiveRoleName%>" />
<c:set var="themeName" value="<%=themeName%>" />
<link href="<c:url value="/resources/css/${themeName}" />
" rel="stylesheet"  type="text/css" />  

<script type="text/javascript">

$(document).ready(function() {
//$('.dropdown-toggle').dropdown();
$('a.lang').click(function (){
	
	var val= $(this).attr('id');
	
	var currUrl = window.location.href;
	var newUrl="";

	///alert(val);
	/* Check if Query String parameter is set or not
	* If yes	
	*/
	if(currUrl.indexOf("?", 0) > 0)
	{
		/* Check if locale is already set without querystring param  */
		 if(currUrl.indexOf("?lang=", 0) > 0)
			  newUrl = "?lang="+val;
		/*  Check if locale is already set with querystring param  */
		 else if(currUrl.indexOf("&lang=", 0) > 0) 
			 newUrl = currUrl.substring(0, currUrl.length-2)+val; 
		 else	 
			  newUrl = currUrl + "&lang="+val;
	}/* If No  */
	else
		newUrl = currUrl + "?lang="+val;
		window.location = newUrl;
	});
});


// $(document).ready(function(){
//     $('a.not-enabled').popover({
//            placement:'bottom',
//            animation:'true',
//            delay: { show: 500, hide: 100 } 
//     });
// });                            
</script>
<sec:authorize access="isAuthenticated()"> 
  <script type="text/javascript">
  <%--  <% String str=userActiveRoleName; %>
	var s="<%=str%>"; 
	alert(s); --%>

	function countUnreadMessages() {
		
		var pathURL = "${pageContext.request.contextPath}/inbox/secureInboxUnread";
		
		$.post(pathURL, function(data) {
			
			if(data == null) {
				$(".unreadRecords").html("0");
			}
			else {
				$(".unreadRecords").html(data);
			}
		});
	}
	countUnreadMessages();
	//setInterval(countUnreadMessages, 10000);
</script> 
</sec:authorize>
<%
	//String logoutUrl = request.getContextPath()+"/account/user/logout";

	/* if session attribute is not set  */
	if(request.getParameter("lang") == null && session.getAttribute("lang") == null){
		session.setAttribute("lang","en");
	}
	else if(request.getParameter("lang") !=null && request.getParameter("lang") != session.getAttribute("lang")   ){
			session.setAttribute("lang", request.getParameter("lang"));
	}
	
	String enval=null;String esval=null;
	
	
	if(session.getAttribute("lang") !=null && session.getAttribute("lang").equals("en")  )
		enval="selected";
	else if(session.getAttribute("lang") !=null && session.getAttribute("lang").equals("es"))
		esval="selected";
	else
		enval="";
%>
<div class="navbar navbar-fixed-top navbar-inverse" id="masthead-demo" role="banner">
  <div class="navbar-inner">
    <div class="container" style="padding-top:6px;padding-bottom:8px">
      <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <ul class="nav">
        <li>
		<%-- <img class="brand" src="<c:url value="/resources/img/" /><%=GhixConfiguration.BRANDFILE%>"  alt="<%=GhixConfiguration.BRANDNAME%>"/> --%>
        <a href="${pageContext.request.contextPath}/admin/broker/manage/list" class=""><img class="brand" src="<c:url value="/resources/img/idaho-logo.jpg" />"/><span style="display: none;">Getinsured Health Exchange</span></a>
        </li>
      </ul>
      <div class="nav-collapse collapse pull-right margin0">
        <ul class="nav">

<!-- Menu for Employee, Employer, Individual, Admin, Broker, Carrier portals -->
<!-- Loged In -->
          <sec:authorize access="isAuthenticated()">
            <li><a href="#" name="skip" class="skip-nav" accesskey="n">Skip Navigation</a></li><!-- all -->
<%-- 			<c:if test="${userActiveRoleName=='employee' || userActiveRoleName=='employer' || userActiveRoleName=='individual'}"> --%>
				<!-- <li><a href="#">Language</a></li> -->			<!-- Employee, Employer, Individual -->
<%-- 			</c:if> --%>
          	<li><a href=<%=myInboxUrl%>>Inbox</a></li>		<!-- all -->
          	<li class="dropdown">
            	<a class="dropdown-toggle" data-toggle="dropdown" href="#">Get Assistance 123<b class="caret"></b></a>
            	<ul class="dropdown-menu">
<%-- 					<c:if test="${userActiveRoleName=='employee' || userActiveRoleName=='employer' || userActiveRoleName=='individual'|| userActiveRoleName=='broker'|| userActiveRoleName=='carrier' || userActiveRoleName=='issuer_representative'}">	 --%>
					  <!-- <li><a href="#">Phone Number</a></li> -->		<!-- Employee, Employer, Individual, Broker, Carrier -->
<%-- 					</c:if> --%>
					<c:if test="${userActiveRoleName=='employee' || userActiveRoleName=='employer' || userActiveRoleName=='individual'}">
					  <!-- <li><a href="#">Chat</a></li> -->				<!-- Employee, Employer, Individual -->
					  <li><a id="getAssistance" href="<c:url value='/broker/search'/>"><spring:message code="label.assistancebroker" /></a></li>	<!-- Employee, Employer, Individual -->
					</c:if>  
                  <li><a href="<c:url value='/faq'/>">Help Center</a></li>		<!-- all -->
                </ul>
            </li>
            <li class="dropdown">
            	<a class="dropdown-toggle" data-toggle="dropdown" href="#">My Account <b class="caret"></b></a>
            	<ul class="dropdown-menu">
            	<!-- Hide until Account Settings page becomes available -->
				<!-- 	<li><a href="#">Account Settings</a></li>	all --> 
					<c:if test="${userActiveRoleName=='employee' || userActiveRoleName=='employer' || userActiveRoleName=='individual'}">
						<li><a href="#">Dashboard</a></li>		<!-- Employee, Employer, Individual -->
					</c:if>
					<c:if test="${userActiveRoleName=='employee' || userActiveRoleName=='employer' || userActiveRoleName=='individual' || userActiveRoleName=='admin' || userActiveRoleName=='broker'}">
						<li><a href="<c:url value='/account/user/accountsettings'/>">Account Settings</a></li>
					</c:if>
					<li><a href=<%=logoutUrl%>>Logout</a></li><!-- all -->
                </ul>
            </li>      
          </sec:authorize>
<!-- Loged In Ends -->
          
<!-- Logged Out -->
          <sec:authorize access="! isAuthenticated()">
            <li><a href="#" name="skip" class="skip-nav" accesskey="n">Skip Navigation</a></li><!-- all -->
<%-- 			<c:if test="${userActiveRoleName=='employee' || userActiveRoleName=='employer' || userActiveRoleName=='individual'}"> --%>
          	  <!-- <li><a href="#">Language</a></li> -->				<!-- Employee, Employer, Individual -->
<%-- 			</c:if> --%>
            <li><a href="<c:url value='/account/user/login'/>">Log In</a></li><!-- all -->
            <li class="dropdown">
            	<a class="dropdown-toggle" data-toggle="dropdown" href="#">Get Assistance <b class="caret"></b></a>
            	<ul class="dropdown-menu">
<%--             	  <c:if test="${userActiveRoleName=='employee' || userActiveRoleName=='employer' || userActiveRoleName=='individual'|| userActiveRoleName=='broker'|| userActiveRoleName=='carrier'}"> --%>
                  	<!-- <li><a href="#">Phone Number</a></li> -->	 	<!-- Employee, Employer, Individual, Broker, Carrier -->
<%--                   </c:if> --%>
                  <c:if test="${userActiveRoleName=='employee' || userActiveRoleName=='employer' || userActiveRoleName=='individual'}">
                  	<!-- <li><a href="#">Chat</a></li> -->				<!-- Employee, Employer, Individual -->
                  	<li><a id="getAssistance" href="<c:url value='/broker/search'/>"><spring:message code="label.assistancebroker" /></a></li>	<!-- Employee, Employer, Individual -->
                  </c:if>
                  <li><a href="<c:url value='/faq'/>">Help Center</a></li>		<!-- all -->
                </ul>
            </li> 
          </sec:authorize>
<!-- Logged Out Ends -->
<!-- Menu for Employee, Employer, Individual, Admin, Broker, Carrier portals ENDS -->

<!-- Admin portal -->
<!-- LOGO goes to GI SHOP or PHIX homepage -->
	<!-- 
          <sec:authorize access="isAuthenticated()">
          	<li><a href=<%=myInboxUrl%>>Inbox</a></li>
          	<li class="dropdown">
            	<a class="dropdown-toggle" data-toggle="dropdown" href="#">Get Assistance <b class="caret"></b></a>
            	<ul class="dropdown-menu">
                  <li><a href="#">Help</a></li>
                </ul>
            </li>
            <li class="dropdown">
            	<a class="dropdown-toggle" data-toggle="dropdown" href="#">My Account <b class="caret"></b></a>
            	<ul class="dropdown-menu">
                  <li><a href="#">Account Settings</a></li>
                  <li> <a href=<%=logoutUrl%>>Logout</a> </li>
                </ul>
            </li>      
          </sec:authorize>
          
          <sec:authorize access="! isAuthenticated()">
            <li> <a href="<c:url value='/account/user/login'/>">Log In</a> </li>
            <li><a href="#">Sign Up</a></li>
            <li class="dropdown">
            	<a class="dropdown-toggle" data-toggle="dropdown" href="#">Get Assistance <b class="caret"></b></a>
            	<ul class="dropdown-menu">
                  <li><a href="#">Help</a></li>
                </ul>
            </li> 
          </sec:authorize>
	-->
<!-- Admin portal ENDS -->


<!-- Employee, Employer, Individual portals -->
<!-- LOGO goes to GI SHOP or PHIX homepage -->
	<!-- 
          <sec:authorize access="isAuthenticated()">
          	<li><a href="#">Language</a></li>
          	<li><a href=<%=myInboxUrl%>>Inbox</a></li>
          	<li class="dropdown">
            	<a class="dropdown-toggle" data-toggle="dropdown" href="#">Get Assistance <b class="caret"></b></a>
            	<ul class="dropdown-menu">
                  <li><a href="#">Phone Number</a></li>
                  <li><a href="#">Chat</a></li>
                  <li><a href="#">Find a Broker</a></li>
                  <li><a href="#">Help Center</a></li>
                </ul>
            </li>
            <li class="dropdown">
            	<a class="dropdown-toggle" data-toggle="dropdown" href="#">My Account <b class="caret"></b></a>
            	<ul class="dropdown-menu">
                  <li><a href="#">Account Settings</a></li>
                  <li><a href="#">Dashboard</a></li>
                  <li> <a href=<%=logoutUrl%>>Logout</a> </li>
                </ul>
            </li>      
          </sec:authorize>
          
          <sec:authorize access="! isAuthenticated()">
          	<li><a href="#">Language</a></li>
            <li><a href="<c:url value='/account/user/login'/>">Log In</a></li>
            <li><a href="#">Sign Up</a></li>
            <li class="dropdown">
            	<a class="dropdown-toggle" data-toggle="dropdown" href="#">Get Assistance <b class="caret"></b></a>
            	<ul class="dropdown-menu">
                  <li><a href="#">Phone Number</a></li>
                  <li><a href="#">Chat</a></li>
                  <li><a href="#">Find a Broker</a></li>
                  <li><a href="#">Help Center</a></li>
                </ul>
            </li> 
          </sec:authorize>
	-->
<!-- Employee, Employer, Individual portals ENDS -->
         
        </ul>
      </div>
      <!-- nav-collapse ends --> 
    </div>
    <!-- container ends -->  
  </div>
  <!-- navbar-inner ends --> 
</div>
<!-- navbar  ends -->