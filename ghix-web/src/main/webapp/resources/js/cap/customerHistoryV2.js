$(document).ready(function() {
    $('#myModal').modal();
   
}); 
var Log=function(str)
{
	try{
		console.log(str);
	}catch(e)
	{
		
	}
};

/*sampleData.sort(function(a,b){
    if(a.time>b.time){
        return 1;
    }else{
        return -1;
    }
});*/

(function(angular,log){
    var app = angular.module('customerHistoryApp', []);

    app.directive("onlyNumber", function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput != inputValue) {
                    try {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    } catch (e) { }
                };
                return transformedInput;
            });
        }
        };
    });

    app.controller('mainController', ['$scope', '$http', function($scope, $http) {
   
    function randomDate(start, end) {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    }

    $scope.isLoading=true;

    //For role based show / hide columns in Manage Member - 08 About This Member - History Tab (default)
   
    $scope.memberAbtLogcall = logCommentsVar == true ? true: false;
    $scope.memberAbtShowcomments = showCommentsVar == true ? true: false;
    
    $scope.eventTypeList=[];
    $scope.eventCategoryList=[
        {
            text:"All",
            value:"All",
            eventsList:{}
        }
    ];

    $scope.roleList=[
        {
            text:"All",
            value:"All",
        }
    ];

    for(key in roleList)
    {
        $scope.roleList.push({
            value:key,
            text:roleList[key]
        });
    };
    $scope.role=$scope.roleList[0];


    $scope.eventTypeList=[{
            text:"All",
            value:"All"
        }];
        $scope.eventType=$scope.eventTypeList[0];

    for(key in Categories)
    {
        $scope.eventCategoryList.push({
            text:key,
            value:key,
            eventsList:Categories[key]
        });
    };

    $scope.eventCategory=$scope.eventCategoryList[0];

    $scope.categoryChange=function()
    {
    	$scope.eventType.value="All";
    	$scope.refreshResult();
        $scope.eventTypeList=[{
            text:"All",
            value:"All"
        }];
//        var subConst=$scope.eventCategory.eventsList;
        var subConst=$scope.filteredEventTypes;
        if(angular.isArray(subConst))
        {
            angular.forEach(subConst, function(nConst){
                $scope.eventTypeList.push({
                    text:nConst,
                    value:nConst
                });
            });
        };

        $scope.eventType=$scope.eventTypeList[0];
        
    };

    //$scope.categoryChange();

    $scope.filteredEvents=[];

    var defPlotOptions={
            title: { text: null },
            legend:{
                enabled:false
            },
            chart: {
                
            },
            xAxis: {
                type: "datetime",
                gridLineWidth:1,
                title: { text: null,enable:false },
                showEmpty: true,
                showFirstLabel:true,
                showLastLabel:true,
                labels:{
                    enable:false,
                    formatter:function(){ 
                        var dt=moment(this.value);
                        return dt.format(this.dateTimeLabelFormat);
                    }
                },
                dateTimeLabelFormats:{
                    millisecond: 'hh:mm:ss a',
                    second: 'hh:mm:ss a',
                    minute: 'hh:mm:ss a',
                    hour: 'hh:mm a',
                    day: 'Do MMM',
                    week: 'Do MMM',
                    month: 'MMM DD YYYY',
                    year: 'YYYY'
                }
            },
            yAxis: {
                title: { text: null,enable:false },
                labels: {enabled:false}
            },
            series: [{
                name: "",
                type: 'scatter',
                allowPointSelect:true,
                color: Highcharts.getOptions().colors[1],
                data: []
            }],
            tooltip: {
                formatter: function (e) {
                    //log(this.point);
                    var dt = new Date(this.point.x);
                    displayBox(this.point.histObj);
                    return "<b>" + this.point.histObj.name + "</b><br>" + dt.toString();

                }
            },
            credits: {
                enabled: false
            }
    };


    
    var timeVar=null;

    var displayBox=function(histObj)
    {

        $scope.filteredEvents.forEach(function(obj){
                obj.open=false;
        });
        histObj.open=true;
        $scope.$digest();
        var idx=$scope.filteredEvents.indexOf(histObj);

        var doScroll=function()
        {
            clearTimeout(timeVar);
            var curPos=$(".boxcontainer").scrollTop();
            var targetPos=idx*57;
            var treshHold=Math.round(Math.abs((targetPos-curPos))/3);
            if(treshHold < 5)
            {
                treshHold=5;
            };
            if( (curPos >= (targetPos-treshHold)) && (curPos <= (targetPos+treshHold)) )
            {
                $(".boxcontainer").scrollTop(targetPos);
            }else if(targetPos > curPos){
                $(".boxcontainer").scrollTop(curPos+treshHold);    
            }else{
                $(".boxcontainer").scrollTop(curPos-treshHold);
            };
            if(targetPos != curPos)
            {
                timeVar=setTimeout(function(){
                    doScroll();
                },20);
            };
        };

        doScroll();
    };

    let filterCategory = function(){
    	$scope.filteredCategory = [];
    	angular.forEach($scope.eventCategoryList,function(cat,index){
    		if(index==0){
    			$scope.filteredCategory.push(cat);
    		}else if($scope.types.has(cat.text)){
    			$scope.filteredCategory.push(cat);
    		}
    	});
    }
    
    var applyFilters=function()
    {

        if(angular.isArray(rawData)){

        }else{
            data=[];
        };

        $scope.filteredEvents=[];
        $scope.filteredEventTypes=[];
        $scope.types=new Set();

        angular.forEach(rawData,function(nObj){
        	$scope.types.add(nObj.type);
            var kObj=angular.copy(nObj);
            kObj.eventObjects=angular.fromJson(kObj.eventJson);
            kObj.creationTimeStamp=(new Date(kObj.creationTimeStamp)).getTime();

            var flag=true;
            if($scope.eventCategory.value != "All")
            {
                if(kObj.type != $scope.eventCategory.value){flag=false;}
            }else{
            	$scope.eventType.value = "All";
            };
            if($scope.eventType.value != "All")
            {
                if(kObj.name != $scope.eventType.value){flag=false;}
            };
            if($scope.role.value != "All")
            {
                if(kObj.createdByUserRole != $scope.role.text){flag=false;}
            };
            if(dateRanges.start !="")
            {
                var startDate=moment(dateRanges.start,"MM/DD/YYYY").format("x");
                var endDate=moment(dateRanges.end,"MM/DD/YYYY").add(1,'day').format("x");
                var thisDate=moment(kObj.creationTimeStamp).format("x");

                if(thisDate >= startDate && thisDate < endDate)
                {
                    
                }else{
                    flag=false;
                }
            };

            if(flag)
            {
                $scope.filteredEvents.push(kObj);
                if($scope.eventCategory.value != "All"){
                	if(!$scope.filteredEventTypes.includes(kObj.name)){
       	    			$scope.filteredEventTypes.push(kObj.name);
       	    		}
                }
            };
        });

        //$scope.filteredEvents=data;        

       
        var plotData = [];
        angular.forEach($scope.filteredEvents, function (nObj) {
            plotData.push({
                x: nObj.creationTimeStamp,
                y: 1,
                name: nObj.name,
                histObj:nObj
            });
        });

        var chartJson={}

        angular.copy(defPlotOptions,chartJson);
        chartJson.series[0].data=plotData;
        //chartJson.xAxis.min=(new Date(dateRanges.start)).getTime(),
        //chartJson.xAxis.max=(new Date(dateRanges.end)).getTime();
        try{
            $('#chartContainer').highcharts().destroy();
        }catch(e){}
        $('#chartContainer').highcharts(chartJson);

        try{
            $scope.$digest()
        }catch(e){}
    };

    $scope.isKeyAllowed=function(key)
    {
        return true;
    };

    $scope.refreshResult=function()
    {
        applyFilters();
    };

    var dateRanges={
        start:"",
        end:""
    };

    var onDateRangeChange=function(start, end,label)
    {
        if(label=="All")
            {
                dateRanges.start="";
                dateRanges.end="";
                $("#dateRange .datedisplay").html("All");
            }else{
                dateRanges.start=start.format('MM/DD/YYYY')
                dateRanges.end=end.format('MM/DD/YYYY');
                $("#dateRange .datedisplay").html(dateRanges.start + " - "+dateRanges.end);
            }
        
        $scope.refreshResult();
    };

    $scope.getRequest = function () {
        $("#dateRange").daterangepicker(dateRangePickerOptions,onDateRangeChange);
        $("#dateRange .datedisplay").html("All");

        $(".form_datetime").datetimepicker({
            format: "mm/dd/yyyy hh:ii",
            autoclose:true,
            initialDate:new Date()
        });

        pullData();
    };


    $scope.toggleBoxDetail=function(eventObj)
    {
        $scope.filteredEvents.forEach(function(obj){
            if(obj != eventObj)
            {
                obj.open=false;
            }
        });
        if(eventObj.open)
        {
            eventObj.open=false;
        }else{
            eventObj.open=true;
        }
    };

    var rawData=null;

    var pullData=function()
    {
        var dataToServer={
            moduleId:moduleId,
        };

        dataToServer[csrfToken.paramName]=csrfToken.paramValue;
        $http({
            method:"POST",
            data: $.param(dataToServer),
            url:historyServiceUrl,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function(data){
            $scope.isLoading=false;
            rawData=data;
            applyFilters();
            filterCategory();
        }).error(function(){

        });
    };

    $scope.resetPage=function()
    {
         $scope.role=$scope.roleList[0];
          $scope.eventTypeList=[{
            text:"All",
            value:"All"
        }];
        $scope.eventType=$scope.eventTypeList[0];
        $scope.eventCategory=$scope.eventCategoryList[0];
        $("#dateRange .datedisplay").html("All");
        dateRanges={
            start:"",
            end:""
        };
        pullData();
    };

    $scope.runValidate=false;

    $scope.onLogACallClick=function()
    {
        $scope.runValidate=false;
        $("#modalLogACall").modal({
            backdrop:"static"
        });
        var tDate=new Date();
        var dateStr=tDate.GetDateString();
        $scope.newCallLogDateTime= dateStr + " " + String(tDate.getHours()).ConvertTo2Digit() +":"+String(tDate.getMinutes()).ConvertTo2Digit();
        $scope.newCallLogDuration="";
        $scope.newCallLogComment="";
        $scope.newCallLogNextStep="";
        allowSend=true;
    };

    $scope.fetchComments =function(eventObj){
        eventObj.isLoading=true;
        var dataToServerForComment={
            encAppEventId:eventObj.id 
        };
        dataToServerForComment[csrfToken.paramName]=csrfToken.paramValue;
        $http({
            method: 'POST',
            data: $.param(dataToServerForComment),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            url: commentUrl
            //headers : { 'Content-Type' : 'application/json' }
        }).success(function(data) {
            eventObj.isLoading=false;

            if(data === null || data === "undefined" || data.length === 0){
                eventObj.isLoading=false;
                eventObj.noComments=true;
                eventObj.hideText=false;
                return;
            }
    		eventObj.commentsList=data;
             eventObj.showComments=true;
             eventObj.hideText=false;
        });
           
           
            /*var commentList= $("#commentList_"+eventObj.id);
            commentList.show();*/
    };

    var allowSend=true;
    $scope.onSubmitCallLog=function()
    {
        $scope.runValidate=true;
        if($scope.frmNewCallProxy.$valid && allowSend)
        {
            allowSend=false;
            var dataToServer={
                callLog:angular.toJson({
                    timeStamp:$scope.newCallLogDateTime,
                    duration:$scope.newCallLogDuration,
                    notes:$scope.newCallLogComment,
                    nextSteps:$scope.newCallLogNextStep,
                    consumerId:houseHoldId
                })               
            };
            dataToServer[csrfToken.paramName]=csrfToken.paramValue;

            $http({
                method:"post",
                url:callLog,
                transformResponse:[function(data){
                    return data;
                }],
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data:$.param(dataToServer)
            }).success(function(data){
                $("#modalLogACall").modal("hide");
                $scope.resetPage();
            });
        };
    };


}]);
app.filter("fieldname",function(){
        return function(key)
        {
            var sepKeys="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var retKey="";
            for(var i=0;i<key.length;i++)
            {
                var keyChar=String(key).substr(i,1);
                if(sepKeys.indexOf(keyChar) >=0)
                {
                    retKey +=" "+keyChar;
                }else{
                    if(i==0)
                    {
                        retKey +=String(keyChar).toUpperCase();
                    }else{
                        retKey += keyChar;
                    }
                }
            };
            return retKey;
        }
});
app.filter("camelcase",function(){
        return function(key)
        {
            var retKey=String(key).toLowerCase();

            return retKey;
        }
});
})(angular,Log);
if (typeof Array.prototype.getDataObj !== 'function') {
    Array.prototype.getDataObj = function (propName, propVal) {
        var retObj = null;
        this.forEach(function (obj) {
            if (String(obj[propName]).toLowerCase() == String(propVal).toLowerCase()) {
                retObj = obj;
            };
        });
        return retObj;
    };
};
if (typeof String.prototype.ConvertTo2Digit !== 'function') {
    String.prototype.ConvertTo2Digit = function () {
        var str = this;
        if ((str == null || str == "" || str == undefined) && str != 0) {
            return str;
        };
        if (String(str).length == 1) {
            return String("0" + str);
        } else {
            return str;
        };
    };
};
if (typeof Date.prototype.GetDateString != 'function') {
    Date.prototype.GetDateString = function () {
        return String(this.getMonth() + 1).ConvertTo2Digit() + "/" + String(this.getDate()).ConvertTo2Digit() + "/" + this.getFullYear();
    };
};
if (typeof Date.prototype.CurrentTimeString != 'function') {
    Date.prototype.CurrentTimeString = function () {
        var currDate = new Date();
        return String(currDate.getHours()).ConvertTo2Digit() + ":" + String(currDate.getMinutes()).ConvertTo2Digit() + ":" + String(currDate.getSeconds()).ConvertTo2Digit();
    };
};



