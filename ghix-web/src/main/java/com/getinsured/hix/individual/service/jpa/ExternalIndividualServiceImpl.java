package com.getinsured.hix.individual.service.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.individual.repository.IExternalIndividualRepository;
import com.getinsured.hix.individual.service.ExternalIndividualService;
import com.getinsured.hix.model.ExternalIndividual;

/**
 * Implements {@link ExternalIndividualService} to find and save external
 * individual
 */
@Service("externalIndividualService")
@Transactional
public class ExternalIndividualServiceImpl implements ExternalIndividualService {

	@Autowired
	private IExternalIndividualRepository externalIndividualRepository;

	/**
	 * @see ExternalIndividualService#findByExternalIndividualID(long)
	 */
	@Override
	public ExternalIndividual findByExternalIndividualID(long externalEmpId) {
		return externalIndividualRepository.findByIndividualCaseId(externalEmpId);
				
	}

}