package com.getinsured.ahbx.platform.mapper;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.getinsured.ahbx.platform.address.Request;
import com.getinsured.ahbx.platform.address.Response;
import com.getinsured.ahbx.platform.address.Response.Record;
import com.getinsured.hix.model.Location;

/**
 * Address Mapper class to transform
 * 		1. GHIX-Request to AHBX Request
 * 		2. AHBX Response to GHIX-Response
 *
 * @author Ekram Ali Kazi
 *
 */
public final class AddressMapper {

	private static final Logger LOGGER = Logger.getLogger(AddressMapper.class);

	private AddressMapper(){}
	/**
	 * Map GHIX-Request to AHBX Request
	 *
	 * @param incomingAddress
	 * @return AHBX Request object
	 */
	public static Request map(Location incomingAddress){
		Request ahbxRequest = new Request();
		//Request.Record record = new Request.Record();
		ahbxRequest.setAddressLine1(incomingAddress.getAddress1());
		ahbxRequest.setAddressLine2(incomingAddress.getAddress2());
		ahbxRequest.setCity(incomingAddress.getCity());
		ahbxRequest.setState(incomingAddress.getState());
		ahbxRequest.setZIPCode(incomingAddress.getZip());

		//List<Request.Record> recordList = new ArrayList<Request.Record>();
		//recordList.add(record);
		//ahbxRequest.getRecord().add(record);
		return ahbxRequest;
	}

	/**
	 * Map AHBX Response to GHIX-Response
	 *
	 * @param ahbxResponse
	 * @return List of Location object
	 */
	@SuppressWarnings("unchecked")
	public static List<Location> map(Response ahbxResponse){

		if (ahbxResponse != null){
			List<Location> validAddressList = new LinkedList<Location>();

			List<Record> ahbxResponseRecord = ahbxResponse.getRecord();

			// Sort desc based on Match_Score field to get maximum match addresses at top
			Collections.sort(ahbxResponseRecord, new Comparator<Record>() {
				@Override
				public int compare(Record r1, Record r2) {
					return -1 * r1.getMatchScore().compareTo(r2.getMatchScore());
				}
			});

			for (Record responseRecord : ahbxResponseRecord) {

				if (	StringUtils.isEmpty(responseRecord.getFullAddress()) ||
						StringUtils.isEmpty(responseRecord.getCity()) ||
						StringUtils.isEmpty(responseRecord.getState()) ||
						StringUtils.isEmpty(responseRecord.getZIPCode())
						){
					// ignore record...
					LOGGER.warn("Ignoring record from AHBX-OEDQ WS. Logging empty record object - " + responseRecord.toString());
					continue;
				}

				String[] address_items = responseRecord.getFullAddress().split("\\|");

				// no Address Line 1 found..... ignore record....
				if (address_items.length == 1){
					LOGGER.warn("Ignoring record from AHBX-OEDQ WS. Logging empty Address Line 1 - " + responseRecord.toString());
					continue;
				}

				Location address = new Location();

				if (address_items.length >=3) {
					// we have line 1 and line 2
					address.setAddress1(address_items[0]);
					address.setAddress2(address_items[1]);
				}else if (address_items.length >=2) {
					// we have only line 1
					address.setAddress1(address_items[0]);
					address.setAddress2(null);
				}

				address.setCity(responseRecord.getCity());
				address.setState(responseRecord.getState());
				address.setZip((responseRecord.getZIPCode()));
				if(responseRecord.getLatitude() != null) {
					address.setLat(responseRecord.getLatitude().doubleValue());
				}
				if(responseRecord.getLongitude() != null) {
					address.setLon(responseRecord.getLongitude().doubleValue());
				}
				address.setCounty(responseRecord.getCounty());
				address.setRdi("");

				validAddressList.add(address);
			}

			return (List<Location>) (validAddressList.isEmpty() ? Collections.emptyList() : validAddressList);
		}

		return Collections.emptyList();

	}

}
