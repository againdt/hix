set projPath=E:\VIMO\NewWorkspace
set jbossPath=E:\JBoss\jboss-eap-6.0

set dirPath=%projPath%\ghix\ghix-entity
set targetPath=%projPath%\ghix\ghix-entity\target
set deployPath=%jbossPath%\standalone\deployments

cd /D %dirPath%
call mvn -DskipTests clean install

copy %targetPath%\ghix-entity.war %deployPath%

pause