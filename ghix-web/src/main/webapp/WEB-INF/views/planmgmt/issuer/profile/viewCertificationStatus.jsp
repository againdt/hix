<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
<div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="<c:url value="/planmgmt/issuer/account/profile/financial/info/list"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
        	<li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="pgheader.certificationStatus"/></li>
        </ul><!--page-breadcrumb ends-->
        <%-- <h1><issuerLogo:view/> ${issuerObj.name}</h1> --%>
        <h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
   
</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.issuerAbout"/></h4>
                </div>
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
 	 	 	 		<jsp:param name="pageName" value="certStatus"/>
 	 	 	 	</jsp:include>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
             	<form class="form-horizontal" id="frmIssuerCertStatus" name="frmIssuerCertStatus" action="#" method="POST">
              		<div class="header margin5-b">
						<h4 class="pull-left">Certification Status</h4>	
					</div>
					<display:table name="statusHistory" pagesize="${pageSize}" list="statusHistory" requestURI="" sort="list" class="table table-condensed table-border-none table-striped"  defaultsort="1" defaultorder="descending" >
				           <display:column property="lastUpdateTimestamp" titleKey="label.date" format="{0,date,MMM dd, yyyy}" sortable="true" class="txt-center" headerClass="graydrkbg txt-center" />
				           <display:column property="certificationStatus" titleKey="label.certificationStatus" sortable="true" class="txt-center"  headerClass="graydrkbg txt-center"/>
				           <display:column property="certificationDoc" titleKey="label.file" sortable="false" class="txt-center"  headerClass="graydrkbg txt-center"/>
				          <%--  <display:column property="lastUpdatedBy" media="none" titleKey="label.userName" sortable="true" headerClass="graydrkbg"/>
				           <display:column property="commentId" media="none" titleKey="label.comment" sortable="false" headerClass="graydrkbg"/> --%>
				           
				           <display:setProperty name="paging.banner.placement" value="bottom" />
				           <display:setProperty name="paging.banner.some_items_found" value=''/>
				           <display:setProperty name="paging.banner.all_items_found" value=''/>
				           <display:setProperty name="paging.banner.group_size" value='50'/>
				           <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
				           <display:setProperty name="paging.banner.page.selected" value='<a class="active"><strong>{0}</strong></a>'/>
				           <display:setProperty name="paging.banner.onepage" value=''/>
				           <display:setProperty name="paging.banner.one_item_found" value=''/>
				           <display:setProperty name="paging.banner.first" value='<span class="pagelinks">
				           <div class="pagination center">
							<ul>
								<li></li>
								<li>{0}</li>
								<li><a href="{3}">Next &gt;</a></li>
							</ul>
							</div>
							</span>'/>
						<display:setProperty name="paging.banner.last" value='<span class="pagelinks">
							<div class="pagination center">
								<ul>
									<li><a href="{2}">&lt; Prev</a></li>
									<li>{0}</li>
									<li></li>
								</ul>
							</div>
							</span>'/>
						<display:setProperty name="paging.banner.full" value='
							<div class="pagination center">
								<ul>
									<li><a href="{2}">&lt; Prev</a></li>
									<li>{0}</li>
									<li><a href="{3}">Next &gt;</a></li>
								</ul>
							</div>
							'/>
					</display:table>
                                      
				</form>
			
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->

 </div>
  <div id="modalBox" class="modal hide fade">
	<div class="modal-header">
		<a class="close" data-dismiss="modal" data-original-title="">x</a>
		<h3 id="myModalLabel"><spring:message code="label.viewComment"/></h3>
	</div>
	<div id="commentDet" class="modal-body">
		<p></p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message code="label.btnClose"/></a>
	</div>
</div>
<script type="text/javascript">
	$('.ttclass').tooltip();
	
	function getComment(commentId){
		$('#modal-body').html("<p> <spring:message code='label.loadingComment'/></p>");
		$('#frmIssuerCertStatus').ajaxSubmit({
			url: "<c:url value='/admin/issuer/getComment'/>",
			data: { "commentId": commentId},
			success: function(data){
					if(data != ""){
						if($.browser.msie==true && $.browser.version=='8.0'){
						  	$('#modal-body').html("<p> "+ data + "</p>");
						  	$('#commentDet').html("<p> "+ data + "</p>");
						}else{
							$('#modal-body').html("<p> "+ data + "</p>");
							$('#commentDet').html("<p> "+ data + "</p>");
						}
		
					}
				},error: function(responseText){  /* For FF*/
					if(responseText != ""){
						$('#modal-body').html("<p> "+ data + "</p>");
						$('#commentDet').html("<p> "+ data + "</p>");
		
					}
				}
		});
	}	
	
	function showdetail(documentUrl) {
		 var rv = -1;
		 if (navigator.appName == 'Microsoft Internet Explorer'){
		    var ua = navigator.userAgent;
		    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
		    if (re.exec(ua) != null){
		       rv = parseFloat( RegExp.$1 );
		    }
		 }
		 if(rv <= 8.0 && navigator.appName == 'Microsoft Internet Explorer'){
			 $('<div class="modal popup-address" id="addressIFrame"><div class="modal-header padding0" style="border-bottom:0; "><div class="graydrkaction"><h4 class="margin0 pull-left"><spring:message  code='label.upgradeBrowser' javaScriptEscape='true'/></h4><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 0px;"><spring:message  code='label.upgradeBrowserMessage' javaScriptEscape='true'/></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true">OK</button></div></div>').modal({backdrop:"static",keyboard:"false"});
		 } else {
			 window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
		 }   
	 }
</script>
