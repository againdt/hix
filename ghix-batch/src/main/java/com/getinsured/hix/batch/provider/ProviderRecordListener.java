package com.getinsured.hix.batch.provider;

import org.apache.log4j.Logger;
import org.springframework.batch.core.annotation.OnSkipInProcess;
import org.springframework.batch.core.annotation.OnSkipInRead;
import org.springframework.batch.core.annotation.OnSkipInWrite;

public class ProviderRecordListener {

	private static Logger logger = Logger.getLogger(ProviderRecordListener.class);
	
	public ProviderRecordListener(){
		/*String logFile = GhixConstants.ENCLARITY_DATA_DIR+File.separatorChar+"provider_error_log.txt";
		SimpleLayout layout = new SimpleLayout();    
	    FileAppender appender = null;
		try {
			appender = new FileAppender(layout,logFile,false);
			appender = new FileAppender(layout,logFile,false,true,10240);
									
			//appender.setBufferedIO(true);
			//appender.setBufferSize(10240);
			logger = Logger.getLogger(ProviderRecordListener.class);
		//	logge
			logger.addAppender(appender);
			logger.setLevel((Level) Level.ERROR);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 	*/
	}
	
	@OnSkipInRead
	public void logValidationError(Throwable t){
		ProviderValidationException pe = null;
		if(t instanceof ProviderValidationException){
			pe = (ProviderValidationException)t;
		}
		if(pe != null){
			logger.error(pe.getMessage());
		}
	}
	
	@OnSkipInWrite
	public void logWriteError(Object obj, Throwable t){
		if(t != null){
			logger.error("[RECORD WRITE]"+t.getMessage());
		}
	}
	
	@OnSkipInProcess
	public void logWriteProcess(Object obj, Throwable t){
		if(t != null){
			logger.error("[RECORD WRITE]"+t.getMessage());
		}
	}
}
