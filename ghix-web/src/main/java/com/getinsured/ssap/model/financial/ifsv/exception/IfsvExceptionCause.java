package com.getinsured.ssap.model.financial.ifsv.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Describes exception cause for {@link IfsvException}
 *
 * @author Yevgen Golubenko
 * @since 2019-06-12
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IfsvExceptionCause {
  @JsonProperty("Exception Type")
  private String exceptionType;

  @JsonProperty("ExceptionMessage")
  private String exceptionMessage;

  @JsonProperty("ExceptionCause")
  private IfsvExceptionCauseDetails exceptionCauseDetails;

  public String getExceptionType() {
    return exceptionType;
  }

  public void setExceptionType(String exceptionType) {
    this.exceptionType = exceptionType;
  }

  public String getExceptionMessage() {
    return exceptionMessage;
  }

  public void setExceptionMessage(String exceptionMessage) {
    this.exceptionMessage = exceptionMessage;
  }

  public IfsvExceptionCauseDetails getExceptionCauseDetails() {
    return exceptionCauseDetails;
  }

  public void setExceptionCauseDetails(IfsvExceptionCauseDetails exceptionCauseDetails) {
    this.exceptionCauseDetails = exceptionCauseDetails;
  }

  @Override
  public String toString() {
    return "IfsvExceptionCause{" +
        "exceptionType='" + exceptionType + '\'' +
        ", exceptionMessage='" + exceptionMessage + '\'' +
        ", exceptionCauseDetails=" + exceptionCauseDetails +
        '}';
  }
}
