(function() {
    'use strict';

    angular
        .module('reconciliationWorkbenchApp')
        .directive('d3PieChart', d3PieChart);

    function d3PieChart() {

            return {
                restrict: 'E',
                // scope: {
                //     val: '=',
                //     grouped: '=',
                //     // data: '='
                // },
                scope : '=',
                link: function (scope, element, attrs) {

                    scope.data = [];
                    scope.data = scope.enrollmentDetails;


                    var width = 175,
                        height = 225,
                        radius = Math.min(width, height) / 2;

                    var outerRadius = width / 2.5;
                    var innerRadius = width / 6;

                    // var color = d3.scale.ordinal()
                    //     .range(["#7fc97f", "#beaed4", "#fdc086", "#ffff99" ]);

                    var color = d3.scaleOrdinal(d3.schemeSet2);

                    var arc = d3.svg.arc()
                        .innerRadius(innerRadius)
                        .outerRadius(outerRadius);

                    var labelArc = d3.svg.arc()
                        .outerRadius(radius - 40)
                        .innerRadius(radius - 40);

                    var pie = d3.layout.pie()
                        .sort(null)
                        .value(function(d) { return d.percentage; });


                    var svg = d3.select(element[0]).append("svg")
                        .attr("width", width)
                        .attr("height", height)
                        .append("g")
                        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");


                    var div = d3.select(element[0]).append("div")
                        .attr("class", "tooltip chart-tooltip")
                        .style("opacity", 0);


                    var g = svg.selectAll(".arc")
                        .data(pie(scope.data))
                        .enter().append("g")
                        .attr("class", "arc");

                    g.append("path")
                        .attr("d", arc)
                        .style("fill", function(d, i ) { return d3.schemeSet2[i]; });

                    g.on("mouseover", function(d, i) {
                        div.transition()
                            .duration(200)
                            .style("opacity", .9);

                        div.html("<h4>Enrollments</h4>" +
                            "<div><div class=color-desc style=background-color:" +  d.data.color + "></div>&nbsp;&nbsp;&nbsp;&nbsp;"  + d.data.field + ":" + d.data.value + "</div>")
                            .style("left", (d3.event.pageX - 34) + "px")
                            .style("top", function(){
                                if(d3.event.pageY +70 > 500){
                                    return d3.event.pageY -200+"px";
                                }else{
                                    return d3.event.pageY+100+"px"
                                }
                            });
                    })
                        .on("mouseout", function(d) {
                            div.transition()
                                .duration(500)
                                .style("opacity", 0);
                        });


                    g.append("text")
                        .attr("transform", function(d) { return "translate(" + labelArc.centroid(d) + ")"; })
                        .attr("dy", ".35em")
                        .style("fill", "white")
                        .style("font-size", "12px")
                        .attr("font-family", "Arial")
                        .attr("stroke", "dark grey")
                        .text(function(d) { return d.data.percentage > 0 ? d.data.percentage + "%" : ""; });

                }
            }
        }
})();