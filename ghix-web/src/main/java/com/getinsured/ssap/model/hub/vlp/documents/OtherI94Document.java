package com.getinsured.ssap.model.hub.vlp.documents;

import java.util.Date;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * Other I-94 Document.
 *
 * @author Yevgen Golubenko
 * @since 2/15/19
 */
public class OtherI94Document extends DhsDocument
{
  public OtherI94Document()
  {
    super.setDocumentType(DocumentType.OTHER_I94);
  }

  @JsonProperty("I94Number")
  private String i94Number;

  @JsonProperty("PassportCountry")
  private String passportCountry;

  @JsonProperty("PassportNumber")
  private String passportNumber;

  @JsonProperty("CountryOfIssuance")
  private String countryOfIssuance;

  @JsonProperty("SEVISID")
  private String sevisId;

  @JsonProperty("DocDescReq")
  private String docDescReq;

  @JsonFormat(pattern = DATE_FORMAT)
  @JsonProperty("DocExpirationDate")
  private Date docExpirationDate;

  @Override
  public boolean isValid()
  {
    if(!validPassportInformation(passportCountry, passportNumber,  countryOfIssuance)) {
      log.error("When passportCountry supplied: {}, passport number: {} and county of issuance: {} are required",
          this.passportCountry, this.passportNumber, this.countryOfIssuance);

      return false;
    }

    if(!notBlank(this.i94Number))
    {
      log.error("id94Number: {} required", this.i94Number);
      return false;
    }

    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @SuppressWarnings("Duplicates")
  public DhsDocument from(final CitizenshipDocument citizenshipDocument)
  {
    final OtherI94Document document = new OtherI94Document();

    document.setI94Number(validValue(citizenshipDocument.getI94Number()));
    document.setCountryOfIssuance(validValue(citizenshipDocument.getForeignPassportCountryOfIssuance()));
    document.setPassportCountry(validValue(citizenshipDocument.getForeignPassportCountryOfIssuance()));
    document.setPassportNumber(validValue(citizenshipDocument.getForeignPassportOrDocumentNumber()));
    document.setSevisId(getSevisId(citizenshipDocument));
    document.setDocDescReq(validValue(citizenshipDocument.getDocumentDescription()));
    document.setDocExpirationDate(citizenshipDocument.getDocumentExpirationDate());

    return document;
  }

  public String getI94Number()
  {
    return i94Number;
  }

  public void setI94Number(final String i94Number)
  {
    this.i94Number = i94Number;
  }

  public String getPassportCountry()
  {
    return passportCountry;
  }

  public void setPassportCountry(final String passportCountry)
  {
    this.passportCountry = passportCountry;
  }

  public String getPassportNumber()
  {
    return passportNumber;
  }

  public void setPassportNumber(final String passportNumber)
  {
    this.passportNumber = passportNumber;
  }

  public String getCountryOfIssuance()
  {
    return countryOfIssuance;
  }

  public void setCountryOfIssuance(final String countryOfIssuance)
  {
    this.countryOfIssuance = countryOfIssuance;
  }

  public String getSevisId()
  {
    return sevisId;
  }

  public void setSevisId(final String sevisId)
  {
    this.sevisId = sevisId;
  }

  public String getDocDescReq()
  {
    return docDescReq;
  }

  public void setDocDescReq(final String docDescReq)
  {
    this.docDescReq = docDescReq;
  }

  public Date getDocExpirationDate()
  {
    return docExpirationDate;
  }

  public void setDocExpirationDate(final Date docExpirationDate)
  {
    this.docExpirationDate = docExpirationDate;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", OtherI94Document.class.getSimpleName() + "[", "]")
        .add("i94Number='" + i94Number + "'")
        .add("passportCountry='" + passportCountry + "'")
        .add("passportNumber='" + passportNumber + "'")
        .add("countryOfIssuance='" + countryOfIssuance + "'")
        .add("sevisId='" + sevisId + "'")
        .add("docDescReq='" + docDescReq + "'")
        .add("docExpirationDate=" + docExpirationDate)
        .toString();
  }
}
