<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
<c:set var="encPlanId" ><encryptor:enc value="${plan.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
			<ul class="page-breadcrumb">
                <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
                <li><a href="#"><spring:message code="label.plans"/></a></li>
                <li><a href="<c:url value="/admin/planmgmt/manageqhpplans"/>"><spring:message code="label.manageQHPS"/></a></li>
                <li><a href="<c:url value="/admin/planmgmt/viewqhpdetail/${encPlanId}" />"><spring:message code="label.linkPlanDetails"/></a></li>
                <li><spring:message code="label.planRate"/></li>
            </ul><!--page-breadcrumb ends-->
			<h1><a name="skip"></a><issuerLogo:view issuerName="${issuerName}" />${planName}</h1>
	</div>
	
	<div class="row-fluid">
        <div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message code="label.aboutPlan"/></h4>
       		</div>
       		<!--  beginning of side bar -->
	            <ul class="nav nav-list graybg">
     				<li><a href="<c:url value="/admin/planmgmt/viewqhpdetail/${encPlanId}" />"><spring:message  code='label.linkPlanDetails'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpbenefits/${encPlanId}"/>"><spring:message  code='label.linkPlanBenefits'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhprates/${encPlanId}" />"><spring:message  code='label.linkPlanRates'/></a></li>
                    <c:if test="${displayProviderNetwork == 'YES'}">
                    	<li><a href="<c:url value="/admin/planmgmt/viewqhpprovidernetwork/${encPlanId}" />"><spring:message  code='label.linkProviderNetwork'/></a></li>
					</c:if>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpenrollmentavail/${encPlanId}"/>"><spring:message  code='label.linkEnrollmentAvail'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpcertification/${encPlanId}" />"><spring:message  code='label.linkCertificationStatus'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhphistory/${encPlanId}"/>"><spring:message  code='label.linkPlanHistory'/></a></li>
			   </ul>
				<br /><br />
				<c:if test="${displayAction == 'YES'}">
					<h4 class="margin0"><spring:message code='label.action'/></h4>
					<ul class="nav nav-list graybg">
						<li><a href="<c:url value="/admin/planmgmt/editqhpdetail/${encPlanId}" />"><i class="icon icon-pencil"></i> <spring:message  code='label.linkEditPlanDetails'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpbenefits/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanBenefits'/></a></li>
						<li class="active"><a href="<c:url value="/admin/planmgmt/editqhprates/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanRates'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpprovidernetwork/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditProviderNetwork'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpenrollmentavail/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditEnrollmentAvail'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpcertification/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditCertificationStatus'/></a></li>
					</ul>
				</c:if>
	
        </div><!-- end of span3 -->
        <div class="span9" id="rightpanel">
				<div class="header">
					<h4 class="pull-left"><spring:message code='label.QHPRates'/></h4>
					<a class="btn btn-small" title="<spring:message  code="label.btnCancel"/>" href="#" onclick="Javascript:location.href='<c:url value="/admin/planmgmt/viewqhprates//${encPlanId}" />'"><spring:message code='label.btnCancel'/></a> <a class="btn btn-primary btn-small" href="#" title="<spring:message  code="label.btnSave"/>" onclick="formSubmit()"><spring:message  code='label.btnSave'/></a>
				</div>
                        <table class="table table-border-none">
                            <tbody>
                                <tr>
	                                <td class="span3 txt-right"><spring:message  code='label.issuerName'/></td>
	                                <td><strong>${issuerName}</strong></td>
                                </tr>
                                <tr>
	                                <td class="txt-right"><spring:message  code='label.planName'/></td>
	                                <td><strong>${planName}</strong></td>
                                </tr>
                                <tr>
	                                <td class="txt-right"><spring:message  code='label.planNumber'/></td>
	                                <td><strong>${planNumber}</strong></td>
                                </tr>
                            </tbody>
                         </table>
                         <hr>
                         <form class="form-horizontal form-bottom gutter10" id="frmRates" name="frmRates" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
	                   	<df:csrfToken/>
	                   	<div class="control-group">
							<label for="rates" class="control-label required"><spring:message  code='label.linkPlanRates'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<!-- <div class="controls">
								<input type="file" class="input-file" id="fileInput">
								<div id="issuerName_error"></div>
							</div> -->	
							<div class="controls">
										<span><input type="file" id="rates" name="rates"  class="input-file">
										&nbsp; <input type="button" value="Upload" title="Upload" class="btn" onclick="validCsv('#rates','#frmRates','#rateFile_error')"/>&nbsp; <a href=<c:url value="/resources/sampleTemplates/plan_rates_by_zip.csv" />><spring:message  code='label.downloadTemplate'/></a>
										<input type="hidden" name="fileType" value="rates"/>
	                          			</span>
										<div id="rateFile_error" class="">${fileError}</div>
							<div id="ratesFileName" class="help">${ratesFileName}</div>
						</div><!-- end of control-group --> 
						</div></form>
						<form class="form-horizontal gutter10" id="frmUploadRates" name="frmUploadRates" action="<c:url value="/admin/planmgmt/editqhprates" />" method="POST">
	                   	<df:csrfToken/>
	                   	<div class="control-group">
							<label for="effectiveDate" class="control-label required"><spring:message code='label.effectiveDate'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" class="input-small datepick" id="effectiveDate" name="effectiveDate" />
								<div id="effectiveDate_error"></div>
							</div>	
						</div><!-- end of control-group --> 
						
	                   	<div class="control-group">
							<label for="comments" class="control-label required"><spring:message code='label.changeJustification'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> <a class="ttclass" rel="tooltip" href="#" data-original-title="Text Holder 1"><i class="icon-question-sign"></i></a></label>
							<div class="controls">								
								<textarea class="input-xlarge" name="comments" id="comments" rows="4" cols="40" style="resize: none;" 
								maxlength="500" spellcheck="true" onkeyup="updateCharCount();" onchange="updateCharCount();"></textarea>	                       
	                       		<div id="chars_left"><spring:message  code="label.charLeft"/> <b><spring:message code='label.charLeftValue'/></b></div>
								<div id="issuerName_error"></div>
							</div>	
						</div><!-- end of control-group --> 
                        
						<input type="hidden" id="rateFile" name="rateFile"/>
						<%-- <input type="hidden" id="id" name="id" value="${planId}"/>	 --%>	
						<input type="hidden" id="id" name="id" value="<encryptor:enc value="${planId}"></encryptor:enc>"/>				
                    </form>
             </div>   <!-- end of span9 -->
                       
             
        </div>
   		<!--  end of row-fluid -->
</div>
<script type="text/javascript">
var commentMaxLen = 500;

$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	})
	$('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/calendar.gif';
		$(this).datepicker({
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true,
			minDate: 0
		});
	});
})

$('.ttclass').tooltip();

//Update remaining characters for comments
function updateCharCount(){	
	var currentLen = $.trim(document.getElementById("comments").value).length;
	var charLeft = commentMaxLen - currentLen;	
	$('#chars_left').html('Characters left <b>' + charLeft + '</b>' );
}

$.validator.addMethod("effDate", function(value, element) {
	var startdatevalue = '${currDate}';
	if (value == null || value.length <1)
		return true;
	return Date.parse(startdatevalue) <= Date.parse(value);
	});

function validCsv(fileElementId, formElementId, errorElementId)
{
	var ext = $(fileElementId).val().split('.').pop().toLowerCase();
	var allow = new Array('csv');
	if(jQuery.inArray(ext, allow) == -1) {
	    $(errorElementId).html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileTypeError'/></span></label>");
	   return false;
	}else{
		$(formElementId).submit();
	  } 
}

function  formSubmit()
{
	$("#rateFile_error").html("");
	$("#frmUploadRates").submit();
}
$(document).ready(function(){
	var accreditationUpload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitAccreditationRequest,  // pre-submit callback 
	        success:       submitAccreditationResponse  // post-submit callback 
	    };
    $('#frmRates').ajaxForm(accreditationUpload);
    isFileReq();
});

function submitAccreditationResponse(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	if(val[0] != 'Error')
	{	
		$("#ratesFileName").text(val[1]);
		$("#rateFile").val(val[2]);
	}
	else
	{
		$("#rateFile_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
	}
}

function submitAccreditationRequest(formData, jqForm, options) {
	var fileName = $('#rates').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}

function isFileReq()
{
	//if("false" == '${isEdit}')
	{
	$("#rateFile").rules("add", "required");
	}	
}

var validator = $("#frmUploadRates").validate({ 
	 ignore: "",
	rules : {
		rateFile : { required : true},
		effectiveDate : { required : true,
			effDate : true}
	}, 
	messages : {
		rateFile : { required : "<span> <em class='excl'>!</em><spring:message  code='err.uploadRatesFile'/></span>"},
		effectiveDate : { required : "<span> <em class='excl'>!</em><spring:message  code='err.greaterThanToday'/></span>",
			effDate : "<span> <em class='excl'>!</em><spring:message  code='err.greaterThanToday'/></span>"},
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	} 
});
jQuery("#frmRates").validate({});
</script>
