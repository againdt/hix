package com.getinsured.iex.hub.qac.wrapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_ee.ObjectFactory;
import com.getinsured.iex.hub.qac.niem.niem.niem_core._2.DateRangeType;

public class InsuranceApplicantRequestWrapper implements JSONAware {
	private PersonWrapper person;
	private String coverageStartDate;
	private String coverageEndDate;
	private InsuranceApplicantType applicantType = null;
	private DateRangeType dateRangeType = null;
	private static com.getinsured.iex.hub.qac.niem.niem.niem_core._2.ObjectFactory niemCoreFactory = null;
	private static com.getinsured.iex.hub.qac.niem.niem.proxy.xsd._2.ObjectFactory proxyFactory;
	
	public InsuranceApplicantRequestWrapper(){
		ObjectFactory extnFactory = new ObjectFactory();
		this.applicantType = extnFactory.createInsuranceApplicantType();
		niemCoreFactory = new com.getinsured.iex.hub.qac.niem.niem.niem_core._2.ObjectFactory();
		proxyFactory = new com.getinsured.iex.hub.qac.niem.niem.proxy.xsd._2.ObjectFactory();
		dateRangeType = niemCoreFactory.createDateRangeType();
		
		applicantType.setInsuranceApplicantRequestedCoverage(dateRangeType);

	}
	
	
	
	public void setPerson(PersonWrapper person){
		if(person != null){
			this.applicantType.setRolePlayedByPerson(person.getSystemRepresentation());
		}
	}
	
	public void setCoverageStartDate(String coverageStartDate) throws HubServiceException {
		if(coverageStartDate == null || coverageStartDate.isEmpty()){
			return;
		}
		
		this.coverageStartDate = coverageStartDate;

		com.getinsured.iex.hub.qac.niem.niem.niem_core._2.DateType startDateType = niemCoreFactory.createDateType();

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		sdf.setLenient(false);
		try {
			java.util.Date javaDate = sdf.parse(coverageStartDate);

			com.getinsured.iex.hub.qac.niem.niem.proxy.xsd._2.Date date = proxyFactory.createDate();
			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(javaDate);
		
		
			date.setValue(DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gc.get(Calendar.YEAR),gc.get(Calendar.MONTH) + 1,
							gc.get(Calendar.DAY_OF_MONTH),
							DatatypeConstants.FIELD_UNDEFINED));
			JAXBElement<com.getinsured.iex.hub.qac.niem.niem.proxy.xsd._2.Date> startDateJAXBElement = niemCoreFactory.createDate(date);
			startDateType.setDateRepresentation(startDateJAXBElement);
			
			dateRangeType.setStartDate(startDateType);
		} catch (DatatypeConfigurationException | ParseException e) {
			throw new HubServiceException(e.getMessage(), e);
		} 
	}
	
	public String getCoverageStartDate( ){
		return coverageStartDate;
	}

	public void setCoverageEndDate(String coverageEndDate) throws HubServiceException{
		if(coverageEndDate == null || coverageEndDate.isEmpty()){
			return;
		}
		this.coverageEndDate = coverageEndDate;
	
		try{
			com.getinsured.iex.hub.qac.niem.niem.niem_core._2.DateType endDateType = niemCoreFactory.createDateType();
		

			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			sdf.setLenient(false);
			java.util.Date javaDate = sdf.parse(coverageEndDate);
	
			com.getinsured.iex.hub.qac.niem.niem.proxy.xsd._2.Date date = proxyFactory.createDate();
			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(javaDate);
			
			date.setValue(DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gc.get(Calendar.YEAR),gc.get(Calendar.MONTH) + 1,
							gc.get(Calendar.DAY_OF_MONTH),
							DatatypeConstants.FIELD_UNDEFINED));
	
			JAXBElement<com.getinsured.iex.hub.qac.niem.niem.proxy.xsd._2.Date> endDateJAXBElement = niemCoreFactory.createDate(date);
			endDateType.setDateRepresentation(endDateJAXBElement);
			dateRangeType.setEndDate(endDateType);
		}
		catch(DatatypeConfigurationException|ParseException e){
			throw new HubServiceException(e.getMessage(), e);
		}
	}
	
	public String getCoverageEndDate(){
		return coverageEndDate;
	}
	
	
	public InsuranceApplicantType getSystemRepresentation()
	{
		return applicantType;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("RolePlayedByPerson", this.person);
		
		JSONObject coverage = new JSONObject();
		if(this.coverageStartDate != null){
			coverage.put("StartDate", this.coverageStartDate);
		}
		if(this.coverageEndDate != null){
			coverage.put("EndDate", this.coverageEndDate);
		}
		obj.put("InsuranceApplicantRequestedCoverage", coverage);
		return obj.toJSONString();
		
	}

}
