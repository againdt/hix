import states from '../constants/statesList';

export const getStateName = stateCode => {
    return (state => state ? state.stateName : '')(states.find(s => s.stateCode === stateCode));
};