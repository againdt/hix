package com.getinsured.iex.client;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.getinsured.eligibility.model.SepEvents.ChangeType;
import com.getinsured.eligibility.model.SepEvents.Financial;
import com.getinsured.iex.dto.SepEventsResource;
import com.getinsured.iex.dto.SsapApplicant;
import com.getinsured.iex.dto.SsapApplicantEvent;
import com.getinsured.iex.dto.SsapApplicantEventResource;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.dto.SsapApplication;
import com.getinsured.iex.dto.SsapApplicationEvent;
import com.getinsured.iex.dto.SsapApplicationEventResource;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.dto.SsapVerificationResource;

/**
 * @author karnam_s
 * 
 */
public interface ResourceCreator {

	/**
	 * @param id
	 * @return
	 */
	public SsapApplicationResource getSsapApplicationById(long id);

	public String getSsapApplicationApplicantById(long id);

	public SsapApplicationResource getSsapApplicationByCmrHouseoldId(
			long mmrHouseoldId) throws JsonParseException,
			JsonMappingException, IOException;

	/**
	 * @param id
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	public String saveSsapApplication(SsapApplication ssapApplication)
			throws JsonGenerationException, JsonMappingException, IOException;

	public String updateSsapApplication(SsapApplication ssapApplication,
			long ssapID) throws JsonGenerationException, JsonMappingException,
			IOException;

	public Map<Long, SsapApplicantResource> getApplicants(long applicationId)
			throws Exception;
	
	public Map<String, SsapApplicantResource>  getApplicantsWithGuidAsKey(long applicationId)
			throws Exception;

	public void updateSsapApplicant(SsapApplicantResource ssapApplicantResource)
			throws Exception;

	public String createSsapApplicant(SsapApplicantResource ssapApplicant,
			String ssapApplicationLink) throws Exception;

	SsapApplicationResource getSsapApplicationByMaxIdAndCmrHouseoldId(
			long cmrHouseoldId) throws JsonParseException,
			JsonMappingException, IOException;

	// public SsapApplicationEvent saveSsapapplicationEvent(SsapApplicationEvent
	// ssapApplicationEvent) throws JsonGenerationException,
	// JsonMappingException, IOException;

	public SsapApplicantEvent saveSsapapplicantEvent(
			SsapApplicantEvent ssapApplicantEvent)
			throws JsonGenerationException, JsonMappingException, IOException;

	// public SsapApplicationEvent findSsapApplicationEventByApplicationId(long
	// id) throws JsonParseException, JsonMappingException, IOException;

	public List<SsapApplicantEvent> findSsapApplicantEventsByApplicationEvent(
			SsapApplicationEvent ssapAplicationEvent)
			throws JsonParseException, JsonMappingException, IOException;

	List<SsapApplicant> getSsapApplicantsByApplicationId(long applicationId)
			throws JsonParseException, JsonMappingException, IOException;

	String saveLCESsapApplicant(SsapApplicantResource ssapApplicant)
			throws Exception;

	SsapApplicationResource findByCmrHouseoldIdAndApplicationStatusAndMaxId(
			long cmrHouseoldId, String status) throws JsonParseException,
			JsonMappingException, IOException;

	Map<Long, SsapApplicantResource> getApplicantsByApplicationId(
			long applicationId) throws Exception;

	String updateSsapApplicationEvent(
			SsapApplicationEventResource ssapApplicationEventResource,
			long ssapApplicationEventId) throws JsonGenerationException,
			JsonMappingException, IOException, Exception;

	String createSsapApplicationEvent(
			SsapApplicationEventResource ssapApplicationEventResource,
			String ssapApplicationEventLink) throws JsonGenerationException,
			JsonMappingException, IOException;

	Map<Long, SsapApplicantResource> getApplicantEvents(long applicationEventId)
			throws Exception;

	String createSsapApplicantEvent(
			SsapApplicantEventResource SsapApplicantEventResource)
			throws JsonGenerationException, JsonMappingException, IOException;

	SsapApplicationEventResource findSsapApplicationEventByApplicationId(
			long applicationId, int lookupValueId) throws JsonParseException,
			JsonMappingException, IOException;

	SsapApplicationEvent saveSsapapplicationEvent(
			SsapApplicationEvent ssapApplicationEvent)
			throws JsonGenerationException, JsonMappingException, IOException,
			RestClientException;

	void updateSsapApplicant(long applicantId) throws Exception;

	SsapApplicantResource getSsapApplicantByApplicationAndPersonId(
			long applicationId, long personId) throws Exception;

	SsapApplicantResource getSsapApplicantById(long id);

	SsapVerificationResource getSsapVerificationById(long id);

	String updateSsapVerification(
			SsapVerificationResource ssapVerificationResource) throws Exception;

	String createSsapVerification(SsapVerificationResource ssapVerification,
			String ssapApplicantLink) throws Exception;

	String updateSsapApplication(SsapApplicationResource ssapApplicationResource)
			throws Exception;
	
	SsapApplicationEventResource findSsapApplicationEventBySsapApplicationId(long applicationId) throws JsonParseException, JsonMappingException, IOException;

	Map<Long, SsapApplicantResource> getActiveApplicants(long applicationId)
			throws Exception;

	SsapApplicantEventResource findSsapApplicantEventByApplicantId(
			long applicantId);

	SsapApplicantEventResource findByApplicationEventIdAndPersonIdAndSepEventId(
			long applicationEventId, long personId, int sepEventId)
			throws Exception;

	void updateSsapApplicantEvent(
			SsapApplicantEventResource ssapApplicantEventResource,
			long ssapApplicantEventId) throws Exception;

	SepEventsResource findByEventNameCategoryAndFinancial(String name,
			String category, Financial financial) throws JsonParseException,
			JsonMappingException, IOException;

	String findSepEventByFinancial(Financial financial)
			throws JsonParseException, JsonMappingException, IOException;

	void deleteRecordById(String tableName, long id);

	List<SepEventsResource> findEventsByApplicationId(long applicationId)
			throws Exception;

	SsapApplicantEventResource findMinAndMaxEnrollmentStartDateByApplicationEventId(
			long applicationEventId, String type) throws Exception;

	SsapApplicationResource updateSsapApplicationById(long id,
			SsapApplicationResource ssapApplicationResource) throws Exception;

	List<SsapApplicantResource> findApplicantsByApplicationEventAndSepEventIds(
			long applicationEventId, Set<Integer> sepEventsIds)
			throws Exception;

	SsapApplicationEventResource getSsapApplicationEventById(long id);

	Map<Long, SsapApplicantResource> getAllApplicants(long applicationId)
			throws Exception;

	SsapApplicationEventResource updateSsapApplicationEventById(long id,
			SsapApplicationEventResource ssapApplicationEventResource)
			throws Exception;

	String saveSsapApplication(SsapApplicationResource ssapApplicationResource)
			throws JsonGenerationException, JsonMappingException, IOException;
	
	long countOfApplicationsByStatus(BigDecimal cmrHouseoldId,String applicationType);
	
	public String findByEventChangeTypeAndFinancial(String changeType, Financial financial, String displayOnUI) 
			throws JsonParseException, JsonMappingException, IOException;
}
