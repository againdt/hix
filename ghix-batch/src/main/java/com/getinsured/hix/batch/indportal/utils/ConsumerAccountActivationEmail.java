package com.getinsured.hix.batch.indportal.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.notify.EmailService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.notify.NotificationAgent;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;


@Component
public class ConsumerAccountActivationEmail extends NotificationAgent{

	@Value("#{configProp.exchangename}")
	private String exchangeName;


	@Override
	public Map<String, String> getTokens(Map<String, Object> notificationContext) {
		Map<String,String> brokerData = new HashMap<String, String>();
		ActivationJson activationJsonObj = (ActivationJson)notificationContext.get("ACTIVATION_JSON");
		AccountActivation accountActivation = (AccountActivation)notificationContext.get("ACCOUNT_ACTIVATION");
		String activationUrl = GhixEndPoints.GHIXWEB_SERVICE_URL + "account/user/activation/" + accountActivation.getActivationToken();
		
		brokerData = activationJsonObj.getCreatedObject().getCustomeFields();
		brokerData.put("exchangename", exchangeName );
		brokerData.put("activationUrl", activationUrl );
		return brokerData;
	}
	
	@Override
	public Map<String, String> getEmailData(Map<String, Object> notificationContext) {
		ActivationJson activationJsonObj = (ActivationJson)notificationContext.get("ACTIVATION_JSON");
		Map<String, String> data = new HashMap<String, String>();
		data.put("To", activationJsonObj.getCreatedObject().getEmailId());
		String exchangeName = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
		data.put("Subject", "New Account Activity at "+exchangeName);
		data.put("KeyId", String.valueOf(activationJsonObj.getCreatedObject().getObjectId()));
		data.put("KeyName", activationJsonObj.getCreatedObject().getRoleName());
		
		return data;
	}
	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setNoticeTypeRepo(NoticeTypeRepository noticeTypeRepo) {
		this.noticeTypeRepo = noticeTypeRepo;
	}

	@Autowired
	public void setNoticeRepo(NoticeRepository noticeRepo) {
		this.noticeRepo = noticeRepo;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	@Autowired
	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	@Autowired
	public void setGhixDBSequenceUtil(GhixDBSequenceUtil ghixDBSequenceUtil) {
		this.ghixDBSequenceUtil = ghixDBSequenceUtil;
	}
	
	@Autowired
	public void setNoticeTemplateFactory(NoticeTemplateFactory templateFactory) {
		this.templateFactory = templateFactory;
	}
}