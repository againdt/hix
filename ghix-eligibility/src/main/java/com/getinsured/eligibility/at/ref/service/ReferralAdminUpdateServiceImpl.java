package com.getinsured.eligibility.at.ref.service;

/**
 * @author chopra_s
 * 
 */
import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.LceDemoCompareApplicantStatusLogic;
import com.getinsured.eligibility.at.ref.dozzer.assembler.SsapAssembler;
import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.HouseholdContact;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.Name;
import com.getinsured.iex.ssap.OtherAddress;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

@Component("referralAdminUpdateService")
@Scope("singleton")
public class ReferralAdminUpdateServiceImpl implements ReferralAdminUpdateService {
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Autowired
	private SsapApplicationRepository sssapApplicationRepository;

	@Autowired
	@Qualifier("iLocationRepository")
	private ILocationRepository iLocationRepository;

	@Autowired
	@Qualifier("ssapAssembler")
	private SsapAssembler ssapAssembler;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	private static final Logger LOGGER = Logger.getLogger(ReferralAdminUpdateServiceImpl.class);

	@Override
	public void executeNonFinancialAdminUpdate(CompareMainDTO compareMainDTO) {
		LOGGER.info("executeNonFinancialAdminUpdate Starts ");
		processAdminUpdate(compareMainDTO, false);
		LOGGER.info("executeNonFinancialAdminUpdate Ends ");
	}

	@Override
	public void executeAdminUpdate(CompareMainDTO compareMainDTO) {
		LOGGER.info("executeAdminUpdate Starts ");
		processAdminUpdate(compareMainDTO, true);
		LOGGER.info("executeAdminUpdate Ends ");
	}

	private void processAdminUpdate(CompareMainDTO compareMainDTO, boolean updateAppStatus) {

		modifyEnrolledApplicants(compareMainDTO);

		final SingleStreamlinedApplication ssappln = ssapJsonBuilder.transformFromJson(compareMainDTO.getEnrolledApplication().getApplicationData());
		
		for (CompareApplicantDTO applicantDTO : compareMainDTO.getEnrolledApplication().getApplicants()) {
			if (applicantDTO.isAdminUpdate()) {
				updateChangedDemographicAndAddress(ssappln, applicantDTO);
			}
			if(applicantDTO.isSubscriber() && applicantDTO.isMailAddressChanged()){
				updateMailingAddressForAllMembers(ssappln, applicantDTO);
			}
		}

		populateLivesAtOtherAddress(getPrimaryHouseHoldMember(ssappln), ssappln);

		final String ssapJson = ssapAssembler.transformSsapToJson(ssappln);
		updateSSAPJSON(compareMainDTO.getEnrolledApplication().getId(), ssapJson);

		if (updateAppStatus) {
			modifyCurrentApplicants(compareMainDTO);
		}
		
		// TODO: update hh's location id
	}

	private void modifyCurrentApplicants(CompareMainDTO compareMainDTO) {
		final List<CompareApplicantDTO> currentList = compareMainDTO.getCurrentApplication().getApplicants();
		for (CompareApplicantDTO currentApplicant : currentList) {
			if (currentApplicant.isAdminUpdate()) {
				persistSSAPApplicantStatus(currentApplicant);
			}
		}
	}

	private void persistSSAPApplicantStatus(CompareApplicantDTO applicantDTO) {
		SsapApplicant ssapapplicant = ssapApplicantRepository.findSsapApplicantById(applicantDTO.getId());
		ssapapplicant.setStatus(LceDemoCompareApplicantStatusLogic.demoChangeApplicantStatus(applicantDTO).value());
		ssapApplicantRepository.save(ssapapplicant);
	}

	private void modifyEnrolledApplicants(CompareMainDTO compareMainDTO) {
		SsapApplicant ssapApplicant = null;
		final List<CompareApplicantDTO> enrolledList = compareMainDTO.getEnrolledApplication().getApplicants();
		for (CompareApplicantDTO enrolledApplicant : enrolledList) {
			if (enrolledApplicant.isAdminUpdate()) {
				ssapApplicant = persistSSAPApplicant(enrolledApplicant);
				if (enrolledApplicant.getPersonId() == ReferralConstants.PRIMARY
				        && (enrolledApplicant.isPrimaryAddressChanged() || enrolledApplicant.isAddressLine1Changed() || enrolledApplicant.isAddressLine2Changed() || enrolledApplicant.isCityChanged() || enrolledApplicant.isMailAddressChanged())
				              || enrolledApplicant.isPartialAddressNotUpdated()) {
					updateAddressForSsapApplicant(ssapApplicant, enrolledApplicant);
				}
			}
		}
	}

	private void updateChangedDemographicAndAddress(SingleStreamlinedApplication ssappln, CompareApplicantDTO applicantDTO) {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		
		for (HouseholdMember houseHoldMember : ssappln.getTaxHousehold().get(0).getHouseholdMember()) {
			if (houseHoldMember.getPersonId() == applicantDTO.getPersonId()) {
				final Name name = houseHoldMember.getName();
				name.setFirstName(applicantDTO.getFirstName());
				name.setMiddleName(applicantDTO.getMiddleName());
				name.setLastName(applicantDTO.getLastName());
				name.setSuffix(applicantDTO.getNameSuffix());
				houseHoldMember.setGender(applicantDTO.getGender());
				houseHoldMember.setExternalId(applicantDTO.getExternalApplicantId());
				
				if ("CA".equalsIgnoreCase(stateCode)) {
					if(applicantDTO.getMarried() != null) {
						houseHoldMember.setMarriedIndicatorCode(applicantDTO.getMarried());
					}else {
						houseHoldMember.setMarriedIndicatorCode("R");
					}
				}
				else
				{
					if ("Yes".equalsIgnoreCase(applicantDTO.getMarried()) || "true".equalsIgnoreCase(applicantDTO.getMarried())) {
						houseHoldMember.setMarriedIndicator(true);
					}else if("No".equalsIgnoreCase(applicantDTO.getMarried()) || "false".equalsIgnoreCase(applicantDTO.getMarried())) {
						houseHoldMember.setMarriedIndicator(false);
					}
				}
				
				houseHoldMember.getSocialSecurityCard().setSocialSecurityNumber(applicantDTO.getSsn());
				if (applicantDTO.isRaceEthnicityChanged()) {
					houseHoldMember.setEthnicityAndRace(applicantDTO.getEthnicityAndRace());
				}
				houseHoldMember.setApplicantPersonType(applicantDTO.getPersonType());	
				
				final HouseholdContact householdContact = houseHoldMember.getHouseholdContact();
				householdContact.getContactPreferences().setEmailAddress(applicantDTO.getEmailAddress());
				if (applicantDTO.isPhoneChanged()) {
					householdContact.setPhone(applicantDTO.getPhone());
					householdContact.setOtherPhone(applicantDTO.getOtherPhone());
				}
				if (householdContact.getContactPreferences() != null && applicantDTO.isAdminUpdate()) {
					if(applicantDTO.isSpokenLanguageChanged())
						householdContact.getContactPreferences().setPreferredSpokenLanguage(applicantDTO.getSpokenLanguage());
					if(applicantDTO.isWrittenLanguageChanged())
						householdContact.getContactPreferences().setPreferredWrittenLanguage(applicantDTO.getWrittenLanguage());
				}
				if (applicantDTO.getCompareApplicationDTO().isHasAddressChanged() || applicantDTO.isPartialAddressNotUpdated()) {

					if (applicantDTO.isMailAddressChanged()) {
						updateAddress(householdContact.getMailingAddress(), applicantDTO.getMailingAddress(), true);
					}
					if (applicantDTO.isPrimaryAddressChanged()) {
						updateAddress(householdContact.getHomeAddress(), applicantDTO.getPrimaryAddress(), true);
						householdContact.setHomeAddressIndicator(householdContact.getHomeAddress().getPostalCode() != null);
					}
					if (applicantDTO.isAddressLine1Changed() || applicantDTO.isAddressLine2Changed() || applicantDTO.isCityChanged()) {
						updateAddress(householdContact.getHomeAddress(), applicantDTO.getPrimaryAddress(), false);
						householdContact.setHomeAddressIndicator(householdContact.getHomeAddress().getPostalCode() != null);
					}
					if (applicantDTO.isAdminUpdate() && applicantDTO.isPartialAddressNotUpdated()) {
						updateAddress(householdContact.getHomeAddress(), applicantDTO.getEnrolledHomeAddress(), true);
						householdContact.setHomeAddressIndicator(householdContact.getHomeAddress().getPostalCode() != null);
					}
					householdContact.setMailingAddressSameAsHomeAddressIndicator(ReferralUtil.compareAddress(householdContact.getHomeAddress(), householdContact.getMailingAddress()));
				}
				if(applicantDTO.isSubscriber() && applicantDTO.isAdminUpdate() && (!applicantDTO.isAddressLine1Changed() || !applicantDTO.isAddressLine2Changed() || !applicantDTO.isCityChanged())) {
					updateAddress(householdContact.getHomeAddress(), applicantDTO.getPrimaryAddress(), true);
					householdContact.setHomeAddressIndicator(householdContact.getHomeAddress().getPostalCode() != null);
				}

				break;
			}
		}
	}
	
	private void updateMailingAddressForAllMembers(SingleStreamlinedApplication ssappln, CompareApplicantDTO applicantDTO) {
		for (HouseholdMember houseHoldMember : ssappln.getTaxHousehold().get(0).getHouseholdMember()) {
			//Since for the subscriber mailing address would be changed in above method 'updateChangedDemographicAndAddress' in the json
			//setting the new mailing address for other members in the HH if there is change in mailing address of primary member
			if (houseHoldMember.getPersonId() != applicantDTO.getPersonId()) {
				final HouseholdContact householdContact = houseHoldMember.getHouseholdContact();
				if (applicantDTO.getCompareApplicationDTO().isHasAddressChanged()) {

					if (applicantDTO.isMailAddressChanged()) {
						updateAddress(householdContact.getMailingAddress(), applicantDTO.getMailingAddress(), true);
					}
					householdContact.setMailingAddressSameAsHomeAddressIndicator(ReferralUtil.compareAddress(householdContact.getHomeAddress(), householdContact.getMailingAddress()));
				}
            }
		}
	}

	private void updateAddress(Address householdaddress, Address address, boolean isAddress) {

		householdaddress.setStreetAddress1(address.getStreetAddress1());
		householdaddress.setStreetAddress2(address.getStreetAddress2());
		householdaddress.setCity(address.getCity());
		if (isAddress) {
			householdaddress.setCounty(address.getCounty());
			householdaddress.setPostalCode(address.getPostalCode());
			householdaddress.setState(address.getState());
			householdaddress.setPrimaryAddressCountyFipsCode(address.getPrimaryAddressCountyFipsCode());
		}
	}

	private void updateAddressForSsapApplicant(SsapApplicant ssapApplicant, CompareApplicantDTO enrolledApplicant) {
		boolean primaryAddressChanged = enrolledApplicant.isPrimaryAddressChanged() || enrolledApplicant.isAddressLine1Changed() || enrolledApplicant.isAddressLine2Changed() || enrolledApplicant.isCityChanged();
		boolean mailingAddressChanged = enrolledApplicant.isMailAddressChanged();
		boolean primary = (!primaryAddressChanged && enrolledApplicant.getPrimaryAddress() != null);

		if (primaryAddressChanged) {
			if (enrolledApplicant.getPrimaryAddress() != null && ReferralUtil.isNotNullAndEmpty(enrolledApplicant.getPrimaryAddress().getPostalCode())) {
				if (ssapApplicant.getOtherLocationId() == null || ssapApplicant.getOtherLocationId().intValue() == 0) {
					final Location newLocation = createAndPersistNewLocation(enrolledApplicant.getPrimaryAddress());
					ssapApplicant.setOtherLocationId(BigDecimal.valueOf(newLocation.getId()));
					primary = true;
				} else {
					retrieveAndPersistLocation(ssapApplicant.getOtherLocationId(), enrolledApplicant.getPrimaryAddress(), enrolledApplicant.isPrimaryAddressChanged());
					primary = true;
				}
			}
		}
		
		if(enrolledApplicant.isAdminUpdate() && enrolledApplicant.isPartialAddressNotUpdated()) {
			    if (ssapApplicant.getOtherLocationId() == null || ssapApplicant.getOtherLocationId().intValue() == 0) {
					final Location newLocation = createAndPersistNewLocation(enrolledApplicant.getEnrolledHomeAddress());
					ssapApplicant.setOtherLocationId(BigDecimal.valueOf(newLocation.getId()));
					primary = true;
				} else {
					retrieveAndPersistLocation(ssapApplicant.getOtherLocationId(), enrolledApplicant.getEnrolledHomeAddress(), true);
					primary = true;
				}
		}

		if (!(ReferralUtil.compareAddress(enrolledApplicant.getPrimaryAddress(), enrolledApplicant.getMailingAddress())) || !primary) {
			BigDecimal primaryLocationId = ssapApplicant.getMailiingLocationId();
			if (mailingAddressChanged) {
				if (enrolledApplicant.getMailingAddress() != null && ReferralUtil.isNotNullAndEmpty(enrolledApplicant.getMailingAddress().getPostalCode())) {
					if (ssapApplicant.getMailiingLocationId() == null || ssapApplicant.getMailiingLocationId().intValue() == 0) {
						final Location newLocation = createAndPersistNewLocation(enrolledApplicant.getMailingAddress());
						ssapApplicant.setMailiingLocationId(BigDecimal.valueOf(newLocation.getId()));
						primaryLocationId = BigDecimal.valueOf(newLocation.getId());
					} /*else {
						retrieveAndPersistLocation(ssapApplicant.getMailiingLocationId(), enrolledApplicant.getMailingAddress(), true);
					}*/
				} else {
					ssapApplicant.setMailiingLocationId(null);
				}
			}

			if (!primary && (primaryAddressChanged || mailingAddressChanged)) {
				ssapApplicant.setOtherLocationId(primaryLocationId);
			}
		}
		ssapApplicantRepository.save(ssapApplicant);
	}

	private SsapApplicant persistSSAPApplicant(CompareApplicantDTO applicantDTO) {
		SsapApplicant ssapapplicant = ssapApplicantRepository.findSsapApplicantById(applicantDTO.getId());
		ssapapplicant.setFirstName(applicantDTO.getFirstName());
		ssapapplicant.setMiddleName(applicantDTO.getMiddleName());
		ssapapplicant.setLastName(applicantDTO.getLastName());
		ssapapplicant.setSsn(applicantDTO.getSsn());
		ssapapplicant.setGender(applicantDTO.getGender());
		ssapapplicant.setEmailAddress(applicantDTO.getEmailAddress());

		ssapapplicant = ssapApplicantRepository.save(ssapapplicant);
		return ssapapplicant;
	}

	private Location createAndPersistNewLocation(Address address) {
		Location newLocation = new Location();
		newLocation.setAddress1(address.getStreetAddress1());
		newLocation.setAddress2(address.getStreetAddress2());
		newLocation.setCity(address.getCity());
		newLocation.setState(address.getState());
		newLocation.setZip(address.getPostalCode());
		newLocation.setCounty(address.getCounty());
		newLocation = iLocationRepository.save(newLocation);
		return newLocation;
	}

	private Location retrieveAndPersistLocation(BigDecimal id, Address address, boolean isAddress) {
		Location location = iLocationRepository.findOne(id.intValue());
		location.setAddress1(address.getStreetAddress1());
		location.setAddress2(address.getStreetAddress2());
		location.setCity(address.getCity());

		if (isAddress) {
			
			location.setState(address.getState());
			location.setZip(address.getPostalCode());
			location.setCounty(address.getCounty());
		}
		location = iLocationRepository.save(location);
		return location;
	}

	private void updateSSAPJSON(long ssapApplicationId, String ssapJson) {
		SsapApplication ssapapplication = sssapApplicationRepository.findOne(ssapApplicationId);
		ssapapplication.setApplicationData(ssapJson);
		sssapApplicationRepository.save(ssapapplication);
		// sssapApplicationRepository.updateSsapObjectData(ssapJson, new
		// Timestamp((new Date()).getTime()), ssapApplicationId);
	}

	private HouseholdMember getPrimaryHouseHoldMember(SingleStreamlinedApplication ssappln) {
		HouseholdMember householdMem = null;
		for (HouseholdMember houseHoldMember : ssappln.getTaxHousehold().get(0).getHouseholdMember()) {
			if (houseHoldMember.getPersonId() == 1) {
				householdMem = houseHoldMember;
				break;
			}
		}
		return householdMem;
	}

	private void populateLivesAtOtherAddress(HouseholdMember primaryHouseholdMember, SingleStreamlinedApplication singleStreamlinedApplication) {
		final List<HouseholdMember> householdMemberList = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
		final int size = ReferralUtil.listSize(householdMemberList);
		primaryHouseholdMember.setLivesWithHouseholdContactIndicator(true);
		if (size > 1) // No Need to check if there is one primary
		{
			boolean blnCheck = false;
			boolean blnLivesOther = false;
			Address otherAddres = null;
			HouseholdMember member = null;
			for (int i = 0; i < size; i++) {
				member = householdMemberList.get(i);
				if (member == null) {
					continue;
				}
				if (member.getPersonId().compareTo(1) != 0) {

					blnCheck = false;
					blnLivesOther = false;
					otherAddres = null;

					if (primaryHouseholdMember.getHouseholdContact().getHomeAddress() != null && member.getHouseholdContact().getHomeAddress() != null) {
						blnCheck = ReferralUtil.compareAddressData(primaryHouseholdMember.getHouseholdContact().getHomeAddress(), member.getHouseholdContact().getHomeAddress());
					}

					if (blnCheck) {
						blnLivesOther = false;
					} else if (member.getHouseholdContact().getHomeAddress() != null && member.getHouseholdContact().getHomeAddress().getPostalCode() != null) {
						blnLivesOther = true;
						otherAddres = member.getHouseholdContact().getHomeAddress();
					} else {
						if (primaryHouseholdMember.getHouseholdContact().getMailingAddress() != null && member.getHouseholdContact().getMailingAddress() != null) {
							blnCheck = ReferralUtil.compareAddressData(primaryHouseholdMember.getHouseholdContact().getMailingAddress(), member.getHouseholdContact().getMailingAddress());
						}

						if (blnCheck) {
							blnLivesOther = false;
						} else if (member.getHouseholdContact().getMailingAddress() != null && member.getHouseholdContact().getMailingAddress().getPostalCode() != null) {
							blnLivesOther = true;
							otherAddres = member.getHouseholdContact().getMailingAddress();
						}
					}

					member.setLivesWithHouseholdContactIndicator(!blnLivesOther);

					if (blnLivesOther) {
						member.setLivesAtOtherAddressIndicator(blnLivesOther);
						if (member.getOtherAddress() == null) {
							member.setOtherAddress(new OtherAddress());
						}

						ReferralUtil.copyProperties(otherAddres, member.getOtherAddress().getAddress());

						member.getOtherAddress().getAddress().setCounty(otherAddres.getPrimaryAddressCountyFipsCode());
						member.getOtherAddress().getAddress().setCountyCode(otherAddres.getPrimaryAddressCountyFipsCode());
						member.getOtherAddress().getAddress().setPrimaryAddressCountyFipsCode(null);
					}
				}
			}
		}
	}
}
