package com.getinsured.eligibility.at.ref.service;


public class ReferralNoticeResponse {

	private String caseNumber;
	private String result;
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}

}

