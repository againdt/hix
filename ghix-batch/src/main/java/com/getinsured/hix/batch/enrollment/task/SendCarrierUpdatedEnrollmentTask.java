/**
 * 
 */
package com.getinsured.hix.batch.enrollment.task;

import java.util.Map;

import org.hibernate.JDBCException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.Ind21Service;
import com.getinsured.hix.batch.util.EnrollmentEmailUtils;
import com.getinsured.hix.dto.enrollment.EnrollmentBatchEmailDTO;
import com.getinsured.hix.enrollment.service.EnrollmentBatchService;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;

/**
 * @author panda_p
 *
 */
public class SendCarrierUpdatedEnrollmentTask implements Tasklet {
private static final Logger LOGGER = LoggerFactory.getLogger(SendCarrierUpdatedEnrollmentTask.class);
private EnrollmentBatchService enrollmentBatchService;
private EnrollmentEmailUtils enrollmentEmailUtils;
private Ind21Service ind21Service;
private JobService jobService;

@Override
public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)throws Exception{
	
	EnrollmentBatchEmailDTO enrollmentBatchEmailDTO = new EnrollmentBatchEmailDTO();
	enrollmentBatchEmailDTO.setFileLocation(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INBOUND834XMLPATH));
	
	try{
		LOGGER.info("SendCarrierUpdatedEnrollmentTask.execute : START");
		long jobExecutionId=-1;
		jobExecutionId=chunkContext.getStepContext().getStepExecution().getJobExecutionId();
		Map<Integer, String> errorMap = ind21Service.sendCarrierUpdatedDataToAHBX(jobExecutionId);
		
		String batchJobStatus=null;
		if(jobService != null && jobExecutionId != -1){
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}
		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}
		
		if(errorMap != null && !errorMap.isEmpty()){
			enrollmentBatchService.updateEnrolleeUpdateSendStatusForFailure(errorMap);
		}
		
		LOGGER.info("SendCarrierUpdatedEnrollmentTask.execute : END");
		
	}catch (Exception exception) {
		
		LOGGER.error("SendCarrierUpdatedEnrollmentTask.execute : FAIL "+EnrollmentUtils.getExceptionMessage(exception));
		
		if(!(exception instanceof JDBCException)) {
			enrollmentBatchEmailDTO.setErrorCode(201);
			
			if(enrollmentBatchEmailDTO.getErrorMessage()==null){
				enrollmentBatchEmailDTO.setErrorMessage(exception.getMessage());
			    }
			
			enrollmentEmailUtils.enrollmentBatchFailureEmailNotification(enrollmentBatchEmailDTO, chunkContext);
		}
		
		chunkContext.getStepContext().getStepExecution().setExitStatus(ExitStatus.FAILED);
		throw new Exception("Exception "+exception.getMessage()+" : Batch ExitStatus"+ ExitStatus.FAILED); 
	}
	
	return RepeatStatus.FINISHED;
}

public EnrollmentBatchService getEnrollmentBatchService() {
	return enrollmentBatchService;
}

public void setEnrollmentBatchService(
		EnrollmentBatchService enrollmentBatchService) {
	this.enrollmentBatchService = enrollmentBatchService;
}

public EnrollmentEmailUtils getEnrollmentEmailUtils() {
	return enrollmentEmailUtils;
}

public void setEnrollmentEmailUtils(EnrollmentEmailUtils enrollmentEmailUtils) {
	this.enrollmentEmailUtils = enrollmentEmailUtils;
}

public Ind21Service getInd21Service() {
	return ind21Service;
}

public void setInd21Service(Ind21Service ind21Service) {
	this.ind21Service = ind21Service;
}

public JobService getJobService() {
	return jobService;
}

public void setJobService(JobService jobService) {
	this.jobService = jobService;
}



}
