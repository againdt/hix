 <%@ include file="include.jsp" %>
 
 <div class="span3 margin30-b" id="sidebar">
			<a href="http://nmmaster.getinsured.com/hix/shop/employer/dashboard#rightpanel" name="skip" class="skip-sidebar" accesskey="s">Skip Side Bar to Main Content</a><div class="header">
				<h4>My Stuff</h4>
			</div>
			<ul class="nav nav-list">
  				<li><a href="#" id="findbroker"><i class="icon-dashboard"></i> Dashboard</a></li>
  				<li><a href="#" id="findbroker"><i class="icon-info-sign"></i> My Information</a></li>
  				<li><a href="#" id="findbroker"><i class="icon-group"></i> My Dependents</a></li>
  				<li><a href="#" id="findbroker"><i class="icon-briefcase"></i> My Plans</a></li>
  				<li><a href="#" id="findbroker"><i class="icon-cog"></i> Account Settings</a></li>
  				<li><a href="#" id="findbroker"><i class="icon-inbox"></i> Inbox</a></li>
			</ul>            
			<div class="header margin20-t">
                  <h4>Quick Links</h4>
			</div>
			<ul class="nav nav-list">
  				<li><a href="#" id="findbroker"><i class="icon-search"></i> Find an Agent</a></li>
  				<li><a href="#" id="findbroker"><i class="icon-plus"></i> Add Family Member</a></li>
  				<li><a href="#" id="findbroker"><i class="icon-pencil"></i> Change Address</a></li>
  				<li><a href="lifeChangeEvent.nmhx" id="findbroker"><i class="icon-flag"></i> Report Other Changes</a></li>
  				<li><a href="#" id="findbroker"><i class="icon-envelope"></i> Contact Customer Support</a></li>
			</ul>   
 </div> 
 
<div id="left_contant_area">
             
             <spring:message code="ssap.main." text=""/>
               <div id='finalEditDiv' class="left_Ui_list" style='display: none;'>
             <form>
              
              <div class="ad_alin_ui1 " style='font-style: italic; padding-left: 25px; border-top: medium none !important;'><spring:message code="ssap.main.editYourInfo" text="Edit your Information:"/></div>
              
                 <div id="HouseHoldDetailsUpdate_head" class="ad_alin_ui1 visited_list">
                   <input id="HouseHoldDetailsUpdate" onChange="editFinalPage();" type="radio" name="editRadioGroup" value="HouseHoldDetailsUpdate" class="radio_listDesign" checked>
                            <spring:message code="ssap.main.household" text="Household"/> </div>
                   <div class="show_divtext" id="HouseHoldDetailsUpdate_div">
                   
                   </div>
                   
                   <div id="nonFinancialUpdate_head" class="ad_alin_ui1">
                   <input type="radio" onChange="editFinalPage();" name="editRadioGroup" value="nonFinancialUpdate" id="nonFinancialUpdate" class="radio_listDesign1">
                          <spring:message code="ssap.main.nonFinancial" text="Non-Financial"/>  </div>
                   
				<div class="show_divtext" id="nonFinancialUpdate_div" style='display:none;'>
                   
                   
                   </div>
                   
                   <div id="financialDetailsUpdate_head" class="ad_alin_ui1 ">
                   <input type="radio" name="editRadioGroup" onChange="editFinalPage();" value="financialDetailsUpdate" id="financialDetailsUpdate" class="radio_listDesign1">
                           <spring:message code="ssap.main.income" text="Income"/> </div>
                   
					<div class="show_divtext" id="financialDetailsUpdate_div" style='display:none;'>
                    
                   </div>
                   
                 
			<div class="ad_alin_ui3" style="">
			<div style="float: left; display: inline; width: 50%; height: 100%;" id="editButtonDiv" onmouseover="$('#submitButtonDiv input').addClass('blueColor');" onmouseout="$('#submitButtonDiv input').removeClass('blueColor');">

							<input type="button" class="login noMargin noPadding" value="Edit" style="background: none repeat scroll 0px 0px rgb(20, 137, 45); border-radius: 0px 0px 0px 0px; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); width: 100%; height: 100%;" onclick="checkStillUpdateRequired();">
			</div>
			<div style="float: right; display: inline; border-left: 1px solid rgb(228, 228, 228); width: 49%; height: 100%;" id="submitButtonDiv" onmouseover="$('#editButtonDiv input').addClass('blueColor');" onmouseout="$('#editButtonDiv input').removeClass('blueColor');">						
							<input type="button" class="login noMargin noPadding" onclick="caller();" value="Submit" style="background: none repeat scroll 0px 0px rgb(20, 137, 45); border-radius: 0px 0px 0px 0px; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2); display: inline; width: 100%; height: 100%;">
			</div>
          </div>
          
             </form>

             </div>
             
             <div id="callProviderDiv" class="left_Ui_list" style="display:none">
	             <div>
	             	<input type="button" onclick="resetPlans()" value="Reset">
	             </div>
	             <br>
	             <div>	
	             	<input type="button" onclick="callProvider()" value="Show Providers">
	             </div>
             </div>
             	
             </div>