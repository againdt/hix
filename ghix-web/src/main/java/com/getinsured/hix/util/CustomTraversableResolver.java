package com.getinsured.hix.util;

import java.lang.annotation.ElementType;

import javax.validation.Path;
import javax.validation.TraversableResolver;
import javax.validation.Path.Node;

public class CustomTraversableResolver implements TraversableResolver {

	@Override
    public boolean isReachable(Object traversableObject,
            Node traversableProperty, Class<?> rootBeanType,
            Path pathToTraversableObject, ElementType elementType) {
        return true;
    }
 
    @Override
    public boolean isCascadable(Object traversableObject,
            Node traversableProperty, Class<?> rootBeanType,
            Path pathToTraversableObject, ElementType elementType) {
        return true;
    }

}