<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<style>
#planTypeDetail .cell, #planTypeDetail .details { width: 50%; }
#planDeductibleFlyDetail .cell , #planDeductibleFlyDetail .details { width: 50%; }
/* #majorDentalCareAdultDetail .cell, #majorDentalCareAdultDetail .details { width: 25%; }
#orthodontiaChildDetail .cell, #orthodontiaChildDetail .details { width: 25%; } */

#tile.span4 {margin-top: 13px !important;}

.details.cell.vertical-align  p {

	text-align: left !important;

}
</style>


<div class="ps-detail__highlights-container">

</div>

<div class="ps-detail__group-container">
	<%-- DEDUCTIBLE & OUT-OF-POCKET MAXIMUM --%>
	<div class="ps-detail__group">
		<div class="ps-detail__header ps-detail__header--group">
			<spring:message code="pd.label.commontext.deductible"/> &amp; <spring:message code="pd.label.icon.outOfPocket"/>
			<i class="icon-chevron-up toggle"></i>
		</div>

		<div class="toggle-div">
			<div class="ps-detail__group-body u-table">

				<div class="ps-detail__service" id="planDeductibleFlyDetail">
					<div class="u-table-cell ps-detail__service-label">
						<spring:message code="pd.label.deductible.ehb"/> (<spring:message code="pd.label.commontext.family"/>)
					</div>
				</div>

				<div class="ps-detail__service" id="planDeductibleIndDetail">
					<div class="u-table-cell ps-detail__service-label">
						<spring:message code="pd.label.deductible.ehb"/> (<spring:message code="pd.label.commontext.individual"/>)
					</div>
				</div>

				<div class="ps-detail__service" id="planOopMaxFlyDetail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode != 'MN'}">
							<spring:message code="pd.label.dentalDeductible.link2"/>
						</c:if>
						<c:if test="${stateCode == 'MN'}">
							<spring:message code="pd.label.ehbOutOfPocketMaximum"/>
						</c:if>
						(<spring:message code="pd.label.commontext.family"/>)
					</div>
				</div>


				<div class="ps-detail__service" id="planOopMaxIndDetail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode != 'MN'}">
							<spring:message code="pd.label.dentalDeductible.link2"/>
						</c:if>
						<c:if test="${stateCode == 'MN'}">
							<spring:message code="pd.label.ehbOutOfPocketMaximum"/>
						</c:if>
						(<spring:message code="pd.label.commontext.individual"/>)
					</div>
				</div>


				<c:if test="${stateCode != 'MN'}">
					<div class="ps-detail__service" id="planAdultDeductibleFlyDetail">
						<div class="u-table-cell ps-detail__service-label">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultDeductible" htmlEscape="false"/>">
								<spring:message code="pd.label.deductible.adultDeductible"/>
							</a>
							(<spring:message code="pd.label.commontext.family"/>)
						</div>
					</div>


					<div class="ps-detail__service" id="planAdultDeductibleIndDetail">
						<div class="u-table-cell ps-detail__service-label">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultDeductible" htmlEscape="false"/>">
								<spring:message code="pd.label.deductible.adultDeductible"/>
							</a>
							(<spring:message code="pd.label.commontext.individual"/>)
						</div>
					</div>

					<div class="ps-detail__service" id="planAdultAnnualBenefitLimitFlyDetail">
						<div class="u-table-cell ps-detail__service-label">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultAnnualBenefitLimit" htmlEscape="false"/>">
								<spring:message code="pd.label.deductible.adultAnnualBenefitLimit"/>
							</a>
							(<spring:message code="pd.label.commontext.family"/>)
						</div>
					</div>

					<div class="ps-detail__service" id="planAdultAnnualBenefitLimitIndDetail">
						<div class="u-table-cell ps-detail__service-label">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultAnnualBenefitLimit" htmlEscape="false"/>">
								<spring:message code="pd.label.deductible.adultAnnualBenefitLimit"/>
							</a>
							(<spring:message code="pd.label.commontext.individual"/>)
						</div>
					</div>

					<div class="ps-detail__service" id="planAdultOutOfPocketMaxFlyDetail">
						<div class="u-table-cell ps-detail__service-label">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultOutOfPocketMax" htmlEscape="false"/>">
								<spring:message code="pd.label.deductible.adultOutOfPocketMax"/>
							</a>
							(<spring:message code="pd.label.commontext.family"/>)
						</div>
					</div>

					<div class="ps-detail__service" id="planAdultOutOfPocketMaxIndDetail">
						<div class="u-table-cell ps-detail__service-label">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultOutOfPocketMax" htmlEscape="false"/>">
								<spring:message code="pd.label.deductible.adultOutOfPocketMax"/>
							</a>
							(<spring:message code="pd.label.commontext.individual"/>)
						</div>
					</div>
				</c:if>


			</div>
		</div>
	</div>
	<%-- DEDUCTIBLE & OUT-OF-POCKET MAXIMUM end --%>

	<%-- Adult Dental Coverage --%>
	<div class="ps-detail__group">
		<div class="ps-detail__header ps-detail__header--group">
			<spring:message code="pd.label.commontext.dental.adultCovg"/>
			<i class="icon-chevron-up toggle"></i>
		</div>

		<div class="toggle-div">
			<div class="ps-detail__group-body u-table">
				<div class="ps-detail__sub-header">
					<div class="u-table-cell">

					</div>
					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
					</div>

					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
					</div>

					<div class="u-table-cell u-table-cell--10">
						<spring:message code="pd.label.title.additionalInfo"/>
					</div>
				</div>

				<div class="ps-detail__service" id="orthodontiaAdultDetail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link7tooltip" htmlEscape="false"/>">
						</c:if>
						<spring:message code="pd.label.children.dental.link7"/>
						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>

				<div class="ps-detail__service" id="majorDentalCareAdultDetail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.majorAdulttooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.children.dental.majorAdult"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>

				<div class="ps-detail__service" id="accidentalDentalDetail">
					<div class="u-table-cell ps-detail__service-label">
						<spring:message code="pd.label.children.dental.accidental"/>
					</div>
				</div>

				<div class="ps-detail__service" id="basicDentalCareAdultDetail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.basicAdulttooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.children.dental.basicAdult"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>

				<div class="ps-detail__service" id="rtnDentalAdultDetail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link9tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.children.dental.link9"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>


			</div>
		</div>
	</div>
	<%-- Adult Dental Coverage END--%>


	<%-- Child Dental Coverage --%>
	<div class="ps-detail__group">
		<div class="ps-detail__header ps-detail__header--group">
			<spring:message code="pd.label.commontext.dental.childCovg"/>
			<i class="icon-chevron-up toggle"></i>
		</div>

		<div class="toggle-div">
			<div class="ps-detail__group-body u-table">
				<div class="ps-detail__sub-header">
					<div class="u-table-cell">

					</div>
					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
					</div>

					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
					</div>

					<div class="u-table-cell u-table-cell--10">
						<spring:message code="pd.label.title.additionalInfo"/>
					</div>
				</div>

				<div class="ps-detail__service" id="orthodontiaChildDetail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link8tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.children.dental.link8"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>

				<div class="ps-detail__service" id="majorDentalCareChildDetail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.majorChildtooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.children.dental.majorChild"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>

				<div class="ps-detail__service" id="dentalCheckupChildrenDetail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.checkuptooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.children.dental.checkup"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>

				<div class="ps-detail__service" id="basicDentalCareChildDetail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.basicChildtooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.children.dental.basicChild"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>


			</div>
		</div>
	</div>
	<%-- Child Dental Coverage ends --%>









</div>



<div class="row-fluid" id="detailHead" style="display:none">


    <div class="table-row span4 margin0" id="tile"></div>

  <input type="hidden" id="exchangeName" value="${exchangeName}">
  <div class="span8 detail-wrapper">
    <div class="benefits dental-benefits clearfix">

      <%-- <div class="table">
        <div class="row header">
            <div class="cell header-name"><h5><spring:message code="pd.label.title.summary"/></h5></div>
    </div>
        <div id="productNameDetail" class="row-detail clearfix">
        	<div class="cell">
	          	<spring:message code="pd.label.planName"/>
       	  	</div>
        </div>
        <div class="row-detail clearfix" id="planTypeDetail">
          <div class="cell" style="width:50%"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.summary.producttype" htmlEscape="true"/>"><spring:message code="pd.label.summary.planType"/> </a></div>
        </div>
        <div class="row-detail clearfix" id="planTierDetail">
          <div class="cell"><spring:message code="pd.label.summary.planTier"/></div>
        </div>
     <c:if test="${dentalGuaranteedEnable=='ON'}">
          <div class="row-detail clearfix" id="premiumTypeDetail">
      <div class="cell"><a data-placement="right" rel="tooltipp" href="#" data-original-title=""><spring:message code="pd.label.summary.premiumType"/> </a></div>
      </div>
      </c:if>
      </div> --%>

      <%-- <div class="table">
    <div class="row header">
      <div class="cell header-name">
        <h5><spring:message code="pd.label.commontext.deductible"/> &amp; <spring:message code="pd.label.icon.outOfPocket"/></h5>
      </div>
    </div>

    <div class="row-detail clearfix" id="planDeductibleFlyDetail">
      <div class="cell"><spring:message code="pd.label.deductible.ehb"/> (<spring:message code="pd.label.commontext.family"/>)</div>
    </div>
    <div class="row-detail clearfix" id="planDeductibleIndDetail">
      <div class="cell"><spring:message code="pd.label.deductible.ehb"/> (<spring:message code="pd.label.commontext.individual"/>)</div>
    </div>
    <div class="row-detail clearfix" id="planOopMaxFlyDetail">
		<div class="cell">
			<c:if test="${stateCode != 'MN'}">
				<spring:message code="pd.label.dentalDeductible.link2"/>
			</c:if>
			<c:if test="${stateCode == 'MN'}">
				<spring:message code="pd.label.ehbOutOfPocketMaximum"/>
			</c:if>
			(<spring:message code="pd.label.commontext.family"/>)
		</div>
    </div>
    <div class="row-detail clearfix" id="planOopMaxIndDetail">
		<div class="cell">
			<c:if test="${stateCode != 'MN'}">
				<spring:message code="pd.label.dentalDeductible.link2"/>
			</c:if>
			<c:if test="${stateCode == 'MN'}">
				<spring:message code="pd.label.ehbOutOfPocketMaximum"/>
			</c:if>
			(<spring:message code="pd.label.commontext.individual"/>)
		</div>
    </div>

    <c:if test="${stateCode != 'MN'}">
    <div class="row-detail clearfix" id="planAdultDeductibleFlyDetail">
      <div class="cell"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultDeductible" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultDeductible"/></a> (<spring:message code="pd.label.commontext.family"/>)</div>
    </div>
    <div class="row-detail clearfix" id="planAdultDeductibleIndDetail">
      <div class="cell"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultDeductible" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultDeductible"/></a> (<spring:message code="pd.label.commontext.individual"/>)</div>
    </div>
    <div class="row-detail clearfix" id="planAdultAnnualBenefitLimitFlyDetail">
      <div class="cell"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultAnnualBenefitLimit" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultAnnualBenefitLimit"/></a> (<spring:message code="pd.label.commontext.family"/>)</div>
    </div>
    <div class="row-detail clearfix" id="planAdultAnnualBenefitLimitIndDetail">
      <div class="cell"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultAnnualBenefitLimit" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultAnnualBenefitLimit"/></a> (<spring:message code="pd.label.commontext.individual"/>)</div>
    </div>
    <div class="row-detail clearfix" id="planAdultOutOfPocketMaxFlyDetail">
      <div class="cell"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultOutOfPocketMax" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultOutOfPocketMax"/></a> (<spring:message code="pd.label.commontext.family"/>)</div>
    </div>
    <div class="row-detail clearfix" id="planAdultOutOfPocketMaxIndDetail">
      <div class="cell"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultOutOfPocketMax" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultOutOfPocketMax"/></a> (<spring:message code="pd.label.commontext.individual"/>)</div>
    </div>
       </c:if>
      </div> --%>

      <%-- <div class="table mobile-table">
        <div class="row header">
            <div class="cell header-name"><h5><spring:message code="pd.label.commontext.dental.adultCovg"/></h5></div>
            <div class="cell header-name xs-hide"><h5><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></h5></div>
            <c:if test="${stateCode != 'CA' && stateCode != 'MN'}"><div class="cell header-name xs-hide"><h5><spring:message code="pd.label.title.appliesToDeductible"/></h5></div>
            	<div class="cell header-name xs-hide"><h5><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></h5></div>
           </c:if> <div class="cell header-name xs-hide"><h5><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></h5></div>
            <div class="cell header-name xs-hide"><h5><spring:message code="pd.label.title.additionalInfo"/></h5></div>
        </div>

          <div class="row-detail clearfix"  id="orthodontiaAdultDetail">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link7tooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.dental.link7"/></a>
            </div>
          </div >
          <div class="row-detail clearfix"  id="majorDentalCareAdultDetail">
            <div class="cell column-name">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.majorAdulttooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.dental.majorAdult"/></a>

            </div>
          </div >
          <div class="row-detail clearfix"  id="accidentalDentalDetail">
            <div class="cell column-name">
            	<spring:message code="pd.label.children.dental.accidental"/>
            </div>
          </div >
          <div class="row-detail clearfix"  id="basicDentalCareAdultDetail">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.basicAdulttooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.dental.basicAdult"/></a>
            </div>
          </div >
          <div class="row-detail clearfix"  id="rtnDentalAdultDetail">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link9tooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.dental.link9"/></a>
            </div>
          </div >

      </div> --%>

      <%-- <div class="table mobile-table">
        <div class="row header">
            <div class="cell header-name"><h5><spring:message code="pd.label.commontext.dental.childCovg"/></h5></div>
            <div class="cell header-name xs-hide"><h5><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></h5></div>
           <c:if test="${stateCode != 'CA' && stateCode != 'MN'}"> <div class="cell xs-hide"><h5><spring:message code="pd.label.title.appliesToDeductible"/></h5></div>
            <div class="cell header-name xs-hide"><h5><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></h5></div>
            </c:if>
            <div class="cell header-name xs-hide"><h5><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></h5></div>
            <div class="cell header-name xs-hide"><h5><spring:message code="pd.label.title.additionalInfo"/></h5></div>
        </div>
        <div class="row-detail clearfix" id="orthodontiaChildDetail">
          <div class="cell column-name">
          		<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link8tooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.dental.link8"/></a>
          </div>
        </div>
        <div class="row-detail clearfix" id="majorDentalCareChildDetail">
          <div class="cell column-name">
            <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.majorChildtooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.dental.majorChild"/></a>
          </div>
        </div>
        <div class="row-detail clearfix" id="dentalCheckupChildrenDetail">
          <div class="cell column-name">
            <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.checkuptooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.dental.checkup"/></a>
          </div>
        </div>
        <div class="row-detail clearfix" id="basicDentalCareChildDetail">
          <div class="cell column-name">
          <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.basicChildtooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.dental.basicChild"/></a>
          </div>
        </div>
      </div> --%>

  </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		setPageTitle('<spring:message code="pd.label.title.viewDentalPlanDetails"/>');
	});
  window.scrollTo(0,0);
  function getSubToDeduct(APPLIES){
  var str = "";
  if(APPLIES != null && APPLIES.AppliesNetwk && APPLIES.AppliesNetwk != "" && APPLIES.AppliesNetwk != "Not Applicable"){
    str += "<i class='has-deductible-"+APPLIES.AppliesNetwk+"'></i>";

  } else {
    str += "<p><small><spring:message code='pd.label.commontext.notAvailable' htmlEscape='true'/></small></p>";
  }
  return str;

  }

</script>

<script type="text/javascript">
var exchName =  '${exchangeName}';
$("#externalWarnBox").live('click',function(e){
	var that = $(this),
	getURL = $(this).attr('href'),
	urlhostname = $('<a>').prop('href', getURL).prop('hostname'),
	getExchangeName = exchName,
	htmlContent = '<h4><spring:message arguments="${exchangeName}" code="pd.label.commontext.externalWarnBox.leavingAlert" /></h4> <p><spring:message code="pd.label.commontext.externalWarnBox.access" /> </p>',
	htmlURL = '<a href="'+getURL+'" target="_blank">'+getURL+'</a>',
                htmlContent1 = '<p>'+getExchangeName+' <spring:message code="pd.label.commontext.externalWarnBox1" /></p>'+
                '<div><a href="#" class="btn externalModalClose"><spring:message code="pd.label.commontext.externalWarnBox.no" /><span class="aria-hidden"><spring:message code="pd.label.commontext.externalWarnBox.closeModal" /></span></a> <a href="'+getURL+'" class="btn btn-primary" target="_blank" id="yesBtn" aria-hidden="false"><spring:message code="pd.label.commontext.externalWarnBox.yes" /></a></div>'+
                '<p><spring:message code="pd.label.commontext.externalWarnBox.thankYou" /></p>';

	if(urlhostname !='' && window.location.hostname != urlhostname){
		$('#warningBox').remove();
		e.preventDefault();
		var warningHTML = $('<div class="modal" id="warningBox"><div class="modal-header" style="border-bottom:0; "><button type="button" class="close externalModalClose" aria-hidden="false"><span aria-hidden="true">&times;</span><span class="aria-hidden">close modal</span></button></div><div class="modal-body gutter10" style="max-height:470px;"><div class="uploadText"></div></div><div class="modal-footer"></div></div>').modal({backdrop:"static",keyboard:"false"});
        $(warningHTML).find('div.uploadText').html(htmlContent + htmlURL + htmlContent1);
	}

	$('.externalModalClose').live('click',function(){
		that.attr('href',getURL);
		$('#warningBox').modal("hide");
	});

	$('#yesBtn').live('click',function(){
		that.attr('href',getURL);
		$('#warningBox').modal("hide");
	});
});
</script>

<script type="text/html" id="planNameTpl">
	<span class="details"><@=name@></span>
</script>

<script type="text/html" id="tileTpl">
	<div class="ps-detail__panel">
		<@ if($("#gpsVersion").val() == 'V1') { @>
			<@ if(smartScore >=80){ @>
				<div class="ps-detail__tile detailPlanTile best details">
					<a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.green"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
			<@ } else if(smartScore >=60){ @>
				<div class="ps-detail__tile detailPlanTile ok details">
					<a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.yellow"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
			<@ } else { @>
				<div class="ps-detail__tile detailPlanTile poor details">
					<a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.red"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
			<@ } @>

				<div class="score">
					<div class="circle-wrapper">
						<div class="center-circle"></div>
						<div class="slice">
							<div class="pie score_<@=smartScore@> pie-color"></div>
						</div>
						<div class="circle-core center-circle">
							<div class="circle-num"><@=smartScore@></div>
						</div>
					</div>
				</div>
			</a>
		<@ } else { @>
			<div class="ps-detail__tile" tabindex="0" aria-label="You are on plan <@=name@>">
		<@ } @>
			<!-- tile header -->

			<!-- tile header end-->

			<!-- tile body -->
	        <div class="cp-tile__body">
	            <!-- logo img -->
	            <div id="detail_<@=id@>" class="cp-tile__img-link cp-tile__img-link-white detail">
	                <@ if(issuer.length > 24) { @>
	                    <img src="<@=issuerLogo@>" alt="<@=issuer.substr(0,24)@>..." class="cp-tile__img">
	                <@ } else { @>
	                    <img src="<@=issuerLogo@>" alt="<@=issuer@>" class="cp-tile__img">
	                <@ } @>
	            </div>
	            <!-- logo img end-->

	            <!-- plan name -->
	            <div class="cp-tile__plan-name">
	                <a class="detail" href="#detail" id="detail_<@=id@>" data-placement="bottom" rel="tooltip" role="tooltip" data-original-title="<@=name@>" aria-label="<@=name@>">
	                    <@ if(name.length > 24) { @>
	                        <@=name.substr(0,24)@>...
	                    <@ } else {@>
	                        <@=name@>
	                    <@ } @>
	                </a>
	            </div>
	            <!-- plan name end-->

				<!-- metal tier -->
	            <div class="cp-tile__metal-tier">
	                <@=level@> &nbsp;<@=networkType@>
	            </div>
	            <!-- metal tier end-->

				<!-- current plan -->
				<@ if ($("#enrollmentType").val() == 'A' && $("#initialDentalPlanId").val() == planId){ @>
					<div>
						<span class="cp-tile__current-plan">
							<spring:message code="pd.label.title.yourCurrentPlan"/>
						</span>
					</div>
				<@ } @>
				<!-- current plan end -->

	            <!-- premium -->
	            <div class="cp-tile__premium">
					<span class="cp-tile__premium-amount">$<@=premiumAfterCredit.toFixed(2) @></span>
					<span class="cp-tile__premium-month"><spring:message code="pd.label.title.premiumPerMonth"/></span>

					<@ if($('#stateCode').val() === "MN") { @>
						<a
							class="u-margin-left-10"
							href="#"
							rel="tooltip"
							data-html="true"
							data-placement="bottom"
							data-original-title="<@=getMemberLevelPremiumHtml(planDetailsByMember, premium.toFixed(2), aptc.toFixed(2), premiumAfterCredit.toFixed(2))@>">
							<spring:message code="pd.label.title.details"/>
						</a>
					<@ } @>
				</div>
	            <!-- premium end-->

	            <!-- tax credit -->
				<@ if($('#exchangeType').val() != 'OFF' || $('#PGRM_TYPE').val() == 'Shop') {@>
					<@ if (aptc > 0) {@>
			            <div class="cp-tile__tax-credit">
							<spring:message arguments="<@=aptc.toFixed(2)@>" code="pd.label.commontext.afterTaxCredit"/>
						</div>
					<@ } @>
				<@ } @>
	            <!-- tax credit end -->
	        </div>
	        <!-- tile body ends -->

			<!-- tile footer -->
	        <div class="ps-detail__tile-footer">
	            <@ if ($('#stateCode').val() != "CT" && ($("#flowType").val() == 'PLANSELECTION' || $("#flowType").val() == 'PREELIGIBILITY')){ @>
					<a href="#" id="cart_<@=planId@>" class="btn btn-primary ps-detail__tile-add addToCart" title="<spring:message code="pd.label.title.add"/>">
						<spring:message code="pd.label.title.add"/>
	                    <i class="icon-shopping-cart"></i>
	                </a>
	            <@ } @>
			</div>
	        <!-- tile footer ends -->
		</div>
		<!-- tile end -->

		<div class="ps-detail__highlights">
			<p class="ps-detail__header"><spring:message code="pd.label.commontext.planHighLights"/></p>
			<table class="ps-detail__highlights-table">
				<!-- Plan name -->
				<tr>
					<th scope="row">
						<spring:message code="pd.label.planName"/>
					</th>
					<td class="cp-tile__value">
						<@=name@>
					</td>
				</tr>
				<!-- Plan name end -->

				<!-- Routine Dental (Adult) -->
				<tr>
					<th scope="row">
						<spring:message code="pd.label.title.routineDentalAdult"/>
					</th>
					<td class="cp-tile__value">
						<@ if(planTier1.RTN_DENTAL_ADULT.tileDisplayVal != null && planTier1.RTN_DENTAL_ADULT.tileDisplayVal != ""){@>
							<@ if($('#stateCode').val() !== 'MN') { @>
                                <spring:message code="pd.label.title.youpay"/>
                            <@ } @>
							<@=planTier1.RTN_DENTAL_ADULT.tileDisplayVal@>
							<@ if($('#stateCode').val() === 'MN' && planTier1.RTN_DENTAL_ADULT.tileDisplayVal.indexOf('%') === -1) { @>
                                <spring:message code="pd.label.copay"/>
                            <@ } else if($('#stateCode').val() === 'MN' && planTier1.RTN_DENTAL_ADULT.tileDisplayVal.indexOf('%') !== -1) { @>
                                <spring:message code="pd.label.coinsurance"/>
                            <@ } @>

						<@ } else { @>
							<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
						<@ } @>
					</td>
				</tr>
				<!-- Routine Dental (Adult)	end -->

				<!-- Dental Checkup (Child)	 -->
				<tr>
					<th scope="row">
						<spring:message code="pd.label.title.dentalCheckupChild"/>
					</th>
					<td class="cp-tile__value">
						<@ if(planTier1.DENTAL_CHECKUP_CHILDREN.tileDisplayVal != null && planTier1.DENTAL_CHECKUP_CHILDREN.tileDisplayVal != ""){@>
							<@ if($('#stateCode').val() !== 'MN') { @>
                                <spring:message code="pd.label.title.youpay"/>
                            <@ } @>
							<@=planTier1.DENTAL_CHECKUP_CHILDREN.tileDisplayVal@>
							<@ if($('#stateCode').val() === 'MN' && planTier1.DENTAL_CHECKUP_CHILDREN.tileDisplayVal.indexOf('%') === -1) { @>
                                <spring:message code="pd.label.copay"/>
                            <@ } else if($('#stateCode').val() === 'MN' && planTier1.DENTAL_CHECKUP_CHILDREN.tileDisplayVal.indexOf('%') !== -1) { @>
                                <spring:message code="pd.label.coinsurance"/>
                            <@ } @>
						<@ } else { @>
							<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
						<@ } @>
					</td>
				</tr>
				<!-- Dental Checkup (Child)	end -->



				<!-- Deductible (Child) -->
				<tr>
					<th scope="row">
						<@ if($('#stateCode').val() != 'CA') { @>
							<a href="#" rel="tooltip" data-placement="right" data-html="true" data-original-title="<spring:message code="pd.tooltip.dental.deductible"/>">
						<@ } @>

						<spring:message code="pd.label.commontext.deductible" htmlEscape='true'/> (<spring:message code="pd.label.commontext.child"/>)

						<@ if($('#stateCode').val() != 'CA') { @>
							</a>
						<@ } @>
					</th>
					<td class="cp-tile__value">
						<@ if(planCosts.DEDUCTIBLE_MEDICAL != null) { @>
							<@ if(PERSON_COUNT != "" && PERSON_COUNT != 0 && PERSON_COUNT > 1  && $('#stateCode').val() == 'CA'){ @>
								<@ if(planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly != null && planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly != ""){ @>
									<a href="#" class="dotted" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly).toFixed()@>" code="pd.tooltip.dental.combined.deductible" htmlEscape="false"/>">$<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly).toFixed()@></a>
								<@ } else if(planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkFly != null && planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkFly != ""){ @>
									<a href="#" class="dotted" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkFly).toFixed()@>" code="pd.tooltip.dental.combined.deductible" htmlEscape="false"/>">$<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkFly).toFixed()@></a>
								<@ } else { @>
									<spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
								<@ } @>
							<@ } else {@>

								<@ if(planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd != null && planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd != ""){@>
									<a href="#" class="dotted" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd).toFixed()@>" code="pd.tooltip.dental.combined.deductible" htmlEscape="false"/>">$<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd).toFixed()@></a>
								<@ } else if(planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkInd != null && planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkInd != ""){ @>
									<a href="#" class="dotted" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkInd).toFixed()@>" code="pd.tooltip.dental.combined.deductible" htmlEscape="false"/>">$<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkInd).toFixed()@></a>
								<@ } else { @>
									<spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
								<@ } @>
							<@ } @>
						<@ } else { @>
							<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
						<@ } @>
					</td>
				</tr>
				<!-- Deductible (Child) end -->

				<@ if($("#showOopOnOff").val() == 'ON') { @>
					<!--  -->
					<tr>
						<th scope="row">
							<a href="#" rel="tooltip" data-placement="right" data-html="true" data-original-title="<spring:message code="pd.tooltip.dental.oopmax"/>">
								<spring:message code="pd.label.oopmax"/>
								<@ if ($('#stateCode').val() != "MN"){ @>
									(<spring:message code="pd.label.commontext.child"/>)
								<@ } @>
							</a>
						</th>
						<td class="cp-tile__value">
							<@ if(planCosts.MAX_OOP_MEDICAL != null) { @>
								<@ if(PERSON_COUNT != "" && PERSON_COUNT != 0 && PERSON_COUNT > 1 ){ @>
									<@ if(planCosts.MAX_OOP_MEDICAL.inNetworkFly != null && planCosts.MAX_OOP_MEDICAL.inNetworkFly != ""){ @>
										<a href="#" class="dotted" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=parseFloat(planCosts.MAX_OOP_MEDICAL.inNetworkFly).toFixed()@>" code="pd.tooltip.dental.oopmax1" htmlEscape="false"/>">$<@=parseFloat(planCosts.MAX_OOP_MEDICAL.inNetworkFly).toFixed()@></a>
									<@ } else if(planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkFly != null && planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkFly != ""){ @>
										<a href="#" class="dotted" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=parseFloat(planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkFly).toFixed()@>" code="pd.tooltip.dental.oopmax1" htmlEscape="false"/>">$<@=parseFloat(planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkFly).toFixed()@></a>
									<@ } else { @>
										<spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
									<@ } @>
								<@ } else {@>
									<@ if(planCosts.MAX_OOP_MEDICAL.inNetworkInd != null && planCosts.MAX_OOP_MEDICAL.inNetworkInd != ""){@>
										<a href="#" class="dotted" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=parseFloat(planCosts.MAX_OOP_MEDICAL.inNetworkInd).toFixed()@>" code="pd.tooltip.dental.oopmax1" htmlEscape="false"/>">$<@=parseFloat(planCosts.MAX_OOP_MEDICAL.inNetworkInd).toFixed()@></a>
									<@ } else if(planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkInd != null && planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkInd != ""){ @>
										<a href="#" class="dotted" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=parseFloat(planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkInd).toFixed()@>" code="pd.tooltip.dental.oopmax1" htmlEscape="false"/>">$<@=parseFloat(planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkInd).toFixed()@></a>
									<@ } else { @>
										<spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
									<@ } @>
								<@ } @>
							<@ } else { @>
								<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
							<@ } @>
						</td>
					</tr>
					<!-- end -->
				<@ } @>
			</table>
		</div>
	</div>

	<div class="ps-detail__benefits">
		<p class="ps-detail__header"><spring:message code="pd.label.commontext.benefitsResournces"/></p>
		<ul class="ps-detail__benefits-list">
			<li class="ps-detail__benefit-list">
				<i class="icon-list-ul"></i>
				<@ if(sbcUrl != null && sbcUrl != '') { @>
					<a
						class="ps-detail__benefit-link gtm_summary_benefits_external"
						href="<@=sbcUrl@>"
						id="externalWarnBox"
						onclick="window.dataLayer.push({'event': 'contentdownloadEvent', 'eventCategory': 'Plan Selection - Content Download', 'eventAction': 'Document Download', 'eventLabel': '<@=issuer@> | <spring:message code="pd.label.link.sbcUrl"/>'});
						window.dataLayer.push({'event': 'outboundlinkEvent', 'eventCategory': 'Plan Selection - Outbound Links', 'eventAction': 'Outbound Link Click', 'eventLabel': '<@=issuer@> | <spring:message code="pd.label.link.sbcUrl"/>'});">
						<spring:message code="pd.label.link.sbcUrl" htmlEscape="true"/></a>
				<@ } else { @>
					<spring:message code="pd.label.link.sbcUrlNotAvailable" htmlEscape="true"/>
				<@ } @>
			</li>

			<li class="ps-detail__benefit-list">
				<i class="icon-file-text-alt"></i>
				<@ if(planBrochureUrl != null && planBrochureUrl != '') { @>
					<a
						class="ps-detail__benefit-link gtm_plan_brochure"
						href="<@=planBrochureUrl@>"
						id="externalWarnBox"
						onclick="window.dataLayer.push({'event': 'contentdownloadEvent', 'eventCategory': 'Plan Selection - Content Download', 'eventAction': 'Document Download', 'eventLabel': '<@=issuer@> | <spring:message code="pd.label.link.downloadBrochure"/>'});
						window.dataLayer.push({'event': 'outboundlinkEvent', 'eventCategory': 'Plan Selection - Outbound Links', 'eventAction': 'Outbound Link Click', 'eventLabel': '<@=issuer@> | <spring:message code="pd.label.link.downloadBrochure"/>'});">
						<spring:message code="pd.label.link.downloadBrochure"/></a>
				<@ } else { @>
					<spring:message code="pd.label.link.planBrochureNotAvailable" htmlEscape="true"/>
				<@ } @>
			</li>

			<li class="ps-detail__benefit-list">
				<i class="icon-stethoscope"></i>
				<@ if(providerLink != null && providerLink != '') { @>
					<a
						class="ps-detail__benefit-link gtm_provider_directory"
						href="<@=providerLink@>"
						id="externalWarnBox"
						onclick="window.dataLayer.push({'event': 'outboundlinkEvent', 'eventCategory': 'Plan Selection - Outbound Links', 'eventAction': 'Outbound Link Click', 'eventLabel': '<@=issuer@> | <spring:message code="pd.label.link.providerDirectoryUrl"/>'});">
						<spring:message code="pd.label.link.providerDirectoryUrl" htmlEscape="true"/>
					</a>
				<@ } else { @>
					<spring:message code="pd.label.link.providerDirectoryNotAvailable" htmlEscape="true"/>
				<@ } @>
			</li>
		</ul>
	</div>

</script>

<script type="text/html" id="planTypeTplDetail">
  <div class="details">
  <@ if(NETWORK_TYPE == 'PPO') { @> <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.ppo"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'HMO') { @><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.hmo"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'POS') { @><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.pos"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'DHMO') { @><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.dhmo"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'DPPO') { @><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.dppo"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'HSA') { @><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.hsa"/>"><@=NETWORK_TYPE@></a>
    <@ } else { @> <@=NETWORK_TYPE@> <@ } @>
  </div>
</script>
<script type="text/html" id="planTierTplDetail">
   <div class="details">
    <p> <@=LEVEL@></p>
   </div>
</script>

<script type="text/html" id="premiumTypeTplDetail">
   <div class="details">
    <p> <@=DENTAL_GURANTEE@></p>
   </div>
</script>

<script type="text/html" id="deductibleIndTplDetail">
<div class="details u-table-cell">
	<@ if(DEDUCTIBLE != null) {@>

		<@ if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() == "1" ) {@>

			<@ if( (DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != "") || (DEDUCTIBLE.inNetworkTier2Ind != null && DEDUCTIBLE.inNetworkTier2Ind != "") || (DEDUCTIBLE.outNetworkInd != null && DEDUCTIBLE.outNetworkInd != "") || (DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != "")) {@>
				<@ if( DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != "" ) {@>
		  			$<@=parseFloat(DEDUCTIBLE.inNetworkInd).toFixed()@> (<spring:message code="pd.label.title.inNetwork" htmlEscape="true"/>)
				<@} @>
				<@ if( DEDUCTIBLE.inNetworkTier2Ind != null && DEDUCTIBLE.inNetworkTier2Ind != "" ) {@>
		  			$<@=parseFloat(DEDUCTIBLE.inNetworkTier2Ind).toFixed()@> (<spring:message code="pd.label.title.tier2Network" htmlEscape="true"/>)
				<@} @>
				<@ if( DEDUCTIBLE.outNetworkInd != null && DEDUCTIBLE.outNetworkInd != "" ) {@>
			  		$<@=parseFloat(DEDUCTIBLE.outNetworkInd).toFixed()@> (<spring:message code="pd.label.title.outOfNetwork" htmlEscape="true"/>)
				<@} @>
				<@ if( DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != "" ) {@>
			  		$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkInd).toFixed()@> (<spring:message code="pd.label.title.combinedAndOutOfNetwork"/>)
				<@} @>
			<@} else {@>
	  			<spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
		  	<@} @>

		<@ } else if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1" ) {@>

			<@ if( (DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != "") || (DEDUCTIBLE.inNetworkTier2FlyPerPerson != null && DEDUCTIBLE.inNetworkTier2FlyPerPerson != "") || (DEDUCTIBLE.outNetworkFlyPerPerson != null && DEDUCTIBLE.outNetworkFlyPerPerson != "") || (DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != "")) {@>
				<@ if( DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != "" ) {@>
		  			$<@=parseFloat(DEDUCTIBLE.inNetworkFlyPerPerson).toFixed()@> (<spring:message code="pd.label.title.inNetwork" htmlEscape="true"/>)
				<@} @>
				<@ if( DEDUCTIBLE.inNetworkTier2FlyPerPerson != null && DEDUCTIBLE.inNetworkTier2FlyPerPerson != "" ) {@>
		  			$<@=parseFloat(DEDUCTIBLE.inNetworkTier2FlyPerPerson).toFixed()@> (<spring:message code="pd.label.title.tier2Network" htmlEscape="true"/>)
				<@} @>
				<@ if( DEDUCTIBLE.outNetworkFlyPerPerson != null && DEDUCTIBLE.outNetworkFlyPerPerson != "" ) {@>
			  		$<@=parseFloat(DEDUCTIBLE.outNetworkFlyPerPerson).toFixed()@> (<spring:message code="pd.label.title.outOfNetwork" htmlEscape="true"/>)
				<@} @>
				<@ if( DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != "" ) {@>
			  		$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFlyPerPerson).toFixed()@> (<spring:message code="pd.label.title.combinedAndOutOfNetwork"/>)
				<@} @>
			<@} else {@>
	  			<spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
		  	<@} @>
	  	<@} @>
	<@ } else{ @>
   		<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
	<@ } @>
</div>
<div class="u-table-cell column-placeholder"></div>
</script>

<script type="text/html" id="deductibleFlyTplDetail">
<div class="details u-table-cell">
	<@ if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1" ) {@>
		<@ if(DEDUCTIBLE != null) {@>
			<@ if(((DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != "") || (DEDUCTIBLE.inNetworkTier2Fly != null && DEDUCTIBLE.inNetworkTier2Fly != "") || (DEDUCTIBLE.outNetworkFly != null && DEDUCTIBLE.outNetworkFly != "") || (DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != ""))) {@>
				<@ if( DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != "" ) {@>
					$<@=parseFloat(DEDUCTIBLE.inNetworkFly).toFixed()@> (<spring:message code="pd.label.title.inNetwork" htmlEscape="true"/>)
				<@} @>
				<@ if( DEDUCTIBLE.inNetworkTier2Fly != null && DEDUCTIBLE.inNetworkTier2Fly != "" ) {@>
					$<@=parseFloat(DEDUCTIBLE.inNetworkTier2Fly).toFixed()@> (<spring:message code="pd.label.title.tier2Network" htmlEscape="true"/>)
				<@} @>
				<@ if( DEDUCTIBLE.outNetworkFly != null && DEDUCTIBLE.outNetworkFly != "" ) {@>
					$<@=parseFloat(DEDUCTIBLE.outNetworkFly).toFixed()@> (<spring:message code="pd.label.title.outOfNetwork" htmlEscape="true"/>)
				<@} @>
				<@ if( DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != "" ) {@>
					$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFly).toFixed()@> (<spring:message code="pd.label.title.combinedAndOutOfNetwork"/>)
				<@} @>
			<@} else {@>
				<spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
			<@} @>
		<@ } else{ @>
			<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
		<@ } @>
	<@} else {@>
		<spring:message code="pd.label.commontext.notApplicable"/>
	<@} @>
</div>
<div class="u-table-cell column-placeholder"></div>
</script>

<script type="text/html" id="benefitTplDetail">
	<div class="details u-table-cell">
		<div class="u-show-block-xs u-margin-top-10">
			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
				<spring:message code="pd.label.title.inNetwork"/>
			</a>
		</div>
		<@=BENEFIT1.displayVal@>
	</div>


	<div class="details u-table-cell">
		<div class="u-show-block-xs u-margin-top-10">
			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
				<spring:message code="pd.label.title.outOfNetwork"/>
			</a>
		</div>

		<@=BENEFITOUT.displayVal@>
	</div>

	<div class="details u-table-cell">
		<div class="u-show-block-xs u-margin-top-10">
			<spring:message code="pd.label.title.additionalInfo"/>
		</div>

		<@ if(EXPLANATION != null) { @>
			<@ if(( EXPLANATION.displayVal != null && EXPLANATION.displayVal != "" ) && ( EXCLUSION.displayVal != null && EXCLUSION.displayVal != "" )) {@>
				<a class="u-hide-xs" href="#" rel="tooltip" data-placement="left" data-html="true" data-original-title="<spring:message arguments="<@=EXPLANATION.displayVal@>,<@=EXCLUSION.displayVal@>" code="pd.tooltip.title.explanationAndExclusions" htmlEscape="false"/>">
					<spring:message code="pd.label.title.view"/>
				</a>

				<a class="u-show-inline-block-xs" href="#" rel="tooltip" data-placement="right" data-html="true" data-original-title="<spring:message arguments="<@=EXPLANATION.displayVal@>,<@=EXCLUSION.displayVal@>" code="pd.tooltip.title.explanationAndExclusions" htmlEscape="false"/>">
					<spring:message code="pd.label.title.view"/>
				</a>

			<@} else if(( EXPLANATION.displayVal == null || EXPLANATION.displayVal == "" ) && ( EXCLUSION.displayVal != null && EXCLUSION.displayVal != "" )) {@>
				<a class="u-hide-xs" href="#" rel="tooltip" data-placement="left" data-html="true" data-original-title="<spring:message arguments="<@=EXCLUSION.displayVal@>" code="pd.tooltip.title.limitsAndExclusions" htmlEscape="false"/>">
					<spring:message code="pd.label.title.view"/>
				</a>

				<a class="u-show-inline-block-xs" href="#" rel="tooltip" data-placement="right" data-html="true" data-original-title="<spring:message arguments="<@=EXCLUSION.displayVal@>" code="pd.tooltip.title.limitsAndExclusions" htmlEscape="false"/>">
					<spring:message code="pd.label.title.view"/>
				</a>

			<@} else if(( EXPLANATION.displayVal != null && EXPLANATION.displayVal != "" ) && ( EXCLUSION.displayVal == null || EXCLUSION.displayVal == "" )) {@>

				<a class="u-hide-xs" href="#" rel="tooltip" data-placement="left" data-html="true" data-original-title="<spring:message arguments="<@=EXPLANATION.displayVal@>" code="pd.tooltip.title.benefitExplanation" htmlEscape="false"/>">
					<spring:message code="pd.label.title.view"/>
				</a>

				<a class="u-show-inline-block-xs" href="#" rel="tooltip" data-placement="right" data-html="true" data-original-title="<spring:message arguments="<@=EXPLANATION.displayVal@>" code="pd.tooltip.title.benefitExplanation" htmlEscape="false"/>">
					<spring:message code="pd.label.title.view"/>
				</a>

			<@} @>
		<@ } @>
	</div>
</script>
