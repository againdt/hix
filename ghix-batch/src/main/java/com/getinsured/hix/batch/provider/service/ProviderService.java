package com.getinsured.hix.batch.provider.service;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.ProviderUpload;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;

/**
 * Interface is used to provide services for Provider Service Batch.
 * @since Mar 14, 2018
 */
public interface ProviderService {

	String getDocumentIdByWaitingStatusFromTrackingTable(String providerTypeIndicator);

	void updateTrackingStatusFromFileName(String fileName, ProviderUpload.STATUS status);

	void updateLogsInTrackingTableUsingFileName(String logs, String fileName, int validRecordCount);

	boolean updateTrackingStatusFromInProgressToCompleteOrFail(boolean hasCompleted, String providerTypeIndicator);

	byte[] getContentDataById(String contentId) throws ContentManagementServiceException;

	Content getContentById(String contentId) throws ContentManagementServiceException;
}
