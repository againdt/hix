<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="gutter10">
  <div class="row-fluid">
    <h1>${employer.name }</h1>
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        <h4 class="graydrkbg">Participation Report</h4>
        <ul class="nav nav-list">
          <li class="active"><a href="#">Summary View</a></li>
          <li><a href="<c:url value="participationreport"/>">Detail View</a></li>
        <!--   <li><a href="#">Premiums Paid Report</a></li> -->
        </ul>
      </div>
      <!-- /.sidebar-->
      <div class="span9" id="sidebar">
        <div class="row-fluid">
        	<div class="header"><h4 class="graydrkbg gutter10">Summary View</h4></div>
          
          <div class="gutter10">
     		<h3 class="txt-center black margin0">Participation Summary</h3>
     		<c:if test="">
     		
     		</c:if>
          	<p class="txt-center margin0">Participation required: ${participation_required}% </p>
          	<c:if test="${showWaivedText == 'YES'}"> Participation requirements are waived for coverage that begins on January 1. </c:if>
		     <c:if test="${errorMsg != ''}">
				<p></p>
				<div class="row-fluid"> 
					<div class="alert alert-danger">
							&nbsp;<p class="txt-center margin0"><c:out value="${errorMsg}"></c:out><p/>
						<br>
					</div>
				</div>
			</c:if>
			
			<c:if test="${infoMsg != ''}">
				<p></p>
				<div class="row-fluid"> 
					<div class="alert">
							&nbsp;<p class="txt-center margin0"><c:out value="${infoMsg}"></c:out><p/>
						<br>
					</div>
				</div>
			</c:if>
			
			<c:if test="${errorMsg == '' }">
	          	<p class="txt-center margin0">${employer.name } Participation: ${employeedetails.participationrate}%</p> 
	            <div id="container" style="width:680px; height: 400px; margin: 0 auto"></div>
            </c:if>  
          </div>
        </div><!--row-fluid--> 
      </div><!--rightpanel--> 
    </div><!--row-fluid--> 
  </div><!--main--> 
</div>
<!--gutter10-->
<script type="text/javascript">
		$(function () {
		    var chart;
		    $(document).ready(function() {
		        chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                plotBackgroundColor: null,
		                plotBorderWidth: null,
		                plotShadow: false
		            },
		            title: {
		                text: ''
		            },
		            colors: [
		                 	'#4f81bd',
		                 	'#c0504d',
		                 	'#9bbb59',
		                 	'#8064a2'
		                 	],
		                 	credits: {
		                             enabled: false
		                         },
		            tooltip: {
		                formatter: function() {
		                    return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(1)+' %';
		                }
		            },
		            exporting:{ buttons: {
						 printButton:{ enabled:false },
						 exportButton: { enabled:false }
					 	}
		  			},
		            plotOptions: {
		                pie: {
		                    allowPointSelect: true,
		                    cursor: 'pointer',
		                    dataLabels: {
		                        enabled: true,
		                        color: '#000000',
		                        connectorColor: '#000000',
		                    },
		                    showInLegend: true,
		                }
		            },
		            
		            series: [{
		                type: 'pie',
		                name: 'Employee Coverage: 2013',
		                data: [
		                    ['Enrolled',   ${employeedetails.enrolled}],
		                    ['Waived: Qualifying Coverage ',     ${employeedetails.waived_qualified }],
		                    ['Waived: No Qualified Coverage ',     ${employeedetails.waived_unqualified }],
		                    ['No Response',  ${employeedetails.noresponse }]
		                ]
		            }]
		        });
		    });
		    
		});
		
		//for accessibility
		$(".highcharts-legend").attr("aria-hidden","true");
		</script>
		<script type="text/javascript" src="<c:url value="/resources/js/highcharts.js" />"></script>