import { modalActionTypes } from '../constants/actionTypes';

const initialState = [];

// const initialState = {
//     isShown: false,         // Mandatory. Responsible for Modal appearing on UI
//     content: null,          // Mandatory. It is the reference to certain Modal that has to be on UI
//     data: null,             // Optional. Passed needed data to Modal
//     callbacks: null         // Optional.  Passed needed functions(methods) to Modal
// };

/**
 *
 * @function: modalReducer
 * @param {Object} state: current state of "modal" prop in Redux Store
 * @param {Object} action
 * @returns {Object}: the same or updated or initial state
 */
export default (state = initialState, action) => {
    switch (action.type) {
    /**
     *
     * ActionCreator - showModal
     * This action invokes certain Modal appearance on UI and if it is needed provides data and callbacks in it
     */
    case modalActionTypes.SHOW_MODAL:
        return [
            ...state,
            {
                content: action.content,
                data: action.data ? action.data : null,
                callbacks: action.callbacks ? action.callbacks : null,
                modalOwner: action.modalOwner,
                zIndex: 10000 * (state.length + 1)
            }
        ];

        /**
         *
         * ActionCreator - hideModal
         * This action removes Modal from UI and clears state
         */
    case modalActionTypes.HIDE_MODAL:
        return state.filter(modal => modal.modalOwner !== action.modalOwner);

    case modalActionTypes.HIDE_ALL_MODALS:
        return initialState;

    /**
     *
     * ActionCreator - clearState
     * Returns Redux Store to initial state and modalReducer as part of it as well
     */
    case modalActionTypes.CLEAR_STATE:
        return initialState;

    /**
     *
     * Returns the same value if another reducer was invoked
     *
    */
    default:
        return state;
    }
};