(function (models) {
	models.Plan = Backbone.Model.extend({
		
		urlRoot : "getplans",
		
		url : function () {
			if(this.id)
				return 'getplans/'+this.id+'?'+(Math.random()*10);
			else
				return 'getplans?'+(Math.random()*10);
		},
		
		//to specify default attributes for model
		defaults: {
			//attrName : default value
			"id"					: "",
			"name"					: "",
			"level"					: "",
			"coInsurance"  			: "",
			"deductible" 			: "",
			"officeVisit"			: "",
			"oopMax"				: "",
			"empCount"				: "",
			"premium"				: "",
			"indvPremium"			:"",
			"issuer"				: "",
			"issuerText"			: "",
			"employerTotalCost"		: "",
			"employerIndvCost"		:"",
			"employerDependentCost"	:"",
			"employeeTotalCost"		: "",
			"employerContrHeight"	: "",
			"employeeContrHeight"	: "",
			"employeePayInPreimum"	: "",
			"emplrCostHeight"		:"",
			"empCostHeight"			:"",
			"premiumchanged"		:"",
			"issuerId"				:"",
			"issuerLogo" 			: ""
		}
	});
})(App.models);