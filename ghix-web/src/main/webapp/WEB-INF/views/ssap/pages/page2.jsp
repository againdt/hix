<%@ include file="../include.jsp" %>   
<div id="appscr52" style="display: none;">
        
	<div class="row-fluid">
	    
		<div class="page-title">
			<h5 class="titleForPages"><spring:message code="ssap.page2.upperContent" text="Start Your Application" /></h5>
		</div>
	        
		<div class="container-fluid">
			<div class="control-group">
				<div class="alert alert-info">
					<p class="margin10-t">
						<strong><spring:message code="ssap.page2.upperContent1" /></strong>
						
					</p>
					<p class="margin10-t">
						<spring:message code="ssap.page2.upperContent2" /> 
						
					</p>
				</div>
				<p>
					<em>
						<spring:message code="ssap.page2.upperContent3" text="All fields on this application are required unless otherwise indicated." />  
					</em>
				</p>
				<hr>
			</div>
                    
			<div class="control-group margin10-t">
				<strong><spring:message code="ssap.page2.youMayNeed" text="You may need:" /></strong> 
				<ul class="checklist gutter10">
					<li><spring:message code="ssap.page2.youMayNeed11"/></li>
					<li><spring:message code="ssap.page2.youMayNeed1"/> </li>
					<li><spring:message code="ssap.page2.youMayNeed2"/></li>
					<li><spring:message code="ssap.page2.youMayNeed4" /> </li>
					<li><spring:message code="ssap.page2.youMayNeed3"/> <a href="#" class="ssapTooltip" data-html="true" data-toggle="tooltip" rel="tooltip" data-placement="bottom" title="<strong><spring:message code="ssap.page2.youMayNeed3.1"/></strong><br><spring:message code="ssap.page2.youMayNeed3.tooltip"/>"><spring:message code="ssap.page2.youMayNeed3.1"/></a></li>
					<li class="non_financial_hide"><spring:message code="ssap.page2.youMayNeed5" text="Pay stubs" /></li>
					<li class="non_financial_hide"><spring:message code="ssap.page2.youMayNeed6" text="W-2 Forms" /></li>
					<li class="non_financial_hide"><spring:message code="ssap.page2.youMayNeed7" text="Information about any other income you get" /></li>
					<li class="non_financial_hide"><spring:message code="ssap.page2.youMayNeed8" text="Information about your current health insurance (if you have it) " /></li>
					<li class="non_financial_hide"><spring:message code="ssap.page2.youMayNeed9" text="Information about any job-related insurance you or your family may be able to get, even if you are not enrolled in it." /></li>
				</ul>
			</div>
		
			<!-- 
			<div class="control-group margin30-t">
				<p>
					<strong>
						<img  alt="Time estimate" src="<c:url value="/resources/images/icons/2icon1.png" />" class="arrow_ui_design" />
						<spring:message code="ssap.page2.youMayNeed10" text="Estimated time for this section: 45 Minutes &ndash; 60 Minutes" />
					</strong>
				</p>
			</div>-->
			                  
			          
		</div>
	     
	     
	</div>
</div>
         
          
        