package com.getinsured.hix.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.getinsured.hix.dto.enrollment.EnrollmentPaymentDTO;
import com.getinsured.hix.dto.finance.FinanceRequest;
import com.getinsured.hix.dto.finance.FinanceResponse;
import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.model.AdjustmentReasonCode;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerPaymentInvoice;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.PaymentIsDefault;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.FinanceServiceEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.thoughtworks.xstream.XStream;

/**
 * 
 * Class written to make rest call to Finance module from WEB component.
 * All the rest call to finance module from WEB are written here, and RestCleint response are processed and sent back to WEB component.
 * 
 * @author Sharma_k
 * @since 15th January 2013
 * 
 */
@Component
public class FinanceRestApiUtils 
{
	/*@Autowired private RestTemplate restTemplate;*/
	@Autowired private GhixRestTemplate ghixRestTemplate;
	
	/**
	 * Method to make rest call to Finance module for searching the Non Paid Employer Invoices
	 * @param searchCriteria Map<String, Object> 
	 * @return Map<String,Object>
	 * @Pre Requires EmployerId and PaidStatus as search Criteria
	 * @Post Response will contain the EmployerInvoices Object.
	 */
	/*public Map<String, Object> searchNotPaidEmployerInvoices(Map<String, Object> searchCriteria)
	{
		
		//This map entry is written to cover all invoice fetch conditional
		if(searchCriteria != null && !searchCriteria.isEmpty())
		{
			String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_NOTPAID_INVOICE, searchCriteria, String.class);
			
			XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
			FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
			if(financeResponse != null)
			{
				if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
					throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
				}else {
					return financeResponse.getEmployerInvoicesMap();
				}
			}
		}
		return null;
	}
	
	/**
	 * Method to make rest call for searching the list of Employer Invoices with passed criteria.
	 * @param searchCriteria Map<String, Object>
	 * @return Map<String, Object>
	 * @Pre Requires EmployerId and InvoiceType as searchCriteria
	 * @Post Response will contain the EmployerInvoices Map Object.
	 */
	public Map<String, Object> searchEmployerInvoices(Map<String, Object> searchCriteria)
	{
		if(searchCriteria != null && !searchCriteria.isEmpty())
		{
			String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_INVOICE, searchCriteria, String.class);
			XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
			FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
			if (financeResponse != null) {
				if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
					throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
				}else {
					return financeResponse.getEmployerInvoicesMap();
				}
			}
		}
		return null;
		
	}
	
	/**
	 * Method to make rest call for searching the Paid Employer Invoices
	 * @param searchCriteria Map<String, Object>
	 * @return Map<String, Object>
	 * @Pre Requires EmployerId, PaidStatus, IsActive as search Criteria
	 * @Post Response will contain the EmployerInvoices Object. 
	 */
	/*public Map<String, Object> searchPaidEmployerInvoices(Map<String, Object> searchCriteria)
	{
		if(searchCriteria != null && !searchCriteria.isEmpty())
		{
			String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_PAID_INVOICE, searchCriteria, String.class);
			XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
			FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
			if(financeResponse != null)
			{
				if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
					throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
				}else {
					return financeResponse.getEmployerInvoicesMap();
				}
			}
		}
		return null;
	}*/
	
	/**
	 * Method to make rest call for invoking the PDF creation flow.
	 * @param invoiceId int
	 * @return
	 * @Pre Requires Employer Invoice ID as search Criteria
	 * @Post Response Check the Employer_Invoices table for Ecm_Doc_Id should must not be null for the given InvoiceId.
	 */
	public String createPdf(int invoiceId)
	{
		String get_resp = ghixRestTemplate.getForObject(FinanceServiceEndPoints.CREATE_PDF+invoiceId, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
		if(financeResponse != null)
		{
			if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
			}else {
				return financeResponse.getStatus();
			}
		}
		return null;
	}
	
	/**
	 * Method to make rest call for reissuing the Current Active Invoice
	 * @param searchCriteria Map<String, Object>
	 * @return String 
	 * @Pre Requires Active Employer Invoice ID and EmployerId as search Criteria
	 * @Post Response will contain the reissued EmployerInvoice with new PDF.
	 */
	public String reissueInvoice(Map<String, Object> searchCriteria)
	{
		String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.REISSUE_INVOICE,searchCriteria, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
		if(financeResponse != null)
		{
			if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
			}else {
				return financeResponse.getReissueInvoiceResponse();
			}
		}
		return null;
	}
	
	/**
	 * Method to make rest call for searching Employer Invoices for passed InvoiceId as parameter
	 * @param invoiceId int
	 * @return Object<EmployerInvoices>
	 * @Pre Requires InvoiceID as search Criteria
	 * @Post Response will contain the EmployerInvoices Object.
	 */
	public EmployerInvoices findEmployerInvoicesById(int invoiceId)
	{
		String get_resp = ghixRestTemplate.getForObject(FinanceServiceEndPoints.EMPLOYER_INVOICE+invoiceId, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
		if(financeResponse != null)
		{
			if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
			}else {
				return financeResponse.getEmployerInvoices();
			}
		}
		return null;
	}
	
	/**
	 * Rest call for Processing the Active Employer Invoice Payment.
	 * @param searchCriteria Map<String, Object>
	 * @return String
	 * @Pre Requires EmployerId, InvoiceId and default PaymentMethod ID as search Criteria
	 * @Post Response will the Success or Failure result for the processed payment.
	 */
	public String processPayment(Map<String, Object> searchCriteria)
	{
		String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PROCESS_PAYMENT,searchCriteria, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
		if(financeResponse != null)
		{
			if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
			}else {
				return financeResponse.getProcessPaymentResposne();
			}
		}
		return null;
	}
	
	/**
	 * Rest call to Find the Active Due Employer Invoices on the basis of passed criteria
	 * @param financeRequest Object<FinanceRequest>
	 * @return List<EmployerInvoices>
	 * @Pre Requires financeRequest Object as parameter, which contains the Required information
	 * @Post Response will contain the List of Employer Invoices.
	 */
	public List<EmployerInvoices> findDueEmeployerInvoices(FinanceRequest financeRequest)
	{
		String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.DUE_EMPLOYER_INVOICES,financeRequest, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
		List<EmployerInvoices> employerInvoicesList = new ArrayList<EmployerInvoices>();
		if(financeResponse != null)
		{
			if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
			}else {
				employerInvoicesList = financeResponse.getEmployerInvoicesList();
				return employerInvoicesList;
			}
		}
		return employerInvoicesList;
	}
	
	/**
	 * Rest call to Find list of Employer Invoices which are Active.
	 * @param searchCriteria Map<String, Object>
	 * @return Map<String, Object>
	 * @Pre Requires IsActive flag to fetch Employer Invoices
	 * @Post Response will contain the EmployerInvoices Map object on the basis of passed IsActive Flag.
	 */
	public Map<String, Object> findListOfActiveInvoices(Map<String, Object> searchCriteria)
	{
		String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.ACTIVE_EMPLOYER_INVOICES, searchCriteria, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
		if(financeResponse != null)
		{
			if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
			}else {
				return financeResponse.getEmployerInvoicesMap();
			}
		}
		
		return null;
	}
	
	/**
	 * Rest call to Process Employer Payments, Parameter contains EmployerId, InvoiceId, default PaymentMethod.
	 * @param financeRequest Object<FinanceRequest>
	 * @return Object<FinanceResponse>
	 * @Pre Requires financeRequest for Processing the Employer Payment.
	 * @Post Response will contain the FinanceResponse object with Success or Failure message.
	 */
	public FinanceResponse processPayment(FinanceRequest financeRequest)
	{
		String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PROCESS_PAYMENT,financeRequest, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
		if(financeResponse != null)
		{
			//return financeResponse.getProcessPaymentResposne();
			if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
			}else {
				return financeResponse;
			}
		}
		return null;
	}
	
	/**
	 * Rest client call to find the Employer Payment Invoice for the employers whose invoice is paid.
	 * @param paymentId int
	 * @return Object<EmployerPaymentInvoice>
	 * @Pre Requires EmployerPayment InvoiceID
	 * @Post Response will contain the EmployerPaymentInvoice Object.
	 */
	public EmployerPaymentInvoice findEmployerPaymentInvoiceById(int paymentId)
	{
		String get_resp = ghixRestTemplate.getForObject(FinanceServiceEndPoints.EMPLOYER_PAYMENT_INVOICE+paymentId, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
		if(financeResponse != null)
		{
			if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
			}else {
				return financeResponse.getEmployerPaymentInvoice();
			}
		}
		return null;
	}
	
	/**
	 * Method to find PaymentEventReport details based on the passed search Criteria
	 * @param searchCriteria Map<String, Object>
	 * @return Object<FinanceResponse>
	 * @Post Response will FinanceResponse Object with PaymentEventReport related information
	 */
	public FinanceResponse getPaymentEventReport(Map<String, Object> searchCriteria){
		String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENT_EVENT_REPORT, searchCriteria, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
		if(financeResponse != null)
		{
			//return financeResponse.getProcessPaymentResposne();
			if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
			}else {
				return financeResponse;
			}
		}
		return null;
	}
	
	/**
	 * Rest call to fetch Payment Event Log for the passed MerchantReferenceNumber.
	 * @param merchantRefCode String
	 * @return Object<FinanceResponse>
	 * @Pre Requires MerchantRefereneCode received from Cybersoure 
	 * @Post Response will be FinanceResponse Object which contains the Payment related information.
	 */
	public FinanceResponse getPymtEventLogByMerchantRefNo(String merchantRefCode){
		String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENTEVENTLOG_BY_MERCHANTREFNO, merchantRefCode, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
		if(financeResponse != null)
		{
			//return financeResponse.getProcessPaymentResposne();
			if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
			}else {
				return financeResponse;
			}
		}
		return null;
	}
	
	/**
	 * Method to find the previous Active and DUE employer Invoice.
	 * @param employerId String
	 * @return Object<FinanceResponse>
	 * @Pre Requires EmployerId as search Criteria
	 * @Post Response will contain the Previous Active Due Invoice as per the passed EmployerId
	 */
	public FinanceResponse findPreviousActiveDueInProcessInvoice(String employerId){
		String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PREVIOUSACTIVEDUEINPROCESSINVOICE, employerId, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		return (FinanceResponse) xstream.fromXML(get_resp);
	}
	
	
	/**
	 * Rest call to fetch Payment History for all the paid Employer_Invoices
	 * @param searchCriteria Map<String, Object>
	 * @return  
	 */
	/*public FinanceResponse searchPaymentHistory(Map<String, Object> searchCriteria)
	{
		if(searchCriteria != null && !searchCriteria.isEmpty())
		{
			String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_PAYMENT_HISTORY, searchCriteria, String.class);
			XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
			FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
			if(financeResponse != null)
			{
				if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0)
				{
					throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN
							+ financeResponse.getErrCode(), null,
							financeResponse.getErrMsg(), Severity.LOW);
				}
				else
				{
					return financeResponse;
				}
			}
		}
		return null;
	}*/
	
	/**
	 * Rest call to fetch Invoices for an employer on basis of Employer Id and Ecm Doc Id
	 * @param searchCriteria Map<String, Object>
	 */
	public String searchIncoiceByEmployerNEcmdocId(int employerId, String ecmDocId) {
		Map<String, Object> searchCriteria = new HashMap<String, Object>();
		searchCriteria.put("employerid", employerId);
		searchCriteria.put("ecmDocId", ecmDocId);
		String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_INVOICE_ECMDOC_ID, searchCriteria, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
		if(financeResponse != null)
		{
			if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0)
			{
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN
						+ financeResponse.getErrCode(), null,
						financeResponse.getErrMsg(), Severity.LOW);
			}
		}
		return financeResponse.getStatus();
	}
	
	/**
	 * Method to redirect the Enrollment payment info to Carrier portal.
	 * @param enrollmentPaymentDTO
	 *            {@link EnrollmentPaymentDTO}
	 * @return samlResponse
	 * 			  {@link String}
	 **/
	public String paymentRedirectInfo(EnrollmentPaymentDTO enrollmentPaymentDTO){
		String samlResponse = null;
		String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENT_REDIRECT_DETAILS, enrollmentPaymentDTO, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
		if (financeResponse != null && financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0)
		{
			throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
		}
		else 
		{
			samlResponse = (String)financeResponse.getDataObject().get(GhixConstants.SAML_RESPONSE);
			return samlResponse;
		}
	}
	
	/**
	 * Method to decode the SAML2.0 Assertion response
	 * @param samlResponse
	 *            {@link String}
	 * @return financeResponse
	 * 			  {@link FinanceResponse}
	 **/
	public FinanceResponse decodeSamlResponse(String samlResponse){
		String get_resp = ghixRestTemplate.postForObject(FinanceServiceEndPoints.DECODE_SAML_RESPONSE, samlResponse, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(get_resp);
		if (financeResponse != null && financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
			}else {
				return financeResponse;
			}
	}
	
	/**
	 * 
	 * @return
	 */
	public FinanceResponse updateManualAdjustment(FinanceRequest financeReq)throws GIException
	{
		if(financeReq != null)
		{
			//TODO url for data update
			String get_resp = ghixRestTemplate.postForObject(
					FinanceServiceEndPoints.UPDATE_INVOICE_FOR_MANUAL_ADJUSTMENT, financeReq, String.class);
			XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
			FinanceResponse financeResponse = (FinanceResponse) xstream
					.fromXML(get_resp);
			if (financeResponse!= null && financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE)
					&& financeResponse.getErrCode() != 0)
			{
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN
						+ financeResponse.getErrCode(), null,
						financeResponse.getErrMsg(), Severity.LOW);
			}
			else
			{
				return financeResponse;
			}
		}
		else
		{
			throw new GIException("Received null or Empty parmeters for update");
		}
	}
	
	public List<AdjustmentReasonCode> findAllAdjustmentReasonCode()throws GIException
	{
		String get_resp = ghixRestTemplate.getForObject(FinanceServiceEndPoints.FIND_ALL_ADJUSTMENT_REASONCODE, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream
				.fromXML(get_resp);
		if (financeResponse != null && financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE)
				&& financeResponse.getErrCode() != 0)
		{
			throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN
					+ financeResponse.getErrCode(), null,
					financeResponse.getErrMsg(), Severity.LOW);
		}
		else
		{
			if(financeResponse != null && !financeResponse.getAdjustmentReasonCodeList().isEmpty())
			{
				return financeResponse.getAdjustmentReasonCodeList();
			}
			else
			{
				throw new GIException("Received Null or empty response from Rest Call "+FinanceServiceEndPoints.FIND_ALL_ADJUSTMENT_REASONCODE);
			}
		}
		
	}
	
	/**
	 * This method is used to get employer's active due invoices.
	 * @param employerId
	 * @param isActive
	 * @param paidStatus
	 * @param invoiceType
	 * @return FinanceResponse
	 */
	public FinanceResponse searchActiveDueInvoicesByEmployer(int employerId)
	{
		String xtreamResponse = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_ACTIVE_DUE_INVOICES_BY_EMPLOYER, employerId, String.class);
	//	String xtreamResponse = restTemplate.getForObject(FinanceServiceEndPoints.SEARCH_ACTIVE_DUE_INVOICES_BY_EMPLOYER, String.class, employerId);
		return getFinanceResponseFromXtreamResponse(xtreamResponse);
	}
	
	/**
	 * This is intended to convert  string xtream XML response to FinanceResponse object.
	 * @param xtreamResponse
	 * @return returns FinanceResponse when success response otherwise returns null. 
	 */
	private FinanceResponse getFinanceResponseFromXtreamResponse(String xtreamResponse)
	{
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = (FinanceResponse) xstream.fromXML(xtreamResponse);
		if (financeResponse != null
				&& GhixConstants.RESPONSE_FAILURE.equals(financeResponse
						.getStatus()) && financeResponse.getErrCode() != 0)
		{
			throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
		}
		return financeResponse;
	}
	/**
	 * Method to make rest call for finding PCI compliance payment data from cyber source.
	 * @param paymentId int
	 * @return Object<PaymentMethods> 
	 */
	public PaymentMethods getPaymentMethodByPCI(final int paymentId)
	{
		String get_resp = ghixRestTemplate.getForObject(FinanceServiceEndPoints.FIND_PAYMENT_METHOD_PCI+paymentId, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		PaymentMethodResponse financeResponse = (PaymentMethodResponse) xstream.fromXML(get_resp);
		if(financeResponse != null)
		{
			if (financeResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE) && financeResponse.getErrCode() != 0) {
				throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
			}else {
				return financeResponse.getPaymentMethods();
			}
		}
		return null;
	}
	
	/**
	 * Persist payment method.
	 * @param PaymentMethods paymentMethods
	 * @return Object<PaymentMethods> 
	 */
	public PaymentMethods savePaymentMethods(PaymentMethods newPaymentMethods) 
	{
		String xtreamResponse = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SAVE_PAYMENT_METHOD, newPaymentMethods, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		PaymentMethodResponse financeResponse = (PaymentMethodResponse) xstream.fromXML(xtreamResponse);
		if (financeResponse != null
				&& GhixConstants.RESPONSE_FAILURE.equals(financeResponse.getStatus()) && financeResponse.getErrCode() != 0)
		{
			throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
		}
		return financeResponse.getPaymentMethods();
	}
	
	/**
	 * Modify payment method status or default setting.
	 * @param paymentId int 
	 * @param isdefault PaymentIsDefault
	 * @param status PaymentStatus 
	 * @return Object<PaymentMethods> 
	 */
	public PaymentMethods modifyPaymentStatus(final int paymentId, final PaymentIsDefault isdefault, final PaymentStatus status) 
	{
		PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
		paymentMethodRequestDTO.setId(paymentId);
		paymentMethodRequestDTO.setIsDefaultPaymentMethod(isdefault);
		paymentMethodRequestDTO.setPaymentMethodStatus(status);
		String xtreamResponse = ghixRestTemplate.postForObject(FinanceServiceEndPoints.MODIFY_PAYMENT_STATUS, paymentMethodRequestDTO, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		PaymentMethodResponse financeResponse = (PaymentMethodResponse) xstream.fromXML(xtreamResponse);
		if (financeResponse != null
				&& GhixConstants.RESPONSE_FAILURE.equals(financeResponse.getStatus()) && financeResponse.getErrCode() != 0)
		{
			throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
		}
		return financeResponse.getPaymentMethods();
	}
	
	public PaymentMethodResponse getPaymentMehtodById(int paymentMethodId)
	{
		String xtreamResponse = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENTMETHODS_BY_ID, paymentMethodId, String.class);
		return getPaymentMethodResponseFromXtreamResponse(xtreamResponse);
	}
	
	/**
	 * Converts  string Xtream XML response to PaymentMethodResponse object.
	 */
	private PaymentMethodResponse getPaymentMethodResponseFromXtreamResponse(String xtreamResponse)
	{
		if(StringUtils.hasText(xtreamResponse)){
			XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
			return  (PaymentMethodResponse) xstream.fromXML(xtreamResponse);
		}
		else{
			return null;
		}
	}
	
	public FinanceResponse getInvoiceLineItemsSummaryByInvoice(Map<String, Object> inputMap)
	{
	//	String xtreamResponse = restTemplate.getForObject(GhixEndPoints.FinanceServiceEndPoints.INVOICE_LINE_ITEMS_DTO_BY_INVOICEID+"?filterJson={filterJson}", String.class, json);
		String xtreamResponse = ghixRestTemplate.postForObject(FinanceServiceEndPoints.INVOICE_LINE_ITEMS_DTO_BY_INVOICEID, inputMap, String.class);
		return getFinanceResponseFromXtreamResponse(xtreamResponse);
	}
	
	/**
	 * 
	 * @param paymentMethodRequestDTO
	 * @return
	 */
	public Map<String, Object> searchPaymentMethod(PaymentMethodRequestDTO paymentMethodRequestDTO)
	{
		
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		String restCallResponse = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, paymentMethodRequestDTO, String.class);
		PaymentMethodResponse paymentMethodResponse = (PaymentMethodResponse)xstream.fromXML(restCallResponse);
		if (paymentMethodResponse != null
				&& GhixConstants.RESPONSE_FAILURE.equals(paymentMethodResponse
						.getStatus()) && paymentMethodResponse.getErrCode() != 0)
		{
			throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+paymentMethodResponse.getErrCode(), null, paymentMethodResponse.getErrMsg(), Severity.LOW);
		}
		
		return paymentMethodResponse.getPaymentMethodMap();
	}
	
	/**
	 * Persist payment method.
	 * @param PaymentMethods paymentMethods
	 * @return Object<PaymentMethods> 
	 */
	public String validateCreditCard(final FinancialInfo financialobj) 
	{
		String xtreamResponse = ghixRestTemplate.postForObject(FinanceServiceEndPoints.VALIDATE_CREDITCARD, financialobj, String.class);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		PaymentMethodResponse financeResponse = (PaymentMethodResponse) xstream.fromXML(xtreamResponse);
		if (financeResponse != null
				&& GhixConstants.RESPONSE_FAILURE.equals(financeResponse.getStatus()) && financeResponse.getErrCode() != 0)
		{
			throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+financeResponse.getErrCode(), null, financeResponse.getErrMsg(), Severity.LOW);
		}
		return financeResponse.getResponseString();
	}
		
	public Map retrieveCustomerProfile(final String subscriptionId)
	{
		PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
		
		if(subscriptionId != null)
		{
			paymentMethodRequestDTO.setSubscriptionID(subscriptionId);
		}
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		String restCallResponse = ghixRestTemplate.postForObject(FinanceServiceEndPoints.RETRIEVE_CUSTOMER_PROFILE, paymentMethodRequestDTO, String.class);
		PaymentMethodResponse paymentMethodResponse = (PaymentMethodResponse)xstream.fromXML(restCallResponse);
		if (paymentMethodResponse != null
				&& GhixConstants.RESPONSE_FAILURE.equals(paymentMethodResponse
						.getStatus()) && paymentMethodResponse.getErrCode() != 0)
		{
			throw new GIRuntimeException(GhixConstants.ERROR_SUFFIX_FIN+paymentMethodResponse.getErrCode(), null, paymentMethodResponse.getErrMsg(), Severity.LOW);
		}
		
		return paymentMethodResponse.getResponsedMap();
	}
}
