//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.06.05 at 10:47:57 AM IST 
//


package com.getinsured.iex.erp.org.nmhix.ssa.person;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RetirementOrPension complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetirementOrPension">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="taxableAmount" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="taxableAmountFrequency" type="{http://www.cohbe.org/SSA/Person}IncomeFrequency-Enum" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetirementOrPension", propOrder = {
    "taxableAmount",
    "taxableAmountFrequency"
})

@Entity
@Table(name="ROPension")
public class RetirementOrPension {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

    protected Double taxableAmount;
    protected IncomeFrequencyEnum taxableAmountFrequency;

    /**
     * Gets the value of the taxableAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getTaxableAmount() {
        return taxableAmount;
    }

    /**
     * Sets the value of the taxableAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setTaxableAmount(Double value) {
        this.taxableAmount = value;
    }

    /**
     * Gets the value of the taxableAmountFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link IncomeFrequencyEnum }
     *     
     */
    public IncomeFrequencyEnum getTaxableAmountFrequency() {
        return taxableAmountFrequency;
    }

    /**
     * Sets the value of the taxableAmountFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link IncomeFrequencyEnum }
     *     
     */
    public void setTaxableAmountFrequency(IncomeFrequencyEnum value) {
        this.taxableAmountFrequency = value;
    }

}
