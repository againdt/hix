package com.getinsured.eligibility.at.resp.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.getinsured.eligibility.at.outbound.service.OutboundATApplicationService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.util.IndividualPortalUtil;
import com.getinsured.ssap.util.SsapUtility;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.at.resp.mapper.EligibilityProgramUiDtoMapper;
import com.getinsured.eligibility.ui.dto.EligibilityProgramDTO;
import com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO;
import com.getinsured.eligibility.ui.dto.SsapApplicationEligibilityDTO;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;


@Service("eligibilityService")
@Transactional(readOnly = true)
public class EligibilityServiceImpl implements EligibilityService {

	private static final String APPLICATION_ID_CANNOT_BE_NULL = "applicationId cannot be null";
	private static final Logger LOGGER = LoggerFactory.getLogger(EligibilityServiceImpl.class);
	private static final String YES = "Y";
	private static final String ELIGIBILITY_TRUE = "TRUE";

	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private GIWSPayloadRepository giwsPayloadRepository;
	@Autowired private SsapApplicantRepository ssapApplicantRepository;
	@Autowired private SsapVerificationService ssapVerificationService;
	@Autowired private OutboundATApplicationService outboundATApplicationService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private CmrHouseholdRepository cmrHouseholdRepository;
	@Autowired private SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired private IndividualPortalUtil individualPortalUtil;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Override
	public Map<Long, SsapApplicantEligibilityDTO> getApplicantsEligibilityForApplicationId(Long applicationId) {

		if (null == applicationId) {
			LOGGER.error(APPLICATION_ID_CANNOT_BE_NULL);
			throw new GIRuntimeException(APPLICATION_ID_CANNOT_BE_NULL);
		}

		SsapApplication ssapApplication =  ssapApplicationRepository.findOne(applicationId);

		Map<Long, SsapApplicantEligibilityDTO> result = Collections.emptyMap();

		if (null == ssapApplication){
			return Collections.unmodifiableMap(result);
		}

		List<SsapApplicant> dataList = ssapApplicantRepository.getEligibilitiesForApplicationId(applicationId);
		List<HouseholdMember> members = extractHouseholdMemberJson(ssapApplication);

		if (!dataList.isEmpty()){
			result = processApplicants(ssapApplication, members, dataList);
		}

		return Collections.unmodifiableMap(result);
	}

	private Map<Long, SsapApplicantEligibilityDTO> processApplicants(SsapApplication ssapApplication, List<HouseholdMember> members, List<SsapApplicant> dataList) {
		boolean financialApp = YES.equals(ssapApplication.getFinancialAssistanceFlag());
		LOGGER.debug("processApplicants::financial app {}", financialApp);
		Map<Long, SsapApplicantEligibilityDTO> result = new HashMap<Long, SsapApplicantEligibilityDTO>();

		for (SsapApplicant sa : dataList) {
			SsapApplicantEligibilityDTO ssapApplicantEligibilityDTO = new SsapApplicantEligibilityDTO();

			ssapApplicantEligibilityDTO.setFirstName(sa.getFirstName() != null ? sa.getFirstName() : StringUtils.EMPTY);
			ssapApplicantEligibilityDTO.setMiddleName(sa.getMiddleName() != null ? sa.getMiddleName() : StringUtils.EMPTY);
			ssapApplicantEligibilityDTO.setLastName(sa.getLastName() != null ? sa.getLastName() : StringUtils.EMPTY);
			ssapApplicantEligibilityDTO.setSuffix(sa.getNameSuffix() != null? sa.getNameSuffix() : StringUtils.EMPTY);
			ssapApplicantEligibilityDTO.setPersonId(sa.getPersonId());
			ssapApplicantEligibilityDTO.setApplicantGuid(sa.getApplicantGuid());

			Map<String, String> verificationMap = setVerifications(sa, financialApp);

			if (LOGGER.isDebugEnabled()) {
				verificationMap.forEach((k, v) -> LOGGER.debug("processApplicants::verifications key {} : value {}", k, v));
			}

			ssapApplicantEligibilityDTO.setVerificationMap(verificationMap);

			ssapApplicantEligibilityDTO.setApplyingForCoverage(YES.equals(sa.getApplyingForCoverage()));
			LOGGER.debug("processApplicants::applying for coverage {}", ssapApplicantEligibilityDTO.isApplyingForCoverage());

			HouseholdMember member = extractMember(members, sa);

			ssapApplicantEligibilityDTO.setConditional(verificationMap.containsValue(EligibilityConstants.NOT_VERIFIED) && YES.equals(sa.getApplyingForCoverage()));
			LOGGER.debug("processApplicants::conditional {}", ssapApplicantEligibilityDTO.isConditional());

			ssapApplicantEligibilityDTO.setVerificationCompleted(ssapVerificationService.isApplicantVerified(ssapApplication.getFinancialAssistanceFlag(), sa, member));

			Set<EligibilityProgramDTO> epDTOList = new HashSet<>();

			for (com.getinsured.eligibility.model.EligibilityProgram ep : sa.getEligibilityProgram()) {
				EligibilityProgramDTO epDTO = EligibilityProgramUiDtoMapper.map(ep, ssapApplication.getMaximumAPTC(), ssapApplication.getCsrLevel());

				String eligibilityType = epDTO.getEligibilityType();
				if (eligibilityType.equals("ExchangeEligibilityType")) {
					ssapApplicantEligibilityDTO.setHealthLinkEligible(epDTO.getEligibilityIndicator().equals(ELIGIBILITY_TRUE));
					LOGGER.debug("processApplicants::health link eligible {}", ssapApplicantEligibilityDTO.isHealthLinkEligible());
				} else if ((eligibilityType.equals("AssessedCHIPEligibilityType") || eligibilityType.equals("AssessedMedicaidMAGIEligibilityType")
						|| eligibilityType.equals("AssessedMedicaidNonMAGIEligibilityType")) && epDTO.getEligibilityIndicator().equals(ELIGIBILITY_TRUE)) {
					ssapApplicantEligibilityDTO.setMedicaidAssessed(true);
					LOGGER.debug("processApplicants::medicaid assessed {}", ssapApplicantEligibilityDTO.isMedicaidAssessed());
				}

				epDTOList.add(epDTO);
			}
			ssapApplicantEligibilityDTO.setEligibilities(epDTOList);

			result.put(sa.getId(), ssapApplicantEligibilityDTO);
		}

		return result;
	}

	/**
	 * Checking to see if all DMIs except Income are Verified
	 * If ExchangeEligible is true and all DMIs except Income are verified then DMI table "Eligibility Result"
	 * column should show "Eligible" else "Conditionally Eligible"
	 *
	 * @param verficationMap - Applicant's verification map
	 * @param healthLinkEligible - If applicant is health link eligible
	 * @return boolean
	 */
	private boolean computeDMIConditionalEligibility(Map<String, String> verficationMap, boolean healthLinkEligible) {
		int mapSize = verficationMap.size() - 1;
		int verifiedSize = 0;
		for (String key : verficationMap.keySet()) {
			if (!key.equals("CURRENT_INCOME")) {
				if (verficationMap.get(key).equals(EligibilityConstants.VERIFIED)) {
					verifiedSize++;
				}
			}
		}

		if (healthLinkEligible) {
			return mapSize != verifiedSize;
		}

		return false;
	}

	private HouseholdMember extractMember(List<HouseholdMember> members, SsapApplicant sa) {
		for (HouseholdMember householdMember : members) {
			if (householdMember.getApplicantGuid().equals(sa.getApplicantGuid())) {
				return householdMember;
			}
		}
		return null;
	}

	private List<HouseholdMember> extractHouseholdMemberJson(SsapApplication ssapApplication) {

		List<HouseholdMember> members = new ArrayList<HouseholdMember>();
		SingleStreamlinedApplication ssapJSON = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
		if (ssapJSON.getTaxHousehold() != null && ssapJSON.getTaxHousehold().size() > 0){
			if (ssapJSON.getTaxHousehold().get(0).getHouseholdMember() != null && ssapJSON.getTaxHousehold().get(0).getHouseholdMember().size() > 0){
				members = ssapJSON.getTaxHousehold().get(0).getHouseholdMember();
			}
		}
		return members;
	}

	private Map<Long, SsapApplicantEligibilityDTO> processApplicantsIneligibleEligibility(SsapApplication ssapApplication, List<HouseholdMember> members, List<SsapApplicant> dataList) {

		Map<Long, SsapApplicantEligibilityDTO> result = new HashMap<Long, SsapApplicantEligibilityDTO>();
		for (SsapApplicant sa : dataList) {

			SsapApplicantEligibilityDTO ssapApplicantEligibilityDTO = new SsapApplicantEligibilityDTO();

			ssapApplicantEligibilityDTO.setFirstName(sa.getFirstName() != null ? sa.getFirstName() : StringUtils.EMPTY);
			ssapApplicantEligibilityDTO.setMiddleName(sa.getMiddleName() != null ? sa.getMiddleName() : StringUtils.EMPTY);
			ssapApplicantEligibilityDTO.setLastName(sa.getLastName() != null ? sa.getLastName() : StringUtils.EMPTY);
			ssapApplicantEligibilityDTO.setPersonId(sa.getPersonId());

			boolean financialApp = YES.equals(ssapApplication.getFinancialAssistanceFlag());
			Map<String, String> verificationMap = setVerifications(sa, financialApp);

			ssapApplicantEligibilityDTO.setVerificationMap(verificationMap);

			HouseholdMember member = extractMember(members, sa);
			ssapApplicantEligibilityDTO.setVerificationCompleted(ssapVerificationService.isApplicantVerified(ssapApplication.getFinancialAssistanceFlag(), sa, member));

			Set<EligibilityProgramDTO> epDTOList = new HashSet<>();

			for (com.getinsured.eligibility.model.EligibilityProgram ep : sa.getEligibilityProgram()) {
				if (!StringUtils.equalsIgnoreCase(ep.getEligibilityIndicator(), EligibilityConstants.FALSE)){
					continue;
				}
				EligibilityProgramDTO epDTO = EligibilityProgramUiDtoMapper.map(ep, ssapApplication.getMaximumAPTC(), ssapApplication.getCsrLevel());
				epDTOList.add(epDTO);
			}
			ssapApplicantEligibilityDTO.setEligibilities(epDTOList);

			result.put(sa.getId(), ssapApplicantEligibilityDTO);
		}

		return result;
	}

	// NOT_VERIFIED/NOT VERIFIED and PENDING are considered NOT_VERIFIED otherwise consider the status as VERIFIED
	// NULL will be treated as VERIFIED b/c cases where applicant will not get a status set.
	// For example: Native American is NULL when you do not declare NAT American, INCOME is NULL for all except primary
	private Map<String, String> setVerifications(SsapApplicant sa, boolean isFinancialApplication) {

		Map<String, String> verificationMap = new HashMap<>();
		verificationMap.put("SSN", checkNotVerifiedStatus(sa.getSsnVerificationStatus()) ? EligibilityConstants.NOT_VERIFIED : EligibilityConstants.VERIFIED);
		verificationMap.put("CITIZENSHIP", checkNotVerifiedStatus(sa.getCitizenshipImmigrationStatus()) ? EligibilityConstants.NOT_VERIFIED : EligibilityConstants.VERIFIED);
		verificationMap.put("ELIGIBLE_IMMIGRATION_STATUS", checkNotVerifiedStatus(sa.getCitizenshipImmigrationStatus()) ? EligibilityConstants.NOT_VERIFIED : EligibilityConstants.VERIFIED);
		verificationMap.put("INCARCERATION_STATUS", checkNotVerifiedStatus(sa.getIncarcerationStatus()) ? EligibilityConstants.NOT_VERIFIED : EligibilityConstants.VERIFIED);
		verificationMap.put("RESIDENCY_STATUS", checkNotVerifiedStatus(sa.getResidencyStatus()) ? EligibilityConstants.NOT_VERIFIED : EligibilityConstants.VERIFIED);
		verificationMap.put("DEATH_STATUS", checkNotVerifiedStatus(sa.getDeathStatus()) ? EligibilityConstants.NOT_VERIFIED : EligibilityConstants.VERIFIED);
		verificationMap.put("NAT_AMERICAN_STATUS", checkNotVerifiedStatus(sa.getNativeAmericanVerificationStatus()) ? EligibilityConstants.NOT_VERIFIED : EligibilityConstants.VERIFIED);
		verificationMap.put("VLP_STATUS", checkNotVerifiedStatus(sa.getVlpVerificationStatus()) ? EligibilityConstants.NOT_VERIFIED : EligibilityConstants.VERIFIED);

		// Financial Application - IFSV, Non ESI MEC, MEC
		if (isFinancialApplication) {
			verificationMap.put("CURRENT_INCOME", checkNotVerifiedStatus(sa.getIncomeVerificationStatus()) ? EligibilityConstants.NOT_VERIFIED : EligibilityConstants.VERIFIED);
			verificationMap.put("NON_ESI_MEC_STATUS", checkNotVerifiedStatus(sa.getNonEsiMecVerificationStatus()) ? EligibilityConstants.NOT_VERIFIED : EligibilityConstants.VERIFIED);
			verificationMap.put("MEC_STATUS", checkNotVerifiedStatus(sa.getMecVerificationStatus()) ? EligibilityConstants.NOT_VERIFIED : EligibilityConstants.VERIFIED);
		}

		return verificationMap;
	}

	/**
	 * NOT_VERIFIED/NOT VERIFIED and PENDING are considered NOT_VERIFIED for verification statuses
	 *
	 * @param status - Verification Status
	 * @return boolean true if input status matches one of the Strings that represents not verified
	 */
	@Override
	public boolean checkNotVerifiedStatus(String status) {
		return status != null && EligibilityConstants.NOT_VERIFIED_STATUS_LIST.stream().anyMatch(status::equalsIgnoreCase);
	}

	@Override
	public SsapApplicationEligibilityDTO getApplicationDataApplicationId(Long applicationId) {

		if (null == applicationId) {
			LOGGER.error(APPLICATION_ID_CANNOT_BE_NULL);
			throw new GIRuntimeException(APPLICATION_ID_CANNOT_BE_NULL);
		}

		SsapApplication ssapApplication = ssapApplicationRepository.findOne(applicationId);

		Map<Long, SsapApplicantEligibilityDTO> result = Collections.emptyMap();

		SsapApplicationEligibilityDTO output = new SsapApplicationEligibilityDTO();

		if (null == ssapApplication){
			return output;
		}

		List<SsapApplicant> dataList = ssapApplicantRepository.getEligibilitiesForApplicationId(applicationId);

		List<HouseholdMember> members = extractHouseholdMemberJson(ssapApplication);
		if (!dataList.isEmpty()){
			result = processApplicants(ssapApplication, members, dataList);
		}

		List<SsapApplicantEligibilityDTO> finalList = new ArrayList<SsapApplicantEligibilityDTO>();

		for (Long ssapApplicantId : result.keySet()) {
			finalList.add(result.get(ssapApplicantId));
		}


		output.setSsapApplicantList(finalList);
		output.setApplicationStatus(ssapApplication.getApplicationStatus());
		output.setPrimaryKey(ssapApplication.getId());
		output.setCaseNumber(ssapApplication.getCaseNumber());
		output.setSource(ssapApplication.getSource());
		output.setCoverageYear(ssapApplication.getCoverageYear());
		output.setApplicationStatus(ssapApplication.getApplicationStatus());
		output.setEligibilityStatus(ssapApplication.getEligibilityStatus());
		output.setExchangeEligibilityStatus(ssapApplication.getExchangeEligibilityStatus());
		output.setCsrLevel(ssapApplication.getCsrLevel());
		output.setMaxAPTCAmount(ssapApplication.getMaximumAPTC());
		output.setMaxStateSubsidy(ssapApplication.getMaximumStateSubsidy());
		output.setElectedAPTCAmount(ssapApplication.getElectedAPTC());
		output.setEnableEnrollment(ssapApplication.getAllowEnrollment());
		output.setEligibilityReceivedDate(ssapApplication.getEligibilityReceivedDate());
		output.setEligibilityResponseType(ssapApplication.getEligibilityResponseType());
		output.setNativeAmericanHousehold(ssapApplication.getNativeAmerican());
		output.setPrimaryFirstName(ssapApplication.getEsignFirstName());
		output.setPrimaryMiddleName(ssapApplication.getEsignMiddleName());
		output.setPrimaryLastName(ssapApplication.getEsignLastName());

		return output;
	}

	@Override
	public SsapApplicationEligibilityDTO getApplicationData(String caseNumber) {
		if (null == caseNumber) {
			LOGGER.error(APPLICATION_ID_CANNOT_BE_NULL);
			throw new GIRuntimeException(APPLICATION_ID_CANNOT_BE_NULL);
		}

		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumberOne(caseNumber);
		SsapApplication ssapApplication;
		if (ssapApplicationList != null && ssapApplicationList.size() > 0){
			ssapApplication = ssapApplicationList.get(0);
		} else {
			throw new GIRuntimeException("Unable to find ssap application for case number - " + caseNumber);
		}

		Map<Long, SsapApplicantEligibilityDTO> result = Collections.emptyMap();

		SsapApplicationEligibilityDTO output = new SsapApplicationEligibilityDTO();
		output.setOutboundATSent(outboundATApplicationService.isOuboundATSent(ssapApplication.getId()));
		LOGGER.debug("getApplicationData::outbound AT sent {}", output.isOutboundATSent());
		if (!output.isOutboundATSent()) {
			LOGGER.debug("getApplicationData::outbound not sent checking gi ws payload");
			List<GIWSPayload> giwsPayload = giwsPayloadRepository.findBySsapApplicationId(ssapApplication.getId(), "ACCOUNT-TRANSFER-OUTBOUND");
			output.setOutboundATSent(giwsPayload != null && !giwsPayload.isEmpty());
			LOGGER.debug("getApplicationData::giwsPayload exists {}", output.isOutboundATSent());
		}

		output.setApplicationType(ssapApplication.getApplicationType());
		int year = TSCalendar.getInstance().get(Calendar.YEAR);
		output.setInsideOEWindow(individualPortalUtil.isInsideOEWindow(year));

		List<SsapApplicationEvent> ssapApplicationEvents = ssapApplicationEventRepository.findBySsapApplication(ssapApplication);
		// Application is SEP Eligible only if application type is SEP or QEP and application is outside OE window and
		// application Exchange Eligibility Status is not None and the current date is within the SSAP Application Events Enrollment start date and end date
		if (ssapApplicationEvents != null && !ssapApplicationEvents.isEmpty() && ssapApplicationEvents.get(0).getEnrollmentStartDate() != null
			&& ssapApplicationEvents.get(0).getEnrollmentEndDate() != null
			&& DateUtil.checkIfInBetween(ssapApplicationEvents.get(0).getEnrollmentStartDate(), ssapApplicationEvents.get(0).getEnrollmentEndDate(), new TSDate())
			&& ssapApplication.getApplicationType().equalsIgnoreCase("SEP") || ssapApplication.getApplicationType().equalsIgnoreCase("QEP")
			&& !ssapApplication.getExchangeEligibilityStatus().equals(ExchangeEligibilityStatus.NONE) && !output.isInsideOEWindow()) {

			// Application are SEP Eligible
			output.setSepEligible(true);
		} else {
			output.setSepEligible(false);
		}


		Household household = cmrHouseholdRepository.findOne(ssapApplication.getCmrHouseoldId().intValue());
		if (household != null) {
			LOGGER.debug("getApplicationData::household case id {}", household.getHouseholdCaseId());
			output.setHouseholdCaseId(household.getHouseholdCaseId());
		}

		try {
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			if(!stateCode.equals("MN")) {
				BigDecimal individualId = ssapApplication.getCmrHouseoldId();
				String brokerDesignatedName = ghixRestTemplate.getForObject(GhixEndPoints.BrokerServiceEndPoints.GET_BROKER_DESIGNATED_NAME + "/" + individualId, String.class);
				output.setBrokerName(brokerDesignatedName);
				LOGGER.debug("getApplicationData::broker name {}", output.getBrokerName());
			}
		} catch (Exception ex) {
			LOGGER.error("getApplicationDataApplicationId::error calling GET_BROKER_DESIGNATED_NAME API", ex);
		}

		List<SsapApplicant> dataList = ssapApplicantRepository.getApplicantEligibilitiesForApplicationId(ssapApplication.getId());
		List<HouseholdMember> members = extractHouseholdMemberJson(ssapApplication);

		/* process eligible eligibility */
		if (!dataList.isEmpty()) {
			result = processApplicants(ssapApplication, members, dataList);
		}

		List<SsapApplicantEligibilityDTO> finalList = new ArrayList<SsapApplicantEligibilityDTO>();

		for (Long ssapApplicantId : result.keySet()) {
			finalList.add(result.get(ssapApplicantId));
		}

		// Setting the global variables on application that depend on applicant level values (Ex: If an applicant has an
		// Income DMI then the whole App has at least one Income DMI)
		setApplicationVariables(output, finalList, ssapApplication);
		output.setApplicationEligibilityStatus(ssapApplication.getEligibilityStatus().getDescription());

		/* process ineligible eligibility */
		if (!dataList.isEmpty()) {
			result = processApplicantsIneligibleEligibility(ssapApplication, members, dataList);
		}

		List<SsapApplicantEligibilityDTO> finalIneligibleEligibilityList = new ArrayList<SsapApplicantEligibilityDTO>();

		// Final list currently holds all applicants and their eligibility's
		List<SsapApplicantEligibilityDTO> finalEligibleEligibilityList = new ArrayList<>(finalList);

		for (Long ssapApplicantId : result.keySet()) {
			finalIneligibleEligibilityList.add(result.get(ssapApplicantId));
		}

		output.setSsapApplicantList(finalList);
		output.setSsapApplicantEligibleEligibilityDTO(finalEligibleEligibilityList);	/* eligible section */
		output.setSsapApplicantIneligibleEligibilityDTO(finalIneligibleEligibilityList); /* in-eligible section */
		output.setApplicationStatus(ssapApplication.getApplicationStatus());
		output.setPrimaryKey(ssapApplication.getId());
		output.setCaseNumber(ssapApplication.getCaseNumber());
		output.setSource(ssapApplication.getSource());
		output.setCoverageYear(ssapApplication.getCoverageYear());
		output.setApplicationStatus(ssapApplication.getApplicationStatus());
		output.setEligibilityStatus(ssapApplication.getEligibilityStatus());
		output.setExchangeEligibilityStatus(ssapApplication.getExchangeEligibilityStatus());
		output.setCsrLevel(ssapApplication.getCsrLevel() != null ? ssapApplication.getCsrLevel() : StringUtils.EMPTY);
		output.setMaxAPTCAmount(ssapApplication.getMaximumAPTC());
		LOGGER.debug("getApplicationData::max APTC Amount {}", output.getMaxAPTCAmount());
		output.setMaxStateSubsidy(ssapApplication.getMaximumStateSubsidy());
		output.setElectedAPTCAmount(ssapApplication.getElectedAPTC());
		output.setEnableEnrollment(ssapApplication.getAllowEnrollment() != null ? ssapApplication.getAllowEnrollment() : StringUtils.EMPTY);
		output.setCmrHouseholdId(ssapApplication.getCmrHouseoldId());

		output.setEligibilityReceivedDate(ssapApplication.getEligibilityReceivedDate());
		output.setEligibilityResponseType(ssapApplication.getEligibilityResponseType());
		output.setNativeAmericanHousehold(ssapApplication.getNativeAmerican());

		output.setPrimaryFirstName(ssapApplication.getEsignFirstName());
		output.setPrimaryMiddleName(ssapApplication.getEsignMiddleName());
		output.setPrimaryLastName(ssapApplication.getEsignLastName());

		return output;
	}

	/**
	 * Loops through all applicants and sets top level application DTO values, SsapApplicationEligibilityDTO.
	 * If applicant is applying for coverage then check additional information.
	 * If applicant is health link eligible then check additional information.
	 *
	 * @param output - Top level application DTO passed to notice templates
	 * @param finalList - List of applicant DTOs
	 */
	private void setApplicationVariables(SsapApplicationEligibilityDTO output, List<SsapApplicantEligibilityDTO> finalList, SsapApplication ssapApplication) {
		final SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
		HouseholdMember householdMember = new HouseholdMember();
		if (singleStreamlinedApplication != null) {
			householdMember = SsapUtility.getPrimaryTaxHouseholdMember(singleStreamlinedApplication.getTaxHousehold().get(0));
		}

		for (SsapApplicantEligibilityDTO applicant : finalList) {
			if (applicant.getApplicantGuid() != null && householdMember.getApplicantGuid().equals(applicant.getApplicantGuid())) {
				if (applicant.getVerificationMap().containsKey("CURRENT_INCOME")) {
					String incomeDMIStatus = applicant.getVerificationMap().get("CURRENT_INCOME");
					LOGGER.debug("setApplicationVariables::income DMI status for applicant id 1: {}", incomeDMIStatus);
					if (incomeDMIStatus.equals(EligibilityConstants.NOT_VERIFIED)) {
						output.setIncomeDMIExists(true);
					}
				}
			}
			LOGGER.debug("setApplicationVariables::Income DMI Exists: {}", output.isIncomeDMIExists());

			if (!applicant.isApplyingForCoverage()) {
				LOGGER.debug("setApplicationVariables::contains not seeking coverage: true");
				output.setContainsNotSeekingCoverage(true);
			} else {
				if (applicant.isHealthLinkEligible()) {
					output.setContainsHealthLinkEligible(true);
					LOGGER.debug("setApplicationVariables::Contains Health Link Eligible: true");

					if (!output.isNonIncomeDMIExists()) {
						for (String verification : applicant.getVerificationMap().keySet()) {
							if (!verification.equals("CURRENT_INCOME") && applicant.getVerificationMap().get(verification).equals(EligibilityConstants.NOT_VERIFIED)) {
								output.setNonIncomeDMIExists(true);
								break;
							}
						}
					}
					LOGGER.debug("setApplicationVariables::Non-Income DMI Exists: {}", output.isNonIncomeDMIExists());

					if (!output.isContainsMedicaidCHIPAssessed()) {
						for (EligibilityProgramDTO eligibilityProgramDTO : applicant.getEligibilities()) {
							if (eligibilityProgramDTO.getEligibilityIndicator().equals("TRUE") && (eligibilityProgramDTO.getEligibilityType().equals("AssessedCHIPEligibilityType") ||
									eligibilityProgramDTO.getEligibilityType().equals("AssessedMedicaidMAGIEligibilityType") ||
									eligibilityProgramDTO.getEligibilityType().equals("AssessedMedicaidNonMAGIEligibilityType"))) {
								output.setContainsMedicaidCHIPAssessed(true);
								break;
							}
						}
					}
					LOGGER.debug("setApplicationVariables::Contains Medicaid/CHIP Assessed: {}", output.isContainsMedicaidCHIPAssessed());
				} else {
					LOGGER.debug("setApplicationVariables::Contains Health Link In-eligible: true");
					output.setContainsHealthLinkInEligible(true);
				}
			}
		}
	}
}
