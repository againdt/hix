<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>

				<div class="gutter10-lr">
					<div class="row-fluid">
						<h1><a name="skip"></a>${assister.firstName} ${assister.lastName}</h1>
					</div>
	<c:set var="assisterId" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
		<form class="form-vertical" id="frmviewassisterprofile" name="frmviewassisterprofile" action="viewprofile" method="POST">
		
			<div id="sidebar" class="span3">
					<div class="header"></div>
					<ul class="nav nav-list">
						<li><a href="/hix/entity/entityadmin/assisterinformation?assisterId=${assisterId}"><spring:message code="label.assister.certifiedCounselor"/></a></li>
						<li  class="active"><a href="#"><spring:message code="label.assister.profile"/></a></li> 
						<c:choose>	
							<c:when test="${loggedUser=='entityAdmin' }">
								<li><a href="/hix/entity/assisteradmin/certificationstatus?assisterId=${assisterId}"><spring:message code="label.assister.certificationStatus"/></a></li>
							</c:when>
						</c:choose>
						<li><a href="/hix/entity/entityadmin/assisterstatus?assisterId=${assisterId}"><spring:message code="label.assister.status"/></a></li>
					</ul>
			</div>
			<!-- end of span3 -->
			<div class="span9">
				<div class="content-header header"><h4 class="pull-left"><spring:message code="label.profile"/></h4>
				</div>
				<div class="row-fluid gutter10-tb">
					<div class="span">
						<table class="table table-border-none span6">
							<tbody>
								<tr>
									<td class="span2 text-right"><spring:url value="/entity/assisteradmin/photo/${assisterId}"
											var="photoUrl" /> <img src="${photoUrl}" alt="Profile image of Counselor"
										class="profilephoto thumbnail" />
									</td>
									<td>
										<h4>${assister.firstName} ${assister.lastName}</h4>
										<p>
											${assister.mailingLocation.address1}<br>
											<c:if test="${assister.mailingLocation.address2!=null}">
												${assister.mailingLocation.address2}<br> 
											</c:if>
											${assister.mailingLocation.city}<br>
											${assister.mailingLocation.state}<br>
											${assister.mailingLocation.zip}
										</p>
									</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.assister.phoneNumber"/></td>
									<td>(${primaryPhone1})${primaryPhone2}-${primaryPhone3}</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.assister.emailAddress"/></td>
									<td>${assister.emailAddress}</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.assister.languageSpoken"/></td>
									<td class="comma">${assisterLanguages.spokenLanguages}</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.assister.languageWritten"/></td>
									<td class="comma">${assisterLanguages.writtenLanguages}</td>
								</tr>
								<c:if test="${CA_STATE_CODE == false}">
									<tr>
										<td class="txt-right"><spring:message code="label.assister.education"/></td>
										<td>${assister.education}</td>
									</tr>
								</c:if>
							</tbody>
						</table>
						<div class="span5 gutter10">
							<iframe width="190" height="190" frameborder="0" scrolling="no"
								marginheight="0" marginwidth="0" class="thumbnail"
								src="//maps.google.com/maps?oe=utf-8&amp;client=firefox-a&amp;q=${assister.mailingLocation.zip}+map&amp;ie=UTF8&amp;hq=&amp;hnear=${assister.mailingLocation.address1}+${assister.mailingLocation.city},+${assister.mailingLocation.state}+${assister.mailingLocation.zip}&amp;gl=us&amp;t=m&amp;ll=${assister.mailingLocation.lat},${assister.mailingLocation.lon}&amp;z=13&amp;iwloc=A&amp;output=embed" title="Assister Location"><spring:message code="label.assister.assister.location"/></iframe>
						</div>
					</div>
				</div>
				<!-- Showing comments -->
				<%-- 							<comment:view targetId="${broker.id}" targetName="BROKER"> --%>
				<%-- 							</comment:view> --%>
			</div>
		</form>
	</div>
	<!-- #row-fluid -->
</div>

<script type="text/javascript">
	$(".comma").text(function(i, val) {
	    return val.replace(/,/g, ", ");
	});
</script>