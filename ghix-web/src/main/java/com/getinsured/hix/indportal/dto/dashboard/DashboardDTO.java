package com.getinsured.hix.indportal.dto.dashboard;

import java.util.List;

public class DashboardDTO {

	private DashboardConfigurationDTO configuration;
	private List<DashboardTabDTO> tabs;

	public DashboardConfigurationDTO getConfiguration() {
		return configuration;
	}

	public void setConfiguration(DashboardConfigurationDTO configuration) {
		this.configuration = configuration;
	}

	public List<DashboardTabDTO> getTabs() {
		return tabs;
	}

	public void setTabs(List<DashboardTabDTO> tabs) {
		this.tabs = tabs;
	}

}
