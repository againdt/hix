<style type="text/css">
body {
  background: #FFF;
}
.waitinganimation {
  position: relative;
}
.waitingatext {
  margin: 0 0 0 100px;
}
.waitingcenter {
    background-color: none;
    border: 8px solid orange;
    opacity: 1;
    border-top: 8px solid #FFF;
    border-left: 8px solid #FFF;
    border-radius: 100%;
    width: 50px;
    height: 50px;
    position: absolute;
    -moz-animation: spinoff 1s infinite linear;
    -webkit-animation: spinoff 1s infinite linear;
}

.waitingmiddle {
    background-color: none;
    border: 8px solid orange;
    opacity: 1;
    border-top: 8px solid #FFF;
    border-left: 8px solid #FFF;
    border-radius: 100%;
    width: 20px;
    height: 20px;
    margin: 0 auto;
    position: absolute;
    top: 15px;
    left: 15px;
    z-index: 5;
    -moz-animation: spin 1s infinite linear;
    -webkit-animation: spin 1s infinite linear;
}

@-webkit-keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(360deg);
    };
}


@-webkit-keyframes spinoff {
    0% {
        -webkit-transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(-360deg);
    };
}
</style>
<script type="text/ng-template" id="review-dummy.jsp">
  <form class="form-horizontal" id="signAndSubmit" name="signAndSubmit" ng-controller="signAndSubmit" novalidate>
  <div class="gutter10" id="esign">
    <h3><spring:message code="label.lce.sighandsubmit"/></h3>

    <h4><spring:message code="label.lce.review.p1"/></h4>


      <div class="control-group margin20-l">
        <p><spring:message code="ssap.page35.incomeDataAgreementNFA" /> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
		<span class="validation-error-text-container" ng-if="submitted && signAndSubmit.futureApplication.$error.required">Required</span>
		</p>
        <div class="margin10-l">
          <label>
            <input type="radio" ng-model="jsonObject.singleStreamlinedApplication.consentAgreement" name="futureApplication" ng-value="true" required ng-click="jsonObject.singleStreamlinedApplication.numberOfYearsAgreed='';"> I agree
          </label>
          <label>
            <input type="radio" ng-model="jsonObject.singleStreamlinedApplication.consentAgreement" name="futureApplication" ng-value="false" required > I disagree
          </label>

        </div>



      </div>

      <div class="control-group margin40-l margin30-t" id="taxReturnPeriodDiv" style="display: block;" ng-if="jsonObject.singleStreamlinedApplication.consentAgreement==false">
        <div class="margin10-l">
          <p>I give my permission to Your Health Idaho to renew my application for a period of: <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><a><span aria-label="Required!"></span></a>
		  <span class="validation-error-text-container" ng-if="submitted && signAndSubmit.renewalPeriod.$error.required">Required</span></p>
          <label class="capitalize-none">
            <input type="radio" name="renewalPeriod" ng-model="jsonObject.singleStreamlinedApplication.numberOfYearsAgreed" ng-value="1" id="renewalPeriod1" ng-required="jsonObject.singleStreamlinedApplication.consentAgreement==false"> 1 year
          </label>
          <label class="capitalize-none">
            <input type="radio" name="renewalPeriod" ng-model="jsonObject.singleStreamlinedApplication.numberOfYearsAgreed" ng-value="2" id="renewalPeriod2" ng-required="jsonObject.singleStreamlinedApplication.consentAgreement==false">  2 years
          </label>
          <label class="capitalize-none">
            <input type="radio" name="renewalPeriod" ng-model="jsonObject.singleStreamlinedApplication.numberOfYearsAgreed" ng-value="3" id="renewalPeriod3" ng-required="jsonObject.singleStreamlinedApplication.consentAgreement==false">  3 years
          </label>
          <label class="capitalize-none">
            <input type="radio" name="renewalPeriod" ng-model="jsonObject.singleStreamlinedApplication.numberOfYearsAgreed" ng-value="4" id="renewalPeriod4" ng-required="jsonObject.singleStreamlinedApplication.consentAgreement==false">  4 years
          </label>
          <label class="capitalize-none">
            <input type="radio" name="renewalPeriod" ng-model="jsonObject.singleStreamlinedApplication.numberOfYearsAgreed" ng-value="5" id="renewalPeriod5" ng-required="jsonObject.singleStreamlinedApplication.consentAgreement==false" > 5 years
          </label>

        </div>


      </div>

      <div class="control-group">
        <div class="">
          <label class="checkbox inline">
            <input type="checkbox" name="agree_stat4" ng-model="agree_stat4" value="medical_expense"  required>
            If any of the information in this application changes during the course of the year, you will have 60 days to notify Your Health Idaho. Changes to your household size, address or other details might affect your eligibility for specific benefits. I understand and will notify Your Health Idaho if my application information changes.
            <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><a><span aria-label="Required!"></span></a>
			<span class="validation-error-text-container" ng-if="submitted && signAndSubmit.agree_stat4.$error.required">Required</span>
           </label>
        </div>
        <div class="">
          <label class="checkbox inline">
            <input type="checkbox" name="agree_stat5" ng-model="agree_stat5" value="medical_expense" required>
            I am signing this application under penalty of perjury. This means I have provided true answers to all the questions on this form to the best of my knowledge. I know that if I am not truthful, I may be subject to a penalty.
            <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><a><span aria-label="Required!"></span></a>
           <span class="validation-error-text-container"  ng-if="submitted && signAndSubmit.agree_stat5.$error.required">Required</span>
     </label>
        </div>
      </div>
      </div>

    <div class="gutter10">

        <div class="control-group">
          <label class="control-label" for="txtESignName">
            <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong><spring:message code="label.lce.selectronicsignature"/>
            <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><a><span aria-label="Required!"></span></a>
          </label>
          <div class="controls">
            <input type="text" class="span6 margin10-t" id="txtESignName" name="eSignName" ng-model="eSignName" required ng-keyup="validationESignName(householdMember);" ng-change="validationESignName(householdMember);" />

          </div>
    			<div class="controls">
    		  		<span class="validation-error-text-container"  ng-if="submitted && signAndSubmit.eSignName.$error.required">Please enter Electronic Signature.</span>
    		  		<span class="validation-error-text-container"  ng-if="submitted && signAndSubmit.eSignName.$error.validESignature">Please enter valid Electronic Signature.</span>
    			</div>
        </div>
    </div>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <!-- <a role="button" tabindex="0" class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a> -->
      <a role="button" tabindex="0" ng-if="!signAndSubmit.$valid" class="btn btn-primary" ng-click="$parent.submitted=true"><spring:message code="label.lce.save"/></a>
      <a role="button" tabindex="0" id="saveDataAndEvents1" ng-if="signAndSubmit.$valid && !saveDisabled" class="btn btn-primary" ng-click="toggleModal();saveSSAPJSON();" ><spring:message code="label.lce.save"/></a>
	    <a role="button" tabindex="0" id="saveDataAndEvents2" ng-if="signAndSubmit.$valid && saveDisabled" class="btn btn-secondary" ng-disabled="true"><spring:message code="label.lce.save"/></a>
    </dl>
  </div>

  <!-- MODAL THAT POPS UP WHEN USER WANT TO SAVE  -->
  <modal-saving show='modalShown'>
      <form class="form-horizontal ng-scope ng-pristine ng-valid">
          <div class="gutter40 margin5-t">
            <div class="waitinganimation">
              <div class="waitingcenter"></div>
              <div class="waitingmiddle"></div>
            </div>
            <div class="waitingatext">
              <h1>Please wait while your updated application is saving!</h1>
            </div>
          </div>
      </form>
  </modal-saving>

  <modal-Tiny show='showNotificationForNoChange' ng-model="modalHouseholdMember">
           <div class="gutter20">
                   <div>
						You haven't made any changes to application.  
                   </div>
				  <div class="pull-right">
                            <button class="btn btn-secondary" ng-click="$root.showNotificationForNoChange=false;">
                                Ok
                            </button>
    			  </div>
		</div>	
   </modal-Tiny>

</form>
</script>