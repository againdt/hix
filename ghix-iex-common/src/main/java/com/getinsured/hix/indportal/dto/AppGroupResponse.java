package com.getinsured.hix.indportal.dto;

import java.util.List;

public class AppGroupResponse {
	public enum GroupType {
		HEALTH, DENTAL, HEALTH_N_DENTAL;
	}

	public enum ProgramType {
		H, D, A;
	}

	private List<Group> groupList;
	private Integer status;
	private String message;
	private String applicationType;
	private Long applicationId;
	private boolean isInsideOEEnrollmentWindow;
	private String serverDate;
	private boolean isRenewalApplication = false;
	private Integer coverageYear;
	

	public Integer getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}

	public boolean isRenewalApplication() {
		return isRenewalApplication;
	}

	public void setRenewalApplication(boolean isRenewalApplication) {
		this.isRenewalApplication = isRenewalApplication;
	}

	public List<Group> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<Group> groupList) {
		this.groupList = groupList;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public boolean isInsideOEEnrollmentWindow() {
		return isInsideOEEnrollmentWindow;
	}

	public void setInsideOEEnrollmentWindow(boolean isInsideOEEnrollmentWindow) {
		this.isInsideOEEnrollmentWindow = isInsideOEEnrollmentWindow;
	}

	public String getServerDate() {
		return serverDate;
	}

	public void setServerDate(String serverDate) {
		this.serverDate = serverDate;
	}
	
}
