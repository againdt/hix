package com.getinsured.hix.batch.externalnotices.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.ExternalNotice;


@Component("externalNoticesFailureReportProcessor")
@Scope("step")
public class ExternalNoticesFailureReportProcessor implements ItemProcessor<ExternalNotice, ExternalNotice> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalNoticesFailureReportProcessor.class);

	@Override
	public ExternalNotice process(ExternalNotice externalNotice) throws Exception {
		LOGGER.info("Id received for processing : " + externalNotice.getId());
		return externalNotice;
	}
}
