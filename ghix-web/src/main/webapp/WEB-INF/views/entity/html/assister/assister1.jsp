<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]--><head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Enrollment Entities | Assister Registration</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">

  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/responsive.css">
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  	<![endif]-->
</head>

 
<div id="main">
 	<div class="container">
		<div class="row-fluid">
	
		<!-- #sidebar -->      
        <div class="span3">
          <div id="sidebar" class="affix">
         	<div class="header"></div>
				<jsp:include page="../leftNavigationMenu.jsp" />
          </div><!-- #sidebar .affix --> 
        </div><!-- .span3 --> 
    <!-- #sidebar ENDS -->
	
		
    
     	<div class="span9">
          	<div id="section">
          		<h4 class="header-container" id="addAssisterStepHeader">Step 4: Assisters</h4>
    
	             <div class="control-group">
	               <a class="btn btn-primary"  id="addAssister" onclick="$('#frmAssister').show()">Add Assister</a>           
	             </div>
	            <div class="control-group">  
	              <table class="table table-striped table-bordered">
						<tr>				                     
			              <td><a class="btn btn-primary" id="assisterDone" onclick="window.location.href='/hix/entity/enrollmententity/paymentinfo'">Done></a></td>   
			              <td width="50"><a class="btn btn-primary" id="assisterBack" onclick="window.location.href='/hix/entity/enrollmententity/entitycontactinfo'">Back</a></td>     
				        </tr>	
				   </table>	           
             </div>
	            
	            <c:if test="${assisterList.size() != 0}"> 
		             <h5  class="header-container" id="assisterHeader">Assister Roster</h5>
		            	<div id="assisterTable" class="control-group">
		          			<div id="section">		 			
						         <table class="table table-striped table-bordered" id="siteList">
						         	<tr>				                     
				                      <th>Name</th>	
				                      <th>Site</th>
				                    </tr>				                  
				                  	<tbody>
						               	<c:forEach items="${assisterList}" var="assister">
						                    <tr>
						                        <td><c:out value="${assister.name}" /></td>			                    	
						                  		<td><c:out value="${assister.primaryAssisterSite.siteLocationName}" /></td>
						                 		<td>
						                 		<div class="dropdown">
						                 		<a class="dropdown-toggle" data-toggle="dropdown" href="#" ><i class="icon-cog"></i><i class="caret"></i></a>
													<ul class="dropdown-menu"><li><a id ="editId" href="<c:url value="/entity/assister/registration?assisterId=${assister.id}"/>"class="offset1">
													<i class="icon-pencil"></i>Edit</a></li></ul>
												</div>									
												</td>
						                  </tr>
					                  	</c:forEach>
				                 </tbody>
				             </table>
						     </div>
						</div>
	            </c:if>
 	
				<form class="form-horizontal gutter10" style="display:none" id="frmAssister" name="frmAssister" action="registration" method="POST">
						<div class="graydrkaction">
							<h4 class="span10">NEW ASSISTER FORM</h4>
						</div>
						<div class="control-group">
							<label for="name" class="required control-label" id="name">Name <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<c:choose>
									<c:when test="${assister.name != null && assister.name != ''}">
										<input type="text" name="name_readonly" id="name" value="${assister.name}" readonly class="input-xlarge" size="30" />
										<input type="hidden" name="name_hidden" id="name_hidden" value="${assister.name}" />
									</c:when>
									<c:otherwise>
										<input type="text" name="name" id="name" value="${assister.name}" class="input-xlarge" size="30" />
									</c:otherwise>
								</c:choose>
								<div id="name_error"></div>
					       </div>
						 </div>
						 
						 <div class="control-group">
			                <label class="control-label" for="emailAddress">Email Address <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
			                <div class="controls">
			                  <input type="text" name="emailAddress" id="emailAddress" value="${assister.emailAddress}" class="input-xlarge" size="30" placeholder="company@email.com"/>
			                	<div id="emailAddress_error"></div>
			                </div>
		             	 </div>
						 
						<div class="control-group phone-group">
							<label for="primaryPhoneNumber" class="required control-label">Phone Number <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="primaryPhone1" id="primaryPhone1" value="${primaryPhone1}" maxlength="3" placeholder="408" class="area-code input-mini" onkeyup="shiftbox(this, 'primaryPhone2')" />
								<input type="text" name="primaryPhone2" id="primaryPhone2" value="${primaryPhone2}" maxlength="3" placeholder="833" class="input-mini" onkeyup="shiftbox(this, 'primaryPhone3')" />
								<input type="text" name="primaryPhone3" id="primaryPhone3" value="${primaryPhone3}" maxlength="4" placeholder="1861" class="input-small" />
								<input type="hidden" name="primaryPhoneNumber" id="primaryPhoneNumber" value="" />      
								<div id="primaryPhone3_error"></div>
							</div>
						</div>
						<div class="control-group phone-group">
							<label for="secondaryPhoneNumber" class="required control-label">Secondary Phone Number <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="secondaryPhone1" id="secondaryPhone1" value="${secondaryPhone1}" maxlength="3" placeholder="408" class="area-code input-mini" onkeyup="shiftbox(this, 'secondaryPhone2')" />
								<input type="text" name="secondaryPhone2" id="secondaryPhone2" value="${secondaryPhone2}" maxlength="3" placeholder="833" class="input-mini" onkeyup="shiftbox(this, 'secondaryPhone3')" />
								<input type="text" name="secondaryPhone3" id="secondaryPhone3" value="${secondaryPhone3}" maxlength="4" placeholder="9978" class="input-small" />
								<input type="hidden" name="secondaryPhoneNumber" id="secondaryPhoneNumber" value="" />
								<div id="secondaryPhone3_error"></div>
							</div>
						</div>
						
						 <div class="control-group">
								<label class="control-label" for="communicationPreference">Preferred Method of Communication <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<label class="radio inline"> 
										<c:choose>
											<c:when test="${assister.communicationPreference == 'email'}">
												<input type="radio" name="communicationPreference" id="email" value="email" checked />
											</c:when>
											<c:otherwise>
												<input type="radio" name="communicationPreference" id="email" value="email" />
											</c:otherwise>
										</c:choose> 
										Email
									</label> 
									<label class="radio inline"> 
										<c:choose>
											<c:when test="${assister.communicationPreference == 'phone'}">
												<input type="radio" name="communicationPreference" id="communicationPreference" value="phone" checked />
											</c:when>
											<c:otherwise>
												<input type="radio" name="communicationPreference" id="communicationPreference" value="phone" />
											</c:otherwise>
										</c:choose> 
										Phone
									</label> 
									<label class="radio inline"> 
										<c:choose>
											<c:when test="${assister.communicationPreference == 'mail'}">
												<input type="radio" name="communicationPreference" id="communicationPreference" value="mail" checked />
											</c:when>
											<c:otherwise>
												<input type="radio" name="communicationPreference" id="communicationPreference" value="mail" />
											</c:otherwise>
										</c:choose> 
										Mail
									</label>
									<div id="communicationPreference_error"></div>
								</div>
						</div>
						 
						<div class="control-group">
							<label for="isAssisterCertified" class="required control-label">Is this Assister Certified? <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="radio" name="isAssisterCertified" value="" id="selectNo" onclick="$('#certification').hide()" checked="checked">No 
								<input type="radio"	name="isAssisterCertified" value="" id="selectYes" onclick="$('#certification').show()">Yes
								<div id="isAssisterCertified_error" class="help-inline"></div>
							</div>
						</div>
						
						<div id="certification" style="display: none" class="control-group">
							<label for="assisterCertification" class="required control-label">Assister Certification # <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="assisterCertification_readonly" id="assisterCertification" value="" class="input-xlarge" size="30" /> 
								<input type="hidden" name="assisterCertification" id="assisterCertification" value="assisterCertification" />
								<div id="assisterCertification_error"></div>
							</div>
						</div>
					
						<div class="control-group">
							<label for="state" class="control-label">Primary Assister Site <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select size="1" id="primaryAssisterSite" name="primaryAssisterSite" path="sitelist" class="input-medium">
									<option value="">Select</option>
									<c:forEach var="site" items="${primarySitelist}">
										<option id="${site.id}"
											<c:if test="${site.id == assister.primaryAssisterSite.id}"> selected="selected"</c:if>
											value="<c:out value="${site.id}" />"> <c:out value="${site.siteLocationName}" />
										</option>
									</c:forEach>
								</select> 
								<input type="hidden" id="primaryAssisterSite_hidden" name="primaryAssisterSite_hidden" value="${assister.primaryAssisterSite.id}">
								<div id="primaryAssisterSite_error"></div>
							</div>
						</div>
		
						<div class="control-group">
							<label for="state" class="control-label">Secondary Assister Site</label>
							<div class="controls">
								<select size="1" id="site" name="secondaryAssisterSite" path="sitelist" class="input-medium">
									<option value="">Select</option>
									<c:forEach var="site" items="${secondarySitelist}">
										<option id="${site.id}"
											<c:if test="${site.id == assister.secondaryAssisterSite.id}"> selected="selected"</c:if>
											value="<c:out value="${site.id}" />"> <c:out value="${site.siteLocationName}" />
										</option>
									</c:forEach>
								</select> <input type="hidden" id="secondaryAssisterSite_hidden" name="secondaryAssisterSite_hidden" value="${assister.secondaryAssisterSite.id}">
							</div>
						</div>
		
						<div class="graydrkaction">
							<h4 class="span10">Mailing Address</h4>
						</div>
						<div class="control-group">
							<label for="address1_mailing" class="control-label required">Street Address <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">							
								<input type="text" placeholder="Street Name, P.O. Box, Company, c/o" name="mailingLocation.address1" id="address1" value="${assister.mailingLocation.address1}" class="input-xlarge" size="30" />
								<input type="hidden" id="address1_hidden" name="address1_hidden" value="${assister.mailingLocation.address1}">
								<div id="address1_error"></div>
							</div>	
						</div>	
						
						<div class="control-group">
							<label for="address2_mailing" class="control-label">Suite/Apartment Number</label>
							<div class="controls">
								<input type="text" placeholder="Apt, Suite, Unit, Bldg, Floor, etc" name="mailingLocation.address2" id="address2" value="${assister.mailingLocation.address2}" class="input-xlarge" size="30" />
								<input type="hidden" id="address2_hidden" name="address2_hidden" value="${assister.mailingLocation.address2}">
							</div>
						</div>
						 
						 <div class="control-group">
								<label for="city_mailing" class="control-label">City <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<input type="text" placeholder="City, Town" name="mailingLocation.city" id="city" value="${assister.mailingLocation.city}" class="input-xlarge" size="30" />
									<input type="hidden" id="city_hidden" name="city_hidden" value="${assister.mailingLocation.city}">
								<div id="city_error"></div>
								</div>	
						</div>	
						 
						 <div class="control-group">
							<label for="zip" class="control-label">Zip Code <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" placeholder="" name="mailingLocation.zip" id="zip_mailing" value="${assister.mailingLocation.zip}" class="input-mini zipCode" maxlength="5"	 />
								<input type="hidden" id="zip_hidden" name="zip_hidden" value="${assister.mailingLocation.zip}">	
								<div id="zip_mailing_error"></div>
							</div>
						</div>	
						<div class="control-group">
							<label for="state" class="control-label">State <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select size="1" id="state" name="mailingLocation.state" path="statelist" class="input-medium">
									<option value="">Select</option>
									<c:forEach var="state" items="${statelist}">
										<option id="${state.code}" <c:if test="${state.code == assister.mailingLocation.state}"> selected="selected"</c:if>  value="<c:out value="${state.code}" />">
											<c:out value="${state.name}" />
										</option>
									</c:forEach>
								</select>
								<input type="hidden" id="state_hidden" name="state_hidden" value="${assister.mailingLocation.state}">
								<div id="state_error"></div>
							</div>	
						</div>	
					
						<div class="graydrkaction">
							<h4 class="span10">Profile Information</h4>
						</div>
						
		              <div id="languagesSupported" >
		               <table id="languageTable" class="table table-striped table-bordered">
		                  <thead>
		                    <tr>
		                      <th>Language</th>
		                      <th>Spoken Languages Supported</th>
		                      <th>Written Languages Supported</th>
		                    </tr>
		                  </thead>
		                  <tbody>
		                  	<c:forEach var="language" items="${languagelist}">
			                  	<tr>
			                      <td><label><c:out value="${language.lookupValueLabel}" /></label></td>
			                      <td><input type="checkbox" id="spokenlanguagesId" value="<c:out value="${language.lookupValueCode}"/>"></td>
			                      <td><input type="checkbox" id="writtenlanguageId" value="<c:out value="${language.lookupValueCode}"/>"></td>
			                    </tr>
							</c:forEach>
								<tr>
			                      <td>Other</td>
			                      <td><input type="text" id="" placeholder=""></td>
			                      <td><input type="text" id="" placeholder=""></td>
			                    </tr>
		                   </tbody>
		                </table>
		                 <input type="hidden" id="spokenLanguages" name="spokenLanguages" value="${assisterLanguages.spokenLanguages}"/>
						 <input type="hidden" id="writtenLanguages" name="writtenLanguages" value="${assisterLanguages.writtenLanguages}"/>
		              </div>
		              
						<div class="control-group">
							<label for="education" class="control-label">Education <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select size="1" id="assister.education" name="assister.education" path="educationlist" class="input-medium">
									<option value="">Select</option>
									<c:forEach var="educationVar" items="${educationlist}">
										<option id="${educationVar}" <c:if test="${educationVar == assister.education}"> selected="selected"</c:if>  value="<c:out value="${educationVar}" />">
											<c:out value="${educationVar}" />
										</option>
									</c:forEach>
								</select>
								<input type="hidden" id="education_hidden" name="education_hidden" value="${assister.education}">
								<div id="education_error"></div>
							</div>	
						</div>	
						
						<div class="control-group">
								<label class="control-label" for="uploadPhoto">Upload Photo <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls paddingT5">
									<input type="file" class="input-file" id="uploadPhoto" name="uploadPhoto">&nbsp;
					   <!--              <input type="submit" id="btnUploadPhoto" value="Browse" class="btn">
					    -->             <div id="uploadPhoto_error" class="help-inline"></div>
								</div>
						</div>
						<div class="form-actions">
							<input type="submit" name="SaveAssister" id="SaveAssister" value="Save Assister" class="btn btn-primary" />							
						</div>
					
				</form>
				</div>
			</div>	
		</div>
	</div>
</div>	

<script type="text/javascript">
$(document).ready(function() {

	var isShow ='${isShow}';
		
	if(isShow=="true") {
		$("#frmAssister").show();
	}  else {	
		$("#frmAssister").hide();
	}
});

$("#languageTable").click(function() {  
	var spokenValues='';	
	var writtenValues='';
       $("#languageTable :checked").each(function() {
       	  	var id = $(this).attr('id');
       		if(id.contains('spokenlanguagesId'))
               spokenValues=spokenValues+$(this).val()+',';	
       		if(id.contains('writtenlanguageId'))
       			writtenValues=writtenValues+$(this).val()+',';                 
});
       
   $("#spokenLanguages").val(spokenValues);
   $("#writtenLanguages").val(writtenValues);
      
});
	
	
var validator = $("#frmAssister").validate({ 
	onkeyup: false,
	onclick: false,
	rules : {
		name : {required : true},
		emailAddress : { required : true,email: true},
		primaryPhone3 : {primaryphonecheck : true},
		secondaryPhone3 : {secondaryphonecheck : true},
		communicationPreference : { required : true},
		"mailingLocation.address1" : { required: true},
		"mailingLocation.city" : { required: true},
		"mailingLocation.state" : { required: true},
		"mailingLocation.zip" : {required: true, MailingZipCodecheck: true},
		"primaryAssisterSite" : {required: true},
		"assister.education" : { required: true}
	},
	messages : {
		name: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateName' javaScriptEscape='true'/></span>"},
		emailAddress: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEmailAddress' javaScriptEscape='true'/></span>",
	    	email : "<span> <em class='excl'>!</em>Please enter valid email.</span>"},
		primaryPhone3: { primaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNo' javaScriptEscape='true'/></span>"},
	    secondaryPhone3: { secondaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSecondaryPhoneNo' javaScriptEscape='true'/></span>"},
	    communicationPreference: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCommunicationPreference' javaScriptEscape='true'/></span>"},

		"mailingLocation.address1" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"},
		"mailingLocation.city" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
		"mailingLocation.state" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateState' javaScriptEscape='true'/></span>"},
		"mailingLocation.zip" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateZip' javaScriptEscape='true'/></span>",
		MailingZipCodecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateZipSyntax' javaScriptEscape='true'/></span>"},
		"primaryAssisterSite" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryAssisterSite' javaScriptEscape='true'/></span>"},
		"assister.education" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEducation' javaScriptEscape='true'/></span>"}
	}
	,
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});

jQuery.validator.addMethod("primaryphonecheck", function(value, element, param) {
	primphone1 = $("#primaryPhone1").val().trim(); 
	primphone2 = $("#primaryPhone2").val().trim(); 
	primphone3 = $("#primaryPhone3").val().trim(); 
 
	if( (primphone1 == "" || primphone2 == "" || primphone3 == "")  || (isNaN(primphone1)) || (primphone1.length < 3 ) || (isNaN(primphone2)) || (primphone2.length < 3 ) || (isNaN(primphone3)) || (primphone3.length < 4 )  )
	{
		return false;
	}
	else
	{
		$("#primaryPhoneNumber").val(primphone1 + primphone2 + primphone3);
		return true;
	}	
} );

jQuery.validator.addMethod("secondaryphonecheck", function(value, element, param) {
	secondaryphone1 = $("#secondaryPhone1").val().trim(); 
	secondaryphone2 = $("#secondaryPhone2").val().trim(); 
	secondaryphone3 = $("#secondaryPhone3").val().trim(); 
 
	if( (secondaryphone1.length < 3 ) || (secondaryphone2.length < 3 )  || (secondaryphone3.length < 4 )  )
	{
		return false;
	}
	else
	{
		$("#secondaryPhoneNumber").val(secondaryphone1 + secondaryphone2 + secondaryphone3);		
		return true;
	}	
} );

function shiftbox(element,nextelement){
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength){
		nextelement = document.getElementById(nextelement);
		nextelement.focus();
	}
}

jQuery.validator.addMethod("MailingZipCodecheck", function(value, element, param) {
	zip = $("#zip_mailing").val().trim(); 
	if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) )){ 
		return false; 
	}
	return true;
});
</script>	
