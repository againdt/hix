package com.getinsured.eligibility.estimator.mini.controller;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.estimator.mini.util.MiniEstimatorUtil;
import com.getinsured.eligibility.estimator.mini.util.MiniEstimatorValidator;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.EligLead.ExchangeFlowType;
import com.getinsured.hix.model.estimator.mini.EligLeadRequest;
import com.getinsured.hix.model.estimator.mini.LeadAcquireRequest;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorRequest;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorResponse;
import com.getinsured.hix.model.estimator.mini.PreEligibilityResults;
import com.getinsured.hix.model.estimator.mini.StateChipMedicaid;
import com.getinsured.hix.model.prescreen.calculator.FplResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.knappsack.swagger4springweb.annotation.ApiExclude;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * This is controller class for the Mini-Estimator API addressed by HIX-12066
 * Part of epic HIX-7247
 * 
 * @author Nikhil Talreja, Sunil Desu
 * @since 10 July, 2013
 */
@Controller
@RequestMapping("/eligibility")
@Api(value = "Eligibility Estimator operations", basePath = "", description = "Operations for Eligibility Estimator API")
public class MiniEstimatorController extends MiniEstimatorUtil {

	private static final Logger LOGGER = Logger.getLogger(MiniEstimatorController.class);
	// private final String exchangeType =
	// DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
	private static final String SUCCESS = "success";
	private static final String FAILURE = "failure";
	private static final String EXCHANGE_TYPE_STATE = "STATE";
	// private static final String EXCHANGE_TYPE_PHIX="PHIX";

	private static Set<String> csrEligibileReductions = new HashSet<String>();

	// @Value("#{configProp['phix.affiliates.autoRegistrationEnabled'].split(';')}")
	// private Set<String> autoRegPartners;

	static {
		csrEligibileReductions.add("CS4");
		csrEligibileReductions.add("CS5");
		csrEligibileReductions.add("CS6");
		csrEligibileReductions.add("CS2");
		csrEligibileReductions.add("CS3");
		csrEligibileReductions.add("cs4");
		csrEligibileReductions.add("cs5");
		csrEligibileReductions.add("cs6");
		csrEligibileReductions.add("cs2");
		csrEligibileReductions.add("cs3");
	}

	private static final MiniEstimatorRequest EMPTY_REQUEST = new MiniEstimatorRequest();

	private static final int ERROR_MSG_LEN = 250;

	/**
	 * @author Sunil Desu, Nikhil Talreja
	 * @since July 10, 2013
	 * 
	 *        This method serves the request of the REST call that is intended to
	 *        compute the eligibility estimate. JIRA: HIX-12790
	 * 
	 * @param MiniEstimatorRequest
	 * @return MiniEstimatorResponse
	 * 
	 */
	@ApiOperation(value = "Calculates Eligibility for a household", notes = "This service estimates the eligibility of a household")
	@RequestMapping(value = "/estimator", method = RequestMethod.POST)
	@ResponseBody
	public MiniEstimatorResponse calculateEligibility(@RequestBody MiniEstimatorRequest request) {

		LOGGER.debug("Calculating Eligiblity for the household.");
		MiniEstimatorResponse response = new MiniEstimatorResponse();
		EligLead record = null;
		String stateCode = null;

		try {
			response.startResponse();

			if (null != request) {
				LOGGER.info("Creating record inside ELIG_LEAD table");
				record = createNewEligLeadRecord(request);
			}
			if (request.isSkipEligibilityCheck()) {
				response.setMembers(request.getMembers());
				response.setStatus(SUCCESS);
				// HP-FOD fix for null dereference
				response.setLeadId(record != null ? record.getId() : 0);
				response.endResponse();
				response.setAptcAmount("0.0");
				if(request.getCsrLevel() != null){
					response.setCsrLevelSilver(request.getCsrLevel());
				}
				else {
					response.setCsrLevelSilver("N/A");
				}
				return response;
			}
			if (EMPTY_REQUEST.equals(request)) {
				response.setAllPlansAvailable(NOT_APPLICABLE);
				response.setSubsidyEligible(NOT_APPLICABLE);
				response.setCsrEligible(NOT_APPLICABLE);
				response.getErrors().put(EMPTY_REQUEST_ERROR_CODE, EMPTY_REQUEST_ERROR_MESSAGE);
				response.setStatus(FAILURE);
				response.endResponse();
				LOGGER.error("Eligibility could not be estimated. Request object was empty");
				return response;
			}
			// Step 1: Null check for request and Validate API Key for affiliated partner
			if (null == request || (!request.isSkipAffiliateValidation()
					&& null == validateAPIKey(request.getAffiliateId(), request.getApiKey(), response))) {
				response.setAllPlansAvailable(NOT_APPLICABLE);
				response.setSubsidyEligible(NOT_APPLICABLE);
				response.setCsrEligible(NOT_APPLICABLE);
				if (null == request) {
					response.getErrors().put(NULL_REQUEST_ERROR_CODE, NULL_REQUEST_ERROR_MESSAGE);
					LOGGER.error("Eligibility could not be estimated. Request object was null");
				}
				response.setStatus(FAILURE);
				response.endResponse();
				return response;
			}
			// response.setFlowId(request.getFlowId());
			// response.setAffiliateId(request.getAffiliateId());
			// response.setAffiliateHouseholdId(request.getAffiliateHouseholdId());

			// Step 2 : Validate Request parameters
			LOGGER.info("Validating inbound elements");
			ZipCode zipCode = validateZipCode(request, response);
			validateMiniEstimatorRequest(request, response);
			if (zipCode == null || !response.getErrors().isEmpty()) {
				response.setStatus(FAILURE);
				response.endResponse();
				LOGGER.info("Validation errors in the request. Returning an error response.");
				return response;
			}
			stateCode = zipCode.getState();

			/*
			 * HIX-104686 : Don't Increment the Family size by 1 if there is a pregnant
			 * Member in the Household.
			 * 
			 * Pregnancy Indication Feature Check.
			 * 
			 */
			int adjustedFamilySize = 0;
			String pregnancyIndicatorConfig = DynamicPropertiesUtil.getPropertyValue(
					IEXConfiguration.IEXConfigurationEnum.IEX_PRESCREENER_MEDICAID_FAMILYSIZE_INCR_PREGNANCY);
			if (pregnancyIndicatorConfig != null && pregnancyIndicatorConfig.equalsIgnoreCase("Y")) {
				// -- Increment the FamilySize by 1 if there is a pregnant Member in the
				// Household.
				adjustedFamilySize = checkForPregnantMembers(request);
			}

			// Step 3 : Calculate FPL, Applicable %, Expected Monthly premium and CSR

			FplResponse fplResponse = calculateFpl(stateCode, request, false, 0);
			double fpl = fplResponse.getFplPercentage();
			LOGGER.debug("fplForQHP : " + fpl);

			double applicablePercentage = DEFAULT_APP_PERCENTAGE;
			if (fpl >= HUNDRED_POINT_ZERO && fpl <= APTC_THRESHOLD) {
				applicablePercentage = calculateApplicablePercentage(fpl,request.getCoverageYear());
				LOGGER.debug("Applicable Percentage : " + applicablePercentage);

				double expectedMonthlyPremium = Math
						.round((((applicablePercentage * request.getHhIncome()) / NO_OF_MONTHS) / HUNDRED)
								* HUNDRED_POINT_ZERO)
						/ HUNDRED_POINT_ZERO;
				response.setExpectedMonthlyPremium("$" + expectedMonthlyPremium);

				LOGGER.debug("Expected monthly premium : " + expectedMonthlyPremium);
			}

			double fplForMedicaid = calculateFpl(stateCode, request, true, adjustedFamilySize).getFplPercentage();
			LOGGER.debug("fplForMedicaid : " + fplForMedicaid);
			calculateMedicaidChipEligibility(stateCode, fplForMedicaid, request, response);

			String csr = computeCSR(fpl);
			LOGGER.info("CSR " + csr);

			if (csrEligibileReductions.contains(csr)) {
				response.setCsrEligible("Y");
				response.setCsrLevelSilver(csr);
			}

			if (response.getMedicaidHousehold().equalsIgnoreCase(NO)
					&& response.getMedicareHousehold().equalsIgnoreCase(NO)
					&& response.getMedicareHousehold().equalsIgnoreCase(NO) && fpl >= HUNDRED_POINT_ZERO
					&& fpl <= APTC_THRESHOLD) {
				response.setAptcEligible("Y");
			}
			if (fpl <= APTC_THRESHOLD) {
				response.setSubsidyEligible("Y");
			}
			// Step 5 : Update Member eligibility String
			updateMemberEligibility(fpl, request, response);

			// Step 6 : Calculate Mandate Penalty
			if (fpl >= HUNDRED_POINT_ZERO) {
				response.setMandatePenaltyAmount("$" + calculateMandatePenalty(request));
			}

			/*
			 * Step 7 : Call Plan Management API to get plan details If there is atleast one
			 * member eligible for aptc or csr, call the API. This API will not be called in
			 * case of Medicaid/CHIP Household or when everyone in the household is Medicare
			 * eligible
			 * 
			 */
			if (StringUtils.equalsIgnoreCase(response.getMedicaidHousehold(), NO)
					&& StringUtils.equalsIgnoreCase(response.getChipHousehold(), NO)
					&& StringUtils.equalsIgnoreCase(response.getMedicareHousehold(), NO)
					&& findNoOfMembersForEligibilityType(response.getMembers(), "CHIP")
							+ findNoOfMembersForEligibilityType(response.getMembers(), "Medicare") != CollectionUtils
									.size(response.getMembers())) {
				// For State Exchange : Get plans (SLSP,LBP,LSP) from PM to compute the
				// SLSP/APTC
				// if(null==exchangeType || exchangeType.equalsIgnoreCase(EXCHANGE_TYPE_STATE)
				// || exchangeType.isEmpty())
				// {
				// Get SLSP ,LSP,LBP from getPlans(), Compute APTC and retrieved SLSP
				// For State API FLowType is always =ON
				getPlanDetails(request, response, stateCode, applicablePercentage, ExchangeFlowType.ON.toString(),
						EXCHANGE_TYPE_STATE, fpl);
				// }
				// else if (exchangeType.equalsIgnoreCase(EXCHANGE_TYPE_PHIX))
				// {
				// For PHIX : Get SLSP from Master file and compute APTC, Call PM to get the LSP
				// LBP plans
				// getPremiumDetails(request,response,zipCode,applicablePercentage, fpl);
				// getPlanDetails(request,response,stateCode,applicablePercentage,determineFlowType(response,record),exchangeType,
				// fpl);

				// }

				// getPlanAvailableInfo(zipCode,response);
				// HIX-30181 :Compute CSR For NativeAmericanHousehold : As per Assaf the CSR
				// wont have any effect on getting the plans
				// As the silver plans are always available for CS2 if it is available for other
				// CS levels
				// if FPL<300 then the CSR should be CS2 : IF FPL>300 then the CSR should be CS3
				String csr_NA = computeCSR(fpl,
						response.getNativeAmericanHousehold().equalsIgnoreCase(YES) ? true : false);
				if (csrEligibileReductions.contains(csr_NA)) {
					response.setCsrEligible("Y");
					response.setCsrLevelSilver(csr_NA);
				}
			} else {
				// Mandate Penalty and Expected Monthly Premium should be sent as NA for
				// Medicare/CHIP/Medicaid household
				response.setMandatePenaltyAmount(NOT_APPLICABLE);
				response.setExpectedMonthlyPremium(NOT_APPLICABLE);
				response.setCsrEligible(NOT_APPLICABLE);
				response.setCsrLevelSilver(NOT_APPLICABLE);
				response.setAptcEligible(NO);
				response.setAptcAmount(NOT_APPLICABLE);
			}

			/*
			 * HIX-21393 Step 8 : Update GI_EXCHANGE_FLOW_TYPE in ELIG_LEAD based on
			 * following rules: Off Exchange = 1. aptcEligible=N & medicaidHousehold=N &
			 * medicareHousehold=N & chipHousehold=N 2. aptcEligible=Y & aptcAmount=$0 On
			 * Exchange = aptcEligible=Y & aptcAmount > $0 or n/a
			 */
			// determineFlowType(response);

			// Step 8 : Auto register user, if affiliate is Jackson Hewitt
			// if(autoRegAffiliate != null &&
			// autoRegPartners.contains(autoRegAffiliate.getCompanyName())){
			// sendEmailToUser(request);
			// }

			response.setStatus(SUCCESS);
			response.endResponse();
			LOGGER.debug("Successfully completed the eligibility estimation. Time taken  is "
					+ response.getExecDuration() + " ms.");
		} catch (Exception e) {
			LOGGER.error("General exception occurred in estimator api. :: ", e);
			handleEstimatorException(response, record, request.getCoverageYear(), e, ERROR_MSG_LEN);
			LOGGER.debug("Failure while eligibility estimation. Time taken  is " + response.getExecDuration() + " ms.");
		} finally {

			try {
				if (null != request && null != record) {
					LOGGER.info("Updating record inside ELIG_LEAD table");
					response.setLeadId(record.getId());
					updateFiledsForEligLeadRecord(record, request.getCoverageYear(), response);
				}
			} catch (Exception e) {
				LOGGER.error("Exception occured while updating record inside ELIG_LEAD table", e);
				LOGGER.debug("Request : " + request);
				LOGGER.debug("Response : " + response);
			}
		}

		// LOGGER.debug("Estimator response " + response);
		return response;
	}

	/**
	 * @author Nikhil Talreja
	 * @since August 14, 2013
	 * 
	 *        This method updates the Stage and Status of a record in ELIG_LEAD
	 *        Table JIRA: HIX-15263
	 * 
	 * 
	 */
	@ApiExclude
	@RequestMapping(value = "/updateEligLeadRecord", method = RequestMethod.POST)
	@ResponseBody
	public String updateEligLeadRecord(@RequestBody EligLeadRequest request) {

		try {

			// Step 1 : Check if status to be updated is valid
			if (request.getStatus() == null) {
				LOGGER.error("Status " + request.getStatus() + " is invalid");
				return "Invalid Status";
			}

			// Step 2 : Check if stage to be updated is valid
			if (request.getStage() == null) {
				LOGGER.error("Stage " + request.getStage() + " is invalid");
				return "Invalid Stage";
			}

			// Step 3 : Check if record exists
			EligLead record = checkEligLeadRecord(request.getId());
			if (record == null) {
				LOGGER.error("Lead with id " + request.getId() + " does not exist");
				return "Lead id does not exist";
			} else {
				int year = request.getCoverageYear();
				PreEligibilityResults results = getPreEligResultForCoverageYear(record, year);
				if (results != null) {
					results.setStage(request.getStage());
					results.setStatus(request.getStatus());
					record = setPreEligResultForCoverageYear(record, results, year);
				}
				record.setStage(request.getStage());
				record.setStatus(request.getStatus());
			}

			// Save record
			updateEligLeadRecord(record);
			return SUCCESS;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return FAILURE;
		}

	}

	/**
	 * @author Nikhil Talreja
	 * @since August 14, 2013
	 * 
	 *        This method updates the contact details for a record in ELIG_LEAD
	 *        Table JIRA: HIX-15263
	 * 
	 */
	@ApiExclude
	@RequestMapping(value = "/updateContactDetailsInEligLeadRecord", method = RequestMethod.POST)
	@ResponseBody
	public String updateContactDetailsInEligLeadRecord(@RequestBody EligLeadRequest request) {

		try {

			// Check if record exists
			EligLead record = checkEligLeadRecord(request.getId());
			if (record == null) {
				LOGGER.error("Lead with id " + request.getId() + " does not exist");
				return FAILURE;
			} else {
				LOGGER.info("Updating contact details in ELIG_LEAD table for record with id " + request.getId());
				record.setEmailAddress(request.getEmail());
				record.setName(request.getName());
				record.setPhoneNumber(request.getPhone());
				record.setIsOkToCall(request.getIsOkToCall());
			}

			// Save record
			updateEligLeadRecord(record);
			return SUCCESS;

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return FAILURE;
		}
	}

	/**
	 * @author Nikhil Talreja
	 * @since August 14, 2013
	 * 
	 *        This method updates the stage of a record in ELIG_LEAD Table
	 * 
	 */
	@ApiExclude
	@RequestMapping(value = "/updateOtherInputsInEligLeadRecord", method = RequestMethod.POST)
	@ResponseBody
	public String updateOtherInputsInEligLeadRecord(@RequestBody EligLeadRequest request) {

		try {

			// Check if record exists
			EligLead record = checkEligLeadRecord(request.getId());
			if (record == null) {
				LOGGER.error("Lead with id " + request.getId() + " does not exist");
				return FAILURE;
			} else {
				LOGGER.info("Updating ELIG_LEAD table for record with id " + request.getId());
				record.setDocVisitFrequency(request.getDocVisitFrequency());
				record.setNoOfPrescriptions(request.getNoOfPrescriptions());
				record.setBenefits(request.getBenefits());
				int year = request.getCoverageYear();
				PreEligibilityResults results = getPreEligResultForCoverageYear(record, year);
				if (results != null) {
					results.setStage(request.getStage());
					record = setPreEligResultForCoverageYear(record, results, year);
				}
				record.setStage(request.getStage());
			}

			// Save record
			updateEligLeadRecord(record);
			return SUCCESS;

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return FAILURE;
		}
	}

	/**
	 * @author Nikhil Talreja
	 * @since November 26, 2013
	 * 
	 *        This method updates the flow type of a record in ELIG_LEAD Table
	 * 
	 */
	@ApiExclude
	@RequestMapping(value = "/updateFlowTypeInEligLeadRecord", method = RequestMethod.POST)
	@ResponseBody
	public String updateFlowTypeInEligLeadRecord(@RequestBody EligLeadRequest request) {

		try {

			// Check if record exists
			EligLead record = checkEligLeadRecord(request.getId());
			if (record == null) {
				LOGGER.error("Invalid lead Id");
				return FAILURE;
			} else {
				LOGGER.info("Updating flow type in ELIG_LEAD table for record with id " + request.getId());
				record.setExchangeFlowType(request.getExchangeFlowType());
			}

			// Save record
			updateEligLeadRecord(record);
			return SUCCESS;

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return FAILURE;
		}
	}

	/**
	 * @author Nikhil Talreja
	 * @since August 30, 2013
	 * 
	 *        This method fetches a record from ELIG_LEAD Table
	 */
	@ApiOperation(value = "Gets a record from ELIG_LEAD table", notes = "This service fetches a record from ELIG_LEAD table based on leadId")
	@RequestMapping(value = "/getEligLeadRecord", method = RequestMethod.POST)
	@ResponseBody
	public EligLead getEligLeadRecord(@RequestBody long leadId) {
		return checkEligLeadRecord(leadId);
	}

	/**
	 * @author Nikhil Talreja
	 * @since September 04, 2013
	 * 
	 *        This method saves a record in ELIG_LEAD Table
	 */
	@ApiOperation(value = "Saves a record in ELIG_LEAD table", notes = "This service saves a record in ELIG_LEAD table")
	@RequestMapping(value = "/saveEligLeadRecord", method = RequestMethod.POST)
	@ResponseBody
	public EligLead saveEligLeadRecord(@RequestBody EligLead record) {
		return updateEligLeadRecord(record);
	}

	/**
	 * @author Nikhil Talreja
	 * @since December 09, 2013
	 * 
	 *        This method updates GI_APP_STATUS and GI_APP_STATUS_UPDATE_TMSTP
	 *        ELIG_LEAD table
	 */
	@ApiOperation(value = "Updates App Status in ELIG_LEAD table", notes = "This service updates GI_APP_STATUS and GI_APP_STATUS_UPDATE_TMSTP ELIG_LEAD table ")
	@RequestMapping(value = "/updateAppStatusInEligLeadRecord", method = RequestMethod.POST)
	@ResponseBody
	public EligLead updateAppStatusInEligLeadRecord(@RequestBody EligLead record) {
		record.setAppStatusUpdateTimeStamp(new TSDate());
		return updateEligLeadRecord(record);
	}

	/**
	 * @author Nikhil Talreja
	 * @since September 10, 2013
	 * 
	 *        This method fetches a record from STATE_MEDICAID_CHIP Table based on
	 *        state code
	 */
	@ApiExclude
	@RequestMapping(value = "/getStateChipMedicaidRecordByState", method = RequestMethod.POST)
	@ResponseBody
	public StateChipMedicaid getStateChipMedicaidRecordByState(@RequestBody String stateCode) {
		return getStateChipMedicaidRecordForState(stateCode);
	}

	/**
	 * @author root
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "Save lead data in ELIG_LEAD table", notes = "This service is for outbound call tool")
	@RequestMapping(value = "/LeadAcquisition", method = RequestMethod.POST)
	@ResponseBody
	public MiniEstimatorResponse acquireLeads(@RequestBody LeadAcquireRequest request) {

		MiniEstimatorResponse response = new MiniEstimatorResponse();
		response.startResponse();
		response.setStatus(FAILURE);

		/**
		 * all failure conditions
		 */
		if (request == null) {
			response.getErrors().put(EMPTY_REQUEST_ERROR_CODE, EMPTY_REQUEST_ERROR_MESSAGE);
			response.endResponse();
			return response;
		}

		/**
		 * Validate API key
		 */
		if (null == validateAPIKey(request.getAffiliateId(), request.getApiKey(), response)) {
			response.endResponse();
			return response;
		}

		/**
		 * First and Last name to be mandatory and not empty
		 */
		if (request.getFirstName() == null || StringUtils.isEmpty(request.getFirstName())) {
			response.getErrors().put(MISSING_PARAMETER_ERROR_CODE, EMPTY_FIRST_NAME);
			response.endResponse();
			return response;
		}

		if (request.getLastName() == null || StringUtils.isEmpty(request.getLastName())) {
			response.getErrors().put(MISSING_PARAMETER_ERROR_CODE, EMPTY_LAST_NAME);
			response.endResponse();
			return response;
		}

		/**
		 * Throw back error if flow id is missing what is a valid flow id?
		 */
		if (request.getFlowId() < 1) {
			response.getErrors().put(MISSING_PARAMETER_ERROR_CODE, FLOW_ID_MISSING);
			response.endResponse();
			return response;

		}
		/**
		 * Validate family size
		 */
		if (request.getFamilySize() <= MiniEstimatorController.MIN_FAMILY_SIZE
				|| request.getFamilySize() > MAX_FAMILY_SIZE) {
			response.getErrors().put(INVALID_FAMILY_SIZE_ERROR_CODE, INVALID_FAMILY_SIZE_ERROR_MESSAGE);
			response.endResponse();
			return response;
		}

		/**
		 * Only accept 10 digit phone number
		 */
		if (request.getPhone() == null) {
			response.getErrors().put(MISSING_PARAMETER_ERROR_CODE, PHONE_NUMBER_MISSING_ERROR_MESSAGE);
			response.endResponse();
			return response;
		}

		/**
		 * Validate zip code
		 */
		if (null == validateZipCode(request, response)) {
			response.endResponse();
			return response;
		}

		/**
		 * Date of birth for each member
		 */
		if (!request.getMembers().isEmpty() && !validateDob(request.getMembers())) {
			response.getErrors().put(INVALID_DOB_ERROR_CODE, INVALID_DOB_ERROR_MESSAGE);
			response.endResponse();
			return response;
		}

		/**
		 * tobacco usage if present
		 */
		if (!validateTobaccoUsage(request.getMembers())) {
			response.getErrors().put(INVALID_VALUE_CODE, "Invalid Tobacco Usage option");
			response.endResponse();
			return response;
		}

		/**
		 * Validate email address
		 */
		if (null != request.getEmail() && StringUtils.isNotBlank(request.getEmail())
				&& !request.getEmail().matches(MiniEstimatorValidator.PATTERN_EMAIL)) {
			response.getErrors().put(INVALID_EMAIL_ERROR_CODE, INVALID_EMAIL_ERROR_MESSAGE);
			response.endResponse();
			return response;
		}

		/**
		 * Length of affiliateHousehold Id if present
		 */
		if (request.getAffiliateHouseholdId() != null && StringUtils.isNotBlank(request.getAffiliateHouseholdId())) {
			if (!StringUtils.isNumeric(request.getAffiliateHouseholdId())
					|| request.getAffiliateHouseholdId().length() > EligibilityConstants.NINETEEN) {
				response.getErrors().put(INVALID_AFFILIATE_ERROR_CODE, "Invalid Affiliate Household Id");
				response.endResponse();
				return response;
			}
		}

		/**
		 * PHone number validation
		 */
		if (StringUtils.isNotBlank(request.getPhone()) && (StringUtils.length(request.getPhone()) != PHONE_NUMBER_LENGTH
				|| !StringUtils.isNumeric(request.getPhone())
				|| StringUtils.startsWithIgnoreCase(request.getPhone(), "0"))) {
			response.getErrors().put(INVALID_PHONE_ERROR_CODE, INVALID_PHONE_ERROR_MESSAGE);
			response.endResponse();
			return response;
		}

		// create eligLead record
		EligLead record = createNewEligLeadRecord(request);
		if (record != null) {
			record.setName(request.getFirstName() + NAME_SPLITTER + request.getLastName());
			record.setBatchProcessed(1L); // initial state other than default
			record.setCreatedByAPI("API_2");
			updateEligLeadRecord(record);
			response.setLeadId(record.getId());
			response.setStatus(SUCCESS);
		} else {
			response.getErrors().put(DB_ERROR_CODE, ERROR_SAVING_TO_DB);
			response.setStatus(FAILURE);
		}
		response.endResponse();

		return response;
	}

}
