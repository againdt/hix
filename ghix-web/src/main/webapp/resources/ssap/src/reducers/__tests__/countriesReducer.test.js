
import countriesReducer, { initialState } from '../countriesReducer';
import { countriesActionTypes } from '../../constants/actionTypes';

const anotherAction = {
    type: 'ANOTHER_ACTION',
    payload: []
};

const setCountriesAction = {
    type: countriesActionTypes.SET_COUNTRIES,
    countries: [
        {
            value: 'BLR',
            text: 'Belarus'
        },
        {
            value: 'FR',
            text: 'France'
        },
    ]
};

const clearStateAction = {
    type: countriesActionTypes.CLEAR_STATE
};


describe('Test for countries reducer', () => {
    it('This reducer hasn\'t been invoked', () => {
        expect(countriesReducer(initialState, anotherAction)).toEqual(initialState);
    });

    it('Should set countries', ()=> {
        expect(countriesReducer(initialState, setCountriesAction)).toEqual(setCountriesAction.countries);
    });

    it('Should clear state', () => {
        expect(countriesReducer({ welcomeText: 'Hi there!' }, clearStateAction)).toEqual(initialState);
    });
});

