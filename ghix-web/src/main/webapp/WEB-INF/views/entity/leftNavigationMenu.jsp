<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<c:set var="assiterUserId" ><encryptor:enc value="${assister.entity.user.id}" isurl="true"/> </c:set>
<c:set var="assiterId" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
<c:set var="assiterEntityId" ><encryptor:enc value="${assister.entity.id}" isurl="true"/> </c:set>
<c:set var="enrollmentEntityUserId" ><encryptor:enc value="${enrollmentEntity.user.id}" isurl="true"/> </c:set>
<c:set var="enrollmentEntityId" ><encryptor:enc value="${enrollmentEntity.id}" isurl="true"/> </c:set>

<!-- #sidebar -->     
        <div class="span3" id="sidebar">
          
          <div class="header gutter10-l">
			<c:if test="${loggedUser != 'entityAdmin'}">
 	 	 	 	    Steps
 	 	 	</c:if>
<!--           		Steps -->
	         	<%-- <c:if test="${loggedUser =='entityAdmin'}">
					<h1><a name="skip"></a>${enrollmentEntity.entityName}</h1>
				</c:if> --%>
			</div>         			
			<ul class="nav nav-list">
			<c:choose>	
				<c:when test="${loggedUser !='entityAdmin' && loggedUser !='assisterLogin'}">
					<c:choose>
						<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
							<li id="entityInformation" class="link"><a href="/hix/entity/enrollmententity/viewentityinformation"><spring:message code="label.entity.entityInformation"/></a></li>
						</c:when>
						<c:when test="${fn:contains(sessionScope.navigationmenumap,'ENTITY_INFO')}">
							<li id="entityInformation" class="link"><a href="/hix/entity/enrollmententity/entityinformation?isLeftNav=true"><i class="icon-ok"></i><spring:message code="label.entity.entityInformation"/></a></li>
						</c:when>
						<c:otherwise>
							<li id="entityInformation"><a href="#">1. <spring:message code="label.entity.entityInformation"/></a></li>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus == 'Incomplete' && fn:contains(sessionScope.nextRegistrationStep,'POPULATION_SERVED')}">
							<li id="populationServed"><a href="/hix/entity/enrollmententity/getPopulationServed">2. <spring:message code="label.entity.populationsServed"/></a></li>      
						</c:when>
						<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
							<li id="populationServed" class="link"><a href="/hix/entity/enrollmententity/viewPopulationServed"><spring:message code="label.entity.populationsServed"/></a></li>
						</c:when>
						<c:when test="${fn:contains(sessionScope.navigationmenumap,'POPULATION_SERVED')}">
							 <li id="populationServed" class="link"><a href="/hix/entity/enrollmententity/getPopulationServed"><i class="icon-ok"></i><spring:message code="label.entity.populationsServed"/></a></li>      
						</c:when>
						<c:otherwise>
							<li id="populationServed"><a href="#">2. <spring:message code="label.entity.populationsServed"/></a></li>      
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus == 'Incomplete' && fn:contains(sessionScope.nextRegistrationStep,'PRIMARY')}">
							 <li id="locationAndHours" class="link"><a href="/hix/entity/enrollmententity/primarysite">3. &nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="label.entity.locationAndHours"/></a>
									<ul>
										<li><a href="/hix/entity/enrollmententity/primarysite"><spring:message code="label.entity.primarySites"/></a></li>
										<li><a href="/hix/entity/enrollmententity/subsite"><spring:message code="label.entity.subSites"/></a></li>
									</ul>     
							</li>		
						</c:when>						
						<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
							<li id="locationAndHours" class="link"><a href="/hix/entity/enrollmententity/viewsite"><spring:message code="label.entity.locationAndHours"/></a>
								<ul>
							 		<li><a href="/hix/entity/enrollmententity/viewsite"><spring:message code="label.entity.primarySites"/></a></li>
							 		<li><a href="/hix/entity/enrollmententity/viewsite"><spring:message code="label.entity.subSites"/></a></li>
							 	</ul>
						</c:when>
						<c:when test="${fn:contains(sessionScope.navigationmenumap,'LOCATION_HOURS')}">
							 <li id="locationAndHours" class="link"><a href="/hix/entity/enrollmententity/primarysite"><i class="icon-ok"></i><spring:message code="label.entity.locationAndHours"/></a>
							 	<ul>
							 		<li class="primarySiteSubpage"><a href="/hix/entity/enrollmententity/primarysite"><spring:message code="label.entity.primarySites"/></a></li>
							 		<li class="subsiteSubpage"><a href="/hix/entity/enrollmententity/subsite"><spring:message code="label.entity.subSites"/></a></li>
							 	</ul>
							 </li>							 
						</c:when>
						<c:otherwise>
							 <li id="locationAndHours"><a href="#">3. <spring:message code="label.entity.locationAndHours"/></a>
							 	<ul>
							 		<li class="primarySiteSubpage"><a href="#"><spring:message code="label.entity.primarySites"/></a></li>
							 		<li class="subsiteSubpage"><a href="#"><spring:message code="label.entity.subSites"/></a></li>
							 	</ul>
							</li>  
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus == 'Incomplete' && fn:contains(sessionScope.nextRegistrationStep,'ENTITY_CONTACT')}">
							<li id="entityContacts"><a href="/hix/entity/enrollmententity/entitycontactinfo">4. <spring:message code="label.entity.contactInformation"/></a></li>
						</c:when>						
						<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
							<li id="entityContacts" class="link"><a href="/hix/entity/enrollmententity/viewentitycontactinfo"><spring:message code="label.entity.contactInformation"/></a></li>
						</c:when>
						<c:when test="${fn:contains(sessionScope.navigationmenumap,'ENTITY_CONTACT')}">
							<li id="entityContacts" class="link"><a href="/hix/entity/enrollmententity/entitycontactinfo"><i class="icon-ok"></i><spring:message code="label.entity.contactInformation"/></a></li>  
						</c:when>
						<c:otherwise>
							<li id="entityContacts"><a href="#">4. <spring:message code="label.entity.contactInformation"/></a></li>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${isascflag != null}">															
							<li id="assisters"><a href="/hix/entity/assister/registration">5. <spring:message code="label.assister.assisters"/></a></li>
						</c:when>						
						<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus == 'Incomplete' && (fn:contains(sessionScope.nextRegistrationStep,'ASSISTER'))}">														
							<li id="assisters"><a href="/hix/entity/assister/registration">5. <spring:message code="label.assister.assisters"/></a></li>
						</c:when>
						<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">							
							<li id="assisters" class="link"><a href="/hix/entity/assister/registration"><spring:message code="label.assister.assisters"/></a></li>
						</c:when>						
						<c:when test="${fn:contains(sessionScope.navigationmenumap,'ASSISTER')}">							
							<li id="assisters" class="link"><a href="/hix/entity/assister/registration"><i class="icon-ok"></i><spring:message code="label.assister.assisters"/></a></li>
						</c:when>	
						<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus == 'Incomplete' && (fn:contains(sessionScope.nextRegistrationStep,'DOCUMENT_UPLOAD') || fn:contains(sessionScope.nextRegistrationStep,'PAYMENT_INFO'))}">														
							<li id="assisters"><a href="/hix/entity/assister/registration">5. <spring:message code="label.assister.assisters"/></a></li>
						</c:when>																
						<c:otherwise>							
							<li id="assisters"><a href="#">5. <spring:message code="label.assister.assisters"/></a></li> 
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${isdlflag != null}">
							  <li id="documentupload" class="link"><a href="/hix/entity/enrollmententity/documententity"><i class="icon-ok"></i><spring:message code="label.entity.docUpload"/></a></li>							  
						</c:when> 
						<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus == 'Incomplete' && fn:contains(sessionScope.nextRegistrationStep,'DOCUMENT_UPLOAD')}">
							<li id="documentupload"><a href="/hix/entity/enrollmententity/documententity">6. <spring:message code="label.entity.docUpload"/></a></li>
						</c:when>
						<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
							<li id="documentupload"><a href="/hix/entity/enrollmententity/documententity"><spring:message code="label.entity.docUpload"/></a></li>
						</c:when>	
						<c:when test="${fn:contains(sessionScope.navigationmenumap,'DOCUMENT_UPLOAD')}">
							  <li id="documentupload" class="link"><a href="/hix/entity/enrollmententity/documententity"><i class="icon-ok"></i><spring:message code="label.entity.docUpload"/></a></li>							  
						</c:when>
						<c:otherwise>
							  <li id="documentupload"><a href="#">6. <spring:message code="label.entity.docUpload"/></a></li> 
						</c:otherwise>
					</c:choose>
					
					<c:if test="${sessionScope.ID_STATE_CODE || sessionScope.NV_STATE_CODE}">
							<c:choose>
								<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus == 'Incomplete' && fn:contains(sessionScope.nextRegistrationStep,'PAYMENT_INFO')}">
									<li id="paymentInfo"><a href="/hix/entity/enrollmententity/viewpaymentinfo">7. <spring:message code="label.entity.paymentInformation"/></a></li> 
								</c:when>
								<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
									<li id="paymentInfo" class="link"><a href="/hix/entity/enrollmententity/viewpaymentinfo"><spring:message code="label.entity.paymentInformation"/></a></li>
								</c:when>
								<c:when test="${fn:contains(sessionScope.navigationmenumap,'PAYMENT_INFO')}">
									<li id="paymentInfo" class="link"><a href="/hix/entity/enrollmententity/viewpaymentinfo"><i class="icon-ok"></i><spring:message code="label.entity.paymentInformation"/></a></li>
								</c:when>
								<c:otherwise>
									<li id="paymentInfo"><a href="#">7. <spring:message code="label.entity.paymentInformation"/></a></li> 
								</c:otherwise>
							</c:choose>
					</c:if>
					<c:choose>
						<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
							<li id="registerStatus" class="link"><a href="/hix/entity/enrollmententity/registrationstatus"><spring:message code="label.entity.registrationStatus"/></a></li>
						</c:when>
						<c:when test="${fn:contains(sessionScope.navigationmenumap,'PAYMENT_INFO')}">
							<li id="registerStatus" class="link"><a href="/hix/entity/enrollmententity/registrationstatus"><i class="icon-ok"></i><spring:message code="label.entity.registrationStatus"/></a></li>
						</c:when>
						<c:otherwise>							
						</c:otherwise>
					</c:choose>
				</c:when>
				
				<c:otherwise>
					<c:choose>
							<c:when test="${IsAssister}">
								 <li id="entityInformation" class="link"><a href="/hix/entity/entityadmin/viewentityinformation/${assiterUserId}"><spring:message code="label.entity.entityInformation"/></a></li>
								 <li id="populationServed" class="link"><a href="/hix/entity/entityadmin/viewPopulationServed/${assiterUserId}"><spring:message code="label.entity.populationsServed"/></a></li>
								 <li id="locationAndHours" class="link"><a href="/hix/entity/entityadmin/viewsite/${assiterUserId}"><spring:message code="label.entity.locationAndHours"/></a></li>
								 <li id="entityContacts" class="link"><a href="/hix/entity/entityadmin/viewentitycontactinfo/${assiterUserId}"><spring:message code="label.entity.contactInformation"/></a></li>
								 <li id="assisters" class="link"><a href="/hix/entity/entityadmin/viewlistofassisters/${assiterUserId}"><spring:message code="label.assister.assisters"/></a></li>					 
								<c:choose>
									<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
											 <li id="documentupload" class="link"><a href="/hix/entity/entityadmin/documententity/${assiterEntityId}"><spring:message code="label.entity.docUpload"/></a></li>
									</c:when>
							  		<c:when test="${fn:contains(sessionScope.navigationmenumap,'DOCUMENT_UPLOAD')}">
								  		 <li id="documentupload" class="link"><a href="/hix/entity/entityadmin/documententity/${assiterEntityId}"><i class="icon-ok"></i><spring:message code="label.entity.docUpload"/></a></li> 
									</c:when>
									<c:otherwise>
								  		<li id="documentupload"><a href="/hix/entity/enrollmententity/entityadmin/${assiterEntityId}">6. <spring:message code="label.entity.docUpload"/></a></li> 
									</c:otherwise>
					 			</c:choose>
								 <c:choose>
								 	<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
								 		<li id="paymentInfo" class="link"><a href="/hix/entity/entityadmin/viewpaymentinfo/${assiterEntityId}"><spring:message code="label.entity.paymentInformation"/></a></li>
								 	</c:when>
							  		<c:when test="${fn:contains(sessionScope.navigationmenumap,'PAYMENT_INFO')}">
								  		 <li id="paymentInfo" class="link"><a href="/hix/entity/entityadmin/viewpaymentinfo/${assiterEntityId}"><i class="icon-ok"></i><spring:message code="label.entity.paymentInformation"/></a></li> 
									</c:when>
									<c:otherwise>
								  		<li id="paymentInfo"><a href="#">7. <spring:message code="label.entity.paymentInformation"/></a></li> 
									</c:otherwise>
						 		 </c:choose>
								 <li id="registerStatus" class="link"><a href="/hix/entity/entityadmin/registrationstatus/${assiterEntityId}"><spring:message code="label.entity.registrationStatus"/></a></li>
							</c:when>						
							<c:otherwise>
								<c:choose>
									<c:when test="${menu == 'enrollmentEntityMenu'}">						
							     		<c:choose>
											<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
												<li id="entityInformation" class="link"><a href="/hix/entity/entityadmin/viewentityinformation/${enrollmentEntityUserId}"><spring:message code="label.entity.entityInformation"/></a></li>
											</c:when>
											<c:when test="${fn:contains(sessionScope.navigationmenumap,'ENTITY_INFO')}">
										 		<li id="entityInformation" class="link"><a href="/hix/entity/entityadmin/viewentityinformation/${enrollmentEntityUserId}"><i class="icon-ok"></i><spring:message code="label.entity.entityInformation"/></a></li>
											</c:when>
											<c:otherwise>
										 		<li id="entityInformation"><a href="#">1. <spring:message code="label.entity.entityInformation"/></a></li>
											</c:otherwise>
										</c:choose>
										<c:choose>
											<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
												<li id="populationServed" class="link"><a href="/hix/entity/entityadmin/viewPopulationServed/${enrollmentEntityUserId}"><spring:message code="label.entity.populationsServed"/></a></li>
											</c:when>
											<c:when test="${fn:contains(sessionScope.navigationmenumap,'POPULATION_SERVED')}">
										 		<li id="populationServed" class="link"><a href="/hix/entity/entityadmin/viewPopulationServed/${enrollmentEntityUserId}"><i class="icon-ok"></i><spring:message code="label.entity.populationsServed"/></a></li>      
											</c:when>
									  		<c:otherwise>
												<li id="populationServed"><a href="#">2. <spring:message code="label.entity.populationsServed"/></a></li>      
											</c:otherwise>
										</c:choose>										  
										<c:choose>
											<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
												<li id="locationAndHours" class="link"><a href="/hix/entity/entityadmin/viewsite/${enrollmentEntityUserId}"><spring:message code="label.entity.locationAndHours"/></a>										 										
												 </li>
											</c:when>
											<c:when test="${fn:contains(sessionScope.navigationmenumap,'LOCATION_HOURS')}">									
										 		<li id="locationAndHours" class="link"><a href="/hix/entity/entityadmin/viewsite/${enrollmentEntityUserId}"><i class="icon-ok"></i><spring:message code="label.entity.locationAndHours"/></a>
										 			<ul>
												 		<li><a href="/hix/entity/enrollmententity/primarysite"><spring:message code="label.entity.primarySites"/></a></li>
												 		<li><a href="/hix/entity/enrollmententity/viewsite"><spring:message code="label.entity.subSites"/></a></li>
												 	</ul>							 
												 </li>
											</c:when>
											<c:otherwise>
										 		<li id="locationAndHours"><a href="#">3. <spring:message code="label.entity.locationAndHours"/></a>
										 			<ul>
												 		<li><a href="#"><spring:message code="label.entity.primarySites"/></a></li>
												 		<li><a href="#"><spring:message code="label.entity.subSites"/></a></li>
												 	</ul>  
												 </li>
											</c:otherwise>
										</c:choose>
										<c:choose>
											<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
												<li id="entityContacts" class="link"><a href="/hix/entity/entityadmin/viewentitycontactinfo/${enrollmentEntityUserId}"><spring:message code="label.entity.contactInformation"/></a></li>
											</c:when>
											<c:when test="${fn:contains(sessionScope.navigationmenumap,'ENTITY_CONTACT')}">
											 	<li id="entityContacts" class="link"><a href="/hix/entity/entityadmin/viewentitycontactinfo/${enrollmentEntityUserId}"><i class="icon-ok"></i><spring:message code="label.entity.contactInformation"/></a></li>  
											</c:when>
											<c:otherwise>
											 	<li id="entityContacts"><a href="#">4. <spring:message code="label.entity.contactInformation"/></a></li>
											</c:otherwise>
										</c:choose>
										<c:choose>
											<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
												<li id="assisters" class="link"><a href="/hix/entity/entityadmin/viewlistofassisters/${enrollmentEntityUserId}"><spring:message code="label.assister.assisters"/></a></li>
											</c:when>
											<c:when test="${fn:contains(sessionScope.navigationmenumap,'ASSISTER')}">
											 	<li id="assisters" class="link"><a href="/hix/entity/entityadmin/viewlistofassisters/${enrollmentEntityUserId}"><i class="icon-ok"></i><spring:message code="label.assister.assisters"/></a></li>
											</c:when>
											<c:otherwise>
											  <li id="assisters"><a href="#">5. <spring:message code="label.assister.assisters"/></a></li> 
											</c:otherwise>
										</c:choose>
										<c:choose>	
										<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
							  					<li id="documentupload" class="link"><a href="/hix/entity/entityadmin/documententity/${enrollmentEntityId}"><!-- <i class="icon-ok"></i> --><spring:message code="label.entity.docUpload"/></a></li>							  
										</c:when>									
										<c:when test="${fn:contains(sessionScope.navigationmenumap,'DOCUMENT_UPLOAD')}">											
											  <li id="documentupload" class="link"><a href="/hix/entity/entityadmin/documententity/${enrollmentEntityId}"><i class="icon-ok"></i><spring:message code="label.entity.docUpload"/></a></li> 
										</c:when>
										<c:otherwise>
											  <li id="documentupload"><a href="/hix/entity/entityadmin/documententity/${enrollmentEntityId}">6. <spring:message code="label.entity.docUpload"/></a></li> 
										</c:otherwise>
									  </c:choose>
										  <c:if test="${sessionScope.ID_STATE_CODE || sessionScope.NV_STATE_CODE}">
									  	<c:choose>
										  	<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
										  		<li id="paymentInfo" class="link"><a href="/hix/entity/entityadmin/viewpaymentinfo/${enrollmentEntityId}"><spring:message code="label.entity.paymentInformation"/></a></li>
										  	</c:when>
										  	<c:when test="${fn:contains(sessionScope.navigationmenumap,'PAYMENT_INFO')}">
												  <li id="paymentInfo" class="link"><a href="/hix/entity/entityadmin/viewpaymentinfo/${enrollmentEntityId}"><i class="icon-ok"></i><spring:message code="label.entity.paymentInformation"/></a></li> 
											</c:when>
											<c:otherwise>
												  <li id="paymentInfo"><a href="#">7. <spring:message code="label.entity.paymentInformation"/></a></li> 
											</c:otherwise>
										</c:choose>
										  </c:if>
										<c:choose>
										  	<c:when test="${registrationStatus != null && registrationStatus != '' && registrationStatus != 'Incomplete'}">
												  <li id="registerStatus" class="link"><a href="/hix/entity/entityadmin/registrationstatus/${enrollmentEntityId}"><spring:message code="label.entity.registrationStatus"/></a></li>
											</c:when>										
											<c:when test="${fn:contains(sessionScope.navigationmenumap,'ENTITY_INFO')}">
												<li id="registerStatus" class="link"><a href="/hix/entity/entityadmin/registrationstatus/${enrollmentEntityId}">8. <spring:message code="label.entity.registrationStatus"/></a></li>
											</c:when>
											<c:otherwise>	
												<li id="registerStatus" class="link"><a href="#"><spring:message code="label.entity.registrationStatus"/></a></li>
											</c:otherwise>
										</c:choose>
<%-- 								  	<li id="addAssister" class="link"><a href="/hix/entity/entityadmin/addnewassisterbyadmin?enrollmentEntityId=${enrollmentEntityUserId}&activateAddAssisterLink=true">Add Assister</a></li> 
 --%>								</c:when>
 								<c:when test="${loggedUser == 'assisterLogin'}">	
									<c:choose> 
											<c:when test="${fn:contains(sessionScope.navigationmenumap,'ASSISTER_INFO')}">
										 		<li id="assisterInformation" class="link"><a href="/hix/entity/assister/viewassisterinformationbyuserid"><spring:message code="label.assister.myInformation"/></a></li>
											</c:when>
											<c:otherwise>
										 		<li id="assisterInformation"><a href="/hix/entity/assister/viewassisterinformationbyuserid"><spring:message code="label.assister.myInformation" /></a></li>
											</c:otherwise>
									 </c:choose>
									 <c:choose>
											<c:when test="${fn:contains(sessionScope.navigationmenumap,'ASSISTER_PROFILE')}">
										 		<li id="assisterProfile" class="link"><a href="/hix/entity/assister/viewassisterprofilebyuserid"><spring:message code="label.assister.profile"/></a></li>
											</c:when>
											<c:otherwise>
										 		<li id="assisterProfile"><a href="/hix/entity/assister/viewassisterprofilebyuserid"><spring:message code="label.assister.profile"/></a></li>
											</c:otherwise>
									 </c:choose>
									 <c:choose>
												<c:when test="${fn:contains(sessionScope.navigationmenumap,'ASSISTER_CERTIFICATION')}">
										 		<li id="assisterCertification" class="link"><a href="/hix/entity/assister/certificationstatusbyuserid"><spring:message code="label.assister.certificationStatus"/></a></li>
											</c:when>
											<c:otherwise>
										 		<li id="assisterCertification"><a href="/hix/entity/assister/certificationstatusbyuserid"><spring:message code="label.assister.certificationStatus"/></a></li>
											</c:otherwise>
									 </c:choose>
								</c:when>
 						
								<c:otherwise>
									<c:choose>
											<c:when test="${fn:contains(sessionScope.navigationmenumap,'ASSISTER_INFO')}">
										 		<li id="assisterInformation" class="link"><a href="/hix/entity/assisteradmin/viewassisterinformation?assisterId=${assiterId}"><spring:message code="label.assister.certifiedCounselor"/></a></li>
											</c:when>
											<c:otherwise>
										 		<li id="assisterInformation"><a href="#"><spring:message code="label.assister.certifiedCounselor"/></a></li>
											</c:otherwise>
									 </c:choose>
									 <c:choose>
											<c:when test="${fn:contains(sessionScope.navigationmenumap,'ASSISTER_PROFILE')}">
										 		<li id="assisterProfile" class="link"><a href="/hix/entity/assisteradmin/viewassisterprofile?assisterId=${assiterId}"><spring:message code="label.assister.profile"/></a></li>
											</c:when>
											<c:otherwise>
										 		<li id="assisterProfile"><a href="#"><spring:message code="label.assister.profile"/></a></li>
											</c:otherwise>
									 </c:choose>
									 <c:choose>
												<c:when test="${fn:contains(sessionScope.navigationmenumap,'ASSISTER_CERTIFICATION')}">
										 		<li id="assisterCertification" class="link"><a href="/hix/entity/assisteradmin/certificationstatus?assisterId=${assiterId}"><spring:message code="label.assister.certificationStatus"/></a></li>
											</c:when>
											<c:otherwise>
										 		<li id="assisterCertification"><a href="#"><spring:message code="label.assister.certificationStatus"/></a></li>
											</c:otherwise>
									 </c:choose>
									 <c:choose>
												<c:when test="${fn:contains(sessionScope.navigationmenumap,'ASSISTER_STATUS')}">
										 		<li id="assisterStatus" class="link"><a href="/hix/entity/entityadmin/assisterstatus?assisterId=${assiterId}"><spring:message code="label.assister.status"/></a></li>
											</c:when>
											<c:otherwise>
										 		<li id="assisterStatus"><a href="#"><spring:message code="label.assister.status"/></a></li>
											</c:otherwise>
									 </c:choose>	
									 <c:choose>
										<c:when test="${assisterActivated == false && (sessionScope.ID_STATE_CODE || sessionScope.NV_STATE_CODE)}">
										 	<li id="assisterStatus" class="link"><a href="/hix/entity/assisteradmin/sendactivationlink/${assiterId}"><spring:message code="label.assister.sendActivationLink"/></a></li>
										</c:when>
									 </c:choose>										 	
								</c:otherwise>
						</c:choose>
				</c:otherwise>
			</c:choose>			
		</c:otherwise>
	</c:choose>		
</ul>
</div><!-- #sidebar  --> 
<!-- #sidebar ENDS -->
    