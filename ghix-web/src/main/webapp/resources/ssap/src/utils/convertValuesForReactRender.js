export const convertValuesForReactRender = value => {

    if(Array.isArray(value)) {
        return value.length === 0 ? '[ ]' : `[ ${value.toLocaleString()} ]`;
    }

    if(typeof value === 'object' && value !== null) {
        throw new Error('Object has passed to render');
    }

    let output = value;

    switch(value) {
    case null:
        output = 'NULL'; break;
    case false:
        output = 'FALSE'; break;
    case true:
        output = 'TRUE'; break;
    case undefined:
        output = 'UNDEFINED'; break;
    }

    return output;
};