
package com.getinsured.iex.hub.ssac.extension;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * Request from HUB to SSA for the SSA composite service to verify the following. 
 *                 1. SSN Verification.
 *                 2. Citizenship Verification.
 *                 3. Incarceration Verification.
 *                 4. Title II Monthly Income Verification.
 *                 5. Title II Annual Income Verification.
 *                 6. Quarters of Coverage Verification.
 *             
 * 
 * <p>Java class for SSACompositeRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSACompositeRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}SSACompositeIndividualRequest" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSACompositeRequestType", propOrder = {
    "ssaCompositeIndividualRequest"
})

@XmlRootElement(name = "SSACompositeRequest", namespace="http://ssac.ee.sim.dsh.cms.hhs.gov")
public class SSACompositeRequestType
    extends ComplexObjectType
{

    @XmlElement(name = "SSACompositeIndividualRequest", required = true)
    protected List<SSACompositeIndividualRequestType> ssaCompositeIndividualRequest;

    /**
     * Gets the value of the ssaCompositeIndividualRequest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ssaCompositeIndividualRequest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSSACompositeIndividualRequest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SSACompositeIndividualRequestType }
     * 
     * 
     */
    
    public List<SSACompositeIndividualRequestType> getSSACompositeIndividualRequest() {
        if (ssaCompositeIndividualRequest == null) {
            ssaCompositeIndividualRequest = new ArrayList<SSACompositeIndividualRequestType>();
        }
        return this.ssaCompositeIndividualRequest;
    }

}
