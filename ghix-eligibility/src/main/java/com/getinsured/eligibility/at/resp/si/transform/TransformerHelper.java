package com.getinsured.eligibility.at.resp.si.transform;

import java.util.Date;

import javax.xml.datatype.XMLGregorianCalendar;

import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;

public final class TransformerHelper {

	private TransformerHelper(){}

	public static Date extractDate(DateType inputDate){

		Date date = null;
		if (null != inputDate){
			if (inputDate.getDate() != null && inputDate.getDate().getValue() != null){
				date = inputDate.getDate().getValue().toGregorianCalendar().getTime();
			} else if (inputDate.getDateTime() != null && inputDate.getDateTime().getValue() != null){
				date = inputDate.getDateTime().getValue().toGregorianCalendar().getTime();
			}
		}
		return date;

	}

	public static Date extractDate(XMLGregorianCalendar input){
		return input.toGregorianCalendar().getTime();

	}

}
