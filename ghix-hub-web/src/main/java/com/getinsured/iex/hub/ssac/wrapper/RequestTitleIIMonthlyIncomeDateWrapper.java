package com.getinsured.iex.hub.ssac.wrapper;

import java.io.IOException;

import com.getinsured.iex.hub.GHIXApplicationContext;
import com.google.gson.Gson;


public class RequestTitleIIMonthlyIncomeDateWrapper {
	private static Gson gson ;
	
	static{
		gson = (Gson) GHIXApplicationContext.getBean("iex_platformGson");
	}
	
	private String requestTitleIIMonthlyIncomeDate;
	private com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.String requestTitleIIMonthlyIncomeDateStr;
	private static com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.ObjectFactory xsdFactory;
	//private static com.getinsured.iex.hub.ssac.extension.ObjectFactory extnFactory;
	
	public RequestTitleIIMonthlyIncomeDateWrapper() {
		xsdFactory = new com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.ObjectFactory();
		//extnFactory = new com.getinsured.iex.hub.ssac.extension.ObjectFactory();
	}
	
	public void setRequestTitleIIMonthlyIncomeDate(String yearMonth){
		
		this.requestTitleIIMonthlyIncomeDateStr = xsdFactory.createString();
		this.requestTitleIIMonthlyIncomeDateStr.setValue(yearMonth);
		this.requestTitleIIMonthlyIncomeDate = yearMonth;
	}
	
	public String getRequestTitleIIMonthlyIncomeDate() {
		if(this.requestTitleIIMonthlyIncomeDateStr == null){
			return null;
		}
		return this.requestTitleIIMonthlyIncomeDate;
	}
	
	public static RequestTitleIIMonthlyIncomeDateWrapper 
		fromJsonString(String jsonString) throws IOException{
		return gson.fromJson(jsonString, RequestTitleIIMonthlyIncomeDateWrapper.class);
	}
	
	public com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.String getSystemRepresentation(){
		return this.requestTitleIIMonthlyIncomeDateStr;
	}
	
	/*public static void main(String[] args) throws JAXBException, IOException {
		
		RequestTitleIIMonthlyIncomeDateWrapper wrapper = fromJsonString("{\"requestTitleIIMonthlyIncomeDate\":\"201212\"}");
		SSACompositeIndividualRequestType req = extnFactory.createSSACompositeIndividualRequestType();
		req.setRequestTitleIIMonthlyIncomeDate(wrapper.getRequestTitleIIMonthlyIncomeDate());
		JAXBElement<SSACompositeIndividualRequestType> ssaCompReq = extnFactory.createSSACompositeIndividualRequest(req);
		JAXBContext jc = JAXBContext.newInstance("com.getinsured.hub.ssac.extension");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(ssaCompReq, System.out);
	}*/

}
