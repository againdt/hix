package com.getinsured.hix.batch.ssap.service;

import com.getinsured.hix.enrollment.service.AdminUpdateService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service("updateEnrlEventForExternalAppIdsService")
public class UpdateEnrlEventForExternalAppIdsServiceImpl implements UpdateEnrlEventForExternalAppIdsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateEnrlEventForExternalAppIdsServiceImpl.class);
	private static final String RESPONSE_SUCCESS = "SUCCESS";

	@Autowired
	private AdminUpdateService adminUpdateService;

	@Override
	public void processUpdateEnrlEvent(String ssapAppIdsFilePath,Long jobId, List<String> issuerIds) throws GIException {
		try {
			Path path = Paths.get(ssapAppIdsFilePath);
			String absolutePath = path.getParent().toString() + File.separator + jobId + "_EnrlEventUpdateProcessStatus.csv";
			FileReader filereader = new FileReader(ssapAppIdsFilePath);
			FileWriter fileWriter = new FileWriter(absolutePath);
			CSVReader csvReader = new CSVReader(filereader);
			CSVWriter csvWriter = new CSVWriter(fileWriter);
			String[] nextRecord;

			// Reading data line by line if no delimiter is specified
			while ((nextRecord = csvReader.readNext()) != null) {
				for (String record : nextRecord) {
					try {
						if (record != null && !record.isEmpty()) {
							Long ssapAppId = Long.valueOf(record);

							if (RESPONSE_SUCCESS.equals(adminUpdateService.updateExternalAppID(ssapAppId, issuerIds))) {
								LOGGER.debug("processUpdateEnrlEvent::successful update of enrollments for app id {}",ssapAppId);
								csvWriter.writeNext(new String[] { record, "SUCCESS" });
							} else {
								LOGGER.error("processUpdateEnrlEvent::failed to updateExternalAppID for app id {}",ssapAppId);
								csvWriter.writeNext(new String[] { record, "FAILURE" });
							}

						} else {
							LOGGER.error("processUpdateEnrlEvent::could not parse line into long for ssap app id {}",record);
							csvWriter.writeNext(new String[] { record, "FAILURE" });
						}
					} catch (Exception ex) {
						LOGGER.error("processUpdateEnrlEvent::error updating enrollment event for ssapAppId {}", record,ex);
						csvWriter.writeNext(new String[] { record, "FAILURE" });
					}
				}
			}

			try {
				csvReader.close();
				csvWriter.close();
			} catch (IOException ioe) {
				LOGGER.error("processUpdateEnrlEvent::error closing csv reader/writer", ioe);
			}
		} catch (Exception ex) {
			LOGGER.error("processUpdateEnrlEvent::error processing ssap apps to update enrolment event", ex);
		}
	}
}
