<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<!-- Tag library for showing comments -->
<form class="form-horizontal" id="frmAppealSummary" name="frmAppealSummary" action="#" method="POST">
<df:csrfToken/>
 <input type="hidden" name="appeal_mode" id="appeal_mode" value="update" >
<input type="hidden" name="no_of_appeals" id="no_of_appeals" value="${totalNoOfAppeals}" >
<div class="gutter10">
	<div class="row-fluid">
		<ul class="page-breadcrumb">
			<li><a href="#">&lt; <spring:message code="label.back" /></a></li>
			<li><a href="#">Account</a></li>
			<li>Manage Account Information</li>
		</ul>
		<!--page-breadcrumb ends-->
		<h1 id="skip">${employerName}</h1>
	</div>

	<div class="row-fluid">
		<div class="span3" id ="sidebar">
           <div class="header">
               <h4 class="margin0"><spring:message code="label.emplacc"/></h4>
              </div>
               <!--  beginning of side bar -->
	           <ul class="nav nav-list">
					<li><a href="<c:url value="/shop/employer/accounts/acccompanyinfo" />"><spring:message code="label.compinfo"/></a></li>
	           	    <li><a href="<c:url value="/shop/employer/application/attestationresult" />"><spring:message code="label.eligibilityresults"/></a></li>
	           	    <c:if test="${not empty appealList}">
	                	<li class="active"><a href="#"><spring:message code="label.appealsummary"/></a></li>
	                </c:if>	
	           </ul>
	           <!-- end of side bar -->
         
           <c:if test="${not empty appealList}">
	           <div class="gutter10-t">
		           <div class="gray">
		               
		               <!--  beginning of side bar -->
		               <c:if test="${lastAppealStatus == 'Needinfo' || lastAppealStatus == 'New' || lastAppealStatus == 'NEEDINFO' || lastAppealStatus == 'NEW'}">
		               	<h4 class="margin0"><spring:message code="label.actions"/></h4>
				           <ul class="nav nav-list">
								<li><a class=""  href="#"  onClick="javascript:submitappeal('cancel','post');"><spring:message code="label.withdrawapplication"/></a></li>
				           	    <li><a href="#"  onClick="javascript:submitappeal('update','get');"><spring:message code="label.updateappeal"/></a></li>
				           </ul>
			           </c:if>
			           <c:if test="${isAppealReviewAllowed == true}">
		               	<h4 class="margin0"><spring:message code="label.actions"/></h4>
				           <ul class="nav nav-list">
				           	    <li><a href="#"  onClick="javascript:submitappeal('create','get');"><spring:message code="label.requestappealreview"/></a></li>
				           </ul>
			           </c:if>
			           <!-- end of side bar -->
		           </div>  
	           </div> 
	       </c:if>          
		</div>
		<!-- end of span3 -->
		<div class="span9" id="rightpanel">
			<!--  start list iteration -->
				<c:if test="${empty appealList}">
					<div class="header">
						<h4 class="span10">No appeals are filed yet.</h4>
					</div>
					<!--  show original eligibility status -->
					<div style="height: 100px" class="gutter10" >
						&nbsp;
					</div>
				</c:if>
			
				<c:if test="${not empty appealList}">
					<div class="header">
						<h4 class="span10">Current Eligibility Status</h4>
					</div>
					<!--  show original eligibility status -->
					<p class="gutter10-l gutter10-b"><strong>${currentEligibilityStatus} on ${currentEligibilityDate} </strong></p>
				</c:if>
				<div class="row-fluid">
				<!--  start list iteration -->
					<c:forEach var ="appealDetails" items="${appealList}"  varStatus="appealCnt">
						<div class="header">
							<h4 class="span10">
								<c:choose>
									<c:when test="${totalNoOfAppeals == 2 && appealCnt.first}">
										<spring:message code="label.appealReviewHead"/>	
									</c:when>
									<c:otherwise>
										<spring:message code="label.appealHead"/>
									</c:otherwise>
								</c:choose>
									 
							</h4>
						</div>
						<div class="gutter10">
												
    							<div class="header">
									<h4><spring:message code="label.results"/></h4>
								</div>
									<table class="table table-border-none">
										<c:if test="${appealDetails.status == 'Canceled' || appealDetails.status == 'Denied' || appealDetails.status == 'Approved' }">
											<tr>
												<td class="txt-right span2"><spring:message code="label.appealstatus"/></td>
												<td><strong>
												<c:choose>
													<c:when test="${totalNoOfAppeals == 2 && appealCnt.first}">
														<spring:message code="label.appealReviewHead"/>	
													</c:when>
													<c:otherwise>
														<spring:message code="label.appealHead"/>
													</c:otherwise>
												</c:choose>
								 				${appealDetails.status} <spring:message code="label.on"/>  ${appealDetails.exchangeUpdateDate}</strong></td>
											</tr>
										</c:if>
										<tr>
											<td class="txt-right span2"><spring:message code="label.eligibilitystatus"/></td>
											<td><strong><spring:message code="label.denied"/> ${appealDetails.eligibilityStatusDate}</strong></td>
										</tr>
										
										<tr>
											<td class="txt-right span2"><spring:message code="label.eligibilitydecisionfactors"/></td>
											<td><strong>${appealDetails.statusNote}</strong></td>
										</tr>
									</table>
							
							
							<input type="hidden" name="ticketId" id="ticketId" value="${appealDetails.ticketId}"/>
							
							  
								<c:forEach var="eachComment" items="${appealDetails.allCommentList}" varStatus="loop">
									<fmt:formatDate pattern="MM/dd/yyyy" value="${eachComment.updated}" var="eachCommentDate"/>
									<div class="header"><h4>
										<c:choose>
											<c:when test="${loop.last}">
												<spring:message code="label.appealsubmitted"/>
											</c:when>
											<c:otherwise>
												<spring:message code="label.appealupdated"/>
											</c:otherwise>
										</c:choose>
										<spring:message code="label.by"/> ${eachComment.commenterName} <spring:message code="label.on"/> ${eachCommentDate}
									</h4></div>
									<table class="table table-border-none">
													<tr>
														<td class="txt-right span2"><spring:message code="label.comments"/></td>
														<td><strong>${eachComment.comment}</strong></td>
													</tr>
									</table>	
										
								</c:forEach>
					</div>					
			<!--  stop list iteration -->
				</c:forEach>
				
			<c:if test="${not empty appealList}">
				<div class="gutter10">
					<div class="header"><h4><spring:message code="label.attachment"/></h4></div>
						<table class="table table-border-none">
						<c:forEach var ="appealDetails" items="${appealList}"  varStatus="appealCnt">
							<c:if test="${fn:length(appealDetails.docNameList) > 0}">
									<tr>
										<td class="txt-right span2"></td>
										<td>
											<c:forEach var="docname" items="${appealDetails.docNameList}" varStatus="loop">
													<strong><strong>${docname}</strong><br/>
											</c:forEach>
								   		</td>
								   	</tr>
							</c:if>					
						</c:forEach>
						</table>
					</div>			
				</div>					
			</c:if>
				
				<c:if test="${not empty appealList}">
					<div class="gutter10">
						<div class="header"><h4><spring:message code="label.rightsandresp"/></h4></div>
						<div class="gutter10">
							<ul>
								<li><spring:message code="label.rightsandrespDesc1"/></li>
								<li><spring:message code="label.rightsandrespDesc2"/></li>
								<li><spring:message  code="label.righttoappeal4"/>&nbsp;${maxAppealDays}&nbsp;<spring:message  code="label.righttoappealDays"/></li>
								<li><spring:message code="label.rightsandrespDesc4"/></li>
							</ul>
						</div>
					</div>
				</c:if>	
			</div>
			
			<br>
			<div class="row-fluid"></div>
			<!--row-fluid-->
		</div>
		<!--rightpanel-->
	</div>
	<!--row-fluid-->
</div>
</form>
<!--gutter10-->
<script type="text/javascript">

	
function submitappeal(strAction,strMethod){
	if(strAction == 'create'){
		$('#appeal_mode').val("create");
	}
	
	$("#frmAppealSummary").attr("action", strAction);
	$("#frmAppealSummary").attr("method", strMethod);
	$("#frmAppealSummary").submit();
}
</script>