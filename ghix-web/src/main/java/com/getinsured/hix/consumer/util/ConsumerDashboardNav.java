
package com.getinsured.hix.consumer.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class ConsumerDashboardNav extends TagSupport {
	private static final String ACCOUNT_SETTINGS = "Account Settings";
	private static final String MY_PLANS = "My Plans";
	private static final String MY_ELIGIBILITY = "My Eligibility";
	private static final String MY_HOUSEHOLD = "My Household";
	private static final String DASHBOARD = "Dashboard";

	private static final long serialVersionUID = 1L;
	
    private JspWriter out;
	private HttpServletRequest request;
    //Flag to disable/enable links like my household/ my eligibility
	//FIXME: change attribute name, it should not be coupled with business entity
    private boolean ffmClear;
    private Map<String,String> navigation;
    private Map<String,String> linkImage;
    private Set<String> alwaysEnabledLinks;
    
    private static final String DASHBOARD_LINK = "/memberportal/home";
    private static final String MYHOUSEHOLD_LINK = "/memberportal/myHousehold";
    private static final String MYELIGIBILITY_LINK = "/memberportal/myeligibility";
    private static final String MYPLANS_LINK = "/memberportal/myplans";
    private static final String MYACCOUNT_LINK = "/memberportal/accountSettings";
    
	private static final String TEMPLATE = "<div class='header' id='list-1'><h4>My Stuff</h4></div>" +
										   " <ul class='nav nav-list'>" +
										     " <MESSAGE> " +
										    " </ul>";
	public boolean isFfmClear() {
		return ffmClear;
	}

	public void setFfmClear(boolean ffmClear) {
		this.ffmClear = ffmClear;
	}

	@Override
	public int doStartTag() throws JspException {
		this.setProperties();
		try {
	            this.out.println(ConsumerDashboardNav.TEMPLATE.replaceAll("<MESSAGE>", this.createNavigation()));
	        } catch (Exception ex) {
	            throw new JspException("Error in Consumer dashboard tag", ex);
	        }
	    return SKIP_BODY;
	}
	
	private String getURL() {
    	return this.request.getAttribute("javax.servlet.forward.request_uri").toString();
	}
	
	private String createNavigation(){
    	StringBuilder navigationNodes = new StringBuilder();
    	String defaultClass = "";
    	String link = "";
    	
    	ArrayList<String> pages = new ArrayList<String>();  
    	pages.add(MY_HOUSEHOLD);
    	pages.add(MY_ELIGIBILITY);
    	pages.add(MY_PLANS);
		for (Map.Entry<String, String> entry : this.navigation.entrySet()) {
		    navigationNodes.append(" <li ");
		    link = entry.getValue();
		    if(ffmClear || this.alwaysEnabledLinks.contains(entry.getKey())){
		    	defaultClass = "";
		    }else{
		    	defaultClass = "disabled";
		    	link = "#";
		    }
		    /*
		    if(employee.getStatus().name().equals("NOT_ENROLLED")){
		    	if(pages.contains( entry.getKey()) ){
		    		defaultClass = "disabled";
		    		link = "#";
		    	}else{
		    		defaultClass = "";
		    	}
		    }*/
		    //defaultClass = "";
		    
		    navigationNodes.append( (this.getURL().equals(entry.getValue())) ? "class='active'>":"class='"+defaultClass+"'>");
		    navigationNodes.append( "<a href='"+link+"'>");
		    navigationNodes.append( this.linkImage.get(entry.getKey()));
		    navigationNodes.append( entry.getKey() + "</a></li>");
		}
    	return navigationNodes.toString();
	}
	
	private void setProperties(){
    	this.out     = pageContext.getOut();
    	this.request = (HttpServletRequest)pageContext.getRequest();
    	this.navigation = new LinkedHashMap<String, String>();
    	this.linkImage = new HashMap<String, String>();
    	this.alwaysEnabledLinks = new HashSet<String>();
    	
    	this.navigation.put(DASHBOARD, this.request.getContextPath() + DASHBOARD_LINK);
    	this.navigation.put(MY_HOUSEHOLD, this.request.getContextPath() + MYHOUSEHOLD_LINK);
    	this.navigation.put(MY_ELIGIBILITY, this.request.getContextPath() + MYELIGIBILITY_LINK);
    	this.navigation.put(MY_PLANS, this.request.getContextPath() + MYPLANS_LINK);
    	this.navigation.put(ACCOUNT_SETTINGS, this.request.getContextPath() + MYACCOUNT_LINK);
    
    	
    	this.linkImage.put(DASHBOARD, "<span><i class='icon-signal icon-white'></i></span>");
    	this.linkImage.put(MY_HOUSEHOLD, "<span><i class='icon-info-sign icon-white'></i></span>");
    	this.linkImage.put(MY_ELIGIBILITY, "<span><i class='icon-home icon-white'></i></span>");
    	this.linkImage.put(MY_PLANS, "<span><i class='icon-shopping-cart icon-white'></i></span>");
    	this.linkImage.put(ACCOUNT_SETTINGS, "<span><i class='icon-cog icon-white'></i></span>");
    	
    	//These links will not be affected by any condition
    	this.alwaysEnabledLinks.add(ACCOUNT_SETTINGS);
    	this.alwaysEnabledLinks.add(DASHBOARD);
    }
		
}

