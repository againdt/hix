<%@ include file="../include.jsp" %>
<div id="appscr81A" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page31A.medicaidAndCHPSpecific" text="Medicaid & CHP+ Specific Questions for" />
				<strong class="nameOfHouseHold"></strong>
			</h4>
		</div>
		<form id="appscr81AForm" class="form-horizontal">
			<div class="control-group">
				<p>
					<spring:message code="ssap.was" text="Was" />&nbsp;<strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page31A.uninsuredWithin" text="uninsured within the last six months" />?
				</p><!-- end of label -->
				<div class="margin5-l">
					<label>
						<input type="radio" name="wasUninsuredWithInTheLast6MonthsP81A" value="Yes" id="male_0" checked="checked"> <spring:message code="ssap.yes" text="Yes" />
					</label>
					
					<label>
						<input type="radio" name="wasUninsuredWithInTheLast6MonthsP81A" value="No" id="male_1"><spring:message code="ssap.no" text="No" />
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.does" text="Does" />&nbsp;<strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page31A.haveHealth" text="have health insurance now" />?
				</p><!-- end of label -->
				<div class="margin5-l">
					<label>
						<input type="radio" name="doesHaveHealthInsuranceNowP81A" value="Yes" id="male_0" checked="checked"> <spring:message code="ssap.yes" text="Yes" />
					</label>
					
					<label>
						<input type="radio" name="doesHaveHealthInsuranceNowP81A" value="No" id="male_1"><spring:message code="ssap.no" text="No" />
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page31A.whatHealthInsurance" text="What health insurance does" />&nbsp;<strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page31A.haveNow" text="have now" />?
				</p><!-- end of label -->
				<div class="margin5-l">
					<label>
						<input type="radio" name="whatHealthInsuranceDoesHaveNow" value="Yes" id="male_0" checked="checked"> <spring:message code="ssap.page31A.planFromHealth" text="[Plan from Health Coverage Page]" />
					</label>
					<label>
						<input type="radio" name="whatHealthInsuranceDoesHaveNow" value="No" id="male_1"> <spring:message code="ssap.page31A.coloradoMedicaid" text="[New Mexico Medicaid]" />
					</label>
					<label>
						<input type="radio" name="whatHealthInsuranceDoesHaveNow" value="Yes" id="male_0"> <spring:message code="ssap.page31A.coloradoChip" text="[New Mexico CHIP]" />
					</label>
					<label>
						<input type="radio" name="whatHealthInsuranceDoesHaveNow" value="No" id="male_1"> <spring:message code="ssap.page31A.medicare" text="Medicare" />
					</label>
					<label>
						<input type="radio" name="whatHealthInsuranceDoesHaveNow" value="Yes" id="male_0"> <spring:message code="ssap.page31A.insuranceThroughAn" text="Insurance through an employer" />
					</label>
					<label>
						<input type="radio" name="whatHealthInsuranceDoesHaveNow" value="No" id="male_1"> <spring:message code="ssap.page31A.veteransOrTricare" text="Veterans or TRICARE" />
					</label>
					<label>
						<input type="radio" name="whatHealthInsuranceDoesHaveNow" value="Yes" id="otherHealthPlanRadioOnPage81P1"> <spring:message code="ssap.county.Other" text="Other" />
					</label>
				</div><!-- end of controls-->
				<div id="otherHealthPlanOnPage80P1" class="hide">
					<div class="control-group">
						<p>
							<spring:message code="ssap.page31A.whatIsTheHealthPlan" text="What is the health plan called" />?
						</p>
						<label>
							<input type="text" placeholder="<spring:message code="ssap.page27B.healthPlanName" text="Health Plan Name"/>" class="input-medium" id="appscr81AHealthName" name="appscr81AHealthName" />
						</label><!-- end of label -->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<p>
							<spring:message code="ssap.page31A.whatIsThePolicyNumber" text="What is the policy number or member ID" />?
						</p>
						<label>
							<input type="text" placeholder="Policy Number/Member ID"  class="input-medium" id="appscr81APolicynum" name="appscr81APolicynum" />
						</label><!-- end of label -->
					</div><!-- control-group ends -->
					
					<div class="control-group margin0">
						<p>
							<spring:message code="ssap.does" text="Does" />&nbsp;<strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page31A.haveAPrimaryCare" text="have a primary care physician" />?
						</p>
						<label>
							<input type="radio" name="doesHaveAPrimaryCarePhysicianP81A" value="Yes" id="male_0" checked="checked"> <spring:message code="ssap.yes" text="Yes" />
						</label>
						<label>
							<input type="radio" name="doesHaveAPrimaryCarePhysicianP81A" value="No" id="male_1"><spring:message code="ssap.no" text="No" />
						</label>
						
						<div id="primaryPhysicianInfoOnPage81P1">
							<div class="control-group margin0">
								<p class="pull-left margin5-t margin10-r">
									<spring:message code="ssap.primaryCarePhysician" text="Primary Care Physician Name" />
								<input type="text" placeholder="<spring:message code="ssap.primaryCarePhysician" text="Primary Care Physician Name" />"  class="input-large" id="appscr81APhyName" name="appscr81APhyName"/>
								<label class="checkbox">
									<input type="checkbox" class="margin0-l" /><spring:message code="ssap.iDontKnow" text="I don't know" />
								</label><!-- end of label -->
							</div><!-- control-group ends -->
						</div><!-- primaryPhysicianInfoOnPage81P1 ends -->
						
					</div><!-- control-group ends -->
				</div><!-- otherHealthPlanOnPage80P1 ends -->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page31A.has" text="Has" />&nbsp;<strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page31A.everGottenAHealthService" text="ever gotten a health service from the Indian Health Service, a tribal health program, or urban Indian health program or through a referral from one of these programs" />?
				</p>
				<label>
					<input type="radio" name="hasGotAHealthServiceFromTheIndianHealthServiceP81A" value="Yes" id="male_0" checked="checked"> <spring:message code="ssap.yes" text="Yes" />
				</label>
				<label>
					<input type="radio" name="hasGotAHealthServiceFromTheIndianHealthServiceP81A" value="No" id="male_1"><spring:message code="ssap.no" text="No" />
				</label>
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.is" text="Is" />&nbsp;<strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page31A.eligibleToGetHealth" text="eligible to get health services from Indian Health Services or a Tribal Health Organization" />?
				</p>
				<label>
					<input type="radio" name="isEligibleForIndianHealthServiceP81A" value="Yes" id="male_0" checked="checked"> <spring:message code="ssap.yes" text="Yes" />
				</label>
				<label>
					<input type="radio" name="isEligibleForIndianHealthServiceP81A" value="No" id="male_1"><spring:message code="ssap.no" text="No" />
				</label>
			</div><!-- control-group ends -->
		</form>
	</div>
</div>