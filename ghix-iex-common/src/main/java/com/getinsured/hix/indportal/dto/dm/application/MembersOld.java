package com.getinsured.hix.indportal.dto.dm.application;

import java.util.Map;

public class MembersOld {
	private  Map<String,Member> members;

	public Map<String, Member> getMembers() {
		return members;
	}

	public void setMembers(Map<String, Member> members) {
		this.members = members;
	}
	 
	}