package com.getinsured.ssap.model.financial;

/**
 * Describes tax filing requirement types.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-10
 */
public enum TaxFilingRequirementType {
  /**
   * Single under age 65
   */
  SINGLE_UNDER_65,

  /**
   * Single age 65 or older
   */
  SINGLE_OVER_65,

  /**
   * Single age 65 or older AND blind.
   */
  SINGLE_BLIND_OVER_65,

  /**
   * Married filing jointly, both spouses under 65
   */
  MFJ_BOTH_UNDER_65,

  /**
   * Married filing jointly, one spouse age 65 or older
   */
  MFJ_ONE_OVER_65,

  /**
   * Married filing jointly, both spouses 65 or older
   */
  MFJ_BOTH_OVER_65,

  /**
   * Married filing separately, any age
   */
  MFS,

  /**
   * Head of household under age 65
   */
  HOH_UNDER_65,

  /**
   * Head of household age 65 or older
   */
  HOH_OVER_65,

  /**
   * Qualifying widow(er) under age 65
   */
  QW_UNDER_65,

  /**
   * Qualifying widow(er) age 65 or older
   */
  QW_OVER_65
}
