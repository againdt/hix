<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 

<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>
<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<!-- End Google Analytics -->	

<style type="text/css">
@media screen and (-webkit-min-device-pixel-ratio:0) {    
	iframe #success {
		display: none;
	}      
}
@-moz-document url-prefix() {       
	iframe #success {
		overflow-x: none;
	} 
}
span.paddingT5{display: block;}
</style>

<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<jsp:useBean id="now" class="java.util.Date" />

<div class="gutter10">
	<div class="row-fluid">
		<h1>
			<spring:message  code="label.brkagentdesignation"/>&#58; 
			<c:choose>
		   		<c:when test="${CA_STATE_CODE}">
				</c:when>
				<c:otherwise>
					<small><spring:message  code="label.brkattestations"/></small>
				</c:otherwise>
		   </c:choose>
		</h1>
	</div>
	<div class="row-fluid">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="rightpanel">
		<c:set var="encryptedId" ><encryptor:enc value="${id}" isurl="true"/> </c:set>
			<form class="form-horizontal" id="frmDesigBrkEsign"
				name="frmDesigBrkEsign" method="POST">
				<df:csrfToken/>
				<div class="">
					<h4><spring:message  code="label.brkagenttobedesignated"/>&#58; ${broker.user.firstName}
						${broker.user.lastName}</h4>
					<div class="control-group gutter10">
						<div class="control-group">
							<label class="checkbox"> <input type="checkbox"
								class="terms" name="terms1" id="terms1" value="1"><spring:message code="label.brkgrantpermission"/>
							</label>
							<div id="terms1_error"></div>
						</div>
						<div class="control-group">
							<label class="checkbox"> <input type="checkbox"
								class="terms" name="terms2" id="terms2" value="2"><spring:message code="label.brkterminatepartnership"/>
							</label>
							<div id="terms2_error"></div>
						</div>
						<div class="control-group">
							<label class="checkbox"> <input type="checkbox"
								class="terms" name="terms3" id="terms3" value="3"><spring:message code="label.brkgrantpaymentinfo"/>
							</label>
							<div id="terms3_error"></div>
						</div>
						<c:if test="${CA_STATE_CODE}">
							<div class="control-group">
								<label class="checkbox"> <input type="checkbox"
									class="terms" name="terms4" id="terms4" value="4"><spring:message code="label.brkauthbyindividual"/>
								</label>
								<div id="terms4_error"></div>
							</div>
						</c:if>
					</div>
				</div>

				<h4 class=""><spring:message code="label.brksignature"/></h4>
				<input type="hidden" name="id" id="id" value="<encryptor:enc value="${id}" isurl="true"/>" />

				<div class="control-group">
					<label class="control-label" for="appName"><spring:message code="label.brkapplicantname"/></label>
					<div class="controls">
						<input type="hidden" class="input-xlarge" id="appName"
							name="appName" value="${applicantName}"> 
							<span class="paddingT5">${applicantName}</span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="esignName">
						<spring:message code="label.brkapplicantesignature"/>
						<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
					</label>
					<div class="controls">
						<input type="text" class="input-xlarge" id="esignName"
							name="esignName" size="30"> <br> <small><spring:message code="label.brkelectronicsignature"/></small>
						<div id="esignName_error"></div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="today_mm"><spring:message code="label.brktoday"/>&#39;<spring:message code="label.brksdate"/></label>
					<label class="control-label aria-hidden" for="today_dd"><spring:message code="label.brktoday"/>&#39;<spring:message code="label.brksdate"/></label>
					<label class="control-label aria-hidden" for="today_yy"><spring:message code="label.brktoday"/>&#39;<spring:message code="label.brksdate"/></label>
					<div class="controls">

						<input type="text" readonly class="input-mini" maxlength="2"
							id="today_mm" name="today_mm"
							value="<fmt:formatDate value="${now}" type="both" pattern="MM" />">
						<input type="text" readonly class="input-mini" maxlength="2"
							id="today_dd" name="today_dd"
							value="<fmt:formatDate value="${now}" type="both" pattern="dd" />">
						<input type="text" readonly class="input-mini" size="4"
							maxlength="4" id="today_yy" name="today_yy"
							value="<fmt:formatDate value="${now}" type="both" pattern="yyyy" />">
					</div>
				</div>

				<div class="form-actions">
					<input type="button" name="submitButton" id="submitButton"
						onClick="javascript:validateForm();"
						class="btn btn-primary pull-right" value="<spring:message code='label.brkConfirm'/>"  title="<spring:message code='label.brkNext'/>" />
				</div>
			</form>
		</div>
	</div>
</div>


<script type="text/javascript">
	function ie8Trim(){
		if(typeof String.prototype.trim !== 'function') {
	        String.prototype.trim = function() {
	        	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
	        }
		}
	}

	var validator = $("#frmDesigBrkEsign").validate({
						rules : {
							esignName : {
								required : true,
								esignName : true
							},
							terms1 : {
								required : true
							},
							terms2 : {
								required : true
							},
							terms3 : {
								required : true
							},
							terms4 : {
								required : true
							}
						},
						messages : {
							esignName : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateApplicantEsig' javaScriptEscape='true'/></span>",
								esignName : "<span> <em class='excl'>!</em><spring:message code='err.brkesignature' javaScriptEscape='true'/></span>"
							},
							terms1 : {
								required : "<span> <em class='excl'>!</em><spring:message code='err.brkacceptattestation' javaScriptEscape='true'/></span>"
							},
							terms2 : {
								required : "<span> <em class='excl'>!</em><spring:message code='err.brkacceptattestation' javaScriptEscape='true'/></span>"
							},
							terms3 : {
								required : "<span> <em class='excl'>!</em><spring:message code='err.brkacceptattestation' javaScriptEscape='true'/></span>"
							},
							terms4 : {
								required : "<span> <em class='excl'>!</em><spring:message code='err.brkacceptattestation' javaScriptEscape='true'/></span>"
							}
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error").attr('class','error span10');
						},
						success: function(error, element){
							var elementId = $(element).attr('id');
							$("#" +elementId+ "_error").attr('class','error span10').html('<label for="'+elementId+'" generated="true" class="error"><span> <em class="excl">!</em><spring:message code="err.brkacceptattestation" javaScriptEscape="true"/></span></label>').hide();
						}
					});

/*checkbox event on designate pop-up*/
$("#frmDesigBrkEsign input[type='checkbox']").each(function(index, element) {
    $(this).click(function(){
    	var checkId = $(this).attr('id');
		if(!$(this).is(':checked')){
			$("#"+checkId+"_error").show();
		}
		else{
			$("#"+checkId+"_error").hide();
		}
	});
});

/*textbox blur event on designate pop-up*/
$("#esignName").blur(function(){
		var checkId = $("#esignName").attr('id');
    	var signVal = $("#esignName").val();
		var appName = $("#appName").val();
    	
		if (validateText(appName) != validateText(signVal)) {
			$("#"+checkId+"_error").show();
		}
		else{
			$("#"+checkId+"_error").hide();
		}
});

	jQuery.validator.addMethod("esignName", function(value, element, param) {
		appName = $("#appName").val();
		confirmEsignName = $("#esignName").val();

		if (validateText(appName) != validateText(confirmEsignName)) {
			return false;
		}
		return true;
	});
	
	function validateText(textVal) {
		ie8Trim();
		if(textVal != null) {
			return textVal.toUpperCase().trim();
		}
	}
	
	function validateForm() {
		var id = $("#id").val();
		//var href = "/hix/broker/acceptAttestations";
		if ($(".terms:checked").length < 3) {
			//$('<div id="chkAlert" class="modal"><div class="modal-header" style="border-bottom:0;"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button></div><div class="modal-body"><iframe id="success" src="'+ href +'" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:100px;"></iframe></div></div>').modal({backdrop:false});
			/* $(
					'<div id="chkAlert" class="modal" style="top:80%"><div class="modal-header" style="border-bottom:0;"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button></div><div class="modal-body"><iframe id="success" src="#" style="overflow-x:hidden;width:25%;border:0;margin:0;padding:0;height:20px;display:none;"></iframe> Please accept all attestations! <br/> <br/> <button class="btn offset2" data-dismiss="modal" aria-hidden="true">Ok</button></div></div>')
					.modal({
						backdrop : false
					}); */
		}
		if ($("#frmDesigBrkEsign").validate().form()) {
			$("#frmDesigBrkEsign").attr("action", "/hix/broker/esignatureSubmit/" + id);
			$("#frmDesigBrkEsign").submit();
		}
	}

	function closeAttestationAlert() {
		$("#chkAlert").remove();
	}
	
	function ie8Trim(){	
		if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
            	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
            }
		}
	}
</script>
