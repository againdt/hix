<%@ taglib prefix="audit" uri="/WEB-INF/tld/audit-view.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
			
       
<div class="gutter10">
	<div class="row-fluid">
	<c:if test="${loggedUser =='entityAdmin'}">
	<div >
		<h1 style="margin-top: 0px;margin-bottom: 0px;"><a name="skip"></a>${enrollmentEntity.entityName}</h1>
	</div>	
	</c:if>
</div>	

	<div class="row-fluid">
		
	<jsp:include page="/WEB-INF/views/entity/leftNavigationMenu.jsp" />
				
	<div class="span9" id="rightpanel">	
	<div class="gutter10">
	 <form class="form-horizontal" id="frmupdatecertification"  enctype="multipart/form-data" name="frmupdatecertification" action="<c:url value="/entity/entityadmin/uploadsubmit"/>"  method="POST">	 
	 <df:csrfToken/>
			<input type="hidden" id="entityId" name="entityId"  value="<encryptor:enc value="${enrollmentEntity.id}"/>"/>
			<input type="hidden" id="documentId" name="documentId" value="<encryptor:enc value="${uploadedDocument}"/>"/>
			<input type="hidden" id="fileToUpload" name="fileToUpload" value="fileInput"/>					
										
<!-- 					<div class="header"> -->
<%-- 						<h4 class="span10"><spring:message code="label.brkCertificationStatus"/></h4> --%>
<%-- 							<a class="btn btn-small" type="button"  href="/hix/admin/broker/certificationstatus/${broker.id}"><spring:message code="label.brkCancel"/></a> --%>
<!-- 					</div> -->
                     
					<div class="header">
					<h4><spring:message code="label.entity.docUpload"/></h4>
					</div>				
						<!-- header class for accessibility -->
									<div class="margin20-b">
									<div id="progress">
		                   		     		                   				
		                   				<h5><spring:message code="label.entity.uploading"/></h5>
		                   					  
		                   				<div style="width:100px;" id="progressbar"></div>
		                   				                	   			
		                   			</div>
		                   		</div>			                   				
		                   					                   	   			
		                   			
	                  				<div class="control-group">
									<c:choose>          
         	 							<c:when test="${enrollmentEntity.registrationStatus!='Incomplete'}">         	                  				
				                   			<div class="controls margin0"><label for="fileInput" class="control-label margin20-r"><spring:message code="label.brkUploadSupportingDocs"/></label>	
				                   				                   				
				                   				<input type="file" class="input-file" id="fileInput" name="fileInput">
				                   				<input type="hidden" id="fileInput_Size" name="fileInput_Size" value="1"/>			                   				
				                   				<input type="button" id="btnUploadDoc" name="btnUploadDoc" value="<spring:message code="label.assister.upload"/>" class="btn" onClick="submitForm('upload')" />		                   	   			
				                   			</div>
				                   			<div>
												<spring:message code="label.uploadDocumentCaption"/>
											</div>
				                   		</c:when>
				                   	</c:choose>		
	                  				</div>																										
				
				<input type="hidden" name="formAction" id="formAction" value="" />			
		</form>
				
			<div class="gutter10">				
					<display:table id="enrollmentRegisterStatusHistory" name="enrollmentRegisterStatusHistory" list="enrollmentRegisterStatusHistory" requestURI="" sort="list" class="table table-condensed table-border-none table-striped" >
				           <display:column property="Date" titleKey="label.entity.date"  format="{0,date,MMM dd, yyyy}" sortable="false" />
				           <display:column titleKey="label.entity.fileName"> 
			                    <c:if test="${enrollmentRegisterStatusHistory.FileName!=null}">	
			                    <c:set var="enrollmentRegisterStatusHistoryDocumentIdEE" ><encryptor:enc value="${enrollmentRegisterStatusHistory.documentIdEE}" isurl="true"/> </c:set>
	                            	<a href="#" onClick="showdetail('${enrollmentRegisterStatusHistoryDocumentIdEE}');" id="edit_${enrollmentRegisterStatusHistory.documentIdEE}">${enrollmentRegisterStatusHistory.FileName}</a> 
	                        	</c:if>
			                </display:column>
							<display:column titleKey="label.entity.remove">
								<c:if test="${enrollmentRegisterStatusHistory.documentIdEE !=null  && loggedUser =='entityAdmin' && enrollmentEntity.registrationStatus != 'Incomplete'}">	
			                       <c:set var="enrollmentRegisterStatusHistoryDocumentIdEE" ><encryptor:enc value="${enrollmentRegisterStatusHistory.documentIdEE}" isurl="true"/> </c:set>
			                       <c:set var="enrollmentEnrollmentEntityId" ><encryptor:enc value="${enrollmentEntity.id}" isurl="true"/> </c:set>
			                       	<a href="#" onClick="removeDocument('${enrollmentRegisterStatusHistoryDocumentIdEE}','${enrollmentEnrollmentEntityId}');" id="remove_${enrollmentRegisterStatusHistory.documentIdEE}"><spring:message code="label.entity.remove"/></a> 
				                </c:if>  					
		               	  	</display:column>
					</display:table>					
		</div>
		
			<!-- Showing comments -->			 					
			<!-- end of span9 -->			
		
		<!-- end of .gutter10 -->
		</div>
		</div>
</div>		
	
</div>
	<script type="text/javascript">
		$(function(){$('.datepick').each(function(){$(this).datepicker({showOn:"button",buttonImage:"../resources/images/calendar.gif",buttonImageOnly:true});});});
		
		$('.date-picker').datepicker({
	        autoclose: true,
	        format: 'mm-dd-yyyy'
		});
	</script>
		<script type="text/javascript">
		$(document).ready(function() {
			$("#documentupload").removeClass("link");
			$("#documentupload").addClass("active");
			document.getElementById('progress').style.display='none';
			var uploadErrorMessageInFlash = '${uploadErrorMessageInFlash}';
			var uploadedDocument = '${uploadedDocument}';
			if(uploadedDocument !="SIZE_FAILURE") {
			if(uploadedDocument != '') {
				if(uploadedDocument != 0) {
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="err.uploadsuccessfully"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});										
				}else{
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="err.unabletoupload"/></h4></br> '+uploadErrorMessageInFlash+'   </div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});													
				}
			}
			}
			else{
				$('<div class="modal popup-address" id="fileUoload"><div class="modal-header header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.validatePleaseSelectFileWithSizeLessThan5MB"/>.</h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
			}
		});			
function submitForm(formAction) {
			$('#formAction').val(formAction);
			if(((formAction == 'upload' && $("#fileInput").val() != '')) || (formAction =='update')) {
				if(formAction == 'update') {
					$('#frmupdatecertification').attr('action', '/hix/entity/entityadmin/documententity/${enrollmentEntity.id}');
					$('#frmupdatecertification').attr('method', 'POST');
				}
				if($("#fileInput_Size").val()==1){
				document.getElementById('progress').style.display='block';
				$('#frmupdatecertification').submit();
				}else{
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.validatePleaseSelectFileWithSizeLessThan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
				}
			} else {
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="err.selectfilebeforuoload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
			}
		}
		
				
</script>
<script>
$( "#progressbar" ).progressbar({
  value: 50
});

function showdetail(documentId) {
	var rv = -1; // Return value assumes failure.
	 if (navigator.appName == 'Microsoft Internet Explorer')
	 {
	    var ua = navigator.userAgent;
	    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
	    if (re.exec(ua) != null)
	       rv = parseFloat( RegExp.$1 );
	 }
	 if(rv <= 8.0 && navigator.appName == 'Microsoft Internet Explorer'){
		 $('<div class="modal popup-address" id="addressIFrame"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><h4><spring:message  code='label.entiy.upgradeBrowser' javaScriptEscape='true'/></h4><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 0px;"><spring:message  code='label.entiy.upgradeBrowserMessage' javaScriptEscape='true'/></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
		 } else {
			 var  contextPath =  "<%=request.getContextPath()%>";
			 var documentUrl = contextPath + "/entity/entityadmin/viewAttachment?encrypteddocumentId="+documentId;
			 window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
		}
	}

function removeDocument(documentIdEE,entityId) {
	var  contextPath =  "<%=request.getContextPath()%>";
	 var documentUrl = contextPath + "/entity/entityadmin/remove?encrypteddocumentId="+documentIdEE+"&encryptedid="+entityId;
	    window.open(documentUrl,"_self","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
}
$(function(){
	 $('#fileInput').change(function(){
			var rv = -1; // Return value assumes failure.
			 if (navigator.appName == 'Microsoft Internet Explorer')
			 {
			    var ua = navigator.userAgent;
			    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			    if (re.exec(ua) != null)
			       rv = parseFloat( RegExp.$1 );
			 }
			 if(!(rv <= 9.0 && navigator.appName == 'Microsoft Internet Explorer')){
			 var f=this.files[0];
			if((f.size > 5242880)||(f.fileSize > 5242880)){
				$("#fileInput_Size").val(0);
			}
			else{
				$("#fileInput_Size").val(1);	
			} 
			}
			else{
				$("#fileInput_Size").val(1);	
			} 
		});
	});	
</script>
