<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />

<div class="gutter10">

	<div class="row-fluid">
		<%-- <div class="page-header">
			<h1><spring:message  code="pgheader.paymentInformation"/></h1>
		</div>  Removed as part of HIX-33915 --%>
	<c:if test="${loggedUser =='entityAdmin'}">
	<div >
		<h1 style="margin-top: 0px;margin-bottom: 0px;"><a name="skip"></a>${enrollmentEntity.entityName}</h1>
	</div>	
	</c:if>
	</div>
	
	<div class="l-page-breadcrumb hide">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
				<li><a
					href="<c:url value="/hix/entity/assister/registration"/>"><spring:message code="label.account" /></a></li>
				<li><spring:message code="label.profile" /></li>
			</ul>
		</div>
		<!--  end of row-fluid -->
	</div>
	<div class="row-fluid">
	<jsp:include page="../leftNavigationMenu.jsp" />
	<div class="span9" id="rightpanel">
	
		<form class="form-vertical" id="frmviewpaymentinfo" name="frmviewpaymentinfo" action="getpaymentinfo" method="POST">
		<df:csrfToken/>
		<c:set var="enrollmentEntityId" ><encryptor:enc value="${enrollmentEntity.id}" isurl="true"/> </c:set>
		<input type="hidden" name="entityId" value="<encryptor:enc value="${enrollmentEntityId}"/>"/>  
			<!-- end of span3 -->
			
			<div class="header">
                <h4 class="pull-left"><spring:message  code="pgheader.paymentInformation"/></h4>				
				<c:choose>          
         	 		<c:when test="${loggedUser =='entityAdmin' && enrollmentEntity.registrationStatus!='Incomplete'}">         
             			<a class="btn btn-small pull-right" href="<c:url value="/entity/enrollmententity/getpaymentinfo?entityId=${enrollmentEntityId}"/>"><spring:message  code="label.edit"/></a>
             		</c:when>             		
         	 		<c:when test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus !='Pending'}">         
             			<a class="btn btn-small pull-right" href="<c:url value="/entity/enrollmententity/getpaymentinfo?entityId=${enrollmentEntityId}"/>"><spring:message  code="label.edit"/></a>
             		</c:when>             		
             	</c:choose>
			</div>
			
			<div class="control-group">
				<table class="table table-border-none">
					<tbody>			
						<tr>
			               <td class="span4 txt-right" scope="row"><spring:message code="label.entity.receivePayments"/></td>
			               <td> <strong> 
					                 <c:if test="${enrollmentEntity.receivedPayments == '1'}"> <spring:message code="label.entity.yes"/> </c:if>
					                 <c:if test="${enrollmentEntity.receivedPayments == '0'}"> <spring:message code="label.entity.no"/> </c:if>
			                	</strong>
			                </td>
			 			</tr>
					 </tbody>
				</table>
			</div>
			<c:if test="${enrollmentEntity.receivedPayments == '1'}">
				<div class="control-group">
					<table class="table table-border-none">
						<tbody>			
							<tr>
				               <td class="span4 txt-right" scope="row"><spring:message code="label.entity.paymentMethod"/></td>
				               <td> <strong> 
						                <c:if test="${paymentMethodsObj.paymentType == 'CHECK'}"> <spring:message code="label.entity.check"/></c:if>
	                    				<c:if test="${paymentMethodsObj.paymentType == 'EFT'}"> <spring:message code="label.entity.eft"/></c:if>
				                	</strong>
				                </td>
				 			</tr>
						 </tbody>
					</table>
				</div>
	
				<c:if test="${paymentMethodsObj.paymentType == 'EFT'}">
					<div class="row-fluid">					
						<div class="span">
							<div class="header margin0">
	                            <h4 class="pull-left"><spring:message code="label.entity.accountInformation"/></h4>
	                        </div>
							<table class="table table-border-none">
								<tbody>
									<tr>
										<td class="span4 txt-right" scope="row"><label class="control-label" for="name-on-account"><spring:message  code="label.entity.nameofaccount"/></label></td>
										<td><strong>${paymentMethodsObj.financialInfo.bankInfo.nameOnAccount}</strong></td>
									</tr>
									<tr>
										<td class="span4 txt-right" scope="row"><label class="control-label" for="bank-name"><spring:message  code="label.bankName"/></label></td>
										<td><strong>${paymentMethodsObj.financialInfo.bankInfo.bankName}</strong></td>
									</tr>
									<tr>
										<td class="span4 txt-right" scope="row"><label class="control-label" for="bank-routing-num"><spring:message  code="label.routingNumber"/></label></td>
										<td><strong>${paymentMethodsObj.financialInfo.bankInfo.routingNumber}</strong></td>
									</tr>
									<tr>
										<td class="span4 txt-right" scope="row"><label class="control-label" for="bank-acc-num"><spring:message  code="label.entity.acctNumber"/></label></td>
										<td><strong>${paymentMethodsObj.financialInfo.bankInfo.accountNumber}</strong></td>
									</tr>
					
									<tr>
										<td class="span4 txt-right" scope="row"><label class="control-label" for="bankAccountType"><spring:message  code="label.entity.accountType"/></label></td>
										<td><strong> <c:if test="${paymentMethodsObj.financialInfo.bankInfo.accountType == 'S'}"> <spring:message code="label.entity.savings"/></c:if><c:if test="${paymentMethodsObj.financialInfo.bankInfo.accountType == 'C'}"> <spring:message code="label.entity.checking"/></c:if></strong></td>
									</tr>
									
								</tbody>									
							</table>
						</div>
					</div>
				</c:if>
				
				<c:if test="${paymentMethodsObj.paymentType == 'CHECK'}">
					<div class="row-fluid">					
						<div class="span">		
							<div class="header margin0">
	                            <h4 class="pull-left"><spring:message code="label.entity.paymentAddress"/></h4>
	                        </div>
							
								<table class="table table-border-none">
							       	<tbody>	
									<tr>
										<td class="span4 txt-right" scope="row"><label class="control-label" for="address1_mailing"><spring:message  code="label.streetaddress"/></label></td>
										<td><strong>${paymentMethodsObj.financialInfo.address1}</strong></td>
									</tr>
									
									<tr>
										<td class="span4 txt-right" scope="row"><label class="control-label" for="address2_mailing"><spring:message  code="label.suite"/></label></td>
										<td><strong>${paymentMethodsObj.financialInfo.address2}</strong></td>
									</tr>
									
									<tr>
										<td class="span4 txt-right" scope="row"><label class="control-label" for="city_mailing"><spring:message  code="label.city"/></label></td>
										<td><strong>${paymentMethodsObj.financialInfo.city}</strong></td>
									</tr>
									
									<tr>
										<td class="span4 txt-right" scope="row"><label class="control-label" for="state_mailing"><spring:message  code="label.state"/></label></td>
										<td><strong>${paymentMethodsObj.financialInfo.state}</strong></td>
									</tr>
									
									<tr>
										<td class="span4 txt-right" scope="row"><label class="control-label" for="zip_mailing"><spring:message  code="label.zipcode"/></label></td>
										<td><strong>${paymentMethodsObj.financialInfo.zipcode}</strong></td>
									</tr>
									
								</tbody>
							</table>						
						</div>
					</div>
				</c:if>
			</c:if>
		</form>
	</div>
</div><!--row-fluid ends-->	
</div>
	
<script type="text/javascript">

$("#paymentInfo").removeClass("link");
$("#paymentInfo").addClass("active");

</script>
