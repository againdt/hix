package com.getinsured.eligibility.at.ref.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.dozzer.assembler.SsapAssembler;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.model.Location;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.HouseholdContact;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.OtherAddress;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

@Component("referralBatchAddressService")
@Scope("singleton")
public class ReferralBatchAddressService {

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
	@Qualifier("ssapAssembler")
	private SsapAssembler ssapAssembler;

	@Autowired
	private SsapApplicationRepository sssapApplicationRepository;

	@Autowired
	@Qualifier("iLocationRepository")
	private ILocationRepository iLocationRepository;

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	private Set<ApplicantStatusEnum> DELETED_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.DELETED, ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE);
	private Set<ApplicantStatusEnum> REMOVED_APPLICANT_STATUS = EnumSet.of(ApplicantStatusEnum.DELETED, ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE, ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE);

	public void updateAddress(SsapApplication sourceApplication, SsapApplication targetApplication) {
		SsapApplicant sourceSsapApplicant = null, primaryApplicant = null;
		SingleStreamlinedApplication ssapplnSource, ssapplnTarget;

		ssapplnSource = ssapJsonBuilder.transformFromJson(sourceApplication.getApplicationData());
		ssapplnTarget = ssapJsonBuilder.transformFromJson(targetApplication.getApplicationData());
		final List<SsapApplicant> copyFromPrimaryList = new ArrayList<>();
		final List<SsapApplicant> sourceApplicants = sourceApplication.getSsapApplicants();
		for (SsapApplicant targetSsapApplicant : targetApplication.getSsapApplicants()) {
			if (DELETED_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(targetSsapApplicant.getStatus()))) {
				continue;
			}
			sourceSsapApplicant = findApplicant(sourceApplicants, targetSsapApplicant.getApplicantGuid());
			if (null == sourceSsapApplicant || REMOVED_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(sourceSsapApplicant.getStatus()))) {
				copyFromPrimaryList.add(targetSsapApplicant);
				continue;
			} else {
				if (sourceSsapApplicant.getPersonId() == ReferralConstants.PRIMARY) {
					primaryApplicant = sourceSsapApplicant;
				}
				updateJsonAddress(ssapplnSource, ssapplnTarget, sourceSsapApplicant.getApplicantGuid(), sourceSsapApplicant);
				updateSsapApplicant(ssapplnTarget, targetSsapApplicant);
			}
		}
		updateAddressFromList(copyFromPrimaryList, primaryApplicant, ssapplnSource, ssapplnTarget);
		populateLivesAtOtherAddress(getPrimaryHouseHoldMember(ssapplnTarget), ssapplnTarget);
		String ssapJson = ssapAssembler.transformSsapToJson(ssapplnTarget);
		updateSSAPJSON(targetApplication.getId(), ssapJson);
	}

	private void updateAddressFromList(List<SsapApplicant> copyFromPrimaryList, SsapApplicant primaryApplicant, SingleStreamlinedApplication ssapplnSource, SingleStreamlinedApplication ssapplnTarget) {
		HouseholdContact householdContactSource = null, householdContactTarget = null;
		householdContactSource = getPrimaryHouseHoldMember(ssapplnSource).getHouseholdContact();

		for (SsapApplicant ssapApplicant : copyFromPrimaryList) {
			householdContactTarget = houseHoldContact(ssapplnTarget, ssapApplicant.getApplicantGuid());
			updateAddress(householdContactTarget.getHomeAddress(), householdContactSource.getHomeAddress());
			householdContactTarget.setHomeAddressIndicator(householdContactTarget.getHomeAddress().getPostalCode() != null);
			householdContactTarget.setMailingAddressSameAsHomeAddressIndicator(ReferralUtil.compareAddress(householdContactTarget.getHomeAddress(), householdContactTarget.getMailingAddress()));
		}
	}

	private void updateSsapApplicant(SingleStreamlinedApplication ssapplnTarget, SsapApplicant targetSsapApplicant) {
		if (ReferralConstants.PRIMARY == targetSsapApplicant.getPersonId()) {
			HouseholdMember householdPrimaryMember = getPrimaryHouseHoldMember(ssapplnTarget);
			final Location newLocation = createAndPersistNewLocation(householdPrimaryMember.getHouseholdContact().getHomeAddress());
			targetSsapApplicant.setOtherLocationId(BigDecimal.valueOf(newLocation.getId()));
			ssapApplicantRepository.save(targetSsapApplicant);
		}
	}

	private Location createAndPersistNewLocation(Address address) {
		Location newLocation = new Location();
		newLocation.setAddress1(address.getStreetAddress1());
		newLocation.setAddress2(address.getStreetAddress2());
		newLocation.setCity(address.getCity());
		newLocation.setState(address.getState());
		newLocation.setZip(address.getPostalCode());
		newLocation.setCounty(address.getCounty());
		newLocation = iLocationRepository.save(newLocation);
		return newLocation;
	}

	private void updateJsonAddress(SingleStreamlinedApplication ssapplnSource, SingleStreamlinedApplication ssapplnTarget, String applicantGuid, SsapApplicant sourceSsapApplicant) {
		HouseholdContact householdContactSource = null, householdContactTarget = null;
		householdContactTarget = houseHoldContact(ssapplnTarget, applicantGuid);
		householdContactSource = houseHoldContact(ssapplnSource, applicantGuid);
		updateAddress(householdContactTarget.getHomeAddress(), householdContactSource.getHomeAddress());
		householdContactTarget.setHomeAddressIndicator(householdContactTarget.getHomeAddress().getPostalCode() != null);
		householdContactTarget.setMailingAddressSameAsHomeAddressIndicator(ReferralUtil.compareAddress(householdContactTarget.getHomeAddress(), householdContactTarget.getMailingAddress()));
	}

	private HouseholdContact houseHoldContact(SingleStreamlinedApplication ssappln, String applicantGuid) {
		HouseholdContact householdContact = null;

		for (HouseholdMember houseHoldMember : ssappln.getTaxHousehold().get(0).getHouseholdMember()) {
			if (houseHoldMember.getApplicantGuid().equals(applicantGuid)) {
				householdContact = houseHoldMember.getHouseholdContact();
				break;
			}
		}
		return householdContact;
	}

	private SsapApplicant findApplicant(final List<SsapApplicant> applicants, String applicantGuid) {
		SsapApplicant returnSsapApplicant = null;
		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getApplicantGuid().equals(applicantGuid)) {
				returnSsapApplicant = ssapApplicant;
				break;
			}
		}
		return returnSsapApplicant;
	}

	private void updateSSAPJSON(long ssapApplicationId, String ssapJson) {
		SsapApplication ssapapplication = sssapApplicationRepository.findOne(ssapApplicationId);
		ssapapplication.setApplicationData(ssapJson);
		sssapApplicationRepository.save(ssapapplication);
	}

	private HouseholdMember getPrimaryHouseHoldMember(SingleStreamlinedApplication ssappln) {
		HouseholdMember householdMem = null;
		for (HouseholdMember houseHoldMember : ssappln.getTaxHousehold().get(0).getHouseholdMember()) {
			if (houseHoldMember.getPersonId() == 1) {
				householdMem = houseHoldMember;
				break;
			}
		}
		return householdMem;
	}

	private void updateAddress(Address householdaddress, Address address) {
		householdaddress.setStreetAddress1(address.getStreetAddress1());
		householdaddress.setStreetAddress2(address.getStreetAddress2());
		householdaddress.setCounty(address.getCounty());
		householdaddress.setCity(address.getCity());
		householdaddress.setPostalCode(address.getPostalCode());
		householdaddress.setState(address.getState());
		householdaddress.setPrimaryAddressCountyFipsCode(address.getPrimaryAddressCountyFipsCode());
	}

	private void populateLivesAtOtherAddress(HouseholdMember primaryHouseholdMember, SingleStreamlinedApplication singleStreamlinedApplication) {
		final List<HouseholdMember> householdMemberList = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
		final int size = ReferralUtil.listSize(householdMemberList);
		primaryHouseholdMember.setLivesWithHouseholdContactIndicator(true);
		if (size > 1) // No Need to check if there is one primary
		{
			boolean blnCheck = false;
			boolean blnLivesOther = false;
			Address otherAddres = null;
			HouseholdMember member = null;
			for (int i = 0; i < size; i++) {
				member = householdMemberList.get(i);
				if (member == null) {
					continue;
				}
				if (member.getPersonId().compareTo(1) != 0) {

					blnCheck = false;
					blnLivesOther = false;
					otherAddres = null;

					if (primaryHouseholdMember.getHouseholdContact().getHomeAddress() != null && member.getHouseholdContact().getHomeAddress() != null) {
						blnCheck = ReferralUtil.compareAddressData(primaryHouseholdMember.getHouseholdContact().getHomeAddress(), member.getHouseholdContact().getHomeAddress());
					}

					if (blnCheck) {
						blnLivesOther = false;
					} else if (member.getHouseholdContact().getHomeAddress() != null && member.getHouseholdContact().getHomeAddress().getPostalCode() != null) {
						blnLivesOther = true;
						otherAddres = member.getHouseholdContact().getHomeAddress();
					} else {
						if (primaryHouseholdMember.getHouseholdContact().getMailingAddress() != null && member.getHouseholdContact().getMailingAddress() != null) {
							blnCheck = ReferralUtil.compareAddressData(primaryHouseholdMember.getHouseholdContact().getMailingAddress(), member.getHouseholdContact().getMailingAddress());
						}

						if (blnCheck) {
							blnLivesOther = false;
						} else if (member.getHouseholdContact().getMailingAddress() != null && member.getHouseholdContact().getMailingAddress().getPostalCode() != null) {
							blnLivesOther = true;
							otherAddres = member.getHouseholdContact().getMailingAddress();
						}
					}

					member.setLivesWithHouseholdContactIndicator(!blnLivesOther);

					if (blnLivesOther) {
						member.setLivesAtOtherAddressIndicator(blnLivesOther);
						if (member.getOtherAddress() == null) {
							member.setOtherAddress(new OtherAddress());
						}

						ReferralUtil.copyProperties(otherAddres, member.getOtherAddress().getAddress());

						member.getOtherAddress().getAddress().setCounty(otherAddres.getPrimaryAddressCountyFipsCode());
						member.getOtherAddress().getAddress().setCountyCode(otherAddres.getPrimaryAddressCountyFipsCode());
						member.getOtherAddress().getAddress().setPrimaryAddressCountyFipsCode(null);

					}
				}
			}
		}
	}
}
