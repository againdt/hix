package com.getinsured.eligibility.passive.enrollment.service;

public interface PassiveEnrollmentNotice {

	String generateNoticeInInbox(String caseNumber) throws Exception;

}
