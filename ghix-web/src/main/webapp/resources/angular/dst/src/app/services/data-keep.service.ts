import { Observable } from 'rxjs/Observable';
import { FetchService } from './fetch.service';
import { DrugDetail } from './../models/data.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { BoxData } from "../models/options.model";
import { ProviderFullData, DosageInfo, DrugInfo, PersonalizedState, ProviderInfo } from '../models/data.model';
import { Response } from "@angular/http";
import { GetIEVersion } from "./utility";
import { Subject } from 'rxjs/Subject';

@Injectable()
export class DataKeepService {
    public shoppingId$ = new BehaviorSubject<string>(undefined);
    public csrfToken: string;
    public personalization: PersonalizedState = {
        doctor: {
            visitFrequency: "",
            doctorSearch: [],
            nearZipcode: "",
        },
        drug: {
            monthlyPrescription: "",
            drugSearch: []
        }
    };
    public ieV = GetIEVersion();
    public whiteScreenMsg$ = new BehaviorSubject<string>("");
    public dosageCard =false;
    public selectDosageAlert = new Subject<boolean>();
    public selectDosageAlert$ = this.selectDosageAlert.asObservable();
    constructor(
        private fetch: FetchService,
    ) {

        // get token and catch error
        let token;
        try {
            token = (<any>document.querySelector("#jsp_csrfToken")).value;
        } catch (err) {
            token = ""
        }
        this.csrfToken = token;
        //.log(this.csrfToken);

        // console.log('Sh' + this.shoppingId$);

        // getting zipcode once shopping id is available
        this.shoppingId$.filter(val => val !== undefined).subscribe(
            id => {
                if (!id) {
                    // console.error(`Shopping id not found`);
                    // this.whiteScreenMsg$.next("ERROR_SCREEN.message.missing_shopping_id");
                } else {
                    this.fetch.getZipcode(id).subscribe(
                        val => {
                            val.household = JSON.parse(val.householdData);
                            this.personalization.doctor.nearZipcode = val.household.zipCode;
                            this.personalization.doctor.doctorSearch = val.pdPreferencesDTO.providers ? JSON.parse(val.pdPreferencesDTO.providers) : [];
                            this.personalization.doctor.visitFrequency = val.pdPreferencesDTO.medicalUse ? val.pdPreferencesDTO.medicalUse : "LEVEL2";
                            this.personalization.drug.drugSearch = val.pdPreferencesDTO.prescriptions ? JSON.parse(val.pdPreferencesDTO.prescriptions) : [];
                            this.personalization.drug.monthlyPrescription = val.pdPreferencesDTO.prescriptionUse ? val.pdPreferencesDTO.prescriptionUse : "LEVEL2";
                            //console.log("loaded data:");
                            //console.log(this.personalization.drug);
                        },
                        err => {
                            this.whiteScreenMsg$.next("ERROR_SCREEN.message.invalid_shopping_id");
                            //console.error("Fail to get zipcode")
                        }
                    )
                }
            }
        )
    }


    public updatePersonalizedState(
        type: "DOC_VISIT_FREQUENCY" | "DOCTOR" | "ZIPCODE" | "MONTHLY_PRECRIPTION" | "DRUG",
        action: "ADD" | "REMOVE" | "UPDATE",
        input: any
    ): { status?: "success" | "failure", msg?: string, index?: number } {
        if (type === "DOC_VISIT_FREQUENCY") {
            this.personalization.doctor.visitFrequency = input;

            return { status: "success" }
        }

        if (type === "DOCTOR") {
            if (action === "ADD") {
                const currentIndex = this.personalization.doctor.doctorSearch ? this.personalization.doctor.doctorSearch.findIndex(el => el.id === input.id) : -1;
                if (this.personalization.doctor.doctorSearch === null)
                    this.personalization.doctor.doctorSearch = [];
                if (currentIndex === -1) {

                    // case: does not exist yet
                    if (this.personalization.doctor.doctorSearch.length === 5) {

                        // case: already have 5 doctors
                        return { status: "failure", msg: "ALERT_BOX.maximum_doc" }
                    } else {

                        // case: less than 5 doctors
                        this.personalization.doctor.doctorSearch.push(input);
                        return { status: "success" };
                    }
                } else {

                    // case: the id currently exists
                    if (this.personalization.doctor.doctorSearch[currentIndex].locationId === input.locationId) {
                        // case: same doctor, same address
                        return { status: "failure", msg: "", index: currentIndex };
                    } else {
                        // case: same doctor, different address
                        this.personalization.doctor.doctorSearch[currentIndex] = input;
                        return { status: "success", msg: "ALERT_BOX.address_changed", index: currentIndex };
                    }
                }
            } else if (action === "REMOVE") {
                const index = this.personalization.doctor.doctorSearch.findIndex((doc: ProviderInfo) => {
                    return doc.id === input.id;
                });
                this.personalization.doctor.doctorSearch.splice(index, 1);
                return { status: "success" };
            }
        }

        if (type === "ZIPCODE") {
            this.personalization.doctor.nearZipcode = input;
            return { status: "success" };
        }

        if (type === "MONTHLY_PRECRIPTION") {
            this.personalization.drug.monthlyPrescription = input;
            return { status: "success" };
        }

        if (type === "DRUG") {
            if (action === "ADD") {
                if (this.personalization.drug.drugSearch.length === 5) {
                    return { status: "failure", msg: "ALERT_BOX.maximum_drug" };
                }
                const currentIndex = this.personalization.drug.drugSearch ? this.personalization.drug.drugSearch.findIndex(el => el.drugID === input.drugID) : -1;
                if (currentIndex !== -1) {
                    // case: already exist
                    return { status: "failure", msg: "ALERT_BOX.same_drug_id" };
                } else {
                    if (Array.isArray(this.personalization.drug.drugSearch))
                        this.personalization.drug.drugSearch.push(input);
                    else {
                        this.personalization.drug.drugSearch = [];
                        this.personalization.drug.drugSearch.push(input);
                    }
                    return { status: "success" };
                }
            } else if (action === 'REMOVE') {
                const index = this.personalization.drug.drugSearch.findIndex(el => el.drugID === input.drugID);
                this.personalization.drug.drugSearch.splice(index, 1);
                return { status: "success" };
            } else if (action === "UPDATE") {
                const el = this.personalization.drug.drugSearch.find(drug => drug.drugID === input.drugID);
                if (!el) {
                    return { status: "failure", msg: `Update drug detail fails because existing record can't be found` };
                }
                Object.assign(el, input);
                return { status: "success" };
            }
        }
    }


    public scoreMyPlan(): Observable<Response> {
        //console.log(`csrfToken:`, this.csrfToken);
        window['personalization'] = this.personalization;

        let numberOfDoctors = 0, numberOfHospitals = 0;

        if( this.personalization.doctor) {
        	for(let i = 0; i < this.personalization.doctor.doctorSearch.length; i ++) {
	        	if(this.personalization.doctor.doctorSearch[i].providerType === 'doctor') {
	        		numberOfDoctors ++;
	        	}else {
	        		numberOfHospitals ++;
	        	}
	        }
        }

       	window['dataLayer'].push({
        	'event': 'calculate_doctor_count',
        	'eventCategory': 'Pref - Doctor Search',
        	'eventAction': 'Click',
        	'eventLabel': `Doctor Search -- ${numberOfDoctors} Doctors, ${numberOfHospitals} Hospitals, 0 Dentists - Calculate`
        });

        window['dataLayer'].push({
        	'event': 'calculate_prescription_count',
        	'eventCategory': 'Pref - Prescription Search',
        	'eventAction': 'Click',
        	'eventLabel': `Prescription Search -- ${this.personalization.drug.drugSearch.length} Prescriptions - Calculate`
        });

        return this.fetch.savePreference(this.shoppingId$.getValue(), this.personalization, this.csrfToken);
    }

    public getDoctors(): ProviderInfo[] {
        return this.personalization.doctor.doctorSearch;
    }

    public validateZip(zipcode: string): Observable<Response> {
        //console.log(`csrfToken`, this.csrfToken);
        return this.fetch.validateZip(this.csrfToken, zipcode);
    }

    public readErrorMessage(inputId, errorId){
        setTimeout(function() {
            document.getElementById(errorId).focus();
        }, 0);
        setTimeout(function() {
            document.getElementById(inputId).focus();
        }, 1000);
    }
    public publishData(alert: boolean){
        this.selectDosageAlert.next(alert);
    }


    public updateAriaLabel(elementId: string, message: string): void {
		const element = document.getElementById(elementId);
        const oldLabel = element.getAttribute("aria-label");
		element.setAttribute("aria-label", message);

        setTimeout(function() {
	        (<HTMLInputElement>element).value = '';
            element.setAttribute("aria-label", oldLabel);
        }, 0);

        element.focus();
	}
}

