<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<link href="<gi:cdnurl value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />



<script type="text/javascript">
	$(document).ready(function() {
		$("#populationServed").removeClass("link");
		$("#populationServed").addClass("active");
		
		var langTotal = 0;
		$('.langPercentage').each(function(){		    	  
			if(this.value != "" && !isNaN(this.value)) {
				langTotal += parseInt(this.value, 10);
	    	} else if(this.placeholder != "" && !isNaN(this.placeholder)) {
	    		langTotal += parseInt(this.placeholder, 10);
    	  	}
      	});
		$('#langPercentageTotal').val(langTotal + "%");
		
		var ethnicityTotal= 0;
		$('.ethnicityPercentage').each(function(){		    	  
			if(this.value != "" && !isNaN(this.value)) {
				ethnicityTotal += parseInt(this.value, 10);
	    	} else if(this.placeholder != "" && !isNaN(this.placeholder)) {
	    		ethnicityTotal += parseInt(this.placeholder, 10);
    	  	}
      	});
		$('#ethnicityPercentageTotal').val(ethnicityTotal + "%");
		
		var indTotal = 0;
		$('.industryPercentage').each(function(){		    	  
			if(this.value != "" && !isNaN(this.value)) {
				indTotal += parseInt(this.value, 10);
	    	} else if(this.placeholder != "" && !isNaN(this.placeholder)) {
	    		indTotal += parseInt(this.placeholder, 10);
    	  	}
      	});
		$('#industryPercentageTotal').val(indTotal + "%");
	});
</script>

<div class="gutter10">
<div class="row-fluid">
	<c:if test="${loggedUser =='entityAdmin'}">
	<div id="titlebar">
		<h1 style="margin-top: 0px;margin-bottom: 0px;"><a name="skip"></a>${enrollmentEntity.entityName}</h1>
	</div>	
	</c:if>
</div>
			<div class="row-fluid">

				<!-- #sidebar -->
				<jsp:include page="../leftNavigationMenu.jsp" />
				<!-- #sidebar ENDS -->
				<c:set var="enrollmentEntityUserId" ><encryptor:enc value="${enrollmentEntity.user.id}" isurl="true"/> </c:set>
				<!-- #section -->
				<div class="span9" id="rightpanel">
					<div id="section">
						<div class="header">
							<h4 class="pull-left"><spring:message code="label.entity.populationsServed"/></h4>
							<c:choose>          
         	 					<c:when test="${loggedUser =='entityAdmin' && enrollmentEntity.registrationStatus!='Incomplete'}">         
             						<a class="btn btn-small pull-right" type="button" href="<c:url value="/entity/entityadmin/getPopulationServed/${enrollmentEntityUserId}"/>"><spring:message code="label.entity.edit"/></a>
             					</c:when> 
             					<c:when test="${CA_STATE_CODE}">
             						<c:if test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus =='Incomplete'}">
             							<a class="btn btn-small pull-right" type="button" href="<c:url value="/entity/enrollmententity/getPopulationServed"/>"><spring:message code="label.entity.edit"/></a>
             						</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus !='Pending'}">
										<a class="btn btn-small pull-right" type="button" href="<c:url value="/entity/enrollmententity/getPopulationServed"/>"><spring:message code="label.entity.edit"/></a>
									</c:if>
								</c:otherwise>	            		
             				</c:choose>
						</div>
						<div class="gutter10">
						<form class="form-horizontal viewPopulationServedform" name="frmViewPopulationServed"
							id="frmViewPopulationServed" action="getPopulationServed"
							method="GET">
							<c:set var="populationServedWrapper"
								value="${populationServedWrapper}" />
							<!-- Languages Starts -->
							<c:choose>
								<c:when test="${empty langList}">
									<h3 class="trigger"><spring:message code="label.entity.languages"/> :</h3><spring:message code="label.entity.noAssociatedLanguages"/>
									</c:when>
								<c:otherwise>
									<h3 data-toggle="collapse" data-target="#languages"
										class="trigger"><i class="icon-chevron-right margin10-l"></i> <spring:message code="label.entity.languages"/></h3>
								</c:otherwise>
							</c:choose>
							
							<div id="languages" class="collapse out">
								<p><spring:message code="label.entity.checkAllLanguagesOfYourTargetPopulationAndSpecifyPercentagesAccordingly"/></p>
								<table class="table">
									<thead>
										<tr>
											<th style="width: 145px;"><spring:message code="label.entity.language"/> <img src="<gi:cdnurl value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
											<th><spring:message code="label.entity.percentOfInLanguageAssistance"/> <img src="<gi:cdnurl value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
											<th><spring:message code="label.entity.numberOfStaffWhoSpeakTheLanguageFluently"/> <img src="<gi:cdnurl value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
 										</tr>
									</thead>
									<tbody>
											<c:forEach items="${langList}"
												var="associatedLanguageList">
														<input type="hidden" name="enrollmentEntityId"
															id="enrollmentEntityId" value="<encryptor:enc value="${enrollmentEntity.id}"/>" />
														<tr>
															<td scope="row"><label class="checkbox"> <input disabled="disabled" checked="checked"
																	type="checkbox" class="selectLang"
																	value="${associatedLanguageList.language}"
																	id="${associatedLanguageList.language}" name="LangChk">
																	${associatedLanguageList.language}
															</label></td>
															<td><input type="text" readonly="readonly"
																name="${associatedLanguageList.language}LangPercentage"
																class="input-mini langPercentage"
																id="${associatedLanguageList.language}LangPercentage"
																value="${associatedLanguageList.languagePercentage}"></td>
															<td><input type="text" readonly="readonly"
																name="${associatedLanguageList.language}LangStaff"
																class="input-mini langStaff"
																id="${associatedLanguageList.language}LangStaff"
																value="${associatedLanguageList.noOfStaffSpeakLanguage}"></td>
														</tr>
														<input type="hidden" name="languagesData" id="languagesData"
															value="" />
													
											</c:forEach>
											<tr>
												<td scope="row"><strong><spring:message code="label.entity.total"/></strong></td>
												<!-- IMPORTANT: total must be 100% -->
												<td><input type="text" readonly="readonly"
													class="input-mini langPercentageTotal"
													name="langPercentageTotal" id="langPercentageTotal"
													placeholder="100%" value=""></td>
												<td>&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</div>
							
							<!-- #languages .collapse out -->
							<!-- Languages Ends -->


							<!-- Ethnicity Starts -->
							<c:choose>
								<c:when test="${empty ethnicityList}">
									<h3 class="trigger"><spring:message code="label.entity.ethnicities"/> :</h3><spring:message code="label.entity.noAssociatedEthnicities"/>
									</c:when>
								<c:otherwise>
									<h3 data-toggle="collapse" data-target="#ethnicities"
										class="trigger"><i class="icon-chevron-right margin10-l"></i> <spring:message code="label.entity.ethnicities"/></h3>
								</c:otherwise>
							</c:choose>
								<div id="ethnicities" class="collapse out">
									<p><spring:message code="label.entity.checkAllThatApplyAndSpecifyPerAcc"/></p>
								<table class="table">
									<thead>
										<tr>
											<th><spring:message code="label.entity.ethnicity"/> <img src="<gi:cdnurl value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
											<th><spring:message code="label.entity.estimatedPlannedToServe"/> <img src="<gi:cdnurl value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
 										</tr>
									</thead>
										<tbody>
											<c:forEach items="${ethnicityList}"
												var="associatedEthnicityList">
														<input type="hidden" name="enrollmentEntityId"
															id="enrollmentEntityId" value="<encryptor:enc value="${enrollmentEntity.id}"/>" />
														<tr>
															<td scope="row"><label class="checkbox"> <input disabled="disabled" checked="checked"
																	type="checkbox" class="selectEthnicity"
																	value="${associatedEthnicityList.ethnicity}"
																	id="${associatedEthnicityList.ethnicity}" name="EthnicityChk">
																	${associatedEthnicityList.ethnicity}
															</label></td>
															<td><input type="text" readonly="readonly"
																name="${associatedEthnicityList.ethnicity}EthnicityPercentage"
																class="input-mini ethnicityPercentage"
																id="${associatedEthnicityList.ethnicity}EthnicityPercentage"
																value="${associatedEthnicityList.ethnicityPercentage}"></td>
														
														</tr>
														<input type="hidden" name="ethnicitiesData" id="ethnicitiesData"
															value="" />
													
											</c:forEach>
											<tr>
												<td scope="row"><strong><spring:message code="label.entity.total"/></strong></td>
												<!-- IMPORTANT: total must be 100% -->
												<td><input type="text" readonly="readonly"
													class="input-mini ethnicityPercentageTotal"
													name="ethnicityPercentageTotal" id="ethnicityPercentageTotal"
													placeholder="100%" value=""></td>
									
											</tr>
										</tbody>
									</table>
								</div>
							<!-- #ethnicity .collapse out -->
							<!-- Ethnicity Ends -->


							<!-- Industries Starts -->
							<c:choose>
								<c:when test="${empty industryList}">
									<h3 class="trigger"><spring:message code="label.entity.industries"/> :</h3><spring:message code="label.entity.noAssociatedIndustries"/>
									</c:when>
								<c:otherwise>
								 	<h3 data-toggle="collapse" data-target="#industries"
										class="trigger"><i class="icon-chevron-right margin10-l"></i> <spring:message code="label.entity.industries"/></h3>
								</c:otherwise>
							</c:choose>
							
							<div id="industries" class="collapse out">
								<p><spring:message code="label.entity.checkAllIndustriesThtApplyAndProvidePercPerInd"/></p>
								<table class="table">
									<thead>
										<tr>
											<th><spring:message code="label.entity.industries"/> <img src="<gi:cdnurl value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
											<th><spring:message code="label.entity.estimatedPlanneToServe"/> <img src="<gi:cdnurl value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
 										</tr>
									</thead>
									<tbody>
											<c:forEach items="${industryList}"
												var="associatedIndustryList">
														<input type="hidden" name="enrollmentEntityId"
															id="enrollmentEntityId" value="<encryptor:enc value="${enrollmentEntity.id}"/>" />
														<tr>
															<td scope="row"><label class="checkbox"> <input disabled="disabled" checked="checked"
																	type="checkbox" class="selectIndustry"
																	value="${associatedIndustryList.industry}"
																	id="${associatedIndustryList.industry}" name="IndustryChk">
																	${associatedIndustryList.industry}
															</label></td>
															<td><input type="text" readonly="readonly"
																name="${associatedIndustryList.industry}IndustryPercentage"
																class="input-mini industryPercentage"
																id="${associatedIndustryList.industry}IndustryPercentage"
																value="${associatedIndustryList.industryPercentage}"></td>
														
														</tr>
														<input type="hidden" name="industriesData" id="industriesData"
															value="" />
													
											</c:forEach>
	
											<tr>
												<td scope="row"><strong><spring:message code="label.entity.total"/></strong></td>
												<!-- IMPORTANT: total must be 100% -->
												<td><input type="text" readonly="readonly"
													class="input-mini industryPercentageTotal"
													name="industryPercentageTotal" id="industryPercentageTotal"
													placeholder="100%" value=""></td>
												
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- #industries .collapse out -->
							<!-- Industries Ends -->

							<table class="table table-border-none">
							<tr>																	
								<td>
								<c:if test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus=='Incomplete'}">
										<a class="btn btn-primary pull-right" id="label.entity.next" onClick="window.location.href='/hix/entity/enrollmententity/primarysite'"><spring:message code="label.entity.next"></spring:message></a>
								</c:if>										
								</td>
						</tr>
						</table>
						</form>
						<!-- .form-horizontal -->

					
						</div>
					</div>
				</div>
				<!-- .span9 -->
</div>
			
  <script>
$('.trigger').click(
		function() {
			$(this).find('i').toggleClass(
					'icon-chevron-right icon-chevron-down');
		});
</script>
