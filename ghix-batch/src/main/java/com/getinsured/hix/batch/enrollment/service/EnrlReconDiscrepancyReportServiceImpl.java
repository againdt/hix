/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.getinsured.hix.batch.enrollment.util.EnrlReconDiscrepancyReportThread;
import com.getinsured.hix.dto.enrollment.EnrlDiscrepancyReportCsvDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentSftpFileTransferDTO;
import com.getinsured.hix.dto.enrollment.ReconEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.ReconMemberDTO;
import com.getinsured.hix.enrollment.repository.IEnrlReconDiscrepancyRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconSnapshotRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconSummaryRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentCmsFileTransferLogRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.service.ValidationFolderPathService;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.enrollment.util.EnrollmentSftpUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.ExchgPartnerLookup;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy;
import com.getinsured.hix.model.enrollment.EnrlReconLkp;
import com.getinsured.hix.model.enrollment.EnrlReconSummary;
import com.getinsured.hix.model.enrollment.EnrlReconSummary.SummaryStatus;
import com.getinsured.hix.model.enrollment.EnrollmentCmsFileTransferLog;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Enrollment Reconciliation Discrepancy Report Service
 * @author negi_s
 * @since 22/03/2017
 *
 */
@Service("enrlReconDiscrepancyReportService")
@Transactional
public class EnrlReconDiscrepancyReportServiceImpl implements EnrlReconDiscrepancyReportService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconDiscrepancyReportServiceImpl.class);
	
	@Autowired private IEnrlReconSummaryRepository enrlReconSummaryRepository;
	@Autowired private IEnrlReconDiscrepancyRepository enrlReconDiscrepancyRepository;
	@Autowired private IEnrollmentRepository enrollmentRepository;
	@Autowired private IEnrlReconSnapshotRepository enrlReconSnapshotRepository;
	@Autowired private Gson platformGson;
	@Autowired private BatchJobExecutionService batchJobExecutionService;
	@Autowired private ValidationFolderPathService validationFolderPathService;
	@Autowired private ContentManagementService ecmService;
	@Autowired private CmsSftpService cmsSftpService;
	@Autowired private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;
	@Autowired private IEnrollmentCmsFileTransferLogRepository enrollmentCmsFileTransferLogRepository;
	
	private static Type listType = new TypeToken<ArrayList<EnrollmentSftpFileTransferDTO>>() {}.getType();
	
	@Value("#{configProp['enrollment.exchg.sftp.reconfolderPath']}")
	private String EXCHG_SFTP_RECON_FOLDERPATH;
	
	private static final List<String> COMMA_ESCAPE_FIELDS = Stream
			.of("homeAddress1", "homeAddress2", "mailingAddress1", "mailingAddress2", "agentBrokerName")
			.collect(Collectors.toList());
		
	@Override
	public void generateReportForHiosIssuerId(String hiosIssuerId, int applicableYear, JobService jobService,
			long jobExecutionId) throws Exception {
		LOGGER.info("Inside EnrlReconDiscrepancyReportServiceImpl:: generateReportForHiosIssuerId():: "+hiosIssuerId );
		// Query summary table with HIOS issuer ID to see if report has been sent or not
		List<Integer> fileIdList = enrlReconSummaryRepository.getFileIdsForReportGeneration(hiosIssuerId, applicableYear);
		processFileIdList(fileIdList, hiosIssuerId, applicableYear, jobService, jobExecutionId);
	}
	
	@Override
	public void generateReportForFileId(String fileId, int applicableYear, JobService jobService, long jobExecutionId)
			throws Exception {
		LOGGER.info("Inside EnrlReconDiscrepancyReportServiceImpl:: generateReportForFileId():: "+fileId );
		List<Integer> fileIdList = enrlReconSummaryRepository.inspectFileIdForReportGeneration(Integer.valueOf(fileId));
		String hiosIssuerId = enrlReconSummaryRepository.getHiosIssuerIdFromFileId(Integer.valueOf(fileId));
		processFileIdList(fileIdList, hiosIssuerId, applicableYear, jobService, jobExecutionId);
	}
	
	/**
	 * Process the fileId list to generate discrepancy report
	 * @param fileIdList
	 * @param hiosIssuerId
	 * @param applicableYear
	 * @param jobService
	 * @param jobExecutionId
	 * @throws Exception
	 */
	private void processFileIdList(List<Integer> fileIdList, String hiosIssuerId, int applicableYear, JobService jobService, long jobExecutionId) throws Exception {
		Date currentDate = new Date();
		int fileCount = 0;
		String outboundDiscrepancyReportPath= getOutboundDiscrepancyReportPath(hiosIssuerId, fileIdList);
		if(null != fileIdList && !fileIdList.isEmpty() && EnrollmentUtils.isNotNullAndEmpty(outboundDiscrepancyReportPath)){
			for(Integer fileId : fileIdList){
				String batchJobStatus=null;
				if(jobService != null && jobExecutionId != -1){
					batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
				}
				if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
					throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
				}

				List<EnrlDiscrepancyReportCsvDTO> csvDtoList = null;
				// Query discrepancy table with file ID and fetch discrepancies
				EnrlReconSummary summary = enrlReconSummaryRepository.getReconSummaryByFileId(fileId);
				String reconFileName = summary.getFileName();
				List<EnrlReconDiscrepancy> discrepancyList = enrlReconDiscrepancyRepository.getDiscrepanciesByFileId(fileId);
				try{
					csvDtoList = processDiscrepancyList(discrepancyList,reconFileName, fileId);
				}catch(Exception e){
					LOGGER.error("Could not create discrepancy report for file Id :: "+ fileId + " File Name :: "+ reconFileName , e);
					logErrorInEnrlReconSummary(summary, EnrollmentUtils.shortStackTrace(e, 1000));
				}
				if(null != discrepancyList && null != csvDtoList && (discrepancyList.size() - csvDtoList.size() == 0)){
					createDiscrepancyFileAndUpdateStatus(csvDtoList,hiosIssuerId, getProcessingDate(currentDate, fileCount++), fileId, outboundDiscrepancyReportPath, summary, jobExecutionId);
				}else if(null != discrepancyList && null != csvDtoList){
					logErrorInEnrlReconSummary(summary, "Failed to process "+  (discrepancyList.size() - csvDtoList.size()) + " discrepancies");
					rollbackDiscrepancyStatus(fileId);
				}
			}
		}else{
			LOGGER.info("No Discrepancy report pending for Hios Issuer Id :: " + hiosIssuerId + " for year :: " + applicableYear);
		}
	}


	/**
	 * Process discrepancy list for report generation
	 * @param discrepancyList
	 * @param reconFileName
	 * @param fileId
	 * @return List of EnrlDiscrepancyReportCsvDTO
	 * @throws InterruptedException
	 */
	private List<EnrlDiscrepancyReportCsvDTO> processDiscrepancyList(List<EnrlReconDiscrepancy> discrepancyList , String reconFileName, int fileId) throws InterruptedException {
		List<EnrlDiscrepancyReportCsvDTO> csvDtoList = new ArrayList<>();
		List<Callable<List<EnrlDiscrepancyReportCsvDTO>>> taskList = new ArrayList<>();
		if (null != discrepancyList && !discrepancyList.isEmpty()) {
			Integer maxThreadPoolSize = EnrollmentConstants.TEN;
			String maxThreadPoolSizeStr = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_DISCREPANCY_REPORT_THREAD_POOLSIZE);
//			String maxThreadPoolSizeStr = "10";
			if(null != maxThreadPoolSizeStr && NumberUtils.isNumber(maxThreadPoolSizeStr.trim())){
				maxThreadPoolSize=Integer.valueOf(maxThreadPoolSizeStr.trim());
			}
			ExecutorService executor = Executors.newFixedThreadPool(maxThreadPoolSize);
			int size = discrepancyList.size();
			Integer maxDiscrepanciesPerPartition = 1000;
			String maxDiscrepanciesPerPartitionStr = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_DISCREPANCY_REPORT_MAX_DISCREPANCY_COUNT);
//			String maxDiscrepanciesPerPartitionStr = "3";
			if(null != maxDiscrepanciesPerPartitionStr && NumberUtils.isNumber(maxDiscrepanciesPerPartitionStr.trim())){
				maxDiscrepanciesPerPartition=Integer.valueOf(maxDiscrepanciesPerPartitionStr.trim());
			}
			int numberOfPartitions = size / maxDiscrepanciesPerPartition;
			if (size % maxDiscrepanciesPerPartition != 0) {
				numberOfPartitions++;
			}
			int firstIndex = 0;
			int lastIndex = 0;
			for (int i = 0; i < numberOfPartitions; i++) {
				firstIndex = i * maxDiscrepanciesPerPartition;
				lastIndex = (i + 1) * maxDiscrepanciesPerPartition;
				if (lastIndex > size) {
					lastIndex = size;
				}
				List<EnrlReconDiscrepancy> subList = discrepancyList.subList(firstIndex, lastIndex);
				taskList.add(new EnrlReconDiscrepancyReportThread(subList,
						reconFileName, fileId,this));
			}
			executor.invokeAll(taskList).stream().map(future -> {
				try {
					return future.get();
				} catch (Exception ex) {
					LOGGER.error("Exception occurred in populateDataDto : " + ex.getMessage());
				}
				return null;
			}).filter(p -> p != null).forEach(e -> csvDtoList.addAll(e));
			executor.shutdown();
		}
		return csvDtoList;
	}
	
	@Override
	public List<EnrlDiscrepancyReportCsvDTO> populateDiscrepancyDto(List<EnrlReconDiscrepancy> discrepancyList, String reconFileName, Integer fileId) {
		List<EnrlDiscrepancyReportCsvDTO> csvDtoList = new ArrayList<>();
		Map<Long, String> cmsPlanIdMap = Collections.synchronizedMap(new HashMap<Long, String>());
		Map<Long, String> enrollmentDataMap = Collections.synchronizedMap(new HashMap<Long, String>());
		Integer issuerDataCounter = 0;
		ReconEnrollmentDTO enrollmentDataDto = null;
		Integer maxDiscrepanciesPerPartition = 1000;
		String maxDiscrepanciesPerPartitionStr = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_DISCREPANCY_REPORT_MAX_DISCREPANCY_COUNT);
		if(null != maxDiscrepanciesPerPartitionStr && NumberUtils.isNumber(maxDiscrepanciesPerPartitionStr.trim())){
			maxDiscrepanciesPerPartition=Integer.valueOf(maxDiscrepanciesPerPartitionStr.trim());
		}
		Integer maxEnrollmentsInMemory = maxDiscrepanciesPerPartition / 10;
		String maxEnrollmentsInMemoryStr = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_DISCREPANCY_REPORT_MAX_ENRL_IN_MEMORY);
		if (null != maxEnrollmentsInMemoryStr && NumberUtils.isNumber(maxEnrollmentsInMemoryStr.trim())
				&& Integer.valueOf(maxEnrollmentsInMemoryStr.trim()) < maxEnrollmentsInMemory
				&& maxEnrollmentsInMemory > 10) {
			maxEnrollmentsInMemory = Integer.valueOf(maxEnrollmentsInMemoryStr.trim());
		} else if (maxEnrollmentsInMemory < 10) {
			maxEnrollmentsInMemory = EnrollmentConstants.TEN;
		}
		if(null != discrepancyList && !discrepancyList.isEmpty()){
			// Implement threads split discrepancies by 1000
			for(EnrlReconDiscrepancy discrepancy : discrepancyList){
				try{
					EnrlDiscrepancyReportCsvDTO dto = new EnrlDiscrepancyReportCsvDTO();
					dto.setExchangeAssignedPolicyId(discrepancy.getHixEnrollmentId());
					if(null != discrepancy.getHixEnrollmentId() && enrollmentDataMap.containsKey(discrepancy.getHixEnrollmentId())){
						enrollmentDataDto = platformGson.fromJson(enrollmentDataMap.get(discrepancy.getHixEnrollmentId()), ReconEnrollmentDTO.class);
					}else if(null != discrepancy.getHixEnrollmentId()){
						String enrollmentData  = enrlReconSnapshotRepository.findIssuerDataByFileIdAndEnrollmentId(fileId, discrepancy.getHixEnrollmentId());
						if(EnrollmentUtils.isNullOrEmpty(enrollmentData)){
							enrollmentData  = enrlReconSnapshotRepository.findHixDataByFileIdAndEnrollmentId(fileId, discrepancy.getHixEnrollmentId());
						}
						if(EnrollmentUtils.isNotNullAndEmpty(enrollmentData)){
							if(issuerDataCounter % maxEnrollmentsInMemory == 0){
								enrollmentDataMap.clear();
							}
							enrollmentDataMap.put(discrepancy.getHixEnrollmentId(), enrollmentData);
							enrollmentDataDto = platformGson.fromJson(enrollmentData, ReconEnrollmentDTO.class);
						}
					}
					if(null != discrepancy.getHixEnrollmentId() && cmsPlanIdMap.containsKey(discrepancy.getHixEnrollmentId())){
						dto.setPlanId(cmsPlanIdMap.get(discrepancy.getHixEnrollmentId())); //Get CMS ID from enrollment
					}else if(null != discrepancy.getHixEnrollmentId()){
						String cmsPlanId= enrollmentRepository.getCmsPlanId(discrepancy.getHixEnrollmentId().intValue());
						if(EnrollmentUtils.isNullOrEmpty(cmsPlanId)) {
							cmsPlanId = enrollmentDataDto.getQhpId();
						}
						cmsPlanIdMap.put(discrepancy.getHixEnrollmentId(), cmsPlanId);
						dto.setPlanId(cmsPlanId);
					}
					// Populate Member information from member ID from Issuer Snapshot data
					populateMemberInformation(enrollmentDataDto, dto, discrepancy.getMemberId());
					EnrlReconLkp reconLkp = discrepancy.getEnrlReconLkpId();
					if(COMMA_ESCAPE_FIELDS.contains(discrepancy.getFieldName())){
					dto.setYhiValue(encloseStringInQuotes(discrepancy.getHixValue()));
					dto.setIssuerValue(encloseStringInQuotes(discrepancy.getIssuerValue()));
					}else{
						dto.setYhiValue(discrepancy.getHixValue());
						dto.setIssuerValue(discrepancy.getIssuerValue());
					}
					if(null != reconLkp){
						dto.setDiscrepancyReasonCode(reconLkp.getCode());
						dto.setDiscrepancyReasonText(reconLkp.getLabel());
						dto.setAutoFixedByYhi((EnrollmentUtils.isNotNullAndEmpty(reconLkp.getSystemAutofix()) ? reconLkp.getSystemAutofix() : "N")); 
						dto.setAssignee(reconLkp.getAssignee());
					}
					dto.setDateOfDiscrepancy(DateUtil.dateToString(discrepancy.getCreationTimestamp(),EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
					dto.setReconFileName(reconFileName);
					csvDtoList.add(dto);
					discrepancy.setReportSentStatus("Y");
					discrepancy.setReportErrorMessage(null);
				}catch(Exception e){
					LOGGER.error("Error processing discrepancy id :: " + discrepancy.getId() + " Enrollment ID::"
									+ discrepancy.getHixEnrollmentId() + " for file ID :: " + discrepancy.getFileId(), e);
					discrepancy.setReportSentStatus("E");
					discrepancy.setReportErrorMessage(EnrollmentUtils.shortStackTrace(e, 1000));
				}
			}
		}
		cmsPlanIdMap.clear();
		enrollmentDataMap.clear();
		//Update discrepancy report sent status
		enrlReconDiscrepancyRepository.save(discrepancyList);
		return csvDtoList;
	}

	/**
	 * Populate member information for the given member ID
	 * @param enrollmentDataDto
	 * @param dto
	 * @param memberId
	 */
	private void populateMemberInformation(ReconEnrollmentDTO enrollmentDataDto, EnrlDiscrepancyReportCsvDTO dto, Integer memberId) {
		if(EnrollmentUtils.isNotNullAndEmpty(enrollmentDataDto)){
			ReconMemberDTO memberDto = enrollmentDataDto.getMemberFromMemberId(memberId);
			ReconMemberDTO subscriberDto = enrollmentDataDto.getSubscriber();
			if(null != memberDto && null != subscriberDto){
				setMemberInfo(dto, memberDto, subscriberDto);
			}else if(null != subscriberDto){
				setMemberInfo(dto, subscriberDto, subscriberDto);
			}
		}
	}
	
	/**
	 * Set Member Information
	 * @param dto
	 * @param memberDto
	 * @param subscriberDto
	 */
	private void setMemberInfo(EnrlDiscrepancyReportCsvDTO dto, ReconMemberDTO memberDto, ReconMemberDTO subscriberDto) {
		dto.setMemberLastName(memberDto.getLastName());
		dto.setMemberFirstName(memberDto.getFirstName());
		dto.setExchangeAssignedMemberId(memberDto.getExchangeAssignedMemberId());
		dto.setIssuerAssignedMemberId(memberDto.getIssuerAssignedMemberId());
		dto.setSubscriberLastName(subscriberDto.getLastName());
		dto.setSubscriberFirstName(subscriberDto.getFirstName());
		dto.setIssuerAssignedSubscriberId(memberDto.getIssuerAssignedSubscriberId());
		dto.setExchangeAssignedSubscriberId(memberDto.getExchangeAssignedSubscriberId());
	}

	/**
	 * Writes CSV file from a list of EnrlDiscrepancyReportCsvDTO
	 * @param csvDtoList
	 * @param hiosIssuerId
	 * @param processingDate
	 * @param fileId 
	 * @param outboundDiscrepancyReportPath 
	 * @param summary 
	 * @param jobExecutionId 
	 * @return boolean
	 */
	private boolean createDiscrepancyFileAndUpdateStatus(List<EnrlDiscrepancyReportCsvDTO> csvDtoList, String hiosIssuerId, Date processingDate, Integer fileId, String outboundDiscrepancyReportPath, EnrlReconSummary summary, long jobExecutionId) {
		boolean isFileCreated = false;
		String discrepancyReportFileName =  null;
		String reconFilePath = null;
		String ecmDiscrepancyReportFileName =  null;
		String ecmDocId = null;

		Writer writer = null;
		reconFilePath = outboundDiscrepancyReportPath;
		//			String reconFilePath = "C:\\Recon";
		Collections.sort(csvDtoList, new Comparator<EnrlDiscrepancyReportCsvDTO>(){
			@Override
			public int compare(EnrlDiscrepancyReportCsvDTO o1, EnrlDiscrepancyReportCsvDTO o2) {
				if(o1.getExchangeAssignedPolicyId() != null && o2.getExchangeAssignedPolicyId() != null){
					return o1.getExchangeAssignedPolicyId().compareTo(o2.getExchangeAssignedPolicyId());
				}
				return 0;
			}

		});
		try{
			EnrollmentUtils.createDirectory(reconFilePath);
			discrepancyReportFileName = "to_"+hiosIssuerId+"_INDV_MONTHLYDISCREPANCY_"+summary.getCoverageYear()+"_"+DateUtil.dateToString(processingDate, "yyyyMMddHHmmss")+".OUT";
			ecmDiscrepancyReportFileName = "to_"+hiosIssuerId+"_INDV_MONTHLYDISCREPANCY_"+summary.getCoverageYear()+"_"+DateUtil.dateToString(processingDate, "yyyyMMddHHmmss")+ ".csv"; 
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(reconFilePath+ File.separatorChar+discrepancyReportFileName), "utf-8"));
			writer.write(EnrlDiscrepancyReportCsvDTO.HEADER+"\n");
			Iterator<EnrlDiscrepancyReportCsvDTO> dtoIterator = csvDtoList.iterator();
			while(dtoIterator.hasNext()){
				EnrlDiscrepancyReportCsvDTO dto = dtoIterator.next();
				if(dtoIterator.hasNext()){
					writer.write(dto.toCsv(',')+"\n");
				}else{
					writer.write(dto.toCsv(','));
				}
			}
			isFileCreated = true;
		}catch(Exception e){
			LOGGER.error("Exception in creating Reconciliation file :: " + e.getMessage(), e);
			logErrorInEnrlReconSummary(summary, EnrollmentUtils.shortStackTrace(e, 1000));
		}finally{
			if(writer != null){
				try {
					writer.close();
				} catch (IOException e) {
					LOGGER.error("Exception closing writer :: " + e.getMessage(), e);
				}
			}
		}
		try{
			if(isFileCreated){
				ecmDocId = uploadReportToEcm(ecmDiscrepancyReportFileName, hiosIssuerId, new File(reconFilePath+ File.separatorChar + discrepancyReportFileName));
				updateEnrlReconSummary(summary, processingDate, discrepancyReportFileName, ecmDocId);
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_SEND_REPORT_COPY_TO_EXCHG))) {
					copyToExchgFolder(reconFilePath, discrepancyReportFileName, jobExecutionId);
				}
			}else{
				rollbackDiscrepancyStatus(fileId);
			}
		}catch(Exception e){
			LOGGER.error("Error in updating statuses for file Id :: "+ fileId);
		}
		return isFileCreated;
	}
	
	/**
	 * Upload discrepancy report file to ECM
	 * @param ecmDiscrepancyReportFileName
	 * @param hiosIssuerId
	 * @param reportFile
	 * @return ecmId
	 */
	private String uploadReportToEcm(String ecmDiscrepancyReportFileName, String hiosIssuerId, File reportFile) {
		String relativeFilePath = "enrollment" + GhixConstants.FRONT_SLASH + "workbench"
				+ GhixConstants.FRONT_SLASH + hiosIssuerId;
		try {
			return ecmService.createContent(relativeFilePath, ecmDiscrepancyReportFileName,
					Files.readAllBytes(reportFile.toPath()),
					ECMConstants.ENROLLMENT.DOC_CATEGORY, ECMConstants.ENROLLMENT.ENROLLMENT_WORKBENCH,
					null);
		} catch (Exception e) {
			LOGGER.error("Failed to upload discrepancy report file to ECM ::  " + reportFile.getName(), e);
		}
		return null;
	}

	/**
	 * Sets discrepancy sent status as null for the given file ID in case of file creation failure
	 * @param fileId
	 */
	private void rollbackDiscrepancyStatus(Integer fileId) {
		List<EnrlReconDiscrepancy> discrepancyList = enrlReconDiscrepancyRepository.getReportedDiscrepanciesByFileId(fileId);
		if(null != discrepancyList){
			LOGGER.info("Rolling back changes for sent discrepancies since report not created for file Id :: "+ fileId);
			for(EnrlReconDiscrepancy discrepancy : discrepancyList){
				discrepancy.setReportSentStatus(null);
			}
			enrlReconDiscrepancyRepository.save(discrepancyList);
		}
	}

	/**
	 * Updates the enrollment reconciliation summary table after successful report generation
	 * @param summary
	 * @param processingDate
	 * @param discrepancyReportFileName
	 * @param ecmDocId 
	 */
	private void updateEnrlReconSummary(EnrlReconSummary summary , Date processingDate, String discrepancyReportFileName, String ecmDocId) {
		if(null != summary){
			LOGGER.info("Updating summary file Id :: "+ summary.getId() + " Discrepancy File Name :: "+ discrepancyReportFileName + " Date :: " + processingDate);
			summary.setStatus(SummaryStatus.COMPLETED.toString());
			summary.setDiscrepancyReportName(discrepancyReportFileName);
			summary.setDiscrepancyReportCreationTimestamp(processingDate);
			summary.setDiscrepancyReportError(null);
			summary.setDiscrepancyReportEcmId(ecmDocId);
			enrlReconSummaryRepository.saveAndFlush(summary);
		}
	}
	
	/**
	 * Updates the enrollment reconciliation summary table with error message if report generation fails
	 * @param summary
	 * @param errorMessage
	 */
	private void logErrorInEnrlReconSummary(EnrlReconSummary summary, String errorMessage) {
		if(null != summary){
			LOGGER.info("Updating summary file Id :: "+ summary.getId() + " with error message ");
//			summary.setStatus(SummaryStatus.STAGE5.toString());
			summary.setDiscrepancyReportError(errorMessage);
			enrlReconSummaryRepository.saveAndFlush(summary);
		}
	}


	@Override
	public List<String> getHiosIdListForReportGeneration(int applicableYear) {
		return enrlReconSummaryRepository.getHiosIdListForReportGeneration(applicableYear);
	}
	
	@Override
	public List<BatchJobExecution> getRunningBatchList(String jobName) {
		return batchJobExecutionService.findRunningJob(jobName);
	}
	
	/**
	 * Get processing date with adjusted time 
	 * @param processingDate 
	 * @param fileCount 
	 * @return Date
	 */
	private Date getProcessingDate(Date processingDate, int fileCount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(processingDate);
		cal.set(Calendar.SECOND, cal.get(Calendar.SECOND) + fileCount);
		return cal.getTime(); 
	}
	
	/**
	 * Enclose passed string within quotes
	 * @param str
	 * @return String
	 */
	private String encloseStringInQuotes(String str) {
		if(null != str){
			return "\""+str+"\"";
		}
		return null;
	}

	/**
	 * Returns the report destination path, null if not present
	 * @param hiosIssuerId
	 * @return outboundDiscrepancyReportPath , null if not present
	 */
	private String getOutboundDiscrepancyReportPath(String hiosIssuerId, List<Integer> fileIdList) {
		ExchgPartnerLookup exchgPartnerLookup = null;
		String outboundDiscrepancyReportPath=null;
		String outputPath=null;
		String errorMessage = null;
		exchgPartnerLookup = validationFolderPathService.findExchgPartnerByHiosIssuerID(hiosIssuerId, GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL,GhixConstants.OUTBOUND);
		if(exchgPartnerLookup!=null ){
			if(exchgPartnerLookup.getSourceDir() != null){
				outboundDiscrepancyReportPath = exchgPartnerLookup.getSourceDir();
				outputPath = outboundDiscrepancyReportPath;
				if ((new File(outputPath).exists())){
					return outboundDiscrepancyReportPath;
				}else{
					LOGGER.error(EnrollmentConstants.ERR_MSG_NO_SRC_EXCHG_DIRECTORY + hiosIssuerId);
					errorMessage = EnrollmentConstants.ERR_MSG_NO_SRC_EXCHG_DIRECTORY + hiosIssuerId;
				}
			}else{
				LOGGER.error(EnrollmentConstants.ERR_MSG_SRC_DIR_NOT_DEFINED + hiosIssuerId);
				errorMessage = EnrollmentConstants.ERR_MSG_SRC_DIR_NOT_DEFINED + hiosIssuerId;
			}
		}else{
			LOGGER.error(EnrollmentConstants.ERR_MSG_EXCHG_PARTNER_DOESNT_EXIST+ hiosIssuerId);
			errorMessage = EnrollmentConstants.ERR_MSG_EXCHG_PARTNER_DOESNT_EXIST + hiosIssuerId;
		}
		for(Integer fileId : fileIdList){
			EnrlReconSummary summary = enrlReconSummaryRepository.getReconSummaryByFileId(fileId);
			logErrorInEnrlReconSummary(summary, errorMessage);
		}
		return null;
	}
	
	/**
	 * Copy discrepancy report to Exchange folder
	 * @param reconFilePath
	 * @param discrepancyReportFileName
	 * @param jobExecutionId
	 */
	private void copyToExchgFolder(String reconFilePath, String discrepancyReportFileName, long jobExecutionId) {
		EnrollmentCmsFileTransferLog transferLog = new EnrollmentCmsFileTransferLog();
		String response = EnrollmentSftpUtil.uploadFile(cmsSftpService.getExchgSftpParameters(), reconFilePath+ File.separatorChar + discrepancyReportFileName, EXCHG_SFTP_RECON_FOLDERPATH, enrollmentGIMonitorUtil);
		List<EnrollmentSftpFileTransferDTO> transferList = platformGson.fromJson(response, listType);
		for(EnrollmentSftpFileTransferDTO dto : transferList) {
			transferLog.setFileName(dto.getFileName());
			transferLog.setFileSize(Long.parseLong(dto.getFileSize()));
			transferLog.setReportType(EnrollmentCmsFileTransferLog.ReportType.CARRIERRECON.toString());
			if(null != dto.getTransferType()){
				transferLog.setDirection(EnrollmentCmsFileTransferLog.Direction.valueOf(dto.getTransferType()).toString());
			}
			transferLog.setErrorDescription(dto.getErrorMsg());
			transferLog.setStatus(dto.getTransferStatus());
			transferLog.setSourceDirectory(reconFilePath);
			transferLog.setTargetDirectory(EXCHG_SFTP_RECON_FOLDERPATH);
			transferLog.setBatchExecutionId(jobExecutionId);
			try {
				enrollmentCmsFileTransferLogRepository.saveAndFlush(transferLog);
			} catch (Exception e) {
				LOGGER.error("Error saving transfer log", e);
			}
		}
	}
	
	/*
	private void populateMemberInformation(EnrlDiscrepancyReportCsvDTO dto, Integer memberId, Integer enrollmentId) {
		if(null != memberId && null != enrollmentId){
			List<Object[]> enrolleeDetails = enrolleeRepository.getEnrolleeDetailsForDiscrepancyReport(String.valueOf(memberId), enrollmentId);
			if(null != enrolleeDetails && !enrolleeDetails.isEmpty()){
				Object[] obj = enrolleeDetails.get(0);
				if(null != obj && obj.length >= 4){
					dto.setMemberLastName((String) obj[EnrollmentConstants.ZERO]);
					dto.setMemberFirstName((String) obj[EnrollmentConstants.ONE]);
					dto.setExchangeAssignedMemberId((String) obj[EnrollmentConstants.TWO]);
					dto.setIssuerAssignedMemberId((String) obj[EnrollmentConstants.THREE]);
				}
				if(obj.length > 6){
					String sponsorName = (String) obj[EnrollmentConstants.FOUR];
					if(EnrollmentUtils.isNotNullAndEmpty(sponsorName)){
						String[] sponsor = EnrollmentUtils.removeExtraSpaces(sponsorName).split(" ");
						if(null != sponsor && sponsor.length > 1){
							dto.setSubscriberLastName(sponsor[sponsor.length -1]);
						}else if(sponsor.length == 1){
							dto.setSubscriberLastName(sponsor[EnrollmentConstants.ZERO]);
						}
					}
					dto.setIssuerAssignedSubscriberId((String) obj[EnrollmentConstants.FIVE]);
					dto.setExchangeAssignedSubscriberId((String) obj[EnrollmentConstants.SIX]);
				}
			}
		}
	}*/
	

}
