<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/consumer-dashboard-util.tld" prefix="util"%>

<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />

<div class="gutter10">
  <h1>Welcome Back,&nbsp;<span id="user-name">${memberName}<span></h1>
  <div class="row-fluid">
    <div class="span3" id="sidebar">
      <util:myStuff ffmClear="${ffmClear}"></util:myStuff>
      <util:quickLinks user="${user}"></util:quickLinks>
    </div>
    <div class="span9" id="rightpanel">      
      <div class="header">
        <h4>My Household</h4>        
      </div>   	
        <div class="gutter10">
          <div class="row-fluid">
 			<div id="mydependent-info" class="row-fluid">
 			<c:if test="${householdContact !=null}">
				<h4 class="bottom-border">${householdContact.firstName} ${householdContact.lastName}</h4>
				<dl  class="dl-horizontal offset1">
					<dt>First Name</dt>
					<dd id="fname">${householdContact.firstName}</dd>
					<dt>Last Name</dt>
					<dd id="lname">${householdContact.lastName}</dd>
					<dt>Birth Date</dt>
					<dd id="d-o-b"><fmt:formatDate pattern="MM/dd/yyyy" value="${householdContact.birthDate}" var="dob" /> ${dob}</dd>
					<dt>Sex</dt>
					<dd id="gender">${householdContact.gender}</dd>
					<dt>Uses Tobacco</dt>
					<dd id="Tobacco-user">
						<c:choose>
			               	  	<c:when test="${householdContact.smoker == null}">
									<weak>N/A</weak>
								</c:when>
								<c:otherwise>
			               			<strong>${fn:toUpperCase(fn:substring(householdContact.smoker, 0, 1))}${fn:toLowerCase(fn:substring(householdContact.smoker, 1, -1))}</strong>
			               		</c:otherwise>
							</c:choose>
					</dd>
					<dt>Marital Status</dt>
					<dd id="marital-status">N/A</dd>
					<dt>Full-Time Student</dt>
					
					<dd id="ft-student">
					    <c:choose>
			               	  	<c:when test="${householdContact.fullTimeStudent == null}">
									<weak>N/A</weak>
								</c:when>
								<c:otherwise>
			               			<strong>${fn:toUpperCase(fn:substring(householdContact.fullTimeStudent, 0, 1))}${fn:toLowerCase(fn:substring(householdContact.fullTimeStudent, 1, -1))}</strong>
			               		</c:otherwise>
						</c:choose>
					</dd>
					<dt>Home Address</dt>
					<dd id="address">
					    <address>
					         ${householdContact.contactLocation.address1} <br>
			                 ${householdContact.contactLocation.city}, 
			                 ${householdContact.contactLocation.state} &nbsp;
			                 ${householdContact.contactLocation.zip}<br>
			                 ${fn:toUpperCase(fn:substring(householdContact.contactLocation.county, 0, 1))}${fn:toLowerCase(fn:substring(householdContact.contactLocation.county, 1, -1))}
					    </address></dd>
				</dl>
				</c:if>
				
				<c:forEach  items="${memberList}" var="member" varStatus="memberCount">
				<h4 class="bottom-border">${member.firstName} ${member.lastName}</h4>
				<dl  class="dl-horizontal offset1">
					<dt>First Name</dt>
					<dd id="fname">${member.firstName}</dd>
					<dt>Last Name</dt>
					<dd id="lname">${member.lastName}</dd>
					<dt>Birth Date</dt>
					<dd id="d-o-b">
					<fmt:formatDate pattern="MM/dd/yyyy" value="${member.birthDate}" var="dob" /> ${dob}
					</dd>
					<dt>Sex</dt>
					<dd id="gender">${member.gender}</dd>
					<dt>Uses Tobacco</dt>
					<dd id="Tobacco-user">
					   <c:choose>
			              	<c:when test="${member.smoker == null}">
								<weak>N/A</weak>
							</c:when>
							<c:otherwise>
			               		<strong>${fn:toUpperCase(fn:substring(member.smoker, 0, 1))}${fn:toLowerCase(fn:substring(member.smoker, 1, -1))}</strong>
			               	</c:otherwise>
					   </c:choose>
					</dd>
					<dt>Marital Status</dt>
					<dd id="marital-status">N/A</dd>
					<dt>Full-Time Student</dt>
					<dd id="ft-student">
					 <c:choose>
			               	  	<c:when test="${member.fullTimeStudent == null}">
									<weak>N/A</weak>
								</c:when>
								<c:otherwise>
			               			<strong>${fn:toUpperCase(fn:substring(member.fullTimeStudent, 0, 1))}${fn:toLowerCase(fn:substring(member.fullTimeStudent, 1, -1))}</strong>
			               		</c:otherwise>
						</c:choose>
									
					<dt>Home Address</dt>
					<dd id="address">
					   <address>
					         ${member.contactLocation.address1} <br>
			                 ${member.contactLocation.city}, 
			                 ${member.contactLocation.state} &nbsp;
			                 ${member.contactLocation.zip}<br>
			                 ${fn:toUpperCase(fn:substring(member.contactLocation.county, 0, 1))}${fn:toLowerCase(fn:substring(member.contactLocation.county, 1, -1))}
					   </address>
					</dd>
				</dl>
			 </c:forEach>
			</div>
          </div>
      </div>
  </div><!--/rightpanel--> 
  </div><!--/row-fluid-->        
</div><!--/main .gutter10-->
<jsp:include page="reportChange.jsp" >
	<jsp:param name="memberName" value="${memberName}" />
</jsp:include> 

<script>
$(".inactive").click(function(e) {
	e.preventDefault();
});
</script>