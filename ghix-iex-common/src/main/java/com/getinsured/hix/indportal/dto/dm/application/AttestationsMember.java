package com.getinsured.hix.indportal.dto.dm.application;

public class AttestationsMember {
	 private Boolean requestingCoverageIndicator;
	 private NonMagi nonMagi;
	 private Demographic demographic;
	 private AttestationMembersIncome income;
	 private Family family;
	 private LawfulPresence lawfulPresence;
	 private InsuranceCoverage insuranceCoverage;
	 private Medicaid medicaid;
	 private Chip chip;
	 private Other other;


	 // Getter Methods 

	 public Boolean getRequestingCoverageIndicator() {
	  return requestingCoverageIndicator;
	 }

	 public NonMagi getNonMagi() {
	  return nonMagi;
	 }

	 public Demographic getDemographic() {
	  return demographic;
	 }

	 public AttestationMembersIncome getIncome() {
	  return income;
	 }

	 public Family getFamily() {
	  return family;
	 }

	 public LawfulPresence getLawfulPresence() {
	  return lawfulPresence;
	 }

	 public InsuranceCoverage getInsuranceCoverage() {
	  return insuranceCoverage;
	 }

	 public Medicaid getMedicaid() {
	  return medicaid;
	 }

	 public Chip getChip() {
	  return chip;
	 }

	 public Other getOther() {
	  return other;
	 }

	 // Setter Methods 

	 public void setRequestingCoverageIndicator(Boolean requestingCoverageIndicator) {
	  this.requestingCoverageIndicator = requestingCoverageIndicator;
	 }

	 public void setNonMagi(NonMagi nonMagi) {
	  this.nonMagi= nonMagi;
	 }

	 public void setDemographic(Demographic demographic) {
	  this.demographic = demographic;
	 }

	 public void setIncome(AttestationMembersIncome income) {
	  this.income = income;
	 }

	 public void setFamily(Family family) {
	  this.family = family;
	 }

	 public void setLawfulPresence(LawfulPresence lawfulPresence) {
	  this.lawfulPresence = lawfulPresence;
	 }

	 public void setInsuranceCoverage(InsuranceCoverage insuranceCoverage) {
	  this.insuranceCoverage = insuranceCoverage;
	 }

	 public void setMedicaid(Medicaid medicaid) {
	  this.medicaid = medicaid;
	 }

	 public void setChip(Chip chip) {
	  this.chip = chip;
	 }

	 public void setOther(Other other) {
	  this.other = other;
	 }
	}
