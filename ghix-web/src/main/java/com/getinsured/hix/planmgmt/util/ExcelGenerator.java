/**
 * 
 */
package com.getinsured.hix.planmgmt.util;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.util.GhixConstants;

/**
 * @author gorai_k
 * 
 */
public final class ExcelGenerator {

	private static final String FORMAT = "$#,#0.00";
	private static final String PERCENT_FORMAT = "0.00%";
	//private static final String FORMAT1 = "$0.00";
	//private static final String FORMAT2 = "$00.00";
	//private static final String FORMAT3 = "$000.00";
	//private static final String FORMAT4 = "$0,000.00";
	//private static final String FORMAT5 = "$00,000.00";
	//private static final String FORMAT6 = "$00,000,000.00";
	
	private static final int ZERO = 0;
	private static final int ONE = 1;
	private static final int TWO = 2;
	private static final int THREE = 3;
	private static final int FOUR = 4;
	private static final int FIVE = 5;
	private static final int SIX = 6;
	private static final int TEN = 10;
	private static final int TWENTY = 20;
	private static final int FIFTEEN = 15;
	private static final int TWENTY_FIVE = 25;
	private static final int THIRTY = 30;
	private static final int FIFTY = 50;
	private static final int TWO_FIFTY_SIX = 256;
	private static final int THREE_HUNDRED = 300;
	private static final String region = "Region";
	private static final String minAge = "minAge";
	private static final String maxAge = "maxAge";
	private static final String tobacco = "tobacco";
	private static final String premiumPMPM = "premium(PMPM)";
	private static final String effStartDate = "Plan Rate Effective Start Date";
	private static final String effEndDate = "Plan Rate Effective End Date";
	private static final String nameAndnumber = "Plan Name and Number";
	
	private ExcelGenerator(){}

	public static HSSFWorkbook createBenefitsExcelSheet(
			List<String[]> planBenefitsList, int rows) {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet();
		sheet.setColumnWidth(0, 50 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(1, 15 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(2, 17 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(3, 20 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(4, 30 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(5, 20 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(6, 20 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(7, 17 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(8, 30 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(9, 20 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(10, 20 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(11, 20 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(12, 20 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(13, 20 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(14, 20 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(15, 30 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(16, 20 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(17, 20 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(18, 20 * TWO_FIFTY_SIX);
		sheet.setColumnWidth(19, 20 * TWO_FIFTY_SIX);

		HSSFPalette palette = workbook.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.DARK_YELLOW.index, (byte) 224,
				(byte) 230, (byte) 119);
		palette.setColorAtIndex(HSSFColor.BROWN.index, (byte) 193, (byte) 213,
				(byte) 245);
		palette.setColorAtIndex(HSSFColor.PLUM.index, (byte) 206, (byte) 188,
				(byte) 207);
		palette.setColorAtIndex(HSSFColor.RED.index, (byte) 233, (byte) 197,
				(byte) 235);
		palette.setColorAtIndex(HSSFColor.BLACK.index, (byte) 200, (byte) 212,
				(byte) 199);
		palette.setColorAtIndex(HSSFColor.SEA_GREEN.index, (byte) 122,
				(byte) 168, (byte) 119);
		palette.setColorAtIndex(HSSFColor.LAVENDER.index, (byte) 222,
				(byte) 179, (byte) 144);
		// Set bold font style
		HSSFFont font = workbook.createFont();
		// font.setFontHeight((short)100);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		HSSFCellStyle styleA = workbook.createCellStyle();
		styleA.setFont(font);
		styleA.setFillForegroundColor(HSSFColor.DARK_YELLOW.index);
		styleA.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styleA.setBorderRight(CellStyle.BORDER_THIN);
		styleA.setBorderBottom(CellStyle.BORDER_THIN);
		styleA.setBorderLeft(CellStyle.BORDER_THIN);
		styleA.setBorderTop(CellStyle.BORDER_THIN);

		HSSFCellStyle style2 = workbook.createCellStyle();
		style2.setFont(font);
		style2.setFillForegroundColor(HSSFColor.BROWN.index);
		style2.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style2.setBorderRight(CellStyle.BORDER_THIN);
		style2.setBorderBottom(CellStyle.BORDER_THIN);
		style2.setBorderLeft(CellStyle.BORDER_THIN);
		style2.setBorderTop(CellStyle.BORDER_THIN);
		style2.setAlignment(CellStyle.ALIGN_CENTER);

		HSSFCellStyle style3 = workbook.createCellStyle();
		style3.setFont(font);
		style3.setFillForegroundColor(HSSFColor.PLUM.index);
		style3.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style3.setBorderRight(CellStyle.BORDER_THIN);
		style3.setBorderBottom(CellStyle.BORDER_THIN);
		style3.setBorderLeft(CellStyle.BORDER_THIN);
		style3.setBorderTop(CellStyle.BORDER_THIN);
		style3.setAlignment(CellStyle.ALIGN_CENTER);

		HSSFCellStyle style4 = workbook.createCellStyle();
		style4.setFont(font);
		style4.setFillForegroundColor(HSSFColor.RED.index);
		style4.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style4.setBorderRight(CellStyle.BORDER_THIN);
		style4.setBorderBottom(CellStyle.BORDER_THIN);
		style4.setBorderLeft(CellStyle.BORDER_THIN);
		style4.setBorderTop(CellStyle.BORDER_THIN);
		style4.setAlignment(CellStyle.ALIGN_CENTER);

		HSSFCellStyle style5 = workbook.createCellStyle();
		style5.setFont(font);
		style5.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
		style5.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style5.setBorderRight(CellStyle.BORDER_THIN);
		style5.setBorderBottom(CellStyle.BORDER_THIN);
		style5.setBorderLeft(CellStyle.BORDER_THIN);
		style5.setBorderTop(CellStyle.BORDER_THIN);
		style5.setAlignment(CellStyle.ALIGN_CENTER);
		
		HSSFCellStyle style6 = workbook.createCellStyle();
		style6.setFont(font);
		style6.setFillForegroundColor(HSSFColor.BLACK.index);
		style6.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style6.setBorderRight(CellStyle.BORDER_THIN);
		style6.setBorderBottom(CellStyle.BORDER_THIN);
		style6.setBorderLeft(CellStyle.BORDER_THIN);
		style6.setBorderTop(CellStyle.BORDER_THIN);
		style6.setAlignment(CellStyle.ALIGN_CENTER);
		
		HSSFCellStyle style7 = workbook.createCellStyle();
		style7.setFont(font);
		style7.setFillForegroundColor(HSSFColor.LAVENDER.index);
		style7.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style7.setBorderRight(CellStyle.BORDER_THIN);
		style7.setBorderBottom(CellStyle.BORDER_THIN);
		style7.setBorderLeft(CellStyle.BORDER_THIN);
		style7.setBorderTop(CellStyle.BORDER_THIN);
		style7.setAlignment(CellStyle.ALIGN_CENTER);

		HSSFCellStyle style11T1 = workbook.createCellStyle();
		style11T1.setFont(font);
		style11T1.setFillForegroundColor(HSSFColor.YELLOW.index);
		style11T1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style11T1.setBorderRight(CellStyle.BORDER_THIN);
		style11T1.setBorderTop(CellStyle.BORDER_THIN);
		style11T1.setBorderBottom(CellStyle.BORDER_THIN);
		style11T1.setBorderLeft(CellStyle.BORDER_THIN);
		style11T1.setAlignment(CellStyle.ALIGN_CENTER);

		HSSFCellStyle alignStyle = workbook.createCellStyle();
		alignStyle.setAlignment(CellStyle.ALIGN_RIGHT);

		HSSFCellStyle alignStyle2 = workbook.createCellStyle();
		alignStyle2.setAlignment(CellStyle.ALIGN_RIGHT);

		HSSFCellStyle alignStyle3 = workbook.createCellStyle();
		alignStyle3.setDataFormat(workbook.createDataFormat()
				.getFormat(FORMAT));
		alignStyle3.setAlignment(CellStyle.ALIGN_RIGHT);

		HSSFCellStyle alignStyle5 = workbook.createCellStyle();
		alignStyle5.setDataFormat(workbook.createDataFormat()
				.getFormat(FORMAT));
		alignStyle5.setAlignment(CellStyle.ALIGN_RIGHT);

		HSSFCellStyle alignStyle8 = workbook.createCellStyle();
		alignStyle8.setDataFormat(workbook.createDataFormat()
				.getFormat(FORMAT));
		alignStyle8.setAlignment(CellStyle.ALIGN_RIGHT);
		
		HSSFCellStyle percentStyle = workbook.createCellStyle();
		percentStyle.setDataFormat(workbook.createDataFormat().getFormat(PERCENT_FORMAT));
		percentStyle.setAlignment(CellStyle.ALIGN_RIGHT);


		int rownum = 0;
		boolean applyStyle = false;

		for (String[] benefitArray : planBenefitsList) {
			int cellnum = 0;

			if (rownum == 0) {
				Row row = sheet.createRow(rownum++);
				Cell cellA = row.createCell(0);
				cellA.setCellValue(benefitArray[0]);
				cellA.setCellStyle(styleA);
				sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 2));
				sheet.addMergedRegion(new CellRangeAddress(0, 0, 3, 4));
				Cell cellB = row.createCell(1);
				Cell cellBC = row.createCell(2);
				cellB.setCellValue(benefitArray[1]);
				cellB.setCellStyle(style2);
				cellBC.setCellStyle(style2);
				Cell cellC = row.createCell(3);
				Cell cellCD = row.createCell(4);
				cellC.setCellValue(benefitArray[2]);
				cellC.setCellStyle(style2);
				cellCD.setCellStyle(style2);

			}
			else if (rownum == 1) {
				Row row = sheet.createRow(rownum++);
				Cell cellA = row.createCell(0);
				cellA.setCellValue(benefitArray[0]);
				cellA.setCellStyle(styleA);
				Cell cellB = row.createCell(1);
				cellB.setCellStyle(style2);
				cellB.setCellValue(benefitArray[1]);
			}
			else if (rownum == 2) {
				Row row = sheet.createRow(rownum++);
				Cell cellA = row.createCell(0);
				cellA.setCellValue(benefitArray[0]);
				cellA.setCellStyle(styleA);
				Cell cellB = row.createCell(1);
				cellB.setCellStyle(style2);
				cellB.setCellValue(benefitArray[1]);
			}
			else if (rownum == 3) {
				Row row1 = sheet.createRow(rownum++);
				Cell cellA = row1.createCell(0);
				cellA.setCellValue(benefitArray[0]);
				sheet.addMergedRegion(new CellRangeAddress(3, 3, 1, 3));
				sheet.addMergedRegion(new CellRangeAddress(3, 3, 4, 6));
				sheet.addMergedRegion(new CellRangeAddress(3, 3, 7, 8));
				sheet.addMergedRegion(new CellRangeAddress(3, 3, 9, 10));
				Cell cellB1 = row1.createCell(1);
				Cell cellB2 = row1.createCell(2);
				Cell cellB3 = row1.createCell(3);
				cellB1.setCellValue(benefitArray[1]);
				cellB1.setCellStyle(style7);
				cellB2.setCellStyle(style7);
				cellB3.setCellStyle(style7);

				Cell cellE = row1.createCell(4);
				Cell cellE5 = row1.createCell(5);
				Cell cellE6 = row1.createCell(6);

				cellE.setCellValue(benefitArray[2]);
				cellE.setCellStyle(style3);
				cellE5.setCellStyle(style3);
				cellE6.setCellStyle(style3);

				Cell cellH = row1.createCell(7);
				Cell cellH8 = row1.createCell(8);
				cellH.setCellValue(benefitArray[3]);
				cellH.setCellStyle(style6);
				cellH8.setCellStyle(style6);

				Cell cellj = row1.createCell(9);
				Cell cellj10 = row1.createCell(10);
				cellj.setCellValue(benefitArray[4]);
				cellj.setCellStyle(style11T1);
				cellj10.setCellStyle(style11T1);
			}
			else if (rownum == 4) {
				Row row = sheet.createRow(rownum++);
				for (String benefit : benefitArray) {
					Cell cell = row.createCell(cellnum++);
					cell.setCellValue(benefit);

					cell.setCellStyle(style2);
					if (cellnum == 1) {
						cell.setCellStyle(styleA);
					}
				}
			}
			else if (rownum == 5 + rows) {
				Row row = sheet.createRow(rownum++);
				Cell cellA = row.createCell(0);
				cellA.setCellValue(benefitArray[0]);

				sheet.addMergedRegion(new CellRangeAddress(5 + rows, 5 + rows, 1, 11));
				sheet.addMergedRegion(new CellRangeAddress(5 + rows, 5 + rows, 12, 16));

				Cell cellB1 = row.createCell(1);
				Cell cellB2 = row.createCell(2);
				Cell cellB3 = row.createCell(3);
				Cell cellB4 = row.createCell(4);
				Cell cellB5 = row.createCell(5);
				Cell cellB6 = row.createCell(6);
				Cell cellB7 = row.createCell(7);
				Cell cellB8 = row.createCell(8);
				Cell cellB9 = row.createCell(9);
				Cell cellB10 = row.createCell(10);
				Cell cellB11 = row.createCell(11);
				cellB1.setCellValue(benefitArray[1]);
				cellB1.setCellStyle(style7);
				cellB2.setCellStyle(style7);
				cellB3.setCellStyle(style7);
				cellB4.setCellStyle(style7);
				cellB5.setCellStyle(style7);
				cellB6.setCellStyle(style7);
				cellB7.setCellStyle(style7);
				cellB8.setCellStyle(style7);
				cellB9.setCellStyle(style7);
				cellB10.setCellStyle(style7);
				cellB11.setCellStyle(style7);

				Cell cellB12 = row.createCell(12);
				Cell cellB13 = row.createCell(13);
				Cell cellB14 = row.createCell(14);
				Cell cellB15 = row.createCell(15);
				Cell cellB16 = row.createCell(16);
				cellB12.setCellValue(benefitArray[2]);
				cellB12.setCellStyle(style2);
				cellB13.setCellStyle(style2);
				cellB14.setCellStyle(style2);
				cellB15.setCellStyle(style2);
				cellB16.setCellStyle(style2);
			}
			else if (rownum == 6 + rows) {
				Row row1 = sheet.createRow(rownum++);

				HSSFCellStyle style11T2 = workbook.createCellStyle();
				style11T2.setFont(font);
				style11T2.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
				style11T2.setFillPattern(CellStyle.SOLID_FOREGROUND);
				style11T2.setBorderRight(CellStyle.BORDER_THIN);
				style11T2.setBorderBottom(CellStyle.BORDER_THIN);
				style11T2.setBorderLeft(CellStyle.BORDER_THIN);

				style11T2.setAlignment(CellStyle.ALIGN_CENTER);

				Cell cellA = row1.createCell(0);
				cellA.setCellValue(benefitArray[0]);
				sheet.addMergedRegion(new CellRangeAddress(6 + rows, 6 + rows, 1, 5));
				sheet.addMergedRegion(new CellRangeAddress(6 + rows, 6 + rows, 6, 10));
				Cell cellB1 = row1.createCell(1);
				Cell cellB2 = row1.createCell(2);
				Cell cellB3 = row1.createCell(3);
				Cell cellB4 = row1.createCell(4);
				Cell cellB5 = row1.createCell(5);

				cellB1.setCellValue(benefitArray[1]);
				cellB1.setCellStyle(style11T1);
				cellB2.setCellStyle(style11T1);
				cellB3.setCellStyle(style11T1);
				cellB4.setCellStyle(style11T1);
				cellB5.setCellStyle(style11T1);

				Cell cellC = row1.createCell(6);
				Cell cellC7 = row1.createCell(7);
				Cell cellC8 = row1.createCell(8);
				Cell cellC9 = row1.createCell(9);
				Cell cellC10 = row1.createCell(10);

				cellC.setCellValue(benefitArray[2]);
				cellC.setCellStyle(style5);
				cellC7.setCellStyle(style5);
				cellC8.setCellStyle(style5);
				cellC9.setCellStyle(style5);
				cellC10.setCellStyle(style5);
			}
			else {
				Row row = sheet.createRow(rownum++);
				applyStyle = false;
				String format = FORMAT;
				for (String benefit : benefitArray) {
					Cell cell = row.createCell(cellnum++);

					cell.setCellType(Cell.CELL_TYPE_NUMERIC);

					if (rows > 0) {
						if ((rownum >= 5 && rownum <= 5 + rows)
								&& benefit != null && !benefit.isEmpty()
								&& cellnum >= 2) {
							format = FORMAT;
							if(cellnum == 4 || cellnum == 7){
								format = PERCENT_FORMAT;
								double parsedBenefit = Double.parseDouble(benefit);
								if(parsedBenefit > 0){
								  cell.setCellValue(parsedBenefit/100);
								}
								else{	
									  cell.setCellValue(parsedBenefit);
								}
								cell.setCellStyle(percentStyle);
							}
							else{
								cell.setCellValue(Double.parseDouble(benefit));
								benefit = benefit
										.substring(0, benefit.indexOf("."));
								if (benefit.length() <= 1) {
									alignStyle2.setDataFormat(workbook
											.createDataFormat().getFormat(format));
									cell.setCellStyle(alignStyle2);
								} else if (benefit.length() <= 2) {
									alignStyle2.setDataFormat(workbook
											.createDataFormat().getFormat(format));
									cell.setCellStyle(alignStyle2);
								} else if (benefit.length() <= 3) {
									alignStyle2.setDataFormat(workbook
											.createDataFormat().getFormat(format));
									cell.setCellStyle(alignStyle2);
	
								} else if (benefit.length() == 4) {
									cell.setCellStyle(alignStyle3);
								} else if (benefit.length() >= 5
										&& benefit.length() <= 7) {
									cell.setCellStyle(alignStyle5);
								} else {
									cell.setCellStyle(alignStyle8);
								}
							}
						} else {
							cell.setCellValue(benefit);
						}

					}

					if (rownum > 8 + rows
							&& (cellnum == 2 || cellnum == 4 || cellnum == 7
									|| cellnum == 9 || cellnum == 13
									|| cellnum == 15 || cellnum == 18)
							&& benefit != null) {
						cell.setCellValue(benefit);
						cell.setCellStyle(alignStyle);
					}

					if (applyStyle) {
						cell.setCellStyle(style2);

					}
					if (benefit != null && benefit.equalsIgnoreCase("benefits")) {
						cell.setCellStyle(styleA);
						applyStyle = true;
					}
				}
			}

		}

		return workbook;
	}
	
	public static HSSFWorkbook createServiceAreaExcelSheet(List<String[]> serviceAreaDetails){
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet();
		sheet.setColumnWidth(ZERO, TWENTY_FIVE * TWO_FIFTY_SIX);
		sheet.setColumnWidth(ONE, FIFTEEN * TWO_FIFTY_SIX);
		sheet.setColumnWidth(TWO, FIFTEEN * TWO_FIFTY_SIX);
		
		//Set bold font style
		HSSFFont font = workbook.createFont();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		HSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		
		HSSFCellStyle style1 = workbook.createCellStyle();
		style1.setFont(font);
		style1.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
		style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style1.setBorderRight(CellStyle.BORDER_THIN);
		style1.setBorderTop(CellStyle.BORDER_THIN);
		style1.setBorderBottom(CellStyle.BORDER_THIN);
		style1.setBorderLeft(CellStyle.BORDER_THIN);
		style1.setAlignment(CellStyle.ALIGN_LEFT);
		
		HSSFCellStyle style2 = workbook.createCellStyle();
		style2.setFont(font);
		style2.setFillForegroundColor(HSSFColor.ROYAL_BLUE.index);
		style2.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style2.setBorderRight(CellStyle.BORDER_THIN);
		style2.setBorderBottom(CellStyle.BORDER_THIN);
		style2.setBorderLeft(CellStyle.BORDER_THIN);
		style2.setAlignment(CellStyle.ALIGN_CENTER);
		
		HSSFCellStyle style3 = workbook.createCellStyle();
		style3.setFont(font);
		style3.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
		style3.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style3.setBorderRight(CellStyle.BORDER_THIN);
		style3.setBorderBottom(CellStyle.BORDER_THIN);
		style3.setBorderLeft(CellStyle.BORDER_THIN);
		style3.setAlignment(CellStyle.ALIGN_CENTER);
		
		HSSFCellStyle style4 = workbook.createCellStyle();
		style4.setAlignment(CellStyle.ALIGN_CENTER);
		
		int rownum = ZERO;
		for (String []details : serviceAreaDetails) {
			Row row = sheet.createRow(rownum++);
			if(rownum == ONE){
				Cell cellA = row.createCell(ZERO);
				cellA.setCellValue(details[ZERO]);
				cellA.setCellStyle(style1);
				
				Cell cellB = row.createCell(ONE);
				cellB.setCellValue(details[ONE]);
				cellB.setCellStyle(style3);
				Cell cellC = row.createCell(TWO);
				cellC.setCellStyle(style3);
				
				Cell cellD = row.createCell(THREE);
				cellD.setCellValue(details[TWO]);
				cellD.setCellStyle(style3);
				Cell cellE = row.createCell(FOUR);
				cellE.setCellStyle(style3);
				sheet.addMergedRegion(new CellRangeAddress(ZERO, ZERO, ONE, TWO));
				sheet.addMergedRegion(new CellRangeAddress(ZERO, ZERO, THREE, FOUR));
			}else{
				int cellnum = ZERO;
				for(String value : details){
					Cell cell = row.createCell(cellnum++);
					if(cellnum == ONE && rownum > FIVE){
						cell.setCellValue(value);
					}else{
						cell.setCellValue(value); 
					}
					if(cellnum == ONE && rownum <= FOUR){
						cell.setCellStyle(style1);
					}else if(cellnum != ONE && rownum <= FOUR){
						cell.setCellStyle(style3);
					}else if(rownum == FIVE){
						cell.setCellStyle(style2);
					}else{
						cell.setCellStyle(style4);
					}
				}
			}
			
		}
		return workbook;
	}
	
	public static HSSFWorkbook createRateExcelSheet(List<Object[]> rateList, Plan plan,String[] effectiveStartDate, String[] effectiveEndDate){
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet();
		
		HSSFPalette palette = workbook.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.ROYAL_BLUE.index, (byte) 78,
				(byte) 150, (byte) 227);
		palette.setColorAtIndex(HSSFColor.GREY_25_PERCENT.index, (byte) 213,
				(byte) 220, (byte) 227);
		
		sheet.setColumnWidth(ZERO, THIRTY * TWO_FIFTY_SIX);
		sheet.setColumnWidth(ONE, FIFTEEN * TWO_FIFTY_SIX);
		sheet.setColumnWidth(TWO, FIFTEEN * TWO_FIFTY_SIX);
		sheet.setColumnWidth(THREE, TEN * TWO_FIFTY_SIX);
		sheet.setColumnWidth(FOUR, TWENTY * TWO_FIFTY_SIX);
		
		//Set bold font style
		HSSFFont font = workbook.createFont();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		HSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		
		HSSFCellStyle style1 = workbook.createCellStyle();
		style1.setFont(font);
		style1.setFillForegroundColor(HSSFColor.BRIGHT_GREEN.index);
		style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style1.setBorderRight(CellStyle.BORDER_THIN);
		style1.setBorderTop(CellStyle.BORDER_THIN);
		style1.setBorderBottom(CellStyle.BORDER_THIN);
		style1.setBorderLeft(CellStyle.BORDER_THIN);
		style1.setAlignment(CellStyle.ALIGN_LEFT);
		
		HSSFCellStyle style2 = workbook.createCellStyle();
		style2.setFont(font);
		style2.setFillForegroundColor(HSSFColor.ROYAL_BLUE.index);
		style2.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style2.setBorderRight(CellStyle.BORDER_THIN);
		style2.setBorderBottom(CellStyle.BORDER_THIN);
		style2.setBorderLeft(CellStyle.BORDER_THIN);
		style2.setAlignment(CellStyle.ALIGN_CENTER);
		
		HSSFCellStyle style3 = workbook.createCellStyle();
		style3.setFont(font);
		style3.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
		style3.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style3.setBorderRight(CellStyle.BORDER_THIN);
		style3.setBorderBottom(CellStyle.BORDER_THIN);
		style3.setBorderLeft(CellStyle.BORDER_THIN);
		style3.setAlignment(CellStyle.ALIGN_CENTER);
		
		HSSFCellStyle style4 = workbook.createCellStyle();
		style4.setAlignment(CellStyle.ALIGN_CENTER);
		
		HSSFCellStyle style5 = workbook.createCellStyle();
		style5.setAlignment(CellStyle.ALIGN_CENTER);
		style5.setFont(font);
		style5.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		style5.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style5.setBorderTop(CellStyle.BORDER_THIN);
		style5.setBorderRight(CellStyle.BORDER_THIN);
		style5.setBorderBottom(CellStyle.BORDER_THIN);
		style5.setBorderLeft(CellStyle.BORDER_THIN);
		
		int rownum = ZERO;
			
			for(int i= 0; i<FIVE; i++){
			if(rownum == 0){
			Row row = sheet.createRow(rownum++);
			Cell cellA = row.createCell(ZERO);
			cellA.setCellStyle(style1);
			cellA.setCellValue(nameAndnumber);
			
			Cell cellB = row.createCell(ONE);
			cellB.setCellStyle(style2);
			Cell cellC = row.createCell(TWO);
			cellC.setCellStyle(style2);
			sheet.addMergedRegion(new CellRangeAddress(ZERO, ZERO, ONE, TWO));
			cellB.setCellValue(plan.getName());
			
			Cell cellD = row.createCell(THREE);
			cellD.setCellValue(plan.getIssuerPlanNumber());
			cellD.setCellStyle(style2);
			Cell cellE = row.createCell(FOUR);
			cellE.setCellStyle(style2);
			sheet.addMergedRegion(new CellRangeAddress(ZERO, ZERO, THREE, FOUR));
			}else if(rownum == ONE){
				Row row = sheet.createRow(rownum++);
				int cellnum = ZERO;
				Cell cellA = row.createCell(cellnum++);
				cellA.setCellStyle(style1);
				cellA.setCellValue(effStartDate);
				
				Cell cellB = row.createCell(cellnum++);
				cellB.setCellValue(effectiveStartDate[ONE]+GhixConstants.FRONT_SLASH+effectiveStartDate[TWO]+GhixConstants.FRONT_SLASH+effectiveStartDate[ZERO]);
				cellB.setCellStyle(style3);
			}else if(rownum == TWO){
				Row row = sheet.createRow(rownum++);
				int cellnum = ZERO;
				Cell cellA = row.createCell(cellnum++);
				cellA.setCellStyle(style1);
				cellA.setCellValue(effEndDate);
				
				Cell cellB = row.createCell(cellnum++);
				cellB.setCellValue(effectiveEndDate[ONE]+GhixConstants.FRONT_SLASH+effectiveEndDate[TWO]+GhixConstants.FRONT_SLASH+effectiveEndDate[ZERO]);
				cellB.setCellStyle(style3);
			}
			else if(rownum == THREE){
				Row row = sheet.createRow(rownum++);
				Cell cellA = row.createCell(ZERO);
				cellA.setCellValue(region);
				cellA.setCellStyle(style5);
				
				Cell cellB = row.createCell(ONE);
				cellB.setCellValue(minAge);
				cellB.setCellStyle(style5);
				
				Cell cellC = row.createCell(TWO);
				cellC.setCellValue(maxAge);
				cellC.setCellStyle(style5);
				
				Cell cellD = row.createCell(THREE);
				cellD.setCellValue(tobacco);
				cellD.setCellStyle(style5);
				
				Cell cellE = row.createCell(FOUR);
				cellE.setCellValue(premiumPMPM);
				cellE.setCellStyle(style5);
			}else{
				for(Object[] rateData : rateList){
					
					Row row = sheet.createRow(rownum++);
						Cell cellA = row.createCell(ZERO);
						cellA.setCellValue(rateData[ZERO].toString());
						cellA.setCellStyle(style4);
						
						Cell cellB = row.createCell(ONE);
						cellB.setCellValue(rateData[ONE].toString());
						cellB.setCellStyle(style4);
						
						Cell cellC = row.createCell(TWO);
						cellC.setCellValue(rateData[TWO].toString());
						cellC.setCellStyle(style4);
						
						Cell cellD = row.createCell(THREE);
						cellD.setCellValue(rateData[THREE]!= null ? rateData[THREE].toString():"");
						cellD.setCellStyle(style4);
						
						Cell cellE = row.createCell(FOUR);
						cellE.setCellValue(Double.parseDouble(rateData[FOUR].toString()));
						cellE.setCellStyle(style4);
					}
				}
			}
		return workbook;
	}
	
	
	public static HSSFWorkbook createEnrollReportExcelSheet(List<String[]> enrolleeData){
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet();
		sheet.setColumnWidth(ZERO, TWENTY_FIVE * THREE_HUNDRED);
		sheet.setColumnWidth(ONE, FIFTEEN * TWO_FIFTY_SIX);
		sheet.setColumnWidth(TWO, FIFTEEN * TWO_FIFTY_SIX);
		sheet.setColumnWidth(THREE, FIFTEEN * TWO_FIFTY_SIX);
		sheet.setColumnWidth(FOUR, FIFTEEN * THREE_HUNDRED);
		sheet.setColumnWidth(FIVE, FIFTEEN * TWO_FIFTY_SIX);
		sheet.setColumnWidth(SIX, FIFTEEN * TWO_FIFTY_SIX);
		
		//Set bold font style
		HSSFFont font = workbook.createFont();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		HSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		
		HSSFCellStyle style1 = workbook.createCellStyle();
		style1.setFont(font);
		style1.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
		style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style1.setBorderRight(CellStyle.BORDER_THIN);
		style1.setBorderTop(CellStyle.BORDER_THIN);
		style1.setBorderBottom(CellStyle.BORDER_THIN);
		style1.setBorderLeft(CellStyle.BORDER_THIN);
		style1.setAlignment(CellStyle.ALIGN_LEFT);
		
		HSSFCellStyle style2 = workbook.createCellStyle();
		style2.setFont(font);
		style2.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
		style2.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style2.setBorderRight(CellStyle.BORDER_THIN);
		style2.setBorderBottom(CellStyle.BORDER_THIN);
		style2.setBorderLeft(CellStyle.BORDER_THIN);
		style2.setAlignment(CellStyle.ALIGN_CENTER);
		
		HSSFCellStyle style3 = workbook.createCellStyle();
		style3.setFont(font);
		style3.setFillForegroundColor(HSSFColor.LAVENDER.index);
		style3.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style3.setBorderRight(CellStyle.BORDER_THIN);
		style3.setBorderBottom(CellStyle.BORDER_THIN);
		style3.setBorderLeft(CellStyle.BORDER_THIN);
		style3.setAlignment(CellStyle.ALIGN_CENTER);
		
		HSSFCellStyle style4 = workbook.createCellStyle();
		style4.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		style4.setAlignment(CellStyle.ALIGN_CENTER);
		
		
		int rownum = ZERO;
		Row row = null;
		Cell cell = null;
		int cellnum = ZERO;
		//Cell cellA = null;
		//Cell cellB = null; 
		
		for (String []details : enrolleeData) {
			row = sheet.createRow(rownum++);
			//if(rownum == ONE){
				/*cellA = row.createCell(ZERO);
				cellA.setCellValue(details[ZERO]);
				cellA.setCellStyle(style1);
				
				cellB = row.createCell(ONE);
				cellB.setCellValue(details[ONE]);
				cellB.setCellStyle(style3);*/
			
			//}else{
				cellnum = ZERO;
				for(String value : details){
					cell = row.createCell(cellnum++);
					if(cellnum == ONE && rownum > THREE){
						cell.setCellValue(value);
					}else{
						cell.setCellValue(value); 
					}
					if(cellnum == ONE && rownum < THREE){
						cell.setCellStyle(style1);
					}else if(cellnum != ONE && rownum < THREE){
						cell.setCellStyle(style3);
					}else if(rownum == THREE){
						cell.setCellStyle(style2);
					}else{
						cell.setCellStyle(style4);
					}
				}
			//}
			
		}
		return workbook;
	}
	
	
	public static HSSFWorkbook createFormularyDrugExcelSheet(List<String[]> drugList,
			List<String[]> formularyPlanList) {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet drugSheet = workbook.createSheet("Drug Data");
		drugSheet.setColumnWidth(ZERO, FIFTEEN * TWO_FIFTY_SIX);
		drugSheet.setColumnWidth(ONE, FIFTEEN * TWO_FIFTY_SIX);
		drugSheet.setColumnWidth(TWO, TWENTY_FIVE * TWO_FIFTY_SIX);
		drugSheet.setColumnWidth(THREE, TWENTY_FIVE * TWO_FIFTY_SIX);
		drugSheet.setColumnWidth(FOUR, FIFTEEN * TWO_FIFTY_SIX);
		drugSheet.setColumnWidth(FIVE, FIFTEEN * TWO_FIFTY_SIX);
		drugSheet.setColumnWidth(SIX, FIFTEEN * TWO_FIFTY_SIX);
		fillSheetData(drugList, drugSheet, workbook);
		HSSFSheet planSheet = workbook.createSheet("Plans");
		planSheet.setColumnWidth(ZERO, TWENTY_FIVE * TWO_FIFTY_SIX);
		planSheet.setColumnWidth(ONE,  FIFTY * TWO_FIFTY_SIX);
		planSheet.setColumnWidth(TWO, FIFTEEN * TWO_FIFTY_SIX);
		fillSheetData(formularyPlanList, planSheet, workbook);
		return workbook;
	}

	private static void fillSheetData(List<String[]> dataList, HSSFSheet sheet, HSSFWorkbook workbook) {

		//Set bold font style
		HSSFFont font = workbook.createFont();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		HSSFCellStyle styleNormalBold = workbook.createCellStyle();
		styleNormalBold.setFont(font);
		styleNormalBold.setBorderRight(CellStyle.BORDER_THIN);
		styleNormalBold.setBorderTop(CellStyle.BORDER_THIN);
		styleNormalBold.setBorderBottom(CellStyle.BORDER_THIN);
		styleNormalBold.setBorderLeft(CellStyle.BORDER_THIN);
		styleNormalBold.setAlignment(CellStyle.ALIGN_LEFT);
		
		HSSFCellStyle styleYellowBG = workbook.createCellStyle();
		styleYellowBG.setFont(font);
		styleYellowBG.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
		styleYellowBG.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styleYellowBG.setBorderRight(CellStyle.BORDER_THIN);
		styleYellowBG.setBorderTop(CellStyle.BORDER_THIN);
		styleYellowBG.setBorderBottom(CellStyle.BORDER_THIN);
		styleYellowBG.setBorderLeft(CellStyle.BORDER_THIN);
		styleYellowBG.setAlignment(CellStyle.ALIGN_LEFT);
		
		HSSFCellStyle style2 = workbook.createCellStyle();
		style2.setFont(font);
		style2.setFillForegroundColor(HSSFColor.ROYAL_BLUE.index);
		style2.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style2.setBorderRight(CellStyle.BORDER_THIN);
		style2.setBorderBottom(CellStyle.BORDER_THIN);
		style2.setBorderLeft(CellStyle.BORDER_THIN);
		style2.setAlignment(CellStyle.ALIGN_CENTER);
		
		HSSFCellStyle stylePaleBlueBG = workbook.createCellStyle();
		stylePaleBlueBG.setFont(font);
		stylePaleBlueBG.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
		stylePaleBlueBG.setFillPattern(CellStyle.SOLID_FOREGROUND);
		stylePaleBlueBG.setBorderRight(CellStyle.BORDER_THIN);
		stylePaleBlueBG.setBorderBottom(CellStyle.BORDER_THIN);
		stylePaleBlueBG.setBorderLeft(CellStyle.BORDER_THIN);
		stylePaleBlueBG.setAlignment(CellStyle.ALIGN_LEFT);
		
		HSSFCellStyle styleNormal = workbook.createCellStyle();
		styleNormal.setAlignment(CellStyle.ALIGN_LEFT);
		
		Row row = null;
		Cell cell = null;
		int rownum = ZERO;
		int cellnum = ZERO;
		
		for (String []details : dataList) {
			row = sheet.createRow(rownum++);
			if(rownum <= FOUR){
				Cell cellA = row.createCell(ZERO);
				cellA.setCellValue(details[ZERO]);
				cellA.setCellStyle(styleYellowBG);
				
				Cell cellB = row.createCell(ONE);
				cellB.setCellValue(details[ONE]);
				cellB.setCellStyle(stylePaleBlueBG);
				
				if(sheet.getColumnWidth(ONE ) < TWENTY_FIVE * TWO_FIFTY_SIX) { 
					Cell cellC = row.createCell(TWO);
					cellC.setCellStyle(stylePaleBlueBG);
					sheet.addMergedRegion(new CellRangeAddress(rownum-1, rownum-1, ONE, TWO));
				}
			}else{
				cellnum = ZERO;
				for(String value : details){
					cell = row.createCell(cellnum++);
					cell.setCellValue(value); 
					
					if(rownum == SIX){
						cell.setCellStyle(styleNormalBold);
					}else {
						cell.setCellStyle(styleNormal);
					}
					cell = null;
				}
			}
		}
		row = null;		
	}
}
