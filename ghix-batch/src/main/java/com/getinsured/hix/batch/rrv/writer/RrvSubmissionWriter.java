package com.getinsured.hix.batch.rrv.writer;

import com.getinsured.eligibility.model.RrvSubmission;
import com.getinsured.eligibility.rrv.service.RrvService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class RrvSubmissionWriter implements ItemWriter<Long> {

    private static final Logger logger = LoggerFactory.getLogger(RrvSubmissionWriter.class);

    private long coverageYear;


    @Autowired
    RrvService rrvService;

    private long batchId;

    @BeforeStep
    public void initial() throws Exception{

        this.batchId = rrvService.getBatchId();

        logger.info("Batch ID is: {}", batchId);

    }

    @Override
    public void write(List<? extends Long> list) throws Exception {
        logger.info("Writing to RRV Submission.");
        logger.info("List size: " + list.size());

        List<RrvSubmission> rrvSubmissionList = rrvService.rrvBatchProcess(list, batchId, coverageYear);

        if(logger.isInfoEnabled()) {
            logger.info("RRV Submission List Returned Size: {}", rrvSubmissionList.size());
        }

        rrvService.saveRrvSubmissions(rrvSubmissionList);

    }

    public RrvService getRrvService() {
        return rrvService;
    }

    public void setRrvService(RrvService rrvService) {
        this.rrvService = rrvService;
    }

    public long getCoverageYear() {
        return coverageYear;
    }

    public void setCoverageYear(long coverageYear) {
        this.coverageYear = coverageYear;
    }
}