/**
 * 
 */
package com.getinsured.hix.broker.util;


import org.springframework.stereotype.Component;

/**
 * Used for defining response codes and loading response description from
 * properties file Loads the key values from properties file :
 * broker_response_code.properties and store in corresponding static fields
 */
@Component
public final class BrokerConstants {

	private BrokerConstants() {
	}

	public static final String DATE_FORMAT = "MM/dd/yyyy";
	public static final String EMAIL_PATTERN = "((([a-zA-Z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-zA-Z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-zA-Z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-zA-Z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-zA-Z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-zA-Z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-zA-Z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-zA-Z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-zA-Z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-zA-Z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))$";

	public static final int BROKERDOESNOTEXIST = 202;
	public static final int RESPONSECODESUCCESS = 200;
	public static final int RESPONSECODEINVALIDINDIVIDUALID = 400;
	public static final int RESPONSECODEINVALIDEMPLOYERID = 401;
	public static final int RESPONSECODEINVALIDMISSINGBROKERID = 402;
	public static final int RESPONSECODEBOTHEMPLOYERINDIVIDUALIDMISSING = 429;
	public static final int RESPONSECODEFAILURE = 404;
	public static final int RESPONSECODEBOTHEMPLOYERINDIVIDUALIDPRESENT = 430;
	public static final int RESPONSECODEINVALIDINPUT = 201;
	public static final int EXCEPTIONFAULTCODE = 205;
	public static final int RESPONSECODEINVALIDLICENSENO = 406;
	public static final int RESPONSECODEMISSINGFIRSTNAME = 407;
	public static final int RESPONSECODEMISSINGLASTNAME = 408;
	public static final int RESPONSECODEINVALIDRECORDTYPE = 432;
	public static final int RESPONSECODERECORDTYPEMISSING = 433;
	public static final int RESPONSECODELICENSENOEXIST = 411;
	public static final int RESPONSECODEBUSINESSLEGALNAMEMISSING = 412;
	public static final int RESPONSECODEINVALIDFEDERALEIN = 413;
	public static final int RESPONSECODEINVALIDSTATEEIN = 414;
	public static final int RESPONSECODEINVALIDEMAILADDRESS = 415;
	public static final int RESPONSECODESTATEEINEXIST = 416;
	public static final int RESPONSECODEBOTHBROKERANDASSISTERIDMISSING = 417;
	public static final int RESPONSECODEINVALIDASSISTERID = 420;
	public static final int RESPONSECODEBOTHAGENTANDASSISTERIDPRESENT = 421;
	public static final int RESPONSECODENOASSISTERDESIGNATIONRECORD = 422;
	public static final int RESPONSECODENOAGENTDESIGNATIONRECORDFOREMPLOYER = 423;
	public static final int RESPONSECODENOAGENTDESIGNATIONRECORDFORINDIVIDUAL = 424;
	public static final int RESPONSECODEMISSINGINDIVIDUALID = 425;
	public static final int RESPONSECODEMISSINGEMPLOYERID = 426;
	public static final int RESPONSECODEAGENTALREADYDEDESIGNATEDBYEMPLOYER = 427;
	public static final int RESPONSECODEAGENTALREADYDEDESIGNATEDBYINDIVIDUAL = 428;
	public static final int RESPONSECODEASSISTERALREADYDEDESIGNATEDBYINDIVIDUAL = 426;
	public static final int RESPONSECODEEMPLOYERIDCANNOTBEPRESENTFORASSISTER = 425;
	public static final int RESPONSECODEINVALIDRECORDID = 432;
	//public static final int responseCodeAgentAlreadyDesignatedByEmployer = 433;
	//public static final int responseCodeAgentAlreadyDesignatedByIndividual = 434;
	public static final int RESPONSECODEINVALIDDESIGNATIONINDICATOR = 431;
	//public static final int responseCodeAssisterAlreadyDesignatedByIndividual = 436;

	//IND48 failure response code
	public static final int RESPONSECODERECORDIDMISSING = 401;
	public static final int RESPONSECODERECORDIDINCORRECT = 402;
	public static final int RESPONSECODEEXCEEDSMAXLENGTH = 403;
	public static final int RESPONSECODEEMPLOYERIDINCORRECT = 404;
	public static final int RESPONSECODEINDIVIDUALIDINCORRECT = 405;
	public static final int RESPONSECODEUNKNOWNERROR = 406;
	
	public static final int RESPONSECODEAGENTIDDOESNOTEXIST = 407;
	public static final int RESPONSECODEEMPLOYERIDDOESNOTEXIST = 408;
	public static final int RESPONSECODEINDIVIDUALIDDOESNOTEXIST = 409;
	public static final int RESPONSECODEASSISTERIDDOESNOTEXIST = 410;
	public static final int RESPONSECODEAGENTALREADYDESIGNATEDBYEMPLOYER = 411;
	public static final int RESPONSECODEAGENTALREADYDESIGNATEDBYINDIVIDUAL = 412;
	public static final int RESPONSECODEASSISTERALREADYDESIGNATEDBYINDIVIDUAL = 413;
	public static final int RESPONSECODEAGENTEMPLOYERDESIGNATIONFAILED = 414;
	public static final int RESPONSECODEAGENTINDIVIDUALDESIGNATIONFAILED = 415;
	public static final int RESPONSECODEASSISTERINDIVIDUALDESIGNATIONFAILED = 416;
	public static final int RESPONSECODEIND50FAILED = 417;
	public static final int RESPONSECODEIND52FAILED = 418;
	public static final int RESPONSECODEAGENTEMPLOYERDEDESIGNATIONFAILED = 419;
	public static final int RESPONSECODEAGENTINDIVIDUALDEDESIGNATIONFAILED = 420;
	public static final int RESPONSECODEASSISTERINDIVIDUALDEDESIGNATIONFAILED = 421;
	public static final int RESPONSECODEAGENCYRECORDCREATIONFAILED = 430;
	public static final int RESPONSECODEMISSINGADDRESS1 = 431;
	public static final int RESPONSECODEMISSINGCITY = 432;
	public static final int RESPONSECODEMISSINGSTATE = 433;
	public static final int RESPONSECODEMISSINGZIPCODE = 434;
	
	
	
	public static final int MAXIDLENGTH = 10;
	
	public static final String BROKERRECORDTYPE = "Agent";
	public static final String ENROLLMENTENTITYRECORDTYPE = "AssisterEnrollmentEntity";
	public static final String AGENCY_MANAGER = "Agency Manager";
	public static final String ASSISTERRECORDTYPE = "Assister";
	public static final String DESIGNATION = "Designate";
	public static final String DEDESIGNATION = "De-Desginate";

	public static final String RESPONSEDESCSUCCESS = "Success";
	public static final String RESPONSEDESCINVALIDEMPLOYERID = "Invalid Employer Id";
	public static final String RESPONSEDESCINVALIDMISSINGBROKERID = "Invalid or Missing Broker Id";
	public static final String RESPONSEDESCINVALIDINDIVIDUALID = "Invalid Individual Id";
	public static final String RESPONSEDESCFAILURE = "Failure";
	public static final String RESPONSEDESCBOTHEMPLOYERINDIVIDUALIDMISSING = "Both 'Employer Id' and 'Individual Id' are Missing";
	public static final String RESPONSEDESCBOTHEMPLOYERINDIVIDUALIDPRESENT = "Both 'Employer Id' and 'Individual Id' cannot be present";
	public static final String RESPONSEDESCINVALIDINPUT = "Invalid Input";
	public static final String RESPONSEDESCINVALIDLICENSENO = "Invalid License No";
	public static final String RESPONSEDESCMISSINGFIRSTNAME = "First Name is missing";
	public static final String RESPONSEDESCMISSINGLASTNAME = "Last Name is missing";
	public static final String RESPONSEDESCINVALIDRECORDTYPE = "Invalid Record Type";
	public static final String RESPONSEDESCRECORDIDGENERATIONERROR = "Failed to generate Record Id";
	public static final String RESPONSEDESCAGENCYGENERATIONERROR = "Failed to generate Agency record";
	public static final String RESPONSEDESCRECORDTYPEMISSING = "Record Type is missing";
	public static final String RESPONSEDESCLICENSENOEXIST = "License Number already exists";
	public static final String RESPONSEDESCBUSINESSLEGALNAMEMISSING = "Business Legal Name is missing";
	public static final String RESPONSEDESCINVALIDFEDERALEIN = "Invalid Federal EIN";
	public static final String RESPONSEDESCINVALIDSTATEEIN = "Invalid State EIN";
	public static final String RESPONSEDESCINVALIDEMAILADDRESS = "Invalid Email Address";
	public static final String RESPONSEDESCSTATEEINEXIST = "State EIN already exists";
	public static final String RESPONSEDESCBOTHBROKERANDASSISTERIDMISSING = "Both 'Agent Id' and 'Enrollment Counselor Id' are missing";
	public static final String RESPONSEDESCINVALIDASSISTERID = "Invalid or Missing Enrollment Counselor Id";
	public static final String RESPONSEDESCBOTHAGENTANDASSISTERIDPRESENT = "Both 'Agent Id' and 'Enrollment Counselor Id' cannot be present";
	public static final String RESPONSEDESCNOASSISTERDESIGNATIONRECORD = "No Designation Record found for given Enrollment Counselor and Individual";
	public static final String RESPONSEDESCNOAGENTDESIGNATIONRECORDFOREMPLOYER = "No Designation Record found for given Agent and Employer";
	public static final String RESPONSEDESCNOAGENTDESIGNATIONRECORDFORINDIVIDUAL = "No Designation Record found for given Agent and Individual";
	public static final String RESPONSEDESCMISSINGINDIVIDUALID = "Missing Individual Id";
	public static final String RESPONSEDESCMISSINGEMPLOYERID = "Missing Employer Id";
	public static final String RESPONSEDESCAGENTALREADYDEDESIGNATEDBYEMPLOYER = "Agent has already been de-designated by the given Employer";
	public static final String RESPONSEDESCAGENTALREADYDEDESIGNATEDBYINDIVIDUAL = "Agent has already been de-designated by the given Individual";
	public static final String RESPONSEDESCASSISTERALREADYDEDESIGNATEDBYINDIVIDUAL = "Enrollment Counselor has already been de-designated by the given Individual";
	public static final String RESPONSEDESCRECORDIDMISSING = "Record Id is missing";
	public static final String RESPONSEDESCEMPLOYERIDCANNOTBEPRESENTFORASSISTER = "Employer Id cannot be present for Record Type 'Assister'";
	public static final String RESPONSEDESCINVALIDRECORDID = "Invalid Record Id";
	public static final String RESPONSEDESCAGENTALREADYDESIGNATEDBYEMPLOYER = "Agent has already been designated by the given Employer";
	public static final String RESPONSEDESCAGENTALREADYDESIGNATEDBYINDIVIDUAL = "Agent has already been designated by the given Individual";
	public static final String RESPONSEDESCINVALIDDESIGNATIONINDICATOR = "Invalid Designation Indicator";
	public static final String RESPONSEDESCASSISTERALREADYDESIGNATEDBYINDIVIDUAL = "Enrollment Counselor has already been designated by the given Individual";
	public static final String RESPONSE_SUCCESS = "SUCCESS";
	public static final String RESPONSE_FAILURE = "FAILURE";
	public static final String BROKER_ID = "brokerId";
	public static final String RESPONSEDESCAGENTIDDOESNOTEXIST = "Agent Id does not exist";
	public static final String RESPONSEDESCEMPLOYERIDDOESNOTEXIST = "Employer Id does not exist";
	public static final String RESPONSEDESCINDIVIDUALIDDOESNOTEXIST = "Individual Id does not exist";
	public static final String RESPONSEDESCASSISTERIDDOESNOTEXIST = "Assister Id does not exist";
	public static final String RESPONSEDESCAGENTEMPLOYERDESIGNATIONFAILED = "Agent Designation by given Employer failed";
	public static final String RESPONSEDESCAGENTINDIVIDUALDESIGNATIONFAILED = "Agent Designation by given Individual failed";
	public static final String RESPONSEDESCASSISTERINDIVIDUALDESIGNATIONFAILED = "Assister Designation by given Individual failed";
	public static final String RESPONSEDESCIND50FAILED = "IND50 Failed";
	public static final String RESPONSEDESCIND52FAILED = "IND52 Failed";
	public static final String RESPONSEDESCAGENTEMPLOYERDEDESIGNATIONFAILED = "Agent De-Designation by given Employer failed";
	public static final String RESPONSEDESCAGENTINDIVIDUALDEDESIGNATIONFAILED = "Agent De-Designation by given Individual failed";
	public static final String RESPONSEDESCASSISTERINDIVIDUALDEDESIGNATIONFAILED = "Assister De-Designation by given Individual failed";
	public static final String RESPONSEDESCUNKNOWNERROR = "Unknown Error";
	public static final String RESPONSEDESCEXCEEDSMAXLENGTH = "Length of Id cannot be more than 10";
	public static final String RESPONSEDESCMISSINGADDRESS1 = "Address1 is missing";
	public static final String RESPONSEDESCMISSINGCITY = "City is missing";
	public static final String RESPONSEDESCMISSINGSTATE = "State is missing";
	public static final String RESPONSEDESCMISSINGZIPCODE = "Zipcode is missing";

	public static final String ADDACTIONFLAG = "Add";
	public static final String REMOVEACTIONFLAG = "Remove";
	public static final String ENROLLMENT_TYPE_INDIVIDUAL = "FI";
	public static final String AGENT_ROLE = "AGENT";
    public static final String ASSISTER_ROLE = "ASSISTER";
    
    public static final String AGENCY_DOC_PATH = "Agency Documents";
    public static final String SLASH = "/";
    public static final String FOLDER_NAME = "ADD_SUPPORT_FOLDER";
    public static final String CA_STATE_CODE = "CA";
	public static final String ID_STATE_CODE = "ID";
	public static final String NV_STATE_CODE = "NV";
	
	public static final String ENDPOINT_FUNCTION = "BULK_TRANSFER";
	
	public static final String RECORD_TYPE_BROKER = "BROKER";
	public static final String RECORD_TYPE_STAFF = "STAFF";
}
