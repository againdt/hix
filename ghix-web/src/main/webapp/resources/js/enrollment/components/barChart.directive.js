(function() {
    'use strict';
    angular
        .module('reconciliationWorkbenchApp')
        .directive('d3BarChart', d3BarChart);

    function d3BarChart() {

        return {
            restrict: 'E',
            scope: {
                val: '=',
                grouped: '=',
                data: '='
            },
            link: function (scope, element, attrs) {

                var categorical = [
                    // { "name" : "schemeAccent", "n": 8},
                    // { "name" : "schemeDark2", "n": 8},
                    // { "name" : "schemePastel2", "n": 8},
                    // { "name" : "schemeSet2", "n": 8},
                    // { "name" : "schemeSet1", "n": 9},
                    // { "name" : "schemePastel1", "n": 9},
                    { "name" : "schemeCategory10", "n" : 10},
                    { "name" : "schemeSet3", "n" : 12 },
                    { "name" : "schemePaired", "n": 12},
                    { "name" : "schemeCategory20", "n" : 20 },
                    { "name" : "schemeCategory20b", "n" : 20},
                    { "name" : "schemeCategory20c", "n" : 20 }
                ];

                if(!scope.data)
                    return;

                var data = scope.data;

                // var colorScale = d3.scaleOrdinal(d3[categorical[0].name]);

                // set the dimensions and margins of the graph
                var margin = {top: 20, right: 20, bottom: 120, left: 40},
                    width = 200 - margin.left - margin.right,
                    height = 250 - margin.top - margin.bottom;

                var x = d3.scaleBand()
                    .range([0, width])
                    .padding(0.1);
                var y = d3.scaleLinear()
                    .range([height, 0]);

                var colorScale = d3.scaleOrdinal(d3.schemePaired);

                var svg = d3.select(element[0]).append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform",
                        "translate(" + margin.left + "," + margin.top + ")");

                x.domain(data.map(function(d) { return d.discrepancyTypeLabel; }));
                y.domain([0, d3.max(data, function(d) { return d.discrepancyCount; })]);

                svg.selectAll(".bar")
                    .data(data)
                    .enter().append("rect")
                    .attr("class", "bar")
                    .attr("x", function(d) { return x(d.discrepancyTypeLabel); })
                    .attr("width", x.bandwidth())
                    .attr("y", function(d) { return y(d.discrepancyCount); })
                    .attr("height", function(d) { return height - y(d.discrepancyCount); })
                    .style("fill", function(d, i ) { return "#1f78b4"; });

                svg.append("g")
                    .attr("transform", "translate(0," + height + ")")
                    .call(d3.axisBottom(x).ticks(10))
                    .selectAll("text")
                    .style("text-anchor", "end")
                    .attr("dx", "-.8em")
                    .attr("dy", ".15em")
                    .attr("transform", "rotate(-60)");

                svg.append("g")
                    .call(d3.axisLeft(y));

            }
        }
    }

//.style({fill: randomColor});
})();