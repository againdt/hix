package com.getinsured.hix.referral.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.getinsured.eligibility.referral.ui.dto.ReferralVerification;
import com.getinsured.eligibility.referral.ui.dto.VerificationQuestion;

@Component("securityQuestionsValidator")
@Scope("singleton")
public class SecurityQuestionsValidator implements Validator {

	private String DOB = "dob";

	private String LAST_NAME = "lastName";

	private String FIRST_NAME = "firstName";

	private java.text.SimpleDateFormat DATE_FORMATTER = new java.text.SimpleDateFormat("MM/dd/yyyy");

	@Override
	public boolean supports(Class<?> arg0) {
		return ReferralVerification.class.equals(arg0);
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		ReferralVerification referralVerification = (ReferralVerification) arg0;

		for (VerificationQuestion verificationQuestion : referralVerification.getVerificationQuestions()) {
			if (verificationQuestion.getCode().equals(FIRST_NAME) && StringUtils.isEmpty(verificationQuestion.getAnswer())) {
				arg1.rejectValue("verificationQuestions[0].answer", "label.referral.validateFirstName");
			} else if (verificationQuestion.getCode().equals(LAST_NAME) && StringUtils.isEmpty(verificationQuestion.getAnswer())) {
				arg1.rejectValue("verificationQuestions[1].answer", "label.referral.validateLastName");
			} else if (verificationQuestion.getCode().equals(DOB)) {
				if (StringUtils.isEmpty(verificationQuestion.getAnswer())) {
					arg1.rejectValue("verificationQuestions[2].answer", "label.referral.validateDateOfBirth");
					break;
				}
				try {
					DATE_FORMATTER.parse(verificationQuestion.getAnswer());
				} catch (java.text.ParseException e) {
					arg1.rejectValue("verificationQuestions[2].answer", "label.referral.validateDateOfBirthFormat");
				}
			}
		}
	}

}
