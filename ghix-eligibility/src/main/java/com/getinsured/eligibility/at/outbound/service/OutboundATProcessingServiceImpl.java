package com.getinsured.eligibility.at.outbound.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.dto.EligEngineHouseholdCompositionRequest;
import com.getinsured.eligibility.at.dto.EligEngineHouseholdCompositionResponse;
import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareApplicationDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.ref.service.ReferralLCECompareService;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.eligibility.repository.outboundat.OutboundATApplicantRepository;
import com.getinsured.eligibility.repository.outboundat.OutboundATApplicationRepository;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.ssap.model.OutboundATApplicant;
import com.getinsured.iex.ssap.model.OutboundATApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.util.TSDate;

@Service
public class OutboundATProcessingServiceImpl implements OutboundATProcessingService {
	
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private SsapApplicantRepository ssapApplicantRepository;
	@Autowired private GIWSPayloadRepository giWsPayloadRepository;
	@Autowired private OutboundATApplicationRepository outboundATApplicationRepository;
	@Autowired private OutboundATApplicantRepository outboundATApplicantRepository;
	@Autowired private ReferralLCECompareService referralLCECompareService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	
	private static final String endpointFunction =  "ACCOUNT-TRANSFER-OUTBOUND";

	private static Logger lOGGER = Logger.getLogger(OutboundATProcessingService.class);
	
	@Value("#{configProp['hsd.AccountTransferURL']}")
	private String endpoint;
	
	@Override
	public void logOutboundAccountTransfer(Long applicationId,Map<String,Object> outboundAtDetailsMap, Integer giWsPayloadId, String status) {
		
	    OutboundATApplication outboundAtAdpplication = populateOutboundATApplication(applicationId, outboundAtDetailsMap.get("caseNumber").toString(), giWsPayloadId, status);
	    List<OutboundATApplicant> applicantsList = populateOutboundAtApplicants(applicationId, (List<SsapApplicant>)outboundAtDetailsMap.get("applicants"));
	    performOutboundAtDBLogging(outboundAtAdpplication,applicantsList);
	}
	
	@Override
	public Map<String,Object> prepareOutboundAtDBLoggingDetails(Long applicationId) {
		
		Map<String,Object> outboundAtDetailsMap = new HashMap<String, Object>();
		
		String caseNumber =  ssapApplicationRepository.findCaseNumberById(applicationId);
		outboundAtDetailsMap.put("caseNumber",caseNumber);
		
		List<SsapApplicant> applicants = ssapApplicantRepository.findBySsapApplicationId(applicationId);
		if(applicants!=null) {
			outboundAtDetailsMap.put("applicants", ssapApplicantRepository.findBySsapApplicationId(applicationId));
		}
		
		return outboundAtDetailsMap;
	}
	
	@Override
	public GIWSPayload populateGiWSPayload(Long ssapAppId,Map<String,Object> outboundAtDetailsMap) {
		
		GIWSPayload giwsPayload = new GIWSPayload();
		giwsPayload.setSsapApplicationId(ssapAppId);
		giwsPayload.setCorrelationId(null);
		giwsPayload.setCreatedTimestamp(new TSDate());
		giwsPayload.setCreatedUserId(null);
		giwsPayload.setEndpointFunction(endpointFunction);
		giwsPayload.setEndpointOperationName(null);
		giwsPayload.setEndpointUrl(endpoint);
		giwsPayload.setRequestPayload(outboundAtDetailsMap.get("request").toString());
	
		return giwsPayload;
	}
	
	@Override
	public OutboundATApplication populateOutboundATApplication(Long applicationId,String caseNumber,Integer giWsPayloadId, String status) {
		
		OutboundATApplication application = new OutboundATApplication();
		application.setCaseNumber(caseNumber);
		application.setDateSent(new Date());
		application.setOutboundATPayload(giWsPayloadId);
		application.setSsapApplicationId(applicationId);
		application.setStatus(status);
		
		return application;
	}
	
	@Override
	public List<OutboundATApplicant> populateOutboundAtApplicants(Long applicationId,List<SsapApplicant> applicants){
		
		List<OutboundATApplicant> outboundApplicants = new ArrayList<OutboundATApplicant>();
		
		applicants.forEach( a -> {
			OutboundATApplicant applicant = new OutboundATApplicant();
			applicant.setSsapApplicationId(applicationId);
			applicant.setApplicantGuid(a.getApplicantGuid());
			
			outboundApplicants.add(applicant);
		});
		
		return outboundApplicants;
		
	}
	
	@Override
	public void performOutboundAtDBLogging(OutboundATApplication application,List<OutboundATApplicant> applicants) {
		
		outboundATApplicationRepository.save(application);
		outboundATApplicantRepository.save(applicants);
		
	}

	@Override
	public boolean compareNewAndEnrolledApplication(Long currentApplicationId) {
		if(this.checkOutboundATApplicantStatus(currentApplicationId)){
			return true;
		}
		return false;
	}
	
	private boolean checkIncomeChanged(CompareMainDTO compareMainDTO) {
		EligEngineHouseholdCompositionResponse currentAppEligEngineHouseholdComposition = this.getHouseholdComposition(compareMainDTO.getCurrentApplication().getId());
		EligEngineHouseholdCompositionResponse enrolledAppEligEngineHouseholdComposition = this.getHouseholdComposition(compareMainDTO.getEnrolledApplication().getId());
		if(currentAppEligEngineHouseholdComposition != null && enrolledAppEligEngineHouseholdComposition != null){
			if(currentAppEligEngineHouseholdComposition.getHouseholdIncome() != enrolledAppEligEngineHouseholdComposition.getHouseholdIncome()){
				return true;
			}
		}
		return false;
	}
	
	
	private EligEngineHouseholdCompositionResponse getHouseholdComposition(Long ssapApplicationId){ 
	try { 
			EligEngineHouseholdCompositionRequest eligEngineHouseholdCompositionRequest = new EligEngineHouseholdCompositionRequest(); 
		    eligEngineHouseholdCompositionRequest.setApplicationId(ssapApplicationId); 
		    eligEngineHouseholdCompositionRequest.setRequestId(String.valueOf(2));
	
		    EligEngineHouseholdCompositionResponse eligEngineHouseholdCompositionResponse = null; 
			eligEngineHouseholdCompositionResponse = ghixRestTemplate.postForObject(GhixEndPoints.EligibilityEndPoints.HOUSEHOLD_COMPOSITION, eligEngineHouseholdCompositionRequest, EligEngineHouseholdCompositionResponse.class); 
			if(eligEngineHouseholdCompositionResponse.getStatus().equals("sucess")){ 
				return eligEngineHouseholdCompositionResponse; 
			}else{ 
				lOGGER.error("Response not ok from eligibility engine " + eligEngineHouseholdCompositionResponse.getStatus()); 
			} 
		} catch (Exception e) { 
			lOGGER.error(e.getStackTrace()); 
		} 
		return null; 
	}

	private boolean checkHHAddedOrRemoved(CompareMainDTO compareMainDTO) {
		List<ApplicantStatusEnum> applicantStatusEnumList = new ArrayList<ApplicantStatusEnum>();
		applicantStatusEnumList.add(ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE);
		applicantStatusEnumList.add(ApplicantStatusEnum.ADD_NEW_ELIGIBILE);
		applicantStatusEnumList.add(ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE);
		applicantStatusEnumList.add(ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE);
		try {
			CompareApplicationDTO currentAppDTO = compareMainDTO.getCurrentApplication();
			List<CompareApplicantDTO> currentApplicantsDTO = currentAppDTO.getApplicants();
			for (CompareApplicantDTO compareApplicantDTO : currentApplicantsDTO) {
				ApplicantStatusEnum applicantStatusEnum = ApplicantStatusEnum.fromValue(compareApplicantDTO.getStatus());
				if (applicantStatusEnumList.contains(applicantStatusEnum)) {
					return true;
				}
			}
		} catch (Exception e) {
			lOGGER.info("Error while comparing HH API");
			lOGGER.error(e.getStackTrace());
		} 
		return false;
	}
	
	private boolean checkOutboundATApplicantStatus(Long currentApplicationId){
		try {
			List<String> eligibilityTypes = new ArrayList<String>();
			eligibilityTypes.add("AssessedMedicaidMAGIEligibilityType");
			eligibilityTypes.add("AssessedMedicaidNonMAGIEligibilityType");
			List<SsapApplicant> ssapApplicants = ssapApplicantRepository.getEligibilitiesForApplicantsByAppId(currentApplicationId, eligibilityTypes);
			
			if(ssapApplicants.size() > 0){
				int rejectedCount = 0;
				List<String> applicantGuids = new ArrayList<String>();
				for (SsapApplicant ssapApplicant : ssapApplicants) {
					applicantGuids.add(ssapApplicant.getApplicantGuid());
				}
				List<OutboundATApplicant> outboundATApplicants = outboundATApplicantRepository.findByApplicantGuidAndApplicationId(applicantGuids, currentApplicationId);
				if(outboundATApplicants.size() == 0){
					return true;
				}else{
					for (OutboundATApplicant outboundATApplicant : outboundATApplicants) {
						if("Rejected".equalsIgnoreCase(outboundATApplicant.getAction())){
							Date eligibilityDeterminationDate = outboundATApplicant.getEligibilityDeterminationDate();//eligibilityDeterminationDate.getTime(); --2222295 8673875
								if(eligibilityDeterminationDate != null){
									
									Date today = new Date();
									Calendar cal = new GregorianCalendar();
									cal.setTime(today);
									cal.add(Calendar.DAY_OF_MONTH, -90);
									Date today90 = cal.getTime(); 
									if(eligibilityDeterminationDate.before(today90)){
										return true;
									}
								}else{
									return true;
								}
								rejectedCount++;
						 }
					}
					if(rejectedCount == outboundATApplicants.size()){
						return false;
					}else{
						return true;
					}
				}
			}
		} catch (Exception e) {
			lOGGER.info("Error while comparing HH API");
			lOGGER.error(e.getStackTrace());
		} 
		return false;
	}
	
}
















