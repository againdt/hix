//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.07.16 at 11:43:39 PM PDT 
//


package com.getinsured.iex.org.nmhix.ssa.person;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AnswerChoices complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AnswerChoices">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="answer" maxOccurs="5">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="answerNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="answerText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnswerChoices", propOrder = {
    "answer"
})
public class AnswerChoices {

    @XmlElement(required = true)
    protected List<AnswerChoices.Answer> answer;

    /**
     * Gets the value of the answer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the answer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnswer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AnswerChoices.Answer }
     * 
     * 
     */
    public List<AnswerChoices.Answer> getAnswer() {
        if (answer == null) {
            answer = new ArrayList<AnswerChoices.Answer>();
        }
        return this.answer;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="answerNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="answerText" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "answerNumber",
        "answerText"
    })
    public static class Answer {

        @XmlElement(required = true)
        protected String answerNumber;
        @XmlElement(required = true)
        protected String answerText;

        /**
         * Gets the value of the answerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAnswerNumber() {
            return answerNumber;
        }

        /**
         * Sets the value of the answerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAnswerNumber(String value) {
            this.answerNumber = value;
        }

        /**
         * Gets the value of the answerText property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAnswerText() {
            return answerText;
        }

        /**
         * Sets the value of the answerText property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAnswerText(String value) {
            this.answerText = value;
        }

    }

}
