package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.I94DocumentID2Type;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;

/**
 * Wrapper Class for I94DocumentID2Type
 * 
 * @author Abhai Chaudhary, Nikhil Talreja
 * @since  20-Jan-2014 
 * 
 */
public class I94DocumentID2Wrapper implements JSONAware {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(I94DocumentID2Wrapper.class);
	
	private ObjectFactory factory;
	private I94DocumentID2Type i94DocumentID2Type;
	
	private String i94Number;
	private String sevisid;
	private String expiryDate;

	private static final String I94_NUMBER_KEY = "I94Number";
	private static final String SEVIS_ID_KEY = "SEVISID";
	private static final String EXPIRY_DATE_KEY = "DocExpirationDate";
	

	public I94DocumentID2Wrapper(){
		this.factory = new ObjectFactory();
		this.i94DocumentID2Type = factory.createI94DocumentID2Type();
	}
	
	public void setI94Number(String i94Number) throws HubServiceException {
		
		if(i94Number == null){
			throw new HubServiceException("No i94 number provided");
		}
		
		this.i94Number = i94Number;
		this.i94DocumentID2Type.setI94Number(this.i94Number);
	}
	
	public String getI94Number() {
		return i94Number;
	}

	public void setSevisid(String sevisid) throws HubServiceException{

		if(sevisid == null){
			throw new HubServiceException("No SEVIS ID provided");
		}
		
		this.sevisid = sevisid;
		this.i94DocumentID2Type.setSEVISID(this.sevisid);
		
	}
	
	public String getSevisid() {
		return sevisid;
	}

	public void setExpiryDate(String expiryDate) {
		
		try{
			this.i94DocumentID2Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			this.expiryDate = expiryDate;
		}catch(Exception e){
			LOGGER.error("Exception occured while setting I94 Document Expiration date",e);
		}
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}

	
	public I94DocumentID2Type geti94DocumentID2Type(){
		return this.i94DocumentID2Type;
	}
	
	/**
	 * Converts the JSON String to I94DocumentID2Wrapper object
	 * 
	 * @param jsonString - String representation for I94DocumentID2Wrapper
	 * @return I94DocumentID2Wrapper Object
	 */
	public static I94DocumentID2Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I94DocumentID2Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		I94DocumentID2Wrapper wrapper = new I94DocumentID2Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(I94_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.i94Number = (String)tmpObj;
			wrapper.i94DocumentID2Type.setI94Number(wrapper.i94Number);
		}
		
		tmpObj = obj.get(SEVIS_ID_KEY);
		if(tmpObj != null){
			wrapper.sevisid = (String)tmpObj;
			wrapper.i94DocumentID2Type.setSEVISID(wrapper.sevisid);
		}
		
		tmpObj = obj.get(EXPIRY_DATE_KEY);
		if(tmpObj != null){
			wrapper.expiryDate = (String)tmpObj;
			try {
				wrapper.i94DocumentID2Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating I94DocumentID2Wrapper from JSON, Invalid expiration date",e);
			}
		}

		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(I94_NUMBER_KEY, this.i94Number);
		obj.put(SEVIS_ID_KEY, this.sevisid);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj.toJSONString();
	}
	
	public static I94DocumentID2Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		I94DocumentID2Wrapper wrapper = new I94DocumentID2Wrapper();
		wrapper.setI94Number((String) jsonObj.get(I94_NUMBER_KEY));
		wrapper.setSevisid((String) jsonObj.get(SEVIS_ID_KEY));
		wrapper.setExpiryDate((String) jsonObj.get(EXPIRY_DATE_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(I94_NUMBER_KEY, this.i94Number);
		obj.put(SEVIS_ID_KEY, this.sevisid);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj;
	}
	
}
