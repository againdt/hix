<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/highcharts.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/underscore-min.js" />"></script>

<div class="gutter10">
	<div class="row-fluid">
		<h1>
			<a name="skip"></a>
			<spring:message code="label.assister.dashboard" />
		</h1>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
				<h4>
					<spring:message code="label.assister.quickLinks" />
				</h4>
			</div>

			<ul class="nav nav-list">
				<li><a href="<c:url value='/entity/assister/individuals?desigStatus=Pending'/>">
					<i class="icon-time"></i> <spring:message code="label.assister.pendingRequests" />
				</a></li>
				<li><a href="<c:url value='/entity/assister/viewassisterprofilebyuserid'/>">
					<i class="icon-user"></i> <spring:message code="label.assister.myProfile" />
				</a></li>
				<c:if test="${CA_STATE_CODE}">
		  			<li><a name="IND66_URL_CA" href="<c:url value='${switchAccUrl}&_pageLabel=individualHomePage&ahbxId=&ahbxUserType=1&recordId=${encryptedAssisterId}&recordType=3&newAppInd=Y&lang='/><spring:message  code="label.language.${lang}"/>"><i class="icon-time"></i> <spring:message code="label.assister.addindividual" /></a></li>
				
					<li>
				  		<a name="enrollmentCounselorToolkit" id="enrollmentCounselorToolkit" href="https://hbex.coveredca.com/toolkit/" target="_blank" >
				  		<i class="icon-wrench"></i> <spring:message  code="label.assister.toolkit"/></a>
				  	</li>
				</c:if>	
				
				<c:if test="${!CA_STATE_CODE}">
					<li><a href="<c:url value='/cap/consumer/addconsumer'/>"><i class="icon-user"></i> <spring:message  code="label.brkaddindividual"/> </a></li>
				</c:if>
			</ul>
		    <br />
		    
		     <c:if test="${ID_STATE_CODE || NV_STATE_CODE}">
			    <!-- ACCESS CODE STARTS--->
				<div class="margin20-t" ng-app="accessCodeApp" ng-controller="accessCodeController" ng-cloak>
					<form name="accessCodeForm" action="<c:url value="/referral/verifyaccesscode"/>" method="post" class="noBorder" novalidate>
					<df:csrfToken/>
						<div class="header">
							<h4 for="access-code" id="access-code-dashboard">
			 	 				<spring:message code="label.homePage.accessCode"/>
			 	 			</h4>
		 	 			</div>	 	 			
		 	 			<div class="margin10-t">
			 	 			<input type="text" class="input-small margin10-l" id="access-code" name="accessCode" ng-focus length="10" required ng-model="accessCode" autocomplete="off">
			 	 			<input type="submit" class="btn margin10-l margin10-b" value="<spring:message code="label.homePage.submit"/>" ng-disabled="accessCodeForm.$invalid">
			 	 			<div class="help-inline" id="accessCode_error">
								<label class="error" ng-if="accessCodeForm.accessCode.$dirty && accessCodeForm.accessCode.$error.required && !accessCodeForm.accessCode.$focused">
									<span> <em class="excl">!</em><spring:message code="label.homePage.accessCodeRequired"/></span>
								</label>
								<label class="error" ng-if="accessCodeForm.accessCode.$dirty && accessCodeForm.accessCode.$error.pattern && !accessCodeForm.accessCode.$focused">
									<span> <em class="excl">!</em><spring:message code="label.homePage.accessCodeValidation"/></span>
								</label>
							</div>
		 	 			</div>
					</form>
				</div>
				<!-- ACCESS CODE ENDS--->
			</c:if>
		</div>

		<c:if test="${csrView == true}">
			<h3>
				<strong>Viewing ${broker.user.fullName}</strong> 
				<a href="/hix/admin/broker/viewprofile/<encryptor:enc value="${broker.id}"/>" class="btn btn-primary showcomments"><spring:message code="label.assister.assMyAccount"/></a>
			</h3>
		</c:if>

		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="margin0">
					<spring:message code="label.assister.enrollmentHighlight" />
				</h4>
			</div>
			<br />

			<script type="text/javascript">
				$(document).ready(function() {
					var chart;
					var jsonObj =${json};
					var text = '';
					var subtitle = '';

					if (jsonObj.length < 4) {
						//console.log('NO plan data receive setting to default :-( ');
						jsonObj = [ {
							"planLevel" : "<spring:message  code="label.assister.assPlatinum"/>",
							"enrollmentCount" : 0
						}, {
							"planLevel" : "<spring:message  code="label.assister.assGold"/>",
							"enrollmentCount" : 0
						}, {
							"planLevel" : "<spring:message  code="label.assister.asskSilver"/>",
							"enrollmentCount" : 0
						}, {
							"planLevel" : "<spring:message  code="label.assister.assBronze"/>",
							"enrollmentCount" : 0
						} ];
						text = '<spring:message  code="label.assister.assNoEnrollmentDataPast30Days"/>';
					} else {
						text = '<spring:message  code="label.assister.assYourEmployerEnrollmentsPast30Days"/>';
						subtitle = 'Source: NMHIX';
					}

					var colors = Highcharts.getOptions().colors;

					//console.log('JSON :'+jsonObj);

					var categories = _.pluck(jsonObj,'planLevel');

					//console.log(' categories : '+categories);

					var data = [];

					for ( var i = 0, jsonLength = jsonObj.length; i < jsonLength; i++) {
						var enrollmentCount = jsonObj[i];
						// alternate colors, then repeat it if exceed the highchart options
						var gdata = [];
						for(var j = 0, jsonLength = jsonObj.length; j < jsonLength; j++){
							if(i == j){
								gdata.push(parseInt(enrollmentCount.enrollmentCount));
							}else{
								gdata.push('');
							}
						}
						data.push({
									name : enrollmentCount.planLevel,
									data : gdata,
									color : colors[i % colors.length],
									dataLabels: { enabled: true,},
								});
					}

					//console.log(' data : '+data);

					chart = new Highcharts.Chart({
								chart : {
									renderTo : 'container_assister_hc',
									type : 'column'
								},
								title : {
									text : '<spring:message  code="label.assister.assYourEnrollmentsPast30Days"/>'
								},
								subtitle : {
									text : '<spring:message  code="label.assister.assSourceGetinsured"/>'
								},
								colors : [ '#cf6f1e',
										'#b1c2c5', '#d3a900',
										'#89b6cd' ],
								xAxis : {
									categories : categories
								},
								yAxis : {
									min : 0,
									title : {
										text : '<spring:message  code="label.assister.assEnrollments"/>'
									}
								},
								legend : {
									layout : 'vertical',
									backgroundColor : '#FFFFFF',
									align : 'left',
									verticalAlign : 'top',
									x : 100,
									y : 70,
									floating : true,
									shadow : true,

								},
								tooltip : {
									formatter : function() {
										return '' + this.x
												+ ': ' + this.y
												+ ' ';
									}
								},
								credits : {
									enabled : false,
								},
								plotOptions : {
									column : {
										pointPadding : .02,
										borderWidth : 0
									}
								},
								series : data
							});
				});
			</script>
			
			<div id="container_assister_hc" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
		</div>
		<!-- End of span6 -->
		
	</div>
	<!-- End of Row Fluid -->

</div>

<script>

var accessCodeApp = angular.module('accessCodeApp',[]);

accessCodeApp.controller('accessCodeController', function($scope) {	
	
	
});

accessCodeApp.directive('ngFocus', [function() {
	var FOCUS_CLASS = "ng-focused";
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attrs, ctrl) {
			ctrl.$focused = false;
			element.bind('focus', function(evt) {
				element.addClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = true;
				});
			}).bind('blur', function(evt) {
				element.removeClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = false;
				});
			});
		}
	};
}]);
</script>