package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for SEVIS Id
 * 
 * @author Nikhil Talreja
 * @since 31-Jan-2014
 *
 */
public class SevisIdValidator extends Validator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SevisIdValidator.class);
	
	/** 
	 * Validations for SEVIS Id
	 * 1. Should be a number
	 * 2. Should be exactly 10 digits
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String sevisId = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			sevisId = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(sevisId.length() < VLPServiceConstants.SEVIS_ID_MIN_LEN
				|| sevisId.length() > VLPServiceConstants.SEVIS_ID_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.SEVIS_ID_LEN_ERROR_MESSAGE);
		}
		
		try{
			Long.parseLong(sevisId);
		}
		catch(NumberFormatException e){
			throw new InputDataValidationException(name + ": "+VLPServiceConstants.NON_NUMERIC_ERROR_MESSAGE, e);
		}
		
	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
