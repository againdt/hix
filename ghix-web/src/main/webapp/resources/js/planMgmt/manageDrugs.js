var app = angular.module('manageDrugListApp', ['ui.grid', 'ui.bootstrap', 'ui.grid.resizeColumns', 'ui.grid.exporter']);

app.controller('ManageDrugListCtrl', ['$scope', 'uiGridExporterService', 'uiGridExporterConstants', 'ManageDrugListService', 
							function ($scope, uiGridExporterService, uiGridExporterConstants, ManageDrugListService) {

	$scope.invalidRxcui = true;
	$scope.invalidRxcuiData = "";
	$scope.invalidRxcuiDataMsg = null; // Defined in JSP
	$scope.exceedsRxcuiDataMsg = null; // Defined in JSP
	$scope.pageSize = 20;

	$scope.resetFilter = function() {
		$scope.invalidRxcui = true;
		$scope.invalidRxcuiData = "";
		$scope.gridOptions.data.length = 0;
		$scope.gridOptions.totalItems = 0;
	};

	$scope.colDef = [];
	var HYPHEN = "-";
	var UNDERSCORE = "_";

	$scope.exportData = function() {
		var grid = $scope.gridApi.grid;
		var rowTypes = uiGridExporterConstants.ALL;
		var colTypes = uiGridExporterConstants.ALL;

		var dt = new Date();
		grid.options.exporterCsvFilename = "DrugList" + HYPHEN + (dt.getMonth() + 1) + HYPHEN + dt.getDate() + HYPHEN + dt.getFullYear() + UNDERSCORE + dt.getHours() + UNDERSCORE + dt.getMinutes() + ".csv";
		uiGridExporterService.csvExport(grid, rowTypes, colTypes);
	};

	$scope.displayName = null; // Defined in JSP

	var setPropertiesFn = function (elem, key) {

		if (!$scope.displayName) {
			return;
		}
        switch (key) {
			case 'rxcui':
			case 'name':
			case 'strength':
			case 'route':
			case 'fullName':
			case 'genericRxcui':
			case 'status':
				elem.displayName = $scope.displayName[key];
        }
	};

	$scope.getDrugDataByRxcuiList = function() {

		var rxcuiFilterValue = frmManageDrugs.rxcuiFilter.value;
//		debugger;

		ManageDrugListService.getReport(rxcuiFilterValue).success(function(responseDTO) {

			if (responseDTO && responseDTO.drugDataList) {
				$scope.gridOptions.data = responseDTO.drugDataList;

				angular.forEach(responseDTO.drugDataList[0], function(value, key) {

					if (!excludeFields.includes(key)) {
						var elem = null;

						if (key == 'name')  {
							elem = {name:key, width: 250};
						}
						else if (key == 'fullName')  {
							elem = {name:key, width: 350};
						}
						else {
							elem = {name:key, width: 150};	// Default width
						}
						setPropertiesFn(elem, key);
						$scope.colDef.push(elem);
					}
				});
				$scope.gridOptions.totalItems = responseDTO.totalRecordCount;
			}
			else {
				$scope.gridOptions.data.length = 0;
				$scope.gridOptions.totalItems = 0;
			}
		});
	};

	var excludeFields = ['rxTermDosageForm', 'rxNormDosageForm', 'type', 'genericName', 'remappedTo'];

	$scope.gridOptions = {
		enablePaginationControls: false,
		columnDefs:  $scope.colDef,
		enableGridMenu: false,
		enableFiltering: false,
		enableSorting: true,
		minRowsToShow: $scope.pageSize,
		paginationCurrentPage: 1,
		enableSelectAll: false,
		exporterMenuCsv: true,

		onRegisterApi: function(gridApi) {
			$scope.gridApi = gridApi;
		}
	};
}]);

app.directive('rxcuiValidation', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attr, mCtrl) {
			function rxcuiValidation(value) {
				var hasValidValue = false;
				scope.invalidRxcui = false;
				// debugger;

				if (!value) {
					hasValidValue = true;
					mCtrl.$setValidity('rxcuiValidation', hasValidValue);
					return value;
				}
				var rxcuiArray = value.split(",");
				var invalidValues = "";

				if (scope.pageSize < rxcuiArray.length) {
					hasValidValue = false;
					scope.invalidRxcui = true;
					scope.invalidRxcuiData = scope.exceedsRxcuiDataMsg;
				}
				else {

					for (var i = 0; i < rxcuiArray.length; i++) {

						// Validate Positive Integer value
						if (!(Math.floor(rxcuiArray[i]) == rxcuiArray[i] && Math.sign(rxcuiArray[i]) === 1 && $.isNumeric(rxcuiArray[i]))) {
							invalidValues += rxcuiArray[i] + ", ";
						}
					}

					if (invalidValues) {
						hasValidValue = false;
						scope.invalidRxcui = true;
						scope.invalidRxcuiData = scope.invalidRxcuiDataMsg + " " + invalidValues.substring(0, invalidValues.trim().length - 1);
					}
					else {
						hasValidValue = true;
						scope.invalidRxcui = false;
						scope.invalidRxcuiData = "";
					}
				}
				mCtrl.$setValidity('rxcuiValidation', hasValidValue);
				return value;
			}
			mCtrl.$parsers.push(rxcuiValidation);
		}
	};
});

app.service('ManageDrugListService',['$http', function ($http) {

	function getReport(rxcuiFilter) {
//		debugger;

		if (!rxcuiFilter) {
			rxcuiFilter = "";
		}

		return  $http({
			method: 'GET',
			url: '/hix/admin/getDrugDataList?rxcuiFilter=' + rxcuiFilter
		});
	}

	return {
		getReport:getReport
	};
}]);
