
package com.getinsured.iex.ssap.enums;


/**
 * @author raguram_p
 *
 *
 */


public enum RaceEnum {

    AMERICAN_INDIAN_OR_ALASKA_NATIVE("1002-5","American Indian or Alaskan Native"),
    ASIAN_INDIAN("2029-7","Asian Indian"),
    AFRICAN_AMERICAN("2054-5","Black or African American"),
    CHINESE("2034-7","Chinese"),
    FILIPINO("2036-2","Filipino"),
    GUAMANIAN_OR_CHAMORRO("2086-7","Guamanian or Chamorro"),
    JAPANESE("2039-6","Japanese"),
    KOREAN("2040-4","Korean"),
    NATIVE_HAWAIIAN("2079-2","Native Hawaiian"),
    OTHER_ASIAN("2028-9","Other Asian"),
    OTHER_PACIFIC_ISLANDER("2500-7","Other Pacific Islander"),
    SAMOAN("2080-0","Samoan"),
    VIETNAMESE("2047-9","Vietnamese"),
    WHITE("2106-3","White"),
    OTHER("2131-1","Other"),
	CUBAN("2182-4","Cuban"),
    MEXICAN_OR_MEXAMER_OR_CHICANO("2148-5","Mexican, Mexican American or Chicano/a"),
    PUERTO_RICAN("2180-8","Puerto Rican"),
    HMONG("2037-0","Hmong"),
    LAOTIAN("2041-2","Laotian"),
    SALVADORAN("2161-8","Salvadoran"),
    GUATEMALAN("2157-6","Guatemalan");
    
    

    private String code = null;
    private String label=null;
    
    private RaceEnum(String code,String label)
    {
    	this.code = code;
    	this.label=label;
    }

    
    public static String value(String v) {
		if (v != null) {
			final RaceEnum data = readValue(v);
			if (data != null) {
				return data.getCode();
			}
		}
		return null;
	}

	public static RaceEnum fromValue(String v) {
		if (v != null) {
			return readValue(v);
		}
		return null;
	}

	private static RaceEnum readValue(String v) {
		RaceEnum data = null;
		for (RaceEnum c : RaceEnum.class.getEnumConstants()) {
			if (c.getLabel().equalsIgnoreCase(v)) {
				data = c;
				break;
			}
		}
		return data;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getLabel() {
		return label;
	}


	public void setLabel(String label) {
		this.label = label;
	}
	
	

}
