package com.getinsured.eligibility.ssap.integration.household.exception;

public class SSAPIntegrationException extends Exception {
	private String message;
	private Exception baseException;
	private static final long serialVersionUID = 1L;

	public SSAPIntegrationException(String message) {
		super();
		this.message = message;
	}

	public SSAPIntegrationException(String message, Exception baseException) {
		super();
		this.message = message;
		this.baseException = baseException;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Exception getBaseException() {
		return baseException;
	}

	public void setBaseException(Exception baseException) {
		this.baseException = baseException;
	}

}
