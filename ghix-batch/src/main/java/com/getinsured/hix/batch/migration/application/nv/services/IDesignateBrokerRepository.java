package com.getinsured.hix.batch.migration.application.nv.services;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.entity.DesignateAssister;

/**
 * Repository for CRUD operations on {@link DesignateBroker}
 * 
 */
public interface IDesignateBrokerRepository extends JpaRepository<DesignateBroker, Integer> {

	/**
	 * Finds {@link DesignateAssister} based on external individual id and agent
	 * id coming from AHBX.
	 * 
	 * @param agent
	 *            Id
	 * @param externalIndividualId
	 * @return {@link DesignateAssister}
	 */
	@Query("FROM DesignateBroker d where d.brokerId = :brokerId AND d.individualId = :individualId")
	DesignateBroker findDesigBrokerByAgentAndIndividualId(@Param("brokerId") int brokerId,
			@Param("individualId") int individualId);
	
	@Modifying
	@Transactional(readOnly=false)
	@Query("delete from DesignateBroker d where d.individualId=:individualId")
	void deleteDesignatedBrokerByCaseId(@Param("individualId") Integer individualId);
	
	@Query("FROM DesignateBroker d where d.individualId = :individualId")
	List<DesignateBroker> findDesigBrokerByIndividualId(
			@Param("individualId") int individualId);

}