import { normalizeBooleanToString } from '../normalizeBooleanToString';

describe('Test for normalizeBooleanToString', () => {


    it('Should return "true"', () => {
        expect(normalizeBooleanToString(true)).toBe('true');
    });

    it('Should return "true + false + true"', () => {
        expect(normalizeBooleanToString(true, false, true)).toBe('true + false + true');
    });

    it('Should throw an error "Not Boolean"', () => {
        expect(() => {
            normalizeBooleanToString(5).toThrow('You passed to normalizeBooleanToString not a Boolean');
        });
    });

});