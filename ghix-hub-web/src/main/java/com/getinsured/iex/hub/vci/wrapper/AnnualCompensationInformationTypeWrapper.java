package com.getinsured.iex.hub.vci.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.AnnualCompensationInformationType;
import com.getinsured.iex.hub.vci.gov.niem.niem.proxy.xsd._2.GYear;

/**
 * Wrapper class for
 * com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.AnnualCompensationInformationType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class AnnualCompensationInformationTypeWrapper implements JSONAware {

	private String incomeYear;
	private String baseCompensation;
	private String totalCompensation;
	
	private static final String YEAR_KEY = "IncomeYear";
	private static final String BASE_COMPENSATION_KEY = "BaseCompensation";
	private static final String TOTAL_COMPENSATION_KEY = "TotalCompensation";
	
	public AnnualCompensationInformationTypeWrapper(
			AnnualCompensationInformationType annualCompensationInformationType) {

		if (annualCompensationInformationType == null) {
			return;
		}
		
		if(annualCompensationInformationType.getIncomeYear() != null
    			&& annualCompensationInformationType.getIncomeYear().getDateRepresentation() != null 
    			&& annualCompensationInformationType.getIncomeYear().getDateRepresentation().getValue() != null){
    		GYear date = (GYear)annualCompensationInformationType.getIncomeYear().getDateRepresentation().getValue();
			this.incomeYear = date.getValue().toString();
		}
		
		this.baseCompensation = annualCompensationInformationType.getBaseCompensation() != null ? annualCompensationInformationType.getBaseCompensation().getValue().toString() : null;
		this.totalCompensation = annualCompensationInformationType.getTotalCompensation() != null ? annualCompensationInformationType.getTotalCompensation().getValue().toString() : null;
		
	}
	
	public String getIncomeYear() {
		return incomeYear;
	}

	public String getBaseCompensation() {
		return baseCompensation;
	}

	public String getTotalCompensation() {
		return totalCompensation;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(YEAR_KEY, this.incomeYear);
		obj.put(BASE_COMPENSATION_KEY, this.baseCompensation);
		obj.put(TOTAL_COMPENSATION_KEY, this.totalCompensation);
		return obj.toJSONString();
	}
	

}
