{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload":{"taxHousehold":[{"householdMember":[{"insurance":{"insurancePolicyExpirationDate":"12/31/2014","insurancePolicyEffectiveDate":"01/31/2014"},"householdContact":{"homeAddress":{"state":"CO"}},"organization":[{"organizationCode":"CHIP"},{"organizationCode":"MEDI"},{"organizationCode":"MEDC"}],"dateOfBirth":"09/17/1960","socialSecurityCard":{"socialSecurityNumber":"222224761"},"name":{"middleName":"C","lastName":"England","firstName":"Peter","personNameSuffixText":""},"gender":"M"}]}]}}