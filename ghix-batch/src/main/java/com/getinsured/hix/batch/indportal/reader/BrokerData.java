package com.getinsured.hix.batch.indportal.reader;

public class BrokerData {
	
	private String qualificationType;
	private String license;
	private String npn;
	private String name;
	private String address;
	private String cityName;
	private String state;
	private String zip;
	private String phone;
	private String email;
	private String originalIssueDate;
	private String licenseExpiryDate;


	public String getQualificationType() {
		return qualificationType;
	}
	public void setQualificationType(String qualificationType) {
		this.qualificationType = qualificationType;
	}
	public String getLicense() {
		return license;
	}
	public void setLicense(String license) {
		this.license = license;
	}
	public String getNpn() {
		return npn;
	}
	public void setNpn(String npn) {
		this.npn = npn;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOriginalIssueDate() {
		return originalIssueDate;
	}
	public void setOriginalIssueDate(String originalIssueDate) {
		this.originalIssueDate = originalIssueDate;
	}
	public String getLicenseExpiryDate() {
		return licenseExpiryDate;
	}
	public void setLicenseExpiryDate(String licenseExpiryDate) {
		this.licenseExpiryDate = licenseExpiryDate;
	}
	@Override
	public String toString() {
		return "BrokerData [qualificationType=" + qualificationType + ", license=" + license + ", npn=" + npn
				+ ", name=" + name + ", address=" + address + ", cityName=" + cityName + ", state=" + state + ", zip="
				+ zip + ", phone=" + phone + ", email=" + email + ", originalIssueDate=" + originalIssueDate
				+ ", licenseExpiryDate=" + licenseExpiryDate + "]";
	}
	
}
