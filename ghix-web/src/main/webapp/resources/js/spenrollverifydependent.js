var Dependent = {
        line_index:    1,
        dependent_row:
       '<tr id="employee_dependent_CN">\n' +
       		'<td><input type="hidden" name="employeeDetails[CN].id" id="dependent_CN_id" /><input type="hidden" name="employeeDetails[CN].type" id="dependent_CN_type" />'+
       		'<input type="hidden" name="employeeDetails[CN].firstName" id="dependent_CN_firstName" />'+
       		'<input type="hidden" name="employeeDetails[CN].middleInitial" id="dependent_CN_middleInitial" />'+
       		'<input type="hidden" name="employeeDetails[CN].lastName" id="dependent_CN_lastName"/> <span id="dependent_CN_NameSpan"></span> </td>\n' +
       		'<td><input type="hidden" name="employeeDetails[CN].location.id" id="dependent_CN_location_id" /> <span id="dependent_CN_RelationshipSpan"></span> </td>\n' +
       		'<td><input type="hidden" name="employeeDetails[CN].gender" id="dependent_CN_gender" /> <span id="dependent_CN_genderSpan"></span> </td>\n' +
       		'<td><input type="hidden" name="employeeDetails[CN].dob" id="dependent_CN_dob" /> <span id="dependent_CN_dobSpan"></span> </td>\n' +
       		'<td><input type="hidden" name="employeeDetails[CN].ssn" id="dependent_CN_ssn" /> <span id="dependent_CN_ssnSpan"></span> </td>\n' +
       		'<td><input type="hidden" name="employeeDetails[CN].smoker" id="dependent_CN_smoker" /> <span id="dependent_CN_smokerSpan"></span> </td>\n' +
       		'<td><input type="hidden" name="employeeDetails[CN].tobaccoCessPrg" id="dependent_CN_tobaccoCessPrg" /> <span id="dependent_CN_tobaccoCessPrgSpan"></span> </td>\n' +
       		'<td><input type="hidden" name="employeeDetails[CN].nativeAmr" id="dependent_CN_nativeAmr" /> <span id="dependent_CN_nativeAmrSpan"></span> </td>\n' +
       		'<input type="hidden" name="employeeDetails[CN].event_id" id="dependent_CN_event_id" />'+
       		'<input type="hidden" name="employeeDetails[CN].location.address1" id="dependent_CN_address1" />'+
       		'<input type="hidden" name="employeeDetails[CN].location.address2" id="dependent_CN_address2" />'+
       		'<input type="hidden" name="employeeDetails[CN].location.city" id="dependent_CN_city" />'+
       		'<input type="hidden" name="employeeDetails[CN].location.state" id="dependent_CN_state" />'+
       		'<input type="hidden" name="employeeDetails[CN].location.county" id="dependent_CN_county" />'+
       		'<input type="hidden" name="employeeDetails[CN].location.zip" id="dependent_CN_zip" />'+
       		'<input type="hidden" name="employeeDetails[CN].isEmployeeAddress" id="dependent_CN_isEmployeeAddress" />'+
       		'<input type="hidden" name="employeeDetails[CN].suffix" id="dependent_CN_suffix" />'+
       		'<input type="hidden" name="employeeDetails[CN].wardDoc" id="dependent_CN_wardDoc" />'+
       		'<input type="hidden" name="employeeDetails[CN].relationship" id="dependent_CN_relationship" />'+
       		'<td><a class="btn btn-mini btn-danger" id="deleteDependent_\CN\"title="Delete" data-toggle="modal"><i class="icon-trash"></i> Delete</a>'+
       		'<a class="btn btn-mini btn-danger" id="undoDeleteDependent_\CN\"title="Undo Delete" style="margin-top: 5px;">Undo Delete</a>'+
        	'<a class="btn btn-mini btn-primary" id="editDependent_\CN\" title="Edit" data-toggle="modal" style="margin-top: 5px;"><i class="icon-pencil"></i> Edit</a></td>\n' +
        '</tr>',
            
        addspouse_btn:
                '<tr id="employee_spouse">' +
                '<td><a class="btn btn-mini btn-primary" title="Add Spouse" onclick="javascript:Dependent.edit_dependent(\'0\',\'SPOUSE\',\'true\')" data-toggle="modal"><i class="icon-plus-sign"></i> Add Spouse</a></td>' +
                '<td>&nbsp;</td>' +
                '<td>&nbsp;</td>' +
                '<td>&nbsp;</td>' +
                '<td>&nbsp;</td>' +
                '<td>&nbsp;</td>' +
                '<td>&nbsp;</td>' +
                '<td>&nbsp;</td>' +
                '</tr>',
   
    add_dependent: function(id, type,imagepath, firstName, lastName, dob, locationid, location, gender, ssn, smoker, tobaccoCessPrg, nativeAmr, middleInitial,address1, address2, city, state, county,zip,event_id,isEmployeeAddress,isDeleted,suffix,relationship, wardDoc,isAddEditDependentAllowed,wardDocumentRequired){
    			var row = this.dependent_row;
                row = row.replace(/CN/g, this.line_index);
                row = row.replace(/DEPENDENT_TYPE/,type);
                
                if(type == "SPOUSE" ||type == "LIFE_PARTNER"){
                    $("#employee_spouse").remove();
                    $('#employeeSpouseTable > tbody').append(row);
	            }else{
	                    $('#employeeChildTable > tbody').append(row);
	            }
                $('#dependent_'+this.line_index+'_id').val(id);
                $('#dependent_'+this.line_index+'_type').val(type);
                $('#dependent_'+this.line_index+'_firstName').val(firstName);
                $('#dependent_'+this.line_index+'_lastName').val(lastName);
                $('#dependent_'+this.line_index+'_dob').val(dob);
                $('#dependent_'+this.line_index+'_location_id').val(locationid);
                $('#dependent_'+this.line_index+'_gender').val(gender);
                $('#dependent_'+this.line_index+'_smoker').val(smoker);
                $('#dependent_'+this.line_index+'_tobaccoCessPrg').val(tobaccoCessPrg);
                $('#dependent_'+this.line_index+'_nativeAmr').val(nativeAmr);
                $('#dependent_'+this.line_index+'_middleInitial').val(middleInitial);
                $('#dependent_'+this.line_index+'_isEmployeeAddress').val(isEmployeeAddress);
                $('#dependent_'+this.line_index+'_suffix').val(suffix);   
                $('#dependent_'+this.line_index+'_relationship').val(relationship);
                $('#dependent_'+this.line_index+'_wardDoc').val(wardDoc);
                
                $('#dependent_'+this.line_index+'_event_id').val(event_id);                
                $('#dependent_'+this.line_index+'_address1').val(address1);
                $('#dependent_'+this.line_index+'_address2').val(address2);
                $('#dependent_'+this.line_index+'_city').val(city);
                $('#dependent_'+this.line_index+'_state').val(state);
                $('#dependent_'+this.line_index+'_county').val(county);
                $('#dependent_'+this.line_index+'_zip').val(zip);
                
                $('#dependent_'+this.line_index+'_NameSpan').html(firstName + ' ' + lastName);
                $('#dependent_'+this.line_index+'_dobSpan').html(dob);
                
                if(relationship!=''){ 
	                if(relationship == 'none'){
	                	$('#dependent_'+this.line_index+'_RelationshipSpan').html(type);
	                }else{
	                	$('#dependent_'+this.line_index+'_RelationshipSpan').html(relationship);
	                }
                }
                else{ 
                	$('#dependent_'+this.line_index+'_RelationshipSpan').html(type);
                }
                $('#dependent_'+this.line_index+'_genderSpan').html(gender);
                var tmp="";var ssnEncode ="";
                if(ssn.length>2){
	                tmp = ssn.substr(7,4);
	                ssnEncode = '***-**-'+tmp;
                }
                $('#dependent_'+this.line_index+'_ssnSpan').html(ssnEncode);
                $('#dependent_'+this.line_index+'_ssn').val(ssnEncode);

                if(type=='WARD'){
                	$('#uploadWardDoc').show();
					/*if(wardDocumentRequired == 'YES'){
                		$('#imgDiv').show();
                	}*/
                }
                else{
                	$('#uploadWardDoc').hide();
                }
                
                /*click event for editting the dependent*/
                $('#editDependent_'+this.line_index).live('click',function(event){
                	var showModal = 'true';

                	if ($(this).attr("disabled")==true || $(this).attr("disabled") == 'disabled'){
                		showModal = 'false';
                	}
                	var currentIdNumber = (event.currentTarget.id).replace('editDependent_',''),
                		ntype = 'NA',
                		showModal;
            		Dependent.edit_dependent(currentIdNumber,ntype,showModal,wardDocumentRequired)
            	});
                
                /*click event for editting the dependent*/
                $('#deleteDependent_'+this.line_index).live('click',function(event){
                	var currentIdNumber = (event.currentTarget.id).replace('deleteDependent_','');
            		Dependent.delete_dependent(currentIdNumber);
            	});
                
                /*click event for undo-delete the dependent*/
                
                if(isDeleted == 'Y'){
                	Dependent.delete_dependentUI(this.line_index);
                }
                else{
                	Dependent.undo_delete_dependentUI(this.line_index);
                }
                var tempIndex=this.line_index;
                $('#undoDeleteDependent_'+this.line_index).live('click',function(event){
                	$('#deleteEmpBody').html("<p>Are you sure you want to undo delete the information for "+ firstName + " " + $('#lastName').val() +"?</p>");
                	 $('#deleteEmployeeButton').bind('click',function(){
                		 $('#deletedependent').modal('hide');
                		 undoDeleteEmployeeDetails(id,tempIndex);
                     });
                     $('#deletedependent').modal('show');
                   	
                	});	
                
                
             // Hide edit buttom in case of death,divorce & Legal separation
                if(isAddEditDependentAllowed == 'false'){
                	$('#editDependent_'+this.line_index).hide();
                }
                
                this.line_index++;
                dataCount = this.line_index;
                
                
        },
        
        add_spouseBtn: function() { 
                $('#employeeSpouseTable > tbody').append(this.addspouse_btn);
                $('#spouseClick').live('click',function(event){
                	var currentIdNumber = 0,
            		ntype = 'SPOUSE',
            		showModal = 'true';
                	Dependent.edit_dependent(currentIdNumber,ntype,showModal,''); 
                });
        },
        
        edit_dependent: function(elementid,ntype,showModal,wardDocumentRequired) {
                Dependent.resetBlock();
                $('#employee_id').val(employeeId);
                $('#CN').val(elementid);
                if(elementid == 0){
                        $('#type').val(ntype);
                        selectedLocation = 'newAddress';
                        $('#modalTitle').html('Add Dependent');
                        $('#location_id').val(0);
                        $('#qualifyeventDIV').show();
	                	$('#qualifyeventdateDIV').show();
	                	$('#isEmployeeAddress').val('N');
                        $('#addressDiv').show();
                        $('#upload_files_container').html('');
                        if(ntype=='WARD'){ $('#uploadWardDoc').show();$('#file_upload_container').show();$('#fileupload').show();
                        	/*if(wardDocumentRequired == 'YES'){
                        		$('#imgDiv').show();
                        	}*/
                        }
                        else{$('#uploadWardDoc').hide();}
                        if(ntype=='SPOUSE' || ntype=='LIFE_PARTNER'){$('#relationshipDiv').show();}
                        else{$('#relationshipDiv').hide();}
                        $('#ssn1').show();
	                    $('#ssn2').show();
	                    $('#ssn3').show();
	                    $('#showEncrSsn').hide();
	                    $('#editSsnButton').hide();
	                    isNewDepe = true;
                }else{
                		selectedLocation = 'homeAddress';
		                $('#id').val($('#dependent_'+elementid+'_id').attr('value'));
		                $('#type').val($('#dependent_'+elementid+'_type').attr('value'));
		                $('#firstName').val($('#dependent_'+elementid+'_firstName').attr('value'));
		                $('#lastName').val($('#dependent_'+elementid+'_lastName').attr('value'));
		                $('#middleInitial').val($('#dependent_'+elementid+'_middleInitial').attr('value'));
		                
		                if($('#dependent_'+elementid+'_event_id').attr('value') == ''){
		                	$('#qualifyeventDIV').hide();
		                	$('#qualifyeventdateDIV').hide();
		                }else{
		                	$('#qualifyeventDIV').show();
		                	$('#qualifyeventdateDIV').show();
		                	
		                	$('#qualifyevent').val($('#dependent_'+elementid+'_event_id').attr('value'));
		                	$('#qualifyevent').trigger('change');
		                }
		               
		                
		                
		                $('#location_id').val($('#dependent_'+elementid+'_location_id').attr('value'));
		                $('#address1').val($('#dependent_'+elementid+'_address1').attr('value'));
		                $('#address2').val($('#dependent_'+elementid+'_address2').attr('value'));
		                $('#city').val($('#dependent_'+elementid+'_city').attr('value'));
		                $('#state').val($('#dependent_'+elementid+'_state').attr('value'));
		                $('#zip').val($('#dependent_'+elementid+'_zip').attr('value'));
		                getCountyList($('#dependent_'+elementid+'_zip').attr('value'), $('#dependent_'+elementid+'_county').attr('value'));
		                
		                var type = $('#dependent_'+elementid+'_type').attr('value');
		                if(type=='SPOUSE' || type=='LIFE_PARTNER'){$('#relationshipDiv').show();}
                        else{$('#relationshipDiv').hide();}
		                
		                var gender = $('#dependent_'+elementid+'_gender').attr('value');
		                if(gender != null && gender != ''){
		                        if(gender == 'MALE'){
		                                $('input:radio[name=gender]')[0].checked = true;
		                        }else if(gender == 'FEMALE'){
		                                $('input:radio[name=gender]')[1].checked = true;
		                        }
		                }else{
		                        $('input:radio[name=gender]').attr('checked',false);
		                }
		               
		                var smoker = $('#dependent_'+elementid+'_smoker').attr('value');
		                if(smoker != null && smoker != ''){ 
		                        if(smoker == 'YES'){
		                                $('input:radio[name=smoker]')[0].checked = true;
		                        }else if(smoker == 'NO'){
		                                $('input:radio[name=smoker]')[1].checked = true;
		                        }
		                }else{
		                        $('input:radio[name=smoker]').attr('checked',false);
		                }
		                
		                var isTobaccoCessPrg = $('#dependent_'+elementid+'_tobaccoCessPrg').attr('value');
		                if(smoker == 'YES'){
	                        if(isTobaccoCessPrg == 'YES'){
	                                $('input:radio[name=tobaccoCessPrg]')[0].checked = true;
	                            	$('#tobaccoCessPrgDiv').show();
	                                
	                        }else if(isTobaccoCessPrg == 'NO'){
	                                $('input:radio[name=tobaccoCessPrg]')[1].checked = true;
	                                $('#tobaccoCessPrgDiv').show();
	                        }else {
	                            $('#tobaccoCessPrgDiv').show();
	                        }
		                }else{
		                	$('input:radio[name=tobaccoCessPrg]').attr('checked',false);
		                	$('#tobaccoCessPrgDiv').hide();	
		                }
		                
		                var nativeAmr = $('#dependent_'+elementid+'_nativeAmr').attr('value');
		                if(nativeAmr != null && nativeAmr != ''){
		                        if(nativeAmr == 'YES'){
		                                $('input:radio[name=nativeAmr]')[0].checked = true;
		                        }else if(nativeAmr == 'NO'){
		                                $('input:radio[name=nativeAmr]')[1].checked = true;
		                        }
		                }else{
		                        $('input:radio[name=nativeAmr]').attr('checked',false);
		                } 
		 
		                var isEmployeeAddress = $('#dependent_'+elementid+'_isEmployeeAddress').attr('value');
		                if(isEmployeeAddress != null && isEmployeeAddress != ''){
		                        if(isEmployeeAddress == 'Y'){
		                                $('input:radio[name=isEmployeeAddress]')[0].checked = true;
		                                $('#addressDiv').hide();
		                        }else if(isEmployeeAddress == 'N'){
		                                $('input:radio[name=isEmployeeAddress]')[1].checked = true;
		                                $('#addressDiv').show();
		                        }
		                }else{
		                        $('input:radio[name=isEmployeeAddress]').attr('checked',false);
		                }
		                
		                var str = $('#dependent_'+elementid+'_dob').attr('value');
		                if(str.match('N/A') == null){
		                	$('#dob').val($('#dependent_'+elementid+'_dob').attr('value'));
		                }else{
		                	$('#dob').val('');
		                }
		                
		                var suffix = $('#dependent_'+elementid+'_suffix').attr('value');
		                if(suffix != null && suffix != ''){
		                       $('#suffix').val(suffix);
		                }else{
		                        $('#suffix').val('');
		                } 
		                
		                var relationship = $('#dependent_'+elementid+'_relationship').attr('value');
		                if(relationship != null && relationship != ''){
		                       $('#relationship').val(relationship);
		                }else{
		                        $('#relationship').val('');
		                } 
		                
		              //ward supporting document
		                var wardDoc = $('#dependent_'+elementid+'_wardDoc').attr('value');
		                
		                if(wardDoc!='none' && wardDoc!='' && wardDoc != null){
		                	$('#uploadWardDoc').show();
		                	fileDetails=wardDoc.split('#');
		            		if(fileDetails.length >=1){
		                		uploadedDocId=fileDetails[0];
		                    	uploadedDocId=uploadedDocId.substring(uploadedDocId.indexOf("workspace"),uploadedDocId.length);
		                    	$('#wardDocument').val(uploadedDocId);
		                    	rowno++;
		                    	appendFileDetails();		
		                    	$( "#fileupload" ).hide();
		                	}
		                }
		                else{
		                	$('#wardDocument').val(null);
		                	 if(type=='WARD'){ $('#uploadWardDoc').show();$('#file_upload_container').show();$('#fileupload').show();
					 		 	/*if(wardDocumentRequired == 'YES'){
		                	 		$('#imgDiv').show();
		                	 	}*/
							 }
		                     else{$('#uploadWardDoc').hide();}
		                	$('#upload_files_container').html('');
		                }
		                
		                var str1 = $('#dependent_'+elementid+'_ssn').attr('value');
		                if(str1.match('N/A') == null && str1 != "--" && str1 != ""){
	                		var taxId = $('#dependent_'+elementid+'_ssn').attr('value').split('-');
                			$('#showEncrSsn').html("***-**-" + taxId[2]);
		                    $('#showEncrSsn').show();
		                    $('#editSsnButton').show();
		                    $('#ssn1').hide();
		                    $('#ssn2').hide();
		                    $('#ssn3').hide();
	                	}else {
                			$('#ssn1').show();
		                    $('#ssn2').show();
		                    $('#ssn3').show();
		                    $('#showEncrSsn').hide();
		                    $('#editSsnButton').hide();
	                	}
		                $('#modalTitle').html('Edit Dependent');
                
                }
             /*   $('.icon-calendar').click(function(){
                	var dateT = $('#dob').val();
                	$('.date-picker1').datepicker('update', dateT);
                	$(".datepicker").css("z-index", "2001");
                 });*/
                
                if(showModal == 'true'){
                        $('#editdependent').modal({show: true, keyboard: false});
                }
                
                
        },
        
        edit_dependentUI: function(elementid,id,newLocId,newLocName) {
        	
                $('#CN').val(0);
                $('#dependent_'+elementid+'_id').val(id);
                $('#dependent_'+elementid+'_type').val($('#type').attr('value'));
                $('#dependent_'+elementid+'_firstName').val($('#firstName').attr('value'));
                $('#dependent_'+elementid+'_lastName').val($('#lastName').attr('value'));
                $('#dependent_'+elementid+'_dob').val($('#dob').attr('value'));
                $('#dependent_'+elementid+'_gender').val($('input[name=gender]:radio:checked').val());
                $('#dependent_'+elementid+'_middleInitial').val($('#middleInitial').attr('value'));
                $('#dependent_'+elementid+'_smoker').val($('input[name=smoker]:radio:checked').val());
                $('#dependent_'+elementid+'_tobaccoCessPrg').val($('input[name=tobaccoCessPrg]:radio:checked').val());
                $('#dependent_'+elementid+'_nativeAmr').val($('input[name=nativeAmr]:radio:checked').val());
                $('#dependent_'+elementid+'_isEmployeeAddress').val($('input[name=isEmployeeAddress]:radio:checked').val());
                $('#dependent_'+elementid+'_suffix').val($('#suffix').val());
                $('#dependent_'+elementid+'_relationship').val($('#relationship').val());
                if(tempFileName!=''){
                	$('#dependent_'+elementid+'_wardDoc').val($('#wardDocument').val()+'#'+tempFileName);
                    tempFileName='';	
                }else{
                	$('#dependent_'+elementid+'_wardDoc').val($('#wardDocument').val());
                }
                
                
                $('#dependent_'+elementid+'_event_id').val($('#qualifyevent').attr('value'));
                
                $('#dependent_'+elementid+'_address1').val($('#address1').attr('value'));
                $('#dependent_'+elementid+'_address2').val($('#address2').attr('value'));
                $('#dependent_'+elementid+'_city').val($('#city').attr('value'));
                $('#dependent_'+elementid+'_state').val($('#state').attr('value'));
                $('#dependent_'+elementid+'_zip').val($('#zip').attr('value'));
                var temp = $('#county').attr('value').split('#');
                $('#dependent_'+elementid+'_county').val(temp[0]);

                if(newLocId != null){
                        $('#dependent_'+elementid+'_location_id').val(newLocId);
                       // $('#dependent_'+elementid+'_locationSpan').html(newLocName);
                }else{

                        $('#dependent_'+elementid+'_location_id').val($('input[id=homeAddress]:radio:checked').val());
                       // $('#dependent_'+elementid+'_locationSpan').html(locationsObj[$('input[id=homeAddress]:radio:checked').val()]);
                }
                
                $('#dependent_'+elementid+'_NameSpan').html($('#firstName').attr('value') + ' ' + $('#lastName').attr('value'));
                $('#dependent_'+elementid+'_dobSpan').html($('#dob').attr('value'));
                if($('#relationship').val()!='' && $('#relationship').val()!='none'){
                $('#dependent_'+elementid+'_RelationshipSpan').html($('#relationship').val());
                }else{
                	$('#dependent_'+elementid+'_RelationshipSpan').html($('#type').val());
                }                

                $('#dependent_'+elementid+'_genderSpan').html($('input[name=gender]:radio:checked').val());
                var tmp="";var ssnEncode ="";
                if($('#ssn').attr('value').length > 2){
	                tmp = $('#ssn').attr('value').substr(7,4);
	                ssnEncode = '***-**-'+tmp;
                }
                $('#dependent_'+elementid+'_ssn').val(ssnEncode);
                $('#dependent_'+elementid+'_ssnSpan').html(ssnEncode);
                
        },
        
        resetBlock: function(){
                $('#CN').val('0');
                $('#id').val('0');
                $('#employee_id').val('0');
                $('#type').val('');
                $('#firstName').val('');
                $('#lastName').val('');
                $('input:radio[name=gender]').attr('checked',false);
                $('#dob').val('');
                $('#homeAddress').val('');
                $('input:radio[name="location.id"]').attr('checked',false);
                $('.new-address').hide();
                $('#homeAddressSpan').html('');
                $('#ssn1').val('');
                $('#ssn2').val('');
                $('#ssn3').val('');
                $('#middleInitial').val('');
                $('input:radio[name=smoker]').attr('checked',false);
                $('input:radio[name=tobaccoCessPrg]').attr('checked',false);
                $('input:radio[name=nativeAmr]').attr('checked',false);
                $('input:radio[id=isEmployeeAddressNo]').attr('checked',true);
                $('#suffix').val('');
                $('#relationship').val('');
                $('#wardDocument').val(null);
                $('#fileupload').val('');
                $('#upload_files_container').html('');
                
                $('#firstName_error').html('');
                $('#lastName_error').html('');
                $('#middleInitial_error').html('');
                $('#gender_error').html('');
                $('#dob_error').html('');
                $('#ssn3_error').html('');
                $('#smoker_error').html('');
                $('#nativeAmrYes_error').html('');
                $('#nativeAmrNo_error').html('');
                
                $('#qualifyevent').val('');
                
                $('#address1').val('');
                $('#address2').val('');
                $('#city').val('');
                $('#state').val('');
                $('#zip').val('');
                selectedLocation = "homeAddress";
                
                $('#address1_error').html('');
                $('#city_error').html('');
                $('#state_error').html('');
                $('#zip_error').html('');
                
                $('#county option:first-child').attr("selected", "selected");
                $('#qualifyevent').val('');
            	$('#qualifyeventdate').val('');
            	$('#fileupload_error').hide();
        },
        
        delete_dependent: function(elementid){
                Dependent.edit_dependent(elementid,'NA','false',wardDocumentRequired);
              //  $('#deleteEmpBody').html("<p>Are you sure you want to delete the information for "+$('#firstName').val()+ " " + $('#lastName').val() +"?</p>");
                $('#name').html($('#firstName').val()+ " " + $('#lastName').val());
                $('#deleteEmployeeButton').bind('click',function(){
                	deleteEmployeeDetails();
                });
                $('#event').val('');
            	$('#eventdate').val('');
            	$('input[name=attestation]').attr('checked', false);
            	$('#submitbtn').attr("disabled", false);
                $('#terminatedependent').modal('show');
                
                /*
                 * $('#deleteEmployeeButton').bind('click',function(){
                        deleteEmployeeDetails(); } );
                 * */
        },
        
        delete_dependentUI: function(index){
                $("#deleteDependent_" + index).hide();
                $("#undoDeleteDependent_" + index).show();
                $("#editDependent_" + index).attr("disabled", true);
        },  
        
        undo_delete_dependentUI: function(index){
            $("#deleteDependent_" + index).show();
            $("#undoDeleteDependent_" + index).hide();
            $("#editDependent_" + index).attr("disabled", false);
    }  
};