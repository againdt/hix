package com.getinsured.iex.ssap.financial;

import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.Name;
import com.getinsured.iex.ssap.financial.type.Frequency;

/**
 * Describes employer sponsored coverage for the household member.
 * @author Yevgen Golubenko
 * @since 6/12/19
 */
public class EmployerSponsoredCoverage {
  private int personId;
  private String companyName;
  private String memberName;
  private Name name = new Name();
  private String phone;
  private String email;
  private Address address = new Address();
  private String ein;
  private Boolean minimumValuePlan = null; // UI requires this to be not pre-selected radio button
  private long employerPremium;
  private Frequency employerPremiumFrequency = Frequency.UNSPECIFIED;
  private boolean employerPremiumUnknown;
  private boolean employerOfferAffordable;

  public int getPersonId() {
    return personId;
  }

  public void setPersonId(int personId) {
    this.personId = personId;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getMemberName() {
    return memberName;
  }

  public void setMemberName(String memberName) {
    this.memberName = memberName;
  }

  public Name getName() {
    return name;
  }

  public void setName(Name name) {
    this.name = name;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public String getEin() {
    return ein;
  }

  public void setEin(String ein) {
    this.ein = ein;
  }

  public Boolean getMinimumValuePlan() {
    return minimumValuePlan;
  }

  public void setMinimumValuePlan(Boolean minimumValuePlan) {
    this.minimumValuePlan = minimumValuePlan;
  }

  public long getEmployerPremium() {
    return employerPremium;
  }

  public void setEmployerPremium(long employerPremium) {
    this.employerPremium = employerPremium;
  }

  public Frequency getEmployerPremiumFrequency() {
    return employerPremiumFrequency;
  }

  public void setEmployerPremiumFrequency(Frequency employerPremiumFrequency) {
    this.employerPremiumFrequency = employerPremiumFrequency;
  }

  public boolean isEmployerPremiumUnknown() {
    return employerPremiumUnknown;
  }

  public void setEmployerPremiumUnknown(boolean employerPremiumUnknown) {
    this.employerPremiumUnknown = employerPremiumUnknown;
  }

  public boolean isEmployerOfferAffordable() {
    return employerOfferAffordable;
  }

  public void setEmployerOfferAffordable(boolean employerOfferAffordable) {
    this.employerOfferAffordable = employerOfferAffordable;
  }

  @Override
  public String toString() {
    return "EmployerSponsoredCoverage{" +
        "personId=" + personId +
        ", companyName='" + companyName + '\'' +
        ", memberName='" + memberName + '\'' +
        ", name=" + name +
        ", phone='" + phone + '\'' +
        ", email='" + email + '\'' +
        ", address=" + address +
        ", ein='" + ein + '\'' +
        ", minimumValuePlan=" + minimumValuePlan +
        ", employerPremium=" + employerPremium +
        ", employerPremiumFrequency=" + employerPremiumFrequency +
        ", employerPremiumUnknown=" + employerPremiumUnknown +
        ", employerOfferAffordable=" + employerOfferAffordable +
        '}';
  }
}
