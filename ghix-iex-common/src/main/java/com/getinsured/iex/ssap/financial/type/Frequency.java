package com.getinsured.iex.ssap.financial.type;

/**
 * Describes frequency for {@link IncomeType} or {@link ExpenseType}.
 *
 * @author Yevgen Golubenko
 * @since 4/1/19
 */
public enum Frequency {
  /**
   * Twice a month.
   */
  BIMONTHLY,

  /**
   * Every two weeks.
   */
  BIWEEKLY,

  /**
   * Daily. Every day.
   */
  DAILY,

  /**
   * Every hour
   */
  HOURLY,

  /**
   * Every month.
   */
  MONTHLY,

  /**
   * One time only.
   */
  ONCE,

  /**
   * Every quarter.
   */
  QUARTERLY,

  /**
   * Every week.
   */
  WEEKLY,

  /**
   * Once a year.
   */
  YEARLY,

  /**
   * Not specified frequency,
   * default.
   */
  UNSPECIFIED
}
