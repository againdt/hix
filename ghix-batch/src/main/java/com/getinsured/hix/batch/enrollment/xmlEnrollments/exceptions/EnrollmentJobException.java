package com.getinsured.hix.batch.enrollment.xmlEnrollments.exceptions;

public class EnrollmentJobException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7840478279042763503L;

	public EnrollmentJobException() {
		// TODO Auto-generated constructor stub
	}

	public EnrollmentJobException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EnrollmentJobException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public EnrollmentJobException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
