(function() {
'use strict';
    angular.module('agencyApp')
    .controller('AppController', AppController);

    AppController.$inject = ['$scope', '$state', '$window', '$location','$http', 'NavService', 'StatusService', 'truncFilter', 'timeValidateFilter', 'dataStorageService'];

    function AppController($scope, $state, $window, $location, $http, NavService, StatusService, truncFilter, timeValidateFilter, dataStorageService) {
        var vm = this;
        var agencyCertificationStatus = {};
        var status = $("#agencyCertificationStatus").val();
        var logoutUrl = $('#logoutUrl').val();
        vm.agencyId = $("#agencyId").val();
        vm.goToPage = goToPage;
        vm.sideNavListOne = NavService.SideNavOne;
        vm.sideNavListTwo = NavService.SideNavTwo;
        vm.sideNavListThree = NavService.SideNavThree;
        vm.sideNavListFour = NavService.SideNavFour;
  	    vm.agencyCertificationStatus = StatusService.CertificationStatus;
        vm.currentUserRole = StatusService.CurrentUserRole;

        function goToPage(link) {
            $window.location.href = link;
        }
    }
})();
