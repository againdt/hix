package com.getinsured.hix.indportal.dto;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.getinsured.hix.dto.enrollment.EnrolleeDataDTO;
import com.getinsured.hix.indportal.IndividualPortalConstants;

/**
 * HIX-59164
 * Display Enrollment History for the Household
 * 
 * @author sahoo_s
 */
public class EnrolleeHistoryEntry {

	private String name;
	private String startDate;
	private String endDate;
	private String relationship;
	private String exchgIndivIdentifier;
	
	private DateTimeFormatter formatter = DateTimeFormat.forPattern(IndividualPortalConstants.SHORT_DATE_FORMAT);
	
	public EnrolleeHistoryEntry(EnrolleeDataDTO enrolleeDataDTO) {
		StringBuilder name = new StringBuilder();
		name.append(enrolleeDataDTO.getFirstName());
		if(null != enrolleeDataDTO.getMiddleName() && !enrolleeDataDTO.getMiddleName().isEmpty()) {
			name.append(" ");
			name.append(enrolleeDataDTO.getMiddleName());
		}
		name.append(" ");
		name.append(enrolleeDataDTO.getLastName());
						
		if(null != enrolleeDataDTO.getSuffix() && !enrolleeDataDTO.getSuffix().isEmpty()) {
			name.append(" ");
			name.append(enrolleeDataDTO.getSuffix());
		}
		this.setName(name.toString());
		this.setStartDate(formatter.print(enrolleeDataDTO.getEnrolleeEffectiveStartDate().getTime()));
		this.setEndDate(formatter.print(enrolleeDataDTO.getEnrolleeEffectiveEndDate().getTime()));
		this.setRelationship(enrolleeDataDTO.getRelationshipToSubscriber());
		this.setExchgIndivIdentifier(enrolleeDataDTO.getMemberId());
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	
	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}

	public void setExchgIndivIdentifier(String exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}
	
}
