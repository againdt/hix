package com.getinsured.hix.batch.provider;

public class ProviderSolrDocument {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
