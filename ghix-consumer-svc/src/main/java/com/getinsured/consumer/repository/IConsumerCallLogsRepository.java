package com.getinsured.consumer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.consumer.ConsumerCallLog;

public interface IConsumerCallLogsRepository extends JpaRepository<ConsumerCallLog, Integer> {
	
	@Query("SELECT  ccl.created,au.userName,ccl.callData,ccl.datetime,ccl.duration, (select c.comment from Comment c,CommentTarget ctr where  c.commentTarget.id=ctr.id"+
			" and ctr.targetId=ccl.id and ctr.targetName='ConsumerCallLog')FROM ConsumerCallLog ccl,AccountUser au WHERE ccl.agentId=au.id and ccl.householdId=:householdId order by ccl.created desc")
	public List<Object[]> getConsumerCallLogs(@Param("householdId") Integer householdId);
	
}
