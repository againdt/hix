package com.getinsured.hix.platform.sms.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.platform.sms.SmsResponse;

public class CallServiceTest extends BaseTest{

	private @Autowired SmsService smsService;

	@Test
	public void testIndiaNumber() {
		SmsResponse status = smsService.call("(650) 618-4604", "This is a vimo junit test message with phone format +1 (650) 618-4604.<pin>","1223232");
		assertNotNull(status);
		assertTrue("FAILURE".equals(status.getStatus()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyToPhone1(){
		smsService.call(null, "test The PIN is <pin>","1223232");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyToPhone2(){
		smsService.call("", "test The PIN is <pin>","1223232");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyBody1(){
		smsService.call("+919819084912", null,"1223232");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyBody2(){
		smsService.call("+919819084912", "","1223232");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidBody2(){
		smsService.call("+1 (650) 618-4604", "how are you?how are you?how are you?how are you?how are you?how are you?how are you?how are you?how are you?how are you?how are you?how are you?how are you?12345","1223232");

	}

	@Test
	@Ignore(value = "Ignored to avoid unnecessary sms sending")
	public void testValidFormat1(){
		SmsResponse status = smsService.call("+16506184604", "This is a vimo junit test message with phone format +16506184604.","1223232");
		assertNotNull(status);
		assertTrue("queued".equals(status.getStatus()));
	}

	@Test
	@Ignore(value = "Ignored to avoid unnecessary sms sending")
	public void testValidFormat2(){
		SmsResponse status = smsService.call("6506184604", "This is a vimo junit test message with phone format 6506184604.","1223232");
		assertNotNull(status);
		assertTrue("queued".equals(status.getStatus()));
	}

	@Test
	@Ignore(value = "Ignored to avoid unnecessary sms sending")
	public void testValidFormat3(){
		SmsResponse status = smsService.call("(650) 618-4604", "This is a vimo junit test message with phone format (650) 618-4604.","1223232");
		assertNotNull(status);
		assertTrue("queued".equals(status.getStatus()));
	}

	@Test
	@Ignore(value = "Ignored to avoid unnecessary sms sending")
	public void testValidFormat4(){
		SmsResponse status = smsService.call("(650)618-4604", "This is a vimo junit test message with phone format (650)618-4604.","1223232");
		assertNotNull(status);
		assertTrue("queued".equals(status.getStatus()));
	}

	@Test
	public void testValidData() {
		SmsResponse status = smsService.call("+919819084912", "This is a vimo junit test message with phone format +1 (650) 618-4604.<pin>","1223232");
		assertNotNull(status);
		assertTrue("queued".equals(status.getStatus()));
	}
}
