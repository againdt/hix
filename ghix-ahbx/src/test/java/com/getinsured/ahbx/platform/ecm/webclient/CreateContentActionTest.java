package com.getinsured.ahbx.platform.ecm.webclient;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.getinsured.hix.model.PlatformRequest;
import com.getinsured.hix.model.PlatformRequest.ECMRequest;
import com.getinsured.hix.model.PlatformResponse;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/applicationContext.xml")
public class CreateContentActionTest {

	@Autowired private CreateContentAction createContentAction;

	@Test
	public void testCreateContent() throws ContentManagementServiceException {

		//String str = "Hello World!!!";
		String str =  "X5O!P%@AP[4\\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*";
		PlatformRequest platformRequest = new PlatformRequest();
		ECMRequest ecmRequest = platformRequest.new ECMRequest();
		ecmRequest.setBytes(str.getBytes());
		ecmRequest.setMetadata(new HashMap<String, String>());
		platformRequest.setEcmRequest(ecmRequest);

		String ahbxResponse = createContentAction.createContent(platformRequest);

		/**
		 * UnMarshal and from PlatformResponse object
		 */
		XStream xstream = GhixUtils.getXStreamStaxObject();
		PlatformResponse platformResponse = (PlatformResponse) xstream.fromXML(ahbxResponse);

		/**
		 * If AHBX UCM call failed, throw Exception to the caller
		 */
		if ("FAILURE".equals(platformResponse.getStatus())){
			throw new ContentManagementServiceException("AHBX UCM call failed - " +  platformResponse.getErrMsg());
		}

		platformResponse.getAhbxEcmId();
		assertNotNull(platformResponse.getAhbxEcmId());
	}

}
