package com.getinsured.hix.batch.node;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.Nodes;
import com.getinsured.hix.platform.util.NetworkUtil;

@Component
@DependsOn("configProp")
public class NodePublisher {
	private Nodes localNode= null;;
	private Timer timer;
	private String nodeAlias = null;
	private String defaultNodeAccessUrl = null;
	private String nodeDetails = null;
	private String protocol = null;
	private boolean publishingEnabled = true;
	private String nodeProfile = null;
	
	private static Logger LOGGER = LoggerFactory.getLogger(NodePublisher.class);
	@PersistenceUnit
	private EntityManagerFactory emf = null;
	
	/*@PostConstruct
	private NodeKeepAlive keepAlive;
	private long keepAliveFrequencyInMillis= 30*1000;
	public void publishNodeHealth() {
		publishingEnabled = this.nodeProfile.equalsIgnoreCase("batch".intern());
		if(!publishingEnabled){
			LOGGER.info("Publishing not enabled for this node profile :".intern()+publishingEnabled);
			return;
		}
		// Check if the node is registered, if not do it
		localNode = this.getLocalNode();
		if(localNode == null){
			Nodes tmp = new Nodes();
			tmp.setAlias(this.nodeAlias);
			List<String> localIpAddresses = NetworkUtil.getLocalIpAddress();
			StringBuilder sb = new StringBuilder();
			for(String s: localIpAddresses){
				sb.append(s);
				sb.append(" ");
			}
			tmp.setIpAddresses(sb.toString());
			tmp.setLastActive(new Timestamp(System.currentTimeMillis()));
			tmp.setEnabled(true);
			tmp.setBaseUrl(defaultNodeAccessUrl);
			if(registerLocalNode(tmp)){
				this.localNode = tmp;
			}else{
				throw new RuntimeException("Failed to register node ".intern()+sb.toString());
			}
		}
		if(LOGGER.isTraceEnabled()){
			LOGGER.trace("Local node available {} with IP:{}".intern(),this.localNode.getAlias(),this.localNode.getIpAddresses());
		}
		if(timer == null){
			timer = new Timer();
			keepAlive = new NodeKeepAlive();
			timer.schedule(keepAlive,10000,keepAliveFrequencyInMillis);
		} 
	}*/
	
	public static Map<String, String> parseNodeDetails(String nodeDetails, String protocol) {
		Map<String, String> nodesMap = new HashMap<String, String>();
		StringTokenizer stringTok = new StringTokenizer(nodeDetails,",");
		
		String alias;
		String serverURL;
		while (stringTok.hasMoreElements()) {
			String token = (String)stringTok.nextElement();
			alias = token.substring(0, token.indexOf(':'));
			serverURL = token.substring(token.indexOf(':')+1);
			nodesMap.put( alias, protocol+"://"+serverURL);
			LOGGER.info("node alias "+ alias + "server url:" + serverURL);
		}
		return nodesMap;
		
	}
	
	@SuppressWarnings("rawtypes")
	@PostConstruct
	public void publishNodes() {
		publishingEnabled = this.nodeProfile.equalsIgnoreCase("batch".intern());
		if(!publishingEnabled){
			LOGGER.info("Publishing not enabled for this node profile :".intern()+publishingEnabled);
			return;
		}
		
		if(this.nodeDetails == null) {
			LOGGER.info("Node details not available in configuration.".intern());
			return;
		}
		deleteAllNodes();
		Map<String, String> nodesMap = parseNodeDetails(nodeDetails, protocol);
		
		Iterator it = nodesMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        Nodes tmp = new Nodes();
			tmp.setAlias((String)pair.getKey());
			tmp.setLastActive(new Timestamp(System.currentTimeMillis()));
			tmp.setEnabled(true);
			tmp.setIpAddresses("Not available");
			tmp.setBaseUrl((String)pair.getValue());
			if(!registerLocalNode(tmp)){
				throw new RuntimeException("Failed to register node: ".intern()+ pair.getValue().toString());
			}
	    }
	     
	}
	
	@SuppressWarnings("unchecked")
	public List<Nodes> getNodes(EntityManager em) {
		List<Nodes> result = em.createQuery("select node from Nodes node").getResultList();
		LOGGER.info("vish: getNodes "+ result.size());
		return result;
	}
	
	
	public void deleteAllNodes() {
		LOGGER.info("vish: deleteAllNodes called");
		
		EntityManager em = emf.createEntityManager();
		if(!em.getTransaction().isActive()){
			em.getTransaction().begin();
		}
		for(Nodes e : getNodes(em))
			em.remove(e);
		em.getTransaction().commit();
		em.clear();
		em.close();
	}
	
	private boolean registerLocalNode(Nodes node){
		boolean success = false;
		try{
			if(node != null) {
				EntityManager em = emf.createEntityManager();
				if(!em.getTransaction().isActive()){
					em.getTransaction().begin();
				}
				em.persist(node);
				em.getTransaction().commit();
				em.clear();
				em.close();
				success= true;
				LOGGER.info("Successfully registered node {}".intern(), node.getIpAddresses());
			}else {
				LOGGER.info("node is null, failed to register node." );
			}
			
		}catch(Exception e){
			LOGGER.error("Error registering node:".intern()+node.toJSONString(),e);
		}
		return success;
	}
	
	public Nodes getLocalNode(){
		if(this.localNode != null){
			return this.localNode;
		}
		// First check if this node exists in the DB
		EntityManager em = emf.createEntityManager();
		@SuppressWarnings("unchecked")
		List<Nodes> list = em.createQuery(
		        "SELECT node FROM Nodes node WHERE node.alias = :alias".intern())
		        .setParameter("alias".intern(), this.nodeAlias)
		        .getResultList();
		if(list == null || list.size() == 0){
			return null;
		}
		if(list.size() > 1){
			//We have 2 nodes configured with same name
			// Figure out which one is ours
			List<String> localIpAddresses = NetworkUtil.getLocalIpAddress();
			for(Nodes nodes: list){
				if(compare(nodes, localIpAddresses)){
					localNode= nodes;
				}
			}
		}
		if(localNode != null){
			return localNode;
		}
		localNode = list.get(0);
		em.clear();
		em.close();
		return list.get(0);
	}
	
	private void updateNodeStatus(){
		if(LOGGER.isTraceEnabled()){
			LOGGER.trace("Initiating node status update for {}".intern(),this.localNode.getIpAddresses());
		}
		EntityManager em = emf.createEntityManager();
		@SuppressWarnings("unchecked")
		List<Nodes> list = em.createQuery(
		        "SELECT node FROM Nodes node WHERE node.id = :id".intern())
		        .setParameter("id".intern(), this.localNode.getId())
		        .getResultList();
		if(list.size() != 1){
			throw new RuntimeException("Failed to find the node using id "+ this.localNode.getId());
		}
		if(!em.getTransaction().isActive()){
			em.getTransaction().begin();
		}
		Nodes tmp = list.get(0);
		tmp.setLastActive(new Timestamp(System.currentTimeMillis()));
		em.persist(tmp);
	//	em.flush();
		em.getTransaction().commit();
		em.clear();
		em.close();
		if(LOGGER.isTraceEnabled()){
			LOGGER.trace("Done Updating node status for node :"+this.localNode.getIpAddresses());
		}
		this.localNode = tmp;
	}
	
	private boolean compare(Nodes nodes, List<String> localIpAddresses) {
		String ips = nodes.getIpAddresses();
		for(String ip: localIpAddresses){
			if(ips.contains(ip)){
				return true;
			}
		}
		return false;
	}

	public String getNodeAlias() {
		return nodeAlias;
	}
	
	@Value("#{configProp['platform.nodes.node_alias'] != null ? configProp['platform.nodes.node_alias'] :'use_ip_address'}")
	public void setNodeAlias(String nodeAlias) {
		this.nodeAlias = nodeAlias;
	}

	public String getDefaultNodeAccessUrl() {
		return defaultNodeAccessUrl;
	}
	
	@Value("#{configProp['platform.nodes.defaultUrl'] != null ? configProp['platform.nodes.defaultUrl'] :'use_ip_address'}")
	public void setDefaultNodeAccessUrl(String defaultNodeAccessUrl) {
		this.defaultNodeAccessUrl = defaultNodeAccessUrl;
	}
	
	@Value("#{configProp['platform.nodes.node_details']}")
	public void setNodeDetails(String nodeDetails) {
		this.nodeDetails = nodeDetails;
	}
	
	@Value("#{configProp['platform.nodes.protocol'] != null ? configProp['platform.nodes.protocol'] :'http'}")
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getNodeProfile() {
		return nodeProfile;
	}
	@Value("#{configProp['platform.session_tracker.node_profile'] != null ? configProp['platform.session_tracker.node_profile'] :'not_available'}")
	public void setNodeProfile(String nodeProfile) {
		this.nodeProfile = nodeProfile;
	}

	@PreDestroy
	public void stopPublishing(){
		if(publishingEnabled && timer != null){
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Stopping status update for {}".intern(),this.localNode.getIpAddresses());
			}
			timer.cancel();
		}
	}
	
	private class NodeKeepAlive extends TimerTask {
		@Override
		public void run() {
			try {
					 updateNodeStatus();
				}
			catch (Exception e) {
				LOGGER.error("Exception occured updating the node keep alive".intern(), e);
			}
		}
	}

}
