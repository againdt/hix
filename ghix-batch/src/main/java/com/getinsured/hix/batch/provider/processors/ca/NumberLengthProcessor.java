package com.getinsured.hix.batch.provider.processors.ca;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;


public class NumberLengthProcessor implements ProviderDataFieldProcessor {

	private ValidationContext context = null;
	private Logger logger = LoggerFactory.getLogger(NumberLengthProcessor.class);
	private int index;
	@Override
	public Object process(Object objToBeValidated)
			throws ProviderValidationException {
		String s = null;
		String maxObj = null;
		String minObj = null;
		String length = context.getNamedConstraintField("length");
		try{
			s = objToBeValidated.toString();
			
			for(char c: s.toCharArray()){
				if(!Character.isDigit(c)){
					throw new ProviderValidationException("Input ["+s+"] not in number format");
				}
			}
			//First check if exact length is expected
			int sLen = s.length();
			if(length != null){
				int len = Integer.valueOf(length);
				if(sLen != len){
					throw new ProviderValidationException("Value length validation failed, Expected:"+len+" Found:"+s.length());
				}
			}else{
			// Now check if range validation is expected
				maxObj = context.getNamedConstraintField("max");
				minObj = context.getNamedConstraintField("min");
				try{
					if(maxObj != null){
						int max = Integer.valueOf(maxObj);
						if(sLen > max){
							throw new ProviderValidationException ("validation failed for maximum value [ max:"+max+"] found: "+sLen);
						}
					}
					if(minObj != null){
						int min = Integer.valueOf(minObj);
						if(sLen < min){
							throw new ProviderValidationException ("validation failed for minimum value [min:"+min+"] found: "+sLen);
						}
					}
				}catch(NumberFormatException ne){
					throw new ProviderValidationException("Illegal validation context", ne);
				}
			}
		}catch(ClassCastException ce){
			throw new ProviderValidationException("Invalid validation context provided expecting a number for \"length\"");
		}
		return s;
	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
		
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}
}
