package com.getinsured.hix.entity.admin.service;

import java.util.List;

import com.getinsured.hix.model.entity.Assister;

/**
 * Encapsulates service layer method calls for Entity Admin
 */
public interface AdminService {

	/**
	 * Finds the list of Assisters
	 * 
	 * @param assister
	 * 
	 * @return list of Assisters
	 */
	List<Assister> findAssister(Assister assister);
}
