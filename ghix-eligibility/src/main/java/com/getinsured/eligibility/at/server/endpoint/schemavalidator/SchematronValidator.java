package com.getinsured.eligibility.at.server.endpoint.schemavalidator;

import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferResponsePayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.util.ReferralUtil;

@Component
@DependsOn("platformConstants")
public class SchematronValidator {

	private static final Logger LOGGER = Logger.getLogger(SchematronValidator.class);
	private static TransformerFactory transformerFactory = new net.sf.saxon.TransformerFactoryImpl();
	private static StringWriter r3;
	private static StringWriter r3_out;
	
	//Added for TS functionality
	private static StringWriter r4;
	static StreamSource schemaSource ;
	static {
		
		InputStream outboundschemaStream = SchematronValidator.class.getResourceAsStream("/AccountTransfer-runtime_Outbound.sch");
		StreamSource schemaSource = new StreamSource(outboundschemaStream);
		try {
			StringWriter r1 = transformation("/iso_dsdl_include.xsl", schemaSource);
			StreamSource ir1 = sourceFromStringWriter(r1);
			StringWriter r2 = transformation("/iso_abstract_expand.xsl", ir1);
			StreamSource ir2 = sourceFromStringWriter(r2);
			r3_out = transformation("/iso_svrl_for_xslt2.xsl", ir2);

		} catch (TransformerException e) {
			LOGGER.error(e);
		}
	}

	static {
		if(!GhixPlatformConstants.TIMESHIFT_ENABLED){
			InputStream schemaStream = SchematronValidator.class.getResourceAsStream("/AccountTransfer-runtime.sch");
	
			 schemaSource = new StreamSource(schemaStream);
	
			try {
				StringWriter r1 = transformation("/iso_dsdl_include.xsl", schemaSource);
				StreamSource ir1 = sourceFromStringWriter(r1);
	
				StringWriter r2 = transformation("/iso_abstract_expand.xsl", ir1);
				StreamSource ir2 = sourceFromStringWriter(r2);
	
				r3 = transformation("/iso_svrl_for_xslt2.xsl", ir2);
	
			} catch (TransformerException e) {
				LOGGER.error(e);
			}
		}
	}
	
	static {
		if(GhixPlatformConstants.TIMESHIFT_ENABLED){
			InputStream schemaStream = SchematronValidator.class.getResourceAsStream("/AccountTransfer-TimeShift-runtime.sch");
	
			 schemaSource = new StreamSource(schemaStream);
	
			try {
				StringWriter r1 = transformation("/iso_dsdl_include.xsl", schemaSource);
				StreamSource ir1 = sourceFromStringWriter(r1);
	
				StringWriter r2 = transformation("/iso_abstract_expand.xsl", ir1);
				StreamSource ir2 = sourceFromStringWriter(r2);
	
				r4 = transformation("/iso_svrl_for_xslt2.xsl", ir2);
	
			} catch (TransformerException e) {
				LOGGER.error(e);
			}
		}
	}

	public static List<String> runSchematronValidationOutBound(String xml) throws TransformerException {

		StreamSource xmlSource = new StreamSource(IOUtils.toInputStream(xml));

		StreamSource ir3 = sourceFromStringWriter(r3_out);
		StringWriter r5 = transformation(ir3, xmlSource);
		String result = r5.toString();
		String[] outcome = StringUtils.substringsBetween(result, "<svrl:text>", "</svrl:text>");
		List<String> output = Collections.emptyList();
		if (outcome != null) {
			output = Arrays.asList(outcome);
		}
		return output;
	}

	public List<String> runSchematronValidation(String requestPayload) {

		List<String> output = new ArrayList<>();
		InputStream inputStream = null;
		try {
			String xml = requestPayload;
			if(!DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equals("NV") ){
				xml = ReferralUtil.extractSoapBoady(requestPayload);
			}

			inputStream = IOUtils.toInputStream(xml, "UTF-8");

			StreamSource xmlSource = new StreamSource(inputStream);

			StreamSource ir3  ;
			
			if(GhixPlatformConstants.TIMESHIFT_ENABLED){
				 ir3 = sourceFromStringWriter(r4);
			}else {
				 ir3 = sourceFromStringWriter(r3);	
			}
			 
			StringWriter r5 = transformation(ir3, xmlSource);
			String result = r5.toString();

			// System.out.println("Result dd - " + result);
			String[] outcome = StringUtils.substringsBetween(result, "<svrl:text>", "</svrl:text>");

			if (outcome != null) {
				for(String str:outcome) {
					output.add(str);
				}
			}

		} catch (Exception e) {
			LOGGER.error("Error reading incoming xml from  - ", e);
			output.add("Unable to parse element AccountTransferRequest - " + e.getMessage());
		} finally {
			IOUtils.closeQuietly(inputStream);
		}
		return output;

	}

	private static StringWriter transformation(String xsltFilePath, StreamSource inputSource) throws TransformerException {
		InputStream xsltStream = SchematronValidator.class.getResourceAsStream(xsltFilePath);
		StreamSource xsltSource = new StreamSource(xsltStream);
		return transformation(xsltSource, inputSource);
	}

	private static StringWriter transformation(StreamSource xsltSource, StreamSource inputSource) throws TransformerConfigurationException, TransformerException {
		Transformer transformer = transformerFactory.newTransformer(xsltSource);
		transformerFactory.setURIResolver(new ClasspathResourceURIResolvers());
		StringWriter writer = new StringWriter();
		StreamResult xmlResult = new StreamResult(writer);
		transformer.transform(inputSource, xmlResult);
		return writer;
	}

	private static StreamSource sourceFromStringWriter(StringWriter writer) {
		String string = writer.toString();
		StringReader reader = new StringReader(string);
		StreamSource source = new StreamSource(reader);
		return source;
	}

	private static final String AT_VERSION = "2.4";
	private static final String ONE_OR_MORE_RULES_FAILED_VALIDATION = "One or more rules failed validation";

	public AccountTransferResponsePayloadType formResponse(String responseValue, List<String> errorList) {

		AccountTransferResponsePayloadType response = new AccountTransferResponsePayloadType();
		response.setAtVersionText(AT_VERSION);

		ResponseMetadataType value = new ResponseMetadataType();
		TextType tt = new TextType();
		tt.setValue(responseValue);
		value.setResponseCode(tt);

		if (!errorList.isEmpty()) {
			TextType responseErrorDescrip = new TextType();
			responseErrorDescrip.setValue(ONE_OR_MORE_RULES_FAILED_VALIDATION);
			value.setResponseDescriptionText(responseErrorDescrip);

			List<TextType> tdsResponseDescriptionTextList = new ArrayList<>();

			for (String error : errorList) {
				TextType errorDescrip = new TextType();
				errorDescrip.setValue(error);
				tdsResponseDescriptionTextList.add(errorDescrip);
			}

			value.getTDSResponseDescriptionText().addAll(tdsResponseDescriptionTextList);
		}

		response.setResponseMetadata(value);
		return response;
	}

}

class ClasspathResourceURIResolvers implements URIResolver {
	@Override
	public Source resolve(String href, String base) throws TransformerException {
		return new StreamSource(SchematronValidator.class.getClassLoader().getResourceAsStream(href));
	}
}
