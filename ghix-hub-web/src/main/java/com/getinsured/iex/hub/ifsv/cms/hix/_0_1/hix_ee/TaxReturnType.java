//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.02.27 at 09:16:54 AM IST 
//


package com.getinsured.iex.hub.ifsv.cms.hix._0_1.hix_ee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ifsv.cms.hix._0_1.hix_types.TaxReturnFilingStatusCodeType;
import com.getinsured.iex.hub.ifsv.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.hub.ifsv.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.hub.ifsv.niem.niem.proxy.xsd._2.GYear;
import com.getinsured.iex.hub.ifsv.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for a tax return as filed with the Internal Revenue Service.
 * 
 * <p>Java class for TaxReturnType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaxReturnType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}PrimaryTaxFiler"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}SpouseTaxFiler" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}TaxReturnYear" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}TaxReturnFilingStatusCode" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}TaxReturnAGIAmount" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}TaxReturnMAGIAmount" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}TaxReturnTaxableSocialSecurityBenefitsAmount" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}TaxReturnTotalExemptionsQuantity" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxReturnType", propOrder = {
    "primaryTaxFiler",
    "spouseTaxFiler",
    "taxReturnYear",
    "taxReturnFilingStatusCode",
    "taxReturnAGIAmount",
    "taxReturnMAGIAmount",
    "taxReturnTaxableSocialSecurityBenefitsAmount",
    "taxReturnTotalExemptionsQuantity"
})
public class TaxReturnType
    extends ComplexObjectType
{

    @XmlElement(name = "PrimaryTaxFiler", required = true)
    protected TaxFilerType primaryTaxFiler;
    @XmlElement(name = "SpouseTaxFiler")
    protected TaxFilerType spouseTaxFiler;
    @XmlElement(name = "TaxReturnYear")
    protected GYear taxReturnYear;
    @XmlElement(name = "TaxReturnFilingStatusCode")
    protected TaxReturnFilingStatusCodeType taxReturnFilingStatusCode;
    @XmlElement(name = "TaxReturnAGIAmount")
    protected AmountType taxReturnAGIAmount;
    @XmlElement(name = "TaxReturnMAGIAmount")
    protected AmountType taxReturnMAGIAmount;
    @XmlElement(name = "TaxReturnTaxableSocialSecurityBenefitsAmount")
    protected AmountType taxReturnTaxableSocialSecurityBenefitsAmount;
    @XmlElement(name = "TaxReturnTotalExemptionsQuantity")
    protected QuantityType taxReturnTotalExemptionsQuantity;

    /**
     * Gets the value of the primaryTaxFiler property.
     * 
     * @return
     *     possible object is
     *     {@link TaxFilerType }
     *     
     */
    public TaxFilerType getPrimaryTaxFiler() {
        return primaryTaxFiler;
    }

    /**
     * Sets the value of the primaryTaxFiler property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxFilerType }
     *     
     */
    public void setPrimaryTaxFiler(TaxFilerType value) {
        this.primaryTaxFiler = value;
    }

    /**
     * Gets the value of the spouseTaxFiler property.
     * 
     * @return
     *     possible object is
     *     {@link TaxFilerType }
     *     
     */
    public TaxFilerType getSpouseTaxFiler() {
        return spouseTaxFiler;
    }

    /**
     * Sets the value of the spouseTaxFiler property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxFilerType }
     *     
     */
    public void setSpouseTaxFiler(TaxFilerType value) {
        this.spouseTaxFiler = value;
    }

    /**
     * Gets the value of the taxReturnYear property.
     * 
     * @return
     *     possible object is
     *     {@link GYear }
     *     
     */
    public GYear getTaxReturnYear() {
        return taxReturnYear;
    }

    /**
     * Sets the value of the taxReturnYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link GYear }
     *     
     */
    public void setTaxReturnYear(GYear value) {
        this.taxReturnYear = value;
    }

    /**
     * Gets the value of the taxReturnFilingStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link TaxReturnFilingStatusCodeType }
     *     
     */
    public TaxReturnFilingStatusCodeType getTaxReturnFilingStatusCode() {
        return taxReturnFilingStatusCode;
    }

    /**
     * Sets the value of the taxReturnFilingStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxReturnFilingStatusCodeType }
     *     
     */
    public void setTaxReturnFilingStatusCode(TaxReturnFilingStatusCodeType value) {
        this.taxReturnFilingStatusCode = value;
    }

    /**
     * Gets the value of the taxReturnAGIAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getTaxReturnAGIAmount() {
        return taxReturnAGIAmount;
    }

    /**
     * Sets the value of the taxReturnAGIAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setTaxReturnAGIAmount(AmountType value) {
        this.taxReturnAGIAmount = value;
    }

    /**
     * Gets the value of the taxReturnMAGIAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getTaxReturnMAGIAmount() {
        return taxReturnMAGIAmount;
    }

    /**
     * Sets the value of the taxReturnMAGIAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setTaxReturnMAGIAmount(AmountType value) {
        this.taxReturnMAGIAmount = value;
    }

    /**
     * Gets the value of the taxReturnTaxableSocialSecurityBenefitsAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getTaxReturnTaxableSocialSecurityBenefitsAmount() {
        return taxReturnTaxableSocialSecurityBenefitsAmount;
    }

    /**
     * Sets the value of the taxReturnTaxableSocialSecurityBenefitsAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setTaxReturnTaxableSocialSecurityBenefitsAmount(AmountType value) {
        this.taxReturnTaxableSocialSecurityBenefitsAmount = value;
    }

    /**
     * Gets the value of the taxReturnTotalExemptionsQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link QuantityType }
     *     
     */
    public QuantityType getTaxReturnTotalExemptionsQuantity() {
        return taxReturnTotalExemptionsQuantity;
    }

    /**
     * Sets the value of the taxReturnTotalExemptionsQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityType }
     *     
     */
    public void setTaxReturnTotalExemptionsQuantity(QuantityType value) {
        this.taxReturnTotalExemptionsQuantity = value;
    }

}
