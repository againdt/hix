<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld"
	prefix="util"%>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/shopstyles.css"/>" />

<!--titlebar-->
<div class="row-fluid">
	<div class="span3" id="sidebar">
		<div class="accordion graysidebar" id="accordion2">
			<util:nav pageName="${page_name}"></util:nav>
		</div>
		<util:selection pageName="${page_name}"></util:selection>
	</div>
	<!--sidebar-->
	<div class="span9" id="rightpanel">
		<div class="row-fluid" id="titlebar">
			<div class="span3 "></div>
			<div class="span9">
				<h3 class="pull-left">Congratulations</h3>
			</div>
		</div>
		<div class="gutter10">
			<p>Now that you have selected your plans and locked in your costs as an employer, you're ready to start the open enrollment process.
				During open enrollment, your employees will verify their personal information and choose the specific health care plans to enroll in. 
			</p>
		</div>
		<!--gutter10-->
	</div>
	<!--rightpanel-->
</div>
<!--row-fluid-->




