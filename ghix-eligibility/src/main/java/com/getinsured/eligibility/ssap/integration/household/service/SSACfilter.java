package com.getinsured.eligibility.ssap.integration.household.service;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


public class SSACfilter {
    private static Set<String> allowedKeys = new HashSet<>();

    static {
        allowedKeys.add("personId");
        allowedKeys.add("name");
        allowedKeys.add("dateOfBirth");
        allowedKeys.add("socialSecurityCard");
        allowedKeys.add("citizenshipImmigrationStatus");
        allowedKeys.add("incarcerationStatus");
    }

    public static JSONArray filterForSSAC(JSONArray updatedtaxHouseHolds) {
        Iterator iterator = updatedtaxHouseHolds.iterator();
        JSONObject household;
        while (iterator.hasNext()) {
            household = (JSONObject) iterator.next();
            Set<String> keys = household.keySet();
            keys.retainAll(allowedKeys);
            Iterator<String> householdKeysIterator = keys.iterator();
            while(householdKeysIterator.hasNext()){
                String elem = householdKeysIterator.next();
                if(!keys.contains(elem)) {
                    household.remove(elem);
                }
            }
        }
        return updatedtaxHouseHolds;
    }

    public static JSONObject filterMemberAttributesForSSAC(JSONObject householdMember) {
            Set<String> keys = householdMember.keySet();
            keys.retainAll(allowedKeys);
            Iterator<String> householdKeysIterator = keys.iterator();
            while(householdKeysIterator.hasNext()){
                String elem = householdKeysIterator.next();
                if(!keys.contains(elem)) {
                    householdMember.remove(elem);
                }
            }
        return householdMember;
    }
}
