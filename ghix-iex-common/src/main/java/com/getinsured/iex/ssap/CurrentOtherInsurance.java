
package com.getinsured.iex.ssap;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 */
public class CurrentOtherInsurance {

  @Expose
  private Boolean peaceCorpsIndicator = false;

  @Expose
  private Boolean tricareEligibleIndicator = false;

  @Expose
  private Boolean nonofTheAbove;

  @Expose
  private Boolean medicareEligibleIndicator = false;

  @Expose
  private Boolean otherStateOrFederalProgramIndicator = false;

  /**
   * List of other state or federal program types.
   */
  @Expose
  private List<OtherStateOrFederalProgram> otherStateOrFederalPrograms = new ArrayList<>();

  /**
   * Has non employer sponsored insurance coverage.
   */
  @Expose
  private boolean hasNonESI;

  public Boolean getPeaceCorpsIndicator() {
    return peaceCorpsIndicator;
  }

  public void setPeaceCorpsIndicator(Boolean peaceCorpsIndicator) {
    this.peaceCorpsIndicator = peaceCorpsIndicator;
  }

  public Boolean getTricareEligibleIndicator() {
    return tricareEligibleIndicator;
  }

  public void setTricareEligibleIndicator(Boolean tricareEligibleIndicator) {
    this.tricareEligibleIndicator = tricareEligibleIndicator;
  }

  public Boolean getNonofTheAbove() {
    return nonofTheAbove;
  }

  public void setNonofTheAbove(Boolean nonofTheAbove) {
    this.nonofTheAbove = nonofTheAbove;
  }

  public Boolean getMedicareEligibleIndicator() {
    return medicareEligibleIndicator;
  }

  public void setMedicareEligibleIndicator(Boolean medicareEligibleIndicator) {
    this.medicareEligibleIndicator = medicareEligibleIndicator;
  }

  public Boolean getOtherStateOrFederalProgramIndicator() {
    return otherStateOrFederalProgramIndicator;
  }

  public void setOtherStateOrFederalProgramIndicator(Boolean otherStateOrFederalProgramIndicator) {
    this.otherStateOrFederalProgramIndicator = otherStateOrFederalProgramIndicator;
  }

  /**
   * Returns list of other state or federal program types.
   * @return list of other state or federal program types.
   */
  public List<OtherStateOrFederalProgram> getOtherStateOrFederalPrograms() {
    return this.otherStateOrFederalPrograms;
  }

  /**
   * Sets other state or federal program types.
   * @param otherStateOrFederalPrograms list of other state or federal program types.
   */
  public void setOtherStateOrFederalPrograms(List<OtherStateOrFederalProgram> otherStateOrFederalPrograms) {
    this.otherStateOrFederalPrograms = otherStateOrFederalPrograms;
  }

  public boolean isHasNonESI() {
    return hasNonESI;
  }

  public void setHasNonESI(boolean hasNonESI) {
    this.hasNonESI = hasNonESI;
  }
}
