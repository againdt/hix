package com.getinsured.hix.batch.plandisplay.exceptions;

public class SolrNotAvailableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SolrNotAvailableException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SolrNotAvailableException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SolrNotAvailableException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SolrNotAvailableException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SolrNotAvailableException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
