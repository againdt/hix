var householdSSNIndicationValue = "";

function validation(currentPage1)
{
	
	var flag = true;
	var idFocus = "";

	
	emptyLabels();
	
	
	if(currentPage1 == 'appscr51' &&  document.getElementById('acceptanceCB').checked == false)
		{
		$('#acceptanceCB').addClass("validateColor");
		addLabels(" accept the agreement !", "acceptanceCB");
		idFocus = "acceptanceCB";
	
		
		flag = false;
		}
	else if(currentPage1 == "appscr53")															//appscr53 Page Validation 		
	{
		/*var alphaExp = /^[a-zA-Z]+$/;
		//First Name validation
		var name = $('#firstName').val();
		if($('#firstName').val()=="" || !(name.match(alphaExp))) 	
		{	
			$('#firstName').addClass("validateColor");
			addLabels("Enter First Name", "firstName");
			idFocus = "firstName";
			flag = false;
		}
	
		$('#dateOfBirth').parsley('validate');
		if($('#dateOfBirth').parsley('isValid')==false){
			flag = false;
		}
		
		
		//Date of Birth Validation
		if($('#dateOfBirth').val()=="" || checkDate($('#dateOfBirth').val())==false)
		{	
			$('#dateOfBirth').addClass("validateColor");
			addLabels("Enter Date of Birth in MM/DD/YYYY Format", "dateOfBirth");
			if(idFocus == "")
				idFocus = "dateOfBirth";
			flag = false;
		}
		
		//Email ID Validation
		if($('#emailAddress').val()=="" || checkEmail($('#emailAddress').val())==false)
		{	
			$('#emailAddress').addClass("validateColor");
			addLabels("Enter Email address in \"example@gmail.com\" Format", "emailAddress");
			if(idFocus == "")
				idFocus = "emailAddress";
			flag = false;
		}
		
		//Home Address 1 Validation	
		if(!($('#homeAddressIndicator').is(":checked")))
		{
			if($('#home_addressLine1').val()=="")
			{
				$('#home_addressLine1').addClass("validateColor");
				addLabels("Enter Home address line 1", "home_addressLine1");
				if(idFocus == "")
					idFocus = "home_addressLine1";
				flag = false;
			}
		}
		else
		{
			$('#home_addressLine1').removeClass("validateColor");
		}
		
		//Mailing Address Validation
		if($('#mailingAddressIndicator').is(":checked"))
		{
			
		}	
		
		//Home Address City Validation		
		if($('#home_primary_city').val()=="")
		{
			$('#home_primary_city').addClass("validateColor");
			addLabels("Enter City", "home_primary_city");
			if(idFocus == "")
				idFocus = "home_primary_city";
			flag = false;
		}
		
		//Home Address Zip Validation		
		if($('#home_primary_zip').val()=="")
		{
			$('#home_primary_zip').addClass("validateColor");
			addLabels("Enter Zip Code", "home_primary_zip");
			if(idFocus == "")
				idFocus = "home_primary_zip";
			flag = false;
		}
		
		//Home Address State Validation		
		if($('#home_primary_state').val()=="")
		{
			$('#home_primary_state').addClass("validateColor");
			addLabels("Enter State", "home_primary_state");
			if(idFocus == "")
				idFocus = "home_primary_state";
			flag = false;
		}
		
		//Home Address Country Validation		
		if($('#home_primary_county').val()=="")
		{
			$('#home_primary_county').addClass("validateColor");
			addLabels("Enter County", "home_primary_county");
			if(idFocus == "")
				idFocus = "home_primary_county";
			flag = false;
		}
		
		//Home Address Mailing State Validation		
		if($('#mailing_primary_state').val()=="")
		{
			$('#mailing_primary_state').addClass("validateColor");
			addLabels("Enter Mailing State", "mailing_primary_state");
			if(idFocus == "")
				idFocus = "mailing_primary_state";
			flag = false;
		}*/
				
	}			
	else if(currentPage1 == "appscr54")                     								//appscr54 Page Validation       
	{
		
		/*need to uncomment after demo on 23 May*/
		/*$('input[name=authorizedRepresentativeIndicator]:radio').parsley('validate');
		if($('input[name=authorizedRepresentativeIndicator]:radio').parsley('isValid')==false){
			flag = false;
		}*/
		
		if($('input[name=authorizedRepresentativeIndicator]:radio:checked').val() == "yes")
		{
			if($('#authorizedFirstName').val()=="")
			{
				$('#authorizedFirstName').addClass("validateColor");
			
				if(idFocus == "")
					idFocus = "authorizedFirstName";
				flag = false;
			}
			
			if($('#authorizedLastName').val()=="")
			{
				$('#authorizedLastName').addClass("validateColor");
			
				if(idFocus == "")
					idFocus = "authorizedLastName";
				flag = false;
			}
			
			if($('#authorizedEmailAddress').val()=="" || checkEmail($('#authorizedEmailAddress').val())==false)
			{
				$('#authorizedEmailAddress').addClass("validateColor");
				
				if(idFocus == "")
					idFocus = "authorizedEmailAddress";
				flag = false;
			}
			
			if($('#authorizedAddress1').val()=="")
			{
				$('#authorizedAddress1').addClass("validateColor");
			
				if(idFocus == "")
					idFocus = "authorizedAddress1";
				flag = false;
			}
			
			if($('#authorizedCity').val()=="")
			{
				$('#authorizedCity').addClass("validateColor");
				
				if(idFocus == "")
					idFocus = "authorizedCity";
				flag = false;
			}
			
			if($('#authorizedZip').val()=="")
			{
				$('#authorizedZip').addClass("validateColor");
			
				if(idFocus == "")
					idFocus = "authorizedZip";
				flag = false;
			}
			
			if($('#authorizedState').val()=="0")
			{
				$('#authorizedState').addClass("validateColor");
			
				if(idFocus == "")
					idFocus = "authorizedState";
				flag = false;
			}
			
			if($('input[name=isPersonHelpingyou4ApplingHealthInsurance]:radio:checked').val() == "Yes")
			{
				if($('#authorizeCompanyName').val()=="")
				{
					$('#authorizeCompanyName').addClass("validateColor");
					
					if(idFocus == "")
						idFocus = "authorizeCompanyName";
					flag = false;
				}
				
				if($('#authorizeOrganizationId').val()=="")
				{
					$('#authorizeOrganizationId').addClass("validateColor");
				
					if(idFocus == "")
						idFocus = "authorizeOrganizationId";
					flag = false;
				}
			}
			
			if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == "Signature")
			{
				if($('#authorizeSignature').val()=="")
				{
					$('#authorizeSignature').addClass("validateColor");
				
					if(idFocus == "")
						idFocus = "authorizeSignature";
					flag = false;
				}
			}
			
		}
		else
		{
			$('#authorizedFirstName').removeClass("validateColor");
			$('#authorizedLastName').removeClass("validateColor");
			$('#authorizedEmailAddress').removeClass("validateColor");
			$('#authorizedAddress1').removeClass("validateColor");
			$('#authorizedCity').removeClass("validateColor");
			$('#authorizedZip').removeClass("validateColor");
			$('#authorizedState').removeClass("validateColor");
			$('#authorizeCompanyName').removeClass("validateColor");
			$('#authorizeOrganizationId').removeClass("validateColor");
			$('#authorizeSignature').removeClass("validateColor");
		}
		
	}
	else if(currentPage1 == "appscr55")
	{	
		$('#get-help-with-costs-form').parsley('validate');
		if($('#get-help-with-costs-form').parsley('isValid')==false){
			flag = false;
		}
		
		if($('#noOfPeople53').val()=="")
			{	
				$('#noOfPeople53').addClass("validateColor");
			
				if(idFocus == "")
					idFocus = "noOfPeople53";
				flag = false;
			}
		
		
	}		
	else if(currentPage1 == "appscr57")                    								//appscr57 Page Validation   
	{
		
		
		//Applicants applying for Health Insurance Validation
		var noOfHouseHold = $('#numberOfHouseHold').text();
		for(var i = 1;i<=noOfHouseHold;i++)
		{
			
			$('#appscr57FirstName'+i).parsley('validate');
			if($('#appscr57FirstName'+i).parsley('isValid')==false){
				flag = false;
			}
			
			$('#appscr57LastName'+i).parsley('validate');
			if($('#appscr57LastName'+i).parsley('isValid')==false){
				flag = false;
			}
			
			$('#appscr57DOB'+i).parsley('validate');
			if($('#appscr57DOB'+i).parsley('isValid')==false){
				flag = false;
			}
			
			//if(!$('input[name=appscr57Tobacco'+i+']:radio').is(':checked')){
				$('input[name=appscr57Tobacco'+i+']:radio').parsley('validate');
				if($('input[name=appscr57Tobacco'+i+']:radio').parsley('isValid')==false){
					flag = false;
				}
			//}
			
		}
	}			
	else if(currentPage1 == "appscr59A"){
		//console.log('valid for 59');
	}
	else if(currentPage1 == "appscr60")                    											//appscr60 Page Validation    
	{
		
		$('#houseHoldQuestion input').parsley('validate');
		if($('#houseHoldQuestion input').parsley('isValid')==false){
			flag = false;
		}
		
		if($('#appscrIsChildDiv').is(':visible') && $('#willClaimDependents').is(':visible') && $('#willBeClaimed').is(':visible') && $('#householdSpouseMainDiv').is(':visible')){
			//$('#appscrIsChildDiv input').parsley('validate');
			$('#willClaimDependents input').parsley('validate');
			if($('#willClaimDependents input').parsley('isValid')==false){
				flag = false;
			}
			
			$('#willBeClaimed input').parsley('validate');
			if($('#willBeClaimed input').parsley('isValid')==false){
				flag = false;
			}
			
			$('input[name=liveWithSpouseIndicator]').parsley('validate');
			if($('input[name=liveWithSpouseIndicator]').parsley('isValid')==false){
				flag = false;
			}
			
			$('input[name=marriedIndicator]').parsley('validate');
			if($('input[name=marriedIndicator]').parsley('isValid')==false){
				flag = false;
			}
			
			$('input[name=planOnFilingJointFTRIndicator]').parsley('validate');
			if($('input[name=planOnFilingJointFTRIndicator]').parsley('isValid')==false){
				flag = false;
			}
			
			$('input[name=householdContactSpouse]').parsley('validate');
			if($('input[name=householdContactSpouse]').parsley('isValid')==false){
				flag = false;
			}
		}
		
		if($('#appscr60ClaimedFilerDiv').is(':visible')){
			$('input[name=householdContactFiler]').parsley('validate');
			if($('input[name=householdContactFiler]').parsley('isValid')==false){
				flag = false;
			}
		}
		
		
	///// Changes start
		
		//Tell Us About Your Household Spouse First Name 
		if($('input[name=householdContactSpouse]:radio:checked').val() == "someOne" && $('input[name=marriedIndicator]:radio:checked').val() == "yes")
		{
			if($('#spouseFirstName').val()=="")
			{
				$('#spouseFirstName').addClass("validateColor");
				$('#parsley-appscr60-s-fn').show();
				if(idFocus == "")
					idFocus = "spouseFirstName";
				flag = false;
			}	
						
			//Tell Us About Your Household Spouse Last Name 
			if($('#spouseLastName').val()=="")
			{
				$('#spouseLastName').addClass("validateColor");
				$('#parsley-appscr60-s-ln').show();
				if(idFocus == "")
					idFocus = "spouseLastName";
				flag = false;
			}
			
			//Tell Us About Your Household Spouse Date Of Birth
			if($('#spouseDOB').val()=="" || checkDate($('#spouseDOB').val())==false)
			{
				$('#spouseDOB').addClass("validateColor");
				$('#parsley-appscr60-s-dob').show();
				if(idFocus == "")
					idFocus = "spouseDOB";
				flag = false;
			}
		}
		else
		{
			$('#spouseFirstName').removeClass("validateColor");
			$('#spouseMiddleName').removeClass("validateColor");
			$('#spouseLastName').removeClass("validateColor");
			$('#spouseDOB').removeClass("validateColor");
		}
		
	
		if($('input[name=householdContactFiler]:radio:checked').val() == "someOne" && $('input[name=doesApplicantHasTaxFiller]:radio:checked').val() == "yes")
		{			
			//Tell Us About Your Household Tax_Filer First Name
			if($('#taxFilerFirstName').val()=="")
			{
				$('#taxFilerFirstName').addClass("validateColor");
				$('#parsley-appscr60-tf-fn').show();
				//addLabels('Enter the tex filer First Name', "taxFilerName60A_firstName");
				if(idFocus == "")
					idFocus = "taxFilerFirstName";
				flag = false;
			}
			else if($('#taxFilerFirstName').val()!="")
				$('#parsley-appscr60-tf-fn').hide();
			
			//Tell Us About Your Household Tax_Filer Last Name
						
			if($('#taxFilerLastName').val()=="")
			{
				$('#taxFilerLastName').addClass("validateColor");
				$('#parsley-appscr60-tf-ln').show();
				
				if(idFocus == "")
					idFocus = "taxFilerLastName";
				flag = false;
			}
			else if($('#taxFilerLastName').val()!="")
				$('#parsley-appscr60-tf-ln').hide();
			
			//Tell Us About Your Household Tax_Filer  Date Of Birth
			if($('#taxFilerDob').val()=="" || checkDate($('#taxFilerDob').val())==false)
			{
				$('#taxFilerDob').addClass("validateColor");
				$('#parsley-appscr60-tf-dob').show();
			
				if(idFocus == "")
					idFocus = "taxFilerDob";
				flag = false;
			}
			else if($('#taxFilerDob').val()=="" && checkDate($('#taxFilerDob').val())==true)
				$('#parsley-appscr60-tf-dob').hide();
		}
		else
		{
			$('#taxFilerFirstName').removeClass("validateColor");
			$('#taxFilerMiddleName').removeClass("validateColor");
			$('#taxFilerLastName').removeClass("validateColor");
			$('#taxFilerDob').removeClass("validateColor");
			
			
		}
		
		
		
		if($('#householdContactDependantSomeOneChecked').is(":checked") && $('input[name=householdHasDependant]:radio:checked').val() == "yes")
		{			
			//Tell Us About Your Household Tax_Filer First Name
			if($('#dependentFirstName').val()=="")
			{
				$('#dependentFirstName').addClass("validateColor");
				$('#parsley-appscr60-dd-fn').show();
				//addLabels('Enter the tex filer First Name', "taxFilerName60A_firstName");
				if(idFocus == "")
					idFocus = "dependentFirstName";
				flag = false;
			}
			else if($('#dependentFirstName').val()!="")
				$('#parsley-appscr60-dd-fn').hide();
			
			//Tell Us About Your Household Tax_Filer Last Name
						
			if($('#dependentLastName').val()=="")
			{
				$('#dependentLastName').addClass("validateColor");
				$('#parsley-appscr60-dd-ln').show();
				
				if(idFocus == "")
					idFocus = "dependentLastName";
				flag = false;
			}
			else if($('#dependentLastName').val()!="")
				$('#parsley-appscr60-dd-ln').hide();
			
			//Tell Us About Your Household Tax_Filer  Date Of Birth
			if($('#dependentDOB').val()=="" || checkDate($('#taxFilerDob').val())==false)
			{
				$('#dependentDOB').addClass("validateColor");
				$('#parsley-appscr60-dd-dob').show();
			
				if(idFocus == "")
					idFocus = "dependentDOB";
				flag = false;
			}
			else if($('#dependentDOB').val()=="" && checkDate($('#dependentDOB').val())==true)
				$('#parsley-appscr60-dd-dob').hide();
		}
		else
		{
			$('#dependentFirstName').removeClass("validateColor");
			
			$('#dependentLastName').removeClass("validateColor");
			$('#dependentDOB').removeClass("validateColor");
			
			
		}
		
		
	}	
	
	////// Changes end, made on March 4, due to bad parsley functionality
	
	
	else if(currentPage1 == "appscr61")                    										//appscr61 Page Validation     		
	{	
		$('input[name=appscr61_gender]').parsley('validate');
		if($('input[name=appscr61_gender]').parsley('isValid')==false){
			flag = false;
		}
		
		$('input[name=socialSecurityCardHolderIndicator]').parsley('validate');
		if($('input[name=socialSecurityCardHolderIndicator]').parsley('isValid')==false){
			flag = false;
		}
		
		if($('#DontaskAboutSSNOnPage61').is(':visible')){
			$('#reasonableExplanationForNoSSN').parsley('validate');
			if($('#reasonableExplanationForNoSSN').parsley('isValid')==false){
				flag = false;
			}
		}
		
		if($('input:radio[name=socialSecurityCardHolderIndicator]:checked').val() == "yes")
		{			
			/*if($('#ssn1').val()=="")
			{
				$('#ssn1').addClass("validateColor");
				addLabels('Enter the SSN Number', "ssn1");
				if(idFocus == "")
					idFocus = "ssn1";
				    flag = false;
			}
			if($('#ssn2').val()=="")
			{
				$('#ssn2').addClass("validateColor");
				addLabels('Enter the SSN Number', "ssn2");
				if(idFocus == "")
					idFocus = "ssn2";
				flag = false;
			}
			
			if($('#ssn3').val()=="")
			{
				$('#ssn3').addClass("validateColor");
				addLabels('Enter the SSN Number', "ssn3");
				if(idFocus == "")
					idFocus = "ssn3";
				flag = false;
			}*/
			
			$('#ssn1').parsley('validate');
			if($('#ssn1').parsley('isValid')==false){
				flag = false;
			}
			
			$('#ssn2').parsley('validate');
			if($('#ssn2').parsley('isValid')==false){
				flag = false;
			}
			
			$('#ssn3').parsley('validate');
			if($('#ssn3').parsley('isValid')==false){
				flag = false;
			}
			
			$('input[name=fnlnsSame]').parsley('validate');
			if($('input[name=fnlnsSame]').parsley('isValid')==false){
				flag = false;
			}
		}
		/*else
		{
			$('#ssn1').removeClass("validateColor");
			$('#ssn2').removeClass("validateColor");
			$('#ssn3').removeClass("validateColor");
		}*/
		
		if($('input:radio[name=socialSecurityCardHolderIndicator]:checked').val() == "yes" && $('input[name=fnlnsSame]:radio:checked').val() == "no")
		{
			$('#firstNameOnSSNCard').parsley('addConstraint', {required: true });
			$('#firstNameOnSSNCard').parsley('validate');
			if($('#firstNameOnSSNCard').parsley('isValid')==false){
				flag = false;
			}
			
			$('#lastNameOnSSNCard').parsley('addConstraint', {required: true });
			$('#lastNameOnSSNCard').parsley('validate');
			if($('#lastNameOnSSNCard').parsley('isValid')==false){
				flag = false;
			}
		}else{
			$('#lastNameOnSSNCard').parsley('removeConstraint', 'required');
			$('#firstNameOnSSNCard').parsley('validate');
			$('#firstNameOnSSNCard').parsley('removeConstraint', 'required');
			$('#lastNameOnSSNCard').parsley('validate');
		}
		
		
		/*if($('input[name=socialSecurityCardHolderIndicator]:radio:checked').val() == 'no')
		{
			$('#exemptionId').parsley('addConstraint', {required: true });
			$('#exemptionId').parsley('validate');
			if($('#exemptionId').parsley('isValid')==false){
				flag = false;
			}
			
			
		}*/
		
		/*if($('#DontaskAboutSSNOnPage61').is(':visible')){
			$('#reasonableExplanationForNoSSN').parsley('addConstraint', {required: true });
			$('#reasonableExplanationForNoSSN').parsley('validate');
			if($('#reasonableExplanationForNoSSN').parsley('isValid')==false){
				flag = false;
			}
		}*/
		
	}		
	else if(currentPage1 =="appscr62Part1")/*appscr62Part1 Page Validation*/{
		/*first question*/
		$('input[name=UScitizen]').parsley('validate');
		if($('input[name=UScitizen]').parsley('isValid')==false){
			flag = false;
		}
		/*naturalized citizen question*/
		if($('#naturalizedCitizenDiv').is(':visible')){
			$('#naturalizedCitizenDiv input').parsley('validate');
			if($('#naturalizedCitizenDiv input').parsley('isValid')==false){
				flag = false;
			}
		}
		
		/*document type*/
		if($('#documentTypeDiv').is(':visible')){
			$('input[name=documentType]').parsley('validate');
			if($('input[name=documentType]').parsley('isValid')==false){
				flag = false;
			}
		}
		
		/*naturalized certificate input textbox when visible*/
		if($('#divNaturalizationCertificate').is(':visible')){
			$('#naturalizationAlignNumber').parsley('validate');
			if($('#naturalizationAlignNumber').parsley('isValid')==false){
				flag = false;
			}
			
			$('#naturalizationCertificateNumber').parsley('validate');
			if($('#naturalizationCertificateNumber').parsley('isValid')==false){
				flag = false;
			}
		}
		
		/*citizenship certificate textbox when visible*/
		if($('#divCitizenshipCertificate').is(':visible')){
			$('#citizenshipAlignNumber').parsley('validate');
			if($('#citizenshipAlignNumber').parsley('isValid')==false){
				flag = false;
			}
			
			$('#appscr62p1citizenshipCertificateNumber').parsley('validate');
			if($('#appscr62p1citizenshipCertificateNumber').parsley('isValid')==false){
				flag = false;
			}
		}
		
		
		/*eligibleImmigrationStatus when NO is selected from naturalized citizen*/
		if($('#eligibleImmigrationStatus').is(':visible')){
			$('input[name=docType]').parsley('validate');
			if($('input[name=docType]').parsley('isValid')==false){
				flag = false;
			}
			
			$('input[name=livesInUS]').parsley('validate');
			if($('input[name=livesInUS]').parsley('isValid')==false){
				flag = false;
			}
			
			$('input[name=honourably-veteran]').parsley('validate');
			if($('input[name=honourably-veteran]').parsley('isValid')==false){
				flag = false;
			}
			
		}
		
		/*options of 1st radio button*/
		if($('#permcarddetailsdiv').is(':visible')){
			$('#permcarddetailsAlignNumber').parsley('validate');
			if($('#permcarddetailsAlignNumber').parsley('isValid')==false){
				flag = false;
			}
			
			$('#permcarddetailsppcardno').parsley('validate');
			if($('#permcarddetailsppcardno').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#permcarddetailsdateOfExp').parsley('validate');
			if($('#permcarddetailsdateOfExp').parsley('isValid')==false){
				flag = false;
			}*/
		}
		
		/*options of 2nd radio button*/
		if($('#tempcarddetailsdiv').is(':visible')){
			$('#tempcarddetailsAlignNumber').parsley('validate');
			if($('#tempcarddetailsAlignNumber').parsley('isValid')==false){
				flag = false;
			}
			
			$('#tempcarddetailsppcardno').parsley('validate');
			if($('#tempcarddetailsppcardno').parsley('isValid')==false){
				flag = false;
			}
			$('#tempcarddetailsCounty').parsley('validate');
			if($('#tempcarddetailsCounty').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#tempcarddetailsPassportexpdate').parsley('validate');
			if($('#tempcarddetailsPassportexpdate').parsley('isValid')==false){
				flag = false;
			}*/
		}
		
		/*options of 3rd radio button*/
		if($('#machinecarddetailsdiv').is(':visible')){
			$('#machinecarddetailsAlignNumber').parsley('validate');
			if($('#machinecarddetailsAlignNumber').parsley('isValid')==false){
				flag = false;
			}
			
			$('#machinecarddetailsppcardno').parsley('validate');
			if($('#machinecarddetailsppcardno').parsley('isValid')==false){
				flag = false;
			}
			
			$('#machinecarddetailsCounty').parsley('validate');
			if($('#machinecarddetailsCounty').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#machinecarddetailsppvisano').parsley('validate');
			if($('#machinecarddetailsppvisano').parsley('isValid')==false){
				flag = false;
			}*/
		}
		
		/*options of 4th radio button*/
		if($('#empauthcarddetailsdiv').is(':visible')){
			$('#empauthcarddetailsAlignNumber').parsley('validate');
			if($('#empauthcarddetailsAlignNumber').parsley('isValid')==false){
				flag = false;
			}
			
			$('#empauthcarddetailsppcardno').parsley('validate');
			if($('#empauthcarddetailsppcardno').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#empauthcarddetailsdateOfExp').parsley('validate');
			if($('#empauthcarddetailsdateOfExp').parsley('isValid')==false){
				flag = false;
			}*/
		}
		
		/*options of 5th radio button*/
		if($('#arrivaldeparturerecorddiv').is(':visible')){
			$('#arrivaldeparturerecordI94no').parsley('validate');
			if($('#arrivaldeparturerecordI94no').parsley('isValid')==false){
				flag = false;
			}
			
			/*
			$('#arrivaldeparturerecordSevisID').parsley('validate');
			if($('#arrivaldeparturerecordSevisID').parsley('isValid')==false){
				flag = false;
			}
			
			$('#arrivaldeparturerecorddateOfExp').parsley('validate');
			if($('#arrivaldeparturerecorddateOfExp').parsley('isValid')==false){
				flag = false;
			}*/
		}
		
		/*options of 6th radio button*/
		if($('#arrivaldeprecordForeigndiv').is(':visible')){
			$('#arrivaldeprecordForeignI94no').parsley('validate');
			if($('#arrivaldeprecordForeignI94no').parsley('isValid')==false){
				flag = false;
			}
			
			$('#arrivaldeprecordForeignppcardno').parsley('validate');
			if($('#arrivaldeprecordForeignppcardno').parsley('isValid')==false){
				flag = false;
			}
			
			$('#arrivaldeprecordForeignCounty').parsley('validate');
			if($('#arrivaldeprecordForeignCounty').parsley('isValid')==false){
				flag = false;
			}
			
			$('#arrivaldeprecordForeigndateOfExp').parsley('validate');
			if($('#arrivaldeprecordForeigndateOfExp').parsley('isValid')==false){
				flag = false;
			}
			/* Don't require validation for following fields #HIX-42693
			$('#arrivaldeprecordForeignppvisano').parsley('validate');
			if($('#arrivaldeprecordForeignppvisano').parsley('isValid')==false){
				flag = false;
			}
			
			$('#arrivaldeprecordForeignSevisID').parsley('validate');
			if($('#arrivaldeprecordForeignSevisID').parsley('isValid')==false){
				flag = false;
			}*/
		}
		
		
		/*options of 7th radio button*/
		if($('#NoticeOfActiondiv').is(':visible')){
			$('#NoticeOfActionAlignNumber').parsley('validate');
			if($('#NoticeOfActionAlignNumber').parsley('isValid')==false){
				flag = false;
			}
			
			$('#NoticeOfActionI94').parsley('validate');
			if($('#NoticeOfActionI94').parsley('isValid')==false){
				flag = false;
			}
		}
		
		/*options of 8th radio button*/
		if($('#ForeignppI94div').is(':visible')){
			$('#ForeignppI94AlignNumber').parsley('validate');
			if($('#ForeignppI94AlignNumber').parsley('isValid')==false){
				flag = false;
			}
			
			$('#ForeignppI94ppcardno').parsley('validate');
			if($('#ForeignppI94ppcardno').parsley('isValid')==false){
				flag = false;
			}
			
			$('#ForeignppI94County').parsley('validate');
			if($('#ForeignppI94County').parsley('isValid')==false){
				flag = false;
			}
			
			$('#ForeignppI94dateOfExp').parsley('validate');
			if($('#ForeignppI94dateOfExp').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#ForeignppI94SevisID').parsley('validate');
			if($('#ForeignppI94SevisID').parsley('isValid')==false){
				flag = false;
			}*/
		}
		
		/*options of 9th radio button*/
		if($('#rentrypermitdiv').is(':visible')){
			
			$('#rentrypermitAlignNumber').parsley('validate');
			if($('#rentrypermitAlignNumber').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#rentrypermitdateOfExp').parsley('validate');
			if($('#rentrypermitdateOfExp').parsley('isValid')==false){
				flag = false;
			}*/			
		}
		
		/*option of 10th radio button*/
		if ($('#refugeetraveldocI-571div').is(':visible')){
			
			$('#refugeetraveldocI-571AlignNumber').parsley('validate');
			if($('#refugeetraveldocI-571AlignNumber').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#refugeetraveldocI-571dateOfExp').parsley('validate');
			if($('#refugeetraveldocI-571dateOfExp').parsley('isValid')==false){
				flag = false;
			}*/
		}
		
		
		/*options of 11th radio button*/
		if($('#certificatenonimmigrantF1-Student-I20div').is(':visible')){
			
			$('#certificatenonimmigrantF1-Student-I20SevisID').parsley('validate');
			if($('#certificatenonimmigrantF1-Student-I20SevisID').parsley('isValid')==false){
				flag = false;
			}
			
			$('#certificatenonimmigrantF1-Student-I20County').parsley('validate');
			if($('#certificatenonimmigrantF1-Student-I20County').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#certificatenonimmigrantF1-Student-I20I94no').parsley('validate');
			if($('#certificatenonimmigrantF1-Student-I20I94no').parsley('isValid')==false){
				flag = false;
			}*/
			/*
			$('#certificatenonimmigrantF1-Student-I20ppcardno').parsley('validate');
			if($('#certificatenonimmigrantF1-Student-I20ppcardno').parsley('isValid')==false){
				flag = false;
			}*/
			/*
			$('#certificatenonimmigrantF1-Student-I20I94no').parsley('validate');
			if($('#certificatenonimmigrantF1-Student-I20dateOfExp').parsley('isValid')==false){
				flag = false;
			}*/
		}
		
		/* option 12th Radio button*/
		if($('#certificatenonimmigrantJ1-Student-DS2019div').is(':visible')){
			
			$('#certificatenonimmigrantJ1-Student-DS2019SevisID').parsley('validate');
			if($('#certificatenonimmigrantJ1-Student-DS2019SevisID').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#certificatenonimmigrantJ1-Student-DS2019I94no').parsley('validate');
			if($('#certificatenonimmigrantJ1-Student-DS2019I94no').parsley('isValid')==false){
				flag = false;
			}*/
			/*
			$('#certificatenonimmigrantJ1-Student-DS2019ppcardno').parsley('validate');
			if($('#certificatenonimmigrantJ1-Student-DS2019ppcardno').parsley('isValid')==false){
				flag = false;
			}*/
			/*
			$('#certificatenonimmigrantJ1-Student-DS2019dateOfExp').parsley('validate');
			if($('#certificatenonimmigrantJ1-Student-DS2019dateOfExp').parsley('isValid')==false){
				flag = false;
			} */
		}
		
		
		if($('#sameNameOnDocument').is(':visible')){
			
			$('#documentFirstName').parsley('validate');
			if($('#documentFirstName').parsley('isValid')==false){
				flag = false;
			}
			
			$('#documentLastName').parsley('validate');
			if($('#documentLastName').parsley('isValid')==false){
				flag = false;
			}	
		}
		
		if($('#sameNameInfoPage62P2').is(':visible')){
			$('input[name=UScitizen62]').parsley('validate');
			if($('input[name=UScitizen62]').parsley('isValid')==false){
				flag = false;
			}
		}
		/*
		if($('#haveTheseDocuments').is(':visible')){
			$('input[name=checkboxValid]').parsley('validate');
			if($('input[name=checkboxValid]').parsley('isValid')==false){
				flag = false;
			}
		}*/
		
		
	}		
	else if(currentPage1 =="appscr62Part2")                    										//appscr62Part2 Page Validation   
	{
			
			if($('input[name=docType1]:radio:checked').val() == "OTHER_DOC")
			{
				if($('#appscr62OtherDocAlignNumber').val()=="")
				{
					$('#appscr62OtherDocAlignNumber').addClass("validateColor");
				
					if(idFocus == "")
						idFocus = "appscr62OtherDocAlignNumber";
					    flag = false;
				}
				
				if($('#appscr62P2I-94Number').val()=="")
				{
					$('#appscr62P2I-94Number').addClass("validateColor");
				
					if(idFocus == "")
						idFocus = "appscr62P2I-94Number";
					    flag = false;
				}				
			}
			else
			{
				$('#appscr62P2I-94Number').removeClass("validateColor");
				$('#appscr62OtherDocAlignNumber').removeClass("validateColor");
			}
			
		}		
		else if(currentPage1 =="appscr63")                    										//appscr63 Page Validation     	
		{
			/*console.log('ddddddddddddddddddddddddddddddd '+ checkBirthDate($('#parent-or-caretaker-dob').val()));*/
			
			// Commented by suneel. This has to be revisited as the data elements doesnt match with the IDs
			// HIX-HIX-39772
			
			
			/*
			$('input[name=appscr63AtLeastOneChildUnder19Indicator]').parsley('validate');
			if($('input[name=appscr63AtLeastOneChildUnder19Indicator]').parsley('isValid')==false){
				flag = false;
			}
			
			if($('.parentCareTaker').is(':visible')){
				$('input[name=takeCareOf]').parsley('validate');
				if($('input[name=takeCareOf]').parsley('isValid')==false){
					flag = false;
				}
			}
			
			if($('.parentOrCaretakerBasicDetailsContainer').is(':visible')){
				$('#parent-or-caretaker-firstName').parsley('validate');
				if($('#parent-or-caretaker-firstName').parsley('isValid')==false){
					flag = false;
				}
				
				$('#parent-or-caretaker-lastName').parsley('validate');
				if($('#parent-or-caretaker-lastName').parsley('isValid')==false){
					flag = false;
				}
				
				$('#applicant-relationship').parsley('validate');
				if($('#applicant-relationship').parsley('isValid')==false){
					flag = false;
				}
				
				$('input[name=birthOrAdoptiveParents]').parsley('validate');
				if($('input[name=birthOrAdoptiveParents]').parsley('isValid')==false){
					flag = false;
				}
				
				$('#parent-or-caretaker-dob').parsley('validate');
				if($('#parent-or-caretaker-dob').parsley('isValid')==false){
					flag = false;
				}
			}*/
		}		
	else if(currentPage1 =="appscr64")                    										//appscr64 Page Validation     	
	{ 
		/*$('input[name=checkPersonNameLanguage]').parsley('validate');
		if($('input[name=checkPersonNameLanguage]').parsley('isValid')==false){
			flag = false;
		}
		
		$('input[name=race]').parsley('validate');
		if($('input[name=race]').parsley('isValid')==false){
			flag = false;
		}
		
		if($('#ethnicityDiv').is(':visible')){
			$('input[name=ethnicity]').parsley('validate');
			if($('input[name=ethnicity]').parsley('isValid')==false){
				flag = false;
			}
		}*/	
	}		
	else if(currentPage1 == "appscr65")												//Validation on appscr65				
	{
		
		$('input[name=householdContactAddress]').parsley('validate');
		if($('input[name=householdContactAddress]').parsley('isValid')==false){
			flag = false;
		}
		
		for(var i=0;i<namesOfHouseHold.length;i++)
		{
			
			//$('input[name=householdContactAddress]').parsley('addConstraint', {required: true });
			$('input[name=householdContactAddress]').parsley('validate');
			if($('input[name=householdContactAddress]').parsley('isValid')==false){
				flag = false;
			}
			if($('#householdContactAddress'+namesOfHouseHold[i]).is(":checked")){
				
				$('#applicant_or_non-applican_address_1'+namesOfHouseHold[i]).parsley('validate');
				if($('#applicant_or_non-applican_address_1'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}
				
				$('#city'+namesOfHouseHold[i]).parsley('validate');
				if($('#city'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}
				
				$('#zip'+namesOfHouseHold[i]).parsley('validate');
				if($('#zip'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}
				
				$('#applicant_or_non-applican_state'+namesOfHouseHold[i]).parsley('validate');
				if($('#applicant_or_non-applican_state'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}
				
				$('input[name=isTemporarilyLivingOutsideIndicator'+namesOfHouseHold[i]+']').parsley('validate');
				if($('input[name=isTemporarilyLivingOutsideIndicator'+namesOfHouseHold[i]+']').parsley('isValid')==false){
					flag = false;
				}
			}

			if($('#inNMDiv'+namesOfHouseHold[i]).is(':visible')){
				
				$('#applicant2city'+namesOfHouseHold[i]).parsley('validate');
				if($('#applicant2city'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}
				
				$('#applicant-2-zip'+namesOfHouseHold[i]).parsley('validate');
				if($('#applicant-2-zip'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}
				
				$('#applicant_or_non-applican_stateTemp'+namesOfHouseHold[i]).parsley('validate');
				if($('#applicant_or_non-applican_stateTemp'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}
				
			}
		}
	}	
	
	else if(currentPage1 == "appscr66"){
		$('input[name=personDisability]').parsley('validate');
		if($('input[name=personDisability]').parsley('isValid')==false){
			flag = false;
		}
		
		$('input[name=personPersonalAssistance]').parsley('validate');
		if($('input[name=personPersonalAssistance]').parsley('isValid')==false){
			flag = false;
		}
		
		$('input[name=peopleBelowTo]').parsley('validate');
		if($('input[name=peopleBelowTo]').parsley('isValid')==false){
			flag = false;
		}
		
		$('input[name=personPregnant]').parsley('validate');
		if($('input[name=personPregnant]').parsley('isValid')==false){
			flag = false;
		}
		
		$('input[name=personfosterCare]').parsley('validate');
		if($('input[name=personfosterCare]').parsley('isValid')==false){
			flag = false;
		}
		
		$('input[name=deceasedPerson]').parsley('validate');
		if($('input[name=deceasedPerson]').parsley('isValid')==false){
			flag = false;
		}
		
		for(var i=1;i<=$('.eligibleCoverage').length ;i++){
			$('input[name=fedHealth'+i+']').parsley('validate');
			if($('input[name=fedHealth'+i+']').parsley('isValid')==false){
				flag = false;
			}
		}
		
		var noOfHouseHold = $('#numberOfHouseHold').text();
		for (i = 1; i <= noOfHouseHold; i++){
			if($('#nativeTribe'+i).is(":visible")){
				$('input[name=AmericonIndianQuestionRadio'+i+']').parsley('addConstraint', {required: true });
				$('input[name=AmericonIndianQuestionRadio'+i+']').parsley('validate');
				if($('input[name=AmericonIndianQuestionRadio'+i+']').parsley('isValid')==false){
					flag = false;
				}
			}
			if($('#americonIndianQuestionDiv'+i).is(":visible")){
				/*$('input[name=americonIndianQuestionSelect'+i+']').parsley('addConstraint', {required: true });*/
				$('#americonIndianQuestionSelect'+i).parsley('validate');
				if($('#americonIndianQuestionSelect'+i).parsley('isValid')==false){
					flag = false;
				}
				
				$('#tribeName'+i).parsley('validate');
				if($('#tribeName'+i).parsley('isValid')==false){
					flag = false;
				}
				
				
			}
		}	
	}
	else if(currentPage1 == "appscrSNAP")
	{
		if($('#shelterAndUtilityExpenseCost').val()=="")
		{
			$('#shelterAndUtilityExpenseCost').addClass("validateColor");			
			if(idFocus == "")
				idFocus = "shelterAndUtilityExpenseCost";
			flag = false;
		}
		if($('#shelterAndUtilityExpenseFrequency').val()=="SelectFrequency")
		{
			$('#shelterAndUtilityExpenseFrequency').addClass("validateColor");			
			if(idFocus == "")
				idFocus = "shelterAndUtilityExpenseFrequency";
			flag = false;
		}
		
	}		
	else if(currentPage1 == "appscr69")												//Validation on Page appscr69	
	{
		
		$('#expeditedIncome69').parsley('validate');
		if($('#expeditedIncome69').parsley('isValid')==false){
			flag = false;
			/*$('.errorMessage ul').show();*/
		}
	}
	else if(currentPage1 == "appscr70")												//Validation on Page appscr70	
	{
		
		$('input[name=appscr70radios4Income]').parsley('validate');
		if($('input[name=appscr70radios4Income]').parsley('isValid')==false){
			flag = false;
		}
		
		if($('#incomeSourceDiv').is(":visible")){
			$('input[name=incomeSources]').parsley('validate');
			if($('input[name=incomeSources]').parsley('isValid')==false){
				flag = false;
			}
		}
	}
	else if(currentPage1 == "appscr71")														//validation on Page appscr71        
	{
		allIncomeValidations();
		if($('.parsley-error-list').is(':visible')){
			flag = false;
			return false;
		}
	}
	else if(currentPage1 == "appscr72")														// Validation on Page appscr72 		
	{
		flag = true;
		//Temporary remove deduction validation
		/*
		if($('#incomeDeductions_div').is(':visible')){
			$('input[name=incomeDeduction]').parsley('validate');
			if($('input[name=incomeDeduction]').parsley('isValid')==false){
				flag = false;
			}
		}
		
		
		if($('#appscr72alimonyPaidTR').is(':visible')){
			
			$('#alimonyAmountInput').parsley('validate');
			if($('#alimonyAmountInput').parsley('isValid')==false){
				flag = false;
			}
			
			$('#alimonyFrequencySelect').parsley('validate');
			if($('#alimonyFrequencySelect').parsley('isValid')==false){
				flag = false;
			}
		}
		
		if($('#appscr72studentLoanInterestTR').is(':visible')){
			
			$('#studentLoanInterestAmountInput').parsley('validate');
			if($('#studentLoanInterestAmountInput').parsley('isValid')==false){
				flag = false;
			}
			
			$('#studentLoanInterestFrequencySelect').parsley('validate');
			if($('#studentLoanInterestFrequencySelect').parsley('isValid')==false){
				flag = false;
			}
		}
		
		if($('#appscr72otherDeductionTR').is(':visible')){
			
			$('#deductionTypeInput').parsley('validate');
			if($('#deductionTypeInput').parsley('isValid')==false){
				flag = false;
			}
			
			$('#deductionAmountInput').parsley('validate');
			if($('#deductionAmountInput').parsley('isValid')==false){
				flag = false;
			}
			
			$('#deductionFrequencySelect').parsley('validate');
			if($('#deductionFrequencySelect').parsley('isValid')==false){
				flag = false;
			}
		}
		
		$('input[name=aboutIncomeSpped]').parsley('validate');
		if($('input[name=aboutIncomeSpped]').parsley('isValid')==false){
			flag = false;
		}
		
		if($('#incomeHighOrLow').is(':visible')){
			$('input[name=incomeHighOrLow]').parsley('validate');
			if($('input[name=incomeHighOrLow]').parsley('isValid')==false){
				flag = false;
			}
		}
		*/
	}			
	else if(currentPage1 == "appscr76P1")											// Validation on Page appscr76P1           
	{
		$('input[name=wasUninsuredFromLast6Month]').parsley('validate');
		if($('input[name=wasUninsuredFromLast6Month]').parsley('isValid')==false){
			flag = false;
		}
		
		$('input[name=isHealthCoverageThroughJob]').parsley('validate');
		if($('input[name=isHealthCoverageThroughJob]').parsley('isValid')==false){
			flag = false;
		}
		
		$('input[name=willHealthCoverageThroughJob]').parsley('validate');
		if($('input[name=willHealthCoverageThroughJob]').parsley('isValid')==false){
			flag = false;
		}
		
		if($('#appscr76P1FirstHideAndShowDiv').is(':visible')){
			$('#appscr76P1HealthCoverageDate').parsley('validate');
			if($('#appscr76P1HealthCoverageDate').parsley('isValid')==false){
				flag = false;
			}
			
			
			if(!$('input[name=healthCoverageEmployee]').is(':checked')){
				$('input[name=healthCoverageEmployee]').parsley('validate');
				if($('input[name=healthCoverageEmployee]').parsley('isValid')==false){
					flag = false;
				}
			}

			
		}
		/*if(!$('input[name=healthCoverageEmployee]').is(':checked')){
			$('input[name=healthCoverageEmployee]').parsley('validate');
			if($('input[name=healthCoverageEmployee]').parsley('isValid')==false){
				flag = false;
			}
		}
		*/
		
		
		if($('#memberNameDataDivPart1').is(':visible')){
			$('#memberNameDataEinText').parsley('validate');
			if($('#memberNameDataEinText').parsley('isValid')==false){
				flag = false;
			}
			
			$('#memberNameDataAddress1').parsley('validate');
			if($('#memberNameDataAddress1').parsley('isValid')==false){
				flag = false;
			}
			
			$('#memberNameDataCity').parsley('validate');
			if($('#memberNameDataCity').parsley('isValid')==false){
				flag = false;
			}
			
			$('#memberNameDataZip').parsley('validate');
			if($('#memberNameDataZip').parsley('isValid')==false){
				flag = false;
			}
			
			$('#memberNameDataState').parsley('validate');
			if($('#memberNameDataState').parsley('isValid')==false){
				flag = false;
			}
		}
		
		if($('#memberNameDataDivPart2').is(':visible')){
			$('#appscr76P1_otherEmployerName').parsley('validate');
			if($('#appscr76P1_otherEmployerName').parsley('isValid')==false){
				flag = false;
			}
			
			$('#appscr76P1_employerEIN').parsley('validate');
			if($('#appscr76P1_employerEIN').parsley('isValid')==false){
				flag = false;
			}
			
			$('#appscr76P1_otherEmployerAddress1').parsley('validate');
			if($('#appscr76P1_otherEmployerAddress1').parsley('isValid')==false){
				flag = false;
			}
			
			$('#appscr76P1_otherEmployerCity').parsley('validate');
			if($('#appscr76P1_otherEmployerCity').parsley('isValid')==false){
				flag = false;
			}
			
			$('#appscr76P1_otherEmployerZip').parsley('validate');
			if($('#appscr76P1_otherEmployerZip').parsley('isValid')==false){
				flag = false;
			}
			
			$('#appscr76P1_otherEmployerState').parsley('validate');
			if($('#appscr76P1_otherEmployerState').parsley('isValid')==false){
				flag = false;
			}
		}
		
		
		
		
	}		
	else if(currentPage1 == "appscr76P2")											// Validation on Page appscr76P2           
	{
		$('input[name=appscr76p2radiosgroup]').parsley('validate');
		if($('input[name=appscr76p2radiosgroup]').parsley('isValid')==false){
			flag = false;
		}
		
		$('input[name=willBeEnrolledInHealthCoverageInFollowingYear]').parsley('validate');
		if($('input[name=willBeEnrolledInHealthCoverageInFollowingYear]').parsley('isValid')==false){
			flag = false;
		}
	}		
	else if(currentPage1 =="appscr77Part1")                    //appscr77Part1 Page Validation     	
	{
		$('input[name=77P1CurrentlyEnrolledInHealthPlan]').parsley('validate');
		if($('input[name=77P1CurrentlyEnrolledInHealthPlan]').parsley('isValid')==false){
			flag = false;
		}
		
		$('input[name=77P1WillBeEnrolledInHealthPlan]').parsley('validate');
		if($('input[name=77P1WillBeEnrolledInHealthPlan]').parsley('isValid')==false){
			flag = false;
		}
		
		if($('#DateToCovered').is(':visible')){
			$('#77P1WillBeEnrolledInHealthPlanDate').parsley('validate');
			if($('#77P1WillBeEnrolledInHealthPlanDate').parsley('isValid')==false){
				flag = false;
			}
		}
		
		$('#77P1NoLongerHealthCoverageDate').parsley('validate');
		if($('#77P1NoLongerHealthCoverageDate').parsley('isValid')==false){
			flag = false;
		}
		
		$('input[name=77P1ExpectChangesToHealthCoverage]').parsley('validate');
		if($('input[name=77P1ExpectChangesToHealthCoverage]').parsley('isValid')==false){
			flag = false;
		}
		
		if($('.doesExceptAnyChangeToHealthCoverageInappscr77Part1').is(':visible')){
			
			$('#77P1PlanToDropHealthCoverageDate').parsley('validate');
			if($('#77P1PlanToDropHealthCoverageDate').parsley('isValid')==false){
				flag = false;
			}
			
			$('#77P1WillOfferCoverageDate').parsley('validate');
			if($('#77P1WillOfferCoverageDate').parsley('isValid')==false){
				flag = false;
			}
			
			$('#77P1PlaningToEnrollInHCDate').parsley('validate');
			if($('#77P1PlaningToEnrollInHCDate').parsley('isValid')==false){
				flag = false;
			}
			
			$('#77P1HealthPlanOptionsGoingToChangeDate').parsley('validate');
			if($('#77P1HealthPlanOptionsGoingToChangeDate').parsley('isValid')==false){
				flag = false;
			}
		}
		
		$('#currentLowestCostSelfOnlyPlanName').parsley('validate');
		if($('#currentLowestCostSelfOnlyPlanName').parsley('isValid')==false){
			flag = false;
		}
		
		$('#comingLowestCostSelfOnlyPlanName').parsley('validate');
		if($('#comingLowestCostSelfOnlyPlanName').parsley('isValid')==false){
			flag = false;
		}
		
		$('#lowestCostSelfOnlyPremiumAmount').parsley('validate');
		if($('#lowestCostSelfOnlyPremiumAmount').parsley('isValid')==false){
			flag = false;
		}
		
		$('#premiumPlansStatus').parsley('validate');
		if($('#premiumPlansStatus').parsley('isValid')==false){
			flag = false;
		}
		
		$('input[name=appscr77p2IsAffordable]').parsley('validate');
		if($('input[name=appscr77p2IsAffordable]').parsley('isValid')==false){
			flag = false;
		}
		
	}			
	else if(currentPage1 =="appscr77Part2")                    //appscr77Part2 Page Validation     	
	{
		
	}			
	else if(currentPage1 =="appscr78")                    //appscr78 Page Validation     			
	{
		
	}			
	else if(currentPage1 =="appscr79")                    //appscr79 Page Validation     		
	{
		
	}		
	else if(currentPage1 =="appscr80")                    //appscr80 Page Validation     		
	{
		if(householdSSNIndicationValue == "no")
		if($('input:radio[name=ssnIndicator80]:checked').val() == "yes")
		{
			if($('#ssn1_80').val()=="")
			{
				$('#ssn1_80').addClass("validateColor");
				addLabels('Enter the SSN Number', "ssn1_80");
				if(idFocus == "")
					idFocus ="ssn1_80";
				    flag = false;
			}
			if($('#ssn2_80').val()=="")
			{
				$('#ssn2_80').addClass("validateColor");
				addLabels('Enter the SSN Number', "ssn2_80");
				if(idFocus == "")
					idFocus ="ssn2_80";
				flag = false;
			}
			
			if($('#ssn3_80').val()=="")
			{
				$('#ssn3_80').addClass("validateColor");
				addLabels('Enter the SSN Number', "ssn3_80");
				if(idFocus == "")
					idFocus ="ssn3_80";
				flag = false;
			}
		}
		else
		{
			$('#ssn1_80').removeClass("validateColor");
			$('#ssn2_80').removeClass("validateColor");
			$('#ssn3_80').removeClass("validateColor");
			
			
		}
		
			
	}		
	else if(currentPage1 =="appscr81B")                    //appscr81B Page Validation     		
	{
		
		
	}			
	else if(currentPage1 =="appscr81C")                    //appscr81C Page Validation     		
	{
		
		
	}
	else
	{
	
		$('#prevBtn').show();
		flag = true;
	}
	
	$('#'+idFocus).focus();
	return flag;
}


//Check Date Format
function checkDate(date)			
{
	
	var validformat = /^\d{2}\/\d{2}\/\d{4}$/;
	
	if (!validformat.test(date))
		return false;
	else 
	{
		var monthfield = date.split("/")[0];
		var dayfield = date.split("/")[1];
		var yearfield = date.split("/")[2];
		
				
		var fdate=new Date();
	  
	    
	    var dayobj = new Date(yearfield, monthfield - 1, dayfield);
	 
	    if(fdate.getTime()<=dayobj.getTime())
	        {	    		
	    			return false;
	        }
		
		
		if ((dayobj.getMonth() + 1 != monthfield) || (dayobj.getDate() != dayfield)	|| (dayobj.getFullYear() != yearfield))
			return false;
		else		
			return true;
	}
}


//Check Email ID Format
function checkEmail(email)				
{
	var a="";
	var b="";
	var c="";
	
	try
	{
		a = email.split("@")[0];
		
		if(a!=undefined)
		{
		b = email.split("@")[1];
		}
		
		if(a!=undefined)
			c = b.split(".")[1];
	}
	catch(Ex)
	{}
	

	if(a==""|| c==""|| b==undefined || c==undefined)
	{
		return false;
		
	}
	else
		return true;
}




//check Number    
function checkNumber(num)
{
	var reg=/[0-9 -()+]+$/; 
	
	if (!reg.test(num))
		return false;
	
	else 
		return true;
}

//check date		
function checkDob(date)
{
	var validformat = /^\d{2}\/\d{2}\/\d{4}$/;
	
	if (!validformat.test(date))
		return false;
	else
		return true;
}

//String Checking					
function checkString(str)
{
	
	var reg = new RegExp("^[a-zA-Z ]+$");
	alert(reg.test(str));
	if (!reg.test(str))
		return false;
	else 
		return true;
}

// check zip code		
function checkZip_code(zipcode)
{
	alert(zipcode);
var zip=/^[0-9]{5}$/;
alert(zip.test(zipcode));
if(!zip.test(zipcode))
	return false;
else
	return true;
	
	
}



function emptyLabels()
{
	$('#labelsAdd').html('');
}



// 		Remove Validation Class 

function removeValidationClass(currentPage1)
{
	emptyLabels();		// Empty labels
	
	if(currentPage1 == "appscr53")
	{
		$('#firstName').removeClass("validateColor");
		$('#lastName').removeClass("validateColor");
		$('#dateOfBirth').removeClass("validateColor");
		$('#emailAddress').removeClass("validateColor");
		$('#home_addressLine1').removeClass("validateColor");
		$('#home_primary_city').removeClass("validateColor");
		$('#home_primary_zip').removeClass("validateColor");
		$('#home_primary_state').removeClass("validateColor");
		$('#home_primary_county').removeClass("validateColor");
	}
	else if(currentPage1 == "appscr54")
	{
		$('#healthInsurBroker_name').removeClass("validateColor");
		$('#healthInsurBroker_id').removeClass("validateColor");
		$('#healthCoverageGuide_name').removeClass("validateColor");
		$('#healthCoverageGuide_id').removeClass("validateColor");
	}
	else if(currentPage1 == "appscr57")
	{
		
	}
}


//Add labels

function addLabels(msg, idname)
{
	var divname = idname+"_div";
	
	var str = "<div  id="+divname+"><label class='validatelblColor'><br />Please "+ msg + "</label></div>";
	
	$('#labelsAdd').append(str);
}


function otherAddressNoneOfTheAbove()
{
	
	if($('#HouseHoldotherAddressNoneOfTheAbove').is(":checked") == true)
	{
		for(var i=2;i<=noOfHouseHold;i++)
		{
			//householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			//var HouseHoldName = householdMemberObj.name.firstName + "_" + householdMemberObj.name.middleName + "_" + householdMemberObj.name.lastName;
		
			document.getElementById('householdContactAddress'+i).checked = false;
		
			$('#other_Address_'+i).hide();
			
		}
	
	}
}


/// All Income Validations
function allIncomeValidations(){
	//console.log(idFocus,'---idFocus')
	
	if($('#Financial_Job_Div').is(':visible')){
		$('#Financial_Job_EmployerName_id').parsley('validate');
		if($('#Financial_Job_EmployerName_id').parsley('isValid')==false){
			flag = false;
		}
		
		$('#Financial_Job_id').parsley('validate');
		if($('#Financial_Job_id').parsley('isValid')==false){
			flag = false;
		}
		
		$('#Financial_Job_select_id').parsley('validate');
		if($('#Financial_Job_select_id').parsley('isValid')==false){
			flag = false;
		}
		
		if($('#howMuchPerWeek').is(':visible')){
			$('#Financial_Job_HoursOrDays_id').parsley('validate');
			if($('#Financial_Job_HoursOrDays_id').parsley('isValid')==false){
				flag = false;
			}
		}
	}
	
	if($('#Financial_Self_Employment_Div').is(':visible')){
		$('#Financial_Self_Employment_TypeOfWork').parsley('validate');
		if($('#Financial_Self_Employment_TypeOfWork').parsley('isValid')==false){
			flag = false;
		}
		
		$('#Financial_Self_Employment_id').parsley('validate');
		if($('#Financial_Self_Employment_id').parsley('isValid')==false){
			flag = false;
		}	
	}
	
	if($('#Financial_SocialSecuritybenefits_Div').is(':visible')){
		
		$('#Financial_SocialSecuritybenefits_id').parsley('validate');
		if($('#Financial_SocialSecuritybenefits_id').parsley('isValid')==false){
			flag = false;
		}
		
		$('#Financial_SocialSecuritybenefits_select_id').parsley('validate');
		if($('#Financial_SocialSecuritybenefits_select_id').parsley('isValid')==false){
			flag = false;
		}
	}
	
	if($('#Financial_Unemployment_Div').is(':visible')){
		
		$('#Financial_Unemployment_StateGovernment').parsley('validate');
		if($('#Financial_Unemployment_StateGovernment').parsley('isValid')==false){
			flag = false;
		}
		
		$('#Financial_Unemployment_id').parsley('validate');
		if($('#Financial_Unemployment_id').parsley('isValid')==false){
			flag = false;
		}
		
		$('#Financial_Unemployment_select_id').parsley('validate');
		if($('#Financial_Unemployment_select_id').parsley('isValid')==false){
			flag = false;
		}
		
		if($('#isDateUnemploymentSet').is(':visible')){
			$('input[name=payPerWeekRadio]').parsley('validate');
			if($('input[name=payPerWeekRadio]').parsley('isValid')==false){
				flag = false;
			}
		}
		
		if($('#dateEmployementExpire').is(':visible')){
			$('#unemploymentIncomeDate').parsley('validate');
			if($('#unemploymentIncomeDate').parsley('isValid')==false){
				flag = false;
			}
		}
	}
	
	
	if($('#Financial_Retirement_pension_Div').is(':visible')){
		$('#Financial_Retirement_pension_id').parsley('validate');
		if($('#Financial_Retirement_pension_id').parsley('isValid')==false){
			flag = false;
		}
		
		$('#Financial_Retirement_pension_select_id').parsley('validate');
		if($('#Financial_Retirement_pension_select_id').parsley('isValid')==false){
			flag = false;
		}
	}
	
	if($('#Financial_Capitalgains_Div').is(':visible')){
		$('#Financial_Capitalgains_id').parsley('validate');
		if($('#Financial_Capitalgains_id').parsley('isValid')==false){
			flag = false;
		}
	}
	
	if($('#Financial_InvestmentIncome_Div').is(':visible')){
		$('#Financial_InvestmentIncome_id').parsley('validate');
		if($('#Financial_InvestmentIncome_id').parsley('isValid')==false){
			flag = false;
		}
		
		$('#Financial_InvestmentIncome_select_id').parsley('validate');
		if($('#Financial_InvestmentIncome_select_id').parsley('isValid')==false){
			flag = false;
		}
	}
	
	if($('#Financial_RentalOrRoyaltyIncome_Div').is(':visible')){
		$('#Financial_RentalOrRoyaltyIncome_id').parsley('validate');
		if($('#Financial_RentalOrRoyaltyIncome_id').parsley('isValid')==false){
			flag = false;
		}
		
		$('#Financial_RentalOrRoyaltyIncome_select_id').parsley('validate');
		if($('#Financial_RentalOrRoyaltyIncome_select_id').parsley('isValid')==false){
			flag = false;
		}
	}
	
	if($('#Financial_FarmingOrFishingIncome_Div').is(':visible')){
		$('#Financial_FarmingOrFishingIncome_id').parsley('validate');
		if($('#Financial_FarmingOrFishingIncome_id').parsley('isValid')==false){
			flag = false;
		}
		
		$('#Financial_FarmingOrFishingIncome_select_id').parsley('validate');
		if($('#Financial_FarmingOrFishingIncome_select_id').parsley('isValid')==false){
			flag = false;
		}
	}
	
	if($('#Financial_AlimonyReceived_Div').is(':visible')){
		$('#Financial_AlimonyReceived_id').parsley('validate');
		if($('#Financial_AlimonyReceived_id').parsley('isValid')==false){
			flag = false;
		}
		
		$('#Financial_AlimonyReceived_select_id').parsley('validate');
		if($('#Financial_AlimonyReceived_select_id').parsley('isValid')==false){
			flag = false;
		}
	}
	
	if($('#Financial_OtherIncome_Div').is(':visible')){
		$('input[name=otherIncome]').parsley('validate');
		if($('input[name=otherIncome]').parsley('isValid')==false){
			flag = false;
		}
	}
	
	if(($('#cancelled_Debts').is(':visible')) && ($('#cancelledDebtsFrequencyDiv').is(':visible'))){
		
		$('#cancelled_Debts_amount').parsley('validate');
		if($('#cancelled_Debts_amount').parsley('isValid')==false){
			flag = false;
		}
		
		$('#cancelledDebtsFrequency').parsley('validate');
		if($('#cancelledDebtsFrequency').parsley('isValid')==false){
			flag = false;
		}		
	}
	
	if(($('#court_awrds').is(':visible')) && ($('#courtAwrdsFrequencyDiv').is(':visible'))){
		
		$('#court_awards_amount').parsley('validate');
		if($('#court_awards_amount').parsley('isValid')==false){
			flag = false;
		}
		
		$('#courtAwrdsFrequency').parsley('validate');
		if($('#courtAwrdsFrequency').parsley('isValid')==false){
			flag = false;
		}		
	}
	
	if(($('#jury_duty_pay').is(':visible')) && ($('#juryDutyPayFrequencyDiv').is(':visible'))){
		
		$('#jury_duty_pay_amount').parsley('validate');
		if($('#jury_duty_pay_amount').parsley('isValid')==false){
			flag = false;
		}
		
		$('#juryDutyPayFrequency').parsley('validate');
		if($('#juryDutyPayFrequency').parsley('isValid')==false){
			flag = false;
		}		
	}
}



// check is member age less than 365 days or not 
// gets two dates in string and compare 
function checkBirthDate(BirthDate)
{
	
	
	var serverOfDate = $('#currentDate').val();
	

	
var serverDate= new Date(serverOfDate);
var houseHoldDob= new Date(BirthDate);


var diffrence = parseInt((serverDate-houseHoldDob)/(1000*60*60*24));


return diffrence;


}

//check num of female that are 13 or above
function checkPregnantCount()
{
	var pregnantAgeCount=0;
	for (var i = 1; i <= noOfHouseHold; i++){			
		var fDob = $('#appscr57DOBTD'+i).text();
		if(checkBirthDate(fDob)>(365*13)){
			pregnantAgeCount++;
		}
	}
	
	return pregnantAgeCount;
}



//Set authorized name and Household name
function setAuthorizedName()
{
var authorizedName = $('#authorizedFirstName').val() + " "+ $('#authorizedMiddleName').val() + " "+ $('#authorizedLastName').val()+" ";

if($('#authorizedFirstName').val()== "" && $('#authorizedMiddleName').val()== "" && $('#authorizedLastName').val()== "")
	$('.appscr54_authorizedName').text("Authorized Representative ");
else
	$('.appscr54_authorizedName').text(authorizedName);	
 
}


// Masking at appscr57
function addMaskingOnAppscr57()
{
	for(var i=1; i<=noOfHouseHold; i++)
	{
		$('#appscr57DOB'+i).mask("00/00/0000");
		
	}
	
	$('#parent-or-caretaker-dob, #appscr76P1HealthCoverageDate, #77P1PlanToDropHealthCoverageDate, #77P1WillOfferCoverageDate, #77P1PlaningToEnrollInHCDate, #77P1HealthPlanOptionsGoingToChangeDate').mask("00/00/0000");
	
}

function checkOnlyDateFormate(date)
{
	var flag = true;
	
	var month = date.split("/")[0];
	var day = date.split("/")[1];
	var year = date.split("/")[2];
	
	if(month != undefined && day != undefined && year != undefined)
	{
		if(month.length == 2 && month<=12 && month>0)
		{
			if(day.length == 2 && day<=31 && day>0)
			{
				if(year.length == 4)
					flag = true;
				else
					flag = false;
			}
			else
				flag = false;
		}
		else
			flag = false;
	}
	else
		flag = false;
	
	return flag;
}


/*function checkForOtherIncome(){
	if($('#cancelled_Debts_amount').val()==""){
		$('#parsley-cancelled-Debts-amount').show();
	}else{
		$('#parsley-cancelled-Debts-amount').hide();
	}
	
	if($('#court_awards_amount').val()==""){
		$('#parsley-court-awards-amount').show();
	}else{
		$('#parsley-court-awards-amount').hide();
	}
	
	if($('#jury_duty_pay_amount').val()==""){
		$('#parsley-jury-duty-pay-amount').show();
	}else{
		$('#parsley-jury-duty-pay-amount').hide();
	}
}*/