
const ADMIN = 'ADMIN';
const BROKER = 'BROKER';
const INDIVIDUAL = 'INDIVIDUAL';
const L1_CSR = 'L1_CSR';
const L2_CSR = 'L2_CSR';
const SUPERVISOR = 'SUPERVISOR';

export default {
    REGULAR_USERS: {
        BROKER,
        INDIVIDUAL
    },
    SUPERUSERS: {
        ADMIN,
        L1_CSR,
        L2_CSR,
        SUPERVISOR
    }
};