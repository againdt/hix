/**
 * 
 */
package com.getinsured.hix.eligibility.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.estimator.mini.EligLead;

/**
 * HIX-70818
 * Repository class for Elig Lead table
 * @author Nikhil Talreja
 *
 */

@Repository
public interface IEligLeadRepository extends JpaRepository<EligLead, Long> {

}
