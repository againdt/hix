<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page isELIgnored="false"%>

<script type="text/javascript" src='<c:url value="/resources/js/jquery.form.js" />'></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

	<div class="row-fluid">
		<div class="gutter10">
    	<div class="row-fluid">
		<div class="span3" id="sidebar"></div>
		<div class="span9 gray" id="rightpanel">
			<div class="gutter10">				 
					 <form id="frmAddProviderNetwork" class="form-horizontal" name="frmAddProviderNetwork" action='<c:url value="/provider/network/add"/>' enctype="multipart/form-data" method="post" onsubmit="return(validate());">

					 <input type="hidden" id="planId" name="planId" value="1"/>
					 <input type="hidden" id="networkType" name="networkType" value="PPO"/>
					 <input type="hidden" id="fileToUpload" name="fileToUpload" value=""/>
					 <input type="hidden" id="hdnPhysicians" name="hdnPhysicians" value=""/>
					 <input type="hidden" id="checkfacAndPhyFiles" name="checkfacAndPhyFiles" value=""/>
							
							<div class="control-group form-inline gutter20-t">
								<label for="physicians" class="control-label"><spring:message code='label.physicians' javaScriptEscape='true'/>&nbsp;<a href="#"><i class="icon-question-sign"></i></a>
								</label>
								<div class="controls">
									<input type="file" id="physicians" name="physicians" class="input-file">
									 &nbsp;
									<button id="btn_upload_physicians" class="btn" type="button" onclick="uploadFileAjax();"><spring:message code='label.upload' javaScriptEscape='true'/></button>
									<div id="physicians_display">${physicians}</div><div id="physicianFile_error"></div>
								</div>		
							<div class="controls">
								<div id="checkfacAndPhyFiles_error" class=""></div>
							</div>		 							
						<div class="form-actions">
					<button class="btn btn-primary" type="submit"><spring:message code='label.saveProviderNetwork' javaScriptEscape='true'/></button>						</div>
				</div>
			  </form>
			</div>
       </div><!-- end of row-fluid -->
      </div>
    </div>
</div>
<!--  end of row-fluid -->

<script type="text/javascript">
	function uploadFileAjax(){
		if($('#physicians').val()==''){
			$('#physicianFile_error').html("<label class='error' for='hiosIssuerId' generated='true'><span><em class='excl'>!</em> <spring:message  code='err.selectPhysicianFile' javaScriptEscape='true'/></span></label>");
		}else{
		$('#fileToUpload').val("physicians");
		$('#frmAddProviderNetwork').ajaxSubmit({
			url: "<c:url value='/provider/network/fileupload'/>",
			success: function(responseText){
				 var val = responseText.split(",");
				 $('#physicianFile_error').html("");
	        	 $("#physicians_display").text(val[0]);
				 $("#hdnPhysicians").val(val[1]);
	   		 }
		});
		return false;
		}		 
	}
	
	function validate() {
		
		var facilities_display = document.getElementById('hdnPhysicians').value;

		if (!facilities_display) {
			$('#physicianFile_error').html("<label class='error' for='hiosIssuerId' generated='true'><span><em class='excl'>!</em> <spring:message  code='err.physicianFile' javaScriptEscape='true'/></span></label>");
			return false;
		}else{
			$('#physicianFile_error').html("");
			return true;
		}
	}
</script>
