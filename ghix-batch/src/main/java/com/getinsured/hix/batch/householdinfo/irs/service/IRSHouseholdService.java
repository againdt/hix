package com.getinsured.hix.batch.householdinfo.irs.service;

import java.util.List;

import us.gov.treasury.irs.common.IRSHouseholdGrpType;

import com.getinsured.eligibility.householdinfo.irs.IRSHouseholdClientRequest;
import com.getinsured.hix.platform.util.exception.GIException;

public interface IRSHouseholdService {
	public List<IRSHouseholdGrpType> getIrsHouseholdInfo(IRSHouseholdClientRequest irsHouseholdClientRequest) throws GIException;

}
