{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload":{
  "taxHousehold": [
    {
      "householdMember": [ {
        
          "dateOfBirth": "01/25/1970",
          "name": {
            "firstName": "John",
            "lastName": "Fox"
          },
          "socialSecurityCard": {
            "socialSecurityNumber": "222222203"
          },
          "gender": "F",
        "householdContact": {
          "homeAddress": {
            "state": "AZ"
          }
        },
        "insurance": {
          "insurancePolicyEffectiveDate": "06/01/2014",
          "insurancePolicyExpirationDate": "05/31/2015"
        },
        "organization": [
          {
            "organizationCode": "MEDC"
          }
        ]
      }  ]
    }
  ]
}}