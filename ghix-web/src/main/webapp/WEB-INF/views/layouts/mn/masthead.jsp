	<%@page import="com.getinsured.hix.model.UserRole"%>
		<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
		<%@page session="true" %>
		<%@page import="java.util.Set"%>
		<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
		<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
		<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
		<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld"%>
		<%@page import="org.apache.commons.lang.StringUtils"%>
		<%@page import="com.getinsured.hix.util.GhixConfiguration"%>
		<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
		<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
		<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
		<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
		

			<%
//   String themeName = (String) request.getSession().getAttribute("switchToThemeName");
//   if(StringUtils.isBlank(themeName)){
// 	  themeName="default";
//   }
//   themeName = GhixConfiguration.ACCENT_LESS_FILE+"-"+themeName+".css";
// 	 themeName = "theme-"+themeName+".css";


String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
String isUserFullyAuthorized =  (String) request.getSession().getAttribute("isUserFullyAuthorized");
String isUserSwitchToOtherView = (String)request.getSession().getAttribute("isUserSwitchToOtherView");
Set<UserRole> userRoles = (Set<UserRole>)request.getSession().getAttribute("userConfiguredRoles");
String userActiveRoleLabel =  (String) request.getSession().getAttribute("userActiveRoleLabel");


	/*
	 * START:: HIX-10725 - Program Menu Links for Inbox, account manintenance
	 *         HIX-19109 - Masthead links w.r.t to userActiveRoleName
	 */
	 String myAccountUrl= GhixConstants.MY_ACCOUNT_URL;
	 String myInboxUrl= GhixConstants.MY_INBOX_URL;
	 String userName =  (String) request.getSession().getAttribute("USER_NAME");
	 String logoutUrl = request.getContextPath()+"/account/user/logout";
	 String logoUrl = (String)request.getSession().getAttribute("userLandingPage");

	if(StringUtils.isBlank(myAccountUrl)){
		myAccountUrl="#";
	}

	if(StringUtils.isNotBlank(myInboxUrl) && StringUtils.isNotBlank(userName)) {
		myInboxUrl = myInboxUrl + "?languageCode=" + session.getAttribute("lang") ;
	}else{
		myInboxUrl="#";
	}

	 String switchToModuleNameLocal=(String)request.getSession().getAttribute("switchToModuleName");
	 String showFindAgentInEm="FALSE";
	 if(   ("Y".equalsIgnoreCase(isUserSwitchToOtherView)) && "employer".equalsIgnoreCase(switchToModuleNameLocal) ){
		 showFindAgentInEm="TRUE";
	 }

	if(StringUtils.isBlank(logoutUrl)){
		logoutUrl="#";
	}

	if(StringUtils.isBlank(isUserSwitchToOtherView)){
		isUserSwitchToOtherView="N";
	}


	String 	faqUrl = "/faq/home";

	if(StringUtils.isNotBlank(userActiveRoleName)){
		if(StringUtils.equalsIgnoreCase(userActiveRoleName, "admin") || StringUtils.equalsIgnoreCase(userActiveRoleName, "operations") || StringUtils.equalsIgnoreCase(userActiveRoleName, "csr")){
			faqUrl= "/faq/admin";
		}else if(StringUtils.equalsIgnoreCase(userActiveRoleName, "issuer") || StringUtils.equalsIgnoreCase(userActiveRoleName, "issuer_representative")){
			faqUrl= "/faq/issuer";
		}else{
			faqUrl= "/faq/"+userActiveRoleName;
		}
	}

	/*
	 * END:: HIX-10725 - Program Menu Links for Inbox, account manintenance
	 *         HIX-19109 - Masthead links w.r.t to userActiveRoleName
	 */



%>
		<c:set var="userActiveRoleName" value="<%=userActiveRoleName%>" />
		<c:set var="isUserFullyAuthorized" value="<%=isUserFullyAuthorized%>" />
		<c:set var="isUserSwitchToOtherView" value="<%=isUserSwitchToOtherView%>" />
		<c:set var="logoUrl" value="<%=logoUrl%>" />
		<c:set var="faqUrl" value="<%=faqUrl%>" />
		<c:set var="userRoles" value="<%=userRoles%>" />
		<c:set var="userActiveRoleLabel" value="<%=userActiveRoleLabel%>" />
		<c:set var="showFindAgentInEm" value="<%=showFindAgentInEm%>" />
		<c:set var="mnsureEnv" value='<%=GhixConstants.MNSURE_ENV%>' />
		<c:set var="iamEnv" value='<%=GhixConstants.MNSURE_IAM_ENV%>' />
		<c:set var="isSsoEnabled" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSO_ENABLED)%>" />
		<%-- <c:set var="themeName" value="<%=themeName%>" /> --%>
		<%-- <link href="<gi:cdnurl value="/resources/css/${themeName}" /> --%>
		<%-- " rel="stylesheet"  type="text/css" />   --%>
		
		<c:if test="${fn:toLowerCase(isSsoEnabled) eq fn:toLowerCase('y')}">
		<img alt="Keep session alive pixel-no user function" src="<c:url value="${iamEnv}/protected/1x1.gif"/>" style="display: none"/>
		</c:if>

		<script type="text/javascript">
		$(document).ready(function() {
		//$('.dropdown-toggle').dropdown();

			if(!sessionStorage.getItem('language')) {
				sessionStorage.setItem('language','English');
				$('#language-select').text("Espa\xF1ol");
			}

			var language = sessionStorage.getItem('language');
			//set the initial language button value
			if (language ==="English") {
				$('#language-select').text("Espa\xF1ol");
			}
			else {
				$('#language-select').text("English");
			}

			$('#language-select').click(function(e) {
				e.preventDefault();
				var _this = $(this);
				var language = sessionStorage.getItem('language');

				if (language === "English") {
					changeLang('es', 'Spanish');
					$('#language-select').text("English");
					window.dataLayer.push({
						'event': 'languageChange',
						'eventCategory': 'Language Change',
						'eventAction': 'Button Click',
						'eventLabel': 'Espanol'
					});
				}
				else if(language === "Spanish") {
					changeLang('en', 'English');
					$('#language-select').text("Espa\xF1ol");
					window.dataLayer.push({
						'event': 'languageChange',
						'eventCategory': 'Language Change',
						'eventAction': 'Button Click',
						'eventLabel': 'English'
					});
				}
			});
			
			var appStatusInterval = null;
			function validateAppStatusInMain() {
				if (window.localStorage.ssapApplicationId) {
					var pathURL = '/hix/indportal/getapplicationstatusbyid/' + window.localStorage.ssapApplicationId;
					$.get(pathURL, function(resp) {
						var jsonStr = JSON.parse(resp);
						if (jsonStr && jsonStr.applicationStatus && jsonStr.applicationStatus === 'CL') {
							$('#app-status-closed-warning-light-box').removeClass("hide").modal({backdrop:"static",keyboard:"false"});
						}
					});
				}
			}
			if (!appStatusInterval) {
				// setInterval(validateAppStatusInMain, 1000*5);
			}

			$('a.lang').click(function (){

				var val= $(this).attr('id');

				var currUrl = window.location.href;
				var newUrl="";

				///alert(val);
				/* Check if Query String parameter is set or not
				* If yes
				*/
				if(currUrl.indexOf("?", 0) > 0)
				{
					/* Check if locale is already set without querystring param  */
					if(currUrl.indexOf("?lang=", 0) > 0)
						newUrl = "?lang="+val;
					/*  Check if locale is already set with querystring param  */
					else if(currUrl.indexOf("&lang=", 0) > 0)
						newUrl = currUrl.substring(0, currUrl.length-2)+val;
					else
						newUrl = currUrl + "&lang="+val;
				}/* If No  */
				else
					newUrl = currUrl + "?lang="+val;
				window.location = newUrl;
			});
		});


		// $(document).ready(function(){
		//     $('a.not-enabled').popover({
		//            placement:'bottom',
		//            animation:'true',
		//            delay: { show: 500, hide: 100 }
		//     });
		// });
		</script>
		<sec:authorize access="isAuthenticated()" var="isUserAuthenticated">
			<script type="text/javascript">
			function countUnreadMessages(currentTime) {

			var pathURL = "${pageContext.request.contextPath}/inbox/secureInboxUnread";
			var csrftoken;
			csrftoken= '${sessionScope.csrftoken}';

			$.post(pathURL,{ csrftoken:csrftoken,currentTime:currentTime}, function(data) {
			//HIX-38087
			if(data != null && data>0) {
			//$(".unreadRecords").show();
			$(".unreadRecords").removeClass("superHide");
			$(".unreadRecords").html(data);
			}
			else {
			//$(".unreadRecords").hide();//.html("0");
			$(".unreadRecords").addClass("superHide");

			/* HIX-45273 */
			$("#notificationMessages .unreadRecords").html(0).removeClass("superHide");
			}
			});
			//return data;
			}
			$(document).ready(function(){
			var secureInboxUnread = '${sessionScope.unreadRecords}';
			var currentTime = new Date().getTime();
			var lastUpdatedTime = '${sessionScope.lastUpdatedTime}';
			var counter = currentTime-lastUpdatedTime;
			if(secureInboxUnread == null || secureInboxUnread =='' || counter >= 300000){
			countUnreadMessages(currentTime);
			}else{
			$(".unreadRecords").removeClass("superHide");
			$(".unreadRecords").html(secureInboxUnread);
			}
			});
			//setInterval(countUnreadMessages, 10000);
			</script>
		</sec:authorize>
			<%
	//String logoutUrl = request.getContextPath()+"/account/user/logout";

	/* if session attribute is not set  */
	if(request.getParameter("lang") == null && session.getAttribute("lang") == null){
		session.setAttribute("lang","en");
	}
	else if(request.getParameter("lang") !=null && request.getParameter("lang") != session.getAttribute("lang")   ){
			session.setAttribute("lang", request.getParameter("lang"));
	}

	String enval=null;String esval=null;


	if(session.getAttribute("lang") !=null && session.getAttribute("lang").equals("en")  )
		enval="selected";
	else if(session.getAttribute("lang") !=null && session.getAttribute("lang").equals("es"))
		esval="selected";
	else
		enval="";
%>
		<c:set var="userActiveRoleName" value="<%=userActiveRoleName%>" />
		<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" />

		<input type="hidden" value="<%=session.getAttribute("lang")%>" id="lang">

		<div class="navbar navbar-fixed-top navbar-inverse" id="masthead" role="banner">
		<input type="hidden" id="exchangeName" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>">
		<input type="hidden" id="stateCode" value="${stateCode}"/>
		<input type="hidden" id="userActiveRoleName" value="${userActiveRoleName}"/>
		<input type="hidden" id="userActiveRoleLabel" value="${userActiveRoleLabel}"/>
		<c:if test="${isUserAuthenticated=='true'}">
			<!-- HIX-27186 User has only one role (default role) then do not show masthead with multi role drop down  -->
			<c:if test="${userRoles.size() > 1}">
				<div id="first-menu-bgcolor-nm"></div>
			</c:if>
		</c:if>
		<div class="navbar-inner">
		<div class="container" >
		<button type="button" aria-label="navigation-button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<ul class="nav">
		<li>

		<c:if test="${isUserAuthenticated=='false'}">
			<a href="#" title="Home page link opens in new window or tab" onclick="openInNewTab('<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL)%>')"><img class="brand" src="<gi:cdnurl value="/resources/img/" /><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE)%>"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/></a>
		</c:if>

		<c:if test="${isUserAuthenticated=='true'}">
			<c:choose>
				<c:when test="${isUserFullyAuthorized != null && isUserFullyAuthorized=='false' }">
					<a href="#"><img class="brand masthead-img" src="<gi:cdnurl value="/resources/img/" /><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE)%>"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/><span style="display: none;"><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%></span></a>
				</c:when>
				<c:otherwise>
					<a href="#" title="Link open in new window or tab" onclick="openInNewTab('<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL)%>')"><img class="brand masthead-img" src="<gi:cdnurl value="/resources/img/" /><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE)%>"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/></a>
				</c:otherwise>
			</c:choose>
			<!-- HIX-27186 User has only one role (default role) then do not show masthead with multi role drop down  -->
			<c:if test="${userRoles.size() > 1}">


			</c:if>
		</c:if>

		</li>
		</ul>
		<div class="nav-collapse collapse pull-right">
		<c:if test="${isUserAuthenticated=='true'}">
			<!-- HIX-27186 User has only one role (default role) then do not show masthead with multi role drop down  -->
			<c:if test="${userRoles.size() > 1}">
				<div id="role-header-nm">
				<ul class="nav pull-right">



				<li><a href=<%=logoutUrl%>>Logout</a></li>

				<c:if test="${userActiveRoleName=='employee' && isUserSwitchToOtherView=='N'}">
					<c:choose>
						<c:when test="${isUserFullyAuthorized != null && isUserFullyAuthorized=='false' }">
							<li id="masthead-account-settings" hidden="true"><a href="#">Account Settings</a></li>
						</c:when>
						<c:otherwise>
							<li id="masthead-account-settings"><a href="<c:url value='/shop/employee/spplanselection/accountsettings'/>">Account Settings</a></li>
						</c:otherwise>
					</c:choose>
				</c:if>

				<c:if test="${isUserSwitchToOtherView == 'N' && userActiveRoleName!='employee'}">
					<c:choose>
						<c:when test="${isUserFullyAuthorized != null && isUserFullyAuthorized=='false' }">
							<li id="masthead-account-settings" hidden="true"><a href="#">Account Settings</a></li>
						</c:when>
						<c:otherwise>
							<li id="masthead-account-settings"><a href="<c:url value='/account/user/accountsettings'/>">Account Settings</a></li>
						</c:otherwise>
					</c:choose>
				</c:if>

				<li class="dropdown">
				<c:choose>
					<c:when test="${(sUser.defRole.name).equalsIgnoreCase(userActiveRoleName)}">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-key"></i> ${userActiveRoleLabel}  (Change<b class="caret"></b>) </a>
					</c:when>
					<c:otherwise>
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">${userActiveRoleLabel}  (Change<b class="caret"></b>) </a>
					</c:otherwise>
				</c:choose>
				<ul class="dropdown-menu">
				<li><a>My Roles</a></li>
				<li class="divider"></li>
				<!-- HIX-27183 : Showing roles from UserRole in masthead.jsp and providing hyperlink to only active roles of user -->
				<c:forEach var="assigned" items="${userRoles}">
					<c:set var="ecryptedrolename" ><encryptor:enc value="${assigned.role.name}" isurl="true"/> </c:set>
					<c:choose>
						<c:when test="${sUser.defRole.name == assigned.role.name}">
							<!-- HIX-27183 : Showing tick mark on the active role of user -->
							<li><a href="<c:url value="/account/user/switchUserRole/${ecryptedrolename}?assigned=true" />"><c:if test="${userActiveRoleLabel == assigned.role.label}"><i class="icon-ok"></i></c:if><i class="icon-key"></i> ${assigned.role.label}</a></li>
						</c:when>
						<c:otherwise>

							<c:choose>
								<c:when test="${userActiveRoleLabel == assigned.role.label}">
									<!-- HIX-27183 : Showing tick mark on the active role of user -->
									<li> <a href="<c:url value="/account/user/switchUserRole/${ecryptedrolename}?assigned=true" />"><i class="icon-ok"></i>${assigned.role.label}</a></li>
								</c:when>
								<c:otherwise>
									<c:if test="${assigned.isActiveUserRole()==true}">
										<li><a href="<c:url value="/account/user/switchUserRole/${ecryptedrolename}?assigned=true" />">
										${assigned.role.label}</a></li>
									</c:if>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				</ul>
				</li>




				</ul>
				</div>

			</c:if>
		</c:if>

		<ul class="nav pull-right clearfix" id="second-menu-nm">
        <li id="skip_main_content"><a href="#container">Skip to Main Content</a></li>
		<c:if test="${isUserAuthenticated=='true'}">
<%--			<script>--%>
<%--			    console.log("userActiveRoleName: " +userActiveRoleName.value);--%>
<%--			</script>--%>
			<c:if test="${fn:toLowerCase(userActiveRoleLabel) eq fn:toLowerCase('individual') || userActiveRoleLabel=='External Assister'}">
				<c:if test="${fn:toLowerCase(userActiveRoleLabel) eq fn:toLowerCase('individual')}">
					<li><a href="<c:url value="${mnsureEnv}/CitizenPortal/application.do?AssociateAssistor" />"> <spring:message code="indportal.portalhome.header.manageAssister" javaScriptEscape="true"/>  </a></li>
				</c:if>
				<c:if test="${userActiveRoleLabel=='External Assister'}">
					<li><a href="<c:url value="${mnsureEnv}/NavigatorS" />"> Assister Home  </a></li>
				</c:if>
			</c:if>
			<li><a  href="#" title="Link open in new window or tab" onclick="openInNewTab('https://www.mnsure.org/learn-more/index.jsp')" ><spring:message code="indportal.portalhome.header.learnMore" javaScriptEscape="true"/></a></li><!---->
			<li><a  href="#" title="Link open in new window or tab" onclick="openInNewTab('https://www.mnsure.org/help/')" ><spring:message code="indportal.portalhome.header.getHelp" javaScriptEscape="true"/></a></li>
			<li><a  href=<%=logoutUrl%> onclick="logOut()"><spring:message code="indportal.portalhome.header.signOut" javaScriptEscape="true"/></a></li>
			<li><a href="#" id="language-select">Espa&ntilde;ol</a></li>
			<!--<li><a href="<c:url value="/account/user/switchNonDefRole/${userActiveRoleName}" />"><i class="icon-home" style="font-size: 18px;"></i><span class="aria-hidden">Home</span></a></li>-->

			<!-- HIX-27186 User has only one role (default role) then do not show masthead with multi role drop down  -->
			<%-- <c:if test="${userRoles.size()>1}">
                    <li id="masthead-dashboard"><a href="<c:url value="/account/user/switchNonDefRole/${userActiveRoleName}" />">Home</a></li>
            </c:if>  --%>
			<!--<li id="masthead-inbox"><a href="<c:url value="<%=myInboxUrl%>" />"><i class="icon-envelope"></i> Inbox <span class="unreadRecords superHide"></span></a></li>-->
			<%-- <c:if test="${userActiveRoleName=='employee' || userActiveRoleName=='individual'}"> --%>
			<c:if test="${userActiveRoleName=='individual'}">
				<!-- <li><a href="#">Cart</a></li>	-->	<!-- employee and Individual -->
			</c:if>


			<c:if test="${userRoles.size()==1}">



			</c:if>



		</c:if>

		<!-- Loged In Ends -->

		<!-- Logged Out -->
		<c:if test="${isUserAuthenticated=='false'}">

			<li><a  href="#" title="Link open in new window or tab" onclick="openInNewTab('https://www.mnsure.org/learn-more/index.jsp')" ><spring:message code="indportal.portalhome.header.learnMore" javaScriptEscape="true"/></a></li><!---->
			<li><a  href="#" title="Link open in new window or tab" onclick="openInNewTab('https://www.mnsure.org/help/')" ><spring:message code="indportal.portalhome.header.getHelp" javaScriptEscape="true"/></a></li>
			<li><a href="<c:url value='/account/user/login'/>"><spring:message code="indportal.portalhome.header.logIn" javaScriptEscape="true"/></a></li><!-- all -->
			<li><a href="#" id="language-select">Espa&ntilde;ol</a></li>
		</c:if>
		<!-- Logged Out Ends -->
		<!-- Menu for Employee, Employer, Individual, Admin, Broker, Carrier portals ENDS -->


		<!-- <script>
		// toggle for langages.
		$(document).ready(function(){

		if(!sessionStorage.getItem('language'))
		{
		sessionStorage.setItem('language','English');
		console.log("first set: " + sessionStorage.getItem('language'));
		$('#language-select').text("Espa\xF1ol");
		}else
		{

		console.log("already set as: " + sessionStorage.getItem('language'));
		}

		var language = sessionStorage.getItem('language');


		//set the initial language button value
		if(language =="English")
		{
		$('#language-select').text("Espa\xF1ol");
		}
		else{
		$('#language-select').text("English");
		}

		var originalUrl = window.location.href;
		var url = originalUrl;
		//console.log("original url: " + originalUrl);


		if(url.indexOf("lang")>0)
		{

		url = url.substring(0, url.indexOf("lang")-1);
		//console.log(url);
		}


		if(url.indexOf("?")<0)
		url += "?";
		else if(url.indexOf("?")>0 && url[url.length-1]!='&')
		url += "&";

		$('#language-select').click(function(){

		if(language == "English")
		{
		url += "lang=es";
		console.log("spanish:  " + url);
		window.location.href = url;
		$(this).text('English');
		sessionStorage.setItem('language','Spanish');
		console.log("changed to " + sessionStorage.getItem('language'));
		}
		else if(language == "Spanish")
		{
		url += "lang=en";
		console.log(("english: " + url));
		window.location.href = url;
		$(this).text('Espa\xF1ol');
		sessionStorage.setItem('language','English');
		console.log("changed to " + sessionStorage.getItem('language'));
		}

		else {
		sessionStorage.setItem("language", "English");
		$(this).text('Espa\xF1ol');
		window.location.href = originalUrl;
		}


		});
		});

		</script>
		-->



		</ul>
		</div>
		<!-- nav-collapse ends -->
		</div>
		<!-- container ends -->
		</div>
		<!-- navbar-inner ends -->
		</div>

		<div id="app-status-closed-warning-light-box" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="appStatusClosedWarningLightBox">
			<div id="appStatusClosedWarningLightBox" class="modal-header">
				<h3><spring:message code="appstatus.closed.header"/></h3>
			</div>
			<div class="modal-body">
				<spring:message code="appstatus.closed.warning"/>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" title="<spring:message code='indportal.portalhome.ok'/>" onclick="gotoDashboard();" data-dismiss="modal"><spring:message code="indportal.portalhome.ok"/></button>
			</div>
		</div>

		<script type="text/javascript">

			function gotoDashboard() {
				window.localStorage.ssapApplicationId = "";
				window.location = '/hix/indportal';
			}

			function logOut() {
				window.localStorage.ssapApplicationId = "";
			}

		/*
		HIX-18015  :  If the current url is one of registration flow, then
		it required to hide the following masthead urls;
		inbox
		My Account -->Account Settings
		My Account -->Dashboard

		*/
		var registrationUrls=new Array("account/securityquestions","/account/phoneverification/sendcode","/user/activation", "account/signup");

		var currDocUrl = document.URL;
		var urlIdx=0;
		var noOfRegUrls = registrationUrls.length;

		while(urlIdx <noOfRegUrls ){
		if(currDocUrl.indexOf(registrationUrls[urlIdx]) >0){
		$('#masthead-inbox').hide();
		$('#masthead-account-settings').hide();
		$('#masthead-dashboard').hide();
		}
		urlIdx++;
		}
		</script>

		<script>
		$('a.dropdown-toggle, .dropdown-menu a').on('touchstart', function(e) {
		e.stopPropagation();
		});
		</script>
		<!-- navbar  ends -->
