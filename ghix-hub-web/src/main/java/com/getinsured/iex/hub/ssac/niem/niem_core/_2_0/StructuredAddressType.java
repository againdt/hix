
package com.getinsured.iex.hub.ssac.niem.niem_core._2_0;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.String;
import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;
import com.getinsured.iex.hub.ssac.niem.usps_states._2_0.USStateCodeType;


/**
 * A data type for an address.
 * 
 * <p>Java class for StructuredAddressType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StructuredAddressType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}AddressDeliveryPoint" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationCityName" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationState" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationPostalCode" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationPostalExtensionCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructuredAddressType", propOrder = {
    "addressDeliveryPoint",
    "locationCityName",
    "locationState",
    "locationPostalCode",
    "locationPostalExtensionCode"
})
public class StructuredAddressType
    extends ComplexObjectType
{

    @XmlElementRef(name = "AddressDeliveryPoint", namespace = "http://niem.gov/niem/niem-core/2.0", type = JAXBElement.class)
    protected JAXBElement<?> addressDeliveryPoint;
    @XmlElement(name = "LocationCityName", nillable = true)
    protected ProperNameTextType locationCityName;
    @XmlElementRef(name = "LocationState", namespace = "http://niem.gov/niem/niem-core/2.0", type = JAXBElement.class)
    protected JAXBElement<?> locationState;
    @XmlElement(name = "LocationPostalCode", nillable = true)
    protected String locationPostalCode;
    @XmlElement(name = "LocationPostalExtensionCode", nillable = true)
    protected String locationPostalExtensionCode;

    /**
     * Gets the value of the addressDeliveryPoint property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     {@link JAXBElement }{@code <}{@link StreetType }{@code >}
     *     
     */
    public JAXBElement<?> getAddressDeliveryPoint() {
        return addressDeliveryPoint;
    }

    /**
     * Sets the value of the addressDeliveryPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     {@link JAXBElement }{@code <}{@link StreetType }{@code >}
     *     
     */
    public void setAddressDeliveryPoint(JAXBElement<?> value) {
        this.addressDeliveryPoint = value;
    }

    /**
     * Gets the value of the locationCityName property.
     * 
     * @return
     *     possible object is
     *     {@link ProperNameTextType }
     *     
     */
    public ProperNameTextType getLocationCityName() {
        return locationCityName;
    }

    /**
     * Sets the value of the locationCityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProperNameTextType }
     *     
     */
    public void setLocationCityName(ProperNameTextType value) {
        this.locationCityName = value;
    }

    /**
     * Gets the value of the locationState property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link USStateCodeType }{@code >}
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     
     */
    public JAXBElement<?> getLocationState() {
        return locationState;
    }

    /**
     * Sets the value of the locationState property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link USStateCodeType }{@code >}
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     
     */
    public void setLocationState(JAXBElement<?> value) {
        this.locationState = value;
    }

    /**
     * Gets the value of the locationPostalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationPostalCode() {
        return locationPostalCode;
    }

    /**
     * Sets the value of the locationPostalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationPostalCode(String value) {
        this.locationPostalCode = value;
    }

    /**
     * Gets the value of the locationPostalExtensionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationPostalExtensionCode() {
        return locationPostalExtensionCode;
    }

    /**
     * Sets the value of the locationPostalExtensionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationPostalExtensionCode(String value) {
        this.locationPostalExtensionCode = value;
    }

}
