package com.getinsured.eligibility.at.ref.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.iex.ssap.Address;

/**
 * @author chopra_s
 * 
 */
public class CompareApplicationDTO {
	private long id;

	private String applicationData;

	private String caseNumber;

	private String externalApplicationId;

	private List<CompareApplicantDTO> applicants = new ArrayList<CompareApplicantDTO>();

	private boolean hasAdminUpdate;

	private boolean hasDobChanged;

	private boolean hasAddressChanged;

	private boolean hasZipCountyChanged;
	
	private boolean hasCitizenShipStatusChanged;
	
	private boolean hasBloodRelationshipChanged;
	
	private EligibilityStatus eligibilityStatus;

	private ExchangeEligibilityStatus exchangeEligibilityStatus;

	private String csrLevel;

	private BigDecimal maximumAPTC;
	
	private EnrollmentAttributesDTO enrolledApplicationAttributes = new EnrollmentAttributesDTO();
	
	private Address mailingAddress = new Address();
	
	private String applicationStatus;

	private BigDecimal maximumStateSubsidy;
	
	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public Address getMailingAddress() {
		return mailingAddress;
	}

	public void setMailingAddress(Address mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public EnrollmentAttributesDTO getEnrolledApplicationAttributes() {
		return enrolledApplicationAttributes;
	}

	public void setEnrolledApplicationAttributes(EnrollmentAttributesDTO enrolledApplicationAttributes) {
		this.enrolledApplicationAttributes = enrolledApplicationAttributes;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isHasCitizenShipStatusChanged() {
		return hasCitizenShipStatusChanged;
	}

	public void setHasCitizenShipStatusChanged(boolean hasCitizenShipStatusChanged) {
		this.hasCitizenShipStatusChanged = hasCitizenShipStatusChanged;
	}

	public boolean isHasAdminUpdate() {
		return hasAdminUpdate;
	}

	public void setHasAdminUpdate(boolean hasAdminUpdate) {
		this.hasAdminUpdate = hasAdminUpdate;
	}

	public boolean isHasDobChanged() {
		return hasDobChanged;
	}

	public void setHasDobChanged(boolean hasDobChanged) {
		this.hasDobChanged = hasDobChanged;
	}

	public String getApplicationData() {
		return applicationData;
	}

	public void setApplicationData(String applicationData) {
		this.applicationData = applicationData;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getExternalApplicationId() {
		return externalApplicationId;
	}

	public void setExternalApplicationId(String externalApplicationId) {
		this.externalApplicationId = externalApplicationId;
	}

	public List<CompareApplicantDTO> getApplicants() {
		return applicants;
	}

	public void setApplicants(List<CompareApplicantDTO> applicants) {
		this.applicants = applicants;
	}

	public boolean isHasAddressChanged() {
		return hasAddressChanged;
	}

	public void setHasAddressChanged(boolean hasAddressChanged) {
		this.hasAddressChanged = hasAddressChanged;
	}

	public boolean isHasZipCountyChanged() {
		return hasZipCountyChanged;
	}

	public void setHasZipCountyChanged(boolean hasZipCountyChanged) {
		this.hasZipCountyChanged = hasZipCountyChanged;
	}

	public EligibilityStatus getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(EligibilityStatus eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public ExchangeEligibilityStatus getExchangeEligibilityStatus() {
		return exchangeEligibilityStatus;
	}

	public void setExchangeEligibilityStatus(ExchangeEligibilityStatus exchangeEligibilityStatus) {
		this.exchangeEligibilityStatus = exchangeEligibilityStatus;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public BigDecimal getMaximumAPTC() {
		return maximumAPTC;
	}

	public void setMaximumAPTC(BigDecimal maximumAPTC) {
		this.maximumAPTC = maximumAPTC;
	}

	public void addApplicant(CompareApplicantDTO applicant) {
		this.applicants.add(applicant);
		applicant.setCompareApplicationDTO(this);
	}

	public boolean isHasBloodRelationshipChanged() {
		return hasBloodRelationshipChanged;
	}

	public void setHasBloodRelationshipChanged(boolean hasBloodRelationshipChanged) {
		this.hasBloodRelationshipChanged = hasBloodRelationshipChanged;
	}

	public BigDecimal getMaximumStateSubsidy() {
		return maximumStateSubsidy;
	}

	public void setMaximumStateSubsidy(BigDecimal maximumStateSubsidy) {
		this.maximumStateSubsidy = maximumStateSubsidy;
	}
}
