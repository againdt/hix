package com.getinsured.eligibility.at.ref.service.nonfinancial;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.NFProcessDTO;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerBaseService;
import com.getinsured.eligibility.at.ref.service.ReferralActivationNotificationService;
import com.getinsured.eligibility.at.ref.service.ReferralLceNotificationService;
import com.getinsured.eligibility.at.ref.service.ReferralProcessingService;
import com.getinsured.eligibility.at.ref.service.ReferralSsapCmrLinkService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
@Component("referralRenNFDeterminationService")
@Scope("singleton")
public class ReferralRenNFDeterminationServiceImpl extends LceProcessHandlerBaseService implements ReferralNonFinancialService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralRenNFDeterminationServiceImpl.class);

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("referralActivationNotificationService")
	private ReferralActivationNotificationService referralActivationNotificationService;

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;

	@Autowired
	@Qualifier("cmrHouseholdRepository")
	private CmrHouseholdRepository cmrHouseholdRepository;

	@Autowired
	@Qualifier("referralNFCompareService")
	private ReferralNFCompareService referralNFCompareService;

	@Autowired
	@Qualifier("referralSsapCmrLinkService")
	private ReferralSsapCmrLinkService referralSsapCmrLinkService;

	@Autowired
	@Qualifier("iLocationRepository")
	private ILocationRepository iLocationRepository;
	
	@Autowired
	ReferralProcessingService referralProcessingService;

	private static final Set<String> APPLICATION_STATUS_CHECK = new HashSet<String>(Arrays.asList(ApplicationStatus.OPEN.getApplicationStatusCode(), ApplicationStatus.SIGNED.getApplicationStatusCode(),
	        ApplicationStatus.SUBMITTED.getApplicationStatusCode(), ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(), ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode()));

	private static final Set<String> APPLICATION_STATUS_ALLOWED = new HashSet<String>(Arrays.asList(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode()));

	@Override
	@ReferralTransactionAnno
	public boolean execute(NFProcessDTO nfProcessRequestDTO) {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("ReferralRenNFDeterminationServiceImpl starts for - " + nfProcessRequestDTO);
		}
		boolean blnComplete = false;
		final SsapApplication application = ssapApplicationRepository.findAndLoadApplicantsByAppId(nfProcessRequestDTO.getCurrentApplicationId());

		if (application == null) {
			LOGGER.error(ReferralConstants.NO_SSAP_FOUND + nfProcessRequestDTO.getCurrentApplicationId());
			throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + nfProcessRequestDTO.getCurrentApplicationId());
		}
		
		if(!nfProcessRequestDTO.isAutoLink()) {
			//Second parameter is householdCaseId in case of CA AT
			referralProcessingService.checkHouseholdExistsAndThenCreate(application.getId(),null);
			sendToActivation(nfProcessRequestDTO, application);
			blnComplete = true;
		}

		/*if (nfProcessRequestDTO.isNonFinancialCmr()) {
			boolean blnNFCmr = determineLogicForMatchingNFCmr(nfProcessRequestDTO, application);
			if (!blnNFCmr) {
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("No Matching NF application household found for primary applicant for application - " + nfProcessRequestDTO.getCurrentApplicationId());
				}
				sendToActivation(nfProcessRequestDTO, application);
				return true;
			}
		} else {
			boolean blnNFCmr = determineLogicForMatchingCmr(nfProcessRequestDTO, application);
			if (!blnNFCmr) {
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("No Single Matching household found for primary applicant for application - " + nfProcessRequestDTO.getCurrentApplicationId());
				}
				sendToActivation(nfProcessRequestDTO, application);
				return true;
			}
		}

		blnComplete = handleSingleConsumerFound(nfProcessRequestDTO, application);
		if (blnComplete) {
			sendToActivation(nfProcessRequestDTO, application);
		} else {
			linkApplicationToHousehold(application, nfProcessRequestDTO.getMatchingCmrId());
		}
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("ReferralRenNFDeterminationServiceImpl ends for - " + nfProcessRequestDTO);
		}*/
		return blnComplete;
	}

	private void linkApplicationToHousehold(SsapApplication application, int cmrId) {
		referralSsapCmrLinkService.executeLinkingWithNFCmr(application.getId(), cmrId);
	}

	private boolean handleSingleConsumerFound(NFProcessDTO nfProcessRequestDTO, SsapApplication application) {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Single Matching household found for primary applicant for application - " + nfProcessRequestDTO.getCurrentApplicationId() + ", CMR ID " + nfProcessRequestDTO.getMatchingCmrId());
		}
		final List<SsapApplication> cmrApplications = ssapApplicationRepository.findByCmrHouseoldId(new BigDecimal(nfProcessRequestDTO.getMatchingCmrId()));
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info(ReferralUtil.listSize(cmrApplications) + " - Applications found for cmr - " + nfProcessRequestDTO.getMatchingCmrId());
		}
		if (ReferralUtil.listSize(cmrApplications) == ReferralConstants.NONE) {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("No Applications found for cmr - " + nfProcessRequestDTO.getMatchingCmrId());
			}
			return true;
		}

		final long nfApplicationId = fetchApplicationId(cmrApplications, application);

		if (nfApplicationId == ReferralConstants.NONE) {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("No Matching Non Financial Application found for application - " + nfProcessRequestDTO.getCurrentApplicationId());
			}
			return true;
		}

		nfProcessRequestDTO.setNfApplicationId(nfApplicationId);

		final boolean blnCompare = referralNFCompareService.renewalMemberMatching(nfProcessRequestDTO);
		if (!blnCompare) {
			return true;
		}

		return false;
	}

	private long fetchApplicationId(List<SsapApplication> cmrApplications, SsapApplication application) {
		final long coverageCompare = application.getCoverageYear() - 1;
		boolean allowedFound = false;
		long applicationId = 0l;
		for (SsapApplication ssapApplication : cmrApplications) {
			if ((coverageCompare == ssapApplication.getCoverageYear()) && APPLICATION_STATUS_CHECK.contains(ssapApplication.getApplicationStatus())) {
				if (ReferralConstants.Y.equals(ssapApplication.getFinancialAssistanceFlag())) {
					return 0; // This case cannot happen
				}
				if (APPLICATION_STATUS_ALLOWED.contains(ssapApplication.getApplicationStatus())) {
					if (allowedFound) {
						return 0;
					}
					applicationId = ssapApplication.getId();
					allowedFound = true;
				} else {
					return 0;
				}
			}
		}
		return applicationId;
	}

	private boolean determineLogicForMatchingNFCmr(NFProcessDTO nfProcessRequestDTO, SsapApplication application) {
		final SsapApplicant primaryApplicant = ReferralUtil.retreiveApplicant(application, ReferralConstants.PRIMARY);
		final long matchingCmr = cmrHouseholdRepository.findMatchingCmrByProfile(ReferralUtil.capitalizeFirstLetter(primaryApplicant.getFirstName()), ReferralUtil.capitalizeFirstLetter(primaryApplicant.getLastName()),
		        primaryApplicant.getBirthDate(), nfProcessRequestDTO.getNfCmrId());
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Number of Matching NF household found for primary applicant for application - " + nfProcessRequestDTO.getCurrentApplicationId() + " : " + matchingCmr);
		}
		if (matchingCmr != ReferralConstants.NONE) {
			nfProcessRequestDTO.setMatchingCmrId(nfProcessRequestDTO.getNfCmrId());
			return true;
		}
		return false;
	}

	@SuppressWarnings("unused")
    private String retreivePrimaryZip(BigDecimal otherLocationId) {
		String zipCode = StringUtils.EMPTY;
		if (otherLocationId != null && otherLocationId.intValue() != ReferralConstants.NONE) {
			Location location = iLocationRepository.findOne(otherLocationId.intValue());
			if (location != null) {
				zipCode = location.getZip();
			}
		}
		return zipCode;
	}

	private boolean determineLogicForMatchingCmr(NFProcessDTO nfProcessRequestDTO, SsapApplication application) {
		final SsapApplicant primaryApplicant = ReferralUtil.retreiveApplicant(application, ReferralConstants.PRIMARY);
		final List<Integer> matchingCmr = cmrHouseholdRepository.findMatchingCmrByProfile(ReferralUtil.capitalizeFirstLetter(primaryApplicant.getFirstName()), ReferralUtil.capitalizeFirstLetter(primaryApplicant.getLastName()),
		        primaryApplicant.getBirthDate(),primaryApplicant.getSsn());
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Number of Matching household found for primary applicant for application - " + nfProcessRequestDTO.getCurrentApplicationId() + " : " + ReferralUtil.listSize(matchingCmr));
		}
		if (ReferralUtil.listSize(matchingCmr) == ReferralConstants.ONE) {
			nfProcessRequestDTO.setMatchingCmrId(matchingCmr.get(0));
			return true;
		}
		return false;
	}

	private void sendToActivation(NFProcessDTO nfProcessRequestDTO, SsapApplication ssapApplication) {
		ssapApplication.setAllowEnrollment(ReferralConstants.Y);
		setRenewalStatus(ssapApplication, RenewalStatus.SEND_ACTIVATION_LINK);
		updateSsapApplication(ssapApplication);
	}

	private void setRenewalStatus(SsapApplication ssapApplication, RenewalStatus status) {
		ssapApplication.setRenewalStatus(status);
	}

}
