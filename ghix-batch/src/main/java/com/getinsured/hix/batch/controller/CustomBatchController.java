package com.getinsured.hix.batch.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.admin.web.JobParametersExtractor;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.JobRegistry;
//import org.springframework.batch.core.configuration.support.MapJobRegistry;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobExecutionNotRunningException;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CustomBatchController{
	private static final Logger LOGGER = Logger.getLogger(CustomBatchController.class);

	@Autowired private SimpleJobOperator jobOperator;

	@Autowired private JobExplorer jobExplorer;

	@Autowired JobRepository jobRepository;

	@Autowired private	JobService jobService;

	@Autowired private JobRegistry ghixJobRegistry;

	//@Autowired private MapJobRegistry mapJobRegistry;

	private JobParametersExtractor jobParametersExtractor = new JobParametersExtractor();

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/listBatchJobs", method = RequestMethod.GET)
	public ResponseEntity<String> getBatchJobList() throws NoSuchJobException {
		LOGGER.debug("REST request to get list of batch jobs : {}");
		JSONObject jsonObject = new JSONObject();

		if(jobExplorer != null){
			//return the list of all configured jobs
			List<String> jobNameList = jobExplorer.getJobNames();
			JSONArray jsonJobArray = new JSONArray();
			for(String jobName : jobNameList){
				JSONObject jsonJob = new JSONObject();
				jsonJob.put("JobName", jobName);
				jsonJob.put("executionCount", jobService.countJobExecutionsForJob(jobName));
				jsonJob.put("launchable", jobService.isLaunchable(jobName));
				jsonJob.put("incrementable", jobService.isIncrementable(jobName));

				jsonJobArray.add(jsonJob);

			}
			jsonObject.put("Jobs", jsonJobArray);
		}

		return new ResponseEntity<String>(jsonObject.toString(), HttpStatus.OK);

	}

	private JobParameters createJobParameters(HashMap<String,String> jobParameterMap) {
		JobParametersBuilder builder = new JobParametersBuilder();
		if(jobParameterMap!=null){
			Iterator<String> it = jobParameterMap.keySet().iterator();
			while (it.hasNext()) {
				String paramName = it.next();
				builder.addString(paramName,jobParameterMap.get(paramName));
			}
		}
		builder.addLong("EXECUTION_DATE", new Date().getTime());  
		return builder.toJobParameters();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/stopJobExecution", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> stopJobExecution(@RequestBody String jobExecutionJson) {
		JSONObject jsonObject = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			JSONObject requestObj = (JSONObject) parser.parse(jobExecutionJson);
			
			String jobExecutionId = (String) requestObj.get("jobExecutionId") ;
			long executionId = Long.parseLong(jobExecutionId);
			JobExecution  jobExecution  = new JobExecution(executionId);
			jobExecution.stop();
			if(jobOperator.stop(executionId)){
				
					jsonObject.put("Success", "Batch job stopped successfully");
				
			}else{
				jsonObject.put("Error", "Stop Batch job failed");
			}
			
		} catch (NumberFormatException | NoSuchJobExecutionException | JobExecutionNotRunningException e) {
			LOGGER.error("Error stopping job" , e);
			jsonObject.put("Error", "Error in stopping batch job execution : " + e.getMessage());
		} catch (ParseException e) {
			LOGGER.error("Error parsing json" , e);
			jsonObject.put("Error", "Error in parsing input json : " + e.getMessage());
		}
		
		return new ResponseEntity<String>(jsonObject.toJSONString(), HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/getJobStatus", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getJobStatus(@RequestParam("jobExecutionId") String jobExecutionId) {
		JSONObject jsonObject = new JSONObject();
		JSONParser parser = new JSONParser();
		BatchStatus status = null;
		try {
			long executionId = Long.parseLong(jobExecutionId);
			JobExecution je = jobService.getJobExecution(executionId);
			
			if(je != null) {
				jsonObject.put("exitCode", je.getExitStatus().getExitCode());
				jsonObject.put("exitDescription", je.getExitStatus().getExitDescription());
				jsonObject.put("executionId", executionId);
				jsonObject.put("Status", je.getStatus().getBatchStatus().name());
			}
			return new ResponseEntity<String>(jsonObject.toString(), HttpStatus.OK);
			
		} catch ( NoSuchJobExecutionException e) {
			LOGGER.error("Error parsing json" , e);
			jsonObject.put("Status", "Failed to get job status");
			jsonObject.put("Error in getting job status: ", e.getMessage());
			return new ResponseEntity<String>(jsonObject.toJSONString(), HttpStatus.BAD_REQUEST);
		}
		
	
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/launchJob", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> launchJob(@RequestBody String jobDetails) {
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = new JSONObject();
	   LOGGER.info("jobDetails:" + jobDetails);
		try {
			JSONObject requestObj = (JSONObject) parser.parse(jobDetails);
			
			String jobName = (String) requestObj.get("jobName") ;
			String params = (String) requestObj.get("jobParams") ;
			 LOGGER.info("jobName:" + jobName);
			 LOGGER.info("params:" + params);
			if(jobName != null && !jobName.isEmpty()){
			JobParameters jobParameters = null;

			if(params != null && params.compareToIgnoreCase("null") != 0){
				jobParameters = jobParametersExtractor.fromString(params);
			}else{
				jobParameters = 
						new JobParametersBuilder()
						.addLong("time",System.currentTimeMillis()).toJobParameters();

			}
				
			SimpleJobLauncher  jobLauncher = new SimpleJobLauncher(); 

			jobLauncher.setJobRepository(jobRepository);
			 LOGGER.info("before ghixJobRegistry");
			if(ghixJobRegistry != null){
				Job job =  ghixJobRegistry.getJob(jobName);
				 LOGGER.info("in ghixJobRegistry job:" + job);
				jobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());
				try {
					JobExecution je = jobLauncher.run(job, jobParameters) ; // (JobExecution) batchSchedulingLauncher.launch();

					if(je != null){
						long jobId = je.getJobId();
						long executionId = je.getId();
						//Start a thread to monitor the launched job
						jsonObject.put("JobId", jobId );
						jsonObject.put("executionId", executionId);
						jsonObject.put("Status", "Job launched successfully");
					}else{
						jsonObject.put("Error", "The job could not be launched, JobExecution is null");
					}

				} catch (Exception e) {
					LOGGER.error("Exception in launching the  job:" + e);
					jsonObject.put("Status", "Job could not be launched");
					jsonObject.put("Error", "The job launch failed with error: " + e.getMessage());
				}
			}
			}else{
				jsonObject.put("Status", "Job could not be launched");
				jsonObject.put("Error", "The job name not provided correctly");
			}


		}catch (Exception e) {
			LOGGER.error("Exception in launching the scheduled job:" + e);
			jsonObject.put("Status", "Job could not be launched");
			jsonObject.put("Error", "The job launch failed with error:" + e.getMessage());
		}
		
		
		return  new ResponseEntity<String>(jsonObject.toJSONString(), HttpStatus.OK);
		
	}
}
