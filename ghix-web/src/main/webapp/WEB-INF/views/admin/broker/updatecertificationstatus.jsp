<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page contentType="text/html" import="java.util.*" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<style>
#fileUoload{
	height:auto !important;
}

#fileUoload .modal-body{
	height:auto !important;
}
</style>

<%
String myInboxUrl;
if ( GhixConstants.MY_INBOX_URL.contains("?")){
	myInboxUrl = GhixConstants.MY_INBOX_URL + "&languageCode=" + session.getAttribute("lang") ;
}else{
	myInboxUrl = GhixConstants.MY_INBOX_URL + "?languageCode=" + session.getAttribute("lang") ;
}
 String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
Calendar date = Calendar.getInstance();
date.setTime(new Date());
date.add(Calendar.YEAR, 1);
Date defaultEndDate = date.getTime();
Date currDateOnLocalScope = new Date();
%>




<div class="gutter10-lr">
	<fmt:formatDate value="<%= currDateOnLocalScope %>" pattern="MM/dd/yyyy" var="dateForJscript" />
	<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
		<div class="l-page-breadcrumb">
			<!--start page-breadcrumb -->
			<div class="row-fluid">
				<ul class="page-breadcrumb" style="margin-top:0px;">
					<li><a href="javascript:history.back()">&lt; <spring:message
								code="label.back" /></a></li>
					<li><a href="<c:url value="/admin/broker/manage"/>"><spring:message code="label.brkAgent"/> </a></li>
					<li><spring:message code="label.brkManage"/></li>
				</ul><!--page-breadcrumb ends-->
			</div>
			<!-- end of .row-fluid -->
		</div>
		<!--l-page-breadcrumb ends-->
		<div class="row-fluid">
			<h1><a name="skip"></a>${broker.user.firstName}&nbsp;${broker.user.lastName}</h1>
		</div>

	<div class="row-fluid">
		<div id="sidebar" class="span3">
				<div class="header"></div>
				<ul class="nav nav-list">
					<li><a href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkBrokerInformation"/></a></li>
					<li><a href="/hix/admin/broker/viewprofile/${encBrokerId}"><spring:message code="label.profile"/></a></li>
					<li class="active"><a href="/hix/admin/broker/certificationstatus/${encBrokerId}"><spring:message code="label.brkCertificationStatus"/></a></li>
					<c:if test="${CA_STATE_CODE}">
						<li id="brkActivityStatus"><a href="/hix/admin/broker/activitystatus/${encBrokerId}"><spring:message code="label.agentactivitystatus"/></a></li>
					</c:if>
					<li><a href="/hix/admin/broker/comments/${encBrokerId}"><spring:message code="label.brkComments"/></a></li>
					<c:if test="${CA_STATE_CODE}">
						<li><a href="#"  onclick="window.open('<%=myInboxUrl%>&userId=${encryptedUserName}');"><spring:message code="label.brkSecureInbox"/></a></li>
					</c:if>
					<c:if test="${!CA_STATE_CODE && broker.user != null}">
						<li><a href="/hix/admin/broker/tickethistory/${encBrokerId}"><spring:message code="label.brkTicketHistory"/></a></li>
						<%-- <li><a href="/hix/admin/broker/securityquestions/${encBrokerId}"><spring:message code="label.brkSecurityQuestions"/></a></li> --%>
					</c:if>
				</ul>

				 <br>
			<c:if test="${!CA_STATE_CODE}">
				<div class="header"><h4><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></h4></div>
				<ul class="nav nav-list">
				<c:if test="${checkDilog == null}">
					<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
				</c:if>
				<c:if test="${checkDilog != null}">
					<li class="navmenu"><a href="/hix/admin/broker/dashboard?switchToModuleName=<encryptor:enc value="broker"/>&switchToModuleId=<encryptor:enc value="${broker.id}"/>&switchToResourceName=${broker.user.firstName}" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
				</c:if>
	            </ul>
	         </c:if>

		</div><!-- end of span3 -->
	<div class="span9" id="rightpanel">
	 <form class="form-horizontal" id="frmupdatecertification"  enctype="multipart/form-data" name="frmupdatecertification" method="POST" action="<c:url value="/admin/broker/updatecertificationstatus"/>">
			 <df:csrfToken/>
			 <div class="row-fluid">
			            <div style="font-size: 14px; color: red">
			              <table>
			              	<c:forEach items="${fieldError}" var="violation">
			                    <tr>
			                        <td><p>
			                         		<c:catch var="exception"> <c:if test="${violation.defaultMessage !=null}"> </c:if>   </c:catch>
											<c:if test="${empty exception}">
												 <c:choose>
													 	<c:when  test="${violation.defaultMessage != null && fn:indexOf(violation.defaultMessage, '{') eq 0}">
										       			  <spring:message  code="${fn:replace(fn:replace(violation.defaultMessage,'{',''),'}','')}"/>
										    		</c:when>
										    		<c:otherwise>
										    		  ${violation.field}	${violation.defaultMessage}
										    		</c:otherwise>
												 </c:choose>
											</c:if>
											<c:catch var="exception"> <c:if test=" ${violation.message !=null}"> </c:if>    </c:catch>
											<c:if test="${empty exception}">
												 <spring:message  code="${fn:replace(fn:replace(violation.message,'{',''),'}','')}"/>
											</c:if>

			                        	<p/>
			                        </td>
			                    </tr>
			                </c:forEach>
			                </table>

			            </div>
			    </div>

			<input type="hidden" id="brokerId" name="brokerId" value="<encryptor:enc value="${broker.id}"/>"/>
			<input type="hidden" id="licenseNumber" name="licenseNumber" value="${broker.licenseNumber}"/>
			<input type="hidden" id="documentId_EODeclPage" name="documentId_EODeclPage" value=""/>
			<input type="hidden" id="documentId_Contract" name="documentId_Contract" value=""/>
			<input type="hidden" id="documentId_Supporting" name="documentId_Supporting" value=""/>
				<div class="row-fluid">
					<div class="header">
						<h4 class="pull-left"><spring:message code="label.brkCertificationStatus"/></h4>
						<a class="btn btn-small pull-right"  onclick="clearUploadComponents();" href="/hix/admin/broker/certificationstatus/${encBrokerId}"><spring:message code="label.brkCancel"/></a>
					</div>
					<div class="row-fluid">
					<table class="table table-border-none table-condensed table-auto verticalThead">
						<tbody>
								    <tr>
								        <td class="span4 txt-right" scope="row"><spring:message code="label.brkNumber"/></td>
							            <td><strong>${broker.brokerNumber}</strong></td>
						            </tr>
						            <tr>
								       <td class="span4 txt-right"><spring:message  code="label.brkApplicationSubmissionDate"/></td>
								       <td><strong><fmt:formatDate value="${broker.applicationDate}" pattern="MM-dd-yyyy"/></strong></td>
							        </tr>
									<tr>
										<td class="span4 txt-right"><spring:message code="label.brkCertificationStatus"/></td>
										<td><strong>${broker.certificationStatus}</strong></td>
									</tr>
									<tr>
								       <td class="span3 txt-right"><spring:message code="label.brkCertificationNumber"/></td>
								       <td class="span6"><strong>${broker.certificationNumber}</strong></td>
							       </tr>
							       <tr id="certificationDate">
										<td class="span4 txt-right"><spring:message code="label.brkCertificationDate"/>
										<td>
											<strong>
												<c:choose>
													<c:when test="${newBroker.certificationDate !=null}">
														<fmt:formatDate value= '${newBroker.certificationDate}' pattern="MM-dd-yyyy"/>
													</c:when>
													<c:otherwise>
														<c:choose>
															<c:when test="${broker.certificationDate !=null}">
																<fmt:formatDate value= '${broker.certificationDate}' pattern="MM-dd-yyyy"/>
															</c:when>
															<c:otherwise>
															</c:otherwise>
														</c:choose>
													</c:otherwise>
												</c:choose>
											</strong>
										 </td>
									</tr>
									<tr id="renewalDate">
										<td class="span4 txt-right"><spring:message code="label.brkRenewalDate"/>
										<td>
											<strong>
												<c:choose>
													<c:when test="${newBroker.reCertificationDate !=null}">
													<fmt:formatDate value= '${newBroker.reCertificationDate}' pattern="MM-dd-yyyy"/>
												     </c:when>
												     <c:otherwise>
												     	<c:choose>
														     <c:when test="${broker.reCertificationDate !=null}">
														     <fmt:formatDate value= '${broker.reCertificationDate}' pattern="MM-dd-yyyy"/>
														     </c:when>
															<c:otherwise>
															</c:otherwise>
														</c:choose>
													</c:otherwise>
												</c:choose>
											</strong>
									 	</td>
									</tr>
									<tr>
										<td class="span4 txt-right"><spring:message code="label.brkDelegationCode"/></td>
										<td><strong>${broker.delegationCode}</strong></td>
									</tr>
							</tbody>
						</table>
					</div>
					<div class="header">
						<h4 class="pull-left"><spring:message code="label.brkUpdateCertification"/></h4>
					</div>
					<div class="clearfix"></div>
					<div class="row-fluid">
					<table class="table table-border-none table-condensed table-auto verticalThead">

						<!-- header class for accessibility -->
								<tbody>
									<tr id="startDateId">
									<td class="span4 txt-right"><spring:message code="label.brkStartDate"/></td>
									<td> <div class="input-append date date-picker" id="date" data-date="">

									<c:choose>
										<c:when test="${currDate < brokerCertificationDate}">
											 <input class="span10" type="text" name="newStartDate" id="newStartDate"  value= "<fmt:formatDate value= '${broker.certificationDate}' pattern="MM/dd/yyyy"/>" pattern="MM/dd/yyyy"/> <span class="add-on"><i class="icon-calendar"></i></span>
										</c:when>
										<c:otherwise>
											<input class="span10" type="text" name="newStartDate" id="newStartDate"  value= "<fmt:formatDate value= '<%= new java.util.Date() %>' pattern="MM/dd/yyyy"/>" pattern="MM/dd/yyyy"/> <span class="add-on"><i class="icon-calendar"></i></span>
										</c:otherwise>
									</c:choose>

									</div>
									<div id="newStartDate_error"></div>
									 </td>
									</tr>
									<tr id="endDateId">
										<td class="span4 txt-right"><spring:message code="label.brkEndDate"/>
									</td>
									<td> <div class="input-append date date-picker" id="date" data-date="" >
									<%-- <c:choose>
										<c:when test="${newBroker.reCertificationDate !=null}">
									        <input class="span10" type="text" name="newEndDate" id="newEndDate"  value= "<fmt:formatDate value= '${newBroker.reCertificationDate}' pattern="MM/dd/yyyy"/>" pattern="MM/dd/yyyy"/> <span class="add-on"><i class="icon-calendar"></i></span>
									     </c:when>
									     <c:otherwise>
									     	<c:choose>
											     <c:when test="${broker.reCertificationDate !=null}">
											        <input class="span10" type="text" name="newEndDate" id="newEndDate"  value= "<fmt:formatDate value= '${broker.reCertificationDate}' pattern="MM/dd/yyyy"/>" pattern="MM/dd/yyyy"/> <span class="add-on"><i class="icon-calendar"></i></span>
											     </c:when>
												<c:otherwise> --%>
													<input class="span10" type="text" name="newEndDate" id="newEndDate"  value= "<fmt:formatDate value= '<%=defaultEndDate%>' pattern="MM/dd/yyyy"/>" pattern="MM/dd/yyyy"/> <span class="add-on"><i class="icon-calendar"></i></span>
											<%-- 	</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>	 --%>

									</div>
									<div id="newEndDate_error"></div>
									 </td>
									</tr>

									<tr>
										<td class="span4 txt-right"><label for="certificationStatus"><spring:message code="label.brkNewStatus"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label></td>
										<td>
										<select  id="certificationStatus" name="certificationStatus" class="input-large">
											<option value="">Select</option>
											<c:forEach  items="${statuslist}" var= "value">
												<option value="${value}" <c:if test="${newBroker.certificationStatus == value}"> SELECTED </c:if>>${value}</option>
											</c:forEach>
										</select>
										<div id="certificationStatus_error"></div>
										</td>
									</tr>
									<tr>
										<td class="span4 txt-right"><label for="comments"><spring:message code="label.brkComment"/></label></td>
										<td><textarea id="comments" name= "comments" maxlength="4000" >${newBroker.comments}</textarea></td>
									</tr>

									<tr>
		                   			<td class="txt-right"><label for="fileInput"><spring:message code="label.brkUploadEODeclarationPage"/></label></td>
			                   			<td>
			                   				<div>
			                   					<input type="file" class="input-file" id="fileInputEODeclPage" name="fileInputEODeclPage" onchange="validateUploadDocument('fileInputEODeclPage');"/>
			                   					<input type="hidden" id="fileInputEODeclPage_Size" name="fileInputEODeclPage_Size" value="1"/>
				                   				<input type="button" id="btn_Upload_fileInputEODeclPage" name="btn_Upload_fileInputEODeclPage" value="<spring:message code="label.upload"/>" class="btn"  />
				                   				<div id="uploadedEODeclPageFileDiv"><label id="fileNameEODeclPage">${fileName_EODeclPage}</label></div>
			                   				</div>
			                   				<div>
			                   					<spring:message code="label.uploadDocumentCaption"/>
			                   				</div>
			                   				<div id="fileInputEODeclPage_error"></div>
			                   			</td>
	                  				</tr>
									<tr>
		                   			<td class="txt-right"><label for="fileInput"><spring:message code="label.brkUploadContract"/></label></td>
			                   			<td>
			                   				<div>
			                   					<input type="file" class="input-file" id="fileInputContract" name="fileInputContract" onchange="validateUploadDocument('fileInputContract');"/>
				                   				<input type="hidden" id="fileInputContract_Size" name="fileInputContract_Size" value="1"/>
				                   				<input type="button" id="btn_Upload_fileInputContract" name="btnUploadContract" value="<spring:message code="label.upload"/>" class="btn"  />
				                   				<div id="uploadedContractFileDiv"><label id="fileNameContract">${fileName_Contract}</label></div>
			                   				</div>
			                   				<div>
			                   					<spring:message code="label.uploadDocumentCaption"/>
			                   				</div>
			                   				<div id="fileInputContract_error"></div>
			                   			</td>
	                  				</tr>
									<tr>
		                   			<td class="txt-right"><label for="fileInput"><spring:message code="label.brkUploadSupportingDocs"/></label></td>
			                   			<td>
			                   				<div>
			                   					<input type="file" class="input-file" id="fileInput" name="fileInput" onchange="validateUploadDocument('fileInput');"/>
			                   					<input type="hidden" id="fileInput_Size" name="fileInput_Size" value="1"/>
				                   				<input type="button" id="btn_UploadDoc_fileInput" name="btnUploadDoc" value="<spring:message code="label.upload"/>" class="btn"  />
				                   				<div id="uploadedFileDiv"><label id="fileName">${fileName}</label></div>
			                   				</div>
			                   				<div>
			                   					<spring:message code="label.uploadDocumentCaption"/>
			                   				</div>
			                   				<div id="fileInput_error"></div>
			                   			</td>
	                  				</tr>
							</tbody>
					</table>
					</div>
				</div>
			<div class="form-actions">
				<a class="btn cancel-btn"  onclick="clearUploadComponents();" href="/hix/admin/broker/certificationstatus/${encBrokerId}"><spring:message code="label.brkCancel"/></a>
			    <input type="hidden" id="fileToUpload" name="fileToUpload" value=""/>
				<input type="button" class="btn btn-primary" name="update" id="updateCertStatus" value="<spring:message code="label.brkSubmit"/>"/>
			</div>
			<input type="hidden" name="formAction" id="formAction" value="" />
			</form>
			<!-- Showing comments -->

<!-- below Table -->

				<div class="gutter10">
							<display:table id="brokerStatusHistory" name="brokerStatusHistory" list="brokerStatusHistory" requestURI="" sort="list" class="table table-condensed table-border-none table-striped" >
								   <display:column property="updated" titleKey="label.brkdate"  format="{0,date,MMM dd, yyyy}" sortable="false" headerClass="graydrkbg" />
								   <display:column property="previousCertificationStatus" titleKey="label.bkr.previousStatus" sortable="false" headerClass="graydrkbg"/>
								   <display:column property="newCertificationStatus" titleKey="label.brkNewStatus" sortable="false" headerClass="graydrkbg"/>
								   <display:column property="comments" titleKey="label.brkComment" sortable="false" headerClass="graydrkbg"/>
								   <display:column titleKey="label.brkViewAttachment" headerClass="graydrkbg ">
										<c:choose>
											<c:when test="${brokerStatusHistory.eoDeclarationDocumentId == null && brokerStatusHistory.contractDocumentId == null && brokerStatusHistory.supportingDocumentId == null}">
												<spring:message code="label.brkNoAttachment"/>
											</c:when>
											<c:otherwise>
												<c:if test="${brokerStatusHistory.eoDeclarationDocumentId != null}">
													  <c:set var="eoDeclarationDocumentId" ><encryptor:enc value="${brokerStatusHistory.eoDeclarationDocumentId}" isurl="true"/> </c:set>
													  <a href="#" onClick="showdetail('${eoDeclarationDocumentId}');" id="edit_${brokerStatusHistory.eoDeclarationDocumentId}"><spring:message code="label.brkViewAttachmentEODeclPage"/></a>
													  <br/>
												</c:if>
												<c:if test="${brokerStatusHistory.contractDocumentId != null}">
													<c:set var="contractDocumentId" ><encryptor:enc value="${brokerStatusHistory.contractDocumentId}" isurl="true"/> </c:set>
													  <a href="#" onClick="showdetail('${contractDocumentId}');" id="edit_${brokerStatusHistory.contractDocumentId}"><spring:message code="label.brkViewAttachmentContract"/></a>
													  <br/>
												</c:if>
												<c:if test="${brokerStatusHistory.supportingDocumentId != null}">
													<c:set var="supportingDocumentId" ><encryptor:enc value="${brokerStatusHistory.supportingDocumentId}" isurl="true"/> </c:set>
													  <a href="#" onClick="showdetail('${supportingDocumentId}');" id="edit_${brokerStatusHistory.supportingDocumentId}"><spring:message code="label.brkViewAttachmentSupporting"/></a>
												</c:if>
											</c:otherwise>
									   </c:choose>
								   </display:column>
							</display:table>
				</div><!-- end of .gutter10 -->
		</div><!-- end of span9 -->
	</div>


	<div id="modalBox" class="modal hide fade">
		<div class="modal-header">
			<a class="close" data-dismiss="modal" data-original-title="">x</a>
			<h3 id="myModalLabel"><spring:message code="label.bkr.viewComment"/></h3>
		</div>
		<div id="commentDet" class="modal-body">
			 <label id="commentlbl" ></label>
		</div>

		<div class="modal-footer">
			<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message code="label.button.close"/></a>
		</div>
	</div>

	<c:if test="${ (not empty sameBrokersList) and ( fn:length(sameBrokersList) gt 1 )    and ( CA_STATE_CODE )   }">
			<div id="multipleBrokerBox" data-backdrop="static" class="modal hide fade">
				<div class="modal-header">
					<a class="close" data-dismiss="modal" data-original-title="">x</a>
					<h3 id="modalHeaderLabel" ><spring:message code="label.brk.cert.save.success"/></h3>
				</div>
				<div id="commentDet" class="modal-body" style="background: #e7e7e7;" >
					<h4>
						<spring:message code="label.brk.additional.profile"/>
					</h4>

					<table class="table table-border-none table-condensed table-auto verticalThead">
						<thead>
							<c:forEach items="${sameBrokersList}" var="localBrokerObj">
									<c:if test="${localBrokerObj.id ne broker.id }" >
										<tr >
											<c:set var="encLocalBrokerId" ><encryptor:enc value="${localBrokerObj.id}" isurl="true"/> </c:set>
											<th  > <b> <input type="checkbox" name="brokerIds" value="${encLocalBrokerId}" /> </b> </th>
											<th  > <b>  <c:if test="${ empty localBrokerObj.companyName }" >  ----  </c:if>
													   <c:if test="${  not empty localBrokerObj.companyName }" >  ${localBrokerObj.companyName }  </c:if>
												  </b>
											</th>
											<th  > <b> <c:if test="${ empty localBrokerObj.user.firstName }" >  ${localBrokerObj.firstName}&nbsp;&nbsp; ${localBrokerObj.lastName}  </c:if>
														<c:if test="${ not empty localBrokerObj.user.firstName }" >  ${localBrokerObj.user.firstName }&nbsp;&nbsp;${localBrokerObj.user.lastName }  </c:if>
											       </b>
											</th>
											<th  > <b>
														<c:if test="${   empty localBrokerObj.user.email }" >  ${localBrokerObj.yourPublicEmail }   </c:if>
											      		<c:if test="${  not empty localBrokerObj.user.email }" >  ${localBrokerObj.user.email }   </c:if>
											      </b>
											</th>

										</tr>
								    </c:if>
							</c:forEach>
						</thead>
					</table>
				</div>

				<div class="modal-footer" style="background: #e7e7e7;">
					<%-- <a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message code="label.button.close"/></a> --%>

					<a href="#" class="btn btn-primary" style="text-transform: none;" id="submitSameBrokers" ><spring:message code="label.entity.updateStatus"/></a>
				</div>
			</div>
	</c:if>

			<div id="brokerStatusSaveInProgress" data-backdrop="static" data-keyboard="false" class="modal hide fade">
				<div class="modal-header">
					<h3 id="progressModalHeaderLabel" ><spring:message code="label.brk.cert.save.progress"/></h3>
				</div>
				<div  class="modal-body" style="background: #e7e7e7;" >
					<img src="<c:url value='/resources/js/upload/img/loader_greendots.gif'/>" alt="Progress bar" />
				</div>

				<div class="modal-footer" style="background: #e7e7e7;">

				</div>
			</div>

	


</div>

<script type="text/javascript">


		$('.date-picker').datepicker({
	        autoclose: true,
	        format: 'mm/dd/yyyy'
		});
	</script>
	<script type="text/javascript">

$(document).ready(function(){

            $('#startDateId').hide();
	    	$('#endDateId').hide();
	    	$("#uploadedEODeclPageFileDiv").hide();
	    	$("#uploadedContractFileDiv").hide();
	    	$("#uploadedFileDiv").hide();

	    	var button_id_names='#btn_Upload_fileInputEODeclPage,'+
			'#btn_Upload_fileInputContract,'+
			'#btn_UploadDoc_fileInput';

			$(button_id_names).click(function(){
										var idName = this.id;
										$("#"+idName).attr('disabled','disabled');

										var upload = idName.split("_");
										$('#fileToUpload').val(upload[2]);
										var file = $('#'+upload[2]).val();
										if(file!=""){
											var hiddenVar=upload[2]+'_Size';
  											if(document.getElementById(hiddenVar).value==1){
												$('#frmupdatecertification').ajaxSubmit({
														url: "<c:url value='/admin/broker/upload/${encBrokerId}'/>",
														success: function(responseText){

																		$("#"+idName).removeAttr('disabled');
																		if(responseText.indexOf('FAILURE') != -1){
																			$('<div class="modal popup-address" id="fileUoload"><div class="modal-header "><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" ><h4><spring:message code="label.failedtouoload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
																			return false;
																		}
																		if(responseText.indexOf('Missing CSRF token') != -1){
																			$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" ><h4><spring:message code="label.broker.brokerUnexpectedErrorOccured"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
																			return false;
																		}
																		var val = responseText.split("|");  /* For IE*/
																		if(val[2]==0){
																			///alert("Failed to Upload File");
																			$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" ><h4><spring:message code="label.failedtouoload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
																		}
																		else if(val[2]==-1){
																			$('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
																		}
																		else if(val[0]==""){
																			$('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.selecteachfilelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
																		}
																		else{
																			//alert("File uploaded Successfully")	;
																			if(val[0]=='fileInputEODeclPage'){
			 																	$("#documentId_EODeclPage").val(val[2]);
			 																	$("#uploadedEODeclPageFileDiv").show();
			 																	$("#fileNameEODeclPage").html(val[1]);
	 	 	 																	clearFileInput("fileInputEODeclPage");
		 																	}else if(val[0]=='fileInputContract'){
			 																	$("#documentId_Contract").val(val[2]);
			 																	$("#uploadedContractFileDiv").show();
			 																	$("#fileNameContract").html(val[1]);
			 																	clearFileInput("fileInputContract");
		 																	}else if(val[0]=='fileInput'){
			 																	$("#documentId_Supporting").val(val[2]);
			 																	$("#uploadedFileDiv").show();
			 																	$("#fileName").html(val[1]);
			 																	clearFileInput("fileInput");
		 																	}
																			$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center"><h4><spring:message code="label.fileuploadedsuccessfully"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
																		}


																},
														error : function(responseText){ /* For Firefox*/
																		$("#"+idName).removeAttr('disabled');
																		var parsedJSON = responseText;
																		var val = parsedJSON.responseText.split("|");
																		if(val[2]==0){
																				//alert("Failed to Upload File");
																				$('<div class="modal popup-address" id="fileUoload"><div class="modal-header "><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" ><h4><spring:message code="label.failedtouoload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
																		}
																		else if(val[2]==-1){
																			$('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
																		}
																		else if(val[0]==""){
																			$('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.selecteachfilelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
																		}
																		else{
																			//alert("File uploaded Successfully")	;
																			$('<div class="modal popup-address" id="fileUoload"><div class="modal-header" ><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" ><h4><spring:message code="label.fileuploadedsuccessfully"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
																		}
     	 																if(val[0]=='fileInputEODeclPage'){
				 															$("#documentId_EODeclPage").val(val[2]);
     	 																}else if(val[0]=='fileInputContract'){
				 															$("#documentId_Contract").val(val[2]);
     	 																} else if(val[0]=='fileInput'){
				 															$("#documentId_Supporting").val(val[2]);
     	 																}

	 															}
												});
										}else if(document.getElementById(hiddenVar).value==0) {
												//alert("Please select a file with size less than 5 MB");
												$('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});

												$("#"+idName).removeAttr('disabled');
												document.getElementById(upload[2]).value=null;
										}
									}
									else{
											//alert("Please select a file before upload");
											$('<div class="modal popup-address" id="fileUoload"><div class="modal-header " ><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center"><h4><spring:message code="label.selectFileBeforeUpload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});

											$("#"+idName).removeAttr('disabled');
										}
									return false;
			});	// button click close
	checkUploadStatus();
	
			$('#updateCertStatus').click( function() {

					//$("#modalHeaderLabel").text("Saving Certification Status.. In Progress");
					//$('#submitSameBrokers').attr("disabled","disabled");
					//submit old form first
					if($( "#frmupdatecertification" ).valid()){
						//Show Progress bar and Submit Form if valid
						$('#brokerStatusSaveInProgress').modal();
						 var clonedData = $('#frmupdatecertification').serialize();
							$.post(
              					'<c:url value="/admin/broker/updatebrokercertificationstatus" />',
               					  clonedData)
               						.done(function(msg){

										if(msg == "SUCCESS" ){
											//Hide Progress-Bar
										 	$('#brokerStatusSaveInProgress').modal('hide');
											//Enable Button submitSameBrokers
											//console.log(msg);
											//$('#submitSameBrokers').attr("disabled","enabled");
											//$("#modalHeaderLabel").text("Agent Status Successfully changed!");
										 	<c:choose>
												<c:when test="${ (not empty sameBrokersList) and ( fn:length(sameBrokersList) gt 1 )  and (CA_STATE_CODE)   }">
													$('#multipleBrokerBox').modal();
												</c:when>
												<c:otherwise>
													window.location.href = '<c:url value="/admin/broker/certificationstatus/${encBrokerId}" />';
												</c:otherwise>
											</c:choose>
											
										}else{
											// error handling    progressModalHeaderLabel
											$("#progressModalHeaderLabel").text('<spring:message code="label.brk.cert.save.error"/>');
											window.location.href = '<c:url value="/admin/broker/certificationstatus/${encBrokerId}" />';
										}

									 })
									.fail(function(xhr, status, error) {
       				 					// error handling    progressModalHeaderLabel
										$("#progressModalHeaderLabel").text('<spring:message code="label.brk.cert.save.error"/>');
										window.location.href = '<c:url value="/admin/broker/certificationstatus/${encBrokerId}" />';
    								});
					 }else{return; }


			});
			
			<c:if test="${ (not empty sameBrokersList) and ( fn:length(sameBrokersList) gt 1 )  and (CA_STATE_CODE)   }">

			$('#submitSameBrokers').click( function() {
				var checkBoxSelected = false;

				$("#frmupdatecertification").find('.dynamicallyAdded').remove();

				$('input[type="checkbox"][name="brokerIds"]:checked').each(function() {
						checkBoxSelected = true;
					   	console.log(this.value);
					   $('<input   />').attr('type', 'hidden')
				       		.attr('class', "dynamicallyAdded")
					   		.attr('name', "brokerIds")
				          	.attr('value', this.value)
				          	.appendTo('#frmupdatecertification');
				});

				if(checkBoxSelected){

					 if($( "#frmupdatecertification" ).valid()){
						//Submit Form if valid
						 $( "#frmupdatecertification" ).submit();
					 }else{
						  //Form is not valid
						 $('#multipleBrokerBox').modal('hide');
					 }

				}else{
					//Show message that not a single checkbox selected.
				}

			});

			$('#multipleBrokerBox').on('hidden.bs.modal', function () {
    			 window.location.href = '<c:url value="/admin/broker/certificationstatus/${encBrokerId}" />';
			});

			$('#multipleBrokerBox').on('hidden', function () {
    			 window.location.href = '<c:url value="/admin/broker/certificationstatus/${encBrokerId}" />';
			});


	</c:if>




});//Document ready close

/**
  * Function to clear file resource components.
  */
	function clearFileInput(controlName) {
      if(null != controlName & "undefined" !== controlName) {
              var errorContainer = $("#" + controlName + "_error");

              if(null != errorContainer & "undefined" !== errorContainer) {
                      errorContainer.html("");
                      errorContainer.attr({'class':null, 'role':null});
              }

              var control = $("#" + controlName + "");

              if(null != control & "undefined" !== control) {
                      control.wrap('<form>').parent('form').trigger('reset');
                      control.unwrap();
              }
      }
}

/**
 * Function to clear upload resource components.
 */
function clearUploadComponents() {
	$("#uploadedEODeclPageFileDiv").hide();
	$("#uploadedContractFileDiv").hide();
	$("#uploadedFileDiv").hide();

	$("#fileNameEODeclPage").val(null);
	$("#fileNameContract").val(null);
	$("#fileName").val(null);

	$("#documentId_EODeclPage").val(null);
	$("#documentId_Contract").val(null);
	$("#documentId_Supporting").val(null);

	$.post("/hix/admin/broker/resetuploads",
			 '',
			  function(data, status){
		});
}


function getComment(comments)
		{

			    $('#commentlbl').html("<p> Loading Comment...</p>");
				comments =comments.replace(/#/g,'<br/>'); //Added to remove new line break marker with HTML equivalent br
				comments=comments.replace(/apostrophes/g,"'"); //Added to replace regex apostrophes with '
				$('#commentlbl').html("<p> "+ comments + "</p>");
		}

		var validator = $("#frmupdatecertification").validate({
			onfocusout: false,
			rules : {
				certificationStatus : {required : true},
				newStartDate :{requiredWhenCertified :true,dateValidate : false,greaterThanToday : true},
				newEndDate :{requiredWhenCertified :true,dateValidate : true,greaterThanToday : true},
				fileInput:{fileInputUploadCheck : true},
				fileInputContract:{fileInputContractUploadCheck : true},
				fileInputEODeclPage:{fileInputEODeclPageUploadCheck : true},
			 },
			 messages : {
				certificationStatus : { required : "<span><em class='excl'>!</em><spring:message  code='label.validateCertificationStatus' javaScriptEscape='true'/></span>"},
				newStartDate : 		  { requiredWhenCertified : "<span><em class='excl'>!</em><spring:message  code='label.validateCertDate' javaScriptEscape='true'/></span>",
			    						dateValidate : "<span><em class='excl'>!</em><spring:message  code='label.validateFromAndToDate' javaScriptEscape='true'/></span>",
			    						greaterThanToday : "<span><em class='excl'>!</em><spring:message  code='label.errorDate' javaScriptEscape='true'/></span>"},
			    newEndDate :          { requiredWhenCertified : "<span><em class='excl'>!</em><spring:message  code='label.validateCertDate' javaScriptEscape='true'/></span>",
										dateValidate : "<span><em class='excl'>!</em><spring:message  code='label.validateFromAndToDate' javaScriptEscape='true'/></span>",
										greaterThanToday : "<span><em class='excl'>!</em><spring:message  code='label.errorDate' javaScriptEscape='true'/></span>"},
				fileInput : { fileInputUploadCheck : "<span><em class='excl'>!</em>Please Upload File Before Submit</span>"},
				fileInputContract : { fileInputContractUploadCheck : "<span><em class='excl'>!</em>Please Upload File Before Submit</span>"},
				fileInputEODeclPage : { fileInputEODeclPageUploadCheck : "<span><em class='excl'>!</em>Please Upload File Before Submit</span>"}
			 },
			 errorClass: "error",
			 errorPlacement: function(error, element) {
			 var elementId = element.attr('id');
			 error.appendTo( $("#" + elementId + "_error"));
			 $("#" + elementId + "_error").attr('class','error help-inline');
			}
		});

		jQuery.validator.addMethod("requiredWhenCertified", function(value, element, param) {
			var status=document.getElementById('certificationStatus').value;
				if(status == 'Certified'){
					if(value == ""){
						return false;
					}
				}
			return true;
		});

		jQuery.validator.addMethod("fileInputUploadCheck", function(value, element, param) {
			var file=document.getElementById('fileInput').value;
				if(file != ""){
					var docId=document.getElementById('documentId_Supporting').value;
					if(docId == ""){
						return false;
					}
				}
			return true;
		});

		jQuery.validator.addMethod("fileInputContractUploadCheck", function(value, element, param) {
			var file=document.getElementById('fileInputContract').value;
				if(file != ""){
					var docId=document.getElementById('documentId_Contract').value;
					if(docId == ""){
						return false;
					}
				}
			return true;
		});

		jQuery.validator.addMethod("fileInputEODeclPageUploadCheck", function(value, element, param) {
			var file=document.getElementById('fileInputEODeclPage').value;
				if(file != ""){
					var docId=document.getElementById('documentId_EODeclPage').value;
					if(docId == ""){
						return false;
					}
				}
			return true;
		});

		jQuery.validator.addMethod("dateValidate", function(value, element, param) {
			var status=document.getElementById('certificationStatus').value;
			var startDate=document.getElementById('newStartDate').value;
			var endDate=document.getElementById('newEndDate').value;
			startDate = startDate.replace(/-/g, '/');
			endDate = endDate.replace(/-/g, '/');
				if(status == 'Certified'){
					if(new Date(startDate).getTime()  > new Date(endDate).getTime()){
							return false;
						}
					}
			return true;
		});

		$.validator.addMethod("greaterThanToday", function(value, element) {
			//var today = '${currDate}';
			if(Date.parse(value) >= Date.parse('${dateForJscript}')){
				return true;
			}
			return false;
		});

function validateUploadDocument(uploadElementId) {
    var fileName = new String(document.getElementById(uploadElementId).value.toLowerCase());
    var fileExt = fileName.substring(fileName.lastIndexOf(".")+1,fileName.length);
    var supportedFileTypes = ["txt", "css", "xml", "csv", "png", "jpg", "jpeg", "bmp", "gif", "doc", "docx", "dot", "dotx", "xls", "xlsx", "ppt", "pptx", "pdf", "zip", "tar", "gz", "js"];

    var position = -1;

    for(var loopVar=0;loopVar<  supportedFileTypes.length ;loopVar++){
    	if(supportedFileTypes[loopVar] == fileExt ){
    		position=loopVar;
    		break;
    	}
    }
    if(position === -1) {
    	//alert('File type '+ fileExt +' not allowed.');
   	 	document.getElementById(uploadElementId).value=null;
    }
 }

$(function(){
	 $('#fileInput, #fileInputEODeclPage, #fileInputContract').change(function(e){

			var rv = -1; // Return value assumes failure.
			 if (navigator.appName == 'Microsoft Internet Explorer')
			 {
			    var ua = navigator.userAgent;
			    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			    if (re.exec(ua) != null)
			       rv = parseFloat( RegExp.$1 );
			 }
			 if(!(rv <= 9.0 && navigator.appName == 'Microsoft Internet Explorer')){
				 var f=this.files[0];
			 	if((f.size > 5242880)||(f.fileSize > 5242880)){
				$("#"+$(e.target).attr('id')+"_Size").val(0);
			}
			else{
				$("#"+$(e.target).attr('id')+"_Size").val(1);
			}
			}
			else{
				$("#"+$(e.target).attr('id')+"_Size").val(1);
			}

		});
	});

function extractGetParam(name){
	   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
	      return decodeURIComponent(name[1]);
	}

function checkUploadStatus() {
	var uploadStatusEODeclPage = null;
	var uploadStatusContract = null;
	var uploadStatusSupport = null;
	var statusMessage = "Could not upload";
	var failureStatus = false;

	uploadStatusEODeclPage = extractGetParam("documentIdEODeclPage");

	uploadStatusContract = extractGetParam("documentIdContract");

	uploadStatusSupport = extractGetParam("documentIdSupporting");

	if(uploadStatusEODeclPage === "0") {
		failureStatus = true;

		statusMessage = statusMessage + " E&O declaration page";
	}


	if(uploadStatusContract === "0") {
		if(failureStatus) {
			statusMessage = statusMessage + ", Contract";
		}
		else {
			statusMessage = statusMessage + " Contract";
		}

		failureStatus = true;
	}


	if(uploadStatusSupport === "0") {
		if(failureStatus) {
			statusMessage = statusMessage + ", Support";
		}
		else {
			statusMessage = statusMessage + " Support";
		}

		failureStatus = true;
	}

	if(failureStatus) {
		statusMessage = statusMessage + " document.";
		//alert(statusMessage);
	}

	statusMessage = null;
	failureStatus = false;

}


</script>
<script >

 function showdetail(documentId) {

	 var rv = -1; // Return value assumes failure.
	 if (navigator.appName == 'Microsoft Internet Explorer')
	 {
	    var ua = navigator.userAgent;
	    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
	    if (re.exec(ua) != null)
	       rv = parseFloat( RegExp.$1 );
	 }
	 if(rv <= 8.0 && navigator.appName == 'Microsoft Internet Explorer'){
		 $('<div class="modal popup-address" id="addressIFrame"><div class="modal-header padding0" style="border-bottom:0; "><div class="graydrkaction"><h4 class="margin0 pull-left"><spring:message  code='label.upgradeBrowser' javaScriptEscape='true'/></h4><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 0px;"><spring:message  code='label.upgradeBrowserMessage' javaScriptEscape='true'/></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
		 } else {
			 var  contextPath =  "<%=request.getContextPath()%>";
			 var documentUrl = contextPath + "/admin/broker/viewAttachment?encDocumentId="+documentId;
			 window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
		}
	 }


$(function() {

	    var
	    jqDdl = $('#certificationStatus'),
	    onChange = function(event) {
	        if ($(this).val() === 'Certified') {
	            $('#startDateId').show();
	            $('#startDateId').focus().select();
	            $('#endDateId').show();
	            $('#endDateId').focus().select();
	        } else {
	            $('#startDateId').hide();
		    	$('#endDateId').hide();
	        }
	    };
	    onChange.apply(jqDdl.get(0)); // To show/hide the Other textbox initially
	    jqDdl.change(onChange);
	});
</script>


<form name="dialogForm" id="dialogForm" action="<c:url value="/admin/broker/dashboard" />" novalidate="novalidate">
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left"><spring:message code="label.ViewAgentAccount"/></h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>

  <div class="modal-body">
    <div class="control-group">
				<div class="controls">
					<spring:message code="label.bkr.AgentViewPopup"/> ${broker.user.firstName}.<br/>
					<spring:message code="label.bkr.AgentSwitchPopup"/><br/>
					<spring:message code="label.bkr.ProceedtoAgentView"/>
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
  <input class="pull-left"  type="checkbox" id="checkAgentView" name="checkAgentView"  >
    <div class="pull-left">&nbsp; <spring:message code="label.employee.dontshow"/></div>
    <button class="btn btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.brkCancel"/></button>
   <button class="btn btn-primary" type="submit"><spring:message code="label.bkr.agentView"/></button>
    <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="broker" />
	<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="<encryptor:enc value="${broker.id}"/>" />
	<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${broker.user.firstName}" />
    <!-- a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>
</form>
