package com.getinsured.hix.planmgmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.RatingArea;
import com.getinsured.hix.model.ZipCountyRatingArea;

public interface ZipCountyRatingAreaRepository extends JpaRepository<ZipCountyRatingArea, Integer> {
	@Query("delete from ZipCountyRatingArea")
	void removeAllData();
	
	@Query("SELECT ra FROM RatingArea ra, ZipCountyRatingArea zipcra WHERE zipcra.ratingArea = ra.id AND zipcra.zip=:strZip")	
	RatingArea getRatingAreaByZip(@Param("strZip") String strZip);
}
