package com.getinsured.hix.util;

import java.io.IOException;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.WSO2Exception;

@ControllerAdvice
@Order(1)
public class GHIXExceptionResolver extends SimpleMappingExceptionResolver {

	private static final Logger logger = Logger.getLogger(GHIXExceptionResolver.class);
	
	private static final String message = "Unexpected error occurred during the operation, please contact customer support team with the error id.";
	
	@Autowired 
	private UserService userService;
	@Autowired 
	private GIMonitorService giMonitorService;

	public GHIXExceptionResolver() {
		super();
		Properties exceptionMappings =  new Properties();
		exceptionMappings.put("org.springframework.security.access.AccessDecisionManager", "error/accessDenied");
		exceptionMappings.put("org.springframework.security.access.AccessDeniedException", "error/accessDenied");
		exceptionMappings.put("com.getinsured.hix.platform.util.exception.CaseOrderIdSaveException", "error/errorPageExternal");
		exceptionMappings.put("com.getinsured.hix.platform.util.exception.GIException", "admin/error");
		setExceptionMappings(exceptionMappings);
		setDefaultErrorView("admin/error");
	}

	@Override
	@ExceptionHandler(value = { Exception.class })
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

		ModelAndView modelAndView = new ModelAndView();

		request.setAttribute("ENVIRONMENT", GhixConstants.ENVIRONMENT);

		//Get the logged user.
		AccountUser user  = userService.getPrincipalUser();

		//Check for an Exception type
		GIMonitor giMonitorObject;
		if(ex instanceof GIRuntimeException) {
			GIRuntimeException giRuntimeException = (GIRuntimeException) ex;
			giMonitorObject = formGIMonitorObject( giRuntimeException.getErrorCode(), giRuntimeException.getEventTime(), giRuntimeException.getClass().getName(),
					ExceptionUtils.getStackTrace(giRuntimeException) + "\n\nCaused by: "+ giRuntimeException.getNestedStackTrace(), 
					user, giRuntimeException.getComponent(), giRuntimeException.getGiMonitorId());
		} 
		else if(ex instanceof WSO2Exception){
			modelAndView.setViewName("account/ssoerrorpage");
			return modelAndView;
		}else if(ex instanceof HttpRequestMethodNotSupportedException){
			modelAndView.setViewName("admin/errorPage");
			try {
				if(response != null){
					response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
				}
			} catch (IOException e) {
				logger.warn("Http Method not allowed");
			} 
			return modelAndView;
		}
		else {
			giMonitorObject = formGIMonitorObject(GIRuntimeException.ERROR_CODE_UNKNOWN, new TSDate(), ex.getClass().getName(),
					ExceptionUtils.getStackTrace(ex), user, null, null);
		}

		ModelAndView model = super.doResolveException(request, response, handler, ex);
		if(model !=null) { modelAndView = model; }
		modelAndView.addObject("message", message);
		modelAndView.addObject("GIMONITOR", giMonitorObject);
		if(modelAndView.getViewName() == null || modelAndView.getViewName().isEmpty()){
			modelAndView.setViewName("admin/error");
		}
		
		return modelAndView;
	}

	private GIMonitor formGIMonitorObject(String errorCode, Date eventTime,
			String exceptionClassName, String exceptionStackTrace,
			AccountUser user, String component, Integer giMonitorId) {
		return giMonitorService.saveOrUpdateErrorLog(errorCode, eventTime, exceptionClassName, exceptionStackTrace, user, getRequestURL(), component, giMonitorId);
	}
	
	private String getRequestURL(){
		ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
	    return sra.getRequest().getRequestURI(); 
	}
}
