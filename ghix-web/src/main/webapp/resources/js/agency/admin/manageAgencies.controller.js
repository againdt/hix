(function() {
'use strict';
    angular.module('brokerAdminApp')
    .controller('ManageAgenciesCtrl', ManageAgenciesCtrl);

    ManageAgenciesCtrl.$inject = ['$http', '$state', 'dataStorageService'];

    function ManageAgenciesCtrl($http, $state, dataStorageService) {
        var vm = this;
        var agencyListUrl = '/hix/admin/agency/managelist';

        vm.init = init;
        vm.getPagination = getPagination;
        vm.viewInfo = viewInfo;

        /** Add / Remove Active Class from Menu bar **/
        $("a[title = 'Agencies']").parent().addClass('active');
        if($("a[title = 'Staff']").parent().hasClass('active')){
        	$("a[title = 'Staff']").parent().removeClass('active');
        }
        if($("a[title = 'Agents']").parent().hasClass('active')){
            $("a[title = 'Agents']").parent().removeClass('active');
        }
        function init(){
        	vm.paginationData = {
				totalRecords: 0,
				sortBy: undefined,
				sortOrder: undefined,
				pageNumber: 1
			}

        	vm.businessName = '';
        	vm.certificationStatus = '';
        	vm.filterData = {};

			getPagination();
		}

        function viewInfo(id, name) {
            var encryptedId = id;
            var businessLegalName = name;
            dataStorageService.setItem('encryptedAgencyId', encryptedId);
            dataStorageService.setItem('businessLegalName', businessLegalName);
            $state.go ("agency.agencyInformation", {agencyId: encryptedId});
        }

        function getPagination(){
			vm.loader = true;

			if(vm.updateRefineResult){
				vm.filterData.businessName = vm.businessName;
				vm.filterData.certificationStatus = vm.certificationStatus && vm.certificationStatus.toUpperCase();
				vm.paginationData.pageNumber = 1;
			}

			vm.filterData.pageNumber = vm.paginationData.pageNumber;


			var promise = $http.post(agencyListUrl, vm.filterData);

			promise.success(function(response){
				vm.loader = false;
				vm.updateRefineResult = false;

				vm.agencyList = response.agencysList;
				vm.certificationStatusList = response.certificationStatus;
                if (response.totalRecords > 0) {
					vm.isResultsFound = true;
					vm.paginationData.totalRecords = response.totalRecords;
				} else {
					vm.isResultsFound = false;
				}
			})


			promise.error(function(response){
				vm.loader = false;
				vm.updateRefineResult = false;
			})
		}

    }
})();
