<%@ page import="com.getinsured.hix.platform.security.service.ModuleUserService"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 

<%
String userActiveRoleName = (String) request.getSession().getAttribute("userActiveRoleName");
%>
<c:set var="userActiveRoleName" value="<%=userActiveRoleName%>" />

<script type="text/javascript">
function validate(desigId, moduleName)
{
  $("form").attr("action", "../../broker/approve/" + desigId + "/" + moduleName);
  $("form").submit();
};

function closeIFrame() {
	$("#imodal").remove();
	parent.location.reload();
	window.parent.closeIFrame();
}


</script>

<div class="gray">
	<h4>${individualUser.firstName} ${individualUser.lastName}</h4>
</div>

<form class="form-vertical" id="frmViewDesigDetail"
	name="frmViewDesigDetail" method="GET" target="_parent">
	<c:set var="encryptedId" ><encryptor:enc value="${individualUser.id}" isurl="true"/> </c:set>
	<c:set var="encryptedModuleName" ><encryptor:enc value="<%=ModuleUserService.INDIVIDUAL_MODULE%>" isurl="true"/> </c:set>
	<table class="table table-border-none" role="presentation">
		<tbody>
			<tr>
				<td class=""><spring:message code="label.brkcontactname"/>: <strong>${individualUser.firstName}
						${individualUser.lastName}</strong></td>
			</tr>
			<tr>
				<td class=""><spring:message code="label.phoneNumber"/>
					<c:if test="${individualUser.phoneNumber ne 0 }">
						<strong>${individualUser.phoneNumber}</strong>
					</c:if>
				 </td>				
			</tr>
			<tr>
				 <td class=""><spring:message code="label.emailAddress"/> <strong>${individualUser.email}</strong></td> 
			</tr>
		</tbody>
	</table>
	<c:if test="${(userActiveRoleName!='assisterenrollmententity') && (desigStatus != 'InActive')}">
		<div class="form-actions">
			<span class="pull-right">
				<a class="btn btn-primary btn-small" onClick="return validate('${encryptedId}', '${encryptedModuleName}')"><spring:message code="label.brkaccept"/></a>
				<a class="btn btn-small" href="#" onClick="window.parent.closeMe('${encryptedId}', '${desigStatus}', '${individualUser.firstName}')"><spring:message code="label.brkdecline"/></a>
			</span>
		</div>
	</c:if>                                                                    
</form>
