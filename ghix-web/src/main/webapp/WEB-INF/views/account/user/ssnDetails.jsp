<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<div id="rightpanel" class="span8 offset2 gutter5" ng-app="accountCreationApp" ng-controller="AccountCreationController as vm" ng-cloak>
	<h2>
		<spring:message code="indportal.ssnDetail.header" />
	</h2>	
	<form id = "frmSSNDetails" action="ssnDetails" method="post">
		<input type="hidden" id="ssn" name="ssn"/>
		<input type="hidden" id="dob1" name="dob1"/>
		<input name="userId" type="hidden" size="50" value="${userId}"  />
		<input name="landingPageUrl" type="hidden" size="50" value="${landingPageUrl}"  />
		<df:csrfToken/>
	</form>

	<form class="form-horizontal" name="vm.accountCreationForm" novalidate ng-submit="vm.submit()">
		<div id="ssn_error" class="error"></div>
		<div class="control-group">
			<label class="control-label" for="ssn1">
				<spring:message code="indportal.communicationPreferences.ssn" />
				<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="required" />
			</label>
			<c:url value="validateSSNDetails" var="theUrltocheckSSN"> </c:url>
			<div class="controls">
				 	<input 
						type="password" 
						id="ssn1" 
						name="ssn1" 
						class="input-mini ssn"
						ng-minlength="3" 
						ng-model="vm.formData.ssn1" 
						number-only 
						maxlength="3"
						required 
						ng-pattern="/(?!000|9\d{2})\d{3}/">
						
						
					<input 
						type="password" 
						id="ssn2" 
						name="ssn2" 
						class="input-mini ssn"
						number-only 
						maxlength="2"
						required 
						ng-minlength="2" 
						ng-model="vm.formData.ssn2" 
						ng-pattern="/(?!00)\d{2}/">
						
					<input 
						type="text" 
						id="ssn3" 
						name="ssn3" 
						class="input-mini ssn"
						ng-minlength="4" 
						ng-model="vm.formData.ssn3"
						number-only
						required
						maxlength="4"
						ng-blur="vm.ssnBlur = true">
					
					<span ng-if="vm.accountCreationForm.ssn1.$dirty && vm.accountCreationForm.ssn2.$dirty && vm.accountCreationForm.ssn3.$dirty && vm.ssnBlur">
						<span 
							class="error-message" 
							ng-if="vm.accountCreationForm.ssn1.$invalid || vm.accountCreationForm.ssn2.$invalid || vm.accountCreationForm.ssn3.$invalid">
							<spring:message code='indportal.communicationPreferences.ssnError1'/> 
							<a href="<c:url value="/preeligibility" />">
								<spring:message code='indportal.communicationPreferences.ssnError1.1'/>
							</a> 
							<spring:message code='indportal.communicationPreferences.ssnError1.2' />
						</span>
					</span>
						
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="confirmSsn1">
					<spring:message code="indportal.communicationPreferences.confirmSsn" />
					<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="required" />
				</label>
				<div class="controls">
					<input 
						type="password" 
						id="confirmSsn1" 
						name="confirmSsn1" 
						class="input-mini ssn"
						ng-minlength="3" 
						ng-model="vm.formData.confirmSsn1"
						number-only 
						maxlength="3"
						required
						ng-pattern="/(?!000|9\d{2})\d{3}/">
						
					<input 
						type="password" 
						id="confirmSsn2" 
						name="confirmSsn2" 
						class="input-mini ssn"
						ng-minlength="2" 
						ng-model="vm.formData.confirmSsn2"
						number-only 
						maxlength="2"
						required
						ng-pattern="/(?!00)\d{2}/">
						
					<input 
						type="text" 
						id="confirmSsn3" 
						name="confirmSsn3" 
						class="input-mini ssn"
						number-only 
						maxlength="4"
						ng-minlength="4" 
						ng-model="vm.formData.confirmSsn3"
						required
						ng-blur="vm.confirmSsnBlur = true">
						
					<span ng-if="vm.accountCreationForm.confirmSsn1.$dirty && vm.accountCreationForm.confirmSsn2.$dirty && vm.accountCreationForm.confirmSsn3.$dirty && vm.confirmSsnBlur">
						<span 
							class="error-message" 
							ng-if="vm.accountCreationForm.confirmSsn1.$invalid || vm.accountCreationForm.confirmSsn2.$invalid || vm.accountCreationForm.confirmSsn3.$invalid">
							<spring:message code='indportal.communicationPreferences.ssnError1' /> 
							<a href="<c:url value="/preeligibility" />">
								<spring:message code='indportal.communicationPreferences.ssnError1.1' />
							</a> 
							<spring:message code='indportal.communicationPreferences.ssnError1.2'/>
						</span>
						
						<span 
							class="error-message" 
							ng-if="vm.accountCreationForm.ssn1.$valid && 
								vm.accountCreationForm.ssn2.$valid && 
								vm.accountCreationForm.ssn3.$valid && 
								vm.accountCreationForm.confirmSsn1.$valid && 
								vm.accountCreationForm.confirmSsn2.$valid && 
								vm.accountCreationForm.confirmSsn3.$valid && 
								(vm.formData.confirmSsn1 !== vm.formData.ssn1 ||
								vm.formData.confirmSsn2 !== vm.formData.ssn2 ||
								vm.formData.confirmSsn3 !== vm.formData.ssn3)">
							<spring:message code='indportal.communicationPreferences.ssnError2'/>
						</span>
					</span>	
						
				</div>
			</div>
			<div class="control-group">
			<label for="dob" class="control-label">
				<spring:message code="indportal.communicationPreferences.dob" />
				<img 
					src='<c:url value="/resources/images/requiredAsterix.png" />' 
					width="10" 
					height="10" 
					alt="required" />
			</label>
	
			<div class="controls">
				<input 
					type="text" 
					name="dob" 
					id="dob" 
					class="input-medium" 
					date-validation="dob" 
					ng-model="vm.formData.dob" 
					ui-mask="99/99/9999" 
					model-view-value="true" 
					placeholder="MM/DD/YYYY" 
					required>
	
				<span class="error-message" ng-show="vm.accountCreationForm.dob.$error.dob && !vm.accountCreationForm.dob.$pristine">
					<spring:message code="indportal.communicationPreferences.dobError1" />
				</span>
				<span class="error-message" ng-show="vm.accountCreationForm.dob.$error.required && !vm.accountCreationForm.dob.$pristine">
					<spring:message code="indportal.communicationPreferences.dobError2" />
				</span>
			</div>
		</div>
		<div class="form-actions">
			<input 
				class="btn btn-primary" 
				type="submit" 
				value="<spring:message code="indportal.communicationPreferences.continue" />" 
				ng-disabled="vm.accountCreationForm.$invalid || 
					vm.formData.confirmSsn1 !== vm.formData.ssn1 || 
					vm.formData.confirmSsn2 !== vm.formData.ssn2 ||
					vm.formData.confirmSsn3 !== vm.formData.ssn3">
			<button class="btn btn-primary" type="button" onClick="location.href = '/hix/account/user/logout';">
				<spring:message code="indportal.communicationPreferences.cancel" />
			</button>
		</div>
		</div>
	</form>
</div>

<script src="<c:url value="/resources/angular/mask.js" />"></script> 
<script src="<c:url value="/resources/js/moment.min.js" />"></script> 
<script src="<c:url value="/resources/js/spring-security-csrf-token-interceptor.js"/>"></script>

<script>
   var theUrltocheckSSN = '/hix/validateSSNDetails';
   var accountCreationApp = angular.module('accountCreationApp', ['ui.mask','spring-security-csrf-token-interceptor']);

	accountCreationApp.controller('AccountCreationController', AccountCreationController);

	function AccountCreationController() {
		var vm = this;
		vm.formData = {};
		
		vm.submit = submit;
		
		function submit() {
			$.ajax({
		                url : theUrltocheckSSN,
		                type : "GET",
		                data : {
		                	ssn : vm.formData.ssn1 + vm.formData.ssn2 + vm.formData.ssn3,
		                	birthDate : vm.formData.dob
		                },
		                success: function(response)
		    			{
		    				if (response!="")
		    				{
		    					$("#ssn_error").html("<label class='error' style='text-transform:none;'><span> <em class='excl'>!</em>"+response+"</span></label><br>");
		    					$("#ssn_error").attr('class','error help-inline');
		    					$("#submitbtn").attr("disabled", false);
		    					return false;
		    				}
		    				else{
								var ssnStr = vm.formData.ssn1 + vm.formData.ssn2 + vm.formData.ssn3;
								var newStr = ssnStr.replace(/-/g, "");
								$("#ssn").val(newStr);
								$("#dob1").val(vm.formData.dob);
		    					$("#frmSSNDetails").submit();  
		    				}
		    			},
		                error: function(e){
		                alert("Failed to submit SSN");
		        		},
		        });
							
		}
		
	}
	
	
	accountCreationApp.directive('numberOnly', numberOnly)
	
	function numberOnly(){
		var directive = {
			restrict: 'AE',
			require: 'ngModel',
			link: function (scope, element, attrs, ngModel) {
				function fromUser(userInput) {
	                if (userInput) {
	                    var transformedInput = userInput.replace(/[^0-9]/g, '');

	                    if (transformedInput !== userInput) {
	                        ngModel.$setViewValue(transformedInput);
	                        ngModel.$render();
	                    }
	                    return transformedInput;
	                }
	                return undefined;
	            }            
	            ngModel.$parsers.push(fromUser);
			}
		};

		return directive;
	}
	
	accountCreationApp.directive("dateValidation", dateValidation);
	
	function dateValidation() {
		return {
			restrict : 'AE',
			require : 'ngModel',
			link : function(scope, element, attrs, ngModel) {		
				
				var dateType = attrs.dateValidation;
				
				function isDateValid(value){
					//remove placeholder
					var date = value.replace(/[A-Z]/g, '');

					//remove extra-last-character
					if(date.length === 11){
						date = date.substring(0, date.length - 1);
					}
					
					
					if(date.length === 10){
						if(!moment(date,'MM/DD/YYYY').isValid()){
							return false;
						}
						
						if(dateType === 'dob'){
							if(ageCheck('between',date, 0, 104)){
								return true;
							}else{
								return false;
							};
						}else if(dateType === 'future'){
							if(ageCheck('greater',date, 0)){
								return true;
							}else{
								return false;
							};
						}else if(dateType === 'under21'){
							if(ageCheck('between',date, 21, 104)){
								return true;
							}else{
								return false;
							};
						}else if(dateType === 'qe'){
							var inputDate = date.split('/');
							var start = new Date(inputDate[2], inputDate[0]-1, inputDate[1]);

							var today = new Date();
							var end = new Date(today.getFullYear(), today.getMonth(), today.getDate());
							
							var diff = (start.getTime() - end.getTime()) / (24*60*60*1000);
							if(diff <= 0 && diff >= -60){
								return true;
							}else{
								return false;
							};
						}
					}else{
						return true;
					}
					
				}

				function ageCheck(comparator, birth, age, maxAge){
					var birthDate = new Date(birth);
					var today = new Date();
					
					 var years = (today.getFullYear() - birthDate.getFullYear());

				    if (today.getMonth() < birthDate.getMonth() || today.getMonth() == birthDate.getMonth() && today.getDate() < birthDate.getDate()) {
				        years--;
				    }
				    
				    if(comparator === 'older'){
				    	if(years >= parseInt(age)){
					    	return true;
					    }else{
					    	return false;
					    }
					}else if(comparator === 'between'){
						if(years >= parseInt(age) && years <= parseInt(maxAge)){
					    	return true;
					    }else{
					    	return false;
					    }
					}else{
						if(years < parseInt(age)){
					    	return true;
					    }else{
					    	return false;
					    }
					}
					
				};

				ngModel.$parsers.unshift(function(value){
					var dateValidationResult = isDateValid(value);
					
					ngModel.$setValidity(dateType, dateValidationResult);
					
					return value;	
				});
				
				 ngModel.$formatters.unshift(function(value) {
					 if(value === undefined || value === null){
						 return;
					 }
					 var dateValidationResult = isDateValid(value);
						
					 ngModel.$setValidity(dateType, dateValidationResult);
					 
					 return value;
	             });
				
				
			}
		};
	}
	

</script> 