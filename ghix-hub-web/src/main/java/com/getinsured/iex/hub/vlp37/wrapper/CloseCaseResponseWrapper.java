package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc.CloseCaseResponseSetType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc.CloseCaseResponseType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc.ResponseMetadataType;






public class CloseCaseResponseWrapper implements JSONAware{

	private CloseCaseResponseMetadataWrapper responseMetadata;
	private String webServSftwrVer;
	private ResponseMetadataType responseMetadataType;
	private CloseCaseResponseSetWrapper responseSetWrapper;
	
	public CloseCaseResponseWrapper(CloseCaseResponseType closeCaseResponseType){
		
		if (closeCaseResponseType == null){
    		return;
    	}
		
		CloseCaseResponseSetType responseSet = closeCaseResponseType.getCloseCaseResponseSet();
		this.responseSetWrapper = new CloseCaseResponseSetWrapper(responseSet);
		
		this.responseMetadataType = closeCaseResponseType.getResponseMetadata();
		
		if(responseMetadataType != null){
			this.responseMetadata = new CloseCaseResponseMetadataWrapper(this.responseMetadataType);
		}
	}
	
	public CloseCaseResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(CloseCaseResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}
	
	public String getWebServSftwrVer() {
		return webServSftwrVer;
	}

	public void setWebServSftwrVer(String webServSftwrVer) {
		this.webServSftwrVer = webServSftwrVer;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseMetadata",this.responseMetadata);
		obj.put("ResponseSet", this.responseSetWrapper);
		return obj.toJSONString();
	}
}
