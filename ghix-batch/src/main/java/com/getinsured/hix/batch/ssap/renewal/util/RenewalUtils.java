package com.getinsured.hix.batch.ssap.renewal.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Class is used to provide utilities to Renewal Jobs.
 */
public class RenewalUtils {

	public static final Integer INNER_QUERY_CHUNK_SIZE = 900;
	public static final String EMSG_RUNNING_BATCH = "Another instance(s) of the same Job is running.";

	/**
	 * Method is used to generate chunk list from list and chunk size. 
	 */
	public static <T> List<List<T>> chunkList(List<T> list, int chunkSize) {

		if (chunkSize <= 0) {
			throw new IllegalArgumentException("Invalid chunk size: " + chunkSize);
		}

		List<List<T>> chunkList = new ArrayList<>(list.size() / chunkSize);

		for (int i = 0; i < list.size(); i += chunkSize) {
			chunkList.add(list.subList(i, i + chunkSize >= list.size() ? list.size() : i + chunkSize));
		}
		return chunkList;
	}
}
