export default {
  normalizePhone: (value, previousValue) => {
    if (!value) {
      return value;
    }
    const onlyNums = value.replace(/[^\d]/g, '');
    if (!previousValue || value.length > previousValue.length) {
      // typing forward
      if (onlyNums.length === 3) {
        return onlyNums + '-';
      }
      if (onlyNums.length === 6) {
        return onlyNums.slice(0, 3) + '-' + onlyNums.slice(3) + '-';
      }
      if (onlyNums.length === 10) {
        //1234567878
        return (
          onlyNums.slice(0, 3) +
          '-' +
          onlyNums.slice(3, 6) +
          '-' +
          onlyNums.slice(6)
        );
      }
    }
    if (onlyNums.length <= 3) {
      return onlyNums;
    }
    if (onlyNums.length <= 6) {
      return onlyNums.slice(0, 3) + '-' + onlyNums.slice(3);
    }
    if (onlyNums.length <= 10) {
      return (
        onlyNums.slice(0, 3) +
        '-' +
        onlyNums.slice(3, 6) +
        '-' +
        onlyNums.slice(6)
      );
    }
    return (
      onlyNums.slice(0, 3) +
      '-' +
      onlyNums.slice(3, 6) +
      '-' +
      onlyNums.slice(6, 10)
    );
  },

  getAdjustedPhoneNumber: phone => {
    if (phone === null) {
      return '';
    }
    const phoneToString = String(phone);
    if (phoneToString.length === 10 && phoneToString.match(/^[0-9]/)) {
      return `(${phoneToString.slice(0, 3)}) ${phoneToString.slice(
        3,
        6
      )}-${phoneToString.slice(6)}`;
    }

    return phoneToString;
  },

  cleanedUpPhone: phone =>
    typeof phone === 'string' &&
    phone
      .split('')
      .filter(ch => ch.match(/^[0-9]*$/))
      .join(''),

  autoFillPhone: (phone, previousPhoneValue) => {
    let result;
    switch (true) {
      case phone.length < previousPhoneValue.length:
        switch (phone.length) {
          case 1:
            result = null;
            break;
          case 5:
            result = phone.slice(0, phone.length - 1);
            break;
          case 6:
            result = phone.slice(0, phone.length - 2);
            break;
          case 10:
            result = phone.slice(0, phone.length - 1);
            break;
          default:
            result = phone;
        }
        break;
      case phone.length > previousPhoneValue.length:
        switch (phone.length) {
          case 1:
            result = `(${phone}`;
            break;
          case 4:
            result = `${phone}) `;
            break;
          case 5:
            result = `${phone.slice(0, phone.length - 1)}) ${phone.slice(-1)}`;
            break;
          case 9:
            result = `${phone}-`;
            break;
          case 10:
            result = `${phone.slice(0, phone.length - 1)}-${phone.slice(-1)}`;
            break;
          default:
            result = phone;
        }
        break;
      default:
        result = phone;
        break;
    }
    return result;
  }
};
