package com.getinsured.iex.shop.mec.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.DatePeriodType;
import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.MECVerificationCodeType;
import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.SourceInformationType;

public class SourceInformationTypeWrapper implements JSONAware {
	private String mecVerificationCode;
	private List<DatePeriodTypeWrapper> mecEligibilityPeriodWrapper;
 
    
	public SourceInformationTypeWrapper(SourceInformationType sourceInformation) throws HubServiceException{
		if(sourceInformation != null){
		MECVerificationCodeType mecVerificationCodeType = sourceInformation.getMECVerificationCode();
		
		if(mecVerificationCodeType != null){
			mecVerificationCode = mecVerificationCodeType.value();
		}
		
		List<DatePeriodType> mecEligibilityPeriod = sourceInformation.getMECEligibilityPeriod();
		
		if(!mecEligibilityPeriod.isEmpty()){
			mecEligibilityPeriodWrapper = new ArrayList<DatePeriodTypeWrapper>();
			for(DatePeriodType datePeriodType:  mecEligibilityPeriod){
				mecEligibilityPeriodWrapper.add(new DatePeriodTypeWrapper(datePeriodType));
			}
		}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		obj.put("MECVerificationCode", this.mecVerificationCode);
			
		JSONArray mecEligPeriod = null;
		if(mecEligibilityPeriodWrapper != null && !mecEligibilityPeriodWrapper.isEmpty()){
			mecEligPeriod = new JSONArray();
			
			for(DatePeriodTypeWrapper datePeriodTypeWrapper : this.mecEligibilityPeriodWrapper){
				mecEligPeriod.add(datePeriodTypeWrapper);
			}
			obj.put("MECEligibilityPeriod", mecEligPeriod);
		}
		return obj.toJSONString();
	}
}
