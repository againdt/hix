<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ghix" uri="/WEB-INF/tld/dropdown-lookup.tld"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/qdp.css" />" />


	<div class="gutter10">
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
	       	<li><a href="#" onclick="javascript:history.back();">&lt;<spring:message  code="label.back"/></a></li>
	        <li><a href="<c:url value="/admin/planmgmt/manageqdpplans"/>"><spring:message code="label.plans"/></a></li>
	        <li><a href="#"><spring:message code='label.addNewQDP'/></a></li>
	        <li><spring:message  code='label.provideplandetails'/></li>
	    </ul><!--page-breadcrumb ends-->
		<h1><a name="skip"></a><spring:message code='label.addNewQDP'/></h1>
	</div> 
		<div class="row-fluid">
			<div class="span3" id="sidebar">
				<div class="header">
					<h4 class="margin0"><spring:message code='label.steps'/></h4>
				</div>
					<!--  beginning of side bar -->
					<ol class="nav nav-list">
						<li class="active"><a href="#"><spring:message code='label.linkDentalPlanDetails'/></a></li>
						<li class="link"><a href="#"><spring:message code='label.linkDentalPlanBenefits'/></a></li>
						<li class="link"><a href="#"><spring:message code='label.linkDentalPlanRates'/></a></li>
						<li class="link"><a href="#"><spring:message code='label.linkDentalProviderNetwork'/></a></li>
						<li class="link"><a href="#"><spring:message code='label.reviewandconfirm'/></a></li>
					</ol><!-- end of side bar -->
				
			</div><!-- end of span3 -->
			<div class="span9" id="rightpanel">
				
			<div class="header">
				<h4 class="margin0"><spring:message code='label.step1'/> : <spring:message code='label.linkDentalPlanDetails'/></h4>
			</div>
				<input type="hidden" id="id" name="id" value="${plan.id}"/>
				<form class="form-horizontal gutter10" id="frmqdpinfo" name="frmqdpinfo" action="<c:url value="/admin/planmgmt/submitqdp" />" method="POST">
					<df:csrfToken/>
					<div class="control-group">
						<label for="issuer_name" class="control-label"><spring:message code='label.issuer'/></label>
						<div class="controls">
							<div id="issuername" class="input">
								<div id="issuername_error" class="help-inline"></div>
							</div>
						</div>
					</div>
					<div class="control-group">
						<label for="plan_name" class="control-label"><spring:message code='label.planName'/></label>
						<div class="controls">
							<input id="name" name="name"  class="input-xlarge" type="text" value="${plan.name}" />
							<div id="name_error" class="help-inline"></div>
						</div>
					</div>
					<div class="control-group">
						<label for="plan_number" class="control-label"><spring:message code='label.planNumber'/></label>
						<div class="controls">
							<input id="issuerPlanNumber" name="issuerPlanNumber"   class="input-xlarge" type="text" value="${plan.issuerPlanNumber}">
							<div id="issuerPlanNumber_error" class="help-inline"></div>
						</div>
					</div>
					<div class="control-group">
						<label for="plan_market" class="control-label"><spring:message code='label.planMarket'/></label>
						<div class="controls">
							<select class="input-medium" name="market" id="market">
								<c:forEach var="qdpmarket" items="${qdpmarketList}">
									<option <c:if test="${plan.market==qdpmarket.key}"><spring:message code='select.option'/></c:if> value="${qdpmarket.key}">${qdpmarket.value}</option>
								</c:forEach>
							</select>
							<div id="planMarket_error" class="help-inline"></div>
						</div>
					</div>
					<div class="control-group" id="plan_level_Id"><c:if test="${displayPlanLevel == 'YES'}">
						<label for="plan_level" class="control-label"><spring:message code='label.planLevel'/></label>
						<div class="controls">
							<%-- <select class="input-medium" name="planDental.planLevel" id="planDentalplanLevel">
								<option value="">Choose</option>
								<c:forEach var="qdplevel" items="${qdplevelList}">
									<option <c:if test="${plan.planDental.planLevel==qdplevel.key}">selected</c:if> value="${qdplevel.key}">${qdplevel.value}</option>						    		
								</c:forEach>
								
							</select>
							<input type="hidden" id="planLevelReadOnly" name="planLevelReadOnly" value="${plan.planDental.planLevel}" />
							<div id="planDentalplanLevel_error" class="help-inline"></div> --%>
							<ghix:dropdown name="planDental.planLevel" id="planDentalplanLevel" cssclass="input-medium"  lookuptype="QDP_PLAN_LEVEL" value="${currentPlanLevel}" />
						</div></c:if>
					</div>
					
					<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.startDate}" var="stDate"/>
					<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.endDate}" var="enDate"/>
					<div class="control-group">
						<label for="plan_start_date" class="control-label"><spring:message code='label.planStartDate'/></label>
						<div class="controls">
							<input type="text" title="MM/dd/yyyy" class="datepick input-small" name="startDate" id="startDate" readonly="true" value="<c:out value="${stDate}"/>" />
							<div id="startDate_error" class="help-inline"></div>
						</div>
					</div>
					<div class="control-group">
						<label for="plan_end_date" class="control-label"><spring:message code='label.planEndDate'/></label>
						<div class="controls">
							<input type="text" title="MM/dd/yyyy" class="datepick input-small" name="endDate" id="endDate" readonly="true" value="<c:out value="${enDate}"/>"/>
							<div id="endDate_error" class="help-inline"></div>
						</div>
					</div>
					<input type="hidden" name="planDental.acturialValueCertificate" id="planDentalacturialValueCertificate" value="${planDentalacturialValueCertificate}" />
					
				</form>
				<spring:message code="label.message"/>
				<form class="form-horizontal gutter10 form-bottom" id="frmCerti" name="frmCerti" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
					<df:csrfToken/>
					<div class="control-group">
						<label for="firstName" class="required control-label" for="fileInput"><spring:message code="label.attach"/> <spring:message code="label.valueCertificate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="file" id="certificates" name="certificates"  class="input-file">
							&nbsp; <button class="btn"><spring:message code="label.upload"/></button>
							<input type="hidden" name="fileType" value="certificates"/>
							<div id="certificatesFileName" class="help-inline">${acturialValueCertificateOrig}</div>
							<div id="planDentalacturialValueCertificate_error" class=""></div>
						</div> 
					</div>
					
					<div class="form-actions">
						<button type="submit" title="<spring:message code="label.saveAndContinue"/>" name="submitBtn" id="submitBtn" class="btn btn-primary paddingTB6" onclick="isExistingPlan()" ><spring:message code='label.saveAndContinue'/></button>
					</div>
				</form>
				
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('.datepick').each(function() {
			var ctx = "${pageContext.request.contextPath}";
			var imgpath = ctx+'/resources/images/calendar.gif';
			$(this).datepicker({
				showOn : "button",
				buttonImage : imgpath,
				buttonImageOnly : true,
				minDate: 0
			});
		});
	});

	function  formSubmit()	{
		$("#frmqdpinfo").submit();
	}
	
	var oldPlanName='${plan.name}';
	var oldIssuerId ='${plan.issuer.id}';
	
	$(document).ready(function(){
		getIssuerNames();
		
		var accreditationUpload = { 
		        //target:        '#certiName',   			// target element(s) to be updated with server response 
		        beforeSubmit:  submitAccreditationRequest,  // pre-submit callback 
		        success:       submitAccreditationResponse  // post-submit callback 
		    };  
	    $('#frmCerti').ajaxForm(accreditationUpload);
	    if(('${plan.id}' >0) && '${plan.planDental.planLevel}' == 'Silver'  )
	        {
	    		$("#planDentalplanLevel").val('${plan.planDental.planLevel}'); 
	   			$("#planDentalplanLevel").attr("disabled", true);
	        }
	});
	
	
	function submitAccreditationResponse(responseText, statusText, xhr, $form)  {
		var val = responseText.split(",");
		if(val[0] != 'Error') {	
			$("#certificatesFileName").text(val[1]);
			$("#planDentalacturialValueCertificate").val(val[2]);
			$("#planDentalacturialValueCertificate_error").val("");
		}
		else {
			$("#planDentalacturialValueCertificate_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
		}
	}
	
	function submitAccreditationRequest(formData, jqForm, options) { 
		var fileName = $('#certificates').val();
		if (fileName.length > 0) {
		    return true;	
		} 
		return false;
	}
	
	function getIssuerNames() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/loader.gif';
		$("#issuername").html("<div class=\"issuerloader\"><img src=\"" + imgpath + "\" alt='Issuer Image'></div>");
		$.ajax({
			  type: 'GET',
			  url: "getissuers",
			  success: function(data) {
			 	$("#issuername").html("");
				$("#issuername").html(data);
				$("#issuerId").val('${plan.issuer.id}');
				
				var el = document.getElementById("issuerId");
				for(var i=0; i<el.options.length; i++) {
				  if ( el.options[i].value == '${plan.issuer.id}' ) {
				    el.selectedIndex = i;
				    break;
				  }
				}
	           }	  
		});
	}
	
	function isExistingPlan() {
		if($("#frmqdpinfo").valid()) 	{
			var ctx = "${pageContext.request.contextPath}";
			if($('#issuerId').val() == oldIssuerId && oldPlanName == $('#name').val()) {
				formSubmit();
			} else {
				$.ajax({
				  type: 'GET',
				  url: "isExistingPlan",
				  data:{"issuerId":$('#issuerId').val(),"name":$('#name').val(),"oldPlanName":oldPlanName},
				  success: function(data) {
					 	if(data=='true')
					 	{
					 		$("#name_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message code='label.message'/></span></label>");
							$("#name_error").attr('class','error help-inline');
					 	}
					 	else
					 	{
					 		formSubmit();
					 	}
						
		            }	  
				});
			}
		}
	}

	$.validator.addMethod("endDate", function(value, element) {
		var startdatevalue = $('#startDate').val();
		if (value == null || value.length <1)
			return true;
		return Date.parse(startdatevalue) < Date.parse(value);
	});
	
	$.validator.addMethod("numericRegex", function(value, element) {
	    return this.optional(element) || /^[0-9]+$/i.test(value);
	});
	
	$.validator.addMethod("planLevel", function(value, element) {
		return !(value == null || value.length <1);
	});
	
	$.validator.addMethod("maxlength", function (value, element, len) {
	   return value == "" || value.length <= len;
	});
	
	var validator = $("#frmqdpinfo").validate({ 
		ignore: "",
		rules : {issuername : { required : true},
			name : { required : true,
				maxlength : '200'},
			issuerPlanNumber : { required : true,
				maxlength : '30'},
			startDate : { required : true},
			endDate : { endDate : true},
			"planDental.planLevel" : {planLevel : true},
			"planDental.acturialValueCertificate" : { required : true}
		}, 
		messages : {
			issuername : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.issuerName'/></span>"},
			name : { 
				required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.planName'/></span>",
				maxlength : "<span> <em class='excl'>!</em> <spring:message  code='label.planName'/> exceeds Maximum length.</span>"},
			issuerPlanNumber : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.planNumber'/></span>",
			maxlength : "<span> <em class='excl'>!</em> <spring:message  code='label.planName'/> exceeds Maximum length.</span>"},
			startDate : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.planStartDate'/></span>"},
			endDate : { endDate : "<span> <em class='excl'>!</em><spring:message  code='label.validateEndDate'/></span>"},
			"planDental.planLevel" : { planLevel : "<span> <em class='excl'>!</em><spring:message  code='label.showSelect'/><spring:message  code='label.planLevel'/></span>"},
			"planDental.acturialValueCertificate" : { required : "<span> <em class='excl'>!</em>Upload <spring:message  code='label.valueCertificate'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			if(elementId == 'planDentalacturialValueCertificate') {
				$("#" + elementId + "_error").html("");
			}
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-block');
		} 
	});
	jQuery("#frmCerti").validate({});
	
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /[0-9]*\./gi;
		$(this).html(completeStep.replace(replaceExpr,'<i class="icon icon-ok"></i>'));
	})
	
</script>
<style>
.ui-datepicker-trigger{
	margin-left: 5px;
}
</style>
