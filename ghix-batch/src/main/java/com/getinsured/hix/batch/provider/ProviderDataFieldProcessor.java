package com.getinsured.hix.batch.provider;

public interface ProviderDataFieldProcessor{
	
	public void setIndex(int idx);
	public int getIndex(); 
	public void setValidationContext(ValidationContext validationContext);
	public ValidationContext getValidationContext();
	public Object process(Object objToBeValidated) throws ProviderValidationException, InvalidOperationException;
}
