package com.getinsured.ssap.model.financial;

import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Describes IFSV household member.
 *
 * @author Yevgen Golubenko
 * @since 6/11/19
 */
public class IfsvHouseholdMember {
  private int personId;
  private Name name;
  private SocialSecurityCard socialSecurityCard;
  @JsonIgnore
  private long magiIncome;

  @JsonProperty("taxFilerCategoryCode")
  private TaxFilerCategory taxFilerCategory = TaxFilerCategory.UNKNOWN;

  public int getPersonId() {
    return personId;
  }

  public void setPersonId(int personId) {
    this.personId = personId;
  }

  public Name getName() {
    return name;
  }

  public void setName(Name name) {
    this.name = name;
  }

  public SocialSecurityCard getSocialSecurityCard() {
    return socialSecurityCard;
  }

  public void setSocialSecurityCard(SocialSecurityCard socialSecurityCard) {
    this.socialSecurityCard = socialSecurityCard;
  }

  public TaxFilerCategory getTaxFilerCategory() {
    return taxFilerCategory;
  }

  public void setTaxFilerCategory(TaxFilerCategory taxFilerCategory) {
    this.taxFilerCategory = taxFilerCategory;
  }

  public long getMagiIncome() {
    return magiIncome;
  }

  public void setMagiIncome(long magiIncome) {
    this.magiIncome = magiIncome;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", IfsvHouseholdMember.class.getSimpleName() + "[", "]")
      .add("personId=" + personId)
      .add("name=" + name)
      .add("socialSecurityCard=" + socialSecurityCard)
      .add("magiIncome=" + magiIncome)
      .add("taxFilerCategory=" + taxFilerCategory)
      .toString();
  }
}
