
export const getCurrentHouseholdMember = (adjustedFlow, activePage, householdMembers) => {
    if(adjustedFlow.length === 0) {
        return {
            hhmIndex: null,
            currentHhm: {
                taxHouseholdComposition: {},
                flagBucket: {}
            }
        };
    }
    const currentHouseholdMemberIndex =  (idx => ~idx ? idx : null)(adjustedFlow[activePage].hhm);
    return {
        hhmIndex: currentHouseholdMemberIndex,
        currentHhm: householdMembers[currentHouseholdMemberIndex]
    };
};

