(function(){
	'use strict';
	
	angular.module('dashboardAnnouncementApp')
	.directive("datePicker", datePicker);

	function datePicker(){
		var directive = {
			restrict : 'AE',
			link : function(scope, element, attrs) {						
				element.find('.icon-calendar').click(function(){
					element.datepicker({
						autoclose: true,
						orientation: "bottom"
					});
					element.find('input').off('click focus');
				});
			}
		};
		
		return directive;
	}
})();