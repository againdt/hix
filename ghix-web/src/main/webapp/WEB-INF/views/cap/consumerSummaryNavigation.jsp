<%@page import="java.util.HashMap"%>
<%@page import="com.getinsured.hix.model.consumer.Household"%>
<%@ taglib prefix="surl" uri="/WEB-INF/tld/secure-url.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>

<input type="hidden" id="permissionMap" value="${permissionMap}" />

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<%!
	String currStateCode = null;
	Boolean viewBasicInfo, viewEnrollment, viewApplications, viewComments, viewHistory, viewMemberAccount,viewAppealHistory,viewTktHist,viewResetPassword,showHouseholdInfo;
%>
<%
	currStateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	pageContext.setAttribute("currStateCode", currStateCode,pageContext.SESSION_SCOPE);
%>

<c:set var="encHouseHoldId">
	<encryptor:enc value="${householdId}" isurl="true" />
</c:set>
<c:set var="calheersEnv" value='<%=GhixConstants.CALHEERS_ENV%>'></c:set>
<input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>
<input type="hidden" id="encHouseHoldId" value="${encHouseHoldId}" />


<input type="hidden" id="encHouseHoldId" value="${encHouseHoldId}" />
<input type="hidden" id="enrollmentId" value="" />
<input type="hidden" id="isEnrollmentEditAllowed" value="${isEnrollmentEditAllowed}" />

<input type="hidden" id="isEnrollmentReadOnlyAllowed" value="${isEnrollmentReadOnlyAllowed}" />
<input type="hidden" id="enrollmentDetail" value='${enrollmentDetail}' />
<input type="hidden" id="encodedEnrollmentID" value='${encodedEnrollmentID}' />

<script type="text/javascript">
var csrfToken = {
        getParam: '${df:csrfTokenParameter()}=<df:csrfToken plainToken="true"/>',
        paramName: '${df:csrfTokenParameter()}',
        paramValue: '<df:csrfToken plainToken="true"/>'
    };
var isAllowed='${isResendAllowed}';
</script>

<div class="header">
	<h4>
		<spring:message code="label.aboutConsumer" />
	</h4>
</div>

<ul class="nav nav-list">
		<c:if test="${permissionMap.memberViewBasicInfo == 'true' }">
			<li id="basicInfoMenu"><a href="/hix/crm/member/viewmember/${encHouseHoldId}"><spring:message code="label.basicInformation" /></a></li>
		</c:if>
		<c:if test="${permissionMap.memberViewApplications}">
 	 		<li id="applicationsMenu"><a href="/hix/crm/newapplications/${encHouseHoldId}">Applications</a></li>
 	 	</c:if>
		<c:if test="${permissionMap.memberViewEnrollments == 'true' }">
			<li id="enrollmentMenu"><a href="<c:url value="/crm/member/enrollment/${encHouseHoldId}" />">Enrollments</a></li>
		</c:if>
		<c:if test="${permissionMap.memberViewComments == 'true' }">
			<li id="commentsMenu"><a href="/hix/crm/member/comments/${encHouseHoldId}"><spring:message code="label.brkComments" /></a></li>
		</c:if>
		<c:if test="${permissionMap.memberViewHistory == 'true' }">
			<li id="historyMenu"><a href="<c:url value="/crm/consumer/viewHistory/" />${encHouseHoldId}">History</a> </li>
		</c:if>
		<c:if test="${permissionMap.memberViewAppealHistory == 'true' }">
			<li id="appealhistoryMenu"><a href="/hix/crm/member/appealhistory/${encHouseHoldId}"><spring:message code="label.brkAppealHistory" /></a></li>
		</c:if>
		<c:if test="${permissionMap.memberViewTicketHistory == 'true' }">
			<li id="tktHistoryMenu"><a href="/hix/crm/member/tickethistory/${encHouseHoldId}"><spring:message code="label.brkTicketHistory" /></a></li>
		</c:if>
		<c:if test="${permissionMap.memberViewResetPassword == 'true' && household!=null && household.user != null}">
			<li id="resetPasswordMenu"><a href="<c:url value="/crm/consumer/pwresetquestions/" />${encHouseHoldId}">Reset Password</a></li>
		</c:if>
</ul>
<c:if test="${permissionMap.memberViewViewMemAcct == 'true' || permissionMap.memberViewCreateTkt == 'true' || permissionMap.memberViewAdminSearch == 'true' || permissionMap.memberViewViewCase == 'true' || permissionMap.memberViewBackToConsumerHome == 'true'}">
	<div class="header margin30-t">
		<h4><i class="icon-cog icon-white"></i> Actions</h4>
	</div>
</c:if>

<ul class="nav nav-list">
	<c:if test="${permissionMap.memberViewBackToConsumerHome == 'true' }">
		<li id="retConsumer"><a href="https://${calheersEnv}/static/lw-web/account-home">Back to Consumer Home</a></li>
	</c:if>
	<c:if test="${permissionMap.memberViewAdminSearch == 'true' }">
		<li id="retAppHomeMenu"><a href="https://${calheersEnv}/apspahbx/ahbxadmin.portal?_nfpb=true&_st=&_nfls=false&_pageLabel=administrationHomePage#">Admin Search</a></li>
	</c:if>
	<c:if test="${permissionMap.memberViewViewCase == 'true' }">
		<li id="retIndSearchMenu"><a href="${addNewViewCaseUrl}">View Case</a></li>
	</c:if>
	<c:if test="${checkDilog == null}">
		<c:if test="${permissionMap.memberViewViewMemAcct == 'true' }">
			<li class="navmenu"><a href="javascript:void(0)" data-target="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Member Account</a></li>
		</c:if>
	</c:if>
	<c:if test="${checkDilog != null}">
		<!-- TODO : From where to pull the household.name in ahref ??? -->
		<jsp:useBean id="paramMap" class="java.util.HashMap" scope="request"/>
                 <c:set target="${paramMap}" property="switchToModuleName" value="individual"/>
                 <c:set var="idAsString">${householdId}</c:set>
                 <c:set target="${paramMap}" property="switchToModuleId" value="${idAsString}"/>
                 <c:set target="${paramMap}" property="switchToResourceName" value="${household.firstName} ${household.lastName}"/>    
                 <surl:secureUrl paramMap="${paramMap}" var="encryptedParamMap" />
                 <li class="navmenu"><a href="/hix/account/user/switchUserRole?ref=${encryptedParamMap}"      role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Member Account</a></li>
	</c:if>
	
	<c:if test="${permissionMap.memberViewCreateTkt == 'true' }">
	<li>
		<a href="<c:url value="/ticketmgmt/ticket/createpage?prefillUser=true&roleName=INDIVIDUAL&moduleId=${householdId}&requestorId=${household.user.id}&requestorName=${household.firstName} ${household.lastName}&moduleName=HOUSEHOLD"/>"
		id="addNewTicket" name="addNewTicket">
			Create Ticket
		</a>
	</li>
	</c:if>
	<c:if test="${household!=null && household.user == null && permissionMap.memberViewSendAccountActivationEmail == 'true'}">
		<li class="navmenu">
			<a href="#confirmActivationDlg" role="button" data-toggle="modal">
				<i class="icon-envelope-alt"></i>Send Account Activation Email 
			</a>
		</li>
	</c:if>
</ul>




				<form class="noborder" name="dialogForm" id="dialogForm" method="POST"  action="<c:url value="/crm/consumer/dashboard" />" novalidate="novalidate">
	<df:csrfToken/>
	<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
		<div class="markCompleteHeader">
	    	<div class="header">
	            <h4 class="margin0 pull-left">View Member Account</h4>
	            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
	        </div>
	    </div>
	  
	  <div class="modal-body clearfix gutter10-lr">
	    <div class="control-group">	
					<div class="controls">
	                Clicking "Member View" will take you to the Member's portal for ${household.firstName} ${household.lastName}.<br/>
	                Through this portal you will be able to take actions on behalf of the member.<br/>
	                Proceed to Member view?
					</div>
				</div>
	  </div>
	  <div class="modal-footer clearfix">
	        <input class="pull-left"  type="checkbox" id="checkConsumerView" name="checkConsumerView"  > 
	        <div class="pull-left">&nbsp; Don't show this message again.</div>
	        <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	        <button class="btn btn-primary" type="submit">Member View</button>
	        <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="<encryptor:enc value="individual"/>" /> 
	        <input type="hidden" name="switchToModuleId" id="switchToModuleId" value="<encryptor:enc value="${householdId}"/>" />
	        <input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${household.firstName} ${household.lastName}" />
	  </div>
	</div>
</form>

<c:set var="encHouseholdId"><encryptor:enc value='${householdId}'/></c:set>
<c:set var="encHouseholdEmail"><encryptor:enc value='${household.email}'/></c:set>
<c:set var="encHouseholdPhone"><encryptor:enc value='${household.phoneNumber}'/></c:set>
<jsp:include page="../crm/sendConsumerActivationLink.jsp">
	<jsp:param value="${encHouseholdId}" name="householdId"/>
	<jsp:param value="${encHouseholdEmail}"  name="householdEmail"/>
	<jsp:param value="${encHouseholdPhone}" name="householdPhone"/>
</jsp:include>
