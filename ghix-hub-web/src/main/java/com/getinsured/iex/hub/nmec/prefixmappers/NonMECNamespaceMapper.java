package com.getinsured.iex.hub.nmec.prefixmappers;

import java.util.HashMap;
import java.util.Map;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class NonMECNamespaceMapper extends NamespacePrefixMapper{
	
private static final Map<String, String> PREFIXES = new HashMap<String, String>();
	
	static{
		PREFIXES.put("http://vesim.dsh.cms.gov/extension/1.0","ext");
		PREFIXES.put("http://niem.gov/niem/niem-core/2.0","nc");
		
		PREFIXES.put("http://vesim.dsh.cms.gov/exchange/1.0","exch");
		PREFIXES.put("http://niem.gov/niem/appinfo/2.1","ns0");
		
		PREFIXES.put("http://niem.gov/niem/proxy/xsd/2.0","neim-xsd");
		PREFIXES.put("http://niem.gov/niem/structures/2.0","s");
		
		PREFIXES.put("http://niem.gov/niem/usps_states/2.0","usps");
		PREFIXES.put("http://niem.gov/niem/appinfo/2.0","i");
		
		PREFIXES.put("http://hix.cms.gov/0.1/hix-core","hix-core");
		PREFIXES.put("http://hix.cms.gov/0.1/hix-ee","hix-ee");
		
		PREFIXES.put("http://niem.gov/niem/fbi/2.0","fbi");
		PREFIXES.put("http://www.w3.org/2001/XMLSchema-instance","xsi");
		PREFIXES.put("http://vnem.ee.sim.dsh.cms.hhs.gov","vnem");
	}
	
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		
		String prefix = PREFIXES.get(namespaceUri);
		
		if(prefix != null){
			return prefix;
		}
		else{
			return suggestion;
		}
	}
}
