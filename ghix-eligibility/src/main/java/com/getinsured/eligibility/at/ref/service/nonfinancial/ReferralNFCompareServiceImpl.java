package com.getinsured.eligibility.at.ref.service.nonfinancial;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareApplicationDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.ref.dto.NFProcessDTO;
import com.getinsured.eligibility.at.ref.handler.DemographicCompareHandler;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.ref.service.ReferralAdminUpdateService;
import com.getinsured.eligibility.at.ref.service.ReferralTriggerAdminupdateService;
import com.getinsured.eligibility.at.ref.transform.CompareApplicationTransformer;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

@Component("referralNFCompareService")
@Scope("singleton")
public class ReferralNFCompareServiceImpl implements ReferralNFCompareService {

	private static final Logger LOGGER = Logger.getLogger(ReferralNFCompareServiceImpl.class);

	@Autowired
	@Qualifier("compareApplicationTransformer")
	private CompareApplicationTransformer compareApplicationTransformer;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("demographicCompareHandler")
	private DemographicCompareHandler demographicCompareHandler;

	@Autowired
	@Qualifier("ssapEnrolleeHandler")
	private SsapEnrolleeHandler ssapEnrolleeHandler;

	@Autowired
	@Qualifier("referralAdminUpdateService")
	private ReferralAdminUpdateService referralAdminUpdateService;

	@Autowired
	@Qualifier("referralTriggerAdminupdateService")
	private ReferralTriggerAdminupdateService referralTriggerAdminupdateService;
	
	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;

	@Override
	public boolean memberMatching(NFProcessDTO nfProcessRequestDTO) {

		boolean matchFound = false;
		ArrayList<Long> appIds = new ArrayList<Long>();
		LOGGER.info("Non Finanacial Member Matching for Current Referral with Enrolled Application starts between enrolled application - " + nfProcessRequestDTO.getNfApplicationId() + " and current application "
		        + nfProcessRequestDTO.getCurrentApplicationId());
		final SsapApplication enrolledApplication = loadSsapApplicants(nfProcessRequestDTO.getNfApplicationId());
		final SsapApplication currentApplication = loadSsapApplicants(nfProcessRequestDTO.getCurrentApplicationId());

		List<SsapApplicant> financialApplicants = currentApplication.getSsapApplicants();
		List<SsapApplicant> nonFinancialApplicants = enrolledApplication.getSsapApplicants();

		if (ReferralUtil.collectionSize(financialApplicants) != ReferralUtil.collectionSize(nonFinancialApplicants)) {
			return false;
		}
		
		for (SsapApplicant nonFinancialApplicant : nonFinancialApplicants) {
			matchFound = false;
			for (SsapApplicant financialApplicant : financialApplicants) {
				if (!appIds.contains(financialApplicant.getId()) && compareApplicants(nonFinancialApplicant, financialApplicant)) {
					appIds.add(financialApplicant.getId());
					matchFound = true;
					break;
				}
			}
			if (!matchFound) {
				return false;
			}
		}
		LOGGER.info("Non Finanacial Member Matching for Current Referral with Enrolled Application Ends");
		return true;
	}

	@Override
	public boolean renewalMemberMatching(NFProcessDTO nfProcessRequestDTO) {

		boolean matchFound = false;
		ArrayList<Long> appIds = new ArrayList<Long>();
		LOGGER.info("Non Finanacial Renewal Member Matching for Current Referral with Enrolled Application starts between enrolled application - " + nfProcessRequestDTO.getNfApplicationId() + " and current application "
		        + nfProcessRequestDTO.getCurrentApplicationId());
		final SsapApplication enrolledApplication = loadSsapApplicants(nfProcessRequestDTO.getNfApplicationId());
		final SsapApplication currentApplication = loadSsapApplicants(nfProcessRequestDTO.getCurrentApplicationId());

		List<SsapApplicant> financialApplicants = currentApplication.getSsapApplicants();
		List<SsapApplicant> nonFinancialApplicants = enrolledApplication.getSsapApplicants();

		if (ReferralUtil.collectionSize(financialApplicants) != ReferralUtil.collectionSize(nonFinancialApplicants)) {
			return false;
		}
		for (SsapApplicant nonFinancialApplicant : nonFinancialApplicants) {
			matchFound = false;
			for (SsapApplicant financialApplicant : financialApplicants) {
				if (!appIds.contains(financialApplicant.getId()) && compareRenewalApplicants(nonFinancialApplicant, financialApplicant)) {
					appIds.add(financialApplicant.getId());
					matchFound = true;
					break;
				}
			}
			if (!matchFound) {
				return false;
			}
		}
		LOGGER.info("Non Finanacial Renewal Member Matching for Current Referral with Enrolled Application Ends");
		return true;
	}

	@Override
	public CompareMainDTO executeCompare(SsapApplication currentApplication, SsapApplication nonFinancialApplication) {
		CompareMainDTO compareMainDTO = null;
		LOGGER.info("Non Finanacial Compare Current Referral with Enrolled Application starts between enrolled application - " + currentApplication.getId() + " and current application " + nonFinancialApplication.getId());
		compareMainDTO = transformCompareDto(nonFinancialApplication, currentApplication);
		getHHEnrolledMailingAddress(compareMainDTO.getEnrolledApplication(),nonFinancialApplication);
		ssapEnrolleeHandler.parseListofEnrollees(compareMainDTO);
		copyNonFinancialApplicantGiud(compareMainDTO);
		LOGGER.info("Non Finanacial Compare for Current Referral with Enrolled Application Ends");
		return compareMainDTO;
	}

	@Override
	public CompareMainDTO executeRenewalCompare(SsapApplication currentApplication, SsapApplication nonFinancialApplication) {
		CompareMainDTO compareMainDTO = null;
		LOGGER.info("Non Finanacial Renewal Compare Current Referral with Enrolled Application starts between enrolled application - " + currentApplication.getId() + " and current application " + nonFinancialApplication.getId());
		compareMainDTO = transformRenewalCompareDto(nonFinancialApplication, currentApplication);
		copyNonFinancialRenewalApplicantGiud(compareMainDTO);
		LOGGER.info("Non Finanacial Renewal Compare for Current Referral with Enrolled Application Ends");
		return compareMainDTO;
	}

	@Override
	public void demoUpdate(CompareMainDTO compareMainDTO) throws Exception {
		demographicCompareHandler.processNonFinancialCompare(compareMainDTO);
		referralAdminUpdateService.executeNonFinancialAdminUpdate(compareMainDTO);
		
		/* Modification of IND57 */
		Address updatedMailingAddress = checkMailingAddressUpdate(compareMainDTO);
		if (updatedMailingAddress == null){
			updatedMailingAddress = new Address();
		}

		/* Modification of IND57 */
		referralTriggerAdminupdateService.triggerAdminupdate(compareMainDTO.getEnrolledApplication().getId(), compareMainDTO.getCurrentApplication().getId(), true, updatedMailingAddress,null);
	}
	
	private Address checkMailingAddressUpdate(CompareMainDTO compareMainDTO) {

		for (CompareApplicantDTO applicantDTO : compareMainDTO.getEnrolledApplication().getApplicants()) {
			if (applicantDTO.isAdminUpdate()) {
				return checkMailingAddress(applicantDTO);
			}
		}
		return null;
	}
	
	private Address checkMailingAddress(CompareApplicantDTO applicantDTO) {
		if (applicantDTO.getCompareApplicationDTO().isHasAddressChanged() && applicantDTO.getPersonId() == 1L) {
			if (applicantDTO.isMailAddressChanged()) {
				return applicantDTO.getMailingAddress();
			}
		}
		return null;
		
	}

	private void copyNonFinancialApplicantGiud(CompareMainDTO compareMainDTO) {

		List<CompareApplicantDTO> financialApplicants = compareMainDTO.getCurrentApplication().getApplicants();
		List<CompareApplicantDTO> nonFinancialApplicants = compareMainDTO.getEnrolledApplication().getApplicants();
		ArrayList<Long> appIds = new ArrayList<Long>();
		for (CompareApplicantDTO nonFinancialApplicant : nonFinancialApplicants) {
			for (CompareApplicantDTO financialApplicant : financialApplicants) {
				if (!appIds.contains(financialApplicant.getId()) && compareAndUpdateApplicants(nonFinancialApplicant, financialApplicant)) {
					appIds.add(financialApplicant.getId());
					break;
				}
			}
		}
	}

	private void copyNonFinancialRenewalApplicantGiud(CompareMainDTO compareMainDTO) {

		List<CompareApplicantDTO> financialApplicants = compareMainDTO.getCurrentApplication().getApplicants();
		List<CompareApplicantDTO> nonFinancialApplicants = compareMainDTO.getEnrolledApplication().getApplicants();
		ArrayList<Long> appIds = new ArrayList<Long>();
		for (CompareApplicantDTO nonFinancialApplicant : nonFinancialApplicants) {
			for (CompareApplicantDTO financialApplicant : financialApplicants) {
				if (!appIds.contains(financialApplicant.getId()) && compareAndUpdateRenwalApplicants(nonFinancialApplicant, financialApplicant)) {
					appIds.add(financialApplicant.getId());
					break;
				}
			}
		}
	}

	private boolean compareAndUpdateApplicants(CompareApplicantDTO nonFinancialApplicant, CompareApplicantDTO financialApplicant) {
		String strFElgStatus = financialApplicant.getEligibilityStatus();
		String strNFElgStatus = nonFinancialApplicant.getEligibilityStatus();
		if(nonFinancialApplicant.getEligibilityStatus() == null) {
			strNFElgStatus = ReferralConstants.ELIGIBLE_NONE;
		}
	
		if(financialApplicant.getEligibilityStatus() == null) {
			strFElgStatus = ReferralConstants.ELIGIBLE_NONE;
		}
		
		if (StringUtils.equalsIgnoreCase(nonFinancialApplicant.getFirstName(),financialApplicant.getFirstName()) && StringUtils.equalsIgnoreCase(nonFinancialApplicant.getLastName(),financialApplicant.getLastName())
		        && ReferralConstants.NONE == ReferralUtil.compareDate(nonFinancialApplicant.getBirthDate(), financialApplicant.getBirthDate()) && StringUtils.equalsIgnoreCase(strNFElgStatus, strFElgStatus)) {
			financialApplicant.setApplicantGuid(nonFinancialApplicant.getApplicantGuid());
			return true;
		}
		return false;
	}

	private boolean compareAndUpdateRenwalApplicants(CompareApplicantDTO nonFinancialApplicant, CompareApplicantDTO financialApplicant) {
		if (StringUtils.equalsIgnoreCase(nonFinancialApplicant.getFirstName(),financialApplicant.getFirstName()) && StringUtils.equalsIgnoreCase(nonFinancialApplicant.getLastName(),financialApplicant.getLastName())
		        && ReferralConstants.NONE == ReferralUtil.compareDate(nonFinancialApplicant.getBirthDate(), financialApplicant.getBirthDate())) {
			financialApplicant.setApplicantGuid(nonFinancialApplicant.getApplicantGuid());
			return true;
		}
		return false;
	}

	private boolean compareApplicants(SsapApplicant nonFinancialApplicant, SsapApplicant financialApplicant) {
		if (!StringUtils.equalsIgnoreCase(nonFinancialApplicant.getFirstName(),financialApplicant.getFirstName())) {
			return false;
		} else if (!((StringUtils.equalsIgnoreCase(nonFinancialApplicant.getLastName(),financialApplicant.getLastName()))
				|| StringUtils.equalsIgnoreCase("FEMALE" ,financialApplicant.getGender()))) {
			return false;
		} else if (ReferralConstants.NONE != ReferralUtil.compareDate(nonFinancialApplicant.getBirthDate(), financialApplicant.getBirthDate())) {
			return false;
		}else if (ReferralConstants.ONE == nonFinancialApplicant.getPersonId() && nonFinancialApplicant.getPersonId() != financialApplicant.getPersonId()) {
			return false;
		}
		else {
			String strFElgStatus = financialApplicant.getEligibilityStatus();
			String strNFElgStatus = nonFinancialApplicant.getEligibilityStatus();
			if(nonFinancialApplicant.getEligibilityStatus() == null) {
				strNFElgStatus = ReferralConstants.ELIGIBLE_NONE;
			}
		
			if(financialApplicant.getEligibilityStatus() == null) {
				strFElgStatus = ReferralConstants.ELIGIBLE_NONE;
			}
			
			if(!StringUtils.equals(strNFElgStatus, strFElgStatus) ) {
				return false;
			}
		}
		return true;
	}

	private boolean compareRenewalApplicants(SsapApplicant nonFinancialApplicant, SsapApplicant financialApplicant) {
		if (!StringUtils.equalsIgnoreCase(nonFinancialApplicant.getFirstName(), financialApplicant.getFirstName())) {
			return false;
		} else if (!StringUtils.equalsIgnoreCase(nonFinancialApplicant.getLastName(), financialApplicant.getLastName())) {
			return false;
		} else if (ReferralConstants.NONE != ReferralUtil.compareDate(nonFinancialApplicant.getBirthDate(), financialApplicant.getBirthDate())) {
			return false;
		} else if (ReferralConstants.ONE == nonFinancialApplicant.getPersonId() && nonFinancialApplicant.getPersonId() != financialApplicant.getPersonId()) {
			return false;
		}
		return true;
	}

	private CompareMainDTO transformCompareDto(SsapApplication enrolledApplication, SsapApplication currentApplication) {
		CompareMainDTO compareMainDTO = new CompareMainDTO();
		compareMainDTO.setEnrolledApplication(compareApplicationTransformer.transformApplication(enrolledApplication, true));
		compareMainDTO.setCurrentApplication(compareApplicationTransformer.transformApplication(currentApplication, true));
		return compareMainDTO;
	}

	private CompareMainDTO transformRenewalCompareDto(SsapApplication enrolledApplication, SsapApplication currentApplication) {
		CompareMainDTO compareMainDTO = new CompareMainDTO();
		compareMainDTO.setEnrolledApplication(compareApplicationTransformer.transformApplication(enrolledApplication, false));
		compareMainDTO.setCurrentApplication(compareApplicationTransformer.transformApplication(currentApplication, false));
		return compareMainDTO;
	}

	private SsapApplication loadSsapApplicants(Long enApp) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(enApp);
	}
	
	private void getHHEnrolledMailingAddress(CompareApplicationDTO enrolledCompareApplicationDto, SsapApplication enrolledApplication) {
		Household enrolledHH = cmrHouseholdRepository.getOne(enrolledApplication.getCmrHouseoldId().intValue());
		enrolledCompareApplicationDto.setMailingAddress(getAddress(enrolledHH.getPrefContactLocation()));
	}
	
	private Address getAddress(Location location){
		Address address = new Address();

		if (location.getCity()!=null) {
			address.setCity(location.getCity());
		}
		if (location.getZip() != null) {
			address.setPostalCode(location.getZip());
		}
		if (location.getState()!= null) {
			address.setState(location.getState());
		}
		if (location.getAddress1() != null) {
			address.setStreetAddress1(location.getAddress1());
		}
		if (location.getAddress2() != null) {
			address.setStreetAddress2(location.getAddress2());
		}
		if (location.getCounty() != null) {
			address.setCounty(location.getCounty());
		}
		return address;
	}
}
