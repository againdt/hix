<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div class="header">
	<h4>
		Ticket Management
	</h4>
</div>
<ul class="nav nav-list">
	<li id="createTicket" class="navmenu"><a href="<c:url value="/ticketmgmt/ticket/createpage" />">Create Ticket</a></li>
	<li id="manageTickets" class="navmenu"><a href="<c:url value="/ticketmgmt/ticket/ticketlist?resetflag=true" />">Manage Tickets</a></li>
	<sec:accesscontrollist hasPermission="TKM_LIST_QUEUE" domainObject="user">
		<li id="manageWorkgroups" class="navmenu"><a href="<c:url value="/ticketmgmt/ticket/managequeue" />">Manage Workgroup</a></li>
	</sec:accesscontrollist>
</ul>
