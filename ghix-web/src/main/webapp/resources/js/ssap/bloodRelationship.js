
var noOfRelation = 0; 



function indivOnChange(){
	
	$('#appscrBloodRelIndivRelatedTolabel').html($('#appscrBloodRelIndivRelatedTo option:selected').text());
	$('#appscrBloodRelIndivLabel').html($('#appscrBloodRelIndiv option:selected').text());
	
}

function clear(){
	
	$("#relat").val(-1);
	$("#appscrBloodRelIndivRelatedTo").val(-1);
	$("#appscrBloodRelIndiv").val(-1);
	indivOnChange();
}


function  addButtonError(){
	
	$('#addButton').addClass('colorRed');
	var t = setTimeout("$('#addButton').removeClass('colorRed');", 1000);
	
	
}



function addRelationship(){	
	
	var indiv = $('#appscrBloodRelIndiv option:selected').val().split("_")[0];
	var individ = $('#appscrBloodRelIndiv option:selected').val().split("_")[1];
	var indivTagrget  = $('#appscrBloodRelIndivRelatedTo option:selected').val().split("_")[0];
	var indivTagrgetid  = $('#appscrBloodRelIndivRelatedTo option:selected').val().split("_")[1];
	var relation  = $('#relat option:selected').text();
	var relationCode  = $('#relat').val();
	/*var isTaxDependent = $('input[name=taxDependent]:radio:checked').val();*/
	
	
	if($("#relat").val() == -1){
		document.getElementById("relat").focus();
		addButtonError();
		return;
	}
	if($("#appscrBloodRelIndivRelatedTo").val() == -1){
		document.getElementById("appscrBloodRelIndivRelatedTo").focus();
		addButtonError();
		return;
	}
	if($("#appscrBloodRelIndiv").val() == -1){
		document.getElementById("appscrBloodRelIndiv").focus();
		addButtonError();
		return;
	}
	
	
	var str = 	'<tr>' +
				'<td class="Indiv doc">'+indiv+'</td>' +
				'<td class="Individ hide">'+individ+'</td>' +
				'<td class="rel doc">'+indivTagrget+'</td>'+
				'<td class="relid hide">'+indivTagrgetid+'</td>'+
				'<td class="relt hide">'+relationCode+'</td>'+
				'<td class="relt1Text doc">' +relation+'</td>'+
				'<td class="relat hide">No</td>'+
				'<td class="pct hide">No</td>'+
				/*'<td class="tax doc">'+isTaxDependent+'</td>'+*/
				'<td class="doc" onclick="removeTR($(this));"><i class="icon-remove"></i></td>'+
				'</tr>';
	
	$('#appscrBloodRelTableBody').append(str); 
	noOfRelation++;
	clear();
	check();
}

function removeTR(id){
	id.parent().remove();
	noOfRelation--;
	check();
}

//Keys
function check(){
	if(noOfRelation <= 0 ){
		$('#bloodRelationshipslabel').text(jQuery.i18n.prop('ssap.noRelationshipAdded'));
		$('#appscrBloodRelTableContainer').hide();
	}
	else{
		$('#bloodRelationshipslabel').text(jQuery.i18n.prop('ssap.bloodRelationship'));
		$('#appscrBloodRelTableContainer').show();
	}
}

function populateDataToRelations()
{
/* $('#appscrBloodRelIndiv').html('');
 
 $('#appscrBloodRelIndivRelatedTo').html('');
  
 
 $('#appscrBloodRelIndiv').append(new Option('Select','-1') );
  
 $('#appscrBloodRelIndivRelatedTo').append(new Option('Select','-1'));
 
 
 	var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;	
	for ( var cnt = 1; cnt <= noOfHouseHoldmember; cnt++) {		
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
		var pureNameOfUser=householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
		$('#appscrBloodRelIndiv').append(new Option(pureNameOfUser,pureNameOfUser+"_"+cnt) );	    
	    $('#appscrBloodRelIndivRelatedTo').append(new Option(pureNameOfUser,pureNameOfUser+"_"+cnt));
	    
	}
 
 populateChechBox();*/
	
	
	var parimaryContactRelationArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
	
	for(var i=0;i<parimaryContactRelationArr.length;i++){
		var individualPersonId = parimaryContactRelationArr[i].individualPersonId;
		var relatedPersonId = parimaryContactRelationArr[i].relatedPersonId;
		var relation = parimaryContactRelationArr[i].relation;
		
		var reversedRelation = '';
		
		if(parseInt(individualPersonId) < parseInt(relatedPersonId)){
			if(relation == '03' || relation == '15'){	//multiple relationship
				
				//get reversed relation
				for(var j=0; j<parimaryContactRelationArr.length; j++){			
					if(parimaryContactRelationArr[j].relatedPersonId == individualPersonId && parimaryContactRelationArr[j].individualPersonId == relatedPersonId){
						reversedRelation = parimaryContactRelationArr[j].relation;
					}
				}
				
				//get relation based on its reversed relation
				if(reversedRelation == '09'){
					$('#'+ individualPersonId + 'to' + relatedPersonId).val('03a');
				}else if(reversedRelation == '10'){
					$('#'+ individualPersonId + 'to' + relatedPersonId).val('03f');
				}else if(reversedRelation == '19'){
					$('#'+ individualPersonId + 'to' + relatedPersonId).val('03');
				}else if(reversedRelation == '26'){
					$('#'+ individualPersonId + 'to' + relatedPersonId).val('15');
				}else if(reversedRelation == '31'){
					$('#'+ individualPersonId + 'to' + relatedPersonId).val('15c');
				}else if(reversedRelation == '15'){
					$('#'+ individualPersonId + 'to' + relatedPersonId).val('03w');
				}else if(reversedRelation == '03'){
					$('#'+ individualPersonId + 'to' + relatedPersonId).val('15p');
				}
				
				
			
			}else{	//one-one relationship
				$('#'+ individualPersonId + 'to' + relatedPersonId).val(parimaryContactRelationArr[i].relation);
			}
			
			//show this and next section
			var nextRelationshipSelection = $('#relationshipSelection' + (parseInt(individualPersonId)+1));
			if(nextRelationshipSelection.find('select').length > 0){
				nextRelationshipSelection.show();
			}
			
		}
	}
	
	
	
	/*var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	
	for ( var i = 1; i <= noOfHouseHoldmember; i++) {
		var householdMemRelArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1].bloodRelationship;
		var showRelationshipSelection = false;
		
		for(var j=0;j<householdMemRelArr.length;j++){
			if(householdMemRelArr[j].individualPersonId < householdMemRelArr[j].relatedPersonId){
				
				var relation = '';
				householdMemMulRelArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[householdMemRelArr[j].relatedPersonId-1].bloodRelationship;
				for(var k=0; k<householdMemMulRelArr.length; k++){
					if(householdMemMulRelArr[k].relatedPersonId == i){
						relation = householdMemMulRelArr[k].relation;
					}
				}
				
				if(householdMemRelArr[j].relation == '03'){					
					if(relation == '09'){
						$('#'+ householdMemRelArr[j].individualPersonId + 'to' + householdMemRelArr[j].relatedPersonId).val('03a');
					}else if(relation == '10'){
						$('#'+ householdMemRelArr[j].individualPersonId + 'to' + householdMemRelArr[j].relatedPersonId).val('03f');
					}else if(relation == '19'){
						$('#'+ householdMemRelArr[j].individualPersonId + 'to' + householdMemRelArr[j].relatedPersonId).val('03');
					}

				}else if(householdMemRelArr[j].relation == '15'){
					if(relation == '26'){
						$('#'+ householdMemRelArr[j].individualPersonId + 'to' + householdMemRelArr[j].relatedPersonId).val('15');
					}else if(relation == '31'){
						$('#'+ householdMemRelArr[j].individualPersonId + 'to' + householdMemRelArr[j].relatedPersonId).val('15c');
					}
				}else{
					$('#'+ householdMemRelArr[j].individualPersonId + 'to' + householdMemRelArr[j].relatedPersonId).val(householdMemRelArr[j].relation);
				}
				
				
				showRelationshipSelection = true;
			}
		}
		
		//show next relationship section
		var nextSelection = $('#relationshipSelection' + (i+1));
		if(showRelationshipSelection && nextSelection.find('select').length > 0 ){
			nextSelection.show();
		}
			
		for ( var j = i + 1; j <= noOfHouseHoldmember; j++ ) {
			$('#'+ i + 'to' + j).val(householdMemberObj.bloodRelationship[i-1].relation);
		}
		
		for ( var j = 0; j < noOfHouseHoldmember; j++ ) {
			$('#'+ i + 'to' + j).val(householdMemberObj.bloodRelationship[j].relation);
		}
	}*/
	
	
	
	
	
	
	//hide and show relationship
	/*for ( var i = 1; i <= noOfHouseHoldmember - 1; i++) {	
		for ( var j = i + 1; j <= noOfHouseHoldmember; j++) {
			if($('#'+ i + 'to' + j).val() !== undefined){
				$('#relationshipSelection' + i).show();
			}
		}
	}*/
	
	

 
}

function populateRelationship() {
	/*$('#appscrBloodRelTableBody').html('');
	var objBloodRelation = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
	noOfRelation = objBloodRelation.length;
	for ( var cnt = 1; cnt <= objBloodRelation.length; cnt++) {
		var relationships = objBloodRelation[cnt-1];
		
		
		var indiv = getNameByPersonId(relationships.individualPersonId);
		var individ = relationships.individualPersonId;
		var indivTagrget  = getNameByPersonId(relationships.relatedPersonId);
		var indivTagrgetid  = relationships.relatedPersonId;
		var relation  = getBloodRelation(relationships.relation);
		var relationCode  = relationships.relation;
		var isTaxDependent = relationships.textDependency == true ? 'Yes' : 'No'; 
		
		
		if((individ != "" && individ != null) && (indivTagrgetid != "" && indivTagrgetid != null)){
		
		var str = 	'<tr>' +
					'<td class="Indiv doc">'+indiv+'</td>' +
					'<td class="Individ hide">'+individ+'</td>' +
					'<td class="rel doc">'+indivTagrget+'</td>'+
					'<td class="relid hide">'+indivTagrgetid+'</td>'+
					'<td class="relt hide">'+relationCode+'</td>'+
					'<td class="relt1Text doc">' +relation+'</td>'+
					'<td class="relat hide">No</td>'+
					'<td class="pct hide">No</td>'+
					'<td class="tax doc">'+isTaxDependent+'</td>'+
					'<td class="doc" onclick="removeTR($(this));"><i class="icon-remove"></i></td>'+
					'</tr>';
		
		$('#appscrBloodRelTableBody').append(str); 
		$('#appscrBloodRelTableContainer').show();
		
		}
	    
	}*/
	var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	
	//Update number in list
	$('.householdMemberCount').text(noOfHouseHoldmember);
	
	//update list
	$('#relationshipMembersList').html('');
	var relationshipMembersList = '';
	for ( var i = 1; i <= noOfHouseHoldmember; i++) {	
		var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var householdMemberObjFullName = householdMemberObj.name.firstName + " " + householdMemberObj.name.middleName + " " + householdMemberObj.name.lastName;
		
		relationshipMembersList += '<li> <span class="camelCaseName">' + householdMemberObjFullName + '</span></li>';
	}
	
	$('#relationshipMembersList').append(relationshipMembersList);
	
	var relationshipOptions = "";
	//update relationship
	if($("#exchangeName").val() != "NMHIX"){
		relationshipOptions = 
		'<option value="">'+jQuery.i18n.prop("ssap.page13.selectRelationship")+'</option>'+
		'<option value="01">'+jQuery.i18n.prop("ssap.page13.spouse")+'</option>'+
		'<option value="03">'+jQuery.i18n.prop("ssap.page13.parentOfChild")+'</option>'+  
		'<option value="03a">'+jQuery.i18n.prop("ssap.page13.parentOfAdoptedChild")+'</option>'+  
		'<option value="03w">'+jQuery.i18n.prop("ssap.page13.parentCaretakerOfWard")+'</option>'+  
		'<option value="09">'+jQuery.i18n.prop("ssap.page13.adoptedChild")+'</option>'+
		'<option value="15c">'+jQuery.i18n.prop("ssap.page13.wardOfCGuardian")+'</option>'+
		'<option value="15p">'+jQuery.i18n.prop("ssap.page13.wardOfParentCaretaker")+'</option>'+
		'<option value="16">'+jQuery.i18n.prop("ssap.page13.stepParent")+'</option>'+
		'<option value="17">'+jQuery.i18n.prop("ssap.page13.stepChild")+'</option>'+
		'<option value="19">'+jQuery.i18n.prop("ssap.page13.childSonDaughter")+'</option>'+
		'<option value="31">'+jQuery.i18n.prop("ssap.page13.courtAppointedGuardian")+'</option>';
	}else{
		relationshipOptions = 
		'<option value="">'+jQuery.i18n.prop("ssap.page13.selectRelationship")+'</option>'+
		'<option value="01">'+jQuery.i18n.prop("ssap.page13.spouse")+'</option>'+
		'<option value="03">'+jQuery.i18n.prop("ssap.page13.parentOfChild")+'</option>'+
		'<option value="03a">'+jQuery.i18n.prop("ssap.page13.parentOfAdoptedChild")+'</option>'+  
		'<option value="03f">'+jQuery.i18n.prop("ssap.page13.parentOfFosterChild")+'</option>'+
		'<option value="04">'+jQuery.i18n.prop("ssap.page13.grandParent")+'</option>'+
		'<option value="05">'+jQuery.i18n.prop("ssap.page13.grandChild")+'</option>'+
		'<option value="06">'+jQuery.i18n.prop("ssap.page13.uncleAunt")+'</option>'+
		'<option value="07">'+jQuery.i18n.prop("ssap.page13.nephewNiece")+'</option>'+
		'<option value="08">'+jQuery.i18n.prop("ssap.page13.firstCousin")+'</option>'+
		'<option value="09">'+jQuery.i18n.prop("ssap.page13.adoptedChild")+'</option>'+
		'<option value="10">'+jQuery.i18n.prop("ssap.page13.fosterChild")+'</option>'+
		'<option value="11">'+jQuery.i18n.prop("ssap.page13.sonDaughterInLaw")+'</option>'+
		'<option value="12">'+jQuery.i18n.prop("ssap.page13.brotherSisterInLaw")+'</option>'+
		'<option value="13">'+jQuery.i18n.prop("ssap.page13.motherFatherInLaw")+'</option>'+
		'<option value="14">'+jQuery.i18n.prop("ssap.page13.sibling")+'</option>'+
		'<option value="15">'+jQuery.i18n.prop("ssap.page13.wardOfGuardian")+'</option>'+
		'<option value="15c">'+jQuery.i18n.prop("ssap.page13.wardOfCGuardian")+'</option>'+
		'<option value="16">'+jQuery.i18n.prop("ssap.page13.stepParent")+'</option>'+
		'<option value="17">'+jQuery.i18n.prop("ssap.page13.stepChild")+'</option>'+
		'<option value="19">'+jQuery.i18n.prop("ssap.page13.childSonDaughter")+'</option>'+
		'<option value="25">'+jQuery.i18n.prop("ssap.page13.formerSpouse")+'</option>'+
		'<option value="26">'+jQuery.i18n.prop("ssap.page13.guardian")+'</option>'+
		'<option value="31">'+jQuery.i18n.prop("ssap.page13.courtAppointedGuardian")+'</option>'+
		'<option value="53">'+jQuery.i18n.prop("ssap.page13.domesticPartner")+'</option>'+
		'<option value="G8">'+jQuery.i18n.prop("ssap.page13.unspecifiedRelationship")+'</option>'+
		'<option value="G9">'+jQuery.i18n.prop("ssap.page13.unspecifiedRelative")+'</option>'+
		'<option value="03-53">'+jQuery.i18n.prop("ssap.page13.parentDomesticPartner")+'</option>'+
		'<option value="53-19">'+jQuery.i18n.prop("ssap.page13.childOfDomesticPartner")+'</option>';
	}
	
	
	$('#relationshipSelection').html('');
	var relationshipSelection = '';
	for ( var i = 1; i <= noOfHouseHoldmember; i++) {	
		var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var householdMemberObjFullName = householdMemberObj.name.firstName + ' ' + householdMemberObj.name.middleName + ' ' + householdMemberObj.name.lastName;
		
		if(i == 1){
			relationshipSelection += '<li id=relationshipSelection' + i + '>How is <strong><span class="camelCaseName">' + householdMemberObjFullName + '</span></strong> related to the other household members?';
		}else{
			relationshipSelection += '<li id=relationshipSelection' + i + ' class="hide">How is <strong><span class="camelCaseName">' + householdMemberObjFullName + '</span></strong> related to the other household members?';
		}
		
		relationshipSelection += '<div class="gutter10">';
		
		for ( var j = i + 1; j <= noOfHouseHoldmember; j++) {	
			var householdMemberRelatetionshipObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[j-1];
			var householdMemberRelatetionshipObjFullName = householdMemberRelatetionshipObj.name.firstName + " " + householdMemberRelatetionshipObj.name.middleName + " " + householdMemberRelatetionshipObj.name.lastName;
			
			relationshipSelection += '<div class="control-group">';
			relationshipSelection += '<label class="control-label capitalize-none"><span class="camelCaseName">'+ householdMemberObjFullName +'</span> is <span class="camelCaseName">' + householdMemberRelatetionshipObjFullName + '</span>\'s </label>';
			relationshipSelection += '<div class="controls">';
			var iPersonId = householdMemberObj.personId;
			var jPersonId = householdMemberRelatetionshipObj.personId;
			if(i != 1 && $("#exchangeName").val() != "NMHIX"){				
				relationshipSelection += '<select onchange="showNextSelection($(this));" id="' + iPersonId + 'to' + jPersonId + '" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.relationshipRequired")+'">' + relationshipOptions + '<option value="14">'+ jQuery.i18n.prop('ssap.page13.sibling') +'</option></select>';				
			}else{
				//relationshipSelection += '<select onchange="showNextSelection($(this));" id="' + i + 'to' + j + '" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.relationshipRequired")+'">' + relationshipOptions + '</select>';				
				relationshipSelection += '<select onchange="showNextSelection($(this));" id="' + iPersonId + 'to' + jPersonId + '" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.relationshipRequired")+'">' + relationshipOptions + '</select>';
			}
			relationshipSelection += '</div></div>';
		}
		//checkMultipleMapping($(this),\''+i+'\',\''+j+'\',\''+householdMemberObjFullName+'\',\''+householdMemberRelatetionshipObjFullName+'\')
		//relationshipSelection += '<div class="multipleParent"></div><div class="multipleWard"></div>';
		
		relationshipSelection += '</div></li>';
	}
	
	$('#relationshipSelection').append(relationshipSelection);
	
}



function saveBloodRelationData(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){ 
		return;
	}
	
	var parimaryContactRelationArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
	
	//before save, clear all relationship
	parimaryContactRelationArr = [];
	
	
	//save relationship data
	$('#relationshipSelection select').each(function(){		
		//get head number as individual person ID 
		var individualPersonId = $(this).attr("id").match(/^\d+/)[0];
		//get tail munber as related person ID
		var relatedPersonId = $(this).attr("id").match(/\d+$/)[0];
		//get relationship number
		var relationshipNum = $(this).val();
		
		//update relationship
		
		//handle 1 to many relationship, only for nm
		var relationshipNumJSON;
		if(relationshipNum == '03a' || relationshipNum == '03f' || relationshipNum == '03w'){
			relationshipNumJSON = '03';
		}else if(relationshipNum == '15c' || relationshipNum == '15p'){
			relationshipNumJSON = '15';
		}else{
			relationshipNumJSON = relationshipNum;
		}
		var relationshipObj = {
				"individualPersonId" : individualPersonId,
				"relatedPersonId" : relatedPersonId,
				"relation" : relationshipNumJSON
		};	
		parimaryContactRelationArr.push(relationshipObj);
		
		//update related relationshiop		
		var relatedRelationshipObj = {
				"individualPersonId" : relatedPersonId,
				"relatedPersonId" : individualPersonId,
				"relation" : getReveseredRel(relationshipNum)
		};
		parimaryContactRelationArr.push(relatedRelationshipObj);

	});
	
	
	//add 'self' to blood relationships
	for(var i=1; i <= webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length; i++){
		var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var selfRelationshipObj = {
				"individualPersonId" : householdMemberObj.personId.toString(),
				"relatedPersonId" : householdMemberObj.personId.toString(),
				"relation" : '18'
		};	
		
		parimaryContactRelationArr.push(selfRelationshipObj);
	}
	
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship = parimaryContactRelationArr;
	
	
	//only for idaho
	if($("#exchangeName").val().trim() == "Your Health Idaho"){
		//var parimaryContactRelationArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
		var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
		
		$('#over26DependentList').html('');
		
		for(var j=0;j<parimaryContactRelationArr.length;j++){
			
			var relatedMemberObj =findApplicantByPersonId(parimaryContactRelationArr[j].relatedPersonId); //householdMemberObj[parimaryContactRelationArr[j].relatedPersonId - 1];

			//only care relationship to PC
			if(parimaryContactRelationArr[j].individualPersonId == '1'){
				
				//No age check for PC, spouse and ward, so under26Indicator should be always true for them
				if(parimaryContactRelationArr[j].relation == '18' || parimaryContactRelationArr[j].relation == '01' || parimaryContactRelationArr[j].relation == '31'){
					relatedMemberObj.under26Indicator = true;
				//check parent/caretaker of ward
				}else if(parimaryContactRelationArr[j].relation == '03'){
					
					var eligibleForCoverage = false;
					for(var k=0;k<parimaryContactRelationArr.length;k++){
						var dateOfBirth = UIDateformat(relatedMemberObj.dateOfBirth);
						
						if(!ageGreaterThan26(dateOfBirth) || (parimaryContactRelationArr[k].relatedPersonId == '1' && parimaryContactRelationArr[k].individualPersonId == parimaryContactRelationArr[j].relatedPersonId && parimaryContactRelationArr[k].relation == '15')){
							eligibleForCoverage = true;
						}
					}
					
					
					if(eligibleForCoverage == false){
						$('#over26DependentList').append('<p>'+relatedMemberObj.name.firstName + ' ' +relatedMemberObj.name.middleName + ' ' +relatedMemberObj.name.lastName + '</p>');
					}
					
					relatedMemberObj.under26Indicator = eligibleForCoverage;
					
				}else{
					var dateOfBirth = UIDateformat(relatedMemberObj.dateOfBirth);
					// if they are 26 years or over, mark under26Indicator as false
					if(ageGreaterThan26(dateOfBirth)){
						
						$('#over26DependentList').append('<p>'+relatedMemberObj.name.firstName + ' ' +relatedMemberObj.name.middleName + ' ' +relatedMemberObj.name.lastName + '</p>');
						
						relatedMemberObj.under26Indicator = false;
					}
					else{
						relatedMemberObj.under26Indicator = true;
					}
				}
			}
		}
		
		if($('#over26DependentList p').length > 0){
			updatePrimaryContactName();
			$('#over26DependentAlert').modal();
			showHouseHoldContactSummary();
		}else{
			showHouseHoldContactSummary();
			next();
		}
		
	}else{
		showHouseHoldContactSummary();
		next();
	}
	
	/*var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
	
	//before save, clear all relationship
	for(var i = 0; i < householdMemberObj.length; i++ ){
		householdMemberObj[i].bloodRelationship = [];
	}
	
	//save relationship data
	$('#relationshipSelection select').each(function(){		
		//get head number as individual person ID 
		var individualPersonId = $(this).attr("id").match(/^\d+/)[0];
		//get tail munber as related person ID
		var relatedPersonId = $(this).attr("id").match(/\d+$/)[0];
		//get relationship number
		var relationshipNum = $(this).val();
		
		//update relationship
		var relationshipObj = {
				"individualPersonId" : individualPersonId,
				"relatedPersonId" : relatedPersonId,
				"relation" : relationshipNum.match(/^\d+/)[0]
		};	
		householdMemberObj[individualPersonId-1].bloodRelationship.push(relationshipObj);
		
		//update related relationshiop		
		var relatedRelationshipObj = {
				"individualPersonId" : relatedPersonId,
				"relatedPersonId" : individualPersonId,
				"relation" : getReveseredRel(relationshipNum)
		};
		householdMemberObj[relatedPersonId-1].bloodRelationship.push(relatedRelationshipObj);
		
		

	});*/
	
	/*var noofrelations = 0;
	
	$('#appscrBloodRelTableBody tr').each(
			function() {				
					var Indiv = $(this).find(".Indiv").html();
					var Individ = $(this).find(".Individ").html();
					var rel = $(this).find(".rel").html();
					var relid = $(this).find(".relid").html();
					var relt = $(this).find(".relt").html();
					var relat = $(this).find(".relat").html();
					var pct = $(this).find(".pct").html();
					var tax = $(this).find(".tax").html();
					tempJson = getDefatulJson();
					objBloodRelation = tempJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship[0];
					
					objBloodRelation.individualPersonId = Individ;
					objBloodRelation.relatedPersonId = relid;
					objBloodRelation.relation = relt;
					objBloodRelation.textDependency = false;
					if(tax == 'Yes')
						objBloodRelation.textDependency = true;
					
					//householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[Individ-1];
					householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];					
					householdMemberObj.bloodRelationship[noofrelations] = objBloodRelation;			
					noofrelations++;
			});*/
}

function getBloodRelation(relationShipCode){
	return $("#relat option[value="+relationShipCode+"]").text();
}

function updateSingleMemRel(){
	var parimaryContactRelationArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
	//before save, clear all relationship
	parimaryContactRelationArr = [];
	
	//add 'self' to blood relationships
	var selfRelationshipObj = {
			"individualPersonId" : "1",
			"relatedPersonId" : "1",
			"relation" : '18'
	};	
		
	parimaryContactRelationArr.push(selfRelationshipObj);
	
	
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship = parimaryContactRelationArr;
	
}

function getReveseredRel(rel){
	var reversedRel = '';
	
	switch(rel) {
		case '01':
	    	reversedRel = '01';
	        break;
		case '03':
	    	reversedRel = '19';
	        break;
		case '03a':
	    	reversedRel = '09';
	        break;
		case '03f':
	    	reversedRel = '10';
	        break;  
		case '03w':
	    	reversedRel = '15';
	        break;      
		case '04':
	    	reversedRel = '05';
	        break;    
	    case '05':
	    	reversedRel = '04';
	        break;        
	    case '06':
	    	reversedRel = '07';
	        break;
	    case '07':
	    	reversedRel = '06';
	        break;
	    case '08':
	    	reversedRel = '08';
	        break;
	    case '09':
	    	reversedRel = '03';
	        break;        
	    case '10':
	    	reversedRel = '03';
	        break;
	    case '11':
	    	reversedRel = '13';
	        break;          
	    case '12':
	    	reversedRel = '12';
	        break;
	    case '13':
	    	reversedRel = '11';
	        break;        
	    case '14':
	    	reversedRel = '14';
	        break;
	    case '15':
	    	reversedRel = '26';
	        break;
	    case '15c':
	    	reversedRel = '31';
	        break;
	    case '15p':
	    	reversedRel = '03';
	        break;    
	    case '16':
	    	reversedRel = '17';
	        break;
	    case '17':
	    	reversedRel = '16';
	        break;
	    case '18':
	    	reversedRel = '18';
	        break;        
	    case '19':
	    	reversedRel = '03';
	        break;
	    case '25':
	    	reversedRel = '25';
	        break;	
	    case '26':
	    	reversedRel = '15';
	        break;
	    case '31':
	    	reversedRel = '15';
	        break;        
	    case '53':
	    	reversedRel = '53';
	        break;
	    case 'G8':
	    	reversedRel = 'G8';
	        break;
	    case 'G9':
	    	reversedRel = 'G9';
	        break;
	    case '03-53':
	    	reversedRel = '53-19';
	        break;        
	    case '53-19':
	    	reversedRel = '03-53';
	    	break;
	    //following relationship does not have reversed mapping, need update from Abhinav
	    case '23':
	    	reversedRel = '23';
	    	break;
	    case '24':
	    	reversedRel = '24';
	    	break;
	    case '38':
	    	reversedRel = '38';
	    	break;
	    case '60':
	    	reversedRel = '60';
	    	break;
	    case 'D2':
	    	reversedRel = 'D2';	
	        break;	    
}
	
	return reversedRel;
}


function showNextSelection(e){
	var showNextFlag = true;
	e.closest('li[id^=relationshipSelection]').find('select').each(function(){
		if($(this).val() == ''){
			showNextFlag = false;
		}
	});
	
	var  nextSelection = e.closest('li').next();
	if(showNextFlag && nextSelection.find('select').length > 0){
		nextSelection.fadeIn("slow");
	}
}

/*
function checkMultipleMapping(e,j,i,householdMemberRelatetionshipObjFullName,householdMemberObjFullName){
	
	var relationshipSelection = '';
	
	if(e.val() == '03'){
		
		relationshipSelection += '<div class="control-group">';
		relationshipSelection += '<label class="control-label capitalize-none">'+ householdMemberObjFullName +' is ' + householdMemberRelatetionshipObjFullName + '\'s </label>';
		relationshipSelection += '<div class="controls">';
		relationshipSelection += '<select onchange="showNextSelection($(this));" id="' + i + 'to' + j + '" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.relationshipRequired")+'">' + $('#parentRelationshipOptions').html() + '</select>';
		relationshipSelection += '</div></div>';
		
		$('#relationshipSelection' + i + ' .multipleParent').html(relationshipSelection);
	}
	
}*/







