package com.getinsured.eligibility.at.ref.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.Ethnicity;
import com.getinsured.iex.ssap.EthnicityAndRace;
import com.getinsured.iex.ssap.OtherPhone;
import com.getinsured.iex.ssap.Phone;
import com.getinsured.iex.ssap.Race;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
public class CompareApplicantDTO {
	private long id;

	private long personId;

	private String externalApplicantId;

	private String applicantGuid;

	private String applyingForCoverage;

	private String firstName;
	
	private String nameSuffix;

	private String middleName;

	private String lastName;

	private String ssn;

	private String married;
	
	private String writtenLanguage;
	
	private String spokenLanguage;
	
	private Date birthDate;

	private String gender;

	private String emailAddress;

	private EthnicityAndRace ethnicityAndRace = new EthnicityAndRace();

	private Address primaryAddress = new Address();

	private Address mailingAddress = new Address();

	private Phone phone = new Phone();

	private OtherPhone otherPhone = new OtherPhone();

	private List<BloodRelationship> bloodRelationships = new ArrayList<BloodRelationship>();

	private boolean citizenShipStatus = false;

	private boolean enrolled = false;

	private boolean subscriber = false;

	private String status;

	private String eligibilityStatus;

	private boolean adminUpdate;

	private boolean firstNameChanged;
	
	private boolean nameSuffixChanged;

	private boolean middleNameChanged;

	private boolean lastNameChanged;

	private boolean ssnChanged;

	private boolean marriedChanged;
	
	private boolean writtenLanguageChanged;
	
	private boolean spokenLanguageChanged;
	
	private boolean addressLine1Changed;

	private boolean addressLine2Changed;
	
	private boolean cityChanged;

	private boolean mailAddressChanged;

	private boolean dobChanged;

	private boolean primaryAddressChanged;

	private boolean zipCountyChanged;

	private boolean genderChanged;

	private boolean emailAddressChanged;

	private boolean raceEthnicityChanged;

	private boolean phoneChanged;

	private boolean citizenShipStatusChanged;

	private boolean bloodRelationshipChanged;
	
	private boolean primaryTaxFilerChange;

	private boolean externalApplicantIdChanged;

	private CompareApplicationDTO compareApplicationDTO;

	private boolean primaryHHLvlChange;
	
	private String csrLevel;
	
	private Address enrolledHomeAddress;
	
	private boolean isPartialAddressNotUpdated;
	
	private SsapApplicantPersonType personType;
	
	private boolean isAPTCEligible;
	
	private boolean isCSREligible;
	
	private boolean isExchangeEligible;
	
	private boolean isSeekQHP = true;

	public boolean isPartialAddressNotUpdated() {
		return isPartialAddressNotUpdated;
	}

	public void setPartialAddressNotUpdated(boolean isPartialAddressNotUpdated) {
		this.isPartialAddressNotUpdated = isPartialAddressNotUpdated;
	}

	public Address getEnrolledHomeAddress() {
		return enrolledHomeAddress;
	}

	public void setEnrolledHomeAddress(Address enrolledHomeAddress) {
		this.enrolledHomeAddress = enrolledHomeAddress;
	}

	public boolean isCitizenShipStatus() {
		return citizenShipStatus;
	}

	public void setCitizenShipStatus(boolean citizenShipStatus) {
		this.citizenShipStatus = citizenShipStatus;
	}

	public boolean isCitizenShipStatusChanged() {
		return citizenShipStatusChanged;
	}

	public void setCitizenShipStatusChanged(boolean citizenShipStatusChanged) {
		this.citizenShipStatusChanged = citizenShipStatusChanged;
	}

	public Phone getPhone() {
		return phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public OtherPhone getOtherPhone() {
		return otherPhone;
	}

	public void setOtherPhone(OtherPhone otherPhone) {
		this.otherPhone = otherPhone;
	}

	public boolean isPhoneChanged() {
		return phoneChanged;
	}

	public void setPhoneChanged(boolean phoneChanged) {
		this.phoneChanged = phoneChanged;
	}

	public boolean isGenderChanged() {
		return genderChanged;
	}

	public EthnicityAndRace getEthnicityAndRace() {
		return ethnicityAndRace;
	}

	public void setEthnicityAndRace(EthnicityAndRace ethnicityAndRace) {
		this.ethnicityAndRace = ethnicityAndRace;
	}

	public boolean isRaceEthnicityChanged() {
		return raceEthnicityChanged;
	}

	public void setRaceEthnicityChanged(boolean raceEthnicityChanged) {
		this.raceEthnicityChanged = raceEthnicityChanged;
	}

	public void setGenderChanged(boolean genderChanged) {
		this.genderChanged = genderChanged;
	}

	public boolean isEmailAddressChanged() {
		return emailAddressChanged;
	}

	public void setEmailAddressChanged(boolean emailAddressChanged) {
		this.emailAddressChanged = emailAddressChanged;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public boolean isMiddleNameChanged() {
		return middleNameChanged;
	}

	public void setMiddleNameChanged(boolean middleNameChanged) {
		this.middleNameChanged = middleNameChanged;
	}

	public boolean isSubscriber() {
		return subscriber;
	}

	public void setSubscriber(boolean subscriber) {
		this.subscriber = subscriber;
	}

	public boolean isFirstNameChanged() {
		return firstNameChanged;
	}

	public void setFirstNameChanged(boolean firstNameChanged) {
		this.firstNameChanged = firstNameChanged;
	}
	
	public boolean isNameSuffixChanged() {
		return nameSuffixChanged;
	}

	public void setNameSuffixChanged(boolean nameSuffixChanged) {
		this.nameSuffixChanged = nameSuffixChanged;
	}

	public boolean isLastNameChanged() {
		return lastNameChanged;
	}

	public void setLastNameChanged(boolean lastNameChanged) {
		this.lastNameChanged = lastNameChanged;
	}

	public boolean isSsnChanged() {
		return ssnChanged;
	}

	public void setSsnChanged(boolean ssnChanged) {
		this.ssnChanged = ssnChanged;
	}

	public boolean isAddressLine1Changed() {
		return addressLine1Changed;
	}

	public void setAddressLine1Changed(boolean addressLine1Changed) {
		this.addressLine1Changed = addressLine1Changed;
	}

	public boolean isAddressLine2Changed() {
		return addressLine2Changed;
	}

	public void setAddressLine2Changed(boolean addressLine2Changed) {
		this.addressLine2Changed = addressLine2Changed;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getExternalApplicantId() {
		return externalApplicantId;
	}

	public void setExternalApplicantId(String externalApplicantId) {
		this.externalApplicantId = externalApplicantId;
	}

	public String getApplicantGuid() {
		return applicantGuid;
	}

	public void setApplicantGuid(String applicantGuid) {
		this.applicantGuid = applicantGuid;
	}

	public String getApplyingForCoverage() {
		return applyingForCoverage;
	}

	public void setApplyingForCoverage(String applyingForCoverage) {
		this.applyingForCoverage = applyingForCoverage;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Address getPrimaryAddress() {
		return primaryAddress;
	}

	public void setPrimaryAddress(Address primaryAddress) {
		this.primaryAddress = primaryAddress;
	}

	public Address getMailingAddress() {
		return mailingAddress;
	}

	public void setMailingAddress(Address mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public boolean isEnrolled() {
		return enrolled;
	}

	public void setEnrolled(boolean enrolled) {
		this.enrolled = enrolled;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isAdminUpdate() {
		return adminUpdate;
	}

	public void setAdminUpdate(boolean adminUpdate) {
		this.adminUpdate = adminUpdate;
	}

	public boolean isDobChanged() {
		return dobChanged;
	}

	public void setDobChanged(boolean dobChanged) {
		this.dobChanged = dobChanged;
	}

	public boolean isZipCountyChanged() {
		return zipCountyChanged;
	}

	public void setZipCountyChanged(boolean zipCountyChanged) {
		this.zipCountyChanged = zipCountyChanged;
	}

	public CompareApplicationDTO getCompareApplicationDTO() {
		return compareApplicationDTO;
	}

	public void setCompareApplicationDTO(CompareApplicationDTO compareApplicationDTO) {
		this.compareApplicationDTO = compareApplicationDTO;
	}

	public boolean isPrimaryAddressChanged() {
		return primaryAddressChanged;
	}

	public void setPrimaryAddressChanged(boolean primaryAddressChanged) {
		this.primaryAddressChanged = primaryAddressChanged;
	}

	public boolean isMailAddressChanged() {
		return mailAddressChanged;
	}

	public void setMailAddressChanged(boolean mailAddressChanged) {
		this.mailAddressChanged = mailAddressChanged;
	}

	public List<BloodRelationship> getBloodRelationships() {
		return bloodRelationships;
	}

	public void setBloodRelationships(List<BloodRelationship> bloodRelationships) {
		this.bloodRelationships = bloodRelationships;
	}

	public boolean isExternalApplicantIdChanged() {
		return externalApplicantIdChanged;
	}

	public void setExternalApplicantIdChanged(boolean externalApplicantIdChanged) {
		this.externalApplicantIdChanged = externalApplicantIdChanged;
	}
	
	public boolean isBloodRelationshipChanged() {
		return bloodRelationshipChanged;
	}

	public void setBloodRelationshipChanged(boolean bloodRelationshipChanged) {
		this.bloodRelationshipChanged = bloodRelationshipChanged;
	}

	private boolean treatNullEmptySameEqualsIgnoreCase(String str1,String str2) {
		if(str1 == null && str2 != null && StringUtils.isEmpty(str2)) {
			return true;	
		} else if(str2 == null && str1 != null && StringUtils.isEmpty(str1)) {
			return true;
		}
		return StringUtils.equalsIgnoreCase(  str1,  str2);
	}
	public void compareDemographic(CompareApplicantDTO currentApplicant,Address houseHoldMailingAddress) {
		// System.out.println("Current " + currentApplicant.getApplicantGuid() + " : " + currentApplicant.getExternalApplicantId());
		// System.out.println("Enrolled " + this.getApplicantGuid() + " : " + this.getExternalApplicantId());
		final boolean blnFName = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getFirstName(), this.getFirstName());
		this.firstNameChanged = !blnFName;
		
		// Added to compare the suffix
		final boolean blnNameSuffix = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getNameSuffix(), this.getNameSuffix());
		this.nameSuffixChanged = !blnNameSuffix;

		final boolean blnMName = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getMiddleName(), this.getMiddleName());
		this.middleNameChanged = !blnMName;

		final boolean blnLName = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getLastName(), this.getLastName());
		this.lastNameChanged = !blnLName;

		final boolean blnSSN = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getSsn(), this.getSsn());
		this.ssnChanged = !blnSSN;

		final boolean  blnMarried = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getMarried(), this.getMarried());
		this.marriedChanged = !blnMarried;
		
		final boolean blnWrittenLanguage = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getWrittenLanguage(), this.getWrittenLanguage());
		this.writtenLanguageChanged = !blnWrittenLanguage;
		
		final boolean blnSpokenLanguage = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getSpokenLanguage(), this.getSpokenLanguage());
		this.spokenLanguageChanged = !blnSpokenLanguage;
		
		final boolean blnGender = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getGender(), this.getGender());
		this.genderChanged = !blnGender;

		final boolean blnEmailAddress = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getEmailAddress(), this.getEmailAddress());
		this.emailAddressChanged = !blnEmailAddress;

		final boolean blnraceEthnicity = !compareEthnicityAndRace(currentApplicant.getEthnicityAndRace());
		this.raceEthnicityChanged = !blnraceEthnicity;

		final boolean blnPhone = comparePhone(currentApplicant.getPhone(), currentApplicant.getOtherPhone());
		this.phoneChanged = !blnPhone;

		// new change added for PTF change
		final boolean blnPTFChange = isPrimaryTaxFilerChanged(currentApplicant.getPersonType(), this.getPersonType());
		this.primaryTaxFilerChange = blnPTFChange;

		final boolean blnEid = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getExternalApplicantId(), this.getExternalApplicantId());
		this.externalApplicantIdChanged = !blnEid;

		// System.out.println("currentApplicant.getPrimaryAddress()");
		// System.out.println(currentApplicant.getPrimaryAddress().getStreetAddress1() + " : " + this.getPrimaryAddress().getStreetAddress1());
		// System.out.println(currentApplicant.getPrimaryAddress().getStreetAddress2() + " : " + this.getPrimaryAddress().getStreetAddress2());
		// System.out.println(currentApplicant.getPrimaryAddress().getCity() + " : " + this.getPrimaryAddress().getCity());
		// System.out.println(currentApplicant.getPrimaryAddress().getState() + " : " + this.getPrimaryAddress().getState());
		// System.out.println(currentApplicant.getPrimaryAddress().getPostalCode() + " : " + this.getPrimaryAddress().getPostalCode());
		// System.out.println(currentApplicant.getPrimaryAddress().getCounty() + " : " + this.getPrimaryAddress().getCounty());
		// System.out.println(currentApplicant.getPrimaryAddress().getPrimaryAddressCountyFipsCode() + " : " + this.getPrimaryAddress().getPrimaryAddressCountyFipsCode());

		// System.out.println("currentApplicant.getMailingAddress()");
		// System.out.println(currentApplicant.getMailingAddress().getStreetAddress1() + " : " + this.getMailingAddress().getStreetAddress1());
		// System.out.println(currentApplicant.getMailingAddress().getStreetAddress2() + " : " + this.getMailingAddress().getStreetAddress2());
		// System.out.println(currentApplicant.getMailingAddress().getCity() + " : " + this.getMailingAddress().getCity());
		// System.out.println(currentApplicant.getMailingAddress().getState() + " : " + this.getMailingAddress().getState());
		// System.out.println(currentApplicant.getMailingAddress().getPostalCode() + " : " + this.getMailingAddress().getPostalCode());
		// System.out.println(currentApplicant.getMailingAddress().getCounty() + " : " + this.getMailingAddress().getCounty());
		// System.out.println(currentApplicant.getMailingAddress().getPrimaryAddressCountyFipsCode() + " : " + this.getMailingAddress().getPrimaryAddressCountyFipsCode());

		boolean blnAdminUpdate;

		if (this.subscriber) {
			boolean primaryaddressNotChanged, primaryaddressLine1NotChanged, primaryaddressLine2NotChanged , primaryCityNotChanged;
			
			primaryaddressNotChanged = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getState(), this.getEnrolledHomeAddress().getState())
			        && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getPostalCode(), this.getEnrolledHomeAddress().getPostalCode())
			        && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getCounty(), this.getEnrolledHomeAddress().getCounty())
			        && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getPrimaryAddressCountyFipsCode(), this.getEnrolledHomeAddress().getPrimaryAddressCountyFipsCode());
		
			if(!primaryaddressNotChanged) {
			    this.setPartialAddressNotUpdated(true);
		    }
			
			primaryaddressLine1NotChanged = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getStreetAddress1(), this.getEnrolledHomeAddress().getStreetAddress1());
			this.addressLine1Changed = !primaryaddressLine1NotChanged && primaryaddressNotChanged;
	
			primaryaddressLine2NotChanged = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getStreetAddress2(), this.getEnrolledHomeAddress().getStreetAddress2());
			this.addressLine2Changed = !primaryaddressLine2NotChanged && primaryaddressNotChanged;
			
			primaryCityNotChanged = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getCity(), this.getEnrolledHomeAddress().getCity());
			this.cityChanged=!primaryCityNotChanged   && primaryaddressNotChanged;
			
			final boolean mailaddressNotChanged = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getMailingAddress().getStreetAddress1(), houseHoldMailingAddress.getStreetAddress1())
			        && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getMailingAddress().getStreetAddress2(), houseHoldMailingAddress.getStreetAddress2())
			        && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getMailingAddress().getCity(), houseHoldMailingAddress.getCity()) && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getMailingAddress().getState(), houseHoldMailingAddress.getState())
			        && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getMailingAddress().getPostalCode(), houseHoldMailingAddress.getPostalCode())
			        && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getMailingAddress().getCounty(), houseHoldMailingAddress.getCounty());
			this.mailAddressChanged = !mailaddressNotChanged;

			blnAdminUpdate = !blnFName || !blnNameSuffix || !blnMName || !blnLName || !blnSSN || !blnMarried || !blnWrittenLanguage || !blnSpokenLanguage  || !blnGender || !blnEmailAddress || !blnraceEthnicity || !blnPhone || !mailaddressNotChanged
			         || ((!primaryaddressLine1NotChanged || !primaryaddressLine2NotChanged || !primaryCityNotChanged) && primaryaddressNotChanged) || blnPTFChange || !blnEid;
			
			//Added as HIX-108613 - 
			this.primaryHHLvlChange = blnPTFChange || !blnEmailAddress ||  !blnPhone  || !mailaddressNotChanged || ((!primaryaddressLine1NotChanged || !primaryaddressLine2NotChanged || !primaryCityNotChanged) && primaryaddressNotChanged);
			 
		} else {
			final boolean primaryaddressNotChanged = treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getStreetAddress1(), this.getPrimaryAddress().getStreetAddress1())
			        && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getStreetAddress2(), this.getPrimaryAddress().getStreetAddress2())
			        && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getCity(), this.getPrimaryAddress().getCity())
			        && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getState(), this.getPrimaryAddress().getState())
			        && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getPostalCode(), this.getPrimaryAddress().getPostalCode())
			        && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getCounty(), this.getPrimaryAddress().getCounty())
			        && treatNullEmptySameEqualsIgnoreCase(currentApplicant.getPrimaryAddress().getPrimaryAddressCountyFipsCode(), this.getPrimaryAddress().getPrimaryAddressCountyFipsCode());

			this.primaryAddressChanged = !primaryaddressNotChanged;
			blnAdminUpdate = !blnFName || !blnNameSuffix || !blnMName || !blnLName || !blnSSN || !blnMarried || !blnWrittenLanguage || !blnSpokenLanguage || !blnGender || !blnEmailAddress || !blnraceEthnicity || !blnPhone || !primaryaddressNotChanged || blnPTFChange || !blnEid;
			// set the primary HH level change if true
			this.primaryHHLvlChange = blnPTFChange;
				
			}

		this.adminUpdate = blnAdminUpdate;

		// System.out.println("this.adminUpdate " + this.adminUpdate);
		// System.out.println("this.firstNameChanged " + this.firstNameChanged);
		// System.out.println("this.middleNameChanged " + this.middleNameChanged);
		// System.out.println("this.lastNameChanged " + this.lastNameChanged);
		// System.out.println("this.ssnChanged " + this.ssnChanged);
		// System.out.println("this.genderChanged " + this.genderChanged);
		// System.out.println("this.phoneChanged " + this.phoneChanged);
		// System.out.println("this.emailAddressChanged " + this.emailAddressChanged);
		// System.out.println("this.primaryAddressChanged " + this.primaryAddressChanged);
		// System.out.println("this.mailAddressChanged " + this.mailAddressChanged);
		// System.out.println("this.addressLine1Changed " + this.addressLine1Changed);
		// System.out.println("this.addressLine2Changed " + this.addressLine2Changed);

		updateCurrentApplicant(currentApplicant);

		if (this.adminUpdate) {
			this.getCompareApplicationDTO().setHasAdminUpdate(true);
			currentApplicant.getCompareApplicationDTO().setHasAdminUpdate(true);
			updateAdminUpdateChangedValues(currentApplicant);
		}

		if (this.primaryAddressChanged || this.addressLine1Changed || this.addressLine2Changed || this.mailAddressChanged || this.cityChanged) {
			this.getCompareApplicationDTO().setHasAddressChanged(true);
			currentApplicant.getCompareApplicationDTO().setHasAddressChanged(true);
		}
	}

	private boolean isPrimaryTaxFilerChanged(SsapApplicantPersonType currentApplicantPersonType, SsapApplicantPersonType enrolledApplicantPersonType) {
		// compare the PTF change 
		if((null != currentApplicantPersonType && currentApplicantPersonType.getPersonType() != null && currentApplicantPersonType.getPersonType().contains(SsapApplicantPersonType.PTF.toString()) &&
				null != enrolledApplicantPersonType && enrolledApplicantPersonType.getPersonType() != null && !(enrolledApplicantPersonType.getPersonType().contains(SsapApplicantPersonType.PTF.toString())))
			|| (null != currentApplicantPersonType && currentApplicantPersonType.getPersonType() != null && !(currentApplicantPersonType.getPersonType().contains(SsapApplicantPersonType.PTF.toString())) &&
					null != enrolledApplicantPersonType && enrolledApplicantPersonType.getPersonType() != null && (enrolledApplicantPersonType.getPersonType().contains(SsapApplicantPersonType.PTF.toString()))))
		{
			return true;
		} else {
			return false;
		}
			
	}

	private boolean comparePhone(Phone currentPhone, OtherPhone currentOtherPhone) {
		return treatNullEmptySameEqualsIgnoreCase(currentPhone.getPhoneNumber(), this.phone.getPhoneNumber()) && treatNullEmptySameEqualsIgnoreCase(currentPhone.getPhoneType(), this.phone.getPhoneType())
		        && treatNullEmptySameEqualsIgnoreCase(currentPhone.getPhoneExtension(), this.phone.getPhoneExtension()) && treatNullEmptySameEqualsIgnoreCase(currentOtherPhone.getPhoneNumber(), this.otherPhone.getPhoneNumber())
		        && treatNullEmptySameEqualsIgnoreCase(currentOtherPhone.getPhoneType(), this.otherPhone.getPhoneType()) && treatNullEmptySameEqualsIgnoreCase(currentOtherPhone.getPhoneExtension(), this.otherPhone.getPhoneExtension());
	}

	private void updateCurrentApplicant(CompareApplicantDTO currentApplicant) {
		currentApplicant.setAdminUpdate(this.adminUpdate);
		currentApplicant.setFirstNameChanged(this.firstNameChanged);
		currentApplicant.setMiddleNameChanged(this.middleNameChanged);
		currentApplicant.setLastNameChanged(this.lastNameChanged);
		currentApplicant.setNameSuffixChanged(this.nameSuffixChanged);
		currentApplicant.setSsnChanged(this.ssnChanged);
		currentApplicant.setGenderChanged(this.genderChanged);
		currentApplicant.setEmailAddressChanged(this.emailAddressChanged);
		currentApplicant.setRaceEthnicityChanged(this.raceEthnicityChanged);
		currentApplicant.setPhoneChanged(this.phoneChanged);
		currentApplicant.setMailAddressChanged(this.mailAddressChanged);
		currentApplicant.setAddressLine1Changed(this.addressLine1Changed);
		currentApplicant.setAddressLine2Changed(this.addressLine2Changed);
		currentApplicant.setPrimaryAddressChanged(this.primaryAddressChanged);
		currentApplicant.setPrimaryHHLvlChange(this.primaryHHLvlChange);
		currentApplicant.setCityChanged(this.cityChanged);
		currentApplicant.setMarriedChanged(this.marriedChanged);
		currentApplicant.setWrittenLanguageChanged(this.writtenLanguageChanged);
		currentApplicant.setSpokenLanguageChanged(this.spokenLanguageChanged);
		// added to set the PTF change to the applicant
		currentApplicant.setPrimaryTaxFilerChange(this.primaryTaxFilerChange);
		currentApplicant.setExternalApplicantIdChanged(this.externalApplicantIdChanged);;
		
	}

	private void updateAdminUpdateChangedValues(CompareApplicantDTO currentApplicant) {
		this.setFirstName(currentApplicant.getFirstName());
		this.setNameSuffix(currentApplicant.getNameSuffix());
		this.setMiddleName(currentApplicant.getMiddleName());
		this.setLastName(currentApplicant.getLastName());
		this.setSsn(currentApplicant.getSsn());
		this.setGender(currentApplicant.getGender());
		this.setEmailAddress(currentApplicant.getEmailAddress());
		this.setEthnicityAndRace(currentApplicant.getEthnicityAndRace());
		this.setPhone(currentApplicant.getPhone());
		this.setOtherPhone(currentApplicant.getOtherPhone());
		this.setExternalApplicantId(currentApplicant.getExternalApplicantId());
		if (this.primaryAddressChanged || this.addressLine1Changed || this.addressLine2Changed || this.cityChanged) {
			this.setPrimaryAddress(currentApplicant.getPrimaryAddress());
		}
		if (this.mailAddressChanged) {
			this.setMailingAddress(currentApplicant.getMailingAddress());
		}
		if(this.primaryTaxFilerChange) {
			this.setPersonType(currentApplicant.getPersonType());
		}
		
		if (this.marriedChanged) {
			this.setMarried(currentApplicant.getMarried());
		}
		if (this.writtenLanguageChanged) {
			this.setWrittenLanguage(currentApplicant.getWrittenLanguage());
		}
		if (this.spokenLanguageChanged) {
			this.setSpokenLanguage(currentApplicant.getSpokenLanguage());
		}
	
	}

	public void compareZipCounty(CompareApplicantDTO currentApplicant) {
		if (this.subscriber) {
			// System.out.println("Current " + currentApplicant.getApplicantGuid() + " : " + currentApplicant.getExternalApplicantId());
			// System.out.println("Enrolled " + this.getApplicantGuid() + " : " + this.getExternalApplicantId());
			final boolean blnZipCounty = StringUtils.equalsIgnoreCase(currentApplicant.getPrimaryAddress().getPostalCode(), this.getEnrolledHomeAddress().getPostalCode())
				        && StringUtils.equalsIgnoreCase(currentApplicant.getPrimaryAddress().getPrimaryAddressCountyFipsCode(), this.getEnrolledHomeAddress().getPrimaryAddressCountyFipsCode())
				        && StringUtils.equalsIgnoreCase(currentApplicant.getPrimaryAddress().getCounty(), this.getEnrolledHomeAddress().getCounty());
			
			this.zipCountyChanged = !blnZipCounty;
			currentApplicant.setZipCountyChanged(this.zipCountyChanged);
			// System.out.println("this.zipCountyChanged " + this.zipCountyChanged);
			if (this.zipCountyChanged) {
				this.getCompareApplicationDTO().setHasZipCountyChanged(true);
				currentApplicant.getCompareApplicationDTO().setHasZipCountyChanged(true);
			}
		}
	}

	public void compareDob(CompareApplicantDTO currentApplicant) {
		final boolean blnDob = DateUtils.isSameDay(currentApplicant.getBirthDate(), this.getBirthDate());
		this.dobChanged = !blnDob;
		currentApplicant.setDobChanged(this.dobChanged);
		if (this.dobChanged) {
			this.getCompareApplicationDTO().setHasDobChanged(true);
			currentApplicant.getCompareApplicationDTO().setHasDobChanged(true);
		}
	}

	public void compareCitizenShipStatus(CompareApplicantDTO currentApplicant) {
		final boolean blnCitizenShipStatus = currentApplicant.isCitizenShipStatus() == this.isCitizenShipStatus();
		this.citizenShipStatusChanged = !blnCitizenShipStatus;
		currentApplicant.setCitizenShipStatusChanged(this.citizenShipStatusChanged);
		if (this.citizenShipStatusChanged) {
			this.getCompareApplicationDTO().setHasCitizenShipStatusChanged(true);
			currentApplicant.getCompareApplicationDTO().setHasCitizenShipStatusChanged(true);
		}
	}

	private boolean compareEthnicityAndRace(EthnicityAndRace ethnicityAndRace) {
		boolean matchFound;
		boolean isRaceEthnicityChanges = false;
		if (!ReferralUtil.compareBoolean(this.ethnicityAndRace.getHispanicLatinoSpanishOriginIndicator(), ethnicityAndRace.getHispanicLatinoSpanishOriginIndicator())
		        || this.ethnicityAndRace.getEthnicity().size() != ethnicityAndRace.getEthnicity().size() || this.ethnicityAndRace.getRace().size() != ethnicityAndRace.getRace().size()) {
			return true;
		}
		// compareRace
		for (Race raceCurrent : this.ethnicityAndRace.getRace()) {
			matchFound = false;
			for (Race race : ethnicityAndRace.getRace()) {
				if (!raceCurrent.isComparedLogic() && StringUtils.equalsIgnoreCase(raceCurrent.getCode(), race.getCode()) && StringUtils.equalsIgnoreCase(raceCurrent.getLabel(), race.getLabel())
				        && StringUtils.equalsIgnoreCase(raceCurrent.getOtherLabel(), race.getOtherLabel())) {
					matchFound = true;
					raceCurrent.setComparedLogic(true);
					break;
				}
			}
			if (matchFound) {
				continue;
			} else {
				return true;
			}
		}
		// compareEthnicity
		for (Ethnicity ethnicityCurrent : this.ethnicityAndRace.getEthnicity()) {
			matchFound = false;
			for (Ethnicity ethnicity : ethnicityAndRace.getEthnicity()) {
				if (!ethnicityCurrent.isComparedLogic() && StringUtils.equalsIgnoreCase(ethnicityCurrent.getCode(), ethnicity.getCode()) && StringUtils.equalsIgnoreCase(ethnicityCurrent.getLabel(), ethnicity.getLabel())
				        && StringUtils.equalsIgnoreCase(ethnicityCurrent.getOtherLabel(), ethnicity.getOtherLabel())) {
					matchFound = true;
					ethnicityCurrent.setComparedLogic(true);
					break;
				}
			}
			if (matchFound) {
				continue;
			} else {
				return true;
			}
		}
		return isRaceEthnicityChanges;
	}

	public void compareBloodRelationship(CompareApplicantDTO currentPrimaryApplicant, Map<String, String> currentidMap, Map<String, String> enrolledidMap) {
		boolean matchFound;
		String individualGUIdEnrolled, relatedPersonIdEnrolled, individualGUIdCurrent, relatedPersonIdCurrent;
		for (BloodRelationship bloodrelationshipEnrolled : this.bloodRelationships) {
			matchFound = false;
			individualGUIdEnrolled = enrolledidMap.get(bloodrelationshipEnrolled.getIndividualPersonId());
			relatedPersonIdEnrolled = enrolledidMap.get(bloodrelationshipEnrolled.getRelatedPersonId());
			if (individualGUIdEnrolled != null && relatedPersonIdEnrolled != null) {
				for (BloodRelationship bloodrelationshipCurrent : currentPrimaryApplicant.bloodRelationships) {
					individualGUIdCurrent = currentidMap.get(bloodrelationshipCurrent.getIndividualPersonId());
					relatedPersonIdCurrent = currentidMap.get(bloodrelationshipCurrent.getRelatedPersonId());
					if (StringUtils.equalsIgnoreCase(individualGUIdEnrolled, individualGUIdCurrent) && StringUtils.equalsIgnoreCase(relatedPersonIdEnrolled, relatedPersonIdCurrent)) {
						if (StringUtils.equalsIgnoreCase(bloodrelationshipEnrolled.getRelation(), bloodrelationshipCurrent.getRelation())) {
							matchFound = true;
						}
						break;
					}
				}
				if (matchFound) {
					continue;
				} else {
					updateBloodRelationship(currentPrimaryApplicant);
					break;
				}
			}
		}
	}

	private void updateBloodRelationship(CompareApplicantDTO currentApplicant) {
		this.bloodRelationshipChanged = true;
		currentApplicant.setBloodRelationshipChanged(this.bloodRelationshipChanged);
		if (this.bloodRelationshipChanged) {
			this.getCompareApplicationDTO().setHasBloodRelationshipChanged(this.bloodRelationshipChanged);
			currentApplicant.getCompareApplicationDTO().setHasBloodRelationshipChanged(this.bloodRelationshipChanged);
		}
	}

	public boolean isPrimaryHHLvlChange() {
		return primaryHHLvlChange;
	}

	public void setPrimaryHHLvlChange(boolean primaryHHLvlChange) {
		this.primaryHHLvlChange = primaryHHLvlChange;
	}

	public boolean isCityChanged() {
		return cityChanged;
	}

	public void setCityChanged(boolean cityChanged) {
		this.cityChanged = cityChanged;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}
	
	public SsapApplicantPersonType getPersonType() {
		return personType;
	}

	public void setPersonType(SsapApplicantPersonType personType) {
		this.personType = personType;
	}
	
	public boolean isPrimaryContact(){
		if(this.personType != null && this.personType.getPersonType().contains(SsapApplicantPersonType.PC.toString())){
			return Boolean.TRUE;
		}
		else{
			return Boolean.FALSE;
		}
	}
	
	public boolean isPrimaryTaxFiler(){
		if(this.personType != null && this.personType.getPersonType().contains(SsapApplicantPersonType.PTF.toString())){
			return Boolean.TRUE;
		}
		else{
			return Boolean.FALSE;
		}
	}

	public String getNameSuffix() {
		return nameSuffix;
	}

	public void setNameSuffix(String nameSuffix) {
		this.nameSuffix = nameSuffix;
	}	
	
	public boolean isAPTCEligible() {
		return isAPTCEligible;
	}

	public void setAPTCEligible(boolean isAPTCEligible) {
		this.isAPTCEligible = isAPTCEligible;
	}

	public boolean isCSREligible() {
		return isCSREligible;
	}

	public void setCSREligible(boolean isCSREligible) {
		this.isCSREligible = isCSREligible;
	}

	public boolean isExchangeEligible() {
		return isExchangeEligible;
	}

	public void setExchangeEligible(boolean isExchangeEligible) {
		this.isExchangeEligible = isExchangeEligible;
	}

	public boolean isPrimaryTaxFilerChange() {
		return primaryTaxFilerChange;
	}

	public void setPrimaryTaxFilerChange(boolean primaryTaxFilerChange) {
		this.primaryTaxFilerChange = primaryTaxFilerChange;
	}

	public String getMarried() {
		return married;
	}

	public void setMarried(String married) {
		this.married = married;
	}

	public boolean isMarriedChanged() {
		return marriedChanged;
	}

	public void setMarriedChanged(boolean marriedChanged) {
		this.marriedChanged = marriedChanged;
	}

	public String getWrittenLanguage() {
		return writtenLanguage;
	}

	public void setWrittenLanguage(String writtenLanguage) {
		this.writtenLanguage = writtenLanguage;
	}

	public String getSpokenLanguage() {
		return spokenLanguage;
	}

	public void setSpokenLanguage(String spokenLanguage) {
		this.spokenLanguage = spokenLanguage;
	}
	
	public boolean isWrittenLanguageChanged() {
		return writtenLanguageChanged;
	}

	public void setWrittenLanguageChanged(boolean writtenLanguageChanged) {
		this.writtenLanguageChanged = writtenLanguageChanged;
	}

	public boolean isSpokenLanguageChanged() {
		return spokenLanguageChanged;
	}

	public void setSpokenLanguageChanged(boolean spokenLanguageChanged) {
		this.spokenLanguageChanged = spokenLanguageChanged;
	}

	public boolean isSeekQHP() {
		return isSeekQHP;
	}

	public void setSeekQHP(boolean isSeekQHP) {
		this.isSeekQHP = isSeekQHP;
	}
	
}
