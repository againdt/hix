package com.getinsured.hix.indportal.dto.dm.application;

public class Links {
	 private String rel;
	 private String href;


	 // Getter Methods 

	 public String getRel() {
	  return rel;
	 }

	 public String getHref() {
	  return href;
	 }

	 // Setter Methods 

	 public void setRel(String rel) {
	  this.rel = rel;
	 }

	 public void setHref(String href) {
	  this.href = href;
	 }
	
}
