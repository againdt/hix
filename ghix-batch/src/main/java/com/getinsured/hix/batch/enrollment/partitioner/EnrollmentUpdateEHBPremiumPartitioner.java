package com.getinsured.hix.batch.enrollment.partitioner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.service.EnrollmentEHBUpdateService;
import com.getinsured.hix.batch.enrollment.skip.Enrollment1095Out;
import com.getinsured.hix.batch.enrollment.skip.EnrollmentEHBUpdation;
import com.getinsured.hix.model.batch.BatchJobExecution;

@Component("enrollmentUpdateEHBPremiumPartitioner")
@Scope("step")
public class EnrollmentUpdateEHBPremiumPartitioner implements Partitioner{
	@Value("#{stepExecution}")
	private StepExecution stepExecution;
	private EnrollmentEHBUpdateService enrollmentEHBUpdateService;
	private String enrollmentIDStr;
	private List<Integer> enrollmentIdList;
	private String coverageYear;
	private EnrollmentEHBUpdation enrollmentEHBUpdation;
	private  String ehbUpdateCommitInterval;
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentUpdateEHBPremiumPartitioner.class);
	
	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		
		List<BatchJobExecution> batchExecutionList = enrollmentEHBUpdateService.getRunningBatchList("updateEHBPremiumJob");
		
		if(batchExecutionList != null && batchExecutionList.size() == 1){
			Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();
			populateEnrollmentIdList(enrollmentIDStr);
			if(enrollmentIdList == null ||(enrollmentIdList != null && enrollmentIdList.isEmpty())){
				enrollmentIdList = enrollmentEHBUpdateService.getUniqueHealthEnrollmentIds(coverageYear);
			}
			if(enrollmentIdList != null && !enrollmentIdList.isEmpty()){
				enrollmentEHBUpdation.clearEnrollmentIdList();
				enrollmentEHBUpdation.clearEnrollmentIdList();
				enrollmentEHBUpdation.addAllToEnrollmentIdList(enrollmentIdList);
				
				int maxEnrollmentsPerPartitions =5000;
				int size = enrollmentEHBUpdation.getEnrollmentIdList().size();
				if(this.ehbUpdateCommitInterval !=null && !this.ehbUpdateCommitInterval.isEmpty()){
					maxEnrollmentsPerPartitions=Integer.valueOf(this.ehbUpdateCommitInterval.trim());
				}
				int numberOfPartitions = size / maxEnrollmentsPerPartitions;
				if (size % maxEnrollmentsPerPartitions != 0) {
					numberOfPartitions++;
				}
				int firstIndex = 0;
				int lastIndex = 0;
				for (int i = 0; i < numberOfPartitions; i++) {
					firstIndex = i * maxEnrollmentsPerPartitions;
					lastIndex = (i + 1) * maxEnrollmentsPerPartitions;
					if (lastIndex > size) {
						lastIndex = size;
					}

					ExecutionContext value = new ExecutionContext();
					value.putInt("startIndex", firstIndex);
					value.putInt("endIndex", lastIndex);
					value.putInt("partition",  i);
					partitionMap.put("partition - " + i, value);
					LOGGER.debug("EnrollmentAnnualIRSOutPartitioner : partition():: partition map entry end for partition :: "+i);

				}
			}
			return partitionMap;
		}
		return null;
	}
	
	
	
	/**
	 * 
	 * @param employerEnrollmentIdsStr
	 */
	private void populateEnrollmentIdList(String enrollmentIdsStr) {
		if(enrollmentIdsStr!=null && !enrollmentIdsStr.trim().equalsIgnoreCase("") && !enrollmentIdsStr.trim().equalsIgnoreCase("null")){
			if(enrollmentIdList == null){
				enrollmentIdList = new ArrayList<Integer>();
			}
			StringTokenizer stringTok = new StringTokenizer(enrollmentIdsStr,"|");

			while (stringTok.hasMoreElements()) {
				enrollmentIdList.add( Integer.parseInt((String)stringTok.nextElement()));
			}
		}
	}
	

	/**
	 * @return the enrollmentEHBUpdateService
	 */
	public EnrollmentEHBUpdateService getEnrollmentEHBUpdateService() {
		return enrollmentEHBUpdateService;
	}

	/**
	 * @param enrollmentEHBUpdateService the enrollmentEHBUpdateService to set
	 */
	public void setEnrollmentEHBUpdateService(EnrollmentEHBUpdateService enrollmentEHBUpdateService) {
		this.enrollmentEHBUpdateService = enrollmentEHBUpdateService;
	}

	/**
	 * @return the enrollmentIDStr
	 */
	public String getEnrollmentIDStr() {
		return enrollmentIDStr;
	}

	/**
	 * @param enrollmentIDStr the enrollmentIDStr to set
	 */
	public void setEnrollmentIDStr(String enrollmentIDStr) {
		this.enrollmentIDStr = enrollmentIDStr;
	}

	/**
	 * @return the enrollmentIdList
	 */
	public List<Integer> getEnrollmentIdList() {
		return enrollmentIdList;
	}

	/**
	 * @param enrollmentIdList the enrollmentIdList to set
	 */
	public void setEnrollmentIdList(List<Integer> enrollmentIdList) {
		this.enrollmentIdList = enrollmentIdList;
	}

	/**
	 * @return the coverageYear
	 */
	public String getCoverageYear() {
		return coverageYear;
	}

	/**
	 * @param coverageYear the coverageYear to set
	 */
	public void setCoverageYear(String coverageYear) {
		if ((null == coverageYear) || (coverageYear.equalsIgnoreCase("null")) || coverageYear.isEmpty()) {
			this.coverageYear = ""+Calendar.getInstance().get(Calendar.YEAR);
		} else {
			this.coverageYear = coverageYear;
		}
	}



	public EnrollmentEHBUpdation getEnrollmentEHBUpdation() {
		return enrollmentEHBUpdation;
	}



	public void setEnrollmentEHBUpdation(EnrollmentEHBUpdation enrollmentEHBUpdation) {
		this.enrollmentEHBUpdation = enrollmentEHBUpdation;
	}



	public String getEhbUpdateCommitInterval() {
		return ehbUpdateCommitInterval;
	}



	public void setEhbUpdateCommitInterval(String ehbUpdateCommitInterval) {
		this.ehbUpdateCommitInterval = ehbUpdateCommitInterval;
	}
	
	
}
