
package com.getinsured.ssap.model.financial.nmec;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "insurance",
    "householdContact",
    "organization",
    "dateOfBirth",
    "socialSecurityCard",
    "name",
    "gender"
})
public class NonEsiMecHouseholdMember implements Serializable
{

    @JsonProperty("insurance")
    private Insurance insurance;
    @JsonProperty("householdContact")
    private HouseholdContact householdContact;
    @JsonProperty("organization")
    private List<Organization> organization = new ArrayList<Organization>();
    @JsonProperty("dateOfBirth")
    private String dateOfBirth;
    @JsonProperty("socialSecurityCard")
    private SocialSecurityCard socialSecurityCard;
    @JsonProperty("name")
    private Name name;
    @JsonProperty("gender")
    private String gender;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 2917084103631273079L;

    @JsonProperty("insurance")
    public Insurance getInsurance() {
        return insurance;
    }

    @JsonProperty("insurance")
    public void setInsurance(Insurance insurance) {
        this.insurance = insurance;
    }

    @JsonProperty("householdContact")
    public HouseholdContact getHouseholdContact() {
        return householdContact;
    }

    @JsonProperty("householdContact")
    public void setHouseholdContact(HouseholdContact householdContact) {
        this.householdContact = householdContact;
    }

    @JsonProperty("organization")
    public List<Organization> getOrganization() {
        return organization;
    }

    @JsonProperty("organization")
    public void setOrganization(List<Organization> organization) {
        this.organization = organization;
    }

    @JsonProperty("dateOfBirth")
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    @JsonProperty("dateOfBirth")
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @JsonProperty("socialSecurityCard")
    public SocialSecurityCard getSocialSecurityCard() {
        return socialSecurityCard;
    }

    @JsonProperty("socialSecurityCard")
    public void setSocialSecurityCard(SocialSecurityCard socialSecurityCard) {
        this.socialSecurityCard = socialSecurityCard;
    }

    @JsonProperty("name")
    public Name getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(Name name) {
        this.name = name;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    @JsonProperty("gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("insurance", insurance).append("householdContact", householdContact).append("organization", organization).append("dateOfBirth", dateOfBirth).append("socialSecurityCard", socialSecurityCard).append("name", name).append("gender", gender).append("additionalProperties", additionalProperties).toString();
    }

}
