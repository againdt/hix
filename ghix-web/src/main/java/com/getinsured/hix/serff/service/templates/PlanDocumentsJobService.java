package com.getinsured.hix.serff.service.templates;

import java.util.Map;

import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanDocumentsJob;

/**
 * 
 * 
 * 
 * @author vardekar_s
 * 
 */
public interface PlanDocumentsJobService {
	
	/**
	 * This method saves Plan Document Job object i.e. details of document that is uploaded in ECM from FTP location.
	 * 
	 * @param documentJob
	 * @return
	 */
	public int persistPlanDocumentRecord(PlanDocumentsJob documentJob);

	/**
	 * 
	 * @param planId
	 * @return
	 */	
	public Long getPlanCount(@Param("planId") String planId);
	
	/**
	 * 
	 * @param planId
	 * @return
	 */	
	public Long getPlanHealthCount(@Param("planId") String planId);	
	
	/**
	 * 
	 * 
	 * @param planIdBrochureMap
	 */
	public void updatePlanDocuments(Map<String, String[]> planIdBrochureMap);	
		
}
