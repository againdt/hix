package com.getinsured.eligibility.at.server.endpoint;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.HttpServletConnection;

import com.getinsured.eligibility.at.dto.AccountTransferResponseDTO;
import com.getinsured.eligibility.at.ref.service.LceEditApplicationService;
import com.getinsured.eligibility.at.ref.service.migration.AccountTransferMigrationService;
import com.getinsured.eligibility.at.ref.service.migration.MigrationUtil;
import com.getinsured.eligibility.at.service.AccountTransferService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.redetermination.service.ApplicationCloneService;
import com.getinsured.eligibility.redetermination.service.ApplicationCloneServiceImpl;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.outboundat.OutboundATApplicantRepository;
import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferResponsePayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityStatusCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityStatusCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.ssap.model.OutboundATApplicant;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.TimeShifterUtil;

@Endpoint
public class AccountTransferEndpoint {
	private static final String INPUT_XML = "inputXml";

	private static final Logger LOGGER = LoggerFactory.getLogger(AccountTransferEndpoint.class);

	private static final String TRANSFER_ACCOUNT_ENDPOINT_INVOKED = "transferAccount endpoint invoked...";
	private static final String TARGET_NAMESPACE = "http://at.dsh.cms.gov/exchange/1.0";
	private static final String REQUEST_LOCAL_NAME = "AccountTransferRequest";

	@Autowired private AccountTransferService accountTransferService;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private SsapApplicantRepository ssapApplicantRepository;
	@Autowired private ApplicationCloneService applicationCloneService;
	@Autowired private AccountTransferMigrationService accountTransferDataMigrationService;
	@Autowired private MigrationUtil migrationUtil;
	@Autowired private GIWSPayloadService giwsPayloadService;
	@Autowired private OutboundATApplicantRepository outboundATApplicantRepository;
	@Autowired private CmrHouseholdRepository cmrHouseholdRepository;
	
	private static final String ENDPOINT_FUNCTION_NAME = "RESPONSE-AT";
	@Autowired private GIWSPayloadRepository giWsPayloadRepository;
	
	public static final List<String> nonCloeasblestatuses = Arrays.asList(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode(), 
			  ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode());
	
	@Autowired private LceEditApplicationService lceEditApplicationService;

	@PayloadRoot(localPart = REQUEST_LOCAL_NAME , namespace = TARGET_NAMESPACE)
	@ResponsePayload public AccountTransferResponsePayloadType transferAccount(@RequestPayload AccountTransferRequestPayloadType request)
	{
		LOGGER.info(TRANSFER_ACCOUNT_ENDPOINT_INVOKED);
		String userName = null;
		String offSet = null;
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			userName = this.getHttpHeaderValue("X-USER".intern());
			offSet = this.getHttpHeaderValue("X-USER-OFFSET".intern());
			LOGGER.warn("Receiving user offset for AT request has been deprecated nt using the provided offset {}, weill fetch directly if available", offSet);
			if(userName != null) {
				long os = TimeShifterUtil.setUserOffset(userName);
				SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
				LOGGER.info("Applied offset {} for user {} with effective date {}", os, userName, sdf.format(new Date(System.currentTimeMillis()+ os)));
				offSet = Long.toString(os);
			}
		}
		long time = TimeShifterUtil.currentTimeMillis();
		MessageContext messageContext = MessageContextHolder.getMessageContext();
		String inputXml = (String) messageContext.getProperty(INPUT_XML);
		
		// 1. Analyze ATRequest...
		AccountTransferResponseDTO result = accountTransferService.prepareATResponse(request, inputXml);
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			
			LOGGER.info("Regestering the offset {} for user {} with AT request", offSet, userName);
			result.setUser(userName);
			result.setTSoffset(offSet);
		}
		AccountTransferResponsePayloadType response = result.getAccountTransferResponsePayloadType();

		// 2. If ATRequest is correct, proceed with Full Response or Referral flow...
		try {
			String dataMigration =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_DATA_MIGRATION_ENABLE);
			String dataMigrationHeader = this.getHttpHeaderValue(MigrationUtil.DATA_MIGRATION.intern());
			
			LOGGER.info("DATA_MIGRATION FLAG FROM AHBX : "+dataMigrationHeader);
			// state NV and InformationExchangeSystemCategoryCode blank then it will be a response AT
			String stateCode = DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes = request.getInsuranceApplication().getApplicationIdentification();
			
			boolean isMedicaidResponse = checkMedicaidResponse(request.getInsuranceApplication());
			if(iTypes.size() > 0) {
				LOGGER.info("List size: "+iTypes.get(0));
			}
			com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType = iTypes.get(0);
			if(tIdentificationType != null) {
				LOGGER.info("tIdentificationType id text: "+tIdentificationType.getIdentificationCategoryText());
			}
			if(StringUtils.isNotEmpty(dataMigrationHeader) && MigrationUtil.ENABLE.equalsIgnoreCase(dataMigrationHeader)){
				String enrollmentList = this.getHttpHeaderValue(MigrationUtil.ENROLLMENT_LIST.intern());
				LOGGER.info("Data Migration Process : Starts : Enrollment List : "+enrollmentList);
				
				AccountTransferResponsePayloadType errors = migrationUtil.validate(request, enrollmentList);
				if(errors!=null){
					return errors;
				}
				
				accountTransferDataMigrationService.process(request, result, enrollmentList);
				LOGGER.info("Data Migration Process : Ends");
			} // Recognize that it is a AT response we need to check IdentificationCategoryText
			else if (stateCode.equalsIgnoreCase("NV") && isMedicaidResponse) {
				// process the response AT
				LOGGER.info("Processing Response AT : Starts");
				this.processResponseAT(request,tIdentificationType,inputXml);
			}
			else {
				LOGGER.info("Processing Transfer AT : Starts");
				callATProcess(request, result);
			}
		}catch(Exception ex) {
			response = accountTransferService.formFailureResponse(Arrays.asList("Failed to process request."),EligibilityConstants.BUSINESS_VALIDATION_ERROR_CODE);
			LOGGER.error("Error occured while processing AT Request, Payload Id - "+result.getGiWsPayloadId(),ex);
		}
		//log processing time
		if(result.getGiwsPayload() != null && result.getGiwsPayload().getId() != null) {
			accountTransferService.logProcessingTime(result.getGiwsPayload().getId(),(TimeShifterUtil.currentTimeMillis() - time));
		}
		// 3. Send back the response to HSD..
		return response;
	}

	private boolean checkMedicaidResponse(InsuranceApplicationType insuranceApplication) {
		/* 
		 * If InsuranceApplicationRequestingMedicaidIndicator is false and if none of the applicant has INITIATED as ReferralActivityStatusCode
		 */
		if(insuranceApplication.getInsuranceApplicationRequestingMedicaidIndicator() != null && !insuranceApplication.getInsuranceApplicationRequestingMedicaidIndicator().isValue()) {
			if(insuranceApplication.getInsuranceApplicant() != null && insuranceApplication.getInsuranceApplicant().size() >0) {
				for(InsuranceApplicantType insuranceApplicantType : insuranceApplication.getInsuranceApplicant()) {
					if(insuranceApplicantType.getReferralActivity() != null 
							&& insuranceApplicantType.getReferralActivity().getReferralActivityStatus()!= null
							&& insuranceApplicantType.getReferralActivity().getReferralActivityStatus().getReferralActivityStatusCode() !=null) {
						if(ReferralActivityStatusCodeSimpleType.INITIATED.equals(insuranceApplicantType.getReferralActivity().getReferralActivityStatus().getReferralActivityStatusCode().getValue())) {
							return false;
						}
					}
				}
			}
			return true;
		}
		return false;
		
	}

	/**
	 * This method is used to process the Response AT
	 * Sequence is 
	 * 1. CLONE_APP
	 * 2. ADD_SSAP_EVENTS
	 * 3. UPDATE_SEEKING_COVERAGE
	 * 4. UPDATE_SEEK_QHP
	 * 5. RUN_VERIFICATION_ELIGIBILITY API
	 * 6. RUN_AT_AUTOMATION
	 * @param accountTransferRequest
	 * @param tIdentificationType 
	 * @param inputXml 
	 */
	private void processResponseAT(AccountTransferRequestPayloadType accountTransferRequest, IdentificationType tIdentificationType, String inputXml) {
		GIWSPayload giPayload = null;
		try {
			// get the application hh case id and retrieve the application from it
			String hhCaseId = accountTransferRequest.getInsuranceApplication().getApplicationIdentification().get(0).getIdentificationID().getValue();
			SsapApplication ssapApplication =null;
			SsapApplication erApplication =null;
			List<SsapApplication> ssapApplications = ssapApplicationRepository.findByHHCaseId(hhCaseId);
			if(!CollectionUtils.isEmpty(ssapApplications)) {
				ssapApplication = getApplicationToClone(ssapApplications);
				erApplication = getERApplication(ssapApplications);
				if(ssapApplication.getId() == erApplication.getId())
				{
					erApplication = null;
				}
			}
			if(ssapApplication != null) {
				
				// persist to gi_ws_payload // endpoint function - response AT
				HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
				String requestUrl = readRequestUrl(request);
				LOGGER.info("Gi ws payload insert for request url: "+requestUrl +" and ssap id: "+ssapApplication.getId());
				giPayload= accountTransferService.logServicePayload(null, inputXml, requestUrl, ENDPOINT_FUNCTION_NAME,null, "PENDING", "processResponseAT");
				// success and failure persist to gi_ws_payload
				//giPayloads = giwsPayloadService.findByEndpointFunctionAndSSAPID(ENDPOINT_FUNCTION_NAME, ssapApplication.getId());
				if(giPayload != null) {
					// out bound call 
					LOGGER.info("Making outboundcall for giwspayload id : "+giPayload.getId());
					updateOutboundAtApplicant(accountTransferRequest, giPayload.getId(),ssapApplication);						
				}				
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("CLONED started for application with hhCaseId: "+hhCaseId);	
				}
				// we clone only ssap application and ssap applicants and not the events hence passing false
				Map<String, String> responseData = applicationCloneService.cloneSsap(ssapApplication,false, new AtomicBoolean(false), true);
				String clonedAppId = responseData.get(ApplicationCloneServiceImpl.RENEWED_APPLICATION_ID);
				// update the entry in gi_Ws_payload
				giWsPayloadRepository.updateSsapApplicationId(Long.valueOf(clonedAppId), giPayload.getId());
				
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("CLONED done for application new ssap app id and entry made in gi_ws_payload "+clonedAppId);
				}
				// close the previous application
				LOGGER.info("Updating existing application status STARTED");
				if(!nonCloeasblestatuses.contains(ssapApplication.getApplicationStatus()))
				{
					if(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus())){
						if(ssapApplication.getApplicationDentalStatus() != null && ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationDentalStatus())){
							//do nothing
						} else {
							lceEditApplicationService.closeApplications(ssapApplication);		
						}
					} else {
						lceEditApplicationService.closeApplications(ssapApplication);
					}					
					LOGGER.info("Updating existing application status is closed now.");
				}
				if(erApplication!= null)
				{
					if(erApplication.getApplicationDentalStatus() == null || !ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(erApplication.getApplicationDentalStatus())){
						lceEditApplicationService.closeApplications(erApplication);		
					}
				}
				
				LOGGER.info("Updating existing application status ENDED");
				
				// now create events for the new application depending on the ReferralActivityStatusCode 
			
				List<InsuranceApplicantType> insuranceApplicantList = accountTransferRequest.getInsuranceApplication()
						.getInsuranceApplicant();
			
				Map<String, Boolean> seekingCoverageaAndQHPMap = new HashMap<String, Boolean>();
				Map<String, String> applicantEventsMap = new HashMap<String, String>();
				for (InsuranceApplicantType insuranceApplicant : insuranceApplicantList) {

					ReferralActivityStatusCodeType referralActivityStatus = insuranceApplicant.getReferralActivity()
							.getReferralActivityStatus().getReferralActivityStatusCode();

					if (insuranceApplicant.getRoleOfPersonReference().getRef() instanceof PersonType) {	
						PersonType person = (PersonType) insuranceApplicant.getRoleOfPersonReference().getRef();
						if (referralActivityStatus != null) {
							if (ReferralActivityStatusCodeSimpleType.ACCEPTED
									.equals(referralActivityStatus.getValue())) {
								// 1. set the seeking coverage indicator for ssap applicant as false
								//ssapApplicant.setApplyingForCoverage("N");
								// 2. set seek_qhp as false in application_data of ssap_Application we need to update the seeksQhp flag for applicant 
								// in taxHousehold -> householdMember with the help of Person Id i.e applicant guid in the database
								// 3. Add event as GainofMedicaid with start date = current date and end date would be start+60
								// use : createApplication_Applicant_SepEvents this method SsapApplicationEventServiceImpl
								if(person.getId().indexOf("P") > 0) {
									// then applicantGuid+P+personId is the key so put it in map as it is 
									seekingCoverageaAndQHPMap.put(person.getId(),false);
									applicantEventsMap.put(person.getId(), "GAIN_OF_MEDICAID");	
								} else {
									// the P+personId so we need to convert that into applicantGuid+P+personId as the key
									String applicantGuidXml = insuranceApplicant.getReferralActivity().getActivityIdentification().getIdentificationID().getValue();
									seekingCoverageaAndQHPMap.put(applicantGuidXml+person.getId(),false);
									applicantEventsMap.put(applicantGuidXml+person.getId(), "GAIN_OF_MEDICAID");
								}								
								
							} else if (ReferralActivityStatusCodeSimpleType.REJECTED
									.equals(referralActivityStatus.getValue())) {
								// 1. set the seeking coverage indicator for ssap applicant as true
								//ssapApplicant.setApplyingForCoverage("Y");
								// 2. set seek_qhp as true in application_data of ssap_Application we need to update the seeksQhp flag for applicant 
								// in taxHousehold -> householdMember with the help of Person Id i.e applicant guid in the database
								// 3. Add event as Other with start date = current date and end date would be start+60
								// use : createApplication_Applicant_SepEvents this method SsapApplicationEventServiceImpl
								if(person.getId().indexOf("P") > 0) {
									// then applicantGuid+P+personId is the key so put it in map as it is 
									seekingCoverageaAndQHPMap.put(person.getId(),true);
									applicantEventsMap.put(person.getId(), "OTHER");
								} else {
									// the P+personId so we need to convert that into applicantGuid+P+personId as the key
									String applicantGuidXml = insuranceApplicant.getReferralActivity().getActivityIdentification().getIdentificationID().getValue();
									seekingCoverageaAndQHPMap.put(applicantGuidXml+person.getId(),true);
									applicantEventsMap.put(applicantGuidXml+person.getId(), "OTHER");
								}
								
							}
						}
					}
				}
				
				// update the values in the database for the response AT
				accountTransferService.processResponseAT(clonedAppId,seekingCoverageaAndQHPMap,applicantEventsMap);
				if(giPayload != null) {
					giWsPayloadRepository.updateStatus("", "SUCCESS",giPayload.getId());
				}
	
			}
		} catch (Exception e) {
			// update the gi_ws_payload 
			String exceptionTrace = ExceptionUtils.getFullStackTrace(e);
			if(giPayload != null) {
				giWsPayloadRepository.updateStatus(exceptionTrace, "FAILURE",giPayload.getId());	
			}			
			LOGGER.error("Error while processing response AT in account transfer endpoint: ", e);			
			
		}		
	}

	private SsapApplication getApplicationToClone(List<SsapApplication> ssapApplications) {
		SsapApplication enpnApplication = null;
		SsapApplication erApplication = null;
		for(SsapApplication application : ssapApplications)
		{
			if(nonCloeasblestatuses.contains(application.getApplicationStatus()))
			{
				enpnApplication =  application;
			}
			else if(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equalsIgnoreCase(application.getApplicationStatus())){
				if(application.getApplicationDentalStatus() != null && ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(application.getApplicationDentalStatus())){
					enpnApplication = application;
				}
				else
				{
					erApplication = application;
				}
			}
		}
		if(enpnApplication != null)
		{
			return enpnApplication;
		}
		else if(erApplication != null)
		{
			return erApplication;
		}
		else
		{
			return ssapApplications.get(0);
		}
	}

	private SsapApplication getERApplication(List<SsapApplication> ssapApplications) {
		SsapApplication erApplication = null;
		for(SsapApplication application : ssapApplications)
		{
			if(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equalsIgnoreCase(application.getApplicationStatus())){
				erApplication = application;
			}
		}
		return erApplication;
	}
	/**
	 * Out bound Applicant entry is made with respect to Referral Activity Status code 
	 * @param accountTransferRequest
	 * @param giwsPayloadId
	 */
	private void updateOutboundAtApplicant(AccountTransferRequestPayloadType accountTransferRequest, Integer giwsPayloadId, SsapApplication ssapApplication){
		List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes = accountTransferRequest.getInsuranceApplication().getApplicationIdentification();
		List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplication(ssapApplication);
		for(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType : iTypes) {
			if( tIdentificationType.getIdentificationID() != null && tIdentificationType.getIdentificationCategoryText() == null ){

				List<InsuranceApplicantType> InsuranceApplicantList = accountTransferRequest.getInsuranceApplication().getInsuranceApplicant();
				for (InsuranceApplicantType insuranceApplicant : InsuranceApplicantList) {
					ReferralActivityStatusCodeType referralActivityStatus = insuranceApplicant.getReferralActivity().getReferralActivityStatus().getReferralActivityStatusCode();
					//FFEVerificationCodeType referralActivityOverallVerificationStatusCode = insuranceApplicant.getReferralActivity().getReferralActivityStatus().getReferralActivityOverallVerificationStatusCode();
					
					if (insuranceApplicant.getRoleOfPersonReference().getRef() instanceof PersonType) {
						PersonType personType = (PersonType) insuranceApplicant.getRoleOfPersonReference().getRef();
						String personId=personType.getId();
						LOGGER.info("personId : "+personId);
						if(referralActivityStatus != null){
							String action ="";
							if(ReferralActivityStatusCodeSimpleType.ACCEPTED.equals(referralActivityStatus.getValue())){
								action = ReferralActivityStatusCodeSimpleType.ACCEPTED.value();
								
								com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean seekingCoverageIndicator = AccountTransferUtil.basicFactory.createBoolean();
								seekingCoverageIndicator.setValue(false);
								personType.setPersonSeekingCoverageIndicator(seekingCoverageIndicator);
							}else if(ReferralActivityStatusCodeSimpleType.REJECTED.equals(referralActivityStatus.getValue())){
								action = ReferralActivityStatusCodeSimpleType.REJECTED.value();
							}							
							if(action.equals(ReferralActivityStatusCodeSimpleType.ACCEPTED.value()) || action.equals(ReferralActivityStatusCodeSimpleType.REJECTED.value())) {
								String applicantGuid = null;
								if (personId.indexOf("P") > 0)
								{
									applicantGuid = personId.substring(0, personId.indexOf("P"));
								}
								else
								{
									for(SsapApplicant ssapApplicant: ssapApplicants) {									
										if(personId.equalsIgnoreCase("P"+ssapApplicant.getPersonId())) {
											applicantGuid = ssapApplicant.getApplicantGuid();
										}
									}
								}
								LOGGER.info("personId : "+personId+" matched with applicantGuid : "+applicantGuid);
								if(applicantGuid != null) {
									List<OutboundATApplicant> OutboundATApplicants = outboundATApplicantRepository.findByApplicantGuid(applicantGuid);//check size and then loop through
									LOGGER.info("OutboundATApplicants size: "+OutboundATApplicants.size());
									if(OutboundATApplicants.size() > 0){
										for (OutboundATApplicant outboundATApplicant : OutboundATApplicants) {
											outboundATApplicant.setAction(action);
											outboundATApplicant.setActionDate(new Date());
											outboundATApplicant.setInboundATPayload(giwsPayloadId);
											outboundATApplicantRepository.save(outboundATApplicant);
										}
									}
								}
							}	
						}
					}
				}				
				break;
			}
		}
	}
	
	private String readRequestUrl(HttpServletRequest request) {
		return request != null ? 
					request.getRequestURL() != null ? request.getRequestURL().toString() : StringUtils.EMPTY 
							: StringUtils.EMPTY;
	}
	
	private void callATProcess(AccountTransferRequestPayloadType request, AccountTransferResponseDTO result) {
		GIWSPayload giwsPayload = giwsPayloadService.save(result.getGiwsPayload());
		result.setGiWsPayloadId(giwsPayload.getId());
		if(this.canAtBeProcessedNow(request, result)){
			LOGGER.info("Account Transfer Process : Starts ");
			
			accountTransferService.process(request, result);	
		}
	}

	private boolean canAtBeProcessedNow(AccountTransferRequestPayloadType request, AccountTransferResponseDTO result) {
		boolean canAtBeProcessedNow = true;
		String responseCode	=	result.getAccountTransferResponsePayloadType().getResponseMetadata().getResponseCode().getValue();
		// Log data into at_span_info table before starting ASYNCH processing of AT.
		String mutipleEligibiltySpanEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_MULTIPLE_ELIGIBILITY_SPAN_CONFIG);
		if(mutipleEligibiltySpanEnabled.equalsIgnoreCase("true") && 
				(StringUtils.equalsIgnoreCase(responseCode, EligibilityConstants.HS000000) || StringUtils.equalsIgnoreCase(responseCode, EligibilityConstants.SUCCESS_WITH_WARNING_CODE))){
			canAtBeProcessedNow = accountTransferService.createSpan(request,result);	
			LOGGER.info("Created span in at_span_info id: "+result.getAtSpanInfoId());
		}
		return canAtBeProcessedNow;
		
	}
	
	private HttpServletRequest getHttpServletRequest() {
	    TransportContext ctx = TransportContextHolder.getTransportContext();
	    return ( null != ctx ) ? ((HttpServletConnection ) ctx.getConnection()).getHttpServletRequest() : null;
	}

	private String getHttpHeaderValue( final String headerName ) {
	    HttpServletRequest httpServletRequest = getHttpServletRequest();
	    return ( null != httpServletRequest ) ? httpServletRequest.getHeader( headerName ) : null;
	}

}
