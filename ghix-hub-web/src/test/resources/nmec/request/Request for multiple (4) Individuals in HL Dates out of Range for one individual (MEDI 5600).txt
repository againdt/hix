{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload": {
    "taxHousehold": [
      {
        "householdMember": [
          {
            "insurance": {
              "insurancePolicyExpirationDate": "12/31/2014",
              "insurancePolicyEffectiveDate": "01/25/2014"
            },
            "householdContact": {
              "homeAddress": {
                "state": "AZ"
              }
            },
            "organization": [
              {
                "organizationCode": "MEDI"
              }
            ],
            "dateOfBirth": "01/25/1976",
            "socialSecurityCard": {
              "socialSecurityNumber": "222222303"
            },
            "name": {
              "middleName": "C",
              "lastName": "Long",
              "firstName": "Amy",
              "personNameSuffixText": ""
            },
            "gender": "F"
          },
          {
            "insurance": {
              "insurancePolicyExpirationDate": "03/31/2014",
              "insurancePolicyEffectiveDate": "01/25/2014"
            },
            "householdContact": {
              "homeAddress": {
                "state": "MD"
              }
            },
            "organization": [
              {
                "organizationCode": "MEDI"
              }
            ],
            "dateOfBirth": "01/25/1985",
            "socialSecurityCard": {
              "socialSecurityNumber": "222222310"
            },
            "name": {
              "middleName": "Ch",
              "lastName": "Young",
              "firstName": "Bill",
              "personNameSuffixText": ""
            },
            "gender": "M"
          }
        ]
      }
    ]
  }
}