package com.getinsured.hix.batch.enrollment.partitioner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsReportService;
import com.getinsured.hix.batch.enrollment.skip.Enrollment1095StagingParams;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.util.exception.GIException;

@Component("enrollmentAnnualIRSOutPartitioner")
@Scope("step")
public class EnrollmentAnnualIRSOutPartitioner implements Partitioner {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAnnualIRSOutPartitioner.class);

	private String strYear;
	List<Integer> applicableYear;
	@Value("#{stepExecution}")
	private StepExecution stepExecution;
	private String irsOutboundCommitInterval;

	private EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService;
	private BatchJobExecutionService batchJobExecutionService;

	private Enrollment1095StagingParams enrollment1095StagingParams;

	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		List<BatchJobExecution> batchExecutionList = enrollmentAnnualIrsReportService.getRunningBatchList("populate1095StagingJob");
		
		if(batchExecutionList != null && batchExecutionList.size() == 1){
			Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();
			ExecutionContext executionContext = null;
			try{
				setApplicableMonthYear();
				if(stepExecution!=null){
					executionContext=stepExecution.getJobExecution().getExecutionContext();
					if(executionContext!=null){
						executionContext.put("year", applicableYear);
					}
				}
				// Check to see if fresh run for year or a re-run
				Date lastJobRunDate = null;
				List<Integer> enrollmentIdListDb = new ArrayList<>();
				lastJobRunDate = batchJobExecutionService.getLastJobRunDate("populate1095StagingJob", "COMPLETED");
				for(int year:applicableYear){
					Boolean isFreshRun = isFreshRunForYear(year);
				if(isFreshRun){
						enrollmentIdListDb.addAll(enrollmentAnnualIrsReportService.getUniqueEnrollmentIds(year)); // Get effectuated enrollments for the year	
				}else{
						enrollmentIdListDb.addAll(enrollmentAnnualIrsReportService.getEnrollmentIdsUpdatedAfterDate(lastJobRunDate, year));
					}
				}
				
				if(enrollmentIdListDb!=null && !enrollmentIdListDb.isEmpty()){
					enrollment1095StagingParams.resetCounters();
					enrollment1095StagingParams.clearEnrollmentIdList();
					enrollment1095StagingParams.addAllToEnrollmentIdList(enrollmentIdListDb);
					int maxEnrollmentsPerFile = 1;
					int size = enrollmentIdListDb.size();
					if(this.irsOutboundCommitInterval !=null && !this.irsOutboundCommitInterval.isEmpty()){
						maxEnrollmentsPerFile=Integer.valueOf(this.irsOutboundCommitInterval.trim());
					}
					int numberOfFiles = size / maxEnrollmentsPerFile;
					if (size % maxEnrollmentsPerFile != 0) {
						numberOfFiles++;
					}
					int firstIndex = 0;
					int lastIndex = 0;
					for (int i = 0; i < numberOfFiles; i++) {
						firstIndex = i * maxEnrollmentsPerFile;
						lastIndex = (i + 1) * maxEnrollmentsPerFile;
						if (lastIndex > size) {
							lastIndex = size;
						}

						ExecutionContext value = new ExecutionContext();
						value.putInt("startIndex", firstIndex);
						value.putInt("endIndex", lastIndex);
						value.putInt("partition",  i);
						partitionMap.put("partition - " + i, value);
						LOGGER.debug("EnrollmentAnnualIRSOutPartitioner : partition():: partition map entry end for partition :: "+i);

					}
				}


			}catch(Exception e){
				LOGGER.info("EnrollmentAnnualIRSOutPartitioner failed to execute : "+ e.getMessage());
				throw new RuntimeException("EnrollmentAnnualIRSOutPartitioner failed to execute : " + e.getMessage()) ;
			}
			return partitionMap;
		} else {
			LOGGER.info("Previous instance of populate1095StagingJob is still running");
			throw new RuntimeException("Previous instance of populate1095StagingJob is still running") ;
		}
	}

	private boolean isFreshRunForYear(int applicableYear) {
		// Call service to get count for enrollments in staging table, if count
		// greater than 0, then rerun
		return enrollmentAnnualIrsReportService.isFreshRunForYear(applicableYear);
	}

	private void setApplicableMonthYear() throws GIException {
		int intYear = 0;
		Calendar calObj = Calendar.getInstance();
		int currentYear = calObj.get(Calendar.YEAR);
		if (NumberUtils.isNumber(strYear)) {
			intYear = Integer.parseInt(strYear);
			if (intYear > 2014 && intYear <= currentYear) {
				applicableYear = Arrays.asList(intYear);
			}else if(intYear == -1){
				applicableYear = Arrays.asList((Calendar.getInstance().get(Calendar.YEAR)-1), (Calendar.getInstance().get(Calendar.YEAR)));
			} else {
				throw new GIException("Please provide year (yyyy) <= current year");
			}
		} else {
			throw new GIException("Please provide valid year(yyyy)");
		}

	}

	public String getStrYear() {
		return strYear;
	}

	public void setStrYear(String strYear) {
		if ((null == strYear) || (strYear.equalsIgnoreCase("null")) || strYear.isEmpty()) {
			this.strYear = "-1";
		} else {
			this.strYear = strYear;
		}
	}

	public String getIrsOutboundCommitInterval() {
		return irsOutboundCommitInterval;
	}

	public void setIrsOutboundCommitInterval(String irsOutboundCommitInterval) {
		this.irsOutboundCommitInterval = irsOutboundCommitInterval;
	}

	public EnrollmentAnnualIrsReportService getEnrollmentAnnualIrsReportService() {
		return enrollmentAnnualIrsReportService;
	}

	public void setEnrollmentAnnualIrsReportService(EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService) {
		this.enrollmentAnnualIrsReportService = enrollmentAnnualIrsReportService;
	}

	public BatchJobExecutionService getBatchJobExecutionService() {
		return batchJobExecutionService;
	}

	public void setBatchJobExecutionService(BatchJobExecutionService batchJobExecutionService) {
		this.batchJobExecutionService = batchJobExecutionService;
	}

	public Enrollment1095StagingParams getEnrollment1095StagingParams() {
		return enrollment1095StagingParams;
	}

	public void setEnrollment1095StagingParams(Enrollment1095StagingParams enrollment1095StagingParams) {
		this.enrollment1095StagingParams = enrollment1095StagingParams;
	}


}
