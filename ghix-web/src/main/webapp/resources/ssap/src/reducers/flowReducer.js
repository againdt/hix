
import { flowActionTypes } from '../constants/actionTypes';
import FLOW_TYPES from '../constants/flowTypes';
import CONFIG from '../.env';
import { initialState as data } from './dataReducer';

const getInitialFlow = () => {
    if(CONFIG.ENV.DEVELOPMENT) {
        if(data.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator) {
            return FLOW_TYPES.FINANCIAL_FLOW;
        }
    }
    return FLOW_TYPES.NON_FINANCIAL_FLOW_NON_SWITCHABLE;
};

const initialState = {
    flowType: getInitialFlow(),
    isFlowLoaded: !!CONFIG.ENV.DEVELOPMENT
};

export default (state = initialState, action) => {
    switch(action.type) {
    case flowActionTypes.SET_FLOW:
        return {
            flowType: action.flowType,
            isFlowLoaded: true
        };
    case flowActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};