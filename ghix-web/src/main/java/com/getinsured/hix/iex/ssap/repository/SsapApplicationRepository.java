package com.getinsured.hix.iex.ssap.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * HIX-56543
 * 
 * Create a repository to extract EN ssap apps for the household + coverage year
 * 
 * @author Nikhil Talreja
 * 
 * 
 */
@Repository
public interface SsapApplicationRepository  extends JpaRepository<SsapApplication, Long> {
	
	/**
	 * Gets the SSAP records for a household for a particular coverage year
	 * @param cmrHouseoldId
	 * @param coverageYear
	 * 
	 * @author Nikhil Talreja
	 * 
	 */
	@Query("from SsapApplication where cmrHouseoldId = :cmrHouseoldId and coverageYear = :coverageYear order by creationTimestamp desc")
	List<SsapApplication> findByCmrHouseoldIdAndCoverageYear(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear);
	
	@Query("from SsapApplication where cmrHouseoldId = :cmrHouseoldId order by creationTimestamp desc")
	List<SsapApplication> findLatestByCmrHouseoldId(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId);
	
	/**
	 * HIX-57778
	 * Gets the SSAP records for a household for a particular coverage year and status
	 * @param cmrHouseoldId
	 * @param coverageYear
	 * 
	 * @author Nikhil Talreja
	 * 
	 */
	SsapApplication findByCmrHouseoldIdAndCoverageYearAndApplicationStatus(BigDecimal cmrHouseoldId, long coverageYear,String applicationStatus);

	@Query("from SsapApplication where cmrHouseoldId = :cmrHouseoldId and applicationStatus = 'EN' and coverageYear = :coverageYear")
	public SsapApplication findEnrolledSsapApplicationForCoverageYear(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId,@Param("coverageYear")long coverageYear) ;
	
			
	@Query("Select application from SsapApplication as application where application.cmrHouseoldId = :cmrHouseoldId and application.applicationStatus in ( 'EN' , 'PN' ) and application.coverageYear = :coverageYear and application.creationTimestamp = ( select max(creationTimestamp ) FROM  SsapApplication  where  cmrHouseoldId = :cmrHouseoldId and  applicationStatus in ( 'EN' , 'PN' ) and  coverageYear = :coverageYear  ) ")
	public SsapApplication findEnPnSsapApplicationForCoverageYear(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear);
	

	@Query("select count(*) from SsapApplication sa where sa.cmrHouseoldId = :cmrHouseoldId and sa.applicationType = :applicationType and sa.applicationStatus in ('ER','SG','SU') and sa.coverageYear = :coverageYear")
	long countOfApplicationsByStatusAndCoverageYear(@Param("cmrHouseoldId")BigDecimal cmrHouseoldId,@Param("applicationType")String applicationType,@Param("coverageYear") long coverageYear);

	@Query("select count(*) from SsapApplication sa where sa.cmrHouseoldId = :cmrHouseoldId and sa.applicationStatus in ('ER','SG','SU','OP') and sa.coverageYear = :coverageYear")
	long countOfSsapApplicationsByStatusAndCoverageYear(@Param("cmrHouseoldId")BigDecimal cmrHouseoldId,@Param("coverageYear") long coverageYear);
	
	List<SsapApplication> findByCmrHouseoldIdAndCoverageYearAndApplicationStatusIn(BigDecimal cmrHouseoldId, long coverageYear, List<String> applicationStatus);

	List<SsapApplication> findByCmrHouseoldIdAndApplicationStatusIn(BigDecimal cmrHouseoldId, List<String> applicationStatus);

	/**
	 * Finds SSAP by given Case Number.
	 * @param caseNumber case number.
	 * @return {@link SsapApplication} or null.
	 */
	SsapApplication findByCaseNumber(final String caseNumber);
	
	@Query("Select id, applicationStatus from SsapApplication where cmrHouseoldId = :cmrHouseoldId and coverageYear = :coverageYear and applicationStatus in ( :statusList)")
	List<Object[]> findSsapByCmrAppStatusAndCoverageYr(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear, @Param("statusList") List<String> statusList);
	
	@Query("Select application from SsapApplication as application where application.cmrHouseoldId = :cmrHouseoldId and application.applicationStatus = 'ER' and application.coverageYear = :coverageYear and application.creationTimestamp > ( select max(creationTimestamp ) FROM  SsapApplication  where  cmrHouseoldId = :cmrHouseoldId and  applicationStatus in ( 'EN' , 'PN' ) and  coverageYear = :coverageYear  ) ")
	public  SsapApplication findErAfterLatestEnPnSsapApplicationForCoverageYear(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear);
	
	@Query("Select id from SsapApplication WHERE cmrHouseoldId = :cmrHouseoldId and coverageYear = :coverageYear AND applicationStatus ='ER' and applicationDentalStatus is null")
	public List<Long> getErAppWithNoDentalEnrl(@Param("cmrHouseoldId")BigDecimal cmrHouseoldId,@Param("coverageYear") long coverageYear);
	
	@Query("Select application from SsapApplication as application where application.cmrHouseoldId = :cmrHouseoldId and application.coverageYear = :coverageYear AND application.applicationStatus ='ER' and application.applicationDentalStatus = 'EN' ")
	public List<SsapApplication> getErAppWithOnlyDentalEnrl(@Param("cmrHouseoldId")BigDecimal cmrHouseoldId,@Param("coverageYear") long coverageYear);
		
}
