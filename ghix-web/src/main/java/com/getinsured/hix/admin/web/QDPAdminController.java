package com.getinsured.hix.admin.web;

import static com.getinsured.hix.planmgmt.service.IssuerService.BENEFITS_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.BROCHURE_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.PLAN_SUPPORT_DOC_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.RATES_FOLDER;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.hix.admin.util.EnrollmentAvailabilityEmailUtil;
import com.getinsured.hix.dto.broker.BrokerContactDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Network;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.EnrollmentAvail;
import com.getinsured.hix.model.Plan.PlanInsuranceType;
import com.getinsured.hix.model.Plan.PlanLevel;
import com.getinsured.hix.model.Plan.PlanStatus;
import com.getinsured.hix.model.PlanDentalBenefit;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.provider.service.ProviderNetworkService;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.planmgmt.util.BrokerNotificationEmail;
import com.getinsured.hix.planmgmt.util.PlanMgmtRestUtil;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.notify.Notification;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import au.com.bytecode.opencsv.CSVWriter;

@Controller
@DependsOn("dynamicPropertiesUtil")
public class QDPAdminController {
	private static final Logger LOGGER = LoggerFactory.getLogger(QDPAdminController.class);
	private static final String AMPERSAND_SIGN = "&";
	private static final String EQUALTO_SIGN = "=";
	private static final String PLAN = "Plan";
	private static final String NETWORK = "network";
	private static final String STATUS = "status";
	private static final String ISSUERNAME = "issuername";
	private static final String MARKET = "market";
	private static final String SORT_BY = "sortBy";
	private static final String PLAN_ID = "planId";
	private static final String IS_EDIT = "isEdit";
	private static final String TRUE_STRING = "true";
	private static final String FALSE_STRING = "false";
	private static final String ORDER = "order";
	private static final String PLAN_SMALL = "plan";
	private static final String COMMENTS = "comments";
	private static final String QDPMARKETLIST = "qdpmarketList";
	private static final String QDPLEVELLIST = "qdplevelList";
	private static final String PAGE_TITLE = "page_title";
	private static final String PLAN_CERTI_FILE_NAME = "planDentalacturialValueCertificate";
	private static final String PLAN_CERTI_ORIG_FILE_NAME = "acturialValueCertificateOrig";
	private static final String PLAN_NUMBER = "planNumber";
	private static final String PLAN_MARKET = "planMarket";
	private static final String PLAN_NAME = "planName";
	private static final String ISSUER_NAME = "issuerName";
	private static final String RATE_FILE = "rateFile";
	private static final String BENEFIT_FILE = "benefitFile";
	private static final String PAGE_SIZE = "pageSize";
	private static final String DATA = "data";
	private static final String SORT_ORDER = "sortOrder";
	private static final String DISPLAYPLANLEVEL = "displayPlanLevel";
	private static final String CURRENTPLANLEVEL = "currentPlanLevel";
	private static final String DISPLAYEDITBUTTON = "displayEditButton";
	private static final String DISPLAYADDQDPBUTTON = "displayAddNewQDPButton";
	private static final String ISSUER_VERIFICATION = "issuerVerificationStatus";
	private static final String MANAGE_QDP_PATH = "redirect:/admin/planmgmt/manageqdpplans";
//	@Autowired private IssuerPlanCertificationEmail issuerPlanCertificationEmail;
//	@Autowired private IssuerPlanDeCertificationEmail issuerPlanDeCertificationEmail;
	private static final String SUPPORT_PLAN_LEVEL = "supportPlanLevel";
	private static final String PLAN_LEVEL = "planLevel";
	private static final String DISPLAYPROVIDERNETWORK = "displayProviderNetwork";
	private static final String DISPLAYACTION = "displayAction";
	private static final String VERIFIED = "verified";
	private static final String EXCHANGE_TYPE = "exType";
	private static final String PLANMGMT_DENTALBENEFITS = "planManagement.DentalBenefits.Benefit.";
	private static final String PLANMGMT_DUPLICATE_KEY = "2";
	private static final String PLAN_DIS_DATE = "planDisplayDate";
	private static final String CERTIFIED = "CERTIFIED";

	@Autowired private PlanMgmtService planMgmtService;
	@Autowired private IssuerService issuerService;
	@Autowired private UserService userService;
	@Autowired private ProviderNetworkService providerNetworkService;
//	@Autowired private IIssuerRepresentativeRepository representativeRepository;
	@Autowired private EnrollmentAvailabilityEmailUtil enrollmentAvailabilityEmailUtil;
	@Autowired private GIMonitorService giMonitorService;
	@Autowired private BrokerNotificationEmail brokerNotificationEmail;
	@Autowired private RestTemplate restTemplate;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private PlanMgmtUtil planMgmtUtils;
	@Autowired private PlanMgmtRestUtil planMgmtRestUtil;
	@Autowired private Gson platformGson;
	
	@Value("#{configProp.uploadPath}") private String uploadPath;

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/admin/planmgmt/manageqdpplans", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String listQDPPlans(@ModelAttribute(PLAN) Plan plan, Model model, HttpServletRequest request) {
		Integer issuerId = 0;
		
		try {
			List<Map<String, Integer>> planYearsList = planMgmtService.getPlanYears(PlanMgmtConstants.DENTAL, issuerId);
		List<Issuer> issuers = issuerService.getAllIssuerNamesByInsuranceType(PlanInsuranceType.DENTAL.toString());
		model.addAttribute("qdpcertiStatusList", plan.getPlanStatuses());
		model.addAttribute("marketList", plan.getMarketList());
		model.addAttribute("levelList", plan.getLevelList());
		model.addAttribute("verifiedList", plan.getVerifiedList());
		model.addAttribute("issuers", issuers);
		model.addAttribute("enrollmentAvailList", plan.getEnrollmentAvailList());
		Integer pageSize = GhixConstants.PAGE_SIZE;
		String reqURI = "?";		
		
		Map<PlanLevel, String> planLevels = plan.getLevelList();
		int showPendingPlanCtr = 1;
		if (request.getParameter(STATUS) != null) {
			reqURI += AMPERSAND_SIGN + STATUS + EQUALTO_SIGN
					+ request.getParameter(STATUS);
			request.setAttribute(STATUS, request.getParameter(STATUS));
			showPendingPlanCtr = 0;
		}
		if (!StringUtils.isEmpty(request.getParameter(PlanMgmtConstants.ENROLLMENTAVAILABILITY))) {
			request.setAttribute(PlanMgmtConstants.ENROLLMENTAVAILABILITY, request.getParameter(PlanMgmtConstants.ENROLLMENTAVAILABILITY));
		}
//		if (request.getParameter(ISSUERNAME) != null) {
//			if(!request.getParameter(PlanMgmtConstants.ISSUERNAME).isEmpty()){
//				String[] issuerDtls = request.getParameter(ISSUERNAME).split("_");
//				issuerName = issuerDtls[1]; 
//				reqURI += AMPERSAND_SIGN + ISSUERNAME + EQUALTO_SIGN + issuerName;
//				request.setAttribute(ISSUERNAME, issuerName);
//			}
//		}
		if (request.getParameter(PlanMgmtConstants.ISSUERID) != null) {
			reqURI += AMPERSAND_SIGN + PlanMgmtConstants.ISSUERID + EQUALTO_SIGN + request.getParameter(PlanMgmtConstants.ISSUERID);
			request.setAttribute(PlanMgmtConstants.ISSUERID, request.getParameter(PlanMgmtConstants.ISSUERID));
		}
		// FFM Changes for plan num
		if (request.getParameter(PLAN_NUMBER) != null) {
			reqURI += AMPERSAND_SIGN + PLAN_NUMBER + EQUALTO_SIGN
					+ request.getParameter(PLAN_NUMBER);
			request.setAttribute(PLAN_NUMBER, request.getParameter(PLAN_NUMBER));
		}

		if (request.getParameter(MARKET) != null
				&& !request.getParameter(MARKET).equals(PlanMgmtConstants.EMPTY_STRING)) {
			request.setAttribute(MARKET, request.getParameter(MARKET));
		}
		if (request.getParameter(VERIFIED) != null
				&& !request.getParameter(VERIFIED).equals(PlanMgmtConstants.EMPTY_STRING)) {
			request.setAttribute(VERIFIED, request.getParameter(VERIFIED));
		}
		if (DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE).equals(GhixConstants.PHIX) && request.getParameter(EXCHANGE_TYPE) != null) {
			
				request.setAttribute(EXCHANGE_TYPE, request.getParameter(EXCHANGE_TYPE));
			
		}
		if (request.getParameter(SORT_BY) != null) {
			reqURI += AMPERSAND_SIGN + SORT_BY + EQUALTO_SIGN
					+ request.getParameter(SORT_BY);
			request.setAttribute(SORT_BY, request.getParameter(SORT_BY));
		}
		
		if (request.getSession().getAttribute(PlanMgmtConstants.SORT_BY) != null) {
			request.setAttribute(PlanMgmtConstants.SORT_BY, request.getSession().getAttribute(PlanMgmtConstants.SORT_BY));
		}
		if (request.getParameter(ORDER) != null) {
			reqURI += AMPERSAND_SIGN + ORDER + EQUALTO_SIGN
					+ request.getParameter(ORDER);
		}

		// Parsing all plan levels
		for (String planLevel : planLevels.values()) {
			if (request.getParameter(PLAN_LEVEL) != null
					&& !request.getParameter(PLAN_LEVEL).isEmpty() && request.getParameter(PLAN_LEVEL).equalsIgnoreCase(planLevel)) {
					// prepare search parameter for plan level
					request.setAttribute(PLAN_LEVEL, request.getParameter(PLAN_LEVEL));
				
			}
		}

		request.setAttribute("pageSize", pageSize);

		String param = request.getParameter("pageNumber");
		Integer startRecord = 0;
		int currentPage = 1;
		if (param != null) {
			startRecord = (Integer.parseInt(param) - 1) * pageSize;
			currentPage = Integer.parseInt(param);
		}
		request.setAttribute("startRecord", startRecord);

			if (request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR) != null && !request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR).equals(PlanMgmtConstants.EMPTY_STRING)) {
				Map<String, Integer> selectedYearMap = new HashMap<String, Integer>();
				selectedYearMap.put(PlanMgmtConstants.PLAN_YEARS_KEY, Integer.parseInt(request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR).toString()));
				model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, selectedYearMap);
				LOGGER.debug("Fetching Plans for Year : " +  SecurityUtil.sanitizeForLogging(selectedYearMap.toString()));
		 	 	request.setAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, (request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR).toString()));
			}
			else {
				if (planYearsList.size() > PlanMgmtConstants.ZERO) {
					if(request.getSession().getAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK) != null){
						model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, Integer.parseInt(request.getSession().getAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK).toString()));
						LOGGER.debug("Fetching Plans for Year : " +  SecurityUtil.sanitizeForLogging(request.getSession().getAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK).toString()));
						request.setAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, request.getSession().getAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK).toString());
						Map<String, Integer> selectedYearMap = new HashMap<String, Integer>();
						selectedYearMap.put(PlanMgmtConstants.PLAN_YEARS_KEY, Integer.parseInt(request.getSession().getAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK).toString()));
						model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, selectedYearMap);
						LOGGER.debug("plan Year from Bulk Update : " +  SecurityUtil.sanitizeForLogging(selectedYearMap.toString()));
						model.addAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, request.getSession().getAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK).toString());
						request.getSession().removeAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK);
					}else{
						model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, planYearsList.get(0));
						LOGGER.debug("Fetching Plans for Year : " +  SecurityUtil.sanitizeForLogging(planYearsList.get(0).toString()));
						request.setAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, (planYearsList.get(0).get(PlanMgmtConstants.PLAN_YEARS_KEY)));
					}
				}
				else {
		 	 	 	return "admin/planmgmt/manageqdpplans";
		 	 	}
			}

			String sortOrder = (StringUtils.isEmpty(request.getParameter(PlanMgmtConstants.SORT_ORDER))) ? PlanMgmtConstants.SORT_ORDER_DESC : ((request.getParameter(PlanMgmtConstants.SORT_ORDER).equalsIgnoreCase(PlanMgmtConstants.SORT_ORDER_ASC) ? PlanMgmtConstants.SORT_ORDER_ASC : PlanMgmtConstants.SORT_ORDER_DESC));
			
			String changeOrder = (StringUtils.isEmpty(request.getParameter(PlanMgmtConstants.CHANGE_ORDER))) ? PlanMgmtConstants.FALSE_STRING : ((request.getParameter(PlanMgmtConstants.CHANGE_ORDER).equalsIgnoreCase(PlanMgmtConstants.FALSE_STRING) ? PlanMgmtConstants.FALSE_STRING : PlanMgmtConstants.TRUE_STRING));
			sortOrder = (changeOrder.equals(TRUE_STRING)) ? ((sortOrder.equals(PlanMgmtConstants.SORT_ORDER_DESC)) ? PlanMgmtConstants.SORT_ORDER_ASC : PlanMgmtConstants.SORT_ORDER_DESC) : sortOrder;
			
			request.removeAttribute(PlanMgmtConstants.CHANGE_ORDER);
			request.setAttribute(SORT_ORDER, sortOrder);
			String order = PlanMgmtConstants.SORT_ORDER_ASC;
		
			if(null == request.getParameter(PlanMgmtConstants.PAGE_NUMBER)){
	 	 	 	HttpSession session = request.getSession();
	 	 	 	if(session.getAttribute(PlanMgmtConstants.SELECTALL_QDP_PLANS) !=  null){
	 	 	 		session.removeAttribute(PlanMgmtConstants.SELECTALL_QDP_PLANS);
	 	 	 	}
			}
			
			List<Plan> plans = planMgmtService.getPlanList(request, PlanMgmtService.DENTAL);
			String globalExchangetype = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
			Map<Plan.PlanStatus, String> bulkStatusList = new HashMap<Plan.PlanStatus, String>();
			bulkStatusList.put(Plan.PlanStatus.CERTIFIED, "Certified");
			bulkStatusList.put(Plan.PlanStatus.INCOMPLETE, "Incomplete");
	
			model.addAttribute(SORT_ORDER, sortOrder);
			model.addAttribute(PlanMgmtConstants.SORT_ORDER_EXCEPTFIRSTCOLUMN, sortOrder);
			model.addAttribute("plans", plans);
			model.addAttribute(STATUS, request.getParameter(STATUS));		
			model.addAttribute(PlanMgmtConstants.ISSUERID,  request.getParameter(PlanMgmtConstants.ISSUERID));
			model.addAttribute(PLAN_NUMBER, request.getParameter(PLAN_NUMBER));
			model.addAttribute("currentPage", currentPage);
			model.addAttribute("order", order);
			model.addAttribute("reqURI", reqURI);
			model.addAttribute(SORT_BY, request.getParameter(SORT_BY));
			model.addAttribute(MARKET, request.getParameter(MARKET));
			model.addAttribute("selectedPlanLevel", request.getParameter(PLAN_LEVEL));
			model.addAttribute(PlanMgmtConstants.PLAN_YEARS_LIST, planYearsList);
			model.addAttribute("issuerVerificationList", plan.getIssuerVerificationList());
			model.addAttribute("enrollmentAvailList", plan.getEnrollmentAvailList());
			model.addAttribute("bulkStatusList", bulkStatusList);
			model.addAttribute(PlanMgmtConstants.CURR_DATE, DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT));
			model.addAttribute(PlanMgmtConstants.TIMEZONE,TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));
			
			if (globalExchangetype.equals(GhixConstants.PHIX)) {
				model.addAttribute(EXCHANGE_TYPE, request.getParameter(EXCHANGE_TYPE));
				model.addAttribute("exTypeList", plan.getExchangeTypeList());
			}
			String displayAddNewQDPButton = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_MANAGEQDP_DISPLAYADDNEWQDPBUTTON);
			model.addAttribute(DISPLAYADDQDPBUTTON, displayAddNewQDPButton);
			
			String supportPlanLevel = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QDP_SUPPORTPLANLEVEL);
			model.addAttribute(SUPPORT_PLAN_LEVEL, supportPlanLevel);
			model.addAttribute("exchangeType", globalExchangetype);
			model.addAttribute("sysDate", new TSDate());
			
			return "admin/planmgmt/manageqdpplans";
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.MANAGE_QDP.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}

	}

	@RequestMapping(value = "/admin/planmgmt/submitqdp", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQDP(@ModelAttribute(PLAN) Plan planmodel, Model model, HttpServletResponse response) {
		try {
		String planId = (String) model.asMap().get(PLAN_ID);
		if (planId != null) {
			Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
			model.addAttribute(PLAN_SMALL, plan);
			model.addAttribute(PLAN_CERTI_FILE_NAME, plan.getPlanDental().getActurialValueCertificate());
			model.addAttribute(PLAN_CERTI_ORIG_FILE_NAME, planMgmtService.getActualFileName(plan.getPlanDental().getActurialValueCertificate()));
			model.addAttribute(CURRENTPLANLEVEL, plan.getPlanDental().getPlanLevel());
		}
		model.addAttribute(QDPMARKETLIST, planmodel.getMarketList());
		// Defect HIX-4918 Code Modified
		// -Function called to fetch config Data getConfigPlanLevelList
		String displayPlanLevel = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QDP_SUPPORTPLANLEVEL);
		model.addAttribute(DISPLAYPLANLEVEL, displayPlanLevel);
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_SUBMIT_QDP.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/planmgmt/submitqdp";
	}

	@RequestMapping(value = "/admin/planmgmt/submitqdp", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN_DETAILS)
	public String submitQDPPersist(@ModelAttribute(PLAN) Plan plan, BindingResult result, Model model, RedirectAttributes redirectAttrs, @RequestParam(value = "planLevelReadOnly", required = false) String planLevelReadOnly) {
		try {
		plan.setStatus(PlanStatus.INCOMPLETE.toString());
		plan.setInsuranceType(PlanMgmtService.DENTAL);
		plan.setEnrollmentAvail(EnrollmentAvail.NOTAVAILABLE.toString());
		plan.setEnrollmentAvailEffDate(new TSDate());
		plan.getPlanDental().setBenefitEffectiveDate(plan.getStartDate());
		plan.getPlanDental().setRateEffectiveDate(plan.getStartDate());
		// set default service area id, when upload QDP from UI
		// plan.setServiceAreaId(issuerService.getDefaultServieAreaId("CAALLSTATE"));
		Calendar today = TSCalendar.getInstance(); // This gets the current date and time.
		Integer applicableYear = today.get(Calendar.YEAR); // This returns the year as an int.
		plan.setServiceAreaId(issuerService.getDefaultServieAreaId("CAALLSTATE", applicableYear));
		
		if (plan.getPlanDental().getPlanLevel() == null) {
			plan.getPlanDental().setPlanLevel(planLevelReadOnly);
		}
		Plan updatedPlan = null;
		Integer userId = null;
			userId = userService.getLoggedInUser().getId();
		plan.setLastUpdatedBy(userId);
		if (plan.getId() > 0) {
			updatedPlan = planMgmtService.updatePlan(plan);
			redirectAttrs.addFlashAttribute(IS_EDIT, TRUE_STRING);
		} else {
			plan.setCreatedBy(userId);
			plan.getPlanDental().setCreatedBy(userId);
			plan.getPlanDental().setLastUpdatedBy(userId);
			updatedPlan = planMgmtService.savePlan(plan);
		}

		redirectAttrs.addFlashAttribute(PLAN_ID, Integer.toString(updatedPlan.getId()));
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.POST_SUBMIT_QDP.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "redirect:/admin/planmgmt/submitqdpbenefits";

	}

	@RequestMapping(value = "/admin/planmgmt/submitqdpbenefits", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQDPBenefits(Model model, HttpServletResponse response) {
		try {
		String planId = (String) model.asMap().get(PLAN_ID);
		if (planId == null) {
			return "redirect:/admin/planmgmt/submitqdp";
		}
		String isEdit = (String) model.asMap().get(IS_EDIT);
		if (isEdit == null || !isEdit.equals(TRUE_STRING)) {
			model.addAttribute(IS_EDIT, FALSE_STRING);
		}
		Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
		model.addAttribute(ISSUER_NAME, plan.getIssuer().getName());
		model.addAttribute(PLAN_ID, planId);
		model.addAttribute(PLAN_NAME, plan.getName());
		model.addAttribute(PLAN_NUMBER, plan.getIssuerPlanNumber());
		return "admin/planmgmt/submitqdpbenefits";
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_EDITQDP_BENEFIT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	@RequestMapping(value = "/admin/planmgmt/submitqdpbenefits", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN_BENEFITS)
	public String submitQDPBenefitsUpload(@ModelAttribute(PLAN) Plan planModel, BindingResult result, @RequestParam(value = BENEFIT_FILE, required = false) String benefitsFile, @RequestParam(value = "brochureFile", required = false) String brochureFile, @RequestParam(value = IS_EDIT, required = false) String isEdit, RedirectAttributes redirectAttrs) {
		try {
		redirectAttrs.addFlashAttribute(IS_EDIT, isEdit);
		redirectAttrs.addFlashAttribute(PLAN_ID, Integer.toString(planModel.getId()));

		if ((benefitsFile != null && benefitsFile.length() > 0) || (brochureFile != null && brochureFile.length() > 0)) {
			Plan plan = planMgmtService.getPlan(planModel.getId());

			if (benefitsFile != null && benefitsFile.length() > 0) {
				if (TRUE_STRING.equals(isEdit)) {
					String filePath = uploadPath + File.separator + BENEFITS_FOLDER + File.separator + benefitsFile;
					boolean isValidPath = planMgmtUtils.isValidPath(filePath);
					if(isValidPath){
						planMgmtService.updatePlanBenefits(plan, filePath);
					}else{
						LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
					}
				} else {
					String filePath = uploadPath + File.separator + BENEFITS_FOLDER + File.separator + benefitsFile;
					boolean isValidPath = planMgmtUtils.isValidPath(filePath);
					if(isValidPath){
						planMgmtService.savePlanBenefits(plan, filePath);
					}else{
						LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
					}
				}
			}

			if (brochureFile != null && brochureFile.length() > 0) {
				String filePath = uploadPath + File.separator + BROCHURE_FOLDER + File.separator + brochureFile;
				boolean isValidPath = planMgmtUtils.isValidPath(filePath);
				if(isValidPath){
					planMgmtService.uploadPlanBrochure(plan, filePath);
				}else{
					LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
				}
			}
		}
		} catch (Exception e) {
			redirectAttrs.addFlashAttribute(PlanMgmtConstants.PLAN_ID, Integer.toString(planModel.getId()));
			redirectAttrs.addFlashAttribute("benefitsFileError", "<label class='error'><span> <em class='excl'>!</em>Error while uploading the file. Please try again.</span></label>");
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.POST_EDITQDP_BENEFIT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "redirect:/admin/planmgmt/submitqdprates";
	}

	@RequestMapping(value = "/admin/planmgmt/submitqdprates", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN_RATES)
	public String submitQDPRates(Model model, HttpServletResponse response) {
		try {
		String planId = (String) model.asMap().get(PLAN_ID);
		if (planId == null) {
			return "redirect:/admin/planmgmt/submitqdp";
		}
		String isEdit = (String) model.asMap().get(IS_EDIT);
		if (isEdit == null || !isEdit.equals(TRUE_STRING)) {
			model.addAttribute(IS_EDIT, FALSE_STRING);
		}
		Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
		model.addAttribute(ISSUER_NAME, plan.getIssuer().getName());
		model.addAttribute(PLAN_NAME, plan.getName());
		model.addAttribute(PLAN_NUMBER, plan.getIssuerPlanNumber());
		model.addAttribute(PLAN_ID, plan.getId());
		model.addAttribute("ratesFileName", planMgmtService.getActualFileName(plan.getPlanDental().getRateFile()));
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_EDITQDP_RATES.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/planmgmt/submitqdprates";
	}

	@RequestMapping(value = "/admin/planmgmt/submitqdprates", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN_RATES)
	public String submitQDPRatesUpload(@ModelAttribute(PLAN) Plan planModel, BindingResult result, Model model, @RequestParam(value = IS_EDIT, required = false) String isEdit, @RequestParam(value = RATE_FILE, required = false) String ratesFile, RedirectAttributes redirectAttrs) {
		try {
		if (ratesFile != null && ratesFile.length() > 0) {
			Plan plan = planMgmtService.getPlan(planModel.getId());
			if (TRUE_STRING.equals(isEdit)) {
				String filePath = uploadPath + File.separator + RATES_FOLDER + File.separator + ratesFile;
				boolean isValidPath = planMgmtUtils.isValidPath(filePath);
				if(isValidPath){
					planMgmtService.updatePlanRates(plan, filePath);
					redirectAttrs.addFlashAttribute(IS_EDIT, TRUE_STRING);
				}else{
					LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
				}
			} else {
				String filePath = uploadPath + File.separator + RATES_FOLDER + File.separator + ratesFile;
				boolean isValidPath = planMgmtUtils.isValidPath(filePath);
				if(isValidPath){
					redirectAttrs.addFlashAttribute(IS_EDIT, FALSE_STRING);
					planMgmtService.savePlanRates(plan, filePath);
				}else{
					LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
				}
			}
		}
		redirectAttrs.addFlashAttribute(PLAN_ID, Integer.toString(planModel.getId()));
		return "redirect:/provider/network";
		} catch (Exception e) {
			redirectAttrs.addFlashAttribute(PlanMgmtConstants.PLAN_ID, Integer.toString(planModel.getId()));
			redirectAttrs.addFlashAttribute("fileError", "<label class='error'><span> <em class='excl'>!</em>Error while uploading the file. Please try again.</span></label>");
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.POST_RATES_FILEUPLOAD.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	@RequestMapping(value = "/admin/planmgmt/submitqdpreview", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQDPReview(Model model, HttpServletResponse response) {
		try {
		String planId = (String) model.asMap().get(PLAN_ID);
		if (planId == null) {
			return "redirect:/admin/planmgmt/submitqdp";
		}
		Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
		model.addAttribute(ISSUER_NAME, plan.getIssuer().getName());
		model.addAttribute(PLAN_NAME, plan.getName());
		model.addAttribute(PLAN_ID, plan.getId());
		model.addAttribute(PLAN_NUMBER, plan.getIssuerPlanNumber());
		model.addAttribute(PLAN_MARKET, plan.getMarket());
		model.addAttribute("startDate", plan.getStartDate());
		model.addAttribute(PLAN_LEVEL, plan.getPlanDental().getPlanLevel());
		model.addAttribute("endDate", (plan.getEndDate() == null ? "Not Specified"
				: plan.getEndDate()));
		// model.addAttribute("maxEnrolee", plan.getMaxEnrolee());
		model.addAttribute("actCerti", planMgmtService.getActualFileName(plan.getPlanDental().getActurialValueCertificate()));
		model.addAttribute(BENEFIT_FILE, planMgmtService.getActualFileName(plan.getPlanDental().getBenefitFile()));
		model.addAttribute(RATE_FILE, planMgmtService.getActualFileName(plan.getPlanDental().getRateFile()));
		model.addAttribute("brochureFile", planMgmtService.getActualFileName(plan.getBrochure()));
		if (plan.getNetwork() != null) {
			model.addAttribute("netName", plan.getNetwork().getName());
			model.addAttribute("netType", plan.getNetwork().getType());
		}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_SUBMIT_QDPREVIEW.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/planmgmt/submitqdpreview";
	}

	@RequestMapping(value = "/admin/planmgmt/submitqdpstatus", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN_DETAILS)
	@ResponseBody
	public String updateQDPPlan(Model model, @RequestParam(value = PLAN_ID, required = false) String planId) {
		String userName = null;
		int userId = 0;
		try {
			userName = userService.getLoggedInUser().getUserName();
			userId = userService.getLoggedInUser().getId();
		planMgmtService.updateStatus(planId, userName, PlanMgmtService.STATUS_LOADED, userId
				+ PlanMgmtConstants.EMPTY_STRING, true, null, null, null);
		}catch (Exception e) {
			//throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.POST_UPDATEQDP.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
			LOGGER.error("Exception occured while uploading in updateQDPPlan: ",e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.POST_UPDATEQDP.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
			return PlanMgmtConstants.ERROR;
		}
		return PlanMgmtConstants.SUCCESS;
	}

	@RequestMapping(value = "/admin/planmgmt/editqdpplanstatus/{id}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN_DETAILS)
	public String updateQDPPlanStatus(@ModelAttribute(PLAN) Plan planmodel, Model model, HttpServletRequest request, @PathVariable("id") Integer id) {
		try {
		Plan plan = planMgmtService.getPlan(id);
		if (plan != null) {
			model.addAttribute(PLAN_SMALL, plan);
			model.addAttribute("actCerti", planMgmtService.getActualFileName(plan.getPlanDental().getActurialValueCertificate()));
			model.addAttribute(BENEFIT_FILE, planMgmtService.getActualFileName(plan.getPlanDental().getBenefitFile()));
			model.addAttribute(RATE_FILE, planMgmtService.getActualFileName(plan.getPlanDental().getRateFile()));
			model.addAttribute("qdpcertiStatusList", planmodel.getPlanStatuses());
			model.addAttribute("network", plan.getNetwork());

			return "admin/planmgmt/updatestatusqdpplans";
		} else {
			return MANAGE_QDP_PATH;
		}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_UPDATEQDP_STATUS.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/updateqdpplanstatus", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN_DETAILS)
	public String updatePlanInfo(Model model, @RequestParam(PLAN_ID) String planId, @RequestParam(STATUS) String status) {
		try {
		Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
		plan.setStatus(status);
		String userName = null;
		int userId = 0;
			userName = userService.getLoggedInUser().getUserName();
			userId = userService.getLoggedInUser().getId();
		planMgmtService.updateStatus(planId, userName, status, userId + PlanMgmtConstants.EMPTY_STRING, false, null, null, null);
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.POST_UPDATEPLANINFO.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return MANAGE_QDP_PATH;
	}

	/**
	 * Fetches plan for populating its data in the UI
	 * 
	 * @param model
	 *            model attribute in which the data should be set.
	 */
	private Plan loadPlanToDisplay(Model model) {
		String planId = (String) model.asMap().get(PLAN_ID);
		Plan plan = null;
		try {
		if (planId != null) {
			plan = planMgmtService.getPlan(Integer.parseInt(planId));
			model.addAttribute(PLAN_SMALL, plan);
			model.addAttribute(PLAN_CERTI_FILE_NAME, plan.getPlanDental().getActurialValueCertificate());
			
			String enrollmentAvial = plan.getEnrollmentAvailList().get(Enum.valueOf(EnrollmentAvail.class, plan.getEnrollmentAvail()));
			Calendar cal = TSCalendar.getInstance();
			Date today = cal.getTime();
			// if enrollment status is available/dependents only but
			// available from future date then availability consider as not
			// available
//			if (!plan.getEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.NOTAVAILABLE.toString()) && (plan.getEnrollmentAvailEffDate().compareTo(today) >= 0)) {
//				enrollmentAvial = plan.getEnrollmentAvailList().get(Enum.valueOf(EnrollmentAvail.class, EnrollmentAvail.NOTAVAILABLE.toString()));
//			}
			model.addAttribute("enrollAvailability", enrollmentAvial);
							
			// Code changes for HIX-HIX-26331
			SimpleDateFormat ft = new SimpleDateFormat ("MMddyyyy");
			if(today.compareTo(plan.getStartDate()) < 0){
				model.addAttribute(PLAN_DIS_DATE, ft.format(plan.getStartDate()));
			}
			else if(today.compareTo(plan.getEndDate()) > 0){
				model.addAttribute(PLAN_DIS_DATE, ft.format(plan.getEndDate()));
			} else {
				model.addAttribute(PLAN_DIS_DATE, ft.format(today));
			}
			// End changes
			Network networkInfo = providerNetworkService.getNetwork(plan.getProviderNetworkId());
			if (networkInfo != null) {
				model.addAttribute(NETWORK, networkInfo);
			} else {
				model.addAttribute(NETWORK, "--");
			}
			if(plan.getServiceAreaId() != null){
				String serfServiceAreaId = issuerService.getSerfServiceAreaIdById(plan.getServiceAreaId());
				model.addAttribute(PlanMgmtConstants.SERVICE_AREA_ID, serfServiceAreaId);
			}

		}
		}catch(Exception e){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.POST_UPDATEPLANINFO.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return plan;
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/editqdpdetail/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN_DETAILS)
	public String editQDPDetail(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			model.addAttribute(PAGE_TITLE, "Edit QDP Detail");
			model.addAttribute(PLAN_ID, decryptedPlanId);
			Plan plan = loadPlanToDisplay(model);
			model.addAttribute(QDPMARKETLIST, plan.getMarketList());
			model.addAttribute(QDPLEVELLIST, plan.getLevelList());
			String displayPlanLevel = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QDP_SUPPORTPLANLEVEL);
			model.addAttribute(DISPLAYPLANLEVEL, displayPlanLevel);
			model.addAttribute(CURRENTPLANLEVEL, plan.getPlanDental().getPlanLevel());
			return "planmgmt/editqdpdetail";
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.POST_UPDATEPLANINFO.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	private Plan updatePlan(Plan plan, RedirectAttributes redirectAttrs, String planLevelReadOnly) throws InvalidUserException, GIException, IOException, ContentManagementServiceException {
		if (plan.getPlanDental().getPlanLevel() == null) {
			plan.getPlanDental().setPlanLevel(planLevelReadOnly);
		}
		Plan updatedPlan = null;
		Integer userId = null;
			userId = userService.getLoggedInUser().getId();
		plan.setLastUpdatedBy(userId);
		if (plan.getId() > 0) {
			updatedPlan = planMgmtService.updatePlan(plan);
			redirectAttrs.addFlashAttribute(IS_EDIT, TRUE_STRING);
		} else {
			plan.setCreatedBy(userId);
			updatedPlan = planMgmtService.savePlan(plan);
		}
		return updatedPlan;
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/editqdpdetail", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN_DETAILS)
	public String editQDPDetailPersist(@ModelAttribute(PLAN) Plan plan, BindingResult result, Model model, RedirectAttributes redirectAttrs, @RequestParam(value = "planLevelReadOnly", required = false) String planLevelReadOnly) {
		model.addAttribute(PAGE_TITLE, "Edit QDP Detail Post");
		try {
			updatePlan(plan, redirectAttrs, planLevelReadOnly);
			model.addAttribute(QDPMARKETLIST, plan.getMarketList());
			model.addAttribute(QDPLEVELLIST, plan.getLevelList());
		}catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.POST_EDITQDP_DETAIL.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(plan.getId()));
		return "redirect://admin/planmgmt/viewqdpdetail/" + encryptedPlanId;
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqdpdetail/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQDPDetail(Model model, @PathVariable("encPlanId") String encPlanId) {

		String decryptedPlanId = null;
		try{
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			model.addAttribute(PAGE_TITLE, "View QDP Detail");
			model.addAttribute(PLAN_ID, decryptedPlanId);
			
			AccountUser accountUser=userService.getLoggedInUser();
			if(accountUser.getUserPermissions() != null){
				if(accountUser.getUserPermissions().contains(PlanMgmtConstants.PERMISSION_EDIT_PLANS)){
					model.addAttribute(PlanMgmtConstants.HAS_EDIT_PERMISSIONS, true);
				} else {
					model.addAttribute(PlanMgmtConstants.HAS_EDIT_PERMISSIONS, false);
				}
			}
			
			Plan plan = loadPlanToDisplay(model);
			String displayPlanLevel = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QDP_SUPPORTPLANLEVEL);
			String displayEditButton = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_MANAGEQDP_PLANRATES_DISPLAYEDITBUTTON);
			model.addAttribute(DISPLAYEDITBUTTON, displayEditButton);
			model.addAttribute(DISPLAYPLANLEVEL, displayPlanLevel);
			model.addAttribute(PlanMgmtConstants.TIMEZONE,TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));
			
			String displayProviderNetwork = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_VIEWQDPDETAILS_DISPLAYPROVIDERNETWORKTAB);
			model.addAttribute(DISPLAYPROVIDERNETWORK, displayProviderNetwork);
			
			String displayAction = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_VIEWQDPDETAILS_DISPLAYACTION);
			model.addAttribute(DISPLAYACTION, displayAction);
			
			model.addAttribute("exchangeType", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			// HIX-31861 Code changes
		 	model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.EMPTY_STRING);
		 	model.addAttribute(PlanMgmtConstants.FUTURE_DATE, plan.getFutureErlAvailEffDate());
		 	
		 	Date sysDate = new TSDate();
			if(plan.getFutureEnrollmentAvail() == null){
				model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.EMPTY_STRING);
			} else if(plan.getFutureErlAvailEffDate() != null && plan.getFutureErlAvailEffDate().compareTo(sysDate) > 0){
				if(plan.getFutureEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.AVAILABLE.toString())){
					model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.AVAILABLE);	
				} else if(plan.getFutureEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.NOTAVAILABLE.toString())){
					model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.NOTAVAILABLE);
				} else if(plan.getFutureEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.DEPENDENTSONLY.toString())){
					model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.DEPENDENTSONLY);
				}
			}
			// HIX-31861 end changes
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_VIEWQDP_DETAIL.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "planmgmt/viewqdpdetail";
	}

	// QDP plan provider network history
	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqdpprovidernetwork/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQDPProviders(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			if (decryptedPlanId == null || decryptedPlanId.isEmpty() || decryptedPlanId.equalsIgnoreCase("0")) {
				return MANAGE_QDP_PATH;
			}
			model.addAttribute(PLAN_ID, decryptedPlanId);
			loadPlanToDisplay(model);
			List<Map<String, Object>> providerData = planMgmtService.loadProviderHistory(Integer.parseInt(decryptedPlanId));
			model.addAttribute(DATA, providerData);
			model.addAttribute(PAGE_SIZE, GhixConstants.PAGE_SIZE);
			String displayEditButton = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_MANAGEQDP_PROVIDERNETWORK_DISPLAYEDITBUTTON);
			model.addAttribute(DISPLAYEDITBUTTON, displayEditButton);
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_VIEWQDP_PROVIDERNETWORK.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/planmgmt/qdp/providers";
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	// QDP plan provider network : Show provider network to edit
	@RequestMapping(value = "/admin/planmgmt/editqdpprovidernetwork/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String editQDPProviderNetwork(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			model.addAttribute(PAGE_TITLE, "Edit QDP Provider Network");
			if (decryptedPlanId == null || decryptedPlanId.isEmpty() || decryptedPlanId.equalsIgnoreCase("0")) {
				return MANAGE_QDP_PATH;
			}
			Plan plan = planMgmtService.getPlan(Integer.parseInt(decryptedPlanId));
			model.addAttribute("issuerId", plan.getIssuer().getId());
			model.addAttribute(ISSUER_NAME, plan.getIssuer().getName());
			model.addAttribute(PLAN_ID, plan.getId());
			model.addAttribute(PLAN_NAME, plan.getName());
			model.addAttribute(PLAN_NUMBER, plan.getIssuerPlanNumber());
			model.addAttribute("currentStatus", plan.getStatus());
			model.addAttribute("networkList", providerNetworkService.getProviderNetworkByIssuerId(plan.getIssuer().getId()));
			int networkId = 0;
			if (plan.getNetwork() != null) {
				networkId = plan.getNetwork().getId();
			}
			model.addAttribute("selNetwork", networkId);
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_UPDATE_PROVIDERNETWORK.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "planmgmt/editqdpprovider";
	}

	// QDP plan provider network : After editing, redirect to provider history
	/*
	 * @Suneel: Secured the method with permission 
	 */
	@RequestMapping(value = "/admin/planmgmt/updateqdpprovidernetwork", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String updateQDPProviderNetwork(Model model, @RequestParam(value = PLAN_ID, required = true) String planId, @RequestParam(value = "providerNetwork", required = false) String providerNetworkId) {
		try {
		if (providerNetworkId == null || providerNetworkId.isEmpty()) {
			return MANAGE_QDP_PATH;
		}
		planMgmtService.updateProvider(planId.toString(), providerNetworkId.toString());
		return "redirect:/admin/planmgmt/viewqdpprovidernetwork/" + planId;
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.POST_UPDATE_PROVIDERNETWORK.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	// Show QDP plan benefits History
	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqdpbenefits/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQDPBenefits(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			model.addAttribute(PAGE_TITLE, "View QDP Benefits");
			model.addAttribute(PLAN_ID, planId.toString());
			loadPlanToDisplay(model);
			List<Map<String, Object>> plansData = planMgmtService.loadPlanBenefitsHistory(planId, PlanMgmtConstants.ADMIN_ROLE);
			model.addAttribute(DATA, plansData);
			model.addAttribute(PAGE_SIZE, GhixConstants.PAGE_SIZE);
			String displayEditButton = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_MANAGEQDP_PROVIDERNETWORK_DISPLAYEDITBUTTON);
			model.addAttribute(DISPLAYEDITBUTTON, displayEditButton);
			model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_VIEW_QDPBENEFIT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/planmgmt/viewqdpbenefits";
	}

	// Edit QDP plan benefits
	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/editqdpbenefits/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN_BENEFITS)
	public String editQDPBenefits(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			if (planId == null) {
				return "redirect:/admin/planmgmt/viewqdpbenefits";
			}
			model.addAttribute(PAGE_TITLE, "Edit QDP Benefits");
			Plan plan = planMgmtService.getPlan(planId);
			model.addAttribute(ISSUER_NAME, plan.getIssuer().getName());
			model.addAttribute(PLAN_ID, planId);
			model.addAttribute(PLAN_NAME, plan.getName());
			model.addAttribute(PLAN_NUMBER, plan.getIssuerPlanNumber());
			model.addAttribute("planStatus", plan.getStatus());
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_EDIT_QDPBENEFIT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/planmgmt/editqdpbenefits";
	}

	// Update QDP plan benefits
	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/editqdpbenefits", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN_BENEFITS)
	public String editQDPBenefitsUpload(@ModelAttribute(PLAN) Plan planModel, BindingResult result, Model model, @RequestParam(value = BENEFIT_FILE, required = false) String benefitsFile, @RequestParam(value = "brochureFile", required = false) String brochureFile, @RequestParam(value = "benefitStartDate", required = false) String benefitStartDate, @RequestParam(value = COMMENTS, required = false) String comments, RedirectAttributes redirectAttrs) {
		try {
		// create planObj only if either benefit file or brochure file uploaed
		if ((benefitsFile != null && benefitsFile.length() > 0) || (brochureFile != null && brochureFile.length() > 0)) {
			Plan planObj = planMgmtService.getPlan(planModel.getId());
			if (benefitsFile != null && benefitsFile.length() > 0) {
				String filePath = uploadPath + File.separator + BENEFITS_FOLDER + File.separator + benefitsFile;
				boolean isValidPath = planMgmtUtils.isValidPath(filePath);
				if(isValidPath){
					planMgmtService.editPlanBenefits(planObj, filePath, benefitStartDate, comments);
				}else{
					LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
				}
			}

			if (brochureFile != null && brochureFile.length() > 0) {
				String filePath = uploadPath + File.separator + BROCHURE_FOLDER + File.separator + brochureFile;
				boolean isValidPath = planMgmtUtils.isValidPath(filePath);
				if(isValidPath){
					planMgmtService.uploadPlanBrochure(planObj, filePath);
				}else{
					LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
				}
			}
		}

		redirectAttrs.addFlashAttribute(PLAN_ID, planModel.getId());
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.POST_UPLOAD_QDPBENEFIT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(planModel.getId()));
		return "redirect:/admin/planmgmt/viewqdpbenefits/" + encryptedPlanId;
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "admin/planmgmt/editqdpenrollmentavail/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String editQDPEnrollmentAvail(Model model, HttpServletRequest request, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		String deCertificationEffDate = null;
		String showSaveButton = PlanMgmtConstants.TRUE_STRING;

		try{
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			model.addAttribute("currDate", DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT));
			int planid = 0;
			planid = Integer.parseInt(decryptedPlanId);
			Plan plan = planMgmtService.getPlan(planid);
			if (plan != null) {
				if(null != plan.getDeCertificationEffDate()){
					deCertificationEffDate = String.valueOf(plan.getDeCertificationEffDate());
				}
				
				showSaveButton = planMgmtService.showSaveButtonForUpdate(plan.getStatus(), deCertificationEffDate);
				model.addAttribute("showSaveButton", showSaveButton);
				// model.addAttribute(PLAN_SMALL, plan);
				model.addAttribute("enrollmentAvailList", plan.getEnrollmentAvailList());
				model.addAttribute(PAGE_TITLE, "Edit QDP Enrollment Availlability");
				model.addAttribute(PlanMgmtConstants.PLAN, plan);
				model.addAttribute(PlanMgmtConstants.CURR_DATE, DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				if(plan.getFutureErlAvailEffDate()!=null){
					model.addAttribute(PlanMgmtConstants.FUTURE_DATE, DateUtil.dateToString(plan.getFutureErlAvailEffDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}
				model.addAttribute("sysDate", new TSDate());
				
				return "planmgmt/editqdpenrollavail";
			} else {
				return MANAGE_QDP_PATH;
			}
		}catch(Exception e){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_EDIT_QDPENROLLMENT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "admin/planmgmt/editqdpenrollmentavail", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQDPEnrollmentAvail(Model model, @RequestParam(value = COMMENTS, required = false) String comments, HttpServletRequest request) {
		Integer userId = null;
		String userName = null;
		try {
			userId = userService.getLoggedInUser().getId();
			userName = userService.getLoggedInUser().getUserName();
			String id = request.getParameter("id");
			String effectiveDate = request.getParameter("effectiveDate");
			String enrollmentAvail = request.getParameter("enrollmentavail");
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			if(effectiveDate.isEmpty() || enrollmentAvail.isEmpty() || !enrollmentAvail.matches(PlanMgmtConstants.ENROLLMENTAVAILABILITY_PATTERN)){
				throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			}
			
			Plan oldPlanInfo = planMgmtService.getPlan(Integer.parseInt(id));
			String oldAvailability = oldPlanInfo.getEnrollmentAvail();
			LOGGER.debug("oldAvailability: " +  SecurityUtil.sanitizeForLogging(oldAvailability));
			Date futureEnrlAailEffDate = oldPlanInfo.getFutureErlAvailEffDate();
			SimpleDateFormat dateFormat = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT_MMDDYYYY);
			Date effDate = dateFormat.parse(effectiveDate);
			String sendNotificationToBroker = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.SEND_NOTIFICATION_TO_BROKER);
			if(null != futureEnrlAailEffDate && effDate.after(futureEnrlAailEffDate)){
				model.addAttribute("futureDateValidationError", PlanMgmtConstants.TRUE_STRING);
				String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(id);
				return "redirect:/admin/planmgmt/editqdpenrollmentavail/" + encryptedPlanId;
			}else{
				if (planMgmtService.updateEnrollmentAvail(id, userId, userName, effectiveDate, enrollmentAvail, comments)) {
					Plan plan = planMgmtService.getPlan(Integer.parseInt(id));
					// send enrollment availability notification to issuer rep only for ID state exchange
					if(plan.getStatus().equalsIgnoreCase(CERTIFIED) && stateCode.equalsIgnoreCase("ID")){
						enrollmentAvailabilityEmailUtil.sendNotificationEmailToIssuerRepresentativeForEnrollment(plan);
					}
					
					LOGGER.debug("sendNotificationToBroker configuration: " + SecurityUtil.sanitizeForLogging(String.valueOf(sendNotificationToBroker)));
					if(PlanMgmtConstants.ON.equalsIgnoreCase(sendNotificationToBroker) && !stateCode.equalsIgnoreCase(PlanMgmtConstants.STATECODEFORCA)){
						// check if the current date is between plan start date and end date 
						Date currentDate = new TSDate();
						if((currentDate.equals(plan.getEndDate()) || currentDate.equals(plan.getStartDate()) ) || (currentDate.before(plan.getEndDate()) && currentDate.after(plan.getStartDate()) ) ){
							Integer conditionalCaseValForAvailability = checkTriggerEmailConditionsforAvailability(oldAvailability ,enrollmentAvail, plan.getStatus());
							if(LOGGER.isDebugEnabled()){
							LOGGER.debug("conditionalCaseValForAvailability : " + SecurityUtil.sanitizeForLogging(String.valueOf(conditionalCaseValForAvailability)));
							}
							
							if(conditionalCaseValForAvailability > PlanMgmtConstants.ZERO){
								List<BrokerContactDTO> brokersList = new ArrayList<BrokerContactDTO>();
								String brokerResponse = restTemplate.postForObject(GhixEndPoints.BrokerServiceEndPoints.GET_LISTOF_CERTIFIED_BROKERS, null, String.class);
								Type type = new TypeToken<List<BrokerContactDTO>>() {}.getType();

								if(StringUtils.isNotBlank(brokerResponse)){
									brokersList = platformGson.fromJson(brokerResponse, type);
								}							
								if(LOGGER.isDebugEnabled()){
									LOGGER.debug("# of brokers recieved through API : " + brokersList.size());
									}
									
								if(brokersList != null && brokersList.size() > PlanMgmtConstants.ZERO){
									//sendNotificationEmailToBrokers(plan, brokersList);
									if(conditionalCaseValForAvailability.equals(PlanMgmtConstants.ONE)){
										sendNotificationEmailToBrokers(planMgmtService.getPlan(Integer.parseInt(id)), brokersList,PlanMgmtConstants.PLANINFORMATION_FOR_NONCERTIFIED_OR_CERTIFIED_OR_INCOMPLETE_AND_NOTAVAILABLE);
	
									}else if(conditionalCaseValForAvailability.equals(PlanMgmtConstants.TWO)){
										sendNotificationEmailToBrokers(planMgmtService.getPlan(Integer.parseInt(id)), brokersList,PlanMgmtConstants.PLANINFORMATION_FOR_CERTIFIED_AND_DEPENDENTSONLY);
	
									}else if(conditionalCaseValForAvailability.equals(PlanMgmtConstants.THREE)){
										sendNotificationEmailToBrokers(planMgmtService.getPlan(Integer.parseInt(id)), brokersList,PlanMgmtConstants.PLANINFORMATION_FOR_CERTIFIED_AND_AVAILABLE);
									}
								}
							}
						}
					}
					String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(id);
					return "redirect:/admin/planmgmt/viewqdpenrollmentavail/" + encryptedPlanId;
				} else {
					return MANAGE_QDP_PATH;
				}
			}
		}catch(Exception e){
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, PlanMgmtConstants.MODULE_NAME, null);
		}
	}
	
	/**
	 * @param oldEnrollmentAvail
	 * @param currentEnrollmentAvail
	 * @param currentPlanStatus
	 * @return Integer 1 or 2 or 3
	 * 1 for CERTIFIED and AVAILABLE plan get NOTAVAILABLE
	 * 2 for CERTIFIED and AVAILABLE plan get DEPENDENTSONLY
	 * 3 for CERTIFIED plan get AVAILABLE
	 */
	private Integer checkTriggerEmailConditionsforAvailability(String oldEnrollmentAvail, String currentEnrollmentAvail, String currentPlanStatus){
		Integer conditionalCaseValForStatus = PlanMgmtConstants.ZERO;
		if(Plan.PlanStatus.CERTIFIED.toString().equals(currentPlanStatus) && Plan.EnrollmentAvail.AVAILABLE.toString().equals(oldEnrollmentAvail) 
				&& Plan.EnrollmentAvail.NOTAVAILABLE.toString().equals(currentEnrollmentAvail)){
			conditionalCaseValForStatus = PlanMgmtConstants.ONE;

		}else if(Plan.PlanStatus.CERTIFIED.toString().equals(currentPlanStatus) && Plan.EnrollmentAvail.AVAILABLE.toString().equals(oldEnrollmentAvail) 
				&& Plan.EnrollmentAvail.DEPENDENTSONLY.toString().equals(currentEnrollmentAvail)){
			conditionalCaseValForStatus = PlanMgmtConstants.TWO;

		}else if(Plan.PlanStatus.CERTIFIED.toString().equals(currentPlanStatus) && Plan.EnrollmentAvail.AVAILABLE.toString().equals(currentEnrollmentAvail)){
			conditionalCaseValForStatus = PlanMgmtConstants.THREE;
		}
		return conditionalCaseValForStatus;
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/editqdpcertification/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String editQDPCertification(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		String decertificationEffiectiveDate = null;
		Long enrollmentCountForPlanId = 0L;
		String showSaveButton = PlanMgmtConstants.TRUE_STRING;
		String deCertificationEffDate = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			model.addAttribute(PAGE_TITLE, "Edit QDP Certification");
			Plan plan = planMgmtService.getPlan(planId);
			if (plan != null) {
				if(null != plan.getDeCertificationEffDate()){
					deCertificationEffDate = String.valueOf(plan.getDeCertificationEffDate());
				}

				showSaveButton = planMgmtService.showSaveButtonForUpdate(plan.getStatus(), deCertificationEffDate);
				model.addAttribute("showSaveButton", showSaveButton);
				model.addAttribute(ISSUER_NAME, plan.getIssuer().getName());
				model.addAttribute("issuerId", plan.getIssuer().getId());
				model.addAttribute("issuerStatus", plan.getIssuer().getCertificationStatus());
				model.addAttribute(PLAN_ID, plan.getId());
				model.addAttribute(PLAN_NAME, plan.getName());
				model.addAttribute(PLAN_NUMBER, plan.getIssuerPlanNumber());
				model.addAttribute("currentStatus", plan.getStatus());
				model.addAttribute("qdpcertiStatusList", plan.getPlanStatuses(plan.getStatus()));
				model.addAttribute("currDate", DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				model.addAttribute(PlanMgmtConstants.PLAN, plan);
				model.addAttribute("qdpIssuerVerificationList", plan.getIssuerVerificationList());
				model.addAttribute("certiSuppDoc", planMgmtService.getActualFileName(plan.getSupportFile()));
				model.addAttribute("exchangeType", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
				if(null != plan.getEnrollmentEndDate()){
					model.addAttribute("enrollmentEndDate", DateUtil.dateToString(plan.getEnrollmentEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}
				String displayProviderNetwork = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_VIEWQHPDETAILS_DISPLAYPROVIDERNETWORKTAB);
				model.addAttribute(DISPLAYPROVIDERNETWORK, displayProviderNetwork);
				LOGGER.debug("displayProviderNetwork_edit: " +  SecurityUtil.sanitizeForLogging(displayProviderNetwork));

				String displayAction = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_VIEWQHPDETAILS_DISPLAYACTION);
				LOGGER.debug("displayAction_edit: " +  SecurityUtil.sanitizeForLogging(displayAction));
				
				if(null != plan.getDeCertificationEffDate()){
					decertificationEffiectiveDate = DateUtil.dateToString(plan.getDeCertificationEffDate(), GhixConstants.REQUIRED_DATE_FORMAT);
				}
				
				/*if(StringUtils.isNotEmpty(terminateEnrollmentConfiguration) && PlanMgmtConstants.ON.equalsIgnoreCase(terminateEnrollmentConfiguration)){
					if(PlanMgmtConstants.ZERO != plan.getId() && PlanMgmtConstants.ZERO != plan.getApplicableYear() && null != decertificationEffiectiveDate){
						enrollmentCountForPlanId = planMgmtRestUtil.getNumberOfEnrollmentForPlanId(plan.getIssuerPlanNumber(), plan.getApplicableYear(), decertificationEffiectiveDate);
						if(LOGGER.isDebugEnabled()){
							LOGGER.info("enrollmentCountForPlanId: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentCountForPlanId)));
						}
					}
				}*/
				String terminateEnrollmentConfiguration = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.TERMINATE_ENROLLMENT_FEATURE);
				Date enrollmentEndDate = planMgmtUtils.getEnrollmentEndDate();
				Date decertificationDate = planMgmtUtils.getDeCertificationDate(enrollmentEndDate);
				model.addAttribute("sysDate", decertificationDate);
				model.addAttribute("enrollmentEndSysDate", DateUtil.dateToString(enrollmentEndDate, GhixConstants.REQUIRED_DATE_FORMAT));
				model.addAttribute("enrollmentCountForPlanId", enrollmentCountForPlanId);
				model.addAttribute("terminateEnrollmentStatus", (null != plan.getTerminateEnrollmentStatus() ? plan.getTerminateEnrollmentStatus() : PlanMgmtConstants.EMPTY_STRING));
				model.addAttribute("showTerminateEnrollment", terminateEnrollmentConfiguration);
			}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_EDIT_QDPENROLLMENT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "planmgmt/editqdpcertification";
	}

	/*
	 * @Suneel: Secured the method with permission 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/admin/planmgmt/updateqdpcertification", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String updateQDPCertification(@ModelAttribute(PlanMgmtConstants.PLAN) Plan planData, Model model, @RequestParam(PLAN_ID) String planId, @RequestParam(STATUS) String status, 
			@RequestParam("hdnCertSuppDoc") String certSuppDoc, @RequestParam(value = COMMENTS, required = false) String comments, 
			@RequestParam(value = ISSUER_VERIFICATION, required = false) String issuerVerificationStatus,@RequestParam(value="deCertificationEffDateUI",required=false) String deCertificationDate,
			@RequestParam(value="enrollmentEndDateUI",required=false) String enrollmentEndDate, RedirectAttributes redirectAttrs) {
		String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(planId);
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		String sendNotificationToBroker = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.SEND_NOTIFICATION_TO_BROKER);
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT);
			if(StringUtils.isNotBlank(deCertificationDate)){
				Date deCertiyDate = formatter.parse(deCertificationDate);
				if(deCertiyDate.before(new TSDate())){
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				}
				planData.setDeCertificationEffDate(deCertiyDate);
			}
			if(StringUtils.isNotBlank(enrollmentEndDate)){
				Date enrollEndDate = formatter.parse(enrollmentEndDate);
				if(enrollEndDate.before(new TSDate())){
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				}
				planData.setEnrollmentEndDate(enrollEndDate);
			}
			//server side validation
			if(!Plan.PlanStatus.DECERTIFIED.toString().equals(status)){
				 Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
				 Set<ConstraintViolation<Plan>> violations = validator.validate(planData, Plan.EditCertificationStatus.class);
				 if(violations != null && !violations.isEmpty()){
					//throw exception in case client side validation breached
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				 }
			}else{
				//server side validation
				 Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
				 Set<ConstraintViolation<Plan>> violations = validator.validate(planData, Plan.DeCertificationStatus.class);
				 if(violations != null && !violations.isEmpty()){
					//throw exception in case client side validation breached
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				 }
			}
			Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
			if(Plan.PlanStatus.DECERTIFIED.toString().equalsIgnoreCase(status) && (Plan.EnrollmentAvail.AVAILABLE.toString().equalsIgnoreCase(plan.getEnrollmentAvail()) ||
					Plan.EnrollmentAvail.DEPENDENTSONLY.toString().equalsIgnoreCase(plan.getEnrollmentAvail()))){
				redirectAttrs.addFlashAttribute("decertificationError", PlanMgmtConstants.TRUE_STRING);
				return "redirect:/admin/planmgmt/editqdpcertification/" + encryptedPlanId;
			}
			
		//	String notifyToIssuer = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.NOTIFYTOISSUER);
			AccountUser user = null;
			boolean isEmailSent = false;
			user = userService.getLoggedInUser();
			String oldStatus = plan.getStatus();
			// update the certification status of the plan
			planMgmtService.updatePlanStatus(Integer.parseInt(planId), user, status, certSuppDoc, comments, planData.getDeCertificationEffDate(), planData.getEnrollmentEndDate(), issuerVerificationStatus);
			
			/*if (Plan.PlanStatus.LOADED.toString().equalsIgnoreCase(oldStatus) && Plan.PlanStatus.CERTIFIED.toString().equalsIgnoreCase(status)) {
	
				if (EXCHANGE_TYPE.equalsIgnoreCase("PHIX") && notifyToIssuer.equalsIgnoreCase("true")) {
					sendNotificationEmail(plan);
				} else if (EXCHANGE_TYPE.equalsIgnoreCase("PHIX") && notifyToIssuer.equalsIgnoreCase("false")) {
					LOGGER.info("Notifier will not send a mail, since notify to issuer is false.");
				} else {
					// sendNotificationEmail(plan);
					sendNotificationEmailToIssuerRepresentative(plan, Plan.PlanStatus.CERTIFIED.toString());
					isEmailSent = true;
				}
			}*/
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("sendNotificationToBroker configuration: " + SecurityUtil.sanitizeForLogging(String.valueOf(sendNotificationToBroker)));
			}
			
			// if send Notification To Broker is ON and state exchange is ID, then send notification to Brokers
			if(PlanMgmtConstants.ON.equalsIgnoreCase(sendNotificationToBroker) && !stateCode.equalsIgnoreCase(PlanMgmtConstants.STATECODEFORCA)){
				// check if the current date is between plan start date and end date 
				Date currentDate = new TSDate();
				if((currentDate.equals(plan.getEndDate()) || currentDate.equals(plan.getStartDate()) ) || (currentDate.before(plan.getEndDate()) && currentDate.after(plan.getStartDate()) ) ){
					Integer conditionalCaseValForStatus = checkTriggerEmailConditionsforCertificationStatus(oldStatus, status, plan.getEnrollmentAvail());
					if(LOGGER.isDebugEnabled()){
					LOGGER.debug("conditionalCaseValForStatus : " + SecurityUtil.sanitizeForLogging(String.valueOf(conditionalCaseValForStatus)));
					}
					
					if(conditionalCaseValForStatus > PlanMgmtConstants.ZERO){
						List<BrokerContactDTO> brokersList = new ArrayList<BrokerContactDTO>();
						String brokerResponse = restTemplate.postForObject(GhixEndPoints.BrokerServiceEndPoints.GET_LISTOF_CERTIFIED_BROKERS, null, String.class);
						Type type = new TypeToken<List<BrokerContactDTO>>() {}.getType();
						
						if(StringUtils.isNotBlank(brokerResponse)){
							brokersList = platformGson.fromJson(brokerResponse, type);
						}				
					    if(LOGGER.isDebugEnabled()){
							LOGGER.debug("# of brokers recieved through API : " + brokersList.size());
							}
							
						if(brokersList != null && brokersList.size() > PlanMgmtConstants.ZERO){
							//sendNotificationEmailToBrokers(planMgmtService.getPlan(Integer.parseInt(planId)), brokersList);
							if(conditionalCaseValForStatus.equals(PlanMgmtConstants.ONE)){
								sendNotificationEmailToBrokers(planMgmtService.getPlan(Integer.parseInt(planId)), brokersList,PlanMgmtConstants.PLANINFORMATION_FOR_NONCERTIFIED_OR_CERTIFIED_OR_INCOMPLETE_AND_NOTAVAILABLE);
							}else if(conditionalCaseValForStatus.equals(PlanMgmtConstants.TWO)){
								sendNotificationEmailToBrokers(planMgmtService.getPlan(Integer.parseInt(planId)), brokersList,PlanMgmtConstants.PLANINFORMATION_FOR_CERTIFIED_AND_AVAILABLE);	
							}
						}
					}
				}
			}
			
			//On plan certification send notification to issuer representative only for ID state exchange	
			if(stateCode.equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_ID) && Plan.PlanStatus.CERTIFIED.toString().equalsIgnoreCase(status)){
				planMgmtService.sendNotificationEmailToIssuerRepresentative(planMgmtService.getPlan(Integer.parseInt(planId)), Plan.PlanStatus.CERTIFIED.toString());
			}
			
			// Code changes for HIX-5252
			if(stateCode.equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_ID) && Plan.PlanStatus.DECERTIFIED.toString().equalsIgnoreCase(status)){
				planMgmtService.sendNotificationEmailToIssuerRepresentative(planMgmtService.getPlan(Integer.parseInt(planId)), Plan.PlanStatus.DECERTIFIED.toString());	
			}
			// End changes
			
			//  HIX-90008 : If plan certification status set as CERTIFIED OR INCOMPLETE then call syncPlanWithAHBX() service to sync plan data with AHBX
			if (status.equalsIgnoreCase(PlanStatus.CERTIFIED.toString()) || status.equalsIgnoreCase(PlanStatus.INCOMPLETE.toString())) {
					planMgmtService.syncPlanWithAHBX(Integer.parseInt(planId));
			}
				
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.POST_UPDATE_QDPCERTIFICATION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "redirect:/admin/planmgmt/viewqdpcertification/" + encryptedPlanId;
	}

	/**
	 * @param oldStatus
	 * @param currentPlanStatus
	 * @param currentEnrollmentAvail
	 * @return integer values 1 or 2
	 * 1 for CERTIFIED plan gets DECERTIFIED OR INCOMPLETE
	 * 2 for CERTIFIED plan gets AVAILABLE
	 */
	private Integer checkTriggerEmailConditionsforCertificationStatus(String oldStatus, String currentPlanStatus, String currentEnrollmentAvail){
		Integer conditionalCaseValForStatus = PlanMgmtConstants.ZERO;
		if(Plan.PlanStatus.CERTIFIED.toString().equals(oldStatus) && (Plan.PlanStatus.DECERTIFIED.toString().equals(currentPlanStatus)  || Plan.PlanStatus.INCOMPLETE.toString().equals(currentPlanStatus))){
			conditionalCaseValForStatus = PlanMgmtConstants.ONE;
		}else if(Plan.PlanStatus.CERTIFIED.toString().equals(currentPlanStatus) && Plan.EnrollmentAvail.AVAILABLE.toString().equals(currentEnrollmentAvail)){
			conditionalCaseValForStatus = PlanMgmtConstants.TWO;
		}		
		return conditionalCaseValForStatus;
	}
	
	private void sendNotificationEmailToBrokers(Plan plan, List<BrokerContactDTO> brokers, String planInformationUpdate) {
		HashMap<String, Object> notificationContext = new HashMap<String, Object>();
		Notice noticeObj = null;
		StringBuilder emailId = new StringBuilder();
		StringBuilder tempString = new StringBuilder();
		//Map<String, String> emailData = new HashMap<String, String>(); 
		Issuer issuerObj = issuerService.getIssuerById(plan.getIssuer().getId());
		notificationContext.put("ISSUER", issuerObj);
		notificationContext.put(PlanMgmtConstants.PLANINFORMATION_UPDATE,planInformationUpdate);
		if(null != brokers){
			for(BrokerContactDTO brokerContactDTO : brokers){
				tempString.append(brokerContactDTO.getEmail());
				tempString.append(";");
				if(tempString.length() > PlanMgmtConstants.BCC_ADDRESS_STRING_MAX_LENGTH){
					tempString.append(PlanMgmtConstants.DOLLARSIGN);
					emailId.append(tempString);
					tempString = new StringBuilder();
				}
			}
			if(!StringUtils.isEmpty(tempString.toString())){
				emailId.append(tempString);
				tempString = null;
			}
			
			notificationContext.put("PLAN",plan);
			notificationContext.put("ISSUER",issuerObj);
			Map<String, String> tokens = null;
			Map<String, String> emailHeaders = null;
			if(null != emailId){
				String []splitBrokerIds = emailId.toString().split(PlanMgmtConstants.DOLLAR);
				for(int i=0; i<splitBrokerIds.length; i++){
					String recepients = splitBrokerIds[i];
					notificationContext.put(PlanMgmtConstants.RECIPIENT, recepients);
					tokens = brokerNotificationEmail.getTokens(notificationContext);
					emailHeaders = brokerNotificationEmail.getEmailData(notificationContext);
					try {
						noticeObj = brokerNotificationEmail.generateEmail(brokerNotificationEmail.getClass().getSimpleName(), null, emailHeaders, tokens);
						Notification notificationObj = brokerNotificationEmail.generateNotification(noticeObj);
						brokerNotificationEmail.sendEmailToMultipleRecipient(notificationObj, noticeObj);
					} catch (Exception e) {
						throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.SEND_NOTIFICATION_EAMIL_EXCEPTION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
					}
		
				}
			}
		}

	}
	
	

	/*private void sendNotificationEmail(Plan plan) throws NotificationTypeNotFound, InvalidUserException, NoticeServiceException {
		Issuer issuerObj = issuerService.getIssuerById(plan.getIssuer().getId());
		AccountUser accountUser = new AccountUser();
		Integer userId = representativeRepository.getUserIdFromIssuer(issuerObj.getId());
		Map<String,Object> data = new HashMap<String,Object>();
		if (null != userId && userId != 0) {
			accountUser = userService.findById(userId);
		}
		if (accountUser != null && !StringUtils.isBlank(accountUser.getEmail()) && PlanMgmtConstants.ACTIVE.equalsIgnoreCase(accountUser.getStatus())) {				
			issuerPlanCertificationEmail.setPlanObj(plan);
			if(null != issuerObj){
				issuerPlanCertificationEmail.setIssuerObj(issuerObj);
			}
			issuerPlanCertificationEmail.setUser(accountUser);

			data = issuerPlanCertificationEmail.getSingleData();
			String fileName = PlanMgmtConstants.ISSUER_PLAN_CERTIFICATION + TimeShifterUtil.currentTimeMillis()+PlanMgmtConstants.PDF_EXT;
			if(null != data){
				noticeService.createNotice(PlanMgmtConstants.PLAN_CERTIFIED_NOTIFICATION,  GhixLanguage.US_EN, data, PlanMgmtConstants.PLAN_ECM_PDF_FILE_PATH, fileName, accountUser, null, GhixNoticeCommunicationMethod.Email);
			}
			LOGGER.info("Message stored in inbox successfully");
		} else {
			LOGGER.info("Issuer email address is blank or null");
		}
	}*/

	@RequestMapping(value = "/admin/planmgmt/uploadqdpcertsuppdoc", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	@ResponseBody
	public String uploadQDPSupportingDocument(Model model, @RequestParam(value = "certSuppDoc", required = false) MultipartFile certSuppDoc) {
		String returnString = null;
		try {
			String modifiedFileName = FilenameUtils.getBaseName(certSuppDoc.getOriginalFilename()) + GhixConstants.UNDERSCORE + TimeShifterUtil.currentTimeMillis() 
										+ GhixConstants.DOT + FilenameUtils.getExtension(certSuppDoc.getOriginalFilename());
			String filePath = uploadPath + File.separator + PLAN_SUPPORT_DOC_FOLDER;
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				if (providerNetworkService.uploadFile(filePath, certSuppDoc, modifiedFileName)) {
					returnString = certSuppDoc.getOriginalFilename() + "|" + modifiedFileName;
					LOGGER.info("File uploaded successfully");
				}
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		} catch (Exception e) {
			returnString = PlanMgmtConstants.EMPTY_STRING;
			LOGGER.error("Exception occured while uploading in uploadQDPSupportingDocument: ",e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.UPLOAD_QDP_SUPPORTING_DOCUMENT_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
			
		}
		return returnString;
	}

	/*
	 * @Suneel: Secured the method with permission 
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqdpcertification/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQDPCertification(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			model.addAttribute(PAGE_TITLE, "View QDP Certification");
			model.addAttribute(PLAN_ID, planId.toString());
			loadPlanToDisplay(model);

			List<Map<String, Object>> testData = planMgmtService.loadPlanCertificationHistory(planId, PlanMgmtConstants.ADMIN_ROLE);
			model.addAttribute(DATA, testData);
			model.addAttribute(PAGE_SIZE, GhixConstants.PAGE_SIZE);
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_VIEW_QDPCERTIFICATION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "planmgmt/viewqdpcertification";
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/editqdprates/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN_RATES)
	public String editQDPRates(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			Date sysDate = new TSDate();
			model.addAttribute(PAGE_TITLE, "Edit QDP Rates");
			Plan plan = planMgmtService.getPlan(planId);
			model.addAttribute(ISSUER_NAME, plan.getIssuer().getName());
			model.addAttribute(PLAN_NAME, plan.getName());
			model.addAttribute(PLAN_NUMBER, plan.getIssuerPlanNumber());
			model.addAttribute(PLAN_ID, plan.getId());
			model.addAttribute("currDate", DateUtil.dateToString(sysDate, GhixConstants.REQUIRED_DATE_FORMAT));
			model.addAttribute("ratesFileName", planMgmtService.getActualFileName(plan.getPlanDental().getRateFile()));
			return "planmgmt/editqdprates";
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_EDIT_QDPRATE.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/editqdprates", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN_RATES)
	public String editQDPRates(@ModelAttribute(PLAN) Plan planModel, BindingResult result, Model model, @RequestParam(value = RATE_FILE, required = false) String ratesFile, @RequestParam(value = "effectiveDate", required = false) Date effectiveDate, @RequestParam(value = COMMENTS, required = false) String comments) {
		try {
		if (ratesFile != null && ratesFile.length() > 0) {
			Plan plan = planMgmtService.getPlan(planModel.getId());
			LOGGER.debug("Rates file " + SecurityUtil.sanitizeForLogging(String.valueOf(ratesFile)));
				planMgmtService.savePlanRatesEditMode(plan, uploadPath + File.separator + RATES_FOLDER + File.separator + ratesFile, effectiveDate, comments);
		}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_EDIT_QDPRATE.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(planModel.getId()));
		return "redirect:/admin/planmgmt/viewqdprates/" + encryptedPlanId;
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqdprates/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQDPRates(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			List<Map<String, Object>> logData = planMgmtService.loadPlanRateGroupByEffectiveDate(planId, PlanMgmtConstants.ADMIN_ROLE);
			Plan plan = planMgmtService.getPlan(planId);
			model.addAttribute(PAGE_TITLE, "View QDP Rates");
			model.addAttribute(PLAN_ID, planId);
			model.addAttribute(PLAN_SMALL, plan);
			model.addAttribute("rates", logData);
			model.addAttribute("recordSize", logData.size());
			model.addAttribute(PAGE_SIZE, GhixConstants.PAGE_SIZE);
			String displayEditButton = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_MANAGEQDP_PLANRATES_DISPLAYEDITBUTTON);
			model.addAttribute(DISPLAYEDITBUTTON, displayEditButton);
			return "planmgmt/viewqdprates";
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_VIEW_QDPRATE.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqdpenrollmentavail/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQDPEnrollmentAvail(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			model.addAttribute(PAGE_TITLE, "View QDP Enrollment Availlability");
			model.addAttribute(PLAN_ID, planId.toString());
			loadPlanToDisplay(model);
			List<Map<String, Object>> testData = planMgmtService.loadPlanEnrollmentHistory(planId, PlanMgmtConstants.ADMIN_ROLE);
			String currStatus = null;
			if (testData != null && !testData.isEmpty()) {
				currStatus = (String) testData.get(0).get(PlanMgmtConstants.ENROLLMENT_AVAIL_COL);
			}
			model.addAttribute("currStatus", currStatus);
			model.addAttribute(DATA, testData);
			model.addAttribute(PAGE_SIZE, GhixConstants.PAGE_SIZE);
			return "planmgmt/viewqdpenrollavail";
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_VIEW_QDPENROLLMENT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqdphistory/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQDPHistory(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			model.addAttribute(PAGE_TITLE, "View QDP History");
			model.addAttribute(PLAN_ID, planId.toString());
			loadPlanToDisplay(model);
			List<Map<String, Object>> plansData = planMgmtService.loadPlanHistory(planId);
			model.addAttribute(DATA, plansData);
			model.addAttribute(PAGE_SIZE, GhixConstants.PAGE_SIZE);
			return "planmgmt/viewqdphistory";
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_VIEW_QDPHISTORY.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	@RequestMapping(value = "/admin/planmgmt/dowloadqdpbenefits/{id}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public ModelAndView downloadQDDBenefit(HttpServletResponse response, @PathVariable("id") Integer planId) {
		FileInputStream fis = null;
		CSVWriter writer = null;
		
		try {
			List<PlanDentalBenefit> plansData = planMgmtService.getPlanDentalBenefit(planId);
			if (plansData != null) {
				String constname;
				String name;
				Map<String, String> healthBenefitMap = new HashMap<String, String>();
				
				for(PlanmgmtConfiguration.PlanmgmtConfigurationEnum pcE : PlanmgmtConfiguration.PlanmgmtConfigurationEnum.values()){
					 constname = pcE.name().endsWith(PLANMGMT_DUPLICATE_KEY)?pcE.name().replace(PLANMGMT_DUPLICATE_KEY,PlanMgmtConstants.EMPTY_STRING):pcE.name();
					 name = pcE.getValue();
					if(name.contains(PLANMGMT_DENTALBENEFITS)){
						healthBenefitMap.put(constname,DynamicPropertiesUtil.getPropertyValue(name));
					}
				}
				String fileName = planId + "PD_BENEFIT" + TimeShifterUtil.currentTimeMillis() + ".csv";
				boolean isValidPath = planMgmtUtils.isValidPath(uploadPath);
				if(isValidPath){
					writer = new CSVWriter(new FileWriter(uploadPath + File.separator + fileName), ',');
					writer.close();
					response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
					response.setContentType("application/octet-stream");
					File file = new File(uploadPath + File.separator + fileName);
					fis = new FileInputStream(file);
					FileCopyUtils.copy(fis, response.getOutputStream());
					if(file.exists()){
						file.delete();
					}
					fis.close();
					LOGGER.info("File downloaded successfully!!!");
				}else{
					LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
				}
			}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOAD_QDPBENEFIT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}finally{
			IOUtils.closeQuietly(fis);
			try {
				if(writer != null){
					writer.close();
				}
			} catch (IOException e) {
				//Ignoring
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/admin/planmgmt/updateQDPstatusinbulk", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String loadUpdateStatusInBlock(Model model, HttpServletRequest request, @RequestParam(value = "planIds", required = true) String planIds,
			@RequestParam(value = "certificationStatus", required = false) String certificationStatus,
			@RequestParam(value = "enrollmentavail", required = false) String enrollmentavail, 
			@RequestParam(value = "effectiveDate", required = false) String effectiveDate, 
			@RequestParam(value = "planYearSelected", required = true) String planYearSelected, 
			@RequestParam(value = "isSelectAllPlans", required = true) String isSelectAllPlans,
			@RequestParam(value = "updateType", required = true) String updateType,
			RedirectAttributes redirectAttributes) {
		LOGGER.debug("loadUpdateStatusInBlock start: " +  SecurityUtil.sanitizeForLogging(planYearSelected));
		boolean isPlanDecertified = false;
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT);
			Date currentDate = dateFormat.parse(dateFormat.format(new TSDate()));
			Integer userId = userService.getLoggedInUser().getId();
						
			if (planYearSelected != null && !planYearSelected.equals(PlanMgmtConstants.EMPTY_STRING)) {
				Map<String, Integer> selectedYearMap = new HashMap<String, Integer>();
				selectedYearMap.put(PlanMgmtConstants.PLAN_YEARS_KEY, Integer.parseInt(planYearSelected.toString()));
				model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, selectedYearMap);
				LOGGER.debug("Updating Plans for Year : " +  SecurityUtil.sanitizeForLogging(selectedYearMap.toString()));
				model.addAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, planYearSelected);
				request.getSession().setAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK, planYearSelected);
			}
			
			if(isSelectAllPlans.equals(PlanMgmtConstants.TRUE)){				
				
				boolean isUpdated = false;
				String planNumber = "ALL";
				String planLevel = "ALL";
				String planMarket = "ALL";
				String status = "ALL";
				String verified = "ALL";
							
				int issuerId = 0;				
				int applicableYear = 2014; 
				
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.PLAN_NUMBER))) {
					planNumber = request.getParameter(PlanMgmtConstants.PLAN_NUMBER);
				}			
				
//				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.ISSUERNAME))) {					
//					if(!request.getParameter(PlanMgmtConstants.ISSUERNAME).isEmpty()){
//						String[] issuerDtls = request.getParameter(ISSUERNAME).split("_");
//						issuerId = Integer.parseInt(issuerDtls[0]); 						
//					}
//				}
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.ISSUERID))) {
					issuerId = Integer.parseInt(request.getParameter(PlanMgmtConstants.ISSUERID));
				}
				
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.SELECTED_PLAN_LEVEL))) {
					planLevel = request.getParameter(PlanMgmtConstants.SELECTED_PLAN_LEVEL);
				}
				
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.PLAN_MARKET))) {
					planMarket = request.getParameter(PlanMgmtConstants.PLAN_MARKET);
				}						
								
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATUS))) {
					status = request.getParameter(PlanMgmtConstants.STATUS);
				}
				
				
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.VERIFIED))) {
					verified = request.getParameter(PlanMgmtConstants.VERIFIED);
				}
				
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR))) {
					applicableYear = Integer.parseInt(request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR));
				}
				
				if(updateType.equalsIgnoreCase(PlanMgmtConstants.CERTIFICATION_STATUS_UPDATE)){
					if(StringUtils.isBlank(certificationStatus) || !certificationStatus.matches(PlanMgmtConstants.CERTIFICATIONSTATUS_PATTERN) ){
						throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					}					
					List<String[]> updatedPlanList = planMgmtService.bulkUpdateCertificationStatus(certificationStatus, userService.getLoggedInUser().getId(), 
							PlanMgmtConstants.DENTAL, planNumber, planLevel, planMarket, status, verified, applicableYear, issuerId, stateCode);
					
					/* IF STATE EXCHANGE ID THEN SEND NOTIFICATION EMAIL TO ISSUER REP ON BULK PLAN CERTIFICATION  */
					if(stateCode.equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_ID)){
						planMgmtService.sendNotificationEmailToIssuerRepresentative(updatedPlanList);
					}
					
					Map<String, String> requestData = null;
					List<Map<String, String>> listOfPlans = new ArrayList<Map<String, String>>();
					if(updatedPlanList != null){	
						for (Object obj : updatedPlanList){
							Object[] temp = (Object[])obj;
							
							requestData = new HashMap<String, String>();
							requestData.put(PlanMgmtConstants.AHBX_PLAN_ID, temp[PlanMgmtConstants.TWO].toString());
							requestData.put(PlanMgmtConstants.AHBX_ISSUER_ID, temp[PlanMgmtConstants.ONE].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_LEVEL, temp[PlanMgmtConstants.THREE].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_MARKET, temp[PlanMgmtConstants.FOUR].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_NAME, temp[PlanMgmtConstants.FIVE].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_NETWORK_TYPE, temp[PlanMgmtConstants.SIX].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_RECORD_INDICATOR, (temp[PlanMgmtConstants.SEVEN].toString().equals(temp[PlanMgmtConstants.EIGHT].toString())) ? "N" : "U");
							requestData.put(PlanMgmtConstants.AHBX_PLAN_TYPE, temp[PlanMgmtConstants.NINE].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_START_DATE, PlanMgmtConstants.AHBX_DATE_FORMAT.format(temp[PlanMgmtConstants.TEN]));
							requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_END_DATE, PlanMgmtConstants.AHBX_DATE_FORMAT.format(temp[PlanMgmtConstants.ELEVEN]));
							requestData.put(PlanMgmtConstants.AHBX_PLAN_HIOS_ID, temp[PlanMgmtConstants.TWELVE].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_STATUS, Plan.PlanStatus.CERTIFIED.toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_YEAR, temp[PlanMgmtConstants.THIRTEEN].toString());
							listOfPlans.add(requestData);						
						}
					}	
					
					/* call syncPlanWithAHBXInBulk() service to sync list of plans with AHBX when we do bulk plan certification */
					planMgmtService.syncPlanWithAHBXInBulk(listOfPlans);
					
					isUpdated = true;
				} else if(updateType.equalsIgnoreCase(PlanMgmtConstants.ENROLLMENT_AVBL_UPDATE)){
					if(StringUtils.isBlank(effectiveDate) || StringUtils.isBlank(enrollmentavail) || !enrollmentavail.matches(PlanMgmtConstants.ENROLLMENTAVAILABILITY_PATTERN) ){
						throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					}

					Date enrDate = dateFormat.parse(effectiveDate);
					isUpdated = planMgmtService.bulkUpdateEnrollmentAvbl(enrollmentavail, enrDate, userService.getLoggedInUser().getId(), 
							PlanMgmtConstants.DENTAL, planNumber, planLevel, planMarket, status, verified, applicableYear, issuerId);							
				} else{
					LOGGER.info("Update type not set, update type should be either certification status update or enrollment availability..");
				}
						
				if (isUpdated) {
					LOGGER.info("Bulk Update successfull");
					redirectAttributes.addAttribute(PlanMgmtConstants.SUCCESS_STR, PlanMgmtConstants.TRUE);
				} else {
					LOGGER.info("Bulk Update failed");
					redirectAttributes.addAttribute(PlanMgmtConstants.SUCCESS_STR, PlanMgmtConstants.FALSE);
				}
			}
			else {
				String[] splittedPlanIds = planIds.split(",");
				List<Plan> plansToUpdate = new ArrayList<Plan>();
				if(updateType.equalsIgnoreCase(PlanMgmtConstants.CERTIFICATION_STATUS_UPDATE)){
					List<String> rejectedPlans = new ArrayList<String>();
				
					for (int i = 0; i < splittedPlanIds.length; i++) {
						String planId = splittedPlanIds[i];
						Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
						String issuerStatus = issuerService.getIssuerStatus(plan.getIssuer().getId());
						if((Plan.PlanStatus.CERTIFIED.name().equals(certificationStatus)) && !(Issuer.certification_status.CERTIFIED.name().equals(issuerStatus))){
							rejectedPlans.add(plan.getIssuerPlanNumber());
						}else{
							plansToUpdate.add(plan);
						}
										
					}
					if(!rejectedPlans.isEmpty()){
						redirectAttributes.addAttribute("rejectedPIds",rejectedPlans.toString());
						redirectAttributes.addAttribute("error",PlanMgmtConstants.TRUE);
						setRedirectAttributes(request,redirectAttributes);
						return PlanMgmtConstants.MANAGE_QDP_PATH;
					}

					String pendingDecertifyPlans  = planMgmtService.findDeCertifiedPlans(plansToUpdate);
					if(StringUtils.isBlank(pendingDecertifyPlans)){
						boolean isUpdated = false;
						boolean doUpdatePlanCertificationStatus = false;
						Map<String, String> requestData = null;
						String planlevel = null;
						List<Map<String, String>> listOfPlans = new ArrayList<Map<String, String>>();
						
						for(Plan plan: plansToUpdate){
							isUpdated = false;
							doUpdatePlanCertificationStatus = false;
							isPlanDecertified = false;
							
							isPlanDecertified = planMgmtUtils.checkIsPlanDecertified(plan.getStatus(), plan.getDeCertificationEffDate());
							if(!plan.getStatus().equalsIgnoreCase(certificationStatus) ){
								doUpdatePlanCertificationStatus = true;
							}
							
							if(doUpdatePlanCertificationStatus && !(isPlanDecertified)){
								plan.setStatus(certificationStatus);
								plan.setLastUpdatedBy(userId);
								// when plan status set to CERTIFIED then set issuer verification status as PENDING
								if(certificationStatus.equalsIgnoreCase(PlanMgmtConstants.CERTIFIED)){
									plan.setIssuerVerificationStatus(Plan.IssuerVerificationStatus.PENDING.toString());
									plan.setCertifiedby(userId.toString());
									plan.setCertifiedOn(new TSDate());
								}
								isUpdated = planMgmtService.modifyPlan(plan); 
							}
							if(isUpdated && doUpdatePlanCertificationStatus) {
								// Only for ID state exchange, send notification to issuer representatives when plan certification status set as CERTIFIED 
								if(stateCode.equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_ID) && certificationStatus.equalsIgnoreCase(Plan.PlanStatus.CERTIFIED.toString())){
									planMgmtService.sendNotificationEmailToIssuerRepresentative(plan,Plan.PlanStatus.CERTIFIED.toString());
								}
								
								// prepare plan data for sync with AHBX - START
								if(plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
									planlevel = plan.getPlanHealth().getPlanLevel();
								}else if(plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
									planlevel = plan.getPlanDental().getPlanLevel();
								}
								requestData = new HashMap<String, String>();
								requestData.put(PlanMgmtConstants.AHBX_PLAN_ID, String.valueOf(plan.getId()));
								requestData.put(PlanMgmtConstants.AHBX_ISSUER_ID, plan.getIssuer().getHiosIssuerId());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_LEVEL, planlevel);
								requestData.put(PlanMgmtConstants.AHBX_PLAN_MARKET, plan.getMarket());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_NAME, plan.getName());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_NETWORK_TYPE, plan.getNetworkType());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_RECORD_INDICATOR, (plan.getLastUpdateTimestamp().equals(plan.getCreationTimestamp())) ? "N" : "U");
								requestData.put(PlanMgmtConstants.AHBX_PLAN_TYPE, plan.getInsuranceType());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_START_DATE, PlanMgmtConstants.AHBX_DATE_FORMAT.format(plan.getStartDate()));
								requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_END_DATE, PlanMgmtConstants.AHBX_DATE_FORMAT.format(plan.getEndDate()));
								requestData.put(PlanMgmtConstants.AHBX_PLAN_HIOS_ID, plan.getIssuerPlanNumber());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_STATUS, plan.getStatus());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_YEAR, Integer.toString(plan.getApplicableYear()));
								listOfPlans.add(requestData);
								// prepare plan data for sync with AHBX - END
							}
						}
						
						/* call syncPlanWithAHBXInBulk() service to sync list of plans with AHBX when we do bulk plan certification */
						planMgmtService.syncPlanWithAHBXInBulk(listOfPlans); 
						
					}else if(StringUtils.isNotBlank(pendingDecertifyPlans)){
						redirectAttributes.addFlashAttribute(PlanMgmtConstants.PENDING_DECERTIFICATION, pendingDecertifyPlans);
						setRedirectAttributes(request, redirectAttributes);
						return PlanMgmtConstants.MANAGE_QDP_PATH;
					}
				} else if(updateType.equalsIgnoreCase(PlanMgmtConstants.ENROLLMENT_AVBL_UPDATE)){
					for (int i = 0; i < splittedPlanIds.length; i++) {
						String planId = splittedPlanIds[i];
						Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
						plansToUpdate.add(plan);							
					}
					
					String enrollmentAvailPlans  = planMgmtService.findEnrollmentAvailablePlans(plansToUpdate);
					Date enrDate = null;
					if(StringUtils.isBlank(enrollmentAvailPlans)){
						boolean isUpdated = false;						
						boolean isEnrollmentUpdated = false;
						
						for(Plan plan: plansToUpdate){
							isUpdated = false;
							isEnrollmentUpdated = false;
							isPlanDecertified = false;
							
							isPlanDecertified = planMgmtUtils.checkIsPlanDecertified(plan.getStatus(), plan.getDeCertificationEffDate());
							if(!plan.getEnrollmentAvail().equalsIgnoreCase(enrollmentavail) || null == plan.getFutureErlAvailEffDate()  || !plan.getFutureErlAvailEffDate().equals(effectiveDate)){
								isEnrollmentUpdated = true;
							}
							if(isEnrollmentUpdated && !(isPlanDecertified)){
								enrDate = dateFormat.parse(effectiveDate);
								if(enrDate.equals(currentDate)){
									plan.setEnrollmentAvail(enrollmentavail);
									plan.setEnrollmentAvailEffDate(enrDate);
									plan.setFutureEnrollmentAvail(null);
									plan.setFutureErlAvailEffDate(null);
								} else{
									plan.setFutureEnrollmentAvail(enrollmentavail);
									plan.setFutureErlAvailEffDate(enrDate);
								}
								plan.setLastUpdatedBy(userId);								
								isUpdated = planMgmtService.modifyPlan(plan); 
							}							
							// send enrollment availability notification to issuer rep only for ID state
							if(isUpdated && isEnrollmentUpdated && stateCode.equalsIgnoreCase("ID")){
								enrollmentAvailabilityEmailUtil.sendNotificationEmailToIssuerRepresentativeForEnrollment(plan);
							}
						}
					}else if(StringUtils.isNotBlank(enrollmentAvailPlans)){
						redirectAttributes.addFlashAttribute(PlanMgmtConstants.ENROLL_AVAIL, enrollmentAvailPlans);
						setRedirectAttributes(request, redirectAttributes);
						return PlanMgmtConstants.MANAGE_QDP_PATH;
					}
				}
				redirectAttributes.addAttribute(PlanMgmtConstants.SUCCESS_STR, PlanMgmtConstants.TRUE);
			}		
			LOGGER.info("loadUpdateStatusInBlock excuted successfully !!");
			request.getSession().setAttribute(PlanMgmtConstants.SORT_BY, "lastUpdateTimestamp");			
			setRedirectAttributes(request, redirectAttributes);

		} catch (Exception e) {
			LOGGER.error("Exception Occured ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.BULK_PLAN_UPDATE, null,
					ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return PlanMgmtConstants.MANAGE_QDP_PATH;
	}
	
	@RequestMapping(value = "/admin/planmgmt/qdp/updateSelectAllFlag", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'SPL_MANAGE_PLAN')")
	@ResponseBody
	public String setUpdateSelectAllFlag(HttpServletRequest request, Model model, RedirectAttributes redirectAttrs,
								@RequestParam(value = PlanMgmtConstants.SELECTALL_QDP_PLANS, required = false) String isSelectAllPlansOnAllPagesForQHD, HttpServletResponse response) {
		String successString = null;
		try {
			HttpSession session = request.getSession();
			LOGGER.debug("isSelectAllPlansOnAllPagesForAME: " +  SecurityUtil.sanitizeForLogging(isSelectAllPlansOnAllPagesForQHD));
			if(isSelectAllPlansOnAllPagesForQHD.equalsIgnoreCase("true")){
			session.setAttribute(PlanMgmtConstants.SELECTALL_QDP_PLANS,isSelectAllPlansOnAllPagesForQHD);
			}else{
				session.removeAttribute(PlanMgmtConstants.SELECTALL_QDP_PLANS);
			}
			successString = isSelectAllPlansOnAllPagesForQHD;
		} catch (Exception e) {
			successString = "fail";
			LOGGER.error("Exception Occurred in setUpdateSelectAllFlag: ", e);
		}
		return successString;
	}
	
	private void setRedirectAttributes(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		if (StringUtils.isNotBlank(request.getParameter(VERIFIED))) {
			redirectAttributes.addAttribute(VERIFIED, request.getParameter(VERIFIED));
		}
		if (StringUtils.isNotBlank(request.getParameter(STATUS))) {
			redirectAttributes.addAttribute(STATUS, request.getParameter(STATUS));
		}
		if (StringUtils.isNotBlank(request.getParameter(MARKET))) {
			redirectAttributes.addAttribute(MARKET, request.getParameter(MARKET));
		}
		if (StringUtils.isNotBlank(request.getParameter(ISSUERNAME))) {
			redirectAttributes.addAttribute(ISSUERNAME, request.getParameter(ISSUERNAME));
		}
		if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.ISSUERID))) {
			redirectAttributes.addAttribute(PlanMgmtConstants.ISSUERID, request.getParameter(PlanMgmtConstants.ISSUERID));
		}
		if (StringUtils.isNotBlank(request.getParameter(PLAN_NUMBER))) {
			redirectAttributes.addAttribute(PLAN_NUMBER, request.getParameter(PLAN_NUMBER));
		}
		if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATES))) {
			redirectAttributes.addAttribute(PlanMgmtConstants.STATES, request.getParameter(PlanMgmtConstants.STATES));
		}
		if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.SELECTED_PLAN_LEVEL))) {
			redirectAttributes.addAttribute(PlanMgmtConstants.SELECTED_PLAN_LEVEL, request.getParameter(PlanMgmtConstants.SELECTED_PLAN_LEVEL));
			redirectAttributes.addAttribute(PlanMgmtConstants.PLAN_LEVEL, request.getParameter(PlanMgmtConstants.SELECTED_PLAN_LEVEL));
		}
	
		HttpSession session = request.getSession();
		if(session.getAttribute(PlanMgmtConstants.SELECTALL_QDP_PLANS) !=  null){
			session.removeAttribute(PlanMgmtConstants.SELECTALL_QDP_PLANS);
		}
	}
	
	
	@RequestMapping(value = "/admin/planmgmt/qdp/terminateEnrollment", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'SPL_MANAGE_PLAN')")
	@ResponseBody
	public String terminateEnrollmentAjaxCall(HttpServletRequest request, Model model, RedirectAttributes redirectAttrs,
			@RequestParam(value = "planId", required = false) String encPlanId,  HttpServletResponse response) {
		String successString = null;
		Plan plan = null;
		String terminateEnrollmentResponse = Plan.TerminateEnrollmentStatusResponse.FAILURE.toString();
		String decertificationEffiectiveDate = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT);
			String decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			if(null != decryptedPlanId){
				plan = planMgmtService.getPlan(Integer.parseInt(decryptedPlanId));
			}
			
			if(null != plan){
				if(null != plan.getDeCertificationEffDate()){
					decertificationEffiectiveDate = DateUtil.dateToString(plan.getDeCertificationEffDate(), GhixConstants.REQUIRED_DATE_FORMAT);
				}
				if(StringUtils.isNotBlank(decertificationEffiectiveDate)){
					Date deCertiyDate = formatter.parse(decertificationEffiectiveDate);
					String terminateEnrollmentConfiguration = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.TERMINATE_ENROLLMENT_FEATURE);
					if(deCertiyDate.after(new TSDate())){
						if(StringUtils.isNotEmpty(terminateEnrollmentConfiguration) && PlanMgmtConstants.ON.equalsIgnoreCase(terminateEnrollmentConfiguration)){
							if(null != plan.getIssuerPlanNumber() && PlanMgmtConstants.ZERO != plan.getApplicableYear() && null != decertificationEffiectiveDate){
								terminateEnrollmentResponse = planMgmtRestUtil.terminateEnrollmentByPlanId(plan.getIssuerPlanNumber(), plan.getApplicableYear(), decertificationEffiectiveDate);
								if(LOGGER.isDebugEnabled()){
									LOGGER.debug("terminateEnrollmentResponse: " + SecurityUtil.sanitizeForLogging(String.valueOf(terminateEnrollmentResponse)));
								}
							}
						}
						if(StringUtils.isNotEmpty(terminateEnrollmentResponse) && Plan.TerminateEnrollmentStatusResponse.SUCCESS.toString().equalsIgnoreCase(terminateEnrollmentResponse)){
							successString = Plan.TerminateEnrollmentStatusResponse.SUCCESS.toString();
						}else{
							successString = Plan.TerminateEnrollmentStatusResponse.FAILURE.toString();
						}
						
						plan.setTerminateEnrollmentStatus(successString);
						boolean updateResponse = planMgmtService.modifyPlan(plan);
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug("Plan Terminated Successfully " + updateResponse);
						}
					}
				}
			}
		} catch (Exception e) {
			successString = Plan.TerminateEnrollmentStatusResponse.FAILURE.toString();
			LOGGER.error("Exception Occurred in terminateEnrollmentAjaxCall: ", e);
		}
		return successString;
	}
}
