package com.getinsured.hix.planmgmt.provider.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Provider;

public interface IProviderRepository extends JpaRepository<Provider, Integer> {
	
	@Query("SELECT p FROM Provider p where LOWER(p.type) = LOWER(:type) AND LOWER(p.name) = LOWER(:name) AND LOWER(p.datasourceRefId) = LOWER(:datasourceRefId)")
    Provider getProviderByTypeNameRefId(@Param("type") String type, @Param("name") String name, @Param("datasourceRefId") String datasourceRefId);
	
	@Query("SELECT p.groupKey FROM Provider p WHERE LOWER(p.groupKey) = LOWER(:groupKey) AND p.type = :providerType")
	String getProviderByGroupKey(@Param("groupKey") String groupKey, @Param("providerType") Provider.ProviderType providerType);
}
