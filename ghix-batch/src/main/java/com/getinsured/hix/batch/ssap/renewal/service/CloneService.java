package com.getinsured.hix.batch.ssap.renewal.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.batch.core.UnexpectedJobExecutionException;

import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * Interface is used to provide services for Clone.
 */
public interface CloneService {

	String AUTO_ENROLL_JOB_NAME = "ssapRedeterminationCloneJob";

	void saveAndThrowsErrorLog(String errorMessage) throws UnexpectedJobExecutionException;

	Integer logToGIMonitor(Exception e, int errorCode, String caseNumber);

	List<Long> getEnrolledSsapApplicationsByCoverageYear(long coverageYear, List<String> applicationStatusList, Long batchSize);

	List<Long> getOTRApplicationsByCoverageYear(long coverageYear, List<String> applicationStatusList, Long batchSize);

	List<SsapApplication> getCloneSsapApplListByIds(List<Long> applicationIdList);

	void logData(List<Map<String, String>> jsonResultSuccess, List<Map<String, String>> jsonResultFailure, Map<String, Long> summary);

	Map<String, String> cloneSsap(SsapApplication ssapApplication, AtomicLong renewalYear, AtomicBoolean cloneToNFA, AtomicBoolean isCloneProgramEligibility);

	List<Long> getEnrolledSsapApplicationsIdByCoverageYear(long coverageYear, List<String> applicationStatusList,
			Long batchSize, String renewalStatus);

}
