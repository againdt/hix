
const AUTHORIZED_REPRESENTATIVE = {
    'signatureisProofIndicator': null,
    'phone': [
        {
            'phoneExtension': null,
            'phoneNumber': null,
            'phoneType': 'CELL'
        },
        {
            'phoneExtension': null,
            'phoneNumber': null,
            'phoneType': 'HOME'
        },
        {
            'phoneExtension': null,
            'phoneNumber': null,
            'phoneType': 'WORK'
        }
    ],
    'partOfOrganizationIndicator': null,
    'address': {
        'postalCode': null,
        'county': null,
        'primaryAddressCountyFipsCode': null,
        'streetAddress1': null,
        'streetAddress2': null,
        'state': null,
        'city': null,
        'countyCode': null,
        'addressId': 0
    },
    'name': {
        'middleName': null,
        'lastName': null,
        'firstName': null,
        'suffix': null
    },
    'emailAddress': null,
    'organizationId': null,
    'companyName': null
};

const CITIZENSHIP_DOCUMENT = {
    sevisId: null,
    foreignPassportOrDocumentNumber: null,
    alienNumber: null,
    nameOnDocument: {
        middleName: null,
        lastName: null,
        firstName: null,
        suffix: null
    },
    documentDescription: null,
    nameSameOnDocumentIndicator: null,
    visaNumber: null,
    documentExpirationDate: null,
    i94Number: null,
    foreignPassportCountryOfIssuance: null,
    cardNumber: null
};

const CITIZENSHIP_STATUS = {
    'honorablyDischargedOrActiveDutyMilitaryMemberIndicator' : null,
    'citizenshipStatusIndicator' : null,
    'eligibleImmigrationDocumentType' : [{
        'UnexpiredForeignPassportIndicator': false,
        'TemporaryI551StampIndicator': false,
        'I551Indicator': false,
        'MachineReadableVisaIndicator': false,
        'OtherDocumentTypeIndicator': false,
        'I94InPassportIndicator': false,
        'I571Indicator': false,
        'I766Indicator': false,
        'I20Indicator': false,
        'I94Indicator': false,
        'I797Indicator': false,
        'I327Indicator': false,
        'DS2019Indicator': false,
        'noneOfThese': false
    }],
    'naturalizationCertificateIndicator' : false,
    'citizenshipManualVerificationStatus' : false,
    'citizenshipAsAttestedIndicator' : false,
    'eligibleImmigrationStatusIndicator' : false,
    'citizenshipDocument' : {
        'foreignPassportOrDocumentNumber' : null,
        'sevisId' : null,
        'alienNumber' : null,
        'nameOnDocument' : {
            'middleName' : null,
            'lastName' : null,
            'firstName' : null,
            'suffix' : null
        },
        'documentDescription' : null,
        'nameSameOnDocumentIndicator' : null,
        'visaNumber' : null,
        'documentExpirationDate' : null,
        'i94Number' : null,
        'foreignPassportCountryOfIssuance' : null,
        'cardNumber' : null
    },
    'lawfulPresenceIndicator' : false,
    'naturalizationCertificateAlienNumber' : null,
    'eligibleImmigrationDocumentSelected' : null,
    'otherImmigrationDocumentType' : {
        'cubanHaitianEntrantIndicator' : false,
        'americanSamoanIndicator' : false,
        'stayOfRemovalIndicator': false,
        'orreligibilityLetterIndicator' : false,
        'orrcertificationIndicator' : false,
        'withholdingOfRemovalIndicator': false,
        'domesticViolence': false,
        'indianOfUsOrCanada': false,
        'noneOfThese': false
    },
    'naturalizedCitizenshipIndicator' : null,
    'livedIntheUSSince1996Indicator' : null,
    'naturalizationCertificateNaturalizationNumber' : null
};

const HOUSEHOLD_COMPOSITION = {
    'claimerIds' : [],
    'taxFilingStatus': 'UNSPECIFIED',
    'taxRelationship': 'UNSPECIFIED'
};

const HOUSEHOLD_CONTACT = {
    'homeAddressIndicator' : false,
    'phone' : {
        'phoneExtension' : null,
        'phoneNumber' : null,
        'phoneType' : null
    },
    'homeAddress' : {
        'postalCode' : null,
        'county' : null,
        'primaryAddressCountyFipsCode' : null,
        'streetAddress1' : null,
        'streetAddress2' : null,
        'state' : null,
        'city' : null,
        'countyCode' : null
    },
    'stateResidencyProof' : null,
    'otherPhone' : {
        'phoneExtension' : null,
        'phoneNumber' : null,
        'phoneType' : null
    },
    'contactPreferences' : {
        'emailAddress' : null,
        'preferredSpokenLanguage' : null,
        'preferredContactMethod' : null,
        'preferredWrittenLanguage' : null
    },
    'mailingAddress' : {
        'postalCode' : null,
        'county' : null,
        'primaryAddressCountyFipsCode' : null,
        'streetAddress1' : null,
        'streetAddress2' : null,
        'state' : null,
        'city' : null,
        'countyCode' : null
    },
    'mailingAddressSameAsHomeAddressIndicator' : false
};

const HOUSEHOLD_MEMBER = {
    'seeksQhp': true,
    'incomes': [],
    'expenses' : [],
    'annualTaxIncome': {
        'reportedIncome': 0,
        'projectedIncome': 0,
    },
    'flagBucket': {
        'taxHouseholdCompositionFlags': 604587154,
        'taxHouseholdCompositionProgressFlags': 21650,
        'annualTaxIncomeFlags': 18
    },
    'taxHouseholdComposition': {
        'claimerIds': [ ],
    },
    'detailedIncome' : {
        'partnershipAndSwarpIncomeIncomeProof' : null,
        'capitalGains' : {
            'annualNetCapitalGains' : null
        },
        'unemploymentBenefit' : {
            'incomeFrequency' : null,
            'stateGovernmentName' : null,
            'benefitAmount' : null,
            'unemploymentDate' : null
        },
        'annuityIncomeProof' : null,
        'scholarshipIncomeProof' : null,
        'otherIncome' : [ ],
        'receiveMAthrough1619' : null,
        'taxExemptedIncomeProof' : null,
        'wageIncomeProof' : null,
        'dividendsIncomeProof' : null,
        'farmFishingIncome' : {
            'incomeFrequency' : null,
            'netIncomeAmount' : null
        },
        'rentalRoyaltyIncomeIndicator' : false,
        'unemploymentBenefitIndicator' : false,
        'capitalGainsIndicator' : false,
        'jobIncome' : [ ],
        'alimonyReceivedIndicator' : false,
        'unemploymentCompensationIncomeProof' : null,
        'selfEmploymentTax' : {
            'incomeAmount' : null
        },
        'studentLoanInterest' : {
            'incomeAmount' : null
        },
        'selfEmploymentIncome' : {
            'monthlyNetIncome' : null,
            'typeOfWork' : null
        },
        'businessIncomeProof' : null,
        'sellingBusinessProperty' : {
            'incomeAmount' : null
        },
        'retirementOrPension' : {
            'taxableAmount' : null,
            'incomeFrequency' : null
        },
        'royaltiesIncomeProof' : null,
        'farmFishingIncomeIndictor' : false,
        'investmentIncome' : {
            'incomeAmount' : null,
            'incomeFrequency' : null
        },
        'farmIncom' : {
            'incomeAmount' : null
        },
        'deductions' : {
            'alimonyDeductionAmount' : null,
            'otherDeductionAmount' : null,
            'alimonyDeductionFrequency' : null,
            'studentLoanDeductionFrequency' : null,
            'deductionFrequency' : null,
            'studentLoanDeductionAmount' : null,
            'deductionType' : [ ],
            'otherDeductionFrequency' : null,
            'deductionAmount' : null
        },
        'rentalRoyaltyIncome' : {
            'incomeFrequency' : null,
            'netIncomeAmount' : null
        },
        'receiveMAthroughSSI' : null,
        'foreignEarnedIncomeProof' : null,
        'socialSecurityBenefit' : {
            'incomeFrequency' : null,
            'benefitAmount' : null
        },
        'foreignEarnedIncome' : {
            'incomeAmount' : null
        },
        'childNaTribe' : null,
        'sellingBusinessPropertyIncomeProof' : null,
        'alimonyReceived' : {
            'amountReceived' : null,
            'incomeFrequency' : null
        },
        'selfEmploymentTaxProof' : null,
        'partnershipsCorporationsIncome' : {
            'incomeAmount' : null
        },
        'studentLoanInterestProof' : null,
        'farmIncomeProof' : null,
        'retirementOrPensionIndicator' : false,
        'investmentIncomeIndicator' : false,
        'socialSecurityBenefitIndicator' : false,
        'rentalRealEstateIncome' : {
            'incomeAmount' : null
        },
        'otherIncomeIndicator' : false,
        'pensionIncomeProof' : null,
        'socialSecurityBenefitProof' : null,
        'jobIncomeIndicator' : false,
        'businessIncome' : {
            'incomeAmount' : null
        },
        'taxableInterestIncomeProof' : null,
        'selfEmploymentIncomeIndicator' : false,
        'rentalRealEstateIncomeProof' : null,
        'discrepancies' : {
            'hasWageOrSalaryBeenCutIndicator' : false,
            'explanationForJobIncomeDiscrepancy' : null,
            'didPersonEverWorkForEmployerIndicator' : false,
            'stopWorkingAtEmployerIndicator' : false,
            'hasHoursDecreasedWithEmployerIndicator' : false,
            'explanationForDependantDiscrepancy' : null,
            'seasonalWorkerIndicator' : false,
            'hasChangedJobsIndicator' : false
        }
    },
    'dateOfBirth' : null,
    'parentCaretakerRelatives' : {
        'relationship' : null,
        'dateOfBirth' : null,
        'mainCaretakerOfChildIndicator' : false,
        'name' : {
            'middleName' : null,
            'lastName' : null,
            'firstName' : null,
            'suffix' : null
        },
        'liveWithAdoptiveParentsIndicator' : false,
        'personId' : [ ],
        'anotherChildIndicator' : false
    },
    'specialCircumstances' : {
        'ageWhenLeftFosterCare' : null,
        'everInFosterCareIndicator' : false,
        'metFosterCareCondition': false,
        'fosterChild' : null,
        'numberBabiesExpectedInPregnancy' : null,
        'pregnantIndicator' : false,
        'gettingHealthCareThroughStateMedicaidIndicator' : false,
        'fosterCareState' : null,
        'tribalManualVerificationIndicator' : false,
        'americanIndianAlaskaNativeIndicator' : false
    },
    'livesWithHouseholdContactIndicator' : true,
    'socialSecurityCard' : {
        'middleNameOnSSNCard' : null,
        'deathManualVerificationIndicator' : false,
        'socialSecurityNumber' : null,
        'suffixOnSSNCard' : null,
        'individualMandateExceptionIndicator' : false,
        'ssnManualVerificationIndicator' : false,
        'deathIndicatorAsAttested' : false,
        'nameSameOnSSNCardIndicator' : null,
        'deathIndicator' : false,
        'firstNameOnSSNCard' : null,
        'socialSecurityCardHolderIndicator' : null,
        'useSelfAttestedDeathIndicator' : false,
        'lastNameOnSSNCard' : null,
        'reasonableExplanationForNoSSN' : null
    },
    'strikerIndicator' : false,
    'birthCertificateType' : null,
    'citizenshipImmigrationStatus' : {
        'honorablyDischargedOrActiveDutyMilitaryMemberIndicator' : null,
        'citizenshipStatusIndicator' : null,
        'eligibleImmigrationDocumentType' : [{
            'unexpiredForeignPassportIndicator': false,
            'temporaryI551StampIndicator': false,
            'i551Indicator': false,
            'machineReadableVisaIndicator': false,
            'otherDocumentTypeIndicator': false,
            'i94InPassportIndicator': false,
            'i571Indicator': false,
            'i766Indicator': false,
            'i20Indicator': false,
            'i94Indicator': false,
            'i797Indicator': false,
            'i327Indicator': false,
            'ds2019Indicator': false,
            'noneOfThese': false
        }],
        'naturalizationCertificateIndicator' : false,
        'citizenshipManualVerificationStatus' : false,
        'citizenshipAsAttestedIndicator' : false,
        'eligibleImmigrationStatusIndicator' : false,
        'citizenshipDocument' : {
            'foreignPassportOrDocumentNumber' : null,
            'sevisId' : null,
            'alienNumber' : null,
            'nameOnDocument' : {
                'middleName' : null,
                'lastName' : null,
                'firstName' : null,
                'suffix' : null
            },
            'documentDescription' : null,
            'nameSameOnDocumentIndicator' : null,
            'visaNumber' : null,
            'documentExpirationDate' : null,
            'i94Number' : null,
            'foreignPassportCountryOfIssuance' : null,
            'cardNumber' : null
        },
        'lawfulPresenceIndicator' : false,
        'naturalizationCertificateAlienNumber' : null,
        'eligibleImmigrationDocumentSelected' : null,
        'otherImmigrationDocumentType' : {
            'cubanHaitianEntrantIndicator' : false,
            'stayOfRemovalIndicator' : false,
            'americanSamoanIndicator' : false,
            'withholdingOfRemovalIndicator' : false,
            'orreligibilityLetterIndicator' : false,
            'orrcertificationIndicator' : false,
            'domesticViolence': false,
            'indianOfUsOrCanada': false,
            'noneOfThese': false
        },
        'naturalizedCitizenshipIndicator' : null,
        'livedIntheUSSince1996Indicator' : null,
        'naturalizationCertificateNaturalizationNumber' : null
    },
    'medicalExpense' : null,
    'specialEnrollmentPeriod' : {
        'birthOrAdoptionEventIndicator' : false
    },
    'marriedIndicator' : false,
    'bloodRelationship' : [ ],
    'drugFellowIndicator' : false,
    'livingArrangementIndicator' : false,
    'taxFiler' : {
        'spouseHouseholdMemberId' : null,
        'claimingDependantsOnFTRIndicator' : false,
        'taxFilerDependants' : [ ],
        'liveWithSpouseIndicator' : false
    },
    'shelterAndUtilityExpense' : null,
    'tobaccoUserIndicator' : false,
    'dependentCareExpense' : null,
    'residencyVerificationIndicator' : false,
    'taxFilerDependant' : {
        'dependantHouseholdMemeberId' : null,
        'taxFilerDependantIndicator' : false
    },
    'name' : {
        'middleName' : null,
        'lastName' : null,
        'firstName' : null,
        'suffix' : null
    },
    'ethnicityAndRace' : {
        'ethnicity' : [ ],
        'race' : [ ],
        'hispanicLatinoSpanishOriginIndicator' : null
    },
    'personId' : null,
    'householdId': 0,
    'addressId': 1,
    'gender' : null,
    'householdContactIndicator' : false,
    'americanIndianAlaskaNative' : {
        'tribeName' : null,
        'tribeFullName' : null,
        'state' : null,
        'stateFullName' : null,
        'memberOfFederallyRecognizedTribeIndicator' : null
    },
    'householdContact' : {
        'homeAddressIndicator' : false,
        'phone' : {
            'phoneExtension' : null,
            'phoneNumber' : null,
            'phoneType' : null
        },
        'homeAddress' : {
            'postalCode' : null,
            'county' : null,
            'primaryAddressCountyFipsCode' : null,
            'streetAddress1' : null,
            'streetAddress2' : null,
            'state' : null,
            'city' : null,
            'countyCode' : null
        },
        'stateResidencyProof' : null,
        'otherPhone' : {
            'phoneExtension' : null,
            'phoneNumber' : null,
            'phoneType' : null
        },
        'contactPreferences' : {
            'emailAddress' : null,
            'preferredSpokenLanguage' : null,
            'preferredContactMethod' : null,
            'preferredWrittenLanguage' : null
        },
        'mailingAddress' : {
            'postalCode' : null,
            'county' : null,
            'primaryAddressCountyFipsCode' : null,
            'streetAddress1' : null,
            'streetAddress2' : null,
            'state' : null,
            'city' : null,
            'countyCode' : null
        },
        'mailingAddressSameAsHomeAddressIndicator' : false
    },
    'applicantGuid' : null,
    'applicantPersonType': 'DEP',
    'applyingForCoverageIndicator' : false,
    'planToFileFTRJontlyIndicator' : false,
    'livesAtOtherAddressIndicator' : false,
    'employerSponsoredCoverage': [ ], // is it real destination?
    'healthCoverage' : {
        'chipInsurance' : {
            'reasonInsuranceEndedOther' : null,
            'insuranceEndedDuringWaitingPeriodIndicator' : false
        },
        'haveBeenUninsuredInLast6MonthsIndicator' : false,
        'currentEmployer' : [ ],
        'formerEmployer' : [ ],
        'employerSponsoredCoverage': [ ], // is it real destination?
        'primaryCarePhysicianName' : null,
        'otherInsuranceIndicator' : false,
        'currentlyEnrolledInCobraIndicator' : false,
        'currentlyHasHealthInsuranceIndicator' : false,
        'employerWillOfferInsuranceIndicator' : null,
        'currentlyEnrolledInRetireePlanIndicator' : false,
        'currentOtherInsurance': {
            'otherStateOrFederalMedicaidProgramName': null,
            'otherStateOrFederalCHIPProgramName':null,
            'otherStateOrFederalPrograms': [],
            'currentlyEnrolledInVeteransProgram': null,
            'hasNonESI': null,
        },
        'currentlyEnrolledInVeteransProgramIndicator' : false,
        'hasPrimaryCarePhysicianIndicator' : false,
        'employerWillOfferInsuranceCoverageStartDate' : null,
        'willBeEnrolledInCobraIndicator' : false,
        'willBeEnrolledInVeteransProgramIndicator' : false,
        'medicaidInsurance' : {
            'requestHelpPayingMedicalBillsLast3MonthsIndicator' : false
        },
        'isOfferedHealthCoverageThroughJobIndicator' : false,
        'willBeEnrolledInRetireePlanIndicator' : false,
        'employerWillOfferInsuranceStartDate' : null
    },
    'planToFileFTRIndicator' : false,
    'otherAddress' : {
        'address' : {
            'postalCode' : null,
            'county' : null,
            'primaryAddressCountyFipsCode' : null,
            'streetAddress1' : null,
            'streetAddress2' : null,
            'state' : null,
            'city' : null,
            'countyCode' : null
        },
        'livingOutsideofStateTemporarilyAddress' : {
            'postalCode' : null,
            'county' : null,
            'primaryAddressCountyFipsCode' : null,
            'streetAddress1' : null,
            'streetAddress2' : null,
            'state' : null,
            'city' : null,
            'countyCode' : null
        },
        'livingOutsideofStateTemporarilyIndicator' : null
    },
    'disabilityIndicator' : false,
    'heatingCoolingindicator' : false,
    'incarcerationStatus' : {
        'incarcerationManualVerificationIndicator' : false,
        'incarcerationStatusIndicator' : false,
        'useSelfAttestedIncarceration' : false,
        'incarcerationPendingDispositionIndicator' : null,
        'incarcerationAsAttestedIndicator' : false
    },
    'infantIndicator' : false,
    'childSupportExpense' : null,
    'expeditedIncome' : {
        'irsReportedAnnualIncome' : null,
        'expectedAnnualIncomeUnknownIndicator' : false,
        'irsIncomeManualVerificationIndicator' : false,
        'expectedAnnualIncome' : null
    },
    'migrantFarmWorker' : null,
    'status' : 'ADD_NEW',
    'under26Indicator' : false,
    'externalId' : null,
    'requestRef' : null,
    'hardshipExempt' : null,
    'ecnNumber' : null,
    'ssiindicator' : false,
    'ipvindicator' : false,
    'pidverificationIndicator' : false,
    'pidindicator' : false
};

const OTHER_ADDRESS = {
    'address': {
        'postalCode': null,
        'county': null,
        'primaryAddressCountyFipsCode': null,
        'streetAddress1': null,
        'streetAddress2': null,
        'state': null,
        'city': null,
        'countyCode': null
    },
    'livingOutsideofStateTemporarilyAddress': {
        'postalCode': null,
        'county': null,
        'primaryAddressCountyFipsCode': null,
        'streetAddress1': null,
        'streetAddress2': null,
        'state': null,
        'city': null,
        'countyCode': null
    },
    'livingOutsideofStateTemporarilyIndicator': null
};

const SOCIAL_SECURITY_CARD = {
    middleNameOnSSNCard : null,
    deathManualVerificationIndicator : false,
    socialSecurityNumber : null,
    suffixOnSSNCard : null,
    individualMandateExceptionIndicator : false,
    ssnManualVerificationIndicator : false,
    deathIndicatorAsAttested : false,
    nameSameOnSSNCardIndicator : null,
    deathIndicator : false,
    firstNameOnSSNCard : null,
    socialSecurityCardHolderIndicator : null,
    useSelfAttestedDeathIndicator : false,
    lastNameOnSSNCard : null,
    reasonableExplanationForNoSSN : null
};

const TRIBE_STATUS = {
    tribeName: null,
    tribeFullName: null,
    state: null,
    stateFullName: null,
    memberOfFederallyRecognizedTribeIndicator: null
};

const SPECIAL_CIRCUMSTANCES = {
    pregnantIndicator: false,
    numberBabiesExpectedInPregnancy: null
};

const FOSTER_CARE = {
    everInFosterCareIndicator: false,
    fosterCareState: null,
    gettingHealthCareThroughStateMedicaidIndicator: null,
    ageWhenLeftFosterCare: null
};

const MEDICAID_CHIP_DENIAL = {
    medicaidChipDenial: false,
    medicaidDeniedDate: null,
    medicaidDeniedDueToImmigration: null,
    changeInImmigrationSince5Year: null,
    changeInImmigrationSinceMedicaidDenial: null
};

const DISABILITY_INFO = {
    blindOrDisabled: false,
    longTerm: false,
    disabilityIndicator: false
};

const OTHER_IMMIGRATION_DOCUMENTS = {
    'cubanHaitianEntrantIndicator' : false,
    'americanSamoanIndicator' : false,
    'stayOfRemovalIndicator': false,
    'orreligibilityLetterIndicator' : false,
    'orrcertificationIndicator' : false,
    'withholdingOfRemovalIndicator': false,
    'domesticViolence': false,
    'indianOfUsOrCanada': false,
    'noneOfThese': false
};

const PROGRAM_TYPES =  [
    {
        'name': 'Medicaid',
        'eligible': null
    },
    {
        'name': 'CHIP',
        'eligible': null
    },
    {
        'name': 'Medicare',
        'eligible': null
    },
    {
        'name': 'TRICARE',
        'eligible': null
    },
    {
        'name': 'Veterans Affairs (VA) Health Care Program',
        'eligible': null
    },
    {
        'name': 'Peace Corps',
        'eligible': null
    },
    {
        'name': 'Employer Insurance',
        'eligible': null
    },
    {
        'name': 'COBRA Coverage',
        'eligible': null
    },
    {
        'name': 'Retire Health Benefits',
        'eligible': null
    },
    {
        'name': 'Other Coverage',
        'eligible': null
    },
    {
        'name': 'None Of the Above',
        'eligible': null
    }];

const CURRENT_OTHER_INSURANCE = {
    'currentlyHasHealthInsuranceIndicator' : false,
    'currentOtherInsurance' : {
        'peaceCorpsIndicator' : false,
        'otherStateOrFederalProgramType' : null,
        'tricareEligibleIndicator' : false,
        'nonofTheAbove' : null,
        'otherStateOrFederalProgramName' : null,
        'medicareEligibleIndicator' : false,
        'otherStateOrFederalProgramIndicator' : false
    }
};

const CURRENT_EMPLOYER_INSURANCE = {
    'currentLowestCostSelfOnlyPlanName': '',
    'willBeEnrolledInEmployerPlanIndicator': false,
    'memberEmployerPlanStartDate': null,
    'willBeEnrolledInEmployerPlanDate': null,
    'comingPlanMeetsMinimumStandardIndicator': false,
    'memberPlansToDropEmployerPlanIndicator': true,
    'employerWillOfferPlanIndicator': true,
    'expectedChangesToEmployerCoverageIndicator': true,
    'employerCoverageEndingDate': null,
    'memberPlansToEnrollInEmployerPlanIndicator': true,
    'employerPlanStartDate': null,
    'memberEmployerPlanEndingDate': null,
    'comingLowestCostSelfOnlyPlanName': '',
    'currentPlanMeetsMinimumStandardIndicator': false,
    'employerWillNotOfferCoverageIndicator': false,
    'isCurrentlyEnrolledInEmployerPlanIndicator': false,
    'insuranceName': null,
    'policyNumber': null
};

export default {
    AUTHORIZED_REPRESENTATIVE,
    CITIZENSHIP_DOCUMENT,
    CITIZENSHIP_STATUS,
    CURRENT_EMPLOYER_INSURANCE,
    CURRENT_OTHER_INSURANCE,
    DISABILITY_INFO,
    FOSTER_CARE,
    HOUSEHOLD_COMPOSITION,
    PROGRAM_TYPES,
    MEDICAID_CHIP_DENIAL,
    HOUSEHOLD_CONTACT,
    HOUSEHOLD_MEMBER,
    OTHER_ADDRESS,
    OTHER_IMMIGRATION_DOCUMENTS,
    SOCIAL_SECURITY_CARD,
    SPECIAL_CIRCUMSTANCES,
    TRIBE_STATUS
};
