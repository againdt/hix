package com.getinsured.hix.broker;

import javax.servlet.http.HttpServletRequest;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.broker.web.controller.BrokerController;
import com.getinsured.hix.broker.web.controller.DesignateSearchController;
import com.getinsured.hix.broker.web.controller.RegistrationController;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.platform.security.service.UserService;

public class BrokerControllerTest extends BaseTest {
	@Autowired
	RegistrationController registrationController;
	
	@Autowired
	BrokerController brokerController;	
	
	@Autowired
	DesignateSearchController searchController;
	@Autowired
	private UserService userService;
/*
	@Test
	public void testBokerRegistration() {

		AccountUser user = new AccountUser();

		user.setId(9);
		user.setfirstName("testFirstName");
		user.setLastName("testLastName");
		user.setUserName("testuserName");
		user.setEmail("priya@vimo.com");

		BindingResult result = null;
		Model model = null;
		HttpServletRequest request = null;
		String strBrokermessage = brokerController.brokerSubmit(user,
				result, model, request);
		Assert.assertEquals("/broker/BrokerRegistrationConfirm",
				strBrokermessage);
		System.out
				.println("=================Broker Registration submitted successfully");
	}
*/
	@Test
	public void testBrokerPasswordSetup() {

		AccountUser user = new AccountUser();

		user.setId(9);
		user.setPassword("test");

		BindingResult result = null;
		Model model = null;
		HttpServletRequest request = null;
		String strbrokerPasswordMsg = brokerController
				.brokerPasswordSave(user, result, model, request);
		Assert.assertEquals("/broker/BrokerPasswordConfirm",
				strbrokerPasswordMsg);
		System.out
				.println("======================Broker password created successfully");
	}

	@Test
	public void testBrokerSearch() throws CloneNotSupportedException {
		Broker asstObj = new Broker();
		AccountUser user = new AccountUser();
		user.setFirstName("Soniya");
		user.setLastName("Selvaraj");
		asstObj.setLanguagesSpoken("English");
		asstObj.setUser(user);
		Model model = new ExtendedModelMap();
		MockHttpServletRequest request = new MockHttpServletRequest();
		String strBrokerDetail = searchController.brokerSearch(
				asstObj, model, request);
		Assert.assertEquals("broker/list", strBrokerDetail);
		System.out.println("======================Broker Search successful");
	}

	@Test
	public void testBrokerDetail() {
		Broker asstObj = new Broker();
		int id = 2;
		Model model = new ExtendedModelMap();
		HttpServletRequest request = null;
		String strBrokerDetail = null;
		
		try {
			strBrokerDetail = searchController.brokerDetail(
					asstObj, id, model, request);
		}
		catch (Exception exception) {
			System.out.println("Exception occurred:" + exception.getMessage());
		}
		
		Assert.assertEquals("broker/detail", strBrokerDetail);
		System.out.println("======================Broker Detail successful");
	}

	/*
	 * @Test public void testBrokerprofileUpdate() { Broker asstObj = new
	 * Broker(); BindingResult result = null; Model model = new
	 * ExtendedModelMap(); HttpServletRequest request = null; String
	 * strBrokerprofile = ""; try { AccountUser user =
	 * userService.getLoggedInUser(); asstObj.setUser(user) ; Broker
	 * assisterObj = new Broker(); Location locobj = new Location();
	 * locobj.setAddOnZip(1); locobj.setAddress1("2050 southwest expy");
	 * locobj.setCity("sanjose"); locobj.setState("CA"); locobj.setZip(1234);
	 * assisterObj.setLocation(locobj);
	 * assisterObj.setCertificationStatus(certification_status.Inprogress);; String phone =
	 * "408-76-4454"; assisterObj.setContactNumber(phone);
	 * assisterObj.setEducationAndTraining("test");
	 * assisterObj.setLanguagesSpoken("EN");
	 * assisterObj.setProductExpertise("test");
	 * assisterObj.setAreaServed("test"); Part file = null; strBrokerprofile =
	 * registrationController.profilesubmit(assisterObj, locobj, result, model,
	 * request, file); }
	 * 
	 * catch(InvalidUserException ex){
	 * 
	 * } Assert.assertEquals("assister/viewprofile", strBrokerprofile);
	 * System.
	 * out.println("======================Broker Profile update successful");
	 * }
	 */
}