package com.getinsured.hix.entity.utils.notifications;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.notify.EmailService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.notify.NotificationAgent;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;

@Component
public class AssisterActivationEmail extends NotificationAgent {

	@Override
	public Map<String, String> getTokens(Map<String, Object> notificationContext) {
		ActivationJson activationJsonObj = (ActivationJson)notificationContext.get("ACTIVATION_JSON");
		AccountActivation accountActivation = (AccountActivation)notificationContext.get("ACCOUNT_ACTIVATION");
		Map<String,String> employerData = new HashMap<String, String>();
		String activationUrl = GhixEndPoints.GHIXWEB_SERVICE_URL + "account/user/activation/" + accountActivation.getActivationToken();
		employerData = activationJsonObj.getCreatedObject().getCustomeFields();
		employerData.put("exchangename", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME) );
		employerData.put("activationUrl", activationUrl );
		employerData.put("assisterName", activationJsonObj.getCreatedObject().getFullName());
		employerData.put("entityName", activationJsonObj.getCreatorObject().getFullName());
		employerData.put("exchangeURL", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL) );
		employerData.put("exchangePhone", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE) );
		return employerData;
	}
	@Override
	public Map<String, String> getEmailData(Map<String, Object> notificationContext) {
		ActivationJson activationJsonObj = (ActivationJson)notificationContext.get("ACTIVATION_JSON");
		Map<String, String> data = new HashMap<String, String>();
		data.put("To", activationJsonObj.getCreatedObject().getEmailId());
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if(!"NV".equalsIgnoreCase(stateCode)) {
			data.put("Subject", "A record has been created for you on the " + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME) );
		}
		return data;

	}
	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setNoticeTypeRepo(NoticeTypeRepository noticeTypeRepo) {
		this.noticeTypeRepo = noticeTypeRepo;
	}

	@Autowired
	public void setNoticeRepo(NoticeRepository noticeRepo) {
		this.noticeRepo = noticeRepo;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	@Autowired
	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	@Autowired
	public void setGhixDBSequenceUtil(GhixDBSequenceUtil ghixDBSequenceUtil) {
		this.ghixDBSequenceUtil = ghixDBSequenceUtil;
	}
	
	@Autowired
	public void setNoticeTemplateFactory(NoticeTemplateFactory templateFactory) {
		this.templateFactory = templateFactory;
	}
}
