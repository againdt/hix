package com.getinsured.hix.planmgmt.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.notify.EmailService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.notify.NotificationAgent;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.util.PlanMgmtConstants;

@Component
public class BrokerNotificationEmail extends NotificationAgent
{
	@Value("#{configProp['appUrl']}")
	private String appUrl;
	@Override
	public Map<String, String> getTokens(Map<String, Object> notificationContext) {
		
		Map<String,String> data = new HashMap<String, String>();
		Issuer issuerObj = (Issuer) notificationContext.get("ISSUER");
		Plan planObj = (Plan) notificationContext.get("PLAN");
		DateFormat dateFormat = new SimpleDateFormat(PlanMgmtConstants.MAIL_DATEFORMAT);
		data.put(PlanMgmtConstants.CURRENT_DATE, dateFormat.format(new TSDate()));
		data.put(PlanMgmtConstants.INSURANCE_EXCHANGE, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_NAME));
		data.put(PlanMgmtConstants.EXCHANGE_ADDRESS, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		data.put(PlanMgmtConstants.EXCHANGE_WEBSITE, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_URL));
		data.put(PlanMgmtConstants.ISSUER_MARKETING_NAME, issuerObj.getMarketingName() == null ? PlanMgmtConstants.EMPTY_STRING: issuerObj.getMarketingName().trim());
		data.put(PlanMgmtConstants.BROKER_NAME,PlanMgmtConstants.BROKER_NAME_TO_ADDRESS);
		data.put(PlanMgmtConstants.BROKER_ADDRESS,PlanMgmtConstants.EMPTY_STRING);
		
		data.put(PlanMgmtConstants.PLAN_NAME, planObj.getName().trim());
		data.put(PlanMgmtConstants.PLAN_CERTIFICATION_STATUS,planObj.getStatus());
		data.put(PlanMgmtConstants.PLAN_ENROLLMENT_STATUS,planObj.getEnrollmentAvail());
		data.put(PlanMgmtConstants.HEALTHEXCHANGE, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_NAME));
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_URL, appUrl == null ? PlanMgmtConstants.EMPTY_STRING : appUrl);
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_TEAM, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_NAME));
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_PHONE));
		data.put(PlanMgmtConstants.PLANINFORMATION_UPDATE,(String) notificationContext.get(PlanMgmtConstants.PLANINFORMATION_UPDATE));
		return data;
		
	}
	
	@Override
	public Map<String, String> getEmailData(Map<String, Object> notificationContext) {
		final Map<String, String> emailHeaders = new HashMap<String, String>();
		Issuer issuerObj = (Issuer) notificationContext.get("ISSUER");
		Plan planObj = (Plan) notificationContext.get("PLAN");
		emailHeaders.put(PlanMgmtConstants.ISSUER_ID, String.valueOf(issuerObj.getId()));
		emailHeaders.put(PlanMgmtConstants.BCC, (String)notificationContext.get(PlanMgmtConstants.RECIPIENT));
		emailHeaders.put(PlanMgmtConstants.ID, Integer.toString(planObj.getId()));
		return emailHeaders;
	}
	
	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setNoticeTypeRepo(NoticeTypeRepository noticeTypeRepo) {
		this.noticeTypeRepo = noticeTypeRepo;
	}

	@Autowired
	public void setNoticeRepo(NoticeRepository noticeRepo) {
		this.noticeRepo = noticeRepo;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	@Autowired
	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	@Autowired
	public void setGhixDBSequenceUtil(GhixDBSequenceUtil ghixDBSequenceUtil) {
		this.ghixDBSequenceUtil = ghixDBSequenceUtil;
	}
	
	@Autowired
	public void setNoticeTemplateFactory(NoticeTemplateFactory templateFactory) {
		this.templateFactory = templateFactory;
	}
}
