package com.getinsured.hix.plandisplay;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.hix.dto.plandisplay.PdSession;
import com.getinsured.hix.model.plandisplay.PdResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;

@Controller
public class PdSecureController {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private PlanDisplayWebHelper planDisplayWebHelper;
	
	@RequestMapping(value="/securePlanDisplay",method = RequestMethod.GET)
	public String securePlanDisplay(HttpServletRequest request, Model model) 
	{	
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth == null || auth.getName().equalsIgnoreCase("Anonymous")) {
			logger.error("No authentication context available for /securePlanDisplay");
		}else {
			logger.info("Secure PD access granted to {}", auth.getName());
		}
		HttpSession session = request.getSession();
		ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(sra != null) {
			if(logger.isInfoEnabled()) {
				logger.info("Request by Referer: {} for session id{}", sra.getRequest().getHeader("Referer".intern()), session.getId());
			}
		}
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession != null && pdSession.getPdHouseholdDTO() != null)
		{
			EnrollmentType enrollmentType = pdSession.getPdHouseholdDTO().getEnrollmentType();
			if(YorN.Y.equals(pdSession.getPdHouseholdDTO().getAutoRenewal())) {
				PdResponse pdResponse = planDisplayWebHelper.processAutoRenewal(pdSession);
	
				XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
				return xstream.toXML(pdResponse);
			} else if(EnrollmentType.S.equals(enrollmentType) || EnrollmentType.A.equals(enrollmentType)) {
				return "redirect:/private/specialEnrollment";
			} else {
				return "redirect:/private/postShopping";
			}
		}
		else
		{
			return "error/errorPageTechnicalIssue";
		}
	}

}
