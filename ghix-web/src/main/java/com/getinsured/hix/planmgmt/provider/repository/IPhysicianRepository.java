package com.getinsured.hix.planmgmt.provider.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Physician;

public interface IPhysicianRepository extends JpaRepository<Physician, Integer> {
	@Query("SELECT ph FROM Physician ph, Provider p left join p.providerNetwork as providerNetwork  where LOWER(p.datasourceRefId) = LOWER(:datasourceRefId) AND (p.id=ph.provider) And providerNetwork.network.id=:networkId")
    Physician getPhysicianByDataSourceRefId(@Param("datasourceRefId") String datasourceRefId,@Param("networkId") int networkId);
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa, Provider p, Specialty s, PhysicianSpecialityRelation psr  where LOWER(ph.type)=LOWER(:type) AND LOWER(s.groupName)=LOWER(ph.type) AND p.id=ph.provider.id AND ph.id=pa.physician.id AND (ph.id=psr.physician.id AND s.id=psr.specialty.id) AND (((p.name is null) OR (LOWER(p.name) like '%'|| LOWER(:name) ||'%')) AND ((ph.gender is null) OR (LOWER(ph.gender) like '%'|| LOWER(:gender) ||'%')) AND ((ph.languages is null) OR (LOWER(ph.languages) like '%'|| LOWER(:languages) ||'%')) AND (s.id=:specialities OR s.parentSpecialtyId=:specialities) AND ((pa.zip is null) OR pa.zip like '%'||:dentistNearZip||'%')) order by ph.type")
    List<Physician> getPhysicians(@Param("name") String name,@Param("gender") String gender,@Param("specialities") int specialities, @Param("languages") String languages,@Param("dentistNearZip") String dentistNearZip, @Param("type") String type);
	
	@Query("SELECT DISTINCT ph FROM Plan pl,Physician ph, PhysicianAddress pa, Provider p, Network n, ProviderNetwork pn, Specialty s, PhysicianSpecialityRelation psr  where LOWER(ph.type)=LOWER(:type) AND pl.id=:planId AND pl.network.id=:networkId AND pl.network.id=n.id AND n.id=pn.network.id AND p.id=pn.provider.id AND p.id=ph.provider.id AND ph.id=pa.physician.id AND (ph.id=psr.physician.id AND s.id=psr.specialty.id) AND ((LOWER(p.name) like '%'|| LOWER(:name) ||'%') AND (LOWER(ph.gender) like '%'|| LOWER(:gender) ||'%') AND LOWER(ph.languages) like '%'|| LOWER(:languages) ||'%' OR (s.id=:specialities OR s.parentSpecialtyId=:specialities) OR (pa.zip like '%'||:dentistNearZip||'%')) order by ph.type")
    List<Physician> getPhysiciansForNetworkAndPlan(@Param("planId") Integer planId, @Param("networkId") Integer networkId, @Param("name") String name,@Param("gender") String gender,@Param("specialities") Integer specialities, @Param("languages") String languages,@Param("dentistNearZip") String dentistNearZip, @Param("type") String type);
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)=LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :firstName IS NOT NULL AND LOWER(ph.first_name) like LOWER(:firstName)||'%') OR ( :lastName IS NOT NULL AND LOWER(ph.last_name) like LOWER(:lastName) ||'%')) " +
			"AND (( :zipCode IS NOT NULL AND LOWER(pa.zip) = LOWER(:zipCode)) OR ( :cityName IS NOT NULL AND LOWER(pa.city) like LOWER(:cityName)||'%'))")
    Page<Physician> getProviders(@Param("firstName") String firstName,@Param("lastName") String lastName, @Param("cityName") String cityName, @Param("zipCode") String zipCode, @Param("type") String physicianType,Pageable pageable);
	
	@Query("SELECT ph.id, ph.first_name, ph.last_name,ph.title, ph.suffix, ph.boardCertification, ph.languages," +
			" pa.id, pa.address1, pa.city, pa.state, pa.zip, pa.phoneNumber, pa.lattitude, pa.longitude," +
			" spec.code, spec.name" +
			" FROM Physician ph" +
			" left join  ph.physicianAddress as pa" +
			" left join  ph.physicianSpecialRelation as psr" +
			" left join  psr.specialty as spec" +
			" WHERE ph.id = :physicianId ")
	List<Object[]> getPhysicianById(@Param("physicianId") Integer PhysicianId);
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)<>LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :name IS NOT NULL AND LOWER(ph.first_name) like LOWER(:name)||'%') OR ( :name IS NOT NULL AND LOWER(ph.last_name) like LOWER(:name) ||'%')) ORDER BY ph.first_name, ph.last_name")
    Page<Physician> getPhysicianByName(@Param("name") String name, @Param("type") String physicianType,Pageable pageable);
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)<>LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :firstName IS NOT NULL AND LOWER(ph.first_name) like LOWER(:firstName)||'%') OR ( :lastName IS NOT NULL AND LOWER(ph.last_name) like LOWER(:lastName) ||'%')) ")
    Page<Physician> getPhysicianByName(@Param("firstName") String firstName,@Param("lastName") String lastName, @Param("type") String physicianType,Pageable pageable);
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)<>LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :firstName IS NOT NULL AND LOWER(ph.first_name) like LOWER(:firstName)||'%') AND ( :lastName IS NOT NULL AND LOWER(ph.last_name) like LOWER(:lastName) ||'%')) ")
    Page<Physician> getPhysicianByFullName(@Param("firstName") String firstName,@Param("lastName") String lastName, @Param("type") String physicianType,Pageable pageable);
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)<>LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :fullName IS NOT NULL AND LOWER(concat(concat(ph.first_name,' '),ph.last_name)) like LOWER(:fullName)||'%')) ORDER BY ph.first_name, ph.last_name")
    Page<Physician> getPhysicianByFullName(@Param("fullName") String fullName, @Param("type") String physicianType,Pageable pageable);
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)<>LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :fullName IS NOT NULL AND LOWER(concat(concat(ph.first_name,' '),ph.last_name)) like LOWER(:fullName)||'%')) " +
			"AND ((( :zipCode IS NOT NULL AND LOWER(pa.zip) = LOWER(:zipCode))) OR (( :cityName IS NOT NULL AND LOWER(pa.city) like LOWER(:cityName)||'%'))) ORDER BY ph.first_name, ph.last_name")
    Page<Physician> getPhysicianByFullNameAndLocation(@Param("fullName") String fullName,@Param("cityName") String cityName, @Param("zipCode") String zipCode, @Param("type") String physicianType,Pageable pageable);
	
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)<>LOWER(:type) AND ph.id=pa.physician.id AND" +
			" (( :zipCode IS NOT NULL AND LOWER(pa.zip) = LOWER(:zipCode)) OR ( :cityName IS NOT NULL AND LOWER(pa.city) like LOWER(:cityName)||'%')) ORDER BY ph.first_name, ph.last_name")
    Page<Physician> getPhysicianByLocation(@Param("cityName") String cityName, @Param("zipCode") String zipCode, @Param("type") String physicianType,Pageable pageable);
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)<>LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :name IS NOT NULL AND LOWER(ph.first_name) like LOWER(:name)||'%') OR ( :name IS NOT NULL AND LOWER(ph.last_name) like LOWER(:name) ||'%')) " +
			"AND (( :zipCode IS NOT NULL AND LOWER(pa.zip) = LOWER(:zipCode)) OR ( :cityName IS NOT NULL AND LOWER(pa.city) like LOWER(:cityName)||'%')) ORDER BY ph.first_name, ph.last_name")
    Page<Physician> getPhysicianByNameAndLocation(@Param("name") String name, @Param("cityName") String cityName, @Param("zipCode") String zipCode, @Param("type") String physicianType,Pageable pageable);
	
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)=LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :firstName IS NOT NULL AND LOWER(ph.first_name) like LOWER(:firstName)||'%') OR ( :lastName IS NOT NULL AND LOWER(ph.last_name) like LOWER(:lastName) ||'%')) " +
			"AND (( :zipCode IS NOT NULL AND LOWER(pa.zip) = LOWER(:zipCode)) OR ( :cityName IS NOT NULL AND LOWER(pa.city) like LOWER(:cityName)||'%'))")
    Page<Physician> getDentist(@Param("firstName") String firstName,@Param("lastName") String lastName, @Param("cityName") String cityName, @Param("zipCode") String zipCode, @Param("type") String physicianType,Pageable pageable);
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)=LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :firstName IS NOT NULL AND LOWER(ph.first_name) like LOWER(:firstName)||'%') OR ( :lastName IS NOT NULL AND LOWER(ph.last_name) like LOWER(:lastName) ||'%')) ")
    Page<Physician> getDentistByName(@Param("firstName") String firstName,@Param("lastName") String lastName, @Param("type") String physicianType,Pageable pageable);
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)=LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :name IS NOT NULL AND LOWER(ph.first_name) like LOWER(:name)||'%') OR ( :name IS NOT NULL AND LOWER(ph.last_name) like LOWER(:name) ||'%')) ORDER BY ph.first_name, ph.last_name")
    Page<Physician> getDentistByName(@Param("name") String name, @Param("type") String physicianType,Pageable pageable);
	
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)=LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :firstName IS NOT NULL AND LOWER(ph.first_name) like LOWER(:firstName)||'%') AND ( :lastName IS NOT NULL AND LOWER(ph.last_name) like LOWER(:lastName) ||'%')) ")
    Page<Physician> getDentistByFullName(@Param("firstName") String firstName,@Param("lastName") String lastName, @Param("type") String physicianType,Pageable pageable);
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)=LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :fullName IS NOT NULL AND LOWER(concat(concat(ph.first_name,' '),ph.last_name)) like LOWER(:fullName)||'%')) ORDER BY ph.first_name, ph.last_name")
    Page<Physician> getDentistByFullName(@Param("fullName") String fullName, @Param("type") String physicianType,Pageable pageable);
	
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)=LOWER(:type) AND ph.id=pa.physician.id AND" +
			" (( :zipCode IS NOT NULL AND LOWER(pa.zip) = LOWER(:zipCode)) OR ( :cityName IS NOT NULL AND LOWER(pa.city) like LOWER(:cityName)||'%')) ORDER BY ph.first_name, ph.last_name")
    Page<Physician> getDentistByLocation(@Param("cityName") String cityName, @Param("zipCode") String zipCode, @Param("type") String physicianType,Pageable pageable);
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)=LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :name IS NOT NULL AND LOWER(ph.first_name) like LOWER(:name)||'%') OR ( :name IS NOT NULL AND LOWER(ph.last_name) like LOWER(:name) ||'%')) " +
			"AND (( :zipCode IS NOT NULL AND LOWER(pa.zip) = LOWER(:zipCode)) OR ( :cityName IS NOT NULL AND LOWER(pa.city) like LOWER(:cityName)||'%')) ORDER BY ph.first_name, ph.last_name")
    Page<Physician> getDentistByNameAndLocation(@Param("name") String name, @Param("cityName") String cityName, @Param("zipCode") String zipCode, @Param("type") String physicianType,Pageable pageable);
	
	@Query("SELECT DISTINCT ph FROM Physician ph, PhysicianAddress pa" +
			" WHERE LOWER(ph.type)=LOWER(:type) AND ph.id=pa.physician.id AND" +
			"(( :fullName IS NOT NULL AND LOWER(concat(concat(ph.first_name,' '),ph.last_name)) like LOWER(:fullName)||'%')) " +
			"AND ((( :zipCode IS NOT NULL AND LOWER(pa.zip) = LOWER(:zipCode))) OR (( :cityName IS NOT NULL AND LOWER(pa.city) like LOWER(:cityName)||'%'))) ORDER BY ph.first_name, ph.last_name")
    Page<Physician> getDentistByFullNameAndLocation(@Param("fullName") String fullName,@Param("cityName") String cityName, @Param("zipCode") String zipCode, @Param("type") String physicianType,Pageable pageable);

	
}
