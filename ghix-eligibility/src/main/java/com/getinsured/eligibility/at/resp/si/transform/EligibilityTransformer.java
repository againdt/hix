package com.getinsured.eligibility.at.resp.si.transform;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.*;
import org.apache.commons.lang.StringUtils;

import com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.util.ReferralUtil;

public final class EligibilityTransformer {

	private static final String EE_PACKAGE_NAME = "com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.";

	private static final String EMPTY = StringUtils.EMPTY;

	private static final Map<String, String> CSR_MAP = new HashMap<>();

	private static final Map<String, String> ELIGIBILITY_REASON_MAP = new HashMap<>();

	static{
		CSR_MAP.put("OpenToIndiansBelow300PercentFPL", "CS2");
		CSR_MAP.put("OpenToIndiansAbove300PercentFPL", "CS3");
		CSR_MAP.put("73PercentActuarialVarianceLevelSilverPlanCSR", "CS4");
		CSR_MAP.put("87PercentActuarialVarianceLevelSilverPlanCSR", "CS5");
		CSR_MAP.put("94PercentActuarialVarianceLevelSilverPlanCSR", "CS6");
	}

	static{
		//ELIGIBILITY_REASON_MAP.put("999", "Not Applicable, as household of primary taxpayer is eligible for APTC.");
		ELIGIBILITY_REASON_MAP.put("999", "Not Applicable.");
		ELIGIBILITY_REASON_MAP.put("278", "Primary Taxpayer indicated that his/her planned return filing status to be 'Married Filing Separately'. For married primary taxpayer to qualify, the planned return filing status needs to be indicated as 'Married Filing Jointly'.");
		ELIGIBILITY_REASON_MAP.put("219", "IRS Records indicate that the primary taxpayer and/or current indicated spouse received APTC tax credit in the previous year and did not comply with HHS's requirement to file a return for that year.");
		ELIGIBILITY_REASON_MAP.put("293", "The tax household of the primary taxpayer does not include any individual that is eligible for QHP while being also ineligible for any Minimum Essential Coverage (Public or ESI).");
		ELIGIBILITY_REASON_MAP.put("212", "Household Income for Primary Taxpayer is over 400% of FPL and does not qualify.");
		ELIGIBILITY_REASON_MAP.put("211", "Household Income for Primary Taxpayer is below 100% of FPL and does not include any lawfully present individual that would have qualified for special rule.");

		ELIGIBILITY_REASON_MAP.put("100", "QHP Ineligible");
		ELIGIBILITY_REASON_MAP.put("101", "APTC Ineligible");
		ELIGIBILITY_REASON_MAP.put("102", "Not Indian and ptcFPL above 250 percent");
		ELIGIBILITY_REASON_MAP.put("103", "Indian and ptcFPL above 300 percent");
		ELIGIBILITY_REASON_MAP.put("104", "Not Indian");
		ELIGIBILITY_REASON_MAP.put("105", "Indian and Ineligible for APTC");

		ELIGIBILITY_REASON_MAP.put("107", "Applicant does not meet criteria for CHIP eligibility");
		ELIGIBILITY_REASON_MAP.put("302", "Residency, income or household size information is pending");

		ELIGIBILITY_REASON_MAP.put("106", "Applicant does not meet all of the Medicaid eligibility criteria");
		ELIGIBILITY_REASON_MAP.put("128", "Applicant's child does not have minimal coverage");
	}

	private EligibilityTransformer(){}

	public static Map<String, EligibilityProgram> eligibilityProgram(InsuranceApplicantType insuranceApplicantType) {

		Map<String, EligibilityProgram> eligibilityProgramMap = Collections.emptyMap();
		boolean defaultEligibilityIndicator = false;
		List<EligibilityType> eligibilityTypeList = insuranceApplicantType.getEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility();

		if (!eligibilityTypeList.isEmpty()) {
			eligibilityProgramMap = new HashMap<>();

			for (EligibilityType eligibilityType : eligibilityTypeList) {
				EligibilityProgram ep = new EligibilityProgram();

				String eligibilityIndicatorType = StringUtils.substringAfter(eligibilityType.getClass().toString(), EE_PACKAGE_NAME);
				ep.setEligibilityType(eligibilityIndicatorType);
				if(null != eligibilityType.getEligibilityIndicator() && eligibilityType.getEligibilityIndicator().isValue()) {
					ep.setEligibilityIndicator(StringUtils.upperCase(String.valueOf(eligibilityType.getEligibilityIndicator().isValue())));	
				} else {
					ep.setEligibilityIndicator(StringUtils.upperCase(String.valueOf(defaultEligibilityIndicator)));
				}				
				ep.setEligibilityStartDate(TransformerHelper.extractDate(eligibilityType.getEligibilityDateRange() != null ? eligibilityType.getEligibilityDateRange().getStartDate() : null));
				ep.setEligibilityEndDate(TransformerHelper.extractDate(eligibilityType.getEligibilityDateRange() != null ? eligibilityType.getEligibilityDateRange().getEndDate() : null));
				ep.setEligibilityDeterminationDate(TransformerHelper.extractDate(eligibilityType.getEligibilityDetermination() != null ? eligibilityType.getEligibilityDetermination().getActivityDate() : null));

				ep.setIneligibleReason(extractIneligibleReason(eligibilityType, eligibilityIndicatorType));

				if (eligibilityType.getEligibilityEstablishingSystem() != null){
					ep.setEstablishingCategoryCode(extractEligibilityInfoString(eligibilityType.getEligibilityEstablishingSystem().getInformationExchangeSystemCategoryCode()));
					ep.setEstablishingStateCode(extractEligibilityInfoString(eligibilityType.getEligibilityEstablishingSystem().getInformationExchangeSystemStateCode()));
					ep.setEstablishingCountyName(extractEligibilityInfoString(eligibilityType.getEligibilityEstablishingSystem().getInformationExchangeSystemCountyName()));
				}

				if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.APTC_ELIGIBILITY_TYPE)){
					APTCEligibilityType aptc = (APTCEligibilityType) eligibilityType;
					setAPTCAmt(ep, aptc);
				}
				if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.STATE_SUBSIDY_ELIGIBILITY_TYPE)){
					StateSubsidyEligibilityType stateSubsidy = (StateSubsidyEligibilityType) eligibilityType;
					setStateSubsidyAmt(ep, stateSubsidy);
				} else if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.CSR_ELIGIBILITY_TYPE)){
					CSREligibilityType csr = (CSREligibilityType) eligibilityType;
					setCSRLevel(ep, csr);
				}
				eligibilityProgramMap.put(eligibilityIndicatorType, ep);

			}
		}


		return eligibilityProgramMap;
	}

	private static String extractIneligibleReason(EligibilityType eligibilityType, String eligibilityIndicatorType) {

		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		String reasonText = "Unknown";
		if ("NM".equals(stateCode)){
			reasonText = extractIneligibleReasonNM(eligibilityType, reasonText);
		} else {
			reasonText = extractIneligibleReasonID(eligibilityType, eligibilityIndicatorType);
		}
		return reasonText;
	}

	private static String extractIneligibleReasonID(
			EligibilityType eligibilityType, String eligibilityIndicatorType) {
		String reasonText = "";
		if (!isEligible(eligibilityType)){
			if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.APTC_ELIGIBILITY_TYPE)){
				reasonText = "Not eligible to buy QHP with any Financial assistance.";
			} else if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.CSR_ELIGIBILITY_TYPE)){
				reasonText = "Not eligible for Cost Sharing Reduction.";
			}
		}
		return reasonText;
	}

	private static String extractIneligibleReasonNM(
			EligibilityType eligibilityType, String reasonText) {
		String reasonCode = extractEligibilityInfoString(eligibilityType.getEligibilityReasonText());
		if (ELIGIBILITY_REASON_MAP.containsKey(reasonCode)){
			reasonText = ELIGIBILITY_REASON_MAP.get(reasonCode);
		}
		return reasonText;
	}
	
	private static boolean isEligible(EligibilityType eligibilityType) {
		return (eligibilityType.getEligibilityIndicator() != null && eligibilityType.getEligibilityIndicator().isValue());
	}

	private static void setAPTCAmt(EligibilityProgram ep, APTCEligibilityType aptc) {

		BigDecimal maxAPTCAmount = aptc != null ? aptc.getAPTC() != null ? aptc.getAPTC().getAPTCMaximumAmount() != null ?
				aptc.getAPTC().getAPTCMaximumAmount().getValue() : null : null : null;
				//BigDecimal maxAPTCAmount = aptc.getAPTC().getAPTCMaximumAmount().getValue();

				ep.setMaxAPTCAmount(maxAPTCAmount);
	}

	private static void setStateSubsidyAmt(EligibilityProgram ep, StateSubsidyEligibilityType stateSubsidy) {

		BigDecimal maxStateSubsidyAmount = stateSubsidy != null ? stateSubsidy.getStateSubsidy() != null ? stateSubsidy.getStateSubsidy().getStateSubsidyAmount() != null ?
				stateSubsidy.getStateSubsidy().getStateSubsidyAmount().getValue() : null : null : null;
		//BigDecimal maxAPTCAmount = aptc.getAPTC().getAPTCMaximumAmount().getValue();

		ep.setMaxStateSubsidyAmount(maxStateSubsidyAmount);
	}

	private static void setCSRLevel(EligibilityProgram ep, CSREligibilityType csr) {

		String csrLevel = csr != null ? csr.getCSRAdvancePayment() != null ?
				csr.getCSRAdvancePayment().getCSRCategoryAlphaCode() != null ?  csr.getCSRAdvancePayment().getCSRCategoryAlphaCode().getValue() : EMPTY: EMPTY : EMPTY;
				//String csrLevel = csr.getCSRAdvancePayment().getCSRCategoryAlphaCode().getValue();

				if (CSR_MAP.containsKey(csrLevel)){
					ep.setCsrLevel(CSR_MAP.get(csrLevel));
				}

	}


	private static String extractEligibilityInfoString(Object input){

		String output = null;

		output = extractEligibilityInfo1(input);

		if (output == null) {
			output = extractEligibilityInfo2(input);
		}

		return output;

	}


	private static String extractEligibilityInfo2(Object input) {
		String output = null;

		if (input instanceof com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeType){
			com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeType usStateCodeType =
					(com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeType) input;

			output = usStateCodeType != null ? (usStateCodeType.getValue() != null ? usStateCodeType.getValue().toString() : output) : output;
		} else if (input instanceof com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ProperNameTextType){
			com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ProperNameTextType properNameTextType =
					(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ProperNameTextType) input;

			output = properNameTextType != null ? (properNameTextType.getValue() != null ? properNameTextType.getValue().toString() : output) : output;
		} else if (input instanceof String) {
			output = input != null ? input.toString() : output;
		}
		return output;
	}

	private static String extractEligibilityInfo1(Object input) {

		String output = null;

		if (input instanceof com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType){
			com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType  tt =
					(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType) input;

			output = tt != null ? tt.getValue() : null;
		} else if (input instanceof com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InformationExchangeSystemCategoryCodeType) {
			com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InformationExchangeSystemCategoryCodeType  informationExchangeSystemCategoryCodeType =
					(com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InformationExchangeSystemCategoryCodeType) input;

			output = informationExchangeSystemCategoryCodeType != null ? (informationExchangeSystemCategoryCodeType.getValue() != null ? informationExchangeSystemCategoryCodeType.getValue().toString() : output) :output;
		}
		return output;
	}
}
