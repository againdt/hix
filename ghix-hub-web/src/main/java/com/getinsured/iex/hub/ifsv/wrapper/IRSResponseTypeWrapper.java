package com.getinsured.iex.hub.ifsv.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.IRSResponseType;

/**
 * Wrapper com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.IRSResponseType
 * 
 * @author Nikhil Talreja
 * @since 27-Feb-2014
 *
 */
public class IRSResponseTypeWrapper implements JSONAware {
	
	private String requestID;
	private HouseholdTypeWrapper household;
	
	private static final String REQUEST_ID_KEY = "RequestID";
	private static final String HOUSHOLD_KEY = "Household";
	
	public IRSResponseTypeWrapper(IRSResponseType irsResponseType){
		
		this.household = new HouseholdTypeWrapper(irsResponseType.getHousehold());
		this.requestID = irsResponseType.getRequestID().getValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public java.lang.String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.household != null){
			obj.put(HOUSHOLD_KEY, this.household);
		}
		obj.put(REQUEST_ID_KEY, this.requestID);
		return obj.toJSONString();
	}
	
	
}
