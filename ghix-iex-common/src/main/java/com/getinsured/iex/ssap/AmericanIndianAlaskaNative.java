
package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class AmericanIndianAlaskaNative {

    @Expose
    private String tribeName;
    @Expose
    private String tribeFullName;
    @Expose
    private String state;
    @Expose
    private String stateFullName;
    @Expose
    private Boolean memberOfFederallyRecognizedTribeIndicator = null; // UI doesn't want anything preselected until user picks.

    public String getTribeFullName() {
		return tribeFullName;
	}

	public void setTribeFullName(String tribeFullName) {
		this.tribeFullName = tribeFullName;
	}

	public String getStateFullName() {
		return stateFullName;
	}

	public void setStateFullName(String stateFullName) {
		this.stateFullName = stateFullName;
	}

	public String getTribeName() {
        return tribeName;
    }

    public void setTribeName(String tribeName) {
        this.tribeName = tribeName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Boolean getMemberOfFederallyRecognizedTribeIndicator() {
        return memberOfFederallyRecognizedTribeIndicator;
    }

    public void setMemberOfFederallyRecognizedTribeIndicator(Boolean memberOfFederallyRecognizedTribeIndicator) {
        this.memberOfFederallyRecognizedTribeIndicator = memberOfFederallyRecognizedTribeIndicator;
    }

}
