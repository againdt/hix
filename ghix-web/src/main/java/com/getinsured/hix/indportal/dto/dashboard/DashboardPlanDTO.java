package com.getinsured.hix.indportal.dto.dashboard;

public class DashboardPlanDTO {

	public enum PlanTypeEnum {
		HEALTH, DENTAL
	}

	public enum PlanLevelEnum {
		GOLD, SILVER, BRONZE, PLATINUM, EXPANDEDBRONZE, CATASTROPHIC
	}

	private String hiosId;
	private PlanTypeEnum type;
	private Float grossPremium;
	private Float netPremium;
	private String planLevel;
	private String planName;
	private String issuerName;
	private Long planId;

	public String getHiosId() {
		return hiosId;
	}

	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}

	public PlanTypeEnum getType() {
		return type;
	}

	public void setType(PlanTypeEnum type) {
		this.type = type;
	}

	public Float getGrossPremium() {
		return grossPremium;
	}

	public void setGrossPremium(Float grossPremium) {
		this.grossPremium = grossPremium;
	}

	public Float getNetPremium() {
		return netPremium;
	}

	public void setNetPremium(Float netPremium) {
		this.netPremium = netPremium;
	}

	public String getPlanLevel() {
		return planLevel;
	}

	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public Long getPlanId() {
		return planId;
	}

	public void setPlanId(Long planId) {
		this.planId = planId;
	}

}
