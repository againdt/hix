package com.getinsured.hix.referral.validator;

public class DynamicSecurityQuestionsDTO {

	public DynamicSecurityQuestionsDTO() {
	}

	private String firstName;
	private String lastName;
	private String birthDate;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DynamicSecurityQuestionsDTO [firstName=").append(firstName).append(", lastName=")
				.append(lastName).append(", birthDate=").append(birthDate).append("]");
		return builder.toString();
	}
}
