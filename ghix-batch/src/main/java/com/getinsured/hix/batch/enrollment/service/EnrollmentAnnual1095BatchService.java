package com.getinsured.hix.batch.enrollment.service;

import java.util.List;

import com.getinsured.hix.model.batch.BatchJobExecution;

public interface EnrollmentAnnual1095BatchService {
	
	/**
	 * 
	 * @return
	 */
	List<Integer> getUniqueIdsFor1095Pdf();

	/**
	 * 
	 * @param enrollment1095Id
	 * @return
	 * @throws Exception
	 */
	String process1095Enrollment(Integer enrollment1095Id) throws Exception;

	/**
	 * 
	 * @param string
	 * @return
	 */
	List<BatchJobExecution> getRunningBatchList(String string);
	
	/**
	 * 
	 * @return
	 */
	List<String> getUniqueHouseholdIdsFor1095Pdf();
	/**
	 * 
	 * @param householdIds
	 * @return
	 */
	List<Integer> getUniqueIdsFor1095Pdf(List<String> householdIds);
}
