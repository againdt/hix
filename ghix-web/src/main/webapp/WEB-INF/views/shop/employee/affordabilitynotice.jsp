<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script>
function wopen(url, name, w, h)
{
// Fudge factors for window decoration space.
w += 800;
h += 1200;
var win = window.open(url,
  name, 
  'width=' + w + ', height=' + h + ', ' +
  'location=yes, menubar=no, ' +
  'status=yes, toolbar=yes, scrollbars=yes, resizable=yes');
win.resizeTo(w, h);
win.focus();
}
</script>

<div class="gutter10">
  <div class="row-fluid">
	<h1><spring:message  code="label.applyforHealthcov"/></h1>
      <div class="row-fluid">
        <div class="span3" id="sidebar">
           <jsp:include page="../../layouts/employeeOpenEnrollSideBar.jsp" flush="true"></jsp:include>
        </div>
        <!-- /.sidebar-->
        <div class="span9" id="rightpanel">
          <div class="row-fluid">
            <h4 class="graydrkbg gutter10"><spring:message  code="label.affordnotice"/></h4>
            <div class="gutter10">
            <%-- <c:choose>
	            <c:when test="${employee.affordabilityStatus == 'YES' }">
		            <p> <spring:message  code="label.affordplansinfo"/> </p>
	            </c:when>
	            <c:otherwise> --%>
          		<c:choose>
					<c:when test="${employee.dependentCount == 0}">
						<c:set var="subsidyLimit" value="$44,680" />
					</c:when>
				<c:otherwise>
					<c:set var="subsidyLimit" value="$92,200" />
				</c:otherwise>
				</c:choose>
						
		    <p>
		    <spring:message code= "label.unaffordplans"/> 
		    <U><spring:message code = "label.totHouseholdIncome"/></U><a href="#" rel="tooltip" data-placement="top" data-original-title="<spring:message code = "label.totHouseholdIncomeDesc"/>" class="info"><i class="icon-question-sign"></i></a> 
		    <spring:message code= "label.isLessThan"/>
		     ${subsidyLimit},  
		     <spring:message code= "label.youMayQualifySubsidized"/> <spring:message code= "label.youCanDetermine"/> 
		     <a href="https://www.healthcare.gov" target="popup" onClick="wopen('https://www.healthcare.gov', 'popup', 640, 480); return false;"> www.healthcare.gov. </a> 
		     <spring:message code="label.continueNext"/> </p>
	            <%-- </c:otherwise>
            </c:choose> --%>
              <form class="form-horizontal" method="post" action="<c:url value="/shop/employee/affordabilitynotice" />"  name="affordabilitynotice_form" id="affordabilitynotice_form">
              <df:csrfToken/>
               <%--  <div class="control-group">
                <div class="center">
                   <c:if test="${employee.affordabilityStatus != 'YES' }">
                   <label class="radio inline">
                      <input type="radio" name="availability" id="#" value="<spring:message  code="label.checkindvsubsidy"/>">
                      <spring:message  code="label.checkindvsubsidy"/> </label>
                    <label class="radio inline">
                      <input type="radio" name="availability" id="availability" value="<spring:message  code="label.viewemployerplans"/>" checked="checked">
                      <spring:message  code="label.viewemployerplans"/> </label> 
                   </c:if>
                  </div>
                </div>--%>
				<div class="form-actions margin-top50">
               		<a href="verifydependent" class="btn"> <spring:message  code="label.back"/> </a> 
               		<input type="button" value="<spring:message  code="label.next"/>" class="btn btn-primary offset2" onclick="formSubmit()">
              </div>
              </form>
            </div>
            <!--gutter10--> 
          </div>
          <!--row-fluid--> 
        </div>
        <!--rightpanel--> 
      </div>
      <!--row-fluid--> 
  </div>
  <!--main--> 
</div>

<jsp:include page="waive_employer_coverage.jsp" >
	<jsp:param name="employeeId" value="${employee.id}" />
</jsp:include>

<script type="text/javascript">
function formSubmit(){
	$("#affordabilitynotice_form").submit();
}

 <%-- function validateSelection(affor_status){
	if(affor_status == 'YES'){
		//window.location.assign("selectplan");
		$("#affordabilitynotice_form").submit();
	}else{
		// check which radio button selected
		var optionVal = $("input[name=availability]:checked").val();
		if(optionVal != "" && optionVal != undefined ){
			//window.location.assign("selectplan");
			$("#affordabilitynotice_form").submit();
		}else{
			alert("<spring:message code='label.validateoption' javaScriptEscape='true'/>");
		}
	}
}  --%>

$(document).ready(function(){
	$('.info').tooltip();
	$('i.icon-ok').parents('li').css('list-style','none');
	
	});

</script>