package com.getinsured.eligibility.ssap.sep.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.ref.service.ReferralOEService;
import com.getinsured.eligibility.at.sep.service.SepEventsService;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.eligibility.ssap.sep.util.SEPOverrideUtil;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.indportal.dto.ApplicantEventsDTO;
import com.getinsured.hix.indportal.dto.SEPOverrideDTO;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.model.SsapApplicantEvent.EventPrecedenceIndicator;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
@Service
public class SEPOverrideServiceImpl implements SEPOverrideService {

	@Autowired
	SsapApplicationEventRepository ssapApplicationEventRepository;
	
	@Autowired
	SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private SsapApplicantEventRepository ssapApplicantEventRepository;
		
	@Autowired
	SepEventsService sepEventsService;
	
	@Autowired private QLEValidationService qleValidationService;
	
	@Autowired private ReferralOEService referralOEService;
	
	@Autowired
	private UserService userService;
	
	private static final Logger LOGGER = Logger.getLogger(SEPOverrideServiceImpl.class);
	
	public static final String KEEP_ONLY_FALSE = "N";
	public static SimpleDateFormat formatter = new SimpleDateFormat(EligibilityConstants.SHORT_DATE_FORMAT);;

	public String saveOverrideSEPDetails(SEPOverrideDTO sepOverrideDTO) throws Exception {
		
		String response = EligibilityConstants.FAILURE;
				
		try {
			Integer userId = sepOverrideDTO.getUserId();
			DateTime sepStartDate = new DateTime(formatter.parse(sepOverrideDTO.getSepStartDate()));
			DateTime sepEndDate = new DateTime(formatter.parse(sepOverrideDTO.getSepEndDate()));
	
			validateSEPDatesForOverride(sepOverrideDTO.getCoverageYear(), sepStartDate, sepEndDate);
			
			updateSSAPApplicationEventAndReRunValidationEngine(sepOverrideDTO, sepStartDate, sepEndDate,userId);
			
			response = EligibilityConstants.SUCCESS;
			
		} catch(Exception e) {
			LOGGER.error("Error occurred whie saving SEP Override Details", e);	
		}
		
		return response;
	}

	/**
	 * Validate the SEP start and End dates
	 * @param coverageYr
	 * @param sepStartDate
	 * @param sepEndDate
	 * @throws GIException
	 */
	private void validateSEPDatesForOverride(Integer coverageYr, DateTime sepStartDate, DateTime sepEndDate) throws GIException {
		
		//SEP End date cannot be before SEP Start date
		if(sepEndDate.isBefore(sepStartDate)) {
			throw new GIException("SEP End date cannot be earlier than SEP Start date");
		}
	}
	
	/**
	 * Update SSAP Application Event with overridden SEP dates 
	 * @param sepOverrideDTO
	 * @param sepStartDate
	 * @param sepEndDate
	 * @throws GIException
	 * @throws ParseException
	 */
	private void updateSSAPApplicationEventAndReRunValidationEngine(SEPOverrideDTO sepOverrideDTO, DateTime sepStartDate, DateTime sepEndDate, Integer lastUpdatedBy) throws GIException, ParseException {

		SsapApplication ssapApplication = ssapApplicationRepository.findByCaseNumberId(sepOverrideDTO.getCaseNumber());
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(ssapApplication.getCoverageYear()) ;

		if(null == ssapApplication) {
			throw new GIException("No SSAP application found for case number: "+ sepOverrideDTO.getCaseNumber());
		}
		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(
				ssapApplication.getId());
		if(null == ssapApplicationEvent 
				|| null == ssapApplicationEvent.getSsapApplicantEvents()
				|| ssapApplicationEvent.getSsapApplicantEvents().isEmpty()) {
			throw new GIException("No SSAP application event/applicant events found for ssap application: "+ ssapApplication.getId());
		}
		
		List<Long> modifiedApplicantEventsBasedOnPreviousEventId = collectEventsForCleanUp(sepOverrideDTO);

		if (modifiedApplicantEventsBasedOnPreviousEventId.size() != ReferralConstants.NONE){
			qleValidationService.cleanUpApplicantEvent(modifiedApplicantEventsBasedOnPreviousEventId, lastUpdatedBy);
            // refresh
			ssapApplicationEvent = ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(ssapApplication.getId());
		}
		List<SsapApplicantEvent> ssapApplEventToBeUpdated = updateSSAPApplicantEvents(ssapApplicationEvent, sepOverrideDTO,isNextYearsOE);
		
		//Update SSAP APplication Event with Overridden SEP dates
		Timestamp sepStartDateTimestamp = new Timestamp(sepStartDate.withTime(0, 0, 0, 0).getMillis());
		Timestamp sepEndDateTimestamp = new Timestamp(sepEndDate.withTime(23, 59, 59, 999).getMillis());
		ssapApplicationEvent.setCreatedBy(null);
		ssapApplicationEvent.setEnrollmentStartDate(sepStartDateTimestamp);
		ssapApplicationEvent.setEnrollmentEndDate(sepEndDateTimestamp);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		
		//Update Existing SSAP Applicants with appropriate Event precedence indicator
		if(!ssapApplEventToBeUpdated.isEmpty()) {
			ssapApplicantEventRepository.save(ssapApplEventToBeUpdated);
		}
		ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
		qleValidationService.reRunValidationEngineAfterSEPOverride(ssapApplication,ssapApplicationEvent,lastUpdatedBy);
	}

	private List<Long> collectEventsForCleanUp(SEPOverrideDTO sepOverrideDTO) throws GIException {
		List<ApplicantEventsDTO> applicantEvents = sepOverrideDTO.getApplicantEvents();

		for (ApplicantEventsDTO applicantEventsDTO : applicantEvents) {
			if (applicantEventsDTO.getEventId() == null || applicantEventsDTO.getPreviousEventId() == null){
			throw new GIException("Incoming data from UI has missing information for event id and previous event id for case number : "+ sepOverrideDTO.getCaseNumber());
			}
		}

        List<Long> modifiedApplicantEventsBasedOnPreviousEventId = new ArrayList<>();
		for (ApplicantEventsDTO applicantEventsDTO : applicantEvents) {
			if (applicantEventsDTO.getEventId().compareTo(applicantEventsDTO.getPreviousEventId()) != 0){
				modifiedApplicantEventsBasedOnPreviousEventId.add(applicantEventsDTO.getApplicantEventId().longValue());
			}
		}
		return modifiedApplicantEventsBasedOnPreviousEventId;
	}
	
	/**
	 * Update SSAP Applicant Event with overridden event type and event date entered
	 * @param ssapApplicationEvent
	 * @param sepOverrideDTO
	 * @return
	 * @throws ParseException
	 */
	private List<SsapApplicantEvent> updateSSAPApplicantEvents(SsapApplicationEvent ssapApplicationEvent, SEPOverrideDTO sepOverrideDTO, boolean isNextYearsOE) throws ParseException {

		List<SsapApplicantEvent> ssapApplEventToBeUpdated = new ArrayList<SsapApplicantEvent>();
		
		if(sepOverrideDTO.getApplicantEvents() != null
				&& !sepOverrideDTO.getApplicantEvents().isEmpty()) {
			for(SsapApplicantEvent ssapApplicantEvent : ssapApplicationEvent.getSsapApplicantEvents()) {
				for(ApplicantEventsDTO applicantEventsDTO : sepOverrideDTO.getApplicantEvents()) {
					//Update SSAP Applicant events for only those which are overridden 
					if(ssapApplicantEvent.getId() == applicantEventsDTO.getApplicantEventId()) {
						
						SsapApplicantEvent precedenceSsapApplicantEvent = null;
						Map<Integer, SsapApplicantEvent> existingSEPEventToSsapApplicantEventMap = new HashMap<Integer, SsapApplicantEvent>();
						
						//Fetch SEP_ENROLLMENT_EVENT for overridden event type
						SepEvents sepEvent = sepEventsService.findSepEventById(applicantEventsDTO.getEventId());
						
						//Fetch all Applicant Events for particular Applicant, for a given SSAP application
						List<SsapApplicantEvent> existingSsapApplicantEvents = ssapApplicantEventRepository.findBySsapApplicantId(ssapApplicantEvent.getSsapApplicant().getId());
						if(existingSsapApplicantEvents.size() > 1) {
							
							//Fetch all existing SSAP Applicant Events for a particular Applicant, other than the overriden event type
							List<SepEvents> existingSepEvents = new ArrayList<SepEvents>();				
							for(SsapApplicantEvent existingSsapApplicantEvent : existingSsapApplicantEvents) {
								if(existingSsapApplicantEvent.getId() == ssapApplicantEvent.getId())
									continue;
								
								//Check if existing Applicant Event has Event Precedence Indicator set a 'Y' 
								if(EventPrecedenceIndicator.Y.equals(existingSsapApplicantEvent.getEventPrecedenceIndicator()))
									precedenceSsapApplicantEvent = existingSsapApplicantEvent;
								
								SepEvents existingSepEvent = existingSsapApplicantEvent.getSepEvents();
								existingSepEvents.add(existingSepEvent);
								existingSEPEventToSsapApplicantEventMap.put(existingSepEvent.getId(), existingSsapApplicantEvent);
							}
							existingSepEvents.add(sepEvent);
							
							//Check the precedence order of SEP Events for particular Applicant including the overriden event type
							Integer precedenceSepEventId = SEPOverrideUtil.checkEventPrecedence(existingSepEvents);
							SsapApplicantEvent higherPrecedenceSSAPApplEvent = existingSEPEventToSsapApplicantEventMap.get(precedenceSepEventId);
							
							//Update Event Precedence Indicator based on latest precedence order
							if(null == higherPrecedenceSSAPApplEvent && precedenceSsapApplicantEvent != null) {
								precedenceSsapApplicantEvent.setEventPrecedenceIndicator(null);
								ssapApplicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
								ssapApplEventToBeUpdated.add(precedenceSsapApplicantEvent);
								
							} else if(null == precedenceSsapApplicantEvent && higherPrecedenceSSAPApplEvent != null){
								ssapApplicantEvent.setEventPrecedenceIndicator(null);
								higherPrecedenceSSAPApplEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
								ssapApplEventToBeUpdated.add(higherPrecedenceSSAPApplEvent);
							}
						} else {
							ssapApplicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
						}
								
						//Update SSAP Applicant Event SEP details with overridden Event date
						DateTime eventDate = new DateTime(formatter.parse(applicantEventsDTO.getEventDate()));
						Timestamp eventDateTimestamp = new Timestamp(eventDate.withTime(0, 0, 0, 0).getMillis());					
						ssapApplicantEvent.setEventDate(eventDateTimestamp);
						ssapApplicantEvent.setReportStartDate(eventDateTimestamp);
						ssapApplicantEvent.setEnrollmentStartDate(eventDateTimestamp);
						ssapApplicantEvent.setEnrollmentEndDate(SEPOverrideUtil.calculateEndDate(eventDateTimestamp,isNextYearsOE));
						
						ssapApplicantEvent.setSepEvents(sepEvent);
						ssapApplEventToBeUpdated.add(ssapApplicantEvent);
						
					}
				}
			}
		}
		
		return ssapApplEventToBeUpdated;
	}
}
