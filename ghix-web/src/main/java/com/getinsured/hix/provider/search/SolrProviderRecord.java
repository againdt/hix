package com.getinsured.hix.provider.search;

import org.apache.solr.common.SolrDocument;
import org.json.simple.JSONAware;

public class SolrProviderRecord implements JSONAware{
	
	private SolrDocument solrDocument;
	public SolrProviderRecord(SolrDocument doc){
		this.solrDocument = doc;
	}

	@Override
	public String toJSONString() {
		return this.solrDocument.toString();
	}

}
