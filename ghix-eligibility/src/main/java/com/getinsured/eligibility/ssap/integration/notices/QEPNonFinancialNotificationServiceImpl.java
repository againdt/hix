package com.getinsured.eligibility.ssap.integration.notices;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

@Service("qepNonFinancialNotificationService")
@Transactional(readOnly = true)
public class QEPNonFinancialNotificationServiceImpl implements
		QEPNonFinancialNotificationService {

	@Autowired
	private NoticeService noticeService;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("iLocationRepository")
	private ILocationRepository iLocationRepository;

	@Autowired private CmrHouseholdRepository cmrHouseholdRepository;

	@Autowired
	private SsapApplicationEventService ssapApplicationEventService;
	
	@Autowired
	private PreferencesService preferencesService;
	

	@Override
	public String generateQEPGrantNoticeForNonFinancial(String caseNumber)
			throws Exception {

		return generate(caseNumber, "EE028NonFinancialQEPGrantNotice");
	}

	private String generate(String caseNumber, String noticeTemplateName) throws NoticeServiceException {
		SsapApplication ssapApplication = getSSAPApplicationData(caseNumber);

		int moduleId = extractHouseholdId(ssapApplication);
		QEPNonFinancialNotificationDTO qepNonFinancialNotificationDTO = getQEPNonFinancialNotificationDTO(ssapApplication,moduleId);

		String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
		String fullName = getName(ssapApplication);
		
		String relativePath = "cmr/" + moduleId + "/";
		String ecmFileName = noticeTemplateName + "_" + moduleId
				+ (new TSDate().getTime()) + ".pdf";

		
		PreferencesDTO preferencesDTO = preferencesService.getPreferences(moduleId, false);

		GhixNoticeCommunicationMethod commPref = preferencesDTO.getPrefCommunication();

		List<String> validEmails = new ArrayList<String>();
		validEmails.add(preferencesDTO.getEmailAddress());

		Notice notice = null;

		notice = noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN,
				getReplaceableObjectData(qepNonFinancialNotificationDTO, ssapApplication, noticeTemplateName),
				relativePath, ecmFileName, moduleName, moduleId, validEmails,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
				fullName, getLocation(preferencesDTO), commPref);
		

		return notice.getEcmId();
	}

	private Location getLocation(PreferencesDTO preferencesDTO) {
		if(preferencesDTO.getLocationDto()==null){
			return null;
		}
		
		Location returnLocation=new Location();
		
		returnLocation.setAddress1(preferencesDTO.getLocationDto().getAddressLine1());
		returnLocation.setAddress2(preferencesDTO.getLocationDto().getAddressLine2());
		returnLocation.setCity(preferencesDTO.getLocationDto().getCity());
		returnLocation.setCounty(preferencesDTO.getLocationDto().getCountyName());
		returnLocation.setCountycode(preferencesDTO.getLocationDto().getCountyCode());
		returnLocation.setState(preferencesDTO.getLocationDto().getState());
		returnLocation.setZip(preferencesDTO.getLocationDto().getZipcode());

		return returnLocation;
	}

	private String getUserName(int householdId){

		return cmrHouseholdRepository.findUserNameByCmrId(householdId);
	}


	private QEPNonFinancialNotificationDTO getQEPNonFinancialNotificationDTO(
			SsapApplication ssapApplication, int moduleId) {
		QEPNonFinancialNotificationDTO qepNonFinancialNotificationDTO = new QEPNonFinancialNotificationDTO();

		qepNonFinancialNotificationDTO.setUserName(getUserName(moduleId));

		qepNonFinancialNotificationDTO.setCaseNumber(ssapApplication
				.getCaseNumber());

		qepNonFinancialNotificationDTO
				.setPrimaryApplicantName(getName(ssapApplication));

		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventService
				.getSsapApplicationEventsByApplicationId(ssapApplication
						.getId());

		if (ssapApplicationEvent != null) {
			qepNonFinancialNotificationDTO
					.setEnrollmentEndDate(ssapApplicationEvent
							.getEnrollmentEndDate());
			
			qepNonFinancialNotificationDTO.setKeepOnly(ssapApplicationEvent
					.getKeepOnly());
		}

		return qepNonFinancialNotificationDTO;
	}

	private String getName(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getFirstName()+ " "+ssapApplicant.getLastName();
			}
		}

		return null;
	}

	private SsapApplication getSSAPApplicationData(String caseNumber) {

		List<SsapApplication> ssapApplications = ssapApplicationRepository
				.findByCaseNumber(caseNumber);
		if (ssapApplications != null && ssapApplications.size() > 0) {
			return ssapApplications.get(0);
		}
		throw new GIRuntimeException(
				"SSAP Id is null.Unable to fetch household id. Email cannot be triggered for"
						+ caseNumber);
	}

	private int extractHouseholdId(SsapApplication ssapApplication) {

		if (ssapApplication.getCmrHouseoldId() != null && ssapApplication.getCmrHouseoldId().intValue() != 0){
			return ssapApplication.getCmrHouseoldId().intValue();
		} else {
			throw new GIRuntimeException("Cmr Household Id ");
		}

	}

	 

	private Map<String, Object> getReplaceableObjectData(QEPNonFinancialNotificationDTO qepNonFinancialNotificationDTO, SsapApplication ssapApplication,String noticeTemplateName)
			throws NoticeServiceException {
		Map<String, Object> tokens = new HashMap<String, Object>();

		tokens.put("QEPNonFinancialNotificationDTO", qepNonFinancialNotificationDTO);
		if(noticeTemplateName.equalsIgnoreCase("SEPEvent") &&  qepNonFinancialNotificationDTO.getEnrollmentEndDate() != null)
		{
			tokens.put("updateSubject", "Your Special Enrollment Period Expires on " + ReferralUtil.formatDate(qepNonFinancialNotificationDTO.getEnrollmentEndDate(),ReferralConstants.DEFDATEPATTERN));

		}
		SimpleDateFormat formatter=new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
		tokens.put("spanishDate", formatter.format(new TSDate()));
		tokens.put("todaysDate", DateUtil.dateToString(new TSDate(), "MMMM dd, YYYY"));
		tokens.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		tokens.put("exgName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		tokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_2, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
		tokens.put("exgCityName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
		tokens.put("exgStateName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
		tokens.put("zip", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
		tokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		tokens.put("exchangeFax",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
		tokens.put("exchangeAddressEmail", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		tokens.put("ssapApplicationId", ssapApplication.getId());


		return tokens;
	}

}
