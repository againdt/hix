package com.getinsured.ssap.model.hub.vlp.documents;

import java.util.Date;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * I-517 document.
 *
 * @author Yevgen Golubenko
 * @since 2/15/19
 */
public class I571Document extends DhsDocument
{
  public I571Document() {
    super.setDocumentType(DocumentType.I571);
  }

  @JsonProperty("AlienNumber")
  private String alienNumber;

  @JsonFormat(pattern = DATE_FORMAT)
  @JsonProperty("DocExpirationDate")
  private Date docExpirationDate;

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isValid()
  {
    return notBlank(this.alienNumber);
  }

  /**
   * {@inheritDoc }
   */
  @Override
  public DhsDocument from(final CitizenshipDocument citizenshipDocument)
  {
    final I571Document document = new I571Document();

    document.setAlienNumber(validValue(citizenshipDocument.getAlienNumber()));
    document.setDocExpirationDate(citizenshipDocument.getDocumentExpirationDate());

    return document;
  }

  public String getAlienNumber()
  {
    return alienNumber;
  }

  public void setAlienNumber(final String alienNumber)
  {
    this.alienNumber = alienNumber;
  }

  public Date getDocExpirationDate()
  {
    return docExpirationDate;
  }

  public void setDocExpirationDate(final Date docExpirationDate)
  {
    this.docExpirationDate = docExpirationDate;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", I571Document.class.getSimpleName() + "[", "]")
        .add("alienNumber='" + alienNumber + "'")
        .add("docExpirationDate=" + docExpirationDate)
        .toString();
  }
}
