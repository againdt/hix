package com.getinsured.hix.ssap;

/**
 *
 * @author Shubhankit Roy
 */

public class Relation {

    public Relation()
    {
        id="0";
        name="";
         
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getRelatedTo() {
        return relatedTo;
    }

    public void setRelatedTo(String relatedTo) {
        this.relatedTo = relatedTo;
    }

    public String getTaxDependent() {
        return taxDependent;
    }

    public void setTaxDependent(String taxDependent) {
        this.taxDependent = taxDependent;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIsTaxDependent() {
        return isTaxDependent;
    }

    public void setIsTaxDependent(boolean isTaxDependent) {
        this.isTaxDependent = isTaxDependent;
    }

    public String getRelationCode() {
        return relationCode;
    }

    public void setRelationCode(String relationCode) {
        this.relationCode = relationCode;
    }

    
    boolean isTaxDependent;
   
    String id,name,relatedTo,taxDependent,relationCode;
    

}
