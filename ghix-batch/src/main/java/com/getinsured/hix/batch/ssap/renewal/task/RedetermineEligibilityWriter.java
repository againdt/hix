package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.eligibility.renewal.util.RenewalConstants;
import com.getinsured.hix.batch.ssap.renewal.service.RedetermineEligibilityService;
import com.getinsured.hix.batch.ssap.renewal.util.EligibilityResponse;
import com.getinsured.hix.batch.ssap.renewal.util.RedetermineEligibilityPartitionerParams;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.iex.ssap.model.SsapApplication;

public class RedetermineEligibilityWriter implements ItemWriter<SsapApplication> {

	private static final Logger LOGGER = LoggerFactory.getLogger(RedetermineEligibilityWriter.class);
	private long jobExecutionId = -1;

	private JobService jobService;
	private RedetermineEligibilityService redetermineEligibilityService;
	private RedetermineEligibilityPartitionerParams redetermineEligibilityPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		jobExecutionId = stepExecution.getJobExecution().getId();
	}

	@Override
	public void write(List<? extends SsapApplication> ssapApplicationListToWriter) throws Exception {

		String batchJobStatus = null;
		if (jobService != null && jobExecutionId != -1) {
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}

		if (batchJobStatus != null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING)
				|| batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))) {
			throw new UnexpectedJobExecutionException(EnrollmentConstants.BATCH_STOP_MSG);
		}

		if (CollectionUtils.isNotEmpty(ssapApplicationListToWriter)) {
			Long renewalYear = new Long(DynamicPropertiesUtil
					.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));

			for (SsapApplication ssapApplication : ssapApplicationListToWriter) {
				try {
					EligibilityResponse result = redetermineEligibilityService.invokeEligibilityEngine(ssapApplication.getId());
					LOGGER.info("redetermineEligibilityPartitionerParams OutboundAt: {} and status: {} ", redetermineEligibilityPartitionerParams.getOutboundAt(), result.getStatus());
					if (redetermineEligibilityPartitionerParams != null
							&& StringUtils.isNotBlank(redetermineEligibilityPartitionerParams.getOutboundAt())
							&& "Y".equalsIgnoreCase(redetermineEligibilityPartitionerParams.getOutboundAt())) {
						if(result!=null && StringUtils.isNotEmpty(result.getStatus()) && ("200".equalsIgnoreCase(result.getStatus()) || RenewalConstants.SUCCESS.equalsIgnoreCase(result.getStatus()))){
							redetermineEligibilityService.invokeOutboundAT(ssapApplication.getId());
						}
					}
				} catch (Exception exception) {
					String errorMessage = "Error in executing redetermine eligibility in writer." + "\n"+ ExceptionUtils.getFullStackTrace(exception);
					LOGGER.error(errorMessage);
					redetermineEligibilityService.saveAndThrowsErrorLog(errorMessage, "RENEWALBATCH_60007");
				}
			}

			LOGGER.info("Successfully executed Redetermine Eligibility For Renewal year: {} and applicationIds: {} ",
					renewalYear, redetermineEligibilityPartitionerParams.getSsapApplicationIdList());
		}
	}
	
	

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public RedetermineEligibilityService getRedetermineEligibilityService() {
		return redetermineEligibilityService;
	}

	public void setRedetermineEligibilityService(RedetermineEligibilityService redetermineEligibilityService) {
		this.redetermineEligibilityService = redetermineEligibilityService;
	}

	public RedetermineEligibilityPartitionerParams getRedetermineEligibilityPartitionerParams() {
		return redetermineEligibilityPartitionerParams;
	}

	public void setRedetermineEligibilityPartitionerParams(
			RedetermineEligibilityPartitionerParams redetermineEligibilityPartitionerParams) {
		this.redetermineEligibilityPartitionerParams = redetermineEligibilityPartitionerParams;
	}

}
