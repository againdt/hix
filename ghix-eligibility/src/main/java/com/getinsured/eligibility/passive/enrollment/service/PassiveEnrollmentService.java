package com.getinsured.eligibility.passive.enrollment.service;

public interface PassiveEnrollmentService {

	Long execute(String ssapJson);

}
