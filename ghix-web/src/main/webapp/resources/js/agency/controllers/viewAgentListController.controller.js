(function() {
'use strict';

angular.module('agencyApp').controller('ViewAgentListController',ViewAgentListController);

	ViewAgentListController.$inject = ['$scope', '$http', 'StatusService'];

	function ViewAgentListController($scope, $http, StatusService) {
		var vm = this;
		var agentListUrl = '/hix/agency/agent/list/' + $('#agencyId').val();
		var agentSearchUrl = '/hix/agency/agent/list/';
		var logoutUrl = $('#logoutUrl').val();

		vm.init = init;
		vm.getData = getData;
		vm.getSorting = getSorting;
		vm.reset = reset;
		vm.isResultsFound = true;
		vm.sortResult = false;
		vm.currentUserRole = StatusService.CurrentUserRole;
		vm.adminSwitchToAgency = StatusService.AdminSwitchToAgency;
		vm.adminStaffApprovalStatus = StatusService.AdminStaffApprovalStatus;
		vm.adminStaffActivityStatus = StatusService.AdminStaffActivityStatus;

		function init(){
			vm.certificationStatus = StatusService.CertificationStatus;
			vm.paginationData = {
				totalRecords: 0
			}
			vm.inputData = {
				firstName : '',
				lastName : '',
				status : '',
				certificationStatus: vm.certificationStatus,
        		licenseNumber : '',
        		sortBy: undefined,
				sortOrder: 'asc',
				pageNumber: 1
			}
			vm.filterData = {};
			getData();
		}

		function getData() {
			vm.loader = true;

			if(vm.refineResult || vm.paginationResult){
				vm.filterData = {};
				var properties = ['firstName', 'lastName','licenseNumber'];
				for (var i=0; i<properties.length; i++) {
					if(vm.inputData[properties[i]] !== '' && vm.inputData[properties[i]] !== undefined) {
						vm.filterData[properties[i]] = vm.inputData[properties[i]];
					}
				}
				if(vm.inputData.status && vm.inputData.status != '') {
					vm.filterData.status = vm.inputData.status;
				}
				if(vm.inputData.certificationStatus && vm.inputData.certificationStatus != '') {
					vm.filterData.certificationStatus = vm.inputData.certificationStatus;
				}
			}
			if(vm.refineResult) {
				vm.filterData.pageNumber = 1;
				vm.inputData.pageNumber = 1;
			}
			if (vm.paginationResult) {
				vm.filterData.pageNumber = vm.inputData.pageNumber;
			}
			else {
				vm.filterData.pageNumber = 1;
			}
			if(vm.inputData.sortOrder && vm.inputData.sortOrder !== undefined) {
				vm.filterData.sortOrder = vm.inputData.sortOrder;
			}
			if(vm.inputData.sortBy && vm.inputData.sortBy !== undefined) {
				vm.filterData.sortBy = vm.inputData.sortBy;
			}
			var promise = $http.post(agentSearchUrl, vm.filterData);
			promise.success(function(response){
				vm.loader = false;
				vm.refineResult = false;
				vm.paginationResult = false;
				if (response.totalRecords > 0) {
					vm.isResultsFound = true;
					if ( vm.certificationStatus !== 'TERMINATED' ) {
						vm.agentList = response.brokersList;
						vm.certificationStatusList = response.certificationStatus;
						vm.paginationData.totalRecords = response.totalRecords
					}
				} else {
					vm.isResultsFound = false;
				}
			})
			promise.error(function(response){
				vm.loader = false;
				vm.refineResult = false;
			})
		}


		function getSorting(column){
			vm.inputData.sortBy = column;
			if(vm.inputData.sortOrder === undefined || vm.inputData.sortOrder === null || vm.inputData.sortOrder === 'desc'){
				vm.inputData.sortOrder = 'asc';
			}else{
				vm.inputData.sortOrder = 'desc';
			}
			getData();
		}

		function reset() {
			vm.inputData = {};
			vm.filterData = {};
			vm.refineResult = true;
			getData();
		}
    }

})();
