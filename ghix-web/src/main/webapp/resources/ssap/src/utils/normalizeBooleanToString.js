export const normalizeBooleanToString = (...payload) => {
    return payload.map(v => {
        if(typeof v !== 'boolean') {
            throw new Error('You passed to normalizeBooleanToString not a Boolean');
        }
        switch(v) {
        case true:
            return 'true';
        case false:
            return 'false';
        }
    }).join(' + ');
};