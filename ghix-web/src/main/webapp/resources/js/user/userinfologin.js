//1.7. Will current applicant be claimed as a dependent on someone else's federal income tax return next year? 
  

//Validating old password length and characters	
		jQuery.validator.addMethod("validateOldpassword", function(value,
				element, param) {
			oldpassword = $("#current-pswd").val();
			var re = /(?=^.{8,20}$)(?=.*[a-zA-Z])(?=.*[0-9])(?!.*\s).*$/; 

			if (!re.test(password)) {
				return false;
			}
			return true;

		});
		
		
		//Validating new password length and characters		
		jQuery.validator.addMethod("validatepassword", function(value,
				element, param) {
			//checkUserPassword(element,'CHK_COMPLEXITY');
			password = $("#newpassword").val();
			var re = RegExp(($("#passwordPolicyRegex").val()));///(?=^.{8,20}$)(?=.*[a-zA-Z])(?=.*[0-9])(?!.*\s).*$/; 
			if (!re.test(password)) {
				return false;
			}
			return true;
		});
		
		jQuery.validator.addMethod("validatepasswordforminage", function(value,
				element, param) {
			//checkUserPassword(element,'CHK_MIN_AGE');
		});
		
		   //Validating confirm password and new password length and characters	
		jQuery.validator.addMethod("passwordcheck", function(value,
				element, param) {
			password = $("#newpassword").val();
			confirmed = $("#confirmpassword").val();

			if (password != confirmed) {
				return false;
			}
			return true;
		});			
function goToPage(url) {
	
	window.location=url;
	 return false;
}



	var validator = $("#pwdchangeform").validate(
				{
					rules : {
						currentpswd :{
							required : true
							//validateOldpassword :true
						},
						
						newpassword : { 
							required : true,
							validatepassword : true
							
						},
						confirmpassword : { 
							required : true, 
							passwordcheck : true
						}									
					},
					messages : {
					
						currentpswd:{
							required : "<span> <em class='excl'>!</em>Please enter your current password.</span>"
							//validateOldpassword : "<span> <em class='excl'>!</em> Please enter valid Password, the password must be at least 8 characters long with at least one letter and one number, no spaces. </span>"		
						},

						newpassword: { 
							required : "<span> <em class='excl'>!</em>Please enter your new password.</span>",
							validatepassword : "<span> <em class='excl'>!</em> "+$("#passwordPolicyErrorMsg").val()+" </span>"
							//validatepasswordforminage : "<span> <em class='excl'>!</em> Validating Password... </span>"
						},
						confirmpassword: { 
							required : "<span> <em class='excl'>!</em>Please confirm your new password.</span>",
							passwordcheck : "<span> <em class='excl'>!</em> Password and confirm password don't match.</span>"
						}									
					},
					onkeyup : false,
					errorClass : "error",
					errorPlacement : function(error, element) {
						var elementId = element.attr('id');
						error.appendTo($("#" + elementId+ "_error"));
						$("#" + elementId + "_error").attr('class','error help-inline');
					}
					
				});
			


function checkUserPassword()
{ 	
	var validateUrl = "/hix/account/signup/checkResetUserPasswordUserLogin";//"<c:url value='/account/signup/checkUserPassword'/>";
	$.ajax({
		url : validateUrl,
		type : "POST",
		data : {
			csrftoken: $("#csrftoken").val(), password : $("#newpassword").val(), action : 'LOGIN_CHANGE_PWD', email : $("#authfailedpasswordusername").val()
					
		},
		success: function(response)
		{
			if (response!="")
			{
				if(response.indexOf('accountlocked') >= 0 ){
					$('#newpassword_error').html("");
					$('#newpassword_error').removeAttr("class");
					//$("#pwdchangeform").append('<input type = "hidden" name="errorCode" value="accountLocked" />');
					$("#pwdchangeform").attr("action", "/hix/account/user/accountLockedOnChangePassword");
					$("#pwdchangeform").submit(); 
				}else{
					$('#newpassword_error').html("<label class='error'><span> <em class='excl'>!</em>"+response+"</span></label>");
					$('#newpassword_error').attr('class','error help-inline');
					return false;
				}
			}
			else{
				

						$('#newpassword_error').html("");
						$('#newpassword_error').removeAttr("class");
						
						$("#pwdchangeform").attr("action", "/hix/account/user/passwordchangeforlogin");
						$("#submitRedirectURL").attr("value", "/account/user/loginfailed");//value("/account/user/login");
						$("#email").attr("value", $("#authfailedpasswordusername").val());
						$("#pwdchangeform").submit(); 
						
				
			}
		},
		
	});
}

