//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.04.29 at 08:34:31 AM IST 
//


package com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmployeeStatusMessageCodeSimpleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EmployeeStatusMessageCodeSimpleType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Active"/>
 *     &lt;enumeration value="On International Assignment"/>
 *     &lt;enumeration value="Casual"/>
 *     &lt;enumeration value="On Long Term Disability"/>
 *     &lt;enumeration value="Sick Leave"/>
 *     &lt;enumeration value="Surviving Spouse"/>
 *     &lt;enumeration value="No Longer Employed"/>
 *     &lt;enumeration value="Inactive"/>
 *     &lt;enumeration value="On Leave"/>
 *     &lt;enumeration value="Multiple Positions"/>
 *     &lt;enumeration value="New Employee"/>
 *     &lt;enumeration value="Lay Off"/>
 *     &lt;enumeration value="Part Time"/>
 *     &lt;enumeration value="Retired"/>
 *     &lt;enumeration value="Separated"/>
 *     &lt;enumeration value="Seasonal"/>
 *     &lt;enumeration value="Temporary"/>
 *     &lt;enumeration value="Intern"/>
 *     &lt;enumeration value="Transferred"/>
 *     &lt;enumeration value="Deceased"/>
 *     &lt;enumeration value="Severed with Pay"/>
 *     &lt;enumeration value="Currently Employed"/>
 *     &lt;enumeration value="On Sabbatical"/>
 *     &lt;enumeration value="Part of Divested Population"/>
 *     &lt;enumeration value="Temporarily Inactive"/>
 *     &lt;enumeration value="Full-Time"/>
 *     &lt;enumeration value="Non-employee Beneficiary"/>
 *     &lt;enumeration value="Not Currently on Assignment"/>
 *     &lt;enumeration value="Not Currently on Payroll"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EmployeeStatusMessageCodeSimpleType")
@XmlEnum
public enum EmployeeStatusMessageCodeSimpleType {


    /**
     * Active
     * 
     */
    @XmlEnumValue("Active")
    ACTIVE("Active"),

    /**
     * On International Assignment
     * 
     */
    @XmlEnumValue("On International Assignment")
    ON_INTERNATIONAL_ASSIGNMENT("On International Assignment"),

    /**
     * Casual
     * 
     */
    @XmlEnumValue("Casual")
    CASUAL("Casual"),

    /**
     * On Long Term Disability
     * 
     */
    @XmlEnumValue("On Long Term Disability")
    ON_LONG_TERM_DISABILITY("On Long Term Disability"),

    /**
     * Sick Leave
     * 
     */
    @XmlEnumValue("Sick Leave")
    SICK_LEAVE("Sick Leave"),

    /**
     * Surviving Spouse
     * 
     */
    @XmlEnumValue("Surviving Spouse")
    SURVIVING_SPOUSE("Surviving Spouse"),

    /**
     * No Longer Employed
     * 
     */
    @XmlEnumValue("No Longer Employed")
    NO_LONGER_EMPLOYED("No Longer Employed"),

    /**
     * Inactive
     * 
     */
    @XmlEnumValue("Inactive")
    INACTIVE("Inactive"),

    /**
     * On Leave
     * 
     */
    @XmlEnumValue("On Leave")
    ON_LEAVE("On Leave"),

    /**
     * Multiple Positions
     * 
     */
    @XmlEnumValue("Multiple Positions")
    MULTIPLE_POSITIONS("Multiple Positions"),

    /**
     * New Employee
     * 
     */
    @XmlEnumValue("New Employee")
    NEW_EMPLOYEE("New Employee"),

    /**
     * Lay Off
     * 
     */
    @XmlEnumValue("Lay Off")
    LAY_OFF("Lay Off"),

    /**
     * Part Time
     * 
     */
    @XmlEnumValue("Part Time")
    PART_TIME("Part Time"),

    /**
     * Retired
     * 
     */
    @XmlEnumValue("Retired")
    RETIRED("Retired"),

    /**
     * Separated
     * 
     */
    @XmlEnumValue("Separated")
    SEPARATED("Separated"),

    /**
     * Seasonal
     * 
     */
    @XmlEnumValue("Seasonal")
    SEASONAL("Seasonal"),

    /**
     * Temporary
     * 
     */
    @XmlEnumValue("Temporary")
    TEMPORARY("Temporary"),

    /**
     * Intern
     * 
     */
    @XmlEnumValue("Intern")
    INTERN("Intern"),

    /**
     * Transferred
     * 
     */
    @XmlEnumValue("Transferred")
    TRANSFERRED("Transferred"),

    /**
     * Deceased
     * 
     */
    @XmlEnumValue("Deceased")
    DECEASED("Deceased"),

    /**
     * Severed with Pay
     * 
     */
    @XmlEnumValue("Severed with Pay")
    SEVERED_WITH_PAY("Severed with Pay"),

    /**
     * Currently Employed
     * 
     */
    @XmlEnumValue("Currently Employed")
    CURRENTLY_EMPLOYED("Currently Employed"),

    /**
     * On Sabbatical
     * 
     */
    @XmlEnumValue("On Sabbatical")
    ON_SABBATICAL("On Sabbatical"),

    /**
     * Part of Divested Population
     * 
     */
    @XmlEnumValue("Part of Divested Population")
    PART_OF_DIVESTED_POPULATION("Part of Divested Population"),

    /**
     * Temporarily Inactive
     * 
     */
    @XmlEnumValue("Temporarily Inactive")
    TEMPORARILY_INACTIVE("Temporarily Inactive"),

    /**
     * Full-Time
     * 
     */
    @XmlEnumValue("Full-Time")
    FULL_TIME("Full-Time"),

    /**
     * Non-employee Beneficiary
     * 
     */
    @XmlEnumValue("Non-employee Beneficiary")
    NON_EMPLOYEE_BENEFICIARY("Non-employee Beneficiary"),

    /**
     * Not Currently on Assignment
     * 
     */
    @XmlEnumValue("Not Currently on Assignment")
    NOT_CURRENTLY_ON_ASSIGNMENT("Not Currently on Assignment"),

    /**
     * Not Currently on Payroll
     * 
     */
    @XmlEnumValue("Not Currently on Payroll")
    NOT_CURRENTLY_ON_PAYROLL("Not Currently on Payroll");
    private final String value;

    EmployeeStatusMessageCodeSimpleType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EmployeeStatusMessageCodeSimpleType fromValue(String v) {
        for (EmployeeStatusMessageCodeSimpleType c: EmployeeStatusMessageCodeSimpleType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
