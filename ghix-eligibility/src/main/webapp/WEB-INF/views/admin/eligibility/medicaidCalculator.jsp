
<%@page import="com.getinsured.hix.model.estimator.mini.MedicaidResponse"%>
<%@page import="com.getinsured.hix.model.estimator.mini.MedicaidRequest"%>
<%@page import="com.getinsured.hix.model.estimator.mini.FamilyMember"%>
<%@page import="com.getinsured.eligibility.prescreen.calculator.MedicaidCalculator"%>
<%@include file="datasource.jsp"%>

<html>
<head>
<title>State Specific Medicaid/CHIP calculator</title>

<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<script type="text/javascript">
	$.datepicker.setDefaults({
		changeYear : true,
		yearRange : "1913:2013"
	});

	$(function() {
		$("#dob1").datepicker().attr('readOnly', 'true');
	});

	$(function() {
		$("#dob2").datepicker().attr('readOnly', 'true');
		;
	});

	$(function() {
		$("#dob3").datepicker().attr('readOnly', 'true');
	});

	$(function() {
		$("#dob4").datepicker().attr('readOnly', 'true');
	});

	function validate() {

		var stateCode = document.getElementById('stateCode').value;

		if (!stateCode) {
			alert('Enter State code');
			return false;
		}

		var fpl = document.getElementById('fpl').value;

		if (!fpl || isNaN(fpl)) {
			alert('Invalid FPL value');
			return false;
		}

	}
</script>
<style>
table {
	border: 1px solid #666;
	width: 80%;
	margin: 20px 0 20px 0 !important;
	font-size: 14px;
}

th,td {
	padding: 2px 4px 2px 4px !important;
	text-align: left;
	vertical-align: top;	
}

thead tr {
	background-color: #fc0;
}

th.sorted {
	background-color: orange;
}

th a,th a:visited {
	color: black;
}

th a:hover {
	text-decoration: underline;
	color: black;
}

th.sorted a,th.sortable a {
	background-position: right;
	display: block;
	width: 100%;
}
tr.odd {
	background-color: #fff
}

tr.tableRowEven,tr.even {
	background-color: #fea
}

div.exportlinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	margin: 2px 0 10px 0;
	width: 79%;
}

span.export {
	padding: 0 4px 1px 20px;
	display: inline;
	display: inline-block;
	cursor: pointer;
}

span.pagebanner {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	margin-top: 10px;
	display: block;
	border-bottom: none;
}

span.pagelinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	display: block;
	border-top: none;
	margin-bottom: -5px;
}


.group-1 {
    font-weight:bold;
    padding-bottom:10px;
    border-top:1px solid black;
}
.group-2 {
    font-style:italic;
    border-top: 1px solid black;

}
.subtotal-sum, .grandtotal-sum {
    font-weight:bold;
    text-align:right;
}
.subtotal-header {
    padding-bottom: 0px;
    border-top: 1px solid white;
}
.subtotal-label, .grandtotal-label {
    border-top: 1px solid white;
    font-weight: bold;
}
.grouped-table tr.even {
    background-color: #fff;
}
.grouped-table tr.odd {
    background-color: #fff;
}
.grandtotal-row {
    border-top: 2px solid black;
}
</style>
</head>
<body>
	<b>State Specific Medicaid/CHIP calculator API</b><br><br>
	
	<form action="medicaidCalculator" method="POST" onsubmit="return(validate());">
		<table border="0" cellspacing="10">
			<tr>
				<td>State Code :</td> 
				<td><input id="stateCode" name="stateCode" type="text" size="5" maxlength="2"></td>
			</tr>
			<tr>	
				<td>Family Size : </td>
				<td><input id="size" name="size" type="text" value="4" size="5" maxlength="2" readonly="readonly"></td>
			</tr>
			<tr>	
				<td>FPL : </td>
				<td><input id="fpl" name="fpl" type="text" size="5" maxlength="3"></td>
			</tr>
		</table>
		<b>Enter DOBs for members who are seeking coverage</b>
		<table>	
			<tr>	
				<td>Family Member 1 : </td>
				<td><input id="dob1" name="dob1" type="text" size="10" maxlength="10" placeholder="mm/dd/yyyy"></td>
			</tr>
			<tr>	
				<td>Family Member 2 : </td>
				<td><input id="dob2" name="dob2" type="text" size="10" maxlength="10" placeholder="mm/dd/yyyy"></td>
			</tr>
			<tr>	
				<td>Family Member 3 : </td>
				<td><input id="dob3" name="dob3" type="text" size="10" maxlength="10" placeholder="mm/dd/yyyy"></td>
			</tr>
			<tr>	
				<td>Family Member 4 : </td>
				<td><input id="dob4" name="dob4" type="text" size="10" maxlength="10" placeholder="mm/dd/yyyy"></td>
			</tr>
			<tr>
				<td><input type="submit" value="Calculate"></td>
			</tr>
		</table>
	</form>
	
</body>

<%
	MedicaidCalculator calculator = null;
%>

<%
	if(request.getParameter("stateCode") != null && request.getParameter("stateCode").trim().length() > 0){

	calculator = (MedicaidCalculator) ctx.getBean("medicaidCalculator");
	
	FamilyMember member = new FamilyMember();
	member.setDateOfBirth(request.getParameter("dob1"));
	FamilyMember spouse = new FamilyMember();
	spouse.setDateOfBirth(request.getParameter("dob2"));
	FamilyMember childOne = new FamilyMember();
	childOne.setDateOfBirth(request.getParameter("dob3"));
	FamilyMember childTwo = new FamilyMember();
	childTwo.setDateOfBirth(request.getParameter("dob4"));
	
	List<FamilyMember> members = new ArrayList<FamilyMember>();
	members.add(member);
	members.add(spouse);
	members.add(childOne);
	members.add(childTwo);
	
	MedicaidRequest medicaidrequest = new MedicaidRequest();
	medicaidrequest.setStateCode(request.getParameter("stateCode").toUpperCase());
	medicaidrequest.setFplPercentage(Double.parseDouble(request.getParameter("fpl")));
	medicaidrequest.setMembers(members);
	
	MedicaidResponse medicaidResponse = calculator.calculateMedicaidAndCHIPEligibility(medicaidrequest);
	
	if(medicaidResponse != null && medicaidResponse.getErrCode() == 0){
		
		out.println("<b>Results</b><br>");
		
		out.println("<table>");
		
		out.println("<tr>");
		out.println("<td>State</td>");
		out.println("<td>" + medicaidrequest.getStateCode() + "</td>");
		out.println("</tr>");
		
		out.println("<tr>");
		out.println("<td>FPL (%)</td>");
		out.println("<td>" + medicaidrequest.getFplPercentage() + "</td>");
		out.println("</tr>");
		
		out.println("<tr>");
		out.println("<td>Household Medicaid Eligible</td>");
		out.println("<td>" + medicaidResponse.isHouseholdMedicaidEligible() + "</td>");
		out.println("</tr>");
		
		out.println("<tr>");
		out.println("<td>Household CHIP Eligible</td>");
		out.println("<td>" + medicaidResponse.isHouseholdCHIPEligible() + "</td>");
		out.println("</tr>");
		
		out.println("<tr>");
		out.println("<td>Household Medicare Eligible</td>");
		out.println("<td>" + medicaidResponse.isHouseholdMedicareEligible() + "</td>");
		out.println("</tr>");
		
		for(FamilyMember record : medicaidResponse.getMembers()){
	
				out.println("<tr>");
				out.println("<td>Member "+record.getSequenceNumber()+"</td>");
				out.println("<td>" + record.getMemberEligibility() + "</td>");
				out.println("</tr>");
		}
		
		out.println("<tr>");
		out.println("<td>SBE Website</td>");
		out.println("<td>" + medicaidResponse.getSbeWebsite() + "</td>");
		out.println("</tr>");
		
		out.println("<tr>");
		out.println("<td>SBE PHONE</td>");
		out.println("<td>" + medicaidResponse.getSbePhone() + "</td>");
		out.println("</tr>");
		
		out.println("</table>");
	}
	
	if(medicaidResponse != null && medicaidResponse.getErrCode() != 0){
		
		out.println("<b>Error occured during calculation</b><br>");
		
		out.println("<b>Error Code : "+medicaidResponse.getErrCode()+"</b><br>");
		out.println("<b>Error Message : "+medicaidResponse.getErrMsg()+"</b><br>");
		
	}
}
%>

</html>