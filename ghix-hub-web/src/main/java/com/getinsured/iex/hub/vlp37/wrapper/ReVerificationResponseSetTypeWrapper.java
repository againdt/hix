package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav.ReverificationResponseSetType;
import com.getinsured.iex.hub.vlp37.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp37.service.VLPServiceUtil;


public class ReVerificationResponseSetTypeWrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReVerificationResponseSetTypeWrapper.class);
	
	private String caseNumber;
	private String nonCitLastName;
	private String nonCitFirstName;
	private String nonCitMiddleName;
	private String nonCitBirthDate;
	private String nonCitEntryDate;
	private String nonCitAdmittedToDate;
	private String nonCitAdmittedToText;
	private String nonCitCountryBirthCd;
	private String nonCitCountryCitCd;
	private String nonCitCoaCode;
	private String nonCitEadsExpireDate;
	private int eligStatementCd;
	private String eligStatementTxt;
	private String webServSftwrVer;
	private String grantDate;
	private String grantDateReasonCd;
	private boolean sponsorDataFoundIndicator;
	private ArrayOfSponsorshipDataTypeWrapper arrayOfSponsorshipData;
	private String sponsorshipReasonCd;
	private boolean photoIncludedIndicator;
	private byte[] photoBinaryAttachment;
	private boolean caseSentToSecondaryIndicator;
	private boolean dshAutoTriggerStepTwo;
	private String fiveYearBarApplyCode;
	private String qualifiedNonCitizenCode;
	private String fiveYearBarMetCode;
	private String usCitizenCode;
	
	    
    public ReVerificationResponseSetTypeWrapper(ReverificationResponseSetType reverificationResponseSetType){
    	
    	if (reverificationResponseSetType == null){
    		return;
    	}
    	
    	this.caseNumber = reverificationResponseSetType.getCaseNumber();
    	this.nonCitLastName = reverificationResponseSetType.getNonCitLastName();
    	this.nonCitFirstName = reverificationResponseSetType.getNonCitFirstName();
    	this.nonCitMiddleName = reverificationResponseSetType.getNonCitLastName();
    	try{
	    	if(reverificationResponseSetType.getNonCitBirthDate() != null){
	    		this.nonCitBirthDate = VLPServiceUtil.xmlDateToString(reverificationResponseSetType.getNonCitBirthDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(reverificationResponseSetType.getNonCitEntryDate() != null){
	    		this.nonCitEntryDate = VLPServiceUtil.xmlDateToString(reverificationResponseSetType.getNonCitEntryDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	/*if(reverificationResponseSetType.getNonCitAdmittedToDate() != null){
	    		this.nonCitAdmittedToDate = VLPServiceUtil.xmlDateToString(reverificationResponseSetType.getNonCitAdmittedToDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}*/
	    	if(reverificationResponseSetType.getNonCitEadsExpireDate() != null){
	    		this.nonCitEadsExpireDate = VLPServiceUtil.xmlDateToString(reverificationResponseSetType.getNonCitEadsExpireDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(reverificationResponseSetType.getGrantDate() != null){
	    		this.grantDate = VLPServiceUtil.xmlDateToString(reverificationResponseSetType.getGrantDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
    	}
    	catch(Exception e){
    		LOGGER.error("Exception occured while setting date",e);
    	}


    	//this.nonCitAdmittedToText = reverificationResponseSetType.getNonCitAdmittedToText();
    	this.nonCitCountryBirthCd = reverificationResponseSetType.getNonCitCountryBirthCd();
    	this.nonCitCountryCitCd = reverificationResponseSetType.getNonCitCountryCitCd();
    	this.nonCitCoaCode = reverificationResponseSetType.getNonCitCoaCode();
    	this.eligStatementCd = reverificationResponseSetType.getEligStatementCd().intValue();
    	this.eligStatementTxt = reverificationResponseSetType.getEligStatementTxt();
    	this.webServSftwrVer = reverificationResponseSetType.getWebServSftwrVer();
    	this.grantDateReasonCd = reverificationResponseSetType.getGrantDateReasonCd();
    	this.sponsorDataFoundIndicator = reverificationResponseSetType.isSponsorDataFoundIndicator() == null ? false : reverificationResponseSetType.isSponsorDataFoundIndicator();
      	this.arrayOfSponsorshipData = new ArrayOfSponsorshipDataTypeWrapper(reverificationResponseSetType.getArrayOfSponsorshipData());
    	
    	this.sponsorshipReasonCd = reverificationResponseSetType.getSponsorshipReasonCd();
    	this.fiveYearBarApplyCode = reverificationResponseSetType.getFiveYearBarApplyCode();
    	this.qualifiedNonCitizenCode = reverificationResponseSetType.getQualifiedNonCitizenCode();
    	this.fiveYearBarMetCode = reverificationResponseSetType.getFiveYearBarMetCode();
    	this.usCitizenCode = reverificationResponseSetType.getUSCitizenCode();
    }
    
	
    
	public String getCaseNumber() {
		return caseNumber;
	}



	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}



	public String getNonCitLastName() {
		return nonCitLastName;
	}



	public void setNonCitLastName(String nonCitLastName) {
		this.nonCitLastName = nonCitLastName;
	}



	public String getNonCitFirstName() {
		return nonCitFirstName;
	}



	public void setNonCitFirstName(String nonCitFirstName) {
		this.nonCitFirstName = nonCitFirstName;
	}



	public String getNonCitMiddleName() {
		return nonCitMiddleName;
	}



	public void setNonCitMiddleName(String nonCitMiddleName) {
		this.nonCitMiddleName = nonCitMiddleName;
	}



	public String getNonCitBirthDate() {
		return nonCitBirthDate;
	}



	public void setNonCitBirthDate(String nonCitBirthDate) {
		this.nonCitBirthDate = nonCitBirthDate;
	}



	public String getNonCitEntryDate() {
		return nonCitEntryDate;
	}



	public void setNonCitEntryDate(String nonCitEntryDate) {
		this.nonCitEntryDate = nonCitEntryDate;
	}



	public String getNonCitAdmittedToDate() {
		return nonCitAdmittedToDate;
	}



	public void setNonCitAdmittedToDate(String nonCitAdmittedToDate) {
		this.nonCitAdmittedToDate = nonCitAdmittedToDate;
	}



	public String getNonCitAdmittedToText() {
		return nonCitAdmittedToText;
	}



	public void setNonCitAdmittedToText(String nonCitAdmittedToText) {
		this.nonCitAdmittedToText = nonCitAdmittedToText;
	}



	public String getNonCitCountryBirthCd() {
		return nonCitCountryBirthCd;
	}



	public void setNonCitCountryBirthCd(String nonCitCountryBirthCd) {
		this.nonCitCountryBirthCd = nonCitCountryBirthCd;
	}



	public String getNonCitCountryCitCd() {
		return nonCitCountryCitCd;
	}



	public void setNonCitCountryCitCd(String nonCitCountryCitCd) {
		this.nonCitCountryCitCd = nonCitCountryCitCd;
	}



	public String getNonCitCoaCode() {
		return nonCitCoaCode;
	}



	public void setNonCitCoaCode(String nonCitCoaCode) {
		this.nonCitCoaCode = nonCitCoaCode;
	}



	public String getNonCitEadsExpireDate() {
		return nonCitEadsExpireDate;
	}



	public void setNonCitEadsExpireDate(String nonCitEadsExpireDate) {
		this.nonCitEadsExpireDate = nonCitEadsExpireDate;
	}



	public int getEligStatementCd() {
		return eligStatementCd;
	}



	public void setEligStatementCd(int eligStatementCd) {
		this.eligStatementCd = eligStatementCd;
	}



	public String getEligStatementTxt() {
		return eligStatementTxt;
	}



	public void setEligStatementTxt(String eligStatementTxt) {
		this.eligStatementTxt = eligStatementTxt;
	}

	public String getWebServSftwrVer() {
		return webServSftwrVer;
	}

	public void setWebServSftwrVer(String webServSftwrVer) {
		this.webServSftwrVer = webServSftwrVer;
	}



	public String getGrantDate() {
		return grantDate;
	}



	public void setGrantDate(String grantDate) {
		this.grantDate = grantDate;
	}



	public String getGrantDateReasonCd() {
		return grantDateReasonCd;
	}



	public void setGrantDateReasonCd(String grantDateReasonCd) {
		this.grantDateReasonCd = grantDateReasonCd;
	}



	public boolean isSponsorDataFoundIndicator() {
		return sponsorDataFoundIndicator;
	}



	public void setSponsorDataFoundIndicator(boolean sponsorDataFoundIndicator) {
		this.sponsorDataFoundIndicator = sponsorDataFoundIndicator;
	}



	public ArrayOfSponsorshipDataTypeWrapper getArrayOfSponsorshipData() {
		return arrayOfSponsorshipData;
	}



	public void setArrayOfSponsorshipData(
			ArrayOfSponsorshipDataTypeWrapper arrayOfSponsorshipData) {
		this.arrayOfSponsorshipData = arrayOfSponsorshipData;
	}



	public String getSponsorshipReasonCd() {
		return sponsorshipReasonCd;
	}



	public void setSponsorshipReasonCd(String sponsorshipReasonCd) {
		this.sponsorshipReasonCd = sponsorshipReasonCd;
	}



	public boolean isPhotoIncludedIndicator() {
		return photoIncludedIndicator;
	}



	public void setPhotoIncludedIndicator(boolean photoIncludedIndicator) {
		this.photoIncludedIndicator = photoIncludedIndicator;
	}



	public byte[] getPhotoBinaryAttachment() {
		
		if (photoBinaryAttachment == null) {
            return new byte[0];
		}
		
		byte [] attachment = new byte[photoBinaryAttachment.length];
		
		System.arraycopy(photoBinaryAttachment, 0, attachment, 0, attachment.length);
		
		return attachment;
	}



	public void setPhotoBinaryAttachment(byte[] photoBinaryAttachment) {
		
		if (null == photoBinaryAttachment) {
            return;
		}

		System.arraycopy(photoBinaryAttachment, 0, this.photoBinaryAttachment, 0,
    		 photoBinaryAttachment.length);

	}



	public boolean isCaseSentToSecondaryIndicator() {
		return caseSentToSecondaryIndicator;
	}



	public void setCaseSentToSecondaryIndicator(boolean caseSentToSecondaryIndicator) {
		this.caseSentToSecondaryIndicator = caseSentToSecondaryIndicator;
	}



	public boolean isDshAutoTriggerStepTwo() {
		return dshAutoTriggerStepTwo;
	}



	public void setDshAutoTriggerStepTwo(boolean dshAutoTriggerStepTwo) {
		this.dshAutoTriggerStepTwo = dshAutoTriggerStepTwo;
	}



	public String getFiveYearBarApplyCode() {
		return fiveYearBarApplyCode;
	}



	public void setFiveYearBarApplyCode(String fiveYearBarApplyCode) {
		this.fiveYearBarApplyCode = fiveYearBarApplyCode;
	}



	public String getQualifiedNonCitizenCode() {
		return qualifiedNonCitizenCode;
	}



	public void setQualifiedNonCitizenCode(String qualifiedNonCitizenCode) {
		this.qualifiedNonCitizenCode = qualifiedNonCitizenCode;
	}



	public String getFiveYearBarMetCode() {
		return fiveYearBarMetCode;
	}



	public void setFiveYearBarMetCode(String fiveYearBarMetCode) {
		this.fiveYearBarMetCode = fiveYearBarMetCode;
	}



	public String getUsCitizenCode() {
		return usCitizenCode;
	}



	public void setUsCitizenCode(String usCitizenCode) {
		this.usCitizenCode = usCitizenCode;
	}



	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.caseNumber != null){
			obj.put("CaseNumber",this.caseNumber);
		}

		if(this.nonCitLastName != null){
			obj.put("NonCitLastName",this.nonCitLastName);
		}

		if(this.nonCitFirstName != null){
			obj.put("NonCitFirstName",this.nonCitFirstName);
		}

		if(this.nonCitMiddleName != null){
			obj.put("NonCitMiddleName",this.nonCitMiddleName);
		}

		if(this.nonCitBirthDate != null){
			obj.put("NonCitBirthDate",this.nonCitBirthDate);
		}

		if(this.nonCitEntryDate != null){
			obj.put("NonCitEntryDate",this.nonCitEntryDate);
		}

		if(this.nonCitAdmittedToDate != null){
			obj.put("NonCitAdmittedToDate",this.nonCitAdmittedToDate);
		}

		if(this.nonCitAdmittedToText != null){
			obj.put("NonCitAdmittedToText",this.nonCitAdmittedToText);
		}

		if(this.nonCitCountryBirthCd != null){
			obj.put("NonCitCountryBirthCd",this.nonCitCountryBirthCd);
		}

		if(this.nonCitCountryCitCd != null){
			obj.put("NonCitCountryCitCd",this.nonCitCountryCitCd);
		}

		if(this.nonCitCoaCode != null){
			obj.put("NonCitCoaCode",this.nonCitCoaCode);
		}

		if(this.nonCitEadsExpireDate != null){
			obj.put("NonCitEadsExpireDate",this.nonCitEadsExpireDate);
		}

		obj.put("EligStatementCd",this.eligStatementCd);
		
		if(this.eligStatementTxt != null){
			obj.put("EligStatementTxt",this.eligStatementTxt);
		}

		if(this.webServSftwrVer != null){
			obj.put("WebServSftwrVer",this.webServSftwrVer);
		}

		if(this.grantDate != null){
			obj.put("GrantDate",this.grantDate);
		}

		if(this.grantDateReasonCd != null){
			obj.put("GrantDateReasonCd",this.grantDateReasonCd);
		}

		obj.put("SponsorDataFoundIndicator",this.sponsorDataFoundIndicator);
		
		if(this.arrayOfSponsorshipData != null){
			obj.put("ArrayOfSponsorshipData",this.arrayOfSponsorshipData);
		}

		if(this.sponsorshipReasonCd != null){
			obj.put("SponsorshipReasonCd",this.sponsorshipReasonCd);
		}

		obj.put("PhotoIncludedIndicator",this.photoIncludedIndicator);
		
		if(this.photoBinaryAttachment != null){
			obj.put("PhotoBinaryAttachment",this.photoBinaryAttachment);
		}

		obj.put("CaseSentToSecondaryIndicator",this.caseSentToSecondaryIndicator);
		obj.put("DSHAutoTriggerStepTwo",this.dshAutoTriggerStepTwo);

		if(this.fiveYearBarApplyCode != null){
			obj.put("FiveYearBarApplyCode",this.fiveYearBarApplyCode);
		}

		if(this.qualifiedNonCitizenCode != null){
			obj.put("QualifiedNonCitizenCode",this.qualifiedNonCitizenCode);
		}

		if(this.fiveYearBarMetCode != null){
			obj.put("FiveYearBarMetCode",this.fiveYearBarMetCode);
		}

		if(this.usCitizenCode != null){
			obj.put("USCitizenCode",this.usCitizenCode);
		}


		return obj.toJSONString();
	}

}
