package com.getinsured.iex.hub.ssac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ssac.extension.SSATitleIIYearlyIncomeType;

public class SSATitleIIYearlyIncomeWrapper implements JSONAware {
	
	private SSATitleIIRequestedYearInformationTypeWrapper titleIIRequestedYearInformation;
	
	public SSATitleIIYearlyIncomeWrapper(SSATitleIIYearlyIncomeType titleIIYearlyIncomeType) {

		if(titleIIYearlyIncomeType == null){
			return;
		}
		
		this.titleIIRequestedYearInformation =  new SSATitleIIRequestedYearInformationTypeWrapper(titleIIYearlyIncomeType.getTitleIIRequestedYearInformation());
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.titleIIRequestedYearInformation != null){
			obj.put("TitleIIRequestedYearInformation", this.titleIIRequestedYearInformation);
		}
		return obj.toJSONString();
	}
}
