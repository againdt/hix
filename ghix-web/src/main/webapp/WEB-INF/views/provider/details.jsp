<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page isELIgnored="false"%>
<%@ page import="com.getinsured.hix.platform.util.ExpUtil"%>
<%@ page import="com.getinsured.hix.platform.util.PhoneUtil"%>
<style type="text/css">
.table-auto {
	width: auto;
}

.search {
	padding-top: 20px;
}

.table td {
	line-height: 16px;
	vertical-align: top;
}
hr {
	clear: both;
}
</style>
    <link href="../resources/css/provider.css" media="screen" rel="stylesheet" type="text/css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script> 
	<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false"></script> 
	<div class="gutter10">
		<div class="row-fluid">
			<div class="span12">
				<c:if test='${type==fn:toUpperCase("hospital")}'>
					<div class="span8">
						<h1>${facility.provider.name}<small> - <c:forEach var="facilitySpecialRelation" items="${facility.facilitySpecialRelation}">
								${facilitySpecialRelation.specialty.name}
							</c:forEach></small></h1>
					</div>
					<div class="span4 search">
						<c:if test='${type==fn:toUpperCase("hospital")}'>
							<a href='<c:url value="/provider/search/hospitals?planId=${plan.id}&type=${type}&hospitalName=${hospitalName}&hospitalType=${hospitalType}&hospitalNearZip=${hospitalNearZip}&fromIndex=${fromIndex}&toIndex=${toIndex}"/>' class="btn"><spring:message code='label.backToResult'/></a>
						</c:if>
						<a href='<c:url value="/provider/search"><c:param name="planId" value="${plan.id}"/></c:url>' class="btn btn-primary"><spring:message code='label.searchAgain'/></a>
					</div>
				</c:if>
				<c:if test='${type==fn:toUpperCase("doctor") || type==fn:toUpperCase("dentist")}'>
					<div class="span8">
						<h1>${physician.provider.name}<small> - <c:forEach var="physicianSpecialRelation" items="${physician.physicianSpecialRelation}" varStatus="loop">
								${physicianSpecialRelation.specialty.name} <c:if test="${not loop.last}">,</c:if>
							</c:forEach></small></h1>
					</div>
					<div class="span4 search">
						<c:if test='${type==fn:toUpperCase("doctor")}'>
							<a href='<c:url value="/provider/search/doctors?planId=${plan.id}&type=${type}&doctorName=${doctorName}&doctorGender=${doctorGender}&doctorSpecialties=${doctorSpecialties}&doctorLanguages=${doctorLanguages}&doctorNearZip=${doctorNearZip}&fromIndex=${fromIndex}&toIndex=${toIndex}"/>' class="btn"><spring:message code='label.backToResult'/></a>
						</c:if>
						<c:if test='${type==fn:toUpperCase("dentist")}'>
							<a href='<c:url value="/provider/search/dentists?planId=${plan.id}&type=${type}&dentistName=${dentistName}&dentistGender=${dentistGender}&dentistSpecialties=${dentistSpecialties}&dentistLanguages=${dentistLanguages}&dentistNearZip=${dentistNearZip}&fromIndex=${fromIndex}&toIndex=${toIndex}"/>' class="btn"><spring:message code='label.backToResult'/></a>
						</c:if>
						<a href='<c:url value="/provider/search"/>' class="btn btn-primary"><spring:message code='label.searchAgain'/></a>
					</div>
				</c:if>
			</div>
			<h2 class="sub-h1">
				<c:if test="${not empty plan.id}">
				  <spring:message code='label.accepting'/> <!-- Carrier Name: example provided -->${plan.issuer.name} - <!--  Plan Name: example provided -->${plan.name}&nbsp;
				</c:if>	
	            <c:if test="${fn:toUpperCase(plan.networkType) == 'HMO'}">
					<spring:message code='label.hmo' javaScriptEscape='true'/>
				</c:if>	
				<c:if test="${fn:toUpperCase(plan.networkType) == 'PPO'}">
					<spring:message code='label.ppo' javaScriptEscape='true'/>
				</c:if>
			</h2>
			<hr />
			<form class="form-horizontal" id="gi_provider_details" name="" method="GET"><!-- action="<c:url value="" />"-->
				<div class="span8 data">
					<table class="table table-border-none table-auto">
						<tbody>
							<c:if test='${type==fn:toUpperCase("hospital")}'>
								<c:forEach var="facilityAddressObj" items="${facility.facilityAddress}" varStatus="rowCounter">
									<c:if test="${not empty facilityAddressObj.address}">
										<tr>
											<td class="txt-right">Address${rowCounter.count}</td>
											<td><a id="${facilityAddressObj.id}" href="javascript:showMarkerInfoWindow(${facilityAddressObj.id})"><img src="<c:url value="/resources/images/red-dot.png"/>" alt="Map pin"/></a>
											<strong>${facilityAddressObj.address}</strong></td>
										</tr>
									</c:if>
									
									<c:if test="${not empty facilityAddressObj.city}">
										<tr>
											<td class="txt-right"><spring:message code='label.city'/></td>
											<td><strong>${facilityAddressObj.city}</strong></td>
										</tr>
									</c:if>
									
									<c:if test="${not empty facilityAddressObj.state}">
										<tr>
											<td class="txt-right"><spring:message code='label.state'/></td>
											<td><strong>${facilityAddressObj.state}</strong></td>
										</tr>
									</c:if>
									
									<c:if test="${not empty facilityAddressObj.zip}">
										<tr>
											<td class="txt-right"><spring:message code='label.zipcode'/></td>
											<td><strong>${facilityAddressObj.zip}</strong></td>
										</tr>
									</c:if>
									
									<c:if test="${not empty facilityAddressObj.phone}">
										<tr>
											<td class="txt-right"><spring:message code='label.phone'/></td>
											<c:set var="fphone" value="${facilityAddressObj.phone}"/>
	                                        <td><strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("fphone"))%></strong></td>
										</tr>
									</c:if>
								</c:forEach>
									<c:if test="${not empty facility.medicalGroup}">
										<tr>
											<td class="txt-right"><spring:message code='label.medicalGroup'/></td>
											<td><strong>${facility.medicalGroup}</strong></td>
										</tr>
									</c:if>
									
									
									
							</c:if>
	
							<c:if test='${type==fn:toUpperCase("doctor") || type==fn:toUpperCase("dentist")}'>
								
								<c:forEach var="physicianAddressObj" items="${physician.physicianAddress}" varStatus="rowCounter">
									
									
									<c:if test="${not empty physicianAddressObj.address}">
										<tr>
											<td class="txt-right">Address${rowCounter.count}</td>
											<td><a id="${physicianAddressObj.id}" href="javascript:showMarkerInfoWindow(${physicianAddressObj.id})"><img src="<c:url value="/resources/images/red-dot.png"/>" alt="Map pin"/></a>
												<strong>${physicianAddressObj.address}</strong>
											</td>
										</tr>
									</c:if>
									
									<c:if test="${not empty physicianAddressObj.city}">
										<tr>
											<td class="txt-right"><spring:message code='label.city'/></td>
											<td><strong>${physicianAddressObj.city}</strong></td>
										</tr>
									</c:if>
									
									<c:if test="${not empty physicianAddressObj.state}">
										<tr>
											<td class="txt-right"><spring:message code='label.state'/></td>
											<td><strong>${physicianAddressObj.state}</strong></td>
										</tr>
									</c:if>
									
									<c:if test="${not empty physicianAddressObj.zip}">
										<tr>
											<td class="txt-right"><spring:message code='label.zipcode'/></td>
											<td><strong>${physicianAddressObj.zip}</strong></td>
										</tr>
									</c:if>
									
									<c:if test="${not empty physicianAddressObj.phone}">
										<tr>
											<td class="txt-right"><spring:message code='label.phone'/></td>
											<c:set var="pphone" value="${physicianAddressObj.phone}"/>
	                                        <td><strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("pphone"))%></strong></td>
										</tr>
									</c:if>
								</c:forEach>
							</c:if>
	
							<c:if test='${type==fn:toUpperCase("doctor") || type==fn:toUpperCase("dentist")}'>
								
								<c:if test="${not empty physician.acceptingNewPatients}">
									<tr>
										<td class="txt-right"><spring:message code='label.acceptingNewPatients'/></td>
										<td><strong>${physician.acceptingNewPatients}</strong></td>
									</tr>
								</c:if>
								<c:if test="${not empty physician.languages}">
									<tr>
										<td class="txt-right"><spring:message code='label.languages'/></td>
										<td><strong>${physician.languages}</strong></td>
									</tr>
								</c:if>
								<c:if test="${not empty physician.gender}">
									<tr>
										<td class="txt-right">Gender</td>
										<td><strong><spring:message code='label.gender.${physician.gender}' javaScriptEscape='true'/></strong></td>
									</tr>
								</c:if>
								<c:if test="${not empty physician.education}">
									<tr>
										<td class="txt-right"><spring:message code='label.education'/></td>
										<td><strong>${physician.education}</strong></td>
									</tr>
								</c:if>
								<c:if test="${not empty physician.internship}">
									<tr>
										<td class="txt-right"><spring:message code='label.internship'/></td>
										<td><strong>${physician.internship}</strong></td>
									</tr>
								</c:if>
								<c:if test="${not empty physician.residency}">
									<tr>
										<td class="txt-right"><spring:message code='label.residency'/></td>
										<td><strong>${physician.residency}</strong></td>
									</tr>
								</c:if>
								<c:if test="${not empty physician.graduationYear}">
									<tr>
										<td class="txt-right"><spring:message code='label.graduationYear'/></td>
										<c:set var="gradYear" value="${physician.graduationYear}"/>
										<td><strong><%=ExpUtil.getExpByYear((String)pageContext.getAttribute("gradYear"))%></strong></td>
									</tr>
								</c:if>
								<c:if test="${not empty physician.boardCertification}">
									<tr>
										<td class="txt-right"><spring:message code='label.boardCertification'/></td>
										<td><strong>${physician.boardCertification}</strong></td>
									</tr>
								</c:if>
								<c:if test="${not empty physician.medicalGroup}">
									<tr>
										<td class="txt-right"><spring:message code='label.medicalGroup'/></td>
										<td><strong>${physician.medicalGroup}</strong></td>
									</tr>
								</c:if>
								<c:if test="${not empty physician.affiliatedHospital}">
									<tr>
										<td class="txt-right"><spring:message code='label.affiliatedHospitals'/></td>
										<td><strong>${physician.affiliatedHospital}</strong></td>
									</tr>
								</c:if>
								
								
							</c:if>
						</tbody>
					</table>
				</div><!-- .span8 -->
	
				<div id="map_canvas" class="map pull-right" style="width: 350px; height: 350px;" ></div>
			</form>
		</div><!-- end .row-fluid -->
	</div>
<script type="text/javascript">
    
  	var locations = jQuery.parseJSON('${markers}');
    var map = new google.maps.Map(document.getElementById('map_canvas'), {
      zoom: 10,
      center: new google.maps.LatLng(locations[0].lat,locations[0].lng),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var markersArray = []; 
	
    for (var i = 0; i < locations.length; i++) {  
       	var marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
        name :  locations[i].name,
        address :  locations[i].address,
        city :  locations[i].city,
        state :  locations[i].state,
        zip :  locations[i].zip,
		map: map
      });
	  	markersArray.push(marker);
	  	google.maps.event.addListener(marker, 'click', function() {
		infowindow.setContent(getContent(marker));
        infowindow.open(map, this);
        }); 
      }
      function showMarkerInfoWindow(id){
 	    for (var i = 0; i < locations.length; i++) {  
		  var marker = markersArray[i];
		  if(locations[i].id==id){
			     infowindow.setContent(getContent(marker));
				 infowindow.open(map, marker);
  
		  }
		}
	  }
      function getContent(marker){
    	  var contentString = '<div id="infoWindow">'
              +'<div id="bodyContent">'
              +'<p>'
              + "<b style='font-size:15px'>"+marker.name+"</b><br/>"
              + marker.address+",<br/>"+marker.city+","+marker.state+","+marker.zip+'</p>'
              +'</div>'
              + '</div>';
       return contentString;
      }
  </script>
	