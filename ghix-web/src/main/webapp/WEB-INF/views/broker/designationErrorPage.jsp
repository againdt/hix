<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page isErrorPage="true" %>

 

<div id="error_page">

        <div class="gutter10">
              <div class="row-fluid">
                      <div class="span12">
                                <div class="row-fluid">
                               <p class="pull-left"><b>Error</b></p>
                                     <div class="span12">
                                              <p class="pull-left">
                                              
                                              <spring:message  code="label.brkagentdesignation.error"/>
                                                      
	                               			</p>                    
                                      </div>
                                </div>
                       </div>
               </div>  
               <div>           
                     <input name="close" type="button" onClick="window.parent.close();" value='Close' class="btn btn-primary pull-right" />
               </div>

        </div>

</div>