<!DOCTYPE html>
<%@ include file="include.jsp" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><spring:message code="ssap.defaultTemplate.title" text="NMHIX"/></title>



<link href="css/bootstrap_NM.css" rel="stylesheet" type="text/css">
<link href="css/ghixcustom.css" rel="stylesheet" type="text/css">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">


<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.i18n.properties-1.0.9.js"></script>
<script type="text/javascript" src="js/incomeCalculation.js"></script>
<script type="text/javascript" src="js/continueFunctionality.js"></script>
<script type="text/javascript" src="js/opentip-jquery-excanvas.min.js"></script>
<script type="text/javascript" src="js/tooltipController.js"></script>
<!-- <script type="text/javascript" src="js/RegistrationPageValidation.js"></script>
<script type="text/javascript" src="js/snapManager.js"></script> -->
<script type="text/javascript" src="js/additionalQuestions.js"></script>
<!-- <script type="text/javascript" src="js/latestResul.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker.js"></script> -->

<script type="text/javascript">
var d;
$(document).ready(function(){
	d = "${model.dateOfServer}"; 
});
</script>

<script type="text/javascript" src="js/navigation.js"></script>
<script type="text/javascript" src="js/NoOfHouseHold.js"></script>
<script type="text/javascript" src="js/bloodRelationship.js"></script>
<script type="text/javascript" src="js/addNonFinancialDetails.js"></script>
<script type="text/javascript" src="js/appscr60A.js"></script>
<script type="text/javascript" src="js/addFinancialDetails.js"></script>
<script type="text/javascript" src="js/ajax3.js"></script>
<script type="text/javascript" src="js/magiValidation.js"></script> 
<script type="text/javascript" src="js/jquery.mask.js" ></script>
<script type="text/javascript" src="js/jquery.mask.min.js" ></script>
<script type="text/javascript" src="js/magiValidationEvents.js"></script>
<script type="text/javascript" src="js/summaryController.js"></script>
<script type="text/javascript" src="js/displayAndHideInfo.js"></script>
<script type="text/javascript" src="js/appscr55.js"></script>
<script type="text/javascript" src="js/appscr81A.js"></script>
<script type="text/javascript" src="js/backFunctionality.js"></script>
<script type="text/javascript" src="js/editController.js"></script>
<script type="text/javascript" src="js/fillValuesFromMongodb.js"></script>
<script type="text/javascript" src="js/saveData.js"></script>
<script type="text/javascript" src="js/scriptGeneralDetail.js"></script>
<script type="text/javascript" src="js/plans.js"></script>
<script type="text/javascript" src="js/placeholder.js"></script>
<script type="text/javascript" src="js/functions.js"></script>

</head>
<body>
	<div id="header">
	 	<tiles:insertAttribute name="header" />
	</div>
	<div id="container-wrap" class="row-fluid">
   		<div class="container">
   			<div>
   				<tiles:insertAttribute name="mainmenu" />
   			</div>
   			<div class="gutter10">
    			<div class="row-fluid">
					<ul class="page-breadcrumb">
						<li><a title="Go Back" href="javascript:history.back()">&lt; Back</a></li>
						<li><a title="Plans" href="#">Plans</a></li>
						<li><a title="Manage SADPs" href="#">Manage SADPs</a></li>
						<li>Standalone Dental Plans </li>
					</ul>
					<h1>
						<a name="skip"></a>
						Fill your SSAP Application
					</h1>
				</div>
				<div class="row-fluid">
					<div id="sidebar">
						<tiles:insertAttribute name="sidebar" />	 
					</div>
					<div class="span9" id="rightpanel">
						<div class="menu_back_image">
							<tiles:insertAttribute name="menu" />	 
						</div>
						<div id="body">
			 				<tiles:insertAttribute name="body" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="push"></div>
	<div class="container" id="footer">
		<tiles:insertAttribute name="footer" />
	</div>
	
	<script>
	$('input[type=text], textarea').placeholder();
	</script>
	
</body>
</html>