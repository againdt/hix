<%@ include file="../include.jsp" %>
 <div id="appscr62Part1" style='display: none;'>
   
   <div class="row-fluid">
      <div class="page-title">
         <h5 class="titleForPages">
            <strong class="nameOfHouseHold"></strong>
            <spring:message code="ssap.page12A.citizenship" text="Citizenship"/> &#47;<spring:message code="ssap.page12A.immiStatus" text="Immigration Status"/>
         </h5>
      </div>
      
      <form id="appscr62Part1Form" class="form-horizontal" data-parsley-excluded=":hidden">
         <div class="header">
            <h4><spring:message code="ssap.pageHeader.familyAndHousehold" text="Family and Household" /></h4>
         </div>
         <div class="gutter10">
         <div class="control-group">
            <p>
               <spring:message code="ssap.page12A.is" text="Is"/>
               <strong class="nameOfHouseHold"></strong>
               <spring:message code="ssap.page12A.usCitizenOrUSNational" text="a U.S. citizen or U.S. national?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
            </p>
            <div class="margin5-l">
               <label for="UScitizenIndicatorYes" class="radio">
                  <input type="radio" id="UScitizenIndicatorYes" name="UScitizen" value="true" onchange="isUSCitizen();" data-parsley-errors-container="#UScitizenIndicatorYesErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" />
                  <spring:message code="ssap.page12A.yes" text="Yes"/>
               </label>
               <label for="UScitizenIndicatorNo" class="radio">
                  <input type="radio" id="UScitizenIndicatorNo" name="UScitizen" value="false" onchange="isUSCitizen();" />
                  <spring:message code="ssap.page12A.no" text="No"/>
               </label>
               <div id="UScitizenIndicatorYesErrorDiv"></div>
            </div>
         </div><!-- control-group ends -->
         <div id="naturalizedCitizenDiv" class="hide">
            <div class="control-group">
               <p>
                  <spring:message code="ssap.page12A.is" text="Is"/>
                  <strong class="nameOfHouseHold"></strong>
                  <spring:message code="ssap.page12A.aNaturalCitizen" text="a naturalized citizen?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
               </p>
               <div class="margin5-l">
                  <label for="naturalizedCitizenshipIndicatorYes" class="radio">
                     <input type="radio" name="naturalizedCitizenshipIndicator" id="naturalizedCitizenshipIndicatorYes" value="true" onchange="isNaturalizedCitizen();" data-parsley-errors-container="#naturalizedCitizenshipIndicatorYesErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"  />
                     <spring:message code="ssap.page12A.yes" text="Yes"/>
                  </label>
                  <label for="naturalizedCitizenshipIndicatorNo" class="radio">
                     <input type="radio" name="naturalizedCitizenshipIndicator" id="naturalizedCitizenshipIndicatorNo" value="false" onchange="isNaturalizedCitizen();" />
                     <spring:message code="ssap.page12A.no" text="No"/>
                  </label>
                  <div id="naturalizedCitizenshipIndicatorYesErrorDiv"></div>
               </div>
            </div><!-- control-group ends -->
         </div><!-- naturalizedCitizenDiv ends -->
         
         <div class="control-group hide">
            <p>
               <spring:message code="ssap.page12A.is" text="Is"/>
               <strong class="nameOfHouseHold"></strong>&apos;
               <spring:message code="ssap.page12A.citizenshipVerified" text="s citizenship Verified Manually?"/>
            </p>
            <div class="margin5-l">
               <label for="UScitizenManualVerificationYes" class="radio">
                  <input type="radio" id="UScitizenManualVerificationYes" name="UScitizenManualVerification" value="yes">
                  <spring:message code="ssap.page12A.yes" text="Yes"/>
               </label>
               <label for="UScitizenManualVerificationNo" class="radio">
                  <input type="radio" id="UScitizenManualVerificationNo" name="UScitizenManualVerification" value="no">
                  <spring:message code="ssap.page12A.no" text="No"/>
               </label>
            </div>
         </div><!-- control-group ends -->
         
         <div id="documentTypeDiv" style="display:none" class="hide">
            <h4>
               <strong><spring:message code="ssap.page12A.docType" text="Document Type"/></strong> 
               <small><spring:message code="ssap.page12A.selectOne" text="(select one)"/></small><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
            </h4>
            
            <div class="control-group gutter10-l margin20-t">
               <label for="naturalizedCitizenNaturalizedIndicator" class="radio">
                  <input type="radio" id="naturalizedCitizenNaturalizedIndicator" name="documentType" value="NaturalizationCertificate" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"  data-parsley-errors-container="#naturalizedCitizenNaturalizedIndicatorErrorDiv"/>
                  <spring:message code="ssap.page12A.naturalizationCertificate" text="Naturalization  Certificate"/>
               </label>
            </div>
            
            <div class="appscr62Part1HideandShowFillingDetails hide" id="divNaturalizationCertificate">
               <div class="gutter10">
               <div class="control-group">
                  <label class="control-label" for="naturalizationAlignNumber">
                     <spring:message code="ssap.page12A.alignNumber" text="Alien Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                  </label>
                  <div class="controls">
                     <input placeHolder="<spring:message code="ssap.page12A.placeholder.alignNum" text="Alien Number"/>" type="text" id="naturalizationAlignNumber" name="naturalizationAlignNumber" data-parsley-pattern="^[0-9]{7,9}$"  data-parsley-pattern-message="<spring:message code='ssap.error.alienNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.alienNumRequired'/>"/>
                  </div><!-- end of controls-->
                  
               </div><!-- control-group ends -->
               
               <div class="control-group">
                  <label class="control-label" for="naturalizationCertificateNumber">
                     <spring:message code="ssap.page12A.naturalizationNumber:" text="Naturalization Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                  </label>
                  <div class="controls">
                     <input placeHolder="<spring:message code="ssap.page12A.placeholder.natNum" text="Naturalization Number"/>" type="text" id="naturalizationCertificateNumber" name="naturalizationCertificateNumber" data-parsley-pattern="^[0-9]{6,12}$"  data-parsley-pattern-message="<spring:message code='ssap.error.naturalizationNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.naturalizationNumRequired'/>" />
                  </div><!-- end of controls-->
                  
               </div><!-- control-group ends -->
               
               <%-- <label class="control-group margin30-l hide">
                  <input type="checkbox" id="naturalizedDonotHave" name="naturalizedCitizen" value="0">
                  <spring:message code="ssap.page12A.iDon" text="I don"/>&apos;<spring:message code="ssap.page12A.haveOne" text="t have one"/>
               </label> --%>
               </div>
            </div><!-- appscr62Part1HideandShowFillingDetails ends -->
            
            <div class="control-group gutter10-l">
               <label class="radio" for="naturalizedCitizenNaturalizedIndicator2">
                  <input type="radio" name="documentType" id="naturalizedCitizenNaturalizedIndicator2" value="CitizenshipCertificate" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"  />
                  <spring:message code="ssap.page12A.certificateOfCitizenship" text="Certificate of Citizenship"/>
               </label>
               <div id="naturalizedCitizenNaturalizedIndicatorErrorDiv"></div>
            </div>
            
            <div class="appscr62Part1HideandShowFillingDetails hide" id="divCitizenshipCertificate">
               <div class="gutter10">
               <div class="control-group">
                  <label class="control-label" for="citizenshipAlignNumber">
                     <spring:message code="ssap.page12A.alignNumber" text="Alien Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                  </label>
                  <div class="controls">
                     <input placeHolder="<spring:message code="ssap.page12A.placeholder.alignNum" text="Alien Number"/>" type="text" id="citizenshipAlignNumber" name="citizenshipAlignNumber" data-parsley-pattern="^[0-9]{7,9}$"  data-parsley-pattern-message="<spring:message code='ssap.error.alienNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.alienNumRequired'/>" />
                  </div><!-- end of controls-->
               </div><!-- control-group ends -->
               
               <div class="control-group">
                  <label class="control-label" for="appscr62p1citizenshipCertificateNumber">
                     <spring:message code="ssap.page12A.citizenshipCertificateNumber" text="Citizenship Certificate Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                  </label>
                  <div class="controls">
                     <input placeHolder="<spring:message code="ssap.page12A.placeholder.citizenCertificateNum" text="Citizenship Certificate Number"/>" size="30" type="text" id="appscr62p1citizenshipCertificateNumber" name="appscr62p1citizenshipCertificateNumber" data-parsley-pattern="^[0-9]{6,12}$"  data-parsley-pattern-message="<spring:message code='ssap.error.citizenshipCertificateNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.citizenshipCertificateNumRequired'/>"/>
                  </div><!-- end of controls-->
               </div><!-- control-group ends -->
               
               <%-- <label class="control-group margin30-l hide">
                  <input type="checkbox" id="citizenDonotHave" name="naturalizedCitizen" value="0">
                  <spring:message code="ssap.page12A.idon" text="I don"/>&apos;<spring:message code="ssap.page12A.haveOne" text="t have one"/>
               </label> --%>
               </div>
            </div><!-- appscr62Part1HideandShowFillingDetails ends -->
            
         </div><!-- documentTypeDiv ends -->
         
         
         <div id="eligibleImmigrationStatusDiv" class="hide">
            <label class="margin20-b checkbox" for="naturalCitizen_eligibleImmigrationStatus">
               <input type="checkbox" id="naturalCitizen_eligibleImmigrationStatus" name="EligibleImmigrationStatuscheckbox" value="checked" onchange="showOrHideNaturalCitizenImmigrationStatus();">
               <spring:message code="ssap.page12A.checkIf" text="Check if"/> <strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page12A.hasEligibleImmigrationStatus" text="has eligible immigration status"/> 
            </label>
            
            <div id="naturalCitizen_eligibleImmigrationStatus_ID" class="hide">
               <h4>
                  <strong><spring:message code="ssap.page12A.docType" text="Document Type"/></strong> 
                  <small><spring:message code="ssap.page12A.selectOne" text="(select one)"/></small><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
               </h4>
               <fieldset>
               <div class="control-group" id="eligibleImmigrationStatus">
                  <!--<label class="radio" for="">
                     <input type="radio" name="docType" value="1">
                     <spring:message code="ssap.page12A.reentryPermit" text="Reentry Permit ("/><spring:message code="ssap.page12A.i" text="I"/>&minus;<spring:message code="ssap.page12A.num327" text="327)"/>
                  </label>-->
                  <label class="radio" for="permcarddetails">
                     <input type="radio" name="docType" id="permcarddetails" value="0" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#permcarddetailsErrorDiv" /><spring:message code="ssap.page12A.permResidentCard" text="Permanent Resident Card ("/> &ldquo;<spring:message code="ssap.page12A.green" text="Green"/> <spring:message code="ssap.page12A.card" text="Card"/>&rdquo;, <spring:message code="ssap.page12A.i" text="I"/>&minus;<spring:message code="ssap.page12A.num551" text="551)"/>
                  </label>          
                  <div class="immigrationDocType hide" id="permcarddetailsdiv">
                     <div class="control-group">
                        <label class="control-label" for="permcarddetailsAlignNumber">
                           <spring:message code="ssap.page12A.alignNumber" text="Alien Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.placeholder.alignNum" text="Alien Number"/>" type="text" id="permcarddetailsAlignNumber" name="permcarddetailsAlignNumber" data-parsley-pattern="^[0-9]{7,9}$"  data-parsley-pattern-message="<spring:message code='ssap.error.alienNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.alienNumRequired'/>" />
                        </div><!-- end of controls-->
                        
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="permcarddetailsppcardno">
                           <spring:message code="ssap.page12A.cardNumber" text="Card Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.cardNumber" text="Card Number"/>" type="text" id="permcarddetailsppcardno" name="permcarddetailsppcardno" data-parsley-pattern="^[a-zA-Z]{3}[0-9]{10}$"  data-parsley-pattern-message="<spring:message code='ssap.error.cardNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.cardNumRequired'/>" />
                        </div><!-- end of controls-->
                        
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="permcarddetailsdateOfExp">
                           <spring:message code="ssap.page12A.expDate" text="Expiration Date"/>
                        </label>
                        <div class="controls">
                           <div data-date="" class="input-append date date-picker">
                              <input type="text" class="input-medium dob-input" name="permcarddetailsdateOfExp" id="permcarddetailsdateOfExp" data-parsley-trigger="change" data-parsley-errors-container="#permcarddetailsdateOfExpErrorDiv"  placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-pattern = "(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>"/>
                              <span class="add-on dateSpan" ><i class="icon-calendar"></i></span>
                           </div>
                           <div id="permcarddetailsdateOfExpErrorDiv"></div>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->               
                  </div>
                  <!--<label class="radio" for="">
                     <input type="radio" name="docType" value="1">
                     <spring:message code="ssap.page12A.refTravelDoc" text="Refugee Travel Document   ("/><spring:message code="ssap.page12A.i" text="I"/>&minus;<spring:message code="ssap.page12A.num571" text="571)"/>
                  </label>
                  <label class="radio" for="">
                     <input type="radio" name="docType" value="0">
                     <spring:message code="ssap.page12A." text="Employment Authorization Card (I"/>&minus;<spring:message code="ssap.page12A.num776" text="766)"/>
                  </label>-->
                  
                  <label class="radio" for="tempcarddetails">
                     <input type="radio" id="tempcarddetails" name="docType" value="0"><spring:message code="ssap.page12A.tempI" text="Temporary I"/>&minus;<spring:message code="ssap.page12A.stamp551" text="551 Stamp (on passport or I"/>&minus;<spring:message code="ssap.page12A.num94WithI" text="94, I"/>&minus;<spring:message code="ssap.page12A.num94A" text="94A)"/>
                  </label>          
                  <div  class="immigrationDocType hide" id="tempcarddetailsdiv">
                     <div class="control-group">
                        <label class="control-label" for="tempcarddetailsAlignNumber">
                           <spring:message code="ssap.page12A.alignNumber" text="Alien Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.placeholder.alignNum" text="Alien Number"/>" type="text" id="tempcarddetailsAlignNumber" name="tempcarddetailsAlignNumber" data-parsley-pattern="^[0-9]{7,9}$"  data-parsley-pattern-message="<spring:message code='ssap.error.alienNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.alienNumRequired'/>"/>
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="tempcarddetailsppcardno">
                           <spring:message code="ssap.page12A.passportNumber" text="Passport Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.passportNumber" text="Passport Number"/>" type="text" id="tempcarddetailsppcardno" name="tempcarddetailsppcardno" data-parsley-pattern="^[a-zA-Z0-9]{6,12}$"  data-parsley-pattern-message="<spring:message code='ssap.error.passportNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.passportNumRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="tempcarddetailsCounty">
                           <spring:message code="ssap.page12A.foreignPassportCountryOfIssuance" text="Country of Issuance"/>
                           <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                           <span aria-label="Required!"></span>
                        </label>
                        <div class="controls">
                           <select name="tempcarddetailsCounty" id="tempcarddetailsCounty" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.passportCountryRequired' />" >
                              <option value=""><spring:message code="ssap.County" text="Country" /></option>
                              <option value="AFG"><spring:message code="ssap.county.Afghanistan" text="Afghanistan" /></option>
                              <option value="ALB"><spring:message code="ssap.county.Albania" text="Albania" /></option>
                              <option value="DZA"><spring:message code="ssap.county.Algeria" text="Algeria" /></option>
                              <option value="ASM"><spring:message code="ssap.county.American Samoa" text="American Samoa" /></option>
                              <option value="AND"><spring:message code="ssap.county.Andorra" text="Andorra" /></option>
                              <option value="AGO"><spring:message code="ssap.county.Angola" text="Angola" /></option>
                              <option value="AIA"><spring:message code="ssap.county.Anguilla" text="Anguilla" /></option>
                              <option value="ATA"><spring:message code="ssap.county.Antarctica" text="Antarctica" /></option>
                              <option value="ATG"><spring:message code="ssap.county.Antigua And Barbuda" text="Antigua And Barbuda" /></option>
                              <option value="ARG"><spring:message code="ssap.county.Argentina" text="Argentina" /></option>
                              <option value="ARM"><spring:message code="ssap.county.Armenia" text="Armenia" /></option>
                              <option value="ABW"><spring:message code="ssap.county.Aruba" text="Aruba" /></option>
                              <option value="AUS"><spring:message code="ssap.county.Australia" text="Australia" /></option>
                              <option value="AUT"><spring:message code="ssap.county.Austria" text="Austria" /></option>
                              <option value="AZE"><spring:message code="ssap.county.Azerbaijan" text="Azerbaijan" /></option>
                              <option value="BHS"><spring:message code="ssap.county.Bahamas" text="Bahamas" /></option>
                              <option value="BHR"><spring:message code="ssap.county.Bahrain" text="Bahrain" /></option>
                              <option value="BGD"><spring:message code="ssap.county.Bangladesh" text="Bangladesh" /></option>
                              <option value="BRB"><spring:message code="ssap.county.Barbados" text="Barbados" /></option>
                              <option value="BLR"><spring:message code="ssap.county.Belarus" text="Belarus" /></option>
                              <option value="BEL"><spring:message code="ssap.county.Belgium" text="Belgium" /></option>
                              <option value="BLZ"><spring:message code="ssap.county.Belize" text="Belize" /></option>
                              <option value="BEN"><spring:message code="ssap.county.Benin" text="Benin" /></option>
                              <option value="BMU"><spring:message code="ssap.county.Bermuda" text="Bermuda" /></option>
                              <option value="BTN"><spring:message code="ssap.county.Bhutan" text="Bhutan" /></option>
                              <option value="BOL"><spring:message code="ssap.county.Bolivia, Plurinational State Of" text="Bolivia, Plurinational State Of" /></option>
                              <option value="BIH"><spring:message code="ssap.county.Bosnia And Herzegovina" text="Bosnia And Herzegovina" /></option>
                              <option value="BWA"><spring:message code="ssap.county.Botswana" text="Botswana" /></option>
                              <option value="BVT"><spring:message code="ssap.county.Bouvet Island" text="Bouvet Island" /></option>
                              <option value="BRA"><spring:message code="ssap.county.Brazil" text="Brazil" /></option>
                              <option value="IOT"><spring:message code="ssap.county.British Indian Ocean Territory" text="British Indian Ocean Territory" /></option>
                              <option value="BRN"><spring:message code="ssap.county.Brunei Darussalam" text="Brunei Darussalam" /></option>
                              <option value="BGR"><spring:message code="ssap.county.Bulgaria" text="Bulgaria" /></option>
                              <option value="BFA"><spring:message code="ssap.county.Burkina Faso" text="Burkina Faso" /></option>
                              <option value="BDI"><spring:message code="ssap.county.Burundi" text="Burundi" /></option>
                              <option value="KHM"><spring:message code="ssap.county.Cambodia" text="Cambodia" /></option>
                              <option value="CMR"><spring:message code="ssap.county.Cameroon" text="Cameroon" /></option>
                              <option value="CAN"><spring:message code="ssap.county.Canada" text="Canada" /></option>
                              <option value="CPV"><spring:message code="ssap.county.Cape Verde" text="Cape Verde" /></option>
                              <option value="CYM"><spring:message code="ssap.county.Cayman Islands" text="Cayman Islands" /></option>
                              <option value="CAF"><spring:message code="ssap.county.Central African Republic" text="Central African Republic" /></option>
                              <option value="CIV"><spring:message code="ssap.county.Cete D'Ivoire" text="Cete D'Ivoire" /></option>
                              <option value="TCD"><spring:message code="ssap.county.Chad" text="Chad" /></option>
                              <option value="CHL"><spring:message code="ssap.county.Chile" text="Chile" /></option>
                              <option value="CHN"><spring:message code="ssap.county.China" text="China" /></option>
                              <option value="CXR"><spring:message code="ssap.county.Christmas Island" text="Christmas Island" /></option>
                              <option value="CCK"><spring:message code="ssap.county.Cocos (Keeling) Islands" text="Cocos (Keeling) Islands" /></option>
                              <option value="COL"><spring:message code="ssap.county.Colombia" text="Colombia" /></option>
                              <option value="COM"><spring:message code="ssap.county.Comoros" text="Comoros" /></option>
                              <option value="COG"><spring:message code="ssap.county.Congo" text="Congo" /></option>
                              <option value="COD"><spring:message code="ssap.county.Congo, Democratic Republic Of The" text="Congo, Democratic Republic Of The" /></option>
                              <option value="COK"><spring:message code="ssap.county.Cook Islands" text="Cook Islands" /></option>
                              <option value="CRI"><spring:message code="ssap.county.Costa Rica" text="Costa Rica" /></option>
                              <option value="HRV"><spring:message code="ssap.county.Croatia" text="Croatia" /></option>
                              <option value="CUB"><spring:message code="ssap.county.Cuba" text="Cuba" /></option>
                              <option value="CYP"><spring:message code="ssap.county.Cyprus" text="Cyprus" /></option>
                              <option value="CZE"><spring:message code="ssap.county.Czech Republic" text="Czech Republic" /></option>
                              <option value="DNK"><spring:message code="ssap.county.Denmark" text="Denmark" /></option>
                              <option value="DJI"><spring:message code="ssap.county.Djibouti" text="Djibouti" /></option>
                              <option value="DMA"><spring:message code="ssap.county.Dominica" text="Dominica" /></option>
                              <option value="DOM"><spring:message code="ssap.county.Dominican Republic" text="Dominican Republic" /></option>
                              <option value="ECU"><spring:message code="ssap.county.Ecuador" text="Ecuador" /></option>
                              <option value="EGY"><spring:message code="ssap.county.Egypt" text="Egypt" /></option>
                              <option value="SLV"><spring:message code="ssap.county.El Salvador" text="El Salvador" /></option>
                              <option value="ALA"><spring:message code="ssap.county.Eland Islands" text="Eland Islands" /></option>
                              <option value="GNQ"><spring:message code="ssap.county.Equatorial Guinea" text="Equatorial Guinea" /></option>
                              <option value="ERI"><spring:message code="ssap.county.Eritrea" text="Eritrea" /></option>
                              <option value="EST"><spring:message code="ssap.county.Estonia" text="Estonia" /></option>
                              <option value="ETH"><spring:message code="ssap.county.Ethiopia" text="Ethiopia" /></option>
                              <option value="FLK"><spring:message code="ssap.county.Falkland Islands (Malvinas)" text="Falkland Islands (Malvinas)" /></option>
                              <option value="FRO"><spring:message code="ssap.county.Faroe Islands" text="Faroe Islands" /></option>
                              <option value="FJI"><spring:message code="ssap.county.Fiji" text="Fiji" /></option>
                              <option value="FIN"><spring:message code="ssap.county.Finland" text="Finland" /></option>
                              <option value="FRA"><spring:message code="ssap.county.France" text="France" /></option>
                              <option value="GUF"><spring:message code="ssap.county.French Guiana" text="French Guiana" /></option>
                              <option value="PYF"><spring:message code="ssap.county.French Polynesia" text="French Polynesia" /></option>
                              <option value="ATF"><spring:message code="ssap.county.French Southern Territories" text="French Southern Territories" /></option>
                              <option value="GAB"><spring:message code="ssap.county.Gabon" text="Gabon" /></option>
                              <option value="GMB"><spring:message code="ssap.county.Gambia" text="Gambia" /></option>
                              <option value="GEO"><spring:message code="ssap.county.Georgia" text="Georgia" /></option>
                              <option value="DEU"><spring:message code="ssap.county.Germany" text="Germany" /></option>
                              <option value="GHA"><spring:message code="ssap.county.Ghana" text="Ghana" /></option>
                              <option value="GIB"><spring:message code="ssap.county.Gibraltar" text="Gibraltar" /></option>
                              <option value="GRC"><spring:message code="ssap.county.Greece" text="Greece" /></option>
                              <option value="GRL"><spring:message code="ssap.county.Greenland" text="Greenland" /></option>
                              <option value="GRD"><spring:message code="ssap.county.Grenada" text="Grenada" /></option>
                              <option value="GLP"><spring:message code="ssap.county.Guadeloupe" text="Guadeloupe" /></option>
                              <option value="GUM"><spring:message code="ssap.county.Guam" text="Guam" /></option>
                              <option value="GTM"><spring:message code="ssap.county.Guatemala" text="Guatemala" /></option>
                              <option value="GGY"><spring:message code="ssap.county.Guernsey" text="Guernsey" /></option>
                              <option value="GIN"><spring:message code="ssap.county.Guinea" text="Guinea" /></option>
                              <option value="GNB"><spring:message code="ssap.county.Guinea-Bissau" text="Guinea-Bissau" /></option>
                              <option value="GUY"><spring:message code="ssap.county.Guyana" text="Guyana" /></option>
                              <option value="HTI"><spring:message code="ssap.county.Haiti" text="Haiti" /></option>
                              <option value="HMD"><spring:message code="ssap.county.Heard Island And Mcdonald Islands" text="Heard Island And Mcdonald Islands" /></option>
                              <option value="VAT"><spring:message code="ssap.county.Holy See (Vatican City State)" text="Holy See (Vatican City State)" /></option>
                              <option value="HND"><spring:message code="ssap.county.Honduras" text="Honduras" /></option>
                              <option value="HKG"><spring:message code="ssap.county.Hong Kong" text="Hong Kong" /></option>
                              <option value="HUN"><spring:message code="ssap.county.Hungary" text="Hungary" /></option>
                              <option value="ISL"><spring:message code="ssap.county.Iceland" text="Iceland" /></option>
                              <option value="IND"><spring:message code="ssap.county.India" text="India" /></option>
                              <option value="IDN"><spring:message code="ssap.county.Indonesia" text="Indonesia" /></option>
                              <option value="IRN"><spring:message code="ssap.county.Iran, Islamic Republic Of" text="Iran, Islamic Republic Of" /></option>
                              <option value="IRQ"><spring:message code="ssap.county.Iraq" text="Iraq" /></option>
                              <option value="IRL"><spring:message code="ssap.county.Ireland" text="Ireland" /></option>
                              <option value="IMN"><spring:message code="ssap.county.Isle Of Man" text="Isle Of Man" /></option>
                              <option value="ISR"><spring:message code="ssap.county.Israel" text="Israel" /></option>
                              <option value="ITA"><spring:message code="ssap.county.Italy" text="Italy" /></option>
                              <option value="JAM"><spring:message code="ssap.county.Jamaica" text="Jamaica" /></option>
                              <option value="JPN"><spring:message code="ssap.county.Japan" text="Japan" /></option>
                              <option value="JEY"><spring:message code="ssap.county.Jersey" text="Jersey" /></option>
                              <option value="JOR"><spring:message code="ssap.county.Jordan" text="Jordan" /></option>
                              <option value="KAZ"><spring:message code="ssap.county.Kazakhstan" text="Kazakhstan" /></option>
                              <option value="KEN"><spring:message code="ssap.county.Kenya" text="Kenya" /></option>
                              <option value="KIR"><spring:message code="ssap.county.Kiribati" text="Kiribati" /></option>
                              <option value="PRK"><spring:message code="ssap.county.Korea, Democratic People'S Republic Of" text="Korea, Democratic People'S Republic Of" /></option>
                              <option value="KOR"><spring:message code="ssap.county.Korea, Republic Of" text="Korea, Republic Of" /></option>
                              <option value="KVO"><spring:message code="ssap.county.Kosovo" text="Kosovo" /></option>
                              <option value="KWT"><spring:message code="ssap.county.Kuwait" text="Kuwait" /></option>
                              <option value="KGZ"><spring:message code="ssap.county.Kyrgyzstan" text="Kyrgyzstan" /></option>
                              <option value="LAO"><spring:message code="ssap.county.Lao People'S Democratic Republic" text="Lao People'S Democratic Republic" /></option>
                              <option value="LVA"><spring:message code="ssap.county.Latvia" text="Latvia" /></option>
                              <option value="LBN"><spring:message code="ssap.county.Lebanon" text="Lebanon" /></option>
                              <option value="LSO"><spring:message code="ssap.county.Lesotho" text="Lesotho" /></option>
                              <option value="LBR"><spring:message code="ssap.county.Liberia" text="Liberia" /></option>
                              <option value="LBY"><spring:message code="ssap.county.Libyan Arab Jamahiriya" text="Libyan Arab Jamahiriya" /></option>
                              <option value="LIE"><spring:message code="ssap.county.Liechtenstein" text="Liechtenstein" /></option>
                              <option value="LTU"><spring:message code="ssap.county.Lithuania" text="Lithuania" /></option>
                              <option value="LUX"><spring:message code="ssap.county.Luxembourg" text="Luxembourg" /></option>
                              <option value="MAC"><spring:message code="ssap.county.Macao" text="Macao" /></option>
                              <option value="MKD"><spring:message code="ssap.county.Macedonia, The Former Yugoslav Republic Of" text="Macedonia, The Former Yugoslav Republic Of" /></option>
                              <option value="MDG"><spring:message code="ssap.county.Madagascar" text="Madagascar" /></option>
                              <option value="MWI"><spring:message code="ssap.county.Malawi" text="Malawi" /></option>
                              <option value="MYS"><spring:message code="ssap.county.Malaysia" text="Malaysia" /></option>
                              <option value="MDV"><spring:message code="ssap.county.Maldives" text="Maldives" /></option>
                              <option value="MLI"><spring:message code="ssap.county.Mali" text="Mali" /></option>
                              <option value="MLT"><spring:message code="ssap.county.Malta" text="Malta" /></option>
                              <option value="MHL"><spring:message code="ssap.county.Marshall Islands" text="Marshall Islands" /></option>
                              <option value="MTQ"><spring:message code="ssap.county.Martinique" text="Martinique" /></option>
                              <option value="MRT"><spring:message code="ssap.county.Mauritania" text="Mauritania" /></option>
                              <option value="MUS"><spring:message code="ssap.county.Mauritius" text="Mauritius" /></option>
                              <option value="MYT"><spring:message code="ssap.county.Mayotte" text="Mayotte" /></option>
                              <option value="MEX"><spring:message code="ssap.county.Mexico" text="Mexico" /></option>
                              <option value="FSM"><spring:message code="ssap.county.Micronesia, Federated States Of" text="Micronesia, Federated States Of" /></option>
                              <option value="MDA"><spring:message code="ssap.county.Moldova, Republic Of" text="Moldova, Republic Of" /></option>
                              <option value="MCO"><spring:message code="ssap.county.Monaco" text="Monaco" /></option>
                              <option value="MNG"><spring:message code="ssap.county.Mongolia" text="Mongolia" /></option>
                              <option value="MNE"><spring:message code="ssap.county.Montenegro" text="Montenegro" /></option>
                              <option value="MSR"><spring:message code="ssap.county.Montserrat" text="Montserrat" /></option>
                              <option value="MAR"><spring:message code="ssap.county.Morocco" text="Morocco" /></option>
                              <option value="MOZ"><spring:message code="ssap.county.Mozambique" text="Mozambique" /></option>
                              <option value="MMR"><spring:message code="ssap.county.Myanmar" text="Myanmar" /></option>
                              <option value="NAM"><spring:message code="ssap.county.Namibia" text="Namibia" /></option>
                              <option value="NRU"><spring:message code="ssap.county.Nauru" text="Nauru" /></option>
                              <option value="NPL"><spring:message code="ssap.county.Nepal" text="Nepal" /></option>
                              <option value="NLD"><spring:message code="ssap.county.Netherlands" text="Netherlands" /></option>
                              <option value="ANT"><spring:message code="ssap.county.Netherlands Antilles" text="Netherlands Antilles" /></option>
                              <option value="NCL"><spring:message code="ssap.county.New Caledonia" text="New Caledonia" /></option>
                              <option value="NZL"><spring:message code="ssap.county.New Zealand" text="New Zealand" /></option>
                              <option value="NIC"><spring:message code="ssap.county.Nicaragua" text="Nicaragua" /></option>
                              <option value="NER"><spring:message code="ssap.county.Niger" text="Niger" /></option>
                              <option value="NGA"><spring:message code="ssap.county.Nigeria" text="Nigeria" /></option>
                              <option value="NIU"><spring:message code="ssap.county.Niue" text="Niue" /></option>
                              <option value="NFK"><spring:message code="ssap.county.Norfolk Island" text="Norfolk Island" /></option>
                              <option value="MNP"><spring:message code="ssap.county.Northern Mariana Islands" text="Northern Mariana Islands" /></option>
                              <option value="NOR"><spring:message code="ssap.county.Norway" text="Norway" /></option>
                              <option value="OMN"><spring:message code="ssap.county.Oman" text="Oman" /></option>
                              <option value="PAK"><spring:message code="ssap.county.Pakistan" text="Pakistan" /></option>
                              <option value="PLW"><spring:message code="ssap.county.Palau" text="Palau" /></option>
                              <option value="PSE"><spring:message code="ssap.county.Palestinian Territory, Occupied" text="Palestinian Territory, Occupied" /></option>
                              <option value="PAN"><spring:message code="ssap.county.Panama" text="Panama" /></option>
                              <option value="PNG"><spring:message code="ssap.county.Papua New Guinea" text="Papua New Guinea" /></option>
                              <option value="PRY"><spring:message code="ssap.county.Paraguay" text="Paraguay" /></option>
                              <option value="PER"><spring:message code="ssap.county.Peru" text="Peru" /></option>
                              <option value="PHL"><spring:message code="ssap.county.Philippines" text="Philippines" /></option>
                              <option value="PCN"><spring:message code="ssap.county.Pitcairn" text="Pitcairn" /></option>
                              <option value="POL"><spring:message code="ssap.county.Poland" text="Poland" /></option>
                              <option value="PRT"><spring:message code="ssap.county.Portugal" text="Portugal" /></option>
                              <option value="PRI"><spring:message code="ssap.county.Puerto Rico" text="Puerto Rico" /></option>
                              <option value="QAT"><spring:message code="ssap.county.Qatar" text="Qatar" /></option>
                              <option value="REU"><spring:message code="ssap.county.Reunion" text="Reunion" /></option>
                              <option value="ROU"><spring:message code="ssap.county.Romania" text="Romania" /></option>
                              <option value="RUS"><spring:message code="ssap.county.Russian Federation" text="Russian Federation" /></option>
                              <option value="RWA"><spring:message code="ssap.county.Rwanda" text="Rwanda" /></option>
                              <option value="BLM"><spring:message code="ssap.county.Saint Barthelemy" text="Saint Barthelemy" /></option>
                              <option value="SHN"><spring:message code="ssap.county.Saint Helena, Ascension And Tristan Da Cunha" text="Saint Helena, Ascension And Tristan Da Cunha" /></option>
                              <option value="KNA"><spring:message code="ssap.county.Saint Kitts And Nevis" text="Saint Kitts And Nevis" /></option>
                              <option value="LCA"><spring:message code="ssap.county.Saint Lucia" text="Saint Lucia" /></option>
                              <option value="MAF"><spring:message code="ssap.county.Saint Martin (French Part)" text="Saint Martin (French Part)" /></option>
                              <option value="SPM"><spring:message code="ssap.county.Saint Pierre And Miquelon" text="Saint Pierre And Miquelon" /></option>
                              <option value="VCT"><spring:message code="ssap.county.Saint Vincent And The Grenadines" text="Saint Vincent And The Grenadines" /></option>
                              <option value="WSM"><spring:message code="ssap.county.Samoa" text="Samoa" /></option>
                              <option value="SMR"><spring:message code="ssap.county.San Marino" text="San Marino" /></option>
                              <option value="STP"><spring:message code="ssap.county.Sao Tome And Principe" text="Sao Tome And Principe" /></option>
                              <option value="SAU"><spring:message code="ssap.county.Saudi Arabia" text="Saudi Arabia" /></option>
                              <option value="SEN"><spring:message code="ssap.county.Senegal" text="Senegal" /></option>
                              <option value="SRB"><spring:message code="ssap.county.Serbia" text="Serbia" /></option>
                              <option value="SYC"><spring:message code="ssap.county.Seychelles" text="Seychelles" /></option>
                              <option value="SLE"><spring:message code="ssap.county.Sierra Leone" text="Sierra Leone" /></option>
                              <option value="SGP"><spring:message code="ssap.county.Singapore" text="Singapore" /></option>
                              <option value="SVK"><spring:message code="ssap.county.Slovakia" text="Slovakia" /></option>
                              <option value="SVN"><spring:message code="ssap.county.Slovenia" text="Slovenia" /></option>
                              <option value="SLB"><spring:message code="ssap.county.Solomon Islands" text="Solomon Islands" /></option>
                              <option value="SOM"><spring:message code="ssap.county.Somalia" text="Somalia" /></option>
                              <option value="ZAF"><spring:message code="ssap.county.South Africa" text="South Africa" /></option>
                              <option value="SGS"><spring:message code="ssap.county.South Georgia And The South Sandwich Islands" text="South Georgia And The South Sandwich Islands" /></option>
                              <option value="SSD"><spring:message code="ssap.county.South Sudan" text="South Sudan" /></option>
                              <option value="ESP"><spring:message code="ssap.county.Spain" text="Spain" /></option>
                              <option value="LKA"><spring:message code="ssap.county.Sri Lanka" text="Sri Lanka" /></option>
                              <option value="STL"><spring:message code="ssap.county.Stateless" text="Stateless" /></option>
                              <option value="SDN"><spring:message code="ssap.county.Sudan" text="Sudan" /></option>
                              <option value="SUR"><spring:message code="ssap.county.Suriname" text="Suriname" /></option>
                              <option value="SJM"><spring:message code="ssap.county.Svalbard And Jan Mayen" text="Svalbard And Jan Mayen" /></option>
                              <option value="SWZ"><spring:message code="ssap.county.Swaziland" text="Swaziland" /></option>
                              <option value="SWE"><spring:message code="ssap.county.Sweden" text="Sweden" /></option>
                              <option value="CHE"><spring:message code="ssap.county.Switzerland" text="Switzerland" /></option>
                              <option value="SYR"><spring:message code="ssap.county.Syrian Arab Republic" text="Syrian Arab Republic" /></option>
                              <option value="TWN"><spring:message code="ssap.county.Taiwan, Province Of China" text="Taiwan, Province Of China" /></option>
                              <option value="TJK"><spring:message code="ssap.county.Tajikistan" text="Tajikistan" /></option>
                              <option value="TZA"><spring:message code="ssap.county.Tanzania, United Republic Of" text="Tanzania, United Republic Of" /></option>
                              <option value="THA"><spring:message code="ssap.county.Thailand" text="Thailand" /></option>
                              <option value="TLS"><spring:message code="ssap.county.Timor-Leste" text="Timor-Leste" /></option>
                              <option value="TGO"><spring:message code="ssap.county.Togo" text="Togo" /></option>
                              <option value="TKL"><spring:message code="ssap.county.Tokelau" text="Tokelau" /></option>
                              <option value="TON"><spring:message code="ssap.county.Tonga" text="Tonga" /></option>
                              <option value="TTO"><spring:message code="ssap.county.Trinidad And Tobago" text="Trinidad And Tobago" /></option>
                              <option value="TUN"><spring:message code="ssap.county.Tunisia" text="Tunisia" /></option>
                              <option value="TUR"><spring:message code="ssap.county.Turkey" text="Turkey" /></option>
                              <option value="TKM"><spring:message code="ssap.county.Turkmenistan" text="Turkmenistan" /></option>
                              <option value="TCA"><spring:message code="ssap.county.Turks And Caicos Islands" text="Turks And Caicos Islands" /></option>
                              <option value="TUV"><spring:message code="ssap.county.Tuvalu" text="Tuvalu" /></option>
                              <option value="UGA"><spring:message code="ssap.county.Uganda" text="Uganda" /></option>
                              <option value="UKR"><spring:message code="ssap.county.Ukraine" text="Ukraine" /></option>
                              <option value="ARE"><spring:message code="ssap.county.United Arab Emirates" text="United Arab Emirates" /></option>
                              <option value="GBR"><spring:message code="ssap.county.United Kingdom" text="United Kingdom" /></option>
                              <option value="USA"><spring:message code="ssap.county.United States" text="United States" /></option>
                              <option value="UMI"><spring:message code="ssap.county.United States Minor Outlying Islands" text="United States Minor Outlying Islands" /></option>
                              <option value="URY"><spring:message code="ssap.county.Uruguay" text="Uruguay" /></option>
                              <option value="UZB"><spring:message code="ssap.county.Uzbekistan" text="Uzbekistan" /></option>
                              <option value="VUT"><spring:message code="ssap.county.Vanuatu" text="Vanuatu" /></option>
                              <option value="VEN"><spring:message code="ssap.county.Venezuela, Bolivarian Republic Of" text="Venezuela, Bolivarian Republic Of" /></option>
                              <option value="VNM"><spring:message code="ssap.county.Viet Nam" text="Viet Nam" /></option>
                              <option value="VGB"><spring:message code="ssap.county.Virgin Islands (British)" text="Virgin Islands (British)" /></option>
                              <option value="VIR"><spring:message code="ssap.county.Virgin Islands (U.S.)" text="Virgin Islands (U.S.)" /></option>
                              <option value="WLF"><spring:message code="ssap.county.Wallis And Futuna" text="Wallis And Futuna" /></option>
                              <option value="ESH"><spring:message code="ssap.county.Western Sahara *" text="Western Sahara *" /></option>
                              <option value="YEM"><spring:message code="ssap.county.Yemen" text="Yemen" /></option>
                              <option value="ZMB"><spring:message code="ssap.county.Zambia" text="Zambia" /></option>
                              <option value="ZWE"><spring:message code="ssap.county.Zimbabwe" text="Zimbabwe" /></option>
                           </select>
                           <span class="errorMessage"></span>
                        </div>
                     </div>                     
                     <div class="control-group">
                        <label class="control-label" for="tempcarddetailsPassportexpdate">
                           <spring:message code="ssap.page12A.expDate" text="Passport Expiration Date"/>
                        </label>
                        <div class="controls">
                           <div data-date="" class="input-append date date-picker">
                              <input type="text" class="input-medium dob-input" name="tempcarddetailsdateOfExp" id="tempcarddetailsPassportexpdate" data-parsley-trigger="change" data-parsley-errors-container="#tempcarddetailsPassportexpdateErrorDiv" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-pattern = "(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" />
                              <span class="add-on dateSpan" ><i class="icon-calendar"></i></span>
                           </div>
                           <div id="tempcarddetailsPassportexpdateErrorDiv"></div>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->               
                  </div>
                  
                  <label class="radio" for="machinecarddetails" >
                     <input type="radio" id="machinecarddetails" name="docType" value="0"><spring:message code="ssap.page12A.machineReadableImmigrationVisa" text="Machine Readable Immigrant Visa (with temporary I-551 language)"/>
                  </label>          
                  <div  class="immigrationDocType hide" id="machinecarddetailsdiv">
                     <div class="control-group">
                        <label class="control-label" for="machinecarddetailsAlignNumber">
                           <spring:message code="ssap.page12A.alignNumber" text="Alien Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.placeholder.alignNum" text="Alien Number"/>" type="text" id="machinecarddetailsAlignNumber" name="machinecarddetailsAlignNumber" data-parsley-pattern="^[0-9]{7,9}$"  data-parsley-pattern-message="<spring:message code='ssap.error.alienNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.alienNumRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="machinecarddetailsppcardno">
                           <spring:message code="ssap.page12A.passportNumber" text="Passport Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.passportNumber" text="Passport Number"/>" type="text" id="machinecarddetailsppcardno" name="machinecarddetailsppcardno"  data-parsley-pattern="^[a-zA-Z0-9]{6,12}$"  data-parsley-pattern-message="<spring:message code='ssap.error.passportNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.passportNumRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="machinecarddetailsCounty">
                           <spring:message code="ssap.page12A.foreignPassportCountryOfIssuance" text="Country of Issuance"/>
                           <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                           <span aria-label="Required!"></span>
                        </label>
                        <div class="controls">
                           <select name="machinecarddetailsCounty" id="machinecarddetailsCounty" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.passportCountryRequired' />">
                              <option value=""><spring:message code="ssap.County" text="Country" /></option>
                              <option value="AFG"><spring:message code="ssap.county.Afghanistan" text="Afghanistan" /></option>
                              <option value="ALB"><spring:message code="ssap.county.Albania" text="Albania" /></option>
                              <option value="DZA"><spring:message code="ssap.county.Algeria" text="Algeria" /></option>
                              <option value="ASM"><spring:message code="ssap.county.American Samoa" text="American Samoa" /></option>
                              <option value="AND"><spring:message code="ssap.county.Andorra" text="Andorra" /></option>
                              <option value="AGO"><spring:message code="ssap.county.Angola" text="Angola" /></option>
                              <option value="AIA"><spring:message code="ssap.county.Anguilla" text="Anguilla" /></option>
                              <option value="ATA"><spring:message code="ssap.county.Antarctica" text="Antarctica" /></option>
                              <option value="ATG"><spring:message code="ssap.county.Antigua And Barbuda" text="Antigua And Barbuda" /></option>
                              <option value="ARG"><spring:message code="ssap.county.Argentina" text="Argentina" /></option>
                              <option value="ARM"><spring:message code="ssap.county.Armenia" text="Armenia" /></option>
                              <option value="ABW"><spring:message code="ssap.county.Aruba" text="Aruba" /></option>
                              <option value="AUS"><spring:message code="ssap.county.Australia" text="Australia" /></option>
                              <option value="AUT"><spring:message code="ssap.county.Austria" text="Austria" /></option>
                              <option value="AZE"><spring:message code="ssap.county.Azerbaijan" text="Azerbaijan" /></option>
                              <option value="BHS"><spring:message code="ssap.county.Bahamas" text="Bahamas" /></option>
                              <option value="BHR"><spring:message code="ssap.county.Bahrain" text="Bahrain" /></option>
                              <option value="BGD"><spring:message code="ssap.county.Bangladesh" text="Bangladesh" /></option>
                              <option value="BRB"><spring:message code="ssap.county.Barbados" text="Barbados" /></option>
                              <option value="BLR"><spring:message code="ssap.county.Belarus" text="Belarus" /></option>
                              <option value="BEL"><spring:message code="ssap.county.Belgium" text="Belgium" /></option>
                              <option value="BLZ"><spring:message code="ssap.county.Belize" text="Belize" /></option>
                              <option value="BEN"><spring:message code="ssap.county.Benin" text="Benin" /></option>
                              <option value="BMU"><spring:message code="ssap.county.Bermuda" text="Bermuda" /></option>
                              <option value="BTN"><spring:message code="ssap.county.Bhutan" text="Bhutan" /></option>
                              <option value="BOL"><spring:message code="ssap.county.Bolivia, Plurinational State Of" text="Bolivia, Plurinational State Of" /></option>
                              <option value="BIH"><spring:message code="ssap.county.Bosnia And Herzegovina" text="Bosnia And Herzegovina" /></option>
                              <option value="BWA"><spring:message code="ssap.county.Botswana" text="Botswana" /></option>
                              <option value="BVT"><spring:message code="ssap.county.Bouvet Island" text="Bouvet Island" /></option>
                              <option value="BRA"><spring:message code="ssap.county.Brazil" text="Brazil" /></option>
                              <option value="IOT"><spring:message code="ssap.county.British Indian Ocean Territory" text="British Indian Ocean Territory" /></option>
                              <option value="BRN"><spring:message code="ssap.county.Brunei Darussalam" text="Brunei Darussalam" /></option>
                              <option value="BGR"><spring:message code="ssap.county.Bulgaria" text="Bulgaria" /></option>
                              <option value="BFA"><spring:message code="ssap.county.Burkina Faso" text="Burkina Faso" /></option>
                              <option value="BDI"><spring:message code="ssap.county.Burundi" text="Burundi" /></option>
                              <option value="KHM"><spring:message code="ssap.county.Cambodia" text="Cambodia" /></option>
                              <option value="CMR"><spring:message code="ssap.county.Cameroon" text="Cameroon" /></option>
                              <option value="CAN"><spring:message code="ssap.county.Canada" text="Canada" /></option>
                              <option value="CPV"><spring:message code="ssap.county.Cape Verde" text="Cape Verde" /></option>
                              <option value="CYM"><spring:message code="ssap.county.Cayman Islands" text="Cayman Islands" /></option>
                              <option value="CAF"><spring:message code="ssap.county.Central African Republic" text="Central African Republic" /></option>
                              <option value="CIV"><spring:message code="ssap.county.Cete D'Ivoire" text="Cete D'Ivoire" /></option>
                              <option value="TCD"><spring:message code="ssap.county.Chad" text="Chad" /></option>
                              <option value="CHL"><spring:message code="ssap.county.Chile" text="Chile" /></option>
                              <option value="CHN"><spring:message code="ssap.county.China" text="China" /></option>
                              <option value="CXR"><spring:message code="ssap.county.Christmas Island" text="Christmas Island" /></option>
                              <option value="CCK"><spring:message code="ssap.county.Cocos (Keeling) Islands" text="Cocos (Keeling) Islands" /></option>
                              <option value="COL"><spring:message code="ssap.county.Colombia" text="Colombia" /></option>
                              <option value="COM"><spring:message code="ssap.county.Comoros" text="Comoros" /></option>
                              <option value="COG"><spring:message code="ssap.county.Congo" text="Congo" /></option>
                              <option value="COD"><spring:message code="ssap.county.Congo, Democratic Republic Of The" text="Congo, Democratic Republic Of The" /></option>
                              <option value="COK"><spring:message code="ssap.county.Cook Islands" text="Cook Islands" /></option>
                              <option value="CRI"><spring:message code="ssap.county.Costa Rica" text="Costa Rica" /></option>
                              <option value="HRV"><spring:message code="ssap.county.Croatia" text="Croatia" /></option>
                              <option value="CUB"><spring:message code="ssap.county.Cuba" text="Cuba" /></option>
                              <option value="CYP"><spring:message code="ssap.county.Cyprus" text="Cyprus" /></option>
                              <option value="CZE"><spring:message code="ssap.county.Czech Republic" text="Czech Republic" /></option>
                              <option value="DNK"><spring:message code="ssap.county.Denmark" text="Denmark" /></option>
                              <option value="DJI"><spring:message code="ssap.county.Djibouti" text="Djibouti" /></option>
                              <option value="DMA"><spring:message code="ssap.county.Dominica" text="Dominica" /></option>
                              <option value="DOM"><spring:message code="ssap.county.Dominican Republic" text="Dominican Republic" /></option>
                              <option value="ECU"><spring:message code="ssap.county.Ecuador" text="Ecuador" /></option>
                              <option value="EGY"><spring:message code="ssap.county.Egypt" text="Egypt" /></option>
                              <option value="SLV"><spring:message code="ssap.county.El Salvador" text="El Salvador" /></option>
                              <option value="ALA"><spring:message code="ssap.county.Eland Islands" text="Eland Islands" /></option>
                              <option value="GNQ"><spring:message code="ssap.county.Equatorial Guinea" text="Equatorial Guinea" /></option>
                              <option value="ERI"><spring:message code="ssap.county.Eritrea" text="Eritrea" /></option>
                              <option value="EST"><spring:message code="ssap.county.Estonia" text="Estonia" /></option>
                              <option value="ETH"><spring:message code="ssap.county.Ethiopia" text="Ethiopia" /></option>
                              <option value="FLK"><spring:message code="ssap.county.Falkland Islands (Malvinas)" text="Falkland Islands (Malvinas)" /></option>
                              <option value="FRO"><spring:message code="ssap.county.Faroe Islands" text="Faroe Islands" /></option>
                              <option value="FJI"><spring:message code="ssap.county.Fiji" text="Fiji" /></option>
                              <option value="FIN"><spring:message code="ssap.county.Finland" text="Finland" /></option>
                              <option value="FRA"><spring:message code="ssap.county.France" text="France" /></option>
                              <option value="GUF"><spring:message code="ssap.county.French Guiana" text="French Guiana" /></option>
                              <option value="PYF"><spring:message code="ssap.county.French Polynesia" text="French Polynesia" /></option>
                              <option value="ATF"><spring:message code="ssap.county.French Southern Territories" text="French Southern Territories" /></option>
                              <option value="GAB"><spring:message code="ssap.county.Gabon" text="Gabon" /></option>
                              <option value="GMB"><spring:message code="ssap.county.Gambia" text="Gambia" /></option>
                              <option value="GEO"><spring:message code="ssap.county.Georgia" text="Georgia" /></option>
                              <option value="DEU"><spring:message code="ssap.county.Germany" text="Germany" /></option>
                              <option value="GHA"><spring:message code="ssap.county.Ghana" text="Ghana" /></option>
                              <option value="GIB"><spring:message code="ssap.county.Gibraltar" text="Gibraltar" /></option>
                              <option value="GRC"><spring:message code="ssap.county.Greece" text="Greece" /></option>
                              <option value="GRL"><spring:message code="ssap.county.Greenland" text="Greenland" /></option>
                              <option value="GRD"><spring:message code="ssap.county.Grenada" text="Grenada" /></option>
                              <option value="GLP"><spring:message code="ssap.county.Guadeloupe" text="Guadeloupe" /></option>
                              <option value="GUM"><spring:message code="ssap.county.Guam" text="Guam" /></option>
                              <option value="GTM"><spring:message code="ssap.county.Guatemala" text="Guatemala" /></option>
                              <option value="GGY"><spring:message code="ssap.county.Guernsey" text="Guernsey" /></option>
                              <option value="GIN"><spring:message code="ssap.county.Guinea" text="Guinea" /></option>
                              <option value="GNB"><spring:message code="ssap.county.Guinea-Bissau" text="Guinea-Bissau" /></option>
                              <option value="GUY"><spring:message code="ssap.county.Guyana" text="Guyana" /></option>
                              <option value="HTI"><spring:message code="ssap.county.Haiti" text="Haiti" /></option>
                              <option value="HMD"><spring:message code="ssap.county.Heard Island And Mcdonald Islands" text="Heard Island And Mcdonald Islands" /></option>
                              <option value="VAT"><spring:message code="ssap.county.Holy See (Vatican City State)" text="Holy See (Vatican City State)" /></option>
                              <option value="HND"><spring:message code="ssap.county.Honduras" text="Honduras" /></option>
                              <option value="HKG"><spring:message code="ssap.county.Hong Kong" text="Hong Kong" /></option>
                              <option value="HUN"><spring:message code="ssap.county.Hungary" text="Hungary" /></option>
                              <option value="ISL"><spring:message code="ssap.county.Iceland" text="Iceland" /></option>
                              <option value="IND"><spring:message code="ssap.county.India" text="India" /></option>
                              <option value="IDN"><spring:message code="ssap.county.Indonesia" text="Indonesia" /></option>
                              <option value="IRN"><spring:message code="ssap.county.Iran, Islamic Republic Of" text="Iran, Islamic Republic Of" /></option>
                              <option value="IRQ"><spring:message code="ssap.county.Iraq" text="Iraq" /></option>
                              <option value="IRL"><spring:message code="ssap.county.Ireland" text="Ireland" /></option>
                              <option value="IMN"><spring:message code="ssap.county.Isle Of Man" text="Isle Of Man" /></option>
                              <option value="ISR"><spring:message code="ssap.county.Israel" text="Israel" /></option>
                              <option value="ITA"><spring:message code="ssap.county.Italy" text="Italy" /></option>
                              <option value="JAM"><spring:message code="ssap.county.Jamaica" text="Jamaica" /></option>
                              <option value="JPN"><spring:message code="ssap.county.Japan" text="Japan" /></option>
                              <option value="JEY"><spring:message code="ssap.county.Jersey" text="Jersey" /></option>
                              <option value="JOR"><spring:message code="ssap.county.Jordan" text="Jordan" /></option>
                              <option value="KAZ"><spring:message code="ssap.county.Kazakhstan" text="Kazakhstan" /></option>
                              <option value="KEN"><spring:message code="ssap.county.Kenya" text="Kenya" /></option>
                              <option value="KIR"><spring:message code="ssap.county.Kiribati" text="Kiribati" /></option>
                              <option value="PRK"><spring:message code="ssap.county.Korea, Democratic People'S Republic Of" text="Korea, Democratic People'S Republic Of" /></option>
                              <option value="KOR"><spring:message code="ssap.county.Korea, Republic Of" text="Korea, Republic Of" /></option>
                              <option value="KVO"><spring:message code="ssap.county.Kosovo" text="Kosovo" /></option>
                              <option value="KWT"><spring:message code="ssap.county.Kuwait" text="Kuwait" /></option>
                              <option value="KGZ"><spring:message code="ssap.county.Kyrgyzstan" text="Kyrgyzstan" /></option>
                              <option value="LAO"><spring:message code="ssap.county.Lao People'S Democratic Republic" text="Lao People'S Democratic Republic" /></option>
                              <option value="LVA"><spring:message code="ssap.county.Latvia" text="Latvia" /></option>
                              <option value="LBN"><spring:message code="ssap.county.Lebanon" text="Lebanon" /></option>
                              <option value="LSO"><spring:message code="ssap.county.Lesotho" text="Lesotho" /></option>
                              <option value="LBR"><spring:message code="ssap.county.Liberia" text="Liberia" /></option>
                              <option value="LBY"><spring:message code="ssap.county.Libyan Arab Jamahiriya" text="Libyan Arab Jamahiriya" /></option>
                              <option value="LIE"><spring:message code="ssap.county.Liechtenstein" text="Liechtenstein" /></option>
                              <option value="LTU"><spring:message code="ssap.county.Lithuania" text="Lithuania" /></option>
                              <option value="LUX"><spring:message code="ssap.county.Luxembourg" text="Luxembourg" /></option>
                              <option value="MAC"><spring:message code="ssap.county.Macao" text="Macao" /></option>
                              <option value="MKD"><spring:message code="ssap.county.Macedonia, The Former Yugoslav Republic Of" text="Macedonia, The Former Yugoslav Republic Of" /></option>
                              <option value="MDG"><spring:message code="ssap.county.Madagascar" text="Madagascar" /></option>
                              <option value="MWI"><spring:message code="ssap.county.Malawi" text="Malawi" /></option>
                              <option value="MYS"><spring:message code="ssap.county.Malaysia" text="Malaysia" /></option>
                              <option value="MDV"><spring:message code="ssap.county.Maldives" text="Maldives" /></option>
                              <option value="MLI"><spring:message code="ssap.county.Mali" text="Mali" /></option>
                              <option value="MLT"><spring:message code="ssap.county.Malta" text="Malta" /></option>
                              <option value="MHL"><spring:message code="ssap.county.Marshall Islands" text="Marshall Islands" /></option>
                              <option value="MTQ"><spring:message code="ssap.county.Martinique" text="Martinique" /></option>
                              <option value="MRT"><spring:message code="ssap.county.Mauritania" text="Mauritania" /></option>
                              <option value="MUS"><spring:message code="ssap.county.Mauritius" text="Mauritius" /></option>
                              <option value="MYT"><spring:message code="ssap.county.Mayotte" text="Mayotte" /></option>
                              <option value="MEX"><spring:message code="ssap.county.Mexico" text="Mexico" /></option>
                              <option value="FSM"><spring:message code="ssap.county.Micronesia, Federated States Of" text="Micronesia, Federated States Of" /></option>
                              <option value="MDA"><spring:message code="ssap.county.Moldova, Republic Of" text="Moldova, Republic Of" /></option>
                              <option value="MCO"><spring:message code="ssap.county.Monaco" text="Monaco" /></option>
                              <option value="MNG"><spring:message code="ssap.county.Mongolia" text="Mongolia" /></option>
                              <option value="MNE"><spring:message code="ssap.county.Montenegro" text="Montenegro" /></option>
                              <option value="MSR"><spring:message code="ssap.county.Montserrat" text="Montserrat" /></option>
                              <option value="MAR"><spring:message code="ssap.county.Morocco" text="Morocco" /></option>
                              <option value="MOZ"><spring:message code="ssap.county.Mozambique" text="Mozambique" /></option>
                              <option value="MMR"><spring:message code="ssap.county.Myanmar" text="Myanmar" /></option>
                              <option value="NAM"><spring:message code="ssap.county.Namibia" text="Namibia" /></option>
                              <option value="NRU"><spring:message code="ssap.county.Nauru" text="Nauru" /></option>
                              <option value="NPL"><spring:message code="ssap.county.Nepal" text="Nepal" /></option>
                              <option value="NLD"><spring:message code="ssap.county.Netherlands" text="Netherlands" /></option>
                              <option value="ANT"><spring:message code="ssap.county.Netherlands Antilles" text="Netherlands Antilles" /></option>
                              <option value="NCL"><spring:message code="ssap.county.New Caledonia" text="New Caledonia" /></option>
                              <option value="NZL"><spring:message code="ssap.county.New Zealand" text="New Zealand" /></option>
                              <option value="NIC"><spring:message code="ssap.county.Nicaragua" text="Nicaragua" /></option>
                              <option value="NER"><spring:message code="ssap.county.Niger" text="Niger" /></option>
                              <option value="NGA"><spring:message code="ssap.county.Nigeria" text="Nigeria" /></option>
                              <option value="NIU"><spring:message code="ssap.county.Niue" text="Niue" /></option>
                              <option value="NFK"><spring:message code="ssap.county.Norfolk Island" text="Norfolk Island" /></option>
                              <option value="MNP"><spring:message code="ssap.county.Northern Mariana Islands" text="Northern Mariana Islands" /></option>
                              <option value="NOR"><spring:message code="ssap.county.Norway" text="Norway" /></option>
                              <option value="OMN"><spring:message code="ssap.county.Oman" text="Oman" /></option>
                              <option value="PAK"><spring:message code="ssap.county.Pakistan" text="Pakistan" /></option>
                              <option value="PLW"><spring:message code="ssap.county.Palau" text="Palau" /></option>
                              <option value="PSE"><spring:message code="ssap.county.Palestinian Territory, Occupied" text="Palestinian Territory, Occupied" /></option>
                              <option value="PAN"><spring:message code="ssap.county.Panama" text="Panama" /></option>
                              <option value="PNG"><spring:message code="ssap.county.Papua New Guinea" text="Papua New Guinea" /></option>
                              <option value="PRY"><spring:message code="ssap.county.Paraguay" text="Paraguay" /></option>
                              <option value="PER"><spring:message code="ssap.county.Peru" text="Peru" /></option>
                              <option value="PHL"><spring:message code="ssap.county.Philippines" text="Philippines" /></option>
                              <option value="PCN"><spring:message code="ssap.county.Pitcairn" text="Pitcairn" /></option>
                              <option value="POL"><spring:message code="ssap.county.Poland" text="Poland" /></option>
                              <option value="PRT"><spring:message code="ssap.county.Portugal" text="Portugal" /></option>
                              <option value="PRI"><spring:message code="ssap.county.Puerto Rico" text="Puerto Rico" /></option>
                              <option value="QAT"><spring:message code="ssap.county.Qatar" text="Qatar" /></option>
                              <option value="REU"><spring:message code="ssap.county.Reunion" text="Reunion" /></option>
                              <option value="ROU"><spring:message code="ssap.county.Romania" text="Romania" /></option>
                              <option value="RUS"><spring:message code="ssap.county.Russian Federation" text="Russian Federation" /></option>
                              <option value="RWA"><spring:message code="ssap.county.Rwanda" text="Rwanda" /></option>
                              <option value="BLM"><spring:message code="ssap.county.Saint Barthelemy" text="Saint Barthelemy" /></option>
                              <option value="SHN"><spring:message code="ssap.county.Saint Helena, Ascension And Tristan Da Cunha" text="Saint Helena, Ascension And Tristan Da Cunha" /></option>
                              <option value="KNA"><spring:message code="ssap.county.Saint Kitts And Nevis" text="Saint Kitts And Nevis" /></option>
                              <option value="LCA"><spring:message code="ssap.county.Saint Lucia" text="Saint Lucia" /></option>
                              <option value="MAF"><spring:message code="ssap.county.Saint Martin (French Part)" text="Saint Martin (French Part)" /></option>
                              <option value="SPM"><spring:message code="ssap.county.Saint Pierre And Miquelon" text="Saint Pierre And Miquelon" /></option>
                              <option value="VCT"><spring:message code="ssap.county.Saint Vincent And The Grenadines" text="Saint Vincent And The Grenadines" /></option>
                              <option value="WSM"><spring:message code="ssap.county.Samoa" text="Samoa" /></option>
                              <option value="SMR"><spring:message code="ssap.county.San Marino" text="San Marino" /></option>
                              <option value="STP"><spring:message code="ssap.county.Sao Tome And Principe" text="Sao Tome And Principe" /></option>
                              <option value="SAU"><spring:message code="ssap.county.Saudi Arabia" text="Saudi Arabia" /></option>
                              <option value="SEN"><spring:message code="ssap.county.Senegal" text="Senegal" /></option>
                              <option value="SRB"><spring:message code="ssap.county.Serbia" text="Serbia" /></option>
                              <option value="SYC"><spring:message code="ssap.county.Seychelles" text="Seychelles" /></option>
                              <option value="SLE"><spring:message code="ssap.county.Sierra Leone" text="Sierra Leone" /></option>
                              <option value="SGP"><spring:message code="ssap.county.Singapore" text="Singapore" /></option>
                              <option value="SVK"><spring:message code="ssap.county.Slovakia" text="Slovakia" /></option>
                              <option value="SVN"><spring:message code="ssap.county.Slovenia" text="Slovenia" /></option>
                              <option value="SLB"><spring:message code="ssap.county.Solomon Islands" text="Solomon Islands" /></option>
                              <option value="SOM"><spring:message code="ssap.county.Somalia" text="Somalia" /></option>
                              <option value="ZAF"><spring:message code="ssap.county.South Africa" text="South Africa" /></option>
                              <option value="SGS"><spring:message code="ssap.county.South Georgia And The South Sandwich Islands" text="South Georgia And The South Sandwich Islands" /></option>
                              <option value="SSD"><spring:message code="ssap.county.South Sudan" text="South Sudan" /></option>
                              <option value="ESP"><spring:message code="ssap.county.Spain" text="Spain" /></option>
                              <option value="LKA"><spring:message code="ssap.county.Sri Lanka" text="Sri Lanka" /></option>
                              <option value="STL"><spring:message code="ssap.county.Stateless" text="Stateless" /></option>
                              <option value="SDN"><spring:message code="ssap.county.Sudan" text="Sudan" /></option>
                              <option value="SUR"><spring:message code="ssap.county.Suriname" text="Suriname" /></option>
                              <option value="SJM"><spring:message code="ssap.county.Svalbard And Jan Mayen" text="Svalbard And Jan Mayen" /></option>
                              <option value="SWZ"><spring:message code="ssap.county.Swaziland" text="Swaziland" /></option>
                              <option value="SWE"><spring:message code="ssap.county.Sweden" text="Sweden" /></option>
                              <option value="CHE"><spring:message code="ssap.county.Switzerland" text="Switzerland" /></option>
                              <option value="SYR"><spring:message code="ssap.county.Syrian Arab Republic" text="Syrian Arab Republic" /></option>
                              <option value="TWN"><spring:message code="ssap.county.Taiwan, Province Of China" text="Taiwan, Province Of China" /></option>
                              <option value="TJK"><spring:message code="ssap.county.Tajikistan" text="Tajikistan" /></option>
                              <option value="TZA"><spring:message code="ssap.county.Tanzania, United Republic Of" text="Tanzania, United Republic Of" /></option>
                              <option value="THA"><spring:message code="ssap.county.Thailand" text="Thailand" /></option>
                              <option value="TLS"><spring:message code="ssap.county.Timor-Leste" text="Timor-Leste" /></option>
                              <option value="TGO"><spring:message code="ssap.county.Togo" text="Togo" /></option>
                              <option value="TKL"><spring:message code="ssap.county.Tokelau" text="Tokelau" /></option>
                              <option value="TON"><spring:message code="ssap.county.Tonga" text="Tonga" /></option>
                              <option value="TTO"><spring:message code="ssap.county.Trinidad And Tobago" text="Trinidad And Tobago" /></option>
                              <option value="TUN"><spring:message code="ssap.county.Tunisia" text="Tunisia" /></option>
                              <option value="TUR"><spring:message code="ssap.county.Turkey" text="Turkey" /></option>
                              <option value="TKM"><spring:message code="ssap.county.Turkmenistan" text="Turkmenistan" /></option>
                              <option value="TCA"><spring:message code="ssap.county.Turks And Caicos Islands" text="Turks And Caicos Islands" /></option>
                              <option value="TUV"><spring:message code="ssap.county.Tuvalu" text="Tuvalu" /></option>
                              <option value="UGA"><spring:message code="ssap.county.Uganda" text="Uganda" /></option>
                              <option value="UKR"><spring:message code="ssap.county.Ukraine" text="Ukraine" /></option>
                              <option value="ARE"><spring:message code="ssap.county.United Arab Emirates" text="United Arab Emirates" /></option>
                              <option value="GBR"><spring:message code="ssap.county.United Kingdom" text="United Kingdom" /></option>
                              <option value="USA"><spring:message code="ssap.county.United States" text="United States" /></option>
                              <option value="UMI"><spring:message code="ssap.county.United States Minor Outlying Islands" text="United States Minor Outlying Islands" /></option>
                              <option value="URY"><spring:message code="ssap.county.Uruguay" text="Uruguay" /></option>
                              <option value="UZB"><spring:message code="ssap.county.Uzbekistan" text="Uzbekistan" /></option>
                              <option value="VUT"><spring:message code="ssap.county.Vanuatu" text="Vanuatu" /></option>
                              <option value="VEN"><spring:message code="ssap.county.Venezuela, Bolivarian Republic Of" text="Venezuela, Bolivarian Republic Of" /></option>
                              <option value="VNM"><spring:message code="ssap.county.Viet Nam" text="Viet Nam" /></option>
                              <option value="VGB"><spring:message code="ssap.county.Virgin Islands (British)" text="Virgin Islands (British)" /></option>
                              <option value="VIR"><spring:message code="ssap.county.Virgin Islands (U.S.)" text="Virgin Islands (U.S.)" /></option>
                              <option value="WLF"><spring:message code="ssap.county.Wallis And Futuna" text="Wallis And Futuna" /></option>
                              <option value="ESH"><spring:message code="ssap.county.Western Sahara *" text="Western Sahara *" /></option>
                              <option value="YEM"><spring:message code="ssap.county.Yemen" text="Yemen" /></option>
                              <option value="ZMB"><spring:message code="ssap.county.Zambia" text="Zambia" /></option>
                              <option value="ZWE"><spring:message code="ssap.county.Zimbabwe" text="Zimbabwe" /></option>
                           </select>
                           <span class="errorMessage"></span>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label" for="machinecarddetailsdateOfExp">
                           <spring:message code="ssap.page12A.expDate" text="Passport Expiration Date"/>
                        </label>
                        <div class="controls">
                           <div data-date="" class="input-append date date-picker">
                              <input type="text" class="input-medium dob-input" name="machinecarddetailsdateOfExp" id="machinecarddetailsdateOfExp" data-parsley-trigger="change" data-parsley-errors-container="#machinecarddetailsdateOfExpErrorDiv" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-pattern = "(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>"/>
                              <span class="add-on dateSpan" ><i class="icon-calendar"></i></span>
                           </div>
                           <div id="machinecarddetailsdateOfExpErrorDiv"></div>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="machinecarddetailsppvisano">
                           <spring:message code="ssap.page12A.visaNumber" text="Visa Number"/>
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.visaNumber" text="Visa Number"/>" type="text" id="machinecarddetailsppvisano" name="machnaturalizationppvisano" data-parsley-pattern="^[a-zA-Z0-9]{8}$"  data-parsley-pattern-message="<spring:message code='ssap.error.visaNumValid'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->            
                  </div>                  
                  
                  <label class="radio" for="">
                     <input type="radio" name="docType" id="empauthcarddetails" value="0"><spring:message code="ssap.page12A.empauthcarddetails" text="Employment Authorization Card (EAD, I-766)"/>
                  </label>          
                  <div class="immigrationDocType hide" id="empauthcarddetailsdiv">
                     <div class="control-group">
                        <label class="control-label" for="empauthcarddetailsAlignNumber">
                           <spring:message code="ssap.page12A.alignNumber" text="Alien Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.placeholder.alignNum" text="Alien Number"/>" type="text" id="empauthcarddetailsAlignNumber" name="empauthcarddetailsAlignNumber" data-parsley-pattern="^[0-9]{7,9}$"  data-parsley-pattern-message="<spring:message code='ssap.error.alienNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.alienNumRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="empauthcarddetailsppcardno">
                           <spring:message code="ssap.page12A.cardNumber" text="Card Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.cardNumber" text="Card Number"/>" type="text" id="empauthcarddetailsppcardno" name="empauthcarddetailsppcardno"  data-parsley-pattern="^[a-zA-Z]{3}[0-9]{10}$"  data-parsley-pattern-message="<spring:message code='ssap.error.cardNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.cardNumRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="empauthcarddetailsdateOfExp">
                           <spring:message code="ssap.page12A.expDate" text="Expiration Date"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <div data-date="" class="input-append date date-picker">
                              <input type="text" class="input-medium dob-input" name="empauthcarddetailsdateOfExp" id="empauthcarddetailsdateOfExp" data-parsley-trigger="change" data-parsley-errors-container="#empauthcarddetailsdateOfExpErrorDiv" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-pattern = "(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.expDateRequired' text = 'Please Enter Expiration Date'/>" />
                              <span class="add-on dateSpan" ><i class="icon-calendar"></i></span>
                           </div>
                           <div id="empauthcarddetailsdateOfExpErrorDiv"></div>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->               
                  </div>
                  
                  <label class="radio" for="arrivaldeparturerecord">                       
                     <input type="radio" id="arrivaldeparturerecord" name="docType" value="arrivaldeparturerecord"><spring:message code="ssap.page12A.arrivaldeparturerecord" text="Arrival/Departure Record (I-94, I-94A)"/>
                  </label>
                  <div class="immigrationDocType hide" id="arrivaldeparturerecorddiv">
                     <div class="control-group">
                        <label class="control-label" for="arrivaldeparturerecordI94no">
                           <spring:message code="ssap.page12A.i94Number" text="I-94 Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.i94Number" text="I-94 Number"/>" type="text" id="arrivaldeparturerecordI94no" name="arrivaldeparturerecordI94no" data-parsley-pattern="^[a-zA-Z0-9]{11}$"  data-parsley-pattern-message="<spring:message code='ssap.error.I94NumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.I94NumRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="arrivaldeparturerecordSevisID">
                           <spring:message code="ssap.page12A.sevisIdNumber" text="SEVIS ID Number"/>
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.sevisIdNumber" text="SEVIS ID Number"/>" type="text" id="arrivaldeparturerecordSevisID" name="arrivaldeparturerecordSevisID" data-parsley-pattern="^[0-9]{10}$"  data-parsley-pattern-message="<spring:message code='ssap.error.SEVISIDValid'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="arrivaldeparturerecorddateOfExp">
                           <spring:message code="ssap.page12A.expDate" text="Passport Expiration Date"/>
                        </label>
                        <div class="controls">
                           <div data-date=""  class="input-append date date-picker">
                              <input type="text" class="input-medium dob-input" name="arrivaldeparturerecordPassportexpdate" id="arrivaldeparturerecorddateOfExp" data-parsley-trigger="change" data-parsley-errors-container="#arrivaldeparturerecorddateOfExpErrorDiv" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />"  data-parsley-pattern = "(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>"/>
                              <span class="add-on dateSpan" ><i class="icon-calendar"></i></span>
                           </div>
                           <div id="arrivaldeparturerecorddateOfExpErrorDiv"></div>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->               
                  </div>
                  
                  <label class="radio" for="arrivaldeprecordForeign">                     <input type="radio" id="arrivaldeprecordForeign" name="docType" value="arrivaldeprecordForeign"><spring:message code="ssap.page12A.arrivaldeprecordForeign" text="Arrival/Departure Record in Foreign Passport (I-94)"/>
                  </label>
                  <div class="immigrationDocType hide" id="arrivaldeprecordForeigndiv">
                     <div class="control-group">
                        <label class="control-label" for="arrivaldeprecordForeignI94no">
                           <spring:message code="ssap.page12A.i94Number" text="I-94 Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.i94Number" text="I-94 Number"/>" type="text" id="arrivaldeprecordForeignI94no" name="arrivaldeprecordForeignI94no" data-parsley-pattern="^[a-zA-Z0-9]{11}$"  data-parsley-pattern-message="<spring:message code='ssap.error.I94NumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.I94NumRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="arrivaldeprecordForeignppcardno">
                           <spring:message code="ssap.page12A.passportNumber" text="Passport Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.passportNumber" text="Passport Number"/>" type="text" id="arrivaldeprecordForeignppcardno" name="arrivaldeprecordForeignppcardno" data-parsley-pattern="^[a-zA-Z0-9]{6,12}$"  data-parsley-pattern-message="<spring:message code='ssap.error.passportNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.passportNumRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->                     
                     <div class="control-group">
                        <label class="control-label" for="arrivaldeprecordForeignCounty">
                           <spring:message code="ssap.page12A.foreignPassportCountryOfIssuance" text="Country of Issuance"/>
                           <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                           <span aria-label="Required!"></span>
                        </label>
                        <div class="controls">
                           <select name="arrivaldeprecordForeignCounty" id="arrivaldeprecordForeignCounty" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.passportCountryRequired' />">
                              <option value=""><spring:message code="ssap.County" text="Country" /></option>
                              <option value="AFG"><spring:message code="ssap.county.Afghanistan" text="Afghanistan" /></option>
                              <option value="ALB"><spring:message code="ssap.county.Albania" text="Albania" /></option>
                              <option value="DZA"><spring:message code="ssap.county.Algeria" text="Algeria" /></option>
                              <option value="ASM"><spring:message code="ssap.county.American Samoa" text="American Samoa" /></option>
                              <option value="AND"><spring:message code="ssap.county.Andorra" text="Andorra" /></option>
                              <option value="AGO"><spring:message code="ssap.county.Angola" text="Angola" /></option>
                              <option value="AIA"><spring:message code="ssap.county.Anguilla" text="Anguilla" /></option>
                              <option value="ATA"><spring:message code="ssap.county.Antarctica" text="Antarctica" /></option>
                              <option value="ATG"><spring:message code="ssap.county.Antigua And Barbuda" text="Antigua And Barbuda" /></option>
                              <option value="ARG"><spring:message code="ssap.county.Argentina" text="Argentina" /></option>
                              <option value="ARM"><spring:message code="ssap.county.Armenia" text="Armenia" /></option>
                              <option value="ABW"><spring:message code="ssap.county.Aruba" text="Aruba" /></option>
                              <option value="AUS"><spring:message code="ssap.county.Australia" text="Australia" /></option>
                              <option value="AUT"><spring:message code="ssap.county.Austria" text="Austria" /></option>
                              <option value="AZE"><spring:message code="ssap.county.Azerbaijan" text="Azerbaijan" /></option>
                              <option value="BHS"><spring:message code="ssap.county.Bahamas" text="Bahamas" /></option>
                              <option value="BHR"><spring:message code="ssap.county.Bahrain" text="Bahrain" /></option>
                              <option value="BGD"><spring:message code="ssap.county.Bangladesh" text="Bangladesh" /></option>
                              <option value="BRB"><spring:message code="ssap.county.Barbados" text="Barbados" /></option>
                              <option value="BLR"><spring:message code="ssap.county.Belarus" text="Belarus" /></option>
                              <option value="BEL"><spring:message code="ssap.county.Belgium" text="Belgium" /></option>
                              <option value="BLZ"><spring:message code="ssap.county.Belize" text="Belize" /></option>
                              <option value="BEN"><spring:message code="ssap.county.Benin" text="Benin" /></option>
                              <option value="BMU"><spring:message code="ssap.county.Bermuda" text="Bermuda" /></option>
                              <option value="BTN"><spring:message code="ssap.county.Bhutan" text="Bhutan" /></option>
                              <option value="BOL"><spring:message code="ssap.county.Bolivia, Plurinational State Of" text="Bolivia, Plurinational State Of" /></option>
                              <option value="BIH"><spring:message code="ssap.county.Bosnia And Herzegovina" text="Bosnia And Herzegovina" /></option>
                              <option value="BWA"><spring:message code="ssap.county.Botswana" text="Botswana" /></option>
                              <option value="BVT"><spring:message code="ssap.county.Bouvet Island" text="Bouvet Island" /></option>
                              <option value="BRA"><spring:message code="ssap.county.Brazil" text="Brazil" /></option>
                              <option value="IOT"><spring:message code="ssap.county.British Indian Ocean Territory" text="British Indian Ocean Territory" /></option>
                              <option value="BRN"><spring:message code="ssap.county.Brunei Darussalam" text="Brunei Darussalam" /></option>
                              <option value="BGR"><spring:message code="ssap.county.Bulgaria" text="Bulgaria" /></option>
                              <option value="BFA"><spring:message code="ssap.county.Burkina Faso" text="Burkina Faso" /></option>
                              <option value="BDI"><spring:message code="ssap.county.Burundi" text="Burundi" /></option>
                              <option value="KHM"><spring:message code="ssap.county.Cambodia" text="Cambodia" /></option>
                              <option value="CMR"><spring:message code="ssap.county.Cameroon" text="Cameroon" /></option>
                              <option value="CAN"><spring:message code="ssap.county.Canada" text="Canada" /></option>
                              <option value="CPV"><spring:message code="ssap.county.Cape Verde" text="Cape Verde" /></option>
                              <option value="CYM"><spring:message code="ssap.county.Cayman Islands" text="Cayman Islands" /></option>
                              <option value="CAF"><spring:message code="ssap.county.Central African Republic" text="Central African Republic" /></option>
                              <option value="CIV"><spring:message code="ssap.county.Cete D'Ivoire" text="Cete D'Ivoire" /></option>
                              <option value="TCD"><spring:message code="ssap.county.Chad" text="Chad" /></option>
                              <option value="CHL"><spring:message code="ssap.county.Chile" text="Chile" /></option>
                              <option value="CHN"><spring:message code="ssap.county.China" text="China" /></option>
                              <option value="CXR"><spring:message code="ssap.county.Christmas Island" text="Christmas Island" /></option>
                              <option value="CCK"><spring:message code="ssap.county.Cocos (Keeling) Islands" text="Cocos (Keeling) Islands" /></option>
                              <option value="COL"><spring:message code="ssap.county.Colombia" text="Colombia" /></option>
                              <option value="COM"><spring:message code="ssap.county.Comoros" text="Comoros" /></option>
                              <option value="COG"><spring:message code="ssap.county.Congo" text="Congo" /></option>
                              <option value="COD"><spring:message code="ssap.county.Congo, Democratic Republic Of The" text="Congo, Democratic Republic Of The" /></option>
                              <option value="COK"><spring:message code="ssap.county.Cook Islands" text="Cook Islands" /></option>
                              <option value="CRI"><spring:message code="ssap.county.Costa Rica" text="Costa Rica" /></option>
                              <option value="HRV"><spring:message code="ssap.county.Croatia" text="Croatia" /></option>
                              <option value="CUB"><spring:message code="ssap.county.Cuba" text="Cuba" /></option>
                              <option value="CYP"><spring:message code="ssap.county.Cyprus" text="Cyprus" /></option>
                              <option value="CZE"><spring:message code="ssap.county.Czech Republic" text="Czech Republic" /></option>
                              <option value="DNK"><spring:message code="ssap.county.Denmark" text="Denmark" /></option>
                              <option value="DJI"><spring:message code="ssap.county.Djibouti" text="Djibouti" /></option>
                              <option value="DMA"><spring:message code="ssap.county.Dominica" text="Dominica" /></option>
                              <option value="DOM"><spring:message code="ssap.county.Dominican Republic" text="Dominican Republic" /></option>
                              <option value="ECU"><spring:message code="ssap.county.Ecuador" text="Ecuador" /></option>
                              <option value="EGY"><spring:message code="ssap.county.Egypt" text="Egypt" /></option>
                              <option value="SLV"><spring:message code="ssap.county.El Salvador" text="El Salvador" /></option>
                              <option value="ALA"><spring:message code="ssap.county.Eland Islands" text="Eland Islands" /></option>
                              <option value="GNQ"><spring:message code="ssap.county.Equatorial Guinea" text="Equatorial Guinea" /></option>
                              <option value="ERI"><spring:message code="ssap.county.Eritrea" text="Eritrea" /></option>
                              <option value="EST"><spring:message code="ssap.county.Estonia" text="Estonia" /></option>
                              <option value="ETH"><spring:message code="ssap.county.Ethiopia" text="Ethiopia" /></option>
                              <option value="FLK"><spring:message code="ssap.county.Falkland Islands (Malvinas)" text="Falkland Islands (Malvinas)" /></option>
                              <option value="FRO"><spring:message code="ssap.county.Faroe Islands" text="Faroe Islands" /></option>
                              <option value="FJI"><spring:message code="ssap.county.Fiji" text="Fiji" /></option>
                              <option value="FIN"><spring:message code="ssap.county.Finland" text="Finland" /></option>
                              <option value="FRA"><spring:message code="ssap.county.France" text="France" /></option>
                              <option value="GUF"><spring:message code="ssap.county.French Guiana" text="French Guiana" /></option>
                              <option value="PYF"><spring:message code="ssap.county.French Polynesia" text="French Polynesia" /></option>
                              <option value="ATF"><spring:message code="ssap.county.French Southern Territories" text="French Southern Territories" /></option>
                              <option value="GAB"><spring:message code="ssap.county.Gabon" text="Gabon" /></option>
                              <option value="GMB"><spring:message code="ssap.county.Gambia" text="Gambia" /></option>
                              <option value="GEO"><spring:message code="ssap.county.Georgia" text="Georgia" /></option>
                              <option value="DEU"><spring:message code="ssap.county.Germany" text="Germany" /></option>
                              <option value="GHA"><spring:message code="ssap.county.Ghana" text="Ghana" /></option>
                              <option value="GIB"><spring:message code="ssap.county.Gibraltar" text="Gibraltar" /></option>
                              <option value="GRC"><spring:message code="ssap.county.Greece" text="Greece" /></option>
                              <option value="GRL"><spring:message code="ssap.county.Greenland" text="Greenland" /></option>
                              <option value="GRD"><spring:message code="ssap.county.Grenada" text="Grenada" /></option>
                              <option value="GLP"><spring:message code="ssap.county.Guadeloupe" text="Guadeloupe" /></option>
                              <option value="GUM"><spring:message code="ssap.county.Guam" text="Guam" /></option>
                              <option value="GTM"><spring:message code="ssap.county.Guatemala" text="Guatemala" /></option>
                              <option value="GGY"><spring:message code="ssap.county.Guernsey" text="Guernsey" /></option>
                              <option value="GIN"><spring:message code="ssap.county.Guinea" text="Guinea" /></option>
                              <option value="GNB"><spring:message code="ssap.county.Guinea-Bissau" text="Guinea-Bissau" /></option>
                              <option value="GUY"><spring:message code="ssap.county.Guyana" text="Guyana" /></option>
                              <option value="HTI"><spring:message code="ssap.county.Haiti" text="Haiti" /></option>
                              <option value="HMD"><spring:message code="ssap.county.Heard Island And Mcdonald Islands" text="Heard Island And Mcdonald Islands" /></option>
                              <option value="VAT"><spring:message code="ssap.county.Holy See (Vatican City State)" text="Holy See (Vatican City State)" /></option>
                              <option value="HND"><spring:message code="ssap.county.Honduras" text="Honduras" /></option>
                              <option value="HKG"><spring:message code="ssap.county.Hong Kong" text="Hong Kong" /></option>
                              <option value="HUN"><spring:message code="ssap.county.Hungary" text="Hungary" /></option>
                              <option value="ISL"><spring:message code="ssap.county.Iceland" text="Iceland" /></option>
                              <option value="IND"><spring:message code="ssap.county.India" text="India" /></option>
                              <option value="IDN"><spring:message code="ssap.county.Indonesia" text="Indonesia" /></option>
                              <option value="IRN"><spring:message code="ssap.county.Iran, Islamic Republic Of" text="Iran, Islamic Republic Of" /></option>
                              <option value="IRQ"><spring:message code="ssap.county.Iraq" text="Iraq" /></option>
                              <option value="IRL"><spring:message code="ssap.county.Ireland" text="Ireland" /></option>
                              <option value="IMN"><spring:message code="ssap.county.Isle Of Man" text="Isle Of Man" /></option>
                              <option value="ISR"><spring:message code="ssap.county.Israel" text="Israel" /></option>
                              <option value="ITA"><spring:message code="ssap.county.Italy" text="Italy" /></option>
                              <option value="JAM"><spring:message code="ssap.county.Jamaica" text="Jamaica" /></option>
                              <option value="JPN"><spring:message code="ssap.county.Japan" text="Japan" /></option>
                              <option value="JEY"><spring:message code="ssap.county.Jersey" text="Jersey" /></option>
                              <option value="JOR"><spring:message code="ssap.county.Jordan" text="Jordan" /></option>
                              <option value="KAZ"><spring:message code="ssap.county.Kazakhstan" text="Kazakhstan" /></option>
                              <option value="KEN"><spring:message code="ssap.county.Kenya" text="Kenya" /></option>
                              <option value="KIR"><spring:message code="ssap.county.Kiribati" text="Kiribati" /></option>
                              <option value="PRK"><spring:message code="ssap.county.Korea, Democratic People'S Republic Of" text="Korea, Democratic People'S Republic Of" /></option>
                              <option value="KOR"><spring:message code="ssap.county.Korea, Republic Of" text="Korea, Republic Of" /></option>
                              <option value="KVO"><spring:message code="ssap.county.Kosovo" text="Kosovo" /></option>
                              <option value="KWT"><spring:message code="ssap.county.Kuwait" text="Kuwait" /></option>
                              <option value="KGZ"><spring:message code="ssap.county.Kyrgyzstan" text="Kyrgyzstan" /></option>
                              <option value="LAO"><spring:message code="ssap.county.Lao People'S Democratic Republic" text="Lao People'S Democratic Republic" /></option>
                              <option value="LVA"><spring:message code="ssap.county.Latvia" text="Latvia" /></option>
                              <option value="LBN"><spring:message code="ssap.county.Lebanon" text="Lebanon" /></option>
                              <option value="LSO"><spring:message code="ssap.county.Lesotho" text="Lesotho" /></option>
                              <option value="LBR"><spring:message code="ssap.county.Liberia" text="Liberia" /></option>
                              <option value="LBY"><spring:message code="ssap.county.Libyan Arab Jamahiriya" text="Libyan Arab Jamahiriya" /></option>
                              <option value="LIE"><spring:message code="ssap.county.Liechtenstein" text="Liechtenstein" /></option>
                              <option value="LTU"><spring:message code="ssap.county.Lithuania" text="Lithuania" /></option>
                              <option value="LUX"><spring:message code="ssap.county.Luxembourg" text="Luxembourg" /></option>
                              <option value="MAC"><spring:message code="ssap.county.Macao" text="Macao" /></option>
                              <option value="MKD"><spring:message code="ssap.county.Macedonia, The Former Yugoslav Republic Of" text="Macedonia, The Former Yugoslav Republic Of" /></option>
                              <option value="MDG"><spring:message code="ssap.county.Madagascar" text="Madagascar" /></option>
                              <option value="MWI"><spring:message code="ssap.county.Malawi" text="Malawi" /></option>
                              <option value="MYS"><spring:message code="ssap.county.Malaysia" text="Malaysia" /></option>
                              <option value="MDV"><spring:message code="ssap.county.Maldives" text="Maldives" /></option>
                              <option value="MLI"><spring:message code="ssap.county.Mali" text="Mali" /></option>
                              <option value="MLT"><spring:message code="ssap.county.Malta" text="Malta" /></option>
                              <option value="MHL"><spring:message code="ssap.county.Marshall Islands" text="Marshall Islands" /></option>
                              <option value="MTQ"><spring:message code="ssap.county.Martinique" text="Martinique" /></option>
                              <option value="MRT"><spring:message code="ssap.county.Mauritania" text="Mauritania" /></option>
                              <option value="MUS"><spring:message code="ssap.county.Mauritius" text="Mauritius" /></option>
                              <option value="MYT"><spring:message code="ssap.county.Mayotte" text="Mayotte" /></option>
                              <option value="MEX"><spring:message code="ssap.county.Mexico" text="Mexico" /></option>
                              <option value="FSM"><spring:message code="ssap.county.Micronesia, Federated States Of" text="Micronesia, Federated States Of" /></option>
                              <option value="MDA"><spring:message code="ssap.county.Moldova, Republic Of" text="Moldova, Republic Of" /></option>
                              <option value="MCO"><spring:message code="ssap.county.Monaco" text="Monaco" /></option>
                              <option value="MNG"><spring:message code="ssap.county.Mongolia" text="Mongolia" /></option>
                              <option value="MNE"><spring:message code="ssap.county.Montenegro" text="Montenegro" /></option>
                              <option value="MSR"><spring:message code="ssap.county.Montserrat" text="Montserrat" /></option>
                              <option value="MAR"><spring:message code="ssap.county.Morocco" text="Morocco" /></option>
                              <option value="MOZ"><spring:message code="ssap.county.Mozambique" text="Mozambique" /></option>
                              <option value="MMR"><spring:message code="ssap.county.Myanmar" text="Myanmar" /></option>
                              <option value="NAM"><spring:message code="ssap.county.Namibia" text="Namibia" /></option>
                              <option value="NRU"><spring:message code="ssap.county.Nauru" text="Nauru" /></option>
                              <option value="NPL"><spring:message code="ssap.county.Nepal" text="Nepal" /></option>
                              <option value="NLD"><spring:message code="ssap.county.Netherlands" text="Netherlands" /></option>
                              <option value="ANT"><spring:message code="ssap.county.Netherlands Antilles" text="Netherlands Antilles" /></option>
                              <option value="NCL"><spring:message code="ssap.county.New Caledonia" text="New Caledonia" /></option>
                              <option value="NZL"><spring:message code="ssap.county.New Zealand" text="New Zealand" /></option>
                              <option value="NIC"><spring:message code="ssap.county.Nicaragua" text="Nicaragua" /></option>
                              <option value="NER"><spring:message code="ssap.county.Niger" text="Niger" /></option>
                              <option value="NGA"><spring:message code="ssap.county.Nigeria" text="Nigeria" /></option>
                              <option value="NIU"><spring:message code="ssap.county.Niue" text="Niue" /></option>
                              <option value="NFK"><spring:message code="ssap.county.Norfolk Island" text="Norfolk Island" /></option>
                              <option value="MNP"><spring:message code="ssap.county.Northern Mariana Islands" text="Northern Mariana Islands" /></option>
                              <option value="NOR"><spring:message code="ssap.county.Norway" text="Norway" /></option>
                              <option value="OMN"><spring:message code="ssap.county.Oman" text="Oman" /></option>
                              <option value="PAK"><spring:message code="ssap.county.Pakistan" text="Pakistan" /></option>
                              <option value="PLW"><spring:message code="ssap.county.Palau" text="Palau" /></option>
                              <option value="PSE"><spring:message code="ssap.county.Palestinian Territory, Occupied" text="Palestinian Territory, Occupied" /></option>
                              <option value="PAN"><spring:message code="ssap.county.Panama" text="Panama" /></option>
                              <option value="PNG"><spring:message code="ssap.county.Papua New Guinea" text="Papua New Guinea" /></option>
                              <option value="PRY"><spring:message code="ssap.county.Paraguay" text="Paraguay" /></option>
                              <option value="PER"><spring:message code="ssap.county.Peru" text="Peru" /></option>
                              <option value="PHL"><spring:message code="ssap.county.Philippines" text="Philippines" /></option>
                              <option value="PCN"><spring:message code="ssap.county.Pitcairn" text="Pitcairn" /></option>
                              <option value="POL"><spring:message code="ssap.county.Poland" text="Poland" /></option>
                              <option value="PRT"><spring:message code="ssap.county.Portugal" text="Portugal" /></option>
                              <option value="PRI"><spring:message code="ssap.county.Puerto Rico" text="Puerto Rico" /></option>
                              <option value="QAT"><spring:message code="ssap.county.Qatar" text="Qatar" /></option>
                              <option value="REU"><spring:message code="ssap.county.Reunion" text="Reunion" /></option>
                              <option value="ROU"><spring:message code="ssap.county.Romania" text="Romania" /></option>
                              <option value="RUS"><spring:message code="ssap.county.Russian Federation" text="Russian Federation" /></option>
                              <option value="RWA"><spring:message code="ssap.county.Rwanda" text="Rwanda" /></option>
                              <option value="BLM"><spring:message code="ssap.county.Saint Barthelemy" text="Saint Barthelemy" /></option>
                              <option value="SHN"><spring:message code="ssap.county.Saint Helena, Ascension And Tristan Da Cunha" text="Saint Helena, Ascension And Tristan Da Cunha" /></option>
                              <option value="KNA"><spring:message code="ssap.county.Saint Kitts And Nevis" text="Saint Kitts And Nevis" /></option>
                              <option value="LCA"><spring:message code="ssap.county.Saint Lucia" text="Saint Lucia" /></option>
                              <option value="MAF"><spring:message code="ssap.county.Saint Martin (French Part)" text="Saint Martin (French Part)" /></option>
                              <option value="SPM"><spring:message code="ssap.county.Saint Pierre And Miquelon" text="Saint Pierre And Miquelon" /></option>
                              <option value="VCT"><spring:message code="ssap.county.Saint Vincent And The Grenadines" text="Saint Vincent And The Grenadines" /></option>
                              <option value="WSM"><spring:message code="ssap.county.Samoa" text="Samoa" /></option>
                              <option value="SMR"><spring:message code="ssap.county.San Marino" text="San Marino" /></option>
                              <option value="STP"><spring:message code="ssap.county.Sao Tome And Principe" text="Sao Tome And Principe" /></option>
                              <option value="SAU"><spring:message code="ssap.county.Saudi Arabia" text="Saudi Arabia" /></option>
                              <option value="SEN"><spring:message code="ssap.county.Senegal" text="Senegal" /></option>
                              <option value="SRB"><spring:message code="ssap.county.Serbia" text="Serbia" /></option>
                              <option value="SYC"><spring:message code="ssap.county.Seychelles" text="Seychelles" /></option>
                              <option value="SLE"><spring:message code="ssap.county.Sierra Leone" text="Sierra Leone" /></option>
                              <option value="SGP"><spring:message code="ssap.county.Singapore" text="Singapore" /></option>
                              <option value="SVK"><spring:message code="ssap.county.Slovakia" text="Slovakia" /></option>
                              <option value="SVN"><spring:message code="ssap.county.Slovenia" text="Slovenia" /></option>
                              <option value="SLB"><spring:message code="ssap.county.Solomon Islands" text="Solomon Islands" /></option>
                              <option value="SOM"><spring:message code="ssap.county.Somalia" text="Somalia" /></option>
                              <option value="ZAF"><spring:message code="ssap.county.South Africa" text="South Africa" /></option>
                              <option value="SGS"><spring:message code="ssap.county.South Georgia And The South Sandwich Islands" text="South Georgia And The South Sandwich Islands" /></option>
                              <option value="SSD"><spring:message code="ssap.county.South Sudan" text="South Sudan" /></option>
                              <option value="ESP"><spring:message code="ssap.county.Spain" text="Spain" /></option>
                              <option value="LKA"><spring:message code="ssap.county.Sri Lanka" text="Sri Lanka" /></option>
                              <option value="STL"><spring:message code="ssap.county.Stateless" text="Stateless" /></option>
                              <option value="SDN"><spring:message code="ssap.county.Sudan" text="Sudan" /></option>
                              <option value="SUR"><spring:message code="ssap.county.Suriname" text="Suriname" /></option>
                              <option value="SJM"><spring:message code="ssap.county.Svalbard And Jan Mayen" text="Svalbard And Jan Mayen" /></option>
                              <option value="SWZ"><spring:message code="ssap.county.Swaziland" text="Swaziland" /></option>
                              <option value="SWE"><spring:message code="ssap.county.Sweden" text="Sweden" /></option>
                              <option value="CHE"><spring:message code="ssap.county.Switzerland" text="Switzerland" /></option>
                              <option value="SYR"><spring:message code="ssap.county.Syrian Arab Republic" text="Syrian Arab Republic" /></option>
                              <option value="TWN"><spring:message code="ssap.county.Taiwan, Province Of China" text="Taiwan, Province Of China" /></option>
                              <option value="TJK"><spring:message code="ssap.county.Tajikistan" text="Tajikistan" /></option>
                              <option value="TZA"><spring:message code="ssap.county.Tanzania, United Republic Of" text="Tanzania, United Republic Of" /></option>
                              <option value="THA"><spring:message code="ssap.county.Thailand" text="Thailand" /></option>
                              <option value="TLS"><spring:message code="ssap.county.Timor-Leste" text="Timor-Leste" /></option>
                              <option value="TGO"><spring:message code="ssap.county.Togo" text="Togo" /></option>
                              <option value="TKL"><spring:message code="ssap.county.Tokelau" text="Tokelau" /></option>
                              <option value="TON"><spring:message code="ssap.county.Tonga" text="Tonga" /></option>
                              <option value="TTO"><spring:message code="ssap.county.Trinidad And Tobago" text="Trinidad And Tobago" /></option>
                              <option value="TUN"><spring:message code="ssap.county.Tunisia" text="Tunisia" /></option>
                              <option value="TUR"><spring:message code="ssap.county.Turkey" text="Turkey" /></option>
                              <option value="TKM"><spring:message code="ssap.county.Turkmenistan" text="Turkmenistan" /></option>
                              <option value="TCA"><spring:message code="ssap.county.Turks And Caicos Islands" text="Turks And Caicos Islands" /></option>
                              <option value="TUV"><spring:message code="ssap.county.Tuvalu" text="Tuvalu" /></option>
                              <option value="UGA"><spring:message code="ssap.county.Uganda" text="Uganda" /></option>
                              <option value="UKR"><spring:message code="ssap.county.Ukraine" text="Ukraine" /></option>
                              <option value="ARE"><spring:message code="ssap.county.United Arab Emirates" text="United Arab Emirates" /></option>
                              <option value="GBR"><spring:message code="ssap.county.United Kingdom" text="United Kingdom" /></option>
                              <option value="USA"><spring:message code="ssap.county.United States" text="United States" /></option>
                              <option value="UMI"><spring:message code="ssap.county.United States Minor Outlying Islands" text="United States Minor Outlying Islands" /></option>
                              <option value="URY"><spring:message code="ssap.county.Uruguay" text="Uruguay" /></option>
                              <option value="UZB"><spring:message code="ssap.county.Uzbekistan" text="Uzbekistan" /></option>
                              <option value="VUT"><spring:message code="ssap.county.Vanuatu" text="Vanuatu" /></option>
                              <option value="VEN"><spring:message code="ssap.county.Venezuela, Bolivarian Republic Of" text="Venezuela, Bolivarian Republic Of" /></option>
                              <option value="VNM"><spring:message code="ssap.county.Viet Nam" text="Viet Nam" /></option>
                              <option value="VGB"><spring:message code="ssap.county.Virgin Islands (British)" text="Virgin Islands (British)" /></option>
                              <option value="VIR"><spring:message code="ssap.county.Virgin Islands (U.S.)" text="Virgin Islands (U.S.)" /></option>
                              <option value="WLF"><spring:message code="ssap.county.Wallis And Futuna" text="Wallis And Futuna" /></option>
                              <option value="ESH"><spring:message code="ssap.county.Western Sahara *" text="Western Sahara *" /></option>
                              <option value="YEM"><spring:message code="ssap.county.Yemen" text="Yemen" /></option>
                              <option value="ZMB"><spring:message code="ssap.county.Zambia" text="Zambia" /></option>
                              <option value="ZWE"><spring:message code="ssap.county.Zimbabwe" text="Zimbabwe" /></option>
                           </select>
                           <span class="errorMessage"></span>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label" for="arrivaldeprecordForeigndateOfExp">
                           <spring:message code="ssap.page12A.expDate" text="Passport Expiration Date"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <div data-date="" class="input-append date date-picker">
                              <input type="text" class="input-medium dob-input" name="arrivaldeprecordForeigndateOfExp" id="arrivaldeprecordForeigndateOfExp" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.passportExpDateRequired' />"  data-parsley-errors-container="#arrivaldeprecordForeigndateOfExpErrorDiv" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-pattern = "(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" />
                              <span class="add-on dateSpan" ><i class="icon-calendar"></i></span>
                           </div>
                           <div id="arrivaldeprecordForeigndateOfExpErrorDiv"></div>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="arrivaldeprecordForeignppvisano">
                           <spring:message code="ssap.page12A.visaNumber" text="Visa Number"/>
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.visaNumber" text="Visa Number"/>" type="text" id="arrivaldeprecordForeignppvisano" name="arrivaldeprecordForeignppvisano" data-parsley-pattern="^[a-zA-Z0-9]{8}$"  data-parsley-pattern-message="<spring:message code='ssap.error.visaNumValid'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->         
                     <div class="control-group">
                        <label class="control-label" for="arrivaldeprecordForeignSevisID">
                           <spring:message code="ssap.page12A.sevisIdNumber" text="SEVIS ID Number"/>
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.sevisIdNumber" text="SEVIS ID Number"/>" type="text" id="arrivaldeprecordForeignSevisID" name="arrivaldeprecordForeignSevisID" data-parsley-pattern="^[0-9]{10}$"  data-parsley-pattern-message="<spring:message code='ssap.error.SEVISIDValid'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->         
                  </div>
                  
                 <%--  <label class="radio" for="NoticeOfAction">
                     <input id="NoticeOfAction" type="radio" name="docType" value="1"><spring:message code="ssap.page12A.noticeOfActionI" text="Notice of Action (I"/>&minus;<spring:message code="ssap.page12A.num797" text="797)"/>
                  </label>          
                  <div class="immigrationDocType hide" id="NoticeOfActiondiv">
                     <div class="control-group">
                        <label class="control-label" for="NoticeOfActionAlignNumber">
                           <spring:message code="ssap.page12A.alignNumber" text="Alien Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.placeholder.alignNum" text="Alien Number"/>" type="text" id="NoticeOfActionAlignNumber" name="NoticeOfActionAlignNumber" data-parsley-pattern="^[0-9]{7,9}$"  data-parsley-pattern-message="<spring:message code='ssap.error.alienNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.alienNumRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="NoticeOfActionI94">
                           <spring:message code="ssap.page12A.i94Number" text="I-94 Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.i94Number" text="I-94 Number"/>" type="text" id="NoticeOfActionI94" name="NoticeOfActionI94" data-parsley-pattern="^[a-zA-Z0-9]{11}$"  data-parsley-pattern-message="<spring:message code='ssap.error.I94NumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.I94NumRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->               
                  </div> --%>
                  
                  <label class="radio" for="ForeignppI94">                       
                     <input type="radio" id="ForeignppI94" name="docType" value="ForeignppI94"><spring:message code="ssap.page12A.arrivaldeprecordForeign" text="Foreign Passport"/>
                  </label>
                  <div class="immigrationDocType hide" id="ForeignppI94div">
                     <div class="control-group">
                        <label class="control-label" for="ForeignppI94AlignNumber">
                           <spring:message code="ssap.page12A.i94Number" text="I-94 Number"/>
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.i94Number" text="I-94 Number"/>" type="text" id="ForeignppI94AlignNumber" name="ForeignppI94" data-parsley-pattern="^[a-zA-Z0-9]{11}$"  data-parsley-pattern-message="<spring:message code='ssap.error.I94NumValid'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="ForeignppI94ppcardno">
                           <spring:message code="ssap.page12A.passportNumber" text="Passport Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.passportNumber" text="Passport Number"/>" type="text" id="ForeignppI94ppcardno" name="ForeignppI94ppcardno" data-parsley-pattern="^[a-zA-Z0-9]{6,12}$"  data-parsley-pattern-message="<spring:message code='ssap.error.passportNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.passportNumRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->                     
                     <div class="control-group">
                        <label class="control-label" for="ForeignppI94County">
                           <spring:message code="ssap.page12A.foreignPassportCountryOfIssuance" text="Country of Issuance"/>
                           <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                           <span aria-label="Required!"></span>
                        </label>
                        <div class="controls required-input-field-container">
                           <select name="ForeignppI94County" id="ForeignppI94County" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.passportCountryRequired' />"  >
                              <option value=""><spring:message code="ssap.County" text="Country" /></option>
                              <option value="AFG"><spring:message code="ssap.county.Afghanistan" text="Afghanistan" /></option>
                              <option value="ALB"><spring:message code="ssap.county.Albania" text="Albania" /></option>
                              <option value="DZA"><spring:message code="ssap.county.Algeria" text="Algeria" /></option>
                              <option value="ASM"><spring:message code="ssap.county.American Samoa" text="American Samoa" /></option>
                              <option value="AND"><spring:message code="ssap.county.Andorra" text="Andorra" /></option>
                              <option value="AGO"><spring:message code="ssap.county.Angola" text="Angola" /></option>
                              <option value="AIA"><spring:message code="ssap.county.Anguilla" text="Anguilla" /></option>
                              <option value="ATA"><spring:message code="ssap.county.Antarctica" text="Antarctica" /></option>
                              <option value="ATG"><spring:message code="ssap.county.Antigua And Barbuda" text="Antigua And Barbuda" /></option>
                              <option value="ARG"><spring:message code="ssap.county.Argentina" text="Argentina" /></option>
                              <option value="ARM"><spring:message code="ssap.county.Armenia" text="Armenia" /></option>
                              <option value="ABW"><spring:message code="ssap.county.Aruba" text="Aruba" /></option>
                              <option value="AUS"><spring:message code="ssap.county.Australia" text="Australia" /></option>
                              <option value="AUT"><spring:message code="ssap.county.Austria" text="Austria" /></option>
                              <option value="AZE"><spring:message code="ssap.county.Azerbaijan" text="Azerbaijan" /></option>
                              <option value="BHS"><spring:message code="ssap.county.Bahamas" text="Bahamas" /></option>
                              <option value="BHR"><spring:message code="ssap.county.Bahrain" text="Bahrain" /></option>
                              <option value="BGD"><spring:message code="ssap.county.Bangladesh" text="Bangladesh" /></option>
                              <option value="BRB"><spring:message code="ssap.county.Barbados" text="Barbados" /></option>
                              <option value="BLR"><spring:message code="ssap.county.Belarus" text="Belarus" /></option>
                              <option value="BEL"><spring:message code="ssap.county.Belgium" text="Belgium" /></option>
                              <option value="BLZ"><spring:message code="ssap.county.Belize" text="Belize" /></option>
                              <option value="BEN"><spring:message code="ssap.county.Benin" text="Benin" /></option>
                              <option value="BMU"><spring:message code="ssap.county.Bermuda" text="Bermuda" /></option>
                              <option value="BTN"><spring:message code="ssap.county.Bhutan" text="Bhutan" /></option>
                              <option value="BOL"><spring:message code="ssap.county.Bolivia, Plurinational State Of" text="Bolivia, Plurinational State Of" /></option>
                              <option value="BIH"><spring:message code="ssap.county.Bosnia And Herzegovina" text="Bosnia And Herzegovina" /></option>
                              <option value="BWA"><spring:message code="ssap.county.Botswana" text="Botswana" /></option>
                              <option value="BVT"><spring:message code="ssap.county.Bouvet Island" text="Bouvet Island" /></option>
                              <option value="BRA"><spring:message code="ssap.county.Brazil" text="Brazil" /></option>
                              <option value="IOT"><spring:message code="ssap.county.British Indian Ocean Territory" text="British Indian Ocean Territory" /></option>
                              <option value="BRN"><spring:message code="ssap.county.Brunei Darussalam" text="Brunei Darussalam" /></option>
                              <option value="BGR"><spring:message code="ssap.county.Bulgaria" text="Bulgaria" /></option>
                              <option value="BFA"><spring:message code="ssap.county.Burkina Faso" text="Burkina Faso" /></option>
                              <option value="BDI"><spring:message code="ssap.county.Burundi" text="Burundi" /></option>
                              <option value="KHM"><spring:message code="ssap.county.Cambodia" text="Cambodia" /></option>
                              <option value="CMR"><spring:message code="ssap.county.Cameroon" text="Cameroon" /></option>
                              <option value="CAN"><spring:message code="ssap.county.Canada" text="Canada" /></option>
                              <option value="CPV"><spring:message code="ssap.county.Cape Verde" text="Cape Verde" /></option>
                              <option value="CYM"><spring:message code="ssap.county.Cayman Islands" text="Cayman Islands" /></option>
                              <option value="CAF"><spring:message code="ssap.county.Central African Republic" text="Central African Republic" /></option>
                              <option value="CIV"><spring:message code="ssap.county.Cete D'Ivoire" text="Cete D'Ivoire" /></option>
                              <option value="TCD"><spring:message code="ssap.county.Chad" text="Chad" /></option>
                              <option value="CHL"><spring:message code="ssap.county.Chile" text="Chile" /></option>
                              <option value="CHN"><spring:message code="ssap.county.China" text="China" /></option>
                              <option value="CXR"><spring:message code="ssap.county.Christmas Island" text="Christmas Island" /></option>
                              <option value="CCK"><spring:message code="ssap.county.Cocos (Keeling) Islands" text="Cocos (Keeling) Islands" /></option>
                              <option value="COL"><spring:message code="ssap.county.Colombia" text="Colombia" /></option>
                              <option value="COM"><spring:message code="ssap.county.Comoros" text="Comoros" /></option>
                              <option value="COG"><spring:message code="ssap.county.Congo" text="Congo" /></option>
                              <option value="COD"><spring:message code="ssap.county.Congo, Democratic Republic Of The" text="Congo, Democratic Republic Of The" /></option>
                              <option value="COK"><spring:message code="ssap.county.Cook Islands" text="Cook Islands" /></option>
                              <option value="CRI"><spring:message code="ssap.county.Costa Rica" text="Costa Rica" /></option>
                              <option value="HRV"><spring:message code="ssap.county.Croatia" text="Croatia" /></option>
                              <option value="CUB"><spring:message code="ssap.county.Cuba" text="Cuba" /></option>
                              <option value="CYP"><spring:message code="ssap.county.Cyprus" text="Cyprus" /></option>
                              <option value="CZE"><spring:message code="ssap.county.Czech Republic" text="Czech Republic" /></option>
                              <option value="DNK"><spring:message code="ssap.county.Denmark" text="Denmark" /></option>
                              <option value="DJI"><spring:message code="ssap.county.Djibouti" text="Djibouti" /></option>
                              <option value="DMA"><spring:message code="ssap.county.Dominica" text="Dominica" /></option>
                              <option value="DOM"><spring:message code="ssap.county.Dominican Republic" text="Dominican Republic" /></option>
                              <option value="ECU"><spring:message code="ssap.county.Ecuador" text="Ecuador" /></option>
                              <option value="EGY"><spring:message code="ssap.county.Egypt" text="Egypt" /></option>
                              <option value="SLV"><spring:message code="ssap.county.El Salvador" text="El Salvador" /></option>
                              <option value="ALA"><spring:message code="ssap.county.Eland Islands" text="Eland Islands" /></option>
                              <option value="GNQ"><spring:message code="ssap.county.Equatorial Guinea" text="Equatorial Guinea" /></option>
                              <option value="ERI"><spring:message code="ssap.county.Eritrea" text="Eritrea" /></option>
                              <option value="EST"><spring:message code="ssap.county.Estonia" text="Estonia" /></option>
                              <option value="ETH"><spring:message code="ssap.county.Ethiopia" text="Ethiopia" /></option>
                              <option value="FLK"><spring:message code="ssap.county.Falkland Islands (Malvinas)" text="Falkland Islands (Malvinas)" /></option>
                              <option value="FRO"><spring:message code="ssap.county.Faroe Islands" text="Faroe Islands" /></option>
                              <option value="FJI"><spring:message code="ssap.county.Fiji" text="Fiji" /></option>
                              <option value="FIN"><spring:message code="ssap.county.Finland" text="Finland" /></option>
                              <option value="FRA"><spring:message code="ssap.county.France" text="France" /></option>
                              <option value="GUF"><spring:message code="ssap.county.French Guiana" text="French Guiana" /></option>
                              <option value="PYF"><spring:message code="ssap.county.French Polynesia" text="French Polynesia" /></option>
                              <option value="ATF"><spring:message code="ssap.county.French Southern Territories" text="French Southern Territories" /></option>
                              <option value="GAB"><spring:message code="ssap.county.Gabon" text="Gabon" /></option>
                              <option value="GMB"><spring:message code="ssap.county.Gambia" text="Gambia" /></option>
                              <option value="GEO"><spring:message code="ssap.county.Georgia" text="Georgia" /></option>
                              <option value="DEU"><spring:message code="ssap.county.Germany" text="Germany" /></option>
                              <option value="GHA"><spring:message code="ssap.county.Ghana" text="Ghana" /></option>
                              <option value="GIB"><spring:message code="ssap.county.Gibraltar" text="Gibraltar" /></option>
                              <option value="GRC"><spring:message code="ssap.county.Greece" text="Greece" /></option>
                              <option value="GRL"><spring:message code="ssap.county.Greenland" text="Greenland" /></option>
                              <option value="GRD"><spring:message code="ssap.county.Grenada" text="Grenada" /></option>
                              <option value="GLP"><spring:message code="ssap.county.Guadeloupe" text="Guadeloupe" /></option>
                              <option value="GUM"><spring:message code="ssap.county.Guam" text="Guam" /></option>
                              <option value="GTM"><spring:message code="ssap.county.Guatemala" text="Guatemala" /></option>
                              <option value="GGY"><spring:message code="ssap.county.Guernsey" text="Guernsey" /></option>
                              <option value="GIN"><spring:message code="ssap.county.Guinea" text="Guinea" /></option>
                              <option value="GNB"><spring:message code="ssap.county.Guinea-Bissau" text="Guinea-Bissau" /></option>
                              <option value="GUY"><spring:message code="ssap.county.Guyana" text="Guyana" /></option>
                              <option value="HTI"><spring:message code="ssap.county.Haiti" text="Haiti" /></option>
                              <option value="HMD"><spring:message code="ssap.county.Heard Island And Mcdonald Islands" text="Heard Island And Mcdonald Islands" /></option>
                              <option value="VAT"><spring:message code="ssap.county.Holy See (Vatican City State)" text="Holy See (Vatican City State)" /></option>
                              <option value="HND"><spring:message code="ssap.county.Honduras" text="Honduras" /></option>
                              <option value="HKG"><spring:message code="ssap.county.Hong Kong" text="Hong Kong" /></option>
                              <option value="HUN"><spring:message code="ssap.county.Hungary" text="Hungary" /></option>
                              <option value="ISL"><spring:message code="ssap.county.Iceland" text="Iceland" /></option>
                              <option value="IND"><spring:message code="ssap.county.India" text="India" /></option>
                              <option value="IDN"><spring:message code="ssap.county.Indonesia" text="Indonesia" /></option>
                              <option value="IRN"><spring:message code="ssap.county.Iran, Islamic Republic Of" text="Iran, Islamic Republic Of" /></option>
                              <option value="IRQ"><spring:message code="ssap.county.Iraq" text="Iraq" /></option>
                              <option value="IRL"><spring:message code="ssap.county.Ireland" text="Ireland" /></option>
                              <option value="IMN"><spring:message code="ssap.county.Isle Of Man" text="Isle Of Man" /></option>
                              <option value="ISR"><spring:message code="ssap.county.Israel" text="Israel" /></option>
                              <option value="ITA"><spring:message code="ssap.county.Italy" text="Italy" /></option>
                              <option value="JAM"><spring:message code="ssap.county.Jamaica" text="Jamaica" /></option>
                              <option value="JPN"><spring:message code="ssap.county.Japan" text="Japan" /></option>
                              <option value="JEY"><spring:message code="ssap.county.Jersey" text="Jersey" /></option>
                              <option value="JOR"><spring:message code="ssap.county.Jordan" text="Jordan" /></option>
                              <option value="KAZ"><spring:message code="ssap.county.Kazakhstan" text="Kazakhstan" /></option>
                              <option value="KEN"><spring:message code="ssap.county.Kenya" text="Kenya" /></option>
                              <option value="KIR"><spring:message code="ssap.county.Kiribati" text="Kiribati" /></option>
                              <option value="PRK"><spring:message code="ssap.county.Korea, Democratic People'S Republic Of" text="Korea, Democratic People'S Republic Of" /></option>
                              <option value="KOR"><spring:message code="ssap.county.Korea, Republic Of" text="Korea, Republic Of" /></option>
                              <option value="KVO"><spring:message code="ssap.county.Kosovo" text="Kosovo" /></option>
                              <option value="KWT"><spring:message code="ssap.county.Kuwait" text="Kuwait" /></option>
                              <option value="KGZ"><spring:message code="ssap.county.Kyrgyzstan" text="Kyrgyzstan" /></option>
                              <option value="LAO"><spring:message code="ssap.county.Lao People'S Democratic Republic" text="Lao People'S Democratic Republic" /></option>
                              <option value="LVA"><spring:message code="ssap.county.Latvia" text="Latvia" /></option>
                              <option value="LBN"><spring:message code="ssap.county.Lebanon" text="Lebanon" /></option>
                              <option value="LSO"><spring:message code="ssap.county.Lesotho" text="Lesotho" /></option>
                              <option value="LBR"><spring:message code="ssap.county.Liberia" text="Liberia" /></option>
                              <option value="LBY"><spring:message code="ssap.county.Libyan Arab Jamahiriya" text="Libyan Arab Jamahiriya" /></option>
                              <option value="LIE"><spring:message code="ssap.county.Liechtenstein" text="Liechtenstein" /></option>
                              <option value="LTU"><spring:message code="ssap.county.Lithuania" text="Lithuania" /></option>
                              <option value="LUX"><spring:message code="ssap.county.Luxembourg" text="Luxembourg" /></option>
                              <option value="MAC"><spring:message code="ssap.county.Macao" text="Macao" /></option>
                              <option value="MKD"><spring:message code="ssap.county.Macedonia, The Former Yugoslav Republic Of" text="Macedonia, The Former Yugoslav Republic Of" /></option>
                              <option value="MDG"><spring:message code="ssap.county.Madagascar" text="Madagascar" /></option>
                              <option value="MWI"><spring:message code="ssap.county.Malawi" text="Malawi" /></option>
                              <option value="MYS"><spring:message code="ssap.county.Malaysia" text="Malaysia" /></option>
                              <option value="MDV"><spring:message code="ssap.county.Maldives" text="Maldives" /></option>
                              <option value="MLI"><spring:message code="ssap.county.Mali" text="Mali" /></option>
                              <option value="MLT"><spring:message code="ssap.county.Malta" text="Malta" /></option>
                              <option value="MHL"><spring:message code="ssap.county.Marshall Islands" text="Marshall Islands" /></option>
                              <option value="MTQ"><spring:message code="ssap.county.Martinique" text="Martinique" /></option>
                              <option value="MRT"><spring:message code="ssap.county.Mauritania" text="Mauritania" /></option>
                              <option value="MUS"><spring:message code="ssap.county.Mauritius" text="Mauritius" /></option>
                              <option value="MYT"><spring:message code="ssap.county.Mayotte" text="Mayotte" /></option>
                              <option value="MEX"><spring:message code="ssap.county.Mexico" text="Mexico" /></option>
                              <option value="FSM"><spring:message code="ssap.county.Micronesia, Federated States Of" text="Micronesia, Federated States Of" /></option>
                              <option value="MDA"><spring:message code="ssap.county.Moldova, Republic Of" text="Moldova, Republic Of" /></option>
                              <option value="MCO"><spring:message code="ssap.county.Monaco" text="Monaco" /></option>
                              <option value="MNG"><spring:message code="ssap.county.Mongolia" text="Mongolia" /></option>
                              <option value="MNE"><spring:message code="ssap.county.Montenegro" text="Montenegro" /></option>
                              <option value="MSR"><spring:message code="ssap.county.Montserrat" text="Montserrat" /></option>
                              <option value="MAR"><spring:message code="ssap.county.Morocco" text="Morocco" /></option>
                              <option value="MOZ"><spring:message code="ssap.county.Mozambique" text="Mozambique" /></option>
                              <option value="MMR"><spring:message code="ssap.county.Myanmar" text="Myanmar" /></option>
                              <option value="NAM"><spring:message code="ssap.county.Namibia" text="Namibia" /></option>
                              <option value="NRU"><spring:message code="ssap.county.Nauru" text="Nauru" /></option>
                              <option value="NPL"><spring:message code="ssap.county.Nepal" text="Nepal" /></option>
                              <option value="NLD"><spring:message code="ssap.county.Netherlands" text="Netherlands" /></option>
                              <option value="ANT"><spring:message code="ssap.county.Netherlands Antilles" text="Netherlands Antilles" /></option>
                              <option value="NCL"><spring:message code="ssap.county.New Caledonia" text="New Caledonia" /></option>
                              <option value="NZL"><spring:message code="ssap.county.New Zealand" text="New Zealand" /></option>
                              <option value="NIC"><spring:message code="ssap.county.Nicaragua" text="Nicaragua" /></option>
                              <option value="NER"><spring:message code="ssap.county.Niger" text="Niger" /></option>
                              <option value="NGA"><spring:message code="ssap.county.Nigeria" text="Nigeria" /></option>
                              <option value="NIU"><spring:message code="ssap.county.Niue" text="Niue" /></option>
                              <option value="NFK"><spring:message code="ssap.county.Norfolk Island" text="Norfolk Island" /></option>
                              <option value="MNP"><spring:message code="ssap.county.Northern Mariana Islands" text="Northern Mariana Islands" /></option>
                              <option value="NOR"><spring:message code="ssap.county.Norway" text="Norway" /></option>
                              <option value="OMN"><spring:message code="ssap.county.Oman" text="Oman" /></option>
                              <option value="PAK"><spring:message code="ssap.county.Pakistan" text="Pakistan" /></option>
                              <option value="PLW"><spring:message code="ssap.county.Palau" text="Palau" /></option>
                              <option value="PSE"><spring:message code="ssap.county.Palestinian Territory, Occupied" text="Palestinian Territory, Occupied" /></option>
                              <option value="PAN"><spring:message code="ssap.county.Panama" text="Panama" /></option>
                              <option value="PNG"><spring:message code="ssap.county.Papua New Guinea" text="Papua New Guinea" /></option>
                              <option value="PRY"><spring:message code="ssap.county.Paraguay" text="Paraguay" /></option>
                              <option value="PER"><spring:message code="ssap.county.Peru" text="Peru" /></option>
                              <option value="PHL"><spring:message code="ssap.county.Philippines" text="Philippines" /></option>
                              <option value="PCN"><spring:message code="ssap.county.Pitcairn" text="Pitcairn" /></option>
                              <option value="POL"><spring:message code="ssap.county.Poland" text="Poland" /></option>
                              <option value="PRT"><spring:message code="ssap.county.Portugal" text="Portugal" /></option>
                              <option value="PRI"><spring:message code="ssap.county.Puerto Rico" text="Puerto Rico" /></option>
                              <option value="QAT"><spring:message code="ssap.county.Qatar" text="Qatar" /></option>
                              <option value="REU"><spring:message code="ssap.county.Reunion" text="Reunion" /></option>
                              <option value="ROU"><spring:message code="ssap.county.Romania" text="Romania" /></option>
                              <option value="RUS"><spring:message code="ssap.county.Russian Federation" text="Russian Federation" /></option>
                              <option value="RWA"><spring:message code="ssap.county.Rwanda" text="Rwanda" /></option>
                              <option value="BLM"><spring:message code="ssap.county.Saint Barthelemy" text="Saint Barthelemy" /></option>
                              <option value="SHN"><spring:message code="ssap.county.Saint Helena, Ascension And Tristan Da Cunha" text="Saint Helena, Ascension And Tristan Da Cunha" /></option>
                              <option value="KNA"><spring:message code="ssap.county.Saint Kitts And Nevis" text="Saint Kitts And Nevis" /></option>
                              <option value="LCA"><spring:message code="ssap.county.Saint Lucia" text="Saint Lucia" /></option>
                              <option value="MAF"><spring:message code="ssap.county.Saint Martin (French Part)" text="Saint Martin (French Part)" /></option>
                              <option value="SPM"><spring:message code="ssap.county.Saint Pierre And Miquelon" text="Saint Pierre And Miquelon" /></option>
                              <option value="VCT"><spring:message code="ssap.county.Saint Vincent And The Grenadines" text="Saint Vincent And The Grenadines" /></option>
                              <option value="WSM"><spring:message code="ssap.county.Samoa" text="Samoa" /></option>
                              <option value="SMR"><spring:message code="ssap.county.San Marino" text="San Marino" /></option>
                              <option value="STP"><spring:message code="ssap.county.Sao Tome And Principe" text="Sao Tome And Principe" /></option>
                              <option value="SAU"><spring:message code="ssap.county.Saudi Arabia" text="Saudi Arabia" /></option>
                              <option value="SEN"><spring:message code="ssap.county.Senegal" text="Senegal" /></option>
                              <option value="SRB"><spring:message code="ssap.county.Serbia" text="Serbia" /></option>
                              <option value="SYC"><spring:message code="ssap.county.Seychelles" text="Seychelles" /></option>
                              <option value="SLE"><spring:message code="ssap.county.Sierra Leone" text="Sierra Leone" /></option>
                              <option value="SGP"><spring:message code="ssap.county.Singapore" text="Singapore" /></option>
                              <option value="SVK"><spring:message code="ssap.county.Slovakia" text="Slovakia" /></option>
                              <option value="SVN"><spring:message code="ssap.county.Slovenia" text="Slovenia" /></option>
                              <option value="SLB"><spring:message code="ssap.county.Solomon Islands" text="Solomon Islands" /></option>
                              <option value="SOM"><spring:message code="ssap.county.Somalia" text="Somalia" /></option>
                              <option value="ZAF"><spring:message code="ssap.county.South Africa" text="South Africa" /></option>
                              <option value="SGS"><spring:message code="ssap.county.South Georgia And The South Sandwich Islands" text="South Georgia And The South Sandwich Islands" /></option>
                              <option value="SSD"><spring:message code="ssap.county.South Sudan" text="South Sudan" /></option>
                              <option value="ESP"><spring:message code="ssap.county.Spain" text="Spain" /></option>
                              <option value="LKA"><spring:message code="ssap.county.Sri Lanka" text="Sri Lanka" /></option>
                              <option value="STL"><spring:message code="ssap.county.Stateless" text="Stateless" /></option>
                              <option value="SDN"><spring:message code="ssap.county.Sudan" text="Sudan" /></option>
                              <option value="SUR"><spring:message code="ssap.county.Suriname" text="Suriname" /></option>
                              <option value="SJM"><spring:message code="ssap.county.Svalbard And Jan Mayen" text="Svalbard And Jan Mayen" /></option>
                              <option value="SWZ"><spring:message code="ssap.county.Swaziland" text="Swaziland" /></option>
                              <option value="SWE"><spring:message code="ssap.county.Sweden" text="Sweden" /></option>
                              <option value="CHE"><spring:message code="ssap.county.Switzerland" text="Switzerland" /></option>
                              <option value="SYR"><spring:message code="ssap.county.Syrian Arab Republic" text="Syrian Arab Republic" /></option>
                              <option value="TWN"><spring:message code="ssap.county.Taiwan, Province Of China" text="Taiwan, Province Of China" /></option>
                              <option value="TJK"><spring:message code="ssap.county.Tajikistan" text="Tajikistan" /></option>
                              <option value="TZA"><spring:message code="ssap.county.Tanzania, United Republic Of" text="Tanzania, United Republic Of" /></option>
                              <option value="THA"><spring:message code="ssap.county.Thailand" text="Thailand" /></option>
                              <option value="TLS"><spring:message code="ssap.county.Timor-Leste" text="Timor-Leste" /></option>
                              <option value="TGO"><spring:message code="ssap.county.Togo" text="Togo" /></option>
                              <option value="TKL"><spring:message code="ssap.county.Tokelau" text="Tokelau" /></option>
                              <option value="TON"><spring:message code="ssap.county.Tonga" text="Tonga" /></option>
                              <option value="TTO"><spring:message code="ssap.county.Trinidad And Tobago" text="Trinidad And Tobago" /></option>
                              <option value="TUN"><spring:message code="ssap.county.Tunisia" text="Tunisia" /></option>
                              <option value="TUR"><spring:message code="ssap.county.Turkey" text="Turkey" /></option>
                              <option value="TKM"><spring:message code="ssap.county.Turkmenistan" text="Turkmenistan" /></option>
                              <option value="TCA"><spring:message code="ssap.county.Turks And Caicos Islands" text="Turks And Caicos Islands" /></option>
                              <option value="TUV"><spring:message code="ssap.county.Tuvalu" text="Tuvalu" /></option>
                              <option value="UGA"><spring:message code="ssap.county.Uganda" text="Uganda" /></option>
                              <option value="UKR"><spring:message code="ssap.county.Ukraine" text="Ukraine" /></option>
                              <option value="ARE"><spring:message code="ssap.county.United Arab Emirates" text="United Arab Emirates" /></option>
                              <option value="GBR"><spring:message code="ssap.county.United Kingdom" text="United Kingdom" /></option>
                              <option value="USA"><spring:message code="ssap.county.United States" text="United States" /></option>
                              <option value="UMI"><spring:message code="ssap.county.United States Minor Outlying Islands" text="United States Minor Outlying Islands" /></option>
                              <option value="URY"><spring:message code="ssap.county.Uruguay" text="Uruguay" /></option>
                              <option value="UZB"><spring:message code="ssap.county.Uzbekistan" text="Uzbekistan" /></option>
                              <option value="VUT"><spring:message code="ssap.county.Vanuatu" text="Vanuatu" /></option>
                              <option value="VEN"><spring:message code="ssap.county.Venezuela, Bolivarian Republic Of" text="Venezuela, Bolivarian Republic Of" /></option>
                              <option value="VNM"><spring:message code="ssap.county.Viet Nam" text="Viet Nam" /></option>
                              <option value="VGB"><spring:message code="ssap.county.Virgin Islands (British)" text="Virgin Islands (British)" /></option>
                              <option value="VIR"><spring:message code="ssap.county.Virgin Islands (U.S.)" text="Virgin Islands (U.S.)" /></option>
                              <option value="WLF"><spring:message code="ssap.county.Wallis And Futuna" text="Wallis And Futuna" /></option>
                              <option value="ESH"><spring:message code="ssap.county.Western Sahara *" text="Western Sahara *" /></option>
                              <option value="YEM"><spring:message code="ssap.county.Yemen" text="Yemen" /></option>
                              <option value="ZMB"><spring:message code="ssap.county.Zambia" text="Zambia" /></option>
                              <option value="ZWE"><spring:message code="ssap.county.Zimbabwe" text="Zimbabwe" /></option>
                           </select>
                           <span class="errorMessage"></span>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label" for="ForeignppI94dateOfExp">
                           <spring:message code="ssap.page12A.expDate" text="Passport Expiration Date"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <div data-date="" class="input-append date date-picker">
                              <input type="text" class="input-medium dob-input" name="ForeignppI94dateOfExp" id="ForeignppI94dateOfExp" data-parsley-trigger="change" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.passportExpDateRequired' />" data-parsley-errors-container="#ForeignppI94dateOfExpErrorDiv" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-pattern = "(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" />
                              <span class="add-on dateSpan" ><i class="icon-calendar"></i></span>
                           </div>
                           <div id="ForeignppI94dateOfExpErrorDiv"></div>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     
                     <div class="control-group">
                        <label class="control-label" for="ForeignppI94SevisID">
                           <spring:message code="ssap.page12A.sevisIdNumber" text="SEVIS ID Number"/>
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.sevisIdNumber" text="SEVIS ID Number"/>" type="text" id="ForeignppI94SevisID" name="ForeignppI94SevisID" data-parsley-pattern="^(N|n)[0-9]{10}$"  data-parsley-pattern-message="<spring:message code='ssap.error.SEVISIDValid'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->         
                  </div>


                  <label class="radio" for="rentrypermit">
                     <input id="rentrypermit" type="radio" name="docType" value="0"><spring:message code="ssap.page12A.reEntryPermit" text="Reentry Permit (I-327)"/>
                  </label>          
                  <div class="immigrationDocType hide" id="rentrypermitdiv">
                     <div class="control-group">
                        <label class="control-label" for="rentrypermitAlignNumber">
                           <spring:message code="ssap.page12A.alignNumber" text="Alien Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.placeholder.alignNum" text="Alien Number"/>" type="text" id="rentrypermitAlignNumber" name="rentrypermitAlignNumber" data-parsley-pattern="^[0-9]{7,9}$"  data-parsley-pattern-message="<spring:message code='ssap.error.alienNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.alienNumRequired'/>"/>
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="rentrypermitdateOfExp">
                           <spring:message code="ssap.page12A.expDate" text="Expiration Date"/>
                        </label>
                        <div class="controls">
                           <div data-date="" class="input-append date date-picker">
                              <input type="text" class="input-medium dob-input" name="rentrypermitdateOfExp" id="rentrypermitdateOfExp" data-parsley-trigger="change" data-parsley-errors-container="#rentrypermitdateOfExpErrorDiv" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-pattern = "(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" />
                              <span class="add-on dateSpan" ><i class="icon-calendar"></i></span>
                           </div>
                           <div id="rentrypermitdateOfExpErrorDiv"></div>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->               
                  </div>   
                  
                  <label class="radio" for="refugeetraveldocI-571">
                     <input id="refugeetraveldocI-571" type="radio" name="docType" value="0"><spring:message code="ssap.page12A.refugeeTravelDocI571" text="Refugee Travel Document (I-571)"/>
                  </label>                            
                  <div class="immigrationDocType hide" id="refugeetraveldocI-571div">
                     <div class="control-group">
                        <label class="control-label" for="refugeetraveldocI-571AlignNumber">
                           <spring:message code="ssap.page12A.alignNumber" text="Alien Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.placeholder.alignNum" text="Alien Number"/>" type="text" id="refugeetraveldocI-571AlignNumber" name="refugeetraveldocI-571AlignNumber" data-parsley-pattern="^[0-9]{7,9}$"  data-parsley-pattern-message="<spring:message code='ssap.error.alienNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.alienNumRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="refugeetraveldocI-571dateOfExp">
                           <spring:message code="ssap.page12A.expDate" text="Expiration Date"/>
                        </label>
                        <div class="controls">
                           <div data-date=""  class="input-append date date-picker">
                              <input type="text" class="input-medium dob-input" name="refugeetraveldocI-571dateOfExp" id="refugeetraveldocI-571dateOfExp" data-parsley-trigger="change" data-parsley-errors-container="#refugeetraveldocI-571dateOfExpErrorDiv" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-pattern = "(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" />
                              <span class="add-on dateSpan" ><i class="icon-calendar"></i></span>
                           </div>
                           <div id="refugeetraveldocI-571dateOfExpErrorDiv"></div>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->               
                  </div>
                  
                  <label class="radio" for="certificatenonimmigrantF1-Student-I20"> 
                     <input type="radio" id="certificatenonimmigrantF1-Student-I20" name="docType" value="certificatenonimmigrantF1-Student-I20"><spring:message code="ssap.page12A.arrivaldeprecordForeign" text="Certificate of Eligibility for Nonimmigrant (F-1) Student Status (I-20)"/>
                  </label>
                  <div class="immigrationDocType hide" id="certificatenonimmigrantF1-Student-I20div">
                     
                     <div class="control-group">
                        <label class="control-label" for="certificatenonimmigrantF1-Student-I20SevisID">
                           <spring:message code="ssap.page12A.sevisIdNumber" text="SEVIS ID Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.sevisIdNumber" text="SEVIS ID Number"/>" type="text" id="certificatenonimmigrantF1-Student-I20SevisID" name="certificatenonimmigrantF1-Student-I20SevisID" data-parsley-pattern="^[0-9]{10}$"  data-parsley-pattern-message="<spring:message code='ssap.error.SEVISIDValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.SEVISIDRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->   
                     <div class="control-group">
                        <label class="control-label" for="certificatenonimmigrantF1-Student-I20I94no">
                           <spring:message code="ssap.page12A.i94Number" text="I-94 Number"/>
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.i94Number" text="I-94 Number"/>" type="text" id="certificatenonimmigrantF1-Student-I20I94no" name="certificatenonimmigrantF1-Student-I20I94no" data-parsley-pattern="^[a-zA-Z0-9]{11}$"  data-parsley-pattern-message="<spring:message code='ssap.error.I94NumValid'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="certificatenonimmigrantF1-Student-I20ppcardno">
                           <spring:message code="ssap.page12A.passportNumber" text="Passport Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.passportNumber" text="Passport Number"/>" type="text" id="certificatenonimmigrantF1-Student-I20ppcardno" name="certificatenonimmigrantF1-Student-I20ppcardno" data-parsley-pattern="^[a-zA-Z0-9]{6,12}$"  data-parsley-pattern-message="<spring:message code='ssap.error.passportNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.passportNumRequired' />"/>
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->                     
                     <div class="control-group">
                        <label class="control-label" for="certificatenonimmigrantF1-Student-I20County">
                           <spring:message code="ssap.page12A.foreignPassportCountryOfIssuance" text="Country of Issuance"/>
                           <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                           <span aria-label="Required!"></span>
                        </label>
                        <div class="controls">
                           <select name="certificatenonimmigrantF1-Student-I20County" id="certificatenonimmigrantF1-Student-I20County" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.passportCountryRequired' />">
                              <option value=""><spring:message code="ssap.County" text="Country" /></option>
                              <option value="AFG"><spring:message code="ssap.county.Afghanistan" text="Afghanistan" /></option>
                              <option value="ALB"><spring:message code="ssap.county.Albania" text="Albania" /></option>
                              <option value="DZA"><spring:message code="ssap.county.Algeria" text="Algeria" /></option>
                              <option value="ASM"><spring:message code="ssap.county.American Samoa" text="American Samoa" /></option>
                              <option value="AND"><spring:message code="ssap.county.Andorra" text="Andorra" /></option>
                              <option value="AGO"><spring:message code="ssap.county.Angola" text="Angola" /></option>
                              <option value="AIA"><spring:message code="ssap.county.Anguilla" text="Anguilla" /></option>
                              <option value="ATA"><spring:message code="ssap.county.Antarctica" text="Antarctica" /></option>
                              <option value="ATG"><spring:message code="ssap.county.Antigua And Barbuda" text="Antigua And Barbuda" /></option>
                              <option value="ARG"><spring:message code="ssap.county.Argentina" text="Argentina" /></option>
                              <option value="ARM"><spring:message code="ssap.county.Armenia" text="Armenia" /></option>
                              <option value="ABW"><spring:message code="ssap.county.Aruba" text="Aruba" /></option>
                              <option value="AUS"><spring:message code="ssap.county.Australia" text="Australia" /></option>
                              <option value="AUT"><spring:message code="ssap.county.Austria" text="Austria" /></option>
                              <option value="AZE"><spring:message code="ssap.county.Azerbaijan" text="Azerbaijan" /></option>
                              <option value="BHS"><spring:message code="ssap.county.Bahamas" text="Bahamas" /></option>
                              <option value="BHR"><spring:message code="ssap.county.Bahrain" text="Bahrain" /></option>
                              <option value="BGD"><spring:message code="ssap.county.Bangladesh" text="Bangladesh" /></option>
                              <option value="BRB"><spring:message code="ssap.county.Barbados" text="Barbados" /></option>
                              <option value="BLR"><spring:message code="ssap.county.Belarus" text="Belarus" /></option>
                              <option value="BEL"><spring:message code="ssap.county.Belgium" text="Belgium" /></option>
                              <option value="BLZ"><spring:message code="ssap.county.Belize" text="Belize" /></option>
                              <option value="BEN"><spring:message code="ssap.county.Benin" text="Benin" /></option>
                              <option value="BMU"><spring:message code="ssap.county.Bermuda" text="Bermuda" /></option>
                              <option value="BTN"><spring:message code="ssap.county.Bhutan" text="Bhutan" /></option>
                              <option value="BOL"><spring:message code="ssap.county.Bolivia, Plurinational State Of" text="Bolivia, Plurinational State Of" /></option>
                              <option value="BIH"><spring:message code="ssap.county.Bosnia And Herzegovina" text="Bosnia And Herzegovina" /></option>
                              <option value="BWA"><spring:message code="ssap.county.Botswana" text="Botswana" /></option>
                              <option value="BVT"><spring:message code="ssap.county.Bouvet Island" text="Bouvet Island" /></option>
                              <option value="BRA"><spring:message code="ssap.county.Brazil" text="Brazil" /></option>
                              <option value="IOT"><spring:message code="ssap.county.British Indian Ocean Territory" text="British Indian Ocean Territory" /></option>
                              <option value="BRN"><spring:message code="ssap.county.Brunei Darussalam" text="Brunei Darussalam" /></option>
                              <option value="BGR"><spring:message code="ssap.county.Bulgaria" text="Bulgaria" /></option>
                              <option value="BFA"><spring:message code="ssap.county.Burkina Faso" text="Burkina Faso" /></option>
                              <option value="BDI"><spring:message code="ssap.county.Burundi" text="Burundi" /></option>
                              <option value="KHM"><spring:message code="ssap.county.Cambodia" text="Cambodia" /></option>
                              <option value="CMR"><spring:message code="ssap.county.Cameroon" text="Cameroon" /></option>
                              <option value="CAN"><spring:message code="ssap.county.Canada" text="Canada" /></option>
                              <option value="CPV"><spring:message code="ssap.county.Cape Verde" text="Cape Verde" /></option>
                              <option value="CYM"><spring:message code="ssap.county.Cayman Islands" text="Cayman Islands" /></option>
                              <option value="CAF"><spring:message code="ssap.county.Central African Republic" text="Central African Republic" /></option>
                              <option value="CIV"><spring:message code="ssap.county.Cete D'Ivoire" text="Cete D'Ivoire" /></option>
                              <option value="TCD"><spring:message code="ssap.county.Chad" text="Chad" /></option>
                              <option value="CHL"><spring:message code="ssap.county.Chile" text="Chile" /></option>
                              <option value="CHN"><spring:message code="ssap.county.China" text="China" /></option>
                              <option value="CXR"><spring:message code="ssap.county.Christmas Island" text="Christmas Island" /></option>
                              <option value="CCK"><spring:message code="ssap.county.Cocos (Keeling) Islands" text="Cocos (Keeling) Islands" /></option>
                              <option value="COL"><spring:message code="ssap.county.Colombia" text="Colombia" /></option>
                              <option value="COM"><spring:message code="ssap.county.Comoros" text="Comoros" /></option>
                              <option value="COG"><spring:message code="ssap.county.Congo" text="Congo" /></option>
                              <option value="COD"><spring:message code="ssap.county.Congo, Democratic Republic Of The" text="Congo, Democratic Republic Of The" /></option>
                              <option value="COK"><spring:message code="ssap.county.Cook Islands" text="Cook Islands" /></option>
                              <option value="CRI"><spring:message code="ssap.county.Costa Rica" text="Costa Rica" /></option>
                              <option value="HRV"><spring:message code="ssap.county.Croatia" text="Croatia" /></option>
                              <option value="CUB"><spring:message code="ssap.county.Cuba" text="Cuba" /></option>
                              <option value="CYP"><spring:message code="ssap.county.Cyprus" text="Cyprus" /></option>
                              <option value="CZE"><spring:message code="ssap.county.Czech Republic" text="Czech Republic" /></option>
                              <option value="DNK"><spring:message code="ssap.county.Denmark" text="Denmark" /></option>
                              <option value="DJI"><spring:message code="ssap.county.Djibouti" text="Djibouti" /></option>
                              <option value="DMA"><spring:message code="ssap.county.Dominica" text="Dominica" /></option>
                              <option value="DOM"><spring:message code="ssap.county.Dominican Republic" text="Dominican Republic" /></option>
                              <option value="ECU"><spring:message code="ssap.county.Ecuador" text="Ecuador" /></option>
                              <option value="EGY"><spring:message code="ssap.county.Egypt" text="Egypt" /></option>
                              <option value="SLV"><spring:message code="ssap.county.El Salvador" text="El Salvador" /></option>
                              <option value="ALA"><spring:message code="ssap.county.Eland Islands" text="Eland Islands" /></option>
                              <option value="GNQ"><spring:message code="ssap.county.Equatorial Guinea" text="Equatorial Guinea" /></option>
                              <option value="ERI"><spring:message code="ssap.county.Eritrea" text="Eritrea" /></option>
                              <option value="EST"><spring:message code="ssap.county.Estonia" text="Estonia" /></option>
                              <option value="ETH"><spring:message code="ssap.county.Ethiopia" text="Ethiopia" /></option>
                              <option value="FLK"><spring:message code="ssap.county.Falkland Islands (Malvinas)" text="Falkland Islands (Malvinas)" /></option>
                              <option value="FRO"><spring:message code="ssap.county.Faroe Islands" text="Faroe Islands" /></option>
                              <option value="FJI"><spring:message code="ssap.county.Fiji" text="Fiji" /></option>
                              <option value="FIN"><spring:message code="ssap.county.Finland" text="Finland" /></option>
                              <option value="FRA"><spring:message code="ssap.county.France" text="France" /></option>
                              <option value="GUF"><spring:message code="ssap.county.French Guiana" text="French Guiana" /></option>
                              <option value="PYF"><spring:message code="ssap.county.French Polynesia" text="French Polynesia" /></option>
                              <option value="ATF"><spring:message code="ssap.county.French Southern Territories" text="French Southern Territories" /></option>
                              <option value="GAB"><spring:message code="ssap.county.Gabon" text="Gabon" /></option>
                              <option value="GMB"><spring:message code="ssap.county.Gambia" text="Gambia" /></option>
                              <option value="GEO"><spring:message code="ssap.county.Georgia" text="Georgia" /></option>
                              <option value="DEU"><spring:message code="ssap.county.Germany" text="Germany" /></option>
                              <option value="GHA"><spring:message code="ssap.county.Ghana" text="Ghana" /></option>
                              <option value="GIB"><spring:message code="ssap.county.Gibraltar" text="Gibraltar" /></option>
                              <option value="GRC"><spring:message code="ssap.county.Greece" text="Greece" /></option>
                              <option value="GRL"><spring:message code="ssap.county.Greenland" text="Greenland" /></option>
                              <option value="GRD"><spring:message code="ssap.county.Grenada" text="Grenada" /></option>
                              <option value="GLP"><spring:message code="ssap.county.Guadeloupe" text="Guadeloupe" /></option>
                              <option value="GUM"><spring:message code="ssap.county.Guam" text="Guam" /></option>
                              <option value="GTM"><spring:message code="ssap.county.Guatemala" text="Guatemala" /></option>
                              <option value="GGY"><spring:message code="ssap.county.Guernsey" text="Guernsey" /></option>
                              <option value="GIN"><spring:message code="ssap.county.Guinea" text="Guinea" /></option>
                              <option value="GNB"><spring:message code="ssap.county.Guinea-Bissau" text="Guinea-Bissau" /></option>
                              <option value="GUY"><spring:message code="ssap.county.Guyana" text="Guyana" /></option>
                              <option value="HTI"><spring:message code="ssap.county.Haiti" text="Haiti" /></option>
                              <option value="HMD"><spring:message code="ssap.county.Heard Island And Mcdonald Islands" text="Heard Island And Mcdonald Islands" /></option>
                              <option value="VAT"><spring:message code="ssap.county.Holy See (Vatican City State)" text="Holy See (Vatican City State)" /></option>
                              <option value="HND"><spring:message code="ssap.county.Honduras" text="Honduras" /></option>
                              <option value="HKG"><spring:message code="ssap.county.Hong Kong" text="Hong Kong" /></option>
                              <option value="HUN"><spring:message code="ssap.county.Hungary" text="Hungary" /></option>
                              <option value="ISL"><spring:message code="ssap.county.Iceland" text="Iceland" /></option>
                              <option value="IND"><spring:message code="ssap.county.India" text="India" /></option>
                              <option value="IDN"><spring:message code="ssap.county.Indonesia" text="Indonesia" /></option>
                              <option value="IRN"><spring:message code="ssap.county.Iran, Islamic Republic Of" text="Iran, Islamic Republic Of" /></option>
                              <option value="IRQ"><spring:message code="ssap.county.Iraq" text="Iraq" /></option>
                              <option value="IRL"><spring:message code="ssap.county.Ireland" text="Ireland" /></option>
                              <option value="IMN"><spring:message code="ssap.county.Isle Of Man" text="Isle Of Man" /></option>
                              <option value="ISR"><spring:message code="ssap.county.Israel" text="Israel" /></option>
                              <option value="ITA"><spring:message code="ssap.county.Italy" text="Italy" /></option>
                              <option value="JAM"><spring:message code="ssap.county.Jamaica" text="Jamaica" /></option>
                              <option value="JPN"><spring:message code="ssap.county.Japan" text="Japan" /></option>
                              <option value="JEY"><spring:message code="ssap.county.Jersey" text="Jersey" /></option>
                              <option value="JOR"><spring:message code="ssap.county.Jordan" text="Jordan" /></option>
                              <option value="KAZ"><spring:message code="ssap.county.Kazakhstan" text="Kazakhstan" /></option>
                              <option value="KEN"><spring:message code="ssap.county.Kenya" text="Kenya" /></option>
                              <option value="KIR"><spring:message code="ssap.county.Kiribati" text="Kiribati" /></option>
                              <option value="PRK"><spring:message code="ssap.county.Korea, Democratic People'S Republic Of" text="Korea, Democratic People'S Republic Of" /></option>
                              <option value="KOR"><spring:message code="ssap.county.Korea, Republic Of" text="Korea, Republic Of" /></option>
                              <option value="KVO"><spring:message code="ssap.county.Kosovo" text="Kosovo" /></option>
                              <option value="KWT"><spring:message code="ssap.county.Kuwait" text="Kuwait" /></option>
                              <option value="KGZ"><spring:message code="ssap.county.Kyrgyzstan" text="Kyrgyzstan" /></option>
                              <option value="LAO"><spring:message code="ssap.county.Lao People'S Democratic Republic" text="Lao People'S Democratic Republic" /></option>
                              <option value="LVA"><spring:message code="ssap.county.Latvia" text="Latvia" /></option>
                              <option value="LBN"><spring:message code="ssap.county.Lebanon" text="Lebanon" /></option>
                              <option value="LSO"><spring:message code="ssap.county.Lesotho" text="Lesotho" /></option>
                              <option value="LBR"><spring:message code="ssap.county.Liberia" text="Liberia" /></option>
                              <option value="LBY"><spring:message code="ssap.county.Libyan Arab Jamahiriya" text="Libyan Arab Jamahiriya" /></option>
                              <option value="LIE"><spring:message code="ssap.county.Liechtenstein" text="Liechtenstein" /></option>
                              <option value="LTU"><spring:message code="ssap.county.Lithuania" text="Lithuania" /></option>
                              <option value="LUX"><spring:message code="ssap.county.Luxembourg" text="Luxembourg" /></option>
                              <option value="MAC"><spring:message code="ssap.county.Macao" text="Macao" /></option>
                              <option value="MKD"><spring:message code="ssap.county.Macedonia, The Former Yugoslav Republic Of" text="Macedonia, The Former Yugoslav Republic Of" /></option>
                              <option value="MDG"><spring:message code="ssap.county.Madagascar" text="Madagascar" /></option>
                              <option value="MWI"><spring:message code="ssap.county.Malawi" text="Malawi" /></option>
                              <option value="MYS"><spring:message code="ssap.county.Malaysia" text="Malaysia" /></option>
                              <option value="MDV"><spring:message code="ssap.county.Maldives" text="Maldives" /></option>
                              <option value="MLI"><spring:message code="ssap.county.Mali" text="Mali" /></option>
                              <option value="MLT"><spring:message code="ssap.county.Malta" text="Malta" /></option>
                              <option value="MHL"><spring:message code="ssap.county.Marshall Islands" text="Marshall Islands" /></option>
                              <option value="MTQ"><spring:message code="ssap.county.Martinique" text="Martinique" /></option>
                              <option value="MRT"><spring:message code="ssap.county.Mauritania" text="Mauritania" /></option>
                              <option value="MUS"><spring:message code="ssap.county.Mauritius" text="Mauritius" /></option>
                              <option value="MYT"><spring:message code="ssap.county.Mayotte" text="Mayotte" /></option>
                              <option value="MEX"><spring:message code="ssap.county.Mexico" text="Mexico" /></option>
                              <option value="FSM"><spring:message code="ssap.county.Micronesia, Federated States Of" text="Micronesia, Federated States Of" /></option>
                              <option value="MDA"><spring:message code="ssap.county.Moldova, Republic Of" text="Moldova, Republic Of" /></option>
                              <option value="MCO"><spring:message code="ssap.county.Monaco" text="Monaco" /></option>
                              <option value="MNG"><spring:message code="ssap.county.Mongolia" text="Mongolia" /></option>
                              <option value="MNE"><spring:message code="ssap.county.Montenegro" text="Montenegro" /></option>
                              <option value="MSR"><spring:message code="ssap.county.Montserrat" text="Montserrat" /></option>
                              <option value="MAR"><spring:message code="ssap.county.Morocco" text="Morocco" /></option>
                              <option value="MOZ"><spring:message code="ssap.county.Mozambique" text="Mozambique" /></option>
                              <option value="MMR"><spring:message code="ssap.county.Myanmar" text="Myanmar" /></option>
                              <option value="NAM"><spring:message code="ssap.county.Namibia" text="Namibia" /></option>
                              <option value="NRU"><spring:message code="ssap.county.Nauru" text="Nauru" /></option>
                              <option value="NPL"><spring:message code="ssap.county.Nepal" text="Nepal" /></option>
                              <option value="NLD"><spring:message code="ssap.county.Netherlands" text="Netherlands" /></option>
                              <option value="ANT"><spring:message code="ssap.county.Netherlands Antilles" text="Netherlands Antilles" /></option>
                              <option value="NCL"><spring:message code="ssap.county.New Caledonia" text="New Caledonia" /></option>
                              <option value="NZL"><spring:message code="ssap.county.New Zealand" text="New Zealand" /></option>
                              <option value="NIC"><spring:message code="ssap.county.Nicaragua" text="Nicaragua" /></option>
                              <option value="NER"><spring:message code="ssap.county.Niger" text="Niger" /></option>
                              <option value="NGA"><spring:message code="ssap.county.Nigeria" text="Nigeria" /></option>
                              <option value="NIU"><spring:message code="ssap.county.Niue" text="Niue" /></option>
                              <option value="NFK"><spring:message code="ssap.county.Norfolk Island" text="Norfolk Island" /></option>
                              <option value="MNP"><spring:message code="ssap.county.Northern Mariana Islands" text="Northern Mariana Islands" /></option>
                              <option value="NOR"><spring:message code="ssap.county.Norway" text="Norway" /></option>
                              <option value="OMN"><spring:message code="ssap.county.Oman" text="Oman" /></option>
                              <option value="PAK"><spring:message code="ssap.county.Pakistan" text="Pakistan" /></option>
                              <option value="PLW"><spring:message code="ssap.county.Palau" text="Palau" /></option>
                              <option value="PSE"><spring:message code="ssap.county.Palestinian Territory, Occupied" text="Palestinian Territory, Occupied" /></option>
                              <option value="PAN"><spring:message code="ssap.county.Panama" text="Panama" /></option>
                              <option value="PNG"><spring:message code="ssap.county.Papua New Guinea" text="Papua New Guinea" /></option>
                              <option value="PRY"><spring:message code="ssap.county.Paraguay" text="Paraguay" /></option>
                              <option value="PER"><spring:message code="ssap.county.Peru" text="Peru" /></option>
                              <option value="PHL"><spring:message code="ssap.county.Philippines" text="Philippines" /></option>
                              <option value="PCN"><spring:message code="ssap.county.Pitcairn" text="Pitcairn" /></option>
                              <option value="POL"><spring:message code="ssap.county.Poland" text="Poland" /></option>
                              <option value="PRT"><spring:message code="ssap.county.Portugal" text="Portugal" /></option>
                              <option value="PRI"><spring:message code="ssap.county.Puerto Rico" text="Puerto Rico" /></option>
                              <option value="QAT"><spring:message code="ssap.county.Qatar" text="Qatar" /></option>
                              <option value="REU"><spring:message code="ssap.county.Reunion" text="Reunion" /></option>
                              <option value="ROU"><spring:message code="ssap.county.Romania" text="Romania" /></option>
                              <option value="RUS"><spring:message code="ssap.county.Russian Federation" text="Russian Federation" /></option>
                              <option value="RWA"><spring:message code="ssap.county.Rwanda" text="Rwanda" /></option>
                              <option value="BLM"><spring:message code="ssap.county.Saint Barthelemy" text="Saint Barthelemy" /></option>
                              <option value="SHN"><spring:message code="ssap.county.Saint Helena, Ascension And Tristan Da Cunha" text="Saint Helena, Ascension And Tristan Da Cunha" /></option>
                              <option value="KNA"><spring:message code="ssap.county.Saint Kitts And Nevis" text="Saint Kitts And Nevis" /></option>
                              <option value="LCA"><spring:message code="ssap.county.Saint Lucia" text="Saint Lucia" /></option>
                              <option value="MAF"><spring:message code="ssap.county.Saint Martin (French Part)" text="Saint Martin (French Part)" /></option>
                              <option value="SPM"><spring:message code="ssap.county.Saint Pierre And Miquelon" text="Saint Pierre And Miquelon" /></option>
                              <option value="VCT"><spring:message code="ssap.county.Saint Vincent And The Grenadines" text="Saint Vincent And The Grenadines" /></option>
                              <option value="WSM"><spring:message code="ssap.county.Samoa" text="Samoa" /></option>
                              <option value="SMR"><spring:message code="ssap.county.San Marino" text="San Marino" /></option>
                              <option value="STP"><spring:message code="ssap.county.Sao Tome And Principe" text="Sao Tome And Principe" /></option>
                              <option value="SAU"><spring:message code="ssap.county.Saudi Arabia" text="Saudi Arabia" /></option>
                              <option value="SEN"><spring:message code="ssap.county.Senegal" text="Senegal" /></option>
                              <option value="SRB"><spring:message code="ssap.county.Serbia" text="Serbia" /></option>
                              <option value="SYC"><spring:message code="ssap.county.Seychelles" text="Seychelles" /></option>
                              <option value="SLE"><spring:message code="ssap.county.Sierra Leone" text="Sierra Leone" /></option>
                              <option value="SGP"><spring:message code="ssap.county.Singapore" text="Singapore" /></option>
                              <option value="SVK"><spring:message code="ssap.county.Slovakia" text="Slovakia" /></option>
                              <option value="SVN"><spring:message code="ssap.county.Slovenia" text="Slovenia" /></option>
                              <option value="SLB"><spring:message code="ssap.county.Solomon Islands" text="Solomon Islands" /></option>
                              <option value="SOM"><spring:message code="ssap.county.Somalia" text="Somalia" /></option>
                              <option value="ZAF"><spring:message code="ssap.county.South Africa" text="South Africa" /></option>
                              <option value="SGS"><spring:message code="ssap.county.South Georgia And The South Sandwich Islands" text="South Georgia And The South Sandwich Islands" /></option>
                              <option value="SSD"><spring:message code="ssap.county.South Sudan" text="South Sudan" /></option>
                              <option value="ESP"><spring:message code="ssap.county.Spain" text="Spain" /></option>
                              <option value="LKA"><spring:message code="ssap.county.Sri Lanka" text="Sri Lanka" /></option>
                              <option value="STL"><spring:message code="ssap.county.Stateless" text="Stateless" /></option>
                              <option value="SDN"><spring:message code="ssap.county.Sudan" text="Sudan" /></option>
                              <option value="SUR"><spring:message code="ssap.county.Suriname" text="Suriname" /></option>
                              <option value="SJM"><spring:message code="ssap.county.Svalbard And Jan Mayen" text="Svalbard And Jan Mayen" /></option>
                              <option value="SWZ"><spring:message code="ssap.county.Swaziland" text="Swaziland" /></option>
                              <option value="SWE"><spring:message code="ssap.county.Sweden" text="Sweden" /></option>
                              <option value="CHE"><spring:message code="ssap.county.Switzerland" text="Switzerland" /></option>
                              <option value="SYR"><spring:message code="ssap.county.Syrian Arab Republic" text="Syrian Arab Republic" /></option>
                              <option value="TWN"><spring:message code="ssap.county.Taiwan, Province Of China" text="Taiwan, Province Of China" /></option>
                              <option value="TJK"><spring:message code="ssap.county.Tajikistan" text="Tajikistan" /></option>
                              <option value="TZA"><spring:message code="ssap.county.Tanzania, United Republic Of" text="Tanzania, United Republic Of" /></option>
                              <option value="THA"><spring:message code="ssap.county.Thailand" text="Thailand" /></option>
                              <option value="TLS"><spring:message code="ssap.county.Timor-Leste" text="Timor-Leste" /></option>
                              <option value="TGO"><spring:message code="ssap.county.Togo" text="Togo" /></option>
                              <option value="TKL"><spring:message code="ssap.county.Tokelau" text="Tokelau" /></option>
                              <option value="TON"><spring:message code="ssap.county.Tonga" text="Tonga" /></option>
                              <option value="TTO"><spring:message code="ssap.county.Trinidad And Tobago" text="Trinidad And Tobago" /></option>
                              <option value="TUN"><spring:message code="ssap.county.Tunisia" text="Tunisia" /></option>
                              <option value="TUR"><spring:message code="ssap.county.Turkey" text="Turkey" /></option>
                              <option value="TKM"><spring:message code="ssap.county.Turkmenistan" text="Turkmenistan" /></option>
                              <option value="TCA"><spring:message code="ssap.county.Turks And Caicos Islands" text="Turks And Caicos Islands" /></option>
                              <option value="TUV"><spring:message code="ssap.county.Tuvalu" text="Tuvalu" /></option>
                              <option value="UGA"><spring:message code="ssap.county.Uganda" text="Uganda" /></option>
                              <option value="UKR"><spring:message code="ssap.county.Ukraine" text="Ukraine" /></option>
                              <option value="ARE"><spring:message code="ssap.county.United Arab Emirates" text="United Arab Emirates" /></option>
                              <option value="GBR"><spring:message code="ssap.county.United Kingdom" text="United Kingdom" /></option>
                              <option value="USA"><spring:message code="ssap.county.United States" text="United States" /></option>
                              <option value="UMI"><spring:message code="ssap.county.United States Minor Outlying Islands" text="United States Minor Outlying Islands" /></option>
                              <option value="URY"><spring:message code="ssap.county.Uruguay" text="Uruguay" /></option>
                              <option value="UZB"><spring:message code="ssap.county.Uzbekistan" text="Uzbekistan" /></option>
                              <option value="VUT"><spring:message code="ssap.county.Vanuatu" text="Vanuatu" /></option>
                              <option value="VEN"><spring:message code="ssap.county.Venezuela, Bolivarian Republic Of" text="Venezuela, Bolivarian Republic Of" /></option>
                              <option value="VNM"><spring:message code="ssap.county.Viet Nam" text="Viet Nam" /></option>
                              <option value="VGB"><spring:message code="ssap.county.Virgin Islands (British)" text="Virgin Islands (British)" /></option>
                              <option value="VIR"><spring:message code="ssap.county.Virgin Islands (U.S.)" text="Virgin Islands (U.S.)" /></option>
                              <option value="WLF"><spring:message code="ssap.county.Wallis And Futuna" text="Wallis And Futuna" /></option>
                              <option value="ESH"><spring:message code="ssap.county.Western Sahara *" text="Western Sahara *" /></option>
                              <option value="YEM"><spring:message code="ssap.county.Yemen" text="Yemen" /></option>
                              <option value="ZMB"><spring:message code="ssap.county.Zambia" text="Zambia" /></option>
                              <option value="ZWE"><spring:message code="ssap.county.Zimbabwe" text="Zimbabwe" /></option>
                           </select>
                           <span class="errorMessage"></span>
                        </div>
                     </div>                     
                     <div class="control-group">
                        <label class="control-label" for="certificatenonimmigrantF1-Student-I20dateOfExp">
                           <spring:message code="ssap.page12A.expDate" text="Passport Expiration Date"/>
                        </label>
                        <div class="controls">
                           <div data-date="" class="input-append date date-picker">
                              <input type="text" class="input-medium dob-input" name="certificatenonimmigrantF1-Student-I20dateOfExp" id="certificatenonimmigrantF1-Student-I20dateOfExp" data-parsley-trigger="change" data-parsley-errors-container="#certificatenonimmigrantF1-Student-I20dateOfExpErrorDiv" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-pattern = "(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>"  />
                              <span class="add-on dateSpan" ><i class="icon-calendar"></i></span>
                           </div>
                           <div id="certificatenonimmigrantF1-Student-I20dateOfExpErrorDiv"></div>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->      
                  </div>
                  
                  
                  

                  <label class="radio" for="certificatenonimmigrantJ1-Student-DS2019">
                     <input type="radio" id="certificatenonimmigrantJ1-Student-DS2019" name="docType" value="0"><spring:message code="ssap.page12A.exchangeVisitorEligibilityCertification" text="Certificate of Eligibility for Exchange Visitor (J-1) Status (DS2019)"/>
                  </label>
                  <div class="immigrationDocType hide" id="certificatenonimmigrantJ1-Student-DS2019div">                   
                     <div class="control-group">
                        <label class="control-label" for="certificatenonimmigrantJ1-Student-DS2019SevisID">
                           <spring:message code="ssap.page12A.sevisIdNumber" text="SEVIS ID Number"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.sevisIdNumber" text="SEVIS ID Number"/>" type="text" id="certificatenonimmigrantJ1-Student-DS2019SevisID" name="certificatenonimmigrantJ1-Student-DS2019SevisID"  data-parsley-pattern="^[0-9]{10}$"  data-parsley-pattern-message="<spring:message code='ssap.error.SEVISIDValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.SEVISIDRequired'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->   
                     <div class="control-group">
                        <label class="control-label" for="certificatenonimmigrantJ1-Student-DS2019I94no">
                           <spring:message code="ssap.page12A.i94Number" text="I-94 Number"/>
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.i94Number" text="I-94 Number"/>" type="text" id="certificatenonimmigrantJ1-Student-DS2019I94no" name="certificatenonimmigrantJ1-Student-DS2019I94no" data-parsley-pattern="^[a-zA-Z0-9]{11}$"  data-parsley-pattern-message="<spring:message code='ssap.error.I94NumValid'/>"/>
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="certificatenonimmigrantJ1-Student-DS2019ppcardno">
                           <spring:message code="ssap.page12A.passportNumber" text="Passport Number"/>
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.passportNumber" text="Passport Number"/>" type="text" id="certificatenonimmigrantJ1-Student-DS2019ppcardno" name="certificatenonimmigrantJ1-Student-DS2019ppcardno" data-parsley-pattern="^[a-zA-Z0-9]{6,12}$"  data-parsley-pattern-message="<spring:message code='ssap.error.passportNumValid'/>" />
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     <div class="control-group">
                        <label class="control-label" for="certificatenonimmigrantJ1-Student-DS2019dateOfExp">
                           <spring:message code="ssap.page12A.expDate" text="Passport Expiration Date"/>
                        </label>
                        <div class="controls">
                           <div data-date="" class="input-append date date-picker">
                              <input type="text" class="input-medium dob-input" name="certificatenonimmigrantJ1-Student-DS2019dateOfExp" id="certificatenonimmigrantJ1-Student-DS2019dateOfExp" data-parsley-trigger="change" data-parsley-errors-container="#certificatenonimmigrantJ1-Student-DS2019dateOfExpErrorDiv" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-pattern = "(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>"/>
                              <span class="add-on dateSpan" ><i class="icon-calendar"></i></span>
                           </div>
                           <div id="certificatenonimmigrantJ1-Student-DS2019dateOfExpErrorDiv"></div>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->      
                  </div>
                  
                  <!--  
                  <label class="radio" for="docType_other">
                     <input type="radio" id="docType_other" name="docType" value="OTHER_DOCUMENT" ><spring:message code="ssap.page12A.otherDocOrStatustype" text="Other documents or status types"/>
                  </label> -->
                  <div id="permcarddetailsErrorDiv"></div>
               </div>
               
               <div id="otherDocStatusOnPage62P1" class="immigrationDocType hide">
                  <div class="validation-error-row-container">
                     <div class="control-group">
                        <label class="control-label" for="DocAlignNumber">
                           <spring:message code="ssap.page12A.alignNum" text="Alien Number"/>
                           <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                           <span aria-label="Required!"></span>
                        </label>
                        <div class="controls required-input-field-container">
                           <input placeHolder="<spring:message code="ssap.page12A.placeholder.alignNum" text="Alien Number"/>" type="text" id="DocAlignNumber" name="DocAlignNumber" data-parsley-pattern="^[0-9]{7,9}$"  data-parsley-pattern-message="<spring:message code='ssap.error.alienNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.alienNumRequired'/>">                          
                        </div>
                     </div>
                     
                     <div class="control-group">
                        <label class="control-label" for="DocI-94Number">
                           <spring:message code="ssap.page12A.i" text="I"/>&minus;<spring:message code="ssap.page12A.number94" text="94 Number"/>
                           <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                           <span aria-label="Required!"></span>
                        </label>
                        <div class="controls required-input-field-container">
                           <input placeHolder="<spring:message code="ssap.page12A.placeholder.i94" text="I-94 Number"/>" type="text" id="DocI-94Number" name="DocI-94Number" data-parsley-pattern="^[a-zA-Z0-9]{11}$"  data-parsley-pattern-message="<spring:message code='ssap.error.I94NumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.I94NumRequired'/>">
                        </div>
                     </div>
                     
                     <div class="control-group">
                        <label class="control-label" for="passportDocNumber">
                           <spring:message code="ssap.page12A.passportOrDocNum" text="Passport or Document Number"/>
                           <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                           <span aria-label="Required!"></span>
                        </label>
                        <div class="controls required-input-field-container">
                           <input placeHolder="<spring:message code="ssap.page12A.placeholder.passportAndDocNum" text="Passport or Document Number"/>" size="30" type="text" id="passportDocNumber" name="passportDocNumber" data-parsley-pattern="^[a-zA-Z0-9]{6,12}$"  data-parsley-pattern-message="<spring:message code='ssap.error.passportDocumentNumValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.passportDocumentNumRequired'/>"/>                         
                        </div>
                     </div>
                     
         <!--           <div class="control-group">
                        <label class="control-label">
                           <spring:message code="ssap.page12A.foreignPassportCountryOfIssuance" text="Foreign Passport Country of Issuance:"/>
                           <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                           <span aria-label="Required!"></span>
                        </label>
                        <div class="controls required-input-field-container">
                           <select name="appscr62P1County" id="appscr62P1County">
                              <option value="0"><spring:message code="ssap.County" text="County" /></option>
                              <option value="Bernalillo"><spring:message code="ssap.county.Bernalillo" text="Bernalillo" /></option>
                              <option value="Catron"><spring:message code="ssap.county.Catron" text="Catron" /></option>
                              <option value="Chaves"><spring:message code="ssap.county.Chaves" text="Chaves" /></option>
                              <option value="Cibola"><spring:message code="ssap.county.Cibola" text="Cibola" /></option>
                              <option value="Colfax"><spring:message code="ssap.county.Colfax" text="Colfax" /></option>
                              <option value="Curry"><spring:message code="ssap.county.Curry" text="Curry" /></option>
                              <option value="De Baca"><spring:message code="ssap.county.DeBaca" text="De Baca" /></option>
                              <option value="Dona Ana "><spring:message code="ssap.county.DonaAna" text="Dona Ana" /> </option>
                              <option value="Eddy"><spring:message code="ssap.county.Eddy" text="Eddy" /></option>
                              <option value="Grant"><spring:message code="ssap.county.Grant" text="Grant" /></option>
                              <option value="Guadalupe"><spring:message code="ssap.county.Guadalupe" text="Guadalupe" /></option>
                              <option value="Harding"><spring:message code="ssap.county.Harding" text="Harding" /></option>
                              <option value="Hidalgo"><spring:message code="ssap.county.Hidalgo" text="Hidalgo" /></option>
                              <option value="Lea"><spring:message code="ssap.county.Lea" text="Lea" /></option>
                              <option value="Lincoln"><spring:message code="ssap.county.Lincoln" text="Lincoln" /></option>
                              <option value="Los Alamos "><spring:message code="ssap.county.LosAlamos" text="Los Alamos" /> </option>
                              <option value="Luna "><spring:message code="ssap.county.Luna" text="Luna" /> </option>
                              <option value="McKinley "><spring:message code="ssap.county.McKinley" text="McKinley" /> </option>
                              <option value="Mora "><spring:message code="ssap.county.Mora" text="Mora" /> </option>
                              <option value="Otero "><spring:message code="ssap.county.Otero" text="Otero" /> </option>
                              <option value="Quay "><spring:message code="ssap.county.Quay" text="Quay" /> </option>
                              <option value="Rio Arriba "><spring:message code="ssap.county.RioArriba" text="Rio Arriba" /> </option>
                              <option value="Roosevelt "><spring:message code="ssap.county.Roosevelt" text="Roosevelt" /> </option>
                              <option value="Sandoval "><spring:message code="ssap.county.Sandoval" text="Sandoval" /> </option>
                              <option value="San Juan "><spring:message code="ssap.county.SanJuan" text="San Juan" /> </option>
                              <option value="San Miguel "><spring:message code="ssap.county.SanMiguel" text="San Miguel" /> </option>
                              <option value="Santa Fe "><spring:message code="ssap.county.SantaFe" text="Santa Fe" /> </option>
                              <option value="Sierra "><spring:message code="ssap.county.Sierra" text="Sierra" /> </option>
                              <option value="Socorro "><spring:message code="ssap.county.Socorro" text="Socorro" /> </option>
                              <option value="Taos "><spring:message code="ssap.county.Taos" text="Taos" /> </option>
                              <option value="Torrance "><spring:message code="ssap.county.Torrance" text="Torrance" /> </option>
                              <option value="Union "><spring:message code="ssap.county.Union " text="Union " /></option>
                              <option value="Valencia "><spring:message code="ssap.county.Valencia" text="Valencia" /> </option>
                              <option value="other"><spring:message code="ssap.county.Other" text="Other" /></option>   
                           </select>
                           <ul id="parsley-62-county" class="parsley-error-list hide">
                              <li class="required">
                                 <span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
                              </li>
                           </ul>
                        </div>
                     </div> -->
                     
                     
                     <div class="control-group">
                        <label class="control-label capitalize-none" for="passportExpDate">
                           <spring:message code="ssap.page12A.passportExpDate" text="Passport Expiration Date"/>
                           <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                        </label><!-- end of label -->
                        <div class="controls">
                           <div class="input-append date date-picker" data-date="">
                              <input type="text" id="passportExpDate" name="passportExpDate" class="input-medium" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />"  data-parsley-pattern = "(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" />
                              <span  class="add-on dateSpan"><i class="icon-calendar"></i></span>
                           </div>
                            
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     
                     <div class="control-group">
                        <label class="control-label" for="sevisIDNumber">
                           <spring:message code="ssap.page12A.SEVISIDNUM" text="SEVIS ID Number"/>
                           <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                           <span aria-label="Required!"></span>
                        </label>
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.page12A.placeholder.sevisIDNum" text="SEVIS ID Number"/>" type="text" id="sevisIDNumber" name="sevisIDNumber" data-parsley-pattern="^(N|n)[0-9]{10}$"  data-parsley-pattern-message="<spring:message code='ssap.error.SEVISIDValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.SEVISIDRequired'/>">                           
                        </div>
                     </div>
                     
                     <div class="control-group">
                        <label class="control-label" for="documentDescription">
                           <spring:message code="ssap.page12A.docDesc" text="Document Description"/>
                        </label>
                        <div class="controls required-input-field-container">
                           <textarea class="TAW700MT12" id="documentDescription" type="text" placeholder="<spring:message code="ssap.page12A.placeholder.description" text="Description"/>"></textarea>
                        </div>
                     </div>
                     
                     
                  </div>
               </div>
               
               <div id="sameNameInfoPage62P2" class="hide"><!-- sameNameInfoPage62P2 starts -->
                  <div class="control-group">
                     <p>
                        <spring:message code="ssap.page12B.is" text="Is"/>
                        <strong class="nameOfHouseHold"></strong>
                        <spring:message code="ssap.page12B.samethatAppers" text=" the same name that appears on"/>
                        <span id="appscr62Part2SSNHisHer"></span>
                        <spring:message code="ssap.page12B.docWithQM" text="document?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                     </p>
                     <div class="margin5-l">
                        <label class="radio" for="62Part2_UScitizenYes">
                           <input type="radio" name="UScitizen62" id="62Part2_UScitizenYes" value="yes" onchange="displayOrHideSameNameInfoPage62();" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"  data-parsley-errors-container="#62Part2_UScitizenYesDiv"/>
                           <spring:message code="ssap.page12B.yes" text="Yes"/>
                        </label>
                        <label class="radio" for="62Part2_UScitizen">
                           <input type="radio" id="62Part2_UScitizen" name="UScitizen62" value="no" onchange="displayOrHideSameNameInfoPage62();" />
                           <spring:message code="ssap.page12B.no" text="No"/>
                        </label>
                        <div id="62Part2_UScitizenYesDiv"></div>
                     </div>
                  </div><!-- control-group ends -->
                  
                  
                  <div id="sameNameOnDocument" class="hide">
                  <div class="control-group">
                        <label class="required control-label" for="documentFirstName">
                           <spring:message code="ssap.firstName" text="First Name"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                        </label><!-- end of label -->
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.firstName" text="First Name"/>" class="input-large" type="text" id="documentFirstName" name="documentFirstName" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.firstNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.firstNameRequired'/>"/>
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     
                     <div class="control-group">
                        <label class="control-label" for="documentMiddleName">
                           <spring:message code="ssap.middleName" text="Middle Name"/>
                        </label><!-- end of label -->
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.middleName" text="Middle Name"/>"  class="input-large" type="text" id="documentMiddleName" name="documentMiddleName" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.middleNameValid'/>"/>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     
                     <div class="control-group">
                        <label class="required control-label" for="documentLastName">
                           <spring:message code="ssap.lastName" text="Last Name" /><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                        </label><!-- end of label -->
                        <div class="controls">
                           <input placeHolder="<spring:message code="ssap.lastName" text="Last Name"/>" class="input-large" type="text" id="documentLastName" name="documentLastName" data-parsley-pattern="^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$"  data-parsley-pattern-message="<spring:message code='ssap.error.lastNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.lastNameRequired'/>"/>
                           <span class="errorMessage"></span>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     
                     <div class="control-group">
                        <label class="control-label" for="documentSuffix">
                           <spring:message code="ssap.suffix" text="Suffix" />
                        </label><!-- end of label -->
                        <div class="controls">
                           <select class="input-large" id="documentSuffix" name="documentSuffix">
                              <option value="" selected="selected"><spring:message code="ssap.suffix" text="Suffix" /></option>
                              <option value="Jr."><spring:message code="ssap.JR" text="Jr." /></option>
                              <option value="Sr."><spring:message code="ssap.SR" text="Sr." /></option>
                              <option value="III"><spring:message code="ssap.III" text="III" /></option>
                              <option value="IV"><spring:message code="ssap.IV" text="IV" /></option>
                           </select>
                           <div class="help-inline" id="suffix_error4"></div>
                        </div><!-- end of controls-->
                     </div><!-- control-group ends -->
                     </div>
               </div><!-- sameNameInfoPage62P2 ends -->
               
               
               <!--Any other Documents starts-->                     
               <div class="control-group hide" id="haveTheseDocuments">
                  <p>
                     <spring:message code="ssap.page12A.Does" text="Does"/>
                     <strong class="nameOfHouseHold"></strong>
                     <spring:message code="ssap.page12A.anyotherdoc" text="also have any of these documents? (Select all that apply)"/>
                  </p>
                  
                  <div class="gutter10-lr">
                     <label class="radio" for="otherDocORR">
                        <input type="checkbox" unchecked id="otherDocORR" name="checkboxValid" value="0" parsley-mincheck="1"/>  <spring:message code="ssap.page12A.otherDocORR" text="Certification From U.S. Department of Health and Human Services (HHS) Office of Refugee Resettlement (ORR)"/> 
                     </label>
                     
                     <label class="radio" for="otherDocORR-under18">
                        <input type="checkbox" unchecked id="otherDocORR-under18" name="checkboxValid" value="0" />  <spring:message code="ssap.page12A.otherDocORRUnder18" text="Office of Refugee Resettlement (ORR) Eligibility Letter (if Under 18)"/> 
                     </label>
                     
                     <label class="radio" for="Cuban-or-HaitianEntrant">
                        <input type="checkbox" unchecked id="Cuban-or-HaitianEntrant" name="checkboxValid" value="0" /> <spring:message code="ssap.page12A.CubanOrHaitianEntrant" text="Cuban/Haitian Entrant"/> 
                     </label>
                     
                     <label class="radio" for="docRemoval">
                        <input type="checkbox" unchecked id="docRemoval" name="checkboxValid" value="0" />  <spring:message code="ssap.page12A.docRemoval" text="Document Indicating Withholding of Removal"/> 
                     </label>
                     
                     <label class="radio" for="americanSamaoresident">
                        <input type="checkbox" unchecked id="americanSamaoresident" name="checkboxValid" value="0" />   <spring:message code="ssap.page12A.americanSamaoresident" text="Resident of American Samoa"/> 
                     </label>
                     
                     <label class="radio" for="adminOrderbyDeptofHomelanSecurity">
                        <input type="checkbox" unchecked id="adminOrderbyDeptofHomelanSecurity" name="checkboxValid" value="0" />   <spring:message code="ssap.page12A.adminOrderbyDeptofHomelanSecurity" text="Administrative Order Staying Removal Issued by the Department of Homeland Security"/> 
                     </label>
                     <span class="errorMessage"></span>
                  </div>
               </div><!-- control-group ends -->
               <!--Any other Documents ends-->  
               
               <!--Lived in U.S. since 1996 starts--> 
                  
               <div class="control-group">
                  <p>
                     <spring:message code="ssap.page12B.has" text="Has"/>
                     <strong class="nameOfHouseHold"></strong>
                     <spring:message code="ssap.page12B.livedInUSSince" text="lived in the U.S. since 1996?"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                  </p>
                  <div class="margin5-l">
                     <label class="radio" for="62Part_livesInUS">
                        <input type="radio" id="62Part_livesInUS" name="livesInUS" value="true" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#62Part_livesInUSErrorDiv"/>
                        <spring:message code="ssap.page12B.yes" text="Yes"/>
                     </label>
                     <label class="radio" for="62Part_livesInUSNo">
                        <input type="radio" id="62Part_livesInUSNo" name="livesInUS" value="false" />
                        <spring:message code="ssap.page12B.no" text="No"/>
                     </label>
                     <div id="62Part_livesInUSErrorDiv"></div>
                  </div>
               </div><!-- control-group ends -->
               <!--Lived in U.S. since 1996 ends-->

                  
               <!--honorably discharged veteran or active-duty member of the military starts-->                   
               <div class="control-group">
                  <p>
                     <spring:message code="ssap.page12A.Is" text="Is"/>
                     <strong class="nameOfHouseHold"></strong>
                     <spring:message code="ssap.page12B.honourably-veteran" text="an honorably discharged veteran or active-duty member of the military?"/>&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
                  </p>
                  <div class="margin5-l">
                     <label class="radio" for="62Part_honourably-veteran">
                        <input type="radio" id="62Part_honourably-veteran" name="honourably-veteran" value="true" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"  data-parsley-errors-container="#62Part_honourably-veteranErrorDiv"/>
                        <spring:message code="ssap.page12B.yes" text="Yes"/>
                     </label>
                     <label class="radio" for="62Part_honourably-veteranNo">
                        <input type="radio"  id="62Part_honourably-veteranNo" name="honourably-veteran" value="false" />
                        <spring:message code="ssap.page12B.no" text="No"/>
                     </label>
                     <div id="62Part_honourably-veteranErrorDiv"></div>
                  </div>
               </div><!-- control-group ends -->
               <!--honorably discharged veteran or active-duty member of the military ends-->   
               
            </div>
         </div><!-- eligibleImmigrationStatusDiv ends -->
         </div>
      </form>
   </div>
</div>
