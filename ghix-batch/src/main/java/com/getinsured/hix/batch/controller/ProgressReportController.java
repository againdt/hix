package com.getinsured.hix.batch.controller;

import com.getinsured.hix.platform.batch.service.ActiveJobMonitor;
import com.getinsured.hix.platform.batch.service.JobProgress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProgressReportController {
    private static final Logger logger = LoggerFactory.getLogger(ProgressReportController.class);
    private final ActiveJobMonitor activeJobMonitor;

    @Autowired
    public ProgressReportController(ActiveJobMonitor activeJobMonitor) {
        this.activeJobMonitor = activeJobMonitor;
    }

    @RequestMapping(value = "/progress", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<JobProgress> getActiveJobProgress() {
        return activeJobMonitor.getJobProgress();
    }

}
