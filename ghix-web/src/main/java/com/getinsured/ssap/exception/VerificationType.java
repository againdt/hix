package com.getinsured.ssap.exception;

/**
 * Type of verification exceptions.
 *
 * @author Yevgen Golubenko
 * @since 2018-12-21
 */
public enum VerificationType implements ExceptionType
{
  /**
   * No particular verification type.
   */
  NOT_SPECIFIED,

  /**
   * RIDP is not verified
   */
  RIDP
}
