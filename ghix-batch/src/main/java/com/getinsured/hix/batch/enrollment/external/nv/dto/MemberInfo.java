package com.getinsured.hix.batch.enrollment.external.nv.dto;

import java.util.ArrayList;

public class MemberInfo {
	 private String birthDate;
	 private String firstName;
	 private String lastName;
	/* private String ssn;
	 private String gender;
	 ArrayList<String> race = new ArrayList<String>();
	 private String ethnicity;
	 private String writtenLanguageTypeCodeName;
	 private String spokenLanguageTypeCodeName;
	 private ArrayList<String> emailAddress = new ArrayList<String>();
	 private ArrayList<Telephone> telephone = new ArrayList<Telephone>();
	 private ArrayList<Address> address = new ArrayList<Address>();*/


	 // Getter Methods 

	 public String getBirthDate() {
	  return birthDate;
	 }

	 public String getFirstName() {
	  return firstName;
	 }

	 public String getLastName() {
	  return lastName;
	 }

	/* public String getSsn() {
	  return ssn;
	 }

	 public String getGender() {
	  return gender;
	 }

	 public String getEthnicity() {
	  return ethnicity;
	 }

	 public String getWrittenLanguageTypeCodeName() {
	  return writtenLanguageTypeCodeName;
	 }

	 public String getSpokenLanguageTypeCodeName() {
	  return spokenLanguageTypeCodeName;
	 }*/

	 // Setter Methods 

	 public void setBirthDate(String birthDate) {
	  this.birthDate = birthDate;
	 }

	 public void setFirstName(String firstName) {
	  this.firstName = firstName;
	 }

	 public void setLastName(String lastName) {
	  this.lastName = lastName;
	 }

	 /*public void setSsn(String ssn) {
	  this.ssn = ssn;
	 }

	 public void setGender(String gender) {
	  this.gender = gender;
	 }

	 public void setEthnicity(String ethnicity) {
	  this.ethnicity = ethnicity;
	 }

	 public void setWrittenLanguageTypeCodeName(String writtenLanguageTypeCodeName) {
	  this.writtenLanguageTypeCodeName = writtenLanguageTypeCodeName;
	 }

	 public void setSpokenLanguageTypeCodeName(String spokenLanguageTypeCodeName) {
	  this.spokenLanguageTypeCodeName = spokenLanguageTypeCodeName;
	 }

	public ArrayList<String> getRace() {
		return race;
	}

	public void setRace(ArrayList<String> race) {
		this.race = race;
	}

	public ArrayList<String> getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(ArrayList<String> emailAddress) {
		this.emailAddress = emailAddress;
	}

	public ArrayList<Telephone> getTelephone() {
		return telephone;
	}

	public void setTelephone(ArrayList<Telephone> telephone) {
		this.telephone = telephone;
	}

	public ArrayList<Address> getAddress() {
		return address;
	}

	public void setAddress(ArrayList<Address> address) {
		this.address = address;
	}*/
	}
