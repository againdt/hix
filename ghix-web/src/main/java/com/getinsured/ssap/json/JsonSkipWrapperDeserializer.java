package com.getinsured.ssap.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;

/**
 * Deserialization implementation to skip JSON wrapper.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-12
 */
public class JsonSkipWrapperDeserializer extends JsonDeserializer<Object> implements ContextualDeserializer {

  private JavaType unwrappedJavaType;
  private String unwrappedProperty;

  @Override
  public JsonDeserializer<?> createContextual(final DeserializationContext deserializationContext, final BeanProperty beanProperty) throws JsonMappingException {
    unwrappedProperty = beanProperty.getMember().getName();
    unwrappedJavaType = beanProperty.getType();
    return this;
  }

  @Override
  public Object deserialize(final JsonParser jsonParser, final DeserializationContext deserializationContext) throws IOException {
    final TreeNode targetObjectNode = jsonParser.readValueAsTree().get(unwrappedProperty);
    return jsonParser.getCodec().readValue(targetObjectNode.traverse(), unwrappedJavaType);
  }
}
