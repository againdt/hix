package com.getinsured.hix.broker.service.jpa;

import java.math.BigDecimal;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.hix.broker.service.BrokerBOBService;
import com.getinsured.hix.broker.utils.ApplicationStatus;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.broker.BrokerBOBDetailsDTO;
import com.getinsured.hix.dto.broker.BrokerBOBSearchParamsDTO;
import com.getinsured.hix.dto.broker.BrokerBobResponseDTO;
import com.getinsured.hix.dto.broker.HouseholdEligibilityInformationDTO;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.feeds.IssuerInfoDTO;
import com.getinsured.hix.dto.feeds.IssuerListDetailsDTO;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.indportal.IndividualPortalUtility;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.planmgmt.dto.PlanLightResponseDTO;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralUtil;
import com.google.gson.Gson;

@Service("brokerBobService")
public class BrokerBobServiceImpl implements BrokerBOBService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerBobServiceImpl.class);

	private static final String LIKE = "LIKE";
	private static final String N = "No";
	private static final String EQUALS_OPERATOR = "=";
	private static final String FIRST_NAME = "firstName";
	private static final String LAST_NAME = "lastName";
	private static final String APPLICATION_TYPE = "applicationType";
	private static final String BROKER_ID = "brokerId";
	private static final String DESIGNATION_STATUS = "designationStatus";
	private static final String YES = "Y";
	private static final String NOT_APPLICABLE = "N/A";
	private static final String DOCUMENT_URL = "download/document?documentId=";
	private static final String DOLLAR_SYMBOL = "$";
	//private static final String SHOP_FOR_PLANS_LINK =         "/hix/account/user/switchUserRole?switchToModuleName=individual&switchToModuleId=";
	//private static final String SUBMIT_THE_APPLICATION_LINK = "/hix/account/user/switchUserRole?switchToModuleName=individual&switchToModuleId=";
	private static final String SHOP_FOR_PLANS = "Shop for Plans";
	private static final String SUBMIT_THE_APPLICATION = "Submit the Application";
	private static final String INCOMPLETE_APPLICATION = "Incomplete Application";
	private static final String ELIGIBLE_FOR_SHOPPINGS = "Eligible for Shopping";
	private static final String ENROLLED_IN_A_QUALIFIED_HEALTH_PLAN = "Enrolled in a Qualified Plan";
	private static final String APPLICATION_SUBMITTED = "Application Submitted";
	private static final String NO_APPLICATION_FOUND = "No Application Found";
	private static final String CANCELED_APPLICATION = "Canceled Application";
	private static final String UPLOAD_DOCUMENTATION ="Upload Documentation";
	private static final String CLOSED = "Closed";
	private static final String OE = "OE";
	public static final String DATE_FORMAT = "MM/dd/yyyy";
	public static final String DATE_FORMAT_APPLICANT = "MMM dd, yyyy HH:mm:ss.S";
	private static final String UI_DATE_FORMAT = "MMM dd, yyyy";
	private static final String FINANCIAL = "Financial";
	private static final String NON_FINANCIAL = "Non-Financial";
	private static final String INSURANCE_TYPE_HEALTH = "Health";
	private static final String INSURANCE_TYPE_DENTAL = "Dental";
	private static final String QHP = "QHP only";
	private static final String QDP = "QDP only";
	private static final String BOTH = "Both QHP & QDP";
	private static final String Y = "Yes";
	public static final String CITIZENSHIP_IMMIGRATION_STATUS = "citizenshipImmigrationStatus";
	public static final String CITIZENSHIP_STATUS_INDICATOR = "citizenshipStatusIndicator";
	public static final String SINGLE_STREAMLINED_APPLICATION = "singleStreamlinedApplication";
	public static final String HOUSEHOLD_MEMBER = "householdMember";
	public static final String TAX_HOUSEHOLD= "taxHousehold";
	public static final String SSAP_APPLICANT = "ssapApplicants";
	public static final String NAME = "name";
	public static final String DATEOFBIRTH = "dateOfBirth";
	public static final String HOME_ADDRESS = "homeAddress";
	public static final String STATE = "state";
	public static final String MAILING_ADDRESS = "mailingAddress";
	public static final String BLOOD_RELATIONSHIPS =  "bloodRelationship";
	public static final String INDIVIDUAL_PERSONID =  "individualPersonId";
	public static final String RELATED_PERSONID =  "relatedPersonId";
	public static final String RELATION =  "relation";
	public static final String FIRSTNAME = "firstName";
	public static final String LASTNAME = "lastName";
	public static final String GENDER = "gender";
	public static final String HOUSEHOLD_CONTACT = "householdContact";
	public static final String SOCIAL_SECURITY_CARD = "socialSecurityCard";
	public static final String SOCIAL_SECURITY_NUMBER = "socialSecurityNumber";
	public static final String SEEKING_COVERAGE = "applyingForCoverageIndicator";
	public static final String FIRST_NAME_ON_SSN_CARD = "firstNameOnSSNCard";
	public static final String LAST_NAME_ON_SSN_CARD = "lastNameOnSSNCard";
	public static final String PERSONID = "personId";
	public static final String TRUE = "true";
	public static final String NAME_ON_SSN_CARD_INDICATOR = "nameSameOnSSNCardIndicator";
	public static final Integer PAGE_SIZE = 10;
	private static final String CURRENT_STATUS = "currentStatus";
	private static final String DUE_DATE = "dueDate";
	private static final String NEXT_STEP = "nextStep";
	private static final String ISSUER = "issuer";
	private static final String THIS_WEEK = "This Week";
	private static final String NEXT_WEEK = "Next Week";
	private static final String FIRST_NAME_ASC = "FIRST_NAME_ASC";
	private static final String FIRST_NAME_DESC = "FIRST_NAME_DESC";
	private static final String LAST_NAME_ASC = "LAST_NAME_ASC";
	private static final String LAST_NAME_DESC = "LAST_NAME_DESC";
	private static final String DUE_DATE_DESC = "DUE_DATE_DESC";
	private static final String COVERAGE_YEAR = "coverageYear";
	private static final String ALL =  "ALL";
	private static final String HEALTH =  "HEALTH";
	private static final String DENTAL =  "DENTAL";
	private static final String NO_COST_SHARING_AVAILABLE = "No cost sharing available";
	private static final String ZERO_COST_SHARING_PLAN = "Zero Cost Sharing Plan";
	private static final String LIMITED_COST_SHARING_PLAN = "Limited Cost Sharing Plan";
	private static final String SEVENTY_THREE_PERCENT_AV_PLAN = "73% AV Plan";
	private static final String EIGHTY_SEVEN_PERCENT_AV_PLAN = "87% AV Plan";
	private static final String NINTY_FOUR_PERCENT_AV_PLAN = "94% AV Plan";
	private static final String CS0 = "CS0";
	private static final String CS1 = "CS1";
	private static final String CS2 = "CS2";
	private static final String CS3 = "CS3";
	private static final String CS4 = "CS4";
	private static final String CS5 = "CS5";
	private static final String CS6 = "CS6";
	private static final String SSAP_APPLICATION_ID = "ssapApplicationId";
	private static final String SSAP_APPLICANT_ID = "ssapApplicantId";
	public static final String FALSE = "false";
	public static final String CSRELIGIBILITYTYPE = "CSREligibilityType";
	public static final String APTCELIGIBILITYTYPE = "APTCEligibilityType";
	public static final String ELIGIBILITYTYPE_TRUE = "TRUE";
	public static final String ELIGIBILITYTYPE_FALSE = "FALSE";
	private static final String ELIGIBILITY_STATUS_QHP = "QHP";
	private static final String ELIGIBILITY_STATUS_NONE = "NONE";
	private static final String ELIGIBILE = "Eligible";
	private static final String NOT_ELIGIBLE = "Not Eligible";
	private static final String CONDITIONAL = "Conditional";
	private static final String PENDING = "Pending";
	private static final String AE = "AE";
	private static final String AM = "AM";
	private static final String PE = "PE";
	private static final String AX = "AX";
	private static final String CA = "CA";
	private static final String CAM = "CAM";
	private static final String CAX = "CAX";
	private static final String CAE = "CAE";
	private static final String DE = "DE";
	private static final String NO_ACTIVE_ENROLLMENT = "No Active Enrollment";
	private static final String REPORT_A_CHANGE = "Report A Change";
	private static final String ENROLL_DURING_OEP = "Enroll During OEP";

	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired
	private PlanMgmtService planMgmtService;

	@Autowired
	IndividualPortalUtility individualPortalUtility;
	@Autowired
	SsapApplicationRepository ssapApplicationRepository;
	@Autowired private Gson platformGson;
	@Value("#{configProp['appUrl']}")
	private String appUrl;

	public SsapApplication getSsapApplication(Long applicationId){
		return ssapApplicationRepository.findOne(applicationId);
	}

	@Override
	public HouseholdEligibilityInformationDTO getHouseholdMemberInformation(
			BrokerBOBDetailsDTO requestDTO) throws Exception {


		HouseholdEligibilityInformationDTO hhEligibilityInfoDTO;

		try {

			hhEligibilityInfoDTO = new HouseholdEligibilityInformationDTO();
			hhEligibilityInfoDTO.setFirstName(requestDTO.getFirstName());
			hhEligibilityInfoDTO.setLastName(requestDTO.getLastName());

			String applicationJSON = requestDTO.getApplicationData();
			JSONObject jsonObj = new JSONObject(applicationJSON);
			JSONObject ssapJsonObj = jsonObj.getJSONObject(SINGLE_STREAMLINED_APPLICATION);
			JSONArray taxHouseHold = ssapJsonObj.getJSONArray(TAX_HOUSEHOLD);
			JSONArray householdMembers = ((JSONArray) ((JSONObject) taxHouseHold.get(0)).get(HOUSEHOLD_MEMBER));
			JSONObject householdMember = null;
			SsapApplicantDTO ssapApplicantDTO = null;
			List<SsapApplicantDTO> sspApplicants = new ArrayList<SsapApplicantDTO>();

			Map<String,BloodRelationship> bloodRelationShip =  bloodRelationshipMap(householdMembers);

			for (int i = 0; i < householdMembers.length(); i++) {

				ssapApplicantDTO = new SsapApplicantDTO();
				householdMember = (JSONObject) householdMembers.get(i);

					if (householdMember != null) {

						getApplicantsName(householdMember, ssapApplicantDTO);
						getApplicantDateOfBirth(householdMember, ssapApplicantDTO);
						getApplicantRelationShip(bloodRelationShip,householdMember, ssapApplicantDTO);
						getApplicantsGender(householdMember, ssapApplicantDTO);
						getSSNInformation(householdMember, ssapApplicantDTO);
						getAddress(householdMember, ssapApplicantDTO);
						getSeekingCoverage(householdMember, ssapApplicantDTO);
						getCitizenshipIndicator(householdMember, ssapApplicantDTO);

						sspApplicants.add(ssapApplicantDTO);

						}
					}
			hhEligibilityInfoDTO.setApplicantEligibility(sspApplicants);

		}
		catch (Exception exception) {
			LOGGER.error("Exception while fetching Household Eligibility Information : " + exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return hhEligibilityInfoDTO;
	}

	private Map<String,BloodRelationship> bloodRelationshipMap(JSONArray householdMembers) throws JSONException {

		Map<String,BloodRelationship> bloodRelationshipMap = new HashMap<String,BloodRelationship> ();
		//JSONArray bloodRelationshipJSONList = householdMembers.getJSONArray(BLOOD_RELATIONSHIPS);
		final int arraySize = ReferralUtil.sizeForJSONArray(householdMembers);

		JSONObject bloodRelationshipJSON;
		BloodRelationship bloodRelationship = null;
		JSONArray bloodRelationshipArray = null;
		for (int i = 0; i < arraySize; i++) {

			bloodRelationshipJSON = (JSONObject) householdMembers.get(i);

			if(bloodRelationshipJSON.get(BLOOD_RELATIONSHIPS) != null) {
				JSONObject bloodRelationshipJSONObj = null;
				bloodRelationshipArray = bloodRelationshipJSON.getJSONArray(BLOOD_RELATIONSHIPS);
				for(int bloodrelation=0; bloodrelation < bloodRelationshipArray.length();bloodrelation++  )
				{

						bloodRelationshipJSONObj = (JSONObject)bloodRelationshipArray.get(bloodrelation);
						String individualPersonId = bloodRelationshipJSONObj.get(INDIVIDUAL_PERSONID).toString();
						String relationId = bloodRelationshipJSONObj.get(RELATED_PERSONID).toString();
						if(relationId.equals("1"))
						{
							bloodRelationship = new BloodRelationship();

							bloodRelationship.setIndividualPersonId(individualPersonId);
							bloodRelationship.setRelatedPersonId(relationId);
							bloodRelationship.setRelation(bloodRelationshipJSONObj.get(RELATION).toString());
							bloodRelationshipMap.put(individualPersonId, bloodRelationship);
					}
				}
			}
		}
		return bloodRelationshipMap;
	}

	private Location address(JSONObject jaddress) {

		Location location = new Location();
		try {
			location.setAddress1(jaddress.get("streetAddress1").toString());
			location.setAddress2(jaddress.get("streetAddress2").toString());
			location.setCity(jaddress.get("city").toString());
			location.setState(jaddress.get("state").toString());
			location.setZip(jaddress.get("postalCode").toString());
			location.setCounty(jaddress.get("county").toString());
		} catch (JSONException e) {

		}
		return location;
	}



	private static void getCitizenshipIndicator(JSONObject householdMember, SsapApplicantDTO ssapApplicantDTO) throws JSONException
	{
		JSONObject citizenShipImmigrationStatus = (JSONObject) householdMember.get(CITIZENSHIP_IMMIGRATION_STATUS);
		if (citizenShipImmigrationStatus != null) {
			String citizenship = citizenShipImmigrationStatus.get(CITIZENSHIP_STATUS_INDICATOR).toString();
			if(citizenship.equals(TRUE))
			{
				citizenship = Y;
			}
			else
			{
				citizenship = N;
			}

			ssapApplicantDTO.setCitizenShipStatus(citizenship);
		}
	}

	private void getApplicantsName(JSONObject householdMember, SsapApplicantDTO ssapApplicantDTO) throws JSONException
	{
		JSONObject applicantsName = (JSONObject) householdMember.get(NAME);
		if (applicantsName != null) {

			ssapApplicantDTO.setFirstName(applicantsName.get(FIRSTNAME).toString());
			ssapApplicantDTO.setLastName(applicantsName.get(LASTNAME).toString());
		}
	}

	private  void getApplicantsGender(JSONObject householdMember, SsapApplicantDTO ssapApplicantDTO) throws JSONException
	{
		String gender =  householdMember.get(GENDER).toString();
		if (gender != null) {

			ssapApplicantDTO.setGender(gender);
		}
	}

	private  void getApplicantDateOfBirth(JSONObject householdMember, SsapApplicantDTO ssapApplicantDTO) throws JSONException
	{
		String dateOfBirth  = householdMember.get(DATEOFBIRTH).toString();
		if (dateOfBirth != null) {

		try {
				ssapApplicantDTO.setBirthDateString(dateOfBirth.substring(0, 13));
			}  catch(Exception exception) {
				throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}
		}
	}
	private void getSSNInformation(JSONObject householdMember, SsapApplicantDTO ssapApplicantDTO) throws JSONException
	{
		JSONObject applicantsSSN = (JSONObject) householdMember.get(SOCIAL_SECURITY_CARD);
		if (applicantsSSN != null) {

			String ssn = applicantsSSN.get(SOCIAL_SECURITY_NUMBER).toString();
			int len = ssn.length();
			StringBuffer sb = new StringBuffer();
			if((applicantsSSN.get(NAME_ON_SSN_CARD_INDICATOR).toString()).equals(TRUE))
			{

				sb.append(ssapApplicantDTO.getFirstName());
				sb.append(" ");
				sb.append(ssapApplicantDTO.getLastName());
			}
			else
			{
				sb.append(applicantsSSN.get(FIRST_NAME_ON_SSN_CARD).toString());
				sb.append(" ");
				sb.append(applicantsSSN.get(LAST_NAME_ON_SSN_CARD).toString());
			}
			if( (ssn != null) && !(ssn.equals("")) && (len > 0 ))
			{
				sb.append("\n");
				sb.append(ssn.substring((len-4)));
			}
			ssapApplicantDTO.setSsn(sb.toString());
		}
	}
	private void getAddress(JSONObject householdMember, SsapApplicantDTO ssapApplicantDTO) throws JSONException
	{


		JSONObject householdContact = (JSONObject) householdMember.get(HOUSEHOLD_CONTACT);

		JSONObject homeAddress = (JSONObject) householdContact.get(HOME_ADDRESS);

		JSONObject mailingAddress = (JSONObject) householdContact.get(MAILING_ADDRESS);

		if (mailingAddress != null) {
			ssapApplicantDTO.setMailingLocation(address(mailingAddress));
		}

		if (homeAddress != null) {
			ssapApplicantDTO.setHomeAddress(address(homeAddress));

		}
	}

	private void getSeekingCoverage(JSONObject householdMember, SsapApplicantDTO ssapApplicantDTO){

		try {
			String seekingCoverage = householdMember.get(SEEKING_COVERAGE).toString();

			if(seekingCoverage.equals(TRUE))
			{
				seekingCoverage = Y;
			}
			else
			{
				seekingCoverage = N;
			}
			ssapApplicantDTO.setSeekingCoverage(seekingCoverage);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private void getApplicantRelationShip(Map<String,BloodRelationship> bloodRelationShipMap,JSONObject householdMember, SsapApplicantDTO ssapApplicantDTO)
			throws JSONException
	{

		String personId  = householdMember.get(PERSONID).toString();
		BloodRelationship bloodRelationship = bloodRelationShipMap.get(personId);

		ssapApplicantDTO.setRelationship(getRelationShipCode(bloodRelationship.getRelation()));

	}

	private String getRelationShipCode(String relationShipCode)
	{
		String relation = null;
		 if (relationShipCode.equals("01")){
	            relation = "Spouse";
	          } else if(relationShipCode.equals("03")){
	            relation =  "Parent of child";
	          }else if(relationShipCode.equals("03a")){
	            relation =  "Parent of adopted child";
	          }else if(relationShipCode.equals("03f")){
	            relation =  "Parent of foster child";
	          }else if(relationShipCode.equals("03w")){
	            relation =  "Parent/Caretaker of the ward";
	          }else if(relationShipCode.equals("04")){
	            relation =  "Grandparent";
	          }else if(relationShipCode.equals("05")){
	            relation =  "Grandchild";
	          }else if(relationShipCode.equals("06")){
	            relation =  "Uncle/aunt";
	          }else if(relationShipCode.equals("07")){
	            relation =  "Nephew/niece";
	          }else if(relationShipCode.equals("08")){
	            relation =  "First cousin";
	          }else if(relationShipCode.equals("09")){
	            relation =  "Adopted child (son or daughter)";
	          }else if(relationShipCode.equals("10")){
	            relation =  "Foster child (foster son or foster daughter)";
	          }else if(relationShipCode.equals("11")){
	            relation =  "Son-in-law or daughter-in-law";
	          }else if(relationShipCode.equals("12")){
	            relation =  "Brother-in-law or sister-in-law";
	          }else if(relationShipCode.equals("13")){
	            relation =  "Mother-in-law or father-in law";
	          }else if(relationShipCode.equals("14")){
	            relation =  "Sibling (brother or sister)";
	          }else if(relationShipCode.equals("15")){
	            relation =  "Ward of guardian";
	          }else if(relationShipCode.equals("15c")){
	            relation =  "Ward of court-appointed guardian";
	          }else if(relationShipCode.equals("15p")){
	            relation =  "Ward of parent/caretaker";
	          }else if(relationShipCode.equals("16")){
	            relation =  "Stepparent (stepfather or stepmother)";
	          }else if(relationShipCode.equals("17")){
	            relation =  "Stepchild (stepson or stepdaughter)";
	          }else if(relationShipCode.equals("18")){
	            relation =  "Self";
	          }else if(relationShipCode.equals("19")){
	            relation =  "Child (son or daughter)";
	          }else if(relationShipCode.equals("23")){
	            relation =  "Sponsored dependent";
	          }else if(relationShipCode.equals("24")){
	            relation =  "Dependent of a minor dependent";
	          }else if(relationShipCode.equals("25")){
	            relation =  "Former spouse";
	          }else if(relationShipCode.equals("26")){
	            relation =  "Guardian";
	          }else if(relationShipCode.equals("31")){
	            relation =  "Court-appointed guardian";
	          }else if(relationShipCode.equals("38")){
	            relation =  "Collateral dependent";
	          }else if(relationShipCode.equals("53")){
	            relation =  "Domestic partner";
	          }else if(relationShipCode.equals("60")){
	            relation =  "Annuitant";
	          }else if(relationShipCode.equals("D2")){
	            relation =  "Trustee";
	          }else if(relationShipCode.equals("G8")){
	            relation =  "Unspecified relationship";
	          }else if(relationShipCode.equals("G9")){
	            relation =  "Unspecified relative";
	          }else if(relationShipCode.equals("03-53")){
	            relation =  "Parent's domestic partner";
	          }else if(relationShipCode.equals("53-19")){
	              relation =  "Child of domestic partner";
	          }
		 return relation;

	}

	public HouseholdEligibilityInformationDTO getHouseholdEligibilityInformation(BrokerBOBDetailsDTO brokerBOBDetailsDTO) throws Exception {


		HouseholdEligibilityInformationDTO hhEligibilityInfoDTO;
		try {
			hhEligibilityInfoDTO = new HouseholdEligibilityInformationDTO();
			hhEligibilityInfoDTO.setFirstName(brokerBOBDetailsDTO.getFirstName());
			hhEligibilityInfoDTO.setLastName(brokerBOBDetailsDTO.getLastName());

			String eligibityStatus = brokerBOBDetailsDTO.getEligibilityStatus();
			if(brokerBOBDetailsDTO.getEligibilityStatus() != null)
			{
				setEligibilityStatus(eligibityStatus,hhEligibilityInfoDTO);
			}
			if(brokerBOBDetailsDTO.getAptcAmount()!=null && !"null".equalsIgnoreCase(brokerBOBDetailsDTO.getAptcAmount()))
			{
				hhEligibilityInfoDTO.setAdvancedPremiumTaxCredit(new Double(brokerBOBDetailsDTO.getAptcAmount()));
			}

			if(StringUtils.isNotBlank(brokerBOBDetailsDTO.getCsrLevel()))
			{
				if(brokerBOBDetailsDTO.getCsrLevel().equalsIgnoreCase(CS0))
				{
					hhEligibilityInfoDTO.setCostSharingReduction(NO_COST_SHARING_AVAILABLE);
				}else if(brokerBOBDetailsDTO.getCsrLevel().equalsIgnoreCase(CS1))
				{
					hhEligibilityInfoDTO.setCostSharingReduction(NO_COST_SHARING_AVAILABLE);
				}
				else if(brokerBOBDetailsDTO.getCsrLevel().equalsIgnoreCase(CS2))
				{
					hhEligibilityInfoDTO.setCostSharingReduction(ZERO_COST_SHARING_PLAN);
				}
				else if(brokerBOBDetailsDTO.getCsrLevel().equalsIgnoreCase(CS3))
				{
					hhEligibilityInfoDTO.setCostSharingReduction(LIMITED_COST_SHARING_PLAN);
				}
				else if(brokerBOBDetailsDTO.getCsrLevel().equalsIgnoreCase(CS4))
				{
					hhEligibilityInfoDTO.setCostSharingReduction(SEVENTY_THREE_PERCENT_AV_PLAN);
				}
				else if(brokerBOBDetailsDTO.getCsrLevel().equalsIgnoreCase(CS5))
				{
					hhEligibilityInfoDTO.setCostSharingReduction(EIGHTY_SEVEN_PERCENT_AV_PLAN);
				}
				else if(brokerBOBDetailsDTO.getCsrLevel().equalsIgnoreCase(CS6))
				{
					hhEligibilityInfoDTO.setCostSharingReduction(NINTY_FOUR_PERCENT_AV_PLAN);
				}
			}
			else
			{
				hhEligibilityInfoDTO.setCostSharingReduction(NO_COST_SHARING_AVAILABLE);
			}


			// Get Applicants details
			if(brokerBOBDetailsDTO != null && brokerBOBDetailsDTO.getSsapApplicationId() != 0)
			{
				getApplicantDetails(hhEligibilityInfoDTO,brokerBOBDetailsDTO.getSsapApplicationId());
			}

			//hhEligibilityInfoDTO.setApplicantEligibility(ssapApplicants);

		}
		catch (Exception exception) {
			LOGGER.error("Exception while fetching Household Eligibility Information : " + exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return hhEligibilityInfoDTO;
	}

	private void setEligibilityStatus(String eligibityStatus,HouseholdEligibilityInformationDTO hhEligibilityInfoDTO)
	{

		if(eligibityStatus.equalsIgnoreCase(AE))
		{
			hhEligibilityInfoDTO.setEligibilityStatus(ELIGIBILE);
		}
		else if(eligibityStatus.equalsIgnoreCase(AM))
		{
			hhEligibilityInfoDTO.setEligibilityStatus(ELIGIBILE);
		}
		else if(eligibityStatus.equalsIgnoreCase(PE))
		{
			hhEligibilityInfoDTO.setEligibilityStatus(PENDING);
		}
		else if(eligibityStatus.equalsIgnoreCase(AX))
		{
			hhEligibilityInfoDTO.setEligibilityStatus(ELIGIBILE);
		}
		else if(eligibityStatus.equalsIgnoreCase(CA))
		{
			hhEligibilityInfoDTO.setEligibilityStatus(CONDITIONAL);
		}
		else if(eligibityStatus.equalsIgnoreCase(CAM))
		{
			hhEligibilityInfoDTO.setEligibilityStatus(CONDITIONAL);
		}
		else if(eligibityStatus.equalsIgnoreCase(CAX))
		{
			hhEligibilityInfoDTO.setEligibilityStatus(CONDITIONAL);
		}
		else if(eligibityStatus.equalsIgnoreCase(CAE))
		{
			hhEligibilityInfoDTO.setEligibilityStatus(CONDITIONAL);
		}
		else if(eligibityStatus.equalsIgnoreCase(DE))
		{
			hhEligibilityInfoDTO.setEligibilityStatus(NOT_ELIGIBLE);
		}

	}

	private void getApplicantDetails(HouseholdEligibilityInformationDTO hhEligibilityInfoDTO,int ssapApplicationId)
	{
		EntityManager em = null;
		Query query = null;
		StringBuilder queryString = new StringBuilder();

		try {
			em = entityManagerFactory.createEntityManager();
			queryString.append("select ssapplicants.FIRST_NAME ,ssapplicants.LAST_NAME ,"
					+ "ssapplicants.ELIGIBILITY_STATUS, ssapplicants.ID "
					+ " from ssap_applications ssaplication "
					+ "inner join ssap_applicants ssapplicants on ssapplicants.SSAP_APPLICATION_ID = ssaplication.ID "
					+ "and ssapplicants.SSAP_APPLICATION_ID=:"+SSAP_APPLICATION_ID);

			query = em.createNativeQuery(queryString.toString());
			setQueryParameter(query, SSAP_APPLICATION_ID, ssapApplicationId);
			List<?> result = query.getResultList();
			Iterator<?> iterator = result.iterator();
			SsapApplicantDTO ssapApplicantDTO = null;
			List<SsapApplicantDTO> ssapApplicants = new ArrayList<SsapApplicantDTO>();
			while (iterator.hasNext()) {

				Object[] objArray = (Object[]) iterator.next();
				ssapApplicantDTO = new SsapApplicantDTO();
				if (objArray[0] != null) {
					ssapApplicantDTO.setFirstName(objArray[0].toString());
				}
				if (objArray[1] != null) {
					ssapApplicantDTO.setLastName(objArray[1].toString());
				}
				if (objArray[2] != null) {
					if(objArray[2].toString().equalsIgnoreCase(ELIGIBILITY_STATUS_QHP))
					{
						ssapApplicantDTO.setEligibilityStatus(Y);
					}
					else if((objArray[2].toString().equalsIgnoreCase(ELIGIBILITY_STATUS_NONE)))
					{
						ssapApplicantDTO.setEligibilityStatus(N);
					}
				}
				else {
					ssapApplicantDTO.setEligibilityStatus(N);
				}
				String ssapApplicantId = objArray[3].toString();

				queryString = new StringBuilder();
				queryString.append("select pe.ELIGIBILITY_TYPE,pe.ELIGIBILITY_INDICATOR from program_eligibilities pe "
						+ "inner join ssap_applicants ssapplicants on pe.SSAP_APPLICANT_ID = ssapplicants.ID "
						+ "and pe.SSAP_APPLICANT_ID =:"+SSAP_APPLICANT_ID);

				query = em.createNativeQuery(queryString.toString());
				setQueryParameter(query, SSAP_APPLICANT_ID,  Integer.parseInt(ssapApplicantId));
				List<?> res = query.getResultList();
				Iterator<?> it = res.iterator();
				while (it.hasNext()) {
					Object[] objAr = (Object[]) it.next();

					if (objAr[0] != null && objAr[1] != null) {
						if(objAr[0].equals(CSRELIGIBILITYTYPE) && objAr[1].equals(ELIGIBILITYTYPE_TRUE))
						{
							ssapApplicantDTO.setCostSharingReduction(Y);
						}
						else if(objAr[0].equals(CSRELIGIBILITYTYPE) && objAr[1].equals(ELIGIBILITYTYPE_FALSE))
						{
							ssapApplicantDTO.setCostSharingReduction(N);
						}
						if(objAr[0].equals(APTCELIGIBILITYTYPE) && objAr[1].equals(ELIGIBILITYTYPE_TRUE))
						{
							ssapApplicantDTO.setAdvancedPremiumTaxCredit(Y);
						}
						else if(objAr[0].equals(APTCELIGIBILITYTYPE) && objAr[1].equals(ELIGIBILITYTYPE_FALSE))
						{
							ssapApplicantDTO.setAdvancedPremiumTaxCredit(N);
						}
					}
				}
				ssapApplicants.add(ssapApplicantDTO);
			}

			hhEligibilityInfoDTO.setApplicantEligibility(ssapApplicants);

		} catch (Exception exception) {

				LOGGER.error("Exception while fetching Broker BOB details : " + exception);
				throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} finally {
				if (em != null && em.isOpen()) {
					em.clear();
					em.close();
				}
		}

	}

	public BrokerBobResponseDTO getBrokerBookOfBusiness(BrokerBOBDetailsDTO requestDTO, BrokerBOBSearchParamsDTO bobSearchParamsDTO, boolean skipPagination) throws Exception {

		EntityManager em = null;
		Query query = null;
		StringBuilder queryString = new StringBuilder();
		BrokerBOBDetailsDTO brokerBOBDetailsDTO;
		String firstName = bobSearchParamsDTO.getFirstName();
		String lastName = bobSearchParamsDTO.getLastName();
		String designationStatus = requestDTO.getDesignationStatus();
		String coverageYear = bobSearchParamsDTO.getCoverageYear();
		Integer brokerId = requestDTO.getBrokerId();
		List<BrokerBOBDetailsDTO> listOfBrokerBOBDetailsDTO = new ArrayList<>();
		BrokerBobResponseDTO brokerBobResponseDTO = new BrokerBobResponseDTO();
		SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yy");

		try {
			em = entityManagerFactory.createEntityManager();

			queryString.append("	SELECT H.ID,	\n");
			queryString.append("	    H.FIRST_NAME,	\n");
			queryString.append("	    H.LAST_NAME,	\n");
			queryString.append("	    H.PHONE_NUMBER,	\n");
			queryString.append("	    H.EMAIL_ADDRESS,	\n");
			queryString.append("	    L.ADDRESS1,	\n");
			queryString.append("	    L.ADDRESS2,	\n");
			queryString.append("	    L.CITY,	\n");
			queryString.append("	    L.STATE,	\n");
			queryString.append("	    L.COUNTY,	\n");
			queryString.append("	    L.ZIP,	\n");
			queryString.append("	    S.APPLICATION_TYPE,	\n");
			queryString.append("	    S.APPLICATION_STATUS,	\n");
			queryString.append("	    S.SSAP_APPLICATION_ID AS SSAP_APPLICATION_ID,	\n");
			queryString.append("	    S.FINANCIAL_ASSISTANCE_FLAG,	\n");
			queryString.append("	    TO_CHAR(SAE.ENROLLMENT_END_DATE, 'MM/dd/yyyy') ENROLLMENT_END_DATE,	\n");
			queryString.append("	    D.SHOW_SWITCH_ROLE_POPUP,	\n");
			queryString.append("	    S.ELIGIBILITY_STATUS,	\n");
			queryString.append("	    S.EXCHANGE_ELIGIBILITY_STATUS,	\n");
			queryString.append("	    S.MAXIMUM_APTC,	\n");
			queryString.append("	    cast('' as  varchar ) as APPLICATION_DATA,	\n");
			queryString.append("	    S.CASE_NUMBER,	\n");
			queryString.append("	    S.COVERAGE_YEAR,	\n");
			queryString.append("	    H.USER_ID,	\n");
			queryString.append("	    S.CSR_LEVEL,	\n");
			queryString.append("	    S.CREATION_TIMESTAMP,	\n");
			queryString.append("	    S.VALIDATION_STATUS	\n");
			queryString.append("	  FROM CMR_HOUSEHOLD H	\n");
			queryString.append("	  INNER JOIN DESIGNATE_BROKER D	\n");
			queryString.append("	  ON H.ID                              =D.INDIVIDUALID	\n");
			queryString.append("	  AND D.BROKERID                       =:"+BROKER_ID+"	\n");
			queryString.append("	  AND D.STATUS                         =:"+DESIGNATION_STATUS+"	\n");
			queryString.append("	  LEFT OUTER JOIN LOCATIONS L on L.ID=H.CONTACT_LOCATION_ID	\n");
			queryString.append("	  LEFT OUTER JOIN	\n");
			queryString.append("	    (SELECT id AS ssap_application_id,	\n");
			queryString.append("	      application_status,	\n");
			queryString.append("	      cmr_houseold_id,	\n");
			queryString.append("	      APPLICATION_TYPE,	\n");
			queryString.append("	      VALIDATION_STATUS, \n");
			queryString.append("	      FINANCIAL_ASSISTANCE_FLAG,	\n");
			queryString.append("	      ELIGIBILITY_STATUS,	\n");
			queryString.append("	      EXCHANGE_ELIGIBILITY_STATUS,	\n");
			queryString.append("	      MAXIMUM_APTC,	\n");
			queryString.append("	      cast('' as  varchar ) as APPLICATION_DATA,	\n");
			queryString.append("	      CASE_NUMBER,	\n");
			queryString.append("	      COVERAGE_YEAR,	\n");
			queryString.append("	      CSR_LEVEL,	\n");
			queryString.append("	      CREATION_TIMESTAMP \n");
			queryString.append("	    FROM	\n");
			queryString.append("	      (	\n");
			queryString.append("	      SELECT id ,	\n");
			queryString.append("	      application_status,	\n");
			queryString.append("	      cmr_houseold_id,	\n");
			queryString.append("	      APPLICATION_TYPE,	\n");
			queryString.append("	      VALIDATION_STATUS, \n");
			queryString.append("	      FINANCIAL_ASSISTANCE_FLAG,	\n");
			queryString.append("	      ELIGIBILITY_STATUS,	\n");
			queryString.append("	      EXCHANGE_ELIGIBILITY_STATUS,	\n");
			queryString.append("	      MAXIMUM_APTC,	\n");
			queryString.append("	      cast('' as  varchar ) as APPLICATION_DATA,	\n");
			queryString.append("	      CASE_NUMBER,	\n");
			queryString.append("	      COVERAGE_YEAR,	\n");
			queryString.append("	      CSR_LEVEL, 	\n");
			queryString.append("	      CREATION_TIMESTAMP \n");
			
			queryString.append("	    FROM	\n");
			queryString.append("	      SSAP_APPLICATIONS	\n");
			queryString.append("	     WHERE id IN	\n");
			queryString.append("	        (SELECT aa.id	\n");
			queryString.append("	        FROM SSAP_APPLICATIONS aa,  DESIGNATE_BROKER dd	\n");
			queryString.append("	        WHERE	\n");
			queryString.append("	         aa.CMR_HOUSEOLD_ID=dd.INDIVIDUALID	\n");
			queryString.append("	           AND dd.BROKERID                       =:"+BROKER_ID+"	\n");
			queryString.append("	           AND dd.STATUS                         =:"+DESIGNATION_STATUS+"	\n");
			queryString.append("	           AND aa.application_status IN ('EN', 'ER', 'OP','UC')	\n");
			queryString.append("	        )	\n");
			queryString.append("	       OR id IN	\n");
			queryString.append("	        (SELECT id	\n");
			queryString.append("	         FROM	\n");
			queryString.append("	          (SELECT aa.id,	\n");
			queryString.append("	            aa.CMR_HOUSEOLD_ID,	\n");
			queryString.append("	            aa.creation_timestamp,	\n");
			queryString.append("	            rank() over (partition BY aa.CMR_HOUSEOLD_ID order by aa.creation_timestamp DESC) r	\n");
			queryString.append("	          FROM SSAP_APPLICATIONS aa, DESIGNATE_BROKER dd	\n");
			queryString.append("	          where	\n");
			queryString.append("	          aa.CMR_HOUSEOLD_ID=dd.INDIVIDUALID	\n");
			queryString.append("	           AND dd.BROKERID                       =:"+BROKER_ID+"	\n");
			queryString.append("	           AND dd.STATUS                         =:"+DESIGNATION_STATUS+"	\n");
			queryString.append("	           AND aa.application_status in ('CC', 'CL') \n");
			queryString.append("	           AND   (  select count(*)  from SSAP_APPLICATIONS xyz      WHERE  aa.CMR_HOUSEOLD_ID = xyz.CMR_HOUSEOLD_ID  AND xyz.application_status   IN ('EN', 'ER', 'OP','UC') ) = 0 \n");
			queryString.append("	          )	SUBQ5   \n");
			queryString.append("	        WHERE r = 1	\n");
			queryString.append("	        )	\n");
			queryString.append("	      ) SUBQ6	\n");
			//queryString.append("	      --where coverage_year = 2016	\n");
			queryString.append("	    ) S	\n");
			queryString.append("	  ON H.ID=S.CMR_HOUSEOLD_ID	\n");
			queryString.append("	  LEFT OUTER JOIN SSAP_APPLICATION_EVENTS SAE	\n");
			queryString.append("	  ON S.ssap_application_id=SAE.SSAP_APPLICATION_ID	\n");


			if(StringUtils.isNotBlank(firstName) || StringUtils.isNotBlank(lastName) || StringUtils.isNotBlank(coverageYear)){
				StringBuilder searchParams = new StringBuilder();
				searchParams.append("WHERE");
				appendToQuery(searchParams, "FIRST_NAME", firstName, FIRST_NAME, LIKE, true);
				appendToQuery(searchParams, "LAST_NAME", lastName, LAST_NAME, LIKE, true);
				appendToQuery(searchParams, "COVERAGE_YEAR", coverageYear, COVERAGE_YEAR, EQUALS_OPERATOR, false);

				queryString.append(searchParams);
			}

			query = em.createNativeQuery(queryString.toString());

			setQueryParameter(query, BROKER_ID, brokerId);
			setQueryParameter(query, DESIGNATION_STATUS, designationStatus, false);
			setQueryParameter(query, FIRST_NAME, firstName, true);
			setQueryParameter(query, LAST_NAME, lastName, true);
			if (coverageYear != null && !"".equals(coverageYear.trim())) {
				setQueryParameter(query, COVERAGE_YEAR, Integer.parseInt(coverageYear.trim()) );	
			}
			
			long currentNanoTime = System.nanoTime();
			LOGGER.info("Main Query execution Started at " + currentNanoTime);
			List<?> result = query.getResultList();
			LOGGER.info("Main Query execution took " + (System.nanoTime() - currentNanoTime));

			Iterator<?> iterator = result.iterator();

			currentNanoTime = System.nanoTime();
			LOGGER.info("Loop through Main Query Started at " + currentNanoTime);
			while (iterator.hasNext()) {
				brokerBOBDetailsDTO = new BrokerBOBDetailsDTO();
				Object[] objArray = (Object[]) iterator.next();

				if (objArray[0] != null) {
					brokerBOBDetailsDTO.setIndividualId(Integer.valueOf(objArray[0].toString()));
					brokerBOBDetailsDTO.setEncryptedIndividualId(ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[0].toString()));
				}
				if (objArray[1] != null) {
					brokerBOBDetailsDTO.setFirstName(objArray[1].toString());
				}
				if (objArray[2] != null) {
					brokerBOBDetailsDTO.setLastName(objArray[2].toString());
				}
				if (objArray[3] != null) {
					brokerBOBDetailsDTO.setPhoneNumber(objArray[3].toString());
				}
				if (objArray[4] != null) {
					brokerBOBDetailsDTO.setEmailAddress(objArray[4].toString());
				}
				if (objArray[5] != null) {
					brokerBOBDetailsDTO.setAddress1(objArray[5].toString());
				}
				if (objArray[6] != null) {
					brokerBOBDetailsDTO.setAddress2(objArray[6].toString());
				}
				if (objArray[7] != null) {
					brokerBOBDetailsDTO.setCity(objArray[7].toString());
				}
				if (objArray[8] != null) {
					brokerBOBDetailsDTO.setState(objArray[8].toString());
				}
				if (objArray[9] != null) {
					brokerBOBDetailsDTO.setCounty(objArray[9].toString());
				}
				if (objArray[10] != null) {
					brokerBOBDetailsDTO.setZipCode(objArray[10].toString());
				}
				if (objArray[11] != null) {
					brokerBOBDetailsDTO.setApplicationType(objArray[11].toString());
				}
				if (objArray[12] != null) {
					brokerBOBDetailsDTO.setApplicationStatus(objArray[12].toString());
				}
				if (objArray[13] != null) {
					brokerBOBDetailsDTO.setSsapApplicationId(Integer.valueOf(objArray[13].toString()));
				}
				if (objArray[14] != null) {
					brokerBOBDetailsDTO.setFinancialAssistanceFlag(objArray[14].toString());
				}
				if (objArray[15] != null) {
					brokerBOBDetailsDTO.setEnrollmentEndDate(new SimpleDateFormat(UI_DATE_FORMAT).format(new SimpleDateFormat(DATE_FORMAT).parse(objArray[15].toString())));
				}
				if (objArray[16] != null) {
					brokerBOBDetailsDTO.setShowSwitchRolePopup(objArray[16].toString());
				}
				if (objArray[17] != null) {
					brokerBOBDetailsDTO.setEligibilityStatus(objArray[17].toString());
				}
				if (objArray[18] != null) {
					brokerBOBDetailsDTO.setExchangeEligibilityStatus(objArray[18].toString());
				}
				if (objArray[19] != null) {
					brokerBOBDetailsDTO.setMaxAPTC(new BigDecimal(objArray[19].toString()).doubleValue());
				}
				if (objArray[20] != null  ) {
					String clobString = (String) objArray[20];
					brokerBOBDetailsDTO.setApplicationData(clobString);
				}
				if (objArray[21] != null) {
					brokerBOBDetailsDTO.setCaseNumber(objArray[21].toString());
				}

				if (objArray[22] != null) {
					brokerBOBDetailsDTO.setCoverageYear(new BigDecimal(objArray[22].toString()).longValue());
				}

				if (objArray[23] != null) {
				 	brokerBOBDetailsDTO.setIsUserExists(true);
				} else {
					brokerBOBDetailsDTO.setIsUserExists(false);
				}

				if (objArray[24] != null) {
					brokerBOBDetailsDTO.setCsrLevel(objArray[24].toString());
				}
				
				if (objArray[25] != null) {
					brokerBOBDetailsDTO.setApplicationDate(dateformat.format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(objArray[25].toString())));
				}
				
				if (objArray[26] != null) {
					brokerBOBDetailsDTO.setApplicationValidationStatus(objArray[26].toString());
				}

				listOfBrokerBOBDetailsDTO.add(brokerBOBDetailsDTO);
			}
			LOGGER.info("Loop through Main Query Ended in " + (System.nanoTime() - currentNanoTime));

		} catch (Exception exception) {
			LOGGER.error("Exception while fetching Broker BOB details : " + exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}

		if(listOfBrokerBOBDetailsDTO!=null && listOfBrokerBOBDetailsDTO.size()>0){
			listOfBrokerBOBDetailsDTO = processHouseholdStatus(listOfBrokerBOBDetailsDTO);
			brokerBobResponseDTO = filter(loadPlanDetails(listOfBrokerBOBDetailsDTO, requestDTO.getUserName()), bobSearchParamsDTO, skipPagination, requestDTO.getUserName());
		}

		return brokerBobResponseDTO;
	}

	public List<BrokerBOBDetailsDTO> processHouseholdStatus(List<BrokerBOBDetailsDTO> listOfBrokerBOBDetailsDTO) throws ParseException{
		List<BrokerBOBDetailsDTO> responseDTO = new ArrayList<>();
		String applicationStatus;

		for(BrokerBOBDetailsDTO brokerBOBDetailsDTO:listOfBrokerBOBDetailsDTO){
			if(brokerBOBDetailsDTO.getSsapApplicationId()!=0){
				applicationStatus = brokerBOBDetailsDTO.getApplicationStatus();

				if(brokerBOBDetailsDTO.getFinancialAssistanceFlag()!=null && YES.equalsIgnoreCase(brokerBOBDetailsDTO.getFinancialAssistanceFlag())){
					if(ApplicationStatus.ER.toString().equalsIgnoreCase(applicationStatus) ){
						brokerBOBDetailsDTO.setNextStep(SHOP_FOR_PLANS);
						String resourceName = brokerBOBDetailsDTO.getFirstName()+" "+brokerBOBDetailsDTO.getLastName();
						String url=this.getEncryptedUrl("individual", brokerBOBDetailsDTO.getIndividualId(), resourceName);
						brokerBOBDetailsDTO.setNextStepLink(url);
						setDueDate(brokerBOBDetailsDTO);
						brokerBOBDetailsDTO.setCurrentStatus(ELIGIBLE_FOR_SHOPPINGS);
					} else if(ApplicationStatus.EN.toString().equalsIgnoreCase(applicationStatus)){
						brokerBOBDetailsDTO.setNextStep(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setDueDate(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setCurrentStatus(ENROLLED_IN_A_QUALIFIED_HEALTH_PLAN);
					} else if(ApplicationStatus.CL.toString().equalsIgnoreCase(applicationStatus)){
						brokerBOBDetailsDTO.setNextStep(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setDueDate(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setCurrentStatus(CLOSED);
					} else if(ApplicationStatus.CC.toString().equalsIgnoreCase(applicationStatus)){
						brokerBOBDetailsDTO.setNextStep(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setDueDate(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setCurrentStatus(CANCELED_APPLICATION);
					}
					brokerBOBDetailsDTO.setApplicationType(FINANCIAL);
				} else {
					if(ApplicationStatus.OP.toString().equalsIgnoreCase(applicationStatus)){
						brokerBOBDetailsDTO.setNextStep(SUBMIT_THE_APPLICATION);
						String resourceName = brokerBOBDetailsDTO.getFirstName()+" "+brokerBOBDetailsDTO.getLastName();
						String url=this.getEncryptedUrl("individual", brokerBOBDetailsDTO.getIndividualId(), resourceName);
						brokerBOBDetailsDTO.setNextStepLink(url);
						setDueDate(brokerBOBDetailsDTO);
						brokerBOBDetailsDTO.setCurrentStatus(INCOMPLETE_APPLICATION);
					} else if(ApplicationStatus.ER.toString().equalsIgnoreCase(applicationStatus)){
						brokerBOBDetailsDTO.setNextStep(SHOP_FOR_PLANS);
						String resourceName = brokerBOBDetailsDTO.getFirstName()+" "+brokerBOBDetailsDTO.getLastName();
						String url=this.getEncryptedUrl("individual", brokerBOBDetailsDTO.getIndividualId(), resourceName);
						brokerBOBDetailsDTO.setNextStepLink(url);
						setDueDate(brokerBOBDetailsDTO);
						brokerBOBDetailsDTO.setCurrentStatus(ELIGIBLE_FOR_SHOPPINGS);
					} else if(ApplicationStatus.EN.toString().equalsIgnoreCase(applicationStatus)){
						brokerBOBDetailsDTO.setNextStep(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setDueDate(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setCurrentStatus(ENROLLED_IN_A_QUALIFIED_HEALTH_PLAN);
					} else if(ApplicationStatus.SU.toString().equalsIgnoreCase(applicationStatus) || ApplicationStatus.SG.toString().equalsIgnoreCase(applicationStatus)){
						brokerBOBDetailsDTO.setNextStep(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setDueDate(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setCurrentStatus(APPLICATION_SUBMITTED);
					} else if(ApplicationStatus.CL.toString().equalsIgnoreCase(applicationStatus)){
						brokerBOBDetailsDTO.setNextStep(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setDueDate(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setCurrentStatus(CLOSED);
					} else if(ApplicationStatus.CC.toString().equalsIgnoreCase(applicationStatus)){
						brokerBOBDetailsDTO.setNextStep(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setDueDate(NOT_APPLICABLE);
						brokerBOBDetailsDTO.setCurrentStatus(CANCELED_APPLICATION);
					}
					brokerBOBDetailsDTO.setApplicationType(NON_FINANCIAL);
				}
				
				if(ApplicationValidationStatus.PENDING.toString().equalsIgnoreCase(brokerBOBDetailsDTO.getApplicationValidationStatus())){
					brokerBOBDetailsDTO.setNextStep(UPLOAD_DOCUMENTATION);
				}
			} else {
				brokerBOBDetailsDTO.setNextStep(NOT_APPLICABLE);
				brokerBOBDetailsDTO.setDueDate(NOT_APPLICABLE);
				brokerBOBDetailsDTO.setCurrentStatus(NO_APPLICATION_FOUND);
			}

			responseDTO.add(brokerBOBDetailsDTO);
			
		}

		return responseDTO;
	}

	private BrokerBOBDetailsDTO setDueDate(BrokerBOBDetailsDTO brokerBOBDetailsDTO) throws ParseException{
		if(OE.equalsIgnoreCase(brokerBOBDetailsDTO.getApplicationType())){
			if(StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE))  && (new TSDate()).after(new SimpleDateFormat(DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE)))) {
				brokerBOBDetailsDTO.setDueDate(NOT_APPLICABLE);
				brokerBOBDetailsDTO.setNextStep(NOT_APPLICABLE);
			}
			else  {
				brokerBOBDetailsDTO.setDueDate(new SimpleDateFormat(UI_DATE_FORMAT).format(new SimpleDateFormat(DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE))));
			}
		} else {
			if(StringUtils.isNotBlank(brokerBOBDetailsDTO.getEnrollmentEndDate()) && (new TSDate()).after(new SimpleDateFormat(UI_DATE_FORMAT).parse((brokerBOBDetailsDTO.getEnrollmentEndDate())))) {
				brokerBOBDetailsDTO.setDueDate(NOT_APPLICABLE);
				brokerBOBDetailsDTO.setNextStep(NOT_APPLICABLE);
			}
			else{
				brokerBOBDetailsDTO.setDueDate(brokerBOBDetailsDTO.getEnrollmentEndDate());
			}
		}

		return brokerBOBDetailsDTO;
	}


	public List<BrokerBOBDetailsDTO> loadPlanDetails(List<BrokerBOBDetailsDTO> listOfBrokerBOBDetailsDTO, String userName){

		List<BrokerBOBDetailsDTO> responseDTO = new ArrayList<>();
		EnrollmentResponse enrollmentResponse;
		Map<String, List<EnrollmentDataDTO>> householdEnrollmentMap;
		List<EnrollmentDataDTO> listOfEnrollments;
		Set<Integer> planSet = new HashSet<>();
		Map<Integer, PlanLightResponseDTO> planResponseMap = new HashMap<>();

		try {
			long currentNanoTime = System.nanoTime();
			LOGGER.info("Call to enrollment Started for " + userName + " at " + currentNanoTime);
			enrollmentResponse = loadEnrollmentData(getListOfHouseholdIds(listOfBrokerBOBDetailsDTO), userName);
			LOGGER.info("Call to enrollment Ended for " + userName + " after " + (System.nanoTime() - currentNanoTime));

			householdEnrollmentMap = enrollmentResponse.getEnrollmentDataDtoListMap();

			currentNanoTime = System.nanoTime();

			for(BrokerBOBDetailsDTO brokerBOBDetailsDTO:listOfBrokerBOBDetailsDTO){
				if(ApplicationStatus.EN.toString().equalsIgnoreCase(brokerBOBDetailsDTO.getApplicationStatus())){
					if(householdEnrollmentMap != null){
						listOfEnrollments = householdEnrollmentMap.get(String.valueOf(brokerBOBDetailsDTO.getIndividualId()));
						if(listOfEnrollments!=null){
							for(EnrollmentDataDTO enrollmentDataDto:listOfEnrollments){
								planSet.add(enrollmentDataDto.getPlanId());
							}
						}
					}
				}
			}
			
			if(!planSet.isEmpty()){
				planResponseMap = planMgmtService.getPlansInfo(new ArrayList<>(planSet));
			}

			LOGGER.info("Call to Plan Started at " + currentNanoTime);
			for(BrokerBOBDetailsDTO brokerBOBDetailsDTO:listOfBrokerBOBDetailsDTO){
				if(ApplicationStatus.EN.toString().equalsIgnoreCase(brokerBOBDetailsDTO.getApplicationStatus())){
					boolean checkIsEnrolled = false;
					
					if(householdEnrollmentMap != null){
						listOfEnrollments = householdEnrollmentMap.get(String.valueOf(brokerBOBDetailsDTO.getIndividualId()));

						if(listOfEnrollments!=null){
							brokerBOBDetailsDTO.setIsEnrolled(BrokerConstants.TRUE);
							if(brokerBOBDetailsDTO.getIndividualId() == 105641 || brokerBOBDetailsDTO.getIndividualId() == 35579 || brokerBOBDetailsDTO.getIndividualId() == 58226){
								LOGGER.warn(brokerBOBDetailsDTO.getIndividualId()+" has " +listOfEnrollments.size()+ " enrollments");
							}
							for(EnrollmentDataDTO enrollmentDataDto:listOfEnrollments){
								BrokerBOBDetailsDTO clonedBrokerBOBDetailsDTO = copyBrokerBobDetailsDTO(brokerBOBDetailsDTO);
								if(enrollmentDataDto.getSsapApplicationId() == clonedBrokerBOBDetailsDTO.getSsapApplicationId()){
									if(clonedBrokerBOBDetailsDTO.getIndividualId() == 105641 || clonedBrokerBOBDetailsDTO.getIndividualId() == 35579 || clonedBrokerBOBDetailsDTO.getIndividualId() == 58226){
										LOGGER.warn("loadPlanDetails indId: "+clonedBrokerBOBDetailsDTO.getIndividualId());
										LOGGER.warn("loadPlanDetails plId: "+enrollmentDataDto.getPlanId());
										LOGGER.warn("loadPlanDetails enrlId: "+enrollmentDataDto.getEnrollmentId());
									}
									
									clonedBrokerBOBDetailsDTO.setPremium(String.valueOf(enrollmentDataDto.getNetPremiumAmt()));
									clonedBrokerBOBDetailsDTO.setInsuranceType(enrollmentDataDto.getPlanType());
									clonedBrokerBOBDetailsDTO.setPlanId(enrollmentDataDto.getPlanId());
									if(null != enrollmentDataDto.getAptcAmount()){
									clonedBrokerBOBDetailsDTO.setAptcAmount(String.valueOf(enrollmentDataDto.getAptcAmount()));
									}
									clonedBrokerBOBDetailsDTO.setGrossPremiumAmt(String.valueOf(enrollmentDataDto.getGrossPremiumAmt()));
									clonedBrokerBOBDetailsDTO = populatePlanDetailsForExcel(planResponseMap.get(enrollmentDataDto.getPlanId()), clonedBrokerBOBDetailsDTO, enrollmentDataDto.getNumberOfEnrollees());
									responseDTO.add(clonedBrokerBOBDetailsDTO);
									checkIsEnrolled = true;
								}
							}
						} 
					} 
					
					if(!checkIsEnrolled){
						brokerBOBDetailsDTO.setIsEnrolled(BrokerConstants.FALSE);
						brokerBOBDetailsDTO.setCurrentStatus(NO_ACTIVE_ENROLLMENT);
						setNextStepForApplicationWithInactiveEnrollment(brokerBOBDetailsDTO);
						responseDTO.add(brokerBOBDetailsDTO);
					}
				} else {
					brokerBOBDetailsDTO.setIsEnrolled(BrokerConstants.FALSE);
					responseDTO.add(brokerBOBDetailsDTO);
				}
			}
			LOGGER.info("Call to Plan Ended at " + (System.nanoTime() - currentNanoTime));
		} catch(Exception exception){
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return responseDTO;
	}
	
	private BrokerBOBDetailsDTO setNextStepForApplicationWithInactiveEnrollment(BrokerBOBDetailsDTO brokerBOBDetailsDTO) throws ParseException{
		Date oepStartDate = new SimpleDateFormat(DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_START_DATE));
		Date oepEndDate = new SimpleDateFormat(DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE));
		Date currentDate = new TSDate();
		
		if(currentDate.before(oepStartDate) || currentDate.after(oepEndDate)){
			brokerBOBDetailsDTO.setNextStep(REPORT_A_CHANGE);
		} else {
			brokerBOBDetailsDTO.setNextStep(ENROLL_DURING_OEP);
		}
		
		return brokerBOBDetailsDTO;
	}

	private BrokerBOBDetailsDTO copyBrokerBobDetailsDTO(BrokerBOBDetailsDTO brokerBOBDetailsDTO) throws CloneNotSupportedException{
		return (BrokerBOBDetailsDTO) brokerBOBDetailsDTO.clone();
	}

	private EnrollmentResponse loadEnrollmentData(List<String> householdIds, String userName){
		EnrollmentResponse enrollmentResponse = null;
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		List<EnrollmentStatus> enrollmentStatusList = new ArrayList<>();

		try {
			enrollmentStatusList.add(EnrollmentRequest.EnrollmentStatus.TERM_ACTIVE);
			enrollmentRequest.setEnrollmentStatusList(enrollmentStatusList);
			enrollmentRequest.setHouseHoldCaseIdList(householdIds);

			ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_DATA_FOR_BROKER_BY_LIST_OF_HOUSEHOLDCASEIDS, userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest));
			if(response != null){
				enrollmentResponse = platformGson.fromJson(response.getBody(), EnrollmentResponse.class);
			}
		} catch (Exception exception) {
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return enrollmentResponse;
	}

	public BrokerBobResponseDTO loadIssuerDetails(String userName)
	{

		IssuerListDetailsDTO issuerListDetailsDTO = new IssuerListDetailsDTO();
		BrokerBobResponseDTO brokerBobResponseDTO = new BrokerBobResponseDTO();

		try {
			issuerListDetailsDTO.setTenantCode(ALL);
			issuerListDetailsDTO.setInsuranceType(HEALTH);
			issuerListDetailsDTO.setStateCode(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));

			ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.PlanMgmtEndPoints.GET_ISSUER_LIST, userName,
					HttpMethod.POST, MediaType.APPLICATION_JSON,String.class, platformGson.toJson(issuerListDetailsDTO));
			if(response != null){
				issuerListDetailsDTO = platformGson.fromJson(response.getBody(), IssuerListDetailsDTO.class);
			}

			List<String> QHPIssuerNamesList = new ArrayList<String>();
			for(IssuerInfoDTO issuer : issuerListDetailsDTO.getIssuersList())
			{
				QHPIssuerNamesList.add(issuer.getIssuerName());
			}
			brokerBobResponseDTO.setQHPIssuerNames(QHPIssuerNamesList);

			issuerListDetailsDTO = new IssuerListDetailsDTO();

			issuerListDetailsDTO.setTenantCode(ALL);
			issuerListDetailsDTO.setInsuranceType(DENTAL);
			issuerListDetailsDTO.setStateCode(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));

			response = ghixRestTemplate.exchange(GhixEndPoints.PlanMgmtEndPoints.GET_ISSUER_LIST, userName,
					HttpMethod.POST, MediaType.APPLICATION_JSON,String.class, platformGson.toJson(issuerListDetailsDTO));
			if(response != null){
				issuerListDetailsDTO = platformGson.fromJson(response.getBody(), IssuerListDetailsDTO.class);
			}

			List<String> QDPIssuerNamesList = new ArrayList<String>();
			for(IssuerInfoDTO issuer : issuerListDetailsDTO.getIssuersList())
			{
				QDPIssuerNamesList.add(issuer.getIssuerName());
			}
			brokerBobResponseDTO.setQDPIssuerNames(QDPIssuerNamesList);

			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE)){
				issuerListDetailsDTO = new IssuerListDetailsDTO();

				issuerListDetailsDTO.setTenantCode(ALL);
				issuerListDetailsDTO.setInsuranceType(ALL);
				issuerListDetailsDTO.setStateCode(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));

				response = ghixRestTemplate.exchange(GhixEndPoints.PlanMgmtEndPoints.GET_ISSUER_LIST, userName,
						HttpMethod.POST, MediaType.APPLICATION_JSON,String.class, platformGson.toJson(issuerListDetailsDTO));
				if(response != null){
					issuerListDetailsDTO = platformGson.fromJson(response.getBody(), IssuerListDetailsDTO.class);
				}

				List<String> bothQHPAndQDPIssuerNamesList = new ArrayList<String>();
				String bothQDPAndQHPIssuerName = null;

				for(IssuerInfoDTO issuer : issuerListDetailsDTO.getIssuersList())
				{
					bothQDPAndQHPIssuerName = issuer.getIssuerName();
					boolean qhpFlag = false;
					boolean qdpFlag = false;
					for(String QHPissuer : QHPIssuerNamesList)
					{
						if(bothQDPAndQHPIssuerName.equalsIgnoreCase(QHPissuer))
						{
							qhpFlag = true;
							break;
						}
					}
					for(String QDPissuer : QDPIssuerNamesList)
					{
						if(bothQDPAndQHPIssuerName.equalsIgnoreCase(QDPissuer))
						{
							qdpFlag = true;
							break;
						}
					}
					if(qhpFlag == true && qdpFlag == true)
					{
						bothQHPAndQDPIssuerNamesList.add(bothQDPAndQHPIssuerName);
						QHPIssuerNamesList.remove(bothQDPAndQHPIssuerName);
						QDPIssuerNamesList.remove(bothQDPAndQHPIssuerName);
					}
				}
				brokerBobResponseDTO.setBothQHPAndQDPIssuerNames(bothQHPAndQDPIssuerNamesList);
			}
		} catch (Exception exception) {
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return brokerBobResponseDTO;
	}

	private List<String> getListOfHouseholdIds(List<BrokerBOBDetailsDTO> listOfBrokerBOBDetailsDTO){
		List<String> listOfIndividualIds = new ArrayList<>();
		for(BrokerBOBDetailsDTO brokerBOBDetailsDTO:listOfBrokerBOBDetailsDTO){
			if(ApplicationStatus.EN.toString().equalsIgnoreCase(brokerBOBDetailsDTO.getApplicationStatus())){
				listOfIndividualIds.add(String.valueOf(brokerBOBDetailsDTO.getIndividualId()));
			}
		}

		return listOfIndividualIds;
	}

	private void appendToQuery(StringBuilder query, String columnName, String columnValue, String columnReferenceKey, String operator, boolean isLikeQuery) {
		if (columnValue != null && !"".equals(columnValue.trim())) {
			if(!StringUtils.containsOnly(query, "WHERE")){
				if(isLikeQuery) {
					query.append(" AND UPPER(").append(columnName).append(") ").append(operator).append(" UPPER(:").append(columnReferenceKey).append(")");
				} else {
					query.append(" AND ").append(columnName).append(" ").append(operator).append(" :").append(columnReferenceKey);
				}
			} else {
				if(isLikeQuery) {
					query.append(" UPPER(").append(columnName).append(") ").append(operator).append(" UPPER(:").append(columnReferenceKey).append(")");
				} else {
					query.append(" ").append(columnName).append(" ").append(operator).append(" :").append(columnReferenceKey);
				}
			}
		}
	}

	private void setQueryParameter(Query searchQuery, String parameter, String value, boolean isWildSearch) {
		if (value != null && !"".equals(value.trim())) {
			if(isWildSearch) {
				searchQuery.setParameter(parameter, "%"+value.trim()+"%");
			} else {
				searchQuery.setParameter(parameter, value.trim());
			}
		}
	}

	private void setQueryParameter(Query searchQuery, String parameter, Integer value) {
		if (value != null && value!=0) {
			searchQuery.setParameter(parameter, value);
		}
	}

	private BrokerBOBDetailsDTO populatePlanDetailsForExcel(PlanLightResponseDTO planLightResponseDTO, BrokerBOBDetailsDTO brokerBOBDetailsDTO, Integer numberOfEnrollees) throws GIException{
		if(planLightResponseDTO!=null){
			brokerBOBDetailsDTO.setPlanName(planLightResponseDTO.getPlanName());
			brokerBOBDetailsDTO.setPlanType(planLightResponseDTO.getNetworkType());
			if(StringUtils.isNotBlank(planLightResponseDTO.getOfficeVisit())){
				brokerBOBDetailsDTO.setOfficeVisit(planLightResponseDTO.getOfficeVisit().replace("<br>", " "));
			}
			if(StringUtils.isNotBlank(planLightResponseDTO.getGenericMedications())){
				brokerBOBDetailsDTO.setGenericDrugs(planLightResponseDTO.getGenericMedications().replace("<br>", " "));
			}
			brokerBOBDetailsDTO.setIssuerLogo(getIssuerLogoUrl(planLightResponseDTO.getIssuerLogo()));
			brokerBOBDetailsDTO.setIssuerName(planLightResponseDTO.getIssuerName());
			if (numberOfEnrollees == 1) {
				if (StringUtils.isNotBlank(planLightResponseDTO.getDeductible())) {
					Float deductibleValue = getFloatValue(planLightResponseDTO.getDeductible());
					if(deductibleValue != null){
						brokerBOBDetailsDTO.setDeductible(DOLLAR_SYMBOL + deductibleValue);
					}else{
						brokerBOBDetailsDTO.setDeductible("NA");
					}

				}
			} else {
				if (StringUtils.isNotBlank(planLightResponseDTO.getDeductibleFamily())) {
					Float deductibleValue = getFloatValue(planLightResponseDTO.getDeductibleFamily());
					if(deductibleValue != null){
						brokerBOBDetailsDTO.setDeductible(DOLLAR_SYMBOL + deductibleValue);
					}else{
						brokerBOBDetailsDTO.setDeductible("NA");
					}
				}
			}
		}

		return brokerBOBDetailsDTO;
	}

	private String getIssuerLogoUrl(String issuerLogo) {
		return (issuerLogo != null && !issuerLogo.isEmpty()) ? appUrl+ DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(issuerLogo) : "";
	}

	private Float getFloatValue(String input) {
		Float val = null;
		if (input != null && !input.equalsIgnoreCase("NA")) {
			try {
				val = Float.valueOf(input);
			} catch (Exception ex) {
				LOGGER.error("", ex);
			}
		}
		return val;
	}

	private BrokerBobResponseDTO filter(List<BrokerBOBDetailsDTO> list, BrokerBOBSearchParamsDTO bobSearchParamsDTO, boolean skipPagination, String userName) throws ParseException {
		List<BrokerBOBDetailsDTO> matchedRecords = null;

	    Map<String, String> map = populateFilterParams(bobSearchParamsDTO);

	    if(map!=null){
	    	for (Map.Entry<String, String> entry : map.entrySet()){
	    		if(matchedRecords!=null && matchedRecords.isEmpty()){
	    			break;
	    		} else if(matchedRecords!=null && matchedRecords.size()>0){
	    			list = matchedRecords;
	    		}
	    		matchedRecords = new ArrayList<BrokerBOBDetailsDTO>();
		    	for (BrokerBOBDetailsDTO brokerBOBDetailsDTO : list) {
			         if (checkProperty(brokerBOBDetailsDTO, entry.getKey(), entry.getValue())){
			        	 matchedRecords.add(brokerBOBDetailsDTO);
			         }
			    }
		    }
	    } else {
	    	matchedRecords = list;
	    }

	    return  getListPerPage(sort(matchedRecords, bobSearchParamsDTO), bobSearchParamsDTO.getStartRecord(), bobSearchParamsDTO.getEndRecord(), skipPagination, bobSearchParamsDTO, userName);
	}

	private List<BrokerBOBDetailsDTO> sort(List<BrokerBOBDetailsDTO> list, BrokerBOBSearchParamsDTO bobSearchParamsDTO){
		String sortBy = bobSearchParamsDTO.getSortBy();

		if(FIRST_NAME_ASC.equalsIgnoreCase(sortBy) || FIRST_NAME_DESC.equalsIgnoreCase(sortBy)){
			Collections.sort(list, BrokerBOBDetailsDTO.FIRST_NAME_COMPARATOR);
		} else if(LAST_NAME_ASC.equalsIgnoreCase(sortBy) || LAST_NAME_DESC.equalsIgnoreCase(sortBy)){
			Collections.sort(list, BrokerBOBDetailsDTO.LAST_NAME_COMPARATOR);
		} else {
			List<BrokerBOBDetailsDTO> listWithoutDueDates = getListWithoutDueDates(list);
			list = getListWithDueDates(list);
			Collections.sort(list, BrokerBOBDetailsDTO.DUE_DATE_COMPARATOR);

			if(DUE_DATE_DESC.equalsIgnoreCase(sortBy)){
				Collections.reverse(list);
			}
			list.addAll(listWithoutDueDates);
		}

		if(FIRST_NAME_DESC.equalsIgnoreCase(sortBy) || LAST_NAME_DESC.equalsIgnoreCase(sortBy)){
			Collections.reverse(list);

		}

		return list;
	}

	private List<BrokerBOBDetailsDTO> getListWithoutDueDates(List<BrokerBOBDetailsDTO> list){
		List<BrokerBOBDetailsDTO> listWithoutDueDates = new ArrayList<>();

		if(list != null) {
			for(BrokerBOBDetailsDTO brokerBOBDetailsDTO:list){
				if(brokerBOBDetailsDTO.getDueDate()==null || "N/A".equalsIgnoreCase(brokerBOBDetailsDTO.getDueDate())){
					listWithoutDueDates.add(brokerBOBDetailsDTO);
				}
			}
		}

		return listWithoutDueDates;
	}

	private List<BrokerBOBDetailsDTO> getListWithDueDates(List<BrokerBOBDetailsDTO> list){
		List<BrokerBOBDetailsDTO> listWithDueDates = new ArrayList<>();

		if(list != null) {
			for(BrokerBOBDetailsDTO brokerBOBDetailsDTO:list){
				if(brokerBOBDetailsDTO.getDueDate()!=null && !"N/A".equalsIgnoreCase(brokerBOBDetailsDTO.getDueDate())){
					listWithDueDates.add(brokerBOBDetailsDTO);
				}
			}
		}

		return listWithDueDates;
	}

	private BrokerBobResponseDTO getListPerPage(List<BrokerBOBDetailsDTO> list, Integer startRecord, Integer endRecord, boolean skipPagination, BrokerBOBSearchParamsDTO bobSearchParamsDTO, String userName) throws ParseException{
		List<BrokerBOBDetailsDTO> listOfRecordsPerPage = null;
		BrokerBobResponseDTO brokerBobResponseDTO = new BrokerBobResponseDTO();

		if(list!=null && list.size()!=0){
			int count = list.size();

			if(skipPagination) {
				listOfRecordsPerPage = list.subList(startRecord, count);
			}
			else {
				try {
					if(count <= PAGE_SIZE){
						listOfRecordsPerPage = list;
					} else if(count>endRecord){
						listOfRecordsPerPage = list.subList(startRecord, endRecord);
					} else {
						listOfRecordsPerPage = list.subList(startRecord, count);
					}
				}  catch(Exception exception){
				     listOfRecordsPerPage = loadFirstPageRecords(list, listOfRecordsPerPage, count);
			    }

			}

			brokerBobResponseDTO.setBrokerBobDetailsDTO(listOfRecordsPerPage);
			brokerBobResponseDTO.setCount(count);
		}


		return brokerBobResponseDTO;
	}

	private List<BrokerBOBDetailsDTO> loadFirstPageRecords(List<BrokerBOBDetailsDTO> list, List<BrokerBOBDetailsDTO> listOfRecordsPerPage, int count){
		  if(count <= PAGE_SIZE){
		   listOfRecordsPerPage = list;
		  } else {
		   listOfRecordsPerPage = list.subList(0, PAGE_SIZE);
		  }

		  return listOfRecordsPerPage;
	}

	private boolean checkProperty(BrokerBOBDetailsDTO brokerBOBDetailsDTO, String property, String propertyValue) throws ParseException {
		boolean isMatched = false;

		switch (property) {
			case APPLICATION_TYPE:
				isMatched = propertyValue.equalsIgnoreCase(brokerBOBDetailsDTO.getApplicationType());
			    break;
			case CURRENT_STATUS:
				isMatched = propertyValue.equalsIgnoreCase(brokerBOBDetailsDTO.getCurrentStatus());
			    break;
			case ISSUER:
				isMatched = checkIssuerName(brokerBOBDetailsDTO, propertyValue);
			    break;
			case NEXT_STEP:
				isMatched = propertyValue.equalsIgnoreCase(brokerBOBDetailsDTO.getNextStep());
			    break;
			case DUE_DATE:
				isMatched = checkDueDate(brokerBOBDetailsDTO.getDueDate(), propertyValue);
			    break;
		}

		 return isMatched;
	}

	private boolean checkIssuerName(BrokerBOBDetailsDTO brokerBOBDetailsDTO, String propertyValue){
		boolean isMatched = false;
		String[] param = propertyValue.split("_");
		String planType = param[0];
		String issuerName = param[1];

		String insuranceType = brokerBOBDetailsDTO.getInsuranceType();

		if(StringUtils.isNotBlank(insuranceType)){
			switch (planType) {
				case QHP:
					isMatched = (INSURANCE_TYPE_HEALTH.equalsIgnoreCase(insuranceType) && issuerName.equalsIgnoreCase(brokerBOBDetailsDTO.getIssuerName()));
				    break;
				case QDP:
					isMatched = (INSURANCE_TYPE_DENTAL.equalsIgnoreCase(insuranceType) && issuerName.equalsIgnoreCase(brokerBOBDetailsDTO.getIssuerName()));
				    break;
				case BOTH:
					isMatched = issuerName.equalsIgnoreCase(brokerBOBDetailsDTO.getIssuerName());
				    break;
			}
		}

		return isMatched;
	}

	private boolean checkDueDate(String dueDate, String propertyValue) throws ParseException{
		boolean dateCheck = false;

		if(StringUtils.isNotBlank(dueDate) && !NOT_APPLICABLE.equalsIgnoreCase(dueDate)){
			Date inputDate = new SimpleDateFormat(UI_DATE_FORMAT).parse(dueDate);
			Date date = new TSDate();

			if(THIS_WEEK.equalsIgnoreCase(propertyValue)){
				dateCheck = inputDate.compareTo(getMinDate(getDate(date, 0))) >= 0 && inputDate.compareTo(getMaxDate(getDate(date, 6))) <= 0;
			} else if(NEXT_WEEK.equalsIgnoreCase(propertyValue)){
				dateCheck = inputDate.compareTo(getMinDate(getDate(date, 7))) >= 0 && inputDate.compareTo(getMaxDate(getDate(date, 13))) <= 0;
			}
		}

		return dateCheck;
	}

	private Map<String, String> populateFilterParams(BrokerBOBSearchParamsDTO bobSearchParamsDTO){
		Map<String, String> map = new HashMap<>();
		boolean flag = false;

		if(StringUtils.isNotBlank(bobSearchParamsDTO.getCurrentStatus())){
			map.put(CURRENT_STATUS, bobSearchParamsDTO.getCurrentStatus());
			flag = true;
		}

		if(StringUtils.isNotBlank(bobSearchParamsDTO.getApplicationType())){
			map.put(APPLICATION_TYPE, bobSearchParamsDTO.getApplicationType());
			flag = true;
		}

		if(StringUtils.isNotBlank(bobSearchParamsDTO.getIssuerval())){
			map.put(ISSUER, bobSearchParamsDTO.getIssuerval());
			flag = true;
		}

		if(StringUtils.isNotBlank(bobSearchParamsDTO.getNextStep())){
			map.put(NEXT_STEP, bobSearchParamsDTO.getNextStep());
			flag = true;
		}

		if(StringUtils.isNotBlank(bobSearchParamsDTO.getDueDate())){
			map.put(DUE_DATE, bobSearchParamsDTO.getDueDate());
			flag = true;
		}

		return flag?map:null;
	}

	private Date getDate(Date date, int days){
		Calendar cal = TSCalendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);

		return cal.getTime();
	}

	private Timestamp getMinDate(Date date){
		Calendar calendar = TSCalendar.getInstance();
	    calendar.setTime(date);
	    calendar.set(Calendar.HOUR_OF_DAY, 00);
	    calendar.set(Calendar.MINUTE, 00);
	    calendar.set(Calendar.SECOND, 00);
	    calendar.set(Calendar.MILLISECOND, 000);

		return new Timestamp(calendar.getTime().getTime());
	}

	private Timestamp getMaxDate(Date date){
		Calendar calendar = TSCalendar.getInstance();
	    calendar.setTime(date);
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 999);

	    return new Timestamp(calendar.getTime().getTime());
	}
	
	private String getEncryptedUrl(String switchToModuleName,int switchToModuleId,String switchToResourceName){
		String baseUrl = "/hix/account/user/switchUserRole?ref=".intern();
		HashMap<String,String> paramMap= new HashMap<String,String>(3);
		paramMap.put("switchToModuleName", switchToModuleName);
		paramMap.put("switchToModuleId", Integer.toString(switchToModuleId));
		paramMap.put("switchToResourceName", switchToResourceName);
		return baseUrl+GhixAESCipherPool.encryptParameterMap(paramMap);
	}
}
