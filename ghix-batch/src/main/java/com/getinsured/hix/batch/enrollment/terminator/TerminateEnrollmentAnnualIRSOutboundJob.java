package com.getinsured.hix.batch.enrollment.terminator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsReportService;
import com.getinsured.hix.batch.enrollment.skip.Enrollment1095StagingParams;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.enrollment1095.service.Enrollment1095NotificationService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;

public class TerminateEnrollmentAnnualIRSOutboundJob  implements Tasklet{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TerminateEnrollmentAnnualIRSOutboundJob.class);
	
	private EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService;
	private Enrollment1095StagingParams enrollment1095StagingParams;
	private Enrollment1095NotificationService enrollment1095NotificationService;
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

		ExecutionContext je = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();

		Object jobExecutionStatus = je.get("jobExecutionStatus");
		String jobStatus = null;

		if(jobExecutionStatus != null){
			jobStatus = (String)jobExecutionStatus;
		}
		
		if(jobStatus != null && jobStatus.compareToIgnoreCase("failed") == 0){
			String errorMsg = "";
			if(null != je.get("errorMessage")){
				errorMsg = (String)je.get("errorMessage");
				LOGGER.error(errorMsg);
			}
			throw new GIException("Error occured during IRS Report Generation" + errorMsg);
		}
		
		if(null != enrollment1095StagingParams){
			// Send email
			Date batchDate = new Date();
			String skippedFileLocation = logSkippedEnrollments(enrollment1095StagingParams.getSkippedEnrollmentMap(), getDateTime(batchDate));
			sendStagingJobCompletionEmail(batchDate, skippedFileLocation);
			enrollment1095StagingParams.clearEnrollmentIdList();
			enrollment1095StagingParams.resetCounters();
		}
		
		//Set void indicator for cancelled enrollment 1095s
		enrollmentAnnualIrsReportService.setVoidIndicatorForCancelledEnrollment();
		
		LOGGER.info("Terminate");
		return RepeatStatus.FINISHED;
	}
	
	/**
	 * Send Email Notification
	 * @author negi_s
	 * @param batchDate 
	 * @param skippedFileLocation 
	 */
	private void sendStagingJobCompletionEmail(Date batchDate, String skippedFileLocation) {
		Map<String, String> emailDataMap=new HashMap<>();  
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_Staging_BatchEmail.GENERATED_DATE, DateUtil.dateToString(batchDate, EnrollmentConstants.FINANCE_DATE_FORMAT));
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_Staging_BatchEmail.JOB_NAME, EnrollmentConstants.ENROLLMENT_1095_STAGING_JOB);
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_Staging_BatchEmail.TOTAL_RECORD_COUNT, enrollment1095StagingParams.getEnrollmentIdList().size()+"");
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_Staging_BatchEmail.UPDATED_RECORD_COUNT,  enrollment1095StagingParams.getTotalUpdateCount()+"");
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_Staging_BatchEmail.OVERWRITTEN_RECORD_COUNT,  enrollment1095StagingParams.getTotalOverWrittenCount()+"");
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_Staging_BatchEmail.FAILED_RECORD_COUNT, enrollment1095StagingParams.getTotalFailedCount()+"");
		emailDataMap.put(EnrollmentConstants.Enrollment_1095_Staging_BatchEmail.SKIPPED_FILE_LOCATION, skippedFileLocation);
		try {
			enrollment1095NotificationService.sendStagingJobCompletionEmail(emailDataMap);
		} catch (GIException e) {
			LOGGER.debug("Error sending staging email notification :: ", e);
		}
	}
	
	/**
	 * Log skipped records in a file
	 * @param skippedEnrollmentMap
	 * @param batchDate
	 * @return String Skipped file location
	 */
	private String logSkippedEnrollments(Map<String, String> skippedEnrollmentMap, String batchDate) {
		String skipListFilePath = "";
		if(null !=skippedEnrollmentMap && !skippedEnrollmentMap.isEmpty()){
			Writer writer = null;
			//			String irsFailureFolderPath = "C:/Data/IRSFailureFolderPath";// + File.separator + getDateTime() ;
			StringBuilder basePathBuilder = new StringBuilder();
			basePathBuilder
					.append(DynamicPropertiesUtil.getPropertyValue(
							EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_OUT_JOB_PATH))
					.append(File.separator).append("STAGING").append(File.separator).append("SkipLog");
			String irsFailureFolderPath = basePathBuilder.toString();
			try{
				EnrollmentUtils.createDirectory(irsFailureFolderPath);
				if(null != irsFailureFolderPath && new File(irsFailureFolderPath).exists()){
					String timeStampFailureFolderPath = irsFailureFolderPath + File.separator + batchDate;
					skipListFilePath = timeStampFailureFolderPath+File.separator + "PopulationJobSkippedRecords.txt";
					if(! new File(timeStampFailureFolderPath).exists()){
						new File(timeStampFailureFolderPath).mkdirs();
					}
					writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(skipListFilePath), "utf-8"));
					writer.write(skippedEnrollmentMap.toString());
				}
			}catch(Exception e){
				LOGGER.error("Exception in creating SkippedHousehold log :: " + e.getMessage(), e);
			}finally{
				if(writer != null){
					try {
						writer.close();
					} catch (IOException e) {
						LOGGER.error("Exception closing writer :: " + e.getMessage(), e);
					}
				}
			}
		}
		return skipListFilePath;
	}
	
	/**
	 * Get date time in String format yyyy-MM-dd_hh-mm-ss
	 * @param date
	 * @return String
	 */
	private String getDateTime(Date date) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");
		return df.format(date);
	}

	public EnrollmentAnnualIrsReportService getEnrollmentAnnualIrsReportService() {
		return enrollmentAnnualIrsReportService;
	}

	public void setEnrollmentAnnualIrsReportService(
			EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService) {
		this.enrollmentAnnualIrsReportService = enrollmentAnnualIrsReportService;
	}

	public Enrollment1095StagingParams getEnrollment1095StagingParams() {
		return enrollment1095StagingParams;
	}

	public void setEnrollment1095StagingParams(Enrollment1095StagingParams enrollment1095StagingParams) {
		this.enrollment1095StagingParams = enrollment1095StagingParams;
	}

	public Enrollment1095NotificationService getEnrollment1095NotificationService() {
		return enrollment1095NotificationService;
	}

	public void setEnrollment1095NotificationService(Enrollment1095NotificationService enrollment1095NotificationService) {
		this.enrollment1095NotificationService = enrollment1095NotificationService;
	}
}
