<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<!-- The following stylesheet link has already been moved to shell.jsp. 
	If you are working on this page try removing the stylesheet link.
	If the page works fine without it, just delete the entire line along with this comment. -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<%-- Fixes for HIX-22267  --%>
<style type="text/css">
.ui-autocomplete { 
	max-height: 100px;
	overflow-y: auto;
	overflow-x: hidden;
}
</style>
<%--  HIX-22267 ends --%> 

<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<%@ page isELIgnored="false"%>

<div class="gutter10">
	<div class="row-fluid">

		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p>
					<c:out value="${errorMsg}"></c:out>
				<p />
			</c:if>
			<br>
		</div>
		
			<h1>
				Employers &nbsp;<font size="4%"><small> <fmt:formatNumber
							type="number" value="${totalNoOfEmp}" /> total employers
						
			</h1>
		

	</div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
				<h4>
					<spring:message code="label.searchBy" />
					<a class="pull-right" href="../../crm/employer/searchemployer"><spring:message code="label.crmResetAll" /></a>
				</h4>
			</div>
			<form class="form-vertical gutter10" method="POST"
				action="<c:url value="/crm/employer/searchemployer" />"
				id="employerSearch" name="employerSearch" novalidate="novalidate">
				<df:csrfToken/>
				<!-- <input type="hidden" name="filter-flow" id="filter-flow" value="true" /> -->
				<div class="control-group">
					<label class="control-label" text-transform="uppercase"><spring:message
							code="label.employerName" /> <a href="#" rel="tooltip"
						data-placement="top"
						data-original-title="Please enter first 3 characters of Employer Name 
							"
						class="info"><i class="icon-question-sign customMargin"></i></a> </label>

					<div class="controls">
						<!--  -->
						<input class="input-medium" type="text"
							placeholder="Employer Name" name="employerName" id="employerName"
							value="${searchCriteria.employerName}" maxlength="400"
							onkeypress="javascript:onEmpNameChangeEvent();">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><spring:message
							code="label.employerAddress" /></label>
					<div class="controls">
						<input class="input-medium" type="text" placeholder="Address"
							name="employerAddress" id="employerAddress" maxlength="1020"
							value="${searchCriteria.employerAddress}">
					</div>
				</div>
				<div class="control-group">
					<!-- Add jquery to allow numbers only -->
					<label for="contactNumber" class="control-label required"
						text-transform="uppercase"><spring:message
							code="label.crmPhone" /></label>
					<div class="controls">
						<input class="input-medium" type="text" placeholder="Phone Number"
							name="contactNumber" id="contactNumber"
							value="${searchCriteria.contactNumber}" maxlength="10">
						<div id="contactNumber_error"></div>
					</div>
				</div>
				<div class="control-group">
					<label for="zipCode" class="control-label required"><spring:message code='label.columnHeaderZip' /></label>
					<div class="controls">
						<input class="input-medium" type="text" placeholder="zip" name="zipCode" id="zipCode" value="${searchCriteria.zipCode}" maxlength="5"> 
					</div>
				</div>
				<div class="control-group">
					<label for="zipCode" class="control-label required"><spring:message code='label.columnHeaderState' /></label>
					<div class="controls">
						<select id="state" class="input-medium" name="state">
							<option value="">Select</option>
							<c:forEach var="employerState" items="${statelist}">
								<option value="${employerState.code}"
									<c:if test="${employerState.code == searchCriteria.state}" >selected</c:if>>${employerState.name}</option>
							</c:forEach>
						</select>
						<div id="zipCode_error"></div>
					</div>
				</div>
				<div class="control-group">
					<!-- Add jquery to allow numbers only -->
					<label for="federalEin" class="control-label required"><spring:message
							code="label.ein" /></label>
					<div class="controls">
						<input class="input-medium" type="text" placeholder="Federal EIN"
							name="federalEin" id="federalEin"
							value="${searchCriteria.federalEin}" maxlength="9">
						<div id="federalEin_error"></div>
					</div>
				</div>
				<div class="txt-center">
					<input type="submit" class="btn"
						value="<spring:message  code='label.go'/>"
						title="<spring:message  code='label.go'/>" id="filter-flow"
						name="filter-flow">
				</div>
			</form>
		</div>
		<div class="span9" id="rightpanel">
			<form class="form-horizontal" id="frmSearchEmployer"
				name="frmSearchEmployer"
				action="<c:url value="/crm/employer/searchemployer" />"
				method="POST">
				<df:csrfToken/>
				<!-- below hidden field is to avoid fetching data on first load -->
				<c:choose>
					<c:when test="${not empty onPageLoad}">
						<h4 class="alert alert-info">
							<spring:message code="label.filterSearchCriteria" />
						</h4>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${resultSize >= 1}">
								<c:choose>
									<c:when test="${fn:length(employerList) > 0}">
										<table class="table table-striped">
											<thead>
												<tr class="header">
													<spring:message code='label.columnHeaderName'
														var="employerName" />

													
													<th class="sortable" scope="col" style="width:135px;"><dl:sort
															title="${employerName}" sortBy="employer.name"
															sortOrder="${searchCriteria.sortOrder}"></dl:sort> <c:choose>
															<c:when
																test="${searchCriteria.sortOrder == 'ASC' && searchCriteria.sortBy == 'employer.name'}">
																<img
																	src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
																	alt="State sort ascending" />
															</c:when>
															<c:when
																test="${searchCriteria.sortOrder == 'DESC' && searchCriteria.sortBy == 'employer.name'}">
																<img
																	src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
																	alt="State sort descending" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose></th>



													<th style="width:150px;"><spring:message code='label.columnHeaderAddress' /></th>
													<th style="width:45px;"><spring:message code='label.columnHeaderZip' /></th>
													<th style="width:85px; text-align:center;"><spring:message code='label.columnHeaderPhone' /></th>
													<th style="width:75px; text-align:center;"><spring:message code='label.columnHeaderEin' /></th>
													<th style="width:70px; text-align:center;"><spring:message code='label.employerTickets' /></th>
													<th scope="col"><i class="icon-cog"></i></th>
												</tr>
											</thead>

											<c:forEach items="${employerList}" var="employerLoc"
												varStatus="vs">
												<tr>
										
													<td><a
														href="<c:url value="/crm/crmemployer/details/${employerLoc.employerid}"/>">${employerLoc.employername}</a></td>
													<td>${employerLoc.locationaddress1}</td>
													<td>${employerLoc.locationzip}</td>
													<td style="text-align:center;">${employerLoc.employercontactNumber}</td>
													<!-- ${employerLoc.employerfederalEIN} -->

													<td style="text-align:center;"><c:if
															test="${not empty employerLoc.employerfederalEIN}">
															<c:set var="myValue">${employerLoc.employerfederalEIN}</c:set>
															<c:set var="einLength">${fn:length(myValue)}</c:set>
													${fn:substring(employerLoc.employerfederalEIN, einLength-4, einLength)}
												</c:if></td>
													<td style="text-align:center;">${employerLoc.employerTicketsCount}</td>
												<td style ="white-space:nowrap">
												<c:if test="${not MODULE_ID_LIST.contains(employerLoc.employerid)}">
												<div >
													<div class="dropdown">
														<a href="/page.html" data-target="#"
															data-toggle="dropdown" role="button" id="dLabel"
															class="dropdown-toggle"> <i
															class="icon-cog"></i> <b class="caret"></b>
														</a>
														<ul  aria-labelledby="dLabel" role="menu"
															class="dropdown-menu pull-right">
															<li><a href="<c:url value="/crm/employer/resendActivationLink/${employerLoc.employerid}"/>"
																class="">Resend Activation Link</a></li>
														</ul>
													</div>
												</div>
												</c:if>
												</td>
												</tr>
											</c:forEach>
										</table>
										<div class="pagination txt-center">
											<dl:paginate resultSize="${resultSize + 0}"
												pageSize="${pageSize + 0}" />
										</div>
									</c:when>
								</c:choose>
							</c:when>
							<c:otherwise>
								<h4 class="alert alert-info">
									<spring:message code='label.norecords' />
								</h4>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
				<%-- </c:when>
			<c:otherwise>
				<h4 class="alert alert-info">
						<spring:message code="label.filterSearchCriteria"/>
				</h4>
			</c:otherwise>
			</c:choose> --%>
			</form>
		</div>
	</div>



	<div class="row-fluid">
		<div class="notes" style="display: none">
			<div class="row">
				<div class="span">
					<p>The prototype showcases three scenarios (A, B and C)
						dependant on a particular Employer's eligibility to use the SHOP
						Exchange.</p>
				</div>
			</div>
		</div>

	</div>
	<!-- row-fluid -->
</div>

<script type="text/javascript">
	var validator = $("#employerSearch")
			.validate(
					{
						rules : {
							'contactNumber' : {
								minlength : 10,
								number : true
							},
							'federalEin' : {
								minlength : 9,
								number : true
							},
							'zipCode' : {
								minlength : 5,
								number : true
							}
						},
						messages : {
							'contactNumber' : {
								minlength : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNoFilterLength' javaScriptEscape='true'/></span>",
								number : "<span> <em class='excl'>!</em><spring:message code='label.validateNumericFilterValue' javaScriptEscape='true'/></span>"
							},
							'federalEin' : {
								minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateEINFilterLength' javaScriptEscape='true'/></span>",
								number : "<span> <em class='excl'>!</em><spring:message code='label.validateNumericFilterValue' javaScriptEscape='true'/></span>"
							},
							'zipCode' : {
								minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateZipFilterLength' javaScriptEscape='true'/></span>",
								number : "<span> <em class='excl'>!</em><spring:message code='label.validateNumericFilterValue' javaScriptEscape='true'/></span>"
							}
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							if ($("#" + elementId + "_error").html() != null) {
								$("#" + elementId + "_error").html('');
							}
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error")
									.attr('class', 'error');
						}
					});
</script>


<script>
	$(function() {
		$('.datepick').each(function() {
			var ctx = "${pageContext.request.contextPath}";
			var imgpath = ctx + '/resources/images/calendar.gif';
			$(this).datepicker({
				showOn : "button",
				buttonImage : imgpath,
				buttonImageOnly : true
			});
		});
	});
</script>

<script type="text/javascript">
	var empFilter = "";

	var availableEmployer = new Array();

	$('.info').tooltip();

	function fetchEmployerList() {
		$("#requester").empty();
		var validateUrl = '<c:url value="fetchemployerlist"></c:url>';
		var employerName = $("#employerName").val();

		$.ajax({
			url : validateUrl,
			type : "POST",
			data : {
				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				empName : employerName
			},
			success : function(response, xhr) {
		        if(isInvalidCSRFToken(xhr))                                  
		          return;

				autoCompleteEmployer(response);
			},
			error : function(e) {
				//alert("Failed to Add subtype");
			},
		});

	}

	function onEmpNameChangeEvent() {
		var txtvalue = $("#employerName").val();
		var len = txtvalue.length;

		if (len >= 1 && empFilter != txtvalue.substring(0, 1)) {
			empFilter = txtvalue;
			fetchEmployerList();
		}
		autoCompleteBox();
	}

	function autoCompleteEmployer(userListJSON) {
		userListJSON = JSON.parse(userListJSON);
		var index = 0;
		for ( var key in userListJSON) {
			availableEmployer[index] = {
				label : userListJSON[key],
				idx : key
			};
			index++;
		}
	}

	function autoCompleteBox() {
		$("#employerName").autocomplete({
			source : availableEmployer,
			change : function(event, ui) {
				if (ui.item == null)
					$("#employerName").val('');
			}
		});
	}
</script>

