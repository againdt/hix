<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ include file="modaltemplate.jsp" %>
<%@ include file="modaltemplatesmall.jsp" %>
<%@ include file="modaltemplatetiny.jsp" %>
<%@ include file="modaltemplatetiny-saving.jsp" %>

<%@ include file="maritalstatus-address.jsp" %>
<%@ include file="maritalstatus-complete.jsp" %>
<%@ include file="maritalstatus-details.jsp" %>
<%@ include file="maritalstatus-ethnicityrace.jsp" %>
<%@ include file="maritalstatus-income.jsp" %>
<%@ include file="maritalstatus-incomededuction.jsp" %>
<%@ include file="maritalstatus-incomesummary.jsp" %>
<%@ include file="maritalstatus-other.jsp" %>
<%@ include file="maritalstatus-special.jsp" %>
<%@ include file="maritalstatus-ssn.jsp" %>
<%@ include file="maritalstatus-summary.jsp" %>
<%@ include file="maritalstatus.jsp" %>
<jsp:include  page="maritalstatus-citizenship.jsp" />
<%@ include file="maritalstatus-incarceration.jsp" %>

<%@ include file="changeinaddress-complete.jsp" %>
<%@ include file="changeinaddress.jsp" %>

<%@ include file="gainofmin-complete.jsp" %>

<%@ include file="gainothermin.jsp" %>

<%@ include file="incarceration-next.jsp" %>
<%@ include file="incarceration.jsp" %>
<%@ include file="nextIncarceration.jsp" %>

<%@ include file="shared-relationship.jsp" %>

<%-- <%@ include file="income-alimony.jsp" %>
<%@ include file="income-capitalgains.jsp" %>
<%@ include file="income-complete.jsp" %>
<%@ include file="income-farming.jsp" %>
<%@ include file="income-invest.jsp" %>
<%@ include file="income-job.jsp" %>
<%@ include file="income-other.jsp" %>
<%@ include file="income-rental.jsp" %>
<%@ include file="income-retire.jsp" %>
<%@ include file="income-self.jsp" %>
<%@ include file="income-ssb.jsp" %>
<%@ include file="income-summary.jsp" %>
<%@ include file="income-unemployment.jsp" %>
<%@ include file="income.jsp" %> --%>

<jsp:include  page="lawfulpresence.jsp" />
<%@ include file="lawfulpresence-complete.jsp" %>

<%@ include file="lossofmin-complete.jsp" %>

<%@ include file="lossofmin.jsp" %>

<jsp:include page="namechange-complete.jsp" />
<jsp:include page="relationshipchange.jsp" />
<jsp:include page="relationshipcomplete.jsp" />

<jsp:include page="namechange.jsp" />

<%@ include file="numberofdependents.jsp" %>
<%@ include file="numofdep-add.jsp" %>
<%@ include file="numofdep-complete.jsp" %>
<%@ include file="numofdep-remove.jsp" %>

<%@ include file="other.jsp" %>
<%@ include file="other-complete.jsp" %>

<%@ include file="review-dummy.jsp" %>

<%@ include file="summary.jsp" %>

<%@ include file="tribechange-complete.jsp" %>

<%@ include file="tribechange.jsp" %>

<style>
.header > li {
  text-align: right;
}.header > li > .active {
  display: list-item;
}
</style>
<script type="text/ng-template" id="next.jsp">
  <div class="gutter10">
    <div class="row-fluid">
      <div id="sidebar" class="span3">
        <div class="header">
          <h4>
				<!-- <span class="margin10-r" style="display: block;" ng-if="showButton"><a class="btn btn-primary" href="/hix/iex/lce/reportyourchange?coverageYear=${coverageYear}" target="_self" data-toggle="tooltip" rel="tooltip" data-placement="right" data-original-title="<spring:message code="label.lce.startOverTooltip"/>"><spring:message code="label.lce.deleteStartOver"/></a></span> -->
				<span class="margin10-t" style="display: block;"><spring:message code="label.lce.sidebar.leftheader"/></span>  
		  </h4>
        </div><!-- header -->
        <ul class="nav nav-list sidebar" ng-repeat="item in selectedEvents | filter:'true'" id="stuff{{$index}}">
          <li class="active" id="{{item.eventCode}}">{{item.eventName}}</li>
          <li ng-repeat="event in item.eventdate" class="subcategories" id="sub{{item.eventCode}}{{$index}}">
            <span class="margin10-r">{{event}}</span>
          </li>
          <li ng-repeat="sub in item.subcategories" class="subcategories2 hide" id="sub{{item.eventCode}}{{$index+1}}">
            <span class="margin10-r">{{sub}}</span>
          </li>
        </ul>
        <ul class="nav nav-list sidebar">
          <li class="active" id="reviewSubmit"><spring:message code="label.lce.sidebar.reviewandsubmit"/></li>
        </ul>
        <div class="back-home">
        	<a href="/hix/iex/lce/reportyourchange?coverageYear=${coverageYear}" target="_self" class="btn btn-primary" data-toggle="tooltip" rel="tooltip" data-placement="right" data-original-title="<spring:message code="label.lce.startOverTooltip"/>"><spring:message code="label.lce.deleteStartOver"/></a>

        </div>
      </div><!-- sidebar -->
      <div id="rightpanel" class="span9">
        <div class="header">
          <h4><spring:message code="label.lce.maintitle"/>: {{lceEvent}}</h4>
        </div><!-- header -->
        <div ng-include="currentPage"></div>
      </div><!-- rightpanel -->
    </div><!-- row-fluid -->
  </div><!-- gutter10 -->
</script>