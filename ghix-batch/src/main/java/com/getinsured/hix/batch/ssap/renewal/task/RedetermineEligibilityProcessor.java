package com.getinsured.hix.batch.ssap.renewal.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.ssap.renewal.service.RedetermineEligibilityService;
import com.getinsured.hix.batch.ssap.renewal.util.RedetermineEligibilityPartitionerParams;
import com.getinsured.iex.ssap.model.SsapApplication;

@Component
public class RedetermineEligibilityProcessor implements ItemProcessor<SsapApplication,SsapApplication> {

	private static final Logger LOGGER = LoggerFactory.getLogger(RedetermineEligibilityProcessor.class);

	private RedetermineEligibilityService redetermineEligibilityService;
	private RedetermineEligibilityPartitionerParams redetermineEligibilityPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Processor ");

		ExecutionContext executionContext = stepExecution.getExecutionContext();

		if (executionContext != null) {
			int partition = executionContext.getInt("partition");
			int startIndex = executionContext.getInt("startIndex");
			int endIndex = executionContext.getInt("endIndex");
			LOGGER.info("partition: {}, startIndex: {}, endIndex: {}", partition, startIndex, endIndex);
		}
	}

	@Override
	public SsapApplication process(SsapApplication ssapApplication) throws Exception {
		LOGGER.info("Returning SsapApplication is null : {}", (null == ssapApplication));
		return ssapApplication;
	}

	public RedetermineEligibilityService getRedetermineEligibilityService() {
		return redetermineEligibilityService;
	}

	public void setRedetermineEligibilityService(RedetermineEligibilityService redetermineEligibilityService) {
		this.redetermineEligibilityService = redetermineEligibilityService;
	}

	public RedetermineEligibilityPartitionerParams getRedetermineEligibilityPartitionerParams() {
		return redetermineEligibilityPartitionerParams;
	}

	public void setRedetermineEligibilityPartitionerParams(
			RedetermineEligibilityPartitionerParams redetermineEligibilityPartitionerParams) {
		this.redetermineEligibilityPartitionerParams = redetermineEligibilityPartitionerParams;
	}
}
