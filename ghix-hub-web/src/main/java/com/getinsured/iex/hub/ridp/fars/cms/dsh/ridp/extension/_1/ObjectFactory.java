
package com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.extension._1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.cms.dsh.ridp.extension._1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DSHReferenceNumber_QNAME = new QName("http://ridp.dsh.cms.gov/extension/1.0", "DSHReferenceNumber");
    private final static QName _FinalDecisionCode_QNAME = new QName("http://ridp.dsh.cms.gov/extension/1.0", "FinalDecisionCode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.cms.dsh.ridp.extension._1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ResponsePayloadType }
     * 
     */
    public ResponsePayloadType createResponsePayloadType() {
        return new ResponsePayloadType();
    }

    /**
     * Create an instance of {@link RequestPayloadType }
     * 
     */
    public RequestPayloadType createRequestPayloadType() {
        return new RequestPayloadType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ridp.dsh.cms.gov/extension/1.0", name = "DSHReferenceNumber")
    public JAXBElement<String> createDSHReferenceNumber(String value) {
        return new JAXBElement<String>(_DSHReferenceNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinalDecisionCodeSimpleType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ridp.dsh.cms.gov/extension/1.0", name = "FinalDecisionCode")
    public JAXBElement<FinalDecisionCodeSimpleType> createFinalDecisionCode(FinalDecisionCodeSimpleType value) {
        return new JAXBElement<FinalDecisionCodeSimpleType>(_FinalDecisionCode_QNAME, FinalDecisionCodeSimpleType.class, null, value);
    }

}
