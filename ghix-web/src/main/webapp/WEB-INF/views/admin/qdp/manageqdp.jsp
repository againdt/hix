<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="ghix" uri="/WEB-INF/tld/dropdown-lookup.tld"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@page import="com.getinsured.hix.model.Plan"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>


<!-- start of secondary navbar -->
<style>

.updateStausError{
    color : red;
}

</style>
<%
	Integer pageSizeInt = (Integer) request.getAttribute("pageSize");
	String reqURI = (String) request.getAttribute("reqURI");	
	Integer currentPage = (Integer) request.getAttribute("currentPage");
%>
<div class="gutter10">
	<div class="row-fluid">
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/admin/planmgmt/manageqhpplans"/>"><spring:message
						code="label.healthplansTitle" /></a></li>
			<li><a href="<c:url value="/admin/planmgmt/manageqdpplans"/>"><spring:message
						code='label.linkManageQDP' /></a></li>
			<li><spring:message code='label.qdpFull' /></li>
		</ul>
		<!--page-breadcrumb ends-->
		<h1>
			<a name="skip"></a>
			<spring:message code='label.qdpFull' />
			<small>${resultSize} <spring:message code='label.totalplans' />
				<c:if test="${showPendingPlanCtr == 1}"> : ${pendingPlanCtr} <spring:message
						code='label.planspending' />
				</c:if></small>
		</h1>
	</div>

	<div class="row-fluid">
		<div class="span3"></div>
		<div class="pull-left  span6">
			<input type="checkbox" id="selectAllPlans" name="selectAllPlans" value="" > <spring:message  code='label.selectAllPlans'/>
		</div>
      	<div class="pull-right">
           	<label class="inline-block margin10-r" for="planYear" style="vertical-align: super;">Plan Year </label>
            <select id="planYear" name="planYear" class="input-medium">
            	<c:if test="${fn:length(planYearsList) <= 0}"><option value="">Select Year</option></c:if>
                <c:forEach var="planYearsList" items="${planYearsList}">
                 	<option <c:if test="${planYearsList.planYearsKey == selectedPlanYear.planYearsKey}">selected</c:if> value="<c:out value="${planYearsList.planYearsKey}"/>">${planYearsList.planYearsKey}</option>
              	</c:forEach>
          	</select>
	  </div>
    </div>
	<!-- end of page-header -->
	<form class="form-vertical gutter10" id="frmfindplan"
		name="frmfindplan" action="/hix/admin/planmgmt/manageqdpplans"
		method="POST">
	<div class="row-fluid">
		<!-- New sidebar to reflect wireframe -->
		<div class="span3" id="sidebar">
			<div class="header">
				<h4 class="margin0">
					<spring:message code='label.refineresult' />
				</h4>
			</div>
			<div class="graybg">
					<df:csrfToken/>
					
					<input type="hidden" id="sortBy" name="sortBy" value="lastUpdateTimestamp">
					<input type="hidden" id="sortOrder" name="sortOrder" value="DESC" >
					<input type="hidden" id="pageNumber" name="pageNumber" value="1">
					<input type="hidden" id="changeOrder" name="changeOrder" >
					<input type="hidden" id="planYearSelected" name="planYearSelected">
					
					<div class="control-group">
						<label for="planNumber" class="control-label"> <spring:message code='label.planNumber' /></label>
						<div class="controls">
							<input type="text" name="planNumber" id="planNumber"
								value="${planNumber}" class="span12" maxlength="16">
							<div id="planNumber_error" class=""></div>
						</div> <!-- end of controls-->
					</div>
					<!-- end of control-group -->
					<div class="control-group">
						<label for="issuername" class="control-label"> <spring:message code='label.issuerName' /></label>
						<div class="controls">
							<select id="issuername" name="issuerid" class="span12">
								<option value="">
									<spring:message code='label.selectIssuer' />
								</option>
								<c:forEach items="${issuers}" var="issuer">
										<option
											<c:if test="${issuer[0] == issuerid}">selected</c:if>
											value='${issuer[0]}'>${issuer[1]}</option>
								</c:forEach>
							</select>
						</div> <!-- end of controls-->
						<!-- end of label -->
					</div>
				
					<c:if test="${supportPlanLevel == 'YES'}">
						<div class="control-group">
							<label for="planLevel" class="control-label"><spring:message code="label.planLevel" /></label>
							<div class="controls">
								<ghix:dropdown id="planLevel" name="planLevel" cssclass="span12"
									lookuptype="QDP_PLAN_LEVEL" value="${selectedPlanLevel}" />
							</div>
							<!-- end of controls-->
						</div>
						<!-- end of control-group -->
					</c:if>
			      <%--  </c:if> --%>
					<%-- <div class="control-group">
						<label for="market" class="control-label"> <spring:message code="label.planMarket" /></label>
						<div class="controls">
							<select id="market" name="market" class="span12">
								<option value="">
									<spring:message code='label.any' />
								</option>
								<c:forEach var="marketList" items="${marketList}">
									<c:if test="${marketList.value != 'SHOP'}">
										<option <c:if test="${market==marketList.key}">selected</c:if> value="${marketList.key}">${marketList.value}</option>
									</c:if>
								</c:forEach>
							</select>
						</div> <!-- end of controls-->
						<!-- end of label -->
					</div> --%>
					<div class="control-group">
						<label for="status" class="control-label"> <spring:message code='label.status.plan' /></label>
						<div class="controls">
							<select id="status" name="status" class="span12">
								<option value="">
									<spring:message code='label.selectStatus' />
								</option>
								<c:forEach var="qdpcertiStatus" items="${qdpcertiStatusList}">
									<option
										<c:if test="${status==qdpcertiStatus.key}">selected</c:if>
										value="${qdpcertiStatus.key}">${qdpcertiStatus.value}</option>
								</c:forEach>
							</select>
						</div> <!-- end of controls -->
						<!-- end of label -->
					</div>
					<div class="control-group">
						<label for="verified" class="control-label"> <spring:message code='label.verified' /></label>
						<div class="controls">
							<select id="verified" name="verified" class="span12">
								<option value="">
									<spring:message code='label.any' />
								</option>
								<c:forEach var="verifiedList" items="${verifiedList}">
									<option
										<c:if test="${verified==verifiedList.key}">selected</c:if>
										value="${verifiedList.key}">${verifiedList.value}</option>
								</c:forEach>
							</select>
						</div> <!-- end of controls -->
						<!-- end of label -->
					</div>
					<c:if test="${exchangeType == 'PHIX'}">
						<div class="control-group">
							<label for="exType" class="control-label"><spring:message code='label.exchangeType' /></label>
							<div class="controls">
								<select id="exType" name="exType" class="span12">
									<option value="">
										<spring:message code='label.any' />
									</option>
									<c:forEach var="exTypeList" items="${exTypeList}">
										<option <c:if test="${exType==exTypeList.key}">selected</c:if>
											value="${exTypeList.key}">${exTypeList.value}</option>
									</c:forEach>
								</select>
							</div>
							<!-- end of controls-->
						</div>
						<!-- end of control-group -->
					</c:if>
					<div class="control-group">
							<label for="enrollmentAvailability" class="control-label"><spring:message
									code='label.enrollAvail' /></label>
							<div class="controls">
								<select id="enrollmentAvailability" name="enrollmentAvailability" class="span12">
									<option value="">
										<spring:message code='label.any' />
									</option>
									<c:forEach var="enrollmentAvailList" items="${enrollmentAvailList}">
										<option <c:if test="${enrollmentAvailability==enrollmentAvailList.key}">selected</c:if>
											value="${enrollmentAvailList.key}">${enrollmentAvailList.value}</option>
									</c:forEach>
								</select>
							</div>
							<!-- end of controls-->
						</div>

					<!-- end of control-group -->
					<div class="control-group txt-center">
						<div class="controls">
							<input type="submit" class="btn" value=" Go " id="go">
						</div>
					</div>

				<c:if test="${displayAddNewQDPButton == 'YES'}">
					<a href="<c:url value="/admin/planmgmt/submitqdp" />"
						class="btn btn-primary btn-block"><i
						class="icon-plus-sign icon-white"></i> <spring:message
							code='label.addNewQDP' /></a>
				</c:if>
			</div>

		</div>


		<!-- start of span 9 -->
		<div class="span9" id="rightpanel">
			
			<c:choose>
				<c:when test="${fn:length(plans) > 0}">
					<table
						class="table table-condensed table-border-none table-striped">
						<thead>
							<tr class="header">
								<spring:message code='label.planNumber' var="planNumberVal" />
								<spring:message code='label.planName' var="planNameVal" />
								<spring:message code='label.issuer' var="issuerVal" />
								<spring:message code='label.level' var="levelVal" />
								<%-- <spring:message code='label.Market' var="marketVal" /> --%>
								<spring:message code='label.lastUpdate' var="lastUpdateVal" />
								<spring:message code='label.status.plan' var="statusVal" />
								<spring:message code='label.verified' var="verifiedVal" />
								<spring:message code='label.state' var="stateVal" />
								<spring:message code='label.enrollAvail' var="enrollment" />
								
								<c:if test="${exchangeType == 'STATE'}">
										<th scope="col" style="width: 10px;"><input type="checkbox" name="updatePlan" value="updatePlan" class="updatePlanHeader"></th>
								</c:if>
								
								<th class="sortable" scope="col" style="width: 90px;"><dl:sort
										title="${planNumberVal}" sortBy="planNumber" customFunctionName="traverse"
										sortOrder="${sortOrder}"></dl:sort> <c:choose>
										<c:when test="${sortOrder == 'ASC' && sortBy == 'planNumber'}">
											<img
												src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
												alt="Plan number sort ascending" />
										</c:when>
										<c:when
											test="${sortOrder == 'DESC' && sortBy == 'planNumber'}">
											<img
												src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
												alt="Plan number sort descending" />
										</c:when>
										<c:otherwise></c:otherwise>
									</c:choose></th>
								<th class="sortable" scope="col" style="width: 150px;"><dl:sort
										title="${planNameVal}" sortBy="name" customFunctionName="traverse" sortOrder="${tempOrder}"></dl:sort>
									<c:choose>
										<c:when test="${sortOrder == 'ASC' && sortBy == 'planName'}">
											<img
												src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
												alt="Plan name sort ascending" />
										</c:when>
										<c:when test="${sortOrder == 'DESC' && sortBy == 'planName'}">
											<img
												src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
												alt="Plan name sort descending" />
										</c:when>
										<c:otherwise></c:otherwise>
									</c:choose></th>
								<th class="sortable" scope="col" style="width: 60px;"><dl:sort
										title="${issuerVal}" sortBy="issuer" customFunctionName="traverse" sortOrder="${tempOrder}"></dl:sort>
									<c:choose>
										<c:when test="${sortOrder == 'ASC' && sortBy == 'issuer'}">
											<img
												src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
												alt="Issuer sort ascending" />
										</c:when>
										<c:when test="${sortOrder == 'DESC' && sortBy == 'issuer'}">
											<img
												src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
												alt="Issuer sort descending" />
										</c:when>
										<c:otherwise></c:otherwise>
									</c:choose></th>
								<c:if test="${supportPlanLevel == 'YES'}">
									<th class="sortable" scope="col" style="width: 48px;"><dl:sort
											title="${levelVal}" sortBy="level" customFunctionName="traverse" sortOrder="${tempOrder}"></dl:sort>
										<c:choose>
											<c:when test="${sortOrder == 'ASC' && sortBy == 'level'}">
												<img
													src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
													alt="Plan level display sort ascending" />
											</c:when>
											<c:when test="${sortOrder == 'DESC' && sortBy == 'level'}">
												<img
													src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
													alt="Plan level display sort descending" />
											</c:when>
											<c:otherwise></c:otherwise>
										</c:choose></th>
								</c:if>
								<th class="sortable" scope="col" style="width: 60px;"><dl:sort
												title="${enrollment}" sortBy="enrollment" customFunctionName="traverse" 
												sortOrder="${tempOrder}"></dl:sort> <c:choose>
												<c:when
													test="${sortOrder == 'ASC' && sortBy == 'enrollment'}">
													<img
														src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
														alt="Enrollment Availability sort ascending" />
												</c:when>
												<c:when
													test="${sortOrder == 'DESC' && sortBy == 'enrollment'}">
													<img
														src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
														alt="Enrollment Availability sort descending" />
												</c:when>
												<c:otherwise></c:otherwise>
											</c:choose>
										</th>
								<th class="sortable" scope="col" style="width: 68px;"><dl:sort
										title="${lastUpdateVal}" sortBy="lastUpdateTimestamp" customFunctionName="traverse"
										sortOrder="${tempOrder}"></dl:sort> <c:choose>
										<c:when test="${sortOrder == 'ASC' && sortBy == 'lastUpdateTimestamp'}">
											<img
												src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
												alt="Last update sort ascending" />
										</c:when>
										<c:when test="${sortOrder == 'DESC' && sortBy == 'lastUpdateTimestamp'}">
											<img
												src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
												alt="Last update sort descending" />
										</c:when>
										<c:otherwise></c:otherwise>
									</c:choose></th>
								
								<th class="sortable" scope="col" style="width: 55px;"><dl:sort
										title="${statusVal}" sortBy="planStatus" customFunctionName="traverse" 
										sortOrder="${tempOrder}"></dl:sort> <c:choose>
										<c:when test="${sortOrder == 'ASC' && sortBy == 'planStatus'}">
											<img
												src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
												alt="Plan status sort ascending" />
										</c:when>
										<c:when
											test="${sortOrder == 'DESC' && sortBy == 'planStatus'}">
											<img
												src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
												alt="Plan status sort descending" />
										</c:when>
										<c:otherwise></c:otherwise>
									</c:choose></th>
								<th class="sortable" scope="col" style="width: 60px;"><dl:sort
										title="${verifiedVal}" sortBy="Verified" customFunctionName="traverse" 
										sortOrder="${tempOrder}"></dl:sort> <c:choose>
										<c:when test="${sortOrder == 'ASC' && sortBy == 'verified'}">
											<img
												src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
												alt="Plan status sort ascending" />
										</c:when>
										<c:when test="${sortOrder == 'DESC' && sortBy == 'verified'}">
											<img
												src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
												alt="Plan status sort descending" />
										</c:when>
										<c:otherwise></c:otherwise>
									</c:choose></th>
								<c:if test="${exchangeType != 'PHIX'}">
									<th class="dropdown" scope="col" style="width: 30px;">	
										<a id="modifyPlansBulk" class="dropdown-toggle disabled" data-toggle="dropdown" href="#" onclick="clearText();">
											<i class="icon-cog"></i><i class="caret" style="margin-top:0;"></i>
											</a>
										<ul class="dropdown-menu right" role="menu" aria-labelledby="dLabel">								 		
								 		<li><a href="#" id="updateCertificationStatus" data-modal="modal" ><spring:message code='label.updateCertificationStatus' /> </a></li>
								 		<li><a href="#" id="updateEnrollmentAvblStatus" data-modal="modal" ><spring:message code='label.updateEnrollmentAvblStatus' /> </a></li>    
									</ul>
									</th>
								</c:if>
							</tr>
						</thead>
						<c:forEach items="${plans}" var="plan">
							<c:set var="encPlanId" ><encryptor:enc value="${plan.id}" isurl="true"/> </c:set>		
							<tr>
								<td> <input type="checkbox" name="updatePlan" value="${plan.id}_${plan.issuerPlanNumber}" class="updatePlanRows" > </td>
								
								<td id="name_${plan.id}"><a
									href="<c:url value='/admin/planmgmt/viewqdpdetail/${encPlanId}'/>">${plan.issuerPlanNumber}</a></td>
								<td>${plan.name}</td>
								<td align="center">${plan.issuer.name}</td>
								<c:if test="${supportPlanLevel == 'YES'}">
									<td align="center">
										${fn:toUpperCase(fn:substring(plan.planDental.planLevel, 0,
										1))}${fn:toLowerCase(fn:substring(plan.planDental.planLevel,
										1, -1))}</td>
								</c:if>
									
									<td>
										<%
											pageContext.setAttribute("NOTAVAILABLE", Plan.EnrollmentAvail.NOTAVAILABLE);
										%>
										<%-- if enrollment availability either available/dependents only and enrollment available date > than today show availability as not available --%>
										<c:choose>
											<c:when	test="${plan.enrollmentAvail != NOTAVAILABLE && plan.enrollmentAvailEffDate > now}"><spring:message code='label.notAvailable' /></c:when>
											<c:when test="${fn:toUpperCase(plan.enrollmentAvail) == 'AVAILABLE'}"><spring:message code='label.available' /></c:when>
											<c:when	test="${fn:toUpperCase(plan.enrollmentAvail) == 'DEPENDENTSONLY'}">	<spring:message code='label.dependentsOnly' /></c:when>
											<c:when	test="${fn:toUpperCase(plan.enrollmentAvail) == 'NOTAVAILABLE'}"><spring:message code='label.notAvailable' /></c:when>
											<c:otherwise>${plan.enrollmentAvail}</c:otherwise>
										</c:choose>
									</td>
								<td><fmt:formatDate type="both" pattern="MMM dd, yyyy"
										value="${plan.lastUpdateTimestamp}" timeZone="${timezone}" /></td>
								<td>
											<c:choose>
												<c:when test="${fn:toUpperCase(plan.status) == 'CERTIFIED'}"><spring:message code='label.certified' /></c:when>
												<c:when test="${fn:toUpperCase(plan.status) == 'LOADED'}"><spring:message code='label.loaded' /></c:when>
												<c:when test="${fn:toUpperCase(plan.status) == 'DECERTIFIED'}"><spring:message code='label.deCertified' /></c:when>
												<c:when test="${fn:toUpperCase(plan.status) == 'NONCERTIFIED'}"><spring:message code='label.nonCertified' /></c:when>
												<c:when test="${fn:toUpperCase(plan.status) == 'RECERTIFIED'}"><spring:message code='label.reCertified' /></c:when>
												<c:when test="${fn:toUpperCase(plan.status) == 'INCOMPLETE'}"><spring:message code='label.incomplete' /></c:when>
												<c:otherwise>${plan.status}</c:otherwise>
											</c:choose>
								</td>
								<td class="txt-center"><c:choose>
										<c:when test="${plan.issuerVerificationStatus == 'VERIFIED'}">
											<spring:message code='label.yes' />
										</c:when>
										<c:otherwise>
											<spring:message code='label.no' />
										</c:otherwise>
									</c:choose></td>
								<c:if test="${exchangeType != 'PHIX'}">
									<td align="right">
										<div class="dropdown">
											<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i
												class="icon-cog"></i><i class="caret"></i></a>
											<ul class="right dropdown-menu " role="menu"
												aria-labelledby="dLabel">
												<li><a
													href="<c:url value="/admin/planmgmt/viewqdpdetail/${encPlanId}" />"><i
														class="icon-pencil"
														<c:if test="${plan.planDental.parentPlanId != 0}"> disabled </c:if>></i>
													<spring:message code='label.editColumnHeading' /></a></li>
												<li><a
													href="<c:url value="/admin/planmgmt/editqdpcertification/${encPlanId}"/>"><i
														class="icon-refresh"></i>
													<spring:message code='label.updateStatus' /></a></li>
											</ul>
										</div>
									</td>
								</c:if>
							</tr>

						</c:forEach>
					</table>
					<div class="center">
						<dl:paginate customFunctionName="traverse" resultSize="${resultSize + 0}"
							pageSize="${pageSize + 0}" hideTotal="true" />
					</div>
				</c:when>
				<c:otherwise>
					<hr />
					<div class="alert alert-info">
						<spring:message code='label.notification' />
					</div>
				</c:otherwise>
			</c:choose>
			<%
				String pageNumber = request.getParameter("pageNumber");
				String showSuccess = request.getParameter("success"); 
				String showErrorIds = request.getParameter("inputErrors");
				String showError = request.getParameter("error");
				String rejectedPIds = request.getParameter("rejectedPIds");
			%>
			<div id="containerUpdateCertiStausId" class="modal hide fade in" style="display:none;">
                    <input type="hidden" id="hiddenPlanIds" name="hiddenPlanIds">
                    <div class="modal-header">
                        <a id="closeStatusDialog" class="close" data-dismiss="modal" data-original-title="">x</a>
                        <span id="planIdCountCert"></span> 
                    </div>
                    <div class="modal-body">
                        <table>
                                <tr>
                                    <td style="width:200px"><label for="updateStatus" class="control-label"><spring:message code="label.updateCertificationStatus"/></label> </td>
                                    <td>
                                         <select id="certificationStatus" name="certificationStatus">
                                             <option value=""><spring:message code="label.selectOption"/></option>
                                             <c:forEach var="certiStatus" items="${bulkStatusList}">
                                                <option value="${certiStatus.key}">${certiStatus.value}</option>
                                            </c:forEach>
                                         </select>
                                         <div id="status_error" class="updateStausError"></div>
                                    </td>
                                </tr>                                                              
                                <tr>
                                	<td></td>
                                    <td>
                                        <div class="form-actions" style="border:none">
                                            <div class="control-group span5">
                                                <input type="button" name="bulkUpdateCertificationSubmitBtn" id="bulkUpdateCertificationSubmitBtn" value="<spring:message code='label.save'/>" class="btn btn-primary" title="<spring:message code='label.save'/>" />
                                            </div>  
                                        </div> 
                                    </td>
                                </tr>
                        </table>
                        <div id="updateCertificationStatusErrorDivId" class="updateStausError"></div>
                    </div>
                   </div>
                    
                    <div id="containerUpdateEnrollmentAvblId" class="modal hide fade in" style="display:none;">
	                    <input type="hidden" id="hiddenPlanIds" name="hiddenPlanIds">
	                    <div class="modal-header">
	                        <a id="closeEnrollmentDialog" class="close" data-dismiss="modal" data-original-title="">x</a>
	                        <span id="planIdCountEnroll"></span> 
	                    </div>
	                    <div class="modal-body">
	                        <table>
	                                <tr>
	                                    <td><label for="enrollmentavail"><spring:message code="label.updateEnrollmentAvailStatus"/></label> </td>
	                                    <td style="width:260px">
	                                        <select name="enrollmentavail" id="enrollmentavail">
	                                            <option value=""><spring:message code="label.selectOption"/></option>                               
	                                            <c:forEach var="enrollmentAvailList" items="${enrollmentAvailList}">
	                                                <option value="${enrollmentAvailList.key}"> ${enrollmentAvailList.value} </option>                              
	                                            </c:forEach>
	                                        </select>
	                                        <div id="enrollmentavail_error" class="updateStausError"></div>
	                                    </td>                               
	                                </tr>                                
	                                 <tr>
	                                    <td style="width:260px"><label for="effectiveDate" class="control-label"> <spring:message  code="label.updateEnrollmentAvailEffectiveDate"/></label> </td> 
	                                     	<td>
	                                     		<fmt:formatDate pattern="MM/dd/yyyy" value="${sysDate}" var="systemDate" />
												<div class="input-append date date-picker" id="date" data-date="${systemDate}" data-date-format="mm/dd/yyyy">
													<input type="text" id="effectiveDate" name="effectiveDate" class="span10" placeholder="MM/dd/yyyy">
													<span class="add-on"><i class="icon-calendar"></i></span>
												</div>			
												<div id="effectiveDate_error" class="updateStausError"></div>													
		                      			</td>                                                                    
	                                </tr>
	                                <tr>
	                                	<td></td>
	                                    <td>
	                                        <div class="form-actions" style="border:none">
	                                            <div class="control-group span5">
	                                                <input type="button" name="bulkUpdateEnrollmentAvblSubmitBtn" id="bulkUpdateEnrollmentAvblSubmitBtn" value="<spring:message code='label.save'/>" class="btn btn-primary" title="<spring:message code='label.save'/>" />
	                                            </div>  
	                                        </div> 
	                                    </td>
	                                </tr>
	                        </table>
	                        <div id="updateStatusEnrollmentAvblErrorDivId" class="updateStausError"></div>
	                    </div>
                    </div>
                    <div id="updateSuccessDivId" class="modal hide fade in" style="display:none;">
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal" data-original-title="">x</a>
                            <h3 class="span5"> <spring:message code='label.dialogTitle'/></h3>
                        </div>
                        <div class="modal-body">
                            <spring:message code='lablel.updateMsg' />
                        </div>
                        <div class="footer margin20-b txt-center">
  							<a class="btn btn-primary" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                    <div id="showRejectPlanIds" class="modal hide fade in" style="display:none;">
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal" data-original-title="">x</a>
                            <h3 class="span5"> <spring:message code='label.dialogTitle'/></h3>
                        </div>
                        <div class="modal-body" id="displayContent">
                        	 <spring:message code='label.certificationAlert'/>
                        	<%if(StringUtils.isNotBlank(rejectedPIds) && showError.equals("true") && StringUtils.isBlank(pageNumber)){%>
                           		<%= rejectedPIds.substring(1, rejectedPIds.length()-1) %>
							<%}%>
                        </div>
                        <div class="footer margin20-b txt-center">
  							<a class="btn btn-primary" data-dismiss="modal">Close</a>
                        </div>
                    </div>
			</div>
		</div>
	</form>
	<form id="updateStatusForm" name="updateStatusForm" action="<c:url value="/admin/planmgmt/updateQDPstatusinbulk" />"  method="POST">
		<df:csrfToken/>
		<input type="hidden"  name="verified" value="${verified}">
		<input type="hidden"  name="status" value="${status}">
		<input type="hidden"  name="market" value="${market}">
		<input type="hidden"  name="issuername" value="${issuername}">
		<input type="hidden"  name="issuerid" value="${issuerid}">
		<input type="hidden"  name="planNumber" value="${planNumber}">
		<input type="hidden"  name="state" value="${state}">
		<input type="hidden"  name="selectedPlanLevel" value="${selectedPlanLevel}">
		<input type="hidden" id="hdnPlanIds" name="planIds">
		<input type="hidden" id="hdnplanYearSelected" name="planYearSelected">
		<input type="hidden" id="hdnCertificationStatus" name="certificationStatus">
		<input type="hidden" id="hdnEffectiveDate" name="effectiveDate">
		<input type="hidden" id="hdnEnrollmentavail" name="enrollmentavail">
		<input type="hidden"  id="isSelectAllPlans" name="isSelectAllPlans" value="false">
		<input type="hidden"  id="updateType" name="updateType" value="none">                
		<input type="hidden"  id="isSelectAllPlansOnAllPages" name="isSelectAllPlansOnAllPages" value="${isSelectAllPlansOnAllPagesForQDP}">
	</form>
</div>
<!-- assister registration -->

<div id="dialog-modal" style="display: none;">
  <p>
  	<c:if test="${not empty ENROLL_AVAIL}">
  		<spring:message code='err.enrollStatusStart'/> ${ENROLL_AVAIL}  <spring:message code='err.planStatusEnd'/>
	</c:if>
	
	<c:if test="${not empty PENDING_DECERTIFICATION}">
		<spring:message code='err.decertifyStatusStart'/> ${PENDING_DECERTIFICATION}  <spring:message code='err.planStatusEnd'/>
	</c:if>
	</p>
</div>
<%  
if(StringUtils.isNotBlank(showSuccess) && showSuccess.equals("true") && StringUtils.isBlank(pageNumber)){
%>
    <script type="text/javascript">
        $('#updateSuccessDivId').modal("show"); 
    </script>
<%
    } if(StringUtils.isNotBlank(showErrorIds) && showErrorIds.equals("true") && StringUtils.isBlank(pageNumber)){ 
     request.removeAttribute("inputErrors");
%>
    <script type="text/javascript">
        var planIds = $("#hiddenErrorId").val();
        displayErrorIDs(planIds);
    </script>
<% 
    } if(StringUtils.isNotBlank(showError) && showError.equals("true") && StringUtils.isBlank(pageNumber)){ 
        request.removeAttribute("error");
%>
    <script type="text/javascript">
        $("#updateCertificationStatusErrorDivId").html("Error occured while updating plans");
        $("#updateStatusEnrollmentAvblErrorDivId").html("Error occured while updating plans");
    </script>    
<% 
 } if(StringUtils.isNotBlank(rejectedPIds) && showError.equals("true") && StringUtils.isBlank(pageNumber)){ 
%>
    <script type="text/javascript">
	    $("#showRejectPlanIds").modal("show"); 
    </script>
<% 
 } 
%>
<input type="hidden" id="hiddenErrorId" value="<%=showErrorIds %>">


<script type="text/javascript">
	/* $(function() {
		$('.datepick').each(function() {
			$(this).datepicker({
				showOn : "button",
				buttonImage : "../resources/images/calendar.gif",
				buttonImageOnly : true
			});
		});
	}); */
	$(document).ready(
		function() {
			var aNext = $("#nextPage");
			if (null != aNext && undefined !== aNext
					&& typeof (aNext) === "object" && aNext.length > 0) {
				aNext.html("Next");
			}
			$("#planLevel").prepend("<option value=''>Any</option>")
					.val('');
		var selectedPlanLevel = '${selectedPlanLevel}';	

		if(selectedPlanLevel == '')
			$("#planLevel option[value='']").attr('selected', 'selected');
		else
			$("#planLevel option[value='"+ selectedPlanLevel +"']").attr('selected', 'selected');
	});
	
	$(document).ready(function(){
		
		if('${ENROLL_AVAIL}' != null && '${ENROLL_AVAIL}' != '')
		{
			 $( "#dialog-modal" ).dialog({
			     modal: true,
				 title: "<spring:message  code='label.updateStatus'/>",
				 buttons: {
				   Ok: function() {
				    $( this ).dialog( "close" );
				   }
				   }
			    });
		}else if('${PENDING_DECERTIFICATION}' != null && '${PENDING_DECERTIFICATION}' != ''){
			 $( "#dialog-modal" ).dialog({
			  		modal: true,
			    	title: "<spring:message  code='label.updateStatus'/>",
			    	buttons: {
			    	Ok: function() {
			    		$( this ).dialog( "close" );
			    	}
			    	}
			    });
		}
		
		 $("#updateCertificationStatus").click(function(){
		        var selectedPlanIds = [];
		        $(".updatePlanRows").each(function(){
		            if($(this).attr('checked') == 'checked'){
		                if(null != $(this).val()){
		                    var planId = $(this).val().split("_");
		                    selectedPlanIds.push(planId[0]);
		                }
		            }
		        });
		        if(jQuery('#selectAllPlans').attr('checked') == 'checked'){
		        	$("#planIdCountCert").html("<spring:message code='label.updateStatus'/> ("+ "${resultSize}" +" <spring:message code='label.planSelected'/>)");
		        }else{
		        	$("#planIdCountCert").html("<spring:message code='label.updateStatus'/> (" + selectedPlanIds.length +" <spring:message code='label.planSelected'/>)");
		        }
		        $("#hdnPlanIds").val(selectedPlanIds);
		        $('#containerUpdateCertiStausId').modal("show");               
		});
		
	    $("#updateEnrollmentAvblStatus").click(function(){
	    	$('#enrollmentavail').val('');
			//$('#effectiveDate').val('MM/dd/yyyy');
	        var selectedPlanIds = [];
	        $(".updatePlanRows").each(function(){
	            if($(this).attr('checked') == 'checked'){
	                if(null != $(this).val()){
	                    var planId = $(this).val().split("_");
	                    selectedPlanIds.push(planId[0]);
	                }
	            }
	        });
	        
	        if(jQuery('#selectAllPlans').attr('checked') == 'checked'){
	        	$("#planIdCountEnroll").html("<spring:message code='label.updateEnrollmentAvailStatus'/> ("+ "${resultSize}" +" <spring:message code='label.planSelected'/>)");
	        }else{
	        	$("#planIdCountEnroll").html("<spring:message code='label.updateEnrollmentAvailStatus'/> (" + selectedPlanIds.length +" <spring:message code='label.planSelected'/>)");
	        }
	        $("#hdnPlanIds").val(selectedPlanIds);	        
	        $('#containerUpdateEnrollmentAvblId').modal("show");     
	    });
	    
	    $("#certificationStatus").change(function(){
	        var cStatus = $("#certificationStatus option:selected").val();
	        if(cStatus == ""){
	            $("#status_error").html("<spring:message code='error.selectCertificationStatus'/>");
	            return false;
	        }else{
	            $("#status_error").html("");
	        }
	    });
	    $("#effectiveDate").click(function(){
	        var eDate = $("#effectiveDate").val();
	        if(eDate == ""){
	            $("#effectiveDate_error").html("<spring:message code='error.selectEffectiveDate'/>");
	            return false;
	        }else{
	            $("#effectiveDate_error").html("");
	        }
	        var startdatevalue = '${currDate}';
			if(Date.parse(startdatevalue) > Date.parse(eDate)){
				 $("#effectiveDate_error").html("<spring:message  code='label.effDateErr'/>");
		            return false;
			}else{
	            $("#effectiveDate_error").html("");
	        }
	    });
	    
	    $("#enrollmentavail").change(function(){
	        var cStatus = $("#enrollmentavail option:selected").val();
	        if(cStatus == ""){
	            $("#enrollmentavail_error").html("<spring:message code='error.selectEnrollmentStatus'/>");
	            return false;
	        }else{
	            $("#enrollmentavail_error").html("");
	        }
	    });
	    
	    $("#bulkUpdateCertificationSubmitBtn").click(function(){
	        var cStatus = null;
	        var eDate = null;
	        var eAvail = null;
	        var planyear = $("#planYear").val();
	        
	        cStatus = $("#certificationStatus option:selected").val();
	        if(cStatus == ""){
	            $("#status_error").html("<spring:message code='error.selectCertificationStatus'/>");
	            return false;
	        }
	        
	        $("#hdnplanYearSelected").val(planyear);
	        $("#hdnCertificationStatus").val(cStatus);
	        $("#hdnEffectiveDate").val(eDate); 
	        $("#hdnEnrollmentavail").val(eAvail);
	        $("#updateType").val("certificationStatusUpdate");        
	        $("#updateStatusForm").submit();
	       	$('#containerUpdateCertiStausId').modal("hide");
	    });
	    
	    $("#bulkUpdateEnrollmentAvblSubmitBtn").click(function(){
	        var cStatus = null;
	        var eDate = null;
	        var eAvail = null;
	        var planyear = $("#planYear").val();
	        
	         eDate = $("#effectiveDate").val();
	        if(eDate == ""){
	            $("#effectiveDate_error").html("<spring:message code='error.selectEffectiveDate'/>");
	            return false;
	        }
	        
	        var startdatevalue = '${currDate}';
	        
			if(Date.parse(startdatevalue) > Date.parse(eDate)){
				 $("#effectiveDate_error").html("<spring:message  code='label.effDateErr'/>");
		            return false;
			}else{
	            $("#effectiveDate_error").html("");
	        }
			
	        eAvail = $("#enrollmentavail option:selected").val();
	        if(eAvail == ""){
	            $("#enrollmentavail_error").html("<spring:message code='error.selectEnrollmentStatus'/>");
	            return false;
	        }
	        
	        $("#hdnplanYearSelected").val(planyear);
	        $("#hdnCertificationStatus").val(cStatus);
	        $("#hdnEffectiveDate").val(eDate); 
	        $("#hdnEnrollmentavail").val(eAvail);
	        $("#updateType").val("enrollmentAvblUpdate");
	        $("#updateStatusForm").submit();
	       	$('#containerUpdateEnrollmentAvblId').modal("hide");
	    });
	    
	    function displayErrorIDs(resultData){
	        resultData = resultData.replace("[", "");
	        resultData = resultData.replace("]", "");
	        var items = resultData.split(",");
	        var t = "";
	        for(var i = 0; i < items.length; i++) {
	            t = t + items[i] + "<br/>";
	        }
	        $("#updateStausErrorDivId").html(t);
	    }
	    
	    function clearText(){
	        $('#status_error').html("");
	        $('#effectiveDate_error').html("");
	        $('#enrollmentavail_error').html("");
	        
	        $('#certificationStatus').html("");
	        $('#effectiveDate').html("");
	        $('#enrollmentavail').html("");
	    }


	    var aNext = $("#nextPage");
	    if(null != aNext && undefined !== aNext && typeof(aNext) === "object" && aNext.length > 0) {
	      aNext.html("Next");
	    }
	$(document).ready(function(){
		$('.complete').each(function(){
			var completeStep = $(this).html();
			var replaceExpr = /html"\>/gi;
			$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
		});
		
		$('.date-picker').datepicker({
			startDate: '+'+ '${systemDate}' + 'd',
			autoclose: true,
		    format: 'mm/dd/yyyy',
		    forceParse: false
		});
	});


	jQuery('.updatePlanHeader').on('click', function(){
	    if(jQuery('.updatePlanHeader').attr('checked') == 'checked'){
	        var r=confirm("Do you want to select all plans");
	        if (r==true)
	          {
	        
	        jQuery(".updatePlanRows").each(function(){
	            if(jQuery(this).attr('disabled') == 'true' || jQuery(this).attr('disabled') == 'disabled')
	                {
	                jQuery(this).removeAttr('checked');
	                }else{
	                    jQuery(this).attr('checked', 'checked');
	                    $( "#modifyPlansBulk" ).removeClass("disabled");
	                }
	            });
	          }else{
	              jQuery(this).removeAttr('checked');
	          }
	        
	    }else{
	        var r=confirm("Do you want to de-select all plans");
	        if (r==true)
	          {
	        
	    jQuery('.updatePlanRows').removeAttr('checked');
	    jQuery(this).removeAttr('checked');
	    $( "#modifyPlansBulk" ).addClass("disabled");
	    }else{
	        jQuery(this).attr('checked', 'checked');
	    }
	    }
	  });
	  
	  
	jQuery('.updatePlanRows').on('click', function(){
	    var activate = 0;
	    jQuery(".updatePlanRows").each(function(){
	    if(jQuery(this).attr('checked') == 'checked'){
	        $( "#modifyPlansBulk" ).removeClass("disabled");
	        activate++;
	        }else{
	             $( "#modifyPlansBulk" ).addClass("disabled");
	        }
	    if(activate !=0){
	        $( "#modifyPlansBulk" ).removeClass("disabled");
	    }
	    });
	});
	});
	
	$("#go").click(function(){
		  var value = $("#planYear").val();
		  setRequireData(null, null, null, null, value);
		  $("#selectedPlanLevel").val($("#planLevel").val());
	});

	$("#planYear").change(function(){
		  var value = $("#planYear option:selected").val();
		  setRequireData(null, null, null, null, value);
		  $("#frmfindplan").submit();
	});
	
	 /* $('.datepick').each(function() {
			var ctx = "${pageContext.request.contextPath}";
			var imgpath = ctx+'/resources/images/calendar.gif';
			$(this).datepicker({
				showOn : "button",
				buttonImage : imgpath,
				buttonImageOnly : true,
				minDate: 0
			});
	}); */
	
	if($( "#isSelectAllPlansOnAllPages" ).val() == 'true'){
	    jQuery('#selectAllPlans').attr('checked', 'checked');
	    jQuery(".updatePlanHeader").attr('disabled', 'disabled');
     	jQuery(".updatePlanRows").each(function(){
         	if(jQuery(this).attr('disabled') == 'true' || jQuery(this).attr('disabled') == 'disabled') {
             	jQuery(this).removeAttr('checked');
             }else{
                 jQuery(this).attr('checked', 'checked');
                 jQuery(this).attr('disabled', 'disabled');
                 $( "#modifyPlansBulk" ).removeClass("disabled");
                 $(".updatePlanHeader").attr('checked', 'checked');
             }
         });
     	jQuery("#isSelectAllPlans").val("true");	    
	 }else{
   		jQuery('#selectAllPlans').removeAttr('checked');
   		jQuery(".updatePlanHeader").removeAttr('disabled');
   		jQuery('.updatePlanRows').removeAttr('checked');
   		jQuery('.updatePlanRows').removeAttr('disabled');	    		
   		jQuery(this).removeAttr('checked');
   		$( "#modifyPlansBulk" ).addClass("disabled");
   		$(".updatePlanHeader").removeAttr('checked');
   		jQuery("#isSelectAllPlans").val("false");	    	  
	 }
	 
	jQuery('#selectAllPlans').on('click', function(){
	    if(jQuery('#selectAllPlans').attr('checked') == 'checked'){
	        var r=confirm("Do you want to select all plans");
	        if (r==true)
	          {
	        	jQuery(".updatePlanHeader").attr('disabled', 'disabled');
	        	jQuery(".updatePlanRows").each(function(){
	            	if(jQuery(this).attr('disabled') == 'true' || jQuery(this).attr('disabled') == 'disabled')
	                {
	                	jQuery(this).removeAttr('checked');
	                }else{
	                    jQuery(this).attr('checked', 'checked');	                    
	                    jQuery(this).attr('disabled', 'disabled');
	                    $( "#modifyPlansBulk" ).removeClass("disabled");
	                    $(".updatePlanHeader").attr('checked', 'checked');
	                }
	            });
	        	jQuery("#isSelectAllPlans").val("true");
	        	$.ajax({
	                type: "POST",
	                url: "<c:url value='/admin/planmgmt/qdp/updateSelectAllFlag'/>",
	                data: {"isSelectAllPlansOnAllPagesForQDP": "true", "csrftoken": $("#csrftoken").val()},
	                success: function(data) {
	                    //alert(data);
	                },
	                 error: function() {
	                    //alert('it broke');
	                },
	                complete: function() {
	                    //alert('it completed');
	                }
	            });
	          }else{
					jQuery(this).removeAttr('checked');
	          }	        
	    }else{
	        var r=confirm("Do you want to de-select all plans");
	        if (r==true)
			{
	        	jQuery(".updatePlanHeader").removeAttr('disabled');
	    		jQuery('.updatePlanRows').removeAttr('checked');
	    		jQuery('.updatePlanRows').removeAttr('disabled');	    		
	    		jQuery(this).removeAttr('checked');
	    		$( "#modifyPlansBulk" ).addClass("disabled");
	    		$(".updatePlanHeader").removeAttr('checked');
	    		jQuery("#isSelectAllPlans").val("false");
	    		$.ajax({
	                type: "POST",
	                url: "<c:url value='/admin/planmgmt/qdp/updateSelectAllFlag'/>",
	                data: {"isSelectAllPlansOnAllPagesForQDP": "false", "csrftoken": $("#csrftoken").val()},
	                success: function(data) {
	                    //alert(data);
	                },
	                 error: function() {
	                    //alert('it broke');
	                },
	                complete: function() {
	                    //alert('it completed');
	                }
	            });
	    	}else{
	        	jQuery(this).attr('checked', 'checked');
	    	}
	    }
	});
	
	function  formSubmit(){	
		$("#updateStatusForm").submit();
	}
	
	$("#closeEnrollmentDialog").click(function(){
		$("#effectiveDate").val("");
		$("#enrollmentavail").val("")
		$("#effectiveDate_error").html("");
	    $("#enrollmentavail_error").html("");
	});
	
	$("#closeStatusDialog").click(function(){
		$("#updateCertificationStatusErrorDivId").html("");
	    $("#certificationStatus").val("");
	});

	function traverse(url){
		var queryString = {};
		url.replace(
			    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
			    function($0, $1, $2, $3) { queryString[$1] = $3; }
			);
		setRequireData(queryString["sortBy"], queryString["sortOrder"], queryString["pageNumber"], queryString["changeOrder"], queryString["planYearSelected"]);
		$("#frmfindplan").submit();
	}

	function setRequireData(sortBy, sortOrder, pageNumber, changeOrder, planYearSelected) {

		if (!sortBy) {
			sortBy = "lastUpdateTimestamp";
		}
		document.forms["frmfindplan"].elements["sortBy"].value = sortBy;

		if (!sortOrder) {
			sortOrder = "DESC";
		}
		document.forms["frmfindplan"].elements["sortOrder"].value = sortOrder;

		if (!pageNumber) {
			pageNumber = "1";
		}
		document.forms["frmfindplan"].elements["pageNumber"].value = pageNumber;

		if (changeOrder) {
			document.forms["frmfindplan"].elements["changeOrder"].value = changeOrder;
		}

		if (!planYearSelected) {
			planYearSelected = $("#planYear").val();
		}
		document.forms["frmfindplan"].elements["planYearSelected"].value = planYearSelected;
	}
</script>
<!--  end of row-fluid -->
