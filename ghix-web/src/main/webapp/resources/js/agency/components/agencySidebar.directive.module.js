(function() {
    'use strict';

    angular.module('agencySidebarDirectiveModule', [])
    .directive('agencySidebar', agencySidebar);
    	
	function agencySidebar(){
		return {
			restrict: 'EA', 
			templateUrl: '/hix/resources/html/agency/templates/agencySidebar.html',
			
		};
	}



})();