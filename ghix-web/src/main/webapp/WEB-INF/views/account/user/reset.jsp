<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
	
<div class="gutter10">
	<h1>Reset your password</h1>
	<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
				<form class="form-horizontal" id="frminvalidpasswordtoken" name="frminvalidpasswordtoken" action="<c:url value="/account/user/submitReset" />" method="POST">
                    <df:csrfToken/>
					<div class="invalidpasswordtoken">	
                        <div class="form-actions">
                            <input type="submit" name="redirecttoforgotpassword" id="redirecttoforgotpassword" value="<spring:message  code='label.resetPasword'/>" class="btn btn-primary" title="<spring:message  code='label.resetPasword'/>"/> 
                        </div>
                    </div><!--  resetpassword -->
                </form>
			</c:if>
			<br>
		</div>
	</div>
	<c:if test="${errorMsg == ''}">
		<div class="row-fluid">		
      <div class="span3" id="sidebar"> 
         <div class="alert alert-danger"> 
           Please select a new password. 
         </div> 
      </div>
	
			<div class="span9" id="rightpanel">
				<div class="graydrkaction">
					<h4>Set password</h4>
				</div>
					<form class="form-horizontal gutter10" id="frmresetpassword" name="frmresetpassword" action="<c:url value="/broker/passwordsetup" />" method="POST" autocomplete="off">
					<df:csrfToken/>
	                    <div class="resetpassword">
	                        <div class="control-group">                                 
	                            <label for="password" class="required control-label"><spring:message  code="label.brkPassword"/>: <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                            <div class="controls">
	                                <input type="password" name="password" id="password" value="" class="input-xlarge" size="30" autocomplete="off" onkeypress="resetErrorMsg(this)"/>
	                                <div id="password_error" class=""></div>
	                            </div>    <!-- end of controls-->
	                        </div>    <!-- end of control-group -->
	
	                        <div class="control-group">
	                            <label for="confirmPassword" class="required control-label"><spring:message code="label.brkConfirmPassword"/>: <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                            <div class="controls">
	                                <input type="password" name="confirmPassword" id="confirmPassword" value="" class="input-xlarge" size="30" autocomplete="off" onkeypress="resetErrorMsg(this)"/>
	                                <div id="confirmPassword_error" class=""></div>
	                                <input type="hidden" name="id" id="id"  value="<encryptor:enc value="${userId}"/>" />
	                            </div>    <!-- end of controls-->
	                        </div>    <!-- end of control-group -->

	                        <div class="form-actions">
	                            <input type="button" name="submitbtn" id="submitbtn" onClick="javascript:checkUserPassword('Submit');"  value="<spring:message  code='label.brkContinue'/>" class="btn btn-primary" title="<spring:message  code='label.brkContinue'/>" /> 
	                        </div>
	                    </div><!--  resetpassword -->
	                </form>				
				</div>
		</div>
	</c:if>
</div>	

<script type="text/javascript">

jQuery.validator.addMethod("passwordcheck", function(value, element, param) {
     password   = $("#password").val(); 
     confirmpwd = $("#confirmPassword").val(); 
     
    if( password != confirmpwd ){ return false; }
    return true;
});


jQuery.validator.addMethod("validatepassword", function(value, element, param) {
    password   = $("#password").val(); 
  
    //var re =  /^[A-Za-z0-9!@#$%^&*()_]{8,20}$/;
    var re = /(?=^.{8,20}$)(?=.*[a-zA-Z])(?=.*[0-9])(?!.*\s).*$/;  // For HIX-4131
           
   // if(!re.test(password)){ return false; }
    return true;
    
});


var validator = $("#frmresetpassword").validate({
    rules : {password : { required : true,validatepassword:true },
        confirmPassword : { required : true, passwordcheck : true}},
        messages : {
            password: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePassword' javaScriptEscape='true'/></span>",
            	validatepassword : "<span> <em class='excl'>!</em><spring:message code='label.validatePasswordLength' javaScriptEscape='true'/></span>"},
            confirmPassword: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePasswordConfirm' javaScriptEscape='true'/></span>",
                passwordcheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePasswordMatch' javaScriptEscape='true'/></span>"
        }
    },
    errorClass: "error",
    errorPlacement: function(error, element) {
        var elementId = element.attr('id');
        error.appendTo( $("#" + elementId + "_error"));
        $("#" + elementId + "_error").attr('class','error help-inline');
    } 
});

function resetErrorMsg(element) {
	var elementId = element.id;

	$("#" + elementId + "_error").html('');
}

function checkUserPassword(action)
{
	if($("#frmresetpassword").valid()){
		var validateUrl = "<c:url value='/account/signup/checkResetUserPassword'/>";
		var csrftoken;
		csrftoken= '${sessionScope.csrftoken}';
		$.ajax({
			url : validateUrl,
			type : "POST",
			data : {
				password : $("#password").val(), token : window.location.href, csrftoken :csrftoken
			},
			success: function(response)
			{
				if (response!="")
				{
					$("#password_error").html("<label class='error'><span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>"+response+"</span></label>");
					$("#password_error").attr('class','error help-inline');
					return false;
				}
				else{
					if(action=='Submit'){
						 $("#frmresetpassword").submit();  
						
						}else{
	
							$("#password_error").html("");
							$("#password_error").removeAttr("class");
							
						}
					
				}
			},
			
		});
	}
}


</script>