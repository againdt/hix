<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<style>
	input.datepicker {
		width: 425px;
	}
</style>

<script type="text/ng-template" id="maritalstatus-citizenship.jsp">

	<div class="gutter10" ng-controller="maritalStatusCitizenship" ng-init="householdMember = jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember[jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1]">
    <h3>
        <spring:message code="label.lce.citizenship"/>
    </h3>
    <form class="form-horizontal" name="formCitizenship" id="formCitizenship" novalidate>
        <div id="citizenship">
            <label for="hidFMLNameLabel1">
                <spring:message code="label.lce.is"/>
                <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
                <spring:message code="label.lce.isacitizen"/>
                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
				<input type="hidden" id="hidFMLNameLabel1" />
            </label>
            <div class="margin10-l">
                <label class="radio-inline" for="citizenshipYes">
                    <input type="radio" id="citizenshipYes" required name="iscitizen" value="Yes" ng-value="true" ng-click="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected='';householdMember.citizenshipImmigrationStatus.citizenshipAsAttestedIndicator=true;householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator=null;householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator=null;" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator">
                    <spring:message code="label.lce.yes"/>
                </label>
                <label class="radio-inline" for="citizenshipNo">
                    <input type="radio" id="citizenshipNo" required name="iscitizen" value="No" ng-value="false" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator"   ng-click="householdMember.citizenshipImmigrationStatus.citizenshipAsAttestedIndicator=false;">
                    <spring:message code="label.lce.no"/>
                </label>
            </div>
            <!-- controls -->
            <!-- Error -->
            <div class="margin30-l margin10-t">
                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.iscitizen.$error.required">
                    <spring:message code="label.lce.thisvalueisrequired"/>
                </span>
            </div>
            <!-- If Spouse if NOT A CITIZEN -->
            <div class="notCitizen" ng-if='householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false' >
                <div id="notacitizen" class="margin30-t">
                    <label for="hidFMLNameLabel2">
                        <spring:message code="label.lce.is"/>
                        <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
                        <spring:message code="label.lce.anaturalizedcitizen"/>
                        <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
						<input type="hidden" id="hidFMLNameLabel2" />
                    </label>
                    <div class="margin10-l">
                        <label class="radio-inline" for="naturalizedyes">
                            <input type="radio" id="naturalizedyes" name="isNaturalizeCitizen" value="Yes" ng-value="true" ng-required="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false" ng-click="householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator=null" ng-model="householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator">
                            <spring:message code="label.lce.yes"/>
                        </label>
                        <label class="radio-inline" for="naturalizedno">
                            <input type="radio" id="naturalizedno" name="isNaturalizeCitizen" value="No" ng-value="false" ng-required="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false" ng-model="householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator" ng-click="tempNaturalizedOrCOC=undefined;householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected='';householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator=false;">
                            <spring:message code="label.lce.no"/>
                        </label>
                    </div>
                    <!-- controls -->
                    <!-- Error -->
                    <div class="margin30-l margin10-t">
                        <span class="validation-error-text-container" ng-if="submitted && formCitizenship.isNaturalizeCitizen.$error.required">
                            <spring:message code="label.lce.thisvalueisrequired"/>
                        </span>
                    </div>
                </div>
                <div ng-if="householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator==true" class="margin30-t">
                    <h4>
                        <spring:message code="label.lce.documenttype"/>
                        <small><spring:message code="label.lce.selectone"/></small>
                        <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"><a><span aria-label="Required!"></span></a>
                    </h4>
                </div>
                <div ng-if="householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator==true" class="margin30-t">
                    <div class="control-group">
                        <div class="margin10-l">
                            <label class="radio-inline" for="naturalizedcertificate">
                            <input type="radio" id="naturalizedcertificate" name="naturalizedOrCOC" ng-required="householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator==true && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator==false"  value="true" ng-value="true" ng-click="householdMember.citizenshipImmigrationStatus.naturalizationCertificateIndicator=true;householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected='NaturalizationCertificate'" ng-model="tempNaturalizedOrCOC">
                            <spring:message code="label.lce.naturalization"/>
                            </label>
                        </div>
                        <!-- NATURALIZATION CERFITIFICATE -->
                        <div class="control-group reentry margin10-t" ng-if="tempNaturalizedOrCOC && (householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='NaturalizationCertificate')">
                            <div class="control-group">
                                <label class="control-label" for="naturCertAlien">
                                    <spring:message code="label.lce.alienregistrationno"/>
                                    <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                                </label>
                                <div class="controls">
                                    <input type="text" name="naturCertAlien" class="span7" placeholder="<spring:message code="label.lce.alienregistrationno"/>" ng-pattern="/^[0-9]{7,9}$/"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.alienNumber" ng-required="householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator==true && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator==false && householdMember.citizenshipImmigrationStatus.naturalizationCertificateIndicator==true">
                                </div>
                                <!-- Error -->
                                <div class="controls">
                                    <span class="validation-error-text-container" ng-if="submitted && formCitizenship.naturCertAlien.$error.required">
                                        <spring:message code="label.lce.pleaseenteralien"/>
                                    </span>
                                    <span class="validation-error-text-container" ng-if="submitted && formCitizenship.naturCertAlien.$error.pattern">
                                        <spring:message code="label.lce.validalien"/>
                                    </span>
                                </div>
                            </div>
                            <!-- control-groups -->
                            <div class="control-group">
                                <label class="control-label" for="natureNumber"><spring:message code="label.lce.anaturalizationnumber"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!"></label>
                                <div class="controls">
                                    <input id="natureNumber" name="natureCertNatureNo" placeholder="Naturalization Number" ng-pattern="/^[0-9a-zA-Z]{6,12}$/" type="text" class="span7"  ng-model="$parent.$parent.$parent.dummySevisIdA" ng-required="householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator==true && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator==false && householdMember.citizenshipImmigrationStatus.naturalizationCertificateIndicator==true">
                                </div>
                                <!-- controls -->
                                <!-- Error -->
                                <div class="controls">
                                    <div class="validation-error-text-container" ng-if="submitted && formCitizenship.natureCertNatureNo.$error.required">
                                        <spring:message code="label.lce.pleaseenternatureno"/>
                                    </div>
                                    <div class="validation-error-text-container" ng-if="submitted && formCitizenship.natureCertNatureNo.$error.pattern">
                                        <spring:message code="label.lce.validnaturcertno"/>
                                    </div>
                                </div>
                            </div>
                            <!-- control-groups -->
                        </div>
                        <!--  PERMIT -->
                        <div class="margin10-l">
                            <label class="radio-inline" for="citizenshipcertificate">
                            <input type="radio" id="citizenshipcertificate" name="naturalizedOrCOC" ng-required="householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator==true && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator==false" value="false" ng-value="false"  ng-click="householdMember.citizenshipImmigrationStatus.naturalizationCertificateIndicator=false;householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected='CitizenshipCertificate'"  ng-model="tempNaturalizedOrCOC"> <spring:message code="label.lce.certificateofcitizenship"/>
                            </label>
                        </div>
                        <!-- controls -->
                        <!-- Error -->
                        <div class="margin30-l margin10-t">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.naturalizedOrCOC.$error.required">
                                <spring:message code="label.lce.thisvalueisrequired"/>
                            </span>
                        </div>
                        <!-- CITIZENSHIP CERTIFICATE -->
                        <div class="control-group reentry margin10-t" ng-if="(!tempNaturalizedOrCOC) && householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='CitizenshipCertificate'">
                            <div class="control-group">
                                <label class="control-label" for="cocAliennumber">
                                    <spring:message code="label.lce.alienregistrationno"/>
                                    <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                                </label>
                                <div class="controls margin10-t">
                                    <input type="text" id="cocAliennumber" name="cocAliennumber"  class="span7" placeholder="<spring:message code="label.lce.alienregistrationno"/>"  ng-pattern="/^[0-9]{7,9}$/" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.alienNumber" ng-required="householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator==true && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator==false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='CitizenshipCertificate'">
                                </div>
                                <!-- controls -->
                                <!-- Error -->
                                <div class="controls">
                                    <span class="validation-error-text-container" ng-if="submitted && formCitizenship.cocAliennumber.$error.required">
                                        <spring:message code="label.lce.pleaseenteralien"/>
                                    </span>
                                    <span class="validation-error-text-container" ng-if="submitted && formCitizenship.cocAliennumber.$error.pattern">
                                        <spring:message code="label.lce.validalien"/>
                                    </span>
                                </div>
                            </div>
                            <!-- control-groups -->
                            <div class="control-group">
                                <label class="control-label" for="cocCertNumber"><spring:message code="label.lce.citizenshipcertificatenumber"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!"></label>
                                <div class="controls margin10-t">
                                    <input type="text" id="cocCertNumber" name="cocCertNumber" ng-pattern="/^[0-9a-zA-Z]{6,12}$/" class="span7" placeholder="Citizenship Certificate Number" ng-model="$parent.$parent.$parent.dummySevisIdB" ng-required="householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator==true && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator==false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='CitizenshipCertificate'">
                                </div>
                                <!-- controls -->
                                <!-- Error -->
                                <div class="controls">
                                    <span class="validation-error-text-container" ng-if="submitted && formCitizenship.cocCertNumber.$error.required">
                                        <spring:message code="label.lce.pleaseentercertno"/>
                                    </span>
                                    <span class="validation-error-text-container" ng-if="submitted && formCitizenship.cocCertNumber.$error.pattern">
                                        <spring:message code="label.lce.validcertno"/>
                                    </span>
                                </div>
                            </div>
                            <!-- control-groups -->
                        </div>
                    </div>
                </div>
                <div ng-if="householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator==false">
                    <div class="control-group">
                        <label class="checkbox inline eligibleimmigration" for="eligibleimmigration">
                            <input type="checkbox" id="eligibleimmigration" ng-model="householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator" ng-click="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=''">
                            <spring:message code="label.lce.checkhereif"/>
                            <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
                            <spring:message code="label.lce.haseligibleimmigrationstatus"/>
                        </label>
                    </div>
                    <!-- controls -->
                    <div ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator">
                        <h4>
                            <spring:message code="label.lce.documenttype"/>
                            <small><spring:message code="label.lce.selectone"/></small>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"><a><span aria-label="Required!"></span></a>
                        </h4>
                    </div>
                    <!-- Drop down -->
                    <div class="control-group reentry margin10-t" ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator">
                        <label class="control-label margin10-t" for="citizenDocType">
                            <spring:message code="label.lce.documenttype"/>
                        </label>
                        <div class="controls">
                            <select id="citizenDocType" name="citizenDocType" class="documentType span12 margin10-t" ng-change="resetDocumentTypes(householdMember)" ng-required="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-model="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected" placeholder="Select Document Type">
                                <option value="">
                                    <spring:message code="label.lce.documenttype"/>
                                </option>
                                <option value="I551">
                                    <spring:message code="label.lce.i551"/>
                                </option>
                                <option value="TempI551">
                                    <spring:message code="label.lce.temp"/>
                                </option>
                                <option value="MacReadI551">
                                    <spring:message code="label.lce.machine"/>
                                </option>
                                <option value="I766">
                                    <spring:message code="label.lce.i766"/>
                                </option>
                                <option value="I94">
                                    <spring:message code="label.lce.i94"/>
                                </option>
                                <option value="I94UnexpForeignPassport">
                                    <spring:message code="label.lce.i94unexpired"/>
                                </option>
                                <!-- *NEW FIELD -->
                               <!--  <option value="I797">
                                    <spring:message code="label.lce.noticeofaction"/>
                                </option>-->
                                <option value="UnexpForeignPassport">
                                    <spring:message code="label.lce.unexpiredforeign"/>
                                </option>
                                <option value="I327">
                                    <spring:message code="label.lce.i327"/>
                                </option>
                                <option value="I571">
                                    <spring:message code="label.lce.i571"/>
                                </option>
                                <option value="I20">
                                    <spring:message code="label.lce.i20"/>
                                </option>
                                <option value="DS2019">
                                    <spring:message code="label.lce.ds2019"/>
                                </option>
                            </select>
                        </div>
                        <!-- controls -->
                        <!-- Error -->
                        <div class="controls">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.citizenDocType.$error.required">
                                <spring:message code="label.lce.pleaseselectadoctype"/>
                            </span>
                        </div>
                    </div>
                    <!-- Drop down -->
                    <!-- ****RE-ENTRY PERMIT -->
                    <div class="control-group reentry margin10-t" ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I327'">
                        <!--<div class="control-group reentry margin10-t" ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I327' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true && householdMember.citizenshipImmigrationStatus.citizenshipDocument.nameSameOnDocumentIndicator==false">-->
                        <div class="control-group">
                            <label class="control-label" for="reentryAlien">
                                <spring:message code="label.lce.alienregistrationno"/>
                                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                            </label>
                            <div class="controls">
                                <input type="text" id="reentryAlien" placeholder="<spring:message code="label.lce.alienregistrationno"/>" name="reentryAlien" class="span12"  ng-pattern="/^[0-9]{7,9}$/" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I327' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.alienNumber">
                            </div>
                            <!-- controls -->
                            <!-- Error -->
                            <div class="controls ">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.reentryAlien.$error.required">
                                    <spring:message code="label.lce.pleaseenteralien"/>
                                </span>
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.reentryAlien.$error.pattern">
                                    <spring:message code="label.lce.validalien"/>
                                </span>
                            </div>
                        </div>
                        <!-- control-groups -->
                        <div class="control-group">
                            <label class="control-label" for="docExpiryDate">
                                <spring:message code="label.lce.expdate"/>
                            </label>
                            <div class="controls">
                                <div id="date" class="input-append datepicker" datetimez ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" >
                                    <input id="docExpiryDate" type="text" date-formatter class="datepicker" date="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate"  placeholder="MM/DD/YYYY">
                                    <span class="add-on">
                                    <i class="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                            <!-- controls -->
                        </div>
                        <!-- control-groups -->
                    </div>
                    <!-- RE-ENTRY PERMIT -->
                    <!-- Permanent Resident Card ( "Green Card", I-551) -->
                    <div class="control-group permRes margin10-t" ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I551' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true ">
                        <div class="control-group">
                            <label class="control-label" for="greenCardAlien">
                                <spring:message code="label.lce.alienregistrationno"/>
                                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                            </label>
                            <div class="controls ">
                                <input id="greenCardAlien" placeholder="<spring:message code="label.lce.alienregistrationno"/>" name="greenCardAlien" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I551' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-pattern="/^[0-9]{7,9}$/" type="text" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.alienNumber">
                            </div>
                            <!-- Error -->
                            <div class="controls">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.greenCardAlien.$error.required">
                                    <spring:message code="label.lce.pleaseenteralien"/>
                                </span>
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.greenCardAlien.$error.pattern">
                                    <spring:message code="label.lce.validalien"/>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="greenCardNumber">
                                <spring:message code="label.lce.cardnumber"/>
                                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                            </label>
                            <div class="controls">
                                <input id="greenCardNumber" placeholder="<spring:message code="label.lce.cardnumber"/>" name="greenCardNumber" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I551' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-pattern="/^[a-zA-Z]{3}[0-9]{10}$/" type="text" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber">
                            </div>
                            <!-- Error -->
                            <div class="controls">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.greenCardNumber.$error.required">
                                    <spring:message code="label.lce.pleaseentercard"/>
                                </span>
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.greenCardNumber.$error.pattern">
                                    <spring:message code="label.lce.validcard"/>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="docExpiryDate2">
                                <spring:message code="label.lce.expdate"/>
                            </label>
                            <div class="controls">
                                <div class="input-append date"  datetimez ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate">
                                    <input id="docExpiryDate2" type="text" class="datepicker" date-formatter date="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" placeholder="MM/DD/YYYY">
                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- PERMENANT RESIDENT -->
                    <!-- REFUGEE TRAVEL -->
                    <div class="control-group refugeeTravel margin10-t" ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I571' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true ">
                        <div class="control-group">
                            <label class="control-label" for="refugeeAlien">
                                <spring:message code="label.lce.alienregistrationno"/>
                                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                            </label>
                            <div class="controls">
                                <input id="refugeeAlien" placeholder="<spring:message code="label.lce.alienregistrationno"/>" name="refugeeAlien" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I571' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-pattern="/^[0-9]{7,9}$/" type="text" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.alienNumber">
                            </div>
                            <!-- Error -->
                            <div class="controls">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.refugeeAlien.$error.required">
                                    <spring:message code="label.lce.pleaseenteralien"/>
                                </span>
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.refugeeAlien.$error.pattern">
                                    <spring:message code="label.lce.validalien"/>
                                </span>
                            </div>
                        </div>
                        <!-- control-group -->
                        <div class="control-group">
                            <label class="control-label" for="docExpiryDate3">
                                <spring:message code="label.lce.expdate"/>
                            </label>
                            <div class="controls">
                                <div class="input-append date" datetimez ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate">
                                    <input id="docExpiryDate3" type="text" class="datepicker" date-formatter date="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" placeholder="MM/DD/YYYY">
                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>
                            <!-- controls -->
                        </div>
                        <!-- control-group -->
                    </div>
                    <!-- REFUGEE TRAVEL -->
                    <!-- EMPLOYEE AUTHORIZATION -->
                    <div class="control-group employAuth margin10-t" ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I766' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true ">
                        <div class="control-group">
                            <label class="control-label" for="empAuthAlien">
                                <spring:message code="label.lce.alienregistrationno"/>
                                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                            </label>
                            <div class="controls">
                                <input id="empAuthAlien" placeholder="<spring:message code="label.lce.alienregistrationno"/>" name="empAuthAlien" ng-pattern="/^[0-9]{7,9}$/" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I766' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" type="text" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.alienNumber">
                            </div>
                            <!-- Error -->
                            <div class="controls">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.empAuthAlien.$error.required">
                                    <spring:message code="label.lce.pleaseenteralien"/>
                                </span>
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.empAuthAlien.$error.pattern">
                                    <spring:message code="label.lce.validalien"/>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="empAuthCardNo">
                                <spring:message code="label.lce.cardnumber"/>
                                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                            </label>
                            <div class="controls">
                                <input id="empAuthCardNo" placeholder="<spring:message code="label.lce.cardnumber"/>" name="empAuthCardNo" ng-pattern="/^[a-zA-Z]{3}[0-9]{10}$/" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I766' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" type="text" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber">
                            </div>
                            <!-- Error -->
                            <div class="controls">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.empAuthCardNo.$error.required">
                                    <spring:message code="label.lce.pleaseentercard"/>
                                </span>
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.empAuthCardNo.$error.pattern">
                                    <spring:message code="label.lce.validcard"/>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="empAuth_DocumentExpiration">
                                <spring:message code="label.lce.expdate"/>
                                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                            </label>
                            <div class="controls">
                                <div class="input-append date" name="empAuth_DocumentExpirationDiv"  datetimez ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I766' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true">
                                    <input id="empAuth_DocumentExpiration" type="text" class="datepicker" name="empAuth_DocumentExpiration" date-formatter date="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate"  placeholder="MM/DD/YYYY">
                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>
                            <div class="controls">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.empAuth_DocumentExpirationDiv.$error.required">
                                    <spring:message code="label.lce.pleaseenterdate"/>
                                </span>
                            </div>
                            <!-- controls -->
                        </div>
                        <!-- control-group -->
                    </div>
                    <!-- EMPLOYEE AUTHORIZATION -->
                    <!-- MACHINE -->
                    <div class="control-group machine margin10-t" ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='MacReadI551' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true ">
                        <div class="control-group">
                            <label class="control-label" for="machineAlien">
                                <spring:message code="label.lce.alienregistrationno"/>
                                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                            </label>
                            <div class="controls">
                                <input id="machineAlien" placeholder="<spring:message code="label.lce.alienregistrationno"/>" name="machineAlien" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='MacReadI551' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-pattern="/^[0-9]{7,9}$/" type="text" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.alienNumber">
                            </div>
                            <!-- Error -->
                            <div class="controls">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.machineAlien.$error.required">
                                    <spring:message code="label.lce.pleaseenteralien"/>
                                </span>
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.machineAlien.$error.pattern">
                                    <spring:message code="label.lce.validalien"/>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="machinePassportNo">
                                <spring:message code="label.lce.passportno"/>
                                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                            </label>
                            <div class="controls">
                                <input type="text" placeholder="<spring:message code="label.lce.passportno"/>" id="machinePassportNo" name="machinePassportNo" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='MacReadI551' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-pattern="/^[a-zA-Z0-9]{6,12}$/" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber">
                            </div>
                            <!-- Error -->
                            <div class="controls">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.machinePassportNo.$error.required">
                                    <spring:message code="label.lce.pleaseenterpassportno"/>
                                </span>
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.machinePassportNo.$error.pattern">
                                    <spring:message code="label.lce.entervalidpasspot"/>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="machineCounty">
                            <spring:message code="label.lce.foreignpassportcountry"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"><a><span aria-label="Required!"></span></a>
                            <span aria-label="Required!"></span>
                            <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span></label>
                            <div class="controls margin10-t">
                                <select name="machineCounty" class="span12" id="machineCounty" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance" format-zero-as-blank ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='MacReadI551' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" >
                                    <option value=''>Country Name</option>
									<option ng-repeat="country in countries" value="{{country.value}}"   ng-selected="{{country.value == householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance}}" label="{{country.name | capitalize:true}}">{{country.name | capitalize:true}}</option>
                                </select>
                            </div>
                            <!-- Error -->
                            <div class="controls">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.machineCounty.$error.required">
                                    <spring:message code="label.lce.passportcountry"/>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="docExpiryDate4">
                                <spring:message code="label.lce.docpassportexpdate"/>
                            </label>
                            <div class="controls">
                                <div class="input-append date" datetimez ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate">
                                    <input id="docExpiryDate4" type="text" class="datepicker" date-formatter date="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" placeholder="MM/DD/YYYY">
                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="machineVisaNo">
                                <spring:message code="label.lce.visano"/>
                            </label>
                            <div class="controls">
                                <input type="text" placeholder="<spring:message code="label.lce.visano"/>" id="machineVisaNo" name="machineVisaNo"  ng-pattern="/^[a-zA-Z0-9]{8}$/" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.visaNumber">
                            </div>
                            <!-- Error -->
                            <div class="margin10-l">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.machineVisaNo.$error.pattern">
                                    <spring:message code="label.lce.entervalidvisa"/>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- MACHINE -->
                    <!-- TEMP -->
                    <div class="control-group tempStamp margin10-t" ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='TempI551' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true ">
                        <div class="control-group">
                            <label class="control-label" for="tempAlienNo">
                                <spring:message code="label.lce.alienregistrationno"/>
                                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                            </label>
                            <div class="controls">
                                <input type="text" placeholder="<spring:message code="label.lce.alienregistrationno"/>" id="tempAlienNo" name="tempAlienNo" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='TempI551' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-pattern="/^[0-9]{7,9}$/" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.alienNumber">
                            </div>
                            <!-- Error -->
                            <div class="controls">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.tempAlienNo.$error.required">
                                    <spring:message code="label.lce.pleaseenteralien"/>
                                </span>
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.tempAlienNo.$error.pattern">
                                    <spring:message code="label.lce.validalien"/>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="tempPassportNo">
                                <spring:message code="label.lce.passportno"/>
                                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                            </label>
                            <div class="controls">
                                <input type="text" placeholder="<spring:message code="label.lce.passportno"/>" id="tempPassportNo" name="tempPassportNo" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='TempI551' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-pattern="/^[a-zA-Z0-9]{6,12}$/" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber">
                            </div>
                            <!-- Error -->
                            <div class="controls">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.tempPassportNo.$error.required">
                                    <spring:message code="label.lce.pleaseenterpassportno"/>
                                </span>
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.tempPassportNo.$error.pattern">
                                    <spring:message code="label.lce.entervalidpasspot"/>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="tempPasCountry">
                            <spring:message code="label.lce.foreignpassportcountry"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"><a><span aria-label="Required!"></span></a>
                            <span aria-label="Required!"></span>
                            <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span></label>
                            <div class="controls margin10-t">
                                <select name="tempPasCountry" class="span12" id="tempPasCountry" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance" format-zero-as-blank ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='TempI551' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" >
                                    <option value=''>Country Name</option>
									<option ng-repeat="country in countries" value="{{country.value}}"   ng-selected="{{country.value == householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance}}" label="{{country.name | capitalize:true}}">{{country.name | capitalize:true}}</option>
                                </select>
                            </div>
                            <!-- Error -->
                            <div class="controls">
                                <span class="validation-error-text-container" for="tempPasCountry" ng-if="submitted && formCitizenship.tempPasCountry.$error.required">
                                    <spring:message code="label.lce.passportcountry"/>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="docPassportExpiryDate">
                                <spring:message code="label.lce.docpassportexpdate"/>
                            </label>
                            <div class="controls">
                                <div class="input-append date" datetimez ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate">
                                    <input id="docPassportExpiryDate" type="text" class="datepicker" date-formatter date="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" placeholder="MM/DD/YYYY">
                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- TEMP -->
                    <!-- i94 -->
                    <div ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I94' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true " class="control-group margin10-t">
                        <div class="control-group">
                            <label class="control-label" for="i94arrdepi94">
                                <spring:message code="label.lce.i94no"/>
                                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                            </label>
                            <div class="controls">
                                <input type="text" placeholder="<spring:message code="label.lce.i94no"/>" id="i94arrdepi94" name="i94arrdepi94" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I94' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-pattern="/^[a-zA-Z0-9]{11}$/" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.i94Number">
                            </div>
                            <!-- Error -->
                            <div class="controls">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.i94arrdepi94.$error.required">
                                    <spring:message code="label.lce.pleaseenteri94"/>
                                </span>
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.i94arrdepi94.$error.pattern">
                                    <spring:message code="label.lce.entervalidi94"/>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="i94arrdepsevisid">
                                <spring:message code="label.lce.sevisid"/>
                            </label>
                            <div class="controls">
                                <input type="text" placeholder="<spring:message code="label.lce.sevisid"/>" id="i94arrdepsevisid" name="i94arrdepsevisid"  ng-pattern="/^(N|n)[0-9]{10}$/" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.SEVISId">
                            </div>
                            <!-- Error -->
                            <div class="margin10-l">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.i94arrdepsevisid.$error.pattern">
                                    <spring:message code="label.lce.entervalidsevisid"/>
                                </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="docPassportExpiryDate2">
                                <spring:message code="label.lce.docpassportexpdate"/>
                            </label>
                            <div class="controls">
                                <div class="input-append date" name='citizenshipDocument_documentExpirationDateDiv' datetimez ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I94' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true">
                                    <input id="docPassportExpiryDate2" type="text" class="datepicker" date-formatter date="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" placeholder="MM/DD/YYYY">
                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                </div>
                            </div>
                            <div class="controls">
                                <span class="validation-error-text-container" for="citizenshipDocument_documentExpirationDateDiv" ng-if="submitted && formCitizenship.citizenshipDocument_documentExpirationDateDiv.$error.required">
                                <spring:message code="lce.pleaseenterdate"/>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- i94 -->
                <!-- i94 UNEXPIRED -->
                <div ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I94UnexpForeignPassport' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true " class="control-group margin10-t">
                    <div class="control-group">
                        <label class="control-label" for="i94Unexpi94">
                            <spring:message code="label.lce.i94no"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <input type="text" placeholder="<spring:message code="label.lce.i94no"/>" id="i94Unexpi94" name="i94Unexpi94" ng-pattern="/^[a-zA-Z0-9]{11}$/" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I94UnexpForeignPassport' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.i94Number">
                        </div>
                        <!-- Error -->
                        <div class="controls">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.i94Unexpi94.$error.required">
                                <spring:message code="label.lce.pleaseenteri94"/>
                            </span>
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.i94Unexpi94.$error.pattern">
                                <spring:message code="label.lce.entervalidi94"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="i94Unexppassportno">
                            <spring:message code="label.lce.passportno"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <input type="text" placeholder="<spring:message code="label.lce.passportno"/>" id="i94Unexppassportno" name="i94Unexppassportno" ng-pattern="/^[a-zA-Z0-9]{6,12}$/" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I94UnexpForeignPassport' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" class="span12"   ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber">
                        </div>
                        <!-- Error -->
                        <div class="controls">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.i94Unexppassportno.$error.required">
                                <spring:message code="label.lce.pleaseenterpassportno"/>
                            </span>
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.i94Unexppassportno.$error.pattern">
                                <spring:message code="label.lce.entervalidpasspot"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="i94UnexpiCountry">
                            <spring:message code="label.lce.foreignpassportcountry"/>
                        <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"><a><span aria-label="Required!"></span></a>
                        <span aria-label="Required!"></span>
                        <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span></label>
                        <div class="controls margin10-t">
                            <select name="i94UnexpiCountry" class="span12" id="i94UnexpiCountry" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance"  format-zero-as-blank ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I94UnexpForeignPassport' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true">
                                <option value=''><spring:message code="label.lce.countryName"/></option>
								<option ng-repeat="country in countries" value="{{country.value}}"   ng-selected="{{country.value == householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance}}" label="{{country.name | capitalize:true}}">{{country.name | capitalize:true}}</option>
                            </select>
                        </div>
                        <!-- Error -->
                        <div class="controls">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.i94UnexpiCountry.$error.required">
                                <spring:message code="label.lce.passportcountry"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="i94UnexDate">
                            <spring:message code="label.lce.docpassportexpdate"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <div class="input-append date" datetimez name="i94UnexDateDiv" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I94UnexpForeignPassport' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true">
                                <input type="text" class="datepicker" id="i94UnexDate" name="i94UnexDate" date-formatter date="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate"   placeholder="MM/DD/YYYY">
                                <span class="add-on"><i class="icon-calendar"></i></span>
                            </div>
                            <!-- Error -->
                            <div class="">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.i94UnexDateDiv.$error.required">
                                    <spring:message code="label.lce.pleaseenterdate"/>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="i94Unexvisano">
                            <spring:message code="label.lce.visano"/>
                        </label>
                        <div class="controls">
                            <input type="text" placeholder="<spring:message code="label.lce.visano"/>" id="i94Unexvisano" name="i94Unexvisano" ng-pattern="/^[a-zA-Z0-9]{8}$/" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.visaNumber">
                        </div>
                        <!-- Error -->
                        <div class="margin10-l">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.i94Unexvisano.$error.pattern">
                                <spring:message code="label.lce.entervalidvisa"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="i94Unexsevisid">
                            <spring:message code="label.lce.sevisid"/>
                        </label>
                        <div class="controls">
                            <input type="text" placeholder="<spring:message code="label.lce.sevisid"/>" id="i94Unexsevisid" name="i94Unexsevisid" ng-pattern="/^(N|n)[0-9]{10}$/" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.SEVISId">
                        </div>
                        <!-- Error -->
                        <div class="margin10-l">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.i94Unexsevisid.$error.pattern">
                                <spring:message code="label.lce.entervalidsevisid"/>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- i94 UNEXPIRED -->
                <!-- UNEXPIRED FOREIGN -->
                <div ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='UnexpForeignPassport' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true " class="control-group margin10-t">
                    <div class="control-group">
                        <label class="control-label" for="unexFori94">
                            <spring:message code="label.lce.i94no"/>
                        </label>
                        <div class="controls">
                            <input type="text" id="unexFori94" placeholder="<spring:message code="label.lce.i94no"/>" name="unexFori94" ng-pattern="/^[a-zA-Z0-9]{11}$/"  class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.i94Number">
                        </div>
                        <!-- Error -->
                        <div class="margin10-l">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.unexFori94.$error.pattern">
                                <spring:message code="label.lce.entervalidi94"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="unexForPassNo">
                            <spring:message code="label.lce.passportno"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <input type="text" id="unexForPassNo" placeholder="<spring:message code="label.lce.passportno"/>" name="unexForPassNo" ng-pattern="/^[a-zA-Z0-9]{6,12}$/" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='UnexpForeignPassport' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber">
                        </div>
                        <!-- Error -->
                        <div class="controls">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.unexForPassNo.$error.required">
                                <spring:message code="label.lce.pleaseenterpassportno"/>
                            </span>
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.unexForPassNo.$error.pattern">
                                <spring:message code="label.lce.entervalidpasspot"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="unexPassCountry">
                        <spring:message code="label.lce.foreignpassportcountry"/>
                        <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"><a><span aria-label="Required!"></span></a>
                        <span aria-label="Required!"></span>
                        <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span> <span class="aria-hidden">required</span></label>
                        <div class="controls margin10-t">
                            <select name="unexPassCountry" class="span12" id="unexPassCountry" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance" format-zero-as-blank  ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='UnexpForeignPassport' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" >
                                <option value=''><spring:message code="label.lce.countryName"/></option>
								<option ng-repeat="country in countries" value="{{country.value}}"   ng-selected="{{country.value == householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance}}" label="{{country.name | capitalize:true}}">{{country.name | capitalize:true}}</option>
                            </select>
                        </div>
                        <!-- Error -->
                        <div class="controls">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.unexPassCountry.$error.required">
                                <spring:message code="label.lce.passportcountry"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="unexDocDate">
                            <spring:message code="label.lce.docpassportexpdate"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <div class="input-append date" name="unexDocDateDiv" datetimez ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='UnexpForeignPassport' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true">
                                <input id="unexDocDate" name="unexDocDate" type="text" class="datepicker" date-formatter date="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" placeholder="MM/DD/YYYY">
                                <span class="add-on"><i class="icon-calendar"></i></span>
                            </div>
                        </div>
                        <!-- Error -->
                        <div class="controls">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.unexDocDateDiv.$error.required"><spring:message code="lce.pleaseenterdate"/></span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="unexForSevis">
                            <spring:message code="label.lce.sevisid"/>
                        </label>
                        <div class="controls">
                            <input type="text" id="unexForSevis" placeholder="<spring:message code="label.lce.sevisid"/>" name="unexForSevis" ng-pattern="/^(N|n)[0-9]{10}$/"  class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.SEVISId">
                        </div>
                        <!-- Error -->
                        <div class="margin10-l">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.unexForSevis.$error.pattern">
                                <spring:message code="label.lce.entervalidsevisid"/>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- UNEXPIRED FOREIGN -->
                <!-- NONIMMIGRATION STUDENT -->
                <div ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I20' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true " class="control-group margin10-t">
                    <div class="control-group">
                        <label class="control-label" for="nonimmigrantsevisid">
                            <spring:message code="label.lce.sevisid"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <input type="text" placeholder="<spring:message code="label.lce.sevisid"/>" id="nonimmigrantsevisid" name="nonimmigrantsevisid" ng-pattern="/^(N|n)[0-9]{10}$/" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I20' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.SEVISId">
                        </div>
                        <!-- Error -->
                        <div class="controls">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.nonimmigrantsevisid.$error.required">
                                <spring:message code="label.lce.pleaseentersevisid"/>
                            </span>
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.nonimmigrantsevisid.$error.pattern">
                                <spring:message code="label.lce.entervalidsevisid"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="nonimmigranti94">
                            <spring:message code="label.lce.i94no"/>
                        </label>
                        <div class="controls">
                            <input type="text" placeholder="<spring:message code="label.lce.i94no"/>" id="nonimmigranti94" name="nonimmigranti94" ng-pattern="/^[a-zA-Z0-9]{11}$/"  class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.i94Number">
                        </div>
                        <!-- Error -->
                        <div class="margin10-l">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.nonimmigranti94.$error.pattern">
                                <spring:message code="label.lce.entervalidi94"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="nonimmigrantpassport">
                            <spring:message code="label.lce.passportno"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <input type="text" id="nonimmigrantpassport" placeholder="<spring:message code="label.lce.passportno"/>" name="nonimmigrantpassport" ng-pattern="/^[a-zA-Z0-9]{6,12}$/" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I20' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber">
                        </div>
                        <!-- Error -->
                        <div class="controls">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.nonimmigrantpassport.$error.required">
                                <spring:message code="label.lce.pleaseenterpassportno"/>
                            </span>
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.nonimmigrantpassport.$error.pattern">
                                <spring:message code="label.lce.entervalidpasspot"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="nonimmigrantPassCountry">
                            <spring:message code="label.lce.foreignpassportcountry"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"><a><span aria-label="Required!"></span></a>
                            <span aria-label="Required!"></span>
                            <span class="aria-hidden">required</span>
                        </label>
                        <div class="controls margin10-t">
                            <select name="nonimmigrantPassCountry" class="span12" id="nonimmigrantPassCountry" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance" format-zero-as-blank ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I20' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true">
                                <option value=''><spring:message code="label.lce.countryName"/></option>
								<option ng-repeat="country in countries" value="{{country.value}}"   ng-selected="{{country.value == householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance}}" label="{{country.name | capitalize:true}}">{{country.name | capitalize:true}}</option>
                            </select>
                        </div>
                        <!-- Error -->
                        <div class="controls">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.nonimmigrantPassCountry.$error.required">
                                <spring:message code="label.lce.passportcountry"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="docPassportExpiryDate2">
                            <spring:message code="label.lce.docpassportexpdate"/>
                        </label>
                        <div class="controls">
                            <div class="input-append date" datetimez ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate">
                                <input id="docPassportExpiryDate2" type="text" class="datepicker" date-formatter date="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate"  placeholder="MM/DD/YYYY">
                                <span class="add-on"><i class="icon-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- NONIMMIGRATION STUDENT -->
                <!-- EXCHANGE VISITOR -->
                <div ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='DS2019' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true " class="control-group margin10-t">
                    <div class="control-group">
                        <label class="control-label" for="excvissevis">
                            <spring:message code="label.lce.sevisid"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <input type="text" id="excvissevis" placeholder="<spring:message code="label.lce.sevisid"/>" name="excvissevis" ng-pattern="/^(N|n)[0-9]{10}$/" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='DS2019' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.SEVISId">
                        </div>
                        <!-- Error -->
                        <div class="controls">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.excvissevis.$error.required">
                                <spring:message code="label.lce.pleaseentersevisid"/>
                            </span>
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.excvissevis.$error.pattern">
                                <spring:message code="label.lce.entervalidsevisid"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="excvisi94">
                            <spring:message code="label.lce.i94no"/>
                        </label>
                        <div class="controls">
                            <input type="text" id="excvisi94" placeholder="<spring:message code="label.lce.i94no"/>" name="excvisi94" ng-pattern="/^[a-zA-Z0-9]{11}$/"  class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.i94Number">
                        </div>
                        <!-- Error -->
                        <div class="margin10-l">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.excvisi94.$error.pattern">
                                <spring:message code="label.lce.entervalidi94"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="excvispassport">
                            <spring:message code="label.lce.passportno"/>
                        </label>
                        <div class="controls">
                            <input type="text" id="excvispassport" placeholder="<spring:message code="label.lce.i94no"/>" name="excvispassport" ng-pattern="/^[a-zA-Z0-9]{6,12}$/" class="span12"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber">
                        </div>
                        <!-- Error -->
                        <div class="margin10-l">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.excvispassport.$error.pattern">
                                <spring:message code="label.lce.entervalidpasspot"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="docPassportExpiryDate3">
                            <spring:message code="label.lce.docpassportexpdate"/>
                        </label>
                        <div class="controls">
                            <div class="input-append date" datetimez ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" data-date-format="mm-dd-yyyy">
                                <input id="docPassportExpiryDate3" type="text" class="datepicker" date-formatter date="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" placeholder="MM/DD/YYYY">
                                <span class="add-on"><i class="icon-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- EXCHANGE VISITOR -->
                <!-- *NEW FIELD -->
                <!-- NOTICE OF ACTION -->
                <div ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I797' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true " class="control-group margin10-t">
                    <div class="control-group">
                        <label class="control-label" for="txtAlienNumber">
                            <spring:message code="label.lce.alienno"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <input id="txtAlienNumber" name="actionAlien" type="text" class="span12" placeholder="<spring:message code="label.lce.alienno"/>" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.alienNumber" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I797' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-pattern="/^[0-9]{7,9}$/" >
                        </div>
						<div class="controls">
                                 <span class="validation-error-text-container" ng-if="submitted && formCitizenship.actionAlien.$error.required">
                                    <spring:message code="label.lce.pleaseenteralien"/>
                                </span>
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.actionAlien.$error.pattern">
                                     <spring:message code="label.lce.validalien"/>
                                 </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="txtI94no">
                            <spring:message code="label.lce.i94no"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <input id="txtI94no" type="text" name="actionI94no" class="span12" placeholder="<spring:message code="label.lce.i94no"/>" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='I797' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-pattern="/^[a-zA-Z0-9]{11}$/" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.i94Number" >
                        </div>
						<!-- Error -->
                            <div class="controls">
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.actionI94no.$error.required">
                                    <spring:message code="label.lce.pleaseenteri94"/>
                                </span>
                                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.actionI94no.$error.pattern">
                                    <spring:message code="label.lce.entervalidi94"/>
                                </span>
                            </div>
                    </div>
                </div>
                <!-- NOTICE OF ACTION -->
                <!-- OTHER -->
                <div ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='other' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true " class="control-group margin10-t">
                    <div class="control-group">
                        <label class="control-label" for="otherAlien">
                            <spring:message code="label.lce.alienno"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <input type="text" id="otherAlien" placeholder="<spring:message code="label.lce.alienno"/>" name="otherAlien" ng-pattern="/^[0-9]{7,9}$/" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='other' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" class="span7"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.alienNumber">
                        </div>
                        <!-- Error -->
                        <div class="margin10-l">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.otherAlien.$error.required">
                                <spring:message code="label.lce.pleaseenteralien"/>
                            </span>
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.otherAlien.$error.pattern">
                                <spring:message code="label.lce.validalien"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="otheri94">
                            <spring:message code="label.lce.i94no"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <input type="text" placeholder="<spring:message code="label.lce.i94no"/>" id="otheri94" name="otheri94" ng-pattern="/^[a-zA-Z0-9]{11}$/" ng-required="householdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected=='other' && householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true"  class="span7"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.i94Number">
                        </div>
                        <!-- Error -->
                        <div class="margin10-l">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.otheri94.$error.required">
                                <spring:message code="label.lce.pleaseenteri94"/>
                            </span>
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.otheri94.$error.pattern">
                                <spring:message code="label.lce.entervalidi94"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="otherpassport">
                            <spring:message code="label.lce.passportno"/>
                        </label>
                        <div class="controls margin10-t">
                            <input type="text" placeholder="<spring:message code="label.lce.passportno"/>" id="otherpassport" name="otherpassport" ng-pattern="/^[a-zA-Z0-9]{6,12}$/"  class="span7"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber">
                        </div>
                        <!-- Error -->
                        <div class="margin10-l">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.otherpassport.$error.pattern">
                                <spring:message code="label.lce.entervalidpasspot"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="othersevis">
                            <spring:message code="label.lce.sevisid"/>
                        </label>
                        <div class="controls">
                            <input type="text" placeholder="<spring:message code="label.lce.sevisid"/>" id="othersevis" name="othersevis" ng-pattern="/^(N|n)[0-9]{10}$/"  class="span7"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.SEVISId">
                        </div>
                        <!-- Error -->
                        <div class="margin10-l">
                            <span class="validation-error-text-container" ng-if="submitted && formCitizenship.othersevis.$error.pattern">
                                <spring:message code="label.lce.entervalidsevisid"/>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="docDescription">
                            <spring:message code="label.lce.docdescription"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls margin10-t">
                            <input id="docDescription" type="text" class="span7" placeholder="<spring:message code="label.lce.docdescription"/>" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentDescription">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="docPassportExpiryDate4">
                            <spring:message code="label.lce.docpassportexpdate"/>
                        </label>
                        <div class="controls margin10-t">
                            <div class="input-append date" datetimez ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" >
                                <input id="docPassportExpiryDate4" type="text" class="datepicker" date-formatter date="householdMember.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate" placeholder="MM/DD/YYYY">
                                <span class="add-on"><i class="icon-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- OTHER -->
            </div>
            <div id="sameNameOnSSN" ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator">
                <label for="hidFirstLastNameLabel">
                    <p>
                        <spring:message code="label.lce.is"/>
                        <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
                        <spring:message code="label.lce.thesamenamethatappearsonthedocument"/>
                        <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                    </p>
					<input type="hidden" id="hidFirstLastNameLabel" />
                </label>
                <div class="margin10-l">
                    <label class="radio-inline" for="samenameT">
                        <input type="radio" id="samenameT" name="samename" ng-value="true" onclick='updateNameOnDocument();' ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.nameSameOnDocumentIndicator" ng-required="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true"  >
                        <spring:message code="label.lce.yes"/>
                    </label>
                    <label class="radio-inline" for="samenameF">
                        <input type="radio" id="samenameF" name="samename" ng-value="false"  ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.nameSameOnDocumentIndicator" ng-required="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" >
                        <spring:message code="label.lce.no"/>
                    </label>
                </div>
                <!-- Error -->
                <div class="margin30-l margin10-t">
                    <span class="validation-error-text-container" ng-if="submitted && formCitizenship.samename.$error.required">
                        <spring:message code="label.lce.thisvalueisrequired"/>
                    </span>
                </div>
            </div>
            <!-- Spouse has a SSN -->
            <!-- //This displays when the spouses's name is not the same on the SSN -->
            <div class="ssnName" ng-show="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true && householdMember.citizenshipImmigrationStatus.citizenshipDocument.nameSameOnDocumentIndicator==false">
                <div class="control-group">
                    <label class="control-label" for="firstNameOnDocument">
                        <spring:message code="label.lce.firstname"/>
                        <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                    </label>
                    <div class="controls">
                        <input type="text" id="firstNameOnDocument" name= "firstNameOnSSNCard" ng-pattern="/^[a-zA-Z]{1,45}$/" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.firstName" class="input-large span6" ng-trim ng-required="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true && householdMember.citizenshipImmigrationStatus.citizenshipDocument.nameSameOnDocumentIndicator==false" placeholder='<spring:message code="label.lce.firstname"/>'>
                    </div>
                    <!-- Error -->
                    <div class="controls margin10-l">
                        <span class="validation-error-text-container" ng-if="submitted && formCitizenship.firstNameOnSSNCard.$error.required">
                            <spring:message code="label.lce.pleaseenterafirstname"/>
                        </span>
                        <span class="validation-error-text-container" ng-if="submitted && formCitizenship.firstNameOnSSNCard.$error.pattern">
                            <spring:message code="label.lce.entervalidfirstname"/>
                        </span>
                    </div>
                </div>
                <!-- First Name -->
                <div class="control-group">
                    <label class="control-label" for="middleNameOnDocument">
                        <spring:message code="label.lce.middlename"/>
                    </label>
                    <div class="controls">
                        <input type="text" id="middleNameOnDocument" ng-pattern="/^[a-zA-Z]{1,45}$/" name="middleNameOnSSNCard" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.middleName" class="input-large span6" placeholder='<spring:message code="label.lce.middlename"/>'>
                    </div>
                    <!-- Error -->
                    <div class="controls margin10-l">
                        <span class="validation-error-text-container" ng-if="submitted && formCitizenship.middleNameOnSSNCard.$error.pattern">
                            <spring:message code="label.lce.entervalidmiddlename"/>
                        </span>
                    </div>
                </div>
                <!-- Middle Name -->
                <div class="control-group">
                    <label class="control-label" for="lastNameOnDocument">
                        <spring:message code="label.lce.lastname"/>
                        <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                    </label>
                    <div class="controls">
                        <input type="text" id="lastNameOnDocument" name="lastNameOnSSNCard" ng-maxlength="45" ng-pattern="/^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$/" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.lastName" class="input-large span6" ng-trim ng-required="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true && householdMember.citizenshipImmigrationStatus.citizenshipDocument.nameSameOnDocumentIndicator==false" placeholder='<spring:message code="label.lce.lastname"/>'>
                    </div>
                    <!-- Error -->
                    <div class="controls margin10-l">
                        <span class="validation-error-text-container" ng-if="submitted && formCitizenship.lastNameOnSSNCard.$error.required">
                            <spring:message code="label.lce.pleaseenteralastname"/>
                        </span>
                        <span class="validation-error-text-container" ng-if="submitted && (formCitizenship.lastNameOnSSNCard.$error.pattern || formCitizenship.lastNameOnSSNCard.$error.maxlength)">
                            <spring:message code="label.lce.entervalidlastname"/>
                        </span>
                    </div>
                </div>
                <!-- Last Name -->
                <div class="control-group">
                    <label class="control-label" for="suffixOnDocument">
                        <spring:message code="label.lce.suffix"/>
                    </label>
                    <div class="controls">
                        <select class="span6" id="suffixOnDocument" name="suffixOnSSNCard" ng-model="householdMember.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.suffix" ng-options="s for s in suffix" >
                            <option value="">
                                <spring:message code="label.lce.suffix"/>
                            </option>
                        </select>
                    </div>
                </div>
                <!-- Suffix -->
            </div>
            <!-- SSN Name the Same -->
        </div>
        <!-- SSN -->
        <div class="control-group margin30-t" id="haveTheseDocuments" style="display: block;" ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator">
            <p>
                <spring:message code="label.lce.does"/>
                <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
                <spring:message code="label.lce.alsohaveanyofthesedocs"/>
            </p>
            <div class="margin10-l docs">
                <label class="checkbox inline" for="otherDocORR">
                    <input type="checkbox"  id="otherDocORR" name="checkboxValid"  ng-model="householdMember.citizenshipImmigrationStatus.otherImmigrationDocumentType.ORRCertificationIndicator">
                    <spring:message code="label.lce.certfromusdep"/>
                </label><br>
                <label class="checkbox inline" for="otherDocORR-under18">
                    <input type="checkbox"  id="otherDocORR-under18" name="checkboxValid" ng-model="householdMember.citizenshipImmigrationStatus.otherImmigrationDocumentType.ORREligibilityLetterIndicator">
                    <spring:message code="label.lce.officeofrefugee"/>
                </label><br>
                <label class="checkbox inline" for="Cuban-or-HaitianEntrant">
                    <input type="checkbox"  id="Cuban-or-HaitianEntrant" name="checkboxValid"  ng-model="householdMember.citizenshipImmigrationStatus.otherImmigrationDocumentType.CubanHaitianEntrantIndicator">
                    <spring:message code="label.lce.cubanhaitian"/>
                </label><br>
                <label class="checkbox inline" for="docRemoval">
                    <input type="checkbox"  id="docRemoval" name="checkboxValid" ng-model="householdMember.citizenshipImmigrationStatus.otherImmigrationDocumentType.WithholdingOfRemovalIndicator">
                    <spring:message code="label.lce.docindicatingwithholdingofremoval"/>
                </label><br>
                <label class="checkbox inline" for="americanSamaoresident">
                    <input type="checkbox"  id="americanSamaoresident" name="checkboxValid"   ng-model="householdMember.citizenshipImmigrationStatus.otherImmigrationDocumentType.AmericanSamoanIndicator">
                    <spring:message code="label.lce.residentofsamoa"/>
                </label><br>
                <label class="checkbox inline" for="adminOrderbyDeptofHomelanSecurity">
                    <input type="checkbox"  id="adminOrderbyDeptofHomelanSecurity" name="checkboxValid" ng-model="householdMember.citizenshipImmigrationStatus.otherImmigrationDocumentType.StayOfRemovalIndicator">
                    <spring:message code="label.lce.adminorderstayingremoval"/>
                </label>
                <span class="errorMessage"></span>
            </div>
        </div>
        <div ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator" class="margin30-t">
            <p>
                <spring:message code="label.lce.has"/>
                <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
                <spring:message code="label.lce.primaryresidencebeenintheus1996"/>
                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
            </p>
            <div class="margin10-l">
                <label class="radio-inline" for="since1996T">
                    <input type="radio" id="since1996T" name="since1996" ng-value="true" ng-required="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-model="householdMember.citizenshipImmigrationStatus.livedIntheUSSince1996Indicator">
                    <spring:message code="label.lce.yes"/>
                </label>
                <label class="radio-inline" for="since1996F">
                    <input type="radio" id="since1996F" name="since1996"  ng-value="false" ng-required="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-model="householdMember.citizenshipImmigrationStatus.livedIntheUSSince1996Indicator">
                    <spring:message code="label.lce.no"/>
                </label>
            </div>
            <!-- Error -->
            <div class="margin30-l margin10-t">
                <span class="validation-error-text-container" ng-if="submitted && formCitizenship.since1996.$error.required">
                  <spring:message code="label.lce.thisvalueisrequired"/>
                </span>
            </div>
        </div>
        <div ng-if="householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator" class="margin30-t">
            <p>
                <spring:message code="label.lce.is"/>
                <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
                <spring:message code="label.lce.anhonorablydischargedveteran"/>
                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
            </p>
            <div class="margin5-l">
                <label class="radio-inline" for="veteranyes">
                    <input type="radio" id="veteranyes" name="veteran" ng-value="true" ng-required="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-model="householdMember.citizenshipImmigrationStatus.honorablyDischargedOrActiveDutyMilitaryMemberIndicator">
                    <spring:message code="label.lce.yes"/>
                </label>
                <label class="radio-inline" for="veteranno">
                    <input type="radio" id="veteranno" name="veteran"  ng-value="false" ng-required="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator == false && householdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false && householdMember.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true" ng-model="householdMember.citizenshipImmigrationStatus.honorablyDischargedOrActiveDutyMilitaryMemberIndicator">
                    <spring:message code="label.lce.no"/>
                </label>
            </div>
            <!-- Error -->
            <div class="margin30-l margin10-t">
                <span class="validation-error-text-container" for="veteranno" ng-if="submitted && formCitizenship.veteran.$error.required">
                    <spring:message code="label.lce.thisvalueisrequired"/>
                </span>
            </div>
        </div>
        <!-- If Spouse is NOT A CITIZEN -->
        <!-- control-group -->
    </form>
    <!-- If Fred is not a citizen, these options will pop up -->
    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
        <a role="button" tabindex="0" class="btn btn-secondary" ng-click="updateNatuCertNumber(householdMember);back()">
            <spring:message code="label.lce.back"/>
        </a>
        <a role="button" tabindex="0" ng-if="formCitizenship.$valid" class="btn btn-primary" ng-click="updateNatuCertNumber(householdMember);next()">
            <spring:message code="label.lce.next"/>
        </a>
        <a role="button" tabindex="0" ng-if="!formCitizenship.$valid" class="btn btn-primary" ng-click="$parent.submitted=true" >
            <spring:message code="label.lce.next"/>
        </a>
    </dl>
</div>
</script>
<script>
var updateNameOnDocument = function(){
	try{
		var scope = angular.element($("#firstNameOnDocument")).scope();
		$('#firstNameOnDocument').val(scope.householdMember.name.firstName).change();
		$('#firstNameOnDocument').triggerHandler("change");

		$('#middleNameOnDocument').val(scope.householdMember.name.middleName).change();
		$('#middleNameOnDocument').triggerHandler("change");

		$('#lastNameOnDocument').val(scope.householdMember.name.lastName).change();
		$('#lastNameOnDocument').triggerHandler("change");

		$('#suffixOnDocument').val(scope.householdMember.name.suffix).change();
		$('#suffixOnDocument').triggerHandler("change");
	} catch(err){ }
};
</script>