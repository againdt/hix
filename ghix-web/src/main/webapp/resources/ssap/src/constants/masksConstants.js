


// Tax return filing status types

const FILING_JOINTLY = 'FILING_JOINTLY';
const FILING_SEPARATELY = 'FILING_SEPARATELY';
const HEAD_OF_HOUSEHOLD = 'HEAD_OF_HOUSEHOLD';
const QUALIFYING_WIDOWER = 'QUALIFYING_WIDOWER';
const SINGLE = 'SINGLE';


// Tax relationships types

const DEPENDENT = 'DEPENDENT';
const FILER_DEPENDENT = 'FILER_DEPENDENT';
const FILER = 'FILER';


// Common type

const UNSPECIFIED = 'UNSPECIFIED';


export const THC_TYPES = {
    HEAD_OF_HOUSEHOLD,
    FILING_JOINTLY,
    FILING_SEPARATELY,
    QUALIFYING_WIDOWER,
    SINGLE,
    FILER,
    FILER_DEPENDENT,
    DEPENDENT,
    UNSPECIFIED
};


const annualTaxIncome = {
    variableIncomeFlags: [
        {
            mask: 1 << 0,
            value: true
        },
        {
            mask: 1 << 1,
            value: null
        },
        {
            mask: 1 << 2,
            value: false
        }
    ],
    unknownIncomeFlags: [
        {
            mask: 1 << 3,
            value: true
        },
        {
            mask: 1 << 4,
            value: null
        },
        {
            mask: 1 << 5,
            value: false
        }
    ]
};

const taxHouseholdComposition = {
    anyChangesFlags: [
        {
            mask: 1 << 0,
            value: true
        },
        {
            mask: 1 << 1,
            value: null
        },
        {
            mask: 1 << 2,
            value: false
        }
    ],
    taxFilerFlags: [
        {
            mask: 1 << 3,
            value: true
        },
        {
            mask: 1 << 4,
            value: null
        },
        {
            mask: 1 << 5,
            value: false
        }
    ],
    moreInformationFlags: [
        {
            mask: 1 << 6,
            value: true
        },
        {
            mask: 1 << 7,
            value: null
        },
        {
            mask: 1 << 8,
            value: false
        }
    ],
    livesWithAnotherParentFlags: [
        {
            mask: 1 << 9,
            value: true
        },
        {
            mask: 1 << 10,
            value: null
        },
        {
            mask: 1 << 11,
            value: false
        }
    ],
    claimedOutsideHouseholdFlags: [
        {
            mask: 1 << 12,
            value: true
        },
        {
            mask: 1 << 13,
            value: null
        },
        {
            mask: 1 << 14,
            value: false
        }
    ],
    isMarriedFlags: [
        {
            mask: 1 << 15,
            value: true
        },
        {
            mask: 1 << 16,
            value: null
        },
        {
            mask: 1 << 17,
            value: false
        }
    ],
    isSpouseOnApplicationFlags: [
        {
            mask: 1 << 18,
            value: true
        },
        {
            mask: 1 << 19,
            value: null
        },
        {
            mask: 1 << 20,
            value: false
        }
    ],

    taxFilingStatusFlags: [
        {
            mask: 1 << 21,
            value: THC_TYPES.HEAD_OF_HOUSEHOLD
        },
        {
            mask: 1 << 22,
            value: THC_TYPES.FILING_JOINTLY
        },
        {
            mask: 1 << 23,
            value: THC_TYPES.FILING_SEPARATELY
        },
        {
            mask: 1 << 24,
            value: THC_TYPES.QUALIFYING_WIDOWER
        },
        {
            mask: 1 << 25,
            value: THC_TYPES.SINGLE
        },
        {
            mask: 1 << 26,
            value: THC_TYPES.UNSPECIFIED
        }
    ],
    taxRelationshipFlags: [
        {
            mask: 1 << 27,
            value: THC_TYPES.FILER
        },
        {
            mask: 1 << 28,
            value: THC_TYPES.DEPENDENT
        },
        {
            mask: 1 << 29,
            value: THC_TYPES.UNSPECIFIED
        }
    ],

};

const taxHouseholdCompositionProgress = {
    provideMoreInfoFlags: [
        {
            mask: 1 << 0,
            value: true
        },
        {
            mask: 1 << 1,
            value: null
        },
        {
            mask: 1 << 2,
            value: false
        }
    ],
    parentExtraQuestionFlags: [
        {
            mask: 1 << 3,
            value: true
        },
        {
            mask: 1 << 4,
            value: null
        },
        {
            mask: 1 << 5,
            value: false
        }
    ],
    siblingExtraQuestionFlags: [
        {
            mask: 1 << 6,
            value: true
        },
        {
            mask: 1 << 7,
            value: null
        },
        {
            mask: 1 << 8,
            value: false
        }
    ],
    questionOneHasBeenResolvedFlags: [
        {
            mask: 1 << 9,
            value: true
        },
        {
            mask: 1 << 10,
            value: false
        },
    ],
    questionTwoHasBeenResolvedFlags: [
        {
            mask: 1 << 11,
            value: true
        },
        {
            mask: 1 << 12,
            value: false
        },
    ],
    questionThreeHasBeenResolvedFlags: [
        {
            mask: 1 << 13,
            value: true
        },
        {
            mask: 1 << 14,
            value: false
        },
    ],
};

export default {
    annualTaxIncome,
    taxHouseholdComposition,
    taxHouseholdCompositionProgress
};