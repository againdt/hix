package com.getinsured.hix.batch.enrollment.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import com.getinsured.enrollment.cms.dsh.sbmi.FileInformationType;
import com.getinsured.enrollment.cms.dsh.sbmi.PolicyMemberType;
import com.getinsured.enrollment.cms.dsh.sbmi.PolicyMemberType.MemberDates;
import com.getinsured.enrollment.cms.dsh.sbmi.PolicyType;
import com.getinsured.enrollment.cms.dsh.sbmi.PolicyType.FinancialInformation;
import com.getinsured.enrollment.cms.dsh.sbmr.FileAcceptanceRejection;
import com.getinsured.enrollment.cms.dsh.sbmr.MissingPolicyType;
import com.getinsured.enrollment.cms.dsh.sbmr.PolicyErrorType;
import com.getinsured.enrollment.cms.dsh.sbmr.SBMIPROCSUMType;
import com.getinsured.enrollment.cms.dsh.sbms.ErrorType;
import com.getinsured.enrollment.cms.dsh.sbms.SBMS;
import com.getinsured.hix.batch.enrollment.util.EnrollmenCmsXmlJobThread;
import com.getinsured.hix.dto.enrollment.EnrollmentCoverageValidationRequest.InsuranceType;
import com.getinsured.hix.dto.enrollment.EnrollmentXMLValidationDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentXMLValidationDTO.XMLValidationStatus;
import com.getinsured.hix.dto.enrollment.EnrollmentXMLValidationDetails;
import com.getinsured.hix.dto.enrollment.EnrollmentXMLValidationDetails.ValidationStatus;
import com.getinsured.hix.enrollment.repository.IEnrolleeAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentCmsInRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentCmsOutDtlRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentCmsOutRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentCmsValidationRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPremiumRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentConstants.EnrollmentCmsValidationDeckEnum;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.enrollment.util.EnrollmentIrsEscapeHandler;
import com.getinsured.hix.enrollment.util.EnrollmentIrsSaxErrorHandler;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.enrollment.util.ResourceResolver;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentCmsCount;
import com.getinsured.hix.model.enrollment.EnrollmentCmsIn;
import com.getinsured.hix.model.enrollment.EnrollmentCmsInDtl;
import com.getinsured.hix.model.enrollment.EnrollmentCmsInMissingPolicy;
import com.getinsured.hix.model.enrollment.EnrollmentCmsOut;
import com.getinsured.hix.model.enrollment.EnrollmentCmsOutDtl;
import com.getinsured.hix.model.enrollment.EnrollmentCmsOutSkippedPolicy;
import com.getinsured.hix.model.enrollment.EnrollmentPremium;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.JiraUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;

@Service("enrollmentCmsOutService")
@Transactional
public class EnrollmentCmsOutServiceImpl implements EnrollmentCmsOutService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentCmsOutServiceImpl.class);

	@Autowired(required = true)
	private IEnrollmentRepository enrollmentRepository;
	@Autowired(required = true)
	private IEnrollmentPremiumRepository enrollmentPremiumRepository;
	@Autowired(required = true)
	private IEnrolleeRepository enrolleeRepository;
	@Autowired(required = true)
	private IEnrolleeAudRepository enrolleeAudRepository;
	@Autowired(required = true)
	private IEnrollmentCmsOutRepository enrollmentCmsOutRepository;
	@Autowired(required = true)
	private IEnrollmentCmsInRepository enrollmentCmsInRepository;
	@Autowired
	private BatchJobExecutionService batchJobExecutionService;
	@Autowired 
	private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;
	@Autowired(required = true)
	private IEnrollmentCmsOutDtlRepository enrollmentCmsOutDtlRepository;
	@Autowired(required = true)
	private IEnrollmentCmsValidationRepository enrollmentCmsValidationRepository;
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private JobService jobService;

	@Autowired private Gson platformGson;
	
	private static final String ERROR_DELIMITER = ":::";
	
	private static int fileNameCounter = 0;
	
	@Override
	public List<BatchJobExecution> getRunningBatchList(String jobName) {
		return batchJobExecutionService.findRunningJob(jobName);
	}

	@Override
	public List<Integer> getEnrollmentIdByIssuerAndYear(int issuerId, int year) {
		List<Integer> allEnrollments = new ArrayList<>();
		List<Integer> activeEnrollments = null;
		if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENABLE_CMS_VALIDATION_DECK))){
			activeEnrollments = enrollmentRepository.getEnrollmentIdByIssuerAndYear(issuerId, Integer.toString(year)); // Query includes enrollments without confirmation dates
		}else {
			activeEnrollments = enrollmentRepository.getEffectuatedEnrollmentIdByIssuerAndYear(issuerId, Integer.toString(year));
		}
		List<Integer> cancelledEnrollments = enrollmentCmsOutDtlRepository.getApplicableCancelledEnrollmentIds(issuerId, year);
		if(null != activeEnrollments && !activeEnrollments.isEmpty()){
			allEnrollments.addAll(activeEnrollments);
		}
		if(null != cancelledEnrollments && !cancelledEnrollments.isEmpty()){
			allEnrollments.addAll(cancelledEnrollments);
		}
		//HIX-108607 Add code to include Pending enrollments
		if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.CMS_INCLUDE_PENDING_ENROLLMENTS))){
			List<Integer> pendingEnrollments = enrollmentRepository.getPendingEnrollmentIdByIssuerAndYear(issuerId, Integer.toString(year));
			if(null != pendingEnrollments && !pendingEnrollments.isEmpty()){
				allEnrollments.addAll(pendingEnrollments);
			}
		}
		return allEnrollments; 
	}

	@Override
	public List<Enrollment> getEnrollmentById(List<Integer> enrollmentIdList) {
		return enrollmentRepository.findEnrollmentByEnrollmentIds(enrollmentIdList);
	}

	@Override
	public void makeAndWriteCMSXml(List<Integer> enrollmentIdList, Integer year, Integer month, Integer issuerId, Integer batchYear, Long batchExecutionId, Integer partitionId, long batchStartTime) throws Exception, InterruptedException, ExecutionException {

		if(null != enrollmentIdList && !enrollmentIdList.isEmpty()){

			int maxEnrollmentPerFile = Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_PER_CMSXMLFILE));

			int numberOfCMSFile = (enrollmentIdList.size() <= maxEnrollmentPerFile ) ? 1 : enrollmentIdList.size()/ maxEnrollmentPerFile;

			if (enrollmentIdList.size() > maxEnrollmentPerFile && enrollmentIdList.size() % maxEnrollmentPerFile != 0) {
				numberOfCMSFile++;
			}

			List<List<Integer>> fileIdList = getSubList(enrollmentIdList, maxEnrollmentPerFile);

			XMLGregorianCalendar fileCreateDateTime = getXmlGregorianCalendarDateTime(new Date());
			
			String issuerFileSetId =  generateIssuerFileIdSet(issuerId, batchExecutionId, partitionId);

			for (int fileNumber = 0 ; fileNumber < fileIdList.size() ; fileNumber++) {

				com.getinsured.enrollment.cms.dsh.sbmi.Enrollment cmsXmlEnrollment = new com.getinsured.enrollment.cms.dsh.sbmi.Enrollment();

				Map<String, String> enrollmentSkipMap = new ConcurrentHashMap<String, String>();

				String fileId =  getHiosIssuerIdById(issuerId)
						       + year.toString()
						       + String.format("%02d", partitionId)
						       + String.format("%02d", fileNumber+1)
							   + String.format("%06d", batchExecutionId);
				
				cmsXmlEnrollment.setFileInformation(getFileInformation(year, issuerId, fileNumber+1, numberOfCMSFile, fileId, fileCreateDateTime, issuerFileSetId));

				List<PolicyType> policyTypeList = cmsXmlEnrollment.getPolicy();
				int defaultThreadPoolSize = 5;
				String enrollmentCmsOutXmlThreadPool = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_CMSOUTXML_THREAD_POOL);
				if(enrollmentCmsOutXmlThreadPool!=null && !"".equals(enrollmentCmsOutXmlThreadPool.trim())){
					defaultThreadPoolSize= Integer.parseInt(enrollmentCmsOutXmlThreadPool);
				}
				ExecutorService executor = Executors.newFixedThreadPool(defaultThreadPoolSize);

				List<Integer> enrollmentSubIdList = fileIdList.get(fileNumber);
				Integer maxIdPerSublist = 100;
				String maxIdPerSublistStr = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_PER_CMSOUTXMLTHREAD);
				if(null != maxIdPerSublistStr && NumberUtils.isNumber(maxIdPerSublistStr.trim())){
					maxIdPerSublist=Integer.valueOf(maxIdPerSublistStr.trim());
				}
			/*	int maxIdPerSublist = (enrollmentSubIdList.size() <= EnrollmentConstants.TEN ) ? enrollmentSubIdList.size() : enrollmentSubIdList.size()/ EnrollmentConstants.TEN;

				if (enrollmentSubIdList.size() > EnrollmentConstants.TEN &&  enrollmentSubIdList.size() % EnrollmentConstants.TEN != 0) {
					maxIdPerSublist++;
				}*/
				List<Integer> filteredList = enrollmentSubIdList;
				/**
				 * Pass enrollment ID list to validation deck. Store defaulters in the enrollment skip map
				 * and pass the remaining in the thread
				 */
				if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENABLE_CMS_VALIDATION_DECK))){
					filteredList = filteredListViaValidationDeck(enrollmentSubIdList, enrollmentSkipMap);
				}
				
				//Pass filtered out enrollments here
				List<List<Integer>> enrollmentSubSubIdList = getSubList(filteredList, maxIdPerSublist);

				List<Callable<List<PolicyType>>> taskList = new ArrayList<>();

				for (int index = 0; index < enrollmentSubSubIdList.size(); index++){
					taskList.add(new EnrollmenCmsXmlJobThread(enrollmentSubSubIdList.get(index), year, issuerId, index, enrollmentSkipMap,batchExecutionId, this));
				} 

				executor.invokeAll(taskList)
				.stream()
				.map(future -> { 
								try{
									return future.get();
									
									}catch(Exception ex){
									
										LOGGER.error("Exception occurred in makeAndWriteCMSXml: ", ex);
								    }
								return null;
							} 
					)
				.filter(p -> p != null)
				.forEach(e -> policyTypeList.addAll(e));
				
				executor.shutdown();
				String batchJobStatus=null;
				if(jobService != null && batchExecutionId != -1){
					batchJobStatus = jobService.getJobExecution(batchExecutionId).getStatus().name();
				}
				if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
					throw new GIException(EnrollmentConstants.BATCH_STOP_MSG);
				}

				Map<String, String> fileParameters = generateCMSXml(cmsXmlEnrollment, year, batchStartTime);
				
				//Jira: HIX-105787, Adding new ExecutorService thread to avoid DB connection time out issue
				ExecutorService executorService = Executors.newFixedThreadPool(1);
				try {
					executorService.submit(new  Runnable() {
					    public void run() {
					    	logToEnrollmentCmsOut(fileParameters, fileId, enrollmentSubIdList, year, batchYear, month, enrollmentSkipMap, issuerFileSetId, batchExecutionId);
					    }
					});		
				}catch(Exception e) {
					LOGGER.error("Error in updating cms out table", e);
				}
				executorService.shutdown();
				executorService.awaitTermination(EnrollmentConstants.ONE, TimeUnit.HOURS);
			}
		}
	}

	private void logToEnrollmentCmsOut(Map<String, String> fileParameters, String fileId, List<Integer> enrollmentIdList, Integer year, Integer batchYear, Integer month, Map<String, String> enrollmentSkipMap, String issuerFileSetId, Long batchExecutionId) {
		EnrollmentCmsOut enrollmentCmsOut = new EnrollmentCmsOut();
		List<Integer> successList = new ArrayList<>();
		String hiosIssuerId = enrollmentRepository.getHiosIssuerId(enrollmentIdList.get(0));
		int enrollmentCount = enrollmentIdList.size();
		enrollmentCmsOut.setEligibleEnrollmentCount(enrollmentCount);
		List<EnrollmentCmsOutSkippedPolicy> enrollmentCmsOutSkippedPolicies = new ArrayList<>();
		String enrollmentIds = null;
		if(!enrollmentSkipMap.isEmpty()){
			enrollmentCount = enrollmentCount - enrollmentSkipMap.size();
			Set<String> skipEnrollmentIdList = enrollmentSkipMap.keySet();
			for(Integer id : enrollmentIdList){
				if(!skipEnrollmentIdList.contains(String.valueOf(id))) {
					successList.add(Integer.valueOf(id));
				}
			}
			enrollmentCmsOut.setSkipRecords(enrollmentSkipMap.toString());
			if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENABLE_CMS_VALIDATION_DECK))){
				for(Entry<String, String> skipMap : enrollmentSkipMap.entrySet()) {
					EnrollmentCmsOutSkippedPolicy skippedPolicy = new EnrollmentCmsOutSkippedPolicy();
					skippedPolicy.setExchangeAssignedPolicyId(skipMap.getKey());
					if(EnrollmentUtils.isNotNullAndEmpty(skipMap.getValue()) && skipMap.getValue().contains(ERROR_DELIMITER)) {
						String[] error = skipMap.getValue().trim().split(ERROR_DELIMITER);
						skippedPolicy.setErrorCode(error[0]);
						skippedPolicy.setDescription(error[1]);
					}else if(EnrollmentUtils.isNotNullAndEmpty(skipMap.getValue())) {
						skippedPolicy.setDescription(skipMap.getValue().trim());
					}
					skippedPolicy.setEnrlCmsOut(enrollmentCmsOut);
					enrollmentCmsOutSkippedPolicies.add(skippedPolicy);
				}
				if(!enrollmentCmsOutSkippedPolicies.isEmpty())
				{
					enrollmentCmsOut.setEnrollmentCmsOutSkippedPolicies(enrollmentCmsOutSkippedPolicies);
				}
			}
			enrollmentIds = successList.stream().map(Object::toString).collect(Collectors.joining(","));
		}else{
			enrollmentIds = enrollmentIdList.stream().map(Object::toString).collect(Collectors.joining(","));
		}
		enrollmentCmsOut.setYear(batchYear);
		enrollmentCmsOut.setCoverageYear(year);
		enrollmentCmsOut.setMonth(month);
		enrollmentCmsOut.setHiosIssuerId(hiosIssuerId);
		enrollmentCmsOut.setFileName(fileParameters.get("fileName"));
		enrollmentCmsOut.setFileSize(Long.valueOf(fileParameters.get("fileSize")));
		enrollmentCmsOut.setOutEnrollmentCount(enrollmentCount);
		enrollmentCmsOut.setOutEnrollmentIds(enrollmentIds);
		enrollmentCmsOut.setFileId(fileId);
		enrollmentCmsOut.setIssuerFileSetId(Long.valueOf(issuerFileSetId));
		enrollmentCmsOut.setBatchExecutionId(batchExecutionId);
		enrollmentCmsOut.setInboundAction("SENT");

		enrollmentCmsOutRepository.saveAndFlush(enrollmentCmsOut);
		LOGGER.info("Cms out logged for file::"+enrollmentCmsOut.getFileName()+" Time::"+ new Date());
	}
	
	@Override
	public void deleteFromEnrollmentCmsOut( Long batchExecutionId) {
		if(batchExecutionId!=null){
			enrollmentCmsOutRepository.deleteEnrollmentCmsOutByJobId(batchExecutionId);
		}
	}

	private List<List<Integer>> getSubList(List<Integer> enrollmentIdList, int maxIdPerSublist) {
		List<List<Integer>> subListList = new ArrayList<>();
		final int N = enrollmentIdList.size();

		for (int i = 0; i < N; i += maxIdPerSublist) {
			subListList.add(new ArrayList<Integer>(enrollmentIdList.subList(i, Math.min(N, i + maxIdPerSublist))));
		}
		return subListList;
	}

	@Override
	public List<PolicyType> makePolicyTypeList(List<Integer> enrollmentIdList, int year, int issuerId, Map<String, String> enrollmentSkipMap, Long jobExecutionId) throws Exception {
		if (null != enrollmentIdList && !enrollmentIdList.isEmpty()) {

			List<Enrollment> enrlList = this.getEnrollmentById(enrollmentIdList);

			List<PolicyType> policyTypeList = new ArrayList<PolicyType>();
//			String batchJobStatus=null;
			for (Enrollment enrollment : enrlList) {
		/*		if(jobService != null && jobExecutionId != -1){
					batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
				}
				if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
					throw new GIException(EnrollmentConstants.BATCH_STOP_MSG);
				}*/
				
				try{
					PolicyType policyType = new PolicyType();
					policyType.setRecordControlNumber(Integer.valueOf(String.valueOf(enrollment.getId())));
					policyType.setQHPId(enrollment.getCMSPlanID().substring(0,enrollment.getCMSPlanID().length() - 2));
					policyType.setExchangeAssignedPolicyId(Integer.toString(enrollment.getId()));
					policyType.setExchangeAssignedSubscriberId(enrollment.getExchgSubscriberIdentifier());
					policyType.setIssuerAssignedPolicyId(enrollment.getIssuerAssignPolicyNo());
					policyType.setIssuerAssignedSubscriberId(enrollment.getIssuerSubscriberIdentifier());
					policyType.setPolicyStartDate(toXMLGregorianCalendarDateOnly(enrollment.getBenefitEffectiveDate()));
					policyType.setPolicyEndDate(toXMLGregorianCalendarDateOnly(enrollment.getBenefitEndDate()));

					if (enrollment.getEnrollmentConfirmationDate() != null && !EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(enrollment.getEnrollmentStatusLkp().getLookupValueCode())) {
						policyType.setEffectuationIndicator(EnrollmentConstants.Y);
					} else {
						policyType.setEffectuationIndicator(EnrollmentConstants.N);
					}

					if (EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode())) {
						policyType.setInsuranceLineCode(InsuranceType.HLT.toString());
					} else if (EnrollmentConstants.INSURANCE_TYPE_DENTAL_CODE.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode())) {
						policyType.setInsuranceLineCode(InsuranceType.DEN.toString());
					}

					List<PolicyMemberType> policyMemberList = policyType.getMemberInformation();
					List<Enrollee> enrolleeList = enrolleeRepository.getEnrolleeByEnrollmentID(enrollment.getId());
					
					List<Enrollee> cancelledEnrollees = enrolleeList.stream()
							.filter(e -> EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(e.getEnrolleeLkpValue().getLookupValueCode()))
							.collect(Collectors.toList());
					
					enrolleeList.removeAll(cancelledEnrollees);
					
					Map<String, List<Enrollee>> enrolleeGroupedByMemberId = enrolleeList.stream()
							.filter(e -> null != e.getExchgIndivIdentifier())
							.collect(Collectors.groupingBy(Enrollee::getExchgIndivIdentifier));
					
					Map<String, PolicyMemberType> memberMap = Collections.synchronizedMap(new HashMap<>());
					
					enrolleeList.forEach(e -> addToMemberMap(e, memberMap));
					
					cancelledEnrollees.stream()
										.filter(e -> !isEnrolleeOverlap(e, enrolleeGroupedByMemberId))
										.forEach(e -> addToMemberMap(e, memberMap));
					
					policyMemberList.addAll(memberMap.values());

					List<FinancialInformation> financialInformationList = policyType.getFinancialInformation();

					List<EnrollmentPremium> enrollmentPremiumList = enrollmentPremiumRepository.findNotNullPremiumByEnrollmentId(enrollment.getId());
					if (null != enrollmentPremiumList && !enrollmentPremiumList.isEmpty()
							&& !enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)) {
						Map<String, String> ratingAreaMap = getRatingAreaInfoFromSubscriber(enrollment);
						
						Float grossPremiumAmount = (float) -1;
						Float aptcAmount = (float) -1;
						Float slcspPremiumAmount = (float) -1;
						Calendar startDate = null;

						for (int i = 0; i < enrollmentPremiumList.size(); i++) {

							EnrollmentPremium enrollmentPremium = enrollmentPremiumList.get(i);

							if (grossPremiumAmount == -1 && aptcAmount == -1 && slcspPremiumAmount == -1) {

								grossPremiumAmount = enrollmentPremium.getGrossPremiumAmount();
								aptcAmount = enrollmentPremium.getAptcAmount();
								slcspPremiumAmount = enrollmentPremium.getSlcspPremiumAmount();
								startDate = EnrollmentUtils.dateToCalendar(enrollment.getBenefitEffectiveDate());
								
								if(enrollmentPremiumList.size() == 1){
									financialInformationList.addAll(getFinancialInformation(enrollmentPremiumList.get(0), enrollment,startDate.getTime(),ratingAreaMap,true));
								}
								continue;
							}

							if (checkIfPremiumChange(grossPremiumAmount, aptcAmount,
									slcspPremiumAmount, enrollmentPremium)) {
								financialInformationList.addAll(getFinancialInformation(enrollmentPremiumList.get(i - 1), enrollment,startDate.getTime(),ratingAreaMap,false));
								grossPremiumAmount = enrollmentPremium.getGrossPremiumAmount();
								aptcAmount = enrollmentPremium.getAptcAmount();
								slcspPremiumAmount = enrollmentPremium.getSlcspPremiumAmount();
								startDate = new GregorianCalendar(enrollmentPremium.getYear(),enrollmentPremium.getMonth()-1, 1);
							}

							if (i == (enrollmentPremiumList.size() - 1)) {
								financialInformationList.addAll(getFinancialInformation(enrollmentPremiumList.get(i), enrollment,startDate.getTime(),ratingAreaMap,true));
							}
						}
					}else if(enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
						// For Cancel Enrollee Populate FinancialInformation from Enrollment table
						FinancialInformation financialInformation = new FinancialInformation();
						
						financialInformation.setFinancialEffectiveStartDate(toXMLGregorianCalendarDateOnly(enrollment.getBenefitEffectiveDate()));
						
						financialInformation.setFinancialEffectiveEndDate(toXMLGregorianCalendarDateOnly(enrollment.getBenefitEndDate()));
						
						if(enrollment.getAptcAmt() != null){
							BigDecimal aptcAmount = enrollment.getAptcAmt() < 0 ? 
											                     new BigDecimal(0) : 
										                         BigDecimal.valueOf(enrollment.getAptcAmt()).setScale(2, BigDecimal.ROUND_HALF_UP);
				                     
							financialInformation.setMonthlyAPTCAmount(aptcAmount);
						}
						
						BigDecimal stateSubsidyAmount = null;
						String enableStateSubsidy = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_SUBSIDY);
												
						if ("Y".equalsIgnoreCase(enableStateSubsidy) && enrollment.getStateSubsidyAmt() != null) {
							stateSubsidyAmount = enrollment.getStateSubsidyAmt().compareTo(BigDecimal.ZERO) < 0
									? new BigDecimal(0)
									: enrollment.getStateSubsidyAmt().setScale(2, BigDecimal.ROUND_HALF_UP);
						}
						
						financialInformation.setMonthlyOtherPaymentAmount1(stateSubsidyAmount);
						financialInformation.setMonthlyOtherPaymentAmount2(null);
						
						if (null != enrollment.getNetPremiumAmt()) {
						financialInformation.setMonthlyTotalIndividualResponsibilityAmount(BigDecimal.valueOf(enrollment.getNetPremiumAmt())
                                .setScale(2, BigDecimal.ROUND_HALF_UP));
						}
						
						if (null != enrollment.getGrossPremiumAmt()) {
							financialInformation.setMonthlyTotalPremiumAmount(BigDecimal.valueOf(enrollment.getGrossPremiumAmt())
                                													.setScale(2, BigDecimal.ROUND_HALF_UP));
						}
						
						if (null != enrollment.getCMSPlanID()) {
							financialInformation.setCSRVariantId(enrollment.getCMSPlanID().substring(14));
						}
						
						if(enrollment.getGrossPremiumAmt() != null & enrollment.getCsrMultiplier() != null){
							if(financialInformation.getCSRVariantId() != null && !financialInformation.getCSRVariantId().equalsIgnoreCase("01")){
								financialInformation.setMonthlyCSRAmount(BigDecimal.valueOf(enrollment.getGrossPremiumAmt() * enrollment.getCsrMultiplier())
			                            .setScale(2, BigDecimal.ROUND_HALF_UP));
							}
						}
						
						Enrollee subEnrollee = enrolleeRepository.findSubscriberByEnrollmentID(enrollment.getId());
						if (null != subEnrollee) {
							financialInformation.setRatingArea(EnrollmentUtils.getFormatedRatingArea(subEnrollee.getRatingArea()));
						}
						
						financialInformationList.add(financialInformation);
					}else{
						LOGGER.error("No premium history found for enrollment Id :: "+ enrollment.getId());
						throw new GIException("No premium history found for enrollment Id :: "+ enrollment.getId());
					}
					policyTypeList.add(policyType);
				}catch(Exception e){
					LOGGER.error("Skip Record :: Exception in creating policy list type for enrollment Id :: " + enrollment.getId(), e);
					enrollmentSkipMap.put(String.valueOf(enrollment.getId()), EnrollmentUtils.shortenedStackTrace(e, 4));
				}
			}
			return policyTypeList;
		} else {
			return null;
		}

	}

	private PolicyMemberType getPolicyMemberType(Enrollee enrollee) {
		PolicyMemberType policyMemberType = new PolicyMemberType();

		policyMemberType.setExchangeAssignedMemberId(enrollee.getExchgIndivIdentifier());
		policyMemberType.setSubscriberIndicator(String.valueOf(enrollee.getSubscriberFlag()));
		policyMemberType.setIssuerAssignedMemberId(enrollee.getIssuerIndivIdentifier());
		policyMemberType.setMemberFirstName(enrollee.getFirstName());
		policyMemberType.setMemberMiddleName(enrollee.getMiddleName());
		policyMemberType.setMemberLastName(enrollee.getLastName());
		policyMemberType.setNameSuffix(enrollee.getSuffix());
		policyMemberType.setBirthDate(toXMLGregorianCalendarDateOnly(enrollee.getBirthDate()));
		policyMemberType.setSocialSecurityNumber(enrollee.getTaxIdNumber());
		if (null != enrollee.getGenderLkp()) {
			policyMemberType.setGenderCode(enrollee.getGenderLkp().getLookupValueCode().toUpperCase());
		}

		if (null != enrollee.getHomeAddressid()) {
			policyMemberType.setPostalCode(enrollee.getHomeAddressid().getZip());
		}

		if (null != enrollee.getLanguageLkp()) {
			policyMemberType.setLanguageCode(enrollee.getLanguageLkp().getLookupValueCode().toUpperCase());
		}
		
		if(null != enrollee.getEffectiveStartDate() && null != enrollee.getEffectiveEndDate() ){
			List<MemberDates> memberDateList = policyMemberType.getMemberDates();
			MemberDates memberDate = new MemberDates();
			memberDate.setMemberStartDate(toXMLGregorianCalendarDateOnly(enrollee.getEffectiveStartDate()));
			memberDate.setMemberEndDate(toXMLGregorianCalendarDateOnly(enrollee.getEffectiveEndDate()));
			memberDateList.add(memberDate);	
		}

		policyMemberType.setLanguageQualifierCode("LE");
		
		policyMemberType.setNonCoveredSubscriberInd(null);
		policyMemberType.setRaceEthnicityCode(null);

		if (null != enrollee.getTobaccoUsageLkp()) {
			policyMemberType.setTobaccoUseCode(enrollee.getTobaccoUsageLkp().getLookupValueCode().toUpperCase());
		}
		return policyMemberType;
	}

	private FileInformationType getFileInformation(int year, int issuerId, int fileNumber, int totalNumberOfFile, String fileId, XMLGregorianCalendar fileCreateDateTime, String issuerFileSetId) {
		FileInformationType fileInformation = new FileInformationType();

		fileInformation.setCoverageYear(year);
		fileInformation.setFileCreateDateTime(fileCreateDateTime);
		fileInformation.setFileId(fileId);
		fileInformation.setTenantId(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)+"0");

		
		FileInformationType.IssuerFileInformation issuerFileInformation = new FileInformationType.IssuerFileInformation();
		
		if(totalNumberOfFile > 1){
			FileInformationType.IssuerFileInformation.IssuerFileSet issuerFileSet = new FileInformationType.IssuerFileInformation.IssuerFileSet();
			issuerFileSet.setIssuerFileSetId(issuerFileSetId);
			issuerFileSet.setFileNumber(fileNumber);
			issuerFileSet.setTotalIssuerFiles(totalNumberOfFile);
			issuerFileInformation.setIssuerFileSet(issuerFileSet);
		}
		
		issuerFileInformation.setIssuerId(getHiosIssuerIdById(issuerId));
		fileInformation.setIssuerFileInformation(issuerFileInformation);
		return fileInformation;
	}

	private boolean checkIfPremiumChange(Float grossPremiumAmount, Float aptcAmount, Float slcspPremiumAmount, EnrollmentPremium enrollmentPremium) {
		if (null != grossPremiumAmount && null == enrollmentPremium.getGrossPremiumAmount()) {
			return true;
		}
		if (null != aptcAmount && null == enrollmentPremium.getAptcAmount()) {
			return true;
		}
		if (null != slcspPremiumAmount && null == enrollmentPremium.getSlcspPremiumAmount()) {
			return true;
		}

		// =============================================================

		if (null == grossPremiumAmount && null != enrollmentPremium.getGrossPremiumAmount()) {
			return true;
		}
		if (null == aptcAmount && null != enrollmentPremium.getAptcAmount()) {
			return true;
		}
		if (null == slcspPremiumAmount &&  null != enrollmentPremium.getSlcspPremiumAmount()) {
			return true;
		}

		// =============================================================

		if (null != grossPremiumAmount && null != enrollmentPremium.getGrossPremiumAmount() && Float.compare(grossPremiumAmount, enrollmentPremium.getGrossPremiumAmount()) != 0 ) {
			return true;
		}
		if (null != aptcAmount && null != enrollmentPremium.getAptcAmount() &&  Float.compare(aptcAmount, enrollmentPremium.getAptcAmount()) != 0) {
			return true;
		}
		if (null != slcspPremiumAmount && null != enrollmentPremium.getSlcspPremiumAmount() &&  Float.compare(slcspPremiumAmount, enrollmentPremium.getSlcspPremiumAmount()) != 0) {
			return true;
		}

		return false;
	}

	private Map<String, String> generateCMSXml(com.getinsured.enrollment.cms.dsh.sbmi.Enrollment cmsXmlEnrollment, Integer year, long batchStartTime) throws GIException {
		Map<String, String> fileParameters = new HashMap<String, String>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("'D'yyMMdd'.T'HHmmssSSS");
		String fileName = null;

		try {
			String tradingPartnerID = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.TradingPartnerID);
			String environment = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_CMS_ENVIRONMENT);

			fileName = tradingPartnerID + ".EPS.SBMI."
					+ dateFormat.format(getUniqueDate(batchStartTime))
					+ "."
					+ environment
					+ "."
					+ EnrollmentConstants.TRANSFER_DIRECTION_IN;

			StringBuilder cmsXMLFileNameBuilder =  EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CMS.toString(),
							                                                                                     EnrollmentConstants.TRANSFER_DIRECTION_OUT)
							                                      .append(File.separatorChar).append(EnrollmentConstants.WIP_FOLDER_NAME);
			//Create Directory
			EnrollmentUtils.createDirectory(cmsXMLFileNameBuilder.toString());
			cmsXMLFileNameBuilder.append(File.separatorChar);
			cmsXMLFileNameBuilder.append(fileName);

			JAXBContext jaxbContext = JAXBContext.newInstance(com.getinsured.enrollment.cms.dsh.sbmi.Enrollment.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "utf-8");
			jaxbMarshaller.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler", new EnrollmentIrsEscapeHandler());

			jaxbMarshaller.marshal(cmsXmlEnrollment, new File(cmsXMLFileNameBuilder.toString()));
			fileParameters.put("fileName", fileName);
			fileParameters.put("fileSize", String.valueOf(new File(cmsXMLFileNameBuilder.toString()).length()));
		} catch (JAXBException e) {
			LOGGER.error("EnrollmentCmsOutServiceImpl @ generateCMSXml  ", e);
			throw new GIException("Error generating CMS XML @ generateCMSXml", e);
		}
		return fileParameters;
	}

	private synchronized Date getUniqueDate(long batchStartTime) {
		return new Date(batchStartTime+(fileNameCounter++));
	}

	/**
	 * Convert normal date to XMLGregorianCalendar date
	 * @param date
	 * @return XMLGregorianCalendar 
	 */
	private XMLGregorianCalendar getXmlGregorianCalendarDateTime(Date date) {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(date);
		XMLGregorianCalendar xmlGrogerianCalendar = null;
		try {
			xmlGrogerianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();

			xmlGrogerianCalendar.setYear(gregorianCalendar.get(GregorianCalendar.YEAR));
			// need to add 1 in month as Calender month is zero base and XMLGregorianCalendar month is 1 base.
			xmlGrogerianCalendar.setMonth(gregorianCalendar.get(GregorianCalendar.MONTH)+1);
			xmlGrogerianCalendar.setDay(gregorianCalendar.get(GregorianCalendar.DAY_OF_MONTH));
			xmlGrogerianCalendar.setHour(gregorianCalendar.get(GregorianCalendar.HOUR_OF_DAY));
			xmlGrogerianCalendar.setMinute(gregorianCalendar.get(GregorianCalendar.MINUTE));
			xmlGrogerianCalendar.setSecond(gregorianCalendar.get(GregorianCalendar.SECOND));

		} catch (DatatypeConfigurationException e) {
			LOGGER.error("Error converting to XMLGregorianCalendar Date", e);
		}
		return xmlGrogerianCalendar;
	}

	private XMLGregorianCalendar toXMLGregorianCalendarDateOnly(Date date){
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(date);
		XMLGregorianCalendar xmlGrogerianCalendar= null;

		try {
			// need to add 1 in month as Calender month is zero base and XMLGregorianCalendar month is 1 base.
			xmlGrogerianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gregorianCalendar.get(Calendar.YEAR),
					gregorianCalendar.get(Calendar.MONTH)+1,
					gregorianCalendar.get(Calendar.DAY_OF_MONTH),
					DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			LOGGER.error("Error converting to XMLGregorianCalendar Date", e);
		}

		return xmlGrogerianCalendar;
	}



	@Override
	public boolean validateGeneratedEnrollmentCMSOutXML(Long jobId) {

		StringBuilder cmsXMLFileNameBuilder =  EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CMS.toString(),
																											 EnrollmentConstants.TRANSFER_DIRECTION_OUT);

		String cmsOutXMLFolderPath = cmsXMLFileNameBuilder.toString();
		String cmsOutXMLWipFolderPath = cmsOutXMLFolderPath + File.separatorChar + EnrollmentConstants.WIP_FOLDER_NAME;
		
		int issuerSuccessCount = 0;
		
		if(StringUtils.isNotEmpty(cmsOutXMLFolderPath) && (new File(cmsOutXMLFolderPath).exists()) && (new File(cmsOutXMLWipFolderPath).exists())){
			
			Map<String,String> fileMapByHiosId = getFileMapByHiosId(jobId);
			LOGGER.info("Validation file map :: "+ fileMapByHiosId);
			
			if(fileMapByHiosId != null && !fileMapByHiosId.isEmpty()){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				String batchDate = sdf.format(new Date()).replace('|', '-').replace(':', '-')+"-"+jobId;
				//Create Directory if not present
				String cmsValidDirectory = cmsOutXMLFolderPath + File.separatorChar + EnrollmentConstants.VALID_FOLDER;
				String cmsInvalidDirectory = cmsOutXMLFolderPath + File.separatorChar + EnrollmentConstants.INVALID_FOLDER + File.separatorChar + batchDate;
				String cmsErrorDirectory = cmsOutXMLFolderPath + File.separatorChar + EnrollmentConstants.XML_VALIDATION_LOG_FOLDER;
				String cmsArchiveDirectory = cmsOutXMLFolderPath + File.separatorChar + EnrollmentConstants.ARCHIVE_FOLDER + File.separatorChar + batchDate;

				if(!new File(cmsValidDirectory).exists()){
					EnrollmentUtils.createDirectory(cmsValidDirectory);
				}

				if(!new File(cmsInvalidDirectory).exists()){
					EnrollmentUtils.createDirectory(cmsInvalidDirectory);
				}

				if(!new File(cmsErrorDirectory).exists()){
					EnrollmentUtils.createDirectory(cmsErrorDirectory);
				}

				if(!new File(cmsArchiveDirectory).exists()){
					EnrollmentUtils.createDirectory(cmsArchiveDirectory);
				}

				try
				{	
					String initialFilePath = null;
					if(EnrollmentConfiguration.isCaCall()){
						String enrollmentBasePath = servletContext.getResource("/WEB-INF/enrollment").getPath();
						if(null != enrollmentBasePath && !enrollmentBasePath.endsWith("/")){
							initialFilePath = enrollmentBasePath + "/" + EnrollmentConstants.CMS_OUT_SCHEMA_BASE_DIRECORY; 
						}else{
							initialFilePath =  enrollmentBasePath + EnrollmentConstants.CMS_OUT_SCHEMA_BASE_DIRECORY; 
						}
					}else{
						ClassLoader classLoader = getClass().getClassLoader();
						initialFilePath =  classLoader.getResource("/enrollment").getPath() + EnrollmentConstants.CMS_OUT_SCHEMA_BASE_DIRECORY; 
					}
					File xsdFile = EnrollmentUtils.searchFile(initialFilePath,EnrollmentConstants.CMS_OUT_SCHEMA_FILENAME, Boolean.TRUE);
					
					if(xsdFile != null){
						String schemaLang = "http://www.w3.org/2001/XMLSchema";
						SchemaFactory factory = SchemaFactory.newInstance(schemaLang);
						//set prefix if your schema is not in the root of classpath
						ResourceResolver resolver = new ResourceResolver(Paths.get(xsdFile.getParent()).getParent().toString() + File.separator);

						factory.setResourceResolver(resolver);
						Schema schema = factory.newSchema(new StreamSource(xsdFile));
						Validator validator = schema.newValidator();
						EnrollmentIrsSaxErrorHandler errorHandler = new EnrollmentIrsSaxErrorHandler();
						List<EnrollmentXMLValidationDetails> enrollmentXMLValidationDetailsList = null;
						File[] listOfXmlFiles = null;
						List<String> fileNameList = null;
						boolean isGeneratedFilesValid = Boolean.FALSE;
						
						for (Map.Entry<String, String> entry : fileMapByHiosId.entrySet()) {
								
							fileNameList = Arrays.asList(entry.getValue().split(","));
							
							EnrollmentXMLValidationDTO enrollmentXMLValidationDTO = new EnrollmentXMLValidationDTO();
							listOfXmlFiles = EnrollmentUtils.getFilesFromDirectory(cmsOutXMLWipFolderPath, fileNameList);
							enrollmentXMLValidationDetailsList = new ArrayList<EnrollmentXMLValidationDetails>();
							int validFileCount = 0, invalidFileCount = 0;
							
							enrollmentXMLValidationDTO.setServerURL(GhixPlatformEndPoints.GHIXWEB_SERVICE_URL);
							enrollmentXMLValidationDTO.setXSDFileName(xsdFile.getName());
							enrollmentXMLValidationDTO.setValidationProcessName("Enrollment_CMS_XML");

							for(File xmlfile : listOfXmlFiles){
								LOGGER.info("CMS File picked up for Validation :: "+ xmlfile.getName() );
								EnrollmentXMLValidationDetails enrollmentXMLValidationDetails = new EnrollmentXMLValidationDetails();
								enrollmentXMLValidationDetails.setXMLFileName(xmlfile.getName());
								try{
									errorHandler.resetHandler();
									validator.setErrorHandler(errorHandler);
									validator.validate(new StreamSource(xmlfile));
									/*errorMessageMap.put(xmlfile.getName(), "VALID");*/
									if(errorHandler.isValid()){
										enrollmentXMLValidationDetails.setValidationStatus(ValidationStatus.VALID);	
										validFileCount++;
									}else{
										LOGGER.error("XML file invalid");
										enrollmentXMLValidationDetails.setValidationStatus(ValidationStatus.INVALID);
										enrollmentXMLValidationDetails.setErrorMessage(errorHandler.getErrorList().toString());
										invalidFileCount++;
									}
								}catch(IOException io){
									LOGGER.error("IO Exception occurred in validateIRSAnnualXML ",io);
									enrollmentXMLValidationDetails.setValidationStatus(ValidationStatus.INVALID);
									enrollmentXMLValidationDetails.setErrorMessage( io.getMessage() != null ? io.getMessage() : EnrollmentUtils.shortenedStackTrace(io, 3));
									invalidFileCount++;
								}
								catch(SAXException sax){
									LOGGER.error("SAXException occurred in validateIRSAnnualXML ",sax);
									enrollmentXMLValidationDetails.setValidationStatus(ValidationStatus.INVALID);
									enrollmentXMLValidationDetails.setErrorMessage(errorHandler.getErrorList().toString());
									invalidFileCount++;
								}

								enrollmentXMLValidationDetailsList.add(enrollmentXMLValidationDetails);
								FileUtils.copyFileToDirectory(xmlfile, new File(cmsArchiveDirectory));
							}
							
							enrollmentXMLValidationDTO.setEnrollmentXMLValidationDetailsList(enrollmentXMLValidationDetailsList);
							enrollmentXMLValidationDTO.setTotalFileCount(listOfXmlFiles.length);
							enrollmentXMLValidationDTO.setTotalValidFileCount(validFileCount);
							enrollmentXMLValidationDTO.setTotalInValidFileCount(invalidFileCount);

							if(listOfXmlFiles.length == validFileCount){
								issuerSuccessCount++;
								isGeneratedFilesValid = Boolean.TRUE;
								enrollmentXMLValidationDTO.setValidationStatus(XMLValidationStatus.PASS);
							}
							else{
								enrollmentXMLValidationDTO.setValidationStatus(XMLValidationStatus.FAILED);
								isGeneratedFilesValid = Boolean.FALSE;
							}

							for (EnrollmentXMLValidationDetails enrollmentXMLValidationDetails : enrollmentXMLValidationDetailsList) {

								if(isGeneratedFilesValid){
									LOGGER.info("CMS File moved to VALID folder :: "+ enrollmentXMLValidationDetails.getXMLFileName() );
									logToEnrollmentCmsOutDtl(new File(cmsOutXMLWipFolderPath + File.separatorChar + enrollmentXMLValidationDetails.getXMLFileName()), jobId);
									EnrollmentUtils.moveFile(cmsOutXMLWipFolderPath, cmsValidDirectory, new File(cmsOutXMLWipFolderPath + File.separatorChar + enrollmentXMLValidationDetails.getXMLFileName()));
								}else{
									LOGGER.info("CMS File moved to INVALID folder :: "+ enrollmentXMLValidationDetails.getXMLFileName() );
									EnrollmentUtils.moveFile(cmsOutXMLWipFolderPath, cmsInvalidDirectory, new File(cmsOutXMLWipFolderPath + File.separatorChar + enrollmentXMLValidationDetails.getXMLFileName()));
									markNullActionOutboundTable(new File(cmsOutXMLWipFolderPath + File.separatorChar + enrollmentXMLValidationDetails.getXMLFileName()), jobId);
								}
							}

							//Gson gson = new GsonBuilder().setPrettyPrinting().create();
							String validationLogs = platformGson.toJson(enrollmentXMLValidationDTO);

							if(!isGeneratedFilesValid){
								logBug("CMS XML Job:: XSD Validation failed for some of the generated XMLs: ",validationLogs);
							}

							//Save the Generated logs
							StringBuilder stringbuilder = new StringBuilder(64);
							stringbuilder.append(cmsErrorDirectory);
							stringbuilder.append(File.separator);
							SimpleDateFormat dateFormat = new SimpleDateFormat(GhixConstants.FILENAME_DATE_FORMAT);
							stringbuilder.append(dateFormat.format(new Date()));
							stringbuilder.append(entry.getKey());
							EnrollmentUtils.createDirectory(stringbuilder.toString());
							stringbuilder.append(File.separator);

							//file Name ANNUAL_1095_XML_VALIDATION_RESULTS.txt
							stringbuilder.append("ENROLLMENT_CMS_XML_VALIDATION_RESULTS");
							stringbuilder.append(".txt");

							try(Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(stringbuilder.toString()), "utf-8"))){
								writer.write(validationLogs);
							}catch(Exception ex){
								throw new GIException(ex);
							}
						}

					}else{
						throw new GIException("Monthly CMS XSD is missing at location: "+initialFilePath);
					}
					
					//Delete "cmsInvalidDirectory" if it is empty.
					if(!Files.list(Paths.get(cmsInvalidDirectory)).findAny().isPresent()){
						Files.delete(Paths.get(cmsInvalidDirectory));
					}
					
					
				}catch(Exception ex){
					LOGGER.error("Exception occurred while validating Monthly CMS XML files: ", ex);
				}
			}
			else{
				//Since no XML present then mark the Boolean flag as TRUE
				//For Runs when no xml is generated
				return true;
			}
		}
		
		if(issuerSuccessCount > 0){
			
			return true;
			
		}else{
			
			return false;
		}
	}

	private Map<String, String> getFileMapByHiosId(Long jobId) {
		List<EnrollmentCmsOut> enrollmentCmsOutList = enrollmentCmsOutRepository.getEnrollmentCmsOutByJobId(jobId);
		Map<String, String> hiosMap = null;
		if (enrollmentCmsOutList != null && !enrollmentCmsOutList.isEmpty()) {
			hiosMap = enrollmentCmsOutList.stream()
					.collect(Collectors.toMap(p -> (p.getHiosIssuerId() + p.getCoverageYear()),
							EnrollmentCmsOut::getFileName, (fileName1, fileName2) -> {
								return fileName1 + "," + fileName2;
							}));
		}
		return hiosMap;
	}

	private void logToEnrollmentCmsOutDtl(File outboundFileName, Long batchExecutionId) {
		if(null != outboundFileName){
			EnrollmentCmsOut enrollmentCmsOut = enrollmentCmsOutRepository.findByNameAndBatchExecutionId(outboundFileName.getName(), batchExecutionId);
			List<EnrollmentCmsOutDtl> outDtlList = new ArrayList<>();
			if(null != enrollmentCmsOut){
				String enrollmentIdStr = enrollmentCmsOut.getOutEnrollmentIds();
				for(String enrollmentId : Arrays.asList(enrollmentIdStr.split(","))){
					EnrollmentCmsOutDtl dtl = new EnrollmentCmsOutDtl();
					dtl.setEnrollmentId(Integer.valueOf(enrollmentId));
					dtl.setEnrlCmsOutId(enrollmentCmsOut.getId());
					dtl.setHiosIssuerId(enrollmentCmsOut.getHiosIssuerId());
					dtl.setCoverageYear(enrollmentCmsOut.getCoverageYear());
					outDtlList.add(dtl);
				}
				try{
					enrollmentCmsOutDtlRepository.save(outDtlList);
				}catch(Exception e){
					LOGGER.error("Unable to save cms out detail table for CMS out ID :: " +enrollmentCmsOut.getId(), e);
				}
			}
		}
	}

	
	private void markNullActionOutboundTable(File outboundFileName, Long batchExecutionId) {
		if(null != outboundFileName){
			EnrollmentCmsOut enrollmentCmsOut = enrollmentCmsOutRepository.findByNameAndBatchExecutionId(outboundFileName.getName(), batchExecutionId);
			enrollmentCmsOut.setInboundAction(null);
			
			if(enrollmentCmsOut.getSkipRecords() != null){
				enrollmentCmsOut.setSkipRecords(" File Validation Failed : Skip_Record : "+ enrollmentCmsOut.getSkipRecords());
			}else{
				enrollmentCmsOut.setSkipRecords("File Validation Failed");
			}
			
			enrollmentCmsOutRepository.saveAndFlush(enrollmentCmsOut);
		}
	}

	@Override
	public List<Integer> findIssuerByYear(String year) {
		return enrollmentRepository.findIssuerByYear(year);
	}

	@Override
	public Integer getIssuerIdByHiosIssuerId(String hiosId) {
		Integer issuerId = null;
		if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)) {
			issuerId = enrollmentRepository.getIssuerIdByHiosIssuerIdPg(hiosId);
		}else if("ORACLE".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
			issuerId = enrollmentRepository.getIssuerIdByHiosIssuerIdOracle(hiosId);
		}
		return issuerId;
	}

	@Override
	public void processCmsInFile(Long jobId, JobService jobService) throws Exception {

		String wipFolderPath = moveAllInFileToWipFolder();

		if(null != wipFolderPath){

			Map<String,SBMS> sbmsMap = readSBMSFile(wipFolderPath);

			Map<String,FileAcceptanceRejection> sbmrMap = readSBMRFile(wipFolderPath);
			
			if(!sbmsMap.isEmpty()){
				
				processAndArchiveSbmsFile(sbmsMap, wipFolderPath, jobId, jobService);
			}

			if(!sbmrMap.isEmpty()){
				
				processAndArchiveSbmrFile(sbmrMap, wipFolderPath, jobId, jobService);
			}
			
		}else{
			LOGGER.error("wipFolderPath is Null");
		}
	}
	
	private void processAndArchiveSbmrFile( Map<String, FileAcceptanceRejection> sbmrMap, String wipFolderPath, Long jobId, JobService jobService) throws Exception{
		// Process
		List<EnrollmentCmsIn> enrollmentCmsInList = sbmrMap.entrySet().stream()
				.map(entry -> getEnrollmentCmsIn(entry.getKey(), entry.getValue()))
				.flatMap(enrCmsInList -> enrCmsInList.stream())
				.collect(Collectors.toList());
		
		//To calculate acceptedCount and acceptedErrorWarnCount
		Map<String,EnrollmentCmsCount> enrollmentCmsCountMap = null;
		for (Map.Entry<String, FileAcceptanceRejection> entry : sbmrMap.entrySet()){
				FileAcceptanceRejection fileAcceptanceRejection= entry.getValue();
				List<com.getinsured.enrollment.cms.dsh.sbmr.FileInformationType> fileInformationList = fileAcceptanceRejection.getSBMIFileInfo();
				enrollmentCmsCountMap = new HashMap<>();
				EnrollmentCmsCount enrollmentCmsCount = null;
				String fileName = null;
				
				for (com.getinsured.enrollment.cms.dsh.sbmr.FileInformationType fileInfo : fileInformationList) {
					enrollmentCmsCount = new EnrollmentCmsCount();
					fileName = enrollmentCmsOutRepository.getOutFileName(fileInfo.getSourceFileId());
					enrollmentCmsCount.setCmsOutFileName(fileName);
					enrollmentCmsCount.setFileProcessingStatus(fileInfo.getFileProcessingStatus());
					
					if(null != fileInfo.getSBMIDetail()){
						enrollmentCmsCount.setErrorWarnCount(fileInfo.getSBMIDetail().size());
					}
					
					enrollmentCmsCountMap.put(fileName, enrollmentCmsCount);
				}
			}
		
		EnrollmentCmsOut enrollmentCmsOut = null;
		String cmsInArchiveDirectory = getCmsInArchiveDirectory(jobId);
		
		for (EnrollmentCmsIn enrollmentCmsIn : enrollmentCmsInList) {
			// SAVE
			enrollmentCmsInRepository.save(enrollmentCmsIn);
			
			//UPDATE
			enrollmentCmsOut = getCmsOut(enrollmentCmsIn, enrollmentCmsCountMap, "SBMR");
			if(null != enrollmentCmsOut){
				enrollmentCmsOutRepository.save(enrollmentCmsOut);
			}

			//ARCHIVE
			File infile = new File(wipFolderPath + File.separatorChar + enrollmentCmsIn.getInFileName());
			infile.renameTo( new File(cmsInArchiveDirectory + File.separatorChar + infile.getName()));
			
			//CHECK FOR BATCH STOP
			checkIfJobIsStop(wipFolderPath, jobId, jobService);
		}	
	}

	private void processAndArchiveSbmsFile(Map<String, SBMS> sbmsMap, String wipFolderPath, Long jobId, JobService jobService) throws Exception {
		// Process
		List<EnrollmentCmsIn> enrollmentCmsInList = sbmsMap.entrySet().stream()
				.map(entry -> getEnrollmentCmsIn(entry.getKey(), entry.getValue()))
				.collect(Collectors.toList());
		
		EnrollmentCmsOut enrollmentCmsOut = null;
		String cmsInArchiveDirectory = getCmsInArchiveDirectory(jobId);
		
		for (EnrollmentCmsIn enrollmentCmsIn : enrollmentCmsInList) {
			
			// SAVE
			enrollmentCmsInRepository.save(enrollmentCmsIn);
			
			//UPDATE
			enrollmentCmsOut = getCmsOut(enrollmentCmsIn, new HashMap<>(), "SBMS");
			if(null != enrollmentCmsOut){
				enrollmentCmsOutRepository.save(enrollmentCmsOut);
			}
			
			//ARCHIVE
			File infile = new File(wipFolderPath + File.separatorChar + enrollmentCmsIn.getInFileName());
			infile.renameTo( new File(cmsInArchiveDirectory + File.separatorChar + infile.getName()));
			
			//CHECK FOR BATCH STOP
			checkIfJobIsStop(wipFolderPath, jobId, jobService);
		}
	}

	private String getCmsInArchiveDirectory(Long jobId) {
		StringBuilder cmsXMLFileNameBuilder =  EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CMS.toString(),
                EnrollmentConstants.TRANSFER_DIRECTION_IN);

		String cmsInArchiveDirectory = cmsXMLFileNameBuilder.toString() + File.separatorChar + EnrollmentConstants.ARCHIVE_FOLDER + File.separatorChar + jobId;
		
		if(!new File(cmsInArchiveDirectory).exists()){
		EnrollmentUtils.createDirectory(cmsInArchiveDirectory);
		}
		
		return cmsInArchiveDirectory;
	}

	private void checkIfJobIsStop(String wipFolderPath, Long jobId, JobService jobService) throws Exception {
		String batchJobStatus=null;
		if(jobService != null && jobId != -1){
			batchJobStatus = jobService.getJobExecution(jobId).getStatus().name();
		}
		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||
				                    batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			
			
			// Move all remaining Files from WIP to CMSInFolder so Next batch execution can process it
			File[] listOfInCMSFile = new File(wipFolderPath).listFiles();
			
			String inboundDir =  EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CMS.toString(),
                    																							 EnrollmentConstants.TRANSFER_DIRECTION_IN)
                    																							 .toString();

			Arrays.stream(listOfInCMSFile).forEach(file-> file.renameTo( new File(inboundDir + File.separatorChar + file.getName())));
			
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}		
	}
	
	private Map<String, SBMS> readSBMSFile(String wipFolderPath) {
		File[] sbmsFiles = EnrollmentUtils.getFilesInAFolderByName(new File(wipFolderPath).getAbsolutePath(), "SBMS");
		Map<String,SBMS> sbmsMap = new HashMap<>();

		for (File file : sbmsFiles) {
			try{
				JAXBContext jaxbContext = JAXBContext.newInstance(com.getinsured.enrollment.cms.dsh.sbms.ObjectFactory.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				SBMS sbms = null;
				sbms = (SBMS) jaxbUnmarshaller.unmarshal(file);
				sbmsMap.put(file.getName(), sbms);
			}catch (JAXBException e) {
				LOGGER.error("JAXBException while Processing Manifest File", e);
				EnrollmentCmsIn enrollmentCmsIn = new EnrollmentCmsIn();
				enrollmentCmsIn.setInFileName(file.getName());
				enrollmentCmsIn.setFileType("SBMS");
				enrollmentCmsIn.setFileStatus("Exception while Parsing");
				enrollmentCmsIn.setInboundFileError(EnrollmentUtils.shortenedStackTrace(e, 5));
				enrollmentCmsInRepository.saveAndFlush(enrollmentCmsIn);
			}
		}		

		return sbmsMap;
	}

	private Map<String, FileAcceptanceRejection> readSBMRFile(String wipFolderPath) {
		File[] sbmrFiles = EnrollmentUtils.getFilesInAFolderByName(new File(wipFolderPath).getAbsolutePath(), "SBMR");
		Map<String,FileAcceptanceRejection> sbmrMap = new HashMap<>();

		for (File file : sbmrFiles) {
			try{
				JAXBContext jaxbContext = JAXBContext.newInstance(com.getinsured.enrollment.cms.dsh.sbmr.ObjectFactory.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				FileAcceptanceRejection sbmr = null;
				sbmr = (FileAcceptanceRejection) jaxbUnmarshaller.unmarshal(file);
				sbmrMap.put(file.getName(), sbmr);
			}catch (JAXBException e) {
				LOGGER.error("JAXBException while Processing Manifest File", e);
				EnrollmentCmsIn enrollmentCmsIn = new EnrollmentCmsIn();
				enrollmentCmsIn.setInFileName(file.getName());
				enrollmentCmsIn.setFileType("SBMR");
				enrollmentCmsIn.setFileStatus("Exception while Parsing");
				enrollmentCmsIn.setInboundFileError(EnrollmentUtils.shortenedStackTrace(e, 5));
				enrollmentCmsInRepository.saveAndFlush(enrollmentCmsIn);
			}
		}		

		return sbmrMap;
	}

	private EnrollmentCmsOut getCmsOut(EnrollmentCmsIn cmrIn, Map<String,EnrollmentCmsCount> enrollmentCmsCountMap, String type){
		EnrollmentCmsOut cmsOut = enrollmentCmsOutRepository.findByFileName(cmrIn.getOutFileName());
		if(null != cmsOut){
			EnrollmentCmsCount enrollmentCmsCount =  enrollmentCmsCountMap.get(cmrIn.getOutFileName());
			if(enrollmentCmsCount != null){
			  if(enrollmentCmsCount.getErrorWarnCount() != null){
				  if(cmsOut.getOutEnrollmentCount() != null){
					  cmsOut.setAcceptedCount(cmsOut.getOutEnrollmentCount() - enrollmentCmsCount.getErrorWarnCount());
				  }
				  
				  cmsOut.setAcceptedErrorWarnCount(enrollmentCmsCount.getErrorWarnCount());
			  }else{
				  if(cmsOut.getOutEnrollmentCount() != null){
					  cmsOut.setAcceptedCount(cmsOut.getOutEnrollmentCount());
				  }
				  
				  cmsOut.setAcceptedErrorWarnCount(0);
			 }
			}
			
			cmsOut.setInboundStatus(cmrIn.getFileStatus());
			cmsOut.setInboundAction(cmrIn.getInboundAction());
			cmsOut.setSbmsSbmrRcvd(type);
		}
		return cmsOut;
	}

	private List<EnrollmentCmsIn> getEnrollmentCmsIn(String fileName, FileAcceptanceRejection sbmr) {
		//SBMRHeaderType SbmrHeaderType = sbmr.getSBMRHeader();
		SBMIPROCSUMType sbmiProcSumType = sbmr.getSBMIPROCSUM();
		List<com.getinsured.enrollment.cms.dsh.sbmr.FileInformationType> fileInformationList = sbmr.getSBMIFileInfo();
		List<MissingPolicyType> missingPolicyList = sbmr.getMissingPolicy();
		
		List <EnrollmentCmsIn> enrollmentCmsInList = fileInformationList.stream().map(fileInfo -> getEnrollmentCmsIn(fileName, fileInfo)).collect(Collectors.toList());

		for(EnrollmentCmsIn enrollmentCmsIn : enrollmentCmsInList){
			if(null != missingPolicyList && !missingPolicyList.isEmpty()){
				List<EnrollmentCmsInMissingPolicy> enrollmentCmsInMissingPolicies = new ArrayList<>();
				String missingPolicyIds = missingPolicyList.stream().map(missingPolicy -> missingPolicy.getExchangeAssignedPolicyId()).collect(Collectors.joining(","));
				//enrollmentCmsInList.forEach(enrlCmsIn -> enrlCmsIn.setMissingPolicyIds(missingPolicyIds));
				enrollmentCmsIn.setMissingPolicyIds(missingPolicyIds);
				for(MissingPolicyType missingPolicyType : missingPolicyList)
				{
					EnrollmentCmsInMissingPolicy missingPolicy = new EnrollmentCmsInMissingPolicy();
					missingPolicy.setCurrentCmsPolicyStatus(missingPolicyType.getCurrentCMSPolicyStatus());
					missingPolicy.setExchangeAssignedPolicyId(missingPolicyType.getExchangeAssignedPolicyId());
					missingPolicy.setQhpId(missingPolicyType.getQHPId());
					missingPolicy.setEnrlCmsIn(enrollmentCmsIn);
					enrollmentCmsInMissingPolicies.add(missingPolicy);
				}
				if(!enrollmentCmsInMissingPolicies.isEmpty())
				{
					enrollmentCmsIn.setEnrollmentMissingPolicies(enrollmentCmsInMissingPolicies);
				}
			}
		}
		
		if(null != sbmiProcSumType){
			enrollmentCmsInList.forEach(enrlCmsIn -> addSbmiProcSumType(enrlCmsIn, sbmiProcSumType));
		}

		return enrollmentCmsInList;
	}
	
	private void addSbmiProcSumType(EnrollmentCmsIn enrlCmsIn, SBMIPROCSUMType sbmiProcSumType) {
		enrlCmsIn.setTotPrevPoliciesNotSubmitted(sbmiProcSumType.getTotalPreviousPoliciesNotSubmitted());
		enrlCmsIn.setNotSubmittedEffectuated(sbmiProcSumType.getNotSubmittedEffectuated());
		enrlCmsIn.setNotSubmittedTerminated(sbmiProcSumType.getNotSubmittedTerminated());
		enrlCmsIn.setNotSubmittedCancelled(sbmiProcSumType.getNotSubmittedCancelled());
		
		if(sbmiProcSumType.getFinalRecordsProcessedSummary() != null){
			SBMIPROCSUMType.FinalRecordsProcessedSummary finalRecordsProcessedSummary = sbmiProcSumType.getFinalRecordsProcessedSummary();
			
			enrlCmsIn.setTotalRecordsProcessed(finalRecordsProcessedSummary.getTotalRecordsProcessed());
			enrlCmsIn.setTotalRecordsRejected(finalRecordsProcessedSummary.getTotalRecordsRejected());
			enrlCmsIn.setCountOfEffectuatedPoliciesCancelled(finalRecordsProcessedSummary.getCountOfEffectuatedPoliciesCancelled());
			
			if(finalRecordsProcessedSummary.getTotalApproved() != null){
				SBMIPROCSUMType.FinalRecordsProcessedSummary.TotalApproved totalApproved = finalRecordsProcessedSummary.getTotalApproved();
				
				enrlCmsIn.setTotalPolicyrecordsApproved(totalApproved.getTotalPolicyRecordsApproved());
				enrlCmsIn.setMatchingPoliciesNoChange_required(totalApproved.getMatchingPoliciesNoChangeRequired());
				enrlCmsIn.setMatchingPoliciesChangeApplied(totalApproved.getMatchingPoliciesChangeApplied());
				enrlCmsIn.setMatchingPoliciesCorrectedChangeApplied(totalApproved.getMatchingPoliciesCorrectedChangeApplied());
				enrlCmsIn.setNewPoliciesCreatedAsSent(totalApproved.getNewPoliciesCreatedAsSent());
				enrlCmsIn.setNewPoliciesCreatedWithCorrectionApplied(totalApproved.getNewPoliciesCreatedWithCorrectionApplied());
			}
		}
	}

	private EnrollmentCmsIn getEnrollmentCmsIn(String fileName, com.getinsured.enrollment.cms.dsh.sbmr.FileInformationType fileInfo) {
		EnrollmentCmsIn enrollmentCmsIn = new EnrollmentCmsIn();
		List<EnrollmentCmsInDtl> enrollmentCmsInDtlList = new ArrayList<>();

		enrollmentCmsIn.setInFileName(fileName);
		if(null != fileInfo.getSourceFileId()){
			enrollmentCmsIn.setOutFileName(enrollmentCmsOutRepository.getOutFileName(fileInfo.getSourceFileId()));
			enrollmentCmsIn.setFileId(fileInfo.getSourceFileId());
			enrollmentCmsIn.setHiosIssuerId(enrollmentCmsOutRepository.getHiosIssuerId(fileInfo.getSourceFileId()));
		}
		enrollmentCmsIn.setFileType(getFileType(fileInfo.getFileProcessingStatus()));
		enrollmentCmsIn.setFileStatus(fileInfo.getFileProcessingStatus());
		enrollmentCmsIn.setInboundAction(getInboundAction(fileInfo.getFileProcessingStatus()));

		if(null != fileInfo.getFileError()){
			EnrollmentCmsInDtl enrollmentCmsInDtl = new EnrollmentCmsInDtl();
			enrollmentCmsInDtl.setErrorCode(fileInfo.getFileError().getErrorCode());
			enrollmentCmsInDtl.setAdditionalErrorInfo( (fileInfo.getFileError().getAdditionalErrorInfo() != null) ? fileInfo.getFileError().getAdditionalErrorInfo().stream().collect(Collectors.joining(",")) : null);
			enrollmentCmsInDtl.setElementInError(fileInfo.getFileError().getElementInError());
			enrollmentCmsInDtl.setErrorDesc(fileInfo.getFileError().getErrorDescription());
			enrollmentCmsInDtl.setEnrollmentCmsIn(enrollmentCmsIn);
			
			enrollmentCmsInDtlList.add(enrollmentCmsInDtl);
		}
		
		if(null != fileInfo.getSBMIDetail()){
			
			for (PolicyErrorType policyErrorType : fileInfo.getSBMIDetail()) {

				if(null != policyErrorType.getError()){
					
					for (PolicyErrorType.Error error : policyErrorType.getError()) {
						
						EnrollmentCmsInDtl enrollmentCmsInDtl = new EnrollmentCmsInDtl();
						if(NumberUtils.isNumber(policyErrorType.getExchangeAssignedPolicyId()) && NumberUtils.isNumber(policyErrorType.getExchangeAssignedSubscriberId())) {
							enrollmentCmsInDtl.setExchAssignPolicyId(Integer.valueOf(policyErrorType.getExchangeAssignedPolicyId()));
							enrollmentCmsInDtl.setIssuerAssignSubsriberId(Integer.valueOf(policyErrorType.getExchangeAssignedSubscriberId()));
						}else {
							LOGGER.info("Skipping, invalid data for ExchAssignPolicyId or IssuerAssignSubsriberId");
							continue;
						}
						enrollmentCmsInDtl.setErrorCode(error.getErrorCode());
						enrollmentCmsInDtl.setAdditionalErrorInfo( (error.getAdditionalErrorInfo() != null) ? error.getAdditionalErrorInfo().stream().collect(Collectors.joining(",")) : null);
						enrollmentCmsInDtl.setElementInError(error.getElementInError());
						enrollmentCmsInDtl.setErrorDesc(error.getErrorDescription());
						
						enrollmentCmsInDtl.setEnrollmentCmsIn(enrollmentCmsIn);
						
						enrollmentCmsInDtlList.add(enrollmentCmsInDtl);
					}
				}
			}
		}
		
		enrollmentCmsIn.setEnrollmentCmsInDtlList(enrollmentCmsInDtlList);

		return enrollmentCmsIn;
	}
	
	private String getInboundAction(String status) {
		switch (status) {
			case "Rejected": return EnrollmentCmsOut.actionEnum.REGEN.toString();
			
			case "On Hold": return EnrollmentCmsOut.actionEnum.HOLD.toString();
			
			case "Pending Files": return EnrollmentCmsOut.actionEnum.REGEN.toString();
			
			case "Freeze": return EnrollmentCmsOut.actionEnum.HOLD.toString();
			
			case "Accepted": return EnrollmentCmsOut.actionEnum.SUCCESS.toString();
			
			case "Accepted with errors": return EnrollmentCmsOut.actionEnum.SUCCESS.toString();
			
			case "Accepted with warnings": return EnrollmentCmsOut.actionEnum.SUCCESS.toString();
			
			case "Approved": return EnrollmentCmsOut.actionEnum.SUCCESS.toString();
			
			case "Approved with errors": return EnrollmentCmsOut.actionEnum.SUCCESS.toString();
			
			case "Approved with warnings": return EnrollmentCmsOut.actionEnum.SUCCESS.toString();
			
			case "Disapproved": return EnrollmentCmsOut.actionEnum.REGEN.toString();
			
			default: return null;
			}
	}

	private String getFileType(String fileProcessingStatus) {
		/* Check for File Type 
		 * SBMR (Interim) FileProcessingStatus : Rejected , Accepted , Accepted with errors , Accepted with warnings
		 * SBMR (Final)   FileProcessingStatus : Approved , Approved with errors , Approved with warnings , Disapproved
		 * */

		List<String> interimFileProcessingStatus = Arrays.asList("Rejected" , "Accepted" , "Accepted with errors" , "Accepted with warnings");
		List<String> finalFileProcessingStatus = Arrays.asList("Approved" , "Approved with errors" , "Approved with warnings" , "Disapproved");

		if(interimFileProcessingStatus.contains(fileProcessingStatus)){
			return "SBMRI";
		}else if(finalFileProcessingStatus.contains(fileProcessingStatus)){
			return "SBMRF";
		}
		return null;
	}

	private EnrollmentCmsIn getEnrollmentCmsIn(String fileName, SBMS sbms) {
		EnrollmentCmsIn enrollmentCmsIn = new EnrollmentCmsIn();

		enrollmentCmsIn.setInFileName(fileName);
		enrollmentCmsIn.setFileType("SBMS");
		enrollmentCmsIn.setOutFileName(sbms.getFileName());
		enrollmentCmsIn.setFileStatus(sbms.getStatus());
		enrollmentCmsIn.setInboundAction(getInboundAction(sbms.getStatus()));
		enrollmentCmsIn.setEnrollmentCmsInDtlList(sbms.getError().stream().map(error -> getErrorDtl(error, enrollmentCmsIn)).collect(Collectors.toList()));

		return enrollmentCmsIn;
	}

	private EnrollmentCmsInDtl getErrorDtl(ErrorType error, EnrollmentCmsIn enrollmentCmsIn) {
		EnrollmentCmsInDtl enrollmentCmsInDtl = new EnrollmentCmsInDtl();
		enrollmentCmsInDtl.setErrorCode(error.getErrorCode());
		enrollmentCmsInDtl.setElementInError(error.getElementInError());
		enrollmentCmsInDtl.setErrorDesc(error.getErrorDescription());
		enrollmentCmsInDtl.setAdditionalErrorInfo(error.getAdditionalErrorInfo().stream().collect(Collectors.joining(",")));
		enrollmentCmsInDtl.setEnrollmentCmsIn(enrollmentCmsIn);

		return enrollmentCmsInDtl;
	}

	private String moveAllInFileToWipFolder() {
		StringBuilder cmsXMLFileNameBuilder =  EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CMS.toString(),
						                                                                                     EnrollmentConstants.TRANSFER_DIRECTION_IN);
		String inboundDir = cmsXMLFileNameBuilder.toString();
		File dir = new File(inboundDir);

		//Move all files from inboundCMSXMLPath to WIP folder

		String wipFolderPath = inboundDir + File.separator + EnrollmentConstants.WIP_FOLDER_NAME;
		File wipFolder = new File(wipFolderPath);
		boolean wipPathValid = true;


		if (dir != null && !(wipFolder.exists())) {
			if(!wipFolder.mkdirs()){
				wipPathValid = false;
			}
		}

		if(wipPathValid){
			//Move files from inboundDir to WIP folder
			if (dir.isDirectory()) {
				File[] files = dir.listFiles();
				for (File file : files) {
					if(file.isFile() && file.getName().endsWith(EnrollmentConstants.FILE_TYPE_OUT) && !file.getName().contains("SI820")){
						File wipFile = new File(wipFolderPath + File.separator+ file.getName());
						file.renameTo(wipFile);
					}
				}
			}
			
			if(wipFolder.list()!= null && wipFolder.list().length > 0){
				return wipFolderPath;
			}else{
				return null;
			}

			
		}

		return null;
	}

	@Override
	public List<Integer> findIssuersForCorrection(Integer year, Integer month) {
		List<Integer> issuerCorrectionList = new ArrayList<Integer>();
		List<String> hiosIssuersCmsTable = enrollmentCmsOutRepository.findDistinctHiosIssuerIdsByYearAndMonth(year, month);
		List<String> totalHiosIssuerRegenList = enrollmentCmsOutRepository.findHiosIssuerIdsWithRegen(year, month);

		if(null != hiosIssuersCmsTable && !hiosIssuersCmsTable.isEmpty() && null != totalHiosIssuerRegenList && !totalHiosIssuerRegenList.isEmpty()){
			List<Integer> issuersEnrollmentTable = findIssuerByYear(String.valueOf(year));
			List<Integer> issuersCmsTable = enrollmentRepository.getIssuerIdByHiosIssuerIdList(hiosIssuersCmsTable);
			List<Integer> totalIssuerRegenList = enrollmentRepository.getIssuerIdByHiosIssuerIdList(totalHiosIssuerRegenList);

			for (Integer issuerId : totalIssuerRegenList) {
				String inboundAction = enrollmentCmsOutRepository.getMaxRecordForHiosIssuerIdByYearAndMonth(getHiosIssuerIdById(issuerId), year, month);
				if (!"SENT".equalsIgnoreCase(inboundAction) && !"SUCCESS".equalsIgnoreCase(inboundAction) && !"HOLD".equalsIgnoreCase(inboundAction)) {
					issuerCorrectionList.add(issuerId);
				}
			}
			if(null != issuersEnrollmentTable && null != issuersCmsTable){
				issuersEnrollmentTable.removeAll(issuersCmsTable);
				issuerCorrectionList.addAll(issuersEnrollmentTable);
			}
		}
		return issuerCorrectionList;
	}

	@Override
	public void logInAactiveIssuer(Integer issuerId, Integer year, Integer month, Integer batchYear, Long batchExecutionId) {
		EnrollmentCmsOut enrollmentCmsOut = new EnrollmentCmsOut();
		enrollmentCmsOut.setYear(batchYear);
		enrollmentCmsOut.setCoverageYear(year);
		enrollmentCmsOut.setMonth(month);
		enrollmentCmsOut.setHiosIssuerId(getHiosIssuerIdById(issuerId));
		enrollmentCmsOut.setBatchExecutionId(batchExecutionId);
		enrollmentCmsOut.setInboundAction("INACTIVE");
		enrollmentCmsOutRepository.saveAndFlush(enrollmentCmsOut);
	}
	
	@Override
	public void deleteFileFromWip() {
		StringBuilder cmsXMLFileNameBuilder =  EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CMS.toString(),
				 																							 EnrollmentConstants.TRANSFER_DIRECTION_OUT);

		String cmsOutXMLFolderPath = cmsXMLFileNameBuilder.toString();
		String cmsOutXMLWipFolderPath = cmsOutXMLFolderPath + File.separatorChar + EnrollmentConstants.WIP_FOLDER_NAME;
		
		if(StringUtils.isNotEmpty(cmsOutXMLFolderPath) && (new File(cmsOutXMLFolderPath).exists()) && (new File(cmsOutXMLWipFolderPath).exists())){
			
			EnrollmentUtils.deleteDirectory(new File(cmsOutXMLWipFolderPath));
		}
		
		
	}
	
	private void logBug(String subject, String bugDescription){
		Boolean isJiraEnabled = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENABLE_JIRA_CREATION));
		String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
		String fixVersion = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_FIX_VERSION);
		try{
			if(isJiraEnabled){
				JiraUtil.logBug(Arrays.asList(enrollmentComponent), 
						Arrays.asList(fixVersion), bugDescription, 
						subject,
						null);
			}
		}
		catch(Exception exc){
			LOGGER.error("Unable to log jira for xml validation failure: ", exc);
		}
		 enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.CMS,
				 subject, bugDescription);
	}
	
	/**
	 * Generate financial information loops based on rating area changes
	 * @param enrollmentPremium
	 * @param enrollment
	 * @param startDate
	 * @param ratingAreaMap
	 * @param isLast
	 * @return List<FinancialInformation>
	 */
	private List<FinancialInformation> getFinancialInformation(EnrollmentPremium enrollmentPremium,
			Enrollment enrollment, Date startDate, Map<String, String> ratingAreaMap, boolean isLast) {
		List<FinancialInformation> financialInformationList = new ArrayList<>();
		Date periodStartDate = startDate;
		Date endDate = null;
		if(isLast){
			endDate = enrollment.getBenefitEndDate();
		}else{
			Calendar monthLastDay = new GregorianCalendar(enrollmentPremium.getYear(),enrollmentPremium.getMonth()-1, 1);
			monthLastDay.set(Calendar.DAY_OF_MONTH, monthLastDay.getActualMaximum(Calendar.DAY_OF_MONTH));
			endDate = monthLastDay.getTime();
		}
		final Date periodEndDate = endDate;
		
		List<Date> ratingAreaEffDateList = new ArrayList<>();
		String lastRatingAreaEffDate = "-1";
		List<String>  ratingAreaList = new ArrayList<>(ratingAreaMap.keySet());
		Collections.sort(ratingAreaList);
		Collections.reverse(ratingAreaList);
		for(String date : ratingAreaList) {
			Date ratingAreaEffDate = DateUtil.StringToDate(date, GhixConstants.REQUIRED_DATE_FORMAT);
			if (EnrollmentUtils.isDateInBetweenStartDateAndEndDate(periodStartDate, periodEndDate, ratingAreaEffDate)
					&& !ratingAreaMap.get(date).equalsIgnoreCase(ratingAreaMap.get(lastRatingAreaEffDate))) {
				ratingAreaEffDateList.add(ratingAreaEffDate);
			}
			if(ratingAreaEffDate.before(periodEndDate)) {
				lastRatingAreaEffDate = date;
			}
		}
		if(ratingAreaEffDateList.isEmpty()) {
			ratingAreaEffDateList.add(DateUtil.StringToDate(lastRatingAreaEffDate, GhixConstants.REQUIRED_DATE_FORMAT));
		}else if(!ratingAreaEffDateList.contains(startDate)) {
			for(String date : ratingAreaList) {
				Date ratingAreaEffDate = DateUtil.StringToDate(date, GhixConstants.REQUIRED_DATE_FORMAT);
				if(ratingAreaEffDate.before(startDate)) {
					ratingAreaEffDateList.add(ratingAreaEffDate);
					break;
				}
			}
		}
		Collections.sort(ratingAreaEffDateList);
		BigDecimal grossPremiumAmount = null;
		BigDecimal netPremiumAmount = null;
		BigDecimal aptcAmount = null;
		String csrVariantId = null;
		BigDecimal csrAmount = null;
		BigDecimal stateSubsidyAmount = null;
		String enableStateSubsidy = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_SUBSIDY);

		if (null != enrollmentPremium.getGrossPremiumAmount()) {
			grossPremiumAmount = BigDecimal.valueOf(enrollmentPremium.getGrossPremiumAmount()).setScale(2, BigDecimal.ROUND_HALF_UP);
		}
		if (null != enrollmentPremium.getNetPremiumAmount()) {
			netPremiumAmount = BigDecimal.valueOf(enrollmentPremium.getNetPremiumAmount()).setScale(2, BigDecimal.ROUND_HALF_UP);
		}
		if (null != enrollmentPremium.getAptcAmount()) {
			
			aptcAmount = enrollmentPremium.getAptcAmount() < 0 ? 
					                     new BigDecimal(0) : 
				                         BigDecimal.valueOf(enrollmentPremium.getAptcAmount()).setScale(2, BigDecimal.ROUND_HALF_UP);
			
		}
		if (null != enrollment.getCMSPlanID()) {
			csrVariantId = enrollment.getCMSPlanID().substring(14);
		}
		if(enrollmentPremium.getGrossPremiumAmount() != null & enrollment.getCsrMultiplier() != null){
			if(csrVariantId != null && !csrVariantId.equalsIgnoreCase("01")){
				csrAmount = BigDecimal.valueOf(enrollmentPremium.getGrossPremiumAmount() * enrollment.getCsrMultiplier())
						.setScale(2, BigDecimal.ROUND_HALF_UP);
			}
		}
		if ("Y".equalsIgnoreCase(enableStateSubsidy) && enrollmentPremium.getStateSubsidyAmount() != null) {
			stateSubsidyAmount = enrollmentPremium.getStateSubsidyAmount().compareTo(BigDecimal.ZERO) < 0
					? new BigDecimal(0)
					: enrollmentPremium.getStateSubsidyAmount().setScale(2, BigDecimal.ROUND_HALF_UP);
		}

		for(int i=0; i < ratingAreaEffDateList.size() ; i++) {
			FinancialInformation financialInformation = new FinancialInformation();
			financialInformation.setFinancialEffectiveEndDate(toXMLGregorianCalendarDateOnly(periodEndDate));
			if(ratingAreaEffDateList.size() > 0 && i < ratingAreaEffDateList.size() - 1 && ratingAreaEffDateList.get(i+1).before(periodEndDate)) {
				financialInformation.setFinancialEffectiveEndDate(toXMLGregorianCalendarDateOnly(EnrollmentUtils.getBeforeDayDate(ratingAreaEffDateList.get(i+1))));
			}
			if(ratingAreaEffDateList.size() == 1 && ratingAreaEffDateList.get(i).after(startDate) ) {
				financialInformation.setFinancialEffectiveStartDate(toXMLGregorianCalendarDateOnly(startDate));
			}
			financialInformation.setFinancialEffectiveStartDate(toXMLGregorianCalendarDateOnly(ratingAreaEffDateList.get(i)));
			if(ratingAreaEffDateList.get(i).before(startDate)) {
				financialInformation.setFinancialEffectiveStartDate(toXMLGregorianCalendarDateOnly(startDate));
			}
			financialInformation.setMonthlyTotalPremiumAmount(grossPremiumAmount);
			financialInformation.setMonthlyTotalIndividualResponsibilityAmount(netPremiumAmount);
			financialInformation.setMonthlyAPTCAmount(aptcAmount);
			financialInformation.setMonthlyOtherPaymentAmount1(stateSubsidyAmount);
			financialInformation.setCSRVariantId(csrVariantId);
			financialInformation.setMonthlyCSRAmount(csrAmount);
			financialInformation.setRatingArea(ratingAreaMap.get(DateUtil.dateToString(ratingAreaEffDateList.get(i), GhixConstants.REQUIRED_DATE_FORMAT)));
			financialInformationList.add(financialInformation);
		}
		return financialInformationList;
	}

	/**
	 * Get rating area changes from subscriber audit
	 * @param enrollment
	 * @return
	 */
	private Map<String, String> getRatingAreaInfoFromSubscriber(Enrollment enrollment) {
		Map<String, String> ratingAreaMap = new LinkedHashMap<>();
		List<Object[]> results = enrolleeAudRepository.getRatingAreaWithEffectiveDates(enrollment.getId(), enrollment.getBenefitEffectiveDate(), enrollment.getBenefitEndDate());
		List<Object[]> removeList = new ArrayList<>();
		
		int size = results.size();
		for(int i=0; i<size ;i++) {
			Date currentDate = (Date) results.get(i)[0];
			if(removeList.contains(results.get(i))) {
				continue;
			}
			for(int j=i+1;j< size;j++) {
				if(currentDate.before((Date)results.get(j)[0]) || DateUtil.equateDate(currentDate, (Date)results.get(j)[0])) {
					removeList.add(results.get(j));
				}
			}
		}
		removeList.forEach(e ->results.remove(e));
		if(null != results && !results.isEmpty()) {
			for(Object[] obj : results) {
				ratingAreaMap.put(DateUtil.dateToString((Date)obj[0], GhixConstants.REQUIRED_DATE_FORMAT), EnrollmentUtils.getFormatedRatingArea((String)obj[1]));
			}
		}
		if (ratingAreaMap.isEmpty()) {
			List<String> missingRating = enrolleeAudRepository.getFirstRatingAreaForEnrollment(enrollment.getId(),
					enrollment.getBenefitEffectiveDate());
			if (null != missingRating && !missingRating.isEmpty()) {
				ratingAreaMap.put(
						DateUtil.dateToString(enrollment.getBenefitEffectiveDate(), GhixConstants.REQUIRED_DATE_FORMAT),
						EnrollmentUtils.getFormatedRatingArea(missingRating.get(0)));
			}
		} else if (!ratingAreaMap.containsKey(DateUtil.dateToString(enrollment.getBenefitEffectiveDate(), GhixConstants.REQUIRED_DATE_FORMAT))) {
			ratingAreaMap.put(
					DateUtil.dateToString(enrollment.getBenefitEffectiveDate(), GhixConstants.REQUIRED_DATE_FORMAT),
					ratingAreaMap.remove(Collections.min(ratingAreaMap.keySet())));
		}
		return ratingAreaMap;
	}
	
	private String getHiosIssuerIdById(Integer issuerId) {
		if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)) {
			return enrollmentRepository.getHiosIssuerIdByIssuerIdPg(issuerId);
		}else if("ORACLE".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
			return enrollmentRepository.getHiosIssuerIdByIssuerIdOracle(issuerId);
		}
		return "";
	}
	
	/**
	 * Filters enrollments based on validation deck queries and log errors in the skip table
	 * @param enrollmentIdList
	 * @param enrollmentSkipMap
	 * @return
	 */
	private List<Integer> filteredListViaValidationDeck(final List<Integer> enrollmentIdList, Map<String, String> enrollmentSkipMap) {
		//Divide list into 1000 chunks
		List<Integer> filteredList = new ArrayList<>();
		int size = enrollmentIdList.size();
		int numberOfPartitions = size / 1000;
		if (size % 1000 != 0) {
			numberOfPartitions++;
		}
		int firstIndex = 0;
		int lastIndex = 0;
		for (int i = 0; i < numberOfPartitions; i++) {
			firstIndex = i * 1000;
			lastIndex = (i + 1) * 1000;
			if (lastIndex > size) {
				lastIndex = size;
			}
			List<Integer> chunkFilteredList = new ArrayList<>(enrollmentIdList.subList(firstIndex, lastIndex));
			if(!chunkFilteredList.isEmpty()) {
				for (EnrollmentCmsValidationDeckEnum validationQuery : EnrollmentCmsValidationDeckEnum.values()) {
					filterListByValidationQuery(chunkFilteredList, validationQuery, enrollmentSkipMap);
				}
				filteredList.addAll(chunkFilteredList);
			}
		}
		return filteredList;
	}

	/**
	 * Filter enrollment list based on the validation query passed
	 * @param chunkFilteredList
	 * @param validationQuery
	 * @param enrollmentSkipMap
	 */
	private void filterListByValidationQuery(List<Integer> chunkFilteredList, EnrollmentCmsValidationDeckEnum validationQuery, Map<String, String> enrollmentSkipMap) {
		List<Integer> records = new ArrayList<>();
		if(null !=chunkFilteredList && !chunkFilteredList.isEmpty()) {
			switch(validationQuery) {
			case COVERAGE_AND_PREMIUM_MONTH_MISMATCH :
				records = enrollmentCmsValidationRepository.getRecordsWithCoveragePremiumMismatch(chunkFilteredList);
				break;
			case CONFIRMATION_DATE_MISSING :
				if(!Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.CMS_INCLUDE_PENDING_ENROLLMENTS))){
					records = enrollmentCmsValidationRepository.getRecordsForConfirmationDateMissing(chunkFilteredList);					
				}
				break;
			case COVERAGE_YEAR_MISMATCH :
				records = enrollmentCmsValidationRepository.getRecordsForCoverageYearMismatch(chunkFilteredList);
				break;
			case COVERAGE_START_GREATER_THAN_END_DATE :
				records = enrollmentCmsValidationRepository.getRecordsForCoverageStartGreaterThanEndDate(chunkFilteredList);
				break;
			case MEMBER_COVERAGE_YEAR_MISMATCH :
				records = enrollmentCmsValidationRepository.getRecordsForMemberCoverageYearMismatch(chunkFilteredList);
				break;
			case MEMBER_COVERAGE_START_GREATER_THAN_END_DATE :
				records = enrollmentCmsValidationRepository.getRecordsForMemberCoverageGreaterThanEndDate(chunkFilteredList);
				break;
			case MEMBER_COVERAGE_START_BEFORE_BIRTH_DATE :
				records = enrollmentCmsValidationRepository.getRecordsForMemberCoverageBeforeBirthDate(chunkFilteredList);
				break;
			case NEGATIVE_APTC :
				records = enrollmentCmsValidationRepository.getRecordsForNegativeAptc(chunkFilteredList);
				break;
			case NEGATIVE_NET_PREMIUM :
				records = enrollmentCmsValidationRepository.getRecordsForNegativePremium(chunkFilteredList);
				break;
			case NO_ACTIVE_MEMBERS :
				records = enrollmentCmsValidationRepository.getRecordsForNoActiveMembers(chunkFilteredList);
				break;
			}
			addRecordsToSkipMap(records, chunkFilteredList, enrollmentSkipMap, validationQuery);
		}
	}

	private void addRecordsToSkipMap(List<Integer> records, List<Integer> chunkFilteredList, Map<String, String> enrollmentSkipMap, EnrollmentCmsValidationDeckEnum validation) {
		for(Integer enrollmentId : records) {
			enrollmentSkipMap.put(String.valueOf(enrollmentId), validation.getValue()+ERROR_DELIMITER+validation.name());
		}
		chunkFilteredList.removeAll(records);
	}
	
	/**
	 * 
	 * @param issuerId
	 * @param batchExecutionId
	 * @param partitionId
	 * @return
	 */
	private String generateIssuerFileIdSet(final Integer issuerId, final Long batchExecutionId, final Integer partitionId) {
		StringBuilder issuerFileSetIDString = new StringBuilder(25);
		issuerFileSetIDString.append(getHiosIssuerIdById(issuerId));
		issuerFileSetIDString.append(String.format("%05d", batchExecutionId + partitionId));
		return issuerFileSetIDString.length() > 10 ? issuerFileSetIDString.substring(0, 10) : issuerFileSetIDString.toString();
	}
	
	/**
	 * Add enrollee details to the policy member type map
	 * @param enrollee
	 * @param memberMap
	 */
	private void addToMemberMap(Enrollee enrollee, Map<String, PolicyMemberType> memberMap) {
		if(null != enrollee.getExchgIndivIdentifier() && memberMap.containsKey(enrollee.getExchgIndivIdentifier())){
			MemberDates memberDate = new MemberDates();
			List <MemberDates> memberDateList = memberMap.get(enrollee.getExchgIndivIdentifier()).getMemberDates();
			memberDate.setMemberStartDate(toXMLGregorianCalendarDateOnly(enrollee.getEffectiveStartDate()));
			memberDate.setMemberEndDate(toXMLGregorianCalendarDateOnly(enrollee.getEffectiveEndDate()));
			memberDateList.add(memberDate);
			memberDateList.sort((d1, d2) -> d1.getMemberStartDate().compare(d2.getMemberStartDate()));
		}else{
			memberMap.put(enrollee.getExchgIndivIdentifier(), getPolicyMemberType(enrollee));
		}
	}

	/**
	 * Check for enrollee overlap
	 * @param enrollee
	 * @param enrolleeGroupedByMemberId
	 * @return
	 */
	private boolean isEnrolleeOverlap(Enrollee enrollee, Map<String, List<Enrollee>> enrolleeGroupedByMemberId) {
		if(enrolleeGroupedByMemberId.containsKey(enrollee.getExchgIndivIdentifier())) {
			List<Enrollee> enrolleeList = enrolleeGroupedByMemberId.get(enrollee.getExchgIndivIdentifier());
			for(Enrollee ee : enrolleeList) {
				if(EnrollmentUtils.isDateInBetweenStartDateAndEndDate(ee.getEffectiveStartDate(), ee.getEffectiveEndDate(), enrollee.getEffectiveStartDate())) {
					return true;
				}
				if(EnrollmentUtils.isDateInBetweenStartDateAndEndDate(ee.getEffectiveStartDate(), ee.getEffectiveEndDate(), enrollee.getEffectiveEndDate())) {
					return true;
				}
			}
		}else {
			enrolleeGroupedByMemberId.put(enrollee.getExchgIndivIdentifier(), new ArrayList<Enrollee>(Arrays.asList(enrollee)));
		}
		return false;
	}
}
