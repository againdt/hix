<script type="text/ng-template" id="income-farming.jsp">
  <div class="gutter10 jobIncome" ng-controller="incomeFarming">
    <h3><spring:message code="label.lce.farmfishingincome"/></h3>
    <form class="form-horizontal" id="farmingFishingIncome" name="farmingFishingIncome" novalidate>
      <spring:message code="label.lce.howmuchdoes"/> {{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}} <spring:message code="label.lce.getfromfarming"/>
      <div class="control-group">
        <label class="control-label" for="typeOfRequest"></label>
        <div class="controls margin10-t">
          <div class="input-prepend">
            <span class="add-on">$</span>
            <input type="text" id="farmFishingNetIncomeAmount" class="span4 margin10-r" name="farmFishingNetIncomeAmount" ng-model="householdMember.detailedIncome.farmFishingIncome.netIncomeAmount" required numbers-Only ng-trim>
            <select class="span5" id="farmFishingIncome.netIncomeFrequency" name="farmFishingIncomeFrequency" ng-model="householdMember.detailedIncome.farmFishingIncome.incomeFrequency" required>
              <option><spring:message code="label.lce.weekly"/>Weekly</option>
			        <option><spring:message code="label.lce.biweekly"/>Bi-Weekly</option>
			        <option><spring:message code="label.lce.monthly"/>Monthly</option>
			        <option><spring:message code="label.lce.bimonthly"/>Bi-Monthly</option>
			        <option><spring:message code="label.lce.yearly"/>Yearly</option>
			        <option><spring:message code="label.lce.onceonly"/>Once Only</option>
            </select>
          </div>
        </div>
         <div class="controls margin5-t">
           <span class="validation-error-text-container" ng-show="submitted && farmingFishingIncome.farmFishingNetIncomeAmount.$error.required"><spring:message code="label.lce.incomeframing.error1"/></span>
         </div>
         <div class="controls margin5-t">
           <span class="validation-error-text-container" ng-show="submitted && farmingFishingIncome.farmFishingIncomeFrequency.$error.required"><spring:message code="label.lce.incomeframing.error2"/></span>
         </div>
      </div>
    </form>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a ng-show="farmingFishingIncome.$valid" class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
      <a ng-show="!farmingFishingIncome.$valid" class="btn btn-primary" ng-click="submitted=true"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>