package com.getinsured.hix.admin.web;

import static com.getinsured.hix.platform.util.GhixConstants.BENEFITS_FILENAME_SUFFIX;
import static com.getinsured.hix.platform.util.GhixConstants.BUSINESS_RULE_FILENAME_SUFFIX;
import static com.getinsured.hix.platform.util.GhixConstants.FTP_UPLOAD_MAX_SIZE;
import static com.getinsured.hix.platform.util.GhixConstants.NETWORK_ID_FILENAME_SUFFIX;
import static com.getinsured.hix.platform.util.GhixConstants.PRESCRIPTION_DRUG_FILENAME_SUFFIX;
import static com.getinsured.hix.platform.util.GhixConstants.RATING_TABLE_FILENAME_SUFFIX;
import static com.getinsured.hix.platform.util.GhixConstants.SERVICE_AREA_FILENAME_SUFFIX;
import static com.getinsured.hix.platform.util.GhixConstants.UNIFIED_RATE_FILENAME_SUFFIX;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferAttachmentInfoDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferInfoDTO;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.serff.service.IssuerDropDownDTO;
import com.getinsured.hix.serff.service.PlanLoadStatusDTO;
import com.getinsured.hix.serff.service.PlanLoadStatusDTO.STATUS;
import com.getinsured.hix.serff.service.SerffPlanTemplatesDTO;
import com.getinsured.hix.serff.service.SerffService;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.google.gson.Gson;

import reactor.util.CollectionUtils;

/**
 * Controller class for PHIX Plan Data Upload.
 * 
 * @author Bhavin Parmar
 * @since 23 August, 2013
 */
@Controller
public class PlanDataUploadController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanDataUploadController.class);
	private static final String RETURN_LOAD_PLANS_PAGE = "admin/serff/loadPlans";
	private static final String RETURN_LOAD_DRUGS_PAGE = "admin/serff/loadDrugs";

	private static final int MANDATORY_FILE_LIST_COUNT = 4;
	private static final int MANDATORY_PND_FILE_LIST_COUNT = 2;
	private static final int OPTIONAL_FILE_LIST_COUNT = 7;
	private static final String CARRIER = "carrier";
	private static final String CARRIER_TEXT = "carrierText";
	private static final String SELECT_YEAR = "selectYear";
	private static final String UPLOAD_STATUS = "uploadStatus";
	private static final String UPLOAD_SUCESS = "uploadSucess";
	private static final String ISSUER_LIST = "issuerList";
	private static final String INTERNAL_APP_ERROR = "Internal application error!";

	@Autowired private SerffService serffService;
	@Autowired private ContentManagementService ecmService;
	@Autowired private Gson platformGson;

	/**
	 * Method is used to required data for upload plan details. 
	 */
	@RequestMapping(value = "/admin/serff/loadPlans", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	public String loadUploadPlanDetails(Model model) {

		LOGGER.debug("loadUploadPlanDetails Begin");

		try {
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			setFields(model, stateCode);

			if (PlanMgmtConstants.STATE_CODE_WA.equalsIgnoreCase(stateCode)
					|| PlanMgmtConstants.STATE_CODE_CT.equalsIgnoreCase(stateCode)) {
				model.addAttribute("hasWaitingPlansToLoad", serffService.checkWaitingPlansToLoad());
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception: " + e.getMessage(), e);
		}
		finally {
			LOGGER.debug("loadUploadPlanDetails End");
		}
		return RETURN_LOAD_PLANS_PAGE;
	}

	/**
	 * Method is used to upload templates at FTP Server. 
	 */
	@RequestMapping(value = "/admin/serff/uploadPlans", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	public String uploadSERFFPlans(Model model, final HttpServletRequest request,
			@RequestParam("fileUploadPlans") MultipartFile[] fileList,
			@RequestParam(required = false) boolean pndCheckbox) {

		LOGGER.debug("uploadSERFFPlans Begin with pndCheckbox is " + pndCheckbox);
		String uploadStatus = null;

		try {
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			// set required data in Model Object
			setFields(model, stateCode);

			// Validate input parameters
			if (!validateUploadPlansRequest(pndCheckbox, model, request, fileList, stateCode)) {
				return RETURN_LOAD_PLANS_PAGE;
			}

			SerffPlanMgmtBatch.OPERATION_TYPE operationType = null;
			if (pndCheckbox) {
				operationType = SerffPlanMgmtBatch.OPERATION_TYPE.PRESCRIPTION_DRUG;
			}
			else {
				operationType = SerffPlanMgmtBatch.OPERATION_TYPE.PLAN;
			}
			// Upload SERFF templates at FTP server
			uploadStatus = serffService.uploadTemplateAttachments(addRequestDataInTemplatePOJO(request, fileList, operationType));

			// Display message to end user that files are submitted for process.
			if (uploadStatus.equalsIgnoreCase(PlanMgmtConstants.SUCCESS)) {
				model.addAttribute(UPLOAD_SUCESS, "All templates have been uploaded successfully!");
			}
			else {
				model.addAttribute(UPLOAD_STATUS, uploadStatus);
			}

			if (PlanMgmtConstants.STATE_CODE_WA.equalsIgnoreCase(stateCode)
					|| PlanMgmtConstants.STATE_CODE_CT.equalsIgnoreCase(stateCode)) {
				model.addAttribute("hasWaitingPlansToLoad", serffService.checkWaitingPlansToLoad());
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception: " + e.getMessage(), e);
		}
		finally {
			LOGGER.debug("uploadSERFFPlans End");
		}
		return RETURN_LOAD_PLANS_PAGE;
	}

	/**
	 * Method is used to required data for upload plan details. 
	 */
	@RequestMapping(value = "/admin/serff/executeBatch", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	public String executeBatch(final HttpServletRequest request, Model model,
			@RequestParam("sourceScreen") String sourceScreen) {

		LOGGER.debug("executeBatch Begin");
		String redirectPage = "redirect:/admin/serff/loadPlans";

		try {
//			model.addAttribute("executeBatchResponse", serffService.executeBatch());
			serffService.executeBatch();

			if (StringUtils.isNotBlank(sourceScreen)) {

				if ("plansLoadStatus".equalsIgnoreCase(sourceScreen)) {
					redirectPage = "redirect:/admin/serff/serffPlansLoadStatus";
				}
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception: " + e.getMessage(), e);
		}
		finally {
			LOGGER.debug("executeBatch End");
		}
		return redirectPage;
	}

	/**
	 * Method is used to check Validations for Upload UI page.
	 */
	private boolean validateUploadPlansRequest(boolean pndCheckFlag, Model model, final HttpServletRequest request,
			final MultipartFile[] fileList, String stateCode) {

		boolean hasValidRequest = false;
		StringBuffer errorMessage = new StringBuffer();

		if (!pndCheckFlag && (null == fileList || fileList.length < MANDATORY_FILE_LIST_COUNT)) {
			errorMessage.append("Mandatory templates not attached, ");
		}
		else if(!pndCheckFlag && !PlanMgmtConstants.STATE_CODE_MN.equalsIgnoreCase(stateCode) && fileList.length > OPTIONAL_FILE_LIST_COUNT) {
			errorMessage.append("Template names not correct, ");
		}
		else if (pndCheckFlag && (null == fileList || fileList.length != MANDATORY_PND_FILE_LIST_COUNT)) {
			errorMessage.append("Proper Drug templates not attached and allowed only "+ MANDATORY_PND_FILE_LIST_COUNT +" mandatory templates, ");
		}
		else {

			long maxSize = FTP_UPLOAD_MAX_SIZE / 1048576;

			for (MultipartFile multipartFile : fileList) {

				if (null != multipartFile
						&& multipartFile.getSize() > FTP_UPLOAD_MAX_SIZE) {
					errorMessage.append(multipartFile.getOriginalFilename() + " File size is exceeding max size " + maxSize + "MB, ");
				}
            }
		}

		if (StringUtils.isBlank(request.getParameter(CARRIER))) {
			errorMessage.append("Issuer not selected, ");
		}

		if (StringUtils.isBlank(request.getParameter(CARRIER_TEXT))) {
			errorMessage.append("Issuer Name not selected, ");
		}

		if (StringUtils.isNotBlank(errorMessage)) {
			LOGGER.error("Validation Erros: " + errorMessage.toString());
			model.addAttribute(UPLOAD_STATUS, errorMessage.substring(0, errorMessage.length() - 2));
		}
		else {
			hasValidRequest = true;
		}
		return hasValidRequest;
	}

	/**
	 * Method is used to add required data in Model Object for Upload UI.
	 * @param model, Model Object
	 * @throws Exception
	 */
	private void setFields(Model model, String stateCode) {
		model.addAttribute(ISSUER_LIST, serffService.getIssuerNameList());
		model.addAttribute("DRUG_SUFFIX", PRESCRIPTION_DRUG_FILENAME_SUFFIX);
		model.addAttribute("NETWORK_SUFFIX", NETWORK_ID_FILENAME_SUFFIX);
		model.addAttribute("BENEFITS_SUFFIX", BENEFITS_FILENAME_SUFFIX);
		model.addAttribute("RATE_SUFFIX", RATING_TABLE_FILENAME_SUFFIX);
		model.addAttribute("SERVICE_AREA_SUFFIX", SERVICE_AREA_FILENAME_SUFFIX);
		model.addAttribute("BUSINESS_RULE_SUFFIX", BUSINESS_RULE_FILENAME_SUFFIX);
		model.addAttribute("UNIFIED_RATE_SUFFIX", UNIFIED_RATE_FILENAME_SUFFIX);
		model.addAttribute("exchangeState", stateCode);
	}

	/**
	 * Method is used to add Request data in SerffPlanTemplatesPOJO.
	 */
	private SerffPlanTemplatesDTO addRequestDataInTemplatePOJO(final HttpServletRequest request, MultipartFile[] fileList, SerffPlanMgmtBatch.OPERATION_TYPE operationType) {

		SerffPlanTemplatesDTO templatePOJO = null;
		templatePOJO = new SerffPlanTemplatesDTO();
		templatePOJO.setIssuerId(request.getParameter(CARRIER));
		templatePOJO.setCarrierName(request.getParameter(CARRIER_TEXT));
		templatePOJO.setFileList(fileList);
		templatePOJO.setOperationType(operationType);
		return templatePOJO;
	}

	/**
	 * Method is used to load Plan status page. 
	 */
	@RequestMapping(value = "/admin/serff/serffPlansLoadStatus", method = {RequestMethod.GET, RequestMethod.POST})
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	public String loadPlansStatusPage(Model model) {
		model.addAttribute(ISSUER_LIST, serffService.getIssuerNameList());
		model.addAttribute("planStatusList", getPlanStatusList());
		model.addAttribute("planTypeList", getPlanTypeList());

		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if (PlanMgmtConstants.STATE_CODE_WA.equalsIgnoreCase(stateCode)
				|| PlanMgmtConstants.STATE_CODE_CT.equalsIgnoreCase(stateCode)) {
			model.addAttribute("hasWaitingPlansToLoad", serffService.checkWaitingPlansToLoad());
		}
		model.addAttribute("exchangeState", stateCode);
		return "admin/serff/serffPlansLoadStatus";
	}

	/**
	 * Method is used to get plan load status data from database.
	 */
	@RequestMapping(value = "/admin/serff/getPlanLoadStatusDataList", method = RequestMethod.GET, produces= "application/json")
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	@ResponseBody
	public Page<PlanLoadStatusDTO> getPlanLoadStatusDataList(@RequestParam("page") String pageStr, @RequestParam("size") String sizeStr,
			final HttpServletRequest request) throws GIException {

		String selectedIssuer = request.getParameter("issuerFilter");
		String selectedType = request.getParameter("typeFilter");
		String selectedStatus = request.getParameter("statusFilter");
		String selectedDate = request.getParameter("dateFilter");

		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("getPlanLoadStatusDataList: pageStr: " + pageStr + ", sizeStr: " + sizeStr
					+ ", selectedIssuer: " + selectedIssuer + ", selectedType: " + selectedType
					+ ", selectedStatus: " + selectedStatus + ", selectedDate: " + selectedDate);
		}

		int page = 1, size = 1;
		if (StringUtils.isNumeric(pageStr)) {
			page = Integer.parseInt(pageStr);
		}

		if (StringUtils.isNumeric(sizeStr)) {
			size = Integer.parseInt(sizeStr);
		}

		Page<PlanLoadStatusDTO> resultPage = serffService.getPlanLoadStatusDataList(page, size, selectedIssuer, selectedType, selectedStatus, selectedDate);
		if (page > resultPage.getTotalPages()) {
			throw new GIException("Page requested is invalid");
		}
		return resultPage;
	}

	/**
	 * Set Map for Plan Status drop-down box.
	 */
	public Map<String, String> getPlanStatusList() {
		Map<String, String> enumMap = new LinkedHashMap<String, String>();
		enumMap.put(SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED.toString(), STATUS.SUCCESS.toString());
		enumMap.put(SerffPlanMgmtBatch.BATCH_STATUS.FAILED.toString(), STATUS.FAILURE.toString());
		enumMap.put(SerffPlanMgmtBatch.BATCH_STATUS.IN_PROGRESS.toString(), STATUS.IN_PROGRESS.toString());
		return enumMap;
	}

	/**
	 * Set Map for Plan Type drop-down box.
	 */
	public Map<String, String> getPlanTypeList() {
		Map<String, String> enumMap = new LinkedHashMap<String, String>();
		enumMap.put(PlanLoadStatusDTO.TYPE.DRUG_LIST.toString(), PlanLoadStatusDTO.TYPE.DRUG_LIST.getName());
		enumMap.put(PlanLoadStatusDTO.TYPE.PLANS.toString(), PlanLoadStatusDTO.TYPE.PLANS.getName());
		return enumMap;
	}

	/**
	 * Method is used to load Plan status page. 
	 */
	@RequestMapping(value = "/admin/serff/serffTransferStatus", method = {RequestMethod.GET, RequestMethod.POST})
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	public String loadSERFFTransferStatusPage(Model model, final HttpServletRequest request) {

		List<IssuerDropDownDTO> issuerList = serffService.getIssuerNameList();

		if (!CollectionUtils.isEmpty(issuerList)) {
			model.addAttribute(ISSUER_LIST, platformGson.toJson(issuerList));
		}
		else {
			model.addAttribute(ISSUER_LIST, null);
		}

		List<Map<String, String>> listMap = getSerffTransferList();
		if (!CollectionUtils.isEmpty(listMap)) {
			model.addAttribute("serffTransferList", platformGson.toJson(listMap));
		}
		else {
			model.addAttribute("serffTransferList", null);
		}

		String selectedIssuer = request.getParameter("issuerFilter");
		String selectedStatus = request.getParameter("statusFilter");
		String selectedDate = request.getParameter("dateFilter");

		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("loadSERFFTransferStatusPage(): selectedIssuer: " + selectedIssuer + ", selectedStatus: "
				+ selectedStatus + ", selectedDate: " + selectedDate);
		}

		model.addAttribute("selectedIssuer", selectedIssuer);
		model.addAttribute("selectedStatus", selectedStatus);
		model.addAttribute("selectedDate", selectedDate);
		return "admin/serff/serffTransferStatus";
	}

	/**
	 * get List Map for Plan Status drop-down box.
	 */
	public List<Map<String, String>> getSerffTransferList() {

		List<Map<String, String>> listMap = new ArrayList<Map<String, String>>();
		Map<String, String> enumMap = new LinkedHashMap<String, String>();
		enumMap.put("key", SerffPlanMgmt.REQUEST_STATUS.S.toString());
		enumMap.put("value", "Success");
		listMap.add(enumMap);

		enumMap = new LinkedHashMap<String, String>();
		enumMap.put("key", SerffPlanMgmt.REQUEST_STATUS.F.toString());
		enumMap.put("value", "Failure");
		listMap.add(enumMap);

		enumMap = new LinkedHashMap<String, String>();
		enumMap.put("key", SerffPlanMgmt.REQUEST_STATUS.P.toString());
		enumMap.put("value", "In Progress");
		listMap.add(enumMap);
		return listMap;
	}

	/**
	 * Method is used to get plan load status data from database.
	 */
	@RequestMapping(value = "/admin/serff/getSERFFTransferStatusDataList", method = RequestMethod.GET, produces= "application/json")
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	@ResponseBody
	public Page<SerffTransferInfoDTO> getSERFFTransferStatusDataList(@RequestParam("page") String pageStr, Model model,
			final HttpServletRequest request) throws GIException {

		String selectedIssuer = request.getParameter("issuerFilter");
		String selectedStatus = request.getParameter("statusFilter");
		String selectedDate = request.getParameter("dateFilter");

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getSERFFTransferStatusDataList(): pageStr: " + pageStr + ", selectedIssuer: " + selectedIssuer
				+ ", selectedStatus: " + selectedStatus + ", selectedDate: " + selectedDate);
		}

		int page = 1;
		if (StringUtils.isNumeric(pageStr)) {
			page = Integer.parseInt(pageStr);
		}
		model.addAttribute("selectedIssuer", selectedIssuer);
		model.addAttribute("selectedStatus", selectedStatus);
		model.addAttribute("selectedDate", selectedDate);

		Page<SerffTransferInfoDTO> resultStatusDataList = serffService.getSERFFTransferInfoDataList(page, selectedIssuer, selectedStatus, selectedDate);
		return resultStatusDataList;
	}

	/**
	 * Method is used to get SERFF Plan Attachments List from database.
	 */
	@RequestMapping(value = "/admin/serff/getSERFFPlanAttachmentsList", method = RequestMethod.GET, produces= "application/json")
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	@ResponseBody
	public List<SerffTransferAttachmentInfoDTO> getSERFFPlanAttachmentsList(@RequestParam("selectedRecordID") String selectedRecordID) throws GIException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getSERFFPlanAttachmentsList(): selectedRecordID: " + SecurityUtil.sanitizeForLogging(selectedRecordID));
		}

		List<SerffTransferAttachmentInfoDTO> resultAttachmentsList = null;

		if (StringUtils.isNumeric(selectedRecordID)) {
			resultAttachmentsList = serffService.getSERFFPlanAttachmentsList(Long.valueOf(selectedRecordID));
		}
		else {
			LOGGER.error("Invalid Record ID to get Attachments List.");
		}
		return resultAttachmentsList;
	}

	/**
	 * Method is used to download SERFF Template from database.
	 */
	@RequestMapping(value = "/admin/serff/downloadSerffTemplate", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	public void downloadSerffTemplate(@RequestParam("selectedDocumentId") String selectedDocumentId, HttpServletResponse response) throws GIException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("downloadSerffTemplate start with selectedDocumentId: " + SecurityUtil.sanitizeForLogging(selectedDocumentId));
		}

		try {

			if (StringUtils.isNotBlank(selectedDocumentId)) {

				byte[] fileByteData = ecmService.getContentDataById(selectedDocumentId);
				Content docMetaData = ecmService.getContentById(selectedDocumentId);

				if (null != fileByteData && null != docMetaData) {

					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("docMetaData getTitle : " + docMetaData.getTitle());
					}
					response.setContentType("application/octet-stream");
//					response.setContentType("application/xml");
					response.setContentLength(fileByteData.length);
					response.setHeader("Content-Disposition", "attachment; filename=" + docMetaData.getTitle());
					FileCopyUtils.copy(fileByteData, response.getOutputStream());
				}
				else if(LOGGER.isWarnEnabled()) {
					LOGGER.warn("File is null for Document ID: " + SecurityUtil.sanitizeForLogging(selectedDocumentId));
				}
			}
			else {
				LOGGER.error("Document ID is empty.");
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred while downloading SERFF Template for Document ID: "
				+ SecurityUtil.sanitizeForLogging(selectedDocumentId)
				+ ": " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("downloadSerffTemplate end");
		}
	}

	/**
	 * Method is used to required data for upload Drugs details. 
	 */
	@RequestMapping(value = "/admin/serff/loadDrugs", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	public String loadUploadDrugDetails(Model model) {

		LOGGER.debug("loadUploadDrugDetails Begin");

		try {
			model.addAttribute(ISSUER_LIST, serffService.getIssuerNameList());
		}
		catch (Exception ex) {
			LOGGER.error("Exception: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("loadUploadDrugDetails End");
		}
		return RETURN_LOAD_DRUGS_PAGE;
	}

	/**
	 * Method is used to upload Drugs data File at FTP Server. 
	 */
	@RequestMapping(value = "/admin/serff/uploadDrugs", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	public String uploadDrugs(Model model, final HttpServletRequest request,
			@RequestParam("fileToUpload") MultipartFile fileToUpload, @RequestParam("fileType") String fileType) {

		LOGGER.debug("uploadDrugs Begin");
		String uploadStatus = null;

		try {
			model.addAttribute(ISSUER_LIST, serffService.getIssuerNameList());

			// Validate input parameters
			if (!validateUploadDrugsRequest(model, request, fileToUpload)) {
				return RETURN_LOAD_DRUGS_PAGE;
			}

			SerffPlanMgmtBatch.OPERATION_TYPE operationType = null;
			if ("XML".equals(fileType)) {
				operationType = SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_XML;
			}
			else if ("JSON".equals(fileType)) {
				operationType = SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_JSON;
			}

			if (null != operationType) {
				// Upload Drug File at FTP server
				MultipartFile fileToUploadList[] = { fileToUpload };
				SerffPlanTemplatesDTO templatePOJO = addRequestDataInTemplatePOJO(request, fileToUploadList, operationType);
				templatePOJO.setDefaultTenant(request.getParameter(SELECT_YEAR));
				uploadStatus = serffService.uploadTemplateAttachments(templatePOJO);

				// Display message to end user that files are submitted for process.
				if (uploadStatus.equalsIgnoreCase(PlanMgmtConstants.SUCCESS)) {
					model.addAttribute(UPLOAD_SUCESS, "Drug File has been uploaded successfully!");
				}
				else {
					model.addAttribute(UPLOAD_STATUS, uploadStatus);
				}
			}
			else {
				model.addAttribute(UPLOAD_STATUS, "Wrong Drug File type!");
			}
		}
		catch (Exception ex) {
			LOGGER.error("Exception: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("uploadDrugs End");
		}
		return RETURN_LOAD_DRUGS_PAGE;
	}

	/**
	 * Method is used to check Validations for Drugs Upload UI page.
	 */
	private boolean validateUploadDrugsRequest(Model model, final HttpServletRequest request, final MultipartFile fileToUpload) {

		LOGGER.debug("validateUploadDrugsRequest Begin");
		boolean hasValidRequest = false;

		try {
			StringBuffer errorMessage = new StringBuffer();
			long maxSize = FTP_UPLOAD_MAX_SIZE / 1048576;

			if (null == fileToUpload || fileToUpload.isEmpty()) {
				errorMessage.append("Mandatory Drug file not attached, ");
			}
			else if (fileToUpload.getSize() > FTP_UPLOAD_MAX_SIZE) {
				errorMessage.append(fileToUpload.getOriginalFilename() + " File size is exceeding max size " + maxSize + "MB, ");
			}

			if (!StringUtils.isNumeric(request.getParameter(CARRIER))) {
				errorMessage.append("Issuer not selected, ");
			}

			if (StringUtils.isBlank(request.getParameter(CARRIER_TEXT))) {
				errorMessage.append("Issuer Name not selected, ");
			}

			if (!StringUtils.isNumeric(request.getParameter(SELECT_YEAR))) {
				errorMessage.append("Year not selected, ");
			}

			if (StringUtils.isNotBlank(errorMessage)) {
				LOGGER.error("Validation Erros: " + errorMessage.toString());
				model.addAttribute(UPLOAD_STATUS, errorMessage.substring(0, errorMessage.length() - 2));
			}
			else {
				hasValidRequest = true;
			}
		}
		finally {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("validateUploadDrugsRequest End with Valid Request: " + hasValidRequest);
			}
		}
		return hasValidRequest;
	}
}
