package com.getinsured.iex.hub.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

/**
 * Controller class to handle synchronous request to RIDP module
 * 
 * @author Nikhil Talreja
 * @since 24-Feb-2014
 * 
 */
@Controller
public class RIDPController extends MessageProcessor {

	@Autowired private WebServiceTemplate ridpServiceTemplate;
	@Autowired private WebServiceTemplate farsServiceTemplate;
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RIDPController.class);

	/**
	 * Handles, primary request for RIDP
	 * 
	 * @author Nikhil Talreja
	 * @since 24-Feb-2014
	 * 
	 */
	@RequestMapping(value = "/invokeRIDPPrimary", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> invokeRIDPPrimary(@RequestBody String primaryRequestJSON){
		GIWSPayload request = null;
		try{
			HubServiceBridge ridpPrimary = HubServiceBridge.getHubServiceBridge("RIDPPrimaryRequest");
			ridpPrimary.getServiceHandler().setJsonInput(primaryRequestJSON);
			request = new GIWSPayload();
			request.setEndpointFunction("RIDP");
			request.setEndpointOperationName("PRIMARY");
			request.setRetryCount(MAX_RETRY_COUNT);
			return this.executeRequest(request, ridpPrimary.getServiceHandler(), this.ridpServiceTemplate);
		}
		catch (Exception e){
			LOGGER.error(e.getMessage(),e);
			return new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
	}
	
	/**
	 * Handles, secondary request for RIDP
	 * 
	 * @author Nikhil Talreja
	 * @since 24-Feb-2014
	 * 
	 */
	@RequestMapping(value = "/invokeRIDPSecondary", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> invokeRIDPSecondary(@RequestBody String secondaryRequestJSON) {
		GIWSPayload request = null;
		try{
			HubServiceBridge ridpPrimary = HubServiceBridge.getHubServiceBridge("RIDPSecondaryRequest");
			ServiceHandler secondaryRequestHandler = ridpPrimary.getServiceHandler();
			// First param is null as the response section is at the root of the json input string
			secondaryRequestHandler.setJsonInput(secondaryRequestJSON);
			request = new GIWSPayload();
			request.setEndpointFunction("RIDP");
			request.setEndpointOperationName("SECONDARY");
			request.setRetryCount(MAX_RETRY_COUNT);
			return this.executeRequest(request, secondaryRequestHandler, this.ridpServiceTemplate);
		}
		catch (Exception e){
			LOGGER.error(e.getMessage(),e);
			return new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Handles, request for FARS
	 * 
	 * @author Nikhil Talreja
	 * @since 20-Mar-2014
	 * 
	 */
	@RequestMapping(value = "/invokeFARS", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> invokeFARS(@RequestBody String requestJSON) {
		GIWSPayload request = null;
		try{
			HubServiceBridge ridpPrimary = HubServiceBridge.getHubServiceBridge("FARSRequest");
			ServiceHandler handler = ridpPrimary.getServiceHandler();
			handler.setJsonInput(requestJSON);
			request = new GIWSPayload();
			request.setEndpointFunction("RIDP");
			request.setEndpointOperationName("FARS");
			request.setRetryCount(MAX_RETRY_COUNT);
			return this.executeRequest(request, handler, this.farsServiceTemplate);
		}
		catch (Exception e){
			LOGGER.error(e.getMessage(),e);
			return new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
	}
}
