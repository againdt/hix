package com.getinsured.ssap.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.ssap.model.eligibility.EligibilityResponse;
import com.getinsured.ssap.model.eligibility.HouseholdMemberResponse;

/**
 * Determination service for exchange eligibility.
 * (sometimes referred as roll-up logic).
 *
 * @author Yevgen Golubenko
 * @since 5/22/19
 */
@Component
public class ExchangeEligibilityDeterminationService {
  /**
   * Does roll-up logic based on the exchange eligibility of the each household member of
   * given SSAP.
   * <p>
   *   Current determination rules are described in HIX-110135 and as follows:
   *   <br/>
   * </p>
   * <p>
   *   <ul>
   *    <li>Any APTC eligible will be rolled up to APTC</li>
   *    <li>Any APTC/ All CSR eligible will be rolled up to APTC_CSR<li/>
   *    <li>If Any APTC and at least one is CSR the HH eligibility will be APTC_CSR</li>
   *    <li>If no member is APTC eligible and at least one member is CSR eligible, then HH will be CSR eligible.</li>
   *    <li>All Exchange eligible will be rolled up to QHP eligible</li>
   *  </ul>
   * </p>
   *
   * @param application {@link SsapApplication}
   */
  public SsapApplication determinateExchangeEligibility(final SsapApplication application, final EligibilityResponse eligibilityResponse) {
    final List<HouseholdMemberResponse> members = eligibilityResponse.getMembers();

    if(members == null || members.size() == 0) {
      application.setExchangeEligibilityStatus(ExchangeEligibilityStatus.NONE);
      return application;
    }

    boolean anyAptcEligible = members.stream().anyMatch(HouseholdMemberResponse::isAptcEligible);
    boolean allCsrEligible = members.stream().allMatch(HouseholdMemberResponse::isCsrEligible);
    boolean someoneCsrEligible = members.stream().anyMatch(HouseholdMemberResponse::isCsrEligible);
    boolean noneAptcEligible = !members.stream().allMatch(HouseholdMemberResponse::isAptcEligible);
    boolean allExchangeEligible = members.stream().allMatch(HouseholdMemberResponse::isExchangeEligible);

    if (anyAptcEligible) {
      if (allCsrEligible || someoneCsrEligible) {
        application.setExchangeEligibilityStatus(ExchangeEligibilityStatus.APTC_CSR);
      } else {
        application.setExchangeEligibilityStatus(ExchangeEligibilityStatus.APTC);
      }
    } else if (noneAptcEligible && someoneCsrEligible) {
      application.setExchangeEligibilityStatus(ExchangeEligibilityStatus.CSR);
    } else if (allExchangeEligible) {
      application.setExchangeEligibilityStatus(ExchangeEligibilityStatus.QHP);
    } else {
      application.setExchangeEligibilityStatus(ExchangeEligibilityStatus.NONE);
    }

    return application;
  }
}
