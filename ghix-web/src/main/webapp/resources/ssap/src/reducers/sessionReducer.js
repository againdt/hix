
import CONFIG from '../.env';
import { sessionActionTypes } from '../constants/actionTypes';

export const initialState = {
    lastAccessedTime: 0,
    inactiveIntervalMin: 0,
    creationTime: 0,
    now: 0,
    nowTimeShifted: 0
};

if(CONFIG.ENV.DEVELOPMENT) {
    initialState.inactiveIntervalMin = 30;
}

export default (state = initialState, action) => {
    switch(action.type) {
    case sessionActionTypes.REFRESH_SESSION:
        return action.session;
    case sessionActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};