package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpiav.ArrayOfErrorResponseMetadataType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpiav.InitiateAdditionalVerifResponseType;

public class InitiateAdditionalVerifResponseTypeWrapper implements JSONAware{

	private InitiateAdditionalVerifResponseType initiateAdditionalVerifResponseType;
	
	private InitiateAddVerifResponseMetadataWrapper responseMetadata;
	private String lawfulPresenceVerifiedCode;

	private ArrayOfErrorResponseMetadataType errorMetadata;
	
	public InitiateAdditionalVerifResponseTypeWrapper(InitiateAdditionalVerifResponseType initiateAdditionalVerifResponseType){
		
		if(initiateAdditionalVerifResponseType == null){
			return;
		}
		
		this.initiateAdditionalVerifResponseType = initiateAdditionalVerifResponseType;
		InitiateAddVerifResponseMetadataWrapper responseMetadataWrapper = new InitiateAddVerifResponseMetadataWrapper(initiateAdditionalVerifResponseType.getResponseMetadata());
		this.responseMetadata = responseMetadataWrapper;
		errorMetadata = initiateAdditionalVerifResponseType.getArrayOfErrorResponseMetadata();
		
	}

	public InitiateAdditionalVerifResponseType getInitiateAdditionalVerifResponseType() {
		return initiateAdditionalVerifResponseType;
	}

	public void setInitiateAdditionalVerifResponseType(
			InitiateAdditionalVerifResponseType initiateAdditionalVerifResponseType) {
		this.initiateAdditionalVerifResponseType = initiateAdditionalVerifResponseType;
	}

	public InitiateAddVerifResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(InitiateAddVerifResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}

	public String getLawfulPresenceVerifiedCode() {
		return lawfulPresenceVerifiedCode;
	}

	public void setLawfulPresenceVerifiedCode(String lawfulPresenceVerifiedCode) {
		this.lawfulPresenceVerifiedCode = lawfulPresenceVerifiedCode;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		if(this.responseMetadata != null) {
			obj.put("ResponseMetadata",this.responseMetadata);
		}
		
		if(this.lawfulPresenceVerifiedCode != null) {
			obj.put("LawfulPresenceVerifiedCode",this.lawfulPresenceVerifiedCode);
		}
		return obj.toJSONString();
	}

}
