<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

	<div class="row-fluid">
		<div class="gutter10">
		<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin-top0"><spring:message  code="label.details"/></h4>
                </div>
                
                <ul class="nav nav-list">
                    <li class="active"><spring:message  code="label.repInfoReview"/></li>
                </ul>
		</div>
			<!-- end of span3 -->
           <div class="span9" id="rightpanel">
           <div class="header">
			<h4><spring:message code="label.representativeDetails"/></h4>
			</div>
			<div class="gutter10">
		
				<table class="table table-border-none">
					<tbody>
						<tr>
							<td class="span5"><spring:message code="label.firstName"/></td>
							<td><strong>${userInfo.firstName}</strong></td>
						</tr>
						<tr>
							<td><spring:message code="label.lastName"/></td>
							<td> <strong>${userInfo.lastName}</strong></td>
						</tr>
						<tr>
							<td><spring:message code="label.title"/></td>
							<td><strong>${userInfo.title}</strong></td>
						</tr>
						<tr>
							<td><spring:message code="label.phoneNumber"/></td>
							<td><strong>${userInfo.phone}</strong></td>
						</tr>
						<tr>
							<td><spring:message code="label.emailAddress"/></td>
							<td><strong>${userInfo.email}</strong></td>
						</tr>
					</tbody>
				</table>
				
			</div>
			<!--gutter10-->
		</div>
		</div>
       </div><!--  end of span9 -->
		
