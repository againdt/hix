 var app =  angular.module('capApp', ['ui.mask']);

 app.controller('mainController',['$scope','$http', function($scope,$http)  {
	
	 $scope.consumer= {callPermission:'YES', ssnRequired:'YES'};
	 
	 if(dataToPopulate !="")
		{
			dataToPopulate=dataToPopulate.replace(/&quot;/g,'"');		
			$scope.consumer=angular.fromJson(dataToPopulate);
			if($scope.consumer.callPermission==undefined)
				{
					$scope.consumer.callPermission="YES";
				}
		}else{
			$scope.consumer={callPermission:'YES', ssnRequired:'YES'};
		}
	 
	 $scope.validateSSNField = function(){
		 if($scope.consumer.ssnRequired == "YES"){
			 return true;
		 }
		 else if($scope.consumer.ssnRequired == "NO" && $scope.consumer.ssnNotReqdReasons){
				return true;
			}
		 else{
			 return false;
		 }
	 }
	 
	// Send information the form
	$scope.consumerSave = function() {
		
		//Show Loader 
		$scope.loader = true;
		
		$scope.consumer.phoneNumber = this.phoneNumber1 + this.phoneNumber2 + this.phoneNumber3;
		$scope.consumer.firstName = this.consumer.firstName;
		$scope.consumer.lastName = this.consumer.lastName;
		$scope.consumer.dob = this.consumer.dob;
		$scope.consumer.zipcode = this.consumer.zipcode;
		$scope.consumer.emailAddress = this.consumer.emailAddress || null;
		$scope.consumer.callPermission = this.consumer.callPermission;
		$scope.consumer.ssnRequired = this.consumer.ssnRequired;
		$scope.consumer.ssnNotReqdReasons = this.consumer.ssnNotReqdReasons;
		//$scope.consumer.url = "";
		
		$scope.showInvalidEmailError=false;
		$scope.showExactHouseholdError=false;
		
		
		
		//var	linkUrl = "/referral/csrflow/" + ssapAppId + "/" + cmrId;
      
		var dataToServer={};
		
		dataToServer.firstName=$scope.consumer.firstName;
		dataToServer.lastName=$scope.consumer.lastName;
		dataToServer.dob=$scope.consumer.dob;
		dataToServer.zipcode=$scope.consumer.zipcode;
		dataToServer.phoneNumber=$scope.consumer.phoneNumber;
		dataToServer.emailAddress=$scope.consumer.emailAddress;
		dataToServer.callPermission=$scope.consumer.callPermission;
		dataToServer.ssnRequired = $scope.consumer.ssnRequired;
		dataToServer.ssnNotReqdReasons = $scope.consumer.ssnNotReqdReasons;
		
		//dataToServer.callPermission=$scope.consumer.callPermission;
		//dataToServer.url="/referral/csrflow/" + $scope.consumer.encssapApplicationId + "/"+

		var postHeader={
			'Content-Type' : 'application/json'
		};
		postHeader[csrf]=csrfValue;

		
		$http({
			method : 'POST',
			url : theUrlForAddconsumersubmit,
			data : dataToServer, // pass in data as strings
			headers:postHeader
		// set the headers so angular passing info as form data (not request
		// payload);
		}).success(function(data,xhr) {
			console.log(data);
			if(isInvalidCSRFToken(xhr))	    				
				return;
			
			//Hide Loader 
			$scope.loader = false;
			
			if(data == null)
				{
					alert(" Something went wrong.. please contact admin. ");
					return;
				}
			if (data.status) {//boolean
				console.log('Works: ' + data.message);
				$scope.householdId = data.householdId;
				if($("#csrReferralFlow").val()=='csrReferralFlow'){
					$scope.url="/referral/csrflow/" + $scope.consumer.encssapApplicationId + "/"+$scope.householdId;
					$("#switchToNonDefaultURL").val($scope.url);
					
				}
				$("#csrf").val($("input[name='csrftoken']").val()).attr("name","csrftoken");				
				
				//Disable Start Button on success
				$('#mainSubmitButton').attr('disabled',true);
				
				$("#viewIndividualAccount").modal();
				console.log($scope.householdId);
			} else {
				// if successful, bind success message to message
				console.log('Fail:' + data.message);
				if(data.message.indexOf("INVALID_CONSUMER") >= 0) {
					$scope.showExactHouseholdError=true;
				}
				if(data.message.indexOf("INVALID_EMAIL") >= 0){
					$scope.showInvalidEmailError=true;
				} else {
					alert("Invalid email");
				}
			}
		}).error(function (data, xhr) {    	
			//Hide Loader 
			$scope.loader = false;
			
			//Remove disabled attribute on Start button if its error:
			$('#mainSubmitButton').removeAttr('disabled');
		});
	};
}]);
 
(function(){
	var spinningLoader = function($timeout){
		return {
			restrict: "EA", 
			template: '<div id="loaderDiv" ng-show="loadSpinner"><img src="../../resources/images/loader_gray.gif" class="ajax-loader" alt="loader"/></div>',
		    link: function (scope, elem, attrs, ctrl) {
	          var starting;
	          scope.$watch(attrs.spinningLoader, function(nV, oV){
	            if (nV === true){
	              starting = new Date();
	              scope.loadSpinner = true;                   
	            } else {
	              var ending = new Date();
	              var timeDiff = ending - starting;
	              if (timeDiff >= 1000){
	                scope.loadSpinner = false;
	              } else {
	                $timeout(function(){
	                  scope.loadSpinner = false;
	                }, (1000 - timeDiff));
	              };           
	            }; 
	         });
	      }
		};
	};
	
	angular.module('capApp')
	.directive("spinningLoader", spinningLoader);
})();
