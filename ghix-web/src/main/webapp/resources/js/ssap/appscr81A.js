function setappscr81A(){
	
	
	if(currentPage == "appscr81A")
		hideSSNQuestions();
	
	next();

}

//Skip or Keep Page80
function skipOrKeepPage80()
{
	householdSSNIndicationValue =  $('#appscr57HouseHoldHavingSSNTD1').text();
	if(currentPage == 'appscr79')
		if(householdSSNIndicationValue == "yes")
		{
			next();
		}
}

function hideSSNQuestions()
{
	if(householdSSNIndicationValue == "yes" || $('input:radio[name=ssnIndicator80]:checked').val() == "yes" )
	{
		$('#wouldLikeToGiveSSN_div').hide();
		$('#wouldLikeToGiveSSNlbl_div').hide();
	}
	else
	{
		$('#wouldLikeToGiveSSN_div').show();
		$('#wouldLikeToGiveSSNlbl_div').show();
	}
}