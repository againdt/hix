package com.getinsured.hix.batch.notification.ssap;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.batch.util.GhixBatchConstants;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.dto.planmgmt.PediatricPlanResponseDTO;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.PassiveEnrollmentRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;

@Service("agedOutDisenrollmentNoticeBatchService")
public class AgedOutDisenrollmentNoticeServiceImpl implements AgedOutDisenrollmentNoticeService {

	private static final Logger LOGGER = Logger.getLogger(AgedOutDisenrollmentNoticeServiceImpl.class);

	@Autowired
	SsapApplicationRepository ssapApplicationRepository;
	@PersistenceUnit
	private EntityManagerFactory emf;
	@Autowired
	private NoticeService noticeService;
	@Autowired
	private PassiveEnrollmentRepository passiveEnrollmentRepository;
	@Autowired
	@Qualifier("iLocationRepository")
	private ILocationRepository iLocationRepository;
	@Autowired
	private PreferencesService preferencesService;
	@Autowired
	private Gson platformGson;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	private static final String DENTAL_AGE_OUT_AGE = "19";
	private static final String HEALTH_AGE_OUT_AGE = "26";
	private static final String HEALTH_CODE = "HLT";
	private static final String DENTAL_CODE = "DEN";
	private static final String FINANCIAL_APP_CODE = "F";
	private static final String NON_FINANCIAL_APP_CODE = "NF";
	private static final String BOTH = "BOTH";
	private static final String FINANCIAL_ASSISTANCE = "financialAssistance";
	private static final String ENROLLMENT_STATUS_LIST = "enrollmentStatusList";
	private static final String INSURANCE_TYPE = "insuranceType";
	private static final String CHILD = "child";
	private static final String SSAP_TAX_HOUSEHOLD_PATH = "$.singleStreamlinedApplication.taxHousehold[*]";
	private static final List<String> ACTIVE_STATUS_LIST = new ArrayList<String>(Arrays.asList("CONFIRM", "PENDING","APPROVED"));

	@SuppressWarnings("serial")
	private static final Map<String, String> BLOOD_RELATIONS = new HashMap<String, String>() {
		{
			put("14", CHILD);
			put("03", CHILD);
			put("16", CHILD);
		}
	};

	public List<String> processAgeOutDependents(String selectedCaseNumbers) {
		String insuranceTypeConfig = DynamicPropertiesUtil
				.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AGE_OUT_INSURANCE_TYPE);
		String applicationTypeConfig = DynamicPropertiesUtil
				.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AGE_OUT_APPLICATION_TYPE);
		boolean ageOutHealth = getBooleanVal(HEALTH_CODE, insuranceTypeConfig);
		boolean ageOutDental = getBooleanVal(DENTAL_CODE, insuranceTypeConfig);
		boolean ageOutFinancial = getBooleanVal(FINANCIAL_APP_CODE, applicationTypeConfig);
		boolean ageOutNonFinancial = getBooleanVal(NON_FINANCIAL_APP_CODE, applicationTypeConfig);
		List<String> errorList = new ArrayList<>();
		List<String> caseNumbers = (StringUtils.isBlank(selectedCaseNumbers))
				? getCaseNumbers(ageOutHealth, ageOutDental, ageOutFinancial, ageOutNonFinancial)
				: Arrays.asList(selectedCaseNumbers.split(","));

		if (caseNumbers == null || caseNumbers.contains("NoLookupFound")) {
			errorList.add("No Lookup Values found ");
			return errorList;
		}
		LOGGER.debug("found " + caseNumbers.size() + " records to process");

		DateTime runDate = DateTime.now();
		DateTime expiryDate = runDate.plusMonths(2);
		for (String caseNumber : caseNumbers) {
			try {
				SsapApplication application = getApplicationData(caseNumber);
				LOGGER.debug("processing caseNumber=" + caseNumber);
				List<AgeOutNoticeDTO> dataList = prepareNoticeData(application, runDate, expiryDate, ageOutHealth,
						ageOutDental);
				for (AgeOutNoticeDTO data : dataList) {
					if (data != null) {
						generate(data, application, "EE005QHPAgeOutNotice");
						if (data != null && data.getHealthDependants() != null
								&& data.getHealthDependants().size() > 0) {
							for (AgeOutDependant ageOutDependant : data.getHealthDependants()) {
								String depAgeOutEcmId = generateDependant(ageOutDependant, data, application,
										"EE023QHPAgeOutDependantNotice");
								LOGGER.info(
										"Dependent Age out notice sent for " + caseNumber + " at " + depAgeOutEcmId);
							}
						}
					}
				}
			} catch (NoticeServiceException e) {
				LOGGER.error("Skipping caseNumber=" + caseNumber + " due to exception : ", e);
				errorList.add(caseNumber);
			} catch(Exception ex) {
				LOGGER.error("Skipping caseNumber=" + caseNumber + " due to exception : ", ex);
				errorList.add(caseNumber);
			}
		}
		return errorList;
	}

	private String generateDependant(AgeOutDependant ageOutDependant, AgeOutNoticeDTO ageOutNoticeDTO,
			SsapApplication application, String noticeTemplateName) throws NoticeServiceException {
		int moduleId = ageOutNoticeDTO.getHouseholdId();
		String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
		String fullName = ageOutNoticeDTO.getPrimaryFirstName() + " " + ageOutNoticeDTO.getPrimaryLastName();

		String relativePath = "cmr/" + moduleId + "/ssap/" + application.getId() + "/notifications/";
		String ecmFileName = noticeTemplateName + "_" + moduleId + (new Date().getTime()) + ".pdf";
		String emailId = ageOutNoticeDTO.getEmailId();
		List<String> validEmails = emailId != null ? Arrays.asList(emailId) : null;
		Notice notice = null;

		PreferencesDTO preferencesDTO = preferencesService.getPreferences(moduleId, false);

		Location location = getLocation(preferencesDTO);
		notice = noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN,
				getReplaceableObjectData(ageOutNoticeDTO, application, ageOutDependant), relativePath, ecmFileName,
				moduleName, moduleId, validEmails,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
				fullName, location, preferencesDTO.getPrefCommunication());

		return notice.getEcmId();

	}

	private String generate(AgeOutNoticeDTO ageOutNoticeDTO, SsapApplication ssapApplication, String noticeTemplateName)
			throws NoticeServiceException {

		int moduleId = ageOutNoticeDTO.getHouseholdId();
		String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
		String fullName = ageOutNoticeDTO.getPrimaryFirstName() + " " + ageOutNoticeDTO.getPrimaryLastName();

		String relativePath = "cmr/" + moduleId + "/ssap/" + ssapApplication.getId() + "/notifications/";
		String ecmFileName = noticeTemplateName + "_" + moduleId + (new Date().getTime()) + ".pdf";
		String emailId = ageOutNoticeDTO.getEmailId();
		List<String> validEmails = emailId != null ? Arrays.asList(emailId) : null;
		Notice notice = null;

		PreferencesDTO preferencesDTO = preferencesService.getPreferences(moduleId, false);

		Location location = getLocation(preferencesDTO);

		notice = noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN,
				getReplaceableObjectData(ageOutNoticeDTO, ssapApplication, null), relativePath, ecmFileName, moduleName,
				moduleId, validEmails,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
				fullName, location, preferencesDTO.getPrefCommunication());

		return notice.getEcmId();
	}

	private Map<String, Object> getReplaceableObjectData(AgeOutNoticeDTO ageOutNoticeDTO,
			SsapApplication ssapApplication, AgeOutDependant ageOutDependant) throws NoticeServiceException {
		Map<String, Object> tokens = new HashMap<String, Object>();
		tokens.put("primaryApplicantName",
				ageOutNoticeDTO.getPrimaryFirstName() + " " + ageOutNoticeDTO.getPrimaryLastName());
		tokens.put("ApplicationID", ssapApplication.getCaseNumber());
		tokens.put("Date", DateUtil.dateToString(new Date(), "MMMM dd, YYYY"));
		tokens.put(TemplateTokens.EXCHANGE_FULL_NAME,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		tokens.put(TemplateTokens.EXCHANGE_PHONE,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil
				.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_2, DynamicPropertiesUtil
				.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
		tokens.put("exgCityName",
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
		tokens.put("exgStateName",
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
		tokens.put("zip",
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
		tokens.put(TemplateTokens.EXCHANGE_URL,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		tokens.put("exchangeFax",
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
		tokens.put("exchangeAddressEmail", DynamicPropertiesUtil
				.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		tokens.put(TemplateTokens.EXCHANGE_NAME,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		tokens.put("ageOutDependents", ageOutNoticeDTO);
		SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
		tokens.put("spanishDate", formatter.format(new Date()));
		tokens.put("caseNumber", ssapApplication.getCaseNumber());
		tokens.put("ssapApplicationId", ssapApplication.getId());
		if (ageOutDependant != null) {
			tokens.put("ageOutDependant", ageOutDependant);
			tokens.put("ageOutDependentName", ageOutDependant.getFirstName() + " " + ageOutDependant.getLastName());
		}

		return tokens;
	}

	@SuppressWarnings("unchecked")
	public List<String> getCaseNumbers(boolean ageOutHealth, boolean ageOutDental, boolean ageOutFinancial,
			boolean ageOutNonFinancial) {
		EntityManager em = null;
		List<String> rsObj = new ArrayList<>();
		List<String> financialAssitance = new ArrayList<String>();
		if (ageOutFinancial) {
			financialAssitance.add("Y");
		}
		if (ageOutNonFinancial) {
			financialAssitance.add("N");
		}
		List<String> insuranceType = new ArrayList<String>();
		if (ageOutHealth) {
			insuranceType.add(HEALTH_CODE);
		}
		if (ageOutDental) {
			insuranceType.add(DENTAL_CODE);
		}
		Query query = null;
		StringBuilder sb = new StringBuilder();
		try {
			sb.append(
					"select distinct a.case_number from ssap_applicants p inner join ssap_applications a on p.ssap_application_id=a.id ");
			sb.append(" inner join enrollment en ");
			sb.append(" on en.ssap_application_id=a.id ");
			sb.append(" inner join lookup_value lv1 ");
			sb.append(" on en.enrollment_status_lkp = lv1.lookup_value_id ");
			sb.append(" inner join lookup_value lv2 ");
			sb.append(" on en.insurance_type_lkp = lv2.lookup_value_id ");
			sb.append(" where p.birth_date is not null ").append(" and a.financial_assistance_flag in :")
					.append(FINANCIAL_ASSISTANCE).append(" and a.application_status = 'EN'");
			sb.append(" and lv1.lookup_value_code in :").append(ENROLLMENT_STATUS_LIST);
			sb.append(" and lv2.lookup_value_code in :").append(INSURANCE_TYPE);
			em = emf.createEntityManager();
			query = em.createNativeQuery(sb.toString());
			query.setParameter(FINANCIAL_ASSISTANCE, financialAssitance);
			query.setParameter(ENROLLMENT_STATUS_LIST, ACTIVE_STATUS_LIST);
			query.setParameter(INSURANCE_TYPE, insuranceType);
			rsObj = query.getResultList();
		} finally {
			if (em != null && em.isOpen()) {
				em.close();
			}
		}
		return rsObj;
	}

	Date getBirthDateGivenYearsBack(int givenYears) {
		return DateTime.now().plusMonths(2).minusYears(givenYears).toDate();
	}

	private SsapApplication getApplicationData(String caseNumber) {
		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByCaseNumber(caseNumber);
		return ssapApplications.get(0);
	}

	private class HouseholdMemberComparator implements Comparator<HouseholdMember> {

		@Override
		public int compare(HouseholdMember o1, HouseholdMember o2) {
			return o1.getPersonId().compareTo(o2.getPersonId());
		}
	}

	private List<AgeOutNoticeDTO> prepareNoticeData(SsapApplication application, DateTime runDate, DateTime expiryDate,
			boolean ageOutHealth, boolean ageOutDental) {
		List<AgeOutNoticeDTO> ageOutNoticeList = new ArrayList<>();
		List<String> insuranceType = new ArrayList<String>();
		if (ageOutHealth) {
			insuranceType.add(HEALTH_CODE);
		}
		if (ageOutDental) {
			insuranceType.add(DENTAL_CODE);
		}

		LOGGER.debug("passiveEnrollmentRepository starting  at   " + new Timestamp(System.currentTimeMillis()));
		List<Enrollment> enrollments = passiveEnrollmentRepository
				.findActiveEnrollmentBySsapApplicationIdAndInsuranceType(application.getId(), insuranceType);
		LOGGER.debug("passiveEnrollmentRepository ending  at   " + new Timestamp(System.currentTimeMillis()));
		
		boolean isChildOnlyDentalPlan = isChildOnlyPlan(enrollments.stream()
				.filter(e -> DENTAL_CODE.equalsIgnoreCase(e.getInsuranceTypeLkp().getLookupValueCode())).findFirst()
				.orElse(null), application.getCoverageYear());

		for (Enrollment enrollment : enrollments) {
			String healthPlanName = getPlanName(HEALTH_CODE, Arrays.asList(enrollment));
			String dentalPlanName = getPlanName(DENTAL_CODE, Arrays.asList(enrollment));
			ageOutNoticeList.add(prepareNotice(application, runDate, expiryDate,
					getEnrolleesByPlan(HEALTH_CODE, Arrays.asList(enrollment)),
					getEnrolleesByPlan(DENTAL_CODE, Arrays.asList(enrollment)), healthPlanName, dentalPlanName, isChildOnlyDentalPlan));
		}
		return ageOutNoticeList;
	}

	private String getPlanName(String planType, List<Enrollment> enrollments) {
		String planName = "";
		for (Enrollment enrollment : enrollments) {
			if (planType.equals(enrollment.getInsuranceTypeLkp().getLookupValueCode())) {
				planName = enrollment.getPlanName();
			}
		}
		return planName;
	}

	private Set<String> getEnrolleesByPlan(String planType, List<Enrollment> enrollments) {
		Set<String> enrollees = new HashSet<String>();
		for (Enrollment enrollment : enrollments) {
			if (planType.equals(enrollment.getInsuranceTypeLkp().getLookupValueCode())) {
				for (Enrollee enrollee : enrollment.getEnrollees()) {
					enrollees.add(enrollee.getExchgIndivIdentifier());
				}
			}
		}
		return enrollees;
	}

	private AgeOutNoticeDTO prepareNotice(SsapApplication application, DateTime runDate, DateTime expiryDate,
			Set<String> healthEnrollees, Set<String> dentalEnrollees, String healthPlanName, String dentalPlanName, boolean isChildOnlyDentalPlan) {
		String ssapJson = application.getApplicationData();
		String caseNumber = application.getCaseNumber();
		TypeToken<List<com.getinsured.iex.ssap.TaxHousehold>> token = new TypeToken<List<com.getinsured.iex.ssap.TaxHousehold>>() {
		};
		String taxhh = JsonPath.read(ssapJson, SSAP_TAX_HOUSEHOLD_PATH).toString();
		LOGGER.debug(" taxhh: " + taxhh);
		List<com.getinsured.iex.ssap.TaxHousehold> thhList = platformGson.fromJson(taxhh, token.getType());
		if (thhList == null || thhList.size() == 0) {
			throw new GIRuntimeException();
		}

		List<HouseholdMember> txMembers = thhList.get(0).getHouseholdMember();
		Optional<HouseholdMember> ptfApplicantOpt = txMembers.stream().filter(HouseholdMember::isPrimaryTaxFiler)
				.findFirst();
		if (!ptfApplicantOpt.isPresent()) {
			ptfApplicantOpt = txMembers.stream().filter(m -> 1 == m.getPersonId().intValue()).findFirst();
		}
		HouseholdMember ptfApplicant = ptfApplicantOpt.get();
		LOGGER.debug("house hold members " + txMembers);
		if (txMembers == null || txMembers.size() == 0) {
			throw new GIRuntimeException();
		}

		java.util.Collections.sort(txMembers, new HouseholdMemberComparator());

		List<BloodRelationship> bloodRelations = ptfApplicant.getBloodRelationship();

		if (ptfApplicant.getHouseholdContact() == null || ptfApplicant.getHouseholdContact().getHomeAddress() == null
				|| StringUtils.isEmpty(ptfApplicant.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode())
				|| StringUtils.isEmpty(ptfApplicant.getHouseholdContact().getHomeAddress().getPostalCode())) {
			throw new GIRuntimeException();
		}

		List<AgeOutDependant> healthDepandants = new ArrayList<AgeOutDependant>();
		List<AgeOutDependant> dentalDepandants = new ArrayList<AgeOutDependant>();
		String primaryFirstName = ptfApplicant.getName().getFirstName();
		String lastFirstName = ptfApplicant.getName().getLastName();
		for (HouseholdMember householdMember : txMembers) {
			BloodRelationship brss = bloodRelations.get(bloodRelations.indexOf(BloodRelationship
					.build(ptfApplicant.getPersonId().toString(), householdMember.getPersonId().toString())));
			DateTime memberDoB = new DateTime(householdMember.getDateOfBirth());
			LOGGER.debug(" memberDoB : " + memberDoB);
			DateTime ninteenYearDoB = memberDoB.plusYears(19);
			if (isChildOnlyDentalPlan && dentalEnrollees.contains(householdMember.getApplicantGuid()) && ninteenYearDoB.isBefore(expiryDate)
					&& ninteenYearDoB.isAfter(runDate)) {
				dentalDepandants.add(createAgeoutDependantsList(dentalPlanName, householdMember, ninteenYearDoB,
						DENTAL_AGE_OUT_AGE));
			}
			if (BLOOD_RELATIONS.containsKey((brss.getRelation()))) {
				/* health */
				DateTime twentySixYearDoB = memberDoB.plusYears(26);
				if (healthEnrollees.contains(householdMember.getApplicantGuid()) && twentySixYearDoB.isAfter(runDate)
						&& twentySixYearDoB.isBefore(expiryDate)) {
					healthDepandants.add(createAgeoutDependantsList(healthPlanName, householdMember, twentySixYearDoB,
							HEALTH_AGE_OUT_AGE));
				}

			}
		}
		DateTimeFormatter df = DateTimeFormat.forPattern("MMMM d, yyyy");
		String sEPendDate = expiryDate.dayOfMonth().withMaximumValue().toString(df);
		return populateAgeOutDto(healthDepandants, dentalDepandants, caseNumber, primaryFirstName, lastFirstName,
				getEmailId(application), application.getCmrHouseoldId(), sEPendDate);
	}

	private AgeOutNoticeDTO populateAgeOutDto(List<AgeOutDependant> healthDepandants,
			List<AgeOutDependant> dentalDepandants, String caseNumber, String primaryFirstName, String lastFirstName,
			String emailAddress, BigDecimal cmrId, String sEPendDate) {
		AgeOutNoticeDTO result = null;
		if (!(healthDepandants.isEmpty() && dentalDepandants.isEmpty())) {
			result = new AgeOutNoticeDTO();
			result.setApplicationID(caseNumber);
			result.setPrimaryFirstName(primaryFirstName);
			result.setPrimaryLastName(lastFirstName);
			result.setEmailId(emailAddress);
			result.setsEPendDate(sEPendDate);
			result.setHouseholdId(cmrId == null ? 0 : cmrId.intValue());
			if (!(healthDepandants.isEmpty())) {
				result.setHealthDependants(healthDepandants);
			}
			if (!(dentalDepandants.isEmpty())) {
				result.setDentalDependants(dentalDepandants);
			}
		}
		return result;
	}

	private AgeOutDependant createAgeoutDependantsList(String planName, HouseholdMember householdMember,
			DateTime currentDoB, String age) {
		AgeOutDependant healthDependant = new AgeOutDependant();
		healthDependant.setFirstName(
				householdMember.getName().getFirstName() != null ? householdMember.getName().getFirstName() : " ");
		healthDependant.setLastName(
				householdMember.getName().getLastName() != null ? householdMember.getName().getLastName() : " ");
		DateTime dropoutDate = currentDoB.plusMonths(1).withDayOfMonth(1).minusDays(1);
		healthDependant.setDropOutDate(DateUtil.dateToString(dropoutDate.toDate(), "MMMM dd, YYYY"));
		DateTime sepEndDate = dropoutDate.plusDays(60);
		healthDependant.setSepEndDate(DateUtil.dateToString(sepEndDate.toDate(), "MMMM dd, YYYY"));
		healthDependant.setPlanId(planName);
		healthDependant.setDropOutReasonText(
				householdMember.getName().getFirstName() + " will be " + age + " years old on %s",
				DateUtil.dateToString(currentDoB.toDate(), "MMMM dd, YYYY"));
		LOGGER.debug(" dependant  added : " + healthDependant.getFirstName() + " " + healthDependant.getLastName());
		return healthDependant;
	}

	private Location getLocation(PreferencesDTO preferencesDTO) {

		if (preferencesDTO.getLocationDto() == null) {
			return null;
		}

		Location returnLocation = new Location();
		returnLocation.setAddress1(preferencesDTO.getLocationDto().getAddressLine1());
		returnLocation.setAddress2(preferencesDTO.getLocationDto().getAddressLine2());
		returnLocation.setCity(preferencesDTO.getLocationDto().getCity());
		returnLocation.setCounty(preferencesDTO.getLocationDto().getCountyName());
		returnLocation.setCountycode(preferencesDTO.getLocationDto().getCountyCode());
		returnLocation.setState(preferencesDTO.getLocationDto().getState());
		returnLocation.setZip(preferencesDTO.getLocationDto().getZipcode());

		return returnLocation;
	}

	private String getEmailId(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();
		for (SsapApplicant ssapApplicant : applicants) {
			if(GhixBatchConstants.isMnCall() && ssapApplicant.isPrimaryTaxFiler()) {
				return ssapApplicant.getEmailAddress();
			}else if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getEmailAddress();
			}
		}
		return null;
	}

	private boolean getBooleanVal(String prop, String config) {
		return config.equalsIgnoreCase(prop) || BOTH.equalsIgnoreCase(config);
	}
	
	private boolean isChildOnlyPlan(Enrollment enrollment, Long coverageYear) {
		if (null != enrollment) {
			Map<String, Object> requestMap = new HashMap<>();
			requestMap.put("hiosPlanId", enrollment.getCMSPlanID());
			requestMap.put("planYear", coverageYear);
			String request = platformGson.toJson(requestMap);
			PediatricPlanResponseDTO pediatricPlanResponseDTO = ghixRestTemplate.postForObject(
					GhixEndPoints.PlanMgmtEndPoints.GET_PEDIATRIC_PLAN_INFO, request, PediatricPlanResponseDTO.class);
			return pediatricPlanResponseDTO.isChildOnly();
		}
		return false;
	}
}
