import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Components

import Form from './Form';
import NamesOnSsnCardInputs from './pageIncludes/common/names/NamesOnSsnCardInputs';
import PageHeader from '../headers/PageHeader';
import UniversalInput from '../../lib/UniversalInput';

// ActionCreators

import { changeData } from '../../actions/actionCreators';

// Utils

import cloneObject from '../../utils/cloneObject';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Constants

import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import EMPTY_OBJECTS from '../../constants/emptyObjects';
import INPUT_TYPES from '../../constants/inputTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';



/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Gender and SSN information' Page
 *
 */
const Page2_08 = props => {

    const {
        data,
        householdMembers
    } = props;

    const {
        hhmIndex : member
    } = props;

    const onFieldChange_gender = newData => {
        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member] = newData;
        props.actions.changeData(newState);
    };

    /**
     *
     * @param {Object} newData - updated <householdMember> object
     *
     *  Updating some props in JSON.
     *  If User has changed <nameSameOnSSNCardIndicator> to <true> then rewrite name inputs
     *  in <socialSecurityCard> object the same as they are in <name> object
     */
    const onFieldChange = newData => {
        if(!newData.socialSecurityCard.socialSecurityCardHolderIndicator) {
            newData.socialSecurityCard = {
                ...cloneObject(EMPTY_OBJECTS.SOCIAL_SECURITY_CARD),
                socialSecurityCardHolderIndicator: false,
                reasonableExplanationForNoSSN: newData.socialSecurityCard.reasonableExplanationForNoSSN
            };
        } else if(newData.socialSecurityCard.nameSameOnSSNCardIndicator) {
            const {
                name: {
                    firstName,
                    middleName,
                    lastName,
                    suffix
                }
            } = householdMembers[member];

            newData.socialSecurityCard = {
                ...newData.socialSecurityCard,
                reasonableExplanationForNoSSN: null,
                firstNameOnSSNCard: firstName,
                middleNameOnSSNCard: middleName,
                lastNameOnSSNCard: lastName,
                suffixOnSSNCard: suffix,
            };
        }

        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member] = newData;
        props.actions.changeData(newState);
    };

    const handleSsnChange = newData => {

        const newState = { ...data };
        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .socialSecurityCard
            .socialSecurityNumber = newData.socialSecurityCard.socialSecurityNumber;
        props.actions.changeData(newState);
    };

    const currentData = householdMembers[member];

    const {
        gender,
        socialSecurityCard: {
            socialSecurityCardHolderIndicator,
            socialSecurityNumber,
            nameSameOnSSNCardIndicator,
            firstNameOnSSNCard,
            middleNameOnSSNCard,
            lastNameOnSSNCard,
            suffixOnSSNCard,
            reasonableExplanationForNoSSN
        },
        name
    } = currentData;

    const getValuesToMatch = (() => {
        return new Set(
            householdMembers
                .map(hhm => hhm.socialSecurityCard.socialSecurityNumber)
                .filter((ssn, idx) => idx !== member && ssn)
        );
    })();

    return (
        <Fragment>
            <div id="fhGetReady" className="fade-in">
                <Form>
                    <div className="subsection">
                        <UniversalInput
                            legend={{
                                legendText:
                                    <span>
                                        <strong>
                                            {`${normalizeNameOfPerson(name)}'s Gender`}
                                        </strong>
                                    </span>
                            }}
                            label={{
                                text: DROPDOWN_OPTIONS.GENDER,
                                labelClass: ['usa-width-one-whole', 'layout--02'],
                                ulClass: ['usa-width-one-whole'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                name: 'gender',
                                value: gender,
                                options: DROPDOWN_OPTIONS.GENDER,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: currentData,
                                fields: []

                            }}
                            inputActions={{
                                onFieldChange: onFieldChange_gender
                            }}
                            errorClasses={['margin-l-0']}
                        />
                    </div>
                    <div className="subsection">
                        <UniversalInput
                            legend={{
                                legendText:
                                    <span>
                                        Does &nbsp;
                                        <strong>
                                            {normalizeNameOfPerson(name)} &nbsp;
                                        </strong>
                                        have a Social Security Number?
                                    </span>
                            }}
                            label={{
                                text: '',
                                labelClass: ['usa-width-one-whole', 'layout--02'],
                                ulClass: ['usa-width-one-whole'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                name: 'socialSecurityCardHolderIndicator',
                                value: socialSecurityCardHolderIndicator,
                                options: DROPDOWN_OPTIONS.YES_NO,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: currentData,
                                fields: ['socialSecurityCard']

                            }}
                            inputActions={{
                                onFieldChange
                            }}
                            errorClasses={['margin-l-0']}
                        />
                        <div className="usa-width-one-whole">
                            <div className="usa-alert usa-alert-info alert--info">
                                <div className="usa-alert-body" >
                                    <p className="usa-alert-text" >
                                        If &nbsp;
                                        <strong>
                                            {normalizeNameOfPerson(name)} &nbsp;
                                        </strong>
                                        is applying for health insurance, you must provide a Social Security Number (SSN) if available. If &nbsp;
                                        <strong>
                                            {normalizeNameOfPerson(name)} &nbsp;
                                        </strong>
                                        {
                                            `doesn't have an SSN please visit www.ssa.gov/ssnumber to apply. 
                                            An SSN can also help with enrolling in a health plan if`

                                        } &nbsp;
                                        <strong>
                                            {normalizeNameOfPerson(name)} &nbsp;
                                        </strong>
                                        is eligible for one.
                                    </p>
                                </div>
                            </div>
                        </div>

                        {/***********************************************************
                         Begin - If User is #socialSecurityCardHolderIndicator (Yes)
                         ************************************************************/}

                        {
                            socialSecurityCardHolderIndicator &&

                            <Fragment>
                                <UniversalInput
                                    label={{
                                        text: 'Social Security Number',
                                        labelClasses: []
                                    }}
                                    inputData={{
                                        isInputRequired: true,
                                        type: INPUT_TYPES.SSN_INPUT,
                                        name: 'socialSecurityNumber',
                                        helperClasses: [],
                                        value: socialSecurityNumber,
                                        placeholder: 'xxx-xx-xxxx',
                                        validationType: VALIDATION_TYPES.SSN,
                                        valuesToMatch: getValuesToMatch,
                                        currentData: currentData,
                                        fields: ['socialSecurityCard']
                                    }}
                                    inputActions={{
                                        onFieldChange: handleSsnChange
                                    }}
                                    indexInArray={member}
                                />
                                <UniversalInput
                                    legend={{
                                        legendText:
                                            <span>
                                                Is &nbsp;
                                                <strong>
                                                    {normalizeNameOfPerson(name)} &nbsp;
                                                </strong>
                                                {`the same name that appears on ${gender === 'male' ? 'his' : 'her'} Social Security card?`}
                                            </span>
                                    }}
                                    label={{
                                        text: DROPDOWN_OPTIONS.YES_NO,
                                        labelClass: ['usa-width-one-whole', 'layout--02', 'margin-t-20'],
                                        ulClass: ['usa-width-one-whole'],
                                        showRequiredIcon: true
                                    }}
                                    inputData={{
                                        isInputRequired: true,
                                        type: INPUT_TYPES.RADIO_BUTTONS,
                                        name: 'nameSameOnSSNCardIndicator',
                                        value: nameSameOnSSNCardIndicator,
                                        options: DROPDOWN_OPTIONS.YES_NO,
                                        validationType: VALIDATION_TYPES.CHOSEN,
                                        currentData: currentData,
                                        fields: ['socialSecurityCard']

                                    }}
                                    inputActions={{
                                        onFieldChange
                                    }}
                                />

                                {
                                    nameSameOnSSNCardIndicator === false &&

                                            <Fragment>
                                                <PageHeader
                                                    text={
                                                        <span>
                                                        Enter the same name as shown on&nbsp;
                                                            {normalizeNameOfPerson(name)}&nbsp;
                                                        Social Security Card
                                                        </span>
                                                    }
                                                    headerClassName={'usa-heading-alt margin-t-20'}
                                                />
                                                <NamesOnSsnCardInputs
                                                    {...{
                                                        firstNameOnSSNCard,
                                                        middleNameOnSSNCard,
                                                        lastNameOnSSNCard,
                                                        suffixOnSSNCard,
                                                        currentData,
                                                        inputActions: {
                                                            onFieldChange
                                                        }
                                                    }}
                                                />
                                            </Fragment>
                                }
                            </Fragment>
                        }
                    </div>
                    {/***********************************************************
                     End - If User is #socialSecurityCardHolderIndicator (Yes)
                     ************************************************************/}

                    {/***********************************************************
                     Begin - If User is #socialSecurityCardHolderIndicator (No)
                     ************************************************************/}

                    {
                        socialSecurityCardHolderIndicator === false &&

                        <Fragment>
                            <div className="subsection">
                                <PageHeader
                                    text='If no Social Security Number is available please select from the following explanations'
                                    headerClassName={'usa-heading-alt h4-not-highlighted'}
                                />
                                <UniversalInput
                                    label={{
                                        text: 'Reason'
                                    }}
                                    inputData={{
                                        isInputRequired: true,
                                        type: INPUT_TYPES.DROPDOWN,
                                        name: 'reasonableExplanationForNoSSN',
                                        value: reasonableExplanationForNoSSN,
                                        options: DROPDOWN_OPTIONS.REASONABLE_EXPLANATIONS_FOR_NO_SSN,
                                        validationType: VALIDATION_TYPES.CHOSEN,
                                        currentData: currentData,
                                        fields: ['socialSecurityCard']
                                    }}
                                    inputActions={{
                                        onFieldChange
                                    }}
                                />
                            </div>
                        </Fragment>
                    }

                    {/***********************************************************
                     End - If User is #socialSecurityCardHolderIndicator (No)
                     ************************************************************/}
                </Form>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page2_08.defaultProps = {
    name: 'Page2_08'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_08);
