package com.getinsured.hix.admin.service.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.admin.service.AdminService;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.planmgmt.service.jpa.IssuerServiceImpl;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;


@Service("adminService")
@Repository
@Transactional
public class AdminServiceImpl implements AdminService {
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerServiceImpl.class);
	@Autowired private UserService userService;
	
	public boolean isAdminLoggedIn() {
		AccountUser user = null;
        try{
           user = userService.getLoggedInUser();
           if(userService.isAdmin(user)){	
        	   return true;
           }
        }catch(InvalidUserException ex){
        	LOGGER.error("Admin not logged in");
        }
       	return false;
	}
}
