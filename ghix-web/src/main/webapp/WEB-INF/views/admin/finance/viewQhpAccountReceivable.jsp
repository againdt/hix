<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>


<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>

<%@ page isELIgnored="false"%>


<div class="gutter10">
<div class="row-fluid">
	<ul class="page-breadcrumb">
	<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="label.back"/></a></li>
	<li class="active">QHP Account Receivable</li>
	</ul>
			<h1>View QHP Account Receivable <a href="#" rel="tooltip" data-placement="top"
			data-original-title="You can view all payments done by Employers against selected Plans." 
			class="info"><i class="icon-question-sign"></i></a></h1>


</div>


<div class="row-fluid">
	<div class="span3" id="sidebar">
			<div class="header">
				<h4 class="margin0">Refine Results</h4>
			</div>
				<div class="graybg">
					<form class="form-vertical gutter10" id="frminvoiceinfolist"
			name="frminvoiceinfolist" action="qhpaccountreceiveable" method="POST">
			<df:csrfToken/>
						<div class="control-group">
							<label class="control-label" for="employerName">Employer Name</label>
							<div class="controls">
								<input class="span12" placeholder="Name" name="employerName" id="employerName" value="${searchCriteria.employerName}">
							</div>
						</div>
						<div class="txt-center">
							<input type="submit" name="submitBtn" id="submitBtn" Value="Go" title="Go" class="btn input-medium">
						</div>
					</form>
			</div>
	</div>

	<div class="span9" id="rightpanel">
		<form id="frmqhpaccountreceiveablelist"
			name="frmqhpaccountreceiveablelist" action="qhpaccountreceiveable" method="POST">
			<c:choose>
			 <c:when test="${not empty ErrorMsg}">
                <h4 class="alert alert-error h4">${ErrorMsg}</h4>
                </c:when>
				<c:when test="${resultSize >= 1}">
					<c:choose>
						<c:when test="${fn:length(receiveableList) > 0}">
								<table class="table table-striped">
									<thead>
										<tr class="header">
											<!--  span9 header -->
											<!-- <th><input type="checkbox"> </th> -->
											<th class="sortable" scope="col"><dl:sort title="Employer Name" sortBy="employerName"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Plan Type" sortBy="planType"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>		
											<th class="sortable" scope="col"><dl:sort title="Net Amount" sortBy="netAmount"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Payment Received" sortBy="paymentReceived"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Pending Amount" sortBy="pendingAmount"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
										</tr>
									</thead>
									
									<c:forEach items="${receiveableList}" var="receiveable" varStatus="vs">
									<tr>
											<!-- <td><input type="checkbox"> </td> -->
											<td>${receiveable.employerName}</td>
											<td>${receiveable.planType}</td>
											<td>
											<fmt:setLocale value="en_US" scope="session"/>
											<fmt:formatNumber value="${receiveable.netAmount}" type="currency"/>
											</td>
											<td>
											<fmt:setLocale value="en_US" scope="session"/>
											<fmt:formatNumber value="${receiveable.paymentReceived}" type="currency"/>
											</td>
											<td>
											<fmt:setLocale value="en_US" scope="session"/>
											<fmt:formatNumber value="${receiveable.pendingAmount}" type="currency"/>
											</td>
										</tr>				
									</c:forEach>
								</table>
                                <div class="pagination">
									<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}"/>
								</div>
						</c:when>
					</c:choose>
				</c:when>
				<c:otherwise>
					<h4 class="alert alert-info">
						<spring:message code='label.norecords' />
					</h4>
				</c:otherwise>
			</c:choose>
		</form>
		<div>
		<c:if test="${fn:length(receiveableList) gt 0}">
         <button class="btn offset2" data-dismiss="modal" id="downloadQhpReceiveable">Download</button>
        </c:if>
		</div>
	</div>
</div>
</div>


<div class="row-fluid">
	
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">

				<p>The prototype showcases three scenarios (A, B and C)
					dependant on a particular Employer's eligibility to use the SHOP
					Exchange.</p>
			</div>
		</div>
	</div>

</div>
<!-- row-fluid -->
	
<script>
$('.info').tooltip();

$('#downloadQhpReceiveable').click(function(){
	var params ="employerName="+$("#employerName").val();
	window.location.href =  "${pageContext.request.contextPath}"+'/admin/downloadQhpReceiveable'+"?"+params;
});
</script>
