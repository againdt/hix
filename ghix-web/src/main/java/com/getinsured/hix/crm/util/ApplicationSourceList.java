package com.getinsured.hix.crm.util;

import java.util.LinkedHashMap;

public class ApplicationSourceList {
	
	public static LinkedHashMap<String, String> s_validApplicationSources = new LinkedHashMap<String, String>();
	
	static
	{
		//Ordering is important - do not change the ordering..
		
		s_validApplicationSources.put("ON", "Online Application	Non-Financial Application originating at YHI");
		s_validApplicationSources.put("WI", "Paper / Walk in Application");
		s_validApplicationSources.put("RF", "Referrals from State	Financial Application via Account Transfer from DHW ");
		s_validApplicationSources.put("CN", "Conversion Non-Financial application via passive enrollment ");
		s_validApplicationSources.put("PH", "Phone Applicant talking to CSR over phone");
		
		
	}

}
