<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datepicker.css"/>" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker-new.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-template.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-address-util.js" />"></script>


<script type="text/javascript">
$(document).ready(function(){
	var isNewHire = '${newHire}';
	if(isNewHire == 'Y'){
		$('#coverageEligibilityDate').val('');
		$('#coverageEligibilityDt').val('');
		$('#addNewHireEmployeeModal').modal('show');
	}
});

jQuery.validator.addMethod("namecheck", function(value, element, param) {
  	if(namecheck(value) == false){
	  	return false;	
	}
	return true;
});

jQuery.validator.addMethod("phonecheck", function(value, element, param) {
	 phone1 = $("#phone1").val(); 
 	 phone2 = $("#phone2").val(); 
 	 phone3 = $("#phone3").val(); 
   	var intPhone1= parseInt( phone1);
  	if( 
  			(phone1 == "" || phone2 == "" || phone3 == "")  || 
  			(isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3)) ||  
  			(phone1.length != 3 || phone2.length != 3 || phone3.length != 4)){ 
  		return false; 
  	}else if(intPhone1<100){
  		return false;
  	} 
  	
	var phonenumber = $("#phone1").val() + $("#phone2").val() + $("#phone3").val();
		
	var phoneno = /^\d{10}$/;
	  if(phonenumber.match(phoneno))      {
        return true;
      }
	return false;
});


jQuery.validator.addMethod("ssn1check", function(value, element, param) {
	ssn1 = $("#ssn1").val();
	return areaCodeCheck(ssn1); 
		
	 });
	 
	 jQuery.validator.addMethod("invalidssn1", function(value, element, param) {
		    ssn1 = $("#ssn1").val();
			ssn2 = $("#ssn2").val();
			ssn3 = $("#ssn3").val(); 
			return notAllowedSSN1(ssn1,ssn2,ssn3);
		 });
	 jQuery.validator.addMethod("invalidssn2", function(value, element, param) {
		    ssn1 = $("#ssn1").val();
			ssn2 = $("#ssn2").val();
			ssn3 = $("#ssn3").val(); 
			return notAllowedSSN2(ssn1,ssn2,ssn3);
		 });


	jQuery.validator.addMethod("ssncheck", function(value, element, param) {
		ssn1 = $("#ssn1").val();
		ssn2 = $("#ssn2").val();
		ssn3 = $("#ssn3").val();
	 	return validateSSN(ssn1,ssn2,ssn3);
	});

		jQuery.validator.addMethod("validateTaxId", function(value, element, param) {
		ssn1 = $("#ssn1").val();
		ssn2 = $("#ssn2").val();
		ssn3 = $("#ssn3").val(); 
		currentSSN=ssn1+'-'+ssn2+'-'+ssn3;
		var csrfParameter = "";
		csrfParameter ='<c:url value="">
					<c:param name="${df:csrfTokenParameter()}"> 
						<df:csrfToken plainToken="true" />
					</c:param> 
				</c:url>';
				
		return isUniqueSSN(currentSSN, 0,0,'employee',csrfParameter);
		
		});
		
jQuery.validator.addMethod("zipcheck", function(value, element, param) {
  	elementId = element.id; 
  	var numzip = new Number($("#"+elementId).val());
    var filter = /^[0-9]+$/;
	if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() )|| numzip<=0 || (!filter.test($("#"+elementId).val()))){
  		return false;	
  	}return true;
});

function submitLocation(){
	var subUrl = '<c:url value="/shop/employer/manage/addWorksites">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';

	$.ajax(
			{
        		type: "POST",
       			url: subUrl,
        		data: $("#frmworksites").serialize(),
        		dataType:'json',
        		success: function(response,xhr){
			        if(isInvalidCSRFToken(xhr))
			          return;
        			populateLocations(response);
        			application.remove_modal();
        		},
       			error: function(e){
        			alert("<spring:message code='label.failedtoaddaddr' javaScriptEscape='true'/>");
        		}
        });
}

function validateLocation(){
	if( $("#frmworksites").validate().form() ) {
		//validateAddressWS();
		submitLocation();
	}
}

function submitFrmADDEmployee(newHire){
	/*to check the text for address line 2 for IE browsers*/
	$("input[id^=address2_]").each(function(){
		if($(this).val() === "Address Line 2"){
			$(this).val("");
		}
	});	
	
	if(newHire == 'Y'){
		if( $("#optionsCheckbox").length > 0 && $("#optionsCheckbox:checked").length < 1){
			 alert("Please read and check the confirmation statement.");
			 return false;
		}else{
			$('input:button').attr("disabled", true);
			$("#frmaddemployee").submit();
		}
	}else{
		$('input:button').attr("disabled", true);
		$("#frmaddemployee").submit();
	}
	
}

function showEmployeeForm(){
	$('#addEmployeeDiv').show();
	$('#confirmDiv').hide();
}

function changeEligibilityDate(){
	var isNewHire = '${newHire}';
	if(isNewHire == 'Y'){
		$('#changeEliDate').val('Y');
		$('#coverageEligibilityDate').val('');
		$('#coverageEligibilityDt').val('');
		$('#addNewHireEmployeeModal').modal('show');
	}
}


function validateForm(btnValue){
	var newHire = '${newHire}';
	$("#clickedBtnname").val(btnValue);
	$("#frmaddemployee").valid();	
	if( $("#frmaddemployee").validate().form() ) {
		var name = $("#firstName").val()+ " " +$("#lastName").val();
		var ssn = $("#ssn1").val()+ "-" +$("#ssn2").val()+ "-" +$("#ssn3").val();
	
		var phone = $("#phone1").val()+ "-" +$("#phone2").val()+ "-" +$("#phone3").val();
		
		$("#name").val(name);
		$('#ssn').val(ssn);
	
		$("#contactNumber").val(phone);
		//validateHomeAddressWS();
		
		if(newHire=='Y'){
			$('#addEmployeeDiv').hide();
			getHireDates($('#employmentDate').val());
			$('#confirmDiv').show();
		}
		else{
			submitFrmADDEmployee(newHire);	
		}
	}
}

jQuery.validator.addMethod("dobcheck", function(value, element, param) {
	var today=new Date();
	var dob = $("#dob").val();
    dob_arr = dob.split(/\//);
	dob_mm = dob_arr[0]; dob_dd = dob_arr[1]; dob_yy = dob_arr[2];
	var birthDate=new Date();
	birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
	if( (today.getFullYear() - 100) >  birthDate.getFullYear() ) { return false; }
	if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) { return false; }
	if(today.getTime() <= birthDate.getTime()){ return false; }
	
	return true;
});

/* function postPopulateIndex(indexValue, zipCodeValue){
	console.log(indexValue, zipCodeValue);
	getCountyList( '_'+indexValue,zipCodeValue, '' );
} */

function getCountyList(eIndex, zip, county) {
	var subUrl = '<c:url value="/shop/employer/application/addCounties">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
	$.ajax({
		type : "POST",
		url : subUrl,
		data : {zipCode : zip}, 
		dataType:'json',
		success : function(response,xhr) {
	        if(isInvalidCSRFToken(xhr))
	          return;
			populateCounties(response, eIndex, county);
		},error : function(e) {
			alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
		}
	});
}

function populateCounties(response, eIndex,county){
	$('#county'+eIndex).html('');
	var optionsstring = '<option value="">Select County...</option>';
	var i =0;
	$.each(response, function(key, value) {
		var optionVal = key+'#'+value;
		var selected = (county == key) ? 'selected' : '';
		var options = '<option value="'+optionVal+'" '+ selected +'>'+ key +'</option>';
		optionsstring = optionsstring + options;
		i++;
	});
	$('#county'+eIndex).html(optionsstring);
	
}


$('#email').keypress(function(){
	var emailFieldLen= $('#email').val().length+1;
	$('#email').attr('size', emailFieldLen);
});

var month=new Array();
month[0]="January"; month[1]="February"; month[2]="March"; month[3]="April"; month[4]="May"; month[5]="June"; 
month[6]="July"; month[7]="August"; month[8]="September"; month[9]="October"; month[10]="November"; month[11]="December";

$(function() {
	$('#addEmpCancel').click(function() {
		if($('#changeEliDate').val() == 'N'){
			window.location.href = '../dashboard';
		}
	});
	
	$('#closebtn').click(function() {
		if($('#changeEliDate').val() == 'N'){
			window.location.href = '../dashboard';
		}
	});
	
	$('#addNewHireEmployeeButton').click(function() {
		if( $("#frmNewHireEmployee").validate().form() ) {
			$('#addNewHireEmployeeModal').modal('hide');
			var dt = $('#coverageEligibilityDate').val();
			var objDate = new Date(dt);
			var formatedcovdate = month[objDate.getMonth()] + " " + objDate.getDate() +", " + objDate.getFullYear();
			$('#employementDateVal').text(formatedcovdate);
			$('#employmentDate').val($('#coverageEligibilityDate').val());
			$('#coverageEligibilityDt').val($('#coverageEligibilityDate').val());
			if($('#changeEliDate').val() == 'Y'){
				getHireDates($('#employmentDate').val());
				$('#changeEliDate').val('N');
			}
		}else{
			return false;
		}
	});
});
</script>
<style>
.popover-title {
	font-size: 10px;
	line-height: 12px;
}
.popover-content p label{
	font-size: 10px;
	line-height: 10px;
}
.popover-content p input{
	margin-top: 0px
}
.popover.bottom .arrow:after {
	left: 0;
}

.Divcovdate{
	margin-top:5px;
}
p input[type="checkbox"] {
	margin: 0 5px 0 0;
}
</style>



<div id="addNewHireEmployeeModal" class="modal hide fade" tabindex="-1"  data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="addNewHireEmployee" aria-hidden="true">
<input  type="hidden" name="changeEliDate" id="changeEliDate" value="N">
	<div class="modal-header">
		<input type="button" class="close" id="closebtn" data-dismiss="modal" value='x' aria-hidden="true"/>
		<h3 id="addNewHireEmpLabel"><spring:message  code='label.coveragestartdate'/></h3>
	</div>
	<form id="frmNewHireEmployee" name="frmNewHireEmployee">
			<df:csrfToken/>
		<div class="modal-body">
			<span id="terminateEmpBodySpan">
			Select the date you'd like your new employee's coverage to start. If your company has a waiting period, you may need to add the employee at a later date.
			</span><br/><br/>
			<div class="controls">
				<div >
				
	         			<select class="input-large"  name="coverageEligibilityDate" id="coverageEligibilityDate">
						<c:forEach var="eachDate" items="${empCoverageDates}" varStatus="loop">
							<fmt:formatDate pattern="MM/dd/yyyy" value="${eachDate}" var="optionValueDate"/>
							<fmt:formatDate type="date" value="${eachDate}" var="optionDisplayDate"/>	         			
	         				<option value="${optionValueDate}">${optionDisplayDate}</option>
	         			</c:forEach>
	         			</select>
	        		 	
	        	</div>
	        	<span id="coverageEligibilityDate_error"></span>
			</div>
		</div>
	</form>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true" id="addEmpCancel"><spring:message  code='label.cancel'/></button>
		<button class="btn btn-primary" id="addNewHireEmployeeButton">Continue</button>
	</div>
</div>

<div class="gutter10"  id="addEmployeeDiv">
<div class="row-fluid">
<ul class="breadcrumb txt-left">
  <li><a href="<c:url value="/shop/employer/manage/list" />" > &lt; Back</a> <span class="divider">|</span></li>
  <li><a href="<c:url value="/shop/employer/" />">Employees</a> <span class="divider">/</span></li>
  <li><a href="<c:url value="/shop/employer/manage/list" />">Manage List </a><span class="divider">/</span></li>
  <li class="active">Add Employee</li>
</ul>
</div>
<!--titlebar-->
<div class="row-fluid">
      <div class="span9">
        <h3 id="skip"><spring:message  code="label.addEmployee"/></h3>
      </div>
</div>
<div class="row-fluid">
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        <h4 class="graydrkbg"> <spring:message  code="label.youshouldknow"/> </h4>
        <div class="gutter10">
          <p><spring:message  code="label.covinfoaddemp"/> <c:out value="${exchangeName}"/></p>         
        </div>
      </div>
      
      
      <!--sidebar-->
      <div class="span9" id="rightpanel">
      <div class="header">
		<h4><spring:message  code="label.employeeInfo"/></h4>
		</div>
		<!--titlebar-->
		<div class="gutter10">
			<form class="form-horizontal" id="frmaddemployee" name="frmaddemployee" action="addemployeeSubmit" method="post" >
				<df:csrfToken/>
				<input type="hidden" id="name" name="name" value=""/>
				<input type="hidden" name="employeeDetails[0].ssn" id="ssn"/>
				<input type="hidden" name="employeeDetails[0].contactNumber" id="contactNumber"/> 
				<input type="hidden" name="employeeDetails[0].type" id="type" value="EMPLOYEE" />
				<input type="hidden" id="id" name="id" value="${employee_id}"/>
				<input type="hidden" id="clickedBtnname" name="clickedBtnname"/>
				<input type="hidden" id="employer_id" name="employer.id" value="${employer_id}"/>
				<input type="hidden" id="newHire" name="newHire" value="${newHire}"/>
				<input type="hidden" id="coverageEligibilityDt" name="coverageEligibilityDt" value=""/>
				
								
					<h4 class="lightgray"><spring:message  code="label.Employment"/></h4>
					<div class="control-group">
						<label class="control-label" for="firstName"><spring:message  code='label.emplFirstName'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" id="firstName" name="employeeDetails[0].firstName"/>
							<div id="firstName_error"></div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="lastName"><spring:message  code='label.emplLastName'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" id="lastName" name="employeeDetails[0].lastName"/>
							<span id="lastName_error"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="location"><spring:message  code='label.employerworksiteaddr'/> </label>
						<div class="controls">
							<select name="employerLocation.id" id="location" class="span8"></select>
							<%-- <a href="#worksite" id="worksiteLink" class="gutter10" data-toggle="modal"><span><i class="icon-plus-sign"></i> <spring:message  code='label.employerworksiteaddr'/></span></a> --%>
							<small>
								<a href="javascript:void(0)" id="worksite_ModalLink" role="button" class="" data-toggle="modal">
								<i class="icon-plus-sign"></i>
								<spring:message  code="label.employeraddworksiteaddr"/></a>
							</small>
						</div>
					</div>
			
					<c:if test="${newHire=='Y'}">
					<div class="control-group">
						<label class="control-label" for="employmentDate">
						     <spring:message  code='label.coveragestartdate'/>
  					    </label>
						<div class="controls Divcovdate">
							<input type="hidden" name="employmentDate" id="employmentDate"/>
							<b><div style='border:none;font-weight:bold' id="employementDateVal"></div></b>
						</div>
					</div>
					</c:if>
				
				<h4 class="lightgray"><spring:message  code='label.emprroasteraddemph7'/></h4>
				<div class="control-group">
					<label class="control-label" for="dob"><spring:message  code='label.dob'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<div class="input-append date date-picker" data-date="" data-date-format="mm/dd/yyyy">
                 			<input class="span10" type="text" name="employeeDetails[0].dob" id="dob">
                  			<span class="add-on"><i class="icon-calendar"></i></span>
                  		</div>
						<span id="dob_error"></span>
					</div>
				</div>
				
					<label class="control-label"><spring:message  code='label.taxidnumber'/>
					<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> 
					<a href="javascript:void(0)" tabindex="-1" rel="tooltip" data-placement="top" data-original-title="<spring:message  code='label.taxidnumbertooltip'/>" 
								class="info"><i class="icon-question-sign"></i>
							</a>
						</label>
					<div class="control-group">
						<div class="controls">
							<label for="ssn1" class="hidden"><spring:message  code='label.taxidnumber'/> <spring:message  code='label.firstSSN'/> <spring:message  code='label.required'/> <a href="javascript:void(0)" tabindex="-1" rel="tooltip" data-placement="top" data-original-title="<spring:message  code='label.taxidnumbertooltip'/>" class="info"><i class="icon-question-sign"></i></a></label>
							<input type="text" class="span2 inline" name="ssn1" id="ssn1"  maxlength="3"/>
							
							<label for="ssn2" class="hidden"><spring:message  code='label.taxidnumber'/> <spring:message  code='label.secondSSN'/> <spring:message  code='label.required'/> </label>
							<input type="text" class="span1 inline" name="ssn2" id="ssn2"  maxlength="2"/>
							
							<label for="ssn3" class="hidden"><spring:message  code='label.taxidnumber'/> <spring:message  code='label.thirdSSN'/> <spring:message  code='label.required'/> </label>
							<input type="text" class="span2 inline" name="ssn3" id="ssn3"  maxlength="4"/>
								<span id="ssn1_error"></span>
								<span id="ssn3_error"></span>
						</div>
					</div>
				
				<c:set var="tobaccoDisplay" value="" />
				<c:if test="${displayTobaccoUse == 'HIDE'}">
				 <c:set var="tobaccoDisplay" value="style=\"display:none;\"" />
				</c:if>
				<div class="control-group" ${tobaccoDisplay}>
					<fieldset>
						<legend class="aria-hidden"><spring:message  code='label.tobacoUser'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></legend>
						<label class="control-label"><spring:message  code='label.tobacoUser'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<label class="radio inline"> <input type="radio" name="employeeDetails[0].smoker" id="smoker" value="YES"> Yes </label>
							<label class="radio inline"> <input type="radio" name="employeeDetails[0].smoker" id="smoker" value="NO"> No </label>
							<span id="smoker_error"></span>
						</div>
					</fieldset>
				</div>
				<c:set var="nativeAmrDisplay" value="" />
				<c:if test="${displayNativeAmerican == 'HIDE'}">
				 <c:set var="nativeAmrDisplay" value="style=\"display:none;\"" />
				</c:if>
				<div class="control-group" ${nativeAmrDisplay}>
					<fieldset>
						<legend class="aria-hidden"><spring:message  code='label.nativeAmerican'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></legend>
						<label class="control-label"><spring:message  code='label.nativeAmerican'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<label class="radio inline"> <input type="radio"
								name="employeeDetails[0].nativeAmr"	id="nativeAmr" value="YES">  Yes 
							</label> <label class="radio inline"> <input type="radio"
								name="employeeDetails[0].nativeAmr"	id="nativeAmr" value="NO">  No 
							</label> <span id="nativeAmr_error"></span>
						</div>
					</fieldset>
				</div>
				
				
				<h4 class="lightgray"><spring:message  code='label.contact'/></h4>
				<div class="control-group">
					<label class="control-label" for="email"><spring:message  code='label.emplEmail'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" name="employeeDetails[0].email" id="email"/> <!-- class="adjWidthEmail" was removed from input label due to the HIX-24728 -->
						<span id="email_error"></span>
					</div>
				</div>
				
					<label class="control-label"><spring:message  code='label.homePhone'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<div class="control-group">
					
					<div class="controls">
						<input type="text" class="span2 inline" name="phone1" id="phone1" maxlength="3"/>
						
						<input type="text" class="span2 inline" name="phone2" id="phone2" maxlength="3"/>
						
						<input type="text" class="span2 inline" name="phone3" id="phone3" maxlength="4"/>
						<span id="phone3_error"></span>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="address1_Home"><spring:message  code='label.homeAddress'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<input type="hidden" name="address2_Home" id="address2_Home" >
							<input type="hidden" name="lat" id="lat_Home" value="0.0"  />
							<input type="hidden" name="lon" id="lon_Home" value="0.0"  />	
							<input type="hidden" name="rdi" id="rdi_Home" value="" />
							<input type="hidden" name="employeeDetails[0].location.countycode" id="countycode_Home" value="" />
					<div class="controls">
						<input type="text" name="employeeDetails[0].location.address1" id="address1_Home"/>
						<span id="address1_Home_error"></span>
						<input type="hidden" id="address1_Home_hidden" name="address1_Home_hidden" >
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="city_Home"><spring:message  code='label.emplCity'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" name="employeeDetails[0].location.city" id="city_Home"/>
						<span id="city_Home_error"></span>
						<input type="hidden" id="city_Home_hidden" name="city_Home_hidden">
					</div>	
				</div>
				<div class="control-group">
					<label class="control-label" for="state_Home"><spring:message  code='label.emplState'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<select class="input-medium" name="employeeDetails[0].location.state" id="state_Home">
							 <option value="">Select</option>
							 <c:forEach var="state" items="${statelist}">
						    	<option id="${state.code}" <c:if test="${state.code == defaultStateCode}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
							</c:forEach>
						</select>
						<span id="state_Home_error"></span>
						<input type="hidden" id="state_Home_hidden" name="state_Home_hidden" >
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="zip_Home"><spring:message  code='label.zip'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" name="employeeDetails[0].location.zip" id="zip_Home" class="input-small zipCode" maxlength="5"/>
						<span id="zip_Home_error"></span>
						<input type="hidden" value="0.0" id="zip_Home_hidden" name="zip_Home_hidden" />
					</div>
				</div>
				
				<div class="control-group" id="county_Home_div">
					<label for="county_Home" class="control-label required"><spring:message  code='label.county'/><img  src="<c:url  value="/resources/images/requiredAsterix.png"  />"  width="10"  height="10"  alt="Required!"  /></label>
					<div class="controls">
						<select class="input-large" name="employeeDetails[0].location.county" id="county_Home">
							<option value="">Select County...</option>
						</select>
						<div id="county_Home_error"></div>		
					</div>
				</div>
				
					<h4 class="lightgray"><spring:message  code='label.dependent'/></h4>
					<div class="control-group">
						<fieldset>
							<legend class="aria-hidden"><spring:message  code='label.isMarried'/></legend>
							<label class="control-label"><spring:message  code='label.isMarried'/></label>
							<div class="controls">
								<label class="radio inline"> <input type="radio" name="isMarried" id="isMarried" value="YES">  Yes </label>
								<label class="radio inline"> <input type="radio" name="isMarried" id="isMarried" value="NO" checked> No </label>
							</div>
						</fieldset>
					</div>
					<div class="control-group">
						<label class="control-label" for="childCount"><spring:message  code='label.childCount'/></label>
						<div class="controls">
							<input type="text" id="childCount" name="childCount"  maxlength="2" />
							<span id="childCount_error"></span>
						</div>
					</div>
				
					<div class="form-actions">
						<a role="button" href="/hix/shop/employer/manage/list" name="cancelButton" id="cancelButton" class="btn clearForm"><spring:message  code='label.cancelButton'/></a>
	        			<input type="button" name="mainSubmitButton" id="mainSubmitButton" onClick="javascript:validateForm('Done');" class="btn btn-primary margin10-l" value="<spring:message  code='label.emplContinue'/>" />
	        		</div>
        		
        		<input type="hidden" name="jsonStateValue" id="jsonStateValue" value='${doctorsJSON}' />
				<input type="hidden" name="defaultStateValue" id="defaultStateValue" value='${defaultStateCode}' />
				<input type="hidden" name="employerId" id="employerId" value="${employer_id}"/>
			</form>
			
			<!--<div id="worksite" class="modal hide fade"  tabindex="-1" role="dialog" aria-labelledby="Addanewaworksite" aria-hidden="true">
			<form class="form-horizontal margin0"  id="frmworksites" name="frmworksites">	
				<df:csrfToken/>
				<input type="hidden" name="locations[0].primaryLocation" id="primaryLocation" value="NO">
					<input type="hidden" name="address2_Ws" id="address2_Ws" > 
					<input type="hidden" id="id" name="id" value="${employer_id}" />
					<input type="hidden" name="lat_0" id="lat_Ws" value="0.0">
					<input type="hidden" name="lon_0" id="lon_Ws" value="0.0">
					<input type="hidden" name="rdi" id="rdi_Ws" value="" />
					<input type="hidden" name="locations[0].location.countycode" id="countycode_Ws" value="" />
				
				<input type="hidden" id="id" name="id" value="${employer_id}"/>
				<div class="modal-header">
					<button type="button" id="crossClose" class="close clearForm" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="worksiteHeading"><spring:message  code='label.employeraddworksiteaddr'/></h3>
				</div>
				<div class="modal-body">
					<div class="control-group">
						<label class="control-label" for="address1_Ws"><spring:message  code='label.street'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" class="input-medium" name="locations[0].location.address1" id="address1_Ws"/>
							<div id="address1_Ws_error"></div>
							<input type="hidden" id="address1_Ws_hidden" name="address1_Ws_hidden" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="city_Ws"><spring:message  code='label.emplCity'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" class="input-medium" name="locations[0].location.city" id="city_Ws"  />
							<div id="city_Ws_error"></div>	
							<input type="hidden" id="city_Ws_hidden" name="city_Ws_hidden">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="state_Ws"><spring:message  code='label.emplState'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<select class="input-medium"  name="locations[0].location.state" id="state_Ws">
								 <option value="">Select</option>
								 <c:forEach var="state" items="${statelist}">
						    		<option id="${state.code}" <c:if test="${state.code == defaultStateCode}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
								</c:forEach>
							</select>
					    	<div id="state_Ws_error"></div>	
					    	<input type="hidden" id="state_Ws_hidden" name="state_Ws_hidden" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="zip_Ws"><spring:message  code='label.zip'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text"  name="locations[0].location.zip" id="zip_Ws" class="input-small zipCode" maxlength="5"/>
							<div id="zip_Ws_error"></div>
							<input type="hidden" id="zip_Ws_hidden" name="zip_Ws_hidden" >
						</div>
					</div>
					<div class="control-group" id="county_Ws_div" style="">
						<label for="county" class="control-label required"><spring:message  code='label.county'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<select class="input-large" name="locations[0].location.county" id="county_Ws">
								<option value="">Select County...</option>
							</select>
							<div id="county_Ws_error"></div>		
						</div>
					</div>
					</div>
					<div class="modal-footer">
						<button class="btn clearForm" data-dismiss="modal" aria-hidden="true" id="cancelBtnModal"><spring:message  code='label.cancel'/></button>
						<input type="button" name="submitbutton" id="submitbutton" onClick="javascript:validateLocation();" class="btn btn-primary" value="<spring:message  code='label.employeraddworksite'/>" />
					</div>
				</form>
			</div> -->
		</div><!--gutter10-->
      </div>
      <!--rightpanel--> 
    </div>
    <!--row-fluid--> 
  </div>
</div><!--gutter10-->

<div class="gutter10" id="confirmDiv" style="display:none;">
  <div class="row-fluid">
    <h1 id="skip"><spring:message code="label.confirmnewhirehead"/></h1>
    <br>
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        <h4 class="graydrkbg"><spring:message code="label.youshouldknow"/></h4>
        <div class="gutter10">
            <p>${exchangeName}&nbsp;<spring:message code="label.enrollmentdatesinfo"/></p>
          </div>
      </div>
      <!-- /.sidebar-->
      <div class="span9" id="rightpanel">
        <div class="row-fluid">
          <h4 class="graydrkbg gutter10"><spring:message code="label.newhireinfo"/></h4>
          <table class="table table-border-none">
				<tbody>
					<tr>
						<td class="span4 txt-right"><strong><spring:message code="label.newhirename"/></strong></td>
						<td id="fullName">${name}</td>
					</tr>
					<tr>
						<td class="span4 txt-right"><strong><spring:message code="label.coveragestartdate"/></strong></td>
						<td id="ceDate">${coverageEligibilityDate}
						</td>
						<td class="span4">
						<strong>
							<a href="javascript:void(0)" id="changeEligibilityDate_ModalLink"  onclick="javascript:changeEligibilityDate();" class="" data-toggle="modal">
							<spring:message  code="label.changestartdate"/></a>
						</strong>
						</td>
					</tr>
				</tbody>
			</table>
        </div>
        <br>
        <div class="row-fluid">
          <table class="table table-border-none">
				<tbody>
			
					<tr>
						<td class="span4 txt-right"><strong><spring:message code="label.openenrollmentbegindate"/></strong></td>
						<td id="openEnrollmentBeginDate">${openEnrollmentBeginDate}</td>
					</tr>
					<tr>
						<td class="span4 txt-right"><strong><spring:message code="label.openenrollmentenddate"/></strong></td>
						<td id="openEnrollmentEndDate">${openEnrollmentEndDate}</td>
					</tr>
				</tbody>
			</table>
			<div class="form-actions">
				<p>
					<input type="checkbox" name="optionsCheckbox" id="optionsCheckbox" class="terms" value="1" role=""> 
					I understand that I will be billed for coverage for this employee effective <strong><span id="termseffectivedate">${effectiveDate}</span></strong>.
				</p>
				
				<input type="button" value="<spring:message code="label.newhireback"/>" title="<spring:message code="label.newhireback"/>" class="btn margin10-r margin180-l" onclick="javascript:showEmployeeForm();">
				<input type="button" value="<spring:message code="label.newhireconfirm"/>" title="<spring:message code="label.newhireconfirm"/>" class="btn btn-primary" onClick="javascript:submitFrmADDEmployee('${newHire}');" >
			</div>
        </div>
        <!--row-fluid--> 
      </div>
      <!--rightpanel--> 
    </div>
    <!--row-fluid--> 
  </div>
  <!--main--> 
</div>
<!--gutter10-->


<script type="text/javascript">
var empDetailsJSON = ${empDetailsJSON};
var empEmployerLocationID = ${employerLocationID};
var empid = '${employer_id}';

populateLocations(empDetailsJSON, empEmployerLocationID);

function populateLocations(empDetailsJSON, empEmployerLocationID) {
	application.populate_location_on_parent_page(empDetailsJSON, empEmployerLocationID,'');
}

function postPopulateIndex(indexValue, zipCodeValue, zipId){
	//console.log(indexValue, zipCodeValue); 
	jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
	getCountyList( '_'+indexValue,zipCodeValue, '' );	
}

jQuery.validator.addMethod("currency", function(value, element, param) {
	var pattern = /^(\\$)?([0-9\,])+(\.\d{0,2})?$/;
	return pattern.test(value); 
});


var validator2 = $('#frmNewHireEmployee').validate({ 
	rules : {
		coverageEligibilityDate: {date: true}
	},
	messages : {
		coverageEligibilityDate: { 
			date : "<span> <em class='excl'>!</em><spring:message code='label.validatedate' javaScriptEscape='true'/></span>" 
			
			
		}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
		$('#terminationDate').unbind('focus');
	} 
});

var validator = $("#frmaddemployee").validate({ 
	rules : {'employeeDetails[0].firstName' : { required : true, namecheck:true},
		     'employeeDetails[0].lastName' : { required : true, namecheck:true},
			 'employeeDetails[0].dob' :{required : true, date: true, dobcheck: true},
			 ssn3 : {ssncheck : true, ssn1check : true, invalidssn1 :true,invalidssn2 :true,validateTaxId: true
			 },
			 'employeeDetails[0].smoker' : {required: true},
			 'employeeDetails[0].nativeAmr' : {required : true},
			 'employeeDetails[0].email' : { required : true, email: true},
			 phone3 : {phonecheck : true},
			 'employeeDetails[0].location.address1' : {
				required : true
			},
			'employeeDetails[0].location.city' : {
				required : true
			},
			'employeeDetails[0].location.state' : {
				required : true
			},
			'employeeDetails[0].location.county' : {
				required : true
			},
			'employeeDetails[0].location.zip' : {
				required : true,
				zipcheck : true
			},		
			 childCount : {digits: true}
	},
	messages : {
		'employeeDetails[0].firstName' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateempfirstname' javaScriptEscape='true'/></span>",
			namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatefirstnamechar' javaScriptEscape='true'/></span>" },
		'employeeDetails[0].lastName'  : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateemplastname' javaScriptEscape='true'/></span>" ,
			namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatelastnamechar' javaScriptEscape='true'/></span>" },
		'employeeDetails[0].dob' :{required :  "<span> <em class='excl'>!</em><spring:message code='label.validateempdob' javaScriptEscape='true'/></span>", 
				date:  "<span> <em class='excl'>!</em><spring:message code='label.validateempdob' javaScriptEscape='true'/></span>",
				dobcheck:  "<span> <em class='excl'>!</em><spring:message code='label.validateempvdob' javaScriptEscape='true'/></span>"},
		ssn3: {
		ssncheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempdigit' javaScriptEscape='true'/></span>",
		ssn1check : "<span> <em class='excl'>!</em><spring:message code='label.validatefirstthreedigit' javaScriptEscape='true'/></span>",
		invalidssn1 : "<span> <em class='excl'>!</em><spring:message code='label.invalidssn1' javaScriptEscape='true'/></span>",
		invalidssn2 : "<span> <em class='excl'>!</em><spring:message code='label.invalidssn2' javaScriptEscape='true'/></span>",
			validateTaxId: "<span> <em class='excl'>!</em><spring:message code='label.validateemptaxid' javaScriptEscape='true'/></span>"},
			
	    'employeeDetails[0].smoker' : {required: "<span> <em class='excl'>!</em><spring:message code='label.validatetobaccouser' javaScriptEscape='true'/></span>"},
	    'employeeDetails[0].nativeAmr': {required : "<span> <em class='excl'>!</em><spring:message code='label.validatenativeamerican' javaScriptEscape='true'/></span>"},
	    'employeeDetails[0].email' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateemailadr' javaScriptEscape='true'/></span>", 
	    						email: "<span> <em class='excl'>!</em><spring:message code='label.validateemail' javaScriptEscape='true'/></span>"},
	     phone3 : { phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvtelephone' javaScriptEscape='true'/></span>"},
		 'employeeDetails[0].location.address1' : {
			required : "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddr' javaScriptEscape='true'/></span>"
		},
		'employeeDetails[0].location.city' : {
			required : "<span> <em class='excl'>!</em><spring:message code='label.validatecity' javaScriptEscape='true'/></span>"
		},
		'employeeDetails[0].location.state' : {
			required : "<span> <em class='excl'>!</em><spring:message code='label.validatestate' javaScriptEscape='true'/></span>"
		},
		'employeeDetails[0].location.county' : {
			required : "<span> <em class='excl'>!</em><spring:message code='label.validatecounty' javaScriptEscape='true'/></span>"
		},
		'employeeDetails[0].location.zip' : {
			required : "<span> <em class='excl'>!</em><spring:message code='label.validateempzip' javaScriptEscape='true'/></span>",
			zipcheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvzip' javaScriptEscape='true'/></span>"
		},
	    childCount : {digits : "<span> <em class='excl'>!</em><spring:message code='label.validatechild' javaScriptEscape='true'/></span>"}  
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").addClass('error');
		$('#employmentDate, #dob').unbind('focus');
	}
});



$(document).ready(function(){
	$('.date-picker').datepicker({
		autoclose:true,
		forceParse: false
	});


	
	$('.info').tooltip();
});



function getHireDates(coverageEligibilityDate){
	
	var subUrl = '<c:url value="/shop/employer/manage/confirm">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
    $.ajax({
		url:url, 
		type: "POST", 
		dataType:'json',
		data: ({coverageEligibilityDate: coverageEligibilityDate}), 
		success: function(response,xhr)
		{
	        if(isInvalidCSRFToken(xhr))
	          return;
			var obj = response;
			$("#effectiveDate").html(obj['effectiveDate']);
			$("#termseffectivedate").html(obj['effectiveDate']);
			$("#openEnrollmentBeginDate").html(obj['openEnrollmentBeginDate']);
			$("#openEnrollmentEndDate").html(obj['openEnrollmentEndDate']);
			$("#fullName").html($('#firstName').val()+" "+$('#lastName').val());
			$("#ceDate").html($('#employmentDate').val());
		},
   });
}
$('input#gender').click(function(){
	$('#gender_error').hide();
});

/* $('#cancelBtnModal, #worksite .close, #crossClose').click(function(){
	$('#county_Ws').html('<option value="">Select County...</option>');
	$('.modal-backdrop').remove();
}); */


$('#email').popover({
    html: true,
    placement: 'right',
    trigger: 'focus',
    title: 'User must have access to this email address and phone number for initial account setup.',
    content: '<label for="noshow-msg"><input id="noshow-msg" type="checkbox"> Don\'\tt show this message again</label>'
});

$('#email').click(function() {
	if ($('#noshow-msg').is(':checked')) {
		$('#email').popover('destroy');
	}
});

$('#email').focusout(function(){
	$('.popover').remove();
});

/* $('#worksite').on('hidden', function () {
	$('.error label').hide();
	         
}); */

/* $(document).keyup(function(e) {
	if (e.keyCode == 27) {
		$('.error label').hide();
		$('.modal-body input').val('');
	}  
}); */
</script>
	