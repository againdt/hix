package com.getinsured.hix.planmgmt.mapper;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.dto.planmgmt.FormularyDrugItemDTO;
import com.getinsured.hix.dto.planmgmt.FormularyDrugListResponseDTO;
import com.getinsured.hix.dto.planmgmt.FormularyPlanDTO;

public final class FormularyDrugMapper {
	private static final String ISSUER_NAME = "Issuer Name";
	private static final String ISSUER_HIOS_ID = "HIOS ID";
	private static final String PLAN_YEAR = "Plan Year";
	private static final String FORMULARY_ID = "Formulary Id";
	private static final String DRUG_TIER_LEVEL = "Drug Tier Level";
	private static final String DRUG_TIER_TYPE1 = "Drug Tier Type 1";
	private static final String DRUG_TIER_TYPE2 = "Drug Tier Type 2";
	private static final String RXCUI = "RXCUI";
	private static final String AUTH_REQD = "Authentication Required";
	private static final String STEP_THERAPY_REQD = "Step Therapy Required";
	private static final String ISSUER_PLAN_NUMBER = "Issuer Plan Number";
	private static final String PLAN_NAME = "Plan Name";
		
	private static final int SEVEN = 7;
	private static final int SIX = 6;
	private static final int FIVE = 5;
	private static final int FOUR = 4;
	private static final int THREE = 3;
	private static final int TWO = 2;
	private static final int ONE = 1;
	private static final int ZERO = 0;

	public static List<String[]> prepareFormularyDrugListToExcel(FormularyDrugListResponseDTO formularyDrugListResponseDTO) {
		List<String[]> formularyDrugDetailsList = new ArrayList<String[]>();
		String[] issuerName = new String[TWO];
		String[] hiosId = new String[TWO];
		String[] planYear = new String[TWO];
		String[] formularyId = new String[TWO];
		String[] blankRow = new String[ONE];
		issuerName[ZERO] = ISSUER_NAME;
		issuerName[ONE] = formularyDrugListResponseDTO.getIssuerName();
		hiosId[ZERO]= ISSUER_HIOS_ID;
		hiosId[ONE] = formularyDrugListResponseDTO.getIssuerHIOSId();
		planYear[ZERO]= PLAN_YEAR;
		planYear[ONE] = formularyDrugListResponseDTO.getApplicableYear();
		formularyId[ZERO] = FORMULARY_ID;
		formularyId[ONE] = formularyDrugListResponseDTO.getFormularyId();
		blankRow[ZERO] = "";
		formularyDrugDetailsList.add(issuerName);
		formularyDrugDetailsList.add(hiosId);
		formularyDrugDetailsList.add(planYear);
		formularyDrugDetailsList.add(formularyId);
		formularyDrugDetailsList.add(blankRow);
		String[] drugHeader = new String[SEVEN];
		drugHeader[ZERO] = FORMULARY_ID;
		drugHeader[ONE] = DRUG_TIER_LEVEL;
		drugHeader[TWO] = DRUG_TIER_TYPE1;
		drugHeader[THREE] = DRUG_TIER_TYPE2;
		drugHeader[FOUR] = RXCUI;
		drugHeader[FIVE] = AUTH_REQD;
		drugHeader[SIX] = STEP_THERAPY_REQD;
		formularyDrugDetailsList.add(drugHeader);
		
		List<FormularyDrugItemDTO> formularyDrugItemList = formularyDrugListResponseDTO.getFormularyDrugItemList();
		for(FormularyDrugItemDTO formularyDrugItem: formularyDrugItemList){
			String[] drugData = new String[SEVEN];
			drugData[ZERO] = formularyDrugItem.getFormularyId();
			drugData[ONE] = String.valueOf(formularyDrugItem.getDrugTierLevel());
			drugData[TWO] = formularyDrugItem.getDrugTierType1();
			drugData[THREE] = formularyDrugItem.getDrugTierType2();
			drugData[FOUR] = formularyDrugItem.getRxcuiCode();
			drugData[FIVE] = formularyDrugItem.getAuthRequired();
			drugData[SIX] = formularyDrugItem.getStepTherapyRequired();
			formularyDrugDetailsList.add(drugData);
		}
		return formularyDrugDetailsList;
	}

	public static List<String[]> prepareFormularyPlanListToExcel(FormularyDrugListResponseDTO formularyDrugListResponseDTO) {
		List<String[]> formularyPlansList = new ArrayList<String[]>();
		String[] issuerName = new String[TWO];
		String[] hiosId = new String[TWO];
		String[] planYear = new String[TWO];
		String[] formularyId = new String[TWO];
		String[] blankRow = new String[ONE];
		issuerName[ZERO] = ISSUER_NAME;
		issuerName[ONE] = formularyDrugListResponseDTO.getIssuerName();
		hiosId[ZERO]= ISSUER_HIOS_ID;
		hiosId[ONE] = formularyDrugListResponseDTO.getIssuerHIOSId();
		planYear[ZERO]= PLAN_YEAR;
		planYear[ONE] = formularyDrugListResponseDTO.getApplicableYear();
		formularyId[ZERO] = FORMULARY_ID;
		formularyId[ONE] = formularyDrugListResponseDTO.getFormularyId();
		blankRow[ZERO] = "";
		formularyPlansList.add(issuerName);
		formularyPlansList.add(hiosId);
		formularyPlansList.add(planYear);
		formularyPlansList.add(formularyId);
		formularyPlansList.add(blankRow);
		String[] planHeader = new String[THREE];
		planHeader[ZERO] = ISSUER_PLAN_NUMBER;
		planHeader[ONE] = PLAN_NAME;
		planHeader[TWO] = FORMULARY_ID;
		formularyPlansList.add(planHeader);
		
		List<FormularyPlanDTO> formularyPlanList = formularyDrugListResponseDTO.getFormularyPlanList();
		for(FormularyPlanDTO formularyPlanItem: formularyPlanList){
			String[] planData = new String[THREE];
			planData[ZERO] = formularyPlanItem.getIssuerPlanNumber();
			planData[ONE] = formularyPlanItem.getPlanName();
			planData[TWO] = formularyPlanItem.getFormularyId();
			formularyPlansList.add(planData);
		}
		return formularyPlansList;
	}

}
