package com.getinsured.hix.batch.householdinfo.irs.service;
import java.util.Comparator;

import us.gov.treasury.irs.common.TaxHouseholdCoverageType;

public class IRSHouseHoldListComparator implements Comparator<TaxHouseholdCoverageType> {

	@Override
	public int compare(TaxHouseholdCoverageType irshousholdInfoObject1, TaxHouseholdCoverageType irshousholdInfoObject2) {
		
		return Integer.valueOf(irshousholdInfoObject1.getApplicableCoverageMonthNum())
				.compareTo(Integer.valueOf(irshousholdInfoObject2.getApplicableCoverageMonthNum()));
		
	}

}
