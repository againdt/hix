package com.getinsured.hix.cap.consumer.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.cap.util.ValidationUtility;
import com.getinsured.hix.platform.location.service.AddressValidatorService;
//import com.getinsured.hix.platform.util.ValidationUtility;

/*{
	  "firstName": "jack",
	  "lastName": "frost",
	  "phoneNumber": "8989998989",
	  "emailAddress": "jfrost@v.com",
	  "callPermission": "YES"
	}
*/
//TODO: Use Spring Validation framework instead of writing your own?
public class ConsumerDto {

	private String firstName;
	private String lastName;
	private String zipcode;
	private String dob;
	private String phoneNumber;
	private String emailAddress;
	private String callPermission;
	private String ssnRequired;
	private String ssnNotReqdReasons;

	private boolean isEmailRequired;
	
	private static final String MM_DD_YYYY= "MM/dd/yyyy";
	
	@Autowired private AddressValidatorService addressValidatorService;
	public boolean isValid() {
		return
				!ValidationUtility.isNullOrEmpty(firstName)
				&& !ValidationUtility.isNullOrEmpty(lastName)
				&& ValidationUtility.isValidZipCode(zipcode)
				&& ValidationUtility.isValidDate(dob, MM_DD_YYYY)
				&& ValidationUtility.isValidPhoneNumber(phoneNumber)
				&& ValidationUtility.isPresentIfRequired(emailAddress, isEmailRequired)
				&& (!isEmailRequired || (isEmailRequired && ValidationUtility.isValidEmail(emailAddress)))
			    && !ValidationUtility.isNullOrEmpty(callPermission);
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getCallPermission() {
		return callPermission;
	}
	public void setCallPermission(String callPermission) {
		this.callPermission = callPermission;
	}

	public String getFullName() {
		return getFirstName() + " " + getLastName();
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}
	
	public Date getDobAsDate() throws ParseException {
		if(getDob() != null) {
			return new SimpleDateFormat(MM_DD_YYYY, Locale.ENGLISH).parse(this.dob);
		} else {
			return null;
		}
	}
	
	public String getSsnRequired() {
		return ssnRequired;
	}

	public void setSsnRequired(String ssnRequired) {
		this.ssnRequired = ssnRequired;
	}

	public String getSsnNotReqdReasons() {
		return ssnNotReqdReasons;
	}

	public void setSsnNotReqdReasons(String ssnNotReqdReasons) {
		this.ssnNotReqdReasons = ssnNotReqdReasons;
	}

}
