
package com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.extension._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ridp.fars.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for The request payload.
 * 
 * <p>Java class for RequestPayloadType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestPayloadType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://ridp.dsh.cms.gov/extension/1.0}DSHReferenceNumber"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestPayloadType", propOrder = {
    "dshReferenceNumber"
})
public class RequestPayloadType
    extends ComplexObjectType
{

    @XmlElement(name = "DSHReferenceNumber", required = true)
    protected String dshReferenceNumber;

    /**
     * Gets the value of the dshReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDSHReferenceNumber() {
        return dshReferenceNumber;
    }

    /**
     * Sets the value of the dshReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDSHReferenceNumber(String value) {
        this.dshReferenceNumber = value;
    }

}
