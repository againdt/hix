package com.getinsured.hix.admin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.Announcement;
import com.getinsured.hix.model.AnnouncementRole;
import com.getinsured.hix.model.Role;

@Repository
public interface IAnnouncementRoleRepository extends JpaRepository<AnnouncementRole, Integer> {
	public List<Role> findRolesByAnnouncement(Announcement announcement);
}