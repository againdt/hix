import React from 'react';

const RelationshipsTooltip = props => {
    return (
        <span className='hyper-navigation-tooltip-anchor'>
             R
            <ul className='hyper-navigation-tooltip wide'>
                <span className='list-caption'>
                    Loaded blood relationships
                </span>
                {
                    props.loadedRelations.map(relation => {
                        const {
                            code,
                            text,
                            mirrorType
                        } = relation;
                        const mirroredRelation = props.loadedRelations.find(el => el.type === mirrorType);
                        return (
                            <li key={text}
                                style={{position: 'relative'}}
                            >

                                <span className='hyper-navigation-tooltip-number'>
                                    { code }
                                </span>
                                <span className='hyper-navigation-tooltip-name'>
                                    <strong>
                                        { text }
                                    </strong>
                                </span>
                                <span className='hyper-navigation-tooltip-mirror-text'>
                                    <strong>
                                        { mirroredRelation && mirroredRelation.text }
                                    </strong>
                                </span>
                                <span className='hyper-navigation-tooltip-mirror-code'>
                                    { mirroredRelation && mirroredRelation.code }
                                </span>
                            </li>
                        );
                    })
                }
            </ul>
        </span>
    );
};

export default RelationshipsTooltip;
