package com.getinsured.hix.serff.util;

import java.io.InputStream;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.PlanDocumentsJob;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch.FTP_STATUS;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.serff.service.templates.PlanDocumentsJobService;

/**
 * 
 * This is a singleton class for upload brochure utility to restrict more than
 * one simultaneous access to upload process.
 * 
 * @author vardekar_s
 * 
 */

@Component
public class SerffUploadBrochureUtils {

	public static final String SERF_ECM_BROCHURE_BASE_PATH = "serff/brochure/";
	private static final Logger LOGGER = Logger
			.getLogger(SerffUploadBrochureUtils.class);
	private static final String CLASS_NAME = "SerffUploadBrochureUtils";
	private static SerffUploadBrochureUtils instance;
	private static volatile boolean isUplaodRunning = false;
	private static String DATABASE_ENTRY_NOT_FOUND = "Document entry not found in the database. ";
	private static String FTP_FILE_MOVE_FAILED = "Moving file in ftp host failed. ";
	private static String SUCCESS = "success";
	private static String ERROR = "error";
	private static String SBC = "sbc";
	private static String BROCHURE = "brochure";
	private static String PDF = "pdf";
	private static String SBC_INITIAL = "S";
	private static String BROCHURE_INITIAL = "B";
	private static String PATH_SEPERATOR = "/";
	private static String EMPTY_STRING = "";

	private SerffUploadBrochureUtils() {

	}

	public static boolean isUploadRunning() {
		return isUplaodRunning;
	}

	private static void setUploadRunning(boolean flag) {
		isUplaodRunning = flag;
	}

	/**
	 * 
	 * This method returns singleton instance of SerffUploadBrochureUtils class.
	 * 
	 * @return SerffUploadBrochureUtils
	 */
	public static SerffUploadBrochureUtils getSerffUploadBrochureUtilsSingleton() {
		if (null == instance) {
			instance = new SerffUploadBrochureUtils();
		}
		return instance;
	}
	
	
	/**
	 * 
	 * This method extracts IssuerPlanNumber from remote file name.
	 * 
	 * 
	 * @return
	 */
	private String getIssuerPlanNumberFromFileName(String remoteFileName) {

		String[] filenameChunks = remoteFileName.split("[.]");

		if (null != filenameChunks && filenameChunks.length > 0) {
			String fileExtension = filenameChunks[filenameChunks.length - 1];

			if (!fileExtension.equals(PDF)) {
				LOGGER.info(CLASS_NAME + ". Not Supported FTP file : "
						+ remoteFileName);
				return null;
			} else {
				LOGGER.info(CLASS_NAME + ".Supported FTP file : "
						+ remoteFileName);	
				String planIdNameComponent = (filenameChunks[0].split("[_]"))[0];
				String issuerPlanNumber = null;
				String variance = null;
				//Check for variance.				
				if(planIdNameComponent.contains("-")){
					LOGGER.info(CLASS_NAME + ".Issuer plan number with variance : "
							+ planIdNameComponent);
					String[] planIdComponentArray = planIdNameComponent.split("[-]");
					issuerPlanNumber = planIdComponentArray[0];
					variance = planIdComponentArray[1];	
					LOGGER.debug(CLASS_NAME + ".Issuer plan number with variance : Issuer plan number : "
							+ issuerPlanNumber);
					LOGGER.debug(CLASS_NAME + ".Issuer plan number with variance : Variance : "
							+ variance);
				}else{					
					issuerPlanNumber = planIdNameComponent;
					LOGGER.debug(CLASS_NAME + ".Issuer plan number without variance : "
							+ issuerPlanNumber);
				}
				
				//Validate planId 				
				if(!isValidateStandardComponentId(issuerPlanNumber)){
					LOGGER.debug(CLASS_NAME + ".Plan id is not valid "
							+ issuerPlanNumber);
					return null;
				}
				
				return (null != variance) ? issuerPlanNumber + variance : issuerPlanNumber ;
			}
		}

		return null;
	}

	/**
	 * This method will download files from designated FTP location and upload
	 * them into the ECM. After file upload is done file entry will be updated
	 * in database table.
	 * 
	 * 
	 * @throws Exception
	 */
	public synchronized boolean uploadBrochure(
			ContentManagementService ecmService,
			PlanDocumentsJobService planDocumentsJobService,
			GHIXSFTPClient ftpClient) throws Exception {
		// Upload proces is not running. Start upload process here.
		setUploadRunning(true);		
		boolean isECMUploadSuccessful = true;
		//Get files from ftp location.
		String dropboxDirectory = GhixConstants.SERFF_CSR_HOME_PATH + GhixConstants.FTP_SERVER_UPLOAD_SUBPATH+PATH_SEPERATOR+GhixConstants.FTP_BROCHURE_PATH ;
		LOGGER.debug(CLASS_NAME + "Dropbox directory path :" + dropboxDirectory);
		Map<String, InputStream> fileList = ftpClient
				.getFilesFromDirectory(dropboxDirectory + PATH_SEPERATOR);
		//Returning false for no file list.
		if(null == fileList){
			LOGGER.error(CLASS_NAME + "There is no file at FTP location for upload process.");
			return false;
		}
		//Check and create error and success directories before processing. 
		//As absence of error and success directory will halt file move functionality and show wrong count on UI.
		checkSuccessAndErrorDirectories(ftpClient);
		
		//Map of pland id and brochure values.
		Map<String, String[]> planIdDocumentsMap = new HashMap<String, String[]>();
		
		// Download from FTP location.
		for (Entry<String, InputStream> entry : fileList.entrySet()) {
			String documentType = EMPTY_STRING;
			
			LOGGER.debug(CLASS_NAME + ".Processing file for issuer_plan_Number "
					+ entry.getKey());
			String fileName = entry.getKey();
			StringBuffer errorMessage = new StringBuffer();
			// Extract IssuerPlanNumber from filename.
			String issuerPlanNumber = getIssuerPlanNumberFromFileName(fileName);
			if (null != issuerPlanNumber && !issuerPlanNumber.isEmpty()) {
				LOGGER.debug(CLASS_NAME + ".Plan id/Issuer Plan Number from filename : "
						+ issuerPlanNumber);
				// Check document existence in database.									
				String hiosProductId = issuerPlanNumber.substring(0,10);				
				long planCount = 0;
				long planHealthCount = 0;
				
				if (fileName.toLowerCase().contains(SBC)) {
					documentType = SBC_INITIAL;
					planHealthCount = planDocumentsJobService.getPlanHealthCount(issuerPlanNumber);
					LOGGER.debug(CLASS_NAME + ".Plan Health count for " + issuerPlanNumber + " is " +  planHealthCount);
				} else if (fileName.toLowerCase().contains(BROCHURE)) {
					documentType = BROCHURE_INITIAL;
					planCount = planDocumentsJobService.getPlanCount(issuerPlanNumber);
					LOGGER.debug(CLASS_NAME + ".Plan count for " + issuerPlanNumber + " is " +  planCount);
				} else {
					LOGGER.error(CLASS_NAME
							+ ". Valid document type not found in " + fileName);
					continue;
				}
				// If current document type is "B" or "S".
				if ((documentType.equals(BROCHURE_INITIAL) && (planCount != 0))
						|| (documentType.equals(SBC_INITIAL) && (planHealthCount != 0))) {
					try {
						// Document is present in database. Upload in ECM.
						String documentId = uploadDocumentInEcm(fileName,
								entry.getValue(), ecmService);					
					
						//Update map values with uploaded document id and document name.
						addOrUpdateBrochureValuesInMap(issuerPlanNumber, documentType, documentId, fileName, planIdDocumentsMap);
						LOGGER.debug(CLASS_NAME + ". Map size after manipulation : " + planIdDocumentsMap.size());
						
						// Update Plan Documents Job table.
						int jobId = updatePlanDocumentJobTable(fileName, EMPTY_STRING, documentType,
								errorMessage.toString(), hiosProductId, issuerPlanNumber, issuerPlanNumber, documentId,
								EMPTY_STRING, FTP_STATUS.COMPLETED,
								planDocumentsJobService);		
						//Move files to success directory.					
						String message = moveFileToDirectory(jobId, true, fileName, ftpClient);	
						errorMessage.append(message);
					} catch (Exception e) {
						LOGGER.error(CLASS_NAME
								+ ". Exception occurred while uploading "+fileName+" in to ECM. Error is " + e.getMessage());
						isECMUploadSuccessful = false;
						// Update Plan Documents Job table.
						int jobId = updatePlanDocumentJobTable(fileName, EMPTY_STRING, documentType,
								e.getMessage(), hiosProductId, issuerPlanNumber, issuerPlanNumber, null, EMPTY_STRING,
								FTP_STATUS.FAILED, planDocumentsJobService);
						//Move file to error directory.
						String message = moveFileToDirectory(jobId, false, fileName, ftpClient);	
						errorMessage.append(message);
						errorMessage.append(e.getMessage());						
					}
				} else {	
					LOGGER.error(CLASS_NAME
							+ " Database entry not found for " + fileName);
					isECMUploadSuccessful = false;					
					// Update Error in Documents Job table.
					int jobId = updatePlanDocumentJobTable(fileName, EMPTY_STRING, documentType,
							DATABASE_ENTRY_NOT_FOUND, hiosProductId, issuerPlanNumber, issuerPlanNumber, null, EMPTY_STRING,
							FTP_STATUS.FAILED, planDocumentsJobService);
					//Move file to error directory.
					String message = moveFileToDirectory(jobId, false, fileName, ftpClient);					
					errorMessage.append(DATABASE_ENTRY_NOT_FOUND);
					errorMessage.append(message);
				}
			} else {
				LOGGER.error(CLASS_NAME + ". Excluding file since file type not supported. "
						+ fileName);
				continue;
			}
		}
		
		//Update values collected in map in single go.
		updatePlanDocuments(planIdDocumentsMap, planDocumentsJobService);		
		
		// Upload process finished. Reset flag to false.
		setUploadRunning(false);
		
		return isECMUploadSuccessful;
	}
	
	
	private void updatePlanDocuments(Map<String, String[]> planIdBrochureMap, PlanDocumentsJobService planDocumentsJobService){
		
		LOGGER.info(CLASS_NAME + ". Map size after manipulation : " + planIdBrochureMap.size() +". Updating database tables.");
		planDocumentsJobService.updatePlanDocuments(planIdBrochureMap);
		
	}
	
	
	/**
	 * This is utility method to hold all the plans and uploaded document information to update those in single go.
	 * 
	 * 
	 * @param issuerPlanNumber
	 * @param documentType
	 * @param documentId
	 * @param fileName
	 * @param idValueMap
	 */
	private void addOrUpdateBrochureValuesInMap(String issuerPlanNumber, String documentType, String documentId, String fileName, Map<String, String[]> idValueMap){
		//Check whether entry is present in the map for the existing plan.
		String[] brochureValueArr = idValueMap.get(issuerPlanNumber);
		if(null == brochureValueArr){
			//There is no previous entry in the map. Create new entry.
			LOGGER.info(CLASS_NAME + ". Creating new value array for " + issuerPlanNumber);
			brochureValueArr = new String[4];
		}
		
		//Either value array is new or already existing , set values and put in the map.
		if (documentType.equals(BROCHURE_INITIAL)) {			
				LOGGER.debug(CLASS_NAME + ". Entry in map for brochure : key : " + issuerPlanNumber + " , Values Before : " + brochureValueArr[0] + " , " + brochureValueArr[1] + " , " + brochureValueArr[2] + " , " + brochureValueArr[3]);	
				brochureValueArr[0] = documentId;
				brochureValueArr[1] = fileName;
				LOGGER.debug(CLASS_NAME + ". Entry in map for brochure  : key : " + issuerPlanNumber + " , Values After : " + brochureValueArr[0] + " , " + brochureValueArr[1] + " , " + brochureValueArr[2] + " , " + brochureValueArr[3]);				
		}
		
		if (documentType.equals(SBC_INITIAL)) {		
				LOGGER.debug(CLASS_NAME + ". Entry in map for SBC : key : " + issuerPlanNumber + " , Values Before : " + brochureValueArr[0] + " , " + brochureValueArr[1] + " , " + brochureValueArr[2] + " , " + brochureValueArr[3]);	
				brochureValueArr[2] = documentId;
				brochureValueArr[3] = fileName;
				LOGGER.debug(CLASS_NAME + ". Entry in map for SBC : key : " + issuerPlanNumber + " , Values After : " + brochureValueArr[0] + " , " + brochureValueArr[1] + " , " + brochureValueArr[2] + " , " + brochureValueArr[3]);							
				idValueMap.put(issuerPlanNumber, brochureValueArr);
		}
		
		//Adding will have no effect when key is present, it will update array values in previous steps.
		idValueMap.put(issuerPlanNumber, brochureValueArr);
		
		}
	
	
	/**
	 * This method checks whether error and success directories exist in dropbox directory. 
	 * 
	 * @param successDirectoryPath
	 * @param errorDirectoryPath
	 * @param ftpClient
	 * @throws Exception
	 */
	public void checkSuccessAndErrorDirectories(GHIXSFTPClient ftpClient){
		
		String dropBoxDirectoryPath = GhixConstants.SERFF_CSR_HOME_PATH + GhixConstants.FTP_SERVER_UPLOAD_SUBPATH + PATH_SEPERATOR + GhixConstants.FTP_BROCHURE_PATH;  
		String successDirectoryPath = dropBoxDirectoryPath + PATH_SEPERATOR + SUCCESS;
		String errorDirectoryPath = dropBoxDirectoryPath + PATH_SEPERATOR + ERROR;
		
		try {			
			if(!ftpClient.isRemoteDirectoryExist(successDirectoryPath+PATH_SEPERATOR)){
				LOGGER.error(CLASS_NAME + " success directory does not exist. Creating success directory in path " + successDirectoryPath);
				ftpClient.createDirectory(successDirectoryPath+PATH_SEPERATOR);
			}else{
				LOGGER.error(CLASS_NAME + " Success directory exist. " + successDirectoryPath);
			}
		} catch (Exception e) {
			LOGGER.error(CLASS_NAME + "Error while creating success directory in path " + successDirectoryPath);
		}

		try {
			if(!ftpClient.isRemoteDirectoryExist(errorDirectoryPath+PATH_SEPERATOR)){
				LOGGER.error(CLASS_NAME + " error directory does not exist. Creating error directory in path " + errorDirectoryPath);
				ftpClient.createDirectory(errorDirectoryPath+PATH_SEPERATOR);
			}else{
				LOGGER.error(CLASS_NAME + " Error directory exist. " + errorDirectoryPath);
			}	
		} catch (Exception e) {
			LOGGER.error(CLASS_NAME + "Error while creating error directory in path " + errorDirectoryPath);
		}
	}
	
	
	/**
	 * This method will move files in success/error directory in ftp server.
	 * 
	 * @param jobId
	 * @param isSuccess
	 * @param fileName
	 * @param ftpClient
	 * @return
	 */
	public String moveFileToDirectory(int jobId, boolean isSuccess, String fileName, GHIXSFTPClient ftpClient){	
		StringBuffer message = new StringBuffer();
		try {		
			String ftpDropboxPath =  GhixConstants.SERFF_CSR_HOME_PATH + GhixConstants.FTP_SERVER_UPLOAD_SUBPATH + PATH_SEPERATOR + GhixConstants.FTP_BROCHURE_PATH;
			LOGGER.info(CLASS_NAME + "FTP dropbox path while moving file : " + ftpDropboxPath);
			if(isSuccess){		
				String ftpSuccessPathWithJobId = ftpDropboxPath + PATH_SEPERATOR+GhixConstants.FTP_BROCHURE_SUCCESS_PATH + jobId;
				LOGGER.debug(CLASS_NAME + ".Moving file to success directory. Path - " + ftpSuccessPathWithJobId + " File -" + fileName ) ;
				ftpClient.moveFile(ftpDropboxPath + PATH_SEPERATOR, fileName, ftpSuccessPathWithJobId + PATH_SEPERATOR, fileName);
				LOGGER.info(CLASS_NAME + ".Moving file to success directory completed." + fileName );
			}else{
				String ftpErrorPathWithJobId = ftpDropboxPath+PATH_SEPERATOR+GhixConstants.FTP_BROCHURE_ERROR_PATH + jobId;
				LOGGER.debug(CLASS_NAME + ".Moving file to error directory. Path - " + ftpErrorPathWithJobId + " File -" + fileName ) ;
				ftpClient.moveFile(ftpDropboxPath + PATH_SEPERATOR, fileName, ftpErrorPathWithJobId + PATH_SEPERATOR, fileName);
				LOGGER.info(CLASS_NAME + ".Moving file to error directory completed." + fileName );
			}			
		}
		catch (Exception ex) {
			LOGGER.error("SFTP Error: " + ex.getMessage(), ex);
			message.append(FTP_FILE_MOVE_FAILED);
			message.append(ex.getMessage());
		}				
		return message.toString();
	}
	
	/**
	 * This method creates entry in database for serff_plan_documents_job table.
	 * 
	 * @param documentNewName
	 * @param documentOldName
	 * @param documentType
	 * @param errorMessage
	 * @param hiosProductId
	 * @param issuerPlanNumber
	 * @param planId
	 * @param ucmNewId
	 * @param ucmOldId
	 * @param status
	 * @param planDocumentsJobService
	 * @return
	 */
	private int updatePlanDocumentJobTable(String documentNewName,
			String documentOldName, String documentType, String errorMessage,
			String hiosProductId, String issuerPlanNumber, String planId,
			String ucmNewId, String ucmOldId, FTP_STATUS status,
			PlanDocumentsJobService planDocumentsJobService) {
		PlanDocumentsJob planDocumentJob = new PlanDocumentsJob();
		planDocumentJob.setDocumentNewName(documentNewName);
		planDocumentJob.setDocumentOldName(documentOldName);
		planDocumentJob.setDocumentType(documentType);
		planDocumentJob.setErrorMessage(errorMessage);
		planDocumentJob.setHiosProductId(hiosProductId);
		planDocumentJob.setIssuerPlanNumber(issuerPlanNumber);
		planDocumentJob.setLastUpdated(new TSDate());
		planDocumentJob.setPlanId(planId);
		planDocumentJob.setUcmNewId(ucmNewId);
		planDocumentJob.setUcmOldId(ucmNewId);
		planDocumentJob.setFtpStatus(status);
		return planDocumentsJobService.persistPlanDocumentRecord(planDocumentJob);
	}


	/**
	 * 
	 * This method uploads brochure documents from ftp to ecm location. 
	 * 
	 * 
	 * @param fileName
	 * @param docStream
	 * @param ecmService
	 * @return
	 * @throws Exception
	 */
	private String uploadDocumentInEcm(String fileName, InputStream docStream,
			ContentManagementService ecmService) throws Exception {
		try {
			LOGGER.info(CLASS_NAME + ".uploading file " + fileName + " in ECM.");
			byte[] fileContent = IOUtils.toByteArray(docStream);
			/*BufferedInputStream inbf = new BufferedInputStream(docStream);
			byte[] fileContent = new byte[BUFFER_SIZE];
			int readCount;
			String result = null;
			StringBuffer resultString = new StringBuffer();
			while ((readCount = inbf.read(fileContent)) > 0) {
				resultString.append(new String(fileContent, 0, readCount));
			}
			result = resultString.toString();
			// Upload file from FTP in to ECM.
			String documentId = ecmService.createContent(
					SERF_ECM_BROCHURE_BASE_PATH, fileName, result.getBytes());*/
			String documentId = ecmService.createContent(
					SERF_ECM_BROCHURE_BASE_PATH, fileName, fileContent
					, ECMConstants.Serff.DOC_CATEGORY, ECMConstants.Serff.DOC_SUB_CATEGORY, null);
			LOGGER.debug(CLASS_NAME + ". file stored in ECM as " + documentId );
			return documentId;
		} catch (Exception e) {			
			LOGGER.error(fileName + " upload to ECM failed. " + e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * This method returns count of files that are present in ftp location.
	 * 
	 * @param workDirectoryPath
	 * @return
	 * @throws Exception
	 */
	public int getFileCount(String workDirectoryPath, GHIXSFTPClient ftpClient)
			throws Exception {
		LOGGER.debug("Getting file count from : " + workDirectoryPath);
		try {
			Map<String, InputStream> fileList = ftpClient
					.getFilesFromDirectory(workDirectoryPath + PATH_SEPERATOR);
			int count = 0;
			for (Entry<String, InputStream> entry : fileList.entrySet()) {
				// Add only valid files to the total file count.
				if (null != getIssuerPlanNumberFromFileName(entry.getKey())) {
					count++;
				}
			}
			return count;
		} catch (Exception ex) {
			LOGGER.info("Exception while getting files count.");
			throw ex;
		}
	}
	
	private static final int LEN_HIOS_PLAN_ID = 14;
	private static final String PATTERN_PLAN_ID = "(^[0-9]{5})([A-Za-z]{2})([0-9]{7})$";
	
	/**
	  * This method validates the Issuer Plan Number.
	  * @param issuerPlanNumber String Format.
	  */
	public boolean isValidateStandardComponentId(String issuerPlanNumber) {

		boolean flag = false;

		if (StringUtils.isNotBlank(issuerPlanNumber)) {

			if (isValidLength(LEN_HIOS_PLAN_ID, issuerPlanNumber)
					&& isValidPattern(PATTERN_PLAN_ID, issuerPlanNumber)) {
				flag = true;
			}
		}
		return flag;
	}
	
	/**
	 * Method is used to validate given value with passed argument pattern.
	 * @param pattern
	 * @return True for Valid otherwise False.
	 */
	protected boolean isValidPattern(final String pattern, final String givenValue) {

		boolean isValid = false;

		if (StringUtils.isNotEmpty(pattern)
				&& StringUtils.isNotEmpty(givenValue)) {

			// Assigning the regular expression
			return givenValue.matches(pattern);
		}
		return isValid;
	}

	/**
	 * Method is used to validate length of field.
	 * @param length
	 * @param value
	 * @return True for Valid URL otherwise False.
	 */
	protected boolean isValidLength(final int length, final String value) {

		boolean isValid = false;

		if (length == StringUtils.length(value)) {
			isValid = true;
		}
		return isValid;
	}

}
