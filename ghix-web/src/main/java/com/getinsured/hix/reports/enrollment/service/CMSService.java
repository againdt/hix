package com.getinsured.hix.reports.enrollment.service;

import org.springframework.data.domain.Page;

import com.getinsured.hix.reports.enrollment.dto.CMSErrorReportDTO;
import com.getinsured.hix.reports.enrollment.dto.CMSFileTransmissionLogDTO;
import com.getinsured.hix.reports.enrollment.dto.CMSXmlRequestDTO;
import com.getinsured.hix.reports.enrollment.dto.CMSXmlSumryReportDTO;
import com.getinsured.hix.reports.enrollment.dto.MonthlyIRSErrorReportDTO;

public interface CMSService {

	public Page<CMSXmlSumryReportDTO> findCmsXmlSumryReportRecords(int page, int size, CMSXmlRequestDTO cmsXmlRequestDTO);
	
	public Page<CMSErrorReportDTO> getCMSErrorReport(int page, int size, CMSXmlRequestDTO cmsErrorRequestDTO);
	
	/**
	 * 
	 * @param String pageNumber
	 * @param String pageSize
	 * @param Object<CMSXmlRequestDTO> cmsXmlRequestDTO
	 * @return
	 */
	Page<CMSFileTransmissionLogDTO> getCMSFileTransmissionLogData(String pageNumber, String pageSize, CMSXmlRequestDTO cmsXmlRequestDTO);
	
	/**
	 * 
	 * @param String pageNumber
	 * @param String pageSize
	 * @param Object<CMSXmlRequestDTO> cmsXmlRequestDTO
	 * @return
	 */
	Page<MonthlyIRSErrorReportDTO> getMonthlyIRSErrorReportData(String pageNumber, String pageSize, CMSXmlRequestDTO cmsXmlRequestDTO);
}
