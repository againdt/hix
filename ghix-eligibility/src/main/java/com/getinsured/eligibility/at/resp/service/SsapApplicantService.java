package com.getinsured.eligibility.at.resp.service;

import java.util.List;

import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;


public interface SsapApplicantService {


	SsapApplicant findByExternalCaseNumber(String caseNumber) throws GIException;

	/**
	 * This method returns SSAP-Applications which has been linked to ssap_applicants#external_applicant_id
	 * Method considers ssap_applicants which have person_id 1
	 * @param caseNumber - This is actually external_applicant_id which is mapped with pay-loads Person:PersonAugmentation:PersonMedicaidIdentification
	 * @return
	 */
	List<SsapApplication> findSsapApplicationForExternalApplicantId(String caseNumber);

	List<SsapApplication> findSsapApplicationForHHCaseId(String hhCaseId);
	
	SsapApplicant findById(long id);
	
	SsapApplicant findByApplicantGuidAndSsapApplication(String applicantGuid, SsapApplication ssapApplication);

}
