package com.getinsured.hix.entity.enrollmententity.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.entity.enrollmententity.repository.IPopulationEthnicityRepository;
import com.getinsured.hix.entity.enrollmententity.service.PopulationEthnicityService;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.PopulationEthnicity;

@Service("populationEthnicityService")
@Transactional
public class PopulationEthnicityServiceImpl implements
		PopulationEthnicityService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PopulationEthnicityServiceImpl.class);

	@Autowired
	private IPopulationEthnicityRepository populationEthnicityRepository;

	/**
	 * @see PopulationEthnicityService#findByEntityId(int)
	 */
	@Override
	public PopulationEthnicity findById(int ethnicityId) {
		
		LOGGER.info("findById: START");
		LOGGER.info("findById: END");
		
		return populationEthnicityRepository.findById(ethnicityId);
	}

	/**
	 * @see PopulationEthnicityService#findByEntityId(int)
	 */
	@Override
	public List<PopulationEthnicity> findByEntityId(int entityId) {
		
		LOGGER.info("findByEntityId: START");
		LOGGER.info("findByEntityId: END");
		
		return populationEthnicityRepository.findByEntityId(entityId);
	}

	/**
	 * @see PopulationEthnicityService#deleteAssociatedEthnicities(int)
	 */
	@Override
	public int deleteAssociatedEthnicities(int entityId) {
		
		LOGGER.info("deleteAssociatedEthnicities: START");
		LOGGER.info("deleteAssociatedEthnicities: END");
				
		return populationEthnicityRepository
				.deleteAssociatedEthnicities(entityId);
	}

	/**
	 * @see PopulationEthnicityService#saveAssociatedEthnicities(EnrollmentEntity,
	 *      List)
	 */
	@Override
	public void saveAssociatedEthnicities(EnrollmentEntity enrollmentEntity,
			List<PopulationEthnicity> populationEthnicities) {
		
		LOGGER.info("saveAssociatedEthnicities: START");

		populationEthnicityRepository
				.deleteAssociatedEthnicities(enrollmentEntity.getId());

		List<PopulationEthnicity> updatedPopulationEthnicities = new ArrayList<>();
		
		if (populationEthnicities != null && !populationEthnicities.isEmpty()) {
			for (PopulationEthnicity ethnicity : populationEthnicities) {
				ethnicity.setEntity(enrollmentEntity);
				updatedPopulationEthnicities.add(ethnicity);
			}
			
			updatedPopulationEthnicities = populationEthnicityRepository.save(updatedPopulationEthnicities);
		}
		
		LOGGER.info("saveAssociatedEthnicities: END");
	}

}
