import React, { Fragment, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import Floater from 'react-floater';

// ActionCreators

import {changeData} from '../../actions/actionCreators';

// Constants

import {DEFAULT_COMMUNICATION_METHOD} from '../../constants/dropdownOptions';



const Page1_01 = props => {

    const {
        data,
        headOfHousehold,
        headOfHousehold: {
            householdContact: {
                contactPreferences: {
                    preferredContactMethod
                }
            }
        },
        stateCode
    } = props;

    useEffect(() => {
        if(stateCode) {
            const newState = { ...data };
            const newData = headOfHousehold;
            if (newData.householdContact.homeAddress.state !== stateCode) {
                newData.householdContact.homeAddress.state = stateCode;
                newState.singleStreamlinedApplication.taxHousehold[0].householdMember[0] = newData;
                props.actions.changeData(newState);
            }
        }
    }, []);

    useEffect(() => {
        if(!preferredContactMethod) {
            const newState = { ...data };
            const newData = headOfHousehold;
            if(newData.householdContact.contactPreferences.preferredContactMethod !== DEFAULT_COMMUNICATION_METHOD) {
                newData.householdContact.contactPreferences.preferredContactMethod = DEFAULT_COMMUNICATION_METHOD;
                newState.singleStreamlinedApplication.taxHousehold[0].householdMember[0] = newData;
                props.actions.changeData(newState);
            }
        }
    }, []);

    return (
        <Fragment>
            <div id="saGetReady" className="">
                <div className="subsection">
                    <div className="usa-alert usa-alert-info alert--info">
                        <div className="usa-alert-body" >
                            <p className="usa-alert-text">
                                <strong>
                                    <FormattedMessage
                                        id="ssap.getReady.msg.one"
                                        defaultMessage="Welcome to Nevada Health Link. "
                                    />
                                </strong><br/>
                                <FormattedMessage
                                    id="ssap.getReady.msg.two"
                                    defaultMessage="Here you&apos;ll be able to shop for health insurance for yourself or anyone in your household. Before you start, please take a moment now to gather the information listed below."
                                />
                            </p>
                        </div>
                    </div>
                    <p>
                        <i>
                            All fields on this application marked with an asterisk (*)
                            are required unless otherwise indicated.
                        </i>
                    </p>
                    <p>
                        For anyone you want to insure, you will need:
                    </p>
                    <ul>
                        <li>
                            Names
                        </li>
                        <li>
                            Addresses
                        </li>
                        <li>
                            Social Security Number
                        </li>
                        <li>
                            Birthdates
                        </li>
                        <li>
                            Document numbers for anyone with eligible &nbsp;
                            <Floater
                                content="Anyone who is seeking coverage may be required to provide documents to prove ability to enroll in coverage"
                                placement="top">
                                <a href='https://help.nevadahealthlink.com/hc/en-us/articles/360028568792' target="_blank"  rel="noopener noreferrer">
                                    <span>
                                        Immigration status
                                    </span>
                                </a>
                            </Floater>
                        </li>
                    </ul>
                </div>
            </div>
        </Fragment>
    );
};


const mapStateToProps = state => ({
    data: state.data,

    headOfHousehold: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember[0],

    stateCode: state
        .headerLinks
        .stateCode
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page1_01.defaultProps = {
    name: 'Page1_01'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page1_01);
