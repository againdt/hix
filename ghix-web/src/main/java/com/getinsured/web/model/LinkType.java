package com.getinsured.web.model;

/**
 * Describes environment link types.
 *
 * @author Yevgen Golubenko
 * @since 2/11/19
 */
public enum LinkType
{
  /**
   * Link that goes to homepage.
   */
  HOME,

  /**
   * Link that goes to help section.
   */
  HELP,

  /**
   * Link that loads assistance page.
   */
  ASSISTANCE,

  /**
   * Link that loads contact us page.
   */
  CONTACT_US,

  /**
   * Link that shows privacy policy page.
   */
  PRIVACY_POLICY,

  /**
   * Link that shows terms of service.
   */
  TOS,

  /**
   * Link that takes user to login page.
   */
  LOGIN,

  /**
   * Link that takes user to logout page.
   */
  LOGOUT,

  /**
   * Link that takes user to dashboard.
   */
  DASHBOARD,

  /**
   * Link to load unread messages for the user.
   */
  UNREAD_MESSAGES,

  /**
   * Link that takes user to user account settings page.
   */
  SETTINGS,

  /**
   * Link that takes user to new agent/broker signup page.
   */
  AGENT_BROKER_SIGNUP,

  /**
   * Link that takes user to new entity signup page.
   */
  ENTITY_SIGNUP,

  /**
   * Chat URL link.
   */
  CHAT,

  /**
   * Phone URL link.
   */
  PHONE,

  /**
   * Manage Assister Link domain.
   * <p>
   *   Currently only applicable to MN environment.
   * </p>
   */
  MANAGE_ASSISTER,

  /**
   * California CalHEERS page url.
   * <p>
   *   CalHEERS stands for
   *   California Healthcare Eligibility, Enrollment and Retention System
   * </p>
   */
  CALHEERS_URL;
}
