package com.getinsured.iex.hub.mec.wrapper;



import java.text.ParseException;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.RequestPersonType;
import com.getinsured.iex.hub.mec.niem.niem.fbi._2.SEXCodeSimpleType;
import com.getinsured.iex.hub.mec.niem.niem.fbi._2.SEXCodeType;
import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.PersonNameTextType;
import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.PersonNameType;
import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.Date;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

public class PersonWrapper implements JSONAware{
	
	private static com.getinsured.iex.hub.mec.niem.niem.niem_core._2.ObjectFactory niemCoreFactory;
	private static com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.ObjectFactory proxyFactory;
	private static com.getinsured.iex.hub.mec.niem.niem.fbi._2.ObjectFactory fbiFactory;

	private com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.ObjectFactory xsdFactory;

	private RequestPersonType reqPerson = null;
	private PersonNameType personNameType = null;
	private PersonNameTextType personGivenNameType = null;
	private PersonNameTextType personMiddleNameType = null;
	private PersonNameTextType personSurnameType = null;
	private TextType personNameSuffixTextType = null;
	private JAXBElement<Date> personBirthDateJAXBElement;
	private IdentificationType personIdentificationType = null;
	private SEXCodeType personSexCodeType = null;
	
	private String personBirthDate = null;
	private String personGivenName = null;
	private String personMiddleName = null;
	private String personSurName = null;
	private String personNameSuffixText = null;
	private String personSSNIdentification = null;
	private String personSexCode = null;
	
	
	public void setPersonSexCode(String personSexCode){
		
		if(personSexCode == null || personSexCode.trim().isEmpty()){
			return;
		}
			
		if(personSexCodeType == null){
			personSexCodeType = fbiFactory.createSEXCodeType();
		}
		personSexCodeType.setValue(SEXCodeSimpleType.fromValue(personSexCode));
		this.reqPerson.setPersonSexCode(personSexCodeType);
		this.personSexCode = personSexCode;
	}
	
	public String getPersonSexCode(){
			return this.personSexCode;
	}
	
	public PersonWrapper(){
		xsdFactory = new com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.ObjectFactory();
		niemCoreFactory = new com.getinsured.iex.hub.mec.niem.niem.niem_core._2.ObjectFactory();
		fbiFactory = new com.getinsured.iex.hub.mec.niem.niem.fbi._2.ObjectFactory();
		proxyFactory = new com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.ObjectFactory();
		
		com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ObjectFactory extnFactory = new com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ObjectFactory();
		this.reqPerson = extnFactory.createRequestPersonType();

		this.personNameType = niemCoreFactory.createPersonNameType();
		this.reqPerson.setPersonName(personNameType);
	}
	
	public void setPersonBirthDate(String mmddyyyy) throws DatatypeConfigurationException, ParseException{

		if(mmddyyyy!=null){
			Date date = proxyFactory.createDate();
			date.setValue(PlatformServiceUtil.stringToXMLDate(mmddyyyy, "MM/dd/yyyy"));
			
			this.personBirthDateJAXBElement = niemCoreFactory.createDate(date);
			
			DateType dateType = niemCoreFactory.createDateType();
			dateType.setDateRepresentation(personBirthDateJAXBElement);
			this.reqPerson.setPersonBirthDate(dateType);
		}
	}
	
	public String getPersonBirthDate(){
			return personBirthDate;
	}
	
	public void setPersonGivenName(String personGivenName){
		if(personGivenName == null || personGivenName.trim().length() == 0){
			return;
		}
		if(personGivenNameType == null){
			personGivenNameType = niemCoreFactory.createPersonNameTextType();
		}
		personGivenNameType.setValue(personGivenName);
		personNameType.setPersonGivenName(personGivenNameType);

		this.personGivenName = personGivenName;
	}
	
	public String getPersonGivenName(){
			return this.personGivenName;
	}
	
	public void setPersonMiddleName(String personMiddleName){
		if(personMiddleName == null || personMiddleName.trim().length() == 0){
			return;
		}
		if(personMiddleName != null){
			
			if(personMiddleNameType == null){
				personMiddleNameType = niemCoreFactory.createPersonNameTextType();
			}
			personMiddleNameType.setValue(personMiddleName);
			personNameType.setPersonMiddleName(personMiddleNameType);
			
			this.personMiddleName = personMiddleName;
		}
	}
	
	public String getPersonMiddleName(){
			return this.personMiddleName;
	}
	
	public void setPersonSurName(String personSurName){
		if(personSurName == null || personSurName.trim().length() == 0){
			return;
		}
		if(personSurName != null){
			if(personSurnameType == null){
				personSurnameType = niemCoreFactory.createPersonNameTextType();
			}
			
			personSurnameType.setValue(personSurName);
			personNameType.setPersonSurName(personSurnameType);
			
			this.personSurName = personSurName;
		}
	}
	
	public String getPersonSurName(){
			return this.personSurName;
	}
	
	public String getPersonNameSuffixText() {
		return personNameSuffixText;
	}

	public void setPersonNameSuffixText(String personNameSuffixText) {
		if(personNameSuffixText == null || personNameSuffixText.trim().length() == 0){
			return;
		}
		if(personNameSuffixText!=null){
			if(personNameSuffixTextType == null){
				personNameSuffixTextType = niemCoreFactory.createTextType();
			}
			this.personNameSuffixTextType.setValue(personNameSuffixText);
			personNameType.setPersonNameSuffixText(personNameSuffixTextType);	
			
			this.personNameSuffixText = personNameSuffixText;
		}
	}

	public String getPersonSSNIdentification() {
		return personSSNIdentification;
	}

	public void setPersonSSNIdentification(String personSSNIdentification) {
		if(personSSNIdentification == null || personSSNIdentification.trim().length() == 0){
			return;
		}
		
		if(personIdentificationType  == null){
			personIdentificationType = niemCoreFactory.createIdentificationType();
		}
		
		com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.String ssnStr = xsdFactory.createString();
		ssnStr.setValue(personSSNIdentification);
		personIdentificationType.getIdentificationID().add(ssnStr);
		this.personSSNIdentification = personSSNIdentification;			
		
		this.reqPerson.setPersonSSNIdentification(personIdentificationType);
	}

	public RequestPersonType getSystemRepresentation(){
		return this.reqPerson;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("PersonSSNIdentification", this.personSSNIdentification);
		obj.put("PersonBirthDate",personBirthDate);
		obj.put("PersonGivenName",personGivenName);
		obj.put("PersonMiddleName", personMiddleName);
		obj.put("PersonSurName",personSurName);
		obj.put("PersonNameSuffixText", personNameSuffixText);
		obj.put("PersonSSNIdentification", personSSNIdentification);
		return obj.toJSONString();
	}
	

	
}
