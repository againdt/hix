package com.getinsured.hix.agency.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.agency.repository.IAgencyAssistantAudRepository;
import com.getinsured.hix.agency.repository.IAgencyAssistantRepository;
import com.getinsured.hix.agency.util.AgencyDtoMapper;
import com.getinsured.hix.agency.util.AgencyUtils;
import com.getinsured.hix.broker.service.BrokerIndTriggerService;
import com.getinsured.hix.broker.util.BrokerUtils;
import com.getinsured.hix.dto.agency.AgencyAssistantApprovalDto;
import com.getinsured.hix.dto.agency.AgencyAssistantApprovalStatusHistory;
import com.getinsured.hix.dto.agency.AgencyAssistantDetailsDto;
import com.getinsured.hix.dto.agency.AgencyAssistantInfoDto;
import com.getinsured.hix.dto.agency.AgencyAssistantListDto;
import com.getinsured.hix.dto.agency.AgencyAssistantSearchDto;
import com.getinsured.hix.dto.agency.AgencyAssistantStatusDto;
import com.getinsured.hix.dto.agency.AgencyAssistantStatusHistory;
import com.getinsured.hix.dto.agency.AgencySiteLocationDto;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.model.agency.AgencySite;
import com.getinsured.hix.model.agency.assistant.AgencyAssistant;
import com.getinsured.hix.model.agency.assistant.AgencyAssistant.ApprovalStatus;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserRoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.timeshift.sql.TSTimestamp;

@Service
public class AgencyAssistantServiceImpl implements AgencyAssistantService{
	
	public static final Logger LOGGER = LoggerFactory.getLogger(AgencyAssistantServiceImpl.class);
	
	private static final String ASSISTANT_ID_SEQ = "ASSISTANT_ID_SEQ";
	public static final Integer PAGE_SIZE = 10;
	private static final String UI_DATE_FORMAT = "MM-dd-yyyy";
	private static final String DATE_FORMAT_1 = "yyyy-MM-dd";
	private static final String DATE_FORMAT_2 = "MMM dd, yyyy";
	private static final String ID = "id";
	private static final String AGENCY_ID = "agency.id";
	private static final String FIRST_NAME = "firstName";
	private static final String LAST_NAME = "lastName";
	private static final String STATUS = "status";
	private static final String ASSISTANT_ROLE = "assistantRole";
	private static final String BUSINESS_LEGAL_NAME = "businessLegalName";
	private static final String STAFF_ID = "assistantNumber";
	private static final String APPROVED_ON = "approvalDate";
	private static final String APPROVAL_STATUS = "approvalStatus";
	private static final String EMPTY_STRING = "";
	private static final String ASSISTANT_APR_SEQ = "ASSISTANT_APR_NUM_SEQ";
	private static final String USER_FIRST_NAME = "user.firstName";
	private static final String USER_LAST_NAME = "user.lastName";
	private static final String ESCAPED_CHAR_PERCENTAGE = "\\%";
	private static final String LIKE_ESCAPE_CHAR = "\\";
	private static final String ESCAPED_CHAR_UNDERSCORE = "\\_";
	private static final String PERCENTAGE = "%";
	
	@Autowired
	private IAgencyAssistantRepository iAgencyAssistantRepository;
	@Autowired
	private IAgencyAssistantAudRepository iAgencyAssistantAudRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired 
	private GhixDBSequenceUtil ghixDBSequenceUtil;
	@Autowired
	private ObjectFactory<QueryBuilder> delegateFactory;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired
	private AgencySiteService agencySiteService;
	@Autowired
	private BrokerIndTriggerService brokerIndTriggerService;
	@Autowired
	private AgencyDtoMapper agencyDtoMapper;
	@Autowired
	private AgencyUtils agencyUtils;
	@Autowired 
	private RoleService roleService;
	@Autowired
	private UserRoleService userRoleService;
		
	@Override
	public AgencyAssistant findByAgencyId(Long agencyId) {
		return iAgencyAssistantRepository.findByAgencyId(agencyId);
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public AgencyAssistant saveOrUpdate(AgencyAssistant agencyAssistant) {
		return iAgencyAssistantRepository.save(agencyAssistant);
	}

	@Override
	public AgencyAssistant findById(Long assistantId) {
		return iAgencyAssistantRepository.findById(assistantId);
	}
	
	@Override
	public AgencyAssistant findByPersonalEmailAddress(String personalEmailAddress) {
		return iAgencyAssistantRepository.findByPersonalEmailAddress(personalEmailAddress);
	}

	@Override
	public AgencyAssistant findByUserId(int userId) {
		return iAgencyAssistantRepository.findByUserId(userId);
	}

	@Override
	public void createModuleUser(AgencyAssistant assistant) {
		ModuleUser moduleUser;
		String moduleName;
		
		try {
			AccountUser user = assistant.getUser();
			if (null != user && (userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L1) || userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2))) {
				if(assistant.getAssistantRole().equals("Level1")){
					moduleName = ModuleUserService.APPROVED_ADMIN_STAFF_L1;
				} else {
					moduleName = ModuleUserService.APPROVED_ADMIN_STAFF_L2;
				}
				
				moduleUser = userService.findModuleUser(assistant.getId().intValue(), moduleName, user);
				
				if (moduleUser == null) {
					userService.createModuleUser(assistant.getId().intValue(), moduleName, user, true);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while creating module user for staff  :" , e);
			throw new GIRuntimeException("Error while creating module user for staff : ",e);
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public AgencyAssistant saveUserForAgencyAssistant(long assistantId, AccountUser user) {
		try {
			AgencyAssistant assistantDb = findById(assistantId);
			if(assistantDb!=null && assistantDb.getUser()==null){
				assistantDb.setUser(user);
				saveOrUpdate(assistantDb);
			}
			return assistantDb;
		} catch (Exception e) {
			LOGGER.error("Error while associating user for staff  :" , e);
			throw new GIRuntimeException("Error while associating user for staff : ",e);
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public AgencyAssistant saveAssistantInformation(AgencyAssistant assistant, AgencyAssistantInfoDto agencyAssistantInfoDto) { 
		AgencyAssistant savedAssistant = null;
		AccountUser accountUser = null;
		String changeType = null;
		try {
			accountUser = userService.getLoggedInUser();
			
			//check assistant id
			if(assistant.getId() == null || assistant.getId() == 0){
				//new record
				assistant.setApprovalStatus(ApprovalStatus.PENDING);
				if(assistant.getAssistantNumber() == null){
					assistant.setAssistantNumber("ST"+generateStaffId());
				}
				if(accountUser != null){
					assistant.setCreatedBy(Long.valueOf(accountUser.getId()));
				}
				assistant.setStatus("Active"); /* Default status for AdminStaff/Assistant is Active */
				assistant.setActivityMode("Enabled"); /*  Admin Staff/Assistant can be disabled when staff/assistant is converted to broker */
				setBusinessAddress(assistant, agencyAssistantInfoDto);
				savedAssistant = saveOrUpdate(assistant);
				
			}else{
				//find agencyAssistant record
				AgencyAssistant assistantDb = findById(assistant.getId());
				if(assistantDb !=  null){
					assistantDb.setFirstName(assistant.getFirstName());
					if(assistantDb.getUser()!=null){
						assistantDb.getUser().setFirstName(assistant.getFirstName());
					}
					assistantDb.setLastName(assistant.getLastName());
					if(assistantDb.getUser()!=null){
						assistantDb.getUser().setLastName(assistant.getLastName());
					}
					assistantDb.setPrimaryContactNumber(assistant.getPrimaryContactNumber());
					assistantDb.setBusinessContactNumber(assistant.getBusinessContactNumber());
					assistantDb.setPersonalEmailAddress(assistant.getPersonalEmailAddress());
					assistantDb.setBusinessEmailAddress(assistant.getBusinessEmailAddress());
					assistantDb.setCommunicationPreference(assistant.getCommunicationPreference());
					assistantDb.setBusinessLegalName(assistant.getBusinessLegalName());
					assistantDb.setAgency(assistant.getAgency());
					assistantDb.setLastUpdatedBy(Long.valueOf(accountUser.getId()));
					
					if(null != assistantDb.getUser() && !assistantDb.getAssistantRole().equals(assistant.getAssistantRole())) {
						UserRole userRole;
						Role role;
						if("Level1".equalsIgnoreCase(assistant.getAssistantRole())){
							userRole = userService.getUserRoleByRoleName(assistantDb.getUser(), RoleService.APPROVED_ADMIN_STAFF_L2);
							role = roleService.findRoleByName(RoleService.APPROVED_ADMIN_STAFF_L1);
							changeType = "AdminStaffL2ToAdminStaffL1";
						} else {
							userRole = userService.getUserRoleByRoleName(assistantDb.getUser(), RoleService.APPROVED_ADMIN_STAFF_L1);
							role = roleService.findRoleByName(RoleService.APPROVED_ADMIN_STAFF_L2);
							changeType = "AdminStaffL1ToAdminStaffL2";
						}
						if(null != userRole && null != role) {
							userRole.setRole(role);
							userRoleService.saveUserRole(userRole);
						}
					}
					setBusinessAddress(assistantDb, agencyAssistantInfoDto);
					assistantDb.setCorrespondenceAddress(assistant.getCorrespondenceAddress());
					assistantDb.setAssistantRole(assistant.getAssistantRole());
					
					savedAssistant = saveOrUpdate(assistantDb);
					
					if(changeType!=null){
						savedAssistant.setChangeType(changeType);
					}
				}
			}
			
			brokerIndTriggerService.triggerInd35(savedAssistant);
			
			return savedAssistant;
		}  catch (Exception e) {
			LOGGER.error("Error while saving assistant information  :" , e);
			throw new GIRuntimeException("Error while saving assistant information  : ",e);
		}
	}
	
	private void setBusinessAddress(AgencyAssistant assistant, AgencyAssistantInfoDto agencyAssistantInfoDto){
		if(agencyAssistantInfoDto != null && agencyAssistantInfoDto.getSiteId() != null){
			AgencySite agencySite = agencySiteService.findSiteById(Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantInfoDto.getSiteId())));
			if(agencySite != null){
				assistant.setBusinessAddress(agencySite.getLocation());
			}
		}
	}
	
	private String generateStaffId() {
		return ghixDBSequenceUtil.getNextSequenceFromDB(ASSISTANT_ID_SEQ);
	}

	@Override
	public AgencyAssistantListDto findAgencyAssistantsForAgency(AgencyAssistantSearchDto agencyAssistantSearchDto)  
	{
		LOGGER.info("findAgencyAssistantsForAgency : START");
		Integer startRecord = 0;
		AgencyAssistantDetailsDto agencyAssistantDetailsDto;
		List<AgencyAssistantDetailsDto> listOfAgencyAssistants = new ArrayList<>();
		AgencyAssistantListDto agencyAssistantListDto = new AgencyAssistantListDto();
		
		try {
			startRecord = (agencyAssistantSearchDto!=null ? (agencyAssistantSearchDto.getPageNumber()!=null ? (agencyAssistantSearchDto.getPageNumber() - 1) : 1) : 1) * PAGE_SIZE;

			QueryBuilder<AgencyAssistant> query = delegateFactory.getObject();

			query.buildSelectQuery(AgencyAssistant.class, Arrays.asList("user"));

			query.applySelectColumns(Arrays.asList(ID, FIRST_NAME, LAST_NAME, STATUS, APPROVAL_STATUS, STAFF_ID, ASSISTANT_ROLE, USER_FIRST_NAME, USER_LAST_NAME));

			query.applyWhere(AGENCY_ID, Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantSearchDto.getAgencyId())), DataType.LONG, ComparisonType.EQ);
			
			createCriteriaQueryForAgencyAssistant(agencyAssistantSearchDto, query);
			
			if(StringUtils.isEmpty(agencyAssistantSearchDto.getSortBy()) || "staffName".equalsIgnoreCase(agencyAssistantSearchDto.getSortBy())){
				List<String> columnNames = new ArrayList<String>();
				columnNames.add(FIRST_NAME);
				columnNames.add(LAST_NAME);
				query.applySortList(columnNames, QueryBuilder.SortOrder.valueOf(agencyAssistantSearchDto.getSortOrder()));
			} else {
				query.applySort(agencyAssistantSearchDto.getSortBy(), QueryBuilder.SortOrder.valueOf(agencyAssistantSearchDto.getSortOrder()));
			}
			
			List<AgencyAssistant>  records = query.getRecords(startRecord, PAGE_SIZE);
			Iterator iterator = records.iterator();
			while(iterator.hasNext()){
				agencyAssistantDetailsDto = new AgencyAssistantDetailsDto();
				Object[] objArray = (Object[]) iterator.next();
				if (objArray[0] != null) {
					agencyAssistantDetailsDto.setEncryptedId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(objArray[0])));
				}
				if (objArray[1] != null) {
					if(objArray[7] != null) {
						agencyAssistantDetailsDto.setFirstName(objArray[7].toString());
					}else{
						agencyAssistantDetailsDto.setFirstName(objArray[1].toString());
					}
				}
				if (objArray[2] != null) {
					if(objArray[8] != null) {
						agencyAssistantDetailsDto.setLastName(objArray[8].toString());
					}else{
						agencyAssistantDetailsDto.setLastName(objArray[2].toString());
					}
				}
				if (objArray[3] != null) {
					agencyAssistantDetailsDto.setStatus(objArray[3].toString());
				}
				if (objArray[4] != null) {
					AgencyAssistant.ApprovalStatus status = (AgencyAssistant.ApprovalStatus)objArray[4];
					agencyAssistantDetailsDto.setApprovalStatus(status.getValue());
				}
				if (objArray[5] != null) {
					agencyAssistantDetailsDto.setStaffId(objArray[5].toString());
				}
				if (objArray[6] != null) {
					agencyAssistantDetailsDto.setRole(objArray[6].toString());
				}
				listOfAgencyAssistants.add(agencyAssistantDetailsDto);
			}
			
			agencyAssistantListDto.setAgencyAssistants(listOfAgencyAssistants);
			if(query.getRecordCount() != null){
				agencyAssistantListDto.setCount(query.getRecordCount());
			}
		} catch(Exception exception) {
    		LOGGER.error("Exception occured while fetching Agency Assistants For Agency : ", exception);
    		throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} finally {
			LOGGER.info("findAgencyAssistantsForAgency : END");
		}

		return agencyAssistantListDto;
	}
	
	private void createCriteriaQueryForAgencyAssistant(AgencyAssistantSearchDto agencyAssistantSearchDto, QueryBuilder<AgencyAssistant> query) {

		String firstName = (agencyAssistantSearchDto != null) ? agencyAssistantSearchDto.getFirstName() : EMPTY_STRING;
		if (StringUtils.isNotBlank(firstName)) {
			addLikeWithConjunctionOr(query, USER_FIRST_NAME, firstName,FIRST_NAME, firstName);
		}

		String lastName = (agencyAssistantSearchDto != null) ? agencyAssistantSearchDto.getLastName() : EMPTY_STRING;
		if (StringUtils.isNotBlank(lastName)) {
			addLikeWithConjunctionOr(query, USER_LAST_NAME, lastName, LAST_NAME, lastName);
		}
		
		String businessLegalName = (agencyAssistantSearchDto != null) ? agencyAssistantSearchDto.getBusinessLegalName() : EMPTY_STRING;
		if (StringUtils.isNotBlank(businessLegalName)) {
			query.applyWhere(BUSINESS_LEGAL_NAME, businessLegalName.toUpperCase(), DataType.STRING, ComparisonType.LIKE);
		}
		
		String staffId = (agencyAssistantSearchDto != null) ? agencyAssistantSearchDto.getStaffId() : EMPTY_STRING;
		if (StringUtils.isNotBlank(staffId)) {
			query.applyWhere(STAFF_ID, staffId.toUpperCase(), DataType.STRING, ComparisonType.LIKE);
		}
		
		String status = ((agencyAssistantSearchDto != null) ? (agencyAssistantSearchDto.getApprovalStatus() != null ? agencyAssistantSearchDto.getApprovalStatus() : EMPTY_STRING) : EMPTY_STRING);
		if (StringUtils.isNotBlank(status)) {
			query.applyWhere(APPROVAL_STATUS, AgencyAssistant.ApprovalStatus.getApprovalStatus(status), DataType.ENUM, ComparisonType.LIKE);
		} else {
			query.applyWhere(APPROVAL_STATUS, status, DataType.STRING, ComparisonType.ISNOTNULL);
		}
	}

	@Override
	public AgencyAssistantListDto findAgencyAssistantsForAdmin(AgencyAssistantSearchDto agencyAssistantSearchDto)  
	{
		LOGGER.info("findAgencyAssistantsForAdmin : START");
		SimpleDateFormat dateFormat = new SimpleDateFormat(UI_DATE_FORMAT);
		Integer startRecord = 0;
		AgencyAssistantDetailsDto agencyAssistantDetailsDto;
		List<AgencyAssistantDetailsDto> listOfAgencyAssistants = new ArrayList<>();
		AgencyAssistantListDto agencyAssistantListDto = new AgencyAssistantListDto();
		
		try {
			startRecord = (agencyAssistantSearchDto!=null ? (agencyAssistantSearchDto.getPageNumber()!=null ? (agencyAssistantSearchDto.getPageNumber() - 1) : 1) : 1) * PAGE_SIZE;

			QueryBuilder<AgencyAssistant> query = delegateFactory.getObject();

			query.buildSelectQuery(AgencyAssistant.class, Arrays.asList("user"));

			query.applySelectColumns(Arrays.asList(ID, FIRST_NAME, LAST_NAME, BUSINESS_LEGAL_NAME, STAFF_ID, APPROVED_ON, APPROVAL_STATUS, ASSISTANT_ROLE, USER_FIRST_NAME, USER_LAST_NAME));

			createCriteriaQueryForAgencyAssistant(agencyAssistantSearchDto, query);
			
			if(StringUtils.isEmpty(agencyAssistantSearchDto.getSortBy()) || "staffName".equalsIgnoreCase(agencyAssistantSearchDto.getSortBy())){
				List<String> columnNames = new ArrayList<String>();
				columnNames.add(FIRST_NAME);
				columnNames.add(LAST_NAME);
//				columnNames.add(USER_FIRST_NAME);
//				columnNames.add(USER_LAST_NAME);
				query.applySortList(columnNames, QueryBuilder.SortOrder.valueOf(agencyAssistantSearchDto.getSortOrder()));
			} else {
				query.applySort(agencyAssistantSearchDto.getSortBy(), QueryBuilder.SortOrder.valueOf(agencyAssistantSearchDto.getSortOrder()));
			}
			
			List<AgencyAssistant>  records = query.getRecords(startRecord, PAGE_SIZE);
			Iterator iterator = records.iterator();
			while(iterator.hasNext()){
				agencyAssistantDetailsDto = new AgencyAssistantDetailsDto();
				Object[] objArray = (Object[]) iterator.next();
				if (objArray[0] != null) {
					agencyAssistantDetailsDto.setEncryptedId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(objArray[0])));
				}
				if (objArray[1] != null){
					if(objArray[8] != null) {
						agencyAssistantDetailsDto.setFirstName(objArray[8].toString());
					}else{
						agencyAssistantDetailsDto.setFirstName(objArray[1].toString());
					}
				}
				if (objArray[2] != null){
					if(objArray[9] != null) {
						agencyAssistantDetailsDto.setLastName(objArray[9].toString());
					}else{
						agencyAssistantDetailsDto.setLastName(objArray[2].toString());
					}
				}
				if (objArray[3] != null) {
					agencyAssistantDetailsDto.setBusinessLegalName(objArray[3].toString());
				}
				if (objArray[4] != null) {
					agencyAssistantDetailsDto.setStaffId(objArray[4].toString());
				}
				if (objArray[5] != null) {
					agencyAssistantDetailsDto.setApprovedOn(dateFormat.format(objArray[5]));
				}
				if (objArray[6] != null) {
					AgencyAssistant.ApprovalStatus status = (AgencyAssistant.ApprovalStatus)objArray[6];
					agencyAssistantDetailsDto.setApprovalStatus(status.getValue());
				}
				if (objArray[7] != null) {
					agencyAssistantDetailsDto.setRole(objArray[7].toString());
				}
				listOfAgencyAssistants.add(agencyAssistantDetailsDto);
			}
			
			agencyAssistantListDto.setAgencyAssistants(listOfAgencyAssistants);
			if(query.getRecordCount() != null){
				agencyAssistantListDto.setCount(query.getRecordCount());
			}
		} catch(Exception exception) {
    		LOGGER.error("Exception occured while fetching Agency Assistants For Admin : ", exception);
    		throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} finally {
			LOGGER.info("findAgencyAssistantsForAdmin : END");
		}

		return agencyAssistantListDto;
	}
	
	@Override
	public AgencyAssistantApprovalDto loadAgencyAssistantApprovalHistory(Long agencyAssistantId) {
		List<AgencyAssistantApprovalStatusHistory> historyList = new LinkedList<>();
		AgencyAssistantApprovalDto agencyAssistantApprovalDto = new AgencyAssistantApprovalDto();
		SimpleDateFormat dateFormat = new SimpleDateFormat(UI_DATE_FORMAT);
		
		try {
			AgencyAssistant agencyAssistant = iAgencyAssistantRepository.findById(agencyAssistantId);
			if (agencyAssistant.getApprovalStatus()!=null){
				agencyAssistantApprovalDto.setApprovalStatus(agencyAssistant.getApprovalStatus().getValue());
			}
			if (agencyAssistant.getApprovalNumber() != null) {
				agencyAssistantApprovalDto.setApprovalNumber(agencyAssistant.getApprovalNumber().toString());
			}
			if(agencyAssistant.getApprovalDate()!=null){
				agencyAssistantApprovalDto.setApprovalDate(dateFormat.format(agencyAssistant.getApprovalDate()));
			}
			if(agencyAssistant.getCreationDate()!=null){
				agencyAssistantApprovalDto.setApplicationSubmissionDate(dateFormat.format(agencyAssistant.getCreationDate()));
			}
			if(agencyAssistant.getAssistantNumber()!=null){
				agencyAssistantApprovalDto.setAssistantNumber(agencyAssistant.getAssistantNumber());
			}
			if(agencyAssistant.getDelegationCode()!=null){
				agencyAssistantApprovalDto.setDelegationCode(agencyAssistant.getDelegationCode());
			}
			
			List<Object[]> certificationHistory = iAgencyAssistantAudRepository.getAgencyAssistantApprovalHistory(agencyAssistantId);
			
			if(certificationHistory!=null && certificationHistory.size()>1){
				for(int i=1;i<=certificationHistory.size()-1;i++){
					Object[] currentElement = (Object[]) certificationHistory.get(i);
					Object[] previousElement = (Object[]) certificationHistory.get(i-1);
					if(!checkDuplicateStatus(currentElement, previousElement)){
						AgencyAssistantApprovalStatusHistory agencyAssistantApprovalStatusHistory = new AgencyAssistantApprovalStatusHistory();
						
						if (currentElement[0] != null) {
							agencyAssistantApprovalStatusHistory.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(currentElement[0].toString()));
						}
						if (currentElement[1] != null) {
							SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_1);
							agencyAssistantApprovalStatusHistory.setApprovalDate(new SimpleDateFormat(DATE_FORMAT_2).format(sdf.parse(currentElement[1].toString())));
						}
						if (currentElement[2] != null) {
							agencyAssistantApprovalStatusHistory.setPreviousStatus(AgencyAssistant.ApprovalStatus.valueOf(previousElement[2].toString()).getValue());
							agencyAssistantApprovalStatusHistory.setNewStatus(AgencyAssistant.ApprovalStatus.valueOf(currentElement[2].toString()).getValue());
						}
						if (currentElement[3] != null) {
							agencyAssistantApprovalStatusHistory.setComment(currentElement[3].toString());
						}
						
						historyList.add(agencyAssistantApprovalStatusHistory);
					}
				}
				Collections.reverse(historyList);
				agencyAssistantApprovalDto.setApprovalStatusHistory(historyList);
			}
			
			agencyAssistantApprovalDto.setAssitantApprovalStatusList(getAgencyAssistantApprovalStatusList());
		} catch (Exception exception) {
			LOGGER.error("Exception while fetching agency assistant approval history : " + exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} 
		
		return agencyAssistantApprovalDto;
	}
	
	private boolean checkDuplicateStatus(Object[] currentElement, Object[] previousElement){
		boolean flag = false;
		if(currentElement[2].toString().equalsIgnoreCase(previousElement[2].toString())){
			if((currentElement[3]==null && previousElement[3]==null)){
				flag = true;
			} else if(currentElement[3]!=null && previousElement[3]!=null && currentElement[3].toString().equalsIgnoreCase(previousElement[3].toString())){
				flag = true;
			}
		}
		return flag;
	}
	
	private List<String> getAgencyAssistantApprovalStatusList() {
		List<String> statusList = new ArrayList<String>();
		statusList.add("Pending");
		statusList.add("Approved");
		statusList.add("Eligible");
		statusList.add("Denied");
		statusList.add("Terminated");
		statusList.add("Terminated-For-Cause");
		
		return statusList;
	}
	
	@Override
	@Transactional
	public AgencyAssistant updateActivityStatus(AgencyAssistantStatusDto agencyAssistantStatusDto){
		AgencyAssistant savedAssistant = null;
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
			if(agencyAssistantStatusDto != null && agencyAssistantStatusDto.getId()!= null && !agencyAssistantStatusDto.getId().isEmpty()) {
				AgencyAssistant agencyAssistant = findById(Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantStatusDto.getId())));
				if (agencyAssistant != null){
					agencyAssistant.setStatus(agencyAssistantStatusDto.getStatus());
					agencyAssistant.setLastUpdatedBy(Long.valueOf(user.getId()));
					//setting comments 
					if(!BrokerUtils.isEmpty(agencyAssistantStatusDto.getComments())){
						Comment comment = agencyUtils.saveComments(String.valueOf(agencyAssistant.getId()), agencyAssistantStatusDto.getComments());
						if(comment!=null){
							agencyAssistant.setActivityCommentsId(Long.valueOf(comment.getId()));
						}
					} else {
						agencyAssistant.setActivityCommentsId(null);
					}
					savedAssistant = saveOrUpdate(agencyAssistant);
				}
			}
		}catch (Exception e) {
			LOGGER.error("Error while saving assistant status  :" , e);
			throw new GIRuntimeException("Error while saving assistant status  : ",e);
		}
		 
		return savedAssistant;
	}
	
	@Override
	@Transactional
	public AgencyAssistant updateApprovalStatus(AgencyAssistantApprovalDto agencyAssistantApprovalDto){
		AgencyAssistant savedAssistant = null;
		AccountUser user = null;
		Long assistantId = null;
		
		try {
			user = userService.getLoggedInUser();
			if(userService.hasUserRole(user, RoleService.BROKER_ADMIN_ROLE)){
				if(agencyAssistantApprovalDto != null && agencyAssistantApprovalDto.getId()!= null && !agencyAssistantApprovalDto.getId().isEmpty()) {
					assistantId = Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantApprovalDto.getId()));
					AgencyAssistant agencyAssistant = findById(assistantId);
					if (agencyAssistant != null){
						agencyAssistant.setApprovalStatus(ApprovalStatus.getApprovalStatus(agencyAssistantApprovalDto.getApprovalStatus()));
						if(ApprovalStatus.APPROVED.toString().equalsIgnoreCase(agencyAssistantApprovalDto.getApprovalStatus())){
							agencyAssistant.setApprovalNumber(Long.valueOf(generateAssitantApprovalNumber()));
							agencyAssistant.setApprovalDate(new TSTimestamp());
						}
						
						agencyAssistant.setLastUpdatedBy(Long.valueOf(user.getId()));
						
						//setting comments
						if(!BrokerUtils.isEmpty(agencyAssistantApprovalDto.getComment())){
							Comment comment = agencyUtils.saveComments(String.valueOf(assistantId), agencyAssistantApprovalDto.getComment());
							if(comment!=null){
								agencyAssistant.setCommentsId(Long.valueOf(comment.getId()));
							}
						} else {
							agencyAssistant.setCommentsId(null);
						}
						
						savedAssistant = saveOrUpdate(agencyAssistant);
						
						brokerIndTriggerService.triggerInd35(savedAssistant);
						
						if (savedAssistant != null && savedAssistant.getUser()==null) {
							if(savedAssistant.getApprovalStatus().toString().contains(ApprovalStatus.APPROVED.toString())){
								brokerIndTriggerService.triggerInd54(savedAssistant);
							}
						}
					}
				}
			}
		}catch (Exception e) {
			LOGGER.error("Error while saving assistant status  :" , e);
			throw new GIRuntimeException("Error while saving assistant status  : ",e);
		}
		 
		return savedAssistant;
	}
	
	@Override
	public AgencyAssistantStatusDto loadAgencyAssistantStatusHistory(Long agencyAssistantId) {
		List<AgencyAssistantStatusHistory> historyList = new LinkedList<>();
		AgencyAssistantStatusDto assistantStatusDto = null;
		List<String> statusList = new ArrayList<String>();
		statusList.add("Active");
		statusList.add("InActive");
		
		try {
			AgencyAssistant assistant = iAgencyAssistantRepository.findById(agencyAssistantId);
			if(assistant!=null){
				assistantStatusDto = agencyDtoMapper.convert(AgencyAssistantStatusDto.class,assistant);
				if(assistantStatusDto!=null){
					assistantStatusDto.setAssitantStatusList(statusList);
				}
			}
			
			List<Object[]> statusHistory = iAgencyAssistantAudRepository.getAgencyAssistantStatusHistory(agencyAssistantId);
			
			if(statusHistory!=null && statusHistory.size()>1){
				for(int i=1;i<=statusHistory.size()-1;i++){
					Object[] currentElement = (Object[]) statusHistory.get(i);
					Object[] previousElement = (Object[]) statusHistory.get(i-1);
					if(!checkDuplicateStatus(currentElement, previousElement)){
						AgencyAssistantStatusHistory assistantStatusHistory = new AgencyAssistantStatusHistory();
						
						if (currentElement[0] != null) {
							assistantStatusHistory.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(currentElement[0].toString()));
						}
						if (currentElement[1] != null) {
							SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_1);
							assistantStatusHistory.setStatusDate(new SimpleDateFormat(DATE_FORMAT_2).format(sdf.parse(currentElement[1].toString())));
						}
						if (currentElement[2] != null) {
							assistantStatusHistory.setPreviousStatus(previousElement[2].toString());
							assistantStatusHistory.setNewStatus(currentElement[2].toString());
						}
						if (currentElement[3] != null) {
							assistantStatusHistory.setComment(currentElement[3].toString());
						}
						
						historyList.add(assistantStatusHistory);
					}
				}
				Collections.reverse(historyList);
				assistantStatusDto.setStatusHistory(historyList);
			}
			
		} catch (Exception exception) {
			LOGGER.error("Exception occured while fetching assistant status history. " + exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} 
		
		return assistantStatusDto;
	}
	
	private String generateAssitantApprovalNumber() {
		return ghixDBSequenceUtil.getNextSequenceFromDB(ASSISTANT_APR_SEQ);
	}
	
	@Override
	public AgencyAssistantInfoDto getAgencySitesForAssistant(AgencyAssistant assistant){
		AgencyAssistantInfoDto agencyAssistantInfoDto = null;
		List<AgencySiteLocationDto> agencySiteDtos = null;
		List<AgencySite> agencySites = null;
		if(assistant!=null){
			agencySites = agencySiteService.getSiteByAgencyId(assistant.getAgency().getId());
			agencyAssistantInfoDto = agencyDtoMapper.convert(AgencyAssistantInfoDto.class,assistant);
			agencySiteDtos = agencyDtoMapper.convertCollection(AgencySiteLocationDto.class, agencySites);
			agencyAssistantInfoDto.setAgencySites(agencySiteDtos);
		}
		return agencyAssistantInfoDto;
	}
	
	private void addLikeWithConjunctionOr(QueryBuilder<?> queryBuilder,String columnName,String value,String orColumnName,String orValue){
		Predicate predicate =  getLikePredicate(queryBuilder, columnName, value);
		Predicate orPredicate =  getLikePredicate(queryBuilder, orColumnName, orValue);
		Predicate conjunctionPredicate = queryBuilder.getBuilder().or(predicate,orPredicate);
		queryBuilder.updateCriteriaPredicate(conjunctionPredicate);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Predicate getLikePredicate(QueryBuilder<?> queryBuilder,String columnName,String value){
		value = value.replace(LIKE_ESCAPE_CHAR,LIKE_ESCAPE_CHAR+LIKE_ESCAPE_CHAR).replace("_", ESCAPED_CHAR_UNDERSCORE).replace("%", ESCAPED_CHAR_PERCENTAGE);
		Expression colName = queryBuilder.getPath(columnName);
		Predicate likePredicate = queryBuilder.getBuilder().like(queryBuilder.getBuilder().upper(colName), PERCENTAGE+value.toUpperCase()+PERCENTAGE,'\\');
		return likePredicate;
	}
	
}
