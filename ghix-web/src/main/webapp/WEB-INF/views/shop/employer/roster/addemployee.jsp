<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/employer-registration-util.tld" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<style>
#mainSubmitButton{
	margin:0px !important;
}
</style>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datepicker.css"/>" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-template.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-address-util.js" />"></script>

<script type="text/javascript">
	/* function shiftbox(element, nextelement) {
		maxlength = parseInt(element.getAttribute('maxlength'));
		if (element.value.length == maxlength) {
			nextelement = document.getElementById(nextelement);
			nextelement.focus();
		}
	} */

	jQuery.validator.addMethod("namecheck", function(value, element, param) {
	  	if(namecheck(value) == false){
		  	return false;	
		}
		return true;
	});
	

	jQuery.validator.addMethod("phonecheck", function(value, element, param) {
		 phone1 = $("#phone1").val(); 
	 	 phone2 = $("#phone2").val(); 
	 	 phone3 = $("#phone3").val(); 
	   	var intPhone1= parseInt( phone1);
	  	if( 
	  			(phone1 == "" || phone2 == "" || phone3 == "")  || 
	  			(isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3)) ||  
	  			(phone1.length != 3 || phone2.length != 3 || phone3.length != 4)){ 
	  		return false; 
	  	}else if(intPhone1<100){
	  		return false;
	  	} 
	  	
		var phonenumber = $("#phone1").val() + $("#phone2").val() + $("#phone3").val();
			
		var phoneno = /^\d{10}$/;
		  if(phonenumber.match(phoneno))      {
	        return true;
	      }
		return false;
	});
	
	jQuery.validator.addMethod("ssn1check", function(value, element, param) {
		ssn1 = $("#ssn1").val();
		return areaCodeCheck(ssn1); 
			
		 });
		 
		 jQuery.validator.addMethod("invalidssn1", function(value, element, param) {
			    ssn1 = $("#ssn1").val();
				ssn2 = $("#ssn2").val();
				ssn3 = $("#ssn3").val(); 
				return notAllowedSSN1(ssn1,ssn2,ssn3);
			 });
		 jQuery.validator.addMethod("invalidssn2", function(value, element, param) {
			    ssn1 = $("#ssn1").val();
				ssn2 = $("#ssn2").val();
				ssn3 = $("#ssn3").val(); 
				return notAllowedSSN2(ssn1,ssn2,ssn3);
			 });


		jQuery.validator.addMethod("ssncheck", function(value, element, param) {
			ssn1 = $("#ssn1").val();
			ssn2 = $("#ssn2").val();
			ssn3 = $("#ssn3").val();
		 	return validateSSN(ssn1,ssn2,ssn3);
		});

			jQuery.validator.addMethod("validateTaxId", function(value, element, param) {
			ssn1 = $("#ssn1").val();
			ssn2 = $("#ssn2").val();
			ssn3 = $("#ssn3").val(); 
			currentSSN=ssn1+'-'+ssn2+'-'+ssn3;

			var csrfParameter = "";
			csrfParameter ='<c:url value="">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
					
			return isUniqueSSN(currentSSN, 0,0,'employee',csrfParameter);
			
			});

	jQuery.validator.addMethod("zipcheck", function(value, element, param) {
	  	elementId = element.id; 
	  	var numzip = new Number($("#"+elementId).val());
	    var filter = /^[0-9]+$/;
		if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() )|| numzip<=0 || (!filter.test($("#"+elementId).val()))){
	  		return false;	
	  	}return true;
	});

	function submitLocation() {
		var subUrl = '<c:url value="/shop/employer/roster/addWorksites">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';

		$.ajax({
			type : "POST",
			url : subUrl,
			data : $("#frmworksites").serialize(),
			dataType : 'json',
			beforeSend: function( xhr ) {
				$('#submitbutton').attr("disabled", true);
			},			
			success : function(response,xhr) {
		        if(isInvalidCSRFToken(xhr))
		          return;
				populateLocations(response);
				application.remove_modal();
				$('#submitbutton').attr("disabled", false);				
			},
			error : function(e) {
				alert("<spring:message code='label.failedtoaddaddr' javaScriptEscape='true'/>");
				$('#submitbutton').attr("disabled", false);				
			}
		});
	}

	/* function validateAddressWS() {
		var enteredAddress = $("#address1_0").val()+","+$("#address2_0").val()+","+$("#city_0").val()+","+$("#state_0").val()+","+$("#zip_0").val()+ ", " +  $("#lat_0").val() + "," + $("#lon_0").val();
		var idsText='address1_0~address2_0~city_0~state_0~zip_0~lat_0~lon_0';
		$.ajax({
					url : "/hix/platform/validateaddress",
					data : {
						enteredAddress : enteredAddress,
						ids : idsText
					},
					success : function(data) {
						var href = '/hix/platform/address/viewvalidaddress';
						$('#addressIFrame').remove(); //remove any present modal 
						if (data == "SUCCESS") {
							$(
									'<div class="modal" id="addressIFrame" tabindex="-1" role="dialog"><div class="modal-header" style="border-bottom:0;"></div><div class="modal-body"><iframe id="modalData" src="'
											+ href
											+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:380px;"></iframe></div><div class="modal-footer"><input type="button" value="Close" class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"></div></div>')
									.modal({
										backdrop : false
									});
							$('#iFrameClose').bind("click", submitLocation);
						} else {
							submitLocation();
						}
					}
				});
	} */

	function validateLocation() {
		if ($("#frmworksites").validate().form()) {
			//validateAddressWS();
			submitLocation();
		}
	}

	/* function validateHomeAddressWS() {
		var enteredAddress = $("#address1").val()+","+$("#address2").val()+","+$("#city").val()+","+$("#state").val()+","+$("#zip").val()+ ", " +  $("#lat").val() + "," + $("#lon").val();
		var idsText='address1~address2~city~state~zip~lat~lon';
		$.ajax({
					url : "/hix/platform/validateaddress",
					data : {
						enteredAddress : enteredAddress,
						ids : idsText
					},
					success : function(data) {
						var href = '/hix/platform/address/viewvalidaddress';
						$('#addressIFrame').remove(); //remove any present modal 
						if (data == "SUCCESS") {
							$(
									'<div class="modal" id="addressIFrame"><div class="modal-header" style="border-bottom:0;"></div><div class="modal-body"><iframe id="modalData" src="'
											+ href
											+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:380px;"></iframe></div><div class="modal-footer"><input type="button" value="Close" class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"></div></div>')
									.modal({
										backdrop : false
									});
							$('#iFrameClose').bind("click",
									submitFrmADDEmployee);
						} else {
							submitFrmADDEmployee();
						}
					}
				});
	} */

	function submitFrmADDEmployee() {
		/*to check the text for address line 2 for IE browsers*/
		$("input[id^=address2_]").each(function(){
			if($(this).val() === "Address Line 2"){
				$(this).val("");
			}
		});	
		$('input:button').attr("disabled", true);
		$("#frmaddemployee").submit();
	}

	function validateForm(btnValue) {
		$("#clickedBtnname").val(btnValue);
		var submittedData = $("#frmaddemployee").serialize();
		if(btnValue == 'Done' &&  defaultData == submittedData){
			window.location.href = "review";
		}else{
			$("#frmaddemployee").valid();
			if ($("#frmaddemployee").validate().form()) {
				var name = $("#firstName").val() + " " + $("#lastName").val();
				var ssn = $("#ssn1").val() + "-" + $("#ssn2").val() + "-"
						+ $("#ssn3").val();

				var phone = $("#phone1").val() + "-" + $("#phone2").val() + "-" + $("#phone3").val();
				
				$("#contactNumber").val(phone);
				$("#name").val(name);
				$('#ssn').val(ssn);

				//validateHomeAddressWS();
				submitFrmADDEmployee();
			}
		}
	}

	jQuery.validator.addMethod("dobcheck", function(value, element, param) {
		var today=new Date();
		var dob = $("#dob").val();
	    dob_arr = dob.split(/\//);
		dob_mm = dob_arr[0]; dob_dd = dob_arr[1]; dob_yy = dob_arr[2];
		var birthDate=new Date();
		birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
		if( (today.getFullYear() - 100) >  birthDate.getFullYear() ) { return false; }
		if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) { return false; }
		if(today.getTime() <= birthDate.getTime()){ return false; }
		
		return true;
	});

	/*function postPopulateIndex(indexValue, zipCodeValue){
		console.log(indexValue, zipCodeValue);
		
		$('#county_' + indexValue).focus();
	}*/

	function getCountyList(eIndex, zip, county) {
		var subUrl = '<c:url value="/shop/employer/addCounties">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
		$.ajax({
			type : "POST",
			url : subUrl,
			data : {zipCode : zip}, 
			dataType:'json',
			success : function(response,xhr) {
		        if(isInvalidCSRFToken(xhr))
		          return;
				populateCounties(response, eIndex, county);
			},error : function(e) {
				alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
			}
		});
	}

	function populateCounties(response, eIndex,county){
		
		if(!startsWith(eIndex,'_')){
			eIndex = '_' + eIndex;
		}
		
		$('#county'+eIndex).html(''); 
		var optionsstring = '<option value="">Select County...</option>';
		var i =0;
		$.each(response, function(key, value) {
			var optionVal = key+'#'+value;
			var selected = (county == key) ? 'selected' : '';
			var options = '<option value="'+optionVal+'" '+ selected +'>'+ key +'</option>';
			optionsstring = optionsstring + options;
			i++;
		});
		$('#county'+eIndex).html(optionsstring);
	}

	function startsWith(str, prefix) {
	    return str.lastIndexOf(prefix, 0) === 0;
	}
</script>

<!--titlebar-->
<div class="gutter10">
	<div class="row-fluid">
		<div class="span9">
			<h1 id="skip">
				<spring:message  code="label.employerroasterinfoh1"/> <small> ${employeeCount} <spring:message  code="label.emprroasteraddemph2"/></small>
			</h1>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<util:nav pageName="Add Employees" navStepNo="2"></util:nav>
		</div>
		<!--sidebar-->
		<div class="span9" id="rightpanel">
			<div class="row-fluid">
				<div class="header">
					<h4><spring:message  code="label.emprroasteraddemph3"/> 
						<%-- <input type="button" name="mainSubmitButton" id="mainSubmitButton" onclick="javascript:validateForm('Done');" class="btn btn-small pull-right" value="<spring:message  code="label.donebtn"/>"> --%>
						<a type="button" tabindex="0" name="mainSubmitButton" id="mainSubmitButton" onclick="javascript:validateForm('Done');" class="btn btn-small pull-right"><spring:message  code="label.donebtn"/></a>
					</h4>
				</div>
				<div class="gutter10 ">
					
					<form class="form-horizontal" id="frmaddemployee"
						name="frmaddemployee" action="addemployeeSubmit" method="post">
						<df:csrfToken/>
						<input type="hidden" id="name" name="name" value="" /> <input
							type="hidden" name="employeeDetails[0].ssn" id="ssn" /> <input
							type="hidden" name="employeeDetails[0].contactNumber"
							id="contactNumber" /> <input type="hidden"
							name="employeeDetails[0].type" id="type" value="EMPLOYEE" /> <input
							type="hidden" id="id" name="id" value="${employee_id}" /> <input
							type="hidden" id="clickedBtnname" name="clickedBtnname" value="Done" /> <input
							type="hidden" id="employer_id" name="employer.id"
							value="${employer_id}" /> 
						<h4 class="lightgray"><spring:message  code="label.emprroasteraddemph4"/></h4>
						<div class="control-group">
							<label class="control-label" for="firstName"><spring:message  code='label.emplFirstName'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" id="firstName"
									name="employeeDetails[0].firstName" /> <span
									id="firstName_error"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="lastName"><spring:message  code='label.emplLastName'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" id="lastName"
									name="employeeDetails[0].lastName" /> <span id="lastName_error"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="location"><spring:message  code='label.employerworksiteaddr'/> </label>
							<div class="controls">
								<select name="employerLocation.id" id="location"></select> 
								<%-- <a
									href="#worksite" id="worksiteLink" class="gutter10" data-toggle="modal"><span><i
										class="icon-plus-sign"></i> <spring:message  code='label.employernewworksiteaddr'/></span></a> --%>
								
								<small>
									<a href="javascript:void(0)" id="worksite_ModalLink" role="button" class="" data-toggle="modal">
									<i class="icon-plus-sign"></i>
									<spring:message  code="label.employeraddworksiteaddr"/></a>
								</small>
							</div>
						</div>
						<div class="control-group" style="display:none;">
							<label class="control-label" for="isOwner"><spring:message  code='label.emprroasteraddemph5'/></label>
							<div class="controls">
								<label class="radio inline"><input type="radio"
									name="isOwner" id="isOwnerYes" value="YES"> Yes</label> <label
									class="radio inline"><input type="radio" name="isOwner"
									id="isOwnerNo" value="NO" checked> No </label>
							</div>
						</div>
						<div class="control-group" style="display:none;">
							<label class="control-label" for="employmentDate"><spring:message  code='label.employmentdate'/></label>
							<div class="controls">
								<div class="input-append date date-picker"
									data-date="" data-date-format="mm/dd/yyyy">
									<input class="span10" type="text" name="employmentDate"
										id="employmentDate"> <span class="add-on"><i
										class="icon-calendar"></i></span>
								</div>
								<span id="employmentDate_error"></span>
							</div>
						</div>
						
						<h4 class="lightgray"><spring:message code='label.emprroasteraddemph7'/></h4>
						<div class="control-group">
							<label class="control-label" for="dob"><spring:message  code='label.dob'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<div class="input-append date date-picker"
									data-date="" data-date-format="mm/dd/yyyy">
									<input class="span10" type="text" name="employeeDetails[0].dob"
										id="dob"> <span class="add-on"><i class="icon-calendar"></i></span>
								</div>
								<span id="dob_error"></span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="ssn1">
							 <spring:message  code='label.taxidnumber'/>
							  <span class="aria-hidden">
								<spring:message  code='label.firstSSN'/></span>
								<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />&nbsp; 
								<a href="javascript:void(0)" tabindex="-1" rel="tooltip" data-placement="top" data-original-title="<spring:message  code='label.taxidnumbertooltip'/>" class="info"> 
									<i class="icon-question-sign"></i> 
								</a>
							</label>				
							<div class="controls">
								<input type="text" class="span2 inline" name="ssn1" id="ssn1"
									maxlength="3" />
								<span id="ssn1_error"></span>
								<label class="aria-hidden" for="ssn2"><spring:message  code='label.taxidnumber'/> <spring:message  code='label.secondSSN'/><spring:message  code='label.required'/></label>	
								<input
									type="text" class="span1 inline" name="ssn2" id="ssn2"
									maxlength="2"  /> 
								<label class="aria-hidden" for="ssn3"><spring:message  code='label.taxidnumber'/> <spring:message  code='label.thirdSSN'/><spring:message  code='label.required'/></label>	
								<input
									type="text" class="span2 inline" name="ssn3" id="ssn3"
									maxlength="4" /> <span id="ssn3_error"></span>
							</div>
						</div>
						
						<c:set var="tobaccoDisplay" value="" />
						<c:if test="${displayTobaccoUse == 'HIDE'}">
						 <c:set var="tobaccoDisplay" value="style=\"display:none;\"" />
						</c:if>
						<div class="control-group" ${tobaccoDisplay} >
							<label class="control-label" for="smoker"><spring:message  code='label.tobacoUser'/></label>
							<div class="controls">
								<label class="radio inline"> <input type="radio"
									name="employeeDetails[0].smoker" id="smokerYes" value="YES">
									Yes
								</label> <span id="smokerYes_error"></span>
								<label class="radio inline"> <input type="radio"
									name="employeeDetails[0].smoker" id="smokerNo" value="NO">
									No
								</label> <span id="smokerNo_error"></span>
							</div>
						</div>
						<c:set var="nativeAmrDisplay" value="" />
						<c:if test="${displayNativeAmerican == 'HIDE'}">
						 <c:set var="nativeAmrDisplay" value="style=\"display:none;\"" />
						</c:if>
						<div class="control-group" ${nativeAmrDisplay}>
						<label class="control-label" for="nativeAmr"><spring:message  code='label.nativeAmerican'/></label>
						<div class="controls">
							<label class="radio inline"> <input type="radio"
								name="employeeDetails[0].nativeAmr"	id="nativeAmrYes" value="YES"> Yes
							</label> <span id="nativeAmrYes_error"></span> 
							<label class="radio inline"> <input type="radio"
								name="employeeDetails[0].nativeAmr"	id="nativeAmr_No" value="NO"> No
							</label> <span id="nativeAmrNo_error"></span>
						</div>
						</div>
						
							<h4 class="lightgray"><spring:message  code='label.contact'/></h4>
							<div class="control-group">
								<label class="control-label" for="email"><spring:message  code='label.emplEmail'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<input type="text" name="employeeDetails[0].email" id="email" />
									<span id="email_error"></span>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label"><spring:message  code='label.homePhone'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<input type="text" class="span2 inline input-mini" name="phone1" id="phone1" maxlength="3"/>
									
									<input type="text" class="span2 inline input-mini" name="phone2" id="phone2" maxlength="3"/>
																	
									<input type="text" class="span2 inline input-mini" name="phone3" id="phone3" maxlength="4" /> <span id="phone3_error"></span>
								</div>
							</div>
						
						
						<div class="control-group">
							<label class="control-label" for="address1_Home"><spring:message  code='label.homeAddress'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label> 			
							<input type="hidden" name="address2_Home" id="address2_Home" >
							<input type="hidden" name="lat" id="lat_Home" value="0.0"  />
							<input type="hidden" name="lon" id="lon_Home" value="0.0"  />	
							<input type="hidden" name="rdi" id="rdi_Home" value="" />
							<input type="hidden" name="employeeDetails[0].location.countycode" id="countycode_Home" value="" />
							
							<div class="controls">
								<input type="text" name="employeeDetails[0].location.address1" id="address1_Home" /> <span id="address1_Home_error"></span>
								<input type="hidden" id="address1_Home_hidden" name="address1_Home_hidden" >
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="city_Home"><spring:message  code='label.emplCity'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="employeeDetails[0].location.city"
									id="city_Home" /> <span id="city_Home_error"></span>
								<input type="hidden" id="city_Home_hidden" name="city_Home_hidden">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="state_Home"><spring:message  code='label.emplState'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select class="input-medium"
									name="employeeDetails[0].location.state" id="state_Home">
									<option value="">Select</option>
									<c:forEach var="state" items="${statelist}">
										<option id="${state.code}" <c:if test="${state.code == defaultStateCode}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
									</c:forEach>
								</select> <span id="state_Home_error"></span>
								<input type="hidden" id="state_Home_hidden" name="state_Home_hidden" >
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="zip_Home"><spring:message  code='label.zip'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="employeeDetails[0].location.zip"
									id="zip_Home"  class="input-small zipCode" maxlength="5" />
									<input type="hidden" value="0.0" id="zip_Home_hidden" name="zip_Home_hidden" >
									 <span id="zip_Home_error"></span>
							</div>
						</div>
						<div class="control-group" id="county_Home_div">
							<label for="county" class="control-label required"><spring:message  code='label.county'/><img  src="<c:url  value="/resources/images/requiredAsterix.png"  />"  width="10"  height="10"  alt="Required!"/></label>
							<div class="controls">
								<select class="input-large" name="employeeDetails[0].location.county" id="county_Home">
									<option value="">Select County...</option>
								</select>
								<div id="county_Home_error"></div>		
							</div>
						</div>
						
							<h4 class="lightgray"><spring:message  code='label.dependent'/></h4>
							<div class="control-group">
								<fieldset>
									<legend class="control-label"><spring:message  code='label.isMarried'/></legend>
									<div class="controls">
										<label class="radio inline"> <input type="radio"
											name="isMarried" id="isMarriedYes" value="YES"> Yes
										</label> <label class="radio inline"> <input type="radio"
											name="isMarried" id="isMarriedNo" value="NO" checked> No
										</label>
									</div>
								</fieldset>
							</div>
							<div class="control-group">
								<label class="control-label" for="childCount"><spring:message  code='label.childCount'/></label>
								<div class="controls">
									<input type="text" id="childCount" name="childCount"
										maxlength="2" /> <span id="childCount_error"></span>
								</div>
							</div>
						<div class="control-group">
							<div class="control">
								<div class="form-actions">
									<input type="button" name="backButton" id="backButton" onclick="javascript:window.location.href ='info' " class="btn" value="<spring:message  code='label.back'/>"> 
									<input type="button" name="addButton" id="addButton" onclick="javascript:validateForm('Add New');" class="btn btn-primary" value="<spring:message  code='label.next'/>">
								</div>
							</div>
						</div>
						
						<input type="hidden" name="jsonStateValue" id="jsonStateValue" value='${doctorsJSON}' />
						<input type="hidden" name="defaultStateValue" id="defaultStateValue" value='${defaultStateCode}' />
						<input type="hidden" name="employerId" id="employerId" value="${employerOBJ.id}"/>
					</form>

					<!--<div id="worksite" class="modal hide fade"
						 tabindex="-1" role="dialog"
						aria-labelledby="Addanewaworksite" aria-hidden="true">
						
						<form class="form-horizontal" id="frmworksites"
							name="frmworksites">
 							<df:csrfToken/>
 							<input type="hidden" name="locations[0].primaryLocation" id="primaryLocation" value="NO"> 
							<input type="hidden" name="address2_Ws" id="address2_Ws" > 
							<input type="hidden" id="id" name="id" value="${employer_id}" />
							<input type="hidden" name="lat_0" id="lat_Ws" value="0.0">
							<input type="hidden" name="lon_0" id="lon_Ws" value="0.0">
							<input type="hidden" name="rdi" id="rdi_Ws" value="" />
							<input type="hidden" name="locations[0].location.countycode" id="countycode_Ws" value="" />
							
							<div class="modal-header">
								<button type="button" class="close clearForm" data-dismiss="modal" id="crossClose" 
									aria-hidden="true">�</button>
								<h3 id="worksiteHeading"><spring:message  code='label.employeraddworksiteaddr'/></h3>
							</div>
							<div class="modal-body">
								<div class="control-group">
									<label class="control-label" for="address1_Ws"><spring:message  code='label.street'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" class="input-medium"
											name="locations[0].location.address1" id="address1_Ws" />
										<div id="address1_Ws_error"></div>
										<input type="hidden" id="address1_Ws_hidden" name="address1_Ws_hidden" >
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="city_Ws"><spring:message  code='label.emplCity'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" class="input-medium"
											name="locations[0].location.city" id="city_Ws" />
										<div id="city_Ws_error"></div>
										<input type="hidden" id="city_Ws_hidden" name="city_Ws_hidden">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="state_Ws"><spring:message  code='label.emplState'/></label>
									<div class="controls">
										<select class="input-medium"
											name="locations[0].location.state" id="state_Ws">
											<option value="">Select</option>
											<c:forEach var="state" items="${statelist}">
												<option id="${state.code}" <c:if test="${state.code == defaultStateCode}"> SELECTED </c:if>  value="${state.code}">${state.name}</option>
											</c:forEach>
										</select>
										<div id="state_Ws_error"></div>
										<input type="hidden" id="state_Ws_hidden" name="state_Ws_hidden" >
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="zip_Ws"><spring:message  code='label.zip'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" class="input-small zipCode"
											name="locations[0].location.zip" id="zip_Ws" maxlength="5" />
										<div id="zip_Ws_error"></div>
										<input type="hidden" id="zip_Ws_hidden" name="zip_Ws_hidden" >
									</div>
								</div>
								<div class="control-group" id="county_Ws_div" style="">
									<label for="county" class="control-label required"><spring:message  code='label.county'/><img  src="<c:url  value="/resources/images/requiredAsterix.png"  />"  width="10"  height="10"  alt="Required!"/></label>
									<div class="controls">
										<select size="1" class="input-large" name="locations[0].location.county" id="county_Ws">
											<option value="">Select County...</option>
										</select>
										<div id="county_Ws_error"></div>		
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button class="btn clearForm" data-dismiss="modal" aria-hidden="true" id="cancelBtnModal"><spring:message  code='label.cancel'/></button>
								<input type="button" name="submitbutton" id="submitbutton"
									onClick="javascript:validateLocation();"
									class="btn btn-primary" value="<spring:message  code='label.employeraddworksite'/>" />
							</div>
						</form>
					</div>-->
				</div>
				<!--gutter10-->
			</div>
			<!-- rightpanel -->
		</div>
		<!-- row-fluid -->
	</div>
</div>

<script type="text/javascript">
var empDetailsJSON = ${empDetailsJSON};
var empEmployerLocationID = ${employerLocationID};
var	empid = '${employerOBJ.id}';


populateLocations(empDetailsJSON, empEmployerLocationID);

function populateLocations(empDetailsJSON, empEmployerLocationID){
	application.populate_location_on_parent_page(empDetailsJSON, empEmployerLocationID,'');
}

function postPopulateIndex(indexValue, zipCodeValue, zipId){
	//console.log(indexValue, zipCodeValue);
	jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
	getCountyList( '_'+indexValue,zipCodeValue, '' );
}

	jQuery.validator.addMethod("currency", function(value, element, param) {
		var pattern = /^(\\$)?([0-9\,])+(\.\d{0,2})?$/;
		return pattern.test(value);
	});

	var validator = $("#frmaddemployee")
			.validate(
					{
						rules : {
							'employeeDetails[0].firstName' : {
								required : true, namecheck:true
							},
							'employeeDetails[0].lastName' : {
								required : true, namecheck:true
							},
							'employeeDetails[0].dob' : {
								required : true,
								date : true,
								dobcheck : true
							},
							
							'employeeDetails[0].email' : { required : true, email: true},
							phone3 : {
								phonecheck : true
							},
							
							ssn3 : {
								ssncheck : true, 
								ssn1check : true, 
								invalidssn1 :true,
								invalidssn2 :true,
								validateTaxId: true
							},
							'employeeDetails[0].location.address1' : {
								required : true
							},
							'employeeDetails[0].location.city' : {
								required : true
							},
							'employeeDetails[0].location.state' : {
								required : true
							},
							'employeeDetails[0].location.county' : {
								required : true
							},
							'employeeDetails[0].location.zip' : {
								required : true,
								zipcheck : true
							},
							childCount : {digits : true}
						},
						messages : {
							'employeeDetails[0].firstName' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateempfirstname' javaScriptEscape='true'/></span>" ,
								namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatefirstnamechar' javaScriptEscape='true'/></span>"
							},
							'employeeDetails[0].lastName' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateemplastname' javaScriptEscape='true'/></span>" ,
								namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatelastnamechar' javaScriptEscape='true'/></span>"
							},
							'employeeDetails[0].dob' :{
								required :  "<span> <em class='excl'>!</em><spring:message code='label.validateempdob' javaScriptEscape='true'/></span>", 
								date:  "<span> <em class='excl'>!</em><spring:message code='label.validateempdob' javaScriptEscape='true'/></span>",
								dobcheck:  "<span> <em class='excl'>!</em><spring:message code='label.validateempvdob' javaScriptEscape='true'/></span>"
							},
						
							'employeeDetails[0].email' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateemailadr' javaScriptEscape='true'/></span>", 
	    						email: "<span> <em class='excl'>!</em><spring:message code='label.validateemail' javaScriptEscape='true'/></span>"},
							phone3 : {
								phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvtelephone' javaScriptEscape='true'/></span>"
							},
							ssn3: {
								ssncheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempdigit' javaScriptEscape='true'/></span>",
								ssn1check : "<span> <em class='excl'>!</em><spring:message code='label.validatefirstthreedigit' javaScriptEscape='true'/></span>",
								invalidssn1 : "<span> <em class='excl'>!</em><spring:message code='label.invalidssn1' javaScriptEscape='true'/></span>",
								invalidssn2 : "<span> <em class='excl'>!</em><spring:message code='label.invalidssn2' javaScriptEscape='true'/></span>",
								validateTaxId: "<span> <em class='excl'>!</em><spring:message code='label.validateemptaxid' javaScriptEscape='true'/></span>"},
							'employeeDetails[0].location.address1' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddr' javaScriptEscape='true'/></span>"
							},
							'employeeDetails[0].location.city' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validatecity' javaScriptEscape='true'/></span>"
							},
							'employeeDetails[0].location.state' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validatestate' javaScriptEscape='true'/></span>"
							},
							'employeeDetails[0].location.county' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validatecounty' javaScriptEscape='true'/></span>"
							},
							'employeeDetails[0].location.zip' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateempzip' javaScriptEscape='true'/></span>",
								zipcheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvzip' javaScriptEscape='true'/></span>"
							},
							childCount : {
								digits : "<span> <em class='excl'>!</em><spring:message code='label.validatechild' javaScriptEscape='true'/></span>" }
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error").attr('class',	'error span10');
							$('#dob').unbind('focus');
						}
					});

	var validator2 = $('#frmworksites')
			.validate(
					{
						onkeyup : false,
						onclick : false,
						onSubmit : true,
						onfocusout : false,
						rules : {
							'locations[0].location.address1' : {
								required : true
							},
							'locations[0].location.zip' : {
								required : true,
								zipcheck : true
							},
							'locations[0].location.city' : {
								required : true
							},
							'locations[0].location.state' : {
								required : true
							},
							'locations[0].location.county' : {
								required : true
							},
							
						},
						messages : {
							'locations[0].location.address1' : {
								required : "<span> <em class='excl'>!</em> <spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"
							},
							'locations[0].location.zip' : {
								required : "<span> <em class='excl'>!</em> <spring:message code='label.validateZip' javaScriptEscape='true'/></span>",
								zipcheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvzip' javaScriptEscape='true'/></span>"
							},
							'locations[0].location.city' : {
								required : "<span> <em class='excl'>!</em> <spring:message code='label.validateCity' javaScriptEscape='true'/></span>"
							},
							'locations[0].location.state' : {
								required : "<span> <em class='excl'>!</em> <spring:message code='label.validateState' javaScriptEscape='true'/></span>"
							},
							'locations[0].location.county' : {
								required : "<span> <em class='excl'>!</em> <spring:message code='label.validatecounty' javaScriptEscape='true'/></span>"
							}
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error").attr('class',	'error span10');
						}
					});
			

$(document).ready(function(){
	$('.date-picker').datepicker({
		autoclose:true,
		forceParse: false
		}).on('changeDate', function(ev) {
		});
	$('.info').tooltip();

});
var defaultData = $("#frmaddemployee").serialize();

/* $('#cancelBtnModal, #worksite .close, #crossClose').click(function(){
	$('#county_Ws').html('<option value="">Select County...</option>');
	$('.modal-backdrop').remove();
}); */
</script>
