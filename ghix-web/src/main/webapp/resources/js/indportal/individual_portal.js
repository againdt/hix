/**
 * @author Sunil Desu, Felix Cheng
 */


var indPortalApp = angular.module('indPortalApp', ['communicationPreferencesCtrlApp', 'communicationPreferencesDirectiveApp', 'addressValidationModule','ngSanitize', 'ngRoute', 'ui.mask', 'ngResource', 'angularFileUpload', 'ui.bootstrap', 'ng-bootstrap-datepicker','ui.mask','ngCookies','spring-security-csrf-token-interceptor']);
var hhdetails ="";
var mmdetails ="";
var addDetails ="";
var oComments="";
var healthplan="";
var dentalplan="";
var tempCoverageYear = null;
var isChangePlanFlowFlag = false;
var isSEPPlanFlow = false;
var isSepFlag = false;
var coverageDate = null;
var planSummary="";

indPortalApp.run(function($rootScope, $window, $route){
	$rootScope.goDash= function (){
		$window.location.href = "/hix/indportal";
		};

	$rootScope.openLink = function(address){
		$window.open(address);
	};

	$rootScope.$on('$routeChangeSuccess', function() {
		var templateName = $route.current.loadedTemplateUrl;
		if (templateName === 'portalHome') {
			document.title = 'My Dashboard / ' + $('#exchangeName').val();
		} else if (templateName && $rootScope[templateName]) {
			document.title = $rootScope[templateName].title + ' / ' + $('#exchangeName').val();
		}
    });
});

indPortalApp.value('isEditMode', true);

// indPortalApp.config(function ($httpProvider) {
// 	$httpProvider.defaults.transformResponse = [function(data) {
// 		console.info(`In transformResponse(): data = `, data);
// 		if (typeof data === 'string') {
// 			return JSON.stringify(eval(data));
// 		}
// 		return data;
// 	}]
// })

indPortalApp.config(['$routeProvider', '$locationProvider',
                    function($routeProvider, $locationProvider) {
                      $routeProvider.
                      when('/', {
                          templateUrl: 'portalHome',
                          controller: 'indPortalAppCtrl'
						}).
						when('/multiHousehold', {
							templateUrl: 'multipleHousehold',
							controller: 'indPortalAppCtrl'
						}).
                        when('/applications', {
                            templateUrl: 'myApplications',
                            controller: 'indPortalAppCtrl'
                          }).
                        when('/contactus', {
                              templateUrl: 'contactUs',
                              controller: 'indPortalAppCtrl'
                            }).
                         when('/eligibilityresults', {
                                templateUrl: 'eligibilityResults',
                                controller: 'indPortalAppCtrl'
                            }).
                         when('/additionalinfo', {
                                templateUrl: 'additionalinfo',
                                controller: 'indPortalAppCtrl'
                            }).
                         when('/plansummary', {
                                templateUrl: 'plansummary',
                                controller: 'indPortalAppCtrl'
                            }).
                        when('/submitappeal', {
                            templateUrl: 'appeals',
                            controller: 'indPortalAppCtrl'
                        }).
                        when('/overridehistory', {
                            templateUrl: 'overridehistory',
                            controller: 'indPortalAppCtrl'
                        }).
                        when('/appeals', {
                            templateUrl: 'appeals',
                            controller: 'indPortalAppCtrl'
                        }).
                        when('/myappeals', {
                            templateUrl: 'myappeals',
                            controller: 'indPortalAppCtrl'
                        }).
                        when('/referrals', {
                            templateUrl: 'referrals',
                            controller: 'indPortalAppCtrl'
                        }).
                        when('/disenrollsep', {
                            templateUrl: 'disenrollsep',
                            controller: 'indPortalAppCtrl'
                        }).
                        when('/qephome', {
                        	templateUrl: 'qephome',
                        	controller: 'indPortalAppCtrl'
                        }).
						when('/qepeligible', {
							templateUrl: 'qepeligible',
							controller: 'indPortalAppCtrl'
						}).
						when('/qepnoteligible', {
							templateUrl: 'qepnoteligible',
							controller: 'indPortalAppCtrl'
						}).
						when('/sepdenial', {
							templateUrl: 'sepdenial',
							controller: 'indPortalAppCtrl'
						}).
						when('/enrollmenthistory', {
							templateUrl: 'enrollmenthistory',
							controller: 'indPortalAppCtrl'
						}).
						when('/mypreferences', {
							templateUrl: 'mypreferences'
						}).
						when('/customGrouping', {
							templateUrl: 'customGrouping',
							controller: 'indPortalAppCtrl'
						}).
                        otherwise({
                          redirectTo: '/'
                        });
                    }]);


indPortalApp.controller('indPortalAppCtrl', ['$scope', '$http', '$location', '$window', '$sce', '$timeout', 'validateDate','$cookies','$cookieStore', '$q', '$route',function($scope, $http, $location, $window, $sce, $timeout, validateDate,$cookies, $cookieStore, $q, $route) {
	var SERVER_DATE =  localStorage.getItem('serverDate');
    var STATE_CODE = $('#sCode').val();

	$scope.isStateCA = $('#sCode').val() === 'CA';
	$scope.stateCode = STATE_CODE;
    $scope.householdId = null;
    $scope.userHouseholds = null;
	$scope.multiHouseholdError = false;
	$scope.hasActive = false;
	$scope.hasInactive = false;
	$scope.hasSelectedTobaccoUse = true;
	$scope.hasAIMembers = false;

	$scope.caseNumber = $route.current && $route.current.params && $route.current.params.caseNumber ? $route.current.params.caseNumber : '';

    $scope.initMultiHousehold = function() {
        var response = $http.get('/hix/indportal/multiHouseholdDetails');

        response.success(function (data, status, headers, config) {
            if(data !== undefined && data !== null && status === 200) {
                console.log("Data: " + JSON.stringify(data));

                if(data.households && data.households.length > 0) {
                    console.log("Success ");
					$scope.userHouseholds = data;
					$scope.hasActive = $scope.userHouseholds.households.some(function (item) {
						if (item.active) {
							return true;
						}
					});
					$scope.hasInactive = $scope.userHouseholds.households.some(function (item) {
						if (!item.active) {
							return true;
						}
					});
                } else {
                    console.log("households data is empty");
                }
            } else {
                console.log("status not 200 or data null: " + status);
            }

            $('#multiHouseholdModal').modal({backdrop: 'static', keyboard: false});
        });

        response.error(function (data, status, headers, config) {
            console.log("Fail to get household details");
        });
    }

    $scope.setActiveHousehold = function(multiHouseholdForm) {
        //console.log("Household " + householdId);
        console.log("Household form" + JSON.stringify(multiHouseholdForm));
        var householdId = multiHouseholdForm.householdId;
        console.log('householdid : ' + householdId);

        if(householdId === undefined || householdId === null) {
            console.log("Should be a valid value!")
            $scope.multiHouseholdError = true;
        } else {
            console.log("householdId = " + householdId);
            var url = 'indportal/activeHousehold';
            var data = householdId;
            var config = {
                headers: {'Content-Type': 'application/json; charset=UTF-8'}
            };

            var response = $http.post(url, data, config);

            response.success(function (data, status, headers, config) {
                console.log("active was success");
                var pageContext = $("#multiHouseholdContext").val();
                if(pageContext) {
					if(pageContext==="enrollmenthistory")
					{
						pageContext="myEnrollments";
					}
                    $('#multiHouseholdForm').modal('hide');
                    $('.modal-backdrop').remove();
                    location.href = './indportal?pageContext=' + pageContext;
                } else {
                location.href = './indportal';
                }
            });

            response.error(function (data, status, headers, config) {
                console.log("activate was error::data = " + JSON.stringify(data));
                $scope.multiHouseholdError = true;
            });
        }
	};

	$scope.showAgentPromo = function(){
		var dismissAgentPromo = $cookieStore.get('dismissAgentPromo');
		if(!dismissAgentPromo){
			$('#agentPromotion').modal('show');
		}
	};

	$scope.nextSteps = false;
	$scope.enableOE = $('#enableEnrollOnOE').val();
	$scope.showReveiwPrefModal = function(noOfDays){
		if(noOfDays <= 0) $scope.reveiwPrefModal = true;
		else $scope.reveiwPrefModal = false;
	};

	$scope.coveragePeriod ={
	    start: "starting",
	    end: "ending"
	};

	//dummy data for sepDenial.jsp
	$scope.eventDetails = [
//	    {applicant:'primary', eventType:'income', eventName:"1", eventDate:"03/03/2015"},
	    {applicant:'primary', eventType:'address', eventName:"2", eventDate:"11/03/2015"},
	    {applicant:'primary', eventType:'other', eventName:"3: a very event with a very long name", eventDate:"03/03/2015"},
//	    {applicant:'2nd', eventType:'income', eventName:"4", eventDate:"03/03/2015"},
//	    {applicant:'3rd', eventType:'remove', eventName:"5", eventDate:"03/03/2015"},
//	    {applicant:'4th', eventType:'income', eventName:"6", eventDate:"03/03/2015"}
	];


	$scope.getligibilityResults = function(){
        $scope.showEligibilityResults(window.caseNumber);
	};

	 $scope.showEligibilityResults = function(caseNumber){
 	    window.caseNumber = caseNumber;
		window.location = './indportal#/eligibilityresults?caseNumber=' + caseNumber;
 	 };

	// QEP START
	// data is put at the top page of qephome.jsp
	  $scope.changeinhouseholdsize = [
	    {eventName: qepStorage.marriage.title, content: qepStorage.marriage.content, image: 'resources/img/lce/maritalstatus.png'},
	    {eventName: qepStorage.divorce.title, content: qepStorage.divorce.content, image: 'resources/img/lce/gainmec.png'},
	    {eventName: qepStorage.birth.title, content: qepStorage.birth.content, image: 'resources/img/lce/dependents.png'},
	    {eventName: qepStorage.death.title, content: qepStorage.death.content, image: 'resources/img/lce/losemec.png'},
	  ];
	  $scope.changeincircumstances = [
	    {eventName: qepStorage.move.title, content: qepStorage.move.content, image: 'resources/img/lce/addresschange.png'},
	    {eventName: qepStorage.income.title, content: qepStorage.income.content, image: 'resources/img/lce/other.png'}
	  ];
	  $scope.changeinstatus = [
	    {eventName: qepStorage.immigration.title, content: qepStorage.immigration.content, image: 'resources/img/lce/citizenship.png'},
	    {eventName: qepStorage.incarceration.title, content: qepStorage.incarceration.content, image: 'resources/img/lce/incarceration.png'},
	    {eventName: qepStorage.tribe.title, content: qepStorage.tribe.content, image: 'resources/img/lce/tribe.png'},
	  ];
	  $scope.inatribe = [
	    {eventName: qepStorage.marriage.title, content: qepStorage.marriage.content, image: ''},
	  ];
	  // QEP END

//	$scope.testLoader= function(){
//		$scope.loaderF = true;
//		$timeout(function(){$scope.loaderF = false;}, 0);
//	};

	//Duplicate functions
	$scope.reload = function(){
		$window.location.reload(true);
	};

	$scope.refresh = function(){
		$window.location.reload(true);
	};
	//

    $scope.showCancelCoverageButton = function(group){
        if(!group.validCoverageSDate) {
            if (group.enrollmentStatus) {
                if(group.enrollmentStatus.toLowerCase() === 'pending' || group.enrollmentStatus.toLowerCase() === 'confirm'){
                    return true;
                }
            }
        }

        return false;
    };

	$scope.checkAllMembersHaveDentalEnrollmentId = function (memberList) {
        return memberList.every(function (member) {
            return 'dentalEnrollmentId' in member;
        })
    };

	$scope.showAddAPTCMessage = function(group){
		if(group.isRenewal){
				return false;
		}
		return (group.electedAptc !== group.maxAptc &&
	            (group.appState.isFinalizePlan || group.appState.isFinalizeEnrollment));
    };

    $scope.showAddStateSubsidyMessage = function(group){
    	if(group.isRenewal){
				return false;
		}

        return (group.electedStateSubsidy !== group.maxStateSubsidy && group.appState.isFinalizePlan);
    };

	$scope.formatAmount = function(amount){
	    if(amount){
            return parseFloat(amount) ? ("$"+ parseFloat(amount).toFixed(2)) : "$0";
        }

        return "$0";
    };

	$scope.isDentalPlansButtonDisabled = function(groupItem){
		if(!groupItem.memberList){
			return true;
		}
		if($scope.stateCode !== "CA"){
			return false;
		}

		for (var i = 0; i < groupItem.memberList.length; i++) {
			var memberInfo = groupItem.memberList[i];

			if(memberInfo.healthEnrollmentId && !memberInfo.dentalEnrollmentId) {
				return false;
			}
		}

		return true;
	};

    $scope.isAllMembersAddedToOtherGroups = function(){

        var selectedGroupsCount = 0;

        for(var i = 0; i < $scope.UnEnrolledhealthGroups.length; i++){
            if($scope.areAllMembersAddedToGroups($scope.UnEnrolledhealthGroups[i])){
                selectedGroupsCount++;
            }
        }

        return selectedGroupsCount !== $scope.UnEnrolledhealthGroups.length;
    };

	$scope.areAllMembersAddedToGroups = function(groupItem){

        if(!groupItem.memberList){
            return false;
        }

        if(groupItem.memberList.length === 0){
            return false;
        }

        var selectedGroups = groupItem.memberList.myFilter(function(x) {
            return x.canBeAddedInEnrolledGroup && x.canBeAddedInEnrolledGroup.length > 0
        });

        if(selectedGroups && selectedGroups.length === groupItem.memberList.length){
            return true;
        }

        return false;
	};

	$scope.calcDaysRemaining = function() {
		$scope.daysRemaining = -1;
		var sepEndDate = localStorage.getItem('sepEndDate');
		if (sepEndDate) {
			var startDate = moment(sepEndDate);
			$scope.daysRemaining = startDate.diff(SERVER_DATE, 'days');
		}
	}

	 $scope.showAppNotToConsider = function(type){
	        $scope.loader = false;
	        $scope.confirmNotToConsider = true;
	        $scope.renewalTabType = type;

	 }

	$scope.appNotToConsider = function(type){
		$scope.loader = true;
		var url = 'indportal/appNotToConsider';
		var params = {
				"applicationId" : $scope.applicationId,
				"type":type
		};
		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'},
			params:params
		};

		var response = $http.post(url, null, config);

		response.success(function(data, status, headers, config) {
			$scope.loader = false;
			if(data === "success"){
				$scope.refresh();
			}
			else{
				$scope.loader = false;
				$scope.customGroupingFailure = true;
				$scope.errorAppNotToConsider = true;
			}
		});
	};

	/**
	 * Returns true if AT LEAST one group has isNativeAmerican member
	 * @param groupList
	 */
	$scope.isNativeAmerican = function(groupList) {
		for (var i = 0; i < groupList.length; i++){
			for (var j = 0; j < groupList[i].memberList.length; j++) {
				if (groupList[i].memberList[j] && groupList[i].memberList[j].isNativeAmerican === "Y") {
					return true;
				}
			}
		}
		return false;
	};

	$scope.getNameBasedOnAppState = function(stateCode, isInsideOEEnrollmentWindow, applicationId, anyAIANMember, applicationType, group) {
		group.appState = {
			isFinalizeEnrollment: false,
			isFinalizePlan: false,
			isChangePlan: false,
			isRenewOrChangePlan: false,
			isBlank: false
		};

		if (isInsideOEEnrollmentWindow) {
			switch (applicationType) {
				case 'SEP':
					if (applicationId !== group.enrollmentPriorApplicationId) {
						if (stateCode === 'MN') {
							group.appState.isFinalizeEnrollment = true;
						} else {
							group.appState.isFinalizePlan = true;
						}
					} else {
						group.appState.isChangePlan = true;
					}
					break;
				case 'OE':
					if (group.isRenewal) {
						group.appState.isRenewOrChangePlan = true;
					} else {
						group.appState.isChangePlan = true;
					}
					break;
				case 'QEP':
					group.appState.isChangePlan = true;
					break;
				default:
					break;
			}
		} else {
			var isEnrollmentCoverageDateWithinThisMonth = null;
			if(group.enrollmentCreationDate){
				isEnrollmentCoverageDateWithinThisMonth = moment(group.enrollmentCreationDate).isSame(new Date(SERVER_DATE), 'month');
			}
			switch (applicationType) {
				case 'QEP':
				case 'OE':
					if (!anyAIANMember) {
						group.appState.isBlank = true;
					} else {
						if (isEnrollmentCoverageDateWithinThisMonth !== null && !isEnrollmentCoverageDateWithinThisMonth) {
							group.appState.isChangePlan = true;
						} else {
							group.appState.isBlank = true;
						}
					}
					break;
				case 'SEP':
					if(applicationId !== group.enrollmentPriorApplicationId){
						if (group.groupType === 'DENTAL' && !$scope.checkAllMembersHaveDentalEnrollmentId(group.memberList)) {
							group.appState.isBlank = true;
						} else {
							if (stateCode === 'MN') {
								group.appState.isFinalizeEnrollment = true;
							} else {
								group.appState.isFinalizePlan = true;
							}
						}
					} else { //HIX-110731
						if (group.allowChangePlan === 'Y' || (group.allowChangePlan === 'N' && anyAIANMember && isEnrollmentCoverageDateWithinThisMonth !== null && !isEnrollmentCoverageDateWithinThisMonth)) {
							group.appState.isChangePlan = true;
						} else {
							group.appState.isBlank = true;
						}
					}
					break;
				default:
					break;
			}
		}
	};

	$scope.showCustomGrouping = function(){

		// HIX-110497: When Multiple Applications are in 'ER' state that time custom grouping screen is throwing blank pop-up
		// Making this change under the assumption that user will land on the custom grouping page after coming through member portal dashboard.
		$scope.coverageYear = $('#coverageYear').val();
		$scope.previousCoverageYear = (parseInt($scope.coverageYear) - 1) +"";

		var enrollCaseNumber = localStorage.getItem('enrollCaseNumber');
        if (!enrollCaseNumber || enrollCaseNumber === "undefined") {
            enrollCaseNumber = $('#enrollCaseNumber').val();
		}
		$scope.calcDaysRemaining();

		var url='/hix/indportal/customGrouping?caseNumber='+enrollCaseNumber;
		response = $http.get(url);
		response.success(function(data){
		    $scope.loader = false;
			if(typeof(data)!=="string" && data !== "" && data.status == null){
					$scope.disenrollDialogDataList = data.disenrollDialogDataList;
					$scope.applicationType = data.applicationType;
					$scope.applicationId = data.applicationId;
					$scope.isInsideOEEnrollmentWindow = data.isInsideOEEnrollmentWindow;
					$scope.isRenewalApplication = data.isRenewalApplication;
					if(data.coverageYear && parseInt($scope.coverageYear) !== data.coverageYear){
						$scope.coverageYear = data.coverageYear+"";
						$scope.previousCoverageYear = ( data.coverageYear - 1) +"";
					}
					$scope.isHealthRenewalGroupAvailable = false;
					$scope.isDentalRenewalGroupAvailable = false;
					$scope.allGroupsRenewal = false;
                    $scope.groupList = data.groupList;
					$scope.anyAIANMember = $scope.isNativeAmerican(data.groupList);
                    var healtGroupArray = [];
                    var dentalGroupArray = [];
                    var unEnrolledHealthGroup = [];
                    angular.forEach(data.groupList, function (groupItem, index) {
                        if (groupItem.coverageDate != null) {
                            coverageDate = groupItem.coverageDate;
							var coverageStartDate = new Date(coverageDate);
                            groupItem.validCoverageSDate = groupItem.allowDisenroll && validateCoverageStartDate(coverageStartDate);
                        }
                        if (groupItem.groupType === 'DENTAL') {
                            if (groupItem.groupEnrollmentId !== null && groupItem.groupEnrollmentId != "" && groupItem.groupEnrollmentId != undefined) {
                                $scope.enableMe = false;
                                $scope.showDentalPlansButtonDisabled = $scope.isDentalPlansButtonDisabled(groupItem);
                            } else {
                                $scope.enableMe = true;
                                $scope.showDentalPlansButtonDisabled = $scope.isDentalPlansButtonDisabled(groupItem);
                            }
							groupItem.isAllSelected = true;

							$scope.getNameBasedOnAppState($scope.stateCode, $scope.isInsideOEEnrollmentWindow, $scope.applicationId, $scope.anyAIANMember, $scope.applicationType, groupItem);
							groupItem.showAptcDentalMessage = true;
							var kidsCount = 0;
							if($scope.stateCode === 'ID' && groupItem.maxAptc !== undefined && groupItem.maxAptc > 0){
								angular.forEach(groupItem.memberList, function(dentalMember, key){
									if(dentalMember.ageAtCoverageDate < 19) {
										kidsCount++;
									}
							    });    
								if(kidsCount === 0){
									groupItem.showAptcDentalMessage = false;
								 }
							}
							dentalGroupArray.push(groupItem);
                            if(groupItem.isRenewal){
                            	$scope.isDentalRenewalGroupAvailable = true;
                            }
                        } else {
                        	if(groupItem.isRenewal){
                        		$scope.isHealthRenewalGroupAvailable = true;
                        	 }

							$scope.getNameBasedOnAppState($scope.stateCode, $scope.isInsideOEEnrollmentWindow, $scope.applicationId, $scope.anyAIANMember, $scope.applicationType, groupItem);
                            healtGroupArray.push(groupItem);
                            if (groupItem.groupEnrollmentId === undefined) {
                                unEnrolledHealthGroup.push(groupItem);
                            }
                        }
                    });

                    if($scope.isRenewalApplication && $scope.isHealthRenewalGroupAvailable){
                    	var onlyhealth = healtGroupArray.myFilter(function(x) { return x.groupEnrollmentId !== undefined});
                    	var renewalgroups = healtGroupArray.myFilter(function(x) { return x.isRenewal === true } );
                    	if(renewalgroups.length > 0 && renewalgroups.length === onlyhealth.length){
                    		$scope.allGroupsRenewal = true;
                    	}
                    }

                    $scope.addMembersGroups = [];

                    angular.forEach(unEnrolledHealthGroup, function (healthGroup, index) {
                        angular.forEach(healthGroup.memberList, function (ind_member, memIndex) {
                            if (ind_member.canBeAddedInEnrolledGroup != null && ind_member.canBeAddedInEnrolledGroup.length > 0) {

                                angular.forEach(ind_member.canBeAddedInEnrolledGroup, function (enrollmentId, memIndex) {
                                    angular.forEach(healtGroupArray, function (groupItem, gIndex) {
                                        var clonedGroupItem = JSON.parse(JSON.stringify(groupItem));

                                        if (clonedGroupItem.groupEnrollmentId == enrollmentId) {
                                            clonedGroupItem.memberList.push(Object.assign({}, ind_member));
                                            clonedGroupItem.selectedMembersCount = $scope.updateSelectedMemberCount(clonedGroupItem, 'SINGLE');
                                            healthGroup.selectedMembersCount = $scope.updateSelectedMemberCount(healthGroup, 'SINGLE');
                                            // check if the group already exists

                                            var selectedGroups = $scope.addMembersGroups.myFilter(function(x) { return x.AddedInEnrolledGroup.id === clonedGroupItem.id } );
                                            if (!selectedGroups || (selectedGroups && selectedGroups.length === 0)) {
                                                $scope.addMembersGroups.push({
                                                    'AddedInEnrolledGroup': clonedGroupItem,
                                                    'enrollmentGroup': healthGroup
                                                });
                                            } else {
                                                if(selectedGroups.length > 0) {
                                                    selectedGroups[0].AddedInEnrolledGroup.memberList.push(Object.assign({}, ind_member));
                                                }
                                            }
                                        }
                                    });
                                });
                            }
                        });
                    });

                    if ($scope.addMembersGroups && $scope.addMembersGroups.length > 0) {
                    	if($scope.applicationType !== 'SEP') {
                            $scope.addMembersGroups[0].AddedInEnrolledGroup.selectedMembersCount = $scope.addMembersGroups[0].AddedInEnrolledGroup.memberList.myFilter(function(x) { return (x.healthEnrollmentId === undefined) } ).length;
                        } else {
                            $scope.addMembersGroups[0].AddedInEnrolledGroup.selectedMembersCount = 0;
						}
                        // HIX-109818: Don't select any member, hence 0.
                        $scope.addMembersGroups[0].enrollmentGroup.selectedMembersCount = 0;
                    }

                    $scope.dentalGroups = dentalGroupArray;
                    $scope.dentalToggleAll();
					$scope.healthGroups =  healtGroupArray.sort(function(a, b) {
						parseInt(a.groupEnrollmentId) - parseInt(b.groupEnrollmentId)
					});
					$scope.isMixOfAIMembers();

                    $scope.UnEnrolledhealthGroups = [];

                    for (var i = 0; i < healtGroupArray.length; i++) {
                        if (healtGroupArray[i].id) {
                            var groups = $scope.addMembersGroups.myFilter(function(x) { return  (x.AddedInEnrolledGroup.id === healtGroupArray[i].id ) } );
                            if (groups.length === 0) {
                                if ($scope.isAnyMembersUnEnrolled(healtGroupArray[i])) {
                                    $scope.UnEnrolledhealthGroups.push(healtGroupArray[i]);
                                }
                            }
                        }
                    }

               if($scope.stateCode === 'ID'){
            	   $scope.UnEnrolledhealthGroups.sort(function(x,y) {
            		   return x.isNonStandardRelationGroup - y.isNonStandardRelationGroup
            	   });
               }
               else{
            	   $scope.UnEnrolledhealthGroups.sort(function(a,b) {
            		   return b.hasAPTCEligibleMembers - a.hasAPTCEligibleMembers
            	   });
               }


                // Default
                if ($scope.UnEnrolledhealthGroups && $scope.UnEnrolledhealthGroups.length > 0 && $scope.applicationType !== 'SEP') {
                    $scope.UnEnrolledhealthGroups.selectedMembersCount = $scope.UnEnrolledhealthGroups[0].memberList.length;
                    var uncheckGroupId = null;
                    // only check the 1st group
                    if ($scope.UnEnrolledhealthGroups.length > 0) {
                    	uncheckGroupId = $scope.UnEnrolledhealthGroups[0].id;

                        for (let i=0; i <= $scope.UnEnrolledhealthGroups[0].memberList.length-1; i++) {
                            $scope.UnEnrolledhealthGroups[0].memberList[i].isChecked = true;
                        }

                        if ($scope.addMembersGroups && $scope.addMembersGroups.length > 0 && $scope.applicationType === 'OE') {
                            for(let i=0; i <=$scope.addMembersGroups.length-1;i++){
                                if(uncheckGroupId !== null){
                                    if($scope.addMembersGroups[i].enrollmentGroup.id === uncheckGroupId){
                                         for (let j=0; j <= $scope.addMembersGroups[i].enrollmentGroup.memberList.length-1; j++) {
                                            $scope.addMembersGroups[i].enrollmentGroup.memberList[j].isChecked = false;

                                         }
                                    }
                                }
                            }

                         }
                    }
                    $scope.UnEnrolledhealthGroups[0].selectedMembersCount = 0;
                }

                    $scope.isAnyGroupEnrolled = $scope.isAnyEnrolledGroups();
                	$scope.isAnyDentalEnrollmentsAvailable = $scope.isAnyDentalEnrollments(); // $scope.dentalGroups;
                	$scope.isAllMembersEnrolledInDental = $scope.isAllMembersEnrolledInDentalGroup();
                    $scope.isAllMembersEnrolledInHealthAndFinalized = $scope.isAllMembersEnrolledInHealthGroupAndFinalized();

                    $scope.showHealthPlansButtonDisabled = true;

                    // Here we are checking all the members in first group.
					var nonEnrolledGroups = $scope.getNonEnrolledGroups();
                    if (nonEnrolledGroups && nonEnrolledGroups.length > 0) {
                        $scope.showHealthPlansButtonDisabled = false;
                        $scope.selectedMembersCount = nonEnrolledGroups[0].memberList ? nonEnrolledGroups[0].memberList.length : 0;
                    }

                    if($scope.addMembersGroups && $scope.addMembersGroups.length > 0){
                    	$scope.slideDiv('health');
                    }

                    if ($scope.isAllMembersEnrolledInHealthAndFinalized && !$scope.isHealthRenewalGroupAvailable && (!$scope.isAllMembersEnrolledInDental || $scope.isDentalRenewalGroupAvailable) ) {
                        $scope.toggleTabs('Dental');
                    }
					console.log(JSON.stringify($scope.addMembersGroups));
				}else{
					$scope.loader = false;
					$scope.customGroupingFailure = true;
					$scope.customGroupingFailureMsg = data.message;
				}

        });
        };

    $scope.canShowGroup = function(enrollmentGroup,addMembersGroups, index){
		for(var i = 0; i < addMembersGroups.length; i++){
			if(addMembersGroups[i].enrollmentGroup.id === enrollmentGroup.id){
				if(i < index) {
					return false;
				}
			}
		}

		return true;
	};

	$scope.checkEnrollmentsSlideDiv = function(event, planType) {
		if (event.keyCode == 13 || event.keyCode == 32) {
			$scope.enrollmentsSlideDiv(planType);
			return true;
		}
		return false;
	}

	$scope.enrollmentsSlideDiv = function(planType) {
		if (planType === 'myEnrollments') {
			$('#my_enrollments_toggle_icon').toggleClass('icon-chevron-sign-up icon-chevron-sign-down');
			$('#myEnrollments').slideToggle();
		} else if (planType === 'myHealthPlan') {
			$('#my_health_plan_toggle_icon').toggleClass('icon-chevron-sign-up icon-chevron-sign-down');
			$('#myHealthPlan').slideToggle();
		} else if (planType === 'myDentalPlan') {
			$('#my_dental_plan_toggle_icon').toggleClass('icon-chevron-sign-up icon-chevron-sign-down');
			$('#myDentalPlan').slideToggle();
		} else if (planType === 'pastEnrollments') {
			$('#my_past_enrollments_toggle_icon').toggleClass('icon-chevron-sign-up icon-chevron-sign-down');
			$('#pastEnrollments').slideToggle();
		}
	}

	$scope.checkPlanDetailsSlideDivEvents = function(event, healthDentalType) {
		if (event.keyCode == 13 || event.keyCode == 32) {
			$scope.slidePlanDetailsDiv(healthDentalType);
			return true;
		}
		return false;
	}

	 $scope.slidePlanDetailsDiv = function(type){
		 if( type === 'medical'){
     		$('#plan_details_medical_toggle_icon').toggleClass('icon-chevron-down icon-chevron-up');
     		$('#plan_details_medical_plan').slideToggle();
     	}
     	else if(type === 'dental'){
     		$('#plan_details_dental_toggle_icon').toggleClass('icon-chevron-down icon-chevron-up');
     		$('#plan_details_dental_plan').slideToggle();
     	}

 	};

	$scope.checkSlideDivEvents = function(event, healthDentalType) {
		if (event.keyCode == 13 || event.keyCode == 32) {
			$scope.slideDiv(healthDentalType);
			return true;
		}
		return false;
	}

	 $scope.slideDiv = function(type){
     	if( type === 'health'){
     		$('#health_toggle_icon').toggleClass('icon-chevron-up icon-chevron-down');
     		$('#aid_health_enrolled_grps').slideToggle();
     	}
     	else if(type === 'dental'){
     		$('#dental_toggle_icon').toggleClass('icon-chevron-up icon-chevron-down');
     		$('#aid_dental_grps_nrg_0').slideToggle();
     	}

 	};

	$scope.getMaxAPTC = function(group) {
		if(group.maxAptc !== undefined && group.hasAPTCEligibleMembers == true){
			var aptc = 0;

	        var anyMemberWithoutAptc = false;

	        angular.forEach(group.memberList, function (member, memIndex) {
	            if (member.aptc) {
	                aptc += parseFloat(member.aptc.toFixed(2));
	            } else {
	                anyMemberWithoutAptc = true;
	            }
	        });

	        if(anyMemberWithoutAptc && group.maxAptc){
	            aptc += parseFloat(group.maxAptc.toFixed(2));
	        }

	        return aptc;
		}
		return null;
	};

	$scope.getMaxStateSubsidy = function(group){
	    if(group.maxStateSubsidy !== undefined && group.hasStateSubsidyEligibleMembers == true){
	        var stateSubsidy = 0;
	        var anyMemberWithoutStateSubsidy = false;
	        angular.forEach(group.memberList, function (member, memIndex){
	            if(member.stateSubsidy){
	                stateSubsidy +=  parseFloat(member.stateSubsidy.toFixed(2));
	            }else{
	                anyMemberWithoutStateSubsidy = true;
	            }
	        });
	        if(anyMemberWithoutStateSubsidy && group.maxStateSubsidy){
	            stateSubsidy += parseFloat(group.maxStateSubsidy.toFixed(2));
	        }
            return stateSubsidy;
	    }
	    return null;
	};

	$scope.updateSelectedMemberCount = function(selectedGroups, groupType){

		var selectedMembersCount = 0;

		if(groupType === 'SINGLE'){
            angular.forEach(selectedGroups.memberList, function (ind_member, memIndex) {
            	if(ind_member.healthEnrollmentId == null || ind_member.healthEnrollmentId === undefined) {
                    if ($('#' + ind_member.id + selectedGroups.id).is(":checked") && $('#' + ind_member.id + selectedGroups.id).is(":visible")) {
                        selectedMembersCount++;
                    }
                }
            });
		} else {
            angular.forEach(selectedGroups, function (groupItem, index) {
                angular.forEach(groupItem.memberList, function (ind_member, memIndex) {
                    if ($('#' + ind_member.id + groupItem.id).is(":checked") && $('#' + ind_member.id + groupItem.id).is(":visible")) {
                        selectedMembersCount++;
                    }
                });
            });
        }

  		return selectedMembersCount;
	};

	//TODO: Atul to give parameters for AI/AN at member level..
	$scope.isMixOfAIMembers = function(){
        var groups = $scope.getNonEnrolledGroups();

        var aiangroupFound = false;
        var non_aiangroupFound = false;

        angular.forEach(groups, function(grp, memIndex){
            if(( grp.groupCsr === "CS2" || grp.groupCsr === "CS3") ){
                aiangroupFound = true;
			} else {
                non_aiangroupFound = true;
			}
		});

		$scope.hasAIMembers = aiangroupFound && non_aiangroupFound;
	};

    $scope.isAnyEnrolledGroups = function() {
		var enrolledGroups = [];
		angular.forEach($scope.healthGroups, function(group, index){
			// skip enrolled groups(
			var isEnrolledGroup = false;
			if(group.groupEnrollmentId)
                isEnrolledGroup =  true ;

			if(isEnrolledGroup) {
                enrolledGroups.push(group);
			}
		});

		$scope.enrolledMembersCount = 0;

        for(var i = 0 ; i < enrolledGroups.length; i++){
        	if(enrolledGroups[i].memberList) {
                var enrolledMembers = enrolledGroups[i].memberList.myFilter( function(x) { return  x.healthEnrollmentId !== undefined } );
                $scope.enrolledMembersCount += enrolledMembers.length;
            }
		}

		return enrolledGroups.length > 0;
    };

    $scope.isAnyDentalEnrollments = function() {

        $scope.enrolledDentalMembersCount = 0;

        for(var i = 0 ; i < $scope.dentalGroups.length; i++){
            if($scope.dentalGroups[i].memberList) {
                var enrolledMembers = $scope.dentalGroups[i].memberList.myFilter(function(x) { return x.dentalEnrollmentId !== undefined } );
                $scope.enrolledDentalMembersCount += enrolledMembers.length;
            }
        }

        return $scope.enrolledDentalMembersCount > 0;
    };

    $scope.isAllMembersEnrolledInHealthGroupAndFinalized = function() {

        var totalMembersCount = 0;
        var enrolledMembersCount = 0;
        var isPlanFinalized = true;

        for(var i = 0 ; i < $scope.healthGroups.length; i++){
            if($scope.healthGroups[i].memberList) {
                var enrolledMembers = $scope.healthGroups[i].memberList.myFilter(function(x) { return  x.healthEnrollmentId !== undefined } );
                enrolledMembersCount += enrolledMembers.length;
            }

            if ($scope.applicationId !== $scope.healthGroups[i].enrollmentPriorApplicationId) {
                isPlanFinalized = false;
            }

            totalMembersCount += $scope.healthGroups[i].memberList.length;
        }

        return enrolledMembersCount === totalMembersCount && isPlanFinalized;
    };

    $scope.isAllMembersEnrolledInDentalGroup = function() {

        var totalMembersCount = 0;
        var enrolledMembersCount = 0;

        for(var i = 0 ; i < $scope.dentalGroups.length; i++){
            if($scope.dentalGroups[i].memberList) {
                var enrolledMembers = $scope.dentalGroups[i].memberList.myFilter(function(x) { return  x.dentalEnrollmentId !== undefined } );
                enrolledMembersCount += enrolledMembers.length;
            }

            totalMembersCount += $scope.dentalGroups[i].memberList.length;
        }

        return enrolledMembersCount === totalMembersCount;
    };

    $scope.getUnEnrolledMembers = function(group){
        var strEnrolledMembers = "";

    	 for (var i = 0 ; i < group.memberList.length; i++){
    	 	if(group.memberList[i].healthEnrollmentId === undefined){
                strEnrolledMembers += group.memberList[i].firstName + " " + group.memberList[i].lastName + ","
			}
		 }

		 if(strEnrolledMembers && strEnrolledMembers.endsWith(",")){
             return strEnrolledMembers.slice(0, strEnrolledMembers.length-1) + " ";
		 }

		 return strEnrolledMembers;
	};

    $scope.isAllEnrolled = function(group) {
        var result = false;
        if(group && group.groupEnrollmentId){
			return true;
        }

        return result;
    };

    $scope.isAnyMembersUnEnrolled = function(group) {
    	var anyMemberUnEnrolled = false;

        angular.forEach(group.memberList, function(member, memIndex){
			if(!member.healthEnrollmentId)
                anyMemberUnEnrolled = true;
        });

        return anyMemberUnEnrolled;
	};
	$scope.checkToggleGroupsKeyEvents = function(event, tabType) {
		if (event.keyCode == 13 || event.keyCode == 32) {
			$scope.toggleGroups(tabType);
			return true;
		}
		return false;
	}
	$scope.showHealthGroups = true;
	$scope.showDentalGroups = false;
	$scope.toggleGroups = function(tabType){
        // $scope.showCustomGrouping();
		if(tabType == "Dental") {
			// Show modal to encourage consumers to enroll in health plans prior to dental.
			$scope.healthPriorDentalModal = false;
			if ($scope.stateCode !== 'CA') {
				if (($scope.applicationType === 'QEP' || $scope.applicationType === 'OE') &&
					$scope.isHouseHoldAPTCEligible($scope.groupList) && !$scope.isAllGroupsMembersEnrolledInHealth($scope.groupList)) {
					$scope.healthPriorDentalModal = true;
				} else {
					$scope.showHealthGroups = false;
					$scope.showDentalGroups = true;
				}
			} else { // Old  code, applicable for CA only
				$scope.showDentalGroups = true;
				$scope.dentalHasHealthEnrollemnts = $scope.isAllEnrolledInHealth();
				var isDentalEnrolled = false;
				isDentalEnrolled = $scope.isAllMembersEnrolledInDentalGroup();
				if (!$scope.isAllEnrolledInHealth() && $scope.stateCode === 'CA' && !isDentalEnrolled) {
					$scope.dentalHasHealthEnrollemnts = true;
				} else {
					$scope.dentalHasHealthEnrollemnts = false;
				}

				$scope.showHealthGroups = false;
			}

		}else{
            $scope.dentalHasHealthEnrollemnts = false;
			$scope.showHealthGroups = true;
			$scope.showDentalGroups = false;
		}
	};

	/**
	 * Returns true if at least one group has maxAptc > 0.
	 * @param groupList
	 * @returns {boolean}
	 */
	$scope.isHouseHoldAPTCEligible = function(groupList) {
		var isHouseHoldAPTCEligible = false;
		angular.forEach(groupList, function(group) {
			if (group.maxAptc && group.maxAptc > 0) {
				isHouseHoldAPTCEligible = true;
			}
		});

		return isHouseHoldAPTCEligible;
	};

	/**
	 * Returns false if at least one member in the HH is not enrolled in health (Looping through ALL groups).
	 * @param groupList
	 * @returns {boolean}
	 */
	$scope.isAllGroupsMembersEnrolledInHealth = function(groupList) {
		var isAllGroupsMembersEnrolledInHealth = true;
		angular.forEach(groupList, function(group) {
			angular.forEach(group.memberList, function(member) {
				if (!member.healthEnrollmentId) {
					isAllGroupsMembersEnrolledInHealth = false;
				}
			});
		});

		return isAllGroupsMembersEnrolledInHealth;
	};

	$scope.toggleTabs = function(tabType){
        $scope.dentalHasHealthEnrollemnts = false;
		$scope.healthPriorDentalModal = false;

        if(tabType == "Dental") {
            $scope.showHealthGroups = false;
            $scope.showDentalGroups = true;
        } else {
            $scope.showHealthGroups = true;
            $scope.showDentalGroups = false;
		}
	};

	//Check if all the members are enrolled in health..
	$scope.isAllEnrolledInHealth = function() {
		var healthEnrollmentsInDental = $scope.dentalGroups[0].memberList.myFilter(function(x) { return  x.healthEnrollmentId } );
		return !(healthEnrollmentsInDental && healthEnrollmentsInDental.length !== $scope.dentalGroups[0].memberList.length);
	};

	
	$scope.isDentalDisabled = function(member){
		return (!member.healthEnrollmentId && $scope.stateCode === "CA");
	};

	$scope.dentalToggleAll = function(){
		var dentalGroupChecked = false;
		angular.forEach($scope.dentalGroups, function(dentalGroup, index){
			var toggleStatus = dentalGroup.isAllSelected;
			dentalGroupChecked = toggleStatus;
			angular.forEach(dentalGroup.memberList, function(member){
				member.isChecked = toggleStatus;
			});
		});
		// if(dentalGroupChecked == true && $scope.enableMe){
		// 	$scope.showDentalPlansButtonDisabled = false;
		// }else{
		// 	$scope.showDentalPlansButtonDisabled = true;
		// }
	};

	$scope.dentalOptionToggled = function(dentalGroup){
		var dentalCheckedCount = 0;
		angular.forEach(dentalGroup.memberList, function(member){
			if(member.isChecked){
				dentalCheckedCount++;
			}
		});
		if(dentalCheckedCount == dentalGroup.memberList.length){
			dentalGroup.isAllSelected = true;
		}else{
			dentalGroup.isAllSelected = false;
		}
		if(dentalCheckedCount > 0){
			$scope.showDentalPlansButtonDisabled = false;
		}else{
			$scope.showDentalPlansButtonDisabled = true;
		}
	};

	$scope.toggleAll = function(healthGroup){
		var groupChecked = false;
		if(healthGroup.isAllSelected == true){
			angular.forEach(healthGroup.memberList, function(member){
				if(member.healthEnrollmentId == null || member.healthEnrollmentId === undefined){
					$('#'+member.id+healthGroup.id).attr('checked', true);
					groupChecked = true;
				}
			});
		}
		else if(healthGroup.isAllSelected == false){
			angular.forEach(healthGroup.memberList, function(member){
				if(member.healthEnrollmentId == null || member.healthEnrollmentId === undefined){
					$('#'+member.id+healthGroup.id).attr('checked', false);
					groupChecked = false;
				}
			});
		}
		var OtherGroupMemberChecked = false;
		angular.forEach($scope.healthGroups, function(healthGroup1, index){
			if(healthGroup.id  !== healthGroup1.id){
				angular.forEach(healthGroup1.memberList, function(member){
					if(member.healthEnrollmentId == null || member.healthEnrollmentId === undefined){
						if($('#'+member.id+healthGroup1.id).is(":checked")){
							OtherGroupMemberChecked = true;
						}
					}

				});
			}
		});
		if(groupChecked == true || OtherGroupMemberChecked == true){
			$scope.showHealthPlansButtonDisabled = false;
		}else{
			$scope.showHealthPlansButtonDisabled = true;
		}
	};

	$scope.setPrefReviewed = function() {
		$scope.loader = true;

		var response = $http.get('./setPrefReviewed');
		response.success(function(data, status, headers, config) {
			$scope.loader = false;
			location.href='#mypreferences';

		});
		response.error(function() {
			$scope.loader = false;
			location.href='./indportal';

		});
	};

	$scope.optionToggled = function(member, group, parentGroups, groupType) {

        parentGroups.selectedMembersCount = $scope.updateSelectedMemberCount(parentGroups, groupType);

		var healthCheckedCount = 0;
		var memberIsChecked = false;
		angular.forEach(group.memberList, function(member){
			if(member.healthEnrollmentId == null || member.healthEnrollmentId === undefined){
				if($('#'+member.id+group.id).is(":checked")){
					healthCheckedCount++;
					memberIsChecked = true;
				}
			}
		});

        group.isAllSelected = healthCheckedCount == group.memberList.length ? true : false;

		var OtherGroupMemberChecked = false;
		angular.forEach($scope.healthGroups, function(healthGroup1, index){
			if(group.id !== healthGroup1.id){
				angular.forEach(healthGroup1.memberList, function(member){
					if(member.healthEnrollmentId == null || member.healthEnrollmentId === undefined){
						if($('#'+member.id+healthGroup1.id).is(":checked")){
							OtherGroupMemberChecked = true;
						}
					}

				});
			}
		});

        $scope.showHealthPlansButtonDisabled = memberIsChecked || OtherGroupMemberChecked ? false : true;

        $scope.lastMemberCheckedId = member.id;
        $scope.processGroupingWarning(group);
	};

	// check if there are 2 non-enrolled groups
	// go through each group to see if any member is checked..
	// create an array for that group check..
	// if more than one group has checked.. throw warnig message..


	$scope.isSubsidizedGroup = function(group){
	    var isGroupSubsidized = group.hasAPTCEligibleMembers;

        if (group.hasStateSubsidyEligibleMembers !== undefined){
            isGroupSubsidized = isGroupSubsidized || group.hasStateSubsidyEligibleMembers;
        }

        return isGroupSubsidized;
	};

	$scope.hasNativeAmericanMember = function(group) {
		var retVal = false;
		if (group.memberList) {
			retVal = group.memberList.some(function (item) {
				return item.isNativeAmerican === 'Y';
			});
		}
		return retVal;
	}

	$scope.processGroupingWarning = function(){
        var nonEnrolledGroups = $scope.getNonEnrolledGroups();

        if(nonEnrolledGroups.length === 1) {
        	return;
        }

		var isMemberSelectedFromAnotherGroup = $scope.checkIfMemberSelectedFromAnotherGroup(nonEnrolledGroups);
		if(!isMemberSelectedFromAnotherGroup) {
			return;
		}

		// list of all selected members from all groups;
        $scope.checkedNonEnrolledMembers = [];

		// set of groups with at least one selected members;
        $scope.checkedNonEnrolledGroups = {};

		// flag to hide modal buttons for grouping case which is not allowed
        $scope.multiGroupFailureNotAllowed = false;
        $scope.multiGroupFailureNonStandardNotAllowed = false;


        angular.forEach(nonEnrolledGroups, function(group) {
                angular.forEach(group.memberList, function(member) {
                    if ($('#'+member.id+group.id).is(":checked")){
                        member.isChecked = true;
                        $scope.checkedNonEnrolledMembers.push(member);

                        if (!$scope.checkedNonEnrolledGroups.id) {
                            $scope.checkedNonEnrolledGroups[group.id] = group;
                        }

                    }
                });
		});

		var firstGroup = $scope.checkedNonEnrolledGroups[Object.keys($scope.checkedNonEnrolledGroups)[0]];
		var secondGroup = $scope.checkedNonEnrolledGroups[Object.keys($scope.checkedNonEnrolledGroups)[1]];

		var isFirstGroupSubsidized = $scope.isSubsidizedGroup(firstGroup);
		var isSecondGroupSubsidized = $scope.isSubsidizedGroup(secondGroup);
		var hasNativeAmericanMemberFirstGroup = $scope.hasNativeAmericanMember(firstGroup);
		var hasNativeAmericanMemberSecondGroup = $scope.hasNativeAmericanMember(secondGroup);
		var isFirstGroupNonStandard = firstGroup.isNonStandardRelationGroup;
		var isSecondGroupNonStandard = secondGroup.isNonStandardRelationGroup;
		
		if((isFirstGroupNonStandard === true && isSecondGroupNonStandard === true) || (isFirstGroupNonStandard !== isSecondGroupNonStandard)){
			$scope.multiGroupFailureNonStandardNotAllowed = $scope.stateCode === 'ID';
			$scope.multiGroupFailure = true;
		}		

		if((isFirstGroupSubsidized === true && isSecondGroupSubsidized === false) ||
		   (isFirstGroupSubsidized === false && isSecondGroupSubsidized === true)) {

			$scope.multiGroupFailureNotAllowed = $scope.stateCode !== 'MN';
			$scope.multiGroupFailure = true;

			// TODO: Revisit later..
           // $scope.updateSelectedMemberCount();
		}

		if((isFirstGroupSubsidized === false && isSecondGroupSubsidized === false) ||
		   (isFirstGroupSubsidized === true && isSecondGroupSubsidized === true)){

			$scope.multiGroupFailure = true;

			if(firstGroup.groupCsr !== secondGroup.groupCsr){
				if( (hasNativeAmericanMemberFirstGroup || hasNativeAmericanMemberSecondGroup) &&
					((firstGroup.groupCsr === "CS2" || firstGroup.groupCsr === "CS3" || firstGroup.groupCsr === "CS4") ||
					( secondGroup.groupCsr === "CS2" || secondGroup.groupCsr === "CS3" || secondGroup.groupCsr === "CS4"))) {
					$scope.multiGroupFailureChangeRecommendedNativeAmerican = true;
				} else {
					$scope.multiGroupFailureChangeRecommended = true;
				}
			} else {
				//Do nothing
			}
		}
	};

	$scope.undoSelection = function(){
		var nonEnrolledGroups = $scope.getNonEnrolledGroups();

        if(nonEnrolledGroups.length === 1) {
			return;
		}

        angular.forEach(nonEnrolledGroups, function(group) {
            angular.forEach(group.memberList, function(member) {
                if (member.isChecked && $scope.lastMemberCheckedId === member.id){
                    $('#'+member.id+group.id).attr('checked', false);
                    member.isChecked = false;
                    $scope.checkedNonEnrolledMembers = $scope.checkedNonEnrolledMembers.myFilter(function(checkedNonEnrolledMember) {
                        return checkedNonEnrolledMember.id !== $scope.lastMemberCheckedId
                    })
                }
            });
        });

		$scope.UnEnrolledhealthGroups.selectedMembersCount = $scope.checkedNonEnrolledMembers.length;
		$scope.multiGroupFailure = false;
	};

	$scope.getNonEnrolledGroups = function(){
		var nonEnrolledGroups = [];
        angular.forEach($scope.healthGroups, function(group, index){
			// skip enrolled groups(
			var isEnrolledGroup = false;
			angular.forEach(group.memberList, function(member){
				if(member.healthEnrollmentId ) {
					isEnrolledGroup = true;
				}
			});
			if(!isEnrolledGroup) {
				nonEnrolledGroups.push(group);
			}
		});

        return nonEnrolledGroups;
    };

    $scope.checkIfMemberSelectedFromAnotherGroup = function(groups){
		var membersFromGroupsSelected = [];

        angular.forEach(groups, function(group, index){
            // skip enrolled groups
            var isMemberSelected = false;
            angular.forEach(group.memberList, function(member){
                if($('#'+member.id+group.id).is(":checked")){
                    isMemberSelected = true;
                }
            });
            if(isMemberSelected) {
                membersFromGroupsSelected.push(group);
            }
        });

        return membersFromGroupsSelected.length > 1;
	};

	$scope.disenrollsepapp = function(sepEventsNumber){
		$scope.loader = true;
		var url = 'indportal/disenrollforsep';
		var data = {'caseNumber': sepEventsNumber};
		var params = {
				"coverageYear" : $('#coverageYear').val()
		};
		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'},
			params:params
		};

		var response = $http.post(url, data, config);

		response.success(function(data, status, headers, config) {
			$scope.loader = false;
			if(data === "success"){
				$scope.disenrollresult = "success";
			}
			else{
				$scope.disenrollresult = "failure";
			}
		});
		response.error(function() {
			$scope.disenrollresult = "failure";
			$scope.loader = false;
		});
	};

	$scope.csractions = function(index) {
		$('#csrMenu_'+index).slideToggle();
	};


	$scope.modalForm = {};
	$scope.cancelApplicationAlert = function(params, coverageYear, applicationDentalStatus, applicationStatus) {
		$scope.caseNumber = params;		
		if (($scope.stateCode === 'NV' || $scope.stateCode === 'ID') &&  applicationDentalStatus === 'EN' && applicationStatus === 'Eligibility Received') {
			$scope.cancelEnrolledApplication(params, coverageYear);
		}else{
			$scope.modalForm.openDialog = true;
		}
	};

//HIX-87479 - HIX-86989 As an exchange system, allow consumers to change their termination date for an enrollment that has been terminated with a future date

$scope.cendDateOptions = {
	currentMode : false,
	openSuccessCTDModal: false,
	openFailCTDModal: false
  };
function convertDate(date) {
	var parts = date.toString().split(" ");
	var months = {
	  Jan: "01",
	  Feb: "02",
	  Mar: "03",
	  Apr: "04",
	  May: "05",
	  Jun: "06",
	  Jul: "07",
	  Aug: "08",
	  Sep: "09",
	  Oct: "10",
	  Nov: "11",
	  Dec: "12"
	};
	return months[parts[1]]+"/"+parts[2]+"/"+parts[3];
}
    $scope.terminationDates = [];

$scope.editTerminationDate = function(myEnrollment)	{
	$scope.terminationDates = [];
	$scope.cendDateOptions.currentMode = true;
	$scope.currentCoverageEndDate = myEnrollment.coverageEndDate;
	$scope.myEnrollmentEdit = myEnrollment;
	var startDate = new Date() > new Date(myEnrollment.coverageStartDate)? new Date() : new Date(myEnrollment.coverageStartDate);
	var endDate = new Date(myEnrollment.coverageEndDate);

	var startMonth = startDate.getMonth();
	var endMonth = endDate.getMonth();

	var year = endDate.getFullYear();

	//var pickMonths = endMonth - startMonth;
	for (var i=startMonth; i < endMonth; i++){
		var lastDay = new Date(year, i + 1, 0);
		var formatedDated = convertDate(lastDay);
		$scope.terminationDates.push(formatedDated);
	}
};

$scope.updateTerminationDate = function(){
	$scope.loader = true;
	$scope.cendDateOptions.currentMode = false;

	var url = 'indportal/enrollments/terminationDate';
	var data = {
		'healthEnrollmentId' : $scope.myEnrollmentEdit.enrollmentId,
		'caseNumber': $scope.myEnrollmentEdit.caseNumber,
		'reasonCode': 'OTHER',
		'coverageStartDate':  $scope.myEnrollmentEdit.coverageStartDate,
		'terminationDate': $scope.newCoverageEndDate
	};

	var promise = $http.post(url, data);

	promise.success(function(response) {
		$scope.loader = false;
		$scope.newCoverageEndDate = '';
		if(response === 'success'){
			$scope.cendDateOptions.openSuccessCTDModal = true;
			$scope.getEnrollmentsHistory();
		}else{
			$scope.cendDateOptions.openFailCTDModal = true;
		}
	});

	promise.error(function() {
		$scope.loader = false;
		$scope.cendDateOptions.openFailCTDModal = true;
		$scope.newCoverageEndDate = '';
	});

};

	//HIX-69292 - Impacts to change reporting for auto renewal
	$scope.checkForAutoRenewal = function(isNonFinancial,applicationStatus,caseNumber,event,typeOfCheck,covYear) {

		$scope.loader = true;
		var url = 'indportal/activeApplicationCheck';


		var params = {
				"coverageYear": covYear
		};
		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'},
			params:params
		};

		var response = $http.post(url, null, config);

		response.success(function(data, status, headers, config) {

			$scope.loader = false;

			if(data == 'true'){
				$scope.openAutoRenewalLceDialog = true;
				if(typeOfCheck == 'reportAChange'){
					$('#autoRenewalButton').click(function(){
						$scope.reportaChange(isNonFinancial,applicationStatus,caseNumber,covYear);
					});
				}
				if(typeOfCheck == 'enroll'){
					$('#autoRenewalButton').click(function(){
						$scope.preEnroll(event,covYear);
					});
				}
				if(typeOfCheck == 'changePlan'){
					$('#autoRenewalButton').click(function(){
						$scope.changePlanInSEP(event);
					});
				}
				if(typeOfCheck == 'changePlanTribe'){
					$('#autoRenewalButton').click(function(){
						$scope.changePlan(event);
					});
				}
			}
			else{
				if(typeOfCheck == 'reportAChange'){
					$scope.reportaChange(isNonFinancial,applicationStatus,caseNumber,covYear);
				}
				if(typeOfCheck == 'enroll'){
					$scope.preEnroll(event,covYear);
				}
				if(typeOfCheck == 'changePlan'){
					$scope.changePlanInSEP(event);
				}
				if(typeOfCheck == 'changePlanTribe'){
					$scope.changePlan(event);
				}
			}

		});
		response.error(function() {
			$scope.loader = false;
		});

	};

	//Function leads to report a change page (under different angular app)
	$scope.reportaChange = function(isNonFinancial,applicationStatus,caseNumber,coverageYear) {

		if(isNonFinancial == true && applicationStatus == 'Enrolled (Or Active)'){

			$scope.loader = true;
			var url = 'indportal/sepcheck';

			var params = {
					'caseNumber': caseNumber
			};
			var config = {
				headers: { 'Content-Type': 'application/json; charset=UTF-8'},
				params:params
			};

			var response = $http.post(url, null, config);

			response.success(function(data, status, headers, config) {

				if(data == 'true'){
					location.href='./iex/lce/reportyourchange?coverageYear='+coverageYear;
				}
				else{
					$location.path('/qephome');
				}

			});
			response.error(function() {
				$scope.loader = false;
			});


		}else{
			$scope.openDialoglce = true;
		}
	};

	$scope.isRecTribe = $('#isRecTribe').val();

	$scope.disEnrollPlan = function(planType, coverageDate, encryptedGroupEnrollmentId, enrollmentAction){

        var enrollCaseNumber = localStorage.getItem('enrollCaseNumber');
        if (!enrollCaseNumber  || enrollCaseNumber === "undefined") {
            enrollCaseNumber = $('#enrollCaseNumber').val();
        }
        
    	for(var i = 0; i<$scope.disenrollDialogDataList.length; i++){
    		if($scope.disenrollDialogDataList[i].groupEnrollmentId === encryptedGroupEnrollmentId){
    			$scope.isPlanEffectuated = $scope.disenrollDialogDataList[i].isPlanEffectuated;
            	$scope.endOfCurrentMonth = $scope.disenrollDialogDataList[i].endOfCurrentMonth;
            	$scope.endOfNextMonth = $scope.disenrollDialogDataList[i].endOfNextMonth;
            	$scope.endOfMonthAfterNext = $scope.disenrollDialogDataList[i].endOfMonthAfterNext;
                $scope.planName = $scope.disenrollDialogDataList[i].planName;
                $scope.hiosIssuerId = $scope.disenrollDialogDataList[i].hiosIssuerId;
                $scope.logoUrl = "./download/logo/" + $scope.hiosIssuerId;

    		}    		
        }

        var disenrollGroup = {
        		caseNumber: enrollCaseNumber,
        		coverageStartDate: coverageDate,
        		typeOfPlan: planType,
        		enrollmentId: encryptedGroupEnrollmentId,
        		disEnroll: true,
        		isPlanEffectuated: $scope.isPlanEffectuated,
        		endOfCurrentMonth: $scope.endOfCurrentMonth,
        		endOfNextMonth: $scope.endOfNextMonth,
        		endOfMonthAfterNext: $scope.endOfMonthAfterNext,
                hiosIssuerId:$scope.hiosIssuerId,
                planName:$scope.planName,
                logoUrl: $scope.logoUrl,
                insideOEEnrollmentWindow: $scope.isInsideOEEnrollmentWindow
          	};
        $scope.disenrollGroup = disenrollGroup;
        $scope.disenrollAlert('customGrouping', planType, -1, enrollmentAction);
	};

	$scope.disenrollAlert = function(from, planType, enrollmentId, enrollmentAction) {
		$scope.enrollmentAction = true;
		if(from === 'planSummary'){
			$scope.enrollmentAction = enrollmentAction;
			if(planType === 'both'){
				$scope.typeOfPlan = 'both';
				angular.forEach($scope.planDetails.activeHealthPlanDetails, function(healthItem, index){
					$scope.planSummaryDetails = healthItem;
				});
				angular.forEach($scope.planDetails.activeDentalPlanDetails, function(dentalItem, index){
					$scope.dentaldetails = dentalItem;
				})
			}else if(planType === 'Health'){
				$scope.typeOfPlan = 'Health';
				for(var i = 0; i<$scope.planDetails.activeHealthPlanDetails.length; i++){
		    		if($scope.planDetails.activeHealthPlanDetails[i].enrollmentId === enrollmentId){
		    			$scope.planSummaryDetails = $scope.planDetails.activeHealthPlanDetails[i];
		    		}    		
		        }
			}else{
				$scope.typeOfPlan = 'Dental';
				for(var i = 0; i<$scope.planDetails.activeDentalPlanDetails.length; i++){
		    		if($scope.planDetails.activeDentalPlanDetails[i].enrollmentId === enrollmentId){
		    			$scope.planSummaryDetails = $scope.planDetails.activeDentalPlanDetails[i];;
		    		}    		
		        }
			}
		}else if(from === 'enrollmentHistory'){
			$scope.enrollmentAction = enrollmentAction;
			if(planType === 'Health'){
				$scope.typeOfPlan = 'Health';
				for(var i = 0; i<$scope.healthEnrollments.length; i++){
		    		if($scope.healthEnrollments[i].enrollmentId === enrollmentId){
		    			$scope.planSummaryDetails = $scope.healthEnrollments[i];
		    		}    		
		        }
			}else{
				$scope.typeOfPlan = 'Dental';
				for(var i = 0; i<$scope.dentalEnrollments.length; i++){
		    		if($scope.dentalEnrollments[i].enrollmentId === enrollmentId){
		    			$scope.planSummaryDetails = $scope.dentalEnrollments[i];
		    		}    		
		        }
			}
		}else if(from === 'customGrouping'){
			$scope.enrollmentAction = enrollmentAction;
			$scope.typeOfPlan = planType;
			$scope.planSummaryDetails = $scope.disenrollGroup;
		}

		$scope.disenrollcasenumber= $scope.planSummaryDetails.caseNumber;

		var covStartDate = $scope.planSummaryDetails.coverageStartDate;
		/*$scope.enrollmentId= enrollmentId;
		$scope.planId = planId;*/

		var currentDate = new Date();
		var coverageStartDate = new Date(covStartDate);
		if (coverageStartDate >= currentDate) $scope.validCoverageSDate = false;
		else $scope.validCoverageSDate = true;
		/*
		 * HIX-59752 Prevent Disenroll Action on an enrolled application
		 * if there is any another application in Eligibility Received status
		 */
		if($scope.typeOfPlan == "Health" || $scope.typeOfPlan == "both"){
			$scope.loader = true;

			var url = 'indportal/checkForPendingApplication';

			var params = {
						"coverageYear":covStartDate.slice(-4)
			};
			var config = {
				headers: { 'Content-Type': 'application/json; charset=UTF-8'},
				params:params
			};

			var response = $http.post(url, null, config);

			response.then(function(responseData, status, headers, config){
				if(responseData.data=="true"){
					$scope.loader = false;
					$scope.pendingAppDialog = true;
				}else{
					$scope.loader = false;
					$scope.disenrollDialog = true;
				}
			}, function(responseData, status, headers, config){
				$scope.loader = false;
				$scope.disenrollDialog = true;
			});
		} else {
			$scope.loader = false;
			$scope.disenrollDialog = true;
		}
	};

	$scope.continueDisenrollment = function (){
		$scope.comingStartDayDialog = false;
		$scope.disenrollDialog = true;
	};

	$scope.disenrollmentReason = function() {

		if ($scope.stateCode === 'MN') {
			$scope.disenrollDialog = false;
			$scope.areYourSureDialog = true;
		} else {
			$scope.disenrollDialog = false;

			/*if($scope.typeOfPlan === "Health"){
				getPlanSummary($scope.enrollmentId, null, $scope.planId);
			}else if($scope.typeOfPlan === "Dental"){
				getPlanSummary(null, $scope.enrollmentId, $scope.planId);
			}*/
	
	
			$scope.areYourSureDialog = true;
			//} else {
			$scope.goToDisenrollDateModal();
			//}
		}
		
	};

	$scope.saveSelectedDate = function (dateValue) {
		if (dateValue === 'CURRENT_MONTH') {
			$scope.selectedCoverageDate = $scope.actualDatesEndOfCurrentMonth;
		} else if (dateValue === 'NEXT_MONTH') {
			$scope.selectedCoverageDate = $scope.actualDatesEndOfNextMonth;
		} else if (dateValue === 'MONTH_AFTER_NEXT_MONTH') {
			$scope.selectedCoverageDate = $scope.actualDatesEndOfMonthAfterNext;
		}
	}

	$scope.goToDisenrollDateModal = function(){

		var isPlanEffectuated = "N";
		var isInsideOe = $("#isInsideOe").val();
		var typeOfPlan = $scope.typeOfPlan;
		var insideOEEnrollmentWindow = false;

		if($scope.planSummaryDetails){
			if(typeOfPlan === "Health" || $scope.typeOfPlan === "both"){
				$scope.healthdetails = $scope.planSummaryDetails;
			} else if(typeOfPlan === "Dental"){
				$scope.dentaldetails = $scope.planSummaryDetails;
			}
		}

		if (typeOfPlan === "Health" || $scope.typeOfPlan === "both"){
			isPlanEffectuated =	$scope.healthdetails.isPlanEffectuated;
			insideOEEnrollmentWindow = $scope.healthdetails.insideOEEnrollmentWindow;
		} else if (typeOfPlan === "Dental"){
			isPlanEffectuated =	$scope.dentaldetails.isPlanEffectuated;
			insideOEEnrollmentWindow = $scope.dentaldetails.insideOEEnrollmentWindow;
        }
        if(isPlanEffectuated === 'Y' && (!insideOEEnrollmentWindow)){

			$scope.areYourSureDialog = false;
			$scope.disenrollDateDialog = {step: 1};
			$scope.disenrollmentDates = [];
			$scope.selectedCoverageDate = null;

			$scope.actualDatesEndOfCurrentMonth = '';
			$scope.actualDatesEndOfNextMonth = '';
			$scope.actualDatesEndOfMonthAfterNext = '';

			if (typeOfPlan === "Health" || $scope.typeOfPlan === "both"){
				$scope.actualDatesEndOfCurrentMonth = $scope.healthdetails.endOfCurrentMonth;
				$scope.actualDatesEndOfNextMonth = $scope.healthdetails.endOfNextMonth;
				$scope.actualDatesEndOfMonthAfterNext = $scope.healthdetails.endOfMonthAfterNext;
			}
			else if (typeOfPlan === "Dental"){
				$scope.actualDatesEndOfCurrentMonth = $scope.dentaldetails.endOfCurrentMonth;
				$scope.actualDatesEndOfNextMonth = $scope.dentaldetails.endOfNextMonth;
				$scope.actualDatesEndOfMonthAfterNext = $scope.dentaldetails.endOfMonthAfterNext;
			}
		}
		else{
			$scope.submitDisenrollment();
		}
	};

	$scope.cancelDisenroll = function(){
		$scope.checked = null;
		$scope.otherReason = "";
	};

	$scope.submitDisenrollment = function() {
		$scope.areYourSureDialog = false;
		$scope.loader = true;
		var url = null;
		var data = null;

		if (!$scope.checked) {
			$scope.checked = "NO_OPTION_SELECTED";
        }
        //need to update reasonCode
		if ($scope.typeOfPlan === "both"){
			url = 'indportal/terminatePlan';
			/*data = {'healthEnrollmentId':$scope.healthdetails.enrollmentId,'caseNumber': $scope.disenrollcasenumber, 'reasonCode':$scope.checked, 'otherReason':$scope.otherReason,'coverageStartDate':$scope.healthdetails.coverageStartDate,'isRecTribe':$scope.isRecTribe};
			if($scope.dentaldetails && $scope.dentaldetails.disEnroll){
				data['dentalEnrollmentId'] = $scope.dentaldetails.enrollmentId;
			}*/
			data = {'applicationId':$scope.healthdetails.ssapApplicationId, 'healthEnrollmentId':$scope.healthdetails.enrollmentId,'caseNumber': $scope.disenrollcasenumber, 'reasonCode':$scope.checked, 'otherReason':$scope.otherReason,'coverageStartDate':$scope.healthdetails.coverageStartDate,'isRecTribe':$scope.isRecTribe};
			if($scope.dentaldetails && $scope.dentaldetails.disEnroll){
				data['dentalEnrollmentId'] = $scope.dentaldetails.enrollmentId;
			}
		}
		else if ($scope.typeOfPlan === "Health"){
			url = 'indportal/terminateHealthPlan';
			data = {'healthEnrollmentId':$scope.healthdetails.enrollmentId,'caseNumber': $scope.disenrollcasenumber, 'reasonCode':$scope.checked, 'otherReason':$scope.otherReason, 'coverageStartDate':$scope.healthdetails.coverageStartDate,'isRecTribe':$scope.isRecTribe};
			if($scope.dentaldetails && $scope.dentaldetails.disEnroll){
				data['dentalActive'] = true;
				data['dentalAptc'] = $scope.dentaldetails.electedAptc;
				data['dentalEnrollmentId'] = $scope.dentaldetails.enrollmentId;
			}
		} else if ($scope.typeOfPlan === "Dental"){
			url = 'indportal/terminateDentalPlan';
			data = {'dentalEnrollmentId': $scope.dentaldetails.enrollmentId, 'caseNumber': $scope.disenrollcasenumber,'reasonCode':$scope.checked, 'otherReason':$scope.otherReason, 'coverageStartDate':$scope.dentaldetails.coverageStartDate};
			if($scope.healthdetails && $scope.healthdetails.disEnroll){
				data['healthActive'] = true;
			}
        }
        if($scope.disenrollDateDialog != undefined && $scope.disenrollDateDialog.date){
			data['terminationDateChoice'] = $scope.disenrollDateDialog.date;
		}

		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'}
		};

		var response = $http.post(url, data, config);
		response.then(function(responseData, status, headers, config){
			if(typeof(responseData.data)==="string" && (responseData.data !== "success" && responseData.data !== "failure")){
				$scope.loader = false;
				location.href='./indportal';
				return;
			}

			if(responseData.data=="success"){
				$scope.loader = false;
				$scope.submitDisenrollmentDialog = true;
			}else{
				$scope.loader = false;
				$scope.submitDisenrollmentFailureDialog = true;
			}
		}, function(responseData, status, headers, config){
			$scope.loader = false;
			$scope.submitDisenrollmentFailureDialog = true;
		});
	};

	$scope.refreshEnrollmentHistory = function(){
		$scope.modalForm.overrideEnrollmentSuccessful = false;
		$location.path('/enrollmenthistory');
		$window.location.reload(true);
	};

	$scope.goTo = function(dest){
		$location.path(dest);
	};

	$scope.goHome = function(){
		$scope.submitDisenrollmentDialog= false;
		$location.path('/');
	};



	$scope.modelattrs =
	                 {'aptc': $('#aptc').val(),
	                  'csr': $('#csr').val(),
	                  'sCode':$('#sCode').val(),
	                  'caseNumber':$('#enrollCaseNumber').val(),
	                  'enCoverageEndDate':$('#coverageEndDate').val()};

	$scope.coverageStartDate = $('#coverageStartDate').val();

	$scope.showEligibilityDetailsFromHome= function(event){
		$scope.showEligibilityResults(event.target.id);
	};

	$scope.preEnroll= function(event,covYear){
		$scope.loader = true;
		var postData = 'caseNumber=' + event.target.id;
		var params = {
				"coverageYear": covYear
		};
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
			params:params
		};
		var response = $http.post('indportal/getApplicantsForAddInfo', postData, config);
		response.success(function(data, status, headers, config) {
			if (data === "" || data === null){
				$scope.loader = false;
				$scope.openInitPreEnroll = true;
				return;
			}
			else if (data != null && data.errCode != "" && data.errCode != null){
				$scope.loader = false;
				$scope.openPreEnrollESD = true;
				return;
			}
			coverageStartDateAddInfo = data.coverageStartDate;
			coverageYearOver = data.coverageYearOver;
			addDetails = data.applicantDetails["eligiblitydetails"];
			addInElgDetails = data.applicantDetails["inEligibilitydetails"];
			addFlag = data["householdExempt"];
			isSepFlag = data["isSep"];
			isChangePlanFlowFlag = false;
			isSEPPlanFlow = false;
			tempCoverageYear = covYear;
			$location.path('/additionalinfo');
		});
		response.error(function() {
			$scope.loader = false;
			$scope.openInitPreEnroll = true;
		});
	};

	//Change plan option
	$scope.changePlan = function(event){
		$scope.loader = true;

		var allowChangePlan = $("#allowChangePlan").val();

		if(allowChangePlan == 'Y'){
			var postData = 'caseNumber=' + event.target.id + '&isChangePlanFlowFlag=true';
			var params = {
					"coverageYear": $('#coverageYear').val()
			};
			var config = {
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
				params:params
			};
			var response = $http.post('indportal/getApplicantsForAddInfo', postData, config);
			response.success(function(data, status, headers, config) {
				if (data === "" || data === null){
					$scope.loader = false;
					$scope.openInitPreEnroll = true;
					return;
				}
				coverageStartDateAddInfo = data.coverageStartDate;
				coverageYearOver = data.coverageYearOver;
				addDetails = data.applicantDetails["eligiblitydetails"];
				addInElgDetails = data.applicantDetails["inEligibilitydetails"];
				addFlag = data["householdExempt"];
				isSepFlag = data["isSep"];
				isChangePlanFlowFlag = true;
				isSEPPlanFlow = false;
				$location.path('/additionalinfo');
			});
			response.error(function() {
				$scope.loader = false;
				$scope.openInitPreEnroll = true;
			});
		}
		else{
			$scope.loader = false;
			$scope.disAlloweChangePlan = true;
		}
	};

	//Change plan in SEP
	$scope.changePlanInSEP = function(event){
		$scope.loader = true;

		var postData = 'caseNumber=' + event.target.id + '&changeSEPPlanFlow=true';
		var params = {
				"coverageYear": $('#coverageYear').val()
		};
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
			params:params
		};
		var response = $http.post('indportal/getApplicantsForAddInfo', postData, config);
		response.success(function(data, status, headers, config) {
			if (data === "" || data === null){
				$scope.loader = false;
				$scope.openInitPreEnroll = true;
				return;
			}
			coverageStartDateAddInfo = data.coverageStartDate;
			coverageYearOver = data.coverageYearOver;
			addDetails = data.applicantDetails["eligiblitydetails"];
			addInElgDetails = data.applicantDetails["inEligibilitydetails"];
			addFlag = data["householdExempt"];
			isSepFlag = data["isSep"];
			isChangePlanFlowFlag = false;
			isSEPPlanFlow = true;
			$location.path('/additionalinfo');
		});
		response.error(function() {
			$scope.loader = false;
			$scope.openInitPreEnroll = true;
		});

	};

	$scope.showApplicantDetails = function(){

		console.log('showApplicantDetails caseNumber=' + window.caseNumber + "window.coverageYear=" + window.coverageYear);
        $scope.loader = true;
        var postData = 'caseNumber=' + (window.caseNumber ? window.caseNumber : $('#enrollCaseNumber').val());
        var params = {
            "coverageYear": window.coverageYear
        };
        var config = {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            params:params
        };
		var response = $http.post('indportal/getApplicantsForAddInfo', postData, config);
        response.success(function(data, status, headers, config) {
            if (data === "" || data === null){
                $scope.loader = false;
                $scope.openInitPreEnroll = true;
                return;
            }
            else if (data != null && data.errCode != "" && data.errCode != null){
                $scope.loader = false;
                $scope.openPreEnrollESD = true;
                return;
            } else {
                var applicationStatus = data.applicationStatus;

                // Tobacco All is true if all members have saved their additional info
                var tobaccoAll = data.applicantDetails["eligiblitydetails"].filter(function(applicant, index) {
                    if(applicant.tobaccoUser === null || applicant.tobaccoUser === 'undefined') {
                        data.applicantDetails["eligiblitydetails"][index].tobaccoUser = false;
                        return true;
                    } else {
                        return false;
                    }
                }).length === 0;

                if((applicationStatus && applicationStatus !== "ER") && tobaccoAll && ($scope.stateCode === "NV" || $scope.stateCode === "MN" || $scope.stateCode === "ID")) {
                    $location.path('customGrouping');
			    }

                coverageStartDateAddInfo = data.coverageStartDate;
                coverageYearOver = data.coverageYearOver;
                addDetails = data.applicantDetails["eligiblitydetails"];
                addInElgDetails = data.applicantDetails["inEligibilitydetails"];
                addFlag = data["householdExempt"];
                isSepFlag = data["isSep"];
                isChangePlanFlowFlag = false;
                isSEPPlanFlow = false;
                // tempCoverageYear = covYear;

				$scope.additionalDetails = addDetails;
				angular.forEach($scope.additionalDetails, function (item) {
					item.exemptionNumber = '';
				});
				$scope.additionalInElgDetails = addInElgDetails;
				$scope.hardship = {
					exemption: addFlag,
					fillExemptNum: false
				};
                $scope.isSep = isSepFlag;
                $scope.isChangePlanFlow = isChangePlanFlowFlag;
                $scope.isSEPPlanFlow = isSEPPlanFlow;
                $scope.coverageStartDateAddInfo = coverageStartDateAddInfo;
                $scope.coverageYearOver = coverageYearOver;
                $scope.loader = false;
            }
        });
        response.error(function() {
            $scope.loader = false;
            $scope.openInitPreEnroll = true;
            $location.path('/');
        });

		//
		//
		// if (addDetails){
		// 	$scope.additionalDetails = addDetails;
		// 	$scope.additionalInElgDetails = addInElgDetails;
		// 	$scope.hardshipExemption = addFlag;
		// 	$scope.isSep = isSepFlag;
		// 	$scope.isChangePlanFlow = isChangePlanFlowFlag;
		// 	$scope.isSEPPlanFlow = isSEPPlanFlow;
		// 	$scope.coverageStartDateAddInfo = coverageStartDateAddInfo;
		// 	$scope.coverageYearOver = coverageYearOver;
		// } else {
		// 	$location.path('/');
		// }
	};

	$scope.validateGroup = function (){
		for(var i = 0; i < $scope.healthGroups.length; i++){
			var healthItem = $scope.healthGroups[i];
			for(var j = 0; j < healthItem.memberList.length; j++){
				var member = healthItem.memberList[j];
                if($('#'+member.id+healthItem.id).is(":checked") && $('#'+member.id+healthItem.id).is(":visible")){
					if(member.canBeAddedInEnrolledGroup !== undefined && member.canBeAddedInEnrolledGroup != null && member.canBeAddedInEnrolledGroup.length > 0){
						for(var k=0; k < member.canBeAddedInEnrolledGroup.length;k++){
							for(var l = 0; l < $scope.healthGroups.length; l++){
								var healthGroup = $scope.healthGroups[l];
								if(healthItem.id !== healthGroup.id){
									for(var m = 0; m < healthGroup.memberList.length; m++){
										var healthMember = healthGroup.memberList[m];
										if($('#'+healthMember.id+healthGroup.id).is(":checked") && $('#'+healthMember.id+healthGroup.id).is(":visible")){
											if(healthMember.healthEnrollmentId === undefined){
                                                $scope.customGroupingFailure = true;
												$scope.errorValidateGroup = true;
												// alert('Please select only one group');
												return false;
											}
										}
									}
								}
							}
						}
					}
				}
			}

		}

		return true;
	};


	$scope.enrollAfterCustomGrouping = function (tabType,groupId, groupsToEnroll, isChangePlan){
		var changePlanFlow = false;
		if(groupsToEnroll !== null && groupsToEnroll[0].appState !== null){
			changePlanFlow = groupsToEnroll[0].appState.isChangePlan;
		}
		var groupMemberArray = [];
		var programType = null;
		var enrollmentEndDate = null;
		var grpMaxAptc = null;
		$scope.isGroupRenewal = false;
		if(tabType == 'HEALTH'){
			programType = "H";
			angular.forEach(groupsToEnroll, function(healthItem, index){
				if(healthItem.id == groupId){
					$scope.coverageDate = healthItem.coverageDate;
				}
			});
			angular.forEach(groupsToEnroll, function(healthItem, index){
				angular.forEach(healthItem.memberList, function(healthMember, key){
					if($('#'+healthMember.id+healthItem.id).is(":checked") && groupMemberArray.myFilter(function(x) { return  (x.id === healthMember.id) } ).length === 0){
						if(healthMember.healthEnrollmentId === undefined && healthMember.canBeAddedInEnrolledGroup === undefined ){
							groupMemberArray.push(healthMember);
							//$scope.grpMaxAptc = healthMember.maxAptc;
						}else{
							if(healthMember.canBeAddedInEnrolledGroup !== undefined && healthMember.canBeAddedInEnrolledGroup.length > 0){
								groupMemberArray.push(healthMember);
							}else{
								if(healthMember.healthEnrollmentId !== undefined){
									if(healthItem.id == groupId){
										grpMaxAptc = healthItem.maxAptc;
										$scope.grpMaxStateSubsidy = healthItem.maxStateSubsidy;
										$scope.encData = healthItem.encData;
										$scope.groupEnrollmentId = healthItem.groupEnrollmentId;
										$scope.isGroupRenewal = healthItem.isRenewal;
										enrollmentEndDate = healthItem.enrollmentEndDate;
										for(var i = 0; i < healthItem.memberList.length; i++){
											var member = healthItem.memberList[i];
											if(member.canBeAddedInEnrolledGroup !== undefined && member.canBeAddedInEnrolledGroup.length > 0 && $('#'+member.id+healthItem.id).is(":checked")){
												groupMemberArray.push(healthMember);
												break;
											} else {
	                                            //$scope.grpMaxAptc = $scope.getMaxAPTC(healthItem);
												// If all the members are in the group are enrolled.. add the member here..[Change Plan Button]
												if(isChangePlan &&
													$scope.isAllEnrolled(healthItem) &&
													$('#'+member.id+healthItem.id).is(":checked") &&
													groupMemberArray.myFilter(function(x) { return x.id === healthMember.id } ).length === 0){
	                                                groupMemberArray.push(healthMember);
	
	                                            }
											}
										}
									}
								}
							}
						}
					}
				});
			});

		}else{
			programType = "D";
			angular.forEach($scope.dentalGroups, function(dentalItem, index){
				if(dentalItem.id == groupId){
					$scope.coverageDate = dentalItem.coverageDate;
					$scope.isGroupRenewal = dentalItem.isRenewal;
					enrollmentEndDate = dentalItem.enrollmentEndDate;
				}
			});
			var kids = 0;
			var totalKids = 0;
			var adults = 0;
			var totalMembers = 0;
            angular.forEach($scope.dentalGroups, function(dentalItem, index){
                angular.forEach(dentalItem.memberList, function(dentalMember, key){
                	if(dentalMember.dentalEnrollmentId !== undefined){
						grpMaxAptc = dentalItem.maxAptc;
					}
                    totalMembers++;

                    if(dentalMember.ageAtCoverageDate < 19) {
                        totalKids++;
                    }

                    if(dentalMember.isChecked && dentalMember.healthEnrollmentId) {
                    	if(dentalMember.ageAtCoverageDate >= 19 ){
                            adults++;
						} else {
                            kids++;
						}
                    }
                });
			});

            //TODO: Dicuss with seema what happens if two child have healthEnrollmentId and third doesn't have healthEnrollmentId ..
			// and user checks two kid, in this case not all kids are enrolled.. what do we do in this
            // if one kid is selected and others are not. User will see this error message.
            if(totalMembers > 0  && kids > 0 &&( $scope.stateCode === "CA" && totalKids !== kids )){
            	if((!isChangePlan && $scope.applicationType === "OE") || ($scope.applicationType !== "OE")){
            		$scope.customGroupingFailure = true;
            		$scope.errorDentalKids = true;
            		return;
            	}
            }

            // Whenever a kid needs to enroll in a Dental plan, there should be at least one adult that needs to sign up for the enrollment.
			// If a user tries to select  only kid(s) in a dental plan, there will an error message shown.
            if(totalMembers > 0 && ($scope.stateCode === "CA" && adults === 0) && kids > 0 && ($scope.stateCode === "CA" || totalMembers!==kids)){
                $scope.customGroupingFailure = true;
				$scope.errorDentalAdults = true;
            	return;
			}

			angular.forEach($scope.dentalGroups, function(dentalItem, index){
				angular.forEach(dentalItem.memberList, function(dentalMember, key){
					
				   if(dentalMember.isChecked && ($scope.stateCode !== "CA" || (dentalMember.healthEnrollmentId !== undefined && $scope.stateCode === "CA"))){
						if($scope.applicationType === "OE"){	
							if((isChangePlan && dentalMember.dentalEnrollmentId !== undefined) || (!isChangePlan)){
								groupMemberArray.push(dentalMember);		
							}
						}
						else{
							groupMemberArray.push(dentalMember);
						}
					}
					else if(dentalMember.isChecked && dentalMember.healthEnrollmentId === undefined  && dentalMember.dentalEnrollmentId !== undefined && $scope.stateCode === "CA"){
						groupMemberArray.push(dentalMember);
					}
				})
			});
		}

		$scope.loader = true;
		$scope.addGroupDetailsToSend = {
			isChangePlanFlow : changePlanFlow,
			isSEPPlanFlow : isSEPPlanFlow,
			group : {
				memberList: groupMemberArray,
				groupType: tabType,
				programType: programType,
				maxAptc: grpMaxAptc,
                maxStateSubsidy: $scope.grpMaxStateSubsidy,
				encData: $scope.encData,
				isRenewal: $scope.isGroupRenewal,
				groupEnrollmentId: $scope.groupEnrollmentId,
				coverageDate: $scope.coverageDate,
				enrollmentEndDate: enrollmentEndDate
			}
		};

		var postdata = angular.toJson($scope.addGroupDetailsToSend);
		saveCustomGrouping(postdata);
	};

	saveCustomGrouping = function(postdata){		
        var enrollCaseNumber = localStorage.getItem('enrollCaseNumber');
        if (!enrollCaseNumber  || enrollCaseNumber === "undefined") {
            enrollCaseNumber = $('#enrollCaseNumber').val();
        }

		var params = {
				"caseNumber": enrollCaseNumber
		};
		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'},
			params:params
		};

		$http.post('indportal/saveCustomGrouping', postdata, config)
			.success(function(data, status, headers, config){

				if(typeof(data)!=="string" && data !== "" && data.status != null){
					if(data.message === 'This action is not permitted as your coverage through this plan is ending soon.'){
						$scope.shopForNewPlanFailure = true;						
					}else{
					$scope.loader = false;
					$scope.customGroupingFailure = true;
					$scope.customGroupingFailureMsg = data.message;
					}
				}else if(data.startsWith('failure')){
					$scope.loader = false;
					$scope.customGroupingFailure = true;
					$scope.customGroupingFailureMsg = data;
				}else{
					location.href='./private/setHousehold/'+data;
				}
			}).error(function(data, status, headers, config){
				$scope.loader = false;
				$scope.customGroupingFailure = true;
				$scope.customGroupingFailureMsg = data;
			});
	};

	$scope.shopForNewPlan = function(){
		var addGroupDetails = $scope.addGroupDetailsToSend;
		if((addGroupDetails !==null || addGroupDetails !== undefined) && (addGroupDetails.group !== null || addGroupDetails.group !== undefined) ){
			var newGroup = addGroupDetails.group;
			newGroup.groupEnrollmentId = null;
			newGroup.enrollmentEndDate = null;
			if(newGroup.memberList !== null || newGroup.memberList != undefined){
				angular.forEach(newGroup.memberList, function(member, key){
					if(newGroup.groupType !== 'DENTAL' && member.healthEnrollmentId){
						member.healthEnrollmentId = null;
					}
					if(member.dentalEnrollmentId){
						member.dentalEnrollmentId = null;
					}
				});
			}
			addGroupDetails.group = newGroup;
			}
		var postdata = angular.toJson(addGroupDetails);
		saveCustomGrouping(postdata);
	};


    $scope.addToExistingGroup = function(tabType,groupId, groupToAdd, isChangePlan){
    	var groupsToEnroll = [];
        groupsToEnroll.push(groupToAdd);
        $scope.enrollAfterCustomGrouping(tabType, groupId, groupsToEnroll, isChangePlan);
	};

    $scope.changePlan = function(tabType,groupId, groupToChange){
    	var isChangePlan = true;
        $scope.addToExistingGroup(tabType,groupId, groupToChange, isChangePlan);
	};

	$scope.checkHasSelectedTobaccoUse = function(item) {
		if (!item.enableTobacco) {
			return true;
		} else {
			return item.enableTobacco && item.isTobaccoUser !== undefined;
		}
	};

	$scope.updateHardship = function() {
	};

	$scope.enrollAfterTobacoo = function (){
		var eliToSend = [];

		$scope.hardship.fillExemptNum = false;
		if ($scope.stateCode === 'MN' || $scope.stateCode === 'ID') {
			$scope.hasSelectedTobaccoUse = true;
			if ($scope.additionalDetails.length > 0) {
				$scope.hasSelectedTobaccoUse = $scope.additionalDetails.every($scope.checkHasSelectedTobaccoUse);
				$scope.additionalDetails.forEach(function(item) {
					item.tobaccoUser = item.isTobaccoUser !== undefined && item.isTobaccoUser;
				});
			}
			if (!$scope.hasSelectedTobaccoUse) return;
		}
		if ($scope.hardship.exemption){
			angular.forEach($scope.additionalDetails, function(val, key){
				if (!val.exemptionNumber){
					$scope.hardship.fillExemptNum = true;
					return;
				}
			});
			if ($scope.hardship.fillExemptNum) {
				return;
			}
            eliToSend = $scope.additionalDetails;

		} else {
			for (var i = 0; i < $scope.additionalDetails.length; i++){
				eliToSend[i] = {};
				for (var prop in $scope.additionalDetails[i]){
					if (prop !== "exemptionNumber"){
						eliToSend[i][prop] = $scope.additionalDetails[i][prop];
					}
				}
            }
		}
        $scope.loader = true;
		$scope.addDetailsToSend = {
			applicantDetails : {
				eligiblitydetails: eliToSend,
				inEligibilitydetails: $scope.additionalInElgDetails
			},
			householdExempt: $scope.hardship.exemption,
			isChangePlanFlow : $scope.isChangePlanFlow,
			isSEPPlanFlow : $scope.isSEPPlanFlow
		};

		var postdata = angular.toJson($scope.addDetailsToSend);
		var params = {
				"coverageYear": tempCoverageYear==null?$('#coverageYear').val() : tempCoverageYear
		};
		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'},
			params:params
		};

		$http.post('indportal/saveAdditionalInfo', postdata, config)
			.success(function(data, status, headers, config){
				if(typeof(data)!=="string" && data !== "" && data.status != null){
					$scope.loader = false;
					$scope.modalForm.techIssue = 'true';
				}else if(data.startsWith('failure')){
					$scope.loader = false;
					$scope.modalForm.techIssue = 'true';
				}else if(data == 'redirectToCustomGrouping'){
					$location.path('customGrouping');
			    }else{
					location.href='./private/setHousehold/'+data;
				}
			}).error(function(data, status, headers, config){
				$scope.loader = false;
				$scope.modalForm.techIssue = 'true';
			});
	};

	$scope.enroll= function(event){
		var postData = 'caseNumber=' + event.target.id;
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		};
		var response = $http.post('indportal/enroll', postData, config);
		response.success(function(data, status, headers, config) {
			if(typeof(data)!=="string" && data !== "" && data.status != null){
				$scope.loader = false;
				$scope.openInitFail = true;
			}else{
				location.href='./private/setHousehold/'+data;
			}
		});
		response.error(function() {
			$scope.openInitFail = true;
		});
	};
	$scope.initPortal = function(){
		var response = $http.get('indportal/init');
		response.success(function(data, status, headers, config) {
			if(typeof(data)==="string" && (data !== "success" && data !== "failure")){
				location.href='./indportal';
				return;
			}
			$scope.portalHome = data;
		});
	};
	//need error block

	$scope.init = function(){
		$scope.loader = true;
		//###############multi-year starts############################################
		var maCurrentCoverageYear = parseInt($('#maCurrentCoverageYear').val());
		var exchangeStartYear = parseInt($('#exchangeStartYear').val());
		var minStartYear = Math.min(maCurrentCoverageYear, exchangeStartYear);
		$scope.maCurrentCoverageYearArray = [];

		for(var i = maCurrentCoverageYear; i >= minStartYear; i--  ){
			$scope.maCurrentCoverageYearArray.push(i);
		}

		if($scope.maCurrentCoverageYear === undefined){
			$scope.maCurrentCoverageYear = maCurrentCoverageYear;
		}

		//###############multi-year ends############################################


		var response = $http.get('indportal/getApplications?coverageYear=' + $scope.maCurrentCoverageYear + '&tmstp='+new Date().getTime());
		response.success(function(data, status, headers, config) {
			$scope.loader = false;
			if(typeof(data)==="string" && (data !== "success" && data !== "failure")){
				location.href='./indportal';
				return;
			}
			$scope.currentApplications = data["currentApplications"];
			$scope.pastApplications = data["pastApplications"];
			$scope.householdCaseNumber = data["householdCaseId"];

			$scope.myAppsInsideOE = data.insideOE;
			$scope.myAppsInsideQEP = data.insideQEP;

		});
		response.error(function() {
			$scope.loader = false;
			$scope.openInitFail = true;
		});
};

$scope.$on('$viewContentLoaded', function(event) {
    $timeout(function() {
    	$scope.bindDatepicker();
    },0);
});

$scope.bindDatepicker = function(){

	/*Bind Datepicker to element
	 TODO:  currently we are using jquery for binding, we have to replace it in angular */
	$('.newSEPEndDate').each(function() {
		$(this).datepicker({
			autoclose : true,
			dateFormat : 'MM/DD/YYYY'
		}).on('changeDate', function(){
			$(this).datepicker('hide');
			$('input[name=newSEPEndDate]').trigger('blur');
		});
	}).on('hide', function(event) {
		event.preventDefault();
		event.stopPropagation();
	});

	$(".datePicker").each(function(){
		$(this).datepicker({
			autoclose: true
		});
	}).on('hide', function(event) {
		event.preventDefault();
		event.stopPropagation();
	});

	/*Setting z-index for date-picker when open in modal dialog */
	$('.js-dateTrigger').on('click', function(){
		setTimeout(function(){
			$('.datepicker').css({zIndex: 1070});
		},80);
	});
};

$scope.showVerificationResults = function(caseNumber){
	window.location= 'indportal/viewVerificationResults?caseNumber='+caseNumber;
};

$scope.showEligibilityDetails = function(){
	console.log('showEligibilityDetails caseNumber=' + window.caseNumber);
	var postData = 'caseNumber=' + $scope.caseNumber;
	var config = {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	};
	var response = $http.post('indportal/geteligibilitydetails', postData, config);
	response.success(function(data, status, headers, config){
		if(typeof(data)==="string" && (data !== "success" && data !== "failure")){
			location.href='./indportal';
			return;
		}
		hhdetails = data["householdEligibilityDetails"];
		mmdetails = data["membersEligibilityDetails"];
        $scope.householdEligibilityDetails = hhdetails;
        $scope.memberEligibilityDetails = mmdetails;

        // HARDCODE FOR ID. Quick fix to filter Medicaid and other unsupported eligibilities.
		if ($scope.stateCode === 'ID') {
			for(let i =0; i < $scope.memberEligibilityDetails.length; i++) {
				$scope.memberEligibilityDetails[i].memberEligibilities = $scope.memberEligibilityDetails[i].memberEligibilities.myFilter(function(memberEligibility) {
					if (memberEligibility.eligibilityStatus === 'Advanced Premium Tax Credit' ||
						memberEligibility.eligibilityStatus === 'Cost Sharing Reduction' ||
						memberEligibility.eligibilityStatus === 'Medical and Dental Insurance') {
						return memberEligibility.eligibilityStatus;
					}
				});
			}
		}

		// if(hhdetails=="" && mmdetails==""){
		// 	$scope.eligResultDialog = true;
		// }else{
		// 	$location.path('/eligibilityresults');
		// }
	});
	//need error block

};


$scope.showAppealsDetails = function(){
	$scope.appealsDetails = appeals;
};

$scope.showAppeals = function(){
	var config = {
			headers: {  'Content-Type': 'application/json; charset=UTF-8'}
	};
	var response = $http.post('indportal/getAppeals', config);
	response.success(function(data, status, headers, config){
		if(typeof(data)==="string" && (data !== "success" && data !== "failure")){
			location.href='./indportal';
			return;
		}
		if(data === null){
			$scope.retrievalFail = true;
		} else {
			$scope.appealsDetails = data["appeals"];
		}
	});
	response.error(function(){
		$scope.retrievalFail = true;
	});

};

$scope.retrieveReferrals = function(){
	var config = {
			headers: {  'Content-Type': 'application/json; charset=UTF-8'},
	};
	var response = $http.post('indportal/pendingReferrals', config);
	response.success(function(data, status, headers, config){
		if(typeof(data)==="string"){
			location.href='./account/user/login';
			return;
		}
		if(data === null){
			$scope.retrievalFail = true;
		} else {
			$scope.pendingReferrals = data['referrals'];
		}
	});
	response.error(function(){
		$scope.retrievalFail = true;
	});

};

$scope.getCoverageStartYear = function(planSummary) {
	if (planSummary) {
		if (planSummary.activeDentalPlanDetails) {
			planSummary.activeDentalPlanDetails.map(function (dentalItem) {
				dentalItem.coverageStartYear = moment(dentalItem.coverageStartDate).year();
			});
		}
		if (planSummary.activeHealthPlanDetails) {
			planSummary.activeHealthPlanDetails.map(function (healthItem) {
				healthItem.coverageStartYear = moment(healthItem.coverageStartDate).year();
			});
		}
	}
};

$scope.getIssuerName = function(tabs, planDetails) {
	for(var i = 0; i < planDetails.length; i++) {
		if (planDetails[i].planId) {
			for(var j=0; j < tabs.length; j++) {
                if (tabs[j].enrollments)
                {
                    for (var k = 0; k < tabs[j].enrollments.length; k++) {
                        if (+tabs[j].enrollments[k].plan.planId === +planDetails[i].planId) {
                            planDetails[i].issuerName = tabs[j].enrollments[k].plan.issuerName
                        }
                    }
                }
			}
		}
	}
};

$scope.getAppType = function(appType) {
	if($scope.stateCode === 'NV') {
		var SPECIAL_ENROLLMENT = 'Special Enrollment',
			UPDATED_APPLICATION = 'Updated Application',
			INITIAL_APPLICATION = 'Initial Application';

		return appType === SPECIAL_ENROLLMENT ? UPDATED_APPLICATION : INITIAL_APPLICATION;
	} else {
		return appType;
	}
};

$scope.showPlanSummary = function(caseNumber){
	$scope.loader = true;
	var postData = 'caseNumber=' + caseNumber;
	var config = {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	};
	var response = $http.post('indportal/getplandetails', postData, config);
	response.success(function(data, status, headers, config){
		if(typeof(data)==="string"){
			location.href='./indportal';
			return;
		}
		$scope.loader = false;
		/*healthplan = data["health"];
		dentalplan = data["dental"];*/
		planSummary = data;
		var tabs = localStorage.getItem('tabs');
		tabs = JSON.parse(tabs);
		$scope.getIssuerName(tabs, planSummary.activeHealthPlanDetails);
		$scope.getIssuerName(tabs, planSummary.activeDentalPlanDetails);
		$scope.getCoverageStartYear(planSummary);
		$location.path('/plansummary');
	});
	//need error block
};

//Called after APTC is updated
$scope.refreshPlanSummary = function(caseNumber){
	$scope.loader = true;
	var postData = 'caseNumber=' + caseNumber;
	var config = {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	};
	var response = $http.post('indportal/getplandetails', postData, config);
	response.success(function(data, status, headers, config){
		if(typeof(data)==="string"){
			location.href='./indportal';
			return;
		}
		$scope.loader = false;
		planSummary = data;
		$location.path('/plansummary');
		if(planSummary!=undefined && planSummary!=""){
			var tabs = localStorage.getItem('tabs');
			tabs = JSON.parse(tabs);
			$scope.getIssuerName(tabs, planSummary.activeHealthPlanDetails);
			$scope.getIssuerName(tabs, planSummary.activeDentalPlanDetails);
			$scope.getCoverageStartYear(planSummary);
			$scope.planDetails = planSummary;
		}
		/*healthplan = data["health"];
		dentalplan = data["dental"];
		$location.path('/plansummary');
		if(healthplan!=undefined && healthplan!=""){
			$scope.healthdetails = healthplan;
		}
		if(dentalplan!=undefined && dentalplan!=""){
			$scope.dentaldetails = dentalplan;
			if ($scope.dentaldetails.officeVisit === "$null"){
				$scope.dentaldetails.officeVisit = "N/A";
			}
		}*/
	});
	//need error block

};

function validateCoverageStartDate(coverageStartDate) {
    var currentDate = new Date(SERVER_DATE);
    var coverageStartDateFormatted = new Date(coverageStartDate);
    return coverageStartDateFormatted < currentDate;
}

$scope.initplansummary = function() {

	if(planSummary === "") {
		$location.path('/indportal');
	}

	if (planSummary) {
		$scope.planDetails = planSummary;
		$scope.showhealthplan = false;
		$scope.showdentalplan = false;
		$scope.isAllPlansDisEnroll = false;
        var isAllHealthDisenroll = true;
        var isAllDentalDisenroll = true;
		$scope.multipleHealthPlans = false;
		var isHealthDisenrollCount = 0;
		var isDentalDisenrollCount = 0;

		var activeHealthPlanDetails = $scope.planDetails.activeHealthPlanDetails;
		var activeDentalPlanDetails = $scope.planDetails.activeDentalPlanDetails;
		var tabs = localStorage.getItem('tabs');
		tabs = JSON.parse(tabs);
		if (activeHealthPlanDetails && activeHealthPlanDetails.length > 1) {
			$scope.multipleHealthPlans = true;
		}

        if (activeHealthPlanDetails && activeHealthPlanDetails.length > 0) {
			$scope.getIssuerName(tabs, activeHealthPlanDetails);
            for(var i = 0; i < activeHealthPlanDetails.length; i++) {
                var activeHealthPlan = activeHealthPlanDetails[i];
                if (activeHealthPlan) {
                    $scope.showhealthplan = true;
					activeHealthPlan.validCoverageHealth = validateCoverageStartDate(activeHealthPlan.coverageStartDate);
					isHealthDisenrollCount = activeHealthPlan.validCoverageHealth ? (isHealthDisenrollCount+1) : isHealthDisenrollCount;
                    if (!activeHealthPlan.disEnroll) {
                        isAllHealthDisenroll = false;
                    }
                }
            }
        }

        if (activeDentalPlanDetails && activeDentalPlanDetails.length > 0) {
			$scope.getIssuerName(tabs, activeDentalPlanDetails);
            for(var i = 0; i< activeDentalPlanDetails.length; i++) {
                var activeDentalPlan = activeDentalPlanDetails[i];
                if (activeDentalPlan) {
                    $scope.showdentalplan = true;
					activeDentalPlan.validCoverageDental = validateCoverageStartDate(activeDentalPlan.coverageStartDate);
					isDentalDisenrollCount = activeDentalPlan.validCoverageDental ? (isDentalDisenrollCount+1) : isDentalDisenrollCount;
                    if (!activeDentalPlan.disEnroll) {
                        isAllDentalDisenroll = false;
                    }
                }
            }
        }

        $scope.isAllPlansDisEnroll = $scope.showhealthplan && $scope.showdentalplan && (isHealthDisenrollCount > 0 || isDentalDisenrollCount > 0);
	}
};

$scope.showOverrideComments = function(){
	if(oComments==""){
		$location.path('/applications');
	}
	$scope.overrideComments = oComments;
};
$scope.getComments = function(caseNumber){
	var postData = 'caseNumber=' + caseNumber;
	var config = {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	};
	var response = $http.post('indportal/getOverrideComments', postData, config);
	response.success(function(data, status, headers, config){
		if(typeof(data)==="string"){
			location.href='./indportal';
			return;
		}
		$scope.modalForm.oComments = data["comments"];
		$scope.modalForm.ORHist = true;
	});

	response.error(function(data, status, headers, config){
		$scope.modalForm.subResult = 'failure';
	});


};

$scope.showApplication = function(caseNumber){
    var ssapFlowVersion = $("#ssapFlowVersion").val();

    if (ssapFlowVersion && ssapFlowVersion === "2.0") {
        $window.location = '/hix/newssap/start?caseNumber=' + caseNumber;
    } else {
        $window.location = 'ssap?caseNumber=' + caseNumber;
    }
};

$scope.showApplicationInViewMode = function(caseNumber){
	var ssapFlowVersion = $("#ssapFlowVersion").val();
	if(ssapFlowVersion && ssapFlowVersion === "2.0") {
		$window.location = '/hix/newssap/start?caseNumber=' + caseNumber + '&mode=view';
	} else {
		$window.location= '/hix/cap/gotossap?caseNumber=' + caseNumber;
	}
};


$scope.startApplication = function(caseNumber){
	$scope.showDialog = false;
	$(".modal-backdrop").remove();
	$location.path('/qepeligible');
};

$scope.startApplication2 = function(caseNumber){
	$scope.showDialog = false;
	$(".modal-backdrop").remove();
	$location.path('/hix/indportal#');
};

$scope.editApplication = function(caseNumber){
	$window.location= 'indportal/editapplication?caseNumber='+caseNumber;
};

$scope.enrollApplication= function(caseNumber){
	var postData = 'caseNumber=' + caseNumber;
	var config = {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	};
	var response = $http.post('indportal/enroll', postData, config);
	response.success(function(data, status, headers, config) {
		if(typeof(data)!=="string" && data !== "" && data.status != null){
			$scope.loader = false;
			$scope.openInitFail = true;
		}else{
			location.href='./private/setHousehold/'+data;
		}

	});
	response.error(function() {
		$scope.openInitFail = true;
	});
};

$scope.cancelApplication= function(caseNumber){
	$scope.loader = true;
	var postData = 'caseNumber=' + caseNumber;
	var config = {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	};
	var response = $http.post('indportal/cancel', postData, config);
	response.success(function(data, status, headers, config) {
		$scope.loader = false;
		$scope.modalForm.openDialog = false;
		if(data==="success"){
			$scope.refresh();
		}
		else{
			//$scope.currentApplications = data["currentApplications"];
			//$scope.pastApplications = data["pastApplications"];
			$scope.openInitFail = true;
		}
	});
	response.error(function() {
		$scope.loader = false;
		$scope.modalForm.openDialog = false;
		$scope.openInitFail = true;
	});
};

//Cancelling an enrolled application
$scope.cancelEnrolledApplication= function(caseNumber,covYear){
	$scope.loader = true;

	//1. Check for pending application
	var url = 'indportal/checkForPendingApplication';

	var params = {
				"coverageYear":covYear
	};
	var config = {
		headers: { 'Content-Type': 'application/json; charset=UTF-8'},
		params:params
	};

	var response = $http.post(url, null, config);

	response.then(function(responseData, status, headers, config){
		if(responseData.data=="true"){
			$scope.loader = false;
			$scope.pendingAppDialog = true;
		}else{
			//2. Check for active enrollment
			url = 'indportal/checkEnrollmentsForCancellation';

			params = {
						"caseNumber":caseNumber
			};
			config = {
				headers: { 'Content-Type': 'application/json; charset=UTF-8'},
				params:params
			};

			response = $http.post(url, null, config);

			response.then(function(responseData, status, headers, config){
				if(responseData.data=="true"){
					$scope.loader = false;
					$scope.activeEnrollmentDialog = true;
				}else{
					$scope.loader = false;
					$scope.caseNumber = caseNumber;
					$scope.modalForm.openDialog = true;
				}
			});

		}
	}, function(responseData, status, headers, config){
		$scope.loader = false;
		$scope.caseNumber = caseNumber;
		$scope.modalForm.openDialog = true;
	});
};

//HIX-75520
$scope.showEventSummary = function(caseNumber){

	$scope.loader = true;

	var url = 'iex/lce/geteventnames/'+caseNumber;

	config = {
		headers: { 'Content-Type': 'application/json; charset=UTF-8'},
	};

	response = $http.get(url, null, config);

	$scope.applicantEvents = [];

	response.success(function(response, status, headers, config){
		if(response && response.length > 0){
			angular.forEach(response, function(val, key){
				$scope.applicantEvents.push(val);
			});
		}
		$scope.loader = false;
		$scope.eventSummaryDialog = true;
	});

	response.error(function (){
		$scope.loader = false;
		$scope.openInitFail = true;
	});

};

$scope.payForHealth= function(caseNumber,planType,enrollmentId){
	var Data ={ 'caseNumber':caseNumber,'typeOfPlan': planType,'enrollmentId': enrollmentId};
	var config = {
		headers: {   'Content-Type': 'application/json; charset=UTF-8'}
	};

	$scope.loader = true;
	var response = $http.post('/hix/indportal/payforhealth', Data, config);
	response.success(function(data, status, headers, config) {
		if(data != null){
			$scope.payForHealthUrl(data);
		} else {
			$scope.loader = false;
			$scope.payFHFail = true;
		}
	});
	response.error(function() {
		$scope.loader = false;
		$scope.payFHFail = true;
	});
};

$scope.payForHealthUrl =function(data){
	$('#finEnrlDataKey').attr('value', data);
	$scope.loader = false;
	$scope.paySuccess = true;
	/*var params = {
			"csrftoken": $('#tokid').val(),
			"finEnrlDataKey":data
	};
	var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
			params:params

	};*/
	/*var response = $http.post('finance/paymentRedirect?',data, config);
	response.success(function(data, status, headers, config) {
		if(data != null && data.STATUS == 'SUCCESS'){
			var actionUrl = data.ffmEndpointURL;
			$('#postform').attr('action',actionUrl);
			$('#SAMLResponse').attr('value', data.SAMLResponse);
			$scope.loader = false;
			$scope.paySuccess = true;
		} else {
			$scope.loader = false;
			$scope.payFHFail = true;
		}
	});
	response.error(function() {
		$scope.loader = false;
		$scope.payFHFail = true;
	});*/
};

$scope.payHealthExternal= function($event){
	$event.preventDefault();
	document.postform.submit();
	$scope.paySuccess = false;
	$scope.loader = false;
};


$scope.financialHelpPath = function(){
	window.open('https://idalink.idaho.gov/');
};

$scope.startYourAppEvent = function() {
	if ($scope.stateCode === 'MN') {
		$window.open('https://www.mnsure.org/new-customers/apply/index.jsp', '_blank');
	} if ($scope.stateCode === 'ID'){
		$scope.showDialog = true;
	} else {
        $scope.goToNewSSAP("QEP");
	}
};

$scope.goToNewSSAP = function(applicationType) {
    var ssapFlowVersion = $("#ssapFlowVersion").val();
    var coverageYear = $("#coverageYear").val();
    if (ssapFlowVersion && ssapFlowVersion === "2.0") {
        $window.location = '/hix/newssap/start?applicationType=' + applicationType + '&coverageYear=' + coverageYear;
    } else {
        applicationType === "OE" ? $scope.goToSsapOE() : $scope.goToSsapQEP();
    }
};

$scope.cloneEditApplication = function(caseNumber) {
    $scope.loader = true;

    var url = '/hix/indportal/cloneApplicationByCaseNumber/' + caseNumber;
    var config = {
        headers: { 'Content-Type': 'application/json; charset=UTF-8'},
    };

    var response = $http.post(url, null, config);

    response.success(function(data, status, headers, config) {
        if (typeof(data) === "string" && data !== "") {
            $scope.loader = false;
            $scope.showApplication(data);
        } else {
            window.location = '/hix/indportal#/error';
        }
    });

    response.error(function() {
        $scope.loader = false;
        window.location = '/hix/indportal#/error';
    });
};

$scope.goToSsapQEP = function(){
    $window.location= 'indportal/startQepApp?coverageYear='+$('#coverageYear').val();
};

$scope.goToSsapOE = function(){
    //location.href='./ssap'; // TODO: If this is used, most likely will need to be doing as goToSsap method above.
    $window.location= 'indportal/startOEApp?coverageYear='+$('#coverageYear').val();
};

$scope.getIndividualPortalPage = function(coverageYear){
	$window.location= 'indportal?coverageYear='+coverageYear;
};

$scope.goQephome = function(){
	$location.path('/qephome');
};

//
	$scope.parseDate = function (str){
		var ans = str;
		if (str && str.length === 8){
			ans = str.substr(0,2) + "/" + str.substr(2,2) + "/" + str.substr(4, 4);
		}
		return ans;
	};

	///////////////////////////////////////
	///CSR related functions & variables///
	///////////////////////////////////////
	$scope.getSubsq = function(event){
		$scope.loader = true;
		var url = 'indportal/' + $scope.currentCsr.route;
		var data;
		var config;
		if ($scope.currentCsr.title === 'Cancel or Terminate Plan'){
			$scope.termDateErr = null;
			var termDate = $scope.modalForm.cancelDate.substring(4) + "-" + $scope.modalForm.cancelDate.substring(0, 2) + "-" + $scope.modalForm.cancelDate.substring(2, 4);
			data = {'caseNumber': $scope.csrModalCaseNum,'coverageStartDate':$scope.coverageStartDate, 'terminationDate': termDate, 'reasonCode':'OTHER'};
			config = {
				headers: { 'Content-Type': 'application/json; charset=UTF-8'}
			};

		} else if ($scope.currentCsr.title === 'Special Enrollment' || $scope.currentCsr.title === 'Change Coverage Start Date'){
			$scope.SEPData.sepStartDate = $scope.parseDate($scope.SEPData.sepStartDate);
			$scope.SEPData.sepEndDate = $scope.parseDate($scope.SEPData.sepEndDate);
			$scope.SEPData.covStartDate = $scope.parseDate($scope.SEPData.covStartDate);
			if ($scope.currentCsr.title === 'Special Enrollment'){
				$scope.SEPData.editSep = true;
				$scope.SEPStartErr = null;
				$scope.SEPEndtErr = null;
			} else {
				$scope.SEPData.editSep = false;
				$scope.SEPCovErr = null;
			}

			data = $scope.SEPData;

			config = {
				headers: { 'Content-Type': 'application/json; charset=UTF-8'}
			};

		} else if($scope.currentCsr.title === 'Reinstate Enrollment') {
			$scope.ReinstateenrollmentData.caseNumber = $scope.csrModalCaseNum;
			delete $scope.ReinstateenrollmentData.showReinstateEnrollmentOptions;
			data = $scope.ReinstateenrollmentData;
			config = {
					headers: { 'Content-Type': 'application/json; charset=UTF-8'}
				};
		} else if($scope.currentCsr.title === 'Override SEP Denial') {
			data = {"caseNumber":$scope.csrModalCaseNum,"enrollmentEndDate":$scope.parseDate($scope.modalForm.newSEPEndDate)};
			config = {
					headers: { 'Content-Type': 'application/json; charset=UTF-8'}
				};
		} else {
			data = 'caseNumber=' + $scope.csrModalCaseNum;
			config = {
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			};
		}
		var response = $http.post(url, data, config);
		response.success(function(responseData, status, headers, config){
			if(typeof(responseData)==="string" && (responseData !== "success" && responseData !== "failure")){
				$scope.loader = false;
				location.href='./indportal';
				return;
			}
			if(responseData=="success"){
				$scope.loader = false;
				$scope.modalForm.subResult = 'success';
			}else{
				$scope.loader = false;
				$scope.modalForm.subResult = 'failure';
			}
		})
		.error(function(responseData, status, headers, config){
			$scope.loader = false;
			$scope.modalForm.subResult = 'failure';
		});
	};

	$scope.csrContainer = {
		edit:{title: 'Edit Application', route: '', func: $scope.editApplication, header: csrStorage.edit.header, content: csrStorage.edit.content},
		initiate:{title: 'Initiate Verifications', route: 'initVerifications', func: $scope.initVerifications, header: csrStorage.initiate.header, content: csrStorage.initiate.content},
		rerun:{title: 'Re-run Eligibility', route: 'rerunEligibility', func: $scope.rerunEligibility,header: csrStorage.rerun.header, content: csrStorage.rerun.content},
		cancelTerm:{title: 'Cancel or Terminate Plan', route: 'terminatePlan', func: $scope.terminatePlan, header: csrStorage.cancelTerm.header, content: csrStorage.cancelTerm.content},
		update:{title: 'Update Carrier', route: 'updateCarrier', func: $scope.updateCarrier, header: csrStorage.update.header, content: csrStorage.update.content},
		view:{title: 'View Override History', route: 'getOverideHistory', func: $scope.getOverideHistory, header: csrStorage.view.header, content: csrStorage.view.content},
		specialEnroll:{title: 'Special Enrollment', route: 'saveSepDetails', func: $scope.getOverideHistory, header: csrStorage.specialEnroll.header, content: csrStorage.specialEnroll.content},
		coverageDate:{title: 'Change Coverage Start Date', route: 'saveSepDetails', func: $scope.getOverideHistory, header: csrStorage.coverageDate.header, content: csrStorage.coverageDate.content},
		reinstate:{title: 'Reinstate Enrollment', route: 'reinstateenrollment', func: $scope.reinstateEnrollment, header: csrStorage.reinstate.header, content: csrStorage.reinstate.content},
		overrideSEPDenial:{title: 'Override SEP Denial', route: 'seps/reversedenial/', func: $scope.getOverideHistory, header: csrStorage.overrideSEPDenial.header, content: csrStorage.overrideSEPDenial.content},
		overrideEnrollment:{title:'Override Enrollment', route: '', func:'',header:"Override Enrollment Status", content:"Please specify the reason for overriding enrollment status for this customer. This will help keep track of updates to the customer's record."}
	};

	$scope.openCSR = function(caseNum, type){
		$scope.modalForm.csr = true;

		$scope.modalForm.overrideComment = "";
		$scope.csrModalCaseNum = caseNum;
		$scope.currentCsr = $scope.csrContainer[type];
		if(type == 'reinstate') {
			$scope.modalForm.overrideEnrollmentDetails = false;
			$scope.csrModalCaseNum = caseNum.caseNumber;
			$scope.reinsEnrollmentId = caseNum.enrollmentId;
			$scope.reinsPlanType = caseNum.planType;
		}

	};

	$scope.openOverrideEnrollStatus = function(caseNum, type, overrideEnrollStatus){
		$scope.modalForm.csr = true;
		$scope.csrModalCaseNum = caseNum;
		$scope.currentCsr = $scope.csrContainer[type];
		if(type === 'overrideEnrollment'){
			$scope.modalForm.overrideEnrollmentDetails = true;
			$scope.overrideEnrollmentData = {
					"enrollmentId":overrideEnrollStatus.enrollmentId,
					"caseNumber":caseNum,
					"familyMembers":overrideEnrollStatus.enrolleeHistoryEntries
			};
		}
	};

	$scope.openOverrideSEPCSR = function(caseNum, type, appCoverageDate){
		$scope.modalForm.csr = true;
		$scope.modalForm.overrideComment = "";
		$scope.csrModalCaseNum = caseNum;
		$scope.currentCsr = $scope.csrContainer[type];
		$scope.appCoverageDate = appCoverageDate;
	};

	$scope.openOverrideSEPDenial = function(caseNum, appCoverageDate){
		$scope.loader = true;
		var url = 'indportal/applications/'+appCoverageDate+'/checkactive';
		var params = {
				'caseNumber': caseNum
			};
			var config = {
				headers: { 'Content-Type': 'application/json; charset=UTF-8'}
			};

		var response = $http.post(url, null, config);
		response.success(function(data, status, headers, config) {
			$scope.loader = false;
			if(data === "success"){
				$scope.activefailuremsg = true;
				$scope.loader = false;
			}
			else{
				$scope.loader = false;
  				$scope.overrideNewSEPDate = true;
			}
		});
		response.error(function() {
			$scope.loader = false;
			$scope.checkactivefailure = true;
		});
	};

	$scope.getFormattedDate = function(eDate, dateFormat){

		//Check eDate is date, if not return it back
		if(!((null != eDate) && !isNaN(eDate) && ("undefined" !== typeof eDate.getDate))){
			return eDate;
		}

		if (typeof(dateFormat)==='undefined' || dateFormat === "") dateFormat = "MM/DD/YYYY"; //default format

		var monthLongNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		var monthShortNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		var _month = eDate.getMonth();
		var _day = eDate.getDate();
		var _year = eDate.getFullYear();
		var formattedDate = "";

		switch (dateFormat) {
		    case "MMMM DD, YYYY": //January 05, 2015
		    	formattedDate = monthLongNames[_month] + " " + ("0" + _day).slice(-2) + ", " + _year ;
		        break;
		    case "MM/DD/YYYY":  //01/05/2015
		    	formattedDate =  ("0" + (parseInt(_month)+1)).slice(-2) + "/" + ("0" + _day).slice(-2) + "/" + _year ;
		        break;
		    case "MMM DD, YYYY": //Jan 05, 2015
		    	formattedDate = monthShortNames[_month] + " " + ("0" + _day).slice(-2) + ", " + _year ;
		        break;
		}  // add cases as the date format require
		return formattedDate;
	};

	$scope.validateSepEndDate = function($event, condition){
		var dateStr = $event.target.value;
		if (dateStr.length === 8){
			dateStr = dateStr.slice(0, 2) + "/" + dateStr.slice(2, 4) + "/" + dateStr.slice(4,8);
        }
        if (validateDate.check(dateStr)){
			var dateArr = dateStr.split("/");
			var eDate = new Date(dateArr[2], dateArr[0] - 1, dateArr[1]);
			var today = new Date($('#maServerDateTime').val());
			if (eDate < today){
				$scope.newSEPEndDateErrMsg = "Past date is not allowed";
				$scope[condition] = true;
			} else {
				dateStr = $scope.parseDate($scope.calculateOverrideSEPEnd($("#sepDenialGracePeriod").val()));
				dateArr = dateStr.split("/");
				var lastDate = new Date(dateArr[2], dateArr[0] - 1, dateArr[1]);
				if (eDate > lastDate){
					$scope.newSEPEndDateErrMsg = "The special enrollment period date cannot be after " + $scope.getFormattedDate(lastDate, 'MMMM DD, YYYY');
					$scope[condition] = true;
				}else{
					delete $scope.newSEPEndDateErrMsg;
					$scope[condition] = false;
				}
			}
		} else {
			$scope.newSEPEndDateErrMsg = "Please enter valid date";
			$scope[condition] = true;
		}
	};

	$scope.calculateOverrideSEPEnd = function(GracePeriod){
		if (typeof(GracePeriod)==='undefined') GracePeriod = 0;
		var endDate = new Date($('#maServerDateTime').val());
		var timeSpan = 60+parseInt(GracePeriod);
		endDate.setDate(endDate.getDate() + timeSpan);
		function modifySingleDigit (date){
			if (date < 10){
				date = "0" + date;
			} else {
				date = "" + date;
            }
            return date;
		}

		var mm = modifySingleDigit(endDate.getMonth() + 1);
		var dd = modifySingleDigit(endDate.getDate());

		var result = ""+ mm + "/" + dd + "/" + endDate.getFullYear();
		return result;
	};

	$scope.modalForm.newSEPEndDate = $scope.parseDate($scope.calculateOverrideSEPEnd($("#sepDenialGracePeriod").val()));

	//The max # of characters allowed in the override reason textarea
	$scope.maxLength = 4000;

	$scope.cancelCsrAction = function(){
		$scope.SPEopened = false;

		if($scope.ReinstateenrollmentData !== undefined){
			$scope.ReinstateenrollmentData.showReinstateEnrollmentOptions = false;
		}

		$scope.csrInputTermDate = false;
		$scope.csrInputOverride = false;
		$scope.termDate = null;
		$scope.changeCovStart = false;
		$scope.SEPStartErr = null;
		$scope.SEPEndErr = null;
		$scope.SEPCovErr = null;
		$scope.termDateErr = null;

		$scope.modalForm.overrideComment = "";
		$scope.modalForm.subResult = null;
		$scope.modalForm.csr = false;
		$scope.modalForm.cancelDate = null;

		$scope.modalForm.sepStartDate = null;
		$scope.modalForm.sepEndDate = null;
		$scope.modalForm.coverageStartDate = null;
		$scope.activefailuremsg = false;
		$scope.overrideNewSEPDate = false;
		$scope.newSEPEndDateErr = false;
		$scope.modalForm.newSEPEndDate = $scope.parseDate($scope.calculateOverrideSEPEnd($("#sepDenialGracePeriod").val())); // reset date

		$scope.modalForm.overrideEnrollmentDetails = true;
		$scope.modalForm.overrideEnrollmentSuccessful = false;

		if($scope.overrideEnrollmentData){
			angular.forEach($scope.overrideEnrollmentData.familyMembers, function(value, key){
				value.issuerAssignedMemberId = '';
			});
		}

		$scope.newCoverageEndDate = '';

	};

	$scope.submitOverrideComment = function(info){
		$scope.loader = true;

		var postData = {'caseNumber': $scope.csrModalCaseNum, 'overrideComment': info};
		var config = {
			headers: {  'Content-Type': 'application/json; charset=UTF-8' }
		};
		var url = 'indportal/addOverrideComment';

		var response = $http.post(url, postData, config);

		response.success(function(aresponse){

			if(typeof(aresponse)==="string" && (aresponse !== "success" && aresponse !== "failure")){
				$scope.loader = false;
				location.href='./indportal';
				return;
            }
            if(aresponse=="success"){
	  			if ($scope.currentCsr.title === "Cancel or Terminate Plan"){
	  				$scope.loader = false;
	  				$scope.csrInputTermDate = true;
	  			} else if ($scope.currentCsr.title === 'Edit Application'){
	  				$scope.editApplication($scope.csrModalCaseNum);
	  			} else if ($scope.currentCsr.title === 'Special Enrollment' || $scope.currentCsr.title === 'Change Coverage Start Date'){
	  				$scope.getDTO();
	  			} else if ($scope.currentCsr.title === "Reinstate Enrollment") {
	  				$scope.getApplicationEnrollmentDetails();
	  			} else if ($scope.currentCsr.title === "Override SEP Denial"){
	  				$scope.openOverrideSEPDenial($scope.csrModalCaseNum, $scope.appCoverageDate);
	  				//$scope.loader = false;
	  				//$scope.overrideNewSEPDate = true;
	  			}else {
	  				$scope.getSubsq();
	  			}
	  		}else{
	  			$scope.loader = false;
	  			$scope.modalForm.subResult = 'failure';
	  		}

		}).error(function(adata){
			$scope.loader = false;
			$scope.modalForm.subResult = 'failure';

		});
	};


	$scope.getOverrideEligibilityStartDate = function(application){
		$scope.loader = true;
		$scope.overrideEligibilityStartDatecaseNumber = application.caseNumber;
		var url = 'indportal/getEligibilityDateDetails?caseNumber=' + $scope.overrideEligibilityStartDatecaseNumber+'&coverageYear='+application.coverageYear;

		var promise = $http.get(url);

		promise.success(function(response){
			$scope.loader = false;

			if(response) {
	  			$scope.overrideEligibilityStartDateData = response;
	  			$('#overrideStartDateModal').modal();

	  		} else {
	  			$('#errorModal').modal();
	  		}

		});

		promise.error(function(response){
			$scope.loader = false;
			$('#errorModal').modal();
		})
	};

	$scope.saveOverrideEligibilityDate = function(){
		$scope.loader = true;
		var url = 'indportal/saveOverrideEligibilityDate?caseNumber=' + $scope.overrideEligibilityStartDatecaseNumber;

//		var enrollmentStartDate = moment($scope.overrideEligibilityStartDateData.enrollmentStartDate, 'MM/DD/YYYY');
//		var financialEffectiveDate = moment($scope.overrideEligibilityStartDateData.financialEffectiveDate, 'MM/DD/YYYY');
//
//		if(enrollmentStartDate.isAfter(financialEffectiveDate)){
//			$scope.overrideEligibilityStartDateData.financialEffectiveDate = $scope.overrideEligibilityStartDateData.enrollmentStartDate
//		}


		var promise = $http.post(url, $scope.overrideEligibilityStartDateData);

		promise.success(function(response){
			$scope.loader = false;
			if(response === 'success') {
				$scope.successModalTitle = "Override APTC Eligibility Start Date";
				$('#successModal').modal();

	  		} else {
	  			$('#errorModal').modal();
	  		}

		});

		promise.error(function(response){
			$scope.loader = false;
			$('#errorModal').modal();
		})
	};


	$scope.getApplicationEnrollmentDetails = function() {
		var postData = 'caseNumber=' + $scope.csrModalCaseNum;
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		};
		var url = 'indportal/getApplicationEnrollmentDetails';

		$http.post(url, postData, config).success(function(resp) {
			$scope.loader = false;

	  		if(resp != null) {
	  			$scope.ReinstateenrollmentData = {};
	  			$scope.ReinstateenrollmentData.showReinstateEnrollmentOptions = true;

	  			if(resp['Health'] != null) {
	  				$scope.ReinstateenrollmentData.healthEnrollmentId = resp['Health'];
	  			}
	  			if(resp['Dental'] != null) {
	  				$scope.ReinstateenrollmentData.dentalEnrollmentId = resp['Dental'];
	  			}
	  			if(resp['Dental'] == null && resp['Health'] == null){
	  				$scope.ReinstateenrollmentData.noenrollmentsPresent = true;
	  			}
	  		} else {
	  			$scope.modalForm.subResult = 'failure';
	  		}
		}).error(function() {
			$scope.loader = false;
			$scope.modalForm.subResult = 'failure';
		});
	};

	$scope.triggerReinstateEnrollment = function() {

	};

	$scope.getDTO = function(){
		var postData = 'caseNumber=' + $scope.csrModalCaseNum;
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		};
		var url = 'indportal/getSepDetails';

		$http.post(url, postData, config).success(function(resp){
			$scope.loader = false;
			if(typeof(resp)==="string" && (resp !== "success" && resp !== "failure")){
				location.href='./indportal';
				return;
            }
            if(resp!=null){
	  			if ($scope.currentCsr.title === 'Special Enrollment'){
	  				$scope.SPEopened = true;
	  			} else if ($scope.currentCsr.title === 'Change Coverage Start Date'){
	  				$scope.changeCovStart = true;
	  			}
  				$scope.SEPData = resp;
	  		}else{
	  			$scope.modalForm.subResult = 'failure';
	  		}
		}).error(function(){
			  $scope.modalForm.subResult = 'failure';
		});
	};

	$scope.initOtr = function(enableOtr, applicationStatus){

		$scope.enableOtr = enableOtr;
		$scope.applicationStatus = applicationStatus;

		if($scope.enableOtr === true && $scope.applicationStatus === 'Enrolled (Or Active)'){
			var renewed = $('#renewed').val() === 'true';

			$scope.isOtrChecked = false;

			$scope.otr = {
				renewed : renewed
			};
		}
	};


	$scope.checkOtr = function(){
		$scope.loader = true;

		var url = 'indportal/otr/' + $('#coverageYear').val();
		var promise = $http.get(url);

		promise.success(function(response){
			$scope.loader = false;

			$scope.otr = response;

			if($scope.otr.showOTR === true){
				$scope.showBootstrapModal('otrOverrideCommentModal');
			}

		});

		promise.error(function(response){
			$scope.loader = false;
			$scope.showBootstrapModal('otrFailModal');
		});
	};


	$scope.saveOtrOverrideComment = function(){
		$scope.loader = true;

		var url = 'indportal/addOverrideComment';

		var data = {
			caseNumber: $scope.otr.caseNumber,
			overrideComment: $scope.overrideComment
		};

		var promise = $http.post(url, data);

		promise.success(function(response){
			$scope.isOtrChecked = true;

			if(response.toUpperCase() === 'SUCCESS'){
				saveOtr();
			}else{
				$scope.loader = false;

				$scope.showBootstrapModal('otrFailModal');
			}

		});

		promise.error(function(response){
			$scope.loader = false;
			$scope.showBootstrapModal('otrFailModal');
		});
	};

	$scope.resetOtr = function(){
		$scope.overrideComment = '';
		$scope.saveOtrResult = null;
	};


	function saveOtr(){
		var url = 'indportal/otr';

		var data = {
			caseNumber: $scope.otr.caseNumber,
			coverageYear: $scope.otr.coverageYear
		};

		var promise = $http.post(url, data);

		promise.success(function(response){
			$scope.loader = false;

			if(response.toUpperCase() === 'SUCCESS'){
				$scope.otr = {
					renewed : true
				}
			}else{
				$scope.showBootstrapModal('otrFailModal');
			}
		});

		promise.error(function(response){
			$scope.loader = false;
			$scope.showBootstrapModal('otrFailModal');
		});

}
    $scope.showBootstrapModal = function(modalId){
		$('#' + modalId).modal({
			backdrop: 'static',
			keyboard: false
		});
	};

	$scope.reinstateEnrollment = function(comment, caseNumber,enrollmentId, planType){
		$scope.loader = true;
		var params = {
				"enrollmentId" : $scope.reinsEnrollmentId,
				"caseNumber" : $scope.csrModalCaseNum,
				"planType" : $scope.reinsPlanType
			};
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
			params:params
		};
		var response = $http.post('indportal/reinstate', null, config);
		response.success(function(data, status, headers, config){
			$scope.loader = false;
			if(typeof(data)==="string" && data==="failure"){
				location.href='./indportal';
				return;
			}
			//$scope.modalForm.subResult = true;
			$scope.refresh();
		});

		response.error(function(data, status, headers, config){
			$scope.modalForm.subResult = 'failure';
		});
	};

	$scope.getOverrideHistory = function(caseNumber,enrollmentId){
		$scope.loader = true;
		var postData = 'caseNumber=' + caseNumber;
		var params = {
				"enrollmentId" : enrollmentId
			};
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
			params:params
		};
		var response = $http.post('indportal/getOverrideCommentsOfEnrollmentHistory', postData, config);
		response.success(function(data, status, headers, config){
			$scope.loader = false;
			if(typeof(data)==="string" && data==="failure"){
				location.href='./indportal';
				return;
			}
			$scope.modalForm.csrOverrideHistory = true;
			$scope.modalForm.subResult = true;
	  		$scope.overrideHistoryData = data["comments"];
		});

		response.error(function(data, status, headers, config){
			$scope.modalForm.subResult = 'failure';
		});
	};

	$scope.validateLastPremiumPaidDate = function($event, condition){//
		var dateStr = $event.target.value;
		if (dateStr.length === 8){
			dateStr = dateStr.slice(0, 2) + "/" + dateStr.slice(2, 4) + "/" + dateStr.slice(4,8);
        }
        if (!validateDate.check(dateStr)){
			$scope.LastPremiumPaidDateErrMsg = "Please enter valid date";
			$scope[condition] = true;
		} else {
			delete $scope.LastPremiumPaidDateErrMsg;
			$scope[condition] = false;
		}
	};

	$scope.getOverrideEnrollmentResult = function(overrideCommentText, overrideEnrollmentData){
		$scope.loader = true;
		var postData = overrideEnrollmentData;
		postData['overrideCommentText'] = overrideCommentText;
		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'}
		};
		var url = 'indportal/updateEnrollmentStatus';

		$http.post(url, postData, config).success(function(resp){
			$scope.loader = false;
			if(typeof(resp)==="string" && (resp !== "success" && resp !== "failure")){
				location.href='./indportal';
				return;
            }
            //hide member details modal body
  			$scope.modalForm.overrideEnrollmentDetails = false;

	  		if(resp!=null && resp === "success"){
	  			//show successful modal body
	  			$scope.modalForm.overrideEnrollmentSuccessful = true;
	  		}else{
	  			$scope.modalForm.subResult = 'failure';
	  		}
		}).error(function(){
			  $scope.modalForm.subResult = 'failure';
		});
	};


	$scope.dateCheck = function($event, condition){
		if (validateDate.check($event.target.value)){
			$scope[condition] = false;
		} else {
			$scope[condition] = true;
		}
	};

	$scope.calculateSEPEnd = function(){
		var endDate = new Date();
		var timeSpan = 70;
		endDate.setDate(endDate.getDate() + timeSpan);
		function modifySingleDigit (date){
			if (date < 10){
				date = "0" + date;
			} else {
				date = "" + date;
            }
            return date;
		}

		var mm = modifySingleDigit(endDate.getMonth() + 1);
		var dd = modifySingleDigit(endDate.getDate());

		var result = ""+ mm + "/" + dd + "/" + endDate.getFullYear();
		return result;
	};

	 $scope.sepEndDate = $scope.calculateSEPEnd();

	$scope.eventDetailsErrorCount = 0;
	$scope.validateEDate = function(dateStr, index){
		if (dateStr.length === 8){
			dateStr = dateStr.slice(0, 2) + "/" + dateStr.slice(2, 4) + "/" + dateStr.slice(4,8);
        }
        if (validateDate.check(dateStr)){
			var dateArr = dateStr.split("/");
			var eDate = new Date(dateArr[2], dateArr[0] - 1, dateArr[1]);
			var today = new Date();
			if (eDate > today){
				$scope.eventDetails[index].dateError = true;
				$scope.eventDetails[index].over70Days = false;
				$scope.eventDetails[index].dateErrorMsg || $scope.eventDetailsErrorCount++;
				$scope.eventDetails[index].dateErrorMsg = "Event date(s) cannot be in the future.";
			} else if ((today - eDate)/(1000*60*60*24) > 70 ){
				$scope.eventDetails[index].over70Days = true;
				(($scope.eventDetailsErrorCount > 0 && $scope.eventDetails[index].dateErrorMsg) && $scope.eventDetailsErrorCount--);
			} else {
				$scope.eventDetails[index].dateError = false;
				$scope.eventDetails[index].over70Days = false;
				(($scope.eventDetailsErrorCount > 0 && $scope.eventDetails[index].dateErrorMsg) && $scope.eventDetailsErrorCount--);
				delete $scope.eventDetails[index].dateErrorMsg;
			}
		} else {

			$scope.eventDetails[index].dateError = true;
			$scope.eventDetails[index].over70Days = false;
			$scope.eventDetails[index].dateErrorMsg || $scope.eventDetailsErrorCount++;
			$scope.eventDetails[index].dateErrorMsg = "Please use a valid date format.";
		}
	};

	angular.forEach($scope.eventDetails, function(val, key){
		$scope.validateEDate(val.eventDate, key);
	});

	$scope.validateSepDate = function(dateStr){
		if (dateStr.length === 8){
			dateStr = dateStr.slice(0, 2) + "/" + dateStr.slice(2, 4) + "/" + dateStr.slice(4,8);
        }
        if (validateDate.check(dateStr)){
			var dateArr = dateStr.split("/");
			var eDate = new Date(dateArr[2], dateArr[0] - 1, dateArr[1]);
			var today = new Date();
			if (eDate > today){
				$scope.SepDateErrorMsg = "Future date is not allowed";
			} else {
				delete $scope.SepDateErrorMsg;
			}
		} else {
			$scope.SepDateErrorMsg = "Please enter valid date";
		}
	};
	//APTC slider

	$scope.showAdjustButton = true;

	$scope.changeAptc = function(activePlanDetails){
		$scope.planSummaryDetails = activePlanDetails;
		$scope.typeOfPlan = activePlanDetails.typeOfPlan;
		$scope.loader = true;
		$scope.isAptcGetUpdated = undefined;

		var healthEnrlId;
		var dentalEnrlId;
		if($scope.typeOfPlan === 'Health'){
			healthEnrlId = $scope.planSummaryDetails.enrollmentId;
		} else if($scope.typeOfPlan === 'Dental'){
			dentalEnrlId = $scope.planSummaryDetails.enrollmentId;
		}

		var url = 'indportal/aptcPremiumEffDate';
		var data = {
			'caseNumber': activePlanDetails.caseNumber,
			'healthEnrollmentId': healthEnrlId,
			'dentalEnrollmentId': dentalEnrlId,
			'aptcPremiumEffectiveDate': $scope.sliderEffectiveDate
		};

		var promise = $http.post(url, data);


		promise.success(function(res){
			$scope.loader = false;

			if(typeof(res) === 'object' && res.response !== null && res.response !== 'failure'){
				$scope.showAptcSliderError = false;

				$scope.showItem1 = true;
				$scope.showItem2 = false;
				$scope.showAptcSlider = true;
				$scope.showConfirmButton = false;
				$scope.showAdjustButton = true;

				$scope.sliderEffectiveDate = res.aptcPremiumEffectiveDate;
				$scope.newAptc = res.aptcAmt;
				//getPlanSummary(res.healthEnrollmentId, res.dentalEnrollmentId, $scope.planId);
			}else{
				$scope.showAptcSliderError = true;
			}
		});


		promise.error(function(res){
			$scope.loader = false;

			$scope.showAptcSliderError = true;
		});

	};

	/*getPlanSummary = function(healthEnrollmentId, dentalEnrollmentId, planId){
		if($scope.planDetails !== undefined && $scope.planDetails.activeHealthPlanDetails.length > 0){
			if(healthEnrollmentId !== undefined){
				angular.forEach($scope.planDetails.activeHealthPlanDetails, function(activeHealthPlan){

					if(activeHealthPlan !== undefined && activeHealthPlan.planId === planId){
						$scope.planSummaryDetails = activeHealthPlan;
					}

				})
			}
			if(dentalEnrollmentId !== undefined){
				angular.forEach($scope.planDetails.activeDentalPlanDetails, function(activeDentalPlan){

					if(activeDentalPlan !== undefined && activeDentalPlan.planId === planId){
						$scope.planSummaryDetails = activeDentalPlan;
					}

				})
			}


		}

	};*/

	$scope.showConfirmButton = false;

	$scope.showSlide1 = function() {
		$scope.showItem1 = true;
		$scope.showItem2 = false;
		$scope.showConfirmButton = false;
		$scope.showAdjustButton = true;
	}

	$scope.showSlide2 = function(){
		$scope.showItem1 = false;
		$scope.showItem2 = true;
		$scope.showConfirmButton = true;
		$scope.showAdjustButton = false;

		$scope.newAptc = $scope.planSummaryDetails.electedAptc;

		$scope.formatAptcCredit();
		$scope.electedAptc = $scope.newAptc;
		$scope.getSliderWidth();
		$scope.aptcOverMax = false;
	};

	$scope.saveAptc = function(){
		if($scope.aptcOverMax){
			return;
        }
        $scope.loader = true;
		var healthEnrlId;
		var dentalEnrlId;
		if($scope.planSummaryDetails.typeOfPlan === 'Health'){
			healthEnrlId = $scope.planSummaryDetails.enrollmentId;
		} else if($scope.planSummaryDetails.typeOfPlan === 'Dental'){
			dentalEnrlId = $scope.planSummaryDetails.enrollmentId;
		}

		var postData = {'caseNumber': $scope.planSummaryDetails.caseNumber,
				'healthEnrollmentId': healthEnrlId,
				'dentalEnrollmentId': dentalEnrlId,
				'aptcAmt':$scope.newAptc,
				'aptcPremiumEffectiveDate': $scope.sliderEffectiveDate};

		/*if ($scope.dentaldetails){
			postData['dentalEnrollmentId'] = $scope.dentaldetails.enrollmentId;
		}*/

		var config = {
			headers: {  'Content-Type': 'application/json; charset=UTF-8' }
		};
		var url = 'indportal/updateaptc';

		var response = $http.post(url, postData, config);

		response.success(function(responseData, status, headers, config){
			if(typeof(responseData)==="string" && (responseData !== "success" && responseData !== "failure")){
				$scope.loader = false;
				location.href='./indportal';
				return;
			}
			if(responseData=="success"){
				$scope.showAptcSlider = false;
				$scope.loader = false;
				$scope.refreshPlanSummary ($scope.planSummaryDetails.caseNumber);
			}else{
				$scope.showAptcSlider = false;
				$scope.loader = false;
				$scope.payFHFail = true;
			}
		})
		.error(function(responseData, status, headers, config){
			$scope.showAptcSlider = false;
			$scope.loader = false;
			$scope.payFHFail = true;
		});
	};

	$scope.getSliderWidth = function (){
		$scope.isAptcGetUpdated === undefined? $scope.isAptcGetUpdated = false : $scope.isAptcGetUpdated = true;
		if (parseFloat($scope.newAptc) <= parseFloat($scope.planSummaryDetails.maxAptc)){
			$scope.width = ($scope.newAptc /$scope.planSummaryDetails.maxAptc)*96.5 +"%";
		} else {
			$scope.width = "96.5%";
		}

	};

	$scope.formatAptcCredit = function(){
		var decimal = $scope.newAptc.indexOf(".");
		var strLen = $scope.newAptc.length;
		if (decimal === 0) {
			$scope.newAptc = "0" + $scope.newAptc;
        }
        if (decimal > -1){
			if (decimal === strLen - 1){
				$scope.newAptc = $scope.newAptc + "00";
			} else if (decimal === strLen - 2){
				$scope.newAptc = $scope.newAptc + "0";
			} else if (decimal < strLen - 3){
				$scope.newAptc = $scope.newAptc.substring(0, decimal + 3);
			}
		} else {
			$scope.newAptc = $scope.newAptc + ".00";
        }
        $scope.newAptc = parseFloat($scope.newAptc);
	};

	$scope.changeSelectedAptc = function(option){
		if (option === "max"){
			//$scope.newAptc = $scope.healthdetails.maxAptc;
			$scope.newAptc = $scope.planSummaryDetails.maxAptc;
		} else {
			$scope.newAptc = $scope.electedAptc;
        }
        $scope.formatAptcCredit();
		$scope.getSliderWidth();
		$scope.aptcOverMax = false;
	};

	//enrollments history
	$scope.getEnrollmentsHistory = function(){

		var ehCurrentCoverageYear = parseInt($('#ehCurrentCoverageYear').val());
		var exchangeStartYear = parseInt($('#exchangeStartYear').val());
		var minStartYear = Math.min(ehCurrentCoverageYear, exchangeStartYear);
		$scope.ehCoverageYearArray = [];

		for(var i = ehCurrentCoverageYear; i >= minStartYear; i--  ){
			$scope.ehCoverageYearArray.push(i);
		}

		if($scope.activeEhCoverageYear === undefined){
			$scope.activeEhCoverageYear = ehCurrentCoverageYear;
		}



		$scope.loader = true;
		var response = $http.get('indportal/enrollmenthistory?coverageYear=' + $scope.activeEhCoverageYear+'&tmstp='+new Date().getTime());
		response.success(function(data, status, headers, config) {

			if(typeof(data)==="string" && (data !== "success" && data !== "failure")){
				location.href='./indportal';
				return;
			}
			 var enrollCaseNumber = localStorage.getItem('enrollCaseNumber');
			 if (!enrollCaseNumber  || enrollCaseNumber === "undefined") {
				 enrollCaseNumber = $('#enrollCaseNumber').val();
			 }
			 
			$scope.loader = false;
			$scope.myEnrollments = data; /*activeHealthEnrollments, data.activeDentalEnrollments, data.inactiveHealthEnrollments, data.inactiveDentalEnrollments];*/
			$scope.healthdetails = data.activeHealthEnrollments[data.activeHealthEnrollments.length-1];
			$scope.dentaldetails = data.activeDentalEnrollments[data.activeDentalEnrollments.length-1];
			var tabs = localStorage.getItem('tabs');
			tabs = JSON.parse(tabs);
			if($scope.healthdetails){
				$scope.getIssuerName(tabs, $scope.healthdetails);
				$scope.getIssuerName(tabs, $scope.myEnrollments.activeHealthEnrollments);
				$scope.healthdetails['disEnroll'] = $scope.healthdetails.allowDisenroll;
				if($scope.healthdetails.caseNumber == 'undefined'){
					$scope.healthdetails['caseNumber'] = enrollCaseNumber;
				}	
			}
			if($scope.dentaldetails){
				$scope.getIssuerName(tabs, $scope.dentaldetails);
				$scope.getIssuerName(tabs, $scope.myEnrollments.activeDentalEnrollments);
				$scope.dentaldetails['disEnroll'] = $scope.dentaldetails.allowDisenroll;
				if($scope.dentaldetails.caseNumber == 'undefined'){
					$scope.dentaldetails['caseNumber'] = enrollCaseNumber;
				}
			}

			$scope.healthEnrollments = data.activeHealthEnrollments;
            $scope.dentalEnrollments = data.activeDentalEnrollments;

            for(var i = 0; i < $scope.healthEnrollments.length; i++) {
                $scope.healthEnrollments[i].validCoverageHealth = validateCoverageStartDate($scope.healthEnrollments[i].coverageStartDate);

            }

            for(var j = 0; j < $scope.dentalEnrollments.length; j++) {
                $scope.dentalEnrollments[j].validCoverageDental = validateCoverageStartDate($scope.dentalEnrollments[j].coverageStartDate);
            }

		});
		response.error(function() {
			$scope.loader = false;
			$scope.openInitFail = true;
		});
	};

	$scope.enrollmentCsrAction = function($event){
		$($event.target).closest('.enrollmentCsrActionDiv').find('.csrButtonDiv').slideToggle();
	};


	$scope.getSepEventDetails = function(application){
		var url = '/hix/ssap/applicant/events/' + application.caseNumber + '/all';
		var promise = $http.get(url);

		$scope.loader = true;

		//remove previous error message
		$scope.sameEventIsSelected = false;

		promise.success(function(response){
			if(response.status === '200'){
				$scope.sepDetails = response;

				for(var i=0; i<$scope.sepDetails.applicants.length; i++){
					for(var j=0; j<$scope.sepDetails.applicants[i].events.length; j++){
						$scope.sepDetails.applicants[i].events[j].previousEventId = $scope.sepDetails.applicants[i].events[j].eventId;
						$scope.sepDetails.applicants[i].events[j].previousEventDate = $scope.sepDetails.applicants[i].events[j].eventDate;
						$scope.sepDetails.applicants[i].events[j].previousEventNameLabel = $scope.sepDetails.applicants[i].events[j].eventLabel;

					}
				}


				$scope.sepDetails.caseNumber = application.caseNumber;
				$scope.sepDetails.isFinancial = application.isNonFinancial? false : true;

				//button only show for QLE and SEP application, no OE application
				$scope.sepDetails.applicationType = application.isQep? 'QEP' : 'SEP';

			}else{
				$('#errorModal').modal();
			}

		}).then(getSepEventDetails);

		promise.error(function(response){
			$scope.loader = false;
			$('#errorModal').modal();
		});
	};

	function getSepEventDetails(){
		var promises = [];

		for(var i=0; i<$scope.sepDetails.applicants.length; i++){
			for(var j=0; j<$scope.sepDetails.applicants[i].events.length; j++){
				promises.push(getSepEventDetailsAPICall(i, j));
			}
		}


		$q.all(promises).then(function(){
			$scope.loader = false;
			openOverrideSepModal();
		}, function(){
			$scope.loader = false;
			$('#errorModal').modal();
		});

	}

	function getSepEventDetailsAPICall(aplicantIndex, eventIndex){
		var url = '/hix/indportal/getSEPEventsByApplicationType';
		var defer = $q.defer();

		var data = {
			isFinancial: $scope.sepDetails.isFinancial? 'Y' : 'N',
			displayOnUI: 'Y',
			changeType: $scope.sepDetails.applicants[aplicantIndex].events[eventIndex].changeType
		};

		var promise = $http.post(url, data);

		promise.success(function(response){
			var eventList = [];
			var currentEventId = $scope.sepDetails.applicants[aplicantIndex].events[eventIndex].eventId;
			var isCurrentEventContained = false;

			response.sepEventss.forEach(function(item){
				var eventId = item._links.self.href.split('/').pop();

				eventList.push({
					eventNameLabel: item.label,
					eventId: eventId,
					type: item.type
				});

				if(eventId == currentEventId){
					isCurrentEventContained = true
				}
			});

			$scope.sepDetails.applicants[aplicantIndex].events[eventIndex].eventList = eventList;

			if(!isCurrentEventContained){
				$scope.sepDetails.applicants[aplicantIndex].events[eventIndex].eventList.push({
					eventNameLabel: $scope.sepDetails.applicants[aplicantIndex].events[eventIndex].eventLabel,
					eventId: $scope.sepDetails.applicants[aplicantIndex].events[eventIndex].eventId,
					type: $scope.sepDetails.applicants[aplicantIndex].events[eventIndex].type
				});
			}

			defer.resolve();
		});

		promise.error(function(response){
			defer.reject();
		});

		return defer.promise;
	}


	function openOverrideSepModal(){

		$scope.isSepEventDetailsForm= true;
		$scope.isSepDateDetailsForm = false;
		$scope.sepDetails.overrideCommentText= '';

		//2 calls: 1-get events and dates; 2-get dropdown list
		$('#overrideSepModal').modal({
			keyboard: false,
			backdrop: 'static'
		});
	}

	$scope.getSepDateDetails = function(){
		//check whether there is changed
		$scope.applicantUpdatedSepEvents = getApplicantEvents();

		//remove previous error message
		$scope.sepDateDetailsForm.sepEndDate.$setValidity("startBeforeEnd", true);

		//console.log($scope.applicantUpdatedSepEvents.length);
		if($scope.applicantUpdatedSepEvents.length === 0){

			$scope.sepDateDetailsForm.sepEndDate.$setValidity("isUpdated", false);

			$scope.loader = true;
			var url = '/hix/indportal/getSEPPeriodDates?caseNumber=' + $scope.sepDetails.caseNumber;

			var promise = $http.get(url);

			promise.success(function(response){
				$scope.loader = false;

				if(response !== ''){
					$scope.sepDetails.sepStartDate = response.sepStartDate;
					$scope.sepDetails.sepEndDate = response.sepEndDate;

					updateSepDatesAndPages();
				}else{
					$('#overrideSepModal').modal('hide');
					$('#errorModal').modal();
				}
			});

			promise.error(function(response){
				$scope.loader = false;
				$('#overrideSepModal').modal('hide');
				$('#errorModal').modal();
			});


		}else{
			$scope.sepDateDetailsForm.sepEndDate.$setValidity("isUpdated", true);

			$scope.sepDetails.sepStartDate = getSepDate('MIN');
			$scope.sepDetails.sepEndDate = getSepDate('MAX');

			updateSepDatesAndPages();

		}

		};


	function updateSepDatesAndPages(){
		$scope.sepDetails.previousSepStartDate = $scope.sepDetails.sepStartDate;
		$scope.sepDetails.previousSepEndDate = $scope.sepDetails.sepEndDate;

		$scope.isSepEventDetailsForm = false;
		$scope.isSepDateDetailsForm = true;
	}

	$scope.validateEventAndDate = function(eventForm, event, applicant){
		hasSameEvent(eventForm);
		$scope.eventDateValidation(eventForm, event, applicant);
	};


	function hasSameEvent(eventForm){

		$scope.sameEventIsSelected = false;

		for(var i=0; i<$scope.sepDetails.applicants.length; i++){
			var events = $scope.sepDetails.applicants[i].events.map(function(eventObj){
				return eventObj.eventId;
			});

			$scope.sameEventIsSelected = events.some(function(event, index){
			    return events.indexOf(event) != index
			});

			if($scope.sameEventIsSelected){
				break;
			}

		}

	}




	$scope.eventDateValidation = function(eventForm, event, applicant){
				eventForm.eventDate.$setValidity('withinFuture60Days', true);
				eventForm.eventDate.$setValidity('withinPast60Days', true);
		eventForm.eventDate.$setValidity('sameYear', true);

		if(event.eventDate.substring(event.eventDate.length - 4) != $scope.maCurrentCoverageYear){
			eventForm.eventDate.$setValidity('sameYear', false);
		}else{
			for(var i = 0; i < $scope.sepDetails.applicants.length; i++){

				//get the right person
				if($scope.sepDetails.applicants[i].personId === applicant.personId){
				for(var j = 0; j < $scope.sepDetails.applicants[i].events.length; j++){

				var eventType;
				for(var k = 0; k < $scope.sepDetails.applicants[i].events[j].eventList.length; k++){
					if(event.eventId === $scope.sepDetails.applicants[i].events[j].eventList[k].eventId){
						eventType = $scope.sepDetails.applicants[i].events[j].eventList[k].type;
					}
				}

						if(eventType !== undefined){
				if(eventType === 4){
					//within future 60 days
					eventForm.eventDate.$setValidity('withinFuture60Days', uiDateValidation(event.eventDate, 'withinFuture60Days'));
				}else{
					//within past 60 days
					eventForm.eventDate.$setValidity('withinPast60Days', uiDateValidation(event.eventDate, 'withinPast60Days'));
				}
			}
					}
				}

		}
	}

	};

	function uiDateValidation(eventDate, type){
		if(eventDate !== undefined){

			var date;
			if(eventDate.length === 10){
				date = eventDate;
			}else{
				date = eventDate.substring(0,2) + '/' + eventDate.substring(2,4) + '/' + eventDate.substring(4,8);
			}

			if(type === 'withinFuture60Days'){

				return moment().isSame(date, 'day') || moment().diff(date, 'day') > -59 && moment().diff(date, 'day') < 0;

			}else if(type === 'withinPast60Days'){

				return moment().isSame(date, 'day') || moment().diff(date, 'day') < 60 && moment().diff(date, 'day') > 0;

			}

		}else{
			return true;
		}


	}


	$scope.validateStartBeforeEnd = function(){
		var startDate = $scope.sepDetails.sepStartDate;
		var endDate = $scope.sepDetails.sepEndDate;

		//if(startDate && endDate && moment(startDate,'MM/DD/YYYY').isValid() && moment(endDate,'MM/DD/YYYY').isValid()){
		//if(startDate && endDate && $scope.sepDateDetailsForm.sepStartDate.$valid && $scope.sepDateDetailsForm.sepEndDate.$valid){
		if(startDate && endDate){
			startDate = startDate.indexOf('/') > -1? startDate : startDate.substring(0,2) + '/' + startDate.substring(2,4)+ '/' + startDate.substring(4,8);
			endDate = endDate.indexOf('/') > -1? endDate : endDate.substring(0,2) + '/' + endDate.substring(2,4)+ '/' + endDate.substring(4,8);
			$scope.sepDateDetailsForm.sepEndDate.$setValidity("startBeforeEnd", moment(endDate).isAfter(startDate, 'day') || moment(endDate).isSame(startDate, 'day') );

		}else{
			$scope.sepDateDetailsForm.sepEndDate.$setValidity("startBeforeEnd", true);
		}


		if($scope.applicantUpdatedSepEvents.length === 0 && startDate === $scope.sepDetails.previousSepStartDate && endDate === $scope.sepDetails.previousEndDate){
			$scope.sepDateDetailsForm.sepEndDate.$setValidity("isUpdated", false);
		}else{
			$scope.sepDateDetailsForm.sepEndDate.$setValidity("isUpdated", true);
		}


	};



	function getUiDateFormat(dateToFormat){
		var date = dateToFormat? new Date(dateToFormat) : new Date();

		var dd = date.getDate();
		var mm = date.getMonth()+1;

		var yyyy = date.getFullYear();
		if(mm<10){
		    mm='0'+mm;
		}

		if(dd<10){
		    dd='0'+dd;
		}
		return mm+'/'+dd+'/'+yyyy;

	}


	function getSepDate(type){
		var dates = [];
		for(var i = 0; i < $scope.sepDetails.applicants.length; i++){
			if($scope.sepDetails.applicationType === 'SEP' || ($scope.sepDetails.applicationType === 'QEP' && $scope.sepDetails.applicants[i].personId === 1)){
			for(var j = 0; j < $scope.sepDetails.applicants[i].events.length; j++){
				dates.push(new Date($scope.sepDetails.applicants[i].events[j].eventDate));
			}
		}
		}

		var dateToReturn;
		var oneDay = 24*60*60*1000;
		if(type === 'MAX'){
			var sepGracePeriod = parseInt($('#gracePeriod').val());
			dateToReturn = Math.max.apply(null,dates);

			dateToReturn += oneDay*(60 + sepGracePeriod);
		}else{
			dateToReturn = Math.min.apply(null,dates);
		}
		return getUiDateFormat(dateToReturn);
	}

	$scope.backToSepDetails = function(){
		$scope.isSepEventDetailsForm= true;
		$scope.isSepDateDetailsForm= false;
	};

	$scope.saveSepOverride = function(){
		$scope.loader = true;
		var url = '/hix/indportal/overrideSEPDetails';

		var data = {
			sepStartDate: $scope.sepDetails.sepStartDate,
			sepEndDate: $scope.sepDetails.sepEndDate,
			caseNumber: $scope.sepDetails.caseNumber,
			applicantEvents: $scope.applicantUpdatedSepEvents,
			overrideCommentText: $scope.sepDetails.overrideCommentText,
			coverageYear: $scope.maCurrentCoverageYear,
			applicationType: $scope.sepDetails.applicationType,
			isFinancial: $scope.sepDetails.isFinancial
		};

		var promise = $http.post(url, data);

		promise.success(function(response){
			$scope.loader = false;

			if(response !== 'success'){
				$('#errorModal').modal();
			}else{
				$scope.successModalTitle = "Override Special Enrollment";
				$('#successModal').modal();
			}
		});

		promise.error(function(response){
			$scope.loader = false;

			$('#errorModal').modal();
		});


	};


	function getApplicantEvents(){
		var applicantEvents = [];

		for(var i=0; i< $scope.sepDetails.applicants.length; i++){
			for(var j=0; j< $scope.sepDetails.applicants[i].events.length; j++){

				var eventObj = $scope.sepDetails.applicants[i].events[j];

				if(eventObj.eventId.toString() !== eventObj.previousEventId.toString() || eventObj.eventDate !== eventObj.previousEventDate){

					var eventNameLabel = '';

					eventObj.eventList.forEach(function(item, index){
						if(item.eventId.toString() === eventObj.eventId.toString()){
							eventNameLabel = item.eventNameLabel
						}
					});

					applicantEvents.push({
						firstName: $scope.sepDetails.applicants[i].firstName,
						middleName: $scope.sepDetails.applicants[i].middleName,
						lastName: $scope.sepDetails.applicants[i].lastName,
						eventId: eventObj.eventId,
						eventDate: eventObj.eventDate,
						applicantEventId: eventObj.applicantEventId,
						eventNameLabel: eventNameLabel,
						previousEventId: eventObj.previousEventId,
						previousEventNameLabel: eventObj.previousEventNameLabel
					})
				}
			}
		}

		//if QEP and there is change, that change must be for primary contact
		if($scope.sepDetails.applicationType === 'QEP' && applicantEvents.length !== 0){

			var eventId = applicantEvents[0].eventId;
			var	eventDate = applicantEvents[0].eventDate;
			var	eventNameLabel = applicantEvents[0].eventNameLabel;

			for(var i=0; i< $scope.sepDetails.applicants.length; i++){
				var applicantObj = $scope.sepDetails.applicants[i];
				if(applicantObj.personId !== 1){
					applicantEvents.push({
						firstName: applicantObj.firstName,
						middleName: applicantObj.middleName,
						lastName: applicantObj.lastName,
						applicantEventId: applicantObj.events[0].applicantEventId,
						previousEventId: applicantObj.events[0].eventId,
					    previousEventNameLabel: applicantObj.events[0].eventLabel,
					    eventId: eventId,
						eventDate: eventDate,
						eventNameLabel: eventNameLabel
					})
				}
			}
		}


		return applicantEvents;
	}

}]);
//End indPortalAppCtrl controller

//This controller is for contactUs.jsp and appeal.jsp
indPortalApp.controller('contactUsCtrl', ['$scope', '$http', '$upload', '$q', '$timeout', function($scope, $http, $upload, $q, $timeout){
  $scope.filesSelected = [];
  $scope.filesUploaded = [];
  var counter = 1;

  $scope.formData = {};
  $scope.appealForm = {};


  $scope.clearReason = function (type){
	  if (type != false) {
		  $scope.formData.reason = null;
      }
  };


  $scope.selectAppealFile =  function($files, event){
	  var name = $files[0].name.split(".");
	  if (!$scope.checkFileType($files)){
		  event.target.parentElement.children[2].value ="";
		  $scope.wrongFileTypeDialog = true;
		  return
      }
      $scope.appealfileSelected= $files[0];
  };

  $scope.cancelAppealfile =  function(){
	  $scope.appealfileSelected= null;
	  event.target.parentElement.children[2].value ="";
  };

  $scope.onFileSelect= function($files, index, event){
      index = index || 0;
	  var name = $files[0].name.split(".");
	  if (!$scope.checkFileType($files)){
		  event.target.parentElement.children[1].value = "";
		  $scope.wrongFileTypeDialog = true;
		  return
      }
      $scope.filesSelected[index] = $files[0];
    };

  $scope.addFile = function(){
    $scope.filesSelected.push(counter);
    counter++;
  };

  $scope.cancelFile = function(index){
    var toDel = $scope.filesSelected[index];

    for (var i = 0; i < $scope.filesUploaded.length; i++) {
      if ($scope.filesUploaded[i].name === toDel.name){
        $scope.filesUploaded.splice(i, 1);
      }
    }
      $scope.filesSelected.splice(index, 1);

    if (index == 0){
    	event.target.parentElement.children[1].value = "";
    }
  };

  $scope.uploadFile = function(index){
    var target = $scope.filesSelected[index];
    if ($scope.filesUploaded.length === 0){
    	return $scope.uploadOneFile(index);
    } else {
      for (var i = 0; i < $scope.filesUploaded.length; i++) {
        if ($scope.filesUploaded[i].name === target.name){
        	return $scope.uploadOneFile(index);
        }
      }
    }
   	return $scope.uploadOneFile(index);
    };


  var uploadURL = "/hix/indportal/upload";
  var submitURL = "/hix/indportal/submitcontactus";
  var uploadAppealURL = "/hix/indportal/upload";

  $scope.checkFileType = function(files){
	  var supportedFileType = {
	     "txt": true,
	     "css": true,
	     "xml": true,
	     "csv": true,
	     "png": true,
	     "jpg": true,
	     "jpeg": true,
	     "bmp": true,
	     "gif": true,
	     "doc": true,
	     "docx": true,
	     "dot": true,
	     "dotx": true,
	     "xls": true,
	     "xlsx": true,
	     "ppt": true,
	     "pptx": true,
	     "pdf": true,
	     "zip": true,
	     "tar": true,
	     "gz": true,
	     "js": true
	  };

	  var name = files[0].name.split(".");

	 if (supportedFileType[name[name.length - 1]]){
		 return true;
	 } else {
		 $scope.wrongFileName = files[0].name;
		 $scope.wrongFileType = name[name.length - 1];
		 return false;
	 }
  };

//fileToUpload {file:}
  $scope.uploadOneFile = function(index, type){

	  var file;
	  var url;
	  var deferred = $q.defer();
	if (type === "appeal"){
		file = $scope.appealfileSelected;
		url = uploadAppealURL;
	} else {
		file = $scope.filesSelected[index];
		url = uploadURL;
		$scope.typeUpload = 'contact';
    }
      if (file.size> 10485760){
		$scope.loader=false;
		$scope.oversizedFileD = true;
		deferred.reject();
		return deferred.promise;
	}

	var fileName = 'fileToUpload';
    var fd = new FormData();

    fd.append(fileName, file),
    fd.append("csrftoken",$('#tokid').val());

   $http.post(url,fd, {
	   transformRequest: angular.identity,
	   headers: {'Content-Type':undefined}
    }).success(function(data){
    	if (data == 'failure'){
    		$scope.loader = false;
    		$scope.openDialog = true;
    		deferred.reject();
    	} else {
    		var temp = {};
    		temp.file = data;
    		temp.name = file.name;
    		if ($scope.typeUpload === 'contact'){
        		$scope.filesUploaded.push(temp);
    		} else {
    			$scope.appealFileUploaded = temp;
    		}
   			file.uploaded = true;
        deferred.resolve();
    	}
    }).error(function(){
    	$scope.loader = false;
    	$scope.openDialog = true;

    	deferred.reject();
    });
    return deferred.promise;
};

$scope.openD= function(){
	$scope.openDialog = true;
	return true;
};

//need to add function to check the filesSelected
  $scope.submitForm = function(){
	  $scope.loader = true;
	  var starting = new Date();
	  var sendToServer = function (){

    	$scope.formData.fileList = [];
    	angular.forEach($scope.filesUploaded, function(val, key){
    		$scope.formData.fileList.push(val.file);
    	});

    	$scope.postResult="";
    	var params = {
    			"csrftoken": $('#tokid').val()
    	};

          $http({
	      method: 'POST',
	      url: submitURL,
	      data: JSON.stringify($scope.formData),
	      headers: { 'Content-Type': 'application/json; charset=UTF-8' },
	      params: params

	    }).success(function(data){
	    	$scope.loader = false;
	    	if (data == "success"){
	   	     	$scope.postResult = 'postsuccess';
	    	} else {
	    		$scope.postResult = 'postfailure';
            }
          }).error(function(data){
	    	$scope.postResult = 'postfailure';
	    	$scope.loader = false;
	    });
	  };

    if ($scope.filesSelected.length !== $scope.filesUploaded.length){
      var promiseList = [];
      for (var i = 0; i < $scope.filesSelected.length; i++) {
    	if (typeof $scope.filesSelected[i] === 'object'){
        	var deferred = $q.defer();
        	promiseList.push(deferred.promise);
        	(function(){
        		var data = $scope.uploadFile(i);
        		deferred.resolve(data);
        	}());
    	}
      }
        var promiseAll = $q.all(promiseList);

       promiseAll.then(function(){
    	   sendToServer();
       });

    } else {
    	sendToServer();
    }

  };

  $scope.submitAppeal= function(){
    var appealUrl = "/hix/indportal/submitappeal";

    $scope.loader = true;

    $scope.appealPostResult='';
	  var sendToServer = function (){
		  if ( $scope.appealFileUploaded){
	   		$scope.appealForm.fileId = $scope.appealFileUploaded.file;
          }
          if ($scope.appealForm.appealReason == null){
			  $scope.appealForm.appealReason = "Qhp";
		  }
    	$scope.postResult='';
          $http({
	      method: 'POST',
	      url: appealUrl,
	      data: JSON.stringify($scope.appealForm),
	      headers: { 'Content-Type': 'application/json; charset=UTF-8' }
	    }).success(function(data){
	    	if (data == "success"){
	   	     	$scope.appealPostResult = 'postsuccess';
	    	} else {
	    		$scope.appealPostResult = 'postfailure';
            }
              $scope.loader = false;
	    }).error(function(data){
	    	$scope.appealPostResult = 'postfailure';
	    	$scope.loader = false;
	    	$scope.openDialog='true';
	    });
	  };

	  if ($scope.appealfileSelected) {
		  var promise = $scope.uploadOneFile(0, 'appeal');
		  promise.then(function(){
			  sendToServer();
		   }, function(){
		     $scope.loader = false;
		     $scope.openDialog='true';
		   });
	  } else {
			  sendToServer();
      }
  };
}]);

indPortalApp.factory('storageFty', function(){
	var storage = {};
	return {
		setVal: function(key, value){
			storage.key = value;
		},

		getVal: function(key){
			return storage.key;
		}

	};
});



(function(){
	var validateDate = function () {
		return {
		  check: function(date){
			if (date === ""){
				return false;
			}


			if (/[dmy]/.test(date)){
				return false;
			}
			var dateArr = date.split("/");

			var daysInMo;

    		switch(dateArr[0]){
    		  case "02":
    		    if (dateArr[2] % 4 === 0){
    		      daysInMo = 29;
    		    } else {
    		      daysInMo = 28;
                }
                  break;
    		  case "04" : case "06" : case "09" : case "11":
    		    daysInMo = 30;
    		    break;
    		  default:
    		    daysInMo = 31;
    		}

    		if (dateArr[0] > 12 || dateArr[1] > daysInMo || dateArr[0] < 1 || dateArr[1] < 1){
    			return false;
    		} else {
    			return true;
    		}
		  }
		};
	};

	angular.module('indPortalApp')
	.factory('validateDate', validateDate);
})();

indPortalApp.filter('removeForwardSlashes', function () {
	return function (input) {
	    if(input){
	    	return input.replace(/\//g, "");
	    }
	    return input;
	};
});

indPortalApp.filter('formatDate', function () {
	return function (input) {
	    if(input){
	    	var inputArr = input.split("/");
	    	var formattedDate= new Date(inputArr[2], inputArr[0] - 1, inputArr[1]);
	    	return formattedDate;
	    }
	    return input;
	};
});

indPortalApp.directive('triggerAddFile', function(){
return {
  link: function(scope, element, attrs){
    element.bind('click', function(){
    	var inputBtn = element.parent().find('input');
        inputBtn.click();
      });
    }
  };
});

indPortalApp.directive('ariaTooltip', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			element.tooltip({
				trigger: 'manual'
			}).focus(function(){
				element.tooltip('show');
			}).blur(function(){
				element.tooltip('hide');
			}).hover(function(){
				element.tooltip('show');
			}, function(){
				element.tooltip('hide');
			});
			var tooltipContent = element.attr('data-original-title');
			element.append('<span class="aria-hidden">Tooltip text: ' + tooltipContent + '</span>');
		}
	}
});

indPortalApp.directive('ngFocus', [function() {
	var FOCUS_CLASS = "ng-focused";
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attrs, ctrl) {
			ctrl.$focused = false;
			element.bind('focus', function(evt) {
				element.addClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = true;
				});
			}).bind('blur', function(evt) {
				element.removeClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = false;
				});
			});
		}
	};
}]);

indPortalApp.directive("modalShow", function ($parse) {
return {
    restrict: "A",
    link: function (scope, element, attrs) {

        //Hide or show the modal
        scope.showModal = function (visible, elem) {
            if (!elem)
                elem = element;

            if (visible)
                $(elem).modal("show");
            else
                $(elem).modal("hide");
        };

        //Watch for changes to the modal-visible attribute
        scope.$watch(attrs.modalShow, function (newValue, oldValue) {
          scope.showModal(newValue, attrs.$$element);
        }, true);

        //Update the visible value when the dialog is closed through UI actions (Ok, cancel, etc.)
        $(element).bind("hide.bs.modal", function () {
          $parse(attrs.modalShow).assign(scope, false);
          if (!scope.$$phase && !scope.$root.$$phase)
              scope.$apply();
        });
    }

};
});
//Plan Summary Accordian: End
indPortalApp.directive("btstAccordion", function () {
    return {
        restrict: "E",
        transclude: true,
        replace: true,
        scope: {},
        templateUrl:"accordionTemplate2.html",
        link: function (scope, element, attrs) {

            // give this element a unique id
            var id = element.attr("id");
            if (!id) {
                id = "btst-acc" + scope.$id;
                element.attr("id", id);
            }

            // set data-parent on accordion-toggle elements
            var arr = element.find(".accordion-toggle");
            for (var i = 0; i < arr.length; i++) {
                $(arr[i]).attr("data-parent", "#" + id);
                $(arr[i]).attr("href", "#" + id + "collapse" + i);
            }
            arr = element.find(".accordion-body");
            $(arr[0]).addClass("in"); // expand first pane
            for (var i = 0; i < arr.length; i++) {
                $(arr[i]).attr("id", id + "collapse" + i);
            }
        },
        controller: function () {}
    };
});

indPortalApp.directive('btstPane', function () {
    return {
        require: "^btstAccordion",
        restrict: "E",
        transclude: true,
        replace: true,
        scope: {
            ngtitle: "@",
            category: "=",
            order: "="
        },
        templateUrl:
            "accordionTemplate.html",
        link: function (scope, element, attrs) {
            scope.$watch("title", function () {
                // NOTE: this requires jQuery (jQLite won't do html)
                var hdr = element.find(".accordion-toggle");
                hdr.html(scope.ngtitle);
            });
        }
    };
});
// Plan Summary Accordian: End



//enrollments collapse
indPortalApp.directive("collapseIconUpdate", function () {
    return {
        restrict: "A",
        link : function(scope, element, attrs) {

        	element.bind('click', function(){
        		if(element.hasClass("icon-chevron-sign-down")){
            		element.removeClass("icon-chevron-sign-down").addClass("icon-chevron-sign-up");
            	}else if(element.hasClass("icon-chevron-sign-up")){
            		element.removeClass("icon-chevron-sign-up").addClass("icon-chevron-sign-down");
            	}
        	});
		}
    };
});

//enrollments collapse end

(function(){
	var spinningLoader = function($timeout){
		return {
			restrict: "EA",
			template: '<div id="loaderDiv" ng-show="loadSpinner"><img alt="loading" src="resources/images/loader_gray.gif" class="ajax-loader"/></div>',
		    link: function (scope, elem, attrs, ctrl) {
	          var starting;
	          scope.$watch(attrs.spinningLoader, function(nV, oV){
	            if (nV === true){
	              starting = new Date();
	              scope.loadSpinner = true;
	            } else {
	              var ending = new Date();
	              var timeDiff = ending - starting;
	              if (timeDiff >= 1000){
	                scope.loadSpinner = false;
	              } else {
	                $timeout(function(){
	                  scope.loadSpinner = false;
	                }, (1000 - timeDiff));
                  }
                }
              });
	      }
		};
	};


	angular.module('indPortalApp')
	.directive("spinningLoader", spinningLoader);
})();


(function() {
	var numOnly = function() {
		return {
			priority: "200",
			restrict : "A",
			require : "ngModel",
			link : function(scope, ele, attrs, ngM) {
				ele.bind("keydown", function(event) {
					if(event.which === 110 || event.which === 190){
						if(!event.currentTarget.id === "aptcText" || !(event.currentTarget.value[event.currentTarget.value.length -1] !== ".") || (event.currentTarget.value % 1 != 0) ){
							event.preventDefault();
						} else {
							return;
						}
					}

					if (event.which === 96 || event.which === 48) {
						if (event.target.value === "0"){
							event.preventDefault();
						}
                    }
                    if(event.ctrlKey || event.metaKey) {
						if (event.which === 86 || event.which === 67){
							return;
						}
					}

					if (event.which < 8 || event.which > 57) {
						if (event.which >= 96 && event.which <=105){
							return;
						}
						event.preventDefault();
					} else {
						if (event.shiftKey){
							event.preventDefault();
						}
					}
				});
		  }
		};
    };

	angular.module('indPortalApp')
		.directive("numOnly", numOnly)
})();

(function(){
	var checkMax = function(){
		return {
			restrict : "A",
			require : "ngModel",
			link : function(scope, ele, attrs, ngM) {
				scope.$watch("newAptc", function(nv, ov, scope){
					if(scope.planSummaryDetails){
                        if (parseFloat(nv) > parseFloat(scope.planSummaryDetails.maxAptc)){
                            scope.aptcOverMax = false;
                        }
					}
				})
			}
		}
	};

	angular.module('indPortalApp')
		.directive("checkMax", checkMax)
})();



(function(){
	function sepDateValidation(){
		var directive = {
			restrict : 'AE',
			require : 'ngModel',
			link : function(scope, element, attrs, ngModel) {
				var vaidationTypeArr = attrs.sepDateValidation.split('_');

				function customDateValidation(value){
					//remove placeholder
					var date = value.replace(/[A-Za-z]/g, '');
					var myScope = scope;
					//remove extra-last-character
					if(date.length === 11){
						date = date.substring(0, date.length - 1);
					}

					if(date.length === 10){
						resetValidation();

						var isValid = true;

						for(var i = 0; i < vaidationTypeArr.length; i++){
							if(vaidationTypeArr[i] === 'date' && !moment(date,'MM/DD/YYYY').isValid()){
								isValid = 'date';
								break;
							}else if(vaidationTypeArr[i] === 'future' && moment().diff(date, 'day') > 0){
								isValid = 'future';
								break;
							}else if(vaidationTypeArr[i] === 'withinPast60' && !(moment().isSame(date, 'day') || moment().diff(date, 'day') < 60 && moment().diff(date, 'day') > 0)){
								isValid = 'withinPast60';
								break;
							}else if(vaidationTypeArr[i] === 'sameCoverageYear' && parseInt(date.substring(6,10)) !== scope.maCurrentCoverageYear){
								isValid = 'sameCoverageYear';
								break;
							}else if(vaidationTypeArr[i] === 'eligibilityStartDate' && moment(date, "MM/DD/YYYY").isAfter(moment(scope.overrideEligibilityStartDateData.enrollmentEndDate, "MM/DD/YYYY"))){
								isValid = 'eligibilityStartDate';
								break;
							}else if(vaidationTypeArr[i] === 'firstDate' && date.substring(3,5) !== '01'){
								isValid = 'firstDate';
								break;
							}
						}

						return isValid;
					}else{
						return true;
					}

				}

				function resetValidation(){
					vaidationTypeArr.forEach(function(item, index){
						ngModel.$setValidity(item, true);
					});
				}

				ngModel.$parsers.unshift(function(value){
					if(value === undefined || value === null){
						return;
					}
					var dateValidationType = customDateValidation(value);
					if(dateValidationType !== true){
						ngModel.$setValidity(dateValidationType, false);
					}

					return value;
				});

				 ngModel.$formatters.unshift(function(value) {
					if(value === undefined || value === null){
						return;
					}

					var dateValidationType = customDateValidation(value);
					if(dateValidationType !== true){
						ngModel.$setValidity(dateValidationType, false);
					}

					return value;
	             });


			}
		};

		return directive;
	}

	angular.module('indPortalApp').directive("sepDateValidation", sepDateValidation)
})();


(function(){
	angular.module('indPortalApp')
	.directive("datePicker", datePicker);

	function datePicker(){
		var directive = {
			restrict : 'AE',
			link : function(scope, element, attrs) {
				element.find('.icon-calendar').click(function(){
					element.datepicker({
						autoclose: true,
						orientation: "bottom"
					});
					element.find('input').off('click focus');
				});
			}
		};

		return directive;
	}
})();

(function(){
    Array.prototype.myFilter = function(callback, context) {
        arr = [];
        for (var i = 0; i < this.length; i++) {
            if (callback.call(context, this[i], i, this))
                arr.push(this[i]);
        }
        return arr;
    };

    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function(searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        };
    }

    if (!String.prototype.endsWith) {
        String.prototype.endsWith = function(search, this_len) {
            if (this_len === undefined || this_len > this.length) {
                this_len = this.length;
            }
            return this.substring(this_len - search.length, this_len) === search;
        };
	}

	// Production steps of ECMA-262, Edition 5, 15.4.4.17
	// Reference: http://es5.github.io/#x15.4.4.17
	if (!Array.prototype.some) {
		Array.prototype.some = function(fun, thisArg) {
		'use strict';
	
		if (this == null) {
			throw new TypeError('Array.prototype.some called on null or undefined');
		}
	
		if (typeof fun !== 'function') {
			throw new TypeError();
		}
	
		var t = Object(this);
		var len = t.length >>> 0;
	
		for (var i = 0; i < len; i++) {
			if (i in t && fun.call(thisArg, t[i], i, t)) {
			return true;
			}
		}
	
		return false;
		};
	}
	
	if (!Array.prototype.every) {
		Array.prototype.every = function(callbackfn, thisArg) {
		  'use strict';
		  var T, k;
	  
		  if (this == null) {
			throw new TypeError('this is null or not defined');
		  }
	  
		  // 1. Let O be the result of calling ToObject passing the this 
		  //    value as the argument.
		  var O = Object(this);
	  
		  // 2. Let lenValue be the result of calling the Get internal method
		  //    of O with the argument "length".
		  // 3. Let len be ToUint32(lenValue).
		  var len = O.length >>> 0;
	  
		  // 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
		  if (typeof callbackfn !== 'function') {
			throw new TypeError();
		  }
	  
		  // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
		  if (arguments.length > 1) {
			T = thisArg;
		  }
	  
		  // 6. Let k be 0.
		  k = 0;
	  
		  // 7. Repeat, while k < len
		  while (k < len) {
	  
			var kValue;
	  
			// a. Let Pk be ToString(k).
			//   This is implicit for LHS operands of the in operator
			// b. Let kPresent be the result of calling the HasProperty internal 
			//    method of O with argument Pk.
			//   This step can be combined with c
			// c. If kPresent is true, then
			if (k in O) {
	  
			  // i. Let kValue be the result of calling the Get internal method
			  //    of O with argument Pk.
			  kValue = O[k];
	  
			  // ii. Let testResult be the result of calling the Call internal method
			  //     of callbackfn with T as the this value and argument list 
			  //     containing kValue, k, and O.
			  var testResult = callbackfn.call(T, kValue, k, O);
	  
			  // iii. If ToBoolean(testResult) is false, return false.
			  if (!testResult) {
				return false;
			  }
			}
			k++;
		  }
		  return true;
		};
	}
})();
