package com.getinsured.hix.indportal.dto.dm.application.result;

public class BrokerDesignationData {

	private String jsonSource;
	
	private String brokerNPN;
	
	private String householdEmail;
	
	private String requestXML;
	
	private String responseXML;

	private String errorMessage;
	
	private String extHouseholdCaseId;

	private String esignFirstName;

	private String esignLastName;
	
	public String getJsonSource() {
		return jsonSource;
	}

	public void setJsonSource(String jsonSource) {
		this.jsonSource = jsonSource;
	}
	
	public String getBrokerNPN() {
		return brokerNPN;
	}

	public void setBrokerNPN(String brokerNPN) {
		this.brokerNPN = brokerNPN;
	}

	public String getHouseholdEmail() {
		return householdEmail;
	}

	public void setHouseholdEmail(String householdEmail) {
		this.householdEmail = householdEmail;
	}

	public String getRequestXML() {
		return requestXML;
	}

	public void setRequestXML(String requestXML) {
		this.requestXML = requestXML;
	}

	public String getResponseXML() {
		return responseXML;
	}

	public void setResponseXML(String responseXML) {
		this.responseXML = responseXML;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getExtHouseholdCaseId() {
		return extHouseholdCaseId;
	}

	public void setExtHouseholdCaseId(String extHouseholdCaseId) {
		this.extHouseholdCaseId = extHouseholdCaseId;
	}

	public String getEsignLastName() {
		return esignLastName;
	}

	public void setEsignLastName(String esignLastName) {
		this.esignLastName = esignLastName;
	}

	public String getEsignFirstName() {
		return esignFirstName;
	}

	public void setEsignFirstName(String esignFirstName) {
		this.esignFirstName = esignFirstName;
	}
	
}
