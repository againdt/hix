<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
<div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="<c:url value="/planmgmt/issuer/account/profile/financial/info/list"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
            <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="pgheader.update.companyProfile"/></li>
        </ul><!--page-breadcrumb ends-->
      <%--   <h1><issuerLogo:view/> ${issuerObj.name}</h1> --%>
        <h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.profile"/></h4>
                </div>
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
 	 	 	 		<jsp:param name="pageName" value="companyProfile"/>
 	 	 	 	</jsp:include>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="pull-left"><spring:message  code="pgheader.update.companyProfile"/></h4>
                    <a class="btn btn-small pull-right" href="<c:url value="/planmgmt/issuer/account/profile/company/edit"/>"><spring:message  code="label.edit"/></a>
                </div>
				<div class="gutter10">
		
				<form class="form-horizontal" id="frmIssuerAcctComProInfo" name="frmIssuerAcctComProInfo" action="#" method="POST">
                    <div class="control-group">
						<label class="control-label" for="comp-legal-name"><spring:message  code="label.companyLegalName"/></label>
						<div class="controls gutter5-t"><strong>${issuerObj.companyLegalName}</strong></div>
					</div><!-- end of control-group-->
                    <div class="control-group">
						<label class="control-label" for="comp-logo"><spring:message  code="label.companyLogo"/></label>
						<div class="controls">
						<c:if test="${not empty companyLogoExist}">	
							<a href="<c:url value="/planmgmt/issuer/downloadlogo/${issuerObj.hiosIssuerId}"/>" target="_blank" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a>
						</c:if>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="state"><spring:message  code="label.stateOfDomicile"/></label>
						<div class="controls gutter5-t"><strong>${issuerObj.stateOfDomicile}</strong></div>
					</div><!-- end of control-group-->
                                       
                    <div class="header">
                    <h4><spring:message  code="pgheader.update.companyAddress"/></h4>
                    </div>
                    <div class="gutter10-t">
                    <div class="control-group">
						<label class="control-label" for="bank-address-1"><spring:message  code="label.addressLine1"/></label>
						<div class="controls gutter5-t"><strong>${issuerObj.companyAddressLine1}</strong></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="bank-address-2"><spring:message  code="label.addressLine2"/></label>
						<div class="controls gutter5-t"><strong>${issuerObj.companyAddressLine2}</strong></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="bank-city"><spring:message  code="label.city"/></label>
						<div class="controls gutter5-t"><strong>${issuerObj.companyCity}</strong></div>
					</div><!-- end of control-group-->
                    
                    
                    <div class="control-group">
						<label class="control-label" for="bank-state"><spring:message  code="label.state"/></label>
						<div class="controls gutter5-t"><strong>${issuerObj.companyState}</strong></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="zipcode"><spring:message  code="label.zipCode"/></label>
						<div class="controls gutter5-t"><strong>${issuerObj.companyZip}</strong></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="issuer-website"><spring:message  code="label.issuerWebsite"/></label>
						<div class="controls gutter5-t"><a href="${issuerObj.siteUrl}"><strong>${issuerObj.siteUrl}</strong></a></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="facing-website"><spring:message  code="label.consumerFacingWebSiteURL"/></label>
						<div class="controls gutter5-t"><a href="${issuerObj.companySiteUrl}"><strong>${issuerObj.companySiteUrl}</strong></a></div>
					</div><!-- end of control-group-->
                   </div>
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
 </div>
<script type="text/javascript">
	$(document).ready(function(){
		$($.find('a[title="Account"]')).parent().addClass('active');	
	});
</script>

  
