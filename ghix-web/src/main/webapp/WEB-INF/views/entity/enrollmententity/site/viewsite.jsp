<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>

<style type="text/css">
.table tr th{
               background: none !important;
               font-weight: normal;
               color: #5A5A5A;
       }
</style>
<div class="gutter10">
<!-- Content -->
<div class="row-fluid">
	<%-- <div class="page-header">
		<h1><spring:message code="label.entity.locationAndHours"/></h1>
	</div> Removed as part of HIX-33915--%>  
	<c:if test="${loggedUser =='entityAdmin'}">
	<div id="titlebar">
		<h1 style="margin-top: 0px;margin-bottom: 0px;"><a name="skip"></a>${enrollmentEntity.entityName}</h1>
	</div>	
	</c:if>
</div>

      <div class="row-fluid">
          <jsp:include page="../../leftNavigationMenu.jsp" />

        <div class="span9" id="rightpanel"> 
        
		    <form class="form-horizontal" name="frmviewSite" id="frmviewSite" method="GET" autocomplete="off" >    
		    <df:csrfToken/>
		    <input type="hidden" name="entityId" value="<encryptor:enc value="${enrollmentEntity.id}"/>" />           

			 <div id="foradd" style="display:none">
           	 		<jsp:include page="addsubsite.jsp"/>
           	 </div>
           	<input type="hidden" name="siteId" id ="siteId" value=""/>
           	<div class="header">
           		<h4>
					<span class="span6"><spring:message code="label.entity.siteName"/></span>
 	 	 			<span class="span6"><spring:message code="label.entity.address"/></span>
				</h4>
           	</div>
           	<div class="clearfix">                  		                  
            	<c:set var="enrollmentEntityUserId" ><encryptor:enc value="${enrollmentEntity.user.id}" isurl="true"/> </c:set> 
            	<c:set var="enrollmentEntityId" ><encryptor:enc value="${enrollmentEntity.id}" isurl="true" /> </c:set>
	               <c:choose>    			                        
	         	 		<c:when test="${loggedUser =='entityAdmin' && registrationStatus!='Incomplete'}">          	 			
	         	 		 	 <button type="button" class="btn btn-primary pull-right margin10-b" onclick="window.location.href='/hix/entity/entityadmin/addsubsite/${enrollmentEntityUserId}?entityId=${enrollmentEntityId}'" id="addSubSitebtn"><spring:message code="label.entity.AddSubsite"/></button>           
	          	    	</c:when>
	          	    	<c:when test="${CA_STATE_CODE}">
							<c:if test="${loggedUser != 'entityAdmin' && registrationStatus == 'Incomplete'}">
								 <button type="button" class="btn btn-primary pull-right margin10-b" onclick="window.location.href='/hix/entity/enrollmententity/addsubsite'" id="addSubSitebtn"><spring:message code="label.entity.AddSubsite"/></button>
							</c:if>
 						</c:when>
						<c:otherwise>
							<c:if test="${loggedUser !='entityAdmin' && registrationStatus!='Pending'}">
								 <button type="button" class="btn btn-primary pull-right margin10-b" onclick="window.location.href='/hix/entity/enrollmententity/addsubsite'" id="addSubSitebtn"><spring:message code="label.entity.AddSubsite"/></button>
							</c:if>
						</c:otherwise>
	          	    </c:choose>
	          	    <c:if test="${loggedUser != 'entityAdmin' && registrationStatus == 'Incomplete'}">
						<div class="control-group">  
					     	<table class="table table-border-none">
								<tr>				                                       	                           
									<td><a class="btn" id="subSiteBack" onClick="window.location.href='/hix/entity/enrollmententity/getPopulationServed'"><spring:message code="label.entity.back"/></a></td>
					            	<td><a class="btn btn-primary" id="subSiteDone" onClick="window.location.href='/hix/entity/enrollmententity/entitycontactinfo'"><spring:message code="label.entity.subsiteDone"/></a></td>
								</tr>	
						    </table>         
					    </div>
				    </c:if>
          	    </div>
	<c:forEach items="${siteList}" var="site">           		
           		<c:set var="siteIdvar" value="${site.id}"/>
           		<div class="control-group">            
	             <h3 data-toggle="collapse" data-target="#${site.id}" class="accordion-box-custom trigger collapsetoggle clearfix">
 	 	 			<i class="icon-chevron-right margin10-l">&nbsp;</i>&nbsp;
 	 	 			<span>${site.siteLocationName}</span>
 	 	 			<span class="pull-right margin10-r">${site.mailingLocation.address1} ${site.mailingLocation.address2} ${site.mailingLocation.city} ${site.mailingLocation.state}</span>
 	 	 		</h3>
	                 
<!-- 	                <label class="control-label" id="siteNamelbl" for="siteName">Primary Site Name</label> -->
<!-- 	                <div class="controls"> -->
<%-- 	                  ${site.siteLocationName} --%>
	               	  
<!-- 	                </div>.controls -->
	            </div><!-- .control-group -->	

				<c:set var="siteId" ><encryptor:enc value="${site.id}" isurl="true"/> </c:set> 
            	<c:set var="siteType" ><encryptor:enc value="${site.siteType}" isurl="true"/> </c:set>

          	<div id="${site.id}" class="collapse out">
          	  <div class="control-group">
          	   <div class="controls">
          	    <c:choose>          
         	 		<c:when test="${loggedUser =='entityAdmin' && enrollmentEntity.registrationStatus!='Incomplete'}">  
	             		<input type="button" name="edit" id="edit" class="btn pull-right" value="<spring:message code="label.entity.edit"/>" aria-label="Edit site information" onClick="window.location.href='/hix/entity/enrollmententity/directToSite?siteId=${siteId}&siteType=${siteType}&entityId=${enrollmentEntityId}'"/>		              
          	    	</c:when>
          	    	<c:when test="${CA_STATE_CODE}">
						<c:if test="${loggedUser != 'entityAdmin' && registrationStatus == 'Incomplete'}">
	             			<input type="button" name="edit" id="edit" class="btn pull-right" value="<spring:message code="label.entity.edit"/>" aria-label="Edit site information" onClick="window.location.href='/hix/entity/enrollmententity/directToSite?siteId=${siteId}&siteType=${siteType}&entityId=${enrollmentEntityId}'"/>		              
						</c:if>
					</c:when>
					<c:otherwise>
						<c:if test="${loggedUser !='entityAdmin' && registrationStatus!='Pending'}">
	             			<input type="button" name="edit" id="edit" class="btn pull-right" value="<spring:message code="label.entity.edit"/>" aria-label="Edit site information" onClick="window.location.href='/hix/entity/enrollmententity/directToSite?siteId=${siteId}&siteType=${siteType}&entityId=${enrollmentEntityId}'"/>		              
						</c:if>
					</c:otherwise>
          	    </c:choose>	
          	    </div>
          	   </div>
               
                <div class="control-group">
          		  <div id="section">
               			<table class="table table-border-none">			                  
					    	<tbody>				                 
						    	<tr>
						        	<td class="span4 txt-right" scope="row"><spring:message code="label.entity.siteName"/></td>					                                          
						        	<td id="siteLocationName"><strong>${site.siteLocationName}</strong></td>	
						        </tr>
						        <tr>
								    <td class="span4 txt-right" scope="row"><spring:message code="label.entity.primaryEmailAddress"/></td>	
								    <td id="primaryEmailAddress"><strong>${site.primaryEmailAddress}</strong></td>
						        </tr>
						        <tr>
								    <td class="span4 txt-right" scope="row"><spring:message code="label.entity.primaryPhoneNumber"/></td>	
								    <td id="primaryPhone">
								     <strong>
								         <c:choose>
									      <c:when test="${site.primaryPhoneNumber!=null && not empty site.primaryPhoneNumber}">
									        <c:set var="primaryPhoneNumber" value="${site.primaryPhoneNumber}"></c:set>
									        <c:set var="formattedPrimaryPhoneNumber" value="(${fn:substring(primaryPhoneNumber,0,3)})${fn:substring(primaryPhoneNumber,3,6)}-${fn:substring(primaryPhoneNumber,6,10)} "></c:set>
									         ${formattedPrimaryPhoneNumber}
									      </c:when>
									      <c:otherwise>
								            ${site.primaryPhoneNumber}
								         </c:otherwise>
									   </c:choose>
							       </strong>
							       </td>
						        </tr>	
						        <tr>
								    <td class="span4 txt-right" scope="row"><spring:message code="label.entity.secondaryPhoneNumber"/></td>	
								    <td id="secondaryPhone">
								    <strong>
								    <c:choose>
									      <c:when test="${site.secondaryPhoneNumber!=null && not empty site.secondaryPhoneNumber}">
									        <c:set var="secondaryPhoneNumber" value="${site.secondaryPhoneNumber}"></c:set>
									        <c:set var="formattedSecondaryPhoneNumber" value="(${fn:substring(secondaryPhoneNumber,0,3)})${fn:substring(secondaryPhoneNumber,3,6)}-${fn:substring(secondaryPhoneNumber,6,10)} "></c:set>
									        ${formattedSecondaryPhoneNumber}
									      </c:when>
									      <c:otherwise>
								            ${site.secondaryPhoneNumber}
								         </c:otherwise>
				  				   </c:choose>
								   </strong>
								    </td>
						        </tr>                		                  
					    	</tbody>				              
				        </table> 
				       </div>
				      </div>

               <div class="control-group">
          		  <div id="from-to-day">
            		  <h4><spring:message code="label.entity.hoursOfOperation"/> </h4>            		       
            		     <c:set var="siteLocationHrsList" value="${LOCATIONHOURSMAPWITHLIST[siteIdvar]}"/>
            		     <c:forEach items ="${LOCATIONHOURSMAPWITHLIST[siteIdvar]}" var="siteLocationHrs">
            		       	<table id="${siteLocationHrs.day}" class="table table-border-none">			                  
					    	<tbody>				                 
						    	<tr>
						        	<td class="span4 txt-right" scope="row">${siteLocationHrs.day}</td>	
						        	<c:choose>
						        	<c:when test="${siteLocationHrs.toTime != null}">				                                          
						        	<td id=""><strong>${siteLocationHrs.fromTime} - ${siteLocationHrs.toTime}</strong></td>
						        	</c:when>
						        	<c:otherwise>				                                          
						        	<td id=""><strong>${siteLocationHrs.fromTime}</strong></td>
						        	</c:otherwise>
						        	</c:choose>
						        </tr>
						       </tbody>
						    </table>           		     	
            		     </c:forEach>      		    
            		 </div>
            	</div>

            	 <div class="control-group">
          		  <div id="section">
            		  <h4><spring:message code="label.entity.mailingAddres"/></h4>
            		  
            		  <table  id="" class="table table-border-none">			                  
					    	<tbody>				                 
						    	<tr>
						        	<td class="span4 txt-right" scope="row"><spring:message code="label.entity.streetAddress"/> </td>					                                          
						        	<td id="mailingLocation.address1"><strong>${site.mailingLocation.address1}</strong></td>	
						        </tr>
						        <tr>
								    <td class="span4 txt-right" scope="row"><spring:message code="label.entity.suite"/></td>	
								    <td id="mailingLocation.address2"><strong>${site.mailingLocation.address2}</strong></td>
						        </tr>
						        <tr>
								    <td class="span4 txt-right" scope="row"><spring:message code="label.entity.city"/></td>	
								    <td id="mailingLocation.city"><strong>${site.mailingLocation.city}</strong></td>
						        </tr>	
						        <tr>
								    <td class="span4 txt-right" scope="row"><spring:message code="label.entity.state"/></td>	
								    <td id="mailingLocation.state">
								    	<strong>
								    	<c:forEach var="state" items="${statelist}">
									 		<c:if test="${state.code == site.mailingLocation.state}">${state.name}</c:if>									
										</c:forEach>
								    	</strong>
								    </td>
						        </tr>  
						        <tr>
								    <td class="span4 txt-right" scope="row"><spring:message code="label.entity.zipCode"/></td>	
								    <td id="mailingLocation.zip"><strong>${site.mailingLocation.zip}</strong></td>
						        </tr>               		                  
					    	</tbody>				              
				        </table>  
			          </div>	            	   		    	   		
            	 	</div>
            																													          
                <div class="control-group">
          		  <div id="section">
            		  <h4><spring:message code="label.entity.physicalAddress"/></h4>
            		  
            		  	<table  id="" class="table table-border-none">				                  
					    	<tbody>				                 
						    	<tr>
						        	<td class="span4 txt-right" scope="row"><spring:message code="label.entity.streetAddress"/></td>					                                          
						        	<td id="physicalLocation.address1"><strong>${site.physicalLocation.address1}</strong></td>	
						        </tr>
						        <tr>
								    <td class="span4 txt-right" scope="row"><spring:message code="label.entity.suite"/></td>	
								    <td id="physicalLocation.address2"><strong>${site.physicalLocation.address2}</strong></td>
						        </tr>
						        <tr>
								    <td class="span4 txt-right" scope="row"><spring:message code="label.entity.city"/></td>	
								    <td id="physicalLocation.city"><strong>${site.physicalLocation.city}</strong></td>
						        </tr>	 
						        <tr>
								    <td class="span4 txt-right" scope="row"><spring:message code="label.entity.state"/></td>	
								    <td id="physicalLocation.state">
								    	<strong>
								    	<c:forEach var="state" items="${statelist}">
											<c:if test="${state.code == site.physicalLocation.state}">${state.name}</c:if>							
										</c:forEach>
								    	</strong>
								    </td>
						        </tr>  
						        <tr>
								    <td class="span4 txt-right" scope="row"><spring:message code="label.entity.zipCode"/> </td>	
								    <td id="physicalLocation.zip"><strong>${site.physicalLocation.zip}</strong></td>
						        </tr>              		                  
					    	</tbody>				              
				        </table>
            		</div>
            	</div>	 	                	
            	
            	<h4><spring:message code="label.entity.languageSupported"/></h4>
            	<div class="control-group">
          			<div id="section">
				    	<table  id="languageTable" class="table table-border-none">				                  
					    	<tbody>				                 
						    	<tr>
						        	<td class="span4 txt-right"><spring:message code="label.entity.spokenLangSupp"/></td>					                                          
						        	<td><strong class='comma'>${SITELANGUAGEMAP[siteIdvar].spokenLanguages}</strong></td>	
						        </tr>
						        <tr>
								    <td class="span4 txt-right"><spring:message code="label.entity.writtenLangSupp"/></td>	
								    <td><strong class='comma'>${SITELANGUAGEMAP[siteIdvar].writtenLanguages}</strong></td>
						        </tr>	                 		                  
					    	</tbody>				              
				        </table>
				     </div>				              
				</div>
		    
	         				
</div> 

</c:forEach>   

<%-- 			<c:if test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus=='Incomplete'}"> --%>
<!-- 				<div class="form-actions"> -->
<%-- 					<a class="btn btn-primary offset8" id="label.entity.next" onClick="window.location.href='/hix/entity/enrollmententity/entitycontactinfo'"><spring:message code="label.entity.next"/></a> --%>
<!-- 				</div> -->
<%-- 		   </c:if>			   --%>
</form> 

 </div>   

          </div><!-- #section --> 
          
 </div>         

    <!-- #section ENDS -->
        

  <script type="text/javascript">
  	function ie8Trim(){
		if(typeof String.prototype.trim !== 'function') {
	        String.prototype.trim = function() {
	        	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
	        };
		}
	}
  
  function setActionAndMethodType(siteType,flag,siteId)
  {
	 	 	ie8Trim();
		  	$("#siteId").val("");		  	
			$("#frmviewSite").attr('method','POST'); 
			$("#frmviewSite").attr('action', '/hix/entity/enrollmententity/subsite');
			var from_time="";
			var to_time="";
			
			var siteLocationHoursHidden = '';
			var days='${daysListNames}';
			days=days.replace("[","");
			days=days.replace("]","");
			days=days.replace(" ","");
			var dayarray=days.split(',');

			for (var i = 0; i < dayarray.length; i++) {
				
				var fromId = dayarray[i] + 'From';
				fromId=fromId.replace(" ","");
				var fromVal = $("#" + fromId).val();
				
				var toId = dayarray[i] + 'To';
				toId=toId.replace(" ","");
				var toVal = $("#" + toId).val();				
				siteLocationHoursHidden = siteLocationHoursHidden + dayarray[i] + ',' + fromVal + ',' + toVal + '~';				
			}			
			$("#siteLocationHoursHidden").val(siteLocationHoursHidden);
			
			var spokenVal ="";
			var writtenVal="";
			$.each($("input[name='spoken']:checked"), function() {			
				var spokenValcurrent = $(this).attr('value');
				
				if(spokenVal == ""){ 
					spokenVal=spokenValcurrent;
				} else {
					spokenVal=spokenVal+","+spokenValcurrent;
				}
			});
			$.each($("input[name='written']:checked"), function() {
				var  writtenValcurrent = $(this).attr('value');
				if(writtenVal == ""){ 
					writtenVal=writtenValcurrent;
				} else {
					writtenVal=writtenVal+","+writtenValcurrent;
				}
				
			});
			otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
			if(spokenVal!="")
			{
				spokenVal = spokenVal+","+otherSpokenLanguage;
			}
		else
			{
				
				spokenVal=otherSpokenLanguage;
			}
	
		
		otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
		if(otherWrittenLanguage != ""){ 
			if(writtenVal!="")
			{
				writtenVal = writtenVal+","+otherWrittenLanguage;
			}
		else
			{
				writtenVal =otherWrittenLanguage;
			}
		}
			 $("#spokenLanguages").val(spokenVal);
			 $("#writtenLanguages").val(writtenVal);
			
  }

  $(document).ready(function() {
	  $("#locationAndHours").removeClass("link");
	  $("#locationAndHours").addClass("active");
	  var addNewSubsite='${addNewSubSite}';	
	  if(addNewSubsite =="true")
	  {
		 $("#foradd").show();
		 //$("#siteLocationHoursErrorField").attr('style','display:none');
	  }
  });
 
  var validator = $("#frmviewSite").validate({ 
		onkeyup: false,
		onclick: false,
		ignore : "hidden",
		rules : {
			siteLocationName:{required : true},
			primaryPhone1 : { numberStartsWithZeroCheck: true},
			primaryPhone3 : { primaryphonecheck : true},
			secondaryPhone1 : { numberStartsWithZeroCheck: true},
			secondaryPhone3 : { secondaryphonecheck : true},
			 "physicalLocation.zip" :{number:true,PhysicalZipCodecheck:true},
			 primaryEmailAddress : { email: true},
			 "mailingLocation.zip":{required : true, number: true,MailingZipCodecheck:true},
			 "mailingLocation.city":{ required :true},
			 "mailingLocation.state":{required : true},
			 "mailingLocation.address1":{required : true},
			 spoken : {SpokenLanguageCheck: true},
			 written : {WrittenLanguageCheck: true},
			 otherSpokenLanguageCheckBox : {otherSpokenLanguageCheckboxCheck : true, otherSpokenLanguageSelectCheck : true},
			 otherWrittenLanguageCheckBox : {otherWrittenLanguageCheckboxCheck : true, otherWrittenLanguageSelectCheck : true},
			 siteLocationHoursHidden:{siteLocationHoursHiddenCheck:true,siteLocationHoursGreaterThanCheck:true}
		},
		messages : {
			primaryPhone1: { numberStartsWithZeroCheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>"},
			primaryPhone3: { primaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNo' javaScriptEscape='true'/></span>"},		
			secondaryPhone1 : { numberStartsWithZeroCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSecondaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>"},
			secondaryPhone3: { secondaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSecondaryPhoneNo' javaScriptEscape='true'/></span>"},
			siteLocationName: { required : "<span> <em class='excl'>!</em><spring:message code='label.entername' javaScriptEscape='true'/></span>"}, 	
		  	primaryEmailAddress: { email : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidEmail' javaScriptEscape='true'/></span>"},
		  	"physicalLocation.zip":{PhysicalZipCodecheck:"<span> <em class='excl'>!</em><spring:message code='label.validateZipSyntax' javaScriptEscape='true'/></span>"},
			 "mailingLocation.zip" :{ required :"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidZipCode' javaScriptEscape='true'/></span>"}, 
			 "mailingLocation.state":{ required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseSelectState' javaScriptEscape='true'/></span>"},
			 "mailingLocation.address1" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterMailingAddress' javaScriptEscape='true'/></span>",
			 MailingZipCodecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidZipCode' javaScriptEscape='true'/></span>"},
			 "mailingLocation.city":{ required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterCity' javaScriptEscape='true'/></span>"},
			 spoken : {SpokenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSpokenLanguage' javaScriptEscape='true'/></span>"},	 	 
			 written : {WrittenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateWrittenLanguage' javaScriptEscape='true'/></span>"},
			 otherSpokenLanguage : {OtherSpokenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateOtherSpokenLanguage' javaScriptEscape='true'/></span>"},
		 	 otherWrittenLanguage : {OtherWrittenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateOtherWrittenLanguage' javaScriptEscape='true'/></span>"},
		 	siteLocationHoursHidden:{siteLocationHoursHiddenCheck:"<span><em class='excl'>!</em><spring:message code='label.validateSiteLocationHours' javaScriptEscape='true'/></span>",
		 	siteLocationHoursGreaterThanCheck:"<span><em class='excl'>!</em><spring:message code='label.validateClosingHours' javaScriptEscape='true'/></span>"},			
		 	otherSpokenLanguageCheckBox : {otherSpokenLanguageCheckboxCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectothercheckbox'/></span>",
			otherSpokenLanguageSelectCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectlangforother'/></span>"},
			otherWrittenLanguageCheckBox : {otherWrittenLanguageCheckboxCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectothercheckboxforwritten'/></span>",
			otherWrittenLanguageSelectCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectlangforotherwritten'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		} 
	});
 	jQuery.validator.addMethod("siteLocationHoursHiddenCheck", function(value, element, param) {
		ie8Trim();
		// This validation is added to check if user enters all the Sitelocation hours.
		 var siteLocationHoursHidden = $("#siteLocationHoursHidden").val().trim();
		 var sitelocationarray = siteLocationHoursHidden.split('~');	
			for (var i = 0; i < sitelocationarray.length-1; i++) {		
				var siteloc = sitelocationarray[i];									
				var slocationarray = siteloc.split(',');						
				var slocarray = slocationarray[1].split(',');
				if((slocarray[0] == "") || (slocarray[1] == ""))
				{				
					return false;
				}			
			}
			return true;
	});
 	jQuery.validator.addMethod("siteLocationHoursGreaterThanCheck", function(value, element, param) {
 		ie8Trim();
 		// This validation is added to check if user enters all the Sitelocation hours.
 		 var days='${daysListNames}';
 			days=days.replace("[","");
 			days=days.replace("]","");
 			days=days.replace(" ","");
 			var dayarray=days.split(',');
 			
 			for (var i = 0; i < dayarray.length; i++) {
 				
 				var fromId = dayarray[i] + 'From';
 				fromId=fromId.replace(" ","");
 				var fromVal = $("#" + fromId).val();
 				fromVal = convertTo24Hour(fromVal);
 				
 				var toId = dayarray[i] + 'To';
 				toId=toId.replace(" ","");
 				var toVal = $("#" + toId).val();
 				toVal = convertTo24Hour(toVal);
 				
 				if((fromVal.indexOf(':') != -1)&&(toVal.indexOf(':') != -1)){
 				   var starttime = fromVal.split(':');
 				   var startHour = starttime[0];
 				   var startMinute = starttime[1];
 				 //Create date object and set the time to that
 				   var startTimeObject = new Date();
 				   startTimeObject.setHours(startHour, startMinute, 00);
 				  
 				   var endtime = toVal.split(':');
 				   var endHour = endtime[0];
 				   var endMinute = endtime[1];
 				  //Create date object and set the time to that
 				   var endTimeObject = new Date();
 				   endTimeObject.setHours(endHour, endMinute, 00);
 				  //Now compare both the dates
 				 
 				   if(startTimeObject >= endTimeObject) return false;
 				   
 				}
 				else if ((fromVal == 'Closed' && toVal != 'Closed') || (fromVal != 'Closed' && toVal == 'Closed')){
 					return false;
 				}
 				
 			}
 			return true;
 	});
 	

 	function convertTo24Hour(time) {
 		
 	    var hours = parseInt(time.substr(0, 2));
 	    if(time.indexOf('am') != -1 && hours == 12) {
 	        time = time.replace('12', '0');
 	    }
 	    if(time.indexOf('pm')  != -1 && hours < 12) {
 	        time = time.replace(hours, (hours + 12));
 	    }
 	    return time.replace(/(am|pm)/, '');
 	}
 	
	jQuery.validator.addMethod("primaryphonecheck", function(value, element, param) {	
		ie8Trim();
		primphone1 = $("#primaryPhone1").val().trim(); 
		primphone2 = $("#primaryPhone2").val().trim(); 
		primphone3 = $("#primaryPhone3").val().trim(); 
		if((primphone1.length == 0) && (primphone2.length == 0)  && (primphone3.length == 0)) {
			return true;
		}
		if(  (isNaN(primphone1)) || (primphone1.length < 3 ) || (isNaN(primphone2)) || (primphone2.length < 3 ) || (isNaN(primphone3)) || (primphone3.length < 4 ) || (primphone1 == '000') || (!isPositiveInteger(primphone1)) || (!isPositiveInteger(primphone2)) || (!isPositiveInteger(primphone3))) 
		{
			return false;
		}
		else
		{
			$("#primaryPhoneNumber").val(primphone1 + primphone2 + primphone3);
			return true;
		}	
		
	} );
	jQuery.validator.addMethod("secondaryphonecheck", function(value, element, param) {
		ie8Trim();
		secondaryphone1 = $("#secondaryPhone1").val().trim(); 
		secondaryphone2 = $("#secondaryPhone2").val().trim(); 
		secondaryphone3 = $("#secondaryPhone3").val().trim(); 
	 
		if((secondaryphone1.length == 0) && (secondaryphone2.length == 0)  && (secondaryphone3.length == 0)) {
			return true;
		}
		if((secondaryphone1.length < 3 ) || (secondaryphone2.length < 3 )  || (secondaryphone3.length < 4 ) || (isNaN(secondaryphone1)) || (isNaN(secondaryphone2)) || (isNaN(secondaryphone3)) || (secondaryphone1 == '000') || (!isPositiveInteger(secondaryphone1)) || (!isPositiveInteger(secondaryphone2)) || (!isPositiveInteger(secondaryphone3)))
		{
			return false;
		}
		else
		{
			$("#secondaryPhoneNumber").val(secondaryphone1 + secondaryphone2 + secondaryphone3);		
			return true;
		}	
	});
	jQuery.validator.addMethod("numberStartsWithZeroCheck", function(value, element, param) {
		ie8Trim();
		// This is added to check if user has not entered anything into the textbox
		if((value.length == 0)) {
			return true;
		}
		
		// If user has entered anything then test, if entered value is valid
		var firstChar = value.charAt(0);
		if(firstChar == 0) {
				return false;
		} else{
		    return true;
		}
	});

// 	function shiftbox(element,nextelement){
// 		ie8Trim();
// 		maxlength = parseInt(element.getAttribute('maxlength'));
// 		if(element.value.length == maxlength){
// 			nextelement = document.getElementById(nextelement);
// 			nextelement.focus();
// 		}
// 	}
	jQuery.validator.addMethod("PhysicalZipCodecheck", function(value, element, param) {
		ie8Trim();
		zip = $("#physicalLocation_zip").val().trim(); 
		if(zip.length != 0 && zip.length<5)
		{
				return false; 
		}
		return true;
	});
	jQuery.validator.addMethod("MailingZipCodecheck", function(value, element, param) {
		ie8Trim();
		zip = $("#mailingLocation_zip").val().trim(); 
		if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || (zip == "00000") )){ 
			return false; 
		}
		return true;
	});
	jQuery.validator.addMethod("OtherSpokenLanguageCheck", function(value, element, param) {
		ie8Trim();
		otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
		if(otherSpokenLanguage != ""){ 
			var languages='${listOflanguageNames}';
			languages=languages.replace("[","");
			languages=languages.replace("]","");
			languages=languages.replace(" ","");
			var languagesArray=languages.split(',');
			
			var found = false;
			for (i = 0; i < languagesArray.length && !found; i++) {
				languagesTocompare=languagesArray[i].trim();
			if (languagesTocompare.toLowerCase().match(otherSpokenLanguage.toLowerCase())) {						
				  found = true;
			  }
			}
			if(found){
				return false;
			}
			else
				{
					return true;
				}
			
		}
		else
			{				
				if($("#otherSpokenLanguageCheckBox").attr("checked")=='checked' && otherSpokenLanguage =="")
				{
			
					return false;
				}
				else
				{
					return true;
				}
			}		
	});

	jQuery.validator.addMethod("OtherWrittenLanguageCheck", function(value, element, param) {
		ie8Trim();
		otherWrittenLanguage = $("#otherWrittenLanguage").val(); 		
		if(otherWrittenLanguage != ""){ 
			var languages='${listOflanguageNames}';
			languages=languages.replace("[","");
			languages=languages.replace("]","");
			languages=languages.replace(" ","");
			var languagesArray=languages.split(',');
			
			var found = false;
			for (i = 0; i < languagesArray.length && !found; i++) {		
			languagesTocompare=languagesArray[i].trim();
			 if (languagesTocompare.toLowerCase().match(otherWrittenLanguage.toLowerCase())) {
				  found = true;
			  }
			}  
			if(found){
				return false;
			}
			else
				{
					return true;
				}
		
		}
		else
		{				
			if($("#otherWrittenLanguageCheckBox").attr("checked")=='checked' && otherWrittenLanguage =="")
			{
				
				return false;
			}
			else
				{
					return true;
				}
		}
	});	
	jQuery.validator.addMethod("SpokenLanguageCheck", function(value, element, param) {
		ie8Trim();
		 if(($("#otherSpokenLanguageCheckBox").attr("checked")=='checked' )){
		      return true;
		     }
		var fields = $("input[name='spoken']").serializeArray(); 
	    if (fields.length == 0) 
	    { 
	        otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
	    	if(otherSpokenLanguage == null || otherSpokenLanguage == ""){ 
	    		return false;
	    	}
	    } 
		return true;
	});
	
	jQuery.validator.addMethod("otherSpokenLanguageCheckboxCheck", function(value, element, param) {
		ie8Trim();
		
		otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
		if (otherSpokenLanguage != null ) 
	    { 
		    if((otherSpokenLanguage != null || otherSpokenLanguage != "") && !($("#otherSpokenLanguageCheckBox").attr("checked")=='checked' )){
		    		return false;
		    	}
	    } 
		return true;
	});

	jQuery.validator.addMethod("otherSpokenLanguageSelectCheck", function(value, element, param) {
		ie8Trim();
		
		temp = $("#otherSpokenLanguageCheckBox").attr("checked")=='checked';
		if (temp != false ) 
	    { 
		    if((otherSpokenLanguage == null || otherSpokenLanguage == "") && ($("#otherSpokenLanguageCheckBox").attr("checked")=='checked' )){
		    		return false;
		    	}
	    } 
		return true;
	}); 
	
	jQuery.validator.addMethod("WrittenLanguageCheck", function(value, element, param) {
		ie8Trim();
		 if(($("#otherWrittenLanguageCheckBox").attr("checked")=='checked' )){
		      return true;
		     }
		var fields = $("input[name='written']").serializeArray(); 
	    if (fields.length == 0) 
	    { 
	        otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
	    	if(otherWrittenLanguage  == null || otherWrittenLanguage == ""){ 
	    		return false;
	    	}
	    } 
		return true;
	});
	
	jQuery.validator.addMethod("otherWrittenLanguageCheckboxCheck", function(value, element, param) {
		ie8Trim();
		otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
		if (otherWrittenLanguage != null) 
	    { 
	    	if((otherWrittenLanguage != null || otherWrittenLanguage != "") && !($("#otherWrittenLanguageCheckBox").attr("checked")=='checked' )){
	    			return false;
	    	}
	    } 
		return true;
	});

	jQuery.validator.addMethod("otherWrittenLanguageSelectCheck", function(value, element, param) {
		ie8Trim();
		
		temp = $("#otherWrittenLanguageCheckBox").attr("checked")=='checked';
		if (temp != false ) 
	    { 
		    if((otherWrittenLanguage == null || otherWrittenLanguage == "") && ($("#otherWrittenLanguageCheckBox").attr("checked")=='checked' )){
		    		return false;
		    	}
	    } 
		return true;
	}); 
	function isPositiveInteger(s)
	{
	    return /^[0-9]*$/.test(s);
	}
  </script>
  <script>
$('.collapsetoggle').click(
		function() {
			$(this).find('i').toggleClass(
					'icon-chevron-right icon-chevron-down');
		});
</script>

<!-- Content ENDS -->
