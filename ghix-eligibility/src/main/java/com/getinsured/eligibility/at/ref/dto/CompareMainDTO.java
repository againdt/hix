package com.getinsured.eligibility.at.ref.dto;


/**
 * @author chopra_s
 *
 */
public class CompareMainDTO {
	private CompareApplicationDTO enrolledApplication;
	
	private CompareApplicationDTO currentApplication;

	public CompareApplicationDTO getEnrolledApplication() {
		return enrolledApplication;
	}

	public void setEnrolledApplication(CompareApplicationDTO enrolledApplication) {
		this.enrolledApplication = enrolledApplication;
	}

	public CompareApplicationDTO getCurrentApplication() {
		return currentApplication;
	}

	public void setCurrentApplication(CompareApplicationDTO currentApplication) {
		this.currentApplication = currentApplication;
	}
}
