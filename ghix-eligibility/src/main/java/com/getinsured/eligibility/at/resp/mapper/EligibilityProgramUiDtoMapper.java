package com.getinsured.eligibility.at.resp.mapper;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

import com.getinsured.eligibility.ui.dto.EligibilityProgramDTO;
import com.getinsured.eligibility.ui.enums.ApplicantEligibilityStatus;
import com.getinsured.eligibility.util.EligibilityConstants;

public final class EligibilityProgramUiDtoMapper {

	private static final Logger LOGGER = Logger.getLogger(EligibilityProgramUiDtoMapper.class);

	private EligibilityProgramUiDtoMapper() {}

	public static EligibilityProgramDTO map(com.getinsured.eligibility.model.EligibilityProgram epDO, BigDecimal maxAPTCAmt, String csrLevel){

		EligibilityProgramDTO eligibilityProgramDto = new EligibilityProgramDTO();
		eligibilityProgramDto.setId(epDO.getId());
		eligibilityProgramDto.setEligibilityType(epDO.getEligibilityType());
		eligibilityProgramDto.setEligibilityIndicator(epDO.getEligibilityIndicator());
		eligibilityProgramDto.setEligibilityStartDate(epDO.getEligibilityStartDate());
		eligibilityProgramDto.setEligibilityEndDate(epDO.getEligibilityEndDate());
		eligibilityProgramDto.setEligibilityDeterminationDate(epDO.getEligibilityDeterminationDate());
		eligibilityProgramDto.setIneligibleReason(epDO.getIneligibleReason());
		eligibilityProgramDto.setEstablishingCategoryCode(epDO.getEstablishingCategoryCode());
		eligibilityProgramDto.setEstablishingCountyName(epDO.getEstablishingCountyName());
		eligibilityProgramDto.setEstablishingStateCode(epDO.getEstablishingStateCode());

		eligibilityProgramDto.setEligibilityStatus(readEligibilityType(eligibilityProgramDto.getEligibilityType()));
		setMaxAPTC(eligibilityProgramDto, maxAPTCAmt);
		setCSRLevel(eligibilityProgramDto, csrLevel);

		return eligibilityProgramDto;
	}

	private static ApplicantEligibilityStatus readEligibilityType(String type){

		ApplicantEligibilityStatus eligType = null;
		switch (type) {
			case EligibilityConstants.MEDICAID_ELIGIBILITY_TYPE: case EligibilityConstants.EMERGENCY_MEDICAID_ELIGIBILITY_TYPE:
			case EligibilityConstants.REFUGEE_MEDICAL_ASSISTANCE_ELIGIBILITY_TYPE: case EligibilityConstants.MEDICAID_MAGI_ELIGIBILITY_TYPE:
			case EligibilityConstants.MEDICAID_NON_MAGI_ELIGIBILITY_TYPE:
				eligType = ApplicantEligibilityStatus.MEDICAID;
				break;
			case EligibilityConstants.ASSESSED_MEDICAID_MAGI_ELIGIBILITY_TYPE: case EligibilityConstants.ASSESSED_MEDICAID_NON_MAGI_ELIGIBILITY_TYPE:
				eligType = ApplicantEligibilityStatus.ASSESSED_MEDICAID;
				break;
			case EligibilityConstants.APTC_ELIGIBILITY_TYPE:
				eligType = ApplicantEligibilityStatus.APTC;
				break;
			case EligibilityConstants.STATE_SUBSIDY_ELIGIBILITY_TYPE:
				eligType = ApplicantEligibilityStatus.StateSubsidy;
				break;
			case EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE:
				eligType = ApplicantEligibilityStatus.QHP;
				break;
			case EligibilityConstants.CSR_ELIGIBILITY_TYPE:
				eligType = ApplicantEligibilityStatus.CSR;
				break;
			case EligibilityConstants.CHIP_ELIGIBILITY_TYPE:
				eligType = ApplicantEligibilityStatus.CHIP;
				break;
			case EligibilityConstants.ASSESSED_CHIP_ELIGIBILITY_TYPE:
				eligType = ApplicantEligibilityStatus.ASSESSED_CHIP;
				break;
			default:
				LOGGER.warn(EligibilityConstants.INVALID_ELIGIBILITY_TYPE_FOUND + type);
				break;
		}

		return eligType;
	}

	private static void setMaxAPTC(EligibilityProgramDTO epDTO, BigDecimal maxAPTCAmt){
		if (epDTO.getEligibilityStatus() == ApplicantEligibilityStatus.APTC){
			epDTO.setMaxAPTCAmount(maxAPTCAmt);
		}
	}


	private static void setCSRLevel(EligibilityProgramDTO epDTO, String csrLevel){
		if (epDTO.getEligibilityStatus() == ApplicantEligibilityStatus.CSR){
			epDTO.setCsrLevel(csrLevel);
		}
	}
}
