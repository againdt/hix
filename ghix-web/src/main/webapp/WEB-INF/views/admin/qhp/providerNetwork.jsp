<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
	            <h4 class="margin0"><a name="skip"></a><spring:message  code='label.addnewQHPapplication'/></h4>
	        </div>
				<!--  beginning of side bar -->
				<ol class="nav nav-list">
                    <li class="done"><a href="#"><spring:message  code='label.provideplandetails'/></a></li>
                    <li class="done"><a href="#"><spring:message  code='label.uploadplanbenefits'/></a></li>
                    <li class="done"><a href="#"><spring:message  code='label.uploadplanrates'/></a></li>
                    <li class="active"><a href="#"><spring:message  code='label.selectoraddprovidernetworks'/></a></li>
                    <!--<li><a href="#"><spring:message  code='label.addcostsharingvariations'/></a></li>-->
                    <li class="link"><a href="#"><spring:message  code='label.reviewandconfirm'/></a></li>
                </ol>
				<!-- end of side bar -->
		</div>
		<!-- end of span3 -->
		
		<div class="span9" id="rightpanel">
		<div class="header">
					<h4 class="margin0"><spring:message code='label.step4'/> <spring:message  code='label.selectoraddprovidernetworks'/></h4>
				</div>
		<form class="form-horizontal gutter10" id="frmProviderNetwork" name="frmProviderNetwork" action='<c:url value="/provider/network/save"/>' method="post">
            <df:csrfToken/>
            <input type="hidden" id="issuerName" name="issuerName" value="${issuerName}"/>
            <input type="hidden" id="planName" name="planName" value="${planName}"/>
            <input type="hidden" id="planNumber" name="planNumber" value="${planNumber}"/>
            <input type="hidden" id="issuerId" name="issuerId" value="${issuerId}"/>
            <input type="hidden" id="planId" name="planId" value="${planId}"/>
		 <input type="hidden" id="isEdit" name="isEdit" value="${isEdit}"/>

				
                <div class="control-group margin0">
                    <label for="planName" class="required control-label"><spring:message  code='label.issuerName'/></label>
                    <div class="controls paddingT5">
                        <strong>${issuerName}</strong>
                    </div> <!-- end of controls-->
                </div>
                
                <div class="control-group margin0">
                    <label for="planName" class="required control-label"><spring:message  code='label.planName'/></label>
                    <div class="controls paddingT5">
                        <strong>${planName}</strong>
                    </div> <!-- end of controls-->
                </div>
                
                <div class="control-group margin0">
                    <label for="planName" class="required control-label"><spring:message  code='label.planNumber'/></label>
                    <div class="controls paddingT5">
                        <strong>${planNumber}</strong>
                    </div> <!-- end of controls-->
                </div>
				
				<div class="box-gutter10">
					<div class="control-group">
						<label class="control-label"><spring:message code='label.providerNetwork' javaScriptEscape='true'/></label>
							<div class="controls">
								<select id="providerNetwork" name="providerNetwork">
								 <c:forEach var="network" items="${networkList}">
									<option <c:if test="${network.id == selNetwork}"> selected='selected' </c:if> value="${network.id}">
									${network.name} - ( <c:if test="${fn:toUpperCase(network.type) == 'HMO'}"><spring:message code='label.hmo' javaScriptEscape='true'/></c:if>	
									<c:if test="${fn:toUpperCase(network.type) == 'PPO'}"><spring:message code='label.ppo' javaScriptEscape='true'/></c:if> )</option>
								 </c:forEach>
								</select>
							</div>
					</div>
					<div class="form-actions">
						<a href="#collapseOne" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle btn"><spring:message code='label.addProviderNetwork' javaScriptEscape='true'/></a>
						<button class="btn btn-primary" type="submit" <c:if test="${fn:length(networkList) < 1}"> disabled="disabled"</c:if>><spring:message code='label.saveAndcontinue' javaScriptEscape='true'/></button>
					</div>
					<div id="result_display">${result}</div>
				</div>
             </form>
			<div class="gutter10">
			<div id="accordion2" class="accordion">
				<div class="accordion-group">
					<div class="accordion-body collapse" id="collapseOne">
					 
					 <form id="frmAddProviderNetwork" class="form-horizontal" name="frmAddProviderNetwork" action='<c:url value="/network/providers/add"/>' enctype="multipart/form-data" method="post">
					 <df:csrfToken/>
					 <input type="hidden" id="planId" name="planId" value="${planId}"/>
					 <input type="hidden" id="isEdit" name="isEdit" value="${isEdit}"/>
					 <!-- Facilities -->
					
						 <input type="hidden" id="hdnFacilities" name="hdnFacilities" value=""/>
					 	 <input type="hidden" id="hdnFacilitiesAddress" name="hdnFacilitiesAddress" value=""/>
					 	 <input type="hidden" id="hdnFacilitiesSpecialty" name="hdnFacilitiesSpecialty" value=""/>
					 				
					 <!-- Physicians -->
					
						 <input type="hidden" id="hdnPhysicians" name="hdnPhysicians" value=""/>
					 	 <input type="hidden" id="hdnPhysiciansAddress" name="hdnPhysiciansAddress" value=""/>
					 	 <input type="hidden" id="hdnPhysiciansSpecialty" name="hdnPhysiciansSpecialty" value=""/>
					 	 
					 	 
					 <!-- Network Adequacy -->
					 
					 <input type="hidden" id="hdnNetworkAdequacy" name="hdnNetworkAdequacy" value=""/>
					 					 	 
					 <input type="hidden" id="fileToUpload" name="fileToUpload" value=""/>
					 
						<div class="accordion-inner">

							<div class="control-group">
								<label class="control-label"><spring:message code='label.networkType' javaScriptEscape='true'/>&nbsp;<a href="#"><i
										class="icon-question-sign"></i></a></label>
								<div class="controls form-inline">
									<label class="radio inline"> <input type="radio"
										checked="" value="PPO" id="ppo"
										name="networkType"><spring:message code='label.ppo' javaScriptEscape='true'/>
									</label> <label class="radio inline"> <input type="radio"
										value="HMO" id="hmo" name="networkType" <c:if test="${networkType == 'HMO'}"> checked="" </c:if>>
										<spring:message code='label.hmo' javaScriptEscape='true'/>
									</label>
								</div>
							</div>

							<div class="control-group">
								<label class="control-label" for="input01"><spring:message code='label.providerNetName' javaScriptEscape='true'/></label>
									<div class="controls">
										<input type="text" class="input-xlarge" id="providerNetName" name="providerNetName">
										<div id="providerNetName_error" class=""></div>
									</div>
							</div>
							
							 <!--Network Adequacy file upload  -->
							
							<div class="control-group form-inline">
								<label for="networkAdequacy" class="control-label"><spring:message code='label.networkAdequacy' javaScriptEscape='true'/>&nbsp;<a href="#"><i class="icon-question-sign"></i></a>
								</label>
								<div class="controls">
									<input type="file" id="networkAdequacy" name="networkAdequacy" class="input-file">
									 &nbsp;
									<button id="btn_upload_networkAdequacy" class="btn" type="submit"><spring:message code='label.upload' javaScriptEscape='true'/></button>
									<div id="networkAdequacy_display">${networkAdequacy}</div>
								</div>

								<input type="hidden" id="checkfacAndPhyFiles" name="checkfacAndPhyFiles" value=""/>
								
							</div>
							<!--Facilities file upload  -->
							
							<div class="control-group form-inline">
								<label for="facilities" class="control-label"><spring:message code='label.facilities' javaScriptEscape='true'/>&nbsp;<a href="#"><i class="icon-question-sign"></i></a>
								</label>
								<div class="controls">
									<input type="file" id="facilities" name="facilities" class="input-file">
									 &nbsp;
									<button id="btn_upload_facilities" class="btn" type="submit"><spring:message code='label.upload' javaScriptEscape='true'/></button>
									<a href=<c:url value="/resources/sampleTemplates/facilities.csv"/>><spring:message code='label.downloadTemplate' javaScriptEscape='true'/></a>
									<div id="facilities_display">${facilities}</div>
								</div>

							</div>
							
							<!--Facilities Address file upload  -->
							<!--  HIX-2251 upload facility data using single file. Hiding upload address and specialty feature   -->
							
							<!-- <div class="control-group form-inline">
								<label for="facilitiesAddress" class="control-label"><spring:message code='label.facilitiesAddress' javaScriptEscape='true'/>&nbsp;<a href="#"><i class="icon-question-sign"></i></a>
								</label>
								<div class="controls">
									<input type="file" id="facilitiesAddress" name="facilitiesAddress" class="input-file">
									 &nbsp;
									<button id="btn_upload_facilitiesAddress" class="btn" type="submit"><spring:message code='label.upload' javaScriptEscape='true'/></button>
									<a href=<c:url value="/resources/sampleTemplates/facilities_address.csv"/>><spring:message code='label.downloadTemplate' javaScriptEscape='true'/></a>
									<div id="facilitiesAddress_display">${facilitiesAddress}</div>
								</div>

							</div>
							 -->
							<!--Facilities Specialty file upload  -->
							<!--  HIX-2251 upload facility data using single file. Hiding upload address and specialty feature   -->
							<!-- 
							<div class="control-group form-inline">
								<label for="facilitiesSpecialty" class="control-label"><spring:message code='label.facilitiesSpecialty' javaScriptEscape='true'/>&nbsp;<a href="#"><i class="icon-question-sign"></i></a>
								</label>
								<div class="controls">
									<input type="file" id="facilitiesSpecialty" name="facilitiesSpecialty" class="input-file">
									 &nbsp;
									<button id="btn_upload_facilitiesSpecialty" class="btn" type="submit"><spring:message code='label.upload' javaScriptEscape='true'/></button>
									<a href=<c:url value="/resources/sampleTemplates/facilities_speciality.csv"/>><spring:message code='label.downloadTemplate' javaScriptEscape='true'/></a>
									<div id="facilitiesSpecialty_display">${facilitiesSpecialty}</div>
								</div>

							</div> -->
							
							<!--Physicians file upload  -->
							
							<div class="control-group form-inline">
								<label for="physicians" class="control-label"><spring:message code='label.physicians' javaScriptEscape='true'/>&nbsp;<a href="#"><i class="icon-question-sign"></i></a>
								</label>
								<div class="controls">
									<input type="file" id="physicians" name="physicians" class="input-file">
									 &nbsp;
									<button id="btn_upload_physicians" class="btn" type="submit"><spring:message code='label.upload' javaScriptEscape='true'/></button>
									<a href=<c:url value="/resources/sampleTemplates/physicians.csv"/>><spring:message code='label.downloadTemplate' javaScriptEscape='true'/></a>
									<div id="physicians_display">${physicians}</div>
								</div>

							</div>
							
							<!--  HIX-2251 upload physician data using single file. Hiding upload address and specialty feature   -->
							<!--Physicians Address file upload  -->
							<!-- 
							<div class="control-group form-inline">
								<label for="physiciansAddress" class="control-label"><spring:message code='label.physiciansAddress' javaScriptEscape='true'/>&nbsp;<a href="#"><i class="icon-question-sign"></i></a>
								</label>
								<div class="controls">
									<input type="file" id="physiciansAddress" name="physiciansAddress" class="input-file">
									 &nbsp;
									<button id="btn_upload_physiciansAddress" class="btn" type="submit"><spring:message code='label.upload' javaScriptEscape='true'/></button>
									<a href=<c:url value="/resources/sampleTemplates/physicians_address.csv"/>><spring:message code='label.downloadTemplate' javaScriptEscape='true'/></a>
									<div id="physiciansAddress_display">${physiciansAddress}</div>
								</div>

							</div>
							 -->
							<!--  HIX-2251 upload physician data using single file. Hiding upload address and specialty feature   -->
							<!--Physicians Specialty file upload  -->
							<!-- 
							<div class="control-group form-inline">
								<label for="physiciansSpecialty" class="control-label"><spring:message code='label.physiciansSpecialty' javaScriptEscape='true'/>&nbsp;<a href="#"><i class="icon-question-sign"></i></a>
								</label>
								<div class="controls">
									<input type="file" id="physiciansSpecialty" name="physiciansSpecialty" class="input-file">
									 &nbsp;
									<button id="btn_upload_physiciansSpecialty" class="btn" type="submit"><spring:message code='label.upload' javaScriptEscape='true'/></button>
									<a href=<c:url value="/resources/sampleTemplates/physicians_speciality.csv"/>><spring:message code='label.downloadTemplate' javaScriptEscape='true'/></a>
									<div id="physiciansSpecialty_display">${physiciansSpecialty}</div>
								</div>

							</div> -->
							<div class="controls">
									<div id="checkfacAndPhyFiles_error" class=""></div>
								</div>
						 							
							<div class="form-actions">
								<button class="btn btn-primary" type="submit"><spring:message code='label.saveProviderNetwork' javaScriptEscape='true'/></button>
							</div>
						  
						</div>
					</form>
					</div>
				</div>
			  </div>
			</div>

           

       </div><!-- end of row-fluid -->
       	</div>
       </div>
<!--  end of row-fluid -->
<script type="text/javascript">
var validator = $("#frmAddProviderNetwork").validate({ 
	 ignore: "",
	rules : {providerNetName : { required : true},
			 checkfacAndPhyFiles : { required : function(){
		    	 if(($("#facilities").val()=='') && ($("#physicians").val()=='')){
                         	return true;
                  }
                 else{
                 	return false;
                  }
		        }
	         } 
	}, 
	messages : {
		providerNetName : { required : "<span> <em class='excl'>!</em><spring:message  code='label.validateProviderNetName' javaScriptEscape='true'/></span>"},
		checkfacAndPhyFiles : { required : "<span> <em class='excl'>!</em><spring:message  code='label.validateProviderFile' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	} 
});
$(document).ready(function() {
var button_id_names='#btn_upload_facilities,'+
					//'#btn_upload_facilitiesAddress,'+
					//'#btn_upload_facilitiesSpecialty,'+
					'#btn_upload_physicians,'+
					//'#btn_upload_physiciansAddress,'+
					//'#btn_upload_physiciansSpecialty,'+
					'#btn_upload_networkAdequacy';
					
	$(button_id_names).click(function(){
	var idName = this.id;
	var upload = idName.split("_");
	$('#fileToUpload').val(upload[2]);
	$('#frmAddProviderNetwork').ajaxSubmit({
		url: "<c:url value='/provider/network/fileupload'/>",
		success: function(responseText){
        	 
			var val = responseText.split(",");
        	 
        	 if(val[2]=='facilities'){
	        	 $("#facilities_display").text(val[0]);
				 $("#hdnFacilities").val(val[1]);
        	 }
        	/* else if(val[2]=='facilitiesAddress'){
	        	 $("#facilitiesAddress_display").text(val[0]);
				 $("#hdnFacilitiesAddress").val(val[1]);
        	 }
        	 else if(val[2]=='facilitiesSpecialty'){
	        	 $("#facilitiesSpecialty_display").text(val[0]);
				 $("#hdnFacilitiesSpecialty").val(val[1]);
        	 }*/
        	 if(val[2]=='physicians'){
	        	 $("#physicians_display").text(val[0]);
				 $("#hdnPhysicians").val(val[1]);
        	 }
        	/* else if(val[2]=='physiciansAddress'){
	        	 $("#physiciansAddress_display").text(val[0]);
				 $("#hdnPhysiciansAddress").val(val[1]);
        	 }
        	 else if(val[2]=='physiciansSpecialty'){
	        	 $("#physiciansSpecialty_display").text(val[0]);
				 $("#hdnPhysiciansSpecialty").val(val[1]);
        	 }*/
        	 if(val[2]=='networkAdequacy'){
	        	 $("#networkAdequacy_display").text(val[0]);
				 $("#hdnNetworkAdequacy").val(val[1]);
        	 }
   		 }
	});
  return false; 
});	// button click close
}); // ready method close
</script>
