package com.getinsured.eligibility.reset.password.util.constraint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.getinsured.eligibility.reset.password.util.constraint.AnswerConstraintValidator;

@Constraint(validatedBy = AnswerConstraintValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface QuestionAnswerType {

	String message() default "Data type don't match for answer.";
	Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
