/**
 * Created by sellathurai_s on 8/28/18.
 */

import moment from 'moment';
import RulesUtil from './rulesUtil';

class ProcessFacts {

    constructor(tabData, config) {
        // console.log("Processing Facts ............");
        this.tabData = tabData;

        this.config = config;
        this.currentDate = this.config.serverDate ? moment(this.config.serverDate, 'MM-DD-YYYY') : moment();
    }


    /**
     * Handles old YesNoEnum type and boolean.
     * TODO: Once we refactor remaining components, this should be removed
     * @param field
     * @returns {string|boolean|*}
     */
    isTrue(field) {
         if(field && typeof field === 'string') {
             return (field && (field.toLowerCase() === "y" || field.toLowerCase() === "yes" || field === true));
         } else {
             return field;
         }

    }

    getApplicationType(){
        return this.tabData.applicationType;
    }

    //TODO: Need to handle "EXTENDED OE"
    getPeriod(startDate, endDate){

        if (this.tabData.applicationStatus === 'CL' || this.tabData.applicationStatus === 'CC' || this.tabData.applicationStatus === 'UNCLAIMED') {
            return "N/A";
        }

        // if the coverage year is 2018 ( current year) and application type is empty (outside oe)
        // if the coverage year is 2019 ( next year) and application type is empty and its within OE start and enddate then  that is also OE

        // HIX-110302
        if (!this.tabData.applicationType && (this.currentDate).isBetween(moment(startDate, RulesUtil.DATE_FORMAT), moment(endDate, RulesUtil.DATE_FORMAT), 'days', true)){
            return "OE";
        }

        if ((this.tabData.applicationType === "OE" || this.tabData.applicationType === "SEP") && (this.currentDate).isBetween(moment(startDate, RulesUtil.DATE_FORMAT), moment(endDate, RulesUtil.DATE_FORMAT), 'days', true)){
            return "OE";
        }

        return "OUTSIDE OE";
    }

    // isSepOpen
    isSEPOpenForMemebers() {
        return (this.tabData && this.tabData.isSepOpen === "Y") ? "Y": "N";
    }

    //TODO:  Take care of < 0 days, if the SEPOpenFlag is 'Y'.
    //TODO: Seema to give use case for < 0 days.

    getNoOfDaysBeforeSEPEndDDate(SEPStartDate, SEPEndDate) {

        if(this.tabData.isSepOpen !== "Y")
            return "N";

        if(!SEPStartDate || !SEPEndDate)
            return "N";

        var startDate = moment(SEPStartDate, RulesUtil.DATE_FORMAT);
        var endDate = moment(SEPEndDate, RulesUtil.DATE_FORMAT);

        let noOfDays = endDate.diff(startDate, 'days');

        if( noOfDays >= 0 ){
            return 'Y';
        } else if ( noOfDays < 0 ) {
            return 'N';
        }
    }

    isEventFlagSet(property) {

        if(!this.tabData.sepEvent)
            return "N";

        if (this.tabData.sepEvent  && this.tabData.sepEvent[property])
            return "Y";

        return "N";
    }

    isEventIdentified() {
        // ER / outside_oe/sepEndays is negative
        //
        // if (appObject.period === 'OE') {
        //     return "N/A";
        // }

        return this.isEventFlagSet("isIdentified");
    }

    isEventVerified() {
        return this.isEventFlagSet("isVerified");
    }

    isActiveEnrollmentsPresent() {
        if( this.tabData.enrollments) {
            for(let i = 0; i < this.tabData.enrollments.length; i++) {
                if(this.tabData.enrollments[i].enrollees.length > 0)
                    return "Y";
            }

        }

        return "N";
    }

    getDentalEnrollments() {
        if( this.tabData.enrollments) {
            for (let i = 0; i < this.tabData.enrollments.length; i++) {
                if (this.tabData.enrollments[i].type.toLowerCase() === "dental") {
                    return this.tabData.enrollments[i].enrollees.length;
                }
            }
        }

        return 0;
    }

    isSSAPIDstheSame() {
        if( this.tabData.enrollments) {
            for (let i = 0; i < this.tabData.enrollments.length; i++) {
                if (this.tabData.enrollments[i].priorSsapApplicationId !== this.tabData.enrollments[i].ssapApplicationId) {
                    return "N";
                }
            }
            return "Y";
        }

        //Not sure of this use case
        return "N";
    }

    /**
     * Returns 'Y' is at least one enrollment has change flag = YES.
     * @returns {string}
     */
    isAllowChangePlan() {
        if( this.tabData.enrollments) {
            const res = this.tabData.enrollments.some((enrollment) => {
                return enrollment.allowChangePlan === "Y";
            });
            return res ? 'Y' : 'N';
        }

        //Not sure of this use case
        return "N";
    }

    // One or more health enrollment exists but no dental enrollment
    isDentalEnrollmentsForHealth() {
        if(this.getDentalEnrollments() === 0)
            return 'N';

        return "Y";
    }

    getNativeAmericanInfo() {
        for (let i = 0; i < this.tabData.applicants.length; i++) {
            if (this.isTrue(this.tabData.applicants[i].nativeAmerican))
                return "Y";
        }

        return "N";
    }

    isNativeAmerican() {
        return this.getNativeAmericanInfo();
    }

    getFinancialStatus(){
        // return this.tabData.isFinancial ? "Y": "N";
        return "Y";
    }

    getVerificationStatus () {
        return this.tabData.validationStatus ? this.tabData.validationStatus : 'N/A';
    }

    getTabData(appObject) {
        appObject.period = this.getPeriod(this.config.oepStartDate, this.config.oepEndDate);
        appObject.applicationStatus = this.tabData.applicationStatus ? this.tabData.applicationStatus : "NO";
        appObject.applicationType = this.getApplicationType() ? this.getApplicationType() : "N/A";
        appObject.sepOpenMembers = this.isSEPOpenForMemebers();
        appObject.isFinancial = this.getFinancialStatus();
        appObject.isDentalEnrollmentsForHealth = "N/A";
        appObject.appIDsAreSame = "N/A";
        appObject.allowChangePlan = "N/A";
        appObject.verificationStatus = "N/A";

        // // HIX-114219 where applicationStatus is 'EN" and applicationType is "SEP" and period is "OE"
        if (appObject.applicationStatus === 'EN' && appObject.applicationType === 'SEP' && appObject.period === 'OE') {
            appObject.renewalFlagON = "N/A";
            appObject.period = "N/A";
            appObject.sepOpenMembers = "Y";
            appObject.noOfDaysSEPEnd = "N/A";
            appObject.eventIdentified = "N/A";
            appObject.eventVerified = "N/A";
            appObject.enrollmentStatus = "Y";
            appObject.isDentalEnrollmentsForHealth = this.isDentalEnrollmentsForHealth();
            appObject.anyAIANMember = "N/A";
            appObject.isFinancial = "Y";
            appObject.appIDsAreSame = "N/A";
            appObject.allowChangePlan = "N/A";
            return appObject;
        }

        // START ------------------------ Application Status === "OP" ------------------------
        // Row: 104, 105, 106,  (107 - Default)
        // TODO: Handle EXTENDED OE.. talk with seema
        if(appObject.applicationStatus === "OP" || appObject.applicationStatus === "NO") {
            appObject.sepOpenMembers = "N/A";
            appObject.renewalFlagON = "N/A";
            appObject.noOfDaysSEPEnd = "N/A";
            appObject.eventIdentified = "N/A";
            appObject.eventVerified = "N/A";
            appObject.enrollmentStatus = "N/A";
            appObject.isDentalEnrollmentsForHealth = "N/A";
            appObject.anyAIANMember = "N/A";
            return appObject;
        }
        // END ------------------------ Application Status === "OP" ------------------------


        // START ------------------------ Application Status === "PN" ------------------------
        // Row: 121, 122, 152, 128a
        if (appObject.applicationStatus === "PN") {
            appObject.renewalFlagON = "N/A";
            appObject.eventIdentified = "N/A";
            appObject.eventVerified = "N/A";
            appObject.isFinancial = this.getFinancialStatus();
            appObject.enrollmentStatus = this.isActiveEnrollmentsPresent(); // should be 'Y' for PN
            appObject.anyAIANMember = this.isNativeAmerican();
            appObject.noOfDaysSEPEnd = "N/A";

            switch(appObject.applicationType) {
                case 'OE': // rows 121,152
                    if (appObject.period === 'OE') {
                        appObject.anyAIANMember = 'N/A';
                        appObject.sepOpenMembers = 'N/A';
                    }
                    break;
                case 'SEP':
                case 'QEP': //rows 122, 128a
                    appObject.period = "N/A";
                    if (appObject.sepOpenMembers === 'Y') {
                        appObject.noOfDaysSEPEnd = this.getNoOfDaysBeforeSEPEndDDate(this.config.sepStartDate, this.config.sepEndDate);
                        appObject.anyAIANMember = 'N/A';
                    }
                    break;
            }
            return appObject;
        }
        // END ------------------------ Application Status === "PN" ------------------------


        // START ------------------------ Application Status === "EN" ------------------------
        // Row: 124, 125, 127, 128, (129 - Default)
        if(this.tabData.applicationStatus === 'EN') {

            appObject.noOfDaysSEPEnd = "N/A";
            appObject.eventIdentified = "N/A";
            appObject.eventVerified = "N/A";
            appObject.enrollmentStatus = this.isActiveEnrollmentsPresent();


            appObject.renewalFlagON = this.tabData.renewalApplication && this.isTrue(this.tabData.renewalApplication) ? "Y" : "N/A";
            appObject.isDentalEnrollmentsForHealth = "N/A";

            if(appObject.period === 'OE' && appObject.applicationType === "OE"){
                appObject.sepOpenMembers = "N/A";
                appObject.anyAIANMember = "N/A";
                // For Row with id: 125, 126..
                if(appObject.renewalFlagON === "N/A") {
                    appObject.isDentalEnrollmentsForHealth = this.isDentalEnrollmentsForHealth();
                }
            }

            if(appObject.applicationType === 'SEP' || appObject.applicationType === 'QEP' ){
                if(appObject.enrollmentStatus === "N"){
                    appObject.applicationType = "N/A";
                } else {
                    appObject.period = "N/A";
                }

                if(appObject.sepOpenMembers === "N") {
                    appObject.isDentalEnrollmentsForHealth = "N/A";
                    appObject.anyAIANMember = this.isNativeAmerican();
                }
                else {
                    if(this.isNativeAmerican() === "Y"){
                        appObject.anyAIANMember = "Y";
                        appObject.sepOpenMembers = "N/A";
                    } else {
                    appObject.anyAIANMember = "N/A";
                    }

                    appObject.isDentalEnrollmentsForHealth = this.isDentalEnrollmentsForHealth();

                        if(appObject.isDentalEnrollmentsForHealth === "Y"){
                    appObject.appIDsAreSame = this.isSSAPIDstheSame();

                            if(appObject.appIDsAreSame === "Y") {
                    appObject.allowChangePlan = this.isAllowChangePlan();
                        }
                    }
                }
            }

            if(appObject.period === 'OUTSIDE OE' ){

                appObject.isDentalEnrollmentsForHealth = "N/A";

                //Use case 152, 152
                if(appObject.applicationType === "OE"){
                    appObject.anyAIANMember = this.isNativeAmerican();
                }

                if(appObject.enrollmentStatus === "N"){
                    appObject.sepOpenMembers = "N/A";
                    appObject.applicationType = "N/A";
                    appObject.anyAIANMember = "N/A";
                }
            }

        }
        // END ------------------------ Application Status === "EN" ------------------------


        // START ------------------------ Application Status === "CL, CC, UC" ------------------------
        // Row: 130, 131, 132..
        if (appObject.applicationStatus === "CL" || appObject.applicationStatus === "CC" || appObject.applicationStatus === "UNCLAIMED" || appObject.applicationStatus === "UC") {
            appObject.renewalFlagON = "N/A";
            appObject.noOfDaysSEPEnd = "N/A";

            appObject.eventIdentified = "N/A";
            appObject.eventVerified = "N/A";
            appObject.enrollmentStatus = "N/A";
            appObject.isDentalEnrollmentsForHealth = "N/A";
            appObject.anyAIANMember = "N/A";
            appObject.isFinancial = "N/A";
            return appObject;
        }
        // END ------------------------ Application Status === "CL, CC, UC" ------------------------


        // START ------------------------ Application Status === "ER" ------------------------
        // Row: 108-119, (120 - Default)
        if(this.tabData.applicationStatus === 'ER'){
            appObject.isDentalEnrollmentsForHealth = "N/A";
            appObject.isFinancial = this.getFinancialStatus();
            appObject.anyAIANMember = "N/A";
            appObject.verificationStatus = this.getVerificationStatus();
            const VERIFICATION_STATUSES = ['PENDING_INFO', 'HMS_INTRANSIT'];

            if(appObject.period === 'OE' && appObject.applicationType === "OE"){
                appObject.renewalFlagON = this.tabData.renewalApplication && this.isTrue(this.tabData.renewalApplication) ? "Y" : "N/A";
                appObject.sepOpenMembers = "N/A";
                appObject.noOfDaysSEPEnd = "N/A";
                appObject.eventIdentified = "N/A";
                appObject.eventVerified = "N/A";
                appObject.enrollmentStatus = "N/A"; // OE
            } else {

                appObject.renewalFlagON = "N/A";
                appObject.period = "N/A";
                appObject.enrollmentStatus = this.isActiveEnrollmentsPresent();

                if(appObject.sepOpenMembers === 'Y'){

                    appObject.noOfDaysSEPEnd = this.getNoOfDaysBeforeSEPEndDDate(this.config.sepStartDate, this.config.sepEndDate);

                    //Row 119
                    if(appObject.noOfDaysSEPEnd === 'N'){
                        appObject.eventIdentified = "N/A";
                        appObject.eventVerified = "N/A";

                    } else { //Row 112 to 118
                        appObject.eventIdentified = this.isEventIdentified();

                        if(appObject.eventIdentified === 'Y') {

                            appObject.eventVerified = this.isEventVerified();
                        } else {
                            appObject.eventVerified = "N/A";
                        }
                    }

                } else { // appObject.sepOpenMembers === 'N' // Row: 110
                    appObject.noOfDaysSEPEnd = "N/A";
                    appObject.eventIdentified = this.isEventIdentified();
                    appObject.eventVerified = "N/A";
                }
            }
            // row 222, 223,224
            if (VERIFICATION_STATUSES.includes(appObject.verificationStatus)) {
                appObject.renewalFlagON = 'N/A';
                appObject.applicationType = 'N/A';
                appObject.period = 'N/A';
                appObject.noOfDaysSEPEnd = this.getNoOfDaysBeforeSEPEndDDate(this.config.sepStartDate, this.config.sepEndDate);
                appObject.eventIdentified = this.isEventIdentified();
                appObject.eventVerified = this.isEventVerified();
                appObject.enrollmentStatus = 'N/A';
                appObject.isFinancial = 'N/A';

                //row 224
                if (appObject.verificationStatus === 'HMS_INTRANSIT') {
                    appObject.noOfDaysSEPEnd = 'N/A'
                }
            }
        }
        // END ------------------------ Application Status === "ER" ------------------------

        console.log(this.tabData.applicationYear + " - Processed Facts =>" + JSON.stringify(appObject));

        return appObject;
    }

    executeFacts() {
        let appObject =  {
            applicationStatus: 'N/A',
            renewalFlagON: 'N/A',
            applicationType: "N/A",
            period: 'N/A',
            sepOpenMembers:  'N/A',
            noOfDaysSEPEnd: 'N/A',
            eventIdentified: 'N/A',
            eventVerified: 'N/A',
            enrollmentStatus: 'N/A',
            isDentalEnrollmentsForHealth: 'N/A',
            anyAIANMember: 'N/A',
            isFinancial: 'N/A',
            appIDsAreSame: "N/A",
            allowChangePlan: "N/A"
        };

        return this.getTabData(appObject);
    }
}

export { ProcessFacts as default};