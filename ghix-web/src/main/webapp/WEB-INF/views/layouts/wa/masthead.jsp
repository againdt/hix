<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>

<%
String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
%>
<c:set var="userActiveRoleName" value="<%=userActiveRoleName%>" />

<div class="navbar navbar-fixed-top navbar-inverse" id="masthead" role="banner">
  <div>
    <div class="container">
      	<div class="nav-collapse collapse pull-right">
			<c:if test="${userActiveRoleName != null && userActiveRoleName != 'null' && userActiveRoleName != ''}">
                <ul class="nav pull-right">
					<li><a href="<gi:cdnurl value="/account/user/logout" />">Logout</a></li>
				</ul>
			</c:if>
		</div>
    </div>  
  </div> 
</div>
