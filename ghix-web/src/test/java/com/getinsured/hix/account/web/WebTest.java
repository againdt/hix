/**
 * 
 */
package com.getinsured.hix.account.web;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.getinsured.hix.platform.test.GhixBaseMvcTest;

/**
 * Web test for testing web urls and such.
 * 
 * @author Yevgen Golubenko
 */
public class WebTest extends GhixBaseMvcTest
{
	public static final String    EMAIL_EXADMIN = "exadmin@ghix.com";

	private MockMvc               mockMvc;
	private MockHttpSession       session = new MockHttpSession();

	@Autowired
	UserController                userController;

	@Resource
	private FilterChainProxy      springSecurityFilterChain;

	@Autowired
	private WebApplicationContext webApplicationContext;

	public MockMvc loginWithEmailAddress(final String email)
	{
		// setup mock MVC
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilters(springSecurityFilterChain).build();

		final UsernamePasswordAuthenticationToken principal = userController.getPrincipal(email);
		SecurityContextHolder.getContext().setAuthentication(principal);

		session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, new MockSecurityContext(principal));

		return mockMvc;
	}

	public static class MockSecurityContext implements SecurityContext
	{

		private static final long serialVersionUID = -1386535243513362694L;

		private Authentication    authentication;

		public MockSecurityContext(final Authentication authentication)
		{
			this.authentication = authentication;
		}

		@Override
		public Authentication getAuthentication()
		{
			return authentication;
		}

		@Override
		public void setAuthentication(final Authentication authentication)
		{
			this.authentication = authentication;
		}
	}

	/**
	 * @return the mockMvc
	 */
	public MockMvc getMockMvc()
	{
		return mockMvc;
	}

	/**
	 * @param mockMvc the mockMvc to set
	 */
	public void setMockMvc(final MockMvc mockMvc)
	{
		this.mockMvc = mockMvc;
	}

	/**
	 * @return the session
	 */
	public MockHttpSession getSession()
	{
		return session;
	}

	/**
	 * @param session the session to set
	 */
	public void setSession(final MockHttpSession session)
	{
		this.session = session;
	}

	/**
	 * @return the userController
	 */
	public UserController getUserController()
	{
		return userController;
	}

	/**
	 * @param userController the userController to set
	 */
	public void setUserController(final UserController userController)
	{
		this.userController = userController;
	}

	/**
	 * @return the webApplicationContext
	 */
	public WebApplicationContext getWebApplicationContext()
	{
		return webApplicationContext;
	}

	/**
	 * @param webApplicationContext the webApplicationContext to set
	 */
	public void setWebApplicationContext(final WebApplicationContext webApplicationContext)
	{
		this.webApplicationContext = webApplicationContext;
	}
}
