package com.getinsured.eligibility.ssap.integration.notices;

public class SsapApplicantVerificationNoticeDTO {
	
	private String name;
	private String ssnStatus;
	private String citizenshipStatus;
	private String incarcerationStatus;
	private String residencyStatus;
	private String deathStatus;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSsnStatus() {
		return ssnStatus;
	}
	public void setSsnStatus(String ssnStatus) {
		this.ssnStatus = ssnStatus;
	}
	public String getCitizenshipStatus() {
		return citizenshipStatus;
	}
	public void setCitizenshipStatus(String citizenshipStatus) {
		this.citizenshipStatus = citizenshipStatus;
	}
	public String getIncarcerationStatus() {
		return incarcerationStatus;
	}
	public void setIncarcerationStatus(String incarcerationStatus) {
		this.incarcerationStatus = incarcerationStatus;
	}
	public String getResidencyStatus() {
		return residencyStatus;
	}
	public void setResidencyStatus(String residencyStatus) {
		this.residencyStatus = residencyStatus;
	}
	public String getDeathStatus() {
		return deathStatus;
	}
	public void setDeathStatus(String deathStatus) {
		this.deathStatus = deathStatus;
	}
	@Override
	public String toString() {
		return "SsapApplicantVerificationNoticeDTO [name=" + name
				+ ", ssnStatus=" + ssnStatus + ", citizenshipStatus="
				+ citizenshipStatus + ", incarcerationStatus="
				+ incarcerationStatus + ", residencyStatus=" + residencyStatus
				+ ", deathStatus=" + deathStatus + "]";
	}

	
	
}
