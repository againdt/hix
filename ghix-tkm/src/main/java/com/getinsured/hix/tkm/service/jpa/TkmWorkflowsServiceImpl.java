package com.getinsured.hix.tkm.service.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.TkmWorkflows;
import com.getinsured.hix.tkm.repository.ITkmWorkflowsRepository;
import com.getinsured.hix.tkm.service.TkmWorkflowsService;
import com.getinsured.hix.tkm.tkmException.TKMException;

@Service("tkmWorkflowsService")
@Repository
public class TkmWorkflowsServiceImpl implements TkmWorkflowsService
{

	@Autowired private ITkmWorkflowsRepository iTkmWorkflowsRepository;
	
	@Override
	public List<TkmWorkflows> getTicketTypeList() throws TKMException {
		return iTkmWorkflowsRepository.findAll();
	}

	@Override
	public List<String> getDistinctTicketType() throws TKMException
	{
		return iTkmWorkflowsRepository.getDistinctTicketType();
	}
	
	@Override
	public TkmWorkflows getTicketWorkflowsByCriteria (String type, String ticketCategory) {
		return iTkmWorkflowsRepository.findByCriteria(type, ticketCategory);
	}

	@Override
	public TkmWorkflows getTicketWorkflowsByCriteria (String ticketType) {
		return iTkmWorkflowsRepository.findByCriteria(ticketType);
	}

	@Override
	public List<TkmWorkflows> getActiveTicketTypeList() throws TKMException {
		return iTkmWorkflowsRepository.findByStatus(TkmWorkflows.TicketStatus.ACTIVE);
	}

}
