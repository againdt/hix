﻿define(['basePage'], function (basePage) {
    var multiPageLeftMenu = new baseComponent();
    multiPageLeftMenu.type = "multiPageLeftMenu";
    multiPageLeftMenu._rawCompData = null;
    multiPageLeftMenu.menuObject = null;
    multiPageLeftMenu.preInit = function () {
        var vm = this;
        vm.context.on(vm.context.events.BEFORE_CONTROLLER_CALLED, {}, function (e) {
            var $scope = e.evData;
            $scope.$on("$routeChangeSuccess", function (e) {
                try {
                    var path = arguments[1].$$route.originalPath;
                    vm.menuObject.activate(path);
                } catch (e) { }
            });
        });
    };
    multiPageLeftMenu.getHTML = function (cgfObj) {
        var defCfg = {};
        cgfObj = $.extend(defCfg, cgfObj);
        var vm = this;
        var compData = cgfObj;
        vm._rawCompData = compData;
        vm.context.registerComponent(basePage);
        var basePageHTML = vm.context.comp["basePage"].getHTML(compData);
        var $basePageHTML = $(basePageHTML);
        var menuHtml;
        for (var g = 0; g < vm._rawCompData.pageData.length; g++) {
            if (true == vm._rawCompData.pageData[g].leftMenu) {
                vm.menuObject = vm.context.comp[vm._rawCompData.pageData[g].type];
                menuHtml = vm.context.comp[vm._rawCompData.pageData[g].type].getHTML(vm._rawCompData.pageData);
            };
        };

        $basePageHTML.find(".pageContent").empty();
        $basePageHTML.find(".pageContent").append('<div class="span3 grey" id="sidebar">'+menuHtml+'</div>');
        $basePageHTML.find(".pageContent").append('<div class="span9" id="rightpanel" ><div ng-view></div></div>');
        vm.configRoute();
        return $basePageHTML[0].outerHTML;
    };
    multiPageLeftMenu.redirectController = function () {

    };
    multiPageLeftMenu.configRoute = function () {
        var vm = this;
        var ngApp = vm.context.ngApp;
        vm.context.registerEvent("ON_ROUTE_CHANGE");
        ngApp.config(function ($routeProvider, $locationProvider) {
            for (var c = 0; c < vm._rawCompData.pageData.length; c++) {
                var nPageData = vm._rawCompData.pageData[c];
                if (nPageData.type == "redirect") {
                    $routeProvider.when('/' + nPageData.id, {
                        redirectTo: nPageData.url
                    });
                } else if (nPageData.type == "singlePage") {
                    $routeProvider.when('/' + nPageData.id, {
                        controller: nPageData.id + "Controller",
                        template: vm.getPageHTML(nPageData)
                    });
                }
                if (nPageData.default == true) {
                    $routeProvider.otherwise({
                        redirectTo: ""+nPageData.id
                    })
                };
            };
        });
    };
    multiPageLeftMenu.getPageHTML = function (pageData) {
        var rawHtml = this.context._getRawHTML(pageData);
        return rawHtml;
    };
    multiPageLeftMenu.redirectTo = function (pageData) {
    };
    return multiPageLeftMenu;
});