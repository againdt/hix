package com.getinsured.eligibility.ssap.integration.household.splitter;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.Splitter;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.getinsured.eligibility.ssap.integration.util.SsapIntegrationUtil;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.timeshift.TSCalendar;

@Component(value="vlpRequestSplitter")
public class VLPRequestSplitter {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(VLPRequestSplitter.class);
    private static final String VLP_DOCUMENT_STATUS = "VLP_DOCUMENT_STATUS";

		@Value("#{configProp['fdh.vlp.casepoc.fullname'] != null ? configProp['fdh.vlp.casepoc.fullname'] : 'DHS Tester'}")
    private String casePOCFullName;

		@Value("#{configProp['fdh.vlp.casepoc.phonenumber'] != null ? configProp['fdh.vlp.casepoc.phonenumber'] : '5555555555'}")
		private String casePOCPhoneNumber;

		@Value("#{configProp['fdh.cms_partner'] != null ? configProp['fdh.cms_partner'] : 'SSHIX'}")
		private String cmsPartner;

    @Splitter
    @SuppressWarnings("unchecked")
    public Message<String>[]  split(Message<String> message) throws IOException, ParseException {
        String ssapJSON = message.getHeaders().get(SSAPIntegrationConstants.FILTERED_SSAP_JSON_HEADER).toString();
        JSONArray nonCitizenMembers = removeCitizens(ssapJSON);
        List<Message<String>> vlpRequests = createVLPRequest(message, nonCitizenMembers);
        return vlpRequests.toArray(new Message[vlpRequests.size()]);
    }
    
    @SuppressWarnings("unchecked")
    private JSONArray removeCitizens(String ssapJSON) throws ParseException {
    	JSONParser parser = new JSONParser();
        // Parse the SSAP JSON and remove citizens
        JSONObject ssapJSONObject = (JSONObject) parser.parse(ssapJSON);
        ssapJSONObject = (JSONObject)ssapJSONObject.get(SSAPIntegrationConstants.SINGLE_STREAMLINED_APPLICATION);
        JSONArray taxHouseholds = (JSONArray) ssapJSONObject.get(SSAPIntegrationConstants.TAX_HOUSEHOLD);
        List<JSONObject> membersToRemove = new ArrayList<JSONObject>();
        JSONArray householdMembers = null;
        
        // Loop through the household and remove citizens
        for(Object taxHousehold : taxHouseholds ) {
            JSONObject household = (JSONObject) taxHousehold;
            householdMembers = (JSONArray) household.get(SSAPIntegrationConstants.HOUSEHOLD_MEMBER);
            for(Object householdMember : householdMembers) {
                JSONObject member = (JSONObject) householdMember;
                JSONObject citizenshipImmigrationStatus = (JSONObject)member.get(SSAPIntegrationConstants.CITIZENSHIP_IMMIGRATION_STATUS);
                if (null != citizenshipImmigrationStatus && 
                    Boolean.parseBoolean(citizenshipImmigrationStatus.get(
                            SSAPIntegrationConstants.CITIZENSHIP_AS_ATTESTED_INDICATOR).toString())) {
                    membersToRemove.add(member);
                }
            }
        }
        if(householdMembers != null && membersToRemove.size() > 0){
        	householdMembers.removeAll(membersToRemove);
        }
        return householdMembers;
    }

    @SuppressWarnings("unchecked")
    private List<Message<String>> createVLPRequest(Message<String> message, JSONArray nonCitizens) throws UnknownHostException {
        List<Message<String>> vlpRequests = new ArrayList<Message<String>>();
        String clientIp =  message.getHeaders().get(SSAPIntegrationConstants.CLIENT_IP_HEADER).toString();
        String ssapApplicationId = message.getHeaders().get(SSAPIntegrationConstants.SSAP_APPLICATION_ID_HEADER).toString();
        List<SsapApplicantEvent> ssapApplicantEvents = (List<SsapApplicantEvent>) message.getHeaders().get(SSAPIntegrationConstants.SSAP_APPLICANT_DATA_HEADER);
        SsapApplicantEvent ssapApplicantEvent = ssapApplicantEvents.get(0);
        Calendar coverageStartDate = TSCalendar.getInstance();
        coverageStartDate.setTimeInMillis(ssapApplicantEvent.getSsapAplicationEvent().getEnrollmentStartDate().getTime());
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String requestedCoverageStartDate = df.format(coverageStartDate.getTime());
        
        // Loop through remaining applicants in household and create VLP request for each applicant
        for(Object nonCitizen : nonCitizens) {
            Message<String> ruleMessage = null;
            JSONObject member = (JSONObject) nonCitizen;
            JSONObject citizenshipImmigrationStatusObj = (JSONObject) member.get("citizenshipImmigrationStatus");
            String eligibleImmigrationDocumentSelected = null;
            if(citizenshipImmigrationStatusObj.get(SSAPIntegrationConstants.ELIGIBLE_IMMIGRATION_DOCUMENT_SELECTED) != null){
            	 eligibleImmigrationDocumentSelected =  SsapIntegrationUtil.getImmigrationDocument(citizenshipImmigrationStatusObj.get(SSAPIntegrationConstants.ELIGIBLE_IMMIGRATION_DOCUMENT_SELECTED).toString());
            }
            
            if(null != citizenshipImmigrationStatusObj.get("eligibleImmigrationStatusIndicator") && Boolean.valueOf(citizenshipImmigrationStatusObj.get("eligibleImmigrationStatusIndicator").toString()) ||
            		(null != eligibleImmigrationDocumentSelected && (eligibleImmigrationDocumentSelected.equals(SSAPIntegrationConstants.NatrOfCert) || 
            				eligibleImmigrationDocumentSelected.equals(SSAPIntegrationConstants.CertOfCit)))) {
                JSONObject vlpRequest = new JSONObject();
                vlpRequest.put("applicationId", Long.parseLong(ssapApplicationId));
                vlpRequest.put(SSAPIntegrationConstants.CLIENT_IP, clientIp);
                vlpRequest.put("documentType", eligibleImmigrationDocumentSelected);
                vlpRequest.put("cms_partner", this.cmsPartner);
                JSONObject payload = new JSONObject();
                payload.put("DHSID", getDHSObject(eligibleImmigrationDocumentSelected, citizenshipImmigrationStatusObj));
                payload.put("RequestedCoverageStartDate", requestedCoverageStartDate);
                JSONObject citizenshipDocument = (JSONObject)citizenshipImmigrationStatusObj.get("citizenshipDocument");
                Object nameSameOnDocumentIndicator = null;
                if(null != citizenshipDocument && null != citizenshipDocument.get("nameSameOnDocumentIndicator")) {
                	nameSameOnDocumentIndicator = citizenshipDocument.get("nameSameOnDocumentIndicator");
                }
                JSONObject nameObject = null;
                if(null != nameSameOnDocumentIndicator && !Boolean.valueOf(nameSameOnDocumentIndicator.toString())) {
                	nameObject = (JSONObject) citizenshipDocument.get("nameOnDocument");
                } else {
                	nameObject = (JSONObject)member.get("name");
                }
                payload.put("LastName", nameObject.get("lastName"));
                payload.put("FirstName", nameObject.get("firstName"));
                if(null != nameObject.get("middleName") && StringUtils.isNotBlank(nameObject.get("middleName").toString())) {
                    payload.put("MiddleName", nameObject.get("middleName"));
                }
                if(null != citizenshipImmigrationStatusObj.get("livedIntheUSSince1996Indicator")) {
                    payload.put("FiveYearBarApplicabilityIndicator", !Boolean.valueOf(citizenshipImmigrationStatusObj.get("livedIntheUSSince1996Indicator").toString()));
                }
                payload.put("RequestGrantDateIndicator", false);
                payload.put("RequesterCommentsForHub", "Initial verification request");
                payload.put("DateOfBirth", member.get("dateOfBirth"));
                
                if(this.casePOCFullName != null && !"".equals(this.casePOCFullName)) {
									payload.put("CasePOCFullName", this.casePOCFullName);
								} else {
									payload.put("CasePOCFullName",
										String.format("%s %s",
											payload.get("FirstName"),
											payload.get("LastName"))
									);
								}

                if(this.casePOCPhoneNumber != null && !"".equals(this.casePOCPhoneNumber)) {
                	payload.put("CasePOCPhoneNumber", this.casePOCPhoneNumber);
								} else {
									payload.put("CasePOCPhoneNumber", "5555555555");
								}

                if(payload.get("MiddleName") != null && !"".equals(payload.get("MiddleName"))) {
									payload.put("AKA",
											String.format("%s %s %s",
													payload.get("FirstName"),
													payload.get("MiddleName"),
													payload.get("LastName"))
									);
								} else {
									payload.put("AKA",
											String.format("%s %s",
													payload.get("FirstName"),
													payload.get("LastName"))
									);
								}

                vlpRequest.put("payload", payload);
                LOGGER.debug(">>>>>>>>>>>>>>>>>> VLP Request: " + vlpRequest.toJSONString());
                ruleMessage = MessageBuilder.withPayload(vlpRequest.toJSONString()).copyHeadersIfAbsent(message.getHeaders())
                                                                         .setHeader(VLP_DOCUMENT_STATUS, "DOCUMENT_AVAILABLE")
                                                                         .setHeader(SSAPIntegrationConstants.SSAP_MEMBER, member.toJSONString())
                                                                         .setHeader(SSAPIntegrationConstants.SSAP_APPLICATION_ID_HEADER, ssapApplicationId).build();
            } else {
                ruleMessage = MessageBuilder.withPayload("NO_DOCUMENT").copyHeadersIfAbsent(message.getHeaders())
                        .setHeader(VLP_DOCUMENT_STATUS, "NO_DOCUMENT")
                        .setHeader(SSAPIntegrationConstants.SSAP_MEMBER, member.toJSONString())
                        .setHeader(SSAPIntegrationConstants.SSAP_APPLICATION_ID_HEADER, ssapApplicationId).build();
            }
            vlpRequests.add(ruleMessage);
            
        }
        return vlpRequests;
    }

    @SuppressWarnings("unchecked")
    private JSONObject getDHSObject(String eligibleImmigrationDocumentSelected, JSONObject citizenshipImmigrationStatusObj) {
        JSONObject dhsIdObj = new JSONObject();
        JSONObject citizenshipDocumentObj = (JSONObject)citizenshipImmigrationStatusObj.get("citizenshipDocument");
        if(eligibleImmigrationDocumentSelected != null){
        
	        switch (eligibleImmigrationDocumentSelected) {
	            case SSAPIntegrationConstants.I327:
	                JSONObject I327DocumentIDObj = new JSONObject();
	                addFieldIfNotNull(I327DocumentIDObj, "AlienNumber", citizenshipDocumentObj.get("alienNumber"));
	                addFieldIfNotNull(I327DocumentIDObj, "DocExpirationDate", citizenshipDocumentObj.get("documentExpirationDate"));
	                dhsIdObj.put(SSAPIntegrationConstants.I327 + "DocumentID", I327DocumentIDObj);
	                break;
	            
	            case SSAPIntegrationConstants.I551:
	                JSONObject I551DocumentIDObj = new JSONObject();
	                addFieldIfNotNull(I551DocumentIDObj, "AlienNumber", citizenshipDocumentObj.get("alienNumber"));
	                addFieldIfNotNull(I551DocumentIDObj, "ReceiptNumber", citizenshipDocumentObj.get("foreignPassportOrDocumentNumber"));
	                addFieldIfNotNull(I551DocumentIDObj, "DocExpirationDate", citizenshipDocumentObj.get("documentExpirationDate"));
	                dhsIdObj.put(SSAPIntegrationConstants.I551 + "DocumentID", I551DocumentIDObj);
	                break;
	            
	            case SSAPIntegrationConstants.I571:
	                JSONObject I571DocumentIDObj = new JSONObject();
	                addFieldIfNotNull(I571DocumentIDObj, "AlienNumber", citizenshipDocumentObj.get("alienNumber"));
	                addFieldIfNotNull(I571DocumentIDObj, "DocExpirationDate", citizenshipDocumentObj.get("documentExpirationDate"));
	                dhsIdObj.put(SSAPIntegrationConstants.I571 + "DocumentID", I571DocumentIDObj);
	                break;
	            
	            case SSAPIntegrationConstants.I766:
	                JSONObject I766DocumentIDObj = new JSONObject();
	                addFieldIfNotNull(I766DocumentIDObj, "AlienNumber", citizenshipDocumentObj.get("alienNumber"));
	                addFieldIfNotNull(I766DocumentIDObj, "ReceiptNumber", citizenshipDocumentObj.get("foreignPassportOrDocumentNumber"));
	                addFieldIfNotNull(I766DocumentIDObj, "DocExpirationDate", citizenshipDocumentObj.get("documentExpirationDate"));
	                dhsIdObj.put(SSAPIntegrationConstants.I766 + "DocumentID", I766DocumentIDObj);
	                break;
	            
	            case SSAPIntegrationConstants.MacReadI551:
	                JSONObject MacReadI551DocumentIDObj = new JSONObject();
	                addFieldIfNotNull(MacReadI551DocumentIDObj, "AlienNumber", citizenshipDocumentObj.get("alienNumber"));
	                addFieldIfNotNull(MacReadI551DocumentIDObj, "VisaNumber", citizenshipDocumentObj.get("visaNumber"));
	                addFieldIfNotNull(MacReadI551DocumentIDObj, "PassportNumber", citizenshipDocumentObj.get("foreignPassportOrDocumentNumber"));
	                addFieldIfNotNull(MacReadI551DocumentIDObj, "CountryOfIssuance", citizenshipDocumentObj.get("foreignPassportCountryOfIssuance"));
	                addFieldIfNotNull(MacReadI551DocumentIDObj, "DocExpirationDate", citizenshipDocumentObj.get("documentExpirationDate"));
	                dhsIdObj.put(SSAPIntegrationConstants.MacReadI551 + "DocumentID", MacReadI551DocumentIDObj);
	                break;
	
	            case SSAPIntegrationConstants.TempI551:
	                JSONObject TempI551DocumentIDObj = new JSONObject();
	                addFieldIfNotNull(TempI551DocumentIDObj, "AlienNumber", citizenshipDocumentObj.get("alienNumber"));
	                addFieldIfNotNull(TempI551DocumentIDObj, "PassportNumber", citizenshipDocumentObj.get("foreignPassportOrDocumentNumber"));
	                addFieldIfNotNull(TempI551DocumentIDObj, "CountryOfIssuance", citizenshipDocumentObj.get("foreignPassportCountryOfIssuance"));
	                addFieldIfNotNull(TempI551DocumentIDObj, "DocExpirationDate", citizenshipDocumentObj.get("documentExpirationDate"));
	                dhsIdObj.put(SSAPIntegrationConstants.TempI551 + "DocumentID", TempI551DocumentIDObj);
	                break;
	            
	            case SSAPIntegrationConstants.I94Document:
	                JSONObject I94DocumentIDObj = new JSONObject();
	                addFieldIfNotNull(I94DocumentIDObj, "I94Number", citizenshipDocumentObj.get("i94Number"));
	                addFieldIfNotNull(I94DocumentIDObj, "SEVISID", sanitizeSevisId(citizenshipDocumentObj.get("SEVISId")));
	                addFieldIfNotNull(I94DocumentIDObj, "SEVISID", sanitizeSevisId(citizenshipDocumentObj.get("sevisId")));
	                addFieldIfNotNull(I94DocumentIDObj, "DocExpirationDate", citizenshipDocumentObj.get("documentExpirationDate"));
	                dhsIdObj.put(SSAPIntegrationConstants.I94Document + "ID", I94DocumentIDObj);
	                break;
	                
	            case SSAPIntegrationConstants.I94UnexpForeignPassport:
	                JSONObject I94UnexpForeignPassportDocumentIDObj = new JSONObject();
	                addFieldIfNotNull(I94UnexpForeignPassportDocumentIDObj, "I94Number", citizenshipDocumentObj.get("i94Number"));
	                addFieldIfNotNull(I94UnexpForeignPassportDocumentIDObj, "VisaNumber", citizenshipDocumentObj.get("visaNumber"));
	                addFieldIfNotNull(I94UnexpForeignPassportDocumentIDObj, "PassportNumber", citizenshipDocumentObj.get("foreignPassportOrDocumentNumber"));
	                addFieldIfNotNull(I94UnexpForeignPassportDocumentIDObj, "CountryOfIssuance", citizenshipDocumentObj.get("foreignPassportCountryOfIssuance"));
	                addFieldIfNotNull(I94UnexpForeignPassportDocumentIDObj, "SEVISID", sanitizeSevisId(citizenshipDocumentObj.get("SEVISId")));
	                addFieldIfNotNull(I94UnexpForeignPassportDocumentIDObj, "SEVISID", sanitizeSevisId(citizenshipDocumentObj.get("sevisId")));
	                addFieldIfNotNull(I94UnexpForeignPassportDocumentIDObj, "DocExpirationDate", citizenshipDocumentObj.get("documentExpirationDate"));
	                dhsIdObj.put(SSAPIntegrationConstants.I94UnexpForeignPassport + "DocumentID", I94UnexpForeignPassportDocumentIDObj);
	                break;
	                
	            case SSAPIntegrationConstants.UnexpForeignPassport:
	                JSONObject UnexpForeignPassportDocumentIDObj = new JSONObject();
	                addFieldIfNotNull(UnexpForeignPassportDocumentIDObj, "I94Number", citizenshipDocumentObj.get("i94Number"));
	                addFieldIfNotNull(UnexpForeignPassportDocumentIDObj, "PassportNumber", citizenshipDocumentObj.get("foreignPassportOrDocumentNumber"));
	                addFieldIfNotNull(UnexpForeignPassportDocumentIDObj, "CountryOfIssuance", citizenshipDocumentObj.get("foreignPassportCountryOfIssuance"));
	                addFieldIfNotNull(UnexpForeignPassportDocumentIDObj, "SEVISID", sanitizeSevisId(citizenshipDocumentObj.get("SEVISId")));
	                addFieldIfNotNull(UnexpForeignPassportDocumentIDObj, "SEVISID", sanitizeSevisId(citizenshipDocumentObj.get("sevisId")));
	                addFieldIfNotNull(UnexpForeignPassportDocumentIDObj, "DocExpirationDate", citizenshipDocumentObj.get("documentExpirationDate"));
	                dhsIdObj.put(SSAPIntegrationConstants.UnexpForeignPassport + "DocumentID", UnexpForeignPassportDocumentIDObj);
	                break;
	                
	            case SSAPIntegrationConstants.DS2019:
	                JSONObject DS2019DocumentIDObj = new JSONObject();
	                addFieldIfNotNull(DS2019DocumentIDObj, "I94Number", citizenshipDocumentObj.get("i94Number"));
	                addFieldIfNotNull(DS2019DocumentIDObj, "SEVISID", sanitizeSevisId(citizenshipDocumentObj.get("SEVISId")));
	                addFieldIfNotNull(DS2019DocumentIDObj, "SEVISID", sanitizeSevisId(citizenshipDocumentObj.get("sevisId")));
	                addFieldIfNotNull(DS2019DocumentIDObj, "DocExpirationDate", citizenshipDocumentObj.get("documentExpirationDate"));
	                dhsIdObj.put(SSAPIntegrationConstants.DS2019 + "DocumentID", DS2019DocumentIDObj);
	                break;
	                
	            case SSAPIntegrationConstants.I20:
	                JSONObject I20DocumentIDObj = new JSONObject();
	                addFieldIfNotNull(I20DocumentIDObj, "I94Number", citizenshipDocumentObj.get("i94Number"));
	                addFieldIfNotNull(I20DocumentIDObj, "SEVISID", sanitizeSevisId(citizenshipDocumentObj.get("SEVISId")));
	                addFieldIfNotNull(I20DocumentIDObj, "SEVISID", sanitizeSevisId(citizenshipDocumentObj.get("sevisId")));
	                addFieldIfNotNull(I20DocumentIDObj, "PassportNumber", citizenshipDocumentObj.get("foreignPassportOrDocumentNumber"));
	                addFieldIfNotNull(I20DocumentIDObj, "CountryOfIssuance", citizenshipDocumentObj.get("foreignPassportCountryOfIssuance"));
	                addFieldIfNotNull(I20DocumentIDObj, "DocExpirationDate", citizenshipDocumentObj.get("documentExpirationDate"));
	                dhsIdObj.put(SSAPIntegrationConstants.I20 + "DocumentID", I20DocumentIDObj);
	                break;
	                
	            case SSAPIntegrationConstants.I797:
	                JSONObject I797DocumentIDObj = new JSONObject();
	                addFieldIfNotNull(I797DocumentIDObj, "DocOtherDesc", SSAPIntegrationConstants.I797);
	                addFieldIfNotNull(I797DocumentIDObj, "SEVISID", sanitizeSevisId(citizenshipDocumentObj.get("SEVISId")));
	                addFieldIfNotNull(I797DocumentIDObj, "SEVISID", sanitizeSevisId(citizenshipDocumentObj.get("sevisId")));
	                addFieldIfNotNull(I797DocumentIDObj, "PassportNumber", citizenshipDocumentObj.get("foreignPassportOrDocumentNumber"));
	                addFieldIfNotNull(I797DocumentIDObj, "CountryOfIssuance", citizenshipDocumentObj.get("foreignPassportCountryOfIssuance"));
	                addFieldIfNotNull(I797DocumentIDObj, "DocExpirationDate", citizenshipDocumentObj.get("documentExpirationDate"));
	                
	                if(null != citizenshipDocumentObj.get("alienNumber")) {
	                    addFieldIfNotNull(I797DocumentIDObj, "AlienNumber", citizenshipDocumentObj.get("alienNumber"));
	                    dhsIdObj.put("OtherCase1DocumentID", I797DocumentIDObj);
	                }
	                if(null != citizenshipDocumentObj.get("i94Number")) {
	                    addFieldIfNotNull(I797DocumentIDObj, "I94Number", citizenshipDocumentObj.get("i94Number"));
	                    dhsIdObj.put("OtherCase2DocumentID", I797DocumentIDObj);
	                }
	                break;
	            case SSAPIntegrationConstants.NatrOfCert:
	                JSONObject natrOfCertDocumentIDObj = new JSONObject();
	                natrOfCertDocumentIDObj.put("AlienNumber", citizenshipDocumentObj.get("alienNumber"));
	                natrOfCertDocumentIDObj.put("NaturalizationNumber", citizenshipDocumentObj.get("SEVISId"));
	                natrOfCertDocumentIDObj.put("NaturalizationNumber", citizenshipDocumentObj.get("sevisId"));
	                dhsIdObj.put(SSAPIntegrationConstants.NatrOfCert + "DocumentID", natrOfCertDocumentIDObj);
	                break;
	            case SSAPIntegrationConstants.CertOfCit:
	                JSONObject certOfCitDocumentIDObj = new JSONObject();
	                certOfCitDocumentIDObj.put("AlienNumber", citizenshipDocumentObj.get("alienNumber"));
	                certOfCitDocumentIDObj.put("CitizenshipNumber", citizenshipDocumentObj.get("SEVISId"));
	                certOfCitDocumentIDObj.put("CitizenshipNumber", citizenshipDocumentObj.get("sevisId"));
	                dhsIdObj.put(SSAPIntegrationConstants.CertOfCit + "DocumentID", certOfCitDocumentIDObj);
	                break;
	        }
        }
        return dhsIdObj;
    }
    
    private String sanitizeSevisId(Object sevisId) {
        // As per requirement of HIX-37347 to remove leading "N" from SevisId
        if(SsapIntegrationUtil.isNotNullOrBlank(sevisId)) {
            if(sevisId.toString().startsWith("N")) {
                return sevisId.toString().substring(1);
            }
            return sevisId.toString();
        } else {
            return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    private void addFieldIfNotNull(JSONObject documentObj, String fieldName, Object fieldValue) {
        if(SsapIntegrationUtil.isNotNullOrBlank(fieldValue)) {
            documentObj.put(fieldName, fieldValue.toString());
        }
    }
}
