import React from 'react';

const Tooltip = props => {
    const {
        anchor,
        content,
        header
    } = props;
    return (
        <span className='tooltip-container'>
            <span className='tooltip-anchor'>
                {anchor}
                <div className='tooltip-content fade-in'>
                    {
                        header && <span className='tooltip-header' > {header} </span>
                    }
                    <div className='tooltip-body'>
                        {content}
                    </div>
                </div>
            </span>

        </span>
    );
};

export default Tooltip;