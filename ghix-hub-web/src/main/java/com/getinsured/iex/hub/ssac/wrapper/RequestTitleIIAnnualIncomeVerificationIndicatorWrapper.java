package com.getinsured.iex.hub.ssac.wrapper;

import java.io.IOException;

import com.getinsured.iex.hub.GHIXApplicationContext;
import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Boolean;
import com.google.gson.Gson;


public class RequestTitleIIAnnualIncomeVerificationIndicatorWrapper {
	private boolean titleIIAnnualIncomeVerificationRequested;
	private Boolean titleIIAnnualIncomeVerificationRequestedIndicator;
	private static com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.ObjectFactory xsdFactory;
	//private static com.getinsured.iex.hub.ssac.extension.ObjectFactory extnFactory;
	private static Gson gson ;
	
	static{
		gson = (Gson) GHIXApplicationContext.getBean("iex_platformGson");
	}
	
	public RequestTitleIIAnnualIncomeVerificationIndicatorWrapper(Boolean titleAnnualIncomeFlagIndicator){
		this.titleIIAnnualIncomeVerificationRequestedIndicator = titleAnnualIncomeFlagIndicator;
		this.titleIIAnnualIncomeVerificationRequested = titleAnnualIncomeFlagIndicator.isValue();
		
	}
	public RequestTitleIIAnnualIncomeVerificationIndicatorWrapper() {
		xsdFactory = new com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.ObjectFactory();
		//extnFactory = new com.getinsured.iex.hub.ssac.extension.ObjectFactory();
	}
	
	public void setTitleIIAnnualIncomeVerificationRequested(boolean isVerifyFlag) {
		this.titleIIAnnualIncomeVerificationRequested = isVerifyFlag;
		if(this.titleIIAnnualIncomeVerificationRequestedIndicator == null){
			this.titleIIAnnualIncomeVerificationRequestedIndicator = xsdFactory.createBoolean();
		}
		this.titleIIAnnualIncomeVerificationRequestedIndicator.setValue(isVerifyFlag);
	}
	
	public boolean isTitleIIAnnualIncomeVerificationRequested() {
		if(this.titleIIAnnualIncomeVerificationRequestedIndicator == null){
			return false;
		}
		return this.titleIIAnnualIncomeVerificationRequested;
	}
	
	public Boolean getSystemRepresentation(){
		return this.titleIIAnnualIncomeVerificationRequestedIndicator;
	}
	
	public static RequestTitleIIAnnualIncomeVerificationIndicatorWrapper 
	fromJsonString(String jsonString) throws IOException {
		return gson.fromJson(jsonString, RequestTitleIIAnnualIncomeVerificationIndicatorWrapper.class);
		
	}
	
	/*public static void main(String[] args) throws JAXBException, IOException {
		RequestTitleIIAnnualIncomeVerificationIndicatorWrapper wrapper = fromJsonString("{\"titleIIAnnualIncomeVerificationRequested\":\"true\"}");
		SSACompositeIndividualRequestType req = extnFactory.createSSACompositeIndividualRequestType();
		req.setRequestTitleIIAnnualIncomeVerificationIndicator(wrapper.titleIIAnnualIncomeVerificationRequestedIndicator);
		JAXBElement<SSACompositeIndividualRequestType> ssaCompReq = extnFactory.createSSACompositeIndividualRequest(req);
		JAXBContext jc = JAXBContext.newInstance("com.getinsured.hub.ssac.extension");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(ssaCompReq, System.out);
	}*/
}
