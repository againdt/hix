package com.getinsured.eligibility.at.resp.si.transform;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.eligibility.at.resp.si.dto.ApplicantDetails;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.resp.si.dto.ApplicationExtension;
import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ApplicationExtensionType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ExtendedApplicantEventType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ExtendedApplicantNonQHPType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ExtendedApplicantType;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
public final class ApplicationExtensionTransformer {

	private ApplicationExtensionTransformer() {
	}

	public static void appExtensionInfo(ERPResponse erpResponse, AccountTransferRequestPayloadType incomingRequest) {

		if (incomingRequest == null || incomingRequest.getInsuranceApplication() == null || incomingRequest.getInsuranceApplication().getApplicationExtension() == null) {
			return;
		}

		final ApplicationExtensionType requestApplicationExtension = incomingRequest.getInsuranceApplication().getApplicationExtension();
		final ApplicationExtension applicationExtension = new ApplicationExtension();
		applicationExtension.setCoverageYear(requestApplicationExtension.getCoverageYear().getValue().getYear());
		if (requestApplicationExtension.getApplicationVerificationIndicator() != null) {
			applicationExtension.setVerificationIndicator(requestApplicationExtension.getApplicationVerificationIndicator().isValue());
		}

		ExtendedApplicantType extendedApplicantType;
		final int size = ReferralUtil.listSize(requestApplicationExtension.getExtendedApplicant());
		List<ApplicantDetails> extendedApplicant = new ArrayList<ApplicantDetails>();
		for (int i = 0; i < size; i++) {
			ApplicantDetails applicant = new ApplicantDetails();
			extendedApplicantType = requestApplicationExtension.getExtendedApplicant().get(i);
			applicant.setIdentificationId(extendedApplicantType.getExtendedApplicantMemberIdentification().getIdentificationID().getValue());
			if (extendedApplicantType.getExtendedApplicantVerificationIndicator() != null) {
				applicant.setVerificationIndicator(extendedApplicantType.getExtendedApplicantVerificationIndicator().isValue());
			}
			transformEvents(applicant, extendedApplicantType);
			transformQHPEvents(applicant, extendedApplicantType);
			extendedApplicant.add(applicant);
		}

		applicationExtension.setExtendedApplicant(extendedApplicant);
		erpResponse.setApplicationExtension(applicationExtension);
	}

	private static void transformQHPEvents(ApplicantDetails applicant, ExtendedApplicantType extendedApplicantType) {
		final int size = ReferralUtil.listSize(extendedApplicantType.getExtendedApplicantNonQHP());
		if (size == ReferralConstants.NONE) {
			return;
		}
		ExtendedApplicantNonQHPType extendedApplicantNonQHPType;
		List<ApplicantEvent> applicantQHPEvents = new ArrayList<ApplicantEvent>();
		for (int j = 0; j < size; j++) {
			ApplicantEvent applicantEvent = new ApplicantEvent();
			extendedApplicantNonQHPType = extendedApplicantType.getExtendedApplicantNonQHP().get(j);
			applicantEvent.setCode(extendedApplicantNonQHPType.getExtendedApplicantNonQHPCode().getValue().value());
			applicantEvent.setEventDate(TransformerHelper.extractDate(extendedApplicantNonQHPType.getExtendedApplicantNonQHPDate()));
			applicantQHPEvents.add(applicantEvent);
		}
		applicant.setApplicantEventsNonQHP(applicantQHPEvents);
	}

	private static void transformEvents(ApplicantDetails applicant, ExtendedApplicantType extendedApplicantType) {
		final int size = ReferralUtil.listSize(extendedApplicantType.getExtendedApplicantEvent());
		if (size == ReferralConstants.NONE) {
			return;
		}
		ExtendedApplicantEventType extendedApplicantEventType;
		List<ApplicantEvent> applicantEvents = new ArrayList<ApplicantEvent>();
		for (int j = 0; j < size; j++) {
			ApplicantEvent applicantEvent = new ApplicantEvent();
			extendedApplicantEventType = extendedApplicantType.getExtendedApplicantEvent().get(j);
			applicantEvent.setCode(extendedApplicantEventType.getExtendedApplicantEventCode().getValue().value());
			applicantEvent.setEventDate(TransformerHelper.extractDate(extendedApplicantEventType.getExtendedApplicantEventDate()));
			applicantEvent.setReportDate(TransformerHelper.extractDate(extendedApplicantEventType.getExtendedApplicantReportDate()));
			applicantEvents.add(applicantEvent);
		}
		applicant.setApplicantEvents(applicantEvents);
	}
}
