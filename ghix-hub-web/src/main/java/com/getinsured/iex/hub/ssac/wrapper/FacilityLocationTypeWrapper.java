package com.getinsured.iex.hub.ssac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ssac.extension.FacilityLocationType;

/**
 * Wrapper class for FacilityLocationType
 * 
 * @author Nikhil Talreja
 * @since 05-Mar-2014
 */
public class FacilityLocationTypeWrapper implements JSONAware {

	private String locationStreet;
	private String locationCityName;
	private String locationStateUSPostalServiceCode;
	private String locationPostalCode;
	
	public FacilityLocationTypeWrapper(FacilityLocationType facilityLocationType){
		
		if(facilityLocationType == null){
			return;
		}
		
		this.locationStreet = facilityLocationType.getLocationStreet();
		this.locationCityName = facilityLocationType.getLocationCityName();
		
		if(facilityLocationType.getLocationStateUSPostalServiceCode() != null){
			String state = facilityLocationType.getLocationStateUSPostalServiceCode().getValue().value();
			this.locationStateUSPostalServiceCode = state;
		}
		
		this.locationPostalCode = facilityLocationType.getLocationPostalCode();
	}

	public String getLocationStreet() {
		return locationStreet;
	}

	public String getLocationCityName() {
		return locationCityName;
	}

	public String getLocationStateUSPostalServiceCode() {
		return locationStateUSPostalServiceCode;
	}

	public String getLocationPostalCode() {
		return locationPostalCode;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("LocationStreet", this.locationStreet);
		obj.put("LocationCityName", this.locationCityName);
		obj.put("LocationStateUSPostalServiceCode", this.locationStateUSPostalServiceCode);
		obj.put("LocationPostalCode", this.locationPostalCode);
		return obj.toJSONString();
	}
	
	
}
