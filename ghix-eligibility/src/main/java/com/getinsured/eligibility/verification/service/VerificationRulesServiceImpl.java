package com.getinsured.eligibility.verification.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.verification.util.SSAPConstants;
import com.getinsured.eligibility.verification.util.SSAPConstants.VERIFICATION_TYPE;
import com.getinsured.eligibility.verification.util.SSAPUtils;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.iex.client.ResourceCreator;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.dto.SsapVerificationResource;

/**
 * Implements {@link VerificationRulesService} to perform Verification Rules
 * related operations like find, update etc.
 */
@Service("verificationRulesService")
public class VerificationRulesServiceImpl implements VerificationRulesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationRulesServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;
	@Autowired 
	ResourceCreator resourceCreator;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	
	/**
	 * @see VerificationRulesService#updateVerificationStatus(int, String)
	 * 
	 * @param personId
	 * 
	 * @param status
	 * @throws Exception
	 * 
	 */
	@Override
	public boolean updateVerificationStatus(long applicationId, long personId, String status, int rule) throws Exception {
		LOGGER.info("Update Verification Status : Starts : SSAP Applicantion ID : "+applicationId+" Person ID : "+personId+": Status : "+status+": Rule : "+rule);
		
		boolean updateStatus = false;
		SsapApplicantResource ssapApplicant;
		long vid;
		
		try {
			Map<Long,SsapApplicantResource> applicantMap = resourceCreator.getApplicants(applicationId);
			
			if(applicantMap !=null && applicantMap.containsKey(personId)){
				 ssapApplicant = applicantMap.get(personId);
				 
				 if(!checkVerificationStatus(ssapApplicant, rule)){
					 vid = getVID(ssapApplicant, rule);
					 if(vid != 0){
						 updateVerification(vid, ssapApplicant, status, rule);
					 } else {
						 vid = createVerification(ssapApplicant, status, rule);
					 }
					 ssapApplicant = setVerificationStatus(ssapApplicant, status, rule, vid);
					 resourceCreator.updateSsapApplicant(ssapApplicant);
					 updateStatus = true;
				 }
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while updating status : ", exception);
			throw exception;
		}

		LOGGER.info("Update Verification Status : Ends : SSAP Applicantion ID : "+applicationId+" : Person ID : "+personId);
		return updateStatus;
	}
	
	/**
	 * @see VerificationRulesService#updateSsapJson
	 * 
	 * @param personId
	 * 
	 * @param status
	 * @throws Exception
	 * 
	 */
	@Override
	public void updateSsapJson(long applicationId, String ssapJson) throws Exception {
		LOGGER.info("Update Verification Status : Starts : SSAP Applicantion ID : "+applicationId);
		
		try {
			SsapApplicationResource ssapApplicationResource = resourceCreator.getSsapApplicationById(applicationId);
			
			if(ssapApplicationResource != null){
				ssapApplicationResource.setApplicationData(ssapJson);
				resourceCreator.updateSsapApplication(ssapApplicationResource);
			} else{
				throw new Exception("SSAP Application does not exist.");
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while updating status : ", exception);
			throw exception;
		}

		LOGGER.info("Update Verification Status : Ends : SSAP Applicantion ID : "+applicationId);
	}
	
	/**
	 * @see VerificationRulesService#checkVerificationStatus(long, long, int)
	 * 
	 * @param applicationId
	 * @param personId
	 * @param rule
	 * 
	 * @param status
	 * @throws Exception
	 * 
	 */
	@Override
	public boolean checkVerificationStatus(long applicationId, long personId, int rule) throws Exception {
		LOGGER.info("Check Verification Status : Starts : SSAP Applicantion ID : "+applicationId+" Person ID : "+personId+": Rule : "+rule);
		
		boolean flag = false;
		SsapApplicantResource ssapApplicant;
		
		try {
			Map<Long,SsapApplicantResource> applicantMap = resourceCreator.getApplicants(applicationId);
			
			if(applicantMap !=null && applicantMap.containsKey(personId)){
				 ssapApplicant = applicantMap.get(personId);
				 
				 flag = checkVerificationStatus(ssapApplicant, rule);
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while checking verification status : ", exception);
			throw exception;
		}

		LOGGER.info("Check Verification Status : Ends : SSAP Applicantion ID : "+applicationId+" : Person ID : "+personId);
		return flag;
	}
	
	/**
	 * Updates SSAP Verification records
	 * 
	 * @param vid
	 * @param ssapApplicant
	 * @param status
	 * @param rule
	 * @return
	 */
	private void updateVerification(long vid, SsapApplicantResource ssapApplicant, String status, int rule) throws Exception{
		SsapVerificationResource ssapVerificationResource;
		
		try{
			ssapVerificationResource = resourceCreator.getSsapVerificationById(vid);
			ssapVerificationResource.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
			setVerificationStatus(ssapVerificationResource, status, rule);
			resourceCreator.updateSsapVerification(ssapVerificationResource);
		} catch(Exception exception){
			throw exception;
		}
	}
	
	/**
	 * Creates SSAP Verification records
	 * 
	 * @param ssapApplicant
	 * @param status
	 * @param rule
	 * @return
	 */
	private long createVerification(SsapApplicantResource ssapApplicant, String status, int rule) throws Exception{
		SsapVerificationResource ssapVerificationResource = new SsapVerificationResource();
		long vid = 0;
		String verificationLink;
		
		try{
			ssapVerificationResource.setCreationTimestamp(new Timestamp(new TSDate().getTime()));
			ssapVerificationResource.setSource(SSAPConstants.HUB);
			ssapVerificationResource.setSsapApplicant(SSAPUtils.getLink(ssapApplicant));
			setVerificationStatus(ssapVerificationResource, status, rule);
			verificationLink = resourceCreator.createSsapVerification(ssapVerificationResource, SSAPUtils.getLink(ssapApplicant));
			
			if(!SSAPUtils.isEmpty(verificationLink)){
				vid = SSAPUtils.getIdFromLink(verificationLink);
			}
		} catch(Exception exception){
			throw exception;
		}
		
		return vid;
	}
	
	/**
	 * Fetch SSAP Verification ID for given person and rule
	 * 
	 * @param ssapApplicant
	 * @param rule
	 * @return
	 */
	private long getVID(SsapApplicantResource ssapApplicant, int rule){
		
		long vid = 0;
		
		switch (rule) {
		case SSAPConstants.RESIDENCY_VERIFICATION_RULE:
			if(ssapApplicant.getResidencyVid()!=null){
				vid = ssapApplicant.getResidencyVid().longValue();
			}
			break;
		case SSAPConstants.SSN_VERIFICATION_RULE:
			if(ssapApplicant.getSsnVerificationVid()!=null){
				vid = ssapApplicant.getSsnVerificationVid().longValue();
			}
			break;
		case SSAPConstants.DEATH_VERIFICATION_RULE:
			if(ssapApplicant.getDeathVerificationVid()!=null){
				vid = ssapApplicant.getDeathVerificationVid().longValue();
			}
			break;
		case SSAPConstants.INCARCERATION_VERIFICATION_RULE:
			if(ssapApplicant.getIncarcerationVid()!=null){
				vid = ssapApplicant.getIncarcerationVid().longValue();
			}
			break;
		case SSAPConstants.CITIZENSHIP_VERIFICATION_RULE:
			if(ssapApplicant.getCitizenshipImmigrationVid()!=null){
				vid = ssapApplicant.getCitizenshipImmigrationVid().longValue();
			}
			break;
		case SSAPConstants.MEC_VERIFICATION_RULE:
			if(ssapApplicant.getMecVerificationVid()!=null){
				vid = ssapApplicant.getMecVerificationVid().longValue();
			}
			break;
		case SSAPConstants.VLP_VERIFICATION_RULE:
			if(ssapApplicant.getVlpVerificationVid()!=null){
				vid = ssapApplicant.getVlpVerificationVid().longValue();
			}
			break;
		case SSAPConstants.INCOME_VERIFICATION_RULE:
			if(ssapApplicant.getIncomeVerificationVid()!=null){
				vid = ssapApplicant.getIncomeVerificationVid().longValue();
			}
			break;
		default:
			break;
		}
		
		return vid;
	}

	/**
	 * Set Verification Status for given Rule
	 * 
	 * @param ssapApplicant
	 * @param status
	 * @param rule
	 * @return
	 */
	private SsapApplicantResource setVerificationStatus(SsapApplicantResource ssapApplicant, String status, int rule, long vid){
		
		switch (rule) {
		case SSAPConstants.RESIDENCY_VERIFICATION_RULE:
			ssapApplicant.setResidencyStatus(status);
			ssapApplicant.setResidencyVid(BigDecimal.valueOf(vid));
			break;
		case SSAPConstants.SSN_VERIFICATION_RULE:
			ssapApplicant.setSsnVerificationStatus(status);
			ssapApplicant.setSsnVerificationVid(BigDecimal.valueOf(vid));
			break;
		case SSAPConstants.DEATH_VERIFICATION_RULE:
			ssapApplicant.setDeathStatus(status);
			ssapApplicant.setDeathVerificationVid(BigDecimal.valueOf(vid));
			break;
		case SSAPConstants.INCARCERATION_VERIFICATION_RULE:
			ssapApplicant.setIncarcerationStatus(status);
			ssapApplicant.setIncarcerationVid(BigDecimal.valueOf(vid));
			break;
		case SSAPConstants.CITIZENSHIP_VERIFICATION_RULE:
			ssapApplicant.setCitizenshipImmigrationStatus(status);
			ssapApplicant.setCitizenshipImmigrationVid(BigDecimal.valueOf(vid));
			break;
		case SSAPConstants.MEC_VERIFICATION_RULE:
			ssapApplicant.setMecVerificationStatus(status);
			ssapApplicant.setMecVerificationVid(BigDecimal.valueOf(vid));
			break;
		case SSAPConstants.VLP_VERIFICATION_RULE:
			ssapApplicant.setVlpVerificationStatus(status);
			ssapApplicant.setVlpVerificationVid(BigDecimal.valueOf(vid));
			break;
		case SSAPConstants.INCOME_VERIFICATION_RULE:
			ssapApplicant.setIncomeVerificationStatus(status);
			ssapApplicant.setIncomeVerificationVid(BigDecimal.valueOf(vid));
			break;
		default:
			break;
		}
		
		return ssapApplicant;
	}
	
	/**
	 * Set Verification Status for given Rule
	 * 
	 * @param ssapApplicant
	 * @param status
	 * @param rule
	 * @return
	 */
	private SsapVerificationResource setVerificationStatus(SsapVerificationResource ssapVerificationResource, String status, int rule){
		
		switch (rule) {
		case SSAPConstants.RESIDENCY_VERIFICATION_RULE:
			ssapVerificationResource.setVerificationStatus(status);
			ssapVerificationResource.setVerficationResults(status);
			ssapVerificationResource.setVerificationType(VERIFICATION_TYPE.RESIDENCY.toString());
			break;
		case SSAPConstants.SSN_VERIFICATION_RULE:
			ssapVerificationResource.setVerificationStatus(status);
			ssapVerificationResource.setVerficationResults(status);
			ssapVerificationResource.setVerificationType(VERIFICATION_TYPE.SSN.toString());
			break;
		case SSAPConstants.DEATH_VERIFICATION_RULE:
			ssapVerificationResource.setVerificationStatus(status);
			ssapVerificationResource.setVerficationResults(status);
			ssapVerificationResource.setVerificationType(VERIFICATION_TYPE.DEATH.toString());
			break;
		case SSAPConstants.INCARCERATION_VERIFICATION_RULE:
			ssapVerificationResource.setVerificationStatus(status);
			ssapVerificationResource.setVerficationResults(status);
			ssapVerificationResource.setVerificationType(VERIFICATION_TYPE.INCARCERATION.toString());
			break;
		case SSAPConstants.CITIZENSHIP_VERIFICATION_RULE:
			ssapVerificationResource.setVerificationStatus(status);
			ssapVerificationResource.setVerficationResults(status);
			ssapVerificationResource.setVerificationType(VERIFICATION_TYPE.CITIZENSHIP.toString());
			break;
		case SSAPConstants.MEC_VERIFICATION_RULE:
			ssapVerificationResource.setVerificationStatus(status);
			ssapVerificationResource.setVerficationResults(status);
			ssapVerificationResource.setVerificationType(VERIFICATION_TYPE.MEC.toString());
			break;
		case SSAPConstants.VLP_VERIFICATION_RULE:
			ssapVerificationResource.setVerificationStatus(status);
			ssapVerificationResource.setVerficationResults(status);
			ssapVerificationResource.setVerificationType(VERIFICATION_TYPE.VLP.toString());
			break;
		case SSAPConstants.INCOME_VERIFICATION_RULE:
			ssapVerificationResource.setVerificationStatus(status);
			ssapVerificationResource.setVerficationResults(status);
			ssapVerificationResource.setVerificationType(VERIFICATION_TYPE.INCOME.toString());
			break;
		case SSAPConstants.EQUIFAX_VERIFICATION_RULE:
			ssapVerificationResource.setVerificationStatus(status);
			ssapVerificationResource.setVerficationResults(status);
			ssapVerificationResource.setVerificationType(VERIFICATION_TYPE.EQUIFAX.toString());
			break;
		default:
			break;
		}
		
		return ssapVerificationResource;
	}

	
	/**
	 * Set Verification Status for given Rule
	 * 
	 * @param ssapApplicant
	 * @param status
	 * @param rule
	 * @return
	 */
	private boolean checkVerificationStatus(SsapApplicantResource ssapApplicant, int rule){
		String existingStatus = "";
		boolean checkVerified = false;
		
		try {
			switch (rule) {
			case SSAPConstants.RESIDENCY_VERIFICATION_RULE:
				existingStatus = ssapApplicant.getResidencyStatus();
				break;
			case SSAPConstants.SSN_VERIFICATION_RULE:
				existingStatus = ssapApplicant.getSsnVerificationStatus();
				break;
			case SSAPConstants.DEATH_VERIFICATION_RULE:
				existingStatus = ssapApplicant.getDeathStatus();
				break;
			case SSAPConstants.INCARCERATION_VERIFICATION_RULE:
				existingStatus = ssapApplicant.getIncarcerationStatus();
				break;
			case SSAPConstants.CITIZENSHIP_VERIFICATION_RULE:
				existingStatus = ssapApplicant.getCitizenshipImmigrationStatus();				
				break;
			case SSAPConstants.MEC_VERIFICATION_RULE:
				existingStatus = ssapApplicant.getMecVerificationStatus();
				break;
			case SSAPConstants.VLP_VERIFICATION_RULE:
				existingStatus = ssapApplicant.getVlpVerificationStatus();
				break;
			case SSAPConstants.INCOME_VERIFICATION_RULE:
				existingStatus = ssapApplicant.getIncomeVerificationStatus();
				break;
			default:
				break;
			}
			
			if(!SSAPUtils.isEmpty(existingStatus) && existingStatus.equals(SSAPConstants.VERIFIED)){
				checkVerified = true;
			}
		} catch(Exception exception){
			throw exception;
		}
		
		return checkVerified;
	}
}
