<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page import="com.getinsured.hix.platform.util.PhoneUtil"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" ></script>
<div class="gutter10">
<c:set var="encIssuerId" scope="session"><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="pgheader.account"/></a></li>
            <li><a href="<c:url value='/planmgmt/managerepresentative'/>"><spring:message  code="label.manageRepresentatives"/></a></li>
        </ul>
	</div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
			</div>
			<div class="span9">
				<h1 id="skip">${issuerObj.name}</h1>
			</div>
		</div>
	</div>
<div class="row-fluid">
	<div class="span3" id="sidebar">
	         <div class="header">
              <h4 class="margin0"><spring:message  code="pgheader.issuerAbout"/></h4>
          </div>
          <!--  beginning of side bar -->
          <jsp:include page="issuerDetailsLeftNav.jsp">
        		<jsp:param name="pageName" value="issuerRep"/>
		  </jsp:include>
       	</div>
		<!-- end of span3 -->
           <div class="span9" id="rightpanel">
                <div class="header">
                   <h4 class="pull-left"><spring:message  code="label.repInfo"/></h4>
                   <%--	Commenting code while code merge for HIX-23920 
                   <a href='<c:url value="/admin/issuer/representative/edit/${issuerRepObj.id}"/>' class="btn btn-primary pull-right margin5-lr"><spring:message  code="label.editColumnHeading"/></a> --%>
                   <c:set var="encIssuerRepId" ><encryptor:enc value="${repId}" isurl="true"/> </c:set>

                   <c:if test="${STATE_CODE != 'CA' || (STATE_CODE == 'CA' && displayStatus != null && fn:toUpperCase(displayStatus) == 'INACTIVE')}">
	                   <c:choose>
						<c:when test="${fn:toUpperCase(repUser.role) == 'ISSUER_ENROLLMENT_REPRESENTATIVE'}" >
							<a href='<c:url value="/admin/issuer/enrollmentrepresentative/edit/${encIssuerRepId}"/>' class="btn btn-small btn-primary pull-right margin5-lr"><spring:message  code="label.editColumnHeading"/></a>
						</c:when>
						<c:otherwise>
							<a href='<c:url value="/admin/issuer/representative/edit/${encIssuerRepId}"/>' class="btn btn-primary btn-small pull-right margin5-lr"><spring:message  code="label.editColumnHeading"/></a>
						</c:otherwise>	
					   </c:choose>
				   </c:if>
					<a href='<c:url value="/admin/issuer/representative/manage/${encIssuerId}"/>' class="btn btn-small pull-right"><spring:message  code="label.btnCancel"/></a>
               </div>
               <div class="gutter10">
	 	 			<table class="table table-condensed table-border-none">
			 	 	 	<%-- <c:choose>
		 	 	 			<c:when test="${isFromUser eq 'USER'}">
		 					<tr>
		 						<td class="txt-right span3"><spring:message  code="label.firstName"/></td>
		 	 					<td><strong>${USER_OBJ.firstName}</strong></td>
		 					</tr>
		 					<tr>
		 						<td class="txt-right span3"><spring:message  code="label.lastName"/></td>
		 	 					<td><strong>${USER_OBJ.lastName}</strong></td>
		 					</tr>
		 					<tr>
		 						<td class="txt-right span3"><spring:message  code="label.title"/></td>
		 	 					<td><strong>${USER_OBJ.title}</strong></td>
		 					</tr>
		 					<tr>
		 						<td class="txt-right span3"><spring:message  code="label.phoneNumber"/></td>
		 						<c:set var="phnNumber" value="${USER_OBJ.phone}"/>		 						
		 	 					<td><strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("phnNumber"))%></strong></td>
		 					</tr>
		 					<tr>
		 						<td class="txt-right span3"><spring:message  code="label.emailAddress"/></td>
		 	 					<td><strong>${USER_OBJ.email}</strong></td>
		 	 				</tr>
		 	 				<c:if test="${fn:toUpperCase(repUser.role) != 'ISSUER_ENROLLMENT_REPRESENTATIVE'}">
			 	 				<tr>
			 	 					<td class="txt-right span3"><spring:message  code="label.primaryContact"/></td>
			 	 					<td><strong>${repUser.primaryContact}</strong></td>
			 	 				</tr>
			 	 			</c:if>	
		 	 	 		</c:when>
			 	 	 	<c:otherwise> --%>
			 	 	 			<tr>
			 	 	 				<td class="txt-right span3"><spring:message  code="label.firstName"/></td>
			 	 	 				<td><strong>
			 	 	 					<c:choose>
										<c:when test="${isFromUser eq 'USER'}">
											${USER_OBJ.firstName}
										</c:when>
										<c:otherwise>
											${repUser.firstName}
										</c:otherwise>
										</c:choose>
			 	 	 				</strong></td>
			 	 	 			</tr>
			 	 	 			<tr>
			 	 	 				<td class="txt-right span3"><spring:message  code="label.lastName"/></td>
			 	 	 				<td><strong>
			 	 	 				<c:choose>
										<c:when test="${isFromUser eq 'USER'}">
											${USER_OBJ.lastName}
										</c:when>
										<c:otherwise>
											${repUser.lastName}
										</c:otherwise>
										</c:choose>
			 	 	 				</strong></td>
			 	 	 			</tr>
			 	 	 			<tr>
			 	 	 				<td class="txt-right span3"><spring:message  code="label.title"/></td>
			 	 	 				<td><strong>${repUser.title}</strong></td>
			 	 	 			</tr>
			 	 	 			<tr>
			 	 	 				<td class="txt-right span3"><spring:message  code="label.phoneNumber"/></td>
			 	 	 				<c:choose>
										<c:when test="${isFromUser eq 'USER'}">
											<c:set var="phoneNum" value="${USER_OBJ.phone}"/>
										</c:when>
										<c:otherwise>
											<c:set var="phoneNum" value="${repUser.phone}"/>
										</c:otherwise>
									</c:choose>
			 	 	 				<td><strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("phoneNum"))%></strong></td>
			 	 	 			</tr>
			 	 	 			<tr>
			 	 	 				<td class="txt-right span3"><spring:message  code="label.emailAddress"/></td>
			 	 					<td><strong>
			 	 						<c:choose>
										<c:when test="${isFromUser eq 'USER'}">
											${USER_OBJ.email}
										</c:when>
										<c:otherwise>
											${repUser.email}
										</c:otherwise>
										</c:choose>
									</strong></td>
			 	 				</tr>
			 	 				<c:if test="${fn:toUpperCase(repUser.role) != 'ISSUER_ENROLLMENT_REPRESENTATIVE' && isFromUser eq 'USER'}">
				 	 				<tr>
				 	 					<td class="txt-right span3"><spring:message  code="label.primaryContact"/></td>
				 	 					<td><strong>${repUser.primaryContact}</strong></td>
				 	 				</tr>
				 	 			</c:if>	
			 	 	 	<%-- </c:otherwise>
			 	 	</c:choose> --%>
	 	 		</table>
 	 		</div>
 	 	</div><!--  end of span9 -->
	
</div><!-- end row-fluid -->
</div>
