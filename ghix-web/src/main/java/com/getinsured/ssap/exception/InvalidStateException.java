package com.getinsured.ssap.exception;

/**
 * Exception that signifies that invalid state is reached for SSAP.
 *
 * @author Yevgen Golubenko
 * @since 1/3/19
 */
public class InvalidStateException extends Exception implements SsapException
{
  private InvalidStateType type = InvalidStateType.NOT_SPECIFIED;

  /**
   * Constructs new exception with given message.
   * @param message Message text.
   */
  public InvalidStateException(String message) {
    super(message);
  }

  /**
   * Constructs new exception with given invalid state type.
   * @param type {@link InvalidStateType}.
   */
  public InvalidStateException(InvalidStateType type) {
    this.type = type;
  }

  /**
   * Constructs new exception with given message and type of invalid state.
   * @param message Any message that you want to associate with this exception.
   * @param type {@link InvalidStateType}.
   */
  public InvalidStateException(String message, InvalidStateType type) {
    super(message);
    this.type = type;
  }

  /**
   * Returns type of invalid state.
   * @return one of types defined in {@link InvalidStateType}.
   */
  public InvalidStateType getType() {
    return type;
  }

  public void setType(InvalidStateType type) {
    this.type = type;
  }
}
