package com.getinsured.eligibility.at.resp.si.handler.helper;

import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Component
public class NativeAmericanEligibilityHelper {

	@Autowired private SsapApplicationRepository ssapApplicationRepository;

	public void processEligibility(	String applicationID) {

		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumber(applicationID);
		if (!ssapApplicationList.isEmpty()){
			SsapApplication ssapApplication = ssapApplicationList.get(0);

			String hhNativeAmerican = setNativeamericanStatus(ssapApplication) ? "Y" : "N";
			ssapApplication.setNativeAmerican(hhNativeAmerican);
			ssapApplicationRepository.updateNativeAmericanEligibilityStatus(hhNativeAmerican, new Timestamp(new TSDate().getTime()), ssapApplication.getId());
			//ssapApplicationRepository.save(ssapApplication);
		} else {
			throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES + applicationID);
		}

	}
	
	public String process(String applicationID) {

		String hhNativeAmerican = null;
		
		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumber(applicationID);
		if (!ssapApplicationList.isEmpty()){
			SsapApplication ssapApplication = ssapApplicationList.get(0);

			hhNativeAmerican = setNativeamericanStatus(ssapApplication) ? "Y" : "N";
		} else {
			throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES + applicationID);
		}
		
		return hhNativeAmerican;

	}

	public boolean setNativeamericanStatus(SsapApplication ssapApplication) {

		/*boolean isHouseholdNativeAmerican = true;
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if ("Y".equals(applicant.getApplyingForCoverage())){
				if (!"Y".equals(applicant.getNativeAmericanFlag())){
					isHouseholdNativeAmerican = false;
					break;
				}

				if (!"QHP".equals(applicant.getEligibilityStatus())){
					isHouseholdNativeAmerican = false;
					break;
				}
			}

		}

		return isHouseholdNativeAmerican;*/
		

		boolean isAtleastOneNativeAmerican = false;
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if ("Y".equals(applicant.getApplyingForCoverage())){
				if ("Y".equals(applicant.getNativeAmericanFlag()) && "QHP".equals(applicant.getEligibilityStatus())){
					isAtleastOneNativeAmerican = true;
					break;
		}
            }
		}

		return isAtleastOneNativeAmerican;

	}



}
