package com.getinsured.hix.indportal.dto;

import java.util.Comparator;

/**
 * HIX-59164
 * Display Enrollment History for the Household
 * 
 * @author sahoo_s
 */
public class EnrollmentHistoryEntryComparator implements Comparator<EnrollmentHistoryEntry>{

	@Override
	public int compare(EnrollmentHistoryEntry enrollHistoryOne, EnrollmentHistoryEntry enrollHistoryTwo) {
		if (enrollHistoryOne.getCreationTimestamp().getTime() > 0
				&& enrollHistoryTwo.getCreationTimestamp().getTime() > 0) {
			return Long.valueOf(enrollHistoryTwo.getCreationTimestamp().getTime()).compareTo(
								Long.valueOf(enrollHistoryOne.getCreationTimestamp().getTime()));
		}

		return 0;
	}

}
