import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from '@cmsgov/design-system-core/dist/components/Button/Button';
import { hideModal } from '../../actions/actionCreators';

const FallbackUIModal = props => {
    const {
        modal: {
            modalOwner
        }
    } = props;

    const handleHideClick = () => {
        props.actions.hideModal(modalOwner);
    };

    return (
        <Fragment>
            <h5>
                {'Needed type of Modal has\'t been provided'} <br/>
                <Button onClick={handleHideClick}>Close modal</Button>
            </h5>
        </Fragment>
    );
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        hideModal,
    }, dispatch)
});

export default connect(null, mapDispatchToProps)(FallbackUIModal);