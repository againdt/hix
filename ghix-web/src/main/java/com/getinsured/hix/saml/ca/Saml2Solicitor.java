/**
 * 
 */
package com.getinsured.hix.saml.ca;

import java.util.UUID;

import javax.xml.datatype.XMLGregorianCalendar;

import gov.ca.calheers.saml2encode.Saml2EncodeInput;

public final class Saml2Solicitor implements Saml2EncodeInput {

	private Object itemToEncode;
	private XMLGregorianCalendar validAfterDate;
	private XMLGregorianCalendar validBeforeDate;
	private XMLGregorianCalendar authorizationDateTime;
	private String subjectId;
	private String subjectName;
	private String subjectAddress;
	private String subjectDNS;
	private String responseItemName;
	private String encodedResult;
	private String issuerName;
	private String spNameQualifier;
	private String nameQualifier;
	private String assertionDestination;
	private String audienceRestriction;
	private String responseDestination;
	private String sessionIndex;
	private UUID assertionUUID;
	private UUID responseUUID;

	/**
	 * @param assertionUUID
	 *            the assertionUUID to set
	 */
	public void setAssertionUUID(UUID assertionUUID) {
		this.assertionUUID = assertionUUID;
	}

	/**
	 * @param responseUUID
	 *            the responseUUID to set
	 */
	public void setResponseUUID(UUID responseUUID) {
		this.responseUUID = responseUUID;
	}

	@Override
	public Object getItemToEncode() {
		return itemToEncode;
	}

	@Override
	public XMLGregorianCalendar getValidAfterDate() {
		return validAfterDate;
	}

	@Override
	public XMLGregorianCalendar getValidBeforeDate() {
		return validBeforeDate;
	}

	@Override
	public XMLGregorianCalendar getAuthorizationDateTime() {
		return authorizationDateTime;
	}

	// @Override
	public String getSubjectId() {
		return subjectId;
	}

	// @Override
	public String getSubjectName() {
		return subjectName;
	}

	@Override
	public String getSubjectAddress() {
		return subjectAddress;
	}

	@Override
	public String getSubjectDNS() {
		return subjectDNS;
	}

	@Override
	public String getResponseItemName() {
		return responseItemName;
	}

	@Override
	public void setEncodedResult(String encodedResult) {
		this.encodedResult = encodedResult;
	}

	@Override
	public String getIssuerName() {
		return issuerName;
	}

	// @Override
	public String getSpNameQualifier() {
		return spNameQualifier;
	}

	// @Override
	public String getNameQualifier() {
		return nameQualifier;
	}

	// @Override
	public String getAssertionDestination() {
		return assertionDestination;
	}

	// @Override
	public String getAudienceRestriction() {
		return audienceRestriction;
	}

	// @Override
	public String getResponseDestination() {
		return responseDestination;
	}

	// @Override
	public String getSessionIndex() {
		return sessionIndex;
	}

	@Override
	public UUID getResponseUUID() {
		return responseUUID;
	}

	@Override
	public UUID getAssertionUUID() {
		return assertionUUID;
	}

	public void setItemToEncode(Object itemToEncode) {
		this.itemToEncode = itemToEncode;
	}

	public void setValidAfterDate(XMLGregorianCalendar validAfterDate) {
		this.validAfterDate = validAfterDate;
	}

	public void setValidBeforeDate(XMLGregorianCalendar validBeforeDate) {
		this.validBeforeDate = validBeforeDate;
	}

	public void setAuthorizationDateTime(XMLGregorianCalendar authorizationDateTime) {
		this.authorizationDateTime = authorizationDateTime;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public void setSubjectAddress(String subjectAddress) {
		this.subjectAddress = subjectAddress;
	}

	public void setSubjectDNS(String subjectDNS) {
		this.subjectDNS = subjectDNS;
	}

	public void setResponseItemName(String responseItemName) {
		this.responseItemName = responseItemName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public void setSpNameQualifier(String spNameQualifier) {
		this.spNameQualifier = spNameQualifier;
	}

	public void setNameQualifier(String nameQualifier) {
		this.nameQualifier = nameQualifier;
	}

	public void setAssertionDestination(String assertionDestination) {
		this.assertionDestination = assertionDestination;
	}

	public void setAudienceRestriction(String audienceRestriction) {
		this.audienceRestriction = audienceRestriction;
	}

	public void setResponseDestination(String responseDestination) {
		this.responseDestination = responseDestination;
	}

	public void setSessionIndex(String sessionIndex) {
		this.sessionIndex = sessionIndex;
	}

	public String getEncodedResult() {
		return encodedResult;
	}

}
