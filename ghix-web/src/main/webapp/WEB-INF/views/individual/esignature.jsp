<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page	import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page	import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<jsp:useBean id="now" class="java.util.Date" />

<style>
.agreement {
	background: #ebebeb;
	overflow: auto;
}
ul.inline,ol.inline {
	margin-bottom: 0;
}
.graydrkbg {
	margin-bottom: 0;
}
.form-actions {
	border-top: none;
}
#eSignature {
	border-bottom: 1px solid #bbb;
}
.esign {
	background: #f2f2f2
}
i.icon-print {
	color: inherit;
}
.agree::-webkit-scrollbar {
	width: 10px;
	height: 10px;
}

.agree::-webkit-scrollbar-track {
	background: #fff;
}

.agree::-webkit-scrollbar-thumb {
	background: #bbb;
}
</style>
<script type="text/javascript">
	window.history.forward();
	function noBack() {
		window.history.forward();
		$("#submitButton").removeAttr("disabled");
	}
</script>
<body onload="noBack();" onpageshow="if (event.persisted) noBack();"
	onunload=""></body>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript">
	function validateForm() {
		if ($("#frmesign").validate().form()) {
			if ($("input[name=terms]:checked").val() == undefined) {
				alert('<spring:message code='label.validateTerms' javaScriptEscape='true'/>');
				return false;
			} else if ($("input[name=agreement]:checked").val() == undefined
					&& $("input[name=agreement]").length > 0) {
				alert('<spring:message code='label.validateAgreement' javaScriptEscape='true'/>');
				return false;
			} else {
				$("#frmesign").submit();
				$("#submitButton").attr("disabled", true);
			}
		}
	}
</script>

<div class="gutter10">
	<div class="row-fluid">
		<div class="topnav">
			<ul class="inline">
				<li><a href="#">Personalise</a></li>
				<li><a href="#">Shop</a></li>
				<li><a href="#">Apply</a></li>
				<li class="active"><a href="#" class="last">Checkout</a></li>
			</ul>
		</div>
	</div>

	<div class="row-fluid">
		<div class="gutter10 span12">
			<form method="post" action="esignature" name="frmesign" id="frmesign">
				<input type="hidden" name="cart_id" id="cart_id" value="${cartId}" />
				<input type="hidden" name="fininfoId" id="fininfoId"
					value="${fininfoId}" /> <input type="hidden"
					name="employeeFullName" id="employeeFullName"
					value="${employeeFullName}" />

				<div class="">
					<h2>Provide eSignature</h2>
					<p>To complete the checkout process, read the agreement here
						and enter your eSignature in the space below. Entering your
						eSignature means that you are sure about the plans you selected
						and that you have read all terms and conditions. <br/>If you are
						eligible for and have chosen to use some or all of your premium
						tax credit, you must review and accept the statements related to
						premium tax credits and federal income taxes. <br/>When you click
						Enroll, the Exchange sends your information to the insurance
						company who carries your plan. You may have the option to make
						your initial payment after selecting Enroll depending on the
						insurance company for your plan. If the initial payment cannot be
						made at this time, the insurance company will contact you for
						payment and to finalize enrollment.</p>

					<h4 class="graydrkbg gutter10">
						I. Exchange Agreement <a data-original-title="" href="#"
							class="btn btn-small pull-right"
							onclick="printContent('user-agreement');"><i
							class="icon-print"></i>&nbsp;<spring:message code="enroll.print" />
						</a>
					</h4>
					<div class="agreement">
						<label for="user-agreement" class="hide"><spring:message code="enroll.esignature.exchangeAgreement"/></label>
						<div class="gutter10">
						<div class="control-group">
						    <div class="controls">
							<span class="span12 agree ms-esignature-agree" tabindex="0" id="user-agreement" readonly="readonly">I understand that I am required to submit changes that
								affect my eligibility, including income, dependency changes,
								address, and incarceration. These changes could affect the plans
								in which I can be enrolled. I cannot change plans unless I have
								a life changing event such as a marriage, birth, or a move to a
								new zip code or county. <br/>In addition, I understand that, if I
								select a Health Plan that uses mandatory binding arbitration to
								resolve disputes, I am agreeing that any dispute between myself,
								my heirs, relatives or other associated parties on the one hand
								and the Health Plan, any contracted health care providers,
								administrators or other associated parties on the other hand,
								including any claim for medical or hospital malpractice or
								relating to the coverage for, or delivery of, services or items,
								irrespective of legal theory, must be decided by binding
								arbitration and I agree to give up the right to a jury trial. I
								understand that the full arbitration provision is in the Health
								Plan's coverage document, which is available for my review.</span>
								</div>
						</div>
						</div>
						<div class="control-group gutter10 pull-left">
						<div class="controls">
							<label class="checkbox" for="terms"> <input
								type="checkbox" value="Accept Terms" name="terms" id="terms">
								<spring:message
									code="enroll.esignature.individual.agreeTermsofService" />
							</label>
							<div id="terms_error" class=""></div>
						</div>
						</div>

					</div>
					<h4 class="graydrkbg gutter10">II. Tax FilerAgreement</h4>
					<div class="agreement">
					<label for="tax-filer-agreement" class="hide"><spring:message code="enroll.esignature.exchangeAgreement"/></label>
						<div class="gutter10"><div class="control-group">
						    <div class="controls">
							<span class="span12 agree ms-esignature-agree" tabindex="0" readonly="readonly" id="tax-filer-agreement">I agree to file a (2014) Tax Return before (April 15,
								2015) to claim the Premium Tax Credit. I understand that I am
								required to submit changes that affect my eligibility, including
								income, dependency changes, address, and incarceration. These
								changes could affect the plans I can be enrolled. I cannot
								change plans unless I have a life triggering event.</span>
								</div></div>

						</div>
						<div class="control-group pull-left gutter10">
						<div class="controls">
							<label class="checkbox" for="terms"> <input
								type="checkbox" value="Accept Terms" name="terms" id="terms">
								<spring:message
									code="enroll.esignature.individual.agreeTermsofService" />
							</label>
							<div id="terms_error" class=""></div>
							</div>
						</div>
					</div>


					<div class="control-group">
						<h4 class="graydrkbg gutter10">Application Filer Signature</h4>
						<div id="app-filer-agreement" class="agreement">
							<div class="gutter10">
								<label for="app-file-sign">To provide your eSignature
									please enter your full name. <img
									src="<c:url value="/resources/images/requiredAsterix.png" />"
									width="10" height="10" alt="Required!" />
								</label> <input type="text" id="app-file-sign" name="app-file-sign"
									maxlength="50" value="" class="" />
							</div>
						</div>
					</div>
					<div class="control-group esign">
						<div class="gutter20">
							<span class="span3"><spring:message
									code="enroll.eSignature" />:</span>
							<div id="eSignature" class="controls span6"></div>
							<span class="pull-left marginL20"><spring:message
									code="enroll.date" />: &nbsp; </span>
							<fmt:formatDate value="${now}" type="both" pattern="MM/dd/yyyy" />
						</div>
						<!-- gutter10 -->
					</div>
					<div class="form-actions">
						<a href="#" class="btn"><spring:message code="enroll.back" /></a>
						<input type="button" name="submitButton" id="submitButton"
							onClick="javascript:validateForm();"
							class="btn btn-primary pull-right" value="Enroll"
							title="Enroll" />
					</div>
				</div>
				<!--rightpanel-->
			</form>
		</div>
		<!-- End Form -->
	</div>
	<!--row-fluid-->
</div>
<!-- <iframe name=print_frame width=400 height=400 frameborder=0 src=about:blank class="hide"></iframe> -->
<script type="text/javascript">
	$(document).ready(function() {
		$('#applicant_esig').bind('keyup focusout', function(event) {
			$('#eSignature').text($('#applicant_esig').val());
		});
		$('.info').tooltip();
		$('span.icon-ok').parents('li').css('list-style', 'none');
	});
</script>

<script type="text/javascript">
	jQuery.validator.addMethod("alphabetsOnly", function(value, element) {
		return /^[a-zA-Z\s]+$/i.test(value);
	});
	jQuery.validator.addMethod("fullNameOnly", function(value, element) {
		if (/\w+\s+\w+/.test(value)) {
			return true;
		} else {
			return false;
		}
	});
	jQuery.validator.addMethod("employeeFullNameOnly",
			function(value, element) {
				var employeeFullName = $('#employeeFullName').val();
				if (value.toUpperCase() == employeeFullName.toUpperCase()) {
					return true;
				} else {
					return false;
				}
			});
	var validator = $("#frmesign")
			.validate(
					{
						rules : {
							'app-file-sign' : {
								required : true,
								alphabetsOnly : true,
								fullNameOnly : true,
								employeeFullNameOnly : true
							},
							'terms' : {
								required : true,
							},
						},
						messages : {
							'app-file-sign' : {
								required : "<span><em class='excl'>!</em><spring:message code='label.validateApplicantEsig' javaScriptEscape='true'/></span>",
								alphabetsOnly : "<span><em class='excl'>!</em><spring:message code='label.validateSignature'/></span>",
								fullNameOnly : "<span><em class='excl'>!</em><spring:message code='label.validateApplicantEsig'/></span>",
								employeeFullNameOnly : "<span><em class='excl'>!</em><spring:message code='label.validateApplicantEsig'/></span>"
							},
							'terms' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.agreeTermsAndConditions' javaScriptEscape='true'/></span>"
							},

						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error")
									.attr('class', 'error');
						}
					});

	function printContent(id) {
		var str = "<html>";
		str = str + "<head></head>";
		str = str + "<body>";
		str = str + "<table align='center'>";
		str = str + "<tr><td align='center'>Exchange Agreement</td></tr>";
		str = str + "<tr><td>" + document.getElementById(id).innerHTML
				+ "</td></tr>";
		str = str + "</table>";
		str = str + "</body>";
		str = str + "</html>";

		window.frames["print_frame"].document.body.innerHTML = str;
		window.frames["print_frame"].window.focus();
		window.frames["print_frame"].window.print();
	}
</script>