<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" ></script>
<div class="gutter10">
<c:set var="encIssuerId" scope="session"><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
		<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="label.manageRepresentatives"/></li>
        </ul><!--page-breadcrumb ends-->
	</div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
			</div>
			<div class="span9">
				<h1 id="skip">${issuerObj.name}</h1>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.issuerAbout"/></h4>
                </div>
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
				  <jsp:param name="pageName" value="issuerRep"/>
				</jsp:include>
		</div>
			<!-- end of span3 -->
            <div class="span9" id="rightpanel">
           		<form class="form-horizontal" id="frmIssuerEditRep" name="frmIssuerEditRep" action="<c:url value="/admin/issuer/representative/update" />" method="POST">
           			<df:csrfToken/>
                	<input type="hidden" id="issuerRepId" name="issuerRepId" value="<encryptor:enc value="${representativeObj.id}"></encryptor:enc>"/>
                    <input type="hidden" id="primaryContactOld" name="primaryContactOld"  value="${representativeObj.primaryContact}">
                     
                     <div class="header">
                         <h4 class="pull-left"><spring:message code="label.repInfo"/></h4>
						 <input type="button" class="btn btn-primary btn-small pull-right margin5-lr margin-5" value="Save" onclick="javascript:submitForm();"/>
                         <a class="btn btn-small pull-right" href='<c:url value="/admin/issuer/representative/manage/${encIssuerId}"/>'><spring:message  code="label.btnCancel"/></a>
                     </div>
                     <div class="gutter10">
     
                      <div class="control-group">
                          <label class="control-label"><spring:message  code="label.firstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                          <div class="controls">
                            <input type="text" name="firstName" id="firstName" value="${repUser.firstName}" class="input-large" maxlength="50">
                              <c:if test="${isIssuerRepDuplicate == 'true'}">
                              <div class="error">
                                   <label class="error"><span><spring:message  code="err.duplicateRepresentative"/></span></label>
                              </div> 
                              </c:if>
                            <div id="firstName_error" class=""></div>
                          </div>
                      </div>
                      <div class="control-group">
                          <label class="control-label"><spring:message  code="label.lastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                          <div class="controls"><input type="text" name="lastName" id="lastName" value="${repUser.lastName}" class="input-large" maxlength="50">
                              <div id="lastName_error" class=""></div>
                          </div>
                      </div>
                             
                     <div class="control-group">
                         <label class="control-label"><spring:message  code="label.title"/></label>
                         <div class="controls"><input type="text" name="title" id="title" value="${representativeObj.title}" class="input-large" maxlength="15">	
                             <div id="title_error" class=""></div>
                         </div>
                     </div>
                             
					<div class="control-group">
                                 <label class="control-label"><spring:message  code="label.phoneNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
                            <input type="text" name="phone1" id="phone1" value="${phone1}" maxlength="3" class="input-mini" /> 
							<input type="text" name="phone2" id="phone2" value="${phone2}" maxlength="3" class="input-mini" /> 
							<input type="text" name="phone3" id="phone3" value="${phone3}" maxlength="4" class="input-small" /> 
                            <input type="hidden" name="phone" id="phone" class="input-large">
                            <div id="" class=""></div><div id="" class=""></div><div id="phone3_error" class=""></div>
						</div>
					</div>
		                            
					<div class="control-group">
							<label class="control-label"><spring:message  code="label.emailAddress"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<%-- <c:choose>
							<c:when test="${isFromUser eq 'USER'}">
								<div class="controls"><input type="text" name="email" id="email" value="${repUser.email}" class="input-large" readonly="true" maxlength="50">
									<div id="email_error" class=""></div>
								</div>
								</c:when>
							<c:otherwise> --%>
								<div class="controls"><input type="text" name="email" id="email" value="${repUser.email}" class="input-large" <c:if test="${representativeObj.userRecord != null}"> readonly="true"</c:if> maxlength="50">
										<div id="email_error" class=""></div>
										<c:if test="${repDuplicateEmail == 'true'}">
                                        <div class="error">
                                               <label class="error"><span><spring:message  code="err.duplicateEmail"/></span></label>
                                        </div> 
                                        </c:if>
								</div>
								
						<%-- 	</c:otherwise>
						</c:choose> --%>
					</div>
                              
                     <div class="control-group">
                         <label class="control-label"><spring:message  code="label.primaryContact"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                         <div class="controls">
	                         <select class="input-small" name="primaryContact" id="primaryContact" <c:if test="${representativeObj.userRecord == null || representativeObj.status == 'INACTIVE'}"> disabled="true"</c:if> onchange="validatePrimaryContact(this.value,'${primaryContact}');">
		                       	<c:forEach var="primaryContactObj" items="${primaryContactList}">
		    						<option <c:if test="${primaryContactObj == primaryContact}"> SELECTED </c:if> value="${primaryContactObj}">${primaryContactObj}</option>
								</c:forEach>
	   						</select>
	    					<div class="error" id="primaryContact_error" style="display:none">
								<label class="error"><span><em class='excl'>!</em><spring:message  code="err.isIssuerHasPrimaryContact"/></span></label>
							</div> 
                        </div>
                     </div>
                             
						<c:if test="${STATE_CODE != null && STATE_CODE == 'CA'}">
						<div class="control-group">
                       		<label for="addressLine1" class="required control-label"><spring:message  code="label.streetaddress1"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="location.address1" id="addressLine1" class="input-xlarge" value="${representativeObj.location.address1}" maxlength="25">
                                <div id="addressLine1_error" class=""></div>
                            </div> <!-- end of controls-->
	                    </div><!-- end of control-group -->
	                    
	                    <div class="control-group">
	                        <label for="addressLine2" class="required control-label"><spring:message  code="label.streetaddress2"/></label>
	                            <div class="controls">
	                                <input type="text" name="location.address2" id="addressLine2" class="input-xlarge" value="${representativeObj.location.address2}" maxlength="25">
	                                <div id="addressLine2_error" class=""></div>
	                            </div> <!-- end of controls-->
	                    </div><!-- end of control-group -->
	                    
	                    <div class="control-group">
	                        <label for="city" class="required control-label"><spring:message  code="label.city"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                            <div class="controls">
	                                <input type="text" name="location.city" id="city" class="input-xlarge" value="${representativeObj.location.city}" maxlength="15">
	                                <div id="city_error" class=""></div>
	                            </div> <!-- end of controls-->
	                    </div><!-- end of control-group -->
	                    
	                    <div class="control-group">
                           <label for="state" class="required control-label"><spring:message  code="label.emplState"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                           <div class="controls">
                               <select size="1"  id="state" name="location.state" path="statelist">
                                    <option value="">Select</option>
                                    <c:forEach var="state" items="${statelist}">
                                       <option <c:if test="${state.code == representativeObj.location.state}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
                                   </c:forEach>
                               </select>
                               <div id="state_error"></div>
                           </div>
                       </div><!-- end of control-group -->                        
                       
                        <div class="control-group">
                            <label for="zip" class="required control-label"><spring:message  code="label.zipCode"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                <div class="controls">
                                    <input type="text" name="location.zip" id="zip" class="input-small" maxlength="5" value="${representativeObj.location.zip}">
                                    <div id="zip_error" class=""></div>
                                </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                     </c:if>
                     						    
                    </div>
                </form>	
        	</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>
<script type="text/javascript">

function validatePrimaryContact(seletedValue, actualValue){
	
	//HIX-23046:Some confirmation message should be displayed to user when user changes the Primary Contact from NO to YES. 
	if(seletedValue.toUpperCase() == "YES" && actualValue.toUpperCase() == "NO"){
		var didConfirm = confirm("This will make current contact as primary and will remove primary setting of other contact. Is it ok?");		
		if (didConfirm == false) {
			document.getElementById("primaryContact").value='NO';
			return false;
		}		
	}
	
	document.getElementById("primaryContact_error").style.display="none";
	if((actualValue.toUpperCase() != "NO") && (seletedValue.toUpperCase() == "NO")){
		document.getElementById("primaryContact_error").style.display="block";
	}
}

function submitForm(){
	if(!(($("#primaryContactOld").val()).toUpperCase() != "NO" &&	($("#primaryContact").val()).toUpperCase() == "NO")){
		$("#frmIssuerEditRep").submit();
	}
}  




var validator = $("#frmIssuerEditRep").validate({ 
	rules : {
		firstName : {required : true, validFirstNameCheck : true},
		lastName : {required : true, validLastNameCheck : true},
		phone1 : {required : true, digits: true},
		phone2 : {required : true, digits: true},
		phone3 : {required : true, digits: true, minlength:4, maxlength:4, phoneUS:true},
		email : { required : true, emailIdCheck: true},
		"location.address1" : { required : true},
		"location.city" : {required : true},
		"location.state" : { required : true},
		"location.zip" : { required : true, digits: true, minlength:5, maxlength:5}
	},
	messages : {
		firstName : { required : "<span><em class='excl'>!</em><spring:message  code='err.firstName' javaScriptEscape='true'/></span>",
					  validFirstNameCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstNameCheck' javaScriptEscape='true'/></span>"},
		lastName: { required : "<span><em class='excl'>!</em><spring:message  code='err.lastName' javaScriptEscape='true'/></span>" ,
			validLastNameCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateLastNameCheck' javaScriptEscape='true'/></span>"},
		phone3 : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
			digits : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
          minlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
          maxlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>"
  		},
		email : { required : "<span><em class='excl'>!</em><spring:message  code='err.email' javaScriptEscape='true'/></span>",
				  emailIdCheck : "<span><em class='excl'>!</em><spring:message  code='err.invalidEmail' javaScriptEscape='true'/></span>"},
		"location.address1" : { required : "<span><em class='excl'>!</em><spring:message  code='err.addressLine1' javaScriptEscape='true'/></span>"},
        "location.city" : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerCity' javaScriptEscape='true'/></span>"},
        "location.state": { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerState' javaScriptEscape='true'/></span>"},
        "location.zip" : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerZip' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error');
	} 
});


//phone number validation
$.validator.addMethod("phoneUS", function(value, element, param) {
	if($.browser.msie==true && $.browser.version=='8.0'){
		phoneNumber1 = $("#phone1").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber2 = $("#phone2").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber3 = $("#phone3").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}else{
		phoneNumber1 = $("#phone1").val().trim();
		phoneNumber2 = $("#phone2").val().trim();
		phoneNumber3 = $("#phone3").val().trim();
	}
	var phoneNumber=phoneNumber1+phoneNumber2+phoneNumber3;
	
	if(!isNaN(phoneNumber1) && !isNaN(phoneNumber2) && !isNaN(phoneNumber3) && phoneNumber.length==10){
		$("#phone").val(phoneNumber1+phoneNumber2+phoneNumber3);
		return true;
	}else{
		if(isNaN(phoneNumber1) || phoneNumber1.length!=3){
			$('#phone1').removeClass('input-mini valid').addClass('input-small error');
		}else{
			$('#phone1').removeClass('input-mini error').addClass('input-small valid');
		}
		if(isNaN(phoneNumber2) || phoneNumber2.length!=3){
			$('#phone2').removeClass('input-mini valid').addClass('input-small error');
		}else{
			$('#phone2').removeClass('input-mini error').addClass('input-small valid');
		}
		if(isNaN(phoneNumber3) || phoneNumber3.length!=4){
			$('#phone3').removeClass('input-mini valid').addClass('input-small error');
		}else{
			$('#phone3').removeClass('input-mini error').addClass('input-small valid');
		}
		return false;
	}	
},"<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>");

$.validator.addMethod("emailIdCheck", function(value, element) {
	//var emailRegEx = new RegExp("^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$");
	var emailRegEx = new RegExp("^[_A-Za-z0-9\u002e_]+([A-Za-z0-9\u002e]+)@[A-Za-z0-9]+([\u002e][a-zA-Z]{2,})+$");
	if("" != value && !emailRegEx.test(value)){
		return false;
	}else{
		return true;
	}
});

jQuery.validator.addMethod("validFirstNameCheck", function(value,
		element, param) {
	fName = $("#firstName").val();
	  var asciiReg = /^[a-zA-Z0-9]+$/;
	  if( !asciiReg.test(fName ) ) {
		return false;
	  } else {
		return true;
	  }
	return false;
});	

jQuery.validator.addMethod("validLastNameCheck", function(value,
		element, param) {
	lName = $("#lastName").val();
	 var asciiReg = /^[a-zA-Z0-9]+$/;
	  if( !asciiReg.test(lName ) ) {
		return false;
	  } else {
		return true;
	  }
	return false;
});
</script>		
