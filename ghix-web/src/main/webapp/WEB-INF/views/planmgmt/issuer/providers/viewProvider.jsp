<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>

<div class="gutter10">
<div class="row-fluid">
	<h1> ${issuerObj.name}</h1>
</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message code="label.providerNetworks"/></h4>
                </div>
                                
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
             	<c:choose>
						<c:when test="${fn:length(networkObj) > 0}">
							<table class="table table-condensed table-border-none table-striped">
								<thead>
									<tr class="graydrkbg">
										<spring:message code="label.networkName" var="networkName"/>
										<spring:message code="label.netwotkType" var="netwotkType"/>
										<spring:message code="label.createdOn" var="createdOn"/>							
								 		<th scope="col" class="sortable"><dl:sort title="${networkName}" sortBy="name" sortOrder="${sortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="${netwotkType}" sortBy="type" sortOrder="${sortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="${createdOn}" sortBy="creationTimestamp" sortOrder="${sortOrder}"></dl:sort></th>
									</tr>
								</thead>
								<c:forEach items="${networkObj}" var="networkObj">
									<tr>
										<td id="name_${networkObj.id}">${networkObj.name}</td>
										<td align="center">${networkObj.type} </td>
										<td align="center"> <fmt:formatDate type="both" pattern="MMM dd, yyyy" value="${networkObj.creationTimestamp}" /></td>
									</tr>
								
								</c:forEach>
							</table>
							<div class="center"><dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/></div>
						</c:when>
					<c:otherwise>
						<div class="alert alert-info"><spring:message code="label.searchResult"/></div>
					</c:otherwise>
				</c:choose>
           </div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>
<script type="text/javascript">
	$('.ttclass').tooltip();
</script>
