package com.getinsured.eligibility.at.resp.service;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.iex.ssap.model.AptcHistory;
import com.getinsured.iex.ssap.repository.AptcHistoryRepository;

@Service("aptcHistoryService")
@Transactional
public class AptcHistoryServiceImpl implements AptcHistoryService {


	@Autowired private AptcHistoryRepository aptcHistoryRepository;

	
	@Override
	public boolean updateEffectiveDate(Date effectiveDate, Long ssapApplicationId) {
		boolean isUpdated = false;
		final AptcHistory aptcHistory = aptcHistoryRepository.findAptcHistoryBySsapApplicationId(ssapApplicationId);
		if(aptcHistory!=null) {
			aptcHistory.setEffectiveDate(effectiveDate);
			aptcHistoryRepository.save(aptcHistory);
	        return true;
		}
		return isUpdated;
	}

	@Override
	public boolean updateCMRId(BigDecimal cmrHouseholdId, Integer ssapApplicationId) {
		boolean isUpdated = false;
		final AptcHistory aptcHistory = aptcHistoryRepository.findAptcHistoryBySsapApplicationId(ssapApplicationId);
		if(aptcHistory!=null) {
			aptcHistory.setCmrHouseholdId(cmrHouseholdId);
			aptcHistoryRepository.save(aptcHistory);
			isUpdated = true;
		}
		return isUpdated;
	}

	@Override
	public AptcHistory getLatestByApplicationId(Long ssapApplicationId) {
		return aptcHistoryRepository.findAptcHistoryBySsapApplicationId(ssapApplicationId);
	}

}
