import { DataKeepService } from './services/data-keep.service';
import { Component, LOCALE_ID, Inject, ViewEncapsulation } from '@angular/core';
import { getParameterByName } from "./services/utility";
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-root',

    template: `<!---->
<!--<h1 i18n>Hello World!</h1>
<ng-template ngFor let-lang [ngForOf]="languages">
    <span *ngIf="lang.code !== localeId">
            <a href="/{{lang.code}}/">{{lang.label}}</a> </span>
    <span *ngIf="lang.code === localeId">{{lang.label}} </span>
</ng-template>-->
    <router-outlet></router-outlet>
<!---->`,
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    constructor(
        private dataKeep: DataKeepService,
        private translate: TranslateService,
    ) {
        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('en');

        // the lang to use, if the lang isn't available, it will use the current loader to get them
    }

    ngOnInit() {

        const lang = getParameterByName("lang");
        if (!!lang) {
            this.translate.use(lang);
        }
        this.dataKeep.shoppingId$.next(getParameterByName("householdId"));
    }
}
