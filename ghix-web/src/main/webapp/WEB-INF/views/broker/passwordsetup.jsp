<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

	<script type="text/javascript">
	
	jQuery.validator.addMethod("passwordcheck", function(value, element, param) {
		 password   = $("#password").val(); 
		 confirmed = $("#confirmPassword").val(); 
		 
		if( password != confirmed ){ return false; }
		return true;
	});
	
	
	jQuery.validator.addMethod("validatepassword", function(value, element, param) {
		password   = $("#password").val(); 
	  
		var re =  /^[A-Za-z0-9!@#$%^&*()_]{8,20}$/;
			   
		if(!re.test(password)){ return false; }
		return true;
		
	});
	
	</script>
<div class="gutter10">
	<div class="row-fluid">
		<h1>Complete Your Registration</h1>
	</div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<!--  beginning of side bar -->
				<ul class="nav nav-list">
					<li class="nav-header">
						Registration
					</li>
				</ul>
			<!-- end of side bar -->
		</div><!-- end of span3 -->

		<div class="span9" id="rightpanel">
				<p>Please select a password to complete the registration process.</p>
			<form class="form-stacked" id="frmbrokerpassword" name="frmbrokerpassword" action="passwordsetup" method="POST">
			<df:csrfToken/>
				<div class="gutter10">
					<div class="control-group">								 
						<label for="password" class="required control-label"><spring:message  code="label.brkPassword"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="password" name="password" id="password" value="" class="input-xlarge" size="30" autocomplete="off" />
							<div id="password_error" class=""></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->

					<div class="control-group">
						<label for="confirmPassword" class="required control-label"><spring:message code="label.brkConfirmPassword"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="password" name="confirmPassword" id="confirmPassword" value="" class="input-xlarge" size="30" autocomplete="off" />
							<div id="confirmPassword_error" class=""></div>
							<input type="hidden" name="id" id="id"  value="${userId}" />
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->

					<div class="form-actions">
						<input type="submit" name="submit" id="submit" value="<spring:message  code='label.saveBtn'/>" class="btn btn-primary" title="<spring:message  code='label.saveBtn'/>"/> 
					</div>
				</div><!--  .propfile -->
			</form>
		</div><!-- end of span9-->
	</div><!-- end of row-fluid -->

	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">
				<p>This information is required to determine initial eligibility to use the SHOP Exchange. All fields are subjected to a basic format validation of the input (e.g. a zip code must be a 5 digit number). The question mark icon which opens a light-box provides helpful information. The EIN number can be validated with an external data source if an adequate web API is available.</p>
			</div>
		</div>
	</div><!-- end of .notes -->
</div>
	<script type="text/javascript">
	var validator = $("#frmbrokerpassword").validate({
		rules : {password : { required : true,validatepassword:true },
			confirmPassword : { required : true, passwordcheck : true}},
			messages : {
				password: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePassword' javaScriptEscape='true'/></span>",
					validatepassword : "<span> <em class='excl'>!</em><spring:message code='label.validatePasswordLength' javaScriptEscape='true'/></span>"},
				confirmPassword: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePasswordConfirm' javaScriptEscape='true'/></span>",
					passwordcheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePasswordMatch' javaScriptEscape='true'/></span>"
			}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		} 
	});
	</script>
