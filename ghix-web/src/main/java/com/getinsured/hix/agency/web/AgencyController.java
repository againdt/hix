package com.getinsured.hix.agency.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.hix.agency.utils.AgencyUtils;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.agency.AgencyAssistantApprovalDto;
import com.getinsured.hix.dto.agency.AgencyAssistantInfoDto;
import com.getinsured.hix.dto.agency.AgencyAssistantSearchDto;
import com.getinsured.hix.dto.agency.AgencyAssistantStatusDto;
import com.getinsured.hix.dto.agency.AgencyBookOfBusinessExportDTO;
import com.getinsured.hix.dto.agency.AgencyBookOfBusinessExportResponseDTO;
import com.getinsured.hix.dto.agency.AgencyBrokerListDTO;
import com.getinsured.hix.dto.agency.AgencyCertificationStatusDto;
import com.getinsured.hix.dto.agency.AgencyDocumentDto;
import com.getinsured.hix.dto.agency.AgencyInformationDto;
import com.getinsured.hix.dto.agency.AgencyResponse;
import com.getinsured.hix.dto.agency.AgentBulkTransferDTO;
import com.getinsured.hix.dto.agency.IndividualPlanDetailsDTO;
import com.getinsured.hix.dto.agency.SearchAgencyBookOfBusinessDTO;
import com.getinsured.hix.dto.agency.SearchAgencyDelegationDTO;
import com.getinsured.hix.dto.agency.SearchBrokerDTO;
import com.getinsured.hix.dto.agency.SearchIndividualPlanDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.planmgmt.dto.PlanLightResponseDTO;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.platform.config.AEEConfiguration;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;

/**
 * Handles requests for the agency application home page.
 */
@Controller
@RequestMapping("/agency")
public class AgencyController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AgencyController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired
	private AgencyUtils agencyUtils;
	
	@Autowired
	GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired
	private BrokerService brokerService;
	
	@Autowired
	private PlanMgmtService planMgmtService;
	
	private static final String DESIGNATED_BROKER_PROFILE = "designatedBrokerProfile";
	private static final String DOLLAR_SYMBOL = "$";
	private static final String SWITCH_TO_MODULE_ID = "switchToModuleId";
	private static final String IS_USER_SWITCH_TO_OTHER_VIEW = "isUserSwitchToOtherView";
	private static final String SWITCH_TO_MODULE_NAME = "switchToModuleName";
	
	
	@Value("#{configProp['agency.agentBulkTransferAndExportUrl']}")
	private String agentBulkTransferAndExportUrl;

	/**
	* Initial URL for Agency
	*
	* @param model
	*            {@link Model}
	* @param request
	*            {@link HttpServletRequest}
	* @return URL for Agency post registration navigation
	*/
	@RequestMapping(value = "/registrationconfirm")
	public String agencyRegistrationConfirm(HttpServletRequest request){

	LOGGER.info("agencyRegistrationConfirm : START");
	String agencyId=null;
	 
	try{
		// Enable the menu
		
		userService.enableRoleMenu(request, "AGENCY_MANAGER");
		String agencyPortalEnable = DynamicPropertiesUtil.getPropertyValue(AEEConfiguration.AEEConfigurationEnum.AGENCY_PORTAL_ENABLE);
		AccountUser user = userService.getLoggedInUser();
		
		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE) && agencyPortalEnable.equalsIgnoreCase("Y") && "COUCH".equalsIgnoreCase(AgencyUtils.ECMTYPE)){
			//invoke IND65
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AgencyResponse.class);
			String response = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/ind65", String.class);
			AgencyResponse agencyResponse = reader.readValue(response);
			user.setRecordId(agencyResponse.getId());
		}
	}catch (Exception ex) {
		LOGGER.error("Exception occured while loading agency information - "+ agencyId,ex);
		throw new GIRuntimeException("Exception occured while loading agency information  - "+agencyId,ex);
	}
	LOGGER.info("agencyRegistrationConfirm : END");

	return "redirect:/agency";
	}
	
	@RequestMapping(value = "/dashboard")
	public String viewStaffDashboard(HttpServletRequest request){

	LOGGER.info("agencyRegistrationConfirm : START");
	String agencyId=null;
	 
	try{
		AccountUser user = userService.getLoggedInUser();
		if(userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L1)){
			userService.enableRoleMenu(request, "APPROVEDADMINSTAFFL1");
		}else{
			userService.enableRoleMenu(request, "APPROVEDADMINSTAFFL2");
		}
		String agencyPortalEnable = DynamicPropertiesUtil.getPropertyValue(AEEConfiguration.AEEConfigurationEnum.AGENCY_PORTAL_ENABLE);
		
		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE) && agencyPortalEnable.equalsIgnoreCase("Y") && "COUCH".equalsIgnoreCase(AgencyUtils.ECMTYPE)){
			AgencyResponse agencyResponse = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/getstaffbypersonalemail/", AgencyResponse.class);
			if(agencyResponse!=null && "200".equals(agencyResponse.getStatusCode())){
				user.setRecordId(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyResponse.getId()));
			} else {
				throw new GIRuntimeException("Account email must be same as staff's personal email address");
			}
		}
	}catch (Exception ex) {
		LOGGER.error("Exception occured while loading agency information - "+ agencyId,ex);
		throw new GIRuntimeException("Exception occured while loading agency information  - "+agencyId,ex);
	}
	LOGGER.info("agencyRegistrationConfirm : END");

	return "redirect:/agency";
	}
	
	/**
	 * Display Agency landing page
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to agency information page
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String agencylanding(Model model, HttpServletRequest request) throws GIRuntimeException {
		LOGGER.info("agency landing page : START");
		AgencyInformationDto agencyInformationDto = null;
		try{
			AccountUser user = userService.getLoggedInUser();
			Broker broker = null;
			String agencyPortalEnable = DynamicPropertiesUtil.getPropertyValue(AEEConfiguration.AEEConfigurationEnum.AGENCY_PORTAL_ENABLE);
			if(agencyPortalEnable.equalsIgnoreCase("Y")){
				if(agencyUtils.isUserSwitch(request)){
					int agencyId = Integer.parseInt((String) request.getSession().getAttribute(SWITCH_TO_MODULE_ID));
					
					request.getSession().setAttribute("landingAgencyId",  ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(agencyId)));
					agencyInformationDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+agencyId, AgencyInformationDto.class);
					if(agencyInformationDto != null && agencyInformationDto.getCertificationStatus() != null){
						request.getSession().setAttribute("agencyCertificationStatus", agencyInformationDto.getCertificationStatus().toString());
					} else{
						request.getSession().setAttribute("agencyCertificationStatus", null);
					}
					request.getSession().setAttribute("adminSwitchToAgency",  userService.hasUserRole(user, RoleService.BROKER_ADMIN_ROLE));
					request.getSession().setAttribute("currentUserRole",  RoleService.AGENCY_MANAGER);
				} else if((userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L1) 
						|| userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2)) && (!StringUtils.isBlank(user.getRecordId()))){
					AgencyAssistantInfoDto agencyAssistantInfoDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/information/"+user.getRecordId(), AgencyAssistantInfoDto.class);
					if(agencyAssistantInfoDto!=null){
						AgencyResponse response = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/linkusertostaff",agencyAssistantInfoDto,AgencyResponse.class);
						if(response!=null && "200".equals(response.getStatusCode())){
							request.getSession().setAttribute("landingAgencyId",  agencyAssistantInfoDto.getAgencyId());
							agencyInformationDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantInfoDto.getAgencyId()), AgencyInformationDto.class);
							if(agencyInformationDto != null && agencyInformationDto.getCertificationStatus() != null){
								request.getSession().setAttribute("agencyCertificationStatus", agencyInformationDto.getCertificationStatus().toString());
							} else{
								request.getSession().setAttribute("agencyCertificationStatus", null);
							}
							if(userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L1)){
								request.getSession().setAttribute("currentUserRole",  RoleService.APPROVED_ADMIN_STAFF_L1);
							}else{
								request.getSession().setAttribute("currentUserRole",  RoleService.APPROVED_ADMIN_STAFF_L2);
							}
							request.getSession().setAttribute("adminStaffApprovalStatus", agencyAssistantInfoDto.getApprovalStatus().toString());
							request.getSession().setAttribute("adminStaffActivityStatus", agencyAssistantInfoDto.getStatus());
						}
					}
				} else {
					if (!StringUtils.isBlank(user.getRecordId())) {
						broker = brokerService.getBrokerDetail(Integer.valueOf(user.getRecordId()));
						//if No user linked to Agent
						if (userService.hasUserRole(user, RoleService.AGENCY_MANAGER)) {
							if(broker.getUser() == null && user != null){
								broker.setUser(user);
								broker = brokerService.saveAgencyManagerUser(broker);
							}
						}
					} else if(user.getActiveModuleId()!=0){
						broker = brokerService.getBrokerDetail(user.getActiveModuleId());
					} else{
						broker = brokerService.findBrokerByUserId(user.getId());
					}
					request.getSession().setAttribute(DESIGNATED_BROKER_PROFILE, broker);
					if(broker!=null){
						request.getSession().setAttribute("landingAgencyId",  ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(broker.getAgencyId())));
						if (userService.hasUserRole(user, RoleService.AGENCY_MANAGER)) {
							Validate.notNull(user);
							agencyInformationDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+broker.getAgencyId(), AgencyInformationDto.class);
							if(agencyInformationDto != null && agencyInformationDto.getCertificationStatus() != null){
								request.getSession().setAttribute("agencyCertificationStatus", agencyInformationDto.getCertificationStatus().toString());
							} else{
								request.getSession().setAttribute("agencyCertificationStatus", null);
							}
							request.getSession().setAttribute("agencyManagerCertificationStatus", broker.getCertificationStatus());
						}
					}
					request.getSession().setAttribute("loggedInRole",userService.hasUserRole(user, RoleService.AGENCY_MANAGER));
					if(userService.hasUserRole(user, RoleService.AGENCY_MANAGER)) {
						request.getSession().setAttribute("currentUserRole",  RoleService.AGENCY_MANAGER);
					}
				}
			}
			setModelAttributes(user, model, request);
		}catch (Exception ex) {
			LOGGER.error("Exception occured while loading agencylanding - "+ ex);
		}
		
		return "/agency/agencyinformation";
}
	
	/**
	 * Display Agency information page
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to agency information page
	 */
	@ResponseBody
	@RequestMapping(value = {"/information/view","/information/view/{agencyId}"}, method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'VIEW_AGENCY_INFORMATION')")
	public String viewAgencyInformation(@PathVariable("agencyId") Optional<String> agencyIdEncrypted, HttpServletRequest request){
		String agencyId = null;
		String userModuleUser = "";
		try {
			AccountUser user = userService.getLoggedInUser();
			agencyId = (String) request.getSession().getAttribute("landingAgencyId");
			if(agencyId!=null && !agencyId.isEmpty()){
				agencyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyId);
			}if(agencyIdEncrypted.isPresent() && !agencyIdEncrypted.get().isEmpty() ){
				agencyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyIdEncrypted.get());
			} else if(user!=null && user.getActiveModuleId()!=0){
				Broker broker = brokerService.getBrokerDetail(user.getActiveModuleId());
				agencyId = String.valueOf(broker.getAgencyId());
			} else if(user.getRecordId() != null){
				agencyId = String.valueOf(agencyUtils.retrieveAgencyMgrForBrokerId(user));
			} else{
				return JacksonUtils.getJacksonObjectWriter(AgencyInformationDto.class).writeValueAsString(new AgencyInformationDto());
			}
			request.getSession().setAttribute("loggedInRole",userService.hasUserRole(user, RoleService.AGENCY_MANAGER));
			return ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+agencyId+userModuleUser, String.class);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching agency information - "+agencyId,ex);
			throw new GIRuntimeException("Exception occured while processing agency information  - "+agencyId,ex);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = {"/information"}, method = RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'EDIT_AGENCY_INFORMATION')")
	public String updateAgencyInformation(@RequestBody AgencyInformationDto agencyInformationDto, HttpServletRequest request) {
		
		try {
			String response = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information",agencyInformationDto,String.class);
			agencyUtils.setModuleUserForAgency();
			if(request.getSession().getAttribute("landingAgencyId")!=null){
				request.getSession().setAttribute("landingAgencyId", agencyInformationDto.getId());
			}
			
			return response;
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching agency information - "+agencyInformationDto.getId(),ex);
			throw new GIRuntimeException("Exception occured while processing agency information  - "+agencyInformationDto.getId(),ex);
		}
	}
	
	
	@ResponseBody
	@RequestMapping(value = {"/site/list", "/site/list/{agencyId}"}, method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'VIEW_AGENCY_SITE_LIST')")
	public ResponseEntity<?> viewSiteList(@PathVariable("agencyId") Optional<String> agencyIdEncrypted, HttpServletRequest request) {
		String agencyId = null;
		try {
			if(agencyIdEncrypted.isPresent() && !agencyIdEncrypted.get().isEmpty() ){
				agencyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyIdEncrypted.get());
			}else{
			agencyId = agencyUtils.getAgencyId(request);
			}
			
			String response = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/site/list/"+agencyId,String.class);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching site information - "+agencyId,ex);
			return new ResponseEntity<>("Exception occured while fetching site information", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = {"/site"}, method = RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'EDIT_AGENCY_SITE')")
	public ResponseEntity<?> updateSite(@RequestBody String request) {
	
		try {
			String response = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/site",request,String.class);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while saving agency site information.",ex);
			return new ResponseEntity<>("Unexpected error occured while saving information.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = {"/site/view/{siteId}"}, method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'VIEW_AGENCY_SITE')")
	public ResponseEntity<?> viewSite(@PathVariable("siteId") String siteId) {
		try {
			siteId = ghixJasyptEncrytorUtil.decryptStringByJasypt(siteId);
			String response = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/site/view/"+siteId,String.class);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching site information - "+siteId,ex);
			return new ResponseEntity<>("Exception occured while fetching site information", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = {"/document/{documentId}"}, method = RequestMethod.GET,produces=MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@PreAuthorize("hasPermission(#model, 'VIEW_AGENCY_DOCUMENT')")
	public void viewDocument(@PathVariable("documentId") String documentId, HttpServletResponse response) throws IOException{
		try {
			AgencyDocumentDto agencyDocumentDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/document/"+ghixJasyptEncrytorUtil.decryptStringByJasypt(documentId), AgencyDocumentDto.class);
			if (agencyDocumentDto != null) {
				response.setContentType(agencyDocumentDto.getDocumentMime().replaceAll("[\n\r]", ""));

				response.addHeader("Content-Disposition",
						"attachment; filename=" + agencyDocumentDto.getDocumentName().replaceAll("[\n\r]", ""));
				FileCopyUtils.copy(agencyDocumentDto.getDocument(), response.getOutputStream());
			} 
		} catch (Exception ex) {
			FileCopyUtils.copy("Techinal Error".getBytes(), response.getOutputStream());
			LOGGER.error("Exception occured while retrieving agency document - "+documentId,ex);
			throw new GIRuntimeException("Exception occured while retrieving agency document - "+documentId,ex);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = {"/document/upload"}, method = RequestMethod.POST,consumes=MediaType.MULTIPART_FORM_DATA_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'UPLOAD_AGENCY_DOCUMENT')")
	public String uploadDocument(@RequestParam(value = "document") MultipartFile file,
			@RequestParam("agencyId") String agencyId, @RequestParam("documentType") String documentType) throws JsonProcessingException {
		AgencyDocumentDto agencyDocumentDto = new AgencyDocumentDto();
		
		try {
			agencyDocumentDto.setAgencyId(Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyId)));
			agencyDocumentDto.setDocument(file.getBytes());
			agencyDocumentDto.setDocumentType(documentType);
			agencyDocumentDto.setOriginalFilename(file.getOriginalFilename());
			agencyDocumentDto = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/document/upload",agencyDocumentDto,AgencyDocumentDto.class);
		} catch (Exception ex) {
			agencyDocumentDto.setStatus(BrokerConstants.RESPONSE_FAILURE);
			LOGGER.error("Exception occured while uploading agency document - "+agencyId,ex);
			throw new GIRuntimeException("Exception occured while uploading agency document - "+agencyId,ex);
		}

		return JacksonUtils.getJacksonObjectWriter(AgencyDocumentDto.class).writeValueAsString(agencyDocumentDto);
	}
	
	@ResponseBody
	@RequestMapping(value = {"/documents","/documents/{agencyId}"}, method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'VIEW_AGENCY_DOC_LIST')")
	public String getDocuments(@PathVariable("agencyId")  Optional<String> agencyIdEncrypted, HttpServletRequest request){
		String agencyId = null;
		try {
			if(agencyIdEncrypted.isPresent() && !agencyIdEncrypted.get().isEmpty()){
				agencyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyIdEncrypted.get());
			}else{
				agencyId = agencyUtils.getAgencyId(request);
			}
			return ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/documents/"+agencyId, String.class);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching agency document information - "+agencyId,ex);
			throw new GIRuntimeException("Exception occured while uploading agency document - "+agencyId,ex);
		}
	}

	public String decryptOrFetchAgencyId(Optional<String> agencyId) throws InvalidUserException, IOException, JsonProcessingException {
		String decryptedAgencyId;
		
		if(agencyId.isPresent() && !agencyId.get().isEmpty() ){
			decryptedAgencyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyId.get());
		} else  {
			AccountUser accountUser = userService.getLoggedInUser();
			decryptedAgencyId = String.valueOf(agencyUtils.retrieveAgencyMgrForBrokerId(accountUser));
		}
		return decryptedAgencyId;
	}

	@ResponseBody
	@RequestMapping(value = "/document/delete/{documentId}", method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'REMOVE_AGENCY_DOCUMENT')")
	public String deleteDocument(@PathVariable("documentId") String documentId) {
		AgencyDocumentDto agencyDocumentDto = new AgencyDocumentDto();
		try {
			agencyDocumentDto = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"/agency/document/delete/"+ghixJasyptEncrytorUtil.decryptStringByJasypt(documentId), null, AgencyDocumentDto.class);
		} catch (Exception exception) {
			agencyDocumentDto.setStatus(BrokerConstants.RESPONSE_FAILURE);
			LOGGER.error("Exception occured while deleting Agency document " , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}	
		
		return agencyDocumentDto.getStatus();
	}

	@ResponseBody
	@RequestMapping(value = {"/certification/view","/certification/view/{agencyId}"}, method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'VIEW_AGENCY_CERT_HISTORY')")
	public String viewAgencyCertification(@PathVariable("agencyId") Optional <String> encAgencyId, HttpServletRequest request){
		String agencyId = null;
		try{
			if(encAgencyId.isPresent() && !encAgencyId.get().isEmpty()){
				agencyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encAgencyId.get());
			}else{
				agencyId = agencyUtils.getAgencyId(request);
			}
			return  ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/agency/certification/view/"+agencyId, String.class);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching agency certification information - "+agencyId,ex);
			throw new GIRuntimeException("Exception occured while uploading agency document - "+agencyId,ex);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = {"/certification"}, method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'EDIT_AGENCY_CERT_STATUS')")
	public AgencyCertificationStatusDto updateCertificationStatus(@RequestParam("agencyId") String agencyId, @RequestParam("certificationStatus") String certificationStatus, @RequestParam(value="comment", required=false)  String comment, HttpServletRequest request) {
		try {
			AgencyCertificationStatusDto agencyCertificationStatusDto = new AgencyCertificationStatusDto();
			agencyCertificationStatusDto.setId(agencyId);
			agencyCertificationStatusDto.setCertificationStatus(certificationStatus);
			agencyCertificationStatusDto.setComment(comment);
			agencyCertificationStatusDto.setDeclarationDocId(request.getParameter("documentId_declaration"));
			agencyCertificationStatusDto.setSupportDocId(request.getParameter("documentId_supporting"));
			agencyCertificationStatusDto.setContractDocId(request.getParameter("documentId_contract"));
			
			return ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"/agency/certification", agencyCertificationStatusDto, AgencyCertificationStatusDto.class);
		} catch (Exception exception) {
			LOGGER.error("Exception occured while updating agency certification : " , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	
	@ResponseBody
	@RequestMapping(value={"/certificationhistory/{agencyId}"},method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'VIEW_AGENCY_CERT_HISTORY')")
	public String getAgencyCertificationHistory(@PathVariable("agencyId") String agencyId){
		try {
			return  ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/agency/certificationhistory/"+ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyId), String.class);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching agency certification history - "+agencyId,ex);
			throw new GIRuntimeException("Exception occured while fetching agency certification history - "+agencyId,ex);
		}
	}
	
	/**
	 * Display View Agents List
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to view agent list page
	 */
	@ResponseBody
	@RequestMapping(value = {"/agent/list/{agencyId}"}, method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'AGENCY_MANAGE_AGENTS')")
	public String viewAgentList(@PathVariable("agencyId") Optional<String> agencyIdEncrypted, HttpServletRequest request, @RequestParam(value = "pageNumber", required = false) Integer pageNumber){
		String agencyId = null;
		AgencyBrokerListDTO agencyBrokerListDTO = null;
		String response = StringUtils.EMPTY;
		SearchBrokerDTO searchBrokerDTO = new SearchBrokerDTO();
		try {
			AccountUser user = userService.getLoggedInUser();
			agencyId = (String) request.getSession().getAttribute("landingAgencyId");
			if(agencyIdEncrypted.isPresent() && !agencyIdEncrypted.get().isEmpty()){
				agencyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyIdEncrypted.get());
			}else if(agencyId!=null && !agencyId.isEmpty()){
				agencyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyId);
			}
			else if(user.getActiveModuleId()!=0){
				Broker broker = brokerService.getBrokerDetail(user.getActiveModuleId());
				agencyId = String.valueOf(broker.getAgencyId());
			} else{
				agencyId = String.valueOf(agencyUtils.retrieveAgencyMgrForBrokerId(user));
			}
			searchBrokerDTO.setId(Long.valueOf(agencyId));
			searchBrokerDTO.setPageNumber(pageNumber!=null?pageNumber:1);
			agencyBrokerListDTO = brokerService.getBrokersForAgency(searchBrokerDTO);
			response =  JacksonUtils.getJacksonObjectWriter(AgencyBrokerListDTO.class).writeValueAsString(agencyBrokerListDTO);
			
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching agents list - "+agencyId,ex);
		}
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = {"/agent/list"}, method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'AGENCY_MANAGE_AGENTS')")
	public String viewAgentResults(@RequestBody SearchBrokerDTO searchBrokerDTO, HttpServletRequest request){
		String agencyId = null;
		AgencyBrokerListDTO agencyBrokerListDTO = null;
		String response = StringUtils.EMPTY;
		try {
			AccountUser user = userService.getLoggedInUser();
			agencyId = (String) request.getSession().getAttribute("landingAgencyId");
			if(agencyId!=null && !agencyId.isEmpty()){
				agencyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyId);
			}
			else if(user.getActiveModuleId()!=0){
				Broker broker = brokerService.getBrokerDetail(user.getActiveModuleId());
				agencyId = String.valueOf(broker.getAgencyId());
			} else{
				agencyId = String.valueOf(agencyUtils.retrieveAgencyMgrForBrokerId(user));
			}
			searchBrokerDTO.setId(Long.valueOf(agencyId));
			//Condition to take care when Page number doesnt come from UI
			if(searchBrokerDTO.getPageNumber() < 1) {
				searchBrokerDTO.setPageNumber(1);
			}
			agencyBrokerListDTO = brokerService.getBrokersForAgency(searchBrokerDTO);
			response =  JacksonUtils.getJacksonObjectWriter(AgencyBrokerListDTO.class).writeValueAsString(agencyBrokerListDTO);
			
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching agents list - "+agencyId,ex);
		}
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value={"/approveRequest/{brokerId}/{individualId}"}, method=RequestMethod.POST)
	public String approveRequest(@PathVariable("brokerId") String brokerId, @PathVariable("individualId") String individualId, HttpServletRequest request){
		String status = BrokerConstants.RESPONSE_SUCCESS;
		try {
			if(!agencyUtils.isUserSwitch(request)){
				AccountUser user = userService.getLoggedInUser();
				if((userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L1) 
						|| userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2))){
					AgencyAssistantInfoDto agencyAssistantInfoDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/information/"+user.getRecordId(), AgencyAssistantInfoDto.class);
					if(agencyAssistantInfoDto!=null && agencyAssistantInfoDto.getApprovalStatus()!=null && !"APPROVED".equalsIgnoreCase(agencyAssistantInfoDto.getApprovalStatus().toString())){
						throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
					}
				}else{
					AgencyInformationDto agencyInformationDto = brokerService.findAgencyByLoggedInUser(user);
					if(agencyInformationDto!=null && agencyInformationDto.getCertificationStatus()!=null && !"CERTIFIED".equalsIgnoreCase(agencyInformationDto.getCertificationStatus().toString())){
						throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
					}
				}
			}
			
			Broker broker = brokerService.getBrokerDetail(Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(brokerId)));
			if(broker!=null && StringUtils.isNotEmpty(broker.getStatus()) && "InActive".equalsIgnoreCase(broker.getStatus())){
				throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
			}

			status = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"/agency/approveRequest/"+ghixJasyptEncrytorUtil.decryptStringByJasypt(brokerId)+"/"+ghixJasyptEncrytorUtil.decryptStringByJasypt(individualId), null, String.class);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while approving Individual request by Agency.", ex);
			status = BrokerConstants.RESPONSE_FAILURE;
		}
		
		return status;
	}
	
	
	@ResponseBody
	@RequestMapping(value={"/delegations/{designationStatus}"}, method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllDelegations(@PathVariable("designationStatus") String designationStatus,
			@RequestBody  SearchAgencyDelegationDTO searchAgencyDelegationDTO ,HttpServletRequest request ){
		Broker broker = null;
		long agencyId = 0;
		
		try {
			AccountUser user = userService.getLoggedInUser();
			
			if(agencyUtils.isUserSwitch(request)){
				agencyId = Long.parseLong((String) request.getSession().getAttribute(SWITCH_TO_MODULE_ID));
			}else if((userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L1) 
					|| userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2))){
				Validate.notNull(user);
				AgencyAssistantInfoDto agencyAssistantInfoDto = null;
				if (!StringUtils.isBlank(user.getRecordId())) {
					agencyAssistantInfoDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/information/"+user.getRecordId(), AgencyAssistantInfoDto.class);
					if(agencyAssistantInfoDto!=null && agencyAssistantInfoDto.getAgencyId()!=null && !agencyAssistantInfoDto.getAgencyId().isEmpty()){
						agencyId = Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantInfoDto.getAgencyId()));
					}
				}else{
					agencyAssistantInfoDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/getstaffbyuser/"+user.getId(), AgencyAssistantInfoDto.class);
					if(agencyAssistantInfoDto.getAgencyId()!=null && !agencyAssistantInfoDto.getAgencyId().isEmpty()){
						agencyId = Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantInfoDto.getAgencyId()));
					}
				}
			}else {
				if(user.getActiveModuleId()!=0){
					broker = brokerService.getBrokerDetail(user.getActiveModuleId());
				} else {
					broker = brokerService.findBrokerByUserId(user.getId());
				}
				
				if(broker.getAgencyId()!=null){
					agencyId = broker.getAgencyId().intValue();
				}
			}
			searchAgencyDelegationDTO.setAgencyId(agencyId);
			
			if(null != searchAgencyDelegationDTO.getPageNumber()) {
				searchAgencyDelegationDTO.setStartPosition((Integer.parseInt(searchAgencyDelegationDTO.getPageNumber())-1)*10); 	
			} else {
				searchAgencyDelegationDTO.setStartPosition(0);
			}
			searchAgencyDelegationDTO.setPageSize(10);
			searchAgencyDelegationDTO.setDesignationStatus(designationStatus); 
			
			String response =  ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"/agency/delegations/"+designationStatus, searchAgencyDelegationDTO, String.class);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching delegation request list for agency - ", ex);
			return new ResponseEntity<>("Exception occured while fetching delegation request list for agency - ",HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = {"/bookofbusiness" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> loadAgencyBookOfBusiness(@RequestBody SearchAgencyBookOfBusinessDTO searchAgencyBookOfBusinessDTO,HttpServletRequest request, Model model) {
		Broker broker = null;
		String agencyId = null;
		String certificationStatus = "";
		
		try {
			AccountUser user = userService.getLoggedInUser();
			
			if(agencyUtils.isUserSwitch(request)){
				agencyId = (String) request.getSession().getAttribute(SWITCH_TO_MODULE_ID);
				
				LOGGER.info("SWITCH_TO_MODULE_ID - ", agencyId);
			}else if((userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L1) 
					|| userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2))){
				Validate.notNull(user);
				AgencyAssistantInfoDto agencyAssistantInfoDto = null;
				if (!StringUtils.isBlank(user.getRecordId())) {
					agencyAssistantInfoDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/information/"+user.getRecordId(), AgencyAssistantInfoDto.class);
					if(agencyAssistantInfoDto!=null && agencyAssistantInfoDto.getAgencyId()!=null && !agencyAssistantInfoDto.getAgencyId().isEmpty()){
						agencyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantInfoDto.getAgencyId());
					}
				}else{
					agencyAssistantInfoDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/getstaffbyuser/"+user.getId(), AgencyAssistantInfoDto.class);
					if(agencyAssistantInfoDto.getAgencyId()!=null && !agencyAssistantInfoDto.getAgencyId().isEmpty()){
						agencyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantInfoDto.getAgencyId());
					}
				}
			}else {
				String switchToModuleName = (String) request.getSession().getAttribute(SWITCH_TO_MODULE_NAME);
				String isUserSwitchToOtherView = (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW);
				
				LOGGER.info("switchToModuleName : "+ switchToModuleName + " : isUserSwitchToOtherView : "+isUserSwitchToOtherView);
				LOGGER.info("user.getActiveModuleName() : "+ user.getActiveModuleName());
				
				if(isUserSwitchToOtherView != null && switchToModuleName!=null && isUserSwitchToOtherView.equalsIgnoreCase(BrokerConstants.YES)
						&& user.getActiveModuleName().equalsIgnoreCase(RoleService.BROKER_ADMIN_ROLE)){
					broker = brokerService.getBrokerDetail(user.getActiveModuleId());
				} else{
					broker = brokerService.findBrokerByUserId(user.getId());
				}
				
				agencyId = broker.getAgencyId().toString();
				certificationStatus = broker.getCertificationStatus();
			}
			searchAgencyBookOfBusinessDTO.setAgencyId(agencyId);

			String response = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL + "/agency/bookofbusiness?aManCertStatus="+certificationStatus, searchAgencyBookOfBusinessDTO, String.class);
			
			setModelAttributes(user, model, request);
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching Agency BOB-Details - ", ex);
			return new ResponseEntity<>("Exception occured while fetching Agency BOB-Details - ",HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	private void setModelAttributes(AccountUser user, Model model, HttpServletRequest request) throws InvalidUserException{
		model.addAttribute(BrokerConstants.ENCRYPTED_RECORD_ID, ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(user.getActiveModuleId())));
		model.addAttribute(BrokerConstants.SWITCH_ACC_URL, GhixConstants.SWITCH_URL_IND66);
		model.addAttribute(BrokerConstants.BOB_DOWNLOAD_URL, GhixConstants.BOB_DOWNLOAD_URL);
		model.addAttribute(BrokerConstants.RECORD_TYPE, user.getActiveModuleName());
	}
	
	@ResponseBody
	@RequestMapping(value = { "/disablepopup"}, method = RequestMethod.GET )
	//@PreAuthorize("hasPermission(#model, 'VIEW_AGENCY_INFORMATION')")
	public String disablePopUp( HttpServletRequest request){
		request.getSession().setAttribute("agencyToIndPopupDisable", "Y");
		return "DISABLED";
	}
	
	@ResponseBody
	@RequestMapping(value = {"/plandetails"}, method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public String getPlanDetails(HttpServletRequest request, @RequestBody SearchIndividualPlanDTO individualPlanDetailsDTO){
		String response = null;
				
		try {
			AccountUser user = userService.getLoggedInUser();
			
			if(individualPlanDetailsDTO!=null && individualPlanDetailsDTO.getIndividualPlanDetails()!=null){
				Map<String, IndividualPlanDetailsDTO> individualPlanDetails = individualPlanDetailsDTO.getIndividualPlanDetails();
				
				Set<Integer> planSet = new HashSet<>();
				for(Map.Entry<String, IndividualPlanDetailsDTO> entry:individualPlanDetails.entrySet()){
					planSet.add(Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(entry.getValue().getPlanId())));
				}
				
				Map<Integer, PlanLightResponseDTO> planResponseMap = planMgmtService.getPlansInfo(new ArrayList<>(planSet));
			
				if(planResponseMap!=null){
					for(Map.Entry<String, IndividualPlanDetailsDTO> entry:individualPlanDetails.entrySet()){
						IndividualPlanDetailsDTO value = entry.getValue();
						PlanLightResponseDTO planLightResponseDTO = planResponseMap.get(Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(value.getPlanId())));
						if(planLightResponseDTO!=null){
							value.setOfficeVisit(planLightResponseDTO.getOfficeVisit());
							value.setGenericDrugs(planLightResponseDTO.getGenericMedications());
							value.setPlanName(planLightResponseDTO.getPlanName());
							value.setPlanType(planLightResponseDTO.getNetworkType());
							value.setIssuerLogo(agencyUtils.getIssuerLogo(value.getIssuerHiosId(), user));
							value.setIssuerName(planLightResponseDTO.getIssuerName());
							
							int noOfEnrollees = findNoOfEnrollees(ghixJasyptEncrytorUtil.decryptStringByJasypt(value.getEnrollmentId()));
							
							if(noOfEnrollees>0){
								if (noOfEnrollees == 1) {
									if (StringUtils.isNotBlank(planLightResponseDTO.getDeductible())) {
										Float deductibleValue = agencyUtils.getFloatValue(planLightResponseDTO.getDeductible());
										if(deductibleValue != null){
											value.setDeductible(DOLLAR_SYMBOL + deductibleValue);
										}else{
											value.setDeductible("NA");
										}
									}
								} else {
									if (StringUtils.isNotBlank(planLightResponseDTO.getDeductibleFamily())) {
										Float deductibleValue = agencyUtils.getFloatValue(planLightResponseDTO.getDeductibleFamily());
										if(deductibleValue != null){
											value.setDeductible(DOLLAR_SYMBOL + deductibleValue);
										} else{
											value.setDeductible("NA");
										}
									}
								}
							}
						}
					}
					response = JacksonUtils.getJacksonObjectWriter(SearchIndividualPlanDTO.class).writeValueAsString(individualPlanDetailsDTO);
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching plant details : ",ex);
		}
		
		return response;
	}
	
	private int findNoOfEnrollees(String enrollmentId){
		int noOfEnrollees = 0;
		try {
			noOfEnrollees = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"/agency/noofenrollees/"+enrollmentId, null, Integer.class);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching number of enrollees ", ex);
		}
		return noOfEnrollees;
	}
	
	
	@RequestMapping(value = "/agentbulktransfer", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?>  agentBulkTransfer(@RequestBody AgentBulkTransferDTO agentBulkTransferDTO) {

		try {
			String response = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL + "/agency/agentbulktransfer",agentBulkTransferDTO, String.class);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while Agency-Bulk-Transfer - ", ex);
			return new ResponseEntity<>("Exception occured while Agency-Bulk-Transfer - ",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@ResponseBody
	@RequestMapping(value = {"/searchagents"}, method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> searchAgents(@RequestBody SearchBrokerDTO searchBrokerDTO, HttpServletRequest request){
		Broker broker;
		long agencyId = 0;
		
		try {
			AccountUser user = userService.getLoggedInUser();
			if(agencyUtils.isUserSwitch(request)){
				agencyId = Long.parseLong((String) request.getSession().getAttribute(SWITCH_TO_MODULE_ID));
			}else if((userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L1) 
					|| userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2))){
				Validate.notNull(user);
				AgencyAssistantInfoDto agencyAssistantInfoDto = null;
				if (!StringUtils.isBlank(user.getRecordId())) {
					agencyAssistantInfoDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/information/"+user.getRecordId(), AgencyAssistantInfoDto.class);
					if(agencyAssistantInfoDto!=null && agencyAssistantInfoDto.getAgencyId()!=null && !agencyAssistantInfoDto.getAgencyId().isEmpty()){
						agencyId = Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantInfoDto.getAgencyId()));
					}
				}else{
					agencyAssistantInfoDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/getstaffbyuser/"+user.getId(), AgencyAssistantInfoDto.class);
					if(agencyAssistantInfoDto.getAgencyId()!=null && !agencyAssistantInfoDto.getAgencyId().isEmpty()){
						agencyId = Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantInfoDto.getAgencyId()));
					}
				}
			}else {
				if(user.getActiveModuleId()!=0){
					broker = brokerService.getBrokerDetail(user.getActiveModuleId());
				} else {
					broker = brokerService.findBrokerByUserId(user.getId());
				}
				agencyId = broker.getAgencyId();
			}
			
			searchBrokerDTO.setId(agencyId);
			String response = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL + "/agency/searchagents", searchBrokerDTO, String.class);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while searching Agency agents.", ex);
			throw new GIRuntimeException("Exception occured while searching Agency agents.", ex);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = {"/export","/export/{agentId}"}, method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public void export(@PathVariable("agentId")  Optional<String> agentId, HttpServletResponse httpServletResponse, HttpServletRequest httpRequest){
		Broker broker = null;
		AgencyBookOfBusinessExportDTO agencyBookOfBusinessExportDTO = new AgencyBookOfBusinessExportDTO();
		long agencyId = 0;
		AgencyInformationDto agencyInformationDto = null;
		
		try {
		    if(agentId.isPresent() && !agentId.get().isEmpty() ){
		    	agencyBookOfBusinessExportDTO.setId(Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agentId.get())));
		    	agencyBookOfBusinessExportDTO.setRecordType("Agent");
		    	broker = brokerService.getBrokerDetail(Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agentId.get())));
			} else {
				if(agencyUtils.isUserSwitch(httpRequest)){
					agencyId = Long.parseLong((String) httpRequest.getSession().getAttribute(SWITCH_TO_MODULE_ID));
				} else {
					AccountUser user = userService.getLoggedInUser();
					
					if(user.getActiveModuleId()!=0){
						broker = brokerService.getBrokerDetail(user.getActiveModuleId());
					} else {
						broker = brokerService.findBrokerByUserId(user.getId());
					}
					agencyId = broker.getAgencyId();
				}
				
				agencyInformationDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+agencyId, AgencyInformationDto.class);
				agencyBookOfBusinessExportDTO.setId(Integer.valueOf(agencyInformationDto.getFederalTaxId()));
				agencyBookOfBusinessExportDTO.setRecordType("AgencyManager");	
			}
		    
		    String request = JacksonUtils.getJacksonObjectWriter(AgencyBookOfBusinessExportDTO.class).writeValueAsString(agencyBookOfBusinessExportDTO);
		    
		    LOGGER.info("URL : "+agentBulkTransferAndExportUrl+" : Request : ", request);
			
			String response = ghixRestTemplate.postForObject(agentBulkTransferAndExportUrl, request, String.class);
			
			LOGGER.info(" BOB Download Response : ", response);
			
			if(StringUtils.isNotBlank(response)){
				AgencyBookOfBusinessExportResponseDTO resonseDto = JacksonUtils.getJacksonObjectReader(AgencyBookOfBusinessExportResponseDTO.class).readValue(response);
				if(BrokerConstants.RESPONSE_SUCCESS_CODE == resonseDto.getResponseCode()){
					httpServletResponse.setContentType("text/csv");
					
					if(agencyUtils.isUserSwitch(httpRequest) && agencyBookOfBusinessExportDTO.getRecordType().equalsIgnoreCase("AgencyManager")){
						httpServletResponse.addHeader("Content-Disposition", "attachment; filename="+agencyInformationDto.getBusinessLegalName().replaceAll(" ", "_")+".csv");
					} else {
						httpServletResponse.addHeader("Content-Disposition", "attachment; filename="+broker.getCompanyName().replaceAll(" ", "_")+".csv");
					}
					FileCopyUtils.copy(resonseDto.getByteStream(), httpServletResponse.getOutputStream());
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while exporting Agency Bob.", ex);
			throw new GIRuntimeException("Exception exporting Agency Bob.", ex);
		}
	}
	
	
	/**
	 * Display Agency assistant information page
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to assistant information page
	 */
	@ResponseBody
	@RequestMapping(value = {"/staff/information/view","/staff/information/view/{assistantId}"}, method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public String addNewAssistant(@PathVariable("assistantId") Optional<String> assistantIdEncrypted, HttpServletRequest request){
		String agencyId = null;
		String assistantId = null;
		try {
			AccountUser user = userService.getLoggedInUser();
			agencyId = (String) request.getSession().getAttribute("landingAgencyId");
			
			if(agencyId!=null && !agencyId.isEmpty()){
				agencyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyId);
			}else if(user!=null && user.getActiveModuleId()!=0){
				if(userService.hasUserRole(user, RoleService.AGENCY_MANAGER)){
					Broker broker = brokerService.getBrokerDetail(user.getActiveModuleId());
					agencyId = String.valueOf(broker.getAgencyId());
				}
			} else if(user.getRecordId() != null && userService.hasUserRole(user, RoleService.AGENCY_MANAGER)){
				agencyId = String.valueOf(agencyUtils.retrieveAgencyMgrForBrokerId(user));
			}
			
			if (user!=null && user.getRecordId() != null && (userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L1) 
					|| userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2))) {
				return ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/information/"+user.getRecordId(), String.class);
			}else{
				if(assistantIdEncrypted.isPresent() && !assistantIdEncrypted.get().isEmpty() ){
					assistantId = ghixJasyptEncrytorUtil.decryptStringByJasypt(assistantIdEncrypted.get());
				}else{
					return ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/information/view/"+agencyId, String.class);
				}
			}
			return ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/information/view/"+agencyId+"/"+assistantId, String.class);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching staff information - "+agencyId,ex);
			throw new GIRuntimeException("Exception occured while processing staff information  - "+agencyId,ex);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = {"/staff/information"}, method = RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public String updateStaffInformation(@RequestBody AgencyAssistantInfoDto agencyAssistantInfoDto, HttpServletRequest request) {
		
		try {
			String response = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/information",agencyAssistantInfoDto,String.class);
			
			return response;
		} catch (Exception ex) {
			LOGGER.error("Exception occured while saving staff information - "+agencyAssistantInfoDto.getId(),ex);
			throw new GIRuntimeException("Exception occured while processing staff information  - "+agencyAssistantInfoDto.getId(),ex);
		}
	}
	
	/**
	 *
	 * @param encIndividualId
	 * @param request
	 * @return
	 * @throws GIRuntimeException
	 */
	@RequestMapping(value = "/decline/individual/{encBrokerId}/{encIndividualId}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String declineIndividualRequest(@PathVariable("encBrokerId") String encBrokerId, @PathVariable("encIndividualId") String encIndividualId, HttpServletRequest request) {
		LOGGER.info("Agency decline : START");
		AccountUser user;
		
		try {
			user = userService.getLoggedInUser();
			agencyUtils.declineBrokerActiveIndividual(ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId), ghixJasyptEncrytorUtil.decryptStringByJasypt(encIndividualId), user);
		} catch(Exception e){
			LOGGER.error("Exception occured while handling the Agency decline designation request for Individual :", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("Agency decline : END");
		
		return "ok";
	}
	
	@ResponseBody
	@RequestMapping(value="/staff/agencyassistantlistforadmin",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public String findAgencyAssistantsForAdmin(@RequestBody AgencyAssistantSearchDto agencyAssistantSearchDto){
		
		try {
			return ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/agencyassistantlistforadmin",agencyAssistantSearchDto,String.class);
		} catch (Exception exception) {
			LOGGER.error("Exception occured while fetching agency assistant list for admin.", exception);
			throw new GIRuntimeException("Exception occured while fetching agency assistant list for admin.", exception);
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/staff/agencyassistantlistforagency",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public String findAgencyAssistantsForAgency(@RequestBody AgencyAssistantSearchDto agencyAssistantSearchDto){
		String response = null;
		try {
			if(agencyAssistantSearchDto!=null && StringUtils.isNotEmpty(agencyAssistantSearchDto.getAgencyId())){
				response = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/agencyassistantlistforagency",agencyAssistantSearchDto,String.class);
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occured while fetching agency assistant list for agency.", exception);
			throw new GIRuntimeException("Exception occured while fetching agency assistant list for agency.", exception);
		}
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = {"/staff/statushistory/{encAssistantId}"}, method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public String viewAssitantActivityStatusHistory(@PathVariable("encAssistantId") Optional<String> encAssistantId){
		String assistantId = null;
		String response = null;
		try{
			AccountUser user = userService.getLoggedInUser();
			if (user!=null && user.getRecordId() != null && (userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L1) 
					|| userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2))) {
				assistantId = user.getRecordId();
			}else{
				if(encAssistantId.isPresent() && !encAssistantId.get().isEmpty()){
					assistantId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encAssistantId.get());
				}
			}
			response = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/statushistory/"+assistantId, String.class);
			return response;
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching assistant status history- "+assistantId,ex);
			throw new GIRuntimeException("Exception occured while fetching assistant status history- "+assistantId,ex);
		}
	}
	
	@ResponseBody
	@RequestMapping(value={"/staff/approvalhistory/{encAgencyAssistantId}"},method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public String getAgencyAssistantApprovalHistory(@PathVariable("encAgencyAssistantId") Optional<String>  encAgencyAssistantId){
		String response = null;
		String agencyAssistantId = null;
		
		try {
			AccountUser user = userService.getLoggedInUser();
			if (user!=null && user.getRecordId() != null && (userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L1) 
					|| userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2))) {
				agencyAssistantId = user.getRecordId();
			}else{
				if(encAgencyAssistantId.isPresent() && !encAgencyAssistantId.get().isEmpty()){
					agencyAssistantId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encAgencyAssistantId.get());
				}
			}
			response =  ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/approvalhistory/"+agencyAssistantId, String.class);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while fetching assistant approval history - "+agencyAssistantId,ex);
			throw new GIRuntimeException("Exception occured while fetching assistant status - "+agencyAssistantId,ex);
		}
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value="/staff/status",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public String updateAssitantActivityStatus(@RequestBody AgencyAssistantStatusDto agencyAssistantStatusDto){
		String response = null;
		try {
			if(agencyAssistantStatusDto!=null && StringUtils.isNotEmpty(agencyAssistantStatusDto.getId())){
				response = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/status",agencyAssistantStatusDto,String.class);
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occured while updating agency assistant status.", exception);
			throw new GIRuntimeException("Exception occured while updating agency assistant status.", exception);
		}
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value="/staff/approvalstatus",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public String updateAssitantApprovalStatus(@RequestBody AgencyAssistantApprovalDto agencyAssistantApprovalDto){
		String response = null;
		try {
			if(agencyAssistantApprovalDto!=null && StringUtils.isNotEmpty(agencyAssistantApprovalDto.getId())){
				response = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/approvalstatus",agencyAssistantApprovalDto,String.class);
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occured while updating agency assistant approval status.", exception);
			throw new GIRuntimeException("Exception occured while updating agency assistant approval status.", exception);
		}
		return response;
	}
}
