package com.getinsured.ssap.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.ssap.model.financial.TaxFilingRequirement;
import com.getinsured.ssap.model.financial.TaxFilingRequirementType;

/**
 * Provides methods to access tax filing thresholds configuration.
 *
 * @author Yevgen Golubenko
 * @since 6/13/19
 */
public class TaxFilingThresholds {
  private static final Logger log = LoggerFactory.getLogger(TaxFilingThresholds.class);
  public static String TAX_FILING_REQUIREMENT_FILE = "/configuration/common/minimum-income-for-tax.json";
  private static final ObjectMapper objectMapper = new ObjectMapper();

  public static List<TaxFilingRequirement> getTaxFilingRequirementsByYear(final int year) {
    final ClassPathResource resource = new ClassPathResource(TAX_FILING_REQUIREMENT_FILE);
    log.debug("Returning tax filing requirements for {} year from: {}", year, TAX_FILING_REQUIREMENT_FILE);

    if(!resource.exists() || !resource.isReadable()) {
      log.error("Could not read tax filing requirements from resource: {}", TAX_FILING_REQUIREMENT_FILE);
      return null;
    }

    assert year > 0 : "Year should be > 0";

    List<TaxFilingRequirement> taxFilingRequirements = new ArrayList<>();
    final String json = JsonUtil.readFile(resource);
    try {
      final JsonNode rootNode = objectMapper.readTree(json);
      final String yearStr = String.valueOf(year);

      if(rootNode != null && rootNode.has(yearStr)) {
        final JsonNode yearlyTaxRequirements = rootNode.get(yearStr);
        LinkedHashMap<String, Integer> taxFilingMap = objectMapper.treeToValue(yearlyTaxRequirements, LinkedHashMap.class);

        taxFilingMap.keySet().forEach(taxFilingRequirementType ->  {
          final TaxFilingRequirementType type = TaxFilingRequirementType.valueOf(taxFilingRequirementType);
          final long amount = taxFilingMap.get(type.name());
          TaxFilingRequirement tfr = new TaxFilingRequirement();
          tfr.setType(type);
          tfr.setDollarAmount(amount);
          taxFilingRequirements.add(tfr);
        });
      }
    } catch (IOException e) {
      log.error("Exception while reading JSON content of {}", TAX_FILING_REQUIREMENT_FILE);
      log.error("Exception details", e);
    }

    return taxFilingRequirements;
  }
}
