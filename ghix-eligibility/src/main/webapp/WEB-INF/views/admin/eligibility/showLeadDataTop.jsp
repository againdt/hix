<html>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ page import="com.getinsured.hix.model.VimoEncryptor"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css" />


<head>
<title>GHIX Prescreen Data for ELIG_LEAD table</title>

<style>
table#table1 {
	border: 1px solid #666;
	width: 80%;
	margin: 20px 0 20px 0 !important;
	font-size: 14px;
}

th,td {
	padding: 2px 4px 2px 4px !important;
	text-align: left;
	vertical-align: top;	
}

thead tr {
	background-color: #fc0;
}

th.sorted {
	background-color: orange;
}

th a,th a:visited {
	color: black;
}

th a:hover {
	text-decoration: underline;
	color: black;
}

th.sorted a,th.sortable a {
	background-position: right;
	display: block;
	width: 100%;
}



tr.odd {
	background-color: #fff
}

tr.tableRowEven,tr.even {
	background-color: #fea
}

div.exportlinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	margin: 2px 0 10px 0;
	width: 79%;
}

span.export {
	padding: 0 4px 1px 20px;
	display: inline;
	display: inline-block;
	cursor: pointer;
}

span.pagebanner {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	margin-top: 10px;
	display: block;
	border-bottom: none;
}

span.pagelinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	display: block;
	border-top: none;
	margin-bottom: -5px;
}


.group-1 {
    font-weight:bold;
    padding-bottom:10px;
    border-top:1px solid black;
}
.group-2 {
    font-style:italic;
    border-top: 1px solid black;

}
.subtotal-sum, .grandtotal-sum {
    font-weight:bold;
    text-align:right;
}
.subtotal-header {
    padding-bottom: 0px;
    border-top: 1px solid white;
}
.subtotal-label, .grandtotal-label {
    border-top: 1px solid white;
    font-weight: bold;
}
.grouped-table tr.even {
    background-color: #fff;
}
.grouped-table tr.odd {
    background-color: #fff;
}
.grandtotal-row {
    border-top: 2px solid black;
}

</style>

</head>

<script>

	$(function() {
		$( "#date1" ).datepicker().attr( 'readOnly' , 'true' );
	});

	$(function() {
		$( "#date2" ).datepicker().attr( 'readOnly' , 'true' );;
	});
	
	function validateDates(){
		var dateStart = document.getElementById('date1').value.split('/');
	
			if (dateStart != "" && dateStart.length > 0) {
	
				var boolean1 = validateDate(dateStart);
			}
	
			var dateEnd = document.getElementById('date2').value.split('/');
	
			if (dateEnd != "" && dateEnd.length > 0) {
				var boolean2 = validateDate(dateEnd);
			}
	}

	function validateDate(dateGiven) {

		var today = new Date();

		var dob_mm = dateGiven[0];
		var dob_dd = dateGiven[1];
		var dob_yy = dateGiven[2];

		var birthDate = new Date();
		birthDate.setFullYear(dob_yy, dob_mm - 1, dob_dd);

		if ((today.getFullYear() - 2) > birthDate.getFullYear()) {
			return false;
		}

		if ((dob_dd != birthDate.getDate())
				|| (dob_mm - 1 != birthDate.getMonth())
				|| (dob_yy != birthDate.getFullYear())) {
			return false;
		}

		if (today.getTime() < birthDate.getTime()) {
			return false;
		}

	}

	function setWhereClause(type) {

		var whereClause = '';
		var filterStatement = '';
		switch (type) {
		case 'B1':
			whereClause = 'CREATION_TIMESTAMP > (CURRENT_TIMESTAMP - interval \'1\' day)';
			filterStatement = ' in last 24 hours';
			break;
		case 'B2':
			whereClause = 'CREATION_TIMESTAMP > (CURRENT_TIMESTAMP - interval \'12\' hour)';
			filterStatement = ' in last 12 hours';
			break;
		case 'B3':
			whereClause = 'CREATION_TIMESTAMP > (CURRENT_TIMESTAMP - interval \'1\' hour)';
			filterStatement = ' in last 1 hour';
			break;
		case 'B4':

			//Validate the dates
			validateDates();

			//Step 1: Get List Box value, if it is not empty
			var fieldName = document.getElementById('field_name').value;
			var fieldValue = document.getElementById('field_value').value;
			if (fieldValue != null && fieldValue.length > 0) {

				whereClause = " " + fieldName + "='"
						+ fieldValue + "'";
				filterStatement = ' with ' + fieldName + " = " + fieldValue;
			}

			//Step 3: Get the Dates
			var date1 = document.getElementById('date1').value;
			var date2 = document.getElementById('date2').value;

			if (date1 && date2) {

				if (whereClause) {
					whereClause = whereClause
							+ "and CREATION_TIMESTAMP between to_timestamp('"
							+ date1
							+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"
							+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					filterStatement = filterStatement + ', start date ' + date1
							+ ', end date ' + date2;

				} else {
					whereClause = whereClause
							+ " CREATION_TIMESTAMP between to_timestamp('"
							+ date1
							+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"
							+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					filterStatement = filterStatement + ' start date ' + date1
							+ ', end date ' + date2;

				}

			} else {
				if (date1 && !date2) {
					if (whereClause) {

						whereClause = whereClause
								+ "and to_char(CREATION_TIMESTAMP  ,'mm/dd/yyyy' )='"
								+ date1 + "'";
						filterStatement = filterStatement + ', start date '
								+ date1;
					} else {
						whereClause = whereClause
								+ " to_char(CREATION_TIMESTAMP  ,'mm/dd/yyyy' )='"
								+ date1 + "'";
						filterStatement = filterStatement + ' start date '
								+ date1 ;
					}
				} else {

					if (!date1 && date2) {
						if (whereClause) {
							whereClause = whereClause
									+ "and to_char(CREATION_TIMESTAMP  ,'mm/dd/yyyy' )='"
									+ date2 + "'";
							filterStatement = filterStatement + ', end date ' + date2;
						} else {
							whereClause = whereClause
									+ " to_char(CREATION_TIMESTAMP  ,'mm/dd/yyyy' )='"
									+ date2 + "'";
							filterStatement = filterStatement + ' end date ' + date2;
						}

					}
				}

			}
			
			//Add status condition.
			var status = document.getElementById('apiStatus').value;
			if(status){
				whereClause = "OVERALL_API_STATUS ='"+status+"'";
				filterStatement = filterStatement + ', Status = ' + status;
			}
			
			//Add duration condition.
			var minDuration = document.getElementById('min').value;
			var maxDuration = document.getElementById('max').value;
			
			if(isNaN(minDuration)){		 
				document.getElementById('min').value = "";
				alert("Please enter valid 'min' number.");
				return false;
			}
			
			if(isNaN(maxDuration)){
				document.getElementById('max').value =  "";
				alert("Please enter valid 'max' number.");
				return false;
			}
			
			if(minDuration){				
				if(maxDuration){					
					whereClause = "GI_EXECUTION_DURATION >= " + minDuration +" AND GI_EXECUTION_DURATION <= " + maxDuration;
					filterStatement = filterStatement + ', Mimimum duration = ' + minDuration + ', Maximum duration = ' + maxDuration;
				}else{
					whereClause = "GI_EXECUTION_DURATION >= " + minDuration ;
					filterStatement = filterStatement + ', Mimimum duration = ' + minDuration;
				}				
				
			}else{
				if(maxDuration){
					whereClause = "GI_EXECUTION_DURATION <= " + maxDuration;
					filterStatement = filterStatement + ', Maximum duration = ' + minDuration;
				}
				
			}
								
			if (!whereClause) {
				whereClause = "CREATION_TIMESTAMP <= systimestamp";
			}

			//alert(whereClause);			
			break;
		}
		document.getElementById('whereClause').value = whereClause;
		document.getElementById('filterStatement').value = filterStatement;
	}
</script>    

<body>



<form method="POST" action="showLeadDataTop">

	<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#C0C0C0">
				<input type="submit" value="24 Hours" name="B1" title="View requests made in last 24 hours" onclick="setWhereClause('B1');">
				<input type="submit" value="12 Hours" name="B2" title="View requests made in last 12 hours" onclick="setWhereClause('B2');">
				<input type="submit" value="1 Hour" name="B3" title="View requests made in last 1 hour" onclick="setWhereClause('B3');">
			</td>
			<td bgcolor="#66FFCC">
				<p>
					&nbsp;Field:
					<select size="1" id="field_name" name="field_name">
						<option value="zipcode" selected="selected">Zip Code</option>
						<option value="county_code">County Code</option>
						<option value="no_of_applicants" >No. of applicants</option>
						<option value="household_income">Household Income</option>
						<option value="affiliate_id">Affiliate ID</option>
						<option value="error_code">Error Code</option>
						<option value="error_msg">Error Message</option>
					</select>
					<input type="text" id="field_value" name="field_value" size="10" maxlength="20">
					&nbsp;API Status:
					<select size="1" id="apiStatus" name="apiStatus">
					    <option value="" selected="selected"></option>
						<option value="success">SUCCESS</option>
						<option value="failure">FAILED</option>
					</select>					
					Date Start:
					<input type="text" id="date1" name="date1" size="10" maxlength="10"> 
					End:
					<input type="text" id="date2" name="date2" size="10" maxlength="10">					
				</p>
			</td>
		</tr>
		<tr>
			<td bgcolor="#C0C0C0">
				&nbsp;Duration:
				&nbsp;Min:<input type="text" id="min" name="min" size="10" maxlength="20">
				&nbsp;Max:<input type="text" id="max" name="max" size="10" maxlength="20">
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="return setWhereClause('B4');">
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">
</form>

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = "CREATION_TIMESTAMP <= systimestamp";		
	}
	
%>

<sql:query dataSource="jdbc/ghixDS" var="result">
	select ID, NAME,ZIPCODE, COUNTY_CODE,FAMILY_SIZE, 
	NO_OF_APPLICANTS, HOUSEHOLD_INCOME,DOC_VISIT_FREQUENCY,NO_OF_PRESCRIPTIONS,BENEFITS, 
	'<a href="showLeadDataBottom?email='||id||'" target="bottom">Email</a>' as EMAIL_ADDRESS,
	PHONE_NUMBER, 
	'<a href="showLeadDataBottom?member='||id||'" target="bottom">Data</a>' as MEMBER_DATA,
	APTC,PREMIUM,CSR,
	AFFILIATE_ID,EXT_AFF_HOUSEHOLD_ID,FLOW_ID,CLICK_ID,
	'<a href="showLeadDataBottom?apiOutput='||id||'" target="bottom">Response</a>' as API_OUTPUT,
	OVERALL_API_STATUS,GI_EXECUTION_DURATION,ERROR_CODE,ERROR_MSG,
	to_char(CREATION_TIMESTAMP,'MM/DD/YYYY HH:MI:SS') CREATION_TIMESTAMP,
	to_char(LAST_UPDATE_TIMESTAMP,'MM/DD/YYYY HH:MI:SS') LAST_UPDATE_TIMESTAMP,
	STAGE as CURRENT_STAGE,
	'<a href="showLeadDataBottom?stage='||id||'" target="bottom">Stage History</a>' as STAGE,
	STATUS,LEAD_TYPE,GI_EXCHANGE_FLOW_TYPE
	from  ( select  *  from   ELIG_LEAD  order by  ID desc )
	where <%=whereClause%>
	and CREATION_TIMESTAMP < sysdate-30 and rownum <= 100
	order by ID desc
</sql:query> 

Requests made for Eligibility Lead data

<%
	if (request.getParameter("filterStatement") != null) {
		out.println(request.getParameter("filterStatement"));
	}
%>

<br>

<display:table name="${result.rows}"  requestURI="showLeadDataTop" id="table1" style="white-space: pre-wrap;width:100%" export="false" pagesize="10">

	<display:column property="ID" title="ID" sortable="false" />
	
	<display:column property="NAME" title="Name" sortable="false" />
	<display:column property="ZIPCODE" title="Zip Code" sortable="false" />
	<display:column property="COUNTY_CODE" title="County Code" sortable="false" />
	<display:column property="FAMILY_SIZE" title="Family size" sortable="false" />
	<display:column property="NO_OF_APPLICANTS" title="No. of applicants" sortable="false" />	
	<display:column property="HOUSEHOLD_INCOME" title="Household income" sortable="false" />
	<display:column property="DOC_VISIT_FREQUENCY" title="Doctor visits frequency" sortable="false" />
	<display:column property="NO_OF_PRESCRIPTIONS" title="No. of prescriptions" sortable="false" />
	<display:column property="BENEFITS" title="Required benefits" sortable="false" />
	<display:column property="EMAIL_ADDRESS" title="E-mail" sortable="false"/>
	<display:column property="PHONE_NUMBER" title="Phone Number" sortable="false"/>
	
	<display:column property="MEMBER_DATA" title="Member Data" sortable="false"/>
	
	<display:column property="APTC" title="APTC" sortable="false"/>
	<display:column property="PREMIUM" title="Premium before APTC" sortable="false"/>
	<display:column property="CSR" title="Cost Sharing Variance" sortable="false"/>
	
	<display:column property="AFFILIATE_ID" title="Affiliate Id" sortable="false"/>
	<display:column property="EXT_AFF_HOUSEHOLD_ID" title="External Household Id" sortable="false"/>
	<display:column property="FLOW_ID" title="Flow Id" sortable="false"/>
	<display:column property="CLICK_ID" title="Click Id" sortable="false"/>
	<display:column property="STATUS" title="Affiliate Status" sortable="false"/>
	<display:column property="CURRENT_STAGE" title="Current Affiliate Stage" sortable="false"/>
	<display:column property="STAGE" title="See stage history" sortable="false"/>
	<display:column property="LEAD_TYPE" title="Lead Type" sortable="false"/>
	<display:column property="GI_EXCHANGE_FLOW_TYPE" title="Plans Type" sortable="false"/>
	
	<display:column property="API_OUTPUT" title="API Reponse" sortable="false"/>
	<display:column property="OVERALL_API_STATUS" title="API Status" sortable="false"/>
	<display:column property="GI_EXECUTION_DURATION" title="Execution Duration (ms)" sortable="false"/>
	<display:column property="ERROR_CODE" title="Error Code" sortable="false"/>
	<display:column property="ERROR_MSG" title="Error Message" sortable="false"/>
	
	<display:column property="CREATION_TIMESTAMP" title="Record creation time" sortable="false" />
	<display:column property="LAST_UPDATE_TIMESTAMP" title="Record update time" sortable="false" />

</display:table>

 </body> 
 </html>


