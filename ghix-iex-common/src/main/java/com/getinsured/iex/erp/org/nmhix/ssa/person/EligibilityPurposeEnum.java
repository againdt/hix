//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.06.05 at 10:47:57 AM IST 
//


package com.getinsured.iex.erp.org.nmhix.ssa.person;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EligibilityPurpose-Enum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EligibilityPurpose-Enum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INITIAL_DETERMINATION"/>
 *     &lt;enumeration value="NINETY_DAY_VERIFICATION_EXPIRATION"/>
 *     &lt;enumeration value="ANNUAL_REDETERMINATION"/>
 *     &lt;enumeration value="SPECIAL_ENROLLMENT"/>
 *     &lt;enumeration value="MANUAL_VERIFICATION_COMPLETE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EligibilityPurpose-Enum")
@XmlEnum
public enum EligibilityPurposeEnum {

    INITIAL_DETERMINATION,
    NINETY_DAY_VERIFICATION_EXPIRATION,
    ANNUAL_REDETERMINATION,
    SPECIAL_ENROLLMENT,
    MANUAL_VERIFICATION_COMPLETE;

    public String value() {
        return name();
    }

    public static EligibilityPurposeEnum fromValue(String v) {
        return valueOf(v);
    }

}
