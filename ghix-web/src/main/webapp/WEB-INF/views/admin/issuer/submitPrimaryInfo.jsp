<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<style>
.popover-title {
	font-size: 10px;
	line-height: 12px;
    display:block !important;
}
.popover-content p label{
	font-size: 10px;
	line-height: 10px;
}
.popover-content p input{
	margin-top: 0px
}
.popover.bottom .arrow:after {
	left: 0;
}
span.alert{display: inline-block;}
span.alert small{font-size:11px;}
</style>
<div class="gutter10">
	<div class="row-fluid">
		<ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="label.issuerTitle"/></a></li>
            <li><a href="<c:url value="/admin/addnewissuer"/>"><spring:message  code='label.addNewIssuer'/></a></li>
            <li><spring:message  code='label.primarycontactdetails'/></li>
        </ul><!--page-breadcrumb ends-->
        <h1><a name="skip"></a><spring:message  code='label.addNewIssuer'/></h1>
    </div>
    <div class="row-fluid">
    	<div class="span3" id="sidebar">
        	<div class="header">
            	<h4><spring:message  code='label.steps'/></h4>
            </div>
            <!--  beginning of side bar -->
            <ol class="nav nav-list">
           		<li> <a href="<c:url value="/admin/addnewissuer"> <c:param name="isBack" value="true"/>  </c:url>" ><spring:message  code="label.issuerDetails"/></a></li>
                <%-- <li><a href="<c:url value="/admin/addnewissuer"/>"><spring:message  code='label.issuerDetails'/></a></li> --%>
                <li class="active"><a href="<c:url value="/admin/issuer/primaryrepinfo"/>"><spring:message  code='label.primarycontactdetails'/></a></li>
                <c:choose>
                 <c:when test="${allowReviewAndSubmit == 'true'}">
                <li><a href="<c:url value="/admin/issuer/reviewissuer"/>"><spring:message  code='label.reviewandsubmit'/></a></li>
                </c:when>
                 <c:otherwise>
                 <li><a class="disableLink" href="#"><spring:message  code='label.reviewandsubmit'/></a></li>
                 </c:otherwise>
                </c:choose>
            </ol><!-- end of side bar -->
        </div><!-- end of span3-->
        <div class="span9" id="rightpanel">
        	<div class="header">
                <h4><spring:message  code='label.primarycontactdetails'/></h4>
            </div>
            <div class="gutter10">
            
            <form class="form-horizontal" id="frmPrimaryRepDetails" name="frmPrimaryRepDetails" action="<c:url value='/admin/issuer/primaryrepinfo'/>" method="POST">
	            <df:csrfToken/>
	            <c:choose>
	            	<c:when test="${issuerDetailsNotSubmitted == 'true'}">
		               <div class="error">
		                  <label class="error" for="error"><span><spring:message  code="err.enterIssuerDetails"/></span></label>
		               </div> 
		            </c:when>
		            <c:otherwise>
	                	<c:if test="${duplicateIssuerRepErr == 'true'}">
	                      <div class="error">
	                         <label class="error"><span><spring:message  code="err.duplicateRepresentative"/></span></label>
	                      </div> 
	                    </c:if>
	                	<div class="control-group">
	                        <label for="firstname" class="required control-label"><spring:message  code="label.firstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                        <div class="controls">
	                            <input type="text" name="firstName" id="firstName" value="${primaryRepInfoSubmitted.firstName}" class="input-large" maxlength="50">
	                            <div id="firstName_error" class=""></div>
	                        </div> 
	                    </div><!-- end of control-group -->
	                    
	                    <div class="control-group">
	                        <label for="lastname" class="required control-label"><spring:message  code="label.lastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                        <div class="controls">
	                            <input type="text" name="lastName" id="lastName" value="${primaryRepInfoSubmitted.lastName}" class="input-large" maxlength="50">
	                            <div id="lastName_error" class=""></div>
	                        </div> 
	                    </div><!-- end of control-group -->
	                    
	                    <div class="control-group">
	                        <label for="title" class="required control-label"><spring:message  code="label.title"/> </label>
	                        <div class="controls">
	                            <input type="text" name="title" id="title" value="${primaryRepInfoSubmitted.title}" class="input-large" maxlength="50">
	                        </div> 
	                    </div><!-- end of control-group -->
	                    
	                    <div class="control-group">
	                        <label for="phone" class="required control-label"><spring:message  code="label.busiPhoneNumber"/>
	                        <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
	                        </label>
	                        <div class="controls">
	                            <input type="text" name="phone1" id="phone1" value="${phone1}" maxlength="3" class="area-code input-mini" /> 
								<input type="text" name="phone2" id="phone2" value="${phone2}" maxlength="3" class="input-mini" /> 
								<input type="text" name="phone3" id="phone3" value="${phone3}" maxlength="4" class="input-mini" /> 
	                            <input type="hidden" name="phone" id="phone" class="input-large">
	                            <c:if test="${STATE_CODE != 'MN'}">
	                           		<span class="alert"><small>(Phone must be enabled to receive text messages)</small></span>
	                            </c:if>
	                            <div id="phone3_error" ></div>
	                        </div> 
	                    </div><!-- end of control-group -->
	                    
	                    <div class="control-group">
	                        <label for="email" class="required control-label"><spring:message  code="label.EmailAddress"/>
	                        <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
	                        </label>
	                        <div class="controls">
	                            <input type="text" name="email" id="email" value="${primaryRepInfoSubmitted.email}" class="input-large" maxlength="50">
<%-- 	                           	<c:if test="${repDuplicateEmail == 'false'}"> --%> <!-- commented out per HIX-25248 -->
	                           		 <div id="email_error" class=""></div>
<%-- 	                            </c:if> --%>
	                            <c:if test="${repDuplicateEmail == 'true'}">
	                               <div class="error">
	                                  <label class="error"><span><spring:message  code="err.duplicateEmail"/></span></label>
	                               </div> 
	                             </c:if>
	                             
	                        </div> 
	                    </div><!-- end of control-group -->
	                    <c:if test="${IS_EMAIL_ACTIVATION != null && IS_EMAIL_ACTIVATION == 'FALSE' && STATE_CODE != null && STATE_CODE != 'CA'}">
	                   <div class="control-group">
	                        <label for="reenterprimaryEmail" class="required control-label"><spring:message  code="label.ReenterEmailAddress"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                        <div class="controls">
	                            <input type="text" name="reenterprimaryemail" id="reenterprimaryemail" value="${primaryRepInfoSubmitted.email}" class="input-large" maxlength="50">
	                            <div id="reenterprimaryemail_error" class=""></div>
	                        </div> 
	                    </div>
	                    
	                    <div class="control-group">
	                        <label for="password" class="required control-label"><spring:message code="label.password" /> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                        <div class="controls">
	                            <input type="password" name="password" id="password" value="${primaryRepInfoSubmitted.password}" class="input-large" size="30" />
	                            <div id="password_error" class="pull-left"></div>
	                        </div>
	                    </div>
	
	                    <div class="control-group">
	                        <label for="confirmPassword" class="required control-label"><spring:message code="label.confirmPassword" /> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                        <div class="controls">
	                            <input type="password" name="confirmPassword" id="issuerConfirmPassword" value="${primaryRepInfoSubmitted.password}" class="input-large" size="30" />
	                        </div>
	                        <div id="issuerConfirmPassword_error" class="pull-left"></div>
	                    </div>
	                    </c:if>
	                    <%-- below code added by kuldeep for Jira HIX-7757 --%>
							<c:if test="${STATE_CODE != null && STATE_CODE == 'CA'}">
								<div class="control-group">
                        <label for="addressLine1" class="required control-label"><spring:message  code="label.streetaddress1"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="location.address1" id="addressLine1" value="${issuerInfoSubmitted.address1}" class="input-xlarge" maxlength="250">
                                <div id="addressLine1_error" class=""></div>
                            </div> <!-- end of controls-->
                    </div><!-- end of control-group -->
                    
                    <div class="control-group">
                        <label for="addressLine2" class="required control-label"><spring:message  code="label.streetaddress2"/><%-- <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> --%></label>
                            <div class="controls">
                                <input type="text" name="location.address2" id="addressLine2" value="${issuerInfoSubmitted.address2}" class="input-xlarge" maxlength="250">
                                <div id="addressLine2_error" class=""></div>
                            </div> <!-- end of controls-->
                    </div><!-- end of control-group -->
                    
                    <div class="control-group">
                        <label for="city" class="required control-label"><spring:message  code="label.city"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="location.city" id="city" value="${issuerInfoSubmitted.city}" class="input-xlarge" maxlength="30">
                                <div id="city_error" class=""></div>
                            </div> <!-- end of controls-->
                    </div><!-- end of control-group -->
                    
                    <div class="control-group">
                            <label for="state" class="required control-label"><spring:message  code="label.emplState"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <select size="1"  id="state" name="location.state" path="statelist">
                                     <option value="">Select</option>
                                     <c:forEach var="state" items="${statelist}">
                                        <option <c:if test="${state.code == issuerInfoSubmitted.state}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
                                    </c:forEach>
                                </select>
                                <div id="state_error"></div>
                            </div>
                        </div><!-- end of control-group -->
                        
                        <div class="control-group">
                            <label for="zip" class="required control-label"><spring:message  code="label.zipCode"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                <div class="controls">
                                    <input type="text" name="location.zip" id="zip" value="${issuerInfoSubmitted.zip}" class="input-small" maxlength="5">
                                    <div id="zip_error" class=""></div>
                                </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
						
							</c:if>
							<div class="form-actions">
	                    	<a href="<c:url value="/admin/addnewissuer"> <c:param name="isBack" value="true"/>  </c:url>" class="btn back-btn"><spring:message  code="label.back"/></a>
	                        <input type="submit" name="IssuerAddRepSubmitBtn" id="IssuerAddRepSubmitBtn" value="<spring:message  code="label.next"/>" class="btn btn-primary" title="<spring:message  code="label.next"/>"/>
	                    </div><!-- end of form-actions -->
		            </c:otherwise>
		        </c:choose>
		    </form>  
            </div>
       </div><!--end of span9-->
    </div>
</div>

<script type="text/javascript">
var csrValue = $("#csrftoken").val();
var stateCode = '${STATE_CODE}';

// duplicate user validation on the basis of email address
$(document).ready(function() {
    $.ajaxSetup({ cache: false }); // prevent Ajax caching in JQuery
    $("#email").keyup(function(){
    	$('#email_error').html("");
    });
    $("#email").focusout(function(){
    	if($("#email").val() != ""){  // if email address is not empty
    		$('#IssuerAddRepSubmitBtn').attr("disabled", false);
    		$.ajax({	
				type : "POST",
				url: "<c:url value='/admin/issuer/checkDuplicate'/>",
				data: { "userEmailId": $("#email").val(),"csrftoken":csrValue},
				success: function(responseText){
					if(responseText){
						$('#email_error').html("<label class='error'><span><em class='excl'>!</em><spring:message code='err.duplicateEmail' javaScriptEscape='true'/></span></label>");
						$('#IssuerAddRepSubmitBtn').attr("disabled", true);
					} 
		       	}
			});
		}	
	  return false; 
	});	 
});

if(stateCode !== 'MN') {
	$('#email').popover({
	    html: true,
	    placement: 'right',
	    trigger: 'click',
	    title: 'User must have access to this email address and phone number for initial account setup.',
	    content: '<label for="noshow-msg"><input id="noshow-msg" type="checkbox"> Don\'\tt show this message again</label>'
	});

	$('#email').click(function() {
		if ($('#noshow-msg').is(':checked')) {
			$('#email').popover('destroy');
		}
	});	
}

$.validator.addMethod("validatepassword", function(value, element, param) {
	password = $("#password").val();
	var re = /(?=^.{8,20}$)(?=.*[a-zA-Z])(?=.*[0-9])(?!.*\s).*$/; 
	if (!re.test(password)) {
		return false;
	}
	return true;

});
// form validations
var validator = $("#frmPrimaryRepDetails").validate({
	onkeyup : false,
	onclick : false,
	rules : {
		firstName : {required : true},
		lastName : {required : true},
		phone1 : {required : true, digits: true, phoneUS: true},
		phone2 : {required : true, digits: true, phoneUS: true},
		phone3 : {required : true, digits: true, phoneUS: true},
		email : {required : true, emailIdCheck: true},
		reenterprimaryemail : {equalTo: "#email"},
		password : {required : true, validatepassword:true},
		issuerConfirmPassword : {equalTo: "#password"},
		"location.address1" : { required : true},
		"location.city" : {required : true},
		"location.state" : { required : true},
		"location.zip" : { required : true, digits: true, minlength:5, maxlength:5}
		
	},
	messages : {
		firstName : {required : "<span><em class='excl'>!</em><spring:message  code='err.firstName' javaScriptEscape='true'/></span>"},
		lastName : {required : "<span><em class='excl'>!</em><spring:message  code='err.lastName' javaScriptEscape='true'/></span>"},
		phone3: { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneEmpty' javaScriptEscape='true'/></span>",
				  phoneUS : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>"},
		email : {required : "<span><em class='excl'>!</em><spring:message  code='err.email' javaScriptEscape='true'/></span>",
				 emailIdCheck : "<span><em class='excl'>!</em><spring:message  code='err.invalidEmail' javaScriptEscape='true'/></span>"},
		reenterprimaryemail : {required : "<span><em class='excl'>!</em><spring:message  code='err.reenterPrimaryEmail' javaScriptEscape='true'/></span>"},
		password : {required : "<span><em class='excl'>!</em><spring:message  code='err.password' javaScriptEscape='true'/></span>",
			        validatepassword : "<span> <em class='excl'>!</em><spring:message code='label.validatePasswordLength' javaScriptEscape='true'/></span>"
		           },
		phone : {phoneUS : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNo' javaScriptEscape='true'/></span>"},
		issuerConfirmPassword : {required : "<span><em class='excl'>!</em><spring:message  code='err.confirmPassword' javaScriptEscape='true'/></span>"},
		"location.address1" : { required : "<span><em class='excl'>!</em><spring:message  code='err.addressLine1' javaScriptEscape='true'/></span>"},
		"location.city" : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerCity' javaScriptEscape='true'/></span>"},
		"location.state": { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerState' javaScriptEscape='true'/></span>"},
		"location.zip" : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerZip' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	}
});

//phone number validation
$.validator.addMethod("phoneUS", function(value, element, param) {
	if($.browser.msie==true && $.browser.version=='8.0'){
		phoneNumber1 = $("#phone1").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber2 = $("#phone2").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber3 = $("#phone3").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}else{
		phoneNumber1 = $("#phone1").val().trim();
		phoneNumber2 = $("#phone2").val().trim();
		phoneNumber3 = $("#phone3").val().trim();
	}
	if((phoneNumber1 == "" || phoneNumber2 == "" || phoneNumber3 == "") || (isNaN(phoneNumber1)) || (phoneNumber1.length < 3) || (isNaN(phoneNumber2)) || (phoneNumber2.length < 3) || (isNaN(phoneNumber3)) || (phoneNumber3.length < 4)){
		return false;
	}else{
		$("#phone").val(phoneNumber1+phoneNumber2+phoneNumber3);
		return true;
	}
},"<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>");


$(function() {
	$("#IssuerAddRepSubmitBtn").click(function(e) {
		if ($("#frmPrimaryRepDetails").validate().form()) {
			$("#frmPrimaryRepDetails").submit();
		}
	});
});

$.validator.addMethod("emailIdCheck", function(value, element) {
	//var emailRegEx = new RegExp("^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$");
	var emailRegEx = new RegExp("^[_A-Za-z0-9\u002e_]+([A-Za-z0-9\u002e]+)@[A-Za-z0-9]+([\u002e][a-zA-Z]{2,})+$");
	if("" != value && !emailRegEx.test(value)){
		return false;
	}else{
		return true;
	}
});
</script>
