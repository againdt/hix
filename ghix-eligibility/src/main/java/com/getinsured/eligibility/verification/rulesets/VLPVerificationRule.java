package com.getinsured.eligibility.verification.rulesets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.verification.service.VerificationRulesService;
import com.getinsured.eligibility.verification.util.SSAPConstants;
import com.getinsured.eligibility.verification.util.SSAPUtils;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.google.gson.JsonObject;

/**
 * Encapsulates VLP Verification Rule methods
 */
@Component
public class VLPVerificationRule {

private static final Logger LOGGER = LoggerFactory.getLogger(VLPVerificationRule.class);
	
	@Autowired
	private VerificationRulesService verificationRulesService;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	
	public static void main(String[] asdf) throws Exception {
		VLPVerificationRule vlpVerificationRule = new VLPVerificationRule();
		vlpVerificationRule.verifyVLP(SSAPUtils.loadSSAPJSON(), SSAPUtils.loadVLPJSON(), null);
	}
	
	/**
	 * This rule will verify VLP Status of Household Member
	 * 
	 * @param ssapJson
	 * @param vlpJson
	 * @param ssapApplicationId
	 * @throws Exception 
	 */
	public boolean verifyVLP(String ssapJSON, String vlpJson, String ssapApplicationId) throws Exception {
		LOGGER.info("Verify VLP Rule : Starts");
		
		String status = null;
		boolean verificationStatus = false;
		JsonObject  householdMember = null;
		String lawfulPresenceVerifiedCode = null;
		String personId = null;
		
		try {
			householdMember = SSAPUtils.parseVLPJSON(ssapJSON);
			if(householdMember != null){
				personId = householdMember.get(SSAPConstants.PERSON_ID).getAsString();
				
				if(!SSAPUtils.isEmpty(vlpJson)){
					if(SSAPConstants.NO_DOCUMENT.equalsIgnoreCase(vlpJson)){
						status = SSAPConstants.NOT_VERIFIED;
					} else {
						lawfulPresenceVerifiedCode = SSAPUtils.getLawfulPresenceVerifiedCode(personId, vlpJson);
						
						if(!SSAPUtils.isEmpty(lawfulPresenceVerifiedCode) && SSAPConstants.YES.equals(lawfulPresenceVerifiedCode)){
							status = SSAPConstants.VERIFIED;
						} else {
							status = SSAPConstants.NOT_VERIFIED;
						}
					}
				} else {
					status = SSAPConstants.PENDING;
				}
				
				verificationStatus = verificationRulesService.updateVerificationStatus(Long.valueOf(ssapApplicationId),
						Long.valueOf(personId), status, SSAPConstants.VLP_VERIFICATION_RULE);
				verificationStatus = true;
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Verifying VLP : ",exception);
			throw exception;
		}
		
		LOGGER.info("Verify VLP Rule : Ends ");
		
		return verificationStatus;
	}
}
