import React from 'react';

const Dummy_Page = () => <div>I am Dummy_Page</div>;

Dummy_Page.defaultProps = {
    name: 'Dummy_Page'
};

export default  Dummy_Page;