/**
 * 
 */
package com.getinsured.hix.entity.util;

import org.springframework.stereotype.Component;

/**
 * Used for defining all the constants and which are initialized from properties file Loads the key
 * values from properties file : EE_Configuration.properties and and store in corresponding static
 * fields
 */
@Component
public final class EntityConstants {

	private EntityConstants() {
	}

	public static final String DATE_FORMAT = "MM/dd/yyyy";
	public static final String DATE_FORMAT_PLAIN = "MMddyyyy";
	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

	public static final int RESPONSE_CODE_SUCCESS = 200;
	public static final int RESPONSE_CODE_FAILURE = 404;
	public static final int INVALID_INPUT_CODE = 4001;

	public static final String RESPONSE_DESC_SUCCESS = "Success";
	public static final String RESPONSE_DESC_FAILURE = "Failure";
	public static final String INVALID_INPUT_MSG = "Invalid input: Input data is null or empty";

	public static final String STATUS_CERTIFIED = "Certified";
	public static final String STATUS_INCOMPLETE = "Incomplete";
	public static final String STATUS_PENDING = "Pending";
	public static final String STATUS_DECERTIFIED = "Decertified";
	public static final String STATUS_REGISTERED = "Registered";
	public static final String STATUS_DEREGISTERED = "DeRegistered";
	public static final String STATUS_ACTIVE = "Active";
	
	public static String STATUS_INACTIVE = "InActive";
	public static String ENTITY_DEREGISTERED = "Entity Deregistered";

	public static final String RESPONSE_SUCCESS = "SUCCESS";
	public static final String RESPONSE_FAILURE = "FAILURE";
	public static final Integer DEFAULT_DISTANCE = 5;

	public static final String AGENT = "AGENT";
	public static final String ASSISTER = "ASSISTER";
	
	public static final String ADDACTIONFLAG = "Add";
	public static final String REMOVEACTIONFLAG = "Remove";
	public static final String ENROLLMENT_TYPE_INDIVIDUAL = "FI";
	public static final String ASSISTER_ROLE = "ASSISTER";
	public static final String CA_STATE_CODE = "CA";


}
