package com.getinsured.iex.ssap.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.repository.query.Param;

import com.getinsured.iex.ssap.model.SsapApplication;

 
public interface CustomSsapApplicationRepository {
	
	public List<SsapApplication> findByCmrHouseoldIdAndCoverageYearNoAppData (@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear);
	
}
