//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonEmploymentAugmentationType;


/**
 * A data type that
 * 				supplements A data type for an employer-sponsored insurance
 * 				arrangement.
 * 
 * <p>Java class for ESIAugmentationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ESIAugmentationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://hix.cms.gov/0.1/hix-core}PersonEmploymentAugmentationType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ESI" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ESIAugmentationType", propOrder = {
    "esi"
})
public class ESIAugmentationType
    extends PersonEmploymentAugmentationType
{

    @XmlElement(name = "ESI")
    protected EmployerSponsoredInsuranceType esi;

    /**
     * Gets the value of the esi property.
     * 
     * @return
     *     possible object is
     *     {@link EmployerSponsoredInsuranceType }
     *     
     */
    public EmployerSponsoredInsuranceType getESI() {
        return esi;
    }

    /**
     * Sets the value of the esi property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmployerSponsoredInsuranceType }
     *     
     */
    public void setESI(EmployerSponsoredInsuranceType value) {
        this.esi = value;
    }

}
