var enrollmentApp = angular.module('enrollmentApp', ['ui.mask']);

enrollmentApp.controller('enrollmentAppCtrl', ['$scope', '$http', function($scope, $http) {
	var currentYear = new Date().getFullYear();
	 
	$scope.editToolJson={'firstName':'','lastName':'','policyNumber':'','ssn':'','coverageYear':currentYear,'sortOrder':'ASC','pageSize':25,'pageNumber':1};
	$scope.currentScreen = "search";
	$scope.currentConsumerId = "";
	$scope.firstTimeSearch = true;
	$scope.totalRecords=0;
	$scope.Math = window.Math;
	$scope.addressValidation = false;
	$scope.validIdahoAddress = true;
	$scope.sameEndStartDate=false;
	
	//Coverage Year display logic
	$scope.coverageYears=[];
	var years=[];	
	 for (var i = 7; i > 0; i--){
		 years.push(currentYear);
		 if(currentYear == 2015) break;
		 currentYear--;
	}
	 $scope.coverageYears = years;
	 
	 
	//Search result display/listing	
	 $scope.searchResult = [];
	 
	 $scope.setSortOrder = function(){
		 $scope.editToolJson.sortOrder = ($scope.editToolJson.sortOrder == "ASC")? "DESC" : "ASC";
		 $scope.search1095();
	 };
	
	$scope.resetSearchForm = function() {
		  var currentYear = new Date().getFullYear();	
		  $scope.editToolJson = {'firstName':'','lastName':'','policyNumber':'','ssn':'','coverageYear':currentYear,'sortOrder':'ASC','pageSize':25,'pageNumber':1};
		  $scope.firstTimeSearch = true;
		};
		
	
		$scope.goSearch=function(){
			$scope.currentPage=1;
			$scope.editToolJson.pageNumber = 1;
			$scope.search1095();			
		};
	$scope.search1095 = function(){		
		var webJsonstring = JSON.stringify($scope.editToolJson).replace(/&quot;/g,'"');
		$scope.showAlertBox("loader");
		var url = baseURL + "/enrollment/1095A/searchmember";
		$.ajax({
			type : 'POST',
			url : url,
			dataType : 'json',
			data : {
				csrftoken :  $('#tokid').val(),			
				editToolJson:webJsonstring			
			},		
			success : function(response,textStatus,xhr) {
				if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
					  alert($('Session is invalid').text());
					  return false;
				}
				$scope.$apply(function(){
					$scope.searchResult =  response.data;
					$scope.closeAlertBox();
					$scope.firstTimeSearch = false;
					$scope.totalPages = Math.round(response.totalRecords/$scope.editToolJson.pageSize);
					$scope.totalRecords = response.totalRecords;
					$scope.pagination();
				});
				
					
			},
			error :function(XMLHttpRequest, textStatus, errorThrown) {
				if(XMLHttpRequest.status == 403 && XMLHttpRequest.getResponseHeader('InvalidCSRFToken') == 'true') {			
					  alert($('Session is invalid').text());
					  return false;
				}
				$scope.closeAlertBox();			
				$scope.showAlertBox("failure");
				
			}
		});
	};
		//Search result pagination
		
		
		  
		
		/*		
		var params = {
				"csrftoken": $('#tokid').val()
		};
		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'},
			params:params
		};
		var data = {
            	editToolJson:webJsonstring
            };
		var serverresponse = $http({
		    url: '',
		    method: 'POST',
		    params: {editToolJson:webJsonstring},
		    headers: params,		    
		}).then(function successCallback(response) {
			$scope.searchResult = response.data;
			return response;
		}, function errorCallback(response) {
			$scope.searchResult = [];
			return	response;
		});
		*/
	
	
	$scope.filteredRecord = []
	  ,$scope.currentPage = 1
	  ,$scope.pagesPerSet = 10
	  ,$scope.pageSet = 1
	  ,$scope.totalPages = 15;
		$scope.pages = [];
		
	$scope.pagination = function(){
		$scope.pages = [];
		for (i=0;i<$scope.pagesPerSet;i++) {
			if($scope.totalPages<($scope.pageSet+i)){break; return;} 
			page=$scope.pageSet+i;
			currentPage=false;
			activeClass = '';	
			if(page==$scope.currentPage){
				currentPage=true;
				activeClass = 'active';
			}			 
		    $scope.pages.push({ pageNo:page, currentPage:currentPage, activeClass:activeClass});
		}		
	};
	$scope.nextPage = function(){
		$scope.pageSet=$scope.pageSet+$scope.pagesPerSet;
		$scope.currentPage=$scope.pageSet;
		$scope.editToolJson.pageNumber=$scope.currentPage;
		$scope.search1095();
		//$scope.pagination();
	};
	$scope.prePage = function(){
		$scope.pageSet= $scope.pageSet-$scope.pagesPerSet;
		$scope.currentPage=$scope.pageSet;
		$scope.editToolJson.pageNumber=$scope.currentPage;
		$scope.search1095();
		//$scope.pagination();
	};
	
	$scope.pageClick = function(pageNo){
		$scope.currentPage=pageNo;
		$scope.editToolJson.pageNumber = pageNo;
		$scope.search1095();
	};
	$scope.pagination();
	  
	
	/******  Consumer Details***************/
	$scope.editDetailMod = false;
	$scope.enrollment1095Details = $('#enrollment1095Details').val();
	
	$scope.editDetail = function(){
		$scope.editDetailMod = true;
	};
	
	$scope.modalForm = {"edit1095": false,"subResult":"","overrideComment":"","processing":false};
	$scope.maxLength = 400;

	
	$scope.enrollment1095Details_org={};
	$scope.enrollment1095Details = {};
	//angular.copy($scope.enrollment1095Details, $scope.enrollment1095Details_org);
	$scope.spouse = {};
	$scope.recipent = {};
	$scope.members = [];
	$scope.isSpouse = false;
		
	memcnt=0;
	try{
	angular.forEach($scope.enrollment1095Details.enrollmentMembers, function(val, key){
		if (val.memberType == 'RECEPIENT'){
			$scope.recipent = $scope.enrollment1095Details.enrollmentMembers[key];							
		} else if (val.memberType == 'SPOUSE'){
			$scope.spouse = $scope.enrollment1095Details.enrollmentMembers[key];
			$scope.isSpouse = true;
		} else if (val.memberType == 'MEMBER'){
			$scope.members[memcnt] = $scope.enrollment1095Details.enrollmentMembers[key];
			memcnt++;
		}	
	});
	} catch (e){};
	
	
	$scope.getDetail = function(consumerId){		
		if ($scope.currentConsumerId == consumerId){
			$scope.currentScreen = "consumerDetail";
			return;
		} else {
			$scope.currentScreen = consumerId;
		}
		$scope.showAlertBox("loader");
		var url = baseURL + "/enrollment/1095A/getDetailById/";
		$.ajax({
			type : 'POST',
			url : url,
			dataType : 'json',
			data : {
				csrftoken :  $('#tokid').val(),			
				id:consumerId	
			},		
			success : function(response,textStatus,xhr) {
				if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
					  alert($('Session is invalid').text());
					  return false;
				}
					$scope.$apply(function(){						
						angular.copy(response,$scope.enrollment1095Details_org);
						angular.copy($scope.enrollment1095Details_org,$scope.enrollment1095Details);	
						if($scope.enrollment1095Details.isActive=='N'){
							$scope.isActive="Fail";
						}else if($scope.enrollment1095Details.isActive=='Y'){
							$scope.isActive="Pass";
						}else{
							$scope.isActive="";
						}
						try{
							memcnt=0;
							angular.forEach($scope.enrollment1095Details.enrollmentMembers, function(val, key){
								if (val.memberType == 'RECEPIENT'){
									$scope.recipent = $scope.enrollment1095Details.enrollmentMembers[key];							
								} else if (val.memberType == 'SPOUSE'){
									$scope.spouse = $scope.enrollment1095Details.enrollmentMembers[key];
									$scope.isSpouse = true;
								} else if (val.memberType == 'MEMBER'){
									$scope.members[memcnt] = $scope.enrollment1095Details.enrollmentMembers[key];
									memcnt++;
								}	
							});
							} catch (e){};								
						$scope.editDetailMod = false;
						$scope.currentScreen = "consumerDetail";
						$scope.closeAlertBox()
					});			
				$scope.closeProcessingDialog();
				$scope.getTotals();
			},
			error :function(XMLHttpRequest, textStatus, errorThrown) {
				if(XMLHttpRequest.status == 403 && XMLHttpRequest.getResponseHeader('InvalidCSRFToken') == 'true') {			
					  alert($('Session is invalid').text());
					  return false;
				}
				$scope.closeProcessingDialog();			
				$scope.showAlertBox("failure");
				$scope.closeAlertBox();
			}
		});
	};
	$scope.cancelEditl = function(){
		//document.getElementById("frmConsuerDetail").reset();
		
		//angular.copy(vjson , $scope.enrollment1095Details);
		
		angular.copy($scope.enrollment1095Details_org, $scope.enrollment1095Details);
		memcnt = 0;
		try{
			angular.forEach($scope.enrollment1095Details.enrollmentMembers, function(val, key){
				if (val.memberType == 'RECEPIENT'){
					$scope.recipent = $scope.enrollment1095Details.enrollmentMembers[key];							
				} else if (val.memberType == 'SPOUSE'){
					$scope.spouse = $scope.enrollment1095Details.enrollmentMembers[key];
					$scope.isSpouse = true;
				} else if (val.memberType == 'MEMBER'){
					$scope.members[memcnt] = $scope.enrollment1095Details.enrollmentMembers[key];
					memcnt++;
				}
			});
		} catch (e){};
		//$scope.enrollment1095Details = angular.copy(vjson);
		$scope.frmConsuerDetail.$setPristine();
		$scope.editDetailMod = false;
	};
	
	$scope.showAlertBox = function(screen){
		$scope.modalForm.subResult = screen;
		$scope.modalForm.edit1095 = true;		
		$('#ssapSubmitModal').modal();
	};
	
	$scope.resendDetail = function(){
		$scope.showAlertBox("loader");		
		url = baseURL + "/enrollment/1095A/resend";
		var id=$scope.enrollment1095Details.id;
		$.ajax({
			type : 'POST',
			url : url,
			dataType : 'json',
			data : {
				csrftoken :  $('#tokid').val(),			
				id:id			
			},			
			success : function(response,textStatus,xhr) {
				if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
					  alert($('Session is invalid').text());
					  return false;
				}
				$scope.$apply(function(){
					$scope.showAlertBox("success");			
					$scope.editDetailMod = false;
					$scope.enrollment1095Details.resendPdfFlag = "Y";
				});				
			},
			error :function(XMLHttpRequest, textStatus, errorThrown) {
				if(XMLHttpRequest.status == 403 && XMLHttpRequest.getResponseHeader('InvalidCSRFToken') == 'true') {			
					  alert($('Session is invalid').text());
					  return false;
				}
				$scope.$apply(function(){
					$scope.showAlertBox("failure");
				});
				
			}
		});
	};
	
	$scope.closeAlertBox = function(){
		$('#ssapSubmitModal').modal('hide');
		$scope.modalForm.edit1095 = false;
		$scope.modalForm.subResult = "";		
	};
	
	$scope.saveDetail = function(){
		if($('.zip-warning').is(':visible')){
			$('#validationModal .modal-body').html('<p>Please Correct all values before submitting page</p>');
			$('#validationModal').modal();
			return false;
		}
		
		if($scope.validIdahoAddress==false){
			$('#noAddressModal .modal-body').html('<p>The home address you entered is not in the postal database. Please check it for accuracy.</p>');
			$('#noAddressModal').modal();					
			return false;
		}
		$scope.modalForm.overrideComment = "";
		$scope.showAlertBox("reason");
	};
	$scope.saveContinue = function(){
		$scope.showAlertBox("loader");
		//$scope.showProcessingDialog();
		$scope.enrollment1095Details.updateNotes = $scope.modalForm.overrideComment;
		var webJsonstring = $scope.enrollment1095Details;
		webJsonstring = angular.toJson(webJsonstring );
		//webJsonstring = JSON.stringify(webJsonstring).replace(/&quot;/g,'"');		
		var headerparams = {
				"csrftoken": $('#tokid').val()
		};
		url = baseURL + "/enrollment/1095A/update";		
		
		$.ajax({
			type : 'POST',
			url : url,
			dataType : 'json',
			data : {
				csrftoken :  $('#tokid').val(),			
				editToolJson:webJsonstring			
			},			
			success : function(response,textStatus,xhr) {
				if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
					  alert($('Session is invalid').text());
					  return false;
				}
				angular.copy(response,$scope.enrollment1095Details_org);
				angular.copy($scope.enrollment1095Details_org,$scope.enrollment1095Details);	
				
				$scope.$apply(function(){
					try{
						memcnt=0;
						angular.forEach($scope.enrollment1095Details.enrollmentMembers, function(val, key){
							if (val.memberType == 'RECEPIENT'){
								$scope.recipent = $scope.enrollment1095Details.enrollmentMembers[key];							
							} else if (val.memberType == 'SPOUSE'){
								$scope.spouse = $scope.enrollment1095Details.enrollmentMembers[key];
								$scope.isSpouse = true;
							} else if (val.memberType == 'MEMBER'){
								$scope.members[memcnt] = $scope.enrollment1095Details.enrollmentMembers[key];
								memcnt++;
							}	
						});
						} catch (e){};		
					$scope.showAlertBox("success");			
					$scope.editDetailMod = false;	
				});
							
			},
			error :function(XMLHttpRequest, textStatus, errorThrown) {
				if(XMLHttpRequest.status == 403 && XMLHttpRequest.getResponseHeader('InvalidCSRFToken') == 'true') {			
					  alert($('Session is invalid').text());
					  return false;
				}
				$scope.$apply(function(){
					$scope.modalForm.subResult = "failure";
				});
			}
		});
	
		/*
		
		var serverresponse = $http({
		    url:url,
		    method: 'POST',
		    data: {enrollment1095:webJsonstring},
		    headers: headerparams,		    
		}).then(function successCallback(response) {
			angular.copy($scope.enrollment1095Details,$scope.enrollment1095Details_org);			
			//$scope.closeProcessingDialog();
			$scope.showAlertBox("success");			
			$scope.editDetailMod = false;			
			return response;
		}, function errorCallback(response) {
			//$scope.closeProcessingDialog();			
			$scope.showAlertBox("failure");
			return	response;
		});*/		
	};
	
	$scope.showProcessingDialog = function(){
		$scope.modalForm.processing = true;		
		$('#processingModal').modal();		
	};
	
	$scope.closeProcessingDialog = function(){
		$('#processingModal').modal('hide');
		$scope.modalForm.processing = false;
	};
	
	$scope.backToSearch = function(){
		$scope.currentScreen = "search";
	};
	
	$scope.getTotals = function(){
		var grossPremium=0;var pediatricEhbAmt=0;var slcspAmount=0;var  aptcAmount=0; var totalPremium=0;
		angular.forEach($scope.enrollment1095Details.enrollmentPremiums, function(val, key){
				 grossPremium 	 = (val.grossPremium!=null) ? eval(grossPremium +	parseFloat(val.grossPremium)) : grossPremium;
				 if(val.pediatricEhbAmt !=null){ 
					 pediatricEhbAmt = eval(pediatricEhbAmt +	parseFloat(val.pediatricEhbAmt));	 
				 }
				 slcspAmount 	 = (val.slcspAmount!=null) ? eval(slcspAmount +	parseFloat(val.slcspAmount)) : slcspAmount;
				 aptcAmount  	 = (val.aptcAmount !=null) ? eval(aptcAmount +	parseFloat(val.aptcAmount)) : aptcAmount;
				 if(val.grossPremium !=null && val.ehbPercent!=null){
					 totalPremium = eval(totalPremium + (parseFloat(val.grossPremium) * parseFloat(val.ehbPercent)));
				 }
				 if($scope.enrollment1095Details.coverageYear < 2017 && val.pediatricEhbAmt !=null && val.accountableMemberDental!=null){
					 totalPremium = eval(totalPremium + (parseFloat(val.pediatricEhbAmt)  * parseFloat(val.accountableMemberDental)));	 
				 }
				 if($scope.enrollment1095Details.coverageYear >= 2017 && val.pediatricEhbAmt !=null){
					 totalPremium = eval(totalPremium + parseFloat(val.pediatricEhbAmt));	 
				 }
				 
		});
		totalPremium = parseFloat(totalPremium).toFixed(2);
		pediatricEhbAmt = (pediatricEhbAmt<=0) ? '' : (parseFloat(pediatricEhbAmt).toFixed(2));
		
		$('#grossPremiumTotal').html(parseFloat(grossPremium).toFixed(2));
		$('#pediatricEhbAmtTotal').html(pediatricEhbAmt);
		$('#PremiumTotal').html(parseFloat(totalPremium).toFixed(2));
		$('#slcspAmountTotal').html(parseFloat(slcspAmount).toFixed(2));
		$('#aptcAmountTotal').html(parseFloat(aptcAmount).toFixed(2));
	};
	
	$scope.changeTotal = function(val1,val2, val3,val4,indx){
		if(val1==null || val1==undefined)
				val1=0;
		if(val3!=null && val4!=null && $scope.enrollment1095Details.coverageYear < 2017 ){
			var total = eval((val1*val2)+(val3*val4));
		}else if(val3!=null && $scope.enrollment1095Details.coverageYear >= 2017 ){
			var total = eval((val1*val2)+val3);
		}else{
			var total = eval((val1*val2));
		}
		total = parseFloat(total).toFixed(2);
		//var elemName = 'total_premium_'+indx;
		$('#total_premium_'+indx).html(total);
	};
	
	$scope.syncMembers = function(memeberId, ismember){
		for(i=0;i<$scope.members.length;i++){
			if($scope.members[i].memberId == $scope.recipent.memberId){
				if(ismember==false){
					$scope.members[i].firstName = $scope.recipent.firstName;
					$scope.members[i].middleName = $scope.recipent.middleName;
					$scope.members[i].lastName = $scope.recipent.lastName;
					$scope.members[i].ssn = $scope.recipent.ssn;
					$scope.members[i].birthDate = $scope.recipent.birthDate;
				}else{
					$scope.recipent.firstName=$scope.members[i].firstName;
					$scope.recipent.middleName=$scope.members[i].middleName;
					$scope.recipent.lastName=$scope.members[i].lastName;
					$scope.recipent.ssn=$scope.members[i].ssn;
					$scope.recipent.birthDate=$scope.members[i].birthDate;
				}
			}
			if($scope.members[i].memberId == $scope.spouse.memberId){
				if(ismember==false){
					$scope.members[i].firstName = $scope.spouse.firstName;
					$scope.members[i].middleName = $scope.spouse.middleName;
					$scope.members[i].lastName = $scope.spouse.lastName;
					$scope.members[i].ssn = $scope.spouse.ssn;
					$scope.members[i].birthDate = $scope.spouse.birthDate;
				} else {
					$scope.spouse.firstName=$scope.members[i].firstName;
					$scope.spouse.middleName=$scope.members[i].middleName;
					$scope.spouse.lastName=$scope.members[i].lastName;
					$scope.spouse.ssn=$scope.members[i].ssn;
					$scope.spouse.birthDate=$scope.members[i].birthDate;
				}
			}
		}
	};
	
	$scope.addressCheck = function(){		
		if($scope.recipent.address1 === undefined || $scope.recipent.city === undefined || $scope.recipent.zip === '' || $('#addressValidationModal').hasClass('in')){
			return;
		}
		
		var enteredAddress = $scope.recipent.address1 + ',' + (($scope.recipent.address2 === null || $scope.recipent.address2 === undefined) ? '': $scope.recipent.address2) + ',' + $scope.recipent.city + ',' + (($scope.recipent.state === null || $scope.recipent.state === undefined) ? '': $scope.recipent.state) + ',' + $scope.recipent.zip;
		var ids = "home_addressLine1~home_addressLine2~home_primary_city~home_primary_state~home_primary_zip~homelat~homelon~rdihome~countyhome";
		$http({
			method : 'GET',
			url : "/hix/platform/validateaddressnew?enteredAddress=" + enteredAddress+"&ids=" + ids
		}).success(function(response) {			
			if(response.toUpperCase() === 'SUCCESS'){
				$scope.addressValidation = true;
				$scope.validIdahoAddress = false;
				$.get('/hix/platform/address/viewvalidaddressnew',function(response){
					var html = $(response).find("#frmviewAddressList").html(); 
					$('#addressValidationModal .modal-body').html(html);
					$('#addressValidationModal').find('#submitAddr').remove();
					$('#addressValidationModal').find('#back_button_from_iframe').remove();
					$('#addressValidationModal').modal({
						backdrop: 'static', keyboard: false
					});
				});
			}else if(response.substr(0,7).toUpperCase() === 'FAILURE'){
				$scope.addressValidation = false;
				
				$('#noAddressModal .modal-body').html('<p>The home address you entered is not in the postal database. Please check it for accuracy.</p>');
				$('#noAddressModal').modal();
				$scope.validIdahoAddress = false;
			}else if(response.substr(0,11).toUpperCase() === 'MATCH_FOUND'){
				$scope.addressValidation = true;
				$scope.validIdahoAddress = true;
			}

		}).error(function(data) {
			$('#addressValidationModal').modal('hide');
		});  

	};
	
	$scope.updateAddress = function(){
		var selectedAddressArr = $('#addressValidationModal input:radio:checked').val().split(",");
		//$scope.recipent.address1 + ',' + ($scope.recipent.address2 === undefined? '': this.data.addressLine2) + ',' + this.data.city + ',' + this.data.state + ',' + this.data.zipcode;
		if($scope.recipent.address1 != selectedAddressArr[0]){
			$scope.validIdahoAddress = true;
		}
		$scope.recipent.address1 = selectedAddressArr[0];

		if(($scope.recipent.address2 != null) && ($scope.recipent.address2.trim() != selectedAddressArr[1].trim())){
			$scope.validIdahoAddress = true;
		}
		$scope.recipent.address2 = selectedAddressArr[1] === 'null'? '' : selectedAddressArr[1].trim();

		if(selectedAddressArr[2] !== undefined && $scope.recipent.city.trim() !== selectedAddressArr[2].trim()){
			$scope.validIdahoAddress = true;
		}
		$scope.recipent.city = selectedAddressArr[2].trim();

		if(selectedAddressArr[4] !== undefined && $scope.recipent.zip.trim() !== selectedAddressArr[4].trim()){
			$scope.validIdahoAddress = true;
		} 
		$scope.recipent.zip = selectedAddressArr[4].trim();

		if(selectedAddressArr[3] !== undefined && $scope.recipent.state.trim() !== selectedAddressArr[3].trim()){
			$scope.validIdahoAddress = true;
		} 
		$scope.recipent.state = selectedAddressArr[3].trim();
	};


$scope.changeSD = function(enrollment1095DetailsScope) {

	angular.forEach(enrollment1095DetailsScope.members, function(val, key){

	var inputDate=enrollment1095DetailsScope.enrollment1095Details.policyStartDate;
	var inputEndDate=enrollment1095DetailsScope.enrollment1095Details.policyEndDate;
	
	if(inputDate=="undefined")
		{return;}
	inputDate = inputDate.replace(/(\d\d)(\d\d)(\d\d\d\d)/g, '$1/$2/$3');
	inputDate=moment(inputDate);
	var startDate =moment(val.coverageStartDate);
	var endDate =moment(val.coverageEndDate);


	if(inputDate.isValid()==false)
		{return;}

	var flag=true;
				
	if(startDate.diff(inputDate) < 0)
		{
			flag=false;
		}
	
	if(inputDate.diff(inputEndDate)==0)
	{
		$scope.frmConsuerDetail.policyStartDate.$setValidity("sameEndStartDate",false);
	}else{
		$scope.frmConsuerDetail.policyStartDate.$setValidity("sameEndStartDate",true);
	}

	angular.forEach(angular.element("ng-form[name=frm4]"),function(frmObj){
		var frmScope=angular.element(frmObj).scope();	
		frmScope.frm4.memberCoverageStartDate.$setValidity("validPolicyStartDate_"+key,flag);	
	})
	

	});
};

								
$scope.changeED = function(enrollment1095DetailsScope) {
	angular.forEach(enrollment1095DetailsScope.members, function(val, key){

		var inputDate=enrollment1095DetailsScope.enrollment1095Details.policyEndDate;
		var inputStartDate=enrollment1095DetailsScope.enrollment1095Details.policyStartDate;
		
		if(inputDate=="undefined")
		{return;}
		inputDate = inputDate.replace(/(\d\d)(\d\d)(\d\d\d\d)/g, '$1/$2/$3');
		inputDate=moment(inputDate);
		var startDate =moment(val.coverageStartDate);
		var endDate =moment(val.coverageEndDate);

		if(inputDate.isValid()==false)
		{return;}

		var flag=true;
		if(inputDate.diff(endDate)<0)
			{
				flag=false;
			}
		
		if(inputDate.diff(inputStartDate)==0)
		{
			$scope.frmConsuerDetail.policyEndDate.$setValidity("sameEndStartDate",false);
		}else{
			$scope.frmConsuerDetail.policyEndDate.$setValidity("sameEndStartDate",true);
		}
		
		angular.forEach(angular.element("ng-form[name=frm5]"),function(frmObj){
		var frmScope=angular.element(frmObj).scope();	
		frmScope.frm5.memberCoverageEndDate.$setValidity("validPolicyEndDate_"+key,flag);	
		})

	});


};

// $scope.changeED = function() {
// 	var date = $("#policyEndDate").val().replace(/[A-Z]/g, '');
// 	var inputDate=moment(date,'MM/DD/YYYY');
		
// 				var EndDate=moment($("#memberCoverageEndDate").val(),'MM/DD/YYYY');
// 				if(EndDate.isValid())
// 				{
// 					var frmScope=angular.element("ng-form[name=frm5]").scope();
// 					if(inputDate.diff(EndDate) < 0)
// 					{
// 						frmScope.frm5.memberCoverageEndDate.$setValidity("CPED",false);
// 					}
// 					else{
// 						frmScope.frm5.memberCoverageEndDate.$setValidity("CPED",true);	
// 					}
// 				}
// 	};
// });
$scope.onMemberStartDateChange=function(enrollment1095Details, memberScope, index)
{
	//console.log(arguments);
	var psDtObj=moment(enrollment1095Details.policyStartDate);
	var peDtObj=moment(enrollment1095Details.policyEndDate);
	var thisObj=moment(memberScope.frm4.memberCoverageStartDate.$viewValue,'MM/DD/YYYY');

	var flag=true;

	if(thisObj.isValid()==false)
		{return;}

 	if(psDtObj.isValid() && thisObj.isValid())
		{
 			if(thisObj.diff(psDtObj)<0)
				{
					flag=false;
				}
		};

	if(psDtObj.isValid() && thisObj.isValid())
		{
			if(peDtObj.diff(thisObj)<0)
				{
					flag=false;
				}
		};

memberScope.frm4.memberCoverageStartDate.$setValidity("validPolicyStartDate",flag);
memberScope.frm4.memberCoverageStartDate.$setValidity("validPolicyStartDate_"+index,true);
//$("#validIndexedStartDate").hide();

};

$scope.onMemberEndDateChange=function(enrollment1095Details, memberScope,index)
{
	var psDtObj=moment(enrollment1095Details.policyStartDate);
	var peDtObj=moment(enrollment1095Details.policyEndDate);
	var thisObj=moment(memberScope.frm5.memberCoverageEndDate.$viewValue,'MM/DD/YYYY');

	var flag=true;

	if(thisObj.isValid()==false)
		{return;}

	if(peDtObj.isValid() && thisObj.isValid())
		{
		if(peDtObj.diff(thisObj)<0)
			{
				flag=false;
			}
	};
	if(psDtObj.isValid() && thisObj.isValid())
		{
		if(psDtObj.diff(thisObj)>0)
			{
				flag=false;
			}
	};

	memberScope.frm5.memberCoverageEndDate.$setValidity("validPolicyEndDate",flag);
	memberScope.frm5.memberCoverageEndDate.$setValidity("validPolicyEndDate_"+index,true);
	//$("#validIndexedEndDate").hide();

};

		
	
}]);

(function(){
	var dateValidation = function(){
		return {
			restrict : 'AE',
			require : 'ngModel',
			scope: {
			      psDt: '=',
			      peDt:'='
			    },
			link : function(scope, element, attrs, ngModel) {		
				
				var dateType = attrs.dateValidation;
				
				function isDateValid(value){
					//remove placeholder
					var date = value.replace(/[A-Z]/g, '');
					
					//remove extra-last-character
					if(date.length === 11){
						date = date.substring(0, date.length - 1);
					}
					
					
					if(date.length === 10){
						if(!moment(date,'MM/DD/YYYY').isValid()){
							return false;
						}
						
						if(dateType === 'dob'){
							if(ageCheck('between',date, 0, 104)){
								return true;
							}else{
								return false;
							};
						}else if(dateType=='PSDate'){
							var today=moment();
							var inputDate=moment(date,'MM/DD/YYYY');
							if(inputDate.year() > today.year())
							{
								return false;
							}
							else{
							
							return true;
							};
						}else if(dateType=='PEDate'){
							var today=moment();
							var inputDate=moment(date,'MM/DD/YYYY');
							if(inputDate.year() > today.year())
							{
								return false;
							} 

							if(typeof scope.psDt !='undefined'){
								var startDate=moment(scope.psDt,'MM/DD/YYYY');
								if(startDate.isValid())
								{
									if(startDate.diff(inputDate) > 0)
									{
										return false;
									}
								}
							};
									
							return true;
						}else if(dateType == 'CPSD'){
							var pstDate = scope.psDt.split('/');
							var pedDate = scope.peDt.split('/');
							var inputDate = date.split('/');
							pstDate1 = new Date(pstDate[2], pstDate[0]-1, pstDate[1], 0, 0, 0, 0);
							pedDate1 = new Date(pedDate[2], pedDate[0]-1, pedDate[1], 0, 0, 0, 0);
							inputDate1 = new Date(inputDate[2], inputDate[0]-1, inputDate[1], 0, 0, 0, 0);
							if(inputDate1  >= pstDate1 && inputDate1 <= pedDate1){
								return true;
							}
							else{
								return false;
							}
						}
						else if(dateType == 'CPED'){
							var pstDate = scope.psDt.split('/');
							var pedDate = scope.peDt.split('/');
							var inputDate = date.split('/');
							pstDate1 = new Date(pstDate[2], pstDate[0]-1, pstDate[1], 0, 0, 0, 0);
							pedDate1 = new Date(pedDate[2], pedDate[0]-1, pedDate[1], 0, 0, 0, 0);
							inputDate1 = new Date(inputDate[2], inputDate[0]-1, inputDate[1], 0, 0, 0, 0);
							if(inputDate1  >= pstDate1 && inputDate1 <= pedDate1){
								return true;
							}
							else{
								return false;
							}
						}
					}else{
						return true;
					}
					
				}

				function ageCheck(comparator, birth, age, maxAge){
					var birthDate = new Date(birth);
					var today = new Date();
					
					 var years = (today.getFullYear() - birthDate.getFullYear());

				    if (today.getMonth() < birthDate.getMonth() || today.getMonth() == birthDate.getMonth() && today.getDate() < birthDate.getDate()) {
				        years--;
				    }
				    
				    if(comparator === 'older'){
				    	if(years >= parseInt(age)){
					    	return true;
					    }else{
					    	return false;
					    }
					}else if(comparator === 'between'){
						if(years >= parseInt(age) && years <= parseInt(maxAge)){
					    	return true;
					    }else{
					    	return false;
					    }
					}else{
						if(years < parseInt(age)){
					    	return true;
					    }else{
					    	return false;
					    }
					}
					
				};
				ngModel.$parsers.unshift(function(value){
					var dateValidationResult = isDateValid(value);
					
					ngModel.$setValidity(dateType, dateValidationResult);
					
					return value;	
				});
				
				 ngModel.$formatters.unshift(function(value) {
					 if(value === undefined || value === null){
						 return;
					 }
					 var dateValidationResult = isDateValid(value);
						
					 ngModel.$setValidity(dateType, dateValidationResult);
					 
					 return value;
	             });
				
				
			}
		};

	};
	angular.module('enrollmentApp').directive("dateValidation", dateValidation);
})();

// (function(){
// 	angular.module('enrollmentApp')
// 	.directive("validPolicyStartDate", noMoreThan);

// 	function noMoreThan()
// 	{
// 		return {
// 			restrict:'A',
// 			require : 'ngModel',
// 			priority: 99,
// 			scope:{
// 				psDt:"=",
// 				peDt:"="
// 			},
// 			link:function($scope, $attr, $elem, ctrl)
// 			{
// 				$scope.$watch("psDt",function(val){
// 					checkValidity();
// 				});
// 				$scope.$watch("peDt",function(val){
// 					checkValidity();
// 				});
// 				function checkValidity(val)
// 				{
// 					var value=val==undefined?ctrl.$viewValue:val;
// 					var psDtObj=moment($scope.psDt,'MM/DD/YYYY');
// 					var thisObj = value.replace(/[A-Z]/g, "");
// 					thisObj = thisObj.split('/');
// 					thisObj = new Date(thisObj[2], thisObj[0]-1, thisObj[1], 0, 0, 0, 0);
// 					thisObj=moment(thisObj);
// 					var peDtObj=moment($scope.peDt,'MM/DD/YYYY');
					
// 					var flag=true;
// 					if(thisObj.isValid()==false){return;}
// 					if(psDtObj.isValid() && thisObj.isValid())
// 					{
// 						if(thisObj.diff(psDtObj)<0)
// 						{
// 							flag=false;
// 						}
// 					};
// 					if(peDtObj.isValid() && thisObj.isValid())
// 					{
// 						if(peDtObj.diff(thisObj)<0)
// 						{
// 							flag=false;
// 						}
// 					};

// 					ctrl.$setValidity("validPolicyStartDate",flag);

// 					return flag;

// 				};
// 				ctrl.$validators.validPolicyStartDate=function(value)
// 				{
// 					return checkValidity(value);
// 				}
// 				/*ngModel.$parsers.push(function(value){
// 					checkValidity();
					
// 						return value;	
// 				});*/
// 			}
// 		}
// 	}

// })();

// (function(){
// 	angular.module('enrollmentApp')
// 	.directive("validPolicyEndDate", noMoreThan);

// 	function noMoreThan()
// 	{
// 		return {
// 			restrict:'A',
// 			require : 'ngModel',
// 			priority: 98,
// 			scope:{
// 				psDt:"=",
// 				peDt:"=",
// 				msDt:"="
// 			},
// 			controller:function($scope)
// 			{

// 			},
// 			link:function(scope, element, attrs, ngModel)
// 			{
// 				scope.$watch("psDt",function(val){
// 					checkValidity();
// 				});
// 				scope.$watch("peDt",function(val){
// 					checkValidity();
// 				});
// 				function checkValidity()
// 				{
// 					var psDtObj=moment(scope.psDt,'MM/DD/YYYY');
// 					var thisObj = ngModel.$viewValue.replace(/[A-Z]/g, "");
// 					thisObj = thisObj.split('/');
// 					thisObj = new Date(thisObj[2], thisObj[0]-1, thisObj[1], 0, 0, 0, 0);
// 					thisObj=moment(thisObj);
// 					var peDtObj=moment(scope.peDt,'MM/DD/YYYY');
// 					var msdtObj=moment(scope.msDt,'MM/DD/YYYY');

// 					//var regTest=new RegExp(/[A-Z]/ig);
// 					//if(regTest.test(ngModel.$viewValue)){return;}
// 					var flag=true;
// 					if(thisObj.isValid()==false){return;}
// 					if(psDtObj.isValid() && thisObj.isValid())
// 					{
// 						if(thisObj.diff(psDtObj)<0)
// 						{
// 							flag=false;
// 						}
// 					};
// 					if(peDtObj.isValid() && thisObj.isValid())
// 					{
// 						if(peDtObj.diff(thisObj)<0)
// 						{
// 							flag=false;
// 						}
// 					};
// 					if(msdtObj.isValid() && thisObj.isValid())
// 					{
// 						if(msdtObj.diff(thisObj)>0)
// 						{
// 							flag=false;
// 						}
// 					};


// 					ngModel.$setValidity("validPolicyEndDate",flag);

// 				};
// 				ngModel.$parsers.push(function(value){
// 					checkValidity();
// 						return value;	
// 				});
// 			}
// 		}
// 	}

// })();


