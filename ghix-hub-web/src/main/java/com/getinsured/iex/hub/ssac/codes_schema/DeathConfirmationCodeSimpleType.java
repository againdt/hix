
package com.getinsured.iex.hub.ssac.codes_schema;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeathConfirmationCodeSimpleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DeathConfirmationCodeSimpleType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Confirmed"/>
 *     &lt;enumeration value="Unconfirmed"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DeathConfirmationCodeSimpleType", namespace = "http://codes.ssac.ee.sim.dsh.cms.hhs.gov")
@XmlEnum
public enum DeathConfirmationCodeSimpleType {


    /**
     * Confirmed if the death can be confirmed by SSA 
     * 
     */
    @XmlEnumValue("Confirmed")
    CONFIRMED("Confirmed"),

    /**
     * Unconfirmed if the Death cannot be confirmed 
     *                         (due to unavailable data or if it cannot be disclosed). 
     * 
     */
    @XmlEnumValue("Unconfirmed")
    UNCONFIRMED("Unconfirmed");
    private final String value;

    DeathConfirmationCodeSimpleType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeathConfirmationCodeSimpleType fromValue(String v) {
        for (DeathConfirmationCodeSimpleType c: DeathConfirmationCodeSimpleType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
