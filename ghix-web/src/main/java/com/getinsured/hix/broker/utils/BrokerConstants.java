package com.getinsured.hix.broker.utils;

import org.springframework.stereotype.Component;

@Component
public final class BrokerConstants {

	private BrokerConstants() {
	}
	public static final String DATE_FORMAT_PLAIN = "MMddyyyy";
	public static final String EXT_EMPLOYER = "extEmployer";
	public static final String EXT_INDIVIDUAL = "extIndividual";
	public static final String IS_BROKER_SWITCH_TO_EMPL_VIEW = "isBrokerSwitchToEmplView";
	public static final String IS_BROKER_SWITCH_TO_INDV_VIEW = "isBrokerSwitchToIndvView";
	public static final String IS_PAYMENT_ENABLED = "isPaymentEnabled";
	public static final String YES = "Y";
	public static final String NO = "N";
	public static final String COMMA = ",";
	public static final String AGENT = "AGENT";
	public static final String ASSISTER = "ASSISTER";
	public static final String ENCODING_UTF_8 = "UTF-8";
	public static final String SWITCH_TO_RESOURCE_FULL_NAME_ENCODED = "encSwitchToResourceFullName";
	public static final String SWITCH_TO_RESOURCE_FULL_NAME_UNENCODED = "unEncSwitchToResourceFullName";
	public static final String ANONYMOUS_FLAG = "anonymousFlag";
	public static final String ANONYMOUS_USER = "anonymousUser";
	public static final String ERROR = "error";
	public static final String MESSAGE = "message";
	
	// Agent and Assister Managing Individuals
	public static final String SORT_ORDER = "sortOrder";
	public static final String ASC = "ASC";
	public static final String CHANGE_ORDER = "changeOrder";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String DESC = "DESC";
	public static final String FIRST_NAME = "FIRST_NAME";
	public static final String FIRSTNAME = "firstName";
	public static final String LASTNAME = "lastName";
	public static final String SORT_BY = "sortBy";
	public static final String SORT_BY_COL = "sortByCol";
	public static final String APPLICATION_STATUS = "applicationStatus";
	public static final String ELIGIBILITY_STATUS = "eligibilityStatus";
	public static final String ENROLLMENT_STATUS = "enrollmentStatus";
	public static final String CURRENT_STATUS = "currentStatus";
	public static final String MODULE_NAME = "moduleName";
	public static final String START_PAGE = "startPage";
	public static final String HOUSEHOLDLIST = "houseHoldList";
	public static final String TOTAL_RECORD_COUNT = "totalRecordCount";
	public static final String BROKER = "BROKER";
	public static final String ENTITY = "ENTITY";
	public static final String MODULE_ID = "moduleId";
	public static final String TOTAL_RECORDS = "totalRecords";
	public static final String CONSUMERS = "consumers";
	public static final String CONSUMER_ID = "consumerId";
	public static final String REQUEST_SENT = "requestSent";
	public static final String REQUESTSENT = "REQUESTSENT";
	public static final String INACTIVE_SINCE = "inActiveSince";
	public static final String INACTIVESINCE ="INACTIVESINCE";
	public static final String FAMILY_SIZE = "familySize";
	public static final String HOUSEHOLD_INCOME = "houseHoldIncome";
	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String STATUS = "status";
	public static final String SEARCH_CRITERIA = "searchCriteria";
	public static final String REQUEST_SENT_FROM = "requestSentFrom";
	public static final String REQUEST_SENT_TO = "requestSentTo";
	public static final String INACTIVE_DATE_FROM = "inactiveDateFrom";
	public static final String INACTIVE_DATE_TO = "inactiveDateTo";
	public static final String ACTIVE_DATE_FROM = "activeDateFrom";
	public static final String ACTIVE_DATE_TO = "activeDateTo";
	public static final String ELIGIBILITY_STATUS_LIST = "eligibilityStatuslist";
	public static final String INDIVIDUAL = "Individual";
	public static final String INDIVIDUALS = "Individuals";
	public static final String TABLE_TITLE = "tableTitle";
	public static final String CEC_ID = "cecId";
	public static final String CEC_FIRST_NAME = "cecFirstName";
	public static final String CEC_LAST_NAME = "cecLastName";
	public static final String INDIVIDUAL_FIRST_OR_LAST_NAME = "indFirstOrLastName";
	public static final String CEC_FIRST_OR_LAST_NAME = "cecFirstOrLastName";
	public static final String APPLICATION_STATUS_LIST = "applicationStatusList";
	public static final String CURRENT_STATUS_LIST = "currentStatusList";
	public static final String ENROLLMENT_STATUS_LIST = "enrollmentStatuslist";
	public static final String INDIVIDUAL_ID = "individualId";
	public static final String EMAIL_ADDRESS = "emailAddress";
	public static final String PHONE_NUMBER = "phoneNumber";
	public static final String ADDRESS1 = "address1";
	public static final String ADDRESS2 = "address2";
	public static final String STATE = "state";
	public static final String ZIP = "zip";
	public static final String CITY = "city";
	public static final String SENDER_OBJ = "SENDER_OBJ";
	public static final String STATUS_ACTIVE = "Active";
	public static final String BOTH = "BOTH";
	public static final String STATUS_INACTIVE = "InActive";
	public static final String INDIVIDUALLIST = "individualList";
		
	//Need to move to properties file
	public static final String ERROR_MESSAGE = "The current password doesn't match with our records";
	public static final String SUCCESS_MESSAGE = "You have successfully unlocked your Account!";
	public static final String FAILURE_MESSAGE = "Account Unlock Failed";
	public static final String INDIVIDUAL_NOTIFICATION_TEMPLATE_NAME = "individualConsumerDe-designationNotification";
	
	public static final String SENDER_BROKER_MODULE = "BROKER";
	public static final String REASON_FOR_DE_DESIGNATION = "ReasonForDeDesignation";
	public static final String AGENT_FIRST_NAME = "agentFN";
	public static final String AGENT_LAST_NAME = "agentLN";
	public static final String NOTIFICATION_DATE = "notificationDate";
	public static final String BROKER_BUSINESS_NAME = "BrokerBusinessName";
	public static final String BROKER_COMPANY_NAME = "BrokerCompanyName";
	public static final Object STATUS_PENDING = "Pending";
	
	public static final String BROKER_ID = "brokerId";
	public static final String CONSUMER_USER_ID = "consumerUserId";
	public static final String SLASH = "/";
	public static final String ADDACTIONFLAG = "Add";
	public static final String REMOVEACTIONFLAG = "Remove";
	public static final String ENROLLMENT_TYPE_INDIVIDUAL = "FI";
    public static final String AGENT_ROLE = "AGENT";
	public static final String ACCESS_IS_DENIED = "Access is denied.";
	public static final String CONTACT_NAME = "CONTACT_NAME";
	public static final String INCOMPLETE = "Incomplete";
	public static final String CERTIFIED = "Certified";
	
	public static final String CA_STATE_CODE = "CA";
	public static final String ID_STATE_CODE = "ID";
	public static final String NV_STATE_CODE = "NV";
	public static final String RESPONSE_SUCCESS = "SUCCESS";
	public static final String RESPONSE_FAILURE = "FAILURE";
	public static final int RESPONSE_SUCCESS_CODE = 200;
	
	public static final String SWITCH_ACC_URL = "switchAccUrl";
	public static final String BOB_DOWNLOAD_URL = "bobDownloadUrl";
	public static final String RECORD_TYPE = "recordType";
	public static final String ENCRYPTED_USER_NAME = "encryptedUserName";
	public static final String REQUEST = "Request";
	public static final String REQUESTS = "Requests";
	public static final String BROKER_OBJECT_FOR_LEFT_NAV = "brokerObjectForLeftNav";
	public static final String ENCRYPTED_RECORD_ID = "encryptedRecordId";
}
