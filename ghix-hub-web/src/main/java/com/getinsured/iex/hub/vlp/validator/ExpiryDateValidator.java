package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;

/**
 * Validator for expiry date
 * 
 * @author Nikhil Talreja
 * @since 31-Jan-2014
 *
 */
public class ExpiryDateValidator extends Validator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExpiryDateValidator.class);
	
	/** 
	 * Validations for expiry date
	 * 1. Should be a valid date
	 * 2. Date format is YYYY-MM-DD
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		try{
			VLPServiceUtil.stringToXMLDate(value.toString(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
		}
		catch(Exception e){
			throw new InputDataValidationException(VLPServiceConstants.DATE_ERROR_MESSAGE+". Expected format is " + VLPServiceConstants.VLP_SERVICE_DATE_FORMAT.toUpperCase(), e);
		}
	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
