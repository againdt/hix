
package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class FormerEmployer {

    @Expose
    private Phone phone=new Phone();
    @Expose
    private String employerName;
    @Expose
    private String employerIdentificationNumber;
    @Expose
    private String employerContactPersonName;
    @Expose
    private Address address=new Address();
    @Expose
    private EmployerContactPhone employerContactPhone=new EmployerContactPhone();
    @Expose
    private String employerContactEmailAddress;

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public String getEmployerIdentificationNumber() {
        return employerIdentificationNumber;
    }

    public void setEmployerIdentificationNumber(String employerIdentificationNumber) {
        this.employerIdentificationNumber = employerIdentificationNumber;
    }

    public String getEmployerContactPersonName() {
        return employerContactPersonName;
    }

    public void setEmployerContactPersonName(String employerContactPersonName) {
        this.employerContactPersonName = employerContactPersonName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public EmployerContactPhone getEmployerContactPhone() {
        return employerContactPhone;
    }

    public void setEmployerContactPhone(EmployerContactPhone employerContactPhone) {
        this.employerContactPhone = employerContactPhone;
    }

    public String getEmployerContactEmailAddress() {
        return employerContactEmailAddress;
    }

    public void setEmployerContactEmailAddress(String employerContactEmailAddress) {
        this.employerContactEmailAddress = employerContactEmailAddress;
    }

}
