<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.tablesorter.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<%@ page isELIgnored="false"%>

<%-- <%-- <% --%> 
<!-- // 	Integer pageSizeInt = (Integer) request.getAttribute("pageSize"); -->
<!-- // 	String reqURI = (String) request.getAttribute("reqURI"); -->
<!-- // 	Integer totalResultCount = (Integer) request -->
<!-- // 			.getAttribute("resultSize"); -->
<!-- // 	if (totalResultCount == null) { -->
<!-- // 		totalResultCount = 0; -->
<!-- // 	} -->
<!-- // 	Integer currentPage = (Integer) request.getAttribute("currentPage"); -->
<!-- // 	int noOfPage = 0; -->
<!-- // 	System.out.println("tes " + totalResultCount); -->
<!-- // 	if (totalResultCount > 0) { -->
<!-- // 		if ((totalResultCount % pageSizeInt) > 0) { -->
<!-- // 			noOfPage = (totalResultCount / pageSizeInt) + 1; -->
<!-- // 		} else { -->
<!-- // 			noOfPage = (totalResultCount / pageSizeInt); -->
<!-- // 		} -->
<!-- // 	} -->
<%-- <%-- %> --%>




<style media="screen" type="text/css">
form label.error {
	background: #fff8af none repeat scroll 0 0
}

form label.error {
	width: auto;
	display: block;
	float: left;
	padding: 3px 4px 3px 3px;
	margin: 0 0 5px 0;
	font: 12px "Droid Sans", Tahoma, arial, sans-serif;
	color: #900;
	background: #fff8af none repeat scroll 0 0;
	text-align: left;
	border-radius: .32em .32em .32em .32em;
	border: solid 1px #bfba91;
	box-shadow: 0 1px 3px rgba(0, 0, 0, .2)
}

form label.error span em.excl {
	-moz-border-radius-bottomleft: 2px;
	-moz-border-radius-bottomright: 2px;
	-moz-border-radius-topleft: 2px;
	-moz-border-radius-topright: 2px;
	background: #ea5200 none repeat scroll 0 0;
	color: #fff;
	font-weight: bold;
	margin: 0 5px;
	padding: 0 4px 0 5px;
	width: auto
}

legend.announcement {
	border: 1px solid #08c;
	line-height: 20px;
	padding: 1px 14px;
	width: 108px;
	font-weight: bold;
	font-size: 13px
}

fieldset.announcement {
	border: 1px solid #08c;
	font: 11px Verdana, Arial, Helvetica, sans-serif;
	padding: 5px 5px 5px 5px
}

.view-broker {
	top: 0 !important;
	position: relative;
	right: 10px
}

.small-caps label, .small-caps p {text-transform: uppercase; font-size: 11px;}

#menu .navbar-inner {
	margin-bottom: 5px;
}
</style>


		<div class="gutter10-lr">
			<div class="l-page-breadcrumb hide">
				<!--start page-breadcrumb -->
				<div class="row-fluid">
					<ul class="page-breadcrumb">
						<li><a href="javascript:history.back()">&lt; <spring:message code="label.entity.back"/></a></li>
						<li><a href="<c:url value="/entity/entityadmin/managelist"/>"><spring:message code="label.entity.entities"/></a></li>
						<li><spring:message code="label.entity.manage"/></li>
					</ul>
					<!--page-breadcrumb ends-->
				</div>
				<!-- end of .row-fluid -->
			</div>
			<!--l-page-breadcrumb ends-->
			<div class="row-fluid">
				<h1>
					<spring:message code="label.entity.enrollmentEntities"/>
					 <small>
						<c:choose>
						   <c:when test="${totalenrollmententitiesbysearch>1}">
						          ${totalenrollmententitiesbysearch} <spring:message code="label.entity.matchingEntities" /> 
						   </c:when>
						   <c:otherwise>
						          ${totalenrollmententitiesbysearch} <spring:message code="label.entityMatchingCertifiedEnrollmentEntity" /> 
						   </c:otherwise>
					    </c:choose>
					</small>
					<c:if test="${CA_STATE_CODE}">
						<a style=" font-size: 13px;" class="pull-right" name="ADD_ENTITY_URL_CA" href="<c:url value='${addNewEntityUrl}'/>"><spring:message code="label.entity.addNewEntity" /></a>
					</c:if>
				</h1>
				
				<div class="pull-right gutter10">
					<div class="controls">
						<div id="dropdown" class="dropdown hide">
							<span class="gray" id="counter"><small>( 0 Items
									Selected )&nbsp;</small></span> <span id="bulkActions" style="display: none;">
								<a class="dropdown-toggle btn btn-primary btn-small"
								id="inactiveLabel" role="button" data-toggle="dropdown"
								data-target="#" href="#"><spring:message code="label.entity.bulkAction"/> <i
									class="icon-cog icon-white"></i><i class="caret icon-white"></i></a>
								<ul class="dropdown-menu pull-right" role="menu"
									aria-labelledby="inactiveLabel">
									<li></li>
									<li><a href="#mark-inactive" onclick="markInactive();"
										class="validateDecline" id="declinelink"><spring:message code="label.entity.markAsInActive"/></a></li>

								</ul>
							</span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row-fluid">
				<div id="sidebar" class="span3">
					<div class="header">
						<h4><spring:message code="label.entity.refResuBy"/> <a class="reset-button" href="#" onclick="resetAll()"><spring:message code="label.entity.resetAll"/></a></h4>
					</div>
						<form method="POST"
							action="<c:url value="/entity/entityadmin/managelist" />"
							id="entitysearch" name="entitysearch"
							class="form-vertical gutter10" novalidate="novalidate">
							<df:csrfToken/>
							<input type="hidden" id="sortBy" name="sortBy" value="id">
							<input type="hidden" id="sortOrder" name="sortOrder" value="DESC" >
							<input type="hidden" id="pageNumber" name="pageNumber" value="1">
							<input type="hidden" id="changeOrder" name="changeOrder" >
							<input type="hidden" id="pageSize" name="pageSize" value="${pageSize + 0}" >
								
							<div class="control-group">
								<label class="control-label" for="entityName"><spring:message code="label.entity.entityName"/></label>
								<div class="controls">
									<input type="text" class="span12" name="entityName"
										id="entityName"
										value="${enrollmentEntitySearchParameters.entityName}">
								</div>
								<!-- end of controls-->
							</div>
							
							<c:if test="${CA_STATE_CODE}">
								<div class="control-group">
									<label class="control-label" for="entityNumber"><spring:message code="label.entity.entityNumber"/></label>
									<div class="controls">
										<input type="text" class="span12" name="entityNumber"
											id="entityNumber"
											value="${enrollmentEntitySearchParameters.entityNumber}">
									</div>
									<!-- end of controls-->
								</div>
							</c:if>
							
							<!--control-group-->
							<div class="control-group">
								<label for="status" class="control-label"><spring:message code="label.entity.status"/></label>
								<div class="controls">
									<select  id="status"
										name="status" class="input-medium">
										<option value=""><spring:message code="label.entity.select"/></option>
										<c:forEach items="${statuslist}" var="value">
										<option value="${value}" <c:if test="${enrollmentEntitySearchParameters.status == value}"> SELECTED </c:if>>${value}</option>
										</c:forEach>
									</select>
								</div>
								<!-- end of controls-->
							</div>
							<!-- end of control-group -->

							<div class="control-group">
								<p><spring:message code="label.RenewalDate"/></p> 
								<label for="fromDate"><span class="aria-hidden"><spring:message code="label.assister.certificationRenewalDate"/></span> <spring:message code="label.entity.From"/></label>
								<div class="controls">
									<div class="input-append date date-picker" data-date="">
										<input class="span10" type="text" name="fromDate"
											id="fromDate" value="${enrollmentEntitySearchParameters.fromDate}"> <span class="add-on"><i title="choose date" class="icon-calendar"></i></span>
									</div>
									<span id="fromDate_error"></span>
								</div>
								<label for="toDate"><span class="aria-hidden"><spring:message code="label.assister.certificationRenewalDate"/></span> <spring:message code="label.entity.to"/></label>
								<div class="controls">
									<div class="input-append date date-picker" data-date="">
										<input class="span10" type="text" name="toDate" id="toDate" value="${enrollmentEntitySearchParameters.toDate}">
										<span class="add-on"><i title="choose date" class="icon-calendar"></i></span>
									</div>

								</div>
								<span id="toDate_error"></span>
							</div>
							<!-- end of control-group -->

							<div class="control-group">
								<fieldset>
									<legend class="control-label" for="receivedPayments"><spring:message code="label.entity.paid"/></legend>				
									<div class="controls">
										<label class="checkbox" for="receivedPayment1">
											<input type="checkbox" name="receivedPayments" id="receivedPayment1" value="1" ${fn:contains(enrollmentEntitySearchParameters.receivedPayments,'1') ? 'checked="checked"' : ''}><spring:message code="label.entity.yes"/>
										</label>
										<label class="checkbox" for="receivedPayment2">
											<input type="checkbox" name="receivedPayments" id="receivedPayment2" value="0" ${fn:contains(enrollmentEntitySearchParameters.receivedPayments,'0') ? 'checked="checked"' : ''}><spring:message code="label.entity.no"/>
										</label>
									</div> 
								</fieldset>
							</div>
								
							<div class="control-group">
								<label for="organizationType" class="control-label"><spring:message code="label.entity.organizationType"/></label>
								<div class="controls">
									<select  id="organizationType" name="organizationType"
										class="input-medium">
										<option value=""><spring:message code="label.entity.select"/></option>
										<c:forEach items="${organizationtypelist}" var="value">
											<option value="${value}" <c:if test="${enrollmentEntitySearchParameters.organizationType == value}"> SELECTED </c:if>>${value}</option>
										</c:forEach>
									</select>
								</div>
								<!-- end of controls-->
							</div>
							<!-- end of control-group -->
							<div class="control-group">
								<label for="language" class="control-label"><spring:message code="label.entity.language"/></label>
								<div class="controls">
									<select  id="language" name="language"
										class="input-medium">
										<option value=""><spring:message code="label.entity.select"/></option>
										<c:forEach items="${languagelist}" var="language">
										<option value="${language.lookupValueLabel}" <c:if test="${enrollmentEntitySearchParameters.language == language.lookupValueLabel}"> SELECTED </c:if>>${language.lookupValueLabel}</option>
										</c:forEach>
									</select>
								</div>
								<!-- end of controls-->
							</div>
							<!-- end of control-group -->
							<div class="control-group">
								<label class="control-label" for="zipCode"><spring:message code="label.entity.zipCode"/></label>
								<div class="controls">
									<input type="text" placeholder="" name="zipCode" id="zipCode" value="${enrollmentEntitySearchParameters.zipCode}"
										class="input-mini zipCode" maxlength="5" />
								</div>
								<!-- end of controls-->
								<div id="zipCode_error"></div>
							</div>
							
							<div class="control-group">
								<label for="countyServed" class="control-label"><spring:message code="label.entity.countiesServed"/></label>
								<div class="controls">
									<select  id="countyServed" name="countyServed"
										class="input-medium">
										<option value=""><spring:message code="label.entity.select"/></option>
										<c:forEach items="${countiesServedlist}" var="countyServed">
										    <%-- <option value="${countyServed.lookupValueLabel}" <c:if test="${enrollmentEntitySearchParameters.countyServed == countyServed.lookupValueLabel}"> SELECTED </c:if>>${countyServed.lookupValueLabel}</option> --%>
										    <option value="${countyServed}" <c:if test="${enrollmentEntitySearchParameters.countyServed == countyServed}"> SELECTED </c:if>>${countyServed}</option>
										</c:forEach>
									</select>
								</div>
								<!-- end of controls-->
							</div>
							
							<!--control-group-->
							<div class="txt-center">
								<input type="submit" value="<spring:message code="label.assister.indGo"/>" class="btn" id="submit"
									name="submit">
							</div>
						</form>
				</div>
				<!-- end of span3 -->
				<div class="span9">
					<form class="form-vertical" id="frmmanageEE" name="frmmanageEE"
						action="<c:url value="/entity/entityadmin/managelistforpagination" />"
						method="POST">
						<df:csrfToken/>
<!-- 						<input type="hidden" name="pEntityName" id="pEntityName" value=""> -->
<!-- 						<input type="hidden" name="pStatus" id="pStatus" value=""> -->
<!-- 						<input type="hidden" name="pOrganizationType" id="pOrganizationType" value=""> -->
<!-- 						<input type="hidden" name="pLanguage" id="pLanguage" value=""> -->
<!-- 						<input type="hidden" name="pFromDate" id="pFromDate" value=""> -->
<!-- 						<input type="hidden" name="pToDate" id="pToDate" value=""> -->
<!-- 						<input type="hidden" name="pZipCode" id="pZipCode" value=""> -->
<!-- 						<input type="hidden" name="pReceivedPayments" id="pReceivedPayments" value=""> -->
<!-- 						<input type="hidden" name="pCountyServed" id="pCountyServed" value=""> -->
<!-- 						<input type="hidden" id="pageNumber" name="pageNumber" value=""> -->
						<c:choose>
							<c:when test="${fn:length(enrollmententitiesmap) > 0}">
								<table class="table" id="enrollmententitiesmap"
									name="enrollmententitiesmap">
									<thead>
										<tr class="graydrkbg">
											<spring:message code="label.entity.name" var="nameVal"/>
											<c:if test="${CA_STATE_CODE}">
												<spring:message code="label.entity.entityNumber" var="entityNumberVal" />
											</c:if>
											<spring:message code="label.assister.assisters" var="assistersNameVal"/>
											<spring:message code="label.entity.renewalDate" var="renewalDateVal"/>
											<spring:message code="label.entity.status" var="statusVal"/>
											 <c:set var="entityNumberSort" value="${sortOrder}" />
											 <c:set var="assistersSort" value="${sortOrder}" />
											 <c:set var="renewalDateSort" value="${sortOrder}" />
											 <c:set var="statusSort" value="${sortOrder}" />
											<th class="sortable" scope="col"><dl:sort customFunctionName="traverse" title="${nameVal}" sortBy="ENTITY_NAME" sortOrder="${sortOrder}"></dl:sort></th>
											<c:if test="${CA_STATE_CODE}">
												<th class="sortable" scope="col"><dl:sort customFunctionName="traverse" title="${entityNumberVal}" sortBy="ENTITY_NUMBER" sortOrder="${entityNumberSort}"></dl:sort></th>
											</c:if>
										    <th class="sortable" scope="col"><dl:sort customFunctionName="traverse" title="${assistersNameVal}" sortBy="assisters" sortOrder="${assistersSort}"></dl:sort></th>
										    <th class="sortable" scope="col"><dl:sort customFunctionName="traverse" title="${renewalDateVal}" sortBy="REGISTRATION_RENEWAL_DATE" sortOrder="${renewalDateSort}"></dl:sort></th>
										    <th class="sortable" scope="col"><dl:sort customFunctionName="traverse" title="${statusVal}" sortBy="REGISTRATION_STATUS" sortOrder="${statusSort}"></dl:sort></th>
										    <th class="sortable" scope="col" style="width:45px"><spring:message code="label.entity.action"/></th>
<!-- 											 <th class="header"><label class="checkbox" for="check_allActive"><input type="checkbox"  id="check_allActive"  name="check_allActive" value=""></label></th> -->
<%-- 											<th class="header" scope="col"><spring:message code="label.entity.name"/></th> --%>
<%-- 											<th class="header" scope="col"><spring:message code="label.assister.assisters"/></th> --%>
<%-- 											<th class="header" scope="col"><spring:message code="label.entity.renewalDate"/></th> --%>
<%-- 											<th class="header" scope="col"><spring:message code="label.entity.status"/></th> --%>
<!-- 											<th class="header"></th> -->
										</tr>
									</thead>
									<c:set var="counter" value="0" />
									<c:forEach items="${enrollmententitiesmap}" var="entry">
										<c:set var="entitylist" value="${entry.key}" />
										<tr>
											<!-- <td><label class="checkbox" for="${counter}"><span
													class="hide">Checkbox</span><input id="${counter}"
													class="indCheckboxActive" type="checkbox"
													name="indCheckboxActive" value="908"></label></td> -->
							 <c:set var="encrytedEntitylistUserid" ><encryptor:enc value="${entitylist.user.id}" isurl="true"/> </c:set>													
											<td><a
												href="<c:url value="/entity/entityadmin/viewentityinformation/${encrytedEntitylistUserid}"/>">${entitylist.entityName}</a></td>
											<c:if test="${CA_STATE_CODE}">
											<td>
												<c:choose>
													<c:when test = "${entitylist.entityNumber == 0}">
													</c:when>
													<c:otherwise>
														${entitylist.entityNumber}
													</c:otherwise>
												</c:choose>
											</td>
											</c:if>
											<td>${entry.value}</td>
											<td><fmt:formatDate
													value="${entitylist.registrationRenewalDate}"
													pattern="MM/dd/yyyy"/></td>
											<td>${entitylist.registrationStatus}</td>
											<td>
												<div class="dropdown">
													<c:choose>
														<c:when test="${fn:contains(entitylist.registrationStatus, 'Incomplete')}">
															<a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-cog"></i><i class="caret"></i></a>
														</c:when>
														<c:otherwise>
															<a href="#" data-toggle="dropdown" class="dropdown-toggle"><i
																class="icon-cog"></i><i class="caret"></i></a>
															<ul aria-labelledby="dLabel" role="menu"
																class="dropdown-menu">
																<li><a
																	href="/hix/entity/entityadmin/entityinformation/${encrytedEntitylistUserid}""><i
																		class="icon-pencil"></i><spring:message code="label.entity.edit"/></a></li>
															 <c:set var="encrytedEntitylistId" ><encryptor:enc value="${entitylist.id}" isurl="true"/> </c:set>			
																<li><a
																	href="/hix/entity/entityadmin/editregistrationstatus/${encrytedEntitylistId}"><i
																		class="icon-refresh"></i><spring:message code="label.entity.updateStatus"/></a></li>
															</ul>
														</c:otherwise>
													</c:choose>
												</div>

											</td>
										</tr>
										<c:set var="counter" value="${counter+1}" />

									</c:forEach>
								</table>

								<div class="pagination center">
									<c:choose>
										<c:when test="${CA_STATE_CODE}">
											<dl:paginate
												resultSize="${totalenrollmententitiesbysearch + 0}"
												pageSize="${pageSize + 0}" hideTotal="true"
												showDynamicPageSize="true" />
										</c:when>
										<c:otherwise>
											<dl:paginate
												resultSize="${totalenrollmententitiesbysearch + 0}"
												pageSize="${pageSize + 0}" hideTotal="true" />
										</c:otherwise>
									</c:choose>

								</div>
							</c:when>
							<c:otherwise>
								<hr />
								<div class="alert alert-info" tabindex="0"><spring:message code="label.entity.noMatchingRecordsFound"/></div>
							</c:otherwise>
						</c:choose>
					</form>
				</div>
			</div>
		</div>

<script type="text/javascript">
	$(function(){$("#getAssistance").click(function(e){e.preventDefault();var href=$(this).attr('href');if(href.indexOf('#')!=0){$('<div id="searchBox" class="modal bigModal" data-backdrop="static"><div class="searchModal-header gutter10-lr"><button type="button" onclick="window.location.reload()" class="close" data-dismiss="modal" aria-hidden="true">�<\/button><\/div><div class=""><iframe id="search" src="'+href+'" class="searchModal-body"><\/iframe><\/div><\/div>').modal();}});});
	function closeSearchLightBox(){$("#searchBox").remove();window.location.reload();}
	function closeSearchLightBoxOnCancel(){$("#searchBox").remove();}
	if(!NREUMQ.f){NREUMQ.f=function(){NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:")+"//"+"d1ros97qkrwjf5.cloudfront.net/42/eum/rum.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-3.newrelic.com","18650c3d6e","1192004","ZVVQN0FQWRBZABBfDFwfeDBjHmAmek4teCUdRlsGREIYD1kaC0MXQR9BC1ZdW01SEBQ=",0,194,new Date().getTime(),"","","","",""]);
	
	function traverse(url){
		var queryString = {};
		url.replace(
			    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
			    function($0, $1, $2, $3) { queryString[$1] = $3; }
			);
		 if( queryString["sortBy"] ) $("#sortBy").val(queryString["sortBy"]);
		 if( queryString["sortOrder"] ) $("#sortOrder").val(queryString["sortOrder"]);
		 if( queryString["pageNumber"] ) $("#pageNumber").val(queryString["pageNumber"]);
		 if( queryString["changeOrder"] ) $("#changeOrder").val(queryString["changeOrder"]);
		$("#entitysearch input[type=submit]").click();
	}
</script>

<script type="text/javascript">
	$(function(){$('.datepick').each(function(){$(this).datepicker({showOn:"button",buttonImage:"../resources/images/calendar.gif",buttonImageOnly:true,buttonText: "Choose"});});});
	$('.date-picker').datepicker({
	    autoclose: true,
	    format: 'mm-dd-yyyy'
	});
	
<%--// 	function populatePageNumberAndSearchParameters(intValue){
// 		$("#pageNumber").val(intValue);
// 		document.forms['frmmanageEE'].elements["pEntityName"].value = document.forms['entitysearch'].elements["entityName"].value;
// 		document.forms['frmmanageEE'].elements["pStatus"].value = document.forms['entitysearch'].elements["status"].value;
// 		document.forms['frmmanageEE'].elements["pOrganizationType"].value = document.forms['entitysearch'].elements["organizationType"].value;
// 		document.forms['frmmanageEE'].elements["pLanguage"].value = document.forms['entitysearch'].elements["language"].value;
// 		document.forms['frmmanageEE'].elements["pFromDate"].value = document.forms['entitysearch'].elements["fromDate"].value;
// 		document.forms['frmmanageEE'].elements["pToDate"].value = document.forms['entitysearch'].elements["toDate"].value;
// 		document.forms['frmmanageEE'].elements["pZipCode"].value = document.forms['entitysearch'].elements["zipCode"].value;
		
// 		var checkboxValue = document.forms['entitysearch'].elements["receivedPayments"];
// 		for(var i = 0; i < checkboxValue.length; i++)
// 		{
// 		   if(checkboxValue[i].checked)
// 		   {
// 			   document.forms['frmmanageEE'].elements["pReceivedPayments"].value = checkboxValue[i].value;
// 		   }
// 		}
// 		document.forms['frmmanageEE'].elements["pCountyServed"].value = document.forms['entitysearch'].elements["countyServed"].value;
// 		document.frmmanageEE.submit();
// 	}
	
// 	$(document).ready(function() {
// 		function ie8Trim(){
// 			if(typeof String.prototype.trim !== 'function') {
// 	            String.prototype.trim = function() {
// 	            	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
// 	            }
// 			}
// 		}
  	
// 	  	var pagenumber ='${pageNumber}';
// 		$("#pageNumber"+pagenumber).removeClass("link");
// 		$("#pageNumber"+pagenumber).addClass("active");
		
// 		//Displaying Pagination Dynamically based on result size
// 		var totalEntities = ${totalenrollmententitiesbysearch};
// 		var count = 0;
// 		var pagesize ='${pageSize}';

// 		if(totalEntities > pagesize){
// 			var pages = Math.floor(totalEntities/pagesize);
// 			var remainder = totalEntities%(pagesize*pages);
// 			if(parseInt(remainder) > 0){
// 				count = parseInt(pages)+1;
// 			} else {
// 				count = parseInt(pages);
// 			}
// 			var pageNumbers = 1;
// 			for(var i=0;i<count;i++){
				
// 				var currentDiv = document.getElementById('paginationDiv');
// 				var newdiv = document.createElement('li');
// 				newdiv.setAttribute('id','pageNumber'+pageNumbers);
// 				newdiv.setAttribute('class','link');
// 				newdiv.innerHTML = '<a href="javascript:populatePageNumberAndSearchParameters('+pageNumbers+');">'+pageNumbers+'</a>';
// 				currentDiv.appendChild(newdiv);
// 				pageNumbers++;
// 			}
// 		}
// 	});--%>
	
	//For each of the broker checkboxes, process the following code when they are changed
	 $(document).ready(function(){$('table[name="brokerlist"]').tablesorter();});$('input[name="check_allActive"]').click(function(){if($('input[name="check_allActive"]').is(":checked"))
									{$('input[name="indCheckboxActive"]').each(function(){$(this).attr("checked",true);});}else{$('input[name="indCheckboxActive"]').each(function(){$(this).attr("checked",false);});}});$('input[name="indCheckboxActive"]').bind('change',function(){if($('input[name="indCheckboxActive"]').filter(':not(:checked)').length==0){$('input[name="check_allActive"]').attr("checked",true);}else{$('input[name="check_allActive"]').attr("checked",false);}});function countChecked(){var n=$("input[name='indCheckboxActive']:checked").length;var itemsSelected="<small>&#40; "+n+(n===1?" Item":" Items")+" Selected &#41;&nbsp;<\/small>";$("#counter").html(itemsSelected);if(n>0){$("#counter").show();}else{$("#counter").show();}}
									countChecked();$(":checkbox").click(countChecked);
	
	

	jQuery.validator.addMethod("chckDate", function(value, element, param) {
		var fromDate=document.getElementById('fromDate').value;
		var toDate=document.getElementById('toDate').value;
		  
		fromDate = fromDate.replace(/-/g, '/');
		toDate = toDate.replace(/-/g, '/');
			 
		if((fromDate != null) &&(toDate !=null)){
			if(new Date(fromDate).getTime()  > new Date(toDate).getTime() ){
				return false;
			}
		 }
		return true;
	});
	
	jQuery.validator.addMethod("ZipCodecheck", function(value, element, param) {
		zip = $("#zipCode").val().trim(); 
		if((zip != "")  && (isNaN(zip))){ 
			return false; 
		}
		return true;
	});
	
	jQuery.validator.addMethod("chkDateFormat", function(value, element, param) {
		// if license renewal date is not added do not validate anything
		if(value == '') {
			return true; 
		} 
		if(value != '') {
			return /^(0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d$/.test(value);
		}	
	});
	
	var validator = $("#entitysearch").validate({ 
		onkeyup: true,
		onclick: true,
		onblur: true,
		rules : {
			zipCode : {ZipCodecheck: true, digits: true},
			toDate : {chckDate: true, chkDateFormat : true},
			fromDate : {chkDateFormat : true}
		},
		messages : {
			zipCode : {ZipCodecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateZipcode.aee' javaScriptEscape='true'/></span>"},
			toDate : {chckDate : "<span> <em class='excl'>!</em><spring:message code='label.validateFromAndToDate' javaScriptEscape='true'/></span>",
				chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='err.validateDateFormat' javaScriptEscape='true'/></span>"},
			fromDate : {chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='err.validateDateFormat' javaScriptEscape='true'/></span>"}
		}
		,
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		} 
	});
	
	 function resetAll() {
		 var  contextPath =  "<%=request.getContextPath()%>";
		    var documentUrl = contextPath + "/entity/entityadmin/resetall";
		    window.open(documentUrl,"_self","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
	 }	
	
	 
</script>