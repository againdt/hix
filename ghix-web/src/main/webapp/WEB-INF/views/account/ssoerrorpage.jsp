
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="org.springframework.context.MessageSource"%>
<%@page import="org.springframework.beans.factory.annotation.Autowired"%>
<%@page import="com.getinsured.hix.platform.util.DisplayUtil"%>
<%@page import="com.getinsured.hix.platform.config.SecurityConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<style>
	#masthead .nav-collapse li {
		display: none;
	}
	#masthead .nav-collapse li.dropdown,
	#masthead .nav-collapse li.dropdown li {
		display: block;
	}
	#menu {
		display: none!important;
	}
	p {
		font-size: 15px;
		line-height: 24px;
	}
</style>


<div class="gutter10">
	<div class="row-fluid">
		<div class="span10 offset1 errorPage" id="rightpanel">
			<div class="alert alert-info margin20-t margin20-b txt-center">
				<i class="icon-cogs"></i>
				<h1>Login is temporarily unavailable</h1>
				<p class="margin20-tb">Due to system updates and maintenance, your account is temporarily unavailable. Please check back later and thanks for your patience.</p>
				<a href="<c:url value="/"/>" class="btn btn-primary btn-large">Continue</a>
			</div>
		</div>
	</div>
</div>
