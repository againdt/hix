package com.getinsured.hix.planmgmt.service.email;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.logging.GhixLogFactory;
import com.getinsured.hix.platform.logging.GhixLogger;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.util.PlanMgmtConstants;

@Component
public class PlanCertificationBulkStatusUpdateEmail {
	
	private static final GhixLogger LOGGER = GhixLogFactory.getLogger(PlanCertificationBulkStatusUpdateEmail.class);
	
	private Issuer issuerObj;
	private AccountUser user;
	private IssuerRepresentative issuerRepObj;
	private String numberOfPlans;

	@Value("#{configProp['appUrl']}")
	private String appUrl;

	public void setIssuerObj(final Issuer issuerObj){
		this.issuerObj = issuerObj;
	}

	public void setUser(final AccountUser user){
		this.user = user;
	}
	
	public void setIssuerRepObj(IssuerRepresentative issuerRepObj) {
		this.issuerRepObj = issuerRepObj;
	}
	
	public void setNumberOfPlans(String numberOfPlans) {
		this.numberOfPlans = numberOfPlans;
	}

	public Map<String, Object> getSingleData(){
		
		final Map<String, Object> data = new HashMap<String, Object>();
		final DateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");

		data.put(PlanMgmtConstants.INSURANCE_EXCHANGE, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_NAME));
		data.put(PlanMgmtConstants.EXCHANGE_ADDRESS, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		data.put(PlanMgmtConstants.EXCHANGE_WEBSITE, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_URL));
		data.put("primaryContact", user.getFullName());
		data.put("numberOfPlans", numberOfPlans == null ? "": numberOfPlans.trim());
		data.put(PlanMgmtConstants.HEALTHEXCHANGE, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_NAME));
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_URL, appUrl == null ? "" : appUrl);
		data.put("verifyEndDate", dateFormat.format(DateUtils.addDays(new TSDate(), 30)));
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_NAME));
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_PHONE));
		
		data.put(PlanMgmtConstants.ISSUER_MARKETING_NAME, issuerObj.getName() == null ? "": issuerObj.getName().trim());
		data.put(PlanMgmtConstants.CITY, issuerObj.getCity());
		data.put(PlanMgmtConstants.STATE, issuerObj.getState());
		data.put(PlanMgmtConstants.ZIPCODE, issuerObj.getZip());
		data.put(PlanMgmtConstants.SYSTEM_DATE, dateFormat.format(new TSDate()));
		
		String firstName = issuerRepObj.getFirstName() == null ? "" : issuerRepObj.getFirstName().trim();
		String lastName = issuerRepObj.getLastName() == null ? "" : issuerRepObj.getLastName().trim();
		
		String address1 = issuerObj.getAddressLine1() == null ? "" : issuerObj.getAddressLine1().trim();
		String address2 = issuerObj.getAddressLine2() == null ? "" : issuerObj.getAddressLine2().trim();
		String address ="";
		if(StringUtils.isNotBlank(address2)){
			address = address1 + PlanMgmtConstants.BREAK_LINE + address2;
		}else{
			address = address1;
		}
		data.put(PlanMgmtConstants.ISSUER_ADDRESS, address);
		
		data.put(PlanMgmtConstants.ISSUER_REPRESENTATIVE, firstName + " " + lastName);

		final Map<String, Object> emailHeaders = new HashMap<String, Object>();

		emailHeaders.put("IssuerId", String.valueOf(issuerObj.getId()));
		emailHeaders.put("UserId", String.valueOf(user.getId()));
		emailHeaders.put("To", user.getEmail());		
		emailHeaders.putAll(data);
		LOGGER.debug("Notification Template Data :-" + SecurityUtil.sanitizeForLogging(String.valueOf(data.toString())));
		return emailHeaders;
	}


}
