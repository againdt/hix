package com.getinsured.consumer.service;

import java.util.List;

import com.getinsured.hix.consumer.history.ConsumerHistoryBase;


public class ConsumerHistoryList {

	private List<ConsumerHistoryBase> historyEntry ;

	public List<ConsumerHistoryBase> getHistoryEntry() {
		return historyEntry;
	}

	public void setHistoryEntry(List<ConsumerHistoryBase> historyEntry) {
		this.historyEntry = historyEntry;
	}


}
