<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<jsp:include page="/WEB-INF/views/layouts/untiletopbar.jsp" />
<body id="user-list">
<div id="maincontainer" class="container">
<div class="row">
	<div class="span4">
		<h2>User Administration</h2>
	</div>
	<div>
		<br><br/>
		<p></p>
	</div>
	<div>
		<form id="frmuserlist" name="frmuserlist" action="user" method="post">
			<df:csrfToken/>
			<fieldset>
			<legend></legend>
				<p></p>
				<br />
				<div class="profile">
					<div class="clearfix">
					<display:table pagesize="5" export="true" name="userlist" sort="list" id="data" requestURI="list">
					
					  <display:setProperty name="paging.banner.placement">bottom</display:setProperty>
					  
					  <display:setProperty name="export.excel" value="true" />
					  <display:setProperty name="export.csv" value="true" />
					  <display:setProperty name="export.xml" value="true" />
					  
					  <display:setProperty name="export.excel.filename" value="userlist.xls"/>
					  <display:setProperty name="export.csv.filename" value="userlist.csv"/>
					  <display:setProperty name="export.xml.filename" value="userlist.xml"/>
					 
					  <display:column property="id" title="ID" />
					  <display:column property="role.id" title="Role Id" sortable="true"/>
					  <display:column property="firstName" title="First Name" sortable="true"/>
					  <display:column property="lastName" title="Last Name" sortable="true"/>
					  <display:column property="title" title="Title" sortable="true"/>
					  <display:column property="email" title="Email" sortable="true"/>
					  <display:column property="created" title="Created" format="{0,date,MM/dd/yyyy}" sortable="true"/>
					</display:table>
					</div></div>
			</fieldset>
		</form>
	</div>
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">
				<p>This information is required to determine initial eligibility to
					use the SHOP Exchange. All fields are subjected to a basic format
					validation of the input (e.g. a zip code must be a 5 digit number).
					The question mark icon which opens a light-box provides helpful
					information. The EIN number can be validated with an external data
					source if an adequate web API is available.</p>
			</div>
		</div>
	</div>
</div>
</div>
<%@ include file="/WEB-INF/views/layouts/untilefooter.jsp"%>