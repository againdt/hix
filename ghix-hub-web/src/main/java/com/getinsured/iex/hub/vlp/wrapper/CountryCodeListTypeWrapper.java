package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlpcoi.CountryCodeListType;

/**
 * This class is the wrapper for com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlpcoi.model.CountryCodeListType
 * 
 * HIX-34220
 * 
 * @author Nikhil Talreja
 * @since 09-Apr-2014
 *
 */
public class CountryCodeListTypeWrapper implements JSONAware{
	
	private String countryCode;
    private String countryDescription;
    
    private static final String COUNTRY_CODE_KEY = "CountryCode";
	private static final String COUNTRY_DESC_KEY = "CountryDescription";
    
    public CountryCodeListTypeWrapper(CountryCodeListType countryCodeListType){
    	
    	if(countryCodeListType == null){
    		return;
    	}
    	
    	this.countryCode = countryCodeListType.getCountryCode();
    	this.countryDescription = countryCodeListType.getCountryDescription();
    	
    }

	public String getCountryCode() {
		return countryCode;
	}

	public String getCountryDescription() {
		return countryDescription;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();

		obj.put(COUNTRY_CODE_KEY, this.countryCode);
		obj.put(COUNTRY_DESC_KEY, this.countryDescription);
		
		return obj.toJSONString();
	}
	
}
