package com.getinsured.eligibility.benchmark.service;

import java.util.Date;


public interface BenchmarkPlanService {

	void processAndPersist(String caseNumber, Date coverageStartDate);

	EligibilityBenchMarkServiceResponse process(String caseNumber, Date coverageStartDate);
}
