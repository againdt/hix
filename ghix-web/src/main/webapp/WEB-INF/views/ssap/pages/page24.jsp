<%@ include file="../include.jsp" %>
<div id="appscr74" style="display: none;">
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page24.incomeSummary" text="Income Summary" />
			</h4>
		</div>
		
		<div id="FinancialDetailsSummaryDiv">
			<div class="header">
				<h4><spring:message code="ssap.pageHeader.income" text="Income" /></h4>
			</div>
			<div class="gutter10">
				<form class="form-horizontal" id="incomeSummaryForm"></form>
			</div>
		</div>
	</div>
</div>