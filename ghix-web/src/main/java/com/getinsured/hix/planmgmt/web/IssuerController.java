package com.getinsured.hix.planmgmt.web;

import static com.getinsured.hix.planmgmt.service.IssuerService.ACCREDITATION_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.ADD_INFO_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.AUTHORITY_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.BENEFITS_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.BROCHURE_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.CERTI_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.DISCLOSURES_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.DOT;
import static com.getinsured.hix.planmgmt.service.IssuerService.MARKETING_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.RATES_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.UNDERSCORE;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.hix.dto.planmgmt.FormularyDrugListResponseDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GIErrorCode;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Issuer.IssuerAccreditation;
import com.getinsured.hix.model.Issuer.certification_status;
import com.getinsured.hix.model.IssuerDocument;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanDental;
import com.getinsured.hix.model.PlanDentalBenefit;
import com.getinsured.hix.model.PlanDentalCost;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.PlanHealthBenefit;
import com.getinsured.hix.model.PlanHealthCost;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.mapper.PlanHealthMapper;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.IssuerService.DocumentType;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.planmgmt.util.ExcelGenerator;
import com.getinsured.hix.planmgmt.util.MarketProfileDTO;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.planmgmt.util.ServiceAreaDetails;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.serff.service.SerffDocumentPlanPOJO;
import com.getinsured.hix.serff.service.SerffService;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;


@Controller
@DependsOn("dynamicPropertiesUtil")
public class IssuerController {
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerController.class);
	
	@Autowired private ContentManagementService ecmService;
	@Autowired private IssuerService issuerService;
	@Autowired private UserService userService;
	@Autowired private PlanMgmtService planMgmtService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private PlanMgmtUtil planMgmtUtils;
	@Autowired private GIMonitorService giMonitorService;
	@Autowired private SerffService serffService;
	
	@Value("#{configProp['appUrl']}") private static String appUrl;
	@Value("#{configProp.uploadPath}") private String uploadPath;
	@Value("#{configProp['ecm.type']}") private String documentConfigurationType;
	
	private static final String APPEND_HTTP = "http://";
	private static final String APPEND_HTTPS = "https://";
	private String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	// HIX-4861: hence issuer dashboard yet not ready so redirect user to issuer
	// landing page for time being
	@RequestMapping(value = "/issuer/dashboard", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String viewDashboard(Model model) throws InvalidUserException {
		try{
		if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is
													// issuer or issuer rep
			return PlanMgmtConstants.ISSUER_LANDING_PATH;
		}
		} catch (Exception e) {
			LOGGER.error("Exception (newIssuerLanding) :: ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME
					+ PlanMgmtErrorCodes.ErrorCode.VIEW_DASHBOARD_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	/* @author raja
	 * START : HIX-1810
	 * @Suneel: Secured the method with permission 
	 */

	@SuppressWarnings("deprecation")
	/* newIssuerLanding() method is to fetch the issuer data & displays on the landing page */
	@RequestMapping(value = "/planmgmt/issuer/landing/info", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	@GiAudit(transactionName = "DisplayIssuer", eventType = EventTypeEnum.PLAN_MANAGEMENT, eventName = EventNameEnum.PII_READ)
	public String newIssuerLanding(Model model) throws GIException {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try {
			AccountUser user = userService.getLoggedInUser();
		
			if (!StringUtils.isBlank(user.getRecordType())
					&& !StringUtils.isBlank(user.getRecordId())) //{
				{
					IssuerRepresentative issuerRepresentative = issuerService.findIssuerRepresentativeById(Integer.valueOf(user.getRecordId()));
					// only if issuer representative is not linked with user, then we will link issuer representative with logged in user
					if (issuerRepresentative.getUserRecord() == null) {
						LOGGER.debug("Updating Issuer Representative data with User Id ::"
								+  SecurityUtil.sanitizeForLogging(String.valueOf(user.getId())));
						issuerRepresentative.setUserRecord(user);
						issuerRepresentative.setUpdatedBy(user);// userService.getLoggedInUser());
						issuerService.saveRepresentative(issuerRepresentative);
						ModuleUser issuerRepUser = userService.findModuleUser(issuerRepresentative.getIssuer().getId(), ModuleUserService.ISSUER_MODULE, user);
						if (issuerRepUser == null) {
							userService.createModuleUser(issuerRepresentative.getIssuer().getId(), ModuleUserService.ISSUER_MODULE, user);
						}
					}
				}
			//}
			if (userService.hasUserRole(user, RoleService.ISSUER_REP_ROLE)) { // if logged in user is issuer or issuer rep
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);

				model.addAttribute("status_register", certification_status.REGISTERED.toString());
				//LOGGER.info("New Issuer Landing Page Info");
				String displayStartButton = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.ISSUERPORTAL_DISPLAYSTARTBUTTON);
				model.addAttribute(PlanMgmtConstants.DISPLAYSTARTBUTTON, displayStartButton);
				model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
				model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
				model.addAttribute("sysDate", new TSDate());
			}
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
		} catch (Exception e) {
			LOGGER.error("Exception (newIssuerLanding) :: ", e);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while fetching issuers"));
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME
					+ PlanMgmtErrorCodes.ErrorCode.ISSUER_LANDING_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
			}
		return "planmgmt/issuer/newissuerlanding";
	}

	
	@RequestMapping(value = "/planmgmt/checkDuplicate", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADD_REPRESENTATIVE')")
	@ResponseBody
	boolean isDuplicate(@RequestParam(value = "userEmailId", required = false) String userEmailId) {
		try {
		return issuerService.isDuplicate(userEmailId);
		} catch (Exception e) {
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.ISDUPLICATE_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
		    return false;
		}
	}

	
	@RequestMapping(value = "/planmgmt/issuer/application", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String newIssuerApplication(Model model) {
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try {
		if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is
													// issuer or issuer rep
			Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			LOGGER.info("New Issuer Application Page Info");
			return "planmgmt/issuer/application";
		}
		} catch (Exception e) {
			throw new GIRuntimeException(
					PlanMgmtConstants.MODULE_NAME
							+ PlanMgmtErrorCodes.ErrorCode.SAVE_REPRESENTATIVE_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	// uploading the issuer application documents
	@RequestMapping(value = "/planmgmt/issuer/application/documents/upload", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	@ResponseBody
	public void uploadIssuerApplicationDocuments(
			Model model,
			HttpServletRequest request,
			@RequestParam(value = "fileToUpload", required = false) String fileToUpload,
			@RequestParam(value = "authority", required = false) List<MultipartFile> authority,
			@RequestParam(value = "accrediting", required = false) List<MultipartFile> accrediting,
			@RequestParam(value = "orgData", required = false) List<MultipartFile> orgData,
			@RequestParam(value = "marketing", required = false) List<MultipartFile> marketing,
			@RequestParam(value = "finDisclosures", required = false) List<MultipartFile> finDisclosures,
			@RequestParam(value = "addSupportDoc", required = false) List<MultipartFile> addSupportDoc,
			@RequestParam(value = "payPolPractices", required = false) List<MultipartFile> payPolPractices,
			@RequestParam(value = "enrollmentAndDis", required = false) List<MultipartFile> enrollmentAndDis,
			@RequestParam(value = "ratingPractices", required = false) List<MultipartFile> ratingPractices,
			@RequestParam(value = "costSharAndPayment", required = false) List<MultipartFile> costSharAndPayment,
			@RequestParam(value = "issuerId", required = false) int issuerId,
			HttpServletResponse response) throws IOException {
		
		
		try {
		String returnString = PlanMgmtConstants.EMPTY_STRING;
		if (fileToUpload != null) {
				boolean isSizeExceeds = issuerService.isFileSizeExceedsLimit(authority,
						accrediting, orgData, marketing, finDisclosures,
						addSupportDoc, payPolPractices, enrollmentAndDis,
						ratingPractices, costSharAndPayment);
				boolean isValidExtension = issuerService.isValidExtension(authority,
						accrediting, orgData, marketing, finDisclosures,
						addSupportDoc, payPolPractices, enrollmentAndDis,
						ratingPractices, costSharAndPayment);
				
				if(!isSizeExceeds){
					if(isValidExtension){
						if (fileToUpload.equalsIgnoreCase("authority")) {
							List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
							List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.AUTHORITY_DOCUMENT_INFO_MAP);
							if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
								multiPartList.addAll(previousMultiPartList);
							}
							String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_AUTHORITY);
							if(StringUtils.isNotBlank(previousReturnString)){
								returnString = previousReturnString;
							}
							for(MultipartFile authFile: authority){
								if(returnString == ""){
									returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.AUTHORITY_FOLDER, authFile, issuerId);
								}else{
									returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.AUTHORITY_FOLDER, authFile, issuerId);
								}
								Map<String, String> multiPartData = new HashMap<String, String>();
								multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, authFile.getOriginalFilename());
								multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, authFile.getContentType());
								multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(authFile.getSize()));
								multiPartList.add(multiPartData);
							}
							
							request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_AUTHORITY, returnString);
							request.getSession().setAttribute(PlanMgmtConstants.AUTHORITY_DOCUMENT_INFO_MAP, multiPartList);
								
							
						} else if (fileToUpload.equalsIgnoreCase("accrediting")) {
							List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
							List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.ACCREDITATION_DOC_INFO_MAP);
							if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
								multiPartList.addAll(previousMultiPartList);
							}
							String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_ACCREDITATION);
							if(StringUtils.isNotBlank(previousReturnString)){
								returnString = previousReturnString;
							}
							for(MultipartFile accreditingFile: accrediting){
								if(returnString == ""){
									returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ACCREDITATION_FOLDER, accreditingFile, issuerId);
								}else{
									returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ACCREDITATION_FOLDER, accreditingFile, issuerId);
								}
								Map<String, String> multiPartData = new HashMap<String, String>();
								multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, accreditingFile.getOriginalFilename());
								multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, accreditingFile.getContentType());
								multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(accreditingFile.getSize()));	
								multiPartList.add(multiPartData);
							}
							request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_ACCREDITATION, returnString);
							request.getSession().setAttribute(PlanMgmtConstants.ACCREDITATION_DOC_INFO_MAP, multiPartList);
						} else if (fileToUpload.equalsIgnoreCase("orgData")) {
							
							List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
							List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.ORG_DATA_DOC_INFO_MAP);
							if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
								multiPartList.addAll(previousMultiPartList);
							}
							String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_ORG_DATA);
							if(StringUtils.isNotBlank(previousReturnString)){
								returnString = previousReturnString;
							}
							for(MultipartFile orgDataFile: orgData){
								if(returnString == ""){
									returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ORGANIZATION_FOLDER, orgDataFile, issuerId);
								}else{
									returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ORGANIZATION_FOLDER, orgDataFile, issuerId);
								}
								Map<String, String> multiPartData = new HashMap<String, String>();
								multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, orgDataFile.getOriginalFilename());
								multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, orgDataFile.getContentType());
								multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(orgDataFile.getSize()));	
								multiPartList.add(multiPartData);
							}
							request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_ORG_DATA, returnString);
							request.getSession().setAttribute(PlanMgmtConstants.ORG_DATA_DOC_INFO_MAP, multiPartList);
						
							
						} else if (fileToUpload.equalsIgnoreCase("marketing")) {
							
							List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
							List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.MARKETING_DOC_INFO_MAP);
							if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
								multiPartList.addAll(previousMultiPartList);
							}
							String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_MARKETING);
							if(StringUtils.isNotBlank(previousReturnString)){
								returnString = previousReturnString;
							}
							for(MultipartFile marketingFile: marketing){
								if(returnString == ""){
									returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.MARKETING_FOLDER, marketingFile, issuerId);
								}else{
									returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.MARKETING_FOLDER, marketingFile, issuerId);
								}
								Map<String, String> multiPartData = new HashMap<String, String>();
								multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, marketingFile.getOriginalFilename());
								multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, marketingFile.getContentType());
								multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(marketingFile.getSize()));	
								multiPartList.add(multiPartData);
							}
							request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_MARKETING, returnString);
							request.getSession().setAttribute(PlanMgmtConstants.MARKETING_DOC_INFO_MAP, multiPartList);
					
						} else if (fileToUpload.equalsIgnoreCase("finDisclosures")) {
							
							List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
							List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.FIN_CLOSURES_DOC_INFO_MAP);
							if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
								multiPartList.addAll(previousMultiPartList);
							}
							String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_FIN_CLOSURES);
							if(StringUtils.isNotBlank(previousReturnString)){
								returnString = previousReturnString;
							}
							for(MultipartFile finDisclosuresFile: finDisclosures){
								if(returnString == ""){
									returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.DISCLOSURES_FOLDER, finDisclosuresFile, issuerId);
								}else{
									returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.DISCLOSURES_FOLDER, finDisclosuresFile, issuerId);
								}
								Map<String, String> multiPartData = new HashMap<String, String>();
								multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, finDisclosuresFile.getOriginalFilename());
								multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, finDisclosuresFile.getContentType());
								multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(finDisclosuresFile.getSize()));	
								multiPartList.add(multiPartData);
							}
							request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_FIN_CLOSURES, returnString);
							request.getSession().setAttribute(PlanMgmtConstants.FIN_CLOSURES_DOC_INFO_MAP, multiPartList);
							
						} else if (fileToUpload.equalsIgnoreCase("addSupportDoc")) {
							
							List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
							List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.ADD_SUPP_DOC_INFO_MAP);
							if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
								multiPartList.addAll(previousMultiPartList);
							}
							String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_ADD_SUPP_DOC);
							if(StringUtils.isNotBlank(previousReturnString)){
								returnString = previousReturnString;
							}
							for(MultipartFile addSupportDocFile: addSupportDoc){
								if(returnString == ""){
									returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ADD_SUPPORT_FOLDER, addSupportDocFile, issuerId);
								}else{
									returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ADD_SUPPORT_FOLDER, addSupportDocFile, issuerId);
								}
								Map<String, String> multiPartData = new HashMap<String, String>();
								multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, addSupportDocFile.getOriginalFilename());
								multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, addSupportDocFile.getContentType());
								multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(addSupportDocFile.getSize()));	
								multiPartList.add(multiPartData);
							}
							request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_ADD_SUPP_DOC, returnString);
							request.getSession().setAttribute(PlanMgmtConstants.ADD_SUPP_DOC_INFO_MAP, multiPartList);
						
						} else if (fileToUpload.equalsIgnoreCase("payPolPractices")) {
							
							List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
							List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.CLAIM_PAYPOL_DOC_INFO_MAP);
							if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
								multiPartList.addAll(previousMultiPartList);
							}
							String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_CLAIM_PAYPOL_DOC);
							if(StringUtils.isNotBlank(previousReturnString)){
								returnString = previousReturnString;
							}
							for(MultipartFile payPolPracticesFile: payPolPractices){
								if(returnString == ""){
									returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.PAYMENT_POLICIES_PRACTICES_FOLDER, payPolPracticesFile, issuerId);
								}else{
									returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.PAYMENT_POLICIES_PRACTICES_FOLDER, payPolPracticesFile, issuerId);
								}
								Map<String, String> multiPartData = new HashMap<String, String>();
								multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, payPolPracticesFile.getOriginalFilename());
								multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, payPolPracticesFile.getContentType());
								multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(payPolPracticesFile.getSize()));	
								multiPartList.add(multiPartData);
							}
							request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_CLAIM_PAYPOL_DOC, returnString);
							request.getSession().setAttribute(PlanMgmtConstants.CLAIM_PAYPOL_DOC_INFO_MAP, multiPartList);
						
						} else if (fileToUpload.equalsIgnoreCase("enrollmentAndDis")) {
							
							List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
							List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.ENROLLMENT_DISENROLLMENT_DOCUMENT_INFO_MAP);
							if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
								multiPartList.addAll(previousMultiPartList);
							}
							String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_ENROLLMENT_DISENROLLMENT);
							if(StringUtils.isNotBlank(previousReturnString)){
								returnString = previousReturnString;
							}
							for(MultipartFile enrollmentAndDisFile: enrollmentAndDis){
								if(returnString == ""){
									returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ENROLLMENT_DISENROLLMENT_FOLDER, enrollmentAndDisFile, issuerId);
								}else{
									returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ENROLLMENT_DISENROLLMENT_FOLDER, enrollmentAndDisFile, issuerId);
								}
								Map<String, String> multiPartData = new HashMap<String, String>();
								multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, enrollmentAndDisFile.getOriginalFilename());
								multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, enrollmentAndDisFile.getContentType());
								multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(enrollmentAndDisFile.getSize()));	
								multiPartList.add(multiPartData);
							}
							request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_ENROLLMENT_DISENROLLMENT, returnString);
							request.getSession().setAttribute(PlanMgmtConstants.ENROLLMENT_DISENROLLMENT_DOCUMENT_INFO_MAP, multiPartList);
						
						} else if (fileToUpload.equalsIgnoreCase("ratingPractices")) {
							
							List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
							List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.RATING_PRACTICES_DOC_INFO_MAP);
							if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
								multiPartList.addAll(previousMultiPartList);
							}
							String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_RATING_PRACTICES);
							if(StringUtils.isNotBlank(previousReturnString)){
								returnString = previousReturnString;
							}
							for(MultipartFile ratingPracticesFile: ratingPractices){
								if(returnString == ""){
									returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.RATING_PRACTICES_FOLDER, ratingPracticesFile, issuerId);
								}else{
									returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.RATING_PRACTICES_FOLDER, ratingPracticesFile, issuerId);
								}
								Map<String, String> multiPartData = new HashMap<String, String>();
								multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, ratingPracticesFile.getOriginalFilename());
								multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, ratingPracticesFile.getContentType());
								multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(ratingPracticesFile.getSize()));	
								multiPartList.add(multiPartData);
							}
							request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_RATING_PRACTICES, returnString);
							request.getSession().setAttribute(PlanMgmtConstants.RATING_PRACTICES_DOC_INFO_MAP, multiPartList);
							
						} else if (fileToUpload.equalsIgnoreCase("costSharAndPayment")) {
							List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
							List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.COST_SHARING_PAY_DOC_INFO_MAP);
							if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
								multiPartList.addAll(previousMultiPartList);
							}
							String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_COST_SHARING);
							if(StringUtils.isNotBlank(previousReturnString)){
								returnString = previousReturnString;
							}
							for(MultipartFile costSharAndPaymentFile: costSharAndPayment){
								if(returnString == ""){
									returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.COST_SHARING_PAYMENTS_FOLDER, costSharAndPaymentFile, issuerId);
								}else{
									returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.COST_SHARING_PAYMENTS_FOLDER, costSharAndPaymentFile, issuerId);
								}
								Map<String, String> multiPartData = new HashMap<String, String>();
								multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, costSharAndPaymentFile.getOriginalFilename());
								multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, costSharAndPaymentFile.getContentType());
								multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(costSharAndPaymentFile.getSize()));	
								multiPartList.add(multiPartData);
							}
							request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_COST_SHARING, returnString);
							request.getSession().setAttribute(PlanMgmtConstants.COST_SHARING_PAY_DOC_INFO_MAP, multiPartList);
							
						}
					}else{
						 returnString = "EXT_FAILURE";
					}	 
				}else{
			 returnString = "SIZE_FAILURE";
		 }
		}
		response.setContentType(PlanMgmtConstants.CONTENT_TYPE);
		response.getWriter().write(returnString);
		response.flushBuffer();
		} catch (Exception e) {
			LOGGER.error("Exception in uploadIssuerApplicationDocuments" ,fileToUpload + "file. Ex: " ,e);
			giMonitorService.saveOrUpdateErrorLog(String.valueOf(PlanMgmtErrorCodes.ErrorCode.UPLOAD_ISSUER_APPLICATIONDOC_EXCEPTION), new TSDate(), Exception.class.getName(),ExceptionUtils.getFullStackTrace(e), null);
		}
		return;
	}
	
	
	// uploading the files into their specific folders
	private String uploadFileIntoSpecificFolder(String fileToUpload, String folderName, MultipartFile file, int issuerId) {
		try {
			Issuer issuerObj;
			if (issuerId != 0) {
				issuerObj = issuerService.getIssuerById(issuerId);
			} else {
				issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			}
			String returnString = null;
			String documentId = null;
			String filePath = GhixConstants.ISSUER_DOC_PATH + GhixConstants.FRONT_SLASH + issuerObj.getId() + GhixConstants.FRONT_SLASH + folderName;
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				if(null != file){
					// creating a new file name with the time stamp
					String modifiedFileName = FilenameUtils.getBaseName(file.getOriginalFilename()) + GhixConstants.UNDERSCORE + TimeShifterUtil.currentTimeMillis() 
											+ GhixConstants.DOT + FilenameUtils.getExtension(file.getOriginalFilename());
					// upload file to DMS
					try {
						documentId = ecmService.createContent(filePath, modifiedFileName, file.getBytes()
								, ECMConstants.Issuer.DOC_CATEGORY, ECMConstants.Issuer.DOC_SUB_CATEGORY_SUPPORT, null);
					} catch (Exception e) {
						LOGGER.error("Error while reading File from DMS : " + e.getMessage());
					}
					returnString = fileToUpload + "|" + file.getOriginalFilename() + "|" + documentId; // return original file name and DMS id
					// LOGGER.info("Upload issuer documents DocumentId: " + documentId);
				}
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
			return returnString;
		} catch (Exception ex) {
			LOGGER.error("Fail to uplaod issuer" , SecurityUtil.sanitizeForLogging(fileToUpload) + "file. Ex: "
					,ex);
			throw new GIRuntimeException(
					PlanMgmtConstants.MODULE_NAME
							+ PlanMgmtErrorCodes.ErrorCode.UPLOADFILE_TO_SPECIFIC_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH);
		}
		
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@SuppressWarnings("unchecked")
	// saving the uploaded files into the issuer documents table
	@RequestMapping(value = "/planmgmt/issuer/application/documents/save", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'EDIT_ACCREDITATION_DOCUMENTS')")
	public String saveIssuerApplicationDocuments(Model model, HttpServletRequest request, @RequestParam(value = "issuerAccreditation", required = false) String issuerAccreditation, @RequestParam(value = "accreditingEntity", required = false) String accreditingEntity, @RequestParam(value = "hdnAuthority", required = false) String authority, @RequestParam(value = "hdnAccrediting", required = false) String accrediting, @RequestParam(value = "hdnOrgData", required = false) String orgData, @RequestParam(value = "hdnMarketing", required = false) String marketing, @RequestParam(value = "hdnFinDisclosures", required = false) String finDisclosures, @RequestParam(value = "hdnAddSupportDoc", required = false) String addSupportDoc, @RequestParam(value = "hdnPayment_policies_practices", required = false) String hdnPayment_policies_practices, @RequestParam(value = "hdnEnrollment_Disenrollment", required = false) String hdnEnrollment_Disenrollment, @RequestParam(value = "hdnRating_practices", required = false) String hdnRating_practices, @RequestParam(value = "hdnCost_sharing_payments", required = false) String hdnCost_sharing_payments, @RequestParam(value = "accreditationExpDate", required = false) String accreditationExpDate) {
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
	
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				List<IssuerDocument> authorityDocuments = null;
				List<IssuerDocument> accreditingDocuments = null;
				List<IssuerDocument> orgDataDocuments = null;
				List<IssuerDocument> marketingDocuments = null;
				List<IssuerDocument> finDisclosuresDocuments = null;
				List<IssuerDocument> addSupportDocDocuments = null;
				List<IssuerDocument> enrollmentAndDisenrollments = null;
				List<IssuerDocument> ratingPracticess = null;
				List<IssuerDocument> costSharingPaments = null;
				List<IssuerDocument> paymentyPoliciesAndPractices = null;
				List<IssuerDocument> issuerDocuments = new ArrayList<IssuerDocument>();
				
				if(null != issuerObj){
	
					if (request.getSession().getAttribute(PlanMgmtConstants.AUTHORITY_DOCUMENT_INFO_MAP) != null) {
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.AUTHORITY_DOCUMENT_INFO_MAP);
							authorityDocuments = getNewIssuerApplicationDocuments(authority, DocumentType.AUTHORITY, issuerObj, multiPartList);
							issuerDocuments.addAll(authorityDocuments);
		
					}
		
					if (request.getSession().getAttribute(PlanMgmtConstants.ACCREDITATION_DOC_INFO_MAP) != null) {
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						multiPartList = (List<Map<String, String>>)  request.getSession().getAttribute(PlanMgmtConstants.ACCREDITATION_DOC_INFO_MAP);
							accreditingDocuments = getNewIssuerApplicationDocuments(accrediting, DocumentType.ACCREDITATION, issuerObj, multiPartList);
							issuerDocuments.addAll(accreditingDocuments);
					}
		
					if (request.getSession().getAttribute(PlanMgmtConstants.ORG_DATA_DOC_INFO_MAP) != null) {
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.ORG_DATA_DOC_INFO_MAP);
							orgDataDocuments = getNewIssuerApplicationDocuments(orgData, DocumentType.ORGANIZATION, issuerObj, multiPartList);
							issuerDocuments.addAll(orgDataDocuments);
					}
		
					if (request.getSession().getAttribute(PlanMgmtConstants.MARKETING_DOC_INFO_MAP) != null) {
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.MARKETING_DOC_INFO_MAP);
							marketingDocuments = getNewIssuerApplicationDocuments(marketing, DocumentType.MARKETING, issuerObj, multiPartList);
							issuerDocuments.addAll(marketingDocuments);
					}
		
					if (request.getSession().getAttribute(PlanMgmtConstants.FIN_CLOSURES_DOC_INFO_MAP) != null) {
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.FIN_CLOSURES_DOC_INFO_MAP);
							finDisclosuresDocuments = getNewIssuerApplicationDocuments(finDisclosures, DocumentType.FINANCIAL_DISCLOSURE, issuerObj, multiPartList);
							issuerDocuments.addAll(finDisclosuresDocuments);
					}
		
					if (request.getSession().getAttribute(PlanMgmtConstants.ADD_SUPP_DOC_INFO_MAP) != null) {
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.ADD_SUPP_DOC_INFO_MAP);
							addSupportDocDocuments = getNewIssuerApplicationDocuments(addSupportDoc, DocumentType.ADDITIONAL_SUPPORT, issuerObj, multiPartList);
							issuerDocuments.addAll(addSupportDocDocuments);
					}
		
					if (request.getSession().getAttribute(PlanMgmtConstants.CLAIM_PAYPOL_DOC_INFO_MAP) != null) {
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.CLAIM_PAYPOL_DOC_INFO_MAP);
							paymentyPoliciesAndPractices = getNewIssuerApplicationDocuments(hdnPayment_policies_practices, DocumentType.CLAIMS_PAY_POL, issuerObj, multiPartList);
							issuerDocuments.addAll(paymentyPoliciesAndPractices);
					}
		
					if (request.getSession().getAttribute("enrollDisenrollDocumentInfoMap") != null) {
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						multiPartList = (List<Map<String, String>>) request.getSession().getAttribute("enrollDisenrollDocumentInfoMap");
							enrollmentAndDisenrollments = getNewIssuerApplicationDocuments(hdnEnrollment_Disenrollment, DocumentType.ENROLLMENT_DISENROLLMENT, issuerObj, multiPartList);
							issuerDocuments.addAll(enrollmentAndDisenrollments);
					}
		
					if (request.getSession().getAttribute(PlanMgmtConstants.RATING_PRACTICES_DOC_INFO_MAP) != null) {
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.RATING_PRACTICES_DOC_INFO_MAP);
							ratingPracticess = getNewIssuerApplicationDocuments(hdnRating_practices, DocumentType.RATING_PRACTICES, issuerObj, multiPartList);
							issuerDocuments.addAll(ratingPracticess);
					}
		
					if (request.getSession().getAttribute(PlanMgmtConstants.COST_SHARING_PAY_DOC_INFO_MAP) != null) {
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						multiPartList = (List<Map<String, String>>)  request.getSession().getAttribute(PlanMgmtConstants.COST_SHARING_PAY_DOC_INFO_MAP);
							costSharingPaments = getNewIssuerApplicationDocuments(hdnCost_sharing_payments, DocumentType.COST_SHARING_PAYMENTS, issuerObj, multiPartList);
							issuerDocuments.addAll(costSharingPaments);
					}
					issuerObj.setIssuerDocuments(issuerDocuments);
	
					if (issuerAccreditation.equalsIgnoreCase(IssuerAccreditation.YES.toString())) {
						issuerObj.setIssuerAccreditation(IssuerAccreditation.YES);
					} else if (issuerAccreditation.equalsIgnoreCase(IssuerAccreditation.NO.toString())) {
						issuerObj.setIssuerAccreditation(IssuerAccreditation.NO);
					} else {
						issuerObj.setIssuerAccreditation(IssuerAccreditation.EXCELLENT);
					}
	
					issuerObj.setAccreditingEntity(accreditingEntity);
	
					if (accreditationExpDate != null && !accreditationExpDate.isEmpty()) {
						Date date;
						SimpleDateFormat dateFormat = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT_MMDDYYYY);
						date = dateFormat.parse(accreditationExpDate);
						issuerObj.setAccreditationExpDate(date);
					}else{
						issuerObj.setAccreditationExpDate(null);
					}
					
					issuerObj.setLastUpdatedBy(userService.getLoggedInUser().getId());
					
					//server side validation
					 Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
					 Set<ConstraintViolation<Issuer>> violations = validator.validate(issuerObj, Issuer.EditAccreditationDocuments.class);
					 if(violations != null && !violations.isEmpty()){
						//throw exception in case client side validation breached
						 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					 }
					// Save issuer
					issuerService.saveIssuer(issuerObj);
					issuerService.removeDataFromSession(request);
		
					return "redirect:/planmgmt/issuer/account/profile/accreditationdocument/view";
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Exception in saveIssuerApplicationDocuments: "
					+ ex.getMessage());
			throw new GIRuntimeException(
					PlanMgmtConstants.MODULE_NAME
							+ PlanMgmtErrorCodes.ErrorCode.SADPENROLLMENT_REPORT_LIST_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	private List<IssuerDocument> getNewIssuerApplicationDocuments(String documentId, DocumentType documentType, Issuer issuer,  List<Map<String, String>>  multipartFileData) {
		List<String> filesList = new ArrayList<String>();
		String []splittedDocumentIds = documentId.split("\\|");
		for(String docId: splittedDocumentIds){
			filesList.add(docId);
		}
		return getIssuerDocumentsSave(filesList, documentType, issuer, multipartFileData);
	}

	private List<IssuerDocument> getIssuerDocumentsSave(List<String> files, DocumentType documentType, Issuer issuer,  List<Map<String, String>>  multipartFileData) {
		List<IssuerDocument> documents = new ArrayList<IssuerDocument>();
		Integer userId = null;
		try {
			userId = userService.getLoggedInUser().getId();
		} catch (InvalidUserException e) {
			LOGGER.error("Invalid User");
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME
					+ PlanMgmtErrorCodes.ErrorCode.INVALID_USER_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		for(int i=0; i<files.size(); i++){
			IssuerDocument document = new IssuerDocument();
			document.setDocumentName(files.get(i));
			document.setDocumentType(documentType.name());
			document.setIssuer(issuer);
			documents.add(document);
			document.setCreatedBy(userId);
			document.setLastUpdatedBy(userId);
			document.setFileName(multipartFileData.get(i).get(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME));
			document.setFileType(multipartFileData.get(i).get(PlanMgmtConstants.MULTIPART_CONTENTTYPE));
			document.setFileSize(Long.parseLong(multipartFileData.get(i).get(PlanMgmtConstants.MULTIPART_SIZE)));
		}
		return documents;
	}

	@SuppressWarnings("unused")
	private List<IssuerDocument> getNewIssuerApplicationDocuments(String documentId, DocumentType documentType, Issuer issuer) {
		List<String> filesList = new ArrayList<String>();
		filesList.add(documentId);
		return getIssuerDocuments(filesList, documentType, issuer);
	}

	
	
	@RequestMapping(value = "/planmgmt/issuer/profile/update/company", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String newIssuerUpdateCompanyProfile(Model model) {
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is
														// issuer or issuer rep
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.STATES_OBJ, new StateHelper().getAllStates());
				model.addAttribute(PlanMgmtConstants.REDIRECT_URL, "redirect:/planmgmt/issuer/profile/update/market/inidividual");
				LOGGER.info("New Issuer Update profile : Company Page Info");
				return "planmgmt/issuer/profile/update/company";
			}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.UPDATE_COMPANY_PROFILE_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	/*To display issuer logo on page load
	 * This should not be used, instead use getIssuerLogoByHiosIssuerId */
	@RequestMapping(value = "/planmgmt/issuer/profile/logo/view/{encIssuerId}", method = RequestMethod.GET, produces="image/png;image/jpeg; charset=utf-8")
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	@ResponseBody
	public byte[] getIssuerLogoById(@PathVariable("encIssuerId") String encIssuerId, HttpServletResponse response)
			throws ContentManagementServiceException, IOException {

		byte[] bytes = null;

		try {
			bytes = issuerService.getIssuerLogoById(encIssuerId);
			response.setContentType(planMgmtUtils.getContentType(bytes));
			FileCopyUtils.copy(bytes, response.getOutputStream());
		}
		catch (Exception e) {
			LOGGER.error("Error for Company Logo - ", e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.ISSUER_LOGO_ID_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),
					e.getStackTrace().toString(), null);
		}
		return null;
	}

	/*To display issuer logo on page load*/
	@RequestMapping(value = "/planmgmt/issuer/profile/logo/hid/{hiosIssuerId}", method = RequestMethod.GET, produces="image/png;image/jpeg; charset=utf-8")
	//@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	@ResponseBody
	public byte[] getIssuerLogoByHiosIssuerId(@PathVariable("hiosIssuerId") String hiosIssuerId, HttpServletResponse response)
			throws ContentManagementServiceException, IOException {

		byte[] bytes = null;

		try {
			bytes = issuerService.getIssuerLogoByHiosId(hiosIssuerId);
			response.setContentType(planMgmtUtils.getContentType(bytes));
			FileCopyUtils.copy(bytes, response.getOutputStream());
		}
		catch (Exception e) {
			LOGGER.error("Error for Company Logo - ", e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.ISSUER_LOGO_ID_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),
					e.getStackTrace().toString(), null);
		}
		return null;
	}
	
	/**
	 * @param model, Model Object
	 * This is depricated and should not be used. Instead use one which return logo by HIOS ID
	 * @return destination page.
	 */
	
	@RequestMapping(value = "/planmgmt/issuer/downloadlogobyid/{encIssuerId}", method = RequestMethod.GET, produces="image/png;image/jpeg; charset=utf-8")
	@ResponseBody
	public byte[] downloadIssuerLogo(@PathVariable("encIssuerId") String encIssuerId,HttpServletResponse response) {
		try {
			byte[] bytes = issuerService.getIssuerLogoById(encIssuerId);

			response.setHeader("Content-Disposition", "attachment;filename=logo.png");
			response.setContentType(planMgmtUtils.getContentType(bytes));
			FileCopyUtils.copy(bytes, response.getOutputStream());
		}catch (Exception e) {
			LOGGER.error("Exception to decrypt issuer id", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOADDOCUMENTS_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		
		return null;
	}		
	
	/**
	 * @param model, Model Object
	 * This is depricated and should not be used. Instead use one which return logo by HIOS ID
	 * @return destination page.
	 */
	@RequestMapping(value = "/planmgmt/issuer/downloadlogo/{hiosIssuerId}", method = RequestMethod.GET, produces="image/png;image/jpeg; charset=utf-8")
	@ResponseBody
	public byte[] downloadIssuerLogoByHiosId(@PathVariable("hiosIssuerId") String hiosIssuerId,HttpServletResponse response) {
		try {
			byte[] bytes = issuerService.getIssuerLogoByHiosId(hiosIssuerId);

			response.setHeader("Content-Disposition", "attachment;filename=logo.png");
			response.setContentType(planMgmtUtils.getContentType(bytes));
			FileCopyUtils.copy(bytes, response.getOutputStream());
		}catch (Exception e) {
			LOGGER.error("Exception to get issuer logo by HIOS ID ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOADDOCUMENTS_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		
		return null;
	}		
	
	// saving the company profile information into the issuer table.
	@RequestMapping(value = "/planmgmt/issuer/profile/update/company/save", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String saveCompanyProfile(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuer, Model model,
			@RequestParam(value = PlanMgmtConstants.COMPANY_LOGOUI, required = true) MultipartFile companyLogo,
			@RequestParam(value = "redirectTo", required = false) String redirectUrl,HttpServletRequest request,
			RedirectAttributes redirectAttributes) {
		boolean siteUrlValidation = false;
		boolean companySiteUrlValidation = false;
		try {
			//server side validation
			siteUrlValidation = planMgmtUtils.isValidUrl(issuer.getSiteUrl());
			companySiteUrlValidation = planMgmtUtils.isValidUrl(issuer.getCompanySiteUrl());
			
			if(siteUrlValidation && companySiteUrlValidation){
				Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
				Set<ConstraintViolation<Issuer>> violations = validator.validate(issuer, Issuer.EditCompanyProfile.class);
				if(violations != null && !violations.isEmpty()){
					//throw exception in case client side validation breached
					throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				}
			}else{
				throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			}
			
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				if(null != issuerObj){
				//	model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
					LOGGER.info("New Issuer Update profile : Save Company Page Info");
					// set the values to the issuer object.
					issuerObj.setCompanyLegalName(issuer.getCompanyLegalName());
					issuerObj.setStateOfDomicile(issuer.getStateOfDomicile());
					issuerObj.setCompanyAddressLine1(issuer.getCompanyAddressLine1());
					issuerObj.setCompanyAddressLine2(issuer.getCompanyAddressLine2());
					issuerObj.setCompanyCity(issuer.getCompanyCity());
					issuerObj.setCompanyState(issuer.getCompanyState());
					issuerObj.setCompanyZip(issuer.getCompanyZip());
					if(StringUtils.isNotBlank(issuer.getSiteUrl())){
						issuerObj.setSiteUrl(issuer.getSiteUrl());
					}else{
						issuerObj.setSiteUrl(null);
					}
					if(StringUtils.isNotBlank(issuer.getCompanySiteUrl())){
						issuerObj.setCompanySiteUrl(issuer.getCompanySiteUrl());
					}else{
						issuerObj.setCompanySiteUrl(null);
					}
					issuerObj.setNationalProducerNumber(issuer.getNationalProducerNumber());
					issuerObj.setAgentFirstName(issuer.getAgentFirstName());
					issuerObj.setAgentLastName(issuer.getAgentLastName());
					issuerObj.setPaymentUrl(issuer.getPaymentUrl());
					issuerObj.setDisclaimer(issuer.getDisclaimer());
					issuerObj.setLastUpdatedBy(userService.getLoggedInUser().getId());
					// Upload Company LOGO at CDN server and set LOGO Content(BLOG) and CDN URL in Issuer table.
					String uploadStatus = planMgmtUtils.uploadLogoAndSetDetails(companyLogo, issuerObj);
					if(PlanMgmtConstants.SUCCESS.equalsIgnoreCase(uploadStatus)) {
						// save the updated details of the issuer in the issuer table
						issuerService.saveIssuer(issuerObj);
						LOGGER.info("New Issuer Update profile : Saved the Issuer information");
					}else {
						LOGGER.error("New Issuer Update profile : Error while Issuer information save");
						redirectAttributes.addFlashAttribute("ERROR", uploadStatus);
						return "redirect:/planmgmt/issuer/account/profile/company/edit";
					}
					return redirectUrl;
				}
			}
		}
		catch (GIRuntimeException ex) {
			throw ex;
		}
		catch (Exception e) {
			LOGGER.error("Error while setting updatedBy in saveCompanyProfile", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+ PlanMgmtErrorCodes.ErrorCode.SAVE_COMPANY_PROFILE_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	@RequestMapping(value = "/planmgmt/issuer/profile/update/market/inidividual", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String newIssuerUpdateIndividualMarketProfile(Model model) {
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.REDIRECT_URL, "redirect:/planmgmt/issuer/profile/update/market/shop");
				LOGGER.info("New Issuer Update profile : Individual Market Page Info");
				return "planmgmt/issuer/profile/update/market/individual";
			}
		} catch (Exception e) {
			LOGGER.error("Error while updating individual market profile", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.UPDATE_INDIVIDUAL_MARKET_PROFILE_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	@RequestMapping(value = "/planmgmt/issuer/profile/update/market/shop", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String newIssuerUpdateShopMarketProfile(Model model) {
	
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
				
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is
														// issuer or issuer rep
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.REDIRECT_URL, "redirect:/planmgmt/issuer/sign/application");
				LOGGER.info("New Issuer Update profile : Shop Market Page Info");
				return "planmgmt/issuer/profile/update/market/shop";
			}
		} catch (Exception e) {
			LOGGER.error("Error while updating individual market profile", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.ISSUER_SHOP_MARKET_PROFILE_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	@RequestMapping(value = "/planmgmt/issuer/profile/update/market/save", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String saveIssuerMarketProfile(Model model,@ModelAttribute(PlanMgmtConstants.MARKETPROFILE) MarketProfileDTO marketProfileDTO,
			@RequestParam(value = PlanMgmtConstants.MARKET, required = false) String market, 
			@RequestParam(value = "redirectTo", required = false) String redirectUrl) {
		boolean facingUrlValidation = false;
		try{
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
				//server side validation
				facingUrlValidation = planMgmtUtils.isValidUrl(marketProfileDTO.getFacingWebSite());
				if(facingUrlValidation){
					Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
					 Set<ConstraintViolation<MarketProfileDTO>> violations = validator.validate(marketProfileDTO, MarketProfileDTO.EditIndividualProfile.class);
					 if(violations != null && !violations.isEmpty()){
						 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					 }
				}else{
					throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				}
				
				if(marketProfileDTO.getCustomerServicePhone().isEmpty() && !marketProfileDTO.getCustomerServiceExt().isEmpty()){
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				}
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				if(null != issuerObj){
					//model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
					LOGGER.info("New Issuer Update profile : Save Market Info");
					if ("individual".equalsIgnoreCase(market)) {
						// setting the individual market profile info to the issuer
						issuerObj.setIndvCustServicePhone(marketProfileDTO.getCustomerServicePhone());
						issuerObj.setIndvCustServicePhoneExt(marketProfileDTO.getCustomerServiceExt());
						issuerObj.setIndvCustServiceTollFree(marketProfileDTO.getCustServiceTollFreeNumber());
						issuerObj.setIndvCustServiceTTY(marketProfileDTO.getCustServiceTTY());
						//issuerObj.setIndvSiteUrl(marketProfileDTO.getFacingWebSite());
						if(StringUtils.isNotBlank(marketProfileDTO.getFacingWebSite())){
							issuerObj.setIndvSiteUrl(marketProfileDTO.getFacingWebSite());
						}else{
							issuerObj.setIndvSiteUrl(null);
						}
						
//						if(StringUtils.isNotBlank(marketProfileDTO.getPaymenturl())){
//							issuerObj.setPaymentUrl(marketProfileDTO.getPaymenturl());
//						}else{
//							issuerObj.setPaymentUrl(null);
//						}
		
					} else if ("shop".equalsIgnoreCase(market)) {
						// setting the shop market profile info to the issuer object
						issuerObj.setShopCustServicePhone(marketProfileDTO.getCustomerServicePhone());
						issuerObj.setShopCustServicePhoneExt(marketProfileDTO.getCustomerServiceExt());
						issuerObj.setShopCustServiceTollFree(marketProfileDTO.getCustServiceTollFreeNumber());
						issuerObj.setShopCustServiceTTY(marketProfileDTO.getCustServiceTTY());
						//issuerObj.setShopSiteUrl(marketProfileDTO.getFacingWebSite());
						if(StringUtils.isNotBlank(marketProfileDTO.getFacingWebSite())){
							issuerObj.setShopSiteUrl(marketProfileDTO.getFacingWebSite());
						}else{
							issuerObj.setShopSiteUrl(null);
						}
		
					}
					issuerObj.setLastUpdatedBy(userService.getLoggedInUser().getId());
					issuerService.saveIssuer(issuerObj);
					LOGGER.debug("New Issuer Update profile : Saved " + SecurityUtil.sanitizeForLogging(String.valueOf(market)) + " Market Info");
					return redirectUrl;
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while setting updatedBy in saveIssuerMarketProfile", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.SAVE_ISSUER_MARKET_PROFILE_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}


	// issuer account profile landing info
	@RequestMapping(value = "/planmgmt/issuer/account/profile/issuerprofile", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String viewProfileInfo(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		/*try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				if(null != issuerObj){
					LOGGER.info("Issuer Account :: Issuer Profile Page");
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
					model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, exchangeType);
					model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
				}
				return "planmgmt/issuer/account/profile/issuerprofile";
			}
		} catch (Exception ex) {
			LOGGER.error("Exception in view profile info " + ex.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_PROFILE_INFO_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(ex), Severity.LOW);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
		*/
		
		// HIX-82314 - redirect issuer representative to issuer landing page
		return "redirect:/planmgmt/issuer/landing/info";
	}

	

	// issuer account company profile
	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/planmgmt/issuer/account/profile/company/info", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'VIEW_COMPNAY_PROFILE')")
	public String viewCompanyProfileInfo(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
				
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				if(null != issuerObj){
					LOGGER.info("Issuer Account :: Company profile Information Page");
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
					model.addAttribute(PlanMgmtConstants.COMPANY_LOGO, PlanMgmtUtil.getIssuerLogoURLByHIOSID(issuerObj.getLogoURL(), issuerObj.getHiosIssuerId(), appUrl));
					if(null != issuerObj.getLogo()){
						model.addAttribute(PlanMgmtConstants.COMPANY_LOGO_EXIST, true);
					}
					model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
					model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
				}
				return "planmgmt/issuer/account/profile/company/info";
			}
		} catch (Exception ex) {
			LOGGER.error("Exception in viewCompanyProfileInfo "
					+ ex.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_COMPANY_PROFILE_INFO_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(ex), Severity.LOW);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	// issuer account edit company profile
	@RequestMapping(value = "/planmgmt/issuer/account/profile/company/edit", method = {RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'EDIT_COMPNAY_PROFILE')")
	public String editCompanyProfileInfo(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				LOGGER.info("Issuer Account :: Edit Company profile Information Page");
				
				String issuerWebsite = issuerObj.getSiteUrl();
		 	 	String facingWebSite = issuerObj.getCompanySiteUrl();
		 	 	if(StringUtils.isNotBlank(issuerWebsite)){
		 	 		if(issuerWebsite.startsWith(APPEND_HTTP) || issuerWebsite.startsWith(APPEND_HTTPS)){
		 	 			issuerObj.setSiteUrl(issuerWebsite);
		 	 		}else{
		 	 			issuerObj.setSiteUrl(APPEND_HTTP+issuerWebsite);
		 	 		}
		 	 	}else{
		 	 		issuerObj.setSiteUrl(null);
		 	 	}
		 	 	
		 	 	if(StringUtils.isNotBlank(facingWebSite)){
		 	 		if(facingWebSite.startsWith(APPEND_HTTP) || facingWebSite.startsWith(APPEND_HTTPS)){
		 	 			issuerObj.setCompanySiteUrl(facingWebSite);
		 	 		}else{
		 	 			issuerObj.setCompanySiteUrl(APPEND_HTTP+facingWebSite);
		 	 		}
		 	 	}else{
		 	 		issuerObj.setCompanySiteUrl(null);
		 	 	}
		 	 	
		 	 	//String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.COMPANY_LOGO, PlanMgmtUtil.getIssuerLogoURLByHIOSID(issuerObj.getLogoURL(), issuerObj.getHiosIssuerId(), appUrl));
				if(null != issuerObj.getLogo()){
					model.addAttribute(PlanMgmtConstants.COMPANY_LOGO_EXIST, true);
				}
				model.addAttribute(PlanMgmtConstants.STATES_OBJ, new StateHelper().getAllStates());
				model.addAttribute(PlanMgmtConstants.REDIRECT_URL, "redirect:/planmgmt/issuer/account/profile/company/info");
				model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
				model.addAttribute(PlanMgmtConstants.STATE_CODE, stateCode);
				if (stateCode.equals(PlanMgmtConstants.STAECODEFORCA)) {
					model.addAttribute(PlanMgmtConstants.STATECODEONEXCHANGE, stateCode);
				} else if (stateCode.equals(GhixConstants.NM)) {
					model.addAttribute(PlanMgmtConstants.STATECODEONEXCHANGE, GhixConstants.NM);
				}else if (stateCode.equals(GhixConstants.MS)) {
					model.addAttribute(PlanMgmtConstants.STATECODEONEXCHANGE, GhixConstants.MS);
				}else if (stateCode.equals(PlanMgmtConstants.ID_CODE)) {
					model.addAttribute(PlanMgmtConstants.STATECODEONEXCHANGE, PlanMgmtConstants.ID_CODE);
				}
				return "planmgmt/issuer/account/profile/company/edit";
			}
		} catch (Exception ex) {
			LOGGER.error("Exception in editCompanyProfileInfo " + ex.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_COMPANY_PROFILE_INFO_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(ex), Severity.LOW);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	// issuer account individual market profile
	@RequestMapping(value = "/planmgmt/issuer/account/profile/market/individual/info", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'VIEW_INDIVIDUAL_MARKET_PROFILE')")
	public String viewIndividualMarketProfileInfo(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				if(null != issuerObj){
					LOGGER.info("Issuer Account :: Individual Market Profile Information Page");
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
					model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
					model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
				}
				return "planmgmt/issuer/account/profile/market/individual/info";
			}
		} catch (Exception ex) {
			LOGGER.error("Exception in viewIndividualMarketProfileInfo "
					+ ex.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_INDIVIDUAL_MARKET_PROFILE_INFO_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(ex), Severity.LOW);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	// issuer account edit individual market profile
	@RequestMapping(value = "/planmgmt/issuer/account/profile/market/individual/edit", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'EDIT_INDIVIDUAL_MARKET_PROFILE')")
	public String editIndividualMarketProfileInfo(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				LOGGER.info("Issuer Account :: Edit Individual Market Profile Information Page");
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.REDIRECT_URL, "redirect:/planmgmt/issuer/account/profile/market/individual/info");
				model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
				model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
				if(org.apache.commons.lang.StringUtils.isNotBlank(issuerObj.getIndvCustServicePhone()) && issuerObj.getIndvCustServicePhone().length() >= PlanMgmtConstants.TEN) {
					String customerPhoneNumber = issuerObj.getIndvCustServicePhone().replace(PlanMgmtConstants.DASH_SIGN_COMPACT, PlanMgmtConstants.EMPTY_STRING);
					if(customerPhoneNumber.length() > PlanMgmtConstants.TEN){
						customerPhoneNumber = customerPhoneNumber.substring(PlanMgmtConstants.ONE);
					}
					model.addAttribute(PlanMgmtConstants.PHONE1, customerPhoneNumber.substring(PlanMgmtConstants.ZERO,PlanMgmtConstants.THREE));
					model.addAttribute(PlanMgmtConstants.PHONE2, customerPhoneNumber.substring(PlanMgmtConstants.THREE,PlanMgmtConstants.SIX));
					model.addAttribute(PlanMgmtConstants.PHONE3, customerPhoneNumber.substring(PlanMgmtConstants.SIX));
				}
				if(org.apache.commons.lang.StringUtils.isNotBlank(issuerObj.getIndvCustServiceTollFree()) && issuerObj.getIndvCustServiceTollFree().length() >= PlanMgmtConstants.TEN) {
					String customerPhoneNumber = issuerObj.getIndvCustServiceTollFree().replace(PlanMgmtConstants.DASH_SIGN_COMPACT, PlanMgmtConstants.EMPTY_STRING);
					if(customerPhoneNumber.length() > PlanMgmtConstants.TEN){
						customerPhoneNumber = customerPhoneNumber.substring(PlanMgmtConstants.ONE);
					}
					model.addAttribute(PlanMgmtConstants.PHONE4, customerPhoneNumber.substring(PlanMgmtConstants.ZERO,PlanMgmtConstants.THREE));
					model.addAttribute(PlanMgmtConstants.PHONE5, customerPhoneNumber.substring(PlanMgmtConstants.THREE,PlanMgmtConstants.SIX));
					model.addAttribute(PlanMgmtConstants.PHONE6, customerPhoneNumber.substring(PlanMgmtConstants.SIX));
				}
				if(org.apache.commons.lang.StringUtils.isNotBlank(issuerObj.getIndvCustServiceTTY()) && issuerObj.getIndvCustServiceTTY().length() >= PlanMgmtConstants.TEN) {
					String customerPhoneNumber = issuerObj.getIndvCustServiceTTY().replace(PlanMgmtConstants.DASH_SIGN_COMPACT, PlanMgmtConstants.EMPTY_STRING);
					if(customerPhoneNumber.length() > PlanMgmtConstants.TEN){
						customerPhoneNumber = customerPhoneNumber.substring(PlanMgmtConstants.ONE);
					}
					model.addAttribute(PlanMgmtConstants.PHONE7, customerPhoneNumber.substring(PlanMgmtConstants.ZERO,PlanMgmtConstants.THREE));
					model.addAttribute(PlanMgmtConstants.PHONE8, customerPhoneNumber.substring(PlanMgmtConstants.THREE,PlanMgmtConstants.SIX));
					model.addAttribute(PlanMgmtConstants.PHONE9, customerPhoneNumber.substring(PlanMgmtConstants.SIX));
				}
				return "planmgmt/issuer/account/profile/market/individual/edit";
			}
		} catch (Exception ex) {
			LOGGER.error("Exception in editIndividualMarketProfileInfo " + ex.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_INDIVIDUAL_MARKET_PROFILE_INFO_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(ex), Severity.LOW);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	// issuer account shop market profile
	@RequestMapping(value = "/planmgmt/issuer/account/profile/market/shop/info", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'VIEW_SHOP_MARKET_PROFILE')")
	public String viewShopMarketProfileInfo(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				if(null != issuerObj){
					LOGGER.info("Issuer Account :: Shop Market Profile Information Page");
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
					model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
				}
				return "planmgmt/issuer/account/profile/market/shop/info";
			}
		} catch (Exception ex) {
			LOGGER.error("Exception in viewShopMarketProfileInfo " + ex.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_SHOP_MARKET_PROFILE_INFO_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(ex), Severity.LOW);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	// issuer account edit shop market profile
	@RequestMapping(value = "/planmgmt/issuer/account/profile/market/shop/edit", method = {RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'EDIT_SHOP_MARKET_PROFILE')")
	public String editShopMarketProfileInfo(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				LOGGER.info("Issuer Account :: Edit Shop Market Profile Information Page");
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.REDIRECT_URL, "redirect:/planmgmt/issuer/account/profile/market/shop/info");
				if(org.apache.commons.lang.StringUtils.isNotBlank(issuerObj.getShopCustServicePhone()) && issuerObj.getShopCustServicePhone().length() >= PlanMgmtConstants.TEN) {
					String customerPhoneNumber = issuerObj.getShopCustServicePhone().replace(PlanMgmtConstants.DASH_SIGN_COMPACT, PlanMgmtConstants.EMPTY_STRING);
					if(customerPhoneNumber.length() > PlanMgmtConstants.TEN){
						customerPhoneNumber = customerPhoneNumber.substring(PlanMgmtConstants.ONE);
					}
					model.addAttribute(PlanMgmtConstants.PHONE1, customerPhoneNumber.substring(PlanMgmtConstants.ZERO,PlanMgmtConstants.THREE));
					model.addAttribute(PlanMgmtConstants.PHONE2, customerPhoneNumber.substring(PlanMgmtConstants.THREE,PlanMgmtConstants.SIX));
					model.addAttribute(PlanMgmtConstants.PHONE3, customerPhoneNumber.substring(PlanMgmtConstants.SIX));
				}
				if(org.apache.commons.lang.StringUtils.isNotBlank(issuerObj.getShopCustServiceTollFree()) && issuerObj.getShopCustServiceTollFree().length() >= PlanMgmtConstants.TEN) {
					String customerPhoneNumber = issuerObj.getShopCustServiceTollFree().replace(PlanMgmtConstants.DASH_SIGN_COMPACT, PlanMgmtConstants.EMPTY_STRING);
					if(customerPhoneNumber.length() > PlanMgmtConstants.TEN){
						customerPhoneNumber = customerPhoneNumber.substring(PlanMgmtConstants.ONE);
					}
					model.addAttribute(PlanMgmtConstants.PHONE4, customerPhoneNumber.substring(PlanMgmtConstants.ZERO,PlanMgmtConstants.THREE));
					model.addAttribute(PlanMgmtConstants.PHONE5, customerPhoneNumber.substring(PlanMgmtConstants.THREE,PlanMgmtConstants.SIX));
					model.addAttribute(PlanMgmtConstants.PHONE6, customerPhoneNumber.substring(PlanMgmtConstants.SIX));
				}
				if(org.apache.commons.lang.StringUtils.isNotBlank(issuerObj.getShopCustServiceTTY()) && issuerObj.getShopCustServiceTTY().length() >= PlanMgmtConstants.TEN) {
					String customerPhoneNumber = issuerObj.getShopCustServiceTTY().replace(PlanMgmtConstants.DASH_SIGN_COMPACT, PlanMgmtConstants.EMPTY_STRING);
					if(customerPhoneNumber.length() > PlanMgmtConstants.TEN){
						customerPhoneNumber = customerPhoneNumber.substring(PlanMgmtConstants.ONE);
					}
					model.addAttribute(PlanMgmtConstants.PHONE7, customerPhoneNumber.substring(PlanMgmtConstants.ZERO,PlanMgmtConstants.THREE));
					model.addAttribute(PlanMgmtConstants.PHONE8, customerPhoneNumber.substring(PlanMgmtConstants.THREE,PlanMgmtConstants.SIX));
					model.addAttribute(PlanMgmtConstants.PHONE9, customerPhoneNumber.substring(PlanMgmtConstants.SIX));
				}
				return "planmgmt/issuer/account/profile/market/shop/edit";
			}
		} catch (Exception ex) {
			LOGGER.error("Exception in editShopMarketProfileInfo " + ex.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_SHOP_MARKET_PROFILE_INFO_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(ex), Severity.LOW);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	
	
	@SuppressWarnings(PlanMgmtConstants.STATIC_ACCESS)
	@RequestMapping(value = "/planmgmt/issuercertificationverifiinfo", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String issuerDashboard(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer
				Issuer issuerObjDashboard = issuerService.getIssuerById(issuerService.getIssuerId());
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObjDashboard);
				model.addAttribute("issuerStatusRegistered", issuerService.CERT_STATUS_WHEN_REGISTERED);
				LOGGER.info("Issuer Certification : Verify Account Info");
				return "planmgmt/issuercertificationverifiinfo";
			}
		} catch (Exception e) {
			LOGGER.error("Exception in issuerDashboard : ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.ISSUER_DASHBOARD_EXCEPTION.getCode(), 
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);

		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}


	@RequestMapping(value = "/planmgmt/updateFiles", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	@ResponseBody
	public String deleteFiles(Model model, HttpServletRequest request) {
		try {
			String response = "FAILED";
			if (!issuerService.isIssuerRepLoggedIn()) {
				return response;
			}
			String fileType = request.getParameter("type");
			String fileName = request.getParameter("name");
			String filePath = null;
			if (DocumentType.AUTHORITY.name().equalsIgnoreCase(fileType)) {
				filePath = uploadPath + File.separator + AUTHORITY_FOLDER
						+ File.separator + fileName;
			} else if (DocumentType.MARKETING.name().equalsIgnoreCase(fileType)) {
				filePath = uploadPath + File.separator + MARKETING_FOLDER
						+ File.separator + fileName;
			} else if (DocumentType.DISCLOSURES.name().equalsIgnoreCase(fileType)) {
				filePath = uploadPath + File.separator + DISCLOSURES_FOLDER
						+ File.separator + fileName;
			} else if (DocumentType.ACCREDITATION.name().equalsIgnoreCase(fileType)) {
				filePath = uploadPath + File.separator + ACCREDITATION_FOLDER
						+ File.separator + fileName;
			} else if (DocumentType.ADDITIONAL_INFO.name().equalsIgnoreCase(fileType)) {
				filePath = uploadPath + File.separator + ADD_INFO_FOLDER
						+ File.separator + fileName;
			}
			
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				if (null != filePath && isFilePresent(filePath)) {
					File file = new File(filePath);
					file.delete();
					response = "SUCCESS";
				} else {
					LOGGER.debug("File does not exists to delete at location : " +  SecurityUtil.sanitizeForLogging(filePath));
				}
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
			return response;
		} catch (Exception e) {
			LOGGER.error("Exception in deleteFiles : ", e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.DELETE_FILES_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
		    return PlanMgmtConstants.EXCEPTION +":"+ e.getMessage();
		}
	}


	private List<IssuerDocument> getIssuerDocuments(List<String> files, DocumentType documentType, Issuer issuer) {
		List<IssuerDocument> documents = new ArrayList<IssuerDocument>();
		for (String file : files) {
			IssuerDocument document = new IssuerDocument();
			document.setDocumentName(file);
			document.setDocumentType(documentType.name());
			document.setIssuer(issuer);
			documents.add(document);
			try {
				int userid = userService.getLoggedInUser().getId();
				document.setCreatedBy(userid);
				document.setLastUpdatedBy(userid);
			} catch (InvalidUserException e) {
				LOGGER.error("Invalid User");
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.ISSUER_DOCUMENTS_EXCEPTION.getCode(),
						null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
			}
		}
		return documents;
	}

	
	private boolean isFilePresent(String path) {
		boolean fileExists = false;
		try{
			boolean isValidPath = planMgmtUtils.isValidPath(path);
			if(isValidPath){
				File file = new File(path);
				fileExists = file.exists();
				LOGGER.debug("File " +  SecurityUtil.sanitizeForLogging(path) + " , Present : " + SecurityUtil.sanitizeForLogging(String.valueOf(fileExists)));
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in isFilePresent: ", ex);
		}
		return fileExists;
	}

	@RequestMapping(value = "/planmgmt/uploadData", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	@ResponseBody
	public String upload(Model model, HttpServletRequest request, MultipartHttpServletRequest mrequest, @RequestParam(value = "fileType", required = false) final String fileType, @RequestParam(value = "fileId", required = false) final String fileId) {
		String result = ",Uploading Failed,,";
		String errorMsg = null;
		Object exception = request.getAttribute(GhixPlatformConstants.MAXUPLOADSIZEEXCEEDED_EXCEPTION);
		try {
			if (exception != null
					&& FileUploadBase.SizeLimitExceededException.class.equals(exception.getClass())) {
				LOGGER.error("File Size exceeded.");
				result = "Error,,,";
				return result;
			}
			String fileName = (fileId != null && fileId.length() > 0 ? fileId
					: fileType);
			MultipartFile file = mrequest.getFile(fileName);
			String loadedFileName = null;
			String actualFileName = null;
			boolean isValidPath = planMgmtUtils.isValidPath(uploadPath);
			if(isValidPath){
				loadedFileName = FilenameUtils.getName(file.getOriginalFilename());
				fileName = FilenameUtils.getBaseName(file.getOriginalFilename())
						+ UNDERSCORE + TimeShifterUtil.currentTimeMillis() + DOT
						+ FilenameUtils.getExtension(file.getOriginalFilename());
				if ("certificates".equals(fileType)) {
					LOGGER.warn("uploading  Certificates" +  SecurityUtil.sanitizeForLogging(loadedFileName));
					actualFileName = issuerService.saveFile(uploadPath + File.separator + CERTI_FOLDER, file, fileName);
				} else if ("benefits".equals(fileType)) {
					LOGGER.warn("uploading  Benefits" +  SecurityUtil.sanitizeForLogging(loadedFileName));
					actualFileName = issuerService.saveFile(uploadPath + File.separator + BENEFITS_FOLDER, file, fileName);
				} else if ("brochure".equals(fileType)) {
					LOGGER.warn("uploading  brochure" +  SecurityUtil.sanitizeForLogging(loadedFileName));
					actualFileName = issuerService.saveFile(uploadPath + File.separator + BROCHURE_FOLDER, file, fileName);
				} else if ("rates".equals(fileType)) {
					LOGGER.warn("uploading  Rates" +  SecurityUtil.sanitizeForLogging(loadedFileName));
					actualFileName = issuerService.saveFile(uploadPath + File.separator + RATES_FOLDER, file, fileName);
				} else if ("authority".equals(fileType)) {
					LOGGER.warn("uploading  Authority " +  SecurityUtil.sanitizeForLogging(loadedFileName));
					actualFileName = issuerService.saveFile(uploadPath + File.separator + AUTHORITY_FOLDER, file, fileName);
				} else if ("marketingReq".equals(fileType)) {
					LOGGER.warn("uploading  Marketing Req" +  SecurityUtil.sanitizeForLogging(loadedFileName));
					actualFileName = issuerService.saveFile(uploadPath + File.separator + MARKETING_FOLDER, file, fileName);
				} else if ("disclosures".equals(fileType)) {
					LOGGER.warn("uploading  Disclosures " +  SecurityUtil.sanitizeForLogging(loadedFileName));
					actualFileName = issuerService.saveFile(uploadPath + File.separator + DISCLOSURES_FOLDER, file, fileName);
				} else if ("accreditation".equals(fileType)) {
					LOGGER.warn("uploading  Accreditation " +  SecurityUtil.sanitizeForLogging(loadedFileName));
					actualFileName = issuerService.saveFile(uploadPath + File.separator + ACCREDITATION_FOLDER, file, fileName);
				} else if ("additionalInfo".equals(fileType)) {
					LOGGER.warn("uploading  Additional info " + SecurityUtil.sanitizeForLogging(loadedFileName));
					actualFileName = issuerService.saveFile(uploadPath + File.separator + ADD_INFO_FOLDER, file, fileName);
				}
				if (actualFileName != null) {
					result = "," + loadedFileName + "," + actualFileName + ",";
				}
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
			return result;
		} catch (Exception e) {
			LOGGER.error("Exception in upload : ", e);
			GIMonitor gobj = giMonitorService.saveOrUpdateErrorLog(String.valueOf(PlanMgmtErrorCodes.ErrorCode.UPLOAD_EXCEPTION), new TSDate(), Exception.class.getName(),ExceptionUtils.getFullStackTrace(e), null);
		    GIErrorCode errorCodeObject = gobj.getErrorCode();
		    errorMsg = errorCodeObject.getMessage();
			return errorMsg;

		}
	}

	
	
	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/planmgmt/issuer/account/profile/accreditationdocument/view", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'VIEW_ACCREDITATION_DOCUMENTS')")
	public String viewAccreditationDocument(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		try {
		if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
			Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
			LOGGER.info("New Accreditation Document Page Info");
			if(null != issuerObj.getAccreditationExpDate()){
				String dateStr = DateUtil.dateToString(issuerObj.getAccreditationExpDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT_MMDDYYYY);
				String accExpDate = com.getinsured.hix.planmgmt.util.DateUtil.changeFormat(dateStr, PlanMgmtConstants.REQUIRED_DATE_FORMAT_MMDDYYYY, PlanMgmtConstants.REQUIRED_DATE_FORMAT_MMDDYYYY);
				model.addAttribute("accExpDate", accExpDate);
			}
			List<IssuerDocument> documents = issuerObj.getIssuerDocuments();
			//LOGGER.info("documentsSize: " + documents.size());

			List<HashMap<String, String>> authFilesList = new ArrayList<HashMap<String, String>>();
			List<HashMap<String, String>> marketingFilesList = new ArrayList<HashMap<String, String>>();
			List<HashMap<String, String>> disclosureFilesList = new ArrayList<HashMap<String, String>>();
			List<HashMap<String, String>> accreditationFilesList = new ArrayList<HashMap<String, String>>();
			List<HashMap<String, String>> addsuppFilesList = new ArrayList<HashMap<String, String>>();
			List<HashMap<String, String>> organizationFilesList = new ArrayList<HashMap<String, String>>();
			List<HashMap<String, String>> payPolPractFilesList = new ArrayList<HashMap<String, String>>();
			List<HashMap<String, String>> enrollDisDataFilesList = new ArrayList<HashMap<String, String>>();
			List<HashMap<String, String>> ratingPractFilesList = new ArrayList<HashMap<String, String>>();
			List<HashMap<String, String>> costSharPaymentFilesList = new ArrayList<HashMap<String, String>>();

			for (IssuerDocument document : documents) {
				HashMap<String, String> linkHM = new HashMap<String, String>();

				if (document.getDocumentName() != null
						&& !document.getDocumentName().isEmpty()) {
					if (document.getDocumentName() != null) {
						linkHM.put(PlanMgmtConstants.UPLOADED_FILE_LINK, document.getDocumentName());
					} else {
						linkHM.put(PlanMgmtConstants.UPLOADED_FILE_LINK, PlanMgmtConstants.EMPTY_STRING);
					}

					if (DocumentType.AUTHORITY.name().equals(document.getDocumentType())) {
						String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
						if (fileNameandLink != null) {
							String authorityDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
									+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, authorityDocumentURL);
						} else {
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
						}
						authFilesList.add(linkHM);

					} else if (DocumentType.MARKETING.name().equals(document.getDocumentType())) {
						String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
						if (fileNameandLink != null) {
							String marketingDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
									+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, marketingDocumentURL);
						} else {
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
						}
						marketingFilesList.add(linkHM);
					} else if (DocumentType.FINANCIAL_DISCLOSURE.name().equals(document.getDocumentType())) {
						String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
						if (fileNameandLink != null) {
							String disclosuresDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
									+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, disclosuresDocumentURL);
						} else {
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
						}
						disclosureFilesList.add(linkHM);

					} else if (DocumentType.ACCREDITATION.name().equals(document.getDocumentType())) {
						String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
						if (fileNameandLink != null) {
							String accreditationDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
									+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, accreditationDocumentURL);
						} else {
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
						}
						accreditationFilesList.add(linkHM);

					} else if (DocumentType.ADDITIONAL_SUPPORT.name().equals(document.getDocumentType())) {
						String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
						if (fileNameandLink != null) {
							String addsupportDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
									+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, addsupportDocumentURL);
						} else {
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
						}
						addsuppFilesList.add(linkHM);

					} else if (DocumentType.ORGANIZATION.name().equals(document.getDocumentType())) {
						String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
						if (fileNameandLink != null) {
							String organizationalDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
									+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, organizationalDocumentURL);
						} else {
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
						}
						organizationFilesList.add(linkHM);

					} else if (DocumentType.CLAIMS_PAY_POL.name().equals(document.getDocumentType())) {
						String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
						if (fileNameandLink != null) {
							String paypoliciesDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
									+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, paypoliciesDocumentURL);
						} else {
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
						}
						payPolPractFilesList.add(linkHM);

					} else if (DocumentType.ENROLLMENT_DISENROLLMENT.name().equals(document.getDocumentType())) {
						String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
						if (fileNameandLink != null) {
							String enrollDisenrollDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
									+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, enrollDisenrollDocumentURL);
						} else {
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
						}
						enrollDisDataFilesList.add(linkHM);

					} else if (DocumentType.RATING_PRACTICES.name().equals(document.getDocumentType())) {
						String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
						if (fileNameandLink != null) {
							String ratingpracticesDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
									+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, ratingpracticesDocumentURL);
						} else {
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
						}
						ratingPractFilesList.add(linkHM);

					} else if (DocumentType.COST_SHARING_PAYMENTS.name().equals(document.getDocumentType())) {
						String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
						if (fileNameandLink != null) {
							String costSharingDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
									+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, costSharingDocumentURL);
						} else {
							linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
						}
						costSharPaymentFilesList.add(linkHM);
					}
				}
			}

			model.addAttribute("accreditionEntity", issuerObj.getAccreditingEntity());
			model.addAttribute("issuerAccreditation", issuerObj.getIssuerAccreditation());
			//LOGGER.debug("finalfilelist: " + issuerService.removeDuplicateFileName(authFilesList));
			
			model.addAttribute("authFilesList", issuerService.removeDuplicateFileName(authFilesList));
			model.addAttribute("marketingFilesList", issuerService.removeDuplicateFileName(marketingFilesList));
			model.addAttribute("disclosureFilesList", issuerService.removeDuplicateFileName(disclosureFilesList));
			model.addAttribute("accreditationFilesList", issuerService.removeDuplicateFileName(accreditationFilesList));
			model.addAttribute("addsuppFilesList", issuerService.removeDuplicateFileName(addsuppFilesList));
			model.addAttribute("organizationFilesList", issuerService.removeDuplicateFileName(organizationFilesList));
			model.addAttribute("payPolPractFilesList", issuerService.removeDuplicateFileName(payPolPractFilesList));
			model.addAttribute("enrollDisDataFilesList", issuerService.removeDuplicateFileName(enrollDisDataFilesList));
			model.addAttribute("ratingPractFilesList", issuerService.removeDuplicateFileName(ratingPractFilesList));
			model.addAttribute("costSharPaymentFilesList", issuerService.removeDuplicateFileName(costSharPaymentFilesList));
			model.addAttribute(PlanMgmtConstants.TIMEZONE,TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));
			return "/planmgmt/issuer/account/profile/accreditationdocument/view";
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
		} catch (Exception e) {			
			throw new GIRuntimeException(
					PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEWACCREDITATIONDOCUMENT_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/planmgmt/issuer/account/profile/accreditationdocument/edit", method = { RequestMethod.GET })
	@PreAuthorize("hasPermission(#model, 'ADD_ACCREDITATION_DOCUMENTS')")
	public String newAccreditationDocument(Model model, HttpServletRequest request) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		try {
			issuerService.removeDataFromSession(request);
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is
														// issuer or issuer rep
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
	
					List<IssuerDocument> documents = issuerObj.getIssuerDocuments();
					LOGGER.debug("documentsSize: " + documents.size());
	
					List<HashMap<String, String>> authFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> marketingFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> disclosureFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> accreditationFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> addsuppFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> organizationFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> payPolPractFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> enrollDisDataFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> ratingPractFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> costSharPaymentFilesList = new ArrayList<HashMap<String, String>>();
	
					for (IssuerDocument document : documents) {
						HashMap<String, String> linkHM = new HashMap<String, String>();
						if (document.getDocumentName() != null
								&& !document.getDocumentName().isEmpty()) {
							if (document.getDocumentName() != null) {
								linkHM.put(PlanMgmtConstants.UPLOADED_FILE_LINK, document.getDocumentName());
							} else {
								linkHM.put(PlanMgmtConstants.UPLOADED_FILE_LINK, PlanMgmtConstants.EMPTY_STRING);
							}
	
							if (DocumentType.AUTHORITY.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String authorityDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
											+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, authorityDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								authFilesList.add(linkHM);
	
							} else if (DocumentType.MARKETING.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String marketingDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
											+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, marketingDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								marketingFilesList.add(linkHM);
							} else if (DocumentType.FINANCIAL_DISCLOSURE.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String disclosuresDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
											+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, disclosuresDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								disclosureFilesList.add(linkHM);
	
							} else if (DocumentType.ACCREDITATION.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String accreditationDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
											+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, accreditationDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								accreditationFilesList.add(linkHM);
	
							} else if (DocumentType.ADDITIONAL_SUPPORT.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String addsupportDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
											+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, addsupportDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								addsuppFilesList.add(linkHM);
	
							} else if (DocumentType.ORGANIZATION.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String organizationalDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
											+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, organizationalDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								organizationFilesList.add(linkHM);
	
							} else if (DocumentType.CLAIMS_PAY_POL.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String paypoliciesDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
											+ document.getFileName()  + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, paypoliciesDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								payPolPractFilesList.add(linkHM);
	
							} else if (DocumentType.ENROLLMENT_DISENROLLMENT.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String enrollDisenrollDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
											+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, enrollDisenrollDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								enrollDisDataFilesList.add(linkHM);
	
							} else if (DocumentType.RATING_PRACTICES.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String ratingpracticesDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
											+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, ratingpracticesDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								ratingPractFilesList.add(linkHM);
	
							} else if (DocumentType.COST_SHARING_PAYMENTS.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String costSharingDocumentURL = PlanMgmtConstants.ISSUER_FILE_DOWNLOAD_URL_START + ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getDocumentName()) + PlanMgmtConstants.FILE_DOWNLOAD_URL_FILE_NAME
											+ document.getFileName() + PlanMgmtConstants.FILE_DOWNLOAD_URL_TARGET + fileNameandLink + PlanMgmtConstants.FILE_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, costSharingDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								costSharPaymentFilesList.add(linkHM);
							}
						}
					}
					model.addAttribute("accreditionEntity", issuerObj.getAccreditingEntity());
					model.addAttribute("issuerAccreditation", issuerObj.getIssuerAccreditation());
	
					model.addAttribute("authFilesList", issuerService.removeDuplicateFileName(authFilesList));
					model.addAttribute("marketingFilesList", issuerService.removeDuplicateFileName(marketingFilesList));
					model.addAttribute("disclosureFilesList", issuerService.removeDuplicateFileName(disclosureFilesList));
					model.addAttribute("accreditationFilesList", issuerService.removeDuplicateFileName(accreditationFilesList));
					model.addAttribute("addsuppFilesList", issuerService.removeDuplicateFileName(addsuppFilesList));
					model.addAttribute("organizationFilesList", issuerService.removeDuplicateFileName(organizationFilesList));
					model.addAttribute("payPolPractFilesList", issuerService.removeDuplicateFileName(payPolPractFilesList));
					model.addAttribute("enrollDisDataFilesList", issuerService.removeDuplicateFileName(enrollDisDataFilesList));
					model.addAttribute("ratingPractFilesList", issuerService.removeDuplicateFileName(ratingPractFilesList));
					model.addAttribute("costSharPaymentFilesList", issuerService.removeDuplicateFileName(costSharPaymentFilesList));
					model.addAttribute("currDate", DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT));
					model.addAttribute("selectedIssuerAccreditation", issuerObj.getIssuerAccreditation());
					
				// check for first time access to edit page
//				if (null != request.getSession().getAttribute("isFirstEdit") && request.getSession().getAttribute("isFirstEdit").toString().equalsIgnoreCase(PlanMgmtConstants.TRUE_STRING)) {
					model.addAttribute("selectedAccreditationExpDate", issuerObj.getAccreditationExpDate());
					model.addAttribute("selectedAccreditionEntity", issuerObj.getAccreditingEntity());
					
					if (null != request.getSession().getAttribute("accreditingForFilePath")) {
						model.addAttribute("accreditingForFilePath", request.getSession().getAttribute("accreditingForFilePath").toString());
					}
//				}
	
					model.addAttribute("sysDate", new TSDate());	
					LOGGER.debug("New Accreditation Document Page Info");
					
				return "/planmgmt/issuer/account/profile/accreditationdocument/edit";
			}
	
			return PlanMgmtConstants.ISSUER_LOGIN_PATH;
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.NEWACCREDITATIONDOCUMENT_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	@RequestMapping(value = "/planmgmt/issuer/account/profile/viewhistory", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String viewIssuerHistory(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		try {
			Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			LOGGER.info("Issuer :: View Issuer History");
			List<Map<String, Object>> issuerHistoryData = issuerService.loadIssuerHistory(issuerObj.getId());
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			model.addAttribute("history", issuerHistoryData);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
			return "planmgmt/issuer/account/profile/viewhistory";
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEWISSUERHISTORY_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	@RequestMapping(value = "/planmgmt/issuer/certification/status", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String issuerCertificationStatus(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		try {
			Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			LOGGER.info("Issuer :: Certification Status Information Page");
			List<Map<String, Object>> certStatusHistory = issuerService.loadIssuerStatusHistory(issuerObj.getId());
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			model.addAttribute(PlanMgmtConstants.STATUS_HISTORY, certStatusHistory);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
			return "planmgmt/issuer/certification/status";
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.ISSUERCERTIFICATIONSTATUS_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	// controller for download documents from DMS, use for annoymous flow
	@RequestMapping(value = "/download/document", method = RequestMethod.GET)
	public ModelAndView downloadDocuments(@RequestParam(value = "documentId", required = false) String documentId, HttpServletResponse response) {
		String docECMId = documentId;	
		try {
			// if ECM id is encrypted format, decryprt ECM id before calling ECM service
			if(StringUtils.isNotBlank(docECMId)) {
				docECMId = ghixJasyptEncrytorUtil.decryptStringByJasypt(docECMId);
			}
		}catch (Exception e) {
			docECMId = documentId;
			/*LOGGER.error("Exception to decrypt document id", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOADDOCUMENTS_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);*/
		}	
		// if document ID (ecm id) exists in cache then don't call ecm service, downlaod from cache else call ecm service to download file content
		try {
			//bytes = ecmService.getContentDataById(docECMId);
			//Content content = ecmService.getContentById(docECMId);
			if(StringUtils.isNotBlank(docECMId)){
				Map<String, Object> documentInfoMap = issuerService.getDownloadDocumentInfo(docECMId);
				if(null != documentInfoMap.get(PlanMgmtConstants.DOCUMENT)) {
					byte[] bytes = (byte[]) documentInfoMap.get(PlanMgmtConstants.DOCUMENT);
					String fileName = (String) documentInfoMap.get(PlanMgmtConstants.DOCUMENT_NAME);
					String contentType = (String) documentInfoMap.get(PlanMgmtConstants.MIME_CONTENT_TYPE);
					if(null != response) {
						if("MN".equalsIgnoreCase(stateCode)) {
							response.setHeader("Content-Disposition", "inline;filename=" + fileName);
						}else{
							response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
						}
						response.setContentType(contentType);
						FileCopyUtils.copy(bytes, response.getOutputStream());
					} else {
						LOGGER.error("Response is null for getdocument call for document ", docECMId);
					}
				} else {
					LOGGER.error("Could not document contents with ID ", docECMId);
				}
			}
			return null;
		} catch (Exception e) {
			LOGGER.error("Exception in downloadDocuments", e.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOADDOCUMENTS_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}				
	}

	/**
	 * controller for download logo by issuerid for anonymous flow
	 */
	@RequestMapping(value = "/download/logobyid/{encIssuerId}", method = RequestMethod.GET, produces="image/png;image/jpeg; charset=utf-8")
	public void anonymousDownloadIssuerLogoById(@PathVariable("encIssuerId") String encIssuerId,HttpServletResponse response) {

		try {
			byte[] bytes = issuerService.getIssuerLogoById(encIssuerId);
			// Fixed HIX-77205 issue
			response.setContentType(planMgmtUtils.getContentType(bytes));
			FileCopyUtils.copy(bytes, response.getOutputStream());
		}
		catch (Exception e) {
			LOGGER.error("Exception to decrypt issuer id", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOADDOCUMENTS_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	/**
	 * controller for download logo by issuer hios id for anonymous flow
	 */
	@RequestMapping(value = "/download/logo/{hiosIssuerId}", method = RequestMethod.GET, produces="image/png;image/jpeg; charset=utf-8")
	public void getIssuerLogoByHiosId(@PathVariable("hiosIssuerId") String hiosIssuerId,HttpServletResponse response) {

		try {
			byte[] bytes = issuerService.getIssuerLogoByHiosId(hiosIssuerId);
			// Fixed HIX-77205 issue
			response.setContentType(planMgmtUtils.getContentType(bytes));
			FileCopyUtils.copy(bytes, response.getOutputStream());
		}
		catch (Exception e) {
			LOGGER.error("Exception to decrypt issuer id", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOADDOCUMENTS_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}	
	
		
	@RequestMapping(value = "/planmgmt/issuer/downLoadServiceArea/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_PLAN_DETAILS)
	public void downLoadServiceArea(@PathVariable("encPlanId") String encPlanId,HttpServletResponse response){
		// return null for suspended issuer representative
		if(issuerService.isIssuerRepSuspended()){
			return;
		}
		
		String planId = null;
		try {
			planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			AccountUser user = userService.getLoggedInUser();
			Issuer issuerObj = issuerService.getIssuer(user);
			Integer issuerId = issuerObj.getId();
			ServiceAreaDetails serviceAreaDetails = planMgmtService.getServiceAreaDetails(Integer.parseInt(planId),issuerId);
			planMgmtService.downLoadServiceArea(serviceAreaDetails, response);
			LOGGER.info("File downloaded successfully!!!");
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_QHP_DETAIL_GET.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}

	}
	
		
	/*@RequestMapping(value = "/issuer/document/filedownload", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public ModelAndView downloadDocumentsUploaded(@RequestParam(value = "documentId", required = false) String documentId, @RequestParam(value = "fileName", required = false) String fileName, HttpServletResponse response) throws IOException {
		try {
			String decDocumentId = ghixJasyptEncrytorUtil.decryptStringByJasypt(documentId);
			byte[] attachment = planMgmtService.getAttachment(decDocumentId);

			if (fileName.equals("PlanSBC_Document")) {
				response.setContentType("application/pdf");
			} else if (fileName.contains("_Document")
					&& !fileName.equals("PlanSBC_Document")) {
				response.setContentType("application/xml");
			} else {
				response.setContentType("application/octet-stream");
			}
			response.setContentLength(attachment.length);
			Content content = planMgmtService.getContent(decDocumentId);
			response.setHeader("Content-Disposition", "attachment;filename="+ content.getTitle());
			FileCopyUtils.copy(attachment, response.getOutputStream());
		} catch (Exception ex) {
			LOGGER.error("Exception Occured: ", ex.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.DOWNLOAD_DOCUMENTS_UPLOADED_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}

		return null;
	}*/
	
	@RequestMapping(value = "/setEffectiveDate/anonymous", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADD_REPRESENTATIVE')")
	@ResponseBody
	public String getEffectiveDateforAnonymous(HttpServletRequest request, Model model, RedirectAttributes redirectAttrs,
			@RequestParam(value = "effectiveStartDate", required = false) String effectiveStartDate, 
			@RequestParam(value = "issuerId", required = false) String issuerId,HttpServletResponse response) {
		String successString = null;
	 	try {
	 		if(effectiveStartDate != null && !(effectiveStartDate.isEmpty())){
	 			request.getSession().setAttribute("EFFECTIVE_START_DATE", effectiveStartDate);
	 			if(issuerId != null && !(issuerId.isEmpty())){
		 			request.getSession().setAttribute("ISSUER_ID", issuerId);
		 			if(LOGGER.isDebugEnabled()){
		 				LOGGER.debug("IssuerId for Anonymous : " +  SecurityUtil.sanitizeForLogging(issuerId));
		 			}
		 		}
	 			successString = "success";
	 			if(LOGGER.isDebugEnabled()){
	 				LOGGER.debug("EffectiveStartDate for Anonymous : " +  SecurityUtil.sanitizeForLogging(effectiveStartDate));
	 			}
	 			
	 		}else{
	 			successString = "fail";
	 		}
	 	} catch (Exception e) {
	 		successString = "fail";
	 		LOGGER.error("Exception Occurred in getEffectiveDateforAnonymous: ", e);
	 	}
	 		return successString;
	}
	
	@RequestMapping(value = "/issuer/application/documents/delete/{issuerId}", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	@ResponseBody 
	public String deleteAccDocuments(@PathVariable(value = "issuerId") String issuerId, @RequestParam(value = "documentId", required = false) String documentID) {
		try{
			String decDocumentId = ghixJasyptEncrytorUtil.decryptStringByJasypt(documentID);
			String response = issuerService.deleteAccreditationDocument(decDocumentId, Integer.parseInt(issuerId));
			if(!response.equals("")){
				return PlanMgmtConstants.SUCCESS;
			} else{
				return PlanMgmtConstants.ERROR;
			}	
		} catch (Exception ex){
			return PlanMgmtConstants.ERROR;
		}
		
	}
	
	@RequestMapping(value = "/issuer/planmgmt/dowloadbenefits/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public ModelAndView downloadPlansBenefits(HttpServletResponse response, @PathVariable("encPlanId") String encPlanId) {
		// return null for suspended issuer representative
		if(issuerService.isIssuerRepSuspended()){
			return null;
		}
		
		FileInputStream fis = null;
		FileOutputStream outFile = null;
		
		try {

			String planId = null;

			try { 
				planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			}
			catch (Exception ex) {
				LOGGER.error("Unable to decript following Plan ID: " + encPlanId);
			}

			Plan plan =	null;
			if (org.apache.commons.lang3.StringUtils.isNumeric(planId)) {
				plan = planMgmtService.getPlan(Integer.valueOf(planId));
			}

			if (null == plan) {
				LOGGER.error("Invalid Plan ID: " + planId);
				return null;
			}
			else if (issuerService.getIssuerId() != plan.getIssuer().getId()) {
				LOGGER.error("Benefits Plan is not belong to Logged in Issuer Representative.");
				return null;
			}

			List<String[]> planBenefitsList = null;
			int rows = 0;

			if (Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(plan.getInsuranceType())) {
				PlanHealth planHealth = planMgmtService.getPlanHealth(plan.getPlanHealth().getId());
				List<PlanHealthBenefit> plansData = planMgmtService.getPlanHelthBenefit(planHealth.getId());
				List<PlanHealthCost> planHealthCostData = planMgmtService.getPlanHealthCost(planHealth.getId());
				rows = planHealthCostData.size();
				String constname;
				String name;
				Map<String, String> healthBenefitMap = new HashMap<String, String>();
				for(PlanmgmtConfiguration.PlanmgmtConfigurationEnum pcE : PlanmgmtConfiguration.PlanmgmtConfigurationEnum.values()){
					 constname = pcE.name().endsWith(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY)?pcE.name().replace(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY,""):pcE.name();name = pcE.getValue();
					if(name.contains(PlanMgmtConstants.PLANMGMT_HEALTHBENEFITS)){
						healthBenefitMap.put(constname,DynamicPropertiesUtil.getPropertyValue(name));
					}
				}
				planBenefitsList = PlanHealthMapper.convertObjectListToStringListFromMap(plansData,healthBenefitMap,plan,planHealthCostData);
			}
	
			if (Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(plan.getInsuranceType())) {
				PlanDental planDental = planMgmtService.getPlanDental(plan.getPlanDental().getId());
				List<PlanDentalBenefit> plansData = planMgmtService.getPlanDentalBenefit(planDental.getId());
				List<PlanDentalCost> planDentalCostData = planMgmtService.getPlanDentalCost(planDental.getId());
				rows = planDentalCostData.size();
				String constname;
				String name;
				Map<String, String> dentalBenefitMap = new HashMap<String, String>();
				
				for(PlanmgmtConfiguration.PlanmgmtConfigurationEnum pcE : PlanmgmtConfiguration.PlanmgmtConfigurationEnum.values()){
					 constname = pcE.name().endsWith(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY)?pcE.name().replace(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY,""):pcE.name();name = pcE.getValue();
					if(name.contains(PlanMgmtConstants.PLANMGMT_DENTALBENEFITS)){
						dentalBenefitMap.put(constname,DynamicPropertiesUtil.getPropertyValue(name));
					}
				}
				planBenefitsList = PlanHealthMapper.convertsQDPBenefitFromMap(plansData,dentalBenefitMap,plan,planDentalCostData);
			}
	
			if (planBenefitsList == null) {
				planBenefitsList = PlanHealthMapper.getFileHeader();
			}
			
			String fileName = plan.getIssuerPlanNumber() +".xls";
			String filePath = uploadPath + File.separator + fileName;
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				HSSFWorkbook workBook = ExcelGenerator.createBenefitsExcelSheet(planBenefitsList, rows);//createBenefitsExcelSheet(planBenefitsList);
				outFile = new FileOutputStream(filePath);
				workBook.write(outFile);
				outFile.close();
				
				response.setHeader("Content-Disposition", "attachment;filename="+ fileName);
				response.setContentType("application/octet-stream");
				File file = new File(filePath);
				fis = new FileInputStream(file);
				FileCopyUtils.copy(fis, response.getOutputStream());
				if(file.exists()){
					file.delete();
				}
				fis.close();
				LOGGER.info("File downloaded successfully!!!");
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOAD_PLAN_BENEFIT, null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}finally{
			 IOUtils.closeQuietly(fis);
			 IOUtils.closeQuietly(outFile);
		}
		return null;
	}
	
	@RequestMapping(value = "/issuer/planmgmt/downloadrates/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public ModelAndView downloadRate(Model model, @PathVariable("encPlanId") String encPlanId, @RequestParam("effective_start_date") String effectiveStartDate, @RequestParam("effective_end_date") String effectiveEndDate, HttpServletResponse response) {
		// return null for suspended issuer representative
		if(issuerService.isIssuerRepSuspended()){
			return null;
		}
		
		FileInputStream fis = null;
		FileOutputStream outFile = null;
		
		try {

			String planId = null;

			try {
				planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			}
			catch (Exception ex) {
				LOGGER.error("Unable to decript following Plan ID: " + encPlanId);
			}

			Plan plan =	null;
			if (org.apache.commons.lang3.StringUtils.isNumeric(planId)) {
				plan = planMgmtService.getPlan(Integer.valueOf(planId));
			}

			if (null == plan) {
				LOGGER.error("Invalid Plan ID: " + planId);
				return null;
			}
			else if (issuerService.getIssuerId() != plan.getIssuer().getId()) {
				LOGGER.error("Benefits Plan is not belong to Logged in Issuer Representative.");
				return null;
			}
			List<Object[]> rateList = null;
			rateList = planMgmtService.downloadPlanRate(Integer.valueOf(planId), effectiveStartDate, effectiveEndDate);
			String[] effectiveStartDateArr = effectiveStartDate.split(PlanMgmtConstants.SEPARATORAT);
			String[] startDatArr = effectiveStartDateArr[0].split(PlanMgmtConstants.HIGHFEN);
			String[] effectiveEndDateArr = effectiveEndDate.split(PlanMgmtConstants.SEPARATORAT);
			String[] endDatArr = effectiveEndDateArr[0].split(PlanMgmtConstants.HIGHFEN);
			String fileName = plan.getHiosProductId() + PlanMgmtConstants.PLANRATES +".xls";
			String filePath = uploadPath + File.separator + fileName;
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				HSSFWorkbook workBook = ExcelGenerator.createRateExcelSheet(rateList, plan,startDatArr,endDatArr);
				outFile = new FileOutputStream(filePath);
				workBook.write(outFile);
				outFile.close();
				response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
				response.setContentType("application/octet-stream");
				File file = new File(filePath);
				fis =  new FileInputStream(file);
				FileCopyUtils.copy(fis, response.getOutputStream());
				if(file.exists()){
					file.delete();
				}
				fis.close();
				LOGGER.info("Rates file downloaded successfully");
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOAD_PLAN_RATE, null,
					ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}finally{
			 IOUtils.closeQuietly(fis);
			 IOUtils.closeQuietly(outFile);
		}	
		return null;
	}
	

	@RequestMapping(value = "/planmgmt/issuer/crossWalkStatus", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String crossWalkPlan(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		List<SerffDocumentPlanPOJO> serffDocList = null;
		String encryptedECMcDocID = null;
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("crossWalkPlan :: View crosswalk status");
		}
		try{
			AccountUser currentUser = userService.getLoggedInUser();
			if (userService.hasUserRole(currentUser, RoleService.ISSUER_REP_ROLE)){
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				if(null != issuerObj){
					serffDocList = serffService.getSerffDocumentByIssuerId(String.valueOf(issuerObj.getId()), PlanMgmtConstants.TRANSFER_CROSSWALK);
					if(!CollectionUtils.isEmpty(serffDocList)){
						for(SerffDocumentPlanPOJO serffDoc : serffDocList ){
							encryptedECMcDocID = ghixJasyptEncrytorUtil.encryptStringByJasypt(serffDoc.getEcmDocId());
							serffDoc.setEcmDocId(encryptedECMcDocID);
						}
					}
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				}
			}
			model.addAttribute("serffDocList" , serffDocList);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
		}catch(Exception ex){
			LOGGER.error("Exception occured in crossWalkPlan: ", ex);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		return "planmgmt/issuer/crossWalkStatus";
	}

	/**
	 * Display Network Transparency Excel data.
	 */
	@RequestMapping(value = "/planmgmt/issuer/displayNetworkTransparencyData", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String displayNetworkTransparencyData(Model model) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		//LOGGER.debug("Display Network Transparency Excel data Start");
		List<SerffDocumentPlanPOJO> serffDocList = null;

		try {

			AccountUser currentUser = userService.getLoggedInUser();

			if (userService.hasUserRole(currentUser, RoleService.ISSUER_REP_ROLE)) {

				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());

				if (null != issuerObj) {
					serffDocList = serffService.getSerffDocumentByIssuerId(String.valueOf(issuerObj.getId()), PlanMgmtConstants.TRANSFER_NW_TRANS);

					String encryptedECMcDocID = null;

					if (!CollectionUtils.isEmpty(serffDocList)) {

						for (SerffDocumentPlanPOJO serffDoc : serffDocList) {
							encryptedECMcDocID = ghixJasyptEncrytorUtil.encryptStringByJasypt(serffDoc.getEcmDocId());
							serffDoc.setEcmDocId(encryptedECMcDocID);
						}
					}
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				}
			}
			model.addAttribute("serffDocList" , serffDocList);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
		}
		catch (Exception ex) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.FAILED_TO_POPULATE_NETWORK_TRANS_DATA.getCode(), ex, null, Severity.HIGH);
		}
		finally {
			LOGGER.debug("Display Network Transparency Excel data End");
		}
		return "planmgmt/issuer/displayNetworkTransparencyData";
	}
	
	
	// controller for suspended user 
	@RequestMapping(value = "/planmgmt/issuer/suspend", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String showSuspendScreen() {
		
		return "planmgmt/issuer/showsuspendscreen";
	}	
	
	
	@RequestMapping(value = "/planmgmt/issuer/downLoadFormularyDrug/{encApplicableYear}/{encIssuerHiosId}/{encFormularyId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'VIEW_PLAN_DETAILS')")
	public void downLoadFormularyDrug(@PathVariable("encApplicableYear") String encApplicableYear, 
			@PathVariable("encIssuerHiosId") String encIssuerHiosId, 
			@PathVariable("encFormularyId") String encFormularyId, HttpServletResponse response){
		LOGGER.info("Download Formulary Drug List Started");
		try {
			if(StringUtils.isEmpty(encApplicableYear) || StringUtils.isEmpty(encFormularyId) || StringUtils.isEmpty(encIssuerHiosId)){
				LOGGER.error("Error While Downloading Formulary Drug List file..!!!");
			}else{
				String decryptedApplicableYear = ghixJasyptEncrytorUtil.decryptStringByJasypt(encApplicableYear);
				String decryptedIssuerHiosId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerHiosId);
				String decryptedFormularyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encFormularyId);
				FormularyDrugListResponseDTO formularyDrugDetails = planMgmtService.getFormularyDrugDetails(decryptedApplicableYear, decryptedIssuerHiosId, decryptedFormularyId);
				planMgmtService.downLoadFormularyDrug(formularyDrugDetails, response);
			}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_QHP_DETAIL_GET.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}
	
}
