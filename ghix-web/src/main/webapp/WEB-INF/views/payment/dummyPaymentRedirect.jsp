<style type="text/css">
.paymentTable
{
	width:60%; 
	text-align:center; 
	margin-left:auto; 
	margin-right:auto;
}
.paymentTable td
{
	padding:5px;
}
</style>
<table class="margin20-t paymentTable"  border="1">	
	<tr>
		<td style="text-align:right">Market Indicator </td>
		<td style="text-align:left"> Individual</td>
	</tr>
	<tr>
		<td style="text-align:right">Enrollment Id </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.enrollmentId}</td>
	</tr>
	<tr>
		<td style="text-align:right">Subscriber Identifier </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.exchgSubscriberIdentifier}</td>
	</tr>
	<tr>
		<td style="text-align:right">Assigned QHP Identifier </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.cmsPlanId}</td>
	</tr>
	<tr>
		<td style="text-align:right">Payment Transaction ID </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.paymentTxnId}</td>
	</tr>
	<tr>
		<td style="text-align:right">Total Amount Owed </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.netPremiumAmt}</td>
	</tr>
	<tr>
		<td style="text-align:right">Premium Amount Total </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.grossPremiumAmt}</td>
	</tr>
	<tr>
		<td style="text-align:right">APTC Amount </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.aptcAmt}</td>
	</tr>
	<tr>
		<td  style="text-align:right">Proposed Coverage Effective Date </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.benefitEffectiveDate}</td>
	</tr>
	<tr>
		<td style="text-align:right">First Name </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.firstName}</td>
	</tr>
	<tr>
		<td style="text-align:right">Middle Name </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.middleName}</td>
	</tr>
	<tr>
		<td style="text-align:right">Last Name </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.lastName}</td>
	</tr>
	<tr>
		<td style="text-align:right">Contact Email Address </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.contactEmail}</td>
	</tr>
	<tr>
		<td style="text-align:right">Enrollee Information </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.enrolleeNames}</td>
	</tr>
	<tr>
		<td style="text-align:right">Street Name 1 </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.addressLine1}</td>
	</tr>
	<tr>
		<td style="text-align:right">Street Name 2 </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.addressLine2}</td>
	</tr>
	<tr>
		<td style="text-align:right">City Name </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.city}</td>
	</tr>
	<tr>
		<td style="text-align:right">State </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.state}</td>
	</tr>
	<tr>
		<td style="text-align:right">Zip Code </td>
		<td style="text-align:left"> ${enrollmentPaymentDTO.zip}</td>
	</tr>
</table>
<div class="margin10-t" style=" text-align: center;">
	<a href="../finance/downloadSAMLPayloadXml">Click here to Download the SAML Pay load</a>
</div>
