<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/ng-template" id="contactUs">
<!-- TODO: Align radio buttons with text -->
<div ng-controller="contactUsCtrl">
  <div class="header">
    <h4><spring:message code="indportal.portalhome.contactus" javaScriptEscape="true"/></h4>
  </div>
  <div class="well">
  <div class="gutter10">
  <div ng-hide="postResult">
    
  <form class="form-horizontal" id="contactUsForm" ng-submit='submitForm' name=contactUsForm>
    <div class="alert alert-info margin30-b">
      <p><spring:message code="indportal.contactus.yourhealthidahoiscommited" javaScriptEscape="true"/></p>
      <p><spring:message code="indportal.contactus.usethisform" javaScriptEscape="true"/></p>
    </div>

    <fieldset class="control-group">
      <legend class="control-label" for="typeOfRequest"><spring:message code="indportal.contactus.selecttypeofmessage" javaScriptEscape="true"/>:</legend>
      <div class="controls">
				<div>
          <input type="radio" name="typeOfRequest" id="requestSuggestions" value="suggestions" ng-model="formData.typeOfRequest" ng-click="clearReason()"checked required>
          <label class="radio contactUs-form"><spring:message code="indportal.contactus.suggestions" javaScriptEscape="true"/></label>
      	</div>
      	<div>
          <input type="radio" name="typeOfRequest" id="requestTechnicalIssue" value="technicalIssue" ng-model="formData.typeOfRequest" required ng-click="clearReason()">
					<label class="radio contactUs-form"><spring:message code="indportal.contactus.technicalissue" javaScriptEscape="true"/></label>
      	</div>
      	<div>
          <input type="radio" name="typeOfRequest" id="requestComplaints" value="complaints" ng-model="formData.typeOfRequest"  required ng-click="clearReason(false)" required>
      		<label class="radio contactUs-form"><spring:message code="indportal.contactus.complaints" javaScriptEscape="true"/></label>
      	</div>
    	</div>
    </fieldset>

<!-- Complaints -->
  <div class="contactDetails">
    <div class="reason" ng-if="formData.typeOfRequest=='complaints'">
      <fieldset class="control-group">
      	<legend class="control-label" for="reason"><spring:message code="indportal.contactus.classification" javaScriptEscape="true"/></legend>
         <div class="controls">
         	<table>
		  			<tr>
							<td>
	              	<input type="radio" name="reason" id="reasonBroker" value="broker" ng-model="formData.reason" required>
									<label class="radio inline" for="reasonBroker"><spring:message code="indportal.contactus.broker" javaScriptEscape="true"/></label>
							</td>
							<td>
		              <input type="radio" name="reason" id="reasonCarrier" value="carrier" ng-model="formData.reason" required>
		              <label class="radio inline" for="reasonCarrier"><spring:message code="indportal.contactus.carrier" javaScriptEscape="true"/></label>
							</td>
						</tr>
						<tr>
							<td>
								<input type="radio" name="reason" id="reasonExchangeStaff" value="exchangeStaff" ng-model="formData.reason" required>
							  <label class="radio inline" for="reasonExchangeStaff"><spring:message code="indportal.contactus.exchangestaff" javaScriptEscape="true"/></label>
							</td>
							<td>		          
	              <input type="radio" name="reason" id="reasonOtherStaff" value="otherStaff" ng-model="formData.reason" required>
	              <label class="radio inline" for="reasonOtherStaff"><spring:message code="indportal.contactus.otherstaff" javaScriptEscape="true"/></label>
							</td>
						</tr>
          </table>
        </div>
      </fieldset>
    </div>
      
      <div class="enter-details">
        <div class="control-group">
          <label class="control-label" for="requestDescription"><spring:message code="indportal.contactus.pleaseenterdetails" javaScriptEscape="true"/>:</label>
          <div class="controls">
              <!-- TODO: Put in QUICKFIX -->
              <textarea rows="5" name="requestDescription" id="requestDescription" placeholder="<spring:message code='indportal.contactus.pleaseenterdetails' javaScriptEscape='true'/>" ng-model="formData.requestDescription" style="width:400px !important"></textarea>
          </div>
        </div>
              
      </div>
      
      

  <div ng-repeat="file in filesUploaded" ng-hide="true"> 
    {{file}}
  </div>
      
    </div><!-- .requestOption3 ENDS -->
</form>

<div class="margin10-b">
  <div class="header"><h4 class="uploadheader"><spring:message code="indportal.contactus.youcanchosetoupload" javaScriptEscape="true"/></h4></div>
  <div class="gutter10"><spring:message code="indportal.contactus.eachfileyouupload" javaScriptEscape="true"/></div>
</div>

<div class="controls">
    
    <button class="btn btn-small" trigger-add-file alt="upload" for="fileUpload"> <spring:message code="indportal.contactus.choosefile" javaScriptEscape="true"/> </button>
    <input type="file" class="custom-file-input"  id="fileUpload" name="fileUpload" file-model="files" ng-file-select="onFileSelect($files, $index, $event)" ng-hide="true">
    <button class="btn btn-small" ng-disabled="!filesSelected[0] || filesSelected[0].uploaded" ng-click="uploadFile(0)" ><i class="icon-upload-alt"></i> <spring:message code="indportal.contactus.upload" javaScriptEscape="true"/> </button>
    <button class="btn btn-small" ng-disabled="!filesSelected[0]" alt="cancelUpload" ng-click="cancelFile(0)"><i class="icon-remove"></i> <spring:message code="indportal.contactus.cancel" javaScriptEscape="true"/> </button>  
    <span>{{filesSelected[0].name}}<span>

  <div ng-repeat="file in filesSelected" ng-if="!$first"> 
    <button class="btn btn-small" trigger-add-file alt="upload" for="fileUpload[$index]"> <spring:message code="indportal.contactus.choosefile" javaScriptEscape="true"/> </button>
    <input type="file" class="custom-file-input" id="fileUpload[$index]" ng-file-select="onFileSelect($files, $index, $event)" ng-hide="true" ng-model="file[$index]">
    <button class="btn btn-small" ng-disabled="filesSelected[$index]==$index || filesSelected[$index].uploaded" ng-click="uploadFile($index)"><i class="icon-upload-alt"></i> <spring:message code="indportal.contactus.upload" javaScriptEscape="true"/> </button>
    <button class="btn btn-small" ng-disabled="filesSelected[$index]==$index" alt="cancelUpload" ng-click="cancelFile($index)"><i class="icon-remove"></i> <spring:message code="indportal.contactus.cancel" javaScriptEscape="true"/> </button>
    <span>{{file.name}}<span>
    <br>
  </div>
  <br>
  
  <button type="file" class="btn btn-primary btn-small addDocument" alt="addMoreDocument" ng-click="addFile()" ng-hide="filesSelected.length==0"><spring:message code="indportal.contactus.addanotherdocument" javaScriptEscape="true"/></button>
</div>

<!--     <div class="control-group">
    <label class="control-label" for=""><spring:message code="indportal.contactus.esignature" javaScriptEscape="true"/>:</label>
      <div class="controls">
        <p><spring:message code="indportal.contactus.iamsigningthissection" javaScriptEscape="true"/></p>
        <input type="text" placeholder="Your Signature" class="input-large" ng-model="formData.signature">
      </div>
  </div> -->

  <div class="margin20-t">
    <div class="margin10-b">
      <div class="margin10-b">
        <div class="header"><h4><spring:message code="indportal.contactus.whathappensnext1" javaScriptEscape="true"/></h4></div>
        <div class="gutter10"><spring:message code="indportal.contactus.whathappensnext2" javaScriptEscape="true"/></div>
        <!--<div class="gutter10 alert alert-info"><spring:message code="indportal.contactus.individualsthathaveqs" javaScriptEscape="true"/></div>-->
		
      </div>
    </div>

    <div class="form-actions margin30-t">
      <button type="reset" class="btn redirectToDashboard pull-left" ng-click="goTo('/')"><spring:message code="indportal.contactus.cancel" javaScriptEscape="true"/></button>
      <button type="submit" class="btn btn-primary pull-right" id="contactUsFormSubmit" ng-click="submitForm()" ng-disabled="contactUsForm.$invalid"><spring:message code="indportal.contactus.submit" javaScriptEscape="true"/></button>
    </div>
  </div>

<div spinning-loader="loader"></div>

  </div>


<!-- GOOD -->
<div id="contactRequestAckSuccess" aria-labelledby="contactRequestAckSuccessLabel" ng-if="postResult=='postsuccess'">
    <h3 id="contactRequestAckSuccessLabel"><spring:message code="indportal.contactus.submittedsuccessfully" javaScriptEscape="true"/></h3>
    <p><spring:message code="indportal.contactus.yourtickethasbeengenerated" javaScriptEscape="true"/></p>
    <a href="/hix/indportal" class="btn btn-primary"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></a>
</div>

<!-- BAD -->
<div id="contactRequestAckFailure" aria-labelledby="contactRequestAckFailureLabel" ng-if="postResult=='postfailure'">
    <h3 id="contactRequestAckFailureLabel"><spring:message code="indportal.contactus.submissionfailed" javaScriptEscape="true"/></h3>
    <p><spring:message code="indportal.contactus.pleasetryagain" javaScriptEscape="true"/></p>
    <a href="/hix/indportal" class="btn btn-primary"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></a>
</div>

		<!-- Modal-->
		<div ng-show= "openDialog">
		      <div modal-show="openDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedUploadModal" aria-hidden="true">
		      <div class="modal-dialog">
		        <div class="modal-content">
		          <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		            <h3 id="failedUploadModal"><spring:message code="indportal.contactus.upload" javaScriptEscape="true"/></h3>
		          </div>
		          <div class="modal-body">
		          <p><spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p>
		          </div>
		          <div class="modal-footer">
		            <button class="btn pull-left" data-dismiss="modal""><spring:message code="indportal.eligibilitysummary.close" javaScriptEscape="true"/></button>
		            <button class="btn btn-primary pull-right" data-dismiss="modal""><spring:message code="indportal.portalhome.yes" javaScriptEscape="true"/></button>
		          </div>
		        </div><!-- /.modal-content-->
		      </div> <!-- /.modal-dialog-->
		     </div>
		</div>
		<!-- Modal-->
		
		<!-- Oversized File Modal-->
		<div ng-show= "oversizedFileD">
		      <div modal-show="oversizedFileD" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="oversizedFileModal" aria-hidden="true">
		      <div class="modal-dialog">
		        <div class="modal-content">
		          <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		            <h3 id="oversizedFileModal"><spring:message code="indportal.contactus.oversized" javaScriptEscape="true"/></h3>
		          </div>
		          <div class="modal-body">
		          <p><spring:message code="indportal.contactus.oversizedcontent" javaScriptEscape="true"/></p>
		          </div>
		          <div class="modal-footer">
		            <button class="btn pull-left" data-dismiss="modal""><spring:message code="indportal.eligibilitysummary.close" javaScriptEscape="true"/></button>
		          </div>
		        </div><!-- /.modal-content-->
		      </div> <!-- /.modal-dialog-->
		     </div>
		</div>
		<!-- Oversized File Modal-->
		
			<!-- Wrong File Modal-->
		<div ng-show= "wrongFileTypeDialog">
		      <div modal-show="wrongFileTypeDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="wrongFileTypeModal" aria-hidden="true">
		      <div class="modal-dialog">
		        <div class="modal-content">
		          <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		            <h3 id="wrongFileTypeModal"><spring:message code="indportal.contactus.wrongfiletype" javaScriptEscape="true"/></h3>
		          </div>
		          <div class="modal-body">
		          <p><spring:message code="indportal.contactus.wrongfiletypecontent" javaScriptEscape="true"/></p>
		          </div>
		          <div class="modal-footer">
		            <button class="btn pull-left" data-dismiss="modal""><spring:message code="indportal.eligibilitysummary.close" javaScriptEscape="true"/></button>
		          </div>
		        </div><!-- /.modal-content-->
		      </div> <!-- /.modal-dialog-->
		     </div>
		</div>
		<!-- Wrong File Modal-->


	</div>
</div>


</script>

  
