<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<div class="gutter10">
<div class="row-fluid">
	<ul class="breadcrumb txt-left">
		<li><a href="<c:url value="/shop/employer/manage/list" />" > &lt; Back</a> <span class="divider">|</span></li>
		<li><a href="<c:url value="/shop/employer/" />">Employees</a> <span class="divider">/</span></li>
		<li><a href="<c:url value="/shop/employer/manage/list" />">Manage List</a> <span class="divider">/</span></li>
		<li class="active">${employee.name}</li>
	</ul>
	<h3 id="skip">${employee.name}
		<small>
				<c:set var="status" value="${fn:toUpperCase(fn:substring(employee.status, 0, 1))}${fn:toLowerCase(fn:substring(employee.status, 1, -1))}" />
				${fn:replace(status,'_',' ')}
		</small>
	</h3>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="accordion graysidebar" id="accordion2">
				<div class="accordion-group">
					<div class="accordion-heading graydrkbg">
						<a class="accordion-toggle" data-toggle="collapse"
							data-parent="#accordion2" href="#collapseOne"> <spring:message  code='label.aboutthisemp'/></a>
					</div>
					<div id="collapseOne" class="accordion-body collapse in">
						<div class="accordion-inner">
							<ul class="nav nav-list">
								<li class="active"><a href="<c:url value="/shop/employer/manage/employeeinfo/${employee.id}?appId=${empappID}" />">Employee Information</a></li>
								<li><a	href="<c:url value="/shop/employer/manage/dependents/${employee.id}?appId=${empappID}" /> ">Dependent Information	(${dependentCount})</a></li>
								<li><a href="<c:url value="/shop/employer/manage/enrollment/${employee.id}?appId=${empappID}" />">Employee Coverage</a></li>
							<%--	<li><a href="<c:url value="/shop/employer/manage/notifications/${employee.id}" />">Notification History</a></li> --%>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<h4 class="graydrkbg">
				<i class="icon-cog icon-white"></i> <spring:message  code='label.actions'/>
			</h4>
			<ul class="nav nav-list">
			<%-- <c:if test="${(employee.status != 'ENROLLED') || (employee.status != 'DELETED')}">
				<li><a
					href="<c:url value="/shop/employer/manage/editemployee?id=${employee.id}" />">
						<i class="icon-pencil"></i> <spring:message  code='label.edit'/>
				</a></li>
			</c:if> --%>
			<c:if test="${(employee.status != 'DELETED')}">
				<li><a href="<c:url value="/shop/employer/manage/editemployee?id=${employee.id}&appId=${empappID}" />"><i class="icon-pencil"></i> <spring:message  code='label.edit'/></a></li>
			</c:if>
				<!-- <li><a href="#"> <i class="icon-folder-open"></i> Enroll
						For
				</a></li> -->
				<%-- <li><a href="#waive-coverage" data-toggle="modal"><i
						class="icon-lock"></i> <spring:message  code='label.waivedcov'/></a></li> --%>
				<!-- modal -->
			<%-- 	<div id="waive-coverage" class="modal hide fade" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">�</button>
						<h3 id="myModalLabel"><spring:message  code='label.waivedcov'/></h3>
					</div>
					<div class="modal-body">
						<p><spring:message  code='label.reasonempwaiving'/></p>
						<div class="control-group">
							<label class="radio"> <input type="radio"
								id="waive_reason" name="waive_reason"
								value="Covered by another employer's plan"> <spring:message  code='label.reasonempanotplan'/> </label>
						</div>
						<div class="control-group">
							<label class="radio"> <input type="radio"
								id="waive_reason" name="waive_reason"
								value="Covered by Individual Health Exchange plan">
								<spring:message  code='label.reasonindvhealth'/>
							</label>
						</div>
						<div class="control-group">
							<label class="radio"> <input type="radio"
								id="waive_reason" name="waive_reason"
								value="Covered by government plan (e.g. Medicare)">
								<spring:message  code='label.reasongovplan'/>
							</label>
						</div>
						<div class="control-group">
							<label class="radio"> <input type="radio"
								id="waive_reason" name="waive_reason"
								value="Covered as dependent on someone else's plan">
								<spring:message  code='label.reasonelsesplan'/>
							</label>
						</div>
						<div class="control-group">
							<label class="radio"> <input type="radio"
								id="waive_reason" name="waive_reason"
								value="Not covered by another plan and do not wish to enroll">
								<spring:message  code='label.reasonanothercovplan'/>
							</label>
						</div>
						<div class="control-group">
							<label class="radio" for="waive_reason_other"> <input type="radio"
								id="waive_reason" name="waive_reason" value="other">
								<spring:message  code='label.reason4'/>
							</label>
							<textarea name="waive_reason_other" id="waive_reason_other"
								class="span5" rows="3" maxlength="255"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message  code='label.cancel'/></button>
						<button class="btn btn-primary" id="waivecoverageButton"
							name="waivecoverageButton"
							onclick="updateWaiveCoverage('${employee.id}')"><spring:message  code='label.ok'/></button>
					</div>
				</div> --%>
				<!-- /modal-->
			</ul>
		</div>
		<!--sidebar-->
		<div class="span9" id="rightpanel">
			<div class="header">
				<h4><spring:message  code='label.employeesummery'/></h4>
			</div>
			<div class="gutter10">
				<table class="table table-border-none">
					<tbody>
						<tr>
							<td class="span5"><spring:message  code='label.emprname'/></td>
							<td><strong>${employee.name}</strong></td>
						</tr>
						<tr>
							<td><spring:message  code='label.worksiteAddress'/></td>
							<td><strong>${employee.employerLocation.location.address1},${employee.employerLocation.location.city},${employee.employerLocation.location.state},${employee.employerLocation.location.zip}</strong></td>
						</tr>
					 	<%-- <tr>
							<td><spring:message  code='label.estAnnualWages'/></td>
							<td><strong>&#36;<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${employee.yearlyIncome}" /></strong></td>
						</tr>  --%>
						<%-- <tr>
							<td><spring:message  code='label.hrsPerWeek'/></td>
							
							<c:if test = "${not empty employee.hrsPerWeek }"><td><strong>${employee.hrsPerWeek}</strong></td></c:if>
							<c:if test = "${empty employee.hrsPerWeek }"><td><weak><spring:message code="label.notApplicable"/></weak></td></c:if>
							<td><strong>${employee.hrsPerWeek}</strong></td>
						</tr> --%>
						<tr>
						<c:if test="${showHireDate=='Y'}">
							<td><spring:message  code='label.covEligibilityDt'/></td>
							<td><strong><fmt:formatDate value="${employee.employmentDate}" pattern="MM/dd/yyyy"/></strong></td>
						</c:if>
						</tr>
					</tbody>
				</table>
				<h4 class="lightgray"><spring:message  code='label.personalinfo'/></h4>
				<table class="table table-border-none">
					<tbody>
						<tr>
							<td class="span5"><spring:message  code='label.dob'/></td>
							<td><strong><fmt:formatDate value="${empDetails.dob}" pattern="MM/dd/yyyy"/></strong></td>
						</tr>
						<tr>
							<td><spring:message  code='label.gender'/></td>
							<c:if test = "${not empty empDetails.gender }"><td><strong>${empDetails.gender}</strong></td></c:if>
							<c:if test = "${empty empDetails.gender }"><td><weak><spring:message code="label.notApplicable"/></weak></td></c:if>
							<%-- <td><strong>${empDetails.gender}</strong></td> --%>
						</tr>
						<tr>
							<td><spring:message  code='label.taxId'/></td>
							<c:if test="${empDetails.ssn != null && empDetails.ssn != ''}">
							<td><strong>
								<c:set var="ssnParts" value="${fn:split(empDetails.ssn,'-')}" />
									***-**-${ssnParts[2]}
							</strong></td>
							</c:if>
						</tr>
						<c:set var="tobaccoDisplay" value="" />
						<c:if test="${displayTobaccoUse == 'HIDE'}">
					 		<c:set var="tobaccoDisplay" value="style=\"display:none;\"" />
						</c:if>
						<tr ${tobaccoDisplay}>
							<td><spring:message  code='label.tobacoUser'/></td>
							<td><strong>${empDetails.smoker}</strong></td>
						</tr>
						<tr ${tobaccoDisplay}>
							<td><spring:message  code='label.nativeAmerican'/></td>
          					<td><c:choose>
							    <c:when test="${empDetails.nativeAmr == null}">
							       <weak>N/A</weak>
							    </c:when>
							    <c:otherwise>
							       <strong>${empDetails.nativeAmr}</strong>
							    </c:otherwise>
							</c:choose></td>
						</tr>
						<tr>
							<td><spring:message  code='label.dependent'/></td>
							<td><strong></strong><c:choose>
							    <c:when test="${dependents == ''}">
							       <weak>N/A</weak>
							    </c:when>
							    <c:otherwise>
							        <strong>${dependents}</strong>
							    </c:otherwise>
							</c:choose></strong></td>
						</tr>
					</tbody>
				</table>
				<h4 class="lightgray"><spring:message  code='label.ContactInfoh2'/></h4>
				<table class="table table-border-none">
					<tbody>
						<tr>
							<td class="span5"><spring:message  code='label.emplEmail'/></td>
							<td><strong>${empDetails.email}</strong></td>
						</tr>
						<tr>
							<td><spring:message  code='label.phonenumber'/></td>
							<td><c:choose>
							    <c:when test="${(empDetails.contactNumber == null) || (fn:length(empDetails.contactNumber) < 5)}">
							       <weak>N/A</weak>
							    </c:when>
							    <c:otherwise>
							        <strong>${empDetails.contactNumber}</strong>
							    </c:otherwise>
							</c:choose></td>
						</tr>
						<tr>
							<td><spring:message  code='label.homeAddress'/></td>
							
							<%-- <td><strong>${empDetails.location.address1},${empDetails.location.city},${empDetails.location.state},${empDetails.location.zip}</</strong></td> --%>
							<c:if test = "${not empty empDetails.location.address1 || not empty empDetails.location.city || not empty empDetails.location.state || not empty empDetails.location.zip  }"><td><strong>${empDetails.location.address1},${empDetails.location.city},${empDetails.location.state},${empDetails.location.zip}</strong></td></c:if>
							<c:if test = "${empty empDetails.location.zip  }"><td><weak><spring:message code="label.notApplicable"/></weak></td></c:if>
							
						</tr>
					</tbody>
				</table>
				<!-- Temporary call to generateActivationLink - Starts -->
				<form class="form-horizontal" id="generateActivationLink" name="generateActivationLink" action="<c:url value="/shop/employer/manage/employeeinfo/generateactivationlink/${employee.id}" />" method="POST">
					<df:csrfToken/>
					<div class="control-group" style="display: none;">
						<div class="controls">
							<input type="submit" class="btn btn-primary" value="<spring:message  code='label.generateactivationlink'/>" id="generateActivationLinkBtn" name="generateActivationLinkBtn" title="<spring:message  code='label.generateactivationlink'/>">
							<p><c:out value="${generateactivationlinkInfo}"></c:out><p/>
						</div>
					</div>
				</form>
				<!-- Temporary call to generateActivationLink - Ends -->
			</div>
			<!--gutter10-->
		</div>
		<!--rightpanel-->
	</div>
	<!--row-fluid-->
</div>
</div> <!--gutter10--> 
<script type="text/javascript">
function updateWaiveCoverage(elem){
	var waiveOption = "";
	 $(':radio[name="waive_reason"]').each(function() {
		if(this.checked)	 
	 		waiveOption = this.value;
	 });
	
	if(waiveOption !=""){
		
		if(waiveOption == 'other'){
			waiveOption = $('#waive_reason_other').val() + "."; }
		window.location.href ='../waivecoverage/'+elem + '&SN='+waiveOption;
	}
}

$(document).ready( function () {
	 
	maxLength = $("textarea#waive_reason_other").attr("maxlength");
        $("textarea#waive_reason_other").after("<div><span id='remainingLengthTempId'>" 
                  + maxLength + "</span> remaining</div>");
 
        $("textarea#waive_reason_other").bind("keyup change", function(){checkMaxLength(this.id,  maxLength); } )
 
    });
 
    function checkMaxLength(textareaID, maxLength){
 
        currentLengthInTextarea = $("#"+textareaID).val().length;
        $(remainingLengthTempId).text(parseInt(maxLength) - parseInt(currentLengthInTextarea));
 
		if (currentLengthInTextarea > (maxLength)) { 
 
			// Trim the field current length over the maxlength.
			$("textarea#waive_reason_other").val($("textarea#waive_reason_other").val().slice(0, maxLength));
			$(remainingLengthTempId).text(0);
 
		}
    }
</script>