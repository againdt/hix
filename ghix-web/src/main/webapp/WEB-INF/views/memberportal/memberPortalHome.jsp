<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/consumer-dashboard-util" prefix="util"%>

<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<!-- <link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" /> -->
<!-- <link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" /> -->
<style>

.well-step {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
    border-color: rgb(227, 227, 227);
    border-radius: 4px 4px 4px 4px;
    border-style: solid;
    border-width: 1px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05) inset;
    margin: 0 10px 20px 0;
    min-height: 20px;
    padding: 19px 5px 10px 19px;
}

.data-display h5#hh-elig {
	width: 280px;
}
</style>

<div class="gutter10" id="member-portal">
	<div class="row-fluid">
  		<h1>Welcome Back,&nbsp;<span class="user-name">${memberName}</span></h1>
  	</div>
  	
	<div class="row-fluid">
     
	    <div class="span3" id="sidebar">
	      <util:myStuff ffmClear="${ffmClear}"></util:myStuff>
	      <util:quickLinks user="${user}"></util:quickLinks>
	    </div>
    
	    <div class="span9 dashboard-rightpanel" id="rightpanel">
	 		<!-- for CRM view  -->
	  		<c:if test="${csrView == true}">  
	    		<h4>Viewing ${employee.name} <a href="/hix/crm/crmemployee/details/${employeeId}"class="btn btn-primary showcomments">My Account</a></h4>
			</c:if>
	  		<!-- for CRM view end -->
	  
			<div class="well-step">	  
				<div class="row-fluid gutter10-trb">
					<div  id="step-progress">        
				        <div id="progress-bar">
				           	<ul>
				           		<li id="step-one" class="step-desc">
				                	<span class="s-no">1</span><span class="step-text" alt="Initial Eligibility" aria-label="Initial Eligibility">Initial Eligibility</span>
				               	</li>
				               	<li id="step-two" class="step-desc">
				                 	<span class="s-no">2</span><span class="step-text" alt="Find a Plan" aria-label="Find a Plan">Find a Plan</span>
				               	</li>
				               	<li id="step-three" class="step-desc">
				                 	<span class="s-no">3</span><span class="step-text" alt="Final Eligibility" aria-label="Final Eligibility">Final Eligibility</span>
				               </li>
				               <li id="step-four" class="step-desc">
				                 	<span class="s-no">4</span><span class="step-text" alt="Enroll" aria-label="Enroll">Enroll</span>
				               </li>
							</ul>             
						</div> <!--/End Progress-->
					</div><!--/End step-progress-->                            
				</div><!--/End row-fluid-->       
				<div id="oep-msg" class="blurb clearfix">
					<h4>What's Next?</h4>
					<div class="gutter10-lr">
						<div class="span8" id="complete-oep"></div>
				        <div id="initial-eligibility" class="span4 action">
				            <button onclick="gotoNext('1');" alt="Initial Eligibility" class="btn btn-primary pull-right">Initial Eligibility</button> 
				        </div>
    				</div><!--/gutter10-lr-->
				</div><!--/oep-msg-->
			</div> <!-- End .well-step -->
			<div class="row-fluid gutter20-t">
				<div class="span6 info-block">                    
				  <div class="gutter10-trb">
				    <div class="header-primary"><h4>Eligibility Summary</h4></div>
				    <div class="widget">
						<div id="plan-summary" class="gutter10">
							<h5 id="msg1-header" class="secondary alert alert-info">Initial Eligibility Needed</h5> <!-- add class ".finished-step-checkmark" to h5 for completed checkmark to appear -->
							<h5 id ="msg2-header" class="secondary alert alert-info">Initial Eligibility Complete</h5>
							<h5 id ="msg3-header" class="secondary alert alert-info">Final Eligibility Complete</h5>
						  
						  	<p id="es-msg1">We need to collect more information from you before we can provide you with your potential savings on healthcare costs.</p>
						  	<p id="es-msg2">It seems that you are <a href="#" id="eligibility-temp-findings" rel="tooltip" data-html="true" tabindex="-1" data-original-title="This determination is based on a simplified subsidies calculator and is yet not final.">most likely</a> eligible for government subsidies for your premiums.</p>
						  	<p id="es-msg3">You are eligible for the following government subsidies for your premiums:</p>
						  	<p id="es-msg4">Congratulations, it seems that you are doing well.</p>
							<div class="data-display well"> <!-- should be removed for the fifth scree and replaced it with the image madicaid -->
								<h5 id="hh-elig">Household Eligibility</h5> <!-- to hide the element add class "hide" -->
								<dl class="dl-horizontal">
							   		<dt>Program:</dt>
						   			<dd id="program">-----</dd>
							   		<dt>Premium Tax Credit:</dt>
							   		<dd id="prem-tax-credit">${aptc}</dd>
							   		<dt>Cost Sharing Reduction:</dt>
							   		<dd id="generic-medic">${csr}</dd>
							  	</dl>
								<div class="msg2 txt-center hide">
									<h3>$115/mo</h3>
									<p>Subsidy for <span class="user-name">Rebecca and George</span></p>
								</div>
							</div>
						    <div id="affordability-notice" class="well alert-danger notice">                        
						        <p id="an-msg1" class="msg1">Check back here after you've completed your tax savings elegibility determination.</p>
						        <p id="an-msg2" class="msg3">You also have family members that are eligible for the Medicaid and CHIP programs.</p>
						        <p id="an-msg3" class="msg4">Unfortunately, that means you are not eligible for any goverment subsidies on your insurance premiums.</p>
						    </div>
						    <div class="gutter10-tb msg2">
						    	<button id="es-details1" class="btn" alt="View Details">View Details</button>
						    	<button id="es-details2" class="btn" alt="View Details">Go to Medicaid</button>
						    </div>
						</div> <!-- plan-summary -->
					</div> <!-- widget -->
				  </div>
				</div> <!-- .span6 -->
				
				<div class="span6 info-block" id="summary">
					<div class="gutter10">
				   		<div class="header-primary"><h4>Plan Summary</h4></div>
				   		<div class="widget">
					  		<div id="sample-plan-summary" class="gutter10">
					   	  		<h5 class="secondary alert alert-info">No Plans Selected</h5>	   	                  
					      		<p id="summary-msg1">When you select a plan, it will be displayed here.</p>
					      		<div class="plan-selected alert alert-disabled">
					        		<div class="info-header txt-center">
					            		<img src="<c:url value="/resources/img/sample-plan.png"/>" alt="Sample Plan Image">
					           			<p><a href="#">CA SILVER+ 2014</a></p>
					           			<h5 class="payment">$230/mo</h5>
					       			</div>
					      
					       			<div id="plan-mask-sample" class="mask"><img src=" <c:url value="/resources/img/example.png"/>" alt="This is a sample plan summary" /></div>
					       			<!-- <div id="plan-mask-waived" class="mask"><img src="/hix/resources/img/waived.png" alt="This is a sample plan summary" /></div> -->
					               	<div class="data-display well">
						                <dl class="dl-horizontal">
						                   <dt>Plan Type:</dt>
						                   <dd id="plan-type">PPO</dd>
						                   <dt>Office Visit:</dt>
						                   <dd id="off-visit">$25</dd>
						                   <dt>Generic Medications:</dt>
						                   <dd id="generic-medic">$10</dd>
						                   <dt>Deductible:</dt>
						                   <dd id="deductible">$1,500</dd>
						                   <dt>Max Out-of-Pocket:</dt>
						                   <dd id="m-o-p">$5,000</dd>
						                 </dl>
					             	</div><!-- data-display well -->
					             </div><!-- plan-selected -->
							</div><!-- sample-plan-summary -->
						</div> <!-- widget -->
						<div class="widget">
					        <div id="plan-summary" class="gutter10 tile hide">
					           	<div class="label label-info">Plan Selected as Favorite</div>
					           	<p id="summary-msg-success">This is the plan you selected:</p>
					       
								<div id="plan-selected alert alert-info">
					                <div class="info-header txt-center">
					                	<img src="<c:url value="/resources/img/sample-plan.png"/>" alt="Sample Plan Image">
					            		<p><a href="#">CA SILVER+ 2014</a></p>
					            		<h5 class="payment">$230/mo</h5>
					        		</div><!-- info-header  -->
					        		<!-- <div id="plan-mask" class="mask"><img src="/hix/resources/img/waived.png" alt="This is a sample plan summary" /></div> -->
					                <div class="data-display well">
						                 <dl class="dl-horizontal">
						                    <dt>Plan Type:</dt>
						                    <dd id="plan-type">PPO</dd>
						                    <dt>Office Visit:</dt>
						                    <dd id="off-visit">$25</dd>
						                    <dt>Generic Medications:</dt>
						                    <dd id="generic-medic">$10</dd>
						                    <dt>Deductible:</dt>
						                    <dd id="deductible">$1,500</dd>
						                    <dt>Max Out-of-Pocket:</dt>
						                    <dd id="m-o-p">$5,000</dd>
						                  </dl>
					                </div><!-- data-display well -->
								</div><!-- plan-selected -->
								<div class="gutter10-tb">
								  	<!--<button id="benefit-details" class="btn hide" alt="View Benefit Details">View Benefit Details</button> -->
									<!--<button id="details" class="btn" alt="View Details">View Details</button> -->
								</div>
							</div><!-- plan-summary -->
						</div> <!-- widget -->           
					</div> <!-- .gutter10-->
				</div> <!-- .span6 #summary-->
			</div><!-- .row-fluid gutter20-t -->
		</div> <!-- #rightpanel -->
	</div><!--/row-fluid-->        
</div><!--/main .gutter10-->


<jsp:include page="reportChange.jsp" >
	<jsp:param name="memberName" value="${memberName}" />
</jsp:include> 
<jsp:include page="concierge.jsp" >
	<jsp:param name="memberName" value="${memberName}" />
</jsp:include> 

<jsp:include page="conciergeDetail.jsp" >
	<jsp:param name="memberName" value="${memberName}" />
</jsp:include> 

<jsp:include page="memberPortalThankYou.jsp" >
	<jsp:param name="memberName" value="${memberName}" />
</jsp:include> 

<script type="text/javascript">

<%
String afterConcierge = (String)request.getAttribute("afterConcierge"); 
String afterConciergeDetail = (String)request.getAttribute("afterConciergeDetail"); 
%>

$(document).ready(function(){
	applyRules();
	$('h1').removeClass('offset3');
	var afterConcierge= '<%=afterConcierge%>';
	var afterConciergeDetail= '<%=afterConciergeDetail%>';
	
	 if(afterConcierge=='afterConcierge'){ 
	        $('#concierge-details').modal({
	            backdrop: true 
	        });
   } 
	 if(afterConciergeDetail=='afterConciergeDetail'){ 
	        $('#concierge-thankYou').modal({
	            backdrop: true 
	  });
}
	
});

$(function(){
    // pop up popover
    $('#eligibility-temp-findings').tooltip('hide');

});

function applyRules(){
	var pageNumber = '${pageNumber}';
	
		if(pageNumber == '1'){
						
			$('#step-one').addClass("active-state");
			$('#step-two').addClass("inactive-state");
			$('#step-three').addClass("inactive-state");
			$('#step-four').addClass("inactive-state");
					
			$('#complete-oep').append("<p>Before we show you plan that are right for you, we will guide you through a quick wizard that will estimate your level of tax subsidies towards your health insurance for <strong>2014</strong>.</p>");
			
			$('#find-plan').remove();
			$('#final-elegibility').remove();
			$('#find-New-Plan').remove();
			$('#enroll').remove();
			
		}
		else if(pageNumber == '2'){
							
			$('#step-one').addClass("completed-state");
			$('#step-two').addClass("active-state");
			$('#step-three').addClass("inactive-state");
			$('#step-four').addClass("inactive-state");
			
			
			$('#complete-oep').append("<p>You have completed the Initial Eligibility determination. Next, Let's <strong>Find a Plan</strong> that best meets your needs.</p>");
		
			$('#initial-eligibility').remove();
			$('#final-elegibility').remove();
			$('#find-New-Plan').remove();
			$('#enroll').remove(); 
									
			addCheckMark(1);

		}
		else if(pageNumber == '3'){
          			
			$('#step-one').addClass("completed-state");
			$('#step-two').addClass("completed-state");
			$('#step-three').addClass("active-state");
			$('#step-four').addClass("inactive-state");
			
			$('#complete-oep').append("<p>We will now navigate to the federal Marketplace to determine your exact <strong>Eligibility</strong> for subsidies.</p>");
			
			$('#initial-eligibility').remove();
			$('#find-plan').remove();
			$('#find-New-Plan').remove();
			$('#enroll').remove();
									
			addCheckMark(2);
		}
		else if(pageNumber == '4'){
									
			$('#step-one').addClass("completed-state");
			$('#step-two').addClass("completed-state");
			$('#step-three').addClass("completed-state");
			$('#step-four').addClass("active-state");
						
			$('#complete-oep').append("<p>We will collect your payment information and <strong>Finalize your Enrollment</strong>.</p>");
			
			$('#initial-eligibility').remove();
			$('#find-plan').remove();
			$('#final-elegibility').remove();
						
			addCheckMark(3);
		}
		updateEligWidget(pageNumber);
}

function updateEligWidget(pageNum){
		
    var hhEligibility = '${hhEligibility}';
    
    var hhChipMedicaid = '${hhChipMedicaid}';
	//alert(hhChipMedicaid);
	if (pageNum == 1){
		$('#hh-elig').addClass("hide");
		$('#msg2-header').addClass("header-secondary hide");
		$('#msg3-header').addClass("header-secondary hide");
		
		$('#es-msg2').addClass("msg1 hide");
		$('#es-msg3').addClass("msg3 hide");
		$('#es-msg4').addClass("msg4 hide");
		$('#es-msg5').addClass("msg4 hide");
		
		$('#an-msg2').addClass("msg2 hide");
		$('#an-msg3').addClass("msg3 hide");
		
		//hide view details and medicaid buttons
		$('#es-details1').addClass("hide");
		$('#es-details2').addClass("hide");
	}
	
	else if (pageNum == 2 || pageNum == 3){
		$('#msg1-header').addClass("header-secondary hide");
		$('#msg3-header').addClass("header-secondary hide");
		
		$('#msg2-header').addClass(".finished-step-checkmark");
		
		$('#es-msg1').addClass("msg1 hide");
		$('#es-msg3').addClass("msg3 hide");
		$('#es-msg4').addClass("msg4 hide");
		$('#es-msg5').addClass("msg4 hide");
		
		//$('#an-msg1').addClass("msg1 hide");
		//$('#an-msg2').addClass("msg2 hide");
		//$('#an-msg3').addClass("msg3 hide");
		$('#affordability-notice').addClass("hide");
		
		
		$('#es-details2').addClass("hide");
	}
	else if (pageNum == 4){
		$('#msg1-header').addClass("header-secondary hide");
		$('#msg2-header').addClass("header-secondary hide");
		$('#msg3-header').addClass("finished-step-checkmark");
		
		if(hhEligibility == 'MEDICAID'){
			$('#es-msg1').addClass("msg1 hide");
			$('#es-msg2').addClass("msg2 hide");
			$('#es-msg3').addClass("msg3 hide");
			$('#es-msg4').addClass("msg4 hide");
			
			$('#affordability-notice').addClass("hide");
			$('#dd-well').addClass("hide");
			$('#es-details1').addClass("hide");
		}
		else if (hhEligibility == 'NO_APTC'){
			$('#es-msg1').addClass("msg4 hide");
			$('#es-msg2').addClass("msg1 hide");
			$('#es-msg3').addClass("msg3 hide");
			$('#es-msg5').addClass("msg4 hide");
			
			$('#an-msg1').addClass("msg1 hide");
			$('#an-msg2').addClass("msg2 hide");
			
			$('#es-details2').addClass("hide");
			$('#dd-well').addClass("hide");
		}
		else{
			$('#es-msg1').addClass("msg4 hide");
			$('#es-msg2').addClass("msg1 hide");
			$('#es-msg4').addClass("msg3 hide");
			$('#es-msg5').addClass("msg4 hide");
			
			if(hhChipMedicaid == 'true'){
				$('#an-msg1').addClass("msg1 hide");
				$('#an-msg3').addClass("msg3 hide");
			}
			else{
				$('#affordability-notice').addClass("hide");
			}
			$('#es-details2').addClass("hide");
		}
		
	}

}


function gotoNext(pageNum){
	if(pageNum=='1'){
		location.href="<c:url value='/phixhome' />";
	}
	else if(pageNum=='2'){
		//alert('Inside page # 2 ' + ${eligLeadId});
		location.href="<c:url value='/private/saveIndividualPHIX/${eligLeadId}' />";
	}
	else if(pageNum=='3'){
		location.href="<c:url value='/private/enrollUser' />";
	}
	else if (pageNum=='4'){
		location.href="<c:url value='/private/renewal/${eligLeadId}' />";
	}
	
}


function addCheckMark(pageNum){
	if (pageNum == '1'){
		$("#s1 .s-no").html("<i class='icon-ok'></i>");
	}
	else if (pageNum == '2'){
		$("#s1 .s-no").html("<i class='icon-ok'></i>");
		$("#s2 .s-no").html("<i class='icon-ok'></i>");
	}
	else if (pageNum == '3'){
		$("#s1 .s-no").html("<i class='icon-ok'></i>");
		$("#s2 .s-no").html("<i class='icon-ok'></i>");
		$("#s3 .s-no").html("<i class='icon-ok'></i>");
	}
	else if (pageNum == '4'){
		$("#s1 .s-no").html("<i class='icon-ok'></i>");
		$("#s2 .s-no").html("<i class='icon-ok'></i>");
		$("#s3 .s-no").html("<i class='icon-ok'></i>");
		$("#s4 .s-no").html("<i class='icon-ok'></i>");
	}
			
}
</script>
