<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link rel="stylesheet" type="text/css" media="screen" href="<c:url value='/resources/css/agency_portal.css' />"/>

<script type="text/javascript" src="<c:url value='/resources/js/moment.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/angular-moment.min.js' />"></script>

<script type="text/javascript" src="<c:url value='/resources/js/agency/lib/angular-ui-router.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/spring-security-csrf-token-interceptor.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/lib/ui-bootstrap-tpls-0.12.0.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/amplify.min.js' />"></script>

<script type="text/javascript" src="<c:url value='/resources/js/agency/admin/brokerAdmin.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/admin/brokerAdmin.routes.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/agency.module.translate.js' />"></script>

<script type="text/javascript" src="<c:url value='/resources/js/addressValidation.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/generalModal.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/components/modal.directive.module.js' />"></script>

<script type="text/javascript" src="<c:url value='/resources/js/agency/components/agencySidebar.directive.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/loader.directive.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/fileUpload.directive.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/state.directive.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/timepicker.directive.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/autofocus.directive.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/popover.directive.js' />"></script>

<script type="text/javascript" src="<c:url value='/resources/js/agency/services/nav.service.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/status.service.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/fileUpload.service.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/locationhours.service.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/datastorage.service.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/strtruncate.factory.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/timefilter.factory.module.js' />"></script>

<script type="text/javascript" src="<c:url value='/resources/js/agency/admin/agency.controller.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/ManageAgencyGlobalApp.controller.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/adminStaffApp.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/admin/manageAgencies.controller.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/agencyInformation.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/locationHour.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/documentUpload.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/certificationStatus.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/viewAdminStaffList.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/adminStaff.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/adminStaffApproval.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/adminStaffStatus.controller.module.js' />"></script>
<%
	String showPopupInFutureForAgency =  (String) request.getSession().getAttribute("showPopupInFutureForAgency");
%>

<c:set var="showPopupInFutureForAgency" value="<%=showPopupInFutureForAgency%>" />
<input type="hidden" id="showPopupInFutureForAgency" value="${showPopupInFutureForAgency}"/>

<input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>
<input id="isAdminRole" name="isAdminRole" type="hidden" value="${sessionScope.isAdminRole}"/>
<div ng-app="brokerAdminApp" id="aid-agency-portal" ng-controller="ManageAgencyController">
	<ui-view></ui-view>
</div>
