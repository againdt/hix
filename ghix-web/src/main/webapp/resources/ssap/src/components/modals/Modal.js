import React, { useEffect } from 'react';
import { connect } from 'react-redux';

// Components

import BloodRelationshipModal from './BloodRelationshipModal';
import FallbackUIModal from './FallbackUIModal';
import LocateAssistanceModal from './LocateAssistanceModal';
import QualifyingEventDetailsModal from './QualifyingEventDetailsModal';
import ValidateAddressModal from './ValidateAddressModal';

// Types

import MODAL_TYPES from '../../constants/modalTypes';


/**
 *
 * @param {Object} props - contains "modal" props from Redux Store
 * @returns {React.Component} - modal layout and needed content for Modal
 * @constructor
 */
const Modal = props => {

    useEffect(() => {
        const disableScroll = () => window.scrollTo(0, 0);
        window.addEventListener('scroll', disableScroll);
        return () => window.removeEventListener('scroll', disableScroll);
    });

    const getModal = () => {
        switch (props.modal.content) {
        case MODAL_TYPES.FALLBACK_UI_MODAL:
            return <FallbackUIModal/>;
        case MODAL_TYPES.BLOOD_RELATIONSHIP_MODAL:
            return <BloodRelationshipModal/>;
        case MODAL_TYPES.LOCATE_ASSISTANCE_MODAL:
            return <LocateAssistanceModal/>;
        case MODAL_TYPES.QUALIFYING_EVENT_DETAILS_MODAL:
            return <QualifyingEventDetailsModal/>;
        case MODAL_TYPES.VALIDATE_ADDRESS_MODAL:
            return <ValidateAddressModal/>;
        default:
            return <FallbackUIModal/>;
        }
    };

    return (
        <div className="modal__layout">
            <div className="modal__content">
                {
                    getModal()
                }
            </div>
        </div>
    );
};

const mapStateToProps = state => ({
    modal: state.modal
});

export default connect(mapStateToProps)(Modal);

