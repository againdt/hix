package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav.ReverifyAgency3InitVerifResponseSetType;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;


/**
 * Wrapper class for ReverifyAgency3InitVerifResponseSetType
 * 
 * @author Nikhil Talreja
 * @since 10-Feb-2014
 * 
 */
public class ReverifyAgency3InitVerifResponseSetTypeWrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReverifyAgency3InitVerifResponseSetTypeWrapper.class);
	
	private String caseNumber;
	private String nonCitLastName;
	private String nonCitFirstName;
	private String nonCitMiddleName;
	private String nonCitBirthDate;
	private String nonCitEntryDate;
	private String nonCitAdmittedToDate;
	private String nonCitAdmittedToText;
	private String nonCitCountryBirthCd;
	private String nonCitCountryCitCd;
	private String nonCitCoaCode;
	private String nonCitEadsExpireDate;
	private int eligStatementCd;
	private String eligStatementTxt;
	private String webServSftwrVer;
	private String grantDate;
	private String grantDateReasonCd;
	private boolean sponsorDataFoundIndicator;
	private ArrayOfSponsorshipDataTypeWrapperForReverify arrayOfSponsorshipData;
	private String sponsorshipReasonCd;
	private boolean photoIncludedIndicator;
	private byte[] photoBinaryAttachment;
	private boolean caseSentToSecondaryIndicator;
	private String fiveYearBarApplyCode;
	private String lawfulPresenceVerifiedCode;
	private boolean dshAutoTriggerStepTwo;
	private String qualifiedNonCitizenCode;
	private String fiveYearBarMetCode;
	private String usCitizenCode;
    
    public ReverifyAgency3InitVerifResponseSetTypeWrapper(ReverifyAgency3InitVerifResponseSetType reverifyAgency3InitVerifResponseSetType){
    	
    	if (reverifyAgency3InitVerifResponseSetType == null){
    		return;
    	}
    	
    	this.caseNumber = reverifyAgency3InitVerifResponseSetType.getCaseNumber();
    	this.nonCitLastName = reverifyAgency3InitVerifResponseSetType.getNonCitLastName();
    	this.nonCitFirstName = reverifyAgency3InitVerifResponseSetType.getNonCitFirstName();
    	this.nonCitMiddleName = reverifyAgency3InitVerifResponseSetType.getNonCitLastName();
    	try{
	    	if(reverifyAgency3InitVerifResponseSetType.getNonCitBirthDate() != null){
	    		this.nonCitBirthDate = VLPServiceUtil.xmlDateToString(reverifyAgency3InitVerifResponseSetType.getNonCitBirthDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(reverifyAgency3InitVerifResponseSetType.getNonCitEntryDate() != null){
	    		this.nonCitEntryDate = VLPServiceUtil.xmlDateToString(reverifyAgency3InitVerifResponseSetType.getNonCitEntryDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(reverifyAgency3InitVerifResponseSetType.getNonCitAdmittedToDate() != null){
	    		this.nonCitAdmittedToDate = VLPServiceUtil.xmlDateToString(reverifyAgency3InitVerifResponseSetType.getNonCitAdmittedToDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(reverifyAgency3InitVerifResponseSetType.getNonCitEadsExpireDate() != null){
	    		this.nonCitEadsExpireDate = VLPServiceUtil.xmlDateToString(reverifyAgency3InitVerifResponseSetType.getNonCitEadsExpireDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(reverifyAgency3InitVerifResponseSetType.getGrantDate() != null){
	    		this.grantDate = VLPServiceUtil.xmlDateToString(reverifyAgency3InitVerifResponseSetType.getGrantDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
    	}
    	catch(Exception e){
    		LOGGER.error("Exception occured while setting date",e);
    	}


    	this.nonCitAdmittedToText = reverifyAgency3InitVerifResponseSetType.getNonCitAdmittedToText();
    	this.nonCitCountryBirthCd = reverifyAgency3InitVerifResponseSetType.getNonCitCountryBirthCd();
    	this.nonCitCountryCitCd = reverifyAgency3InitVerifResponseSetType.getNonCitCountryCitCd();
    	this.nonCitCoaCode = reverifyAgency3InitVerifResponseSetType.getNonCitCoaCode();
    	this.eligStatementCd = reverifyAgency3InitVerifResponseSetType.getEligStatementCd().intValue();
    	this.eligStatementTxt = reverifyAgency3InitVerifResponseSetType.getEligStatementTxt();
    	
    	this.grantDateReasonCd = reverifyAgency3InitVerifResponseSetType.getGrantDateReasonCd();
    	this.sponsorDataFoundIndicator = reverifyAgency3InitVerifResponseSetType.isSponsorDataFoundIndicator();
    
    	this.arrayOfSponsorshipData = new ArrayOfSponsorshipDataTypeWrapperForReverify(reverifyAgency3InitVerifResponseSetType.getArrayOfSponsorshipData());
    	
    	this.sponsorshipReasonCd = reverifyAgency3InitVerifResponseSetType.getSponsorshipReasonCd();
    	this.photoIncludedIndicator = reverifyAgency3InitVerifResponseSetType.isPhotoIncludedIndicator();
    	this.photoBinaryAttachment = reverifyAgency3InitVerifResponseSetType.getPhotoBinaryAttachment();
    	this.caseSentToSecondaryIndicator = reverifyAgency3InitVerifResponseSetType.isCaseSentToSecondaryIndicator();
    	
    	this.webServSftwrVer = reverifyAgency3InitVerifResponseSetType.getWebServSftwrVer();
    	this.lawfulPresenceVerifiedCode = reverifyAgency3InitVerifResponseSetType.getLawfulPresenceVerifiedCode();
    	this.dshAutoTriggerStepTwo = reverifyAgency3InitVerifResponseSetType.isDSHAutoTriggerStepTwo();
    	this.fiveYearBarApplyCode = reverifyAgency3InitVerifResponseSetType.getFiveYearBarApplyCode();
    	this.qualifiedNonCitizenCode = reverifyAgency3InitVerifResponseSetType.getQualifiedNonCitizenCode();
    	this.fiveYearBarMetCode = reverifyAgency3InitVerifResponseSetType.getFiveYearBarMetCode();
    	this.usCitizenCode = reverifyAgency3InitVerifResponseSetType.getUSCitizenCode();
    }
    
	
    
	public String getCaseNumber() {
		return caseNumber;
	}



	public String getNonCitLastName() {
		return nonCitLastName;
	}



	public String getNonCitFirstName() {
		return nonCitFirstName;
	}



	public String getNonCitMiddleName() {
		return nonCitMiddleName;
	}



	public String getNonCitBirthDate() {
		return nonCitBirthDate;
	}



	public String getNonCitEntryDate() {
		return nonCitEntryDate;
	}



	public String getNonCitAdmittedToDate() {
		return nonCitAdmittedToDate;
	}



	public String getNonCitAdmittedToText() {
		return nonCitAdmittedToText;
	}



	public String getNonCitCountryBirthCd() {
		return nonCitCountryBirthCd;
	}



	public String getNonCitCountryCitCd() {
		return nonCitCountryCitCd;
	}



	public String getNonCitCoaCode() {
		return nonCitCoaCode;
	}



	public String getNonCitEadsExpireDate() {
		return nonCitEadsExpireDate;
	}



	public int getEligStatementCd() {
		return eligStatementCd;
	}



	public String getEligStatementTxt() {
		return eligStatementTxt;
	}



	public String getWebServSftwrVer() {
		return webServSftwrVer;
	}



	public String getGrantDate() {
		return grantDate;
	}



	public String getGrantDateReasonCd() {
		return grantDateReasonCd;
	}



	public boolean isSponsorDataFoundIndicator() {
		return sponsorDataFoundIndicator;
	}



	public ArrayOfSponsorshipDataTypeWrapperForReverify getArrayOfSponsorshipData() {
		return arrayOfSponsorshipData;
	}



	public String getSponsorshipReasonCd() {
		return sponsorshipReasonCd;
	}



	public boolean isPhotoIncludedIndicator() {
		return photoIncludedIndicator;
	}



	public byte[] getPhotoBinaryAttachment() {
		
		if (photoBinaryAttachment == null) {
            return new byte[0];
		}
		
		byte [] attachment = new byte[photoBinaryAttachment.length];
		
		System.arraycopy(photoBinaryAttachment, 0, attachment, 0, attachment.length);
		
		return attachment;
		
	}



	public boolean isCaseSentToSecondaryIndicator() {
		return caseSentToSecondaryIndicator;
	}



	public String getFiveYearBarApplyCode() {
		return fiveYearBarApplyCode;
	}



	public String getLawfulPresenceVerifiedCode() {
		return lawfulPresenceVerifiedCode;
	}



	public boolean isDshAutoTriggerStepTwo() {
		return dshAutoTriggerStepTwo;
	}



	public String getQualifiedNonCitizenCode() {
		return qualifiedNonCitizenCode;
	}



	public String getFiveYearBarMetCode() {
		return fiveYearBarMetCode;
	}



	public String getUsCitizenCode() {
		return usCitizenCode;
	}



	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("CaseNumber",this.caseNumber);
		obj.put("NonCitLastName",this.nonCitLastName);
		obj.put("NonCitFirstName",this.nonCitFirstName);
		obj.put("NonCitMiddleName",this.nonCitMiddleName);
		obj.put("NonCitBirthDate",this.nonCitBirthDate);
		obj.put("NonCitEntryDate",this.nonCitEntryDate);
		obj.put("NonCitAdmittedToDate",this.nonCitAdmittedToDate);
		obj.put("NonCitAdmittedToText",this.nonCitAdmittedToText);
		obj.put("NonCitCountryBirthCd",this.nonCitCountryBirthCd);
		obj.put("NonCitCountryCitCd",this.nonCitCountryCitCd);
		obj.put("NonCitCoaCode",this.nonCitCoaCode);
		obj.put("NonCitEadsExpireDate",this.nonCitEadsExpireDate);
		obj.put("EligStatementCd",this.eligStatementCd);
		obj.put("EligStatementTxt",this.eligStatementTxt);
		obj.put("WebServSftwrVer",this.webServSftwrVer);
		obj.put("GrantDate",this.grantDate);
		obj.put("GrantDateReasonCd",this.grantDateReasonCd);
		obj.put("SponsorDataFoundIndicator",this.sponsorDataFoundIndicator);
		obj.put("ArrayOfSponsorshipData",this.arrayOfSponsorshipData);
		obj.put("SponsorshipReasonCd",this.sponsorshipReasonCd);
		obj.put("PhotoIncludedIndicator",this.photoIncludedIndicator);
		obj.put("PhotoBinaryAttachment",this.photoBinaryAttachment);
		obj.put("CaseSentToSecondaryIndicator",this.caseSentToSecondaryIndicator);
		obj.put("FiveYearBarApplyCode",this.fiveYearBarApplyCode);
		obj.put("LawfulPresenceVerifiedCode",this.lawfulPresenceVerifiedCode);
		obj.put("DSHAutoTriggerStepTwo",this.dshAutoTriggerStepTwo);
		obj.put("QualifiedNonCitizenCode",this.qualifiedNonCitizenCode);
		obj.put("FiveYearBarMetCode",this.fiveYearBarMetCode);
		obj.put("USCitizenCode",this.usCitizenCode);

		return obj.toJSONString();
	}

}
