package com.getinsured.iex.ssap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.iex.ssap.model.SsapApplicantEventVerification;

public interface SsapApplicantEventVerificationRepository extends JpaRepository<SsapApplicantEventVerification, Long> {

	SsapApplicantEventVerification findById(Long id);
	
	@Query("from SsapApplicantEventVerification event where event.ssapApplicantEvent.id = :ssapApplicantEventId")
	SsapApplicantEventVerification findBySsapApplicantEventId(@Param("ssapApplicantEventId") long ssapApplicantEventId);
}
