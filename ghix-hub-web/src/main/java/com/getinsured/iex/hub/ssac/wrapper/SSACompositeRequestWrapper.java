package com.getinsured.iex.hub.ssac.wrapper;

import javax.xml.bind.JAXBElement;

import org.json.simple.JSONAware;

import com.getinsured.iex.hub.ssac.exchange.ObjectFactory;
import com.getinsured.iex.hub.ssac.extension.SSACompositeIndividualRequestType;
import com.getinsured.iex.hub.ssac.extension.SSACompositeRequestType;

public class SSACompositeRequestWrapper implements JSONAware{
	
	private JAXBElement<SSACompositeRequestType> ssaRequest;
	private SSACompositeRequestType compRequest;

	public SSACompositeRequestWrapper(){
		ObjectFactory exchFactory = new ObjectFactory();
		com.getinsured.iex.hub.ssac.extension.ObjectFactory extnfactory = new com.getinsured.iex.hub.ssac.extension.ObjectFactory();
		this.compRequest = extnfactory.createSSACompositeRequestType();
		this.ssaRequest = exchFactory.createSSACompositeRequest(compRequest);
	}
	
	public void addIndividualRequest(SSACompositeIndividualRequestWrapper individual){
		this.compRequest.getSSACompositeIndividualRequest().add(individual.getSystemRepresentation());
	}
	
	public void addIndividualRequest(SSACompositeIndividualRequestType individual){
		this.compRequest.getSSACompositeIndividualRequest().add(individual);
	}
	
	public JAXBElement<SSACompositeRequestType> getSystemRepresentation(){
		return this.ssaRequest;
	}

	@Override
	public String toJSONString() {
		return null;
	}

}
