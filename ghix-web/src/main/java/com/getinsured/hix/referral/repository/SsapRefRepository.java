package com.getinsured.hix.referral.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.accountactivation.repository.IGHIXCustomRepository;
import com.getinsured.hix.platform.security.service.UserService;


/**
 * @author chopra_s
 * Need to move this class under shared models
 */
@Repository
@Qualifier("ssapRefRepository")
@Transactional(readOnly = true)
public class SsapRefRepository  implements IGHIXCustomRepository<Object, Integer>{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SsapRefRepository.class);

	@Autowired 
	private UserService userService;
	
	@Override
	public boolean recordExists(Integer primaryKey) {
		LOGGER.info("Inside SsapRefRepository recordExists method");
		return true;
	}

	@Override
	public void updateUser(AccountUser accountUser, Integer primaryKey) {
		/**Commented As per HIX-57145*/
		/*try {
			LOGGER.info("Inside SsapRefRepository updateUser method primaryKey - " + primaryKey);
			userService.createModuleUser(primaryKey, ModuleUserService.INDIVIDUAL_MODULE, accountUser, true);
		} catch (GIException e) {
			LOGGER.error("Module user creation failed" + e.getMessage());
			
		}*/
		LOGGER.info("Inside SsapRefRepository updateUser method primaryKey - " + primaryKey);
	}

}
