package com.getinsured.hix.plandisplay;

import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.hix.dto.plandisplay.*;
import com.getinsured.hix.dto.plandisplay.dst.SaveHouseholdRequest;
import com.getinsured.hix.model.*;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.EligLeadRequest;
import com.getinsured.hix.model.plandisplay.PdHouseholdDataResponse;
import com.getinsured.hix.model.plandisplay.PdResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.*;
import com.getinsured.hix.model.plandisplay.ProviderBean;
import com.getinsured.hix.plandisplay.util.PlanDisplayConstants;
import com.getinsured.hix.plandisplay.util.PlanDisplayRestClient;
import com.getinsured.hix.plandisplay.util.PlanDisplayUtil;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.PlanDisplayConfiguration;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.screener.util.ScreenerUtil;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household;
import com.getinsured.hix.webservice.plandisplay.individualinformation.ProductType;
import com.google.common.base.Joiner;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


@Controller
@RequestMapping(value="/private")
public class PrivatePlanDisplayController 
{

	private static final Logger LOGGER = LoggerFactory.getLogger(PrivatePlanDisplayController.class);

	@Autowired private PlanDisplayWebHelper planDisplayWebHelper;
	@Autowired private PlanDisplayUtil planDisplayUtil;
	@Autowired private PlanDisplayRestClient planDisplayRestClient;
	@Autowired private UserService userService;
	@Autowired @Qualifier("screenerUtil") private ScreenerUtil phixUtil;
	@Autowired private IssuerService issuerService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private GIWSPayloadService giWSPayloadService;
	@Autowired private Gson platformGson;
	@Autowired private ZipCodeService zipCodeService;	

	@Value("#{configProp}")
	private Properties properties;

	private  String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	
	@RequestMapping(value="/saveIndividualPHIX", method = RequestMethod.GET)
	public String saveIndividualPHIX(final HttpServletRequest request, final Model model, final RedirectAttributes redirectAttrs) throws GIRuntimeException {
		String leadId = null;
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside saveIndividualPHIX GET Method---->", null, false);

		if(request.getSession().getAttribute(ScreenerUtil.LEAD_ID) == null) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Lead Id is not found inside session---->", null, false);
			return "error/errorPageTimeOut";
		} else {
			leadId = request.getSession().getAttribute(ScreenerUtil.LEAD_ID).toString();
		}

		final EligLead eligLead = phixUtil.fetchEligLeadRecord(Long.parseLong(leadId));
		InsuranceType insuranceType = InsuranceType.HEALTH;
		PdResponse pdResponse = planDisplayWebHelper.invokePreShopping(eligLead, insuranceType, eligLead.getCoverageStartDate(),null);
		if(pdResponse ==null || pdResponse.getPdHouseholdDTO()==null){
			return "error/errorPageTimeOut";
		}
		
		PdHouseholdDTO pdHouseholdDTO = pdResponse.getPdHouseholdDTO();
		
		PdSession pdSession = new PdSession();
		pdSession.setPdHouseholdDTO(pdHouseholdDTO);
		pdSession.setFlowType(FlowType.PREELIGIBILITY);
		pdSession.setPdPreferencesDTO(pdResponse.getPdPreferencesDTO());
		
		String issuerId = null;
		
		//HIX-106036
		HttpSession session = request.getSession(true);
		Object issuerIdObj = session.getAttribute("ISSUER_ID");
		if(issuerIdObj != null && StringUtils.isNotBlank(issuerIdObj.toString())) {
			issuerId = issuerIdObj.toString();
		}else {
			issuerId = String.valueOf(issuerService.getIssuerId());
		}
		
        if(StringUtils.isNotBlank(issuerId)  && !issuerId.equalsIgnoreCase("0")){
        	pdSession.setIssuerId(issuerId);
        	pdSession.setFlowType(FlowType.ISSUERVERIFICATION);
        }
			
		List<PdPersonDTO> pdPersonDTOList = planDisplayRestClient.findPersonByHouseholdId(pdHouseholdDTO.getId());
		pdSession.setPdPersonDTOList(pdPersonDTOList);
		planDisplayWebHelper.setCatastrophicEligibility(pdSession, pdPersonDTOList);
		pdSession.setSpecialEnrollmentFlowType(EnrollmentFlowType.NEW);

		session.setAttribute("pdSession", pdSession);
		
		return "redirect:/private/preferences";
	}
	
	@RequestMapping(value="/preferences", method = RequestMethod.GET)
	public String getPreferences(final HttpServletRequest request, final Model model) throws GIRuntimeException
	{	
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getPreferences method---->", null, false);

		final HttpSession session = request.getSession();
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession == null) {
			return "error/errorPageTimeOut";
		}
		PdHouseholdDTO pdHouseholdDTO = pdSession.getPdHouseholdDTO();
		
		PdPreferencesDTO pdPreferencesDTO = pdSession.getPdPreferencesDTO();
		
		PreferencesLevel medicalVal = pdPreferencesDTO.getMedicalUse();
		model.addAttribute("medicalVal", medicalVal);
		
		PreferencesLevel drugVal =	pdPreferencesDTO.getPrescriptionUse();
		model.addAttribute("drugVal", drugVal);

		String benefits= Joiner.on(",").join( pdPreferencesDTO.getBenefits());
		model.addAttribute("benefits",benefits);

		Long householdId = pdHouseholdDTO.getId();
		model.addAttribute("householdId",householdId);
		
		model.addAttribute("showMenu", false);
		
		boolean isCovgYear2018 = planDisplayUtil.is2018CovgYear(pdHouseholdDTO.getCoverageStartDate());
		
		String providerSearchConfig = "OFF";
		if(!("ID".equalsIgnoreCase(stateCode) && isCovgYear2018) && StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue("planSelection.providerSearch"))){
			providerSearchConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.providerSearch");
		}
		model.addAttribute("providerSearchConfig", providerSearchConfig);
		
		String benefitQuestionsConfig = "ON";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.benefitQuestions").isEmpty()){
			benefitQuestionsConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.benefitQuestions");
		}
		model.addAttribute("benefitQuestionsConfig", benefitQuestionsConfig);
		
		List<PdPersonDTO> pdPersonDTOList = pdSession.getPdPersonDTOList();
		model.addAttribute("zipCode", pdPersonDTOList.get(0).getZipCode());
		
		String providersJson="";
		List<ProviderBean> providers = new ArrayList<ProviderBean>();
		if(pdPreferencesDTO.getProviders()!=null){
			providers = platformGson.fromJson(pdPreferencesDTO.getProviders(),new TypeToken<List<ProviderBean>>() {}.getType());
			providersJson = platformGson.toJson(providers);
		}
		model.addAttribute("providersList", providers);
		model.addAttribute("providersJson", providersJson);	
		model.addAttribute("insuranceType", request.getParameter("insuranceType"));		
		
		String prescriptionSearchConfig = "OFF";
		if(!("ID".equalsIgnoreCase(stateCode) && isCovgYear2018) && StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue("planSelection.prescriptionSearch"))){
			prescriptionSearchConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.prescriptionSearch");
		}
		model.addAttribute("prescriptionSearchConfig", prescriptionSearchConfig);
		
		String drugSearchAPIConfig = "OFF";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.drugSearch.API").isEmpty()){
			drugSearchAPIConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.drugSearch.API");
		}
		model.addAttribute("drugSearchAPIConfig", drugSearchAPIConfig);
		
		
		String prescriptionsJson="";
		if(pdPreferencesDTO.getPrescriptions() != null){
			prescriptionsJson = pdPreferencesDTO.getPrescriptions();
		}
		model.addAttribute("prescriptionRequestList", prescriptionsJson);
		
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		model.addAttribute("stateCode", stateCode);
		
		AccountUser loggedInUser = null;
		try {
			loggedInUser = userService.getLoggedInUser();
		} catch (final InvalidUserException ex) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----InvalidUserException in showPlans Method---->", ex, false);
		}
		model.addAttribute("loggedInUser", loggedInUser);
		
		Role currUserDefRole = null;
		if(loggedInUser!=null){
			currUserDefRole = userService.getDefaultRole(loggedInUser);
		}
		model.addAttribute("isIndividual", planDisplayUtil.isIndividualDefaultRole(currUserDefRole));
		
		return "private/preferences";
	}
	
	@RequestMapping(value="/preferences", method = RequestMethod.POST)
	public String savePreferences(final HttpServletRequest request, final Model model) throws GIRuntimeException
	{	
		final HttpSession session = request.getSession();
		
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession == null) {
			return "error/errorPageTimeOut";
		}
		PdHouseholdDTO pdHouseholdDTO = pdSession.getPdHouseholdDTO();
		PdPreferencesDTO pdPrefsDTO = pdSession.getPdPreferencesDTO();

		PdPreferencesDTO pdPreferencesDTO = preparePreferencesDTO(request, pdHouseholdDTO, pdPrefsDTO);
		pdSession.setPdPreferencesDTO(pdPreferencesDTO);
		
		session.setAttribute("pdSession", pdSession);
		InsuranceType insuranceType = StringUtils.isNotEmpty(request.getParameter("insuranceType"))?InsuranceType.valueOf(request.getParameter("insuranceType").toUpperCase()):InsuranceType.HEALTH;
		return "redirect:/private/planselection?insuranceType="+insuranceType;	
	}
	
	@RequestMapping(value="/planselection", method = RequestMethod.GET)
	public String showplans(final HttpServletRequest request, final Model model) throws GIException, GIRuntimeException
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside showplans() WEB---->", null, false);
		final HttpSession session = request.getSession();
		
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession == null) {
			return "error/errorPageTimeOut";
		}
		PdHouseholdDTO pdHouseholdDTO = pdSession.getPdHouseholdDTO();
		
		String insurncType = request.getParameter("insuranceType");
		if(insurncType != null && !"HEALTH".equals(insurncType.toUpperCase()) && !"DENTAL".equals(insurncType.toUpperCase()) ) {
			model.addAttribute("errMassage", "Invalid Insurance Type");
			return "private/errorPage";
		}
		
		InsuranceType insuranceType = InsuranceType.HEALTH;
		if(request.getParameter("insuranceType") != null){
			insuranceType = InsuranceType.valueOf(request.getParameter("insuranceType").toUpperCase());
		}else if(ProductType.D.equals(pdSession.getProductType())){
			insuranceType = InsuranceType.DENTAL;
		}
		
		if(ProductType.D.equals(pdSession.getProductType()) && InsuranceType.HEALTH.equals(insuranceType)){
			model.addAttribute("errMassage", "Invalid Insurance Type");
			return "private/errorPage";
		}
		pdSession.setInsuranceType(insuranceType);
		session.setAttribute("pdSession", pdSession);
		
		String defaultSort =  PlanDisplayConstants.sortConfigs.get("MONTHLY PRICE");
		if(DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.DEFAULT_SORT) != null){
			defaultSort = PlanDisplayConstants.sortConfigs.get(DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.DEFAULT_SORT).toUpperCase());
			if(defaultSort == null || ("estimatedTotalHealthCareCost".equalsIgnoreCase(defaultSort) && InsuranceType.DENTAL.equals(insuranceType))){
				defaultSort =  PlanDisplayConstants.sortConfigs.get("MONTHLY PRICE");
			}
		}					
		model.addAttribute("defaultSort", defaultSort);
		
		String dentalPlansOnOff = planDisplayUtil.getDentalConfig(pdHouseholdDTO.getCoverageStartDate());
		model.addAttribute(PlanDisplayConstants.DENTAL_PLANS_ONOFF, dentalPlansOnOff);
		
		String customGroupingOnOff = "OFF";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.customGrouping").isEmpty()){
			customGroupingOnOff = DynamicPropertiesUtil.getPropertyValue("planSelection.customGrouping");
		}
		model.addAttribute("customGroupingOnOff", customGroupingOnOff);
		
		String gpsVersion = "Baseline";
		if(!properties.getProperty("GPSVersion.individual").isEmpty()){
			gpsVersion = properties.getProperty("GPSVersion.individual").trim();
		}		
		model.addAttribute("gpsVersion", gpsVersion);

		String showQualityRating = "NO";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.showQualityRatingIndividual").isEmpty()){
			showQualityRating = DynamicPropertiesUtil.getPropertyValue("planSelection.showQualityRatingIndividual").trim();
		}
		model.addAttribute("showQualityRating", showQualityRating);
		
		String showOopOnOff = "ON";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.ShowOop").isEmpty()){
			showOopOnOff = DynamicPropertiesUtil.getPropertyValue("planSelection.ShowOop").trim();
		}
		model.addAttribute("showOopOnOff", showOopOnOff);
		
		String dentalGuaranteedEnable = "ON";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.DentalGuaranteed").isEmpty()){
			dentalGuaranteedEnable = DynamicPropertiesUtil.getPropertyValue("planSelection.DentalGuaranteed").trim();
		}
		model.addAttribute("dentalGuaranteedEnable", dentalGuaranteedEnable);
		model.addAttribute("personCount", pdSession.getPdPersonDTOList().size());
		
		String subscirberZipCode = planDisplayRestClient.getSubscriberZip(pdSession);
		if(subscirberZipCode != null){
			model.addAttribute("zipcode", subscirberZipCode);
		}else{
			model.addAttribute("zipcode", pdSession.getPdPersonDTOList().get(0).getZipCode());
		}
		
		
		List<PdPersonDTO> pdPersonDTOList = pdSession.getPdPersonDTOList();
		model.addAttribute("personDataListJson", platformGson.toJson(pdPersonDTOList));
		
		boolean isSeekingCoverage = planDisplayUtil.isSeekingCoverage(pdPersonDTOList);
		model.addAttribute("isSeekingCoverage", isSeekingCoverage);
		if(InsuranceType.DENTAL.equals(insuranceType))
		{
			pdPersonDTOList = planDisplayUtil.getSeekingCovgDentalPersonList(pdPersonDTOList);	
			String healthPlanName = "";
			String missingDental = "";
			String missingDentalNames = "";
			YorN isDentalPlanSelected = YorN.N;
			List<PdOrderItemDTO> orderItemList = planDisplayRestClient.findCartItemsByHouseholdId(pdHouseholdDTO.getId());
			for(PdOrderItemDTO pdOrderItemDTO : orderItemList)
			{
				if(InsuranceType.HEALTH.equals(pdOrderItemDTO.getInsuranceType()))
				{
					IndividualPlan individualPlan = planDisplayRestClient.getPlanDetails(pdOrderItemDTO.getPlanId(), pdHouseholdDTO.getCoverageStartDate(), false);
					healthPlanName = individualPlan.getName();
					String missingDentalwithNames = planDisplayUtil.getMissingDental(individualPlan, pdHouseholdDTO, pdPersonDTOList, pdSession.getSpecialEnrollmentFlowType());
					if(StringUtils.isNotBlank(missingDentalwithNames) && missingDentalwithNames.indexOf("#") > -1) {
						missingDental = missingDentalwithNames.split("#").length >= 1 ? missingDentalwithNames.split("#")[0] : "None";
						missingDentalNames = missingDentalwithNames.split("#").length >= 2 ? missingDentalwithNames.split("#")[1] : "";
					}
					if(pdHouseholdDTO.getHealthSubsidy() != null && pdHouseholdDTO.getMaxSubsidy() != null && pdHouseholdDTO.getMaxSubsidy() != 0){
						Float remainingAptc = new BigDecimal(Float.toString(pdHouseholdDTO.getMaxSubsidy())).subtract(new BigDecimal(Float.toString(pdHouseholdDTO.getHealthSubsidy()))).floatValue();
						model.addAttribute("showAptc", true);
						model.addAttribute("remainingAptc", new BigDecimal(Float.toString(remainingAptc)).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
					}
				} else if(InsuranceType.DENTAL.equals(pdOrderItemDTO.getInsuranceType())){
					isDentalPlanSelected = YorN.Y;
				}
			}
			
			List<Map<String,String>> customGroupList = planDisplayWebHelper.buildCustomGroupList(pdSession.getPdPersonDTOList(), pdHouseholdDTO, pdSession.getSpecialEnrollmentFlowType(), dentalPlansOnOff);
			model.addAttribute("personDataList", customGroupList);
			model.addAttribute("personCount", customGroupList.size());		
			model.addAttribute("healthPlanName", healthPlanName);
			model.addAttribute("missingDental", missingDental);	 
			model.addAttribute("missingDentalNames", missingDentalNames);	 
			model.addAttribute("isDentalPlanSelected", isDentalPlanSelected);
			model.addAttribute("dentalPersonList", pdPersonDTOList);
			
			String closedPopup = request.getParameter("closedPopup");
			model.addAttribute("closedPopup", closedPopup);
		}
		
		String iframe = "no";
		if(session.getAttribute("iframe") != null){
			iframe = (String)session.getAttribute("iframe");
		}
		model.addAttribute("iframe", iframe.toLowerCase());
		
		AccountUser loggedInUser = null;
		try {
			loggedInUser = userService.getLoggedInUser();
			if(loggedInUser != null) {
				LOGGER.info(Thread.currentThread().getName() + " Logged in user : {}", loggedInUser.getUserName());
			}else {
				LOGGER.info(Thread.currentThread().getName()  + " Logged in user is null");
			}
		} catch (final InvalidUserException ex) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----InvalidUserException in showPlans Method---->", ex, false);
		}
		model.addAttribute("loggedInUser", loggedInUser);
		
		FlowType flowType = pdSession.getFlowType();
		model.addAttribute("flowType", flowType);
		
		model.addAttribute("enrollmentType", pdHouseholdDTO.getEnrollmentType());
		model.addAttribute("specialEnrollmentFlowType",pdSession.getSpecialEnrollmentFlowType());
		model.addAttribute("coverageStartDate", DateUtil.dateToString(pdHouseholdDTO.getCoverageStartDate(), "MM/dd/yyyy"));
		model.addAttribute("insuranceType", insuranceType);
		
		BigDecimal maxSubsidy = pdHouseholdDTO.getMaxSubsidy() != null ? new BigDecimal(pdHouseholdDTO.getMaxSubsidy()).setScale(2, BigDecimal.ROUND_HALF_UP) : null;
		BigDecimal maxStateSubsidy = pdHouseholdDTO.getMaxStateSubsidy() != null ? pdHouseholdDTO.getMaxStateSubsidy().setScale(2, BigDecimal.ROUND_HALF_UP) : null;
		model.addAttribute("maxAptc", maxSubsidy);
		model.addAttribute("maxStateSubsidy", maxStateSubsidy);
		model.addAttribute("csr", pdHouseholdDTO.getCostSharing());
		
		if(InsuranceType.HEALTH.equals(insuranceType) && pdHouseholdDTO.getMaxSubsidy() != null && pdHouseholdDTO.getMaxSubsidy() != 0){
			model.addAttribute("showAptc", true);
		}
		
		YorN healthPlanPresent = YorN.N;
		YorN dentalPlanPresent = YorN.N;
		if(EnrollmentType.S.equals(pdHouseholdDTO.getEnrollmentType())){
			List<PdOrderItemDTO> orderItemList = planDisplayRestClient.findCartItemsByHouseholdId(pdHouseholdDTO.getId());
			for(PdOrderItemDTO pdOrderItemDTO : orderItemList){
				if(pdOrderItemDTO.getInsuranceType().equals(InsuranceType.HEALTH)) {
					healthPlanPresent = YorN.Y;
				}else {
					dentalPlanPresent = YorN.Y;
				}
			}
		}
		model.addAttribute("healthPlanPresent", healthPlanPresent);
		model.addAttribute("dentalPlanPresent", dentalPlanPresent);
		
		model.addAttribute("showMenu", false);
		
		String exchangeName = "";
		if(!DynamicPropertiesUtil.getPropertyValue("global.ExchangeName").isEmpty()){
			exchangeName = DynamicPropertiesUtil.getPropertyValue("global.ExchangeName");
		}
		model.addAttribute("exchangeName", exchangeName);
		model.addAttribute("benefitsArray", pdSession.getPdPreferencesDTO() != null ? pdSession.getPdPreferencesDTO().getBenefits() : new ArrayList());
		model.addAttribute("productType", pdSession.getProductType() != null ? pdSession.getProductType().toString() : "");
		
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		String exchangeCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
		model.addAttribute("stateCode", stateCode);
		model.addAttribute("exchangeCode", exchangeCode);	
		model.addAttribute("initialHealthPlanId", pdSession.getInitialHealthPlanId());	
		model.addAttribute("initialDentalPlanId", pdSession.getInitialDentalPlanId());	
		model.addAttribute("coverageYear", Integer.toString(planDisplayUtil.getCoverageyear(pdHouseholdDTO.getCoverageStartDate()) -1));
		
		String showSilverFirst = "OFF";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.showSilverFirst").isEmpty()){
			showSilverFirst = DynamicPropertiesUtil.getPropertyValue("planSelection.showSilverFirst").trim();
		}	
		model.addAttribute("showSilverFirst", showSilverFirst);
		
		String showCurrentPlanFirst = "OFF";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.showCurrentPlanFirst").isEmpty()){
			showCurrentPlanFirst = DynamicPropertiesUtil.getPropertyValue("planSelection.showCurrentPlanFirst").trim();
		}	
		model.addAttribute("showCurrentPlanFirst", showCurrentPlanFirst);
		
		String simplifiedDeductible = "OFF";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.simplifiedDeductible").isEmpty()){
			simplifiedDeductible = DynamicPropertiesUtil.getPropertyValue("planSelection.simplifiedDeductible").trim();
		}	
		model.addAttribute("simplifiedDeductible", simplifiedDeductible);
		
		boolean isCovgYear2018 = planDisplayUtil.is2018CovgYear(pdHouseholdDTO.getCoverageStartDate());
		
		String providerSearchConfig = "OFF";
		if( !("ID".equalsIgnoreCase(stateCode) && isCovgYear2018) && StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue("planSelection.providerSearch"))){
			providerSearchConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.providerSearch");
		}
		model.addAttribute("providerSearchConfig", providerSearchConfig);
		
		String providersJson="";
		List<ProviderBean> providers = new ArrayList<ProviderBean>();
		if(pdSession.getPdPreferencesDTO().getProviders()!=null){
			providers = platformGson.fromJson(pdSession.getPdPreferencesDTO().getProviders(),new TypeToken<List<ProviderBean>>() {}.getType());
			providersJson = platformGson.toJson(providers);
		}
		model.addAttribute("providersList", providers);
		model.addAttribute("providersJson", providersJson);	
		
		String prescriptionSearchConfig = "OFF";
		if( !("ID".equalsIgnoreCase(stateCode) && isCovgYear2018) && StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue("planSelection.prescriptionSearch"))){
			prescriptionSearchConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.prescriptionSearch");
		}
		model.addAttribute("prescriptionSearchConfig", prescriptionSearchConfig);
		
		String prescriptionJson="";
		List<DrugDTO> prescriptionList = new ArrayList<DrugDTO>();
		if(pdSession.getPdPreferencesDTO().getPrescriptions()!=null){
			prescriptionList = platformGson.fromJson(pdSession.getPdPreferencesDTO().getPrescriptions(),new TypeToken<List<DrugDTO>>() {}.getType());
			prescriptionJson = platformGson.toJson(prescriptionList);
		}
		model.addAttribute("prescriptionList", prescriptionList);
		model.addAttribute("prescriptionJson", prescriptionJson);	
		
		String showNetworkTransparencyRating = "OFF";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.showNetworkTransparencyRating").isEmpty()){
			showNetworkTransparencyRating = DynamicPropertiesUtil.getPropertyValue("planSelection.showNetworkTransparencyRating").trim();
		}
		model.addAttribute("showNetworkTransparencyRating", showNetworkTransparencyRating);
		
		String providerMapConfig = "OFF";
		if(StringUtils.isNotEmpty(DynamicPropertiesUtil.getPropertyValue("planSelection.providerMap"))){
			providerMapConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.providerMap");
		}
		model.addAttribute("providerMapConfig", providerMapConfig);
		
		String showSBCScenarios = "OFF";
		if(StringUtils.isNotEmpty(DynamicPropertiesUtil.getPropertyValue("planSelection.showSBCScenarios"))){
			showSBCScenarios = DynamicPropertiesUtil.getPropertyValue("planSelection.showSBCScenarios");
		}
		model.addAttribute("showSBCScenarios", showSBCScenarios);
		
		String providerFilterConfig = "OFF";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.providerFilter").isEmpty()){
			providerFilterConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.providerFilter");
		}
		model.addAttribute("providerFilterConfig", providerFilterConfig);
		
		model.addAttribute("medicalUse", pdSession.getPdPreferencesDTO() != null ? pdSession.getPdPreferencesDTO().getMedicalUse() : "");
		model.addAttribute("prescriptionUse", pdSession.getPdPreferencesDTO() != null ? pdSession.getPdPreferencesDTO().getPrescriptionUse() : "");
		
		Role currUserDefRole = null;
		if(loggedInUser!=null){
			currUserDefRole = userService.getDefaultRole(loggedInUser);
		}
		model.addAttribute("isIndividual", planDisplayUtil.isIndividualDefaultRole(currUserDefRole));
		
		return "private/planselection";
	}
	
	@RequestMapping(value="/getIndividualPlans", method = RequestMethod.GET)
	public @ResponseBody String getplans(final Model model, final HttpServletRequest request) 
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getplans() WEB---->", null, false);
		HttpSession session = request.getSession();
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
	
		List<IndividualPlan> plansWithCost = new ArrayList<IndividualPlan>();
		String planListStr = platformGson.toJson(plansWithCost);
		
		if(pdSession == null) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getplans() WEB, where sessionData : null---->", null, false);
	 	 	return planListStr;
	 	}
		
		String initialCmsPlanId = null;
		
		Boolean showPrevYearEnrlPlan = null;
		String showPrevYearEnrolledPlan = DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.ENABLE_ENROLLED_PLAN_COMPARISON);
		if("ID".equalsIgnoreCase(stateCode) && (StringUtils.isNotBlank(showPrevYearEnrolledPlan) && Boolean.parseBoolean(showPrevYearEnrolledPlan))){
			showPrevYearEnrlPlan = Boolean.TRUE;
		}
		
		String individualPlanStr = planDisplayRestClient.getPlans(pdSession, false, initialCmsPlanId, null, showPrevYearEnrlPlan);
		if(StringUtils.isNotBlank(individualPlanStr)){
			planListStr = individualPlanStr;
		}
			
		return planListStr;
	}

	@RequestMapping(value="/individualCartItems/{planId}", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody String saveCartItems(@PathVariable("planId") final Long planId, @RequestBody final IndividualPlan individualPlan, final HttpServletRequest request) throws GIRuntimeException 
	{	
		String methodName="saveCartItems";
		long startTime=planDisplayUtil.startMethodLog(methodName);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside saveCartItems() WEB method---->", null, false);
		
		final HttpSession session = request.getSession();		
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession == null) {
			return "error/errorPageTimeOut";
		}
		
		PdHouseholdDTO pdHouseholdDTO = pdSession.getPdHouseholdDTO();
		PdResponse pdResponse = planDisplayWebHelper.savePdOrderItem(pdSession, individualPlan);
		
		if(InsuranceType.HEALTH.equals(pdSession.getInsuranceType()) && individualPlan.getAptc() != 0 && pdHouseholdDTO.getMaxSubsidy() != null && pdHouseholdDTO.getMaxSubsidy() != 0){
			float remainingAptc = new BigDecimal(Float.toString(pdHouseholdDTO.getMaxSubsidy())).subtract(new BigDecimal(Float.toString(individualPlan.getAptc()))).floatValue();
			pdHouseholdDTO.setHealthSubsidy(new BigDecimal(Float.toString(individualPlan.getAptc())).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
			if(individualPlan.getStateSubsidy() != null) {
				pdHouseholdDTO.setStateSubsidy(individualPlan.getStateSubsidy().setScale(2, BigDecimal.ROUND_HALF_UP));
			}
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			boolean isCAProfile = stateCode != null ? stateCode.equalsIgnoreCase("CA") : false;
			if(!isCAProfile)
			{
				pdHouseholdDTO.setDentalSubsidy(new BigDecimal(Float.toString(remainingAptc)).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
			}
		}
		
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----PdOrderItemId---->"+pdResponse.getResponse(), null, true);

		final Map<String, String> resp = new HashMap<String, String>();
		resp.put("id", pdResponse.getResponse());
		resp.put("result", pdResponse.getStatus());

		final String gsonString = platformGson.toJson(resp);

		planDisplayUtil.endMethodLog(methodName, startTime);
		return gsonString;
	}	
	
	@RequestMapping(value="/individualCartItems/{itemId}", method = RequestMethod.POST)
	public @ResponseBody String removecartItemsById(@PathVariable("itemId") final long itemId, final HttpServletRequest request) 
	{	
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside removecartItemsById()---->"+itemId, null, false);
		
		String methodName="removecartItemsById";
		long startTime=planDisplayUtil.startMethodLog(methodName);
		
		final HttpSession session = request.getSession();
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession == null) {
			return "FAILED";
		}
		
		PdResponse response = planDisplayRestClient.deleteCartItem(itemId);
		final Map<String, String> resp = new HashMap<String, String>();
		resp.put("result", response.getStatus());
		
		PdHouseholdDTO pdHouseholdDTO = pdSession.getPdHouseholdDTO();
		if("SUCCESS".equals(response.getStatus())) {
			if("HEALTH".equals(response.getResponse())) {
				pdHouseholdDTO.setHealthSubsidy(null);
				pdHouseholdDTO.setDentalSubsidy(null);
				planDisplayRestClient.updateHouseholdSubsidy(pdHouseholdDTO);
			}
		}

		final String gsonString = platformGson.toJson(resp);
		planDisplayUtil.endMethodLog(methodName, startTime);
		return gsonString;
	}

	@RequestMapping(value="/individualCartItems", method = RequestMethod.GET)
	public @ResponseBody List<IndividualPlan> getcartItems(final HttpServletRequest request) 
	{	
		String methodName="getcartItems";
		long startTime=planDisplayUtil.startMethodLog(methodName);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getcartItems()---->", null, false);
		final HttpSession session = request.getSession();
		List<IndividualPlan> cartItemsList = new ArrayList<IndividualPlan>();
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession == null) {
			return cartItemsList;
		}
		PdHouseholdDTO pdHouseholdDTO = pdSession.getPdHouseholdDTO();
		List<PdOrderItemDTO> orderItemList = planDisplayRestClient.findCartItemsByHouseholdId(pdHouseholdDTO.getId());
		for(PdOrderItemDTO pdOrderItemDTO : orderItemList){
			IndividualPlan individualPlan = new IndividualPlan();
			individualPlan.setPlanId(pdOrderItemDTO.getPlanId().intValue());
			individualPlan.setOrderItemId(pdOrderItemDTO.getId().intValue());
			individualPlan.setPlanType(pdOrderItemDTO.getInsuranceType().toString());
			cartItemsList.add(individualPlan);
		}
		
		planDisplayUtil.endMethodLog(methodName, startTime);
		return cartItemsList;			
	}
	
	@RequestMapping(value="/showCart", method = RequestMethod.GET)
	public String showCart(final HttpServletRequest request, final Model model) 
	{	
		String methodName="showCart";
		long startTime=planDisplayUtil.startMethodLog(methodName);
		final HttpSession session = request.getSession();
		
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession == null) {
			return "error/errorPageTimeOut";
		}
		PdHouseholdDTO pdHouseholdDTO = pdSession.getPdHouseholdDTO();
		
		List<PdOrderItemDTO> orderItemList = planDisplayRestClient.findCartItemsByHouseholdId(pdHouseholdDTO.getId());
		YorN healthPlanPresent = YorN.N;
		YorN  dentalPlanPresent = YorN.N;
		Long healthPlanId = 0L;
		Long dentalPlanId = 0L;
		Float healthAPTC = 0f;
		Float dentalAPTC = 0f;
		Float healthPremium = 0f;
		Float dentalPremium = 0f;
		
		YorN isHealthPlanChanged = YorN.N;
		YorN isDentalPlanChanged = YorN.N;
		String healthPriceChange = "";
		String dentalPriceChange = "";
		String taxCreditChange = "";
		String stateSubsidyChange = "";
		float healthSubsidy = 0.0f;
		float dentalSubsidy = 0.0f;
		float initialTotalSubsidy = 0.0f;
		BigDecimal initialStateSubsidy = pdSession.getInitialStateSubsidy() != null ? pdSession.getInitialStateSubsidy() : new BigDecimal(0);
		
		String initialHealthSubscriberId = pdSession.getInitialHealthSubscriberId();
		String initialDentalSubscriberId = pdSession.getInitialDentalSubscriberId();
		String currentHealthSubscriberId =  null;
		String currentDentalSubscriberId =  null;
		initialTotalSubsidy = pdSession.getInitialHealthSubsidy() != null ? pdSession.getInitialHealthSubsidy() : 0f;
		initialTotalSubsidy += pdSession.getInitialDentalSubsidy() != null ? pdSession.getInitialDentalSubsidy() : 0f;
        float maxSubsidy = 0.0f; // aka MaxAPTC
		maxSubsidy=pdSession.getInitialMaxAPTC()!= null ? pdSession.getInitialMaxAPTC() :initialTotalSubsidy ;
		LOGGER.info(" Max APTC  : {} , Total InitialSubsidy : {}", maxSubsidy,initialTotalSubsidy);
		IndividualPlan cartItemHealth = new IndividualPlan();
		IndividualPlan cartItemDental = new IndividualPlan();
		for(PdOrderItemDTO pdOrderItemDTO : orderItemList){
			IndividualPlan individualPlan = planDisplayRestClient.getPlanDetails(pdOrderItemDTO.getPlanId(), pdHouseholdDTO.getCoverageStartDate(), true);
			individualPlan.setOrderItemId(pdOrderItemDTO.getId().intValue());
			float aptc = pdOrderItemDTO.getMonthlySubsidy()!=null?pdOrderItemDTO.getMonthlySubsidy():0.0f;
			individualPlan.setAptc(aptc);

			if(pdOrderItemDTO.getMonthlyStateSubsidy() != null){
				individualPlan.setStateSubsidy(pdOrderItemDTO.getMonthlyStateSubsidy());
			}else{
				individualPlan.setStateSubsidy(new BigDecimal(0));
			}

			individualPlan.setPremiumAfterCredit(pdOrderItemDTO.getPremiumAfterCredit());
			individualPlan.setPremiumBeforeCredit(pdOrderItemDTO.getPremiumBeforeCredit());
			
			if(pdOrderItemDTO.getInsuranceType().equals(InsuranceType.HEALTH)) {
				cartItemHealth = individualPlan;
			}
			if(pdOrderItemDTO.getInsuranceType().equals(InsuranceType.DENTAL)) {
				cartItemDental = individualPlan;
			}
		}

		List<PdPersonDTO> pdPersonDTOList = planDisplayRestClient.findPersonByHouseholdId(pdHouseholdDTO.getId());
		List<Map<String, String>> healthPlanDetailsByMember = null;
		List<Map<String, String>> dentalPlanDetailsByMember = null;

		for(PdOrderItemDTO pdOrderItemDTO : orderItemList){
			if(pdOrderItemDTO.getInsuranceType().equals(InsuranceType.HEALTH)) {
				healthPlanPresent = YorN.Y;
				healthPlanId = pdOrderItemDTO.getPlanId();
				healthAPTC = pdOrderItemDTO.getMonthlySubsidy();
				healthPremium = pdOrderItemDTO.getPremiumBeforeCredit();
				if(EnrollmentType.S.equals(pdHouseholdDTO.getEnrollmentType()) || FlowType.PLANSELECTION.equals(pdSession.getFlowType())) {
					if(pdSession.getInitialHealthPlanId() != null){						
						String hiosPlanNumber = PlanDisplayUtil.create14DigitNum(cartItemHealth.getIssuerPlanNumber());
						String oldHiosPlanNumber = PlanDisplayUtil.create14DigitNum(pdSession.getInitialHealthCmsPlanId());
						if(hiosPlanNumber.compareTo(oldHiosPlanNumber) != 0){
							isHealthPlanChanged = YorN.Y;
						}
						if(pdOrderItemDTO.getPremiumBeforeCredit().compareTo(pdSession.getInitialHealthPremium()) < 0){
							healthPriceChange = "decreased";
						} else if(pdOrderItemDTO.getPremiumBeforeCredit().compareTo(pdSession.getInitialHealthPremium()) > 0){
							healthPriceChange = "increased";
						}


					}
				}
				healthSubsidy = pdHouseholdDTO.getHealthSubsidy()!=null?pdHouseholdDTO.getHealthSubsidy():0.0f;
				currentHealthSubscriberId =  getSubscriberMemberId(pdOrderItemDTO.getPdOrderItemPersonDTOList(), pdSession.getPdPersonDTOList());
				healthPlanDetailsByMember = planDisplayWebHelper.getPlanDetailsByMember(pdOrderItemDTO.getPdOrderItemPersonDTOList(), pdPersonDTOList);
			}
			else {
				dentalPlanPresent = YorN.Y;
				dentalPlanId = pdOrderItemDTO.getPlanId();
				dentalAPTC = pdOrderItemDTO.getMonthlySubsidy();
				dentalPremium = pdOrderItemDTO.getPremiumBeforeCredit();
				if(EnrollmentType.S.equals(pdHouseholdDTO.getEnrollmentType()) || FlowType.PLANSELECTION.equals(pdSession.getFlowType())) {
					if(pdSession.getInitialDentalPlanId() != null){
						if(pdOrderItemDTO.getPlanId().compareTo(pdSession.getInitialDentalPlanId()) != 0){
							isDentalPlanChanged = YorN.Y;
						}
						if(pdOrderItemDTO.getPremiumBeforeCredit().compareTo(pdSession.getInitialDentalPremium()) < 0){
							dentalPriceChange = "decreased";
						} else if(pdOrderItemDTO.getPremiumBeforeCredit().compareTo(pdSession.getInitialDentalPremium()) > 0){
							dentalPriceChange = "increased";
						}
					}
				}
				dentalSubsidy = pdHouseholdDTO.getDentalSubsidy()!=null?pdHouseholdDTO.getDentalSubsidy():0.0f;
				currentDentalSubscriberId =  getSubscriberMemberId(pdOrderItemDTO.getPdOrderItemPersonDTOList(), pdSession.getPdPersonDTOList());
				dentalPlanDetailsByMember = planDisplayWebHelper.getPlanDetailsByMember(pdOrderItemDTO.getPdOrderItemPersonDTOList(), pdPersonDTOList);
			}
		}
		model.addAttribute("healthPlanDetailsByMember", platformGson.toJson(healthPlanDetailsByMember)); // Used for Details tooltip of Health-Plan
		model.addAttribute("dentalPlanDetailsByMember", platformGson.toJson(dentalPlanDetailsByMember)); // Used for Details tooltip of Dental-Plan

		boolean showHealthSubscriberChanged = false;
		boolean showDentalSubscriberChanged = false;
		if(EnrollmentType.S.equals(pdHouseholdDTO.getEnrollmentType())) {
		if((initialHealthSubscriberId !=null && currentHealthSubscriberId !=null) && !initialHealthSubscriberId.equalsIgnoreCase(currentHealthSubscriberId))
		{
			showHealthSubscriberChanged = true;
		}
		if((initialDentalSubscriberId !=null && currentDentalSubscriberId !=null) && !initialDentalSubscriberId.equalsIgnoreCase(currentDentalSubscriberId))
		{
			showDentalSubscriberChanged = true;
		}
		
		}
		model.addAttribute("showHealthSubscriberChanged", showHealthSubscriberChanged);
		model.addAttribute("showDentalSubscriberChanged", showDentalSubscriberChanged);
		if(pdHouseholdDTO.getMaxSubsidy() != null && (EnrollmentType.S.equals(pdHouseholdDTO.getEnrollmentType()) || FlowType.PLANSELECTION.equals(pdSession.getFlowType()))) {
			if(pdHouseholdDTO.getMaxSubsidy().compareTo(maxSubsidy) < 0){
				taxCreditChange = "decreased";
			} else if(pdHouseholdDTO.getMaxSubsidy().compareTo(maxSubsidy) > 0){
				taxCreditChange = "increased";
			}
		}
		if (pdHouseholdDTO.getMaxStateSubsidy() != null
				&& (EnrollmentType.S.equals(pdHouseholdDTO.getEnrollmentType())
				|| FlowType.PLANSELECTION.equals(pdSession.getFlowType()))) {

			if(pdHouseholdDTO.getMaxStateSubsidy().compareTo(initialStateSubsidy) < 0){
				stateSubsidyChange = "decreased";
			}else if(pdHouseholdDTO.getMaxStateSubsidy().compareTo(initialStateSubsidy) > 0){
				stateSubsidyChange = "increased";
			}
		}
		Float maxAptc = pdHouseholdDTO.getMaxSubsidy();
		Float maxAppliedAptc = BigDecimal.valueOf(healthSubsidy).add(BigDecimal.valueOf(dentalSubsidy)).setScale(2,RoundingMode.HALF_UP).floatValue();		
		healthAPTC = healthAPTC!=null?healthAPTC:0.0f;
		dentalAPTC = dentalAPTC!=null?dentalAPTC:0.0f;	
		
		Float maxUsedAptc =  BigDecimal.valueOf(healthAPTC).add(BigDecimal.valueOf(dentalAPTC)).setScale(2,RoundingMode.HALF_UP).floatValue();
		
		model.addAttribute("cartItemHealth", cartItemHealth);
		model.addAttribute("cartItemDental", cartItemDental);
		model.addAttribute("pdHouseholdId", pdHouseholdDTO.getId());

		model.addAttribute("enrollmentType", pdHouseholdDTO.getEnrollmentType());
		model.addAttribute("coverageDate", DateUtil.dateToString(pdHouseholdDTO.getCoverageStartDate(), "MM/dd/yyyy"));
		model.addAttribute("healthPlanPresent", healthPlanPresent);
		model.addAttribute("dentalPlanPresent", dentalPlanPresent);
		model.addAttribute("applicationId", pdHouseholdDTO.getApplicationId());
		model.addAttribute("personList", pdPersonDTOList);
		model.addAttribute("totalMemberCount", pdPersonDTOList.size());
		model.addAttribute("maxAptc", maxAptc);
		if(pdHouseholdDTO.getMaxStateSubsidy() != null){
			model.addAttribute("stateSubsidy", pdHouseholdDTO.getMaxStateSubsidy());
		}
		model.addAttribute("maxAppliedAptc", maxAppliedAptc);
		model.addAttribute("maxUsedAptc", maxUsedAptc);
		model.addAttribute("healthAPTC", healthAPTC);
		model.addAttribute("dentalAPTC", dentalAPTC);
		model.addAttribute("healthPremium", healthPremium);
		model.addAttribute("dentalPremium", dentalPremium);
		model.addAttribute("healthPlanId", healthPlanId);
		model.addAttribute("dentalPlanId", dentalPlanId);
		model.addAttribute("KEEP_ONLY", pdHouseholdDTO.getKeepOnly());
		model.addAttribute("renewalType", pdSession.getSpecialEnrollmentFlowType());
		model.addAttribute("showMenu", false);
		
		List<PdPersonDTO> dentalPersonList = planDisplayUtil.getSeekingCovgDentalPersonList(pdPersonDTOList);
		model.addAttribute("dentalPersonList", dentalPersonList);
		
		boolean isSeekingCoverage = planDisplayUtil.isSeekingCoverage(pdPersonDTOList);
		model.addAttribute("isSeekingCoverage", isSeekingCoverage);
		
		final String dentalPlansOnOff = planDisplayUtil.getDentalConfig(pdHouseholdDTO.getCoverageStartDate());
		model.addAttribute("dentalPlansOnOff", dentalPlansOnOff);
		
		final String lightBoxOnOff = DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.CATCH_ALL_LIGHTBOX_INDIVIDUAL);
		model.addAttribute("lightBoxOnOff", lightBoxOnOff);

		final String selectDentalOnly = DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.SELECT_DENTAL_ONLY);
		model.addAttribute("selectDentalOnly", selectDentalOnly);
		
		model.addAttribute("wasHealthPlanAvailable", pdSession.getInitialHealthPlanAvailable());
		model.addAttribute("wasDentalPlanAvailable", pdSession.getInitialDentalPlanAvailable());
		model.addAttribute("isHealthPlanChanged", isHealthPlanChanged);
		model.addAttribute("isDentalPlanChanged", isDentalPlanChanged);
		model.addAttribute("healthPriceChange", healthPriceChange);
		model.addAttribute("dentalPriceChange", dentalPriceChange);
		model.addAttribute("taxCreditChange", taxCreditChange);
		model.addAttribute("stateSubsidyChange", stateSubsidyChange);
		model.addAttribute("oldStateSubsidy", initialStateSubsidy);
		model.addAttribute("oldTotalSubsidy", maxSubsidy);//initialTotalSubsidy
		model.addAttribute("oldHealthPremium", pdSession.getInitialHealthPremium());
		model.addAttribute("oldDentalPremium", pdSession.getInitialDentalPremium());
		model.addAttribute("oldDentalPlanId", pdSession.getInitialDentalPlanId());
		
		FlowType flowType = pdSession.getFlowType();
		model.addAttribute("flowType", flowType);
		
		String healthEnrollmentPresent = "N";
		for (PdPersonDTO pdPersonDTO : pdPersonDTOList) {
			String healthEnrlmentId = pdPersonDTO.getHealthEnrollmentId() != null ? pdPersonDTO.getHealthEnrollmentId().toString() : null;
			if(StringUtils.isNotBlank(healthEnrlmentId)) {
				healthEnrollmentPresent = "Y";
			}
		}
		model.addAttribute("healthEnrollmentPresent", healthEnrollmentPresent);
		
		AccountUser loggedInUser = null;
		try {
			loggedInUser = userService.getLoggedInUser();
			
			if(loggedInUser != null) {
				LOGGER.info(Thread.currentThread().getName()+ " Logged in user : {}", loggedInUser.getUserName());
			}else {
				LOGGER.info(Thread.currentThread().getName() + "Logged in user is null");
			}
		} catch (final InvalidUserException ex) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----InvalidUserException in showCart Method---->", ex, false);
		}
		model.addAttribute("loggedInUser", loggedInUser);
		
		String exchangeName = "";
		if(!DynamicPropertiesUtil.getPropertyValue("global.ExchangeName").isEmpty()){
			exchangeName = DynamicPropertiesUtil.getPropertyValue("global.ExchangeName");
		}
		model.addAttribute("exchangeName", exchangeName);
		
		List<Map<String,String>> customGroupList = planDisplayWebHelper.buildCustomGroupList(pdSession.getPdPersonDTOList(), pdHouseholdDTO, pdSession.getSpecialEnrollmentFlowType(), dentalPlansOnOff);
		model.addAttribute("personDataList", customGroupList);
		
		String customGroupingOnOff = "OFF";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.customGrouping").isEmpty()){
			customGroupingOnOff = DynamicPropertiesUtil.getPropertyValue("planSelection.customGrouping");
		}
		model.addAttribute("customGroupingOnOff", customGroupingOnOff);
		model.addAttribute("insuranceType", InsuranceType.DENTAL);
		model.addAttribute("productType", pdSession.getProductType() != null ? pdSession.getProductType().toString() : "");
		
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		String exchangeCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
		String shoppingId = pdSession.getPdHouseholdDTO().getShoppingId();
		model.addAttribute("stateCode", stateCode);
		model.addAttribute("exchangeCode", exchangeCode);
		String anonymousApplyURL = DynamicPropertiesUtil.getPropertyValue(PlanDisplayConfiguration.PlanDisplayConfigurationEnum.ANONYMOUS_APPLY_URL);
		String parts = anonymousApplyURL.substring(anonymousApplyURL.indexOf("?")+1);
		String preShoppingId = "previewId="+shoppingId;
		if (parts != null && !StringUtils.isEmpty(parts))
		{
			preShoppingId = "&"+preShoppingId;
		}
		model.addAttribute("anonymousApplyURL", anonymousApplyURL+preShoppingId);
		//--- financialEffectiveDate defaulting to coverageStartDate as it is used in ID profile ---
		String financialEffectiveDate = DateUtil.dateToString(pdHouseholdDTO.getCoverageStartDate(), "MM/dd/yyyy");
		if(pdSession.getPdHouseholdDTO().getFinancialEffectiveDate() != null)
		{
			financialEffectiveDate = DateUtil.dateToString(pdSession.getPdHouseholdDTO().getFinancialEffectiveDate(), "MM/dd/yyyy");
		}
		model.addAttribute("financialEffectiveDate", financialEffectiveDate);
		
		planDisplayUtil.endMethodLog(methodName, startTime);
		return "private/showcart";
	}
	
	private String getSubscriberMemberId(List<PdOrderItemPersonDTO> pdOrderItemPersons, List<PdPersonDTO> pdPersonsFromSession)
	{
		Long subscriberPdPersonId = null;
		for(PdOrderItemPersonDTO pdOrderItemPersonDTO : pdOrderItemPersons)
		{
			if(YorN.Y.equals(pdOrderItemPersonDTO.getSubscriberFlag()))
			{
				subscriberPdPersonId = pdOrderItemPersonDTO.getPdPersonId();
				break;
			}
		}
		if(subscriberPdPersonId != null)
		{
			for(PdPerson pdPersonSession : pdPersonsFromSession)
			{
				if (pdPersonSession.getId().equals(subscriberPdPersonId))
				{
					return pdPersonSession.getExternalId();
				}
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/updateSeekCoverageDental",  method = RequestMethod.POST)    
	public @ResponseBody String updateSeekCoverageDental(final HttpServletRequest request) {	
		final HttpSession session = request.getSession();
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession == null) {
			return "error/errorPageTimeOut";
		}
		
		String dataString = request.getParameter("dataString");
		try
		{
			List<String[]> dataList = planDisplayWebHelper.getSeekingCoverageData(dataString);
			List<PdPersonDTO> pdPersonDTOList = pdSession.getPdPersonDTOList();
			for(PdPersonDTO pdPersonDTO : pdPersonDTOList) {
				for(int i = 0; i < dataList.size(); i++) {
					String[] personRec = (String[])dataList.get(i);
					if(pdPersonDTO.getId() == Long.parseLong(personRec[0])) {
						pdPersonDTO.setSeekCoverage(YorN.valueOf(personRec[1]));
					}
				}
			}

			String response = planDisplayRestClient.updateSeekCoverageDental(pdPersonDTOList);
		
			if("SUCCESS".equals(response)) {
				pdSession.setPdPersonDTOList(pdPersonDTOList);
				session.setAttribute("pdSession", pdSession);
			} else {
				return "FAILED";	
			}
		} catch(Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occur while executing updateSeekCoverageDental method---->", e, false);
			return "FAILED";
		}
		return "SUCCESS";
	}
	
	@RequestMapping(value = "/checkAndAddAvailablePlan",method = RequestMethod.POST)    
    public @ResponseBody String checkAndAddAvailablePlan(final HttpServletRequest request)
	{		
		   PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside checkAndAddAvailablePlan()---->", null, false);
           final HttpSession session = request.getSession();
           PdSession pdSession = (PdSession)session.getAttribute("pdSession");
           if(pdSession == null) {
                  return "error/errorPageTimeOut";
           }
           
           String dataString = request.getParameter("dataString");
           String itemId = request.getParameter("itemId");
           String dentalPlanId = request.getParameter("dentalId");

           pdSession.setInsuranceType(InsuranceType.DENTAL);
           if(pdSession.getInitialStateSubsidy() == null){
        		pdSession.setInitialStateSubsidy(new BigDecimal(0));
           }
           
           PdSession dummyPdSession = new PdSession();
           BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
           try {
        	   beanUtilsBean.getConvertUtils().register(new DateConverter(null), Date.class);
        	   beanUtilsBean.copyProperties(dummyPdSession, pdSession);
           }catch (IllegalAccessException | InvocationTargetException e1) {
        	   PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occur while executing checkAndAddAvailablePlan for Dental---->", e1, false);
           }
           
           dummyPdSession.setSpecialEnrollmentFlowType(EnrollmentFlowType.KEEP);
           List<PdPersonDTO> pdPersonDTOList = null;
           try
      		{
      			List<String[]> dataList = planDisplayWebHelper.getSeekingCoverageData(dataString);
      			pdPersonDTOList = dummyPdSession.getPdPersonDTOList();
      			for(PdPersonDTO pdPersonDTO : pdPersonDTOList) {
      				for(int i = 0; i < dataList.size(); i++) {
      					String[] personRec = (String[])dataList.get(i);
      					if(pdPersonDTO.getId() == Long.parseLong(personRec[0])) {
      						pdPersonDTO.setSeekCoverage(YorN.valueOf(personRec[1]));
      					}
      				}
      			}
      		} catch(Exception e) {
      			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occur while executing updateSeekCoverageDenta method---->", e, false);
      		}
           List<IndividualPlan> individualPlans = null;
           try
           {
	           String individualPlanStr = planDisplayRestClient.getPlans(dummyPdSession, false, null, dentalPlanId, null);
	           Type type = new TypeToken<List<IndividualPlan>>() {}.getType();
	           individualPlans = (List<IndividualPlan>)platformGson.fromJson(individualPlanStr, type);
           } catch(Exception e) {
        	   PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occur while executing getPlans method---->", e, false);
     		}

           if(individualPlans != null && !individualPlans.isEmpty() && individualPlans.size() == 1) {
           String response = planDisplayRestClient.updateSeekCoverageDental(pdPersonDTOList);
   		   if("SUCCESS".equals(response)) {
   				pdSession.setPdPersonDTOList(pdPersonDTOList);
   				session.setAttribute("pdSession", pdSession);
   		   }
   			
           PdResponse deleteResponse = planDisplayRestClient.deleteCartItem(Long.parseLong(itemId));
           if(deleteResponse != null && "SUCCESS".equals(deleteResponse.getStatus())) 
           {
        	   PdResponse pdResponse = planDisplayWebHelper.savePdOrderItem(pdSession, individualPlans.get(0));
        	   if(pdResponse != null && "SUCCESS".equals(pdResponse.getStatus())){
        		  return "SUCCESS";
        	   }
            }
           }
          return "FAILURE";
    }
	
	@RequestMapping(value="/planinfo/{coverageDate}/{planId}", method = RequestMethod.GET)
	public String getPlansById(@PathVariable("coverageDate") String coverageDate, @PathVariable("planId") final int planId, final Model model) throws GIRuntimeException
	{
		Date coverageStartDate = DateUtil.StringToDate(coverageDate, "MMddyyyy");

		IndividualPlan individualPlan = planDisplayRestClient.getPlanDetails(Long.valueOf(planId), coverageStartDate, false);
		if(individualPlan != null) {
			model.addAttribute("CSR", individualPlan.getCostSharing());
		}	
		model.addAttribute("individualPlan", individualPlan);

		String gpsVersion = "Baseline";
		if(!properties.getProperty("GPSVersion.individual").isEmpty()){
			gpsVersion = properties.getProperty("GPSVersion.individual").trim();
		}		
		model.addAttribute("gpsVersion", gpsVersion);
		
		String showQualityRating = "NO";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.showQualityRatingIndividual").isEmpty()){
			showQualityRating = DynamicPropertiesUtil.getPropertyValue("planSelection.showQualityRatingIndividual").trim();
		}
		model.addAttribute("showQualityRating", showQualityRating);
		
		String simplifiedDeductible = "OFF";
		if(!DynamicPropertiesUtil.getPropertyValue("planSelection.simplifiedDeductible").isEmpty()){
			simplifiedDeductible = DynamicPropertiesUtil.getPropertyValue("planSelection.simplifiedDeductible").trim();
		}	
		model.addAttribute("simplifiedDeductible", simplifiedDeductible);
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		model.addAttribute("stateCode", stateCode);
		
		String showSBCScenarios = "OFF";
		if(StringUtils.isNotEmpty(DynamicPropertiesUtil.getPropertyValue("planSelection.showSBCScenarios"))){
			showSBCScenarios = DynamicPropertiesUtil.getPropertyValue("planSelection.showSBCScenarios");
		}
		model.addAttribute("showSBCScenarios", showSBCScenarios);
		
		if(individualPlan != null && individualPlan.getPlanType() != null && "DENTAL".equalsIgnoreCase(individualPlan.getPlanType())){
			return "private/dentalplaninfo";
		}else{
			return "private/healthplaninfo";
		}
	}
	
	@RequestMapping(value = "/getDisclaimersInfo",  method = RequestMethod.POST)    
	public @ResponseBody Map<String,String> getDisclaimersInfo(final HttpServletRequest request) {	
		String methodName="getDisclaimersInfo";
		long startTime=planDisplayUtil.startMethodLog(methodName);	
		
		String exchangeType = "ON";
		String issuerIdsString = request.getParameter("issuersString");
		
		String[] issuerIds = issuerIdsString.split(",");
		Map<String,String> disclaimerInfoMap = new HashMap<String, String>();
		for(int i=0;i<issuerIds.length; i++){
			String disclaimerInfo = planDisplayRestClient.getDisclaimerInfo(Integer.parseInt(issuerIds[i]), exchangeType);
			disclaimerInfoMap.put(issuerIds[i], disclaimerInfo);
		}
		planDisplayUtil.endMethodLog(methodName, startTime);
		return disclaimerInfoMap;
	}
	
	@RequestMapping(value="/setSpecialEnrollmentType", method = RequestMethod.GET)
	public String setSpecialEnrollmentType(final HttpServletRequest request){
		final HttpSession session = request.getSession();            
        PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession == null) {
			return "error/errorPageTimeOut";
		}
	
		pdSession.setSpecialEnrollmentFlowType(EnrollmentFlowType.NEW);
		session.setAttribute("pdSession", pdSession);
		
		InsuranceType insuranceType = InsuranceType.valueOf(request.getParameter("insuranceType"));
		return "redirect:/private/planselection?insuranceType="+insuranceType;
	}

	@RequestMapping(value="/setSpecEnrFlowType", method = RequestMethod.GET)
	public String setSpecialEnrollmentFlowType( final HttpServletRequest request)
	{
		String methodName = "setSpecEnrFlowType";
		long startTime = planDisplayUtil.startMethodLog(methodName);
        final HttpSession session = request.getSession();            
        PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession == null) {
			return "error/errorPageTimeOut";
		}
		
		Long pdHouseholdId = pdSession.getPdHouseholdDTO().getId();
		List<Long> pdOrderItemIds = planDisplayRestClient.getOrderItemIdsByHouseholdId(pdHouseholdId);
		for(Long pdOrderItemId : pdOrderItemIds) {
			planDisplayRestClient.deleteCartItem(pdOrderItemId);
		}
		pdSession.getPdHouseholdDTO().setHealthSubsidy(null);		
		
		if(pdSession.getProductType() == null || !ProductType.D.equals(pdSession.getProductType())){
        	pdSession.getPdHouseholdDTO().setDentalSubsidy(null);
        } 
		
		List<PdPersonDTO> pdPersonDTOList = pdSession.getPdPersonDTOList();
		
		String shopForDifPlan = request.getParameter("shopForDifPlan");
		if("Y".equals(shopForDifPlan)) {
			pdSession.setSpecialEnrollmentFlowType(EnrollmentFlowType.NEW);
			if(!(stateCode.equalsIgnoreCase("CA")) && EnrollmentType.S.equals(pdSession.getPdHouseholdDTO().getEnrollmentType())){
				CostSharing csrValue = planDisplayRestClient.restoreCSRValue(pdSession.getPdHouseholdDTO().getId());
				if(csrValue != null) {
					pdSession.getPdHouseholdDTO().setCostSharing(csrValue);
				}
			}
		} else {
			pdSession.setSpecialEnrollmentFlowType(EnrollmentFlowType.KEEP);
			planDisplayUtil.setPersonSeekCoverageDental(pdSession.getPdHouseholdDTO(), pdPersonDTOList); 
			planDisplayRestClient.updateSeekCoverageDental(pdPersonDTOList);
		}
		
		session.setAttribute("pdSession", pdSession);
		
		planDisplayUtil.endMethodLog(methodName, startTime);
		
		if("Y".equals(shopForDifPlan)) {
			return "redirect:/private/planselection";
		} else {
			return "redirect:/private/specialEnrollment";
		}
	}
	

	
	@RequestMapping(value="/specialEnrollment", method = RequestMethod.GET)
	public String showSpecialEnrollment(final Model model, final HttpServletRequest request) throws GIException,GIRuntimeException {		
		HttpSession session = request.getSession();		
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession == null) {
			return "error/errorPageTimeOut";
		}
		/*EnrollmentType enrollmentType = pdSession.getPdHouseholdDTO().getEnrollmentType();
		if(EnrollmentType.S.equals(enrollmentType) || EnrollmentType.A.equals(enrollmentType)) {
			Boolean isMRC43 = planDisplayWebHelper.getAddressChangeFlag(pdSession.getPdPersonDTOList());
			if(isMRC43)
			{
				String redirectUrl = planDisplayWebHelper.processAddressChange(pdSession);
				if (redirectUrl != null)
				{
					return redirectUrl;
				}
			}
		}*/
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		ProductType productType = null;
		
		GIWSPayload giWSPayload = giWSPayloadService.findById(pdSession.getPdHouseholdDTO().getGiWsPayloadId().intValue());
		IndividualInformationRequest ind19Request = null;
        try {
        	String payload= giWSPayload.getRequestPayload();
			JAXBContext context  = JAXBContext.newInstance(IndividualInformationRequest.class);
			StringReader stringReaderIND19 = new StringReader(payload);
			ind19Request = (IndividualInformationRequest) context.createUnmarshaller().unmarshal(stringReaderIND19);   
        } catch (Exception e) {
               e.printStackTrace();
        } 
        productType = ind19Request.getHousehold().getProductType();
        if(productType == null){
        	productType = ProductType.A;
        }
        pdSession.setProductType(productType);
		
		
		try {
			planDisplayWebHelper.processEnrollment(pdSession);
		} catch(Exception e){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception in showEmployeeRenewal Method---->", e, false);
		}
			
		session.setAttribute("pdSession", pdSession);
		PdHouseholdDTO pdHouseholdDTO = pdSession.getPdHouseholdDTO();
		if(YorN.N.equals(pdSession.getInitialHealthPlanAvailable()) && !(stateCode.equalsIgnoreCase("CA")) && EnrollmentType.S.equals(pdSession.getPdHouseholdDTO().getEnrollmentType()))
		{
			CostSharing csrValue = planDisplayRestClient.restoreCSRValue(pdSession.getPdHouseholdDTO().getId());
			if(csrValue != null) {
				pdSession.getPdHouseholdDTO().setCostSharing(csrValue);
			}
		}
			
		if(YorN.N.equals(pdSession.getInitialHealthPlanAvailable()) && YorN.N.equals(pdSession.getInitialDentalPlanAvailable())) {
			List<IndividualPlan> unavailablePlans = new ArrayList<IndividualPlan>();
			if(pdSession.getInitialHealthPlanId() != null){
				IndividualPlan individualPlan = planDisplayRestClient.getPlanDetails(pdSession.getInitialHealthPlanId(), pdHouseholdDTO.getCoverageStartDate(), false);
				individualPlan.setPremiumBeforeCredit(pdSession.getInitialHealthPremium());
				unavailablePlans.add(individualPlan);
			}
			
			if(pdSession.getInitialDentalPlanId() != null){
				IndividualPlan individualPlan = planDisplayRestClient.getPlanDetails(pdSession.getInitialDentalPlanId(), pdHouseholdDTO.getCoverageStartDate(), false);
				individualPlan.setPremiumBeforeCredit(pdSession.getInitialDentalPremium());
				unavailablePlans.add(individualPlan);
			}
			
			model.addAttribute("showExpenseEstimate", "NO");
			model.addAttribute("enrollmentType", pdSession.getPdHouseholdDTO().getEnrollmentType());
			model.addAttribute("unAvailablePlans", unavailablePlans);
			pdSession.getPdHouseholdDTO().setKeepOnly(YorN.N);
			
			String showOopOnOff = "ON";
			if(!DynamicPropertiesUtil.getPropertyValue("planSelection.ShowOop").isEmpty()){
				showOopOnOff = DynamicPropertiesUtil.getPropertyValue("planSelection.ShowOop").trim();
			}
			model.addAttribute("showOopOnOff", showOopOnOff);
			
			model.addAttribute("personCount", pdSession.getPdPersonDTOList().size());
			model.addAttribute("productType", pdSession.getProductType() != null ? pdSession.getProductType().toString() : "");
			
			return "private/unavailablePlans";				
		} else {
			return "redirect:/private/showCart";
		}
	}
	
	@RequestMapping(value="/preShoppingComplete", method = RequestMethod.GET)
	public String preShoppingComplete(final HttpServletRequest request) 
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside the method preShoppingComplete---->", null, false);
		long startTime = planDisplayUtil.startMethodLog("preShoppingComplete");
		AccountUser loggedInUser = null;
		final HttpSession session = request.getSession();
		session.setAttribute("previousPageURL", "/private/preShoppingComplete");
		if(session.getAttribute(ScreenerUtil.LEAD_ID) == null)
		{
			return "error/errorPageTimeOut";
		}
		final String eligLeadId = session.getAttribute(ScreenerUtil.LEAD_ID).toString();
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		PdHouseholdDTO pdHouseholdDTO = pdSession.getPdHouseholdDTO();
		final EligLeadRequest eligLeadRequest = new EligLeadRequest();
		eligLeadRequest.setCoverageYear(planDisplayUtil.getCoverageyear(pdHouseholdDTO.getCoverageStartDate()));
		eligLeadRequest.setId(Long.parseLong(eligLeadId));
		eligLeadRequest.setStage(EligLead.STAGE.PRE_SHOPPING_COMPLETE);
		eligLeadRequest.setStatus(EligLead.STATUS.REDIRECTED);
		final String result = planDisplayRestClient.updateEligLeadRecord(eligLeadRequest);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Response from updateEligLeadRecord---->"+result, null, true);
        
        String roleName = ghixJasyptEncrytorUtil.encryptStringByJasypt("INDIVIDUAL");
        String returnUrl = "redirect:/account/signup/" + roleName;
       
        try {
        	loggedInUser = userService.getLoggedInUser();
            planDisplayUtil.endMethodLog("preShoppingComplete", startTime);
            if(loggedInUser != null){
            	returnUrl = "redirect:/indportal";
            }
        } catch (final InvalidUserException ex) { 
        	PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----InvalidUserException in preShoppingComplete Method---->", ex, false);
        }
        return returnUrl;
	}
	
	@RequestMapping(value="/setHousehold/{shoppingId}", method = RequestMethod.GET)
	public String setHousehold(@PathVariable final String shoppingId, final HttpServletRequest request) throws GIRuntimeException 
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside setHousehold Method---->", null, false);
		
		PdResponse pldResponse = planDisplayRestClient.findHouseholdByShoppingId(shoppingId);
		if(pldResponse == null || pldResponse.getPdHouseholdDTO() == null){
			return "error/errorPageTechnicalIssue";
		} 
		PdHouseholdDTO pdHouseholdDTO = pldResponse.getPdHouseholdDTO();
		PdSession pdSession = new PdSession();
		
		pdSession.setPdHouseholdDTO(pdHouseholdDTO);
		pdSession.setPdPreferencesDTO(pldResponse.getPdPreferencesDTO());
		
		pdSession.setFlowType(FlowType.PLANSELECTION);
			
		List<PdPersonDTO> pdPersonDTOList = planDisplayRestClient.findPersonByHouseholdId(pdHouseholdDTO.getId());
		pdSession.setPdPersonDTOList(pdPersonDTOList);			
		//planDisplayWebHelper.setCatastrophicEligibility(pdSession, pdPersonDTOList);	
		pdSession.setShowCatastrophicPlan(Boolean.FALSE);
		if(pdPersonDTOList != null && !pdPersonDTOList.isEmpty()) 
		{
			Boolean isCatastophic = Boolean.TRUE;
			for(PdPersonDTO pdPersonDTO : pdPersonDTOList)
	    	{
				if (pdPersonDTO.getCatastrophicEligible() == null ||  pdPersonDTO.getCatastrophicEligible().equals(YorN.N))
				{
					isCatastophic = Boolean.FALSE;
					break;
				}
	    	}
			pdSession.setShowCatastrophicPlan(isCatastophic);
		}
		pdSession.setSpecialEnrollmentFlowType(EnrollmentFlowType.KEEP);

		HttpSession session = request.getSession(true);
		session.setAttribute("pdSession", pdSession);
		if("CA".equalsIgnoreCase(stateCode)){
			session.setAttribute("householdCaseID", pdHouseholdDTO.getExternalId());
		}
		
		return "redirect:/securePlanDisplay";
	}
	
	@RequestMapping(value="/postShopping",method = RequestMethod.GET)
	public String postShopping(final Model model, final HttpServletRequest request) throws GIRuntimeException
	{
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside postShopping()---->", null,false);
		
		HttpSession session = request.getSession();
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession == null) {
			return "error/errorPageTimeOut";
		}
		
		PdHouseholdDTO pdHouseholdDTO = pdSession.getPdHouseholdDTO();
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		List<PdOrderItemDTO> preShoppingOrderItems = new ArrayList<PdOrderItemDTO>();
		ProductType productType = null;
		Household household = getHouseholdFromIND19(pdHouseholdDTO);
		if(stateCode.equalsIgnoreCase("CA") && household != null && StringUtils.isNotBlank(household.getPreviewId())){
	        String oldShoppingId = household.getPreviewId();
	        productType = household.getProductType();
	        if(productType == null){
	        	productType = ProductType.A;
	        }
	        pdSession.setProductType(productType);
	        if(oldShoppingId != null){
				PdResponse pdResponse = planDisplayRestClient.findHouseholdByShoppingId(oldShoppingId);
				if(pdResponse != null && pdResponse.getPdHouseholdDTO() != null){
					PdHouseholdDTO previewHouseholdDTO = pdResponse.getPdHouseholdDTO();
					List<PdOrderItemDTO> preViewOrderItems = planDisplayRestClient.findCartItemsByHouseholdId(previewHouseholdDTO.getId());
					for(PdOrderItemDTO pdOrderItemDTO : preViewOrderItems){
						if(InsuranceType.HEALTH.equals(pdOrderItemDTO.getInsuranceType()) && !ProductType.D.equals(productType)){
							preShoppingOrderItems.add(pdOrderItemDTO);
						}
						if(InsuranceType.DENTAL.equals(pdOrderItemDTO.getInsuranceType()) && !ProductType.H.equals(productType)){
							preShoppingOrderItems.add(pdOrderItemDTO);
						}		
					}
				}
	        }
	        
			if(preShoppingOrderItems.size() <= 0){
				if(ProductType.D.equals(productType)){
					return "redirect:/private/planselection?insuranceType=DENTAL";
				}else{
					return "redirect:/private/preferences";
				}
			}
			 
		}else{
			String leadId = session.getAttribute(ScreenerUtil.LEAD_ID) != null ? session.getAttribute(ScreenerUtil.LEAD_ID).toString() : null;
			String year = String.valueOf(planDisplayUtil.getCoverageyear(pdHouseholdDTO.getCoverageStartDate()));
			preShoppingOrderItems = planDisplayRestClient.findPreShoppingCartItems(leadId ,year);
			productType = ProductType.A;
			if(household != null && household.getProductType() != null) {
				productType = household.getProductType();
			}
	        pdSession.setProductType(productType);
			if(leadId == null){
				return "redirect:/private/preferences";
			}
		}
		
		try {
			planDisplayWebHelper.processPostShopping(pdSession, preShoppingOrderItems);
		} catch(Exception e){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception in postShopping Method---->", e, false);
			if(ProductType.D.equals(productType)){
				return "redirect:/private/planselection?insuranceType=DENTAL";
			}else{
				return "redirect:/private/preferences";
			}
		}
		
		if(pdSession.getInitialHealthPlanId() == null && pdSession.getInitialDentalPlanId() == null){
			if(ProductType.D.equals(productType)){
				return "redirect:/private/planselection?insuranceType=DENTAL";
			}else{
				return "redirect:/private/preferences";
			}
		}
			
		session.setAttribute("pdSession", pdSession);
		
		if(YorN.N.equals(pdSession.getInitialHealthPlanAvailable()) && YorN.N.equals(pdSession.getInitialDentalPlanAvailable())) {
			List<IndividualPlan> unavailablePlans = new ArrayList<IndividualPlan>();
			if(pdSession.getInitialHealthPlanId() != null){
				IndividualPlan individualPlan = planDisplayRestClient.getPlanDetails(pdSession.getInitialHealthPlanId(), pdSession.getPdHouseholdDTO().getCoverageStartDate(), false);
				unavailablePlans.add(individualPlan);
			}
			
			if(pdSession.getInitialDentalPlanId() != null){
				IndividualPlan individualPlan = planDisplayRestClient.getPlanDetails(pdSession.getInitialDentalPlanId(), pdSession.getPdHouseholdDTO().getCoverageStartDate(), false);
				unavailablePlans.add(individualPlan);
			}
			if(unavailablePlans.isEmpty()){
				if(ProductType.D.equals(productType)){
					return "redirect:/private/planselection?insuranceType=DENTAL";
				}else{
					return "redirect:/private/preferences";
				}
			}
			model.addAttribute("showExpenseEstimate", "NO");
			model.addAttribute("enrollmentType", pdSession.getPdHouseholdDTO().getEnrollmentType());
			model.addAttribute("unAvailablePlans", unavailablePlans);
			return "private/unavailablePlans";				
		} else {
			return "redirect:/private/showCart";
		}
	}
	
	@RequestMapping(value="/applyNewAptc",method = RequestMethod.POST)
	public @ResponseBody String applyNewAptc(final HttpServletRequest request){
		HttpSession session = request.getSession();
		PdSession pdSession = (PdSession)session.getAttribute("pdSession");
		if(pdSession == null) {
			return "error/errorPageTimeOut";
		}
		
		PdHouseholdDTO pdHouseholdDTO = pdSession.getPdHouseholdDTO();
		float monthlyAptc = Float.parseFloat(request.getParameter("montax"));
	
		planDisplayRestClient.reCalculateSubsidy(pdHouseholdDTO.getId(), monthlyAptc);
		
		return "SUCCESS";
	}
	
	@RequestMapping(value="/getHouseholdDataByShoppingId/{shoppingId}", method = RequestMethod.GET)
	public @ResponseBody PdHouseholdDataResponse getHouseholdDataByShoppingId(@PathVariable("shoppingId") String shoppingId, final HttpServletRequest request, final Model model, HttpServletResponse response) throws GIRuntimeException
	{
		PdHouseholdDataResponse pdHouseholdDataResponse = new PdHouseholdDataResponse();
		PdResponse pldResponse = planDisplayRestClient.findHouseholdByShoppingId(shoppingId);
		if(pldResponse == null || pldResponse.getPdHouseholdDTO() == null){
			response.setStatus(500);
			return pdHouseholdDataResponse;
		} 
		PdHouseholdDTO pdHouseholdDTO = pldResponse.getPdHouseholdDTO();
		
		HttpSession session = request.getSession();
		response.setHeader("csrftoken", (session != null && session.getAttribute("csrftoken")!= null) ? session.getAttribute("csrftoken").toString() : "");
		SaveHouseholdRequest saveHouseholdRequest = platformGson.fromJson(pdHouseholdDTO.getHouseholdData(), SaveHouseholdRequest.class);
		saveHouseholdRequest.setPlanPremiumList(null);
		PdPreferencesDTO pdPreferencesDTO = pldResponse.getPdPreferencesDTO();
		
		pdHouseholdDataResponse.setHouseholdData(platformGson.toJson(saveHouseholdRequest));
		pdHouseholdDataResponse.setPdPreferencesDTO(pdPreferencesDTO);
		
		return pdHouseholdDataResponse;
	}
	
	@RequestMapping(value="/savePreferencesForShoppingId/{shoppingId}", method = RequestMethod.POST)
	public @ResponseBody String savePreferencesForShoppingId(@PathVariable("shoppingId") String shoppingId, final HttpServletRequest request, final Model model) throws GIRuntimeException
	{
		PdResponse pldResponse = planDisplayRestClient.findHouseholdByShoppingId(shoppingId);
		if(pldResponse == null || pldResponse.getPdHouseholdDTO() == null){
			return "error/errorPageTechnicalIssue";
		} 
		PdHouseholdDTO pdHouseholdDTO = pldResponse.getPdHouseholdDTO();

		PdPreferencesDTO pdPreferencesDTO = preparePreferencesDTO(request, pdHouseholdDTO, null);
		if(pdPreferencesDTO != null)
		{
			return "SUCCESS";
		}
		else
		{
			return "FAIL";
		}
	}

	private PdPreferencesDTO preparePreferencesDTO(final HttpServletRequest request, PdHouseholdDTO pdHouseholdDTO,
			PdPreferencesDTO pdPrefsDTO) {
		String doctorvisit = null;
		String prescription = null;
		String benefitsArray[] = request.getParameterValues("benefits");
		
		if(request.getParameter("doctorvisit") != null)
		{
			doctorvisit = request.getParameter("doctorvisit");
		}
		else if(pdPrefsDTO != null && pdPrefsDTO.getMedicalUse() != null)
		{
			doctorvisit = pdPrefsDTO.getMedicalUse().name();
		}
		
		if(request.getParameter("prescription") != null)
		{
			prescription = request.getParameter("prescription");
		}
		else if(pdPrefsDTO != null && pdPrefsDTO.getPrescriptionUse() != null)
		{
			prescription = pdPrefsDTO.getPrescriptionUse().name();
		}
		
		List<String> benefits = new ArrayList<String>();
		if (benefitsArray != null && benefitsArray.length > 0){
			benefits=Arrays.asList(benefitsArray);
		}
		
		String providers = null;
		if(request.getParameter("providersJson") != null && StringUtils.isNotBlank(request.getParameter("providersJson"))){
			String doctors = StringEscapeUtils.unescapeHtml(request.getParameter("providersJson"));
			List<ProviderBean> providerBeanList = null;
			doctors=doctors.replaceAll("&quot;", "\"");
			if(doctors!=null && !"all".equals(doctors) && !"[]".equals(doctors)&& !doctors.isEmpty()){
				providerBeanList = platformGson.fromJson(doctors,new TypeToken<List<ProviderBean>>() {}.getType());
				providers = platformGson.toJson(providerBeanList);
			}
		}
		
		String prescriptions = null;
		if(request.getParameter("prescriptionSearchRequest") != null && StringUtils.isNotBlank(request.getParameter("prescriptionSearchRequest"))){
			String drugData = StringEscapeUtils.unescapeHtml(request.getParameter("prescriptionSearchRequest"));
			List<DrugDTO> prescriptionRequestList  = planDisplayUtil.getPrescriptionSearchRequestList(drugData);
			if(prescriptionRequestList != null && !prescriptionRequestList.isEmpty()){
				prescriptions = platformGson.toJson(prescriptionRequestList);
			}
		}
		
		PdPreferencesDTO pdPreferencesDTO=new PdPreferencesDTO();
		pdPreferencesDTO.setMedicalUse(PreferencesLevel.valueOf(doctorvisit));
		pdPreferencesDTO.setPrescriptionUse(PreferencesLevel.valueOf(prescription));
		pdPreferencesDTO.setBenefits(benefits);
		pdPreferencesDTO.setProviders(providers);
		pdPreferencesDTO.setPrescriptions(prescriptions);
					
		Long householdId = pdHouseholdDTO.getId();
		if(householdId != null){
			planDisplayUtil.savePreferences(householdId, pdPreferencesDTO);
		}
		return pdPreferencesDTO;
	}
	
	@RequestMapping(value = "/validateZipAndPullCounties", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Map<String,String>> validateZipAndPullCounties(HttpServletRequest httpRequest) {
		   PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Validating zip code ---->", null, false);
           try {
                  String zip = httpRequest.getParameter("zip");
                  if (StringUtils.isNotBlank(zip)) {
                  List<ZipCode> records = zipCodeService.findByZip(zip);
                  PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Zip---->"+zip, null, false);
                  Map<String, Map<String,String>> retMap = null;
                  if (!records.isEmpty()) {
					retMap = new HashMap<String, Map<String,String>>();
					// Adding counties to return map
					for (int i = 0; i < records.size(); i++) {
						String county = records.get(i).getCounty();
						String countyCode = records.get(i).getCountyCode();
						Map<String,String> values = new HashMap<String, String>();
						values.put("countyCode", countyCode);
						retMap.put(county, values);
					}
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----County Values---->"+retMap, null, false);
					return retMap;
				} else {
					return null;
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		}
	}
	
	@RequestMapping(value = "/changeLang", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, String>> changeLang(@RequestParam(value = "lang", defaultValue = "en") String lang, HttpServletRequest request)
	{
		final HttpSession session = request.getSession();
		final Map<String, String> response = new HashMap<>(2);
		session.setAttribute("lang", lang);
		session.setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, lang);

		response.put("message", "success");
		response.put("lang", session.getAttribute("lang").toString());

		return ResponseEntity.ok(response);
	}
	
	private Household getHouseholdFromIND19(PdHouseholdDTO pdHouseholdDTO) {
		IndividualInformationRequest ind19Request = null;
		try {
			GIWSPayload giWSPayload = giWSPayloadService.findById(pdHouseholdDTO.getGiWsPayloadId().intValue());
			String payload= giWSPayload.getRequestPayload();
			JAXBContext context  = JAXBContext.newInstance(IndividualInformationRequest.class);
			StringReader stringReaderIND19 = new StringReader(payload);
			ind19Request = (IndividualInformationRequest) context.createUnmarshaller().unmarshal(stringReaderIND19); 
			return ind19Request.getHousehold();
		} catch (Exception e) {
		       e.printStackTrace();
		} 
		
		return null;
	}
}
