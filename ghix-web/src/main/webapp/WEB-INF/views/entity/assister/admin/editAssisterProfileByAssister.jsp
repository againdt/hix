<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script type="text/javascript"
	src="<c:url value="/resources/js/addressutils.js" />"></script>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<link href="<c:url value="/resources/css/chosen.css" />"
	rel="stylesheet" type="text/css" media="screen,print">
<link href="<c:url value="/resources/css/entity-custom.css" />"
	media="screen" rel="stylesheet" type="text/css" />
<script src="../resources/js/jquery.validate.min.js"
	type="text/javascript"></script>
<script src="../resources/js/jquery.autoSuggest.minified.js"
	type="text/javascript"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/chosen.jquery.js" />"></script>

<script type="text/javascript">
  $(document).ready(function(){
	  var config = {
		      '.chosen-select'           : {},
		      '.chosen-select-deselect'  : {allow_single_deselect:true},
		      '.chosen-select-no-single' : {disable_search_threshold:10},
		      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		      '.chosen-select-width'     : {width:"95%"}
		    }
		    for (var selector in config) {
		      $(selector).chosen(config[selector]);
		    }
  });
    
</script>
<div class="gutter10">
 <c:set var="encAssisterId" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
  <c:set var="encAssisterEntityId" ><encryptor:enc value="${assister.entity.id}" isurl="true"/> </c:set>
	<div id="sidebar" class="span3">
		<div class="header"></div>
		<ul class="nav nav-list">
			<li><a
				href="<c:url value="/entity/entityadmin/assisterinformation?assisterId=${encAssisterId}&entityId=${encAssisterEntityId}" />"><spring:message
						code="label.assister.myInformation" /></a></li>
			<li><a
				href="<c:url value="/entity/assister/viewassisterprofilebyuserid" />"><spring:message
						code="label.assister.profile" /></a></li>
			<li><a
				href="<c:url value="/entity/assister/certificationstatusbyuserid" />"><spring:message
						code="label.assister.certificationStatus" /></a></li>
		</ul>
	</div>
	<!-- end of span3 -->
	<div class="span9">
		<form class="form-horizontal" id="frmupdateassisterprofile"
			name="frmupdateassisterprofile" method="post"
			action="editassisterprofile" enctype="multipart/form-data">
			<df:csrfToken/>
			
			
			<c:choose>
				<c:when test="${assister.postalMail!=null && assister.postalMail == 'Y'}">
						<input type="hidden" name="postalMail"  value="${assister.postalMail}" />
				</c:when>
				<c:otherwise>
						<input type="hidden" name="postalMail"  value="N" />
				</c:otherwise>
			</c:choose>
			
			
			
			<input type="hidden" id="assisterId" name="assisterId"
				value="<encryptor:enc value="${assister.id}"/>" /> <input type="hidden"
				name="assisterLanguageId" name="assisterLanguageId"
				value="${assisterLanguages.id}" /> <input type="hidden"
				id="entityId" name="entityId" value="<encryptor:enc value="${assister.entity.id}"/>" />
			<div class="header">
				<h4 class="pull-left">Assister Profile</h4>
			</div>

			<div class="control-group">
				<label class="control-label" for="fileInput"><spring:message
						code="label.assister.uploadPhoto" /></label>
				<div class="controls paddingT5">
					<input type="file" class="input-file" id="fileInput"
						name="fileInput">&nbsp; <input type="hidden"
						id="fileInput_Size" name="fileInput_Size" value="1">
					<div>
						<spring:message code="label.uploadDocumentCaption" />
					</div>
					<div id="fileInput_error" class="help-inline"></div>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="firstName"><spring:message
						code="label.assister.Firstname" /><img
					src="<c:url value="/resources/img/requiredAsterisk.png" />"
					alt="Required!" /></label>
				<div class="controls">
					<input type="text" name="firstName" id="firstName"
						value="${assister.firstName}" class="input-xlarge" size="30"
						maxlength="50" />
					<div id="firstName_error"></div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="lastName"><spring:message
						code="label.assister.Lastname" /><img
					src="<c:url value="/resources/img/requiredAsterisk.png" />"
					alt="Required!" /></label>
				<div class="controls">
					<input type="text" name="lastName" id="lastName"
						value="${assister.lastName}" class="input-xlarge" size="30"
						maxlength="50" />
					<div id="lastName_error"></div>
				</div>
			</div>

			<div class="header">
				<h4 class="pull-left">
					<spring:message code="label.assister.mailingAddress" />
				</h4>
			</div>

			<div class="control-group"></div>
			<div class="control-group">
				<label for="address1" class="control-label required"><spring:message
						code="label.assister.streetAddress" /><img
					src="<c:url value="/resources/img/requiredAsterisk.png" />"
					alt="Required!" /></label>
				<div class="controls">
					<input type="text"
						placeholder="<spring:message code="label.address1placeholder"/>"
						name="mailingLocation.address1" id="address1"
						value="${assister.mailingLocation.address1}" class="input-xlarge"
						size="30" maxlength="25" /> <input type="hidden"
						id="address1_hidden" name="address1_hidden"
						value="${assister.mailingLocation.address1}">
					<div id="address1_error"></div>
				</div>
			</div>

			<div class="control-group">
				<label for="address2" class="control-label"><spring:message
						code="label.assister.suite" /></label>
				<div class="controls">
					<input type="text"
						placeholder="<spring:message code="label.address2placeholder"/>"
						name="mailingLocation.address2" id="address2"
						value="${assister.mailingLocation.address2}" class="input-xlarge"
						size="30" maxlength="25" /> <input type="hidden"
						id="address2_hidden" name="address2_hidden"
						value="${assister.mailingLocation.address2}">
				</div>
			</div>

			<div class="control-group">
				<label for="city" class="control-label"><spring:message
						code="label.assister.city" /><img
					src="<c:url value="/resources/img/requiredAsterisk.png" />"
					alt="Required!" /></label>
				<div class="controls">
					<input type="text"
						placeholder="<spring:message code="label.cityplaceholder"/>"
						name="mailingLocation.city" id="city"
						value="${assister.mailingLocation.city}" class="input-xlarge"
						size="30" maxlength="15" /> <input type="hidden" id="city_hidden"
						name="city_hidden" value="${assister.mailingLocation.city}">
					<div id="city_error"></div>
				</div>
			</div>


			<div class="control-group">
				<label for="state" class="control-label"><spring:message
						code="label.assister.state" /><img
					src="<c:url value="/resources/img/requiredAsterisk.png" />"
					alt="Required!" /></label>
				<div class="controls">
					<select size="1" id="state" name="mailingLocation.state"
						path="statelist" class="input-medium">
						<option value="">
							<spring:message code="label.assister.select" />
						</option>
						<c:forEach var="state" items="${statelist}">
							<option id="${state.code}"
								<c:if test="${state.code == assister.mailingLocation.state}"> selected="selected"</c:if>
								value="<c:out value="${state.code}" />">
								<c:out value="${state.name}" />
							</option>
						</c:forEach>
					</select> <input type="hidden" id="state_hidden" name="state_hidden"
						value="${assister.mailingLocation.state}">
					<div id="state_error"></div>
				</div>
			</div>

			<div class="control-group">
				<label for="zip_mailing" class="control-label"><spring:message
						code="label.assister.zipCode" /><img
					src="<c:url value="/resources/img/requiredAsterisk.png" />"
					alt="Required!" /></label>
				<div class="controls">
					<input type="text" placeholder="" name="mailingLocation.zip"
						id="zip" value="${assister.mailingLocation.zip}"
						class="input-mini zipCode" maxlength="5" /> <input type="hidden"
						id="zip_hidden" name="zip_hidden"
						value="${assister.mailingLocation.zip}">
					<div id="zip_error"></div>
				</div>
			</div>

			<div class="control-group phone-group">
				<label for="primaryPhoneNumber" class="required control-label"><spring:message
						code="label.assister.phoneNumber" /><img
					src="<c:url value="/resources/img/requiredAsterisk.png" />"
					alt="Required!" /></label> <label for="primaryPhone1" class="hide">Primary
					phone 1</label> <label for="primaryPhone2" class="hide">Primary
					phone 2</label> <label for="primaryPhone3" class="hide">Primary
					phone 3</label>
				<div class="controls">
					<input type="text" name="primaryPhone1" id="primaryPhone1"
						value="${primaryPhone1}" maxlength="3" placeholder="xxx"
						class="area-code input-mini" /> <input type="text"
						name="primaryPhone2" id="primaryPhone2" value="${primaryPhone2}"
						maxlength="3" placeholder="xxx" class="input-mini" /> <input
						type="text" name="primaryPhone3" id="primaryPhone3"
						value="${primaryPhone3}" maxlength="4" placeholder="xxxx"
						class="input-small" /> <input type="hidden"
						name="primaryPhoneNumber" id="primaryPhoneNumber" value="" />
					<div id="primaryPhone3_error"></div>
					<div id="primaryPhone1_error"></div>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="emailAddress"><spring:message
						code="label.assister.emailAddress" /><img
					src="<c:url value="/resources/img/requiredAsterisk.png" />"
					alt="Required!" /></label>
				<div class="controls">
					<input type="text" name="emailAddress" id="emailAddress"
						value="${assister.emailAddress}" class="input-xlarge" size="30"
						placeholder="<spring:message code="label.assister.asscompany@email.com"/>" />
					<div id="emailAddress_error"></div>
				</div>
			</div>

			<div class="control-group">
				<label for="spoken" class="control-label"><spring:message
						code="label.assister.spokenLanguagesSupported" /><img
					src="<c:url value="/resources/img/requiredAsterisk.png" />"
					alt="Required!" /></label> <input id="${otherSpokenLanguage}" class="hide">

				<label for="${language.lookupValueLabel}" class="hide">${language.lookupValueLabel}</label>
				<div class="controls">
					<c:forEach var="language" items="${languageSpokenList}">
						<input type="checkbox" name="spoken" id="spoken"
							value="${language.lookupValueLabel}"
							${fn:contains(assisterLanguages.spokenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' }>
						<c:out value="${language.lookupValueLabel}" />
						<br>
					</c:forEach>

					<input type="checkbox" name="otherLang" id="otherLang" value="${otherSpokenLanguage}" ${otherSpokenLanguage!=null ? 'checked="checked"' : '' }">
					<spring:message code="label.assister.otherLang" />
					&nbsp;&nbsp; <label for="${otherSpokenLanguage}" class="hide">${otherSpokenLanguage}</label>
					<select
						data-placeholder="<spring:message code="label.selectsomeoptions"/>"
						id="otherSpokenLanguage" name="otherSpokenLanguage"
						class="chosen-select" multiple style="width: 350px;" tabindex="4"></select>
					<input type="hidden" id="spokenLanguages" name="spokenLanguages"
						value="" />

					<div id="spoken_error"></div>
					<div id="otherSpokenLanguage_error"></div>
					<div id="otherLang_error"></div>
				</div>
			</div>

			<div class="control-group">
				<label for="languagesSupported" class="control-label"><spring:message
						code="label.assister.writtenLanguagesSupported" /><img
					src="<c:url value="/resources/img/requiredAsterisk.png" />"
					alt="Required!" /></label> <input class="hide" id="languagesSupported">

				<label for="${language.lookupValueLabel}" class="hide">${language.lookupValueLabel}</label>
				<div class="controls">
					<c:forEach var="language" items="${languageWrittenList}">
						<input type="checkbox" id="written" name="written"
							value="${language.lookupValueLabel}"
							${fn:contains(assisterLanguages.writtenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' }>
						<c:out value="${language.lookupValueLabel}" />
						<br>
					</c:forEach>

					<input type="checkbox" name="otherWrittenLang" id="otherWrittenLang" value="${otherWrittenLanguage}" ${otherWrittenLanguage!=null ? 'checked="checked"' : '' }">
					<spring:message code="label.assister.otherLang" />
					&nbsp;&nbsp; <label for="${otherWrittenLanguage}" class="hide"></label>
					<select
						data-placeholder="<spring:message code="label.selectsomeoptions"/>"
						id="otherWrittenLanguage" name="otherWrittenLanguage"
						class="chosen-select" multiple style="width: 350px;" tabindex="4"></select>
					<input type="hidden" id="writtenLanguages" name="writtenLanguages"
						value="" />

					<div id="written_error"></div>
					<div id="otherWrittenLanguage_error"></div>
					<div id="otherWrittenLang_error"></div>
				</div>
			</div>

			<div class="control-group">
				<label for="education" class="control-label"><spring:message
						code="label.assister.education" /><img
					src="<c:url value="/resources/img/requiredAsterisk.png" />"
					alt="Required!" /></label>
				<div class="controls">
					<select size="1" id="education" name="education"
						path="educationlist" class="input-large">
						<option value="">
							<spring:message code="label.assister.select" />
						</option>
						<c:forEach var="educationVar" items="${educationlist}">
							<option id="${educationVar}"
								<c:if test="${educationVar == assister.education}"> selected="selected"</c:if>
								value="<c:out value="${educationVar}" />">
								<c:out value="${educationVar}" />
							</option>
						</c:forEach>
					</select> <input type="hidden" id="education_hidden" name="education_hidden"
						value="${assister.education}">
					<div id="education_error"></div>
				</div>
			</div>

			<div class="form-actions noBackground">
	<a class="btn cancel-btn" href="<c:url value="/entity/assister/viewassisterprofilebyuserid?assisterId=${encAssisterId}&entityId=${encAssisterEntityId}" />">Cancel</a>

	<input type="submit" name="SaveAssister" id="SaveAssister"
					value="Save" class="btn btn-primary" />
			</div>

			<input type="hidden" name="mailingLocation.lat" id="lat"
				value="${assister.mailingLocation.lat != null ? assister.mailingLocation.lat : 0.0}" />
			<input type="hidden" name="mailingLocation.lon" id="lon"
				value="${assister.mailingLocation.lon != null ? assister.mailingLocation.lon : 0.0}" />
			<input type="hidden" name="mailingLocation.rdi" id="rdi"
				value="${assister.mailingLocation.rdi != null ? assister.mailingLocation.rdi : ''}" />
			<input type="hidden" name="mailingLocation.county" id="county"
				value="${assister.mailingLocation.county}" />
		</form>
	</div>
</div>

<script type="text/javascript">
function ie8Trim(){
	if(typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function() {
        	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
        };
	}
}	
$(function() {
	$("#SaveAssister").click(function(){
		ie8Trim();
		var spokenVal ="";
		var writtenVal="";
		$.each($("input[name='spoken']:checked"), function() {
			var spokenValcurrent = $(this).attr('value');
			if(spokenVal == ""){ 
				spokenVal=spokenValcurrent;
			} else {
				spokenVal=spokenVal+","+spokenValcurrent;
			}
		});
		$.each($("input[name='written']:checked"), function() {
			var  writtenValcurrent = $(this).attr('value');
			if(writtenVal == ""){ 
				writtenVal=writtenValcurrent;
			} else {
				writtenVal=writtenVal+","+writtenValcurrent;
			}
		});
		otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
		if(otherSpokenLanguage != "" &&  otherSpokenLanguage !=null){ 
			if(spokenVal != ""){ 
				spokenVal = spokenVal+","+otherSpokenLanguage;
			}
			else{ 
				spokenVal =otherSpokenLanguage;
			}
		}
		
		otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
		if(otherWrittenLanguage != "" &&  otherWrittenLanguage !=null){ 
			if(writtenVal != ""){ 
				writtenVal = writtenVal+","+otherWrittenLanguage;
			}
			else{ 
				writtenVal =otherWrittenLanguage;
			}
		}
		
		 $("#spokenLanguages").val(spokenVal);
		 $("#writtenLanguages").val(writtenVal);
		 //adding code for subsite id selected by enrollment entity representative during add assister operation 
		 var selectedSiteId=$('#secondarySite').val();
		 $("#secondaryAssisterSite_id").val(selectedSiteId);
	});
});


		$(function(){
			 $('#fileInput').change(function(){
					var rv = -1; // Return value assumes failure.
					 if (navigator.appName == 'Microsoft Internet Explorer')
					 {
					    var ua = navigator.userAgent;
					    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
					    if (re.exec(ua) != null)
					       rv = parseFloat( RegExp.$1 );
					 }
					 if(!(rv <= 9.0 && navigator.appName == 'Microsoft Internet Explorer')){
					 var f=this.files[0];
					if((f.size > 5242880)||(f.fileSize > 5242880)){
						$("#fileInput_Size").val(0);
					}
					else{
						$("#fileInput_Size").val(1);	
					} 
					}
					else{
						$("#fileInput_Size").val(1);	
					} 
		
				});
			});
		
		jQuery.validator.addMethod("sizeCheck", function(value, element, param) { 
			  ie8Trim();
			  if($("#fileInput_Size").val()==1){
				  return true;
			  }
			  else{
				  document.getElementById('fileInput').value=null;
				  $("#fileInput_Size").val(1);
				  return false;
			  }
		});
		
		jQuery.validator.addMethod("OtherSpokenLanguageCheck", function(value, element, param) {
			ie8Trim();
			var otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
			if(otherSpokenLanguage != ""){ 
				var languages='${languageNames}';
				languages=languages.replace("[","");
				languages=languages.replace("]","");
				languages=languages.replace(" ","");
				var languagesArray=languages.split(',');
				
				var found = false;
				for (i = 0; i < languagesArray.length && !found; i++) {
					languagesTocompare=languagesArray[i].trim();
				if (languagesTocompare.toLowerCase().match(otherSpokenLanguage.toLowerCase())) {			
					  found = true;
				  }
				}
				if(found){
					return false;
				}
				else
					{
						return true;
					}
			}
			else
			{
				if($("#otherSpokenLanguageCheckbox").attr("checked")=='checked' && otherSpokenLanguage =="")
				{					
						return false;
				}
				else
					{
						return true;
					}
			}	
		});
		jQuery.validator.addMethod("OtherWrittenLanguageCheck", function(value, element, param) {
			ie8Trim();
			var otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
			if(otherWrittenLanguage != ""){ 
				var languages='${languageWrittenNames}';
				languages=languages.replace("[","");
				languages=languages.replace("]","");
				languages=languages.replace(" ","");
				var languagesArray=languages.split(',');
				
				var found = false;
				for (i = 0; i < languagesArray.length && !found; i++) {
				languagesTocompare=languagesArray[i].trim();
				 if (languagesTocompare.toLowerCase().match(otherWrittenLanguage.toLowerCase())) {	
					  found = true;
				  }
				}  
				if(found){
					return false;
				}
				else
					{
						return true;
					}
			
			}
			else
			{	
				if($("#otherWrittenLanguageCheckbox").attr("checked")=='checked' && otherWrittenLanguage =="")
				{
					
					return false;
				}
				else
					{
						return true;
					}
			}
		});
		jQuery.validator.addMethod("SpokenLanguageCheck", function(value, element, param) {
			ie8Trim();
			var fields = $("input[name='spoken']").serializeArray(); 
			if (fields.length == 0) 
		   { 
		       otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
		   	if(otherSpokenLanguage == null || otherSpokenLanguage == ""){
		   		return false;
		   	}
		   } 
			return true;
		});
		
		jQuery.validator.addMethod("otherSpokenLanguageCheckboxCheck", function(value, element, param) {
			ie8Trim();
			
			otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
			if (otherSpokenLanguage != null ) 
		    { 
			    if((otherSpokenLanguage != null || otherSpokenLanguage != "") && !($("#otherLang").attr("checked")=='checked' )){
			    		return false;
			    	}
		    } 
			return true;
		});

		jQuery.validator.addMethod("otherSpokenLanguageSelectCheck", function(value, element, param) {
			ie8Trim();
			
			temp = $("#otherLang").attr("checked")=='checked';
			if (temp != false ) 
		    { 
			    if((otherSpokenLanguage == null || otherSpokenLanguage == "") && ($("#otherLang").attr("checked")=='checked' )){
			    		return false;
			    	}
		    } 
			return true;
		});
		
		jQuery.validator.addMethod("WrittenLanguageCheck", function(value, element, param) {
			ie8Trim();
			var fields = $("input[name='written']").serializeArray(); 
		   if (fields.length == 0) 
		   { 
		   
		      otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
		      if(otherWrittenLanguage  == null || otherWrittenLanguage == ""){ 
		   		return false;
		   	}
		   } 
			return true;
		});
			
		
		jQuery.validator.addMethod("otherWrittenLanguageCheckboxCheck", function(value, element, param) {
			ie8Trim();
			otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
			if (otherWrittenLanguage != null) 
		    { 
		    	if((otherWrittenLanguage != null || otherWrittenLanguage != "") && !($("#otherWrittenLang").attr("checked")=='checked' )){
		    			return false;
		    	}
		    } 
			return true;
		});

		jQuery.validator.addMethod("otherWrittenLanguageSelectCheck", function(value, element, param) {
			ie8Trim();
			
			temp = $("#otherWrittenLang").attr("checked")=='checked';
			if (temp != false ) 
		    { 
			    if((otherWrittenLanguage == null || otherWrittenLanguage == "") && ($("#otherWrittenLang").attr("checked")=='checked' )){
			    		return false;
			    	}
		    } 
			return true;
		});
		
		jQuery.validator.addMethod("PhotoUploadCheck", function(value, element, param) {
			ie8Trim();
		     var file = $('input[type="file"]').val();
		     var exts = ['jpg','jpeg','gif','png','bmp'];
		     if ( file ) {
		       var get_ext = file.split('.');
		       get_ext = get_ext.reverse();
		
		       if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
		         return true;
		       } else {
		         return false;
		       }
		    }
		    return true;
		});
		
		jQuery.validator.addMethod("languagesSpokenCheck", function(value, element, param) {
			if(value == '') {
				return true;
			}
			checkLanguageSpokenForSite();
			return $("#languagesSpokenCheck").val() == 1 ? false : true;
			});
			
		function checkLanguageSpokenForSite(){
			
			$.get('addnewassisterbyadmin/checkLanguagesSpokenForAsister',
			{otherSpokenLanguage: $("#otherSpokenLanguage").val()},
			            function(response){
			                    if(response == true){
			                            $("#languagesSpokenCheck").val(0);
										
			                    }else{
			                            $("#languagesSpokenCheck").val(1);
										
			                    }
			            }
			        );
			}
		
		jQuery.validator.addMethod("languagesWrittenCheck", function(value, element, param) {
			if(value == '') {
				return true;
			}
			checkLanguageWrittenForSite();
			return $("#languagesWrittenCheck").val() == 1 ? false : true;
			});
			
		function checkLanguageWrittenForSite(){
			
			$.get('addnewassisterbyadmin/checkLanguagesWrittenForAssister',
			{otherWrittenLanguage: $("#otherWrittenLanguage").val()},
			            function(response){
			                    if(response == true){
			                            $("#languagesWrittenCheck").val(0);
										
			                    }else{
			                            $("#languagesWrittenCheck").val(1);
										
			                    }
			            }
			        );
			}

</script>

<script type="text/javascript">
			
	var validator = $("#frmupdateassisterprofile").validate({ 
		onkeyup: false,
		onclick: false,
		rules : {
			firstName : {required : true},
			lastName : {required : true},		
			emailAddress : { required : true,email: true},
			primaryPhone1 : { numberStartsWithZeroCheck: true},
			primaryPhone3 : {primaryphonecheck : true},	
			"mailingLocation.address1" : { required: true},
			"mailingLocation.city" : { required: true},
			"mailingLocation.state" : { required: true},
			"mailingLocation.zip" : {required: true, MailingZipCodecheck: true},
			spoken : {SpokenLanguageCheck: true},
			written : {WrittenLanguageCheck: true},
			otherWrittenLanguage : {OtherWrittenLanguageCheck: true,languagesWrittenCheck : true},
			otherSpokenLanguage : {OtherSpokenLanguageCheck: true, languagesSpokenCheck: true},		
			fileInput : {PhotoUploadCheck: true, sizeCheck: true},
		 	education : { required: true},
		 	otherLang : {otherSpokenLanguageCheckboxCheck : true, otherSpokenLanguageSelectCheck : true},
		 	otherWrittenLang : {otherWrittenLanguageCheckboxCheck : true, otherWrittenLanguageSelectCheck : true}
		},
		messages : {
			firstName: { required : "<span> <em class='excl'>!</em><spring:message code='label.entityValidateFirstName' javaScriptEscape='true'/></span>"},
	         lastName: { required : "<span> <em class='excl'>!</em><spring:message code='label.entityValidateLastName' javaScriptEscape='true'/></span>"},         
			emailAddress: { required : "<span> <em class='excl'>!</em><spring:message code='label.entityValidatePleaseEnterValidEmail' javaScriptEscape='true'/></span>",
		    email : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidEmail'/></span>"},
		    primaryPhone1 : {numberStartsWithZeroCheck :  "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>" },
		    primaryPhone3: { primaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNo' javaScriptEscape='true'/></span>"},
		   "mailingLocation.address1" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"},
			"mailingLocation.city" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
			"mailingLocation.state" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateState' javaScriptEscape='true'/></span>"},
			"mailingLocation.zip" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateZip' javaScriptEscape='true'/></span>",
			MailingZipCodecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateZipSyntax' javaScriptEscape='true'/></span>"},
			spoken : {SpokenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSpokenLanguage' javaScriptEscape='true'/></span>"},
	 		written : {WrittenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateWrittenLanguage' javaScriptEscape='true'/></span>"},
	 		 otherWrittenLanguage : {OtherWrittenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateOtherWrittenLanguage' javaScriptEscape='true'/></span>",
	 			languagesWrittenCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterLanguageWrittenFromDropDown' javaScriptEscape='true'/></span>"},
	 		otherSpokenLanguage : {OtherSpokenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateOtherSpokenLanguage' javaScriptEscape='true'/></span>",
	 			languagesSpokenCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterLanguageSpokenFromDropDown' javaScriptEscape='true'/>.</span>"}, 		
	 		education : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEducation' javaScriptEscape='true'/></span>"},
	 		fileInput : {PhotoUploadCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePhoto' javaScriptEscape='true'/></span>",
	 			         sizeCheck : "<span> <em class='excl'>!</em><spring:message  code='label.brkvalidatePleaseSelectFileWithSizeLessThan5MB' javaScriptEscape='true'/></span>"},
	 			        otherLang : {otherSpokenLanguageCheckboxCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectothercheckbox'/></span>",
				otherSpokenLanguageSelectCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectlangforother'/></span>"},
				otherWrittenLang : {otherWrittenLanguageCheckboxCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectothercheckboxforwritten'/></span>",
				otherWrittenLanguageSelectCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectlangforotherwritten'/></span>"}
		}
		,
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		} 
	});
	
	jQuery.validator.addMethod("alphaNumeric", function(value, element, param) {
		ie8Trim();
		 return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
		 
	});

	jQuery.validator.addMethod("primaryphonecheck", function(value, element, param) {
		ie8Trim();
		primphone1 = $("#primaryPhone1").val().trim(); 
		primphone2 = $("#primaryPhone2").val().trim(); 
		primphone3 = $("#primaryPhone3").val().trim(); 
		var a=(/^[0-9]*$/.test(primphone1)&& /^[0-9]*$/.test(primphone2)&&/^[0-9]*$/.test(primphone3));
		if( (primphone1 == "" || primphone2 == "" || primphone3 == "")  || (primphone1 != parseInt(primphone1) || isNaN(primphone1)) || (primphone1.length < 3 ) || (primphone2 != parseInt(primphone2) || isNaN(primphone2)) || (primphone2.length < 3 ) || (primphone3 != parseInt(primphone3) || isNaN(primphone3)) || (primphone3.length < 4 )  )
		{
			return false;
		}
		if(!a){
			return false;
		}
		else
		{
			$("#primaryPhoneNumber").val(primphone1 + primphone2 + primphone3);
			return true;
		}	
	} );
	
	jQuery.validator.addMethod("MailingZipCodecheck", function(value, element, param) {
		ie8Trim();
		zip = $("#zip").val().trim(); 
		if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || (zip == "00000") )){ 
			return false; 
		}
		return true;
	});
	

	jQuery.validator.addMethod("numberStartsWithZeroCheck", function(value, element, param) {
		ie8Trim();
		if((value.length == 0)) {
			return true;
		}

		var firstChar = value.charAt(0);
		if(firstChar == 0) {
				return false;
		} else{
		    return true;
		}
		
	});
	
	var secret = $('.secret');
	secret.hide();
	$('input:radio[name="assisterCertified"]').change(function(){
	    if ($(this).is(':checked') && $(this).val() === 'certifiedYes') {
	       secret.fadeToggle();
	    }
	    else{
	      secret.hide();
	      }
	  });

	/*focus out event for the text input to call the lightbox*/
	var pre_address1, pre_address2, pre_city, pre_state, pre_zip;

	$('.zipCode').focusout(function(e) {
		if ($(this).val().length == 5){	
			
			var address1_e='address1'; var address2_e='address2'; var city_e= 'city'; var state_e='state'; var zip_e='zip';
			var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
			
			var model_address1 = address1_e + '_hidden' ;
			var model_address2 = address2_e + '_hidden' ;
			var model_city = city_e + '_hidden' ;
			var model_state = state_e + '_hidden' ;
			var model_zip = zip_e + '_hidden' ;
			
			var thisAddress1=$('#'+ address1_e).val();
			var thisAddress2=$('#'+ address2_e).val();
			var thisCity=$('#'+ city_e).val();
			var thisState=$('#'+ state_e).val();
			var thisZip=$('#'+ zip_e).val();
			
			var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
			if(($('#'+ address1_e).val() != "Street Name, P.O. Box, Company, c/o")&&($('#'+ city_e).val() != "City, Town")&&($('#'+ state_e).val() != "State")&&($('#'+ zip_e).val() != '')){
				if(($('#'+ address2_e).val())==="Apt, Suite, Unit, Bldg, Floor, etc"){
					$('#'+ address2_e).val('');
				}	
				if(pre_address1 != thisAddress1 || pre_address2 != thisAddress2 || pre_city != thisCity || pre_state != thisState || pre_zip != thisZip){
				viewValidAddressListNew(thisAddress1,thisAddress2,thisCity,thisState,thisZip, 
					$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
					idsText);
				pre_address1 = thisAddress1;
				pre_address2 = thisAddress2; 
				pre_city = thisCity;
				pre_state = thisState; 
				pre_zip = thisZip;
				}
			}
			
		}
	}); 

	$('#address1').focusin(function() {
		
				if(($('#address2').val())==="Address Line 2"){
					$('#address2').val('');
				}
			
			
		
	}); 

	function split(val) {
	    return val.split(/,\s*/);
	}
	function extractLast(term) {
	    return split(term).pop();
	}
		
	</script>

<script type="text/javascript">
 //load the jquery chosen plugin 
	function loadLanguages() {
		$("#otherSpokenLanguage").html('');
		var respData = $.parseJSON('${languagesList}');
		var counties='${otherSpokenLanguage}';
		for ( var key in respData) {
	    	var isSelected = false;
		     if(counties!=null){
			      isSelected = checkLanguages(respData[key]);
		      }
		      if(isSelected){
		    	  $('#otherSpokenLanguage').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#otherSpokenLanguage').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }
	     
	     $('#otherSpokenLanguage').trigger("liszt:updated");
	   }
	function checkLanguages(county){
	     var counties='${otherSpokenLanguage}';
	     var countiesArray=counties.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i];
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;
			    	
			     }
			 }
		   return found;
    }
	
	$(document).ready(function() {
		loadLanguages();
	});
	

</script>
<script type="text/javascript">
 //load the jquery chosen plugin 
	function loadLanguagesForWritten() {
		$("#otherWrittenLanguage").html('');
		var respData = $.parseJSON('${languagesList}');
		var counties='${otherWrittenLanguage}';
	    for ( var key in respData) {
	    	var isSelected = false;
		     if(counties!=null){
			      isSelected = checkLanguage(respData[key]);
		      }
		      if(isSelected){
		    	  $('#otherWrittenLanguage').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#otherWrittenLanguage').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }
	    $('#otherWrittenLanguage').trigger("liszt:updated");
	   
	   }
	function checkLanguage(county){
	     var counties='${otherWrittenLanguage}';
	     var countiesArray=counties.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i];
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;
			    	
			     }
			 }
		   return found;
    }
	
	$(document).ready(function() {
		loadLanguagesForWritten();
	});
	
</script>