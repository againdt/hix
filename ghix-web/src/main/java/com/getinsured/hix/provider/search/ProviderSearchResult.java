package com.getinsured.hix.provider.search;

import java.util.Iterator;
import java.util.List;

import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

public class ProviderSearchResult implements JSONAware{
	private long currentStart = 0;
	private int pageSize;
	private int nextStart;
	private List<SolrProviderRecord> pageRecords;
	private QueryResponse solrResponse;
	private long totalHits;
	private Object message;
	
	public ProviderSearchResult(QueryResponse solrResponse){
		this.solrResponse = solrResponse;
		this.totalHits = solrResponse.getResults().getNumFound();
		this.currentStart = solrResponse.getResults().getStart();
		this.pageSize = solrResponse.getResults().size();
	}
	
	public ProviderSearchResult(String message){
		this.solrResponse = null;
		this.totalHits = 0;
		this.currentStart = 0;
		this.pageSize = 0;
		this.message = message;
	}
	
	public long getCurrentStart() {
		return this.currentStart;
	}
	public void setCurrentStart(int currentStart) {
		this.currentStart = currentStart;
		this.nextStart = currentStart+this.solrResponse.getResults().size();
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getNextStart() {
		return nextStart;
	}
	public void setNextStart(int nextStart) {
		this.nextStart = nextStart;
	}
	public long getTotalHits() {
		return totalHits;
	}
	public void setTotalHits(long totalHits) {
		this.totalHits = totalHits;
	}
	public List<SolrProviderRecord> getPageRecords() {
		return pageRecords;
	}
	public void setPageRecords(List<SolrProviderRecord> pageRecords) {
		this.pageRecords = pageRecords;
	}
	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("total_hits", new Long(this.totalHits));
		obj.put("current_start", new Long(this.currentStart));
		obj.put("next_start",new Integer(this.nextStart));
		obj.put("page_size", new Integer(this.pageSize));
		if(this.solrResponse != null){
			FacetField field = solrResponse.getFacetField("languages_spoken");
			List<Count> y = field.getValues();
			JSONArray languages = new JSONArray(); 
			for(Count ct: y){
				languages.add(ct.getName());
			}
			obj.put("languages", languages);
			
			
			field = solrResponse.getFacetField("taxonomy_code");
			y = field.getValues();
			JSONArray taxonomyCodes = new JSONArray(); 
			for(Count ct: y){
				taxonomyCodes.add(ct.getName());
			}
			
			obj.put("taxonomyCodes", taxonomyCodes);
			
			JSONArray specialtyCodes = new JSONArray(); 
			field = solrResponse.getFacetField("specialty_code");
			y = field.getValues();
			
			for(Count ct: y){
				if(!ct.getName().trim().contains("Not Available"));
				{
					specialtyCodes.add(ct.getName());
				}
			}
			
			obj.put("specialtyCodes", specialtyCodes);
			
			Iterator<SolrDocument> cursor = solrResponse.getResults().iterator();
			SolrDocument tmpProvider = null;
			JSONArray providers = new JSONArray();
			while(cursor.hasNext()){
				tmpProvider = cursor.next();
				Provider provider = new Provider(tmpProvider);
				providers.add(provider.getNewProviderObj());
			}
			obj.put("providers", providers);
		}
		if(this.message != null){
			obj.put("message", this.message);
		}
		return obj.toJSONString();
	}
}
