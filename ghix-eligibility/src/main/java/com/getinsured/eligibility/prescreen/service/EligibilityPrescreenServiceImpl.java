/**
 * 
 */
package com.getinsured.eligibility.prescreen.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.prescreen.calculator.CsrCalculator;
import com.getinsured.eligibility.prescreen.calculator.FplCalculator;
import com.getinsured.eligibility.prescreen.repository.IPrescreenDataRepository;
import com.getinsured.hix.model.prescreen.PrescreenData;
import com.getinsured.hix.model.prescreen.calculator.FplRequest;
import com.getinsured.hix.model.prescreen.calculator.FplResponse;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenDTO;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenRequest;

/**
 * Service Implementation for Eligibility Prescreen Services
 * 
 * @author Nikhil Talreja
 * @since  April 19, 2013
 */
@Service("eligibilityPrescreenService")
public class EligibilityPrescreenServiceImpl implements EligibilityPrescreenService {
	
	private static final Logger LOGGER = Logger.getLogger(EligibilityPrescreenServiceImpl.class);
	
	//@Autowired private AptcCalculator aptcCalculator;
	@Autowired private CsrCalculator csrCalculator;
	//@Autowired private MagiCalculator magiCalculator;
	@Autowired private FplCalculator fplCalculator;
	//@Autowired private AptcProductCalculator productAptcCalculator;
	
	@Autowired private IPrescreenDataRepository iPrescreenDataRepository;
	
	@Override
	public PrescreenRequest calculateAptc(PrescreenRequest aptcRequest) {
		LOGGER.info("Calculating APTC");
		//return aptcCalculator.calculateAptc(aptcRequest);
		return null;
	}

	@Override
	public String computeCSR(double fpl) {
		LOGGER.info("Computing CSR");
		return csrCalculator.computeCSR(fpl);
	}

	@Override
	public double calculateMagi(PrescreenRequest prescreenRequest) {
		LOGGER.info("Calculating MAGI for household");
		//return magiCalculator.calculateMagi(prescreenRequest);
		return 0.0;
	}
	
	@Override
	public FplResponse calculateFpl(FplRequest fplRequest) {
		LOGGER.info("Calculating FPL");
		return fplCalculator.calculateFpl(fplRequest,false,0);
	}
	
	@Override
	public PrescreenData savePrescreenRequestRecord(PrescreenData record) {
		return iPrescreenDataRepository.save(record);	
	}

	@Override
	public PrescreenData getPrescreenRequestRecord(long id) {
		return iPrescreenDataRepository.findOne(id);
	}

	@Override
	public PrescreenData getPrescreenRecordBySessionId(String sessionId){
		return iPrescreenDataRepository.findBySessionId(sessionId);
	}

	@Override
	public PrescreenDTO getAptc(PrescreenDTO aptcRequest) {
		LOGGER.info("Calculating APTC-Product");
		//return productAptcCalculator.getAptc(aptcRequest);
		return null;
	}
}
