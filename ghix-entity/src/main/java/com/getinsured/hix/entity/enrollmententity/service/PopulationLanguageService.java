package com.getinsured.hix.entity.enrollmententity.service;

import java.util.List;

import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.PopulationLanguage;

/**
 * Encapsulates service layer method calls for languages served by EE
 */
public interface PopulationLanguageService {

	/**
	 * Retrieves {@link PopulationLanguage} based on id
	 * 
	 * @param languageId
	 *            language id
	 * @return {@link PopulationLanguage}
	 */
	PopulationLanguage findById(int languageId);

	/**
	 * Retrives {@link List} of {@link PopulationLanguage}
	 * 
	 * @param entityId
	 *            EE Id
	 * @return {@link List} of {@link PopulationLanguage}
	 */
	List<PopulationLanguage> findByEntityId(int entityId);

	/**
	 * Removes languages associated with EE
	 * 
	 * @param entityId
	 *            EE Id
	 * @return Number of records deleted
	 */
	int deleteAssociatedLanguages(int entityId);

	/**
	 * Saves languages associated with EE
	 * 
	 * @param enrollmentEntity
	 *            {@link EnrollmentEntity}
	 * @param populationLanguages
	 *            {@link List} of {@link PopulationLanguage}
	 */
	void saveAssociatedLanguages(EnrollmentEntity enrollmentEntity,
			List<PopulationLanguage> populationLanguages);
}
