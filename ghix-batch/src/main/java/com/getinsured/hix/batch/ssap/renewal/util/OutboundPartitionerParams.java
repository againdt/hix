package com.getinsured.hix.batch.ssap.renewal.util;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Component;

/**
 * DTO class is used to add Outbound Job parameters.
 * 
 * @since September 12, 2019
 */
@Component("outboundPartitionerParams")
public class OutboundPartitionerParams {

	private List<Long> renewalApplicationIdList = null;
	private AtomicLong renewalYear;

	public OutboundPartitionerParams() {
		this.renewalApplicationIdList = new CopyOnWriteArrayList<Long>();
	}

	public List<Long> getRenewalApplicationIdList() {
		return renewalApplicationIdList;
	}

	public synchronized void clearRenewalApplicationIdList() {
		this.renewalApplicationIdList.clear();
	}

	public synchronized void addAllToRenewalApplicationIdList(List<Long> renewalApplicationList) {
		this.renewalApplicationIdList.addAll(renewalApplicationList);
	}

	public synchronized void addToRenewalApplicationIdList(Long renewalApplicationId) {
		this.renewalApplicationIdList.add(renewalApplicationId);
	}

	public AtomicLong getRenewalYear() {
		return renewalYear;
	}

	public void setRenewalYear(long renewalYear) {
		this.renewalYear = new AtomicLong(renewalYear);
	}
}
