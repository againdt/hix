package com.getinsured.eligibility.ssap.validation.service;


import java.util.List;

import org.springframework.security.access.AccessDeniedException;

import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.iex.dto.QleRequestDto;
import com.getinsured.iex.dto.QleResponeDto;

public interface QleValidationService {
	
	public QleResponeDto retrieveEventDataByApplicationId(String caseNumber,long moduleId,String gated);
	/**
	 * This method saves provided fileContents on ECM server(file-content-server) and returns ECM Document id of saved file. 
	 *
	 * @param relativePath (Path on ECM/File server, under which provided file is being saved.)
	 * @param fileName (Name of document/file being saved.)
	 * @param fileContents (Contents of file)
	 * @param fileCategory (Like Verify SSN, Verify Incarceration, Document Verification, Verify Citizenship, Verify Death, Authorized Representative Authorization, Verify Lawful Presence)
	 * @param fileSubCategory
	 * @param fileType (Like  Social Security Card, Certificate of Naturalization, Driver's License, Death Certificate, Hospital Birth Record, Authorized Representative, Passport, Other Birth Records, Federal or State Census Records showing U.S. Birth, School Records)
	 * @return String(ECM Document id of saved file)
	 *  If this method returns null, then it may be due to exception occurred during save operation.
	 * @throws ContentManagementServiceException 
	 */
	public String saveValidationDocument(String relativePath,String fileName, byte[] fileContents, String fileCategory, String fileSubCategory,String fileType) throws ContentManagementServiceException;
	/**
	 * This method creates new record in table CMR_DOCUMENTS for saved document. 
	 * @param ecmDocumentId (Id Returned by ECM/File server while saving file/document.)
	 * @param documentCategory (Like Verify SSN, Verify Incarceration, Document Verification, Verify Citizenship, Verify Death, Authorized Representative Authorization, Verify Lawful Presence)
	 * @param documentName (File/Document name)
	 * @param documentType (Like  Social Security Card, Certificate of Naturalization, Driver's License, Death Certificate, Hospital Birth Record, Authorized Representative, Passport, Other Birth Records, Federal or State Census Records showing U.S. Birth, School Records)
	 * @param targetId (Id of related record in targetName table )
	 * @param targetName (Table name whose record is related this record)
	 * @param createdBy (user id by which this record has been created)
	 * @return
	 */
	public Long saveDocumentRecord(String ecmDocumentId,String documentCategory,String documentName,String documentType,Long targetId,String targetName,Long createdBy);
	/**
	 * This method creates ticket for uploaded document so admin/authorize user can verify document.
	 * @param ticketSubject 
	 * @param ticketComment 
	 * @param ticketCategory (Like QLE)
	 * @param ticketType (Like Qle Validation)
	 * @param ecmDocumentId (Id Returned by ECM/File server while saving file/document.)
	 * @param consumerDocumentId (Id of document in CMR_DOCUMENTS table )
	 * @param caseNumber (case number of application filed.)
	 * @param moduleName (Module name from which ticket is being raised)
	 * @param userId 
	 * @param userModuleId
	 * @param userRoleId
	 * @param applicantEventId
	 * 
	 * @return
	 */
	public TkmTickets createTicket(QleRequestDto qleRequestDto ,String ecmDocumentId,Long consumerDocumentId );
	
	List<ConsumerDocument> getUploadedDocByTargetNameAndTargetId(String targetName,long targetId) ;
	public String retrieveDocumentListForEvent(Integer sepEventId);
	
	/**
	 * This method process QLE admin override.
	 * @param qleRequestDto request object
	 * @return 200 - Successful Operation, 500 - Exception/Failure scenario.
	 */
	public String adminQleOverride(QleRequestDto qleRequestDto);
	
	
	public boolean triggerQleValidation(String caseNumber,Integer userId);
	void processQleForSepDenial(String caseNumber, Integer userId);
	
	/***
	 * 
	 * @param qleRequestDto
	 * @throws AccessDeniedException
	 */
	public void validateRequest(QleRequestDto qleRequestDto) throws AccessDeniedException;
	
	
}
