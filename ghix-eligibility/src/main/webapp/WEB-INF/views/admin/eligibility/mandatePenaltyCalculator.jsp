
<%@page import="com.getinsured.eligibility.prescreen.calculator.MandatePenaltyCalculator"%>
<%@page import="com.getinsured.hix.model.estimator.mini.MedicaidResponse"%>
<%@page import="com.getinsured.hix.model.estimator.mini.MedicaidRequest"%>
<%@page import="com.getinsured.hix.model.estimator.mini.FamilyMember"%>
<%@page import="com.getinsured.eligibility.prescreen.calculator.MedicaidCalculator"%>
<%@include file="datasource.jsp"%>

<html>
<head>
<title>Mandate Penalty calculator</title>


<script type="text/javascript">
	
	function validate() {
		
		var year = document.getElementById('year').value;

		if (!year || isNaN(parseInt(year)) || parseInt(year) < 2014) {
			alert('Coverage year should be 2014 or after');
			return false;
		}
		
		var adults = document.getElementById('adults').value;

		if (!adults || isNaN(parseInt(adults))) {
			alert('Invalid No. of adults');
			return false;
		}
		
		var children = document.getElementById('children').value;

		if (!children || isNaN(parseInt(children))) {
			alert('Invalid No. of children');
			return false;
		}
		
		var income = document.getElementById('income').value;

		if (!income || isNaN(income)) {
			alert('Invalid income');
			return false;
		}

		

	}
</script>
<style>
table {
	border: 1px solid #666;
	width: 80%;
	margin: 20px 0 20px 0 !important;
	font-size: 14px;
}

th,td {
	padding: 2px 4px 2px 4px !important;
	text-align: left;
	vertical-align: top;	
}

thead tr {
	background-color: #fc0;
}

th.sorted {
	background-color: orange;
}

th a,th a:visited {
	color: black;
}

th a:hover {
	text-decoration: underline;
	color: black;
}

th.sorted a,th.sortable a {
	background-position: right;
	display: block;
	width: 100%;
}
tr.odd {
	background-color: #fff
}

tr.tableRowEven,tr.even {
	background-color: #fea
}

div.exportlinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	margin: 2px 0 10px 0;
	width: 79%;
}

span.export {
	padding: 0 4px 1px 20px;
	display: inline;
	display: inline-block;
	cursor: pointer;
}

span.pagebanner {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	margin-top: 10px;
	display: block;
	border-bottom: none;
}

span.pagelinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	display: block;
	border-top: none;
	margin-bottom: -5px;
}


.group-1 {
    font-weight:bold;
    padding-bottom:10px;
    border-top:1px solid black;
}
.group-2 {
    font-style:italic;
    border-top: 1px solid black;

}
.subtotal-sum, .grandtotal-sum {
    font-weight:bold;
    text-align:right;
}
.subtotal-header {
    padding-bottom: 0px;
    border-top: 1px solid white;
}
.subtotal-label, .grandtotal-label {
    border-top: 1px solid white;
    font-weight: bold;
}
.grouped-table tr.even {
    background-color: #fff;
}
.grouped-table tr.odd {
    background-color: #fff;
}
.grandtotal-row {
    border-top: 2px solid black;
}
</style>
</head>
<body>
	<b>Mandate Penalty Calculator</b><br><br>
	
	<form action="mandatePenaltyCalculator" method="POST" onsubmit="return(validate());">
		<table border="0" cellspacing="10">
			<tr>
				<td>Year of coverage:</td> 
				<td><input id="year" name="year" type="text" size="5" maxlength="4" value=2014></td>
			</tr>
			<tr>
				<td>No. of Adults (Age > 18):</td> 
				<td><input id="adults" name="adults" type="text" size="5" maxlength="2" value=0></td>
			</tr>
			<tr>	
				<td>No. of Children (Age < 19): </td>
				<td><input id="children" name="children" type="text" size="5" maxlength="2" value=0></td>
			</tr>
			<tr>	
				<td>Household Income : </td>
				<td><input id="income" name="income" type="text" size="10" maxlength="9" value=0.0></td>
			</tr>
			<tr>
				<td><input type="submit" value="Calculate"></td>
			</tr>
		</table>
	</form>
	
</body>

<%
	MandatePenaltyCalculator calculator = null;
%>

<%
	
	if(request.getParameter("year") != null && request.getParameter("year").trim().length() > 0 
		&& request.getParameter("adults") != null && request.getParameter("adults").trim().length() > 0
		&& request.getParameter("children") != null && request.getParameter("children").trim().length() > 0
		&& request.getParameter("income") != null && request.getParameter("income").trim().length() > 0){

		calculator = (MandatePenaltyCalculator) ctx.getBean("mandatePenaltyCalculator");
		
		int year = Integer.parseInt(request.getParameter("year"));
		int adults = Integer.parseInt(request.getParameter("adults"));
		int children = Integer.parseInt(request.getParameter("children"));
		double hhIncome = Double.parseDouble(request.getParameter("income"));
		
		out.println("<br><b>Coverage year : "+year+"</b>");
		out.println("<br><b>No. of adults : "+adults+"</b>");
		out.println("<br><b>No. of Children : "+children+"</b>");
		out.println("<br><b>Household Income : "+hhIncome+"</b>");
		out.println("<br><b>Mandate Penalty for household : "+calculator.calculateMandatePenalty(year,adults, children, hhIncome)+"</b>");
	}
%>

</html>