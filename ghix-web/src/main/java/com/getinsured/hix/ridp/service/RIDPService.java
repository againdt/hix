package com.getinsured.hix.ridp.service;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.ridp.exception.RIDPServiceException;


public interface RIDPService {
	String validatePrimaryDetails(String primaryDetailData) throws RIDPServiceException;
	String validateSecondaryDetails(String secondaryDetailData) throws RIDPServiceException;
	String currentUserInfo() throws RIDPServiceException;
	String handleException(Throwable exception);
	String validateFARS(String jsonRequest) throws RIDPServiceException;
	String clearDHSReferenceNumber() throws RIDPServiceException;
	String manualVerificationFileUpload(CommonsMultipartFile file, String docType) throws RIDPServiceException;
	String csrOverrideForPaperIdentification() throws RIDPServiceException;
	String pushToManualVerification() throws RIDPServiceException;
	String sendRidpVerificationToWso2(AccountUser user) throws RIDPServiceException;
	Boolean isRidpRequired() throws RIDPServiceException;
}
