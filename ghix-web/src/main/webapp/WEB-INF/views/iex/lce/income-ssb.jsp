<script type="text/ng-template" id="income-ssb.jsp">
  <div class="gutter10 jobIncome" ng-controller="incomeSSB" >
    <h3><spring:message code="label.lce.ssbincome"/></h3>

    <form class="form-horizontal" id="socialSecurityBenifitIncomeForm" name="socialSecurityBenifitIncomeForm" novalidate>
      <spring:message code="label.lce.howmuchdoes"/> {{householdMember.name.firstName}} {{householdMember.name.lastName}} <spring:message code="label.lce.getfromssb"/>
      <div class="control-group margin10-t">
        <label class="control-label" for="typeOfRequest"></label>
        <div class="controls margin10-r">
          <div class="input-prepend">
            <span class="add-on">$</span>
            <input type="text" class="span4 margin10-r" name="socialSecurityBenifitIncome" ng-model="householdMember.detailedIncome.socialSecurityBenefit.benefitAmount" required numbers-Only />
            <select class="span5" name="socialSecurityIncomeFrquency" ng-model="householdMember.detailedIncome.socialSecurityBenefit.incomeFrequency" required >
              <option><spring:message code="label.lce.weekly"/>Weekly</option>
			        <option><spring:message code="label.lce.biweekly"/>Bi-Weekly</option>
			        <option><spring:message code="label.lce.monthly"/>Monthly</option>
			        <option><spring:message code="label.lce.bimonthly"/>Bi-Monthly</option>
			        <option><spring:message code="label.lce.yearly"/>Yearly</option>
			        <option><spring:message code="label.lce.onceonly"/>Once Only</option>
            </select>
          </div>
        </div>
        <div class="controls margin5-t">
          <span class="validation-error-text-container" ng-show="submitted && socialSecurityBenifitIncomeForm.socialSecurityBenifitIncome.$error.required"><spring:message code="label.lce.incomessb.error1"/></span>
        </div>
        <div class="controls margin5-t">
          <span class="validation-error-text-container" ng-show="submitted && socialSecurityBenifitIncomeForm.socialSecurityIncomeFrquency.$error.required"><spring:message code="label.lce.incomessb.error2"/></span>
        </div>
      </div>
    </form>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a ng-show="socialSecurityBenifitIncomeForm.$valid" class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
      <a ng-show="!socialSecurityBenifitIncomeForm.$valid" class="btn btn-primary" ng-click="submitted=true"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>