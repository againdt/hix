package com.getinsured.hix.planmgmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.ExchgControlNumer;

public interface IExchgControlNumerRepository extends JpaRepository<ExchgControlNumer, Integer> {
	@Query("SELECT controlnumber FROM ExchgControlNumer WHERE hiosIssuerId = :hiosIssuerId")
	Integer getControlNumber(@Param("hiosIssuerId") String hiosIssuerId);
}
