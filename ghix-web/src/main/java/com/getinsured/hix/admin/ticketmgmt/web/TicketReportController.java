package com.getinsured.hix.admin.ticketmgmt.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.getinsured.hix.dto.tkm.TkmPendingReportDto;
import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.util.RestfulResponseException;
import com.getinsured.hix.util.TicketMgmtUtils;

@Controller
public class TicketReportController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TicketReportController.class);
	
	private static final String TICKET_QUEUE = "queueName";
	private static final String OPEN_TICKETS = "openCount";
	private static final String DUE_IN = "dueIn";
	private static final String SORT_ORDER = "sortOrder";
	private static final String DESC = "DESC";
	private static final String FALSE = "false";
	private static final String TRUE = "true";
	private static final int LISTING_NUMBER = 10;
	private static final String ASSIGNEE = "assignee";
	
	@Autowired private TicketMgmtUtils ticketMgmtUtils;
		
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/ticketmgmt/ticket/pendingreport")
	@PreAuthorize("hasPermission(#model, 'TKM_REPORT')")
	public String getPendingTicketDetails(Model model, HttpServletRequest request) throws InvalidUserException {
		LOGGER.info("In TicketReportController::getPendingTicketDetails");
		Map<String, Object> searchCriteria = createSearchCriteriaForPendingRec(request);
		List<TkmPendingReportDto> ticketDetailList = null;
		List<TkmQueues> tkmQueues = null;
		try{
			Map<String, Object> resultMap = ticketMgmtUtils.getPendingTicketDetails(searchCriteria);
			ticketDetailList =  (List<TkmPendingReportDto>)resultMap.get("ticketDetailList");
			tkmQueues = ticketMgmtUtils.getQueuesList();
		}
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
				case INTERNAL_SERVER_ERROR:
					LOGGER.error("Internal server error in retrieving pending ticket list.",ex.getMessage());	
					break;
				default:
					LOGGER.error("Unknown error in retrieving pending ticket list", ex.getMessage());
					break;
				}
		}
		catch(Exception e){
			LOGGER.error("Unknown error in retrieving pending ticket list", e.getMessage());
		}
		
		model.addAttribute("detailList", ticketDetailList);
		model.addAttribute("recordSize", (ticketDetailList !=null) ? ticketDetailList.size() : 0 );
		model.addAttribute("duein",TkmPendingReportDto.DUEIN.values());
		model.addAttribute(TICKET_QUEUE, tkmQueues);
		model.addAttribute("searchCriteria", searchCriteria);
		
		
		
		return "ticketmgmt/ticket/pendingreport";
	}
	
	@RequestMapping(value = "/ticketmgmt/ticket/pendingdrilldown")
	@PreAuthorize("hasPermission(#model, 'TKM_LIST_TICKET')")
	public String showPendingDrilldown(Model model, HttpServletRequest request, 
			@PathVariable("colIndex") String colIndex, 
			@PathVariable("dto") TkmPendingReportDto dto, 
			@PathVariable("searchCriteria") Map<String, Object> searchCriteria) {
		LOGGER.info("In TicketReportController::showPendingDrilldown");
		
		Map<String, Object> mapTicketSearch = new HashMap<>();
		
		if(dto.getAssigneeId() == 0) { // For Unassigned tickets assignee id will be 0
			mapTicketSearch.put("ticketStatus", TkmTickets.ticket_status.UnClaimed.toString());
		} else {
			mapTicketSearch.put("ticketStatus", TkmTickets.ticket_status.UnClaimed.toString() + " " + TkmTickets.ticket_status.Open.toString());
		}
		
		
		return "ticketmgmt/ticket/pendingdrilldown";
	}

	private Map<String, Object> createSearchCriteriaForPendingRec(HttpServletRequest request) {

		HashMap<String, Object> searchCriteria = new HashMap<String, Object>();

		String sortBy = request.getParameter("sortBy");
		sortBy = XssHelper.stripXSS(sortBy);
		sortBy = StringUtils.isEmpty(sortBy) ? "created" : sortBy;

		searchCriteria.put("sortBy", sortBy);

		request.removeAttribute("changeOrder");
		
		String sortOrder=getSortOrder(request);
		
		searchCriteria.put(SORT_ORDER, sortOrder);
		
	
		// filter by Queue
		filterByQueue(searchCriteria,request);
	
		
		// filter by Open Tickets
		filterByOpenTickets(searchCriteria,request);
			
		// filter by Due In
		filterBydueIn(searchCriteria, request);
	
		
		// filter by Assignee
		filterByAssignee(searchCriteria,request);
		
		String strPageNum = request.getParameter("pageNumber");
		int iPageNum = StringUtils.isEmpty(strPageNum) ? 0 : (Integer
				.parseInt(strPageNum) - 1);
		int startRecord = iPageNum * LISTING_NUMBER;

		searchCriteria.put("startRecord", startRecord);
		searchCriteria.put("pageSize", LISTING_NUMBER);
	
	

		return searchCriteria;
	
	}
	
	private String getSortOrder(HttpServletRequest request) {
		String sortOrder = request.getParameter(SORT_ORDER);
		sortOrder = XssHelper.stripXSS(sortOrder);
		sortOrder = StringUtils.isEmpty(sortOrder) ? DESC : sortOrder;

		String changeOrder = request.getParameter("changeOrder");
		changeOrder = XssHelper.stripXSS(changeOrder);
		changeOrder = StringUtils.isEmpty(changeOrder) ? FALSE : changeOrder;

		if (changeOrder.equals(TRUE)) {
			sortOrder = (sortOrder.equals(DESC)) ? "ASC" : DESC;
		}
		
		return sortOrder;
	}
	
	private void filterByQueue(Map<String, Object> searchCriteria,
			HttpServletRequest request) {
		String selectedTicketQueue = request.getParameter(TICKET_QUEUE);
		if (!StringUtils.isEmpty(selectedTicketQueue)) {
			searchCriteria.put(TICKET_QUEUE, XssHelper.stripXSS(selectedTicketQueue));
		}
	}
	
	  private void filterByAssignee(Map<String, Object> searchCriteria,
				HttpServletRequest request) {
			String selectedAssignee = request.getParameter("assigneeId");
			String selectedAssigneeName = request.getParameter(ASSIGNEE);
			selectedAssignee = XssHelper.stripXSS(selectedAssignee);
			selectedAssigneeName = XssHelper.stripXSS(selectedAssigneeName);
			if(!StringUtils.isEmpty(selectedAssignee) && !StringUtils.isEmpty(selectedAssigneeName)){
				 searchCriteria.put("assigneeId", selectedAssignee);
			 }
			searchCriteria.put(ASSIGNEE, selectedAssigneeName);
			
		}
	  
	  
	  private void filterByOpenTickets(Map<String, Object> searchCriteria,
				HttpServletRequest request) {
			String selectedOpenTickets = request.getParameter(OPEN_TICKETS);
			if (!StringUtils.isEmpty(selectedOpenTickets)) {
				//String[] split=selectedOpenTickets.split("-");
				searchCriteria.put(OPEN_TICKETS, XssHelper.stripXSS(selectedOpenTickets));
				//searchCriteria.put("startCnt", XssHelper.stripXSS(split[0]));
				//searchCriteria.put("endCnt", XssHelper.stripXSS(split[1]));
			}
		}
	  
	  
	  private void filterBydueIn(Map<String, Object> searchCriteria,
				HttpServletRequest request) {
			String selectedDueIn = request.getParameter(DUE_IN);
			if (!StringUtils.isEmpty(selectedDueIn)) {
				searchCriteria.put(DUE_IN, XssHelper.stripXSS(selectedDueIn));
			}
		}
}
