<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="gutter10">
	<h1>Unlock Your Account</h1>
	<%-- <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
		<c:if test="${errorMsg != ''}">
			<p><c:out value="${errorMsg}"></c:out><p/>
		</c:if>
			<br>
		</div>
	</div> --%>
	<div class="row-fluid">
	
		<div class="span3" id="sidebar">
			<div class="header">
				<h4 class="margin0">You should know</h4>
			</div>
			<ul class="nav nav-list">
			    <li>To unlock your account you will need your email, answers to security questions and certain miscellaneous personal information.</li>
		    </ul>
		</div>
		
		<div class="span9" id="rightpanel">			
		
			<div class="header">
				<h4>Step 4: Verify personal information</h4>
			</div>
			
			<!-- <div class="row-fluid  margin5-t marginL5"  id="member-portal">		  	
				<div class="well-step">
					<div class="row-fluid">
						<div id="step-progress">
							<div id="progress-bar">
								<ul>
									<li id="step-one" class="step-desc active-state step5"><span class="s-no">1</span></li>
									<li id="step-two" class="step-desc active-state step5"><span class="s-no">2</span></li>
									<li id="step-three" class="step-desc active-state step5"><span class="s-no">3</span></li>
									<li id="step-four" class="step-desc active-state step5"><span class="s-no">4</span></li>
									<li id="step-five" class="step-desc inactive-state step5"><span class="s-no">5</span></li>
								</ul>
							</div>
							/End Progress
						</div>
						/End step-progress
					</div>
					/End row-fluid
				</div>
				End .well-step
			</div> -->
			
			<form class="form-horizontal gutter10" id="personalInfoForm" action="<c:url value="/account/unlock/information"/>" method="POST">
		
				<df:csrfToken/>
				<div class="control-group">					
					<label for="zipCode" class="control-label">Your primary zip code</label>
						<div class="controls">
							<input type="text"  id="zipCode" name="zipCode">
							<div class="error help-inline" id="zipCode_error"></div>							
						</div>
				</div>	
				<c:set var="userRole" value="${userRole}"></c:set>
				<c:out value="${user.getDefRole().getName()}"></c:out>
				<c:if test="${fn:containsIgnoreCase(userRole, 'EMPLOYEE') or fn:containsIgnoreCase(userRole, 'INDIVIDUAL') }">
					<div class="control-group">
						<label for="ssn" class="control-label">Last four digits of SSN</label>
							<div class="controls">
								<input type="text" id="ssn" name="ssn" maxlength="4">
								<div class="error help-inline" id="ssn_error"></div>														
							</div>
					</div>
				</c:if>
				
				<c:if test="${fn:containsIgnoreCase(userRole, 'BROKER') or fn:containsIgnoreCase(userRole, 'EMPLOYER') }">
					<div class="control-group">
						<label for="taxId" class="control-label">Federal Tax ID</label>
							<div class="controls">
								<input type="text"  id="taxId" name="taxId" maxlength="9">	
								<div class="error help-inline" id="taxId_error"></div>													
							</div>
					</div>
				</c:if>
				
				<div class="control-group">
					<div class="controls">							
						<input type="submit" value="<spring:message  code='label.brkContinue'/>" class="btn btn-primary btn-large" title="<spring:message  code='label.brkContinue'/>">
						
						<c:if test="${!empty errorMsg}">
							<div class="alert alert-error alert-error-custom">
								<c:out value="${errorMsg}"></c:out>
							</div>
						</c:if>
						
					</div>
				</div>		
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">

	jQuery.validator.addMethod("ZipCodeCheck", function(value, element, param) {
		if(value == '00000'){ 
			return false; 
		}
		return true;
	});
	
    jQuery.validator.addMethod("validatepassword", function(value, element, param) {
        password = $("#password").val();

        //var re = /^[A-Za-z0-9!@#$%^&*()_]{8,20}$/;
        //var re = /(?=^.{8,20}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=^.*[^\s].*$).*$/; 
        //var re = /(?=^.{8,20}$)(?=.*[a-z])(?=.*[0-9])(?=^.*[^\s].*$).*$/; 
        var re = /(?=^.{8,20}$)(?=.*[a-zA-Z])(?=.*[0-9])(?!.*\s).*$/;

        if (!re.test(password)) {
               return false;
        }
        return true;

	 });

	var validator = $("#personalInfoForm").validate({ 
		rules : {
			zipCode: {required : true,
				      number : true,
				      minlength : 5,
				      maxlength : 5,
				      ZipCodeCheck : true},
			ssn: {required : true,
				  number : true,
				  minlength : 4,
			      maxlength : 4,
				  },
			taxId: {required : true,
				number : true,
				  minlength : 9,
			      maxlength : 9,}
		},
		messages : {
			zipCode: { 
				required : "<span> <em class='excl'>!</em>Zip Code is required.</span>",
				number:"<span> <em class='excl'>!</em>Please Enter Valid Zip Code.</span>",
				minlength:"<span> <em class='excl'>!</em>Please Enter Valid Zip Code.</span>",
				maxlength:"<span> <em class='excl'>!</em>Please Enter Valid Zip Code.</span>",
				ZipCodeCheck:"<span> <em class='excl'>!</em>Please Enter Valid Zip Code.</span>",
				},
			ssn: {
				required : "<span> <em class='excl'>!</em>Last four digits of SSN are required.</span>",
				number:"<span> <em class='excl'>!</em>Please Enter Last four digits of SSN</span>",
				minlength:"<span> <em class='excl'>!</em>Please Enter Last four digits of SSN</span>",
				maxlength:"<span> <em class='excl'>!</em>Please Enter Last four digits of SSN</span>",
			},
			taxId:{
				required : "<span> <em class='excl'>!</em>Federal Tax ID is required.</span>",
				number:"<span> <em class='excl'>!</em>Federal Tax ID should numeric.</span>",
				minlength:"<span> <em class='excl'>!</em>Federal Tax ID should 9 digits numeric.</span>",
				maxlength:"<span> <em class='excl'>!</em>Federal Tax ID should 9 digits numeric.</span>",
			}
		},
		onkeyup: false,
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		}
	});
</script>
