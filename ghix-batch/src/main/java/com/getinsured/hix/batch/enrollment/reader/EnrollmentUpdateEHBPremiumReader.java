package com.getinsured.hix.batch.enrollment.reader;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.enrollment.skip.EnrollmentEHBUpdation;

public class EnrollmentUpdateEHBPremiumReader implements ItemReader<Integer>{
	
	int partition;
	int loopCount;
	int startIndex;
	int endIndex;
	private EnrollmentEHBUpdation enrollmentEHBUpdation;
	private List<Integer> enrollmentIdList;
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		ExecutionContext ec = stepExecution.getExecutionContext();
		if(ec != null){
			partition =ec.getInt("partition");
			startIndex=ec.getInt("startIndex");
			endIndex=ec.getInt("endIndex");
			if(enrollmentEHBUpdation!=null && enrollmentEHBUpdation.getEnrollmentIdList()!=null && !enrollmentEHBUpdation.getEnrollmentIdList().isEmpty()) {
				if(startIndex<enrollmentEHBUpdation.getEnrollmentIdList().size() && endIndex<=enrollmentEHBUpdation.getEnrollmentIdList().size()){
					enrollmentIdList= new ArrayList<Integer>(enrollmentEHBUpdation.getEnrollmentIdList().subList(startIndex, endIndex));
				}
			}
		}
	}
	
	@Override
	public Integer read() throws Exception, UnexpectedInputException,
			ParseException, NonTransientResourceException {
		if(enrollmentIdList!=null && !enrollmentIdList.isEmpty()){
			if(loopCount<enrollmentIdList.size()){
				loopCount++;
				return enrollmentIdList.get(loopCount-1);
				
			}else{
				return null;
			}
		}
			return null;
	}


	
	public EnrollmentEHBUpdation getEnrollmentEHBUpdation() {
		return enrollmentEHBUpdation;
	}

	public void setEnrollmentEHBUpdation(EnrollmentEHBUpdation enrollmentEHBUpdation) {
		this.enrollmentEHBUpdation = enrollmentEHBUpdation;
	}

	/**
	 * @return the enrollmentIdList
	 */
	public List<Integer> getEnrollmentIdList() {
		return enrollmentIdList;
	}

	/**
	 * @param enrollmentIdList the enrollmentIdList to set
	 */
	public void setEnrollmentIdList(List<Integer> enrollmentIdList) {
		this.enrollmentIdList = enrollmentIdList;
	}
}
