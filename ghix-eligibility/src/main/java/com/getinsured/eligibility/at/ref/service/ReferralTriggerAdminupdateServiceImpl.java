package com.getinsured.eligibility.at.ref.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.indportal.enrollment.api.IndPortalEnrollmentUtility;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.dto.Ind57Mapping;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;


/**
 * @author chopra_s
 *
 */
@Component("referralTriggerAdminupdateService")
@DependsOn("dynamicPropertiesUtil")
@Scope("singleton")
public class ReferralTriggerAdminupdateServiceImpl implements ReferralTriggerAdminupdateService {
	private static final Logger LOGGER = Logger.getLogger(ReferralTriggerAdminupdateServiceImpl.class);

	@Autowired
	private IndPortalEnrollmentUtility indPortalEnrollmentUtility;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	private final static String PORTAL_SUCCESS_RESPONSE = "success";



	@Override
	public void triggerAdminupdate(long enrolledApplicationId, long currentApplicationId, Boolean isDemo, Address updateMailingAddress, List<String> memberGuIdList) throws Exception {
		try {
			LOGGER.info("Trigger Admin Update for enrolled application - " + enrolledApplicationId + " and current application " + currentApplicationId);
			//As per Srinis comment (e-MAIL#Subject-ID state specific check)admin update will be applicable for all states
			//if (isStateID()) {
				executeUpdate(enrolledApplicationId, currentApplicationId, isDemo, updateMailingAddress,memberGuIdList);
			//}
			LOGGER.info("Trigger Admin Update Ends");
		} catch (Exception e) {
			throw e;
		}
	}

	private void executeUpdate(long enrolledApplicationId, long currentApplicationId, Boolean isDemo, Address updateMailingAddress, List<String> memberGuIdList) throws Exception {
		boolean isDemoOnly = isDemo == null ? false : isDemo.booleanValue();

		if (!isDemoOnly){
			final long countOfApplicants = ssapApplicantRepository.countofApplicantsWithDemoUpdates(currentApplicationId, ReferralConstants.DEMOGRAPHIC_APPLICANT_STATUS_VALUE);
			if (countOfApplicants == ReferralConstants.NONE) {
				LOGGER.info("No Applicants with Demo Updates for applciation - " + currentApplicationId);
				return;
			}
		}
		Integer mailingLocationId = compareMailingAddress(enrolledApplicationId, currentApplicationId);
		
		final String caseNumber = ssapApplicationRepository.fetchCaseNumberById(enrolledApplicationId);
		
		triggerUpdate(enrolledApplicationId, caseNumber, mailingLocationId,memberGuIdList);
	}

	private void triggerUpdate(long enrolledApplicationId, String caseNumber, Integer mailingLocationId, List<String> memberGuidList ) throws Exception {

		String responseValue = null;
		StringBuilder errorReason = new StringBuilder();
		try {
			LOGGER.info("triggerUpdate Starts for caseNumber " + caseNumber);
			final Ind57Mapping ind57Mapping = new Ind57Mapping();
			ind57Mapping.setCaseNumber(caseNumber);
			ind57Mapping.setUserName(ReferralConstants.EXADMIN_USERNAME);
			ind57Mapping.setMemberGuids( memberGuidList);
			
			if (mailingLocationId == null){
				ind57Mapping.setMailingAddressChanged(false);
			} else {
				ind57Mapping.setMailingAddressChanged(true);
				ind57Mapping.setUpdatedMailingAddressLocationID(mailingLocationId.longValue());
			}
			
			final String status = indPortalEnrollmentUtility.invokeAdminUpdateEnrollment(ind57Mapping);
			if (!PORTAL_SUCCESS_RESPONSE.equalsIgnoreCase(status)) {
				responseValue = GhixConstants.RESPONSE_FAILURE;
				errorReason.append("Error while invoking Admin update for case number ").append(caseNumber)
				.append(", MailingAddressChanged - ").append(ind57Mapping.getMailingAddressChanged())
				.append(", UpdatedMailingAddressLocationID - " ).append(ind57Mapping.getUpdatedMailingAddressLocationID());
				throw new GIRuntimeException(errorReason.toString());
			}
			responseValue = GhixConstants.RESPONSE_SUCCESS;
			errorReason.append("Admin update for case number ").append(caseNumber)
				.append(", MailingAddressChanged - ").append(ind57Mapping.getMailingAddressChanged())
				.append(", UpdatedMailingAddressLocationID - " ).append(ind57Mapping.getUpdatedMailingAddressLocationID());
			LOGGER.info("triggerUpdate Ends for caseNumber " + caseNumber);
		} catch (Exception e) {
			responseValue = GhixConstants.RESPONSE_FAILURE;
			errorReason.append("Error while invoking Admin update for case number ").append(caseNumber).append(" - ").append(e);
			throw e;
		} finally {
			logData(enrolledApplicationId, errorReason.toString(), responseValue);
		}
		
	}

	private Integer compareMailingAddress(long enrolledApplicationId, long currentApplicationId) {
		SsapApplicant currApplicant = ssapApplicantRepository.findByPersonId(currentApplicationId, 1L);
		SsapApplicant enApplicant = ssapApplicantRepository.findByPersonId(enrolledApplicationId, 1L);
		
		Location currMailingLocation = readMailingLocation(currApplicant);
		Location enrolledMailingLocation = readMailingLocation(enApplicant);
		
		if (currMailingLocation != null && enrolledMailingLocation != null){
			if (!StringUtils.equalsIgnoreCase(currMailingLocation.getAddress1(), enrolledMailingLocation.getAddress1())
					|| !StringUtils.equalsIgnoreCase(currMailingLocation.getAddress2(), enrolledMailingLocation.getAddress2())
					|| !StringUtils.equalsIgnoreCase(currMailingLocation.getCity(), enrolledMailingLocation.getCity())
					|| !StringUtils.equalsIgnoreCase(currMailingLocation.getState(), enrolledMailingLocation.getState())
					|| !StringUtils.equalsIgnoreCase(currMailingLocation.getZip(), enrolledMailingLocation.getZip())
							){
				return currMailingLocation.getId();
			}
		}
		return null;
		
	}
	
	private Location readMailingLocation(SsapApplicant ssapApplicant) {
		// Other Location is the Primary Location
		if (ssapApplicant.getMailiingLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getMailiingLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
		} else if (ssapApplicant.getOtherLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getOtherLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
		} else {
			return null;
		}
	}

	@SuppressWarnings("unused")
	private void triggerUpdate(long enrolledApplicationId, String caseNumber, Address updateMailingAddress) throws Exception {
		String responseValue = null;
		StringBuilder errorReason = new StringBuilder();
		try {
			LOGGER.info("triggerUpdate Starts for caseNumber " + caseNumber);
			final Ind57Mapping ind57Mapping = new Ind57Mapping();
			ind57Mapping.setCaseNumber(caseNumber);
			ind57Mapping.setUserName(ReferralConstants.EXADMIN_USERNAME);
			
			if (isUpdatedMailingAddressEmpty(updateMailingAddress)){
				// set flag as false and location id as null
				ind57Mapping.setMailingAddressChanged(false);
			} else {
				// set flag as true and location id as new Location
				ind57Mapping.setMailingAddressChanged(true);
				long updatedMailingAddressLocationID = createLocation(updateMailingAddress);
				ind57Mapping.setUpdatedMailingAddressLocationID(updatedMailingAddressLocationID);
			}
			
			final String status = indPortalEnrollmentUtility.invokeAdminUpdateEnrollment(ind57Mapping);
			if (!PORTAL_SUCCESS_RESPONSE.equalsIgnoreCase(status)) {
				responseValue = GhixConstants.RESPONSE_FAILURE;
				errorReason.append("Error while invoking Admin update for case number ").append(caseNumber)
				.append(", MailingAddressChanged - ").append(ind57Mapping.getMailingAddressChanged())
				.append(", UpdatedMailingAddressLocationID - " ).append(ind57Mapping.getUpdatedMailingAddressLocationID());
				throw new GIRuntimeException(errorReason.toString());
			}
			responseValue = GhixConstants.RESPONSE_SUCCESS;
			errorReason.append("Admin update for case number ").append(caseNumber)
				.append(", MailingAddressChanged - ").append(ind57Mapping.getMailingAddressChanged())
				.append(", UpdatedMailingAddressLocationID - " ).append(ind57Mapping.getUpdatedMailingAddressLocationID());
			LOGGER.info("triggerUpdate Ends for caseNumber " + caseNumber);
		} catch (Exception e) {
			responseValue = GhixConstants.RESPONSE_FAILURE;
			errorReason.append("Error while invoking Admin update for case number ").append(caseNumber).append(" - ").append(e);
			throw e;
		} finally {
			logData(enrolledApplicationId, errorReason.toString(), responseValue);
		}
	}

	private boolean isUpdatedMailingAddressEmpty(Address updateMailingAddress) {
		return StringUtils.isEmpty(updateMailingAddress.getStreetAddress1());
	}
	
	@Autowired
	@Qualifier("iLocationRepository")
	private ILocationRepository iLocationRepository;
	
	private int createLocation(Address address) {
		Location newLocation = new Location();
		newLocation.setAddress1(address.getStreetAddress1());
		newLocation.setAddress2(address.getStreetAddress2());
		newLocation.setCity(address.getCity());
		newLocation.setState(address.getState());
		newLocation.setZip(address.getPostalCode());
		newLocation.setCounty(address.getCounty());
		newLocation.setCountycode(address.getCountyCode());
		newLocation = iLocationRepository.save(newLocation);
		return newLocation.getId();

	}

	private boolean isStateID() {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		return "ID".equals(stateCode);
	}

	@Autowired private IntegrationLogService integrationLogService;
	private static final String ADMIN_UPDATE_IND57 = "ADMIN_UPDATE_IND57";
	private void logData(Long ssapApplicationId, String message, String status) {
		integrationLogService.save(message, ADMIN_UPDATE_IND57, status, ssapApplicationId, null);
	}


}
