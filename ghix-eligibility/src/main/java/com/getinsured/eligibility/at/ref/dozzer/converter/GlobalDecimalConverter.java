/**
 * 
 */
package com.getinsured.eligibility.at.ref.dozzer.converter;

import java.math.BigDecimal;

import org.dozer.DozerConverter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author chopra_s
 * 
 */
@Component("dozzerDecimalConverter")
@Scope("singleton")
public class GlobalDecimalConverter
		extends
		DozerConverter<com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Decimal, BigDecimal> {

	public GlobalDecimalConverter() {
		super(com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Decimal.class,
				BigDecimal.class);
	}

	@Override
	public com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Decimal convertFrom(
			BigDecimal arg0,
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Decimal arg1) {
		return null;
	}

	@Override
	public BigDecimal convertTo(
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Decimal source,
			BigDecimal dest) {
		if (source != null) {
			return source.getValue();
		}
		return null;
	}

}
