import { tribesActionTypes } from '../constants/actionTypes';
import CONFIG from '../.env';

export const initialState = CONFIG.ENV.PRODUCTION ? [] :
    [
        {
            name: 'Alabama',
            code: 'AL',
            tribes: [
                {
                    tribeName: 'Poarch Band of Creek Indians of Alabama',
                    tribeCode: 'AL010001'
                }
            ]
        }
    ];

export default (state = initialState, action) => {
    switch(action.type) {
    case tribesActionTypes.SET_TRIBES:
        return action.tribes;
    case tribesActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};