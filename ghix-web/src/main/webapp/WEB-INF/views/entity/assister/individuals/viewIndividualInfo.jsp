<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script type="text/javascript">
function validate(desigId)
{
  $("form").attr("action", "../../assister/approve/" + desigId);
  $("form").submit();
};

function decline(desigId)
{
  $("form").attr("action", "../../assister/decline/" + desigId);
  $("form").submit();
};


function closeIFrame() {
	$("#imodal").remove();
	parent.location.reload();
	window.parent.closeIFrame();
}


function formatPhoneNumber(){
	var vNumber, eleNumber, phoneNumber;
	
	eleNumber = document.getElementById("phone");
	if(null != eleNumber && "undefined" != eleNumber) {
		vNumber = eleNumber.innerHTML;
		
		if(null != vNumber && "undefined" != vNumber && "" != vNumber) {
			phoneNumber = "(" + vNumber.substring(0,3) + ") " + vNumber.substring(3,6) + "-" + vNumber.substring(6,10);

			eleNumber.innerHTML = phoneNumber;
		}
	}
}

$(window).load(function() {
	formatPhoneNumber();
});

</script>

<!-- <div class="gray"> -->
<%-- 	<h4>${externalIndividual.firstName} ${externalIndividual.lastName}</h4> --%>
<!-- </div> -->

<form class="form-vertical" id="frmViewDesigDetail"
	name="frmViewDesigDetail" method="GET" target="_parent">
	 <input type="hidden" name="prevStatus"	id="prevStatus" value="${desigStatus}">
	<table class="table table-border-none">
		<tbody>
			<c:if test="${caStateCode}">
				 <tr>
	 				<td class=""><spring:message code="label.assister.contactName"/> <strong>${externalIndividual.firstName} 
	 						${externalIndividual.lastName}</strong></td> 
	 			</tr>  
 			</c:if>
			<tr>
				<td class=""><spring:message code="label.assister.PhoneNumber"/>: <strong id="phone">${externalIndividual.phoneNumber}</strong></td>
			</tr>
			<tr>
				<td class=""><spring:message code="label.assister.emailAddress"/>: <strong>${externalIndividual.email}</strong></td>
			</tr>
		</tbody>
	</table>
	<c:if test="${(desigStatus != 'InActive')}">
		<div class="form-actions" style="margin: 0;padding: 0 20px;">
			<span class="pull-right">
				<a class="btn btn-primary btn-small" onClick="return validate('${encryptedIndividualId}')"><spring:message code="label.assister.indAccept"/></a>
				<a class="btn btn-small" href="#" onClick="return decline('${encryptedIndividualId}')"><spring:message code="label.assister.indDecline"/></a>
			</span>
		</div>
	</c:if>
	<c:if test="${(desigStatus == 'InActive')}">
		<div class="form-actions" style="margin: 0;margin-top:60px; padding: 20px 20px 0 20px;">
			<span class="pull-right">
				<a class="btn btn-small" href="#" onClick="closeIFrame()"><spring:message code="label.assister.close"/></a>
			</span>
		</div>
	</c:if>
</form>
