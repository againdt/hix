var app = angular.module('cmssummaryreportapp', ['ui.grid','ui.grid.exporter', 'ui.bootstrap','ui.router', 'ui.grid.resizeColumns', 'ui.grid.pagination']);

app.controller('CMSXmlSummaryReportCtrl', ['$scope', '$filter', '$timeout', 'CMSXmlSumryReportService', 'uiGridConstants','uiGridExporterConstants','uiGridExporterService', function ($scope,$filter,$timeout,CMSXmlSumryReportService, uiGridConstants,uiGridExporterConstants,uiGridExporterService) {
	var paginationOptions = {
			pageNumber: 1,
			pageSize: 20,
			sort: null
	};	
 
	$scope.colDef = [];
	$scope.data = null;
	$scope.totalElem = null;
	$scope.externalConfig = {};
    $scope.dropDownColumns = [];
	
	var monthandYearFilter = function(start,end) {
		var selectOptions = [];
		for (var i=start; i<=end; i++) {
			selectOptions.push({ value: i, label : i});
		}
		return selectOptions;
	};
	
	var nextAction = ['SENT', 'SUCCESS' , 'HOLD', 'REGEN' ,'INACTIVE'];
	var fileStatus = ['Accepted', 'Accepted with warnings','Accepted with errors','Rejected', 'Approved', 'Approved with warnings', 'Approved with errors', 'Disapproved'];
	
	var selectOptions = function(data) {
		var selectOption = [];
		for (var i=0; i<data.length; i++) {
			selectOption.push({ value: data[i], label : data[i]});
		}
		return selectOption;
	};
	
    var displayName = {hoisIssuerId : 'HOIS Issuer ID', sbmsSbmrRcvd: 'CMS Processing Stage', issuerFileSetId : 'File Set ID', 
    		acceptedErrorWarnCount : 'CMS Response - Policy Acceptance Count with Errors/Warnings', acceptedCount : 'CMS Response - Policy Acceptance Count',
    		totPrevPoliciesNotSub : 'CMS Response - Policies Not Submitted Count', missingPolicyIds : 'CMS Response - Policies Not Submitted IDs',
    		skipRecords : 'Policy IDs Not Sent Due to Errors', totalPolicyrecordsApproved : 'CMS Response - Policies Approved Count',
    		totalRecordsRejected : 'CMS Response - Policies Rejected Count', totalRecordsProcessed : 'CMS Response - Policies Processed Count'};

	
	var setPropertiesFn = function (elem, key) {
		var filter = { type: uiGridConstants.filter.SELECT, condition : uiGridConstants.filter.EXACT };
		
        switch (key) {
            case 'reportMonth':
            	elem.filter = filter;
            	elem.filter.selectOptions = monthandYearFilter(1, 12);
            	$scope.dropDownColumns.push(key);
            	break;
            case 'reportYear':
            	elem.filter = filter;
            	elem.filter.selectOptions = monthandYearFilter(2016, new Date().getFullYear());
            	$scope.dropDownColumns.push(key);
                break;
            case 'nextAction':
            	elem.filter = filter;
            	elem.filter.selectOptions = selectOptions(nextAction);
            	elem.enableSorting = false;
            	$scope.dropDownColumns.push(key);
                break;
            case 'status':
            	elem.filter = filter;
            	elem.filter.selectOptions = selectOptions(fileStatus);
            	elem.enableSorting = false;
            	$scope.dropDownColumns.push(key);
                break;
            case 'hoisIssuerId':
            case 'sbmsSbmrRcvd':
            case 'acceptedErrorWarnCount':
            case 'acceptedCount':
            case 'totPrevPoliciesNotSub' :
            case 'missingPolicyIds' :
            case 'skipRecords' :
            case 'totalPolicyrecordsApproved' :
            case 'totalRecordsRejected' :
            case 'totalRecordsProcessed' :
            	elem.displayName = displayName[key];
            
            default:
            	if(key != 'sbmsSbmrRcvd' && key != 'missingPolicyIds' && key != 'skipRecords' && key != 'fileName')
            		elem.enableFiltering = false;
            	if(key != 'hoisIssuerId')
            		elem.enableSorting = false;

        }
    };
    
	CMSXmlSumryReportService.getReport(paginationOptions.pageNumber,
			paginationOptions.pageSize, null).success(function(data){
				$scope.data = $scope.gridOptions.data = data.content;
				if(data.size != 0) {
					
					var orderElem = [];
					angular.forEach(data.content[0], function(value, key) {
						if(key != 'creationTimeStamp') {		
	
							var elem = {name:key, width: 150};
							setPropertiesFn(elem,key);

							if(key == 'status' || key == 'nextAction') {
								orderElem.push(elem);
							} else {
								setPropertiesFn(elem,key);
								$scope.colDef.push(elem);
							}
						}
					});
					
					//Add status and nextAction column after fileName column
					var foundItem = $filter('filter')($scope.colDef, { name: 'fileName' }, true)[0];
					var index = $scope.colDef.indexOf(foundItem);
					for(var i = 0; i< orderElem.length; i++) {
						$scope.colDef.splice(index + i + 1, 0, orderElem[i]);
					}
					
					// Add Creation Time column as last
					$scope.colDef.push({ name: 'creationTimeStamp', displayName: 'Creation time', enableSorting: true, enableFiltering: false, cellFilter: 'date:\'yyyy-MM-dd HH:mm\'', width: 200,
						sort: {
						    direction: uiGridConstants.DESC
						   }
					});
				}
				$scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;   
				
			});

	$scope.gridOptions = {
			paginationPageSizes: [5, 10, 20],
			paginationPageSize: paginationOptions.pageSize,
			useExternalPagination: true,
			columnDefs:  $scope.colDef,
			enableFiltering: true,
			enableGridMenu: true, 
			useExternalFiltering: true,
			useExternalSorting: true,
			exporterMenuPdf: false,
			exporterMenuCsv: false,
			exporterPdfFilename: 'download.pdf',
			exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
			onRegisterApi: function(gridApi) {
				$scope.gridApi = gridApi;
				
				$scope.gridApi.core.on.filterChanged( $scope, function() {
                    var grid = this.grid;
                    var filterExact = {}, filterLike = {};
                    angular.forEach(grid.columns, function (column, index) {
                            if (column.filters && column.filters[0].term) {
                                    if($scope.dropDownColumns.indexOf(column.field) !== -1)
                                            filterExact[column.field] = column.filters[0].term;
                                    else
                                            filterLike[column.field] = column.filters[0].term;
                            }
                    });
                    $scope.externalConfig.filterExact = filterExact;
                    $scope.externalConfig.filterLike = filterLike;
                    CMSXmlSumryReportService.getReport(paginationOptions.pageNumber, paginationOptions.pageSize, $scope.externalConfig).success(function(data){
                                          $scope.data = $scope.gridOptions.data = data.content;
                                          $scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
                                  });
                   });
                  
                  $scope.gridApi.core.on.sortChanged( $scope, function(grid, sortColumns){
                          var grid = this.grid;
                    var asc = [], desc = [];
                    angular.forEach(grid.columns, function (column, index) {
                            console.log(column);
                            if (column.sort && column.sort.direction) {
                                    if(column.sort.direction == uiGridConstants.ASC)
                                            asc.push(column.field);
                                    else if(column.sort.direction == uiGridConstants.DESC)
                                            desc.push(column.field);
                            }
                        });
                    $scope.externalConfig[uiGridConstants.ASC] = asc;
                    $scope.externalConfig[uiGridConstants.DESC] = desc;
                    CMSXmlSumryReportService.getReport(paginationOptions.pageNumber, paginationOptions.pageSize, $scope.externalConfig).success(function(data){
                                          $scope.data = $scope.gridOptions.data = data.content;
                                          $scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
                                  });
                  });
                  
				gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
					paginationOptions.pageNumber = newPage;
					paginationOptions.pageSize = pageSize;
					CMSXmlSumryReportService.getReport(newPage,pageSize, $scope.externalConfig).success(function(data){
						$scope.data = $scope.gridOptions.data = data.content;
						$scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
					});
				});
			}
	};
	
	$scope.gridOptions.gridMenuCustomItems= [
	                                         {
	                                        	 title:'Export all data as CSV',
	                                             action: function ($event) {
	                                            	 CMSXmlSumryReportService.getReport(1,$scope.gridOptions.totalItems,null).success(function(data) {
	                                                     $scope.gridOptions.totalItems = data.totalElements;
	                                                     $scope.gridOptions.data = data.content;
	                                                     $timeout(function()
	                                                             {
	                                                                 var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
	                                                                 $scope.gridApi.exporter.csvExport( uiGridExporterConstants.ALL, uiGridExporterConstants.ALL, myElement );
	                                                                 $scope.gridOptions.data =  $scope.data;
	        	                                                     $scope.gridOptions.totalItems = $scope.totalElem;
	                                                             }, 1000);
	                                                     

	                                                     });
	                                             },
	                                             order:0
	                                           },
	                                           {
		                                        	 title:'Export all data as PDF',
		                                             action: function ($event) {
		                                            	 CMSXmlSumryReportService.getReport(1,$scope.gridOptions.totalItems,null).success(function(data) {
		                                                     $scope.gridOptions.totalItems = data.totalElements;
		                                                     $scope.gridOptions.data = data.content;
		                                                     $timeout(function()
		                                                             {
		                                                                 $scope.gridApi.exporter.pdfExport( uiGridExporterConstants.ALL, uiGridExporterConstants.ALL );
		                                                                 $scope.gridOptions.data =  $scope.data;
		        	                                                     $scope.gridOptions.totalItems = $scope.totalElem;
		                                                             }, 1000);
		                                                     

		                                            	});
		                                             },
		                                             order:0
	                                           }
	                                          ];
}]);

app.service('CMSXmlSumryReportService',['$http', function ($http) {

	function getReport(pageNumber,size,requestdata) {
		pageNumber = pageNumber > 0?pageNumber - 1:0;
		return  $http({
			method: 'POST',
			url: '/hix/admin/reports/getcmsxmlsummaryreport?page='+pageNumber+'&size='+size,
            dataType: "json",
            headers: {
                'Content-Type': "application/json",
                "csrftoken": $('#tokid').val()
            },
            data: JSON.stringify(requestdata)
		});
	}

	return {
		getReport:getReport
	};

}]);

