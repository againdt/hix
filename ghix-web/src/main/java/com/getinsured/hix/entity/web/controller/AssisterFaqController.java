package com.getinsured.hix.entity.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * FAQ Controller class to intercept any FAQ specific requests
 * for Assister
 * 
 */
@Controller
public class AssisterFaqController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AssisterFaqController.class);

	/**
	 * Intercepts any FAQ specific requests for assister
	 */
	@RequestMapping(value = "/entity/assister/faq", method = {RequestMethod.GET,RequestMethod.POST})
	public String interceptRequest(Model model, HttpServletRequest request) {
		
		LOGGER.info("interceptRequest: START");
		
		LOGGER.info("InterceptRequest method has been invoked");
		
		LOGGER.info("interceptRequest: END");
		
		return "entity/assister/faq";
	}
}
