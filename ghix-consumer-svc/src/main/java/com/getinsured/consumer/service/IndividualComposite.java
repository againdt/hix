package com.getinsured.consumer.service;

public class IndividualComposite {
	
	private String firstName;
	private String lastName;
	private String applicationStatus;
	private String eligibilityStatus;
	private String emailAddress;
	private String requestSent;
	private long consumerId;
	private String status;
	private int familySize;
	private double houseHoldIncome;
	private long cecId;
	private String cecFirstName;
	private String cecLastName;
	private String phoneNumber;
	private String address1;
	private String address2; 
	private String city;
	private String state;
	private String zip;
	private long consumerUserId;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}

	public String getRequestSent() {
		return requestSent;
	}

	public void setRequestSent(String requestSent) {
		this.requestSent = requestSent;
	}

	private String inActiveSince;
	
	public String getInActiveSince() {
		return inActiveSince;
	}

	public void setInActiveSince(String inActiveSince) {
		this.inActiveSince = inActiveSince;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	
	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getFamilySize() {
		return familySize;
	}

	public void setFamilySize(int familySize) {
		this.familySize = familySize;
	}

	public double getHouseHoldIncome() {
		return houseHoldIncome;
	}

	public void setHouseHoldIncome(double houseHoldIncome) {
		this.houseHoldIncome = houseHoldIncome;
	}

	public long getCecId() {
		return cecId;
	}

	public void setCecId(long cecId) {
		this.cecId = cecId;
	}

	public String getCecFirstName() {
		return cecFirstName;
	}

	public void setCecFirstName(String cecFirstName) {
		this.cecFirstName = cecFirstName;
	}

	public String getCecLastName() {
		return cecLastName;
	}

	public void setCecLastName(String cecLastName) {
		this.cecLastName = cecLastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public long getConsumerUserId() {
		return consumerUserId;
	}

	public void setConsumerUserId(long consumerUserId) {
		this.consumerUserId = consumerUserId;
	}
	
	
}
