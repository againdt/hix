package com.getinsured.hix.indportal.dto.dm.application;

public class JobIncome {
	private String employerName;
	private String averageWeeklyWorkDays;
	private String averageWeeklyWorkHours;

	// Getter Methods

	public String getAverageWeeklyWorkDays() {
		return averageWeeklyWorkDays;
	}

	public String getAverageWeeklyWorkHours() {
		return averageWeeklyWorkHours;
	}

	public String getEmployerName() {
		return employerName;
	}

	// Setter Methods

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	public void setAverageWeeklyWorkDays(String averageWeeklyWorkDays) {
		this.averageWeeklyWorkDays = averageWeeklyWorkDays;
	}

	public void setAverageWeeklyWorkHours(String averageWeeklyWorkHours) {
		this.averageWeeklyWorkHours = averageWeeklyWorkHours;
	}

}