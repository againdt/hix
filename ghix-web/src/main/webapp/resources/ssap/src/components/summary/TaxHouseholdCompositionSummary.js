import React from 'react';

const TaxHouseholdCompositionSummary = props => {

    const {
        analyzeFamily,
        family,
        householdMembers,
    } = props;

    const pairsInfo = family.pairs.filter(pair => pair.isPairAccordingToLaw).map(pair => {

        const {
            pairId,
            spouse1: {
                spouse1fullName
            },
            spouse2: {
                spouse2fullName
            },
            liveTogether
        } = pair;

        return (
            <p key={pairId}>
                <strong>
                    {spouse1fullName} &nbsp;
                </strong>
                and &nbsp;
                <strong>
                    {spouse2fullName} &nbsp;
                </strong>
                are spouses and { liveTogether ? 'live' : 'do not live'} together
            </p>
        );
    });

    const singles = family.dependents.filter(d => !d.spouse).map(d => {
        const {
            addressId,
            dependentFullName,
            dependentPersonId,
            parents,
            siblings
        } = d;

        const parentsQtyLivesWith = (() => {
            if(!parents) {
                return 0;
            }
            return parents.filter(parent => analyzeFamily.getHhmByPersonId(parent, householdMembers).addressId === addressId).length;
        })();

        const siblingsQtyLivesWith = (() => {
            if(!siblings) {
                return 0;
            }
            return siblings.filter(sibling => analyzeFamily.getHhmByPersonId(sibling, householdMembers).addressId === addressId).length;
        })();

        return (
            <p key={`${dependentFullName}_${dependentPersonId}`}>
                <strong>
                    {dependentFullName} &nbsp;
                </strong>
                {
                    !parents ?
                        'has no parents' :
                        `${parentsQtyLivesWith === 0
                            ? 'does not live with parents' :
                            parentsQtyLivesWith === 1 ? 'lives with 1 parent' : 'lives with both parents'}`
                }
                &nbsp; and &nbsp;
                {
                    !siblings ?
                        'has no siblings' :
                        siblingsQtyLivesWith === 0
                            ? 'does not live with siblings' : `lives with ${siblingsQtyLivesWith} ${siblingsQtyLivesWith === 1 ? 'sibling' : 'siblings'}`
                }
            </p>
        );

    });

    const disclaimer =
        <p key={'disclaimer'}>
            It is important that everyone living with you is entered into the application, even if they are not applying for health coverage
        </p>;

    return [
        ...pairsInfo,
        ...singles,
        disclaimer
    ];
};

export default TaxHouseholdCompositionSummary;