<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/WEB-INF/tld/comments-view" prefix="comment" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>

<div  class="gutter10-lr clearfix">
	<div class="row-fluid">
				<ul class="page-breadcrumb hide">
					<li><a href="javascript:history.back()">&lt; <spring:message
								code="label.back" /></a></li>
					<li><a href="<c:url value="/entity/entityadmin/managelist"/>"><spring:message
								code="label.entities" /></a></li>
					<li><spring:message code="label.entity.manage"/></li>
					<li>${enrollmentEntity.entityName}</li>
				</ul><!--page-breadcrumb ends-->
	</div><!--  end l-page-breadcrumb -->
	
	<div class="row-fluid">
		<c:if test="${loggedUser =='entityAdmin'}">
			<div >
				<h1 style="margin-top: 0px;margin-bottom: 0px;"><a name="skip"></a>${enrollmentEntity.entityName}</h1>
			</div>	
		</c:if>
	</div>
	
	
	<div class="row-fluid">
	<!-- #sidebar -->  
	<jsp:include page="/WEB-INF/views/entity/leftNavigationMenu.jsp" />
	<!-- #sidebar ENDS -->  	
	<div class="span9">
	
			<div class="header">
				<h4 class="pull-left"><spring:message code="label.RegistrationStatus"/></h4>
				<c:choose>
					<c:when test="${fn:containsIgnoreCase(enrollmentEntity.registrationStatus,'Incomplete')}">
					</c:when>
						<c:otherwise>	
						<c:set var="encrytedEntitylistId"><encryptor:enc value="${enrollmentEntity.id}" isurl="true"/> </c:set>		
								<a class="btn btn-small pull-right"  href="<c:url value="/entity/entityadmin/editregistrationstatus/${encrytedEntitylistId}"/>" title="<spring:message code="label.entEdit"/>"><spring:message code="label.entEdit"/></a>
								
						</c:otherwise>
					</c:choose>  
				
			</div>
		
			<form class="form-horizontal" id="frmregistrationstatus" name="frmregistrationstatus" action="registrationstatus" method="GET">
			<input type="hidden" id="loggedUser" name="loggedUser" value="admin"/>
			<input type="hidden" id="documentId1" name="documentId1" value=""/>	
				<div class="gutter10-tb">
					<table class="table table-border-none">
						<tbody>	
							<tr>
							    <td class="span3 txt-right" scope="row"><spring:message code="label.EntityNumber"/></td>
								<td class="span6"><strong>${enrollmentEntity.entityNumber}</strong></td>	
							</tr>					
							<tr>
								<td class="span3 txt-right" scope="row"><spring:message code="label.RegistrationStatus"/></td>
								<td class="span6"><strong>${enrollmentEntity.registrationStatus}</strong></td>
							</tr>
							<tr>
<!-- 							<tr> -->
<!-- 								<td class="span3 txt-right">Entity Registration Number</td> -->
<!-- 								<td class="span6"><strong>298377283</strong></td> -->
<!-- 							</tr> -->
							<c:choose>
								<c:when test="${enrollmentEntity.registrationStatus=='Registered' || enrollmentEntity.registrationStatus=='Active'}">
								<td class="span3 txt-right" scope="row"><spring:message code="label.RenewalDate"/></td>
								<td class="span6"><strong><fmt:formatDate value= "${enrollmentEntity.registrationRenewalDate}" pattern="MM/dd/yyyy"/></strong></td>
								</c:when>
								
								<c:otherwise>
								<td class="span3 txt-right" scope="row"><spring:message code="label.RenewalDate"/></td>
								<td class="span6"><strong><spring:message code="label.entity.nA"/></strong></td>
								</c:otherwise>
							</c:choose>	
							</tr>
																		
						</tbody>
					</table>
				</div><!-- end of .gutter10 -->
				
					
                	<p><spring:message code="label.entity.viewTheStatusOfUrCertificationAppHere"/></p>
                	<div class="header">
                    	<h4><spring:message code="label.entity.certificationHistory"/></h4>
                    </div>	
                    <div class="gutter10">		
					<display:table id="enrollmentRegisterStatusHistory" name="enrollmentRegisterStatusHistory" list="enrollmentRegisterStatusHistory" requestURI="" sort="list" class="table" >
				           <display:column property="updatedOn" titleKey="label.entity.date"  format="{0,date,MM/dd/yyyy}" sortable="false" />
				           <display:column property="previousRegistrationStatus" titleKey="label.entity.previousStatus" sortable="false"/>
				           <display:column property="newRegistrationStatus" titleKey="label.NewStatus" sortable="false"/>
						   <display:column property="comments" titleKey="label.entity.vieComment" sortable="false"/>
							<display:column titleKey="label.assister.viewAttachment" > 
				                 <c:choose>
				                      <c:when test="${enrollmentRegisterStatusHistory.documentId!=null}">	
				                       <c:set var="encrytedenrollmentRegisterStatusHistorydocumentId"><encryptor:enc value="${enrollmentRegisterStatusHistory.documentId}" isurl="true" /> </c:set>
			                                <a href="#" onClick="showdetail('${encrytedenrollmentRegisterStatusHistorydocumentId}');" id="edit_${enrollmentRegisterStatusHistory.documentId}"><spring:message code="label.assister.viewAttachment"/></a> 
			                          </c:when>
			                          <c:otherwise>	
			                                <spring:message code="label.entity.noAttachment"/>
			                          </c:otherwise>
			                     </c:choose>
						   </display:column>
					</display:table>
					</div>					
				<!-- end of form-actions -->

			</form>
		</div><!-- end of span9 -->
		</div>
	</div>
	<div id="modalBox" class="modal hide fade">
	<div class="modal-header">
		<a class="close" data-dismiss="modal" data-original-title="">x</a>
		<h3 id="myModalLabel"><spring:message code="label.entity.vieComment"/></h3>
	</div>
	<div id="commentDet" class="modal-body">
		<label id="commentlbl" ></label>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message code="label.entity.close"/></a>
	</div>
</div>

	<!--  end of row-fluid -->
	
	<!-- <div id="modalBox" class="modal hide fade">
	<div class="modal-header">
		<a class="close" data-dismiss="modal" data-original-title="">x</a>
		<h3 id="myModalLabel">View Comment</h3>
	</div>
	<div id="commentDet" class="modal-body">
		<p>Data data data ....</p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title="">Close</a>
	</div>
</div> -->

<script type="text/javascript">
$(document).ready(function() {	
	$("#registerStatus").removeClass("link");
	$("#registerStatus").addClass("active");
	$("#myTable").tablesorter();
	$("#showGraphs").verticaltabs({speed: 500,slideShow: false,activeIndex: 2});

	$('#feedback-badge-right').click(function() {
		$dialog.dialog('open');
		return false;
	});
});

function getComment(comments)
{	
	$('#commentlbl').html("<p> Loading Comment...</p>");
	comments =comments.replace(/#/g,'<br/>'); //Added to remove new line break marker with HTML equivalent br
	comments=comments.replace(/apostrophes/g,"'"); //Added to replace regex apostrophes with '
	$('#commentlbl').html("<p> "+ comments + "</p>");
	
}

$(function () {
	$("a[rel=twipsy]").twipsy({
		live: true
	});
});

$(function () {
	$("a[rel=popover]")
		.popover({
			offset: 10
		})
		.click(function(e) {
			e.preventDefault()  // prevent the default action, e.g., following a link
		});
});

function highlightThis(val){
	var currUrl = window.location.href;
	var newUrl="";

	/* Check if Query String parameter is set or not
	* If yes *
	*/
	if(currUrl.indexOf("?", 0) > 0) {
		if(currUrl.indexOf("?lang=", 0) > 0) { /* Check if locale is already set without querystring param */
			newUrl = "?lang="+val;
		} else if(currUrl.indexOf("&lang=", 0) > 0) { /* Check if locale is already set with querystring param  */
			newUrl = currUrl.substring(0, currUrl.length-2)+val; 
		} else {
			newUrl = currUrl + "&lang="+val;
		}
	} else { /* If No */
		newUrl = currUrl + "?lang="+val;
	}
	window.location = newUrl;
}
</script>
<script type="text/javascript">

function showdetail(documentId) {
	 
	 var rv = -1; // Return value assumes failure.
	 if (navigator.appName == 'Microsoft Internet Explorer')
	 {
	    var ua = navigator.userAgent;
	    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
	    if (re.exec(ua) != null)
	       rv = parseFloat( RegExp.$1 );
	 }
	 if(rv <= 8.0 && navigator.appName == 'Microsoft Internet Explorer'){
		 $('<div class="modal popup-address" id="addressIFrame"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><h4><spring:message  code='label.entiy.upgradeBrowser' javaScriptEscape='true'/></h4><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 0px;"><spring:message  code='label.entiy.upgradeBrowserMessage' javaScriptEscape='true'/></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
		 } else {
			var  contextPath =  "<%=request.getContextPath()%>";
			 var documentUrl = contextPath + "/entity/entityadmin/viewAttachment?encrypteddocumentId="+documentId;
		      window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
		}
	}		
</script>
