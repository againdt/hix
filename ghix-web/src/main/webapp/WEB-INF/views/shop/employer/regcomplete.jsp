<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="row-fluid gutter10">
	<div class="span12">
		<div class="alert alert-info">
			<strong><spring:message code='label.emplthanks'/>!</strong><br />
			<spring:message code='label.emplrecoverylink'/>
		</div>
		<div class="page-header">
			<h1><a name="skip" class="skip"><spring:message code='label.emplregcomplete'/></a></h1>
		</div><!-- end of page-header -->
		<div class="gutter10">
			<p><spring:message code='label.emplcall'/></p>
			<p><spring:message code='label.emplquestions'/></p>
		</div>
	</div>
</div>