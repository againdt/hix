(function (models) {
	models.Cart = Backbone.Model.extend({
		
		urlRoot : "cartItems",
		
		url : function () {
			if(this.id)
				return 'cartItems/'+this.id+'?'+(Math.random()*10);
			else
				return 'cartItems?'+(Math.random()*10);
		},
		
		//to specify default attributes for model
		defaults: {
			//attrName : default value
			"srNo"		: "",
			"id"		: "",
			"planId"		: "",
			"planCount"		: "",
			"name"		: "",
			"issuer"	: "",
			"issuerText"	: "",
			"level"  : "",
			"premium" : "",
			"avgPremium"	: "",
			"coInsurance"  : "",
			"deductible" : "",
			"officeVisit"	: "",
			"oopMax"	: "",
			"empCount"	: "",
			"providers"	: "",
			"employeeTotalCount"	: "",
			"employerTotalCost"	: "",
			"fromEmployerTotalCost"	: "",
			"toEmployerTotalCost"	: "",
			"employerContrForEmp"	: "",
			"estimatedTaxCredit"	: "",
			"employerContrForEmpInPreimum"	: "",
			"employeePayInPreimum"	: "",
			"notAffodableCount"	: "",
			"taxPenalty"	: ""
		}
	});
})(App.models);