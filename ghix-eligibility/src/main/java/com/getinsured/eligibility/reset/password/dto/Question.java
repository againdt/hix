package com.getinsured.eligibility.reset.password.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.getinsured.eligibility.reset.password.util.constraint.QuestionAnswerType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@QuestionAnswerType
public class Question {

	public enum DataType {STRING, INT, DOB}
	public enum Code {FIRSTNAME, LASTNAME, DOB, SSN, PHONE, ZIPCODE, COUNTY, HOUSEHOLDSIZE, GENDER}

	@NotNull
	private Code code;
	@NotNull
	private DataType type;
	@NotEmpty
	private String answer;



}
