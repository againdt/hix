package com.getinsured.hix.batch.ssap.renewal.service;

import java.util.List;

import org.springframework.batch.core.UnexpectedJobExecutionException;

import com.getinsured.hix.dto.enrollment.QhpReportDTO;

/**
 * Interface is used to provide services for QHP Reports.
 */
public interface QhpReportService {

	String QHP_REPORT_JOB_NAME = "renewalQHPReportJob";

	List<Integer> getEnrollmentIdListByCoverageYearForQHPReports(Integer coverageYear);

	List<QhpReportDTO> getEnrollmentAndEnrolleeDataToGenerateQHPReports(Integer enrollmentId);

	boolean storeQHPReportData(List<QhpReportDTO> qhpReportList, Long batchJobExecutionId);

	boolean cleanOldQHPReportDataByJobId(Long batchJobExecutionId);

	void saveAndThrowsErrorLog(String errorMessage) throws UnexpectedJobExecutionException;
}
