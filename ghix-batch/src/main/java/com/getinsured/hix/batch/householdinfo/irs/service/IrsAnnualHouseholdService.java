/**
 * 
 */
package com.getinsured.hix.batch.householdinfo.irs.service;

import java.util.Set;

import com.getinsured.hix.dto.enrollment.EnrollmentAnnualIrsRecipientDTO;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
public interface IrsAnnualHouseholdService {
	/**
	 * Get recipient information from SSAP application table for annual reporting
	 * @param ssapApplicationId
	 * @param ssapApplicationSet 
	 * @return EnrollmentAnnualIrsRecipientDTO
	 * @throws GIException 
	 */
	EnrollmentAnnualIrsRecipientDTO getRecipientInformationForAnnualReport(Long ssapApplicationId, Set<Long> ssapApplicationSet) throws GIException;
}
