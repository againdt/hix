package com.getinsured.ahbx.platform.ecm.webclient;

import javax.xml.bind.JAXBElement;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.ahbx.platform.ecm.Content;
import com.getinsured.ahbx.platform.ecm.GetFile;
import com.getinsured.ahbx.platform.ecm.GetFileResponse;
import com.getinsured.ahbx.platform.ecm.mapper.GetContentMapper;
import com.getinsured.ahbx.util.AHBXUtils;
import com.getinsured.ahbx.util.GhixAhbxConstants;
import com.getinsured.hix.model.PlatformRequest;
import com.getinsured.hix.model.PlatformResponse;

/**
 *
 * Class to invoke AHBX ECM SOAP Service.
 *
 * @author Ekram Ali Kazi
 *
 */
@Component
public class GetContentAction {

	private static final Logger LOGGER = Logger.getLogger(GetContentAction.class);

	@Value("#{configProp['ecmServiceWSDL']}")
	private String wsdlUrl;

	//@Autowired private GIHTTPAuthClient gihttpAuthClient;
	
	@Autowired
	private WebServiceTemplate webServiceTemplate;
	
//	@Autowired private SoapWebClient<JAXBElement<GetFileResponse>> soapWebClient;

	public String getFileContent(PlatformRequest platformRequest) {

		/**
		 * Initiate Platform Response Object, to be returned to the caller
		 */
		PlatformResponse platformResponse = new PlatformResponse();
		platformResponse.startResponse();
		platformResponse.setAppID(wsdlUrl);

		/**
		 * Populate AHBX GetFile Request Object
		 */
		JAXBElement<GetFile> getFileRequest = formAHBXRequest(platformRequest);
		//LOGGER.info("GetFile request - " + getFileRequest.getValue());

		JAXBElement<GetFileResponse> ahbxResponse = null;

		try {
			webServiceTemplate.setDefaultUri(wsdlUrl);
			/*The below line is added for basic authentication for ECM.
			 *  As per request commenting this on 15 Jan 2014 */
			//webServiceTemplate.setMessageSender(new CommonsHttpMessageSender(gihttpAuthClient.getHttpClient()));
			ahbxResponse = (JAXBElement<GetFileResponse>) webServiceTemplate.marshalSendAndReceive(getFileRequest);
//			ahbxResponse = soapWebClient.send(getFileRequest, wsdlUrl, "SHARED-SERVICES-ECM-GET", webServiceTemplate);
		} catch (WebServiceIOException wsIOE) {
			LOGGER.error(wsIOE);
			platformResponse.setModuleStatusCode("P-E101");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(wsIOE.getMessage());
		} catch (SoapFaultClientException sfce) {
			LOGGER.error(sfce);
			LOGGER.error("Fault Code : " + sfce.getFaultCode());
			LOGGER.error("Fault Reason : " + sfce.getFaultStringOrReason());
			LOGGER.error("Fault Role : " + sfce.getSoapFault().getFaultActorOrRole());
			platformResponse.setModuleStatusCode("P-E102");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(sfce.getMessage());
		} catch (Exception e) {
			LOGGER.error(e);
			platformResponse.setModuleStatusCode("P-E103");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(e.getMessage());
		}

		//LOGGER.info("GetFile response - " + ahbxResponse);
		/**
		 * If AHBX Response found, populate Platform Response Object, in this
		 * case AhbxEcmId
		 */
		if (ahbxResponse != null) {
			com.getinsured.hix.dto.platform.ecm.Content content = processAHBXResponse(ahbxResponse);

			if (content == null) {
				platformResponse.setModuleStatusCode("P-E104");
				platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				platformResponse.setErrMsg("GetFile response received is null.");
			} else {
				platformResponse.setModuleStatusCode("P-E000");
				platformResponse.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);
				platformResponse.setGiContent(content);
			}
		}

		platformResponse.endResponse();

		/**
		 * Marshal and send response back to the caller
		 */
		return AHBXUtils.marshal(platformResponse);
	}

	private com.getinsured.hix.dto.platform.ecm.Content processAHBXResponse(
			JAXBElement<GetFileResponse> ahbxResponse) {
		//LOGGER.info("GetFile response - " + ahbxResponse.getValue());

		// populate giContentObject from abhxContentObject
		Content abhxContentObject = ahbxResponse.getValue().getReturn();

		if (null == abhxContentObject) {
			LOGGER.info("GetFile response received is null - " + ahbxResponse.getValue());
			return null;
		}

		return GetContentMapper.map(abhxContentObject);
	}



	private JAXBElement<GetFile> formAHBXRequest(PlatformRequest platformRequest) {

		return GetContentMapper.map(platformRequest.getEcmRequest());
	}

}
