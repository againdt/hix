<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<div class="ps-compare__plan-tiles" id="mainSummaryCmp">
    <div class="ps-compare__placeholder"></div>
</div>

<div id="benefitHead" style="display:none">
  <div class="accordion benefits">
    <div class="accordion-group"><!-- Summary -->
      <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseOne" aria-expanded="true">
          <i class="icon-chevron-down"></i> <spring:message code="pd.label.title.summary"/>
        </a>
      </div>
      <div id="collapseOne" class="accordion-body collapse in" aria-expanded="true">
        <div class="accordion-inner">
          <div class="row-fluid">
            <div class="u-flex-display" id="smartScoreRow">
              <div class="u-flex-25">
              	<c:if test="${gpsVersion == 'V1'}">
              	  <spring:message code="pd.label.title.getInsuredPlanScore"/>
              	</c:if>
              </div>
            </div>
            
            <div class="u-flex-display" id="planType">
              <div class="u-flex-25">
                <p><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.summary.producttype" htmlEscape="true"/>"><spring:message code="pd.label.summary.planType"/> </a></p>
              </div>
            </div>
            
            <div class="u-flex-display" id="planTier">
              <div class="u-flex-25"><p><spring:message code="pd.label.summary.planTier"/></p></div>
            </div>
            
            <c:if test="${dentalGuaranteedEnable=='ON'}">
            <div class="u-flex-display" id="premiumType">
 	 	 	 <div class="u-flex-25">
 	 	 	 	<p><a data-placement="right" rel="tooltip" href="#" data-original-title=""><spring:message code="pd.label.summary.premiumType"/> </a></p>
			 </div>
 	 	 	</div>
 	 	 	</c:if>
 	 	 	 	 	
          </div>
        </div>
      </div>
    </div>  <!-- Summary -->
    
    <div class="accordion-group">
      <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseTwo" aria-expanded="true">
          <i class="icon-chevron-down"></i> <spring:message code="pd.label.commontext.deductible"/> &amp; <spring:message code="pd.label.icon.outOfPocket"/>
        </a>
      </div>
      <div id="collapseTwo" class="accordion-body collapse in" aria-expanded="true">
        <div class="accordion-inner">
          <div class="row-fluid">
            <div class="u-flex-display" id="planDeductibleFly">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.deductible.ehb"/> (<spring:message code="pd.label.commontext.family"/>)</p>
              </div>
            </div>
            <div class="u-flex-display" id="planDeductibleInd">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.deductible.ehb"/> (<spring:message code="pd.label.commontext.individual"/>) </p>
              </div>
            </div>
            
            <div class="u-flex-display" id="planOopMaxFly">
              <div class="u-flex-25">
               <p><spring:message code="pd.label.dentalDeductible.link2"/> (<spring:message code="pd.label.commontext.family"/>)</p>
             </div>
             </div>
            <div class="u-flex-display" id="planOopMaxInd">
              <div class="u-flex-25">
                <p><spring:message code="pd.label.dentalDeductible.link2"/> (<spring:message code="pd.label.commontext.individual"/>)</p>
              </div>
            </div>
            
            <c:if test="${stateCode != 'MN' }">
	            <div class="u-flex-display" id="planAdultDeductibleFly">
	              <div class="u-flex-25">
	                <p><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultDeductible" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultDeductible"/></a> (<spring:message code="pd.label.commontext.family"/>)</p>
	              </div>
	            </div>
	            <div class="u-flex-display" id="planAdultDeductibleInd">
	              <div class="u-flex-25">
	                <p><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultDeductible" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultDeductible"/></a> (<spring:message code="pd.label.commontext.individual"/>)</p>
	              </div>
	            </div>
	            <div class="u-flex-display" id="planAdultAnnualBenefitLimitFly">
	              <div class="u-flex-25">
	                <p><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultAnnualBenefitLimit" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultAnnualBenefitLimit"/></a> (<spring:message code="pd.label.commontext.family"/>)</p>
	              </div>
	            </div>
	            <div class="u-flex-display" id="planAdultAnnualBenefitLimitInd">
	            <div class="u-flex-25">
	                <p><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultAnnualBenefitLimit" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultAnnualBenefitLimit"/></a> (<spring:message code="pd.label.commontext.individual"/>)</p>
	              </div>
	            </div>
	            <div class="u-flex-display" id="planAdultOutOfPocketMaxFly">
	              <div class="u-flex-25">
	                <p><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultOutOfPocketMax" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultOutOfPocketMax"/></a> (<spring:message code="pd.label.commontext.family"/>)</p>
	              </div>
	            </div>
	            <div class="u-flex-display" id="planAdultOutOfPocketMaxInd">
	              <div class="u-flex-25">
	                <p><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultOutOfPocketMax" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultOutOfPocketMax"/></a> (<spring:message code="pd.label.commontext.individual"/>)</p>
	            </div>
	            </div>
	         </c:if>   
          </div>          
        </div>
      </div>
    </div>
    
    
    
    
    <div class="accordion-group">
        <div class="accordion-heading">
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseThree" aria-expanded="false">
            <i class="icon-chevron-right"></i> <spring:message code="pd.label.commontext.dental.adultCovg"/>
          </a>
        </div>
        <div id="collapseThree" class="accordion-body collapse" aria-expanded="false">
          <div class="accordion-inner">
            <div class="row-fluid">
            
             <div class="u-flex-display" id="orthodontiaAdult">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.link7"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="majorDentalCareAdult">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.majorAdult"/></p>
                </div>
              </div>
              
              <div class="u-flex-display" id="accidentalDental">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.accidental"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="basicDentalCareAdult">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.basicAdult"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="rtnDentalAdult">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.link9"/></p>
                </div>
              </div>              
            </div>
          </div>
        </div>
      </div>


<div class="accordion-group">
        <div class="accordion-heading">
          <a class="accordion-toggle" data-toggle="collapse" data-parent="#benefitHead" href="#collapseFour" aria-expanded="false">
            <i class="icon-chevron-right"></i> <spring:message code="pd.label.commontext.dental.childCovg"/>
          </a>
        </div>
        <div id="collapseFour" class="accordion-body collapse" aria-expanded="false">
          <div class="accordion-inner">
            <div class="row-fluid">
              <div class="u-flex-display" id="orthodontiaChild">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.link8"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="majorDentalCareChild">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.majorChild"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="dentalCheckupChildren">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.checkup"/></p>
                </div>
              </div>
              <div class="u-flex-display" id="basicDentalCareChild">
                <div class="u-flex-25">
                  <p><spring:message code="pd.label.children.dental.basicChild"/></p>
                </div>
              </div>      
                           
            </div>
          </div>
        </div>
      </div>

</div>
      
</div>

<script type="text/html" id="mainSummaryCmpTpl">
<div class="ps-compare__plan-tile" tabindex="0" aria-label="You are on plan <@=name@>">
	<!-- tile body -->
    <div class="cp-tile__body">
		<!-- logo img -->
		<div id="detail_<@=id@>" class="cp-tile__img-link cp-tile__img-link-white detail">
			<@ if (issuer.length > 24) {@>
				<img src="<@=issuerLogo@>" alt="<@=issuer.substr(0,24)@>..." class="cp-tile__img">
			<@ } else { @>
				<img src="<@=issuerLogo@>" alt="<@=issuer.substr(0,24)@>..." class="cp-tile__img">
			<@ } @>
		</div>
		<!-- logo img end-->

		<!-- plan name -->
        <div class="cp-tile__plan-name">
			<a class="detail" href="#" id="detail_<@=id@>" data-placement="bottom" rel="tooltip" role="tooltip" data-original-title="<@=name@>" aria-label="<@=name@>">
				<@ if (name.length > 24) {@>
					<@=name.substr(0,24)@>...
				<@ } else { @>
					<@=name@>
				<@ } @>
			</a>
		</div>
		<!-- plan name end-->

		<!-- metal tier -->
		<div class="cp-tile__metal-tier">
			<@=level@> &nbsp;<@=networkType@>
		</div>
		<!-- metal tier end-->

		<!-- current plan -->
		<@ if ($("#enrollmentType").val() == 'A' && $("#initialDentalPlanId").val() == planId){ @>
			<div>
				<span class="cp-tile__current-plan">
					<spring:message code="pd.label.title.yourCurrentPlan"/>
				</span>
			</div>
		<@ } @>
		<!-- current plan end -->

		<!-- premium -->
		<div class="cp-tile__premium">
			<span class="cp-tile__premium-amount">$<@=premiumAfterCredit.toFixed(2) @></span>
			<span class="cp-tile__premium-month"><spring:message code="pd.label.title.premiumPerMonth" htmlEscape="true"/></span>
		</div>
		<!-- premium end-->

		<!-- tax credit -->
		<div class="cp-tile__tax-credit">
			<@ if (aptc > 0 && $('#exchangeType').val() != 'OFF') {@>
				<spring:message arguments="<@=aptc.toFixed(2)@>" code="pd.label.commontext.afterTaxCredit"/>
			<@ } @>
		</div>
		<!-- tax credit end -->	
	</div>
	<!-- tile body end-->

	<!-- tile footer -->
	<div class="ps-compare__tile-footer">
		<@ if ($('#stateCode').val() != "CT" && ($("#flowType").val() == 'PLANSELECTION' || $("#flowType").val() == 'PREELIGIBILITY')){ @>
			<@ if ( planId == 0) { @>
				<span id="cart_<@=planId@>" class="btn btn-primary ps-detail__tile-add addToCart ps-prev_year-btn" title="<spring:message code="pd.label.title.prevYearPlan"/>">
                    <spring:message code="pd.label.title.prevYearPlan"/>
                </span>
			<@ } else { @>
				<a href="#" id="cart_<@=planId@>" class="btn btn-primary ps-detail__tile-add addToCart" title="<spring:message code="pd.label.title.add"/>">
					<spring:message code="pd.label.title.add"/>
					<i class="icon-shopping-cart"></i>
			    </a>
            <@ } @> 
		<@ } @>
	</div>
	<!-- tile footer ends -->
</div>
</script>

<script type="text/html" id="smartScoreTpl">
<@ if($("#gpsVersion").val() == 'V1' && SMART_SCORE != '0') { @>
  <div class="plan-details">
    <@ if(SMART_SCORE >=80){ @>
      <div class="tile best">
		<a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.green" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
    <@ } else if(SMART_SCORE >=60){ @>
      <div class="tile ok">
		<a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.yellow" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
	
    <@ } else { @>
      <div class="tile poor">
 		<a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.red" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
  <@ } @>

    <div class="score">
	  <div class="circle-wrapper">
	  	<div class="center-circle"></div>
	    <div class="slice">
	      <div class="pie score_<@=SMART_SCORE@> pie-color"></div>
	    </div>
	    <div class="circle-core center-circle"> 
	      <div class="circle-num"><@=SMART_SCORE@></div>
	    </div>
	  </div>
	</div>
  </a></div>
</div>	
<@ } @>
</script>

<script type="text/html" id="planTypeTpl">
 <@ if($(".smartScoreVal").val() >= 80) { @>
 	      <div class="plan-details u-flex-25-gs best">
 	 <@ } else if($(".smartScoreVal").val() >= 60) { @>
 	      <div class="plan-details u-flex-25-gs ok">
 	 <@ } else { @>
    	   <div class="plan-details u-flex-25-gs poor">
  <@ } @><p>
	<@ if(NETWORK_TYPE == 'PPO') { @> <a data-placement="left" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.ppo"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'HMO') { @><a data-placement="left" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.hmo"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'POS') { @><a data-placement="left" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.pos"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'DHMO') { @><a data-placement="left" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.dhmo"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'DPPO') { @><a data-placement="left" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.dppo"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'HSA') { @><a data-placement="left" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.hsa"/>"><@=NETWORK_TYPE@></a>
    <@ } else { @> <@=NETWORK_TYPE@> <@ } @> </p>
  </div>
</script>

<script type="text/html" id="planTierTpl">
 	 <div class="plan-details u-flex-25-gs">
	 	<p> <@=LEVEL@></p>
 	 </div>
</script>

<script type="text/html" id="premiumTypeTpl">
 	 <div class="plan-details u-flex-25-gs">
	 	<p> <@=DENTAL_GURANTEE@></p>
 	 </div>
</script>

<script type="text/html" id="deductibleIndTpl">
   <@ if($(".smartScoreVal").val() >= 80) { @>
 	      <div class="plan-details u-flex-25-gs best">
 	 <@ } else if($(".smartScoreVal").val() >= 60) { @>
 	      <div class="plan-details u-flex-25-gs ok">
 	 <@ } else { @>
    	   <div class="plan-details u-flex-25-gs poor">
  <@ } @>

	<@ if(DEDUCTIBLE != null) { @>

		<@ if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() == "1") {@>

			<@ if(DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != ""){@>
    	  		<p>$<@=parseFloat(DEDUCTIBLE.inNetworkInd).toFixed()@></p>
    		<@ } else if(DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != ""){ @>
        		<p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkInd).toFixed()@></p>
      		<@ } else { @>
      			<p><spring:message code="pd.label.commontext.notApp" htmlEscape="true"/></p>
    		<@ } @>

		<@ } else if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1" ){ @>

			<@ if(DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != ""){@>
   	  			<p>$<@=parseFloat(DEDUCTIBLE.inNetworkFlyPerPerson).toFixed()@></p>
   			<@ } else if(DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != ""){ @>
       			<p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFlyPerPerson).toFixed()@></p>
    		<@ } else { @>
      			<p><spring:message code="pd.label.commontext.notApp" htmlEscape="true"/></p>
    		<@ } @>
		<@} @>

	<@ } else { @>
		<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
  	<@ } @>

</div>
</script>

<script type="text/html" id="deductibleFlyTpl">
   <@ if($(".smartScoreVal").val() >= 80) { @>
 	      <div class="plan-details u-flex-25-gs best">
 	 <@ } else if($(".smartScoreVal").val() >= 60) { @>
 	      <div class="plan-details u-flex-25-gs ok">
 	 <@ } else { @>
    	   <div class="plan-details u-flex-25-gs poor">
  <@ } @>
	<@ if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1" ){ @>
	<@ if(DEDUCTIBLE != null) { @>
      <@ if(DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != ""){ @>
        <p>$<@=parseFloat(DEDUCTIBLE.inNetworkFly).toFixed()@></p>
      <@ } else if(DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != ""){ @>
        <p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFly).toFixed()@></p>
      <@ } else { @>
        <p><spring:message code="pd.label.commontext.notApp" htmlEscape="true"/></p>				
      <@ } @>
	<@ } else { @>
	  <spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
    <@ } @>
  <@ } else { @>
 	  <p><spring:message code="pd.label.commontext.notApplicable" htmlEscape="true"/></p>
  <@ } @>
  </div>
</script>

<script type="text/html" id="benefitTpl">
<@ if($(".smartScoreVal").val() >= 80) { @>
    <div class="plan-details u-flex-25-gs best">
  <@ } else if($(".smartScoreVal").val() >= 60) { @>
    <div class="plan-details u-flex-25-gs ok">
  <@ } else { @>
    <div class="plan-details u-flex-25-gs poor">
  <@ } @>
  <@ if(BENEFIT != null && BENEFIT.displayVal != "") { @>
	<@=BENEFIT.displayVal@>
  <@ } @>
  <@ if(EXPLANATION != null) { @>
  <@ if(( EXPLANATION.displayVal != null && EXPLANATION.displayVal != "" ) && ( EXCLUSION.displayVal != null && EXCLUSION.displayVal != "" )) {@>
	<p><a href="#" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=EXPLANATION.displayVal@>,<@=EXCLUSION.displayVal@>" code="pd.tooltip.title.explanationAndExclusions" htmlEscape="false"/>"><spring:message code="pd.label.title.additionalInformation"/></a></p>
  <@} else if(( EXPLANATION.displayVal == null || EXPLANATION.displayVal == "" ) && ( EXCLUSION.displayVal != null && EXCLUSION.displayVal != "" )) {@>      
	<p><a href="#" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=EXCLUSION.displayVal@>" code="pd.tooltip.title.limitsAndExclusions" htmlEscape="false"/>"><spring:message code="pd.label.title.additionalInformation"/></a></p>
  <@} else if(( EXPLANATION.displayVal != null && EXPLANATION.displayVal != "" ) && ( EXCLUSION.displayVal == null || EXCLUSION.displayVal == "" )) {@>      
	<p><a href="#" rel="tooltip" data-placement="top" data-html="true" data-original-title="<spring:message arguments="<@=EXPLANATION.displayVal@>" code="pd.tooltip.title.benefitExplanation" htmlEscape="false"/>"><spring:message code="pd.label.title.additionalInformation"/></a></p>
  <@} @>
  <@ } @>
</div>
</script> 

<script type="text/javascript">
  window.scrollTo(0,0);
  function getSubToDeduct(APPLIES){
	var str = "";
	if(APPLIES != null && APPLIES.AppliesNetwk && APPLIES.AppliesNetwk != "" && APPLIES.AppliesNetwk != "Not Applicable"){
	  str += "<i class='has-deductible-"+APPLIES.AppliesNetwk+"'></i>";
	
	} else {
	  str += "<p class='txt-center'><small><spring:message code='pd.label.commontext.notAvailable' htmlEscape='true'/></small></p>";
	} 
	return str;
	
  }
</script>

<script type="text/javascript">
  window.scrollTo(0,0);
  function getSubToDeduct(APPLIES){
	var str = "";
	if(APPLIES != null && APPLIES.AppliesNetwk && APPLIES.AppliesNetwk != "" && APPLIES.AppliesNetwk != "Not Applicable"){
	  str += "<i class='has-deductible-"+APPLIES.AppliesNetwk+"'></i>";
	
	} else {
	  str += "<p class='txt-center'><small><spring:message code='pd.label.commontext.notAvailable' htmlEscape='true'/></small></p>";
	} 
	return str;
	
  }
</script>