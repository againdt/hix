const fs = require('fs');
const dir = "./build/static/js";

fs.readdir(dir, (err, files) => {
    files.forEach(file => {
        if(file.endsWith(".js") && file !== "main.js") {
            fs.copyFile(dir + "/" + file, dir + "/main.js", (err) => {
                if(err) throw err;
                console.log("Copied " + file + " to " + dir + "/main.js");
            })
        } else if(file.endsWith(".map") && file !== "main.js.map") {
            fs.copyFile(dir + "/" + file, dir + "/main.js.map", (err) => {
                if(err) throw err;
                console.log("Copied " + file + " to " + dir + "/main.js.map");
            })
        }
    })
})
