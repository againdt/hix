<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<ul class="breadcrumb">
	<a href="/">Home</a>
	<li><span class='divider'>/</span> <a href="/account/signup/employer">Employer Registration</a>
	<li><span class='divider'>/</span> <a href="/shop/employer/contact-info">Contact Information</a>
	<li><span class='divider'>/</span> <a href="/shop/employer/eligibilitya">Eligibility A</a>
	<li><span class='divider'>/</span> <a href="/shop/employer/verification/comp=a">Verification &amp; Attestation</a>
</ul>
<script type="text/javascript" src="/js/ajaxupload.js"></script>
<script type="text/javascript">
	function removeDoc(docId){
		$.get('verification',
			{op: 'REMOVEDOC',
			 docId: docId
			},				
			function(response){					
				$('#verficationDiv').html(response);
			}				
		);
	}
	
	function updateDoc(fileName,fileType){
		$.get('verification',
			{op: 'UPDATEDOC',
			 docComment: $('input[name=docType]:checked').val(),
			 otherDoc: $('#otherDoc').val(),
			 fileName: fileName,
			 fileType: fileType
			},				
			function(response){					
				$('#verficationDiv').html(response);
			}				
		);
	}
	
	</script>


<div class="row">
	<div class="span4">
		<h2>Employer Verification &#38; Attestation</h2>
		<p>In this page, we are asking you to authenticate your company. 
			The Minnesota Health Benefit Exchange accepts one of the following
			documents to authenticate your corporation. This is a mandatory
			requirement.</p>
	</div>
	<div class="span12">
		<form class="form-stacked" id="frmVerification" name="frmVerification" action="verification" method="post" enctype="multipart/form-data">
			<df:csrfToken/>
			<fieldset>
				<legend>Please upload either</legend>
				<div class="profile">
					<dt id="docType-label">&#160;</dt>
					<ul class="inputs-list">
						<li>
							<label for="docType-TheCertificateofIncorporation">
								<input type="radio" name="docType" id="docType-TheCertificateofIncorporation" value="The Certificate of Incorporation"
									onClick="$(&#39;input[name=docType]&#39;).attr(&#39;disabled&#39;,true);$(&#39;#fileElement&#39;).show();">
									The Certificate of Incorporation
							</label>
							<label for="docType-BusinessLicenseinthestate">
								<input type="radio" name="docType" id="docType-BusinessLicenseinthestate" value="Business License in the state"
									onClick="$(&#39;input[name=docType]&#39;).attr(&#39;disabled&#39;,true);$(&#39;#fileElement&#39;).show();">
									Business License in the state</label><label for="docType-Other">
								<input type="radio" name="docType" id="docType-Other" value="Other"
									onClick="$(&#39;input[name=docType]&#39;).attr(&#39;disabled&#39;,true);$(&#39;#fileElement&#39;).show();">
									Other
							</label>
						</li>
					</ul>
					<label for="otherDoc" class="optional">please specify:</label> <input
						type="text" name="otherDoc" id="otherDoc" value="" class="xlarge"
						size="30">
				</div>
				<!-- profile -->
			</fieldset>

			<div class="actions">
				<ul class="inputs-list">
					<li>
						<div class="clearfix" id="fileElement" style="display: none">
							<dt id="fileName-label">
								<label for="fileName" class="optional">Select a file</label>
							</dt>
							<dd>
								<input type="hidden" name="MAX_FILE_SIZE" value="8388608" id="MAX_FILE_SIZE"> 
								<input type="file" name="fileName" id="fileName" class="xlarge" size="30">
							</dd>
						</div>
						<div id="verficationDiv"></div>
					</li>
				</ul>
			</div>
