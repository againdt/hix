
package com.getinsured.iex.hub.ssac.niem.niem_core._2_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * A data type for a building, place, or structure that provides a particular service.
 * 
 * <p>Java class for FacilityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FacilityType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}FacilityName" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}FacilityLocation" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}FacilityContactInformation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FacilityType", propOrder = {
    "facilityName",
    "facilityLocation",
    "facilityContactInformation"
})
public class FacilityType
    extends ComplexObjectType
{

    @XmlElement(name = "FacilityName", nillable = true)
    protected ProperNameTextType facilityName;
    @XmlElement(name = "FacilityLocation", nillable = true)
    protected LocationType facilityLocation;
    @XmlElement(name = "FacilityContactInformation", nillable = true)
    protected ContactInformationType facilityContactInformation;

    /**
     * Gets the value of the facilityName property.
     * 
     * @return
     *     possible object is
     *     {@link ProperNameTextType }
     *     
     */
    public ProperNameTextType getFacilityName() {
        return facilityName;
    }

    /**
     * Sets the value of the facilityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProperNameTextType }
     *     
     */
    public void setFacilityName(ProperNameTextType value) {
        this.facilityName = value;
    }

    /**
     * Gets the value of the facilityLocation property.
     * 
     * @return
     *     possible object is
     *     {@link LocationType }
     *     
     */
    public LocationType getFacilityLocation() {
        return facilityLocation;
    }

    /**
     * Sets the value of the facilityLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *     
     */
    public void setFacilityLocation(LocationType value) {
        this.facilityLocation = value;
    }

    /**
     * Gets the value of the facilityContactInformation property.
     * 
     * @return
     *     possible object is
     *     {@link ContactInformationType }
     *     
     */
    public ContactInformationType getFacilityContactInformation() {
        return facilityContactInformation;
    }

    /**
     * Sets the value of the facilityContactInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactInformationType }
     *     
     */
    public void setFacilityContactInformation(ContactInformationType value) {
        this.facilityContactInformation = value;
    }

}
