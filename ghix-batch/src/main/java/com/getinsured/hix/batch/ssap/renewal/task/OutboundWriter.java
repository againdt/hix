package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.ssap.renewal.service.OutboundService;
import com.getinsured.hix.batch.ssap.renewal.util.OutboundPartitionerParams;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;

/**
 * Writer class is used to process Outbound data.
 * 
 * @since September 12, 2019
 */
public class OutboundWriter implements ItemWriter<Long> {

	private static final Logger LOGGER = LoggerFactory.getLogger(OutboundWriter.class);
	private long jobExecutionId = -1;

	private JobService jobService;
	private OutboundService outboundService;
	private OutboundPartitionerParams outboundPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		jobExecutionId = stepExecution.getJobExecution().getId();
	}

	@Override
	public void write(List<? extends Long> ssapApplicationIdListToWrite) throws Exception {

		String batchJobStatus = null;
		if (jobService != null && jobExecutionId != -1) {
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}

		if (batchJobStatus != null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING)
				|| batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))) {
			throw new UnexpectedJobExecutionException(EnrollmentConstants.BATCH_STOP_MSG);
		}

		if (CollectionUtils.isNotEmpty(ssapApplicationIdListToWrite)) {

			for (Long ssapApplicationId : ssapApplicationIdListToWrite) {

				try {
					outboundService.outboundSsap(ssapApplicationId);
				}
				catch (Exception e) {
					outboundService.logToGIMonitor(e, Long.toString(ssapApplicationId));
				}
			}
		}
		LOGGER.info("Successfully execute Outbound Job.");
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public OutboundService getOutboundService() {
		return outboundService;
	}

	public void setOutboundService(OutboundService outboundService) {
		this.outboundService = outboundService;
	}

	public OutboundPartitionerParams getOutboundPartitionerParams() {
		return outboundPartitionerParams;
	}

	public void setOutboundPartitionerParams(OutboundPartitionerParams outboundPartitionerParams) {
		this.outboundPartitionerParams = outboundPartitionerParams;
	}
}
