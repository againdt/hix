(function (){
    angular.module('statusServiceModule', [])
    .service('StatusService', function() {
        var agencyCertificationStatus = $("#agencyCertificationStatus").val();
        var agencyManagerCertificationStatus = $("#agencyManagerCertificationStatus").val();
        var currentUserRole = $("#currentUserRole").val();
        var adminSwitchToAgency = $("#adminSwitchToAgency").val();
        var isAdminRole = $("#isAdminRole").val();
        var adminStaffApprovalStatus = $("#adminStaffApprovalStatus").val();
        var adminStaffActivityStatus = $("#adminStaffActivityStatus").val();

        if(adminSwitchToAgency) {
            adminSwitchToAgency = true;
        } else {
            adminSwitchToAgency = false;
        }
        return {
            "CertificationStatus" : agencyCertificationStatus,
            "AgencyManagerCertificationStatus": agencyManagerCertificationStatus,
            "CurrentUserRole": currentUserRole,
            "AdminStaffApprovalStatus": adminStaffApprovalStatus,
            "AdminStaffActivityStatus": adminStaffActivityStatus,
            "AdminSwitchToAgency": adminSwitchToAgency,
            "IsAdminRole": isAdminRole
        };
    });
})();
