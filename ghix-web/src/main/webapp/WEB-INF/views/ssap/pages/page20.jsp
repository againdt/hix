<%@ include file="../include.jsp" %>
<div id="appscr70" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<strong class="nameOfHouseHold"></strong>
				<span><spring:message code="ssap.page20.curIncome" text="'s Current Income"/></span>
			</h4>
		</div>
		
		<form id="appscr70Form" class="form-horizontal">
			
			<div class="header">
			<h4>
				<spring:message code="ssap.page20.incomeSource" text="Select Income Sources "/>
			</h4>
			</div>
			
			<div class="gutter10">
			<div class="control-group">
				<fieldset>
				<legend>
					<spring:message code="ssap.page20.does" text="Does"/> <strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page20.haveAnyIncome" text="have any income?"/>&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</legend>
				<div class="margin5-l controls">
					<label class="radio" for="appscr70radios4IncomeYes">
						<input type="radio" id="appscr70radios4IncomeYes" name="appscr70radios4Income" onchange="selectIncomesCheckBoxes()" value="Yes" data-parsley-errors-container="#appscr70radios4IncomeDivErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />"/>
						<spring:message code="ssap.page20.yes" text="Yes"/>
					</label>
					<label class="radio" for="appscr70radios4IncomeNo">
						<input type="radio" id="appscr70radios4IncomeNo" name="appscr70radios4Income" onchange="selectIncomesCheckBoxes()" value="No" />
						<spring:message code="ssap.page20.no" text="No"/>
					</label>
					<div id="appscr70radios4IncomeDivErrorDiv"></div>
					<span class="errorMessage"></span>
				</div>
				</fieldset>
			</div><!-- control-group ends -->
			
			<div class="control-group hide" id="incomeSourceDiv">
				<fieldset>
				<legend>
					<spring:message code="ssap.page20.checkAllThatApply" text="Check all that apply."/>&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</legend>
				<div class="margin5-l">
					<label class="checkbox" for="Financial_None">
						<input type="checkbox" id="Financial_None" onchange="noneOfTheseAboveIncome()" parsley-mincheck="1" name="incomeSources" data-parsley-errors-container="#incomeSourceDivErrorDiv" data-parsley-required data-parsley-required-message ="<spring:message code='ssap.error.checkBoxRequired'/>"/>
						<span><spring:message code="ssap.page20.none" text="None"/> </span>
					</label>
					<label class="checkbox" for="Financial_Job">
						<input type="checkbox"  id="Financial_Job" onchange="unckeckToNoneCheckBox('Financial_Job')"  name="incomeSources" />
						<span><spring:message code="ssap.page20.job" text="Job"/></span>
					</label>
					<label class="checkbox" for="Financial_Self_Employment">
						<input type="checkbox"  id="Financial_Self_Employment" onchange="unckeckToNoneCheckBox('Financial_Self_Employment')" name="incomeSources" />
						<span><spring:message code="ssap.page20.selfEmp" text="Self-employment"/></span>
					</label>
					<label class="checkbox" for="Financial_SocialSecuritybenefits">
						<input type="checkbox"  id="Financial_SocialSecuritybenefits" onchange="unckeckToNoneCheckBox('Financial_SocialSecuritybenefits')" name="incomeSources" />
						<span><spring:message code="ssap.page20.socialSecurityBenefits" text="Social Security benefits "/></span>
					</label>
					<label class="checkbox" for="Financial_Unemployment">
						<input type="checkbox"  id="Financial_Unemployment" onchange="unckeckToNoneCheckBox('Financial_Unemployment')"  name="incomeSources" />
						<span><spring:message code="ssap.page20.unemp" text="Unemployment"/></span>
					</label>
					<label class="checkbox" for="Financial_Retirement_pension">
						<input type="checkbox"  id="Financial_Retirement_pension" onchange="unckeckToNoneCheckBox('Financial_Retirement_pension')" name="incomeSources" />
					<span><spring:message code="ssap.page20.retirement" text="Retirement/pension"/> </span>
					</label>
					<label class="checkbox" for="Financial_Capitalgains">
						<input type="checkbox"  id="Financial_Capitalgains" onchange="unckeckToNoneCheckBox('Financial_Capitalgains')" name="incomeSources" />
						<span><spring:message code="ssap.page20.capitalGain" text="Capital gains"/></span>
					</label>
					<label class="checkbox" for="Financial_InvestmentIncome">
						<input type="checkbox"   id="Financial_InvestmentIncome" onchange="unckeckToNoneCheckBox('Financial_InvestmentIncome')" name="incomeSources" />
					<span><spring:message code="ssap.page20.investmentIncome" text="Investment income"/></span>
					</label>
					<label class="capitalize-none checkbox" for="Financial_RentalOrRoyaltyIncome">
						<input type="checkbox"  id="Financial_RentalOrRoyaltyIncome" onchange="unckeckToNoneCheckBox('Financial_RentalOrRoyaltyIncome')" name="incomeSources" />
					<span> <spring:message code="ssap.page20.rentalRoyalIncome" text="Rental or royalty income "/></span>
					</label>
					<label class="capitalize-none checkbox" for="Financial_FarmingOrFishingIncome">
						<input type="checkbox"  id="Financial_FarmingOrFishingIncome" onchange="unckeckToNoneCheckBox('Financial_FarmingOrFishingIncome')"name="incomeSources" />
						<span><spring:message code="ssap.page20.farmingFishingIncome" text="Farming or fishing income "/></span> 
					</label>
					<label class="checkbox" for="Financial_AlimonyReceived">
						<input type="checkbox"  id="Financial_AlimonyReceived" onchange="unckeckToNoneCheckBox('Financial_AlimonyReceived')" name="incomeSources" />
						<span><spring:message code="ssap.page20.alimonyReceived" text="Alimony received"/></span>
					</label>
					<label class="checkbox" for="Financial_OtherIncome">
						<input type="checkbox"  id="Financial_OtherIncome" onchange="unckeckToNoneCheckBox('Financial_OtherIncome')" name="incomeSources" />
						<span><spring:message code="ssap.page20.otherIncome" text="Other income"/></span>
					</label>
					<div id= "incomeSourceDivErrorDiv"></div>
					<span class="errorMessage"></span>
				</div>
				</fieldset>
			</div><!-- control-group ends -->
			</div>
		</form>
	</div>
</div>