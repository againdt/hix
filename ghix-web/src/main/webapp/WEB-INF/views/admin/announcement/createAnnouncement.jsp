<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>


<script type="text/javascript">

 function getCurrentDate(){
	var currentDate = new Date();
    var year = currentDate.getFullYear();
    var currMonth = currentDate.getMonth()+1;
    var month = (currMonth > 9 ) ? currMonth : '0' + currMonth; 
    var day = currentDate.getDate();

    var today = month + '/' + day + '/' + year ;
    
    return today;
 }
 
jQuery.validator.addMethod("effectiveStartDateCheck", function (value, element, param) {
    var effectiveStartDate = $("#effectiveStartDate").val();
    var today = getCurrentDate() ;
    if (new Date(effectiveStartDate) < new Date(today)) {
        return false;
    }
    return true;
});


jQuery.validator.addMethod("effectiveEndDateCheck", function (value, element, param) {
	var effectiveEndDate = $("#effectiveEndDate").val();
    var today = getCurrentDate();
    if (new Date(effectiveEndDate) <= new Date(today)) {
        return false;
    }
    return true;
});

jQuery.validator.addMethod("checkDuration", function (value, element, param) {
	 var effectiveStartDate = new Date($("#effectiveStartDate").val());
	 var effectiveEndDate = new Date($("#effectiveEndDate").val());
	 
	 if(new Date(effectiveStartDate) >= new Date(effectiveEndDate)){
		 return false;
	 }return true;
});

function validateForm(){
	if( $("#frmcreateannouncement").validate().form() ) {
		 var validateUrl =  'chkName';
	       $.ajax({
			url:validateUrl, 
			type: "POST", 
			data: {name: $("#name").val()}, 
			success: function(response)
			{
				if(response == 'EXIST'){
					error = "<label class='error' generated='true'><span><em class='excl'>!</em>Announcement already exist.</span></label>";
					$('#name_error').html(error);
				 	return false;
				}else{
					$("#frmcreateannouncement").submit();
				}
			},
       });
    }
}
</script>

	
<div class="gutter10">	
        <div class="row-fluid">
            <ul class="page-breadcrumb">
                <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
                <li><a href="<c:url value="/admin/announcement/manage"/>">Manage Announcement</a></li>
                <li><spring:message  code="label.createAnnouncement"/></li>
            </ul><!--page-breadcrumb ends-->
            <h1><a name="skip"></a><spring:message  code="label.createAnnouncement"/></h1>
        </div>
	
		<div class="row-fluid">
        	<div id="sidebar" class="span3 ">
            
            </div>
            
			<div class="span9" id="rightpanel">		
				<div class="header">
					<h4>&nbsp;</h4>
				</div>
             <form class="form-horizontal gutter10" id="frmcreateannouncement" name="frmcreateannouncement" action="create" method="POST">
                    <div class="control-group">
                        <label class="control-label required" for="name" ><spring:message  code="label.announcementName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                        <div class="controls">
                            <input class="input-large" type="text"  name="name" id="name" value="" size="35">
                            <div id="name_error"></div>	
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="announcementText" class="control-label required"><spring:message  code="label.text"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                        <div class="controls">
                            <textarea name="announcementText" id="announcementText" class="xlarge" rows="2" cols="20" size="5"></textarea>
                            <div id="announcementText_error"  class="help-inline"></div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="effectiveStartDate" class="required control-label"><spring:message  code="label.effectiveStartDate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input title="MM/dd/yyyy" class="datepick input-small"  name="effectiveStartDate" id="effectiveStartDate" />
                                <div id="effectiveStartDate_error" class="help-inline"></div>
                            </div> <!-- end of controls-->
                    </div>
                    <div class="control-group">
                        <label for="effectiveEndDate" class="required control-label"><spring:message  code="label.effectiveEndDate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input title="MM/dd/yyyy" class="datepick input-small" name="effectiveEndDate" id="effectiveEndDate" />
                                <div id="effectiveEndDate_error" class="help-inline"></div>
                            </div> <!-- end of controls-->
                    </div>
                    <div class="control-group">
                        <label for="status" class="control-label required"><spring:message  code="label.status"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label> 
                        <div class="controls">
                            <select  name="status" id="status" class="input-medium">
                                <option value="PENDING">PENDING</option>
                                <option value="APPROVED">APPROVED</option>
                                <option value="DELETED">DELETED</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="roles" class="control-label required"><spring:message  code="label.roles"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                        <div class="controls">
                            <c:forEach var="role" items="${roles}"> 
                            <label class="checkbox">
                                <input type="checkbox" name="roles" id="roles" value="${role.id}">&nbsp;${role.name}&nbsp;&nbsp;&nbsp;
                            </label>
                            </c:forEach>
                            <div id="roles_error" class="help-inline"></div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="button" name="submitButton" id="submitButton" onClick="javascript:validateForm();" value="<spring:message  code="label.submit"/>" title="<spring:message  code="label.submit"/>" class="btn btn-primary"/>			
                    </div>
            </form>
        </div>
		</div>

		<div class="notes" style="display: none">
			<div class="row">
				<div class="span">
	
					<p>The prototype showcases three scenarios (A, B and C) dependant on
						a particular Employer's eligibility to use the SHOP Exchange.</p>
				</div>
			</div>
		</div>

</div> 
<script type="text/javascript">
$(function() {
	
	$('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/calendar.gif';
		$(this).datepicker({
			minDate: '0',
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true,
			option : $.datepicker.regional["en-GB"]
		});
	});
});

var validator = $("#frmcreateannouncement").validate({ 
	rules : { 'name':{required : true},
			  'announcementText':{required : true},
			  'effectiveStartDate':{required : true, date: true, checkDuration : true, effectiveStartDateCheck : true },
			  'effectiveEndDate':{required : true, date: true, checkDuration : true, effectiveEndDateCheck : true},
			  'roles':{required : true}
	},
	messages : {
			'name' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateName' javaScriptEscape='true'/></span>"},
			'announcementText' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateAnnouncementText' javaScriptEscape='true'/></span>"},
			'effectiveStartDate' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEffectiveStartDate' javaScriptEscape='true'/></span>",
				effectiveStartDateCheck:  "<span> <em class='excl'>!</em><spring:message code='label.validateStartDateVal' javaScriptEscape='true'/></span>" ,
				checkDuration : "<span> <em class='excl'>!</em><spring:message code='label.validateDurationStart' javaScriptEscape='true'/></span>",
				date : "<span> <em class='excl'>!</em><spring:message code='label.validateDateFormat' javaScriptEscape='true'/></span>"				
			},
			'effectiveEndDate' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEffectiveEndDate' javaScriptEscape='true'/></span>",
				effectiveEndDateCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateEndDateVal' javaScriptEscape='true'/></span>", 
				checkDuration : "<span> <em class='excl'>!</em><spring:message code='label.validateDurationEnd' javaScriptEscape='true'/></span>",
				date : "<span><em class='excl'>!</em><spring:message code='label.validateDateFormat' javaScriptEscape='true'/></span>"
		 	},
			'roles' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateRoles' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});
</script>