(function() {
    'use strict';

    angular.module('formatPhoneModule', [])
    .filter('giFormatPhone', FormatPhoneFilter);

    
    function FormatPhoneFilter() {
        return function(phoneNum) {
            if (!phoneNum) { return ''; }
            phoneNum = String(phoneNum);
            phoneNum = phoneNum.replace(/[^0-9]*/g, '');
            var formattedNumber = phoneNum;

            var c = (phoneNum[0] == '1') ? '1' : '';
            phoneNum = phoneNum[0] == '1' ? phoneNum.slice(1) : phoneNum;

            var area = phoneNum.substring(0, 3);
            var front = phoneNum.substring(3, 6);
            var end = phoneNum.substring(6, 10);

            if (front) {
                formattedNumber = (c + "(" + area + ") " + front);
            }
            if (end) {
                formattedNumber += ("-" + end);
            }
            return formattedNumber;
        };
    }


})();
