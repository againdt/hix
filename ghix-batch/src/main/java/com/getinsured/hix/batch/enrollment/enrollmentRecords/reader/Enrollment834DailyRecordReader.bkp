package com.getinsured.hix.batch.enrollment.enrollmentRecords.reader;

import static EnrollmentUtils.isNotNullAndEmpty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.database.JpaPagingItemReader;

import com.getinsured.hix.batch.enrollment.enrollmentRecords.IssuerFileList;
import com.getinsured.hix.batch.enrollment.skip.Enrollment834Out;
import com.getinsured.hix.batch.enrollment.xmlEnrollments.exceptions.InvalidRecordException;
import EnrollmentBatchService;
import ValidationFolderPathService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRace;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;

public class Enrollment834DailyRecordReaderbkp extends JpaPagingItemReader<Enrollment>{
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment834DailyRecordReaderbkp.class);
	Date startDate = null;
	Date endDate = 	null;
	boolean bIssuerValidated = false;
	String jobName;
	String enrollmentType;
	private ValidationFolderPathService validationFolderPathService = null;
	private UserService userService = null; 
	private ExecutionContext executionContext;
	private EnrollmentBatchService enrollmentBatchService;
	private String batchJobStatus;
	private IssuerFileList issuerFileList;
	private Enrollment834Out enrollment834Out;
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	//private static final String ENROLLMENT_TYPE_INDIVIDUAL = "FI";
	
	public ValidationFolderPathService getValidationFolderPathService() {
		return validationFolderPathService;
	}

	public void setValidationFolderPathService(
			ValidationFolderPathService validationFolderPathService) {
		this.validationFolderPathService = validationFolderPathService;
	}

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader ");
		
		ExecutionContext ec = stepExecution.getExecutionContext();
		 if(stepExecution.getJobExecution().getStatus()!=null){
			 batchJobStatus=(String)stepExecution.getJobExecution().getStatus().name();
		   }
		if(ec != null){
			executionContext = stepExecution.getJobExecution().getExecutionContext();
			String issuerIds = ec.get("issuerIds").toString();
			enrollmentType = ec.getString("enrollmentType");
			Object carrierResendFlag = ec.get("carrierResendFlag");
			startDate = (Date) ec.get("startDate");
			endDate = 	(Date) ec.get("endDate");
			jobName=(String)stepExecution.getJobExecution().getJobInstance().getJobName();
			
			String carriedResend = null;
			
			if(carrierResendFlag != null){
				carriedResend = (String ) carrierResendFlag;
			}
			
			LOGGER.info(Thread.currentThread().getName() + " : Setting query in beforeStep execution for Reader for Issuers: "+ issuerIds);
			if(issuerIds != null && enrollmentType != null) {
				String sql=null;
				
				if(jobName!=null && jobName.equalsIgnoreCase(EnrollmentConstants.INDIVIDUAL_ENROLLMENT_BATCH_JOB)){

				sql =	"SELECT distinct enrollment " +
						" FROM Enrollment enrollment, EnrollmentEvent event " +
						" where enrollment.issuer.id in (" + issuerIds + ") " +
						" and enrollment.enrollmentTypeLkp.lookupValueId = :enrollmentTypeLookupId " +
						" and ((event.createdOn BETWEEN :startDate AND :endDate) OR (event.extractionStatus = 'RESEND' AND event.createdOn <= :endDate)) " +
						" and event.enrollment = enrollment.id " +
						" and (event.sendToCarrierFlag = 'true' OR event.sendToCarrierFlag IS NULL) " +
						" and enrollment.enrollmentStatusLkp.lookupValueCode <>'ABORTED'";
				}else if(jobName!=null && jobName.equalsIgnoreCase(EnrollmentConstants.SHOP_ENROLLMENT_BATCH_JOB)){
					sql="SELECT distinct enrollment "+
						"FROM Enrollment enrollment, EnrollmentEvent event "+
						"where enrollment.issuer.id in ("+ issuerIds+ ") " +
						"and enrollment.enrollmentTypeLkp.lookupValueId = :enrollmentTypeLookupId "+
						"and ((event.createdOn BETWEEN :startDate AND :endDate) OR (event.extractionStatus = 'RESEND' AND event.createdOn <= :endDate)) " +
						"and event.enrollment = enrollment.id "+
						"and (event.sendToCarrierFlag = 'true' OR event.sendToCarrierFlag IS NULL) " +
						"and enrollment.enrollmentStatusLkp.lookupValueCode NOT IN ('ABORTED','PENDING')";
				}

				if(carrierResendFlag != null){
					sql += " and enrollment.carrierResendFlag = '" + carriedResend + "'";
				}

				sql += " ORDER BY enrollment.issuer.id, enrollment.createdOn ASC";
				
				setQueryString(sql);
				LOGGER.info(Thread.currentThread().getName() + " : Reader called for SQL : " + sql);
			}
		}
	}
	
	
	/*private boolean isDateBetween(Date startDate, Date endDate, Date dateToCompare){
		return ((dateToCompare.after(startDate) && dateToCompare.before(endDate)) || (dateToCompare.equals(startDate) || dateToCompare.equals(endDate)));
	}*/
	
		private void setEnrollmentsFor834Daily(Enrollment enrollment, Date startDate, Date endDate) throws InvalidRecordException {
		if(enrollment != null){
			List<Enrollee> enrolleesList=null;
			try{
			List<Enrollee> allEnrolleeList = enrollment.getEnrollees();
			try{
				if(isNotNullAndEmpty(enrollmentType) && enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
					if(enrollmentBatchService.isEnrollmentFromPendingToCancel(enrollment)){
						throw new InvalidRecordException("Enrollment is Cancelled from Pending for SHOP type enrollment id= " + enrollment.getId());					}
				}
				enrolleesList = filterEnrolleesForEDIDaily(allEnrolleeList, startDate,endDate);
			}catch(Exception e){
				throw new InvalidRecordException(e);
			}
			enrollment.setEnrollees(enrolleesList);
			}catch(InvalidRecordException e){
				throw e;
			}
			catch(Exception e){
				enrollment.setReaderStatus(EnrollmentConstants.ENROLLMENT_EVENT_STATUS_FAILED);
				enrollment.setReaderErrorMsg(e.getMessage());
			}
			if(enrollment.getReaderStatus()==null &&(enrolleesList == null || enrolleesList.isEmpty())){
				LOGGER.info(Thread.currentThread().getName() + " :  Skipping enrollment as Enrollee list is empty for enrollment id: " + enrollment.getId());
				throw new InvalidRecordException("Enrollment need not be extracted as it does not have any enrollee events in specified period " + enrollment.getId());
			}
		}
	}

	/*public void getFormatedRatingArea(Enrollee enrollee){
		if(enrollee != null && enrollee.getRatingArea() != null){
			String ratingArea = enrollee.getRatingArea();
			try{
				StringBuilder formatedRatingArea = new StringBuilder();
				//String stateCode = EnrollmentConstants.STATE_CODE;
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
				formatedRatingArea.append(EnrollmentConstants.RATING_AREA_PREFIX);
				formatedRatingArea.append(stateCode);

				NumberFormat numberFormater = NumberFormat.getInstance();
				numberFormater.setMinimumIntegerDigits(EnrollmentConstants.THREE);
				numberFormater.setMaximumFractionDigits(EnrollmentConstants.ZERO);
				numberFormater.setGroupingUsed(false);
				formatedRatingArea.append(numberFormater.format(Integer.parseInt(ratingArea)));
				enrollee.setRatingArea(formatedRatingArea.toString());
			}
			catch (Exception e){
				LOGGER.error("Error in formatting Rating Area for Enrollee with id:: " +  enrollee.getId() +  " -- " + e.getMessage() , e);
			}
		}
	}*/

		/**
		 * @author Aditya
		 * 
		 * @param enrolleeList
		 * @return
		 * @throws GIException
		 */
		/*private boolean isEnrollmentFromPendingToCancel(Enrollment enrollment) throws GIException {
			boolean isFromPendingToCancel = false;
			try{
				if(enrollment.getEnrollmentStatusLkp() != null && 
						enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
					List<EnrollmentAud> enrollmentAudList = enrollmentAudRepository.getEnrollmentByIdAndNoPendingStatus(enrollment.getId());
					if(enrollmentAudList == null || enrollmentAudList.isEmpty()){
						isFromPendingToCancel = true;
					}
				}
			}catch(Exception e){
				LOGGER.error("filterOutPendingToCancelEnrollments :: " + e.getMessage(),e);
				throw new GIException("Error in filterOutPendingToCancelEnrollments :: " + e.getMessage(),e);
			}
			
			return isFromPendingToCancel;
		}*/
	
	private List<Enrollee> filterEnrolleesForEDIDaily(List<Enrollee> allEnrollees, Date startDate,Date endDate) throws GIException{
		List<Enrollee> updatedEnrolleeList = null;
		try{
			if(allEnrollees!=null ){
				AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
				updatedEnrolleeList = new ArrayList<Enrollee>();
				for(Enrollee enrollee:allEnrollees){
					LOGGER.debug(Thread.currentThread().getName() +  " : Reader : filterEnrolleesForEDIDaily :: Started filtering for enrollee id="+enrollee.getId());
					if(enrollee.getPersonTypeLkp()!=null &&(enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER) )){
							List<EnrollmentEvent> allEvents= enrollee.getEnrollmentEvents();
							List<EnrollmentEvent> createdEvents= new ArrayList<EnrollmentEvent>();
							Date eventCreationDate=null;
							for(EnrollmentEvent event:allEvents){
								// This event.getSendToCarrierFlag() can be null, in case of null we need to send this to carrier.
								// Don't send only when this flag is false. This is false in case of renewal issuer is same and terminate prior enrollment.
								if(event.getSendToCarrierFlag() != null && !Boolean.parseBoolean(event.getSendToCarrierFlag())){
									LOGGER.debug(Thread.currentThread().getName() + " : Reader: Ignoring event : " + event.getId() + " as event.getSendToCarrierFlag() is false for Enrollee: " + enrollee.getId());
									continue;
								}
								eventCreationDate=event.getCreatedOn();
								if((!(eventCreationDate.before(startDate))&&!(eventCreationDate.after(endDate)))  || (event.getExtractionStatus() != null && event.getExtractionStatus().equalsIgnoreCase(EnrollmentConstants.RESEND_FLAG) && !event.getCreatedOn().after(endDate))){
									if(event.getCreatedBy() == null || ((isNotNullAndEmpty(user) && isNotNullAndEmpty(event.getCreatedBy())) && (event.getCreatedBy().getId() != user.getId()))){
										createdEvents.add(event);
									}else{
								LOGGER.debug(Thread.currentThread().getName() + " : Reader: Ignoring event : " + event.getId() + " as " + (event.getCreatedBy() == null ? null : event.getCreatedBy().getId())  +" is not same as " +  user.getId() + " for Enrollee: " + enrollee.getId());
							}
								}
							}
							//if((createdEvents != null && !createdEvents.isEmpty())||
							//		(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))){
							if((createdEvents!=null && !createdEvents.isEmpty())){
								enrollee.setEnrollmentEvents(createdEvents);
								updatedEnrolleeList.add(enrollee);
								List<EnrolleeRace> enrolleeRaceList =  enrollee.getEnrolleeRace();
								
							}else{
						LOGGER.debug(Thread.currentThread().getName() + " : Reader: Ignoring enrollee : " + enrollee.getId() + " as there are no events in the provided date range");
					}
					}else{
						LOGGER.debug( (Thread.currentThread().getName() + " : Reader: Ignoring enrollee : " + enrollee.getId() + " as personTypeLkp is " + (enrollee.getPersonTypeLkp() == null? null : enrollee.getPersonTypeLkp().getLookupValueCode())));
					}
				}
			}else{
				LOGGER.debug(Thread.currentThread().getName() + " : Reader: Enrollee list passed to filterEnrolleesForEDIDaily is null ");
			}
		}catch(Exception e){
			LOGGER.error("Exception in filterEnrolleesForEDIDaily()"+e.getMessage());
			throw new GIException("Exception in filterEnrolleesForEDIDaily()"+e.getMessage(),e);
		}
		return updatedEnrolleeList;
	}
	
	
	@Override
    public Enrollment doRead() throws Exception {
		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			if(issuerFileList!=null){
				issuerFileList.deleteAllFiles();
			}
			return null;
		}
		
		
		if(executionContext != null){
			Object jobExecutionStatus = executionContext.get("jobExecutionStatus");
			String jobStatus = null;
			
			if(jobExecutionStatus != null){
				jobStatus = (String)jobExecutionStatus;
			}
			
			if(jobStatus != null && jobStatus.compareToIgnoreCase("failed") == 0){
				LOGGER.info(Thread.currentThread().getName() +  " : Terminating thread " + Thread.currentThread().getName() + " due to failure of other thread execution");
				throw new GIException("Terminating thread " + Thread.currentThread().getName() + "due to failure of other thread execution");
			}
		}
		
		Enrollment enrollment = super.doRead();
		
		if(enrollment != null && enrollment.getIssuer() != null){
			setEnrollmentsFor834Daily(enrollment, startDate,endDate);
		}else{
			if(enrollment == null){
				LOGGER.info(Thread.currentThread().getName() + "Reader: setEnrollmentsFor834Daily was not called as Enrollment is null");
			}else{
				LOGGER.info(Thread.currentThread().getName() + "Reader: setEnrollmentsFor834Daily was not called as issuer is null for enrollment : " + enrollment.getId());
			}
		}
		return enrollment;
		
    }
	@Override
	public void doClose() throws Exception {
		super.doClose();
	}

	public EnrollmentBatchService getEnrollmentBatchService() {
		return enrollmentBatchService;
	}

	public void setEnrollmentBatchService(
			EnrollmentBatchService enrollmentBatchService) {
		this.enrollmentBatchService = enrollmentBatchService;
	}

	public IssuerFileList getIssuerFileList() {
		return issuerFileList;
	}

	public void setIssuerFileList(IssuerFileList issuerFileList) {
		this.issuerFileList = issuerFileList;
	}

	public Enrollment834Out getEnrollment834Out() {
		return enrollment834Out;
	}

	public void setEnrollment834Out(Enrollment834Out enrollment834Out) {
		this.enrollment834Out = enrollment834Out;
	}
	
	
}
