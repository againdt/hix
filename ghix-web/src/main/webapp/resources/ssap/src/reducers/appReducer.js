import { combineReducers } from 'redux';
import affordability from './memberAffordabilityReducer';
import relations from './bloodRelationsReducer';
import console from './consoleAdjusterReducer';
import contexts from './contextReducer';
import counties from './countiesReducer';
import countries from './countriesReducer';
import data from './dataReducer';
import direction from './directionReducer';
import error from './errorReducer';
import exception from './exceptionReducer';
import instructions from './instructionsReducer';
import flow from './flowReducer';
import form from './formReducer';
import headerLinks from './envReducer';
import language from './languageReducer';
import methods from './methodsReducer';
import messages from './messagesReducer';
import modals from './modalReducer';
import page from './activePageReducer';
import adjustedFlow from './adjustedFlowReducer';
import scroll from './scrollReducer';
import session from './sessionReducer';
import spinner from './spinnerReducer';
import tribes from './tribeReducer';

export default combineReducers({
    adjustedFlow,
    affordability,
    console,
    contexts,
    counties,
    countries,
    data,
    direction,
    error,
    exception,
    instructions,
    flow,
    form,
    headerLinks,
    language,
    methods,
    messages,
    modals,
    page,
    relations,
    scroll,
    session,
    spinner,
    tribes
});
