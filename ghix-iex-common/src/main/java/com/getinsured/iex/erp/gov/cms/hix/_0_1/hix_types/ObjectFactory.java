//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExtendedApplicantNonQHPCodeType }
     * 
     */
    public ExtendedApplicantNonQHPCodeType createExtendedApplicantNonQHPCodeType() {
        return new ExtendedApplicantNonQHPCodeType();
    }

    /**
     * Create an instance of {@link IncomeCompatibilityVerificationMethodCodeType }
     * 
     */
    public IncomeCompatibilityVerificationMethodCodeType createIncomeCompatibilityVerificationMethodCodeType() {
        return new IncomeCompatibilityVerificationMethodCodeType();
    }

    /**
     * Create an instance of {@link EmployerInsuranceSponsorshipStatusCodeType }
     * 
     */
    public EmployerInsuranceSponsorshipStatusCodeType createEmployerInsuranceSponsorshipStatusCodeType() {
        return new EmployerInsuranceSponsorshipStatusCodeType();
    }

    /**
     * Create an instance of {@link EligibilityBasisStatusCodeType }
     * 
     */
    public EligibilityBasisStatusCodeType createEligibilityBasisStatusCodeType() {
        return new EligibilityBasisStatusCodeType();
    }

    /**
     * Create an instance of {@link InsurancePlanVariantCategoryAlphaCodeType }
     * 
     */
    public InsurancePlanVariantCategoryAlphaCodeType createInsurancePlanVariantCategoryAlphaCodeType() {
        return new InsurancePlanVariantCategoryAlphaCodeType();
    }

    /**
     * Create an instance of {@link TDSFEPSAlphaCodeType }
     * 
     */
    public TDSFEPSAlphaCodeType createTDSFEPSAlphaCodeType() {
        return new TDSFEPSAlphaCodeType();
    }

    /**
     * Create an instance of {@link DisenrollmentReasonCodeType }
     * 
     */
    public DisenrollmentReasonCodeType createDisenrollmentReasonCodeType() {
        return new DisenrollmentReasonCodeType();
    }

    /**
     * Create an instance of {@link ExtendedApplicantEventCodeType }
     * 
     */
    public ExtendedApplicantEventCodeType createExtendedApplicantEventCodeType() {
        return new ExtendedApplicantEventCodeType();
    }

    /**
     * Create an instance of {@link ExpenseCategoryCodeType }
     * 
     */
    public ExpenseCategoryCodeType createExpenseCategoryCodeType() {
        return new ExpenseCategoryCodeType();
    }

    /**
     * Create an instance of {@link ActuarialValueMetallicTierCodeType }
     * 
     */
    public ActuarialValueMetallicTierCodeType createActuarialValueMetallicTierCodeType() {
        return new ActuarialValueMetallicTierCodeType();
    }

    /**
     * Create an instance of {@link EmploymentStatusCodeType }
     * 
     */
    public EmploymentStatusCodeType createEmploymentStatusCodeType() {
        return new EmploymentStatusCodeType();
    }

    /**
     * Create an instance of {@link VerificationCategoryCodeType }
     * 
     */
    public VerificationCategoryCodeType createVerificationCategoryCodeType() {
        return new VerificationCategoryCodeType();
    }

    /**
     * Create an instance of {@link AddressVerificationCodeType }
     * 
     */
    public AddressVerificationCodeType createAddressVerificationCodeType() {
        return new AddressVerificationCodeType();
    }

    /**
     * Create an instance of {@link InsuranceSourceCodeType }
     * 
     */
    public InsuranceSourceCodeType createInsuranceSourceCodeType() {
        return new InsuranceSourceCodeType();
    }

    /**
     * Create an instance of {@link SEPQualifyingEventCategoryCodeType }
     * 
     */
    public SEPQualifyingEventCategoryCodeType createSEPQualifyingEventCategoryCodeType() {
        return new SEPQualifyingEventCategoryCodeType();
    }

    /**
     * Create an instance of {@link IncomeCategoryCodeType }
     * 
     */
    public IncomeCategoryCodeType createIncomeCategoryCodeType() {
        return new IncomeCategoryCodeType();
    }

    /**
     * Create an instance of {@link ReferralActivityReasonCodeType }
     * 
     */
    public ReferralActivityReasonCodeType createReferralActivityReasonCodeType() {
        return new ReferralActivityReasonCodeType();
    }

    /**
     * Create an instance of {@link FrequencyCodeType }
     * 
     */
    public FrequencyCodeType createFrequencyCodeType() {
        return new FrequencyCodeType();
    }

    /**
     * Create an instance of {@link PercentageType }
     * 
     */
    public PercentageType createPercentageType() {
        return new PercentageType();
    }

    /**
     * Create an instance of {@link TaxReturnFilingStatusCodeType }
     * 
     */
    public TaxReturnFilingStatusCodeType createTaxReturnFilingStatusCodeType() {
        return new TaxReturnFilingStatusCodeType();
    }

    /**
     * Create an instance of {@link InformationExchangeSystemCategoryCodeType }
     * 
     */
    public InformationExchangeSystemCategoryCodeType createInformationExchangeSystemCategoryCodeType() {
        return new InformationExchangeSystemCategoryCodeType();
    }

    /**
     * Create an instance of {@link ImmigrationDocumentCategoryCodeType }
     * 
     */
    public ImmigrationDocumentCategoryCodeType createImmigrationDocumentCategoryCodeType() {
        return new ImmigrationDocumentCategoryCodeType();
    }

    /**
     * Create an instance of {@link CaretakerDependentCodeType }
     * 
     */
    public CaretakerDependentCodeType createCaretakerDependentCodeType() {
        return new CaretakerDependentCodeType();
    }

    /**
     * Create an instance of {@link DHSSAVEVerificationCodeType }
     * 
     */
    public DHSSAVEVerificationCodeType createDHSSAVEVerificationCodeType() {
        return new DHSSAVEVerificationCodeType();
    }

    /**
     * Create an instance of {@link ReferralActivityStatusCodeType }
     * 
     */
    public ReferralActivityStatusCodeType createReferralActivityStatusCodeType() {
        return new ReferralActivityStatusCodeType();
    }

    /**
     * Create an instance of {@link FamilyRelationshipHIPAACodeType }
     * 
     */
    public FamilyRelationshipHIPAACodeType createFamilyRelationshipHIPAACodeType() {
        return new FamilyRelationshipHIPAACodeType();
    }

    /**
     * Create an instance of {@link EligibilityProgramCodeType }
     * 
     */
    public EligibilityProgramCodeType createEligibilityProgramCodeType() {
        return new EligibilityProgramCodeType();
    }

    /**
     * Create an instance of {@link EnrollmentCodeType }
     * 
     */
    public EnrollmentCodeType createEnrollmentCodeType() {
        return new EnrollmentCodeType();
    }

    /**
     * Create an instance of {@link AbsentParentOrSpouseCodeType }
     * 
     */
    public AbsentParentOrSpouseCodeType createAbsentParentOrSpouseCodeType() {
        return new AbsentParentOrSpouseCodeType();
    }

    /**
     * Create an instance of {@link ContactPreferenceCodeType }
     * 
     */
    public ContactPreferenceCodeType createContactPreferenceCodeType() {
        return new ContactPreferenceCodeType();
    }

    /**
     * Create an instance of {@link VerificationStatusCodeType }
     * 
     */
    public VerificationStatusCodeType createVerificationStatusCodeType() {
        return new VerificationStatusCodeType();
    }

    /**
     * Create an instance of {@link ContactInformationCategoryCodeType }
     * 
     */
    public ContactInformationCategoryCodeType createContactInformationCategoryCodeType() {
        return new ContactInformationCategoryCodeType();
    }

    /**
     * Create an instance of {@link G845CodeType }
     * 
     */
    public G845CodeType createG845CodeType() {
        return new G845CodeType();
    }

    /**
     * Create an instance of {@link HouseholdSizeVerificationMethodCodeType }
     * 
     */
    public HouseholdSizeVerificationMethodCodeType createHouseholdSizeVerificationMethodCodeType() {
        return new HouseholdSizeVerificationMethodCodeType();
    }

    /**
     * Create an instance of {@link FFEVerificationCodeType }
     * 
     */
    public FFEVerificationCodeType createFFEVerificationCodeType() {
        return new FFEVerificationCodeType();
    }

    /**
     * Create an instance of {@link PlanYearCategoryCodeType }
     * 
     */
    public PlanYearCategoryCodeType createPlanYearCategoryCodeType() {
        return new PlanYearCategoryCodeType();
    }

}
