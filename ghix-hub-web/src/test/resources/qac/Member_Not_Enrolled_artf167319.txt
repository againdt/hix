{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload": {
    "taxHousehold": [
      {
        "householdMember": [
          {
            "insurance": {
              "insurancePolicyEffectiveDate": "05/04/2006",
              "insurancePolicyExpirationDate": "05/04/2006"
            },
            "dateOfBirth": "06/05/2006",
            "socialSecurityCard": {
              "socialSecurityNumber": "231554339"
            },
            "name": {
              "lastName": "Norton",
              "firstName": "Cal"
            }
          }
        ]
      }
    ]
  }
}