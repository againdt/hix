package com.getinsured.hix.ridp.web.controller;

import javax.servlet.http.HttpServletRequest;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.ridp.exception.RIDPServiceException;
import com.getinsured.hix.ridp.service.RIDPService;

/**
 * Exposes REST based API's for RIDP services and storing the RIDP verification input data 
 * and verification results to database 
 * 
 * @author Nikunj Singh
 * @since 3/16/2014
 */

@Controller
@RequestMapping("/ridp")
public class RIDPClientController {
		
	@Autowired
	private RIDPService ridpService;
	
	@Autowired
	private UserService userService;
	
	private static final Logger LOGGER = Logger.getLogger(RIDPClientController.class);

	@RequestMapping(value = "/currentUserInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'RIDP_CONTACT_INFO')")
	public @ResponseBody String currentUserInfo() throws RIDPServiceException {
		LOGGER.info("Inside currentUserInfo");
		return ridpService.currentUserInfo();
	}
	
	@RequestMapping(value = "/clearDHSReferenceNumber", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'RIDP_FARS')")
	public @ResponseBody String clearDHSReferenceNumber() throws RIDPServiceException {
		LOGGER.info("Inside clearDHSReferenceNumber");
		return ridpService.clearDHSReferenceNumber();
	}
	
	@RequestMapping(value = "/validateRIDPPrimary", method = RequestMethod.POST, 
			        produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'RIDP_CONTACT_INFO')")
	public @ResponseBody String validateRIDPPrimary(@RequestBody String jsonRequest) {
		LOGGER.info("Inside validateRIDPPrimary");
		
		String jsonResponse;
		try {
			jsonResponse = ridpService.validatePrimaryDetails(jsonRequest);
		} catch (RIDPServiceException e) {
			jsonResponse = ridpService.handleException(e);
		} catch (Exception e) {
			LOGGER.error("Error validating RIDP primary details", e);
			jsonResponse = ridpService.handleException(e);
		}
		return jsonResponse;
	}

	@RequestMapping(value = "/validateRIDPSecondary", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'RIDP_QUESTION')")
	public @ResponseBody String validateRIDPSecondary(@RequestBody String jsonRequest) {
		LOGGER.info("Inside validateRIDPSecondary");
		String jsonResponse = null;
		try {
			jsonResponse = ridpService.validateSecondaryDetails(jsonRequest);
		} catch (RIDPServiceException e) {
			jsonResponse = ridpService.handleException(e);
		} catch (Exception e) {
			LOGGER.error("Error validating RIDP secondary details", e);
			jsonResponse = ridpService.handleException(e);
		}
		return jsonResponse;
	}
	
	@RequestMapping(value = "/validateFARS", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'RIDP_FARS')")
	public @ResponseBody String validateFARS(@RequestBody String jsonRequest) {
		LOGGER.info("Inside validateFARS");
		String jsonResponse = null;
		try {
			jsonResponse = ridpService.validateFARS(jsonRequest);
		} catch (RIDPServiceException e) {
			jsonResponse = ridpService.handleException(e);
		} catch (Exception e) {
			LOGGER.error("Error validating FARS details", e);
			jsonResponse = ridpService.handleException(e);
		}
		return jsonResponse;
	}
	
	
	
	@RequestMapping(value = "/verification", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'RIDP_GET_STARTED')")
    public String getSTM(Model model, HttpServletRequest request)
    {
		String ssapFlowVersion = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_SSAP_FLOW_VERSION);
		model.addAttribute("ssapFlowVersion", ssapFlowVersion);
		return "verification";
    }
	
	@RequestMapping(value = "/manualVerification", method = RequestMethod.GET)
	public String manualVerification(Model model, HttpServletRequest request)
	{
		return "manualVerification";
	}

	@RequestMapping(value = "/manualVerificationFileUpload", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'RIDP_MANUAL_VERIFICATION')")
	public @ResponseBody String manualVerificationFileUpload(@RequestParam(value = "manualVerificationFile", required = false) CommonsMultipartFile uploadedFile, 
			MultipartHttpServletRequest mrequest)
	{
		LOGGER.info("Inside manualVerificationFileUpload");
		String jsonResponse = null;
		try {
			jsonResponse = ridpService.manualVerificationFileUpload(uploadedFile, mrequest.getParameter("doctype"));
		} catch (RIDPServiceException ridpServiceException) {
			jsonResponse = ridpService.handleException(ridpServiceException);
		} catch (Exception e) {
			LOGGER.error("Error during manual verfication document upload", e);
			jsonResponse = ridpService.handleException(e);
		}
		return jsonResponse;
	}
	
	@RequestMapping(value = "/csrOverrideForPaperIdentification", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'RIDP_CSR_PAPER_APPLICATION')")
    public @ResponseBody String csrOverrideForPaperIdentification(@RequestBody String jsonRequest) {
        LOGGER.info("Inside csrOverrideForPaperIdentification");
        String jsonResponse = null;
        try {
            jsonResponse = ridpService.csrOverrideForPaperIdentification();
        } catch (RIDPServiceException ridpServiceException) {
            jsonResponse = ridpService.handleException(ridpServiceException);
        } catch (Exception e) {
            LOGGER.error("Error in csr override for Paper Identification", e);
            jsonResponse = ridpService.handleException(e);
        }
        return jsonResponse;
    }
	
	@RequestMapping(value = "/pushToManualVerification", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'RIDP_CSR_PUSH_TO_MANUAL')")
	public @ResponseBody String pushToManualVerification(@RequestBody String jsonRequest) {
	    LOGGER.info("Inside pushToManualVerification");
	    String jsonResponse = null;
	    try {
	        jsonResponse = ridpService.pushToManualVerification();
	    } catch (RIDPServiceException ridpServiceException) {
	        jsonResponse = ridpService.handleException(ridpServiceException);
	    } catch (Exception e) {
	        LOGGER.error("Error in pushToManualVerification", e);
	        jsonResponse = ridpService.handleException(e);
	    }
	    return jsonResponse;
	}
	
	@RequestMapping(value = "/sendRidpVerificationToWso2", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String sendRidpVerificationToWso2(@RequestBody String userId) {
	    LOGGER.info("Inside pushToManualVerification");
	    String jsonResponse = null;
	    try {
	        jsonResponse = ridpService.sendRidpVerificationToWso2(userService.findById(Integer.valueOf(userId)));
	    } catch (RIDPServiceException ridpServiceException) {
	        jsonResponse = ridpService.handleException(ridpServiceException);
	    } catch (Exception e) {
	        LOGGER.error("Error in pushToManualVerification", e);
	        jsonResponse = ridpService.handleException(e);
	    }
	    return jsonResponse;
	}
}
