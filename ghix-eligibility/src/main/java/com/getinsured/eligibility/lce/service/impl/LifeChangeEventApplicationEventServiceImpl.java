package com.getinsured.eligibility.lce.service.impl;

import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.lce.service.LifeChangeEventApplicationEventService;
import com.getinsured.eligibility.lce.task.LifeChangeEventHandler;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;

@Service
public class LifeChangeEventApplicationEventServiceImpl implements
		LifeChangeEventApplicationEventService {
		
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired
	private SsapApplicantEventRepository ssapApplicantEventRepository;

	@Override
	public List<SsapApplicationEvent> cloneSsapApplicationEvent(SsapApplication currentApplication, Long userId, SsapApplicationEventTypeEnum eventType) throws Exception {
		List<SsapApplicationEvent> ssapApplicationEvents = new ArrayList<SsapApplicationEvent>();
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(userId.intValue());
		ssapApplicationEvent.setCreatedTimeStamp(new Timestamp(new TSDate().getTime()));
		ssapApplicationEvent.setSsapApplication(currentApplication);
		ssapApplicationEvent.setEventType(eventType);
		ssapApplicationEvent.setKeepOnly(LifeChangeEventConstant.NO);
		ssapApplicationEvents.add(ssapApplicationEvent);
		
		currentApplication.setSsapApplicationEvents(ssapApplicationEvents);
		
		return ssapApplicationEvents;
	}
}
