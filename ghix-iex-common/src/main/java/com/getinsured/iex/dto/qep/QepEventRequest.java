package com.getinsured.iex.dto.qep;

public class QepEventRequest {

	private Long ssapApplicationId;
	private String qepEvent;
	private String qepEventDate;
	private String caseNumber;
	private String changeType;
	private String applicationType;
	private Integer userId;
	
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public String getQepEvent() {
		return qepEvent;
	}
	public void setQepEvent(String qepEvent) {
		this.qepEvent = qepEvent;
	}
	public String getQepEventDate() {
		return qepEventDate;
	}
	public void setQepEventDate(String qepEventDate) {
		this.qepEventDate = qepEventDate;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getChangeType() {
		return changeType;
	}
	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}
	public String getApplicationType() {
		return applicationType;
	}
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	
	
}
