
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for I20DocumentID26Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="I20DocumentID26Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}I94Number" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}PassportCountry" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}SEVISID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}DocExpirationDate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "I20DocumentID26Type", propOrder = {
    "i94Number",
    "passportCountry",
    "sevisid",
    "docExpirationDate"
})
public class I20DocumentID26Type {

    @XmlElement(name = "I94Number")
    protected String i94Number;
    @XmlElement(name = "PassportCountry")
    protected PassportCountryType passportCountry;
    @XmlElement(name = "SEVISID", required = true)
    protected String sevisid;
    @XmlElement(name = "DocExpirationDate")
    protected XMLGregorianCalendar docExpirationDate;

    /**
     * Gets the value of the i94Number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getI94Number() {
        return i94Number;
    }

    /**
     * Sets the value of the i94Number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setI94Number(String value) {
        this.i94Number = value;
    }

    /**
     * Gets the value of the passportCountry property.
     * 
     * @return
     *     possible object is
     *     {@link PassportCountryType }
     *     
     */
    public PassportCountryType getPassportCountry() {
        return passportCountry;
    }

    /**
     * Sets the value of the passportCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link PassportCountryType }
     *     
     */
    public void setPassportCountry(PassportCountryType value) {
        this.passportCountry = value;
    }

    /**
     * Gets the value of the sevisid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEVISID() {
        return sevisid;
    }

    /**
     * Sets the value of the sevisid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEVISID(String value) {
        this.sevisid = value;
    }

    /**
     * Gets the value of the docExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDocExpirationDate() {
        return docExpirationDate;
    }

    /**
     * Sets the value of the docExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDocExpirationDate(XMLGregorianCalendar value) {
        this.docExpirationDate = value;
    }

}
