//1.7. Will current applicant be claimed as a dependent on someone else's federal income tax return next year? 

function resetErrorMsg(event, element) {
				var elementId = element.id;
				
				$("#" + elementId + "_error").html('');
				if(event.keyCode == 13){
					$("#changePwdSave").click();
				}
			}
			
			//Validating old password length and characters	
			jQuery.validator.addMethod("validateOldpassword", function(value,
					element, param) {
				oldpassword = $("#current-pswd").val();
				var re = /(?=^.{8,20}$)(?=.*[a-zA-Z])(?=.*[0-9])(?!.*\s).*$/; 

				if (!re.test(password)) {
					return false;
				}
				return true;

			});
			
			
			//Validating new password length and characters		
			jQuery.validator.addMethod("validatepassword", function(value,
					element, param) {
				//checkUserPassword(element,'CHK_COMPLEXITY');
				password = $("#newpassword").val();
				var re = RegExp(($("#passwordPolicyRegex").val()));///(?=^.{8,20}$)(?=.*[a-zA-Z])(?=.*[0-9])(?!.*\s).*$/; 
				if (!re.test(password)) {
					return false;
				} 
				return true;
			});
			
			//Validating new password length and characters		
			jQuery.validator.addMethod("xssnewpassword", function(value,
					element, param) {
				password = $("#newpassword").val();
				
				if(password.indexOf("\\0") > -1) {
					return false;
				}
				return true;
			});
			
			jQuery.validator.addMethod("validatepasswordforminage", function(value,
					element, param) {
				checkUserPassword(element,'CHK_MIN_AGE');
			});
			
			   //Validating confirm password and new password length and characters	
			jQuery.validator.addMethod("passwordcheck", function(value,
					element, param) {
				password = $("#newpassword").val();
				confirmed = $("#confirmpassword").val();

				if (password != confirmed) {
					return false;
				}
				return true;
			});

			
			var validator = $("#pwdchangeform")
					.validate(
							{
								rules : {
									currentpswd :{
										required : true
										//validateOldpassword :true
									},
									
									newpassword : { 
										required : true,
										validatepassword : true,
										xssnewpassword : true
									},
									confirmpassword : { 
										required : true, 
										passwordcheck : true
									}									
								},
								messages : {
								
									currentpswd:{
										required : "<span> <em class='excl'>!</em>Please enter your current password.</span>"
										//validateOldpassword : "<span> <em class='excl'>!</em> Please enter valid Password, the password must be at least 8 characters long with at least one letter and one number, no spaces. </span>"		
									},

									newpassword: { 
										required : "<span> <em class='excl'>!</em>Please enter your new password.</span>",
										validatepassword : "<span> <em class='excl'>!</em> "+$("#passwordPolicyErrorMsg").val()+" </span>",
										xssnewpassword : "<span> <em class='excl'>!</em>\\0 is not supported</span>"
										//validatepasswordforminage : "<span> <em class='excl'>!</em> Validating Password... </span>"
									},
									confirmpassword: { 
										required : "<span> <em class='excl'>!</em>Please confirm your new password.</span>",
										passwordcheck : "<span> <em class='excl'>!</em> Password and confirm password don't match.</span>"
									}									
								},
								onkeyup : false,
								errorClass : "error",
								errorPlacement : function(error, element) {
									var elementId = element.attr('id');
									error
											.appendTo($("#" + elementId
													+ "_error"));
									$("#" + elementId + "_error").attr('class',
											'error help-inline');
								}
								
							});
						
			var validator = $("#frmsecurityquesions").validate({ 
				rules : { securityQuestion1 : { required : true},
					securityQuestion2 : { required : true},
					securityQuestion3 : { required : true},
					securityAnswer1 : { required : true,maxlength : 4000},
					securityAnswer2 : { required : true,maxlength : 4000},
					securityAnswer3 : { required : true,maxlength : 4000}
				},
				messages : {
					
					securityQuestion1: { required : "<span> <em class='excl'>!</em>Please select your security question.</span>"},
					securityQuestion2 : { required : "<span> <em class='excl'>!</em>Please select your security question.</span>"},
					securityQuestion3: { required : "<span> <em class='excl'>!</em>Please select your security question.</span>"},
					securityAnswer1: { required : "<span> <em class='excl'>!</em>Please provide your security answer.</span>",
									   maxlength : "<span> <em class='excl'>!</em>Entered details should not be more than 4000 characters.</span>"},
					securityAnswer2 : { required : "<span> <em class='excl'>!</em>Please provide your security answer.</span>",
										maxlength : "<span> <em class='excl'>!</em>Entered details should not be more than 4000 characters.</span>"},
					securityAnswer3: { required : "<span> <em class='excl'>!</em>Please provide your security answer.</span>",
									   maxlength : "<span> <em class='excl'>!</em>Entered details should not be more than 4000 characters.</span>"}
					
				},
				errorClass: "error",
				errorPlacement: function(error, element) {
					var elementId = element.attr('id');
					error.appendTo($("#" + elementId + "_error"));
					$("#" + elementId + "_error").attr('class',
							'error help-inline');
				} 
				
			});
			
			$(document).keydown(function(e) {
			    // ESCAPE key pressed
			    if (e.keyCode == 27) {
			    	if ($("#frmsecurityquesions").valid()) {
			    		$("#cancelbutton2").click();
			        }
			        else {
			        	e.preventDefault();
			        	$("#cancelbutton1").click();
			        }
			    	
			    }
			});
			
			
			
function goToPage(url) {
	
	window.location=url;
	 return false;
}

function clearAnswer(Idx) {
	$("#securityAnswer"+Idx).val("");
}

function clearAnswerTwo(Idx){
	$("#securityAnswer"+Idx).val("");
}

function clearAnswerTwo(){
	$("#securityAnswer2").val("");
}
function clearAnswerOne(){
	$("#securityAnswer1").val("");
}
function clearAnswerThree(){
	$("#securityAnswer3").val("");
}

function checkUserPassword()
{
	var validateUrl = "/hix/account/signup/checkResetUserPasswordLoggedInUser";//"<c:url value='/account/signup/checkUserPassword'/>";
	
	$.ajax({
		url : validateUrl,
		type : "POST",
		data : {
			password : $("#newpassword").val(), action : '', oldpassword : $("#currentpswd").val(), csrftoken : $("#csrftoken").val()
		},
		success: function(response)
		{
			if (response!="")
			{
				if(response.indexOf('accountlocked') >= 0){
					$('#currentpswd_error').html("");
					$('#currentpswd_error').removeAttr("class");
					$('#newpassword_error').html("");
					$('#newpassword_error').removeAttr("class");
					//$("#pwdchangeform").append('<input type = "hidden" name="errorCode" value="accountLocked" />');
					$("#pwdchangeform").attr("action", "/hix/account/user/accountLockedOnChangePassword");
					$("#pwdchangeform").submit(); 
					
				}else if(response =="The current password doesn't match with our records."){
					$('#currentpswd_error').html("<label class='error'><span> <em class='excl'>!</em>"+response+"</span></label>");
					$('#currentpswd_error').attr('class','error help-inline');
				}
				else{
				$('#newpassword_error').html("<label class='error'><span> <em class='excl'>!</em>"+response+"</span></label>");
				$('#newpassword_error').attr('class','error help-inline');
				}
				/*$("#passwordPolicyErrorMsg").html(response)  ;
				document.getElementById('passwordPolicyErrorMsg').value = response;
				*/return false;
			}
			else{
				
						$('#currentpswd_error').html("");
						$('#currentpswd_error').removeAttr("class");
						$('#newpassword_error').html("");
						$('#newpassword_error').removeAttr("class");
						
						$("#pwdchangeform").submit();  	
				
			}
		},
		
	});
}

function checkUserCurrentPassword()
{
	var validateUrl = "/hix/account/signup/checkResetQuestionPasswordLoggedInUser";//"<c:url value='/account/signup/checkUserPassword'/>";
	
	$.ajax({
		url : validateUrl,
		type : "POST",
		data : {
			oldpassword : $('#currentpswdans').val(), csrftoken : $('#csrftoken').val()
		},
		success: function(response)
		{
			if (response !== "")
			{
				if(response === "The current password doesn't match with our records."){
					$('#currentpswdans_error').html("<label class='error help-inline'><span> <em class='excl'>!</em>"+response+"</span></label>");
				}
				return false;
			}
			else{
				$('#currentpswdans_error').html("");
				$('#currentpswdans_error').removeAttr("class");
				$("#frmsecurityquesions").submit();  	
			}
		},
		
	});
}

function checkCurrentPasswordAndEmail() {
	var newEmail = $("#newEmail").val();
	var confirmEmail = $('#confirmEmail').val();
	if (newEmail === confirmEmail){
		var validateUrl = "/hix/account/user/checkCurrentPasswordAndEmail";//"<c:url value='/account/signup/checkUserPassword'/>";
		$.ajax({
			url: validateUrl,
			type: "POST",
			data: {
				oldpassword: $('#currentpassword').val(),
				newEmailAddress: $("#newEmail").val(),
				csrftoken: $('#csrftoken').val()
			},
			success: function (response) {
				if (response !== "") {
					$("#change-email").modal('hide');
					$('<div class="modal popup-address"><div class="modal-header"><h3 class="pull-left">Confirmation Email</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><p>A confirmation email has been sent to ' + $("#newEmail").val() + '</p><p>Please check your email and click on the link provided to confirm this change.</p></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="window.location.reload();">OK</button></div></div>').modal({
						backdrop: "static",
						keyboard: "false"
					});
				}
				return false;
			},
			error: function (response) {
				if (response !== "") {
					if (response.status === 401) {
						$('#currentpassword_error').html("<label class='error help-inline'><span> <em class='excl'>!</em>Password you entered doesn't match your records</span></label>");
						//$("#change-email").modal('show');
						event.preventDefault();
					} else if (response.status === 406) {
						$('#newEmail_error').html("<label class='error help-inline'><span> <em class='excl'>!</em>Account already exists</span></label>");
					} else {
						$("#change-email").modal('hide');
						$('<div class="modal popup-address"><div class="modal-header"><h3 class="pull-left">Error!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><p>Sorry, we are experiencing issues at the moment, try again</p></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="window.location.reload();">OK</button></div></div>').modal({
							backdrop: "static",
							keyboard: "false"
						});
					}
				}
				return false;
			}
		});
	} else {
		$('#confirmEmail_error').html("<label class='error help-inline'><span> <em class='excl'>!</em>Please check your email address</span></label>");
	}
}

var validator = $("#change-email-address").validate(
		{
			rules : {
				currentpswdemail : {
					required : true
				},
				newEmail :{
					required : true,
					validEmailCheck: true
				},
				
				confirmEmail : { 
					required : true,
					confirmEmail : true,
					validEmailCheck : true
					
				}								
			},
			messages : {
				currentpswdemail:{
					required : "<span> <em class='excl'>!</em>Please enter your current password.</span>",
				},
				newEmail:{
					required : "<span> <em class='excl'>!</em>Please enter your email.</span>",
					validEmailCheck : "<span> <em class='excl'>!</em>Please enter valid email address.</span>"		
				},

				confirmEmail: { 
					required : "<span> <em class='excl'>!</em>Please enter your email.</span>",
					confirmEmail : "<span> <em class='excl'>!</em>Email and Confirm Email don't match.</span>",
					validEmailCheck : "<span> <em class='excl'>!</em>Please enter a valid email address.</span>"
				}
													
			},
			onkeyup : false,
			errorClass : "error",
			errorPlacement : function(error, element) {
				var elementId = element.attr('id');
				error.appendTo($("#" + elementId + "_error"));
				$("#" + elementId + "_error").attr('class', 'error help-inline');
			}
			
		});


jQuery.validator.addMethod("confirmEmail", function(value, element, param) {
	email = $("#newEmail").val();
	confirmEmail = $("#confirmEmail").val();

	return email != confirmEmail ? false : true;

});
jQuery.validator.addMethod("validEmailCheck", function(value, element, param) {
	email = $("#newEmail").val();
	//var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var emailReg = /^([\w-]+(?:\.[\w-]+)*@([\w-]+\.)+[\w-]{2,25})?$/;
	if (!emailReg.test(email)) {
		return false;
	} else {
		return true;
	}
	return false;
});
