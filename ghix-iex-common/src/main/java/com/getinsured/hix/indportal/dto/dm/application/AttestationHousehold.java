package com.getinsured.hix.indportal.dto.dm.application;

import java.util.ArrayList;

public class AttestationHousehold {
	private ArrayList < Object > familyRelationships;
	private ArrayList <ArrayList<String>  > taxRelationships;
	private ArrayList < Object > legalRelationships;
	public ArrayList<Object> getFamilyRelationships() {
		return familyRelationships;
	}
	public void setFamilyRelationships(ArrayList<Object> familyRelationships) {
		this.familyRelationships = familyRelationships;
	}
	public ArrayList<ArrayList<String>> getTaxRelationships() {
		return taxRelationships;
	}
	public void setTaxRelationships(ArrayList<ArrayList<String>> taxRelationships) {
		this.taxRelationships = taxRelationships;
	}
	public ArrayList<Object> getLegalRelationships() {
		return legalRelationships;
	}
	public void setLegalRelationships(ArrayList<Object> legalRelationships) {
		this.legalRelationships = legalRelationships;
	}


	 // Getter Methods 



	 // Setter Methods 


	}
