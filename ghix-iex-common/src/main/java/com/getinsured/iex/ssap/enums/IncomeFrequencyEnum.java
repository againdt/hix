
package com.getinsured.iex.ssap.enums;

/***
 * 
 * @author raguram_p
 *
 *
 */
public enum IncomeFrequencyEnum {

    ONE_TIME_ONLY,
    WEEKLY,
    EVERY_TWO_WEEKS,
    TWICE_A_MONTH,
    MONTHLY,
    ANNUALLY;

    public String value() {
        return name();
    }

    public static String value(String v) {
		if (v != null) {
			final IncomeFrequencyEnum data = readValue(v);
			if (data != null) {
				return data.value();
			}
		}
		return null;
	}

	public static IncomeFrequencyEnum fromValue(String v) {
		if (v != null) {
			return readValue(v);
		}
		return null;
	}

	private static IncomeFrequencyEnum readValue(String v) {
		IncomeFrequencyEnum data = null;
		for (IncomeFrequencyEnum c : IncomeFrequencyEnum.class.getEnumConstants()) {
			if (c.value().equals(v)) {
				data = c;
				break;
			}
		}
		return data;
	}


}
