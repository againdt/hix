<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div>
        <c:if test="${not empty migrationStatus }">
                <h6>Response Status :- <c:out value="${migrationStatus}"></c:out></h6>
        </c:if>
        <c:if test="${not empty recordsForMigration }">
                <h6>Records Given For Migration :- <c:out value="${recordsForMigration}"></c:out></h6>
        </c:if>
        <c:if test="${not empty successRecordCount }">
                <h6>Success Record Count :- <c:out value="${successRecordCount}"></c:out></h6>
        </c:if>
        <c:if test="${not empty failRecordCount }">
                <h6>Failed Record Count :- <c:out value="${failRecordCount}"></c:out></h6>
        </c:if>
        <c:if test="${not empty migrationFailureMessage }">
                <h4>Response from Finance API :- <c:out value="${migrationFailureMessage}"></c:out></h4>
        </c:if>
        <c:if test="${not empty failureResponseList }">
                <c:forEach items="${failureResponseList}" var="paymentMethodResponseList">
                        <c:forEach var="paymentMethodResponseMap" items="${paymentMethodResponseList}">
                                PaymentId: ${paymentMethodResponseMap.key}  - Exception: ${paymentMethodResponseMap.value}
                        </c:forEach>
                </c:forEach>
        </c:if>
</div>