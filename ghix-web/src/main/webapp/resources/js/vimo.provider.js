    $(".accordian-select #radio-yes").change(function(){
        $('#group-yes').addClass("in");
        $('#group-no').removeClass("in");
    });
    $(".accordian-select #radio-no").change(function(){
        $('#group-no').addClass("in");
        $('#group-yes').removeClass("in");
    });
    
    $('.delete').click(function(){ //delete the row
        $(this).parents('tr').html('');
    });
    
$('.modal a').click(function(e){
    e.preventDefault();
    var href = $(this).attr('href');

    if (href.indexOf('#') == 0) {
        return true;
    } else {
        $('.modal').load(href);
    }
});

$('document').ready(function(){
    $('.modal a').click(function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        if (href.indexOf('#') == 0) {
            return true;
        } else {
            $('.modal').load(href);
        }
    });
});
    
$('.modal a').click(function(e){
    e.preventDefault();
    var href = $(this).attr('href');

    if (href.indexOf('#') == 0) {
        return true;
    } else {
        $.get(href, function(data) {
            $('.modal').html(data);
            }).success(function() { $('input:text:visible:first').focus();
        });
    }
});
$('.tooltip-save').tooltip({'title': "Save as My Preferred Provider"})
$('.tooltip-save').click(function(e){
    e.preventDefault();
    giProviderCount++;
    $('#gi_provider_results .providerCount').text(giProviderCount)
})
/*
$('table .provider').click(function(){
    var lsUID = $(this).attr("id");
    $('.modal').html('details_r2.php?id=' + lsUID);
});
*/
    var giProviderCount = 3;
    var giPage =  0;
    var giDisplay = 7;
    var giDisplayStart = giPage * giDisplay + 1;
    var giDisplayEnd = giPage * giDisplay + giDisplay;
    var giRecordCount = $('table .provider').size();
    var giTotalPages = Math.ceil(giRecordCount/giDisplay);
    
    for(var i = 1;i <= giTotalPages; i++) { //place the pages
        if (i==1) {
            $('.pagination ul li:first').append('<li class="active"><a href="#' + (i - 1) + '">' + i + '</a></li>'); // start by showing the first page as "active"
        } else {
            $('.pagination ul li:first').append('<li><a href="#' + (i - 1) + '">' + i + '</a></li>');
        }
    }
    function fnShowResults(piDisplayStart,piDisplayEnd) {
        for(var i = piDisplayStart;i <= piDisplayEnd; i++) {
            var lsid = "#u_" + i + '' + i + '' + i
            $(lsid).show();
        }
    }
    
    $('.provider').hide();//hide all records
    fnShowResults(giDisplayStart,giDisplayEnd); // display the first set of records
    
    $('.pagination a').click(function(e){
        var lsHref = $(this).attr('href')
        var giPage = lsHref.replace(/#/g,"");
        $('.pagination li').removeClass('active');
        $(this).parent('li').addClass('active');
        var liDisplayStart = giPage * giDisplay + 1;
        var liDisplayEnd = giPage * giDisplay + giDisplay;
        $(this).scrollTop();
    
        $('.provider').hide();//hide all records
        fnShowResults(liDisplayStart,liDisplayEnd);//display the relevant records
        
    })
    $('#gi_provider_results .displayStart').text(giDisplayStart)
    $('#gi_provider_results .displayEnd').text(giDisplayEnd)
    $('#gi_provider_results .recordCount').text(giRecordCount)
    $('#gi_provider_results .providerCount').text(giProviderCount)