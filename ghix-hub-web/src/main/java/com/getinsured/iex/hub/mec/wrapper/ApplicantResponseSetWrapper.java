package com.getinsured.iex.hub.mec.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ApplicantResponseSetType;
import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ApplicantResponseType;
import com.getinsured.iex.hub.mec.wrapper.ApplicantResponseWrapper;

public class ApplicantResponseSetWrapper implements JSONAware{
	private List<ApplicantResponseWrapper> responseList;

	public ApplicantResponseSetWrapper(ApplicantResponseSetType responseSet) {
		List<ApplicantResponseType> rList = responseSet.getApplicantResponse();
		if(!rList.isEmpty()){
			this.responseList = new ArrayList<ApplicantResponseWrapper>();
		}
		ApplicantResponseWrapper tmp;
		for(ApplicantResponseType response:rList ){
			tmp = new ApplicantResponseWrapper(response);
			responseList.add(tmp);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		JSONArray list = new JSONArray();
		for(ApplicantResponseWrapper item: this.responseList){
			list.add(item);
		}
		obj.put("ResponseSet", list);
		return obj.toJSONString();
	}
}
