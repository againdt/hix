package com.getinsured.ssap.model.hub.vlp.documents;

import java.util.Date;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * Machine Readable Immigrant Visa (I-551) document.
 *
 * @author Yevgen Golubenko
 * @since 2/15/19
 */
public class MachineReadableImmigrantVisaDocument extends DhsDocument
{
  public MachineReadableImmigrantVisaDocument()
  {
    super.setDocumentType(DocumentType.MACHINE_READABLE_IMMIGRANT_VISA);
  }

  @JsonProperty("AlienNumber")
  private String alienNumber;

  @JsonProperty("VisaNumber")
  private String visaNumber;

  @JsonProperty("PassportNumber")
  private String passportNumber;

  @JsonProperty("CountryOfIssuance")
  private String countryOfIssuance;

  @JsonFormat(pattern = DATE_FORMAT)
  @JsonProperty("DocExpirationDate")
  private Date docExpirationDate;

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isValid()
  {
    return notBlank(this.alienNumber) &&
            notBlank(this.passportNumber) &&
              notBlank(this.countryOfIssuance);
  }

  /**
   * {@inheritDoc }
   */
  @Override
  public DhsDocument from(final CitizenshipDocument citizenshipDocument)
  {
    final MachineReadableImmigrantVisaDocument document = new MachineReadableImmigrantVisaDocument();

    document.setAlienNumber(validValue(citizenshipDocument.getAlienNumber()));
    document.setVisaNumber(validValue(citizenshipDocument.getVisaNumber()));
    document.setPassportNumber(validValue(citizenshipDocument.getForeignPassportOrDocumentNumber()));
    document.setCountryOfIssuance(validValue(citizenshipDocument.getForeignPassportCountryOfIssuance()));
    document.setDocExpirationDate(citizenshipDocument.getDocumentExpirationDate());

    return document;
  }

  public String getAlienNumber()
  {
    return alienNumber;
  }

  public void setAlienNumber(final String alienNumber)
  {
    this.alienNumber = alienNumber;
  }

  public String getVisaNumber()
  {
    return visaNumber;
  }

  public void setVisaNumber(final String visaNumber)
  {
    this.visaNumber = visaNumber;
  }

  public String getPassportNumber()
  {
    return passportNumber;
  }

  public void setPassportNumber(final String passportNumber)
  {
    this.passportNumber = passportNumber;
  }

  public String getCountryOfIssuance()
  {
    return countryOfIssuance;
  }

  public void setCountryOfIssuance(final String countryOfIssuance)
  {
    this.countryOfIssuance = countryOfIssuance;
  }

  public Date getDocExpirationDate()
  {
    return docExpirationDate;
  }

  public void setDocExpirationDate(final Date docExpirationDate)
  {
    this.docExpirationDate = docExpirationDate;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", MachineReadableImmigrantVisaDocument.class.getSimpleName() + "[", "]")
        .add("alienNumber='" + alienNumber + "'")
        .add("visaNumber='" + visaNumber + "'")
        .add("passportNumber='" + passportNumber + "'")
        .add("countryOfIssuance='" + countryOfIssuance + "'")
        .add("docExpirationDate=" + docExpirationDate)
        .toString();
  }
}
