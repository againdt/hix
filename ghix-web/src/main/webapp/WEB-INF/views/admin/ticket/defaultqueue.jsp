<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page isELIgnored="false"%>
<script type="text/javascript">
var csrfToken = {
        getParam: '${df:csrfTokenParameter()}=<df:csrfToken plainToken="true"/>',
        paramName: '${df:csrfTokenParameter()}',
        paramValue: '<df:csrfToken plainToken="true"/>'
    };
var defaultTicketWorkgroupServiceUrl='<c:url value="/ticketmgmt/ticket/savedefaultqueue" />';
var queueList=null;
queueList=${queueList};
</script>
<div class="gutter10" id="defaultTicketWorkgroupApp">
	<div class="row-fluid">
		<div><h1>Default Ticket Workgroups</h1></div>
	</div>
	<div class="row-fluid">
		<div class="header margin20-b">
			<h4>Workgroup List</h4>
		</div>
	</div>
	<div class="row-fluid gutter10" ng-controller="defaultTicketWorkgroupController">
		<form name="frmDefaultTicketWorkgroup" id="frmDefaultTicketWorkgroup">
			<div class="row-fluid">
				<table class="table table-hover" style="width:60%;margin:auto;">
					<tr>
						<th>Workgroup</th>
						<th class="text-center" style="width:100px;">Is Default</th>
					</tr>
					<tr ng-repeat="workgroup in queueList">
						<td>{{workgroup.name}}</td>
						<td class="text-center"><input type="checkbox" ng-false-value="N" ng-true-value="Y" ng-model="workgroup.isDefault" /></td>
					</tr>
				</table>
			</div>
			<div class="form-actions text-center">
				<div ng-if="result != false" class="gutter5" ng-class="{'text-success':result=='SUCCESS','text-error':result!='SUCCESS'}">
					<span ng-if="result!='SUCCESS'">Error saving data...</span>
					<span ng-if="result=='SUCCESS'">Data successfully saved...</span>
				</div>
				<button class="btn" id="btnReset" ng-click="onBtnReset()">Reset</button>
				<button class="btn btn-primary" id="btnSubmit" ng-click="onBtnSubmit()" data-loading-text="Saving...">Submit</button>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript" src='<c:url value="/resources/js/cap/defaultTicketWorkgroup.js" />'></script>
