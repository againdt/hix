(function() {
    'use strict';

    angular.module('autofocusDirectiveModule',[])
    .directive('autoTabTo', autoTabTo);
    function autoTabTo (){
        return {
            restrict: "A",
            link: function (scope, el, attrs) {
                el.bind('keyup', function(e) {
                    //if(e.keyCode !== 9 || e.which !== 9) {
                        if (this.value.length === parseInt(attrs.ngMaxlength)) {
                            var element = document.getElementById(attrs.autoTabTo);
                            if (element) {
                                element.focus();
                            }
                        }
                    //} 
                });
            }
        };
    }
})();
