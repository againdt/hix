package com.getinsured.eligibility.at.outbound.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.repository.outboundat.OutboundATApplicationRepository;
import com.getinsured.iex.ssap.model.OutboundATApplication;

@Service
public class OutboundATApplicationServiceImpl  implements OutboundATApplicationService {
	
	@Autowired private OutboundATApplicationRepository outboundATApplicationRepository;

	@Override
	public List<OutboundATApplication> findBySsapApplicationId(long ssapApplicationId) {
		return outboundATApplicationRepository.findBySsapApplicationId(ssapApplicationId);
	}
	
	@Override
	public boolean isOuboundATSent(long ssapApplicationId) {
		List<OutboundATApplication> OutboundATApplications = this.findBySsapApplicationId(ssapApplicationId);
		if(OutboundATApplications != null && OutboundATApplications.size() > 0){
			return true;
		}else{
			return false;
		}
	}
}
