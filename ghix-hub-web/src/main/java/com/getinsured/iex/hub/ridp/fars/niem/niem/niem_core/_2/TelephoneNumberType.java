
package com.getinsured.iex.hub.ridp.fars.niem.niem.niem_core._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ridp.fars.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for a telephone number for a telecommunication device.
 * 
 * <p>Java class for TelephoneNumberType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TelephoneNumberType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}FullTelephoneNumber"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TelephoneNumberType", propOrder = {
    "fullTelephoneNumber"
})
public class TelephoneNumberType
    extends ComplexObjectType
{

    @XmlElement(name = "FullTelephoneNumber", required = true, nillable = true)
    protected FullTelephoneNumberType fullTelephoneNumber;

    /**
     * Gets the value of the fullTelephoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link FullTelephoneNumberType }
     *     
     */
    public FullTelephoneNumberType getFullTelephoneNumber() {
        return fullTelephoneNumber;
    }

    /**
     * Sets the value of the fullTelephoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link FullTelephoneNumberType }
     *     
     */
    public void setFullTelephoneNumber(FullTelephoneNumberType value) {
        this.fullTelephoneNumber = value;
    }

}
