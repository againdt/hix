<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/employee-dashboard-util" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/shop-enrollment-data" prefix="enrollData"%>

<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />

<div class="row-fluid">
    <div class="header margin20-tb heightAuto">
      <h4>${enrolleeNames}</h4>
      <div>
      		<c:if test="${insuranceType=='HEALTH'}">
	      		<input type='checkbox' id='terminateHealth' name='terminatePlan' value='health' onclick="selectPlan('health');"/>
	      		<a class="btn btn-primary" href="#">Terminate Health Plan</a>
      		</c:if>
      		<c:if test="${insuranceType=='DENTAL'}">
	      		<input type='checkbox' id='terminateDental' name='terminatePlan' value='dental' onclick="selectPlan('dental');"/>
	      		<a class="btn btn-primary" href="#">Terminate Dental Plan</a>
      		</c:if>
      </div>
    </div>
</div>

<div class="row-fluid">
	<div class="span6 info-block">                    
	  <div class="gutter10">
	    <div class="plan-selected alert alert-success">
	        <div class="info-header txt-center">
	            <img src="$logoStr$" alt="$issuerName$" class="resize-img">
	            <p>${planName}</p>
	            <h5 class="payment">&#36;${monthlyPremium}</h5>
	        </div>
	        <div class="data-display well">
	         <dl class="dl-horizontal">
	            <dt>Plan Type:</dt>
	            <dd id="plan-type_$planId$">${planType}</dd>
	            <dt>Office Visit Copay:</dt>
	            <dd id="off-visit_$planId$">${officeVisit}</dd>
	            <dt>Generic Drugs Copay:</dt>
	            <dd id="generic-medic_$planId$">${genericMedications}</dd>
	            <dt>Deductible Ind:</dt>
	            <dd id="deductible_$planId$">${deductible}</dd>
	            <dt>Max Out-of-Pocket Ind:</dt>
	            <dd id="m-o-p_$planId$">${oopMax}</dd>
	          </dl>
	        </div>
	        <div class="center">
	        	<a href="$PlanUrl$" data-toggle="modal" id="viewBenifitDetails_$planId$" class="btn" alt="View Benifit Details">
	        		View Benefit Details
	        	</a>
	      </div>
	      </div>
	      <div class="gutter10-tb"></div>  
	  </div>
	</div> <!-- .span6 -->
	
	
    <div class="span6 info-block">
      <div class="gutter10">
       	  <div class="header"><h4>Policy Information</h4></div>              
          <div class="gutter10-lr">
            <dl class="dl-horizontal dl-lr-style">
                <dt>Policy Number:</dt>
                <dd id="policy-number_$planId$">${policyNumber}</dd>
                <dt>Coverage Period:</dt>
                <dd id="enrollment-date_$planId$">${coverageStartDate} - ${coverageEndDate}</dd>
              </dl>                   
          </div>
          <div class="header"><h4>Payment Information</h4></div>
		  <div class="gutter10-lr">
            <dl class="dl-horizontal dl-lr-style">
                <dt>Monthly Premium:</dt>
                <dd id="monthly-premium_$planId$">&#36; ${monthlyPremium}</dd>
                <dt>Employer Contribution:</dt>
                <dd id="emp-contribution_$planId$"><span>-</span>&#36; ${employerContribution}</dd>
                <dt class="noWhiteSpace"><strong>Monthly Payroll Deduction:</strong></dt>
                <dd id="monthly-payroll-deduct_$planId$"><strong>&#36; ${monthlyPayrollDeduction}</strong></dd>
              </dl>                   
          </div>
          <div class="header"><h4>Plan Contact</h4></div>              
          <div class="gutter10-lr">
            <dl class="dl-horizontal dl-lr-style">
                <dt>Customer Service:</dt>
                <dd id="cust-service_$planId$">${issuerPhone}</dd>
                <dt>Web:</dt>
                <dd id="web-address_$planId$" class="breakword">${issuerSite}</dd>
              </dl>                   
          </div>
        </div><!-- .gutter10-->
    </div> <!-- .span6 -->
    
 </div><!-- .row-fluid -->