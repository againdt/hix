<script type="text/ng-template" id="maritalstatus-other.jsp">
  <div class="gutter10 jobIncome" ng-init="householdMember = jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember[jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1]">
    <h3><spring:message code="label.lce.additionalquestions"/></h3>

    <form class="form-horizontal">
      <spring:message code="label.lce.was"/> <strong class="nameOfHouseHold">{{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}}</strong> <spring:message code="label.lce.uninsuredwithinthelast6months"/>
      <div class="control-group">
        <label class="control-label" for="typeOfRequest"></label>
        <div class="controls">
          <label class="radio inline">
  	        <input type="radio" name="other1" id="uninsuredyes" value="true" ng-model="householdMember.healthCoverage.haveBeenUninsuredInLast6MonthsIndicator" > <label for="uninsuredyes"><spring:message code="label.lce.yes"/></label>
          </label>
          <label class="radio inline">
  	        <input type="radio" value="false" id="uninsuredno" name="other1" ng-model="householdMember.healthCoverage.haveBeenUninsuredInLast6MonthsIndicator"> <label for="uninsuredno"><spring:message code="label.lce.no"/></label>
          </label>
        </div>
      </div>

      <spring:message code="label.lce.was"/> <strong class="nameOfHouseHold">{{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}}</strong> <spring:message code="label.lce.offeredhealthcoverage"/>
      <div class="control-group">
        <label class="control-label" for="typeOfRequest"></label>
        <div class="controls">
          <label class="radio inline">
            <input type="radio" name="other2" id="throughJobYes"  value="true"   ng-model="householdMember.healthCoverage.isOfferedHealthCoverageThroughJobIndicator"> <label for="throughJobYes"><spring:message code="label.lce.yes"/></label>
          </label>
          <label class="radio inline">
            <input type="radio" name="other2" id="throughJobNo" value="false" ng-model="householdMember.healthCoverage.isOfferedHealthCoverageThroughJobIndicator" > <label for="throughJobNo"><spring:message code="label.lce.no"/></label>
          </label>
        </div>
      </div>


      <spring:message code="label.lce.will"/> <strong class="nameOfHouseHold">{{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}}</strong> <spring:message code="label.lce.beoffered"/>
      <div class="control-group">
        <label class="control-label" for="typeOfRequest"></label>
        <div class="controls">
          <label class="radio inline">
            <input type="radio" name="other3" value="true" ng-model="householdMember.healthCoverage.employerWillOfferInsuranceIndicator"> <spring:message code="label.lce.yes"/>
          </label>
          <label class="radio inline">
            <input type="radio" name="other3"  value="false" ng-model="householdMember.healthCoverage.employerWillOfferInsuranceIndicator"> <spring:message code="label.lce.no"/>
          </label>
        </div>
      </div>

      <spring:message code="label.lce.is"/> <strong class="nameOfHouseHold">{{householdMember.name.firstName}} {{householdMember.name.lastName}}</strong> <spring:message code="label.lce.enrolledinhealthcoverage"/><br><br>
     <div class="control-group">
        <label class="control-label" for="typeOfRequest"></label>
        <div class="controls">
          <label class="radio inline">
            <input type="radio" id="currentlyEnrolledInCobraIndicator" name="healthCoverageType" ng-model="householdMember.healthCoverage.currentlyEnrolledInCobraIndicator"> <label for="currentlyEnrolledInCobraIndicator"><spring:message code="label.lce.cobra"/></label>
          </label>
        </div>
        <div class="controls">
          <label class="radio inline">
            <input type="radio" id="currentlyEnrolledInRetireePlanIndicator" name="healthCoverageType" ng-model="householdMember.healthCoverage.currentlyEnrolledInRetireePlanIndicator"><label for="currentlyEnrolledInRetireePlanIndicator"><spring:message code="label.lce.retireehealthplan"/></label>
          </label>
        </div>
        <div class="controls">
          <label class="radio inline">
            <input type="radio" id="currentlyEnrolledInVeteransProgramIndicator" name="healthCoverageType" ng-model="householdMember.healthCoverage.currentlyEnrolledInVeteransProgramIndicator"><label for="currentlyEnrolledInVeteransProgramIndicator"><spring:message code="label.lce.veteranshealthprogram"/></label>
          </label>
        </div>
        <div class="controls">
          <label class="radio inline">
            <input type="radio" id="noneOftheAbove" name="healthCoverageType" ><label for="noneOftheAbove"><spring:message code="label.lce.noneoftheabove"/></label>
          </label>
        </div>
        <div class="controls">
          <label class="radio inline">
            <input type="radio" id="other" name="healthCoverageType" ><label for="other"><spring:message code="label.lce.other"/></label>
          </label>
        </div>
      </div>

    </form>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a class="btn btn-primary" ng-click="nextHouseholdDependent();"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>