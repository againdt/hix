package com.getinsured.eligibility.ssap.sep.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;

public class SEPOverrideUtil {
	
	private static final Logger LOGGER = Logger.getLogger(SEPOverrideUtil.class);
	public static final String LONG_DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";

	private static int enrollmentGracePeriod;
	private static final int QEP_WINDOW = 60;
		
	
	@PostConstruct
	public void doPost() {
		
		enrollmentGracePeriod = 9;
		String enrollmentGracePeriodstr = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
		if (StringUtils.isNumeric(enrollmentGracePeriodstr)) {
			enrollmentGracePeriod = Integer.parseInt(enrollmentGracePeriodstr);
		}
	}

	/**
	 * Calculate Enrollment End date based on grace period and window period allowed for SEP/QEP applications
	 * @param currDate
	 * @return
	 */
	public static Timestamp calculateEndDate(Date currDate,boolean isNextYearsOE) {
		 
		if (isNextYearsOE)
		{
			try {
				return new Timestamp(new SimpleDateFormat(LONG_DATE_FORMAT).
						parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE) + " 23:59:59").getTime());
			} catch (ParseException e) {
				return null;
			}
		}
		else {
			DateTime endDate = new DateTime(currDate.getTime());
			endDate = endDate.plusDays(QEP_WINDOW + enrollmentGracePeriod);
			
			return new Timestamp(endDate.getMillis());
		}
	}
	
	/**
	 * Calculate Event precedence for SEP Events based on Event precedence order
	 * @param collection
	 * @return
	 */
	public static Integer checkEventPrecedence(Collection<SepEvents> collection) {
		SepEvents highestPrecedenceEvent = null;
		int prevPrecedenceOrder = 0;
		int nextPrecedenceOrder;
		
		for(SepEvents event: collection){
			if(event.getEventPrecedenceOrder()==null){
				nextPrecedenceOrder = 0;
			} else {
				nextPrecedenceOrder = event.getEventPrecedenceOrder();
			}
			
			if(prevPrecedenceOrder==0){
				prevPrecedenceOrder = nextPrecedenceOrder;
				highestPrecedenceEvent = event;
			} else if(nextPrecedenceOrder < prevPrecedenceOrder){
				prevPrecedenceOrder = nextPrecedenceOrder;
				highestPrecedenceEvent = event;
			}
		}
		
		return highestPrecedenceEvent.getId();
	}
}
