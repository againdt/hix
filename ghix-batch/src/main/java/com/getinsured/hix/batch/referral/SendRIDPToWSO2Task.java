package com.getinsured.hix.batch.referral;

import java.net.InetAddress;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.simple.JSONObject;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.util.GhixEndPoints;

public class SendRIDPToWSO2Task extends StepExecutionListenerSupport implements Tasklet {

	private static final Logger lOGGER = Logger.getLogger(SendRIDPToWSO2Task.class);
	private static final String dATE_RIDP_WSO2_SENT = "date.ridp.wso2.sent";

	//@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	private RestTemplate restTemplate;
	
	private String specificDate;
	
	  @Override
	public void beforeStep(StepExecution stepExecution) {
	      JobParameters jobParameters = stepExecution.getJobParameters();
	      specificDate = jobParameters.getString("execution_date");
	  }
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		lOGGER.debug("SendRIDPToWSO2Task started on " + new Timestamp(System.currentTimeMillis()));
		lOGGER.debug("processing records for specificDate:" + specificDate);
		DateTime startdt = null;
		if(StringUtils.isBlank(specificDate)){
			lOGGER.debug("no specfic date is chosen, running this task for default(Yesterday)");
			startdt = DateTime.now().minusDays(1).toDateMidnight().toDateTime();
		}else{		
			try {
				startdt = DateTime.parse(specificDate,  DateTimeFormat.forPattern("yyyy-MM-dd"));
			} catch (Exception e1) {
				startdt = DateTime.now().minusDays(1).toDateMidnight().toDateTime();
				lOGGER.debug("failed to parse given Date "+specificDate+" running this task for default(Yesterday)");
			}
		}
		lOGGER.debug(" startdt.toDate() " + startdt.toDate()+ " startdt.plusHours(24).toDate(), "+startdt.plusHours(24).toDate());
		try{
				List<Household> houseHolds =  cmrHouseholdRepository.findByRidpVerifiedAndRidpDateBetween("Y", startdt.toDate(), startdt.plusHours(24).toDate());
				lOGGER.debug(" picked "+ houseHolds.size() + " records for sending RIDP to WSO2");
				for(Household houseHold: houseHolds){
					if(houseHold.getUser() != null) {
						String response = sendRidpVerificationToWso2(houseHold.getUser());
						lOGGER.debug(response + " for "+ houseHold.getId());
					}else{
						lOGGER.debug("no AccountUser found for "+ houseHold.getId());	
					}
				}	
			}catch(Exception e){
				lOGGER.error(e);
			}
			lOGGER.debug("SendRIDPToWSO2Task finished on " + new Timestamp(System.currentTimeMillis()));
			return RepeatStatus.FINISHED;
		}

		private String sendRidpVerificationToWso2(AccountUser user){
			JSONObject request = new JSONObject();
			try {
				request.put("clientIp", InetAddress.getLocalHost().getHostAddress());
				JSONObject payload = new JSONObject();
				payload.put("userName", user.getEmail());
				payload.put("claimURI", "http://wso2.org/claims/ridpFlag");
				payload.put("claimValue", "YES");
				request.put("payload", payload);
				lOGGER.debug(" sending request : "+ request.toJSONString());
				lOGGER.debug(" to : "+ GhixEndPoints.IdentityServiceEndPoints.WSO2_UPDATE_USER_URL);
				String response = restTemplate.postForObject(GhixEndPoints.IdentityServiceEndPoints.WSO2_UPDATE_USER_URL, request.toJSONString(), String.class);
				if(StringUtils.containsIgnoreCase("SUCCESS", response)) {
					return "SUCCESS";
				} else {
					return "FAILED";
				}
			} catch (Exception exception) {
				lOGGER.error("Error setting RIDP flag in SSO(wso2) ", exception);
				return "FAILED";
			}
		}

		public RestTemplate getRestTemplate() {
			return restTemplate;
		}

		public void setRestTemplate(RestTemplate restTemplate) {
			this.restTemplate = restTemplate;
		}

		public CmrHouseholdRepository getCmrHouseholdRepository() {
			return cmrHouseholdRepository;
		}

		public void setCmrHouseholdRepository(
				CmrHouseholdRepository cmrHouseholdRepository) {
			this.cmrHouseholdRepository = cmrHouseholdRepository;
		}
	}
