/**
 * 
 */
package com.getinsured.hix.batch.enrollment.task;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.util.EnrollmentEmailUtils;
import com.getinsured.hix.batch.util.GhixBatchConstants;
import com.getinsured.hix.dto.enrollment.EnrollmentBatchEmailDTO;
import com.getinsured.hix.enrollment.service.EnrollmentBatchService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author parhi_s
 *
 */
public class IndividualEnrollmentReconciliationTask implements Tasklet {
	private static final Logger LOGGER = LoggerFactory.getLogger(IndividualEnrollmentReconciliationTask.class);
	private EnrollmentBatchService enrollmentBatchService;
	private EnrollmentEmailUtils enrollmentEmailUtils;
	private String extractMonth;
	private String hiosIssuerIdStr;
	private String hiosIssuerId;
	private String coverageYear;
	private JobService jobService;

	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws GIException {
		
		LOGGER.info("IndividualEnrollmentReconciliationTask.execute : START");
		
		EnrollmentBatchEmailDTO enrollmentBatchEmailDTO = new EnrollmentBatchEmailDTO();
		//enrollmentBatchEmailDTO.setFileLocation(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.XMLINDRECONEXTRACTPATH));
		enrollmentBatchEmailDTO.setFileLocation("");
		enrollmentBatchEmailDTO.setErrorCode(201);
		enrollmentBatchEmailDTO.setAppServer(GhixBatchConstants.appserver);
		try{
			if(StringUtils.isNotBlank(coverageYear) && !NumberUtils.isNumber(coverageYear)){
				throw new GIException("Invalid coverageYear parameter received as batch job parameter: "+coverageYear);
			}
			if(extractMonth!=null &&!(extractMonth.equalsIgnoreCase("null"))  &&!(extractMonth.trim().equals("")) && !(extractMonth.equalsIgnoreCase(EnrollmentConstants.IND_RECON_EXTRACT_MONTH_PRIOR)) && !(extractMonth.equalsIgnoreCase(EnrollmentConstants.IND_RECON_EXTRACT_MONTH_CURRENT))){
				LOGGER.error("Error in IndividualEnrollmentReconciliation Job =="+"Reconciliation_Month = "+extractMonth + " is not a valid value."+ "You can either pass as "+EnrollmentConstants.IND_RECON_EXTRACT_MONTH_PRIOR+" or " + EnrollmentConstants.IND_RECON_EXTRACT_MONTH_CURRENT+ " or null "+ " else you can leave it blank \n");
				enrollmentBatchEmailDTO.setErrorMessage("Reconciliation_Month = "+extractMonth + " is not a valid value."+ "You can either pass as "+EnrollmentConstants.IND_RECON_EXTRACT_MONTH_PRIOR+" or " + EnrollmentConstants.IND_RECON_EXTRACT_MONTH_CURRENT+ " or null "+" else you can leave it blank");
				throw new GIException("IndividualEnrollmentReconciliationTask.execute : Reconciliation_Month = "+extractMonth + " is not a valid value."+ "You can either pass as "+EnrollmentConstants.IND_RECON_EXTRACT_MONTH_PRIOR+" or " + EnrollmentConstants.IND_RECON_EXTRACT_MONTH_CURRENT+ " or null "+" else you can leave it blank \n");
			}
			
			// Apply Validation on HIOSIssuer Id 
			if(isNotNullAndEmpty(hiosIssuerIdStr)){
			 hiosIssuerId = hiosIssuerIdStr;
			 
			}
			
			//Long jobExecutionId = chunkContext.getStepContext().getStepExecution().getJobExecution().getJobId();
			Long jobExecutionId=chunkContext.getStepContext().getStepExecution().getJobExecutionId();
			String batchJobStatus=null;
			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
			
			
			enrollmentBatchService.serveEnrollmentReconciliationJob(extractMonth, EnrollmentConstants.ENROLLMENT_TYPE_INDIVIDUAL, hiosIssuerId, coverageYear);
			
			LOGGER.info("IndividualEnrollmentReconciliationTask.execute : IndividualEnrollmentReconciliationJob Ended Succesfully");
		}catch(Exception e){
			LOGGER.error("Error in IndividualEnrollmentReconciliationTask.execute() ::"+e.getMessage(),e);
			enrollmentBatchEmailDTO.setErrorMessage("Batch Failure Meessage : "+e.getMessage());
			enrollmentEmailUtils.enrollmentBatchFailureEmailNotification(enrollmentBatchEmailDTO, chunkContext);
			throw new GIException("IndividualEnrollmentReconciliationTask.execute : Error in IndividualEnrollmentReconciliationTask.execute() ::"+e.getMessage(),e);
		}
		LOGGER.info("IndividualEnrollmentReconciliationTask.execute : END");
		return RepeatStatus.FINISHED;
	}
	
	public EnrollmentBatchService getEnrollmentBatchService() {
		return enrollmentBatchService;
	}
	public void setEnrollmentBatchService(
			EnrollmentBatchService enrollmentBatchService) {
		this.enrollmentBatchService = enrollmentBatchService;
	}
	public String getExtractMonth() {
		return extractMonth;
	}
	public void setExtractMonth(String extractMonth) {
		this.extractMonth = extractMonth;
	}
	public EnrollmentEmailUtils getEnrollmentEmailUtils() {
		return enrollmentEmailUtils;
	}
	public void setEnrollmentEmailUtils(EnrollmentEmailUtils enrollmentEmailUtils) {
		this.enrollmentEmailUtils = enrollmentEmailUtils;
	}

	public String getHiosIssuerIdStr() {
		return hiosIssuerIdStr;
	}

	public void setHiosIssuerIdStr(String hiosIssuerIdStr) {
		this.hiosIssuerIdStr = hiosIssuerIdStr;
	}

	public String getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
}
