package com.getinsured.eligibility.lce.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.lce.service.LifeChangeEventApplicationService;
import com.getinsured.iex.lce.SepRequestParamDTO;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.LifeChangeEventUtil;
import com.getinsured.iex.util.SsapUtil;

@Service
public class LifeChangeEventApplicationServiceImpl implements
		LifeChangeEventApplicationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LifeChangeEventApplicationServiceImpl.class);
	private final String SSAPAPPLICATION_GUID_KEY = "SSAP_APPLICATION_DISPLAY_ID";
	
	@Autowired	
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private SsapUtil ssapUtil;
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	/**
	 * This method returns true if application status is SG, SU, ER or EN.
	 * Uses SSAP_APPLICATIONS.APPLICATION_STATUS
	 * @author sahay_b 
	 */
	@Override
	public Boolean isValidLCEApplication(SsapApplication ssapApplication) {
		if (ssapApplication != null && !isValidLCEApplication(ssapApplication.getApplicationStatus())) {
			return false;
		}
		return true;
	}

	/**
	 * Returns the Enrolled Application for LCE
	 */
	@Override
	public SsapApplication getEnrolledSsapApplicationByMaxIdAndCmrHouseoldId(
			BigDecimal cmrHouseoldId,long coverageYear) {
		return ssapApplicationRepository.findLatestEnPnSsapApplicationForCoverageYear(cmrHouseoldId,coverageYear);
	}
	

	@Override
	public List<SsapApplication> getRenewedSsapApplicationsByCoverageYear(Long renewalYear,Long batchSize) {
		return ssapApplicationRepository.getRenewedSsapApplicationsByCoverageYear(renewalYear, new PageRequest(0,batchSize.intValue()));
	}

	@Override
	public SsapApplication findByMaxIdCoverageYearAndCmrHouseoldId(
			BigDecimal cmrHouseoldId, long coverageYear, String isFinancial) {
		return ssapApplicationRepository.findByMaxIdCoverageYearAndCmrHouseoldId(cmrHouseoldId, coverageYear, isFinancial);
	}

	@Override
	public void closeSsapApplication(SsapApplication ssapApplication) {
		ssapApplication.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());		
		ssapApplicationRepository.saveAndFlush(ssapApplication);
	}
	
	@Override
	public SsapApplication saveSsapSepApplication(
			SsapApplication currentApplication, boolean updateJson) throws Exception {
		String applicationData=null;
		currentApplication = ssapApplicationRepository.saveAndFlush(currentApplication);
		if(updateJson) {
			applicationData = LifeChangeEventUtil.setApplicationIdAndGuid(currentApplication.getId(), currentApplication.getApplicationData(), currentApplication.getCaseNumber());
			applicationData=ssapJsonBuilder.transformToJson(ssapJsonBuilder.transformFromJson(applicationData));
			currentApplication.setApplicationData(applicationData);
			currentApplication = ssapApplicationRepository.saveAndFlush(currentApplication);
		}
		return currentApplication;
	}
	
	@Override
	public SsapApplication cloneSsapApplicationFromParent(String ssapJson, SsapApplication currentApplication, 
			SsapApplication parentApplication, SepRequestParamDTO sepRequestParamDTO, String applicationType, 
			String applicationSource, String applicationStatus, Long renewalYear) {
		if(currentApplication == null) {
			currentApplication = new SsapApplication();
		}
		
		currentApplication.setApplicationData(ssapJson);
		currentApplication.setParentSsapApplicationId(new BigDecimal(parentApplication.getId()));
		setSSAPApplication(currentApplication, parentApplication, sepRequestParamDTO, applicationType, applicationSource, applicationStatus, renewalYear);
		return currentApplication; 
	}
	
	private SsapApplication setSSAPApplication(SsapApplication ssapApplication, SsapApplication parentApplication, SepRequestParamDTO sepRequestParamDTO, String applicationType, 
			String applicationSource, String applicationStatus, Long renewalYear) {
		Date date = new TSDate();
		String esignFirstName;
		String esignMiddleName;
		String esignLastName;
		
		esignFirstName = sepRequestParamDTO.getEsignFirstName();
		esignMiddleName = sepRequestParamDTO.getEsignMiddleName();
		esignLastName = sepRequestParamDTO.getEsignLastName();
		ssapApplication.setEsignDate(new Timestamp(new TSDate().getTime()));
		ssapApplication.setEsignFirstName(esignFirstName);
		ssapApplication.setEsignMiddleName(esignMiddleName);
		ssapApplication.setEsignLastName(esignLastName);
		ssapApplication.setApplicationType(applicationType);
		ssapApplication.setCaseNumber(ssapUtil.getNextSequenceFromDB(SSAPAPPLICATION_GUID_KEY));			
		ssapApplication.setLastUpdatedBy(null);
		ssapApplication.setLastUpdateTimestamp(null); // Abhai : Why is it null?
		ssapApplication.setSource(applicationSource);
		ssapApplication.setEligibilityStatus(EligibilityStatus.PE);
		ssapApplication.setExchangeEligibilityStatus(ExchangeEligibilityStatus.NONE);
		ssapApplication.setParentSsapApplicationId(new BigDecimal(parentApplication.getId()));
		ssapApplication.setApplicationStatus(applicationStatus);
		ssapApplication.setAllowEnrollment(LifeChangeEventConstant.NO);
		ssapApplication.setCmrHouseoldId(parentApplication.getCmrHouseoldId());			
		ssapApplication.setCoverageYear(renewalYear);
		ssapApplication.setCurrentPageId("appscr67");
		ssapApplication.setSepQepDenied(null);
		if(sepRequestParamDTO.getUserId() != null) {
			ssapApplication.setCreatedBy(new BigDecimal(sepRequestParamDTO.getUserId()));
		}
		ssapApplication.setCreationTimestamp(new Timestamp(date.getTime()));
		ssapApplication.setStartDate(parentApplication.getStartDate());
		ssapApplication.setFinancialAssistanceFlag(parentApplication.getFinancialAssistanceFlag());

		return ssapApplication;
	}
	
	/**
	 * Checks for valid LCE Application
	 * @param applicationStatus
	 * @return
	 */
	private boolean isValidLCEApplication(String applicationStatus) {
		return applicationStatus.equals(ApplicationStatus.SIGNED
				.getApplicationStatusCode())
				|| applicationStatus.equals(ApplicationStatus.SUBMITTED
						.getApplicationStatusCode())
				|| applicationStatus
						.equals(ApplicationStatus.ELIGIBILITY_RECEIVED
								.getApplicationStatusCode()) 
				|| applicationStatus
						.equals(ApplicationStatus.ENROLLED_OR_ACTIVE
								.getApplicationStatusCode()) 
				|| applicationStatus
						.equals(ApplicationStatus.PARTIALLY_ENROLLED
								.getApplicationStatusCode());
	}

	@Override
	public List<Long> getEnrolledSsapApplicationsByCoverageYear(
			long coverageYear) {
		return ssapApplicationRepository.getEnrolledSsapApplicationsByCoverageYear(coverageYear);
	}

	public SsapApplicationRepository getSsapApplicationRepository() {
		return ssapApplicationRepository;
	}

	public void setSsapApplicationRepository(
			SsapApplicationRepository ssapApplicationRepository) {
		this.ssapApplicationRepository = ssapApplicationRepository;
	}

	public SsapUtil getSsapUtil() {
		return ssapUtil;
	}

	public void setSsapUtil(SsapUtil ssapUtil) {
		this.ssapUtil = ssapUtil;
	}

	public SsapJsonBuilder getSsapJsonBuilder() {
		return ssapJsonBuilder;
	}

	public void setSsapJsonBuilder(SsapJsonBuilder ssapJsonBuilder) {
		this.ssapJsonBuilder = ssapJsonBuilder;
	}

}
