package com.getinsured.hix.serff.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferAttachmentInfoDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferInfoDTO;
import com.getinsured.hix.model.IssuerPaymentInformation;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;

/**
 * Interface is used to provide services of SERFF.
 * @author Bhavin Parmar
 */
public interface SerffService {

	SerffPlanMgmt getSerffPlanMgmtById(long id);
	SerffPlanMgmt saveSerffPlanMgmt(final SerffPlanMgmt serffPlanMgmt);
	
	SerffDocument getSerffDocumentById(long id);
	SerffDocument saveSerffDocument(final SerffDocument serffDocument);
	
	SerffPlanMgmtBatch getSerffPlanMgmtBatchById(long id);
	SerffPlanMgmtBatch saveSerffPlanMgmtBatch(final SerffPlanMgmtBatch serffPlanMgmtBatch );
	String processSerffPlanMgmtBatch();
	List<IssuerDropDownDTO> getIssuerNameList();
	List<Tenant> getTenantNameList();
	
	List<SerffDocumentPlanPOJO> getSerffDocumentByIssuerId(String issuerId, String serffOperation);
	
    IssuerPaymentInformation getIssuerPaymentInfo(Integer issuerId);
	IssuerPaymentInformation saveIssuerPaymentInfo(final IssuerPaymentInformation issuerPayment);

	String uploadTemplateAttachments(SerffPlanTemplatesDTO templatePOJO);

	Page<PlanLoadStatusDTO> getPlanLoadStatusDataList(int page, int size, String selectedIssuer, String selectedType, String selectedStatus, String selectedDate);

	Page<SerffTransferInfoDTO> getSERFFTransferInfoDataList(int page, String selectedIssuer, String selectedStatus, String selectedDate);

	List<SerffTransferAttachmentInfoDTO> getSERFFPlanAttachmentsList(long selectedRecordID);

	boolean checkWaitingPlansToLoad();

	String executeBatch();
}
