<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  Lucid Chart - GHIX_Assister_Registration Registration_1 - Cheryl.  -->

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<!--src="<c:url value="/resources/js/employer_location.js" />"  -->

	
	<div class="row-fluid">
	<div class="span12">
	
		<div class="span3">
			<p><strong>  </strong> </p>
			<br></br>
		</div>
		
		<div class="span9">
		<h2>Registration</h2>
		<form class="form-stacked" id="frmbrokerreg" name="frmbrokerreg" action="brokerSignup" method="POST">
			<fieldset>

<h2>Register with State Health Benefit Exchange</h2>
Please fill out this form to start the registration process with the State Health Benefit Exchange.
On submission of the form, an email will be sent to you to activate your account.
<hr>

<label for="firstName" class="required">Your Email</label>
<div class="input">
<input type="text" name="firstName" id="firstName" value="" class="xlarge" size="30">
</div>	
<div id="firstName_error"></div>
						
<label for="firstName" class="required">Your First Name</label>
<div class="input">
<input type="text" name="firstName" id="firstName" value="" class="xlarge" size="30">
</div>	
<div id="firstName_error"></div>
<label for="firstName" class="required">Your Last Name</label>
<div class="input">
<input type="text" name="firstName" id="firstName" value="" class="xlarge" size="30">
</div>	
<div id="firstName_error"></div>

<label for="firstName" class="required">Username</label>
<div class="input">
<input type="text" name="firstName" id="firstName" value="" class="xlarge" size="30">
</div>	
<div id="firstName_error"></div>																		
						
<div class="actions">
<input type="submit" name="submit" id="submit" value="Submit" class="btn primary" title="Submit"/> 
</div>
</div>
</fieldset>
</form>
</div>
</div>

	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">
				<p>This information is required to determine initial eligibility to
					use the SHOP Exchange. All fields are subjected to a basic format
					validation of the input (e.g. a zip code must be a 5 digit number).
					The question mark icon which opens a light-box provides helpful
					information. The EIN number can be validated with an external data
					source if an adequate web API is available.</p>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
jQuery.validator.addMethod("userNameCheck", function(value, element, param) {
	return $("#userNameCheck").val() == 0 ? false : true;

});

function chkUserName(){
	$.get('brokerSignup/chkUserName',
            {userName: $("#userName").val()},                
            function(response){                    
            	if(response == "EXIST"){
            		$("#userNameCheck").val(0);
                }else{
                	$("#userNameCheck").val(1);
                }
            }                
        );
}

var validator = $("#frmbrokerreg").validate({ 
	rules : { firstName : { required : true},
		lastName : { required : true},
	   email : { required : true},
		userName : { required: true,
					userNameCheck : true}
	
	    
	    	
	},
	messages : {
		
		firstName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstName' javaScriptEscape='true'/></span>"},
		lastName : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateLastName' javaScriptEscape='true'/></span>"},
	
		email :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateEmail' javaScriptEscape='true'/></span>"},
		userName :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateUserName' javaScriptEscape='true'/></span>",
					userNameCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateUserNameCheck' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	} 
});


</script>
