package com.getinsured.hix.indportal;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeUpdateDto;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentStatusUpdateDto;
import com.getinsured.hix.indportal.dto.CoveredFamilyMember;
import com.getinsured.hix.indportal.dto.EnrollmentStatusUpdate;
import com.getinsured.hix.indportal.dto.OverrideComment;
import com.getinsured.hix.indportal.dto.ReverseSepQepDenial;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.enrollment.enrollmentreinstatement.EnrollmentReinstatementRequest;
import com.getinsured.iex.dto.Ind57Mapping;
import com.getinsured.iex.indportal.dto.IndDisenroll;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

/**
 * @author Krishna Gajulapalli, Sunil Desu
 *
 */
@Component
public class CSROverrideUtility extends IndividualPortalCommons{
	
	private static final String CANCELLED = "CANCEL";

	private static final String SUCCESS = "success";

	private static final Logger LOGGER = LoggerFactory.getLogger(CSROverrideUtility.class);
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired
	private UserService userService;
	
	@Autowired 
	private CommentTargetService commentTargetService;
	
	private static final int HTTP_OK = 200;
	
	private static final String FAILURE = "failure";
	
	public String invokeSSAPIntegration(long applicationId) {
		String responseMessage=FAILURE;
		 LOGGER.debug("Invoking the SSAP orchestration flow");
		 try {
			 AccountUser user = userService.getLoggedInUser();
			 ghixRestTemplate.exchange(GhixEndPoints.SsapIntegrationEndpoints.SSAP_INTEGRATION_URL, user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_XML, String.class, String.valueOf(applicationId));
			 //restTemplate.postForObject(GhixEndPoints.SsapIntegrationEndpoints.SSAP_INTEGRATION_URL,applicationId, String.class);
			 responseMessage = SUCCESS;
		 }catch(Exception e) {
			 LOGGER.error("Exception in invokeSSAPIntegration", e);
			 responseMessage = FAILURE;
		 }	     
	     return responseMessage;
	 }			
	
	public String invokeIndDisenrollment(IndDisenroll indDisenroll) {
		LOGGER.debug("Invoking  ind disenrollement utility");
		String responseMsg=FAILURE;				
		try {
			AccountUser user = userService.getLoggedInUser();
			ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"indportal/enrollment/disenrollment", user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, indDisenroll);
			if (response.getBody() != null) {
				responseMsg = response.getBody();
			}
			//responseMsg = restTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL+"indportal/enrollment/disenrollment",indDisenroll, String.class);
		}catch(Exception e) {
			LOGGER.error("Exception in invokeIndDisenrollment", e);
			responseMsg = FAILURE;
			throw new GIRuntimeException("Exception occurred while invoking disenrollment",e);
		}	
		return responseMsg;
	}
	
	public String invokeAdminUpdateEnrollment(Ind57Mapping ind57Mapping) {	
		String responseMsg = FAILURE;			
		LOGGER.debug("Invoking AdminUpate Enrollment");		
		try {
			AccountUser user = userService.getLoggedInUser();
			ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"indportal/enrollment/adminupdateenrollment", user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, ind57Mapping);
			//responseMsg =restTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL+"indportal/enrollment/adminupdateenrollment",ind57Mapping, String.class);
			if (response.getBody() != null) {
				responseMsg = response.getBody();
			} 		
		}catch(Exception e) {
			LOGGER.error("Exception in invokeAdminUpdateEnrollment", e);
			responseMsg = FAILURE;
			throw new GIRuntimeException("Exception occcurred while invoking admin update enrollment",e);			
		}			
	    return responseMsg;
	 }
	
	public String invokeDentalDisenrollment(IndDisenroll indDisenroll) {
		LOGGER.debug("Invoking  ind disenrollement utility");
		String responseMsg=FAILURE;	
		EnrollmentRequest enrollRequest = new EnrollmentRequest();
		EnrollmentResponse enrollmentResponse = null;
		try {
			if(indDisenroll != null){
				XStream xStream = GhixUtils.getXStreamStaxObject();
				List<Integer> idList = new ArrayList<Integer>();
				Map<String, Object> disEnroll = new HashMap<String, Object>();
				idList.add(Integer.parseInt(indDisenroll.getDentalEnrollmentId()));
				disEnroll.put("TerminationDate", indDisenroll.getTerminationDate());
				disEnroll.put("TerminationReasonCode", indDisenroll.getReasonCode());
				if(indDisenroll.getReasonCode().equalsIgnoreCase("3")){
					disEnroll.put("TerminationReasonCode", indDisenroll.getDeathDate());
				}
				enrollRequest.setIdList(idList);
				enrollRequest.setMapValue(disEnroll);
				responseMsg =  (ghixRestTemplate.exchange(GhixEndPoints.ENROLLMENT_URL+"/enrollment/disenrollbyenrollmentid", indDisenroll.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,xStream.toXML(enrollRequest))).getBody();
				enrollmentResponse = (EnrollmentResponse) xStream.fromXML(responseMsg);
				if(null != responseMsg && 
						enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS) && 
						enrollmentResponse.getErrCode() == HTTP_OK){
					responseMsg=SUCCESS;	
				}
			}

		} catch(Exception e) {
			LOGGER.error("Exception in invokedentalDisenrollment", e);
			responseMsg = FAILURE;
			throw new GIRuntimeException("Exception occurred while invokedentalDisenrollment",e);
		}	
		return responseMsg;
	}
	
	 /**
     * HIX-69276 Provide ability to disenroll from QHP only
     * 
     *  @author Nikhil Talreja
     *  
     */
	public String invokeHealthDisenrollment(IndDisenroll indDisenroll) {
		LOGGER.debug("Invoking  ind disenrollement utility for health plan");
		String responseMsg=FAILURE;	
		EnrollmentRequest enrollRequest = new EnrollmentRequest();
		EnrollmentResponse enrollmentResponse = null;
		try {
			if(indDisenroll != null){
				XStream xStream = GhixUtils.getXStreamStaxObject();
				List<Integer> idList = new ArrayList<Integer>();
				Map<String, Object> disEnroll = new HashMap<String, Object>();
				idList.add(Integer.parseInt(indDisenroll.getHealthEnrollmentId()));
				disEnroll.put("TerminationDate", indDisenroll.getTerminationDate());
				disEnroll.put("TerminationReasonCode", indDisenroll.getReasonCode());
				
				enrollRequest.setIdList(idList);
				enrollRequest.setMapValue(disEnroll);
				responseMsg =  (ghixRestTemplate.exchange(GhixEndPoints.ENROLLMENT_URL+"/enrollment/disenrollbyenrollmentid", indDisenroll.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,xStream.toXML(enrollRequest))).getBody();
				enrollmentResponse = (EnrollmentResponse) xStream.fromXML(responseMsg);
				if(null != responseMsg && 
						enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS) && 
						enrollmentResponse.getErrCode() == HTTP_OK){
					responseMsg=SUCCESS;	
				}
			}

		} catch(Exception e) {
			LOGGER.error("Exception in invokeHealthDisenrollment", e);
			responseMsg = FAILURE;
			throw new GIRuntimeException("Exception occurred while invokeHealthDisenrollment",e);
		}	
		return responseMsg;
	}
	
	
	public String reinstateEnrollment(long ssapOrEnrollmentId, boolean reinstateBothDentalAndHealth, String userName) {
		String responseValue = FAILURE;
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentReinstatementRequest reinstatementRequest = new EnrollmentReinstatementRequest();
		EnrollmentResponse enrollmentResponse = null;
		
		try {		
				if(reinstateBothDentalAndHealth) {
					reinstatementRequest.setApplicationId(ssapOrEnrollmentId);
				} else {
					reinstatementRequest.setEnrollmentId(ssapOrEnrollmentId);
				}
				
				enrollmentRequest.setEnrollmentReinstatementRequest(reinstatementRequest);
				try {
					XStream xStream = GhixUtils.getXStreamStaxObject();	
					ResponseEntity<String> response = ghixRestTemplate.exchange(
									GhixEndPoints.EnrollmentEndPoints.ENROLLMENT_REINSTATEMENT_URL,
									userName, HttpMethod.POST,
									MediaType.APPLICATION_JSON, String.class,
									xStream.toXML(enrollmentRequest));
					if(null != response && null != response.getBody()) {
						enrollmentResponse = (EnrollmentResponse) xStream.fromXML(response.getBody());
						if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS) && enrollmentResponse.getErrCode() == 200) {
							responseValue = SUCCESS;		
						} else if(enrollmentResponse.getErrCode() == 305) {
							responseValue = "CANNOT_REINSTATED" ;
							LOGGER.error("reinstateEnrollment call to enrollment was not successful ");
						} else if(enrollmentResponse.getErrCode() == 204) {
							responseValue = "SUBSEQUENT_APP_SUBMITTED" ;
							LOGGER.error("enrollment cannot be reinstated because a subsequent application has been submitted ");
						} else {
							LOGGER.error("reinstateEnrollment call to enrollment was not successful ");
						}
					}
				} catch(Exception e) {
					LOGGER.error("Exception in reinstateEnrollment call to enrollment ", e);
				}
		 } catch(Exception e) {		
			 LOGGER.error("Exception in reinstateEnrollment ", e);
		 }  
	    return responseValue;
	}

	/*private void logError(long ssapOrEnrollmentId, boolean reinstateBothDentalAndHealth, ResponseEntity<String> response) {
		StringBuilder errorString = new StringBuilder(); 
		errorString.append("Error response from reinstateEnrollment for ssapOrEnrollmentId: " + ssapOrEnrollmentId + " reinstateBothDentalAndHealth?: " + reinstateBothDentalAndHealth);
		errorString.append("Response received: " + response.getBody());
		LOGGER.error(errorString.toString());
	}*/
	
	public List<OverrideComment> transformComments(List<Comment> comments){
		List<OverrideComment> overrideComments = new ArrayList<OverrideComment>(0);
		if(comments!=null && !comments.isEmpty()){
			for(Comment comment:comments){
				OverrideComment overrideComment = new OverrideComment();
				overrideComment.setCommentedDate(new SimpleDateFormat("MMMMMMMMMMM dd yyyy, HH:mm:ss a").format(comment.getUpdated()));
				overrideComment.setCommenterName(comment.getCommenterName());
				overrideComment.setOverrideComment(comment.getComment());
				overrideComments.add(overrideComment);
			}
		}
		return overrideComments;
	}

    public String reRunEligibility(String caseNumber) {
        LOGGER.debug("Invoking Account Transfer Service");
        String responseMsg=FAILURE;
        try {
        	AccountUser user = userService.getLoggedInUser();
            //restTemplate.postForObject(GhixEndPoints.EligibilityEndPoints.RERUN_ELIGIBILITY_URL, caseNumber, String.class);
        	//restTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL+"nonfinancial/eligibility/rerun", caseNumber, String.class);
        	ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"nonfinancial/eligibility/rerun", user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_XML, String.class, caseNumber);
        	responseMsg = SUCCESS;
         }catch(Exception e) {
             LOGGER.error("Exception in reRunEligibilityService", e);
             responseMsg = FAILURE;
         }     
        return responseMsg;
    }
		
	public String invokeDisenrollmentForSep(IndDisenroll indDisenroll) {
		LOGGER.debug("Invoking  ind disenrollement utility for sep disenrollment");
		String responseMsg=FAILURE;				
		try {
			AccountUser user = userService.getLoggedInUser();
			//responseMsg = restTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL+"indportal/enrollment/disenrollmentforsep",indDisenroll, String.class);
        	ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"indportal/enrollment/disenrollmentforsep", user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, indDisenroll);
   			if (response.getBody() != null) {
   				responseMsg = response.getBody();
   			} 
		}catch(Exception e) {
			LOGGER.error("Invoking  ind disenrollement utility for sep disenrollment", e);
			responseMsg = FAILURE;
			throw new GIRuntimeException("Exception occurred while invoking disenrollment for sep",e);
		}	
		return responseMsg;
	}
	
	public Map<String, String> getEnrollmentsForApplication(long applicationId, String userName) {
		Map<String, String> enrollTypeToEnrollmentIdMap = new HashMap<>();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrolleeResponse enrolleeResponse;
		EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
		enrolleeRequest.setSsapApplicationId(applicationId);
		
		String xmlResponse = (ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID, 
				userName, 
				HttpMethod.POST, 
				MediaType.APPLICATION_JSON, 
				String.class, xstream.toXML(enrolleeRequest))).getBody();
		
		enrolleeResponse = (EnrolleeResponse) xstream.fromXML(xmlResponse);
		
		if(enrolleeResponse.getEnrollmentShopDTOList() != null) {
			Map<String, EnrollmentShopDTO> enrollTypeToEnrollmentMap = new HashMap<>();
		
			for (EnrollmentShopDTO enrollment: enrolleeResponse.getEnrollmentShopDTOList()) {
				// Only return the latest terminated enrollments (enrollment status value as 'Cancel') 
				EnrollmentShopDTO existingEnrollment = enrollTypeToEnrollmentMap.get(enrollment.getPlanType());
				if(CANCELLED.equals(enrollment.getEnrollmentStatusValue()) && 
						(existingEnrollment == null || existingEnrollment.getEnrollmentCreationTimestamp().compareTo(enrollment.getEnrollmentCreationTimestamp()) < 0)) {
					enrollTypeToEnrollmentMap.put(enrollment.getPlanType(), enrollment);
				}
			}
			
			for (Map.Entry<String, EnrollmentShopDTO> planType : enrollTypeToEnrollmentMap.entrySet()) {
				enrollTypeToEnrollmentIdMap.put(planType.getKey(), 
						ghixJasyptEncrytorUtil.encryptStringByJasypt(enrollTypeToEnrollmentMap.get(planType.getKey()).getEnrollmentId().toString()));
			}
		}
		return enrollTypeToEnrollmentIdMap;
	}

	public void sendSepQepGrantedNotice(ReverseSepQepDenial reverseSepQepDenial) {
		LOGGER.debug("Invoking SepQepDenied utility");
		try {
			AccountUser user = userService.getLoggedInUser();
			reverseSepQepDenial.setCmrHouseholdId(Long.valueOf(user.getActiveModuleId()));
			//TODO check to handle failure
        	ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"eligibility/indportal/reverseSepQepDenial/sendnotice", user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, reverseSepQepDenial);
		}catch(Exception e) {
			LOGGER.error("Invoking SepQepDenied for sending notices", e);
			persistToGiMonitor(e);
		}	
	}

	/**
	 * HIX-79572 : API used for to invoke the 
	 * Update Enrollment Status for the 1095, 
	 * when the enrollment status is pending or terminated
	 * @author Sunil Sahoo
	 * @return SUCCESS or FAILURE
	 * 
	*/
	public String invokeEnrollmentStatusUpdate(EnrollmentStatusUpdate enrollmentStatusUpdate) {
		if (StringUtils.isNotBlank(enrollmentStatusUpdate.getEnrollmentId())) {
			String enrollmentId = ghixJasyptEncrytorUtil.decryptStringByJasypt(enrollmentStatusUpdate.getEnrollmentId());
			boolean overrideResponse = saveOverrideComments(enrollmentStatusUpdate.getOverrideCommentText().trim(),enrollmentId);
		if (overrideResponse) {
				return invokeEnrollmentStatusOverride(enrollmentStatusUpdate, Integer.valueOf(enrollmentId));
			}
		}
		return FAILURE;
	}
	
	/**
	 * HIX-79572 : API used to prepare enrollment request and  
	 * call the enrollment API to update the enrollment status 
	 * @author Sunil Sahoo
	 * @param enrollmentId 
	 * @param overrideCommentText, caseNumber
	 * @return SUCCESS or FAILURE
	 * 
	*/
	private String invokeEnrollmentStatusOverride(EnrollmentStatusUpdate enrollmentStatusUpdate, int enrollmentId) {
		LOGGER.debug("Invoking Update Enrollment Status Request");
		String responseMsg=FAILURE;
		try {
			
			List<EnrolleeUpdateDto> enrolleeUpdateDtoList = new ArrayList<EnrolleeUpdateDto>();
			EnrollmentStatusUpdateDto enrollmentStatusUpdateDto = new EnrollmentStatusUpdateDto();
			
			enrollmentStatusUpdateDto.setEnrollmentId(enrollmentId);
			
			if (StringUtils.isNotBlank(enrollmentStatusUpdate.getIssuerAssignedPolicyId())) {
				enrollmentStatusUpdateDto.setIssuerAssignPolicyNo(enrollmentStatusUpdate.getIssuerAssignedPolicyId().trim());
			}
			
			List <CoveredFamilyMember> familyMembers = enrollmentStatusUpdate.getFamilyMembers();
			for (CoveredFamilyMember coveredFamilyMember : familyMembers) {
				EnrolleeUpdateDto enrolleeUpdateDto = new EnrolleeUpdateDto();
				enrolleeUpdateDto.setExchgIndivIdentifier(coveredFamilyMember.getExchgIndivIdentifier());
				if (StringUtils.isNotBlank(coveredFamilyMember.getIssuerAssignedMemberId())) {
				enrolleeUpdateDto.setIssuerIndivIdentifier(coveredFamilyMember.getIssuerAssignedMemberId().trim());
				}
				enrolleeUpdateDto.setLastPremiumPaidDate(enrollmentStatusUpdate.getLastPremiumPaidDate());
				enrolleeUpdateDto.setEffectiveStartDate(coveredFamilyMember.getStartDate());
				enrolleeUpdateDtoList.add(enrolleeUpdateDto);
			}
			enrollmentStatusUpdateDto.setEnrolleeUpdateDtoList(enrolleeUpdateDtoList);
			
			AccountUser user = userService.getLoggedInUser();
			ResponseEntity<String> enrollmentJsonResponse = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.UPDATE_ENROLLMENT_STATUS, user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, enrollmentStatusUpdateDto);
			
			if(null != enrollmentJsonResponse && null != enrollmentJsonResponse.getBody()) {
				Gson gson = new Gson();
				EnrollmentResponse enrollmentResponse = gson.fromJson(enrollmentJsonResponse.getBody(), EnrollmentResponse.class);
				if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS) && enrollmentResponse.getErrCode() == 200) {
					responseMsg = SUCCESS;		
				} 
			}
		}catch(Exception e) {
			LOGGER.error("Invoking Update Enrollment Status Request", e);
			persistToGiMonitor(e);
			responseMsg = FAILURE;
		}
		return responseMsg;	
	}
	
	/**
	 * HIX-79572 : API used to save the 
	 * override comments while updating enrollment status 
	 * @author Sunil Sahoo
	 * @param overrideCommentText, caseNumber
	 * @return SUCCESS or FAILURE
	 * 
	*/
	private boolean saveOverrideComments(String overrideCommentText, String targetId) {
		
		LOGGER.info("Retrieving logged in user's name");
		AccountUser accountUser = null;
		try{
			accountUser = userService.getLoggedInUser();
			if(accountUser==null){
				throw new InvalidUserException();
			}
		
			CommentTarget commentTarget = commentTargetService.findByTargetIdAndTargetType(new Long(targetId), TargetName.ENROLLMENT_OVERRIDE);
			
			if(commentTarget == null){
			commentTarget =  new CommentTarget();
				commentTarget.setTargetId(new Long(targetId));
				commentTarget.setTargetName(TargetName.ENROLLMENT_OVERRIDE);
			}
			
			List<Comment> comments = null;
			
			if(commentTarget.getComments() == null || commentTarget.getComments().size() == 0 ){
			comments = new ArrayList<Comment>();
			}
			else{
				comments = commentTarget.getComments();
			}
			
			Comment comment = new Comment();
			String commenterName = "";
			if(accountUser != null){
				commenterName = accountUser.getFirstName()+" "+accountUser.getLastName();
			}
			
			LOGGER.info("Commented By : " + accountUser.getId());
			comment.setComentedBy(accountUser.getId());
			LOGGER.info("Commenter Name : " + commenterName);
			comment.setCommenterName(commenterName);
			comment.setComment(overrideCommentText);
			comment.setCommentTarget(commentTarget);
			
			comments.add(comment);
			commentTarget.setComments(comments);
			commentTargetService.saveCommentTarget(commentTarget);
			
		}catch(InvalidUserException e){
			LOGGER.info("Exception has occured",e);
			persistToGiMonitor(e);
			return false;
		}catch (Exception e) {
			LOGGER.info("Exception has occured",e);
			persistToGiMonitor(e);
			return false;
		}
		return true;
	}

}
