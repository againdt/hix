<html>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ page import="com.getinsured.hix.model.VimoEncryptor"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css" />


<head>
<title>GHIX Prescreen Show Data</title>

<style>
table#table1 {
	border: 1px solid #666;
	width: 80%;
	margin: 20px 0 20px 0 !important;
	font-size: 14px;
}

th,td {
	padding: 2px 4px 2px 4px !important;
	text-align: left;
	vertical-align: top;	
}

thead tr {
	background-color: #fc0;
}

th.sorted {
	background-color: orange;
}

th a,th a:visited {
	color: black;
}

th a:hover {
	text-decoration: underline;
	color: black;
}

th.sorted a,th.sortable a {
	background-position: right;
	display: block;
	width: 100%;
}



tr.odd {
	background-color: #fff
}

tr.tableRowEven,tr.even {
	background-color: #fea
}

div.exportlinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	margin: 2px 0 10px 0;
	width: 79%;
}

span.export {
	padding: 0 4px 1px 20px;
	display: inline;
	display: inline-block;
	cursor: pointer;
}



span.pagebanner {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	margin-top: 10px;
	display: block;
	border-bottom: none;
}

span.pagelinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	display: block;
	border-top: none;
	margin-bottom: -5px;
}


.group-1 {
    font-weight:bold;
    padding-bottom:10px;
    border-top:1px solid black;
}
.group-2 {
    font-style:italic;
    border-top: 1px solid black;

}
.subtotal-sum, .grandtotal-sum {
    font-weight:bold;
    text-align:right;
}
.subtotal-header {
    padding-bottom: 0px;
    border-top: 1px solid white;
}
.subtotal-label, .grandtotal-label {
    border-top: 1px solid white;
    font-weight: bold;
}
.grouped-table tr.even {
    background-color: #fff;
}
.grouped-table tr.odd {
    background-color: #fff;
}
.grandtotal-row {
    border-top: 2px solid black;
}

</style>

</head>

<script>

	$(function() {
		$( "#date1" ).datepicker().attr( 'readOnly' , 'true' );
	});

	$(function() {
		$( "#date2" ).datepicker().attr( 'readOnly' , 'true' );;
	});

	function validateDates(){
		var dateStart = document.getElementById('date1').value.split('/');
	
			if (dateStart != "" && dateStart.length > 0) {
	
				var boolean1 = validateDate(dateStart);
			}
	
			var dateEnd = document.getElementById('date2').value.split('/');
	
			if (dateEnd != "" && dateEnd.length > 0) {
				var boolean2 = validateDate(dateEnd);
			}
	}

	function validateDate(dateGiven) {

		var today = new Date();

		var dob_mm = dateGiven[0];
		var dob_dd = dateGiven[1];
		var dob_yy = dateGiven[2];

		var birthDate = new Date();
		birthDate.setFullYear(dob_yy, dob_mm - 1, dob_dd);

		if ((today.getFullYear() - 2) > birthDate.getFullYear()) {
			return false;
		}

		if ((dob_dd != birthDate.getDate())
				|| (dob_mm - 1 != birthDate.getMonth())
				|| (dob_yy != birthDate.getFullYear())) {
			return false;
		}

		if (today.getTime() < birthDate.getTime()) {
			return false;
		}

	}

	function setWhereClause(type) {

		var whereClause = '';
		var filterStatement = '';
		switch (type) {
		case 'B1':
			whereClause = 'session_start_ts > (CURRENT_TIMESTAMP - interval \'1\' day)';
			filterStatement = ' in last 24 hours';
			break;
		case 'B2':
			whereClause = 'session_start_ts > (CURRENT_TIMESTAMP - interval \'12\' hour)';
			filterStatement = ' in last 12 hours';
			break;
		case 'B3':
			whereClause = 'session_start_ts > (CURRENT_TIMESTAMP - interval \'1\' hour)';
			filterStatement = ' in last 1 hour';
			break;
		case 'B4':

			//Validate the dates
			validateDates();

			//Step 1: Get List Box value, if it is not empty
			var fieldName = document.getElementById('field_name').value;
			var fieldValue = document.getElementById('field_value').value;
			if (fieldValue != null && fieldValue.length > 0) {

				whereClause = " lower(" + fieldName + ") like lower('%"
						+ fieldValue + "%')";
				filterStatement = ' with ' + fieldName + " = " + fieldValue;
			}

			//Step 2: Get the Version Value
			var statuses = document.getElementById('version');
			var status = statuses.options[statuses.selectedIndex].value;
			if (status == 'P' || status == 'M') {
				if (whereClause) {
					whereClause = whereClause + " and input_page = '" + status
							+ "'";
				} else {
					whereClause = whereClause + " input_page = '" + status
							+ "'";
				}

			}
			if (status == 'P') {

				filterStatement = filterStatement
						+ ' made using the product version';
			}
			if (status == 'M') {

				filterStatement = filterStatement
						+ ' made using the marketing version';
			}

			//Step 3: Get the Dates
			var date1 = document.getElementById('date1').value;
			var date2 = document.getElementById('date2').value;

			if (date1 && date2) {

				if (whereClause) {
					whereClause = whereClause
							+ "and session_start_ts between to_timestamp('"
							+ date1
							+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"
							+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					filterStatement = filterStatement + 'start date ' + date1
							+ 'end date ' + date2;

				} else {
					whereClause = whereClause
							+ " session_start_ts between to_timestamp('"
							+ date1
							+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"
							+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					filterStatement = filterStatement + 'start date ' + date1
							+ 'end date ' + date2;

				}

			} else {
				if (date1 && !date2) {
					if (whereClause) {

						whereClause = whereClause
								+ "and to_char(session_start_ts  ,'mm/dd/yyyy' )='"
								+ date1 + "'";
						filterStatement = filterStatement + 'start date '
								+ date1 + 'end date ' + date2;
					} else {
						whereClause = whereClause
								+ " to_char(session_start_ts  ,'mm/dd/yyyy' )='"
								+ date1 + "'";
						filterStatement = filterStatement + 'start date '
								+ date1 + 'end date ' + date2;
					}
				} else {

					if (!date1 && date2) {
						if (whereClause) {
							whereClause = whereClause
									+ "and to_char(session_end_ts  ,'mm/dd/yyyy' )='"
									+ date2 + "'";
							filterStatement = filterStatement + 'start date '
									+ date1 + 'end date ' + date2;
						} else {
							whereClause = whereClause
									+ " to_char(session_end_ts  ,'mm/dd/yyyy' )='"
									+ date2 + "'";
							filterStatement = filterStatement + 'start date '
									+ date1 + 'end date ' + date2;
						}

					}
				}

			}
			if (!whereClause) {
				whereClause = "session_start_ts <= systimestamp";
			}

			break;
		}

		document.getElementById('whereClause').value = whereClause;
		document.getElementById('filterStatement').value = filterStatement;
	}
</script>    

<body>



<form method="POST" action="showDataTop">

	<table border="1" style="border-color:#C0C0C0;width:1100px" >
		<tbody><tr>
			<td bgcolor="#C0C0C0">
				<input type="submit" value="24 Hours" name="B1" title="View requests made in last 24 hours" onclick="setWhereClause('B1');">
				<input type="submit" value="12 Hours" name="B2" title="View requests made in last 12 hours" onclick="setWhereClause('B2');">
				<input type="submit" value="1 Hour" name="B3" title="View requests made in last 1 hour" onclick="setWhereClause('B3');">
			</td>
			<td bgcolor="#66FFCC">
				<p>
					&nbsp;Field:
					<select size="1" id="field_name" name="field_name">
						<option value="first_name">Claimant Name</option>
						<option value="zipcode" selected="selected">Zip Code</option>
						<option value="no_of_dependents" >No. of dependents</option>
						<option value="household_income">Household Income</option>
					</select>
					<input type="text" id="field_value" name="field_value" size="10" maxlength="100">
					Version Accessed:
					<select size="1" id="version" name="status">
						<option value="both" selected="selected">Both</option>
						<option value="P" >Product</option>
						<option value="M">Marketing</option>
					</select>
					Date Start:
					<input type="text" id="date1" name="date1" size="10" maxlength="10"> End:
					<input type="text" id="date2" name="date2" size="10" maxlength="10">
					<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
				</p>
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">
</form>

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = "session_start_ts <= systimestamp";
		
	}
	
%>

<sql:query dataSource="jdbc/ghixDS" var="result1">
	select id, session_id, first_name, zipcode, county,email_address,
	decode(is_married, 'Y' , 'Yes' ,'N' , 'No') is_married, 
	no_of_dependents, household_income, 
	to_char(claimant_dob,'MM/DD/YYYY') claimant_dob,
	decode(is_disabled, 'Y' , 'Yes' ,'N' , 'No') is_disabled, 
	alimony_deduction, student_loan_deduction,
	round(fpl,2) fpl,
	csr, final_credit_displayed, final_plan_showed,
	decode (benchmark_status,'Y','Success','N','Failure') benchmark_status,
	benchmark_status_desc,
	'<a href="showDataBottom?request='||id||'" target="bottom">Session Data</a>' as session_data_clob,
	decode (screen_visited, 1,'Profile',2,'Household',3,'Income',4,'Results') screen_visited,
	'<a href="showDataBottom?email='||id||'" target="bottom">Email</a>' as email,
	results_type,
	round(premium_value,2) premium_value,
	to_char(session_start_ts,'MM/DD/YYYY HH:MI:SS') session_start_ts,
	to_char(session_end_ts,'MM/DD/YYYY HH:MI:SS') session_end_ts,
	TO_CHAR(TRUNC(total_time_spent/3600000),'FM9900') || ':' || TO_CHAR(TRUNC(MOD(total_time_spent/1000,3600)/60),'FM00') || ':' || TO_CHAR(MOD(total_time_spent/1000,60),'FM00') total_time_spent,
	decode(input_page, 'P' , 'Product' ,'M' , 'Marketing') input_page,state_cd,source
	from  ( select  *  from   prescreen_data  order by  id desc )
	where <%=whereClause%>
	and rownum <= 100
	order by id desc
</sql:query> 

Requests made for Prescreen Eligibility

<%
	if (request.getParameter("filterStatement") != null) {
		out.println(request.getParameter("filterStatement"));
	}
%>

<br>

<display:table name="${result1.rows}"  requestURI="showDataTop" id="table1" style="white-space: pre-wrap;" export="false" pagesize="10">

	<display:column property="id" title="ID" sortable="false" />
	<display:column property="session_data_clob" title="Session Data" sortable="false"  value="Session Data"/>
	
	<display:column property="first_name" title="Name" sortable="false" />
	<display:column property="zipcode" title="Zip Code" sortable="false" />
	<display:column property="county" title="County" sortable="false" />
	<display:column property="state_cd" title="State Code" sortable="false" />
	<display:column property="is_married" title="Is Married" sortable="false" />
	<display:column property="no_of_dependents" title="No. of Dependents" sortable="false" />	
	<display:column property="household_income" title="Household income" sortable="false" />
	
	<display:column property="claimant_dob" title="Claimant's Dob" sortable="false" />
	<display:column property="is_disabled" title="Is Any member Disabled?" sortable="false" />
	<display:column property="alimony_deduction" title="Alimony Deduction" sortable="false" />
	<display:column property="student_loan_deduction" title="Student Loan Deduction" sortable="false" />
	
	<display:column property="fpl" title="FPL" sortable="false" />
	<display:column property="csr" title="CSR" sortable="false" />
	<display:column property="final_credit_displayed" title="APTC" sortable="false" />
	<display:column property="final_plan_showed" title="Final Plan Id" sortable="false" />
	<display:column property="premium_value" title="Benchmark Premium Estimate" sortable="false" />
	<display:column property="benchmark_status" title="Benchmark Status" sortable="false" />
	<display:column property="benchmark_status_desc" title="Benchmark Status Description" sortable="false" />
	
	<display:column property="screen_visited" title="Screen Visited" sortable="false" />
	<display:column property="email" title="E-mail" sortable="false"/>
	<display:column property="results_type" title="Result Type" sortable="false" />
	
	<display:column property="session_start_ts" title="Session Start time" sortable="false" />
	<display:column property="session_end_ts" title="Session End time" sortable="false" />
	<display:column property="total_time_spent" title="Total Session time (hh:mm:ss)" sortable="false" />
	<display:column property="input_page" title="Version Accessed" sortable="false" />
	<display:column property="source" title="Source" sortable="false" />

</display:table>

 </body> 
 </html>


