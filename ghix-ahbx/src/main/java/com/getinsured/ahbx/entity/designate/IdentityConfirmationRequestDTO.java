//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference
// Implementation, vJAXB 2.1.10 in JDK 6
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Any modifications to this file will be lost upon recompilation of the source schema.
// Generated on: 2013.07.17 at 02:45:11 PM IST
//

package com.getinsured.ahbx.entity.designate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for identityConfirmationRequestDTO complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="identityConfirmationRequestDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="acceptanceStatus" type="{http://ahbx.accenture.com/AHBXValidation}acceptanceStatusValue"/>
 *         &lt;element name="agentAssisterId" type="{http://ahbx.accenture.com/AHBXValidation}LongLength10"/>
 *         &lt;element name="individualID" type="{http://ahbx.accenture.com/AHBXValidation}LongLength10" minOccurs="0"/>
 *         &lt;element name="employerID" type="{http://ahbx.accenture.com/AHBXValidation}LongLength10" minOccurs="0"/>
 *         &lt;element name="recordType" type="{http://ahbx.accenture.com/AHBXValidation}AgentAssisterType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "identityConfirmationRequestDTO", propOrder = { "acceptanceStatus", "agentAssisterId", "individualID",
		"employerID", "recordType" })
public class IdentityConfirmationRequestDTO {

	@XmlElement(required = true)
	protected AcceptanceStatusValue acceptanceStatus;
	protected long agentAssisterId;
	protected Long individualID;
	protected Long employerID;
	@XmlElement(required = true)
	protected AgentAssisterType recordType;

	/**
	 * Gets the value of the acceptanceStatus property.
	 * 
	 * @return possible object is {@link AcceptanceStatusValue }
	 */
	public AcceptanceStatusValue getAcceptanceStatus() {
		return acceptanceStatus;
	}

	/**
	 * Sets the value of the acceptanceStatus property.
	 * 
	 * @param value
	 *            allowed object is {@link AcceptanceStatusValue }
	 */
	public void setAcceptanceStatus(AcceptanceStatusValue value) {
		this.acceptanceStatus = value;
	}

	/**
	 * Gets the value of the agentAssisterId property.
	 */
	public long getAgentAssisterId() {
		return agentAssisterId;
	}

	/**
	 * Sets the value of the agentAssisterId property.
	 */
	public void setAgentAssisterId(long value) {
		this.agentAssisterId = value;
	}

	/**
	 * Gets the value of the individualID property.
	 * 
	 * @return possible object is {@link Long }
	 */
	public Long getIndividualID() {
		return individualID;
	}

	/**
	 * Sets the value of the individualID property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 */
	public void setIndividualID(Long value) {
		this.individualID = value;
	}

	/**
	 * Gets the value of the employerID property.
	 * 
	 * @return possible object is {@link Long }
	 */
	public Long getEmployerID() {
		return employerID;
	}

	/**
	 * Sets the value of the employerID property.
	 * 
	 * @param value
	 *            allowed object is {@link Long }
	 */
	public void setEmployerID(Long value) {
		this.employerID = value;
	}

	/**
	 * Gets the value of the recordType property.
	 * 
	 * @return possible object is {@link AgentAssisterType }
	 */
	public AgentAssisterType getRecordType() {
		return recordType;
	}

	/**
	 * Sets the value of the recordType property.
	 * 
	 * @param value
	 *            allowed object is {@link AgentAssisterType }
	 */
	public void setRecordType(AgentAssisterType value) {
		this.recordType = value;
	}

	@Override
	public String toString() {
		return new StringBuilder().append("IdentityConfirmationRequestDTO : agentAssisterId = ").append(agentAssisterId).append(" , individualID =").
				append(individualID).append(" ,employerID = ").append(employerID).toString();
	}
}
