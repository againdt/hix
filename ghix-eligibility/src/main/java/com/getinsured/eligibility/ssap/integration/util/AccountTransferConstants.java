package com.getinsured.eligibility.ssap.integration.util;


public final class AccountTransferConstants {

	private AccountTransferConstants(){}
	
	public static final String AT_VERSION_TXT = "2.3";
	public static final String AT_VERSION_TXT_2 = "2.4";
	public static final String REQUEST_SOURCE = "request_source";
    public static final String REQUEST_SOURCE_VALUE = "inBoundAccountTransferRequest";
    public static final String ERROR_CODE_ONE = "AT0000501";
    public static final String ERROR_CODE_TWO = "AT0000502";
    public static final String ERROR_CODE_THREE = "AT0000503";
    public static final String ERROR_CODE_FOUR = "AT0000504";
    public static final String ERROR_CODE_FIVE = "AT0000505";
    public static final String ERROR_CODE_SIX = "AT0000506";
    public static final String INBOUND = "INBOUND";
    public static final String OUTBOUND = "OUTBOUND";
}
