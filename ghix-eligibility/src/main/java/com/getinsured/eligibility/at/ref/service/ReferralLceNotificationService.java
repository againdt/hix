package com.getinsured.eligibility.at.ref.service;

import java.util.Date;

import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;


/**
 * @author chopra_s
 *
 */

public interface ReferralLceNotificationService {
	
	 String generateDemographicChangeNotice(String caseNumber) throws Exception;
	 //String generateNoChangeNotice(String caseNumber) throws Exception;
	 String generateSEPEventNotice(String caseNumber) throws Exception;
	 String generateSEPEventNoticeQEPConfirmLifeEvent(String caseNumber,boolean triggerDocumentRequiredNoticeFlag) throws Exception;
	 String generateFinancialLCENotice(String caseNumber) throws Exception;
	 String generateEligibilityLossNotice(String caseNumber) throws Exception;
	 String generateSEPDenialNotice(String caseNumber) throws Exception;
	 String generateQEPEventNotice(String caseNumber) throws Exception;
	 String generateSEPEventKeepOnlyNotice(String caseNumber) throws Exception;
	String generateAPTCChangeNotice(String caseNumber) throws Exception;
	String generateHHLostEligibilityNoticewithNonQhpIndicators(String caseNumber, long enrolledApplicationId) throws Exception;
	String generateNonFinConversionNoticeWithAptcOnlyIneligibile(String caseNumber) throws Exception;
	String generateNonFinConversionNoticeWithAptcCsrIneligibile(String caseNumber) throws Exception;
	String generateAddRemoveAutomationNotice(String caseNumber) throws Exception;
	String generateChangeActionNotice(String caseNumber, LCEProcessRequestDTO lceProcessRequestDTO, boolean triggerEE20) throws Exception;
	String generateDenialNotice(String caseNumber)  throws Exception;
	String generateNonFinToFinConversionNoticeManual(String caseNumber) throws Exception;
	String generateNonFinToFinConversionNotice(String caseNumber) throws Exception;
	String generateCSPlanChangeNotice(String caseNumber, boolean isHealthDisEnrolled, boolean isDentalDisEnrolled, Date terminationDate) throws Exception;
	String generateRatingAreaChangeNotice(String caseNumber, boolean isHealthDisEnrolled, boolean isDentalDisEnrolled, Date terminationDate) throws Exception;
	String generateAutomationNoticeEE060(String caseNumber) throws Exception;
	String generateLceDocumentRequiredNotice(String caseNumber) throws Exception;
	String generateAIANLCENotice(String caseNumber) throws Exception;
	String generateLossOfCSRIndNotice(String caseNumber) throws Exception;
	String generateResponseATChangeNotice(String caseNumber) throws Exception;
}
