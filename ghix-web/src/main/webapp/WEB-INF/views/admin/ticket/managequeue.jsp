<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/inbox.css" />" />
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>

<style rel="stylesheet" type="text/css">
	.row-fluid .span70 {
    width: 70%;
}
.head-btn{
	float:right;
	width:auto;
}
</style>

<div class="gutter10">
	<div class="row-fluid" id="titlebar">
		<h1 id="skip"><spring:message code="lbl.mq_title"/></h1>
	</div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">	
    		<jsp:include page="ticketsidebar.jsp"></jsp:include>
		</div>
		
		<div class="span9" id="rightpanel">
			  <div style="font-size: 14px; color: red" id="tkmErrorDiv">
					<c:if test="${errorMsg != null && errorMsg != ''}">
						<p><c:out value="${errorMsg}"></c:out><p/>
						<c:remove var="errorMsg"/>
					</c:if>
				</div>
			<div class="header">
				<h4 class="pull-left" id="pageTitle"> <spring:message code="lbl.mq_subTitle"/> </h4>
				<div class="head-btn">
					<a href="<c:url value='/ticketmgmt/ticket/ticketlist'/>" id="cancelBtn" class="btn btn-small "><spring:message code="btn.mq_Cancel"/></a>
					<a class="btn btn-primary btn-small " id="updateQueueBtn"><spring:message code="btn.mq_Update"/></a>
				</div>
			</div>
            <div class="gutter10 hide" id="savingMessage">
                <div class="row-fluid">
                    Saving... <img style="margin-bottom:9px;" src="<c:url value='/resources/images/loader.white.gif' />" />
                </div>
            </div>
			<div class="gutter10" id="mainActivity">
                <div class="row-fluid margin20-b"><spring:message code="lbl.mq_infomessage" /></div>
				<div class="row-fluid">
					<div id="queueListDiv" class="control-group">
						<label class="control-label required"><spring:message code="lbl.mq_queuelist"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> </label>
						<div class="controls">
							<select class="input-large" name="queueType" id="queueType" onchange="javascript:loadUsers();">
                                <option value=""><spring:message code="lbl.mq_select" /></option>
								<c:forEach var="queue" items="${queueList}">
									<option value="<encryptor:enc value = "${queue.id}"/>">
										<c:out value="${queue.name}" />
								</c:forEach>
							</select>
                            <img style="margin-bottom:9px;" id="loadingMessage" title="Loading users" src="<c:url value='/resources/images/loader.white.gif'/>" />
							<div id="queue_error"></div>
						</div>
					</div>
					<!-- end of control-group -->
					<div id="queueMembers" class="control-group" >
						<label class="control-label required"><spring:message code="lbl.mq_adminlist"/> </label>
						<select id="addressBook" name="addressBook"
							data-placeholder="Add users to workgroup..."
							style="width: 543px; display: none" multiple="multiple"
							tabindex="3" class="chosen-select" size="10">
						</select> <input id="names" name="names" type="hidden">
						<div class="error" id="addressBook_error"></div>
					</div>
					<!-- end of control-group -->			
				</div>
                <div class="row-fluid hide margin20-t" id="tblExistingUsers">
                    <div class="row-fluid"><spring:message code="lbl.mq_deleteTitle" /></div>
                    <div class="row-fluid margin10-t">
                        <table class="table table-hover extUserList">
                        </table>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    //$('#errorMsg').html('');
    var xhrRequest = null;
    var cancelReq = false;
	var cancelReqAdd=false;
	var xhrRequestAdd=null;
	var idsAdded=[];
    function loadUsers() {

        if (xhrRequest != null) {
            cancelReq = true;
            xhrRequest.abort();
        };

        disableControls();

        if ($("#queueType").val() == "") {
            $("#loadingMessage").hide();
            return;
        };

		var queueName = $("#queueType option:selected").text();
		 var pathURL = '<c:url value="getexistingqueueusers"></c:url>';
		
		//$('#errorMsg').val("");
		
		 
		

		xhrRequest=$.ajax({
			url : pathURL,
			type : "POST",
			data : {
				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				queueName : queueName
			},
			success: function(response,xhr)
            {
               if(isInvalidCSRFToken(xhr))
                   return;
               cancelReq = false;
				try{
					 JSON.parse(response);
					 
				   }
				   catch(e){
					  $('#tkmErrorDiv').html(response);
					}
				   loadData(response);
    		},
			error: function (e) {
			    if (!cancelReq) {
			        alert("Failed to Add users");
			    };
    		},
		});
		
		//enableControls();
	}
	
	// load the jquery chosen plugin with the AJAX data
	function loadData(response) {
	    enableControls();

	    


		$("#loadingContacts").show('slow','linear');
		$("#addressBook").html('');
		
		$("#tblExistingUsers").show();

		var tblExtUsers = $("#tblExistingUsers").find(".extUserList");

		var tableHeader="<tr>";
		tableHeader +="<th style=\"width:30px;\"></th>";
		tableHeader +="<th><spring:message code="lbl.tableHeaderMember"/></th>";
		tableHeader +="<th><spring:message code="lbl.tableHeaderUserRole"/></th>";
		tableHeader +="<th style=\"width:60px;\"><spring:message code="lbl.tableHeaderDelete"/></th>";
		tableHeader +="</tr>";

		tblExtUsers.html(tableHeader);       

		
		var respData = $.parseJSON(response);

		var listOfExistUsersArray=[];
		for ( var key in respData) {
		    listOfExistUsersArray.push({
		        keyid:key,
		        keydata:$.parseJSON(respData[key])
		    });
		};

		listOfExistUsersArray=listOfExistUsersArray.sort(function(a,b){
		    var aLastName=String(a.keydata.name).split(" ")[String(a.keydata.name).split(" ").length-1];
		    var bLastName=String(b.keydata.name).split(" ")[String(b.keydata.name).split(" ").length-1];
		    return aLastName == bLastName ? 0 : aLastName < bLastName ? -1 : 1
		});

		var cnt = 1;
		idsAdded=[];
		for (var k=0;k<listOfExistUsersArray.length;k++) {
		    //$('#addressBook').append("<option value='"+key+"' selected='selected'>"+ respData.existingUsers[key] + "</option>");
		    var dData=listOfExistUsersArray[k].keyid;
		    var rObj=listOfExistUsersArray[k].keydata;
		    var extUserRow = ""
		    extUserRow +="<tr>";
		    extUserRow += "<td>" + cnt + ".</td>";
		    extUserRow += "<td>" +rObj.name + "</td>";
		    extUserRow +="<td>"+rObj.role+"</td>";
		    extUserRow += "<td style=\"text-align:center;\"><input  type=\"checkbox\" class=\"usersToDelete\" value=\"" + listOfExistUsersArray[k].keyid + "\" /></td>";
		    extUserRow += "</tr>";
		    idsAdded.push(listOfExistUsersArray[k].keyid);
		    cnt++;
		    tblExtUsers.append(extUserRow);
		};
		for ( var key in respData.otherUsers) {
		    $('#addressBook').append("<option value='"+key+"'>" + respData.otherUsers[key]+ "</option>");
		};


		$("#addressBook").chosen().change(function() {
			if ($('ul.chzn-choices li').length >= 1) {
				$('#addressBook_error').html('');
			}
		});

		$('#addressBook').trigger("liszt:updated");
		$("#loadingContacts").hide('slow', 'linear');
		
		setTimeout(function(){
			$("#addressBook_chzn").find("input").off("keyup",onKeyOuOnAdd);
			$("#addressBook_chzn").find("input").on("keyup",onKeyOuOnAdd);
		});
		
	};
	var doPull=true;
	function onKeyOuOnAdd(e)
	{	    
	    var val=$("#addressBook_chzn").find("input").val();
	    val=String(val).trim();
	    if(String(val).length < 3){
	        doPull=true;
	    };
		if(String(val).length < 3){
		    return;
		};
		if(String(val).length > 3){
		    doPull=false;
		};

		if(!doPull){return;};

		if(xhrRequestAdd != null)
		{
			cancelReqAdd=true;
			xhrRequestAdd.abort();
		};
		
		xhrRequestAdd=$.ajax({
			url:'<c:url value="getworkgroupuserstoadd"></c:url>',
			type:"POST",
			data:{
				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				username:val
			},
			success:function(data,xhr)
			{
				if(isInvalidCSRFToken(xhr))
                  return;
				  cancelReqAdd=false;
				  reCreateChoose(data);
				
			},
			error:function()
			{
				if(!cancelReqAdd)
				{
			     alert("Failed to fetch users");
				 }
			}
		});
		//$('#addressBook').append("<option value='1234'>adad</option>");
		//$('#addressBook').trigger("liszt:updated");
	};
	
	function reCreateChoose(data)
	{
	    data=$.parseJSON(data);
	    var hasKeys=false;
	    for ( var key in data.otherUsers) {
	        hasKeys=true;
	    };
	    if(!hasKeys){return;};
		var addedKeys=[];
		$('#addressBook').children("option").each(function(idx,obj){
		    obj=$(obj);
		    if(obj.is(":selected")){
		        addedKeys.push(obj.html());
		    }else{
		        obj.remove();
		    };
		});
		for ( var key in data.otherUsers) {
		    if(addedKeys.indexOf(data.otherUsers[key]) < 0)
		    {
		        if(idsAdded.indexOf(key) < 0)
		        {
		            $('#addressBook').append("<option value='"+key+"' >"+ data.otherUsers[key] + "</option>");
		        };
		    };
		};
		$('#addressBook').trigger("liszt:updated");
	};

	//called when the update button is clicked
	function updateQueue() {
		var newUsers = new Array();
		$("#addressBook").each(function() {
			$(this).children("option").each(function() {
				if ($(this).is(':selected')) {
					var $this = $(this);
					newUsers.push($this.val());
				}
			});
		});

		$(".usersToDelete").each(function (idx,obj) {
		    obj = $(obj);
		    if (obj.is(":checked") == false) {
		        newUsers.push(obj.val());
		    };
		});

		var queueId = $("#queueType option:selected").val();
		sendUpdatedQueueMembers(newUsers, queueId);
	}

	//send queue member list
	function sendUpdatedQueueMembers(newUsers, queueId) {
		var pathURL = '<c:url value="updatequeueusers"></c:url>';

	//	$('#errorMsg').val("");
		disableControls();
		$("#cancelBtn").attr("disabled", true);
		$("#savingMessage").show();
		$("#mainActivity").hide();

		$.ajax({
			url : pathURL,
			type : "POST",
			data : {
				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				queueId : queueId,
				users : JSON.stringify(newUsers)
			},
			success: function(response,xhr)
            {
			    if (isInvalidCSRFToken(xhr)) {
			        return;
			    };
			    if (response != "" && response != null) {
			        $("#savingMessage").hide();
			        $("#mainActivity").show();

                        $('#tkmErrorDiv').html(response);
                        enableControls();
                        $("#cancelBtn").attr("disabled", false);
			    } else {
			        $("#savingMessage>div").html("Successfully saved...");
                        window.location.replace("<c:url value='/ticketmgmt/ticket/ticketlist'/>");
                    };
			},
			error : function(e) {
			    alert("Failed to update users");
			    $("#savingMessage").hide();
			    $("#mainActivity").show();
			    enableControls();
			    $("#cancelBtn").attr("disabled", false);
			},
		});

		
	}

	function isInvalidCSRFToken(xhr){
        var rv = false;
        if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {                  
                alert($('Session is invalid').text());
        rv = true;
        }
        return rv;
	}
	
	function disableControls() {
	    //$("#queueType").attr('disabled', 'disabled');
	    $("#tblExistingUsers").hide();
	    $("#loadingMessage").show();
		$("#updateQueueBtn").attr("disabled", true);
		//$("#cancelBtn").attr("disabled", true);
		$("#updateQueueBtn").unbind("click", updateQueue);
		$("#queueMembers").hide();
	}

	function enableControls() {
	    //$("#queueType").removeAttr('disabled');
	    $("#loadingMessage").hide();
		$("#updateQueueBtn").attr("disabled", false);
		//$("#cancelBtn").attr("disabled", false);
		$("#updateQueueBtn").bind("click", updateQueue);
		$("#queueMembers").show();
	}

	//page load init function
	$(document).ready(function() {
	    loadUsers();

	});
</script>