<%@page import="java.util.HashMap"%>
<%@ taglib prefix="surl" uri="/WEB-INF/tld/secure-url.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<input type="hidden" id="permissionMap1" value="${permissionMap}" />

<input type="hidden" id="memberPrmHistActions" value="${permissionMap.memberPrmHistActions}" />
<input type="hidden" id="memberEnrlmntPrmHist" value="${permissionMap.memberEnrlmntPrmHist}" />
<input type="hidden" id="memberEnrlmnt834hist" value="${permissionMap.memberEnrlmnt834hist}" />
<input type="hidden" id="memberEnrlmntLtst834" value="${permissionMap.memberEnrlmntLtst834}" />
<input type="hidden" id="memberEnrlmntAddnlinfo" value="${permissionMap.memberEnrlmntAddnlinfo}" />
<input type="hidden" id="memberPrmHistChange" value="${permissionMap.memberPrmHistChange}" />
<input type="hidden" id="memberPrmHistCancel" value="${permissionMap.memberPrmHistCancel}" />
<input type="hidden" id="memberEnrlmntStrtenddt" value="${permissionMap.memberEnrlmntStrtenddt}" />
<input type="hidden" id="memberEnrlmntMembrName" value="${permissionMap.memberEnrlmntMembrName}" />
<input type="hidden" id="memberEnrlmntMembrGender" value="${permissionMap.memberEnrlmntMembrGender}" />
<input type="hidden" id="memberEnrlmntMembrSsn" value="${permissionMap.memberEnrlmntMembrSsn}" />
<input type="hidden" id="memberEnrlmntDpndtStrtEndDt" value="${permissionMap.memberEnrlmntDpndtStrtEndDt}" />
<input type="hidden" id="memberEnrlmntAptc" value="${permissionMap.memberEnrlmntAptc}" />
<input type="hidden" id="memberEnrlmntNetprmum" value="${permissionMap.memberEnrlmntNetprmum}" />
<input type="hidden" id="memberEnrlmntEhbprmum" value="${permissionMap.memberEnrlmntEhbprmum}" />
<input type="hidden" id="memberEnrlmntGrpMaxPrmum" value="${permissionMap.memberEnrlmntGrpMaxPrmum}" />
<input type="hidden" id="memberEnrlmntMembrNmEditable" value="${permissionMap.memberEnrlmntMembrNmEditable}" />
<input type="hidden" id="memberEnrlmntMembrGndrEditable" value="${permissionMap.memberEnrlmntMembrGndrEditable}" />
<input type="hidden" id="memberEnrlmntMembrSsnEditable" value="${permissionMap.memberEnrlmntMembrSsnEditable}" />
<input type="hidden" id="memberEnrlmntAptcEditable" value="${permissionMap.memberEnrlmntAptcEditable}" />
<input type="hidden" id="memberEnrlmntNetPrmumEditable" value="${permissionMap.memberEnrlmntNetPrmumEditable}" />
<input type="hidden" id="memberEnrlmntEhbPrmumEditable" value="${permissionMap.memberEnrlmntEhbPrmumEditable}" />
<input type="hidden" id="memberEnrlmntGrpmaxPrmumEditable" value="${permissionMap.memberEnrlmntGrpmaxPrmumEditable}" />

<input type="hidden" id="memberEnrlmntMembrDpndntNm" value="${permissionMap.memberEnrlmntMembrDpndntNm}" />
<input type="hidden" id="memberEnrlmntMembrDpndntGndr" value="${permissionMap.memberEnrlmntMembrDpndntGndr}" />
<input type="hidden" id="memberEnrlmntMembrDpndntSsn" value="${permissionMap.memberEnrlmntMembrDpndntSsn}" />

<input type="hidden" id="memberEnrlmntMembrDpndntNmEditable" value="${permissionMap.memberEnrlmntMembrDpndntNmEditable}" />
<input type="hidden" id="memberEnrlmntMembrDpndntGndrEditable" value="${permissionMap.memberEnrlmntMembrDpndntGndrEditable}" />
<input type="hidden" id="memberEnrlmntMembrDpndntSsnEditable" value="${permissionMap.memberEnrlmntMembrDpndntSsnEditable}" />

<input type="hidden" id="memberEnrlmntStrtenddtEditable" value="${permissionMap.memberEnrlmntStrtenddtEditable}" />
<input type="hidden" id="memberEnrlmntDpndtStrtEndDtEditable" value="${permissionMap.memberEnrlmntDpndtStrtEndDtEditable}" />
<input type="hidden" id="memberEnrlmntAppType" value="${permissionMap.memberEnrlmntAppType}" />
<input type="hidden" id="memberEnrlmntGroupMaxAptcEditable" value="${permissionMap.memberEnrlmntGroupMaxAptcEditable}" />
<input type="hidden" id="memberEnrlmntGroupMaxAptc" value="${permissionMap.memberEnrlmntGroupMaxAptc}" />
<div class="gutter10">
    <div class="row-fluid">
        <h1 class="householdInfo">${household.firstName} ${household.lastName} (ID: ${householdId})</h1>
    </div>
    <div class="row-fluid">
        <div class="span3" id="sidebar">
               <jsp:include page="consumerSummaryNavigation.jsp"></jsp:include>
        </div>

        <div class="span9" id="rightpanel" ng-app="enrollmentHistoryApp">
            <div class="row-fluid">
                <div ui-view></div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
	$("#enrollmentMenu").addClass("active navmenu");
});
const ALL = "ALL";

var csrStorage = {
		edit:{header: '<spring:message code="indportal.portalhome.editapplication2"/>', content: '<spring:message code="indportal.portalhome.editapplicationcontent" javaScriptEscape="true"/> '},
		initiate:{header: '<spring:message code="indportal.portalhome.initiateverifications2" />', content: '<spring:message code="indportal.portalhome.initiateverificationscontent" javaScriptEscape="true"/> '},
		rerun:{header: '<spring:message code="indportal.portalhome.reruneligibility2"/>', content: '<spring:message code="indportal.portalhome.reruneligibilitycontent" javaScriptEscape="true"/> '},
		cancelTerm:{header: '<spring:message code="indportal.portalhome.cancelorterminatemyplan2" />', content: '<spring:message code="indportal.portalhome.cancelorterminatemyplancontent" javaScriptEscape="true"/>'},
		update:{header: '<spring:message code="indportal.portalhome.updatecarrier2" />', content: '<spring:message code="indportal.portalhome.updatecarriercontent" javaScriptEscape="true"/>'},
		view:{header: 'view', content: 'csrStorage.view.content'},
		specialEnroll:{header: '<spring:message code="indportal.portalhome.openspecialenorollment2"/>', content: '<spring:message code="indportal.portalhome.openspecialenorollmentcontent" javaScriptEscape="true"/>'},
		coverageDate:{header: '<spring:message code="indportal.portalhome.changecoveragedate2" />', content: '<spring:message code="indportal.portalhome.changecoveragedatecontent" javaScriptEscape="true"/>'},
		reinstate:{header: '<spring:message code="indportal.portalhome.reinstateenrollment2" />', content: '<spring:message code="indportal.portalhome.reinstateenrollmentcontent" javaScriptEscape="true"/> '},
		overrideSEPDenial:{header: '<spring:message code="indportal.portalhome.overridesepdenial2"/>', content: '<spring:message code="indportal.portalhome.overridesepdenialcontent" javaScriptEscape="true"/>'}
};
</script>

<script src="<c:url value="/resources/angular/mask.js" />"></script>
<script src="<c:url value="/resources/js/moment.min.js" />"></script> 
<script src="<c:url value="/resources/js/spring-security-csrf-token-interceptor.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/dashboardAnnouncement/angular-ui-router.min.js" />"></script>

<script src="<c:url value="/resources/js/cap/enrollmentHistory/enrollmentHistory.app.js"/>"></script> 
<script src="<c:url value="/resources/js/cap/enrollmentHistory/enrollmentHistory.router.js"/>"></script> 
<script src="<c:url value="/resources/js/cap/enrollmentHistory/enrollmentHistory.controller.js"/>"></script> 
<script src="<c:url value="/resources/js/cap/enrollmentHistory/enrollmentHistory.filter.js"/>"></script> 
<script src="<c:url value="/resources/js/cap/enrollmentHistory/enrollmentHistory.directive.js"/>"></script> 
<script src="<c:url value="/resources/js/cap/enrollmentHistory/enrollmentHistory.service.js"/>"></script> 

<script src="<c:url value="/resources/js/cap/enrollmentHistory/premiumHistory.controller.js"/>"></script> 
<script src="<c:url value="/resources/js/cap/enrollmentHistory/AddOrChangeEnrollmentTxn.controller.js"/>"></script> 
        