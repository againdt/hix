package com.getinsured.eligibility;

import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.benchmark.service.BenchmarkPlanService;
import com.getinsured.eligibility.benchmark.service.EligibilityBenchMarkServiceResponse;
import com.getinsured.eligibility.indportal.enrollment.api.IndPortalEnrollmentUtility;
import com.getinsured.eligibility.indportal.util.IndportalUtil;
import com.getinsured.eligibility.ssap.sep.service.SEPOverrideService;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.indportal.dto.ReverseSepQepDenial;
import com.getinsured.hix.indportal.dto.SEPOverrideDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardApplicantDTO;
import com.getinsured.iex.dto.SsapApplicantDto;
import com.getinsured.iex.indportal.dto.IndExHelper;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.IndividualPortalUtil;
import com.google.gson.Gson;

/**
 * 
 * HIX-60186
 * 
 * Controller class for Ind Portal operations
 * 
 * @author Nikhil Talreja
 *
 */
@Controller
@RequestMapping("/eligibility")
public class IndPortalHelperController {
	
	private static final Logger LOGGER = Logger.getLogger(IndPortalHelperController.class);

	@Autowired
	private BenchmarkPlanService benchmarkPlanService;
	
	@Autowired
	private IndPortalEnrollmentUtility indPortalEnrollmentUtility;
	
	@Autowired
	private IndportalUtil indportalUtil;
	
	@Autowired
	private SEPOverrideService indPortalSSAPService;
	
	@Autowired private Gson platformGson;
	
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private IndividualPortalUtil individualPortalUtil;
	
	
	private static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";
	
	/**
	 * Calls BenchmarkPlanService for getting 
	 * benchmark premium amount from plan management
	 * 
	 * @param indExHelper - DTO containing the inputs
	 * @return IndExHelper
	 * 
	 * @author Nikhil Talreja
	 */
	@RequestMapping(value = "/indportal/getBenchMarkPremiumAmount", method = RequestMethod.POST)
	@ResponseBody
	public IndExHelper getBenchMarkPremiumAmount(@RequestBody IndExHelper indExHelper){
		
		LOGGER.info("Calling benchmark premium service");
		EligibilityBenchMarkServiceResponse elgBenchMarkServiceResponse = null;
		SimpleDateFormat df = new SimpleDateFormat(SHORT_DATE_FORMAT);
		try {
			elgBenchMarkServiceResponse =	benchmarkPlanService.process(indExHelper.getCaseNumber(),
					df.parse(indExHelper.getEffectiveStartDate()));
		} catch (Exception e) {
			LOGGER.error(e);
			return null;
		}
		
		if(null != elgBenchMarkServiceResponse 
				&& elgBenchMarkServiceResponse.getResponseCode().equals(EligibilityBenchMarkServiceResponse.ResponseCode.SUCCESS)){
			float benchmarkPremium = elgBenchMarkServiceResponse.getBenchMarkPremiumAmount().floatValue();
			LOGGER.debug("benchmark premium " + benchmarkPremium);
			indExHelper.setEhbAmount(benchmarkPremium);
			return indExHelper;
		}
		LOGGER.info("benchmark premium service call was not successful");
		return null;
	}
	
	/**
	 * Story HIX-66712
	 * As an Exchange, automate processing and updating the Carrier of the 
	 * change the Elected APTC Amount
	 * 
	 * Sub-task HIX-67346
	 * Create api to accept enrollment id, elected aptc 
	 * 
	 * Makes the API call to change the elected APTC
	 * for the enrollment
	 * 
	 * @param model
	 * @param request
	 * @param aptcUpdate
	 * @return SUCCESS or FAILURE
	 * @author Sunil Sahoo
	 */
	@RequestMapping(value = "/indportal/updateaptc", method = RequestMethod.POST)
	@ResponseBody
	public String updateAptc(@RequestBody AptcUpdate aptcUpdate){
		
		try{
			LOGGER.info("Calling updateAptc service");
			if (null != aptcUpdate) {
				LOGGER.info("Modify updateAptc amount");
				return indPortalEnrollmentUtility.updateAPTC(aptcUpdate);				
			}
		}
		catch(Exception e){
			LOGGER.error("Error occured while trying  to change elected APTC: ",e);
			return EligibilityConstants.FAILURE;
		}
		
		return EligibilityConstants.FAILURE;
		
	}
	
	/**
	 * @param reverseSepQepDenial
	 * @return SUCCESS or FAILURE
	 * @author sahoo_s
	 */
	@RequestMapping(value = "/indportal/reverseSepQepDenial/sendnotice", method = RequestMethod.POST)
	@ResponseBody
	public void sendNoticeOnreverseSepQepDenial(@RequestBody ReverseSepQepDenial reverseSepQepDenial){
		
		try{
			LOGGER.info("Calling Financial & Non Finacial Notice for SEP/QEP denial");
			if (null != reverseSepQepDenial) {
				indPortalEnrollmentUtility.notifyOnSepQepDenied(reverseSepQepDenial);
			}
		}
		catch(Exception e){
			LOGGER.error("Error occured while calling Financial & Non Finacial Notice for SEP/QEP denial: ",e);
		}
	}
	
	/**
	 * @param saveOverrideSEPDetails
	 * @return SUCCESS or FAILURE
	 */
	@RequestMapping(value = "/indportal/saveOverrideSEPDetails", method = RequestMethod.POST)
	@ResponseBody
	public String updateOverrideSEPDetails(@RequestBody SEPOverrideDTO sepOverrideDTO){
		
		String response = EligibilityConstants.FAILURE;
		
		try{
			LOGGER.info("Calling save Override SEP Details service");
			if (null != sepOverrideDTO) {
				response = indPortalSSAPService.saveOverrideSEPDetails(sepOverrideDTO);
			}
		}
		catch(Exception e){
			LOGGER.error("Error occured while trying to save Override SEP Details: ",e);
		}
		
		return response;
	}
	
	@RequestMapping(value = "/indportal/getApplicantsBySsapId/{ssapApplicantionId}", method = RequestMethod.GET)
	public @ResponseBody String getApplicantsBySsapId(@PathVariable Long ssapApplicantionId){
		String applicantsJson = null;
		try{
			List<DashboardApplicantDTO> applicants = indPortalEnrollmentUtility.getApplicantDetails(ssapApplicantionId);
			applicantsJson = platformGson.toJson(applicants);
		}catch(Exception e){
			LOGGER.error("Error occured while fetching applicationts By SsapId: ",e);
		}		
		return applicantsJson;		
	}
	
	@RequestMapping(value = "/indportal/getEligibleApplicantsByCaseNumber/{caseNumber}", method = RequestMethod.GET)
	public @ResponseBody String getEligibleApplicantsBySsapId(@PathVariable String caseNumber){
		String applicantsJson = null;
		try{
			Long ssapApplicationId = ssapApplicationRepository.getApplicationIdByCaseNumber(caseNumber);
			List<SsapApplicantDto> applicants = indportalUtil.getEligibleApplicantDetails(ssapApplicationId);
			applicantsJson = platformGson.toJson(applicants);
		}catch(Exception e){
			LOGGER.error("Error occured while fetching applicationts By SsapId: ",e);
		}		
		return applicantsJson;		
	}
	
	@RequestMapping(value = "/indportal/isInsideOEEnrollment/{coverageYear}", method = RequestMethod.GET)
	@ResponseBody
	public boolean isInsideOEEnrollment(@PathVariable("coverageYear") String coverageYear) {
		boolean result = false;
		if(StringUtils.isNotBlank(coverageYear) && StringUtils.isNumeric(coverageYear)){
			result = individualPortalUtil.isInsideOEEnrollmentWindow(Integer.parseInt(coverageYear));
		}
		return result;				
	}
}
