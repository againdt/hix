import React from 'react';
import { Button } from '@cmsgov/design-system-core';

const AvailabilityButton = props => {
  const submitAvailability = () => {
    console.log('submitting new availability');
    // Call action to submit availability
    let body = {
      available: !props.data.isAvailable
    };
    props.actions.submitAvailability('/hix/broker/available', body);
  };

  return (
    <Button
      className="ds-c-button ds-c-button--small ds-c-button--primary"
      name="brokerDemandAvailabilitySwitch"
      id="brokerConnectAvailabilitySwitch"
      //disabled={!props.data.isAvailable}
      onClick={submitAvailability}
    >
      {props.data.isAvailable || props.availability ? 'ON' : 'OFF'}
    </Button>
  );
};

export default AvailabilityButton;
