{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload":{
  "taxHousehold": [
    {
      "householdMember": [ {
        
          "dateOfBirth": "01/25/1970",
          "name": {
            "firstName": "John",
            "middleName": "D",
            "lastName": "Fox",
            "personNameSuffixText": ""
          },
          "socialSecurityCard": {
            "socialSecurityNumber": "222222209"
          },
          "gender": "M",
        "householdContact": {
          "homeAddress": {
            "state": "MD"
          }
        },
        "insurance": {
          "insurancePolicyEffectiveDate": "05/01/2015",
          "insurancePolicyExpirationDate": "12/31/2014"
        },
        "organization": [
          {
            "organizationCode": "MEDC"
          }
        ]
      }  ]
    }
  ]
}}