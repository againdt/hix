(function (views) {
	views.Plans = Backbone.View.extend({
		events : {
			'click a.selectPlan': 'selectPlan',
			'click a.removePlan': 'removePlan',
			'click a.setBenchmarkPlan': 'setBenchmarkPlan'
		},
		el : '#summary',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'loadAll', 'addOne','selectPlan','removePlan','setBenchmarkPlan');
			var self = this;
			
			
			self.collection.fetch({
				success : function () {
					
					if(self.collection.length==0){
						$('#summary').empty();
						$('#summary').hide();
						$('#summary_err').show();
					}
					self.collection.setSort("indvPremium",'asc'); //setting default sort on indvPremium Asc
					self.collection.pager();
					
					self.loadAll();
				},
				silent:true
			});
						
			self.planModel =options.planModel;
			$('.selectPlan').bind('click', this.selectPlan);
			$('.setBenchmarkPlan').bind('click', this.setBenchmarkPlan);
			
			self.collection.bind('reset', self.addAll);
		},
		addAll : function () {
			var self = this;
			$('#summary').empty();
			$('#employerCost').empty();
			$('#employeeCost').empty();
						
			var maxEmployerCost=0;
			var maxEmployeeCost=0;
			self.collection.each (function(model){
				if(model.get("employerTotalCost") > maxEmployerCost){
					maxEmployerCost = Math.ceil(model.get("employerTotalCost"));
				}
				if(model.get("employeeTotalCost") > maxEmployeeCost){
					maxEmployeeCost = Math.ceil(model.get("employeeTotalCost"));
				}
			});
			
			$('#maxEmployerCost').val(maxEmployerCost+10);
			$('#maxEmployeeCost').val(maxEmployeeCost+10);
			
			self.collection.each (function(model){
				if(model.get("premiumchanged") != "Y"){
					var vIndvPremium = model.get("indvPremium")/model.get("employeeIds").length;
					model.set({ indvPremium : Math.round(vIndvPremium)});
					model.set({premiumchanged : "Y"});
				}
				
				//Creating logo image link
				
				var contextPath = ctx +"/admin/issuer/company/profile/logo/view/"+model.get("issuerId");
				model.set({issuerLogo : contextPath});
			});
			self.collection.each (self.addOne);
		},
		
		loadAll : function () {
			var self = this;
			self.collection.loadIssuers();
			self.collection.each (function(model){
				
				if($('#benchmarkPlan').val() == model.get("planId") && $('#benchmarkPlan').val() !=''){
					$('#'+'plan_'+ model.get("planId")).hide();
					$('#'+'removeplan_'+ model.get("planId")).show();
					self.collection.benchmarkplanId = $('#benchmarkPlan').val();
				}else{
					$('#'+'plan_'+ model.get("planId")).show();
					$('#'+'removeplan_'+ model.get("planId")).hide();
				}	
			});
		},
		
		addOne : function (model) {
			var self = this;
			
			var viewSummary = new Summary({model:model});

			$('#summary').append(viewSummary.render());
			$('#employerCost').append(viewSummary.getEmployerCost());
			$('#employeeCost').append(viewSummary.getEmployeeCost());
			
			//alert(model.get("employerTotalCost")-model.get("employerIndvCost"));
//			drawEmployerBar('Employer_'+model.get("planId"), model.get("employerDependentCost"),model.get("employerIndvCost"),$('#maxEmployerCost').val());
			drawEmployerBar('Employer_'+model.get("planId"), (model.get("employerTotalCost")-model.get("employerIndvCost")),model.get("employerIndvCost"),$('#maxEmployerCost').val());
			drawEmployeeBar('Employee_'+model.get("planId"), model.get("employeeTotalCost"),$('#maxEmployeeCost').val());
			
			/*var vIndvPremium = model.get("indvPremium");
			model.set({ indvPremium : formatCurrency(vIndvPremium)});*/
			
			$('.info').tooltip();
		},
		
		selectPlan : function (e) {
			var self = this;
			if(self.collection.benchmarkplanId!=""){
				$('#'+'plan_'+ self.collection.benchmarkplanId).show();
				$('#'+'removeplan_'+ self.collection.benchmarkplanId).hide();
				self.collection.benchmarkplanId = e.target.id.replace("plan_","");
				$('#'+'plan_'+ self.collection.benchmarkplanId).hide();
				$('#'+'removeplan_'+ self.collection.benchmarkplanId).show();
			}
			else{
				self.collection.benchmarkplanId = e.target.id.replace("plan_","");
				$('#plan_'+self.collection.benchmarkplanId).hide();
				$('#removeplan_'+ self.collection.benchmarkplanId).show();
			}
		},
		removePlan : function (e) {
			var self = this;
			self.collection.benchmarkplanId = "";;
			$('#plan_'+e.target.id.replace("removeplan_","")).show();
			$('#removeplan_'+ e.target.id.replace("removeplan_","")).hide();
		},
		setBenchmarkPlan : function (e) {
			var self=this;
			if(self.collection.benchmarkplanId !="" ){
				$('#missinginfo').modal('show');
				self.planModel.save({id: self.collection.benchmarkplanId},{success:function(){
					$('#missinginfo').modal('hide');
					location.href="finalcontribution";	
				}});
			}
			else{
				alert('Please select a reference plan.');
			}
		}
	});

	var Summary = Backbone.View.extend({
		template: _.template($('#resultItemTemplateSummary').html()),
		template_employer_cost: _.template($('#resultItemTemplateEmployerCost').html()),
		template_employee_cost: _.template($('#resultItemTemplateEmployeeCost').html()),
		
		initialize: function() {
			this.model.bind('change', this.render, this);
			this.model.bind('destroy', this.remove, this);
		},
		
		render : function () {
			return this.template(this.model.toJSON());
		},
		
		getEmployerCost : function () {
			return this.template_employer_cost(this.model.toJSON());
		},
		getEmployeeCost : function () {
			return this.template_employee_cost(this.model.toJSON());
		},
		
		
	});
})(App.views);