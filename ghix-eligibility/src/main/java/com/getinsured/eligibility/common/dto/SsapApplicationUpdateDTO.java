package com.getinsured.eligibility.common.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;

public class SsapApplicationUpdateDTO {
	
	private boolean csrLevelFlag;
	private boolean applicationStatusFlag;
	private boolean ehbAmountFlag;
	private boolean nativeAmericanFlag;
	private boolean eligibilityReceivedDateFlag;
	private boolean allowEnrollmentFlag;
	private boolean exchangeEligibilityStatusFlag;
	private boolean eligibilityStatusFlag;

	private String csrLevel;
	private String applicationStatus;
	private BigDecimal ehbAmount;
	private String nativeAmerican;
	private Date eligibilityReceivedDate;
	private String allowEnrollment;
	private ExchangeEligibilityStatus exchangeEligibilityStatus;
	private EligibilityStatus eligibilityStatus;
	
	public boolean isCsrLevelFlag() {
		return csrLevelFlag;
	}
	public void setCsrLevelFlag(boolean csrLevelFlag) {
		this.csrLevelFlag = csrLevelFlag;
	}
	public boolean isEhbAmountFlag() {
		return ehbAmountFlag;
	}
	public void setEhbAmountFlag(boolean ehbAmountFlag) {
		this.ehbAmountFlag = ehbAmountFlag;
	}
	public boolean isNativeAmericanFlag() {
		return nativeAmericanFlag;
	}
	public void setNativeAmericanFlag(boolean nativeAmericanFlag) {
		this.nativeAmericanFlag = nativeAmericanFlag;
	}
	public boolean isEligibilityReceivedDateFlag() {
		return eligibilityReceivedDateFlag;
	}
	public void setEligibilityReceivedDateFlag(boolean eligibilityReceivedDateFlag) {
		this.eligibilityReceivedDateFlag = eligibilityReceivedDateFlag;
	}
	public boolean isAllowEnrollmentFlag() {
		return allowEnrollmentFlag;
	}
	public void setAllowEnrollmentFlag(boolean allowEnrollmentFlag) {
		this.allowEnrollmentFlag = allowEnrollmentFlag;
	}
	public boolean isExchangeEligibilityStatusFlag() {
		return exchangeEligibilityStatusFlag;
	}
	public void setExchangeEligibilityStatusFlag(
			boolean exchangeEligibilityStatusFlag) {
		this.exchangeEligibilityStatusFlag = exchangeEligibilityStatusFlag;
	}
	public boolean isEligibilityStatusFlag() {
		return eligibilityStatusFlag;
	}
	public void setEligibilityStatusFlag(boolean eligibilityStatusFlag) {
		this.eligibilityStatusFlag = eligibilityStatusFlag;
	}
	public String getCsrLevel() {
		return csrLevel;
	}
	public boolean isApplicationStatusFlag() {
		return applicationStatusFlag;
	}
	public void setApplicationStatusFlag(boolean applicationStatusFlag) {
		this.applicationStatusFlag = applicationStatusFlag;
	}
	public String getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatusFlag = true;
		this.applicationStatus = applicationStatus;
	}
	public void setCsrLevel(String csrLevel) {
		this.csrLevelFlag = true;
		this.csrLevel = csrLevel;
	}
	public BigDecimal getEhbAmount() {
		return ehbAmount;
	}
	public void setEhbAmount(BigDecimal ehbAmount) {
		this.ehbAmountFlag = true;
		this.ehbAmount = ehbAmount;
	}
	public String getNativeAmerican() {
		return nativeAmerican;
	}
	public void setNativeAmerican(String nativeAmerican) {
		this.nativeAmericanFlag = true;
		this.nativeAmerican = nativeAmerican;
	}
	public Date getEligibilityReceivedDate() {
		return eligibilityReceivedDate;
	}
	public void setEligibilityReceivedDate(Date eligibilityReceivedDate) {
		this.eligibilityReceivedDateFlag = true;
		this.eligibilityReceivedDate = eligibilityReceivedDate;
	}
	public String getAllowEnrollment() {
		return allowEnrollment;
	}
	public void setAllowEnrollment(String allowEnrollment) {
		this.allowEnrollmentFlag = true;
		this.allowEnrollment = allowEnrollment;
	}
	public ExchangeEligibilityStatus getExchangeEligibilityStatus() {
		return exchangeEligibilityStatus;
	}
	public void setExchangeEligibilityStatus(ExchangeEligibilityStatus exchangeEligibilityStatus) {
		this.exchangeEligibilityStatusFlag = true;
		this.exchangeEligibilityStatus = exchangeEligibilityStatus;
	}
	public EligibilityStatus getEligibilityStatus() {
		return eligibilityStatus;
	}
	public void setEligibilityStatus(EligibilityStatus eligibilityStatus) {
		this.eligibilityStatusFlag = true;
		this.eligibilityStatus = eligibilityStatus;
	}
	
	public boolean isUpdated() {
		return 	csrLevelFlag || applicationStatusFlag || ehbAmountFlag || nativeAmericanFlag || eligibilityReceivedDateFlag || allowEnrollmentFlag || exchangeEligibilityStatusFlag || eligibilityStatusFlag;
	}
}
