package com.getinsured.eligibility.redetermination.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicantService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.IEligibilityProgram;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.iex.lce.ChangedApplicant;
import com.getinsured.iex.lce.RequestParamDTO;
import com.getinsured.iex.lce.SepRequestParamDTO;
import com.getinsured.iex.lce.SepResponseParamDTO;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.LifeChangeEventUtil;
import com.getinsured.iex.util.SsapUtil;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Service
public class SsapCloneApplicantServiceImpl implements
	SsapCloneApplicantService {
	
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	@Autowired
	private SsapUtil ssapUtil;
	@Autowired
	private ILocationRepository iLocationRepository;
	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	@Autowired
	private IUserRepository userRepository;
	@Autowired
	private LifeChangeEventUtil lifeChangeEventUtil;
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	@Autowired
	private IEligibilityProgram iEligibilityProgram;
	
	private final String SSAPAPPLICANT_GUID_KEY = "SSAP_APPLICANT_DISPLAY_ID";
	private static final Logger LOGGER = LoggerFactory.getLogger(SsapCloneApplicantServiceImpl.class);

	@Override
	public List<SsapApplicant> cloneSsapApplicants(SsapApplication currentApplication, List<RequestParamDTO> events,
			Long userId, SepResponseParamDTO sepResponseParamDTO, SsapApplication parentApplication,
			SepRequestParamDTO sepRequestParamDTO, AtomicBoolean isCloneProgramEligibility) throws Exception {
		
		String personId;
		Map<String, String> applicantsGuid = new HashMap<String, String>();
		List<SsapApplicant> ssapApplicants = new ArrayList<SsapApplicant>();
		
		List<SsapApplicant> applicantsBeforeClone = ssapApplicantRepository.findBySsapApplicationId(currentApplication.getParentSsapApplicationId().longValue());
		Map<Long, SsapApplicant> applicantsBeforeCloneMap = getApplicantsMap(applicantsBeforeClone);
		
		//update not eligible applicant to Not Seeking, if parent application is financial.
		updateSeekingCoverageForNotEligible(parentApplication, currentApplication,applicantsBeforeCloneMap);
				
		JsonArray householdMembers = LifeChangeEventUtil.getHouseholdMembers(currentApplication.getApplicationData());
		Map<Long, List<RequestParamDTO>> changedApplicantsWithEvents = this.getChangedApplicantsWithEvents(events, householdMembers);
		Iterator<JsonElement> iterator = householdMembers.iterator();
		
		JsonObject primaryHouseholdMember = ssapUtil.getBloodRelationship(householdMembers);
		
		while (iterator.hasNext()) {
			JsonObject householdMember = iterator.next().getAsJsonObject();
			personId = householdMember.get(LifeChangeEventConstant.PERSON_ID).getAsString();
			
			SsapApplicant ssapApplicant = cloneSsapApplicant(new Long(personId), householdMember, changedApplicantsWithEvents, applicantsBeforeCloneMap, currentApplication, userId, sepResponseParamDTO, sepRequestParamDTO, isCloneProgramEligibility);
			ssapApplicant = updateSsapApplicant(new Long(personId), householdMember, householdMembers, ssapApplicant, changedApplicantsWithEvents);
			if(ssapApplicant != null){
				applicantsGuid.put(personId, ssapApplicant.getApplicantGuid());
			}
			ssapApplicant.setRelationship(ssapUtil.getApplicantRelation(primaryHouseholdMember, personId));
			ssapApplicants.add(ssapApplicant);
		}
		updateApplicantsGuid(currentApplication, applicantsGuid);

		return ssapApplicants;
	}

	public SsapApplicant cloneSsapApplicant(Long personId, JsonObject householdMember,
			Map<Long, List<RequestParamDTO>> changedApplicantsWithEvents,
			Map<Long, SsapApplicant> applicantsBeforeCloneMap, SsapApplication currentApplication, Long userId,
			SepResponseParamDTO sepResponseParamDTO, SepRequestParamDTO sepRequestParamDTO,
			AtomicBoolean isCloneProgramEligibility) throws Exception {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		SsapApplicant ssapApplicant;
		String onApplication = LifeChangeEventConstant.YES;
		String tobaccoUser;
		String applyingForCoverage;
		String dateOfBirth = null;
		String marriedIndicator = LifeChangeEventConstant.FALSE;
		JsonObject householdContact;
		String householdContactIndicator;
		long applicantId = 0;
		boolean isApplicantBeforeClone = false;
		
		if(applicantsBeforeCloneMap.containsKey(personId)){
			ssapApplicant = applicantsBeforeCloneMap.get(personId);
			applicantId = ssapApplicant.getId();
			isApplicantBeforeClone = true;
		} else {
			ssapApplicant = new SsapApplicant();
		}
		ssapApplicant.setId(0);
		ssapApplicant.setPersonId(personId);
		ssapApplicant.setOnApplication(onApplication);
		ssapApplicant.setSsapApplication(currentApplication);
		ssapApplicant.setCreatedBy(userId.intValue());
		ssapApplicant.setSsapApplicantEvents(null);
		ssapApplicant.setSsapVerifications(null);

		if (sepRequestParamDTO.getIsRenewalFlow()== null || sepRequestParamDTO.getIsRenewalFlow())
		{
			ssapApplicant.setCreationTimestamp(new Timestamp(new TSDate().getTime()));
			ssapApplicant.setFirstName(LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().get(LifeChangeEventConstant.FIRST_NAME)));
			ssapApplicant.setMiddleName(LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().get(LifeChangeEventConstant.MIDDLE_NAME)));
			ssapApplicant.setLastName(LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().get(LifeChangeEventConstant.LAST_NAME)));
			ssapApplicant.setNameSuffix(LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.NAME).getAsJsonObject().get(LifeChangeEventConstant.SUFFIX)));
			ssapApplicant.setGender(LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.GENDER)));
			ssapApplicant.setStatus("OE_RENEWED");		
			if ("CA".equalsIgnoreCase(stateCode)) {
				marriedIndicator = "R";
				if(LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.MARRIED_INDICATOR_CODE)) != null){
					marriedIndicator =  LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.MARRIED_INDICATOR_CODE));
				}
			}
			else if(LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.MARRIED_INDICATOR)) != null){
				marriedIndicator =  LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.MARRIED_INDICATOR)).equals("true")?"true":"false";
			}
			ssapApplicant.setMarried(marriedIndicator);
			
			dateOfBirth = LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.DATE_OF_BIRTH));
			
			if(!LifeChangeEventUtil.isEmpty(dateOfBirth)){
				ssapApplicant.setBirthDate(LifeChangeEventUtil.parseDate(dateOfBirth));
			}
			
			tobaccoUser = LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.TOBACCO_USER_INDICATOR));
	
			if("CA".equalsIgnoreCase(stateCode)){
				ssapApplicant.setTobaccouser(null);
			}
			else if(!isApplicantBeforeClone && !LifeChangeEventUtil.isEmpty(tobaccoUser)){
				ssapApplicant.setTobaccouser(tobaccoUser.equals(LifeChangeEventConstant.TRUE) ? LifeChangeEventConstant.YES : LifeChangeEventConstant.NO);
			}
			
			applyingForCoverage =  LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.APPLYING_FOR_COVERAGE_INDICATOR));
			
			if(!LifeChangeEventUtil.isEmpty(applyingForCoverage)){
				ssapApplicant.setApplyingForCoverage(applyingForCoverage.equals(LifeChangeEventConstant.TRUE) ? LifeChangeEventConstant.YES : LifeChangeEventConstant.NO);
			}
	
			householdContactIndicator =  LifeChangeEventUtil.getJsonAsString(householdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT_INDICATOR));
			if(!LifeChangeEventUtil.isEmpty(householdContactIndicator)){
				ssapApplicant.setHouseholdContactFlag(householdContactIndicator.equals(LifeChangeEventConstant.TRUE) ? LifeChangeEventConstant.YES : LifeChangeEventConstant.NO);
			}
			
			if(LifeChangeEventUtil.isEmpty(ssapApplicant.getApplicantGuid())){
				ssapApplicant.setApplicantGuid(ssapUtil.getNextSequenceFromDB(SSAPAPPLICANT_GUID_KEY));
			}
			
			JsonObject specialCircumstances = householdMember.get(LifeChangeEventConstant.SPECIAL_CIRCUMSTANCES).getAsJsonObject();
			if( specialCircumstances != null && !specialCircumstances.isJsonNull()
					&& specialCircumstances.get(LifeChangeEventConstant.AMERICAN_INDIAN_ALASKA_NATIVE_INDICATOR) != null && !specialCircumstances.get(LifeChangeEventConstant.AMERICAN_INDIAN_ALASKA_NATIVE_INDICATOR).isJsonNull()){
				ssapApplicant.setNativeAmericanFlag(specialCircumstances.get(LifeChangeEventConstant.AMERICAN_INDIAN_ALASKA_NATIVE_INDICATOR).getAsBoolean()?"Y":"N");
			}
			
			JsonObject socialSecurityCard = householdMember.get(LifeChangeEventConstant.SOCIAL_SECURITY_CARD).getAsJsonObject();
			
			householdContact = householdMember.get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
			
			if(socialSecurityCard.get(LifeChangeEventConstant.SOCIAL_SECURITY_NUMBER) != null && !socialSecurityCard.get(LifeChangeEventConstant.SOCIAL_SECURITY_NUMBER).isJsonNull()){
				ssapApplicant.setSsn(socialSecurityCard.get(LifeChangeEventConstant.SOCIAL_SECURITY_NUMBER).getAsString());
			}
		}
		//Setting Verification Flags for these rules : Death, Tribe Status, SSN, Citizenship and Incarceration
		List<RequestParamDTO> changedEvents = changedApplicantsWithEvents.get(personId);
		if(changedEvents!=null){
			setVerificationFlagsAndOtherData(applicantsBeforeCloneMap.get(personId), ssapApplicant);
		}

		if (null != isCloneProgramEligibility) {
			ssapApplicant.setEligibilityStatus(applicantsBeforeCloneMap.get(personId).getEligibilityStatus());
			cloneEligibilityProgram(applicantId, ssapApplicant, isCloneProgramEligibility);
		}
		else {
			ssapApplicant.setEligibilityProgram(null);
		}

		if(LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID==personId && checkEventName(LifeChangeEventConstant.DEMOGRAPHICS_NAME_CHANGE, changedApplicantsWithEvents.get(personId))){
			updateNameInCmrHouseholdAndUserTable(currentApplication, householdMember, sepResponseParamDTO);
		}
		
		
		return ssapApplicant;
	
	}
	
	@Override
	public Map<Long, List<RequestParamDTO>> getChangedApplicantsWithEvents(
			List<RequestParamDTO> events, JsonArray householdMembers)
			throws Exception {
		Map<Long, List<RequestParamDTO>> setOfModifiedApplicants = new  HashMap<>();
		List<RequestParamDTO> personEvents = null;
		
		for(RequestParamDTO requestParamDTO:events){
			List<ChangedApplicant> changedApplicants = requestParamDTO.getChangedApplicants();
			if(changedApplicants != null){
				for (ChangedApplicant changedApplicant : changedApplicants) {
					if(setOfModifiedApplicants.get(changedApplicant.getPersonId())!=null){
						personEvents = setOfModifiedApplicants.get(changedApplicant.getPersonId());
						personEvents.add(requestParamDTO);
					} else {
						personEvents = new ArrayList<>();
						personEvents.add(requestParamDTO);
					}
					setOfModifiedApplicants.put(changedApplicant.getPersonId(), personEvents);
				}
			}
		}
		
		return setOfModifiedApplicants;
	}
	
	@Override
	public SsapApplicant saveSsapApplicant(SsapApplicant ssapApplicant) {
		return ssapApplicantRepository.saveAndFlush(ssapApplicant);
	}
	
	private Map<Long, SsapApplicant> getApplicantsMap(
			List<SsapApplicant> applicantsBeforeClone) {
		Map<Long, SsapApplicant> applicantsBeforeCloneMap = new HashMap<Long, SsapApplicant>();
		for (SsapApplicant ssapApplicant : applicantsBeforeClone) {
			applicantsBeforeCloneMap.put(ssapApplicant.getPersonId(), ssapApplicant);
		}
		return applicantsBeforeCloneMap;
	}

	
	private SsapApplicant updateSsapApplicant(Long personId,
			JsonObject householdMember, JsonArray householdMembers,
			SsapApplicant ssapApplicant, Map<Long, List<RequestParamDTO>> events) {
		JsonObject primaryHouseholdContact;
		
		primaryHouseholdContact = householdMembers.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
		JsonObject otherPhone = primaryHouseholdContact.get(LifeChangeEventConstant.OTHER_PHONE).getAsJsonObject();
		if(otherPhone!=null && !otherPhone.isJsonNull() && otherPhone.get(LifeChangeEventConstant.PHONE_NUMBER)!=null && !otherPhone.get(LifeChangeEventConstant.PHONE_NUMBER).isJsonNull()){
			ssapApplicant.setPhoneNumber(otherPhone.get(LifeChangeEventConstant.PHONE_NUMBER).getAsString());
		}
		
		JsonObject phone = primaryHouseholdContact.get(LifeChangeEventConstant.PHONE).getAsJsonObject();
		if(phone!=null && !phone.isJsonNull() && phone.get(LifeChangeEventConstant.PHONE_NUMBER)!=null && !phone.get(LifeChangeEventConstant.PHONE_NUMBER).isJsonNull()){
			ssapApplicant.setMobilePhoneNumber(phone.get(LifeChangeEventConstant.PHONE_NUMBER).getAsString());
		}
		
		JsonObject contactPreferences = primaryHouseholdContact.get(LifeChangeEventConstant.CONTACT_PREFERENCES).getAsJsonObject();
		if(contactPreferences!=null && !contactPreferences.isJsonNull() && contactPreferences.get(LifeChangeEventConstant.EMAIL_ADDRESS)!=null && !contactPreferences.get(LifeChangeEventConstant.EMAIL_ADDRESS).isJsonNull()){
			ssapApplicant.setEmailAddress(contactPreferences.get(LifeChangeEventConstant.EMAIL_ADDRESS).getAsString());
		}
		updateHomeAddress(personId, householdMember, householdMembers, ssapApplicant);		
		updateMailingAddress(personId, householdMember, householdMembers, ssapApplicant);
		
		ssapApplicant.setEligibilityStatus(ExchangeEligibilityStatus.NONE.name());

		return ssapApplicant;
	}
	
	private void updateHomeAddress(Long personId, JsonObject householdMember, JsonArray householdMembers, SsapApplicant ssapApplicant){
		Location primaryLocation = new Location();
		JsonObject primaryHouseholdContact = householdMembers.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
		JsonObject homeAddress = primaryHouseholdContact.get(LifeChangeEventConstant.HOME_ADDRESS).getAsJsonObject();
		setAddress(primaryLocation, homeAddress);
		iLocationRepository.save(primaryLocation);
		ssapApplicant.setOtherLocationId(new BigDecimal(primaryLocation.getId()));
	}
	
	private void updateMailingAddress(Long personId, JsonObject householdMember, JsonArray householdMembers, SsapApplicant ssapApplicant){
		Location mailingLocation = new Location();
		JsonObject primaryHouseholdContact;
		
		primaryHouseholdContact = householdMembers.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_CONTACT).getAsJsonObject();
		JsonObject mailingAddress = primaryHouseholdContact.get(LifeChangeEventConstant.MAILING_ADDRESS).getAsJsonObject();
		setAddress(mailingLocation, mailingAddress);
		mailingLocation = iLocationRepository.save(mailingLocation);
		ssapApplicant.setMailiingLocationId(new BigDecimal(mailingLocation.getId()));
	}
	
	private void setAddress(Location location, JsonObject address){
		location.setAddress1(LifeChangeEventUtil.getJsonAsString(address.get(LifeChangeEventConstant.STREET_ADDRESS_1)));
		location.setAddress2(LifeChangeEventUtil.getJsonAsString(address.get(LifeChangeEventConstant.STREET_ADDRESS_2)));
		location.setCity(LifeChangeEventUtil.getJsonAsString(address.get(LifeChangeEventConstant.CITY)));
		location.setState(LifeChangeEventUtil.getJsonAsString(address.get(LifeChangeEventConstant.STATE)));
		location.setZip(LifeChangeEventUtil.getJsonAsString(address.get(LifeChangeEventConstant.POSTAL_CODE)));
		location.setCounty(LifeChangeEventUtil.getJsonAsString(address.get(LifeChangeEventConstant.COUNTY)));
	}
	
	private boolean checkEventName(String eventName, List<RequestParamDTO> events){
		if(events!=null){
			for(RequestParamDTO param:events){
				String name = param.getEventSubCategory();
				if(eventName.equals(name)){
					return true;
				}
			}
		}
		
		return false;
	}
	
	private void setVerificationFlagsAndOtherData(SsapApplicant previousData, SsapApplicant ssapApplicant){
		
		ssapApplicant.setDeathStatus(null);
		ssapApplicant.setDeathVerificationVid(null);

		ssapApplicant.setResidencyStatus(previousData.getResidencyStatus());
		ssapApplicant.setResidencyVid(previousData.getResidencyVid());
		
		ssapApplicant.setSsnVerificationStatus(previousData.getSsnVerificationStatus());
		ssapApplicant.setSsnVerificationVid(previousData.getSsnVerificationVid());
			
		ssapApplicant.setCitizenshipImmigrationStatus(previousData.getCitizenshipImmigrationStatus());
		ssapApplicant.setCitizenshipImmigrationVid(previousData.getCitizenshipImmigrationVid());
		ssapApplicant.setVlpVerificationVid(previousData.getVlpVerificationVid());
		ssapApplicant.setVlpVerificationStatus(previousData.getVlpVerificationStatus());
		
		ssapApplicant.setIncarcerationStatus(previousData.getIncarcerationStatus());
		ssapApplicant.setIncarcerationVid(previousData.getIncarcerationVid());			

		ssapApplicant.setHardshipExempt(previousData.getHardshipExempt());
		ssapApplicant.setCsrLevel(previousData.getCsrLevel());
		ssapApplicant.setFullTimeStudent(previousData.getFullTimeStudent());
		ssapApplicant.setPreferredTimeToContact(previousData.getPreferredTimeToContact());
		ssapApplicant.setNativeAmericanFlag(previousData.getNativeAmericanFlag());
		ssapApplicant.setApplicantGuid(previousData.getApplicantGuid());
		ssapApplicant.setIsEnrldCessPrgrm(previousData.getIsEnrldCessPrgrm());
		ssapApplicant.setMaritialStatusId(previousData.getMaritialStatusId());
		ssapApplicant.setNativeAmericanFlag(previousData.getNativeAmericanFlag());
		ssapApplicant.setMobilePhoneNumber(previousData.getMobilePhoneNumber());
		ssapApplicant.setEcnNumber(previousData.getEcnNumber());
		ssapApplicant.setHardshipExempt(previousData.getHardshipExempt());
		ssapApplicant.setMecSource(previousData.getMecSource());
		ssapApplicant.setStatus(previousData.getStatus());
		ssapApplicant.setOnApplication(previousData.getOnApplication());
		ssapApplicant.setExternalApplicantId(previousData.getExternalApplicantId());
	}

	/**
	 * Method is used to clone Eligibility Program.
	 */
	private void cloneEligibilityProgram(Long applicantId, SsapApplicant ssapApplicant, AtomicBoolean isCloneProgramEligibility) {

		List<EligibilityProgram> eligibilityProgramList = null;

		List<EligibilityProgram> previousEligibilityProgram = iEligibilityProgram.getApplicantEligibilities(applicantId);

		if (isCloneProgramEligibility.get() && CollectionUtils.isNotEmpty(previousEligibilityProgram)) {
			eligibilityProgramList = new ArrayList<>();
			EligibilityProgram eligibilityProgram = null;

			for (EligibilityProgram prevEligibilityProgram : previousEligibilityProgram) {

				eligibilityProgram = new EligibilityProgram();
				eligibilityProgram.setEligibilityType(prevEligibilityProgram.getEligibilityType());
				eligibilityProgram.setEligibilityIndicator(prevEligibilityProgram.getEligibilityIndicator());
				eligibilityProgram.setEligibilityStartDate(prevEligibilityProgram.getEligibilityStartDate());
				eligibilityProgram.setEligibilityEndDate(prevEligibilityProgram.getEligibilityEndDate());
				eligibilityProgram.setEligibilityDeterminationDate(prevEligibilityProgram.getEligibilityDeterminationDate());
				eligibilityProgram.setIneligibleReason(prevEligibilityProgram.getIneligibleReason());
				eligibilityProgram.setEstablishingCategoryCode(prevEligibilityProgram.getEstablishingCategoryCode());
				eligibilityProgram.setEstablishingStateCode(prevEligibilityProgram.getEstablishingStateCode());
				eligibilityProgram.setEstablishingCountyName(prevEligibilityProgram.getEstablishingCountyName());
				eligibilityProgram.setEligStartDateOverride(prevEligibilityProgram.getEligStartDateOverride());
				eligibilityProgram.setSsapApplicant(ssapApplicant);
				eligibilityProgramList.add(eligibilityProgram);
			}
		}
		// Add List of EligibilityProgram in New SSAP Applicant
		ssapApplicant.setEligibilityProgram(eligibilityProgramList);
	}

	private SsapApplication updateApplicantsGuid(SsapApplication ssapApplication, Map<String, String> applicantsGuid) throws Exception{
		ssapApplication.setApplicationData(LifeChangeEventUtil.setApplicantsGuid(ssapApplication.getApplicationData(), applicantsGuid));
		return ssapApplication;
	}
	
	private void updateNameInCmrHouseholdAndUserTable(SsapApplication currentApplication, JsonObject householdMember, SepResponseParamDTO sepResponseParamDTO) throws Exception{
		try {
			Household household = cmrHouseholdRepository.findOne(currentApplication.getCmrHouseoldId().intValue());
			//Updating Household Object from Json
			household = lifeChangeEventUtil.setNameInCmrHousehold(householdMember, household);
			cmrHouseholdRepository.saveAndFlush(household);
			if(household.getUser()!=null){
				userRepository.saveAndFlush(household.getUser()); 
				sepResponseParamDTO.setIsNameChangedForPrimary(true);
				sepResponseParamDTO.setPrimaryFirstName(household.getFirstName());
				sepResponseParamDTO.setPrimaryLastName(household.getLastName());
				sepResponseParamDTO.setPrimaryMiddleName(household.getMiddleName());
			}
		} catch(Exception exception){
			throw exception;
		}
	}
	
	private void updateSeekingCoverageForNotEligible(SsapApplication parentApplication,SsapApplication currentApplication, Map<Long, SsapApplicant> applicantsBeforeCloneMap) {
		if(parentApplication.getFinancialAssistanceFlag() != null && "Y".equals(parentApplication.getFinancialAssistanceFlag())){
			
			SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
			
			for(TaxHousehold taxHousehold : singleStreamlinedApplication.getTaxHousehold()) {
				for(HouseholdMember houseHoldMember : taxHousehold.getHouseholdMember()) {
					SsapApplicant ssapApplicant = applicantsBeforeCloneMap.get(new Long(houseHoldMember.getPersonId()));
					if(ssapApplicant != null){
						if(ssapApplicant.getEligibilityStatus() != null && "NONE".equalsIgnoreCase(ssapApplicant.getEligibilityStatus())
							&& "Y".equalsIgnoreCase(ssapApplicant.getApplyingForCoverage())){
							houseHoldMember.setApplyingForCoverageIndicator(false);
						}
					}
							
				}
			}
			currentApplication.setApplicationData(ssapJsonBuilder.transformToJson(singleStreamlinedApplication));
		}
	}
}
