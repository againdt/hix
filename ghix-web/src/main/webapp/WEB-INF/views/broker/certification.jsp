<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<!--src="<c:url value="/resources/js/employer_location.js" />"  -->

	<!-- start of secondary navbar - TO DO: Move to .topnav.jsp -->
<!-- 	<ul class="nav nav-list"> -->
<!-- 		<li class="active"><a href="#">Home</a></li> -->
<!-- 		<li><a href="#">Find Insurance</a></li> -->
<!-- 		<li><a href="#">Learn More</a></li>    -->
<!-- 		<li><a href="#">Get Assistance</a></li>    -->
<!-- 	</ul> -->
	<!-- end of secondary navbar -->
<div class="gutter10">
	<div class="row-fluid">
		<div class="span3" id="sidebar">
		</div><!-- end of span3 -->
		<div class="span9" id="rightpanel">
			<form class="form-stacked" id="frmbrokerCertification" name="frmbrokerCertification" action="Certification" method="POST">
				<fieldset>
					<legend>Agent Certification</legend>
					<div class="page-header">
						<h1> Certification Status: Not Certified</h1>
						<p> To be certified by Getinsured Health Insurance Exchange, please provide the following documents in the formats requested.</p>
					</div><!-- end of page-header -->

					<div class="gutter10">

						<div class="control-group">
							<label class="control-label" for="fileInput">1. Presentation of valid Health & Life Individual or Agency License issued by state </label>
							<div class="controls">
								<input class="input-file" id="fileInput" type="file">
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="fileInput">2. Presentation of valid Health & Life Individual or Agency License issued by state </label>
							<div class="controls">
								<input class="input-file" id="fileInput" type="file">
							</div>
						</div>

					</div><!--  end of .gutter10  -->

					<div class="form-actions">
						<input type="submit" name="submit" id="submit" value="<spring:message  code='label.brkContinue'/>"  class="btn btn-primary" title="<spring:message  code='label.brkContinue'/>"/>
					</div>

				</fieldset>

			</form>

		</div><!-- end of span9 -->
		<!-- broker registration -->
</div>
		<div class="notes" style="display: none">
			<div class="row">
				<div class="span">
					<p>This information is required to determine initial eligibility to use the SHOP Exchange. All fields are subjected to a basic format validation of the input (e.g. a zip code must be a 5 digit number). The question mark icon which opens a light-box provides helpful information. The EIN number can be validated with an external data source if an adequate web API is available.</p>
				</div>
			</div>
		</div>

	</div><!--  end of row-fluid -->
