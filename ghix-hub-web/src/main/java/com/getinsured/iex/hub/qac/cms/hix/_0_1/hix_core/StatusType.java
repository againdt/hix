//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.03.29 at 06:16:44 PM PDT 
//


package com.getinsured.iex.hub.qac.cms.hix._0_1.hix_core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.qac.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.iex.hub.qac.niem.niem.proxy.xsd._2.Boolean;



/**
 * A data type for a state of something or someone.
 * 
 * <p>Java class for StatusType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StatusType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/niem-core/2.0}StatusType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}StatusValidDateRange" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}StatusIndicator"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusType", propOrder = {
    "statusValidDateRange",
    "statusIndicator"
})
public class StatusType
    extends com.getinsured.iex.hub.qac.niem.niem.niem_core._2.StatusType
{

    @XmlElement(name = "StatusValidDateRange")
    protected DateRangeType statusValidDateRange;
    @XmlElement(name = "StatusIndicator", required = true)
    protected Boolean statusIndicator;

    /**
     * Gets the value of the statusValidDateRange property.
     * 
     * @return
     *     possible object is
     *     {@link DateRangeType }
     *     
     */
    public DateRangeType getStatusValidDateRange() {
        return statusValidDateRange;
    }

    /**
     * Sets the value of the statusValidDateRange property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateRangeType }
     *     
     */
    public void setStatusValidDateRange(DateRangeType value) {
        this.statusValidDateRange = value;
    }

    /**
     * Gets the value of the statusIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getStatusIndicator() {
        return statusIndicator;
    }

    /**
     * Sets the value of the statusIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStatusIndicator(Boolean value) {
        this.statusIndicator = value;
    }

}
