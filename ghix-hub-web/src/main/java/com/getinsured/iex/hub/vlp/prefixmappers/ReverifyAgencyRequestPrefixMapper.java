package com.getinsured.iex.hub.vlp.prefixmappers;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class ReverifyAgencyRequestPrefixMapper extends NamespacePrefixMapper{
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		//System.out.println("Mapper URI:"+namespaceUri+" mapped to:vil");
		return "vil";
	}
}
