<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" ></script>
	<div class="gutter10">
	<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="label.manageRepresentatives"/></li>
        </ul><!--page-breadcrumb ends-->
	</div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
			</div>
			<div class="span9">
				<h1 id="skip">${issuerObj.name}</h1>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
					<div class="header">
						 <h4><spring:message  code="pgheader.issuerAbout"/></h4>
					</div>
					<jsp:include page="issuerDetailsLeftNav.jsp">
        					<jsp:param name="pageName" value="issuerRep"/>
					</jsp:include>
			</div>
			<!-- end of span3 -->
            <div class="span9" id="rightpanel">
                <form class="form-horizontal" id="frmIssuerAddRep" name="frmIssuerAddRep" action="<c:url value="/admin/issuer/representative/save" />" method="POST">
                	 <input type="hidden" id="issuer.id" name="issuer.id" value="<encryptor:enc value="${issuerObj.id}"></encryptor:enc>"/>
                    	<df:csrfToken/>
                        <div class="header">
                            <h4 class="pull-left"><spring:message  code="pgheader.addRepresentative"/></h4>
							<input type="submit" name="IssuerAddRepSubmitBtn" id="IssuerAddRepSubmitBtn" class="btn btn-primary btn-small pull-right margin5-lr" value="<spring:message  code="label.save"/>" title="<spring:message  code="label.save"/>">
                            <a href='<c:url value="/admin/issuer/representative/manage/${encIssuerId}"/>' class="btn btn-small pull-right"><spring:message  code="label.cancel"/></a>
                        </div>
                        <div class="gutter10">
        
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.firstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                    <div class="controls">
                                      <input type="text" name="firstName" id="firstName" value="${repUser.firstName}" class="input-large" maxlength="50">
                                        <c:if test="${isIssuerRepDuplicate == 'true'}">
                                        <div class="error">
                                               <label class="error"><span><spring:message  code="err.duplicateRepresentative"/></span></label>
                                        </div> 
                                        </c:if>
                                      <div id="firstName_error" class=""></div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.lastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                    <div class="controls"><input type="text" name="lastName" id="lastName" value="${repUser.lastName}" class="input-large" maxlength="50">
                                        <div id="lastName_error" class=""></div>
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.title"/> </label>
                                    <div class="controls"><input type="text" name="title" id="title" value="${repUser.title}" class="input-large" maxlength="15">	
                                        <div id="title_error" class=""></div>
                                    </div>
                                </div>
                                
                                 <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.phoneNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                    <div class="controls">
			                            <input type="text" name="phone1" id="phone1" value="${phone1}" maxlength="3" class="input-mini" /> 
										<input type="text" name="phone2" id="phone2" value="${phone2}" maxlength="3" class="input-mini" /> 
										<input type="text" name="phone3" id="phone3" value="${phone3}" maxlength="4" class="input-small" /> 
			                            <input type="hidden" name="phone" id="phone" class="input-large">
			                            <small style="font-size: 11px;">(Phone must be enabled to receive text messages)</small>
			                            <div id="" ></div><div id="" ></div><div id="phone3_error" ></div>
			                        </div> 
                                </div>
                                <div class="control-group">
                                    <label class="control-label"><spring:message  code="label.emailAddress"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                    <div class="controls"><input type="text" name="email" id="email" value="${repUser.email}" class="input-large" maxlength="50">
                                        <div id="email_error" class=""></div>
                                         <c:if test="${repDuplicateEmail == 'true'}">
                                        <div class="error">
                                               <label class="error"><span><spring:message  code="err.duplicateEmail"/></span></label>
                                        </div> 
                                        </c:if>
                                    </div>
                                </div>
                                
                                <c:if test="${IS_EMAIL_ACTIVATION != null && IS_EMAIL_ACTIVATION == 'FALSE' && STATE_CODE != null && STATE_CODE != 'CA'}">	                
                                
	                                <div class="control-group">
	                                    <label class="control-label"><spring:message  code="label.password"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                                    <div class="controls"><input type="password" name="password" id="password" class="input-large">
	                                        <div id="password_error" class=""></div>
	                                    </div>
	                                </div>
	                                
	                                <div class="control-group">
	                                    <label class="control-label"><spring:message  code="label.confirmPassword"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                                    <div class="controls"><input type="password" name="confirmPassword" id="confirmPassword" class="input-large">
	                                        <div id="confirmPassword_error" class=""></div>
	                                    </div>
	                                </div>
                                
                                </c:if>
                               
                               <c:if test="${STATE_CODE != null && STATE_CODE == 'CA'}">

									<div class="control-group">
				                        	<label for="addressLine1" class="required control-label"><spring:message  code="label.streetaddress1"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				                            <div class="controls">
				                                <input type="text" name="location.address1" id="addressLine1" value="${issuerInfoSubmitted.addressLine1}" class="input-xlarge" maxlength="25">
				                                <div id="addressLine1_error" class=""></div>
				                            </div> <!-- end of controls-->
				                    </div><!-- end of control-group -->
			                    
				                    <div class="control-group">
				                        <label for="addressLine2" class="required control-label"><spring:message  code="label.streetaddress2"/><%-- <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> --%></label>
				                            <div class="controls">
				                                <input type="text" name="location.address2" id="addressLine2" value="${issuerInfoSubmitted.addressLine2}" class="input-xlarge" maxlength="25">
				                                <div id="addressLine2_error" class=""></div>
				                            </div> <!-- end of controls-->
				                    </div><!-- end of control-group -->
			                    
				                    <div class="control-group">
				                        <label for="location.city" class="required control-label"><spring:message  code="label.city"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				                            <div class="controls">
				                                <input type="text" name="location.city" id="city" value="${issuerInfoSubmitted.city}" class="input-xlarge" maxlength="15">
				                                <div id="city_error" class=""></div>
				                            </div> <!-- end of controls-->
				                    </div><!-- end of control-group -->
		                    
		                    	<div class="control-group">
		                            <label for="state" class="required control-label"><spring:message  code="label.emplState"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
		                            <div class="controls">
		                                <select  id="state" name="location.state" path="statelist">
		                                     <option value="">Select</option>
		                                     <c:forEach var="state" items="${statelist}">
		                                        <option <c:if test="${state.code == issuerInfoSubmitted.state}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
		                                    </c:forEach>
		                                </select>
		                                <div id="state_error"></div>
		                            </div>
		                        </div><!-- end of control-group -->
		                        
		                        <div class="control-group">
		                            <label for="zip" class="required control-label"><spring:message  code="label.zipCode"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
		                                <div class="controls">
		                                    <input type="text" name="location.zip" id="zip" value="${issuerInfoSubmitted.zip}" class="input-small" maxlength="5">
		                                    <div id="zip_error" class=""></div>
		                                </div> <!-- end of controls-->
		                        </div><!-- end of control-group -->
                         </c:if> 
                       </div>
                </form>	
        	</div><!--  end of span9 -->
		</div>
	</div>
     <c:if test="${isIssuerRepAdded == 'true'}">
		<div id="addRepresentative" class="modal hide fade">
			<div class="modal-header">
				<a class="close" data-dismiss="modal" data-original-title="">x</a>
							<h3 id="myModalLabel"><spring:message  code="pgheader.addMoreRepresentatives"/></h3>
			</div>
			<div class="modal-body">
							<p><spring:message  code="pgheader.doYouWantMoreRepresentatives"/></p>
			</div>
			<div class="modal-footer">
				<a href='<c:url value="/planmgmt/issuer/application"/>' class="btn"><spring:message  code="label.no"/></a>
				<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message  code="label.yes"/></a>
		   	</div>
		</div>
		<script type="text/javascript">
		   $('#addRepresentative').modal("show");
		</script>	 
     </c:if>

<script type="text/javascript">
var csrValue = $("#csrftoken").val();
$(document).ready(function() {
    $.ajaxSetup({ cache: false }); // prevent Ajax caching in JQuery
    $("#email").keyup(function(){
    	$('#email_error').html("");
    });
    $("#email").focusout(function(){
    	if($("#email").val() != ""){  // if email address is not empty
    		$('#IssuerAddRepSubmitBtn').attr("disabled", false);
    		$('#frmIssuerAddRep').ajaxSubmit({
				type : "POST",
				url: "<c:url value='/planmgmt/checkDuplicate'/>",
				data: { "userEmailId": $("#email").val(),"csrftoken":csrValue},
				success: function(responseText){
					if(responseText){
						$('#email_error').html("<label class='error'><span><em class='excl'>!</em><spring:message code='err.duplicateEmail' javaScriptEscape='true'/></span></label>");
						$('#IssuerAddRepSubmitBtn').attr("disabled", true);
					} 
		       	}
			});
		}	
	  return false; 
	});	 
});

var validator = $("#frmIssuerAddRep").validate({ 
	rules : {
		firstName : {required : true, validFirstNameCheck : true},
		lastName : {required : true, validLastNameCheck : true},
		phone1 : {required : true, digits: true},
		phone2 : {required : true, digits: true},
		phone3 : {required : true, digits: true, minlength:4, maxlength:4, phoneUS:true},
		email : { required : true, emailIdCheck: true},
		password : { required : true, minlength:8},		
		confirmPassword : {equalTo: "#password"},
		"location.address1" : { required : true},
		"location.city" : {required : true},
		"location.state" : { required : true},
		"location.zip" : { required : true, digits: true, minlength:5, maxlength:5}
	},
	messages : {
		firstName : { required : "<span><em class='excl'>!</em><spring:message  code='err.firstName' javaScriptEscape='true'/></span>",
			  validFirstNameCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstNameCheck' javaScriptEscape='true'/></span>"},
		lastName: { required : "<span><em class='excl'>!</em><spring:message  code='err.lastName' javaScriptEscape='true'/></span>" ,
				validLastNameCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateLastNameCheck' javaScriptEscape='true'/></span>"},
		phone3 : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
				digits : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
         		minlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
          		maxlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>"
  		},
		email : { required : "<span><em class='excl'>!</em><spring:message  code='err.email' javaScriptEscape='true'/></span>",
				  emailIdCheck : "<span><em class='excl'>!</em><spring:message  code='err.invalidEmail' javaScriptEscape='true'/></span>"},
		password : { required : "<span><em class='excl'>!</em><spring:message  code='err.password' javaScriptEscape='true'/></span>"},		
		confirmPassword : {required : "<span><em class='excl'>!</em><spring:message  code='err.confirmPassword' javaScriptEscape='true'/></span>"},
		"location.address1" : { required : "<span><em class='excl'>!</em><spring:message  code='err.addressLine1' javaScriptEscape='true'/></span>"},
		"location.city" : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerCity' javaScriptEscape='true'/></span>"},
		"location.state": { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerState' javaScriptEscape='true'/></span>"},
		"location.zip" : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerZip' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error');
	} 
});


//phone number validation
$.validator.addMethod("phoneUS", function(value, element, param) {
	if($.browser.msie==true && $.browser.version=='8.0'){
		phoneNumber1 = $("#phone1").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber2 = $("#phone2").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber3 = $("#phone3").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}else{
		phoneNumber1 = $("#phone1").val().trim();
		phoneNumber2 = $("#phone2").val().trim();
		phoneNumber3 = $("#phone3").val().trim();
	}
	var phoneNumber=phoneNumber1+phoneNumber2+phoneNumber3;
	
	if(!isNaN(phoneNumber1) && !isNaN(phoneNumber2) && !isNaN(phoneNumber3) && phoneNumber.length==10){
		$("#phone").val(phoneNumber1+phoneNumber2+phoneNumber3);
		return true;
	}else{
		if(isNaN(phoneNumber1) || phoneNumber1.length!=3){
			$('#phone1').removeClass('input-mini valid').addClass('input-small error');
		}else{
			$('#phone1').removeClass('input-mini error').addClass('input-small valid');
		}
		if(isNaN(phoneNumber2) || phoneNumber2.length!=3){
			$('#phone2').removeClass('input-mini valid').addClass('input-small error');
		}else{
			$('#phone2').removeClass('input-mini error').addClass('input-small valid');
		}
		if(isNaN(phoneNumber3) || phoneNumber3.length!=4){
			$('#phone3').removeClass('input-mini valid').addClass('input-small error');
		}else{
			$('#phone3').removeClass('input-mini error').addClass('input-small valid');
		}
		return false;
	}	
},"<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>");

$.validator.addMethod("emailIdCheck", function(value, element) {
	//var emailRegEx = new RegExp("^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$");
	var emailRegEx = new RegExp("^[_A-Za-z0-9\u002e_]+([A-Za-z0-9\u002e]+)@[A-Za-z0-9]+([\u002e][a-zA-Z]{2,})+$");
	if("" != value && !emailRegEx.test(value)){
		return false;
	}else{
		return true;
	}
});
jQuery.validator.addMethod("validFirstNameCheck", function(value,
		element, param) {
	fName = $("#firstName").val();
	  var asciiReg = /^[a-zA-Z0-9]+$/;
	  if( !asciiReg.test(fName ) ) {
		return false;
	  } else {
		return true;
	  }
	return false;
});	

jQuery.validator.addMethod("validLastNameCheck", function(value,
		element, param) {
	lName = $("#lastName").val();
	 var asciiReg = /^[a-zA-Z0-9]+$/;
	  if( !asciiReg.test(lName ) ) {
		return false;
	  } else {
		return true;
	  }
	return false;
});
</script>		
