package com.getinsured.eligibility.lce.task;

import java.util.concurrent.Executor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.getinsured.hix.platform.util.ContextAwarePoolExecutor;

@Configuration
@EnableAsync
public class EligibilityAsyncConfiguration implements AsyncConfigurer {
	 @Bean
     public LifeChangeEventTask demographicChangeTask() {
         return new LifeChangeEventTask();
     }

	 @Override
     public Executor getAsyncExecutor() {
         ThreadPoolTaskExecutor executor = new ContextAwarePoolExecutor();
         executor.setCorePoolSize(1);
         executor.setMaxPoolSize(10);
         //executor.setQueueCapacity(11);
         executor.setThreadNamePrefix("EligibilitySvc-ThreadPoolTaskExecutor-");
         executor.initialize();
         return executor;
     }

	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return new SimpleAsyncUncaughtExceptionHandler() ;
	}
}
