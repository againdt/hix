function baseFunctionForBackButton() {
	var appscrnpage = currentPage;

	if((flagToUpdateFromResult || editFlag) && ( appscrnpage == 'appscr61' || appscrnpage == 'appscr62Part1')){
		//$('#back_button_div').hide();
		$('#countinue_button_div').show();
		$('#contBtn').text('Continue');
		$('#contBtn').show();
	}
	else
		{
		$('#back_button_div').show();
		$('#countinue_button_div').show();
		$('#contBtn').text('Continue');
		$('#contBtn').show();
		}

	switch (appscrnpage) {


	case "appscr61":
		if (nonFinancialHouseHoldDone > 1) {


			nonFinancialHouseHoldDone--;

			if(getInfantData(nonFinancialHouseHoldDone)=='yes')
				while(getInfantData(nonFinancialHouseHoldDone)=='yes')
				{
					nonFinancialHouseHoldDone--;
				}

			setNonFinancialPageProperty(nonFinancialHouseHoldDone);

			appscr67EditController(nonFinancialHouseHoldDone);
			editFlag = false;
			$('#back_button_div').show();





			fetchDataNonFinancialHouseHold();
			backData64();
			goToPageById("appscr64");

			return false;
		}
		else
			{

			//HouseHoldRepeat = parseInt($('#numberOfHouseHold').text());

			//always go back to primary contact tax file page
			HouseHoldRepeat = 1;

			$("input[name=planToFileFTRIndicator], input[name=householdHasDependant], #householdContactDependant1").prop("disabled",false);

			if(getInfantData(HouseHoldRepeat)=='yes')
				while(getInfantData(HouseHoldRepeat)=='yes')
				{
					HouseHoldRepeat--;
				}

			setNonFinancialPageProperty(HouseHoldRepeat);
			fetchDataAddHouseHold();
			backData60(); // for save

			if(ssapApplicationType == "NON_FINANCIAL"){
				goToPageById("appscr59A");
			}else{
				//pre-populate data from JSON  
				updateAppscr60FromJSON(HouseHoldRepeat);
				goToPageById("appscr60");
			}


			return false;

			}
		break;


	case "appscr69":

		if (financialHouseHoldDone > 1) {

			financialHouseHoldDone--;

			appscr74EditController(financialHouseHoldDone);
			$('#back_button_div').show();

			editFlag=false;

			if($('#incomesStatus'+financialHouseHoldDone).text() != 'No')
				{
				backData73();
				goToPageById("appscr73");
				}
			else
				{


				goToPageById("appscr70");
				}
			fetchDataAddFinancialHouseHold();
			setNonFinancialPageProperty(financialHouseHoldDone);
			backData73();
			backData72();
			backData71();
			backData70();
			return false;
		}
		else
			{
			setNonFinancialPageProperty(nonFinancialHouseHoldDone);
			}
		break;


	case "appscr76P1":
		if(addQuetionsHouseHoldDone != 1)
		{
			addQuetionsHouseHoldDone--;
			setNonFinancialPageProperty(addQuetionsHouseHoldDone);
			setHouseHoldData();

			goToPageById("appscr81B");
			return false;
		}
		break;

	/*case "appscr76P2":
		if($("#appscrFinancial_Job57TD"+financialHouseHoldDone).text()=='0')
		{

		goToPageById("appscr75A");
		return false;

		}
		backData76P1();
		break;*/

	case "appscr74":

			appscr74EditController(financialHouseHoldDone);


			editFlag=false;

			if($("#appscrFinancial_Job57TD"+financialHouseHoldDone).text()=='0')

				$("#appscr73EmployerNameDiv").hide();
			else

				$("#appscr73EmployerNameDiv").show();


			fetchDataAddFinancialHouseHold();
			
		if(anyIncome(financialHouseHoldDone)==false){
			    backData69();backData70();backData71();backData72();backData73();
				goToPageById("appscr70");

				}
				else{
				backData73();
				goToPageById("appscr73");
				}



			$('#back_button_div').show();

			setNonFinancialPageProperty(financialHouseHoldDone);
			backData73();
			return false;

			break;


	case "appscr60":

		if (HouseHoldRepeat > 1) {

			$("input[name=planToFileFTRIndicator], input[name=householdHasDependant], input[name=householdContactSpouse], #householdContactDependant1").prop("disabled",false);

			//HouseHoldRepeat--;
			HouseHoldRepeat = 1;

			if(getInfantData(HouseHoldRepeat)=='yes')
				while(getInfantData(HouseHoldRepeat)=='yes')
				{
					HouseHoldRepeat--;
				}



			setNonFinancialPageProperty(HouseHoldRepeat);

			fetchDataAddHouseHold();

			backData60(); // for save
			
			//pre-populate data from JSON  
			updateAppscr60FromJSON(HouseHoldRepeat);
			goToPageById("appscr60");

			return false;

		}
		else
			{



			goToPageById("appscr59A");
			return false;

			}

	case "appscr65":

			if(getInfantData(nonFinancialHouseHoldDone)=='yes')
			while(getInfantData(nonFinancialHouseHoldDone)=='yes')
			{
				nonFinancialHouseHoldDone--;
			}


			setNonFinancialPageProperty(nonFinancialHouseHoldDone);

			appscr67EditController(nonFinancialHouseHoldDone);
			editFlag = false;
			$('#back_button_div').show();

			fetchDataAddHouseHold();
			backData64();
			goToPageById("appscr64");

			return false;



		break;

	case "appscr63":

		var usVariable = $("#appscr57UScitizenTD"+nonFinancialHouseHoldDone).text();
		var TempUScitizen=$('input[name=UScitizen]:radio:checked').val();

		goToPageById("appscr62Part1");
		backData62Part1();
		return false;



		/*if(usVariable == 'yes' || TempUScitizen == 'yes')
			{
			goToPageById("appscr62Part1");
			backData62Part1();
			return false;
			}
		else
			{
			goToPageById("appscr62Part2");
			backData62Part2();
			return false;
			}


		break;*/

	}
}


function anyIncome(applicantID){
	
	
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[applicantID-1];

		var haveAnyIcome = false;
		
		if(householdMemberObj.detailedIncome.jobIncomeIndicator == true){			
			haveAnyIcome = true;
		}		
		if(householdMemberObj.detailedIncome.selfEmploymentIncomeIndicator == true){			
			haveAnyIcome = true;
		}		
		if(householdMemberObj.detailedIncome.socialSecurityBenefitIndicator == true){			
			haveAnyIcome = true;
		}		
		if(householdMemberObj.detailedIncome.unemploymentBenefitIndicator == true){		
			haveAnyIcome = true;
		}		
		if(householdMemberObj.detailedIncome.retirementOrPensionIndicator == true){		
			haveAnyIcome = true;
		}		
		if(householdMemberObj.detailedIncome.capitalGainsIndicator == true){			
			haveAnyIcome = true;
		}		
		if(householdMemberObj.detailedIncome.rentalRoyaltyIncomeIndicator == true){			
			haveAnyIcome = true;
		}		
		if(householdMemberObj.detailedIncome.farmFishingIncomeIndictor == true){		
			haveAnyIcome = true;
		}		
		if(householdMemberObj.detailedIncome.alimonyReceivedIndicator == true){		
			haveAnyIcome = true;
		}		
		if(householdMemberObj.detailedIncome.investmentIncomeIndicator == true){		
			haveAnyIcome = true;
		}		
		if(householdMemberObj.detailedIncome.otherIncomeIndicator == true){		
			haveAnyIcome = true;
		}		
		return haveAnyIcome;	
}

/*********************************************************************************************/
function fetchDataAddHouseHold() {

	var houseHoldIndex = HouseHoldRepeat;

	getSpouse(houseHoldIndex);
	/*var martialStatus=$("#appscr57isMarriedTD"+houseHoldIndex).text();
	var spouseID=$("#appscr57spouseIdTD"+houseHoldIndex).text();
	var isPayingJointly=$("#appscr57payJointlyTD"+houseHoldIndex).text();
	var  isTaxFiller=$("#appscr57isTaxFilerTD"+houseHoldIndex).text();

	if($('#appscrIsChildDiv').is(':visible')){
		if(martialStatus.toLowerCase()=="yes"||martialStatus.toLowerCase()=="true"){
			$("#marriedIndicatorYes").prop('checked',true);
		}
		else if(martialStatus.toLowerCase()=="no"||martialStatus.toLowerCase()=="false") {
				$("#marriedIndicatorNo").prop('checked',true);
		}
	}


	if($('#householdSpouseMainDiv').is(':visible')){
		if(isPayingJointly.toLowerCase()=="yes"||isPayingJointly.toLowerCase()=="true"){
			$("#planOnFilingJointFTRIndicatorYes").prop('checked',true);
		}
		else if(isPayingJointly.toLowerCase()=="no"||isPayingJointly.toLowerCase()=="false") {
			$("#planOnFilingJointFTRIndicatorNo").prop('checked',true);
		}
	}



	if (isTaxFiller.toLowerCase()=="yes"||isTaxFiller.toLowerCase()=="true") {
		$("#planToFileFTRIndicatorYes").prop('checked',true);

	} else if (isTaxFiller.toLowerCase()=="no"||isTaxFiller.toLowerCase()=="false") { {
		$("#planToFileFTRIndicatorNo").prop('checked',true);
	}

	for ( var hNum = 1; hNum <= noOfHouseHold; hNum++) {
		if($("#HouseHold"+hNum).is(":checked")){
			if ($("#HouseHold"+hNum)==spouseID) {
				$("#HouseHold"+hNum).prop('checked',true);
				break;
			}
		}

		}
	}*/



}

/*********************************************************************************************/
function fetchDataNonFinancialHouseHold() {
	
	/*

	$('input[name=appscr61_gender][value='+$("#appscrHouseHoldgender57TD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);

	$('input[name=socialSecurityCardHolderIndicator][value='+$("#appscr57HouseHoldHavingSSNTD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);




	var HouseHoldSSN=$("#appscr57HouseHoldSSNTD"+nonFinancialHouseHoldDone).text();
	if(HouseHoldSSN=='')
	{

	 $('#ssn1').val("");
	 $('#ssn2').val("");
	 $('#ssn3').val("");
		$('#ssn').val("");
	}
	else
		{
			$('#ssn').val(HouseHoldSSN);
		 $('#ssn1').val(HouseHoldSSN.split("-")[0]);
		 $('#ssn2').val(HouseHoldSSN.split("-")[1]);
		 $('#ssn3').val(HouseHoldSSN.split("-")[2]);

		}


		$('#reasonableExplanationForNoSSN').val($("#appscr57notAvailableSSNReasonTD"+nonFinancialHouseHoldDone).text());



		$('input[name=UScitizen][value='+$("#appscr57UScitizenTD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);

		$('input[name=UScitizenManualVerification][value='+$("#appscr57UScitizenManualVerificationTD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);

		$('input[name=naturalizedCitizenshipIndicator][value='+$("#appscr57naturalizedCitizenshipIndicatorTD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);

		$('input[name=livedIntheUSSince1996Indicator][value='+$("#appscr57livedIntheUSSince1996IndicatorTD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);

		$('input[name=naturalCitizen_eligibleImmigrationStatus][value='+$("#appscr57ImmigrationStatusTD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);

*/
}

/*********************************************************************************************/
function fetchDataAddFinancialHouseHold() {

		/*var availableIncome=false;
			var dataIndex=0;

				$('input[name=appscr70radios4Income][value='+$("#incomesStatus"+financialHouseHoldDone).text()+']').prop('checked',true);

			for (;dataIndex < financial_DetailsArray.length ; dataIndex++) {


			if(dataIndex==0)
			{
					 $('#irsIncome').val($("#appscrirsIncome57TD"+financialHouseHoldDone).text());
					 $('#irsIncomeLabel').html("$"+$("#appscrirsIncome57TD"+financialHouseHoldDone).text());
					 continue;
			}
			var data = financial_DetailsArray[dataIndex];


			 $('#'+data+'_id').val($("#appscr"+data+"57TD"+financialHouseHoldDone).html());
				if(dataIndex!=2 && dataIndex!=6)
					$('#'+data+'_select_id').val($("#appscr"+data+"Frequency57TD"+financialHouseHoldDone).text());



				if( $("#appscr"+data+"57TD"+financialHouseHoldDone).text() == '0')
				{

					$('#' + data + '_Div').hide();
					$('#' + data ).prop('checked', false);


				}
			else
				{
				//$('#' + data + '_Div').show();
				//$('#' + data ).prop('checked', true);

				availableIncome=true;
				}
			}

			if($("#appscrexpectedIncome57TD"+financialHouseHoldDone).html() != 'N/A')
				$('#expectedIncomeByHouseHoldMember').val($("#appscrexpectedIncome57TD"+financialHouseHoldDone).text());
			else
				$('#expectedIncomeByHouseHoldMember').val('');

			if(availableIncome==true)
				{

				$('#checkBoxes_div').show();
				$('#checkBoxes_divlbl').show();

				}
			else
				{
				$('#Financial_None').prop('checked',true);
				$('#checkBoxes_div').hide();
				$('#checkBoxes_divlbl').hide();
				}


			if($("#appscrFinancial_Job57TD"+financialHouseHoldDone).text()=='0')

				$("#appscr73EmployerNameDiv").hide();
			else

				$("#appscr73EmployerNameDiv").show();
*/

}