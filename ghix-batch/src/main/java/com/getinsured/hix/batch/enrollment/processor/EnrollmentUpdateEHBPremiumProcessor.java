package com.getinsured.hix.batch.enrollment.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;

import com.getinsured.hix.batch.enrollment.service.EnrollmentEHBUpdateService;
import com.getinsured.hix.batch.enrollment.skip.EnrollmentEHBUpdation;
import com.getinsured.hix.batch.enrollment.xmlEnrollments.exceptions.InvalidRecordException;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;

public class EnrollmentUpdateEHBPremiumProcessor implements ItemProcessor< Integer, Integer> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentUpdateEHBPremiumProcessor.class);
	private EnrollmentEHBUpdateService enrollmentEHBUpdateService;
	private EnrollmentEHBUpdation enrollmentEHBUpdation;
	private JobService jobService;
	long jobExecutionId = -1;
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Processor ");
		jobExecutionId=stepExecution.getJobExecution().getId();
	}
	
	@Override
	public Integer process(Integer enrollmentId) throws Exception {
		
		String batchJobStatus=null;
		if(jobService != null && jobExecutionId != -1){
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}
		
		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) || 
				                    	batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}
		
		if(enrollmentId != null ){
			try{
				enrollmentEHBUpdateService.updateEnrollmentEHBAmount(enrollmentId);
			
			}catch(Exception e){
				enrollmentEHBUpdation.addToSkippedEnrollmentIdList(enrollmentId);
				throw new InvalidRecordException(e.getMessage());
			}
		}
		
		return enrollmentId;
	}
	
	/**
	 * @return the enrollmentEHBUpdateService
	 */
	public EnrollmentEHBUpdateService getEnrollmentEHBUpdateService() {
		return enrollmentEHBUpdateService;
	}

	/**
	 * @param enrollmentEHBUpdateService the enrollmentEHBUpdateService to set
	 */
	public void setEnrollmentEHBUpdateService(EnrollmentEHBUpdateService enrollmentEHBUpdateService) {
		this.enrollmentEHBUpdateService = enrollmentEHBUpdateService;
	}

	public EnrollmentEHBUpdation getEnrollmentEHBUpdation() {
		return enrollmentEHBUpdation;
	}

	public void setEnrollmentEHBUpdation(EnrollmentEHBUpdation enrollmentEHBUpdation) {
		this.enrollmentEHBUpdation = enrollmentEHBUpdation;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
	

}
