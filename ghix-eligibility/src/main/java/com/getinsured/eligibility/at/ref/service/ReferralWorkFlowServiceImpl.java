package com.getinsured.eligibility.at.ref.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ReferralActivationRepository;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author chopra_s
 * 
 */
@Component("referralWorkFlowService")
@Scope("singleton")
public class ReferralWorkFlowServiceImpl implements ReferralWorkFlowService {
	@Autowired
	@Qualifier("referralRidpService")
	private ReferralRidpService referralRidpService;

	@Autowired
	@Qualifier("referralSsapCmrLinkService")
	private ReferralSsapCmrLinkService referralSsapCmrLinkService;
	
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	
	@Autowired
	@Qualifier("referralActivationRepository")
	private ReferralActivationRepository referralActivationRepository;
	
	@Autowired
	@Qualifier("cmrHouseholdRepository")
	private CmrHouseholdRepository cmrHouseholdRepository;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired private AppEventService appEventService;
	@Autowired private LookupService lookupService;

	
	private int PRIMARY_PERSON = 1;
	private static final Logger LOGGER = Logger.getLogger(ReferralWorkFlowServiceImpl.class);



	@Override
	public Map<String, String> processRidpAndLinking(final long referralActivationId, int userId, int cmrId) {
		Map<String, String> result = null;
		
		
		final ReferralActivation referralActivation = referralActivationRepository.findOne(referralActivationId);
		if (referralActivation == null) {
			LOGGER.warn(ReferralConstants.NO_REFERRALACTIVATION_FOUND);
			result = new HashMap<String, String>();
			result.put(ReferralConstants.FLOW, String.valueOf(ReferralConstants.ERROR));
			return result;
		}

		/*if (!referralActivation.isRidpPending()) {
			LOGGER.warn(ReferralConstants.WORKFLOW_STATUS_NOT_VALID);
			result = ReferralConstants.STATUS_NOT_VALID;
			return result;
		}*/
		// cmrId(household)- This is ID to which application is being linked (Old HH)
		// referralActivation- This is required to find recently pushed AT and its details
		//ssapApplicant- This is a primary applicant in newly pushed AT (New HH)
		
		SsapApplicant ssapApplicant = ssapApplicantRepository.findByPersonId(referralActivation.getSsapApplicationId(), PRIMARY_PERSON);
		Household household = cmrHouseholdRepository.findOne(cmrId);//Old HH
		int flow  = ReferralConstants.ERROR;
		boolean sameSSNflag= false;
		 //Get SSN from primary-applicant/ Get SSN of CMR / Check if both have same SSN / If same SSN OR primary have no SSN then directly call  #executeRidp
		 if(null != ssapApplicant && household != null) {
			if( (ssapApplicant.getSsn() == null ||  ssapApplicant.getSsn().isEmpty()) && (household.getSsn() == null || household.getSsn().isEmpty())) {
				sameSSNflag = true;
			}else if (Objects.equals(ssapApplicant.getSsn(), household.getSsn()) ) {
				sameSSNflag = true;
			}
		 } 
			
		//If primary-applicant and CMR has different SSN then 
		 boolean diffSSNflag= false;
		 boolean nullifySSN = false;
		 Integer newHHId= null;
		 List<SsapApplication> localAppList = null;
		 int ssnConflictedCMRid =0;
		 if(!sameSSNflag) {
			// Find CMR from primary-applicant-SSN / If there is no CMR..then directly call  #executeRidp /  if there is CMR and it don't have user_id/enrolledApps --> FLAG-1 Then null SSN into CMR with Reason 
				if(ssapApplicant.getSsn() != null && !ssapApplicant.getSsn().isEmpty()) {
					List <Household> householdList = cmrHouseholdRepository.findMatchingCmrObjectBySSN(ssapApplicant.getSsn());
					
					if(null == householdList || householdList.isEmpty()) {
						diffSSNflag = true;
					}else {

						 GiAuditParameterUtil.add(new GiAuditParameter("HH Linking fail",  " Can't link due to SSN Conflict for CMR "+householdList.get(0).getId() + " for linking application "+ referralActivation.getSsapApplicationId() + "  with CMR "+ household.getId()));
						 ssnConflictedCMRid = 		householdList.get(0).getId();
					}
					 
				}
			 	
		 }
		 
		
		if(sameSSNflag || diffSSNflag) {
			flow = referralRidpService.executeRidp(ssapApplicant, userId, household);
		}else {
			//Return error list from here itself
			
			Map<String, String> mapEventParam = new HashMap<>();
			mapEventParam.put("Application Id",referralActivation.getSsapApplicationId().toString() );
			mapEventParam.put("CMR linked with", household.getId()+"" );
			mapEventParam.put("CMR for clearing SSN ",  ssnConflictedCMRid +"" );
			  
			logIndividualAppEvent("HH_LINKING","SSN_CONFLICT",  mapEventParam, household.getId());
			
			
			
			result = new HashMap<String, String>();
			result.put(ReferralConstants.FLOW, String.valueOf(ReferralConstants.ERROR));
			result.put("REASON",  "Application causing conflict { "+ referralActivation.getSsapApplicationId().toString() +" }");
			return result;
		}
		

		if (flow != ReferralConstants.SUCCESS) {
			result = new HashMap<String, String>();
			result.put(ReferralConstants.FLOW, String.valueOf(flow));
			return result;
		}

		result = referralSsapCmrLinkService.executeLinking(referralActivationId, userId, cmrId);

		return result;
	}
	
	private void logIndividualAppEvent(String eventName, String eventType, Map<String, String> mapEventParam,Integer cmrId ){

		try {
			//event type and category
			 
			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(eventName, eventType);
			EventInfoDto eventInfoDto = new EventInfoDto();

			

			eventInfoDto.setModuleId(cmrId );
			eventInfoDto.setModuleName("INDIVIDUAL");
			eventInfoDto.setEventLookupValue(lookupValue);
			appEventService.record(eventInfoDto, mapEventParam);

		} catch(Exception e){
			LOGGER.error("Exception occured while log Application event"+e.getMessage(), e);
			//throw new GIRuntimeException(e);
		}
	}
	

}