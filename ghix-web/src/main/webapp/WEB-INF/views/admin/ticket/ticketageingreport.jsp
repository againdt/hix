
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page isELIgnored="false"%>

 
<script type="text/javascript" src="<c:url value="/resources/js/highcharts.js" />"></script>


<c:choose>
<c:when test="${reportDtoListJSon.length()=='2'}">
<div id="noRecords" ><B><spring:message code='text.series.norecords' /> </B></div>
</c:when>

<c:otherwise>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<div id="btnDownload" align="center">
		  
	<input type="button" name="downloadBtn" id="downloadAgeingReport" value="<spring:message code="btn.ticketstat.download"/>" class="btn btn-primary btn-small" />
</div>
</c:otherwise>
</c:choose>

     <input type="hidden" value="<encryptor:enc value = "${type}"/>" name="type" id="type" />

<script type="text/javascript" >
var data=${reportDtoListJSon};
var categiries =${categories};

$('#downloadAgeingReport').click(function(){
	var type =encodeURIComponent($("#type").val());
	var datatocontroller = encodeURIComponent(JSON.stringify(data));
	var categoryList = encodeURIComponent(JSON.stringify(categiries));

	window.location.assign(
			"${pageContext.request.contextPath}"+'/ticketmgmt/ticket/downloadAgeingReport?type='+type+'&categoryList='+categoryList+'&graphData='+datatocontroller
   );
	
});
	 
$(function () {
	
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Ticket Aging Report'
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: categiries
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Count of Tickets'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -70,
            verticalAlign: 'top',
            y: 20,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            formatter: function() {
                return '<b>'+ this.x +'</b><br/>'+
                    this.series.name +': '+ this.y +'<br/>'+
                    'Total: '+ this.point.stackTotal;
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                      
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        if (this.y != 0) {
                          return this.y ;
                        } else {
                          return null;
                        }
                    },       
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                },
		        point: {
		            events: {
		                click: function() {
                            openTicket(this.series.name,this.category);
		                    /*window.location.assign(
		                    		"${pageContext.request.contextPath}"+'/ticketmgmt/ticket/ticketlist?ticketStatus=Open,UnClaimed&ticketPriority='+this.series.name+'&period='+this.category
		                   );*/
		                }
		            }
		        }
            }
        },
      
          series: data
    });
});
var openTicket=function(a,b)
{
    $("#graphPoster #ticketStatus").val("Open,UnClaimed");
    $("#graphPoster #ticketPriority").val(a);
    $("#graphPoster #period").val(b);
    $("#graphPoster").submit();
};
</script>
<form id="graphPoster" method="POST" action="${pageContext.request.contextPath}/ticketmgmt/ticket/ticketlist?fromAgeingReports=true">
<input type="hidden" name="ticketStatus" id="ticketStatus" />
<input type="hidden" name="ticketPriority" id="ticketPriority" />
<input type="hidden" name="period" id="period" />
<df:csrfToken/>
</form>