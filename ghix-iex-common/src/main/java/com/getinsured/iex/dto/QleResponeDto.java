package com.getinsured.iex.dto;

import java.util.List;

public class QleResponeDto {
	
	List<QleApplicantsDto> applicants;
	String applicationId;
	String status;
	String validationStatus;
	String eligibilityStatus;
	String financialAssistanceFlag;
	
	public List<QleApplicantsDto> getApplicants() {
		return applicants;
	}
	public void setApplicants(List<QleApplicantsDto> applicants) {
		this.applicants = applicants;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getValidationStatus() {
		return validationStatus;
	}
	public void setValidationStatus(String validationStatus) {
		this.validationStatus = validationStatus;
	}
	
	public String getEligibilityStatus() {
		return eligibilityStatus;
	}
	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}
	public String getFinancialAssistanceFlag() {
		return financialAssistanceFlag;
	}
	public void setFinancialAssistanceFlag(String financialAssistanceFlag) {
		this.financialAssistanceFlag = financialAssistanceFlag;
	}
	
	
	
	
}
