package com.getinsured.hix.indportal.dto.dm.application;

public class ApplicationSignature {
	 private String applicationSignatureText;
	 private String applicationSignatureType;
	 private String applicationSignatureDate;


	 // Getter Methods 

	 public String getApplicationSignatureText() {
	  return applicationSignatureText;
	 }

	 public String getApplicationSignatureType() {
	  return applicationSignatureType;
	 }

	 public String getApplicationSignatureDate() {
	  return applicationSignatureDate;
	 }

	 // Setter Methods 

	 public void setApplicationSignatureText(String applicationSignatureText) {
	  this.applicationSignatureText = applicationSignatureText;
	 }

	 public void setApplicationSignatureType(String applicationSignatureType) {
	  this.applicationSignatureType = applicationSignatureType;
	 }

	 public void setApplicationSignatureDate(String applicationSignatureDate) {
	  this.applicationSignatureDate = applicationSignatureDate;
	 }
	}
