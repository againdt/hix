package com.getinsured.hix.iex.lce.task;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.SsapEndPoints;
import com.getinsured.hix.platform.util.GhixRestServiceInvoker;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.client.ResourceCreator;
import com.getinsured.iex.dto.LifeChangeEventDTO;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.dto.SsapSEPIntegrationDTO;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.LifeChangeEventUtil;
import com.thoughtworks.xstream.XStream;

@Component
public class LifeChangeEventTask extends LifeChangeEventHandler {
	private static final String TRIGGER_IND57 = "triggerIND57";

	private static final String OLD_SSAP_JSON = "oldSsapJson";

	private static final Logger LOGGER = LoggerFactory.getLogger(LifeChangeEventTask.class);
	
	@Autowired
	private GhixRestServiceInvoker ghixRestServiceInvoker;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	@Autowired
	private ResourceCreator resourceCreator;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private UserService userService;
	
	//@Async
	public void process(long oldApplicationId, long newApplicationId, long newApplicationEventId, Map<Long, List<String>> demographicEventsData, AccountUser user, Map<String, Boolean> demographicFlagMap) throws Exception {
		LOGGER.info("Life Change Event Task  : Starts");
		try {
			if(demographicFlagMap.get(LifeChangeEventConstant.IS_DEMOGRAPHIC_CHANGE) || demographicFlagMap.get(LifeChangeEventConstant.IS_ADDRESS_DEMOGRAPHIC_CHANGE)){
				LOGGER.info("Demographic Changes Processing  : Starts");
				
				LifeChangeEventDTO lifeChangeEventDTO = new LifeChangeEventDTO();
				lifeChangeEventDTO.setOldApplicationId(oldApplicationId);
				lifeChangeEventDTO.setUserName(user.getUsername());
				
				LOGGER.info("Fetching Enrollment Details for Application : Starts"+oldApplicationId);
				lifeChangeEventDTO = getEnrollmentDetails(lifeChangeEventDTO);
				LOGGER.info("Fetching Enrollment Details : Ends"+lifeChangeEventDTO);
					
				if(lifeChangeEventDTO!=null && GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(lifeChangeEventDTO.getStatus())){
					Set<Long> enrolledPersons = lifeChangeEventDTO.getPersonIds();
					if(enrolledPersons!=null && enrolledPersons.size()>0){
						SsapApplicationResource ssapApplicationResource = resourceCreator.getSsapApplicationById(oldApplicationId);
						String oldSsapJson = ssapApplicationResource.getApplicationData();
						//Update previous application json for enrolled members if there is change in Demo events
						LOGGER.info("Updating Enrolled Members Info in Json and DB : Starts");
						Map<String,Object> updateResult = updateSsapJsonForEnrolledApplicants(newApplicationId, newApplicationEventId, enrolledPersons, oldSsapJson, demographicEventsData);
						boolean triggerIND57 = (boolean)updateResult.get(TRIGGER_IND57);
						LOGGER.info("Updating Enrolled Members Info in Json and DB : Ends");
						
						if(triggerIND57) {
							//Invoking IND57
							oldSsapJson = (String)updateResult.get(OLD_SSAP_JSON);
							ssapApplicationResource.setApplicationData(oldSsapJson);
							updateSsapApplication(oldApplicationId, ssapApplicationResource);
							LOGGER.info("IND57 Call for Case Number : Starts"+ssapApplicationResource.getCaseNumber());
							lifeChangeEventDTO.setCaseNumber(ssapApplicationResource.getCaseNumber());
							lifeChangeEventDTO.setUserName("exadmin@ghix.com");
							invokeRestService(SsapEndPoints.LCE_INVOKE_IND57, lifeChangeEventDTO);
							LOGGER.info("IND57 Call : Ends");
						}
					}
				}
				
				LOGGER.info("Demographic Changes Processing  : Ends");
			}
			
			//Ssap Integration call to run Hub Verifications and check Eligibility
			SsapSEPIntegrationDTO ssapSEPIntegrationDTO = new SsapSEPIntegrationDTO();
			ssapSEPIntegrationDTO.setApplicationId(newApplicationId);
			ssapSEPIntegrationDTO.setIsAddressChangeDemoEvent(demographicFlagMap.get(LifeChangeEventConstant.IS_ADDRESS_DEMOGRAPHIC_CHANGE));
			triggerSSAPIntegration(ssapSEPIntegrationDTO);
		} catch(Exception exception){
			exception.printStackTrace();		
		LOGGER.info("Life Change Event Task  : Ends");
		}
	}
	
	private Map<String,Object> updateSsapJsonForEnrolledApplicants(long newApplicationId, long applicationEventId, Set<Long> oldEnrolledApplicants, String oldSsapJson, Map<Long, List<String>> demographicEventsData) throws Exception{
		Map<String,Object> result= new HashMap<String,Object>();
		Set<Long> newPersonIds = demographicEventsData.keySet();
		boolean triggerIND57 = false;
		SsapApplicationResource ssapApplicationResource = resourceCreator.getSsapApplicationById(newApplicationId);
		for (Long enrolledPersonId:oldEnrolledApplicants) {
			if(newPersonIds.contains(enrolledPersonId)){
				triggerIND57 = true;
				List<String> events = demographicEventsData.get(enrolledPersonId);
				for(String event:events){
					if(LifeChangeEventConstant.DEMOGRAPHICS_MAILING_ADDRESS_CHANGE.equals(event)){
						oldSsapJson = LifeChangeEventUtil.updateJsonForDemographicEvents(event, String.valueOf(LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID), oldSsapJson, ssapApplicationResource.getApplicationData());
					} else {
						oldSsapJson = LifeChangeEventUtil.updateJsonForDemographicEvents(event, enrolledPersonId.toString(), oldSsapJson, ssapApplicationResource.getApplicationData());
					}
				}
			}
		}
		result.put(TRIGGER_IND57, triggerIND57);
		result.put(OLD_SSAP_JSON, oldSsapJson);
		return result;
	}
	
	private LifeChangeEventDTO getEnrollmentDetails(LifeChangeEventDTO lifeChangeEventDTO) throws InvalidUserException, JsonProcessingException, IOException{
		String responseDTO = ghixRestServiceInvoker.invokeRestService(lifeChangeEventDTO, 
				SsapEndPoints.LCE_GET_ENROLLED_APPLICANTS, HttpMethod.POST, GIRuntimeException.Component.LCE.toString());
		lifeChangeEventDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(LifeChangeEventDTO.class).readValue(responseDTO);
		
		return lifeChangeEventDTO;
	}
	
	private String invokeRestService(String url, LifeChangeEventDTO lifeChangeEventDTO){
		try {
			return ghixRestServiceInvoker.invokeRestService(lifeChangeEventDTO, url, HttpMethod.POST, GIRuntimeException.Component.LCE.toString());
		} catch(Exception exception){
			throw exception;
		}
	}
	
	private void triggerSSAPIntegration(SsapSEPIntegrationDTO ssapSEPIntegrationDTO) {
		try {
			LOGGER.info("Invoking the SSAP orchestration flow : starts");
			restTemplate.postForObject(GhixEndPoints.SsapIntegrationEndpoints.LCE_INTEGRATION_URL,	ssapSEPIntegrationDTO, String.class);
			LOGGER.info("Invoking the SSAP orchestration flow : ends");
		} catch (Exception exception) {
			LOGGER.error("Error invoking the SSAP orchestration flow", exception);
			giWSPayloadRepository.saveAndFlush(LifeChangeEventUtil.createGIWSPayload(exception, "SSAP Application ID : "+String.valueOf(ssapSEPIntegrationDTO), LifeChangeEventConstant.ENDPOINT_OPERATION_INVOKE_SSAP_INTEGRATION));
		}
	}
}
