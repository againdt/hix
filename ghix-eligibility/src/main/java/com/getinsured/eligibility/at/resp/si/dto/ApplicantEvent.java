package com.getinsured.eligibility.at.resp.si.dto;

import java.util.Date;

/**
 * @author chopra_s
 * 
 */
public class ApplicantEvent {
	private String code;
	private Date eventDate;
	private Date reportDate;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

}
