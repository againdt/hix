package com.getinsured.eligibility.at.ref.si;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.at.ref.service.ReferralProcessingService;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;

@Component("queuedApplicationHandler")
public class QueuedApplicationHandler {
	
	@Autowired
	@Qualifier("referralProcessingService")
	private ReferralProcessingService referralProcessingService;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	
	private static final String SEP = "SEP";

	private static final String QEP = "QEP";
	
	public void updateQueuedApplicationsForFurtherProcessing(AccountTransferRequestDTO accountTransferRequest,
			ReferralResponse referralResponse) {

		
		// update application type
		if(referralResponse.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID) != null){
			long ssapApplicationId = (Long)referralResponse.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);	
			SsapApplication ssapApplication = ssapApplicationRepository.findById(ssapApplicationId);
			if(ssapApplication != null){
				if (accountTransferRequest.isLCE()) {
					ssapApplication.setApplicationType(SEP);
					ssapApplication.setValidationStatus(null);
				} else if (accountTransferRequest.isQE()) {
					ssapApplication.setApplicationType(QEP);
					ssapApplication.setValidationStatus(null);
					
					List<SsapApplicant> ssapApplicants = this.updateQEApplicantStatus(ssapApplication);
					ssapApplication.setSsapApplicants(ssapApplicants);
				}
			}
			ssapApplicationRepository.save(ssapApplication);
			
			this.deleteOEEvent(accountTransferRequest, ssapApplicationId);
		}
	}


	private void deleteOEEvent(AccountTransferRequestDTO accountTransferRequest, long ssapApplicationId) {
		// If application got created in QU status during OE period, an OE event is present in ssap_application_event table.
		// This event needs to be deleted because now the application will be processed as QE or SEP
		if(accountTransferRequest.isLCE() || accountTransferRequest.isQE()){
			SsapApplicationEvent oeEvent = ssapApplicationEventRepository.findEventForOpenEnrollment(ssapApplicationId);
			if(oeEvent != null){
				ssapApplicationEventRepository.delete(oeEvent.getId());
			}	
		}
	}


	private List<SsapApplicant> updateQEApplicantStatus(SsapApplication ssapApplication) {
		List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplication(ssapApplication);
		// Set qualifying event as applicant status for QE applications.
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			ssapApplicant.setStatus(ApplicantStatusEnum.QUALIFYING_EVENT.value());
		}
		ssapApplicantRepository.save(ssapApplicants);
		return ssapApplicants;
	}


}
