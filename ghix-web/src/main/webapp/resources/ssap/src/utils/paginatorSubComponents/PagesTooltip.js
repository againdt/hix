import React from 'react';

const PagesTooltip = props => {
    const {
        adjustedFlow,
    } = props;

    const getComponentName = component => {
        return component.props.name ?
            `<${component.props.name}/>` :
            typeof component.type === 'string' ?
                `<${component.type}/>` :
                `<${component.type.WrappedComponent.defaultProps.name}/>`;
    };

    return (
        <span className='hyper-navigation-tooltip-anchor'>
            P
            <ul className='hyper-navigation-tooltip'>
                <span className='list-caption'>
                    Pages and Components
                </span>
                {
                    adjustedFlow.map((el, idx) => {

                        return (
                            <li key={`${el.pageName}_${idx}`}
                                style={{
                                    position: 'relative',
                                    color: el.isActive ? 'black' : 'red'
                                }}
                            >
                                <span className='hyper-navigation-tooltip-number'>
                                    { `${el.sectionIndex + 1}_${idx}` }
                                </span>
                                <span className='hyper-navigation-tooltip-name'>
                                    { el.hhm === -1 ? '' : <b>{`[${el.hhm}]`}</b> } { el.pageName }
                                </span>
                                <span className='hyper-navigation-tooltip-component'>
                                    {
                                        getComponentName(el.page.component)
                                    }
                                </span>
                            </li>
                        );
                    })
                }
            </ul>
        </span>
    );
};

export default PagesTooltip;