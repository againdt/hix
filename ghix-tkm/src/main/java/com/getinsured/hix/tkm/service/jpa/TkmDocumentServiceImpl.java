/**
 * 
 */
package com.getinsured.hix.tkm.service.jpa;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.TkmDocuments;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.tkm.repository.ITicketRepository;
import com.getinsured.hix.tkm.repository.ITkmDocumentsRepository;
import com.getinsured.hix.tkm.service.TkmDocumentService;
import com.getinsured.hix.tkm.tkmException.TKMException;

/**
 * @author hardas_d
 *
 */
@Service("TkmDocumentService")
public class TkmDocumentServiceImpl implements TkmDocumentService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TkmDocumentServiceImpl.class);


	@Autowired private ITkmDocumentsRepository iTkmDocumentsRepository;
	@Autowired private ITicketRepository iTicketRepository;

	@Override
	@Transactional
	public boolean saveDocument(Integer ticketId, Integer userId,List<String> docList, boolean bUpdate) {
		boolean bStatus = false;
		if(!iTicketRepository.exists(ticketId)) {
			return bStatus;
		}	
		
		List<TkmDocuments> documentList = null;
		List<String> existingDocs = new ArrayList<String>();
		TkmTickets tickets = null;
		
		documentList = iTkmDocumentsRepository.findByTicketId(ticketId);
		
		if((documentList != null) && (!documentList.isEmpty())) {
			//get ticket for many of the list item
			tickets = documentList.get(0).getTicket();
			for (TkmDocuments tkmDocuments : documentList) {
				existingDocs.add(tkmDocuments.getDocumentPath());
			}
		} else {
			documentList = new ArrayList<TkmDocuments>();
			
			tickets = new TkmTickets();
			tickets.setId(ticketId);			
		}
		
		//create a list of document object to save
		for (String documentPath : docList) {
			
			TkmDocuments tkmDocuments = new TkmDocuments();
			tkmDocuments.setTicket(tickets);
			tkmDocuments.setDocumentPath(documentPath);
			tkmDocuments.setUserId(userId);
			
			if(!existingDocs.contains(documentPath)) {
				documentList.add(tkmDocuments);
			}
		}
			bStatus = saveDocsInDB(documentList);
		
		return bStatus;
	}

	private boolean saveDocsInDB(List<TkmDocuments> documentList) {
		boolean bStatus = false;
		
		if(!documentList.isEmpty()) {
			try {
				List<TkmDocuments> savedDocs = iTkmDocumentsRepository.save(documentList);
				bStatus = (savedDocs.size() == documentList.size()) ? true : bStatus;
			} catch (Exception e) {
				LOGGER.info("Error in saving Documents in DB" ,e);
				bStatus = false;
			}
		} else {
			//No saving required. all docs already exists
			bStatus = true;
		}
		
		return bStatus;
	} 
	
	@Override
	@Transactional(readOnly=true)
	public List<TkmDocuments> getDocumentList(Integer ticketId) {
		List<TkmDocuments> docList = null;
			if(!iTicketRepository.exists(ticketId)) {
				return docList;
			}
			
			docList = iTkmDocumentsRepository.findByTicketId(ticketId);
	
		return docList;
	}
	
	@Override
	@Transactional(readOnly=true)
	public List<String> getDocumentPathList(Integer ticketId) {
		List<String> docList = null;
		if (!iTicketRepository.exists(ticketId)) {
			return docList;
		}
			docList = iTkmDocumentsRepository.findDocPathByTicketId(ticketId);
		
		return docList;
	}

	@Override
	@Transactional
	public boolean  deleteDocument(String docId) {
		
			iTkmDocumentsRepository.delete(Integer.valueOf(docId));
			return true;

	}


}
