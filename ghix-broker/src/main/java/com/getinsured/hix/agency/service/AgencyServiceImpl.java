package com.getinsured.hix.agency.service;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.agency.repository.IAgencyAudRepository;
import com.getinsured.hix.agency.repository.IAgencyDocumentRepository;
import com.getinsured.hix.agency.repository.IAgencyRepository;
import com.getinsured.hix.broker.repository.IBrokerRepository;
import com.getinsured.hix.broker.repository.IDesignateBrokerRepository;
import com.getinsured.hix.broker.service.BrokerIndTriggerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.broker.util.BrokerConstants;
import com.getinsured.hix.broker.util.BrokerUtils;
import com.getinsured.hix.dto.agency.AgencyBookOfBusinessDTO;
import com.getinsured.hix.dto.agency.AgencyBrokerListDTO;
import com.getinsured.hix.dto.agency.AgencyCertificationHistory;
import com.getinsured.hix.dto.agency.AgencyCertificationStatusDto;
import com.getinsured.hix.dto.agency.AgencyDelegationDTO;
import com.getinsured.hix.dto.agency.AgencyDelegationListDTO;
import com.getinsured.hix.dto.agency.AgencyDocumentDto;
import com.getinsured.hix.dto.agency.AgencyInformationDto;
import com.getinsured.hix.dto.agency.AgencyLocationDto;
import com.getinsured.hix.dto.agency.AgencyManagerProfileDto;
import com.getinsured.hix.dto.agency.IndividualDetailsDTO;
import com.getinsured.hix.dto.agency.SearchAgencyBookOfBusinessDTO;
import com.getinsured.hix.dto.agency.SearchAgencyDelegationDTO;
import com.getinsured.hix.dto.agency.SearchBrokerDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Broker.certification_status;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.DesignateBroker.Status;
import com.getinsured.hix.model.ExternalIndividual;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.agency.Agency;
import com.getinsured.hix.model.agency.Agency.CertificationStatus;
import com.getinsured.hix.model.agency.AgencyDocument;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;


@Service
public class AgencyServiceImpl implements AgencyService {
	
	private static final String HLT = "HLT";

	public static final Logger LOGGER = LoggerFactory.getLogger(AgencyServiceImpl.class);
	
	private static final String DATE_FORMAT = "MM-dd-yyyy";
	private static final String DATE_FORMAT_1 = "yyyy-MM-dd";
	private static final String DATE_FORMAT_2 = "MMM dd, yyyy";
	private static final int MAX_COMMENT_LENGTH = 4000;
	private static final String TARGET_NAME = "AGENCY";
	
	
	@Autowired
	private IAgencyRepository iAgencyRepository;
	@Autowired
	private UserService userService;
	@Autowired
	private IBrokerRepository iBrokerRepository;
	@Autowired
	private IAgencyAudRepository iAgencyAudRepository;
	@Autowired
	private IAgencyDocumentRepository iAgencyDocumentRepository;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired 
	private CommentTargetService commentTargetService;
	@Autowired
	private IDesignateBrokerRepository brokerDesignateRepository;
	@Autowired
	private BrokerIndTriggerService brokerIndTriggerService;
	@Autowired
	private DesignateService designateService;
	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public Agency saveOrUpdate(Agency agency) {
		return iAgencyRepository.save(agency);
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public Agency findById(Long agencyId) {
		return iAgencyRepository.findById(agencyId);
	}

	@Override
	public Page<Agency> findAll(Pageable pageable) {
		return iAgencyRepository.findAll(pageable);
	}
	
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void createModuleUser(Broker broker) {
		try {
			if (broker != null){
				AccountUser user = broker.getUser();
				if (null != user && userService.hasUserRole(user, RoleService.AGENCY_MANAGER)) {
					ModuleUser moduleUser = userService.findModuleUser(broker.getId(), ModuleUserService.AGENCY_MODULE,
							user);
					if (moduleUser == null) {
						userService.createModuleUser(broker.getId(), ModuleUserService.AGENCY_MODULE, user, true);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while creating Module user for agency :", e);
			throw new GIRuntimeException("Excpetion occured while creating module user for id: " + broker.getId(), e);
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public Agency saveAgencyInformation(Agency agency) { 
		Agency savedAgency = null;
		AccountUser accountUser = null;
		Broker broker = null;
		try {
			accountUser = userService.getLoggedInUser();
			//check agency id
			if(agency.getId() == null || agency.getId() == 0){
				//new record
				agency.setCertificationStatus(CertificationStatus.INCOMPLETE);
				if(accountUser != null){
					agency.setCreatedBy(Long.valueOf(accountUser.getId()));
				}
				savedAgency = saveOrUpdate(agency);
				broker = new Broker();
				broker.setLicenseNumber(agency.getLicenseNumber());
				broker.setFederalEIN(String.valueOf(agency.getFederalTaxId()));
				broker.setCompanyName(agency.getBusinessLegalName());
				broker.setAgencyId(savedAgency.getId());
				if(accountUser != null){
					broker.setCreatedBy(accountUser.getId());
					broker.setUser(accountUser);
				}
				broker.setCertificationStatus(certification_status.Incomplete.toString());
				broker = iBrokerRepository.save(broker);
				
			}else{
				//find agency record
				Agency agencyDb = findById(agency.getId());
				agencyDb.setAgencyName(agency.getAgencyName());
				agencyDb.setFederalTaxId(agency.getFederalTaxId());
				agencyDb.setLicenseNumber(agency.getLicenseNumber());
				agencyDb.setBusinessLegalName(agency.getBusinessLegalName());
				if(agencyDb.getCertificationStatus() == null){
					agencyDb.setCertificationStatus(CertificationStatus.INCOMPLETE);
				}
				savedAgency = saveOrUpdate(agencyDb);
				//update broker table with user id
				broker = iBrokerRepository.findByUserId(accountUser.getId());
				//if No user linked to Agent
//				if(broker.getUser() == null && accountUser != null){
//					broker.setUser(accountUser);
//					broker = iBrokerRepository.save(broker);
//				}
			}
			
			createModuleUser(broker);
			
			return savedAgency;
		} catch (Exception e) {
			LOGGER.error("Error while creating Module user for agency :" , e);
			throw new GIRuntimeException("Excpetion occured while creating module user for id: "+agency.getId(),e);
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public Agency findByBrokerId(int agentId) {
		return iAgencyRepository.findByBrokerId(agentId);
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveAgencyAgentCertificationStatus(Agency agency) { 
		Broker broker = null;
		try {
			AccountUser user = userService.getLoggedInUser();
			if(agency!= null && userService.hasUserRole(user, RoleService.AGENCY_MANAGER)){
				Agency agencyDb = findById(agency.getId());
				agencyDb.setCertificationStatus(agency.getCertificationStatus());
				agencyDb.setCertificationNumber(agency.getCertificationNumber());
				agencyDb.setCertificationDate(agency.getCertificationDate());
				agencyDb.setRecertifyDate(agency.getRecertifyDate());
				saveOrUpdate(agencyDb);
				
				if(CertificationStatus.PENDING.equals(agency.getCertificationStatus())){
					//update broker certification status
					broker = iBrokerRepository.findByUserId(userService.getLoggedInUser().getId());
					if(broker != null && broker.getCertificationStatus().toString().equalsIgnoreCase("Incomplete") ){
						broker.setCertificationStatus(certification_status.Pending.toString());
						broker.setApplicationDate(new TSDate());
						broker = iBrokerRepository.save(broker);
						
						/*if(BrokerUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
							PaymentMethods paymentMethods = brokerUtils.getPaymentMethodDetailsForBroker(broker.getId(), PaymentMethods.ModuleName.BROKER);
							LOGGER.info("Send Broker Details call starts");
							brokerIndTriggerService.triggerInd35(broker, paymentMethods);
							LOGGER.info("Send Broker Details call ends");
						}*/
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while changing certification status of Agency and Agent :" , e);
			throw new GIRuntimeException("Error while changing certification status of Agency and Agent : "+agency.getId(),e);
		}
	}
	
	@Override
	public AgencyCertificationStatusDto loadAgencyCertificationHistory(Long agencyId) {
		List<AgencyDocumentDto> documents;
		StringBuffer updatedByName;
		List<AgencyCertificationHistory> historyList = new LinkedList<>();
		AgencyCertificationStatusDto agencyCertificationStatusDto = new AgencyCertificationStatusDto();
		
		try {
			Agency agency = iAgencyRepository.findById(agencyId);
			if (agency.getCertificationStatus()!=null){
				agencyCertificationStatusDto.setCertificationStatus(agency.getCertificationStatus().getValue());
			}
			if (agency.getCertificationNumber() != null) {
				agencyCertificationStatusDto.setCertificationNumber(agency.getCertificationNumber().toString());
			}
			if(agency.getCertificationDate()!=null){
				SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
				agencyCertificationStatusDto.setCertificationDate(dateFormat.format(agency.getCertificationDate()));
			}
			if(agency.getCreationTimestamp()!=null){
				SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
				agencyCertificationStatusDto.setCreationDate(dateFormat.format(agency.getCreationTimestamp()));
			}
			if(agency.getCommentsId()!=null){
				//To Do once comment API is finalized
				agencyCertificationStatusDto.setComment("");
			}
			
			agencyCertificationStatusDto.setAgentCount(iBrokerRepository.getAgentCount(agencyId));
			
			List<Object[]> certificationHistory = iAgencyAudRepository.getAgencyCertificationHistory(agencyId);
			
			if(certificationHistory!=null && certificationHistory.size()>1){
				for(int i=1;i<=certificationHistory.size()-1;i++){
					Object[] currentElement = (Object[]) certificationHistory.get(i);
					Object[] previousElement = (Object[]) certificationHistory.get(i-1);
					if(!checkDuplicateStatus(currentElement, previousElement)){
						AgencyCertificationHistory agencyCertificationHistory = new AgencyCertificationHistory();
						
						if (currentElement[0] != null) {
							agencyCertificationHistory.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(currentElement[0].toString()));
						}
						if (currentElement[1] != null) {
							SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_1);
							agencyCertificationHistory.setCertificationDate(new SimpleDateFormat(DATE_FORMAT_2).format(sdf.parse(currentElement[1].toString())));
						}
						if (currentElement[2] != null) {
							agencyCertificationHistory.setPreviousStatus(Agency.CertificationStatus.valueOf(previousElement[2].toString()).getValue());
							agencyCertificationHistory.setNewStatus(Agency.CertificationStatus.valueOf(currentElement[2].toString()).getValue());
						}
						if (currentElement[3] != null) {
							agencyCertificationHistory.setComment(currentElement[3].toString());
						}
						updatedByName = new StringBuffer();
						if (currentElement[4] != null) {
							updatedByName.append(currentElement[4].toString()).append(" ");
						}
						if (currentElement[5] != null) {
							updatedByName.append(currentElement[5].toString());
						}
						if(updatedByName.length()!=0){
							agencyCertificationHistory.setUpdatedBy(updatedByName.toString());
						}
						documents = new ArrayList<>();
						if (currentElement[6] != null) {
							documents.add(setAgencyDocumentDTO(Long.valueOf(currentElement[6].toString())));
						}
						if (currentElement[7] != null) {
							documents.add(setAgencyDocumentDTO(Long.valueOf(currentElement[7].toString())));
						}
						if (currentElement[8] != null) {
							documents.add(setAgencyDocumentDTO(Long.valueOf(currentElement[8].toString())));
						}
						if(!documents.isEmpty()){
							agencyCertificationHistory.setDocuments(documents);
						}
						
						historyList.add(agencyCertificationHistory);
					}
				}
				Collections.reverse(historyList);
				agencyCertificationStatusDto.setAgencyCertificationHistory(historyList);
			}
			
			agencyCertificationStatusDto.setAgencyCertificationStatusList(getAgencyCertificationStatusList());
		} catch (Exception exception) {
			LOGGER.error("Exception while fetching agency certification history : " + exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} 
		
		return agencyCertificationStatusDto;
	}
	
	private AgencyDocumentDto setAgencyDocumentDTO(long documentId){
		AgencyDocument document = iAgencyDocumentRepository.findById(documentId);
		AgencyDocumentDto dto = new AgencyDocumentDto();
		dto.setDocumentId(ghixJasyptEncrytorUtil.encryptStringByJasypt(document.getId().toString()));
		dto.setDocumentName(document.getDocumentName());
		dto.setDocumentType(document.getDocumentType());
		
		return dto;
	}
	
	private boolean checkDuplicateStatus(Object[] currentElement, Object[] previousElement){
		boolean flag = false;
		if(currentElement[2].toString().equalsIgnoreCase(previousElement[2].toString())){
			if(currentElement[3]!=null || currentElement[6]!=null || currentElement[7]!=null || currentElement[8]!=null 
					|| previousElement[3]!=null || previousElement[6]!=null || previousElement[7]!=null || previousElement[8]!=null){
			} else {
				flag = true;
			}
		}
		return flag;
	}

	@Override
	public Comment saveComments(String targetId, String commentText) {
		List<Comment> comments = null;
		AccountUser accountUser = null;
		String commenterName = "";
		CommentTarget commentTarget = null;
		
		try {
			//CommentTarget commentTarget = commentTargetService.findByTargetIdAndTargetType(targetId, TargetName.valueOf(TARGET_NAME));
			if(commentTarget == null){
				commentTarget =  new CommentTarget();
				//commentTarget.setTargetId(targetId);
				commentTarget.setTargetName(TargetName.valueOf(TARGET_NAME));
			}
			
			if(commentTarget.getComments() == null || commentTarget.getComments().size() == 0 ){
				comments = new ArrayList<Comment>();
			}
			else{
				comments = commentTarget.getComments();
			}

			accountUser = userService.getLoggedInUser();
				
			String firstName = accountUser.getFirstName();
			String lastName  = accountUser.getLastName();
			commenterName += (StringUtils.isBlank(firstName)) ? "" :  firstName+" ";
			commenterName += (StringUtils.isBlank(lastName)) ? "" : lastName;
			
			//Checking if user's name is blank
			if(StringUtils.isBlank(commenterName)){
				commenterName = "Anonymous User";
			}
			
			Comment comment = new Comment();
			comment.setComentedBy(accountUser.getId());
			comment.setCommenterName(commenterName);
			
			//Fetching first 4000 char for comment text;
			int beginIndex = 0;
			int endIndex = commentText.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  commentText.length();
			commentText = commentText.substring(beginIndex, endIndex);
			comment.setComment(commentText);
			
			comment.setCommentTarget(commentTarget);
			
			comments.add(comment);
			commentTarget.setComments(comments);
			commentTarget = commentTargetService.saveCommentTarget(commentTarget);
			List<Comment> list = commentTarget.getComments();
			Collections.sort(list);
			
			return list.get(0);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while saving agency comments.", ex);
			throw new GIRuntimeException("Exception occured while saving agency comments.", ex);
		}
	}
	
	private List<String> getAgencyCertificationStatusList() {
		List<String> statusList = new ArrayList<String>();
		statusList.add("Pending");
		statusList.add("Suspended");
		statusList.add("Certified");
		statusList.add("Terminated");
		return statusList;
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public Agency updateAgencyCertificationStatus(Agency agency) { 
		
		try {
			return saveOrUpdate(agency);
		} catch (Exception exception) {
			LOGGER.error("Exception while updating agency certification status : " + exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} 
	}

	@Override
	public String approveRequest(int brokerId, int individualId, AccountUser user) { 
		EntityResponseDTO entityResponseDTO = null;
		String status = BrokerConstants.RESPONSE_FAILURE;
		
		try {
			DesignateBroker designateBroker = brokerDesignateRepository.findDesigBrokerByAgentAndIndividualAndStatus(brokerId, individualId, DesignateBroker.Status.Pending);
			
			if (designateBroker != null) {
				designateBroker.setStatus(Status.Active);
				designateBroker.setUpdated(new TSDate());
				designateBroker.setUpdatedBy(user.getId());
				
				if(BrokerUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					synchronized (this) {
						LOGGER.warn("Designate broker flow by agency manager");
						DesignateBroker dBinDB= brokerDesignateRepository.findDesigBrokerByAgentAndIndividualAndStatus(designateBroker.getBrokerId(), designateBroker.getExternalIndividualId(), designateBroker.getStatus()) ;
					    LOGGER.warn("Designate broker record exists: "+ dBinDB);
					    if (   dBinDB == null  ) {
						     entityResponseDTO = brokerIndTriggerService.triggerInd47(designateBroker);
						     LOGGER.warn("Triggered IND47 response: "+ entityResponseDTO);
						     LOGGER.warn("IND47 response code: "+ entityResponseDTO.getResponseCode() + ", Status: "+entityResponseDTO.getStatus());
	
						     if(entityResponseDTO!=null && (entityResponseDTO.getResponseCode()==200 || BrokerConstants.RESPONSE_SUCCESS.equalsIgnoreCase(entityResponseDTO.getStatus()))){
							      LOGGER.warn("Pre updating Designate broker ...");
							      brokerDesignateRepository.save(designateBroker);
							      LOGGER.warn("Post updating Designate broker...");
							      Broker broker = iBrokerRepository.findById(designateBroker.getBrokerId());
								  if(broker != null){
									designateService.updateEnrollmentDetails(broker, BrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL, individualId, BrokerConstants.AGENT_ROLE, BrokerConstants.ADDACTIONFLAG, user.getUsername());
								  }
								  status =  BrokerConstants.RESPONSE_SUCCESS;
						     }
						     LOGGER.warn("saveDesignateBroker: CA->"+ entityResponseDTO.getStatus());
					    }
					}
				}else {
					brokerDesignateRepository.save(designateBroker);
					status =  BrokerConstants.RESPONSE_SUCCESS;
				}
			}
		} catch (Exception exception) {
			LOGGER.error("Exception while approving Individual Request : " + exception);
		} 
		
		return status;
	}
	
	@Override
	public AgencyDelegationListDTO findAllDelegations(SearchAgencyDelegationDTO searchAgencyDelegationDTO) {

		AgencyDelegationListDTO agencyDelegationListDTO = new AgencyDelegationListDTO();
		EntityManager localEMOne= null;
		EntityManager localEMTwo = null;
		try {
			CriteriaBuilder cb = entityManagerFactory.getCriteriaBuilder();

			CriteriaQuery criteriaQuery = cb.createQuery();

			Root<Broker> brokers = criteriaQuery.from(Broker.class);
			Root<DesignateBroker> designateBroker = criteriaQuery.from(DesignateBroker.class);
			Root<ExternalIndividual> externalIndividual = criteriaQuery.from(ExternalIndividual.class);

			Join user = brokers.join("user", JoinType.LEFT);
			// user.on(cb.equal(brokers.get("user") ));

			criteriaQuery.multiselect(
					cb.selectCase().when(cb.isNull(user.get("firstName")), brokers.get("firstName"))
							.otherwise(user.get("firstName")),
					cb.selectCase().when(cb.isNull(user.get("lastName")), brokers.get("lastName"))
							.otherwise(user.get("lastName")),
					brokers.get("id"), designateBroker.get("created"), externalIndividual.get("id"),
					externalIndividual.get("firstName"), externalIndividual.get("lastName"));

			final List<Predicate> predicates = new ArrayList<Predicate>();
			predicates.add(cb.equal(designateBroker.get("externalIndividualId"), externalIndividual.get("id")));// Join on
																												// designateBroker
																												// externalIndividual
			predicates.add(cb.equal(brokers.get("id"), designateBroker.get("brokerId")));// Join on broker designatebroker
			predicates.add(cb.equal(brokers.get("agencyId"), searchAgencyDelegationDTO.getAgencyId()));// Search for brokers
																										// under given
																										// agency.

			predicates.add(cb.equal(designateBroker.get("status"),
					Status.valueOf(searchAgencyDelegationDTO.getDesignationStatus())));// Search for only those individuals
																						// which has pending or inactive
																						// status.

			if (null != searchAgencyDelegationDTO.getAgentFirstName()) {
				predicates.add(cb.or(
						cb.like(cb.upper(brokers.get("firstName")),
								"%" + searchAgencyDelegationDTO.getAgentFirstName().toUpperCase().trim() + "%"),
						cb.like(cb.upper(user.get("firstName")),
								"%" + searchAgencyDelegationDTO.getAgentFirstName().toUpperCase().trim() + "%")));
			}
			if (null != searchAgencyDelegationDTO.getAgentLastName()) {
				predicates.add(cb.or(
						cb.like(cb.upper(brokers.get("lastName")),
								"%" + searchAgencyDelegationDTO.getAgentLastName().toUpperCase().trim() + "%"),
						cb.like(cb.upper(user.get("lastName")),
								"%" + searchAgencyDelegationDTO.getAgentLastName().toUpperCase().trim() + "%")));
			}

			if (null != searchAgencyDelegationDTO.getIndividualFirstName()) {
				predicates.add(cb.like(cb.upper(externalIndividual.get("firstName")),
						"%" + searchAgencyDelegationDTO.getIndividualFirstName().toUpperCase().trim() + "%"));// External
																												// Individual
																												// First-Name
			}
			if (null != searchAgencyDelegationDTO.getIndividualLastName()) {
				predicates.add(cb.like(cb.upper(externalIndividual.get("lastName")),
						"%" + searchAgencyDelegationDTO.getIndividualLastName().toUpperCase().trim() + "%"));// External
																												// Individual
																												// Last-Name
			}

			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			if (null != searchAgencyDelegationDTO.getFromDate()) {
				predicates.add(cb.greaterThanOrEqualTo(designateBroker.get("created"),
						df.parse(searchAgencyDelegationDTO.getFromDate() + " 00:00:00")));//
			}
			if (null != searchAgencyDelegationDTO.getToDate()) {
				predicates.add(cb.lessThanOrEqualTo(designateBroker.get("created"),
						df.parse(searchAgencyDelegationDTO.getToDate() + " 23:59:59")));//
			}

			criteriaQuery.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

			if("desc".equalsIgnoreCase( searchAgencyDelegationDTO.getSortOrder())) {
				if("individualFirstName".equalsIgnoreCase( searchAgencyDelegationDTO.getSortBy())) {
					criteriaQuery.orderBy(cb.desc( externalIndividual.get("firstName")  )   );
				}else if("individualLastName".equalsIgnoreCase( searchAgencyDelegationDTO.getSortBy())) {
					criteriaQuery.orderBy(cb.desc(  externalIndividual.get("lastName")  )   );
				}else if("agentFirstName".equalsIgnoreCase( searchAgencyDelegationDTO.getSortBy())) {
					criteriaQuery.orderBy(cb.desc( cb.selectCase().when(cb.isNull(user.get("firstName")), brokers.get("firstName"))
					.otherwise(user.get("firstName"))  )   );
				}else if("agentLastName".equalsIgnoreCase( searchAgencyDelegationDTO.getSortBy())) {
					criteriaQuery.orderBy(cb.desc( cb.selectCase().when(cb.isNull(user.get("lastName")), brokers.get("lastName"))
					.otherwise(user.get("lastName"))  )   );
				}else {
					criteriaQuery.orderBy(cb.desc(  designateBroker.get("created")  )   );
				}
			}else {
				if("individualFirstName".equalsIgnoreCase( searchAgencyDelegationDTO.getSortBy())) {
					criteriaQuery.orderBy(cb.asc( externalIndividual.get("firstName")  ));
				}else if("individualLastName".equalsIgnoreCase( searchAgencyDelegationDTO.getSortBy())) {
					criteriaQuery.orderBy(cb.asc(  externalIndividual.get("lastName")  ));
				}else if("agentFirstName".equalsIgnoreCase( searchAgencyDelegationDTO.getSortBy())) {
					criteriaQuery.orderBy(cb.asc( cb.selectCase().when(cb.isNull(user.get("firstName")), brokers.get("firstName"))
					.otherwise(user.get("firstName"))  ));
				}else if("agentLastName".equalsIgnoreCase( searchAgencyDelegationDTO.getSortBy())) {
					criteriaQuery.orderBy(cb.asc( cb.selectCase().when(cb.isNull(user.get("lastName")), brokers.get("lastName"))
					.otherwise(user.get("lastName"))  ));
				}else {
					criteriaQuery.orderBy(cb.asc(  designateBroker.get("created")  ));
				}
			}

			localEMOne= entityManagerFactory.createEntityManager();
			TypedQuery<Object[]> typedQuery = localEMOne.createQuery(criteriaQuery);

			if (null != searchAgencyDelegationDTO.getStartPosition()) {
				typedQuery.setFirstResult(searchAgencyDelegationDTO.getStartPosition());
				if (null != searchAgencyDelegationDTO.getPageSize()
						&& searchAgencyDelegationDTO.getPageSize().intValue() > 0) {
					typedQuery.setMaxResults(searchAgencyDelegationDTO.getPageSize());
				}
			}

			List<Object[]> results = typedQuery.getResultList(); 
			List<AgencyDelegationDTO> delegations = new ArrayList<AgencyDelegationDTO>();
			AgencyDelegationDTO tmpAgencyDelegationDTO;
			for (Object[] result : results) {
				tmpAgencyDelegationDTO = new AgencyDelegationDTO();
				
				tmpAgencyDelegationDTO.setAgentFirstName((String) result[0]);
				tmpAgencyDelegationDTO.setAgentLastName((String) result[1]);

				tmpAgencyDelegationDTO.setBrokerId( ghixJasyptEncrytorUtil.encryptStringByJasypt( Integer.toString( (int) result[2])     )   );
				
				tmpAgencyDelegationDTO.setReceivedOn( df.format( (Date)result[3])  );
				
				tmpAgencyDelegationDTO.setIndividualId( ghixJasyptEncrytorUtil.encryptStringByJasypt( Long.toString( (long) result[4])     )   );
				
				tmpAgencyDelegationDTO.setIndividualFirstName((String) result[5]);
				
				tmpAgencyDelegationDTO.setIndividualLastName((String) result[6]);
				
				
				delegations.add(tmpAgencyDelegationDTO);
			}
			agencyDelegationListDTO.setDelegations(delegations);

			criteriaQuery.select(cb.count(designateBroker));
			localEMTwo= entityManagerFactory.createEntityManager();
			Object resultCount = localEMTwo.createQuery(criteriaQuery).getSingleResult();
			agencyDelegationListDTO.setCount((long) resultCount);
			
			
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching delegation list for agency.", e);
			throw new GIRuntimeException("Exception occured while fetching delegation list for agency.", e);
		}finally {
			if(localEMOne != null) {
				localEMOne.close();
			}
			if(null != localEMTwo) {
				localEMTwo.close();
			}
			
		}
		

		return agencyDelegationListDTO;
	}

	@Override
	public AgencyBookOfBusinessDTO findAllIndividuals(SearchAgencyBookOfBusinessDTO searchAgencyBookOfBusinessDTO) {
		AgencyBookOfBusinessDTO responseDTO =  null;
		
		try {
			if(StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getCaseNumber()) || StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getApplicationYear()) || StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getIssuer())){
				responseDTO = findIndividualsWithEnrollmentAndIssuerSearch(searchAgencyBookOfBusinessDTO);
			} else {
				responseDTO = findIndividualsWithoutEnrollmentAndIssuerSearch(searchAgencyBookOfBusinessDTO);
			}
			
			responseDTO.setPageNumber(searchAgencyBookOfBusinessDTO.getPageNumber());
			
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching Agency BOB-Individuals-Enrollment-Plans.", e);
			throw new GIRuntimeException("Exception occured while fetching Agency BOB-Individuals-Enrollment-Plans.", e);
		}
		
		return responseDTO;
	}
	
	private AgencyBookOfBusinessDTO findIndividualsWithEnrollmentAndIssuerSearch(SearchAgencyBookOfBusinessDTO searchAgencyBookOfBusinessDTO) {
		AgencyBookOfBusinessDTO responseDTO =  new AgencyBookOfBusinessDTO();
		List<IndividualDetailsDTO> listOfIndividuals =  new ArrayList<>();
		IndividualDetailsDTO individualDTO;
		StringBuilder queryString = new StringBuilder();
		EntityManager em = null;
		
		try {
			queryString.append(" SELECT EI.INDIVIDUAL_CASE_ID,  EI.FIRSTNAME, EI.LASTNAME, EI.PRI_PHONE, EI.EMAILID,  EI.ADDRESSLINE1, EI.ADDRESSLINE2, EI.CITY,  EI.STATE, EI.ZIP, ");
			queryString.append(" 	( CASE  WHEN B.USERID IS NOT NULL  THEN U.FIRST_NAME  ELSE B.FIRST_NAME END) AS BROKER_FIRST_NAME,");
			queryString.append(" 	(  CASE  WHEN B.USERID IS NOT NULL  THEN U.LAST_NAME  ELSE B.LAST_NAME  END) AS BROKER_LAST_NAME, ");
			queryString.append(" 	B.LICENSE_NUMBER, en.id as enrollment_id, en.EXT_HOUSEHOLD_CASE_ID,  TO_CHAR(en.benefit_effective_date, 'YYYY') AS APPLICATION_YEAR , en.gross_premium_amt ,  en.PLAN_ID , enrollee.PERSON_TYPE_LKP ,  i.name, ");
			queryString.append(" 	D.BROKERID, D.STATUS, en.CMS_PLAN_ID ");
			queryString.append(" FROM EXTERNAL_INDIVIDUAL EI INNER JOIN DESIGNATE_BROKER D ON EI.INDIVIDUAL_CASE_ID = D.EXTERNALINDIVIDUALID ");
			
			if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getFirstName())) {
				queryString.append(" AND UPPER(EI.FIRSTNAME) LIKE UPPER(:firstName)");
			}
			
			if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getLastName())) {
				queryString.append(" AND UPPER(EI.LASTNAME) LIKE UPPER(:lastName)");
			}
			
			queryString.append(" INNER JOIN BROKERS B ON B.ID = D.BROKERID AND D.STATUS ='Active' ");
			if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getAgentLicenseNumber())) {
				queryString.append(" AND B.LICENSE_NUMBER = :licenseNumber");
			}
			
			queryString.append(" INNER JOIN USERS U ON U.ID = B.USERID AND B.AGENCY_ID = :agencyId");
			if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getAgentFirstName())) {
				queryString.append(" AND UPPER(U.FIRST_NAME) LIKE UPPER(:brokerFirstName) OR  B.FIRST_NAME  LIKE UPPER(:brokerFirstName)");
			}
			
			if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getAgentLastName())) {
				queryString.append(" AND UPPER(U.LAST_NAME) LIKE UPPER(:brokerLastName) OR  B.LAST_NAME  LIKE UPPER(:brokerLastName)");
			}
			
			queryString.append(" 	LEFT OUTER JOIN enrollment en ON en.EXCHG_SUBSCRIBER_IDENTIFIER = EI.INDIVIDUAL_CASE_ID and en.BENEFIT_END_DATE >= sysdate ");
			queryString.append( " 	LEFT OUTER JOIN ENROLLEE enrollee ON en.id    = enrollee.enrollment_id ");
			queryString.append(" 		AND enrollee.PERSON_TYPE_LKP IN (SELECT LOOKUP_VALUE_ID  FROM LOOKUP_VALUE  WHERE LOOKUP_VALUE_CODE IN 'SUBSCRIBER') ");
			queryString.append(" 		AND (enrollee.ENROLLEE_STATUS_LKP in (SELECT lookup_value_id FROM lookup_value WHERE lookup_value_code IN ('PENDING','CONFIRM')) ");
			queryString.append(" 				OR (enrollee.ENROLLEE_STATUS_LKP in (SELECT lookup_value_id FROM lookup_value WHERE lookup_value_code IN('TERM')) AND EFFECTIVE_END_DATE >= sysdate)	)");
			queryString.append(" 	LEFT OUTER JOIN PLAN p ON en.plan_id = p.id "); 
			queryString.append(" 	LEFT OUTER JOIN ISSUERS i ON p.ISSUER_ID = i.id where ");
			
			if(StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getApplicationYear())) {
				queryString.append(" TO_CHAR(en.benefit_effective_date, 'YYYY') = :effectiveDate ");
			}
			if(StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getCaseNumber())) {
				if(StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getApplicationYear())) {
					queryString.append(" AND ");
				}
				queryString.append(" 	en.EXT_HOUSEHOLD_CASE_ID = :caseNumber ");
			}
			if(StringUtils.isNoneEmpty(searchAgencyBookOfBusinessDTO.getIssuer())) {
				if(StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getApplicationYear()) || StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getCaseNumber()) ) {
					queryString.append(" AND ");
				}
				queryString.append(" 	UPPER(i.name) LIKE UPPER(:issuerName) ");
			}
			
			if( null != searchAgencyBookOfBusinessDTO.getSortBy() && !searchAgencyBookOfBusinessDTO.getSortBy().trim().isEmpty()) {
				queryString.append(" ORDER BY "+searchAgencyBookOfBusinessDTO.getSortBy()+" "+searchAgencyBookOfBusinessDTO.getSortOrder());
			}
			
			em = entityManagerFactory.createEntityManager();
			Query query = setParameterValue(em, searchAgencyBookOfBusinessDTO, queryString);
			
			query.setFirstResult((searchAgencyBookOfBusinessDTO.getPageNumber()-1)*10);
			query.setMaxResults(10);
			
			List<Object[]> resultList = query.getResultList();
			
			for (Object[] data : resultList) {
				individualDTO = new IndividualDetailsDTO();
				
				if(null != data[0]) {
					individualDTO.setIndividualId(ghixJasyptEncrytorUtil.encryptStringByJasypt(data[0].toString()));	
				}
				if(null != data[1]) {
					individualDTO.setFirstName(data[1].toString());
				}
				if(null != data[2]) {
					individualDTO.setLastName(data[2].toString());
				}
				if(null != data[3]) {
					individualDTO.setPhoneNumber(data[3].toString());
				}
				if(null != data[4]) {
					individualDTO.setEmailAddress(data[4].toString());
				}
				if(null != data[5]) {
					individualDTO.setAddress1(data[5].toString());
				}
				if(null != data[6]) {
					individualDTO.setAddress2(data[6].toString());
				}
				if(null != data[7]) {
					individualDTO.setCity(data[7].toString());
				}
				if(null != data[8]) {
					individualDTO.setState(data[8].toString());
				}
				if(null != data[9]) {
					individualDTO.setZipCode(data[9].toString());
				}
				if(null != data[10]) {
					individualDTO.setAgentFirstName(data[10].toString());
				}
				if(null != data[11]) {
					individualDTO.setAgentLastName(data[11].toString());
				}
				if(null != data[12]) {
					individualDTO.setAgentLicenseNumber(data[12].toString());
				}
				if(null != data[13]) {
					individualDTO.setEnrollmentId(ghixJasyptEncrytorUtil.encryptStringByJasypt(data[13].toString()));
				}
				if(null != data[14]) {
					individualDTO.setCaseNumber(data[14].toString());
				}
				if(null != data[15]) {
					individualDTO.setApplicationYear(data[15].toString());
				}
				if(null != data[16]) {
					individualDTO.setPremium(data[16].toString());
				}
				if(null != data[17]) {
					individualDTO.setPlanId(ghixJasyptEncrytorUtil.encryptStringByJasypt(data[17].toString()));
				}
				if(null != data[20]) {
					individualDTO.setBrokerId(ghixJasyptEncrytorUtil.encryptStringByJasypt(data[20].toString()));
				}
				if(null != data[21]) {
					individualDTO.setStatus(data[21].toString());
				}
				if(null != data[22]) {
					individualDTO.setIssuerHiosId(data[22].toString().substring(0, 5));
				}
				
				listOfIndividuals.add(individualDTO);
			}
			responseDTO.setListOfIndividuals(listOfIndividuals);
			responseDTO.setCount(findIndividualsCountWithEnrollmentAndIssuerSearch(searchAgencyBookOfBusinessDTO));
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching Agency BOB-Individuals-Enrollment-Plans.", e);
			throw new GIRuntimeException("Exception occured while fetching Agency BOB-Individuals-Enrollment-Plans.", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		
		return responseDTO;
	}
	
	private int findIndividualsCountWithEnrollmentAndIssuerSearch(SearchAgencyBookOfBusinessDTO searchAgencyBookOfBusinessDTO){
		StringBuilder queryString = new StringBuilder();
		EntityManager em = null;
		
		try {
			queryString.append(" SELECT count(*) FROM EXTERNAL_INDIVIDUAL EI INNER JOIN DESIGNATE_BROKER D ON EI.INDIVIDUAL_CASE_ID = D.EXTERNALINDIVIDUALID ");
			
			if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getFirstName())) {
				queryString.append(" AND UPPER(EI.FIRSTNAME) LIKE UPPER(:firstName)");
			}
			
			if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getLastName())) {
				queryString.append(" AND UPPER(EI.LASTNAME) LIKE UPPER(:lastName)");
			}
			
			queryString.append(" 	INNER JOIN BROKERS B ON B.ID      = D.BROKERID AND D.STATUS ='Active' ");
			if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getAgentLicenseNumber())) {
				queryString.append(" AND B.LICENSE_NUMBER = :licenseNumber");
			}
			
			queryString.append(" 	INNER JOIN USERS U ON U.ID = B.USERID AND B.AGENCY_ID = :agencyId");
			if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getAgentFirstName())) {
				queryString.append(" AND UPPER(U.FIRST_NAME) LIKE UPPER(:brokerFirstName) OR  B.FIRST_NAME  LIKE UPPER(:brokerFirstName)");
			}
			
			if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getAgentLastName())) {
				queryString.append(" AND UPPER(U.LAST_NAME) LIKE UPPER(:brokerLastName) OR  B.LAST_NAME  LIKE UPPER(:brokerLastName)");
			}
			
			queryString.append(" 	LEFT OUTER JOIN enrollment en ON en.EXCHG_SUBSCRIBER_IDENTIFIER = EI.INDIVIDUAL_CASE_ID and en.BENEFIT_END_DATE >= sysdate ");
			queryString.append( " 	LEFT OUTER JOIN ENROLLEE enrollee ON en.id    = enrollee.enrollment_id ");
			queryString.append(" 		AND enrollee.PERSON_TYPE_LKP IN (SELECT LOOKUP_VALUE_ID  FROM LOOKUP_VALUE  WHERE LOOKUP_VALUE_CODE IN 'SUBSCRIBER') ");
			queryString.append(" 		AND (enrollee.ENROLLEE_STATUS_LKP in (SELECT lookup_value_id FROM lookup_value WHERE lookup_value_code IN ('PENDING','CONFIRM')) ");
			queryString.append(" 				OR (enrollee.ENROLLEE_STATUS_LKP in (SELECT lookup_value_id FROM lookup_value WHERE lookup_value_code IN('TERM')) AND EFFECTIVE_END_DATE >= sysdate)	)");
			queryString.append(" 	LEFT OUTER JOIN PLAN p ON en.plan_id = p.id "); 
			queryString.append(" 	LEFT OUTER JOIN ISSUERS i ON p.ISSUER_ID = i.id where ");
			
			if(StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getApplicationYear())) {
				queryString.append(" TO_CHAR(en.benefit_effective_date, 'YYYY') = :effectiveDate ");
			}
			if(StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getCaseNumber())) {
				if(StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getApplicationYear())) {
					queryString.append(" 	AND " );
				}
				queryString.append(" 	en.EXT_HOUSEHOLD_CASE_ID = :caseNumber ");
			}
			if(StringUtils.isNoneEmpty(searchAgencyBookOfBusinessDTO.getIssuer()) ) {
				if(StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getApplicationYear())  || StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getCaseNumber())  ) {
					queryString.append(" 	AND " );
				}
				queryString.append(" 	 UPPER(i.name) LIKE UPPER(:issuerName) ");
			}
			
			em = entityManagerFactory.createEntityManager();
			Query query = setParameterValue(em, searchAgencyBookOfBusinessDTO, queryString);
			
			Object resultList = query.getSingleResult();
			return Integer.valueOf(resultList.toString());
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching Individuals count with enrollment or issuer search.", e);
			throw new GIRuntimeException("Exception occured while fetching Individuals count with enrollment or issuer search.", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
	}
	
	private AgencyBookOfBusinessDTO findIndividualsWithoutEnrollmentAndIssuerSearch(SearchAgencyBookOfBusinessDTO searchAgencyBookOfBusinessDTO) {
		StringBuilder queryString = new StringBuilder();
		AgencyBookOfBusinessDTO responseDTO =  new AgencyBookOfBusinessDTO();
		IndividualDetailsDTO individualDTO;
		Set<String> individuals = new HashSet<>();
		EntityManager em = null;
		List<IndividualDetailsDTO> listOfIndividuals = new ArrayList<>();
		
		try {
			queryString.append(" SELECT EI.INDIVIDUAL_CASE_ID, EI.FIRSTNAME, EI.LASTNAME, EI.PRI_PHONE, EI.EMAILID, EI.ADDRESSLINE1, EI.ADDRESSLINE2, "
					+ " 	EI.CITY, EI.STATE, EI.ZIP,(CASE WHEN B.USERID is not null THEN U.FIRST_NAME ELSE B.FIRST_NAME END) as BROKER_FIRST_NAME,"
					+ " 	(CASE WHEN B.USERID is not null THEN U.LAST_NAME ELSE B.LAST_NAME END) as BROKER_LAST_NAME,B.LICENSE_NUMBER, D.BROKERID, D.STATUS "
					+ " FROM EXTERNAL_INDIVIDUAL EI , DESIGNATE_BROKER D, BROKERS B, USERS U "
					+ " WHERE 	EI.INDIVIDUAL_CASE_ID = D.EXTERNALINDIVIDUALID   "
					+ " 		AND B.ID = D.BROKERID AND D.STATUS ='Active' and  U.ID = B.USERID AND B.AGENCY_ID = :agencyId ");
			
			
			setParameter(queryString, searchAgencyBookOfBusinessDTO);
			
			em = entityManagerFactory.createEntityManager();
			Query query = setParameterValue(em, searchAgencyBookOfBusinessDTO, queryString);
			
			query.setFirstResult((searchAgencyBookOfBusinessDTO.getPageNumber()-1)*10);
			query.setMaxResults(10);
			
			List<Object[]> resultList = query.getResultList();
			for (Object[] data : resultList) {
				individualDTO = new IndividualDetailsDTO();
				
				if(null != data[0]) {
					individualDTO.setIndividualId(ghixJasyptEncrytorUtil.encryptStringByJasypt(data[0].toString()));
					individuals.add(data[0].toString());
				}
				if(null != data[1]) {
					individualDTO.setFirstName(data[1].toString());
				}
				if(null != data[2]) {
					individualDTO.setLastName(data[2].toString());
				}
				if(null != data[3]) {
					individualDTO.setPhoneNumber(data[3].toString());
				}
				if(null != data[4]) {
					individualDTO.setEmailAddress(data[4].toString());
				}
				if(null != data[5]) {
					individualDTO.setAddress1(data[5].toString());
				}
				if(null != data[6]) {
					individualDTO.setAddress2(data[6].toString());
				}
				if(null != data[7]) {
					individualDTO.setCity(data[7].toString());
				}
				if(null != data[8]) {
					individualDTO.setState(data[8].toString());
				}
				if(null != data[9]) {
					individualDTO.setZipCode(data[9].toString());
				}
				if(null != data[10]) {
					individualDTO.setAgentFirstName(data[10].toString());
				}
				if(null != data[11]) {
					individualDTO.setAgentLastName(data[11].toString());
				}
				if(null != data[12]) {
					individualDTO.setAgentLicenseNumber(data[12].toString());
				}
				if(null != data[13]) {
					individualDTO.setBrokerId(ghixJasyptEncrytorUtil.encryptStringByJasypt(data[13].toString()));
				}
				if(null != data[14]) {
					individualDTO.setStatus(data[14].toString());
				}
				
				listOfIndividuals.add(individualDTO);
			}

			responseDTO.setListOfIndividuals(findIndividualEnrollments(individuals, listOfIndividuals));
			responseDTO.setCount(findIndividualsCountWithoutEnrollmentAndIssuerSearch(searchAgencyBookOfBusinessDTO));
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching Agency BOB-Individuals.", e);
			throw new GIRuntimeException("Exception occured while fetching Agency BOB-Individuals.", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		
		return responseDTO;
	}
	
	private int findIndividualsCountWithoutEnrollmentAndIssuerSearch(SearchAgencyBookOfBusinessDTO searchAgencyBookOfBusinessDTO){
		StringBuilder queryString = new StringBuilder();
		EntityManager em = null;
		
		try {
			queryString.append(" SELECT count(*) FROM EXTERNAL_INDIVIDUAL EI , DESIGNATE_BROKER D, BROKERS B,USERS U "
					+ " where EI.INDIVIDUAL_CASE_ID = D.EXTERNALINDIVIDUALID and  "
					+ " B.ID = D.BROKERID AND D.STATUS ='Active' and  U.ID = B.USERID AND B.AGENCY_ID = :agencyId ");
			
			setParameter(queryString, searchAgencyBookOfBusinessDTO);
			
			em = entityManagerFactory.createEntityManager();
			Query query = setParameterValue(em, searchAgencyBookOfBusinessDTO, queryString);
			
			Object resultList = query.getSingleResult();
			return Integer.valueOf(resultList.toString());
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching Individuals count with enrollment or issuer search.", e);
			throw new GIRuntimeException("Exception occured while fetching Individuals count with enrollment or issuer search.", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
	}

	private List<IndividualDetailsDTO> findIndividualEnrollments(Set<String> individuals, List<IndividualDetailsDTO> listOfIndividuals){
		StringBuilder queryString = new StringBuilder();
		EntityManager em = null;
		
		try {
			if(listOfIndividuals!=null && !listOfIndividuals.isEmpty()){
				queryString.append(" SELECT en.EXCHG_SUBSCRIBER_IDENTIFIER, en.id, en.EXT_HOUSEHOLD_CASE_ID, TO_CHAR(en.benefit_effective_date, 'YYYY') AS APPLICATION_YEAR, en.gross_premium_amt, en.PLAN_ID, enrollee.PERSON_TYPE_LKP, i.name, en.CMS_PLAN_ID, etlkup.LOOKUP_VALUE_CODE "
						+ "from  enrollment en "
						+ "INNER JOIN ENROLLEE enrollee on en.id = enrollee.enrollment_id "
						+ "INNER JOIN LOOKUP_VALUE lkup on enrollee.PERSON_TYPE_LKP= lkup.LOOKUP_VALUE_ID AND lkup.LOOKUP_VALUE_CODE='SUBSCRIBER' "
						+ "INNER JOIN  PLAN p on en.plan_id = p.id "
						+ "INNER JOIN  ISSUERS i on p.ISSUER_ID = i.id "
						+ "INNER JOIN LOOKUP_VALUE etlkup on en.INSURANCE_TYPE_LKP= etlkup.LOOKUP_VALUE_ID "
						+ "where en.EXCHG_SUBSCRIBER_IDENTIFIER in (:listOfIndividualIds) and en.BENEFIT_END_DATE >= sysdate ");
				
				em = entityManagerFactory.createEntityManager();  
				Query query = em.createNativeQuery(queryString.toString());
				query.setParameter("listOfIndividualIds", individuals);
				
				List<Object[]> resultList = query.getResultList();
				
				Map<String,LinkedList<Object[]>> map= new HashMap<String,LinkedList<Object[]>>();
				for (Object[] data : resultList) {
					LinkedList<Object[]> list =  map.get(data[0].toString());
					if(list == null) {
						list = new LinkedList<Object[]>();
						list.add(data);
						map.put(data[0].toString(), list);
					}else {
						if(HLT.equals(data[9])){
							list.addFirst(data);
						}else {
							list.addLast(data);
						}
					}
				}
				
				
				
				if(resultList!=null && !resultList.isEmpty()){
					for(IndividualDetailsDTO individual : listOfIndividuals){
						Object[] data = findIndividualEnrollment(ghixJasyptEncrytorUtil.decryptStringByJasypt(individual.getIndividualId()), map);
						if(data!=null){
							if(null != data[1]) {
								individual.setEnrollmentId(ghixJasyptEncrytorUtil.encryptStringByJasypt(data[1].toString()));
							}
							if(null != data[2]) {
								individual.setCaseNumber(data[2].toString());
							}
							if(null != data[3]) {
								individual.setApplicationYear(data[3].toString());
							}
							if(null != data[4]) {
								individual.setPremium(data[4].toString());
							}
							if(null != data[5]) {
								individual.setPlanId(ghixJasyptEncrytorUtil.encryptStringByJasypt(data[5].toString()));
							}
							if(null != data[8]) {
								individual.setIssuerHiosId(data[8].toString().substring(0, 5));
							}
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching Agency BOB-Individuals-Enrollment.", e);
			throw new GIRuntimeException("Exception occured while fetching Agency BOB-Individuals-Enrollment.", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		
		return listOfIndividuals;
	}
	
	private StringBuilder setParameter(StringBuilder queryString, SearchAgencyBookOfBusinessDTO searchAgencyBookOfBusinessDTO){
		if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getAgentFirstName())) {
			queryString.append(" AND (UPPER(U.FIRST_NAME) LIKE UPPER(:brokerFirstName) OR  B.FIRST_NAME  LIKE UPPER(:brokerFirstName))");
		}
		
		if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getAgentLastName())) {
			queryString.append(" AND (UPPER(U.LAST_NAME) LIKE UPPER(:brokerLastName) OR  B.LAST_NAME  LIKE UPPER(:brokerLastName))");
		}
		
		if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getAgentLicenseNumber())) {
			queryString.append(" AND B.LICENSE_NUMBER = :licenseNumber");
		}
		
		if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getFirstName())) {
			queryString.append(" AND UPPER(EI.FIRSTNAME) LIKE UPPER(:firstName)");
		}
		
		if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getLastName())) {
			queryString.append(" AND UPPER(EI.LASTNAME) LIKE UPPER(:lastName)");
		}
		
		if( null != searchAgencyBookOfBusinessDTO.getSortBy() && !searchAgencyBookOfBusinessDTO.getSortBy().trim().isEmpty()) {
			queryString.append(" ORDER BY "+searchAgencyBookOfBusinessDTO.getSortBy()+" "+searchAgencyBookOfBusinessDTO.getSortOrder());
		}
		
		return queryString;
	}
	
	private Query setParameterValue(EntityManager em,  SearchAgencyBookOfBusinessDTO searchAgencyBookOfBusinessDTO, StringBuilder queryString){
		Query query = em.createNativeQuery(queryString.toString());
		if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getAgencyId())) {
			query.setParameter("agencyId", searchAgencyBookOfBusinessDTO.getAgencyId());
		}
		if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getAgentFirstName())) {
			query.setParameter("brokerFirstName", searchAgencyBookOfBusinessDTO.getAgentFirstName());
		}
		if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getAgentLastName())) {
			query.setParameter("brokerLastName", searchAgencyBookOfBusinessDTO.getAgentLastName());
		}
		if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getFirstName())) {
			query.setParameter("firstName", searchAgencyBookOfBusinessDTO.getFirstName());
		}
		if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getLastName())) {
			query.setParameter("lastName", searchAgencyBookOfBusinessDTO.getLastName());
		}
		if (StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getAgentLicenseNumber())) {
			query.setParameter("licenseNumber", searchAgencyBookOfBusinessDTO.getAgentLicenseNumber());
		}
		if(StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getApplicationYear())) {
			query.setParameter("effectiveDate", searchAgencyBookOfBusinessDTO.getApplicationYear());
		}
		if(StringUtils.isNotEmpty(searchAgencyBookOfBusinessDTO.getCaseNumber())) {
			query.setParameter("caseNumber", searchAgencyBookOfBusinessDTO.getCaseNumber());
		}
		if(StringUtils.isNoneEmpty(searchAgencyBookOfBusinessDTO.getIssuer())) {
			query.setParameter("issuerName", searchAgencyBookOfBusinessDTO.getIssuer());
		}
		
		return query;
	}

	
	private Object[] findIndividualEnrollment(String individualId, Map<String,LinkedList<Object[]>> map){
		LinkedList<Object[]> list =  map.get(individualId);
		if(list != null) {
			return list.getFirst();
		}
		return null;
	}
	
	@Override
	public AgencyBrokerListDTO searchAgents(SearchBrokerDTO searchBrokerDTO) {
		List<SearchBrokerDTO> list = new ArrayList<>();
		StringBuilder queryString = new StringBuilder();
		AgencyBrokerListDTO agencyBrokerListDTO = new AgencyBrokerListDTO();
		EntityManager em = null;
		
		try {
			queryString.append("SELECT b.id, U.FIRST_NAME, U.LAST_NAME, U.email,  "
					+ "L.ADDRESS1, L.ADDRESS2, L.CITY, L.STATE, L.ZIP , b.LICENSE_NUMBER FROM BROKERS B inner JOIN USERS U ON U.ID = B.USERID "
					+ "left JOIN AGENCY_SITE S ON b.agency_id = S.AGENCY_ID and b.location_id=s.location_id "
					+ "left JOIN LOCATIONS L ON S.LOCATION_ID=L.ID "
					+ "where b.agency_id = :agencyId and B.CERTIFICATION_STATUS='Certified' ");
				
			if (StringUtils.isNotEmpty(searchBrokerDTO.getLicenseNumber())) {
				queryString.append("  AND b.LICENSE_NUMBER = :licenseNumber  ");
			}
			
			if (StringUtils.isNotEmpty(searchBrokerDTO.getFirstName())) {
				queryString.append(" AND UPPER(U.FIRST_NAME) LIKE UPPER(:firstName)");
			}
			if (StringUtils.isNotEmpty(searchBrokerDTO.getLastName())) {
				queryString.append(" AND UPPER(U.LAST_NAME) LIKE UPPER(:lastName)");
			}
			if (StringUtils.isNotEmpty(searchBrokerDTO.getEmail())) {
				queryString.append(" AND UPPER(U.EMAIL) LIKE UPPER(:email) ");
			}
			if (StringUtils.isNotEmpty(searchBrokerDTO.getSiteName())) {
				queryString.append(" AND UPPER(S.SITE_LOCATION_NAME) = UPPER(:siteName) ");
			}
			
			queryString.append(" order by first_name");
			
			em = entityManagerFactory.createEntityManager();
			Query query = em.createNativeQuery(queryString.toString());
			query.setParameter("agencyId", searchBrokerDTO.getId());
			if (StringUtils.isNotEmpty(searchBrokerDTO.getFirstName())) {
				query.setParameter("firstName", searchBrokerDTO.getFirstName());
			}
			if (StringUtils.isNotEmpty(searchBrokerDTO.getLastName())) {
				query.setParameter("lastName", searchBrokerDTO.getLastName());
			}
			if (StringUtils.isNotEmpty(searchBrokerDTO.getEmail())) {
				query.setParameter("email", searchBrokerDTO.getEmail());
			}
			if (StringUtils.isNotEmpty(searchBrokerDTO.getSiteName())) {
				query.setParameter("siteName", searchBrokerDTO.getSiteName());
			}
			
			if (StringUtils.isNotEmpty(searchBrokerDTO.getLicenseNumber())) {
				query.setParameter("licenseNumber", searchBrokerDTO.getLicenseNumber() );
			}
			
			List<Object[]> resultList = query.getResultList();
			
			for (Object[] data : resultList) {
				SearchBrokerDTO brokerDetails = new SearchBrokerDTO();
				if(null != data[0]) {
					brokerDetails.setBrokerId(ghixJasyptEncrytorUtil.encryptStringByJasypt(data[0].toString()));
				}
				if(null != data[1]) {
					brokerDetails.setFirstName(data[1].toString());
				}
				if(null != data[2]) {
					brokerDetails.setLastName(data[2].toString());
				}
				if(null != data[3]) {
					brokerDetails.setEmail(data[3].toString());
				}
				AgencyLocationDto location = new AgencyLocationDto();
				if(null != data[4]) {
					location.setAddress1(data[4].toString());
				}
				if(null != data[5]) {
					location.setAddress2(data[5].toString());
				}
				if(null != data[6]) {
					location.setCity(data[6].toString());
				}
				if(null != data[7]) {
					location.setState(data[7].toString());
				}
				if(null != data[8]) {
					location.setZip(data[8].toString());
				}
				brokerDetails.setLocation(location);
				
				brokerDetails.setLicenseNumber(data[9].toString());
				
				list.add(brokerDetails);
			}
			agencyBrokerListDTO.setBrokers(list);	
		} catch (Exception e) {
			LOGGER.error("Exception occured while searching Agency agents.", e);
			throw new GIRuntimeException("Exception occured while searching Agency agents.", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		
		return agencyBrokerListDTO;
	}
	
	@Override
	public int findNoOfEnrollees(long enrollmentId) {
		StringBuilder queryString = new StringBuilder();
		EntityManager em = null;
		
		try {
			queryString.append(" select count (*) from enrollee where enrollment_id = :enrollmentId and PERSON_TYPE_LKP in "
					+ " (SELECT LOOKUP_VALUE_ID FROM LOOKUP_VALUE WHERE LOOKUP_VALUE_CODE IN('SUBSCRIBER', 'ENROLLEE')) "
					+ " AND ( ENROLLEE_STATUS_LKP IN  (SELECT LOOKUP_VALUE_ID FROM LOOKUP_VALUE WHERE LOOKUP_VALUE_CODE IN('PENDING','CONFIRM')) "
					+ " OR (ENROLLEE_STATUS_LKP IN (SELECT LOOKUP_VALUE_ID FROM LOOKUP_VALUE WHERE LOOKUP_VALUE_CODE IN('TERM')) AND EFFECTIVE_END_DATE >= sysdate ))");
					
			em = entityManagerFactory.createEntityManager();
			Query query = em.createNativeQuery(queryString.toString());
			query.setParameter("enrollmentId", enrollmentId);
			
			Object resultList = query.getSingleResult();
			return Integer.valueOf(resultList.toString());
		} catch (Exception e) {
			LOGGER.error("Exception occured while finding number of enrollees.", e);
			throw new GIRuntimeException("Exception occured while finding number of enrollees.", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
	}
	
	@Override
	public Agency findByAgencyAssistantId(long agencyAssistantId) {
		return iAgencyRepository.findByAgencyAssistantId(agencyAssistantId);
	}
	
	@Override
	public AgencyInformationDto loadAgencyManagers(AgencyInformationDto agencyInformationDto, int agencyId) {
		AgencyManagerProfileDto agencyManagerProfileDto = null;
		List<AgencyManagerProfileDto> agencyManagersInfoDto = new ArrayList<>();
		
		try {
			List<Broker> agencyManagers = iBrokerRepository.findByAgencyManagerInfo(agencyId);
			if(agencyManagers!=null && agencyManagers.size()>0){
				for(Broker broker:agencyManagers){
					agencyManagerProfileDto = new AgencyManagerProfileDto();
					agencyManagerProfileDto.setAgencyManagerFirstName(broker.getUser().getFirstName());
					agencyManagerProfileDto.setAgencyManagerLastName(broker.getUser().getLastName());
					
					agencyManagersInfoDto.add(agencyManagerProfileDto);
				}
				Collections.sort(agencyManagersInfoDto);
				agencyInformationDto.setAgencyManagersInfoDto(agencyManagersInfoDto);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while loading agency managers.", e);
			throw new GIRuntimeException("Exception occured while loading agency managers.", e);
		}
		
		return agencyInformationDto;
	}
}
