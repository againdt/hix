const inputHouseholds = ["regression/APTC_3HH"]
before(function () {
    cy.runBeforeBlock()
})

inputHouseholds.forEach( file => {
    describe(`CIC4: Change SSN for ${file} enrolled into medical only`, function() {
        it (`Init household info with SSN, DOB with checks for ${file}`, function() {
            cy.ServerInfo = this.serverInfo
            cy.ServerConfig = this.serverConfig.body
            cy.ServerTime = this.serverTime.body
            cy.initHousehold(file)
        })
        it(`Creates Account Transfer for ${file}`, function () {
            cy.createAccountTransfer(cy.household)
        })
        it(`Login to  ` + cy.MN.URL, function () {
            cy.login(cy.h.user, cy.h.pass)
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Enrolls household into Medical only - ${file}`, function () {
            cy.enrollIntoMedicalOnly();
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Logout`, function () {
            cy.logout()
        })
        it(`Update SSN for all members in ${file}`, function () {
            cy.log("SSN for primary user before change: " + cy.h.people[0].ssn)
            for (let i = 0; i < cy.h.people.length; i++){
                cy.h.people[i].ssn = cy.getRandomSSN()
            }
            cy.log("SSN for primary user after change: " + cy.h.people[0].ssn)
            cy.checkSSNunique(cy.h)
            cy.h.appStatus = "enroll_med_demo_update"
        })
        it(`AT update for ${file}`, function () {
            cy.updateAccountTransfer(cy.h)
        })
        it(`Login to  ` + cy.MN.URL, function () {
            cy.login(cy.h.user, cy.h.pass)
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Logout`, function () {
            cy.logout()
        })
    })
})









