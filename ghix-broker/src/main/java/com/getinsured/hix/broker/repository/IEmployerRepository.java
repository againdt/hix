package com.getinsured.hix.broker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Employer;

public interface IEmployerRepository extends JpaRepository<Employer, Integer> {
	@Query("SELECT emp FROM Employer emp where externalId = :externalId")
	Employer findByExternaId(@Param("externalId") String externalId);
}