<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Batch Nodes</title>
<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
<style type="text/css">
table, td, th
{
border:1px solid green;
font-family: 'Oxygen', sans-serif;
}
th
{
background-color:green;
color:white;
}
body
{
	text-align: center;
}
.container
{
	margin-left: auto;
	margin-right: auto;
	width: 40em;
}
h4
{
	font-family: 'Oxygen', sans-serif;
	color:#1E90FF;
}
.reddiv{
height:20px;
width:20px;
background:red;
border-radius:20px;
}

.yellowdiv{
height:20px;
width:20px;
background:yellow;
border-radius:20px;
}

.greendiv{
height:20px;
width:20px;
background:green;
border-radius:20px;
}

</style>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#tablediv").hide();
	
	$("#showTable").click(function(event){
        var nodesUrl = "/hix/batch/getBatchNodes";
	    $.ajax({
		url : nodesUrl,
		type : "GET",
		data : null,
		success: function(response)
		{
			var responseJson = JSON.parse(response);
				$("#nodestable tr").remove(); 
		       var table1 = $("#nodestable");
					var rowNew = $('<tr>  							<th scope="col" width ="20%">Alias</th>  							<th scope="col" width ="20%">IP Address</th>  							<th scope="col" width ="20%">Base URL</th>  							<th scope="col" width ="20%">Last Active Time </th>  							<th scope="col" width ="20%">Status</th>  						</tr> ');
							 rowNew.appendTo(table1);
	               $.each(responseJson, function(key,value) { 
					   console.log(key,value);
					    
	               	       rowNew = $("<tr><td></td><td></td><td></td><td></td><td align='center'></td></tr>");
	               	       
	               	       var base_urls = new Array();
	               	       base_urls = value['base_url'].split(" ");
	               	       
			               	 var arrayLength = base_urls.length;
			               	 var strURL = "";
			               	 for (var i = 0; i < arrayLength; i++) {
			               		strURL += "<a href = /hix/admin/batchjobs?server_url=" + 
			               		base_urls[i] + ">" + base_urls[i]  + "</a><br>";
			               	 }
	               	 
						   rowNew.children().eq(0).text(value['alias']); 
	                       rowNew.children().eq(1).text(value['ip_addresses']); 
	                       rowNew.children().eq(2).html(strURL); 
	                       rowNew.children().eq(3).text(value['lastActive']); 
						   var d1 = new Date();
						   var d2 = new Date(value['lastActive']);
						
						   var difference = d1.getTime() - d2.getTime();

						   if (Math.floor(difference/1000) <= 30)
						   {
							  // rowNew.children().eq(4).html('<img src="/hix/resources/images/green.jpg" alt="Green">');
							   rowNew.children().eq(4).html('<div text-align = "center" class="greendiv"></div>');
						   }else if (Math.floor(difference/1000) > 30 && Math.floor(difference/1000) <= 60)
						   {
							   rowNew.children().eq(4).html('<div class="yellowdiv"></div>');
						   }else{
								 rowNew.children().eq(4).html('<div class="reddiv"></div>');
						   }
	                       
	                       rowNew.appendTo(table1);
			      
	               });
		},
		
	});

	     $("#tablediv").show();          
});    
	$("#showTable").click();
});
</script>
</head>
<body class="container">
<h4>Batch Nodes</h4>
<br/>
<table><tr><td align = "left">
<input type="button" value="Refresh" id="showTable" />
</td></tr></table>
<br/>
<div id="tablediv">
<table cellspacing="0" id="nodestable"> 
    
</table>
</div>
</body>
</html>