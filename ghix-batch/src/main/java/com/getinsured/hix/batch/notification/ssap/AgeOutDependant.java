package com.getinsured.hix.batch.notification.ssap;

public class AgeOutDependant {
	
	private String firstName;
	private String lastName;
	private String dropOutDate;
	private String dropOutReasonText;
	private String planId;
	private String sepEndDate;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDropOutDate() {
		return dropOutDate;
	}
	public void setDropOutDate(String dropOutDate) {
		this.dropOutDate = dropOutDate;
	}
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public String getDropOutReasonText() {
		return dropOutReasonText;
	}
	public void setDropOutReasonText(String dropOutReasonText, String dateOfbirth) {
		
		this.dropOutReasonText = String.format(dropOutReasonText, dateOfbirth) ;
	}
	public String getSepEndDate() {
		return sepEndDate;
	}
	public void setSepEndDate(String sepEndDate) {
		this.sepEndDate = sepEndDate;
	}
	
	
}
