package com.getinsured.eligibility.at.ref.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountActivation.STATUS;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.repository.IAccountActivationRepository;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.accountactivation.service.jpa.PrintEmailAccountActivationServiceImpl;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.HouseholdContact;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author chopra_s
 * 
 */
@Component("referralAccountActivationService")
@DependsOn("dynamicPropertiesUtil")
@Scope("singleton")
public class ReferralAccountActivationServiceImpl implements ReferralAccountActivationService {

	private static final Logger LOGGER = Logger.getLogger(ReferralAccountActivationServiceImpl.class);
	private static final String EMAIL_TYPE_KEY = "emailType";

	@Autowired
	private PrintEmailAccountActivationServiceImpl printEmailAccountActivationServiceImpl;

	private AccountActivationService accountActivationService;
	private static Gson gson = null;

	static {
		final GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.serializeNulls();
		gsonBuilder.setDateFormat(ReferralConstants.JSON_DATE_FORMAT);
		gsonBuilder.serializeSpecialFloatingPointValues();
		gson = gsonBuilder.create();
	}

	@Autowired
	private IAccountActivationRepository iAccountActivationRepository;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private ILocationRepository iLocationRepository;

	@Autowired
	private SsapJsonBuilder ssapJsonBuilder;

	@PostConstruct
	public void createStateContext() {
		accountActivationService = printEmailAccountActivationServiceImpl;
	}

	@Override
	public String retriggerAccountActivation(String caseNumber, String notificationType) {
		LOGGER.info("Trigger Account Activation Starts");

		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByCaseNumber(caseNumber);

		SsapApplication ssapApplication = null;
		if (ssapApplications != null && ssapApplications.size() > 0) {
			ssapApplication = ssapApplications.get(0);
		} else {
			throw new GIRuntimeException("No ssap application found!");
		}

		Long applicationId = ssapApplication.getId(); /* ssapApplication.getId() will never return NULL as this is primitive */
		List<AccountActivation> aaList = iAccountActivationRepository.findByCreatedObjectTypeAndCreatorObjectId("INDIVIDUAL_REFERRAL", applicationId.intValue());
		if (aaList == null) {
			return "No active activation link found.";
		}
		// find atleast one active account activation object and deactivate all other
		boolean areAllInactive = true;
		for (AccountActivation accountActivation : aaList) {
			if (accountActivation.getStatus() == STATUS.NOTPROCESSED) {
				areAllInactive = false;
				updateAccountActivation(accountActivation);
			}
		}
		// if atleast one active not found, do not proceed, stop
		if (areAllInactive) {
			return "No active activation link found.";
		}

		executeAccountActivation(ssapApplication, notificationType);

		return "retriggered successfully";
	}

	@Override
	public void triggerAccountActivation(SingleStreamlinedApplication singleStreamlinedApplication, SsapApplication ssapApplication, AccountTransferRequestDTO accountTransferRequest) {
		LOGGER.info("Trigger Account Activation Starts");
		final HouseholdMember primaryHouseholdMember = ReferralUtil.primaryMember(singleStreamlinedApplication);
		if (primaryHouseholdMember == null) {
			LOGGER.info("No primary household member found.");
			throw new GIRuntimeException(ReferralProcessingService.NO_DATA);
		}

		executeAccountActivation(primaryHouseholdMember, ssapApplication, accountTransferRequest);

		LOGGER.info("Trigger Account Activation Ends");
	}

	private Location createLocation(HouseholdMember primaryHouseholdMember) {
		final HouseholdContact householdContact = primaryHouseholdMember.getHouseholdContact();

		Address address = null;

		if (householdContact.getMailingAddress() != null && ReferralUtil.isNotNullAndEmpty(householdContact.getMailingAddress().getPostalCode())) {
			address = householdContact.getMailingAddress();
		}

		if (address == null && householdContact.getHomeAddress() != null && ReferralUtil.isNotNullAndEmpty(householdContact.getHomeAddress().getPostalCode())) {
			address = householdContact.getHomeAddress();
		}

		Location newLocation = new Location();
		if (address != null) {
			newLocation.setAddress1(address.getStreetAddress1());
			newLocation.setAddress2(address.getStreetAddress2() != null ? address.getStreetAddress2() : "");
			newLocation.setCity(address.getCity());
			newLocation.setState(address.getState());
			newLocation.setZip(address.getPostalCode());
			newLocation.setCounty(address.getCounty());
			newLocation.setCountycode(address.getCountyCode());
		}
		return newLocation;
	}

	private void executeAccountActivation(HouseholdMember primaryHouseholdMember, SsapApplication ssapApplication, AccountTransferRequestDTO accountTransferRequest) {
		try {
			CreatedObject createdObject = new CreatedObject();
			createdObject.setEmailId(primaryHouseholdMember.getHouseholdContact().getContactPreferences().getEmailAddress());
			createdObject.setObjectId((int) ssapApplication.getId());
			createdObject.setRoleName("INDIVIDUAL_REFERRAL");
			createdObject.setPhoneNumbers(ReferralUtil.fetchPhonenumbers(primaryHouseholdMember));
			createdObject.setFullName(primaryHouseholdMember.getName().getFirstName() + " " + primaryHouseholdMember.getName().getLastName());
			createdObject.setFirstName(primaryHouseholdMember.getName().getFirstName());
			createdObject.setLastName(primaryHouseholdMember.getName().getLastName());

			final Location location = createLocation(primaryHouseholdMember);
			String locationJson = gson.toJson(location);

			Map<String, String> fields = new HashMap<>();

			final int expirationDays = calculateExpirationDays(accountTransferRequest);

			if (accountTransferRequest.isAccountMigration()) {
				fields.put(EMAIL_TYPE_KEY, "AccountMigrationEmail");
			} else if (accountTransferRequest.isQE()) {
				fields.put(EMAIL_TYPE_KEY, "ReferralActivationWithoutAutoLinkingEmail");
			} else {
				fields.put(EMAIL_TYPE_KEY, "ReferralAccountActivationEmail");
			}

			fields.put("expirationDays", Integer.toString(expirationDays));
			fields.put("location", locationJson);

			fields.put("accessCode", populateAccessCodeFromPrimaryApplicant(ssapApplication));
			fields.put("caseNumber", ssapApplication.getCaseNumber());
			fields.put("externalAppId", ssapApplication.getExternalApplicationId());
			
			createdObject.setCustomeFields(fields);

			CreatorObject creator = new CreatorObject();
			creator.setObjectId(0);
			creator.setFullName("Administrator");
			creator.setRoleName(GhixRole.ADMIN.toString());

			accountActivationService.initiateActivationForCreatedRecord(createdObject, creator, expirationDays);
		} catch (GIException ge) {
			throw new GIRuntimeException(ge);
		}
	}

	private String populateAccessCodeFromPrimaryApplicant(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getApplicantGuid();
			}
		}

		return null;
	}

	private void executeAccountActivation(SsapApplication ssapApplication, String notificationType) {

		SsapApplicant primaryApplicant = null;
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if (applicant.getPersonId() == 1) {
				primaryApplicant = applicant;
				break;
			}
		}

		try {
			CreatedObject createdObject = new CreatedObject();
			// sonar fix
			if (primaryApplicant != null) {
				createdObject.setEmailId(primaryApplicant.getEmailAddress());
				createdObject.setPhoneNumbers(primaryApplicant.getPhoneNumber() != null ? Arrays.asList(primaryApplicant.getPhoneNumber()) : new ArrayList<String>());
				createdObject.setFullName(primaryApplicant.getFirstName() + " " + primaryApplicant.getLastName());
				createdObject.setFirstName(primaryApplicant.getFirstName());
				createdObject.setLastName(primaryApplicant.getLastName());
			}
			createdObject.setObjectId((int) ssapApplication.getId());
			createdObject.setRoleName("INDIVIDUAL_REFERRAL");

			final Location location = getLocation(ssapApplication);
			String locationJson = gson.toJson(location);

			Map<String, String> fields = new HashMap<String, String>();

			AccountTransferRequestDTO accountTransferRequest = new AccountTransferRequestDTO();
			if ("ReferralActivationWithoutAutoLinkingEmail".equals(notificationType)) {
				fields.put("emailType", "ReferralActivationWithoutAutoLinkingEmail");
				accountTransferRequest.setQE(true);
			} else if ("ReferralAccountActivationWithoutEmail".equals(notificationType)) {
				fields.put("emailType", "ReferralAccountActivationWithoutEmail");
			} else {
				fields.put("emailType", "ReferralAccountActivationEmail");
			}

			final int expirationDays = calculateExpirationDays(accountTransferRequest);
			fields.put("expirationDays", Integer.toString(expirationDays));
			fields.put("location", locationJson);
			fields.put("accessCode", populateAccessCodeFromPrimaryApplicant(ssapApplication));
			fields.put("caseNumber", ssapApplication.getCaseNumber());
			createdObject.setCustomeFields(fields);

			CreatorObject creator = new CreatorObject();
			creator.setObjectId(0);
			creator.setFullName("Administrator");
			creator.setRoleName(GhixRole.ADMIN.toString());

			accountActivationService.initiateActivationForCreatedRecord(createdObject, creator, expirationDays);
		} catch (GIException ge) {
			throw new GIRuntimeException(ge);
		}
	}

	private Location getLocation(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		Location location = null;
		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				if (ssapApplicant.getMailiingLocationId() != null) {
					location = iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
				} else if (ssapApplicant.getOtherLocationId() != null) {
					location = iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
				}
			}
		}

		if (location != null) {
			location.setAddress2(location.getAddress2() != null ? location.getAddress2() : "");
			location.setCreated(null);
			location.setUpdated(null);
		}

		return location;
	}

	private int calculateExpirationDays(AccountTransferRequestDTO accountTransferRequest) {
		int iDays = 0;
		String lceDaysExpiration = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_REFERRAL_EXPIRATION_DAYS);
		String qeDaysExpiration = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.QE_REFERRAL_EXPIRATION_DAYS);
		String oeEndDate = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.OPEN_ENROLLMENT_END_DATE);
		final int lceDays = Integer.parseInt(lceDaysExpiration);
		final int qeDays = Integer.parseInt(qeDaysExpiration);
		
		if (accountTransferRequest.isLCE()) {
			iDays = lceDays;
		} else if (accountTransferRequest.isQE()) {
			iDays = qeDays;
		} else {
			final String currentDate = ReferralUtil.formatDate(ReferralUtil.currentDate(), ReferralConstants.DEFDATEPATTERN);
			if (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(currentDate), ReferralUtil.convertStringToDate(oeEndDate)) >= 0) {
				iDays = (int) ReferralUtil.dayDifference(ReferralUtil.convertStringToDate(oeEndDate), ReferralUtil.convertStringToDate(currentDate));
			} else {
				iDays = lceDays;
			}
		}
		LOGGER.debug("Account Activation Expiration Days - " + iDays);
		return iDays;
	}

	@Override
	public AccountActivation updateAccountActivation(int ssapApplicationId) {
		AccountActivation accountActivationObj = accountActivationService.getAccountActivationByCreatedObjectId(ssapApplicationId, "INDIVIDUAL_REFERRAL");
		accountActivationObj.setStatus(STATUS.PROCESSED);
		return accountActivationService.update(accountActivationObj);
	}

	private AccountActivation updateAccountActivation(AccountActivation accountActivationObj) {
		accountActivationObj.setStatus(STATUS.PROCESSED);
		return accountActivationService.update(accountActivationObj);
	}

	@Override
	public Boolean triggerAccountActivation(Long ssapApplicationId) {
		Boolean isTriggered = false;
		SsapApplication ssapApplication = null;
		SingleStreamlinedApplication ssapJson = null;
		AccountTransferRequestDTO accountTransferRequestDTO = null;

		List<SsapApplication> ssapApplications = null;

		try {
			ssapApplications = ssapApplicationRepository.findByAppId(ssapApplicationId);

			if (null != ssapApplications && !ssapApplications.isEmpty()) {
				ssapApplication = ssapApplications.get(0);
				ssapJson = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
				accountTransferRequestDTO = new AccountTransferRequestDTO();

				triggerAccountActivation(ssapJson, ssapApplication, accountTransferRequestDTO);

				ssapApplication.setApplicationStatus(ApplicationStatus.UNCLAIMED.getApplicationStatusCode());
				ssapApplicationRepository.save(ssapApplication);
				isTriggered = true;
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE SENDING ACCOUNT ACTIVATION: ", ex);
		}

		return isTriggered;
	}

}
