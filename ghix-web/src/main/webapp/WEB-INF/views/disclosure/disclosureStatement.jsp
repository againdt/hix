<%@page session="true" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div class="gutter10">
	<div class="row-fluid">
		<h1><a name="skip"></a> Terms of Use</h1>
	</div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
				<h4>&nbsp;</h4>
			</div>
		</div>
		
		<div class="span9" id="rightpanel">
			<div class="header">
				<h4>PII Disclosure statement</h4>
			</div>
			<div class="gutter10">
				<p>GetInsured shall use or disclose the data/PII it receives from Consumers, Applicants, Enrollees, or the Hub, only to perform the specific functions including:</p>
				
				<ol>
					<li>Display of all QHPs available through the FFE</li>
					<li>Support the selection of a QHP and the enrollment in a QHP</li>
					<li>Facilitation of the application for and the receipt of APTCs, and the collection of standardized attestations acknowledging the receipt of the APTC determination, if applicable</li>
					<li>Inform Consumers, Applicants, or Enrollees of eligibility for Medicaid or Children’s Health Insurance Program (CHIP);</li>
					<li>Transmission of information about the Consumer, Applicant, or Enrollee’s decisions regarding QHP enrollment and/or CSR and APTC information from the Web-broker’s web site to the FFE</li>
					<li>Facilitation of payment of the initial premium amount</li>
					<li>Support of an Enrollee’s ability to disenroll from a QHP</li>
					<li>Facilitation of an Enrollee’s ability to report changes in eligibility status to the FFE</li>
					<li>Maintenance of an electronic record of information related to health care coverage enrollment activity on the Web-broker’s web site for a period of 10 years</li>
				</ol>
			</div>
		</div>

	</div> 
</div>
<!--  end of gutter10 -->

