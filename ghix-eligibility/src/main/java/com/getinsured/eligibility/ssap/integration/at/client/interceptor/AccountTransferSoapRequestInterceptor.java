package com.getinsured.eligibility.ssap.integration.at.client.interceptor;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.util.SoapClientLoggingInterceptor;
import com.getinsured.hix.platform.util.SoapHelper;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

public class AccountTransferSoapRequestInterceptor implements MethodInterceptor {
  private static final String SSAP_APPLICATION_STATUS_SUBMITTED = "SU";

  private static final String STATUS_FAILURE = "FAILURE";

  private static final String STATUS_SUCCESS = "SUCCESS";

  private static final String endpointFunction =  "ACCOUNT-TRANSFER-OUTBOUND";

  private static final Logger LOGGER = Logger.getLogger(SoapClientLoggingInterceptor.class);

  private static final Object SUCCESS_RESPONSE_CODE = "0000";

  @Autowired
  private GIWSPayloadService giwsPayloadService;

  @Autowired
  private SsapApplicationRepository ssapApplicationRepository;

  @Override
  public Object invoke(MethodInvocation methodInvocation) throws Throwable {

    Object result = null;

    String requestPayload = null;
    String responsePayload = null;
    String exceptionTrace = null;
    String status = STATUS_SUCCESS;
    
    Object[] arg = methodInvocation.getArguments();

    try {
      requestPayload = SoapHelper.marshal(arg[0]);
      result = methodInvocation.proceed();
      responsePayload = SoapHelper.marshal(result);
    } catch (Exception e) {
      status = STATUS_FAILURE;
      exceptionTrace = ExceptionUtils.getFullStackTrace(e);
      throw e;
    } finally {
      try {
    	  
    	String responseCode = SoapHelper.extractResponseCode(responsePayload);
		if (!responseCode.equalsIgnoreCase((String) SUCCESS_RESPONSE_CODE)) {
			status = STATUS_FAILURE;
		}
    	
        Long ssapApplicationId = (Long) arg[1];
        String endpointUrl = arg[2].toString();
        GIWSPayload giwsPayload = new GIWSPayload();
        giwsPayload.setSsapApplicationId(ssapApplicationId);
        giwsPayload.setCorrelationId(null);
        giwsPayload.setCreatedTimestamp(new TSDate());
        giwsPayload.setCreatedUserId(null);
        giwsPayload.setEndpointFunction(endpointFunction);
        giwsPayload.setEndpointOperationName(null);
        giwsPayload.setEndpointUrl(endpointUrl);
        giwsPayload.setExceptionMessage(exceptionTrace);
        giwsPayload.setRequestPayload(requestPayload);
        giwsPayload.setResponseCode(responseCode);
        giwsPayload.setResponsePayload(responsePayload);
        giwsPayload.setStatus(status);
        saveServiceLog(giwsPayload);
        
        // Set the ssap application status to submitted if transfer was successful
        if (null != responseCode && responseCode.equals(SUCCESS_RESPONSE_CODE)) {
            SsapApplication ssapApplication = ssapApplicationRepository.findOne(ssapApplicationId);
            ssapApplication.setApplicationStatus(SSAP_APPLICATION_STATUS_SUBMITTED);
            ssapApplicationRepository.save(ssapApplication);
            LOGGER.debug("Application status to submitted for application id " + ssapApplicationId);
        }
      } catch (Exception e) {
        /**
         * Log and eat exception to give chance to get response back to the caller. 
         * Something has gone wrong with SoapClientLoggingInterceptor recording.
         */
        LOGGER.error("Error recording request/response in AccountTransferSoapRequestInterceptor", e);
      }
    }
    return result;
  }

  private GIWSPayload saveServiceLog(GIWSPayload giwsPayload) {
    return giwsPayloadService.save(giwsPayload);
  }

}
