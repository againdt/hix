<%@page import="java.util.HashMap"%>
<%@ taglib prefix="surl" uri="/WEB-INF/tld/secure-url.tld" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>


<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/inbox.css" />" />
<!-- File upload scripts -->
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript">


function clearssnfield(){
	$('#displaySSN').val('');
	$('#ssnValueSpan').hide();
	$('#ssnRequiredNo').removeAttr('disabled');
	$('#clearSSNDiv').hide();
	
}
$(document).ready(function(){
	
	$('.date-picker').datepicker({
	    autoclose: true,
	    dateFormat: 'MM/DD/YYYY',
	    endDate:new Date()
	});
	
	var ssnValue = '${household.ssn}';
	//var ssnValue = '${consumerssn}';
	var ssnNotRequiredReason = '${household.ssnNotProvidedReason}';
	if(ssnValue){
		$('#ssnRequiredNo').attr('disabled',true);
		$('#ssnRequiredYes').attr('checked',true);
		$('#clearSSNDiv').show();
	}
	else if(ssnNotRequiredReason){
		$('#ssnNotReqdReasons').show();
		$('#ssnRequiredNo').attr('checked',true);	
		$("#ssnNotReqdReasons").val(ssnNotRequiredReason);
		$('#clearSSNDiv').hide();
	}
	else{
		$('#clearSSNDiv').hide();
		$('#ssnRequiredYes').attr('checked',true);
	}
	$("#ssnRequiredNo").bind("click",function(event){
	    $('#ssnNotReqdReasons').show();
	    $('#ssnNotReqdReasons').attr('required',true);
	    $('#clearSSNDiv').hide();
	    $('#ssnValueSpan').hide();
	    $("#ssnNotReqdReasons").val('');
	});
	$("#ssnRequiredYes").bind("click",function(event){
	    $('#ssnNotReqdReasons').hide();
	    $('#ssnNotReqdReasons_error').hide();
	    $('#ssnNotReqdReasons').attr('required',false);
	    
		if(ssnValue){
			$('#ssnRequiredNo').attr('disabled',true);
			$('#displaySSN').val(ssnValue);
			$('#ssnValueSpan').show();
			$('#clearSSNDiv').show();
		}
	});
	
	$("#mark_verified").click(function(e) {
		var comment = prompt("<spring:message  code='label.ridpVerificationReason'/>");
		if (comment != null) {
			var ridpOverrideUrl = '<c:url value="/crm/member/markRidpVerified"/>';
			var csrftoken = '${sessionScope.csrftoken}';
			var householdId = '<encryptor:enc value="${householdId}"/>';
			var ridpVerificationFailedMessage = "<spring:message  code='label.ridpVerificationCommentNotProvidedMessage'/>";
			$.ajax({
				url : ridpOverrideUrl,
				type : "POST",
				data : {csrftoken:csrftoken, householdId:householdId, comments:comment},
				success: function(data, textStatus, jqXHR){
					 if(data.responseCode == "HS000000") {
						$('#ridp_status').html("<strong><spring:message  code='label.ridpVerified'/></strong>");
					 } else {
						alert(ridpVerificationFailedMessage + data.responseDescription);
					 }
				},
				error: function(jqXHR, textStatus, errorThrown){
					alert(ridpVerificationFailedMessage + textStatus + ", errorThrown: " + errorThrown);
				},
			});
		} else {
			alert("<spring:message  code='label.ridpVerificationCommentNotProvidedMessage'/>");
		}
	});
	
	$("#manual_verification").click(function(e) {
		var pushToManualVerificationUrl = '<c:url value="/crm/member/convertManualRIDP"/>';
		var csrftoken = '${sessionScope.csrftoken}';
		var householdId = '${householdId}';
		var convertToManualVerificationFailureMessage = "<spring:message  code='label.ridpVerificationCommentNotProvidedMessage'/>";
		var convertToManualVerificationSuccessMessage = "<spring:message  code='label.convertToManualVerificationSuccessMessage'/>";
		$.ajax({
			url : pushToManualVerificationUrl,
			type : "POST",
			data : {csrftoken:csrftoken, householdId:householdId},
			success: function(data, textStatus, jqXHR){
				 if(data.responseCode == "HS000000") {
					alert(convertToManualVerificationSuccessMessage);
					$("#manual_verification" ).remove();
				 } else {
					
				 }
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert(convertToManualVerificationFailureMessage + ":" + textStatus + ", errorThrown: " + errorThrown);
			},
		});
	});
});


</script>

<c:set var="encHouseholdId"><encryptor:enc value='${householdId}'/></c:set>
<c:set var="encHouseholdEmail"><encryptor:enc value='${household.email}'/></c:set>
<c:set var="encHouseholdPhone"><encryptor:enc value='${household.phoneNumber}'/></c:set>

<jsp:include page="sendConsumerActivationLink.jsp">
	<jsp:param value="${encHouseholdId}" name="householdId"/>
	<jsp:param value="${encHouseholdEmail}"  name="householdEmail"/>
	<jsp:param value="${encHouseholdPhone}" name="householdPhone"/>
</jsp:include>

<!--start page-breadcrumb -->
<div class="gutter10">
<div class="row-fluid">	
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/crm/member/managemembers"/>">Members</a></li>
			<li><spring:message code="label.basicInformation"/></li>
		</ul> 
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
		<h1 class="householdInfo">${household.firstName} ${household.lastName} (ID: ${householdId})</h1> 

	</div>
	<!--page-breadcrumb ends-->
	
	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<jsp:include page="../cap/consumerSummaryNavigation.jsp"></jsp:include>
		</div>
		
		<div id="rightpanel" class="span9">
			<div class="header">
                 <h4><spring:message code="label.basicInformation"/></h4>
            </div>
            <div class="gutter10">
			<form class="form-horizontal" id="frmeditconsumer" name="frmeditconsumer" action="<c:url value='/crm/member/editmemberSubmit'/>"  method="POST">
			<df:csrfToken/>
				<div class="control-group">
					<label class="control-label">First Name:<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" id="firstName" name="firstName" maxlength="100" value="${household.firstName}" />
						<div id="firstName_error"></div>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">Last Name:<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" id="lastName" name="lastName"  maxlength="100" value="${household.lastName}"/>
						<div id="lastName_error"></div>
					</div>
				</div>
				
				<div class="control-group">
					<fmt:formatDate pattern="MM/dd/yyyy" value="${household.birthDate}" var="dob"/>
					<label class="control-label">Date Of Birth:<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<div class="input-append date date-picker" >
							<input class="input-medium" maxlength="10" type="text" name="birthDate" id="birthDate" pattern="MM/DD/YYYY" placeholder="MM/DD/YYYY" ui-mask="99/99/9999" value="${dob}" /> 
							<span class="add-on"><i class="icon-calendar"></i></span>
						</div>
						<div id="birthDate_error"></div>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label"><spring:message  code='label.columnHeaderAddress'/>:</label>
					<div class="controls gutter5-t">
						<c:if test="${household.prefContactLocation !=null}">
							<strong>
								<p>${household.prefContactLocation.address1} <br />
									<c:if test="${household.prefContactLocation.address2 != null}">${household.prefContactLocation.address2} address2<br /></c:if>
									${household.prefContactLocation.city}  <c:if test="${household.prefContactLocation.city != null && household.prefContactLocation.state != null}">, </c:if> ${household.prefContactLocation.state}
								</p>
							</strong>
						</c:if>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label"><spring:message  code='label.columnHeaderZip'/>:</label>
					<div class="controls gutter5-t">
						<c:if test="${household.prefContactLocation !=null}">
							<strong>${household.prefContactLocation.zip}</strong>
						</c:if>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label"><spring:message  code='label.emailAddress'/></label>
					<div class="controls gutter5-t">
						<c:choose>
							<c:when test="${household.user == null}">
								<td><input type="text" name="email" id="email1" value="${household.email}"/>
									<span id="email1_error"></span>
								</td>
							</c:when>
							<c:otherwise>
								<td><input type="hidden" name="email" id="email" value="${household.email}"/><strong>${household.email}</strong></td>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label"><spring:message  code='label.columnHeaderPhone'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />:</label>
					<div class="controls">
						<input type="text"  name="phoneNumber" id="phoneNumber" value="${household.phoneNumber}"/>
						<div id="phoneNumber_error"></div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><spring:message  code='label.columnHeaderAccessCode'/>:</label>
					<div class="controls gutter5-t">
						 <c:if test="${accessCode !=null}"> 
							<strong>${accessCode}</strong>
						</c:if>
					</div>
				</div>
				<c:if test="${permissionMap.memberViewCaseid}">
					<div class="control-group">
						<label class="control-label"><spring:message  code='label.columnHeadercaseId'/>:</label>
						<div class="controls gutter5-t">
							<c:if test="${not empty household.householdCaseId}">
								<strong>${household.householdCaseId}</strong>
							</c:if>
						</div>
					</div>
				</c:if>
				<div class="control-group">
					<label class="control-label"><spring:message  code='label.columnHeaderRidpVerified'/>:</label>
					<div class="controls gutter5-t">
						<c:if test="${null != household.ridpVerified && 'Y'.equals(household.ridpVerified)}">
               	 			<span id="ridp_status"><strong><spring:message  code='label.ridpVerified'/></strong></span>
						</c:if>
						<c:if test="${null == household.ridpVerified || !'Y'.equals(household.ridpVerified)}">
               	 			<span id="ridp_status">
               	 				<button type="button" class='btn' id='mark_verified'><spring:message  code='label.markVerified'/></button>&nbsp;
               	 				<c:if test="${!'MANUAL'.equals(household.ridpSource)}">
               	 					<button type="button" class='btn' id='manual_verification'><spring:message  code='label.convertToManualVerification'/></button>
               	 				</c:if>
               	 			</span>
						</c:if>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label">SSN<img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" />:</label>
					<div class="controls gutter5-t">
						<div style="display: inline;" >
									<input type="radio" name="ssnRequired" id="ssnRequiredYes" value="YES" required />
									<span> SSN is required</span>
								</div>
								<div style="display: inline; padding-left: 30px" id="clearSSNDiv">
									<input type="hidden" value="${consumerssn}" name="displaySSN" id="displaySSN">
									<span id="ssnValueSpan">${consumerssn}</span>
									<span class="dropdown" style="display: inline-block;padding-left: 20px">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" alt="dropdown"><i class="icon-cog" style="vertical-align: baseline;"></i><i class="caret"></i><span class="hide">Action</span><span aria-hidden="true" class="hide"> Dropdown Menu. Press enter to open it and tab through its options</span></a>
									<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
										<li><a href="javascript:clearssnfield();" onclick="" class="offset1"><i class="icon-refresh "></i>Clear SSN</a></li>
									</ul>
								</span>
								</div>
								<br/>
								<div>
									<input type="radio" name="ssnRequired"  id="ssnRequiredNo" value="NO" ><span>SSN is not required.</span> 
								
									<select name="ssnNotReqdReasons" id="ssnNotReqdReasons" class="input-medium" style="display: none;">
					                    <option value="">Select Reason</option>
                    					<option value="RELIGIOUS_EXCEPTION">Religious Exception</option>
					                    <option value="JUST_APPLIED">Just Applied</option>
					                    <!-- <option value="ILLNESS_EXCEPTION">Illness Exception</option> -->
					                    <option value="CITIZEN_EXCEPTION">Citizen Exception</option>
					                    <option value="OTHER">Other</option>
					            	</select>
					            
								</div>
								<br/>
								<div id="ssnRequiredYes_error"></div>
								<div id="ssnNotReqdReasons_error"></div>
					</div>
				</div>
	
				<input type="hidden" name="consumerName" id="consumerName" value="${household.firstName} ${household.lastName}" />
				<div class="form-actions">
					
					
    <div  class="pull-right"><button type="submit" name="mainSubmitButton" id="mainSubmitButton" onClick="javascript:submitEditConsumer();" class="btn btn-primary"><spring:message  code='label.save'/></button></div>
        		</div>
				<c:set var="encHouseHoldId" ><encryptor:enc value="${householdId}" isurl="true"/> </c:set>
				<input type="hidden" value="${encHouseHoldId}" name="encHouseholdid" id="encHouseholdid" />
			</form>
			</div>
		</div>
		

	</div> 
	<!-- row-fluid -->
</div>
<!-- gutter10 -->
<!--  Latest UI -->
<!-- Modal 
<form name="dialogForm" id="dialogForm" action="<c:url value="/crm/employer/home/${employerId}" />" novalidate="novalidate">
<form name="dialogForm" id="dialogForm" action="<c:url value="/account/user/switchUserRole/employer/${employerId}" />" novalidate="novalidate">-->
<form name="dialogForm" id="dialogForm" method="POST"  action="<c:url value="/crm/consumer/dashboard" />" novalidate="novalidate">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left">View Member Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body clearfix gutter0-tb">
    <div class="control-group">	
				<div class="controls">
                Clicking "Member View" will take you to the Member's portal for ${household.firstName} ${household.lastName}.<br/>
                Through this portal you will be able to take actions on behalf of the member.<br/>
                Proceed to Member view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
        <input class="pull-left"  type="checkbox" id="checkConsumerView" name="checkConsumerView"  > 
        <div class="pull-left">&nbsp; Don't show this message again.</div>
        <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-primary" type="submit">Member View</button>
        <input type="hidden" name="switchToModuleName" id="switchToModuleName" value='<encryptor:enc value="individual"/>' /> 
        <input type="hidden" name="switchToModuleId" id="switchToModuleId" value="<encryptor:enc value="${householdId}"/>" />
        <input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${household.firstName} ${household.lastName}" />
    <!--a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>

<script type="text/javascript">
jQuery.validator.addMethod("validEmail", function(value, element, param) {
	password = $("#email1").val();
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if (!emailReg.test(password)) {
		return false;
	} else {
		return true;
	}
	return false;
});

jQuery.validator.addMethod("phonecheck", function(value, element, param) {
	var phonenumber = $("#phoneNumber").val();

	var phoneno = /^\d{10}$/;
	  if(phonenumber.match(phoneno))     
	  {
		  return true;
		}
	return false;
});

jQuery.validator.addMethod("validFirstName", function(value, element, param) {
	var firstName = $("#firstName").val();
	var firstNameReg = /^[a-zA-Z]{1,50}$/;
	if (!firstNameReg.test(firstName)) {
		return false;
	} else {
		return true;
	}
});

jQuery.validator.addMethod("validLastName", function(value, element, param) {
	var lastName = $("#lastName").val();
	var lastNameReg = /^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$/;
	if (!lastNameReg.test(lastName)) {
		return false;
	} else {
		return true;
	}
});

jQuery.validator.addMethod("ssncheck", function(value, element, param) {
	var ssnReqdVal = $('#ssnRequiredNo').is(':checked')
	var ssnNotReqReasons = $( "#ssnNotReqdReasons" ).val();
	if (ssnReqdVal && ssnNotReqReasons == '') {
		return false;
	} else {
		return true;
	}
});

jQuery.validator.addMethod("dobcheck", function(value, element, param) {
	 var dob = $("#birthDate").val();
	 var today = getCurrentDate() ;
	 if (new Date(dob) >= new Date(today)) {
	       return false;
	 }
	 return true;
});

function getCurrentDate(){
	var currentDate = new Date();
   var year = currentDate.getFullYear();
   var currMonth = currentDate.getMonth()+1;
   var month = (currMonth > 9 ) ? currMonth : '0' + currMonth; 
   var day = currentDate.getDate();

   var today = month + '/' + day + '/' + year ;
   
   return today;
}

$('#frmeditconsumer').validate({
	rules: {
		firstName : {required: true, validFirstName: true},
	    lastName : {required: true, validLastName: true },
	    email : {validEmail : true},
	    phoneNumber:{required:true,phonecheck : true},
	    ssnRequired:{required:true},
	    ssnNotReqdReasons:{required:false, ssncheck : true},
	    birthDate :{required : true, date: true, dobcheck: true}
	},
	messages : {
		firstName : { 
			required : "<span> <em class='excl'>!</em><spring:message code='label.validateempfirstname' javaScriptEscape='true'/></span>",
			validFirstName: "<span> <em class='excl'>!</em><spring:message code='label.ridp.pattern.firstName' javaScriptEscape='true'/></span>"
			},
		lastName  : { 
			required : "<span> <em class='excl'>!</em><spring:message code='label.validateemplastname' javaScriptEscape='true'/></span>",
			validLastName: "<span> <em class='excl'>!</em><spring:message code='label.ridp.pattern.lastName' javaScriptEscape='true'/></span>"

			},
		email : {
			
			validEmail : "<span> <em class='excl'>!</em><spring:message code='label.validateEmailSyntax' javaScriptEscape='true'/></span>"
		//remote : "<span> <em class='excl'>!</em><spring:message code='label.validateEmailCheck' javaScriptEscape='true'/></span>"
		},
		phoneNumber:{
			required : "<span> <em class='excl'>!</em><spring:message code='label.validateempvtelephone' javaScriptEscape='true'/></span>",
			phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvtelephone' javaScriptEscape='true'/></span>"
		}, 
		ssnRequired:{
			required : "<span> <em class='excl'>!</em>Please select ssn required/not required option.</span>"
		},
		ssnNotReqdReasons:{
			ssncheck : "<span> <em class='excl'>!</em>Please select ssn not required reason.</span>"
		},
		birthDate :{required :  "<span> <em class='excl'>!</em><spring:message code='label.validateempvdob' javaScriptEscape='true'/></span>" , 
			date:  "<span> <em class='excl'>!</em><spring:message code='label.validateempvdob' javaScriptEscape='true'/></span>",
			dobcheck:  "<span> <em class='excl'>!</em><spring:message code='label.validateempvdob' javaScriptEscape='true'/></span>"}

	},
		errorClass : "error",
		errorPlacement : function(error, element) {
			var elementId = element.attr('id');
			if ($("#" + elementId + "_error").html() != null) {
				$("#" + elementId + "_error").html('');
			}
			error.appendTo($("#" + elementId + "_error"));
			}
		});
		
function submitEditConsumer(){
	 if( $("#frmeditconsumer").validate().form() ) {
/* 		 $("#frmeditconsumer").attr("action",'<c:url value="/crm/member/editmemberSubmit"/>');
 */		 
 		
		 $("#frmeditconsumer").submit();
	 }
	 

	}		
</script>		
</form>