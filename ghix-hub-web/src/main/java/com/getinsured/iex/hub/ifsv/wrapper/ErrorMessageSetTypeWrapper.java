package com.getinsured.iex.hub.ifsv.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.ErrorMessageSetType;
import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.ErrorMessageType;
import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.ObjectFactory;

/**
 * Wrapper com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.ErrorMessageSetType
 * 
 * @author Nikhil Talreja
 * @since 27-Feb-2014
 *
 */
public class ErrorMessageSetTypeWrapper implements JSONAware{
	
	private ErrorMessageSetType errorMessageSetType;
	
	private List<ErrorMessageTypeWrapper> errorMessageDetail;
	
	private static final String ERROR_MESSAGE_SET_KEY = "ErrorMessageDetail";

	
	public ErrorMessageSetTypeWrapper(ErrorMessageSetType errorMessageSetType){
		
		ObjectFactory factory = new ObjectFactory();
		this.errorMessageSetType = factory.createErrorMessageSetType();
		
		for(ErrorMessageType error : errorMessageSetType.getErrorMessageDetail()){
			ErrorMessageTypeWrapper wrapper = new ErrorMessageTypeWrapper(error);
			this.getErrorMessageDetail().add(wrapper);
			this.errorMessageSetType.getErrorMessageDetail().add(error);
		}
	}

	public ErrorMessageSetType getErrorMessageSetType() {
		return errorMessageSetType;
	}

	public List<ErrorMessageTypeWrapper> getErrorMessageDetail() {
		
		if(this.errorMessageDetail == null){
			this.errorMessageDetail = new ArrayList<ErrorMessageTypeWrapper>();
		}
		
		return errorMessageDetail;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		JSONArray errorMessageType = new JSONArray();
		for(ErrorMessageTypeWrapper data : this.errorMessageDetail){
			errorMessageType.add(data);
		}
		obj.put(ERROR_MESSAGE_SET_KEY,errorMessageType);
		return obj.toJSONString();
	}
	
}
