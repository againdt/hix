package com.getinsured.hix.externalassister.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.ExternalAssisterDesignate;


@Repository
public interface IExternalAssisterDesignateRepository extends JpaRepository<ExternalAssisterDesignate,Integer> {
	
	@Query("FROM ExternalAssisterDesignate where cmrHouseHoldId = :id ")
	public List<ExternalAssisterDesignate> getByHouseholdId(@Param("id")int id);
	
	@Query("FROM ExternalAssisterDesignate where cmrHouseHoldId = :id AND active='Y'")
	public ExternalAssisterDesignate getByActiveHouseholdId(@Param("id")int id);
	
	@Query(" SELECT EA.externalAssisterId, ED.cmrHouseHoldId, ED.active, EA.name, EA.type, EA.federalTaxId, EA.agentNpn, EA.userId, EA.id "
			+ " FROM ExternalAssisterDesignate AS ED, ExternalAssister AS EA where EA.id = ED.externalAssisterId "
			+ " AND ED.cmrHouseHoldId = :id AND ED.active = 'Y' ")
	public List<Object[]> getExternalBrokerDetails(@Param("id")int id);
	
	@Query(" SELECT EA.externalAssisterId, ED.cmrHouseHoldId, ED.active, EA.name, EA.type, EA.federalTaxId, EA.agentNpn, EA.userId "
			+ " FROM ExternalAssisterDesignate AS ED, ExternalAssister AS EA , AccountUser AS US, Household AS HH "
			+ " where EA.id = ED.externalAssisterId AND US.extnAppUserId = :extnAppUserId AND  US.id = HH.user "
			+ " AND EA.externalAssisterId = :externalAssisterId AND ED.cmrHouseHoldId IN (HH.id) AND ED.active = 'Y' ")
	public List<Object[]> getExternalBrokerDetailsForUserId(@Param("extnAppUserId")String id,@Param("externalAssisterId")String externalAssisterId);
	
	@Query(" SELECT EA.externalAssisterId, ED.cmrHouseHoldId, ED.active, EA.name, EA.type, EA.federalTaxId, EA.agentNpn, EA.userId "
			+ " FROM ExternalAssisterDesignate AS ED, ExternalAssister AS EA where EA.id = ED.externalAssisterId "
			+ " AND EA.externalAssisterId = :id AND ED.active = 'Y' ")
	public List<Object[]> getHouseholdDetailsForAssister(@Param("id")String id);
	
	@Query("SELECT US.extnAppUserId "
			+"FROM ExternalAssisterDesignate AS ED, ExternalAssister AS EA , AccountUser AS US, Household AS HH "
			+" where EA.id = ED.externalAssisterId  AND  US.id = HH.user "
			+" AND EA.externalAssisterId = :externalAssisterId AND ED.cmrHouseHoldId IN (HH.id) AND ED.active = 'Y' ")
	public List<String> getUsersForExternalAssisterId(@Param("externalAssisterId")String externalAssisterId);
	
}
