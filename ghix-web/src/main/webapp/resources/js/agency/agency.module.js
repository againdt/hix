(function() {
'use strict';

    angular.module('agencyApp',[
    	'ui.router',
    	'spring-security-csrf-token-interceptor',
        'pascalprecht.translate',
    	'addressValidationModule',
        'adminStaffAppControllerModule',
        'adminStaffCtrlModule',
        'adminStaffApprovalCtrlModule',
        'adminStaffStatusCtrlModule',
    	'agencyBOBCtrlModule',
    	'agencyInformationCtrlModule',
        'autofocusDirectiveModule',
    	'calendarComponent',
    	'certificationStatusCtrlModule',
    	'dataStorageServiceModule',
    	'documentUploadCtrlModule',
    	'formatPhoneModule',
    	'generalModalModule',
        'modalDirective',
        'navServiceModule',
        'popoverDirective',
    	'ui.bootstrap',
    	'spinningLoaderDirectiveModule',
    	'fileModelDirectiveModule',
    	'stateDropdownDirectiveModule',
        'statusServiceModule',
        'timefilterFactoryModule',
    	'timePickerDirectiveModule',
    	'transferConsumerDelegationsCtrl',
        'truncateFactoryModule',
    	'locationHourCtrlModule',
    	'locationHoursServiceModule',
        'viewAdminStaffCtrlModule',
        'yearSelectDirectiveModule'
    	]);

})();
