$(document).ready(function() {
  $('#myModal').modal();
}); 
var Log=function(str)
{
	try{
		console.log(str);
	}catch(e)
	{
		
	}
};



var app = angular.module('customerHistoryApp', []);

app.controller('mainController', ['$scope', '$http', function($scope, $http) {

	// Houshold ID of Individual
	$scope.householdId = $('#householdId').val();
	
	// GET $HTTP REQUEST
	$scope.getRequest = function() {
	  $http({
	    method: 'GET', 
	    url: '../callLogHistory?householdId='+ $scope.householdId
	  }).success(function(data) {
		//console.log(data);
	      $scope.histories = JSON.parse(data.data);
	      if ($scope.histories.historyList) {
	          angular.forEach($scope.histories.historyList, function (value, key) {
	              //this.push(key + ': ' + value);
	              if ("type" in value) {	                  
	                  value.eventLevel = value.type;
	              };
	              if ("metadata" in value) {
	                  if ("date" in value.metadata) {
	                      value.datetime = value.metadata.date;
	                  };
	              };
	          });
	      }

		//console.log($scope.histories);
		//console.log("==================");
		if (data == null) {
		  //alert(" Something went wrong.. please contact admin.");
		  return;
	    }
		if (data.status) {
		  //console.log("==================");
		  //console.log('Works: ' + data.message);
		  //$scope.histories = data.data;
		  //console.log("==================");
		  //console.log($scope.histories);
		} else {
		  //console.log('Fail:' + data.message);
		  //alert(data.message);
		}
	  });
	};
	// GET $HTTP REQUEST
	
	// FUNTION TO FILER BY TYPE
	$scope.filterFn = function (param) {
	    if (!param) {
	        return;
	    }
	    return function (history) {
	        //console.log(param);
	        var flag = true;
	        var typeFlag = true;

	        if (param.date != null && param.date != undefined) {
	            flag = false;
	            var serverDate = new Date(history.datetime);
	            var histDate = new Date(serverDate.getFullYear(), serverDate.getMonth(), serverDate.getDate());

	            var today = new Date();
	            today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
	            var diff = null;
	            diff = (today.getTime() - histDate.getTime()) / (24 * 3600 * 1000);
	            if (param.date == CapString.Today) {

	                if (diff == 0) {
	                    flag = true;
	                }
	            }
	            if (param.date == CapString.LastWeek) {
	                if (diff > 0 && diff <= 7) {
	                    flag = true;
	                }
	            }
	            if (param.date == CapString.LastMonth) {
	                if (diff > 0 && diff <= 30) {
	                    flag = true;
	                }
	            }
	            if (param.date == CapString.LastYear) {
	                if (diff > 0 && diff <= 365) {
	                    flag = true;
	                }
	            }
	        }
	        if (param.type != null && param.type != undefined) {
	            if (flag) {
	                if (String(history.eventLevel).trim() == String(param.type).trim()) {
	                    typeFlag = true;
	                } else {
	                    typeFlag = false;
	                }
	            } else {
	                typeFlag = String(history.eventLevel).trim() == String(param.type).trim();
	            }

	        }
	        //Log(history.eventLevel + "===" + param.type);
	        //Log(flag);
	        if (typeFlag == true && flag == true) {
	            return true;
	        } else {
	            return false;
	        };

	    };
	};
	
	// VALUES FOR FILTERS AND OTHER DROPDOWN MENUS
	$scope.filters = ['Notes', 'Call', 'Household Created '];
	$scope.types = ['Notes'];
	$scope.wrapCodeSelection = ['Hung Up', 'Pick Up'];
	$scope.typeSelection = ['Inbound', 'Outbound'];
	$scope.dateRangeFilter = [CapString.Today, CapString.LastWeek, CapString.LastMonth, CapString.LastYear];
	
	// DATE
	$scope.date = Date.now();
	
	// INSTANTIATE COMMENT SCOPE VARIABLE HERE
	$scope.comment = {};

	// POST $HTTP REQUEST FOR NOTE
	$scope.postNoteHistory = function() {
		$http({
			method: 'POST',
			data: $.param({
				jsonLogData: JSON.stringify($scope.comment),
				householdId: $scope.householdId
			}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			url: '../../../crm/consumer/saveConsumerCommentLog'
			
			//headers : { 'Content-Type' : 'application/json' }
		}).success(function(data) {
			//alert("post request completed");
			$scope.comment.mainData.desc = '';
			$scope.getRequest();
			//console.log("========sending==========");
			//debugger
			$scope.histories = data;
			//console.log($scope.histories);
			//console.log("========sending==========");
			if (data == null) {
			  //alert(" Something went wrong.. please contact admin.");
			  return;
		    }
			if (data.status) {
			  //console.log("==================");
			  //console.log('Works: ' + data.message);
			  $scope.histories = data;
			  //console.log("==================");
			  //console.log($scope.histories);
			} else {
			  //console.log('Fail:' + data.message);
			  //alert(data.message);
			}
		  });
	};
	
	// POST $HTTP REQUEST FOR CALL
	$scope.postCallHistory = function() {
		$http({
			method: 'POST',
			data: $.param({
				jsonLogData: JSON.stringify($scope.comment),
				householdId: $scope.householdId
			}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			url: '../../../crm/consumer/saveConsumerCallLog'
			//headers : { 'Content-Type' : 'application/json' }
		}).success(function(data) {
			//alert("post request completed");
			$scope.comment.mainData.desc = '';
			$scope.getRequest();
			//console.log("========sending==========");
			//debugger
			$scope.histories = data;
			//console.log($scope.histories);
			//console.log("========sending==========");
			if (data == null) {
			  //alert(" Something went wrong.. please contact admin.");
			  return;
		    }
			if (data.status) {
			  //console.log("==================");
			  //console.log('Works: ' + data.message);
			  $scope.histories = data;
			  //console.log("==================");
			  //console.log($scope.histories);
			} else {
			  //console.log('Fail:' + data.message);
			  //alert(data.message);
			}
		  });
	};
}]);