<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
	<div class="row-fluid">
		<%-- <h1><issuerLogo:view issuerId="${plan.issuer.id}"/> ${planName}</h1> --%>
		<c:if test="${isBulkAction != 'YES'}">
			<h1><img class="resize-img" src="<c:url value="/admin/issuer/company/profile/logo/hid/${plan.issuer.hiosIssuerId}"/>"/> ${planName}</h1>
		</c:if>
		
		<c:if test="${isBulkAction == 'YES'}">
			<h1>${issuerObj.name}  ${planCount}  <spring:message code='label.plans'/></h1>
		</c:if>
	</div>
	
	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		 <c:if test="${isBulkAction == 'YES'}">
		  	<div class="header margin10-b">
       			<h4>${issuerObj.name}</h4>       			
       		</div>
		  </c:if>
		  <c:if test="${isBulkAction != 'YES'}">
		 	 <div class="header margin10-b">
       			<h4> ${planName}</h4>
       		</div>
		  </c:if>
       		<!--  beginning of side bar -->
	            <ul class="nav nav-tabs nav-stacked">
     				<li><spring:message  code='pgheader.planApprovalMsg'/></li>
			   </ul>

        </div><!-- end of span3 -->
		
		<div class="span9" id="rightpanel">
        	<div class="header">
                <h4 class="span10">Issuer Verification of the SADP</h4>
            </div>
            <form class="form-horizontal gutter10" id="frmVerifyPlan" name="frmVerifyPlan" action="<c:url value="/issuer/qdp/verifyplan" />" method="POST">
            	<df:csrfToken/>
            	 <input type="hidden" name="moduleName" id="moduleName" value="<encryptor:enc value="${moduleName}"></encryptor:enc>"/>
            	  <input type="hidden" id="planYearSelected" name="planYearSelected"  value="${planYearSelected}">
            	<c:if test="${isBulkAction != 'YES'}">
            		 <input type="hidden" name="refId" id="refId" value="<encryptor:enc value="${planId}"></encryptor:enc>"/>
            	</c:if>
            	
            	<c:if test="${isBulkAction == 'YES'}">
            		<spring:message code='message.plansInfo'/>${planNumbers}
            		 <input type="hidden" name="isBulkAction" id="isBulkAction"  value="YES">
            		 <input type="hidden" name="planIds" id="planIds"  value="${planIds}">
            	
            	</c:if>
            	
            	<div class="control-group">
                    <label for="statement" class="control-label"><spring:message  code='label.planStatement'/></label>
                    <div class="controls">
                    	 <textarea style="width: 333px; height: 111px;" rows="3" id="statementTextbox" class="input-xlarge noresize" readonly="readonly"><spring:message  code='message.QDPVerificationStatement'/></textarea>
                    	<input type="hidden" name="statements" value="<spring:message  code='message.QDPVerificationStatement'/>">
                    </div>	
                </div><!-- end of control-group -->
                
                <div class="control-group">
                    <label for="statement" class="control-label"><spring:message  code='label.planDeclaration'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                    <div class="controls">
                    	<input type="checkbox" id="declaration" name="declaration" />
                        <span class="marginL10"><spring:message  code='label.planDeclarationText'/></span>
                        <div id="declaration_error"></div>
                    </div>	
                </div><!-- end of control-group -->
                
                <div class="control-group">
                    <label for="statement" class="control-label"><spring:message  code='label.firstName'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                    <div class="controls">
                    	<input type="text" id="firstName" name="firstName" class="input-large" placeholder="<spring:message  code='label.firstName'/>" maxlength="50"/>
                    	<div id="firstName_error"></div> 
                    </div>	
                </div><!-- end of control-group -->
                
                <div class="control-group">
                    <label for="statement" class="control-label"><spring:message  code='label.lastName'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                    <div class="controls">
                        <input type="text" id="lastName" name="lastName" class="input-large" placeholder="<spring:message  code='label.lastName'/>" maxlength="50"/>
						<div id="lastName_error"></div>
                    </div>	
                </div><!-- end of control-group -->
                
                <div class="control-group">
                    <label for="eSignature" class="control-label"><spring:message  code='label.planeSignature'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                    <div class="controls">
                    	<input type="text" id="eSignature" name="eSignature" class="input-large" placeholder="<spring:message  code='label.planeSignatureText'/>" maxlength="100" />
                    	<div id="eSignature_error"></div> 
                    </div>	
                </div><!-- end of control-group -->
                
                <div class="control-group">
                    <label for="eSignature" class="control-label"><spring:message  code='label.planVerifyDate'/></label>
                    <div class="controls paddingT5">${currDate}</div>	
                </div><!-- end of control-group -->
                <input type="hidden" name="validFirstName" id="validFirstName"  value="${validFirstName}">
				<input type="hidden" name="validLastName" id="validLastName"  value="${validLastName}">
                <div class="form-actions">
                    <input type="submit" name="planVerifyButton" id="planVerifyButton" class="btn btn-primary" value="<spring:message  code='label.planVerify'/>" title="<spring:message  code='label.planVerify'/>"/>
                </div>	
               
            </form>
		</div><!-- end of .span9 -->
	</div><!--  end of .row-fluid -->
</div><!-- end of .gutter10 -->

<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<script type="text/javascript">
$.validator.addMethod("isFirstNameValid", function() {
	return validateFirstName();
});

$.validator.addMethod("isLastNameValid", function() {
	return validateLastName();
});

var validator = $("#frmVerifyPlan").validate({ 
	onfocusout: false,
    onkeyup: false,
	rules : {
		declaration : {required : true},
		firstName : {required : true, maxlength:50, isFirstNameValid:true},
		lastName : { required : true, maxlength:50, isLastNameValid:true},
		eSignature : { required : true, maxlength:100}
	},
	messages : {
		declaration : { required : "<span><em class='excl'>!</em><spring:message  code='err.checkbox' javaScriptEscape='true'/></span>"},
		firstName : { required : "<span><em class='excl'>!</em><spring:message  code='err.firstName' javaScriptEscape='true'/></span>", isFirstNameValid:"Please enter your valid first name."},
		lastName : { required : "<span><em class='excl'>!</em><spring:message  code='err.lastName' javaScriptEscape='true'/></span>", isLastNameValid:"Please enter your valid last name."},
		eSignature : { required : "<span><em class='excl'>!</em><spring:message  code='err.eSignature' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error');
	} 
});

$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	})
})

function validateFirstName() {
	var firstName = $('#firstName').val();	
	var validFirstName = $('#validFirstName').val();	
	if(firstName.toLowerCase() == validFirstName.toLowerCase()) {
		return true;
	} else {		
		return false;
	} 
}

function validateLastName() {
	var lastName = $('#lastName').val();
	var validLastName = $('#validLastName').val();
	if(lastName.toLowerCase() == validLastName.toLowerCase()) {
		return true;
	} else {		
		return false;
	} 
}
</script>
