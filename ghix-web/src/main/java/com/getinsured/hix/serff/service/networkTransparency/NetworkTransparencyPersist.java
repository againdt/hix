package com.getinsured.hix.serff.service.networkTransparency;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.NetworkTransparency;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.planmgmt.repository.INetworkTransparencyRepository;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyVO.NetworkTransparencyDataVO;

/**
 *-----------------------------------------------------------------------------
 * HIX-84043 Load and persist Network transparency rating.
 *-----------------------------------------------------------------------------
 * 
 * Class is used to persist data in Network Transparency table.
 * @author Bhavin Parmar
 * @since July 25, 2016
 */
@Service("networkTransparencyPersist")
public class NetworkTransparencyPersist {

	private static final Logger LOGGER = Logger.getLogger(NetworkTransparencyPersist.class);

	@Autowired private INetworkTransparencyRepository iNetworkTransparencyRepository = null;
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;

	public boolean persistNetworkTransparencyData(NetworkTransparencyVO networkTransMainVO, SerffPlanMgmt trackingRecord, int userId) throws GIException {

		LOGGER.info("persistNetworkTransparencyData() Start");
		boolean hasDataPersistingComplete = false;
		EntityManager entityManager = null;
		List<NetworkTransparency> newNetworkTransparencyList = new ArrayList<NetworkTransparency>();

		try {
			entityManager = entityManagerFactory.createEntityManager(); // Get DB connection using Entity Manager Factory
			entityManager.getTransaction().begin(); // Begin Transaction

			String hisoIssuerId = networkTransMainVO.getHiosIssuerID();
			int applicableYear = Integer.valueOf(networkTransMainVO.getPlanYear());
			NetworkTransparency newNetworkTransparency = null;

			for (NetworkTransparencyDataVO networkTransparencyData : networkTransMainVO.getNetworkTransData()) {
				newNetworkTransparency = setNewNetworkTransparency(networkTransparencyData, hisoIssuerId, applicableYear, userId);
				newNetworkTransparencyList.add(newNetworkTransparency);
			}
			hasDataPersistingComplete = saveNetworkTransparencyList(newNetworkTransparencyList, hisoIssuerId, applicableYear, userId, trackingRecord, entityManager);
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred in persisting Network Transparency Data: " + ex.getMessage(), ex);
			hasDataPersistingComplete = false;
			trackingRecord.setPmResponseXml(ex.getMessage());
			throw new GIException(ex);
		}
		finally {
			/*LOGGER.info("Has saved Network Transparency List: " + CollectionUtils.isEmpty(newNetworkTransparencyList));

			if (!CollectionUtils.isEmpty(newNetworkTransparencyList)) {
				hasDataPersistingComplete = true;
			}*/
			hasDataPersistingComplete = completeTransaction(hasDataPersistingComplete, entityManager);
			updateTrackingRecord(hasDataPersistingComplete, newNetworkTransparencyList, trackingRecord);

			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
			newNetworkTransparencyList.clear();
			newNetworkTransparencyList = null;
			LOGGER.info("persistNetworkTransparencyData() End with value hasDataPersistingComplete: " + hasDataPersistingComplete);
		}
		return hasDataPersistingComplete;
	}

	private boolean saveNetworkTransparencyList(List<NetworkTransparency> newNetworkTransparencyList, String hisoIssuerId, int applicableYear,
			int userId, SerffPlanMgmt trackingRecord, EntityManager entityManager) throws GIException {

		LOGGER.debug("saveNetworkTransparencyList() Start");
		boolean hasMergedEntity = true;

		try {
			softDeleteExixtingNetworkTransparency(entityManager, hisoIssuerId, applicableYear, userId);

			for (NetworkTransparency networkTransparency : newNetworkTransparencyList) {
				mergeEntityManager(entityManager, networkTransparency);
			}
		}
		catch (Exception ex) {
			hasMergedEntity = false;
			throw new GIException(ex);
		}
		finally {
			LOGGER.debug("saveNetworkTransparencyList() End");
		}
		return hasMergedEntity;
	}

	/**
	 * Method is used to soft delete all existing Network Transparency Data.
	 */
	private void softDeleteExixtingNetworkTransparency(EntityManager entityManager, String hisoIssuerId, int applicableYear, int userId) {

		LOGGER.debug("softDeleteExixtingNetworkTransparency() Start");

		try {
			List<NetworkTransparency> existingNetworkTransparencyList = iNetworkTransparencyRepository.getAllNetworkTransparencyDataforIssuer(hisoIssuerId, applicableYear);
			LOGGER.debug("Fetching list of exixting Network Transparency");

			if (!CollectionUtils.isEmpty(existingNetworkTransparencyList)) {

				for (NetworkTransparency existingNetworkTransparency : existingNetworkTransparencyList) {
					existingNetworkTransparency.setIsDeleted(NetworkTransparency.IS_DELETED.Y.name());
					existingNetworkTransparency.setLastUpdateBy(userId);
					mergeEntityManager(entityManager, existingNetworkTransparency);
				}
			}
			else {
				LOGGER.debug("Existing Network Transparency record is not present for issuer : " + hisoIssuerId);
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("softDeleteExixtingNetworkTransparency() End");
		}
	}

	/**
	 * Set Network Transparency Data in POJO.
	 */
	private NetworkTransparency setNewNetworkTransparency(NetworkTransparencyDataVO networkTransparencyData,
			String hisoIssuerId, int applicableYear, int userId) {

		LOGGER.debug("setNewNetworkTransparency() Start");
		NetworkTransparency networkTransparency = new NetworkTransparency();

		try {
			networkTransparency.setHisoIssuerId(hisoIssuerId);
			networkTransparency.setNetworkID(networkTransparencyData.getNetworkID());
			networkTransparency.setIssuerPlanNumber(networkTransparencyData.getPlanHiosID());
			networkTransparency.setCountyName(networkTransparencyData.getCounty());
			networkTransparency.setCountyFips(networkTransparencyData.getCountyFips());
			networkTransparency.setRating(networkTransparencyData.getRating());
			networkTransparency.setApplicableYear(applicableYear);
			networkTransparency.setIsDeleted(NetworkTransparency.IS_DELETED.N.name());
			networkTransparency.setCreatedBy(userId);
			networkTransparency.setLastUpdateBy(userId);
		}
		finally {
			LOGGER.debug("setNewNetworkTransparency() End");
		}
		return networkTransparency;
	}

	/**
	 *  Method is used to update tracking record of SERFF Plan Management table.
	 * 
	 * @param isDataProcessingComplete - Whether data processing is completed
	 * @param savedPlanList - Plans those are saved successfully
	 * @param failedPlanVOList - Plans those are failed to persist
	 * @param trackingRecord - Serff Plan Mgmt record.
	 */
	private void updateTrackingRecord(boolean isDataProcessingComplete, List<NetworkTransparency> savedNetworkTransList, SerffPlanMgmt trackingRecord) {

		LOGGER.debug("updateTrackingRecord() Start");

		try {
			if (null == trackingRecord) {
				return;
			}

			if (isDataProcessingComplete) {
				//Process completed. No plans saved.All Certified plans or All plan's validation failed.
				if (CollectionUtils.isEmpty(savedNetworkTransList)) {
					trackingRecord.setRequestStateDesc("Process completed successfully. No Network Transparency Data Loaded. Please see status message.");
				}
				else {
					trackingRecord.setRequestStateDesc("Network Transparency Data have been Loaded Successfully.");
				}
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
				LOGGER.info(trackingRecord.getRequestStateDesc());
			}
			else {
				//Exception occurred while committing or merging entity.
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
				trackingRecord.setRequestStateDesc("Error occurred while saving Network Transparency Data. Check PM Response for details.");
				LOGGER.error(trackingRecord.getRequestStateDesc());
			}
		}
		finally {

			if (null != trackingRecord && StringUtils.isBlank(trackingRecord.getPmResponseXml())
					&& StringUtils.isNotBlank(trackingRecord.getRequestStateDesc())) {
				trackingRecord.setPmResponseXml(trackingRecord.getRequestStateDesc());
			}
			LOGGER.debug("updateTrackingRecord() End");
		}
	}

	/**
	 * Method is used to Commit/Rollback transaction depends on Status argument.
	 * 
	 * @param commitRequestStatus - Request to whether commit the transaction
	 * @param entityManager - The entity manager instance
	 * @return - Whether transaction is committed
	 * @throws GIException
	 */
	private boolean completeTransaction(boolean commitRequestStatus, EntityManager entityManager) throws GIException {

		LOGGER.debug("completeTransaction() Start");
		boolean commitResponseStatus = false;

		try {

			if (null != entityManager && entityManager.isOpen()) {

				if (commitRequestStatus) {
					entityManager.getTransaction().commit();
					commitResponseStatus = true;
					LOGGER.debug("Transaction has been Commited");
				}
				else {
					entityManager.getTransaction().rollback();
					commitResponseStatus = false;
					LOGGER.warn("Transaction has been Rollback");
				}
			}
		}
		catch (Exception ex) {
			commitResponseStatus = false;
			LOGGER.error("Error occured while commiting Transaction " + commitResponseStatus, ex);
			throw new GIException(ex);
		}
		finally {
			LOGGER.debug("completeTransaction() End with Commit Status : "  + commitResponseStatus);
		}
		return commitResponseStatus;
	}

	/**
	 * Merges entities by using provided entity manager.
	 * 
	 * @param entityManager  -The entity manager instance.
	 * @param saveObject - Entity to be saved in database.
	 * @return - Entity saved in database.
	 */
	private Object mergeEntityManager(EntityManager entityManager, Object saveObject) {

		LOGGER.debug("mergeEntityManager() Start");
		Object savedObject = null;

		if (null != saveObject && null != entityManager && entityManager.isOpen()) {
			savedObject = entityManager.merge(saveObject);

			if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(saveObject.getClass() + " bean has been Persisted Successfully.");
		}
		}
		LOGGER.debug("mergeEntityManager() End");
		return savedObject;
	}
}
