package com.getinsured.hix.indportal.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * HIX-59164
 * Display Enrollment History for the Household
 * 
 * @author sahoo_s
 */
public class EnrollmentHistory {

	private List<EnrollmentHistoryEntry> activeHealthEnrollments = new ArrayList<EnrollmentHistoryEntry>();
	private List<EnrollmentHistoryEntry> activeDentalEnrollments = new ArrayList<EnrollmentHistoryEntry>();
	private List<EnrollmentHistoryEntry> inactiveHealthEnrollments = new ArrayList<EnrollmentHistoryEntry>();
	private List<EnrollmentHistoryEntry> inactiveDentalEnrollments = new ArrayList<EnrollmentHistoryEntry>();
	
	public List<EnrollmentHistoryEntry> getActiveHealthEnrollments() {
		return activeHealthEnrollments;
	}
	public void setActiveHealthEnrollments(
			List<EnrollmentHistoryEntry> activeHealthEnrollments) {
		this.activeHealthEnrollments = activeHealthEnrollments;
	}
	public List<EnrollmentHistoryEntry> getActiveDentalEnrollments() {
		return activeDentalEnrollments;
	}
	public void setActiveDentalEnrollments(
			List<EnrollmentHistoryEntry> activeDentalEnrollments) {
		this.activeDentalEnrollments = activeDentalEnrollments;
	}
	public List<EnrollmentHistoryEntry> getInactiveHealthEnrollments() {
		return inactiveHealthEnrollments;
	}
	public void setInactiveHealthEnrollments(
			List<EnrollmentHistoryEntry> inactiveHealthEnrollments) {
		this.inactiveHealthEnrollments = inactiveHealthEnrollments;
	}
	public List<EnrollmentHistoryEntry> getInactiveDentalEnrollments() {
		return inactiveDentalEnrollments;
	}
	public void setInactiveDentalEnrollments(
			List<EnrollmentHistoryEntry> inactiveDentalEnrollments) {
		this.inactiveDentalEnrollments = inactiveDentalEnrollments;
	}
	
	@JsonIgnore
	public List<EnrollmentHistoryEntry> fullList(){
		
		List<EnrollmentHistoryEntry> allEnrollments = new ArrayList<EnrollmentHistoryEntry>();

		List<EnrollmentHistoryEntry> activeEnrollments = Stream.concat(activeHealthEnrollments.stream(), activeDentalEnrollments.stream()).collect(Collectors.toList());
		List<EnrollmentHistoryEntry> inActiveEnrollments = Stream.concat(inactiveHealthEnrollments.stream(), inactiveDentalEnrollments.stream()).collect(Collectors.toList());
		
		allEnrollments = Stream.concat(activeEnrollments.stream(), inActiveEnrollments.stream()).collect(Collectors.toList());
		
		return allEnrollments;
	}
}
