package com.getinsured.hix.batch.enrollment.service;

import java.util.List;

import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.util.exception.GIException;

public interface EnrollmentEHBUpdateService {
	
	public void updateEnrollmentEHBAmount(Integer enrollmentId) throws GIException;
	/**
	 * 
	 * @param jobName
	 * @return
	 */
	List<BatchJobExecution> getRunningBatchList(String jobName);
	
	/**
	 * 
	 * @param coverageYear
	 * @return
	 */
	List<Integer> getUniqueHealthEnrollmentIds(String coverageYear);

}
