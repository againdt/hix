<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.getinsured.hix.platform.util.*"%>
<%@page import="com.getinsured.hix.util.GhixConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.AEEConfiguration" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<jsp:useBean id="date" class="java.util.Date" />
<fmt:formatDate var="now" value="${date}" pattern="yyyy" />
<%!
	String currStateExchangeType = null;
%>
<%
 currStateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
 String agencyPortalEnable = DynamicPropertiesUtil.getPropertyValue(AEEConfiguration.AEEConfigurationEnum.AGENCY_PORTAL_ENABLE);
 String householdCaseID =  (String) request.getSession().getAttribute("householdCaseId");
 String userName =  (String) request.getSession().getAttribute("USER_NAME");
 String userFirstName =  (String) request.getSession().getAttribute("consumerFirstName");
 String userLastName =  (String) request.getSession().getAttribute("consumerlastName");
 String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
%>
<sec:authorize access="isAuthenticated()" var="isUserAuthenticated" />
<c:set var="calheersEnv" value='<%=DynamicPropertiesUtil.getPropertyValue("global.calheersEnv")%>'></c:set>
<c:set var="userName" value="<%=userName%>" />
<c:set var="householdCaseID" value="<%=householdCaseID%>" />
<c:set var="userFirstName" value="<%=userFirstName%>" />
<c:set var="userLastName" value="<%=userLastName%>" />
<input type="hidden" id="userName" name="userName" value="<%=userName%>">
<input type="hidden" id="householdCaseID" name="householdCaseID" value="<%=householdCaseID%>">
<input type="hidden" id="isUserAuthenticated" value="${isUserAuthenticated=='true'}"/>
<script>
    $(document).ready(function(){
        var currentYear = new Date().getFullYear();
        $('#curYear').html(currentYear);

        // Data layer variable to be pushed at the end of shell.jsp
        dl_caseID = '${householdCaseID}';
    });
</script>
<footer data-autamation-id="footer" role="contentinfo">
    <div class="container">
    <c:if test="${isUserAuthenticated=='false'}">
            <p>
                Copyright &copy; <span id="curYear"></span> Covered California
            </p>
    </c:if>
    <c:if test="${isUserAuthenticated=='true'}">
        <div class="col-xs-6 col-sm-6 footer-links">
            <%-- <c:if test="${userActiveRoleName != 'agency_manager'}"> --%>
                <c:if test="${!fn:containsIgnoreCase(userActiveRoleName,'agency_manager') 
                    && !fn:containsIgnoreCase(userActiveRoleName,'broker')
                    && !fn:containsIgnoreCase(userActiveRoleName,'assisterenrollmententity')}">
                <ul class="inline">
                    <%if (userFirstName != null){%>
                        <%=userFirstName %>
                    <%}%>
                    <%if (userLastName != null){%>
                        <%=userLastName %>
                    <%}%>
                    <c:if test="${not empty householdCaseID}">
                        &#124;<li><spring:message code="pd.label.footer.caseNumber"/> ${householdCaseID}</li>
                    </c:if>
                </ul>
            </c:if>
        </div>
    </c:if>
    <%
		if(GhixConstants.GHIX_ENVIRONMENT.equals("DEV")){
	 %>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <ul>
	        <li>
               <c:set var="individualRoleName">
                   <encryptor:enc value="individual" isurl="true" />
               </c:set>
               <a href="<c:url value='/account/signup/${individualRoleName}' />">Individual</a>
               <br/>
               <%if(agencyPortalEnable.equalsIgnoreCase("Y")){ %>
                   <c:set var="agencyRoleName">
                       <encryptor:enc value="agency_manager" isurl="true" />
                   </c:set>
                   <a href="<c:url value='/account/signup/${agencyRoleName}' />">Agency</a>
                   <br/>
                   <c:set var="approvedAdminStaffL1">
                       <encryptor:enc value="APPROVEDADMINSTAFFL1" isurl="true" />
                   </c:set>
                   <a href="<c:url value='/account/signup/${approvedAdminStaffL1}' />">Admin Staff L1</a>
                   <br/>
                   <c:set var="approvedAdminStaffL2">
                       <encryptor:enc value="APPROVEDADMINSTAFFL2" isurl="true" />
                   </c:set>
                   <a href="<c:url value='/account/signup/${approvedAdminStaffL2}' />">Admin Staff L2</a>
                   <br/>
               <% } %>
               <c:set var="brokerRoleName">
                   <encryptor:enc value="broker" isurl="true" />
               </c:set>
               <a href="<c:url value='/account/signup/${brokerRoleName}' />">Health Insurance Agent/Broker</a>
               <br/>
               <c:set var="assisterRoleName">
                   <encryptor:enc value="assisterenrollmententity" isurl="true" />
               </c:set>
               <a href="<c:url value="/account/signup/${assisterRoleName}"/>">Enrollment Entities</a>
	        </li>
        </ul>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <ul>
               <li>BRANCH NAME: <%= GhixPlatformConstants.BUILD_BRANCH_NAME %><br>BUILD NUMBER: <%= GhixPlatformConstants.BUILD_NUMBER %><br>BUILD DATE: <%= GhixPlatformConstants.BUILD_DATE %></li>
            </ul>
        </div>
     <%  }%>


    <%--<c:if test="${isUserAuthenticated=='false'}">--%>
        <%--<div class="col-xs-6 col-sm-6 ">--%>
            <%--Copyright © 2018 Covered California--%>
        <%--</div>--%>
    <%--</c:if>--%>
		<%-- <div class="col-xs-6 col-sm-6 footer-links">
		<ul>
			<li><h3>Announcements</h3></li>
			<li class="announcements">No announcements</li>
			<li>View all announcements</li>
			<li>
	 				<%
					if(GhixConstants.GHIX_ENVIRONMENT.equals("DEV")){
					%>
						<c:set var="individualRoleName">
							<encryptor:enc value="individual" isurl="true" />
						</c:set>
						<a href="<c:url value='/account/signup/${individualRoleName}' />">Individual</a>
						<br/>
						<%if(agencyPortalEnable.equalsIgnoreCase("Y")){ %>
							<c:set var="agencyRoleName">
								<encryptor:enc value="agency_manager" isurl="true" />
							</c:set>
							<a href="<c:url value='/account/signup/${agencyRoleName}' />">Agency</a>
							<br/>
							<c:set var="approvedAdminStaffL1">
								<encryptor:enc value="APPROVEDADMINSTAFFL1" isurl="true" />
							</c:set>
							<a href="<c:url value='/account/signup/${approvedAdminStaffL1}' />">Admin Staff L1</a>
							<br/>
							<c:set var="approvedAdminStaffL2">
								<encryptor:enc value="APPROVEDADMINSTAFFL2" isurl="true" />
							</c:set>
							<a href="<c:url value='/account/signup/${approvedAdminStaffL2}' />">Admin Staff L2</a>
							<br/>
						<% } %>
						<c:set var="brokerRoleName">
							<encryptor:enc value="broker" isurl="true" />
						</c:set>
						<a href="<c:url value='/account/signup/${brokerRoleName}' />">Health Insurance Agent/Broker</a>
						<br/>
						<c:set var="assisterRoleName">
							<encryptor:enc value="assisterenrollmententity" isurl="true" />
						</c:set>
						<a href="<c:url value="/account/signup/${assisterRoleName}"/>">Enrollment Entities</a>
	 				<%  }%>
			</li>

			<li style="white-space: nowrap;"><a href="#">Branch Name: <%= GhixPlatformConstants.BUILD_BRANCH_NAME %></a></li>
			<li style="white-space: nowrap;"><a href="#">Build Number: <%= GhixPlatformConstants.BUILD_NUMBER %></a></li>
			<li style="white-space: nowrap;"><a href="#">Build Date: <%= GhixPlatformConstants.BUILD_DATE %></a></li>


		</ul>
	</div>
	<div class="col-xs-12 col-sm-4 footer-links">
		<ul>
			<li><h3>Manage My Application</h3></li>
			<li>Withdraw application</li>
		</ul>
	</div>
	<div class="col-xs-12 col-sm-4 footer-links">
		<ul>
			<li><h3>More Actions</h3></li>
			<li>My Profile</li>
			<li>Secured Mailbox (0)</li>
			<li>Authorized Representative</li>
			<li>Manage Delegates</li>
			<li>Shop and Compare</li>
			<li>Certified Enrollment Counselor home</li>
			<li><a id="account-home-footer-moreActions-btn-downloadPDF" href="http://www.coveredca.com/apply/#paper-applications" target="_blank"><span>Download PDF application</span></a></li>
			<li><a id="account-home-footer-moreActions-btn-adobePDFReader" href="https://get.adobe.com/reader/" target="_blank"><span>Get Adobe PDF Reader</span></a></li>
		</ul>
	</div> --%>
</footer><!-- footer -->
