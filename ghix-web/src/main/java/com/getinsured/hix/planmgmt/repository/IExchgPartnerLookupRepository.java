package com.getinsured.hix.planmgmt.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.getinsured.hix.model.ExchgPartnerLookup;


public interface IExchgPartnerLookupRepository extends JpaRepository<ExchgPartnerLookup, Integer> {
	
	Page<ExchgPartnerLookup> findByHiosIssuerId(String hiosIssuerId, Pageable pageable);

}

