<script type="text/ng-template" id="maritalstatus-incarceration.jsp">
<form class="form-horizontal"  id="maritalIncarcerationForm" name="maritalIncarcerationForm" >
  <div class="gutter10" ng-init="householdMember = jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember[jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1]">
    <h3><spring:message code="label.lce.incarceration"/></h3>

    <div class="control-group">
      <div>
        <label for="hidIncarcerated">Is <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong> incarcerated? <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"> <span class="validation-error-text-container" ng-if="submitted && signAndSubmit.isIncarcerated.$error.required">Required</span> <input type="hidden" id="hidIncarcerated" /></label>
        <label class="radio-inline">
          <input type="radio" ng-model="householdMember.incarcerationStatus.incarcerationStatusIndicator" ng-value="true" name="isIncarcerated" required >
          <spring:message code="label.lce.yes"/>
        </label>
        <label class="radio-inline">
          <input type="radio" ng-model="householdMember.incarcerationStatus.incarcerationStatusIndicator" ng-value="false" name="isIncarcerated" required ng-change="householdMember.incarcerationStatus.incarcerationPendingDispositionIndicator=false;updateGlobalIncarcerationFlag();">
          <spring:message code="label.lce.no"/>
        </label>
      </div>
    </div><!-- control-group ends -->

    <div class="control-group margin30-t" ng-if="householdMember.incarcerationStatus.incarcerationStatusIndicator">
      <div>
        <label for="hidPendingDispo">Is this person incarcerated pending disposition of charges? <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
          <span class="validation-error-text-container" ng-if="submitted && signAndSubmit.pedingDisposition.$error.required"><spring:message code="label.lce.pleaseselectstate"/></span>
			<input type="hidden" id="hidPendingDispo" />
        </label>
        <label class="radio-inline">
          <input type="radio" ng-model="householdMember.incarcerationStatus.incarcerationPendingDispositionIndicator" ng-value="true" > <spring:message code="label.lce.yes"/>
        </label>
        <label class="radio-inline">
          <input type="radio" ng-model="householdMember.incarcerationStatus.incarcerationPendingDispositionIndicator" ng-value="false" > <spring:message code="label.lce.no"/>
        </label>
      </div>
    </div><!-- control-group ends -->

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a role="button" tabindex="0" class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a role="button" ng-if="maritalIncarcerationForm.$valid" tabindex="0" class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
      <a role="button" ng-if="!maritalIncarcerationForm.$valid" tabindex="0" class="btn btn-primary" ng-click="$parent.submitted=true"><spring:message code="label.lce.next"/></a>
    </dl>

  </div>
</form>
</script>