<!--Subsidy Waiver Page for Private Exchange-->

<!DOCTYPE html>
<html>
	<head>
		<title>Shop for Health Insurance</title>
	</head>
	
<body>
<div class="gutter10">
	<div class="row-fluid">
		<h1>Shop for Health Insurance</h1>
	</div>
	
	<div class="row-fluid">
		<div class="span3">
			<div class="gray">
			<h4 class="margin0">Steps</h4>
			</div>
			<ol class="nav nav-list">
				<li class="active"><a href="#">Eligibility</a></li>
				<li><a href="#">Plan Selection</a></li>
				<li><a href="#">Checkout</a></li>
			</ol>
		</div>
		<!-- /.span3-->
		
		<div class="span9">
			<div class="graydrkaction margin0">
				<h4 class="span10">Subsidy Waiver</h4>
			</div>
			<div class="gutter10">
				<form class"form-horizontal" method="POST">
					<div class="gutter10">
						By checking the box below you are waiving your right to any health insurance subsidies. Once you waive your right to a 
						subsidy you may not claim one until next annual open enrollment
						unless you experience a qualified event that would change your subsidy eligibility determination. Even if we have determined 
						you to be unlikely
						eligible for a subsidy you always have the optino to go through a detailed subsidy determination on the Federal Marketplace.
						If you would like to go through the Advanced Federal Eligibility questionnaire please click "Cancel."
					</div>
					<div class="controls">
						<label class="checkbox"><input type="checkbox">I agree to waive my right to health subsidies for 2014.</label>
					</div>
					
					<div class="form-actions">
						<a class="btn" type="button" title="Cancel" href="#">Cancel</a>
						<input type="button" class="btn btn-primary" value="Save" title="Save">
					</div>
				</form>
			</div>
		</div>
		<!-- /.span9 -->
	
	</div>
	<!--/.row-fluid-->

</div>
<!-- ./gutter10 -->
</body>
</html>