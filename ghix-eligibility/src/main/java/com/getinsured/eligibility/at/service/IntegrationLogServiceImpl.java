package com.getinsured.eligibility.at.service;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.getinsured.iex.ssap.model.SsapIntegrationLog;
import com.getinsured.iex.ssap.repository.SsapIntegrationLogRepository;

@Service("integrationLogService")
public class IntegrationLogServiceImpl implements IntegrationLogService {

	@Autowired
	private SsapIntegrationLogRepository ssapIntegrationLogRepository;

	@Override
	public SsapIntegrationLog save(SsapIntegrationLog integrationLog) {
		return ssapIntegrationLogRepository.save(integrationLog);
	}
	
	@Override
	public SsapIntegrationLog findByGiWsPayloadId(long giWsPayloadId) {
		return ssapIntegrationLogRepository.findByGiWsPayload(giWsPayloadId);
	}

	@Override
	@Async
	public void save(String payload, String serviceName, String status, Long ssap_pk, Long giWsPayloadId) {
		SsapIntegrationLog iL = new SsapIntegrationLog();
		iL.setCreatedDate(new TSDate());
		iL.setPayload(payload);
		iL.setServiceName(serviceName);
		iL.setStatus(status);
		iL.setSsapApplicationId(ssap_pk);
		iL.setGiWsPayloadId(giWsPayloadId);
		ssapIntegrationLogRepository.save(iL);
	}

}
