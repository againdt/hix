package com.getinsured.ssap.model.financial;

import java.util.List;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Describes IFSV payload.
 *
 * @author Yevgen Golubenko
 * @since 6/11/19
 */
@JsonRootName(value = "myRootName")
public class IfsvPayload {
  private List<IfsvTaxHousehold> taxHousehold;

  @JsonProperty("requestID")
  private String requestId;

  public List<IfsvTaxHousehold> getTaxHousehold() {
    return taxHousehold;
  }

  public void setTaxHousehold(List<IfsvTaxHousehold> taxHousehold) {
    this.taxHousehold = taxHousehold;
  }

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", IfsvPayload.class.getSimpleName() + "[", "]")
        .add("taxHousehold=" + taxHousehold)
        .add("requestId='" + requestId + "'")
        .toString();
  }
}
