 <%@ include file="../include.jsp" %>

 <div id="appscr59A" style="display:none;">
	<div class="row-fluid">
		<div class="page-title">
			<h5 class="titleForPages"><spring:message code="ssap.page9.familyAndHousehold" text="Family & Household" /></h5>
		</div>
		
		<div class="container-fluid">
			<div class="control-group">
				<div class="alert alert-info">
					<div class="non_financial_hide">
						<p class="margin10-t">
							<spring:message code="ssap.page9A.familyAndHouseholdText1" text="We need to know about everyone included on your same federal Income tax return (if you file taxes) AND all the family members who live with you, even if they are not applying for health insurance." /> 
						</p>
						<p>
							<spring:message code="ssap.page9A.familyAndHouseholdText2" text="We will match you with programs based on your income and family size, so we need this information to make sure you get the most help possible." />
						</p>
						<p class="margin10-t">
							<spring:message code="ssap.page9A.save" /> 
						</p>
					</div>
					<div class="financial_hide">
						<p class="margin10-t">
							<spring:message code="ssap.page9A.nonFinsncial" text="In this section, we will ask for more information about everyone who is applying for health Insurance." /> 
						</p>
						<p class="margin10-t">
							<spring:message code="ssap.page9A.save"/> 
						</p>
					</div>	
				</div>
				<p>
					<em>
						<spring:message code="ssap.page9A.familyAndHouseholdText3" text="All fields on this Family & Household section are required unless otherwise indicated." /> 
					</em>
				</p>
			</div>
				<hr>
				
				<div class="control-group margin10-t">
					<strong><spring:message code="ssap.page2.youMayNeed" text="You may need:" /></strong> 
					
					<ul class="checklist gutter10">
						<li><spring:message code="ssap.page9A.youMayNeedText1" text="Social Security numbers" /></li>
						<li><spring:message code="ssap.page2.youMayNeed3"/> <a href="#" class="ssapTooltip" data-html="true" data-toggle="tooltip" rel="tooltip" data-placement="bottom" title="<p><strong><spring:message code="ssap.page2.youMayNeed3.1"/></strong></p><spring:message code="ssap.page2.youMayNeed3.tooltip"/>"><spring:message code="ssap.page2.youMayNeed3.1"/></a></li>
						<%-- <li><spring:message code="ssap.page9A.youMayNeedText3" text="Birth dates" /></li> --%>
					</ul>
				</div>

				
				<img alt="call us for help" src="<c:url value="/resources/images/icons/2button.png" />" class="call-us-img pull-right hide">

				
				<div class="control-group margin30-t hide">
					<p>
						<strong>
							<img alt="Time estimate" src="<c:url value="/resources/images/icons/2icon1.png" />" class="arrow_ui_design" />
							<spring:message code="ssap.page9.estimatedTime" text="Estimated time for this section:" />&nbsp;
							<spring:message code="ssap.page9.estimatedTimeMinutes" text="10 Minutes &ndash; 15 Minutes" />
						</strong>
					</p>
				</div>
			
		</div><!-- .container-fluid -->
	</div><!-- .row-fluid -->
</div><!-- #appscr59A -->

