package com.getinsured.hix.entity.enrollmententity.service;

import java.util.List;

import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.PopulationEthnicity;

/**
 * Encapsulates service layer method calls for ethnicities served by EE 
 */
public interface PopulationEthnicityService {

	/**
	 * Retrieves {@link PopulationEthnicity} based on ethnicity id
	 * 
	 * @param ethnicityId
	 *            ethnicity id
	 * @return {@link PopulationEthnicity}
	 */
	PopulationEthnicity findById(int ethnicityId);

	/**
	 * Retrieves list of {@link PopulationEthnicity} based on entity id
	 * 
	 * @param entityId
	 *            entity identification number
	 * @return {@link List} of {@link PopulationEthnicity}
	 */
	List<PopulationEthnicity> findByEntityId(int entityId);

	/**
	 * Removes ethnicities associated with particular entity
	 * 
	 * @param entityId
	 *            entity identification number
	 * @return returns number of deleted records
	 */
	int deleteAssociatedEthnicities(int entityId);

	/**
	 * Saves {@link PopulationEthnicity} associated with
	 * {@link EnrollmentEntity}
	 * 
	 * @param enrollmentEntity
	 *            {@link EnrollmentEntity}
	 * @param populationEthnicities
	 *            {@link PopulationEthnicity}
	 */
	void saveAssociatedEthnicities(EnrollmentEntity enrollmentEntity,
			List<PopulationEthnicity> populationEthnicities);
}
