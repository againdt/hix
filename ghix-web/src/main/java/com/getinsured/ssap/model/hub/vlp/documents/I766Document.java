package com.getinsured.ssap.model.hub.vlp.documents;

import java.util.Date;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * I-766 Document.
 *
 * @author Yevgen Golubenko
 * @since 2/15/19
 */
public class I766Document extends DhsDocument
{
  public I766Document() {
    super.setDocumentType(DocumentType.I766);
  }

  @JsonProperty("AlienNumber")
  private String alienNumber;

  @JsonProperty("ReceiptNumber")
  private String receiptNumber;

  @JsonFormat(pattern = DATE_FORMAT)
  @JsonProperty("DocExpirationDate")
  private Date docExpirationDate;

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isValid()
  {
    return notBlank(this.alienNumber) &&
            notBlank(this.receiptNumber) &&
              this.docExpirationDate != null;
  }

  /**
   * {@inheritDoc }
   */
  @Override
  public DhsDocument from(final CitizenshipDocument citizenshipDocument)
  {
    final I766Document document = new I766Document();

    document.setAlienNumber(validValue(citizenshipDocument.getAlienNumber()));

    // TODO: This on UI and JSON is 'Card Number'?
    document.setReceiptNumber(validValue(citizenshipDocument.getCardNumber()));
    document.setDocExpirationDate(citizenshipDocument.getDocumentExpirationDate());

    return document;
  }

  public String getAlienNumber()
  {
    return alienNumber;
  }

  public void setAlienNumber(final String alienNumber)
  {
    this.alienNumber = alienNumber;
  }

  public String getReceiptNumber()
  {
    return receiptNumber;
  }

  public void setReceiptNumber(final String receiptNumber)
  {
    this.receiptNumber = receiptNumber;
  }

  public Date getDocExpirationDate()
  {
    return docExpirationDate;
  }

  public void setDocExpirationDate(final Date docExpirationDate)
  {
    this.docExpirationDate = docExpirationDate;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", I766Document.class.getSimpleName() + "[", "]")
        .add("alienNumber='" + alienNumber + "'")
        .add("receiptNumber='" + receiptNumber + "'")
        .add("docExpirationDate=" + docExpirationDate)
        .toString();
  }
}
