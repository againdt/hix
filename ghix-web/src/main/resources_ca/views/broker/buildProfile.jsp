<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<link href="/hix/resources/css/chosen.css" rel="stylesheet" type="text/css" media="screen,print">

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/modal-template.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-address-util.js" />"></script>


<script src="<c:url value="/resources/js/jquery.autoSuggest.minified.js" />" type="text/javascript"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>


  <script type="text/javascript">
  $(document).ready(function(){
	  var config = {
		      '.chosen-select'           : {enable_split_word_search:false},
		      '.chosen-select-deselect'  : {allow_single_deselect:true},
		      '.chosen-select-no-single' : {disable_search_threshold:10},
		      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		      '.chosen-select-width'     : {width:"95%"}
		    }
		    for (var selector in config) {
		      $(selector).chosen(config[selector]);
		    }
  });

  </script>



<script>
	function ie8Trim(){
		if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
            	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
            }
		}
	}
function removecomma(objName)
{

  var lstLetters = objName;
  str = String(lstLetters);
  ie8Trim();
  str = str.trim();
  var temp = "";
  var temp1 = "";
   temp1 = str.substring(str.length-1, str.length);

  if(temp1 == ",")
	  {
  temp = str.substring(0, str.length-1);
	  }

  else
	  {

	  temp = str;
	  }

  return temp;
}
</script>
<div class="gutter10">
	<div class="row-fluid">
		<c:choose>
				<c:when test="${broker.certificationStatus == 'Pending' || broker.certificationStatus == 'Incomplete' || broker.certificationStatus == null || broker.certificationStatus ==''}">
					<h1><spring:message code="label.newBrokerReg"/></h1>
				</c:when>
				<c:otherwise>
					<h1>${broker.user.firstName}&nbsp;${broker.user.lastName}</h1>
				</c:otherwise>
		 </c:choose>
	</div>

	<div class="row-fluid">
			<!-- #sidebar -->
		       <jsp:include page="brokerLeftNavigationMenu.jsp" />
		    <!-- #sidebar ENDS -->
			<div class="span9" id="rightpanel">
			<div class="header">
				<h4><spring:message code="label.brkStep"/> 2: <spring:message code="label.profile"/></h4>
			</div>
			<div class="row-fluid">
				<div class="span gutter10">
					<div class="span10"><spring:message code="label.brkBuildProfilePage"/>

						<!-- This profile information will be viewable by consumers and employers searching for agents on the exchange.
						The more information you provide, the more likely consumers are to contact you. -->
					</div>
			</div>

				<form class="form-horizontal gutter10" id="frmbrokerProfile" name="frmbrokerProfile" enctype="multipart/form-data"  action="buildprofile" method="POST">
					<div class="row-fluid">
			            <div style="font-size: 14px; color: red">
			              <table>
			              	<c:forEach items="${fieldError}" var="violation">
			                    <tr>
			                        <td><p>
			                         		<c:catch var="exception"> <c:if test="${violation.defaultMessage !=null}"> </c:if>   </c:catch>
											<c:if test="${empty exception}">
												 <c:choose>
													 	<c:when  test="${violation.defaultMessage != null && fn:indexOf(violation.defaultMessage, '{') eq 0}">
										       			  <spring:message  code="${fn:replace(fn:replace(violation.defaultMessage,'{',''),'}','')}"/>
										    		</c:when>
										    		<c:otherwise>
										    		  ${violation.field}	${violation.defaultMessage}
										    		</c:otherwise>
												 </c:choose>
											</c:if>
											<c:catch var="exception"> <c:if test=" ${violation.message !=null}"> </c:if>    </c:catch>
											<c:if test="${empty exception}">
												 <spring:message  code="${fn:replace(fn:replace(violation.message,'{',''),'}','')}"/>
											</c:if>
			                        	<p/>
			                        </td>
			                    </tr>
			                </c:forEach>
			                </table>
			                <br/>
			            </div>
			    </div>
<%-- 				<form class="form-horizontal gutter10" id="frmbrokerProfile" name="frmbrokerProfile" enctype="multipart/form-data"> --%>

				<div class="control-group">
						<label for="clientsServed" class="control-label required"><spring:message  code="label.brkClientServed"/> <a class="brktip" rel="tooltip" data-toggle="tooltip" href="#" data-original-title="<spring:message code="label.brkClientServedTooltip" htmlEscape="true"/>"><i class="icon-question-sign"></i></a></label>
						<div class="controls">
						<c:set var="clientsServedList" value="${broker.clientsServed}"/>
	 							<c:if test="${fn:toLowerCase(stateExchangeType) eq fn:toLowerCase('Both') || fn:toLowerCase(stateExchangeType) eq fn:toLowerCase('Individual')}">
                 					 <label class="checkbox"> <input type="checkbox" value="Individuals &#47; Families" id="clientsServed"  name="clientsServed"  ${fn:contains(clientsServedList, 'Individuals') ? 'checked="checked"' : '' } >Individuals &#47; Families</label> </br>
                 				</c:if>
                  				<c:if test="${fn:toLowerCase(stateExchangeType) eq fn:toLowerCase('Both') || fn:toLowerCase(stateExchangeType) eq fn:toLowerCase('SHOP')}">
                 					 <label class="checkbox"> <input type="checkbox" value="Employers" id="Employers" name="clientsServed"  ${fn:contains(clientsServedList, 'Employers') ? 'checked="checked"' : '' } >Employers</label> </br>
                				</c:if>
								<div id="clientsServed_error"></div>
						</div>	<!-- end of controls-->
					</div><!-- end of control-group -->

					<div class="control-group">
						<label for="languagesSpoken" class="control-label"><spring:message  code="label.brkLanguagesSpoken"/></label>
						<div class="controls">
							<select id="languagesSpoken" name="languagesSpoken"  class="chosen-select" multiple style="width:315px;" tabindex="4" ></select>
						 <div id="languagesSpoken_error"></div>
						</div>	<!-- end of controls-->
					</div><!-- end of control-group -->

					<div class="control-group">
						<label for="hidProductExpertise" class="control-label required"><spring:message  code="label.brkProductExpertise"/> <a class="brktip" rel="tooltip" href="#" data-original-title="<spring:message code="label.brkProductExpertiseTooltip" htmlEscape="true"/>"><i class="icon-question-sign"></i></a>
						<input type="hidden" id="hidProductExpertise"/>
						</label>

						<!-- This field allows you to list the insurance products that you focus on, such as dental, vision, primary health, Casualty,life, etc. -->
						<div class="controls">

							 <c:set var="productExpertiseList" value="${broker.productExpertise}"/>

							<label class="checkbox"><input type="checkbox" name="productExpertise" value="Health"  ${fn:contains(productExpertiseList,'Health') ? 'checked="checked"' : '' }>  Health</label></br>
				            <label class="checkbox"><input type="checkbox" name="productExpertise" value="Dental" ${fn:contains(productExpertiseList,'Dental') ? 'checked="checked"' : ''} />  Dental</label></br>
				            <label class="checkbox"><input type="checkbox" name="productExpertise" value="Vision" ${fn:contains(productExpertiseList,'Vision') ? 'checked="checked"' : '' }/>  Vision</label></br>
				            <label class="checkbox"><input type="checkbox" name="productExpertise" value="Life" ${fn:contains(productExpertiseList,'Life')? 'checked="checked"' : '' }/>  Life</label> 	</br>
							<label class="checkbox"><input type="checkbox" name="productExpertise" value="Medicare" ${fn:contains(productExpertiseList,'Medicare')? 'checked="checked"' : '' }/>  Medicare</label></br>
							<label class="checkbox"><input type="checkbox" name="productExpertise" value="Medicaid" ${fn:contains(productExpertiseList,'Medicaid')? 'checked="checked"' : '' }/>  Medicaid</label></br>
				            <label class="checkbox"><input type="checkbox" name="productExpertise" value="CHIP" ${fn:contains(productExpertiseList,'CHIP') ? 'checked="checked"' : '' }/>  CHIP</label></br>
				            <label class="checkbox"><input type="checkbox" name="productExpertise" value="Workers Compensation" ${fn:contains(productExpertiseList,'Workers Compensation') ? 'checked="checked"' : '' }>  Workers Compensation</label></br>
				            <label class="checkbox"><input type="checkbox" name="productExpertise" value="Property/Casualty" ${fn:contains(productExpertiseList,'Property/Casualty') ? 'checked="checked"' : '' }>  Property/Casualty</label></br>

							<div id="productExpertise_error"></div>
						</div>	<!-- end of controls-->
					</div><!-- end of control-group -->

					<div class="control-group">
						<label for="yourWebSite" class="control-label required"><spring:message  code="label.brkYourWebsite"/><a class="brktip" rel="tooltip" href="#" data-original-title="<spring:message code="label.brkYourWebsiteTooltip" htmlEscape="true"/>"><i class="icon-question-sign"></i></a></label>
						<div class="controls">
							<input type="text" id="yourWebSite" class="input-xlarge" name="yourWebSite" size="50" value="${broker.yourWebSite}" />
							<div id="yourWebSite_error"></div>
						</div>	<!-- end of controls-->
					</div><!-- end of control-group -->

					<div class="control-group">
                      <label class="required control-label" for="yourPublicEmail"><spring:message  code="label.brkpublicEmail"/><a class="brktip" rel="tooltip" href="#" data-original-title="<spring:message code="label.brkYourPublicEmailToolTip" htmlEscape="true"/>"><i class="icon-question-sign"></i></a></label>
                      <div class="controls">
                          <input type="text" name="yourPublicEmail" id="yourPublicEmail" value='<c:choose><c:when test="${broker.certificationStatus =='Incomplete'}">${broker.user.email}</c:when><c:otherwise>${broker.yourPublicEmail}</c:otherwise></c:choose>' class="input-xlarge" size="30" maxlength="100"  />
						  <div id="yourPublicEmail_error" class="help-inline"></div>
                      </div>
                    </div>

					<div class="control-group">
						<label for="education" class="control-label required"><spring:message code="label.brkEducation"/></label>
						<div class="controls">
							<select id="education" class="input-xlarge" name="education">
								<option value=""><spring:message code="label.brkSelect"/></option>
								<c:forEach items="${educationList}" var="currentEducation">
									<option value="${currentEducation.key}" <c:if test="${currentEducation.key eq broker.education}"> SELECTED </c:if>>${currentEducation.value}</option>
								</c:forEach>
							</select>
							<div id="education_error"></div>
						</div>
					</div>


					<div class="control-group">
						<label for="aboutMe" class="control-label required"><spring:message code="label.aboutYourself"/></label>
						<div class="controls">
							<textarea name="aboutMe" id="aboutMe" class="input-xlarge" rows="3" maxlength="4000">${broker.aboutMe}</textarea>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->

					<div class="control-group">
					<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
					<label for="fileInputPhoto" class="control-label required"><spring:message code="label.uploadPhoto"/></label>
					<spring:url value="/broker/photo/${encBrokerId}"  var="photoUrl"/>
					<div class="controls paddingT5">
<%-- 					<img src="${photoUrl}" alt="Profile photo thumbnail" class="profilephoto thumbnail" /> --%>
						 <input type="file" name="fileInputPhoto" id="fileInputPhoto" class="input-file" />
						  <input type="hidden"  id="fileInputPhoto_Size" name="fileInputPhoto_Size" value="1">
						  <input type="button" id="btn_UploadPhoto" name="btn_UploadPhoto" value="<spring:message code="label.brokerupload"/>"  onclick="uploadPhoto();" class="btn"  /><%--  <spring:message code="label.brokerupload"/> --%>
<!-- 						<input type = "button" value = "Choose file" onclick ="javascript:document.getElementById('file').click();"> -->
						<div>
                  			<spring:message code="label.uploadCaption"/>
                  		</div>
                  		<div id="fileInputPhoto_error" class="help-inline">
					 	<c:if test="${!empty exceedFile}">
					 		<label class="error" for="fileInputPhoto" generated="true" >
					  			<span><em class='excl'>!</em><spring:message  code='err.validateFileSize' javaScriptEscape='true'/> </span>
					   		</label>
					    </c:if>
					 </div>
					</div><!-- end of controls-->
					</div>



				<!-- Comment Engine Start -->

<%-- 				<jsp:include page="../platform/comment/addcomments.jsp"> --%>
<%-- 					<jsp:param value="" name=""/> --%>
<%-- 				</jsp:include> --%>
<!-- 				<input type="hidden" value="BROKER" id="target_name" name="target_name" /> -->
<%-- 				<input type="hidden" value="${broker.id}" id="target_id" name="target_id" /> --%>

				<!-- Comment Engine end -->
				<div class="controls">
						<input type="button" name="back" id="back" value="<spring:message  code='label.brkBack'/>" title="<spring:message  code='label.brkBack'/>" class="btn" onClick="window.location.href='/hix/broker/certificationapplication'"/>
						<c:choose>
							<c:when test="${isPaymentEnabled}">
								<input type="submit" name="submit_finish_next" id="submit_finish_next" value="<spring:message  code='label.brkNext'/>" class="btn btn-primary" title="<spring:message  code='label.brkNext'/>"/>
							</c:when>
							<c:otherwise>
								<input type="submit" name="submit_finish_next" id="submit_finish_next" value="<spring:message  code='label.brkFinish'/>" class="btn btn-primary" title="<spring:message  code='label.brkFinish'/>"/>
							</c:otherwise>
						</c:choose>
				</div><!-- end of form-actions -->
				<input type="hidden" name="languagesSpokenCheck" id="languagesSpokenCheck" value="1" />
				<df:csrfToken/>
			</form>
		</div><!-- end of span9 -->
		</div><!-- end of row-fluid -->
		</div>
		</div>
		<div class="notes" style="display: none">
			<div class="row">
				<div class="span">
					<p><spring:message code="label.brkBrokerEligibilityInfo"/>	</p>
				</div>
			</div>
		</div><!--  end of .notes -->

<div id="dialog-modal" title="AHBX Delegation Info" style="display: none;">
  <p>Broker Name: ${sessionBrokerName}<br/>
  Delegation Code is: ${sessionDelegationCode}
  </p>
</div>
<div id="dialog-modal_error" title="AHBX Delegation Info" style="display: none;">
<p>Message: ${sessionErrorInfo}</p>
</div>
<script type="text/javascript">

	$(document).ready(function() {
		$("#brkProfile").removeClass("link");
		$("#brkProfile").addClass("active");

		/* alert("Delegation code is: "+'${sessionErrorInfo}'); */
		if('${sessionDelegationCode}' != null && '${sessionDelegationCode}' != '' && '${sessionBrokerName}' != null && '${sessionBrokerName}' != '')
		{
			 $( "#dialog-modal" ).dialog({
			      height: 140,
			      modal: true
			    });
		}
		else if('${sessionErrorInfo}' != null && '${sessionErrorInfo}' != '')
		{
			$( "#dialog-modal_error" ).dialog({
			      height: 140,
			      modal: true
			    });
		}

		var brkId = ${broker.id};
		if(brkId != 0) {
			$('#certificationInfo').attr('class', 'done');
		} else {
			$('#certificationInfo').attr('class', 'visited');
		};

		var paymentid = ${paymentId};
		if (paymentid != 0) {
			$('#paymentinfo').attr('class', 'done');
		} else {
			$('#paymentinfo').attr('class', 'visited');
		};
	});

	function shiftbox(element,nextelement){
		maxlength = parseInt(element.getAttribute('maxlength'));
		if(element.value.length == maxlength){
			nextelement = document.getElementById(nextelement);
			nextelement.focus();
		}
	}

</script>
<script type="text/javascript">
function split(val) {
    return val.split(/,\s*/);
}
function extractLast(term) {
    return split(term).pop();
}

function uploadPhoto(){
	$('#btn_UploadPhoto').attr('disabled','disabled');
	 var file = $('input[type="file"]').val();
	 if(file!=""){
		 var file1 = $('input[type="file"]').val();
		 var exts = ['jpg','jpeg','gif','png','bmp'];
		 var get_ext = file1.split('.');
		 get_ext = get_ext.reverse();
		 if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
			 if(document.getElementById('fileInputPhoto_Size').value==1){
				 $('#frmbrokerProfile').ajaxSubmit({
					 url: "<c:url value='/broker/uploadphoto/${encBrokerId}'/>",
					 success: function(responseText) {
						 $("#btn_UploadPhoto").removeAttr('disabled');
						 if(responseText.indexOf('Missing CSRF token') != -1){
								$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.broker.brokerUnexpectedErrorOccured"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"  ><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
								return false;
						  }
						 if(-1 < responseText.search("SUCCESS")){  //for IE8
							 $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"></button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.fileuploadedsuccessfully"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});


						 }else{
							 $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});

						 }
					 },
					 error : function(responseText) {
						 $("#btn_UploadPhoto").removeAttr('disabled');
						 $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4>  <spring:message code="label.failedtouoload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
					 }

				 });
			 }else{
				 $("#btn_UploadPhoto").removeAttr('disabled');
				 $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message  code='label.brkvalidatePleaseSelectFileWithSizeLessThan5MB' javaScriptEscape='true'/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
			 }
		 }else{
			 $("#btn_UploadPhoto").removeAttr('disabled');
			 $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.selectimgfile"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
		 }
	 }else{
		 $("#btn_UploadPhoto").removeAttr('disabled');
		 $('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"></button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4> <spring:message code="label.selectfilebeforeupload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});

	 }
	 return false;
}


$(document).ready(function() {

	 $( "#languagesSpoken").autocomplete({
        source: function (request, response) {
            $.getJSON("${pageContext. request. contextPath}/getLanguageSpokenList", {
                term: extractLast(request.term)
            }, response);
        },
        search: function () {
            // custom minLength
            var term = extractLast(this.value);
            if (term.length < 1) {
                return false;
            }
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });

});

var validator = $("#frmbrokerProfile").validate({
	onkeyup: false,
	rules : {
		fileInputPhoto  : {PhotoUploadCheck: true, sizeCheck:true}	,
		languagesSpoken : {required : false, number:false, languagesSpokenCheck:true},
		yourWebSite : {checkurl : true},
		yourPublicEmail : { required : false, validateEmail: true}
	},
	messages : {
		fileInputPhoto  : {PhotoUploadCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePhoto' javaScriptEscape='true'/></span>",
			sizeCheck : "<span> <em class='excl'>!</em><spring:message  code='label.brkvalidatePleaseSelectFileWithSizeLessThan5MB' javaScriptEscape='true'/> </span>"},
			languagesSpoken : { languagesSpokenCheck: "<span> <em class='excl'>!</em><spring:message code='label.brokerValidatePleaseEnterLanguageSpokenFromDropDown' javaScriptEscape='true'/></span>"},
		yourWebSite : {checkurl : "<span><em class='excl'>!</em><spring:message  code='err.validateYourWebSite' javaScriptEscape='true'/></span>"},
		yourPublicEmail : {validateEmail:"<span> <em class='excl'>!</em><spring:message code='label.validatePublicEmail' javaScriptEscape='true'/></span>"},
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	}
});

jQuery.validator.addMethod("validateEmail", function(value, element) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if($.trim(value) == ""){
		return true;
	}
	if (filter.test(value)) {
	    return true;
	}
	else {
	    return false;
	}
});

$(function(){
	$('#fileInputPhoto').change(function(){
		var rv = -1; // Return value assumes failure.
		if (navigator.appName == 'Microsoft Internet Explorer'){
			 var ua = navigator.userAgent;
			 var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			 if (re.exec(ua) != null){
				 rv = parseFloat( RegExp.$1 );
			 }
		}
		if(!(rv <= 9.0 && navigator.appName == 'Microsoft Internet Explorer')){
			 var f=this.files[0];
			 if((f.size > 5242880)||(f.fileSize > 5242880)){
				 $("#fileInputPhoto_Size").val(0);
			 }else{
				 $("#fileInputPhoto_Size").val(1);
			 }
		}else{
			 $("#fileInputPhoto_Size").val(1);
		}
	});
});

jQuery.validator.addMethod("checkurl", function(value, element) {
	//Should validate only if a value has been entered
	if(value == '') {
		return true;
	}
	if((value.indexOf('..') != -1 )|| (value[value.length-1]=='.')){
		 return false;
	}
	if(!(value.indexOf('http://') == 0 || value.indexOf('https://') == 0 || value.indexOf('www.') == 0)){
		 return false;
	}
	// now check if valid url
	var regx = "(((https?:[/][/](www.)?)|(www.))([-]|[a-z]|[A-Z]|[0-9]|[/.]|[~])+[.]([a-z]|[A-Z]|[0-9]|[~])+([a-z]|[A-Z]|[0-9]|[/.]|[~])*)";
	if(value.indexOf('http://www') == 0 || value.indexOf('https://www')==0){
		 regx = "(((https?:[/][/])((www)))[.]([-]|[a-z]|[A-Z]|[0-9]|[~])+[.]([a-z]|[A-Z]|[0-9]|[~])+([a-z]|[A-Z]|[0-9]|[/.]|[~])*)";
	}
	var regexp = new RegExp(regx);
	return regexp.test(value);

});

jQuery.validator.addMethod("sizeCheck", function(value, element, param) {
	  ie8Trim();
	  if($("#fileInputPhoto_Size").val()==1){
		  return true;
	  }else{
		  return false;
	  }
});
jQuery.validator.addMethod("PhotoUploadCheck", function(value, element, param) {
	  ie8Trim();
      var file = $('input[type="file"]').val();
      var exts = ['jpg','jpeg','gif','png','bmp'];
      if ( file ) {
        var get_ext = file.split('.');
        get_ext = get_ext.reverse();

        if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
          return true;
        } else {
          return false;
        }
     }
     return true;
});

jQuery.validator.addMethod("languagesSpokenCheck", function(value, element, param) {
	if(value == '') {
		return true;
	}
	checkLanguageSpoken();
	return $("#languagesSpokenCheck").val() == 1 ? false : true;
	});

 function checkLanguageSpoken(){
	$("#languagesSpoken_error").hide();
	$.get('buildProfile/checkLanguagesSpoken',
	{languagesSpoken: $("#languagesSpoken").val()},
	            function(response){
	                    if(response == true){
	                            $("#languagesSpokenCheck").val(0);
								$("#languagesSpoken_error").hide();
	                    }else{
	                            $("#languagesSpokenCheck").val(1);
								$("#languagesSpoken_error").show();
	                    }
	     	    }
	        );
	}


$(function() {
$("#submit_finish_next").click(function(e){

	if($("#frmbrokerProfile").validate().form()) {
		$("#frmbrokerProfile").submit();
	}
});
});

$('.brktip').tooltip();
</script>

<script type="text/javascript">
	function loadUsers() {
		var matched = $("#languagesSpoken option:selected").text();
		var pathURL = "/hix/broker/buildprofile/getlanguage";
		$('#errorMsg').val("");
		$.ajax({
			url : pathURL,
			type : "GET",
			data : {
				matched : matched
			},
			success: function(response){
				loadData(response);
    		},
   			error: function(e){
    			alert("Failed to load Languages");
    		},
		});

   	}

	//load the jquery chosen plugin with the AJAX data
	function populatelanguages() {
		$("#languagesSpoken").html('');
		var respData = $.parseJSON('${languagesList}');
		var languages='${broker.languagesSpoken}';
		for ( var key in respData) {
	    	var isSelected = false;
		     if(languages!=null){
			      isSelected = checkLanguages(respData[key]);
		      }
		      if(isSelected){
		    	  $('#languagesSpoken').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#languagesSpoken').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }

		$('#languagesSpoken').trigger("liszt:updated");
	}

	function checkLanguages(county){
	     var languages='${broker.languagesSpoken}';
	     var countiesArray=languages.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i].replace(/^\s+|\s+$/g, '');
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;

			     }
			 }
		   return found;
   }

	$(document).ready(function() {
		populatelanguages();
	});

</script>
