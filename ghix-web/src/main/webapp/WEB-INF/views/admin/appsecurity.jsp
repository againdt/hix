<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<link rel="stylesheet" type="text/css" href='<gi:cdnurl value="/resources/css/ngtable/ng-table.css" />'/>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/ngtable/ng-table.js" />"></script>
<Style>
.sortorder:after {
  content: '\25b2';
}
.sortorder.reverse:after {
  content: '\25bc';
}
</Style>
<div class="gutter10" ng-app="endpoints-app" ng-controller="endpoints-controller" ng-init="loadendpoints()">
	<h1 class="margin20-b">Application Endpoints and Permissions <a href="javascript:void(0)" ng-if="data.length !==0 " ng-click="exportData()" class="btn btn-primary pull-right">Export to Excel</a></h1>
	<div class="row-fluid" id="exportable" ng-if="data.length !==0 ">
		<div class="input-group">
			<input style="display:table-cell; width:98%" ng-model="searchKeyword" placeholder="Search" type="search" ng-change="search()" /> 
		</div>
		<table ng-table="tableParams" class="table table-condensed table-bordered table-striped">
			<thead>
				<tr class="urlrow">
					<th ng-click="order('url')">
						<span>URL</span>
         				<span class="sortorder" ng-show="predicate === 'url'" ng-class="{reverse:reverse}"></span>
					</th>

					<th ng-click="order('permission')">
						<span>Permission</span>
					    <span class="sortorder" ng-show="predicate === 'permission'" ng-class="{reverse:reverse}"></span>
					</th>
					<th>
						<span>Method</span>
					</th>
					<th ng-click="order('description')">
						<span>Description</span>
					    <span class="sortorder" ng-show="predicate === 'description'" ng-class="{reverse:reverse}"></span>
					</th>
				</tr>
			<thead>
			<tbody>
				<tr ng-repeat="endpoint in $data | filter: searchKeyword | orderBy:predicate:reverse " class="urlrow">
					<td>{{endpoint.url}}</td>
					<td>{{endpoint.permission}}</td>
					<td>{{endpoint.httpMethod}}</td>
					<td>{{endpoint.description}}</td>
				</tr>
			</tbody>
		</table>
		</div>
</div>

<script>
	var app = angular.module('endpoints-app', [ 'ngTable' ]);
	app.controller('endpoints-controller', ['$scope', '$http', 'ngTableParams',	'$filter', function($scope, $http, ngTableParams, $filter) {
				var tableData = []
				$scope.tableParams = new ngTableParams({
					page : 1,
					count : 100000,
					 counts: [],
					sorting: { url: "asc" } 
				}, {
					getData : function($defer, params) {
						$http.get('../endpoints?tmstp=' + new Date().getTime())
								.then(
										function(response) {
											   $scope.data = response.data;
											   $scope.data = params.sorting() ? $filter('orderBy')($scope.data, params.orderBy()) :  $scope.data;
											   $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
											   $defer.resolve($scope.data);
										});
					}
				});
				
				
				$scope.exportData = function () {
					
				        var data = $scope.data;
				        if(data == '')
				            return;
				        data = angular.toJson(data)
				        $scope.JSONToCSVConvertor(data);
			    };
			    
			    $scope.predicate = 'url';
			    $scope.reverse = false;
			    $scope.order = function(predicate) {
			      $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
			      $scope.predicate = predicate;
			    };			    
			    
			    
			    $scope.JSONToCSVConvertor = function(JSONData) {
			        var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
			        var CSV = '';    
		            var row = "";
		            for (var index in arrData[0]) {
		                row += index + ',';
		            }
		            row = row.slice(0, -1);
		            CSV += row + '\r\n';
			        for (var i = 0; i < arrData.length; i++) {
			            var row = "";
			            for (var index in arrData[i]) {
			            	if(arrData[i][index]!==null){
			                	row += '"' + arrData[i][index] + '",';
			            	}else{
			                	row += '"",';
			            	}
			            }
			            row.slice(0, row.length - 1);
			            CSV += row + '\r\n';
			        }

			        if (CSV == '') {        
			            alert("Invalid data");
			            return;
			        }   
			        
			        var fileName = "URL_Mappings";
			        
			        var uri = 'data:text/xls;charset=utf-8,' + escape(CSV);
			        var link = document.createElement("a");    
			        link.href = uri;
			        link.style = "visibility:hidden";
			        link.download = fileName + ".csv";
			        document.body.appendChild(link);
			        link.click();
			        document.body.removeChild(link);
			    };			    
			} ]);
</script>