<div class="span6 info-block">  
<div class="gutter10">
   <div class="header"> <h4>Plan Summary</h4> </div>
   <div id="$planSummaryClass$" class="gutter10 tile " >
      <p id="enrolled-summary-msg">$message$</p>
      
      <div id="plan-selected" class="alert alert-info">
        <div class="info-header center">
            <img class="carrierlogo hide" src='$logoStr$' alt="$issuerName$"  onload="imgSize()" />
            <p><a href="#">$planName$</a></p>
            <h5 class="payment" align="center"> $monthlyPayrollDeduction$/mo </h5>
        </div>
        
        $if(showDummy)$
        <div id="plan-mask-sample" class="mask"><img src="/hix/resources/img/example.png" alt="This is a sample plan summary" /></div>
        $endif$
         $if(showWaived)$
        <div id="plan-mask-waived" class="mask"><img src="/hix/resources/img/waived.png" alt="This is a sample plan summary" /></div>
        $endif$
       
        <div class="data-display well">
        	<table>
        		<tr>
        			<td class="vertical-align-top">Plan Type:</td>
                    <td class="vertical-align-top" id="plan-type"><strong>$planType$</strong></td>
               </tr>
               <tr>
                    <td class="vertical-align-top">Office Visit Copay:</td>
                    <td class="vertical-align-top" id="off-visit"><strong>$officeVisit$</strong></td>
               </tr>
               <tr>
                    <td class="vertical-align-top">Generic Drug Copay:</td>
                    <td class="vertical-align-top" id="generic-medic"><strong>$genericMedications$</strong></td>
               </tr>
               <tr>    
                    <td class="vertical-align-top">Deductible Ind:</td>
                    <td class="vertical-align-top" id="deductible"><strong>$deductible$</strong></td>
              </tr>
              <tr>     
                    <td class="vertical-align-top">Max Out-of-Pocket Ind:</td>
                    <td class="vertical-align-top" id="m-o-p"><strong>$oopMax$</strong></td>
        		</tr>
        	</table>
         </div>
         
      </div>
       $if(showViewDetails)$
      <div class="gutter10-tb">
        <button id="details" class="btn" alt="View Details" onclick="gotoNext('5');">View Details</button>
      </div>
       $endif$
    </div>
</div> <!-- .gutter10--></div> 