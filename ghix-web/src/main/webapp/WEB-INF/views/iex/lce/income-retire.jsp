<script type="text/ng-template" id="income-reture.jsp">
  <div class="gutter10 jobIncome" ng-controller="incomeRetire" >
    <h3><spring:message code="label.lce.retirepension"/></h3>
    <form class="form-horizontal"  id="retirementOrPensionIncome" name="retirementOrPensionIncome" novalidate>
      <spring:message code="label.lce.howmuchdoes"/> {{householdMember.name.firstName}} {{householdMember.name.lastName}} <spring:message code="label.lce.getfromretirement"/>
      <div class="control-group">
        <label class="control-label" for="typeOfRequest"></label>
        <div class="controls margin10-t">
          <div class="input-prepend">
            <span class="add-on">$</span>
            <input type="text" class="span4 margin10-r" value="700" name="retirementOrPensionIncomeAmount"  ng-model="householdMember.detailedIncome.retirementOrPension.taxableAmount"  required numbers-Only>
            <select class="span5" name="retirementOrPensionIncomeAmountFrequency" ng-model="householdMember.detailedIncome.retirementOrPension.incomeFrequency" required>
              <option><spring:message code="label.lce.weekly"/>Weekly</option>
			        <option><spring:message code="label.lce.biweekly"/>Bi-Weekly</option>
			        <option><spring:message code="label.lce.monthly"/>Monthly</option>
			        <option><spring:message code="label.lce.bimonthly"/>Bi-Monthly</option>
			        <option><spring:message code="label.lce.yearly"/>Yearly</option>
			        <option><spring:message code="label.lce.onceonly"/>Once Only</option>
            </select>
          </div>
         </div>
        <div class="controls margin5-t">
          <span class="validation-error-text-container" ng-show="submitted && retirementOrPensionIncome.retirementOrPensionIncomeAmount.$error.required"><spring:message code="label.lce.incomeretire.error1"/></span>
        </div>
        <div class="controls margin5-t">
          <span class="validation-error-text-container" ng-show="submitted && retirementOrPensionIncome.retirementOrPensionIncomeAmountFrequency.$error.required"><spring:message code="label.lce.incomeretire.error2"/></span>
        </div>
      </div>
    </form>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a ng-show="retirementOrPensionIncome.$valid" class="btn btn-primary" ng-click="next();"><spring:message code="label.lce.next"/></a>
      <a ng-show="!retirementOrPensionIncome.$valid" class="btn btn-primary" ng-click="submitted=true;"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>