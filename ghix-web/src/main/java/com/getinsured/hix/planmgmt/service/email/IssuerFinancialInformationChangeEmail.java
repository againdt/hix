package com.getinsured.hix.planmgmt.service.email;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.logging.GhixLogFactory;
import com.getinsured.hix.platform.logging.GhixLogger;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.util.PlanMgmtConstants;

@Component
public class IssuerFinancialInformationChangeEmail{
	
	private static final GhixLogger LOGGER = GhixLogFactory.getLogger(IssuerFinancialInformationChangeEmail.class);

	private Issuer issuerObj;
	private IssuerRepresentative issuerRepObj;
	private AccountUser user;


	public void setIssuerObj(Issuer issuer) {
		this.issuerObj = issuer;
	}

	public void setIssuerRepObj(IssuerRepresentative issuerRepObj) {
		this.issuerRepObj = issuerRepObj;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}
	
	
	
	public Map<String, Object> getSingleData() {
		Map<String, Object> data = new HashMap<String, Object>();
		DateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
		data.put(PlanMgmtConstants.SYSTEM_DATE, dateFormat.format(new TSDate()));
		data.put(PlanMgmtConstants.ISSUER_NAME, issuerObj.getName());
		String firstName = issuerRepObj.getFirstName() == null ? "" : issuerRepObj.getFirstName().trim();
		String lastName = issuerRepObj.getLastName() == null ? "" : issuerRepObj.getLastName().trim();
		
		data.put(PlanMgmtConstants.ISSUER_REPRESENTATIVE, firstName + " " + lastName);
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		data.put(PlanMgmtConstants.INSURANCE_EXCHANGE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		data.put(PlanMgmtConstants.EXCHANGE_ADDRESS,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		data.put(PlanMgmtConstants.EXCHANGE_WEBSITE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		data.put(PlanMgmtConstants.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		
		data.put(PlanMgmtConstants.ISSUER_MARKETING_NAME, issuerObj.getName() == null ? "": issuerObj.getName().trim());
		data.put(PlanMgmtConstants.CITY, issuerObj.getCity());
		data.put(PlanMgmtConstants.STATE, issuerObj.getState());
		data.put(PlanMgmtConstants.ZIPCODE, issuerObj.getZip());
		String address1 = issuerObj.getAddressLine1() == null ? "" : issuerObj.getAddressLine1().trim();
		String address2 = issuerObj.getAddressLine2() == null ? "" : issuerObj.getAddressLine2().trim();
		
		String address ="";
		if(StringUtils.isNotBlank(address2)){
			address = address1 + PlanMgmtConstants.BREAK_LINE + address2;
		}else{
			address = address1;
		}
		data.put(PlanMgmtConstants.ISSUER_ADDRESS, address);
		Map<String, Object> emailHeaders = new HashMap<String, Object>();
		emailHeaders.put("IssuerId", String.valueOf(issuerObj.getId()));
		emailHeaders.put("UserId", String.valueOf(user.getId()));
		emailHeaders.put("To", user.getEmail());
		emailHeaders.put("id", Integer.toString(issuerRepObj.getId()));
		emailHeaders.putAll(data);
		LOGGER.debug("Notification Template Data :-" + SecurityUtil.sanitizeForLogging(String.valueOf(data.toString())));
		return emailHeaders;
	}

}
