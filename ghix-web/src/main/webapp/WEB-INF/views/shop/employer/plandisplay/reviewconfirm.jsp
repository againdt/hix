<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld" prefix="util"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css"  href="<c:url value="/resources/css/shopstyles.css"/>" />
<style>
.dialogCSS{background-color: white}

</style>
<div class="gutter10">
<div class="row-fluid">
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        <div class="accordion graysidebar" id="accordion2">
         	<util:nav pageName="${page_name}"></util:nav>
        </div>
         <util:selection pageName="${page_name}"></util:selection>
      </div>
      <!--sidebar-->
      
      <div class="span9" id="rightpanel">
      <h4 class="graydrkbg gutter10" id="skip">
      <c:choose>
      	<c:when test="${displayDental == 'Yes'}">
      		 <a href="<c:url value="/shop/employer/plandisplay/dentalcontribution" />" class="btn"> <spring:message code = "label.back"/></a>
      	</c:when>
      	<c:otherwise>
      		 <a href="<c:url value="/shop/employer/plandisplay/finalcontribution" />" class="btn"> <spring:message code = "label.back"/></a>
      	</c:otherwise>
      </c:choose>
			
      <span class="offset1"><spring:message code = "label.confirmSelections"/></span>
            <a href="#" class="btn btn-primary pull-right confirm_btn"><spring:message code = "label.confirm"/></a>
       </h4>    
    <!--titlebar-->
      <form id="frmreviewconfirm" name="frmreviewconfirm" action="reviewconfirm" method="post">
        <df:csrfToken/>
        <div class="gutter10">
          <p><spring:message code = "label.reviewSelectionText"/> </p>
          <h4 class="lightgray">Summary of Estimated Employer Costs</h4>
          <table class="table table-border-none">
          		<tr>
          			<td class="span5">&nbsp;</td>
          			<td>&nbsp;</td>
          			<td>Healthcare</td>
          			<c:if test="${displayDental == 'Yes'}">
          			<td>Dental</td>
          			</c:if>
          		</tr>
          		<tr>
          			<td class="span5">Contribution for employees</td>
          			<td>&nbsp;</td>
          			<fmt:formatNumber value="${empPlanSelectionDTO.employerBenchmarkIndvPremium}" var="employerBenchmarkIndvPremium" currencySymbol="$" maxFractionDigits="0" minFractionDigits="0" type="currency"/>
          			<td><strong> ${employerBenchmarkIndvPremium} </strong></td>
          			<c:if test="${displayDental == 'Yes'}">
          			   <td><strong>$${empPlanSelectionDTO.employerDentalTotalIndvContribution}</strong></td>
          			</c:if>
          		</tr>
          		<tr>
          			<td class="span5">Contribution for dependents</td>
          			<td>&nbsp;</td>
          			<fmt:formatNumber value="${empPlanSelectionDTO.employerBenchmarkDeptPremium}" var="employerBenchmarkDeptPremium" currencySymbol="$" maxFractionDigits="0" minFractionDigits="0" type="currency"/>
          			<td><strong> ${employerBenchmarkDeptPremium} </strong></td>
          			<c:if test="${displayDental == 'Yes'}">
          				<td><strong>$${empPlanSelectionDTO.employerDentalTotalDepeContribution}</strong></td>
          			</c:if>
          		</tr>
          		<tr>
          			<c:set var="totalhealthcontribution" value="${empPlanSelectionDTO.employerBenchmarkIndvPremium + empPlanSelectionDTO.employerBenchmarkDeptPremium }"></c:set> 
          			<fmt:formatNumber value="${totalhealthcontribution}" var="totalhealthcontribution" currencySymbol="$" maxFractionDigits="0" minFractionDigits="0" type="currency"/>
          			<c:if test="${displayDental == 'Yes'}">
          				<c:set var="totaldentalcontribution" value="${empPlanSelectionDTO.employerDentalTotalIndvContribution + empPlanSelectionDTO.employerDentalTotalDepeContribution }"></c:set> 
          				<fmt:formatNumber value="${totaldentalcontribution}" var="totaldentalcontribution" currencySymbol="$" maxFractionDigits="0" minFractionDigits="0" type="currency"/>
          			</c:if>
          			<td class="span5">Total Employer Contribution</td>
          			<td>&nbsp;</td>
          			<td><strong>${totalhealthcontribution}</strong></td>
          			<c:if test="${displayDental == 'Yes'}">
          				<td><strong>${totaldentalcontribution} </strong></td>
          			</c:if>
          		</tr>
          		
          </table> 
          <h4 class="lightgray"><spring:message code = "label.yourSelections"/></h4>
          <table class="table table-border-none">
	          <tr>
	            <td class="span4"><spring:message code = "label.contri"/> %</td>
	            <fmt:formatNumber value="${empPlanSelectionDTO.employerIndvContribution}" var="employerIndvContribution" pattern="0" type="number"/>
	            <td><strong><spring:message code = "label.employees"/></strong></td>
	            <td><strong>${employerIndvContribution}%</strong></td>
	            <td><i class="icon-pencil"></i> <a href="<c:url value="/shop/employer/plandisplay/finalcontribution" />"><spring:message code = "label.edit"/> <span class="aria-hidden"><spring:message code = "label.contri"/> % for <spring:message code = "label.employees"/></span></a></td>
	          </tr>
	          <tr>
	          	<fmt:formatNumber value="${empPlanSelectionDTO.employerDepeContribution}" var="employerDepeContribution" pattern="0" type="number"/>
	            <td class="span4">&nbsp;</td>
	            <td><strong><spring:message code = "label.dependent"/></strong></td>
	            <td><strong>${employerDepeContribution}%</strong></td>
	            <td><i class="icon-pencil"></i> <a href="<c:url value="/shop/employer/plandisplay/finalcontribution" />"><spring:message code = "label.edit"/> <span class="aria-hidden"><spring:message code = "label.contri"/> % for <spring:message code = "label.dependent"/></span></a></td>
	          </tr>
	          <!-- <tr>
		          <td>&nbsp;</td>
		          <td><strong>Dependents</strong></td>
		          <td><p><strong>10%</strong></p></td>
		          <td><i class="icon-pencil"></i> <a href="<c:url value="/shop/employer/plandisplay/finalcontribution" />">Edit</a></td>
	          </tr> -->
	          <tr>
		          <td><spring:message code = "label.planLevel"/></td>
		          <td><strong>${empPlanSelectionDTO.tierType}</strong></td>
		          <td>&nbsp;</td>
		          <td><i class="icon-pencil"></i> <a href="<c:url value="/shop/employer/plandisplay/plantier" />"><spring:message code = "label.edit"/> <span class="aria-hidden"><spring:message code = "label.planLevel"/></span></a></td>
	          </tr>
	          <tr>
		          <td><spring:message code = "label.refPlan"/></td>
		          <td><strong>${empPlanSelectionDTO.benchmarkPlanName}</strong></td>
		          <td>&nbsp;</td>
		          <td><i class="icon-pencil"></i> <a href="<c:url value="/shop/employer/plandisplay/benchmarkplan" />"><spring:message code = "label.edit"/> <span class="aria-hidden"><spring:message code = "label.refPlan"/></span></a></td>
	          </tr>
	          <tr>
	          	  <fmt:formatDate pattern="MM/dd/yyyy" value="${empPlanSelectionDTO.coverageDate}" var="effectiveDate" />
		      	  <td><spring:message code = "label.effectiveDt"/></td>
		          <td><strong>${effectiveDate}</strong></td>
		          <td>&nbsp;</td>
		          <td>&nbsp;</td>
	          </tr>
	          <c:if test="${stateCode != 'CA'}">
	           <tr>
	          	  <fmt:formatDate pattern="MM/dd/yyyy" value="${empPlanSelectionDTO.employerEnrollment.openEnrollmentStart}" var="enrollmentStartDate" />
		      	  <td><spring:message code = "label.openEnrolStartDt"/></td>
		          <td><strong>${enrollmentStartDate}</strong></td>
		          <td>&nbsp;</td>
		          <td>&nbsp;</td>
	          </tr>
	          <tr>
	          	  <fmt:formatDate pattern="MM/dd/yyyy" value="${empPlanSelectionDTO.employerEnrollment.openEnrollmentEnd}" var="enrollmentEndDate" />
		      	  <td><spring:message code = "label.openEnrolEndDt"/></td>
		          <td><strong>${enrollmentEndDate}</strong></td>
		          <td>&nbsp;</td>
		          <td>&nbsp;</td>
	          </tr>
	          </c:if>
          </table>
          <c:if test="${displayDental == 'Yes'}">
	          <h4 class="lightgray">Your Dental Selection</h4>
	          <table class="table table-border-none">
	          		<tr>
	          			<td class="span5">Contribution for employees</td>
	          			<td>&nbsp;</td>
	          			<td><strong>$${empPlanSelectionDTO.employerDentalIndvContribution}</strong></td>
	          			<td><i class="icon-pencil"></i> <a href="<c:url value="/shop/employer/plandisplay/dentalcontribution" />"><spring:message code = "label.edit"/> <span class="aria-hidden"><spring:message code = "label.contri"/> % for <spring:message code = "label.dependent"/></span></a></td>
	          		</tr>
	          		<tr>
	          			<td class="span5">Contribution for dependents</td>
	          			<td>&nbsp;</td>
	          			<td><strong>$${empPlanSelectionDTO.employerDentalDepeContribution}</strong></td>
	          			<td><i class="icon-pencil"></i> <a href="<c:url value="/shop/employer/plandisplay/dentalcontribution" />"><spring:message code = "label.edit"/> <span class="aria-hidden"><spring:message code = "label.contri"/> % for <spring:message code = "label.dependent"/></span></a></td>
	          		</tr>
	          </table> 
          </c:if>
          <h4 class="lightgray"><spring:message code = "label.potTaxImpact"/></h4>
          <table class="table table-border-none">
  	        <tr>
				<fmt:formatNumber value="${empPlanSelectionDTO.taxCredit}" var="taxCredit" currencySymbol="$" maxFractionDigits="0" minFractionDigits="0"  type="currency"/>
				<td class="span5" scope="row"><spring:message code = "label.estTaxCredit"/></td>
           		<td><strong>${taxCredit} / <spring:message code = "label.month"/></strong></td>
            </tr>
          </table>
          <div class="gutter10"><spring:message code = "label.estTaxPenaltyAmt"/> </div>
        </div>
        <!--gutter10--> 
        </form>
      </div>
      <!--rightpanel--> 
    </div>
    <!--row-fluid--> 
</div>
<!--main-->

<div id="dialog-confirm" title="<spring:message code = "label.startOpenEnrol"/>">
  <p><span style="float: left; margin: 0 7px 20px 0;" class="ui-icon ui-icon-alert"></span><spring:message code = "label.confirmCheck"/> </p><p style="margin: -5px 0 0 20px;"><spring:message code = "label.confirmAction"/></p>
</div>
</div>
<script>
$(document).ready(function () {
    $('.info').tooltip();
    
});

$( ".confirm_btn" ).click(function() {
  $( "#dialog-confirm" ).dialog( "open" );
});

$( "#dialog-confirm" ).dialog({
	 autoOpen: false,
    resizable: false,
    height:200,
    width: 500,
    dialogClass:"dialogCSS",
    modal: true,
    buttons: [
              {
                  text: "No",
                  "class": 'btn',
                  click: function() {
                	  $(this).dialog( "close" );
                  }
              },
              {
                  text: "Yes",
                  "class": 'btn btn-primary',
                  click: function() {
                	  $( this ).dialog( "close" );
                      document.location.href ='<c:url value="/shop/employer/plandisplay/confirm" />';
                  }
              }
          ],

  });
</script>