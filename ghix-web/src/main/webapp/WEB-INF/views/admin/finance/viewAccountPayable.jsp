<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<%@ page isELIgnored="false"%>


<div class="gutter10">
<div class="row-fluid">
	<ul class="page-breadcrumb">
	<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="label.back"/></a></li>
	<li class="active">Account Payable</li>
	</ul>
			<h1>View Account Payable <a href="#" rel="tooltip" data-placement="top"
			data-original-title="You can view all payments done to Issuer." 
			class="info"><i class="icon-question-sign"></i></a></h1>

</div>


<div class="row-fluid">
	<div class="span3" id="sidebar">
			<div class="header">
				<h4 class="margin0">Refine Results</h4>
			</div>
				<div class="graybg">
					<form class="form-vertical gutter10" id="frmaccountpayablefilter" name="frmaccountpayablefilter" action="accountpayable" method="POST">
					<df:csrfToken/>
            <input type="hidden" value="<encryptor:enc value = "${employerid}"/>" name="id" id="id" />
						<div class="control-group">
							<label class="control-label" for="startdate">Start Date</label>
							
							<%-- <input title="MM/DD/YYYY" class="datepick input-small"   name="startdate" id="mystartdate" value="" onChange="setdate();"/> --%>
							<div class="input-append date date-picker" id="date" data-date="">
								<input class="span10" type="text" name="startdate" id="mystartdate" value="${searchCriteria.startdate}" pattern="MM/DD/YYYY" onChange="setdate();"/> 
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
							<div id="mystartdate_error" class="help-inline"></div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="enddate">End Date</label>
							<%-- <input type="text" title="MM/DD/YYYY" class="datepick input-small" name="enddate" id="myenddate" value="${searchCriteria.enddate}"  onChange="setdate();"/> --%>
							<div class="input-append date date-picker" id="date" data-date="">
								<input class="span10" type="text" name="enddate" id="myenddate" value="${searchCriteria.enddate}" pattern="MM/DD/YYYY" onChange="setdate();"/> 
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
							<div id="myenddate_error" class="help-inline"></div>
							
						</div>
						<div class="txt-center">
							<input type="submit" name="submitBtn" id="submitBtn" Value="Go" title="Go" class="btn input-medium">
						</div>
					</form>
				</div>
	</div>


	<div class="span9" id="rightpanel">
		<form id="frmaccountpayablelist"
			name="frmaccountpayablelist" action="accountpayable" method="POST">
			<input type="hidden" value="<encryptor:enc value = "${employerid}"/>" name="id" id="id" />
			<input type="hidden" value="" name="startdate" id="startdate" />
		    <input type="hidden" value="" name="enddate" id="enddate" />
			<c:choose>
			
			    <c:when test="${not empty ErrorMsg}">
                <h4 class="alert alert-error h4">${ErrorMsg}</h4>
                </c:when>
				<c:when test="${resultSize >= 1}">
					<c:choose>
						<c:when test="${fn:length(payableList) > 0}">
								<table class="table table-striped">
									<thead>
										<tr class="header">
											<!--  span9 header -->
											<!-- <th><input type="checkbox"> </th> -->
											<th class="sortable" scope="col"><dl:sort title="Issuer Name" sortBy="issuerName"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Invoice Number" sortBy="invoiceNumber"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Statement Date" sortBy="statementDate"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>		
											<th class="sortable" scope="col"><dl:sort title="Total Amount Due" sortBy="totalAmountDue"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Paid Amount" sortBy="paidAmount"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<!-- //<th></th> -->
											<th class="sortable" scope="col"><dl:sort title="Balance Amount" sortBy="balanceAmount"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<!-- <th></th> -->
										</tr>
									</thead>
									
									<c:forEach items="${payableList}" var="payable" varStatus="vs">
									<tr>
											<!-- <td><input type="checkbox"> </td> -->
											<%-- <td>${payable.issuerId}</td> --%>
											<td>${payable.issuerName}</td>
											<td style ="white-space:nowrap">${payable.invoiceNumber}</td>
											<td><fmt:formatDate
													value="${payable.statementDate}"
													pattern="MM-dd-yyyy" /></td>
											<td><fmt:setLocale value="en_US" scope="session"/>
											<fmt:formatNumber value="${payable.totalAmountDue}" type="currency"/>
											</td>
											<td><fmt:setLocale value="en_US" scope="session"/>
											<fmt:formatNumber value="${payable.paidAmount}" type="currency"/>
											</td>
											<td>
											<fmt:setLocale value="en_US" scope="session"/>
											<fmt:formatNumber value="${payable.balanceAmount}" type="currency"/></td>
										</tr>				
									</c:forEach>
								</table>
                                <div class="pagination">
									<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}"/>
								</div>
						</c:when>
					</c:choose>
				</c:when>
				<c:otherwise>
					<h4 class="alert alert-info">
						<spring:message code='label.norecords' />
					</h4>
				</c:otherwise>
			</c:choose>
		</form>
		<div>
		<c:if test="${fn:length(payableList) gt 0}">
         <button class="btn offset2" data-dismiss="modal" aria-hidden="true" id="downloadAccountPayable">Download</button>
        </c:if>
		</div>
	</div>
</div>


<div class="row-fluid">
	
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">

				<p>The prototype showcases three scenarios (A, B and C)
					dependant on a particular Employer's eligibility to use the SHOP
					Exchange.</p>
			</div>
		</div>
	</div>

</div>
<!-- row-fluid -->
</div>

<script>
$('.date-picker').datepicker({
    autoclose: true,
    format: 'mm-dd-yyyy'
});
		/* $(function() {
			$('.datepick').each(function() {
				var ctx = "${pageContext.request.contextPath}";
				var imgpath = ctx+'/resources/images/calendar.gif';
				$(this).datepicker({
					showOn : "button",
					buttonImage : imgpath,
					buttonImageOnly : true
				});
			});
		}); */
		
		$('#downloadAccountPayable').click(function(){
			var params ="startdate="+$("#mystartdate").val() +"&"+"enddate="+$("#myenddate").val();
			window.location.href =  "${pageContext.request.contextPath}"+'/admin/downloadAccountPayable'+"?"+params;
			});


$.validator.addMethod("endDate", function(value, element) {
	var startdatevalue = $('#mystartdate').val(),
		regExp = (/(\d{1,2})\-(\d{1,2})\-(\d{2,4})/);
	
	//console.log('startdatevalue',startdatevalue)
	if (value == null || value.length <1)
		return true;
	//console.log(parseInt(value.replace(regExp, "$3$2$1")) > parseInt(startdatevalue.replace(regExp, "$3$2$1")))
	return (parseInt(value.replace(regExp, "$3$1$2")) > parseInt(startdatevalue.replace(regExp, "$3$1$2")));
});
	
function setdate(){
	$("#startdate").val($("#mystartdate").val());
	$("#enddate").val($("#myenddate").val());
}
	
var validator = $("#frmaccountpayablefilter").validate({ 
		ignore: "",
		rules : {
			enddate : { endDate : true}
		}, 
		messages : {
			enddate : { endDate : "<span> <em class='excl'>!</em><spring:message  code='label.validateEndDate'/></span>"}
		},
		highlight: function (element, errorClass) {
			$('#myenddate').removeAttr('error');
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			
			error.appendTo($("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class', 'error');
		} ,
		
});

$('.info').tooltip()
</script>
