package com.getinsured.iex.hub.vlp37.servicehandler;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.JAXBElement;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.utils.GhixHubConstants;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationIndividualResponseType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationRequestSetType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationResponseSetType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationResponseType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.vlp37.model.VLP37CaseLog;
import com.getinsured.iex.hub.vlp37.prefixmappers.VLPNamespacePrefixMapper;
import com.getinsured.iex.hub.vlp37.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp37.service.VLPServiceConstants.VLP_DHS_ID_TYPES;
import com.getinsured.iex.hub.vlp37.service.VLP_PostProcessor;
import com.getinsured.iex.hub.vlp37.wrapper.CertOfCitDocumentID23Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.DHSIDTypeWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.DS2019DocumentID27Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.I20DocumentID26Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.I327DocumentID3Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.I551DocumentID4Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.I571DocumentID5Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.I766DocumentID9Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.I94DocumentID2Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.I94UnexpForeignPassportDocumentID10Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.InitialVerificationIndividualResponseTypeWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.InitialVerificationRequestTypeWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.InitialVerificationResponseSetTypeWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.InitialVerificationResponseTypeWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.MacReadI551DocumentID22Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.NatrOfCertDocumentID20Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.OtherCase1DocumentID1Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.OtherCase2DocumentID1Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.TempI551DocumentID21Wrapper;
import com.getinsured.iex.hub.vlp37.wrapper.UnexpForeignPassportDocumentID30Wrapper;


@SuppressWarnings("rawtypes")
public class InitialVerificationServiceHandler extends AbstractServiceHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(InitialVerificationServiceHandler.class);
	
	private static ObjectFactory factory;
	
	private static Map<Object,Class> dhsIdMappings;
	
	private static Map<String,String> dhsIdKeys;
	
	static{
		
		if(factory == null){
			factory = new ObjectFactory();
		}
		
		dhsIdMappings = new HashMap<Object, Class>();
		
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I327.toString(),I327DocumentID3Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I551.toString(),I551DocumentID4Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I571.toString(),I571DocumentID5Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I766.toString(),I766DocumentID9Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.CertOfCit.toString(),CertOfCitDocumentID23Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.NatrOfCert.toString(),NatrOfCertDocumentID20Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.MacReadI551.toString(),MacReadI551DocumentID22Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.TempI551.toString(),TempI551DocumentID21Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I94Document.toString(),I94DocumentID2Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I94UnexpForeignPassport.toString(),I94UnexpForeignPassportDocumentID10Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.UnexpForeignPassport.toString(),UnexpForeignPassportDocumentID30Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I20.toString(),I20DocumentID26Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.DS2019.toString(),DS2019DocumentID27Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.OtherCase1.toString(),OtherCase1DocumentID1Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.OtherCase2.toString(),OtherCase2DocumentID1Wrapper.class);
		
		dhsIdKeys = new HashMap<String, String>();
		
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I327.toString(),"I327DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I551.toString(),"I551DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I571.toString(),"I571DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I766.toString(),"I766DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.CertOfCit.toString(),"CertOfCitDocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.NatrOfCert.toString(),"NatrOfCertDocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.MacReadI551.toString(),"MacReadI551DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.TempI551.toString(),"TempI551DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I94Document.toString(),"I94DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I94UnexpForeignPassport.toString(),"I94UnexpForeignPassportDocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.UnexpForeignPassport.toString(),"UnexpForeignPassportDocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I20.toString(),"I20DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.DS2019.toString(),"DS2019DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.OtherCase1.toString(),"OtherCase1DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.OtherCase2.toString(),"OtherCase2DocumentID");
	}

	private InitialVerificationRequestType reqObj;

	private Jaxb2Marshaller marshaller;
	
	/**
	 * Method to generate the payload for request
	 * 
	 * author - Nikhil Talreja
	 * since - 14-Jan-2014
	 * @throws HubServiceException 
	 * @throws  
	 * @throws HubMappingException 
	 */
	
	private DHSIDTypeWrapper setDocumentType(String dhsIdType,String clsName) throws HubServiceException{
		
		Object obj = getNamespaceObjects().get(clsName);
		if(obj == null){
			throw new HubServiceException("Invalid DHSID type ["+dhsIdType+"] provided, no data found for this document");
		}
		
		return new DHSIDTypeWrapper(obj);
	}
	
	private String validateDHSID(JSONObject payloadObj, String dhsIdType) throws HubServiceException{
		
		/*
		 * Checking if provided DHS ID key in JSON input is same as 
		 * object Identifier
		 */
		Object dhsId = payloadObj.get("DHSID");
		if(dhsId == null){
			throw new HubServiceException("DHS Id is missing in request");
		}
		JSONObject dhsIdObject = (JSONObject)dhsId;
		String dhsIdValue = (String)dhsIdObject.keySet().toArray()[0];
		
		LOGGER.info("Received DHS ID type : " + dhsIdType);
		Class cls = dhsIdMappings.get(dhsIdType);
		
		if(dhsIdType == null || cls == null){
			throw new HubServiceException("The DHS Id Type " + dhsIdType + " is not valid for this request, one of following DHSID (\"documentType\") types is expected:["+VLPServiceConstants.getAvailableDHSIdTypes()+"]");
		}
		
		
		if(!dhsIdKeys.get(dhsIdType).equalsIgnoreCase(dhsIdValue)){
			throw new HubServiceException("Mismatch in DHS ID information provided. documentType is " + dhsIdType + " but payload contains " + dhsIdValue + " information.");
		}
		
		return cls.getName();
		
	}
	
	public Object getRequestpayLoad() throws HubServiceException{
			if(this.reqObj != null) {
				return this.reqObj;
			}
			String dhsIdType = (String) getContext().get("objectIdentifier");
			try{
				JSONParser parser = new JSONParser();
				JSONObject payloadObj = (JSONObject) parser.parse(getJsonInput());
				
				String clsName = validateDHSID(payloadObj, dhsIdType);
				
				processJSONObject(payloadObj);
				
				InitialVerificationRequestTypeWrapper obj = (InitialVerificationRequestTypeWrapper)getNamespaceObjects().get(InitialVerificationRequestTypeWrapper.class.getName());
				if(obj == null){
					throw new HubServiceException("required VLP Request parameters not available");
				}
				obj.setDhsid(this.setDocumentType(dhsIdType,clsName));
				InitialVerificationRequestSetType request = obj.getRequest();
				reqObj = factory.createInitialVerificationRequestType();
				reqObj.getInitialVerificationRequestSet().add(request);
				setJsonInput(obj.toJSONString());
				return reqObj;
			}catch(Exception pe){
				throw new HubServiceException("Failed to generate the payload:",pe);
			}
			
	}
	
	@SuppressWarnings("unchecked")
	private void processJSONObject(JSONObject obj) throws HubServiceException, HubMappingException{
	        Set<Entry<String, Object>> keySet = obj.entrySet();
	        Iterator<Entry<String, Object>> cursor = keySet.iterator();
	        Entry<String, Object> jsonElement = null;
	        while(cursor.hasNext()){
	               jsonElement = cursor.next();
	               handleJsonElement(jsonElement.getKey(), jsonElement.getValue());
	        }
	 }
	 
	 private void handleJsonElement(String key, Object value) throws HubServiceException, HubMappingException {
	        if(value instanceof JSONObject){
	           processJSONObject((JSONObject)value);
	        }else if(value instanceof JSONArray){
	        	processJSONArray((JSONArray) value);
	        }
	        else if(value instanceof String || value instanceof Boolean){
	        	this.handleInputParameter(key, value);
	        }
	 }

	
	private void processJSONArray(JSONArray value) throws HubServiceException, HubMappingException {
		int arrayLen = value.size();
		Object tmp;
		for(int i = 0; i < arrayLen; i++){
			tmp = value.get(i);
			if(tmp instanceof JSONObject){
				processJSONObject((JSONObject)tmp);
			}
			// should we expect anything other than JSONObject in this array?
		}
	}

	
	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		String message = "";
		try{
			Map<String, Object> ctx = this.getContext();
			JAXBElement<InitialVerificationResponseType> resp= (JAXBElement<InitialVerificationResponseType>)responseObject;
			InitialVerificationResponseType payload = resp.getValue();
			
			InitialVerificationResponseTypeWrapper response = new InitialVerificationResponseTypeWrapper(payload);
			InitialVerificationIndividualResponseTypeWrapper individualResponse = null; 
			InitialVerificationResponseSetTypeWrapper initialVerificationResponseSetTypeWrapper = 
					new InitialVerificationResponseSetTypeWrapper(payload.getInitialVerificationResponseSet());
			response.setInitialVerificationResponseSet(initialVerificationResponseSetTypeWrapper);
			if(response != null && response.getInitialVerificationResponseSet() != null){	
				message = response.toJSONString();
				/*
				 * To set Response code for VLP, we will use the first response in the set of individual responses
				 * Since VLP service will always be called separately for each applicant, we will receive at most
				 * once response 
				 */
				individualResponse = response.getInitialVerificationResponseSet().getInitVerifIndividualResponse().get(0);
				if(individualResponse != null && individualResponse.getResponseMetadata() != null){
					this.setResponseCode(individualResponse.getResponseMetadata().getResponseCode());
				}
				if(!GhixHubConstants.VLP_2_3_Enabled) {
					this.requestStatus = RequestStatus.PENDING_CLOSE;
				}else {
					this.requestStatus = RequestStatus.HUB_ERROR_RESPONSE;
					InitialVerificationResponseSetType individualResponseSet = payload.getInitialVerificationResponseSet();
					if(individualResponseSet != null) {
						List<InitialVerificationIndividualResponseType> individualResponseList = individualResponseSet.getInitialVerificationIndividualResponse();
						if(individualResponseList != null && individualResponseList.size() > 0) {
							this.requestStatus=RequestStatus.SUCCESS;
						}
					}
				}
			}
			VLP_PostProcessor responseHandler = (VLP_PostProcessor) ctx.get(ServiceHandler.RESPONSE_HANDLER);
			responseHandler.submit(this.reqObj, payload);
			getCaseNumber(individualResponse);
			return message;
		}catch(ClassCastException ce){
			message = "Help! I do not any thing about "+responseObject.getClass().getName()+" Expecting:"+InitialVerificationResponseTypeWrapper.class.getName();
			this.requestStatus = RequestStatus.HUB_ERROR_RESPONSE;
			throw new HubServiceException(message, ce);
		}
	}
	
	private void getCaseNumber(InitialVerificationIndividualResponseTypeWrapper individualResponse){
		
		//Case number
		if(individualResponse != null && individualResponse.getInitVerifIndividualResponseSet() != null){
			this.getResponseContext().put("HUB_CASE_NUMBER", individualResponse.getInitVerifIndividualResponseSet().getCaseNumber());
			/*this.getResponseContext().put("NonCitLastName", individualResponse.getInitVerifIndividualResponseSet().getNonCitLastName());
			this.getResponseContext().put("NonCitFirstName", individualResponse.getInitVerifIndividualResponseSet().getNonCitFirstName());
			this.getResponseContext().put("EligStatementCd", individualResponse.getInitVerifIndividualResponseSet().getEligStatementCd());
			this.getResponseContext().put("EligStatementTxt", individualResponse.getInitVerifIndividualResponseSet().getEligStatementTxt());
			this.getResponseContext().put("WebServSftwrVer", individualResponse.getInitVerifIndividualResponseSet().getWebServSftwrVer());
			this.getResponseContext().put("FiveYearBarApplyCode", individualResponse.getInitVerifIndividualResponseSet().getFiveYearBarApplyCode());
			this.getResponseContext().put("QualifiedNonCitizenCode", individualResponse.getInitVerifIndividualResponseSet().getQualifiedNonCitizenCode());
			this.getResponseContext().put("FiveYearBarMetCode", individualResponse.getInitVerifIndividualResponseSet().getFiveYearBarMetCode());
			this.getResponseContext().put("USCitizenCode", individualResponse.getInitVerifIndividualResponseSet().getUsCitizenCode());*/
		}
		
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}
	
	@Override
	public String getServiceIdentifier() {
		return "H92";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}

}
