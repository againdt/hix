<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="stateCode" value="${StateConfiguration.stateCode}" />
<c:if test="${stateCode == 'CA'}">
  <jsp:include page="./ca/footer.jsp" />
</c:if>
<c:if test="${stateCode == 'CT'}">
  <jsp:include page="./ct/footer.jsp" />
</c:if>
<c:if test="${stateCode == 'ID'}">
  <jsp:include page="./id/footer.jsp" />
</c:if>
<c:if test="${stateCode == 'MN'}">
  <jsp:include page="./mn/footer.jsp" />
</c:if>
<c:if test="${stateCode == 'NV'}">
  <jsp:include page="./nv/footer.jsp" />
</c:if>
<c:if test="${stateCode == 'WA'}">
  <jsp:include page="./wa/footer.jsp" />
</c:if>
