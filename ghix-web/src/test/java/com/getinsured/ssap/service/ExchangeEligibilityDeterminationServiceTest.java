package com.getinsured.ssap.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.ssap.model.eligibility.EligibilityResponse;
import com.getinsured.ssap.model.eligibility.HouseholdMemberResponse;

/**
 * JUnit test cases for exchange eligibility determination service
 *
 *
 * @author Yevgen Golubenko
 * @since 5/23/19
 */
public class ExchangeEligibilityDeterminationServiceTest {

  private ExchangeEligibilityDeterminationService exchangeEligibilityDeterminationService;
  private SsapApplication application = null;
  private EligibilityResponse eligibilityResponse = null;

  @Before
  public void setup() {
    exchangeEligibilityDeterminationService = new ExchangeEligibilityDeterminationService();
    application = new SsapApplication();
    eligibilityResponse = new EligibilityResponse();
  }

  @Test
  public void determinateExchangeEligibility_APTC_CSR() {

    eligibilityResponse.setAptcEligible(true);

    HouseholdMemberResponse hhm1 = new HouseholdMemberResponse();
    HouseholdMemberResponse hhm2 = new HouseholdMemberResponse();
    HouseholdMemberResponse hhm3 = new HouseholdMemberResponse();

    hhm1.setCsrEligible(true);
    hhm2.setCsrEligible(true);
    hhm3.setCsrEligible(true);

    List<HouseholdMemberResponse> members = new ArrayList<>(Arrays.asList(hhm1, hhm2, hhm3));
    eligibilityResponse.setMembers(members);

    application = exchangeEligibilityDeterminationService.determinateExchangeEligibility(application, eligibilityResponse);
    Assert.assertNotNull("SsapApplication was null", application);
    Assert.assertEquals("Exchange Eligibility status is not APTC_CSR", ExchangeEligibilityStatus.CSR,
        application.getExchangeEligibilityStatus());

  }

  @Test
  public void determinateExchangeEligibility_APTC_CSR_2() {
    eligibilityResponse.setAptcEligible(true);

    HouseholdMemberResponse hhm1 = new HouseholdMemberResponse();
    HouseholdMemberResponse hhm2 = new HouseholdMemberResponse();
    HouseholdMemberResponse hhm3 = new HouseholdMemberResponse();

    hhm1.setCsrEligible(false);
    hhm2.setCsrEligible(true);
    hhm3.setCsrEligible(true);

    List<HouseholdMemberResponse> members = new ArrayList<>(Arrays.asList(hhm1, hhm2, hhm3));
    eligibilityResponse.setMembers(members);

    application = exchangeEligibilityDeterminationService.determinateExchangeEligibility(application, eligibilityResponse);
    Assert.assertNotNull("SsapApplication was null", application);
    Assert.assertEquals("Exchange Eligibility status is not APTC_CSR", ExchangeEligibilityStatus.CSR,
        application.getExchangeEligibilityStatus());

  }

  @Test
  public void determinateExchangeEligibility_APTC() {

    eligibilityResponse.setAptcEligible(true);

    HouseholdMemberResponse hhm1 = new HouseholdMemberResponse();
    HouseholdMemberResponse hhm2 = new HouseholdMemberResponse();
    HouseholdMemberResponse hhm3 = new HouseholdMemberResponse();
    hhm3.setAptcEligible(true);

    List<HouseholdMemberResponse> members = new ArrayList<>(Arrays.asList(hhm1, hhm2, hhm3));
    eligibilityResponse.setMembers(members);

    application = exchangeEligibilityDeterminationService.determinateExchangeEligibility(application, eligibilityResponse);
    Assert.assertNotNull("SsapApplication was null", application);
    Assert.assertEquals("Exchange Eligibility status is not APTC", ExchangeEligibilityStatus.APTC,
        application.getExchangeEligibilityStatus());
  }

  @Test
  public void determinateExchangeEligibility_CSR() {

    HouseholdMemberResponse hhm1 = new HouseholdMemberResponse();
    HouseholdMemberResponse hhm2 = new HouseholdMemberResponse();
    HouseholdMemberResponse hhm3 = new HouseholdMemberResponse();
    hhm1.setCsrEligible(true);

    List<HouseholdMemberResponse> members = new ArrayList<>(Arrays.asList(hhm1, hhm2, hhm3));
    eligibilityResponse.setMembers(members);

    application = exchangeEligibilityDeterminationService.determinateExchangeEligibility(application, eligibilityResponse);
    Assert.assertNotNull("SsapApplication was null", application);
    Assert.assertEquals("Exchange Eligibility status is not CSR", ExchangeEligibilityStatus.CSR,
        application.getExchangeEligibilityStatus());
  }

  @Test
  public void determinateExchangeEligibility_NONE() {
    HouseholdMemberResponse hhm1 = new HouseholdMemberResponse();
    HouseholdMemberResponse hhm2 = new HouseholdMemberResponse();
    HouseholdMemberResponse hhm3 = new HouseholdMemberResponse();

    List<HouseholdMemberResponse> members = new ArrayList<>(Arrays.asList(hhm1, hhm2, hhm3));
    eligibilityResponse.setMembers(members);

    application = exchangeEligibilityDeterminationService.determinateExchangeEligibility(application, eligibilityResponse);
    Assert.assertNotNull("SsapApplication was null", application);
    Assert.assertEquals("Exchange Eligibility status is not NONE", ExchangeEligibilityStatus.NONE,
        application.getExchangeEligibilityStatus());

  }

  @Test
  public void determinateExchangeEligibility_QHP() {
    HouseholdMemberResponse hhm1 = new HouseholdMemberResponse();
    HouseholdMemberResponse hhm2 = new HouseholdMemberResponse();
    HouseholdMemberResponse hhm3 = new HouseholdMemberResponse();

    hhm1.setExchangeEligible(true);
    hhm2.setExchangeEligible(true);
    hhm3.setExchangeEligible(true);

    List<HouseholdMemberResponse> members = new ArrayList<>(Arrays.asList(hhm1, hhm2, hhm3));
    eligibilityResponse.setMembers(members);

    application = exchangeEligibilityDeterminationService.determinateExchangeEligibility(application, eligibilityResponse);
    Assert.assertNotNull("SsapApplication was null", application);
    Assert.assertEquals("Exchange Eligibility status is not QHP", ExchangeEligibilityStatus.QHP,
        application.getExchangeEligibilityStatus());
  }
}
