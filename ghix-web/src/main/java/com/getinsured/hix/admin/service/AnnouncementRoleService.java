package com.getinsured.hix.admin.service;

import java.util.List;

import com.getinsured.hix.model.Announcement;
import com.getinsured.hix.model.AnnouncementRole;
import com.getinsured.hix.model.Role;


public interface AnnouncementRoleService {
	public List<Role> findRolesByAnnouncement(Announcement announcement);
	
	public AnnouncementRole setAnnouncementRole(Announcement announcement, Role role);
	
	public void deleteAnnouncementRole(Announcement announcement);	
	
}