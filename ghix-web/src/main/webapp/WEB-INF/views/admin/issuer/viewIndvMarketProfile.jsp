<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ page import="com.getinsured.hix.platform.util.PhoneUtil"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js"></script>
<div class="gutter10">
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/issuer/company/profile/${encIssuerId}"/>">&lt; <spring:message  code="label.back"/></a></li>
       		<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
            <li><spring:message  code="pgheader.update.inidividualMarketProfile"/></li>
        </ul><!--page-breadcrumb ends-->
    </div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
			</div>
			<div class="span9">
				<h1>${issuerObj.name}</h1>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
				<h4 class="margin0"><spring:message  code="pgheader.issuerAbout"/></h4>
			</div>
			<!--  beginning of side bar -->
			<jsp:include page="issuerDetailsLeftNav.jsp">
        			<jsp:param name="pageName" value="individualProfile"/>
			</jsp:include>
			<!-- end of side bar -->
		</div>
			<!-- end of span3 -->
		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="pull-left"><spring:message  code="pgheader.update.inidividualMarketProfile"/></h4>
				<a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/market/individual/profile/edit/${encIssuerId}"/>"><spring:message  code="label.edit"/></a>
			</div>
			<div class="gutter10">		
				<form class="form-horizontal" id="frmIssuerAcctIndvdlMarktProfInfo" name="frmIssuerAcctIndvdlMarktProfInfo" action="" method="POST">
				
				<table class="table table-border-none">
					<tbody>
						<tr>
							<th class="txt-right span4">
								<spring:message  code="label.customerServicePhone"/>
							</th>
							<td>
								<c:set var="custServPhone" value="${issuerObj.indvCustServicePhone}"/>
								<strong class="marginR10"><%=PhoneUtil.formatElevenDigitPhoneNumber((String)pageContext.getAttribute("custServPhone"))%></strong>  
								&nbsp; &nbsp;
								<spring:message  code="label.phone.ext"/>&nbsp;
								<strong class="marginL10">${issuerObj.indvCustServicePhoneExt}</strong>
							</td>
						</tr>
						<tr>
							<th class="txt-right span4">
								<spring:message  code="label.customerServiceTollFreeNumber"/>
							</th>
							<td>
								<c:set var="custTollFree" value="${issuerObj.indvCustServiceTollFree}"/>
								<strong><%=PhoneUtil.formatElevenDigitPhoneNumber((String)pageContext.getAttribute("custTollFree"))%></strong>  
							</td>
						</tr>
						<tr>
							<th class="txt-right span4">
								<spring:message  code="label.customerServiceTTY"/>
							</th>
							<td>
								<c:set var="custServTTY" value="${issuerObj.indvCustServiceTTY}"/>
								<strong><%=PhoneUtil.formatElevenDigitPhoneNumber((String)pageContext.getAttribute("custServTTY"))%></strong>  
							</td>
						</tr>
						<tr>
							<th class="txt-right span4">
								<spring:message  code="label.consumerFacingWebSiteURL"/>
							</th>
							<td>
								<a href="${issuerObj.indvSiteUrl}"><strong>${issuerObj.indvSiteUrl}</strong></a>
							</td>
						</tr>
					</tbody>
				</table>				
				
				<br>
				
                <!--      <input type="hidden" id="market" name="market" value="individual"/>
                    <div class="control-group">
						<label class="control-label" for="cust-serv-phone"><spring:message  code="label.customerServicePhone"/></label>
						<c:set var="custServPhone" value="${issuerObj.indvCustServicePhone}"/>                                        
						<div class="controls span4 paddingT5"><strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custServPhone"))%></strong></div>
                        <label class="control-label span2 marginR10" for="cust-ext"><spring:message  code="label.phone.ext"/></label>
						<div class="controls paddingT5"><strong>${issuerObj.indvCustServicePhoneExt}</strong></div>
					</div><!-- end of control-group-->
                 <!--    
                    <div class="control-group">
						<label class="control-label" for="cust-toll-free"><spring:message  code="label.customerServiceTollFreeNumber"/></label>
						<c:set var="custTollFree" value="${issuerObj.indvCustServiceTollFree}"/>
						<div class="controls paddingT5"><strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custTollFree"))%></strong></div>
					</div>
                    
                    <div class="control-group">
						<label class="control-label" for="cust-serv-tty"><spring:message  code="label.customerServiceTTY"/></label>
						<c:set var="custServTTY" value="${issuerObj.indvCustServiceTTY}"/>
						<div class="controls paddingT5"><strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custServTTY"))%></strong></div>
					</div>
                    
                    <div class="control-group">
						<label class="control-label" for="cust-website-url"><spring:message  code="label.consumerFacingWebSiteURL"/></label>
						<div class="controls paddingT5"><strong>${issuerObj.indvSiteUrl}</strong></div>
					</div>
					-->
				</form>	
			</div><!--  end of gutter10 inside span9-->		
		</div><!--  end of span9 -->
	</div><!-- end row-fluid -->
</div><!--  end of main gutter10-->	
