<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<style>
	.error:after {
		padding-top:10px;
	}
</style>


<div class="gutter10">
	<div class="row-fluid">
		<h1><a name="skip"></a>New <c:out value="${roleTitle}" /> Account Set-Up</h1>
	</div><!-- end of page-header -->

		<!-- start of secondary navbar - TO DO: Move to topnav.jsp -->
	<!-- 	<ul class="nav nav-tabs">
			<li class="active"><a href="#">Home</a></li>
			<li><a href="#">Find Insurance</a></li>
			<li><a href="#">Learn More</a></li>   
			<li><a href="#">Get Assistance</a></li>   
		</ul> -->
		<!-- end of secondary navbar -->

	<div class="row-fluid">
		<!-- add id for skip side bar -->
		<div class="span3" id="sidebar">
			<div class="header">
				<h4 class="margin0">Steps</h4>
			</div>
				<ol class="nav nav-list">
					<li><a href="#">Basic Information</a></li>
					<li class="active"><a href="#">Security Questions</a></li>
				</ol>
		</div>
		<!-- end of span3 -->


		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="margin0">Security  Questions</h4>
			</div>
				<form class="form-vertical gutter10" id="frmsecurityquesions" name="frmsecurityquesions" action="securityquestions" method="POST">
					<df:csrfToken/>
					<div class="control-group security-quest">
						<label for="securityQuestion1" class="control-label required">Security question 1<span class="aria-hidden">required</span>: <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<select  id="securityQuestion1" name="securityQuestion1" path="securityQuestionsList1" class="span8">
								<option value="">Select</option>
								<c:forEach var="questionList" items="${securityQuestionsList1}">
									<option value="<c:out value="${questionList.name}" />">
										<c:out value="${questionList.name}" />
									</option>
								</c:forEach>
							</select>
							<div id="securityQuestion1_error"></div>
						</div>	<!-- end of controls-->
					</div><!-- end of control-group -->
					
					<div class="control-group security-ans">
						<label for="securityAnswer1" class="control-label required">Answer</label>
						<div class="controls">
							<input type="text" name="securityAnswer1" id="securityAnswer1" class="span8" />							
							<div id="securityAnswer1_error"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->
					
					<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" /> 
					<c:if test="${stateCode ne 'ID'}">
						<div class="control-group security-quest">
							<label for="securityQuestion2" class="control-label required">Security question 2<span class="aria-hidden">required</span>: <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select  id="securityQuestion2" name="securityQuestion2" path="securityQuestionsList2" class="span8">
									<option value="">Select</option>
									<c:forEach var="questionList" items="${securityQuestionsList2}">
										<option value="<c:out value="${questionList.name}" />">
											<c:out value="${questionList.name}" />
										</option>
									</c:forEach>
								</select>
								<div id="securityQuestion2_error"></div>
							</div>	<!-- end of controls-->
						</div><!-- end of control-group -->
					
					
						<div class="control-group security-ans">
							<label for="securityAnswer2" class="control-label required">Answer</label>
							<div class="controls">
								<input type="text" name="securityAnswer2" id="securityAnswer2" class="span8">
								<div id="securityAnswer2_error"></div>
							</div>	<!-- end of controls-->
						</div>	<!-- end of control-group -->
						<div class="control-group security-quest">
							<label for="securityQuestion3" class="control-label required">Security question 3<span class="aria-hidden">required</span>: <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select  id="securityQuestion3" name="securityQuestion3" path="securityQuestionsList3" class="span8">
									<option value="">Select</option>
										<c:forEach var="questionList" items="${securityQuestionsList3}">
											<option value="<c:out value="${questionList.name}" />">
												<c:out value="${questionList.name}" />
											</option>
										</c:forEach>
								</select>
								<div id="securityQuestion3_error"></div>
							</div>	<!-- end of controls-->
						</div><!-- end of control-group -->
					</c:if>
					<div class="control-group security-ans">
						<label for="securityAnswer3" class="control-label required">Answer</label>
						<div class="controls">
							<input type="text"  name="securityAnswer3" id="securityAnswer3" class="span8">
							<div id="securityAnswer3_error"></div>
						</div>	<!-- end of controls-->
					</div>	<!-- end of control-group -->
            <input type="hidden" name="defUserRoleName" id="defUserRoleName" value="${defUserRoleName}" />
				<div class="form-actions">
					<input type="submit" name="submit" id="submit" value="Continue" class="btn btn-primary" title="Continue" /> 
				</div><!-- end of form-actions -->
			</form>
			</div><!-- end of .span9 -->
		</div><!-- end of .row-fluid -->
	</div>
	<script>
	var validator = $("#frmsecurityquesions").validate({ 
		rules : { securityQuestion1 : { required : true},
			securityQuestion2 : { required : true},
			securityQuestion3 : { required : true},
			securityAnswer1 : { required : true,maxlength : 4000},
			securityAnswer2 : { required : true,maxlength : 4000},
			securityAnswer3 : { required : true,maxlength : 4000}
		},
		messages : {
			
			securityQuestion1: { required : "<span> <em class='excl'>!</em>Please select your security question.</span>"},
			securityQuestion2 : { required : "<span> <em class='excl'>!</em>Please select your security question.</span>"},
			securityQuestion3: { required : "<span> <em class='excl'>!</em>Please select your security question.</span>"},
			securityAnswer1: { required : "<span> <em class='excl'>!</em>Please provide your security answer.</span>",
				   			   maxlength : "<span> <em class='excl'>!</em>Entered details should not be more than 4000 characters.</span>"},
			securityAnswer2 : { required : "<span> <em class='excl'>!</em>Please provide your security answer.</span>",
				   				maxlength : "<span> <em class='excl'>!</em>Entered details should not be more than 4000 characters.</span>"},
			securityAnswer3: { required : "<span> <em class='excl'>!</em>Please provide your security answer.</span>",
				   			   maxlength : "<span> <em class='excl'>!</em>Entered details should not be more than 4000 characters.</span>"}
			
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo($("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class',
					'error help-inline');
		} 
		
	});
	
	
	</script>
