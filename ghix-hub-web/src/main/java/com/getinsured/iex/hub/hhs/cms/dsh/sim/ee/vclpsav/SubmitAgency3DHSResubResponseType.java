
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpsav;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmitAgency3DHSResubResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitAgency3DHSResubResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}SubmitAgency3DHSResubResponseSet" minOccurs="0"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}ResponseMetadata"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitAgency3DHSResubResponseType", propOrder = {
    "submitAgency3DHSResubResponseSet",
    "responseMetadata"
})
public class SubmitAgency3DHSResubResponseType {

    @XmlElement(name = "SubmitAgency3DHSResubResponseSet")
    protected SubmitAgency3DHSResubResponseSetType submitAgency3DHSResubResponseSet;
    @XmlElement(name = "ResponseMetadata", required = true)
    protected ResponseMetadataType responseMetadata;

    /**
     * Gets the value of the submitAgency3DHSResubResponseSet property.
     * 
     * @return
     *     possible object is
     *     {@link SubmitAgency3DHSResubResponseSetType }
     *     
     */
    public SubmitAgency3DHSResubResponseSetType getSubmitAgency3DHSResubResponseSet() {
        return submitAgency3DHSResubResponseSet;
    }

    /**
     * Sets the value of the submitAgency3DHSResubResponseSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmitAgency3DHSResubResponseSetType }
     *     
     */
    public void setSubmitAgency3DHSResubResponseSet(SubmitAgency3DHSResubResponseSetType value) {
        this.submitAgency3DHSResubResponseSet = value;
    }

    /**
     * Gets the value of the responseMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMetadataType }
     *     
     */
    public ResponseMetadataType getResponseMetadata() {
        return responseMetadata;
    }

    /**
     * Sets the value of the responseMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMetadataType }
     *     
     */
    public void setResponseMetadata(ResponseMetadataType value) {
        this.responseMetadata = value;
    }

}
