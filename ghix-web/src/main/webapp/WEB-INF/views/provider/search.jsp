<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page isELIgnored="false"%>
    <script src="../resources/js/bootstrap-tab.js" type="text/javascript"></script>
    <link href="../resources/css/provider.css" media="screen" rel="stylesheet" type="text/css" />
    <script src="../resources/js/vimo.provider.js" type="text/javascript"></script>
	<script type="text/javascript" src='<c:url value="/resources/js/jquery.form.js" />'></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
	
	<div class="form-horizontal"><!--  not a form but necessary for the actions -->

        <div class="gutter10">
            <h1><spring:message code='label.searchProvidersHeader'/></h1>
            <h2 class="sub-h1">
                <c:if test="${not empty plan.id}">
                	<!-- Carrier Name: example provided -->${plan.issuer.name} - <!--  Plan Name: example provided -->${plan.name}&nbsp;
                </c:if>
	            <c:if test="${fn:toUpperCase(plan.networkType) == 'HMO'}">
					<spring:message code='label.hmo' javaScriptEscape='true'/>
				</c:if>	
				<c:if test="${fn:toUpperCase(plan.networkType) == 'PPO'}">
					<spring:message code='label.ppo' javaScriptEscape='true'/>
				</c:if>
		   </h2>
        </div>

        <div class="gutter10">
	        <div class="row-fluid">
	            <div class="tabbable">
	                <ul class="nav nav-tabs">
	                    <li id="doctors" class="active"><a href="#" data-toggle="tab" onclick="javascript:selectTab('tab-doctors');"><spring:message code='label.doctors'/></a></li>
	                    <li id="hospitals"><a href="#" data-toggle="tab" onclick="javascript:selectTab('tab-hospitals')"><spring:message code='label.hospitalsFacilities'/></a></li>
	                    <li id="dentists"><a href="#" data-toggle="tab" onclick="javascript:selectTab('tab-dentists')"><spring:message code='label.dentists'/></a></li>
	                </ul>
	                <div class="tab-content">
	
	                    <div id="tab-doctors" class="tab-pane active"><!-- doctors -->
	
	                        <form class="form-horizontal modal-form" id="frmProviderSearchDoctors" name="frmProviderSearchDoctors" action='<c:url value="/provider/search/doctors"/>' method="post">
	                         <input type="hidden" id="planId" name="planId" value="${plan.id}"/>   
	                            <div class="control-group">
	                                <label for="doctorName" class="control-label"><spring:message code='label.name'/></label>
	                                <div class="controls">
	                                    <input type="text" id="doctorName" name="doctorName" class="input-xlarge" value="" />
	                                </div>
	                            </div>
	
	                            <div class="control-group">
	                                <label for="doctorNearZip" class="control-label"><spring:message code='label.zip'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                                <div class="controls">
	                                    <input type="text" id="doctorNearZip" name="doctorNearZip" class="input-medium" value="" />
	                                </div>
	                                <div class="controls">
									     <div id="doctorNearZip_error" class=""></div>
								    </div>
	                            </div>
	
	                            <div class="control-group">
	                                <label for="doctorDistance" class="control-label"><spring:message code='label.distance'/></label>
	                                <div class="controls">
	                                    <select id="doctorDistance" name="doctorDistance" class="input-mini">
		                                    <option value="2">2</option>
		                                    <option value="5" selected="selected">5</option>
		                                    <option value="10">10</option>
		                                    <option value="15">15</option>
		                                    <option value="20">20</option>
		                                    <option value="30">30</option>
		                                    <option value="more">50+</option>
		                                </select>
		                                <spring:message code='label.miles'/>
	                                </div>
	                            </div>
	                            
	                            <div class="control-group">
	                                <label for="doctorSpecialties" class="control-label"><spring:message code='label.specialty'/></label>
	                                <div class="controls">
		                                <select id="doctorSpecialties" class="input-xlarge" name="doctorSpecialties">
										<option value=""><spring:message code='label.selectOption'/></option>
										<c:forEach var="specialtyObj" items="${listDoctorSpecialties}">
											<c:set var="parentSpecialtyObj" value="${specialtyObj.key}"/>
											<option value="${parentSpecialtyObj.id}" style="font-weight: bold;">${parentSpecialtyObj.name}</option>
												<c:forEach var="childSpecialtyObj" items="${specialtyObj.value}">
													<option value="${childSpecialtyObj.id}">&nbsp;&nbsp;${childSpecialtyObj.name}</option>
												</c:forEach>
										    </optgroup>  
										</c:forEach>
										</select>
	                                </div>
	                            </div>
	
	                            <div class="control-group">
	                                <label for="doctorGender" class="control-label"><spring:message code='label.gender'/></label>
	                                <div class="controls">
	                                    <select id="doctorGender" name="doctorGender" class="input-xlarge">
		                                    <option value=""><spring:message code='label.selectOption'/></option>
		                                    <c:forEach var="genderObj" items="${listGender}">
												<option value="${genderObj.key}"><spring:message code='label.gender.${genderObj.value}' javaScriptEscape='true'/></option>
											</c:forEach>
		                                </select>
	                                </div>
	                            </div>
	
	                            <div class="control-group">
	                                <label for="doctorLanguages" class="control-label"><spring:message code='label.languages'/></label>
	                                <div class="controls">
		                                <select id="doctorLanguages" class="input-xlarge" name="doctorLanguages">
										<option value=""><spring:message code='label.selectOption'/></option>
										<c:forEach var="languageObj" items="${listLanguages}">
		    								<option value="${languageObj.key}">${languageObj.value}</option>
										</c:forEach>
		                        		</select>
	                                    <br />
	                                    <spring:message code='label.alldoctorsSpeakEng'/>
	                                </div>
	                            </div>
	
	                        </form>
	
	                    </div><!-- #tab-doctors -->
	
	
	
	                    <div id="tab-hospitals" class="tab-pane"><!-- hospitals -->
	
	                        <form class="form-horizontal modal-form" id="frmProviderSearchHospitals" name="frmProviderSearchHospitals" action='<c:url value="/provider/search/hospitals"/>' method="post"> 
								<input type="hidden" id="planId" name="planId" value="${plan.id}"/>
								<div class="control-group">
	                                <label class="control-label" for="hospital_name"><spring:message code='label.name'/></label>
	                                <div class="controls">
	                                    <input type="text" id="hospitalName" name="hospitalName" class="input-xlarge" value="" />
	                                </div>
	                            </div>
	                            
	                            <div class="control-group">
	                                <label class="control-label" for="hospital_near_zip"><spring:message code='label.zip'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                                <div class="controls">
	                                    <input type="text" id="hospitalNearZip" name="hospitalNearZip" class="input-medium" value="" />
	                                </div>
	                                 <div class="controls">
									     <div id="hospitalNearZip_error" class=""></div>
								    </div>
	                            </div>
	
	                            <div class="control-group">
	                                <label class="control-label" for="hospital_distance"><spring:message code='label.distance'/></label>
	                                <div class="controls">
	                                    <select id="hospitalDistance" name="hospitalDistance" class="input-mini">
		                                    <option value="2">2</option>
		                                    <option value="5" selected="selected">5</option>
		                                    <option value="10">10</option>
		                                    <option value="15">15</option>
		                                    <option value="20">20</option>
		                                    <option value="30">30</option>
		                                    <option value="more">50+</option>
		                                </select>
		                                <spring:message code='label.miles'/>
	                                </div>
	                            </div>
	
	                            <div class="control-group">
	                                <label class="control-label" for="hospital_medicalGroup"><spring:message code='label.type'/></label>
	                                <div class="controls">
	                                  
	                                     <select id="hospitalType" name="hospitalType">
											<option value=""><spring:message code='label.selectOption'/></option>
											<c:forEach var="hospitalTypeObj" items="${listHospitalType}">
			    								<option value="${hospitalTypeObj.key}">${hospitalTypeObj.value}</option>
											</c:forEach>
	                                	</select>
	                                
	                                   <!-- input type="text" id="hospitalMedicalGroup" name="hospitalMedicalGroup" class="input-xlarge" value="" possibleValues="$hospital.type" / -->
	                                 </div>
	                            </div>
	                        </form>
	                    </div><!-- #tab-hospitals -->
	                    <div id="tab-dentists" class="tab-pane"><!-- Dentists -->
	
	                        <form class="form-horizontal modal-form" id="frmProviderSearchDentists" name="frmProviderSearchDentists" action='<c:url value="/provider/search/dentists"/>' method="post">
							<input type="hidden" id="planId" name="planId" value="${plan.id}"/>
	                            <div class="control-group">
	                                <label class="control-label" for="dentistName"><spring:message code='label.name'/></label>
	                                <div class="controls">
	                                    <input type="text" id="dentistName" name="dentistName" class="input-xlarge" value="" />
	                                </div>
	                            </div>
	
	                            <div class="control-group">
	                                <label class="control-label" for="dentistNearZip"><spring:message code='label.zip'/>< <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />/label>
	                                <div class="controls">
	                                    <input type="text" id="dentistNearZip" name="dentistNearZip" class="input-medium" value="" />
	                                </div>
	                                <div class="controls">
									     <div id="dentistNearZip_error" class=""></div>
								    </div>
	                            </div>
	
	                            <div class="control-group">
	                                <label class="control-label" for="dentistDistance"><spring:message code='label.distance'/></label>
	                                <div class="controls">
	                                    <select id="dentistDistance" name="dentistDistance" class="input-mini">
		                                    <option value="2">2</option>
		                                    <option value="5" selected="selected">5</option>
		                                    <option value="10">10</option>
		                                    <option value="15">15</option>
		                                    <option value="20">20</option>
		                                    <option value="30">30</option>
		                                    <option value="more">50+</option>
		                                </select>
		                                <spring:message code='label.miles'/>
	                                </div>
	                            </div>
	
	                            <div class="control-group">
	                                <label class="control-label" for="dentistSpecialties"><spring:message code='label.specialty'/></label>
	                                <div class="controls">
		                                <select id="dentistSpecialties" class="input-xlarge" name="dentistSpecialties">
										<option value=""><spring:message code='label.selectOption'/></option>
											<c:forEach var="specialtyObj" items="${listDentistSpecialties}">
												<c:set var="parentSpecialtyObj" value="${specialtyObj.key}"/>
												<option value="${parentSpecialtyObj.id}" style="font-weight: bold;">${parentSpecialtyObj.name}</option>
													<c:forEach var="childSpecialtyObj" items="${specialtyObj.value}">
														<option value="${childSpecialtyObj.id}">&nbsp;&nbsp;${childSpecialtyObj.name}</option>
													</c:forEach>
											    </optgroup>  
											</c:forEach>
										</select>
	                                </div>
	                            </div>
	
	                            <div class="control-group">
	                                <label class="control-label" for="dentistGender"><spring:message code='label.gender'/></label>
	                                <div class="controls">
	                                    <select id="dentistGender" name="dentistGender" class="input-xlarge">
		                                    <option value=""><spring:message code='label.selectOption'/></option>
		                                    <c:forEach var="genderObj" items="${listGender}">
												<option value="${genderObj.key}"><spring:message code='label.gender.${genderObj.value}' javaScriptEscape='true'/></option>
											</c:forEach>
		                                </select>
	                                </div>
	                            </div>
	
	                            <div class="control-group">
	                                <label class="control-label" for="dentistLanguages"><spring:message code='label.languages'/></label>
	                                <div class="controls">
	                                    <select id="dentistLanguages" name="dentistLanguages" class="input-xlarge">
											<option value=""><spring:message code='label.selectOption'/></option>
											<c:forEach var="languageObj" items="${listLanguages}">
												<option value="${languageObj.key}">${languageObj.value}</option>
											</c:forEach>
	                                	</select>
	                                	<!-- input type="text" id="dentistLanguages" name="dentistLanguages" class="input-xlarge" value="" /-->
	                                    <br />
	                                     <spring:message code='label.alldoctorsSpeakEng'/>
	                                </div>
	                            </div>
	
	                        </form>
	
	                    </div><!-- #tab-dentists -->
	                </div><!-- .tab-content -->
	            </div><!-- .tabbable -->
   			</div>
	    </div><!-- .gutter10 -->
	
	        <div class="form-actions">
		        <div class="gutter10">
		            <a href="#" class="btn btn-primary" onclick="onSubmit();"><spring:message code='label.search'/></a>
		            <!-- input type="submit" name="submit" id="submit" class="btn btn-primary" value="Search" title="Search"/-->
	            </div>
	        </div>
    </div>
<script>

var validator1 = $("#frmProviderSearchDoctors").validate({ 
	 ignore: "",
	rules : {doctorNearZip : { required : false, 
		                       number : true,
		                       minlength : 5,
		                       maxlength : 5
	                         }
	}, 
	messages : {
		doctorNearZip : { number : "<span> <em class='excl'>!</em><spring:message  code='label.validateIsZipcodeNumber' javaScriptEscape='true'/></span>",
						  minlength : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcodeMaxlength' javaScriptEscape='true'/></span>",              
						  maxlength : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcodeMaxlength' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	} 
});

var validator2 = $("#frmProviderSearchDentists").validate({ 
	 ignore: "",
	rules : {dentistNearZip : { required : false, 
			                       number : true,
			                       minlength : 5,
			                       maxlength : 5
		                         }
		     
			 
	}, 
	messages : {
		dentistNearZip : { number : "<span> <em class='excl'>!</em><spring:message  code='label.validateIsZipcodeNumber' javaScriptEscape='true'/></span>",
						   minlength : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcodeMaxlength' javaScriptEscape='true'/></span>",               
						   maxlength : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcodeMaxlength' javaScriptEscape='true'/></span>"}
		
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	} 
});
var validator3 = $("#frmProviderSearchHospitals").validate({ 
	 ignore: "",
	rules : { hospitalNearZip : { required : false, 
				                       number : true,
				                       minlength : 5,
				                       maxlength : 5
			                         } 
			 
	}, 
	messages : {
		hospitalNearZip : { number : "<span> <em class='excl'>!</em><spring:message  code='label.validateIsZipcodeNumber' javaScriptEscape='true'/></span>",
							minlength : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcodeMaxlength' javaScriptEscape='true'/></span>",
							maxlength : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcodeMaxlength' javaScriptEscape='true'/></span>"}
		
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	} 
});

$('input[type=text]').on('keyup', function(e) {
    if (e.which == 13) {
    	onSubmit();  
    }
});

function selectTab(id) {

    if(id=='tab-hospitals'){
        $('#tab-hospitals,#hospitals').addClass('active');
        $('#tab-dentists,#dentists').removeClass('active');
        $('#tab-doctors,#doctors').removeClass('active');
    }
    else if(id=='tab-dentists'){
        $('#tab-hospitals,#hospitals').removeClass('active');
        $('#tab-dentists,#dentists').addClass('active');
        $('#tab-doctors,#doctors').removeClass('active');
    }
    else{
        $('#tab-hospitals,#hospitals').removeClass('active');
        $('#tab-dentists,#dentists').removeClass('active');
        $('#tab-doctors,#doctors').addClass('active');
    }
    return false;
}
function onSubmit(){
    if($('#tab-hospitals').hasClass('active')){
        $('#frmProviderSearchHospitals').submit();
    }
    else if($('#tab-dentists').hasClass('active')){
        $('#frmProviderSearchDentists').submit();
    }
    else if($('#tab-doctors').hasClass('active')){
        $('#frmProviderSearchDoctors').submit();
    }

}
</script>
