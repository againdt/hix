package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav.ObjectFactory;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav.PassportCountryType;
import com.getinsured.iex.hub.platform.HubServiceException;


public class ReVerifyPassportCountryWrapper implements JSONAware {
	
	private String passportNumber;
	private String countryOfIssuance;
	private PassportCountryType passportCountryType;
	
	private static final String PASSPORT_NUMBER_KEY = "PassportNumber";
	private static final String COUNTRY_KEY = "CountryOfIssuance";
	
	public ReVerifyPassportCountryWrapper(){
		ObjectFactory factory = new ObjectFactory();
		passportCountryType = factory.createPassportCountryType();
	}
	
	public void setPassportNumber(String passportNumber) throws HubServiceException{
		
		if(passportNumber == null){
			throw new HubServiceException("No Passport number provided");
		}
		
		this.passportNumber = passportNumber;
		this.passportCountryType.setPassportNumber(passportNumber);
	}
	
	public String getPassportNumber() {
		return passportNumber;
	}
	
	public void setCountryOfIssuance(String countryOfIssuance) throws HubServiceException{
		
		if(countryOfIssuance == null){
			throw new HubServiceException("No Country of Issuance provided");
		}
		
		this.countryOfIssuance = countryOfIssuance;
		this.passportCountryType.setCountryOfIssuance(countryOfIssuance);
	}
	
	public String getCountryOfIssuance() {
		return countryOfIssuance;
	}
	
	public PassportCountryType getPassportCountryType(){
		return this.passportCountryType;
	}

	
	/**
	 * Converts the JSON String to PassportCountryWrapper object
	 * 
	 * @param jsonString - String representation for PassportCountryWrapper
	 * @return PassportCountryWrapper Object
	 */
	public static ReVerifyPassportCountryWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create PassportCountryWrapper from null or empty input");
		}

		Object tmpObj = null;
		
		ReVerifyPassportCountryWrapper wrapper = new ReVerifyPassportCountryWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		tmpObj = obj.get(PASSPORT_NUMBER_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create PassportCountryWrapper, No Passport number found");
		}
		wrapper.passportNumber = (String)tmpObj;
		wrapper.passportCountryType.setPassportNumber(wrapper.passportNumber);
		
		tmpObj = obj.get(COUNTRY_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create PassportCountryWrapper, No Country of Issuance found");
		}
		wrapper.countryOfIssuance = (String)tmpObj;
		wrapper.passportCountryType.setCountryOfIssuance(wrapper.countryOfIssuance);
		
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(PASSPORT_NUMBER_KEY, this.passportNumber);
		obj.put(COUNTRY_KEY, this.countryOfIssuance);
		return obj.toJSONString();
	}
	
	/**
	 * Converts the JSON Object to PassportCountryWrapper object
	 * 
	 * @param obj - JSON representation for PassportCountryWrapper
	 * @return PassportCountryWrapper Object
	 */
	public static ReVerifyPassportCountryWrapper fromJSONObject(JSONObject obj) throws HubServiceException {
		ReVerifyPassportCountryWrapper wrapper = new ReVerifyPassportCountryWrapper();
		wrapper.setPassportNumber((String)obj.get(PASSPORT_NUMBER_KEY));
		wrapper.setCountryOfIssuance((String)obj.get(COUNTRY_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(PASSPORT_NUMBER_KEY, this.passportNumber);
		obj.put(COUNTRY_KEY, this.countryOfIssuance);
		return obj;
	}
}

