
<link rel="stylesheet" href="../resources/css/eligibility.css" type="text/css">


<form class="form-horizontal">
<div class="row-fluid" >
    <div class="row-fluid" id="titlebar">
      <div class="span3">
        <a class="btn btn-link" href="e0-sign-in">Back </a>
      </div>
      <div class="span9 pull-left">
        <h1>Start Your Application</h1>
      </div>
    </div>
    
    <!--titlebar-->
    <div class="row-fluid" id="main">
      <div class="span3 gutter10" id="sidebar">
        <ul class="nav nav-tabs nav-stacked"  data-spy="affix">
          <li class="active"> <a href="e1-1-start-app"> <i class="icon-ok"></i> Start Your Application</a></li>
          <li><a href="e3-build-your-household"> Build Your Household</a></li>
          <li><a href="e4-household-income"> Your Household Income </a></li>
          <li><a href="e5-additional-questions"> Additional Questions </a></li>
          <li><a href="e6-review-declare-file"> Review &amp; Sign </a></li>
        </ul>
      </div>
      <!--sidebar-->
          <div class="span9" id="rightpanel">
      <img src="../resources/eligibilitynm/img/household.png" alt="header image" class="img-polaroid">
        <div class="margin-right"> <!-- gutter10  -->
           
          <div class="span12">
            <h3>Before you continue, please read our Privacy Policy</h3>
            
            
            <div class="control-group">
                  <textarea rows="5" class="span12">We'll keep your information private as required by law. Your answers on this form will only be used to determine eligibility for health coverage or help paying for coverage. We'll check your answers using the information in our electronic databases and the databases of other federal agencies. If the information doesn't match, we may ask you to send us proof.

We won't ask any questions about your medical history. Household members who don't want coverage won't be asked questions about citizenship or immigration status.

IMPORTANT: As part of the application process, we may need to retrieve your information from the Internal Revenue Service (IRS), Social Security, the Department of Homeland Security, and/or a consumer reporting agency. We need this information to check your eligibility for coverage and help paying for coverage if you want it and to give you the best service possible. We may also check your information at a later time to make sure your information is up to date. We'll notify you if we find something has changed.</textarea>
                </div>
                
                <div class=" gutter10">
                  <label class="checkbox">
                    <input type="checkbox" value="" checked>
                   I agree to have my information used and retrieved from data sources for this application. I have consent for all people 
                   I'll list on the application for their information to be retrieved and used from data sources. <br/>
                   <a href="#">Learn more about your Data</a> &amp;
                   <a href="#">Privacy Act Statement</a></label>
                </div>
          </div>
          
            <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn next btn-primary" href="#application1">Accept &amp; Continue</a> 
	                  <a class="btn edit btn-primary secondary">Accept &amp; Continue</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
           

            <div id="application1" class="section">
            <div class="opacity">
              		<div class="header">
              		<h4>Household Contact</h4>
              </div>
              <p class="gutter10">If you are seeking coverage for yourself or others in your household, please enter your contact information. 
              If you are helping someone apply who is not a member of your household, please <a href="#">click here</a>. 
              If you would like to designate a broker as an authorized representative, please <a href="#">click here</a>.</p>
              
              
                
                <h4>Contact Information</h4>
                 
                <div class="control-group">
                  <label class="control-label" for="inputfName">First Name</label>
                  <div class="controls">
                    <input type="text" name="inputfName" value="Stan">
                  </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label" for="inputmName">Middle Name</label>
                  <div class="controls">
                    <input type="text" name="inputmName" value="" class="input-medium">
                  </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label" for="inputlName">Last Name</label>
                  <div class="controls">
                    <input type="text" name="inputlName" value="Jacobs">
                  </div>
                </div>
                
                <div class="control-group ">
                  <label class="control-label" for="inputSuffix">Suffix</label>
                  <div class="controls">
                    <input type="text" name="inputSuffix" class="input-small" value="Jr., Sr.">
                  </div>
                </div>
                
                
        <div class="resedentialAddress">
                <h4 class="clear">Contact Home Address</h4>
                 
                <div class="control-group">
                  <label class="control-label" for="inputStreet">Street Address</label>
                  <div class="controls">
                    <input type="text" name="inputStreet" class="input-large" value="1971 Homer Ave.">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="inputApt">Apt.</label>
                  <div class="controls">
                    <input type="text" name="inputApt" class="input-mini" value="">
                  </div>
                </div>
                

                <div class="control-group">
                  <label class="control-label" for="inputCity">City</label>
                  <div class="controls">
                    <input type="text" name="inputCity"  class="input-medium" value="Boise">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="inputState">State</label>
                  <div class="controls">
                    <select name="inputState" class="input-medium" id="inputState">
                      <option value="">Select a State</option>
                      <option value="AL">Alabama</option>
                      <option value="AK">Alaska</option>
                      <option value="AZ">Arizona</option>
                      <option value="AR">Arkansas</option>
                      <option value="CA">California</option>
                      <option value="CO">Colorado</option>
                      <option value="CT">Connecticut</option>
                      <option value="DE">Delaware</option>
                      <option value="DC">District Of Columbia</option>
                      <option value="FL">Florida</option>
                      <option value="GA">Georgia</option>
                      <option value="HI">Hawaii</option>
                      <option value="ID" selected>Idaho</option>
                      <option value="IL">Illinois</option>
                      <option value="IN">Indiana</option>
                      <option value="IA">Iowa</option>
                      <option value="KS">Kansas</option>
                      <option value="KY">Kentucky</option>
                      <option value="LA">Louisiana</option>
                      <option value="ME">Maine</option>
                      <option value="MD">Maryland</option>
                      <option value="MA">Massachusetts</option>
                      <option value="MI">Michigan</option>
                      <option value="MN">Minnesota</option>
                      <option value="MS">Mississippi</option>
                      <option value="MO">Missouri</option>
                      <option value="MT">Montana</option>
                      <option value="NE">Nebraska</option>
                      <option value="NV">Nevada</option>
                      <option value="NH">New Hampshire</option>
                      <option value="NJ">New Jersey</option>
                      <option value="NM">New Mexico</option>
                      <option value="NY">New York</option>
                      <option value="NC">North Carolina</option>
                      <option value="ND">North Dakota</option>
                      <option value="OH">Ohio</option>
                      <option value="OK">Oklahoma</option>
                      <option value="OR">Oregon</option>
                      <option value="PA">Pennsylvania</option>
                      <option value="RI">Rhode Island</option>
                      <option value="SC">South Carolina</option>
                      <option value="SD">South Dakota</option>
                      <option value="TN">Tennessee</option>
                      <option value="TX">Texas</option>
                      <option value="UT">Utah</option>
                      <option value="VT">Vermont</option>
                      <option value="VA">Virginia</option>
                      <option value="WA">Washington</option>
                      <option value="WV">West Virginia</option>
                      <option value="WI">Wisconsin</option>
                      <option value="WY">Wyoming</option>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="inputZip">Zip</label>
                  <div class="controls">
                    <input type="text" name="inputZip" class="input-small" value="83725">
                  </div>
                </div>

              </div>
                
                <div class="control-group">
                  <div class="controls">
                    <label class="checkbox">
                      <input type="checkbox">
                      No home address </label>

                  </div>
                </div>


              <div class="mailingAddress">
                <h4 class="clear">Contact Mailing Address</h4>
                <div class="control-group">
                <label class="control-label">Is your mailing address the same as your home address?</label>
                <div class="controls">
                  <label class="radio inline">
                  <input type="radio" name="coveragehelp" id="coveragehelp" value="Yes" checked>
                  Yes </label>
                  
                  <label class="radio inline">
                  <input type="radio" name="coveragehelp" id="coveragehelp" value="No">
                  No </label>
                  </div>
                </div>
              </div><!--.mailingAddress-->



                <h4 class="clear">Contact Phone</h4>
                 
                  <div class="control-group">
		              <label for="phone1" class="required control-label">Phone Number</label>
		              <div class="controls">
		                <input type="text" name="phone1" id="phone1" value="208" maxlength="3" class="area-code input-mini" onkeyup="shiftbox(this, 'phone2')" aria-required="true" aria-label="Primary contact number">
		                <input type="text" name="phone2" id="phone2" value="555" maxlength="3" class="input-mini" onkeyup="shiftbox(this, 'phone3')" aria-label="Primary contact number">
		                <input type="text" name="phone3" id="phone3" value="1212" maxlength="4" class="input-small" aria-label="Primary contact number">
		                <div id="phone3_error"></div>
		              </div>
		           </div>
         <div class="control-group">
                  <label class="control-label" for="inputEmail">Phone Type</label>
                  <div class="controls">
                  	<select class="span3">
                  		<option></option>
                  		<option selected>Home</option>
                  		<option>Cell</option>
                  		<option>Work</option>
                  	</select>
                    
                  </div>
                 </div>
                
                  <div class="control-group ">
		              <label for="phone1" class="required control-label">Second Phone Number</label>
		              <div class="controls">
		                <input type="text" name="phone1" id="phone1" value="" maxlength="3" class="area-code input-mini" onkeyup="shiftbox(this, 'phone2')" aria-required="true" aria-label="Primary contact number">
		                <input type="text" name="phone2" id="phone2" value="" maxlength="3" class="input-mini" onkeyup="shiftbox(this, 'phone3')" aria-label="Primary contact number">
		                <input type="text" name="phone3" id="phone3" value="" maxlength="4" class="input-small" aria-label="Primary contact number">
		              </div>
		            </div>
                <div class="control-group">
                  <label class="control-label" for="inputEmail">Phone Type</label>
                 <div class="controls">
                  	<select class="span3">
                  		<option selected></option>
                  		<option>Home</option>
                  		<option>Cell</option>
                  		<option>Work</option>
                  	</select>
                    
                  </div>
                </div>
             </div><!-- opacity -->
	              <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn next" href="#application2">Save</a> 
	                  <a class="btn edit secondary">Edit</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
	            </div><!-- #application2 .section -->   
                
                
                
            <div id="application2" class="section">
              <div class="opacity">
               	<div class="header">
              			<h4>Contact Preferences</h4>
              		</div>
                 <div class="gutter10">
                <div class="control-group">
                  <label class="control-label" for="contactpref">Preferred spoken language:</label>
                  <div class="controls">
                    <select name="contactpref" class="span7" id="contactpref">
                      <option value="English" selected>English</option>
                      <option value="Spanish">Spanish</option>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="contactpref">Preferred written language:</label>
                  <div class="controls">
                    <select name="contactpref" class="span7" id="contactpref">
                      <option value="English" selected>English</option>
                      <option value="Spanish">Spanish</option>
                    </select>
                  </div>
                </div>
                </div>
                
              <h5>We need to know the best way to contact you about this application and your health 
                  coverage if you're eligible. Do you want to read your notices about your application on your electronic "My account" on this 
                  website?</h5>
               
                  <div class="control-group">
                  <div class="controls">
                    <label class="checkbox">
                      <input class="flip" type="checkbox" checked>
                      Yes, I want to read my notices online. </label>
                    <label class="checkbox">
                      <input type="checkbox">
                      No, I want to get paper notices sent to me in the mail. </label>
                  </div>
                  </div>
                <h5>You'll be contacted when a notice is ready for you on this website. How can we contact you?</h5>
                <div class="control-group">
                  <div class="controls">
                   <label class="checkbox inline">
                   <input type="checkbox">
                   Text
	                    <input type="text" name="phone1" id="phone1" value="208" maxlength="3" class="area-code input-mini" onkeyup="shiftbox(this, 'phone2')" aria-required="true" aria-label="Phone NumberSecond Phone NumberSocial Security Number" role="">
	             		<input type="text" name="phone2" id="phone2" value="555" maxlength="3" class="input-mini" onkeyup="shiftbox(this, 'phone3')" aria-label="phone2" role="">
	              		<input type="text" name="phone3" id="phone3" value="1212" maxlength="4" class="input-mini" aria-label="phone3" role=""> 
	              		 </label> 
	              		 
	              Messaging rates will apply 
               
            	</div>
                </div>
                <div class="control-group">
                  <div class="controls">
                   <label class="checkbox ">
                  <input type="checkbox" checked>
                    Email to  
                    	<input type="text" value="stan.jacobs@gmail.com" class="input-medium" >
                    	</label>
               
            	</div>
                </div>
                  
                
                <!-- <div class="span9">
                  <label class="control-label" for="inputAlerts">Receive alerts electronically?</label>
                  <div class="controls">
                     <label class="checkbox">
                    <input type="checkbox" id="inputAlerts">
                      via text messaging <strong>(415) 867-5309</strong> </label>
                    <label class="checkbox">
                      <input type="checkbox" name="inputAlerts">
                      via email at  <strong>stan.jacobs@gmail.com</strong> </label>
                  </div>
                </div> -->
                <h5 class="clear">Authorized Representative</h5>
                 <h5>Do you want to name someone as your authorized representative?</h5>
                <div class="control-group">
                    <label class="checkbox">
                      <input class="flip" type="checkbox" >
                      Yes</label>
                    <label class="checkbox">
                      <input type="checkbox" checked>
                      No </label>
                </div>
                
                <h5 class="clear">Assistance with Completing the Application</h5>
                <h5>Is anyone helping you with this application?</h5>
                <div class="control-group">
                    <label class="checkbox">
                      <input class="flip" type="checkbox" >
                      Yes</label>
                    <label class="checkbox">
                      <input type="checkbox" checked>
                      No </label>
                </div>
              </div><!-- opacity -->
	              <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn btn-primary" href="e3-build-your-household">Save &amp; Continue</a> 
	                  <a class="btn edit secondary">Edit</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
	            </div><!-- #household .section -->
              
            
             
           </div>
    <!--gutter10-->
  </div>
  <!--rightpanel-->
          
          
</div>


<!-- #modal -->

          </div>

   <!--container-->
  <!--main-->

 </form>

<!-- Modal -->

        <script type="text/javascript">

          function replaceModalLabel(){
            document.getElementById('myModalLabel').innerHTML='Would you like help paying for your health coverage?';
          }

          function loadincomediv(){
            document.getElementById('incomelevel').style.display="none";
            fplcalculator();

          }

          function fplcalculator() {

                      var familysize = parseInt(document.getElementsByName('familySize')[0].value);
                              document.getElementById('incomelevel').style.display="";
                              var familyIncome = ((familysize-8)*3960+38890)*4;
                              document.getElementById('fplCalculatedIncome').innerHTML="$"+familyIncome.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                          }
                 function populateResultScreen(){
                        document.getElementById('myModalLabel').innerHTML='Your Pre-Screening Results';
                            if(document.getElementsByName('incomelevelInput')[0].checked==true){
                      document.getElementById('outcomeHeading').innerHTML='Good news';
                      document.getElementById('outcomeText').innerHTML='Based on what you have told us so far, you may be eligible for health programs or tax credits to make your health coverage more affordable. However, for an accurate determination of your eligibility, you will need to apply.';
                            }
                            else if(document.getElementsByName('incomelevelInput')[1].checked==true){
                      document.getElementById('outcomeHeading').innerHTML='Bad news';
                      document.getElementById('outcomeText').innerHTML='Based on what you told us so far, you are likely not eligible for health programs or tax credits to make your health coverage more affordable. However, you can still use the Mississippi Exchange to buy commercial health insurance coverage, which is competitively priced and affordable.';
                            }
                            if(document.getElementsByName('chipButton')[0].checked==true){
                      document.getElementById('medicaidCheck').innerHTML='By law, any members of your household who are currently enrolled in Medicaid or CHIP cannot seek premium tax credits or cost sharing subsidies for commercial plans on the Mississippi Exchange.';
                            } else if(document.getElementsByName('chipButton')[1].checked==true){
                              document.getElementById('medicaidCheck').innerHTML='';
                            }
                          }
                  <!--validations-->


                          function validateflow(){
                    document.getElementById("familySize_error").innerHTML="";

                    var a = document.getElementById('checkFamilySize');
                    var familysizeString = document.getElementsByName('familySize')[0].value;
                    var familysizeInt = parseInt(document.getElementsByName('familySize')[0].value);
                    if(familysizeInt<1 || familysizeInt>99 || isNaN(familysizeInt) ){
                      a.href="";
                      document.getElementById("familySize_error").innerHTML="<span> <em class='excl'>!</em>Please enter a valid number.</span>";

                    }else{
                      document.getElementById("familySize_error").innerHTML="";

                    a.href = "#chip";
                    }
                  }

                  function disablehref(){
                    var a = document.getElementById('checkFamilySize');
                    a.href="";
                  }

                  function numbersOnly(evt) {
                    var theEvent = evt || window.event;
                    var key = theEvent.keyCode || theEvent.which;
                    key = String.fromCharCode( key );
                    var regex = /[0-9]|\./;
                    if( !regex.test(key) ) {
                      theEvent.returnValue = false;
                      if(theEvent.preventDefault) theEvent.preventDefault();
                    }
                  }
                  <!--validations>-->

           </script>



<script type="text/javascript" src="../resources/eligibilitynm/js/ghixcustom.js"></script>

<script>
    $(document).ready(function () {
        $(".flip").click(function () {
            $(".mailingAddress").fadeToggle();
        });
        $('input, textarea').placeholder();
    });
    $(".next").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        var hash = $(this.hash);
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(hash).closest('.section').show();
        $(currentsection).next().addClass('activediv');
        $(hash).prev('div').removeClass('activediv');
        $('html,body').delay(300).animate({
            scrollTop: $(this.hash).offset().top - 100
        }, 500)
        $(hash).delay(700).prev('div').find('.edit').show();
        $(hash).delay(700).prev('div').find('.next').hide();
    });
    $(".edit").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 1.0
        });
        $(currentsection).find('.cancel, .save').show();
        $('.activediv').animate({
            opacity: 0.45
        });
        $(currentsection).find('.edit').hide();
    });
    $(".save").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(currentsection).find('.cancel, .save').hide();
        $(currentsection).find('.edit').show();
        $('.activediv').animate({
            opacity: 1
        });
        $('html,body').delay(300).animate({
            scrollTop: $(currentsection).nextAll('.activediv').offset().top - 100
        }, 500)
    });
    $(".cancel").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(currentsection).find('.cancel, .save').hide();
        $(currentsection).find('.edit').show();
        $('.activediv').animate({
            opacity: 1
        });
        $('html,body').delay(300).animate({
            scrollTop: $('div').nextAll('.activediv').offset().top - 100
        }, 500)
    });
</script>

<!-- <script>
$(document).ready(function() {
 $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top-80
            }, 1000);
            return false;
        }
    }
});
 
});
</script> -->

<script>
$(document).ready(function(){
$('#sidebar').affix();
  $(".flip").click(function(){
    $(".mailingAddress").fadeToggle();
  });

/*   $('a.next').click(function(e) {
  e.preventDefault();
  //$("div.section").next('.section').height();
   var currentHeight = $("div.section").height();
   var height = $("div.section").next('div.section').height();
   console.log(height);
   var currentHeight =+height
  $('html, body, .section').animate({scrollTop: currentHeight}, 500);
 return false;
	});

	$('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top-80
            }, 1000);
            return false;
        }
    }
});
 */
  $('input, textarea').placeholder();

});


</script>


