package com.getinsured.hix.tkm.activiti;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public  final class  Main {
	
	private Main(){
		
	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext(
				new String[] { "applicationContext.xml", "activiti.cfg.xml" });
		ProcessEngine processEngine = (ProcessEngine) appContext
				.getBean("processEngine");
		
		HistoryService historyService = processEngine.getHistoryService();
		RuntimeService runtimeService = (RuntimeService) appContext
				.getBean("runtimeService");


		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("requesterName", "Kermit");
		variables.put("ticketStatus", "New");
		variables.put("groupName", "Triage Queue");
		
		ProcessInstance processInstance = runtimeService
				.startProcessInstanceByKey("AppealRequest", variables);
		
		
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = taskService.createTaskQuery().taskAssignee("Kermit").list();
		for (Task task : tasks) {
			LOGGER.info("Task for Kermit: " + task.getName());
            // Complete the task
			Map<String, Object> taskVariables = new HashMap<String, Object>();
			taskVariables.put("numberOfDays", Integer.valueOf("4"));
			taskVariables.put("vacationMotivation", "I'm really tired!");
			taskService.complete(task.getId(), taskVariables);
		}
		
		IdentityService identityService = processEngine.getIdentityService();
		List<Group> group = identityService.createGroupQuery()
				.groupName("Triage Queue").list();

		tasks = taskService.createTaskQuery()
				.taskCandidateGroup("Triage Queue").list();

		List<User> userlist = identityService.createUserQuery()
				.memberOfGroup((group.get(0).getName())).list();
		for (Task task : tasks) {
			LOGGER.info("Following task is available for management group: "
					+ task.getName());
			if (!userlist.isEmpty()) {
				User user = userlist.get(0);
				taskService.claim(task.getId(), user.getId() + "");
			}
			// claim it
		}

		// Verify Fozzie can now retrieve the task
		if (!userlist.isEmpty()) {
			User user = userlist.get(0);
			tasks = taskService.createTaskQuery()
					.taskAssignee(user.getId() + "").list();
		}

		for (Task task : tasks) {
			LOGGER.info("Task for fozzie: " + task.getName());

			// Complete the task
			Map<String, Object> taskVariables = new HashMap<String, Object>();
			taskVariables.put("ticketStatus", "NeedInfo");
			taskVariables.put("managerMotivation", "We have a tight deadline!");
			taskService.complete(task.getId(), taskVariables);
			
		}

		// Verify Fozzie can now retrieve the task
		tasks = taskService.createTaskQuery().taskAssignee("Kermit").list();
		for (Task task : tasks) {
			LOGGER.info("Task for Kermit: " + task.getName());

			// Complete the task
			Map<String, Object> taskVariables = new HashMap<String, Object>();
			taskVariables.put("moreinfo", "testt");
			//taskVariables.put("resendRequest", "No");
			taskVariables.put("sendinfo", "No");
			
			taskService.complete(task.getId(), taskVariables);
		}
		// verify that the process is actually finished
		HistoricProcessInstance historicProcessInstance = historyService
				.createHistoricProcessInstanceQuery()
				.processInstanceId(processInstance.getId()).singleResult();
		LOGGER.info("Process instance end time: "
				+ historicProcessInstance.getEndTime());
		
	}

}
