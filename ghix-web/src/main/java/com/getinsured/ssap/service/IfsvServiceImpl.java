package com.getinsured.ssap.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.getinsured.hix.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.ssap.model.financial.*;
import com.getinsured.ssap.model.hub.vlp.ApplicantVerificationStatus;
import com.getinsured.ssap.model.hub.vlp.response.ResponseCode;
import com.getinsured.ssap.model.hub.vlp.response.ResponseMetadata;
import com.getinsured.ssap.util.BloodRelationshipCode;
import com.getinsured.ssap.util.HouseholdUtil;
import com.getinsured.ssap.util.JsonUtil;

/**
 * Implementation of {@link IfsvService}.
 *
 * @author Yevgen Golubenko
 * @since 6/11/19
 */
@Service("ifsvService")
public class IfsvServiceImpl implements IfsvService {
  private static final Logger log = LoggerFactory.getLogger(IfsvServiceImpl.class);
  private final SsapApplicationRepository ssapApplicationRepository;
  private final SsapApplicantRepository ssapApplicantRepository;
  private final GhixRestTemplate restTemplate;
  private final Properties configProp;

  private static String hubIntegrationUrl;
  private static final ObjectMapper om = new ObjectMapper();

  @Autowired
  public IfsvServiceImpl(final SsapApplicationRepository ssapApplicationRepository,
                         final SsapApplicantRepository ssapApplicantRepository,
                         final GhixRestTemplate restTemplate,
                         final Properties configProp) {
    this.ssapApplicationRepository = ssapApplicationRepository;
    this.ssapApplicantRepository = ssapApplicantRepository;
    this.restTemplate = restTemplate;
    this.configProp = configProp;
  }

  @PostConstruct
  public void postConstruct() {
    hubIntegrationUrl = this.configProp.getProperty("ghixHubIntegrationURL");

    if(hubIntegrationUrl == null || "".equals(hubIntegrationUrl)) {
      log.error("Unable to get AWS Hub integration URL from configuration.properties, using default");
      hubIntegrationUrl = "https://7ntk685owc.execute-api.us-gov-west-1.amazonaws.com/nv/ghix/";
    }

    // Remove last slash from hub integration URL.
    if(hubIntegrationUrl.endsWith("/")) {
      log.info("Removing last slash from hub integration url: {}", hubIntegrationUrl);
      hubIntegrationUrl = hubIntegrationUrl.substring(0, hubIntegrationUrl.length() - 1);
      log.info("After removing last slash from hub integration url: {}", hubIntegrationUrl);
    }

    hubIntegrationUrl += "/invokeIFSV";
    om.enable(SerializationFeature.INDENT_OUTPUT);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public IfsvResponse invoke(final long ssapApplicationId) {
    log.info("Invoking IFSV service for application id: {}", ssapApplicationId);
    assert ssapApplicationId > 0 : "ssapApplicationId should be > 0";
    final SsapApplication ssapApplication = ssapApplicationRepository.getOne(ssapApplicationId);

    if(ssapApplication == null) {
      log.error("Unable to find SSAP by given application id: {}", ssapApplicationId);
      return null;
    } else {
      if(ssapApplication.getFinancialAssistanceFlag() == null || ssapApplication.getFinancialAssistanceFlag().equalsIgnoreCase("N")) {
        IfsvResponse ifsvResponse = new IfsvResponse();
        IfsvErrorMessageDetail errorMessageDetail = new IfsvErrorMessageDetail();
        ResponseMetadata responseMetadata = new ResponseMetadata();
        responseMetadata.setResponseCode(ResponseCode.EXCEPTION);
        responseMetadata.setResponseDescriptionText(String.format("This SSAP [%d] doesn't have financial assistance flag set to Y: %s, skipping IFSV call.", ssapApplication.getId(), ssapApplication.getFinancialAssistanceFlag()));
        errorMessageDetail.setResponseMetadata(responseMetadata);

        List<IfsvErrorMessageDetail> errors = new ArrayList<>(1);
        errors.add(errorMessageDetail);
        ifsvResponse.setErrorMessageDetail(errors);

        return ifsvResponse;
      }

      log.info("Invoking IFSV for given application id: {}", ssapApplicationId);
    }

    final IfsvRequest ifsvRequest = getRequestForApplication(ssapApplication);

    if(ifsvRequest == null ||CollectionUtils.isEmpty(ifsvRequest.getSsnDob())) {
        IfsvResponse ifsvResponse = new IfsvResponse();
        IfsvErrorMessageDetail errorMessageDetail = new IfsvErrorMessageDetail();
        ResponseMetadata responseMetadata = new ResponseMetadata();
        responseMetadata.setResponseCode(ResponseCode.EXCEPTION);
        responseMetadata.setResponseDescriptionText(String.format("This SSAP [%d] doesn't have  any members  eligible for IFSV verification , skipping IFSV call.", ssapApplication.getId()));
        errorMessageDetail.setResponseMetadata(responseMetadata);

        List<IfsvErrorMessageDetail> errors = new ArrayList<>(1);
        errors.add(errorMessageDetail);
        ifsvResponse.setErrorMessageDetail(errors);

        return ifsvResponse;
      }
    try {
		log.info("Sending IFSV request for URL: {}, Request {}", hubIntegrationUrl, om.writeValueAsString(ifsvRequest));
	  } catch (JsonProcessingException e1) {
      log.info("unable to parse ifsvRequest "+ifsvRequest);
	  }

    IfsvResponse ifsvResponse = null;

    try {
      ifsvResponse = restTemplate.postForObject(hubIntegrationUrl, ifsvRequest, IfsvResponse.class);
      log.info("Got IFSV response: {}", ifsvResponse);
      if(ifsvResponse == null) {
        throw new Exception("IFSV Response was null");
      }
    }
    catch(HttpClientErrorException e) {
      log.error("HTTP Client exception occurred", e);
      //log.error("Exception details", e);
      ifsvResponse = new IfsvResponse();
      ifsvResponse.setErrorMessageDetail(getIfsvErrorDetailsFromException(e));
      ifsvResponse.setIfsvVerification(IfsvVerification.ERROR);
    }
    catch(HttpServerErrorException se) {
      log.error("HTTP Server exception occurred", se);
      //log.error("Exception details", se);
      ifsvResponse = new IfsvResponse();
      ifsvResponse.setErrorMessageDetail(getIfsvErrorDetailsFromException(se));
      ifsvResponse.setIfsvVerification(IfsvVerification.ERROR);
    }
    catch(Exception ex) {
      log.error("REST Template exception occurred", ex);
      //log.error("Exception details", ex);
      ifsvResponse = new IfsvResponse();
      ifsvResponse.setErrorMessageDetail(getIfsvErrorDetailsFromException(ex));
      ifsvResponse.setIfsvVerification(IfsvVerification.ERROR);
    }

    try {
      final HouseholdMember primaryTaxFiler = HouseholdUtil.getPrimaryTaxHouseholdMember(JsonUtil.parseApplicationDataJson(ssapApplication).getTaxHousehold());
      SsapApplicant applicant=ssapApplicantRepository.findByApplicantGuidAndAppId(primaryTaxFiler.getApplicantGuid(),ssapApplication.getId());
      if (ifsvResponse.getIfsvVerification() == IfsvVerification.VERIFIED) {
          applicant.setIncomeVerificationStatus(ApplicantVerificationStatus.VERIFIED.getValue());
          applicant.setIncomeVerificationVid(new BigDecimal(ifsvRequest.getPayload().getRequestId()));
      } else if(ifsvResponse.getIfsvVerification() == IfsvVerification.NOT_VERIFIED) {
          applicant.setIncomeVerificationStatus(ApplicantVerificationStatus.NOT_VERIFIED.getValue());
          applicant.setIncomeVerificationVid(new BigDecimal(ifsvRequest.getPayload().getRequestId()));
      } else {
          applicant.setIncomeVerificationStatus(ApplicantVerificationStatus.PENDING.getValue());
          applicant.setIncomeVerificationVid(new BigDecimal(ifsvRequest.getPayload().getRequestId()));
      }
      log.info("IFSV (SSAP: {}) updating primary applicant verification status to: {}", ssapApplicationId, applicant.getIncomeVerificationStatus());
      ssapApplicantRepository.save(applicant);
    }
    catch (Exception e) {
      log.error("Unable to update applicant income verification status {}", e.getMessage());
      // log.error("Exception details", e);
      ifsvResponse = new IfsvResponse();
      ifsvResponse.setErrorMessageDetail(getIfsvErrorDetailsFromException(e));
      ifsvResponse.setIfsvVerification(IfsvVerification.ERROR);
    }

    return ifsvResponse;
  }

  /**
   * Constructs {@link IfsvRequest} object from given {@link SsapApplication} object.
   *
   * @param ssapApplication {@link SsapApplication} object to construct request from.
   * @return Constructed {@link IfsvRequest} object that is ready to be send to IFSV service.
   */
  public IfsvRequest getRequestForApplication(final SsapApplication ssapApplication) {
    final SingleStreamlinedApplication singleStreamlinedApplication = JsonUtil.parseApplicationDataJson(ssapApplication);
    final IfsvRequest ifsvRequest = new IfsvRequest();
    ifsvRequest.setApplicationId(ssapApplication.getId());

    List<Map<String, String>> dobSsnList = new ArrayList<>();

    if (singleStreamlinedApplication != null) {
      ifsvRequest.setClientIp(singleStreamlinedApplication.getClientIp());

      List<IfsvHouseholdMember> ifsvTaxHouseholds = new LinkedList<>();

      assert singleStreamlinedApplication.getTaxHousehold() != null &&
          singleStreamlinedApplication.getTaxHousehold().size() > 0 : "SingleStreamlinedApplication doesn't have any TaxHousehold members";

      assert !singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember().isEmpty() :
          "There is no household members in given SingleStreamlinedApplication";

      if(!ifsvVerificationRequired(singleStreamlinedApplication))
      {
    	  log.info("Skip IFSV verification : No APTC eligible member(s)/ Member(s) has chosen other coverage options");
    	  ifsvRequest.setSsnDob(dobSsnList);
    	  return ifsvRequest;

      }

      final List<HouseholdMember> householdMembers = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
      final HouseholdMember primaryHouseholdMember = HouseholdUtil.getPrimaryTaxHouseholdMember(singleStreamlinedApplication.getTaxHousehold());
      final List<BloodRelationship> allBloodRelationships = HouseholdUtil.getAllBloodRelationships(householdMembers);

      householdMembers.forEach(hhm -> {

        if(hhm == null) {
          log.error("Household member is null");
        }

        assert hhm != null : "Household member is null";

        Map<String, String> ssnDob = new HashMap<>();

        IfsvHouseholdMember member = new IfsvHouseholdMember();
        member.setPersonId(hhm.getPersonId());

        if (hhm.getName() != null) {
          if(hhm.getName().getMiddleName() != null && !hhm.getName().getMiddleName().equals("")) {
            member.setName(new Name(hhm.getName().getFirstName(), hhm.getName().getMiddleName(), hhm.getName().getLastName()));
          }
          else {
            member.setName(new Name(hhm.getName().getFirstName(),  hhm.getName().getLastName()));
          }
        }

        if (hhm.getSocialSecurityCard() != null && hhm.getSocialSecurityCard().getSocialSecurityNumber() != null) {
          member.setSocialSecurityCard(new SocialSecurityCard(hhm.getSocialSecurityCard().getSocialSecurityNumber()));

          if(hhm.getDateOfBirth() != null) {
            ssnDob.put(member.getSocialSecurityCard().getSocialSecurityNumber(),
                DateUtil.dateToString(hhm.getDateOfBirth(), "yyyy-MM-dd"));
            dobSsnList.add(ssnDob);
          }
        } else {
          log.warn("Can't set IFSV household member's social security card from application household because it's null/blank");
        }

        member.setTaxFilerCategory(getTaxFilerCategory(allBloodRelationships, primaryHouseholdMember, hhm));
        ifsvTaxHouseholds.add(member);
      });

      IfsvPayload payload = new IfsvPayload();
      payload.setRequestId(getRandomRequestId());

      List<IfsvTaxHousehold> taxHouseholds = new ArrayList<>();
      IfsvTaxHousehold taxHousehold = new IfsvTaxHousehold();
      taxHousehold.setHouseholdMember(ifsvTaxHouseholds);
      taxHouseholds.add(taxHousehold);
      payload.setTaxHousehold(taxHouseholds);

      //IFSV needs "Total Household Income" not "Total MAGI income"
      ifsvRequest.setTotalHouseholdIncome(HouseholdIncomeCalculator.calculateTotalHouseholdIncome(householdMembers));
      ifsvRequest.setSsnDob(dobSsnList);
      ifsvRequest.setPayload(payload);
    } else {
      log.error("Cannot construct IFSV Request. Given SsapApplication doesn't contain valid SingleStreamlinedApplication in it's data column");
    }

    return ifsvRequest;
  }

  private String getRandomRequestId() {
    // requestID should be within the length range of [1-9]
    int leftLimit = 1;
    int rightLimit = 999_999_999;
    int generatedInteger = leftLimit + (int) (new Random().nextFloat() * (rightLimit - leftLimit));
    return String.valueOf(generatedInteger);
  }

  /**
   * Returns {@link TaxFilerCategory} for the given household member. Tax filer category based
   * on the blood relationship of the given member to the primary tax filer.
   *
   * @param bloodRelationshipList blood relationships list for SSAP.
   * @param primaryHouseholdMember primary household member.
   * @param householdMember household member for which we need tax filer category.
   * @return {@link TaxFilerCategory}.
   */
  TaxFilerCategory getTaxFilerCategory(final List<BloodRelationship> bloodRelationshipList, final HouseholdMember primaryHouseholdMember, final HouseholdMember householdMember) {

    Optional<BloodRelationship> bloodRelationshipOptional =
        bloodRelationshipList.stream()
            .filter(br -> br != null && br.getRelatedPersonId() != null &&
                br.getIndividualPersonId().equals(String.valueOf(householdMember.getPersonId())) &&
                    br.getRelatedPersonId().equals(String.valueOf(primaryHouseholdMember.getPersonId())))
            .findFirst();

    if (bloodRelationshipOptional.isPresent()) {
      BloodRelationshipCode code = BloodRelationshipCode.getBloodRelationForCode(bloodRelationshipOptional.get().getRelation());
      if (code == BloodRelationshipCode.SELF) {
        return TaxFilerCategory.PRIMARY;
      } else if (code == BloodRelationshipCode.SPOUSE) {
        return TaxFilerCategory.SPOUSE;
      } else {
        return TaxFilerCategory.DEPENDENT;
      }
    }

    log.error("Unable to determinate TaxFilerCategory from given blood relationship list ({} relationships) for given household member: applicant guid: {}, person id: {}, " +
      "primary applicant guid: {}, primary person id: {} returning TaxFilerCategory.UNKNOWN",
      bloodRelationshipList.size(),
      householdMember.getApplicantGuid(),
      householdMember.getPersonId(),
      primaryHouseholdMember.getApplicantGuid(),
      primaryHouseholdMember.getPersonId()
    );

    return TaxFilerCategory.UNKNOWN;
  }

  /**
   * Parses exception and returns list of {@link IfsvErrorMessageDetail}.
   * @param exception exception.
   * @return list of {@link IfsvErrorMessageDetail}.
   */
  public static List<IfsvErrorMessageDetail> getIfsvErrorDetailsFromException(Exception exception) {
    IfsvErrorMessageDetail errorMessageDetail = new IfsvErrorMessageDetail();
    ResponseMetadata responseMetadata = new ResponseMetadata();

    if(exception instanceof HttpServerErrorException) {
      HttpServerErrorException serverErrorException = (HttpServerErrorException) exception;
      responseMetadata.setResponseDescriptionText(serverErrorException.getMessage());
      if(serverErrorException.getStatusCode().is5xxServerError()) {
        responseMetadata.setResponseCode(ResponseCode.COMMUNICATION_ERROR);
      } else {
        responseMetadata.setResponseCode(ResponseCode.EXCEPTION);
      }
    }
    else {
      responseMetadata.setResponseCode(ResponseCode.EXCEPTION);
      responseMetadata.setResponseDescriptionText(exception.getMessage());
    }

    errorMessageDetail.setResponseMetadata(responseMetadata);

    return Collections.singletonList(errorMessageDetail);
  }

  /**
   * IFSV verification is only required when at least one APTC eligible member with seeking for coverage set
   *
   * IFSV verification is not required when the consumer chooses to SHOP with out APTC
   *
   */
  private boolean ifsvVerificationRequired(final SingleStreamlinedApplication application)
  {
    List<HouseholdMember> primaryHouseholdMembers = HouseholdUtil.getPrimaryTaxHouseholdMembers(application);

    return primaryHouseholdMembers.stream().anyMatch(hhm ->
      hhm.getApplyingForCoverageIndicator() != null &&
        hhm.getApplyingForCoverageIndicator() &&
        hhm.isSeeksQhp() &&
        hhm.getEligibilityResponse() != null &&
        hhm.getEligibilityResponse().isAptcEligible()
    );
  }
}
