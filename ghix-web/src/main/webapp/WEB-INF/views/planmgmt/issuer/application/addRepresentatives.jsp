<!-- 
 @author raja
 newIssuerAddRepresentatives.jsp is the requirement of the new Issuer Portal
 @UserStory : HIX-1810
 @Jira-id : HIX-2451
 -->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<div class="row-fluid">
    <div class="gutter10">
    	<ul class="page-breadcrumb">
        	<li><a href="<c:url value="/planmgmt/issuer/landing/info"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
            <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.newIssuerApplication"/></a></li>
            <li><spring:message  code="pgheader.step"/> 1</li>
        </ul><!--page-breadcrumb ends-->
    	<h1><issuerLogo:view issuerName="" /><spring:message  code="pgheader.newIssuerApplication"/></h1>
    </div>
</div>
	<div class="row-fluid">
		<div class="gutter10">
		<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.steps"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ol class="nav nav-list">
                    <li class="active"><spring:message  code="pgheader.addRepresentative"/></li>
                    <li><a href="#"><spring:message  code="pgheader.issuerApplication"/></a></li>
                    <li><a href="#"><spring:message  code="pgheader.financialInformation"/></a></li>
                    <li><a href="#"><spring:message  code="pgheader.updateProfile"/></a></li>
                    <li><a href="#"><spring:message  code="pgheader.signApplication"/></a></li>
                </ol>
                <!-- end of side bar -->
		</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="span9"><spring:message  code="pgheader.step"/> 1: <spring:message  code="pgheader.addRepresentative"/></h4>
                </div>
				<div class="gutter10">
					<p><spring:message  code="pgheader.addRepresentativeText"/></p>
			
				<form class="form-horizontal" id="frmIssuerAddRep" name="frmIssuerAddRep" action="<c:url value="/planmgmt/issuer/representatives/save" />" method="POST">
					<df:csrfToken/>
					<input type="hidden" name="returnTo" id="returnTo" value="/planmgmt/issuer/representatives/add">
						<div class="control-group">
							<label class="control-label"><spring:message  code="label.firstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
						      <input type="text" name="firstName" id="firstName" value="${repUser.firstName}" class="input-large">
						      	<c:if test="${isIssuerRepDuplicate == 'true'}">
							  	<div class="error">
                                       <label class="error"><span><spring:message  code="err.duplicateRepresentative"/></span></label>
                              	</div> 
                              	</c:if>
						      <div id="firstName_error" class=""></div>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><spring:message  code="label.lastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls"><input type="text" name="lastName" id="lastName" value="${repUser.lastName}" class="input-large">
								<div id="lastName_error" class=""></div>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label"><spring:message  code="label.title"/> <!-- <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> --></label>
							<div class="controls"><input type="text" name="title" id="title" value="${repUser.title}" class="input-large">	
								<div id="title_error" class=""></div>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label"><spring:message  code="label.phoneNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls"><input type="text" name="phone" id="phone" value="${repUser.phone}" class="input-large">
							    <div id="phone_error" class=""></div>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"><spring:message  code="label.emailAddress"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="email" id="email" value="${repUser.email}" class="input-large">
							    <div id="email_error" class=""></div>
							     <c:if test="${repDuplicateEmail == 'true'}">
                                   <div class="error">
                                      <label class='error'><span><em class='excl'>!</em><spring:message code='err.duplicateEmail' javaScriptEscape='true'/></span></label>
                                   </div> 
                                 </c:if>
							</div>
						</div>				    
						<%-- <c:if test="${IS_CA_CALL != null && IS_CA_CALL != 'true'}">	 --%>
						<c:if test="${STATE_CODE != null && STATE_CODE != 'CA'}">	
						<div class="control-group">
							<label class="control-label"><spring:message  code="label.password"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls"><input type="password" name="password" id="password" class="input-large">
								<div id="password_error" class=""></div>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label"><spring:message  code="label.confirmPassword"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls"><input type="password" name="confirmPassword" id="confirmPassword" class="input-large">
							    <div id="confirmPassword_error" class=""></div>
							</div>
						</div>
						</c:if>
					
                    <div class="form-actions">
                    	<a href='<c:url value="/planmgmt/issuer/landing/info"/>' class="btn"><spring:message  code="label.back"/></a>
                        <input type="submit" name="IssuerAddRepSubmitBtn" id="IssuerAddRepSubmitBtn" class="btn btn-primary" value="<spring:message  code='label.next'/>" title="<spring:message  code='label.next'/>">
                    </div>
				</form>

			</div>
   			
		</div><!--  end of span9 -->
		</div>
	</div><!-- end row-fluid -->
        <c:if test="${isIssuerRepAdded == 'true'}">
			<div id="addRepresentative" class="modal hide fade">
				<div class="modal-header">
					<a class="close" data-dismiss="modal" data-original-title="">x</a>
								<h3 id="myModalLabel"><spring:message  code="pgheader.addMoreRepresentatives"/></h3>
				</div>
				<div class="modal-body">
								<p><spring:message  code="pgheader.doYouWantMoreRepresentatives"/></p>
				</div>
				<div class="modal-footer">
					<a href='<c:url value="/planmgmt/issuer/application"/>' class="btn"><spring:message  code="label.no"/></a>
					<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message  code="label.yes"/></a>
			   	</div>
			</div>
	        <script type="text/javascript">
		       $('#addRepresentative').modal("show");
			</script>	 
        </c:if>

<script type="text/javascript">
$(document).ready(function() {
    $.ajaxSetup({ cache: false }); // prevent Ajax caching in JQuery
    $("#email").keyup(function(){
    	$('#email_error').html("");
    });
    $("#email").focusout(function(){
    	if($("#email").val() != ""){  // if email address is not empty
    		$('#IssuerAddRepSubmitBtn').attr("disabled", false);
    		$('#frmIssuerAddRep').ajaxSubmit({
				type : "POST",
				url: "<c:url value='/planmgmt/checkDuplicate'/>",
				data: { "userEmailId": $("#email").val()},
				success: function(responseText){
					if(responseText){
						$('#email_error').html("<label class='error'><span><em class='excl'>!</em><spring:message code='err.duplicateEmail' javaScriptEscape='true'/></span></label>");
						//$('#IssuerAddRepSubmitBtn').attr("disabled", true);
					} 
		       	}
			});
		}	
	  return false; 
	});	 
});

var validator = $("#frmIssuerAddRep").validate({ 
	rules : {
		firstName : {required : true},
		lastName : {required : true},
		phone : { required : true, number : true, minlength : 10, maxlength : 10},
		email : { required : true, emailIdCheck: true},
		password : { required : true, minlength:8},		
		confirmPassword : {equalTo: "#password"}
	},
	messages : {
		firstName : { required : "<span><em class='excl'>!</em><spring:message  code='err.firstName' javaScriptEscape='true'/></span>"},
		lastName: { required : "<span><em class='excl'>!</em><spring:message  code='err.lastName' javaScriptEscape='true'/></span>"},
		phone : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
			            number : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>",
			            minlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneMinMaxlength' javaScriptEscape='true'/></span>",
			            maxlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneMinMaxlength' javaScriptEscape='true'/></span>"
			          },
		email : { required : "<span><em class='excl'>!</em><spring:message  code='err.email' javaScriptEscape='true'/></span>",
				  emailIdCheck : "<span><em class='excl'>!</em><spring:message  code='err.invalidEmail' javaScriptEscape='true'/></span>"},
		password : { required : "<span><em class='excl'>!</em><spring:message  code='err.password' javaScriptEscape='true'/></span>"},		
		confirmPassword : {required : "<span><em class='excl'>!</em><spring:message  code='err.confirmPassword' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error');
	} 
});

$.validator.addMethod("emailIdCheck", function(value, element) {
	//var emailRegEx = new RegExp("^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$");
	var emailRegEx = new RegExp("^[_A-Za-z0-9\u002e_]+([A-Za-z0-9\u002e]+)@[A-Za-z0-9]+([\u002e][a-zA-Z]{2,})+$");
	if("" != value && !emailRegEx.test(value)){
		return false;
	}else{
		return true;
	}
});
</script>		
