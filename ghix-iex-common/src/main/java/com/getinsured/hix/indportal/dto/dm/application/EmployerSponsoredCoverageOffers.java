package com.getinsured.hix.indportal.dto.dm.application;

import java.util.Map;

public class EmployerSponsoredCoverageOffers {

	private Map<String,EscOffer> employerSponsoredCoverageOffers;
	 EscOffer escOffer1;
	 EscOffer escOffer2;


	 // Getter Methods 

	 public EscOffer getEscOffer2() {
		return escOffer2;
	}

	public void setEscOffer2(EscOffer escOffer2) {
		this.escOffer2 = escOffer2;
	}

	public EscOffer getEscOffer1() {
	  return escOffer1;
	 }

	 // Setter Methods 

	 public void setEscOffer1(EscOffer escOffer1) {
	  this.escOffer1 = escOffer1;
	 }
	}
