import React, { useEffect } from 'react';

/*****

 Implementation using Class-Component, because we need life-cycle methods.
 Should be removed later

     class Spinner extends React.PureComponent {

        componentDidMount() {
            window.addEventListener('scroll', this.disableScroll);
        }

        componentWillUnmount() {
            window.removeEventListener('scroll', this.disableScroll);
        }

        disableScroll = () => {
            window.scrollTo(0, 0);
        };

        render() {
            return (
                <div className="lds-roller-container">
                    <div className="lds-roller">
                        <div/>
                        <div/>
                        <div/>
                        <div/>
                        <div/>
                        <div/>
                        <div/>
                        <div/>
                    </div>
                </div>

            );
        }
    }

 *****/





/*****

 Implementation using Functional-Component with Hooks.
 Should be left

*****/

const Spinner = props => {
    // console.log(props);

    useEffect(() => {
        const disableScroll = () => window.scrollTo(0, 0);
        window.addEventListener('scroll', disableScroll);
        return () => window.removeEventListener('scroll', disableScroll);
    });

    return (
        <div className="lds-roller-container">
            <div className="ls-roller-wrapper">
                <div className="lds-roller-message">{props.message}</div>
                <br/>
                <div className="lds-roller">
                    {Array.from({length: 8}, () => <div key={Math.random()}/>)}
                </div>
            </div>
        </div>
    );
};

export default Spinner;