<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
    	<div class="row-fluid">
            <div class="span3" id="sidebar">
                <div class="header">
                	<h4 class="margin0"><a name="skip"></a><spring:message  code='label.addnewQHPapplication'/></h4>
                </div>
                    <!--  beginning of side bar -->
                    <ol class="nav nav-list">
                        <li class="done"><a href="#"><spring:message  code='label.provideplandetails'/></a></li>
                        <li class="done"><a href="#"><spring:message  code='label.uploadplanbenefits'/></a></li>
                        <li class="active"><a href="#"><spring:message  code='label.uploadplanrates'/></a></li>
                        <li class="link"><a href="#"><spring:message  code='label.selectoraddprovidernetworks'/></a></li>
                        <!--<li><a href="#"><spring:message  code='label.addcostsharingvariations'/></a></li>-->
                        <li class="link"><a href="#"><spring:message  code='label.reviewandconfirm'/></a></li>
                    </ol>
                    <!-- end of side bar -->
                
            </div><!-- end of span3 -->
            
            
            <div class="span9" id="rightpanel">
            <div class="header">
                      <h4 class="margin0">Step 3. <spring:message  code='label.uploadplanrates'/></h4>
                    </div>
                
                        <form class="form-horizontal" id="frmUploadRates" name="frmUploadRates" action="<c:url value="/admin/planmgmt/submitqhprates" />" method="POST">
                        <df:csrfToken/>
                        <div class="control-group margin0">
                            <label for="planName" class="required control-label"><spring:message  code='label.issuerName'/></label>
                            <div class="controls paddingT5">
                                <strong>${issuerName}</strong>
                            </div> <!-- end of controls-->
                        </div>
                        
                        <div class="control-group margin0">
                            <label for="planName" class="required control-label"><spring:message  code='label.planName'/></label>
                            <div class="controls paddingT5">
                                <strong>${planName}</strong>
                            </div> <!-- end of controls-->
                        </div>
                        
                        <div class="control-group margin0">
                            <label for="planName" class="required control-label"><spring:message  code='label.planNumber'/></label>
                            <div class="controls paddingT5">
                                <strong>${planNumber}</strong>
                            </div> <!-- end of controls-->
                        </div>
                        
                            <input type="hidden" id="rateFile" name="rateFile"/>
                            <input type="hidden" id="id" name="id" value="${planId}"/>
                            <input type="hidden" id="isEdit" name="isEdit" value="${isEdit}"/>
                            
                        </form> 
                        <form class="form-horizontal form-bottom" id="frmRates" name="frmRates" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
                            <div class="box-gutter10"> 
                             <div class="control-group">
                                <label for="planRates" class="control-label"><spring:message  code='label.planRate'/> </label>
                                <div class="controls">
                                            <span><input type="file" id="rates" name="rates"  class="input-file">
                                            &nbsp; <input type="button" value="Upload" title="Upload" class="btn" onclick="validCsv('#rates','#frmRates','#rateFile_error')"/>&nbsp; <a href=<c:url value="/resources/sampleTemplates/plan_rates_by_zip.csv" />><spring:message code='label.downloadTemplate'/></a>
                                            <input type="hidden" name="fileType" value="rates"/>
                                            </span>
                                            <div id="rateFile_error" class="">${fileError}</div>
                                <div id="ratesFileName" class="help">${ratesFileName}</div>
                                        
                                </div>    <!-- end of controls-->
                             </div>  <!-- end of control-group -->
            
                            
                                <div class="form-actions">
                                    <button type="button" title="<spring:message  code='label.saveAndContinue'/>" name="submitBtn" id="submitBtn" class="btn btn-primary" onclick="formSubmit()"><spring:message  code='label.saveAndContinue'/> </button>
                                </div><!-- end of form-actions -->
                            </div>
                        </form>   
                 </div>   <!-- end of span9 -->
                       
        </div>     
        </div>
   		<!--  end of row-fluid -->


<script type="text/javascript">
function validCsv(fileElementId, formElementId, errorElementId)
{
	var ext = $(fileElementId).val().split('.').pop().toLowerCase();
	var allow = new Array('csv');
	if(jQuery.inArray(ext, allow) == -1) {
	    $(errorElementId).html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileTypeError'/></span></label>");
	   return false;
	}else{
		$(formElementId).submit();
	  } 
}

function  formSubmit()
{
	$("#rateFile_error").html("");
	$("#frmUploadRates").submit();
}
$(document).ready(function(){
	var accreditationUpload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitAccreditationRequest,  // pre-submit callback 
	        success:       submitAccreditationResponse  // post-submit callback 
	    };
    $('#frmRates').ajaxForm(accreditationUpload);
    isFileReq();
});

function submitAccreditationResponse(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	if(val[0] != 'Error')
	{	
		$("#ratesFileName").text(val[1]);
		$("#rateFile").val(val[2]);
	}
	else
	{
		$("#rateFile_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
	}
}

function submitAccreditationRequest(formData, jqForm, options) {
	var fileName = $('#rates').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}

function isFileReq()
{
	if("false" == '${isEdit}')
	{
	$("#rateFile").rules("add", "required");
	}	
}

var validator = $("#frmUploadRates").validate({ 
	 ignore: "",
	rules : {rateFile : false
	}, 
	messages : {
		rateFile : { required : "<span> <em class='excl'>!</em><spring:message  code='error.rateFileRequired'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	} 
});
jQuery("#frmRates").validate({});
</script>
