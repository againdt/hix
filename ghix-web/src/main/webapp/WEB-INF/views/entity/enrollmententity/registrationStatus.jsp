<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@ taglib uri="/WEB-INF/tld/comments-view" prefix="comment" %>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="announcement" uri="/WEB-INF/tld/announcement-view.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />


<div class="gutter10">
	<div class="row-fluid">
		<div class="page-header">
			<h1><a name="skip"></a>${enrollmentEntity.entityName}</h1>
		</div>
	</div>

	<div class="row-fluid">
		<jsp:include page="../leftNavigationMenu.jsp" />
		<!-- end of span3 -->

	<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="pull-left"><spring:message code="label.entity.registrationStatus"/></h4>
			</div>
			<form class="form-horizontal" id="frmregistrationstatus" name="frmregistrationstatus" action="registrationstatus"method="GET">

			<input type="hidden" id="documentId1" name="documentId1" value=""/>
				<div class="gutter10-tb">
					<table class="table table-border-none">
						<tbody>
						<tr>
								<td class="span4 txt-right" scope="row"><spring:message code="label.EntityNumber"/></td>
								<td><strong>${enrollmentEntity.entityNumber}</strong></td>
							</tr>
							<tr>
								<td class="span4 txt-right" scope="row"><spring:message code="label.entity.status"/></td>
								<td><strong>${enrollmentEntity.registrationStatus}</strong></td>
							</tr>
							<tr>
							<c:choose>
								<c:when test="${enrollmentEntity.registrationStatus=='Active' || enrollmentEntity.registrationStatus=='Registered'}">
									<td class="span4 txt-right" scope="row"><spring:message code="label.RenewalDate"/></td>
									<td><strong><fmt:formatDate value= "${enrollmentEntity.registrationRenewalDate}" pattern="MM-dd-yyyy"/></strong></td>
								</c:when>
								<c:otherwise>
									<td class="span4 txt-right" scope="row"><spring:message code="label.RenewalDate"/></td>
									<td><strong><spring:message code="label.entity.nA"/></strong></td>
								</c:otherwise>
							</c:choose>
							</tr>

						</tbody>
					</table>
				</div><!-- end of .gutter10 -->



                <h4><spring:message code="label.entity.registrationHistory"/></h4>

                <div class="gutter10">
					<display:table id="entityRegStatusHistory" name="entityRegStatusHistory" list="entityRegStatusHistory" requestURI="" sort="list" class="table" >
				           <display:column property="updatedOn" titleKey="label.entity.date"  format="{0,date,MM/dd/yyyy}" sortable="false"  />
				           <display:column property="previousRegistrationStatus" titleKey="label.entity.previousStatus" sortable="false" />
				           <display:column property="newRegistrationStatus" titleKey="label.NewStatus" sortable="false" />
						   <display:column property="comments" titleKey="label.entity.coment" sortable="false" />

					</display:table>
				</div>
				<!-- end of form-actions -->

			</form>
		</div><!-- end of span9 -->
		</div>
	</div>
	<div id="modalBox" class="modal hide fade">
					<div class="modal-header">
						<a class="close" data-dismiss="modal" data-original-title="">x</a>
						<h3 id="myModalLabel">
							<spring:message code="label.assister.viewComment"/>
						</h3>
					</div>
					<div id="commentDet" class="modal-body">
						<label id="commentlbl"></label>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message code="label.assister.close"/></a>
					</div>
	</div>
	<!--  end of row-fluid -->


<script>
function getComment(comments)
{
	$('#commentDet').html("<p> Loading Comment...</p>");
	comments =comments.replace(/#/g,'<br/>'); //Added to remove new line break marker with HTML equivalent br
	comments=comments.replace(/apostrophes/g,"'"); //Added to replace regex apostrophes with '
	$('#commentDet').html("<p> "+ comments + "</p>");

}

$("#registerStatus").removeClass("link");
$("#registerStatus").addClass("active");

$(function () {
	$("a[rel=popover]")
		.popover({
			offset: 10
		})
		.click(function(e) {
			e.preventDefault();  // prevent the default action, e.g., following a link
		});
});

$(document).ready(function(){
	$("#myTable").tablesorter();
	$("#showGraphs").verticaltabs({speed: 500,slideShow: false,activeIndex: 2});

	$('#feedback-badge-right').click(function() {
		$dialog.dialog('open');
		return false;
	});
});

function highlightThis(val){
	var currUrl = window.location.href;
	var newUrl="";

	/* Check if Query String parameter is set or not
	* If yes *
	*/
	if(currUrl.indexOf("?", 0) > 0) {
		if(currUrl.indexOf("?lang=", 0) > 0) { /* Check if locale is already set without querystring param */
			newUrl = "?lang="+val;
		} else if(currUrl.indexOf("&lang=", 0) > 0) { /* Check if locale is already set with querystring param  */
			newUrl = currUrl.substring(0, currUrl.length-2)+val;
		} else {
			newUrl = currUrl + "&lang="+val;
		}
	} else { /* If No */
		newUrl = currUrl + "?lang="+val;
	}
	window.location = newUrl;
}
</script>
