<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<script>
		$(document).ready(function(){
			$('.ps-detail').on('click', '.toggle', function() {
				var me = $(this);
				var toggleDiv = me.closest('.ps-detail__group').find('.toggle-div');
				var header = me.closest('div');

				if(me.hasClass('icon-chevron-up')) {
					me.removeClass('icon-chevron-up').addClass('icon-chevron-down');
					header.css('background-color', '#fff').css('border-bottom', '1px solid #f0f0f0');
				}else {
					me.removeClass('icon-chevron-down').addClass('icon-chevron-up');
					header.css('background-color', '#f0f0f0').css('border-bottom', 'none');
				}
				toggleDiv.slideToggle( "fast" );
			});
		});
	</script>

<script type="text/ng-template" id="customGrouping">

		<style>
		ul {
		list-style: none;
		}

		.tabs li {
		float: left;
		width: 50%;
		}
		.tabs a {
		display: block;
		text-align: center;
		text-decoration: none;
		text-transform: uppercase;
		color: #0e6094;
		padding: 10px 0;
		border-bottom: 1px solid #888;
		/*background: #f7f7f7;*/
		font-size: 17px;
		font-weight: bold;
		}
		.tabs a:hover,
		.tabs a.active {
		border-bottom: 3px solid #0f6eaa;
		}

		.clearfix:after {
		content: "";
		display: table;
		clear: both;
		}
		.flex-container {
		display: flex;
		flex-direction: row;
		border-radius: 5px;
		padding: 10px;
		margin: 5px;
		border: solid 1px #e2e2e2;
		background-color: #ffffff;"
		}

		.flex-child-1 {
			flex-basis: 30%;
		}

		.flex-child-2 {
			flex-basis: 70%;
		}

		.member-info {
			text-transform: capitalize;
			line-height: 2.2;
			letter-spacing: normal;
			color: #22313f;
			margin: 10px;
		}

		.group-header {
			padding: 0px 22px;
			font-size: 14px;
		}

		.member-spacing {
			padding-left:20px;
		}

		.enrolled {
			color: green;
		}

		.groupCheck {
			height: 20px;
			width: 20px
		}

		</style>


		<div class="ps-detail well ng-scope" ng-init="showCustomGrouping()">
			<ul class="tabs clearfix" data-tabgroup="first-tab-group">
				<li> <a href="javascript:void(0)" ng-click="toggleGroups('Health')" id='tab-0' ng-class="{'active': showHealthGroups}" ><spring:message code="indportal.customGrouping.shopHealthPlans" /></a></li>
				<li> <a href="javascript:void(0)" ng-click="toggleGroups('Dental')" id='tab-1' ng-class="{'active': showDentalGroups}" ><spring:message code="indportal.customGrouping.shopDentalPlans" /></a></li>
			</ul>

	<div class="well step group-header" id="healthGroups" ng-show="showHealthGroups" style="padding: 0 0px 0 25px; border-radius: 5px;">

		<div ng-show="isAnyGroupEnrolled" id="aid_enrolled_grp">
			<div class="ps-detail__group">
				<div class="ps-detail__header ps-detail__header--group" style="background-color: rgb(240, 240, 240); border-bottom: none;">
					 <span id="aid_enrolledHeader"><spring:message code="indportal.customGrouping.enrolled" /> ( {{enrolledMembersCount}} <spring:message code="indportal.customGrouping.members" />)</span>
					 <i class="toggle icon-chevron-up"></i >
				</div>
				<div class="toggle-div" style="border: solid 1px #e2e2e2;">
					<div class="ps-detail__group-body u-table" style="padding: 0px">
						<div ng-if="healthGroups.length>0" ng-repeat="group in healthGroups" >
							<div ng-if="isAllEnrolled(group)" id="{{'aid_enrolled_grp_' + $index}}">
								<div style="padding: 5px">
									<div ng-if="showAddAPTCMessage(group)">
										<span><spring:message code="indportal.customGrouping.APTC" /> <span>{{formatAmount(group.maxAptc)}} </span> <spring:message code="indportal.customGrouping.APTCMsg" /></span>
									</div>
									<div ng-if="!showAddAPTCMessage(group)">
									<div ng-if="group.hasAPTCEligibleMembers" >
										<span><spring:message code="indportal.customGrouping.APTCSuccess" /> </span>
		<span ng-if="group.electedAptc" id="{{'aid_elected_aptc_lbl_' + $index}}"><spring:message code="indportal.customGrouping.APTCMsg2" /> <strong><span id="{{'aid_elected_aptc_value_' + $index}}"> {{ formatAmount(group.electedAptc) }} </span ></strong><spring:message code="indportal.customGrouping.APTCMsg3" /></span>
									</div>
									<div ng-if="!group.hasAPTCEligibleMembers">
										<span id="{{'aid_enrolled_success_' + $index}}"><spring:message code="indportal.customGrouping.APTCSuccess" /></span>
									</div>
								</div>
								</div>
								<div class="flex-container" style="border: none">
									<div class="flex-child-1">
										<div ng-repeat="member in group.memberList" class="member-spacing">
											<span ng-show="member.healthEnrollmentId !== undefined" >
												<input type="checkbox" class="ps-form__check-input groupCheck" id="{{member.id+group.id}}" checked style="display: none">
												<i class="icon-ok icon-large enrolled">
													<strong class="member-info">
														{{member.firstName}} {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
													</strong>
												</i>
											</span>
										</div>
									</div>

									<div class="flex-child-2">
										<div ng-if="group.groupCsr != null && group.groupCsr != 'CS1'" id="{{'aid_members_qualify_' + $index}}"><spring:message code="indportal.customGrouping.groupCsr" /></div>

										<%--<div ng-if="group.maxAptc > 0">Total Advance Premium Tax Credit for this group: <strong>$ {{group.maxAptc}} </strong></div>--%>
										<div style="margin-top: 10px; margin-right: 10px;">
											<div id="{{'aid_issuer_name' + $index}}"> {{group.issuerName}} </div>
											<div id="{{'aid_plan_name' + $index}}"> {{group.planName}} </div>
		<div ng-if="group.netPremium"><span id="{{'aid_net_premium' + $index}}" > {{formatAmount(group.netPremium)}}</span></div>
										</div>
										<div class="flex-container" style="border: none">
											<div class="flex-child-1" style="justify-content: center;" ng-show="group.validCoverageSDate">
                                                <button class="btn btn-primary btn-small pull-left" id="{{'aid_btn_disenroll' + $index}}" ng-click="disEnrollPlan('Health', group.coverageDate, group.encryptedGroupEnrollmentId)"><spring:message code="indportal.customGrouping.disenroll" /></button>
											</div>
											<div class="flex-child-1" style="justify-content: center;" ng-show="showCancelCoverageButton(group)">
                                                <button class="btn btn-primary btn-small pull-left" id="{{'aid_btn_cancel_coverage' + $index}}" ng-click="disEnrollPlan('Health', group.coverageDate, group.encryptedGroupEnrollmentId)"><spring:message code="indportal.customGrouping.canceCoverage" /></button>
											</div>
											<div class="flex-child-2" style="justify-content: center;" ng-if="getButtonText(group)">
                                                <button class="btn btn-primary btn-small pull-right" id="{{'aid_btn_finalize_changeplan' + $index}}" ng-click="changePlan('HEALTH', group.id, group)">{{getButtonText(group)}}</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div ng-if="showHealthGroups" style="margin-top: 10px;" id="aid_unenrolled_grps">
			<div ng-if="UnEnrolledhealthGroups.length > 0  && isAllMembersAddedToOtherGroups()">
				<div style="margin-top: 8px; margin-bottom: 8px;" id="aid_uneg_whom_are_you_shopping"><strong><spring:message code="indportal.customGrouping.whomYouAreShopping" /></strong></div>

				<div ng-show="isMixOfAIMembers()">
					<span style="margin-bottom: 15px" id="aid_uneg_mix_ai_an_member"> <spring:message code="indportal.customGrouping.aiAnMember1" /></span>
				</div>

				<div ng-show="!isMixOfAIMembers()">
					<span id="aid_uneg_no_mix_ai_an_member"> <spring:message code="indportal.customGrouping.aiAnMember2" /></span>
					<p><spring:message code="indportal.customGrouping.aiAnMember3" /></p>
					<p><spring:message code="indportal.customGrouping.aiAnMember4" /></p>
					<p><spring:message code="indportal.customGrouping.aiAnMember5" /></p>
					<p style="margin-bottom: 15px" ><spring:message code="indportal.customGrouping.aiAnMember6" /></p>
				</div>

				<div style="margin-top: 10px;">
					<label class="ps-form__check-input" id="aid_uneg_shop_members" > <strong> <spring:message code="indportal.customGrouping.shopForThese" /></strong> </label>
				</div>
				<div ng-if="healthGroups.length>0" ng-repeat="group in UnEnrolledhealthGroups" style="font-size: 14px;">
					<div ng-if="isAnyMembersUnEnrolled(group) && !areAllMembersAddedToGroups(group)" id="{{ 'aid_unenrolled_grp_' + $index }}">
						<div class="flex-container" id="{{'aid_uneg_members_' + $index}}">
							<div class="flex-child-1 member-info-container">
								<div ng-if="group.memberList != null && group.memberList.length > 0" ng-repeat="member in group.memberList" class="member-spacing">
									<div  ng-show="member.healthEnrollmentId === undefined" class="ps-form__check" >
											<input type="checkbox" ng-checked="$parent.$parent.$index === 0 ? true: false" class="ps-form__check-input groupCheck" id="{{member.id+group.id}}" ng-model="member.isChecked" ng-change="optionToggled(member, group, UnEnrolledhealthGroups)" value="{{member}}" ng-disabled="member.enrollmentId !== null && member.enrollmentId !=='' && member.enrollmentId !=undefined ? true : false">
											<label class="ps-form__check-input">
												<strong class="member-info">
													{{member.firstName}} {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
												</strong>
											</label>
									</div>
								</div>
							</div>

							<div class="flex-child-2">
								<div ng-if="group.groupCsr != null && group.groupCsr != 'CS1'"><spring:message code="indportal.customGrouping.csrMsg" /></div>
								<div ng-if="group.maxAptc > 0"><spring:message code="indportal.customGrouping.totalAPTC" /><strong><span id="{{'aid_uneg_maxaptc_' + $index}}"> {{formatAmount(group.maxAptc)}}</span></strong></div>
								<div style="margin-top: 10px; margin-right: 10px;">
									<div id="{{'aid_uneg_group_' + $index}}" > {{group.issuerName}} </div>
									<div id="{{'aid_uneg_planname_' + $index}}"> {{group.planName}} </div>
									<div ng-if="group.netPremium">
										<span id="{{'aid_uneg_net_premium_' + $index}}"> {{formatAmount(group.netPremium)}}</span>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div style="margin-left: 10px; display: flex; flex-direction:row-reverse; margin-top: 20px">
					<div ng-if="UnEnrolledhealthGroups.selectedMembersCount > 0">
						<a href="javascript:void(0)" class="btn btn-primary" id="aid_uneg_shop_for_members" ng-click="showHealthPlansButtonDisabled==true || (validateGroup() === true && enrollAfterCustomGrouping('HEALTH', UnEnrolledhealthGroups[0].id, healthGroups, false))" ><spring:message code="indportal.customGrouping.shopHealthPlans" /> <span> ( {{UnEnrolledhealthGroups.selectedMembersCount}} <spring:message code="indportal.customGrouping.members2" />)</span> </a>
					</div>

					<div ng-if="UnEnrolledhealthGroups.selectedMembersCount === 0">
						<a href="javascript:void(0)" class="btn btn-primary" id="aid_uneg_shop_with_existing_members" ng-click="showHealthPlansButtonDisabled==true || (validateGroup() === true && enrollAfterCustomGrouping('HEALTH', UnEnrolledhealthGroups[0].id, healthGroups, false))" disabled="true"><spring:message code="indportal.customGrouping.shopHealthPlans" /></a>
					</div>
				</div>
			</div>
		</div>

		<div ng-show="showHealthGroups && addMembersGroups && addMembersGroups.length > 0" style="margin-top: 21px; border: 1px solid lightgrey; border-radius: 5px;" class="" id="multiGrp">
			<div class="ps-detail__header ps-detail__header--group" style="background-color: rgb(240, 240, 240); border-bottom: none;">
				<spring:message code="indportal.customGrouping.selectOption" />
			</div>

			<div ng-repeat="groupSet in addMembersGroups" style="font-size: 14px;">

				<div ng-if="groupSet.AddedInEnrolledGroup" style="border: 1px solid lightgray; margin: 10px; padding: 10px; border-radius: 5px;" id="{{ 'aid_multi_enrolled_grp_' + $index}}">

						<div style="margin-top: 10px;">
							<label class="ps-form__check-input" id="{{ 'aid_mng_grp_add_' + $index}}" > <strong> <spring:message code="indportal.customGrouping.addMembers" /> </strong> </label>
						</div>
						<div style="margin-top: 10px;">
							<label class="ps-form__check-input" id="{{ 'aid_mngd_grp_' + $index}}" > {{getUnEnrolledMembers(groupSet.AddedInEnrolledGroup)}} <spring:message code="indportal.customGrouping.sameLevel" /></label>
						</div>

						<div class="flex-container" style="border: none; margin-left: 10px;">
							<div class="flex-child-1">
								<div ng-if="groupSet.AddedInEnrolledGroup.memberList != null && groupSet.AddedInEnrolledGroup.memberList.length > 0" ng-repeat="member in groupSet.AddedInEnrolledGroup.memberList" class="member-spacing">
									<div ng-show="member.healthEnrollmentId !== undefined" id="{{ 'aid_mngd_grp_members_enr_' + $parent.$index}}">
										<input type="checkbox" class="ps-form__check-input groupCheck" id="{{member.id+group.id}}" checked style="display: none">
										<i class="icon-ok icon-large enrolled">
											<strong class="member-info">
												{{member.firstName}} {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
											</strong>
										</i>
									</div>

									<div  ng-show="member.healthEnrollmentId === undefined" class="ps-form__check" id="{{ 'aid_mngd_grp_members_nen_' + $parent.$index}}">
											<input
												type="checkbox"
												ng-checked="applicationType !== 'SEP' && $parent.$parent.$index === 0 ? true: false"
												class="ps-form__check-input groupCheck" id="{{member.id+groupSet.AddedInEnrolledGroup.id}}"
												ng-model="member.isChecked"
												ng-change="optionToggled(member, groupSet.AddedInEnrolledGroup, groupSet.AddedInEnrolledGroup, 'SINGLE')"
												value="{{member}}"
												ng-disabled="member.enrollmentId !== null && member.enrollmentId !=='' && member.enrollmentId !=undefined ? true : false">
											<label class="ps-form__check-input">
												<strong class="member-info">
													{{member.firstName}} {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
												</strong>
											</label>
									</div>
								</div>
							</div>

							<div class="flex-child-2">
								<div style="margin-top: 10px; margin-right: 10px;">
									<div id="{{ 'aid_mngd_issuer_name_' + $index}}"> {{groupSet.AddedInEnrolledGroup.issuerName}} </div>
									<div id="{{ 'aid_mngd_plan_name_' + $index}}"> {{groupSet.AddedInEnrolledGroup.planName}} </div>
									<div id="{{ 'aid_mngd_net_premium_' + $index}}" ng-if="groupSet.AddedInEnrolledGroup.netPremium">{{formatAmount(groupSet.AddedInEnrolledGroup.netPremium)}}</div>
								</div>

								<div id="{{ 'aid_mngd_add_engrp_' + $index}}" ng-if="groupSet.AddedInEnrolledGroup.groupCsr != null && groupSet.AddedInEnrolledGroup.groupCsr != 'CS1'"><spring:message code="indportal.customGrouping.csrMsg" /></div>
		<div id="{{ 'aid_mngd_add_engrp_aptc' + $index}}" ng-if="groupSet.AddedInEnrolledGroup.maxAptc > 0">Total Advance Premium Tax Credit for this group: <strong>{{ formatAmount(getMaxAPTC(groupSet.AddedInEnrolledGroup)) }}</strong></div>

							</div>

						</div>

						<div style="margin-left: 10px; display: flex; flex-direction:row-reverse;">
								<div ng-if="groupSet.AddedInEnrolledGroup.selectedMembersCount > 0">
									<a href="javascript:void(0)" id="{{'aid_mngd_add_engrp_shop_' + $index}}" class="btn btn-primary" ng-click="showHealthPlansButtonDisabled==true || (validateGroup() === true && addToExistingGroup('HEALTH', groupSet.AddedInEnrolledGroup.id, groupSet.AddedInEnrolledGroup, false))"><spring:message code="indportal.customGrouping.addToPlan" /><span> ( {{groupSet.AddedInEnrolledGroup.selectedMembersCount}} Members)</span> </a>
								</div>

								<div ng-if="groupSet.AddedInEnrolledGroup.selectedMembersCount  === 0">
									<a href="javascript:void(0)" id="{{'aid_mngd_add_engrp_add_shop_' + $index}}" class="btn btn-primary" disabled="true"><spring:message code="indportal.customGrouping.addToPlan" /></a>
								</div>
						</div>

				</div>
			</div>

			<div style="display: flex; justify-content: center; margin: 10px;"> <span style="
			   height: 32px;
			   width: 32px;
			   font-size: 12px;
			   border: 2px solid #69696959;
			   border-radius: 21px;
			   background: lightgrey; display: flex; justify-content: center;flex-direction: column; text-align: center;"><spring:message code="indportal.customGrouping.or" /></span>
			</div>

			<div ng-repeat="groupSet in addMembersGroups" style="font-size: 14px;" id="{{ 'aid_un_mngd_' + $index}}">

				<div ng-if="canShowGroup(groupSet.enrollmentGroup,addMembersGroups, $index)" style="border: 1px solid lightgray; margin: 10px; padding: 10px; border-radius: 5px;">

						<div style="margin-top: 10px;">
							<label id="{{ 'aid_un_sho_health_plans_' + $index}}" class="ps-form__check-input"> <strong><spring:message code="indportal.customGrouping.shopNewHealthPlan" /></strong></label>
						</div>

						<div class="flex-container" style="border: none; margin-left: 10px;">
							<div class="flex-child-1">
								<div ng-if="groupSet.enrollmentGroup.memberList != null && groupSet.enrollmentGroup.memberList.length > 0" ng-repeat="member in groupSet.enrollmentGroup.memberList" class="member-spacing">

									<div  ng-show="member.healthEnrollmentId === undefined" class="ps-form__check" id="{{ 'aid_un_sho_member_' + $parent.$index}}" >
											<input type="checkbox" class="ps-form__check-input groupCheck" id="{{member.id+groupSet.enrollmentGroup.id}}" ng-model="member.isChecked" ng-change="optionToggled(member, groupSet.enrollmentGroup, groupSet.enrollmentGroup, 'SINGLE')" value="{{member}}" ng-disabled="member.enrollmentId !== null && member.enrollmentId !=='' && member.enrollmentId !=undefined ? true : false">
											<label class="ps-form__check-input">
												<strong class="member-info">
													{{member.firstName}} {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
												</strong>
											</label>
									</div>
								</div>
							</div>

							<div class="flex-child-2">
								<div id="{{ 'aid_un_group_csr_' + $index}}" ng-if="groupSet.enrollmentGroup.groupCsr != null && groupSet.enrollmentGroup.groupCsr != 'CS1'"><spring:message code="indportal.customGrouping.csrMsg" /></div>
								<div id="{{ 'aid_un_max_aptc_' + $index}}" ng-if="groupSet.enrollmentGroup.maxAptc > 0"><spring:message code="indportal.customGrouping.totalAPTC" /><strong>{{ formatAmount(getMaxAPTC(groupSet.enrollmentGroup)) }}</strong></div>
								<div style="margin-top: 10px; margin-right: 10px;">
									<div id="{{ 'aid_un_issuer_name_' + $index}}" > {{groupSet.enrollmentGroup.issuerName}} </div>
									<div id="{{ 'aid_un_plan_name_' + $index}}"> {{groupSet.enrollmentGroup.planName}} </div>
									<div id="{{ 'aid_un_sho_net_premium_' + $index}}" ng-if="groupSet.enrollmentGroup.netPremium">{{ formatAmount(groupSet.enrollmentGroup.netPremium)}} </div>
								</div>
							</div>
						</div>

						<div style="margin-left: 10px; display: flex; flex-direction:row-reverse;">
								<div ng-if="groupSet.enrollmentGroup.selectedMembersCount > 0">
									<a href="javascript:void(0)" id="{{ 'aid_un_enroll_shop_btn_' + $index}}" class="btn btn-primary" ng-click="showHealthPlansButtonDisabled==true || (validateGroup() === true && enrollAfterCustomGrouping('HEALTH', groupSet.enrollmentGroup.id, healthGroups, false))"><spring:message code="indportal.customGrouping.shopForHealthPlans" /><span> ( {{groupSet.enrollmentGroup.selectedMembersCount}} Members)</span> </a>
								</div>

								<div ng-if="groupSet.enrollmentGroup.selectedMembersCount  === 0">
									<a href="javascript:void(0)" id="{{ 'aid_un_enroll_shop_count_btn_' + $index}}" class="btn btn-primary" ng-click="showHealthPlansButtonDisabled==true || (validateGroup() === true && enrollAfterCustomGrouping('HEALTH', groupSet.enrollmentGroup.id, healthGroups, false))" disabled="true"><spring:message code="indportal.customGrouping.shopForHealthPlans" /></a>
								</div>
						</div>


				</div>
		</div>

		</div>

		<div ng-if="healthGroups.length<=0">
			<p class="alert alert-info" id="aid_no_health_group"><spring:message code="indportal.customGrouping.noHealthGroup" /></p>
		</div>

	</div>

	<div class="well step group-header" id="dentalGroups_enr" ng-show="showDentalGroups && isAnyDentalEnrollmentsAvailable" style="padding: 0 0px 0 25px;">
		<div class="ps-detail__header ps-detail__header--group" style="background-color: rgb(240, 240, 240); border-bottom: none;">
			<span id="aid_enrolledHeader_enr"><spring:message code="indportal.customGrouping.enrolled" /> ( {{enrolledDentalMembersCount}} <spring:message code="indportal.customGrouping.members" />)</span>
			<i class="toggle icon-chevron-up"></i >
		</div>

		  	<div ng-if="dentalGroups.length>0" ng-repeat="group in dentalGroups" class="flex-container" id="{{ 'aid_dental_grps_nrg_' + $index}}">
				<div  class="flex-child-1">
					<div ng-if="group.memberList != null && group.memberList.length > 0" ng-repeat="member in group.memberList" class="member-spacing">

						<div ng-show="member.dentalEnrollmentId !== undefined" class="ps-form__check" id="{{ 'aid_dental_grps_member_enr_nrg_' + $parent.$index}}">
							<input type="checkbox" class="ps-form__check-input groupCheck" id="{{member.id}}" checked style="display: none" >
							<i class="icon-ok icon-large enrolled">
								<strong class="member-info">
									{{member.firstName}} {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
								</strong>
							</i>
						</div>
					</div>
				</div>
				<div  class="flex-child-2">

					<div id="{{ 'aid_dental_grps_max_aptc_all_enr_' + $index}}" ng-if="group.electedAptc > 0"><spring:message code="indportal.customGrouping.totalAPTC" /> <span>$</span>{{group.electedAptc}}</div>

					<div style="margin-top: 10px; margin-right: 10px;">
						<div id="{{ 'aid_dental_issuer_name_all_enr_' + $index}}"> {{group.issuerName}} </div>
						<div id="{{ 'aid_dental_plan_name_all_enr_' + $index}}"> {{group.planName}} </div>
						<div id="{{ 'aid_dental_net_premium_all_enr_' + $index}}" ng-if="group.netPremium">{{ formatAmount(group.netPremium) }} </div>
					</div>

					<div style="justify-content: center; margin: 5px;" ng-show="group.validCoverageSDate">
						<button class="btn btn-primary btn-small pull-left" id="{{ 'aid_dental_btn_disenroll_nrg_' + $index}}" ng-click="disEnrollPlan('Dental', group.coverageDate, group.encryptedGroupEnrollmentId)"><spring:message code="indportal.customGrouping.disenroll" /></button>
					</div>
					<div class="flex-child-1" style="justify-content: center;" ng-show="showCancelCoverageButton(group)">
						<button class="btn btn-primary btn-small pull-left" id="{{ 'aid_dental_btn_cancel_coverage_nrg_' + $index}}" ng-click="disEnrollPlan('Dental', group.coverageDate, group.encryptedGroupEnrollmentId)"><spring:message code="indportal.customGrouping.canceCoverage" /></button>
					</div>
					<div style="justify-content: center; margin: 5px;" ng-if="getButtonText(group)">
						<button class="btn btn-primary btn-small pull-right" id="{{ 'aid_dental_btn_change_finalize_nrg_' + $index}}" href="javascript:void(0)" ng-click="changePlan('DENTAL', group.id, group)">{{getButtonText(group)}}</button>
					</div>
				</div>
		  	</div>

		<div ng-if="dentalGroups.length<=0">
			<p id="aid_dental_no_dental_grp_nrg_" class="alert alert-info"><spring:message code="indportal.customGrouping.noDentalGroup" /></p>
		</div>

	</div>

		<div class="well step group-header" id="dentalGroups" ng-show="showDentalGroups && !isAllMembersEnrolledInDental" style="padding: 0 0px 0 25px;">
		<div>
			<label id="aid_shop_for_dental" class="ps-form__check-input"> <strong><spring:message code="indportal.customGrouping.shopForThese" /> </strong> </label>
		</div>

		  	<div ng-if="dentalGroups.length>0" ng-repeat="group in dentalGroups" class="flex-container" id="{{ 'aid_dental_grps_' + $index}}">
				<div  class="flex-child-1">
					<div ng-if="group.memberList != null && group.memberList.length > 0" ng-repeat="member in group.memberList" class="member-spacing">

						<div ng-show="member.dentalEnrollmentId !== undefined" class="ps-form__check" id="{{ 'aid_dental_grps_member_enr_' + $parent.$index}}">
							<input type="checkbox" class="ps-form__check-input groupCheck" id="{{member.id}}" checked style="display: none" >
							<i class="icon-ok icon-large enrolled">
								<strong class="member-info">
									{{member.firstName}} {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
								</strong>
							</i>
						</div>

						<div ng-show="member.dentalEnrollmentId === undefined" class="ps-form__check" id="{{ 'aid_dental_grps_member_nenr_' + $parent.$index}}">
							<input type="checkbox" class="ps-form__check-input groupCheck" id="{{member.id}}" ng-model="member.isChecked" ng-change="dentalOptionToggled(group)" value="{{member}}" ng-disabled= "isDentalDisabled(member)" >
							<label class="ps-form__check-input">
								<strong class="member-info">
									{{member.firstName}} {{member.lastName}} <span ng-if="member.suffix !== undefined"> {{member.suffix}} </span>
								</strong>
							</label>
						</div>

					</div>
				</div>
				<div  class="flex-child-2">
					<div id="{{ 'aid_dental_grps_max_aptc_' + $index}}" ng-if="group.maxAptc > 0"><spring:message code="indportal.customGrouping.totalAPTC" /> {{formatAmount(group.maxAptc)}}</div>
					<div style="margin-top: 10px; margin-right: 10px;">
						<div id="{{ 'aid_dental_issuer_name_' + $index}}"> {{group.issuerName}} </div>
						<div id="{{ 'aid_dental_plan_name_' + $index}}"> {{group.planName}} </div>
							<div id="{{ 'aid_dental_net_premium_' + $index}}" ng-if="group.netPremium">{{formatAmount(group.netPremium)}} </div>
					</div>
				</div>
		  	</div>

		<div ng-if="dentalGroups.length<=0">
			<p id="aid_dental_no_dental_grp" class="alert alert-info"><spring:message code="indportal.customGrouping.noDentalGroup" /></p>
		</div>

		<div class="pull-right" ng-if="dentalGroups.length>0">
			<a href="javascript:void(0)" id="aid_dental_shop_btn_dental_" class="btn btn-primary" ng-click="showDentalPlansButtonDisabled ==true || enrollAfterCustomGrouping('DENTAL', dentalGroups[0].id, dentalGroups, false)" ng-hide="showDentalPlansButtonDisabled"><spring:message code="indportal.customGrouping.shopDentalPlans" /></a>
		</div>
	</div>

</div>


		<div ng-show="pendingAppDialog_unused">
			 <div modal-show="pendingAppDialog_unused" id="aid_pending_app_dialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="pendingAppModal" aria-hidden="true">
				<div class="modal-dialog">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button"  class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					  <h3 id="pendingAppModal" style="font-size: 18px;border: none; z-index: 1101;"><spring:message code="indportal.pendingAppModal.header" /></h3>
					</div>
					<div class="modal-body">
						<spring:message code="indportal.pendingAppModalForCancel.body" />
					</div>
					<div class="modal-footer">
					  <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.ok" /></a>
					</div>
				  </div><!-- /.modal-content-->
				</div> <!-- /.modal-dialog-->
			 </div>
		</div>

		<!-- Modal for failing of forming custom groups -->
			<div ng-show= "customGroupingFailure">
				<div modal-show="customGroupingFailure" id="aid_custom_grouping_failure_dialog" class="modal fade customGroupingFailure" tabindex="-1" role="dialog" aria-labelledby="failedFormingGroups" aria-hidden="true">
					 <div class="modal-dialog">
						<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title"><spring:message code="indportal.customGrouping.customGrouping" /></h4>
								</div>
								<div class="modal-body">
									<p>{{customGroupingFailureMsg}}</p>
								</div>
								<div class="modal-footer">
						<button class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.portalhome.close"/></button>
						</div>
					   </div><!-- /.modal-content-->
					 </div> <!-- /.modal-dialog-->
				</div>
			</div>
		<!-- Modal for failing of forming custom groups -->

		<!-- Modal for failing of multi custom groups -->
				<div ng-show= "multiGroupFailure" id="aid_multi_group_failure_dialog" modal-show="multiGroupFailure" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedFormingGroups" aria-hidden="true">
					<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" ng-click="undoSelection()" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body">
								<p>{{multiGroupFailureFailureMsg}}</p>
								<p>{{multiGroupFailureFailureMsg2}}</p>
							</div>
							<div class="modal-footer">


								<button class="btn" ng-click="undoSelection()" ng-if="!multiGroupFailureNotAllowed">
									<spring:message code="indportal.customGrouping.cancel" />
								</button>
								<button class="btn"  data-dismiss="modal" ng-if="!multiGroupFailureNotAllowed">
									<spring:message code="indportal.customGrouping.continue" />
								</button>
								<button class="btn"  ng-click="undoSelection()" ng-if="multiGroupFailureNotAllowed">
									<spring:message code="indportal.customGrouping.continue" />
								</button>
							</div>
				   </div><!-- /.modal-content-->
				</div>
		<!-- Modal for failing of forming custom groups -->

	<!-- Modal to encourage consumers to enroll in health plans prior to dental -->
	<div ng-show="healthPriorDentalModal" modal-show="healthPriorDentalModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedFormingGroups" aria-hidden="true">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" id="shop_popup_close" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p><spring:message code="indportal.customGrouping.healthEnrollPriorDental" /></p>
			</div>
			<div class="modal-footer">
				<button class="btn" id="continueWithHealth" ng-click="toggleTabs('Health')"><spring:message code="indportal.customGrouping.continueWithHealth" /></button>
				<button class="btn" id="StartDentalShopping" ng-click="toggleTabs('Dental')"><spring:message code="indportal.customGrouping.startDentalShopping" /></button>
			</div>

		</div><!-- /.modal-content-->
	</div>
	<!-- Modal to encourage consumers to enroll in health plans prior to dental -->


		<!-- Modal for failing of multi custom groups -->
		<div ng-show= "dentalHasHealthEnrollemnts" id="aid_has_dental_enrollments_dialog" modal-show="dentalHasHealthEnrollemnts" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedFormingGroups" aria-hidden="true">
			<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="shop_popup_close" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<p>"{{dentalHasHealthEnrollemntsMsg}}"</p>
					</div>
					<div class="modal-footer">
						<button class="btn" id="start_dental_enroll" ng-click="toggleTabs('Dental')"><spring:message code="indportal.customGrouping.startDentalEnrollment" /></button>
						<button class="btn" id="continue_health_enroll" ng-click="toggleTabs('Health')"><spring:message code="indportal.customGrouping.continuesHealthEnrollment" /></button>
					</div>

		   </div><!-- /.modal-content-->
		</div>
		<!-- Modal for failing of forming custom groups -->

<!-- Pending app dialog -->
<div ng-show="pendingAppDialog">
 <div modal-show="pendingAppDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="pendingAppModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="pendingAppModal" style="font-size: 18px;border: none; z-index: 1101;"><spring:message code="indportal.pendingAppModal.header" /></h3>
        </div>
        <div class="modal-body">
			<spring:message code="indportal.pendingAppModal.body" />
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.cancel" /></a>
        </div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- Pending app dialog -->

		<!-- Disenroll Modal-->
		<div ng-show="disenrollDialog" id="aid_disenroll_dialog">
		 <div modal-show="disenrollDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disenrollModal" aria-hidden="true">
			<div class="modal-dialog">
			  <div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancelDisenroll()">&times;</button>
				  <h3 id="disenrollModal"><spring:message code="indportal.plansummary.voluntarydisenrollmentreasons" /></h3>
				</div>
				<div class="modal-body ">
				  <form class="form-horizontal">
					<h5 class="margin10-l" id="subtitle"><strong><spring:message code="indportal.plansummary.whyareyoudisenrolling" /></strong></h5>

					<div class="margin30-l">

					  <label class="radio">
						  <input type="radio" name="disenrollmentreason" id="disaffordability" value="CANNOT_AFFORD_PAYMENT" checked="" required="" class="" ng-model="checked">
						  <spring:message code="indportal.plansummary.icantafford" />
					  </label>
					  <br>
					  <label class="radio" >
						  <input type="radio" name="disenrollmentreason" id="disnothappywithplan" value="NO_PROPER_SERVICE" required="" class="" ng-model="checked">
						  <spring:message code="indportal.plansummary.imnothappywith" />
					  </label>
					  <br>
					  <label class="radio">
						  <input type="radio" name="disenrollmentreason" id="outOfnetwork" value="PHY_OUT_OF_NETWORK" required="" class="" ng-model="checked">
						  <spring:message code="indportal.plansummary.phyOutOfNetwork" />
					  </label>
					  <br>
					  <label class="radio">
						  <input type="radio" name="disenrollmentreason" id="agentError" value="AGENT_ERROR" class="" ng-model="checked">
						  <spring:message code="indportal.plansummary.agentError"/>
					  </label>
					  <br>
					  <label class="radio">
						  <input type="radio" name="disenrollmentreason" id="other" value="OTHER" class="" ng-model="checked">
						  <spring:message code="indportal.plansummary.other"/>
					  </label>

					</div>
				   <div id="otherDisenrollReason" ng-if="checked=='OTHER'">
					  <div class="margin30-l">
						<input type="text" name="tellusmore" id="tellusmore" class="span11" ng-model="$parent.otherReason"/>
					  </div>
				   </div>
				  </form>
					  <div class="margin5-l margin5-r margin20-t alert alert-info margin0-b">
					  <p><spring:message code="indportal.plansummary.otherReasonText"/></p>
					  </div>
				</div>
				<div class="modal-footer">
				  <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal" ng-click="cancelDisenroll()"><spring:message code="indportal.portalhome.cancel" /></a>
				  <a href="javascript:void(0);" class="btn btn-primary"  ng-if="!checked" ng-click="disenrollmentReason()" class=""><spring:message code="indportal.customGrouping.skipAndContinue" /></a>
				  <a href="javascript:void(0);" class="btn btn-primary"  ng-if="checked" ng-click="disenrollmentReason()" class=""><spring:message code="indportal.portalhome.continue" /></a>
				</div>
			  </div><!-- /.modal-content-->
			</div> <!-- /.modal-dialog-->
		   </div>
		</div>
<!-- Disenroll Modal-->

<!-- Are You Sure Modal-->
<div ng-show="areYourSureDialog">
 <div modal-show="areYourSureDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disenrollCheckModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="disenrollCheckModal">
			<span ng-if="typeOfPlan == 'Dental'"><spring:message code="indportal.plansummary.doyouwanttocontinuedental" /></span>
			<span ng-if="typeOfPlan == 'Health'"><spring:message code="indportal.plansummary.doyouwanttocontinue" /></span>
			<span ng-if="typeOfPlan == 'both'"><spring:message code="indportal.plansummary.doyouwanttocontinueboth" /></span>
		  </h3>
        </div>
        <div class="modal-body gutter20">
		  <span ng-if="typeOfPlan == 'Health'">
          	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll" /></p>
          </span>
		  <span ng-if="typeOfPlan == 'Dental'">
          	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenrolldental" /></p>
		 	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll2dental" /></p>
          </span>
		  <span ng-if="typeOfPlan == 'both'">
          	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenrollboth" /></p>
		 	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll2both" /> <a href="https://www.coveredca.com/individuals-and-families/getting-covered/special-enrollment/qualifying-life-events/" target="_blank"><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll2_1" /></a></p>
		  	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll3both" /></p>
          </span>
		  <span ng-if="typeOfPlan == 'Health' && dentaldetails && dentaldetails.disEnroll && dentaldetails.electedAptc > 0">
			<p><spring:message code="indportal.plansummary.aptcwarning" /></p>
		  </span>
          <div class="margin20-t alert alert-info margin0-b" ng-if="typeOfPlan == 'Health' || typeOfPlan == 'both'">
            <spring:message code="indportal.plansummary.ifyoucanaffordbut" />
            <br><br>
			<spring:message code="indportal.plansummary.thefeecontent4" /> <a href="<spring:message code='indportal.plansummary.yhiExemptionsUrl'/>" target="_blank"> <spring:message code="indportal.plansummary.yhiExemptionsUrl"/></a>
          </div>
        </div>
        <div class="modal-footer">
          <a href="#" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.no" /></a>
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="goToDisenrollDateModal()" class=""><spring:message code="indportal.portalhome.yes" /></a>
        </div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- Are You Sure Modal-->
<!-- Choosing Disenrollment Date Modal-->
<div ng-show="disenrollDateDialog.step">
 <div modal-show="disenrollDateDialog.step" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disenrollCheckModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="disenrollDateModal" ng-if="disenrollDateDialog.step == 1"><spring:message code="indportal.plansummary.selectTerminationDate"/></h3>
		  <h3 id="ConfirmDisenrollDateModal" ng-if="disenrollDateDialog.step == 2"><spring:message code="indportal.plansummary.confirmEndDate"/></h3>
		  <h3 id="ConfirmSubmitDisenrollDateModal" ng-if="disenrollDateDialog.step == 3"><spring:message code="indportal.plansummary.confirmation"/></h3>
        </div>
        <div class="modal-body gutter20" ng-if="disenrollDateDialog.step == 1">
		  <form class="form-horizontal">
            <div class="margin30-l">
              <label class="radio margin5-b" ng-repeat ="date in disenrollmentDates">
                  <input type="radio" name="disenrollmentdate" checked="" required="" class="" ng-model="disenrollDateDialog.date" ng-value="date">
                  {{date.display}} ({{date.actual}})
              </label>
            </div>

		  </form>
		  <div class="margin20-t alert alert-info margin0-b"><spring:message code="indportal.plansummary.needSpecificTermDate"/></div>
        </div>

        <div class="modal-body gutter20" ng-if="disenrollDateDialog.step == 2">
          <p>
			<spring:message code="indportal.plansummary.chosenEndDate"/> {{disenrollDateDialog.date.actual| date:'longDate'}} <spring:message code="indportal.plansummary.from"/>
			<span ng-if="typeOfPlan == 'Health'"> {{healthdetails.planName}}</span>
			<span ng-if="typeOfPlan == 'Dental'"> {{dentaldetails.planName}}</span>
			<span ng-if="typeOfPlan == 'both'"> {{healthdetails.planName}} <spring:message code="indportal.customGrouping.and"/> {{dentaldetails.planName}}</span>
		  </p>

		  <div class="margin20-t margin0-b disenroll-confirm-box">
			<!--<p><spring:message code="indportal.plansummary.from"/></p>-->
			<div ng-if="typeOfPlan == 'Health'"><img class="pull-left" ng-src="{{healthdetails.planLogoUrl}}" alt="{{healthdetails.planName}} logo"></div>
			<div ng-if="typeOfPlan == 'Dental'"><img class="pull-left" ng-src="{{dentaldetails.planLogoUrl}}" alt="{{dentaldetails.planName}} logo"></div>
			<div ng-if="typeOfPlan == 'both'"><img class="pull-left" ng-src="{{healthdetails.planLogoUrl}}" alt="{{healthdetails.planName}} logo">
			<img class="pull-right" ng-src="{{dentaldetails.planLogoUrl}}" alt="{{dentaldetails.planName}} logo"></div>
          </div>
        </div>
        <div class="modal-body gutter20" ng-if="disenrollDateDialog.step == 3">
		  <div class="margin20-t alert alert-info margin0-b disenroll-confirm-box">
             <p ng-if="typeOfPlan == 'Health'">
				<spring:message code="indportal.plansummary.disenrollConfirmContent1"/> <em>{{healthdetails.planName}}</em> <spring:message code="indportal.plansummary.disenrollConfirmContent2"/> {{disenrollDateDialog.date.actual| date:'longDate'}}.
			</p>
			<p ng-if="typeOfPlan == 'Dental'">
				<spring:message code="indportal.plansummary.disenrollConfirmContent1"/> <em>{{dentaldetails.planName}}</em> <spring:message code="indportal.plansummary.disenrollConfirmContent2"/> {{disenrollDateDialog.date.actual| date:'longDate'}}.
			</p>
			<p ng-if="typeOfPlan == 'both'">
				<spring:message code="indportal.plansummary.disenrollConfirmContent1"/> <em>{{healthdetails.planName}}</em> <spring:message code="indportal.customGrouping.and"/> <em>{{dentaldetails.planName}}</em> <spring:message code="indportal.plansummary.disenrollConfirmContent2"/> {{disenrollDateDialog.date.actual| date:'longDate'}}.
			</p>
          </div>
		  <div class="margin20-t alert alert-info margin0-b">
			<spring:message code="indportal.plansummary.note"/>:
			<ul>
			  <li><spring:message code="indportal.plansummary.disenrollConfirmPt1"/></li>
			</ul>
		  </div>
        </div>

        <div class="modal-footer" ng-if="disenrollDateDialog.step == 1">
          <button class="btn btn-primary" ng-click="disenrollDateDialog.step = 2" ng-disabled="!disenrollDateDialog.date"><spring:message code="indportal.portalhome.continue"/></button>
        </div>
        <div class="modal-footer" ng-if="disenrollDateDialog.step == 2">
          <button class="btn btn-secondary" ng-click="disenrollDateDialog.step = 1"><spring:message code="indportal.portalhome.continue"/></button>
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" class="" ng-click="disenrollDateDialog.step = 3"><spring:message code="indportal.portalhome.continue"/></a>
        </div>
        <div class="modal-footer" ng-if="disenrollDateDialog.step == 3">
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" class="" ng-click="submitDisenrollment()"><spring:message code="indportal.contactus.submit"/></a>
        </div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- Choosing Disenrollment Date Modal-->

<!-- SubmitDisenroll-->
<div ng-show="submitDisenrollmentDialog">
 <div modal-show="submitDisenrollmentDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="submitDisenrollModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="submitDisenrollModal"><spring:message code="indportal.plansummary.voluntarydisenrollmentrequestsubmitted" /></h3>
        </div>
        <div class="modal-body gutter20">
          <p><spring:message code="indportal.plansummary.wehavereviewed" /></p>
          <!--<p><spring:message code="indportal.plansummary.youwillbecovered" /></p>-->
          <p><spring:message code="indportal.plansummary.ifyouhaveanyquestions" /></p>
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="goHome()"><spring:message code="indportal.portalhome.godashboard" /></a>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
</div>
<!-- SubmitDisenroll-->
<!-- SubmitDisenroll Failure Modal-->
<div ng-show="submitDisenrollmentFailureDialog">
 <div modal-show="submitDisenrollmentFailureDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedSumissionModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="failedSumissionModal"><spring:message code="indportal.plansummary.technicalissues" /></h3>
        </div>
        <div class="modal-body gutter20">
          <p><spring:message code="indportal.plansummary.cannotsubmitrequest" /></p>
          <p><spring:message code="indportal.plansummary.ifyouhaveanyquestions" /></p>
        </div>
        <div class="modal-footer">
          <a class="btn btn-secondary" data-dismiss="modal" href="/#"><spring:message code="indportal.portalhome.cancel" /></a>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
  </div>
</div>
<!-- SubmitDisenroll Failure Modal-->
</script>
