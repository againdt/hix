package com.getinsured.web.model;

/**
 * Contains asset types.
 *
 * @author Yevgen Golubenko
 * @since 2/11/19
 */
public enum AssetType
{
  /**
   * Logo URL
   */
  LOGO,

  /**
   * Mobile logo
   */
  LOGO_MOBILE,

  /**
   * Second logo (sub-logo) url.
   */
  SUB_LOGO,

  /**
   * Style for Fav Icon.
   */
  FAV_ICON,

  /**
   * Specific CSS file that will override default CSS rules for current environment.
   */
  CSS;
}
