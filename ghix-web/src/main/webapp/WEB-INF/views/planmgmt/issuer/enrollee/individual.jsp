<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="ghix" uri="/WEB-INF/tld/dropdown-lookup.tld"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">

	<div class="row-fluid">
    	<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#">Members</a></li>
            <li>Manage Individual</li>
        </ul><!--page-breadcrumb ends-->
		<h1><issuerLogo:view/><a class="skip"><spring:message code='label.individualMembers'/></a> <small>${resultSize} <spring:message code='label.totalMembers'/></small></h1>
	</div><!-- end of page-header -->
	
    <div class="row-fluid">
        <div class="span3" id="sidebar">
			<div class="header">
				<h4 class="margin0"><spring:message code='label.refineresult' /></h4>
			</div>
                	<form class="form-vertical" id="frmApplication" name="frmApplication" action="<c:url value="/planmgmt/issuer/indvmembers" />"  method="POST">
                    	<df:csrfToken/>
                        <div class="graybg gutter10">
                        	<div class="control-group">
                                <label for="planname" class="control-label"><spring:message code='label.memberName'/></label>
                                <div class="controls">
                                     <input type="text" name="name" id="name" value="${searchCriteria.name}"  class="span12" size="30" />
                                </div> <!-- end of controls-->
                            </div><!-- end of control-group -->
                            
                            <div class="control-group">
                                <label for="planname" class="control-label"><spring:message code='label.planNumber'/></label>
                                <div class="controls">
                                    <input type="text" name="plannumber" id="plannumber" value="${searchCriteria.plannumber}"  class="span12" size="30" />
                                </div> <!-- end of controls-->
                            </div><!-- end of control-group -->
                            <div class="control-group">
                                <label for="planname" class="control-label"><spring:message code='label.policyNumber'/></label>
                                <div class="controls">
                                    <input type="text" name="policynumber" id="policynumber" value="${searchCriteria.policynumber}"  class="span12" size="30" />
                                </div> <!-- end of controls-->
                            </div><!-- end of control-group -->
                            <div class="control-group">
                                <label for="planname" class="control-label"><spring:message  code='label.status'/></label>
                                <div class="controls">                                   
                                    <ghix:dropdown name="status" id="status" cssclass="span12" lookuptype="ENROLLMENT_STATUS"  value="${searchCriteria.status}" />
                                </div> <!-- end of controls-->
                            </div><!-- end of control-group -->
                            <input type="submit" name="submitbtn" id="submitbtn" value="<spring:message code='label.go'/>" class="btn"  title="<spring:message code='label.go'/>"/> 
                        </div>
                        
                    </form>
         
        </div><!--end of .span3  -->
        <div class="span9" id="rightpanel">
			<c:choose>
                    <c:when test="${fn:length(applicationlist) > 0}">
                        <table class="table">
                            <thead>
                                <tr class="header">                                     	
                                     <th scope="col"><dl:sort title="Member Name" sortBy="firstName" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                     <th scope="col"><dl:sort title="Plan Number" sortBy="enrollment.CMSPlanID" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>						    
                                     <th scope="col"><dl:sort title="Policy Number" sortBy="healthCoveragePolicyNo" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                     <th scope="col"><dl:sort title="Effective Date" sortBy="enrollment.benefitEffectiveDate" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                     <th scope="col"><dl:sort title="Status" sortBy="enrollment.enrollmentStatusLkp.lookupValueCode" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>						     					     
                                </tr>
                            </thead>
                            	<c:forEach items="${applicationlist}" var="application">
                                   <tr>
                                        <td><a href="<c:url value="/planmgmt/issuer/viewindvmember/${application['id']}" />">${application['firstName']} ${application['middleName']} ${application['lastName']}</a></td>
                                        <td>${application['enrollmentCMSPlanID']}</td>
                                        <td>${application['healthCoveragePolicyNo']}</td>														
                                       <td><fmt:formatDate value="${application['effectiveStartDate']}" pattern=" MMM dd, yyyy"/></td>
                                        <td>${application['enrollmentenrollmentStatusLkplookupValueLabel']}</td>					
                                    </tr>
                                    
                                </c:forEach>
                        </table>
                        
                         <div class="center"><dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}"  hideTotal="true"/></div>
                        
                    </c:when>
                    <c:otherwise>
                        <div class="alert alert-info"><spring:message code='label.recordsNotFound'/></div>
                    </c:otherwise>
                </c:choose>
        </div><!-- end of .span9 -->
    </div><!-- end of .row-fluid -->
</div><!-- end of .row-fluid -->

<script type="text/javascript">
	$(document).ready(function(){                   
        $("<option value=''>Select</option>").prependTo('#status'); // add "Any" option into plan level
        
        if('${searchCriteria.status}'.length<=0){
           $("#status option[value='']").attr('selected', 'selected');
        }
        $("#status").on("change", function (e) {
        	var optionSelected = $("option:selected",this);
        	if(optionSelected[0].value === 'CONFIRM'){
        		optionSelected.attr("value","Enrolled");
        	}
        });
	});
	
</script>