<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/tld/employee-dashboard-util" prefix="util"%>
<%@ taglib prefix="initnav" uri="/WEB-INF/tld/employee-initialenroll-nav" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<style type="text/css">
	.dialogCSS{background-color: white}
	#emp-contribution.well {
		padding: 10px;
	}
	.dl-horizontal dt {overflow: visible;}
	#plan-summary .dl-horizontal dt, #emp-contribution .dl-horizontal dt { width: 196px!important;}
	#plan-summary .dl-horizontal dd, #emp-contribution .dl-horizontal dd { margin-left: 195px !important;}
	
	.no-close .ui-dialog-titlebar-close { display: none;background-color: white}
</style>


<fmt:formatDate pattern="MM/dd/yyyy" value="${enrollmentStartDt}" var="enollStartDate" />
<fmt:formatDate pattern="MM/dd/yyyy" value="${enrollmentEndDt}" var="enollEndDate" />

<div class="gutter10">
	<c:if test="${UpcomingCoverageAvailable=='Y'}">
		<div class="row-fluid bubble-nav-shop">
		   <ul class="nav nav-tabs">
				<li id="currentEnrollmentTab"  <c:if test="${selectedTab=='currentCoverage' }" > class="active" </c:if> ><a href="<c:url value='/shop/employee/home/${currentApplicationId}'/>" >${currentTabName}</a></li>
				<li id="upcomingEnrollmentTab"  <c:if test="${selectedTab=='upcomingCoverage' }" > class="active"  </c:if> ><a href="<c:url value='/shop/employee/home/${upcomingApplicationId}'/>" >${upcommingTabName}</a></li>
			</ul>
			<div class="alert">
				<span <c:if test="${selectedTab == 'upcomingCoverage' }" > class="hide"</c:if> id="currentEnrollmentAlert">The information below is related to your current coverage which ends on <strong>${coverageEndDate}</strong>.</span>
				<span <c:if test="${selectedTab == 'currentCoverage' }" > class="hide"</c:if> id="upcomingEnrollmentAlert">The information below is related to your upcoming coverage which begins on <strong>${effectiveDate}</strong>.</span>
			</div>
		</div>
	</c:if>
	
	<div class="row-fluid" id="titlebar">
  		<h1>Welcome &nbsp;<span id="user-name">${employee.name}</span></h1>
  	</div>
  
  	<div class="row-fluid">
	    <div class="span3" id="sidebar">
	      <util:myStuff employeeApplication="${employeeApplication}"></util:myStuff>
	      <security:authorize  access="hasPermission('SHOP_WAIVECOVERAGE_EMPLOYEE')"> 
	      		<util:quickLinks employeeApplication="${employeeApplication}"></util:quickLinks>
	      </security:authorize>
	    </div><!--/span3-->
   
	    <div class="span9" id="rightpanel">
	    	<div class="header">
		      	<span class="pull-right" id="enroll-date">
		      		<dl class="dl-horizontal">
		      			<dt>Open Enrollment:</dt>
		                <dd>${enollStartDate} - ${enollEndDate}</dd>
		            </dl>                    
		        </span>
		        <h4>What's Next</h4>        
	      	</div><!--header  -->
	        <div class="gutter10">
	           	<div class="row-fluid">
	 			<!-- TLD for employee special enrollment -->
				  	<initnav:initialEnrollment employeeApplication="${employeeApplication}" errorMsg="${errorMsg}"></initnav:initialEnrollment>
		          	<div class="row-fluid">
			           	<!-- TLD for employer contribution and plan summary -->
		      		     <util:employerContribution employeeApplication="${employeeApplication}"></util:employerContribution>
			      		 <util:planSummary employeeApplication="${employeeApplication}" isSpEnrollment="false"></util:planSummary>
	      			</div><!-- .row-fluid -->
	  			</div><!-- .row-fluid-->
	  		</div><!--/gutter10-->        
		</div><!--/rightpanel-->
	</div><!-- .row-fluid -->
</div>

<div id="dialog-confirm" title="<spring:message code = "label.startmakeChanges"/>">
  <p><span style="float: left; margin: 0 7px 20px 0;" class="ui-icon ui-icon-alert"></span> </p><p style="margin: -5px 0 0 20px;"><spring:message code = "label.makechanges"/></p>
  <p id="wait" class="info-header" style="display:none">Please Wait......</p>
</div>

<div id="dialog-undowaiver-confirm" title="<spring:message code = "label.undowaiver"/>">
  <p><span style="float: left; margin: 0 7px 20px 0;" class="ui-icon ui-icon-alert"></span><spring:message code = "label.confirmundowaiver"/> </p>
</div>

<jsp:include page="waive_employer_coverage.jsp" >
	<jsp:param name="employeeApplicationId" value="${employeeApplication.id}" />
</jsp:include>                


<script type="text/javascript">

$(document).ready(function(){
	$('div[rel=popover]').popover({trigger:'hover'});
	$('div[rel=popover]').popover({trigger:'focus'});	
	
	var employee_status = '${employee.status}';
	if(employee_status == 'TERMINATED' || employee_status == 'NOT_ELIGIBLE'){
		$("button.btn-primary").css('display', 'none');
	}
	
});



function gotoNext(pageNum){
	var applicationCode = '${employeeApplication.id}';
	if(pageNum=='1'){
		location.href = '<c:url value="/shop/employee/editemployee/${employeeApplication.id}" />';
	}
	else if(pageNum=='2'){
		location.href = '<c:url value="/shop/employee/verifydependent/${employeeApplication.id}" />';
	}
	else if(pageNum=='3'){
		location.href = '<c:url value="/shop/employee/employeeselectplan/${employeeApplication.id}" />';
	}
	else if(pageNum=='5'){
		location.href = '<c:url value="/shop/employee/spplanselection/myplans/${employeeApplication.id}" />';
	}
	/* else if(pageNum=='4'){
		location.href="<c:url value='/shop/employee/verifyemployee' />";
	}
	 */
}



$( ".makeChanges" ).click(function() {
	  $( "#dialog-confirm" ).dialog( "open" );
	});

	$( "#dialog-confirm" ).dialog({
		 autoOpen: false,
	    resizable: false,
	    height:250,
	    width: 500,
	    dialogClass:"dialogCSS",
	    modal: true,
	    buttons: [
	              {
	                  text: "No",
	                  "class": 'btn',
	                  click: function() {
	                	  $(this).dialog( "close" );
	                  }
	              },
	              {
	                  text: "Yes",
	                  "class": 'btn btn-primary',
	                  click: function() {
							var subUrl = '<c:url value="/shop/employee/updateEnrollment/${employeeApplication.id}">
												<c:param name="${df:csrfTokenParameter()}"> 
													<df:csrfToken plainToken="true" />
												</c:param> 
											</c:url>';
	                	  $('#wait').show();
	                	  $.ajax({
	              			type : "POST",
	              			url : subUrl,
	              			success : function(response,xhr) {
								if(isInvalidCSRFToken(xhr))	    				
									return;
	              				$('#wait').hide();
	              				$( "#dialog-confirm" ).dialog( "close" );
	              				if(response=="SUCCESS"){
	              					window.location.reload(true);
	              				}
	              				if(response=="FAIL"){
	              					alert('Unable to make changes');
	              				}
	              					
	              			},
	              			error : function(e) {
	              				alert("Failed to Makce Changes");
	              				$('#wait').hide();
	              				$( "#dialog-confirm" ).dialog( "close" );
	              			}
	              		});
	                  }
	              }
	          ],

});
	
$( ".undoWaiver" ).click(function() {
	  $( "#dialog-undowaiver-confirm" ).dialog( "open" );
	});

$( "#dialog-undowaiver-confirm" ).dialog({
	 autoOpen: false,
    resizable: false,
    height:200,
    width: 500,
    dialogClass:"dialogCSS",
    modal: true,
    buttons: [
              {
                  text: "No",
                  "class": 'btn',
                  click: function() {
                	  $(this).dialog( "close" );
                  }
              },
              {
                  text: "Yes",
                  "class": 'btn btn-primary',
                  click: function() {
            	  	var urlUndoWaiver = '<c:url value="/shop/employee/undowaive/${employeeApplication.id}">
									<c:param name="${df:csrfTokenParameter()}"> 
										<df:csrfToken plainToken="true" />
									</c:param> 
								</c:url>';

                	  $.ajax({
	              			type : "POST",
	              			url : urlUndoWaiver,
	              			data : {id : '${employeeApplication.id}'}, 
	              			success : function(response,xhr) { 
								if(isInvalidCSRFToken(xhr))	    				
									return;
	              				$('#dialog-undowaiver-confirm').dialog( "close" );
	              				if(response=="SUCCESS"){
	              					window.location.reload(true);
	              				}
	              				if(response=="FAIL"){
	              					alert('Unable to Undo Waiver');
	              				}
	              					
	              			},
	              			error : function(e) {
	              				alert("Failed to Undo Waiver");
	              				$('#dialog-undowaiver-confirm').dialog( "close" );
	              			}
	              		});
                	  
                  }
              }
          ],

});

function loadData(val, appCode){
	location.href=	'/hix/shop/employee/home/'+appCode;
}  	

</script>
