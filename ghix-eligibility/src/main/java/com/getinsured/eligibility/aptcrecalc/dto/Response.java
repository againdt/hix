package com.getinsured.eligibility.aptcrecalc.dto;

public class Response {
    private String shopAptcAmount;
    private String keepAptcAmount;
    private String shopStateSubsidyAmount;
    private String keepStateSubsidyAmount;
    private String status;
    private String errorCode;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getShopAptcAmount() {
        return shopAptcAmount;
    }

    public String getKeepAptcAmount() {
        return keepAptcAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setShopAptcAmount(String shopAptcAmount) {
        this.shopAptcAmount = shopAptcAmount;
    }

    public void setKeepAptcAmount(String keepAptcAmount) {
        this.keepAptcAmount = keepAptcAmount;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShopStateSubsidyAmount() {
        return shopStateSubsidyAmount;
    }

    public void setShopStateSubsidyAmount(String shopStateSubsidyAmount) {
        this.shopStateSubsidyAmount = shopStateSubsidyAmount;
    }

    public String getKeepStateSubsidyAmount() {
        return keepStateSubsidyAmount;
    }

    public void setKeepStateSubsidyAmount(String keepStateSubsidyAmount) {
        this.keepStateSubsidyAmount = keepStateSubsidyAmount;
    }
}
