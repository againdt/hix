
package com.getinsured.iex.ssap;

import javax.validation.Valid;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class HouseholdContact {

    @Expose
    private Boolean homeAddressIndicator=false;
    @Expose
    private Phone phone=new Phone();
    @Expose
    @Valid
    private HomeAddress homeAddress=new HomeAddress();
    @Expose
    private String stateResidencyProof;
    @Expose
    private OtherPhone otherPhone=new OtherPhone();
    @Valid
    @Expose
    private ContactPreferences contactPreferences=new ContactPreferences();
    @Expose
    @Valid
    private MailingAddress mailingAddress=new MailingAddress();
    @Expose
    private Boolean mailingAddressSameAsHomeAddressIndicator=false;

    public Boolean getHomeAddressIndicator() {
        return homeAddressIndicator;
    }

    public void setHomeAddressIndicator(Boolean homeAddressIndicator) {
        this.homeAddressIndicator = homeAddressIndicator;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public HomeAddress getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(HomeAddress homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getStateResidencyProof() {
        return stateResidencyProof;
    }

    public void setStateResidencyProof(String stateResidencyProof) {
        this.stateResidencyProof = stateResidencyProof;
    }

    public OtherPhone getOtherPhone() {
        return otherPhone;
    }

    public void setOtherPhone(OtherPhone otherPhone) {
        this.otherPhone = otherPhone;
    }

    public ContactPreferences getContactPreferences() {
        return contactPreferences;
    }

    public void setContactPreferences(ContactPreferences contactPreferences) {
        this.contactPreferences = contactPreferences;
    }

    public MailingAddress getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(MailingAddress mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

    public Boolean getMailingAddressSameAsHomeAddressIndicator() {
        return mailingAddressSameAsHomeAddressIndicator;
    }

    public void setMailingAddressSameAsHomeAddressIndicator(Boolean mailingAddressSameAsHomeAddressIndicator) {
        this.mailingAddressSameAsHomeAddressIndicator = mailingAddressSameAsHomeAddressIndicator;
    }

}
