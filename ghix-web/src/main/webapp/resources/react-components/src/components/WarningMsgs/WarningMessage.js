import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClock } from '@fortawesome/free-regular-svg-icons'

import { WARNING_TYPES } from '../../constants/dashboardModelTypes';

const WarningMessage = ({ content, type, showIcon }) => {

    /**
     * Returns color coding depending of a warning's type.
     * @param type
     * @returns {*}
     */
    const getBackgroundStyle = (type) => {
        const backgrounds = {
            [WARNING_TYPES.WARNING]: 'alert alert-error',
            [WARNING_TYPES.ALERT]: 'alert alert-info',
            [WARNING_TYPES.ANNOUNCEMENT]: 'ds-u-fill--warn'
        };

        return backgrounds[type];
    };

    return (
        <div className="ds-u-margin--2">
            <div className={getBackgroundStyle(type)}>
                <div className="oep-notice">
                    {showIcon && <FontAwesomeIcon icon={faClock} style={{marginRight: '5px'}}/>}
                    <span className="ds-text ds-u-measure--wide">
                        {content}
                    </span>
                </div>
            </div>
        </div>
    )
};

export default WarningMessage;