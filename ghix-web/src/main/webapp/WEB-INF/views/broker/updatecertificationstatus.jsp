<%@ taglib prefix="audit" uri="/WEB-INF/tld/audit-view.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@page import="com.getinsured.hix.model.Broker"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/assister.css" />" media="screen"/>
<link href="../resources/css/accessibility.css" media="screen" rel="stylesheet" type="text/css" />
<%@ page isELIgnored="false"%>

	<style>
		form .box-loose .form-actions {
			margin-bottom: 0;
		}
		.table-auto {
			width: auto;
		}
	</style>
	<link href="/hix/resources/css/broker.css" media="screen" rel="stylesheet" type="text/css" />
	
	<div class="container">
		<div class="gutter10">
		<form class="form-horizontal" id="frmupdatecertification" name="frmupdatecertification" action='<c:url value="/admin/broker/updatecertificationstatus"/>' method="POST">
			<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
			<input type="hidden" id="brokerId" name="brokerId" value="${broker.id}"/>	
			<!-- start of secondary navbar - TO DO: Move to topnav.jsp -->
				<%-- <ul class="nav nav-tabs">
					<li><a href="#">Dashboard</a></li>
					<li><a href="<c:url value="/admin/manageissuer" />">Issuers</a></li>
					<li><a href="<c:url value="/admin/planmgmt/manageqhpplans" />">Health Plans</a></li>
					<li class="active"><a href="<c:url value="/admin/broker/manage"/>">Brokers</a></li>
					<li><a href="#">Enrollment</a></li>
					<li><a href="#">Account</a></li>
				</ul>

				<ul class="nav nav-pills">
					  <li class="active"><a href="<c:url value="/admin/broker/manage"/>">Manage Brokers</a></li>
				</ul> --%>
				<!-- end of secondary navbar -->

				<div class="row-fluid">
					<div class="page-header gutter10">
						<h3><a name="skip" class="skip"><spring:message code="label.brkupdatecertstatus"/></a></h3>
					</div><!-- end of page-header -->
	
					<ul class="thumbnails" id="broker-identification">
						<spring:url value="/broker/photo/${encBrokerId}"  var="photoUrl"/>
						<li class="span2">
							<img src="${photoUrl}" alt="Broker thumbnail photo" class="thumbnail profilephoto" />
						</li>
						<li class="span10">
							<h3 class="name broker-name">${broker.user.firstName}&nbsp;${broker.user.lastName}</h3>
							<table class="table table-border-none table-condensed table-auto">
								<tbody>
									<tr>
										<td class="span4 txt-right"><spring:message  code="label.brkEmail"/></td>
										<td><strong>${broker.user.email}</strong></td>
									</tr>
									<tr>
										<td class="txt-right"><spring:message  code="label.brkPhone"/></td>
										<td><strong>${broker.contactNumber}</strong></td>
									</tr>
									<tr>
										<td class="txt-right"><spring:message  code="label.brkAddress"/></td>
										<td><strong>${broker.location.address1}, ${broker.location.address2}, ${broker.location.city}, ${broker.location.state}, ${broker.location.zip}</strong></td>
									</tr>
									<tr>
										<td class="txt-right"><spring:message  code="label.licenseNumber"/></td>
										<td><strong>${broker.licenseNumber}</strong></td>
									</tr>
								</tbody>
							</table>
							
								<div class="span5 gutter10">
								<iframe width="225" height="225" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="thumbnail" src="//maps.google.com/maps?oe=utf-8&amp;client=firefox-a&amp;q=${broker.location.address1}+${broker.location.city},+${broker.location.state}+${broker.location.zip}+map&amp;ie=UTF8&amp;hq=&amp;hnear=${broker.location.address1}+${broker.location.city},+${broker.location.state}+${broker.location.zip}&amp;gl=us&amp;t=m&amp;ll=${broker.location.lat},${broker.location.lon}&amp;z=13&amp;iwloc=A&amp;output=embed"></iframe>
							</div>
						</li>
					</ul>

					<div class="box-loose">
						<div class="gutter10">
							<div class="control-group">
								<label for="certificationStatus" class="control-label"><spring:message  code="label.certificationstatus"/></label>
								<div class="controls">
									<select  size="1"  id = "certificationStatus" name="certificationStatus" path="statuslist" class="input-medium">
								<option value=""><spring:message  code="label.brkSelect"/></option>
									<c:forEach  items="${statuslist}" var= "value">
										<option <c:if test="${value == broker.certificationStatus}"> SELECTED </c:if> value="${value}">${value}</option> 
									</c:forEach>
									</select>
									<div id="certificationStatus_error"></div>
								</div>
							</div>

							<div class="control-group">
								<label for="comments" class="control-label"><spring:message  code="label.brkaddcomments"/></label>
								<div class="controls">
									<textarea id="comments" name= "comments">${broker.comments}</textarea>
								</div>
							</div><!-- end of control-group -->

						<!-- 	<div class="control-group">
								<label for="fileInput" class="control-label">Upload File</label>
								<div class="controls">
									<input type="file" id="fileInput" class="input-file" />
									<button class="btn btn-small">Upload</button>
								</div>
							</div> -->

							<div class="form-actions">
								<input type="submit" class="btn btn-primary" name="submit" id="submit" value="Update" title="Update"/>
								<a class="btn" href="<c:url value="/admin/broker/manage"/>"><spring:message  code="label.brkCancel"/></a>
							</div>
						</div>
							
						<h2><a><spring:message  code="label.brkauditinfo"/></a></h2>
						<div class="audit_info">
							<!-- Display Audit information - Starts -->
							<audit:view 
								entityId="${Agent.id}" 
								className="com.getinsured.hix.model.Broker"
								repoBeanName="iBrokerRepository">
							</audit:view>
							<!-- Display Audit information - Ends -->
						</div>
					</div>
				</div>

			</form>
		</div>

	</div><!--  end of .container -->

		<script type="text/javascript">
			var validator = $("#frmupdatecertification").validate({ 
				rules : {
					certificationStatus : {required : true},
					
				},
				messages : {
					certificationStatus : { required : "<span><em class='excl'>!</em><spring:message  code='label.validateCertificationStatus' javaScriptEscape='true'/></span>"}
					
				},
				errorClass: "error",
				errorPlacement: function(error, element) {
					var elementId = element.attr('id');
					error.appendTo( $("#" + elementId + "_error"));
					$("#" + elementId + "_error").attr('class','error help-inline');
				}
			});
		</script>

	