package com.getinsured.iex.hub.nmec.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.ResponseMetadataType;


/**
 * Wrapper class for gov.hhs.cms.dsh.sim.ee.vlp.ResponseMetadataType
 * 
 * @author Nikhil Talreja
 * @since 10-Feb-2014
 * 
 */
public class ResponseMetadataWrapper implements JSONAware{
	
	private String responseCode;
	private String responseDescriptionText;
	private String tdsResponseDescriptionText;
    
    public ResponseMetadataWrapper(ResponseMetadataType responseMetadataType){
    	
    	if(responseMetadataType == null){
    		return;
    	}
    	
    	this.responseCode = responseMetadataType.getResponseCode();
    	this.responseDescriptionText = responseMetadataType.getResponseDescriptionText();
    	this.tdsResponseDescriptionText = responseMetadataType.getTDSResponseDescriptionText();
    }
    
    public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescriptionText() {
		return responseDescriptionText;
	}

	public void setResponseDescriptionText(String responseDescriptionText) {
		this.responseDescriptionText = responseDescriptionText;
	}

	public String getTdsResponseDescriptionText() {
		return tdsResponseDescriptionText;
	}

	public void setTdsResponseDescriptionText(String tdsResponseDescriptionText) {
		this.tdsResponseDescriptionText = tdsResponseDescriptionText;
	}
    
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseCode", this.responseCode);
		obj.put("ResponseDescriptionText", this.responseDescriptionText);
		obj.put("TDSResponseDescriptionText", this.tdsResponseDescriptionText);
		return obj.toJSONString();
	}
	
}
