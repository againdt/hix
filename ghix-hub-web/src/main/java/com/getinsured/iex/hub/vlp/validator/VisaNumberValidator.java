package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for visa number
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 *
 */
public class VisaNumberValidator extends Validator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VisaNumberValidator.class);
	
	/** 
	 * Validations for visa number
	 * 1. Should be exactly 8 characters
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String visaNumber = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			visaNumber = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(visaNumber.length() < VLPServiceConstants.VISA_NUMBER_MIN_LEN
				|| visaNumber.length() > VLPServiceConstants.VISA_NUMBER_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.VISA_NUMBER_LEN_ERROR_MESSAGE);
		}
		
	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
