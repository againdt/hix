package com.getinsured.eligibility.at.ref.service;


import com.getinsured.eligibility.enums.ApplicationSource;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import org.springframework.transaction.annotation.Transactional;

@Component("referralActivationNotificationService")
public class ReferralActivationNotificationServiceImpl implements ReferralActivationNotificationService{

	private static final Logger LOGGER = Logger.getLogger(ReferralActivationNotificationServiceImpl.class);
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Autowired
	private ReferralAccountActivationService referralAccountActivationService;

	@Autowired
	private ReferralQENotificationService referralQENotificationService;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Override
	public String triggerQEAutoLinkingAccountActivation(long ssapAppId) throws Exception {
		return referralQENotificationService.generateQEWithAutoLinkingNotice(ssapAppId);
	}
	
	@Override
	public String triggerOEAutoLinkingAccountActivation(long ssapAppId) throws Exception{
		return referralQENotificationService.generateOEWithAutoLinkingNotice(ssapAppId);
	}
	
	@Override
	public void triggerQEWithoutAutoLinkingAccountActivation(SsapApplication ssapApplication) throws Exception {
		AccountTransferRequestDTO accountTransferRequest = new AccountTransferRequestDTO();

		final SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());

		accountTransferRequest.setNotificationType(ReferralConstants.WITH_LINK);
		accountTransferRequest.setLCE(false);
		accountTransferRequest.setQE(true);
		accountTransferRequest.setAccountMigration(ssapApplication.getSource() != null && ssapApplication.getSource().equals(ApplicationSource.DATA_MIGRATION_AT.getApplicationSourceCode()));

		referralAccountActivationService.triggerAccountActivation(singleStreamlinedApplication, ssapApplication, accountTransferRequest);
	}
	
	@Override
	public void triggerOEAccountActivation(SsapApplication ssapApplication) throws Exception {
		AccountTransferRequestDTO accountTransferRequest = new AccountTransferRequestDTO();

		final SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());

		accountTransferRequest.setNotificationType(ReferralConstants.WITH_LINK);
		accountTransferRequest.setLCE(false);
		accountTransferRequest.setQE(false);
		accountTransferRequest.setAccountMigration(ssapApplication.getSource() != null && ssapApplication.getSource().equals(ApplicationSource.DATA_MIGRATION_AT.getApplicationSourceCode()));

		referralAccountActivationService.triggerAccountActivation(singleStreamlinedApplication, ssapApplication, accountTransferRequest);
	}
	
	@Override
	@Transactional
	public void initiateActivation(Long applicationId) {
		try {
			SsapApplication application = ssapApplicationRepository.findOne(applicationId);
			application.setSsapApplicants(ssapApplicantRepository.findBySsapApplication(application));

			if ("OE".equalsIgnoreCase(application.getApplicationType())) {
				triggerOEAccountActivation(application);
			} else if ("QEP".equalsIgnoreCase(application.getApplicationType())) {
				triggerQEWithoutAutoLinkingAccountActivation(application);
			}
		}catch(Exception ex) {
			LOGGER.error("error calling initiateActivation for applicationId " + applicationId, ex);
		}
	}
}
