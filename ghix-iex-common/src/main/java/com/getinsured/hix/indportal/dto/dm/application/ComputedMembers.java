package com.getinsured.hix.indportal.dto.dm.application;

import java.util.Map;

public class ComputedMembers {
	private Map<String, AttestationsMember> members;

	public Map<String, AttestationsMember> getMembers() {
		return members;
	}

	public void setMembers(Map<String, AttestationsMember> members) {
		this.members = members;
	}
	 
	}
