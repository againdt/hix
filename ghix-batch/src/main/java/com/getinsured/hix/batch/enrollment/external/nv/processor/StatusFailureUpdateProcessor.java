package com.getinsured.hix.batch.enrollment.external.nv.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrlPopulateExtEnrollmentService;

public class StatusFailureUpdateProcessor implements Tasklet {
	private static final Logger LOGGER = LoggerFactory.getLogger(StatusFailureUpdateProcessor.class);
	private EnrlPopulateExtEnrollmentService enrlPopulateExtEnrollmentService;
	
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		Long jobId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
		Integer fileId = (Integer) chunkContext.getStepContext().getJobExecutionContext().get("fileId");
		enrlPopulateExtEnrollmentService.updateSummery(fileId, "FAILED");
		LOGGER.debug("-----------------job updated FAILED---------"+jobId);
		return null;
	}

	public void setEnrlPopulateExtEnrollmentService(EnrlPopulateExtEnrollmentService enrlPopulateExtEnrollmentService) {
		this.enrlPopulateExtEnrollmentService = enrlPopulateExtEnrollmentService;
	}
	 
}
