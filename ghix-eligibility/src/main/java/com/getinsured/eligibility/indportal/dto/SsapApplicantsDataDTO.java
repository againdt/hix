package com.getinsured.eligibility.indportal.dto;

public class SsapApplicantsDataDTO {

	private Long id;
	private String guid;
	private Long personId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}	
}
