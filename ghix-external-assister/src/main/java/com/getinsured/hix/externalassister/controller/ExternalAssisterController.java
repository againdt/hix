package com.getinsured.hix.externalassister.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.externalassister.DesignateAssisterDTO;
import com.getinsured.hix.dto.externalassister.DesignateAssisterRequest;
import com.getinsured.hix.dto.externalassister.DesignateAssisterResponse;
import com.getinsured.hix.externalassister.service.ExternalAssisterService;
import com.getinsured.hix.externalassister.util.ExternalBrokerConstants;
import com.getinsured.hix.externalassister.util.ExternalBrokerUtils;
import com.getinsured.hix.platform.util.GhixConstants;
import com.google.gson.Gson;

import static com.getinsured.hix.externalassister.util.ExternalBrokerUtils.isNotNullAndEmpty;


@Controller
@RequestMapping(value = "/externalassister")
public class ExternalAssisterController {
	
	private static final Logger LOGGER = Logger.getLogger(ExternalAssisterController.class);
	
	private Gson platformGson;
	private ExternalAssisterService externalAssisterService;
	
	@Autowired 
	public ExternalAssisterController(Gson platformGson, ExternalAssisterService externalAssisterService) {
		this.platformGson = platformGson;
		this.externalAssisterService = externalAssisterService;
	}

	/**
	 * Jira Id: HIX-110523
	 * Api to designat,redesignate and dedesignate the external assister
	 * @author Anoop
	 * @since 24th Jan 2019
	 * @param designateAssisterRequest
	 * @return designateAssisterResponse
	 */
	@RequestMapping(value = "/designateassister", method = RequestMethod.POST)
	@ResponseBody
	public String designateAssister(@RequestBody String requestData) {
		DesignateAssisterResponse designateAssisterResponse = new DesignateAssisterResponse();
		LOGGER.info("=========== started designate Assister Service ========");
		if (StringUtils.isNotEmpty(requestData)) {
			LOGGER.info("designateAssister Request :: "  + requestData);
			try {
				DesignateAssisterRequest designateAssisterRequest = platformGson.fromJson(requestData, DesignateAssisterRequest.class);
				if (designateAssisterRequest != null ) {
					designateAssisterResponse = externalAssisterService.designator(designateAssisterRequest);

				} else {
					designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_201);
					designateAssisterResponse.setErrMsg(ExternalBrokerConstants.ERR_MSG_REQUEST_IS_NULL);
					designateAssisterResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			} catch (Exception ex) {
				LOGGER.error("Exception occurred in designating API: ", ex);
				designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_203);
				designateAssisterResponse.setErrMsg(ExternalBrokerConstants.ERR_MSG_IN_DESIGNATING_BROKER_API + " :: "
						+ ExternalBrokerUtils.shortenedStackTrace(ex, 4));
				designateAssisterResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		} else {
			LOGGER.info("Received Null or Empty request for designate assister API");
			designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_202);
			designateAssisterResponse.setErrMsg(ExternalBrokerConstants.ERR_MSG_REQUEST_IS_NULL);
			designateAssisterResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(designateAssisterResponse);
	}
	
	/**
	 * Jira Id: HIX-110523
	 * Api to get the external assister detail for the household id
	 * @author Anoop
	 * @since 24th Jan 2019
	 * @param designateAssisterRequest
	 * @return designateAssisterResponse
	 */
	@RequestMapping(value = "/getassisterdetailforhouseholdid/{householdId}", method = RequestMethod.GET)
	@ResponseBody
	public String getAssisterDetailForhouseholdId(@PathVariable("householdId") int householdId) {
		DesignateAssisterResponse designateAssisterResponse = new DesignateAssisterResponse();
		LOGGER.info("=========== started get Assister Detail for HouseholdId Service ========");
		if (isNotNullAndEmpty(householdId)) {
			LOGGER.info("Assister details for the household request :: "  + householdId);
			try {
					designateAssisterResponse = externalAssisterService.getAssisterDetailForhouseholdId(householdId);
					designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_200);
					designateAssisterResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			} catch (Exception ex) {
				LOGGER.error("Exception occurred in getAssisterDetailForhouseholdId API: ", ex);
				designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_205);
				designateAssisterResponse.setErrMsg(ExternalBrokerConstants.ERR_MSG_IN_GET_ASSISTER_DETAIL + " :: "
						+ ExternalBrokerUtils.shortenedStackTrace(ex, 4));
				designateAssisterResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		} else {
			LOGGER.info("Received Null or Empty request for getAssisterdetailforhouseholdid");
			designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_202);
			designateAssisterResponse.setErrMsg(ExternalBrokerConstants.ERR_MSG_REQUEST_IS_NULL);
			designateAssisterResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(designateAssisterResponse);
	}
	
	/**
	 * Jira Id: HIX-110523
	 * Api to get the households detail for the external broker id and MN user id
	 * @author Anoop
	 * @since 24th Jan 2019
	 * @param designateAssisterRequest
	 * @return designateAssisterResponse
	 */
	@RequestMapping(value = "/householdsforbroker", method = RequestMethod.POST)
	@ResponseBody
	public String householdsForBroker(@RequestBody String requestData) {
		DesignateAssisterDTO designateAssisterDTO = new DesignateAssisterDTO();
		LOGGER.info("=========== started householdsforbroker Service ========");
		if (StringUtils.isNotEmpty(requestData)) {
			LOGGER.info("households for the assister request :: "  + requestData);
			try {
				DesignateAssisterRequest designateAssisterRequest = platformGson.fromJson(requestData, DesignateAssisterRequest.class);
				if (designateAssisterRequest != null ) {
					designateAssisterDTO.setDesignateAssisterResponse(externalAssisterService.householdsForBroker(designateAssisterRequest));
					designateAssisterDTO.setErrCode(ExternalBrokerConstants.ERROR_CODE_200);
					designateAssisterDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);

				} else {
					designateAssisterDTO.setErrCode(ExternalBrokerConstants.ERROR_CODE_206);
					designateAssisterDTO.setErrMsg(ExternalBrokerConstants.ERR_MSG_REQUEST_IS_NULL);
					designateAssisterDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			} catch (Exception ex) {
				LOGGER.error("Exception occurred in households for the external assister API: ", ex);
				designateAssisterDTO.setErrCode(ExternalBrokerConstants.ERROR_CODE_207);
				designateAssisterDTO.setErrMsg(ExternalBrokerConstants.ERR_MSG_IN_HOUSEHOLDS_FOR_ASSISTER + " :: "
						+ ExternalBrokerUtils.shortenedStackTrace(ex, 4));
				designateAssisterDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		} else {
			LOGGER.info("Received Null or Empty request for households for the external assister request ");
			designateAssisterDTO.setErrCode(ExternalBrokerConstants.ERROR_CODE_202);
			designateAssisterDTO.setErrMsg(ExternalBrokerConstants.ERR_MSG_REQUEST_IS_NULL);
			designateAssisterDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(designateAssisterDTO);
	}
	
	/**
	 * Jira Id: HIX-110523
     * Api to get the external assister detail for the household id
	 * @author Anoop
	 * @since 24th Jan 2019
	 * @param designateAssisterRequest
	 * @return designateAssisterResponse
	 */
	@RequestMapping(value = "/getexternalassisterdetail/{externalassisterid}", method = RequestMethod.GET)
	@ResponseBody
	public String getExternalAssisterDetail(@PathVariable("externalassisterid") String externalassisterid) {
		DesignateAssisterResponse designateAssisterResponse = new DesignateAssisterResponse();
		LOGGER.info("=========== started get Assister Detail for HouseholdId Service ========");
		if (isNotNullAndEmpty(externalassisterid)) {
			LOGGER.info("Get External Assister detail request :: "  + externalassisterid);
			try {
				designateAssisterResponse = externalAssisterService.getAssisterDetails(externalassisterid);
				designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_200);
				designateAssisterResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			} catch (Exception ex) {
				LOGGER.error("Exception occurred in get external assister details API: ", ex);
				designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_205);
				designateAssisterResponse.setErrMsg(ExternalBrokerConstants.ERR_MSG_IN_GET_ASSISTER_DETAIL + " :: "
						+ ExternalBrokerUtils.shortenedStackTrace(ex, 4));
				designateAssisterResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		} else {
			LOGGER.info("Received Null or Empty request for get external assister details API");
			designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_202);
			designateAssisterResponse.setErrMsg(ExternalBrokerConstants.ERR_MSG_REQUEST_IS_NULL);
			designateAssisterResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return platformGson.toJson(designateAssisterResponse);
	}
	
	@RequestMapping(value = "/householdsforbroker/{externalassisterid}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity householdsForBrokerId(@PathVariable("externalassisterid") String externalAssisterId) {
		LOGGER.info("=========== started householdsforbroker Service ========");
		if (StringUtils.isNotEmpty(externalAssisterId)) {
			LOGGER.info("households for the assister request :: "  + externalAssisterId);
			try {
				if (externalAssisterId != null && !externalAssisterId.isEmpty() ) {
					String responseString=externalAssisterService.getExternalUserIdsForBroker(externalAssisterId);
					return new ResponseEntity(responseString,HttpStatus.OK);
					
				} else {
					return new ResponseEntity(externalAssisterId,HttpStatus.NOT_FOUND);
				}
			} catch (Exception ex) {
				LOGGER.error("Exception occurred in households for the broker external assister API: ", ex);
				return new ResponseEntity(externalAssisterId,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			LOGGER.info("Received Null or Empty request for households for the external assister request ");
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
	}

}
