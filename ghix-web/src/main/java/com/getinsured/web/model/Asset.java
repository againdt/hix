package com.getinsured.web.model;

/**
 * Describes assets for current environment.
 * @author Yevgen Golubenko
 * @since 2/11/19
 */
public class Asset
{
  private AssetType type;
  private String value;

  public AssetType getType()
  {
    return type;
  }

  public void setType(final AssetType type)
  {
    this.type = type;
  }

  public String getValue()
  {
    return value;
  }

  public void setValue(final String value)
  {
    this.value = value;
  }
}
