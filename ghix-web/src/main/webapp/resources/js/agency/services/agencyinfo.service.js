(function(){
    'use strict';

    angular.module('agencyApp')
    .service('AgencyInfoService', AgencyInfoService);

    AgencyInfoService.$inject = ['$state'];
    function AgencyInfoService($state) {
        var service = this;        
        service.editPage = function () {
            $state.go ("accsetup.agencyinfo", {agencyId: agencyId});
        };
        service.cancelEdit = function () {
            $state.go ("accsetup.agencyinfoview", {agencyId: agencyId});
        };
    }


})();