package com.getinsured.eligibility.at.ref.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.service.SsapApplicantService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.resp.si.handler.helper.ApplicantEligibilityHelper;
import com.getinsured.eligibility.at.resp.si.handler.helper.NativeAmericanEligibilityHelper;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.repository.IEligibilityProgram;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.util.TSDate;

@Component("financialApplicationConversionService")
public class FinancialApplicationConversionServiceImpl implements FinancialApplicationConversionService {

	private static final Logger LOGGER = Logger.getLogger(FinancialApplicationConversionServiceImpl.class);

	@Autowired
	private SsapApplicantService ssapApplicantService;

	@Autowired
	private NativeAmericanEligibilityHelper nativeAmericanEligibilityHelper;

	@Autowired
	private IEligibilityProgram eligibilityProgramRepository;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
    @Autowired
    private ApplicantEligibilityHelper applicantEligibilityHelper;

    @Autowired
	private EntityManagerFactory emf;
    
	private static final String Y = "Y";
	private static final String TRUE = "TRUE";
	private static final String FALSE = "FALSE";
	private static final String ERROR_IN_JSON_STRING = "Json String is not in correct format";
	private static final String ONLINE_APPLICATION = "ON";
	private static final String CONVERSION = "CN";

	@Value("#{configProp['nativeAmerican.default.cslevel'] != null ? configProp['nativeAmerican.default.cslevel'] : 'CS3'}")
	public String nativeAmericanHouseholdCSLevel;

	@Override
	public void convertFinancialAppToNonFinancial(SsapApplication currentApplication, SsapApplication enrolledApplication) {
		convertToNonFinancialApplication(currentApplication, enrolledApplication, new HashMap<String, ApplicantEvent>(), true);
	}

	@Override
	public void convertFinancialAppToNonFinancial(SsapApplication currentApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap) {
		LOGGER.info("Converting From Financial to Non financial Application for " + currentApplication.getId());
		convertToNonFinancialApplication(currentApplication, enrolledApplication, applicantEventMap, false);
	}

	private void convertToNonFinancialApplication(SsapApplication currentApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap, boolean isRenewal) {
		modifyApplicantAndApplicationDetails(currentApplication, enrolledApplication, isRenewal, applicantEventMap);
		currentApplication.setApplicationData(modifyJsonForNonFinancialConversion(currentApplication, enrolledApplication, applicantEventMap));
		// calculate AI/AN
		String hhNativeAmerican = nativeAmericanEligibilityHelper.process(currentApplication.getCaseNumber());
		currentApplication.setNativeAmerican(hhNativeAmerican);
		if (ReferralConstants.Y.equals(currentApplication.getNativeAmerican())) {
			currentApplication.setCsrLevel(nativeAmericanHouseholdCSLevel);
		} else {
			currentApplication.setCsrLevel(null);
		}
		updateSsapApplication(currentApplication);

	}

	private String modifyJsonForNonFinancialConversion(SsapApplication currentSsapApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap) {
		String applicationData = currentSsapApplication.getApplicationData();
		try {
			SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(applicationData);
			singleStreamlinedApplication.setApplyingForFinancialAssistanceIndicator(false);
			modifySsapApplicants(singleStreamlinedApplication, currentSsapApplication, enrolledApplication, applicantEventMap);
			applicationData = ssapJsonBuilder.transformToJson(singleStreamlinedApplication);
		} catch (JSONException jsonException) {
			LOGGER.error(ERROR_IN_JSON_STRING, jsonException);
			throw new GIRuntimeException(jsonException);
		}
		return applicationData;
	}

	private void modifySsapApplicants(SingleStreamlinedApplication singleStreamlinedApplication, SsapApplication currentSsapApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap) throws JSONException {
		TaxHousehold taxhousehold = singleStreamlinedApplication.getTaxHousehold().get(0);
		List<HouseholdMember> taxhouseholdMembers = taxhousehold.getHouseholdMember();
		SsapApplicant ssapApplicant;
		String applicantGuid;
		for (HouseholdMember householdMember : taxhouseholdMembers) {

			applicantGuid = householdMember.getApplicantGuid();
			ssapApplicant = ReferralUtil.retreiveApplicantbasedonGuid(currentSsapApplication, applicantGuid);
			if (ssapApplicant != null && Y.equalsIgnoreCase(ssapApplicant.getOnApplication()) && checkApplicantIsPreviouslyEnrolled(enrolledApplication, ssapApplicant) && !checkForNonQHPEvents(ssapApplicant, applicantEventMap)) {
				householdMember.setUnder26Indicator(true);
				householdMember.setApplyingForCoverageIndicator(true);
			} else {
				householdMember.setUnder26Indicator(false);
				householdMember.setApplyingForCoverageIndicator(false);
			}
		}
	}

	private void modifyApplicantAndApplicationDetails(SsapApplication currentApplication, SsapApplication enrolledApplication, boolean isRenewal, Map<String, ApplicantEvent> applicantEventMap) {
		if (isRenewal) {
			currentApplication.setSource(CONVERSION);
		} else {
			currentApplication.setSource(ONLINE_APPLICATION);
		}
		currentApplication.setFinancialAssistanceFlag("N");
		currentApplication.setEligibilityStatus(EligibilityStatus.AE);
		currentApplication.setExchangeEligibilityStatus(ExchangeEligibilityStatus.QHP);
		currentApplication.setMaximumAPTC(null);
		currentApplication.setCsrLevel(null);
        List<EligibilityProgram> eligibilityProgramList = new ArrayList<EligibilityProgram>();
		for (SsapApplicant currentSsapApplicant : currentApplication.getSsapApplicants()) {
			if (Y.equalsIgnoreCase(currentSsapApplicant.getOnApplication()) && checkApplicantIsPreviouslyEnrolled(enrolledApplication, currentSsapApplicant) && !checkForNonQHPEvents(currentSsapApplicant, applicantEventMap)) {
				currentSsapApplicant.setEligibilityStatus(ExchangeEligibilityStatus.QHP.name());
				EligibilityProgram eligibilityProgram = createOrUpdateProgramEligibilityForQhp(currentSsapApplicant, TRUE);
				eligibilityProgramList.add(eligibilityProgram);

			} else {
				currentSsapApplicant.setEligibilityStatus(ExchangeEligibilityStatus.NONE.name());
				EligibilityProgram eligibilityProgram = createOrUpdateProgramEligibilityForQhp(currentSsapApplicant, FALSE);
				eligibilityProgramList.add(eligibilityProgram);
			}

			currentSsapApplicant.setCsrLevel(null);

		}
		applicantEligibilityHelper.setSsapApplicationStartDate(currentApplication, eligibilityProgramList);
	}

	private boolean checkApplicantIsPreviouslyEnrolled(SsapApplication enrolledApplication, SsapApplicant ssapApplicant) {

		boolean previouslyenrolled = false;
		if (enrolledApplication == null) {
			return previouslyenrolled;
		}
		SsapApplicant applicant = ReferralUtil.retreiveApplicantbasedonGuid(enrolledApplication, ssapApplicant.getApplicantGuid());
		if (applicant != null && ExchangeEligibilityStatus.QHP.name().equalsIgnoreCase(applicant.getEligibilityStatus())) {
			previouslyenrolled = true;
		}
		return previouslyenrolled;
	}

	private EligibilityProgram createOrUpdateProgramEligibilityForQhp(SsapApplicant ssapApplicant, String indicatorVal) {
		List<EligibilityProgram> eligibilityProgramList = eligibilityProgramRepository.getApplicantEligibilities(ssapApplicant.getId());
		EligibilityProgram eligibilityProgramDO = null;
		if(eligibilityProgramList != null && eligibilityProgramList.size()>0){
			for (EligibilityProgram eligibilityProgram : eligibilityProgramList) {
				if(eligibilityProgram.getEligibilityType().equalsIgnoreCase(EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE)){
					eligibilityProgramDO = eligibilityProgram;
					break;
				}
			}
		}
		if(eligibilityProgramDO == null){
			Date currentDate = new TSDate();
			eligibilityProgramDO = new EligibilityProgram();
			eligibilityProgramDO.setEligibilityType(EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE);
			eligibilityProgramDO.setEligibilityStartDate(currentDate);
			DateTime date = TSDateTime.getInstance().withTime(23, 59, 59, 999);
			eligibilityProgramDO.setEligibilityEndDate(date.toDate());
			eligibilityProgramDO.setEligibilityDeterminationDate(currentDate);
			eligibilityProgramDO.setSsapApplicant(ssapApplicant);	
			eligibilityProgramDO.setEligibilityIndicator(indicatorVal);
			eligibilityProgramRepository.save(eligibilityProgramDO);
		}
		else
		{
			eligibilityProgramDO.setEligibilityIndicator(indicatorVal);
			updateEligibilityProgram(eligibilityProgramDO);
		}
		return eligibilityProgramDO;
	}

	private void updateEligibilityProgram(EligibilityProgram eligibilityProgramDO){
		EntityManager em = null;

		try {
			em = emf.createEntityManager();
			if(!em.getTransaction().isActive()){
				em.getTransaction().begin();
			}
			
			Query query = em
					.createNativeQuery("UPDATE PROGRAM_ELIGIBILITIES SET ELIGIBILITY_INDICATOR = ? WHERE ID = ?");
			
			query.setParameter(1, eligibilityProgramDO.getEligibilityIndicator());
			query.setParameter(2, eligibilityProgramDO.getId());
			query.executeUpdate();
			em.getTransaction().commit();
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while updating EligibilityProgram", exception);
			throw new GIRuntimeException("Exception occurred while updating EligibilityProgram", exception);
		} finally {
			if(em != null && em.isOpen()){
				em.close();
			}
		}
	}
	
	private void updateSsapApplication(SsapApplication ssapApplication) {
		ssapApplicationRepository.save(ssapApplication);
	}

	private boolean checkForNonQHPEvents(SsapApplicant currentSsapApplicant, Map<String, ApplicantEvent> applicantEventMap) {
		if (applicantEventMap.containsKey(currentSsapApplicant.getApplicantGuid())) {
			return true;
		}
		return false;
	}
}
