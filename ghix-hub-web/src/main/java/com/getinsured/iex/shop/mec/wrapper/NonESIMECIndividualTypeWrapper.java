package com.getinsured.iex.shop.mec.wrapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.DatePeriodType;
import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.NonESIMECIndividualType;
import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.ObjectFactory;
import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.OrganizationCodeSimpleType;
import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.PersonNameType;

public class NonESIMECIndividualTypeWrapper {
	
	private NonESIMECIndividualType nonESIMECIndividualType;
	
	private PersonNameType personName;
    private DatePeriodType requestedInsurancePeriod;
    private OrganizationCodeSimpleType organizationCodeType;
	
    private String personSSN;
	private String personDateOfBirth;
    private String personSexCode;
    private String firstName;
    private String middleName;
    private String lastName;
    private String nameSuffix;
    private String coverageStartDate;
    private String coverageEndDate;
    private String organizationCode;
    
    public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) throws HubServiceException{
		try{
			organizationCodeType = OrganizationCodeSimpleType.fromValue(organizationCode);
			this.organizationCode = organizationCode;
		}
		catch(IllegalArgumentException e){
			throw new HubServiceException("Invalid Organization Code" , e);
		}
		
	}
	
	public String getPersonSSN() {
		return personSSN;
	}

	public void setPersonSSN(String personSSN) {
		this.personSSN = personSSN;
		nonESIMECIndividualType.setPersonSSN(personSSN);
	}

	public String getPersonDateOfBirth() {
		return personDateOfBirth;
	}

	public void setPersonDateOfBirth(String personDateOfBirth) throws HubServiceException {
		this.personDateOfBirth = personDateOfBirth;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			sdf.setLenient(false);
			java.util.Date javaDate = sdf.parse(personDateOfBirth);

			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(javaDate);
			XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH)+1, gc.get(Calendar.DAY_OF_MONTH),DatatypeConstants.FIELD_UNDEFINED);
			xmlGregorianCalendar.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
			nonESIMECIndividualType.setPersonDateOfBirth(xmlGregorianCalendar);
		} catch (ParseException | DatatypeConfigurationException e) {
			throw new HubServiceException(e.getMessage(), e);
		}
	}

	public String getPersonSexCode() {
		return personSexCode;
	}

	public void setPersonSexCode(String personSexCode) {
		this.personSexCode = personSexCode;
		nonESIMECIndividualType.setPersonSexCode(personSexCode);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
		personName.setFirstName(firstName);
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
		personName.setMiddleName(middleName);
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
		personName.setLastName(lastName);
	}

	public String getNameSuffix() {
		return nameSuffix;
	}

	public void setNameSuffix(String nameSuffix) {
		this.nameSuffix = nameSuffix;
		personName.setNameSuffix(nameSuffix);
	}

	public String getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(String coverageStartDate) throws HubServiceException {
		this.coverageStartDate = coverageStartDate;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			sdf.setLenient(false);
			java.util.Date javaDate = sdf.parse(coverageStartDate);

			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(javaDate);
			XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH)+1, gc.get(Calendar.DAY_OF_MONTH),DatatypeConstants.FIELD_UNDEFINED);
			xmlGregorianCalendar.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
			requestedInsurancePeriod.setStartDate(xmlGregorianCalendar);
			
		} catch (ParseException | DatatypeConfigurationException e) {
			throw new HubServiceException(e.getMessage(), e);
		}
	}

	public String getCoverageEndDate() {
		return coverageEndDate;
	}

	public void setCoverageEndDate(String coverageEndDate) throws HubServiceException {
		this.coverageEndDate = coverageEndDate;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			sdf.setLenient(false);
			java.util.Date javaDate = sdf.parse(coverageEndDate);

			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(javaDate);
			XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH)+1, gc.get(Calendar.DAY_OF_MONTH),DatatypeConstants.FIELD_UNDEFINED);
			xmlGregorianCalendar.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
			requestedInsurancePeriod.setEndDate(xmlGregorianCalendar);
		} catch (ParseException | DatatypeConfigurationException e) {
			throw new HubServiceException(e.getMessage(), e);
		}
	}

	public NonESIMECIndividualTypeWrapper()
	{
		ObjectFactory objectFactory = new ObjectFactory();
		nonESIMECIndividualType = objectFactory.createNonESIMECIndividualType();
		personName = objectFactory.createPersonNameType();
	    requestedInsurancePeriod = objectFactory.createDatePeriodType();
	}
	
	public NonESIMECIndividualType getSystemRepresentation()
	{
		nonESIMECIndividualType.setPersonName(personName);
		nonESIMECIndividualType.setRequestedInsurancePeriod(requestedInsurancePeriod);
		nonESIMECIndividualType.setOrganizationCode(organizationCodeType);
		return nonESIMECIndividualType;
	}
}
