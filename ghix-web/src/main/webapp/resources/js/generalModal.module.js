(function(){
	'use strict';
	var app = angular.module("generalModalModule", []);

	app.directive('generalModalDirective', generalModalDirective);
	generalModalDirective.$inject = ['$location'];

	function generalModalDirective(){
		var directive = {
			restrict: 'AE',
			transclude: true,
			templateUrl: '/hix/resources/html/generalModal.temp.html',
			controller: ModalCtrl,
			controllerAs: 'vm',
			bindToController: true,
			scope:{
				modalId: '=',
				modalHeaderContent: '=',
				submitContent: '=',
				cancelContent: '='
			},
			link: function(scope, element, attrs){

			}
		}

		return directive;
	}

	function ModalCtrl($scope, $location){
		var vm = this;

		vm.clicked = function() {
			$('#' + $scope.modalId).modal('hide');
			$location.path("/viewagentlist");
		}
	}

})()
