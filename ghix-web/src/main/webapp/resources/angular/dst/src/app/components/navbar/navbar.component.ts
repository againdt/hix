import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  skipLinkPath: string;
  logoUrl: string;

  constructor() { }

  ngOnInit() {
        // console.log(window.location.href);
        // this.skipLinkPath = this.router.url.split('#')[0] + '#main-content';
        this.skipLinkPath = window.location.href + '#main-content';
        // console.log(this.skipLinkPath);

        try {
            this.logoUrl = (<HTMLInputElement>document.querySelector("#jsp_logo")).value;
        } catch (err) {
            this.logoUrl = "";
        }
  }

}
