package com.getinsured.ssap.service;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.platform.security.GhixRestTemplate;

/**
 * Implementation of outbound account transfer.
 *
 * @author Yevgen Golubenko
 * @since 2019-07-10
 */
@Service("outboundAccountTransferService")
public class OutboundAccountTransferServiceImpl implements OutboundAccountTransferService {
  private static final Logger log = LoggerFactory.getLogger(OutboundAccountTransferServiceImpl.class);
  private static String ELIGIBILITY_URL = null;
  private static String OUTBOUND_ACCOUNT_TRANSFER_PATH = "ssapapplication/generateAndPushAT?applicationId=";
  private final Properties configProp;
  private final GhixRestTemplate ghixRestTemplate;

  @Autowired
  public OutboundAccountTransferServiceImpl(final Properties configProp,
                                            final GhixRestTemplate ghixRestTemplate)
  {
    this.configProp = configProp;
    this.ghixRestTemplate = ghixRestTemplate;

    if(ELIGIBILITY_URL == null) {
      ELIGIBILITY_URL = this.configProp.getProperty("ghixEligibilityServiceURL");
      if(ELIGIBILITY_URL == null || "".equals(ELIGIBILITY_URL)) {
        ELIGIBILITY_URL = "http://localhost:8081/ghix-eligibility/";
        log.warn("Unable to find ghixEligibilityServiceURL in configuration.properties, using default: {}", ELIGIBILITY_URL);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void invokeOutboundAccountTransfer(final long applicationId) {
    final String url = String.format("%s%s%d", ELIGIBILITY_URL, OUTBOUND_ACCOUNT_TRANSFER_PATH, applicationId);
    try {
      log.info("Invoking outbound account transfer url: {}", url);
      String response = ghixRestTemplate.getForObject(url, String.class);
      log.info("Outbound account transfer response: {}", response);
    }
    catch(Exception e) {
      log.error("Error occurred while invoking outbound account transfer url: {}, error: {}", url, e.getMessage());
      log.error("Exception details", e);
    }
  }
}
