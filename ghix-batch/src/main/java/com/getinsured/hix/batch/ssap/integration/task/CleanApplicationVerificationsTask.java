package com.getinsured.hix.batch.ssap.integration.task;

import com.getinsured.hix.batch.ssap.service.CleanApplicationVerificationsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.sql.Timestamp;

public class CleanApplicationVerificationsTask extends StepExecutionListenerSupport implements Tasklet {
    private static final Logger LOGGER = LoggerFactory.getLogger(CleanApplicationVerificationsTask.class);
    private static volatile Boolean isBatchRunning = false;
    private String creationDate;
    private CleanApplicationVerificationsService cleanApplicationVerificationsService;

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public CleanApplicationVerificationsService getCleanApplicationVerificationsService() {
        return cleanApplicationVerificationsService;
    }

    public void setCleanApplicationVerificationsService(CleanApplicationVerificationsService cleanApplicationVerificationsService) {
        this.cleanApplicationVerificationsService = cleanApplicationVerificationsService;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        synchronized (isBatchRunning) {
            if(isBatchRunning) {
                return RepeatStatus.FINISHED;
            } else {
                isBatchRunning = true;
            }
        }

        try {
            if(LOGGER.isDebugEnabled()) {
                LOGGER.debug(this.getClass().getName() + " started at " + new Timestamp(System.currentTimeMillis()));
            }

            cleanApplicationVerificationsService.cleanVerificationsAndUpdateApplicationEligibility(creationDate);
        } catch(Exception ex) {
            LOGGER.error("execute::error while executing", ex);
        } finally{
            isBatchRunning = false;
        }

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug(this.getClass().getName() + " finishing at " + new Timestamp(System.currentTimeMillis()));
        }

        return RepeatStatus.FINISHED;
    }
}
