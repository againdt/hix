<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@include file="datasource.jsp" %>
<head>
<base target="main_1">
<style>
table {
	border: 1px solid #666;
	width: 80%;
	margin: 20px 0 20px 0 !important;
	font-size: 14px;
}

th,td {
	padding: 2px 4px 2px 4px !important;
	text-align: left;
	vertical-align: top;	
}

thead tr {
	background-color: #fc0;
}

th.sorted {
	background-color: orange;
}

th a,th a:visited {
	color: black;
}

th a:hover {
	text-decoration: underline;
	color: black;
}

th.sorted a,th.sortable a {
	background-position: right;
	display: block;
	width: 100%;
}

th.sortable a {
	background-image: url(../img/arrow_off.png);
}

th.order1 a {
	background-image: url(../img/arrow_down.png);
}

th.order2 a {
	background-image: url(../img/arrow_up.png);
}

tr.odd {
	background-color: #fff
}

tr.tableRowEven,tr.even {
	background-color: #fea
}

div.exportlinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	margin: 2px 0 10px 0;
	width: 79%;
}

span.export {
	padding: 0 4px 1px 20px;
	display: inline;
	display: inline-block;
	cursor: pointer;
}

span.excel {
	background-image: url(../img/ico_file_excel.png);
}

span.csv {
	background-image: url(../img/ico_file_csv.png);
}

span.xml {
	background-image: url(../img/ico_file_xml.png);
}

span.pdf {
	background-image: url(../img/ico_file_pdf.png);
}

span.rtf {
	background-image: url(../img/ico_file_rtf.png);
}

span.pagebanner {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	margin-top: 10px;
	display: block;
	border-bottom: none;
}

span.pagelinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	display: block;
	border-top: none;
	margin-bottom: -5px;
}


.group-1 {
    font-weight:bold;
    padding-bottom:10px;
    border-top:1px solid black;
}
.group-2 {
    font-style:italic;
    border-top: 1px solid black;

}
.subtotal-sum, .grandtotal-sum {
    font-weight:bold;
    text-align:right;
}
.subtotal-header {
    padding-bottom: 0px;
    border-top: 1px solid white;
}
.subtotal-label, .grandtotal-label {
    border-top: 1px solid white;
    font-weight: bold;
}
.grouped-table tr.even {
    background-color: #fff;
}
.grouped-table tr.odd {
    background-color: #fff;
}
.grandtotal-row {
    border-top: 2px solid black;
}

</style>
    
</head>
<% 
String req_Id=request.getParameter("attachments");
%>
<%
if(null == req_Id || req_Id == "")
  req_Id="0";
%>
Attachments for ID: <%= req_Id%>

<sql:query dataSource="${jspDataSource}" var="result1">
select serff_doc_id, ecm_doc_id, document_name, document_type, document_size,  
'<a href="getBottomRight?attachment='||ecm_doc_id||'&'||'type='||document_type||'" target="main_1">Show</a>' as attachment, 
to_char(CREATION_TIMESTAMP,'DD-MON-YY HH24:MM:SS') as createdon
from serff_document
where serff_req_id = <%= req_Id%>
order by serff_doc_id
</sql:query>

<display:table name="${result1.rows}" id="table1" style="white-space: nowrap;">
<display:column property="serff_doc_id" title="ID" sortable="false" />
 <display:column property="document_name" title="Doc Name" sortable="false" />
 <display:column property="document_type" title="Type" sortable="false" />
 <display:column property="attachment" title="Attachment" sortable="false" />
 <display:column property="document_size" title="Size" sortable="false" />
 <display:column property="createdon" title="Created On" sortable="false" />
  
</display:table>

