<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript" src="<c:url value="/resources/js/dashboardAnnouncement/angular-ui-router.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/dashboardAnnouncement/ckeditor/ckeditor.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/dashboardAnnouncement/ng-ckeditor-1.0.1.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/angular/mask.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-sanitize.min.js" />"></script>
<script type="text/javascript" src='<c:url value="/resources/js/dashboardAnnouncement/ui-bootstrap-tpls-0.12.0.min.js"/>'></script>
<script type="text/javascript" src="<c:url value="/resources/js/dashboardAnnouncement/dashboardAnnouncement.app.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/dashboardAnnouncement/dashboardAnnouncementList.controller.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/dashboardAnnouncement/dashboardAnnouncement.controller.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/dashboardAnnouncement/datePicker.directive.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/dashboardAnnouncement/endWith.filter.js" />"></script>

<script type="text/javascript">
var csrfToken = {
        getParam: '${df:csrfTokenParameter()}=<df:csrfToken plainToken="true"/>',
        paramName: '${df:csrfTokenParameter()}',
        paramValue: '<df:csrfToken plainToken="true"/>'
    };
</script>

<sec:accesscontrollist hasPermission="IND_PORTAL_CREATE_ANNOUNCEMENTS" domainObject="user">
	<div ng-app="dashboardAnnouncementApp">
	<div ui-view></div>
	</div>
</sec:accesscontrollist>