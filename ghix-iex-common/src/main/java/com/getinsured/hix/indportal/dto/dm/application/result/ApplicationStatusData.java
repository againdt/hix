package com.getinsured.hix.indportal.dto.dm.application.result;

public class ApplicationStatusData {

	private long applicationId;
	
	private String existingApplicationStatus;


	public long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(long applicationId) {
		this.applicationId = applicationId;
	}

	public String getExistingApplicationStatus() {
		return existingApplicationStatus;
	}

	public void setExistingApplicationStatus(String existingApplicationStatus) {
		this.existingApplicationStatus = existingApplicationStatus;
	}
	
	
	
}
