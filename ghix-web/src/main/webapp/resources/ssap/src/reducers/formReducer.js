import { formActionTypes } from '../constants/actionTypes';
import cloneObject from '../utils/cloneObject';

const initialStateEmptyObject = {
    isFormSubmitted: false,
    isEligiblyForSave: true,
    isFormOnTopLevelValid: true,
    areAllInputsValid: true,
    registeredInputs: []
};

const initialState = {
    Form: {
        ...cloneObject(initialStateEmptyObject)
    },
    FormForModal: {
        ...cloneObject(initialStateEmptyObject)
    }
};

export default (state = initialState, action) => {
    switch(action.type) {
    case formActionTypes.REMOVE_FORM:
        return {
            ...state,
            [action.formName]: {
                ...cloneObject(initialStateEmptyObject)
            }
        };
    case formActionTypes.SUBMIT_FORM:
        return {
            ...state,
            [action.formName]: {
                ...state[action.formName],
                isFormSubmitted: true
            }
        };
    case formActionTypes.CLEAR_SUBMISSION:
        return {
            ...state,
            [action.formName]: {
                ...state[action.formName],
                isFormSubmitted: false
            }
        };

    case formActionTypes.REGISTER_INPUT:
        if(state[action.formName] && !state[action.formName].registeredInputs.includes(action.input)) {
            state[action.formName].registeredInputs.push(action.input);
        }
        return state;

    case formActionTypes.RE_REGISTER_INPUT:
        return {
            ...state,
            [action.formName]: {
                ...state[action.formName],
                registeredInputs: state[action.formName].registeredInputs.map(i => {
                    if(i.inputName === action.input.inputName) {
                        return action.input;
                    } else {
                        return i;
                    }
                })
            }
        };

    case formActionTypes.REMOVE_INPUT:
        return {
            ...state,
            [action.formName]: {
                ...state[action.formName],
                registeredInputs: state[action.formName] && state[action.formName].registeredInputs.filter(input => input.inputId !== action.input.inputId)
            }
        };
    case formActionTypes.CHANGE_INPUT_STATUS:
        return {
            ...state,
            [action.formName]: {
                ...state[action.formName],
                registeredInputs: state[action.formName] && state[action.formName].registeredInputs.map(
                    input => input.inputId === action.input.inputId ?
                        {
                            ...input,
                            ...action.input,
                            isInputTouched: input.isInputTouched !== null
                        } : input
                )
            }
        };
    case formActionTypes.VALIDATE_FORM:
        return {
            ...state,
            [action.formName]: {
                ...state[action.formName],
                ...action.payload
            }
        };
    case formActionTypes.CLEAR_STATE:
        return initialState;
    default: return state;
    }
};