package com.getinsured.hix.finance.controller;

import java.util.HashMap;
import java.util.List;

import org.opensaml.xml.signature.SignatureConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.dto.planmgmt.IssuerPaymentInfoResponse;
import com.getinsured.saml.payment.IssuerConfig;
import com.getinsured.saml.payment.OverrideAttribute;

public class IssuerPaymentConfiguration {
	private String issuerHiosId;
	private IssuerPaymentInfoResponse paymentInfo;
	private IssuerConfig issuerConfig;
	private Logger logger = LoggerFactory.getLogger(getClass());
	private String relayState = null;
	private long lastUpdateTime ;
	private boolean samlSupported;
	private static final String DEFAULT_SIGNATURE_ALGO="RSAwithSHA256";
	private static final String SHA1_SIGNATURE_ALGO="RSAwithSHA1";
	
	public IssuerPaymentConfiguration(String issuerHiosId, IssuerPaymentInfoResponse paymentInfo, IssuerConfig issuerConfig) {
		this.issuerHiosId = issuerHiosId;
		this.paymentInfo = paymentInfo;
		this.issuerConfig = issuerConfig;
		this.lastUpdateTime = System.currentTimeMillis();
	}
	
	public String getEntityId() {
		return this.issuerConfig.getEntityId();
	}
	
	public String getIssuerAuthDNS() {
		return this.paymentInfo.getSecurityDnsName();
	}
	
	public String getIssuerAuthAddress() {
		return this.paymentInfo.getSecurityAddress();
	}
	
	public String getIssuerHiosId() {
		return issuerHiosId;
	}
	public void setIssuerHiosId(String issuerHiosId) {
		this.issuerHiosId = issuerHiosId;
	}
	public IssuerPaymentInfoResponse getPaymentInfo() {
		return paymentInfo;
	}
	public void setPaymentInfo(IssuerPaymentInfoResponse paymentInfo) {
		this.paymentInfo = paymentInfo;
	}
	public IssuerConfig getIssuerConfig() {
		return issuerConfig;
	}
	public void setIssuerConfig(IssuerConfig issuerConfig) {
		this.issuerConfig = issuerConfig;
	}
	
	public boolean wantsEncryptedAssertions() {
		if(this.issuerConfig.getEnableEncryption().equalsIgnoreCase("true")) {
			String entityId = this.issuerConfig.getEntityId();
			return entityId != null;
		}
		return false;
	}
	
	public boolean destinationIsHost() {
		return this.issuerConfig.getUseHostAsDestination().equalsIgnoreCase("true");
	}
	
	public boolean useRelayState() {
		String relayState = this.issuerConfig.getUseRelayState();
		if(relayState == null) {
			return false;
		}
		if(relayState.startsWith("http")) {
			this.relayState = relayState.trim();
			return true;
		}
		return false;
	}
	
	
	public String getRelayState() {
		if(this.relayState == null) {
			this.relayState = this.issuerConfig.getUseRelayState();
			if(this.relayState != null && relayState.startsWith("http")) {
				this.relayState = relayState.trim();
			}
		}
		return this.relayState;
	}
	
	public boolean metadataEnabled() {
		return this.issuerConfig.getHaveMetadata().equalsIgnoreCase("true");
	}
	
	public boolean isSamlSupported() {
		String samlDirective = this.issuerConfig.getSamlRedirectSupported();
		if(samlDirective == null) {
			return true;
		}
		return samlDirective.equalsIgnoreCase("true");
	}
	
	/**
	 * 
	 * @param cmsCompliantClaims Payment attributes we have implemented that are CMS Compliant
	 * @return update cmsCompliantClaims overriding the cms Attribute name with payment provider specific attributes
	 */
	public HashMap<String,String> getIssuerSupportedClaims(HashMap<String,String> cmsCompliantClaims){
		if(cmsCompliantClaims == null || cmsCompliantClaims.size() == 0) {
			return cmsCompliantClaims;
		}
		List<OverrideAttribute> overrideAttributes = this.issuerConfig.getOverrideAttributes();
		if(overrideAttributes == null) {
			return cmsCompliantClaims;
		}
		for(OverrideAttribute oAttr: overrideAttributes) {
			String source = oAttr.getSource();
			if(!cmsCompliantClaims.containsKey(source)) {
				continue;
			}
			String target  = oAttr.getTarget();
			logger.info("Overriding CMS provided attribute \"{}\" with \"{}\" for HIOSID {}", source, target, this.issuerHiosId);
			String val = cmsCompliantClaims.remove(source);
			if(val != null) {
				cmsCompliantClaims.put(target, val);
			}
		}
		return cmsCompliantClaims;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((issuerHiosId == null) ? 0 : issuerHiosId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IssuerPaymentConfiguration other = (IssuerPaymentConfiguration) obj;
		if (issuerHiosId == null) {
			if (other.issuerHiosId != null)
				return false;
		} else if (!issuerHiosId.equals(other.issuerHiosId))
			return false;
		return true;
	}

	public boolean isStale() {
		long now = System.currentTimeMillis();
		if((now - this.lastUpdateTime) > 5*60*1000) {
			return true;
		}
		return false;
	}

	public String getIssuerAuthURL() {
		return this.paymentInfo.getIssuerAuthURL();
	}

	public boolean getUseHostAsDestination() {
		return this.issuerConfig.getUseHostAsDestination().equalsIgnoreCase("true");
	}
	
	public String getSignatureAlgoId() {
		String signatureAlgoId = this.issuerConfig.getSignatureAlgoId();
		if(signatureAlgoId == null) {
			signatureAlgoId=DEFAULT_SIGNATURE_ALGO;
		}
		if(signatureAlgoId.equals(DEFAULT_SIGNATURE_ALGO)) {
			return SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA256;
		}
		if(signatureAlgoId.equals(SHA1_SIGNATURE_ALGO)){
			return SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA1;
		}
		throw new RuntimeException("Signature algorithm "+signatureAlgoId+" not understood");
	}
	
	public String getDigestAlgoId() {
		String signatureAlgoId = this.issuerConfig.getSignatureAlgoId();
		if(signatureAlgoId == null) {
			signatureAlgoId=DEFAULT_SIGNATURE_ALGO;
		}
		switch(signatureAlgoId){
			case DEFAULT_SIGNATURE_ALGO:{
				return SignatureConstants.ALGO_ID_DIGEST_SHA256;
			}
			case SHA1_SIGNATURE_ALGO:{
				return SignatureConstants.ALGO_ID_DIGEST_SHA1;
			}
			default:
				throw new RuntimeException("Signature algorithm "+signatureAlgoId+" not understood");
		}
	}
}
