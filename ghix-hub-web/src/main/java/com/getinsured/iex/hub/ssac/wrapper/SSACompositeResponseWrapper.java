package com.getinsured.iex.hub.ssac.wrapper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.platform.IEXResponseWrapper;
import com.getinsured.iex.hub.ssac.extension.ResponseMetadataType;
import com.getinsured.iex.hub.ssac.extension.SSACompositeIndividualResponseType;
import com.getinsured.iex.hub.ssac.extension.SSACompositeResponseType;

public class SSACompositeResponseWrapper extends IEXResponseWrapper implements JSONAware {

	private String responseCode;
	private String tdsResponseText;
	private String responseText;
	private List<SSACompositeIndividualResponseWrapper> individualResponses = new ArrayList<SSACompositeIndividualResponseWrapper>();
	private ResponseMetadataWrapper metadataWrapper;
	
	public SSACompositeResponseWrapper(SSACompositeResponseType responseType){
		ResponseMetadataType metadata = responseType.getResponseMetadata();
		if(metadata != null){
			this.responseCode = metadata.getResponseCode();
			this.responseText = metadata.getResponseDescriptionText();
			this.tdsResponseText = metadata.getTDSResponseDescriptionText();
			this.metadataWrapper = new ResponseMetadataWrapper(metadata);
		}
		SSACompositeIndividualResponseWrapper individualResponse;
		List<SSACompositeIndividualResponseType> individualResponseList = responseType.getSSACompositeIndividualResponse();
		for(SSACompositeIndividualResponseType individualResponseType: individualResponseList){
			individualResponse = new SSACompositeIndividualResponseWrapper(individualResponseType);
			individualResponses.add(individualResponse);
		}
	}

	public String getResponseCode() {
		return responseCode;
	}

	public String getTdsResponseText() {
		return tdsResponseText;
	}

	public String getResponseText() {
		return responseText;
	}

	public List<SSACompositeIndividualResponseWrapper> getIndividualResponses() {
		return individualResponses;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		JSONObject giContext = new JSONObject();
		if(getContext() != null){
			Set<Entry<String, Object>> entrySet = getContext().entrySet();
			Iterator<Entry<String, Object>> cursor = entrySet.iterator();
			Entry<String, Object> entry;
			while(cursor.hasNext()){
				entry = cursor.next();
				giContext.put(entry.getKey(), entry.getValue());
			}
		}
		obj.put("gi_context", giContext);
		//Metadata
		if(this.metadataWrapper != null){
			obj.put("ResponseMetadata", this.metadataWrapper);
		}
		//Response
		JSONArray ssaResponses = new JSONArray();
		for(SSACompositeIndividualResponseWrapper individualResponse: this.individualResponses){
			ssaResponses.add(individualResponse);
		}
		if(!ssaResponses.isEmpty()){
			obj.put("SSACompositeResponse", ssaResponses);
		}
		return obj.toJSONString();
	}

}
