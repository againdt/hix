package com.getinsured.iex.hub.nmec.wrapper;

import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.ObjectFactory;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.VerifyNonESIMECRequestType;

public class VerifyNonESIMECRequestTypeWrapper {
	private VerifyNonESIMECRequestType request = null;
	private ObjectFactory objectFactory = null;
	
	public VerifyNonESIMECRequestTypeWrapper(){
		objectFactory = new ObjectFactory();
		request = objectFactory.createVerifyNonESIMECRequestType();
		
		IndividualRequestTypeWrapper individualRequestTypeWrapper = new IndividualRequestTypeWrapper();
		request.getIndividualRequest().add(individualRequestTypeWrapper.getSystemRepresentation());
	}
	
	public VerifyNonESIMECRequestType getSystemRepresentation()
	{
		return request;
	}

	public void setVerifyNonESIMECRequestType(
			VerifyNonESIMECRequestType verifyNonESIMECRequestType) {
		if(objectFactory ==null){
			objectFactory = new ObjectFactory();
		}
		if(verifyNonESIMECRequestType == null){
			verifyNonESIMECRequestType = objectFactory.createVerifyNonESIMECRequestType();
		}
		this.request = verifyNonESIMECRequestType;
	}
	
}

