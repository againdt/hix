<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
    <%@ taglib uri="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>
    <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
             pageEncoding="ISO-8859-1" %>

    <c:set var="actionBarTicketId" scope="request">
        <encryptor:enc value="${param.ticketId}" isurl="true"/>
    </c:set>
    <input type="hidden" id="ticketId" name="ticketId" value="${actionBarTicketId}">
    <style>
    .pull-right {
        float: right;
    }
    </style>

    <div>
    <table class="table table-striped">
    <tbody>
    <tr>
    <td> <b>Ticket Status: </b>${quickAction.status} </td>
    <c:if test="${quickAction.status != 'Unclaimed' }">
        <c:if test="${not empty quickAction.assignee}">
            <td> <b>Assignee: </b>${quickAction.assignee}</td>
        </c:if>
    </c:if>
    <c:if test="${quickAction.status =='Open' || quickAction.status == 'Unclaimed' || quickAction.status == 'Restarted' || quickAction.status == 'Reopened'}">
        <td> <b>Current Task: </b>${quickAction.currentTask}</td>

        <c:choose>
            <c:when test="${quickAction.taskStatus =='Claimed'}">
                <td><input type="button" name="btn1" value="
                <spring:message code="option_tktTask.markAsComplete"/>
                " class="btn btn-primary btn-small pull-right" onclick="javascript:completeTicket('${actionBarTicketId}', '${param.ticketCategory}')"/></td>
            </c:when>

            <c:when test="${quickAction.taskStatus=='UnClaimed'}">
                <td><input type="button" name="btn1" value="<spring:message code="option_tktTask.claim"/>" class="btn btn-primary btn-small pull-right" onclick="javascript:completeTicket('${actionBarTicketId}', '${param.ticketCategory}')" /> </td>
            </c:when>
            <c:otherwise>
                <td>&nbsp;</td>
            </c:otherwise>
        </c:choose>
    </c:if>
    </tr>
    </tbody>
    </table>
    </div>
