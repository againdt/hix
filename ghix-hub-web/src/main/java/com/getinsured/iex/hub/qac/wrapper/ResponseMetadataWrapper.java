package com.getinsured.iex.hub.qac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.hub.qac.niem.niem.niem_core._2.TextType;

public class ResponseMetadataWrapper implements JSONAware {

	private String responseCode = null;
	private String responseDesc = null;
	private String tdsResponse = null;
	public ResponseMetadataWrapper(ResponseMetadataType metadata){
		if(metadata != null){
			
			TextType resp = metadata.getResponseCode();
			
			if(resp != null){
				this.responseCode = resp.getValue();
			}
			
			resp = metadata.getResponseDescriptionText();
			
			if(resp != null){
				this.responseDesc = resp.getValue();
			}
			
			resp = metadata.getTDSResponseDescriptionText();
			
			if(resp != null){
				this.tdsResponse = resp.getValue();
			}

		}
	}
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj  = new JSONObject();
		obj.put("ResponseCode", this.responseCode);
		obj.put("ResponseDescriptionText", this.responseDesc);
		obj.put("TDSResponseDescriptionText", this.tdsResponse);
		return obj.toJSONString();
	}

}
