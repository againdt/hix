<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>

<%@ page import="java.util.Calendar"%>
<%@ page isELIgnored="false"%>

<link rel="stylesheet" href="//cdn.rawgit.com/angular-ui/bower-ui-grid/master/ui-grid.min.css">

<style>
	.ui-grid-pager-control input {
	    height: 14px;
	}

	.ui-grid-pager-control .ui-grid-pager-max-pages-number {
	    vertical-align: middle;
	}

	.grid-left-align {
	    text-align: left;
	}

	h1 img {
		height: auto;
		width: auto;
	}
</style>

<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/angular-animate.1.5.6.js" />'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/angular-touch.1.5.6.js" />'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/ui-grid.min.4.0.2.js" />'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/csv.js" />'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/pdfmake.js" />'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/vfs_fonts.js" />'></script>

<script type="text/javascript" src="<gi:cdnurl value="/resources/js/lce/ui-bootstrap-tpls-0.11.0.js"/>"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/dashboardAnnouncement/angular-ui-router.min.js"/>"></script>

<script type="text/javascript" src='<gi:cdnurl value="/resources/js/provider/providerLoadStatusList.js" />'></script>

<script type="text/javascript">

	function uploadProviderDataFile(formName, url) {

		if (isValidateForm() && formName && url) {

			document.forms[formName].method = "post";
			document.forms[formName].action = url;
			document.forms[formName].submit();
		}
	}

	function isValidateForm() {

		var flag = true;
		var loForm = document.forms["frmUploadProviderFile"];

		if (loForm) {

			var laFields = new Array();
			var li = 0;

			var nodeValue = loForm["selectYear"].value;
			if (!nodeValue) {
				laFields[li++] = "Plan Year must be required.";
			}

			nodeValue = loForm["selectType"].value;
			if (!nodeValue) {
				laFields[li++] = "Provider Type must be required.";
			}

			var nodeFileNode = loForm["fileToUpload"];

			if (nodeFileNode.files.length == 0) {
				laFields[li++] = "Please select a Provider file to upload.";
			}
			else {
				var fileName = nodeFileNode.value;
				var ext = fileName.split('.').pop();

				if (ext != "txt") {
					laFields[li++] = "File type must be TEXT.";
				}
			}

			if (laFields.length > 0) {

				if (laFields.length > 1) {

					for (var fi = 0; fi < laFields.length; fi++) {
						laFields[fi] = "- " + laFields[fi];
					}
				}
				alert(laFields.join('\n'));
				flag = false;
			}
		}
		return flag;
	}
</script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>

<div class="gutter10">

	<div class="row-fluid">
		<h1 class="pull-left">
			<spring:message code="pgheader.manageProviderFileUploads" />
		</h1>
	</div><!-- end of row-fluid -->

	<div class="row-fluid" id="rightpanel">

		<div class="header">
			<h4 class="pull-left"><spring:message code="label.providerFile"/></h4>
			<a class="btn btn-small pull-right" href="javascript:uploadProviderDataFile('frmUploadProviderFile', '${pageContext.request.contextPath}/admin/provider/uploadProviderDataFile');" class="btn btn-primary offset1" type="submit" id="save">
				<spring:message code="label.upload" />
			</a>
		</div>

		<div class="gutter10">
			<div class="row-fluid">
				<c:if test="${uploadProviderFileMessage != null}">
					<div align="left" style="width:100%; font-size: 10pt; color: green; font-weight: bold;">
						<c:out value="${uploadProviderFileMessage}"/>
					</div>
					<br/>
				</c:if>

				<c:if test="${uploadFailedProviderFileMessage != null}">
					<div align="left" style="width:100%; font-size: 10pt; color: red; font-weight: bold;">
						<c:out value="${uploadFailedProviderFileMessage}"/>
					</div>
					<br/>
				</c:if>

				<form class="form-horizontal" id="frmUploadProviderFile" name="frmUploadProviderFile" action="${pageContext.request.contextPath}/admin/provider/uploadProviderDataFile" enctype="multipart/form-data" method="POST">
					<df:csrfToken/>

					<div style="width: 30%; float:left">
						<label class="control-label" for="selectYear"><spring:message code="label.selectYear"/> 
						<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
						</label>
					</div>

					<div style="width: 70%; float:right">
						<c:set var="currentYear" ><%=Calendar.getInstance().get(Calendar.YEAR)%></c:set>
						<select size="1" id="selectYear" name="selectYear" style="width :25%">
							<option value="${currentYear}" selected>${currentYear}</option>
							<option value="${currentYear + 1}">${currentYear + 1}</option>
						</select>
					</div>
					<br/><br/>

					<div style="width: 30%; float:left">
						<label class="control-label" for="selectType"><spring:message code="label.selectType"/> 
						<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
						</label>
					</div>

					<div style="width: 70%; float:right">
						<select size="1" id="selectType" name="selectType" style="width :25%">
							<option value="" selected="selected"><spring:message code="label.selectType"/></option>
							<c:forEach var="providerType" items="${providerTypeIndicatorList}">
								<option value="${providerType.key}"><c:out value="${providerType.value}"/></option>
							</c:forEach>
						</select>
					</div>
					<br/><br/>

					<div style="width: 30%; float:left">
						<label class="control-label" for="fileToUpload"><spring:message code="label.providerFile"/>
							<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
						</label>
					</div>

					<div style="width: 70%; float:right">
						<input type="file" class="filestyle" multiple="multiple" id="fileToUpload" name="fileToUpload" data-classButton="btn">
					</div>
				</form>
			</div><!-- end of .row-fluid -->
		</div> <!-- end of .gutter10 -->

		<br/><br/>
		<div class="gutter10" id="providerLoadStatusListDiv" ng-app="providerLoadStatusListApp" ng-controller="ProviderLoadStatusListCtrl">
			<div class="row-fluid">
				<div ui-grid="gridOptions" style="height: 100%" class="grid" ui-grid-pagination ui-grid-resize-columns></div>
			</div><!-- end of .row-fluid -->

			<div>
				<form class="form-horizontal" id="frmDownloadProviderLogs" name="frmDownloadProviderLogs" action="${pageContext.request.contextPath}/admin/provider/downloadProviderLogs" method="POST">
					<df:csrfToken/>
					<input type="hidden" id="selectedLogs" name="selectedLogs" />
				</form>
			</div>
		</div> <!-- end of .gutter10 -->
	</div><!-- end of .row-fluid -->
</div><!-- end .gutter10 -->
