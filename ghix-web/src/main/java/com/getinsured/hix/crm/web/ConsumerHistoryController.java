package com.getinsured.hix.crm.web;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.cap.util.CapPortalConfigurationUtil;
import com.getinsured.hix.cap.util.CapRequestValidatorUtil;
import com.getinsured.hix.crm.util.ConsumerEventsUtil;
import com.getinsured.hix.dto.crm.ConsumerHistoryRequest;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.RestfulResponseException;
import com.thoughtworks.xstream.XStream;

@Controller
public class ConsumerHistoryController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerHistoryController.class);
	private static final String ERROR_MESSAGE = "errorMsg";
	private static final String APPLICATION_ERROR = "Application Error :::Please contact system admin";
	public static final String CONTENT_TYPE = "application/json";
	private static final String CAUSE = "Cause : ";
	private static final String VIEW_CONSUMER_CRM_PERMISSION = "hasPermission(#model, 'VIEW_CONSUMER_CRM')";
	private static final String APP_EVENT = "APPEVENT_";
	private static final String MODULE_ID = "moduleId";
	private static final List<String> HISTORY_RESTRICTED_ROLES = Arrays.asList("APPROVEDADMINSTAFFL1","APPROVEDADMINSTAFFL2");
	
	private static final String ADD_NEW_VIEW_CASE_URL = "addNewViewCaseUrl";
	
	@Autowired private HIXHTTPClient hIXHTTPClient;
	@Autowired private RestTemplate restTemplate;
	@Autowired private RestTemplate mvcRestTemplate;
	@Autowired private UserService userService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private RoleService roleService;
	@Autowired private LookupService lookupService;
	@Autowired private ConsumerEventsUtil consumerEventsUtil;
	@Autowired private CapRequestValidatorUtil capRequestValidatorUtil;
	@Autowired private CapPortalConfigurationUtil capPortalConfigurationUtil;
	
	public static String VIEW_CASE_URL;
	
	@Value("#{configProp['cap.basicInfo.viewCaseAhbxUrl']}")
	public void setADD_VIEW_CASE_URL(String vIEW_CASE_URL) {
		VIEW_CASE_URL = vIEW_CASE_URL;
	}
	/**
	 * Handles the GET request for viewing consumer history.
	 *
	 * @param id
	 * @param request
	 * @param model
	 * @return
	 * @throws InvalidUserException
	 */
	@RequestMapping(value = "crm/consumer/viewHistory/{id}/OLD", method = RequestMethod.GET)
	@PreAuthorize(VIEW_CONSUMER_CRM_PERMISSION)
	public String getConsumerHistoryView(@PathVariable("id") String encHouseholdId, HttpServletRequest request, Model model)throws InvalidUserException {
		
		LOGGER.info("consumer - getConsumerHistoryView : START ");
		String StrEncHouseholdId = (StringUtils.isNumeric(encHouseholdId)) ? encHouseholdId: ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdId);
		Integer id = Integer.parseInt(StrEncHouseholdId);
		Household household = getConsumerDetails(id);
		model.addAttribute("householdId", id);
		model.addAttribute("household",household);
		capPortalConfigurationUtil.setMemberNavMenuPermissions(model,request);
		capPortalConfigurationUtil.setMemberHistoryPermissions(model,request);
		setViewMemberLink(model);
		LOGGER.info("consumer - getConsumerHistoryView : END ");
		return "crm/consumer/viewHistory";
	}
	
	/**
	 * Gets the consumer details 
	 * @param id
	 * @return
	 */
	private Household getConsumerDetails(int id) {
		String postParams = "\"" + id + "\"";
		XStream xstream = GhixUtils.getXStreamStaxObject();
		try{
			ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID, postParams, String.class);
			String postResp = responseEntity.getBody();
			ConsumerResponse consumerResponse = (ConsumerResponse) xstream.fromXML(postResp);
			if(null == consumerResponse){
				return null;
			}
			return consumerResponse.getHousehold();
		}
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Unable to get household by id:"+id,ex.getMessage());	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex.getMessage());
				break;
			}
			return null;
		} 
		
	}
	
	/**
	 * Handles GET/POST request for displaying consumer call log history
	 *
	 * @param id
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/crm/consumer/callLogHistory", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	@PreAuthorize(VIEW_CONSUMER_CRM_PERMISSION)
	public AjaxResponseBase getConsumerCallLogHistory(@RequestParam("householdId") String householdId, Model model, HttpServletRequest request) {

		LOGGER.info("consumer - getConsumerCallLogHistory : START");
		ObjectMapper mapper =  new ObjectMapper();
		String response ="";
		//System.out.println("here...");
		AjaxResponseBase ajaxResponse = new AjaxResponseBase(false, "this is the msg");
		//System.out.println("here..");
		
		try {
			ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.GET_CONSUMER_CALL_HISTORY, mapper.writeValueAsString(householdId), String.class);
			response = responseEntity.getBody();
			ajaxResponse.setStatus(true);
			ajaxResponse.setData(response);
		} 
		catch(RestfulResponseException ex){
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Error in consumer - GET_CONSUMER_CALL_HISTORY",ex.getMessage() + CAUSE);	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex.getMessage());
				break;
			}
			ajaxResponse.setMessage(ex.getMessage());
		} 
		catch (Exception exception) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in consumer - getConsumerTicketHistory: " + exception.getMessage() + CAUSE
					+ exception.getCause());
			ajaxResponse.setMessage(exception.getMessage());
		}
		LOGGER.info("consumer - getConsumerCallLogHistory : END");
		return ajaxResponse;
	}
	
	/**
	 * Handles GET/POST request for saving consumer call log 
	 *
	 * @param id
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/crm/consumer/saveConsumerCallLog", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	@PreAuthorize(VIEW_CONSUMER_CRM_PERMISSION)
	public String saveConsumerCallLog(@RequestParam("jsonLogData") String jsonLogData,@RequestParam("householdId") String householdId, HttpServletRequest request) {

		LOGGER.info("consumer - getConsumerCallLogHistory : START");
		String afterDecoding = org.apache.commons.lang.StringEscapeUtils.unescapeHtml(jsonLogData);
		String response="false";
		ObjectMapper mapper =  new ObjectMapper();
		AccountUser user = null;

		try {
			user = userService.getLoggedInUser();
			ConsumerHistoryRequest  consumerHistoryRequest = new ConsumerHistoryRequest();
			consumerHistoryRequest.setAgentId(user.getId());
			consumerHistoryRequest.setHouseholdId(Integer.parseInt(householdId));
			consumerHistoryRequest.setLogData(afterDecoding);
			consumerHistoryRequest.setAgentName(user.getFullName());
			
			ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.SAVE_CONSUMER_CALL_LOG, mapper.writeValueAsString(consumerHistoryRequest),String.class);
			response = responseEntity.getBody(); 
			
		} 
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Error in consumer - SAVE_CONSUMER_CALL_LOG: " + ex.getMessage() + CAUSE
						+ ex.getCause());	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex.getMessage());
				break;
			}
		}
		catch (Exception exception) {
			LOGGER.error("Error in consumer - SAVE_CONSUMER_CALL_LOG: " + exception.getMessage() + CAUSE
					+ exception.getCause());
		}
		LOGGER.info("consumer - getConsumerTicketHistory : END");
		return response;
	}
	
	/**
	 * Handles GET/POST request for saving consumer Comment log 
	 *
	 * @param id
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/crm/consumer/saveConsumerCommentLog", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	@PreAuthorize(VIEW_CONSUMER_CRM_PERMISSION)
	public String saveConsumerCommentLog(@RequestParam("jsonLogData") String jsonLogData,@RequestParam("householdId") String householdId, HttpServletRequest request) {

		LOGGER.info("consumer - getConsumerCallLogHistory : START");
		String response="false";
		String afterDecoding = org.apache.commons.lang.StringEscapeUtils.unescapeHtml(jsonLogData);
		ObjectMapper mapper =  new ObjectMapper();
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
			ConsumerHistoryRequest  consumerHistoryRequest = new ConsumerHistoryRequest();
			consumerHistoryRequest.setAgentId(user.getId());
			consumerHistoryRequest.setHouseholdId(Integer.parseInt(householdId));
			consumerHistoryRequest.setLogData(afterDecoding);
			consumerHistoryRequest.setAgentName(user.getFullName());
			ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.SAVE_CONSUMER_COMMENT_LOG, mapper.writeValueAsString(consumerHistoryRequest), String.class);
			response = responseEntity.getBody();
		}
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Error in consumer - SAVE_CONSUMER_COMMENT_LOG: " + ex.getMessage() + CAUSE
						+ ex.getCause());	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex.getMessage());
				break;
			}
		} 
		catch (Exception exception) {
			LOGGER.error("Error in consumer - SAVE_CONSUMER_COMMENT_LOG: " + exception.getMessage() + CAUSE
					+ exception.getCause());
		}
		LOGGER.info("consumer - getConsumerTicketHistory : END");
		return response;
	}
	
	@RequestMapping(value = "crm/consumer/viewHistory/{id}", method = RequestMethod.GET)
	@PreAuthorize(VIEW_CONSUMER_CRM_PERMISSION)
	public String getHistory(@PathVariable("id") String encHouseholdId, HttpServletRequest request, Model model)throws InvalidUserException {
		
		LOGGER.info("Getting cutomer history");
		capRequestValidatorUtil.validateGetHistory(encHouseholdId);
		
		try {
			String StrEncHouseholdId = (StringUtils.isNumeric(encHouseholdId)) ? encHouseholdId: ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdId);
			model.addAttribute("householdId", StrEncHouseholdId);
			Integer id = Integer.parseInt(StrEncHouseholdId);
			Household household = getConsumerDetails(id);
			model.addAttribute("household",household);
			
			Map<String, String> roleMap = new HashMap<String, String>();
			List<Role> roles = roleService.findAll();
			getRestrictedRolesForMemberHistory(roles);
			for (Role role : roles) {
				roleMap.put(role.getName(), role.getLabel()); 
			}
			model.addAttribute("roleList", JacksonUtils.getJacksonObjectWriterForHashMap(String.class, String.class).writeValueAsString(roleMap));
			
			model.addAttribute("typeAndNames", JacksonUtils.getJacksonObjectWriter(Map.class).writeValueAsString(getEventTypeAndNames()));
			capPortalConfigurationUtil.setMemberNavMenuPermissions(model,request);
			capPortalConfigurationUtil.setMemberHistoryPermissions(model,request);

			setViewMemberLink(model);
		} catch (Exception exception) {
			LOGGER.error("Error in getting customer history: " + exception.getMessage() + CAUSE
					+ exception.getCause());
		}
		
		return "crm/consumer/viewHistory";
	}
	
	@RequestMapping(value = "crm/consumer/searchAppEvents", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody public String searchAppEvents(HttpServletRequest request, HttpServletResponse response)  {
		LOGGER.info("Searching application events");
		String responseJson = StringUtils.EMPTY;
		if(StringUtils.isBlank(request.getParameter(MODULE_ID))) {
			return  "Invalid request";
		}
		
		try {
			Map<String, Object> searchCriteria = createAppEventSearchCriteria(request);
			ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.SEARCH_APP_EVENTS, searchCriteria, String.class);
			responseJson = responseEntity.getBody();
			
		}
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Error in consumer - SEARCH_APP_EVENTS: " + ex.getMessage() + CAUSE
						+ ex.getCause());	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex.getMessage());
				break;
			}
		} 
		catch (Exception exception) {
			LOGGER.error("Error in searching application events: " + exception.getMessage() + CAUSE
					+ exception.getCause());
		}
		
		return  responseJson;
	}
	
	private Map<String, List<String>> getEventTypeAndNames() {
		Map<String, List<String>> typeAndNames = new HashMap<String, List<String>>();
		List<LookupValue> lookupValueList = lookupService.getLookupValueListForAppEvents(APP_EVENT);
		for(LookupValue lookupValue : lookupValueList) {
			String type = lookupValue.getLookupType().getDescription();
			List<String> names = (typeAndNames.containsKey(type)) ? typeAndNames.get(type) : new ArrayList<String>();
			names.add(lookupValue.getLookupValueLabel());
			typeAndNames.put(type, names);
		}
		return typeAndNames;
	}
	
	private Map<String, Object> createAppEventSearchCriteria(HttpServletRequest request) throws ParseException {
		Map<String, Object> searchCriteria = new HashMap<String, Object>();
		Enumeration<String> names = request.getParameterNames();
		while (names.hasMoreElements()) {
			String name = names.nextElement();
			if(name.contains(MODULE_ID)) {
				String moduleId = (StringUtils.isNumeric(request.getParameter(name))) ? request.getParameter(name) : ghixJasyptEncrytorUtil.decryptStringByJasypt(request.getParameter(name));
				searchCriteria.put(name, moduleId);
				continue;
			}

			searchCriteria.put(name, request.getParameter(name));
		}
		
		return searchCriteria;
	}
	
	@RequestMapping(value = "/cap/history/calllog", method = {RequestMethod.GET,RequestMethod.POST})
	@ResponseBody
	@PreAuthorize(VIEW_CONSUMER_CRM_PERMISSION)
	public void createCallLog(@RequestParam("callLog") String jsonLogData, HttpServletRequest request) {

		LOGGER.info("consumer - createCallLog : START");
		String afterDecoding = org.apache.commons.lang.StringEscapeUtils.unescapeHtml(jsonLogData);
		consumerEventsUtil.createCallLogEvent(afterDecoding);
		LOGGER.info("consumer - createCallLog : END");
	}

	private void getRestrictedRolesForMemberHistory(List<Role> roles) {
		java.util.Iterator<Role> it = roles.iterator();
		while(it.hasNext()){
			Role role = it.next();
			if(HISTORY_RESTRICTED_ROLES.contains(role.getName())){
				it.remove();
			}
		}
		
	}
	
	private void setViewMemberLink(Model model) {
    	model.addAttribute(ADD_NEW_VIEW_CASE_URL, VIEW_CASE_URL);
	}
}
