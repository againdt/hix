package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpsav.SubmitAgency3DHSResubResponseSetType;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;


/**
 * Wrapper class for SubmitAgency3DHSResubResponseSetType
 * 
 * @author Nikhil Talreja
 * @since 10-Feb-2014
 * 
 */
public class SubmitAgency3DHSResubResponseSetTypeWrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SubmitAgency3DHSResubResponseSetTypeWrapper.class);
	
	private String caseNumber;
    private String nonCitLastName;
    private String nonCitFirstName;
    private String nonCitMiddleName;
    private String nonCitBirthDate;
    private String nonCitEntryDate;
    private String nonCitAdmittedToDate;
    private String nonCitAdmittedToText;
    private String nonCitCountryBirthCd;
    private String nonCitCountryCitCd;
    private String nonCitCoaCode;
    private int eligStatementCd;
    private String eligStatementTxt;
    private String webServSftwrVer;
    private String fiveYearBarApplyCode;
    private String lawfulPresenceVerifiedCode;
    private boolean dshAutoTriggerStepTwo;
    private String qualifiedNonCitizenCode;
    private String fiveYearBarMetCode;
    private String usCitizenCode;
    
    public SubmitAgency3DHSResubResponseSetTypeWrapper(SubmitAgency3DHSResubResponseSetType submitAgency3DHSResubResponseSetType){
    	
    	if (submitAgency3DHSResubResponseSetType == null){
    		return;
    	}
    	
    	this.caseNumber = submitAgency3DHSResubResponseSetType.getCaseNumber();
    	this.nonCitLastName = submitAgency3DHSResubResponseSetType.getNonCitLastName();
    	this.nonCitFirstName = submitAgency3DHSResubResponseSetType.getNonCitFirstName();
    	this.nonCitMiddleName = submitAgency3DHSResubResponseSetType.getNonCitLastName();
    	try{
	    	if(submitAgency3DHSResubResponseSetType.getNonCitBirthDate() != null){
	    		this.nonCitBirthDate = VLPServiceUtil.xmlDateToString(submitAgency3DHSResubResponseSetType.getNonCitBirthDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(submitAgency3DHSResubResponseSetType.getNonCitEntryDate() != null){
	    		this.nonCitEntryDate = VLPServiceUtil.xmlDateToString(submitAgency3DHSResubResponseSetType.getNonCitEntryDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(submitAgency3DHSResubResponseSetType.getNonCitAdmittedToDate() != null){
	    		this.nonCitAdmittedToDate = VLPServiceUtil.xmlDateToString(submitAgency3DHSResubResponseSetType.getNonCitAdmittedToDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
    	}
    	catch(Exception e){
    		LOGGER.error("Exception occured while setting date",e);
    	}


    	this.nonCitAdmittedToText = submitAgency3DHSResubResponseSetType.getNonCitAdmittedToText();
    	this.nonCitCountryBirthCd = submitAgency3DHSResubResponseSetType.getNonCitCountryBirthCd();
    	this.nonCitCountryCitCd = submitAgency3DHSResubResponseSetType.getNonCitCountryCitCd();
    	this.nonCitCoaCode = submitAgency3DHSResubResponseSetType.getNonCitCoaCode();
    	this.eligStatementCd = submitAgency3DHSResubResponseSetType.getEligStatementCd().intValue();
    	this.eligStatementTxt = submitAgency3DHSResubResponseSetType.getEligStatementTxt();
    	
    	this.webServSftwrVer = submitAgency3DHSResubResponseSetType.getWebServSftwrVer();
    	this.dshAutoTriggerStepTwo = submitAgency3DHSResubResponseSetType.isDSHAutoTriggerStepTwo();
    	this.lawfulPresenceVerifiedCode = submitAgency3DHSResubResponseSetType.getLawfulPresenceVerifiedCode();
    	this.fiveYearBarApplyCode = submitAgency3DHSResubResponseSetType.getFiveYearBarApplyCode();
    	this.qualifiedNonCitizenCode = submitAgency3DHSResubResponseSetType.getQualifiedNonCitizenCode();
    	this.fiveYearBarMetCode = submitAgency3DHSResubResponseSetType.getFiveYearBarMetCode();
    	this.usCitizenCode = submitAgency3DHSResubResponseSetType.getUSCitizenCode();
    }
    
	public String getCaseNumber() {
		return caseNumber;
	}




	public String getNonCitLastName() {
		return nonCitLastName;
	}




	public String getNonCitFirstName() {
		return nonCitFirstName;
	}




	public String getNonCitMiddleName() {
		return nonCitMiddleName;
	}




	public String getNonCitBirthDate() {
		return nonCitBirthDate;
	}




	public String getNonCitEntryDate() {
		return nonCitEntryDate;
	}




	public String getNonCitAdmittedToDate() {
		return nonCitAdmittedToDate;
	}




	public String getNonCitAdmittedToText() {
		return nonCitAdmittedToText;
	}




	public String getNonCitCountryBirthCd() {
		return nonCitCountryBirthCd;
	}




	public String getNonCitCountryCitCd() {
		return nonCitCountryCitCd;
	}




	public String getNonCitCoaCode() {
		return nonCitCoaCode;
	}




	public int getEligStatementCd() {
		return eligStatementCd;
	}




	public String getEligStatementTxt() {
		return eligStatementTxt;
	}




	public String getWebServSftwrVer() {
		return webServSftwrVer;
	}




	public String getFiveYearBarApplyCode() {
		return fiveYearBarApplyCode;
	}




	public String getLawfulPresenceVerifiedCode() {
		return lawfulPresenceVerifiedCode;
	}




	public boolean isDshAutoTriggerStepTwo() {
		return dshAutoTriggerStepTwo;
	}




	public String getQualifiedNonCitizenCode() {
		return qualifiedNonCitizenCode;
	}




	public String getFiveYearBarMetCode() {
		return fiveYearBarMetCode;
	}




	public String getUsCitizenCode() {
		return usCitizenCode;
	}




	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("CaseNumber",this.caseNumber);
		obj.put("NonCitLastName",this.nonCitLastName);
		obj.put("NonCitFirstName",this.nonCitFirstName);
		obj.put("NonCitMiddleName",this.nonCitMiddleName);
		obj.put("NonCitBirthDate",this.nonCitBirthDate);
		obj.put("NonCitEntryDate",this.nonCitEntryDate);
		obj.put("NonCitAdmittedToDate",this.nonCitAdmittedToDate);
		obj.put("NonCitAdmittedToText",this.nonCitAdmittedToText);
		obj.put("NonCitCountryBirthCd",this.nonCitCountryBirthCd);
		obj.put("NonCitCountryCitCd",this.nonCitCountryCitCd);
		obj.put("NonCitCoaCode",this.nonCitCoaCode);
		obj.put("EligStatementCd",this.eligStatementCd);
		obj.put("EligStatementTxt",this.eligStatementTxt);
		obj.put("WebServSftwrVer",this.webServSftwrVer);
		obj.put("LawfulPresenceVerifiedCode",this.lawfulPresenceVerifiedCode);
		obj.put("dshAutoTriggerStepTwo",this.dshAutoTriggerStepTwo);
		obj.put("FiveYearBarApplyCode",this.fiveYearBarApplyCode);
		obj.put("QualifiedNonCitizenCode",this.qualifiedNonCitizenCode);
		obj.put("FiveYearBarMetCode",this.fiveYearBarMetCode);
		obj.put("USCitizenCode",this.usCitizenCode);
		return obj.toJSONString();
	}

}
