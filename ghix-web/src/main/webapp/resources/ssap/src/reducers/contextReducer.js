import { contextActionTypes } from '../constants/actionTypes';

export const initialState = {};

export default (state = initialState, action) => {
    switch (action.type) {
    case contextActionTypes.ADD_CONTEXT:
        return state[action.name] ?
            state :
            {
                ...state,
                [action.name]: action.context
            };
    case contextActionTypes.REMOVE_CONTEXT:
        delete state[action.name];
        return state;
    case contextActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};