package com.getinsured.eligibility.at.ref.service.migration;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.ref.service.ReferralLceNotificationService;
import com.getinsured.eligibility.at.ref.service.ReferralOEService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.exception.DataNotFoundException;
import com.getinsured.eligibility.exception.InvalidIncomingValueException;
import com.getinsured.eligibility.exception.InvalidStateException;
import com.getinsured.eligibility.model.SepEvents.Gated;
import com.getinsured.eligibility.model.SepEvents.Source;
import com.getinsured.eligibility.qlevalidation.CmrDocumentServiceUtils;
import com.getinsured.eligibility.qlevalidation.CmrDocumentServiceUtils.AcceptedValue;
import com.getinsured.eligibility.qlevalidation.CmrDocumentServiceUtils.CmrTargetName;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.eligibility.qlevalidation.TkmServiceUtils;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.dto.ssap.SsapApplicantEvent;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.iex.ssap.model.SsapApplicantEvent.ApplicantEventValidationStatus;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;

@Service("qleValidationMigrationService")
public class QLEValidationMigrationServiceImpl implements QLEValidationService {

	private static final String NO_EVENTS_FOUND_FOR_APPLICATION = "No events found for application - ";

	private static final String SEP_OVERRIDE = "SEP Override by user - ";

	private static final String DOCUMENT_ACCEPTED_BY = "Document accepted by -";
	private static final String DOCUMENT_REJECTED_BY = "Document rejected by -";
	private static final String SEP_DENIED = "SEP Denied as window expired - ";
	private static final String EVENT_OVERRIDDEN_BY_USER = "Event Overridden by user - ";
	private static final String NO_APPLICANT_EVENT_FOUND_FOR = "No Applicant event found for - ";
	private static final String INVALID_SOURCE_TYPE_MUST_BE = "Invalid source type; must be - ";
	private static final String ACCOUNT_USER_NOT_FOUND_FOR_LAST_UPDATED_BY = "Account user not found for lastUpdatedBy - ";
	private static final String CLOSED_BY_SYSTEM = "Closed by system - ";
	private static final String CANCELLED_BY_USER = "Cancelled by user - ";
	private static final String SSAP_APPLICATION_NOT_FOUND_FOR_CASE_NUMBER = "Ssap application not found for case number - ";
	private static final String DISCARD_APP_INVALID_STATE_MSG = "Current application status - %s ;application status should not be in any one of these statuses - %s ";
	private static final String INVALID_VALIDATION_STATUS_STATE_MSG = "Incoming Validation Status is not valid; current : %s , incoming : %s ";

	private static final Set<ApplicantEventValidationStatus> OPEN_EVENT_VALIDATION_STATUS = EnumSet.of(
			ApplicantEventValidationStatus.INITIAL, ApplicantEventValidationStatus.PENDING,
			ApplicantEventValidationStatus.SUBMITTED, ApplicantEventValidationStatus.REJECTED);

	private static final Set<ApplicationValidationStatus> OPEN_APPLICATION_VALIDATION_STATUS = EnumSet
			.of(ApplicationValidationStatus.INITIAL, ApplicationValidationStatus.PENDING);

	@SuppressWarnings("serial")
	private static final Set<String> INVALID_CANCELLED_CLOSED_APPLICATION_STATUS = new HashSet<String>() {
		{
			add(ApplicationStatus.CANCELLED.getApplicationStatusCode());
			add(ApplicationStatus.CLOSED.getApplicationStatusCode());
			add(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode());
		}
	};

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired
	private SsapApplicantEventRepository ssapApplicantEventRepository;
	@Autowired
	private IUserRepository iUserRepository;
	@Autowired
	private TkmServiceUtils tkmServiceUtils;
	@Autowired
	private CmrDocumentServiceUtils cmrDocumentServiceUtils;

	@Autowired
	@Qualifier("ssapEnrolleeHandler")
	private SsapEnrolleeHandler ssapEnrolleeHandler;

	@Autowired
	@Qualifier("qleAutomationMigrationHandler")
	private QLEAutomationMigrationHandler qleAutomationMigrationHandler;

	@Override
	public void discardApp(String caseNumber, ApplicationStatus applicationStatus, Integer lastUpdatedBy) {

		SsapApplication ssap = validateCaseNumber(caseNumber);

		if (INVALID_CANCELLED_CLOSED_APPLICATION_STATUS.contains(ssap.getApplicationStatus())) {
			throw new InvalidStateException(String.format(DISCARD_APP_INVALID_STATE_MSG, ssap.getApplicationStatus(),
					Arrays.asList(INVALID_CANCELLED_CLOSED_APPLICATION_STATUS)));
		}

		AccountUser accountUser = validateAccountUser(lastUpdatedBy);

		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository
				.findApplicationEventAndApplicantEventsBySsapApplication(ssap.getId());

		Timestamp currentTimestamp = new Timestamp(new TSDate().getTime());

		List<Integer> submittedEventIds = new ArrayList<>();

		if (ssapApplicationEvent != null) {
			List<com.getinsured.iex.ssap.model.SsapApplicantEvent> applicantEvents = ssapApplicationEvent.getSsapApplicantEvents();

			boolean eventsUpdated = false;

			if (applicantEvents != null && !applicantEvents.isEmpty()) {
				for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
					if (OPEN_EVENT_VALIDATION_STATUS.contains(ssapApplicantEvent.getValidationStatus())) {
						eventsUpdated = true;
						if (ssapApplicantEvent.getValidationStatus() == ApplicantEventValidationStatus.SUBMITTED) {
							submittedEventIds.add((int) ssapApplicantEvent.getId());
						}

						ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.CANCELLED);
						ssapApplicantEvent.setLastUpdatedBy(lastUpdatedBy);
						ssapApplicantEvent.setLastUpdateTimestamp(currentTimestamp);
					}
				}

				if (eventsUpdated) {
					ssapApplicantEventRepository.save(applicantEvents);
				}
			}
		}

		if (OPEN_APPLICATION_VALIDATION_STATUS.contains(ssap.getValidationStatus())) {
			ssap.setValidationStatus(ApplicationValidationStatus.CANCELLED);
		}

		String reason = null;

		if (applicationStatus == ApplicationStatus.CANCELLED) {
			reason = CANCELLED_BY_USER;
			ssap.setApplicationStatus(ApplicationStatus.CANCELLED.getApplicationStatusCode());
			ssap.setLastUpdatedBy(new BigDecimal(lastUpdatedBy));
			ssap.setLastUpdateTimestamp(currentTimestamp);
		} else {
			reason = CLOSED_BY_SYSTEM;
			ssap.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
			ssap.setLastUpdatedBy(new BigDecimal(lastUpdatedBy));
			ssap.setLastUpdateTimestamp(currentTimestamp);
		}
		ssapApplicationRepository.save(ssap);

		if (ReferralUtil.listSize(submittedEventIds) > 0) {
			cancelTickets(accountUser, submittedEventIds, reason);
			updateCmrDocuments(accountUser, submittedEventIds, AcceptedValue.N, reason);
		}

	}

	private void updateCmrDocuments(AccountUser accountUser, List<Integer> submittedEventIds, AcceptedValue accepted,
			String reason) {
		cmrDocumentServiceUtils.updateCmrDocuments(CmrTargetName.SSAP_APPLICANT_EVENTS, submittedEventIds, accepted,
				reason + accountUser.getUsername());
	}

	private AccountUser validateAccountUser(Integer lastUpdatedBy) {
		AccountUser accountUser = iUserRepository.getUserBasicInfo(lastUpdatedBy);

		if (accountUser == null) {
			throw new DataNotFoundException(ACCOUNT_USER_NOT_FOUND_FOR_LAST_UPDATED_BY + lastUpdatedBy);
		}
		return accountUser;
	}

	private SsapApplication validateCaseNumber(String caseNumber) {
		SsapApplication ssap = ssapApplicationRepository.findByCaseNumberId(caseNumber);

		if (ssap == null) {
			throw new DataNotFoundException(SSAP_APPLICATION_NOT_FOUND_FOR_CASE_NUMBER + caseNumber);
		}
		return ssap;
	}

	@Override
	public boolean updateApplicantEventAndRerunValidationEngine(Long applicantEventID, SsapApplicantEvent event) {

		AccountUser accountUser = validateAccountUser(event.getLastUpdatedUserId());

		com.getinsured.iex.ssap.model.SsapApplicantEvent applicantEvent = validateApplicantEvent(applicantEventID,
				event);

		List<Integer> submittedEventIds = new ArrayList<>(1);
		submittedEventIds.add((int) applicantEvent.getId());
		if (event.getStatus() == ApplicantEventValidationStatus.OVERRIDDEN
				&& applicantEvent.getValidationStatus() == ApplicantEventValidationStatus.SUBMITTED) {
			cancelTickets(accountUser, submittedEventIds, EVENT_OVERRIDDEN_BY_USER);
			updateCmrDocuments(accountUser, submittedEventIds, AcceptedValue.N, EVENT_OVERRIDDEN_BY_USER);
		} else if (event.getStatus() == ApplicantEventValidationStatus.REJECTED) {
			updateCmrDocuments(accountUser, submittedEventIds, AcceptedValue.N, DOCUMENT_REJECTED_BY);
		} else if (event.getStatus() == ApplicantEventValidationStatus.VERIFIED) {
			updateCmrDocuments(accountUser, submittedEventIds, AcceptedValue.Y, DOCUMENT_ACCEPTED_BY);
		}

		Timestamp todayTimestamp = new Timestamp(new TSDate().getTime());

		applicantEvent.setValidationStatus(event.getStatus());
		applicantEvent.setLastUpdatedBy(event.getLastUpdatedUserId());
		applicantEvent.setLastUpdateTimestamp(todayTimestamp);
		ssapApplicantEventRepository.save(applicantEvent);

		SsapApplication currentApp = applicantEvent.getSsapAplicationEvent().getSsapApplication();
		SsapApplicationEvent ssapApplicationEvent = applicantEvent.getSsapAplicationEvent();

		boolean isSepExpired = false;
		if (ssapApplicationEvent.getEnrollmentEndDate().before(todayTimestamp)) {
			isSepExpired = true;
		}

		ApplicationValidationStatus currentStatus = rerunValidationEngine(currentApp.getCaseNumber(),
				event.getLastUpdatedUserId());

		if (isSepExpired) {
			denySep(currentApp.getCaseNumber(), event.getLastUpdatedUserId());
		} else {
			if (event.getStatus() == ApplicantEventValidationStatus.REJECTED) { /* event rejected */
			} else {
				if (currentStatus == ApplicationValidationStatus.VERIFIED) { /* currentApp got verified*/
					if (StringUtils.equalsIgnoreCase(currentApp.getFinancialAssistanceFlag(), ReferralConstants.Y)) {
						if (StringUtils.equals(currentApp.getApplicationType(), SEP)){
							qleAutomationMigrationHandler.doAutomation(currentApp.getId());
						} else {
						}
					} else {
					}
				}
			}
		}

		return true;
	}

	private void cancelTickets(AccountUser accountUser, List<Integer> submittedEventIds, String reason) {
		tkmServiceUtils.cancelTickets(submittedEventIds, accountUser, reason + accountUser.getUsername());
	}

	@Override
	public boolean denySep(String caseNumber, Integer lastUpdatedUserId) {

		SsapApplication ssap = validateCaseNumber(caseNumber);

		if (INVALID_CANCELLED_CLOSED_APPLICATION_STATUS.contains(ssap.getApplicationStatus())) {
			throw new InvalidStateException(String.format(DISCARD_APP_INVALID_STATE_MSG, ssap.getApplicationStatus(),
					Arrays.asList(INVALID_CANCELLED_CLOSED_APPLICATION_STATUS)));
		}

		AccountUser accountUser = validateAccountUser(lastUpdatedUserId);

		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository
				.findApplicationEventAndApplicantEventsBySsapApplication(ssap.getId());

		/*if (ssapApplicationEvent == null){
			throw new DataNotFoundException(NO_EVENTS_FOUND_FOR_APPLICATION + caseNumber);
		}*/

		Timestamp currentTimestamp = new Timestamp(new TSDate().getTime());

		/*if (currentTimestamp.before(ssapApplicationEvent.getEnrollmentEndDate())){
			throw new InvalidStateException(SEP_END_DATE_IS_STILL_VALID_FOR + ssapApplicationEvent.getEnrollmentEndDate());
		}*/

		List<Integer> submittedEventIds = new ArrayList<>();

		if(ssapApplicationEvent!=null){
			List<com.getinsured.iex.ssap.model.SsapApplicantEvent> applicantEvents = ssapApplicationEvent
					.getSsapApplicantEvents();

			if (ReferralUtil.listSize(applicantEvents) != ReferralConstants.NONE){
				boolean eventsUpdated = false;

				for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
					if (OPEN_EVENT_VALIDATION_STATUS.contains(ssapApplicantEvent.getValidationStatus())) {
						eventsUpdated = true;
						if (ssapApplicantEvent.getValidationStatus() == ApplicantEventValidationStatus.SUBMITTED) {
							submittedEventIds.add((int) ssapApplicantEvent.getId());
						}

						ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.CANCELLED);
						ssapApplicantEvent.setLastUpdatedBy(lastUpdatedUserId);
						ssapApplicantEvent.setLastUpdateTimestamp(currentTimestamp);
					}
				}

				if (eventsUpdated) {
					ssapApplicantEventRepository.save(applicantEvents);
				}
			}

		}

		if (OPEN_APPLICATION_VALIDATION_STATUS.contains(ssap.getValidationStatus())) {
			ssap.setValidationStatus(ApplicationValidationStatus.CANCELLED);
		}

		ssap.setAllowEnrollment(ReferralConstants.Y);
		ssap.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
		ssap.setSepQepDenied(ReferralConstants.Y);
		ssap.setLastUpdateTimestamp(currentTimestamp);
		ssap.setLastUpdatedBy(new BigDecimal(lastUpdatedUserId));
		ssapApplicationRepository.save(ssap);

		if (ReferralUtil.listSize(submittedEventIds) != ReferralConstants.NONE) {
			cancelTickets(accountUser, submittedEventIds, SEP_DENIED);
			updateCmrDocuments(accountUser, submittedEventIds, AcceptedValue.N, SEP_DENIED);
		}

		return true;
	}

	private ApplicationValidationStatus rerunValidationEngine(String caseNumber, Integer lastUpdatedBy) {
		SsapApplication ssap = validateCaseNumber(caseNumber);

		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository
				.findApplicationEventAndApplicantEventsBySsapApplication(ssap.getId());

		List<com.getinsured.iex.ssap.model.SsapApplicantEvent> applicantEvents = ssapApplicationEvent
				.getSsapApplicantEvents();

		boolean allEventsVerified = areEventsVerified(applicantEvents);

		if (allEventsVerified) {
			ssap.setValidationStatus(ApplicationValidationStatus.VERIFIED);
			ssap.setAllowEnrollment(EligibilityConstants.Y);
			ssap.setLastUpdatedBy(new BigDecimal(lastUpdatedBy));
			ssap.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
			ssapApplicationRepository.save(ssap);
		} else {
			/* handle exit path */
			if (ssap.getValidationStatus() != ApplicationValidationStatus.PENDING) {
				ssap.setValidationStatus(ApplicationValidationStatus.PENDING);
				ssap.setAllowEnrollment(EligibilityConstants.Y);
				ssap.setLastUpdatedBy(new BigDecimal(lastUpdatedBy));
				ssap.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
				ssapApplicationRepository.save(ssap);
			}
		}

		return ssap.getValidationStatus();

	}

	private boolean areEventsVerified(List<com.getinsured.iex.ssap.model.SsapApplicantEvent> applicantEvents) {

		boolean allEventsVerified = true;
		if (applicantEvents != null){
			for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
				if (OPEN_EVENT_VALIDATION_STATUS.contains(ssapApplicantEvent.getValidationStatus())) {
					allEventsVerified = false;
					break;
				}
			}
		}
		return allEventsVerified;
	}

	private com.getinsured.iex.ssap.model.SsapApplicantEvent validateApplicantEvent(Long applicantEventID,
			SsapApplicantEvent event) {

		com.getinsured.iex.ssap.model.SsapApplicantEvent applicantEvent = ssapApplicantEventRepository.findOne(applicantEventID);

		if (applicantEvent == null) {
			throw new DataNotFoundException(NO_APPLICANT_EVENT_FOUND_FOR + applicantEventID);
		}

		if (!applicantEvent.getValidationStatus().isNextStatusValid(event.getStatus())) {
			throw new InvalidStateException(String.format(INVALID_VALIDATION_STATUS_STATE_MSG,
					applicantEvent.getValidationStatus(), event.getStatus()));
		}

		return applicantEvent;
	}

	private static final String OE = "OE";
	private static final String QEP = "QEP";
	private static final String SEP = "SEP";

	@Override
	public Optional<ApplicationValidationStatus> runValidationEngine(String caseNumber, Source sourceType,
			Integer lastUpdatedBy) {

		SsapApplication ssap = validateCaseNumber(caseNumber);

		validateAccountUser(lastUpdatedBy);
		validateSourceType(sourceType);

		if (INVALID_CANCELLED_CLOSED_APPLICATION_STATUS.contains(ssap.getApplicationStatus())) {
			throw new InvalidStateException(String.format(DISCARD_APP_INVALID_STATE_MSG, ssap.getApplicationStatus(),
					Arrays.asList(INVALID_CANCELLED_CLOSED_APPLICATION_STATUS)));
		}

		SsapApplicationEvent ssapApplicationEvent = validateSsapApplicantEvents(caseNumber, ssap);

		List<com.getinsured.iex.ssap.model.SsapApplicantEvent> applicantEvents = ssapApplicationEvent.getSsapApplicantEvents();

		Timestamp currentTimestamp = new Timestamp(new TSDate().getTime());

		if (!StringUtils.equals(ssap.getApplicationType(), OE)){
			if (skipSepAppForOEP(ssap)){
				if (ReferralUtil.listSize(applicantEvents) != ReferralConstants.NONE){
					for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
						ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.NOTREQUIRED);
					}
				}
			} else {
				for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
					if (sourceType == Source.EXTERNAL){ /* this means referral account transfer flow */
						if (ssapApplicantEvent.getSepEvents().getGated() == Gated.Y
								&& ssapApplicantEvent.getSepEvents().getSource() == Source.EXCHANGE){
							ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.PENDING);
						} else {
							ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.NOTREQUIRED);
						}
					} else { /* this means F/NF manual flow */
						if (ssapApplicantEvent.getSepEvents().getGated() == Gated.Y){
							ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.PENDING);
						} else {
							ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.NOTREQUIRED);
						}
					}
					ssapApplicantEvent.setLastUpdatedBy(lastUpdatedBy);
					ssapApplicantEvent.setLastUpdateTimestamp(currentTimestamp);
				}

				/** NOTREQUIRED for non-primary for QEP flow */
				if (StringUtils.equals(ssap.getApplicationType(), QEP)){
					for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
						if (ssapApplicantEvent.getSsapApplicant().getPersonId() != 1){
							ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.NOTREQUIRED);
						}
					}
				}
			}
		} else {
			if (ReferralUtil.listSize(applicantEvents) != ReferralConstants.NONE){
				for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
					ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.NOTREQUIRED);
				}
			}
		}

		if (ReferralUtil.listSize(applicantEvents) != ReferralConstants.NONE){
			ssapApplicantEventRepository.save(applicantEvents);
		}

		boolean allEventsVerified = areEventsVerified(applicantEvents);

		ssap.setAllowEnrollment(EligibilityConstants.Y);
		if (allEventsVerified){
			ssap.setValidationStatus(ApplicationValidationStatus.NOTREQUIRED);
		} else {
			ssap.setValidationStatus(ApplicationValidationStatus.PENDING);
		}

		ssap.setLastUpdatedBy(new BigDecimal(lastUpdatedBy));
		ssap.setLastUpdateTimestamp(currentTimestamp);

		ssapApplicationRepository.save(ssap);

		return Optional.of(ssap.getValidationStatus());
	}

	private boolean skipSepAppForOEP(SsapApplication ssap) {
		return isSkipOepQleValidationEnabled() && isOEPeriod(ssap.getCoverageYear()) && StringUtils.equals(ssap.getApplicationType(), SEP);
	}

	private boolean isSkipOepQleValidationEnabled() {
		String skipSepForOE = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_SKIP_OEP_QLE_VALIDATION);
		return StringUtils.equalsIgnoreCase(skipSepForOE, ReferralConstants.Y);
	}

	@Autowired
	@Qualifier("referralOEService")
	private ReferralOEService referralOEService;

	boolean isOEPeriod(long coverageYear) {
		return referralOEService.isOpenEnrollment(coverageYear);
	}

	private SsapApplicationEvent validateSsapApplicantEvents(String caseNumber, SsapApplication ssap) {
		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository
				.findApplicationEventAndApplicantEventsBySsapApplication(ssap.getId());

		if (StringUtils.equals(ssap.getApplicationType(), OE)){
			if (ssapApplicationEvent == null){
				throw new DataNotFoundException(NO_EVENTS_FOUND_FOR_APPLICATION + caseNumber);
			}
		} else {
			if (ssapApplicationEvent == null
					|| ReferralUtil.listSize(ssapApplicationEvent.getSsapApplicantEvents()) == ReferralConstants.NONE){
				throw new DataNotFoundException(NO_EVENTS_FOUND_FOR_APPLICATION + caseNumber);
			}
		}
		return ssapApplicationEvent;
	}

	private void validateSourceType(Source sourceType) {
		if (sourceType == null) {
			throw new InvalidIncomingValueException(INVALID_SOURCE_TYPE_MUST_BE + Arrays.asList(Source.EXCHANGE, Source.EXTERNAL));
		}
	}

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;

	@Override
	public boolean cancelEventsAndTickets(String caseNumber, Integer lastUpdatedUserId) {

		SsapApplication ssap = validateCaseNumber(caseNumber);
		AccountUser accountUser = validateAccountUser(lastUpdatedUserId);

		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository
				.findApplicationEventAndApplicantEventsBySsapApplication(ssap.getId());

		if (ssapApplicationEvent == null){
			throw new DataNotFoundException(NO_EVENTS_FOUND_FOR_APPLICATION + caseNumber);
		}

		Timestamp currentTimestamp = new Timestamp(new TSDate().getTime());
		List<Integer> submittedEventIds = new ArrayList<>();

		List<com.getinsured.iex.ssap.model.SsapApplicantEvent> applicantEvents = ssapApplicationEvent
				.getSsapApplicantEvents();

		if (ReferralUtil.listSize(applicantEvents) != ReferralConstants.NONE){
			boolean eventsUpdated = false;

			for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
				if (OPEN_EVENT_VALIDATION_STATUS.contains(ssapApplicantEvent.getValidationStatus())) {
					eventsUpdated = true;
					if (ssapApplicantEvent.getValidationStatus() == ApplicantEventValidationStatus.SUBMITTED) {
						submittedEventIds.add((int) ssapApplicantEvent.getId());
					}

					ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.CANCELLED);
					ssapApplicantEvent.setLastUpdatedBy(lastUpdatedUserId);
					ssapApplicantEvent.setLastUpdateTimestamp(currentTimestamp);
				}
			}

			if (eventsUpdated) {
				ssapApplicantEventRepository.save(applicantEvents);
			}
		}



		if (OPEN_APPLICATION_VALIDATION_STATUS.contains(ssap.getValidationStatus())) {
			ssap.setValidationStatus(ApplicationValidationStatus.CANCELLED);
			ssapApplicationRepository.save(ssap);
		}

		if (ReferralUtil.listSize(submittedEventIds) != ReferralConstants.NONE) {
			cancelTickets(accountUser, submittedEventIds, SEP_DENIED);
			updateCmrDocuments(accountUser, submittedEventIds, AcceptedValue.N, SEP_DENIED);
		}

		return true;
	}

	@Override
	public void ignoreNonPrimaryEvents(String caseNumber, Integer lastUpdatedBy) {
		SsapApplication ssap = validateCaseNumber(caseNumber);

		validateAccountUser(lastUpdatedBy);

		if (INVALID_CANCELLED_CLOSED_APPLICATION_STATUS.contains(ssap.getApplicationStatus())) {
			throw new InvalidStateException(String.format(DISCARD_APP_INVALID_STATE_MSG, ssap.getApplicationStatus(),
					Arrays.asList(INVALID_CANCELLED_CLOSED_APPLICATION_STATUS)));
		}

		SsapApplicationEvent ssapApplicationEvent = validateSsapApplicantEvents(caseNumber, ssap);

		List<com.getinsured.iex.ssap.model.SsapApplicantEvent> applicantEvents = ssapApplicationEvent.getSsapApplicantEvents();

		Timestamp currentTimestamp = new Timestamp(new TSDate().getTime());

		boolean eventsUpdated = false;
		if (ReferralUtil.listSize(applicantEvents) != ReferralConstants.NONE){
			for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
				if (ssapApplicantEvent.getSsapApplicant().getPersonId() != 1){
					ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.NOTREQUIRED);
					ssapApplicantEvent.setLastUpdatedBy(lastUpdatedBy);
					ssapApplicantEvent.setLastUpdateTimestamp(currentTimestamp);
					eventsUpdated = true;
				}
			}
		}

		if (eventsUpdated){
			ssapApplicantEventRepository.save(applicantEvents);
		}

	}

	@Override
	public boolean cleanUpApplicantEvent(List<Long> applivantEventIds, Integer lastUpdatedBy) {

		List<Integer> submittedAndVerifiedEventIds = new ArrayList<>();
		List<Integer> submittedEventIds = new ArrayList<>();
		AccountUser accountUser = validateAccountUser(lastUpdatedBy);
		List<com.getinsured.iex.ssap.model.SsapApplicantEvent> applicantEventList = ssapApplicantEventRepository.findBySsapApplicantEventId(applivantEventIds);
		Timestamp currentTimestamp = new Timestamp(new TSDate().getTime());
		boolean eventsUpdated = false;

		if (ReferralUtil.listSize(applicantEventList) > 0) {
		    for(com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEventList) {
				if (ssapApplicantEvent.getValidationStatus() == ApplicantEventValidationStatus.SUBMITTED ||
						ssapApplicantEvent.getValidationStatus() == ApplicantEventValidationStatus.VERIFIED) {
					submittedAndVerifiedEventIds.add((int) ssapApplicantEvent.getId());
					if(ssapApplicantEvent.getValidationStatus() == ApplicantEventValidationStatus.SUBMITTED){
						submittedEventIds.add((int) ssapApplicantEvent.getId());
					}
				}
				if(ssapApplicantEvent.getValidationStatus() != ApplicantEventValidationStatus.INITIAL){
					eventsUpdated = true;
					ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.INITIAL);
					ssapApplicantEvent.setLastUpdatedBy(lastUpdatedBy);
					ssapApplicantEvent.setLastUpdateTimestamp(currentTimestamp);
				}
			}
		}

		if (eventsUpdated) {
			ssapApplicantEventRepository.save(applicantEventList);
		}

		if (ReferralUtil.listSize(submittedEventIds) > 0) {
			cancelTickets(accountUser, submittedEventIds, SEP_OVERRIDE);
		}
		if(ReferralUtil.listSize(submittedAndVerifiedEventIds) > 0) {
			updateCmrDocuments(accountUser, submittedEventIds, AcceptedValue.N, SEP_OVERRIDE);
		}

		return true;
	}

	@Override
	public Optional<ApplicationValidationStatus> reRunValidationEngineAfterSEPOverride(SsapApplication ssap,SsapApplicationEvent ssapApplicationEvent, Integer lastUpdatedBy) {

		List<com.getinsured.iex.ssap.model.SsapApplicantEvent> applicantEvents = ssapApplicationEvent.getSsapApplicantEvents();

		Timestamp currentTimestamp = new Timestamp(new TSDate().getTime());

		if (!StringUtils.equals(ssap.getApplicationType(), OE)){
			for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
				if(ssapApplicantEvent.getValidationStatus() == ApplicantEventValidationStatus.INITIAL){
					if (ssapApplicantEvent.getSepEvents().getGated() == Gated.Y){
						ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.PENDING);
					} else {
						ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.NOTREQUIRED);
					}
				    ssapApplicantEvent.setLastUpdatedBy(lastUpdatedBy);
					ssapApplicantEvent.setLastUpdateTimestamp(currentTimestamp);
				}
			}

			if (StringUtils.equals(ssap.getApplicationType(), QEP)){
				for (com.getinsured.iex.ssap.model.SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
					if (ssapApplicantEvent.getSsapApplicant().getPersonId() != 1){
						ssapApplicantEvent.setValidationStatus(ApplicantEventValidationStatus.NOTREQUIRED);
					}
				}
			}

		}

		if (ReferralUtil.listSize(applicantEvents) != ReferralConstants.NONE){
			ssapApplicantEventRepository.save(applicantEvents);
		}

		boolean allEventsVerified = areEventsVerified(applicantEvents);

		ssap.setAllowEnrollment(EligibilityConstants.Y);
		if (allEventsVerified){
			if(ssap.getValidationStatus() != ApplicationValidationStatus.VERIFIED){
			ssap.setValidationStatus(ApplicationValidationStatus.NOTREQUIRED);
			}
		} else {
			ssap.setValidationStatus(ApplicationValidationStatus.PENDING);
		}

		ssap.setLastUpdatedBy(new BigDecimal(lastUpdatedBy));
		ssap.setLastUpdateTimestamp(currentTimestamp);

		ssapApplicationRepository.save(ssap);

		return Optional.of(ssap.getValidationStatus());
	}

}


