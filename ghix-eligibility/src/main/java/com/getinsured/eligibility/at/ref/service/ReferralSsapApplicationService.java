package com.getinsured.eligibility.at.ref.service;

import java.util.List;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * @author chopra_s
 *
 */
public interface ReferralSsapApplicationService {
	SsapApplication executeCreate(String ssapJson, SingleStreamlinedApplication singleStreamlinedApplication, AccountTransferRequestDTO accountTransferRequest);

	SsapApplication executeCreate(String ssapJson, SingleStreamlinedApplication singleStreamlinedApplication);

	void populateSsapApplicants(SsapApplication ssapApplication, HouseholdMember member, String onApplication, boolean isQe, List<BloodRelationship> bloodRelationship, String responsibleMemberPersonId);
}
