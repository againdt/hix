<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld"	prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<div class="row-fluid">
		<div class="gutter10">
			<!--page-breadcrumb ends-->
			<h1 id="skip"><spring:message code = "label.setEffectiveDt"/></h1>
		</div>
	</div> 
		<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="accordion graysidebar" id="accordion2">
				<util:nav pageName="${page_name}"></util:nav>
			</div>
			<c:if test="${errorMsg == '' }">
				<util:selection pageName="${page_name}"></util:selection>
			</c:if>
		</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="graydrkaction margin0">
                    <h4 class="span10"><spring:message code = "label.selectCovEffDtText"/></h4>
                </div>
				<div class="gutter10">
					<div class="profile">
					<fieldset>
					<legend class="aria-hidden"><spring:message code = "label.selectCovEffDtText"/></legend>
                        <p><spring:message code = "label.selectCovEffDtDesc"/></p></br></br>
                    	<form class="form-horizontal" id="frmselectcoverage" name="frmselectcoverage" action="coveragestart" method="POST">
						<df:csrfToken/>
                    	<input type="hidden" name="pgname" value="selectcoverage"/>    
                        	<div class="effectiveDate">
                        		<c:forEach items="${coverageDates}" var="coverageDate"  varStatus="counter">
                        			<div class="enrollmentPeriodDate pull-left">
										<input type="radio" name="coverageDateStart" id="coverageDateStart_${counter.count}" value="${coverageDate[0] }" class="pull-left" <c:if test="${coverageDate[0] == selectedDate}"> checked="checked" </c:if> onClick="clearNavigation('${coverageDate[0]}')"/><label for="coverageDateStart_${counter.count}"><span class="paddingRL5">${coverageDate[0] }</span> <span class="aria-hidden"><spring:message code = "label.openEnroll"/> ${coverageDate[1] } Help text <spring:message code = "label.openEnrollDesc"/> Help text finished</span> <span class="aria-hidden"><spring:message code = "label.firstPayment"/> ${coverageDate[2] } Help text <spring:message code = "label.paymentComments"/> Help text finished</span>	</label>
                        					<div class="marginL15">
                        						<p class="font10 marginTop10"><small><spring:message code = "label.openEnroll"/> 
                        						<a href="javascript:void(0)" alt="tooltip for open enrollment" rel="tooltip" data-placement="top" data-original-title="<spring:message code = "label.openEnrollDesc"/>" class="info">
													<i class="icon-question-sign"></i></a>
													</small><br>
                        						<strong>${coverageDate[1] }</strong></p>
                        					
                        						<p class="font10 marginTop10"><small><spring:message code = "label.firstPayment"/> <a href="javascript:void(0)" rel="tooltip" data-placement="top" data-original-title="<spring:message code = "label.paymentComments"/>" class="info">
													<i class="icon-question-sign"></i></a></small><br>
                        						<strong>${coverageDate[2] }</strong></p>
                        					</div>                       			
                        			</div>
                        		</c:forEach>
                        	</div>
                 			<div id="coverageDateStart_error"></div>
                 			<div id="activeEnrollment" class="modal hide fade" data-backdrop="static" tabindex="-1"  role="dialog" aria-labelledby="activeEnroll" aria-hidden="true">
				               <div class="modal-header">
				                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
				               </div>
				               <div class="modal-body" id="activeEnrollmentMsg"><spring:message code = "label.covEffectiveMsg"/></div>
				               <div class="modal-footer">
				                 <input type="button" data-dismiss="modal" id="activeEnrollmentButton" class="btn btn-primary" value="OK" />
				               </div>
				            </div>
                        </form>
                        <p class="paddingT20 clear"><%-- <strong><spring:message code = "label.note"/></strong> <spring:message code = "label.noteDesc"/> --%></p>
						</fieldset>
					</div>	
					
						<div class="form-actions">
	                    	<a href="<c:url value="/shop/employer/plandisplay/coveragestart"/>" class="btn"><spring:message  code="label.back"/></a>
	                        <input type="submit" name="submitButton" id="submitButton" onClick="javascript:submitForm();" value="<spring:message  code="label.next"/>" class="btn btn-primary pull-right" title="<spring:message  code="label.next"/>"/>
	                    </div>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->


<script type="text/javascript">
	function submitForm() {
		if( $("#frmselectcoverage").validate().form() ) {
			$("#frmselectcoverage").submit();
		}
	}
	
	var validator1 = $('#frmselectcoverage').validate({ 
		onkeyup: false,
		onclick: false,
		onSubmit: true,
		onfocusout: false,
		rules : {
			coverageDateStart : { required : true}
		},
		messages : {
			coverageDateStart : { required : "<span> <em class='excl'>!</em><spring:message code='label.selectCovDt' javaScriptEscape='true'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementName = element.attr('name');
			error.appendTo( $("#" + elementName + "_error"));
			$("#" + elementName + "_error").attr('class','error help-inline');
		} 
	});
	
	function clearNavigation(coverageDate){
		if(coverageDate != '${selectedDate}'){
			$('li.navmenu').each(function(index, element) {
				var index = index+1;
				if(index > 2){
					var link = $(this).find($('a')).attr('href');
					var finalHtml = $(this).html().replace('<i class="icon-ok"></i>', '<span>' + index +'. </span>').replace(link, '#');
			        $(this).html(finalHtml);
				}
			});
		}
		
	}
	if($('input:radio[name=date]').is(':checked')){
		$('input[name=date]:checked', '#frmGettingDate').parent().addClass("gettingActiveDate");		
	}
	
	$('input:radio[name=date]').change(function(){
		$(this).parents('.effectiveDate').find('.gettingActiveDate').removeClass("gettingActiveDate");
		$(this).parent().addClass("gettingActiveDate");
	});
	$('.info').tooltip()
	

$(document).ready(function(){
	var isActiveEnrollExist = ${isActiveEnrollExist};
	if(isActiveEnrollExist == true){
		$("#activeEnrollment").modal('show');
	}
});
</script>
