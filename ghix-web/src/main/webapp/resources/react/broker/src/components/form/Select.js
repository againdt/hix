import React from 'react';
import classNames from 'classnames';
import InputFeedback from './InputFeedback';

const Select = ({
  input,
  meta,
  id,
  label,
  labelClassName,
  selectClassName,
  ...props
}) => {
  let labelHTML = props.label ? (
    <label
      className={classNames('ds-c-label ds-u-margin-top--0', labelClassName)}
      htmlFor={id}
    >
      {label}
    </label>
  ) : (
    ''
  );

  return (
    <div>
      {labelHTML}
      <select
        className={classNames(
          'ds-c-field',
          selectClassName,
          meta.touched && meta.error
            ? 'ds-c-field--inverse ds-c-field--error'
            : ''
        )}
        name={input.name}
        id={id}
        value={input.value}
        onChange={input.onChange}
        onBlur={input.onBlur}
        disabled={props.disabled}
      >
        <option
          disabled
          value={props.defaultOptionValue}
          key={props.defaultOptionValue}
        >
          {props.defaultOptionLabel}
        </option>
        {props.choices}
      </select>
      {meta.touched && meta.error && (
        <InputFeedback
          error={
            <div class="error help-inline">
              <span className="error">
                <em className="excl">!</em>
                {/*<i className="fas fa-exclamation-circle" />*/}
                {meta.error}
              </span>
            </div>
          }
          errorClasses="ds-c-field__hint nv-label-error"
        />
      )}
    </div>
  );
};

export default Select;
