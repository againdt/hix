package com.getinsured.eligibility.at.ref.common;

/**
 * Added this enum for Type of Assiter
 * It can be Broker, Navigator or CAC
 * @author sahasrabuddhe_n
 *
 */
public enum AssisterTypeEnum {

	BROKER("BROKER","Broker"), NAVIGATOR("NAVIGATOR","Navigator"), CAC("CAC","Certified Application Counselor");

	public String value() {
		return name();
	}
	
	public String assisterTypeCode;
	public String assisterTypeValue;

	public static AssisterTypeEnum fromValue(String v) {
		AssisterTypeEnum data = null;
		for (AssisterTypeEnum c : AssisterTypeEnum.class.getEnumConstants()) {
			if (c.value().equals(v)) {
				data = c;
				break;
			}
		}
		return data;
	}

	@Override
	public String toString() {
		return value();
	}

	private AssisterTypeEnum(String assisterTypeCode, String assisterTypeValue) {
		this.assisterTypeCode = assisterTypeCode;
		this.assisterTypeValue = assisterTypeValue;
	}
	
}
