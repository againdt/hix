<%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>
<%@ page import="com.getinsured.hix.platform.notification.CKEditorConfigHelper" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<!-- The following stylesheet link has already been moved to shell.jsp. 
	If you are working on this page try removing the stylesheet link.
	If the page works fine without it, just delete the entire line along with this comment. -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/ckeditor/ckeditor.js">
</script>

<div class="gutter10">
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<p class="alert alert-info"> <spring:message code="label.userNoteOne" /> </p>
			<p class="alert alert-info"> <spring:message code="label.userNoteTwo" /></p>
			 <select multiple id="noticeElementsSelecet" name="noticeElementsSelecet" size=10 style='height: 100%;' onchange="enablePasteButton();">
			<c:forEach var="desc" items="${elementsList}">
				<option value="${desc.internalName}">${desc.friendlyLabel}</option>
			</c:forEach> 
			</select>
			<a href="<c:url value="/admin/noticetypelist"/>"
						class="btn btn-small pull-right" id="AddItem" name="AddItem" disbled="true;"> <spring:message code="button.copyVariablesButton" /> </a>
		</div>
		<div class="span9" id="rightpanel">
			<div class="header">
			  <h4 ><spring:message code="label.editTemplate" />:  ${templateName}</h4>
			</div>
			<form id="saveNotificationTemplate" name="saveNotificationTemplate" >
			<input type="hidden" id="templateId" name ="templateId" value=${templateId} }>
			<div id="desc" class="control-group">
					<div class="controls">
						<div class="input" id="ckeditorDiv" name="ckeditorDiv">
							<c:if test="${errorMsg != null && errorMsg != ''}">
								<div style="color: red">
									<c:out value="${errorMsg}"></c:out>
								</div>
							</c:if>
						
							<c:if test="${errorMsg == null}">						
							<textarea name="description" id="description" class="xlarge"
								rows="2" cols="20">${template}</textarea>
								<script>
	                            	CKEDITOR.replace( 'description' ,{
	                            		customConfig : "<c:url value="../../resources/ckeditor_config.js"/>"  
	                            	});
                            	</script>
							</c:if>	
						</div>
						<div id="description_error"></div>
					</div>
				</div>
				<div  class="control-group">
					<div class="controls">
						<iframe id="previewArea" style="width: 500px; height: auto;  border: 2px solid #e4e4e4; display: none"></iframe> 
					</div>
				</div>
				<div  class="control-group" >
					<c:if test="${errorMsg == null}">
						<a class="btn btn-primary btn-small pull-right"
							 value="Preview" onclick="openDialog();" id="previewSaveButton"><spring:message code="button.preview" /></a>
					</c:if>
					<a class="btn btn-primary btn-small pull-right"
							 value="Preview" id="publishChanges" style="display: none"><spring:message code="button.publish" /></a>
					<a href="<c:url value="/admin/noticetypelist"/>"
						class="btn btn-small pull-right"><spring:message code="button.cancel" /></a>
					<a href="<c:url value="/admin/noticetypelist"/>"
						class="btn btn-primary pull-left" id="backToEdit" style="display: none"><spring:message code="button.backToEdit" /></a>

				</div>
				
				
		<div id="noticeElementsDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="noticeElementsDialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
				<div class="markCompleteHeader">
			    	<div class="header">
			            <h4 class="margin0 pull-left padding0"><spring:message
							code="label.elementsModelName" /></h4>
			            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose clearForm" title="x" type="button" onclick="javascript:cancelModel();"><spring:message code="btn.crossClose" /></button>
			        </div>
			    </div>
			    <div style="background-color:#FF9933;" >
			    	<h3 class="margin10-l"><spring:message code="label.elementsModelHeading" /></h3>
			    </div>
			    <div class="gutter10"  id="noticeElementsDialogDiv" >	
									
				</div>
			  
			   <div id="reclassifyDialogLoader" style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top: -52px;margin-left: -47px;">
					<img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
			   </div>
			   <div class="modal-footer clearfix">
				   <button id="reclassifyDialogCancelButton" class="btn pull-left clearForm" data-dismiss="modal" aria-hidden="true" onclick="javascript:cancelModel();"><spring:message code='button.cancel' /></button>
				   <button id="conitnuePreview" class="btn btn-primary abc" ><spring:message code='button.continue' /></button>
			   </div>
		</div>
		  </form>
	</div>
	</div>
</div>

<script type="text/javascript" >
	$('document').ready(function(){
	/* $('#previewArea').css('display','none');
	$('#backToEdit').css('display','none');
	$('#publishChanges').css('display','none'); */
	
	
});
var entries =new Array();
var elementsFriendlyNames =new Array()
function enablePasteButton(){
	if($('#noticeElementsSelecet').val()){
		//$('#AddItem').disabled(false);
		$("#AddItem").attr("disabled",false);
		$("#AddItem").addClass("btn-primary");
	}
}
$("#AddItem").click(function(event)
		  {
		    event.preventDefault(); // cancel default behavior
			var var1 = document.getElementById("noticeElementsSelecet")
		   for (i=0;i<var1.options.length;i++) {
		        if (var1.options[i].selected) {
		            CKEDITOR.instances.description.insertText(var1.options[i].value);
		        }
		    } 
		  });
		  
 function openDialog(){
	 var arr = new Array();
	 var i=0;
	 <c:forEach items="${elementsList}" var="element" varStatus="loop">
		
		
		if('${element.friendlyLabel}'=='headerContent'||'${element.friendlyLabel}'=='footerContent'){
			
		}
		else{
		var obj = new Object();
		obj['${element.internalName}'] = '${element.friendlyLabel}' ;
		entries[i]='${element.internalName}';
		elementsFriendlyNames[i]='${element.friendlyLabel}';
		i++;
		arr.push(obj);
		}
		
		
		
		
    </c:forEach>
    
	for (var i = 0 ; i < arr.length ; i++){
		var propertyLabel = document.createElement("label");
		 if(arr[i][entries[i]]=='headerContent'||arr[i][entries[i]]=='footerContent'){
			
		}
		else{ 
		propertyLabel.className="control-label span4";
		propertyLabel.innerHTML=arr[i][entries[i]];
		var controlDiv = document.createElement("div");
		controlDiv.className = "control-group";
	//	$('#tktFormPropertiesDialog').append(propertyLabel);
		controlDiv.appendChild(propertyLabel);
		var divv = document.createElement("div");
		divv.className = "controls";
		txtField = document.createElement("input");
		txtField.name="formPropertyTextField";
		txtField.id=entries[i];
		txtField.value=entries[i];
		divv.appendChild(txtField);
		controlDiv.appendChild(divv);
		$('#noticeElementsDialogDiv').append(controlDiv);
		
		td=document.createElement("br");
		$('#noticeElementsDialogDiv').append(td);	
		td=document.createElement("br");
	}
	}
	$('#noticeElementsDialog').modal('show');
	
 }
 
 function cancelModel(){
	 $('#noticeElementsDialog').modal('hide');
	 $('#noticeElementsDialogDiv').empty();	 
 }
 function saveTemplate(){
     var validateUrl =  "<c:url value="/admin/editTemplate" />";
     var successRedirect = "<c:url value="/admin/noticetypelist"/>" 
     CKEDITOR.instances.description.updateElement();
     var ContentFromEditor = CKEDITOR.instances.description.getData();
     var encodedHtml  = $('<div/>').text(ContentFromEditor).html();
     var dataToSend = JSON.stringify({'encodedHtml':encodedHtml});
     var csrftoken;
	 csrftoken= '${sessionScope.csrftoken}';
     	$.ajax({
		 type: "POST",
		 url: validateUrl,
		 data: {
			 	"description":dataToSend,
			 	"templateId":$("#templateId").val(),
			 	csrftoken :csrftoken
			 	},
		 success: function(data) {
			 					window.location.href=    successRedirect;   
		                     },
		 }); 
	 
 }
 
 $("#conitnuePreview").click(function(event){
		
	 event.preventDefault(); // cancel default behavior
	 var myObject = new Object();
	 for(i=0;i<entries.length;i++){
		 
		 if(entries[i]=='headerContent'||entries[i]=='footerContent'){
				
		}else{
		 if(document.getElementById(entries[i]).value){
			 myObject[elementsFriendlyNames[i]]= document.getElementById(entries[i]).value;
		 }
		 else{
			 myObject[elementsFriendlyNames[i]]= entries[i];
		 }
		}
	 }
	 
    
     var myString = JSON.stringify(myObject);
     $('#noticeElementsDialog').modal('hide');
     
     
     var validateUrl =  "<c:url value="/admin/prepareTemplateReview" />";
     CKEDITOR.instances.description.updateElement();
     var ContentFromEditor = CKEDITOR.instances.description.getData();
     var encodedHtml  = $('<div/>').text(ContentFromEditor).html();
     var dataToSend = JSON.stringify({'encodedHtml':encodedHtml});
     var csrftoken;
	 csrftoken= '${sessionScope.csrftoken}';
     	$.ajax({
		 type: "POST",
		 url: validateUrl,
		 data: {
			 	"description":myString,
			 	"templateId":dataToSend,
			 	csrftoken :csrftoken
			 	},
		 success: function(data) {
			 						if(data === ""){ 
			 							alert("Could Not Generate Template Review. Template might be corrupted.");
			 							return;
			 						}
			 						else{
			 							 enablePreview();
			 						}	
		                     },
         error: function(data) {
										alert("Could Not Generate Template Review. Template might be corrupted.");
										return;
             					},
		 async:   false
		 }); 
	 
 });
 
 function enablePreview(){
	 var templateViewUrl = "${pageContext.request.contextPath}/admin/returnTemplateReview";
	 document.getElementById('previewArea').setAttribute("src", templateViewUrl);
 	 $('#previewSaveButton').css('display','none');
 	 
 	 $("#previewArea").removeAttr('style').css({'display': 'block !important',
		   width : '100%',
		   height : '450px'
		});
	 $("#backToEdit").removeAttr('style').css("display", "block !important");
	 $('#desc').css('display','none');
	 $("#publishChanges").removeAttr('style').css("display", "block !important");
}
 
 $("#backToEdit").click(function(event){
		
	 event.preventDefault(); // cancel default behavior
	 $('#previewArea').css('display','none');
	 $('#backToEdit').css('display','none');
	 $("#desc").removeAttr('style').css("display", "block !important");
	 $('#publishChanges').css('display','none');
	 $("#previewSaveButton").removeAttr('style').css("display", "block !important");
	 $('#noticeElementsDialogDiv').empty();
 });
 
 $("#publishChanges").click(function(event){
	 $('#noticeElementsDialogDiv').empty();
	 var tr = confirm("Press OK To save the Changes In Template.");
	 if (tr == true) {
	     saveTemplate();
	 } else {
		 event.preventDefault();    
	 }
 });
 
 

</script> 	