<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<style type="text/css">
.modal .header{
	border-radius: 6px 6px 0 0 !important;
    padding: 10px !important;
}
.modal .dialogClose{
	margin: -4px !important;
    padding: 4px;
}
.control-group > span {
	display:inline-block; 
	width:130px;"
}
</style>
<input type="hidden" id="actionType" name="actionType" value="" />
<input type="hidden" id="ticketId" name="ticketId" value="${encTicketid}"> 

<!-- Cancel confirmation dialog  -->
<div id="reassignDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="reassignDialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="pull-left margin0-tb padding0"><spring:message code="option_tktTask.reAssign" /></h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossCloseReassign" class="close" title="x" type="button"><spring:message code="btn.crossClose" /></button>
        </div>
    </div>
  
  <div class="modal-body">
  		<div class="control-group margin0-tb">
  				<input type="hidden" id="tkmTaskId" name="" >	
				<div class="controls">
						<!-- below div will visible if user execute assign functionality -->
						<div style="display: none;" id="assignTktMsg" class="control-group">
							<span class="span5 txt-right">Assignment:</span> 
							<span class="span5">Choose a ticket workgroup and workgroup member to assign the ticket	to		</span>
						</div>
						
						<div class="control-group margin0-b margin10-t">
							<span class="span5 txt-right"><spring:message code="text_tkt.queue" /> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></span> 
							<span class="span5">
								<select class="input-medium" id="activitiTaskQueues" onchange="javascript:loadQueueUsers();" >
									<option value=""><spring:message code="label.select" /></option>
								</select>
							</span>
						</div>
						
						<div style="display: none;" id="selectTaskUser" class="control-group">
							<span class="span5 txt-right"><spring:message code="text_tkt.assignee" /> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></span> 
							<span class="span5">
							<select class="input-medium" id="activitiTaskUser"></select>
							</span>
						</div>
				</div>
				
		</div>
  </div>
  
  <div id="reassignTicketloader" style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top: -52px;margin-left: -47px;">
		<img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
  </div>
  <div class="modal-footer clearfix">
    <button id="reassignCancelBtn" class="btn pull-left" data-dismiss="modal" aria-hidden="true"><spring:message code='button.cancel' /></button>
    <button id="reassignBtn" class="btn btn-primary def" onclick="javascript:reassignSubmit();"><spring:message code='sp_button.submit' /></button>
  </div>
</div>
<!-- Cancel ticket dialog ends -->
<script type="text/javascript">
var queueName=null;
var taskClaimed=true;
function divpopulate(actionType){
	if(typeof actionType == "undefined"){
		actionType == "";
	}
	
	if (actionType === "Assign"){
		$("#actionType").val('Assign');
	}
	else{
		$("#actionType").val("");
	}
	var validateUrl = "<c:url value='/ticketmgmt/ticket/getTicketActiveTasks'/>";
	var encTicketId = document.getElementById("ticketId").value;
	$("#reassignBtn").attr("disabled",false);
	$("#crossCloseReassign").attr("disabled",false);
	$("#reassignCancelBtn").attr("disabled",false);
	$.ajax({
		url : validateUrl,
		type : "GET",
		cache: false,
		data : {
			ticketId : encTicketId
		},
		success: function(response){
			populateQueue(response);
			if ($("#actionType").val() === "Assign"){
				$("#assignTktMsg").show();
				$("#reassignDialog .header h4").html("Assign");
			} else {
				$("#assignTktMsg").hide();
				$("#reassignDialog .header h4").html("Reassign");
			}
			//$('#displayReassignDialog').click();
			$('#reassignDialog').modal('show');
   		},
  			error: function(e){
   			alert("No active task for the ticket");
   		},
	});
}	
function populateQueue(subListJSON){
	subListJSON = JSON.parse(subListJSON);
	$('#activitiTaskQueues')
    .find('option')
    .remove()
    .end()
    .append('<option value="Select">Select</option>')
    $('#activitiTaskUser')
    .find('option')
    .remove()
    .end()
    .append('<option value="Select">Select</option>')
	for (var key in subListJSON) {
		
		if(key!='Task_Status'&&key!='Task_Id'){
	    var queueName = subListJSON[key]
	    $('#activitiTaskQueues')
         .append($("<option></option>")
         .attr("value",key)
         .text(queueName));
		}
		else{
			if(key=='Task_Status'){
				if(subListJSON[key]=='Claimed'){
					
					taskClaimed=true;
				}
				else{
					taskClaimed=false;
				}
			}
			else{
				$("#tkmTaskId").val(subListJSON[key]);
			}
			$("#selectTaskUser").show();
		}
	}
	
}
function loadQueueUsers() {
	var activitiUserCombo = $('#activitiTaskUser').length
	if(activitiUserCombo>0){
	    $("#activitiTaskUser").empty();
	    $("#activitiTaskUser").attr("disabled", "disabled");
		var validateUrl = "<c:url value='/ticketmgmt/ticket/getAllUsersForQueue'/>";
		queueName = $("#activitiTaskQueues option:selected").text();
		$.ajax({
			url : validateUrl,
			type : "GET",
			data : {
				qName : queueName
			},
			success: function(response){
				try{
					JSON.parse(response);
					}
					catch(e){
						 $('#tkmErrorDiv').html(response);
					}
					populateTkmUsers(response);
				
	   		},
	  			error: function(e){
	  			    alert("Failed to load Queue Users"); 
	   		},
		});

	}
	
}

function populateTkmUsers(userListJSON) {
    
	userListJSON = JSON.parse(userListJSON);
	  $('#activitiTaskUser')
      .append($("<option></option>")
      .attr("value","")
      .text("Select"));
	  $("#activitiTaskUser").removeAttr("disabled");
	for (var key in userListJSON) {
	    var userName = userListJSON[key]
	    $('#activitiTaskUser')
         .append($("<option></option>")
         .attr("value",key)
         .text(userName)); 
	}}
	
function reassignSubmit(){
	//Check if group is selected
	 if($('#activitiTaskQueues').val()==''||$('#activitiTaskQueues').val()=='Select'){
		 alert("Please select a Queue Name");
		 return;
	 }
	 
	 //If task is claimed, check the users selection
	 if(taskClaimed){
		 //If the user is not selected
		 if($('#activitiTaskUser').length>0&&($('#activitiTaskUser').val()==''||$('#activitiTaskUser').val()=='Select')){
		 	alert("Please select Assignee");
		 	return;
		} 	
	}
	 
	 performReassign();
}

function performReassign(){
	var validateUrl = "<c:url value='/ticketmgmt/ticket/reassignTicket'/>";
	var taskId = $("#tkmTaskId").val();
	var assignee;
	var encTicketId = document.getElementById("ticketId").value;
	var ticketId = encTicketId ;
	var action="";
	if($("#actionType").val() === "Assign"){
		action = "Assign";
	}
	if($('#activitiTaskUser').length>0){
		assignee = $('#activitiTaskUser').val();
	}
	else{
		assignee=null;
	}
	var queueId = $('#activitiTaskQueues').val();
	$("#reassignTicketloader").show();
	$("#reassignBtn").attr("disabled",true);
	$("#reassignCancelBtn").attr("disabled",true);
	$("#crossCloseReassign").attr("disabled",true);
	
	$.ajax({
		url: validateUrl,
		data: {taskId : taskId,
			   queueName : queueId,
			   assignee  : assignee,
			   ticketId : encTicketId,
			   action : action},
		success: function(response)
		{
				$("#reassignTicketloader").hide();
				$('#reassignDialog').modal('hide');
				$('#reassignDialog a').removeAttr('href')
			 	notifyReassignResponse(true, ticketId,assignee,queueName,true);
		},
		error : function(error) {
			$("#reassignTicketloader").hide();
			notifyReassignResponse(false, ticketId,assignee,queueName,false);
		}
		});
	
}

</script>