package com.getinsured.iex.ssap;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.getinsured.iex.ssap.financial.AnnualTaxIncome;
import com.getinsured.iex.ssap.financial.Expense;
import com.getinsured.iex.ssap.financial.Income;
import com.getinsured.iex.ssap.financial.type.ExpenseType;
import com.getinsured.iex.ssap.financial.type.Frequency;
import com.getinsured.iex.ssap.financial.type.IncomeSubType;
import com.getinsured.iex.ssap.financial.type.IncomeType;

/**
 * Test cases for {@link HouseholdMember}
 * @author Yevgen Golubenko
 * @since 4/1/19
 */
public class HouseholdMemberTest {
  private static final Logger log = LoggerFactory.getLogger(HouseholdMemberTest.class);

  @Test
  public void testIncome() {
    Income jobIncome = new Income();
    Assert.assertEquals("Income's default type is not correct", IncomeType.UNSPECIFIED, jobIncome.getType());
    Assert.assertEquals("Income's default frequency is not correct", Frequency.UNSPECIFIED, jobIncome.getFrequency());
    jobIncome.setType(IncomeType.JOB);
    jobIncome.setSourceName("GetInsured, Inc");
    // $1,305.22
    jobIncome.setAmount(130522L);
    jobIncome.setFrequency(Frequency.MONTHLY);
    double resultDollarsCents = jobIncome.getAmount() / 100.0;
    Assert.assertEquals("Amount in dollars & cents is not correct", 1305.22D, resultDollarsCents, 0);
  }

  @Test
  public void testExpense() {
    Expense expense = new Expense();
    Assert.assertEquals("Expense's default type is not correct", ExpenseType.UNSPECIFIED, expense.getType());
    Assert.assertEquals("Expense's default frequency is not correct", Frequency.UNSPECIFIED, expense.getFrequency());

    expense.setType(ExpenseType.ALIMONY);
    expense.setFrequency(Frequency.BIWEEKLY);
    // $3,333.33
    expense.setAmount(333333);
    double resultDollarsCents = expense.getAmount() / 100.0;
    Assert.assertEquals("Amount in dollars & cents is not correct", 3_333.33, resultDollarsCents, 0);
  }

  @Test
  public void testHouseholdMemberIncomeExpense() throws JsonProcessingException {
    HouseholdMember hhm = new HouseholdMember();
    hhm.setPersonId(1);

    List<Income> incomes = new ArrayList<>();
    List<Expense> expenses = new ArrayList<>();

    Expense alimony = new Expense();
    alimony.setType(ExpenseType.ALIMONY);
    alimony.setFrequency(Frequency.BIWEEKLY);
    alimony.setAmount(130000);
    expenses.add(alimony);

    Expense studentLoan = new Expense();
    studentLoan.setType(ExpenseType.STUDENT_LOAN_INTEREST);
    studentLoan.setFrequency(Frequency.MONTHLY);
    studentLoan.setAmount(43200);
    expenses.add(studentLoan);

    Expense otherExpense = new Expense();
    otherExpense.setType(ExpenseType.OTHER);
    otherExpense.setFrequency(Frequency.BIMONTHLY);
    otherExpense.setAmount(8600);
    expenses.add(otherExpense);

    Income jobIncome = new Income();
    jobIncome.setType(IncomeType.JOB);
    jobIncome.setFrequency(Frequency.BIWEEKLY);
    jobIncome.setAmount(104800);
    jobIncome.setSourceName("GetInsured, Inc.");
    incomes.add(jobIncome);

    Income alimonyIncome = new Income();
    alimonyIncome.setType(IncomeType.ALIMONY);
    alimonyIncome.setFrequency(Frequency.MONTHLY);
    alimonyIncome.setAmount(130000);
    incomes.add(alimonyIncome);

    Income investmentIncome  = new Income();
    investmentIncome.setType(IncomeType.INVESTMENT);
    investmentIncome.setFrequency(Frequency.DAILY);
    investmentIncome.setAmount(3100);
    incomes.add(investmentIncome);

    Income capitalGainIncome = new Income();
    capitalGainIncome.setType(IncomeType.CAPITAL_GAIN);
    capitalGainIncome.setFrequency(Frequency.YEARLY);
    capitalGainIncome.setAmount(84100);
    incomes.add(capitalGainIncome);

    Income farmingFishingIncome = new Income();
    farmingFishingIncome.setType(IncomeType.FARMING_FISHING);
    farmingFishingIncome.setFrequency(Frequency.MONTHLY);
    farmingFishingIncome.setAmount(1200);
    incomes.add(farmingFishingIncome);

    Income pensionIncome = new Income();
    pensionIncome.setType(IncomeType.PENSION);
    pensionIncome.setFrequency(Frequency.MONTHLY);
    pensionIncome.setAmount(84100);
    incomes.add(pensionIncome);

    Income rentalIncome = new Income();
    rentalIncome.setType(IncomeType.RENTAL_ROYALTY);
    rentalIncome.setFrequency(Frequency.MONTHLY);
    rentalIncome.setAmount(30000);
    incomes.add(rentalIncome);

    Income retirementIncome = new Income();
    retirementIncome.setType(IncomeType.RETIREMENT);
    retirementIncome.setFrequency(Frequency.BIWEEKLY);
    retirementIncome.setAmount(56000);
    incomes.add(retirementIncome);

    Income scholarshipIncome = new Income();
    scholarshipIncome.setType(IncomeType.SCHOLARSHIP);
    scholarshipIncome.setFrequency(Frequency.BIWEEKLY);
    scholarshipIncome.setAmount(14311);
    scholarshipIncome.setRelatedExpense(43123);
    incomes.add(scholarshipIncome);

    Income selfEmploymentIncome = new Income();
    selfEmploymentIncome.setType(IncomeType.SELF_EMPLOYMENT);
    selfEmploymentIncome.setFrequency(Frequency.ONCE);
    selfEmploymentIncome.setAmount(300100);
    incomes.add(selfEmploymentIncome);

    Income socialSecurityIncome = new Income();
    socialSecurityIncome.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    socialSecurityIncome.setFrequency(Frequency.DAILY);
    socialSecurityIncome.setAmount(20100);
    incomes.add(socialSecurityIncome);

    Income unemploymentIncome = new Income();
    unemploymentIncome.setType(IncomeType.UNEMPLOYMENT);
    unemploymentIncome.setFrequency(Frequency.WEEKLY);
    unemploymentIncome.setAmount(31000);
    incomes.add(unemploymentIncome);

    Income otherIncome = new Income();
    otherIncome.setType(IncomeType.OTHER);
    otherIncome.setFrequency(Frequency.HOURLY);
    otherIncome.setAmount(2645);
    otherIncome.setSubType(IncomeSubType.GAMBLING_PRIZE_AWARD);
    incomes.add(otherIncome);

    hhm.setExpenses(expenses);
    hhm.setIncomes(incomes);

    AnnualTaxIncome annualTaxIncome = new AnnualTaxIncome();
    annualTaxIncome.setReportedIncome(4300000);
    annualTaxIncome.setProjectedIncome(4000000);

    hhm.setAnnualTaxIncome(annualTaxIncome);

    ObjectMapper om = new ObjectMapper();
    om.enable(SerializationFeature.INDENT_OUTPUT);

    String json = om.writeValueAsString(hhm);

    if(log.isInfoEnabled()) {
      log.info("JSON: {}", json);
    } else {
      System.out.format("JSON:%n%s", json);
    }

    System.out.println();
    hhm.getIncomes().forEach(i -> {
      System.out.format("%d => %.2f%n", i.getAmount(), i.getAmount()/100.0d);
    });
  }
}
