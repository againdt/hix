package com.getinsured.ahbx.broker.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.ahbx.broker.repository.IDesignateBrokerRepository;
import com.getinsured.ahbx.util.GhixAhbxConstants;

@Service("designateService")
@Transactional
public class DesignateBrokerServiceImpl implements DesignateService {
    private static final Logger LOGGER = LoggerFactory
	    .getLogger(DesignateBrokerServiceImpl.class);

    @Autowired
    private IDesignateBrokerRepository brokerDesignateRepository;

    /**
     * @see DesignateService#saveDesignateBroker(DesignateBroker)
     */
    @Override
    @Transactional
    public void saveDesignateBroker(DesignateBroker designateBroker) {

	LOGGER.debug("Saving DesignateBroker record for external Id. Broker Id: "
		+ designateBroker.getBrokerId());

	brokerDesignateRepository.save(designateBroker);
    }

    /**
     * @see DesignateService#findByBrokerIdAndExternalId(int, String, int)
     */
    @Override
    @Transactional
    public DesignateBroker findByBrokerIdAndExternalId(int brokerId,
	    String role, int externalId) {

	LOGGER.debug("Finding DesignateBroker record. Broker Id: " + brokerId
		+ " and External id: " + externalId + " and Role: " + role);
	DesignateBroker designateBroker = null;

	if (GhixAhbxConstants.EMPLOYER_ROLE.equals(role)) {
	    
	    designateBroker = brokerDesignateRepository
		    .findByBrokerIdAndExternalEmployerId(brokerId, externalId);
	} else if (GhixAhbxConstants.INDIVIDUAL_ROLE.equals(role)) {
	    
	    designateBroker = brokerDesignateRepository
		    .findByBrokerIdAndExternalIndividualId(brokerId, externalId);
	}

	return designateBroker;
    }
}
