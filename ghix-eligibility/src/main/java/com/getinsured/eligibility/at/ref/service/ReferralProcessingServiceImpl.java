package com.getinsured.eligibility.at.ref.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.at.ref.dozzer.assembler.SsapAssembler;
import com.getinsured.eligibility.at.ref.dozzer.assembler.SsapAssemblerImpl;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.ReferralActivationEnum;
import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import com.getinsured.eligibility.indportal.indCreation.IHousehold;
import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.phix.service.PhixService;
import com.getinsured.eligibility.referral.ui.dto.PendingReferralDTO;
import com.getinsured.eligibility.referral.ui.dto.ReferralVerification;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.eligibility.repository.ReferralActivationRepository;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.eligibility.util.RandomQuestionsUtil;
import com.getinsured.hix.dto.consumer.ssap.AptcHistoryRequest;
import com.getinsured.hix.dto.eligibility.PersonTypeDTO;
import com.getinsured.hix.dto.eligibility.PersonTypeRequest;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.referral.PrimaryApplicant;
import com.getinsured.iex.referral.ReferralLinkingOverride;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.AptcHistory;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;

/**
 * @author chopra_s
 * 
 */
@Component("referralProcessingService")
@DependsOn("dynamicPropertiesUtil")
@Scope("singleton")
public class ReferralProcessingServiceImpl implements ReferralProcessingService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralProcessingServiceImpl.class);

	private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("MM/dd/yyyy".intern());

	private static final int SECURITY_COUNT = 3;

	@Autowired
	@Qualifier("ssapAssembler")
	private SsapAssembler ssapAssembler;

	@Autowired
	@Qualifier("referralSsapApplicationService")
	private ReferralSsapApplicationService referralSsapApplicationService;

	@Autowired
	@Qualifier("referralWorkFlowService")
	private ReferralWorkFlowService referralWorkFlowService;

	@Autowired
	@Qualifier("referralAccountActivationService")
	private ReferralAccountActivationService referralAccountActivationService;

	@Autowired
	@Qualifier("referralActivationRepository")
	private ReferralActivationRepository referralActivationRepository;
	
	@Autowired private AptcHistoryRepository aptcHistoryRepository;

	@Autowired
	@Qualifier("referralCoverageYearService")
	private ReferralCoverageYearService referralCoverageYearService;

	@Autowired
	@Qualifier("referralSsapCmrLinkService")
	private ReferralSsapCmrLinkService referralSsapCmrLinkService;

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;

	@Autowired
	private RandomQuestionsUtil randomQuestionsUtil;

	@Autowired
	private PhixService phixService;

	@Autowired
	private ILocationRepository iLocationRepository;
	


	int PRIMARY_PERSON = 1;

	private static final String REFERRAL = "REFERRAL";

	@Override
	public String executeReferral(AccountTransferRequestDTO accountTransferRequest) {
		LOGGER.info("ReferralProcessingServiceImpl executeReferral starts ".intern());

		final ReferralResponse referralResponse = processReferral(accountTransferRequest);

		final String response = EligibilityUtils.marshal(referralResponse);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("ReferralProcessingServiceImpl executeReferral ends - Response is - {}".intern(), response);
		}
		return response;
	}

	private ReferralResponse processReferral(AccountTransferRequestDTO accountTransferRequest) {
		ReferralResponse referralResponse = null;
		try {
			referralResponse = new ReferralResponse();
			LOGGER.info("Creating a new Ssap Referral Application Starts".intern());
			final SingleStreamlinedApplication singleStreamlinedApplication = ssapAssembler.assembleSsapXmlToJson(accountTransferRequest);

			ssapAssembler.buildSsapBeanForJson(singleStreamlinedApplication);

			final String ssapJson = ssapAssembler.transformSsapToJson(singleStreamlinedApplication);
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("ssap Json\n" + ssapJson);
			}

			final SsapApplication ssapApplication = referralSsapApplicationService.executeCreate(ssapJson, singleStreamlinedApplication, accountTransferRequest);

			referralResponse.getData().put(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID, ssapApplication.getId());

			referralAccountActivationService.triggerAccountActivation(singleStreamlinedApplication, ssapApplication, accountTransferRequest);

			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(PROCESS_SUCCESS);
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

			LOGGER.info("Creating a new Ssap Referral Application Ends".intern());
		} catch (GIRuntimeException e) {
			LOGGER.error("Error at processReferral - " + e.getMessage(), e);
			referralResponse.setMessage("Error at processReferral" + e.getMessage());
		} catch (Exception e) {
			LOGGER.error("Error at processReferral - " + e.getMessage(), e);
			referralResponse.setMessage("Error at processReferral" + e.getMessage());
		} finally {
			referralResponse.getData().put(ReferralProcessingConstants.KEY_GI_WS_PAYLOAD_ID, accountTransferRequest.getGiwsPayloadId());
		}

		return referralResponse;
	}

	private long createReferral(AccountTransferRequestDTO accountTransferRequest) {
		final SingleStreamlinedApplication singleStreamlinedApplication = ssapAssembler.assembleSsapXmlToJson(accountTransferRequest);
		ssapAssembler.buildSsapBeanForJson(singleStreamlinedApplication);
		final String ssapJson = ssapAssembler.transformSsapToJson(singleStreamlinedApplication);
		if (LOGGER.isInfoEnabled()){
			LOGGER.info("ssap Json\n {}".intern(), ssapJson);
		}
		final SsapApplication ssapApplication = referralSsapApplicationService.executeCreate(ssapJson, singleStreamlinedApplication, accountTransferRequest);
		return ssapApplication.getId();
	}
	
	private long createATDto(AccountTransferRequestDTO accountTransferRequest) {
		long applicationID = ssapAssembler.compareAndUpdateStatus(accountTransferRequest);
		return applicationID;
	}

	@Override
	public long executeOeReferral(AccountTransferRequestDTO accountTransferRequest) throws Exception {
		long ssapApplicationId = 0;
		try {
			LOGGER.info("Creating a new Ssap Referral Application Starts".intern());
			ssapApplicationId = createReferral(accountTransferRequest);
			LOGGER.info("Creating a new Ssap Referral Application Ends".intern());
		} catch (Exception e) {
			throw e;
		}
		return ssapApplicationId;
	}

	@Override
	public long executeLceReferral(AccountTransferRequestDTO accountTransferRequest) throws Exception {
		long ssapApplicationId = 0;
		try {
			LOGGER.info("Creating a new Ssap Referral Application Starts".intern());
			ssapApplicationId = createReferral(accountTransferRequest);
			LOGGER.info("Creating a new Ssap Referral Application Ends".intern());
		} catch (Exception e) {
			throw e;
		}
		return ssapApplicationId;
	}

	@Override
	public long executeQeReferral(AccountTransferRequestDTO accountTransferRequest) throws Exception {
		long ssapApplicationId = 0;
		try {
			LOGGER.info("Creating a new Ssap Referral Application Starts".intern());
			ssapApplicationId = createReferral(accountTransferRequest);
			LOGGER.info("Creating a new Ssap Referral Application Ends".intern());
		} catch (Exception e) {
			throw e;
		}
		return ssapApplicationId;
	}

	/**
	 * Method is used to get Organization Identification ID from Authorized Representative which is used as External Applicant ID in users.
	 */
	@Override
	public String getOrganizationIdentification(AccountTransferRequestPayloadType accountTransferRequestPayloadType) {

		String identificationID = null;

		if (accountTransferRequestPayloadType != null && null != accountTransferRequestPayloadType.getAuthorizedRepresentative()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
						.getPersonOrganizationAssociation()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
						.getPersonOrganizationAssociation().getOrganization()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
						.getPersonOrganizationAssociation().getOrganization().getOrganizationIdentification()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
						.getPersonOrganizationAssociation().getOrganization().getOrganizationIdentification().getIdentificationID()
				&& null != accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
						.getPersonOrganizationAssociation().getOrganization().getOrganizationIdentification().getIdentificationID().getValue()) {

			identificationID = accountTransferRequestPayloadType.getAuthorizedRepresentative().getRolePlayedByPerson().getPersonAugmentation()
					.getPersonOrganizationAssociation().getOrganization().getOrganizationIdentification().getIdentificationID().getValue().trim();

			if (StringUtils.isBlank(identificationID)) {
				return null;
			}
		}
		return identificationID;
	}

	@Override
	public long executeRenewalReferral(AccountTransferRequestDTO accountTransferRequest) throws Exception {
		long ssapApplicationId = 0;
		try {
			LOGGER.info("Creating a new Ssap Referral Application Starts".intern());
			ssapApplicationId = createReferral(accountTransferRequest);
			LOGGER.info("Creating a new Ssap Referral Application Ends".intern());
		} catch (Exception e) {
			throw e;
		}
		return ssapApplicationId;
	}

	@Override
	public ReferralActivation referralActivationById(long id) {
		return referralActivationRepository.findOne(id);
	}

	@Override
	public ReferralActivation referralActivationBySsapApplication(int id) {
		return referralActivationRepository.findBySsapApplication(id);
	}

	@Override
	public ReferralActivation referralActivationByUserAndSsapApplication(int user, int ssapApplication) {
		return referralActivationRepository.findByUserAndSsapApplication(user, ssapApplication);
	}

	@Override
	public ReferralActivation referralActivationByCmrAndSsapApplication(int cmrHouseholdId, int ssapApplication) {
		return referralActivationRepository.findByCmrHouseholdAndSsapApplication(cmrHouseholdId, ssapApplication);
	}

	@Override
	@Transactional
	public Long linkUserCmrActivation(final Integer[] arguments) {
		long result = 0;
		LOGGER.info("Linking User with Ssap starts ".intern());
		if (arguments == null || arguments.length != 3) {
			LOGGER.warn(INCOMING_DATA_NOT_PROPER);
			return result;
		}

		final int userId = arguments[0];
		@SuppressWarnings("unused")
		final int cmrHouseHoldId = arguments[1];
		final int ssapApplicationId = arguments[2];

		ReferralActivation referralActivation = referralActivationRepository.findBySsapApplication(ssapApplicationId);

		if (referralActivation == null) {
			LOGGER.warn(NO_SSAP_FOUND + ssapApplicationId);
			return result;
		}

		if (ReferralUtil.isValidInt(referralActivation.getCmrHouseholdId())) {
			LOGGER.warn(UPDATION_NOT_ALLOWED);
			return result;
		}

		if (!referralActivation.canStartSecurity() || !referralActivation.getStatus().equals(ReferralConstants.ACTIVE)) {
			LOGGER.warn(INVALID_WORKFLOW);
			return result;
		}

		referralActivation.setUserId(userId);
		referralActivation.setLastUpdatedOn(new TSDate());
		referralActivation = referralActivationRepository.save(referralActivation);
		// Mark Account Activation Link as PROCESSED
		referralAccountActivationService.updateAccountActivation(ssapApplicationId);
		result = referralActivation.getId();

		LOGGER.info("Linking User with Ssap process done - result " + result);
		return result;
	}

	/*@SuppressWarnings("unused".intern())
	private boolean checkForApplicationsWithdifferntMedicaidId(ReferralActivation referralActivation, int cmrHouseHoldId) {
		if (isStateID()) {
			final String externalApplicationId = ssapApplicantRepository.getExternalIdBySsapApplicationIdandPersonId(referralActivation.getSsapApplicationId(), ReferralConstants.PRIMARY);
			long count = ssapApplicationRepository.countofApplicationswithDiffMedicaidIdFromSSAPApplication(referralActivation.getSsapApplicationId().intValue(), new BigDecimal(cmrHouseHoldId), externalApplicationId);
			if (count > 0) {
				return false;
			} else {
				count = ssapApplicationRepository.countofApplicationswithDiffMedicaidIdFromReferralActivation(referralActivation.getSsapApplicationId().intValue(), cmrHouseHoldId, externalApplicationId);
				if (count > 0) {
					return false;
				}
			}
		}
		return true;
	}*/

	@Override
	public Integer validateReferralAnswers(ReferralVerification referralVerification) {
		LOGGER.info("Validating ReferralVerification starts - " + referralVerification);
		int result = 0;
		final boolean blnFlag = runValidation(referralVerification);

		final ReferralActivation referralActivation = referralActivationRepository.findByCmrHouseholdAndSsapApplication(referralVerification.getReferralActivation().getCmrHouseholdId(), referralVerification.getReferralActivation()
		        .getSsapApplicationId());

		if (blnFlag) {
			referralActivation.setWorkflowStatus(ReferralActivationEnum.RIDP_PENDING);
			referralActivation.setLastUpdatedOn(new TSDate());
			referralActivationRepository.save(referralActivation);
			result = 1;
		} else if (referralVerification.isSecurityCheck()) {
			if (referralActivation.getSecurityRetryCount() >= SECURITY_COUNT) {
				referralActivation.setLastUpdatedOn(new TSDate());
				referralActivation.setWorkflowStatus(ReferralActivationEnum.SECURITY_FAILED);
				result = 2;
			}
			referralActivation.setSecurityRetryCount(referralActivation.getSecurityRetryCount() + 1);
			referralActivationRepository.save(referralActivation);
		}
		LOGGER.info("Validating ReferralVerification ends result - " + result);
		return result;
	}

	private boolean runValidation(ReferralVerification referralVerification) {
		boolean blnFlag = false;
		if (referralVerification == null) {
			LOGGER.warn(INCOMING_DATA_NOT_PROPER + referralVerification);
			return blnFlag;
		}

		final long ssapApplicationId = referralVerification.getReferralActivation().getSsapApplicationId();
		final String fname = referralVerification.getAnswer(FIRST_NAME);
		final String lname = referralVerification.getAnswer(LAST_NAME);
		final String dobStr = referralVerification.getAnswer(DOB2);
		final Date dob = validateIncomingData(ssapApplicationId, fname, lname, dobStr);

		if (dob == null) {
			LOGGER.warn(INCOMING_DATA_NOT_PROPER + referralVerification);
			return blnFlag;
		}

		final Object[] primaryApplicant = ssapApplicantRepository.findByPersonIdSelectedColumns(ssapApplicationId, 1);
		if (primaryApplicant == null) {
			LOGGER.warn(PRIMARY_APPLICANT_NOT_FOUND_FOR_SSAP_APPLICATION_ID + ssapApplicationId);
		} else {
			if (StringUtils.equalsIgnoreCase(fname, primaryApplicant[0] + "".intern()) && StringUtils.equalsIgnoreCase(lname, primaryApplicant[1] + "".intern()) && compareDate(dob, primaryApplicant[3])) {
				blnFlag = true;
			}
		}

		return blnFlag;
	}

	private static boolean compareDate(Date userEntryDate, Object dbCompareDate) {
		boolean blnReturn = false;
		if (dbCompareDate != null) {
			blnReturn = userEntryDate.compareTo((Date) dbCompareDate) == 0;
		}
		return blnReturn;
	}

	private Date validateIncomingData(long ssapApplicationId, String fname, String lname, String dobStr) {

		if (ssapApplicationId == 0 || StringUtils.isEmpty(fname) || StringUtils.isEmpty(lname)) {
			return null;
		}

		Date dob = null;
		try {
			dob = DATE_FORMATTER.parse(dobStr);
		} catch (ParseException e) {
			LOGGER.warn(INCOMING_DATA_DATE_FORMAT_NOT_PROPER + ssapApplicationId);
		}
		return dob;
	}

	@Override
	@Transactional
	public Integer updateActivationStatus(Integer[] arguments) {
		int result = 0;
		LOGGER.info("Updating Referral Activation Status Starts ".intern());

		if (arguments == null || arguments.length != 2) {
			LOGGER.warn(INCOMING_DATA_NOT_PROPER);
			return result;
		}

		final int referralActivationId = arguments[0];

		final ReferralActivation referralActivation = referralActivationRepository.findOne(new Long(referralActivationId));

		if (referralActivation == null) {
			LOGGER.warn(NO_REFERRALACTIVATION_FOUND);
			return result;
		}

		final int workflowStatusType = arguments[1];
		if (workflowStatusType == CSR_VERIFICATION_PENDING) {
			if (referralActivation.isSecurityFailed()) {
				referralActivation.setWorkflowStatus(ReferralActivationEnum.CSR_VERIFICATION_PENDING);
				referralActivation.setLastUpdatedOn(new TSDate());
				referralActivationRepository.save(referralActivation);
				result = 1;
			} else {
				result = 1;
			}

		}

		LOGGER.info("Updating Referral Activation Status ends - result " + result);
		return result;
	}

	@Override
	public Integer persistCoverageYear(Integer[] arguments) {
		return referralCoverageYearService.executeCoverageYear(arguments[0], arguments[1]);
	}

	@Override
	public Map<String, String> processRidp(Integer[] arguments) {
		return referralWorkFlowService.processRidpAndLinking(arguments[0], arguments[1], arguments[2]);
	}

	@SuppressWarnings("serial")
	@Override
	public List<PendingReferralDTO> pendingReferralsForCmr(int cmrHousehold) {
		final List<ReferralActivationEnum> statusList = new ArrayList<ReferralActivationEnum>() {
			{
				add(ReferralActivationEnum.ACTIVATION_COMPELETE);
				add(ReferralActivationEnum.CANCEL_ACTIVATION);
			}
		};

		return referralActivationRepository.findPendingReferrals(cmrHousehold, statusList);
	}

	@Override
	public Map<String, String> linkingStatusForCSR(long ssapplicationid, BigDecimal cmrhouseholdId) {
		ReferralActivation referralActivation = referralActivationRepository.findBySsapApplication((int) ssapplicationid);
		SsapApplication application = ssapApplicationRepository.findOne(Long.valueOf(ssapplicationid));
		long coverageYear = application.getCoverageYear();
		List<SsapApplication> ssapApplications = ssapApplicationRepository.findSsapByCmrHouseholdIdAppstatus(cmrhouseholdId, "EN".intern(), coverageYear);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			ssapApplications = ssapApplicationRepository.findSsapByCmrHouseholdIdAppstatus(cmrhouseholdId, "ER".intern(), coverageYear);
			// This is done to handle cancellation after dis-enroll as portal sets ER status
		}

		Map<String, String> statusMap = new HashMap<String, String>();
		statusMap.put("COVERAGE_YEAR".intern(), String.valueOf(coverageYear));
		if (referralActivation == null) {
			statusMap.put("REF_LCE_STATUS_FLG".intern(), "N".intern());
			statusMap.put("REF_LCE_CMR_ID".intern(), "".intern());
		} else {
			statusMap.put("REF_LCE_STATUS_FLG".intern(), referralActivation.getIsLce());
			statusMap.put("REF_LCE_CMR_ID".intern(), referralActivation.getCmrHouseholdId() != null ? referralActivation.getCmrHouseholdId().toString() : "".intern());
		}
		if (ssapApplications != null && ssapApplications.size() > 0) {
			statusMap.put("CMR_ENROLLED_FLG".intern(), "Y".intern());
		} else {
			statusMap.put("CMR_ENROLLED_FLG".intern(), "N".intern());
		}

		return statusMap;
	}

	@SuppressWarnings("serial")
	@Override
	public long getActiveApplicationCountForCMR(int cmrhouseholdId, int currSsapApplicationId) {

		long activeAppCount = 0;

		final List<String> statusList = new ArrayList<String>() {
			{
				add(ApplicationStatus.CANCELLED.getApplicationStatusCode());
				add(ApplicationStatus.CLOSED.getApplicationStatusCode());
			}
		};

		SsapApplication application = ssapApplicationRepository.findOne(Long.valueOf(currSsapApplicationId));
		long coverageYear = application.getCoverageYear();
		activeAppCount = ssapApplicationRepository.countOfNonFinancialAppByCmrAndApplicationStatus(new BigDecimal(cmrhouseholdId), coverageYear, statusList);

		return activeAppCount;
	}

	private boolean isStateID() {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		return "ID".equals(stateCode);
	}

	@Override
	public SsapApplicant getSsapApplicant(String applicantId) {
		SsapApplication ssapApplication = null;
		SsapApplicant ssapApplicant = null;

		List<SsapApplication> ssapApplications = null;
		List<SsapApplicant> ssapApplicants = null;

		try {
			ssapApplications = ssapApplicationRepository.findbyApplicantGuidForPrimary(applicantId);
			if (ReferralUtil.listSize(ssapApplications) != ReferralConstants.NONE){
				ssapApplication = ssapApplications.get(0);
				if (null != ssapApplication) {
					ssapApplicants = ssapApplication.getSsapApplicants();
					if (ReferralUtil.listSize(ssapApplicants) != ReferralConstants.NONE) {
						if(null!=ssapApplication.getCmrHouseoldId()) {
							Integer userId = cmrHouseholdRepository.findUserByCmrId(ssapApplication.getCmrHouseoldId().intValue());
							if(null == userId) {
								ssapApplicant = ssapApplicants.get(0);
							}
						}else {
							ssapApplicant = ssapApplicants.get(0);
						}
					}
				}
			}
			
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE FETCHING APPLICANT: ".intern(), ex);
		}

		return ssapApplicant;
	}
	
	private Map<Integer, String> getSsapQuestions(String applicantId, boolean isCsr, boolean updateCount) {
		String status = "INVALID";
		String ssapApplicationId = null;

		Map<Integer, String> questionsMap = new HashMap<Integer, String>();
		Map<Integer, String> birthMismatchMap  = new HashMap<Integer, String>();
		
		SsapApplicant ssapApplicant = null;
		List<Integer> households = null;

		ReferralActivation referralActivation = null;
		Date lockDate = null;

		try {

			ssapApplicant = getSsapApplicant(applicantId);

			if (null != ssapApplicant) {
				//check if this
				boolean hasHousehold = false;
				BigDecimal cmrHouseholdId = ssapApplicationRepository.getHouseholdIdByApplicantId(ssapApplicant.getId());
				if(cmrHouseholdId != null && BigDecimal.ZERO.compareTo(cmrHouseholdId) == -1)
				{
					households = new ArrayList<Integer>(1);
					households.add(cmrHouseholdId.intValue());
				}
				else 
				{
					Location location = null;
					if (ssapApplicant.getOtherLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getOtherLocationId().intValue())) {
						location = iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
					} else if (ssapApplicant.getMailiingLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getMailiingLocationId().intValue())) {
						location = iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
					}
					String zip = location != null ? location.getZip() : null;
					if(StringUtils.isNotBlank(ssapApplicant.getSsn()) && ssapApplicant.getBirthDate() !=null ) { 
						   households = cmrHouseholdRepository.findMatchingCmrBySsnDoB( ssapApplicant.getBirthDate(),ssapApplicant.getSsn());
						   // If hh Not found then only find by SSN and if found by SSN only then generate error-Message for DoB
						   if(null == households || households.isEmpty()) {
							 List<Household> hhObjectList =   cmrHouseholdRepository.findMatchingCmrObjectBySSN(ssapApplicant.getSsn());
							
							 if(null != hhObjectList && !hhObjectList.isEmpty()) {
								 birthMismatchMap.put(9001, "BIRTH_DATE_MISMATCH".intern());
								 birthMismatchMap.put(9002, Integer.toString(hhObjectList.get(0).getId() ));
								 birthMismatchMap.put(9003,ssapApplicant.getBirthDate().toString());
							 }
						   }
					} else {
						   households = cmrHouseholdRepository.findByFirstNameLastNameAndBirthDate(ReferralUtil.capitalizeFirstLetter(ssapApplicant.getFirstName()), ReferralUtil.capitalizeFirstLetter(ssapApplicant.getLastName()), ssapApplicant.getBirthDate(), zip);
					}
				}
				if (null != households && !households.isEmpty() && households.size() > 1) {
					status = "MULTIPLE_HOUSEHOLDS";
				} else {
					status = "VALID";

					ssapApplicationId = String.valueOf(ssapApplicant.getSsapApplication().getId());
					referralActivation = referralActivationRepository.findBySsapApplication(Integer.valueOf(ssapApplicationId));

					List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByAppId(Long.valueOf(ssapApplicationId));
					SsapApplication ssapApplication = null;
					int noOfApplicants = 0;
					if (ssapApplicationList != null && ssapApplicationList.size() > 0) {
						ssapApplication = ssapApplicationList.get(0);
						noOfApplicants = ReferralUtil.listSize(ssapApplication.getSsapApplicants());
					}

					if (!isCsr && referralActivation.getSecurityRetryCount() >= 4) {
						lockDate = referralActivation.getLockTime();
						if (null != lockDate) {
							Long currentTime = TimeShifterUtil.currentTimeMillis();
							Long lockTime = lockDate.getTime();
							if (currentTime < lockTime) {
								status = "REFERRAL_LOCKED";
							} else {
								questionsMap = randomQuestionsUtil.generateRandomList(ssapApplicant, noOfApplicants);
								if(updateCount && !isCsr){
									referralActivation.setSecurityRetryCount(1);
								}
								else{
									referralActivation.setSecurityRetryCount(0);
								}
								referralActivation.setLockTime(null);
								referralActivation.setLastUpdatedOn(new TSDate());
								referralActivationRepository.save(referralActivation);
							}
						}
					} else {
						if (updateCount && !isCsr) {
							updateSecurityCountDuringLanding(referralActivation);
						}
						questionsMap = randomQuestionsUtil.generateRandomList(ssapApplicant, noOfApplicants);
						referralActivation.setLastUpdatedOn(new TSDate());
						referralActivationRepository.save(referralActivation);
					}

					if (null != households && households.size() == 1) {
						questionsMap.put(1003, String.valueOf(households.get(0)));
					}
					questionsMap.put(1004, String.valueOf(referralActivation.getId()));
					questionsMap.put(1002, ssapApplicationId);
				}
			}
			
			if(!birthMismatchMap.isEmpty()) {
				questionsMap.putAll(birthMismatchMap);
			}
			
			questionsMap.put(1001, status);
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE FETCHING QUESTIONS: ".intern(), e);
		}

		return questionsMap;
	}
	
	@Override
	public Map<Integer, String> getSsapQuestionsFromHome(String applicantId, boolean isCsr) {
		return getSsapQuestions(applicantId, isCsr, false);
	}

	@Override
	public Map<Integer, String> getSsapQuestions(String applicantId, boolean isCsr) {
		return getSsapQuestions(applicantId, isCsr, true);
	}

	private void updateSecurityCountDuringLanding(ReferralActivation referralActivation) {
		int retryCount=referralActivation.getSecurityRetryCount();
		retryCount=retryCount+1;
		if(retryCount>=4){
		Calendar cal = TSCalendar.getInstance();
		cal.add(Calendar.MINUTE, 15);
		Date lockTime = cal.getTime();
		referralActivation.setLockTime(lockTime);
		}
		referralActivation.setSecurityRetryCount(retryCount);
		
	}

	@Override
	public String getApplicantGuid(String ssapApplicationId) {
		SsapApplication ssapApplication = null;
		SsapApplicant ssapApplicant = null;
		String applicantGuid = null;

		List<SsapApplication> ssapApplications = null;
		List<SsapApplicant> ssapApplicants = null;

		try {
			ssapApplications = ssapApplicationRepository.findByApplicationIdForPrimary(Long.valueOf(ssapApplicationId));
			if (null != ssapApplications && !ssapApplications.isEmpty()) {
				ssapApplication = ssapApplications.get(0);
				if (null != ssapApplication && ApplicationStatus.UNCLAIMED.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus())) {
					ssapApplicants = ssapApplication.getSsapApplicants();
					if (null != ssapApplicants && !ssapApplicants.isEmpty()) {
						ssapApplicant = ssapApplicants.get(0);
						applicantGuid = ssapApplicant.getApplicantGuid();
					}
				}
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE FETCHING APPLICANT: ".intern(), ex);
		}

		return applicantGuid;
	}

	@Override
	public Integer updateReferralActivation(String[] arguments) {

		int result = 0;
		LOGGER.info("Updating Referral Activation Status Starts ".intern());

		if (arguments == null || arguments.length != 2) {
			LOGGER.warn(INCOMING_DATA_NOT_PROPER);
			return result;
		}

		final int referralActivationId = Integer.valueOf(arguments[0]);

		final ReferralActivation referralActivation = referralActivationRepository.findOne(new Long(referralActivationId));

		if (referralActivation == null) {
			LOGGER.warn(NO_REFERRALACTIVATION_FOUND);
			return result;
		}

		final String workflowStatusType = arguments[1];
		if (StringUtils.isNotEmpty(workflowStatusType)) {
			referralActivation.setStatus(workflowStatusType);
			referralActivation.setLastUpdatedOn(new TSDate());
			referralActivationRepository.save(referralActivation);
		}
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Updating Referral Activation Status ends - result " + result);
		}
		return result;

	}

	@Override
	public int checkHouseholdExistsAndThenCreate(long ssapApplicationId, String householdCaseID) {

		final SsapApplicant ssapApplicant = ssapApplicantRepository.findByPersonId(ssapApplicationId, PRIMARY_PERSON);
		final String hasLinkUserConfig =  StringUtils.trimToEmpty(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_LINK_USER));
		boolean hasLinkUserEnabled = "TRUE".equalsIgnoreCase(hasLinkUserConfig);
		
		int householdId = 0;
		Household household = null;

		if (ssapApplicant == null) {
			throw new GIRuntimeException("Errow while loading primary for " + ssapApplicationId);
		}

		// check for existing household
		Location location = populateLocation(ssapApplicant);
		String zip = location != null ? location.getZip() : null;
		List<Integer> households = null;
		
		final String useExternalId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_HOUSEHOLD_CASE_ID_ENABLE);
		if(null != useExternalId && "TRUE".equalsIgnoreCase(useExternalId)) {
			households =   cmrHouseholdRepository.findMatchingCmrByHHCaseId(householdCaseID);
		}else {
			if(StringUtils.isNotBlank(ssapApplicant.getSsn())  ) { 
				if(!hasLinkUserEnabled){
					households = cmrHouseholdRepository.findMatchingCmrBySSN(ssapApplicant.getSsn());	
				}
			}

		}
		if (ReferralUtil.listSize(households) == ReferralConstants.NONE) {
			household = createNewHousehold(ssapApplicant, location,householdCaseID);
			householdId = household.getId();
		} else if (ReferralUtil.listSize(households) == ReferralConstants.ONE) {
			householdId = households.get(0);
		} else {
			throw new GIRuntimeException("Error while fetching household. More than one Household Exists for " + ssapApplicationId);
		}

		return householdId;
	}
	
	@Override
	public Integer findHouseholdFromSsnDoB(long ssapApplicationId) {
		final SsapApplicant ssapApplicant = ssapApplicantRepository.findByPersonId(ssapApplicationId, PRIMARY_PERSON);
		int householdId = 0;

		if (ssapApplicant == null) {
			throw new GIRuntimeException("Errow while loading primary for " + ssapApplicationId);
		}

		// checkfor exisiting household
		Location location = populateLocation(ssapApplicant);
		String zip = location != null ? location.getZip() : null;
		List<Integer> households = null;
		if(StringUtils.isNotBlank(ssapApplicant.getSsn()) && ssapApplicant.getBirthDate() !=null  ) { 
		   households = cmrHouseholdRepository.findMatchingCmrBySsnDoB( ssapApplicant.getBirthDate(),ssapApplicant.getSsn());
		}
		else {
		   households = cmrHouseholdRepository.findByFirstNameLastNameAndBirthDate(ReferralUtil.capitalizeFirstLetter(ssapApplicant.getFirstName()), ReferralUtil.capitalizeFirstLetter(ssapApplicant.getLastName()), ssapApplicant.getBirthDate(), zip);
		}
		 if (ReferralUtil.listSize(households) == ReferralConstants.ONE) {
			householdId = households.get(0);
		} else {
			throw new GIRuntimeException("Error while fetching household from SSN DoB for ApplicationId " + ssapApplicationId + " and applicant Id "+ssapApplicant.getId());
		}

		return householdId;
	}
	
 
	
	@Autowired 
	private LookupService lookUpService;

	private static final String COMMUNICATION_MODE = "PreferredContactMode";
	
	private Household createNewHousehold(final SsapApplicant ssapApplicant, Location location, String householdCaseID) {
		EligLead eligLead = new EligLead();
		eligLead.setPhoneNumber(ssapApplicant.getPhoneNumber());
		eligLead.setStage(EligLead.STAGE.WELCOME);
		eligLead = phixService.saveEligLeadRecord(eligLead);

		Household household = new Household();
		household.setNameSuffix(ssapApplicant.getNameSuffix());
		household.setFirstName(ssapApplicant.getFirstName());
		household.setLastName(ssapApplicant.getLastName());
		household.setBirthDate(ssapApplicant.getBirthDate());
		household.setPhoneNumber(ssapApplicant.getPhoneNumber());
		household.setEmail(ssapApplicant.getEmailAddress());
		household.setRidpVerified(ReferralConstants.Y);
		household.setRidpDate(new TSDate());
		household.setRidpSource(REFERRAL);
		household.setEligLead(eligLead);
		if(ssapApplicant.getSsn()!=null) {
			household.setSsn(ssapApplicant.getSsn());
		}
		
		Integer prefCommMethodLookup = readDefaultPrefCommMethod(household);
		household.setPrefContactMethod(prefCommMethodLookup);
		household.setHouseholdCaseId(householdCaseID);
		
		household = cmrHouseholdRepository.save(household);
		household.setLocation(location);
		household.setPrefContactLocation(populateMailingLocation(ssapApplicant));
		household.setZipCode(location.getZip());
		household = cmrHouseholdRepository.save(household);
		return household;
	}

	private Integer readDefaultPrefCommMethod(final Household household) {
		LookupValue prefCommMethodLookup = null;
		GhixNoticeCommunicationMethod prefCommunication = null;
		if (StringUtils.isNotBlank(household.getEmail())){
			prefCommunication = GhixNoticeCommunicationMethod.Email;
		} else {
			prefCommunication = GhixNoticeCommunicationMethod.Mail;
		}
		prefCommMethodLookup = lookUpService.getlookupValueByTypeANDLookupValueCode(COMMUNICATION_MODE, prefCommunication.getMethod());
		
		return prefCommMethodLookup != null ? prefCommMethodLookup.getLookupValueId() : null;
	}

	
	private Location populateMailingLocation(SsapApplicant ssapApplicant) {
		// Other Location is the Primary Location
		if (ssapApplicant.getMailiingLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getMailiingLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
		} else if (ssapApplicant.getOtherLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getOtherLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
		} else {
			return null;
		}
	}
	
	private Location populateLocation(SsapApplicant ssapApplicant) {
		// Other Location is the Primary Location
		if (ssapApplicant.getOtherLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getOtherLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
		} else if (ssapApplicant.getMailiingLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getMailiingLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
		} else {
			return null;
		}
	}

	@Override
	public ReferralLinkingOverride compareHouseholdAndPrimary(long ssapApplicationId, int householdId) {

		List<SsapApplication> ssapApplication = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ReferralUtil.listSize(ssapApplication) == 0) {
			throw new GIRuntimeException("Invalid ssap application id" + ssapApplicationId);
		}

		SsapApplicant ssapApplicant = getPrimaryApplicant(ssapApplication.get(0));
		Household household = cmrHouseholdRepository.findOne(householdId);
		//boolean householdAndPrimarySame = false;

		if (null == ssapApplicant || null == household) {
			throw new GIRuntimeException("Error while fetching household/applicant details for" + ssapApplicationId);
		}

		String locationZip = StringUtils.EMPTY;
		if (ssapApplicant.getOtherLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getOtherLocationId().intValue())) {
			Location location = iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
			locationZip = location.getZip();
		}
		
		PrimaryApplicant referralApplicant = new PrimaryApplicant(ssapApplicant.getFirstName(),ssapApplicant.getLastName(),ssapApplicant.getBirthDate(),ssapApplicant.getSsn(),locationZip);
		PrimaryApplicant householdApplicant = new PrimaryApplicant(household.getFirstName(),household.getLastName(),household.getBirthDate(),household.getSsn(),household.getZipCode());
		
		ReferralLinkingOverride referralLinkingOverride = new ReferralLinkingOverride(referralApplicant,householdApplicant);

		/*if (household.getBirthDate() != null && ssapApplicant.getBirthDate() != null) {
			DateTime householdDOB = new DateTime(household.getBirthDate()).withTimeAtStartOfDay();
			DateTime primaryDOB = new DateTime(ssapApplicant.getBirthDate()).withTimeAtStartOfDay();
			
			if (StringUtils.equalsIgnoreCase(ReferralUtil.capitalizeFirstLetter(household.getFirstName()), ReferralUtil.capitalizeFirstLetter(ssapApplicant.getFirstName())) && StringUtils.equalsIgnoreCase(ReferralUtil.capitalizeFirstLetter(household.getLastName()), ReferralUtil.capitalizeFirstLetter(ssapApplicant.getLastName())) && householdDOB.compareTo(primaryDOB) == 0
			        && StringUtils.equalsIgnoreCase(household.getZipCode(), locationZip)) {
				householdAndPrimarySame = true;
			}
		}*/
		return referralLinkingOverride;
	}

	private SsapApplicant getPrimaryApplicant(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant;
			}
		}

		return null;
	}

	@Override
	public Integer updateReferralCount(String referralId) {
		int count = -1;
		int referralActivationId = -1;

		ReferralActivation referralActivation;

		try {
			if (StringUtils.isNotEmpty(referralId)) {
				referralActivationId = Integer.valueOf(referralId);
				referralActivation = referralActivationRepository.findOne(new Long(referralActivationId));

				if (null != referralActivation) {
					count = referralActivation.getSecurityRetryCount();
					
					referralActivation.setSecurityRetryCount(count);
					if (count >= 4) {
						Calendar cal = TSCalendar.getInstance();
						cal.add(Calendar.MINUTE, 15);
						Date lockTime = cal.getTime();
						referralActivation.setLockTime(lockTime);
					}
					referralActivation.setLastUpdatedOn(new TSDate());
					referralActivationRepository.save(referralActivation);
				}
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE UPDATING RFRRL CNT: ".intern(), ex);
		}

		return count;
	}
	
	@Override
	public Integer updateReferralSuccessCount(String referralId) {
		int count = -1;
		int referralActivationId = -1;

		ReferralActivation referralActivation;

		try {
			if (StringUtils.isNotEmpty(referralId)) {
				referralActivationId = Integer.valueOf(referralId);
				referralActivation = referralActivationRepository.findOne(new Long(referralActivationId));
				
				if (null != referralActivation) {
					referralActivation.setSecurityRetryCount(1);
					referralActivation.setLockTime(null);
					referralActivation.setLastUpdatedOn(new TSDate());
					referralActivationRepository.save(referralActivation);
					count = 0;
				}
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE UPDATING RFRRL CNT: ".intern(), ex);
		}

		return count;
	}
	
	

	@Override
	public Map<String,BigDecimal> getMonthWiseAPTC(AptcHistoryRequest aptcHistoryRequest) {
        Map<String,BigDecimal> monthWiseAptc =  null;
		if(aptcHistoryRequest!=null) {
			if(!CollectionUtils.isEmpty(aptcHistoryRequest.getMonths())) {
				List<AptcHistory> aptcHistoryList = aptcHistoryRepository.findAptcHistoryByCmrHouseholdIdCovYear(aptcHistoryRequest.getCmrHouseholdId(),aptcHistoryRequest.getCoverageYear());
				StringBuffer monthsWithValue = new StringBuffer();
				if (aptcHistoryList != null) {
					monthWiseAptc = new HashMap<String,BigDecimal>();
					List<Integer> iMonth = getMonthNumber(aptcHistoryRequest.getMonths());
					for (AptcHistory aptcHistory : aptcHistoryList) {
						for(Integer oneMonth : iMonth) {
						    String sMonth = getStringMonth(oneMonth);
							if(aptcHistory.getEligStartDate().getMonth()<=oneMonth.intValue() && aptcHistory.getEligEndDate().getMonth()>=oneMonth.intValue()
									&& monthsWithValue.indexOf(sMonth)<0) {
								monthWiseAptc.put(sMonth, aptcHistory.getAptcAmount());
								monthsWithValue.append(sMonth);
							}
						}
						if(monthWiseAptc.size()==aptcHistoryRequest.getMonths().size()) {
							break;
						}
					}
				}
			}
		}
		return monthWiseAptc;
	}
	
	private List<Integer> getMonthNumber(List<String> months) {
		List<Integer> month = new ArrayList<Integer>();
		for(String oneMonth : months) {
			month.add(getIntMonth(oneMonth));
		}    
		return month;
	}
		
	private Integer getIntMonth(String month) {
		Integer iMonth = null;
		switch (month)
	    {
	    case "JAN":
	    	iMonth = 0;
	        break;
	    case "FEB":
	    	iMonth = 1;
	        break;
	    case "MAR":
	    	iMonth = 2;
	        break;
	    case "APR":
	    	iMonth = 3;
	        break;
	    case "MAY":
	    	iMonth = 4;
	        break;
	    case "JUN":
	    	iMonth = 5;
	        break;
	    case "JUL":
	    	iMonth = 6;
	        break;
	    case "AUG":
	    	iMonth = 7;
	        break;
	    case "SEP":
	    	iMonth = 8;
	        break;
	    case "OCT":
	    	iMonth = 9;
	        break;
	    case "NOV":
	    	iMonth = 10;
	        break;
	    case "DEC":
	    	iMonth = 11;
	        break;
	    }
		return iMonth;
	}
	
	private String getStringMonth(int month) {
		String sMonth = null;
		switch (month)
	    {
	    case 0:
	    	sMonth = "JAN";
	        break;
	    case 1:
	    	sMonth = "FEB";
	        break;
	    case 2:
	    	sMonth = "MAR";
	        break;
	    case 3:
	    	sMonth = "APR";
	        break;
	    case 4:
	    	sMonth = "MAY";
	        break;
	    case 5:
	    	sMonth = "JUN";
	        break;
	    case 6:
	    	sMonth = "JUL";
	        break;
	    case 7:
	    	sMonth = "AUG";
	        break;
	    case 8:
	    	sMonth = "SEP";
	        break;
	    case 9:
	    	sMonth = "OCT";
	        break;
	    case 10:
	    	sMonth = "NOV";
	        break;
	    case 11:
	    	sMonth = "DEC";
	        break;
	    }
		return sMonth;
	}
	
	/**
	 * This method is used to set the external household case id in the referral response for further processing
	 * For MN it would be passed in IntegratedCaseID and for CA it would be passed in HouseholdCaseId 
	 * @param referralResponse
	 * @param accountTransferRequest
	 */
	public void setExternalHouseholdCaseIdInReferralResponse(ReferralResponse referralResponse,AccountTransferRequestDTO accountTransferRequest) {
		// config for MN
		final String hasLinkUserStr =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_LINK_USER);
		// config for CA
		final String useExternalId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_HOUSEHOLD_CASE_ID_ENABLE);
		//config for nv
		final String linkATUsingAppId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_LINK_AT_USING_APPLICATION_ID);
		
		String textToCompare = ReferralConstants.HOUSE_HOLD_CASE_ID;
		if(null != hasLinkUserStr && "TRUE".equalsIgnoreCase(hasLinkUserStr)){
			// set the text to compare to INTEGRATED_CASE_ID
			textToCompare = ReferralConstants.INTEGRATED_CASE_ID;
		}
		
		if((null != useExternalId && "TRUE".equalsIgnoreCase(useExternalId)) || (null != hasLinkUserStr && "TRUE".equalsIgnoreCase(hasLinkUserStr)) ) {
			if(accountTransferRequest.getAccountTransferRequestPayloadType() != null){
			List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes=accountTransferRequest.getAccountTransferRequestPayloadType().getInsuranceApplication().getApplicationIdentification();
			String hhCaseId = null ;
			for(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType : iTypes) {
				if( null != linkATUsingAppId && "TRUE".equalsIgnoreCase(linkATUsingAppId) && (tIdentificationType.getIdentificationCategoryText( ) == null) ){
					hhCaseId = tIdentificationType.getIdentificationID().getValue();
					break;
				}
				if(textToCompare.equalsIgnoreCase( tIdentificationType.getIdentificationCategoryText( ).getValue())) {
					hhCaseId = tIdentificationType.getIdentificationID().getValue();
					break;
				}
			}
			referralResponse.getData().put(ReferralConstants.HOUSE_HOLD_CASE_ID, hhCaseId);
		}
	}
		//state is NV and AccountTransferRequestPayloadType is null then set the hhcaseid from cmr household external case id
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if(accountTransferRequest.getAccountTransferRequestPayloadType() == null &&  stateCode.equals("NV")){
			String hhCaseId = null ;
			//int cmrId = accountTransferRequest.getApplicationsWithSameId().get(0).getCmrId();
			//Household  household = cmrHouseholdRepository.findOne(cmrId);
			//hhCaseId = household.getHouseholdCaseId();
			//referralResponse.getData().put(ReferralConstants.HOUSE_HOLD_CASE_ID, hhCaseId);
		}
	}
	
	@Override
	public long executeLceATDto(AccountTransferRequestDTO accountTransferRequest) throws Exception {
		long ssapApplicationId = 0;
		try {
			LOGGER.info("Creating a new Ssap Referral Application Starts".intern());
			ssapApplicationId = createATDto(accountTransferRequest);
			LOGGER.info("Creating a new Ssap Referral Application Ends".intern());
		} catch (Exception e) {
			throw e;
		}
		return ssapApplicationId;
	}
	
	@Override
	public Map<String,SsapApplicantPersonType> determineApplicantPersonType(PersonTypeRequest personTypeRequest) {
		Map<String,SsapApplicantPersonType> personTypeMap = new HashMap<String, SsapApplicantPersonType>();
		List<PersonTypeDTO> list = personTypeRequest.getList();
		if(CollectionUtils.isNotEmpty(list))
		{
			for(PersonTypeDTO personTypeDTO : list)
			{
				if(StringUtils.isNotBlank(personTypeDTO.getPersonId())&& StringUtils.isNotBlank(personTypeRequest.getPrimaryApplicantId()) 
						&& StringUtils.isNotBlank(personTypeRequest.getPrimaryTaxFilerId()))
				{
					SsapApplicantPersonType personType = ssapAssembler.determineApplicantPersonType(personTypeRequest.getPrimaryApplicantId(), personTypeRequest.getPrimaryTaxFilerId(), personTypeDTO.getPersonId(), personTypeDTO.isApplyingForCoverage());
					personTypeMap.put(personTypeDTO.getPersonId(), personType);
				}
				else
				{
					throw new GIRuntimeException("Input parameters can not be null/blank.");
				}
			}
		}
		else
		{
			throw new GIRuntimeException("Person id list can not be null/blank.");
		}
		return personTypeMap;
	}
}
