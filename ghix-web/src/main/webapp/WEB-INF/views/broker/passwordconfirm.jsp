<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script src="../resources/js/jquery.validate.min.js" type="text/javascript"></script>

<div class="gutter10">
	<div class="row-fluid">
		<c:choose>
			<c:when test="${not empty errorMsg}">
				<div class="span6 offset2">
					<div class="row-fluid errorblock alert alert-info txt-center">
						${errorMsg}.
					</div>
					<!-- .alert -->
					<div class="row-fluid txt-center">
						<input type="button" value="Back" class="btn btn-primary" onclick="javascript:history.go(-1)">
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<div class="span6 offset2">
					<div class="row-fluid">
						<h1>Congratulations&#33;</h1>
					</div>
					<div class="alert alert-info">Your password has been changed.
					</div>
					<!-- .alert -->
					<div class="row-fluid txt-center">
						<a href="../account/user/login" class="btn btn-primary">Login</a>
					</div>
				</div>
				<!-- end of span9 -->
			</c:otherwise>
		</c:choose>
	</div><!--  end of .row-fluid -->
</div>
<script>
/* $(document).ready(function() {
	$('.row-fluid').removeClass('titlebar');
}); */

</script>