import moment from 'moment';
import phoneNumberUtils from '../utils/phoneNumberUtils';

const checkRegex = (
  value,
  regex,
  affordableValues = undefined,
  reverse = undefined
) => {
  if (affordableValues && affordableValues.includes(value)) {
    return true;
  }
  if (value === null) {
    return false;
  }
  return reverse
    ? !(value && value.match(regex))
    : !!(value && value.match(regex));
};

const methods = {
  phoneNumber: ([value]) => {
    if (value === null || value === '') {
      return true;
    }
    const numbersQty =
      typeof value === 'string' &&
      value
        .split('')
        .filter(c => c.match(/^[0-9]*$/))
        .join('').length;
    return numbersQty === 10;
  }
};

export const validatePhoneNumber = phoneNumber => {
  if (!phoneNumber) {
    return 'Phone number required';
  } else if (phoneNumberUtils.cleanedUpPhone(phoneNumber).length < 10) {
    return 'Phone number not valid';
  }
};

export const validateTimeSelections = (fromTime, toTime, closed) => {
  let from = moment(fromTime, 'HH:mm');
  let to = moment(toTime, 'HH:mm');
  if (!closed && (fromTime === 'select' || toTime === 'select')) {
    return 'Please select a time or mark closed';
  } else if (from.isAfter(to)) {
    return 'Start time must be before end time';
  } else if (to.isBefore(from)) {
    return 'Start time must be before end time';
  } else if (to.isSame(from)) {
    return 'Times cannot be the same';
  }
};

export const validateAvailabilitySubmit = values => {
  let errors = {};
  let error;
  error = validateTimeSelections(
    values.MondayStart,
    values.MondayEnd,
    values.closed_Monday
  );
  if (error) {
    errors.MondayStart = error;
    errors.MondayEnd = error;
  }
  error = validateTimeSelections(
    values.TuesdayStart,
    values.TuesdayEnd,
    values.closed_Tuesday
  );
  if (error) {
    errors.TuesdayStart = error;
    errors.TuesdayEnd = error;
  }
  error = validateTimeSelections(
    values.WednesdayStart,
    values.WednesdayEnd,
    values.closed_Wednesday
  );
  if (error) {
    errors.WednesdayStart = error;
    errors.WednesdayEnd = error;
  }
  error = validateTimeSelections(
    values.ThursdayStart,
    values.ThursdayEnd,
    values.closed_Thursday
  );
  if (error) {
    errors.ThursdayStart = error;
    errors.ThursdayEnd = error;
  }
  error = validateTimeSelections(
    values.FridayStart,
    values.FridayEnd,
    values.closed_Friday
  );
  if (error) {
    errors.FridayStart = error;
    errors.FridayEnd = error;
  }
  error = validateTimeSelections(
    values.SaturdayStart,
    values.SaturdayEnd,
    values.closed_Saturday
  );
  if (error) {
    errors.SaturdayStart = error;
    errors.SaturdayEnd = error;
  }
  error = validateTimeSelections(
    values.SundayStart,
    values.SundayEnd,
    values.closed_Sunday
  );
  if (error) {
    errors.SundayStart = error;
    errors.SundayEnd = error;
  }
  return errors;
};
