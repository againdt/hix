(function (collections, pagination, model) {
	collections.dentalPlans = Backbone.Collection.extend({
		model : model,
		url : 'getDentalPlans?'+(Math.random()*10),
		
		/**
		 * @param resp the response returned by the server
		 * @returns (Array) dental plans
		 */
		parse : function (resp) {
			var dentalPlans = resp;
			return dentalPlans;
		}
	});
	
	_.extend(collections.dentalPlans.prototype, pagination);
})(App.collections, App.backbone.Pagination, App.models.dentalPlan);