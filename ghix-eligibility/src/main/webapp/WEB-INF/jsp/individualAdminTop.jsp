<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@include file="datasource.jsp" %>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
 <link rel="stylesheet" href="/resources/demos/style.css" />
<head>
<title>Eligibility Result Processor</title>

<style>
table#table1 {
	border: 1px solid #666;
	width: 80%;
	margin: 20px 0 20px 0 !important;
	font-size: 14px;
}

th,td {
	padding: 2px 4px 2px 4px !important;
	text-align: left;
	vertical-align: top;	
}

thead tr {
	background-color: #fc0;
}

th.sorted {
	background-color: orange;
}

th a,th a:visited {
	color: black;
}

th a:hover {
	text-decoration: underline;
	color: black;
}

th.sorted a,th.sortable a {
	background-position: right;
	display: block;
	width: 100%;
}

th.sortable a {
	background-image: url(../img/arrow_off.png);
}

th.order1 a {
	background-image: url(../img/arrow_down.png);
}

th.order2 a {
	background-image: url(../img/arrow_up.png);
}

tr.odd {
	background-color: #fff
}

tr.tableRowEven,tr.even {
	background-color: #fea
}

div.exportlinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	margin: 2px 0 10px 0;
	width: 79%;
}

span.export {
	padding: 0 4px 1px 20px;
	display: inline;
	display: inline-block;
	cursor: pointer;
}

span.excel {
	background-image: url(../img/ico_file_excel.png);
}

span.csv {
	background-image: url(../img/ico_file_csv.png);
}

span.xml {
	background-image: url(../img/ico_file_xml.png);
}

span.pdf {
	background-image: url(../img/ico_file_pdf.png);
}

span.rtf {
	background-image: url(../img/ico_file_rtf.png);
}

span.pagebanner {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	margin-top: 10px;
	display: block;
	border-bottom: none;
}

span.pagelinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	display: block;
	border-top: none;
	margin-bottom: -5px;
}


.group-1 {
    font-weight:bold;
    padding-bottom:10px;
    border-top:1px solid black;
}
.group-2 {
    font-style:italic;
    border-top: 1px solid black;

}
.subtotal-sum, .grandtotal-sum {
    font-weight:bold;
    text-align:right;
}
.subtotal-header {
    padding-bottom: 0px;
    border-top: 1px solid white;
}
.subtotal-label, .grandtotal-label {
    border-top: 1px solid white;
    font-weight: bold;
}
.grouped-table tr.even {
    background-color: #fff;
}
.grouped-table tr.odd {
    background-color: #fff;
}
.grandtotal-row {
    border-top: 2px solid black;
}

</style>
    

<script type="text/javascript">


$(function() {
	$( "#date1" ).datepicker().attr( 'readOnly' , 'true' );
	
	});

$(function() {
	$( "#date2" ).datepicker().attr( 'readOnly' , 'true' );;
	});

function validateDates(){

	var dateStart = document.getElementById('date1').value.split('/');
	
	if(dateStart != "" && dateStart.length > 0)
	{
		
	var boolean1 = validateDate(dateStart);
	}

	var dateEnd = document.getElementById('date2').value.split('/');
	
	if(dateEnd !=  "" && dateEnd.length > 0)
        {
		var boolean2 = validateDate(dateEnd);
	}
}

function validateDate(dateGiven){
	
	var today=new Date();

	var dob_mm = dateGiven[0];  
	var dob_dd = dateGiven[1];  
	var dob_yy = dateGiven[2]; 
	
	var birthDate=new Date();
	birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);

	if( (today.getFullYear() - 2) >  birthDate.getFullYear() ) {
	//alert('Please enter a valid date in MM/DD/YYYY format');
	return false; 
	}

	if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) {
//	alert('Please enter a valid date in MM/DD/YYYY format');
	return false; 
	}

	if(today.getTime() < birthDate.getTime()){
//	alert('Please enter a valid date in MM/DD/YYYY format');
	return false; 
	}
	
}

function setWhereClause(type){
	
	var whereClause = '';
	var filterStatement = '';
	var topTenSelectClause='';
	var topTenWhereClause='';
	
	switch (type) {
		case 'B1' : 
			whereClause = "TO_NUMBER(TO_CHAR(created_timestamp, 'YYYYMMDDhh24mi')) between TO_NUMBER(TO_CHAR(sysdate - 1, 'YYYYMMDDhh24mi')) and TO_NUMBER(TO_CHAR(sysdate, 'YYYYMMDDhh24mi'))";
			filterStatement = ' in last 24 hours';
			break;
		case 'B2' : 
			whereClause = "TO_NUMBER(TO_CHAR(created_timestamp, 'YYYYMMDDhh24mi')) between TO_NUMBER(TO_CHAR(sysdate - 1/2, 'YYYYMMDDhh24mi')) and TO_NUMBER(TO_CHAR(sysdate, 'YYYYMMDDhh24mi'))";
			filterStatement = ' in last 12 hours';
			break;
		case 'B3' : 
			whereClause = "TO_NUMBER(TO_CHAR(created_timestamp, 'YYYYMMDDhh24mi')) between TO_NUMBER(TO_CHAR(sysdate - 1/24, 'YYYYMMDDhh24mi')) and TO_NUMBER(TO_CHAR(sysdate, 'YYYYMMDDhh24mi'))";
			filterStatement = ' in last 1 hour';
			break;
		case 'B4' : 
		
			
			//Validate the dates
		        validateDates();
		         
		         //Step 1: Get List Box value, if it is not empty
			var fieldName = document.getElementById('field_name').value;
			var fieldValue = document.getElementById('field_value').value;
			if(fieldValue != null && fieldValue.length > 0)
			{
			whereClause = " lower("+fieldName + ") like lower('%" + fieldValue + "%')";
			filterStatement = ' with ' + fieldName + " = " + fieldValue;
			}
			
			//Step 2: Get the Status Value
			var statuses = document.getElementById('status');
			var status = statuses.options[statuses.selectedIndex].value;
			if(status != '')
			{
				if(whereClause){
					whereClause = whereClause  + " and lower(status) = lower('" + status + "')";
				}else{
					whereClause = whereClause  + " lower(status) = lower('" + status + "')";
				}
			
				
			}
			if(status == 'S'){
				
				filterStatement = filterStatement+ ' which were successful';
			}
			if(status == 'F'){
			
				filterStatement = filterStatement+ ' which have failed';
			}
			
			//Step 3: Get the Dates
			var date1 = document.getElementById('date1').value;
			var date2 = document.getElementById('date2').value;
			
			if(date1 && date2)
			{	
				
				if( whereClause){
					whereClause = whereClause+" and created_timestamp between to_timestamp('"+date1+" 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"+date2+" 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					filterStatement = filterStatement + ' start date ' +  date1 + 'end date ' +  date2;
					
				}else{
					whereClause = whereClause+" created_timestamp between to_timestamp('"+date1+" 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"+date2+" 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					filterStatement = filterStatement + ' start date ' +  date1 + 'end date ' +  date2;
					
				}
			
			}else{
				if(date1&& !date2  ){
					if( whereClause){
					
					whereClause = whereClause+" and to_char(created_timestamp  ,'mm/dd/yyyy' )='"+date1+"'" ;
					filterStatement = filterStatement + ' start date ' +  date1;
					}else{
						whereClause = whereClause+" to_char(created_timestamp  ,'mm/dd/yyyy' )='"+date1+"'" ;
						filterStatement = filterStatement + ' start date ' +  date1;
					}
				}else{
					
					 if(!date1 && date2 ){
						 if( whereClause){
								whereClause = whereClause+" and to_char(created_timestamp  ,'mm/dd/yyyy' )='"+date2+"'" ;
								filterStatement = filterStatement + ' end date ' +  date2;
						 }else{
								whereClause = whereClause+" to_char(created_timestamp  ,'mm/dd/yyyy' )='"+date2+"'" ;
								filterStatement = filterStatement + ' end date ' +  date2;
							 }
				
						}
					}
				
				}if(!whereClause){
					whereClause = "created_timestamp <= sysdate";
				}
			
			break;
		
		case 'B5' : 
			topTenSelectClause=' Select * from ( ';
			topTenWhereClause=' ) where rownum <=10 order by  rownum';
			filterStatement = 'for latest records';
			break;
			
	}
	
	document.getElementById('whereClause').value = whereClause;
	document.getElementById('filterStatement').value = filterStatement;
	document.getElementById('topTenSelectClause').value = topTenSelectClause;
	document.getElementById('topTenWhereClause').value = topTenWhereClause;
	//alert('whereClause==>'+whereClause);
	//alert('topTenSelectClause==>'+topTenSelectClause);
	//alert('topTenWhereClause==>'+topTenWhereClause); 
}
</script>    
    
</head>

<form method="POST" action="getTopPage">

	<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#C0C0C0">
				<input type="submit" value="24 Hours" name="B1" title="View requests made in last 24 hours" onclick="setWhereClause('B1');">
				<input type="submit" value="12 Hours" name="B2" title="View requests made in last 12 hours" onclick="setWhereClause('B2');">
				<input type="submit" value="1 Hour" name="B3" title="View requests made in last 1 hour" onclick="setWhereClause('B3');">
				<input type="submit" value="Show Latest" name="B5" title="View top 10 requests" onclick="setWhereClause('B5');">
			</td>
			<td bgcolor="#66FFCC">
				<p>
					&nbsp;Field:
					<select size="1" id="field_name" name="field_name">
						<option value="ssap_application_id" selected="">Application ID</option>
						<option value="correlation_id">Correlation ID</option>
						<option value="gi_ws_payload_id" >GI WS Payload ID</option>
						<option value="response_code" >Response Code</option>						
					</select>
					<input type="text" id="field_value" name="field_value" size="10" maxlength="100">
					Status:
					<select size="1" id="status" name="status">
					<option value="" selected="">All</option>
					<option value="Success" >Success</option>
					<option value="Failure">Failure</option>
				</select>
				Date Start:
				<input type="text" id="date1" name="date1" size="10" maxlength="10"> End:
				<input type="text" id="date2" name="date2" size="10" maxlength="10">
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
				</p>
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">
	<input id="topTenSelectClause" name="topTenSelectClause" type="hidden">
	<input id="topTenWhereClause" name="topTenWhereClause" type="hidden">
</form>

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = "created_timestamp >= sysdate-2";
		
	}
	String topTenSelectClause = request.getParameter("topTenSelectClause");
	if( topTenSelectClause == null || topTenSelectClause == ""){
		topTenSelectClause="";
		
	}
	
	String topTenWhereClause = request.getParameter("topTenWhereClause");
	if( topTenWhereClause == null || topTenWhereClause == ""){
		topTenWhereClause="";
		
	}
%>

<sql:query dataSource="${jspDataSource}" var="result1">
<%=topTenSelectClause %>select GI_WS_PAYLOAD_ID,
'<a href="getBottomRight?request='||GI_WS_PAYLOAD_ID||'" target="main_1">Request</a>' as REQUEST_PAYLOAD1, 
'<a href="getBottomRight?response='||GI_WS_PAYLOAD_ID||'" target="main_1">Response</a>' as RESPONSE_PAYLOAD1, 
RESPONSE_CODE,STATUS,CORRELATION_ID,
to_char(CREATED_TIMESTAMP,'DD-MON-YY HH24:MM:SS') as created_date,
'<a href="getBottomRight?exception='||GI_WS_PAYLOAD_ID||'" target="main_1">Exception</a>' as EXCEPTION_MESSAGE1, 
RETRY_COUNT,SSAP_APPLICATION_ID ,
'<a href="factsBottomRight?request='||CORRELATION_ID||'" target="main_1">Facts / Actions</a>' as FACTS,
endpoint_function
from gi_ws_payload
where endpoint_function in ('ERP-TRANSFERACCOUNT','ACCOUNT-TRANSFER-OUTBOUND') and  
<%=whereClause%>
order by GI_WS_PAYLOAD_ID desc <%=topTenWhereClause %>
</sql:query>

Requests Received by Eligibility Result Processor 
	<% 
		if(request.getParameter("filterStatement") != null){
			out.println(request.getParameter("filterStatement"));
		}
	%>
<br>


<display:table name="${result1.rows}" requestURI="/admin/getTopPage" id="table1" style="white-space: pre-wrap;" export="false" pagesize="10">
	<display:column property="GI_WS_PAYLOAD_ID" title="GI WS Payload ID" sortable="false" />
	<display:column property="REQUEST_PAYLOAD1" title="Request" sortable="false"  />
	<display:column property="RESPONSE_PAYLOAD1" title="Response" sortable="false"  />
	<display:column property="RESPONSE_CODE" title="Response Code" sortable="false"  />
	<display:column property="STATUS" title="Status" sortable="false" />
	<display:column property="CORRELATION_ID" title="Correlation ID" sortable="false" />
	<display:column property="endpoint_function" title="Inbound/Outbound" sortable="false" />
	<display:column property="EXCEPTION_MESSAGE1" title="Exception" sortable="false" />
	<display:column property="RETRY_COUNT" title="Retry Count" sortable="false" />
	<display:column property="SSAP_APPLICATION_ID" title="Application ID" sortable="false" />
	
	<display:column property="FACTS" title="Facts / Actions" sortable="false" />
	
	<display:column property="created_date" title="Created Date" sortable="false" />
</display:table>

