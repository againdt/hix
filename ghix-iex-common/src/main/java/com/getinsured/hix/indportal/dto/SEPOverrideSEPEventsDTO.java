package com.getinsured.hix.indportal.dto;

/**
 * @author vishwanath_s
 * Class to store SEP Override SEP Events DTO
 */
public class SEPOverrideSEPEventsDTO {

	private String isFinancial;
	private String displayOnUI;
	private String changeType;
	
	/**
	 * @return the isFinancial
	 */
	public String getIsFinancial() {
		return isFinancial;
	}
	/**
	 * @param isFinancial the isFinancial to set
	 */
	public void setIsFinancial(String isFinancial) {
		this.isFinancial = isFinancial;
	}
	/**
	 * @return the displayOnUI
	 */
	public String getDisplayOnUI() {
		return displayOnUI;
	}
	/**
	 * @param displayOnUI the displayOnUI to set
	 */
	public void setDisplayOnUI(String displayOnUI) {
		this.displayOnUI = displayOnUI;
	}
	/**
	 * @return the changeType
	 */
	public String getChangeType() {
		return changeType;
	}
	/**
	 * @param changeType the changeType to set
	 */
	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}
	
}
