package com.getinsured.hix.batch.enrollment.email;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.getinsured.hix.dto.enrollment.EnrollmentBatchEmailDTO;
import com.getinsured.hix.platform.notification.NotificationAgent;

/**
 * Helper class to send Enrollment Batch failure email notification flow.
 *
 * @author Raja
 *
 */
@Component
@Scope("prototype")
public class EnrollmentBatchFailureEmailNotification extends NotificationAgent {
	
	private EnrollmentBatchEmailDTO enrollmentBatchEmailDTO;
	private Map<String, String> singleData;
	
	@Override
	public Map<String, String> getSingleData() {
		Map<String,String> bean = new HashMap<String, String>();
		
		bean.put("jobId", enrollmentBatchEmailDTO.getJobId());
		bean.put("jobName", enrollmentBatchEmailDTO.getJobName());
		bean.put("stepId",enrollmentBatchEmailDTO.getStepId());
		bean.put("stepName",enrollmentBatchEmailDTO.getStepName());
		bean.put("failureDate",enrollmentBatchEmailDTO.getFailureDate().toString());
		bean.put("errorCode",enrollmentBatchEmailDTO.getErrorCode().toString());
		bean.put("errorMessage",enrollmentBatchEmailDTO.getErrorMessage());
		bean.put("fileLocation",enrollmentBatchEmailDTO.getFileLocation());
		bean.put("appServer",enrollmentBatchEmailDTO.getAppServer());
		setTokens(bean);

		Map<String, String> data = new HashMap<String, String>();
		data.put("To",enrollmentBatchEmailDTO.getTo());
		data.put("From",enrollmentBatchEmailDTO.getFrom());
		data.put("Subject", enrollmentBatchEmailDTO.getSubject());

		if (singleData != null)
		{
			data.putAll(singleData);
		}
		return data;
	}

	public void updateSingleData(Map<String, String> singleData)
	{
		this.singleData = singleData;
	}

	public EnrollmentBatchEmailDTO getEnrollmentBatchEmailDTO() {
		return enrollmentBatchEmailDTO;
	}

	public void setEnrollmentBatchEmailDTO(
			EnrollmentBatchEmailDTO enrollmentBatchEmailDTO) {
		this.enrollmentBatchEmailDTO = enrollmentBatchEmailDTO;
	}

}