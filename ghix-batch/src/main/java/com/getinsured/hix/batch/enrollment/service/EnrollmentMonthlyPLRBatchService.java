package com.getinsured.hix.batch.enrollment.service;

import java.util.List;

import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.util.exception.GIException;


public interface EnrollmentMonthlyPLRBatchService {
	
	List<String> getUniqueHouseHolds(int month, int year) throws GIException;
	
	void processPLRHousehold(List<String> householdCaseIdList, int applicableMonth, int applicableYear, int partition) throws GIException;
	
	void generateFilePayloadBatchTransmissionXML(int month, int year, boolean isGeneratedXmlValid) throws GIException;
	
	/**
	 * 
	 * @param jobName
	 * @return
	 */
	List<BatchJobExecution> getRunningBatchList(String jobName);
	
	/**
	 * 
	 * @param jobId
	 * @return
	 * @throws GIException
	 */
	boolean validateGeneratedMonthlyPLRXML(Long jobId);
}
