// @flow

type PromiseTypes = {
    errorMessage: string | null,
    errorType: string | null
};


/**
 * @async
 * @param {Object} response - data we retrieve from server
 * @param {Function} dispatch
 * @param {Function} setError
 * @returns {Promise<*>}:
 *  {
 *      errorMessage {null, string },   - message we would like to show on UI for particular error
 *      errorType {null, string},       - error type
 *  }
 */
const checkResponse = async (response : Object, dispatch: Function, setError: Function) : Promise<any> => {

    const {
        status,
        url
    } = response;

    const time = new Date();

    const errorAdditionalInfo = {
        time: `${time.toLocaleString()}, time zone offset: ${time.getTimezoneOffset()}`,
        endpoint: url,
        code: status
    };

    switch (true) {
    case status === 200:
        return await response.json();

    case status >= 500 && status <= 599:
        dispatch(setError({
            errorMessage: 'Your request could not be completed. Please try again later. If this issue persists, please contact support at',
            errorType: '500 series',
            ...errorAdditionalInfo
        }));
        return null;

    case status >= 400 && status <= 499:
        dispatch(setError({
            errorMessage: 'Your request could not be completed. Please try again later. If this issue persists, please contact support at',
            errorType: '400 series',
            ...errorAdditionalInfo
        }));
        return null;

    default:
        dispatch(setError({
            errorMessage: 'Your request could not be completed. Please try again later. If this issue persists, please contact support at',
            errorType: 'Unknown error',
            ...errorAdditionalInfo
        }));
        return null;
    }
};

/**
 *
 * @param {Function} dispatch
 * @param {Function} setError
 * @returns {null}
 */
const handleFetchError = (dispatch: Function, setError: Function) => {

    const time = new Date();

    const errorAdditionalInfo = {
        time: `${time.toLocaleString()}, time zone offset: ${time.getTimezoneOffset()}`,
        endpoint: '---',
        code: '---'
    };

    dispatch(setError({
        errorMessage: 'Your request could not be completed. Please try again later. If this issue persists, please contact support at',
        errorType: 'Fetch error',
        ...errorAdditionalInfo
    }));
    return null;
};

export default {
    /**
     * @async
     * @param {string} url - URL to download from
     * @param {Function} dispatch
     * @param {Object} actions
     * @returns {Promise<*>} - and we pass this Promise to <checkResponse> for:
     * a) handling errors;
     * b) extracting data on success request
     */
    getData: (url : string, dispatch: Function, actions: Object) : Promise<PromiseTypes> => {

        const {
            setError,
            startSpinner,
            stopSpinner
        } = actions;

        const fetchConfig : Object = {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'cookie' : '{cookie}',
                'Content-Type': 'application/json'
            }
        };
        dispatch(startSpinner('Loading data...'));
        return fetch(url, fetchConfig)
            .finally(dispatch(stopSpinner()))
            .then(response => checkResponse(response, dispatch, setError))
            .catch(() => handleFetchError(dispatch, setError));
    },

    /**
     *
     * @param {Array} args - a) URL to download from; b) body
     * @returns {Promise<*>} - and we pass this Promise to <checkResponse> for:
     * a) handling errors;
     * b) extracting data on success request
     */
    postData: (...args : Array<any>) => {

        const [ [url, body, method = 'POST', shouldNotStopSpinner ] , dispatch, actions ] = args;

        const {
            setError,
            startSpinner,
            stopSpinner
        } = actions;

        dispatch(startSpinner('Saving data...'));

        const fetchConfig : Object = {
            method,
            mode: 'cors',
            credentials: 'include',
            headers: {
                'cookie' : '{cookie}',
                'Content-Type': 'application/json',
                'X-Requested-With' : 'XMLHttpRequest',
            },
            body: JSON.stringify({...body}),
        };
        return fetch(url, fetchConfig)
            .finally(shouldNotStopSpinner ? () => {} : dispatch(stopSpinner()))
            .then(response => checkResponse(response, dispatch, setError))
            .catch(() => handleFetchError(dispatch, setError));
    }
};
