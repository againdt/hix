
package com.getinsured.ssap.model.financial.nmec;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "orgCode",
    "mecVerified",
    "startdateRequestedWithinCoverageReceived",
    "mecCodeReceived"
})
public class VHPC implements Serializable
{

    @JsonProperty("orgCode")
    private String orgCode;
    @JsonProperty("mecVerified")
    private String mecVerified;
    @JsonProperty("startdateRequestedWithinCoverageReceived")
    private Boolean startdateRequestedWithinCoverageReceived;
    @JsonProperty("mecCodeReceived")
    private String mecCodeReceived;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 8027338701867943046L;

    @JsonProperty("orgCode")
    public String getOrgCode() {
        return orgCode;
    }

    @JsonProperty("orgCode")
    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    @JsonProperty("mecVerified")
    public String getMecVerified() {
        return mecVerified;
    }

    @JsonProperty("mecVerified")
    public void setMecVerified(String mecVerified) {
        this.mecVerified = mecVerified;
    }

    @JsonProperty("startdateRequestedWithinCoverageReceived")
    public Boolean getStartdateRequestedWithinCoverageReceived() {
        return startdateRequestedWithinCoverageReceived;
    }

    @JsonProperty("startdateRequestedWithinCoverageReceived")
    public void setStartdateRequestedWithinCoverageReceived(Boolean startdateRequestedWithinCoverageReceived) {
        this.startdateRequestedWithinCoverageReceived = startdateRequestedWithinCoverageReceived;
    }

    @JsonProperty("mecCodeReceived")
    public String getMecCodeReceived() {
        return mecCodeReceived;
    }

    @JsonProperty("mecCodeReceived")
    public void setMecCodeReceived(String mecCodeReceived) {
        this.mecCodeReceived = mecCodeReceived;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("orgCode", orgCode).append("mecVerified", mecVerified).append("startdateRequestedWithinCoverageReceived", startdateRequestedWithinCoverageReceived).append("mecCodeReceived", mecCodeReceived).append("additionalProperties", additionalProperties).toString();
    }

}
