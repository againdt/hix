<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/general.css" />" media="screen" />

<!--<a href="<c:url value="/admin/reports/qhpenrollmentreport/graph"/>">back to Graphpage</a>-->


<div class="gutter10">
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
                    <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
                    <li><a href="#"><spring:message code='label.report'/></a></li>
                    <li><spring:message code='label.qhpReport'/></li>
                </ul><!--page-breadcrumb ends-->
        	<h1><spring:message code='label.qhpEnrollmentReport'/></h1>
        </div>
    <div class="row-fluid">
    	<div class="span3" id="sidebar">
    	<div class="header">
			<h4>Refine Results</h4>
		</div>
	             <form class="form-vertical gutter10 lightgray" id="frmbrokerreg" name="frmbrokerreg" action="<c:url value="/issuer/reports/qhpenrollmentreport/list" />" method="POST">
							<df:csrfToken/>
							<div class="control-group">
								<label for="planNumber" class="control-label"><spring:message code='label.planNumber' /></label>
								<div class="controls">
									<select id="planId" name="planId" class="span12">
										<option value=""><spring:message code='label.any' /></option>
										<c:forEach items="${planList}" var="plan">
											<option <c:if test="${planId == plan.issuerPlanNumber}">selected</c:if> value="${plan.issuerPlanNumber}">${plan.issuerPlanNumber}</option>
										</c:forEach>
									</select>
								</div> <!-- end of controls-->
							</div> <!-- end of control-group -->
                            
							<div class="control-group">
							<label for="planLevel" class="control-label"><spring:message  code="label.planLevel"/></label>
							<div class="controls">
								<c:set var="platinum" value="false"></c:set>
								<c:set var="gold" value="false"></c:set>
								<c:set var="silver" value="false"></c:set>
								<c:set var="bronze" value="false"></c:set>
								<c:set var="expandedBronze" value="false"></c:set>
								<c:set var="catastrophic" value="false"></c:set>
								
								<c:forEach items="${planLevel}" var="planlevelName">								 
									<c:choose>
									    <c:when test="${'platinum' == fn:toLowerCase(planlevelName)}">
									    	<c:set var="platinum" value="true"></c:set>
									    </c:when>
									    <c:when test="${'gold' == fn:toLowerCase(planlevelName)}">
									    	<c:set var="gold" value="true"></c:set>
									    </c:when>
									    <c:when test="${'silver' == fn:toLowerCase(planlevelName)}">
									     <c:set var="silver" value="true"></c:set>
									    </c:when>
									    <c:when test="${'bronze' == fn:toLowerCase(planlevelName)}">
									    	<c:set var="bronze" value="true"></c:set>
									    </c:when>
									    <c:when test="${'expandedbronze' == fn:toLowerCase(planlevelName)}">
									    	<c:set var="expandedBronze" value="true"></c:set>
									    </c:when>
									    <c:when test="${'catastrophic' == fn:toLowerCase(planlevelName)}">
									     	<c:set var="catastrophic" value="true"></c:set>
									    </c:when>
									    <c:otherwise>
									    </c:otherwise>
									</c:choose>	

								</c:forEach>
								
							    <label class="label-checkbox clearfix"><input id="platinumCheck" name="planLevel" type="checkbox" value="platinum" <c:if test="${platinum == 'true'}">checked</c:if> /><span><spring:message code='label.platinum'/></span></label>
							    <label class="label-checkbox clearfix"><input id="goldCheck" name="planLevel" type="checkbox" value="gold" <c:if test="${gold == 'true'}">checked </c:if> /><span><spring:message code='label.gold'/></span></label>
								<label class="label-checkbox clearfix"><input id="silverCheck" name="planLevel" type="checkbox" value="silver" <c:if test="${silver == 'true'}">checked</c:if> /><span><spring:message code='label.silver'/></span></label>
								<label class="label-checkbox clearfix"><input id="bronzeCheck" name="planLevel" type="checkbox" value="bronze" <c:if test="${bronze == 'true'}">checked</c:if> /><span><spring:message code='label.bronze'/></span></label>
								<label class="label-checkbox clearfix"><input id="expandedBronzeCheck" name="planLevel" type="checkbox" value="expandedbronze" <c:if test="${expandedBronze == 'true'}">checked</c:if> /><span><spring:message code='label.expandedBronze'/></span></label>
								<label class="label-checkbox clearfix"><input id="catastrophicCheck" name="planLevel" type="checkbox" value="catastrophic" <c:if test="${catastrophic == 'true'}">checked</c:if> /><span><spring:message code='label.catastrophic'/></span></label>															
								
								<%--<label class="label-checkbox clearfix"><input id="platinumCheck" name="platinumCheck" type="checkbox" value="platinum" checked="<c:if test="${platinum==platinumCheck and platinumCheck!='uncheck'}">checked</c:if>"/><span>Platinum</span></label> --%>
								<%--<label class="label-checkbox clearfix"><input id="goldCheck" name="goldCheck" type="checkbox" value="gold" checked="<c:if test="${gold==goldCheck}">checked</c:if>"/><span>Gold</span></label>--%>
								<%--<label class="label-checkbox clearfix"><input id="silverCheck" name="silverCheck" type="checkbox" value="silver" checked="<c:if test="${silver==silverCheck}">checked</c:if>"/><span>Silver</span></label> --%>
								<%--<label class="label-checkbox clearfix"><input id="bronzeCheck" name="bronzeCheck" type="checkbox" value="bronze" checked="<c:if test="${bronze==bronzeCheck}">checked</c:if>"/><span>Bronze</span></label>--%>
								<%--<label class="label-checkbox clearfix"><input id="catastrophicCheck" name="catastrophicCheck" type="checkbox" value="Catastrophic" checked="<c:if test="${Catastrophic==catastrophicCheck}">checked</c:if>"/><span>Catastrophic</span></label>--%>
							</div> <!-- end of controls-->
						</div> <!-- end of control-group -->

						<div class="control-group">
							<label for="market" class="control-label"><spring:message  code="label.planMarket"/></label>
							<div class="controls">
								<select id="marketID" name="marketID" class="span12">
									<option value=""><spring:message code='label.any' /></option>
									<option <c:if test="${'Shop'==marketID}">selected</c:if> value="Shop"><spring:message code='label.shop'/></option>
									<option <c:if test="${'Individual'==marketID}">selected</c:if> value="Individual"><spring:message code='label.individual'/></option>
								</select>
							</div> <!-- end of controls-->
						</div> <!-- end of control-group -->
                           
						<div class="control-group">
							<label for="status" class="control-label"><spring:message code='label.Period' /></label>
							<div class="controls">
								<select id="periodID" name="periodID" class="span12">
									<option <c:if test="${'Monthly'==periodID}">selected</c:if> value="Monthly"><spring:message code='label.monthly'/></option>
									<option <c:if test="${'Quarterly'==periodID}">selected</c:if> value="Quarterly"><spring:message code='label.quarterly'/></option>
									<option <c:if test="${'Yearly'==periodID}">selected</c:if> value="Yearly"><spring:message code='label.yearly'/></option>
							</select>
							</div> <!-- end of controls-->
						</div> <!-- end of control-group -->
						
						<div class="control-group">
							<label for="verified" class="control-label"><spring:message code='label.RatingRegion' /></label>
							<div class="controls">
								<select id="rName" name="rName" class="span12">
									<option value=""><spring:message code='label.any' /></option>
									<c:forEach var="regionList" items="${regionList}">
										<option <c:if test="${regionList==rName}">selected</c:if> value="${regionList}">${regionList}</option>
									</c:forEach>
								</select>
							</div> <!-- end of controls-->
						</div> <!-- end of control-group -->
                            
						<div class="txt-center">
 						<input type="submit" class="btn" value="<spring:message  code='label.go'/>" title="<spring:message  code='label.go'/>">
 						</div>
				 </form>
	</div>
    	
        <div id="rightpanel" class="span9">
        <div class="header">
        	<h4 class="pull-left">List View of Report</h4>
        	<a class="btn btn-primary btn-small pull-right" href="<c:url value="/issuer/reports/qhpenrollmentreport/graph?list=No"/>">Graph</a>
        </div>
           	<form class="form-horizontal" id="frmReport" name="frmReport" action="#" method="POST">
				<display:table name="listPageData" pagesize="${pageSize}" list="rates" requestURI="" sort="list" class="table table-condensed table-border-none table-striped" >
					<display:column property="timePeriod" titleKey="label.timePeriod" sortable="true" headerClass=""/>
		           	<display:column property="planNumber" titleKey="label.planNumber" sortable="false" headerClass="" />
		           	<display:column property="planLevel" titleKey="label.level" sortable="false" headerClass="" />
		           	<display:column property="planMarket" titleKey="label.Market" sortable="false" headerClass="" />
					<display:column property="enrollment" titleKey="label.enrollment" sortable="false" headerClass="" style="text-align: center;"/>
		           
		           <display:setProperty name="paging.banner.placement" value="bottom" />
		           <display:setProperty name="paging.banner.some_items_found" value=''/>
		           <display:setProperty name="paging.banner.all_items_found" value=''/>
		           <display:setProperty name="paging.banner.group_size" value='50'/>
		           <display:setProperty name="paging.banner.last" value=''/>
		           <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
		           <display:setProperty name="paging.banner.page.selected" value='<a class="active"><strong>{0}</strong></a>'/>
                   <display:setProperty name="paging.banner.onepage" value=''/>
		           <display:setProperty name="paging.banner.one_item_found" value=''/>
		           <display:setProperty name="paging.banner.first" value='<span class="pagelinks">
		           <div class="pagination center">
					<ul>
						<li>{0}</li>
						<li><a href="{3}"><spring:message code="label.next"/></a></li>
					</ul>
					</div>
					</span>'/>
				<display:setProperty name="paging.banner.last" value='<span class="pagelinks">
					<div class="pagination center">
						<ul>
							<li><a href="{2}"><spring:message code="label.prev"/></a></li>
							<li>{0}</li>
						</ul>
					</div>
					</span>'/>
				<display:setProperty name="paging.banner.full" value='
					<div class="pagination center">
						<ul>
							<li><a href="{2}"><spring:message code="label.prev"/></a></li>
							<li>{0}</li>
							<li><a href="{3}"><spring:message code="label.next"/></a></li>
						</ul>
					</div>
					'/>
				</display:table>
			</form>
        </div>
        
    </div><!--  end of row-fluid -->
</div>    	<!--  end of gutter10 -->