package com.getinsured.ssap.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.ssap.model.SaveRequest;

/**
 * Single Streamlined Application Service definition.
 *
 * @author Yevgen Golubenko
 * @since 2018-12-21
 */
public interface SingleStreamlinedApplicationService {
  /**
   * Saves {@link SingleStreamlinedApplication} object.
   *
   * @param application           {@link SingleStreamlinedApplication} object.
   * @param csrOverride           (optional) Customer Service Representative Override
   * @param applicationSource     application source.
   * @param sepEventType          type of Special Enrollment Period (Got Married, Divorced, etc.).
   * @param sepEventDate          date of Special Enrollment Event.
   * @return Updated {@link SingleStreamlinedApplication} object.
   * @throws Exception if any exceptions occurs.
   */
  SingleStreamlinedApplication save(SingleStreamlinedApplication application,
                                    boolean csrOverride,
                                    String applicationSource,
                                    String sepEventType,
                                    Date sepEventDate) throws Exception;

  /**
   * Saves {@link SingleStreamlinedApplication} that is part of given {@link SaveRequest}
   * @param saveRequest {@link SaveRequest} object.
   * @return saved {@link SingleStreamlinedApplication}
   * @see #save(SingleStreamlinedApplication, boolean, String, String, Date)
   */
  SingleStreamlinedApplication save(SaveRequest saveRequest) throws Exception;

  /**
   * Returns application for given application id.
   *
   * @param applicationId application id
   * @return {@link SingleStreamlinedApplication} object or null.
   * @throws Exception if exception is thrown.
   */
  SingleStreamlinedApplication getApplicationByApplicationId(long applicationId) throws Exception;

  /**
   * Returns list of {@link SsapApplicationResource} for given coverage year.
   *
   * @param coverageYear coverage year.
   * @return list of {@link SsapApplicationResource}
   */
  List<SsapApplicationResource> getSsapApplicationsForYear(int coverageYear) throws Exception;

  /**
   * Returns new {@link SsapApplication} for given {@link Household} object.
   * This can be used as a starting point for client side UI.
   *
   * @param household    {@link Household} record.
   * @param coverageYear coverage year.
   * @return stub of {@link SsapApplication}
   * @throws Exception if exception is thrown.
   */
  SsapApplication getNewSsapApplicationForHousehold(Household household, int coverageYear) throws Exception;

  /**
   * Returns JSON stub for new {@link SingleStreamlinedApplication} with given household details.
   *
   * @param household house hold details.
   * @return {@link SingleStreamlinedApplication}
   * @throws Exception if exception occurred.
   */
  SingleStreamlinedApplication createNewSingleStreamlinedApplication(Household household) throws Exception;

  /**
   * Retrieves all {@link SsapApplicationResource} for given coverage year.
   *
   * @param householdId  household id.
   * @param coverageYear coverage year (e.g. 2018, 2019, 2020, etc.)
   * @return list of {@link SsapApplicationResource} for given coverage year.
   */
  List<SsapApplicationResource> getSsapApplicationsForCoverageYear(long householdId, int coverageYear);

  /**
   * For given user, returns boolean value if we are allowed to start new SSAP for given year.
   * @param user {@link AccountUser} object (usually currently logged-in user).
   * @param coverageYear coverage year for which we need to check if new SSAP is allowed.
   * @return true if allowed to create new SSAP for given year, false otherwise.
   */
  boolean isNewApplicationCreationAllowed(AccountUser user, int coverageYear);

  /**
   * Returns application by given GUID (Case Number).
   *
   * @param caseNumber Case Number.
   * @return {@link SsapApplicationResource} object or null.
   */
  SsapApplication getSsapApplicationByCaseNumber(final String caseNumber);

  /**
   * Returns list of counties for given state code.
   * @param stateCode state code (CA, NV, etc.)
   * @return list of counties in given state or empty list if state code is invalid.
   */
  List<Map<String, String>> populateCountiesForState(String stateCode);

  /**
   * Returns list of counties for given state code and zip code.
   * @param stateCode state code (CA, NV, etc.)
   * @param zipCode zip code
   * @return list of counties in given state or empty list if state code/zip code combination is invalid.
   */
  List<Map<String, String>> populateCountiesForStateAndZipCode(String stateCode, String zipCode);
}
