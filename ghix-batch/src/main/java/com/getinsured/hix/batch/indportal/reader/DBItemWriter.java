package com.getinsured.hix.batch.indportal.reader;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.indportal.utils.BrokerService;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;

public class DBItemWriter implements ItemWriter<Broker> {

	private static final Logger LOGGER = Logger.getLogger(DBItemWriter.class);
	private String skipPattern;
	
    public String getSkipPattern() {
		return skipPattern;
	}
	private static String ACCOUNT_USER_ACTIVATION_LINK;

	private static String EXCHANGE_NAME;

	private static String EXCHANGE_PHONE;

	private static String EXCHANGE_URL;

	private static int EXPIRATION_DAYS;

	private static String EXPIRATION_DAYS_STR;
	
	private static final String CONSUMER_ACCOUNT_ACTIVATION_EMAIL = "consumerAccountActivationEmail";
	private static final String ADMIN_ACCOUNT_ACTIVATION_EMAIL = "adminUserAccountActivationEmail";

	private static final String ADMINISTRATOR = "Administrator";

	private BrokerService brokerService;

	private AccountActivationService accountActivationService;
	public void setAccountActivationService(AccountActivationService accountActivationService) {
		this.accountActivationService = accountActivationService;
	}

	public void setBrokerService(BrokerService brokerService) {
		this.brokerService = brokerService;
	}

	public void createStateContext() {
		ACCOUNT_USER_ACTIVATION_LINK = GhixPlatformEndPoints.GHIXWEB_SERVICE_URL + "account/user/activation/";
		EXCHANGE_NAME = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
		EXCHANGE_PHONE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE);
		EXCHANGE_URL = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL);
		EXPIRATION_DAYS = StringUtils.isNumeric(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.USER)) ?	
				Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.USER)) : 30 ;

		EXPIRATION_DAYS_STR = String.valueOf(EXPIRATION_DAYS);
	}
	
	@Override 
    public void write(List<? extends Broker> items) throws Exception { 
		

        for(Broker brokerObj : items) {
        brokerObj = brokerService.saveBrokerWithLocation(brokerObj);

        LOGGER.info(new StringBuffer().append("Broker Saved"));
		Map<String,String> consumerActivationDetails = new HashMap<String, String>();
		consumerActivationDetails.put("consumerName", brokerObj.getFirstName()+" " +brokerObj.getLastName());
		consumerActivationDetails.put("exchangeName",EXCHANGE_NAME);
		consumerActivationDetails.put("exchangePhone",EXCHANGE_PHONE);
		consumerActivationDetails.put("exchangeURL",EXCHANGE_URL);
		consumerActivationDetails.put("emailType",ADMIN_ACCOUNT_ACTIVATION_EMAIL);
		consumerActivationDetails.put("expirationDays",EXPIRATION_DAYS_STR);
		consumerActivationDetails.put("name", brokerObj.getFirstName()+" " +brokerObj.getLastName());
		
		CreatedObject createdObject = new CreatedObject();
		createdObject.setObjectId(brokerObj.getId());
		createdObject.setEmailId(brokerObj.getYourPublicEmail());
		createdObject.setRoleName(GhixRole.BROKER.toString());
		createdObject.setPhoneNumbers(Arrays.asList(brokerObj.getContactNumber().replaceAll("-", "")));
		createdObject.setFullName(brokerObj.getFirstName() + " " + brokerObj.getLastName());
		createdObject.setFirstName(brokerObj.getFirstName());
		createdObject.setLastName(brokerObj.getLastName());
		createdObject.setCustomeFields(consumerActivationDetails);

		CreatorObject creatorObject = new CreatorObject();
		creatorObject.setObjectId(0);
		creatorObject.setFullName(ADMINISTRATOR);
		creatorObject.setRoleName(GhixRole.ADMIN.toString());

		LOGGER.info(new StringBuffer().append("Invoked accountActivationService.initiateActivationForCreatedRecord()"));
		AccountActivation activationObj = accountActivationService.initiateActivationForCreatedRecord(createdObject, creatorObject, EXPIRATION_DAYS);
		LOGGER.info(new StringBuffer().append("Invocation success accountActivationService.initiateActivationForCreatedRecord()"+activationObj));
        }
 
    } 
}
