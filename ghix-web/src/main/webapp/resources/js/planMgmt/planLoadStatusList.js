var app = angular.module('planLoadStatusListApp', ['ui.grid', 'ui.bootstrap', 'ui.grid.resizeColumns', 'ui.grid.pagination']);

app.controller('PlanLoadStatusListCtrl', ['$scope', 'PlanLoadStatusListService', 'uiGridConstants', function ($scope, PlanLoadStatusListService, uiGridConstants) {

	var paginationOptions = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	};

	$(function(){$(".date-picker").datepicker({autoclose: true, forceParse: false, format: 'mm/dd/yyyy'});});

	$scope.colDef = [];
	$scope.PAGE_NUMBER = 1;

	$scope.resetFilter = function(logsData) {
		$("#issuerFilter").val("");
		$("#typeFilter").val("");
		$("#statusFilter").val("");
		$("#dateFilter").val("");
		$("#containsOfLogsArea").html("");
	};

	$scope.addLogsDataInPopup = function(logsData) {
		$("#containsOfLogsArea").html(logsData);
		$('#containerLogs').modal('show');
	};

	$scope.getPlanDataReport = function(logsData) {

		PlanLoadStatusListService.getReport($scope.PAGE_NUMBER, paginationOptions.pageSize, $("#issuerFilter").val(), $("#typeFilter").val(), $("#statusFilter").val(), $("#dateFilter").val()).success(function(data) {
//			debugger;
			if (data) {
				$scope.gridOptions.paginationCurrentPage = $scope.PAGE_NUMBER;
				$scope.gridOptions.data = data.content;
				$scope.gridOptions.totalItems = data.totalElements;
			}
			else {
				$scope.gridOptions.data.length = 0;
				$scope.gridOptions.totalItems = 0;
			}
		});
	};

	$scope.downloadDataFile = function(data) {
		PlanLoadStatusListService.downloadFile(data);
	};

	var displayName = {
		issuerName : 'Issuer Name',
		hiosIssuerID : 'Issuer HIOS ID',
		uploadedDate : 'Uploaded Date',
		type : 'Type',
		status : 'Status',
		description : 'Description',
		differenceReport : 'Difference Report'
	};

	var setPropertiesFn = function (elem, key) {

        switch (key) {
			case 'issuerName':
			case 'hiosIssuerID':
			case 'uploadedDate':
			case 'type':
			case 'status':
			case 'description':
			case 'differenceReport':
				elem.displayName = displayName[key];
        }
	};

	var excludeFields = ['serffReqID', 'logs', 'batchStatus', 'differenceReport'];

	PlanLoadStatusListService.getReport(paginationOptions.pageNumber, paginationOptions.pageSize, $("#issuerFilter").val(), $("#typeFilter").val(), $("#statusFilter").val(), $("#dateFilter").val()).success(function(data) {

//		debugger;
		$scope.gridOptions.data = data.content;

		if (data && data.size != 0) {

			angular.forEach(data.content[0], function(value, key) {

				if (!excludeFields.includes(key)) {
					var elem = null;

					if (key == 'issuerName')  {
						elem = {name:key, width: 250};
					}
					else if (key == 'description')  {
						elem = {name:key, width: 340};
					}
					else {
						elem = {name:key, width: 120};	// Default width
					}
					setPropertiesFn(elem, key);
					$scope.colDef.push(elem);
				}
			});

			// Add Logs column as 2nd last
			$scope.colDef.push({
				name: 'logs',
				visible: true,
				displayName: 'Logs',
				enableSorting: false,
				cellTemplate: '<a ng-show="COL_FIELD" href="javascript:void(0);" ng-click="grid.appScope.addLogsDataInPopup(COL_FIELD)">logs</a>',
				width: 60
			});

			// Add Difference Report column as last for MN state
			var exchangeState = $("#exchangeState").val();
			if (exchangeState && ('MN' == exchangeState || 'WA' == exchangeState || 'ID' == exchangeState)) {
				$scope.colDef.push({
					name: 'differenceReport',
					visible: true,
					displayName: 'Difference Report',
					enableSorting: false,
					cellTemplate: '<a ng-show="COL_FIELD" href="javascript:void(0);" ng-click="grid.appScope.downloadDataFile(COL_FIELD)">Download</a>',
					width: 140
				});
			}
			$scope.gridOptions.totalItems = data.totalElements;
		}
		else {
			$scope.gridOptions.data.length = 0;
			$scope.gridOptions.totalItems = 0;
		}
	});

	$scope.gridOptions = {
		paginationPageSizes: [5, 10, 20],
		paginationPageSize: paginationOptions.pageSize,
		useExternalPagination: true,
		columnDefs:  $scope.colDef,
		enableGridMenu: false,
		enableFiltering: false,
		enableSorting: true,
		// exporterPdfFilename: 'download.pdf',

		onRegisterApi: function(gridApi) {
			$scope.gridApi = gridApi;

			gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
				paginationOptions.pageNumber = newPage;
				paginationOptions.pageSize = pageSize;

				PlanLoadStatusListService.getReport(newPage, pageSize, $("#issuerFilter").val(), $("#typeFilter").val(), $("#statusFilter").val(), $("#dateFilter").val()).success(function(data) {

//					debugger;
					$scope.gridOptions.data = data.content;
					$scope.gridOptions.totalItems = data.totalElements;
				});
			});
		}
	};
}]);

app.service('PlanLoadStatusListService',['$http', function ($http) {

	function getReport(pageNumber, size, selectedIssuer, selectedType, selectedStatus, selectedDate) {
//		debugger;
		pageNumber = pageNumber > 0 ? pageNumber - 1 : 0;

		if (!selectedIssuer) {
			selectedIssuer = "";
		}

		if (!selectedType) {
			selectedType = "";
		}

		if (!selectedStatus) {
			selectedStatus = "";
		}

		if (!selectedDate) {
			selectedDate = "";
		}

		return  $http({
			method: 'GET',
			url: '/hix/admin/serff/getPlanLoadStatusDataList?page=' + pageNumber + '&size=' + size + '&issuerFilter=' + selectedIssuer + '&typeFilter=' + selectedType + '&statusFilter=' + selectedStatus + '&dateFilter=' + selectedDate
		});
	}

	function downloadFile(selectedDocumentId) {
//		debugger;

		if (!selectedDocumentId) {
			alert("Document ID is empty");
		}
		else {
			window.location.href = '/hix/admin/serff/downloadSerffTemplate?selectedDocumentId=' + selectedDocumentId;
		}
	}

	return {
		getReport:getReport,
		downloadFile:downloadFile
	};
}]);
