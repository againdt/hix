package com.getinsured.eligibility.redetermination.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.eligibility.ATXMLConverterController;
import com.getinsured.eligibility.active.enrollment.service.ActiveEnrollmentService;
import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareApplicationDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.dto.LifeChangeEventDTO;
import com.getinsured.iex.lce.ChangedApplicant;
import com.getinsured.iex.lce.RequestParamDTO;
import com.getinsured.iex.lce.SepRequestParamDTO;
import com.getinsured.iex.lce.SepResponseParamDTO;
import com.getinsured.iex.lce.SepTransientDTO;
import com.getinsured.iex.ssap.HomeAddress;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.MailingAddress;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.ReferralUtil;
import com.google.gson.Gson;

/**
 * Class is used to provide services for Clone Job.
 * 
 * @since July 30, 2019
 */
@Service("applicationCloneService")
public class ApplicationCloneServiceImpl implements ApplicationCloneService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationCloneServiceImpl.class);
	private static final String EXADMIN_USERNAME = "exadmin@ghix.com";
	private static final String SSAP_RENEWAL_EVENT = "SSAP_RENEWAL_EVENT";
	private static final String PROCESSING_RESULT = "processingResult";
	public static final String RENEWED_APPLICATION_ID = "renewedApplicationId";
	private static final String PARENT_APPLICATION_ID = "parentApplicationId";
	public static final String CLONED_APPLICATION_CASENUMBER = "clonedApplicationCaseNumber";
	private static final String APPLICATION_EXIST_IN = "APPLICATION_EXIST_IN_";
	private static final String PROCESSING_TIME = "processingTime";
	private static final String RENEWAL_CLONE_BATCH = "RENEWAL_CLONE_BATCH";

	@Autowired
	private SsapCloneApplicationService ssapCloneApplicationService;
	@Autowired
	private SsapCloneApplicantService ssapCloneApplicantService;
	@Autowired
	private SsapCloneApplicantEventService ssapCloneApplicantEventService;
	@Autowired
	private SsapCloneApplicationEventService ssapCloneApplicationEventService;
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private GIMonitorService giMonitorService;
	@Autowired
	private ActiveEnrollmentService activeEnrollmentService;
	@Autowired
	private UserService userService;
	@Autowired
	private SsapJsonBuilder ssapJsonBuilder;


	/**
	 * Method is used to Clone SSAP application.
	 */
	@Override
	public synchronized Map<String, String> cloneSsap(SsapApplication parentApplication, boolean createEventsFlag, AtomicBoolean cloneProgramEligibilities, boolean changeAppData) {

		LOGGER.info("Started Processing application {}", parentApplication.getId());
		Map<String, String> result = new HashMap<String, String>();
		SsapApplication currentApplication = null;
		long time = System.currentTimeMillis();
		String processingResult = null;
		String applicationSource = null;

		try {
			result.put(PARENT_APPLICATION_ID, String.valueOf(parentApplication.getId()));
			// Check if there is an active enrollment. If not put application in skipped status

			List<SsapApplicant> ssapApplicants;
			List<SsapApplicationEvent> ssapApplicationEvents;
			List<SsapApplicantEvent> ssapApplicantEvents;

			try {
				SepTransientDTO sepTransientDTO = new SepTransientDTO();
				SepResponseParamDTO sepResponseParamDTO = new SepResponseParamDTO();// -- seems unused DTO -- can be removed?
				String ssapJson = null;
				Map<String, Boolean> demographicFlagMap = new HashMap<String, Boolean>();
				Map<Long, List<String>> demographicEventsData = new HashMap<Long, List<String>>();
				List<Boolean> homeAddressChangeDemoFlags = new ArrayList<Boolean>();
				Date demographicEventDate = null;
				Date dobEventDate = null;

				ssapJson = parentApplication.getApplicationData();
				List<SsapApplicant> parentApplicants = ssapApplicantRepository.findBySsapApplication(parentApplication);
				SepRequestParamDTO sepRequestParamDTO = createSepRequestParamDTOForRenewal(parentApplication, parentApplicants);

				sepTransientDTO.setDemographicEventDate(demographicEventDate);
				sepTransientDTO.setDemographicEventsData(demographicEventsData);
				sepTransientDTO.setDemographicFlagMap(demographicFlagMap);
				sepTransientDTO.setHomeAddressChangeDemoFlags(homeAddressChangeDemoFlags);
				sepTransientDTO.setDobEventDate(dobEventDate);
				sepTransientDTO.setCmrHouseholdId(sepRequestParamDTO.getHouseholdId());
				//sepTransientDTO.setEnrolledPersons(invokeEnrollmentApi(parentApplication.getId(), sepRequestParamDTO.getUserName(), parentApplicants));
				sepTransientDTO.setUserName(sepRequestParamDTO.getUserName());
				sepTransientDTO.setUserId(sepRequestParamDTO.getUserId());
				List<RequestParamDTO> events = sepRequestParamDTO.getEvents();
				

				if (parentApplication.getRenewalStatus() != null && RenewalStatus.OTR.equals(parentApplication.getRenewalStatus())) {
					applicationSource = "ON";
				}
				else
				{
					applicationSource = parentApplication.getSource();
				}
				String applicationType = parentApplication.getApplicationType();
				if (ATXMLConverterController.nonCloeasblestatuses.contains(parentApplication.getApplicationStatus()))
				{
					applicationType = "SEP";
				}
				currentApplication = ssapCloneApplicationService.cloneSsapApplicationFromParent(ssapJson,
						currentApplication, parentApplication, sepRequestParamDTO, applicationType, applicationSource,
						ApplicationStatus.OPEN.getApplicationStatusCode(), parentApplication.getCoverageYear());
				ssapApplicants = ssapCloneApplicantService.cloneSsapApplicants(currentApplication, events,
						sepTransientDTO.getUserId(), sepResponseParamDTO, parentApplication,sepRequestParamDTO, cloneProgramEligibilities);
				if(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equalsIgnoreCase(parentApplication.getApplicationStatus()) && createEventsFlag){
					ssapApplicationEvents = ssapCloneApplicationEventService.cloneSsapApplicationEvent(currentApplication, sepTransientDTO.getUserId(), SsapApplicationEventTypeEnum.OE);
					ssapApplicantEvents = ssapCloneApplicantEventService.cloneSsapApplicantEvents(currentApplication, ssapApplicationEvents.get(0), ssapApplicants, events, sepTransientDTO);
				}
				
				ssapJson = updateMailingAddressSameAsHomeAddressIndicator(ssapJson, currentApplication);
				if(sepRequestParamDTO.getIsRenewalFlow() != null && !sepRequestParamDTO.getIsRenewalFlow() && changeAppData) {
					SingleStreamlinedApplication appJsonObj = ssapJsonBuilder.transformFromJson(ssapJson);
					appJsonObj.setCurrentPage(0);
					for(HouseholdMember householdMember : appJsonObj.getTaxHousehold().get(0).getHouseholdMember()) {
						householdMember.setSeeksQhp(true);
					}
					
					//Sort members by peronId
					Collections.sort(appJsonObj.getTaxHousehold().get(0).getHouseholdMember(), new Comparator<HouseholdMember>() {
						@Override
						public int compare(HouseholdMember householdMember1, HouseholdMember householdMember2) {
							return householdMember1.getPersonId().compareTo(householdMember2.getPersonId());
						}
					});
					
					ssapJson=ssapJsonBuilder.transformToJson(appJsonObj);
					
					for(SsapApplicant ssapApplicant : ssapApplicants) {
						ssapApplicant.setStatus("NO_CHANGE");
					}
				}
				currentApplication.setApplicationData(ssapJson);
				//ssapApplicationEvents.get(0).setSsapApplicantEvents(ssapApplicantEvents);
				currentApplication.setSsapApplicants(ssapApplicants);
				//currentApplication.setSsapApplicationEvents(ssapApplicationEvents);
				// Story - HIX-86352 - update CSR Level
				//ssapCloneApplicationService.updateCSRLevel(currentApplication);

				if (currentApplication != null) {
//							currentApplication.setRenewalStatus(RenewalStatus.STARTED);
					currentApplication = ssapCloneApplicationService.saveSsapSepApplication(currentApplication, true);

					if (LOGGER.isInfoEnabled()) {
						LOGGER.info("Cloned SSAP Application case number {} from Parent Application case number {}",
								currentApplication.getCaseNumber(), parentApplication.getCaseNumber());
					}
					parentApplication.setRenewalStatus(RenewalStatus.CLONED);
					ssapCloneApplicationService.saveSsapSepApplication(parentApplication, false);
					processingResult = "APPLICATION_CLONED".intern();
					result.put(RENEWED_APPLICATION_ID, String.valueOf(currentApplication.getId()));
					result.put(CLONED_APPLICATION_CASENUMBER, currentApplication.getCaseNumber());
				}
			}
			catch (Exception e) {
				LOGGER.error("Error while cloning application: {}", parentApplication.getId(), e);
				throw new GIException(50005, "Error while cloning application. " + parentApplication.getId()
						+ ExceptionUtils.getFullStackTrace(e), "High");
			}
		}
		catch (GIException e) {
			parentApplication.setRenewalStatus(RenewalStatus.ERROR);

			try {
				ssapCloneApplicationService.saveSsapSepApplication(parentApplication, false);
				giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_" + e.getErrorCode(), new Date(),
						this.getClass().getName(), ExceptionUtils.getFullStackTrace(e), null,
						parentApplication.getCaseNumber(), GIRuntimeException.Component.BATCH.getComponent(), null);
			}
			catch (Exception ex) {
				LOGGER.error("Error Processing application case number" + parentApplication.getCaseNumber() + " after step " + parentApplication.getRenewalStatus(), e);
			}
			LOGGER.error("Error Processing application case number" + parentApplication.getCaseNumber() + " after step " + parentApplication.getRenewalStatus(), e);
			processingResult = RenewalStatus.ERROR.toString();
			result.put("exception", ExceptionUtils.getFullStackTrace(e));
		}
		catch (Exception e) {
			parentApplication.setRenewalStatus(RenewalStatus.ERROR);

			try {
				ssapCloneApplicationService.saveSsapSepApplication(parentApplication, false);
				giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50001", new Date(), this.getClass().getName(),
						ExceptionUtils.getFullStackTrace(e), null, parentApplication.getCaseNumber(),
						GIRuntimeException.Component.BATCH.getComponent(), null);
			}
			catch (Exception ex) {
				LOGGER.error("Error Processing application case number" + parentApplication.getCaseNumber() + " after step " + parentApplication.getRenewalStatus(), e);
			}
			LOGGER.error("Error Processing application case number" + parentApplication.getCaseNumber() + " after step " + parentApplication.getRenewalStatus(), e);
			processingResult = RenewalStatus.ERROR.toString();
			result.put("exception", ExceptionUtils.getFullStackTrace(e));
		}
		result.put(PROCESSING_TIME, String.valueOf(System.currentTimeMillis() - time));
		result.put(PROCESSING_RESULT, processingResult);
		LOGGER.info("Task completed for application " + parentApplication.getId());
		return result;
	}

	/**
	 * Check if there is an existing application for renewal year
	 */
	private boolean noRenewalApplicationExist(SsapApplication parentApplication, Long renewalYear) {

		Long openApplicationCount = ssapCloneApplicationService.findOpenApplicationByCoverageYearAndCmrHouseoldId(parentApplication.getCmrHouseoldId(), renewalYear);

		if (openApplicationCount > 0) {
			return false;
		}
		return true;
	}

	/**
	 * Call Enrollment to check if there is an active enrollment
	 */
	private boolean isEnrollmentActive(SsapApplication parentApplication) throws GIException, RestClientException, JsonProcessingException {

		boolean isEnrollmentActive = false;

		try {
			isEnrollmentActive = activeEnrollmentService.isEnrollmentActive(parentApplication.getId(), EXADMIN_USERNAME);
		}
		catch (Exception e) {
			throw new GIException(50004, "Error while calling for enrollment API to determine isEnrollmentActive for " + parentApplication.getCaseNumber() + parentApplication.getId(), "High");
		}
		return isEnrollmentActive;
	}

	private SepRequestParamDTO createSepRequestParamDTOForRenewal(SsapApplication parentApplication, List<SsapApplicant> ssapApplicants) throws InvalidUserException {

		SepRequestParamDTO sepRequestParamDTO = new SepRequestParamDTO();

		if (parentApplication.getEsignDate() != null) {
			sepRequestParamDTO.setEsignDate(parentApplication.getEsignDate().toString());
		}
		sepRequestParamDTO.setEsignFirstName(parentApplication.getEsignFirstName());
		sepRequestParamDTO.setEsignLastName(parentApplication.getEsignLastName());
		sepRequestParamDTO.setEsignMiddleName(parentApplication.getEsignMiddleName());
		sepRequestParamDTO.setEvents(getEvents(ssapApplicants));
		sepRequestParamDTO.setHouseholdId(parentApplication.getCmrHouseoldId().longValue());
		sepRequestParamDTO.setSsapJSON(parentApplication.getApplicationData());
		sepRequestParamDTO.setIsRenewalFlow(false);
		if (userService.getLoggedInUser() != null && userService.getLoggedInUser().getId() > 0) {
			sepRequestParamDTO.setUserId(new Long(userService.getLoggedInUser().getId()));
			sepRequestParamDTO.setUserName(userService.getLoggedInUser().getUsername());
		}
		else {
			sepRequestParamDTO.setUserId(1L);
			sepRequestParamDTO.setUserName(EXADMIN_USERNAME);
		}
		return sepRequestParamDTO;
	}

	private List<RequestParamDTO> getEvents(List<SsapApplicant> ssapApplicants) {

		List<RequestParamDTO> requestParamDTOList = new ArrayList<RequestParamDTO>();
		RequestParamDTO requestParamDTO = new RequestParamDTO();
		requestParamDTO.setDataChanged(true);
		requestParamDTO.setEventCategory("OTHER");
		requestParamDTO.setEventSubCategory("OTHER");
		requestParamDTO.setEventSubCategoryDate(new Date().toString());
		List<ChangedApplicant> changedApplicants = new ArrayList<ChangedApplicant>();

		for (SsapApplicant ssapApplicant : ssapApplicants) {
			ChangedApplicant changedApplicant = new ChangedApplicant();
			changedApplicant.setEventDate(new Date().toString());
			changedApplicant.setIsChangeInZipCodeOrCounty(false);
			changedApplicant.setPersonId(ssapApplicant.getPersonId());
			changedApplicants.add(changedApplicant);
		}

		requestParamDTO.setChangedApplicants(changedApplicants);
		requestParamDTOList.add(requestParamDTO);
		return requestParamDTOList;
	}

	private Set<Long> invokeEnrollmentApi(long applicationId, String userName, List<SsapApplicant> parentApplicants)
			throws RestClientException, Exception {

		LifeChangeEventDTO lifeChangeEventDTO = new LifeChangeEventDTO();
		lifeChangeEventDTO.setOldApplicationId(applicationId);
		lifeChangeEventDTO.setUserName(userName);
		// String responseDTO =
		// ghixRestServiceInvoker.invokeRestService(lifeChangeEventDTO,
		// SsapEndPoints.LCE_GET_ENROLLED_APPLICANTS, HttpMethod.POST,
		// GIRuntimeException.Component.LCE.toString());
		Set<Long> enrolledPersons = null;
		lifeChangeEventDTO = invokeEnrollmentApi(lifeChangeEventDTO);
		enrolledPersons = lifeChangeEventDTO.getPersonIds();
		return enrolledPersons;
	}

	private LifeChangeEventDTO invokeEnrollmentApi(LifeChangeEventDTO lifeChangeEventDTO) throws Exception {

		List<EnrollmentShopDTO> enrollmentShopDTOs = null;
		EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
		Gson g = new Gson();
		enrolleeRequest.setSsapApplicationId(lifeChangeEventDTO.getOldApplicationId());
		// EnrolleeResponse enrolleeResponse =
		// enrolleeService.findEnrolleeByApplicationid(enrolleeRequest);
		ResponseEntity<String> response = ghixRestTemplate.exchange(
				GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID_JSON,
				lifeChangeEventDTO.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,
				g.toJson(enrolleeRequest));

		EnrolleeResponse enrolleeResponse = (EnrolleeResponse) g.fromJson(response.getBody(), EnrolleeResponse.class);

		if (null != enrolleeResponse & enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
			enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
			return extractEnrolledData(enrollmentShopDTOs, executeCompare(lifeChangeEventDTO.getOldApplicationId()), lifeChangeEventDTO);
		}
		else {
			throw new GIException("Unable to get Enrollment Plan Details. Error Details: " + enrolleeResponse.getErrCode() + ":" + enrolleeResponse.getErrMsg());
		}
	}

	private LifeChangeEventDTO extractEnrolledData(List<EnrollmentShopDTO> enrollmentShopDTOs, CompareMainDTO compareMainDTO, LifeChangeEventDTO lifeChangeEventDTO) {

		Set<Long> personIds = new HashSet<>();

		for (EnrollmentShopDTO enrollmentShopDTO : enrollmentShopDTOs) {
			List<EnrolleeShopDTO> enrolleeShopDTO = enrollmentShopDTO.getEnrolleeShopDTOList();

			for (EnrolleeShopDTO enrolleeMembers : enrolleeShopDTO) {

				for (CompareApplicantDTO enrolledApplicant : compareMainDTO.getEnrolledApplication().getApplicants()) {

					if (enrolleeMembers.getExchgIndivIdentifier().equals(enrolledApplicant.getApplicantGuid())) {
						personIds.add(enrolledApplicant.getPersonId());
					}
				}
			}
		}
		lifeChangeEventDTO.setPersonIds(personIds);
		return lifeChangeEventDTO;
	}

	private CompareMainDTO executeCompare(long enrolledApplicationId) throws Exception {

		CompareMainDTO compareMainDTO = null;
		final SsapApplication enrolledApplication = loadSsapApplicants(enrolledApplicationId);
		compareMainDTO = transformCompareDto(enrolledApplication);
		return compareMainDTO;
	}

	private SsapApplication loadSsapApplicants(Long enApp) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(enApp);
	}

	private CompareMainDTO transformCompareDto(SsapApplication enrolledApplication) {

		CompareMainDTO compareMainDTO = new CompareMainDTO();
		compareMainDTO.setEnrolledApplication(transformApplication(enrolledApplication));
		return compareMainDTO;
	}

	private CompareApplicationDTO transformApplication(SsapApplication ssapApplication) {

		final CompareApplicationDTO compareApplicationDTO = new CompareApplicationDTO();
		ReferralUtil.copyProperties(ssapApplication, compareApplicationDTO);
		transformApplicants(compareApplicationDTO, ssapApplication);
		return compareApplicationDTO;
	}

	private void transformApplicants(CompareApplicationDTO compareApplicationDTO, SsapApplication ssapApplication) {

		CompareApplicantDTO compareApplicantDTO;

		for (SsapApplicant ssapApplicant : ssapApplication.getSsapApplicants()) {
			compareApplicantDTO = new CompareApplicantDTO();
			ReferralUtil.copyProperties(ssapApplicant, compareApplicantDTO);
			compareApplicationDTO.addApplicant(compareApplicantDTO);
		}
	}

	private void updateDemographicMaps(Map<String, Boolean> demographicFlagMap) {
		demographicFlagMap.put(LifeChangeEventConstant.IS_DEMOGRAPHIC_CHANGE, false);
		demographicFlagMap.put(LifeChangeEventConstant.IS_DOB_CHANGE, false);
		demographicFlagMap.put(LifeChangeEventConstant.IS_OTHER_CHANGE, false);
		demographicFlagMap.put(LifeChangeEventConstant.IS_ADDRESS_DEMOGRAPHIC_CHANGE, false);
	}

	private String updateMailingAddressSameAsHomeAddressIndicator(String ssapJson, SsapApplication currentApplication) {

		SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());

		for (TaxHousehold taxHousehold : singleStreamlinedApplication.getTaxHousehold()) {

			for (HouseholdMember houseHoldMember : taxHousehold.getHouseholdMember()) {

				if (houseHoldMember.getHouseholdContact() != null) {
					HomeAddress homeAddress = houseHoldMember.getHouseholdContact().getHomeAddress();
					MailingAddress mailingAddress = houseHoldMember.getHouseholdContact().getMailingAddress();
					houseHoldMember.getHouseholdContact().setMailingAddressSameAsHomeAddressIndicator(isHomeAddressSameAsMailingAddress(homeAddress, mailingAddress) ? true : false);
				}
			}
		}
		return ssapJsonBuilder.transformToJson(singleStreamlinedApplication);
	}

	private boolean isHomeAddressSameAsMailingAddress(HomeAddress homeAddress, MailingAddress mailingAddress) {

		if (homeAddress != null && mailingAddress != null) {

			return (nullCheckedValue(homeAddress.getStreetAddress1()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getStreetAddress1()))
					&& nullCheckedValue(homeAddress.getStreetAddress2()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getStreetAddress2()))
					&& nullCheckedValue(homeAddress.getCity()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getCity()))
					&& nullCheckedValue(homeAddress.getCounty()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getCounty()))
					&& nullCheckedValue(homeAddress.getCountyCode()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getCountyCode()))
					&& nullCheckedValue(homeAddress.getPostalCode()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getPostalCode()))
					&& nullCheckedValue(homeAddress.getState()).equalsIgnoreCase(nullCheckedValue(mailingAddress.getState())));
		}
		return true;
	}

	private String nullCheckedValue(String value) {

		if (StringUtils.isBlank(value)) {
			return "";
		}
		return value;
	}
}
