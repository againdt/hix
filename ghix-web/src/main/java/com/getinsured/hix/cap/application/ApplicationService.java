package com.getinsured.hix.cap.application;

import javax.servlet.http.HttpServletRequest;

import com.getinsured.hix.dto.cap.ProgramEligibilityDTO;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface ApplicationService {

	public SsapApplication getSsapApplication(Long applicationId);

	ProgramEligibilityDTO getApplication(ProgramEligibilityDTO application) throws Exception;

	public String overrideProgramEligibility(ProgramEligibilityDTO programEligibilityDTO, String oldApplicationCaseNumber, String newApplicationCaseNumber, String coverageYear, HttpServletRequest request);

}
