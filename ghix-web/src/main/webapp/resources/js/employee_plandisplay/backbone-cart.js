(function (backbone) {
	/**
	 * @class
	 * Cart
	 */
	backbone.Cart = {
		/**  how many items to show per page */
		perPage : 1,
		
		/** page to start off on */
		page : 1,
	
		/**
		 *
		 */
		initialize: function(){

			this.resetProps();
		},
		
		resetProps: function(){
			this.cartSummary = [];
			this.cartSummary.planCount = 0;
			this.cartItems = [];
			this.selectedCartItem = "";
		},
		
		setCartSummary: function (models) {
			var self = this;
			if(!models){
				models = self.models;
			}
			self.cartSummary['planCount'] = models.length;
			if(models.length){
				_.each(models, function(model){
					self.cartItems.push(model.get("id"));
					self.selectedCartItem = model.get("id");
				});
			}else{
				self.resetProps();
			}
			self.reset(models);
		},
		
		removeSelectedItems: function (itemId) {
			var self = this
			var updatedModels = [];

			_.each(self.models, function(model){
				if(itemId == model.get("id") ){
					model.set({id: model.get("id")});
					$('#pleasewait').modal('show');
					model.destroy({success: function(model, response) {
						$('#pleasewait').modal('hide');
					}});
				}else{
					updatedModels.push(model);
				}
			});
			
			this.resetProps();
			this.setCartSummary(updatedModels);
		}

	};
	
})(App.backbone);