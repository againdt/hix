package com.getinsured.hix.provider.web;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.hix.admin.service.AdminService;
import com.getinsured.hix.model.Facility;
import com.getinsured.hix.model.Facility.FacilityType;
import com.getinsured.hix.model.FacilityAddress;
import com.getinsured.hix.model.FacilitySpecialityRelation;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Network;
import com.getinsured.hix.model.Network.NetworkType;
import com.getinsured.hix.model.Physician;
import com.getinsured.hix.model.Physician.PhysicianGender;
import com.getinsured.hix.model.Physician.PhysicianType;
import com.getinsured.hix.model.PhysicianAddress;
import com.getinsured.hix.model.PhysicianSpecialityRelation;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.PlanInsuranceType;
import com.getinsured.hix.model.Plan.PlanLevel;
import com.getinsured.hix.model.Provider;
import com.getinsured.hix.model.Provider.ProviderType;
import com.getinsured.hix.model.ProviderNetwork;
import com.getinsured.hix.model.Specialty;
import com.getinsured.hix.model.Specialty.GroupName;
import com.getinsured.hix.planmgmt.provider.repository.INetworkRepository;
import com.getinsured.hix.planmgmt.provider.repository.IProviderRepository;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.platform.location.service.AddressValidatorService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.DisplayUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixConstants.FileType;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.provider.service.ProviderSearchNetworkService;
import com.getinsured.hix.provider.service.ProviderService;
import com.google.gson.Gson;

@Controller
public class ProviderController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProviderController.class);
	private static final String IS_EDIT = "isEdit";
	private static final String PLAN_ID = "planId";

	private static final String PHYSICIANS = "physicians";
	private static final String PHYSICIANS_ADDRESS = "physiciansAddress";
	private static final String PHYSICIANS_SPECIALTY = "physiciansSpecialty";
	
	private static final String FACILITIES = "facilities";
	private static final String FACILITIES_ADDRESS = "facilitiesAddress";
	private static final String FACILITIES_SPECIALTY = "facilitiesSpecialty";
	
	private static final String NETWORK_ADEQUACY = "networkAdequacy";
	
	private static final String THE_FILE = "The file ";
	private static final String UPLOADED_SUCCESSFULLY = " uploaded successfully." ;
	
	private static final String PLAN = "plan";
	private static final String HOSPITAL_NAME = "hospitalName";
	private static final String HOSPITAL_TYPE = "hospitalType";
	private static final String HOSPITAL_NEAR_ZIP = "hospitalNearZip";

	private static final String FROM_INDEX = "fromIndex";
	private static final String TO_INDEX = "toIndex";
	
	private static final String TYPE = "type";

	private static final String DOCTOR_NAME = "doctorName";
	private static final String DOCTOR_GENDER = "doctorGender";
	private static final String DOCTOR_SPECIALTIES = "doctorSpecialties";
	private static final String DOCTOR_LANGUAGES = "doctorLanguages";
	private static final String DOCTOR_NEAR_ZIP = "doctorNearZip";

	private static final String DENTIST_NAME = "dentistName";
	private static final String DENTIST_GENDER = "dentistGender";
	private static final String DENTIST_SPECIALTIES = "dentistSpecialties";
	private static final String DENTIST_LANGUAGES = "dentistLanguages";
	private static final String DENTIST_NEAR_ZIP = "dentistNearZip";
	private static final String MARKERS = "markers";
	private static final String NO_NETWORK = "noNetwork";
	private static final String NOT_ASSOCIATED = "notAssociated";
	private static final String ALREADY_ADDED = "alreadyAdded";
	private static final String DATA_MISSING = "dataMissing";
	private static final String HIOS_STRING = "HIOS";
	private static final String HIOS_ADDED ="hiosAdded";
	private static final String HIOS_REJECTED ="hiosRejected";
	private static final String HIOS_NOTPROVIDED = "hiosNotProvided";
	private static final Boolean VALID_RECORD = true;
	private static final Boolean INVALID_RECORD = false;
	
	
	
	@Value("#{configProp['provider.uploadFilePath']}")
	private String uploadFilePath;
	@Value("#{configProp['enclarity.fileLocation']}")
	private String enclarityFileLocation;
	@Value("#{configProp['enclarity.StatusLogLocation']}")
	private String enclarityStatusLogLocation;
	
	@Value("#{configProp['provider.batchSize']}")
	private String batchSize;
	

	@Value("#{configProp['provider.fileFlushValue']}")
	private String fileFlushValue;

	@Autowired private ProviderService providerService;
	@Autowired private ProviderSearchNetworkService providerSearchNetworkService;
	@Autowired private PlanMgmtService planMgmtService;
	@Autowired private AdminService adminService;	
	@Autowired private IssuerService issuerService;
	@Autowired private ZipCodeService zipCodeService;
	@Autowired private AddressValidatorService addressValidatorService;
	@Autowired private INetworkRepository iNetworkRepository;
	@Autowired private IProviderRepository iProviderRepository;
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	private String adminLoginPath = "redirect:/account/user/login?module=admin";
	@Autowired private Gson platformGson;
	
	
	// Validation error header key
	private static final int ERROR_ON_RECORD = 0;
	private static final int PROVIDER_ID_ERROR =1;
	private static final int PROVIDER_TYPE_ERROR =2;
	private static final int FIRST_NAME_ERROR=7;
	private static final int LAST_NAME_ERROR=8;
	private static final int ADDRESS_1_ERROR =10;
	private static final int CITY_ERROR =11;
	private static final int STATE_ERROR =12;
	private static final int ZIP_ERROR =13;
	private static final int DATASOURCE_REF_ID_ERROR =16;
	private static final int FACILITY_TYPE_ERROR =19;
	private static final int NETWORK_ID_ERROR =20;
	private static final int FACILITY_NAME_ERROR =21;
	private static final int PHYSICIAN_OBJECT_ERROR =22;
	private static final int FACILITY_OBJECT_ERROR =23;
	private static final int NETWORK_TIER_ERROR = 24;
	private static final int GROUP_KEY_ERROR = 25;
	
	private static final String PAGE_SIZE = "pageSize";
	private static final String RESULT_SIZE = "resultSize";
	private static final String PHYSICIAN_OBJ = "physicianObj";
	private static final String FACILITY_OBJ = "facilityObj";
	
	private static final int PHYSICIAN_INDEX = 0;
	private static final int FACILITY_INDEX = 0;
	private static final int NETWORK_ID_INDEX = 1;
	private static final int NETWORK_TIER_INDEX = 2;
	private static final int GROUP_KEY_INDEX = 3;
	private static final int SPECIALITY_CODE_INDEX = 4;
	private static final int TAXONOMY_CODE_INDEX = 5;
	private static final int IN_CLAUSE_ITEM_COUNT = 500;

	private static Map<String,List<Network>> networkMap = new HashMap<String,List<Network>>();
	

	@RequestMapping(value = "/provider/network", method = RequestMethod.GET)
	public String showProviderNetwork(Model model, @RequestParam(value = IS_EDIT, required = false) String isEdit) {
		if (adminService.isAdminLoggedIn()) {
			String planId = (String) model.asMap().get(PLAN_ID);
			Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
			model.addAttribute("issuerId", plan.getIssuer().getId());
			model.addAttribute("issuerName", plan.getIssuer().getName());
			model.addAttribute(PLAN_ID, plan.getId());
			model.addAttribute("planName", plan.getName());
			model.addAttribute(IS_EDIT, isEdit);
			model.addAttribute("planNumber", plan.getIssuerPlanNumber());
			model.addAttribute("networkList", providerSearchNetworkService.getProviderNetworkByIssuerId(plan.getIssuer().getId()));
			int networkId = 0;
			if(plan.getNetwork() != null) {
				networkId = plan.getNetwork().getId();
			}
			model.addAttribute("selNetwork", networkId);
	
			if(plan.getInsuranceType().equalsIgnoreCase(PlanInsuranceType.HEALTH.toString())){
				return "provider/qhpnetwork";
			}
			else if(plan.getInsuranceType().equalsIgnoreCase(PlanInsuranceType.DENTAL.toString())){
				return "provider/qdpnetwork";
			}	
		}
		return adminLoginPath;
	}
	
	@RequestMapping(value = "/provider/network/save", method = RequestMethod.POST)
	public String saveProviderNetwork(
			@RequestParam("providerNetwork") String networkId,
			@RequestParam(PLAN_ID) String planId,  
			@RequestParam(value=IS_EDIT, required=false) String isEdit, 
			RedirectAttributes redirectAttrs) {
		if (!adminService.isAdminLoggedIn()) {
			return adminLoginPath;
		}
		
		String nextView = "";
		nextView = "redirect:/admin/planmgmt/submitqdpreview";
		
		Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
		
		if (plan.getInsuranceType().equals(PlanMgmtService.HEALTH)) {
			if (plan.getPlanHealth().getPlanLevel().equalsIgnoreCase(PlanLevel.SILVER.toString())) {
				nextView = "redirect:/admin/planmgmt/submitqhpvariations";
			} else {
				nextView = "redirect:/admin/planmgmt/submitqhpreview";
			}
		} else if (plan.getInsuranceType().equals(PlanMgmtService.DENTAL)) {
			nextView = "redirect:/admin/planmgmt/submitqdpreview";
		}
		
		try {
			planMgmtService.updateProvider(planId, networkId);
		} catch (Exception e) {
			throw new GIRuntimeException("ProviderController.saveProviderNetwork:"+e.getMessage(),e);
		}
		redirectAttrs.addFlashAttribute(PLAN_ID ,Integer.toString(plan.getId()));
		redirectAttrs.addFlashAttribute(IS_EDIT ,isEdit);
		return nextView;
	}

	/**
	 * uploadFile method is to store the file to the configured location folder.
	 * uploadFilePath is configured in configuration.properties file with the
	 * key name provider.uploadFilePath
	 */

	@RequestMapping(value = "/provider/network/fileupload", method = RequestMethod.POST)
	@ResponseBody public
	String uploadFile(
			@RequestParam(value = "fileToUpload", required = false) String fileToUpload,
			@RequestParam(value = PHYSICIANS, required = false) MultipartFile physicians,
			@RequestParam(value = PHYSICIANS_ADDRESS, required = false) MultipartFile physiciansAddress,
			@RequestParam(value = PHYSICIANS_SPECIALTY, required = false) MultipartFile physiciansSpecialty,
			@RequestParam(value = FACILITIES, required = false) MultipartFile facilities,
			@RequestParam(value = FACILITIES_ADDRESS, required = false) MultipartFile facilitiesAddress,
			@RequestParam(value = FACILITIES_SPECIALTY, required = false) MultipartFile facilitiesSpecialty,
			@RequestParam(value = NETWORK_ADEQUACY, required = false) MultipartFile networkAdequacy)
			{

		/*if (!adminService.isAdminLoggedIn()) {
			return adminLoginPath;
		}*/
		LOGGER.info("ProviderNetworkController :: uploadFile(request,multipart-request)");

		String returnString = null;

		if (fileToUpload.equalsIgnoreCase(PHYSICIANS) && physicians != null
				&& physicians.getSize() > 0) {
			String newPhyFileName = getFileName(PHYSICIANS);
			if (providerSearchNetworkService.uploadFile(uploadFilePath, physicians,newPhyFileName)) {
				returnString = physicians.getOriginalFilename() + ","+ newPhyFileName + "," + PHYSICIANS;
				LOGGER.info(THE_FILE + physicians.getOriginalFilename()+ UPLOADED_SUCCESSFULLY);
			}
		} else if (fileToUpload.equalsIgnoreCase(PHYSICIANS_ADDRESS)
				&& physiciansAddress != null && physiciansAddress.getSize() > 0) {
			String newPhyAddFileName = getFileName(PHYSICIANS_ADDRESS);
			if (providerSearchNetworkService.uploadFile(uploadFilePath,physiciansAddress, newPhyAddFileName)) {
				returnString = physiciansAddress.getOriginalFilename() + ","+ newPhyAddFileName + "," + PHYSICIANS_ADDRESS;
				LOGGER.info(THE_FILE + physiciansAddress.getOriginalFilename()+ UPLOADED_SUCCESSFULLY);
			}
		}

		else if (fileToUpload.equalsIgnoreCase(PHYSICIANS_SPECIALTY)
				&& physiciansSpecialty != null
				&& physiciansSpecialty.getSize() > 0) {
			String newPhySpecFileName = getFileName(PHYSICIANS_SPECIALTY);
			if (providerSearchNetworkService.uploadFile(uploadFilePath,physiciansSpecialty, newPhySpecFileName)) {
				returnString = physiciansSpecialty.getOriginalFilename() + ","+ newPhySpecFileName + "," + PHYSICIANS_SPECIALTY;
				LOGGER.info(THE_FILE + physiciansSpecialty.getOriginalFilename()+ UPLOADED_SUCCESSFULLY);
			}
		} else if (fileToUpload.equalsIgnoreCase(FACILITIES)
				&& facilities != null && facilities.getSize() > 0) {
			String newFacFileName = getFileName(FACILITIES);
			if (providerSearchNetworkService.uploadFile(uploadFilePath, facilities,newFacFileName)) {
				returnString = facilities.getOriginalFilename() + ","+ newFacFileName + "," + FACILITIES;
				LOGGER.info(THE_FILE + facilities.getOriginalFilename()+ UPLOADED_SUCCESSFULLY);
			}
		}

		else if (fileToUpload.equalsIgnoreCase(FACILITIES_ADDRESS)
				&& facilitiesAddress != null && facilitiesAddress.getSize() > 0) {
			String newFacAddFileName = getFileName(FACILITIES_ADDRESS);
			if (providerSearchNetworkService.uploadFile(uploadFilePath,facilitiesAddress, newFacAddFileName)) {
				returnString = facilitiesAddress.getOriginalFilename() + ","+ newFacAddFileName + "," + FACILITIES_ADDRESS;
				LOGGER.info(THE_FILE + facilitiesAddress.getOriginalFilename()+ UPLOADED_SUCCESSFULLY);
			}
		}

		else if (fileToUpload.equalsIgnoreCase(FACILITIES_SPECIALTY)
				&& facilitiesSpecialty != null
				&& facilitiesSpecialty.getSize() > 0) {
			String newFacSpecFileName = getFileName(FACILITIES_SPECIALTY);
			if (providerSearchNetworkService.uploadFile(uploadFilePath,facilitiesSpecialty, newFacSpecFileName)) {
				returnString = facilitiesSpecialty.getOriginalFilename() + ","+ newFacSpecFileName + "," + FACILITIES_SPECIALTY;
				LOGGER.info(THE_FILE + facilitiesSpecialty.getOriginalFilename()+ UPLOADED_SUCCESSFULLY);
			}
		}

		else if (fileToUpload.equalsIgnoreCase(NETWORK_ADEQUACY)
				&& networkAdequacy != null && networkAdequacy.getSize() > 0) {
			String newNetAdqFileName = getFileName(NETWORK_ADEQUACY);
			if (providerSearchNetworkService.uploadFile(uploadFilePath,networkAdequacy, newNetAdqFileName)) {
				returnString = networkAdequacy.getOriginalFilename() + ","+ newNetAdqFileName + "," + NETWORK_ADEQUACY;
				LOGGER.info(THE_FILE + networkAdequacy.getOriginalFilename()+ UPLOADED_SUCCESSFULLY);
			}
		}
		return returnString;
	}

	
	@RequestMapping(value = "/network/providers/add", method = RequestMethod.POST)
	public String addProviderNetwork(
			Model model,
			@RequestParam(value = PLAN_ID, required = false) String planId,
			@RequestParam(value = "networkType", required = false) String networkType,
			@RequestParam(value = "providerNetName", required = false) String providerNetName,
			@RequestParam(value = "hdnFacilities", required = false) String facilities,
			@RequestParam(value = "hdnFacilitiesAddress", required = false) String facilitiesAddress,
			@RequestParam(value = "hdnFacilitiesSpecialty", required = false) String facilitiesSpecialty,
			@RequestParam(value = "hdnPhysicians", required = false) String physicians,
			@RequestParam(value = "hdnPhysiciansAddress", required = false) String physiciansAddress,
			@RequestParam(value = "hdnPhysiciansSpecialty", required = false) String physiciansSpecialty,
			@RequestParam(value = "hdnNetworkAdequacy", required = false) String networkAdequacy,
			@RequestParam(value = IS_EDIT, required=false) String isEdit,
			RedirectAttributes redirectAttrs) {

		if (!adminService.isAdminLoggedIn()) {
			return adminLoginPath;
		}
		LOGGER.info("ProviderNetworkController :: addProviderNetwork()");
		Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));

		redirectAttrs.addFlashAttribute(PLAN_ID, Integer.toString(plan.getId()));
		redirectAttrs.addFlashAttribute(IS_EDIT ,isEdit);

		try {
			int issuerId = plan.getIssuer().getId();
			if (!providerNetName.isEmpty() && providerSearchNetworkService.isNetworkExists(networkType, providerNetName, issuerId)) {
				redirectAttrs.addFlashAttribute("result", "Provider Network Name already exists.Please add the provider with different name.");
				return "redirect:/provider/network";
			} else {
				
				Network network = new Network();
				if (!providerNetName.isEmpty()) {
					network.setIssuer(plan.getIssuer());
					network.setName(providerNetName);
					if(networkType.equalsIgnoreCase(NetworkType.PPO.toString())){
						network.setType(NetworkType.PPO);
					}else{
						network.setType(NetworkType.HMO);
					}
					network = providerSearchNetworkService.saveNetwork(network);
				}
				
				if (!facilities.isEmpty()) {
					List<List> listFacilities = providerSearchNetworkService.getFacilities(uploadFilePath + "/" + facilities);					
					for (List facilityData : listFacilities) {
						Facility facility = (Facility) facilityData.get(PHYSICIAN_INDEX);
						if (facility != null) {
							Provider provider = null;
							if (facility.getProvider() != null) {
								
								provider = facility.getProvider();									
								provider.setType(ProviderType.FACILITY);

								Set<ProviderNetwork> listProviderNetwork = new HashSet<ProviderNetwork>();
								ProviderNetwork providerNetwork = new ProviderNetwork();
								providerNetwork.setNetwork(network);
								providerNetwork.setProvider(provider);
								listProviderNetwork.add(providerNetwork);
								provider.setProviderNetwork(listProviderNetwork);

								facility.setProvider(provider);

							}
							providerSearchNetworkService.saveFacility(facility);

							if(facility.getFacilityAddressObj() != null && provider.getDatasourceRefId() != null){
								Facility facility2 = providerSearchNetworkService.getFacilityByDataSourceRefId(provider.getDatasourceRefId(), network.getId());
								FacilityAddress facilityAddress = null;
								facilityAddress = facility.getFacilityAddressObj();							
								facilityAddress.setFacility(facility2);								
								providerSearchNetworkService.saveFacilityAddress(facilityAddress);
							}
							
							if (facility.getSpecialty() != null && provider.getDatasourceRefId() != null && facility.getSpecialty().getName()!=null) {
								String[] specialityNameStr =  facility.getSpecialty().getName().split("\\|");
								String[] specialityCodeStr =  facility.getSpecialty().getCode().split("\\|");
								
								int index=0;
								for(String specialtyName : specialityNameStr){									
								
									Facility facility3 = providerSearchNetworkService.getFacilityByDataSourceRefId(provider.getDatasourceRefId(), network.getId());									
									Set<FacilitySpecialityRelation> facilitySpecialRelation = new HashSet<FacilitySpecialityRelation>();
									FacilitySpecialityRelation fsr = new FacilitySpecialityRelation();
									Specialty specialty = new Specialty();
									
									if (facility3 != null && providerSearchNetworkService.isSpecialtyExists(specialtyName.toString(), GroupName.FACILITY.toString())) {
										specialty = providerSearchNetworkService.getSpecialty(specialtyName.toString(), GroupName.FACILITY.toString());										
									} else {			
										specialty.setName(specialtyName.toString());
										if (specialityCodeStr!=null && specialityCodeStr.length>index) {
											specialty.setCode(specialityCodeStr[index]);
										}
										specialty.setGroupName(GroupName.FACILITY);	
									}

									fsr.setFacility(facility3);								
									fsr.setSpeciality(specialty);
									facilitySpecialRelation.add(fsr);
									specialty.setFacilitySpecialRelation(facilitySpecialRelation);
									providerSearchNetworkService.saveSpecialty(specialty);
									index ++;
								}					
							}
						}
					}
					planMgmtService.uploadAssociatedFiles(facilities, FileType.PROVIDER, null);
				}

				if (!physicians.isEmpty()) {
					
					List<List> listPhysicians = providerSearchNetworkService.getPhysicians(uploadFilePath + "/"+physicians);
					
					for (List physicianData : listPhysicians) {
						Physician physician = (Physician) physicianData.get(PHYSICIAN_INDEX);
						if (physician != null) {
							Provider provider = null;
							if (physician.getProviderObj() != null) {
									 
								provider = physician.getProviderObj();
								provider.setType(ProviderType.PHYSICIAN);
								
								String physicianName = physician.getFirst_name();
								physicianName += (physician.getMiddle_name().isEmpty()) ? physician.getMiddle_name() : " " + physician.getMiddle_name();
								physicianName += (physician.getLast_name().isEmpty()) ? physician.getLast_name() : " " + physician.getLast_name();
								physicianName += (physician.getSuffix().isEmpty()) ? physician.getSuffix() : " " + physician.getSuffix();			
								provider.setName(physicianName);	

								Set<ProviderNetwork> listProviderNetwork = new HashSet<ProviderNetwork>();
								ProviderNetwork providerNetwork = new ProviderNetwork();

								providerNetwork.setNetwork(network);
								providerNetwork.setProvider(provider);
								listProviderNetwork.add(providerNetwork);
								provider.setProviderNetwork(listProviderNetwork);
								
								physician.setProvider(provider);
							}
							providerSearchNetworkService.savePhysician(physician);
							
							if(physician.getPhysicianAddressObj() != null && provider.getDatasourceRefId() != null){
								// find physician in respect of provider ref # and provider network  
								Physician physician2 = providerSearchNetworkService.getPhysicianByDataSourceRefId(provider.getDatasourceRefId(), network.getId());
								PhysicianAddress physicianAddress = null;
								physicianAddress = physician.getPhysicianAddressObj();							
								physicianAddress.setPhysician(physician2);								
								providerSearchNetworkService.savePhysicianAddress(physicianAddress);
							}
							
							if (physician.getSpecialityObj() != null && provider.getDatasourceRefId() != null) {
								
								String[] specialityNameStr =   physician.getSpecialityObj().getName().split("\\|");
								String[] specialityCodeStr =   physician.getSpecialityObj().getCode().split("\\|");
								int index = 0;
								
								for(String specialtyName : specialityNameStr){									
									Physician physician3 = providerSearchNetworkService.getPhysicianByDataSourceRefId(provider.getDatasourceRefId(), network.getId());									
									Set<PhysicianSpecialityRelation> physicianSpecialRelation = new HashSet<PhysicianSpecialityRelation>();
									PhysicianSpecialityRelation psr = new PhysicianSpecialityRelation();

									Specialty specialty = new Specialty();
									
									if (physician3 != null && providerSearchNetworkService.isSpecialtyExists(specialtyName.toString(), physician.getType().toString())) {
										
										specialty = providerSearchNetworkService.getSpecialty(specialtyName.toString(), physician.getType().toString());										
									} else {			
										
										specialty.setName(specialtyName.toString());
										if (specialityCodeStr!=null && specialityCodeStr.length>index) {
											specialty.setCode(specialityCodeStr[index]);
										}
										
										if(physician.getType().toString().equalsIgnoreCase(GroupName.DENTIST.toString())){
											specialty.setGroupName(GroupName.DENTIST);
										}else if(physician.getType().toString().equalsIgnoreCase(GroupName.DOCTOR.toString())){
											specialty.setGroupName(GroupName.DOCTOR);
										}																			
									}

									psr.setPhysician(physician3);								
									psr.setSpecialty(specialty);
									physicianSpecialRelation.add(psr);
									specialty.setPhysicianSpecialRelation(physicianSpecialRelation);
									providerSearchNetworkService.saveSpecialty(specialty);
									index++;
								}					
							}
							
						}
					}
					planMgmtService.uploadAssociatedFiles(physicians, FileType.PROVIDER, null);
				}
				
			}// end of the provider network exists

		} catch (Exception e) {
			redirectAttrs.addFlashAttribute("result", "Could not add Provider Network.");
			LOGGER.error("Could not add Provider Network.");
		}// end of the catch
		redirectAttrs.addFlashAttribute("result", "Provider Network added successfully.");
		return "redirect:/provider/network";
	}
	
	public String getFileName(String type) {
		String issuer = "1";
		try {
			if (issuerService.isIssuerRepLoggedIn()) {
				issuer = issuerService.getIssuerId().toString();
			}
		} catch (InvalidUserException e) {
			throw new GIRuntimeException("ProviderController.getFileName:"+e.getMessage(),e);
		}
		StringBuffer sb = new StringBuffer();
		sb.append(issuer);
		sb.append("_");
		sb.append("provider");
		sb.append("_");
		sb.append(type);
		sb.append("_");
		sb.append(TimeShifterUtil.currentTimeMillis());
		sb.append(".csv");
		return sb.toString();
	}

	
	
	/* Provider Search */
	
	@RequestMapping(value = "/provider/search", method = RequestMethod.GET)
	public String showProviderSearch(Model model,@RequestParam(value = PLAN_ID, required = false) String planId){
	if(planId!=null && !(planId.isEmpty())){
		Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
		model.addAttribute(PLAN,plan);
	}
	model.addAttribute("listDentistSpecialties",populateDentistSpecialitiesList());
	model.addAttribute("listDoctorSpecialties",populateDoctorSpecialitiesList());
	return "provider/search";
	}

	@RequestMapping(value = "/provider/search/hospitals", method = {RequestMethod.GET, RequestMethod.POST })
	public String showProviderHospitalsSearchResults(
			Model model,
			@RequestParam(value = PLAN_ID, required = false) String planId,
			@RequestParam(value = HOSPITAL_NAME, required = false) String hospitalName,
			@RequestParam(value = HOSPITAL_TYPE, required = false) String hospitalType,
			@RequestParam(value = HOSPITAL_NEAR_ZIP, required = false) String hospitalNearZip,
			@RequestParam(value = "hospitalDistance", required = false) String hospitalDistance,
			@RequestParam(value = FROM_INDEX, required = false) String strFromIndex,
			@RequestParam(value = TO_INDEX, required = false) String strToIndex,
			@ModelAttribute("listHospitalType") Map<String, String> mapHospitalType)
			{
		
		List<Facility> listFacilities=null;
		String trimHospitalName    = hospitalName.trim();
		String trimHospitalNearZip = hospitalNearZip.trim();
		
		if(planId!=null && !(planId.isEmpty())){
			Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
			Integer networkId = plan.getNetwork()!=null?plan.getNetwork().getId():null;
			model.addAttribute(PLAN,plan);
			listFacilities = providerService.findHospitalsForNetworkAndPlan(plan.getId(),networkId,trimHospitalName, mapHospitalType.get(hospitalType),trimHospitalNearZip);
		}else{
			listFacilities = providerService.findHospitals(trimHospitalName, mapHospitalType.get(hospitalType),trimHospitalNearZip);
		}
				
		int totalSize=listFacilities.size();
		int fromIndex=0;
		int pageSize=(totalSize < 10)?totalSize:10;
		int toIndex=pageSize;
		
		if(strFromIndex!=null){
			
			if(Integer.parseInt(strFromIndex) > totalSize || Integer.parseInt(strFromIndex)<0){
				fromIndex=0;
				toIndex=pageSize;
				strFromIndex=Integer.toString(fromIndex);
				strToIndex=Integer.toString(toIndex);
			}
			else{
				fromIndex=Integer.parseInt(strFromIndex);
				toIndex=Integer.parseInt(strToIndex);
				toIndex=(toIndex>totalSize)?totalSize:toIndex;
			}
					
		}
		else{
			strFromIndex=Integer.toString(fromIndex);
			strToIndex=Integer.toString(toIndex);
		}
		
		
		
		List<Facility> subListFacilities = listFacilities.subList(fromIndex, toIndex);
				
		model.addAttribute("listFacilities", subListFacilities);
		model.addAttribute(TYPE, FacilityType.M.toString());
		model.addAttribute(HOSPITAL_NAME, hospitalName);
		model.addAttribute(HOSPITAL_TYPE, hospitalType);
		model.addAttribute(HOSPITAL_NEAR_ZIP, hospitalNearZip);
		model.addAttribute("displaySearchParam", DisplayUtil.displayValuesInParanthesis(hospitalName,hospitalNearZip,mapHospitalType.get(hospitalType)));
		model.addAttribute("size", totalSize);
		model.addAttribute(FROM_INDEX, strFromIndex);
		model.addAttribute(TO_INDEX, strToIndex);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute(MARKERS, (subListFacilities!=null)?getMarkersForFacilities(subListFacilities):"");
		
		return "provider/results";
	}
	@RequestMapping(value = "/provider/search/doctors",method = {RequestMethod.GET, RequestMethod.POST })
	public String showProviderDoctorsSearchResults(
			Model model,
			@RequestParam(value = PLAN_ID, required = false) String planId,
			@RequestParam(value = DOCTOR_NAME, required = false) String doctorName,
			@RequestParam(value = DOCTOR_GENDER, required = false) String doctorGender,
			@RequestParam(value = DOCTOR_SPECIALTIES, required = false) String doctorSpecialties,
			@RequestParam(value = DOCTOR_LANGUAGES, required = false) String doctorLanguages,
			@RequestParam(value = DOCTOR_NEAR_ZIP, required = false) String doctorNearZip,
			@RequestParam(value = "doctorDistance", required = false) String doctorDistance,
			@RequestParam(value = FROM_INDEX, required = false) String strFromIndex,
			@RequestParam(value = TO_INDEX, required = false) String strToIndex,
			@ModelAttribute("listLanguages") Map<String, String> mapLanguages)
			{
		
		
		List<Physician> listDoctors=null;
		String trimDoctorName=doctorName.trim();
		String trimDoctorNearZip=doctorNearZip.trim();
		int docSpecialties=(doctorSpecialties!=null && !doctorSpecialties.isEmpty())?Integer.parseInt(doctorSpecialties):0;
		
		Specialty specialty=providerService.findSpecialtyById(docSpecialties);
		
		if(planId!=null && !(planId.isEmpty())){
			Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
			Integer networkId = plan.getNetwork()!=null?plan.getNetwork().getId():null;
			model.addAttribute(PLAN,plan);
			listDoctors = providerService.findDoctorsForNetworkAndPlan(plan.getId(),networkId,trimDoctorName,doctorGender, docSpecialties,mapLanguages.get(doctorLanguages),trimDoctorNearZip);
		}else{
			listDoctors = providerService.findDoctors(trimDoctorName,doctorGender, docSpecialties,mapLanguages.get(doctorLanguages),trimDoctorNearZip);
		}
		
		
		
		int totalSize=listDoctors.size();
		int fromIndex=0;
		int pageSize=(totalSize < 10)?totalSize:10;
		int toIndex=pageSize;
		
		
		if(strFromIndex!=null){
			
			if(Integer.parseInt(strFromIndex) > totalSize|| Integer.parseInt(strFromIndex)<0){
				fromIndex=0;
				toIndex=pageSize;
				strFromIndex=Integer.toString(fromIndex);
				strToIndex=Integer.toString(toIndex);
			}
			else{
				fromIndex=Integer.parseInt(strFromIndex);
				toIndex=Integer.parseInt(strToIndex);
				toIndex=(toIndex>totalSize)?totalSize:toIndex;
			}
					
		}
		else{
			strFromIndex=Integer.toString(fromIndex);
			strToIndex=Integer.toString(toIndex);
		}
		
		List<Physician> subListDoctors = listDoctors.subList(fromIndex, toIndex);
		
		model.addAttribute("listPhysicians", subListDoctors);
		model.addAttribute(TYPE, PhysicianType.DOCTOR.toString());
		model.addAttribute(DOCTOR_NAME, doctorName);
		model.addAttribute(DOCTOR_GENDER,doctorGender);
		model.addAttribute(DOCTOR_SPECIALTIES,doctorSpecialties);
		model.addAttribute(DOCTOR_LANGUAGES,mapLanguages.get(doctorLanguages));
		model.addAttribute(DOCTOR_NEAR_ZIP, doctorNearZip);
		model.addAttribute("displaySearchParam", DisplayUtil.displayValuesInParanthesis(doctorName,doctorNearZip,(specialty!=null)?specialty.getName():null,getGenderValue(doctorGender),mapLanguages.get(doctorLanguages)));
		
		model.addAttribute("size", totalSize);
		model.addAttribute(FROM_INDEX, strFromIndex);
		model.addAttribute(TO_INDEX, strToIndex);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute(MARKERS, (subListDoctors!=null)?getMarkersForPhysicians(subListDoctors):"");
		return "provider/results";
	}

	
	private String getMarkersForPhysicians(List<Physician> physicianList){
		
		List<Map<String,String>> physicianMarkersList = new ArrayList<Map<String,String>>();
		for(Physician physicianObj: physicianList){
		    for(PhysicianAddress physicianAddressObj: physicianObj.getPhysicianAddress()){
				
						Map<String,String> physicianMarkersMap = new HashMap<String,String>();	
						physicianMarkersMap.put("lat", (physicianAddressObj.getLattitude()!=null)?physicianAddressObj.getLattitude().toString():"");
						physicianMarkersMap.put("lng", (physicianAddressObj.getLongitude()!=null)?physicianAddressObj.getLongitude().toString():"");
						physicianMarkersMap.put("name", physicianObj.getProvider().getName());
						physicianMarkersMap.put("address",physicianAddressObj.getAddress1() + " " + physicianAddressObj.getAddress2());
						physicianMarkersMap.put("city",physicianAddressObj.getCity());
						physicianMarkersMap.put("state",physicianAddressObj.getState());
						physicianMarkersMap.put("zip",physicianAddressObj.getZip());
						physicianMarkersMap.put("id", Integer.toString(physicianAddressObj.getId()));
						physicianMarkersList.add(physicianMarkersMap);	  
			  }
		}
		
		String physicianMarkersStr = platformGson.toJson(physicianMarkersList);
		
	return physicianMarkersStr;	
	}
private String getMarkersForFacilities(List<Facility> facilityList){
		
		List<Map<String,String>> facilityMarkersList = new ArrayList<Map<String,String>>();
		for(Facility facilityObj: facilityList){
		    for(FacilityAddress facilityAddressObj: facilityObj.getFacilityAddress()){
				
						Map<String,String> facilityMarkersMap = new HashMap<String,String>();	
						facilityMarkersMap.put("lat", (facilityAddressObj.getLattitude()!=null)?facilityAddressObj.getLattitude().toString():"");
						facilityMarkersMap.put("lng", (facilityAddressObj.getLongitude()!=null)?facilityAddressObj.getLongitude().toString():"");
						facilityMarkersMap.put("name", facilityObj.getProvider().getName());
						facilityMarkersMap.put("address",facilityAddressObj.getAddress1() + facilityAddressObj.getAddress2());
						facilityMarkersMap.put("city",facilityAddressObj.getCity());
						facilityMarkersMap.put("state",facilityAddressObj.getState());
						facilityMarkersMap.put("zip",facilityAddressObj.getZip());
						facilityMarkersMap.put("id", Integer.toString(facilityAddressObj.getId()));
						facilityMarkersList.add(facilityMarkersMap);	  
			  }
		}
		
		String  facilityMarkersStr = platformGson.toJson(facilityMarkersList);
		
	return facilityMarkersStr;	
	}
	@RequestMapping(value = "/provider/search/dentists", method = {RequestMethod.GET, RequestMethod.POST })
	public String showProviderDentistsSearchResults(
			Model model,
			@RequestParam(value = PLAN_ID, required = false) String planId,
			@RequestParam(value = DENTIST_NAME, required = false) String dentistName,
			@RequestParam(value = DENTIST_GENDER, required = false) String dentistGender,
			@RequestParam(value = DENTIST_SPECIALTIES, required = false) String dentistSpecialties,
			@RequestParam(value = DENTIST_LANGUAGES, required = false) String dentistLanguages,
			@RequestParam(value = DENTIST_NEAR_ZIP, required = false) String dentistNearZip,
			@RequestParam(value = "dentistDistance", required = false) String dentistDistance,
			@RequestParam(value = FROM_INDEX, required = false) String strFromIndex,
			@RequestParam(value = TO_INDEX, required = false) String strToIndex,
			@ModelAttribute("listLanguages") Map<String, String> mapLanguages)
			{
		
		List<Physician> listDentists =null;
		String trimDentistName=dentistName.trim();
		String trimDentistNearZip=dentistNearZip.trim();
		int denSpecialties=((dentistSpecialties!=null) && !dentistSpecialties.isEmpty())?Integer.parseInt(dentistSpecialties):0;
		Specialty specialty=providerService.findSpecialtyById(denSpecialties);
		
		if(planId!=null && !(planId.isEmpty())){
			Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
			Integer networkId = plan.getNetwork()!=null?plan.getNetwork().getId():null;
			model.addAttribute(PLAN,plan);
			listDentists = providerService.findDentistsForNetworkAndPlan(plan.getId(), networkId, trimDentistName, dentistGender, denSpecialties,mapLanguages.get(dentistLanguages),trimDentistNearZip);
		}else{
			listDentists = providerService.findDentists(trimDentistName, dentistGender, denSpecialties,mapLanguages.get(dentistLanguages),trimDentistNearZip);
		}
						
		int totalSize=listDentists.size();
		int fromIndex=0;
		int pageSize=(totalSize < 10)?totalSize:10;
		int toIndex=pageSize;
		
		if(strFromIndex!=null){
			
			if(Integer.parseInt(strFromIndex) > totalSize || Integer.parseInt(strFromIndex)<0){
				fromIndex=0;
				toIndex=pageSize;
				strFromIndex=Integer.toString(fromIndex);
				strToIndex=Integer.toString(toIndex);
			}
			else{
				fromIndex=Integer.parseInt(strFromIndex);
				toIndex=Integer.parseInt(strToIndex);
				toIndex=(toIndex>totalSize)?totalSize:toIndex;
			}
					
		}
		else{
			strFromIndex=Integer.toString(fromIndex);
			strToIndex=Integer.toString(toIndex);
		}
		
		List<Physician> subListDentists = listDentists.subList(fromIndex, toIndex);
		
		model.addAttribute("listPhysicians", subListDentists);
		model.addAttribute(TYPE,PhysicianType.DENTIST.toString());
		
		model.addAttribute(DENTIST_NAME, dentistName);
		model.addAttribute(DENTIST_GENDER,dentistGender);
		
		model.addAttribute(DENTIST_SPECIALTIES,dentistSpecialties);
		model.addAttribute(DENTIST_LANGUAGES,mapLanguages.get(dentistLanguages));
		model.addAttribute(DENTIST_NEAR_ZIP, dentistNearZip);
	    model.addAttribute("displaySearchParam", DisplayUtil.displayValuesInParanthesis(dentistName,dentistNearZip,(specialty!=null)?specialty.getName():null,getGenderValue(dentistGender),mapLanguages.get(dentistLanguages)));
		
		model.addAttribute("size", totalSize);
		model.addAttribute(FROM_INDEX, strFromIndex);
		model.addAttribute(TO_INDEX, strToIndex);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute(MARKERS, (subListDentists!=null)?getMarkersForPhysicians(subListDentists):"");
		
		return "provider/results";
	}

	@RequestMapping(value = "/provider/details", method = RequestMethod.GET)
	public String showProviderDetails(Model model,
			@RequestParam(value = PLAN_ID, required = false) String planId,
			@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = TYPE, required = false) String type,
			@RequestParam(value = HOSPITAL_NAME, required = false) String hospitalName,
			@RequestParam(value = HOSPITAL_TYPE, required = false) String hospitalType,
			@RequestParam(value = HOSPITAL_NEAR_ZIP, required = false) String hospitalNearZip,
			@RequestParam(value = DENTIST_NAME, required = false) String dentistName,
			@RequestParam(value = DENTIST_GENDER, required = false) String dentistGender,
			@RequestParam(value = DENTIST_SPECIALTIES, required = false) String dentistSpecialties,
			@RequestParam(value = DENTIST_LANGUAGES, required = false) String dentistLanguages,
			@RequestParam(value = DENTIST_NEAR_ZIP, required = false) String dentistNearZip,
			@RequestParam(value = DOCTOR_NAME, required = false) String doctorName,
			@RequestParam(value = DOCTOR_GENDER, required = false) String doctorGender,
			@RequestParam(value = DOCTOR_SPECIALTIES, required = false) String doctorSpecialties,
			@RequestParam(value = DOCTOR_LANGUAGES, required = false) String doctorLanguages,
			@RequestParam(value = DOCTOR_NEAR_ZIP, required = false) String doctorNearZip,
			@RequestParam(value = FROM_INDEX, required = false) String strFromIndex,
			@RequestParam(value = TO_INDEX, required = false) String strToIndex)
			{
		
		if(planId!=null && !(planId.isEmpty())){
			Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
			model.addAttribute(PLAN,plan);
		}
		model.addAttribute(TYPE, type);
		model.addAttribute(FROM_INDEX,strFromIndex);
		model.addAttribute(TO_INDEX, strToIndex);
		
		String insuranceNetwork=null;
		String insurancePlan=null;
		Set<ProviderNetwork> listProviderNetwork=null;
		
		if (type.equalsIgnoreCase(FacilityType.M.toString())) {
			Facility facility = providerService.findFacility(Integer.parseInt(id));
			listProviderNetwork= facility.getProvider().getProviderNetwork();
			// START : HIX-4186 :: Geocoding: Details - Doctors, Dentists and Hospitals/Facilities.
			List<Facility> facilityList = new ArrayList<Facility>();
			facilityList.add(facility);
			model.addAttribute(MARKERS, getMarkersForFacilities(facilityList));
			// END : HIX-4186
			model.addAttribute("facility", facility);
			model.addAttribute(HOSPITAL_NAME, hospitalName);
			model.addAttribute(HOSPITAL_TYPE, hospitalType);
			model.addAttribute(HOSPITAL_NEAR_ZIP, hospitalNearZip);
			
			
		} else if (type.equalsIgnoreCase(PhysicianType.DENTIST.toString()) ) {
			Physician physician = providerService.findPhysician(Integer.parseInt(id));
			listProviderNetwork= physician.getProvider().getProviderNetwork();
			// START : HIX-4186 :: Geocoding: Details - Doctors, Dentists and Hospitals/Facilities.
			List<Physician> dentistList = new ArrayList<Physician>();
			dentistList.add(physician);
			model.addAttribute(MARKERS, getMarkersForPhysicians(dentistList));
			// END : HIX-4186
			model.addAttribute("physician", physician);
			model.addAttribute(DENTIST_NAME, dentistName);
			model.addAttribute(DENTIST_GENDER, dentistGender);
			model.addAttribute(DENTIST_SPECIALTIES, dentistSpecialties);
			model.addAttribute(DENTIST_LANGUAGES, dentistLanguages);
			model.addAttribute(DENTIST_NEAR_ZIP, dentistNearZip);
			
		}
		else if(type.equalsIgnoreCase(PhysicianType.DOCTOR.toString())){
			Physician physician = providerService.findPhysician(Integer.parseInt(id));
			listProviderNetwork= physician.getProvider().getProviderNetwork();
			// START : HIX-4186 :: Geocoding: Details - Doctors, Dentists and Hospitals/Facilities.
			List<Physician> doctorList = new ArrayList<Physician>();
			doctorList.add(physician);
			model.addAttribute(MARKERS, getMarkersForPhysicians(doctorList));
			// END : HIX-4186
			model.addAttribute("physician", physician);
			model.addAttribute(DOCTOR_NAME, doctorName);
			model.addAttribute(DOCTOR_GENDER, doctorGender);
			model.addAttribute(DOCTOR_SPECIALTIES, doctorSpecialties);
			model.addAttribute(DOCTOR_LANGUAGES, doctorLanguages);
			model.addAttribute(DOCTOR_NEAR_ZIP, doctorNearZip);
			
		}

		for(ProviderNetwork providerNetworkObj :listProviderNetwork){
			
			try{
			
				insuranceNetwork=providerNetworkObj.getNetwork().getIssuer().getName();
				Plan plan=providerService.getPlanByNetworkId(providerNetworkObj.getNetwork().getId());
				insurancePlan=(plan!=null)?plan.getName():"";
			
			}catch(Exception e){
				LOGGER.error("Error getting Insurance Plan");
			}
		}
		if(!type.equalsIgnoreCase(PhysicianType.DOCTOR.toString()))
		{
			model.addAttribute("insuranceNetwork",insuranceNetwork);
		}
		model.addAttribute("insurancePlan",insurancePlan);
		
		return "provider/details";
	}

	@ModelAttribute("listHospitalType")
	public Map<String, String> populateHospitalTypeList() {
		Map<String, String> hospitaltype = new LinkedHashMap<String, String>();
		List<Specialty> specialties= null;//providerService.getParentSpecialtiesByGroupName(GroupName.FACILITY.toString());
		if(specialties!=null){
			for(Specialty specialtyObj:specialties){
				hospitaltype.put(Integer.toString(specialtyObj.getId()),specialtyObj.getName());
			}
		}
		return hospitaltype;
	}

	@ModelAttribute("listLanguages")
	public Map<String, String> populateLanguagesList() {
		Map<String, String> languages = new LinkedHashMap<String, String>();
		languages.put("1", "English");
		languages.put("2", "Spanish");
		return languages;
	}
	@ModelAttribute("listGender")
	public Map<String, String> populateGenderList() {
		Map<String, String> gender = new LinkedHashMap<String, String>();
		for(PhysicianGender genderObj : PhysicianGender.values()){
			gender.put(genderObj.toString(),genderObj.toString());
		}
		return gender;
	}
	
	public Map<Specialty, List<Specialty>> populateDoctorSpecialitiesList() {
	return getParentAndChildSpecialitiesByGroupName(GroupName.DOCTOR.toString());
	}
	
	public Map<Specialty, List<Specialty>> populateDentistSpecialitiesList() {
	return getParentAndChildSpecialitiesByGroupName(GroupName.DENTIST.toString());
	}
	
	public Map<Specialty, List<Specialty>> getParentAndChildSpecialitiesByGroupName(String groupName){
		Map<Specialty, List<Specialty>> listSpecialties = new LinkedHashMap<Specialty, List<Specialty>>();
		List<Specialty> specialties= providerService.getParentSpecialtiesByGroupName(groupName);
		for(Specialty specialtyObj:specialties){
			List<Specialty> childSpecialties=null;
			try{
					
				         boolean exists = providerService.isChildSpecialtiesExists(groupName,specialtyObj.getId());
				         if(exists){
				        	 childSpecialties=providerService.getChildSpecialtiesByParentId(groupName,specialtyObj.getId());
				        	 listSpecialties.put(specialtyObj,childSpecialties);
				         }
				         else{
							listSpecialties.put(specialtyObj,null);
						 }
				
				}catch(Exception e){
					LOGGER.error("Error getting Specialties.");
				}
			
		}
	 return listSpecialties;
	}
  
	public String getGenderValue(String gender){
	  HashMap<String, String> genderMap = new HashMap<String, String>();
	  genderMap.put("M", "Male");
	  genderMap.put("F", "Female");
	 return ((gender!=null) && !gender.isEmpty())?genderMap.get(gender):"";
    } 
	
	@RequestMapping(value = "/provider/enclaritydoctorupload", method = RequestMethod.GET)
	public String enclarityProviderUpload() {
		if (adminService.isAdminLoggedIn()) {
			return "provider/enclaritydoctorupload";
		}
		return adminLoginPath;
	}
	
	@RequestMapping(value = "/provider/enclarityfacilityupload", method = RequestMethod.GET)
	public String enclarityFacilityUpload() {
		if (adminService.isAdminLoggedIn()) {
			return "provider/enclarityfacilityupload";
		}
		return adminLoginPath;
	}


	private Location copyAddress(PhysicianAddress physicianAddress){
		Location location = new Location();
		try {
			location.setAddOnZip(Integer.parseInt(physicianAddress.getZip()));
			location.setAddress1(physicianAddress.getAddress1());
			location.setAddress2(physicianAddress.getAddress2());
			location.setCity(physicianAddress.getCity());
			location.setLat(physicianAddress.getLattitude());
			location.setLon(physicianAddress.getLongitude());
			location.setState(physicianAddress.getState());
			location.setCounty(physicianAddress.getState());
		} catch (Exception e) {
			LOGGER.error("parsing error "+e.getMessage());
		}
		return location;
	}
	
	private Location copyAddress(FacilityAddress facilityAddress){
		Location location = new Location();
		try {
			location.setAddOnZip(Integer.parseInt(facilityAddress.getZip()));
			location.setAddress1(facilityAddress.getAddress1());
			location.setAddress2(facilityAddress.getAddress2());
			location.setCity(facilityAddress.getCity());
			location.setLat(facilityAddress.getLattitude());
			location.setLon(facilityAddress.getLongitude());
			location.setState(facilityAddress.getState());
			location.setCounty(facilityAddress.getState());
		} catch (Exception e) {
			LOGGER.error("parsing error "+e.getMessage());
		}
		return location;
	}
	
	/* Add Provider from UI functionality code.
	@RequestMapping(value = "/provider/network/add", method = RequestMethod.POST)
	public String addProviderNetwork(Model model,
			@RequestParam(value = "hdnPhysicians", required = false) String physicians,
			@RequestParam(value = "hdnFacilities", required = false) String facilities,
			@RequestParam(value = PHYSICIANS, required = false) MultipartFile physiciansFile,
			@RequestParam(value = FACILITIES, required = false) MultipartFile facilitiesFile) {
		
		String type = "";
		String fileName = "";
		if (!adminService.isAdminLoggedIn()) {
			return adminLoginPath;
		}
		
		List<Map<Integer, String>> errorAndWarningList = new ArrayList<Map<Integer,String>>();
		
		try{
			if (physicians!=null && !physicians.isEmpty()) {
				type = PHYSICIANS;
				fileName = physiciansFile.getOriginalFilename();
				List<List> listPhysicians = providerSearchNetworkService.getPhysicians(uploadFilePath + "/"+physicians);

				int counter = 0;
				boolean errorFlag;
				int recordCounter = 1;

				for (List physicianData : listPhysicians){
					Physician physician = (Physician) physicianData.get(PHYSICIAN_INDEX);
					String networkIds = physicianData.get(NETWORK_ID_INDEX).toString();
					String networkTiers = physicianData.get(NETWORK_TIER_INDEX).toString();
					String groupKey = physicianData.get(GROUP_KEY_INDEX).toString();


					++recordCounter;
					errorFlag = false;

					Map<Integer, String> errorAndWarnMap = new HashMap<Integer, String>();

					if (null!= physician) {

						String  findProvider = providerService.getProviderByGroupKey(groupKey,Provider.ProviderType.PHYSICIAN);

						if(StringUtils.isBlank(findProvider)){
							if (null==groupKey||groupKey.isEmpty()) {
								errorAndWarnMap.put(PROVIDER_ID_ERROR, "Group Key is required");
								errorFlag = true;
							}

							if (null == physician.getType() || physician.getType().toString().isEmpty()) {
								errorAndWarnMap.put(PROVIDER_TYPE_ERROR, "Provider Type Indicator is required");
								errorFlag = true;
							}

							if (null == physician.getProviderObj().getNpiNumber() || physician.getProviderObj().getNpiNumber().isEmpty()) {
								errorAndWarnMap.put(NPI_NUMBER_ERROR, "NPI is required");
								errorFlag = true;
							}
							if (null == physician.getTitle() || physician.getTitle().isEmpty()) {
								errorAndWarnMap.put(TITLE_ERROR, "Title is required");
								errorFlag = true;
							}

							if (null == physician.getFirst_name() || physician.getFirst_name().isEmpty()) {
								errorAndWarnMap.put(FIRST_NAME_ERROR, "First Name is required");
								errorFlag = true;
							}

							if (null == physician.getLast_name() || physician.getLast_name().isEmpty()) {
								errorAndWarnMap.put(LAST_NAME_ERROR, "Last Name is required");
								errorFlag = true;
							}

							if (null == physician.getGender() || physician.getGender().toString().isEmpty()) {
								errorAndWarnMap.put(GENDER_ERROR, "Gender is required");
								errorFlag = true;
							}

							if (null  == physician.getPhysicianAddressObj().getAddress1() || physician.getPhysicianAddressObj().getAddress1().isEmpty()) {
								errorAndWarnMap.put(ADDRESS_1_ERROR, "Address line1 is required");
								errorFlag = true;
							}

							if (null == physician.getPhysicianAddressObj().getCity() || physician.getPhysicianAddressObj().getCity().isEmpty()) {
								errorAndWarnMap.put(CITY_ERROR, "City is required");
								errorFlag = true;
							}

							if (null == physician.getPhysicianAddressObj().getState() || physician.getPhysicianAddressObj().getState().isEmpty()) {
								errorAndWarnMap.put(STATE_ERROR, "State is required");
								errorFlag = true;
							}

							if (null == physician.getPhysicianAddressObj().getZip() || physician.getPhysicianAddressObj().getZip().isEmpty() || physician.getPhysicianAddressObj().getZip().length() != 5) {
								errorAndWarnMap.put(ZIP_ERROR, "Zip is required");
								errorFlag = true;
							}

							if (null == physician.getPhysicianAddressObj().getCounty() || physician.getPhysicianAddressObj().getCounty().isEmpty()) {
								errorAndWarnMap.put(COUNTY_ERROR, "County is required");
								errorFlag = true;
							}

							if (null == physician.getPhysicianAddressObj().getPhoneNumber() || physician.getPhysicianAddressObj().getPhoneNumber().isEmpty()) {
								errorAndWarnMap.put(PHONE_ERROR, "Phone number is required");
								errorFlag = true;
							}

							if (null == physician.getProviderObj().getDatasource() || physician.getProviderObj().getDatasource().toString().isEmpty()) {
								errorAndWarnMap.put(DATASOURCE_REF_ID_ERROR, "Data source is required");
								errorFlag = true;
							}

							if (null == physician.getProviderObj().getCreationTimestamp()) {
								errorAndWarnMap.put(CREATED_DATE_ERROR, "Creation date is required");
								errorFlag = true;
							}

							if (null == physician.getProviderObj().getLastUpdateDate()) {
								errorAndWarnMap.put(LAST_UPDATED_ERROR, "Last updated date is required");
								errorFlag = true;
							}

							if(null==networkIds||networkIds.isEmpty()){
								errorAndWarnMap.put(NETWORK_ID_ERROR, "Network ID is required");
								errorFlag = true;
							}
							if(null==networkTiers||networkTiers.isEmpty()){
								errorAndWarnMap.put(NETWORK_TIER_ERROR, "Network Tier is required");
								errorFlag = true;
							}
							if(null==groupKey||groupKey.isEmpty()){
								errorAndWarnMap.put(GROUP_KEY_ERROR, "Group Key is required");
								errorFlag = true;
							}

							if(errorFlag){
								errorAndWarnMap.put(ERROR_ON_RECORD,"Row No. "+recordCounter+" :Error while storing doctor / dentist .  ");
							}

							Location location = copyAddress(physician.getPhysicianAddressObj());
							location = validateAddress(location);

							if (location != null) {
								if(physician.getPhysicianAddressObj().getLattitude() == null && location.getLat() > 0){
									physician.getPhysicianAddressObj().setLattitude((float) location.getLat());
								}
								if (physician.getPhysicianAddressObj().getLongitude() == null && location.getLon() > 0) {
									physician.getPhysicianAddressObj().setLongitude((float) location.getLon());
								}
							}
							if(!errorFlag){

								StringBuffer physicianName = new StringBuffer();

								if (null!= physician.getSuffix() && physician.getSuffix().isEmpty()) {
									physicianName.append(physician.getSuffix());
									physicianName.append(" ");
								}

								physicianName.append(physician.getFirst_name());

								physicianName.append(" ");

								if (null!= physician.getMiddle_name() && physician.getMiddle_name().isEmpty()) {
									physicianName.append(physician.getMiddle_name());
									physicianName.append(" ");
								}
								
								Provider provider = physician.getProviderObj();
								provider.setName(physicianName.toString());
								provider.setGroupKey(groupKey);
								provider.setType(ProviderType.PHYSICIAN);
								physicianName.append(physician.getLast_name());
								
								List<String> networkTierList = new ArrayList<String>();
								if(networkTiers.contains("|")){
								String[] tempNetworkTierArray = networkTiers.split("\\|");
								for(int networkTierCount=0;networkTierCount<tempNetworkTierArray.length;networkTierCount++){
									networkTierList.add(tempNetworkTierArray[networkTierCount]);
								}
								}else{
									networkTierList.add(networkTiers);
								}
								int networkListcount = 0;
								Set<ProviderNetwork> setProviderNetwork = new HashSet<ProviderNetwork>();
								while(networkListcount<networkTierList.size()){
									Network network = new Network();
									String[] networkInfo = networkTierList.get(networkListcount).split("_");
									String networkId = networkInfo[0];
									String networkTierId = networkInfo[1];
									try{
										network = iNetworkRepository.getNeworkByNetworkIdValue(networkId);
										if(network!=null){
											ProviderNetwork providerNetwork = new ProviderNetwork();
											providerNetwork.setNetwork(network);
											providerNetwork.setProvider(provider);
											providerNetwork.setNetworkTier(networkTierId);
											setProviderNetwork.add(providerNetwork);
										}	
									}catch(Exception e){
										LOGGER.info("ERROR While fetchin netwokTier");
										errorAndWarnMap.put(NETWORK_ID_ERROR, "Network Tier value is incorrect");
									}
									networkListcount++;
								}
								if(setProviderNetwork!=null && setProviderNetwork.size()>0){
								provider.setProviderNetwork(setProviderNetwork);
								Set<PhysicianSpecialityRelation> physicianSpecialityRelationSet = new HashSet<PhysicianSpecialityRelation>();
								PhysicianSpecialityRelation physicianSpecialityRelation = new PhysicianSpecialityRelation();
								Specialty specialty = providerSearchNetworkService.saveSpecialty(physician.getSpecialityObj());

								physicianSpecialityRelation.setPhysician(physician);
								physicianSpecialityRelation.setSpecialty(specialty);
								physicianSpecialityRelationSet.add(physicianSpecialityRelation);
								physician.setProvider(iProviderRepository.saveAndFlush(provider));
								physician.setPhysicianSpecialRelation(physicianSpecialityRelationSet);

								Set<PhysicianAddress> physicianAddresseSet = new HashSet<PhysicianAddress>();
								PhysicianAddress physicianAddress = physician.getPhysicianAddressObj();	
								physicianAddresseSet.add(physicianAddress);
								physicianAddress.setPhysician(physician);
								physician.setPhysicianAddress(physicianAddresseSet);

								providerSearchNetworkService.savePhysician(physician);
								counter++;
								errorAndWarnMap.put(ERROR_ON_RECORD," Row No. "+recordCounter+"  :Doctor/Dentist data successfully added in dataBase.  ");
								errorFlag= true;
								}else{
									errorAndWarnMap.put(ERROR_ON_RECORD,"Row No. "+recordCounter+" :Error while storing doctor / dentist .  ");
									errorAndWarnMap.put(NETWORK_ID_ERROR,"Doctor/Dentist is not associated with network.");
									errorFlag= true;
								}
							}
						}else{
							errorAndWarnMap.put(ERROR_ON_RECORD,"Row No. "+recordCounter+"  :Doctor / Dentist Already Available in DataBase.");
							errorFlag = true;
						}
						
					}else{
						errorAndWarnMap.put(PHYSICIAN_OBJECT_ERROR, "Physician should not be null");
					}

					if(errorFlag){
						errorAndWarningList.add(errorAndWarnMap);
					}
					
				}
				model.addAttribute("TOTAL_ROWS", listPhysicians.size());
				model.addAttribute("SUCCESSFUL_ROWS", counter);
				model.addAttribute("FAILURE_ROWS", listPhysicians.size() - counter);
				model.addAttribute("errorsList", errorAndWarningList);
				writeEnclarityLog(fileName, type, counter, errorAndWarningList, listPhysicians.size(),true,null,null);
				try{
					planMgmtService.uploadAssociatedFiles(physicians, FileType.PROVIDER, null);
				}catch(Exception e){
					LOGGER.error("ERROR :"+e.getMessage());
				}
				return "provider/doctorupload/result";
			}

			if (facilities!=null && !facilities.isEmpty()) {

				List<List> listFacilities = providerSearchNetworkService.getFacilities(uploadFilePath + "/" + facilities);					
				type = FACILITIES;
				fileName = facilitiesFile.getOriginalFilename();
				int counter = 0;
				boolean errorFlag;
				int recordCounter = 1;

				for (List facilityData : listFacilities){
					Facility facility = (Facility) facilityData.get(FACILITY_INDEX);
					String networkIds = facilityData.get(NETWORK_ID_INDEX).toString();
					String networkTiers = facilityData.get(NETWORK_TIER_INDEX).toString();
					String groupKey = facilityData.get(GROUP_KEY_INDEX).toString();
					errorFlag = false;
					++recordCounter;
					Map<Integer, String> errorAndWarnMap = new HashMap<Integer, String>();

					if (facility != null) {
						
						String  findProvider = providerService.getProviderByGroupKey(groupKey,Provider.ProviderType.FACILITY);
						if(StringUtils.isBlank(findProvider)){
							if (null == groupKey || groupKey.isEmpty()) {
								errorAndWarnMap.put(PROVIDER_ID_ERROR, "Group Key is required");
								errorFlag = true;
							}

							if (null== facility.getFacility_type_indicator() || facility.getFacility_type_indicator().isEmpty()) {
								errorAndWarnMap.put(FACILITY_TYPE_ERROR, "Facility Type Indicator is required");
								errorFlag = true;
							}

							if (null== networkIds || networkIds.isEmpty()) {
								errorAndWarnMap.put(NETWORK_ID_ERROR, "Network Id is required");
								errorFlag = true;
							}

							if (null== networkTiers || networkTiers.isEmpty()) {
								errorAndWarnMap.put(NETWORK_TIER_ERROR, "Network Tier is required");
								errorFlag = true;
							}

							if (null== facility.getProvider().getName() || facility.getProvider().getName().isEmpty()) {
								errorAndWarnMap.put(FACILITY_NAME_ERROR, "Title is required");
								errorFlag = true;
							}

							if (null== facility.getFacilityAddressObj().getAddress1() || facility.getFacilityAddressObj().getAddress1().isEmpty()) {
								errorAndWarnMap.put(ADDRESS_1_ERROR, "Address line1 is required");
								errorFlag = true;
							}

							if (null== facility.getFacilityAddressObj().getCity() || facility.getFacilityAddressObj().getCity().isEmpty()) {
								errorAndWarnMap.put(CITY_ERROR, "City is required");
								errorFlag = true;
							}

							if (null== facility.getFacilityAddressObj().getState() || facility.getFacilityAddressObj().getState().isEmpty()) {
								errorAndWarnMap.put(STATE_ERROR, "State is required");
								errorFlag = true;
							}

							if (null== facility.getFacilityAddressObj().getZip() || facility.getFacilityAddressObj().getZip().isEmpty() || facility.getFacilityAddressObj().getZip().length() != 5) {
								errorAndWarnMap.put(ZIP_ERROR, "Zip is required");
								errorFlag = true;
							}

							if (null== facility.getFacilityAddressObj().getCounty() || facility.getFacilityAddressObj().getCounty().isEmpty()) {
								errorAndWarnMap.put(COUNTY_ERROR, "County is required");
								errorFlag = true;
							}

							if (null== facility.getFacilityAddressObj().getPhoneNumber() || facility.getFacilityAddressObj().getPhoneNumber().isEmpty()) {
								errorAndWarnMap.put(PHONE_ERROR, "Phone number is required");
								errorFlag = true;
							}

							if (null== facility.getProvider().getCreationTimestamp()) {
								errorAndWarnMap.put(CREATED_DATE_ERROR, "Creation date is required");
								errorFlag = true;
							}

							if (null== facility.getProvider().getLastUpdateDate()) {
								errorAndWarnMap.put(LAST_UPDATED_ERROR, "Last updated date is required");
								errorFlag = true;
							}

							if(errorFlag){
								errorAndWarnMap.put(ERROR_ON_RECORD,"Row No. "+recordCounter+"  :Error while storing hospital / facility. ");
							}

							Location location = copyAddress(facility.getFacilityAddressObj());
							location = validateAddress(location); 

							if (location!= null) {

								if (facility.getFacilityAddressObj().getLattitude() == null && location.getLat() > 0) {
									facility.getFacilityAddressObj().setLattitude((float) location.getLat());
								}
								if (facility.getFacilityAddressObj().getLongitude() == null && location.getLon() > 0) {
									facility.getFacilityAddressObj().setLongitude((float) location.getLon());
								}
							}

							if(!errorFlag) 
							{

								Provider provider = facility.getProvider();					
								provider.setType(ProviderType.FACILITY);
								provider.setGroupKey(groupKey);

								ProviderNetwork providerNetwork = new ProviderNetwork();

								providerNetwork.setProvider(provider);
								List<String> networkTierList = new ArrayList<String>();
								if(networkTiers.contains("|")){
									String[] tempNetworkTierArray = networkTiers.split("\\|");
									for(int networkTierCount=0;networkTierCount<tempNetworkTierArray.length;networkTierCount++){
										networkTierList.add(tempNetworkTierArray[networkTierCount]);
									}
								}else{
									networkTierList.add(networkTiers);
								}
								int networkListcount = 0;
								Set<ProviderNetwork> setProviderNetwork = new HashSet<ProviderNetwork>();
								while(networkListcount<networkTierList.size()){
									Network network = new Network();
									String[] networkInfo = networkTierList.get(networkListcount).split("_");
									String networkId = networkInfo[0];
									String networkTierId = networkInfo[1];
									try{
										network = iNetworkRepository.getNeworkByNetworkIdValue(networkId);
										if(network !=null){
											providerNetwork.setNetwork(network);
											providerNetwork.setProvider(provider);
											providerNetwork.setNetworkTier(networkTierId);
											setProviderNetwork.add(providerNetwork);
										}	
									}catch(Exception e){
										LOGGER.info("ERROR While fetchin netwokTier");
										errorAndWarnMap.put(NETWORK_ID_ERROR, "Network Tier is incorrect");
									}
									networkListcount++;
								}
								if(setProviderNetwork!=null && setProviderNetwork.size()>0){

								provider.setProviderNetwork(setProviderNetwork);


								Set<FacilitySpecialityRelation> facilitySpecialRelationSet = new HashSet<FacilitySpecialityRelation>();
								FacilitySpecialityRelation specialityRelation = new FacilitySpecialityRelation();
								Specialty specialty = providerSearchNetworkService.saveSpecialty(facility.getSpecialty());

								specialityRelation.setFacility(facility);
								specialityRelation.setSpeciality(specialty);
								facilitySpecialRelationSet.add(specialityRelation);
								facility.setFacilitySpecialRelation(facilitySpecialRelationSet);
								facility.setProvider(provider);
								List<FacilityAddress> facilityAddressList = new ArrayList<FacilityAddress>();
								FacilityAddress facilityAddress = facility.getFacilityAddressObj();							
								facilityAddress.setFacility(facility);
								facilityAddressList.add(facilityAddress);
								facility.setFacilityAddress(facilityAddressList);

								providerSearchNetworkService.saveFacility(facility);
								counter++;
								errorAndWarnMap.put(ERROR_ON_RECORD," Row No. "+recordCounter+"  :Facility data successfully added in dataBase.  ");
								errorFlag = true;
								}else{
									errorAndWarnMap.put(ERROR_ON_RECORD," Row No. "+recordCounter+"  :Facility is not associated with network.  ");
									errorFlag= true;
								}
							}
							
						}else{
							errorAndWarnMap.put(ERROR_ON_RECORD," Row No. "+recordCounter+"  :Facility data Already Available in DataBase.  ");
							errorFlag = true;
						}

					}else{
						errorAndWarnMap.put(FACILITY_OBJECT_ERROR, "Facility should not be null");
						errorFlag = true;
					}
					
					if(errorFlag){
						errorAndWarningList.add(errorAndWarnMap);
					}
				}
				model.addAttribute("TOTAL_ROWS", listFacilities.size());
				model.addAttribute("SUCCESSFUL_ROWS", counter);
				model.addAttribute("FAILURE_ROWS", listFacilities.size() - counter);
				model.addAttribute("errorsList", errorAndWarningList);
				try{
				planMgmtService.uploadAssociatedFiles(physicians, FileType.PROVIDER, null);
				}catch(Exception e){
					LOGGER.error("ERROR :"+e.getMessage());
				}
				writeEnclarityLog(fileName, type, counter, errorAndWarningList, listFacilities.size(),true,null,null);
				return "provider/doctorupload/result";
			}
		}
		catch (Exception e){
			LOGGER.error("Could not add Provider Network." + e.getMessage());
		}
		return adminLoginPath;
	}
*/

	@RequestMapping(value = "/provider/network/filebaseupload", method = RequestMethod.POST)
	public @ResponseBody String addProviderNetworkFileBasedUpload(@RequestBody String req){
		String type = "";
		String fileName = "";
		String uploadedFileName = "";
		int maxBatchSize = Integer.parseInt(batchSize);
		int maxFileSize = Integer.parseInt(fileFlushValue);

		long startTime, endTime;
		StringBuilder builder = new StringBuilder();
		
		boolean isIndividualFileUnavailable = true;
		boolean isFacilityFileUnavailable = true;
		File file = null;
		
		Map<String, Set<PhysicianAddress>> duplicatePhysicianGroupIdMap = new HashMap<String, Set<PhysicianAddress>>();
		Map<String, Set<FacilityAddress>> duplicateFacilityGroupIdMap = new HashMap<String, Set<FacilityAddress>>();
		
		List<Map<Integer, String>> errorAndWarningListFacility = new ArrayList<Map<Integer,String>>(maxFileSize);
		List<Map<Integer, String>> errorAndWarningListIndividual = new ArrayList<Map<Integer,String>>(maxFileSize);;
		
		try{
			String files;
			File uploadFolder = new File(enclarityFileLocation);
			File logFolder = new File(enclarityStatusLogLocation);
			if(!uploadFolder.exists()){
				uploadFolder.mkdirs();
			}
			if(!logFolder.exists()){
				logFolder.mkdirs();
			}
			File[] listOfAllFiles = uploadFolder.listFiles();
			List<File> listOftxtFiles = new ArrayList<File>();
			Date date = new TSDate( );
			SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyyMMdd");
			for(int i = 0; i < listOfAllFiles.length; i++){
				String tempFileName = listOfAllFiles[i].getName();
				if(tempFileName.contains("individual")||tempFileName.contains("facility")){
					if(tempFileName.contains(dateFormat.format(date).toString())){
						listOftxtFiles.add(listOfAllFiles[i]);	
					}					
				}
			}
			File[] listOfFiles = listOftxtFiles.toArray(new File[listOftxtFiles.size()]);
			
			Map<String, Integer> networkIdStatusMapPhysician = new  HashMap<String, Integer>();
			Map<String, Integer> networkIdStatusMapFacility = new  HashMap<String, Integer>();
			Map<String, Specialty> specialtyMap = new HashMap<String,Specialty>();
			Map<String,String> taxonomySpecialtyCodeMap = new HashMap<String,String>();
			try{
				for(Specialty specialty : providerService.getSpecialityList()){
					specialtyMap.put(specialty.getCode(), specialty);
					if(taxonomySpecialtyCodeMap.containsKey(specialty.getTaxonomyCode())){
						String taxonomySpcialtyCode = taxonomySpecialtyCodeMap.get(specialty.getTaxonomyCode());
						taxonomySpecialtyCodeMap.put(specialty.getTaxonomyCode(), taxonomySpcialtyCode+"|"+specialty.getCode());;
					}else{
						taxonomySpecialtyCodeMap.put(specialty.getTaxonomyCode(), specialty.getCode());
					}
				}
			}catch(Exception e){
				LOGGER.error("Error while setting specialty Map"+e);
			}
			for (int i = 0; i < listOfFiles.length; i++) {

				if (listOfFiles[i].isFile()) {
					files = listOfFiles[i].getName();
					if (files.contains("individual")){
						isIndividualFileUnavailable = false;
						file =  listOfFiles[i];
						uploadedFileName = file.getPath();
						fileName = file.getName();
						type = PHYSICIANS;
						if((files.endsWith(".txt") || files.endsWith(".TXT"))){
							uploadedFileName = uploadFilePath + File.separator + copyToCSV(file, PHYSICIANS);
						}else if(files.endsWith(".csv") || files.endsWith(".CSV")){
							uploadedFileName = enclarityFileLocation+ File.separator +fileName;
						}
					}else{
						isFacilityFileUnavailable = false;
						if (files.contains("facility")&&(files.endsWith(".txt") || files.endsWith(".TXT")||files.endsWith(".csv") || files.endsWith(".CSV"))){
							file =  listOfFiles[i];
							uploadedFileName = file.getPath();
							fileName = file.getName();
							type = FACILITIES;
							if((files.endsWith(".txt") || files.endsWith(".TXT"))){
								uploadedFileName = uploadFilePath + File.separator + copyToCSV(file, FACILITIES);
							}else if(files.endsWith(".csv") || files.endsWith(".CSV")){
								uploadedFileName = enclarityFileLocation+ File.separator +fileName;
							}
						}
					}

				}


				try{
					StringBuilder groupKeyBuilder = new StringBuilder();
					if (type!=null && !type.isEmpty()&&type.equalsIgnoreCase(PHYSICIANS)) {
						
						startTime = TimeShifterUtil.currentTimeMillis();
						
						List<List> listPhysicians = providerSearchNetworkService.getPhysicians(uploadedFileName);
						
						endTime = TimeShifterUtil.currentTimeMillis();
						
						LOGGER.info("***************************************************************");
						LOGGER.info("Time taken to read the file is	: "+ (endTime - startTime)/1000);
						LOGGER.info("***************************************************************");
						
						int counter = 0;
						boolean errorFlag;
						boolean isNetworkIdError;
						int recordCounter = 1;
						Set<String> hiosIdSetPhysician = new HashSet<String>();
						Map<String,Boolean> physicianGroupKeyDb  = new HashMap<String,Boolean>();
						Map<String, String> fieldSizeErrorMap = new HashMap<String, String>();
						
						if(listPhysicians.size()>0){
							EntityManager entityManager = entityManagerFactory.createEntityManager();
							File physicianStatusLogFile = writeEnclarityLogFile(uploadedFileName, type, true);
							boolean isNewPhysician = false;
							int insertCount = 0;
							for (List physicianData : listPhysicians){
								if(insertCount==0 && !entityManager.getTransaction().isActive()){
									entityManager.getTransaction().begin();
								}
								
								isNewPhysician = false;
								Physician physician = (Physician) physicianData.get(PHYSICIAN_INDEX);
								String networkIds = physicianData.get(NETWORK_ID_INDEX).toString();
								String networkTiers = physicianData.get(NETWORK_TIER_INDEX).toString();
								String groupKey = physicianData.get(GROUP_KEY_INDEX).toString();
								String taxonomyCode = physicianData.get(TAXONOMY_CODE_INDEX).toString();
								String specialityCode = getSpecialityCode(taxonomyCode, physicianData.get(SPECIALITY_CODE_INDEX).toString(), taxonomySpecialtyCodeMap);
								

								long recordReadStartTime = TimeShifterUtil.currentTimeMillis();
								++recordCounter;
								builder = new StringBuilder();
								builder.append("Physician record read started, Record Number :	");
								builder.append(recordCounter);
								builder.append("   at  :	");
								builder.append(recordReadStartTime);
								LOGGER.debug(builder.toString());
								errorFlag = false;
								isNetworkIdError = false;
								Map<Integer, String> errorAndWarnMap = new HashMap<Integer, String>();

								if (null!= physician) {
									String  findProvider = "";
										
									boolean isFieldLengthValid = validateFieldLength(physicianData, true, fieldSizeErrorMap);
									
									if(isFieldLengthValid)
									{
										if(physicianGroupKeyDb.containsKey(groupKey)){
											findProvider = groupKey;
										}else{
											findProvider = providerService.getProviderByGroupKey(groupKey,Provider.ProviderType.PHYSICIAN);	
										}
	
										if(physicianGroupKeyDb.containsKey(groupKey) && physicianGroupKeyDb.get(groupKey)){
											if(physician.getPhysicianAddressObj() != null){
												buildDuplicatePhysicianGroupIdMap(duplicatePhysicianGroupIdMap, groupKey, physician.getPhysicianAddressObj());											
											}
										}
										
										if((physicianGroupKeyDb.containsKey(groupKey) && !physicianGroupKeyDb.get(groupKey))||StringUtils.isBlank(findProvider)){ 
											try{
												if (null==groupKey||groupKey.isEmpty()) {
													errorAndWarnMap.put(PROVIDER_ID_ERROR, "Group Key is required");
													errorFlag = true;
												}
	
												if (null == physician.getType() || physician.getType().toString().isEmpty()) {
													errorAndWarnMap.put(PROVIDER_TYPE_ERROR, "Provider Type Indicator is required");
													errorFlag = true;
												}
												if (null == physician.getFirst_name() || physician.getFirst_name().isEmpty()) {
													errorAndWarnMap.put(FIRST_NAME_ERROR, "First Name is required");
													errorFlag = true;
												}
	
												if (null == physician.getLast_name() || physician.getLast_name().isEmpty()) {
													errorAndWarnMap.put(LAST_NAME_ERROR, "Last Name is required");
													errorFlag = true;
												}
												if (null  == physician.getPhysicianAddressObj().getAddress1() || physician.getPhysicianAddressObj().getAddress1().isEmpty()) {
													errorAndWarnMap.put(ADDRESS_1_ERROR, "Address line1 is required");
													errorFlag = true;
												}
	
												if (null == physician.getPhysicianAddressObj().getCity() || physician.getPhysicianAddressObj().getCity().isEmpty()) {
													errorAndWarnMap.put(CITY_ERROR, "City is required");
													errorFlag = true;
												}
	
												if (null == physician.getPhysicianAddressObj().getState() || physician.getPhysicianAddressObj().getState().isEmpty()) {
													errorAndWarnMap.put(STATE_ERROR, "State is required");
													errorFlag = true;
												}
	
												if (null == physician.getPhysicianAddressObj().getZip() || physician.getPhysicianAddressObj().getZip().isEmpty() || physician.getPhysicianAddressObj().getZip().length() != 5) {
													errorAndWarnMap.put(ZIP_ERROR, "Zip is required");
													errorFlag = true;
												}
												if (null == physician.getProviderObj().getDatasource() || physician.getProviderObj().getDatasource().toString().isEmpty()) {
													errorAndWarnMap.put(DATASOURCE_REF_ID_ERROR, "Data source is required");
													errorFlag = true;
												}
												if(null==networkIds||networkIds.isEmpty()){
													errorAndWarnMap.put(NETWORK_ID_ERROR, "Network ID is required");
													errorFlag = true;
													isNetworkIdError = true;
												}
												if(null==networkTiers||networkTiers.isEmpty()){
													errorAndWarnMap.put(NETWORK_TIER_ERROR, "Network Tier is not available for record, setting 01");
												}
												if(null==groupKey||groupKey.isEmpty()){
													errorAndWarnMap.put(GROUP_KEY_ERROR, "Group Key is required");
													errorFlag = true;
												}
	
												if(errorFlag){
													physicianGroupKeyDb.put(groupKey, INVALID_RECORD);
													builder = new StringBuilder();
													builder.append("Group Key ");
													builder.append(groupKey);
													builder.append(", Row No. ");
													builder.append(recordCounter);
													builder.append("  :Error while storing doctor / dentist.");
													errorAndWarnMap.put(ERROR_ON_RECORD, builder.toString());
												}
	
												if(!isNetworkIdError){
													EnclarityNetworkTierAndId enclarityNetworkTierAndId = new EnclarityNetworkTierAndId();
													Map<String,String> networkTierIdAndHiosMap= enclarityNetworkTierAndId.getNetworkIdTierAndHiosInfo(networkIds, networkTiers);
													Set<String> networkIdTierHiosKeys = networkTierIdAndHiosMap.keySet();
													boolean isHiosFound = false;
													for(String tempNetworkIdTierHiosKeys : networkIdTierHiosKeys){
														if(tempNetworkIdTierHiosKeys.contains(HIOS_STRING)){
															isHiosFound = true;
															hiosIdSetPhysician.add(networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys));
														}
														if(errorFlag){
															if(tempNetworkIdTierHiosKeys.contains(HIOS_STRING)){
																if(networkIdStatusMapPhysician.containsKey(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys))){
																	try{
																		Integer rejectedCount = networkIdStatusMapPhysician.get(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys))+1;
																		networkIdStatusMapPhysician.put(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys), rejectedCount);
																	}catch(Exception e){
																		LOGGER.error("Error in HIOS id",e);
																	}
																}else{
																	networkIdStatusMapPhysician.put(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys), 1);
																}
															}
														}
													}
													if(!isHiosFound){
														if(networkIdStatusMapPhysician.containsKey(HIOS_NOTPROVIDED)){
															Integer rejectedCount = networkIdStatusMapPhysician.get(HIOS_NOTPROVIDED)+1;
															networkIdStatusMapPhysician.put(HIOS_NOTPROVIDED, rejectedCount);
														}else{
															networkIdStatusMapPhysician.put(HIOS_NOTPROVIDED, 1);
														}
													}
	
													if(!errorFlag){
	
														StringBuilder physicianName = new StringBuilder();
	
														if (null!=physician.getSuffix() && physician.getSuffix().isEmpty()) {
															physicianName.append(physician.getSuffix());
															physicianName.append(" ");
														}
	
														physicianName.append(physician.getFirst_name());
	
														physicianName.append(" ");
	
														if (null!= physician.getMiddle_name() && physician.getMiddle_name().isEmpty()) {
															physicianName.append(physician.getMiddle_name());
															physicianName.append(" ");
														}
	
														Provider provider = physician.getProviderObj();
														provider.setName(physicianName.toString());
														provider.setGroupKey(groupKey);
														provider.setType(ProviderType.PHYSICIAN);
														physicianName.append(physician.getLast_name());
	
														List<String> specialityCodeList = new ArrayList<String>();
														if(StringUtils.isNotEmpty(specialityCode)){
															if(specialityCode.contains("|")){
																String[] specialityCodeArray = specialityCode.split("\\|");
																for(int specialityCodeCount = 0;specialityCodeCount<specialityCodeArray.length;specialityCodeCount++){
																	specialityCodeList.add(specialityCodeArray[specialityCodeCount]);
																}
															}else{
																specialityCodeList.add(specialityCode);
															}
														}
														Set<ProviderNetwork> setProviderNetwork = new HashSet<ProviderNetwork>();
														String networkId = "";
														String hiosId = null;
														for(String networkIdTierHiosKey:networkIdTierHiosKeys){
															if(!networkIdTierHiosKey.contains(HIOS_STRING)){
																networkId = networkIdTierHiosKey;
																try{
																	if(networkId.length()>6){
																		if(StringUtils.isNumeric(networkId.substring(0, 5))){
																			hiosId = networkId.substring(0, 5);
																			networkId= networkId.substring(5);
																		}
																	}
																	List<Network> networkList = getNetworkForProvider(networkId,hiosId);
																	if(networkList==null||networkList.size()==0){
																		if(networkIdStatusMapPhysician.containsKey(NOT_ASSOCIATED+networkId)){
																			Integer notAssociatedCount = networkIdStatusMapPhysician.get(NOT_ASSOCIATED+networkId)+1;
																			networkIdStatusMapPhysician.put(NOT_ASSOCIATED+networkId, notAssociatedCount);
																		}else{
																			networkIdStatusMapPhysician.put(NOT_ASSOCIATED+networkId, 1);
																		}
																	}else{
																		for(Network network : networkList){
																			if(network !=null){
																				if(networkIdStatusMapPhysician.containsKey(networkId)){
																					Integer addedCount = networkIdStatusMapPhysician.get(networkId)+1;
																					networkIdStatusMapPhysician.put(networkId, addedCount);
																				}else{
																					networkIdStatusMapPhysician.put(networkId, 1);
																				}
																				ProviderNetwork providerNetwork = new ProviderNetwork();
																				providerNetwork.setNetwork(network);
																				providerNetwork.setProvider(provider);
																				providerNetwork.setNetworkTier(StringUtils.isBlank(networkTierIdAndHiosMap.get(networkIdTierHiosKey))?"01":networkTierIdAndHiosMap.get(networkIdTierHiosKey));
																				setProviderNetwork.add(providerNetwork);
																			}else{
																				if(networkIdStatusMapPhysician.containsKey(NOT_ASSOCIATED+networkId)){
																					Integer notAssociatedCount = networkIdStatusMapPhysician.get(NOT_ASSOCIATED+networkId);
																					networkIdStatusMapPhysician.put(NOT_ASSOCIATED+networkId, notAssociatedCount);
																				}else{
																					networkIdStatusMapPhysician.put(NOT_ASSOCIATED+networkId, 1);
																				}
																			}
																		}
																	}
																}catch(Exception e){
																	LOGGER.error("ERROR While fetchin netwokTier");
																	errorAndWarnMap.put(NETWORK_ID_ERROR, "Network Tier is incorrect");
																}
															}
														}
	
	
														if(setProviderNetwork!=null && setProviderNetwork.size()>0){
															provider.setProviderNetwork(setProviderNetwork);
	
															physician.setProvider(provider);
	
															Set<PhysicianSpecialityRelation> physicianSpecialityRelationSet = new HashSet<PhysicianSpecialityRelation>();
															for (String speciality :specialityCodeList){
																PhysicianSpecialityRelation physicianSpecialityRelation = new PhysicianSpecialityRelation();
																if(StringUtils.isNotBlank(speciality)){
																	speciality = speciality.trim();
																	Specialty specialty = specialtyMap.get(speciality);
																	if(specialty!=null){
																		physicianSpecialityRelation.setPhysician(physician);
																		physicianSpecialityRelation.setSpecialty(specialty);
																		physicianSpecialityRelationSet.add(physicianSpecialityRelation);
																	}
																}
															}
	
	
															physician.setPhysicianSpecialRelation(physicianSpecialityRelationSet);
	
															Set<PhysicianAddress> physicianAddresseSet = new HashSet<PhysicianAddress>();
															PhysicianAddress physicianAddress = physician.getPhysicianAddressObj();	
															physicianAddresseSet.add(physicianAddress);
															physicianAddress.setPhysician(physician);
															physician.setPhysicianAddress(physicianAddresseSet);
															try{
																groupKeyBuilder.append(groupKey);
																groupKeyBuilder.append(", ");
																entityManager.persist(physician);
																isNewPhysician = true;
																insertCount++;
																physicianGroupKeyDb.put(groupKey, VALID_RECORD);
																if(insertCount % maxBatchSize == 0){
																	startTime = TimeShifterUtil.currentTimeMillis();
																	insertCount = 0;
																	entityManager.flush();	
																	entityManager.getTransaction().commit();
																	logGroupKeys(groupKeyBuilder, true, physicianStatusLogFile);
																	groupKeyBuilder = new StringBuilder();																			
																	endTime = TimeShifterUtil.currentTimeMillis();
																	
																	LOGGER.debug("***************************************************************");
																	LOGGER.debug("Time taken to flush records is	: "+ (endTime - startTime)/1000);
																	LOGGER.debug("***************************************************************");
																}
															}catch(Exception e){
																logGroupKeys(groupKeyBuilder, false, physicianStatusLogFile);
																groupKeyBuilder = new StringBuilder();
																try{
																	entityManager.getTransaction().rollback();
																}catch(Exception e1){
																	LOGGER.error("Error while rollback transaction",e1);
																}
																LOGGER.error("ERROR Occured while storing physician data", e);
															}
	
															counter++;
															builder = new StringBuilder();
															builder.append("Group Key ");
															builder.append(groupKey);
															builder.append(", Row No. ");
															builder.append(recordCounter);
															builder.append("  :Doctor/Dentist data successfully added in dataBase.  ");														
															errorAndWarnMap.put(ERROR_ON_RECORD, builder.toString());
															errorFlag= true;
														}else{
															physicianGroupKeyDb.put(groupKey, INVALID_RECORD);
															builder = new StringBuilder();
															builder.append("Group Key ");
															builder.append(groupKey);
															builder.append(", Row No. ");
															builder.append(recordCounter);
															builder.append("  :Doctor/Dentist is not associated with network.  ");	
															errorAndWarnMap.put(ERROR_ON_RECORD, builder.toString());
															errorFlag= true;
														}
														if(isNewPhysician){
															for(String tempNetworkIdTierHiosKeys : networkIdTierHiosKeys){
																if(tempNetworkIdTierHiosKeys.contains(HIOS_STRING)){
																	if(networkIdStatusMapPhysician.containsKey(HIOS_ADDED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys))){
																		Integer addedCount = networkIdStatusMapPhysician.get(HIOS_ADDED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys))+1;
																		networkIdStatusMapPhysician.put(HIOS_ADDED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys), addedCount);
																	}else{
																		networkIdStatusMapPhysician.put(HIOS_ADDED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys), 1);
																	}
																}
															}
														}else{
															for(String tempNetworkIdTierHiosKeys : networkIdTierHiosKeys){
																if(tempNetworkIdTierHiosKeys.contains(HIOS_STRING)){
																	if(networkIdStatusMapPhysician.containsKey(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys))){
																		Integer rejectedCount = networkIdStatusMapPhysician.get(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys))+1;
																		networkIdStatusMapPhysician.put(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys), rejectedCount);
																	}else{
																		networkIdStatusMapPhysician.put(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys), 1);
																	}
																}
															}
														}
													}else{
														String networkId = DATA_MISSING;
														if(networkIdStatusMapPhysician.containsKey(networkId)){
															Integer addedCount = networkIdStatusMapPhysician.get(networkId)+1;
															networkIdStatusMapPhysician.put(networkId, addedCount);
														}else{
															networkIdStatusMapPhysician.put(networkId, 1);
														}
													}
	
												}else{
													String networkId = NO_NETWORK;
													if(networkIdStatusMapPhysician.containsKey(networkId)){
														Integer addedCount = networkIdStatusMapPhysician.get(networkId)+1;
														networkIdStatusMapPhysician.put(networkId, addedCount);
													}else{
														networkIdStatusMapPhysician.put(networkId, 1);
													}
												}
											}catch(Exception e){
												LOGGER.error("ERROR Occured while storing physician data",e);
											}
										}else{
											physicianGroupKeyDb.put(groupKey, VALID_RECORD);
											if(networkIdStatusMapPhysician.containsKey(ALREADY_ADDED)){
												int alreadyAdded = networkIdStatusMapPhysician.get(ALREADY_ADDED)+1;
												networkIdStatusMapPhysician.put(ALREADY_ADDED, alreadyAdded);	
											}else{
												networkIdStatusMapPhysician.put(ALREADY_ADDED, 1);
											}
											
											builder = new StringBuilder();
											builder.append("Group Key ");
											builder.append(groupKey);
											builder.append(", Row No. ");
											builder.append(recordCounter);
											builder.append("  :Doctor/Dentist Already Available in DataBase.  ");	
											errorAndWarnMap.put(ERROR_ON_RECORD, builder.toString());
											errorFlag = true;
										}
									}
								}else{
									errorAndWarnMap.put(PHYSICIAN_OBJECT_ERROR, "Physician should not be null");
								}

								if(errorFlag){
									errorAndWarningListIndividual.add(errorAndWarnMap);
								}
							if(recordCounter % maxFileSize == 0){
									if(!errorAndWarningListIndividual.isEmpty()) {
										writeEnclarityStatusLog(errorAndWarningListIndividual, physicianStatusLogFile);										
										errorAndWarningListIndividual = new ArrayList<Map<Integer,String>>(maxFileSize);
										
									}
								}
							}

							if(!errorAndWarningListIndividual.isEmpty()) {
								LOGGER.debug("Writing enclarity physician file status log started");
								writeEnclarityStatusLog(errorAndWarningListIndividual, physicianStatusLogFile);
								LOGGER.debug("Writing enclarity physician file status log completed");
								errorAndWarningListIndividual = new ArrayList<Map<Integer,String>>(maxFileSize);
							}
							
							if(!fieldSizeErrorMap.isEmpty()){
								LOGGER.debug("Ready to print fields that violated size");
								writeFieldSizeViolationRecords(fieldSizeErrorMap, physicianStatusLogFile);
							}

							try{
								if(entityManager.getTransaction().isActive()) {									
									entityManager.flush();	
									entityManager.getTransaction().commit();
									logGroupKeys(groupKeyBuilder, true, physicianStatusLogFile);
								}
							}catch(Exception e){
								logGroupKeys(groupKeyBuilder, false, physicianStatusLogFile);
								LOGGER.error("Error while commiting transaction :",e);
							}finally{
								if(entityManager.isOpen()){
									entityManager.close();
								}
							}
							
							writeEnclarityEndLog(true, networkIdStatusMapPhysician, hiosIdSetPhysician, listPhysicians.size(), physicianStatusLogFile, counter, type);
							
							if(!duplicatePhysicianGroupIdMap.isEmpty()){
								LOGGER.debug("Writing enclarity individual file status log started");
								insertDuplicatePhysicianAddresses(duplicatePhysicianGroupIdMap);
								LOGGER.debug("Writing enclarity individual file status log completed");
							}
							

						}else{
							File physicianStatusLogFile = writeEnclarityLogFile(uploadedFileName, type, false);
							Map<Integer, String> errorAndWarnMapIndividual = new HashMap<Integer, String>();
							
							builder = new StringBuilder();
							builder.append("Row No. ");
							builder.append(recordCounter);
							builder.append("  :Individual Enclarity file is empty or contain incorrect data.");
							
							errorAndWarnMapIndividual.put(ERROR_ON_RECORD,builder.toString());
							errorAndWarningListIndividual.add(errorAndWarnMapIndividual);
							writeEnclarityStatusLog(errorAndWarningListIndividual, physicianStatusLogFile);
							writeEnclarityEndLog(false, null, null, 0, physicianStatusLogFile, counter, type);

						}
						LOGGER.debug("Individual / Physician file upload completed");
					}

					if (type!=null && !type.isEmpty()&&type.equalsIgnoreCase(FACILITIES)) {		
						
						startTime = TimeShifterUtil.currentTimeMillis();
						groupKeyBuilder = new StringBuilder();
						List<List> listFacilities = providerSearchNetworkService.getFacilities(uploadedFileName);
						
						endTime = TimeShifterUtil.currentTimeMillis();
						
						LOGGER.info("***************************************************************");
						LOGGER.info("Time taken to read the file is	: "+ (endTime - startTime)/1000);
						LOGGER.info("***************************************************************");																								
						
						int insertCount = 0;
						int counter = 0;
						boolean errorFlag;
						int recordCounter = 1;
						
						Map<String,Boolean> facilityGroupKeyDb  = new HashMap<String,Boolean>();
						Set<String> hiosIdSetFacility = new HashSet<String>();
						if(listFacilities.size()>0){
							EntityManager entityManager = entityManagerFactory.createEntityManager();
							File facilityStatusLogFile = writeEnclarityLogFile(uploadedFileName, type, true);
							boolean isNewFacility = false;
							Map<String, String> fieldSizeErrorMap = new HashMap<String, String>();
							for (List facilityData : listFacilities){
								Facility facility = (Facility) facilityData.get(FACILITY_INDEX);
								String networkIds = facilityData.get(NETWORK_ID_INDEX).toString();
								String networkTiers = facilityData.get(NETWORK_TIER_INDEX).toString();
								String groupKey = facilityData.get(GROUP_KEY_INDEX).toString();
								String specialityCode = facilityData.get(SPECIALITY_CODE_INDEX).toString();
								errorFlag = false;
								boolean isNetworkIdError = false;
								++recordCounter;
								long recordReadStartTime = TimeShifterUtil.currentTimeMillis();
								
								builder = new StringBuilder();
								builder.append("Facility record read started, Record Number :	");
								builder.append(recordCounter);
								builder.append("   at  :	");
								builder.append(recordReadStartTime);
								
								LOGGER.debug(builder.toString());
								Map<Integer, String> errorAndWarnMap = new HashMap<Integer, String>();
								
								if(insertCount==0 && !entityManager.getTransaction().isActive()){
									entityManager.getTransaction().begin();
								}
								if (facility != null) {
									try{
										String  findProvider = "";

										boolean isFieldLengthValid = validateFieldLength(facilityData, false, fieldSizeErrorMap);
										
										if(isFieldLengthValid)
										{
											if(facilityGroupKeyDb.containsKey(groupKey)){
												findProvider = groupKey;
											}else{
												findProvider = providerService.getProviderByGroupKey(groupKey,Provider.ProviderType.FACILITY);	
											}
											
											if(facilityGroupKeyDb.containsKey(groupKey) && facilityGroupKeyDb.get(groupKey)){
												if(facility.getFacilityAddressObj() != null){
													buildDuplicateFacilityGroupIdMap(duplicateFacilityGroupIdMap, groupKey, facility.getFacilityAddressObj());											
												}
											}
											if((facilityGroupKeyDb.containsKey(groupKey) && !facilityGroupKeyDb.get(groupKey))||StringUtils.isBlank(findProvider)){
												if (null == groupKey || groupKey.isEmpty()) {
													errorAndWarnMap.put(PROVIDER_ID_ERROR, "Group Key is required");
													errorFlag = true;
												}
	
												if (null== facility.getFacility_type_indicator() || facility.getFacility_type_indicator().isEmpty()) {
													errorAndWarnMap.put(FACILITY_TYPE_ERROR, "Facility Type Indicator is required");
													errorFlag = true;
												}
	
												if (null== networkIds || networkIds.isEmpty()) {
													errorAndWarnMap.put(NETWORK_ID_ERROR, "Network Id is required");
													errorFlag = true;
													isNetworkIdError = true;
												}
	
												if (null== networkTiers || networkTiers.isEmpty()) {
													errorAndWarnMap.put(NETWORK_TIER_ERROR, "Network Tier is not available for record, setting 01");
												}
	
												if (null== facility.getProvider().getName() || facility.getProvider().getName().isEmpty()) {
													errorAndWarnMap.put(FACILITY_NAME_ERROR, "Title is required");
													errorFlag = true;
												}
	
												if (null== facility.getFacilityAddressObj().getAddress1() || facility.getFacilityAddressObj().getAddress1().isEmpty()) {
													errorAndWarnMap.put(ADDRESS_1_ERROR, "Address line1 is required");
													errorFlag = true;
												}
	
												if (null== facility.getFacilityAddressObj().getCity() || facility.getFacilityAddressObj().getCity().isEmpty()) {
													errorAndWarnMap.put(CITY_ERROR, "City is required");
													errorFlag = true;
												}
	
												if (null== facility.getFacilityAddressObj().getState() || facility.getFacilityAddressObj().getState().isEmpty()) {
													errorAndWarnMap.put(STATE_ERROR, "State is required");
													errorFlag = true;
												}
	
												if (null== facility.getFacilityAddressObj().getZip() || facility.getFacilityAddressObj().getZip().isEmpty() || facility.getFacilityAddressObj().getZip().length() != 5) {
													errorAndWarnMap.put(ZIP_ERROR, "Zip is required");
													errorFlag = true;
												}
												if(errorFlag){
													facilityGroupKeyDb.put(groupKey, INVALID_RECORD);
													builder = new StringBuilder();
													builder.append("Group Key ");
													builder.append(groupKey);
													builder.append(", Row No. ");
													builder.append(recordCounter);
													builder.append("  :Error while storing hospital / facility. ");
													
													errorAndWarnMap.put(ERROR_ON_RECORD,builder.toString());
												}
												if(!isNetworkIdError){
													EnclarityNetworkTierAndId enclarityNetworkTierAndId = new EnclarityNetworkTierAndId();
													Map<String,String> networkTierIdAndHiosMap= enclarityNetworkTierAndId.getNetworkIdTierAndHiosInfo(networkIds, networkTiers);
													Set<String> networkIdTierHiosKeys = networkTierIdAndHiosMap.keySet();
													boolean isHiosFound = false;
													for(String tempNetworkIdTierHiosKeys : networkIdTierHiosKeys){
														if(tempNetworkIdTierHiosKeys.contains(HIOS_STRING)){
															isHiosFound = true;
															hiosIdSetFacility.add(networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys));
														}
														if(errorFlag){
															if(tempNetworkIdTierHiosKeys.contains(HIOS_STRING)){
																if(networkIdStatusMapFacility.containsKey(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys))){
																	try{Integer rejectedCount = networkIdStatusMapFacility.get(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys))+1;
																	networkIdStatusMapFacility.put(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys), rejectedCount);
																	}catch(Exception e){
																		LOGGER.error("Error message",e);
																	}
																}else{
																	networkIdStatusMapFacility.put(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys), 1);
																}
															}
														}
													}
													if(!isHiosFound){
														if(networkIdStatusMapFacility.containsKey(HIOS_NOTPROVIDED)){
															Integer rejectedCount = networkIdStatusMapFacility.get(HIOS_NOTPROVIDED)+1;
															networkIdStatusMapFacility.put(HIOS_NOTPROVIDED, rejectedCount);
														}else{
															networkIdStatusMapFacility.put(HIOS_NOTPROVIDED, 1);
														}
													}
													Facility newFacility = new Facility();
													if(!errorFlag) 
													{
	
														Provider provider = facility.getProvider();					
														provider.setType(ProviderType.FACILITY);
														provider.setGroupKey(groupKey);
	
	
	
														List<String> networkTierList = new ArrayList<String>();
														if(networkTiers.contains("|")){
															String[] tempNetworkTierArray = networkTiers.split("\\|");
															for(int networkTierCount=0;networkTierCount<tempNetworkTierArray.length;networkTierCount++){
																networkTierList.add(tempNetworkTierArray[networkTierCount]);
															}
														}else{
															networkTierList.add(networkTiers);
														}
														
														List<String> specialityCodeList = new ArrayList<String>();
														if(StringUtils.isNotEmpty(specialityCode)){
															if(specialityCode.contains("|")){
																String[] specialityCodeArray = specialityCode.split("\\|");
																for(int specialityCodeCount = 0;specialityCodeCount<specialityCodeArray.length;specialityCodeCount++){
																	specialityCodeList.add(specialityCodeArray[specialityCodeCount]);
																}
															}else{
																specialityCodeList.add(specialityCode);
															}
														}
														Set<ProviderNetwork> setProviderNetwork = new HashSet<ProviderNetwork>();
														String networkId = "";
														String hiosId = null;
														for(String networkIdTierHiosKey:networkIdTierHiosKeys){
															if(!networkIdTierHiosKey.contains(HIOS_STRING)){												
																networkId = networkIdTierHiosKey;
																try{
																	if(networkId.length()>6){
																		if(StringUtils.isNumeric(networkId.substring(0, 5))){
																			hiosId = networkId.substring(0, 5);
																			networkId= networkId.substring(5);
																		}
																	}
																	List<Network> networkList  = getNetworkForProvider(networkId,hiosId);
																	if(networkList==null||networkList.size()==0){
																		if(networkIdStatusMapPhysician.containsKey(NOT_ASSOCIATED+networkId)){
																			Integer notAssociatedCount = networkIdStatusMapPhysician.get(NOT_ASSOCIATED+networkId)+1;
																			networkIdStatusMapPhysician.put(NOT_ASSOCIATED+networkId, notAssociatedCount);
																		}else{
																			networkIdStatusMapPhysician.put(NOT_ASSOCIATED+networkId, 1);
																		}
																	}else{
																		for(Network network : networkList){
																			ProviderNetwork providerNetwork = new ProviderNetwork();
																			if(network !=null){
																				if(networkIdStatusMapFacility.containsKey(networkId)){
																					Integer addedCount = networkIdStatusMapFacility.get(networkId)+1;
																					networkIdStatusMapFacility.put(networkId, addedCount);
																				}else{
																					networkIdStatusMapFacility.put(networkId, 1);
																				}
																				providerNetwork.setNetwork(network);
																				providerNetwork.setProvider(provider);
																				providerNetwork.setNetworkTier(StringUtils.isBlank(networkTierIdAndHiosMap.get(networkIdTierHiosKey))?"01":networkTierIdAndHiosMap.get(networkIdTierHiosKey));
																				setProviderNetwork.add(providerNetwork);
																			}else{
																				if(networkIdStatusMapFacility.containsKey(NOT_ASSOCIATED+networkId)){
																					Integer notAssociatedCount = networkIdStatusMapFacility.get(NOT_ASSOCIATED+networkId);
																					networkIdStatusMapFacility.put(NOT_ASSOCIATED+networkId, notAssociatedCount);
																				}else{
																					networkIdStatusMapFacility.put(NOT_ASSOCIATED+networkId, 1);
																				}
																			}
																		}
																	}
	
																}catch(Exception e){
																	LOGGER.error("ERROR While fetchin netwokTier");
																	errorAndWarnMap.put(NETWORK_ID_ERROR, "Network Tier is incorrect");
																}
															}
														}
														if(setProviderNetwork!=null && setProviderNetwork.size()>0){
	
															provider.setProviderNetwork(setProviderNetwork);
	
															Set<FacilitySpecialityRelation> facilitySpecialRelationSet = new HashSet<FacilitySpecialityRelation>();
															FacilitySpecialityRelation facilitySpecialityRelation = new FacilitySpecialityRelation();
															for (String speciality :specialityCodeList){
																if(StringUtils.isNotBlank(speciality)){
																	speciality = speciality.trim();
																	Specialty specialty = specialtyMap.get(speciality);
																	if(specialty!=null){
																		facilitySpecialityRelation.setFacility(facility);
																		facilitySpecialityRelation.setSpeciality(specialty);
																		facilitySpecialRelationSet.add(facilitySpecialityRelation);
																	}
																}
															}
															facility.setFacilitySpecialRelation(facilitySpecialRelationSet);
															facility.setProvider(provider);
															List<FacilityAddress> facilityAddressList = new ArrayList<FacilityAddress>();
															FacilityAddress facilityAddress = facility.getFacilityAddressObj();							
															facilityAddress.setFacility(facility);
															facilityAddressList.add(facilityAddress);
															facility.setFacilityAddress(facilityAddressList);
								
															try{
																
																groupKeyBuilder.append(groupKey);
																groupKeyBuilder.append(", ");
																entityManager.persist(facility);																
																isNewFacility = true;
																insertCount++;
																facilityGroupKeyDb.put(groupKey, VALID_RECORD);
																if(insertCount % maxBatchSize == 0){
																	startTime = TimeShifterUtil.currentTimeMillis();
																	LOGGER.debug("Writing set of facilities record started at  :	"+startTime);
																	insertCount = 0;
																	entityManager.flush();	
																	entityManager.getTransaction().commit();	
																	logGroupKeys(groupKeyBuilder, true, facilityStatusLogFile);
																	groupKeyBuilder = new StringBuilder();																	
																	endTime = TimeShifterUtil.currentTimeMillis();
																	LOGGER.debug("***************************************************************");
																	LOGGER.debug("Time taken to insert records to db is	: "+ (endTime - startTime)/1000);
																	LOGGER.debug("***************************************************************");
																}
																
																builder = new StringBuilder();
																builder.append("Group Key ");
																builder.append(groupKey);
																builder.append(", Row No. ");
																builder.append(recordCounter);
																builder.append("  :Facility data successfully added in dataBase.  ");
																
																errorAndWarnMap.put(ERROR_ON_RECORD,builder.toString());
																errorFlag = true;
															}catch(Exception e){
																logGroupKeys(groupKeyBuilder, false, facilityStatusLogFile);
																groupKeyBuilder = new StringBuilder();
																try{
																	entityManager.getTransaction().rollback();
																}catch(Exception e1){
																	LOGGER.error("Error while rollback transaction",e1);
																}
																LOGGER.error("ERROR Occured while storing facility data",e);
															}
															counter++;
														}else{
															facilityGroupKeyDb.put(groupKey, INVALID_RECORD);
															
															builder = new StringBuilder();
															builder.append("Group Key ");
															builder.append(groupKey);
															builder.append(", Row No. ");
															builder.append(recordCounter);
															builder.append("  :Error while storing hospital / facility.  ");
															
															errorAndWarnMap.put(ERROR_ON_RECORD, builder.toString());
															errorAndWarnMap.put(NETWORK_ID_ERROR,"Facility is not associated with network.  ");
															errorFlag= true;
														}
														if(isNewFacility){
															for(String tempNetworkIdTierHiosKeys : networkIdTierHiosKeys){
																if(tempNetworkIdTierHiosKeys.contains(HIOS_STRING)){
																	if(networkIdStatusMapFacility.containsKey(HIOS_ADDED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys))){
																		Integer addedCount = networkIdStatusMapFacility.get(HIOS_ADDED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys))+1;
																		networkIdStatusMapFacility.put(HIOS_ADDED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys), addedCount);
																	}else{
																		networkIdStatusMapFacility.put(HIOS_ADDED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys), 1);
																	}
																}
															}
														}else{
															for(String tempNetworkIdTierHiosKeys : networkIdTierHiosKeys){
																if(tempNetworkIdTierHiosKeys.contains(HIOS_STRING)){
																	if(networkIdStatusMapFacility.containsKey(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys))){
																		Integer rejectedCount = networkIdStatusMapFacility.get(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys))+1;
																		networkIdStatusMapFacility.put(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys), rejectedCount);
																	}else{
																		networkIdStatusMapFacility.put(HIOS_REJECTED+networkTierIdAndHiosMap.get(tempNetworkIdTierHiosKeys), 1);
																	}
																}
															}
														}
													}
												}else{
													String networkId = DATA_MISSING;
													if(networkIdStatusMapFacility.containsKey(networkId)){
														Integer addedCount = networkIdStatusMapFacility.get(networkId)+1;
														networkIdStatusMapFacility.put(networkId, addedCount);
													}else{
														networkIdStatusMapFacility.put(networkId, 1);
													}
												}
	
											}else{
												facilityGroupKeyDb.put(groupKey, VALID_RECORD);
												if(networkIdStatusMapFacility.containsKey(ALREADY_ADDED)){
													int alreadyAdded = networkIdStatusMapFacility.get(ALREADY_ADDED)+1;
													networkIdStatusMapFacility.put(ALREADY_ADDED, alreadyAdded);	
												}else{
													networkIdStatusMapFacility.put(ALREADY_ADDED, 1);
												}
												
												builder = new StringBuilder();
												builder.append("Group Key ");
												builder.append(groupKey);
												builder.append(", Row No. ");
												builder.append(recordCounter);
												builder.append("  :Facility data Already Available in DataBase.  ");
												errorAndWarnMap.put(ERROR_ON_RECORD,builder.toString());
												errorFlag = true;
											}
										}
									}catch(Exception e){
										LOGGER.error("ERROR Found while storing facility data", e);
									}
								}else{
									errorAndWarnMap.put(FACILITY_OBJECT_ERROR, "Facility should not be null");
									errorFlag = true;
								}

								if(errorFlag){
									errorAndWarningListFacility.add(errorAndWarnMap);
								}
							if(recordCounter % maxFileSize == 0){
									if(!errorAndWarningListIndividual.isEmpty()) {
										writeEnclarityStatusLog(errorAndWarningListFacility, facilityStatusLogFile);										
										errorAndWarningListFacility = new ArrayList<Map<Integer,String>>(maxFileSize);
									}
								}
							}
							if(!errorAndWarningListFacility.isEmpty()) {
								writeEnclarityStatusLog(errorAndWarningListFacility, facilityStatusLogFile);								
								errorAndWarningListFacility = new ArrayList<Map<Integer,String>>(maxFileSize);
							}
							
							if(!fieldSizeErrorMap.isEmpty()){
								LOGGER.debug("Ready to print fields that violated size");
								writeFieldSizeViolationRecords(fieldSizeErrorMap, facilityStatusLogFile);
							}							

							try{
								if(entityManager.getTransaction().isActive()) {
									entityManager.flush();	
									entityManager.getTransaction().commit();
									
									logGroupKeys(groupKeyBuilder, true, facilityStatusLogFile);
								}
							}catch(Exception e){
								logGroupKeys(groupKeyBuilder, false, facilityStatusLogFile);
								LOGGER.error("Error while commiting transaction :",e);
							}finally{
								if(entityManager.isOpen()){
									entityManager.close();
								}
							}
							
							writeEnclarityEndLog(true, networkIdStatusMapFacility, hiosIdSetFacility, listFacilities.size(), facilityStatusLogFile, counter, type);
							
							
							LOGGER.debug("Duplicate address for facility added, Now going for writing status log");
							if(!duplicateFacilityGroupIdMap.isEmpty()){
								LOGGER.debug("Writing enclarity facility file status log started");
								insertDuplicateFacilityAddresses(duplicateFacilityGroupIdMap);
								LOGGER.debug("Writing enclarity facility file status log completed");
							}
							
						}else{
							File facilityStatusLogFile = writeEnclarityLogFile(uploadedFileName, type, false);
							Map<Integer, String> errorAndWarnMap = new HashMap<Integer, String>();
							
							builder = new StringBuilder();
							builder.append("Row No. ");
							builder.append(recordCounter);
							builder.append("  :Facility Enclarity file is empty or contain incorrect data.");
							
							errorAndWarnMap.put(ERROR_ON_RECORD, builder.toString());
							errorAndWarningListFacility.add(errorAndWarnMap);
							writeEnclarityStatusLog(errorAndWarningListFacility, facilityStatusLogFile);
							writeEnclarityEndLog(false, null, null, 0, facilityStatusLogFile, 0, FACILITIES);
						}
						LOGGER.debug("Facility file upload completed");
					}
				}
				catch (Exception e){
					LOGGER.error("Could not add Provider Network.", e); /*Need only message no need of full stacktrace*/
				}
			}
			if(isIndividualFileUnavailable){
				File physicianStatusLogFile = writeEnclarityLogFile(uploadedFileName, PHYSICIANS, false);
				Map<Integer, String> errorAndWarnMap = new HashMap<Integer, String>();
				errorAndWarnMap.put(ERROR_ON_RECORD, "Enclarity File for Individual Not found for today's date");
				errorAndWarningListIndividual.add(errorAndWarnMap);
				writeEnclarityStatusLog(errorAndWarningListIndividual, physicianStatusLogFile);
				writeEnclarityEndLog(false, null, null, 0, physicianStatusLogFile, 0, PHYSICIANS);
			}
			if(isFacilityFileUnavailable){
				File facilityStatusLogFile = writeEnclarityLogFile(uploadedFileName, FACILITIES, false);
				Map<Integer, String> errorAndWarnMap = new HashMap<Integer, String>();
				errorAndWarnMap.put(ERROR_ON_RECORD, "Enclarity File for Facility Not found for today's date");
				errorAndWarningListFacility.add(errorAndWarnMap);
				writeEnclarityStatusLog(errorAndWarningListFacility, facilityStatusLogFile);
				writeEnclarityEndLog(false, null, null, 0, facilityStatusLogFile, 0, FACILITIES);
			}
		}catch(Exception e){
			LOGGER.error("ERROR : ",e);
		}
		LOGGER.debug("***************************************************************");
		LOGGER.debug("Enclarity data processing completed, for both physician and facility file.");
		LOGGER.debug("***************************************************************");
		return "success";
	}
	

	private void logGroupKeys(StringBuilder groupKeyBuilder, boolean isInsertSuccessful, File file) {
		
		BufferedWriter writer = null;				
		try {
			FileWriter fileWriter  = new FileWriter(file,true);
			writer = new BufferedWriter(fileWriter);
			
			writer.newLine();
			writer.append("=======================================================================");
			writer.newLine();
			
			if(isInsertSuccessful){
				writer.append("Following group keys inserted in a batch : ");
			}
			else
			{
				writer.append("Following group keys failed in a batch : ");
			}
			
			writer.newLine();
			writer.newLine();			
			writer.append(groupKeyBuilder.toString());
			writer.newLine();
			writer.append("=======================================================================");

		}
		catch(Exception exception){
			LOGGER.error("Error occured while logging group keys : ", exception);
		}
		finally
		{
			try {
				writer.flush();
				writer.close();				
			} catch (IOException e) {
				LOGGER.error("Error while closing BufferedWriter");
			}			
		}		
	}

	private void writeFieldSizeViolationRecords(Map<String,String> fieldSizeViolationMap, File file) {
		
		BufferedWriter writer = null;				
		try {
			FileWriter FileWriter  = new FileWriter(file,true);
			writer = new BufferedWriter(FileWriter);
			
			writer.newLine();
			writer.append("=======================================================================");
			writer.newLine();
			writer.append("Following group keys were skipped due to invalid length");
			writer.newLine();
			writer.newLine();
			
			for (Map.Entry<String,String> entry : fieldSizeViolationMap.entrySet()) {
			    String groupKey = entry.getKey();
			    String errorMessage = entry.getValue();
			    
			    writer.append(groupKey + " :::::: " );
			    writer.newLine();
			    writer.append(errorMessage);
			    writer.newLine();
			    
			}
			writer.newLine();
			writer.append("=======================================================================");

		}
		catch(Exception exception){
			LOGGER.error("Error occured while print field size violation messages : ", exception);
		}
		finally
		{
			try {
				writer.flush();
				writer.close();				
			} catch (IOException e) {
				LOGGER.error("Error while closing BufferedWriter");
			}			
		}
	}
	
	private String getSpecialityCode(String taxonomyCode, String specialityCode, Map<String,String> taxonomySpecialtyCodeMap){
		if(StringUtils.isNotBlank(taxonomyCode)){
			specialityCode = "";
			String tempSpcialityCode = specialityCode;
			if(taxonomyCode.contains("|")){
				String[] taxonomyCodeArray = 	taxonomyCode.split("\\|");
				for(String taxonomyCodeValue : taxonomyCodeArray){
					if(StringUtils.isEmpty(specialityCode)){
						specialityCode= taxonomySpecialtyCodeMap.get(taxonomyCodeValue);
					}else{
						specialityCode = specialityCode+"|"+taxonomySpecialtyCodeMap.get(taxonomyCodeValue);
					}
				}
			}else{
				specialityCode= taxonomySpecialtyCodeMap.get(taxonomyCode);
			}
			
			if(StringUtils.isBlank(specialityCode)){
				specialityCode = tempSpcialityCode;
			}
		}
		
		return specialityCode;
	}

	private boolean validateFieldLength(List data, boolean isPhysician, Map<String, String> fieldErrorMap) {
		StringBuilder builder = new StringBuilder();
		
		String networkIds = data.get(NETWORK_ID_INDEX).toString();
		String networkTiers = data.get(NETWORK_TIER_INDEX).toString();
		String groupKey = data.get(GROUP_KEY_INDEX).toString();
		String specialityCode = data.get(SPECIALITY_CODE_INDEX).toString();
		
		if(isPhysician)
		{
			Physician physician = (Physician) data.get(PHYSICIAN_INDEX);
			
			
			if(groupKey==null || groupKey.length()>100) {
				builder.append("Group key is null or greater than 100 characters,  ");
			}	
				
			if(physician.getProviderObj().getNpiNumber()!= null && physician.getProviderObj().getNpiNumber().length()>100){
				builder.append("Npi number is greater than 100 characters,  ");
			}			
			
			if(physician.getTitle()!=null && physician.getTitle().length()>100){
				builder.append("Title is greater than 100 characters,  ");
			}
			
			if(physician.getFirst_name()==null || physician.getFirst_name().length()>100){
				builder.append("First name is null or greater than 100 characters,  ");
			}
			
			if(physician.getMiddle_name()==null || physician.getMiddle_name().length()>100){
				builder.append("Middle name is null or greater than 100 characters,  ");
			}
			
			if(physician.getLast_name()==null || physician.getLast_name().length()>100){
				builder.append("Last name is null or greater than 100 characters,  ");
			}

			if(physician.getSuffix()!=null && physician.getSuffix().length()>10){
				builder.append("Suffix is greater than 10 characters,  ");
			}
			
			if(physician.getAffiliatedHospital()!=null && physician.getAffiliatedHospital().length()>1000){
				builder.append("Affiliated Hospital is greater than 1000 characters,  ");
			}
			
			if(physician.getPhysicianAddressObj().getAddress1()==null || physician.getPhysicianAddressObj().getAddress1().length()>1000){
				builder.append("Address1 is null or greater than 1000 characters,  ");
			}
			
			if(physician.getPhysicianAddressObj().getAddress2()!=null && physician.getPhysicianAddressObj().getAddress2().length()>1000){
				builder.append("Address2 is greater than 1000 characters,  ");
			}
			
			if(physician.getEmailAddress()!=null && physician.getEmailAddress().length()>100){
				builder.append("Email address is greater than 100 characters,  ");
			}
			
			if(physician.getPhysicianAddressObj().getCity()==null || physician.getPhysicianAddressObj().getCity().length()>80){
				builder.append("City is null or greater than 80 characters,  ");
			}
			
			if(physician.getPhysicianAddressObj().getState()==null || physician.getPhysicianAddressObj().getState().length()>8){
				builder.append("State is null or greater than 8 characters,  ");
			}
			
			if(physician.getPhysicianAddressObj().getZip()==null || physician.getPhysicianAddressObj().getZip().length()>20){
				builder.append("Zip is null or greater than 20 characters,  ");
			}
			
			if(physician.getPhysicianAddressObj().getCounty()!=null && physician.getPhysicianAddressObj().getCounty().length()>80){
				builder.append("County is greater than 80 characters,  ");
			}
			
			if(physician.getProviderObj().getDatasource()!=null && physician.getProviderObj().getDatasource().length()>60){
				builder.append("Data source is greater than 60 characters,  ");
			}
						
		}
		else
		{
			Facility facility = (Facility) data.get(FACILITY_INDEX);
			
			if(groupKey==null || groupKey.length()>100) {
				builder.append("Group key is null or greater than 100 characters,  ");
			}
			
			if(facility.getProvider().getName()==null || facility.getProvider().getName().length()>400){
				builder.append("Name is null or greater than 400 characters,  ");
			}
			if(facility.getFacilityAddressObj().getCity()==null || facility.getFacilityAddressObj().getCity().length()>80){
				builder.append("City is null or greater than 80 characters,  ");
			}
			
			if(facility.getFacilityAddressObj().getState()==null ||  facility.getFacilityAddressObj().getState().length()>8){
				builder.append("State is null or greater than 8 characters,  ");
			}
			
			if(facility.getFacilityAddressObj().getZip()==null || facility.getFacilityAddressObj().getZip().length()>40){
				builder.append("Zip is null or greater than 40 characters,  ");
			}
			
			if(facility.getFacilityAddressObj().getCounty()!=null && facility.getFacilityAddressObj().getCounty().length()>80){
				builder.append("County is greater than 80 characters,  ");
			}
			
			if(facility.getProvider().getDatasource()!=null && facility.getProvider().getDatasource().length()>60){
				builder.append("Data source is greater than 60 characters,  ");
			}
			
		}
		
		if(builder.length() != 0){
			fieldErrorMap.put(groupKey, builder.toString());
			return false;
		}
		
		return true;
	}


	private void buildDuplicatePhysicianGroupIdMap(Map<String, Set<PhysicianAddress>> duplicateGroupIdMap, String groupKey, PhysicianAddress physicianAddress) {
		
		if(duplicateGroupIdMap.containsKey(groupKey)){
			Set<PhysicianAddress> physicianAddresses = duplicateGroupIdMap.get(groupKey);
			physicianAddresses.add(physicianAddress);
		}
		else{
			Set<PhysicianAddress> physicianAddresses = new HashSet<PhysicianAddress>();
			physicianAddresses.add(physicianAddress);
			duplicateGroupIdMap.put(groupKey, physicianAddresses);
		}
	}
	
	
	private void buildDuplicateFacilityGroupIdMap(Map<String, Set<FacilityAddress>> duplicateFacilityGroupIdMap, String groupKey, FacilityAddress facilityAddress){
		if(duplicateFacilityGroupIdMap.containsKey(groupKey)){
			Set<FacilityAddress> facilityAddresses = duplicateFacilityGroupIdMap.get(groupKey);
			facilityAddresses.add(facilityAddress);
		}
		else{
			Set<FacilityAddress> facilityAddresses = new HashSet<FacilityAddress>();
			facilityAddresses.add(facilityAddress);
			duplicateFacilityGroupIdMap.put(groupKey, facilityAddresses);
		}
	}
	

	private void insertDuplicatePhysicianAddresses(Map<String, Set<PhysicianAddress>> duplicateGroupIdMap){
		EntityManager entityManager = null;
		try {
			int maxBatchSize = Integer.parseInt(batchSize);
			StringBuilder strQuery = new StringBuilder();
			entityManager = entityManagerFactory.createEntityManager();
			
			Query query;
			
			List<Object[]> physicianIds = new ArrayList<Object[]>();
			int fromIndex = 0, count = IN_CLAUSE_ITEM_COUNT;
			Object[] groupKeyArr = duplicateGroupIdMap.keySet().toArray();
			while(fromIndex < groupKeyArr.length) {
				physicianIds.addAll(getPhysicianIdGroupKeyForGroups(fromIndex, count, groupKeyArr, entityManager));
				fromIndex += count;
			}
			
			int insertCount = 0;
			for(int i=0;i<physicianIds.size();i++){
				try
				{
					Object[] data = physicianIds.get(i);
					
					BigDecimal physicianId=(BigDecimal)data[0];
					String groupKey = (String)data[1];
					
					Set<PhysicianAddress> physicianAddresses= duplicateGroupIdMap.get(groupKey);
					
					if(insertCount==0 && !entityManager.getTransaction().isActive()){
						entityManager.getTransaction().begin();
					}
					
					for(PhysicianAddress physicianAddress : physicianAddresses){
						strQuery = new StringBuilder();
						
						query = entityManager.createNativeQuery("SELECT physician_address_seq.nextval FROM DUAL");
						BigDecimal sequenceValue = (BigDecimal) query.getSingleResult();
						
						strQuery.append("insert into PHYSICIAN_ADDRESS ");
						strQuery.append("(ID,ADDRESS1,CITY,COUNTY,CREATION_TIMESTAMP,EXTENDED_ZIPCODE,FAX,PHONE_NUMBER,");
						strQuery.append("STATE,STATUS,LAST_UPDATE_TIMESTAMP,ZIP,PHYSICIAN_ID,ADDRESS2,LATTITUDE,LONGITUDE) values ");
						strQuery.append( "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
						
						LOGGER.debug("Query to insert PHYSICIAN_ADDRESS : " + strQuery.toString() );
						
						query = entityManager.createNativeQuery(strQuery.toString());
						
						//printPhysicianAddress(physicianAddress, sequenceValue, physicianId);
						
						query.setParameter(1, sequenceValue);
						query.setParameter(2, physicianAddress.getAddress1());
						query.setParameter(3, physicianAddress.getCity());
						query.setParameter(4, physicianAddress.getCounty());
						query.setParameter(5,  new TSDate());
						query.setParameter(6, physicianAddress.getExtendedZipcode());
						query.setParameter(7, physicianAddress.getFax());
						query.setParameter(8, physicianAddress.getPhoneNumber());
						query.setParameter(9, physicianAddress.getState());
						query.setParameter(10, physicianAddress.getStatus());
						query.setParameter(11, new TSDate());
						query.setParameter(12, physicianAddress.getZip());
						query.setParameter(13, physicianId);
						query.setParameter(14, physicianAddress.getAddress2());
						query.setParameter(15, physicianAddress.getLattitude()==null?"":physicianAddress.getLattitude());
						query.setParameter(16, physicianAddress.getLongitude()==null?"":physicianAddress.getLongitude());
						
						query.executeUpdate();
					}
					
					if(insertCount % maxBatchSize == 0){
						entityManager.getTransaction().commit();
						insertCount=0;
					}
				} catch (Exception exception){
					LOGGER.error("Error occured while inserting duplicate addresses", exception);
					if(entityManager.getTransaction().isActive()) {
						entityManager.getTransaction().rollback();
						insertCount=0;
					}
				}
			}
			
			if(entityManager.getTransaction().isActive()) {	
				entityManager.getTransaction().commit();
			}
		} catch(Exception exception){
			LOGGER.error("Error occcured in ", exception);
		}
		finally{
			if(entityManager.isOpen()){
				entityManager.close();
			}
		}
	}

	private List<Object[]> getPhysicianIdGroupKeyForGroups(int fromIndex, int count, Object[] keys, EntityManager entityManager) {
		StringBuilder strQuery = new StringBuilder();
		int toIndex = fromIndex + count - 1;
		if(toIndex >= keys.length) {
			toIndex = keys.length -1;
		}
		LOGGER.debug("getPhysicianIdGroupKeyForGroups - from: " + fromIndex + " to:" +  toIndex + " total records:" + keys.length);
		strQuery.append("select physician.ID, provider.GROUP_KEY from PHYSICIAN physician, PROVIDER provider");
		strQuery.append(" where physician.PROVIDER_ID=provider.ID and provider.GROUP_KEY IN (");

		for(int i=fromIndex; i <= toIndex ; i++) {
			if(i == toIndex){
				strQuery.append("?");
			}
			else
			{
				strQuery.append("?,");
			}			
		}
		strQuery.append(")");		
		LOGGER.debug("Query to retrieve PHYSICIAN.ID, PROVIDER.GROUP_KEY : " + strQuery.toString() );

		Query query = entityManager.createNativeQuery(strQuery.toString());;
		int cnt=1;
		for(int i=fromIndex; i <= toIndex ; i++){
			query.setParameter(cnt++, keys[i]);
		}
		return query.getResultList();
	}

	
	private List<Object[]> getFacilityIdGroupKeyForGroups(int fromIndex, int count, Object[] keys, EntityManager entityManager) {
		StringBuilder strQuery = new StringBuilder();
		int toIndex = fromIndex + count - 1;
		if(toIndex >= keys.length) {
			toIndex = keys.length -1;
		}
		LOGGER.debug("getFacilityIdGroupKeyForGroups - from: " + fromIndex + " to:" +  toIndex + " total records:" + keys.length);
		strQuery.append("select facility.ID, provider.GROUP_KEY from FACILITY facility, PROVIDER provider");
		strQuery.append(" where facility.PROVIDER_ID=provider.ID and provider.GROUP_KEY IN (");
		
		for(int i=fromIndex; i <= toIndex ; i++){
			if(i == toIndex){
				strQuery.append("?");
			}
			else
			{
				strQuery.append("?,");
			}			
		}
		strQuery.append(")");		
		LOGGER.debug("Query to retrieve FACILITY.ID, FACILITY.GROUP_KEY : " + strQuery.toString() );
		Query query = entityManager.createNativeQuery(strQuery.toString());;
		int cnt=1;
		for(int i=fromIndex; i <= toIndex ; i++){
			query.setParameter(cnt++, keys[i]);
		}
		return query.getResultList();
	}
	
	private void insertDuplicateFacilityAddresses(Map<String, Set<FacilityAddress>> duplicateGroupIdMap){
		EntityManager entityManager = null;
		try {
			int maxBatchSize = Integer.parseInt(batchSize);
			StringBuilder strQuery = new StringBuilder();
			
			entityManager = entityManagerFactory.createEntityManager();
			Query query;
			
			List<Object[]> facilityIds = new ArrayList<Object[]>();
			int fromIndex = 0, count = IN_CLAUSE_ITEM_COUNT;
			Object[] groupKeyArr = duplicateGroupIdMap.keySet().toArray();
			int groupKeysSize = groupKeyArr.length;
			while(fromIndex < groupKeysSize) {
				facilityIds.addAll(getFacilityIdGroupKeyForGroups(fromIndex, count, groupKeyArr, entityManager));
				fromIndex += count;
			}
			
			int insertCount = 0;
			for(int i=0;i<facilityIds.size();i++){
				try
				{
					Object[] data = facilityIds.get(i);
					
					BigDecimal facilityId=(BigDecimal)data[0];
					String groupKey = (String)data[1];
					
					Set<FacilityAddress> facilityAddresses= duplicateGroupIdMap.get(groupKey);
					
					if(insertCount==0 && !entityManager.getTransaction().isActive()){
						entityManager.getTransaction().begin();
					}
					
					for(FacilityAddress facilityAddress : facilityAddresses){
						strQuery = new StringBuilder();
						
						query = entityManager.createNativeQuery("SELECT facility_address_seq.nextval FROM DUAL");
						BigDecimal sequenceValue = (BigDecimal) query.getSingleResult();
						
						strQuery.append("insert into FACILITY_ADDRESS ");
						strQuery.append("(ID,ADDRESS1,CITY,COUNTY,CREATION_TIMESTAMP,EXTENDED_ZIPCODE,FAX,PHONE_NUMBER,");
						strQuery.append("STATE,STATUS,LAST_UPDATE_TIMESTAMP,ZIP,FACILITY_ID,ADDRESS2,LATTITUDE,LONGITUDE) values ");
						strQuery.append( "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
						
						LOGGER.debug("Query to insert FACILITY_ADDRESS : " + strQuery.toString() );
						
						query = entityManager.createNativeQuery(strQuery.toString());
						
						//printPhysicianAddress(physicianAddress, sequenceValue, physicianId);
						
						query.setParameter(1, sequenceValue);
						query.setParameter(2, facilityAddress.getAddress1());
						query.setParameter(3, facilityAddress.getCity());
						query.setParameter(4, facilityAddress.getCounty());
						query.setParameter(5,  new TSDate());
						query.setParameter(6, facilityAddress.getExtendedZipcode());
						query.setParameter(7, facilityAddress.getFax());
						query.setParameter(8, facilityAddress.getPhoneNumber());
						query.setParameter(9, facilityAddress.getState());
						query.setParameter(10, facilityAddress.getStatus());
						query.setParameter(11, new TSDate());
						query.setParameter(12, facilityAddress.getZip());
						query.setParameter(13, facilityId);
						query.setParameter(14, facilityAddress.getAddress2());
						query.setParameter(15, facilityAddress.getLattitude()==null?"":facilityAddress.getLattitude());
						query.setParameter(16, facilityAddress.getLongitude()==null?"":facilityAddress.getLongitude());
						
						query.executeUpdate();
					}
					
					if(insertCount % maxBatchSize == 0){
						entityManager.getTransaction().commit();
						insertCount=0;
					}
				} catch (Exception exception){
					LOGGER.error("Error occured while inserting duplicate addresses for facilities", exception);
					if(entityManager.getTransaction().isActive()) {
						entityManager.getTransaction().rollback();
						insertCount=0;
					}
				}
			}
			
			if(entityManager.getTransaction().isActive()) {	
				entityManager.getTransaction().commit();
			}
		} catch(Exception exception){
			LOGGER.error("Error occcured in ", exception);
		}
		finally{
			if(entityManager.isOpen()){
				entityManager.close();
			}
		}
	}
	
	/*
	 * Unused Method
	 * private void printPhysicianAddress(PhysicianAddress physicianAddress, BigDecimal sequenceValue, BigDecimal physicianId) {
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("Sequence Value : " +  sequenceValue);
		buffer.append("\nphysicianAddress.getAddress1() : ");
		buffer.append(physicianAddress.getAddress1());
		buffer.append("\nphysicianAddress.getCity() : ");
		buffer.append(physicianAddress.getCity());
		buffer.append("\nphysicianAddress.getCounty() : ");
		buffer.append(physicianAddress.getCounty());
		buffer.append("\nDate1 : ");
		buffer.append(new TSDate());
		buffer.append("\nphysicianAddress.getExtendedZipcode() : ");
		buffer.append(physicianAddress.getExtendedZipcode());
		buffer.append("\nphysicianAddress.getFax() : ");
		buffer.append(physicianAddress.getFax());
		buffer.append("\nphysicianAddress.getPhoneNumber() : ");
		buffer.append(physicianAddress.getPhoneNumber());
		buffer.append("\nphysicianAddress.getState() : ");
		buffer.append(physicianAddress.getState());
		buffer.append("\nphysicianAddress.getStatus() : ");
		buffer.append(physicianAddress.getStatus());
		buffer.append("\nphysicianAddress.getAddress1() : ");
		buffer.append(new TSDate());
		buffer.append("\nphysicianAddress.getZip() : ");
		buffer.append(physicianAddress.getZip());
		buffer.append("\nphysicianId : " + physicianId);
		buffer.append("\nphysicianAddress.getAddress2() : ");
		buffer.append(physicianAddress.getAddress2());
		buffer.append("\nphysicianAddress.getLattitude() : ");
		buffer.append(physicianAddress.getLattitude());
		buffer.append("\nphysicianAddress.getLongitude() : ");
		buffer.append(physicianAddress.getLongitude());
		
		LOGGER.info("Physician Address is : " + buffer.toString());
	}

	private List<Object[]> getPhysicianAndGroupKey(Set<String> groupKeySet){
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		StringBuilder query = new StringBuilder();
		query.append("select physician.ID, provider.GROUP_KEY");
		query.append("from PHYSICIAN physician,PROVIDER provider");
		if(!entityManager.getTransaction().isActive()){
			
		}
		return null;
	}
*/
	private String copyToCSV(File file,String type){
		InputStream inStream = null;
		OutputStream outStream = null;
		String csvFileName = "enclarity_"+type+"_"+TimeShifterUtil.currentTimeMillis()+".csv";
	    	try{
	    	    File csvFile =new File(csvFileName);
	 
	    	    inStream = new FileInputStream(file);
	    	    outStream = new FileOutputStream(uploadFilePath + File.separator+csvFile);
	 
	    	    byte[] buffer = new byte[1024];
	 
	    	    int length;
	    	    while ((length = inStream.read(buffer)) > 0){
	 
	    	    	outStream.write(buffer, 0, length);
	 
	    	    }
	 
	    	    inStream.close();
	    	    outStream.close();
	 
	    	}catch(IOException e){
	    		LOGGER.info("Error :"+e.getMessage());
	    	}
	    	return csvFileName;
	}
	
	private File writeEnclarityLogFile(String uploadedFileName,String type,boolean isFileFound){
		File file = null;
		Date date = new TSDate( );
		SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyyMMdd_HH_mm_ss");
		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
		if(type.equalsIgnoreCase(PHYSICIANS)){
			file = new File(enclarityStatusLogLocation+File.separator+"calhx_individual_"+dateFormat.format(date)+"_status.txt");		
		}else{
			file = new File(enclarityStatusLogLocation+File.separator+"calhx_facility_"+dateFormat.format(date)+"_status.txt");
		}
		BufferedWriter writer = null;
		try {
			FileWriter fw  = new FileWriter(file.getAbsoluteFile(),true);
			writer = new BufferedWriter(fw);
			if(isFileFound){
				if(type.equalsIgnoreCase(PHYSICIANS)){
					writer.append("Physician File :  "+uploadedFileName+", Reading started.");
				}else{
					writer.append("Facility File  :  "+uploadedFileName+", Reading started.");
				}
			}
			writer.newLine();
			writer.append("Status Log at : "+df.format(date));
			writer.newLine();
			writer.newLine();
			writer.append("=======================================================================");
			writer.newLine();
			writer.newLine();
			
			
		} catch (IOException e1) {
			LOGGER.error("Error while creating file file",e1);
		}
		finally{
			try {
				writer.flush();
				writer.close();				
			} catch (IOException e) {
				LOGGER.error("Error while closing BufferedWriter");
			}
		}		
		return file;
		
	}
	
	private void writeEnclarityStatusLog(List<Map<Integer, String>> errorAndWarningList, File file){
		BufferedWriter writer = null;				
		try {
			FileWriter fw  = new FileWriter(file,true);
			writer = new BufferedWriter(fw);
			for(Map<Integer,String> errors : errorAndWarningList){
				int errorCount = 0;
				for(Integer i : errors.keySet()){
					writer.newLine();					
					if(errorCount==0){
						writer.append(errors.get(i));
						writer.newLine();
					}else{
						StringBuilder builder = new StringBuilder();					
						builder.append("	");
						builder.append(errorCount);
						builder.append(".  ");
						builder.append(errors.get(i));						
						writer.append(builder.toString());
						writer.newLine();
					}
					errorCount++;
				}				
				writer.newLine();
				writer.append("=======================================================================");
				writer.newLine();
				
			}
		} catch (Exception e) {
			LOGGER.error("Error while writing enclarity status log ", e);
		}
		finally{
			try {
				writer.flush();
				writer.close();				
			} catch (IOException e) {
				LOGGER.error("Error while closing BufferedWriter");
			}
		}		
	}
	
	private void writeEnclarityEndLog(boolean isFileFound,Map<String,Integer>networkIdStatusMap, Set<String> hiosIdSet, Integer rows, File file, Integer counter, String type){
		BufferedWriter writer = null;
		StringBuilder builder = new StringBuilder();
		Integer  records = null;
		try {
			FileWriter fw  = new FileWriter(file,true);
			writer = new BufferedWriter(fw);
			writer.newLine();
			if(isFileFound){
				
				writer.append("Finished Reading File.");
				writer.newLine();
				writer.append("Number rows in file : "+rows);
				writer.newLine();
				writer.append("Number rows imported successfully: "+counter);
				writer.newLine();
				int alreadyAdded = 0;
				if(networkIdStatusMap.containsKey(ALREADY_ADDED)){
					
					records = networkIdStatusMap.get(ALREADY_ADDED);
					builder.append("No. Of ");
					builder.append(type);
					builder.append(" already imported in database : ");
					builder.append(records);
					
					writer.append(builder.toString());
					alreadyAdded = records==null?0:records;
					writer.newLine();
				}
				int failed = rows - counter-alreadyAdded;
				writer.append("Number rows import failed : "+failed);
			}
			if(networkIdStatusMap!=null){
				writer.newLine();
				writer.newLine();
				writer.append("=======================================================================");
				writer.newLine();
				writer.newLine();
				
					for(String networkIdKey : networkIdStatusMap.keySet()){
						records = networkIdStatusMap.get(networkIdKey);
						if(networkIdKey.contains(NO_NETWORK)){
							builder = new StringBuilder();
							builder.append("No. Of ");
							builder.append(type);
							builder.append(" Network Id unavailable :  ");
							builder.append(records);
							
							writer.append(builder.toString());
							writer.newLine();
							writer.newLine();
						}else if(networkIdKey.contains(NOT_ASSOCIATED)){
							String networkId = networkIdKey.replace(NOT_ASSOCIATED,"");
							builder = new StringBuilder();
							builder.append("No. Of ");
							builder.append(type);
							builder.append("  Not Associated with network  ");
							builder.append(networkId);
							builder.append("  :");
							builder.append(records);
							
							writer.append(builder.toString());
							writer.newLine();
							writer.newLine();
						}else if(networkIdKey.contains(DATA_MISSING)){
							
							builder = new StringBuilder();
							builder.append("No. Of Records, mandatory fields missing   :  ");
							builder.append(records);
							
							writer.append(builder.toString());
							writer.newLine();
							writer.newLine();
						}else if(networkIdKey.contains(HIOS_NOTPROVIDED)){
							
							builder = new StringBuilder();
							builder.append("No. Of Records, Issuer Hios Id not provided   :  ");
							builder.append(records);
							
							writer.append(builder.toString());
							writer.newLine();
							writer.newLine();							
						}else if(!networkIdKey.contains("hios")&&!networkIdKey.contains(ALREADY_ADDED)){
							writer.newLine();
							writer.newLine();
							
							builder = new StringBuilder();
							builder.append("No. Of ");
							builder.append(type);
							builder.append(" added for network Id  ");
							builder.append(networkIdKey);
							builder.append(":   ");
							builder.append(records);
							
							writer.append(builder.toString());
							writer.newLine();
							writer.newLine();
						}
					}
					if(hiosIdSet!=null){
						writer.newLine();
						writer.newLine();
						writer.append("=======================================================================");
						writer.newLine();
						writer.newLine();
						for(String hiosId : hiosIdSet){
							writer.append("Issuer HIOS Id :   "+hiosId);
							writer.newLine();
								if(networkIdStatusMap.get(HIOS_ADDED+hiosId)!=null){
									builder = new StringBuilder();
									builder.append("No. Of ");
									builder.append(type);
									builder.append(" import successful :   ");
									builder.append(networkIdStatusMap.get(HIOS_ADDED+hiosId));									
									writer.append(builder.toString());
									
								}else{
									builder = new StringBuilder();
									builder.append("No. Of ");
									builder.append(type);
									builder.append(" import successful :   0");																		
									writer.append(builder.toString());																		
								}
								writer.newLine();
								if(networkIdStatusMap.get(HIOS_REJECTED+hiosId)!=null){
									
									builder = new StringBuilder();
									builder.append("No. Of ");
									builder.append(type);
									builder.append(" import failed :   ");
									builder.append(networkIdStatusMap.get(HIOS_REJECTED+hiosId));									
									writer.append(builder.toString());
																	
								}else{
									builder = new StringBuilder();
									builder.append("No. Of ");
									builder.append(type);
									builder.append(" import failed :   0");																		
									writer.append(builder.toString());																			
								}
								writer.newLine();
								writer.newLine();
						}
					}
				}
			writer.newLine();
			writer.newLine();
			writer.append("============================END OF LOGS================================");
			writer.newLine();
			Date date = new TSDate( );
			SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
			writer.append(type+" file status log completed at : "+df.format(date));			
			
		} catch (Exception e) {
			LOGGER.error("Error while writing enclarity end status log  ",e);
		}
		finally{
			try {
				writer.flush();
				writer.close();				
			} catch (Exception e) {
				LOGGER.error("Error while closing BufferedWriter",e);
			}
		}
		LOGGER.error("Writing status logs for "+type+" file completed successfully!");
	}
	
	/*
	 * Unused method
	 * 
	 * private void writeEnclarityLog(String uploadedFileName,String type, Integer counter, List<Map<Integer, String>> errorAndWarningList, Integer rows, boolean isFileFound,Map<String,Integer>networkIdStatusMap, Set<String> hiosIdSet){
		File file = null;
		Date date = new TSDate( );
		SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyyMMdd");
		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
		if(type.equalsIgnoreCase(PHYSICIANS)){
			file = new File(enclarityStatusLogLocation+File.separator+"calhx_individual_"+dateFormat.format(date)+"-status.txt");		
		}else{
			file = new File(enclarityStatusLogLocation+File.separator+"calhx_facility_"+dateFormat.format(date)+"-status.txt");
		}
		BufferedWriter writer = null;
		try {
			FileWriter fw  = new FileWriter(file.getAbsoluteFile());
			writer = new BufferedWriter(fw);
			if(isFileFound){
				if(type.equalsIgnoreCase(PHYSICIANS)){
					writer.append("Physician File :  "+uploadedFileName+", Reading started.");
				}else{
					writer.append("Facility File  :  "+uploadedFileName+", Reading started.");
				}
			}
			writer.newLine();
			writer.append("Status Log at : "+df.format(date));
			writer.newLine();
			writer.newLine();
			writer.append("=======================================================================");
			writer.newLine();
			writer.newLine();
			
			
		} catch (IOException e1) {
			LOGGER.info("Error while reading file"+e1.getMessage());
		}
		if(!errorAndWarningList.isEmpty()){
			int rowCount = 1;
			try {
			for(Map<Integer,String> errors : errorAndWarningList){
				int errorCount = 0;
				for(Integer i : errors.keySet()){
						writer.newLine();					
					if(errorCount==0){
						writer.append(rowCount+".  "+errors.get(i));
						writer.newLine();
					}else{
						writer.append("	"+errorCount+".  "+errors.get(i));
					}
					errorCount++;
				}
				rowCount++;
				writer.newLine();
				writer.append("=======================================================================");
				writer.newLine();
			}
			} catch (IOException e) {
			}
		}
		try {
			writer.newLine();
			if(isFileFound){
				writer.append("Finished Reading File.");
				writer.newLine();
				writer.append("Number rows in file : "+rows);
				writer.newLine();
				writer.append("Number rows imported successfully: "+counter);
				writer.newLine();
				int failed = rows - counter;
				writer.append("Number rows import failed : "+failed);
			}
			if(networkIdStatusMap!=null){
				writer.newLine();
				writer.newLine();
				writer.append("=======================================================================");
				writer.newLine();
				writer.newLine();
				
					for(String networkIdKey : networkIdStatusMap.keySet()){
						if(networkIdKey.contains(NO_NETWORK)){
							writer.append("No. Of "+type+" Network Id unavailable :  "+networkIdStatusMap.get(networkIdKey));
							writer.newLine();
							writer.newLine();
						}else if(networkIdKey.contains(NOT_ASSOCIATED)){
							String networkId = networkIdKey.replace(NOT_ASSOCIATED,"");
							writer.append("No. Of "+type+" Not Associated with network "+networkId+"  :"+networkIdStatusMap.get(networkIdKey));
							writer.newLine();
							writer.newLine();
						}else if(networkIdKey.contains(ALREADY_ADDED)){
							writer.append("No. Of "+type+" already imported in database :  "+networkIdStatusMap.get(networkIdKey));
							writer.newLine();
							writer.newLine();
						}else if(networkIdKey.contains(DATA_MISSING)){
							writer.append("No. Of Records, mandatory fields missing   :  "+networkIdStatusMap.get(networkIdKey));
							writer.newLine();
							writer.newLine();
						}else if(networkIdKey.contains(HIOS_NOTPROVIDED)){
							writer.append("No. Of Records, Issuer Hios Id not provided   :  "+networkIdStatusMap.get(networkIdKey));
							writer.newLine();
							writer.newLine();							
						}else if(!networkIdKey.contains("hios")){
							writer.newLine();
							writer.newLine();
							writer.append("No. Of "+type+" added for network Id  "+networkIdKey+":   "+networkIdStatusMap.get(networkIdKey));
							writer.newLine();
							writer.newLine();
						}
					}
					if(hiosIdSet!=null){
						writer.newLine();
						writer.newLine();
						writer.append("=======================================================================");
						writer.newLine();
						writer.newLine();
						for(String hiosId : hiosIdSet){
							writer.append("Issuer HIOS Id :   "+hiosId);
							writer.newLine();
								if(networkIdStatusMap.get(HIOS_ADDED+hiosId)!=null){
								writer.append("No. Of "+type+" import successful :   "+networkIdStatusMap.get(HIOS_ADDED+hiosId));
								}else{
									writer.append("No. Of "+type+" import successful :   "+" 0 ");
								}
								writer.newLine();
								if(networkIdStatusMap.get(HIOS_REJECTED+hiosId)!=null){
								writer.append("No. Of "+type+" import failed :   "+networkIdStatusMap.get(HIOS_REJECTED+hiosId));
								}else{
									writer.append("No. Of "+type+" import failed :   "+" 0 ");	
								}
								writer.newLine();
								writer.newLine();
						}
					}
				}
			writer.newLine();
			writer.newLine();
			writer.append("============================END OF LOGS================================");
			writer.close();
		} catch (IOException e) {
		}
		
	}*/
	
	private List<Network> getNetworkForProvider(String networkId,String hiosId){
	 if(networkMap.containsKey(networkId+hiosId)){
			return networkMap.get(networkId+hiosId);
		}else{
			try{
				List<Network> network = providerSearchNetworkService.getNetworkByNetworkId(networkId, hiosId);
				if(network!=null){
					networkMap.put(networkId+hiosId, network);
				}
				return network;
			}catch(Exception e){
				LOGGER.error("Multiple netowork found with same network Id",e);
				return null;
			}
		}
	}
	
	@RequestMapping(value = "/provider/searchproviders", method = RequestMethod.GET)
	public String searchTemplate(HttpServletRequest request,Model model) {
		try {
			LOGGER.debug("Inside provider search");
			
			String domElement = request.getParameter("domElement");
			String defaultZipCode = request.getParameter("zipcode");
			model.addAttribute("domElement",domElement); 
			model.addAttribute("defaultZip",defaultZipCode);
		} catch (Exception e) {
			LOGGER.error("error "+e.getMessage());	
		}
		return "provider/searchproviders";
	}
	
	@RequestMapping(value = "/provider/doctors/search/DEPRECATED",  method = {RequestMethod.GET, RequestMethod.POST})
	public String searchPhysician(Model model,HttpServletRequest request,
			@RequestParam(value = "doctorName", required = false) String name,
			@RequestParam(value = "location", required = false) String location,
			@RequestParam(value = "defaultZip", required = false) String defaultZip) {
		try {
			Pageable pageable =	issuerService.getPaging(model, request);
			if(StringUtils.isNotBlank(location)){
				location = location.trim();
			}
			boolean isFullName = false;
			String physicianName = null;
			String zipCode = "";
			if(StringUtils.isNotBlank(name)){
				name = name.trim();
				if(name.contains(" ")){
					String [] nameArray = name.split(" ");
					int wordsCount = 0;
					name ="";
					for(String tempName:nameArray){
						if(StringUtils.isNotBlank(tempName)){
							wordsCount++;
							if(StringUtils.isNotBlank(name)){
								name = name+" ";
							}
								name = name + tempName;			
						}
						
					}
					if(wordsCount==1){
						physicianName = name;
						isFullName = false;
					}else{
						physicianName = name;
						isFullName = true;
					}
				}else{
					physicianName = name.trim();
					isFullName = false;
				}
				
			}
			

			if(StringUtils.isBlank(location)){
					location = null;
					zipCode = null;
			if(StringUtils.isBlank(name)&&StringUtils.isNotBlank(defaultZip)){
					zipCode = defaultZip;
				}
			}else if(StringUtils.isNumeric(location)){
				zipCode = location;
				location = null;
			}	
			Page<Physician> physicians  = providerService.getPhysicianOrDentist(physicianName,isFullName, location,zipCode,PhysicianType.DOCTOR.toString(),pageable);
			
			model.addAttribute(PHYSICIAN_OBJ,physicians.getContent());
			model.addAttribute(RESULT_SIZE,physicians.getTotalElements());
			model.addAttribute(PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute("defaultZip",defaultZip);
			
			StringBuffer mapString = new StringBuffer("[");
			
			for(Physician physician :physicians){
				if(physician.getPhysicianAddress().size()>0){
					PhysicianAddress physicianAddress = physician.getPhysicianAddress().iterator().next();
					if(physician.getFirst_name()!=null||physician.getLast_name()!=null){
						mapString = mapString.append("["+physicianAddress.getLattitude()+","+physicianAddress.getLongitude()+"],");
					}
				}
			}
			
			mapString.setLength(mapString.length()-1);
			mapString.append("]");
			model.addAttribute("mapString", mapString);
			model.addAttribute("physicianType","DOCTOR");
			LOGGER.info(physicians.toString());
		} catch (Exception e) {
			LOGGER.error("Error "+e.getMessage());
			e.fillInStackTrace();
		}
		return "provider/search/physicians";
	}
	
	@RequestMapping(value = "/provider/dentists/search",  method = {RequestMethod.GET, RequestMethod.POST})
	public String searchDentists(Model model,HttpServletRequest request,
			@RequestParam(value = "dentistName", required = false) String name,
			@RequestParam(value = "location", required = false) String location,
			@RequestParam(value = "defaultZip", required = false) String defaultZip) {
		try {
			Pageable pageable =	issuerService.getPaging(model, request);
			if(StringUtils.isNotBlank(location)){
				location = location.trim();
			}
			boolean isFullName = false;
			String dentistnName = null;
			String zipCode = "";
			if(StringUtils.isNotBlank(name)){
				name = name.trim();
				if(name.contains(" ")){
					String [] nameArray = name.split(" ");
					int wordsCount = 0;
					name ="";
					for(String tempName:nameArray){
						if(StringUtils.isNotBlank(tempName)){
							wordsCount++;
							if(StringUtils.isNotBlank(name)){
								name = name+" ";
							}
								name = name + tempName;			
						}
						
					}
					if(wordsCount==1){
						dentistnName = name;
						isFullName = false;
					}else{
						dentistnName = name;
						isFullName = true;
					}
				}else{
					dentistnName = name.trim();
					isFullName = false;
				}
				
			}
			if(StringUtils.isBlank(location)){
					location = null;
					zipCode = null;
			if(StringUtils.isBlank(name)&&StringUtils.isNotBlank(defaultZip)){
					zipCode = defaultZip;
				}
			}else if(StringUtils.isNumeric(location)){
				zipCode = location;
				location = null;
			}	
			Page<Physician> physicians  = providerService.getPhysicianOrDentist(dentistnName,isFullName, location,zipCode,PhysicianType.DENTIST.toString(),pageable);
			
			model.addAttribute(PHYSICIAN_OBJ,physicians.getContent());
			model.addAttribute(RESULT_SIZE,physicians.getTotalElements());
			model.addAttribute(PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute("defaultZip",defaultZip);
			
			StringBuffer mapString = new StringBuffer("[");
			for(Physician physician :physicians){
				if (physician.getPhysicianAddress().size()>0) {
					PhysicianAddress physicianAddress = physician.getPhysicianAddress().iterator().next();
					if(physician.getFirst_name()!=null||physician.getLast_name()!=null){
						mapString = mapString.append("["+physicianAddress.getLattitude()+","+physicianAddress.getLongitude()+"],");
					}
				}
			}
			mapString.setLength(mapString.length()-1);
			mapString.append("]");
			model.addAttribute("mapString", mapString);
			model.addAttribute("physicianType","DENTIST");
			LOGGER.debug(physicians.toString());
		} catch (Exception e) {
			LOGGER.error("Error "+e.getMessage());
		}
		return "provider/search/physicians";
	}
	

	@RequestMapping(value = "/provider/facilities/search",  method = {RequestMethod.GET, RequestMethod.POST})
	public String searchFacility(Model model,HttpServletRequest request,
			@RequestParam(value = "facilityName", required = false) String name,
			@RequestParam(value = "location", required = false) String location,
			@RequestParam(value = "defaultZip", required = false) String defaultZip) {
		String zipCode = null;
		if(StringUtils.isNotBlank(name)){
			name = name.trim();	
		}
		if(StringUtils.isNotBlank(location)){
			location = location.trim();
		}
		try {
			Pageable pageable =	issuerService.getPaging(model, request);
			if(StringUtils.isNotBlank(location)){
				if(StringUtils.isNumeric(location)){
					zipCode = location;
					location = null;
				}else{
					zipCode = null;
				}
			}else{
				location = null;
				zipCode = null;
			}
			if(StringUtils.isBlank(name)) {
				name = null;
				if(StringUtils.isBlank(location)&&StringUtils.isBlank(zipCode)){
					zipCode = defaultZip;
				}
			}
			


			Page<Facility> facilities  = providerService.findFacility(name,location,zipCode,ProviderType.FACILITY.toString(),pageable);
			
			model.addAttribute(FACILITY_OBJ,facilities.getContent());
			model.addAttribute(RESULT_SIZE,facilities.getTotalElements());
			model.addAttribute(PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute("defaultZip",defaultZip);
			
			StringBuffer mapString = new StringBuffer("[");
			for(Facility facility :facilities){
				if(facility.getFacilityAddress().size()>0){
					FacilityAddress facilityAddress = facility.getFacilityAddress().iterator().next();
					if(facilityAddress.getLattitude()!=null||facilityAddress.getLongitude()!=null){
						mapString = mapString.append("["+facilityAddress.getLattitude()+","+facilityAddress.getLongitude()+"],");
					}
				}
			}
			mapString.setLength(mapString.length()-1);
			mapString.append("]");
			model.addAttribute("mapString", mapString);
			LOGGER.debug("Incide Facility Search");
		} catch (Exception e) {
			LOGGER.error("Error "+e.getMessage());
		}
		return "provider/search/facilities";
	}
	
	
	@RequestMapping(value = "/provider/get_Location_list/DEPRICATED",method = RequestMethod.GET, headers="Accept=*/*")
	@Deprecated
	public @ResponseBody List<String> getLocationList(@RequestParam("term") String query) {
		List<String> countryList = new ArrayList<String>();
		if(Character.isDigit(query.charAt(0)) && query.length() >= 3){
			Iterator zipCodes = zipCodeService.getZipList(query).iterator();
			while(zipCodes.hasNext()){
				countryList.add((String) zipCodes.next());
			}
		}else{
			Iterator cities = zipCodeService.getCityList(query.toUpperCase()).iterator();
			while(cities.hasNext()){
				countryList.add((String) cities.next());
			}
		}
		return countryList;
	}
	
	@RequestMapping(value = "/provider/physiciandetails/{id}", method = RequestMethod.GET)
	public String providerDetails(Model model,@PathVariable("id") int physicianId,HttpServletRequest request){
		try {
			
			Physician physician  = providerService.findPhysician(physicianId);
			StringBuffer mapString  = new StringBuffer("[");
			StringBuffer specialties = new StringBuffer(" ");
			
			PhysicianAddress physicianAddressObj = null;
			
			if (physician.getPhysicianAddress().size()>0) {
				for(PhysicianAddress physicianAddress: physician.getPhysicianAddress()){
					mapString .append("["+physicianAddress.getLattitude()+","+physicianAddress.getLongitude()+"],");
					physicianAddressObj= physicianAddress;
				}
			}
			
			if(physician.getSpecialitySet().size()>0){
				int count = 0;
				for(Specialty physicianSpecialityRelation: physician.getSpecialitySet()){
					specialties.append(physicianSpecialityRelation.getName());
					if (physician.getSpecialitySet().size()>1&&count<physician.getSpecialitySet().size()-1) {
						specialties.append(", ");
					}
					count++;
				}
			}
			String defaultZipCode = request.getParameter("zipcode");
			model.addAttribute("defaultZip",defaultZipCode);
			
			model.addAttribute("specialties", specialties.toString());
			mapString.setLength(mapString.length()-1);
			mapString.append("]");
			model.addAttribute("physician", physician);
			model.addAttribute("mapString", mapString);
			model.addAttribute("physicianAddressObj", physicianAddressObj);
			String physicianType = request.getParameter("phytype");
			model.addAttribute("physicianType",physicianType);
			LOGGER.debug(physician.toString());
		
		} catch (Exception e) {
			LOGGER.error("Error "+e.getMessage());
		}
		LOGGER.debug("Inside providerDetails");
		return "provider/physiciandetails";
	}
	
	
	
	@RequestMapping(value = "/provider/facilitydetails/{id}", method = RequestMethod.GET)
	public String facilityDetails(Model model,@PathVariable("id") int facilityId,HttpServletRequest request) {
		try {
			Facility facility  = providerService.findFacility(facilityId);
			StringBuffer mapString  = new StringBuffer("[");
			FacilityAddress facilityAddressObj =null;
			
			if (facility.getFacilityAddress().size()>0) {
				for(FacilityAddress faciliyAddress: facility.getFacilityAddress()){
					mapString .append("["+faciliyAddress.getLattitude()+","+faciliyAddress.getLongitude()+"],");
					facilityAddressObj = faciliyAddress;
				}
			}
			
			StringBuffer specialties = new StringBuffer();
			for(FacilitySpecialityRelation facilitySpecialityRelation: facility.getFacilitySpecialRelation()){
				specialties.append(facilitySpecialityRelation.getSpecialty().getName());
				if (facility.getFacilitySpecialRelation().size()>1) {
					specialties.append(" | ");
				}
			}
			String defaultZipCode = request.getParameter("zipcode");
			model.addAttribute("defaultZip",defaultZipCode);
			
			model.addAttribute("specialties", specialties.toString());
			mapString.setLength(mapString.length()-1);
			mapString.append("]");
			model.addAttribute("facility", facility);
			model.addAttribute("facilityAddressObj", facilityAddressObj);
			model.addAttribute("mapString", mapString);
			LOGGER.debug("Facility Details : "+facility.getId());
		} catch (Exception e) {
			LOGGER.error("Error "+e.getMessage());
		}
		return "provider/facilitydetails";
	}
	
	
	/*
	 * Unused Method
	 * 
	 * private Location validateAddress(Location location){
		try {
			AddressValidatorResponse response = addressValidatorService.validateAddress(location);
			if (response.getStatus().equals("failed")) {
				LOGGER.error("Address validation failed");
				return null;
			}
			if(response.getList().size()>0) {
				LOGGER.info("got "+response.getList().size()+" new addresses");
				return response.getList().get(0);
			}else{
				LOGGER.info("Address not found");
			}
		} catch (Exception e) {
			LOGGER.error("error while validating address");
		}
		return null;
	}
*/
	
}
