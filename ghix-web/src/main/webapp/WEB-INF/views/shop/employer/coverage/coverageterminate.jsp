<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<div class="gutter10"  id="">

<!--titlebar-->
<div class="row-fluid">
      <div class="span9">
        <h3 id="skip">${employer_name}</h3>
      </div>
</div>
<div class="row-fluid">
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        <div class="header">
        	<h4><spring:message  code='label.youshouldknow'/></h4>
        </div>
        <div class="gutter10">
          <p><spring:message  code='label.terminateYouShouldKnowComment1'/> ${exchangeName} <spring:message  code='label.terminateYouShouldKnowComment2'/> ${exchangeName} <spring:message  code='label.terminateYouShouldKnowComment3'/></p>
          <p><spring:message  code='label.terminateYouShouldKnowComment4'/></p>
        </div>
      </div>
       <c:if test="${terminationAllowed=='N'}">
      	<div class="span9" id="rightpanelerror">
      		<div class="gutter10">
      		<p>You have an outstanding payment.  Please clear any outstanding payments before proceeding.<br/>
      			If you need assistance, please call Customer Service at ${exchangePhone}.
      		</p>
      		</div>
      	</div>	
      </c:if>
      <c:if test="${not empty result_message}">
      	<div class="span9" id="rightpanelerror">
      		<div class="gutter10">
      		${result_message}
      		</div>
      	</div>	
      </c:if>
      <!--sidebar-->
      <c:if test="${empty result_message && terminationAllowed=='Y'}">
      <div class="span9" id="rightpanel">
      	<div class="header">
			<h4><spring:message  code='label.terminationRequest'/></h4>
		</div>
		<!--titlebar-->
		<div class="gutter10">
			<form class="form-horizontal" id="frmterminatecoverage" name="frmterminatecoverage" action="coverageterminate" method="post" >
			<df:csrfToken/>
			
				<input type="hidden" id="employerEnrollmentId" name="employerEnrollmentId" value="<encryptor:enc value="${employerEnrollmentId}"/>" />
			
				<div class="control-group">
					<label class="control-label" for="comp_name"><spring:message code="label.businessName" /></label>
					<div class="controls">
						<label id="comp_name">${employer_name}</label>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="terminationDate"><spring:message code="label.terminationDate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<fmt:parseDate value="${terminationDate}" var="parsedEmpDate" pattern="MMMM d,yyyy" />
						<input type="text" name="terminationDate" id="terminationDate"  value="<fmt:formatDate value="${parsedEmpDate}" type="date" dateStyle="long" pattern="MMMM d, yyyy" />" readonly/>
					</div>
					
				</div>
				<div id="terminationDate_error"></div>
				
				<div class="control-group" >
					<fieldset>
						<legend class="control-label"><spring:message  code='label.reason'/> <span class="aria-hidden">required</span> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></legend>
						<div class="controls">
	<!-- 						<label class="radio "><input type="radio" name="reason" disabled="disabled" style="visibility: hidden;"> </label> -->
							<label class="radio "><input type="radio" name="reason" id="reason1" value="<spring:message code="label.reason1"/>" checked> <spring:message code="label.reason1"/> </label>
							<label class="radio "><input type="radio" name="reason" id="reason2" value="<spring:message code="label.reason2"/>" > <spring:message code="label.reason2"/> </label>
							<label class="radio "><input type="radio" name="reason" id="reason3" value="<spring:message code="label.reason3"/>" > <spring:message code="label.reason3"/> </label>
							<label class="radio "><input type="radio" name="reason" id="reason4" value="<spring:message code="label.reason4"/>" > <spring:message code="label.reason4"/> </label>
						</div>
						<div id="reason_error"></div>
					</fieldset>
				</div>
				
				
				
				<div class="control-group">
					<label class="control-label" for="representative"><spring:message code="label.representative" /></label>
					<div class="controls">
						<label id="representative">${user_name}</label>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="esignature"><spring:message code="label.esignature"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" id="esignature" name="esignature"/>
						<span id="esignature_error"></span>
					</div>
				</div>
				
				<div class="control-group">
						<label class="control-label" for="todaysDate"><spring:message code="label.todaysDate" /></label>
						<div class="controls">
							<label id="todaysDate">${todaysDate}</label>
						</div>
				</div>
				
			
			
			<div class="form-actions">
				<div  class="pull-right">
					<a  href="javascript:history.back();" name="cancelButton" id="cancelButton" class="btn"><spring:message code="label.cancelButton" /></a>
        			<input type="button" name="mainSubmitButton" id="mainSubmitButton" onclick="javascript:submitForm();"  class="btn btn-primary" value="<spring:message code="label.submit"/>" /></div>
       			</div>
			
			</form>
			</div><!--rightpanel-->
			</div>
		</c:if>
      </div>
    </div>
  </div>

<script type="text/javascript">
function submitForm(){
	if ($("#frmterminatecoverage").validate().form()){
		$("#frmterminatecoverage").submit();
	}
}

jQuery.validator.addMethod("esignNamecheck", function(value, element, param) {
	var firstname = '${loggedInUser.firstName}'.replace('&amp;','&');
	var lastname = '${loggedInUser.lastName}'.replace('&amp;','&');
	var esign = $("#esignature").val();
	var userName = firstname + " " + lastname;
	if( esign.toLowerCase() == userName.toLowerCase() ){
		return true ;
	}
	return false;
});

var validator = $("#frmterminatecoverage").validate({ 
	onkeyup: false,
	onclick: false,
	onfocusout: false,
	onSubmit: true,
	rules : {
		'terminationDate' : { required : true},
		'reason' : { required : true},
		'esignature' : { required : true , esignNamecheck : true}
	
	} ,// rules end
	
	messages : {
		'terminationDate' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateTerminationDate' javaScriptEscape='true'/></span>"},
		'reason' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateReason' javaScriptEscape='true'/></span>"},
		'esignature' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateesignature' javaScriptEscape='true'/></span>",
			 esignNamecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateEsignUserName'/></span>"}
	},
	
	});
</script>	