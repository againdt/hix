package com.getinsured.hix.indportal.dto;

import java.util.Comparator;

import com.getinsured.hix.platform.util.DateUtil;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/*
 * HIX-58447
 * Sort current and past applications based on case number
 */

public class MyApplicationsEntryComparator implements Comparator < MyApplicationsEntry > {

  private static final Logger LOGGER = LoggerFactory.getLogger(MyApplicationsEntryComparator.class);

  @Override
  public int compare(MyApplicationsEntry app1, MyApplicationsEntry app2) {
    //HIX-113602 sort queued applications in increasing order of eligibility start date
    if (app1.getIsApplicationQueued() && app2.getIsApplicationQueued()) {
      Date startDate1 = null;
      Date startDate2 = null;
      if (app1.getEligibilityStartDate() == null || app2.getEligibilityStartDate() == null) {
        return 0;
      }

      try {
        startDate1 = new SimpleDateFormat("MMM d, y").parse(app1.getEligibilityStartDate());
        startDate2 = new SimpleDateFormat("MMM d, y").parse(app2.getEligibilityStartDate());
      } catch (ParseException e) {
        LOGGER.error("MyApplicationsEntryComparator- Error parsing eligibilty Start Dates:", e);
      }

      return startDate1.compareTo(startDate2);
    } else {
      Date creationDate1 = null;
      Date creationDate2 = null;

      if (app1.getCreatedDate() == null || app2.getCreatedDate() == null) {
        return 0;
      }

      try {
        creationDate1 = DateUtil.StringToDate(app1.getCreatedDate(), "MMM d, y, h:mm:ss a");
        creationDate2 = DateUtil.StringToDate(app2.getCreatedDate(),"MMM d, y, h:mm:ss a");

        return creationDate2.compareTo(creationDate1);
      } catch (Exception e) {
        LOGGER.error("MyApplicationsEntryComparator - Error converting created dates:", e);
      }
    }

    return 0;
  }
}
