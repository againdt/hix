package com.getinsured.eligibility;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.util.EligibilityConstants;

@Controller
public class BaseController {

	private static final Logger LOGGER = Logger.getLogger(BaseController.class);

	/**
	 * To disable caching for GET methods.
	 *
	 * @return responseHeaders
	 */
	/*protected HttpHeaders getNoCacheHeaders() {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Cache-Control", "no-cache");
		return responseHeaders;
	}*/

	/**
	 *  This method is used to test the working of Eligibility Result Processor
	 *  module after deployment.
	 *
	 * @return WELCOME_MESSAGE as string
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {

		LOGGER.info(EligibilityConstants.WELCOME_MESSAGE);
		return EligibilityConstants.WELCOME_MESSAGE;
	}

}