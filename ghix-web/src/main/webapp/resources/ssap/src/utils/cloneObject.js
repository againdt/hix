export default obj => {
    if(typeof obj !== 'object') {
        return {};
    } else {
        return JSON.parse(JSON.stringify(obj));
    }
};