package com.getinsured.hix.externalassister.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.ExternalAssister;

@Repository
public interface IExternalAssisterRepository extends JpaRepository<ExternalAssister, Integer>{
	
	@Query("FROM ExternalAssister where externalAssisterId= :id ")
	public ExternalAssister getByExternalAssisterId(@Param("id")String id);

	@Query(   " SELECT US.id "
			+ " FROM  AccountUser AS US "
			+ " where US.extnAppUserId = :externalAssisterId ")
	public Integer getUserIdForExternalAssisterId(@Param("externalAssisterId")String externalAssisterId);
	
}
