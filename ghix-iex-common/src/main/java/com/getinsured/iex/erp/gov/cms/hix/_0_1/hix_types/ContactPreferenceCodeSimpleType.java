//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContactPreferenceCodeSimpleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ContactPreferenceCodeSimpleType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="TextMessage"/>
 *     &lt;enumeration value="Email"/>
 *     &lt;enumeration value="Mail"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ContactPreferenceCodeSimpleType")
@XmlEnum
public enum ContactPreferenceCodeSimpleType {


    /**
     * Text message; i.e.,
     * 						Simple Message Service (SMS)
     * 
     */
    @XmlEnumValue("TextMessage")
    TEXT_MESSAGE("TextMessage"),

    /**
     * Email
     * 					
     * 
     */
    @XmlEnumValue("Email")
    EMAIL("Email"),

    /**
     * U.S. Mail
     * 					
     * 
     */
    @XmlEnumValue("Mail")
    MAIL("Mail");
    private final String value;

    ContactPreferenceCodeSimpleType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContactPreferenceCodeSimpleType fromValue(String v) {
        for (ContactPreferenceCodeSimpleType c: ContactPreferenceCodeSimpleType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
