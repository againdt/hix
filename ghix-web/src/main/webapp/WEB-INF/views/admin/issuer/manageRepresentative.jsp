<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" ></script>
<div class="gutter10">
	<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="label.manageRepresentatives"/></li>
        </ul><!--page-breadcrumb ends-->
	</div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h"/>
			</div>
			<div class="span9">
				<h1 id="skip">${issuerObj.name}</h1>
			</div>
		</div>
	</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
					<div class="header">
						 <h4 class="margin0"><spring:message  code="pgheader.issuerAbout"/></h4>
					</div>
					<jsp:include page="issuerDetailsLeftNav.jsp">
        					<jsp:param name="pageName" value="issuerRep"/>
					</jsp:include>
			</div>
			<c:set var="newSortOrder" value="${sortOrder}"></c:set>
			<div class="span9" id="rightpanel">
				<div class="header">
					<h4 class="pull-left"><spring:message  code="pgheader.issuerRepresentative"/></h4>
					<a class="btn btn-primary btn-small pull-right plus" href="<c:url value='/admin/issuer/representative/add/${encIssuerId}'/>"> <i class="icon-plus"></i> <spring:message code='pgheader.addRepresentative' /></a>
					<c:if test="${exchangeState == 'ID' || exchangeState == 'CA'|| exchangeState == 'NV' || exchangeState == 'MN'}"><a class="btn btn-primary btn-small pull-right plus" href="<c:url value='/admin/issuer/enrollmentrepresentative/add/${encIssuerId}'/>"> <i class="icon-plus"></i> <spring:message code='label.addEnrollmentRepresentative' /></a></c:if>	
				</div>
				<div class="row-fluid">
					<c:choose>
						<c:when test="${fn:length(issuerReps) > 0}">
							<spring:message  code="label.nameOnly" var="nameValue" />
							<spring:message  code="label.title" var="titleValue" />
							<spring:message  code="label.lastUpdated" var="lastUpdated" />
							<spring:message  code="label.status.plan" var="statusValue" />
							<spring:message  code="label.updatedBy" var="updatedBy" />
							<spring:message  code="label.role" var="roleValue" />
							
							<table class="table table-striped">
								<thead>
									<tr class="header">							
								 		<th scope="col" class="sortable"><dl:sort title="${nameValue}" sortBy="firstName" sortOrder="${newSortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="${titleValue}" sortBy="title" sortOrder="${newSortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="${lastUpdated}" sortBy="updated" sortOrder="${newSortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="${statusValue}" sortBy="status" sortOrder="${newSortOrder}"></dl:sort></th>								     	
								     	<c:if test="${exchangeState == 'ID'  || exchangeState == 'CA'|| exchangeState == 'NV' || exchangeState == 'MN'}">
								     		<th scope="col" class="sortable"><dl:sort title="${roleValue}" sortBy="role" sortOrder="${newSortOrder}"></dl:sort></th>
								     	</c:if>	
								     	<th scope="col" class="sortable"><dl:sort title="${updatedBy}" sortBy="updatedBy.firstName" sortOrder="${newSortOrder}"></dl:sort></th>
									 	<th class="sortable" scope="col"><i class="icon-cog"></i><!-- <i class="caret"></i> --></th>
									</tr>
								</thead>
								<c:forEach var="issuerRepsObj" items="${issuerReps}">
									<tr>
										<c:set var="encIssuerRepId" ><encryptor:enc value="${issuerRepsObj.id}" isurl="true"/> </c:set>
										<td>
											
													<a href="<c:url value='/admin/issuer/representative/details/${encIssuerRepId}'/>">${issuerRepsObj.firstName} ${issuerRepsObj.lastName}</a>
												
										</td>
										<td>${issuerRepsObj.title}</td>
										<td>${issuerRepsObj.updated}</td>
										<td>${issuerRepsObj.displayStatus}</td>
										<c:if test="${exchangeState == 'ID' || exchangeState == 'CA'|| exchangeState == 'NV' || exchangeState == 'MN'}">
											<td>
												<c:choose>
													<c:when test="${fn:toUpperCase(issuerRepsObj.role) == 'ISSUER_ENROLLMENT_REPRESENTATIVE'}" ><spring:message  code='label.enrollmentRepresentative'/></c:when>
													<c:otherwise><spring:message  code='label.representative'/></c:otherwise>
												</c:choose>
											</td>
										</c:if>	
										<td>${issuerRepsObj.updatedby}</td>
										<td>
											<div class="controls">
												<div class="dropdown">
													<c:choose>
														<c:when test="${exchangeState != 'CA' || (exchangeState == 'CA' && (issuerRepsObj.primarycontact != 'YES' || fn:toUpperCase(issuerRepsObj.displayStatus) != 'ACTIVE'))}">
															<!--  show gear menu start -->
															<a class="dropdown-toggle" id="dLabel" role="button"  data-toggle="dropdown" data-target="#" href="/page.html">
																<i class="icon-cog"></i> <b class="caret"></b>
															</a>

															<ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dLabel">

																<c:if test="${exchangeState != 'CA' || (exchangeState == 'CA' && fn:toUpperCase(issuerRepsObj.displayStatus) == 'INACTIVE')}">
																	<c:choose>
																		<c:when test="${fn:toUpperCase(issuerRepsObj.role) == 'ISSUER_ENROLLMENT_REPRESENTATIVE'}" >
																			<li><a href="<c:url value='/admin/issuer/enrollmentrepresentative/edit/${encIssuerRepId}'/>"><i class="icon-pencil"></i><spring:message code='label.edit'/></a></li>
																		</c:when>
																		<c:otherwise>
																			<li><a href="<c:url value='/admin/issuer/representative/edit/${encIssuerRepId}'/>"><i class="icon-pencil"></i><spring:message code='label.edit'/></a></li>
																		</c:otherwise>
																	</c:choose>
																</c:if>
																<c:if test="${issuerRepsObj.primarycontact != 'YES'}">
																	<li>
																		<%-- a. Do not show suspend option for Primary Contact.
																			 b. Show Activate option for Inactive issuer representative
																			 c. Logged-in issuer rep can't suspend himself/herself
																		 --%>
																		<c:choose>
																			<c:when test="${fn:toUpperCase(issuerRepsObj.displayStatus) == 'SUSPENDED'}"><a href="<c:url value='/admin/issuer/representative/manage/${encIssuerId}?action=active&repId=${encIssuerRepId}'/>"><i class="icon-activate"></i> <spring:message  code='label.activate'/></a></c:when>
																			<c:when test="${fn:toUpperCase(issuerRepsObj.displayStatus) == 'ACTIVE'}"><a href="#" onclick="doSuspend('${issuerRepsObj.firstName}', '${issuerRepsObj.lastName}','${encIssuerRepId}','${encIssuerId}')"><i class="icon-trash"></i> <spring:message  code='label.suspend'/></a></c:when>
																			<%-- Show 'Send email activation' link for unregistered issuer representative only on ID state exchange  --%>
																			<c:when test="${issuerRepsObj.haveUserId == 'N' && isEmailActivation == true && (exchangeState == 'ID' || exchangeState == 'NV' || exchangeState == 'MN')}">
																				<a href="<c:url value='/admin/issuer/representative/sendActivationlink/${encIssuerId}?repId=${encIssuerRepId}'/>"><spring:message  code='label.sendactivatelink'/></a>
																			</c:when>
																		</c:choose>	
																	</li>
																</c:if>
															</ul>
														</c:when>
														<c:otherwise>
															<span class="dropdown-toggle" id="dLabel" role="button" data-toggle="dropdown" data-target="#">
																<i class="icon-cog"></i>
															</span>
														</c:otherwise>
													</c:choose>
												</div>
											</div>
										</td>
									</tr>
								</c:forEach>
							</table>
							<div class="center">
							 <dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
							 </div>
						</c:when>
						<c:otherwise>
							<div class="alert alert-info"><spring:message code='label.recordsNotFound'/></div>
						</c:otherwise>
					</c:choose>
				</div>
			</div><!-- end of .span9 -->
		</div>
	<!--  end of row-fluid -->
</div>

<div id="dialog-modal" style="display: none;">
  <p>
  	<c:if test="${not empty sessionRepresentativeName}">
    	Representative Name: ${sessionRepresentativeName} <br/>
	</c:if>
	<c:if test="${not empty sessionDelegationCode}">
    	Delegation Code is: ${sessionDelegationCode} <br/>
	</c:if>
	<c:if test="${not empty generateactivationlinkInfo}">
    	${generateactivationlinkInfo}
	</c:if>
	<c:if test="${not empty ERROR}">
    	${Error}
	</c:if>
  </p>
</div>
<div id="dialog-modal_error" style="display: none;">
<p>Message: ${sessionErrorInfo}</p>
</div>

<script type="text/javascript">
$(document).ready(function()
{
	
	if('${sessionDelegationCode}' != null && '${sessionDelegationCode}' != '' && '${sessionRepresentativeName}' != null && '${sessionRepresentativeName}' != '')
	{
		 $( "#dialog-modal" ).dialog({
		     modal: true,
			 title: "<spring:message  code='label.ahbxInfo'/>",
			 buttons: {
			   Ok: function() {
			    $( this ).dialog( "close" );
			   }
			   }
		    });
	}
	else if('${sessionErrorInfo}' != null && '${sessionErrorInfo}' != '')
	{
		$( "#dialog-modal_error" ).dialog({
		      height: 140,
		      modal: true,
		      title: "<spring:message  code='label.ahbxInfo'/>",
		      buttons: {
					Ok: function() {
					$( this ).dialog( "close" );
					}
					}
		    });			
	}
	else if('${generateactivationlinkInfo}' != null && '${generateactivationlinkInfo}' != '')
	{
		 $( "#dialog-modal" ).dialog({
		  		modal: true,
		    	title: "<spring:message  code='label.registrationStatus'/>",
		    	buttons: {
		    	Ok: function() {
		    		$( this ).dialog( "close" );
		    	}
		    	}
		    });
	}
	else if('${ERROR}' != null && '${ERROR}' != '')
	{
		 $( "#dialog-modal" ).dialog({
		  		modal: true,
		    	title: "Permission Error",
		    	buttons: {
		    	Ok: function() {
		    		$( this ).dialog( "close" );
		    	}
		    	}
		    });
	}
});

function doSuspend(firstName, lastName, repId, issuerId){
	var didConfirm = confirm("Are you sure you want to suspend the authorized representative '" + firstName + " " + lastName + "' ?");
	if (didConfirm == true) {
		location.href="<c:url value='/admin/issuer/representative/manage/" + issuerId + "?action=Inactive&repId=" + repId + "'/>";
	} else {
		return false;
	}
}
</script>
