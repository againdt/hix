import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'capFirst'
})
export class CapFirstPipe implements PipeTransform {

    transform(text: string): string {
        if (typeof text !== "string") {
            //console.warn(`cap first pipe only accepts string as input, not ${typeof text}`);
            return text;
        }
        if (text.length === 0) {
            //console.warn(`cap first pipe receives an empty string`)
            return text;
        }
        return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
    }

}
