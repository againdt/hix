/**
 * 
 */
package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrlPopulateExtEnrollmentService;

/**
 * @author negi_s
 *
 */
public class EnrlUpdateApplicantsTask implements Tasklet {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlUpdateApplicantsTask.class);
	private EnrlPopulateExtEnrollmentService enrlPopulateExtEnrollmentService;
	long jobExecutionId = -1;

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

		if (null != enrlPopulateExtEnrollmentService) {
			LOGGER.trace("EnrlUpdateApplicantsTask calling EnrollmentReconciliationService.matchAndUpdateApplicants");
			try {
				enrlPopulateExtEnrollmentService.matchAndUpdateApplicants();
			} catch (Exception e) {
				LOGGER.error("Error updating applicants", e);
				throw new RuntimeException("Error updating applicants", e);
			}
		} else {
			LOGGER.error("EnrlPopulateExtEnrollmentService is null ");
			throw new RuntimeException("EnrlPopulateExtEnrollmentService is null");
		}

		return RepeatStatus.FINISHED;
	}

	public EnrlPopulateExtEnrollmentService getEnrlPopulateExtEnrollmentService() {
		return enrlPopulateExtEnrollmentService;
	}

	public void setEnrlPopulateExtEnrollmentService(EnrlPopulateExtEnrollmentService enrlPopulateExtEnrollmentService) {
		this.enrlPopulateExtEnrollmentService = enrlPopulateExtEnrollmentService;
	}
}
