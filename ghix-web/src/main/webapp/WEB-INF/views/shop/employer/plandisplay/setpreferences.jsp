<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>


<style type="text/css">

.modal {
	width: 760px;
}

h5 {
margin-bottom:0
}

form label.error {
float:none;
}
</style>
	
<div class="gutter10">
    
       <div class="row-fluid" id="titlebar">
       		<div class="gutter10" >
				<div class="span3">
					<div class="subnav">
						<ul class="nav nav-pills">
							<li class="dropdown"><a
								href="<c:url value="/shop/employer/plandisplay/coverageoptions" />">Back</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="span9">
					<h3 class="pull-left">Define your preferences to find the right
						set of plans for your employees</h3>
				</div>
			</div>
     	</div><!--titlebar-->

    <div class="row-fluid">


		<div class="span3" id="sidebar">
			<div class="gutter10">
				<p>These questions will help in filtering sorting plans on the
					exchange to match your unique needs. If you wish to skip the
					questions, please click on the Next button to view all plans</p>
			</div>
		</div>
		<!--sidebar-->
		
		<div class="span9 pull-right" id="rightpanel">
        <div id="planSelect" class="tabbedSections">
        
        	<div class="gutter10">

				<form class="form" id="spfrm" name="spfrm" 	action="<c:url value="/shop/employer/plandisplay/setpreferences" />"	method="POST">
          <df:csrfToken/>
                    <div class="accordion" id="accordion2">
                              <div class="accordion-group">
                                 <div class="accordion-heading">
                                  <h4> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo" data-original-title=""> <i class="icon-chevron-down"></i> How important are the following attributes of health insurance to you?</a></h4>
                                </div>
                                     <div style="height: 0px;" id="collapseTwo" class="accordion-body collapse">
                                      <div class="accordion-inner">
                                      <!--iframe load-->
                                      <!--	<iframe src="slideriFrame.html" width="100%" frameborder="0" height="320px" scrolling="no"></iframe>-->
                                        <div class="gutter10">
                                            <h5>Affordable Monthly Premium</h5>
                                            <div id="Premium_error"  class="help-block"></div>
                                           <div class="controls">
                                              <label class="label-inline">
                                                <input type="radio"  value="not_important" id="Premium"  name="Premium" <c:if test="${planPref.Premium == 'not_important'}"> checked="checked" </c:if> />
                                                Not Important
                                              </label>
                                              <label class="label-inline">
                                                <input type="radio" value="slight_important" id="Premium"  name="Premium" <c:if test="${planPref.Premium == 'slight_important'}"> checked="checked" </c:if>  />
                                                Slightly Important
                                              </label>
                                                <label class="label-inline">
                                                <input type="radio" value="very_important" id="Premium"  name="Premium" <c:if test="${planPref.Premium == 'very_important'}"> checked="checked" </c:if> />
                                                Very Important
                                              </label>
                                                   <label class="label-inline">
                                                <input type="radio" value="most_important" id="Premium"  name="Premium" <c:if test="${planPref.Premium == 'most_important'}"> checked="checked" </c:if> />
                                                Most Important
                                              </label>
                                            </div>
                                   
                        				</div>
                                        
                       				 <div class="gutter10">
                                      <h5>Deductible</h5>
                                      <div id="Deductible_error"  class="help-block"></div>
                                   		 <div class="controls">
                                              <label class="label-inline">
                                                <input type="radio"  value="not_important" id="Deductible"  name="Deductible" <c:if test="${planPref.Deductible == 'not_important'}"> checked="checked" </c:if> />
                                                Not Important
                                              </label>
                                              <label class="label-inline">
                                                <input type="radio" value="slight_important" id="Deductible"  name="Deductible" <c:if test="${planPref.Deductible == 'slight_important'}"> checked="checked" </c:if>  />
                                                Slightly Important
                                              </label>
                                                <label class="label-inline">
                                                <input type="radio" value="very_important" id="Deductible"  name="Deductible" <c:if test="${planPref.Deductible == 'very_important'}"> checked="checked" </c:if> />
                                                Very Important
                                              </label>
                                                   <label class="label-inline">
                                                <input type="radio" value="most_important" id="Deductible"  name="Deductible" <c:if test="${planPref.Deductible == 'most_important'}"> checked="checked" </c:if> />
                                                Most Important
                                              </label>
                                            </div>
                                    
                       				 </div>
                       				<div class="gutter10">
                                      <h5>Wide choice of in-network providers</h5>
                                     <div id="Network_error"  class="help-block"></div>
                                   		 <div class="controls">
                                              <label class="label-inline">
                                                <input type="radio"  value="not_important" id="Network"  name="Network" <c:if test="${planPref.Network == 'not_important'}"> checked="checked" </c:if> />
                                                Not Important
                                              </label>
                                              <label class="label-inline">
                                                <input type="radio" value="slight_important" id="Network"  name="Network" <c:if test="${planPref.Network == 'slight_important'}"> checked="checked" </c:if>  />
                                                Slightly Important
                                              </label>
                                                <label class="label-inline">
                                                <input type="radio" value="very_important" id="Network" name="Network" <c:if test="${planPref.Network == 'very_important'}"> checked="checked" </c:if> />
                                                Very Important
                                              </label>
                                                   <label class="label-inline">
                                                <input type="radio" value="most_important" id="Network"  name="Network" <c:if test="${planPref.Network == 'most_important'}"> checked="checked" </c:if> />
                                                Most Important
                                              </label>
                                          </div>
                       				 </div>
                        		</div>
                     
                     
                 </div><!--inner-->
            </div><!--accordion-body-->
     
          <!--#1-->

   
    
          <div class="accordion-group">
        <div class="accordion-heading">
              <h4> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" data-original-title=""> <i class="icon-chevron-down"></i> Providers</a> </h4>
            </div>
        <div style="height: 0px;" id="collapseOne" class="accordion-body collapse">
              <div class="accordion-inner">
            
                  <fieldset>
             
                <div class="control-group" style="margin-bottom:0">
                      <label class="control-label" style="width:auto;margin-right:20px">Are there any providers (doctors, clinics or hospitals) you'd like to make available to your employees?</label>
                      <div class="controls">
                    <label class="label-inline">
                          <input type="radio" name="dcoption" value="option1" id="dryes">
                          Yes <!-- <a rel="popover" data-content="You will be able to search and find the doctors you currently visit." data-original-title="Find Doctors" href="#"><span class="label">?</span></a>--> </label>
                    <label class="label-inline">
                          <input type="radio" name="dcoption" value="option1" id="drno">
                          No  </label>
                  </div>
                    </div>
                <div id="drfind" style="display:none">
                      <label class="control-label">Provider's Name</label>
                      <div class="controls">
                   		 <input type="text" class="span3" placeholder="Enter Doctor's Name">
                   		 <a class="" data-toggle="modal" href="#myModal" >Add More</a> 
                      </div>
                      <div class="controls"> <a class="btn" data-toggle="modal" href="#myModal" >Find Doctors</a> </div>
                     	 <div class="modal hide fade" id="myModal">
                    	<div class="modal-header"> <a data-dismiss="modal" class="close">�</a>
                          <h3>Find Your Doctor</h3>
                        </div>
                    <div class="modal-body">
                          <table class="table table-striped">
                        <tbody>
                              <tr>
                            <td>1</td>
                            <td class="span3"><img src="img/map.png" alt="Map"/>
                                  <br>
                                  <small><a style="color:#0000FF;text-align:left" href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=S+tempe+ct+CO+80012&amp;aq=&amp;sll=39.594185,-104.726958&amp;sspn=0.003286,0.006287&amp;ie=UTF8&amp;hq=&amp;hnear=S+Tempe+Ct,+Aurora,+Arapahoe,+Colorado+80016&amp;t=m&amp;ll=39.584656,-104.72683&amp;spn=0.019844,0.025578&amp;z=13&amp;iwloc=A" data-original-title="">View Larger Map</a></small></td>
                            <td><strong>Christopher Burrowes M.D.</strong><br>
                                  Arapahoe CO 80012</td>
                            <td><label class="checkbox">
                                <input type="checkbox" value="option1" name="optionsCheckboxList1">
                                Select this doctor </label></td>
                          </tr>
                              <tr>
                            <td>2</td>
                            <td><img src="img/map.png" alt="Map"/>
                                  <br>
                                  <small><a style="color:#0000FF;text-align:left" href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=S+tempe+ct+CO+80012&amp;aq=&amp;sll=39.594185,-104.726958&amp;sspn=0.003286,0.006287&amp;ie=UTF8&amp;hq=&amp;hnear=S+Tempe+Ct,+Aurora,+Arapahoe,+Colorado+80016&amp;t=m&amp;ll=39.584656,-104.72683&amp;spn=0.019844,0.025578&amp;z=13&amp;iwloc=A" data-original-title="">View Larger Map</a></small></td>
                            <td><strong>Sophie Burrowes M.D.</strong><br>
                                  Arapahoe CO 80012</td>
                            <td><label class="checkbox">
                                <input type="checkbox" value="option1" name="optionsCheckboxList1">
                                Select this doctor </label></td>
                          </tr>
                            </tbody>
                      </table>
                        </div>
                    <div class="modal-footer"> <a data-dismiss="modal" class="btn" href="#">Close</a> <a data-dismiss="modal" class="btn btn-primary" href="#" id="dradd">Continue</a> </div>
                  </div>
                    </div>
                    
                    <div id="drshow" style="display:none;margin-top:20px">
                    	<p>Your health plan should be accepted by :</p>
                        <p><strong>Christopher Burrowes M.D.</strong></p>
                          <p><strong>Sophie Burrowes  M.D.</strong></p>
                          
                <!--            <div class="form-actions" style="" >
                      <button type="submit" class="btn btn-primary">Next Question</button>
                    </div>-->
                    </div>
                <!--HIDE-->
              <!--  
                <div class="form-actions" style="display:none" id="next">
                      <button type="submit" class="btn btn-primary">Next Question</button>
                    </div>-->
              </fieldset>
               
          </div>
            </div>
      </div>
          <!--#1-->
         
        
          <div class="accordion-group">
       		 <div class="accordion-heading">
              <h4> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6" data-original-title=""> <i class="icon-chevron-down"></i> Prescription Drugs</a></h4>
            </div>
                <div style="height: 0px;" id="collapse6" class="accordion-body collapse">
                  <div class="accordion-inner">
                        <p>Enter names of prescriptions drugs your employees may need coverage for:</p>
                        
            <script type="text/javascript">
                    $(document).ready(function() {
                      
                    var scntDiv = $('#p_scents');
                    var i = $('#p_scents p').size() + 1;
                    
                    $('#addScnt').live('click', function() {
                            $('<p><label for="p_scnts"><input type="text" id="p_scnt" size="20" name="p_scnt_' + i +'" value="" placeholder="Enter Drug Name Here" /></label> <a href="#" id="remScnt">Remove</a></p>').appendTo(scntDiv);
                            i++;
                            return false;
                    });
                    
                    $('#remScnt').live('click', function() {
                            if( i > 2 ) {
                                    $(this).parents('p').remove();
                                    i--;
                            }
                            return false;
                    });
            
                    });
                </script>
                         <a href="#" id="addScnt">Add Drug Name</a>
                            <div id="p_scents">
                                <p>
                                    <label for="p_scnts"><input type="text" id="p_scnt" size="20" name="p_scnt" value="" placeholder="Enter Drug Name" /></label>
                                </p>
                            </div>
                     </div>
       			</div>
 			 </div>
         
        	</div>
  			
    
    		<div class="form-actions">
            	<div class="pull-right">
            		<a class="btn" type="submit" onclick="formSubmit();">Save</a>
                	<a class="btn btn-primary " type="submit" onclick="formSubmit();">Next &rarr;View Plans</a>
           		 </div>
        	</div>
        	
          </form>
          
          </div><!-- gutter10 --> 
    
    </div><!-- planSelect -->
              </div>  <!-- span9 --> 
          </div> <!-- row-fluid --> 
      </div> <!-- gutter10 --> 


<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript">

$(document).ready(function(){

	$(".collapse").collapse();
	
	
	$("#dryes").change(function(i){ 
		$('#drfind').toggle();
	});
	$("#dryes").click(function(i){
		$('#next').css("display","none");
	});
	$("#drno").click(function(i){
		$('#next').css("display","block");
		$('#drfind').css("display","none");
		$('#drshow').css("display","none");
	});
	$("#dradd").click(function(i){
		$('#drshow').css("display","block");
	});
});


$('a.btn').popover({
	placement:'left',
});
$('#rightpanel a').popover({
	placement:'left',
});
	
function formSubmit(){
	
	$('#spfrm').submit();
}


var validator = $("#spfrm").validate({ 
	rules : {
		Premium : {required : true},
		Deductible : {required : true},
		Network : {required : true}
	},
	messages : {
		Premium : { required : "<span><em class='excl'>!</em> Please Select any option from Affordable Monthly Premium</span>"},
		Deductible : { required : "<span><em class='excl'>!</em> Please Select any option from Deductible</span>"},
		Network : { required : "<span><em class='excl'>!</em> Please Select any option from Wide choice of in-network providers</span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error');
	} 
});	
	
</script>
