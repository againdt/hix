<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
    <%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df" %>
    <%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
    <%@page import="com.getinsured.hix.platform.util.GhixConstants"%>

    <c:set var="calheersEnv" value='<%=GhixConstants.CALHEERS_ENV%>'></c:set>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="<c:url value="/resources/css/tiles.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/resources/css/planselection.css"/>" type="text/css">

    <style>
    div {
    position: static;
    }
    .gutter50-t {
      padding-top: 50px;
    }
    @media screen and (max-width:768px){
    .gutter50-t {
    padding-top: 0px;
    }}
    .view-broker {
    top: 8px!important;
    z-index: 10;
    }
    /* .topnav {
    position: fixed;
    width: 940px;
    height: 40px;
    z-index: 1000;
    top: 0;
    } */
    #sort .dk_toggle {width:180px !important; padding: 8px 6px;}
    .row-fluid .span4 {
    margin-top: 10px;
    }
    .top-nav {
    top: 30px;

    }
    .uppercase {
    text-transform: uppercase;
    }

    .disclaimer {
    border-top: 1px solid #eee;
    }

    .disclaimer-text {
    color: #656360;
    }

    #sort h3 {line-height: 10px;}
    label[for="sortBy"] {font-size: 18px !important;}

    .compare-widget-show-tab-position {
    right: 0;
    top: 22%;
    position: fixed;
    bottom: 0;
    left: auto;
    width: 170px;
    margin: 0 auto;
    height: 0;
    z-index: 1000;
    background-color: transparent;
    }
    /* .compare-tray-tab-show {
    top: 28.7%;
    width: 29.7em;
    height: 100%;
    position: fixed;
    right: 15px;
    }
    .compare-tray-tab-show .tab {
    color: #fff;
    padding: 10px;
    background-color: #554D57;
    }*/

    .compare-tray-content-wrap{
    -webkit-transition: all .5s ease;
    -moz-transition: all .5s ease;
    transition: all .5s ease;
    }
    .tab.phix-theme {
    display: block;
    height: initial;
    opacity: 1;
    animation: FadeIn .8s ease-in-out;
    }
    .compare-tray-content-wrap.active{
    display: block;
    height: initial;
    opacity: 1;
    animation: FadeIn .8s ease-in-out;
    }
    .tab.tab-show {
    opacity: 1;
    animation: FadeIn .8s ease-in-out;
    }
    @keyframes FadeIn {
    0% {
    opacity: 0;

    }
    100% {
    opacity: 1;

    }
    }
    @keyframes FadeOut {
    0% {
    opacity: 1;

    }
    /* 99% {
    opacity: 0;
    height: initial;
    }*/
    100% {

    opacity: 0;
    }
    }

    @keyframes SlideOut {
    0% {
    opacity: 0;
    width:1px;

    }
    100% {
    opacity: 1;
    width:initial;

    }
    }
    @keyframes SlideIn {
    0% {
    opacity: 1;
    width:initial;

    }

    100% {

    opacity: 0;
    width:1px;
    }
    }
    <c:if test="${iframe == 'yes'}">
        .ps-sidebar{
        top: 0;
        }
    </c:if>
    .metal-tier .checkbox small {color: #3e3e3e;}
    .filters {margin-bottom: 20px;}
    </style>
    <script type="text/javascript">
    var plan_display = new Array();

    plan_display['pd.label.title.removeDentalPlan'] = "<spring:message code='pd.label.title.removeDentalPlan' javaScriptEscape='true'/>";
    plan_display['pd.label.title.add'] = "<spring:message code='pd.label.title.add' javaScriptEscape='true'/>";
    plan_display['pd.label.title.showCompare'] = "<spring:message code='pd.label.title.showCompare' javaScriptEscape='true'/>";
    plan_display['pd.label.title.hideCompare'] = "<spring:message code='pd.label.title.hideCompare' javaScriptEscape='true'/>";
    plan_display['pd.label.title.removeDental'] = "<spring:message code='pd.label.title.removeDental' javaScriptEscape='true'/>";
    plan_display['pd.label.selectOneMember'] = "<spring:message code='pd.label.selectOneMember' javaScriptEscape='true'/>";
    plan_display['pd.label.adultDentalSelection'] = "<spring:message code='pd.label.adultDentalSelection' javaScriptEscape='true'/>";
    plan_display['pd.label.selectOnePlan'] = "<spring:message code='pd.label.selectOnePlan' javaScriptEscape='true'/>";
    plan_display['pd.label.dentalPlanAlreadyAdded'] = "<spring:message code='pd.label.dentalPlanAlreadyAdded' javaScriptEscape='true'/>";
    plan_display['pd.label.removeThisPlan'] = "<spring:message code='pd.label.removeThisPlan' javaScriptEscape='true'/>";

    plan_display['pd.label.selectPlanAlreadyInCart'] = "<spring:message code='pd.label.selectPlanAlreadyInCart' javaScriptEscape='true'/>";
    plan_display['pd.label.goToCart'] = "<spring:message code='pd.label.goToCart' javaScriptEscape='true'/>";
    plan_display['pd.label.title.addPlan'] = "<spring:message code='pd.label.title.addPlan' javaScriptEscape='true'/>";
    plan_display['pd.label.title.comparePrevPlan'] = "<spring:message code='pd.label.title.comparePrevPlan' javaScriptEscape='true'/>";
    plan_display['pd.label.title.yourCurrentPlan'] = "<spring:message arguments='${coverageYear}' code='pd.label.title.yourCurrentPlan' javaScriptEscape='true'/>";
    plan_display['pd.label.errorProcessingData'] = "<spring:message code='pd.label.errorProcessingData' javaScriptEscape='true'/>";
    plan_display['pd.label.title.compare'] = "<spring:message code='pd.label.title.compare' javaScriptEscape='true'/>";
    plan_display['pd.label.title.comparePlans'] = "<spring:message code='pd.label.title.comparePlans' javaScriptEscape='true'/>";
    plan_display['pd.label.title.showCompare'] = "<spring:message code='pd.label.title.showCompare' javaScriptEscape='true'/>";
    plan_display['pd.label.viewDetails'] = "<spring:message code='pd.label.viewDetails' javaScriptEscape='true'/>";
    plan_display['pd.label.title.addToCart'] = "<spring:message code='pd.label.title.addToCart' javaScriptEscape='true'/>";
    plan_display['pd.label.title.detail'] = "<spring:message code='pd.label.title.detail' javaScriptEscape='true'/>";
    plan_display['pd.label.text.remove'] = "<spring:message code='pd.label.text.remove' javaScriptEscape='true'/>";
    plan_display['pd.label.memberPremium.title'] = "<spring:message code='pd.label.memberPremium.title' javaScriptEscape='true'/>";
    plan_display['pd.label.memberPremium.memberAge'] = "<spring:message code='pd.label.memberPremium.memberAge' javaScriptEscape='true'/>";
    plan_display['pd.label.memberPremium.total'] = "<spring:message code='pd.label.memberPremium.total' javaScriptEscape='true'/>";
    plan_display['pd.label.memberPremium.aptc'] = "<spring:message code='pd.label.memberPremium.aptc' javaScriptEscape='true'/>";
    plan_display['pd.label.memberPremium.netPremium'] = "<spring:message code='pd.label.memberPremium.netPremium' javaScriptEscape='true'/>";
    plan_display['pd.label.text.addThisPlan'] = "<spring:message code='pd.label.text.addThisPlan' javaScriptEscape='true'/>";
    plan_display['pd.label.script.networkType.hmo'] = "<spring:message code='pd.label.script.networkType.hmo' javaScriptEscape='true'/>";
    plan_display['pd.label.script.networkType.ppo'] = "<spring:message code='pd.label.script.networkType.ppo' javaScriptEscape='true'/>";
    plan_display['pd.label.script.networkType.epo'] = "<spring:message code='pd.label.script.networkType.epo' javaScriptEscape='true'/>";
    plan_display['pd.label.script.networkType.pos'] = "<spring:message code='pd.label.script.networkType.pos' javaScriptEscape='true'/>";
    plan_display['pd.label.script.networkType.indemnity'] = "<spring:message code='pd.label.script.networkType.indemnity' javaScriptEscape='true'/>";
    plan_display['pd.label.script.deductible.andLess'] = "<spring:message code='pd.label.script.deductible.andLess' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.one'] = "<spring:message code='pd.label.tooltip.qualityRatings.1.1' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.two'] = "<spring:message code='pd.label.tooltip.qualityRatings.2' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.three'] = "<spring:message code='pd.label.tooltip.qualityRatings.3' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.threedotone'] = "<spring:message code='pd.label.tooltip.qualityRatings.3.1' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.threedottwo'] = "<spring:message code='pd.label.tooltip.qualityRatings.3.2' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.four'] = "<spring:message code='pd.label.tooltip.qualityRatings.4' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.fourdotone'] = "<spring:message code='pd.label.tooltip.qualityRatings.4.1' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.fourdottwo'] = "<spring:message code='pd.label.tooltip.qualityRatings.4.2' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.five'] = "<spring:message code='pd.label.tooltip.qualityRatings.5' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.fivedotone'] = "<spring:message code='pd.label.tooltip.qualityRatings.5.1' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.fivedottwo'] = "<spring:message code='pd.label.tooltip.qualityRatings.5.2' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.six'] = "<spring:message code='pd.label.tooltip.qualityRatings.6' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.seven'] = "<spring:message code='pd.label.tooltip.qualityRatings.7' javaScriptEscape='true'/>";
    plan_display['pd.label.tooltip.qualityRatings.eight'] = "<spring:message code='pd.label.tooltip.qualityRatings.8' javaScriptEscape='true'/>";

    plan_display['pd.label.title.continueToHealthPlans'] = "<spring:message code='pd.label.title.continueToHealthPlans' javaScriptEscape='true'/>";
    plan_display['pd.label.title.continueToDentalPlans'] = "<spring:message code='pd.label.title.continueToDentalPlans' javaScriptEscape='true'/>";
    plan_display['pd.label.title.continueToCart'] = "<spring:message code='pd.label.title.continueToCart' javaScriptEscape='true'/>";
    plan_display['pd.label.commontext.members'] = "<spring:message code='pd.label.commontext.members' javaScriptEscape='true'/>";
    plan_display['pd.label.commontext.member'] = "<spring:message code='pd.label.commontext.member' javaScriptEscape='true'/>";
    plan_display['pd.label.title.for'] = "<spring:message code='pd.label.title.for' javaScriptEscape='true'/>";
    plan_display['url.showCart'] = "<c:url value='/private/showCart'/>";
    plan_display['pd.label.quotedMembersString.and'] = "<spring:message code='pd.label.quotedMembersString.and' javaScriptEscape='true'/>";

    </script>
    <input type="hidden" name="personDataListJson" id="personDataListJson" value='${personDataListJson}'/>
    <input type="hidden" id="loggedInUser" name="loggedInUser" value="${loggedInUser}"/>
    <input type="hidden" id="isIndividual" name="isIndividual" value="${isIndividual}"/>
    <script>
    $(document).ready(function(){
    var persons = JSON.parse(document.getElementById("personDataListJson").value);
    var quotingPersons=[];
    var memberInfo = [];
    var memberHTML = '';
    var memberCount = persons.length;
    var noName = true;

    if(($("#insuranceType").val() == 'DENTAL')){
        setPageTitle('<spring:message code="pd.label.title.dentalPlans"/>');
    	persons.forEach(function(person) {
    		if( person.seekCoverage=='Y')
    			{
    				quotingPersons.push(person);
    			}
    	});
	}
	else
		{
            setPageTitle('<spring:message code="pd.label.title.healthPlans"/>');
		quotingPersons = persons;
		}
    quotingPersons.forEach(function(person, index) {
    	
    if(person.firstName) {
    	memberHTML = '<span class="member_name">' +person.firstName+", "+ '</span>';
		if(persons.length == 1) {
		memberHTML = '<span class="ps-eligibility__member_name" id="memInfo">'+person.firstName+'</span>';
		}
		if(persons.length >= 2) {
			if (index !== persons.length-1) {
			memberHTML = '<span class="ps-eligibility__member_name" id="memInfo">' +person.firstName+", "+ '</span>';
			} else {
			memberHTML = '<span class="ps-eligibility__member_name" id="memInfo"> ' + plan_display['pd.label.quotedMembersString.and'] + ' ' +person.firstName+'</span>';
			}
		}
		noName = false;
		$(".member-info").append(memberHTML);
    } else {
    noName = true;
    }
    });
    if (noName) {
    if(memberCount > 1) {
    memberHTML = '<span class="ps-eligibility__member_name" id="memInfo">'+ memberCount + ' '+
    plan_display['pd.label.commontext.members']+'</span>';
    } else {
    memberHTML = '<span class="ps-eligibility__member_name" id="memInfo">' + memberCount + ' '+
    plan_display['pd.label.commontext.member']+'</span>';
    }
    $(".member-info").append(memberHTML);
    }
    });
    </script>

    <div data-ng-app="PlanView" data-ng-controller="ProviderMapController as pm">
    <div id="map-modal"
    modal-show
    class="modal fade"
    tabindex="-1"
    role="dialog"
    aria-hidden="true"
    url="<c:url value='/provider/doctors/search'/>" style="display: none;">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#120;</button>
    <h3>
    <spring:message code="pd.provider.label.physiciansIn"/>
    <spring:message arguments='{{pm.distance}}' code="pd.provider.label.distance"/>
    <spring:message code="pd.provider.label.of"/>
    {{pm.zip}}
    </h3>
    </div>
    <div class="provider-map modal-body">
    <div class="provider-map__sidebar">
    <doctors-sidebar></doctors-sidebar>
    <provider-map-pagination></provider-map-pagination>
    </div>
    <div class="provider-map__map">
    <map ng-if="pm.showProviderMap"></map>
    </div>
    </div>
    </div>

    <!-- map template -->
    <script type="text/ng-template" id="map.html">
        <div class="google-map">
            <div id="google-map-canvas"></div>
            <div class="conceal">
                <infowindow></infowindow>
            </div>
        </div>
    </script>
    <!-- doctor sidebar template -->
    <script type="text/ng-template" id="doctorsSidebar.html">
        <div class="doctor-sb">
            <div class="doctor-sb__doctor" data-ng-repeat="doctor in ds.getDoctors()" data-ng-click="ds.onClick(doctor, $index)">
                <div class="doctor-sb__index">
                    <span class="doctor-sb__pin">{{ ds.getStart() + $index }}</span><span class="doctor-sb__detail__name"><strong data-ng-bind="doctor.getName()"></strong></span>
                </div>
                <div class="doctor-sb__detail">
                    <!-- p class="doctor-sb__detail__p"><strong data-ng-bind="doctor.getName()"></strong></p> -->
                    <p class="doctor-sb__detail__p"><i data-ng-bind="doctor.getSpeciality()"></i></p>
                    <p class="doctor-sb__detail__p" data-ng-bind="doctor.getStreet()"></p>
                    <p class="doctor-sb__detail__p" data-ng-bind="doctor.getCityStateZip()"></p>
                </div>
            </div>
        </div>
    </script>
    <!-- infowindow template -->
    <script type="text/ng-template" id="infowindow.html">
        <div class="infowindow">
            <p><strong data-ng-bind="vm.getName()"></strong></p>
            <p><i data-ng-bind="vm.getSpeciality()"></i></p>
            <p data-ng-bind="vm.getStreet()"></p>
            <p data-ng-bind="vm.getCityStateZip()"></p>
        </div>
    </script>
    <script type="text/ng-template" id="pagination.html">
        <div class="provider-pagination" data-ng-style="pg.getStyle()">
            <ul class="provider-pagination__links">
                <li data-ng-repeat="page in pg.getPages()">
                    <a href="#" data-ng-bind="page" class="provider-pagination__option" data-ng-click="pg.selectPage(page)" data-ng-class="{'current': page === pg.getCurrentPage()}"></a>
                </li>
                <li>
                    <a class="provider-pagination__arrow" ng-if="!pg.isFirstPage()" ng-click="pg.selectPrePage()"><i class="icon-caret-left"></i></a>
                </li>
                <li>
                    <a class="provider-pagination__arrow" ng-if="!pg.isEndPage()" ng-click="pg.selectNextPage()"><i class="icon-caret-right"></i></a>
                </li>

            </ul>
        </div>
    </script>
    </div>


    <section class="plan-selection">
    <div class="js-planselection-loading-add planselection-loading">
        <h3 class="planselection-loading__add-plan-header"><spring:message code="pd.label.commontext.addingPlans" htmlEscape="true"/></h3>
        <img src="<c:url value="../resources/images/hourglass.svg"/>" class="planselection-loading__loading-image" alt="loading">
        <p class="planselection-loading__sub-description"><spring:message code="pd.label.commontext.pleaseWait" htmlEscape="true"/></p>
    </div>
    <div class="js-planselection-loading-remove planselection-loading">
        <h3 class="planselection-loading__add-plan-header"><spring:message code="pd.label.commontext.removingPlans" htmlEscape="true"/></h3>
        <img src="<c:url value="../resources/images/hourglass.svg"/>" class="planselection-loading__loading-image" alt="loading">
        <p class="planselection-loading__sub-description"><spring:message code="pd.label.commontext.pleaseWait" htmlEscape="true"/></p>
    </div>

    <div class="ps-top-links">
        <div class="ps-top-links__left gutter50-t">
            <c:if test="${loggedInUser == null && stateCode == 'CA'}">
                <a href="https://${calheersEnv}/lw-shopandcompare/" class="cp-link border-b"><i class="icon-caret-left"></i>&nbsp;
                <spring:message code="pd.label.title.backToShopAndCompare"/></a>
            </c:if>
            <c:if test="${loggedInUser != null && ((stateCode == 'CA' && isIndividual ) || (stateCode == 'MN' && userActiveRoleName != 'issuer_representative'))}">
                <a href="<c:url value="/indportal#"/>" class="cp-link border-b"><i class="icon-caret-left"></i>&nbsp;<spring:message
                    code="pd.label.title.backTodashboard"/></a>
            </c:if>
            <c:if test="${productType != 'D'}">
                <a href="preferences?insuranceType=${insuranceType}" class="cp-link plan-display-show border-b">
                <i class="icon-caret-left"></i>
                <spring:message code="pd.label.title.backToPreferences"/>
                </a>
            </c:if>

    <%-- <a href="#" class="back-to-all-plans-link cp-link plan-detail-show plan-compare-show hide">
        <i class="icon-caret-left"></i>
        <spring:message code="pd.label.title.backToAllPlans" htmlEscape="true"/>
    </a> --%>
            <span class="back-to-all-plans-link-container plan-detail-show plan-compare-show hide">
                <a href="#" class="back-to-all-plans-link-detail cp-link plan-detail-show border-b">
                    <i class="icon-caret-left"></i>
                    <spring:message code="pd.label.title.backToAllPlans" htmlEscape="true"/>
                </a>

                <a href="#" class="back-to-all-plans-link-compare cp-link plan-compare-show border-b">
                    <i class="icon-caret-left"></i>
                    <spring:message code="pd.label.title.backToAllPlans" htmlEscape="true"/>
                </a>
            </span><!-- detail/compare div to show Back to all plans on plan detail view -->
        </div>

    <div class="ps-top-links__right">
        <c:if test="${stateCode == 'CT'}">
            <button class="btn btn-primary" data-toggle="modal" data-target="#enrollNow" role="button"><spring:message
                code="pd.label.enrollNow"/></button>
        </c:if>

        <c:if test="${flowType == 'ISSUERVERIFICATION'}">
            <a class="btn btn-primary" data-toggle="modal" id="exportCSV"><spring:message
                code="pd.label.commontext.export"/></a>
        </c:if>
    </div>

    </div>

    <nav class="ps-nav plan-display-show">
    <ul class="nav nav-tabs ps-nav__links">
    <c:if test="${healthPlanPresent == 'N' && productType != 'D'}">
        <li class="ps-nav__list ${insuranceType == 'HEALTH'? 'ps-nav__list--active' : ''}">
        <a href="<c:url value="/private/planselection?insuranceType=HEALTH"/>" class="ps-nav__link">
        <c:if test="${insuranceType == 'HEALTH'}">
            <span id="filteredPlanCount" class="ps-nav__plan-count"></span>
        </c:if>
        <spring:message code="pd.label.title.healthPlans" htmlEscape="true"/>
        </a>
        </li>
    </c:if>

    <c:if test="${isSeekingCoverage != false && dentalPlanPresent == 'N'  && productType != 'H'}">
        <li class="ps-nav__list ${insuranceType == 'DENTAL'? 'ps-nav__list--active' : ''}">
        <a href="<c:url value="/private/planselection?insuranceType=DENTAL"/>" class="ps-nav__link">
        <c:if test="${insuranceType == 'DENTAL'}">
            <span id="filteredPlanCount" class="ps-nav__plan-count"></span>
        </c:if>
        <spring:message code="pd.label.title.dentalPlans" htmlEscape="true"/>
        </a>
        </li>
    </c:if>
    </ul>

    <c:if test="${stateCode != 'CT' && (flowType == 'PLANSELECTION' || flowType == 'PREELIGIBILITY')}">
        <button
        type="button"
        id="aid-cart"
        aria-label="Shopping Cart"
        class="btn btn-primary cp-btn cp-btn__cart"
        title="<spring:message code="pd.label.title.cart"/>">
        <i class="icon-shopping-cart"></i> <span id="planNumber"></span>
        </button>
    </c:if>
    </nav>


    <div class="ps-eligibility plan-display-show">
    <c:choose>
        <c:when test="${(maxAptc > 0 || maxStateSubsidy > 0)}">
            <div class="ps-eligibility__saving">
            <c:if test="${insuranceType != 'DENTAL'}">
                <div><h2 class="ps-eligibility__header ps-eligibility__inline" id="mem-header_health_01"><spring:message
                    code="pd.label.commontext.estimatedMonthlySaving"/></h2>
                    <c:choose>
                    <c:when test="${stateCode == 'CA' && maxStateSubsidy > 0}">
                        <a      class="ps-eligibility__amount"
                                href="#"
                                rel="tooltip"
                                role="tooltip"
                                data-html="true"
                                data-placement="bottom"
                                data-original-title="<table>
                                                    <tr>
                                                        <td><spring:message code="pd.label.commontext.federal" htmlEscape="false"/>
                                                        <td>=</td>
                                                        <td>${maxAptc}<td/>
                                                    </tr>
                                                    <tr>
                                                        <td><spring:message code="pd.label.commontext.state" htmlEscape="false"/>
                                                        <td>=</td>
                                                        <td>${maxStateSubsidy}<td/>
                                                    <tr>
                                                        <td><spring:message code="pd.label.commontext.total" htmlEscape="false"/>
                                                        <td>=</td>
                                                        <td>${maxAptc+maxStateSubsidy}<td/>
                                                    </tr>
                                                    </table>">
                            $<span class="numberToFormat">${maxAptc+maxStateSubsidy}</span><spring:message code="pd.label.title.perMonth"/>
                        </a>
                        </a>
                    </c:when>
                    <c:otherwise>
                        <a class="ps-eligibility__amount" data-original-title="<spring:message code="pd.label.tooltip.taxcredit.anonymous" htmlEscape="true"/>"
                        data-html="true" rel="tooltip" data-placement="right" href="#">
                            $<span class="numberToFormat">${maxAptc+maxStateSubsidy}</span><spring:message code="pd.label.title.perMonth"/>
                        </a>
                    </c:otherwise>
                    </c:choose>
                <span class="member-info ps-eligibility__paragraph">
                    <spring:message code="pd.label.title.for"/>&nbsp;
                </span>
                <span  class="supporting_text" id="inzip_01">
                    <spring:message code="pd.label.title.inzip"/>&nbsp;${zipcode}.</span>
                    <c:if test="${loggedInUser == null && insuranceType != 'DENTAL' && stateCode != 'CA'}">
                        <a href="<c:url value="/preeligibility"/>" class="cp-link border-b"><spring:message
                            code="pd.label.title.editFamilyInfo"/></a>
                    </c:if>
                </div>
            </c:if>

            <c:if test="${insuranceType == 'DENTAL'}">
                <div><h2 class="ps-eligibility__header ps-eligibility__inline" id="mem-header_dental_01"><spring:message
                    code="pd.label.commontext.dentalCoverage"/></h2>
                    <span class="member-info ps-eligibility__paragraph">
                        <spring:message code="pd.label.title.for"/>&nbsp;
                    </span>
                     <span id="inzip_02"><spring:message code="pd.label.title.inzip"/>&nbsp;${zipcode}.</span>
                    <c:if test="personCount > 1 && customGroupingOnOff != 'OFF' && flowType == 'PLANSELECTION' && loggedInUser != null}">
                        <a href="#" class="changeLink active" data-placement="bottom" data-html="false">
                            <spring:message code="pd.label.commontext.change"/>
                        </a>
                    </c:if>
                </div>
            </c:if>


            <p class="ps-eligibility__paragraph" id="cov_date_01">
            <c:choose>
                <c:when test="${flowType == 'PREELIGIBILITY'}">
                    <spring:message arguments='${coverageStartDate}' code="pd.label.title.message4.pre"/>
                </c:when>
                <c:otherwise>
                    <spring:message arguments='${coverageStartDate}' code="pd.label.title.message4.post"/>
                </c:otherwise>
            </c:choose>
            </p>
            </div>
        </c:when>
        <c:otherwise>
            <div class="ps-eligibility__saving">
                <div>
                    <c:if test="${insuranceType != 'DENTAL'}">
                        <h2 class="ps-eligibility__header ps-eligibility__inline" id="mem-header_health"><spring:message
                            code="pd.label.commontext.healthCoverage"/></h2>
                    </c:if>
                    <c:if test="${insuranceType == 'DENTAL'}">
                        <h2 class="ps-eligibility__header ps-eligibility__inline" id="mem-header_dental"><spring:message
                            code="pd.label.commontext.dentalCoverage"/></h2>
                    </c:if>
                    <p class="ps-eligibility__paragraph">
                        <span class="member-info">
                            <spring:message code="pd.label.title.for"/>&nbsp;
                        </span>
                        <span id="inzip_03">
                            <spring:message code="pd.label.title.inzip"/>&nbsp;${zipcode}.
                        </span>
                        <c:if test="${loggedInUser == null && insuranceType != 'DENTAL' && stateCode != 'CA'}">
                            <a href="<c:url value="/preeligibility"/>" id="cov_date_02" class="cp-link border-b"><spring:message
                            code="pd.label.title.editFamilyInfo"/></a>
                        </c:if>
                    </p>
                </div>
                <c:choose>
                    <c:when test="${flowType == 'PREELIGIBILITY'}">
                        <spring:message arguments='${coverageStartDate}' code="pd.label.title.message4.pre"/>
                    </c:when>
                    <c:otherwise>
                        <spring:message arguments='${coverageStartDate}' code="pd.label.title.message4.post"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </c:otherwise>
    </c:choose>

    <c:if test="${insuranceType == 'HEALTH'}">
        <div class="ps-eligibility__csr ps-csr-show hide">
        <h2 class="ps-eligibility__header ps-eligibility__inline"><spring:message code="pd.label.title.costShare"/></h2>
        <span class="cp-tile__metal-tier cp-tile__metal-tier--csr">
            <spring:message code="pd.label.title.csr"/>
            <a
                class="cp-tooltip__icon-link"
                href="#"
                rel="tooltip"
                data-html="true"
                data-placement="top"
                data-original-title="<spring:message code="pd.label.title.CSREligibletooltip"/>">
                <span class="aria-hidden">More Information</span>
                <i class="icon-question-sign cp-tile__icon"></i>
            </a>
            </span>
            <span class="ps-eligibility__csr-text"><spring:message code="pd.label.title.csrText"/>
        </span>
        </div>
    </c:if>
    </div>

    <hr class="plan-display-show ps-hr">

    <script>
    $('.numberToFormat').each(function(index,item){
    $(item).html(parseFloat($(item).html()).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
    });
    $(document).ready(function(){
    $('body').on('click', '#aid-continueHealthPlan', function(){
    window.location = "/hix/private/planselection?insuranceType=HEALTH";
    });
    $('body').on('click', '#aid-continueDentalPlan', function(){
    window.location = "/hix/private/planselection?insuranceType=DENTAL";
    });
    $('body').on('click', '.aid-continueCart', function(){
    window.location = "/hix/private/showCart";
    });
    $('body').on('click', '#aid-cart', function(){
    window.location = "/hix/private/showCart";
    });
    if(performance.navigation.type == 2){
    location.reload(true);
    }

    });
    </script>

    <div class="plan-selection__container">
    <aside class="ps-sidebar">

    <div id="sidebar" class="row-fluid ps-sidebar-toggler show-side-bar">
    &nbsp;Filters <span class="icon-chevron-right"></span>
    </div>

    <%-- plan compare --%>
    <div class="js-plan-compare-widget cp-compare-plans hide u-hide-xs" data-accesskey="c">
    <div class="cp-compare-plans__header js-compare-plans" data-show="true">
    <div class="cp-compare-plans__title-container">
    <span class="cp-compare-plans__title js-plan-compare-title">
    <spring:message code='pd.label.title.comparePlans'/>
    </span>
    <span>
    <span id="compareCount">0</span>
    <spring:message code='pd.label.title.comparePlansOf'/>
    3
    </span>
    </div>
    <div class="cp-compare-plans__collapse">
    <i class="icon-minus"></i>
    </div>
    </div>
    <div id="compareBox" class="cp-compare-plans__body">

    </div>
    <div class="cp-compare-plans__footer">
    <a type="button" title="<spring:message code='pd.label.title.comparePlan'/>" name="button"
    class="cp-compare-plans__cta compareButton gtm_compareButton" href="#compare"><spring:message
        code='pd.label.title.comparePlan'/></a>
    </div>
    </div>
    <%-- plan compare end --%>

    <%-- sort by --%>
    <div id="sort" class="ps-sidebar__group"></div>
    <%-- sort by end --%>

    <%-- filter by --%>
    <div id="filter" class="ps-sidebar__group hide">
    <fieldset>
    <legend class="ps-sidebar__group-header u-margin-0">
    <spring:message code="pd.label.dropdown.filterBy"/>
    </legend>


    <%-- preferred provider --%>
    <c:if test="${providersList.size() gt 0 && providerFilterConfig == 'ON'}">
        <div class="ps-sidebar__filter">
        <h3 class="ps-sidebar__filter-header">
        <c:if test="${stateCode == 'CA'}">
            <a
            data-original-title="
            <spring:message code='pd.label.preferences.note1' htmlEscape='true'/>
            <spring:message code='pd.label.preferences.note2' htmlEscape='true'/>"
            data-html="true"
            rel="tooltip"
            data-placement="bottom"
            href="#">
            <spring:message code="pd.label.title.preferredProvider"/>
            </a>
        </c:if>
        <c:if test="${stateCode != 'CA'}">
            <spring:message code="pd.label.title.preferredProvider"/>
        </c:if>
        </h3>

        <c:forEach var="provider" varStatus="status" items="${providersList}">
            <c:set var="pname" value="${fn:toLowerCase(provider.name)}"/>
            <div class="ps-form__check">
            <input
            type="checkbox"
            class="ps-form__check-input doctor_filter_checkbox"
            data-filter-label="${pname}"
            value="${provider.id}"
            id="${provider.id}"/>
            <label for="${provider.id}" class="ps-form__check-label txt-capitalize">
            ${pname}
            </label>
            </div>

        </c:forEach>

        </div>
    </c:if>
    <%-- preferred provider end --%>

    <%-- plan type --%>
    <div id="planTypeFilter" class="ps-sidebar__filter">
    <h3 class="ps-sidebar__filter-header">
    <spring:message code="pd.label.title.planType"/>
    </h3>
    </div>
    <%-- plan type end --%>


    <%-- plan features --%>
    <c:if test="${insuranceType != 'DENTAL'}">
        <div class="ps-sidebar__filter">
        <h3 class="ps-sidebar__filter-header">
        <spring:message code="pd.label.title.planFeatures"/>
        </h3>

        <div id="csrFilter" class="ps-form__check">
        <input
        type="checkbox"
        class="plantype_filter_csr gtm_filter ps-form__check-input"
        value="CS1"
        data-filter-label="<spring:message code='pd.label.title.CSREligible'/>"
        id="plantype_filter_csr"/>
        <label for="plantype_filter_csr" class="ps-form__check-label">
        <c:if test="${stateCode != 'CT'}">
            <a
            data-original-title="<spring:message code='pd.label.title.CSREligibletooltip' htmlEscape='true'/>"
            data-html="true"
            rel="tooltip"
            data-placement="right"
            href="#">
            <spring:message code='pd.label.title.CSREligible'/>
            </a>
        </c:if>
        <c:if test="${stateCode == 'CT'}">
            <spring:message code='pd.label.title.CSREligible'/>
        </c:if>
        <div class="ps-form__check-label-sub">
        <small><spring:message code="pd.label.title.csrMessage"/></small>
        </div>

        </label>
        </div>


        <div class="ps-form__check">
        <input
        type="checkbox"
        class="plantype_filter_hsa gtm_filter ps-form__check-input"
        value="Yes"
        data-filter-label="<spring:message code='pd.label.title.hsaQualified'/>"
        id="plantype_filter_hsa"/>
        <label for="plantype_filter_hsa" class="ps-form__check-label">

        <c:if test="${stateCode == 'CA' || stateCode == 'MN'}">
            <a
            data-original-title="<spring:message code='pd.label.title.hsaQualifiedtooltip' htmlEscape='true'/>"
            data-html="true"
            rel="tooltip"
            role="tooltip"
            data-placement="right"
            href="#">
            <spring:message code='pd.label.title.hsaQualified'/>
            </a>
        </c:if>
        <c:if test="${stateCode != 'CA' && stateCode != 'MN'}">
            <spring:message code='pd.label.title.hsaQualified'/>
        </c:if>
        <div class="ps-form__check-label-sub">
        <small><spring:message code="pd.label.title.hsaMessage"/></small>
        </div>
        </label>
        </div>

        </div>
    </c:if>
    <%-- plan features end --%>


    <%-- preferences --%>
    <div class="ps-sidebar__filter hide">
    <h3 class="ps-sidebar__filter-header">
    <spring:message code="pd.label.title.yourPreferences"/>
    </h3>
    <div class="ps-form__check">
    <input
    type="checkbox"
    value="BENEFITS"
    class="filter_checkbox ps-form__check-input"
    data-filter-label="<spring:message code='pd.label.title.yourBenefits'/>"
    id="your_preferences_benefits"/>
    <label for="your_preferences_benefits" class="ps-form__check-label">
    <spring:message code='pd.label.title.yourBenefits'/>
    <small class="gutter10-l">
    <a href="preferences"><spring:message code="pd.label.title.edit"/></a>
    </small>
    </label>
    </div>
    </div>
    <%-- preferences end --%>


    <%-- metal tier --%>
    <div class="ps-sidebar__filter">
    <h3 class="ps-sidebar__filter-header">
    <c:if test="${insuranceType == 'DENTAL'}">
        <spring:message code="pd.label.planTier"/>
    </c:if>

    <c:if test="${insuranceType != 'DENTAL'}">

        <c:if test="${stateCode != 'ID'}">
            <a
            data-original-title="<spring:message code='pd.label.title.metalTiertooltip' htmlEscape='true'/>"
            data-html="true"
            rel="tooltip"
            data-placement="right"
            href="#"
            class="gtm_metal_tier_heading"
            onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Plan Selection - Left Nav
            Hover', 'eventAction': '
            <spring:message code="pd.label.title.metalTier"/>
            - Hover', 'eventLabel': '<spring:message code="pd.label.title.metalTier"/>'});">
            <spring:message code="pd.label.title.metalTier"/>
            </a>
        </c:if>
        <c:if test="${stateCode == 'ID'}">
            <spring:message code="pd.label.title.metalTier"/>
        </c:if>
    </c:if>
    </h3>

    <c:choose>
        <c:when test="${insuranceType == 'DENTAL'}">
            <div class="ps-form__check">
            <input
            type="checkbox"
            class="filter_checkbox plantTier ps-form__check-input gtm_filter"
            value="LOW"
            data-filter-label="<spring:message code='pd.label.low'/>"
            id="filter_plantTier_low"/>
            <label for="filter_plantTier_low" class="ps-form__check-label">
            <c:if test="${stateCode == 'MN'}">
                <a
                data-original-title="<spring:message code='pd.label.title.dentalPlanTireLowTooltip' htmlEscape='true'/>"
                data-html="true"
                rel="tooltip"
                data-placement="right"
                href="#"
                onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Plan Selection - Hover',
                'eventAction': '
                <spring:message code="pd.label.low"/>
                - Hover', 'eventLabel': '<spring:message code="pd.label.low"/>'});">
                <spring:message code="pd.label.low"/>
                </a>
            </c:if>
            <c:if test="${stateCode != 'MN'}">
                <spring:message code="pd.label.low"/>
            </c:if>
            </label>
            </div>

            <div class="ps-form__check">
            <input
            type="checkbox"
            class="filter_checkbox plantTier ps-form__check-input gtm_filter"
            value="HIGH"
            data-filter-label="<spring:message code='pd.label.high'/>"
            id="filter_plantTier_high"/>
            <label for="filter_plantTier_high" class="ps-form__check-label">
            <c:if test="${stateCode == 'MN'}">
                <a
                data-original-title="<spring:message code='pd.label.title.dentalPlanTireHighTooltip' htmlEscape='true'/>
                "
                data-html="true"
                rel="tooltip"
                data-placement="right"
                href="#"
                onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Plan Selection - Hover',
                'eventAction': '
                <spring:message code="pd.label.high"/>
                - Hover', 'eventLabel': '<spring:message code="pd.label.high"/>'});">
                <spring:message code="pd.label.high"/>
                </a>
            </c:if>
            <c:if test="${stateCode != 'MN'}">
                <spring:message code="pd.label.high"/>
            </c:if>
            </label>
            </div>
        </c:when>
        <c:otherwise>

            <%-- platinum --%>
            <div class="ps-form__check">
            <input
            type="checkbox"
            class="filter_checkbox gtm_filter ps-form__check-input"
            value="PLATINUM"
            data-filter-label="<spring:message code='pd.label.filterby.platinum'/>"
            id="filter_platinum"/>
            <label for="filter_platinum" class="ps-form__check-label">
            <c:if test="${stateCode == 'CA'}">
                <a
                class="cp-tooltip__icon-link"
                data-original-title="<spring:message code='pd.label.filterby.platinumtooltip' htmlEscape='true'/>"
                data-html="true"
                rel="tooltip"
                data-placement="right"
                href="#">
                <spring:message code="pd.label.filterby.platinum"/>
                </a>
            </c:if>
            <c:if test="${stateCode != 'CA'}">
                <spring:message code="pd.label.filterby.platinum"/>
            </c:if>
            <div class="ps-form__check-label-sub">
            <small><spring:message code="pd.label.title.message13"/></small>
            </div>
            </label>
            </div>
            <%-- platinum end --%>


            <%-- gold --%>
            <div class="ps-form__check">
            <input
            type="checkbox"
            class="filter_checkbox gtm_filter ps-form__check-input"
            value="GOLD"
            data-filter-label="<spring:message code='pd.label.filterby.gold'/>"
            id="filter_gold"/>
            <label for="filter_gold" class="ps-form__check-label">
            <c:if test="${stateCode == 'CA'}">
                <a
                data-original-title="<spring:message code='pd.label.filterby.goldtooltip' htmlEscape='true'/>"
                data-html="true"
                rel="tooltip"
                data-placement="right"
                href="#">
                <spring:message code="pd.label.filterby.gold"/>
                </a>
            </c:if>
            <c:if test="${stateCode != 'CA'}">
                <spring:message code="pd.label.filterby.gold"/>
            </c:if>

            <div class="ps-form__check-label-sub">
            <small><spring:message code="pd.label.title.message14"/></small>
            </div>
            </label>
            </div>
            <%-- gold end --%>


            <%-- silver --%>
            <div class="ps-form__check">
            <input
            type="checkbox"
            class="filter_checkbox gtm_filter ps-form__check-input"
            value="SILVER"
            data-filter-label="<spring:message code='pd.label.filterby.silver'/>"
            id="filter_silver"/>
            <label for="filter_silver" class="ps-form__check-label">
            <c:if test="${stateCode == 'CA'}">
                <a
                data-original-title="<spring:message code='pd.label.filterby.silvertooltip' htmlEscape='true'/>"
                data-html="true"
                rel="tooltip"
                data-placement="right"
                href="#">
                <spring:message code="pd.label.filterby.silver"/>
                </a>
            </c:if>
            <c:if test="${stateCode != 'CA'}">
                <spring:message code="pd.label.filterby.silver"/>
            </c:if>

            <div class="ps-form__check-label-sub">
            <small><spring:message code="pd.label.title.message15"/></small>
            </div>

            </label>
            </div>
            <%-- silver end --%>

            <%-- bronze --%>
            <div class="ps-form__check">
            <input
            type="checkbox"
            class="filter_checkbox gtm_filter ps-form__check-input"
            value="BRONZE"
            data-filter-label="<spring:message code='pd.label.filterby.bronze'/>"
            id="filter_bronze"/>
            <label for="filter_bronze" class="ps-form__check-label">
            <c:if test="${stateCode == 'CA'}">
                <a
                data-original-title="<spring:message code='pd.label.filterby.bronzetooltip' htmlEscape='true'/>"
                data-html="true"
                rel="tooltip"
                data-placement="right"
                href="#">
                <spring:message code="pd.label.filterby.bronze"/>
                </a>
            </c:if>
            <c:if test="${stateCode != 'CA'}">
                <spring:message code="pd.label.filterby.bronze"/>
            </c:if>

            <div class="ps-form__check-label-sub">
            <small><spring:message code="pd.label.title.message16"/></small>
            </div>
            </label>
            </div>
            <%-- bronze end --%>

            <%-- catastrophic --%>
            <div class="ps-form__check" id="catastrphicFilter" style="display:none;">
            <input
            type="checkbox"
            class="filter_checkbox gtm_filter ps-form__check-input"
            value="CATASTROPHIC"
            data-filter-label="<spring:message code='pd.label.filterby.catastropic'/>"
            id="filter_catastrophic"/>
            <label for="filter_catastrophic" class="ps-form__check-label">
            <c:if test="${stateCode == 'CA' || stateCode == 'MN'}">
                <a
                data-original-title="<spring:message code='pd.label.filterby.catastropictooltip' htmlEscape='true'/>"
                data-html="true"
                rel="tooltip"
                data-placement="right"
                href="#">
                <spring:message code="pd.label.filterby.catastropic"/>
                </a>
            </c:if>
            <c:if test="${stateCode != 'CA' && stateCode != 'MN'}">
                <spring:message code="pd.label.filterby.catastropic"/>
            </c:if>

            <div class="ps-form__check-label-sub">
            <small><spring:message code="pd.label.title.message.catastrophic"/></small>
            </div>


            </label>
            </div>
            <%-- catastrophic end--%>

        </c:otherwise>
    </c:choose>
    </div>
    <%-- metal tier end --%>


    <%-- yearly deductible --%>
    <div class="ps-sidebar__filter">
    <h3 class="ps-sidebar__filter-header">
    <c:if test="${stateCode == 'CA' || stateCode == 'MN'}">
        <a
        onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Plan Selection - Left Nav Hover',
        'eventAction': '
        <spring:message code="pd.label.Deductible"/>
        - Hover', 'eventLabel': '<spring:message code="pd.label.Deductible"/>'});"
        data-original-title="<spring:message code='pd.label.tooltip.deductible' htmlEscape='true'/>"
        data-html="true"
        rel="tooltip"
        data-placement="right"
        href="#">
        <spring:message code="pd.label.Deductible"/>
        </a>
    </c:if>
    <c:if test="${stateCode != 'CA' && stateCode != 'MN'}">
        <spring:message code="pd.label.Deductible"/>
    </c:if>
    </h3>

    <c:choose>
        <c:when test="${insuranceType == 'DENTAL'}">
            <div class="ps-form__check">
            <input
            type="checkbox"
            class="deductible_filter_checkbox ps-form__check-input gtm_filter"
            value="0-49"
            data-filter-label="<spring:message code='pd.label.title.0-49'/>"
            id="deductible_filter_49"/>
            <label for="deductible_filter_49" class="ps-form__check-label">
            <spring:message code="pd.label.title.0-49"/>
            </label>
            </div>

            <div class="ps-form__check">
            <input
            type="checkbox"
            class="deductible_filter_checkbox ps-form__check-input gtm_filter"
            value="50-99"
            data-filter-label="<spring:message code='pd.label.title.50-99'/>"
            id="deductible_filter_99"/>
            <label for="deductible_filter_99" class="ps-form__check-label">
            <spring:message code="pd.label.title.50-99"/>
            </label>
            </div>

            <div class="ps-form__check">
            <input
            type="checkbox"
            class="deductible_filter_checkbox ps-form__check-input gtm_filter"
            value="100-10000"
            data-filter-label="<spring:message code='pd.label.title.100-10000'/>"
            id="deductible_filter_10000"/>
            <label for="deductible_filter_10000" class="ps-form__check-label">
            <spring:message code="pd.label.title.100-10000"/>
            </label>
            </div>
        </c:when>
        <c:otherwise>
            <div id="deductibleRangeFilter"></div>
        </c:otherwise>
    </c:choose>
    </div>
    <%-- yearly deductible end --%>

    <%--  / company --%>
    <div class="ps-sidebar__filter" id="carrierFilter">
    <h3 class="ps-sidebar__filter-header">
    <spring:message code="pd.label.title.company"/>
    </h3>
    </div>
    <%-- carrier / company end --%>


    <%-- quality rating --%>
    <c:if test="${insuranceType != 'DENTAL' && showQualityRating == 'YES'}">
        <div class="ps-sidebar__filter">
        <h3 class="ps-sidebar__filter-header">
        <c:if test="${stateCode == 'MN'}">
            <a
            data-original-title="<spring:message code='pd.label.title.tooltip.qualityRatings' htmlEscape='true'/>"
            data-html="true"
            rel="tooltip"
            data-placement="right"
            href="#"
            class="gtm_quality_rating_hover"
            onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Plan Selection - Hover',
            'eventAction': '
            <spring:message code="pd.label.title.qualityRatings"/>
            - Hover', 'eventLabel': '<spring:message code="pd.label.title.qualityRatings"/>'});">
            <spring:message code="pd.label.title.qualityRatings"/>
            </a>
        </c:if>
        <c:if test="${stateCode != 'MN'}">
            <spring:message code="pd.label.title.qualityRatings"/>
        </c:if>
        </h3>

        <c:if test="${stateCode != 'CT'}">
            <div class="ps-form__check">
            <input
            type="checkbox"
            class="qualityrating_filter_checkbox gtm_filter ps-form__check-input"
            value="5-6"
            data-filter-label="5<spring:message code='pd.label.planselection.starsFilter'/>"
            id="fivestars"/>
            <label for="fivestars" class="ps-form__check-label">
            <span class="aria-hidden">5<spring:message code='pd.label.planselection.starsFilter'/></span>
            <a><i class="icon-star"></i></a>
            <a><i class="icon-star"></i></a>
            <a><i class="icon-star"></i></a>
            <a><i class="icon-star"></i></a>
            <a><i class="icon-star"></i></a>
            </label>
            </div>
        </c:if>

        <div class="ps-form__check">
        <input
        type="checkbox"
        class="qualityrating_filter_checkbox gtm_filter ps-form__check-input"
        value="4-5"
        data-filter-label="4<spring:message code='pd.label.planselection.starsFilter'/>"
        id="fourstars"/>
        <label for="fourstars" class="ps-form__check-label">
        <span class="aria-hidden">4<spring:message code='pd.label.planselection.starsFilter'/></span>
        <a><i class="icon-star"></i></a>
        <a><i class="icon-star"></i></a>
        <a><i class="icon-star"></i></a>
        <a><i class="icon-star"></i></a>
        <c:if test="${stateCode != 'CT'}">
            <a><i class="icon-star-empty"></i></a>
        </c:if>
        </label>
        </div>

        <div class="ps-form__check">
        <input
        type="checkbox"
        class="qualityrating_filter_checkbox gtm_filter ps-form__check-input"
        value="3-4"
        data-filter-label="3<spring:message code='pd.label.planselection.starsFilter'/>"
        id="threestars"/>
        <label for="threestars" class="ps-form__check-label">
        <span class="aria-hidden">3<spring:message code='pd.label.planselection.starsFilter'/></span>
        <a><i class="icon-star"></i></a>
        <a><i class="icon-star"></i></a>
        <a><i class="icon-star"></i></a>
        <a><i class="icon-star-empty"></i></a>
        <c:if test="${stateCode != 'CT'}">
            <a><i class="icon-star-empty"></i></a>
        </c:if>
        </label>
        </div>


        <div class="ps-form__check">
        <input
        type="checkbox"
        class="qualityrating_filter_checkbox gtm_filter ps-form__check-input"
        value="2-3"
        data-filter-label="2<spring:message code='pd.label.planselection.starsFilter'/>"
        id="twostars"/>
        <label for="twostars" class="ps-form__check-label">
        <span class="aria-hidden">2<spring:message code='pd.label.planselection.starsFilter'/></span>
        <a><i class="icon-star"></i></a>
        <a><i class="icon-star"></i></a>
        <a><i class="icon-star-empty"></i></a>
        <a><i class="icon-star-empty"></i></a>
        <c:if test="${stateCode != 'CT'}">
            <a><i class="icon-star-empty"></i></a>
        </c:if>
        </label>
        </div>


        <div class="ps-form__check">
        <input
        type="checkbox"
        class="qualityrating_filter_checkbox gtm_filter ps-form__check-input"
        value="1-2"
        data-filter-label="1<spring:message code='pd.label.planselection.starFilter'/>"
        id="onestar"/>
        <label for="onestar" class="ps-form__check-label">
        <span class="aria-hidden">1<spring:message code='pd.label.planselection.starFilter'/></span>
        <a><i class="icon-star"></i></a>
        <a><i class="icon-star-empty"></i></a>
        <a><i class="icon-star-empty"></i></a>
        <a><i class="icon-star-empty"></i></a>
        <c:if test="${stateCode != 'CT'}">
            <a><i class="icon-star-empty"></i></a>
        </c:if>
        </label>
        </div>


        </div>
    </c:if>
    <%-- quality rating end --%>
    </fieldset>
    </div>
    <%-- filter by end --%>
    </aside>


    <div id="atleast-one-plan" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="atleastone"
    aria-hidden="true">
    <div class="modal-header">
    <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="atleastone"><spring:message code="pd.label.title.message17"/></h3>
    </div>
    <div class="modal-body">
    <p><spring:message code="pd.label.title.message18"/></p>
    </div>
    <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal"><spring:message code="pd.label.title.cancel"/></button>
    </div>
    </div>

    <div id="enrollNow" class="modal bigger-modal hide fade" tabindex="-1" role="dialog"
    aria-labelledby="enrollNowHeader" aria-hidden="true">
    <div class="modal-header">
    <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="enrollNowHeader"><spring:message code="pd.label.enrollNow.modal.header"/></h3>
    </div>
    <div class="modal-body">

    <h4><spring:message code="pd.label.enrollNow.modal.content1"/></h4>
    <p class="margin10-l">
    1.<spring:message code="pd.label.enrollNow.modal.content1.1"/><br>
    2.
    <spring:message code="pd.label.enrollNow.modal.content1.2"/>
    </p>

    <h4 class="margin30-t"><spring:message code="pd.label.enrollNow.modal.content2"/></h4>

    <div class="cta-container">
    <div class="cta-container__cta">
    <p><spring:message code="pd.label.enrollNow.modal.signIn.content"/></p>
    <a class="btn btn-primary" href="https://auth.mnsure.org/login/Login.jsp" target="_blank"><spring:message
        code="pd.label.enrollNow.modal.signIn"/></a>
    </div>
    <div class="cta-container__cta">
    <p><spring:message code="pd.label.enrollNow.modal.createAccount.content"/></p>
    <a class="btn btn-primary" href="https://www.mnsure.org/acct-access/create-account-check.jsp" target="_blank">
    <spring:message code="pd.label.enrollNow.modal.createAccount"/></a>
    </div>
    <div class="cta-container__cta">
    <p><spring:message code="pd.label.enrollNow.modal.searchHelp.content"/></p>
    <a class="btn btn-primary" href="https://www.mnsure.org/help/index.jsp" target="_blank"><spring:message
        code="pd.label.enrollNow.modal.searchHelp"/></a>
    </div>
    </div>

    </div>
    </div>


    <div id="more-than-four" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="greater"
    aria-hidden="true">
    <div class="modal-header">
    <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="greater"><spring:message code="pd.label.title.message19"/></h3>
    </div>
    <div class="modal-body">
    <p><spring:message code="pd.label.title.message22"/></p>
    </div>
    <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal"><spring:message code="pd.label.title.cancel"/></button>
    </div>
    </div>

    <div id="cart-error" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="carterror"
    aria-hidden="true">
    <div class="modal-header">
    <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="carterror"><spring:message code="pd.label.title.message23"/></h3>
    </div>
    <div class="modal-body">
    <p><spring:message code="pd.label.title.message24"/></p>
    </div>
    <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal" title="<spring:message code="pd.label.title.backToShopping"/>">
    <spring:message code="pd.label.title.backToShopping"/></button>
    </div>
    </div>


    <c:if test="${loggedInUser != null && flowType == 'PLANSELECTION' }">
        <div id="shopping-modal" class="modal hide fade shoppingForModal" tabindex="-1" role="dialog" aria-label="Dialog
        Window (Press escape to close)" aria-hidden="true">
        <div class="modal-header">
        <h3 id="shoppingforHeader"><spring:message code="pd.label.title.shoppingFor"/></h3>
        </div>
        <div class="modal-body">
        <table class="table table-striped margin0">
        <tr>
        <th class="noBorder"><spring:message code="pd.label.title.applicant"/></th>
        <th class="noBorder txt-center"><spring:message code="pd.label.title.dob"/></th>
        <th class="noBorder txt-center"><spring:message code="pd.label.title.seekingCoverage"/></th>
        </tr>
        <c:set var="seekingCoverageDentalObj" value=""></c:set>
        <c:forEach var="person" items="${personDataList}" varStatus="loop">
            <tr>
            <td style="text-transform: capitalize;">${fn:toLowerCase(person.firstName)}</td>
            <td class="txt-center">${person.birthDay}</td>
            <td class="txt-center"><input aria-label="Seeking Coverage" type="checkbox" name="chkSCoverage" id="chkSCoverage${loop.count}"/></td>

            <c:set var="temp"
                   value='{"chk_id":"chkSCoverage${loop.count}","seekingCoverageDental":"${person.seekCoverage}","age":"${person.age}","relationship":"${person.relationship}","personId":"${person.id}"}'></c:set>
            <c:choose>
                <c:when test="${loop.first}">
                    <c:set var="seekingCoverageDentalObj" value="${temp}"></c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="seekingCoverageDentalObj" value="${seekingCoverageDentalObj},${temp}"></c:set>
                </c:otherwise>
            </c:choose>
            </tr>
        </c:forEach>
        </table>
        </div>

        <div class="modal-footer clearfix">
        <div id="shopping-modal-error" class="app-error hide"></div>
        <button class="btn btn-default pull-left" data-dismiss="modal" title="<spring:message
            code="pd.label.title.cancel"/>"><spring:message code="pd.label.title.cancel"/></button>
        <button class="btn btn-primary pull-right" data-dismiss="modal" id="shopping-modal-update" title="
        <spring:message code="pd.label.title.updateResults"/>"><spring:message code="pd.label.title.updateResults"/>
        </button>
        </div>
        </div>
    </c:if>

    <div id="shoppingPop-modal" class="modal hide fade shoppingForModal " tabindex="-1" role="dialog" aria-label="Dialog
    Window (Press escape to close)" aria-hidden="true">
        <div class="modal-header txt-center">
            <h3 id="shoppingforHeader"><spring:message code="pd.label.popup.planselection.info1.1"/></h3>
        </div>
        <div class="modal-body">
            <p>
                <spring:message code="pd.label.popup.planselection.info"/>&nbsp;<span style="font-weight: bold;" class="txt-center" id="PlanName"></span>&nbsp;<spring:message code="pd.label.popup.planselection.info1"/>
            </p>
            <div id="healthButton"></div>
            <div id="dentalButton"></div>
            <div id="healthdentalButton"></div>
        </div>
    </div>


    <div id="rightpanel" class="ps-rightpanel">
        <main class="ps-plans plan-display-show">
            <div id="pagination" class="ps-pagination row-fluid"></div>
            <div class="ps-plans__header">
                <div class="ps-plans__filters-container">
                    <div class="ps-plans__filters hide">
                        <span class="ps-plans__filters-label"><spring:message code="pd.label.title.filtersApplied"/></span>
                        <span class="ps-plans__filters-applied"></span>
                    </div>
                </div>
            </div>

            <input type="hidden" name="closedPopup" id="closedPopup" value="${closedPopup}" />
            <input type="hidden" name="isDentalPlanSelected" id="isDentalPlanSelected" value="${isDentalPlanSelected }" />
            <input type="hidden" id = "csrfToken" value ='<df:csrfToken plainToken="true"/>'>
            <input type="hidden" name="PGRM_TYPE" id="PGRM_TYPE" value="${PGRM_TYPE}" />
            <input type="hidden" name="indType" id="indType" value="${private_TYPE}" />
            <input type="hidden" name="enableCart" id="enableCart" value="${enableCart}" />

            <div class="popover right first-time" data-toggle="popover">
                <button type="button" class="close first-timer" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="arrow"></div>
                    <h3 class="popover-title">
                        <strong><spring:message code="pd.label.title.popoverTitle"/></strong>
                    </h3>
                <div class="popover-content">
                    <ul>
                        <li><spring:message code="pd.label.title.popoverContent1"/></li>
                        <li><spring:message code="pd.label.title.popoverContent2"/></li>
                        <li><spring:message code="pd.label.title.popoverContent3"/></li>
                    </ul>
                    <a class="first-timer btn btn-primary" href="#"><spring:message code="pd.label.title.gotIt"/></a>
                </div>
            </div>

            <c:choose>
                <c:when test="${insuranceType == 'DENTAL'}">
                    <jsp:include page="dentalTile.jsp" flush="true"></jsp:include>
                </c:when>
                <c:otherwise>
                    <jsp:include page="tile.jsp" flush="true"></jsp:include>
                </c:otherwise>
            </c:choose>

            <div id="pagination_bottom" class="ps-pagination row-fluid"></div>
        </main>

        <main class="ps-detail plan-detail-show hide">
        <c:choose>
            <c:when test="${insuranceType == 'DENTAL'}">
                <jsp:include page="dentalPlanDetail.jsp" flush="true"></jsp:include>
            </c:when>
            <c:otherwise>
                <jsp:include page="planDetail.jsp" flush="true"></jsp:include>
            </c:otherwise>
        </c:choose>
        </main>

        <main class="ps-compare ps-compare-show">
        <c:choose>
            <c:when test="${insuranceType == 'DENTAL'}">
                <jsp:include page="dentalPlanCompare.jsp" flush="true"></jsp:include>
            </c:when>
            <c:otherwise>
                <jsp:include page="planCompare.jsp" flush="true"></jsp:include>
            </c:otherwise>
        </c:choose>
        </main>
    </div>

    </div><!--.row-fluid-->

    <c:if test="${stateCode == 'CA' || (stateCode == 'MN' && insuranceType == 'HEALTH')}">
        <p class="disclaimer benefits-summary-disclaimer">
        <spring:message code="pd.label.title.benefitsSummaryDisclaimer"/>
        <span class="disclaimer-text"><spring:message code="pd.label.benefitsSummaryDisclaimerText"/></span>
        </p>
    </c:if>

    <c:if test="${stateCode == 'CA'}">
        <p class="disclaimer qrs-disclaimer hide">
        <spring:message code="pd.label.title.qrsDisclaimer"/>
        <span class="disclaimer-text"><spring:message code="pd.label.qrsDisclaimerText"/></span>
        </p>
    </c:if>

    </section><!--.row-fluid-->


    <input type="hidden" name="PERSON_COUNT" id="PERSON_COUNT" value="${personCount}" />
    <input type="hidden" name="dentalPersonCount" id="dentalPersonCount" value="${dentalPersonCount}" />
    <input type="hidden" name="PLAN_TYPE" id="PLAN_TYPE" value="${PLAN_TYPE}" />
    <input type="hidden" name="ISSUBSIDIZED" id="ISSUBSIDIZED" value="${isSubsidized}" />
    <input type="hidden" name="shoppingType" id="shoppingType" value="${shoppingType}" />

    <input type="hidden" name="elegibilityComplete" id="elegibilityComplete" value="${elegibilityComplete}" />
    <input type="hidden" name="existingSADPEnrollmentID" id="existingSADPEnrollmentID"
    value="${existingSADPEnrollmentID}" />
    <input type="hidden" name="exchangeType" id="exchangeType" value="${exchangeType}" />
    <input type="hidden" name="stateCode" id="stateCode" value="${stateCode}" />
    <input type="hidden" name="Child_Flag" id="Child_Flag" value="${Child_Flag}" />
    <input type="hidden" name="providerLinkOnOff" id="providerLinkOnOff" value="${providerLinkOnOff}" />
    <input type="hidden" name="qualitySectionOnOff" id="qualitySectionOnOff" value="${qualitySectionOnOff}" />
    <input type="hidden" name="doctorSectionOnOff" id="doctorSectionOnOff" value="${doctorSectionOnOff}" />
    <input type="hidden" name="facilitySectionOnOff" id="facilitySectionOnOff" value="${facilitySectionOnOff}" />
    <input type="hidden" name="dentistSectionOnOff" id="dentistSectionOnOff" value="${dentistSectionOnOff}" />
    <input type="hidden" name="currentGroupId" id="currentGroupId" value="${currentGroupId}">
    <c:if test="${exchangeType== 'OFF'}">
        <input type="hidden" name="isPlanAdded" id="isPlanAdded" value="${isPlanAdded}" />
    </c:if>
    <input type="hidden" name="qualityRatingOnOff" id="qualityRatingOnOff" value="${qualityRatingOnOff}" />
    <input type="hidden" name="showOopOnOff" id="showOopOnOff" value="${showOopOnOff}" />
    <input type="hidden" name="csr" id="csr" value="${csr}" />
    <input type="hidden" name="dentalPlansOnOff" id="dentalPlansOnOff" value="${dentalPlansOnOff}" />
    <input type="hidden" name="customGroupingOnOff" id="customGroupingOnOff" value="${customGroupingOnOff}" />
    <input type="hidden" name="multipleItemsEntryOnOff" id="multipleItemsEntryOnOff" value="${multipleItemsEntryOnOff}"
    />
    <input type="hidden" name="gpsVersion" id="gpsVersion" value="${gpsVersion}" />
    <input type="hidden" name="flowType" id="flowType" value="${flowType}" />
    <input type="hidden" name="showQualityRating" id="showQualityRating" value="${showQualityRating}" />
    <input type="hidden" name="showSilverFirst" id="showSilverFirst" value="${showSilverFirst}" />
    <input type="hidden" name="showCurrentPlanFirst" id="showCurrentPlanFirst" value="${showCurrentPlanFirst}" />
    <input type="hidden" id="view" name="view" value="" />
    <input type="hidden" name="healthPlanPresent" id=healthPlanPresent value="${healthPlanPresent}"/>
    <input type="hidden" name="dentalPlanPresent" id="dentalPlanPresent" value="${dentalPlanPresent}"/>
    <input type="hidden" name="enrollmentType" id="enrollmentType" value="${enrollmentType}"/>
    <input type="hidden" name="showNextToCart" id="showNextToCart" value="${showNextToCart}"/>
    <input type="hidden" name="insuranceType" id="insuranceType" value="${insuranceType}" />
    <input type="hidden" name="dentalGuaranteedEnable" id="dentalGuaranteedEnable" value="${dentalGuaranteedEnable}" />
    <input type="hidden" name="defaultSort" id="defaultSort" value="${defaultSort}"/>
    <input type="hidden" name="seekingCoverageDentalObj" id="seekingCoverageDentalObj"
    value='[${seekingCoverageDentalObj}]'/>
    <input type="hidden" name="isSeekingCoverage" id="isSeekingCoverage" value="${isSeekingCoverage}"/>
    <input type="hidden" name="productType" id="productType" value="${productType}"/>
    <input type="hidden" name="initialHealthPlanId" id="initialHealthPlanId" value="${initialHealthPlanId}"/>
    <input type="hidden" name="initialDentalPlanId" id="initialDentalPlanId" value="${initialDentalPlanId}"/>
    <input type="hidden" name="providersList" id="providersList" value="${providersList}"/>
    <input type="hidden" name="showNetworkTransparencyRating" id="showNetworkTransparencyRating"
    value="${showNetworkTransparencyRating}" />
    <input type="hidden" name="iframe" id="iframe" value="${iframe}"/>
    <input type="hidden" name="coverageStartDate" id="coverageStartDate" value="${coverageStartDate}"/>
    <input type="hidden" name="zipcode" id="zipcode" value="${zipcode}" />
    <input type="hidden" name="providerMapConfig" id="providerMapConfig" value="${providerMapConfig}" />
    <input type="hidden" name="medicalUse" id="medicalUse" value="${medicalUse}" />
    <input type="hidden" name="prescriptionUse" id="prescriptionUse" value="${prescriptionUse}" />

    <!--sample template for pagination UI-->
    <script type="text/html" id="tmpPagination">
    <div class="cp-pagination">
    <span class="cp-pagination__btn cp-pagination__btn--left">
    <a href="#<@=page - 1@>" class="cp-pagination__arrow prev">
    <span class="aria-hidden">Previous</span>
    <span class="aria-hidden pg_number_google_tracking"><@=page@></span>
    <i class="icon-chevron-left cp-pagination__icon"></i>
    </a>
    </span>
    <span class="cp-pagination__content"><@=page@> of <@=pageSet.length @></span>
    <span class="cp-pagination__btn cp-pagination__btn--right">
    <a href="#<@=page + 1@>" class="cp-pagination__arrow next">
    <span class="aria-hidden">Next</span>
    <span class="aria-hidden pg_number_google_tracking"><@=page@></span>
    <i class="icon-chevron-right cp-pagination__icon"></i>
    </a>
    </span>
    </div>
    </script>

    <script type="text/html" id="tmpSort">
    <fieldset>
    <legend class="ps-sidebar__group-header">
    <spring:message code="pd.label.script.sortBy"/>
    </legend>

    <@ if($("#gpsVersion").val() == 'V1' && $("#insuranceType").val() == 'HEALTH') { @>
    <div class="ps-form__check">
    <input
    type="radio"
    class="ps-form__check-input gtm_sort"
    name="sortBy"
    value="smartScore"
    id="smartScore"/>
    <label for="smartScore" class="ps-form__check-label">
    <spring:message code="pd.label.title.planScore" htmlEscape="true"/>
    </label>
    </div>
    <@ } @>

    <c:if test="${insuranceType != 'DENTAL'}">
        <div class="ps-form__check">
        <input
        type="radio"
        class="ps-form__check-input gtm_sort"
        name="sortBy"
        value="estimatedTotalHealthCareCost"
        id="estimatedTotalHealthCareCost"/>
        <label for="estimatedTotalHealthCareCost" class="ps-form__check-label">
        <spring:message code="pd.label.title.expenseEstimate"/>
        </label>
        </div>
    </c:if>

    <div class="ps-form__check">
    <input
    type="radio"
    class="ps-form__check-input gtm_sort"
    name="sortBy"
    value="premiumAfterCredit"
    id="premiumAfterCredit"/>
    <label for="premiumAfterCredit" class="ps-form__check-label">
    <spring:message code="pd.label.title.popoverContent1" htmlEscape="true"/>
    <spring:message code="pd.label.lowtohigh" htmlEscape="true"/>
    </label>
    </div>


    <@ if($("#qualityRatingOnOff").val() == 'ON') { @>
    <div class="ps-form__check">
    <input
    type="radio"
    class="ps-form__check-input gtm_sort"
    name="sortBy"
    value="quality"
    id="overAllQuality"/>
    <label for="overAllQuality" class="ps-form__check-label">
    <spring:message code="pd.label.commontext.quality" htmlEscape="true"/>
    </label>
    </div>
    <@ } @>

    <div class="ps-form__check <c:if test="${insuranceType == 'HEALTH' && stateCode == 'CA' }">hide</c:if>">
    <input
    type="radio"
    class="ps-form__check-input gtm_sort"
    name="sortBy"
    value="deductible"
    id="deductible"/>
    <label for="deductible" class="ps-form__check-label">
    <spring:message code="pd.label.Deductible" htmlEscape="true"/>
    <spring:message code="pd.label.lowtohigh" htmlEscape="true"/>
    </label>
    </div>

    <@ if($("#showOopOnOff").val() == 'ON') { @>
    <div class="ps-form__check">
    <input
    type="radio"
    class="ps-form__check-input gtm_sort"
    name="sortBy"
    value="oopMax"
    id="oopMax"/>
    <label for="oopMax" class="ps-form__check-label">
    <spring:message code="pd.label.oop"/>
    <spring:message code="pd.label.lowtohigh" htmlEscape="true"/>
    </label>
    </div>
    <@ } @>
    </fieldset>

    <!-- <fieldset>
    <label for="sortBy"><span class="sortby-label"><spring:message code="pd.label.script.sortBy"/></span>
    <select id="sortBy" class="sort input-medium margin0" name="sortBy"
    onchange="triggerGoogleTrackingEvent(['_trackEvent', 'Plan Display Sorting', 'select',
    $(this).find('option:selected').text()]);">
    <@ if($("#gpsVersion").val() == 'V1' && $("#insuranceType").val() == 'HEALTH') { @>
    <option id="smartScore" value="smartScore"><spring:message code="pd.label.title.planScore" htmlEscape="true"/>
    </option>
    <@ } @>

    <option id="premiumAfterCredit" value="premiumAfterCredit">
    <spring:message code="pd.label.title.popoverContent1" htmlEscape="true"/>
    <spring:message code="pd.label.lowtohigh" htmlEscape="true"/></option>

    <c:if test="${insuranceType != 'DENTAL'}">
        <option id="estimatedTotalHealthCareCost" value="estimatedTotalHealthCareCost"><spring:message
            code="pd.label.title.expenseEstimate"/></option>
    </c:if>

    <@ if($("#qualityRatingOnOff").val() == 'ON') { @>
    <option id="overAllQuality" value="quality"><spring:message code="pd.label.commontext.quality" htmlEscape="true"/>
    </option>
    <@ } @>

    <option id="deductible" value="deductible">
    <spring:message code="pd.label.Deductible" htmlEscape="true"/>
    <spring:message code="pd.label.lowtohigh" htmlEscape="true"/></option>

    <@ if($("#showOopOnOff").val() == 'ON') { @>
    <option id="oopMax" value="oopMax">
    <spring:message code="pd.label.oop"/>
    <spring:message code="pd.label.lowtohigh" htmlEscape="true"/></option>
    <@ } @>
    </select>
    </label>
    </fieldset> -->
    </script>

    <%-- <script type="text/html" id="compareBoxTemplate">
    <li>
      <a href="javascript:void(0);"  class="close-icon js-removeCompare" role="button" aria-label="Remove"><i class="icon-remove" id="removeChk_<@=id@>"></i></a>
      <img src='<@=issuerLogo@>' alt="<@=issuer@>" />
      <div class="pull-left"><@=networkType@></div>
      <div class="pull-right">$<@=premiumAfterCredit.toFixed(2) @></div>
    </li>
    </script> --%>
    <script type="text/html" id="compareBoxTemplate">
    <div class="cp-compare-plans__plan cp-compare-plans__plan--added">
    <div class="cp-compare-plans__plan-info">
    <div class="cp-compare-plans__img-container">
    <img class="cp-compare-plans__img" src='<@=issuerLogo@>' alt="<@=issuer@>">
    </div>
    <a href="javascript:void(0);" class="js-removeCompare">
    <span class="aria-hidden">
    <spring:message code="pd.label.title.removeFromComapre"/>
    </span>
    <i id="removeChk_<@=id@>" class="icon-remove-circle cp-compare-plans__remove gtm_compare_plans_remove"
    data-issuer="<@=issuer@>" data-name="<@=name@>" data-level="<@=level@>"></i>
    </a>
    <div class="cp-compare-plans__name">
    <@if(planType === "HEALTH"){ @>
    <@ if(level === 'PLATINUM') {@>
    <span class="cp-tile__metal-tier cp-tile__metal-tier--platinum">
    <spring:message code="pd.label.filterby.platinum"/>
    <@ } else if(level === 'GOLD') { @>
    <span class="cp-tile__metal-tier cp-tile__metal-tier--gold">
    <spring:message code="pd.label.filterby.gold"/>
    <@ } else if(level === 'SILVER') { @>
    <span class="cp-tile__metal-tier cp-tile__metal-tier--silver">
    <spring:message code="pd.label.filterby.silver"/>
    <@ } else if(level === 'BRONZE') {@>
    <span class="cp-tile__metal-tier cp-tile__metal-tier--bronze">
    <spring:message code="pd.label.filterby.bronze"/>
    <@ } else if(level === 'CATASTROPHIC') {@>
    <span class="cp-tile__metal-tier cp-tile__metal-tier--catastrophic">
    <spring:message code="pd.label.filterby.catastropic"/>
    <@ } else {@>
    <span class="cp-tile__metal-tier cp-tile__metal-tier--bronze"><@=level@>
    <@ } @>

    <@} @>

    <@if(hsa === 'Yes'){ @> HSA <@} @>

    <@if(planType === 'DENTAL'){ @>
    <@=level@>
    <@} @>

    <@=networkType@></span>

    <@if(planType == "HEALTH"){ @>
    <@ var planNum = issuerPlanNumber.substr(issuerPlanNumber.length - 2, issuerPlanNumber.length) @>
    <@ if(planNum !== '00' && planNum !== '01') {@>
    <br>
    <span class="cp-tile__metal-tier cp-tile__metal-tier--csr cp-tile__metal-tier--csr-block ps-csr-show hide">
    <spring:message code="pd.label.title.csr"/></span>
    <@ } @>
    <@} @>
    </div>
    </div>

    <div class="cp-compare-plans__premium">
    $<@=premiumAfterCredit.toFixed(2) @>
    </div>
    </div>
    </script>

    <script type="text/html" id="tmpDetail">
    <a href="#" class="back-to-all-plans-link cp-link u-margin-top-20 u-margin-left-20 border-b">
    <i class="icon-caret-left"></i>
    <spring:message code="pd.label.title.backToAllPlans" htmlEscape="true"/>
    </a>
    </script>

    <script type="text/html" id="tmpCompare">
    <a href="#" class="backToAll" style="display:none;"><i class="icon-caret-left"></i>&nbsp;<spring:message
        code="pd.label.title.backToAllPlans" htmlEscape="true"/></a>
    </script>
    <script src="<c:url value="/resources/js/waypoints.min.js"/>"></script>
    <script src="<c:url value="/resources/js/waypoints-sticky.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.dropkick-min.js"/>"></script>

    <script type="text/javascript"
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDdTkn_k5hHWnbRAsI876vvXR2MAnL_UeE"></script>

    <script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js"/>"></script>


    <!-- backbone support libs -->
    <script src="<c:url value="/resources/js/underscore-min.js"/>"></script>
    <script src="<c:url value="/resources/js/backbone-min.js"/>"></script>
    <script src="<c:url value="/resources/js/json2.js"/>"></script>
    <script src="<c:url value="/resources/js/private_plandisplay/planselection.js"/>"></script>

    <!--Backbone.Paginator-->
    <script src="<c:url value="/resources/js/private_plandisplay/backbone-pagination.js"/>"></script>
    <script src="<c:url value="/resources/js/private_plandisplay/backbone-cart.js"/>"></script>

    <!--Models/Collections-->
    <script src="<c:url value="/resources/js/private_plandisplay/models/plan.js"/>"></script>
    <script src="<c:url value="/resources/js/private_plandisplay/collections/PaginatedCollection.js"/>"></script>
    <script src="<c:url value="/resources/js/private_plandisplay/models/cart.js"/>"></script>
    <script src="<c:url value="/resources/js/private_plandisplay/collections/CartCollection.js"/>"></script>

    <!--Views-->
    <script src="<c:url value="/resources/js/private_plandisplay/views/PlanView.js"/>"></script>
    <script src="<c:url value="/resources/js/private_plandisplay/views/PaginationView.js"/>"></script>
    <script src="<c:url value="/resources/js/private_plandisplay/views/CompareView.js"/>"></script>
    <script src="<c:url value="/resources/js/private_plandisplay/views/DetailView.js"/>"></script>
    <script src="<c:url value="/resources/js/private_plandisplay/views/SortView.js"/>"></script>

    <!-- angular provider map -->
    <!-- angular plan view script -->

    <!-- plan view script -->
    <script type="text/javascript" src="<c:url value="/resources/planView/planView.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/resources/providerMap/providerMap.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/providerMap/map.directive.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/providerMap/doctorsSidebar.directive.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/providerMap/infowindow.directive.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/providerMap/doctors.factory.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/providerMap/pagination.directive.js"/>"></script>

    <script type="text/javascript">
    $(document).ready(function(){

        $('.ps-sidebar-toggler').click(function(){
            if($(this).hasClass('show-side-bar')){
                $( ".ps-sidebar" ).animate({
                    left: "100%"
                }, 700, function() {
                    $('#footer,#rightpanel').css('display', 'none');
                    $('#sidebar span').removeClass('icon-chevron-right').addClass("pull-right icon-chevron-left");
                });
                $('.ps-sidebar-toggler').removeClass('show-side-bar');
            } else {
                $( ".ps-sidebar" ).animate({
                    left: "0%"
                }, 700, function() {
                    $('#footer,#rightpanel').css('display', 'block');
                    $('#sidebar span').removeClass('pull-right icon-chevron-left').addClass('icon-chevron-right');
                });
                $('.ps-sidebar-toggler').addClass('show-side-bar');
            }
        });
    
    // Data layer variable to be pushed at the end of shell.jsp
    dl_pageCategory = 'Plan Selection';

    var mtAriaLabel = $('.metal-tier legend a').val($('.metal-tier legend a')).attr('aria-label');
    var ydAriaLabel = $('.yearly-deductible legend a').val($('.yearly-deductible legend a')).attr('aria-label');
    $('.metal-tier legend a, .yearly-deductible legend a').bind('focus blur', function(e){
    e.stopPropagation();
    if(e.type == "focus"){
    if(this.id == "mt"){
    $(this).attr('aria-label',mtAriaLabel);
    }
    if(this.id == "yd"){
    $(this).attr('aria-label',ydAriaLabel);
    }
    }

    else if(e.type == "blur"){
    $(this).removeAttr('aria-label');
    }

    });

    // Export Test Rates on "View Consumer Shopping"
    var coverageStartDate = document.getElementById("coverageStartDate").value;
    //var persons = JSON.parse(document.getElementById("personDataListJson").value);

    // load getPrivateIndividualPlans url with jstl tag
    <c:url var="getPrivateIndividualPlans" value="/private/getIndividualPlans"/>

    $("#exportCSV").click(function() {
    $.get("${getPrivateIndividualPlans}", function(data, status) {
    if (status === 'success') {
    var jsonData = JSON.parse(data);
    var result = [];

    /** ======= persons info ====== **/
    // persons info -> title
    			result.push({'title_Household_Information': 'Household Information', 'title_Birth_Day' : 'Birth Day', 'title_Tobacco': 'Tobacco', 'title_Relation': 'Relation'});
    var household_id = 1;
    			var persons = JSON.parse(document.getElementById("personDataListJson").value);
    persons.forEach(function(person) {
    result.push({
    'Household Information': 'M' + household_id++,
    'Birth Day': formatDate(new Date(person.birthDay)),
    'Tobacco': person.tobacco,
    'Relation': person.relationship
    });
    });

    result.push({});

    /** ======= coverage start date, zipcode, county ====== **/
    			result.push({'title_coverageStartDate': 'Effective Coverage Start Date', 'value_coverageStartDate' : coverageStartDate });
    var zipcode = persons[0].zipCode.startsWith('0') ? '\'' + persons[0].zipCode : persons[0].zipCode;
    var county = persons[0].countyCode.startsWith('0') ? '\'' + persons[0].countyCode : persons[0].countyCode;

    result.push({'title_ZipCode': 'Zip Code', 'value_ZipCode': zipcode});
    result.push({'title_County': 'County', 'value_County' : county});

    result.push({});

    /** ======= plan info ====== **/
    // plan info -> title
    result.push({'title_plan_name': 'Plan Name', 'title_plan_number' : 'Plan HIOS ID(16 Digit)',
    'title_monthly_premium': 'Monthly Premium'});
    // plan info -> value
    jsonData.forEach(function(item) {
    result.push({
    	'Plan Name': item.name,
    	'Plan HIOS ID(16 Digit)': item.issuerPlanNumber,
    	'Monthly Premium after tax': item.premiumAfterCredit
    });
    });

    exportCSVFile(result, 'Individual-Plans'); // call the exportCSVFile() function to process the JSON and trigger the
    download
    } else {
    // console.error("Cannot export individual plans report.");
    }
    });
    });
    });
    </script>

    <script src="<c:url value="/resources/js/private_plandisplay/plan-selection.js"/>"></script>
