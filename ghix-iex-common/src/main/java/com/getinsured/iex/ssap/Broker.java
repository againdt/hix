
package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class Broker {

    @Expose
	private Integer internalBrokerId;
	@Expose
    private String brokerFederalTaxIdNumber;
    @Expose
    private String brokerName;
    @Expose
    private String brokerFirstName;
    @Expose
    private String brokerLastName;
    
    public Integer getInternalBrokerId() {
		return internalBrokerId;
	}

	public void setInternalBrokerId(Integer internalBrokerId) {
		this.internalBrokerId = internalBrokerId;
	}
	
	public String getBrokerFederalTaxIdNumber() {
        return brokerFederalTaxIdNumber;
    }

    public void setBrokerFederalTaxIdNumber(String brokerFederalTaxIdNumber) {
        this.brokerFederalTaxIdNumber = brokerFederalTaxIdNumber;
    }

    public String getBrokerName() {
        return brokerName;
    }

    public void setBrokerName(String brokerName) {
        this.brokerName = brokerName;
    }

	public String getBrokerFirstName() {
		return brokerFirstName;
	}

	public void setBrokerFirstName(String brokerFirstName) {
		this.brokerFirstName = brokerFirstName;
	}

	public String getBrokerLastName() {
		return brokerLastName;
	}

	public void setBrokerLastName(String brokerLastName) {
		this.brokerLastName = brokerLastName;
	}

}
