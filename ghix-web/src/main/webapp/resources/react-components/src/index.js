import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'react-app-polyfill/ie11';
import "@cmsgov/design-system-layout/dist/index.css";
import "@cmsgov/design-system-core/dist/index.css";
import store from './stores/configureStore';
import App from "./containers/App";

const node = document.getElementById('myReactID');

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>, node);