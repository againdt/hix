package com.getinsured.hix.indportal.dto;

public class MemberEligibilityStatus {
	private String eligibilityStatus;

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}
	
}
