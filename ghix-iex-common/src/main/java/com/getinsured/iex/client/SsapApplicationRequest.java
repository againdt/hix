package com.getinsured.iex.client;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class SsapApplicationRequest {

	private String caseNumber;
	long ssapApplicationId;
	private String exemptFlag;
	private String applicationData;
    private String applicantGuidToRemove;
    private BigDecimal userId;
    private List<String> exchgIndivIdList;
    private Date effectiveDate;
    private boolean isApplicationDentalStatusUpdate;

    public String getApplicantGuidToRemove() {
		return applicantGuidToRemove;
	}

	public void setApplicantGuidToRemove(String applicantGuidToRemove) {
		this.applicantGuidToRemove = applicantGuidToRemove;
	}

	public String getApplicationData() {
	            return applicationData;
	      }

	      public void setApplicationData(String applicationData) {
	            this.applicationData = applicationData;
	      }

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	private String applicationStatus;

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getExemptFlag() {
		return exemptFlag;
	}

	public void setExemptFlag(String exemptFlag) {
		this.exemptFlag = exemptFlag;
	}

	public BigDecimal getUserId() {
		return userId;
	}

	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}

	public List<String> getExchgIndivIdList() {
		return exchgIndivIdList;
	}

	public void setExchgIndivIdList(List<String> exchgIndivIdList) {
		this.exchgIndivIdList = exchgIndivIdList;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public boolean isApplicationDentalStatusUpdate() {
		return isApplicationDentalStatusUpdate;
	}

	public void setApplicationDentalStatusUpdate(boolean isApplicationDentalStatusUpdate) {
		this.isApplicationDentalStatusUpdate = isApplicationDentalStatusUpdate;
	}
		
}
