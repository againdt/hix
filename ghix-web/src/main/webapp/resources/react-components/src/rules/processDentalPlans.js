/**
 * Created by sellathurai_s on 11/16/18.
 */

import moment from 'moment';

class ProcessDentalPlans {

    constructor(tabData, config) {
        this.tabData = tabData;
        this.config = config;
        this.currentDate = this.config.serverDate ? moment(this.config.serverDate, 'MM-DD-YYYY') : moment();
    }

    isTrue(field) {
        return (field && (field.toLowerCase() === "y" || field.toLowerCase() === "yes" || field === true));
    }

    isFavPlanAvailable(planType){
        if (this.tabData.preEligibility && this.tabData.preEligibility.plans) {
            let favPlans = this.tabData.preEligibility.plans.filter(x => x.type.toLowerCase() === planType.toLowerCase());
            return favPlans && favPlans.length > 0 ? "Y" : "NO";
        } else {
            return "NO";
        }
    }

    getEnrollments(planType){
        if(!this.tabData.enrollments || this.tabData.enrollments.length <= 0)
            return [];

        let panTypeEnrollments = this.tabData.enrollments.filter(x => x.type.toLowerCase() === planType.toLowerCase());

        let enrollments = panTypeEnrollments.map(function (enrollment) {
            return {
                'plan': enrollment.plan,
                'memberCount': enrollment.enrolles && enrollment.enrolles.length > 0 ? enrollment.enrolles.length : 0
            }
        });

        return enrollments;
    }

    getPlanData(appObject, planType) {
        const enrollments = this.getEnrollments(planType);
        const isEnrollmentsPresent = enrollments && enrollments.length > 0;
        const { applicationStatus } = appObject;
        const appStatuses = ['ER', 'EN', 'PN', 'OP'];

        // Id 5, 6, 7, 8
        if (isEnrollmentsPresent && appStatuses.includes(applicationStatus)) {
            appObject.favoritePlanAvailable = "N/A";
            appObject.enrolledInDental = 'Y';
            return appObject;

        } else {
            appObject.favoritePlanAvailable = this.isFavPlanAvailable(planType);
        }

        if (appObject.applicationStatus === 'ER') { // Id 3, 4
            appObject.period = this.tabData.isInsideOEEnrollmentWindow ? 'OE' : 'OUTSIDE OE';
        }

        // DEFAULT CASE if no dental enrollments and no favorite dental enrollments. No need to check any application status
        if (!isEnrollmentsPresent && appObject.favoritePlanAvailable === 'NO') {
            appObject.applicationStatus = 'N/A';
        }

        return appObject;
    }

    getPlans(planType){
        let appObject =  {
            "applicationStatus": this.tabData.applicationStatus  ? this.tabData.applicationStatus : "NO",
            "period": "N/A",
            "enrolledInDental": "NO",
            "favoritePlanAvailable":"NO",
        };



        return this.getPlanData(appObject, planType);
    }

    getDentalPlanInfo() {
        return this.getPlans("dental");
    }
}

export { ProcessDentalPlans as default};