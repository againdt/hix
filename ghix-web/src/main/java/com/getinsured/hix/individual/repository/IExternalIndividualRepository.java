package com.getinsured.hix.individual.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.ExternalIndividual;

/**
 * Repository to fetch data from external individual table
 */
public interface IExternalIndividualRepository extends JpaRepository<ExternalIndividual, Long> {

	/**
	 * Retrieves data for external individual
	 * 
	 * @param extindividualid external individual id
	 * @return {@link ExternalIndividual}
	 */
	@Query("FROM ExternalIndividual as an where an.id = :extindividualid")
	ExternalIndividual findByIndividualCaseId(
			@Param("extindividualid") long extindividualid);

}