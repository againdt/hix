(function (models) {
	models.Tier = Backbone.Model.extend({
		
		urlRoot : "gettiers",
		
		url : function () {
			if(this.id)
				return 'gettiers/'+this.id+'?'+(Math.random()*10);
			else
				return 'gettiers?'+(Math.random()*10);
		},
		
		//to specify default attributes for model
		defaults: {
			//attrName : default value
			"tier"	: "",
			"planCount"	: "",
			"minPremium"	: "",
			"maxPremium"	: "",
			"minMonthlyEmployerCost"	:"",
			"maxMonthlyEmployerCost"	:"",
			"minMonthlyEmployeeCost"	:"",
			"maxMonthlyEmployeeCost"	:"",
			"minEmplrCostHeight"		:"",
			"maxEmplrCostHeight"		:"",
			"minEmpCostHeight"			:"",
			"maxEmpCostHeight"			:"",
			"notAffordableEmployees"	:"",
			"notAffordableEmpList" 		: ""
		}
	});
})(App.models);