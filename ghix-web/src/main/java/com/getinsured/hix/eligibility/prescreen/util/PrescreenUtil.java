/**
 * 
 */
package com.getinsured.hix.eligibility.prescreen.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.model.planmgmt.Member;
import com.getinsured.hix.model.planmgmt.TeaserPlanRequest;
import com.getinsured.hix.model.planmgmt.TeaserPlanResponse;
import com.getinsured.hix.model.prescreen.PrescreenData;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PrescreenServiceEndPoints;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenDTO;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenDeductions;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenHousehold;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenHouseholdMember;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenProfile;

/**
 * Utility class for Prescreen Eligibility
 * @author Nikhil Talreja
 * @since 17 April, 2013 
 * 
 */
@Component
public class PrescreenUtil {

	private static final Logger LOGGER = Logger.getLogger(PrescreenUtil.class);

	@Autowired private HIXHTTPClient hIXHTTPClient;

	private static ObjectMapper mapper = new ObjectMapper();

	private static final Set<Integer> NO_PLAN_FOUND_ERRORS = new HashSet<Integer>();
	private static final Map<String,String> ISSUER_IMAGE_URL_MAP = new HashMap<String, String>();
	
	private static final String PHYSICALS = "REHABILITATIVE_PHYSICAL_THERAPY";
	private static final String DEDUCTIBLE_INDIVIDUAL = "DEDUCTIBLE_IND";
	private static final String DEDUCTIBLE_FAMILY = "DEDUCTIBLE_FLY";
	private static final String GENERIC_DRUG = "GENERIC";
	private static final String DOCTOR_VISITS = "PRIMARY_VISIT";
	private static final String MAX_OUT_OF_POCKET_INDIVIDUAL = "OOP_MAX_IND";
	private static final String MAX_OUT_OF_POCKET_FAMILY = "OOP_MAX_FLY";
	private static final String DOLLAR_AMOUNT = "Dollar Amount";
	private static final String DOLLAR_AMOUNT_COPAY = "Dollar Amount copay";
	private static final String PERCENT_COINSURANCE = "Percent co-insurance";
	private static final String PLAN_METADATA_VALUE = "netwkValue";
	private static final String PLAN_METADATA_ATTRIBUTE = "netwkAttrib";
	
	private static final int FIFTEEN = 15;
	private static final int RESULTS_PAGE = 4;
	private static final int MEDICAID_AGE = 65;
	private static final int ERROR_CODE_LIMIT = 5000;
	
	//Generic error codes
	private static final int ERROR_999991 = 999991;
	private static final int ERROR_999992 = 999992;
	private static final int ERROR_999993 = 999993;
	private static final int ERROR_999994 = 999994;
	private static final int ERROR_999995 = 999995;
	private static final int ERROR_999996 = 999996;
	private static final int ERROR_999997 = 999997;
	
	static{
		NO_PLAN_FOUND_ERRORS.add(GhixConstants.PLAN_NOT_FOUND);
		
		//TODO - Add all issuers name here
		ISSUER_IMAGE_URL_MAP.put("United Health One", "logo_UnitedHealthOne.png");
		ISSUER_IMAGE_URL_MAP.put("Aetna", "logo_aetna.png");
		ISSUER_IMAGE_URL_MAP.put("Default", "logo_UnitedHealthOne.png");
	}

	/**
	 * This method makes a REST call to prescreen service to find APTC for a request
	 * @author Nikhil Talreja
	 * @since April 17, 2013
	 * @param request - The Prescreen Request for which APTC needs to be found
	 * @return String - APTC value
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	public PrescreenDTO calculateAptcREST(PrescreenDTO prescreenDTO) throws IOException, URISyntaxException{
		
		String postParams = mapper.writeValueAsString(prescreenDTO);
		LOGGER.info("Request " + postParams);
		String response = hIXHTTPClient.getPOSTData(PrescreenServiceEndPoints.GET_APTC_URL,postParams,PrescreenConstants.CONTENT_TYPE);

		if(response != null){
			try {
				PrescreenDTO prescreenDTORes = mapper.readValue(response, PrescreenDTO.class);
				return prescreenDTORes;
			} catch (Exception e) {
				LOGGER.error(e.getMessage(),e);
				return prescreenDTO;
			}
		}
		else{
			return prescreenDTO;
		}
	}

	/**
	 * This method gets a record from PRESCREEN_DATA table using primary key (id)
	 * @author Nikhil Talreja
	 * @since April 17, 2013
	 * @param id - The Prescreen Request Object Id to be fetched
	 * @return PrescreenData - The fetched record
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	public PrescreenData getPrescreenRecord(long id) throws IOException, URISyntaxException{
		String postParams = "\"" + id + "\"";
		LOGGER.info("Record Id " + postParams);

		String response = hIXHTTPClient.getPOSTData(PrescreenServiceEndPoints.GET_PRESCREEN_RECORD,postParams,PrescreenConstants.CONTENT_TYPE);

		PrescreenData record = null;
		if(StringUtils.isNotBlank(response)){
			record = mapper.readValue(response, PrescreenData.class);
		}

		return record;
	}

	/**
	 * This method saves a record in PRESCREEN_DATA table
	 * @author Nikhil Talreja
	 * @since April 17, 2013
	 * @param record - The Prescreen Request Object to be saved
	 * @return PrescreenData - The saved record
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	public PrescreenData savePrescreenRecord(PrescreenData record) throws IOException, URISyntaxException{

		String postParams = mapper.writeValueAsString(record);
		String response = hIXHTTPClient.getPOSTData(PrescreenServiceEndPoints.SAVE_PRESCREEN_RECORD,postParams,PrescreenConstants.CONTENT_TYPE);

		PrescreenData prescreenRecord = null;
		if(StringUtils.isNotBlank(response)){
			prescreenRecord = mapper.readValue(response, PrescreenData.class);
		}

		return prescreenRecord;
	}

	/**
	 * This method gets a record from PRESCREEN_DATA table using HHTP session id
	 * @author Nikhil Talreja
	 * @since April 17, 2013
	 * @param sessionId - The HTTP session id for the current session
	 * @return PrescreenData - The fetched record
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	public PrescreenData  getPrescreenRecordBySessionId(String sessionId) throws IOException, URISyntaxException{

		String response = hIXHTTPClient.getPOSTData(PrescreenServiceEndPoints.GET_PRESCREEN_RECORD_BY_SESSION,sessionId,PrescreenConstants.CONTENT_TYPE);

		PrescreenData record = null;
		if(StringUtils.isNotBlank(response)){
			record = mapper.readValue(response, PrescreenData.class);
		}

		return record;
	}

	/**
	 * This method copies values from Prescreen Request object to PrescreenData record
	 * @author Nikhil Talreja
	 * @since April 17, 2013
	 * @param record, PrescreenDTO
	 */
	public void updateFieldsForPrescreenRecord(PrescreenData record,
			PrescreenDTO prescreenDTO) {

		PrescreenProfile profile = prescreenDTO.getPrescreenProfile();
		PrescreenHousehold household = prescreenDTO.getPrescreenHousehold();
		PrescreenDeductions deductions = prescreenDTO.getPrescreenDeductions();

		//Updating profile Details
		if(profile != null){
			record.setName(profile.getClaimerName());
			record.setZipCode(profile.getZipCode());
			record.setIsMarried("N");
			if(profile.getIsMarried()){
				record.setIsMarried("Y");
			}
			record.setNoOfDependents(profile.getNumberOfDependents());
			record.setHouseholdIncome(profile.getTaxHouseholdIncome());
			record.setIsDisabled("N");
			if(prescreenDTO.getIsAnyMemberDisabled()){
				record.setIsDisabled("Y");
			}
			record.setStateCd(profile.getStateCode());
			record.setCounty(profile.getCounty());
		}

		//Updating household details
		updateHouseholdDetails(household,record);

		//Updating deductions
		if(deductions != null){
			record.setAlimonyDeduction(deductions.getAlimonyPaid());
			record.setStudentLoanDeduction(deductions.getStudentLoans());
		}

		//APTC, FPL, CSR and Plan
		record.setFinalCreditDisplayed(prescreenDTO.getAptcValue());
		record.setFpl(prescreenDTO.getFplValue());
		record.setCsr(prescreenDTO.getCsr());
		record.setFinalPlanShowed(prescreenDTO.getPlanId());
		record.setPremiumValue(prescreenDTO.getBenchmarkPlanPremium());
		record.setResultsType(prescreenDTO.getResultsType());
		
		//Benchmark premium call status
		if(StringUtils.isBlank(prescreenDTO.getErrorDescription())){
			record.setBenchmarkStatus("Y");
			record.setBenchmarkStatusDesc("Success");
		}
		else{
			record.setBenchmarkStatus("N");
			record.setBenchmarkStatusDesc(prescreenDTO.getErrorDescription());
		}
		
		//Session Time
		if(record.getSessionStartTime() != null && record.getSessionEndTime() != null){
			record.setTotalTimeSpent(record.getSessionEndTime().getTime() - record.getSessionStartTime().getTime());
		}

		//LOGGER.info("sessionData==="+sessionData);
		record.setSessionDataClob(addSessionDataToPrescreenRecord( profile,  household,
				 deductions, prescreenDTO));

		if(StringUtils.isNumeric(prescreenDTO.getCurrentTab()) && record.getScreenVisisted() != RESULTS_PAGE){
			record.setScreenVisisted(Integer.parseInt(prescreenDTO.getCurrentTab()));
		}


	}
	
	private String addSessionDataToPrescreenRecord(PrescreenProfile profile, PrescreenHousehold household,
			PrescreenDeductions deductions, PrescreenDTO prescreenDTO){
		
		LOGGER.info("Adding session data to Prescreen Data record");
		StringBuffer sessionData = new StringBuffer();

		sessionData.append("Claimer Name : ");
		sessionData.append(profile.getClaimerName());
		sessionData.append("\nZip Code : ");
		sessionData.append(profile.getZipCode());
		sessionData.append("\nIs Married : ");
		sessionData.append(profile.getIsMarried());
		sessionData.append("\nNo of dependents : ");
		sessionData.append(profile.getNumberOfDependents());
		sessionData.append("\nTax household income : ");
		sessionData.append(profile.getTaxHouseholdIncome());

		sessionData.append("\nHouse hold members : ");
		sessionData.append(household.getHouseholdMembers());
		sessionData.append("\nNo of members seeking coverage : ");
		sessionData.append(household.getNumberOfMembersSeekingCoverage());

		sessionData.append("\nMember incomes : ");
		sessionData.append(prescreenDTO.getPrescreenIncomes());

		sessionData.append("\nAlimony Deductions : ");
		sessionData.append(deductions.getAlimonyPaid());
		sessionData.append("\nStudent Loan Deductions : ");
		sessionData.append(deductions.getStudentLoans());

		sessionData.append("\nApplicable Percentage : ");
		sessionData.append(prescreenDTO.getApplicablePercentage());
		sessionData.append("\nBenchmark Premium : ");
		sessionData.append(prescreenDTO.getBenchmarkPlanPremium());
		sessionData.append("\nFPL : ");
		sessionData.append(prescreenDTO.getFplValue());
		sessionData.append("\nCSR : ");
		sessionData.append(prescreenDTO.getCsr());

		return sessionData.toString();
	}
	
	private void updateHouseholdDetails(PrescreenHousehold household,PrescreenData record ){

		//Updating household details
		if (household != null) {
			// Updating claimer DOB
			List<PrescreenHouseholdMember> members = household
					.getHouseholdMembers();
			if (!CollectionUtils.isEmpty(members)) {
				for (PrescreenHouseholdMember member : members) {
					if (StringUtils.equalsIgnoreCase(
							member.getRelationshipWithClaimer(), "self")
							&& validateDateOfBirth(member.getDateOfBirth())) {
						record.setClaimantDob(DateUtil.StringToDate(
								member.getDateOfBirth(),
								GhixConstants.REQUIRED_DATE_FORMAT));
						break;
					}
				}
			}
		}

	}
	/**
	 * This method makes a REST call to prescreen service to find APTC for a request
	 * @author Sunil Desu
	 * @since May 08, 2013
	 * @param request - The fpl for which CSR needs to be found
	 * @return String - CSR value
	 * @throws URISyntaxException 
	 * @throws IOException
	 */
	public String calculateCsrREST(double fpl) throws URISyntaxException{
		String response="";
		try {
		LOGGER.info("Request for CSR " + fpl);
		response = hIXHTTPClient.getPOSTData(PrescreenServiceEndPoints.CALCULATE_CSR_URL,Double.toString(fpl),PrescreenConstants.CONTENT_TYPE);
		} catch (ClientProtocolException e) {
			LOGGER.error("Error invoking CSR calculator "+e.getMessage());
		} catch (IOException e) {
			LOGGER.error("Error invoking CSR calculator "+e.getMessage());
		}
		return response;
	}

	/**
	 * Utility method to get age of a member
	 *
	 * @author Nikhil Talreja
	 * @since May 20, 2013
	 * @param String - The DOB in MM/dd/yyyy format
	 * @return int - age of the member
	 */
	public int getAgeFromDob(String dob){

		if(StringUtils.isBlank(dob)){
			return 0;
		}

		int age = 0;
	    int factor = 0;
	   
	    Date dobDate = null;
		dobDate = DateUtil.StringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
		
		Calendar cal1 = new GregorianCalendar();
	    Calendar cal2 = computeEffectiveDate();

	    cal1.setTime(dobDate);
	    if(cal2.get(Calendar.DAY_OF_YEAR) < cal1.get(Calendar.DAY_OF_YEAR)) {
	            factor = -1;
	    }
	    age = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR) + factor;

	    return age;
	}

	/**
	 * This method checks if a household is eligible for medicare.
	 * Used in results determination
	 *
	 * @author Nikhil Talreja
	 * @since May 17, 2013
	 * @param household - The household object for a request
	 * @return boolean - true/false
	 */
	public boolean checkMedicare(PrescreenHousehold household) throws ParseException{

		if(household == null || CollectionUtils.isEmpty(household.getHouseholdMembers())){
			return false;
		}

	    int age = 0;

		for(PrescreenHouseholdMember member : household.getHouseholdMembers()){

		    age = getAgeFromDob(member.getDateOfBirth());
		    if(age < MEDICAID_AGE){
		    	return false;
		    }
		}


		return true;
	}

	/**
	 * This method determines result type based on FPL value
	 *
	 * @author Nikhil Talreja
	 * @since May 17, 2013
	 * @param request - The prescreen request object
	 */
	public void determineResults(PrescreenDTO prescreenDTO) throws ParseException {

		if (checkMedicare(prescreenDTO.getPrescreenHousehold())) {
			prescreenDTO.setResultsType("Medicare");
			LOGGER.info("Results type : Medicare");
			return;
		}
		else{
			prescreenDTO.setResultsType(prescreenDTO.getCsr());
		}

		LOGGER.info("Results type : " + prescreenDTO.getResultsType());
	}

	/**
	 * This method checks if DOB is entered for all household members
	 *
	 * @author Nikhil Talreja
	 * @since May 17, 2013
	 * @param household
	 * @return boolean
	 */
	public boolean validateDateOfBirths(PrescreenHousehold household){

		if (household == null || CollectionUtils.isEmpty(household
				.getHouseholdMembers())) {

			return false;
		}

		List<PrescreenHouseholdMember> members = household
					.getHouseholdMembers();

			for (PrescreenHouseholdMember member : members) {
				if (StringUtils.isBlank(member.getDateOfBirth()) || !validateDateOfBirth(member.getDateOfBirth())) {
					LOGGER.info("Date of birth is missing/invalid for one or more members");
					return false;
				}
			}



		return true;
	}
	
	private boolean validateDateOfBirth(String dob){
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	    try{
	    	df.format(df.parse(dob));
	    }
	    catch(Exception e){
	    	return false;
	    }
		
	    return true;
	}
	
	public Calendar computeEffectiveDate(){
		Calendar effectiveDate = TSCalendar.getInstance();
		Calendar currentDate = TSCalendar.getInstance();

		Calendar fifteenthOfTheMonth = TSCalendar.getInstance();		
		fifteenthOfTheMonth.set(Calendar.DAY_OF_MONTH, FIFTEEN);
		fifteenthOfTheMonth.set(Calendar.HOUR_OF_DAY, 0);
		fifteenthOfTheMonth.set(Calendar.MINUTE, 0);
		fifteenthOfTheMonth.set(Calendar.SECOND, 0);
		fifteenthOfTheMonth.set(Calendar.MILLISECOND, 0);
		
		Calendar fifteenthOfDecember = TSCalendar.getInstance();
		fifteenthOfDecember.set(Calendar.MONTH, Calendar.DECEMBER);
		fifteenthOfDecember.set(Calendar.DAY_OF_MONTH, FIFTEEN);
		fifteenthOfDecember.set(Calendar.HOUR_OF_DAY, 0);
		fifteenthOfDecember.set(Calendar.MINUTE, 0);
		fifteenthOfDecember.set(Calendar.SECOND, 0);
		fifteenthOfDecember.set(Calendar.MILLISECOND, 0);

		Calendar fifteenthOfNovember = TSCalendar.getInstance();
		fifteenthOfNovember.set(Calendar.MONTH, Calendar.NOVEMBER);
		fifteenthOfNovember.set(Calendar.DAY_OF_MONTH, FIFTEEN);
		fifteenthOfNovember.set(Calendar.HOUR_OF_DAY, 0);
		fifteenthOfNovember.set(Calendar.MINUTE, 0);
		fifteenthOfNovember.set(Calendar.SECOND, 0);
		fifteenthOfNovember.set(Calendar.MILLISECOND, 0);
		try{
		if(currentDate.getTimeInMillis()>fifteenthOfNovember.getTimeInMillis()){

			if(currentDate.getTimeInMillis() < fifteenthOfDecember.getTimeInMillis()){
				effectiveDate.set(Calendar.YEAR,currentDate.get(Calendar.YEAR) + 1);
				effectiveDate.set(Calendar.MONTH, 0);
				effectiveDate.set(Calendar.DAY_OF_MONTH, 1);
				effectiveDate.set(Calendar.HOUR_OF_DAY, 0);
				effectiveDate.set(Calendar.MINUTE, 0);
				effectiveDate.set(Calendar.SECOND, 0);
				effectiveDate.set(Calendar.MILLISECOND, 0);
			}
			else{
				effectiveDate.set(Calendar.YEAR,currentDate.get(Calendar.YEAR) + 1);
				effectiveDate.set(Calendar.MONTH, 1);
				effectiveDate.set(Calendar.DAY_OF_MONTH, 1);
				effectiveDate.set(Calendar.HOUR_OF_DAY, 0);
				effectiveDate.set(Calendar.MINUTE, 0);
				effectiveDate.set(Calendar.SECOND, 0);
				effectiveDate.set(Calendar.MILLISECOND, 0);
			}
		
		}
		else{
			
			if(currentDate.getTimeInMillis() < fifteenthOfTheMonth.getTimeInMillis()){
				effectiveDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH) + 1);
				effectiveDate.set(Calendar.DAY_OF_MONTH, 1);
				effectiveDate.set(Calendar.HOUR_OF_DAY, 0);
				effectiveDate.set(Calendar.MINUTE, 0);
				effectiveDate.set(Calendar.SECOND, 0);
				effectiveDate.set(Calendar.MILLISECOND, 0);
			}
			else{
				effectiveDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH) + 2);
				effectiveDate.set(Calendar.DAY_OF_MONTH, 1);
				effectiveDate.set(Calendar.HOUR_OF_DAY, 0);
				effectiveDate.set(Calendar.MINUTE, 0);
				effectiveDate.set(Calendar.SECOND, 0);
				effectiveDate.set(Calendar.MILLISECOND, 0);
			}
		}
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("EffectiveDate is "+TSDate.getNoOffsetTSDate(effectiveDate.getTimeInMillis()));
		}
		}catch(Exception ex){
			LOGGER.info("Error computing EffectiveDate");
		}
		return effectiveDate;
	}
	
	/**
	 * This method makes a REST call to TeaserPlan API interface to find BenchmarkPlanPremium and Plan information for a household
	 * @author Sunil Desu
	 * @since June 05, 2013
	 * @param request - The prescreenDTO for which premium needs to be found
	 * @return TeaserPlanResponse 
	 * @throws IOException
	 */
	public TeaserPlanResponse invokeTeaserPlanAPI(PrescreenDTO prescreenDTO){
		String response="";

		TeaserPlanRequest teaserPlanRequest = new TeaserPlanRequest();
		TeaserPlanResponse teaserPlanResponse = new TeaserPlanResponse();
		
		try {
		LOGGER.info("Request for invoking TeaserPlanAPI " + prescreenDTO.toString());
		teaserPlanRequest = generateTeaserPlanRequest(prescreenDTO);
		String postParams = mapper.writeValueAsString(teaserPlanRequest);
		LOGGER.info("TeaserPlanRequest is: " + postParams);
		response = hIXHTTPClient.getPOSTData(GhixEndPoints.PlanMgmtEndPoints.TEASER_PLAN_API_URL,postParams,PrescreenConstants.CONTENT_TYPE);
		if(StringUtils.isNotBlank(response)){
			try {
				teaserPlanResponse = mapper.readValue(response, TeaserPlanResponse.class);
			} catch (JsonParseException e) {
				LOGGER.error("JsonParseException parsing TeaserPlan API response "+e.getMessage());
				teaserPlanResponse.setErrCode(ERROR_999991);
				teaserPlanResponse.setErrMsg("Error parsing response from Teaser Plan API.");
			} catch (JsonMappingException e) {
				LOGGER.error("JsonMappingException parsing TeaserPlan API response "+e.getMessage());
				teaserPlanResponse.setErrCode(ERROR_999992);
				teaserPlanResponse.setErrMsg("Error mapping response from Teaser Plan API.");
			} catch (IOException e) {
				LOGGER.error("IOException parsing TeaserPlan API response "+e.getMessage());
				teaserPlanResponse.setErrCode(ERROR_999993);
				teaserPlanResponse.setErrMsg("IO exception during Teaser Plan API call.");
			}
		}else{
			teaserPlanResponse.setErrCode(ERROR_999994);
			teaserPlanResponse.setErrMsg("Empty response from Teaser Plan API.");
		}
		} catch (ClientProtocolException e) {
			LOGGER.error("ClientProtocol Error invoking Teaser Plan API interface: "+e.getMessage());
			teaserPlanResponse.setErrCode(ERROR_999995);
			teaserPlanResponse.setErrMsg("ClientProtocol Error invoking Teaser Plan API.");
		} catch (IOException e) {
			LOGGER.error("IO Error invoking Teaser Plan API interface: "+e.getMessage());
			teaserPlanResponse.setErrCode(ERROR_999996);
			teaserPlanResponse.setErrMsg("IO Error while invoking Teaser Plan API.");
		}catch (Exception e) {
			LOGGER.error("System Error invoking Teaser Plan API interface: "+e.getMessage());
			teaserPlanResponse.setErrCode(ERROR_999997);
			teaserPlanResponse.setErrMsg("System error invoking Teaser Plan API.");
		}
		
		LOGGER.info("Premium value fetched is "+teaserPlanResponse.getBenchmarkPremium());
		return teaserPlanResponse;
	}

	private TeaserPlanRequest generateTeaserPlanRequest(PrescreenDTO prescreenDTO){
		TeaserPlanRequest teaserPlanRequest= new TeaserPlanRequest();
		PrescreenHousehold prescreenHousehold = prescreenDTO.getPrescreenHousehold();
		List<Member> members = new ArrayList<Member>();
		Member member;
		SimpleDateFormat jspDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		boolean isPrimaryAdded = false;
		try{
			for(PrescreenHouseholdMember householdMember: prescreenHousehold.getHouseholdMembers()){
				if (!householdMember.isSeekingCoverage()) {
					continue;
				}
				member = new Member();
				createTeaserPlanMember(member,householdMember, isPrimaryAdded);
				if(householdMember.getDateOfBirth()!=null && !householdMember.getDateOfBirth().trim().equals("")){
					Calendar dateOfBirth = TSCalendar.getInstance();
					dateOfBirth.setTime(jspDateFormat.parse(householdMember.getDateOfBirth()));
					member.setDateOfBirth(dateOfBirth);
					member.setAge(getAgeFromDob(householdMember.getDateOfBirth()));
					//LOGGER.info("Date is "+dateOfBirth.getTime());
					//LOGGER.info("Age is " + member.getAge());
				}
				member.setTobaccoUser(false);
				members.add(member);
			}
			if(!isPrimaryAdded){
				for(Member membr:members){
					if(membr.getMemberType().equalsIgnoreCase("spouse")){
						membr.setMemberType("primary");
						LOGGER.info("Primary tax filer is not an applicant. Making spouse as primary.");
						break;
					}
				}
			}
			teaserPlanRequest.setMembers(members);
			teaserPlanRequest.setEffectiveDate(computeEffectiveDate());
			teaserPlanRequest.setState(prescreenDTO.getPrescreenProfile().getStateCode());
			teaserPlanRequest.setCounty(prescreenDTO.getPrescreenProfile().getCounty());
			teaserPlanRequest.setZipCode(prescreenDTO.getPrescreenProfile().getZipCode());
			teaserPlanRequest.setAppID("Prescreen");
			teaserPlanRequest.setRequestTime(TSCalendar.getInstance());
			teaserPlanRequest.setCostSharingVariation(prescreenDTO.getCsr());
		}catch(Exception ex){
			LOGGER.error("Error generating household request");
		}

		LOGGER.info("TeaserPlanRequest request is "+teaserPlanRequest.toString());
		return teaserPlanRequest;
	}
	
	private void createTeaserPlanMember(Member member,PrescreenHouseholdMember householdMember, boolean isPrimaryAdded ){
		
		if (householdMember.getRelationshipWithClaimer().equals("self")) {
			member.setMemberType("primary");
			member.setGender("MALE");
			member.setStudent(false);
			isPrimaryAdded=true;
		} else if (householdMember.getRelationshipWithClaimer().equals(
				"spouse")) {
			member.setMemberType("spouse");
			member.setGender("FEMALE");
			member.setStudent(false);
		} else {
			member.setMemberType("child");
			member.setGender("FEMALE");
			member.setStudent(true);
		}
		
	}
	
	public boolean checkForErrorCondition(PrescreenDTO prescreenDTO, TeaserPlanResponse teaserPlanResponse){
		boolean isErroneous = false;
		if(teaserPlanResponse.getErrCode() > ERROR_CODE_LIMIT){
			prescreenDTO.setErrorDescription(teaserPlanResponse.getErrCode()+":"+((teaserPlanResponse.getErrMsg()==null||teaserPlanResponse.getErrMsg().trim().length()==0)?"Error returned by TeaserPlan API.":teaserPlanResponse.getErrMsg()));
			prescreenDTO.setErrorMessage(getErrorMessageFromErrorCode(teaserPlanResponse.getErrCode()));
			isErroneous = true;
		}else{
			prescreenDTO.setErrorDescription(null);
			prescreenDTO.setErrorMessage(null);
		}
		LOGGER.info("Error from teaser plan: "+prescreenDTO.getErrorDescription());

		return isErroneous;
	}

	/**
	 * @author Sunil Desu
	 * @since June 07 2013
	 * 
	 * @param errorCode
	 * @param errorMessage
	 */
	private String getErrorMessageFromErrorCode(int errorCode){
		String errorMessage ="Your request cannot be completed at this time. Please try again later.";
		if(NO_PLAN_FOUND_ERRORS.contains(errorCode)){
			errorMessage="No plans can be found for the provided data.";
		}
		return errorMessage;
	}
	
	/**
	 * @author Sunil Desu
	 * @since June 07 2013
	 * 
	 */
	public void extractPlanAndPremiumDetailsFromTeaserResponse(PrescreenDTO prescreenDTO, TeaserPlanResponse teaserPlanResponse){
		prescreenDTO.setBenchmarkPlanPremium(teaserPlanResponse.getBenchmarkPremium());
		prescreenDTO.setPlanId(teaserPlanResponse.getBenchmarkPlanId());	
		prescreenDTO.setPlanData(extractPlanData(prescreenDTO, teaserPlanResponse));
	}
	
	/**
	 * @author Sunil Desu
	 * @since June 11, 2013
	 */
	private Map<String,String> extractPlanData(PrescreenDTO prescreenDTO,TeaserPlanResponse teaserPlanResponse){
		Map<String,String> planData = new HashMap<String, String>();
		
		Map<String, HashMap<String,String>> benefits = teaserPlanResponse.getBenefits();
		HashMap<String,String> benefit = null;
		
		String[] individualBenefitsName = {DOCTOR_VISITS,PHYSICALS,MAX_OUT_OF_POCKET_INDIVIDUAL,
				GENERIC_DRUG,DEDUCTIBLE_INDIVIDUAL};
		
		String[] familyBenefitsName = {DOCTOR_VISITS,PHYSICALS,MAX_OUT_OF_POCKET_FAMILY,
				GENERIC_DRUG,DEDUCTIBLE_FAMILY};
		
		String[] benefitsHeaders = {"Doctor Visits","Physicals","Max Out of Pocket",
				"Generic Drug","Deductible"};
		
		String[] actualBenfits = {};
		
		if(!CollectionUtils.isEmpty(benefits)){
			
			LOGGER.info("Adding benefits to plan data " + benefits);
			
			actualBenfits = individualBenefitsName;
			if(prescreenDTO.getPrescreenHousehold().getNumberOfMembersSeekingCoverage() > 1){
				actualBenfits = familyBenefitsName;
			}
			
			for(int i=0; i<actualBenfits.length; i++){
				
				benefit = benefits.get(actualBenfits[i]);
				if(!CollectionUtils.isEmpty(benefit) && StringUtils.isNotBlank(benefit.get(PLAN_METADATA_VALUE))){	
					if(StringUtils.equalsIgnoreCase(benefit.get(PLAN_METADATA_ATTRIBUTE), DOLLAR_AMOUNT) ||
								StringUtils.equalsIgnoreCase(benefit.get(PLAN_METADATA_ATTRIBUTE),DOLLAR_AMOUNT_COPAY)){
							planData.put(benefitsHeaders[i], "$"+benefit.get(PLAN_METADATA_VALUE));
					}
					else if(StringUtils.equalsIgnoreCase(benefit.get(PLAN_METADATA_ATTRIBUTE),PERCENT_COINSURANCE)){
							planData.put(benefitsHeaders[i], benefit.get(PLAN_METADATA_VALUE)+"%");						
					}
				}
			}
			
			LOGGER.info("Plan Data " + planData);
			
			prescreenDTO.setPlanName(teaserPlanResponse.getPlanName());
			
			//Getting issuer image url
			String imgUrl = ISSUER_IMAGE_URL_MAP.get(teaserPlanResponse.getIssuerName());
			if(StringUtils.isBlank(imgUrl)){
				imgUrl = ISSUER_IMAGE_URL_MAP.get("Default");
			}
			prescreenDTO.setPlanImgUrl(imgUrl);
		}
		return planData;
	}
}
