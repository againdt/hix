 <%@ include file="../include.jsp" %>
 <%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>

 
 <div id="appscr53" style="display: none;">
 	<div class="row-fluid">
 		<div class="page-title">
			<h5 class="titleForPages"><spring:message code="ssap.page3.primaryContact" text="Primary Contact Information" /></h5>
			<%-- <label class="titleForPages"><spring:message code="ssap.page2.upperContent" text="Start Your Application" /></label> --%>
		</div>
		
		<form method="POST" action="#" class="form-horizontal gutter10" id="primary-contact-information-form" autocomplete="off" data-parsley-excluded=":hidden">
			
			<div class="header margin10-b">
				<h4><spring:message code="ssap.page3.primaryContactName.header" /></h4>
			</div>
			
			<div class="alert alert-info hide">
				<label for="checkIsAccountHolder" class="checkbox margin20-l">
				<input type="checkbox" id="checkIsAccountHolder" name="checkIsAccountHolder" value="true" />
				<spring:message code="ssap.page3.primaryContact1" text="Check here if you are the account holder." />
			</label>
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<label class="required control-label" for="firstName">
					<spring:message code="ssap.firstName" text="First Name" />
					<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</label><!-- end of label -->
				<div class="controls">
					<input type="text" id="firstName" name="firstName" class="input-large" placeholder="<spring:message code='ssap.firstName' text='First Name' />" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.firstNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.firstNameRequired'/>" />
					 
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<label class="control-label" for="middleName">
					<spring:message code='ssap.middleName' text='Middle Name' />
				</label><!-- end of label -->
				<div class="controls">
					<input type="text" id="middleName" name="middleName" class="input-large" placeholder="<spring:message code='ssap.middleName' text='Middle Name' />" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.middleNameValid'/>" />
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<label class="required control-label" for="lastName">
					<spring:message code='ssap.lastName' text='Last Name' />
					<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					<span aria-label="Required!"></span>
				</label><!-- end of label -->
				<div class="controls">
					<input type="text" id="lastName" name="lastName" class="input-large" placeholder="<spring:message code='ssap.lastName' text='Last Name' />" data-parsley-pattern="^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$"  data-parsley-pattern-message="<spring:message code='ssap.error.lastNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.lastNameRequired'/>" />
					 
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<label class="control-label" for="appscr53Suffix">
					<spring:message code="ssap.suffix" text="Suffix" />
				</label><!-- end of label -->
				<div class="controls">
					<select id="appscr53Suffix" name="appscr53Suffix" class="input-large">
						<option value=""><spring:message code="ssap.suffix" text="Suffix" /></option>
						<option value="Jr."><spring:message code="ssap.JR" text="Jr." /></option>
						<option value="Sr."><spring:message code="ssap.SR" text="Sr." /></option>
						<option value="III"><spring:message code="ssap.III" text="III" /></option>
						<option value="IV"><spring:message code="ssap.IV" text="IV" /></option>
					</select>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<label class="required control-label capitalize-none" for="dateOfBirth">
					<spring:message code="ssap.dateOfBirth" text="Date of Birth" />
					<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					<span aria-label="Required!"></span>
				</label><!-- end of label -->
				<div class="controls">
					<div class="input-append date date-picker"  data-date="">
						<input type="text" id="dateOfBirth" name="dateOfBirth" class="input-medium dob" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-errors-container="#dateOfBirthErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.dobRequired'/>" />
						<span class="add-on dateSpan"><i class="icon-calendar"></i></span>
						<div id="dateOfBirthErrorDiv"></div>   
					</div>
					 
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<label class="required control-label" for="emailAddress">
					<spring:message code="ssap.page3.primaryContactEmail" text="Email Address"/>
					<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					<span aria-label="Required!"></span>
				</label><!-- end of label -->
				<div class="controls">
					<input type="text" id="emailAddress" name="emailAddress" class="input-large" placeholder="<spring:message code='ssap.page3.primaryContactPlaceholderEmail' text='Email Address' />"  data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.emailRequired'/>" data-parsley-pattern="^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$"  data-parsley-pattern-message="<spring:message code='ssap.error.emailValid'/>" data-parsley-length="[5, 100]" data-parsley-length-message="<spring:message code='ssap.error.emailValid'/>"/>
					 
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<h4><spring:message code="ssap.page3.primaryContactContactAddress" text="Contact Home Address" /></h4>
			<div class="profile addressBlock">
				<div class="control-group">
					<label class="required control-label" for="home_addressLine1">
						<spring:message code='ssap.address1' text='Address 1' />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="home_addressLine1" name="home_addressLine1" class="input-large addressValidation" placeholder="<spring:message code='ssap.address1' text='Address 1' />" data-parsley-pattern="^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{5,75}$"  data-parsley-pattern-message="<spring:message code='ssap.error.addressValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.addressRequired'/>" />
						  
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="home_addressLine2">
						<spring:message code='ssap.address2' text='Address 2' />
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="home_addressLine2" name="home_addressLine2" class="input-large addressValidation" placeholder="<spring:message code='ssap.address2' text='Address 2' />" data-parsley-pattern="^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{5,75}$"  data-parsley-pattern-message="<spring:message code='ssap.error.addressValid'/>"/>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="home_primary_city">
						<spring:message code='ssap.city' text='City' />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="home_primary_city" name="home_primary_city" class="input-large addressValidation" placeholder="<spring:message code='ssap.city' text='City' />"  data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.cityRequired'/>"  data-parsley-pattern="^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$"  data-parsley-pattern-message="<spring:message code='ssap.error.cityValid'/>" data-parsley-length="[1, 100]" data-parsley-length-message="<spring:message code='ssap.error.cityValid'/>"/>
						 
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="home_primary_zip">
						<spring:message code='ssap.zip' text='Zip' />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="home_primary_zip" name="home_primary_zip" onchange="getCountyList(this.value,'', this.id);" class="zipCode input-large addressValidation" placeholder="<spring:message code='ssap.zip' text='Zip' />" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.zipRequired'/>" data-parsley-pattern="^[0-9]{5}?$"  data-parsley-pattern-message="<spring:message code='ssap.error.zipValid'/>"/>
						<div id="home_primary_zipErrorDiv" style="display:none"> <div class="errorsWrapper filled"><span class="validation-error-text-container " id="home_primary_zipErrorSpan"></span></div></div>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="home_primary_state">
						<spring:message code="ssap.State" text="State" />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<select data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.stateRequired'/>" id="home_primary_state" name="home_primary_state" class="input-large addressValidation">
							<option value=''><spring:message code="ssap.State" text="State" /></option>
							<option selected value='<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>'><spring:message code="ssap.state.Idaho"  text="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME)%>" /></option>
							<!-- <option value="NM">New Mexico</option> -->
							
						</select>
						 
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="home_primary_county">
						<spring:message code="ssap.County" text="County" />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<select data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.countyRequired'/>" id="home_primary_county" name="home_primary_county" class="input-large" data-rel="countyPopulate">
							<option value=""><spring:message code="ssap.County" text="Select County" /></option>
							<%-- <option value="Bernalillo"><spring:message code="ssap.county.Bernalillo" text="Bernalillo" /></option> --%>
						</select>
						 
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
			</div>
			
			<h4><spring:message code="ssap.page3.primaryContactMailingAddress" text="Contact Mailing Address" /></h4>
			
			<div class="control-group">
				<div class="controls">
					<label for="mailingAddressIndicator" class="checkbox">
						<input type="checkbox" id="mailingAddressIndicator" name="mailingAddressIndicator" value="false" onclick="addressIsSame();" />
						<spring:message code="ssap.page3.primaryContactAndMailAddressSame" text=" Select if it's the same as Contact Home Address." />
					</label>
				 </div>
			</div><!-- control-group ends -->
			
			<div class="physicalmailingAddress addressBlock">
				<div class="control-group">
					<label class="required control-label" for="mailing_addressLine1">
						<spring:message code='ssap.address1' text='Address 1' />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="mailing_addressLine1" name="mailing_addressLine1" class="input-large addressValidation" placeholder="<spring:message code='ssap.address1' text='Address 1' />" data-parsley-pattern="^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{5,75}$"  data-parsley-pattern-message="<spring:message code='ssap.error.addressValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.addressRequired'/>" />
						  
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="mailing_addressLine2">
						<spring:message code='ssap.address2' text='Address 2' />
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="mailing_addressLine2" name="mailing_addressLine2" class="input-large addressValidation" placeholder="<spring:message code='ssap.address2' text='Address 2' />" data-parsley-pattern="^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{5,75}$"  data-parsley-pattern-message="<spring:message code='ssap.error.addressValid'/>"/>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="mailing_primary_city">
						<spring:message code='ssap.city' text='City' />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="mailing_primary_city" name="mailing_primary_city" class="input-large addressValidation"  placeholder="<spring:message code='ssap.city' text='City' />" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.cityRequired'/>"  data-parsley-pattern="^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$"  data-parsley-pattern-message="<spring:message code='ssap.error.cityValid'/>" data-parsley-length="[1, 100]" data-parsley-length-message="<spring:message code='ssap.error.cityValid'/>" />
						   
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="mailing_primary_zip">
						<spring:message code='ssap.zip' text='Zip' />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" id="mailing_primary_zip" name="mailing_primary_zip " onchange="getCountyListForState('mailing_primary_zip','mailing_primary_state','');"  class="input-large addressValidator zipCode addressValidation" placeholder="<spring:message code='ssap.zip' text='Zip' />" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.zipRequired'/>" data-parsley-pattern="^[0-9]{5}?$"  data-parsley-pattern-message="<spring:message code='ssap.error.zipValid'/>" />
						 
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label" for="mailing_primary_state">
						<spring:message code="ssap.State" text="State" />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<select data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.stateRequired'/>" id="mailing_primary_state" name="mailing_primary_state" class="input-large addressValidation" onchange="getCountyListForState('mailing_primary_zip','mailing_primary_state','');">
							<option value=''><spring:message code="ssap.State" text="State" /></option>
							<option value='AL'><spring:message code="ssap.state.Albama" text="Alabama" /></option>
							<option value='AK'><spring:message code="ssap.state.Alaska" text="Alaska" /></option>
							<option value='AS'><spring:message code="ssap.state.AmericanSamoa" text="American Samoa" /></option>
							<option value='AZ'><spring:message code="ssap.state.Arizona" text="Arizona" /></option>
							<option value='AR'><spring:message code="ssap.state.Arkansas" text="Arkansas" /></option>
							<option value='CA'><spring:message code="ssap.state.California" text="California" /></option>
							<option value='CO'><spring:message code="ssap.state.Colorado" text="Colorado" /></option>
							<option value='CT'><spring:message code="ssap.state.Connecticut" text="Connecticut" /></option>
							<option value='DE'><spring:message code="ssap.state.Delaware" text="Delaware" /></option>
							<option value='DC'><spring:message code="ssap.state.DistrictOfColumbia" text="District of Columbia" /></option>
							<option value='FL'><spring:message code="ssap.state.Florida" text="Florida" /></option>
							<option value='GA'><spring:message code="ssap.state.Georgia" text="Georgia" /></option>
							<option value='GU'><spring:message code="ssap.state.Guam" text="Guam" /></option>
							<option value='HI'><spring:message code="ssap.state.Hawaii" text="Hawaii" /></option>
							<option value='ID'><spring:message code="ssap.state.Idaho" text="Idaho" /></option>
							<option value='IL'><spring:message code="ssap.state.Illinois" text="Illinois" /></option>
							<option value='IN'><spring:message code="ssap.state.Indiana" text="Indiana" /></option>
							<option value='IA'><spring:message code="ssap.state.Iowa" text="Iowa" /></option>
							<option value='KS'><spring:message code="ssap.state.Kansas" text="Kansas" /></option>
							<option value='KY'><spring:message code="ssap.state.Kentucky" text="Kentucky" /> </option>
							<option value='LA'><spring:message code="ssap.state.Louisiana" text="Louisiana" /></option>
							<option value='ME'><spring:message code="ssap.state.Maine" text="Maine" /></option>
							<option value='MD'><spring:message code="ssap.state.Maryland" text="Maryland" /></option>
							<option value='MA'><spring:message code="ssap.state.Massachusetts" text="Massachusetts" /></option>
							<option value='MI'><spring:message code="ssap.state.Michigan" text="Michigan" /></option>
							<option value='MN'><spring:message code="ssap.state.Minnesota" text="Minnesota" /></option>
							<option value='MS'><spring:message code="ssap.state.Mississippi" text="Mississippi" /></option>
							<option value='MO'><spring:message code="ssap.state.Missouri" text="Missouri" /></option>
							<option value='MT'><spring:message code="ssap.state.Montana" text="Montana" /></option>
							<option value='NE'><spring:message code="ssap.state.Nebraska" text="Nebraska" /></option>
							<option value='NV'><spring:message code="ssap.state.Nevada" text="Nevada" /></option>
							<option value='NH'><spring:message code="ssap.state.NewHampshire" text="New Hampshire" /></option>
							<option value='NJ'><spring:message code="ssap.state.NewJersey" text="New Jersey" /></option>
							<option value='NM'><spring:message code="ssap.state.NewMexico" text="New Mexico" /></option>
							<option value='NY'><spring:message code="ssap.state.NewYork" text="New York" /></option>
							<option value='NC'><spring:message code="ssap.state.NorthCarolina" text="North Carolina" /></option>
							<option value='ND'><spring:message code="ssap.state.NorthDakota" text="North Dakota" /></option>
							<option value='MP'><spring:message code="ssap.state.NorthernMarianasIslands" text="Northern Marianas Islands" /></option>
							<option value='OH'><spring:message code="ssap.state.Ohio" text="Ohio" /></option>
							<option value='OK'><spring:message code="ssap.state.Oklahoma" text="Oklahoma" /></option>
							<option value='OR'><spring:message code="ssap.state.Oregon" text="Oregon" /></option>
							<option value='PA'><spring:message code="ssap.state.Pennsylvania" text="Pennsylvania" /></option>
							<option value='PR'><spring:message code="ssap.state.PuertoRico" text="Puerto Rico" /></option>
							<option value='RI'><spring:message code="ssap.state.RhodeIsland" text="Rhode Island" /></option>
							<option value='SC'><spring:message code="ssap.state.SouthCarolina" text="South Carolina" /></option>
							<option value='SD'><spring:message code="ssap.state.SouthDakota" text="South Dakota" /></option>
							<option value='TN'><spring:message code="ssap.state.Tennessee" text="Tennessee" /></option>
							<option value='TX'><spring:message code="ssap.state.Texas" text="Texas" /></option>
							<option value='UT'><spring:message code="ssap.state.Utah" text="Utah" /></option>
							<option value='VT'><spring:message code="ssap.state.Vermont" text="Vermont" /></option>
							<option value='VA'><spring:message code="ssap.state.Virginia" text="Virginia" /></option>
							<option value='VI'><spring:message code="ssap.state.VirginIslands" text="Virgin Islands" /></option>
							<option value='WA'><spring:message code="ssap.state.Washington" text="Washington" /></option>
							<option value='WV'><spring:message code="ssap.state.WestVirginia" text="West Virginia" /></option>
							<option value='WI'><spring:message code="ssap.state.Wisconsin" text="Wisconsin" /></option>
							<option value='WY'><spring:message code="ssap.state.Wyoming" text="Wyoming" /></option>						
						</select>
						 
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group"> <!-- Hide county HIX-51353 -->
					<label class="control-label" for="mailing_primary_county">
						<spring:message code="ssap.County" text="County" />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<select data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.countyRequired'/>" id="mailing_primary_county" name="mailing_primary_county" class="input-large" data-rel="countyPopulate">
							<option value=""><spring:message code="ssap.County" text="Select County" /></option>
						</select>
						  
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
			</div>
			
			<h4><spring:message code="ssap.page3.primaryContactPhone" text="Contact Phone" /></h4>
			
			<div class="control-group">
				<label class="control-label" for="first_phoneNo">
					<spring:message code="ssap.cellPhone" text="Cell" />
				</label><!-- end of label -->
				<div class="controls">
					<div class="pull-left">
						<input type="text" id="first_phoneNo" name="first_phoneNo" class="phoneNumber input-medium" placeholder="(xxx) xxx-xxxx"  data-parsley-pattern="^\(\d{3}\) \d{3}-\d{4}$" data-parsley-errors-container="#first_phoneNoErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumValid'/>"/>
					</div>
					<label class="control-label aria-hidden" for="first_ext">
						<spring:message code="ssap.accessibility.cellExt" />
					</label><!-- end of label -->
					<div class="pull-left margin5-l hide">
						<input type="text" id="first_ext" name="first_ext" class="input-mini phoneNumberExt" placeholder="<spring:message code='ssap.phoneExt' text='Ext.' />" data-parsley-pattern="^\d{4}$" data-parsley-errors-container="#first_extErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumExtValid'/>"/>
					</div>
					<div id="first_phoneNoErrorDiv"></div>
					<div id="first_extErrorDiv"></div>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<label class="control-label" for="first_homePhoneNo">
					<spring:message code="ssap.homePhone" text="Home" />
				</label><!-- end of label -->
				<div class="controls">
					<div class="pull-left">
						<input type="text" id="first_homePhoneNo" name="first_homePhoneNo" class="phoneNumber input-medium" placeholder="(xxx) xxx-xxxx" data-parsley-pattern="^\(\d{3}\) \d{3}-\d{4}$" data-parsley-errors-container="#first_homePhoneNoErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumValid'/>"/>
					</div>
					<label class="control-label aria-hidden" for="first_homeExt">
						<spring:message code="ssap.accessibility.homeExt" />
					</label><!-- end of label -->
					<div class="pull-left margin5-l">
						<input type="text" id="first_homeExt" name="first_homeExt" class="input-mini phoneNumberExt" placeholder="<spring:message code='ssap.phoneExt' text='Ext.' />" data-parsley-pattern="^\d{4}$" data-parsley-errors-container="#first_homeExtErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumExtValid'/>"/>
					</div>
					<div id="first_homePhoneNoErrorDiv"></div>
					<div id="first_homeExtErrorDiv"></div>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<!--<div class="control-group">
				<label for="first_workPhoneNo" class="control-label">
					<spring:message code="ssap.workPhone" text="Work" />
				</label>
				<div class="controls">
					<input type="text" id="first_workPhoneNo" name="first_workPhoneNo" class="phoneNumber input-medium" placeholder="<spring:message code='ssap.phonePlaceholder' text='(xxx) xxx-xxxx' />" />
					<input type="text" id="first_workExt" name="first_workExt" class="input-mini" placeholder="<spring:message code='ssap.phoneExt' text='Ext.' />" />
				</div>
			</div>--><!-- control-group ends -->
			
			<h4><spring:message code="ssap.page3.primaryContactContactPreferences" text="Contact Preferences" /></h4>
			
			<div class="control-group">
				<label class="control-label" for="preffegred_spoken_language">
					<spring:message code="ssap.page3.primaryContactPreferredSpokenLanguage" text="Preferred Spoken Language" />
				</label><!-- end of label -->
				<div class="controls">
					<select id="preffegred_spoken_language" name="preffered_spoken_language" class="input-medium">
						<option value="English"><spring:message code="ssap.page3.primaryContactEnglish" text="English" /></option>
						<option value="Spanish"><spring:message code="ssap.page3.primaryContactSpanish" text="Spanish" /></option>
					</select>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<label class="control-label" for="preffered_written_language">
					<spring:message code="ssap.page3.primaryContactPreferredWrittenLanguage" text="Preferred Written Language" />
				</label><!-- end of label -->
				<div class="controls">
					<select id="preffered_written_language" name="preffered_written_language" class="input-medium">
						<option value="English"><spring:message code="ssap.page3.primaryContactEnglish" text="English" /></option>
						<option value="Spanish"><spring:message code="ssap.page3.primaryContactSpanish" text="Spanish" /></option>
					</select>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<%-- <p><spring:message code="ssap.page3.primaryContactInformation" text="I can get information about this application by (select all that apply):" /></p> --%>
			<div class="control-group">
				<fieldset>
					<%-- <legend class="aria-hidden"><spring:message code="ssap.page3.primaryContactInformation" text="I can get information about this application by (select all that apply):"/> <spring:message  code='label.required'/></legend> --%>
					<legend class="control-label">
						<spring:message code="ssap.page3.primaryContactInformation" text="I can get information about this application by (select all that apply):"/>
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</legend>
					<div class="controls">
						<label for="email">
							<input type="radio" id="email" name="contactPreference" value="email" data-parsley-errors-container ="#contactPreferenceErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field'/>"/> 
							<spring:message code="ssap.email" text="Email" /> 
						</label>
						<label for="inTheEmail">
							<input type="radio" id="inTheEmail" name="contactPreference" value="inTheEmail" />
							<spring:message code="ssap.page3.primaryContactInTheMail" text="In The mail" />
						</label>
						<%if(!"ID".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE))){%>
							<label for="emailAndMail">
								<input type="radio" id="emailAndMail" name="contactPreference" value="EmailAndMail" />
								<spring:message code="ssap.page3.primaryContactBoth" text="Both (E-mail and Postal Mail)" />
							</label>
						<% }%> 
						<label for="noEmailMail">
							<input type="radio" id="noEmailMail" name="contactPreference" value="None" onclick="showOptOutNotificationsModal()"/>
							<spring:message code="ssap.page3.primaryContactNone" text="None" />
						</label> 
						<div id= "contactPreferenceErrorDiv"></div>
					</div>
				</fieldset>
				<p class="alert alert-info margin0">
  					<spring:message code="indportal.communicationPreferences.changeCommunicationMethod" />
				</p>
			</div><!-- control-group ends -->			
		</form>
 	</div>
</div>	

<div id="addressModal" class="modal hide address-modal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3><spring:message code="ssap.page3.checkYourAddress"/></h3>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="ssap.page3.cancelbutton"/></button>
				<button class="btn btn-primary" id="modalAddressOk" onclick="addressSelected();"><spring:message code="ssap.page3.ok"/></button>
			</div>
 </div>
  <div id="addressErrorModal" class="modal hide address-modal" tabindex="-1" role="dialog"  aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3><spring:message code="ssap.page3.confrimAddress"/></h3>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="ssap.page3.close"/></button>
			</div>
		</div>

<div class="txt-left">
<!-- addressMatchModal -->
<div class="modal hide fade addressMatchModal address-modal" id="addressMatchModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Select your address</h3>
	</div>
	<div class="modal-body">
		<div>
			<h4>You entered</h4>
			<p class="margin10-l enteredAddress">
			</p>
		</div>
		
		<div class="margin30-t">
			<h4>We found</h4>
			<div class="suggestedAddress">
			</div>
		</div>

	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
		<button class="btn btn-primary updateAddress" data-dismiss="modal">OK</button>
	</div>
</div>
<!-- addressMatchModal End-->


<!-- addressNotFoundModal -->
<div class="modal hide fade addressNotFoundModal address-modal" id="addressNotFoundModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Address not found</h3>
	</div>
	<div class="modal-body">
		<div>
			<h4>You entered</h4>
			<p class="margin10-l enteredAddress"></p>
		</div>
		
		<p class="alert alert-error margin20-t">The address you entered is not in the postal database. Please check it for accuracy. Click OK to proceed with the address you entered.</p>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal">Cancel</button>
		<button class="btn btn-primary" data-dismiss="modal">OK</button>
	</div>
</div>
<!-- addressNotFoundModal End-->

<!-- Modal-->
<div id="optOutNotificationsModal" class="modal fade hide" tabindex="-1" role="dialog" aria-labelledby="optOutNotificationsModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header modal-header-alert-danger">
			Important
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<p class="margin20-l"><spring:message code="indportal.communicationPreferences.optOutNotifications" javaScriptEscape="true"/></p>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary pull-right" data-dismiss="modal"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></button>
			</div>
		</div><!-- /.modal-content-->
	</div> <!-- /.modal-dialog-->
</div>
<!-- Modal-->
</div>

<script type="text/javascript">
/*dynamic county population*/

function postPopulateIndex(zipCodeValue, zipId){
	/* console.log(zipCodeValue,'---page---',zipId); */
	jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
	getCountyList(zipCodeValue, '', zipId);	
}

function getCountyList(zip, county, zipId) {
	/* console.log(zip, county, 'get county list', zipId); */
	var csrftoken = '${sessionScope.csrftoken}';
	$.ajax({
		type : "POST",
		url : theUrlForAddCounties,
		data : {zipCode : zip, csrftoken : csrftoken},
		dataType:'json',
		success : function(response) {
			populateCounties(response, county, zipId);
		},error : function(e) {
			alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
		}
	});
}


function getCountyListForState(zipId,stateCodeId,county) {
	/* console.log(zip, county, 'get county list', zipId); */
	var zip = $('#'+zipId).val();
	var stateCode = $('#'+stateCodeId).val();
	if(zip == "" || zip.length< 5 || stateCode == ""){
		return;
	}
	
	if(stateCode == "" || zip.length< 5 ){
		return;
	}
	
	var csrftoken = '${sessionScope.csrftoken}';
	showProgress();
	$.ajax({
		type : "POST",
		url : theUrlForAddCountiesForStates,
		data : {zipCode : zip, csrftoken : csrftoken,stateCode:stateCode},
		dataType:'json',
		success : function(response) {
			populateCounties(response, county, zipId);
			countyLoaded();
			stopProgress();
		},error : function(e) {
			alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
		}
	});
}

function getCountyListForAddress(zip, county, zipId) {
	/* console.log(zip, county, 'get county list', zipId); */
	var csrftoken = '${sessionScope.csrftoken}';
	showProgress();
	$.ajax({
		type : "POST",
		url : theUrlForAddCounties,
		data : {zipCode : zip, csrftoken : csrftoken},
		dataType:'json',
		success : function(response) {
			populateCounties(response, county, zipId);
			countyLoaded();
			stopProgress();
		},error : function(e) {
			alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
		}
	});
}

function populateCounties(response, county, zipIdFocused) {	
	/* console.log(zipIdFocused,'populate counties'); */
    var optionsstring = '<option value="">Select County</option>';
		var i =0;
		$.each(response, function(key, value) {
			var selected = (county == key) ? 'selected' : '';
			var optionVal = key;/*+'#'+value;*/
			var options = '<option value="'+value+'" '+ selected +'>'+ key +'</option>';
			optionsstring = optionsstring + options;
			i++;
		});
		$('#'+zipIdFocused).parents('.addressBlock').find('select[data-rel="countyPopulate"]').html(optionsstring);		
		if(i==1){
			$('#'+zipIdFocused).parents('.addressBlock').find('select[data-rel="countyPopulate"]').prop('selectedIndex', 1);
		}
} // end populateCounties

function showOptOutNotificationsModal(){
	$('#optOutNotificationsModal').modal();
}
</script>