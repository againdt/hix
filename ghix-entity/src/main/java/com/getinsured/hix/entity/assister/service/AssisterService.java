package com.getinsured.hix.entity.assister.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.entity.AssisterExportDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.AssisterNumber;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.ExternalIndividual;

/**
 * Encapsulates service layer method calls for Assister
 */
public interface AssisterService {

	/**
	 * Finds the Assister by given id
	 * 
	 * @param assisterId
	 *            the given Assister id
	 * @return the existing Assister if found
	 */
	Assister findById(int assisterId);

	/**
	 * Updates the Assister table with the given Assister object
	 * 
	 * @param assister
	 *            object to update existing record
	 * @return the updated Assister object
	 */
	Assister update(Assister assister, boolean isAHBXInfo);

	/**
	 * Saves the given Assister object in Assister table
	 * 
	 * @param assister
	 *            object to save
	 * @return the saved Assister object
	 */
	Assister saveAssister(Assister assister);

	/**
	 * Finds the List of Assisters by given Enrollment Entity
	 * 
	 * @param enrollmentEntity
	 *            the given Enrollment Entity
	 * @return the list of Assisters for the given Enrollment Entity
	 */
	List<Assister> findByEnrollmentEntity(EnrollmentEntity enrollmentEntity);

	/**
	 * Finds the List of DesignateAssisters by given assister id
	 * 
	 * @param assisterId
	 *            the given Assister's Id
	 * @return the list of DesignateAssisters for the given assister
	 */
	List<DesignateAssister> findDesignateAssisterByAssisterId(int assisterId);

	/**
	 * Finds the Assister by given User Id
	 * 
	 * @param userId
	 *            the given user id
	 * @return the Assister for the given User
	 */
	Assister findByUserId(Integer userId);
	

	/**
	 * Finds the Assister by given Assister Id
	 * 
	 * @param assisterId
	 *            the given assister id
	 * @return Assister
	 */
	Assister findAssisterById(int assisterId);

	/**
	 * Finds the Assister by given Assister Id
	 * 
	 * @param id
	 *            the given assister id
	 * @return Assister
	 */
	Assister getAssisterByRecordID(int id);

	/**
	 * Updates the Assister Information
	 * 
	 * @param assister
	 *            the given assister
	 * @param isAHBXInfo
	 *            flag for sending Assister Information to AHBX
	 * @return Assister
	 */
	Assister updateAssisterInformation(Assister assister, boolean isAHBXInfo);

	/**
	 * Finds the Activity Status History for the given Assister
	 * 
	 * @param userId
	 *            the given Assister User id
	 * @return list of Assister's status history
	 */
	List<Map<String, Object>> loadAssisterActivityStatusHistoryForAdmin(Integer userId);

	/**
	 * Finds list of Assisters based on search criteria
	 * 
	 * @param enrollmentEntityRequestDTO
	 *            contains the search parameters
	 * @return list of Assisters
	 */
	List<Assister> searchAssisters(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO);

	/**
	 * Finds list of Assisters based on given enrollment entity ID
	 * 
	 * @param enrollmentEntityId
	 *            enrollment entity ID
	 * @return list of Assisters
	 */
	List<Assister> findAssistersByEEId(Integer enrollmentEntityId);

	/**
	 * Finds list of Assisters based on given enrollment entity ID
	 * 
	 * @param enrollmentEntityId
	 *            enrollment entity ID
	 * @param siteId
	 *            site ID
	 * @return list of Assisters
	 */
	List<Assister> findByEnrollmentEntityAndSite(Integer enrollmentEntityId, Integer siteId);

	ExternalIndividual findIndividualInfo(Integer individualId);

	AssisterResponseDTO searchIndividualForAssister(Map<String, Object> searchCriteria, String desigStatus,
			Integer startRecord, Integer pageSize);

	/**
	 * Finds DesignateAssister record based on individual Id
	 * 
	 * @param individualId
	 *            individual ID
	 * @return DesignateAssister record
	 */
	DesignateAssister getDesignateAssister(Integer individualId);
	

	/**
	 * Finds list of Assisters based on given enrollment entity ID and Site ID
	 * 
	 * @param assisterRequestDTO
	 *            contains enrollment entity ID ans site ID
	 * @return list of Assisters
	 */
	List<Assister> searchAssistersByEntityAndSite(AssisterRequestDTO assisterRequestDTO);

	/**
	 * Finds number of Assisters based on given enrollment entity ID and Site ID
	 * 
	 * @param assisterRequestDTO
	 *            contains enrollment entity ID ans site ID
	 * @return no of Assisters
	 */
	int totalAssistersByEntityAndSite(AssisterRequestDTO assisterRequestDTO);
	/**
	 * Finds Assister details
	 * 
	 * @param userId
	 * @return no of Assister
	 */
	Assister getAssisterByUserId(Integer userId);
	/**
	 * Finds Assister Number
	 * 
	 * @param assisterId
	 * @return no of AssisterNumber
	 */
	AssisterNumber findAssisterNumberByAssisterId(int id);
	
	/**
	 * Finds the List of Assisters by given Enrollment Entity
	 * 
	 * @param enrollmentEntity id of the given Enrollment Entity
	 * @return the count of Assisters for the given Enrollment Entity
	 */
	int findByAssisterEnrollmentEntity(AssisterRequestDTO assisterRequestDTO,int entityId);
	
	/**
	 * 
	 * @param emailAddress
	 * @return list of assisters.
	 */
	List<Assister> findByEmailAddress(String emailAddress);

	Assister getAssisterRecordByUserId(Integer userId);

	List<AssisterExportDTO> getAssisterExportList();

}
