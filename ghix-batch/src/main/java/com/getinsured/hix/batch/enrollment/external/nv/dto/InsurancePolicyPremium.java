package com.getinsured.hix.batch.enrollment.external.nv.dto;

import java.math.BigDecimal;

public class InsurancePolicyPremium {
	 private BigDecimal monthlyPolicyPremiumAmount;
	 private String exchangeRateAreaReference;
	 private BigDecimal ehbPremiumAmount;
	 private BigDecimal allocatedAPTCAmount;
	 private BigDecimal receivedFromOtherQHPAmount;
	 private BigDecimal appliedAPTCAmount;
	 private BigDecimal distributedToOtherQHPAmount;
	 private BigDecimal distributedToSADPAmount;
	 private BigDecimal systemElectedAPTCAmount;
	 private BigDecimal individualResponsibleAmount;


	 // Getter Methods 

	 public BigDecimal getMonthlyPolicyPremiumAmount() {
	  return monthlyPolicyPremiumAmount;
	 }

	 public String getExchangeRateAreaReference() {
	  return exchangeRateAreaReference;
	 }

	 public BigDecimal getEhbPremiumAmount() {
	  return ehbPremiumAmount;
	 }

	 public BigDecimal getAllocatedAPTCAmount() {
	  return allocatedAPTCAmount;
	 }

	 public BigDecimal getReceivedFromOtherQHPAmount() {
	  return receivedFromOtherQHPAmount;
	 }

	 public BigDecimal getAppliedAPTCAmount() {
	  return appliedAPTCAmount;
	 }

	 public BigDecimal getDistributedToOtherQHPAmount() {
	  return distributedToOtherQHPAmount;
	 }

	 public BigDecimal getDistributedToSADPAmount() {
	  return distributedToSADPAmount;
	 }

	 public BigDecimal getSystemElectedAPTCAmount() {
	  return systemElectedAPTCAmount;
	 }

	 public BigDecimal getIndividualResponsibleAmount() {
	  return individualResponsibleAmount;
	 }

	 // Setter Methods 

	 public void setMonthlyPolicyPremiumAmount(BigDecimal monthlyPolicyPremiumAmount) {
	  this.monthlyPolicyPremiumAmount = monthlyPolicyPremiumAmount;
	 }

	 public void setExchangeRateAreaReference(String exchangeRateAreaReference) {
	  this.exchangeRateAreaReference = exchangeRateAreaReference;
	 }

	 public void setEhbPremiumAmount(BigDecimal ehbPremiumAmount) {
	  this.ehbPremiumAmount = ehbPremiumAmount;
	 }

	 public void setAllocatedAPTCAmount(BigDecimal allocatedAPTCAmount) {
	  this.allocatedAPTCAmount = allocatedAPTCAmount;
	 }

	 public void setReceivedFromOtherQHPAmount(BigDecimal receivedFromOtherQHPAmount) {
	  this.receivedFromOtherQHPAmount = receivedFromOtherQHPAmount;
	 }

	 public void setAppliedAPTCAmount(BigDecimal appliedAPTCAmount) {
	  this.appliedAPTCAmount = appliedAPTCAmount;
	 }

	 public void setDistributedToOtherQHPAmount(BigDecimal distributedToOtherQHPAmount) {
	  this.distributedToOtherQHPAmount = distributedToOtherQHPAmount;
	 }

	 public void setDistributedToSADPAmount(BigDecimal distributedToSADPAmount) {
	  this.distributedToSADPAmount = distributedToSADPAmount;
	 }

	 public void setSystemElectedAPTCAmount(BigDecimal systemElectedAPTCAmount) {
	  this.systemElectedAPTCAmount = systemElectedAPTCAmount;
	 }

	 public void setIndividualResponsibleAmount(BigDecimal individualResponsibleAmount) {
	  this.individualResponsibleAmount = individualResponsibleAmount;
	 }
	}
