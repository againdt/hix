package com.getinsured.eligibility.at.ref.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ApplicationsWithSameIdDTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class ApplicationsWithSameIdDTO {

	@XmlElement(name = "applicationId", required = true)
	private long applicationId = 0l;

	@XmlElement(name = "status", required = true)
	private String status;

	@XmlElement(name = "cmrId", required = false)
	private int cmrId;

	@XmlElement(name = "enrolledActive", required = false)
	private boolean enrolledActive;

	@XmlElement(name = "coverageYear", required = false)
	private long coverageYear;

	@XmlElement(name = "financialAssistance", required = true)
	private String financialAssistance;

	public String getFinancialAssistance() {
		return financialAssistance;
	}

	public void setFinancialAssistance(String financialAssistance) {
		this.financialAssistance = financialAssistance;
	}

	public long getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}

	public long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(long applicationId) {
		this.applicationId = applicationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCmrId() {
		return cmrId;
	}

	public void setCmrId(int cmrId) {
		this.cmrId = cmrId;
	}

	public boolean isEnrolledActive() {
		return enrolledActive;
	}

	public void setEnrolledActive(boolean enrolledActive) {
		this.enrolledActive = enrolledActive;
	}

}
