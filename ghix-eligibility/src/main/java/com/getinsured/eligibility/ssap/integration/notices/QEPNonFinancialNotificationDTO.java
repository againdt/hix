package com.getinsured.eligibility.ssap.integration.notices;

import java.util.Date;


public class QEPNonFinancialNotificationDTO {

	private String userName;

	private String primaryApplicantName;

	private String caseNumber;

	private Date enrollmentEndDate;
	
	private String keepOnly;

	public String getKeepOnly() {
		return keepOnly;
	}

	public void setKeepOnly(String keepOnly) {
		this.keepOnly = keepOnly;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPrimaryApplicantName() {
		return primaryApplicantName;
	}

	public void setPrimaryApplicantName(String primaryApplicantName) {
		this.primaryApplicantName = primaryApplicantName;
	}

	public Date getEnrollmentEndDate() {
		return enrollmentEndDate;
	}

	public void setEnrollmentEndDate(Date enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}


}
