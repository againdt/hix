<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>


<div class="gutter10">
	<div class="row-fluid">
		<!--  beginning of side bar -->
		<h1>
			<a name="skip"></a>Temporary DashBoard
		</h1>
	</div>
	<!-- end of header -->
	<!-- end of row-fluid -->


	<!-- start of span3 -->
	<div class="row-fluid">
		<!-- add id for skip side bar -->
		<div class="span3" id="sidebar">
		
			<div class="gray">
				<h4 class="margin0">Common Actions</h4>
				<ol class="nav nav-list">
					<li class="active"><a href="<c:url value="/eligibility/startapplication" />">Start My Application</a></li>
					<li><a href="<c:url value="/plandisplay/individual" />">Find Plans</a></li>
				</ol>
			</div>
		</div>
		<!-- end of span3 -->


		<!-- start of span9 -->
		<div class="span9">
		
				<div class="gray">
					<h4 class="margin0">Home</h4>
				</div>
			
		</div> <!-- end of span9 -->
		
	</div> <!-- end of row-fluid -->
</div> <!-- end of gutter10-->

		