<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<%@ page isELIgnored="false"%>

<div class="gutter10">
	<div class="row-fluid">
		<div id="sidebar" class="span3">			
		</div>
		<!--sidebar-->

		<div id="rightpanel" class="span9">
			<div id="autoPaydiv">
				<h4 class="graydrkbg gutter10">Setup Automatic Payments</h4>
				<div class="gutter10">
					
					<form method="POST" action="autopay" name="fromautoPay" id="fromautoPay" class="form-vertical gutter10">
					<df:csrfToken/>
					<input type="hidden" value="<encryptor:enc value = "${employerid}"/>" name="employerid" id="employerid" />
						<div class="control-group">
							<label class="control-label" for="autopay"> <h5>Monthly automatic payments are :</h5> 
							</label>
							<div class="controls inline">
								<select class=" input-medium" id="autopay" name="autopay">
									<option <c:if test="${isAutoPay}"> SELECTED </c:if> value="Y">On</option>
									<option <c:if test="${!isAutoPay}"> SELECTED </c:if> value="N">Off</option>		
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="text-Normal">
							<p><spring:message code="employer.autopay.message"/></p>
							</label>
						</div>
						<div class="form-actions">
							<a href="paymentmethods" class="btn">Cancel</a>
							<!-- <input type="button" value="Cancel" class="btn btn-primary">-->
							<input type="button" title="Continue" class="btn btn-primary" value="Save and Exit" id="submitBtn" name="submitBtn" onClick="javascript:validateForm();">
						</div>
					</form>
				</div>
			</div>
			<!-- For selecting automatic payment option on/off -->
		</div>
	</div>
</div>
<script>
function validateForm() {

	if ($("#fromautoPay").validate().form()) {
		if ($("#autopay").val() == 'Y') {
			checkDefaultPaymentMethod();
		} 
		else {
			document.getElementById("submitBtn").disabled = true; 
			$("#fromautoPay").submit();
		}
	}
}
	function checkDefaultPaymentMethod() {
		
		    var validateUrl = '';
			var parameter = '';
			
			    validateUrl = '<c:url value="/shop/employer/checkDefaultPaymentMethod"> <c:param name="${df:csrfTokenParameter()}"> <df:csrfToken plainToken="true" /> 	</c:param> </c:url>';
				
				parameter = {
					employerid : $("#employerid").val()
				};
			

			$
					.ajax({
						url : validateUrl,
						type : "POST",
						data : parameter,
						success : function(response, xhr) {
							if(isInvalidCSRFToken(xhr))	    				
								return;
							if (response != null || response != 'null') {
								
									if (response == 'N') {
									  alert('Kindly set default payment method for the employer before setting Auto pay on');
									}else if(response.indexOf("Error") >=0){
										alert("Error :: Please provide the correct information related to user or Please contact customer support team.");
									}
									else {
										$("#fromautoPay").submit();
									}
							}
						},
					});
		
	}
	function isInvalidCSRFToken(xhr){
		var rv = false;
		if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
			alert($('Session is invalid').text());
			rv = true;
		}
		return rv;
	}
</script>