package com.getinsured.hix.broker.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.getinsured.hix.model.ExternalIndividual;

/**
 * Spring-JPA repository that encapsulates methods that interact with the ExternalIndividual table
 */
public interface IExternalIndividualRepository extends JpaRepository<ExternalIndividual, Long> {
	/**
	 * Fetches the external individual record against given individual case id
	 * 
	 * @param extindividualid the external individual case id
	 * @return the external individual record against the individual case id
	 */
	@Query("FROM ExternalIndividual as an where an.id = :extindividualid")
	ExternalIndividual findByIndividualCaseId(@Param("extindividualid") long extindividualid);

}

