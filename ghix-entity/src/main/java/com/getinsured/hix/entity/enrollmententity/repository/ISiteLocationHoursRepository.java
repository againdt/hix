package com.getinsured.hix.entity.enrollmententity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.SiteLocationHours;


public interface ISiteLocationHoursRepository extends JpaRepository<SiteLocationHours, Long> {

	@Query("FROM SiteLocationHours as an where an.id = :siteLocationHourId")
	SiteLocationHours findBySiteLocationHoursId(@Param("siteLocationHourId") long siteLocationHourId);
	List<SiteLocationHours> findSiteLocationHoursBySite(Site site);
	@Modifying
	@Transactional
	@Query("delete from SiteLocationHours sh where sh.site.id = :siteId")
	int deleteSiteLocationHours(@Param("siteId") int siteId);
}
