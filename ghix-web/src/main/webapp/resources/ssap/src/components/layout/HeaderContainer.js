import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// ActionCreators

import {
    changeLanguage,
    showModal
} from '../../actions/actionCreators';

import { ENVIRONMENT_TYPES } from '../../constants/flowTypes';
import HeaderMN from './headers/MN';
import HeaderCA from './headers/CA';
import HeaderID from './headers/ID';
import HeaderNV from './headers/NV';

import MODAL_TYPES from '../../constants/modalTypes';


function HeaderContainer ({ refs, headerLinks, language, messages, actions: { changeLanguage, showModal } }) {
    const { stateCode } = headerLinks;

    const handleOpeningModal = () => {
        showModal(MODAL_TYPES.LOCATE_ASSISTANCE_MODAL);
    };

    const onChangeLanguage = (updatedLanguage) => {
        sessionStorage.language = updatedLanguage === 'en' ? 'English' : 'Spanish';
        changeLanguage(updatedLanguage);
    };

    const UndefinedHeader = () => {
        return null;
    };

    let HeaderComponent = UndefinedHeader;
    
    switch (stateCode) {
    case ENVIRONMENT_TYPES.MINNESOTA:
        HeaderComponent =  HeaderMN;
        break;
    case ENVIRONMENT_TYPES.NEVADA:
        HeaderComponent = HeaderNV;
        break;
    case ENVIRONMENT_TYPES.CALIFORNIA:
        HeaderComponent = HeaderCA;
        break;
    case ENVIRONMENT_TYPES.IDAHO:
        HeaderComponent = HeaderID;
        break;
    default:
        HeaderComponent = UndefinedHeader;
        break;
    }

    return (
        <>
            <HeaderComponent {...{refs, headerLinks, language, messages, handleOpeningModal, onChangeLanguage}} />
        </>
    );
}

const mapStateToProps = state => ({
    headerLinks: state.headerLinks,
    language: state.language,
    messages: state.messages
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeLanguage,
        showModal
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);
