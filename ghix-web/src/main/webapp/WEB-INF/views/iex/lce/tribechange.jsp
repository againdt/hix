<script type="text/ng-template" id="tribechange.jsp">
    <div class="gutter20" ng-controller="tribeChangeController">
        <div class="">
            <div class="" >
                <h4><spring:message code="label.lce.famHouseholdSum"/></h4>
                <table role="presentation" id="" class="table table-striped"  ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold">
                    <tbody>
                        <tr>
                            <td>
                                <spring:message code="label.lce.firstname"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.lastname"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.relationship"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.seekingcoverage"/>
                            </td>
                            <td><spring:message code="label.lce.takeAction"/></td>
                        </tr>
                        <tr ng-repeat="(fIndex, householdMember)  in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
                            <td>{{householdMember.name.firstName}}</td>
                            <td>{{householdMember.name.lastName}}</td>
                            <td>
                                {{getRelationShipWithPrimaryHousehold(householdMember.personId)}}
                            </td>
                            <td>
                                <span ng-if="householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.yes"/></span>
                                <span ng-if="!householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.no"/></span>
                            </td>
                            <td>
                                <a class="btn btn-primary btn-small" ng-click='toggleModal(householdMember,getOriginalIndex( householdMember,taxHousehold.householdMember));'><spring:message code="label.lce.update"/></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- MODAL THAT POPS UP WHEN USER WANT TO EDIT  -->
                <modal-dialog-static-backdrop show='modalShown' ng-model="modalHouseholdMember" modal-event-type="modalEventType">
                    <div class="margin50-lr">
                        <h4>
                            <spring:message code="label.lce.pleaseupdate"/>
                            <strong>{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{ modalHouseholdMember.name.lastName}}</strong>
                            <spring:message code="label.lce.stribestatus"/>
                        </h4>
                        <form class="form-horizontal" name="tribedate">
                            <div class="control-group">
                                <label class="control-label" for="input1">
                                    <spring:message code="label.lce.eventdateTribe"/>
                                    <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                                </label>
                                <div class="controls">
                                    <div id="tribeChangeEventDate" name="input1" required  class="input-append" datetimez ng-model="eventInfoArrayTemp[originalIndex].tribeChangeDate"  check-after="0">
                                        <input data-format="MM/dd/yyyy" type="text" id="input1" ng-model="eventInfoArrayTemp[originalIndex].tribeChangeDate"  class="span8"  placeholder="MM/DD/YYYY">
                                        <span class="add-on">
                                        <i class="icon-calendar" ng-click='appalyZindex()'></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="controls">
                                    <span class="validation-error-text-container" ng-if="submitted && tribedate.input1.$error.required"><spring:message code="label.lce.enterValidDate"/></span>
                                    <span class="validation-error-text-container" ng-if="submitted && tribedate.input1.$error.checkBefore"><spring:message code="label.lce.eventDateLimit1"/></span>
                                    <span class="validation-error-text-container" ng-if="submitted && tribedate.input1.$error.checkAfter"><spring:message code="label.lce.futureNotAllowed"/></span>
                                </div>
                            </div>
                        </form>
                        <form class="form-horizontal" name="additionalQuestionsForm">
                            <!-- Tribe member? -->
                            <div class="tribeMember">
                                <div id="native">
                                    <span>
                                        <spring:message code="label.lce.is"/>
                                        <strong>{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{ modalHouseholdMember.name.lastName}}</strong> an American Indian or Alaska Native? <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                                    </span>
                                    <div class="margin10-l">
                                        <label class="radio-inline" for="indiannativeyes">
                                            <input type="radio" id="indiannativeyes" name="indiannative" required value="Yes" ng-value="true" ng-model="modalHouseholdMember.specialCircumstances.americanIndianAlaskaNativeIndicator">
                                            <spring:message code="label.lce.yes"/>
                                        </label>
                                        <label class="radio-inline" for="indiannativeno">
                                            <input type="radio" id="indiannativeno" name="indiannative" required value="No" ng-value="false" ng-model="modalHouseholdMember.specialCircumstances.americanIndianAlaskaNativeIndicator" ng-click="modalHouseholdMember.americanIndianAlaskaNative.tribeName='';modalHouseholdMember.americanIndianAlaskaNative.tribeFullName='';modalHouseholdMember.americanIndianAlaskaNative.state='';modalHouseholdMember.americanIndianAlaskaNative.stateFullName='';modalHouseholdMember.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator=false;">
                                            <spring:message code="label.lce.no"/>
                                        </label>
                                    </div>
                                    <!-- Error -->
                                    <div class="controls">
                                        <span class="validation-error-text-container" ng-if="submitted && additionalQuestionsForm.indiannative.$error.required"><spring:message code="label.lce.selectvalidoption"/></span>
                                    </div>
                                </div>
                                <div class="margin10-t" ng-if="modalHouseholdMember.specialCircumstances.americanIndianAlaskaNativeIndicator==true" >
                                    <!-- Tribe member? -->
                                    <div class="tribeMember margin30-t">
                                        <div id="tribe">
                                            <span>
                                                Did
                                                <strong>{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{ modalHouseholdMember.name.lastName}}</strong>
                                                 become <spring:message code="label.lce.amemberofarecognizedtribe"/>
                                                <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                                            </span>
                                            <div class="margin10-l">
                                                <label class="radio-inline" for="tribeYes">
                                                    <input type="radio" id="tribeYes" name="tribeRadio" value="Yes" ng-value="true" ng-required="modalHouseholdMember.specialCircumstances.americanIndianAlaskaNativeIndicator==true" ng-model="modalHouseholdMember.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator">
                                                    <spring:message code="label.lce.yes"/>
                                                </label>
                                                <label class="radio-inline" for="tribeNo">
                                                    <input type="radio" id="tribeNo" name="tribeRadio" value="No" ng-value="false" ng-required="modalHouseholdMember.specialCircumstances.americanIndianAlaskaNativeIndicator==true" ng-click="modalHouseholdMember.americanIndianAlaskaNative.tribeName='';modalHouseholdMember.americanIndianAlaskaNative.tribeFullName='';modalHouseholdMember.americanIndianAlaskaNative.state='';modalHouseholdMember.americanIndianAlaskaNative.stateFullName='';" ng-model="modalHouseholdMember.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator">
                                                    <spring:message code="label.lce.no"/>
                                                </label>
                                            </div>
                                            <!-- Error -->
                                            <div class="controls margin5-t">
                                                <span class="validation-error-text-container" ng-if="submitted && additionalQuestionsForm.tribeRadio.$error.required"><spring:message code="label.lce.selectvalidoption"/></span>
                                            </div>
                                        </div>
                                        <!-- What state and tribe? -->
                                        <div ng-if="modalHouseholdMember.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator==true && modalHouseholdMember.specialCircumstances.americanIndianAlaskaNativeIndicator==true">
                                            <div class="control-group">
                                                <label class="control-label" for="ddlState">
                                                    <spring:message code="label.lce.state"/>
                                                    <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                                                </label>
                                                <div class="controls">
                                                    <select id="ddlState" name="state" ng-model="modalHouseholdMember.americanIndianAlaskaNative.state" ng-change="modalHouseholdMember.americanIndianAlaskaNative.tribeName=undefined;loadTribes(modalHouseholdMember.americanIndianAlaskaNative.state);" select-label-model="modalHouseholdMember.americanIndianAlaskaNative.stateFullName" ng-required="modalHouseholdMember.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator==true && modalHouseholdMember.specialCircumstances.americanIndianAlaskaNativeIndicator==true"  ng-options="state.stateCode as state.stateName for state in stateArray" >
                                                        
                                                    </select>
                                                </div>
                                                <div class="controls validation-error-text-container" ng-if="submitted && additionalQuestionsForm.state.$error.required" ><spring:message code="label.lce.plsSelectState"/></div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="ddlTribes">
                                                    <spring:message code="label.lce.tribename"/>
                                                    <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                                                </label>
                                                <div class="controls" >
                                                    <select id="ddlTribes" name="tribes" ng-model="modalHouseholdMember.americanIndianAlaskaNative.tribeName" select-label-model="modalHouseholdMember.americanIndianAlaskaNative.tribeFullName" ng-required="modalHouseholdMember.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator==true && modalHouseholdMember.specialCircumstances.americanIndianAlaskaNativeIndicator==true">
                                                        <option value=""><spring:message code="label.lce.selectTribe"/></option>
                                                        <option ng-repeat="(key,value) in tribes" value="{{key}}" ng-selected="{{key == modalHouseholdMember.americanIndianAlaskaNative.tribeName}}" label="{{value}}">{{value}}</option>
                                                    </select>
                                                </div>
                                                <div class="controls validation-error-text-container" ng-if="submitted && additionalQuestionsForm.tribes.$error.required" ><spring:message code="label.lce.plsSelectTribe"/></div>
                                            </div>
                                        </div>
                                        <!-- What state and tribe? -->
                                    </div>
                                    <!-- tribeMember -->
                                </div>
                            </div>
                        </form>
                        <dl class="dl-horizontal pull-right">
                            <a  class="btn btn-secondary" ng-click="closeModal()"><spring:message code="lce.cancelbutton"/></a>
                            <a  class="btn btn-primary" ng-click="modelOK(modalHouseholdMember)" ng-if="additionalQuestionsForm.$valid && tribedate.$valid"><spring:message code="label.lce.ok"/></a>
                            <a ng-if="(!additionalQuestionsForm.$valid) || (!tribedate.$valid) " class="btn btn-primary" ng-click="$parent.submitted=true;"><spring:message code="label.lce.ok"/></a>
                        </dl>
                    </div>
                </modal-dialog-static-backdrop>
            </div>
        </div>
        <dl class="dl-horizontal pull-right">
            <a class="btn btn-secondary" ng-click="back()" ng-if="!(lifeEventCounter >= 0 || counter >= 1)">
                <spring:message code="label.lce.back"/>
            </a>
            <a class="btn btn-primary" href="" ng-click="next();">
                <spring:message code="label.lce.next"/>
            </a>
        </dl>
    </div>
</script>
