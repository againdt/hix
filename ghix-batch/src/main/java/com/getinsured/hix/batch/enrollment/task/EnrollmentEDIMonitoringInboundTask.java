package com.getinsured.hix.batch.enrollment.task;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.enrollment.service.EnrollmentBatchService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.util.exception.GIException;

public class EnrollmentEDIMonitoringInboundTask implements Tasklet{

	private EnrollmentBatchService enrollmentBatchService;
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEDIMonitoringInboundTask.class);
	private JobService jobService;
	long jobExecutionId = -1;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		jobExecutionId=chunkContext.getStepContext().getStepExecution().getJobExecutionId();
		String batchJobStatus=null;
		
		if(jobService != null && jobExecutionId != -1){
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}
		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}
		
		try {
			LOGGER.info("EnrollmentEDIMonitoringInboundTask.execute : Welcome ");
			enrollmentBatchService.monitorInboundEDIjob(getDateTime());
		}catch(Exception exception){
			LOGGER.info("EnrollmentEDIMonitoringInboundTask.execute : Failed "+exception.getCause(),exception);
			throw new GIException("EnrollmentEDIMonitoringInboundTask :: Failed to execute : "+exception.getMessage(),exception);
		}
		return RepeatStatus.FINISHED;
	}

	public EnrollmentBatchService getEnrollmentBatchService() {
		return enrollmentBatchService;
	}

	public void setEnrollmentBatchService(EnrollmentBatchService enrollmentBatchService) {
		this.enrollmentBatchService = enrollmentBatchService;
	}
	
	private String getDateTime() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");
		return df.format(new Date());
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

}
