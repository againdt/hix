(function(){
	
	angular.module('verificationResultDashboardApp')
	.directive("spinningLoader", spinningLoader)
	.directive("fileModelBinding", fileModelBinding)
	.directive('collapseIconPlus', collapseIconPlus);
//	.directive('maxFileSize', maxFileSize);
	
	spinningLoader.$inject = ['$timeout'];
	
	function spinningLoader($timeout){
		return {
			restrict: "EA", 
			template: '<div id="loaderDiv" ng-show="loadSpinner"><img alt="loading" src="/hix/resources/images/loader_gray.gif" class="ajax-loader"/></div>',
		    link: function (scope, elem, attrs, ctrl) {
	          var starting;
	          scope.$watch(attrs.spinningLoader, function(nV, oV){
	            if (nV === true){
	              starting = new Date();
	              scope.loadSpinner = true;                   
	            } else {
	              var ending = new Date();
	              var timeDiff = ending - starting;
	              if (timeDiff >= 1000){
	                scope.loadSpinner = false;
	              } else {
	                $timeout(function(){
	                  scope.loadSpinner = false;
	                }, (1000 - timeDiff));
	              };           
	            }; 
	         });
	      }
		};
	}

	fileModelBinding.$inject = ['$parse'];

	function fileModelBinding($parse){
		return {
	        restrict: 'A',
	        require : 'ngModel',
	        link: function(scope, element, attrs, ngModel) {
	            var model = $parse(attrs.fileModelBinding);
	            var modelSetter = model.assign;
	            
	            
	            function checkSize(fileSize){
	        		return attrs.maxFileSize >= fileSize
	        	}
	            
	            element.bind('change', function(){
	                scope.$apply(function(){
	                    modelSetter(scope, element[0].files[0]);
	                    
	                    if(attrs.maxFileSize!== undefined){
	                    	if(element[0].files[0] !== undefined){
		                    	var isSizeValid =checkSize(element[0].files[0].size);
		                    	
		                    	ngModel.$dirty = true;
		                    	ngModel.$pristine = false;
		                    	ngModel.$setValidity('maxFileSize', isSizeValid);
		                    	
		                    }else{
		                    	ngModel.$setValidity('maxFileSize', true);
		                    }
	                    }  
	                });
	            });
	        }
	    };
	}
	
	
	collapseIconPlus.$inject = ['$timeout'];
	function collapseIconPlus($timeout){
		var directive = {
			restrict: "A",
			link : function(scope, element, attrs, ngModel) {
	        	
	        	element.bind('click', function(){
	        		
	        		$('.accordion-group').each(function(){
	        			if($(this).find(".icon-minus-sign").length > 0){
	        				$(this).find("i").removeClass("icon-minus-sign").addClass("icon-plus-sign");
	        			}
	        		});
	        		
	        		$timeout(function(){
	        			if(element.find(".icon-plus-sign").length > 0 && element.closest('.accordion-group').find('.in').length > 0){
		            		element.find("i").removeClass("icon-plus-sign").addClass("icon-minus-sign");
		            	}
	        		}, 100)
	        		
	        	});
			}
		};
		
		return directive;
	}
	
	
	
	function maxFileSize(){
		return {
	        restrict: 'A',
	        require : 'ngModel',
	        link: function(scope, element, attrs) {
	        	var maxSize = attrs.maxFileSize;
	        	
	        	
	        	function checkSize(fileSize){
	        		return maxSize >= fileSize
	        	}
	        	
	        	ngModel.$parsers.unshift(function(value){
					var isSizeValid =checkSize(value.size);
					
					ngModel.$setValidity(fileSize, isSizeValid);
					
					return value;	
				});
				
				 ngModel.$formatters.unshift(function(value) {
					 if(value === undefined || value === null){
						 return;
					 }
					 var isSizeValid =checkSize(value.size);
						
					 ngModel.$setValidity(fileSize, isSizeValid);
					 
					 return value;
	             });
	        }
	    };
	}

})();
