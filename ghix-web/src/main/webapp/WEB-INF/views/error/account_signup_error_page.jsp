<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isErrorPage="true" %>
<!DOCTYPE html>
<html lang="en">
<head>

<style type="text/css">
	div.application-error{
		position:relative;
	}
	div.application-error div.application-error-btn{
		position:absolute;
		top:360px;
		height:95px;
		width:200px;	
		left: 70%;
	}
	div.application-error div.application-error-btn a{
		display:block;
		width:200px;
		height:95px;
	}
	table.error-environment{
		background-color:#f0f0f0;
		width:100%;
		margin-top:20px;
	}
	table.error-environment th{
		padding:15px 20px;
		background-color:#e2e2e2;
		text-align:left;
	}
	table.error-environment td{
		padding:5px 0 0 20px;
	}


</style>


</head>
<body>
<div>
	<br/>
	<div class="application-error" align="middle">
		<span>Account cannot be created, please contact YHI. </span><a href="/hix"> Go back to Home page</a>
		
	</div>	
	
	
</div>
</body>
</html>