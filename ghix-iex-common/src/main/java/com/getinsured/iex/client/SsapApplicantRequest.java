package com.getinsured.iex.client;

import java.math.BigDecimal;

public class SsapApplicantRequest {

	private String tobaccoUsage; 
	private String isEnrldCessPrgrm; 
	private BigDecimal maritialStatusId;
	private long id;
	private String applicantGuid;
	private String ecnNumber;
	
	private String hardshipExempt;
	
	private String applyingForCoverage;
	
	private String caseNumber;
	
	public String getApplicantGuid() {
		return applicantGuid;
	}
	public void setApplicantGuid(String applicantGuid) {
		this.applicantGuid = applicantGuid;
	}
	public String getTobaccoUsage() {
		return tobaccoUsage;
	}
	public void setTobaccoUsage(String tobaccoUsage) {
		this.tobaccoUsage = tobaccoUsage;
	}
	
	public String getIsEnrldCessPrgrm() {
		return isEnrldCessPrgrm;
	}
	public void setIsEnrldCessPrgrm(String isEnrldCessPrgrm) {
		this.isEnrldCessPrgrm = isEnrldCessPrgrm;
	}
	public BigDecimal getMaritialStatusId() {
		return maritialStatusId;
	}
	public void setMaritialStatusId(BigDecimal maritialStatusId) {
		this.maritialStatusId = maritialStatusId;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEcnNumber() {
		return ecnNumber;
	}
	public void setEcnNumber(String ecnNumber) {
		this.ecnNumber = ecnNumber;
	}
	public String getHardshipExempt() {
		return hardshipExempt;
	}
	public void setHardshipExempt(String hardshipExempt) {
		this.hardshipExempt = hardshipExempt;
	}
	public String getApplyingForCoverage() {
		return applyingForCoverage;
	}
	public void setApplyingForCoverage(String applyingForCoverage) {
		this.applyingForCoverage = applyingForCoverage;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
}
