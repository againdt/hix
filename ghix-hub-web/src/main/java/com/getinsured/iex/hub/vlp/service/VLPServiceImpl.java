package com.getinsured.iex.hub.vlp.service;


/**
 * Implements the services for Verify Lawful Presence service 
 * 
 * @author Nikhil Talreja
 * @since 29-Jan-2014
 *
 */
public class VLPServiceImpl {
	
	
	/**
	 * <p>
	 * Service method to handle the request to HUB for verification of Lawful Presence
	 * 
	 * <ul>
	 * <li>Step 1 : Validate incoming request</li>
	 * 
	 * <li>Step 2 : Prepare Request for Lawful Presence HUB service. This includes DHS ID</li>
	 * and other details such has name, I94 number, date of birth etc.
	 * 
	 * <li>Step 3 : Call HUB service with above request</li>

	 * <li>Step 4 : Based on response from HUB, prepare response of the service</li>
	 * 
	 * 		<ul>
	 * 		<li>Step 4.1 : If the Eligibility Statement Code does not equal 5, 32, or 37, set the
	 * 		response parameters</li>
	 * 
	 * 		<li>Step 4.2 : Determine if 5-year bar is applicable and if yes, if it has been met</li>
	 * 
	 * 		<li>Step 4.3 : If the Eligibility Statement Code is 37, which indicates incomplete or invalid data
	 * 		call ReVerifyAgency3InitVerif to invoke the re-verify service</li>
	 * 
	 * 		<li>Step 4.4 : If the Eligibility Statement Code is 32, which indicates the need for a SEVIS ID,
	 * 		calls= SubmitAgency3DhsResub to resubmit the case with the SEVIS ID of the student or exchange visitor</li>
	 * 
	 * 		<p>The SubmitAgency3DhsResub method can also return an Eligibility Statement Code of 119, 
	 * 		which indicates Refer Student/Exchange Visitor to the School/Program Sponsor. In such cases,
	 * 		notify the school/program sponsor to make the change, which updates the SAVE System. 
	 * 		After an individual confirms that the school made the change, submit a new request for Step 2</p>
	 * 
	 * 		</li>
	 * 		</ul>
	 * </ul>
	 * 
	 * <b>Eligibility Statement Code 5 </b>
	 * 
	 * <p>
	 * When the Eligibility Statement Code equals 5, (which indicates Institute Additional Verification), 
	 * the Hub auto-invokes the Agency3SubmitAdditVerif DHS Service that submits additional (secondary) 
	 * data to SAVE Immigration Status Verifiers (ISVs) and requests that DHS verify the status of this case
	 * </p>
	 * 
	 * <p>
	 * The Requester cannot change or add additional information to the data originally delivered to DHS in the request. 
	 * To verify new information, the Requester must close and re-initiate the case.
	 * 
	 * To get the status of this case, we need to invoke the VLP Retrieve Case Resolution Service
	 * </p>
	 * 
	 * <p>
	 * If the G-845 Major Code equals 14 in the above step response, which indicates Resubmit Doc (need copy or original), 
	 * the Hub auto-invokes SubmitThirdVerif and lets the Requester know that SubmitThirdVerif  is in progress
	 * 
	 * To retrieve the pre-filled G-845 form, we need to invoke the VLP Redisplay G845 Form callback service
	 * After receiving the form, we need to acknowledge the same and then proceed to invoke the VLP Retrieve Case Resolution Service
	 * 
	 * </p>
	 * </p>
	 * 
	 * @param VerifyLawfulPresenceInput request - Contains list of LawfulPresenceRequestType objects
	 * for which lawful presence needs to be determined 
	 * 
	 * @return VerifyLawfulPresenceOutput object containing LawfulPresenceResponseType object
	 * 
	 * @author - Nikhil Talreja
	 * @since - 29-Jan-2014
	 * 
	 */
	
	
}
