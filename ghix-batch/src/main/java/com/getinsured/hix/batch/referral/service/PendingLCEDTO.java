package com.getinsured.hix.batch.referral.service;

import java.sql.Timestamp;

import com.getinsured.eligibility.enums.ApplicationValidationStatus;

public class PendingLCEDTO {
	private long ssapApplicationId;

	private String caseNumber;
	
	private String keepOnly;

	private Timestamp enrollmentStartDate;

	private Timestamp enrollmentEndDate;

	private String notificationNo;

	private String notificationName;

	private int intervalFactor;
	
	private ApplicationValidationStatus applicationValidationStatus;

	public String getNotificationName() {
		return notificationName;
	}

	public int getIntervalFactor() {
		return intervalFactor;
	}

	public void setIntervalFactor(int intervalFactor) {
		this.intervalFactor = intervalFactor;
	}

	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}

	private int cmrHouseHoldId;

	private Timestamp ssapApplicationCreationTimeStamp;

	public int getCmrHouseHoldId() {
		return cmrHouseHoldId;
	}

	public void setCmrHouseHoldId(int cmrHouseHoldId) {
		this.cmrHouseHoldId = cmrHouseHoldId;
	}

	public Timestamp getSsapApplicationCreationTimeStamp() {
		return ssapApplicationCreationTimeStamp;
	}

	public void setSsapApplicationCreationTimeStamp(Timestamp ssapApplicationCreationTimeStamp) {
		this.ssapApplicationCreationTimeStamp = ssapApplicationCreationTimeStamp;
	}

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public Timestamp getEnrollmentStartDate() {
		return enrollmentStartDate;
	}

	public void setEnrollmentStartDate(Timestamp enrollmentStartDate) {
		this.enrollmentStartDate = enrollmentStartDate;
	}

	public Timestamp getEnrollmentEndDate() {
		return enrollmentEndDate;
	}

	public void setEnrollmentEndDate(Timestamp enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}

	public String getNotificationNo() {
		return notificationNo;
	}

	public void setNotificationNo(String notificationNo) {
		this.notificationNo = notificationNo;
	}

	public String getKeepOnly() {
		return keepOnly;
	}

	public void setKeepOnly(String keepOnly) {
		this.keepOnly = keepOnly;
	}
	
	public ApplicationValidationStatus getApplicationValidationStatus() {
		return applicationValidationStatus;
	}

	public void setApplicationValidationStatus(ApplicationValidationStatus applicationValidationStatus) {
		this.applicationValidationStatus = applicationValidationStatus;
	}

	@Override
	public String toString() {
		return "PendingLCEDTO [ssapApplicationId=" + ssapApplicationId + ", caseNumber=" + caseNumber + ", enrollmentStartDate=" + enrollmentStartDate + ", enrollmentEndDate=" + enrollmentEndDate + ", notificationNo=" + notificationNo
		        + ", notificationName=" + notificationName + ", intervalFactor=" + intervalFactor + ", cmrHouseHoldId=" + cmrHouseHoldId + ", ssapApplicationCreationTimeStamp=" + ssapApplicationCreationTimeStamp + ", applicationValidationStatus=" + applicationValidationStatus.toString() + "]";
	}

}
