const MONTH_VALUES = [
    {
        name: 'Jan',
        value: '1'
    },
    {
        name: 'Feb',
        value: '2'
    },
    {
        name: 'Mar',
        value: '3'
    },
    {
        name: 'Apr',
        value: '4'
    },
    {
        name: 'May',
        value: '5'
    },
    {
        name: 'Jun',
        value: '6'
    },
    {
        name: 'Jul',
        value: '7'
    },
    {
        name: 'Aug',
        value: '8'
    },
    {
        name: 'Sep',
        value: '9'
    },
    {
        name: 'Oct',
        value: '10'
    },
    {
        name: 'Nov',
        value: '11'
    },
    {
        name: 'Dec',
        value: '12'
    },
    {
        name: '',
        value: ''
    },
    {
        name: '',
        value: '0'
    }
];

export function monthNumberToString(monthNumber) {
    if (monthNumber > 12 || monthNumber === '---') {
        return '';
    }
    return MONTH_VALUES.find(month => month.value === monthNumber).name;
}

export function monthStringToNumber(monthString) {
    if (monthString === '---') {
        return '---';
    }
    return MONTH_VALUES.find(month => month.name === monthString).value;
}