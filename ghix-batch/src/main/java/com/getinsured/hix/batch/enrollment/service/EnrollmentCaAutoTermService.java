package com.getinsured.hix.batch.enrollment.service;

public interface EnrollmentCaAutoTermService {
	
	/**
	 * Invoked by Auto Terminate batch job. Dis Enrolls all active enrollments for given year.
	 * 
	 * @param dateYear
	 */
	public void processAutoTermEnrollment(Integer dateYear, long jobExecutionId, String hiosIssuerId, Boolean autoTermRenewals, Boolean generate834) throws Exception;

}
