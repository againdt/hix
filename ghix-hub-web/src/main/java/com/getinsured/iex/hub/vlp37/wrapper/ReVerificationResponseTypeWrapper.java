package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav.ReverificationResponseType;

public class ReVerificationResponseTypeWrapper implements JSONAware{

	private ReverificationResponseType reVerificationResponseType;
	
	private ReVerifyResponseMetadataWrapper responseMetadata;
	private String lawfulPresenceVerifiedCode;
	private ReVerificationResponseSetTypeWrapper reVerificationResponseTypeResponseSet;
	
	public ReVerificationResponseTypeWrapper(ReverificationResponseType reVerificationResponseType){
		
		if(reVerificationResponseType == null){
			return;
		}
		
		ReVerifyResponseMetadataWrapper responseMetadataWrapper = new ReVerifyResponseMetadataWrapper(reVerificationResponseType.getResponseMetadata());
		this.responseMetadata = responseMetadataWrapper;
		
//		this.lawfulPresenceVerifiedCode = intialVerificationResponseType.getLawfulPresenceVerifiedCode();
		
		ReVerificationResponseSetTypeWrapper ReVerificationResponseSetTypeWrapper = 
				new ReVerificationResponseSetTypeWrapper(reVerificationResponseType.getReverificationResponseSet());
		
		this.reVerificationResponseTypeResponseSet = ReVerificationResponseSetTypeWrapper;
	}

	public ReverificationResponseType getReVerificationResponseType() {
		return reVerificationResponseType;
	}

	public void setReVerificationResponseType(
			ReverificationResponseType reVerificationResponseType) {
		this.reVerificationResponseType = reVerificationResponseType;
	}

	public ReVerifyResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(ReVerifyResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}

	public String getLawfulPresenceVerifiedCode() {
		return lawfulPresenceVerifiedCode;
	}

	public void setLawfulPresenceVerifiedCode(String lawfulPresenceVerifiedCode) {
		this.lawfulPresenceVerifiedCode = lawfulPresenceVerifiedCode;
	}

	public ReVerificationResponseSetTypeWrapper getReVerificationResponseSet() {
		return reVerificationResponseTypeResponseSet;
	}

	public void setReVerificationResponseSet(
			ReVerificationResponseSetTypeWrapper reVerificationResponseTypeResponseSet) {
		this.reVerificationResponseTypeResponseSet = reVerificationResponseTypeResponseSet;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		if(this.responseMetadata != null){		
			obj.put("ResponseMetadata",this.responseMetadata);
		}
		
		if(this.lawfulPresenceVerifiedCode != null){
			obj.put("LawfulPresenceVerifiedCode",this.lawfulPresenceVerifiedCode);
		}
		
		if(this.reVerificationResponseTypeResponseSet != null){
			obj.put("IntialVerificationResponseTypeResponseSet",this.reVerificationResponseTypeResponseSet);
		}
		
		return obj.toJSONString();
	}

}
