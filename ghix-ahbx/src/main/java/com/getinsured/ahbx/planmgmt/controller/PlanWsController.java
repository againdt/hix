package com.getinsured.ahbx.planmgmt.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.ws.http.HTTPException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import com.getinsured.ahbx.planmgmt.PlanInfoDTO;
import com.getinsured.ahbx.planmgmt.UpdatePlanInfo;
import com.getinsured.ahbx.planmgmt.UpdatePlanInfoResponse;
import com.getinsured.ahbx.util.GIException;
import com.getinsured.ahbx.util.GhixAhbxConstants;
import com.getinsured.hix.platform.webclient.SoapWebClient;

@Controller
@RequestMapping(value = "/plan")
public class PlanWsController {

	//@Autowired WebServiceTemplate webServiceTemplate;
	@Autowired WebServiceTemplate planSyncServiceTemplate;
	@Autowired private SoapWebClient<JAXBElement<UpdatePlanInfoResponse>> soapWebClient;
	private static final Logger LOGGER = Logger.getLogger(PlanWsController.class);
	@Value("#{configProp.AHBXPlanSyncWSDL}")
	private String AHBXPlanSyncWSDL;	
	
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody public String welcome()throws HTTPException, Exception
	{
		LOGGER.info("Welcome to GHIX-AHBX SOAP Planmgmt Service module");		
		return "Welcome to GHIX-AHBX SOAP Planmgmt Service module";
	}
	
//	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/syncWithAHBX", method = RequestMethod.POST)
	@ResponseBody public String syncWithAHBX(@RequestBody List<Map<String, String>> listOfPlans) throws  GIException
	{		
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("==================== inside syncWithAHBX response =======================" );
			LOGGER.debug("PLan Sync WSDL " + AHBXPlanSyncWSDL);
			
		}
		
		if (listOfPlans == null || listOfPlans.size() == 0)		{
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Invalid input: request data is null or empty");
			}
			return GhixAhbxConstants.RESPONSE_FAILURE;
		}else{
			
			PlanInfoDTO planInfoDTO = null; 
			List<PlanInfoDTO> planInfoDTOList = new ArrayList<PlanInfoDTO>();
			
			for(Map<String, String> plan : listOfPlans){
				planInfoDTO = new PlanInfoDTO();
				planInfoDTO.setIssuerHiosId(plan.get("issuerId"));
				planInfoDTO.setPlanEffectiveEndDate(plan.get("planEffectiveEndDate"));
				planInfoDTO.setPlanEffectiveStartDate(plan.get("planEffectiveStartDate"));
				planInfoDTO.setPlanYear(Integer.parseInt(plan.get("planYear")));
				//planInfoDTO.setPlanId(Integer.parseInt(plan.get("planId")));
				planInfoDTO.setPlanLevel(processMedicalString(plan.get("planLevel")));
				planInfoDTO.setHiosPlanId(plan.get("hiosPlanId"));
				planInfoDTO.setPlanMarket(processMedicalString(plan.get("planMarket")));
				planInfoDTO.setPlanName(plan.get("planName"));
				planInfoDTO.setPlanNetworkType(plan.get("planNetworkType"));
				planInfoDTO.setPlanRecordIndicator(plan.get("planRecordIndicator"));
				planInfoDTO.setPlanInfoStatus(plan.get("planInfoStatus"));
				planInfoDTO.setPlanType(plan.get("planType"));
				
				planInfoDTOList.add(planInfoDTO);
			}	
			UpdatePlanInfo planInfo = null;
			com.getinsured.ahbx.planmgmt.ObjectFactory objectFactory = new com.getinsured.ahbx.planmgmt.ObjectFactory();
			
			planInfo = objectFactory.createUpdatePlanInfo();
			planInfo.setArg0(planInfoDTOList);
			
			
			JAXBElement<UpdatePlanInfo> requestElement = objectFactory.createUpdatePlanInfo(planInfo);
						
			UpdatePlanInfoResponse updatePlanInfoResponse = new UpdatePlanInfoResponse();
			planSyncServiceTemplate.setDefaultUri(AHBXPlanSyncWSDL);
			
			try{
				JAXBElement<UpdatePlanInfoResponse> ahbxResponse = soapWebClient.send(requestElement, AHBXPlanSyncWSDL, "IND04", planSyncServiceTemplate);
				updatePlanInfoResponse = ahbxResponse.getValue();
				
				if(updatePlanInfoResponse.getReturn() != null && !updatePlanInfoResponse.getReturn().isEmpty() && updatePlanInfoResponse.getReturn().size() > 0){
					return GhixAhbxConstants.RESPONSE_SUCCESS;
				}else{
					return GhixAhbxConstants.RESPONSE_FAILURE;
				}
			}catch (WebServiceIOException webServiceIOException) {
				LOGGER.error("Exception occurred while sync plan data with AHBX: "
						+ webServiceIOException.getMessage());	
				return GhixAhbxConstants.RESPONSE_FAILURE;
			} catch (Exception exception) {
				LOGGER.error("Exception occurred while sync plan data with AHBX: "
						+ exception.getMessage());		
				return GhixAhbxConstants.RESPONSE_FAILURE;
			}
		}
	}
	
	private String processMedicalString(String inputString){
		if(inputString.equalsIgnoreCase("MEDICAL")){
			inputString = "Medi-Cal";
		}
		return inputString;
	}
}
