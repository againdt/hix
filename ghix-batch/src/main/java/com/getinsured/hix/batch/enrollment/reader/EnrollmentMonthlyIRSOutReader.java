package com.getinsured.hix.batch.enrollment.reader;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.enrollment.skip.EnrollmentIRSOut;

public class EnrollmentMonthlyIRSOutReader implements ItemReader<String> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyIRSOutReader.class);
	List<String> householdIds;		
	int partition;
	int startIndex;
	int endIndex;
	int loopCount;
	private EnrollmentIRSOut enrollmentIRSOut;
	
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader ");
		
		ExecutionContext ec = stepExecution.getExecutionContext();
		if(ec != null){
			partition =ec.getInt("partition");
			startIndex=ec.getInt("startIndex");
			endIndex=ec.getInt("endIndex");
			if(enrollmentIRSOut!=null && enrollmentIRSOut.getHouseholdCaseIds()!=null && !enrollmentIRSOut.getHouseholdCaseIds().isEmpty()) {
				if(startIndex<enrollmentIRSOut.getHouseholdCaseIds().size() && endIndex<=enrollmentIRSOut.getHouseholdCaseIds().size()){
					householdIds= new ArrayList<String>(enrollmentIRSOut.getHouseholdCaseIds().subList(startIndex, endIndex));
				}
				LOGGER.info("======= Enrollment_IRS_OUT :: Thread Name: "+Thread.currentThread().getName() +" StartIndex: "+startIndex+" EndIndex: "+ endIndex  +" No Of householdIds: " +householdIds);
			}
		}
	}
	
	@Override
	public String read() throws Exception, UnexpectedInputException,
			ParseException, NonTransientResourceException {
		
		if(householdIds!=null && !householdIds.isEmpty()){
			if(loopCount<householdIds.size()){
				loopCount++;
				return householdIds.get(loopCount-1);
				
			}else{
				return null;
			}
			
		}
			return null;
	}

	
	public EnrollmentIRSOut getenrollmentIRSOut() {
		return enrollmentIRSOut;
	}

	public void setenrollmentIRSOut(EnrollmentIRSOut enrollmentIRSOut) {
		this.enrollmentIRSOut = enrollmentIRSOut;
	}

	public List<String> getHouseholdIds() {
		return householdIds;
	}

	public void setHouseholdIds(List<String> householdIds) {
		this.householdIds = householdIds;
	}
	
	
	
}
