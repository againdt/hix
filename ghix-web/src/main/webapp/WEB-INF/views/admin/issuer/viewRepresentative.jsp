<!-- 
 @author Kishor 
 @UserStory : HIX-7260
 @Jira-id : HIX-7525
 -->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>
<%@ page import="com.getinsured.hix.platform.util.PhoneUtil"%>


<script  type="text/javascript">
/* window.onload = attachTitle();
function  attachTitle() {
document.title ='GetInsured Health Exchange: Representative Details';
} */
</script>
<div class="gutter10">
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="pgheader.account"/></a></li>
            <li><a href="<c:url value="/admin/issuer/representative/manage/${issuerId}"/>"><spring:message  code="label.manageRepresentatives"/></a></li>
            
            <li>${issuerRepObj.userRecord.firstName} ${issuerRepObj.userRecord.lastName}</li>
        </ul><!--page-breadcrumb ends-->
    	<h1>${issuerRepObj.userRecord.firstName}<c:if test="${issuerRepObj.userRecord.lastName != ''}"> ${issuerRepObj.userRecord.lastName}</c:if>  <small><spring:message code="pgheader.authRepAddedOn"/> ${repUserCreatedOn}</small></h1>
    </div>

<div class="row-fluid">
	<div class="span3" id="sidebar">
               <div class="header">
                   <h4><spring:message  code="label.details"/></h4>
               </div>
               <!--  beginning of side bar -->
               <ul class="nav nav-list">
                   <li class="active"><spring:message  code="label.repInfoReview"/></li>
               </ul>
               <!-- end of side bar -->
	</div>
		<!-- end of span3 -->
           <div class="span9" id="rightpanel">
                <div class="header">
                   <h4 class="pull-left"><spring:message  code="label.repInfo"/></h4>
                   <a  href="<c:url value="/admin/issuer/representative/manage/${issuerId}"/>" class="btn btn-small pull-right"><spring:message  code="label.btnCancel"/></a>
               </div>
               <div class="gutter10">
					<table class="table table-condensed table-border-none ">
						<tr>
							<td class="txt-right span3"><spring:message  code="label.firstName"/></td>
							<td><strong>${issuerRepObj.userRecord.firstName}</strong></td>
						</tr>
						<tr>
							<td class="txt-right span3"><spring:message  code="label.lastName"/></td>
							<td><strong>${issuerRepObj.userRecord.lastName}</strong></td>
						</tr>
						<tr>
							<td class="txt-right span3"><spring:message  code="label.title"/></td>
							<td><strong>${issuerRepObj.userRecord.title}</strong></td>
						</tr>
						<tr>
							<td class="txt-right span3"><spring:message  code="label.phoneNumber"/></td>
							<c:set var="issRepPhone" value="${issuerRepObj.userRecord.phone}"/>
							<td><strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("issRepPhone"))%></strong></td>
						</tr>
						<tr>
							<td class="txt-right span3"><spring:message  code="label.emailAddress"/></td>
							<td><strong>${issuerRepObj.userRecord.email}</strong></td>
						</tr>
						
						<tr>
							<td class="txt-right span3"><spring:message  code="label.primaryContact"/></td>
							<td><strong>${issuerRepObj.primaryContact}</strong></td>
						</tr>
						
                        <tr>
                        	<td class="txt-right span3">&nbsp;</td>
                        </tr>
					</table>
               </div>
       	</div><!--  end of span9 -->
	</div>
</div>
