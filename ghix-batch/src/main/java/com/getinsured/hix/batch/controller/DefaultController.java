package com.getinsured.hix.batch.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class DefaultController {
	private static final Logger LOGGER = Logger.getLogger(DefaultController.class);
	private static final String LOGIN_FILE_PATH = "/account/user/login";
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
    public String getHome(HttpServletRequest request) {
		LOGGER.info("Batch web module home");
		return "home";
	}

	@RequestMapping(value = "/account/user/login", method = RequestMethod.GET)
	public String getLogin(HttpServletRequest request) {
		LOGGER.info("Batch login module home");
		return "redirect:/jobs/jobs";
	}
	
	@RequestMapping(value = "/account/user/loginSuccess", method = RequestMethod.GET)
	public String getLoginSuccess(Model model, HttpServletRequest request) {
		LOGGER.info("Batch login success"); 
		if(request.isUserInRole("ADMIN"))
		{
			return "redirect:/jobs/jobs";
		}
		else
		{
			model.addAttribute("authRoleFailed", "true");
			return LOGIN_FILE_PATH;
		}
	}

	@RequestMapping(value = "/account/user/loginfailed", method = RequestMethod.GET)
	public String getLoginFailure(Model model) {
		LOGGER.info("Batch login failed");
		model.addAttribute("authfailed", "true");
		return LOGIN_FILE_PATH;
    }

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(Model model) {

		return LOGIN_FILE_PATH;

	}

}
