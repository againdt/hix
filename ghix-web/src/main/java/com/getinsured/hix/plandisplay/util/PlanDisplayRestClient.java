package com.getinsured.hix.plandisplay.util;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.plandisplay.DrugDataRequestDTO;
import com.getinsured.hix.dto.plandisplay.DrugDataResponseDTO;
import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdOrderItemDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdSession;
import com.getinsured.hix.dto.planmgmt.IssuerRequest;
import com.getinsured.hix.dto.planmgmt.IssuerResponse;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.shop.EmployerDTO;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanDental;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.PldHousehold;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.estimator.mini.EligLeadRequest;
import com.getinsured.hix.model.plandisplay.GroupData;
import com.getinsured.hix.model.plandisplay.IndividualRequest;
import com.getinsured.hix.model.plandisplay.PdCrossWalkIssuerIdRequest;
import com.getinsured.hix.model.plandisplay.PdPreShopRequest;
import com.getinsured.hix.model.plandisplay.PdQuoteRequest;
import com.getinsured.hix.model.plandisplay.PdResponse;
import com.getinsured.hix.model.plandisplay.PdSaveCartRequest;
import com.getinsured.hix.model.plandisplay.PdSavePrefRequest;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentFlowType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ExchangeType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayRequest;
import com.getinsured.hix.model.plandisplay.PlanDisplayResponse;
import com.getinsured.hix.model.plandisplay.PldOrderResponse;
import com.getinsured.hix.model.plandisplay.PremiumTaxCreditResponse;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ConsumerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PlandisplayEndpoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ShopEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.thoughtworks.xstream.XStream;

/**
 * @author Mishra_m
 * 
 */

@Component
public class PlanDisplayRestClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanDisplayRestClient.class);

	@Autowired private PlanDisplayUtil planDisplayUtil;
	@Autowired private RestTemplate restTemplate;
	@Autowired private GhixRestTemplate ghixRestTemplate;
    @Autowired private UserService userService;
    @Autowired private IssuerService issuerService;
    @Autowired private GIMonitorService giMonitorService;
    @Autowired private Gson platformGson;
	

	private static final String STATUS_FAILED = "FAILED";
	
	// New REST client
	
	public PdResponse savePreShoppingData(PdPreShopRequest pdPreShopRequest) {
		PdResponse pdResponse = null;
		try {
			//String postParams = generateJSON(pdPreShopRequest);
			//String response = getPOSTData(PlandisplayEndpoints.SAVE_PRE_SHOPPING_DATA, postParams);
			String response = ghixRestTemplate.postForObject(PlandisplayEndpoints.SAVE_PRE_SHOPPING_DATA, pdPreShopRequest, String.class);
			if(response != null){
				ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PdResponse.class);
				pdResponse = objectReader.readValue(response);
			}
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME + PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_INDIVIDUAL_PHIX, null, PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_INDIVIDUAL_PHIX, Severity.HIGH);
		}
		return pdResponse;
	}
	
	public PdResponse findHouseholdByShoppingId(String shoppingId) throws GIRuntimeException {
		try {
			String response =  ghixRestTemplate.getForObject(PlandisplayEndpoints.FIND_HOUSEHOLD_BY_SHOPPING_ID+"?shoppingId="+shoppingId, String.class);
			if(response != null){
				ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PdResponse.class);
				PdResponse pdResponse = objectReader.readValue(response);
				return pdResponse;
			}
			return null;
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_HOUSEHOLD_BY_SHOPPING_ID, null, PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_HOUSEHOLD_BY_SHOPPING_ID, Severity.HIGH);
		} 
	}
	
	public List<PdPersonDTO> findPersonByHouseholdId(Long householdId) throws GIRuntimeException {
		try {
			String response =  ghixRestTemplate.getForObject(PlandisplayEndpoints.FIND_PESON_BY_HOUSEHOLD_ID+"?householdId="+householdId, String.class);

            Type type = new TypeToken<List<PdPersonDTO>>() {}.getType();
            List<PdPersonDTO> personDTOList = platformGson.fromJson(response, type);
            List<PdPersonDTO> pdPersonDTOList = new ArrayList<PdPersonDTO>();
        	for (PdPersonDTO pdPersonDTO : personDTOList) {                     
                pdPersonDTO.setPdHousehold(null);
                pdPersonDTOList.add(pdPersonDTO);
        	}

			return pdPersonDTOList;
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PESON_BY_HOUSEHOLD_ID, null, PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PESON_BY_HOUSEHOLD_ID, Severity.HIGH);
		} 
	}
	
	public void savePreferences(PdSavePrefRequest pdSavePrefRequest) throws GIRuntimeException{
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PdSavePrefRequest.class);
			postParams = writer.writeValueAsString(pdSavePrefRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(pdSavePrefRequest);
		
		String jsonString = getPOSTData(PlandisplayEndpoints.SAVE_PREFERENCES_BY_HOUSEHOLD_ID, postParams);
		PdResponse pdResponse = getJsonAsPDResponse(jsonString);
		if(null == pdResponse){
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_PREFERENCES, null, PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_PREFERENCES, Severity.HIGH);
		}
		if(STATUS_FAILED.equalsIgnoreCase(pdResponse.getStatus())) // If status is failed . 
		{
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+pdResponse.getErrCode(), null, pdResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pdResponse.getNestedStackTrace(), Severity.HIGH);
		}
	}
	
	private PdResponse getJsonAsPDResponse(String jsonString){
		if (jsonString != null) {
			try {
				ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PdResponse.class);
				return objectReader.readValue(jsonString);
			} catch (JsonParseException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			} catch (JsonMappingException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			} catch (IOException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			}
		} else {
			return null;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<PdOrderItemDTO> findCartItemsByHouseholdId(Long householdId) throws GIRuntimeException {
		try {
			String response =  ghixRestTemplate.getForObject(PlandisplayEndpoints.FIND_CART_ITEMS_BY_HOUSEHOLD_ID+"?householdId="+householdId, String.class);

			JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
				 @Override
				 public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
					 return json == null ? null : new Date(json.getAsLong());
				 }
			};

			Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, deser).create();
			List<PdOrderItemDTO> pdPersonDTOList = new ArrayList<PdOrderItemDTO>();
			if(response!=null){
				Type type = new TypeToken<List<PdOrderItemDTO>>() {}.getType();
				pdPersonDTOList = (List<PdOrderItemDTO>)gson.fromJson(response, type);
			}			

			return pdPersonDTOList;
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PESON_BY_HOUSEHOLD_ID, null, PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PESON_BY_HOUSEHOLD_ID, Severity.HIGH);
		} 
	}
	
	
	public IndividualPlan getPlanDetails(Long planId, Date quotingCoverageDate, boolean minimizeData) throws GIRuntimeException {
		try {
			String coverageStartDate = DateUtil.dateToString(quotingCoverageDate, "yyyy-MM-dd");
			String minimizeDataStr = minimizeData ? "true" : "false";
			String response =  ghixRestTemplate.getForObject(PlandisplayEndpoints.GET_PLAN_DETAILS+"?planId="+planId+"&coverageStartDate="+coverageStartDate+"&minimizeData="+minimizeDataStr, String.class);
			ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(IndividualPlan.class);
			IndividualPlan individualPlan = objectReader.readValue(response);
			return individualPlan;
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_HOUSEHOLD_BY_SHOPPING_ID, null, PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_HOUSEHOLD_BY_SHOPPING_ID, Severity.HIGH);
		} 
	}
	
	public String getPlans(PdSession pdSession, boolean minimizePlanData, String initialCmsPlanId, String internalPlanId, Boolean showPrevYearEnrlPlan) throws GIRuntimeException {
		
		PdQuoteRequest pdQuoteRequest = new PdQuoteRequest();		
		pdQuoteRequest.setPdPreferencesDTO(pdSession.getPdPreferencesDTO());
		pdQuoteRequest.setPdHouseholdDTO(pdSession.getPdHouseholdDTO());
		pdQuoteRequest.setPdPersonDTOList(pdSession.getPdPersonDTOList());
		pdQuoteRequest.setExchangeType(ExchangeType.ON);
		pdQuoteRequest.setShowCatastrophicPlan(pdSession.getShowCatastrophicPlan());
		pdQuoteRequest.setInsuranceType(pdSession.getInsuranceType());
		pdQuoteRequest.setMinimizePlanData(minimizePlanData);
		pdQuoteRequest.setIssuerId(pdSession.getIssuerId());
		if(EnrollmentType.S.equals(pdSession.getPdHouseholdDTO().getEnrollmentType())){
			pdQuoteRequest.setSpecialEnrollmentFlowType(pdSession.getSpecialEnrollmentFlowType());
			if(InsuranceType.HEALTH.equals(pdSession.getInsuranceType())){
				pdQuoteRequest.setInitialSubscriberId(pdSession.getInitialHealthSubscriberId());
			}else{
				pdQuoteRequest.setInitialSubscriberId(pdSession.getInitialDentalSubscriberId());
			}
			if(EnrollmentFlowType.NEW.equals(pdSession.getSpecialEnrollmentFlowType())){
				if(InsuranceType.HEALTH.equals(pdSession.getInsuranceType())){
					pdQuoteRequest.setInitialPlanIdToEliminate(pdSession.getInitialHealthPlanId());
					pdQuoteRequest.setInitialCmsPlanId(pdSession.getInitialHealthCmsPlanId());
				}else{
					pdQuoteRequest.setInitialPlanIdToEliminate(pdSession.getInitialDentalPlanId());
				}
			}else{
				pdQuoteRequest.setInitialCmsPlanId(initialCmsPlanId);
			}
		}
		
		if(EnrollmentType.A.equals(pdSession.getPdHouseholdDTO().getEnrollmentType())) {
			pdQuoteRequest.setInitialCmsPlanId(initialCmsPlanId);
		}
		if(StringUtils.isNotBlank(internalPlanId)){
			List<String> planIdList = new ArrayList<String>();
			planIdList.add(internalPlanId);
			pdQuoteRequest.setPlanIdList(planIdList);
		}		
		pdQuoteRequest.setShowPrevYearEnrlPlan(showPrevYearEnrlPlan);
		
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PdQuoteRequest.class);
			postParams = writer.writeValueAsString(pdQuoteRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(pdQuoteRequest);
		String jsonString = getPOSTData(PlandisplayEndpoints.GET_PLANS, postParams);

		return jsonString;
	}
	
	public PdResponse deleteCartItem(Long itemId){
		String postParams = itemId.toString();
		String jsonString = getPOSTData(PlandisplayEndpoints.DELETE_CARTITEM_URL, postParams);
		PdResponse pdResponse = getJsonAsPDResponse(jsonString);
		return pdResponse;
	}
	
	public String updateSeekCoverageDental(List<PdPersonDTO> pdPersonDTOList)throws GIRuntimeException {
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaTypeList(pdPersonDTOList.getClass(), PdPersonDTO.class);
		 	 postParams = writer.writeValueAsString(pdPersonDTOList);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(pdPersonDTOList);
		String jsonString = getPOSTData(PlandisplayEndpoints.UPDATE_SEEK_COVERAGE_DENTAL, postParams);
		
		JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
			 @Override
			 public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
				 return json == null ? null : new Date(json.getAsLong());
			 }
		};

		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, deser).create();
		PdResponse pdResponse = gson.fromJson(jsonString, PdResponse.class);

		if (null == pdResponse) {
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_UPDATE_SEEK_COVERAGE_DENTAL, null, PlanDisplayConstants.FAILED_TO_EXECUTE_UPDATE_SEEK_COVERAGE_DENTAL, Severity.HIGH);
		}
		if(STATUS_FAILED.equalsIgnoreCase(pdResponse.getStatus())) // If status is failed . 
		{
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+pdResponse.getErrCode(), null, pdResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pdResponse.getNestedStackTrace(), Severity.HIGH);
		}
		return pdResponse.getStatus();
	}
	
	public List<Long> getOrderItemIdsByHouseholdId(Long householdId) {
		String postParams = householdId.toString();
		List<Long> orderItemIds = new ArrayList<Long>();
		try {
			String jsonString = getPOSTData(PlandisplayEndpoints.FIND_ORDERITEM_IDS_BY_HOUSEHOLDID, postParams);
			PdResponse pdResponse = getJsonAsPDResponse(jsonString);
			if(null == pdResponse){
				return null;
			}
			orderItemIds = pdResponse.getOrderItemIds();
		}
		catch(Exception e) {
			giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ PlanDisplayConstants.FAILED_TO_EXECUTE_DELETE_CART_ITEM, new TSDate(),PlanDisplayConstants.FAILED_TO_EXECUTE_DELETE_CART_ITEM, PlanDisplayConstants.FAILED_TO_EXECUTE_DELETE_CART_ITEM + PlanDisplayConstants.HYPHEN , null);
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		return orderItemIds;
	}
	
	public PdResponse saveCartItem(PdSaveCartRequest pdSaveCartRequest) throws GIRuntimeException {
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PdSaveCartRequest.class);
			postParams = writer.writeValueAsString(pdSaveCartRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(pdSaveCartRequest);
		String jsonString = getPOSTData(PlandisplayEndpoints.SAVE_CART_ITEM_URL, postParams);
		
		PdResponse pdResponse = getJsonAsPDResponse(jsonString);
		if (pdResponse == null) {
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_CART_ITEM, null, PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_CART_ITEM, Severity.HIGH);
		}
		
		// If status is failed . 
		if(STATUS_FAILED.equalsIgnoreCase(pdResponse.getStatus())) {
			//LOGGER.error(pdResponse.getErrMsg() + PlanDisplayConstants.HYPHEN + pdResponse.getNestedStackTrace());
			giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ pdResponse.getErrCode(), new TSDate(), pdResponse.getErrMsg(), pdResponse.getErrMsg() + PlanDisplayConstants.HYPHEN + pdResponse.getNestedStackTrace(), null);
		}

		return pdResponse;
	}
	
		
	public void updateHouseholdSubsidy(PdHouseholdDTO pdHouseholdDTO) {
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PdHouseholdDTO.class);
			postParams = writer.writeValueAsString(pdHouseholdDTO);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(pdHouseholdDTO);

		getPOSTData(PlandisplayEndpoints.UPDATE_HOUSEHOLD_SUBSIDY, postParams);
	}
	
		
	public PdHouseholdDTO reCalculateSubsidy(Long householdId, float aptc) throws GIRuntimeException {
		try {
			Map<String,String> inputData = new HashMap<String,String>();
			inputData.put("householdId", householdId.toString());
			inputData.put("aptc", aptc+"");
			String postParams = generateHashMapStringJSON(inputData);
			
			String response =  ghixRestTemplate.postForObject(PlandisplayEndpoints.RE_CALCULATE_SUBSIDY, postParams, String.class);
			ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PdHouseholdDTO.class);
			PdHouseholdDTO pdHouseholdDTO = objectReader.readValue(response);
			return pdHouseholdDTO;
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_RE_CALCULATE_SUBSIDY, null, PlanDisplayConstants.FAILED_TO_EXECUTE_RE_CALCULATE_SUBSIDY, Severity.HIGH);
		} 
	}
	
	
	
	
	
	// Old Rest Client
	
	

	public PldHousehold findPldHouseholdById(int householdId) throws GIRuntimeException
	{
		String postParams = "\"" + householdId + "\"";
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Fetching pldHousehold---->"+postParams, null, false);
		String jsonString = getPOSTData(PlandisplayEndpoints.FIND_PLD_HOUSEHOLD_BYID_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PLD_HOUSEHOLDBY_ID, new TSDate(),PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PLD_HOUSEHOLDBY_ID, PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PLD_HOUSEHOLDBY_ID + PlanDisplayConstants.HYPHEN , null);
			return null;
		}
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{
			giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PLD_HOUSEHOLDBY_ID, new TSDate(),PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PLD_HOUSEHOLDBY_ID, PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PLD_HOUSEHOLDBY_ID + PlanDisplayConstants.HYPHEN , null);
			return null;
		}
		return pldResponse.getPldHousehold();
	}
	
	public PldHousehold findPldHouseholdByCmrHouseholdId(int cmrHouseholdId) {

		String postParams = "\"" + cmrHouseholdId + "\"";
		//LOGGER.info("Fetching pldHousehold  : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.FIND_PLD_HOUSEHOLD_BY_CMR_HOSUSEHOLD_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			return null;
		}
		return pldResponse.getPldHousehold();
	}
	
	
	public Map<String,Object> findPldOrderItemById(int orderItemId)throws GIRuntimeException {

		String postParams = "\"" + orderItemId + "\"";
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Fetching pldOrderItem---->"+postParams, null, false);
		String jsonString = getPOSTData(PlandisplayEndpoints.FIND_PLD_ORDERITEM_BYID_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PLD_ORDER_ITEM_BY_ID, null, PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PLD_ORDER_ITEM_BY_ID, Severity.HIGH);
		}
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+pldResponse.getErrCode(), null, pldResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pldResponse.getNestedStackTrace(), Severity.HIGH);
		}
		return pldResponse.getOutputData();
	}

	public PldHousehold saveHousehold(PldHousehold pldHousehold) {
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PldHousehold.class);
			postParams = writer.writeValueAsString(pldHousehold);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(pldHousehold);
		//LOGGER.info("Saving PldHousehold : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.SAVE_HOUSEHOLD_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			return null;
		}
		return pldResponse.getPldHousehold();
	}

	public void deleteIndividualPersonGroup(int groupId, int personId) {
		Map<String, Integer> inputData = new HashMap<String, Integer>();
		inputData.put("groupId", groupId);
		inputData.put("personId", personId);
		
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForHashMap(String.class, Integer.class);
			postParams = writer.writeValueAsString(inputData);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(inputData);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Deleting IndividualPersonGroup---->"+postParams, null, false);
		getPOSTData(PlandisplayEndpoints.DELETE_INDIVIDUAL_PERSON_GROUP_URL, postParams);
		
	}

	public String getCartItemByPlan(int householdId, String status, int groupId, int planId) 
	{
		Map<String, Object> inputData = new HashMap<String, Object>();
		inputData.put("householdId", householdId);
		inputData.put("status", status);
		inputData.put("groupId", groupId);
		inputData.put("planId", planId);
		inputData.put("planId", planId);
		
		String postParams = generateHashMapObjectJSON(inputData);
		//LOGGER.info("Fetching  PldOrderItem : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.GET_CARTITEM_BYPLAN_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			return null;
		}
		return pldResponse.getResponse();
	}
	
	public List<IndividualPlan> getPlanInfo(String planId, String coverageStartDate,boolean ...minimizePlanData) throws GIRuntimeException {
		Map<String, Object> inputData = new HashMap<String, Object>();
		inputData.put("coverageStartDate", coverageStartDate);
		inputData.put("planId", planId);
		if(null!=minimizePlanData && minimizePlanData.length>0)
		{
			inputData.put("minimizePlanData",minimizePlanData[0]);
		}
		
		String postParams = generateHashMapObjectJSON(inputData);
		//LOGGER.info("Fetching  IndividualPlanList : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.GET_PLAN_INFO_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_GET_PALN_INFO, null, PlanDisplayConstants.FAILED_TO_EXECUTE_GET_PALN_INFO, Severity.HIGH);
		}
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+pldResponse.getErrCode(), null, pldResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pldResponse.getNestedStackTrace(), Severity.HIGH);
		}
		return pldResponse.getIndividualPlanList();

	}

    public void deleteOrderItem(int itemId) 
    {
        String postParams = "\"" + itemId + "\"";
        try 
        {
            getPOSTData(PlandisplayEndpoints.DELETE_ORDERITEM_URL, postParams);
        }
        catch(Exception e) 
        {
            giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ PlanDisplayConstants.FAILED_TO_EXECUTE_DELETE_ORDER_ITEM, new TSDate(),PlanDisplayConstants.FAILED_TO_EXECUTE_DELETE_ORDER_ITEM, PlanDisplayConstants.FAILED_TO_EXECUTE_DELETE_ORDER_ITEM + PlanDisplayConstants.HYPHEN , null);
            PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
        }
    }

	public List<IndividualPlan> getCartItems(Map<String, Integer> preferences, Map<String, String> household, String coverageStartDate, GroupData groupData, int householdId, String status, String planLevel, Boolean showCatastrophicPlan, String planType, String pgrmType,String isSpecialEnrollment, Map<String,Object> subscriberData, String eligLeadBenefits, String tenant, String totalContribution, String renewalType, String exchangeType) 
	{
		PlanDisplayRequest planDisplayRequest = new PlanDisplayRequest();
		
		Map<String, Object> inputData = new HashMap<String, Object>();		
		inputData.put("preferences", preferences);
		inputData.put("household", household);
		inputData.put("coverageStartDate", coverageStartDate);
		inputData.put("householdId", householdId);
		inputData.put("status", status);
		inputData.put("planLevel", planLevel);
		inputData.put("showCatastrophicPlan", showCatastrophicPlan);
		inputData.put("planType", planType);
		inputData.put("pgrmType", pgrmType);
		inputData.put("isSpecialEnrollment", isSpecialEnrollment);
		inputData.put("eligLeadBenefits", eligLeadBenefits);
		inputData.put("tenant", tenant);
		inputData.put("totalContribution", totalContribution);
		inputData.put("renewalType", renewalType);
		inputData.put("exchangeType", exchangeType);
		
		planDisplayRequest.setGroupData(groupData);
		planDisplayRequest.setInputData(inputData);
		
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PlanDisplayRequest.class);
			postParams = writer.writeValueAsString(planDisplayRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(planDisplayRequest);
		//LOGGER.info("Fetching  IndividualPlanList : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.GET_CARTITEMS_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ PlanDisplayConstants.FAILED_TO_EXECUTE_GET_CART_ITEMS, new TSDate(),PlanDisplayConstants.FAILED_TO_EXECUTE_GET_CART_ITEMS, PlanDisplayConstants.FAILED_TO_EXECUTE_GET_CART_ITEMS + PlanDisplayConstants.HYPHEN , null);
			return null;
		}
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{
			giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ pldResponse.getErrCode(), new TSDate(), pldResponse.getErrMsg(), pldResponse.getErrMsg() + PlanDisplayConstants.HYPHEN + pldResponse.getNestedStackTrace(), null);
		return null;
		}
		

		return pldResponse.getIndividualPlanList();		
	}

	public List<IndividualPlan> getIndividualPlans(String insuranceType, Integer groupId, Map<String, Integer> preferences, Map<String, String> household, String coverageStartDate, GroupData groupData, String planIdStr, String planLevel, Boolean showCatastrophicPlan, String planType, String pgrmType, List<List<Map<String,Object>>> providers, String isSpecialEnrollment,String enrollmentType, Map<String,Object> subscriberData, String eligLeadBenefits, String tenant, String totalContribution, String renewalType, String exchangeType,boolean...minimizePlanData) throws GIRuntimeException 
	{
		PlanDisplayRequest planDisplayRequest = new PlanDisplayRequest();
		
		Map<String, Object> inputData = new HashMap<String, Object>();
		inputData.put("insuranceType", insuranceType);
		inputData.put("groupId", groupId);
		inputData.put("preferences", preferences);
		inputData.put("household", household);
		inputData.put("coverageStartDate", coverageStartDate);
		inputData.put("planIdStr", planIdStr);
		inputData.put("planLevel", planLevel);
		inputData.put("showCatastrophicPlan", showCatastrophicPlan);
		inputData.put("planType", planType);
		inputData.put("pgrmType", pgrmType);
		inputData.put("providers", providers);
		inputData.put("isSpecialEnrollment", isSpecialEnrollment);
		inputData.put("enrollmentType", enrollmentType);
		inputData.put("subscriberData", subscriberData);
		inputData.put("eligLeadBenefits", eligLeadBenefits);
		inputData.put("tenant", tenant);
		inputData.put("totalContribution", totalContribution);
		inputData.put("renewalType", renewalType);
		inputData.put("exchangeType", exchangeType);
		if(minimizePlanData!=null && minimizePlanData.length > 0)
		{
			inputData.put("minimizePlanData",Boolean.toString(minimizePlanData[0]));
		}
		
		//Issuer Id in case of PHIX (anonymous) flow.
		String issuerId = null;
		boolean issuerVerifiedFlag = true;
		
		try
		{
			issuerId = String.valueOf(issuerService.getIssuerId());
			if(issuerId != null && !issuerId.equals("0"))
			{
				issuerVerifiedFlag = false;
			}
		}
		catch(Exception e){
			// Incase issuer is not logged in
			issuerId = null ;
		}
		
		inputData.put("issuerId", issuerId);
		inputData.put("issuerVerifiedFlag", issuerVerifiedFlag);
		
		planDisplayRequest.setInputData(inputData);
		planDisplayRequest.setGroupData(groupData);
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PlanDisplayRequest.class);
			postParams = writer.writeValueAsString(planDisplayRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(planDisplayRequest);
		//LOGGER.info("Fetching  IndividualPlanList : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.GET_INDIVIDUALPLANS_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ PlanDisplayConstants.FAILED_TO_EXECUTE_GET_PLANS, new TSDate(),PlanDisplayConstants.FAILED_TO_EXECUTE_GET_PLANS, PlanDisplayConstants.FAILED_TO_EXECUTE_GET_PLANS + PlanDisplayConstants.HYPHEN , null);
			return null;
		}
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ +pldResponse.getErrCode(), new TSDate(),PlanDisplayConstants.FAILED_TO_EXECUTE_GET_PLANS, pldResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pldResponse.getNestedStackTrace() , null);
		return null;
		}
		return pldResponse.getIndividualPlanList();
	}

	public Map<String, Object> postSplitHousehold(Map<String, Object> inputData) throws GIRuntimeException {
		String postParams = generateHashMapObjectJSON(inputData);
		//LOGGER.info("Fetching  postSplitHousehold inputs : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.POST_SPLITHOUSEHOLD_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_POST_SPLITHOUSEHOLD, null, PlanDisplayConstants.FAILED_TO_EXECUTE_POST_SPLITHOUSEHOLD, Severity.HIGH);
		}
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+pldResponse.getErrCode(), null, pldResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pldResponse.getNestedStackTrace(), Severity.HIGH);
		}
		return pldResponse.getOutputData();
	}

	public PremiumTaxCreditResponse showPreferences(Map<String, Object> inputData) {
		String postParams = generateHashMapObjectJSON(inputData);
		//LOGGER.info("Fetching  showPreferences inputs : " + postParams);
		String response = getPOSTData(PlandisplayEndpoints.SHOW_PREFERENCES_URL, postParams);
		if (response != null) {
			try {
				ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PremiumTaxCreditResponse.class);
				 return objectReader.readValue(response);
			} catch (JsonParseException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			} catch (JsonMappingException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			} catch (IOException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			}
		} else {
			return null;
		}
	}

	public Map<String, Object> showCustomgroups(Map<String, Object> inputData) {
		String postParams = generateHashMapObjectJSON(inputData);
		//LOGGER.info("Fetching  showCustomgroups inputs : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.SHOW_CUSTOMGROUPS_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			return null;
		}
		return pldResponse.getOutputData();		
	}

	public Map<String, Object> postCustomgroups(Map<Integer, Boolean> grpIds, int householdId) {
		Map<String, Object> inputData = new HashMap<String, Object>();
		inputData.put("grpIds", grpIds);
		inputData.put("householdId", householdId);
		
		String postParams = generateHashMapObjectJSON(inputData);
		//LOGGER.info("Fetching  postCustomgroups inputs : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.POST_CUSTOMGROUPS_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			return null;
		}
		return pldResponse.getOutputData();		
	}
	
	public String createAndUpdateGroup(int householdId, ArrayList<Map<String, String>> individualPersonList, String groupName, List<String> members, int groupId) {
		Map<String, Object> inputData = new HashMap<String, Object>();
		inputData.put("householdId", householdId);
		inputData.put("individualPersonList", individualPersonList);
		inputData.put("groupName", groupName);
		inputData.put("members", members);
		inputData.put("groupId", groupId);
		
		String postParams = generateHashMapObjectJSON(inputData);
		//LOGGER.info("Fetching  createAndUpdateGroup inputs : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.CREATE_ANDUPDATE_GROUP_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			return null;
		}
		return pldResponse.getResponse();
	}
	

	public PremiumTaxCreditResponse showingCart(PlanDisplayRequest planDisplayRequest) {
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PlanDisplayRequest.class);
			postParams = writer.writeValueAsString(planDisplayRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(planDisplayRequest);
		//LOGGER.info("Fetching  showingCart inputs : " + postParams);
		String response = getPOSTData(PlandisplayEndpoints.SHOWING_CART_URL, postParams);
		if (response != null) {
			try {
				ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PremiumTaxCreditResponse.class);
				return objectReader.readValue(response);
			} catch (Exception e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ PlanDisplayConstants.FAILED_TO_EXECUTE_SHOWING_CART, new TSDate(),PlanDisplayConstants.FAILED_TO_EXECUTE_SHOWING_CART, PlanDisplayConstants.FAILED_TO_EXECUTE_SHOWING_CART + PlanDisplayConstants.HYPHEN , null);
				return null;
			} 
		} else {
			giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ PlanDisplayConstants.FAILED_TO_EXECUTE_SHOWING_CART, new TSDate(),PlanDisplayConstants.FAILED_TO_EXECUTE_SHOWING_CART, PlanDisplayConstants.FAILED_TO_EXECUTE_SHOWING_CART + PlanDisplayConstants.HYPHEN , null);
			return null;
		}
	}
	
	
	public Map<String, Object> populateHousehold(Map<String, Object> inputData) {
		String postParams = generateHashMapObjectJSON(inputData);
		//LOGGER.info("Fetching  populateHousehold inputs : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.POPULATE_PDLHOUSEHOLD_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if (null == pldResponse) {
			return null;
		}
		return pldResponse.getOutputData();
		
	}
	
	
	public PlanDisplayResponse saveCartItems(Map<String, Object> inputData) throws GIRuntimeException{
		String postParams = generateHashMapObjectJSON(inputData);
		//LOGGER.info("Saving CartItems : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.SAVE_CART_ITEMS_URL, postParams);
		//LOGGER.info("Saving CartItems : jsonString " + jsonString);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if (pldResponse == null) {
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_CART_ITEMS, null, PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_CART_ITEMS, Severity.HIGH);
		}
		//LOGGER.info("Saving CartItems : "+pldResponse);
		//LOGGER.info("Saving CartItems getResponse : "+pldResponse.getResponse());
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, pldResponse.getErrMsg() + PlanDisplayConstants.HYPHEN + pldResponse.getNestedStackTrace(), null, false);
			giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ pldResponse.getErrCode(), new TSDate(), pldResponse.getErrMsg(), pldResponse.getErrMsg() + PlanDisplayConstants.HYPHEN + pldResponse.getNestedStackTrace(), null);
		}


		return pldResponse;
	}
	
	public PlanDisplayResponse findPldHouseholdByShoppingId(String houseHoldId) throws GIRuntimeException {
		String postParams = "\"" + houseHoldId + "\"";
		//LOGGER.info("Fetching  findPldHouseholdByShoppingId by houseHoldId : " + postParams);
		String response = getPOSTData(PlandisplayEndpoints.FIND_PLDHOUSEHOLD_BYSHOPPINGID_URL, postParams);		
		PlanDisplayResponse pldResponse = getJsonAsObject(response);
		if (null == pldResponse) {
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PLDHOUSEHOLD_BY_SHOPPING_ID, null, PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PLDHOUSEHOLD_BY_SHOPPING_ID, Severity.HIGH);
		}
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+pldResponse.getErrCode(), null, pldResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pldResponse.getNestedStackTrace(), Severity.HIGH);
		}
		return pldResponse;		
	}
	
	public Map getHouseholdData(String houseHoldId) {
		String postParams = "\"" + houseHoldId + "\"";
		//LOGGER.info("Fetching  findPldHouseholdByShoppingId by houseHoldId : " + postParams);
		String response = getPOSTData(PlandisplayEndpoints.GET_HOUSEHOLD_DATA_URL, postParams);		
		PlanDisplayResponse pldResponse = getJsonAsObject(response);
		if (null == pldResponse) {
			return null;
		}
		return pldResponse.getHouseholdData();		
	}
	
	public String sendAdminUpdate(PlanDisplayRequest planDisplayRequest) {
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PlanDisplayRequest.class);
			postParams = writer.writeValueAsString(planDisplayRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(planDisplayRequest);
		//LOGGER.info("Fetching  sendAdminUpdate inputs : " + postParams);
		String response = getPOSTData(PlandisplayEndpoints.SEND_ADMIN_UPDATE_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(response);
		if(null == pldResponse){
			return null;
		}
		Map<String, Object> outputData = pldResponse.getOutputData();
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Admin Update URL---->"+GhixEndPoints.EnrollmentEndPoints.ADMIN_UPDATE_ENROLLMENT_URL, null, false);
		//LOGGER.info("Request Data "+outputData.get("enrollmentRequest"));
		String enrollmentRequestJsonString = platformGson.toJson(outputData.get("enrollmentRequest"));
		String postResp = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.ADMIN_UPDATE_ENROLLMENT_URL, enrollmentRequestJsonString, String.class);
		PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.INFO, "<----Response from admin update---->"+postResp, null, true);
		return "SUCCESS";
	}
	
	public Map extractPersonData(String personData) {
		String postParams = "\"" + personData + "\"";
		String response = getPOSTData(PlandisplayEndpoints.EXTRACT_PERSON_DATA_URL , postParams);		
		PlanDisplayResponse pldResponse = getJsonAsObject(response);
		if (null == pldResponse) {
			return null;
		}
		return pldResponse.getHouseholdData();		
	}	
	
	private String generateHashMapStringJSON(Map<String, String> str){
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForHashMap(String.class, String.class);
			postParams = writer.writeValueAsString(str);
			return postParams;
		} catch (JsonParseException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		} catch (JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		}
	}
	
	private String generateHashMapObjectJSON(Map<String, Object> obj){
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForHashMap(String.class, Object.class);
			postParams = writer.writeValueAsString(obj);
			return postParams;
		} catch (JsonParseException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		} catch (JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		}
	}
	
	private String getPOSTData(final String requestURL, final String postParams){
		String response = null;
		long startTime = planDisplayUtil.startMethodLog("REST Call getPOSTData() : "+ requestURL);
		try {
			response = ghixRestTemplate.postForObject(requestURL, postParams,String.class);
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		planDisplayUtil.endMethodLog("REST Call getPOSTData() : "+ requestURL, startTime);
		return response;
	}
	
	private PlanDisplayResponse getJsonAsObject(String jsonString){
		if (jsonString != null) {
			try {
				ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PlanDisplayResponse.class);
				 return objectReader.readValue(jsonString);
			} catch (JsonParseException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			} catch (JsonMappingException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			} catch (IOException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			}
		} else {
			return null;
		}		
	}
	
	public String saveOrderItem(PlanDisplayRequest planDisplayRequest) throws GIRuntimeException
	{
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PlanDisplayRequest.class);
			postParams = writer.writeValueAsString(planDisplayRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(planDisplayRequest);
		//LOGGER.info("Fetching  saveOrderItem inputs : " + postParams);
		String response = getPOSTData(PlandisplayEndpoints.SAVE_ORDER_ITEM_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(response);
		if(null == pldResponse){
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_ORDER_ITEM, null, PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_ORDER_ITEM, Severity.HIGH);
		}
	
	if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
	{
		throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+pldResponse.getErrCode(), null, pldResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pldResponse.getNestedStackTrace(), Severity.HIGH);
		}
		return pldResponse.getStatus();
	}
	
	public String saveAptc(PlanDisplayRequest planDisplayRequest) throws GIRuntimeException
	{
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PlanDisplayRequest.class);
			postParams = writer.writeValueAsString(planDisplayRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(planDisplayRequest);
		//LOGGER.info("Fetching  saveAptc inputs : " + postParams);
		String response = getPOSTData(PlandisplayEndpoints.SAVE_APTC_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(response);
		if(null == pldResponse){
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_APTC, null, PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_APTC, Severity.HIGH);
		}
	
	if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
	{
		throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+pldResponse.getErrCode(), null, pldResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pldResponse.getNestedStackTrace(), Severity.HIGH);
		}
		return pldResponse.getStatus();
	}
	
	
	
	
	public Map<String, String> getEmployer1(String externalEmployerId) throws GIException {
		Map<String, String> responseData  = new HashMap<String, String>();
		String get_resp = restTemplate.getForObject(ShopEndPoints.GET_EMPLOYER_ENROLLMENT+"?externalEmployerId="+externalEmployerId,String.class);
		
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		ShopResponse  shopResponse = (ShopResponse) xstream.fromXML(get_resp);
		
		if(shopResponse.getErrCode()==1){
			responseData.put("Error", shopResponse.getErrMsg());
			return responseData;
		}
		
		EmployerDTO employerDTO = (EmployerDTO) shopResponse.getResponseData().get("Employer");
	
		responseData.put("employerId", String.valueOf(employerDTO.getId()));
		
		return responseData;
	}
	
	public EnrollmentResponse findPlanIdAndOrderItemIdByEnrollmentId(String id)
	{		
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		enrollmentRequest.setEnrollmentId(Integer.parseInt(id));
		/*XStream xstream = GhixUtils.getXStreamStaxObject();
		String postResp = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.GET_PLANID_AND_ORDERITEMID_BY_ENROLLMENT_ID_URL, xstream.toXML(enrollmentRequest),String.class);*/
		String postResp = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.GET_PLANID_ORDERITEM_ID_BY_ENROLLMENT_ID_JSON, platformGson.toJson(enrollmentRequest), String.class);
		EnrollmentResponse enrollmentResponse = platformGson.fromJson(postResp, EnrollmentResponse.class);
		return enrollmentResponse;
	}
	
	
	
	public String keepPlan(Map<String, Object> inputData) throws GIRuntimeException{
		String postParams = generateHashMapObjectJSON(inputData);
		//LOGGER.info("In keepPlan : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.SPECIAL_ENROLLMENT_KEEP_PLAN_URL, postParams);
		//LOGGER.info("In keepPlan : jsonString " + jsonString);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if (pldResponse == null) {
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_KEEP_PLAN, null, PlanDisplayConstants.FAILED_TO_EXECUTE_KEEP_PLAN, Severity.HIGH);
		}
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+pldResponse.getErrCode(), null, pldResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pldResponse.getNestedStackTrace(), Severity.HIGH);
		}
		//LOGGER.info("In keepPlan Response : "+pldResponse.getResponse());
		return pldResponse.getStatus();
	}
	
	public String specialEnrollmentPersonDataUpdate(Map<String, Object> inputData) {
		String postParams = generateHashMapObjectJSON(inputData);
		//LOGGER.info("In specialEnrollmentPersonDataUpdate : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.SPECIAL_ENROLLMENT_PERSONDATA_UPDATE, postParams);
		//LOGGER.info("In specialEnrollmentPersonDataUpdate : jsonString " + jsonString);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ PlanDisplayConstants.FAILED_TO_EXECUTE_SPECIAL_ENROLLMENT_PERSONDATA_UPDATE, new TSDate(),PlanDisplayConstants.FAILED_TO_EXECUTE_SPECIAL_ENROLLMENT_PERSONDATA_UPDATE, PlanDisplayConstants.FAILED_TO_EXECUTE_SPECIAL_ENROLLMENT_PERSONDATA_UPDATE + PlanDisplayConstants.HYPHEN , null);
			return null;
		}
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ pldResponse.getErrCode(), new TSDate(), pldResponse.getErrMsg(), pldResponse.getErrMsg() + PlanDisplayConstants.HYPHEN + pldResponse.getNestedStackTrace(), null);
		
		}
		//LOGGER.info("In specialEnrollmentPersonDataUpdate Response : "+pldResponse);
		return pldResponse.getResponse();
	}	
	
	public String checkItemInCart(Map<String, String> inputData) throws GIRuntimeException
	{
		String postParams = generateHashMapStringJSON(inputData);
		//LOGGER.info("In checkItemInCart : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.SPECIAL_ENROLLMENT_CHECK_ITEMINCART, postParams);
		//LOGGER.info("In checkItemInCart : jsonString " + jsonString);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if (pldResponse == null) {

			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_CHECK_ITEM_IN_CART, null, PlanDisplayConstants.FAILED_TO_EXECUTE_CHECK_ITEM_IN_CART, Severity.HIGH);
			}
		//LOGGER.info("In checkItemInCart Response : "+pldResponse);
		
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+pldResponse.getErrCode(), null, pldResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pldResponse.getNestedStackTrace(), Severity.HIGH);
		}
		
		return pldResponse.getResponse();
	}

	public PremiumTaxCreditResponse formGroupList(Map<String, Object> inputData) {
		String postParams = generateHashMapObjectJSON(inputData);
		//LOGGER.info("Fetching formGroupList inputs : " + postParams);
		String response = getPOSTData(PlandisplayEndpoints.FORM_GROUP_LIST, postParams);
		if (response != null) {
			try {
				 ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PremiumTaxCreditResponse.class);
				 return objectReader.readValue(response);
			} catch (JsonParseException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			} catch (JsonMappingException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			} catch (IOException e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			}
		} else {
			return null;
		}
	}
	
	public PldHousehold getHousehold(IndividualRequest individualRequest) {
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(IndividualRequest.class);
			postParams = writer.writeValueAsString(individualRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(individualRequest);
		//LOGGER.info("Getting Household ID" + postParams);
		String response = getPOSTData(PlandisplayEndpoints.GET_PLDHOUSEHOLD_URL, postParams);		
		PlanDisplayResponse pldResponse = getJsonAsObject(response);
		if (null == pldResponse) {
			return null;
		}
		return pldResponse.getPldHousehold();		
	}
	

	public PlanDisplayResponse saveIndividualPHIX(Map<String, Object> inputData) throws GIRuntimeException{
		String postParams = generateHashMapObjectJSON(inputData);
		//LOGGER.info("In saveIndividualPHIX : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.SAVE_INDIVIDUAL_PHIX, postParams);
		//LOGGER.info("In saveIndividualPHIX : jsonString " + jsonString);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_INDIVIDUAL_PHIX, null, PlanDisplayConstants.FAILED_TO_EXECUTE_SAVE_INDIVIDUAL_PHIX, Severity.HIGH);
		}
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+pldResponse.getErrCode(), null, pldResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pldResponse.getNestedStackTrace(), Severity.HIGH);
		}
		//LOGGER.info("In saveIndividualPHIX Response : "+pldResponse);
		return pldResponse;
	}
	
	public Map<String, Object> getPreferences(int groupId) {
		String postParams = "\"" + groupId + "\"";
		//LOGGER.info("Fetching  getPreferences inputs : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.GET_PREFERENCES_URL, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			return null;
		}
		return pldResponse.getOutputData();
	}

	
	public Map<String, Object> getSubscriberData(int householdId, String enrollmentType, String programType, String quotingGroup) throws GIRuntimeException 
	{
		Map<String,String> inputData = new HashMap<String,String>();
	 	inputData.put("householdId", ""+householdId);
	 	inputData.put("enrollmentType", enrollmentType);
	 	inputData.put("programType", programType);
	 	inputData.put("quotingGroup", quotingGroup);
	 	String postParams = generateHashMapStringJSON(inputData);
		String jsonString = getPOSTData(PlandisplayEndpoints.GET_SUBSCRIBER_DATA, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_GET_SUBSCRIBER_DATA, null, PlanDisplayConstants.FAILED_TO_EXECUTE_GET_SUBSCRIBER_DATA, Severity.HIGH);
		}
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+pldResponse.getErrCode(), null, pldResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pldResponse.getNestedStackTrace(), Severity.HIGH);
		}
		return pldResponse.getOutputData();
	}
	
	public void restoreAptcValue(int groupId) {
		String postParams = "\"" + groupId + "\"";

		//LOGGER.info("Restoring AptcValue : " + postParams);
		getPOSTData(PlandisplayEndpoints.RESTORE_APTC_VALUE_URL, postParams);
		
	}
	
	public Map<String, Object> getPlanData(int itemId) {
		String postParams = ""+itemId;

		//LOGGER.info("Restoring AptcValue : " + postParams);
		String response = getPOSTData(PlandisplayEndpoints.GET_PLAN_DATA, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(response);
		if (null == pldResponse) {
			return null;
		}
		return pldResponse.getOutputData();
	}
	
	public PldHousehold saveHouseholdForApplicantEligibility(Map<String, Object> householdMap) {
		String postParams = generateHashMapObjectJSON(householdMap);
		//LOGGER.info("Getting Household ID" + postParams);
		String response = getPOSTData(PlandisplayEndpoints.SAVE_HOUSEHOLD_APPLICANT_ELIGIBILITY, postParams);		
		PlanDisplayResponse pldResponse = getJsonAsObject(response);
		if (null == pldResponse) {
			return null;
		}
		return pldResponse.getPldHousehold();		
	}
	
	public String updateEligLeadRecord(EligLeadRequest eligLeadRequest) {
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(EligLeadRequest.class);
			postParams = writer.writeValueAsString(eligLeadRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(eligLeadRequest);
		//LOGGER.info("Updating stage column in Eleg_Lead table " + postParams);
		String response = getPOSTData(PhixEndPoints.UPDATE_LEAD, postParams);	
		return response;
	}
	
	public String findOrderIdByLeadId(String leadId){
		PldOrderResponse pldOrderResponse = null;
		String orderId = null;
		try{			
			String getResponse = restTemplate.getForObject(PlandisplayEndpoints.FIND_ORDERID_BY_LEADID+leadId, String.class);
			if(getResponse == null){
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----No data found for leadId---->"+leadId, null, false);
				return null;
			}else{
				pldOrderResponse = platformGson.fromJson(getResponse,PldOrderResponse.class);
				if(isNotNull(pldOrderResponse)){
					String status = pldOrderResponse.getStatus();
					if(isNotNull(status) && status.equals(GhixConstants.RESPONSE_SUCCESS)){
						orderId = pldOrderResponse.getOrderId();
						//householdId = pldOrderResponse.getHouseholdId();
					}else{
						PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Not success response code data for leadId---->"+leadId, null, false);
					}
				}else{
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Not success response code data for leadId---->"+leadId, null, false);
				}
			}
		}catch(Exception e){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception while getting orderId by LeadId---->", e, false);
		}
		return orderId;
	}
	
	public PldHousehold findLatestPldHouseholdByLeadId(Map<String,String> householdMap) 
	{
		PlanDisplayResponse planDisplayResponse = null;
		PldHousehold latestPldHousehold = null;
		
		try
		{			
			String postParams = generateHashMapStringJSON(householdMap);
			String getResponse = getPOSTData(PlandisplayEndpoints.FIND_LATEST_PLD_HOUSEHOLD_BY_LEADID, postParams);
			if(getResponse == null) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----No data found for leadId---->"+householdMap.get("leadId").toString(), null, false);
				return null;
			} 
			else
			{
				planDisplayResponse = platformGson.fromJson(getResponse, PlanDisplayResponse.class);
				if(isNotNull(planDisplayResponse))
				{
					String status = planDisplayResponse.getStatus();
					if(isNotNull(status) && status.equals(GhixConstants.RESPONSE_SUCCESS)) {
						latestPldHousehold = planDisplayResponse.getPldHousehold();
					} else {
						PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----No data found for leadId---->"+householdMap.get("leadId").toString(), null, false);
					}
				} else {
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----No data found for leadId---->"+householdMap.get("leadId").toString(), null, false);
				}
			}
		}
		catch(Exception e){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception while getting orderId by LeadId---->", e, false);
		}
		return latestPldHousehold;
	}	
	
	public static <T> boolean isNotNull(T e){
		boolean isNotNUll=false;
		if((e!=null)){
			isNotNUll=true;
		}
		return isNotNUll;
	}
	
	public String getIdFromSAMLResponse(Map<String,String> samlParams) {
		String samlResponse = null;
		try {
			String postParams = generateHashMapStringJSON(samlParams);
			samlResponse = getPOSTData(PlandisplayEndpoints.GET_SAML_RESPONSE, postParams);
			if (null == samlResponse) {
				return null;
			}
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception while getting Id  from SAMLResponse---->", e, false);
		}
		return samlResponse;
	}
	
	/*public String saveApplicantEligibility(Map<String, String> map) {
		PlanDisplayResponse planDisplayResponse = null;
		String pldHouseholdId = null;
		try{			
			String postParams = generateJSON(map);
			LOGGER.info("saveApplicantEligibility input data:" + postParams);
			String getResponse = getPOSTData(HubEndPoints.SAVE_APPLICANT_ELIGIBILITY, postParams);
			if(getResponse == null){
				LOGGER.error("No data found for calling FFM WSDL");
			}else{
				planDisplayResponse = new Gson().fromJson(getResponse, PlanDisplayResponse.class);
				if(isNotNull(planDisplayResponse)){
					String status = planDisplayResponse.getStatus();
					if(isNotNull(status) && status.equals(GhixConstants.RESPONSE_SUCCESS)){
						pldHouseholdId = planDisplayResponse.getResponse();
					}else{
						LOGGER.error("Not success response found from FFM WSDL.");
					}
				}else{
					LOGGER.error("Not success response found from FFM WSDL.");
				}
			}
		}catch(Exception e){
			LOGGER.error("Exception while calling FFM WSDL: " ,e);
		}
		
		return pldHouseholdId;
	}*/
	//TODO: HS : Umcomment the consumer code once it gets integrated
	/*public void saveFFMDataTOMemberPortal(FFMHouseholdResponse fFMHouseholdResponse){		
		try {
			String postParams = generateJSON(fFMHouseholdResponse);
			LOGGER.info("passFFMDataTOMemberPortal input data" + postParams);
			String getResponse = getPOSTData(ConsumerServiceEndPoints.SAVE_FFM_RESPONSE, postParams);
		} catch (Exception e) {
			LOGGER.error("Exception while saving FFM Data To Member Portal: " ,e);
		}
	}*/
	
	//TODO: HS : Umcomment the consumer code once it gets integrated
	/*public void saveTobbacoInfoToMemberPortal(FFMHouseholdResponse fFMHouseholdResponse){		
		try {
			String postParams = generateJSON(fFMHouseholdResponse);
			LOGGER.info("passFFMDataTOMemberPortal input data" + postParams);
			String getResponse = getPOSTData(ConsumerServiceEndPoints.UPDATE_SMOKER_INFO_FOR_MEMBERS, postParams);
		} catch (Exception e) {
			LOGGER.error("Exception while saving smoker Data To Member Portal: " ,e);
		}
	}*/
	
	/*public Map<String, String> getSAMLResponseAttributes(String samlResponse)
	{
		String response = this.getPOSTData(PlandisplayEndpoints.GET_SAML_RESPONSE_ATTRIBUTES, samlResponse);
		try {
			return mapper.readValue(response, Map.class);
		} catch (JsonParseException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		} catch (JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		}
	}*/
	
	public String saveFFMAssignedConsumerIDinPldHousehold(Map<String, String> ffmData)
	{
		String postParams = generateHashMapStringJSON(ffmData);
		return this.getPOSTData(PlandisplayEndpoints.SAVE_FFM_ASSIGNED_CONSUMER_ID, postParams);
	}
	
	public Household getCmrHouseholdByPartnerAssignedConsumerId(String partnerAssignedConsumerID)
	 {
	  Household household = null;
	  try{ 
	   String response = restTemplate.getForObject(ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_GI_HOUSEHOLD_ID+"/"+partnerAssignedConsumerID,String.class);
	   //XStream xStream = GhixUtils.getXStreamStaxObject();
	   //household = (Household)xStream.fromXML(response);
	   ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(Household.class);
	   household = objectReader.readValue(response);
	   if(household == null){
		   PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----No data found in response of calling CmrHouseholdByPartnerAssignedConsumerId---->", null, false);
		   return null;
	   }
	  }catch(Exception e){
		  PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception while calling CmrHouseholdByPartnerAssignedConsumerId---->", e, false);
	  }
	  return household;
	 }
	
	public String saveFFMUserIDinAccountUser(int accountUserId, String ffmUserID)
	{
		this.userService.saveFFMUserId(accountUserId, ffmUserID);
		return "SUCCESS";
	}
	
	public String getDisclaimerInfo(int issuerId, String exchangeType)
	{		
		IssuerRequest issuerRequest = new IssuerRequest();
		issuerRequest.setId(String.valueOf(issuerId));
		issuerRequest.setExchangeType(exchangeType);
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(IssuerRequest.class);
			postParams = writer.writeValueAsString(issuerRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(issuerRequest);
		String jsonString = getPOSTData(PlanMgmtEndPoints.DISCLAIMER_INFO, postParams);
		String disclaimerInfo = null;
		if (jsonString != null) 
		{
			try {
				ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(IssuerResponse.class);
				IssuerResponse response = objectReader.readValue(jsonString);
				disclaimerInfo = response.getDisclaimerInfo();
			} catch (Exception e) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ PlanDisplayConstants.FAILED_TO_EXECUTE_GET_PALN_INFO, new TSDate(),PlanDisplayConstants.FAILED_TO_EXECUTE_GET_PALN_INFO, PlanDisplayConstants.FAILED_TO_EXECUTE_GET_PALN_INFO + PlanDisplayConstants.HYPHEN , null);
				return null;
			}
		}
		return disclaimerInfo;
	}
	
	public String updateContributionPerPerson(Map<String, Float> inputData) {
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForHashMap(String.class, Float.class);
			postParams = writer.writeValueAsString(inputData);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(inputData);
		//LOGGER.info("updateContributionPerPerson : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.UPDATE_CONTRIBUTION_PER_PERSON, postParams);
		//LOGGER.info("updateContributionPerPerson : jsonString " + jsonString);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if (pldResponse == null) {
			return null;
		}
		//LOGGER.info("updateContributionPerPerson : "+pldResponse);
		//LOGGER.info("updateContributionPerPerson getResponse : "+pldResponse.getResponse());
		return pldResponse.getResponse();
	}
	
	public String findPlanIdByHouseholdId(String householdId){
		PldOrderResponse pldOrderResponse = null;
		String planId = null;		
		try{			
			String getResponse = getPOSTData(PlandisplayEndpoints.FIND_PLAN_BY_HOUSEHOLDID, householdId);
			if(getResponse == null){
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----No data found---->", null, false);
				return null;
			}else{
				pldOrderResponse = platformGson.fromJson(getResponse,PldOrderResponse.class);
				if(isNotNull(pldOrderResponse)){
					String status = pldOrderResponse.getStatus();
					if(isNotNull(status) && status.equals(GhixConstants.RESPONSE_SUCCESS)){
						planId = pldOrderResponse.getPlanId();
					}
				}
			}
		}catch(Exception e){
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception while getting planId by LeadId And CmrHouseholdId---->", e, false);
		}
		return planId;
	}
	
	
	
	public String updatePldPersonForTobaccoUsage(List<Map<String, String>> list) {
		PlanDisplayRequest planDisplayRequest = new PlanDisplayRequest();
		Map<String, Object> inputData = new HashMap<String, Object>();
		inputData.put("updatedPersonIdWithTobaccoList", list);
		planDisplayRequest.setInputData(inputData);
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PlanDisplayRequest.class);
			postParams = writer.writeValueAsString(planDisplayRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(planDisplayRequest);
		//LOGGER.info("In updatePldPersonForTobaccoUsage : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.UPDATE_PLDPERSON_FOR_TOBACCO_USAGE, postParams);
		//LOGGER.info("In updatePldPersonForTobaccoUsage : jsonString " + jsonString);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if (pldResponse == null || pldResponse.getStatus()!=null && pldResponse.getStatus().equalsIgnoreCase("FAILED")) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Pld Person is not updated successfully for Tobacco Flag---->", null, false);
			return null;
		}
		//LOGGER.info("In updatePldPersonForTobaccoUsage Response : "+pldResponse);
		return pldResponse.getResponse();
	}
	
	public String findOrderIdByHouseholdIdandStatus(Map<String, String> inputData) {
		String postParams = generateHashMapStringJSON(inputData);
		//LOGGER.info("Find OrderId : " + postParams);
		String jsonString = getPOSTData(PlandisplayEndpoints.FIND_OREDERID_BY_HOUSEHOLDID_CARTSTATUS, postParams);
		//LOGGER.info("Find OrderId : jsonString " + jsonString);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if (pldResponse == null) {
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_GET_ORDER_ID_BY_HOUSEDHOLD_ID_AND_STATUS, null, PlanDisplayConstants.FAILED_TO_EXECUTE_GET_ORDER_ID_BY_HOUSEDHOLD_ID_AND_STATUS, Severity.HIGH);
		}
		
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+pldResponse.getErrCode(), null, pldResponse.getErrMsg()+PlanDisplayConstants.HYPHEN+pldResponse.getNestedStackTrace(), Severity.HIGH);
		} 
		//LOGGER.info("Find OrderId : "+pldResponse);
		//LOGGER.info("Find OrderId getResponse : "+pldResponse.getResponse());
		return pldResponse.getResponse();
	}
	
	
	
	public List<IndividualPlan> getPlanBenefitCostData(List<Integer> planIds, String insuranceType, String coverageDate)
	{
		Map<String, Object> inputData = new HashMap<String, Object>();
		inputData.put("planIds", planIds);
		inputData.put("insuranceType", insuranceType);
		inputData.put("coverageDate", coverageDate);
		
		String postParams = generateHashMapObjectJSON(inputData);
		String jsonString = getPOSTData(PlandisplayEndpoints.FIND_PLAN_BENEFIT_AND_COST_DATA, postParams);
		PlanDisplayResponse pldResponse = getJsonAsObject(jsonString);
		if(null == pldResponse){
			giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ PlanDisplayConstants.FAILED_TO_EXECUTE_GET_PLAN_BENEFIT_COST_DATA, new TSDate(),PlanDisplayConstants.FAILED_TO_EXECUTE_GET_PLAN_BENEFIT_COST_DATA, PlanDisplayConstants.FAILED_TO_EXECUTE_GET_PLAN_BENEFIT_COST_DATA + PlanDisplayConstants.HYPHEN , null);
			return null;
		}
		if(STATUS_FAILED.equalsIgnoreCase(pldResponse.getStatus())) // If status is failed . 
		{giMonitorService.saveOrUpdateErrorLog(PlanDisplayConstants.MODULE_NAME+ pldResponse.getErrCode(), new TSDate(), pldResponse.getErrMsg(), pldResponse.getErrMsg() + PlanDisplayConstants.HYPHEN + pldResponse.getNestedStackTrace(), null);
		return null;
		}
		return pldResponse.getIndividualPlanList();	
	}
	
	public PdOrderItemDTO findCartItemById(Long cartItemId) throws GIRuntimeException {
		try {
			String response =  ghixRestTemplate.getForObject(PlandisplayEndpoints.FIND_CART_ITEM_BY_ID+"?cartItemId="+cartItemId, String.class);
			ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PdOrderItemDTO.class);
			PdOrderItemDTO pdOrderItemDTO = objectReader.readValue(response);
			return pdOrderItemDTO;
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_CART_ITEM_BY_ID, null, PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_CART_ITEM_BY_ID, Severity.HIGH);
		} 
	}
	
	public List<PdOrderItemDTO> findPreShoppingCartItems(String leadId , String year) throws GIRuntimeException {
		try {
			Type type = new TypeToken<List<PdOrderItemDTO>>() {}.getType();
			String response =  ghixRestTemplate.getForObject(PlandisplayEndpoints.FIND_PERSHOPPING_CART_ITEMS+"?leadId="+leadId + "&year="+ year, String.class);

			JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
				 @Override
				 public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
					 return json == null ? null : new TSDate(json.getAsLong());
				 }
			};

			Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, deser).create();
			List<PdOrderItemDTO> pdOrderItemDTOList = null;
			if(response!=null){
				pdOrderItemDTOList = gson.fromJson(response, type);
			}			

			return pdOrderItemDTOList;
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PERSHOPPING_CART_ITEMS, null, PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_PERSHOPPING_CART_ITEMS, Severity.LOW);
		} 
	}
	
	@SuppressWarnings("unchecked")
	public List<Integer> getListOfPlanIds(PdSession pdSession, String zipcode, String countycode,InsuranceType insuranceType) 
	{
		PdQuoteRequest pdQuoteRequest = new PdQuoteRequest();		
		pdQuoteRequest.setPdPreferencesDTO(pdSession.getPdPreferencesDTO());
		pdQuoteRequest.setPdHouseholdDTO(pdSession.getPdHouseholdDTO());
		BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
		if(zipcode != null && countycode != null) {
			List<PdPersonDTO> pdPersonDTOList = new ArrayList<PdPersonDTO>();
			for(PdPersonDTO pdPerson : pdSession.getPdPersonDTOList()) {
				PdPersonDTO pdPersonDTO = new PdPersonDTO();
				try {
					beanUtilsBean.getConvertUtils().register(new DateConverter(null), Date.class);
					beanUtilsBean.copyProperties(pdPersonDTO, pdPerson);
				} 
				catch (IllegalAccessException | InvocationTargetException e1) {
					PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception while executing PlanDisplayRestClient.getListOfPlanIds---->", e1, false);
				}
				pdPersonDTO.setZipCode(zipcode);
				pdPersonDTO.setCountyCode(countycode);
				pdPersonDTOList.add(pdPersonDTO);
			}
			pdQuoteRequest.setPdPersonDTOList(pdPersonDTOList);
		} else {
			pdQuoteRequest.setPdPersonDTOList(pdSession.getPdPersonDTOList());
		}
		pdQuoteRequest.setExchangeType(ExchangeType.ON);
		pdQuoteRequest.setShowCatastrophicPlan(pdSession.getShowCatastrophicPlan());
		pdQuoteRequest.setInsuranceType(insuranceType);
		pdQuoteRequest.setMinimizePlanData(true);
		pdQuoteRequest.setIssuerId(null);
		pdQuoteRequest.setSpecialEnrollmentFlowType(pdSession.getSpecialEnrollmentFlowType());
		if(InsuranceType.HEALTH.equals(insuranceType))
		{
			pdQuoteRequest.setInitialSubscriberId(pdSession.getInitialHealthSubscriberId());
		}
		else if (InsuranceType.DENTAL.equals(insuranceType))
		{
			pdQuoteRequest.setInitialSubscriberId(pdSession.getInitialDentalSubscriberId());
		}
		
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PdQuoteRequest.class);
			postParams = writer.writeValueAsString(pdQuoteRequest);
		} catch (JsonParseException | JsonMappingException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		} catch (IOException e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		//String postParams = generateJSON(pdQuoteRequest);
		String jsonString = getPOSTData(PlandisplayEndpoints.GET_PLANS, postParams);
		
		Type type = new TypeToken<List<IndividualPlan>>() {}.getType();
		List<IndividualPlan> individualPlans = (List<IndividualPlan>)platformGson.fromJson(jsonString, type);
		List<Integer> planIds = new ArrayList<Integer>();
		if(individualPlans != null && !individualPlans.isEmpty()) {
			for(IndividualPlan individualPlan : individualPlans) {
				planIds.add(individualPlan.getPlanId());
			}
		}
		return planIds;
	}
	
	public PdResponse getCrossWalkIssuerPlanId(PdCrossWalkIssuerIdRequest crossWalkRequest) throws GIRuntimeException {
		PdResponse pdResponse = null;
		try {
			String response = ghixRestTemplate.postForObject(PlandisplayEndpoints.GET_CROSSWALK_ISSUER_PLANID, crossWalkRequest, String.class);
			ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PdResponse.class);
			pdResponse = objectReader.readValue(response);
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME + "Error occurred while executing getCrossWalkIssuerPlanId()", null, "Error occurred while executing getCrossWalkIssuerPlanId()", Severity.HIGH);
		}
		return pdResponse;
	}

	public String getRxcuiByNdc(String ndc) throws GIRuntimeException {
		try {
			String response =  ghixRestTemplate.getForObject(PlandisplayEndpoints.GET_RX_CUI_BY_NDC + "/" + ndc, String.class);
			return response;
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME+PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_HOUSEHOLD_BY_SHOPPING_ID, null, PlanDisplayConstants.FAILED_TO_EXECUTE_FIND_HOUSEHOLD_BY_SHOPPING_ID, Severity.HIGH);
		} 
	}
	
	public String appendHiosPlanIdWithCSR(String initialHealthCmsPlanId, String costSharing, String householdId, String planLevel, String isEligibleCatastrophic)
	{
		Map<String, String> inputData = new HashMap<String, String>();
		inputData.put("householdId", householdId);		
		inputData.put("costSharing", costSharing);
		inputData.put("planLevel", planLevel);
		inputData.put("initialHealthCmsPlanId", initialHealthCmsPlanId);
		inputData.put("isEligibleCatastrophic", isEligibleCatastrophic);
		
		String postParams = generateHashMapStringJSON(inputData);
		String jsonString = getPOSTData(PlandisplayEndpoints.APPEND_HIOSPLANID_WITH_CSR, postParams);
		PdResponse pldResponse = getJsonAsPDResponse(jsonString);
		if(null == pldResponse){
			return null;
		}
		
		return pldResponse.getResponse();	
	}
	
	public CostSharing restoreCSRValue(Long householdId)
	{
		Map<String, String> inputData = new HashMap<String, String>();
		CostSharing costSharing = null; 
		String postParams = generateHashMapStringJSON(inputData);
		String jsonString = getPOSTData(PlandisplayEndpoints.RESTORE_CSR_VALUE+"/"+householdId, postParams);
		PdResponse pdResponse = getJsonAsPDResponse(jsonString);
		if(null == pdResponse){
			return null;
		}
		if("SUCCESS".equals(pdResponse.getStatus()) && pdResponse.getPdHouseholdDTO() != null)
		{
			costSharing = pdResponse.getPdHouseholdDTO().getCostSharing();
		}
		return costSharing;	
	}
	
	public Plan getPlanDetails(String hiosPlanId, String effectiveDate)
	{
		Plan plan = new Plan();
		
		try {
			PlanRequest planRequest= new PlanRequest();
			planRequest.setHiosPlanId(hiosPlanId);
			planRequest.setEffectiveDate(effectiveDate);
			String planDataResponse = ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_PLAN_INFO_BY_HIOS_PLAN_ID, planRequest, String.class);
			PlanResponse planResponse = platformGson.fromJson(planDataResponse, PlanResponse.class);
			if(planResponse!=null && GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(planResponse.getStatus())){
				plan.setId(planResponse.getPlanId());
				plan.setName(planResponse.getPlanName());
				plan.setInsuranceType(planResponse.getInsuranceType());
				if(planResponse.getPlanLevel()!=null){
					if("HEALTH".equalsIgnoreCase(planResponse.getInsuranceType()))
					{
						PlanHealth planHealth = new PlanHealth();
						planHealth.setPlanLevel(planResponse.getPlanLevel());
						plan.setPlanHealth(planHealth);
					}
					else if("DENTAL".equalsIgnoreCase(planResponse.getInsuranceType()))
					{
						PlanDental planDental = new PlanDental();
						planDental.setPlanLevel(planResponse.getPlanLevel());
						plan.setPlanDental(planDental);
					}
				}				
			}			
		}
		catch(Exception e) {			
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		
		return plan;
	}

	/**
	 * Method is used to invoke get Drug Data by RXCUI-List API.
	 */
	public DrugDataResponseDTO getDrugDataByRxcuiSearchList(DrugDataRequestDTO requestDTO) throws GIRuntimeException {

		DrugDataResponseDTO responseDTO = null;

		try {

			String response = ghixRestTemplate.postForObject(PlandisplayEndpoints.GET_DRUG_DATA_BY_RXCUI_SEARCH_LIST, requestDTO, String.class);

			if (response != null) {
				return platformGson.fromJson(response, DrugDataResponseDTO.class);
			} 
		}
		catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, "Exception Occured in PlanRestClient.getDrugDataByRxcuiSearchList ", e, false);
			throw new GIRuntimeException(PlanDisplayConstants.MODULE_NAME + PlanDisplayConstants.FAILED_TO_EXECUTE + PlandisplayEndpoints.GET_DRUG_DATA_BY_RXCUI_SEARCH_LIST, e, e.getMessage(), Severity.HIGH);
		}
		return responseDTO;
	}
	
	public String getSubscriberZip(PdSession pdSession){
		PdQuoteRequest pdQuoteRequest = new PdQuoteRequest();		
		pdQuoteRequest.setPdHouseholdDTO(pdSession.getPdHouseholdDTO());
		pdQuoteRequest.setPdPersonDTOList(pdSession.getPdPersonDTOList());		
		pdQuoteRequest.setInsuranceType(pdSession.getInsuranceType());		
		pdQuoteRequest.setIssuerId(pdSession.getIssuerId());
		if(EnrollmentType.S.equals(pdSession.getPdHouseholdDTO().getEnrollmentType())){
			pdQuoteRequest.setSpecialEnrollmentFlowType(pdSession.getSpecialEnrollmentFlowType());
			if(InsuranceType.HEALTH.equals(pdSession.getInsuranceType())){
				pdQuoteRequest.setInitialSubscriberId(pdSession.getInitialHealthSubscriberId());
			}else{
				pdQuoteRequest.setInitialSubscriberId(pdSession.getInitialDentalSubscriberId());
			}			
		}
		
		String postParams = null;
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PdQuoteRequest.class);
			postParams = writer.writeValueAsString(pdQuoteRequest);
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		
		String jsonString = getPOSTData(GhixEndPoints.PLANDISPLAY_URL + "plandisplay/getSubscriberZip", postParams);
		PdResponse pdResponse = getJsonAsPDResponse(jsonString);
		if(pdResponse != null && pdResponse.getResponse() !=null){
			return pdResponse.getResponse();
		}else{
			return null;
		}
	}
}
