<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false"%>

<%
Integer pageSizeInt = (Integer)request.getAttribute("pageSize");
String reqURI = (String)request.getAttribute("reqURI");
Integer totalResultCount = (Integer)request.getAttribute("resultSize"); 
if(totalResultCount == null){
	totalResultCount = 0;
}
Integer currentPage = (Integer)request.getAttribute("currentPage");
int noOfPage = 0;
 
if(totalResultCount>0){
	if((totalResultCount%pageSizeInt)>0){
		noOfPage = (totalResultCount/pageSizeInt)+1;
	}
	else{
		noOfPage = (totalResultCount/pageSizeInt);
	}
}

%>



			<!-- start of secondary navbar -->
			
			<!-- end of secondary navbar -->
	
<div class="gutter10">
	<div class="row-fluid">
			<div class="span3" id="sidebar">
			<div class="header">
				<h4><spring:message code="label.employers"/></h4>
			</div>
				
					<div class="well gutter10">
						
						<form class="form-vertical gutter10">
							<h4><spring:message code="label.brkrefineresults"/></h4>
							<div class="control-group">
								<label class="control-label" for="inputEmail"><spring:message code="label.companyName"/></label>
								<div class="controls">
									<input class="input-medium" type="text" id="companyName"
										placeholder="">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="inputPassword"><spring:message code="label.brkcontactname"/></label>
								<div class="controls">
									<input class="input-medium" type="password" id="contactName" placeholder="">
								</div>
							</div>
							<h4><spring:message code="label.brkrequestsent"/></h4>
	                        <div class="input-append date well" id="startDate" data-date="${coverageDate}" data-date-format="mm/dd/yyyy">
	                        	<div class="control-group">
		                        	<label class="control-label" for="inputEmail"><spring:message code="label.brkfrom"/></label>
		                       		<input class="input-small" type="text" <c:if test="${coverageDate !=''}" > value="${coverageDate}" </c:if> name="startDate" id="startDate"  readonly="readonly" >
		                       		<span class="add-on"><i class="icon-calendar"></i></span>
									<div id="startDate_error"  class="help-inline"></div>
								</div>
								<div class="control-group">
		                        	<label class="control-label" for="inputEmail"><spring:message code="label.brkto"/></label>
		                       		 <input class="input-small" type="text" <c:if test="${coverageDate !=''}" > value="${coverageDate}" </c:if> name="startDate" id="startDate"  readonly="readonly" >
		                       		 <span class="add-on"><i class="icon-calendar"></i></span>
									<div id="startDate_error"  class="help-inline"></div>
								</div>
							</div>
							<button type="button" class="btn" title="Go"><spring:message code="label.brkGo"/></button>
						</form>
					</div>
					
				
			</div><!-- end of span3 -->
				
			<div class="span9" id="rightpanel">
				
				<form class="form-vertical gutter10" id="brokeremployers" name="brokeremployers" action="employers" method="POST">
<!-- 					<div class="page-header"> -->
<!-- 						<h1><a name="skip" class="skip">Applications in Progress</a></h1> -->
<!-- 					</div>end of page-header -->
					
				<!-- 	<div>
						<label for="status">Filter By Status : </label>
						<div class="controls">
							<select size="1"  id="status">
								<option value="Pending" <c:if test="${'Pending' == empStatus}"> SELECTED </c:if>>Pending For Request</option>
								<option value="Active" <c:if test="${'Active' == empStatus}"> SELECTED </c:if>>Active</option>
								<option value="InActive" <c:if test="${'InActive' == empStatus}"> SELECTED </c:if>>Inactive</option>
							</select>
						</div> -->	<!-- end of controls-->
				<!-- 	</div> --><!-- end of control-group -->
					
					<input type="hidden" name="prevStatus" id="prevStatus" value="${empStatus}">
					<div id="brokerlist">
						<h4><spring:message code="label.brkpendingrequest"/></h4>
						<input type="hidden" name="id" id="id" value="">
						<c:choose>
							
							<c:when test="${fn:length(approvedbrokerslist) > 0}">
								<table class="table">
									<thead>
										<tr class="header">
											<th scope="col"><spring:message code="label.brkemployername"/></th>
											<th scope="col"><spring:message code="label.brkStatus"/></th>
											<th scope="col"><spring:message code="label.brkassistingfrom"/></th>
											<th scope="col"><spring:message code="label.brkAction"/></th>
										</tr>
									</thead>
									<c:forEach items="${approvedbrokerslist}" var="broker">
										<tr>
										    
											<td><a href="../shop/employer/employercase/${broker.employerId}">${broker.employerName}</a></td> 
											<td></td>
											<td> <fmt:formatDate value="${broker.created}" pattern="MM-dd-yyyy"/></td>
										    <td></td>

										</tr>
									</c:forEach>
								</table>
							</c:when>
							<c:when test="${fn:length(brokerslist) > 0}">								
								<table class="table table-striped" id="empTable">
									<thead>
										<tr>
											<th scope="col"><spring:message code="label.companyName"/></th>
											<th scope="col" class="txt-center"><spring:message code="label.brkcontactname"/></th>
											<th scope="col" class="txt-center"><spring:message code="label.brkrequestsent"/><i class="caret"></i></th>
											<th scope="col" class="txt-center"></th>
										</tr>
									</thead>
									<c:forEach items="${brokerslist}" var="broker">
										<tr>
										    <td ><a name="emplink" href="<c:url value="/shop/employer/viewemployerdetail/${broker.employerId}"/>" id =  "emp${broker.employerId}" class="empdetail">${broker.employerName}</a></td>
											<!--  <td>${broker.employerName}</td>-->
											<td class="txt-center"><spring:message code="label.employer"/></td>
											<td class="txt-center">Oct 21, 2014</td>
											<c:choose>
												<c:when test="${(broker.status == 'Pending')}">
											   		<td class="txt-center">
										   			    <div class="dropdown">
														    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cog"></i><i class="caret"></i></a>
														    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
														   		<li><a class="pull-left" href="<c:url value="/broker/approve/${broker.employerId}?prevStatus=${empStatus}"/>" onclick = "return validate()"><i class="icon-user"></i><spring:message code="label.brkaccept"/></a></li>
														   		<li><a class="pull-left" href="<c:url value="/broker/decline/${broker.employerId}?prevStatus=${empStatus}"/>"><i class="icon-user"></i> <spring:message code="label.brkdecline"/></a></li>
														   		<!--  <li><a class="pull-left" href="#"><i class="icon-user"></i> Send Message</a></li>-->
														    </ul>
											   			</div>
											   		</td>
											   	</c:when>
											   	<c:otherwise>
											   		<td class="txt-center"><a href="<c:url value="/broker/approve/${broker.employerId}?prevStatus=${empStatus}"/>" class="btn btn-primary btn-small"  onclick = "return validate()"><spring:message code="label.brkactivate"/></a></td>
											   	</c:otherwise>
										   	</c:choose>											
										</tr>
									</c:forEach>
								</table>
							</c:when>
							<c:when test="${fn:length(brokerslist) > 0 || fn:length(approvedbrokerslist) > 0}">		
								<div class="pagination">
									<ul>
										<% if(noOfPage>1) { %>
											<% if(noOfPage>1 && currentPage>1) { %>
												<li><a href="<%=reqURI%>?usesession=yes&pageNumber=<%=(currentPage-1)%>"><spring:message code="label.brkprev"/></a></li>
											<% }
											for(int iCount=1;iCount<=noOfPage;iCount++) { %>
												<li <% if (currentPage == iCount){ %> class="active" <%} %>><a href="<%=reqURI%>?usesession=yes&pageNumber=<%=iCount%>"><%=iCount%></a></li>
											<% }
											if(noOfPage>1 && currentPage<noOfPage){ %> 
												<li><a href="<%=reqURI%>?usesession=yes&pageNumber=<%=(currentPage+1)%>"><spring:message code="label.brkNext"/></a></li>
											<% } %>
										<% } %>
									</ul>
								</div>
							</c:when>
							<c:otherwise>
								<hr />
								<c:choose>
									<c:when test="$(\"#status option:selected\").val() == 'Pending'">
										<div class="alert alert-info"><spring:message code="label.brkapprovalalert"/></div>
									</c:when>
									<c:otherwise>
										<div class="alert alert-info"><spring:message code="label.brknomatching"/></div>
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>

					</div>
					
					<div class="pagination offset3">
		              <ul>
		                <li><a href="#">�</a></li>
		                <li><a href="#">1</a></li>
		                <li><a href="#">2</a></li>
		                <li><a href="#">3</a></li>
		                <li><a href="#">4</a></li>
		                <li><a href="#">5</a></li>
		                <li><a href="#">�</a></li>
		              </ul>
	            	</div>				
	            </form>

			</div>
		</div>
				
	</div> <!-- end of .row-fluid -->
	
	
	
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker.js" />"></script>


<script type="text/javascript">
	$(document).ready(function(){
		
		$('#startDate').datepicker();
	
	});
</script>		


			<script>
				$(function() {
					$(".empdetail").click(function(e){
				        e.preventDefault();
				        var href = $(this).attr('href');
						
				        if (href.indexOf('#') != 0) {
				           $('<div class="modal"><div class="modal-header" style="border-bottom:0;"><button type="button" title="x" class="close" data-dismiss="modal" aria-hidden="true">�</button></div><div class="modal-body"><iframe src="' + href + '" style="overflow:hidden;width:100%;border:0;margin:0;padding:0;height:400px;"></iframe></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Close</button></div></div>').modal({backdrop:false});
						}
					});
				});		
				$("#status").change(function() {
					var optValue = $("#status option:selected").val();
					  $.ajax({
						url: "members",
					    data: {empStatus: optValue},
					    success: function(data){
					    	document.getElementById("brokerlist").innerHTML = $(data).find("#brokerlist").html();
					    	location.reload();
					    }    
					  });
					});
			    function validate()
			    {
			      var confirmMessage="Are you sure to approve.";

			      if(confirm(confirmMessage)==false)
			      {
			          return false;
			      }
			    }

			</script>
		
