package com.getinsured.hix.reports.enrollment.service.jpa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.getinsured.hix.reports.enrollment.dto.CMSErrorReportDTO;
import com.getinsured.hix.reports.enrollment.dto.CMSFileTransmissionLogDTO;
import com.getinsured.hix.reports.enrollment.dto.CMSXmlRequestDTO;
import com.getinsured.hix.reports.enrollment.dto.CMSXmlSumryReportDTO;
import com.getinsured.hix.reports.enrollment.dto.MonthlyIRSErrorReportDTO;
import com.getinsured.hix.reports.enrollment.service.CMSService;

@Service("cmsService")
public class CMSServiceImpl implements CMSService {

	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	private HashMap<String,String> errorReportDTOTOEntity = null;
	private HashMap<String,String> summaryReportDTOTOEntity = null;
	private HashMap<String, String> cmsXmlFileSummaryEntityMap = null;
	private HashMap<String, String> monthlyIRSXmlErrorReportMap = null;
	private static final Logger LOGGER = Logger.getLogger(CMSServiceImpl.class);
	private static final String SORT_ORDER_ASC = "ASC";
	private static final String SORT_ORDER_DESC = "DESC";
	
	@PostConstruct
	void updateDTOToEntityMaps() {
				
		errorReportDTOTOEntity = new HashMap<String, String>(11);
		errorReportDTOTOEntity.put("reportMonth", "o.month");
		errorReportDTOTOEntity.put("reportYear", "o.year");
		errorReportDTOTOEntity.put("fileName", "o.fileName");
		errorReportDTOTOEntity.put("hoisIssuerId", "o.hiosIssuerId");
		errorReportDTOTOEntity.put("fileStatus", "i.fileStatus");
		errorReportDTOTOEntity.put("nextAction", "o.inboundAction");
		errorReportDTOTOEntity.put("exchangeAssignedPolicyID", "d.exchAssignPolicyId");
		errorReportDTOTOEntity.put("cmsErrorCode", "d.errorCode");
		errorReportDTOTOEntity.put("fieldNameAssociatedwithError", "d.elementInError");
		errorReportDTOTOEntity.put("additionalErrorInfo", "d.additionalErrorInfo");
		errorReportDTOTOEntity.put("subscriberID", "d.issuerAssignSubsriberId");
		errorReportDTOTOEntity.put("creationtime", "d.createdOn");
		
		summaryReportDTOTOEntity = new HashMap<String, String>(10);
		summaryReportDTOTOEntity.put("reportMonth", "o.month");
		summaryReportDTOTOEntity.put("reportYear", "o.year");
		summaryReportDTOTOEntity.put("fileName", "o.OUTBOUND_FILE_NAME");
		summaryReportDTOTOEntity.put("status", "o.INBOUND_STATUS");
		summaryReportDTOTOEntity.put("nextAction", "o.INBOUND_ACTION");
		summaryReportDTOTOEntity.put("skipRecords", "o.SKIP_RECORDS");
		summaryReportDTOTOEntity.put("sbmsSbmrRcvd", "o.SBMS_SBMR_RCVD");
		summaryReportDTOTOEntity.put("hoisIssuerId", "o.HIOS_ISSUER_ID");
		summaryReportDTOTOEntity.put("creationTimeStamp", "o.CREATION_TIMESTAMP");
		summaryReportDTOTOEntity.put("missingPolicyIds", "i.MISSING_POLICY_IDS");
		
		cmsXmlFileSummaryEntityMap = new HashMap<String, String>(7);
		cmsXmlFileSummaryEntityMap.put("fileName", "file_name");
		cmsXmlFileSummaryEntityMap.put("fileSize", "file_size");
		cmsXmlFileSummaryEntityMap.put("reportType", "report_type");
		cmsXmlFileSummaryEntityMap.put("direction", "direction");
		cmsXmlFileSummaryEntityMap.put("batchExecutionId", "batch_execution_id");
		cmsXmlFileSummaryEntityMap.put("status", "status");
		cmsXmlFileSummaryEntityMap.put("creationTimestamp", "creation_timestamp");
		cmsXmlFileSummaryEntityMap.put("errorDescription", "ERROR_DESC");
		
		monthlyIRSXmlErrorReportMap = new HashMap<String, String>(5);
		monthlyIRSXmlErrorReportMap.put("householdCaseId", "irsInDtl.household_case_id");
		monthlyIRSXmlErrorReportMap.put("errorCode", "irsInDtl.error_code");
		monthlyIRSXmlErrorReportMap.put("errorFieldName", "irsInDtl.err_field_name");
		monthlyIRSXmlErrorReportMap.put("xpathContent", "irsInDtl.xpath_content");
		monthlyIRSXmlErrorReportMap.put("batchId", "irsIn.batch_id");
		monthlyIRSXmlErrorReportMap.put("errorMessage", "irsInDtl.ERROR_MESSAGE");
		monthlyIRSXmlErrorReportMap.put("month","TO_CHAR(irsInDtl.CREATION_TIMESTAMP, 'MM')");
		monthlyIRSXmlErrorReportMap.put("year","TO_CHAR(irsInDtl.CREATION_TIMESTAMP, 'YYYY')");	
	}
		
	@SuppressWarnings("unchecked")
	@Override
	public Page<CMSXmlSumryReportDTO> findCmsXmlSumryReportRecords(int page, int size, CMSXmlRequestDTO cmsXmlRequestDTO) {
		
		Page<CMSXmlSumryReportDTO> resultList = null;
		EntityManager em = null;
    	Query query = null;
		int resultCount = 0;
		
    	try {
			em = entityManagerFactory.createEntityManager();

			StringBuilder queryStr = new StringBuilder("SELECT o.CREATION_TIMESTAMP, o.MONTH, o.YEAR, o.coverage_year, o.HIOS_ISSUER_ID, o.OUTBOUND_FILE_NAME, "
					+ " o.ISSUER_FILE_SET_ID, o.OUT_ENROLLMENT_COUNT, o.SKIP_RECORDS, o.INBOUND_STATUS, o.INBOUND_ACTION, o.SBMS_SBMR_RCVD,"
					+ " o.ACCEPTED_COUNT, o.ACCEPTED_ERROR_WARN_COUNT, i.MISSING_POLICY_IDS, i.TOTAL_RECORDS_PROCESSED, i.TOTAL_RECORDS_REJECTED,"
					+ " i.TOTAL_POLICYRECORDS_APPROVED");
			String fromQueryStr = " FROM  ENRL_CMS_OUT o left outer join ENRL_CMS_IN i on o.outbound_file_name = I.outbound_file_name"; 
			String initialWhereClauseQueryString = " WHERE o.INBOUND_ACTION != 'INACTIVE' ";
			String whereClause = createWhereClauseQueryString(initialWhereClauseQueryString, summaryReportDTOTOEntity, cmsXmlRequestDTO);
			
			
			//Record count query
			String queryForCount = "SELECT count(*) " + fromQueryStr + whereClause;
		   	Object objCount = em.createNativeQuery(queryForCount).getSingleResult();
			if (objCount != null) {
				resultCount = Integer.parseInt(objCount.toString());
			}
			String intialOrderByQueryString = " o.CREATION_TIMESTAMP DESC ";
			
			//Actual record query
			query = em.createNativeQuery(queryStr + fromQueryStr + whereClause + createOrderByQueryString(intialOrderByQueryString, summaryReportDTOTOEntity, cmsXmlRequestDTO));
			
			query.setFirstResult(page * size);
			query.setMaxResults(size);
			
			List<CMSXmlSumryReportDTO> resultCMSXmlSumryReportDTO = converttoCMSXmlSumryReportDTO(query.getResultList());
			resultList = new PageImpl<CMSXmlSumryReportDTO>(resultCMSXmlSumryReportDTO, new PageRequest(page, size), resultCount);		
			
		} catch (Exception exception) {
			throw exception;
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
    	
    	return resultList;
    }

	/**
	 * Populates CMSXmlSumryReportDTO with result obtained
	 * @param result
	 * @return
	 */
	private List<CMSXmlSumryReportDTO> converttoCMSXmlSumryReportDTO(List<Object[]> result) {

		List<CMSXmlSumryReportDTO> cmsXmlSumryReportDTOs = new ArrayList<CMSXmlSumryReportDTO>();
		if(result != null && !result.isEmpty()) {
			for(Object[] columns : result) {
				CMSXmlSumryReportDTO cmsXmlSumryReportDTO = new CMSXmlSumryReportDTO();
				cmsXmlSumryReportDTO.setCreationTimeStamp(columns[0] != null ? (Date)columns[0] :  null);
				cmsXmlSumryReportDTO.setReportMonth(columns[1] != null ? ((BigDecimal)columns[1]).intValue() :  null);
				cmsXmlSumryReportDTO.setReportYear(columns[2] != null ? ((BigDecimal)columns[2]).intValue() :  null);
				cmsXmlSumryReportDTO.setFileCoverageYear(columns[3] != null ? ((BigDecimal)columns[3]).intValue() :  null);
				cmsXmlSumryReportDTO.setHoisIssuerId(columns[4] != null ? columns[4].toString() :  null);
				cmsXmlSumryReportDTO.setFileName(columns[5] != null ? columns[5].toString() :  null);
				cmsXmlSumryReportDTO.setIssuerFileSetId(columns[6] != null ? ((BigDecimal)columns[6]).longValue() :  null);
				cmsXmlSumryReportDTO.setPolicyCount(columns[7] != null ? ((BigDecimal)columns[7]).intValue() :  null);
				cmsXmlSumryReportDTO.setSkipRecords(columns[8] != null ? columns[8].toString() :  null);
				cmsXmlSumryReportDTO.setStatus(columns[9] != null ? columns[9].toString() :  null);
				cmsXmlSumryReportDTO.setNextAction(columns[10] != null ? columns[10].toString() :  null);
				cmsXmlSumryReportDTO.setSbmsSbmrRcvd(columns[11] != null ? columns[11].toString() :  null);
				cmsXmlSumryReportDTO.setAcceptedCount(columns[12] != null ? ((BigDecimal)columns[12]).intValue() :  null);
				cmsXmlSumryReportDTO.setAcceptedErrorWarnCount(columns[13] != null ? ((BigDecimal)columns[13]).intValue() :  null);
				cmsXmlSumryReportDTO.setMissingPolicyIds(columns[14] != null ? columns[14].toString() :  null);
				cmsXmlSumryReportDTO.setTotalRecordsProcessed(columns[15] != null ? ((BigDecimal)columns[15]).intValue() :  null);
				cmsXmlSumryReportDTO.setTotalRecordsRejected(columns[16] != null ? ((BigDecimal)columns[16]).intValue() :  null);				
				cmsXmlSumryReportDTO.setTotalPolicyrecordsApproved(columns[17] != null ? ((BigDecimal)columns[17]).intValue() :  null);
				cmsXmlSumryReportDTOs.add(cmsXmlSumryReportDTO);
			}
		}
	
		return cmsXmlSumryReportDTOs;
	}
	
	@Override
	public Page<CMSErrorReportDTO> getCMSErrorReport(int page, int size, CMSXmlRequestDTO cmsXmlRequestDTO) {
		EntityManager em = null;
    	Query query = null;
		int resultCount = 0;
    	Page<CMSErrorReportDTO> resultList = null;
    	
		try {
			em = entityManagerFactory.createEntityManager();
			
			StringBuilder queryStr = new StringBuilder("select new com.getinsured.hix.reports.enrollment.dto.CMSErrorReportDTO(o.month, o.year, o.fileName,"
					+ " o.inboundAction, o.hiosIssuerId, i.fileStatus, d.exchAssignPolicyId, d.issuerAssignSubsriberId,	d.errorCode, d.errorDesc,"
					+ " d.elementInError,	d.additionalErrorInfo, d.createdOn )"); 
			String initialWhereClauseQueryString = " FROM EnrollmentCmsOut o, EnrollmentCmsIn i join i.enrollmentCmsInDtlList d "
					+ " WHERE o.fileName = i.outFileName AND o.inboundAction != 'INACTIVE' ";
			String whereClauseQueryString = createWhereClauseQueryString(initialWhereClauseQueryString, errorReportDTOTOEntity, cmsXmlRequestDTO);
			
			String queryForCount = "SELECT count(*) " + whereClauseQueryString;
		   	Object objCount = em.createQuery(queryForCount.toString()).getSingleResult();
			if (objCount != null) {
				resultCount = Integer.parseInt(objCount.toString());
			}
			String defaultOrderByQueryString = " d.createdOn DESC ";
			
			queryStr.append(whereClauseQueryString);
			queryStr.append(createOrderByQueryString(defaultOrderByQueryString, errorReportDTOTOEntity, cmsXmlRequestDTO));
	
			query = em.createQuery(queryStr.toString());
				
			Integer start = (page)*size;
			query.setFirstResult(start);
			query.setMaxResults(size);
			
			@SuppressWarnings("unchecked")
			List<CMSErrorReportDTO> result = query.getResultList();
	
			resultList = new PageImpl<CMSErrorReportDTO>(result, new PageRequest(page, size) , resultCount);	
		
		} catch (Exception exception) {
			throw exception;
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
    	
		return resultList;
	}
	
	@Override
	public Page<CMSFileTransmissionLogDTO> getCMSFileTransmissionLogData(final String pageNumber, final String pageSize,
			final CMSXmlRequestDTO cmsXmlRequestDTO) {
		Page<CMSFileTransmissionLogDTO> cmsFileTransmissionLogDTOPageData = null;
		EntityManager entityManager = null;
		try{
			int startRecord = 0, pageSizeLocal = 1, pageNumberLocal = 1;
			Long totalResultCount = 0l;
			StringBuilder selectSqlQueryString = new StringBuilder("SELECT id, file_name, file_size, report_type, direction, batch_execution_id, status, error_desc, creation_timestamp ");
			String countSqlQueryString = "SELECT COUNT(*) ";
			String whereClauseSqlQueryString = createWhereClauseQueryString(" FROM ENRL_CMS_FILE_TRANSFER_LOG WHERE direction IS NOT NULL ", cmsXmlFileSummaryEntityMap, cmsXmlRequestDTO);
			
			String initialOrderByQueryString = " creation_timestamp DESC ";
			entityManager = entityManagerFactory.createEntityManager();
			List<CMSFileTransmissionLogDTO> cmsFileTransmissionLogDTOList = new ArrayList<CMSFileTransmissionLogDTO>();
			selectSqlQueryString.append(whereClauseSqlQueryString);
			selectSqlQueryString.append(createOrderByQueryString(initialOrderByQueryString, cmsXmlFileSummaryEntityMap, cmsXmlRequestDTO));
			
			Query selectSqlQuery = entityManager.createNativeQuery(selectSqlQueryString.toString());
			Query countSqlQuery = entityManager.createNativeQuery(countSqlQueryString + whereClauseSqlQueryString);
			
			
			if(StringUtils.isNotBlank(pageSize)){
				pageSizeLocal = Integer.parseInt(pageSize);
				selectSqlQuery.setMaxResults(pageSizeLocal);
			}
			
			if(StringUtils.isNotBlank(pageNumber)){
				pageNumberLocal = Integer.parseInt(pageNumber);
			}
			startRecord =  pageNumberLocal != 0 ? (pageNumberLocal * pageSizeLocal) - pageSizeLocal : 0;
			selectSqlQuery.setFirstResult(startRecord);
			
			
			
			List<?> objectList = selectSqlQuery.getResultList();
			totalResultCount = ((Number)countSqlQuery.getSingleResult()).longValue();
			if(objectList != null && !objectList.isEmpty()){
				
				Iterator<?> rsIterator = objectList.iterator();
				Object[] cmsFileTransmissionLogObj = null;
				while (rsIterator.hasNext()) {
					cmsFileTransmissionLogObj = (Object[]) rsIterator.next();
					
					if(cmsFileTransmissionLogObj != null){
					CMSFileTransmissionLogDTO cmsFileTransmissionLogDTO = new CMSFileTransmissionLogDTO();
					cmsFileTransmissionLogDTO.setId(cmsFileTransmissionLogObj[0] != null ? ((Number)cmsFileTransmissionLogObj[0]).intValue(): null);
					cmsFileTransmissionLogDTO.setFileName((String) cmsFileTransmissionLogObj[1]);
					cmsFileTransmissionLogDTO.setFileSize(cmsFileTransmissionLogObj[2] != null ? ((Number) cmsFileTransmissionLogObj[2]).longValue(): null);
					cmsFileTransmissionLogDTO.setReportType((String) cmsFileTransmissionLogObj[3]);
					cmsFileTransmissionLogDTO.setDirection((String) cmsFileTransmissionLogObj[4]);
					cmsFileTransmissionLogDTO.setBatchExecutionId(cmsFileTransmissionLogObj[5] != null ? ((Number) cmsFileTransmissionLogObj[5]).longValue() : null);
					cmsFileTransmissionLogDTO.setStatus((String) cmsFileTransmissionLogObj[6]);
					cmsFileTransmissionLogDTO.setErrorDescription((String) cmsFileTransmissionLogObj[7]);
					cmsFileTransmissionLogDTO.setCreationTimestamp((Date) cmsFileTransmissionLogObj[8]);
					
					cmsFileTransmissionLogDTOList.add(cmsFileTransmissionLogDTO);
					}
				}
			}
			cmsFileTransmissionLogDTOPageData = new PageImpl<CMSFileTransmissionLogDTO>(cmsFileTransmissionLogDTOList, new PageRequest(pageNumberLocal, pageSizeLocal), totalResultCount);
		}
		catch(Exception ex){
			LOGGER.error("CMS_FILE_TRANSMISSION_REPORT:: Exception caught while processing request ", ex);
		}
		finally{
			if(entityManager != null && entityManager.isOpen()){
				try{
					entityManager.clear();
					entityManager.close();
				}
				catch(IllegalStateException ex){
					//Suppressing exception
					LOGGER.error("CMS_FILE_TRANSMISSION_REPORT:: Exception caught while closing connection ", ex);
				}
			}
		}
		return cmsFileTransmissionLogDTOPageData;
	}

	@Override
	public Page<MonthlyIRSErrorReportDTO> getMonthlyIRSErrorReportData(final String pageNumber, final String pageSize,
			final CMSXmlRequestDTO cmsXmlRequestDTO) {
		int startRecord = 0, pageSizeLocal = 1, pageNumberLocal = 1;
		Page<MonthlyIRSErrorReportDTO> monthlyIRSErrorReportDTOData = null;
		Long totalResultCount = 0l;
		EntityManager entityManager = null;
		try{
			entityManager = entityManagerFactory.createEntityManager();
			StringBuilder selectSqlQueryString = new StringBuilder("SELECT irsInDtl.household_case_id, irsInDtl.error_code, irsInDtl.error_message, irsInDtl.err_field_name," 
					+ " irsInDtl.xpath_content, irsIn.batch_id, irsInDtl.creation_timestamp");
			String whereClauseSqlQueryString = createWhereClauseQueryString(" FROM ENRL_IRS_MONTHLY_IN_DTL irsInDtl, ENRL_IRS_MONTHLY_IN irsIn "
					+ " WHERE irsIn.id = irsInDtl.enrl_irs_monthly_in_id ", monthlyIRSXmlErrorReportMap, cmsXmlRequestDTO);
			String intialOrderByQueryString = " irsInDtl.creation_timestamp DESC";
			String countSqlQueryString = "SELECT COUNT(*) ";
			List<MonthlyIRSErrorReportDTO> monthlyIRSErrorReportDTOList = new ArrayList<MonthlyIRSErrorReportDTO>();
			selectSqlQueryString.append(whereClauseSqlQueryString);
			selectSqlQueryString.append(createOrderByQueryString(intialOrderByQueryString, monthlyIRSXmlErrorReportMap, cmsXmlRequestDTO));
			Query selectQuery = entityManager.createNativeQuery(selectSqlQueryString.toString());
			Query countQuery = entityManager.createNativeQuery(countSqlQueryString + whereClauseSqlQueryString);
			
			if(StringUtils.isNotBlank(pageSize)){
				pageSizeLocal = Integer.parseInt(pageSize);
				selectQuery.setMaxResults(pageSizeLocal);
			}
			
			if(StringUtils.isNotBlank(pageNumber)){
				pageNumberLocal = Integer.parseInt(pageNumber);	
			}
			startRecord =  pageNumberLocal != 0 ? (pageNumberLocal * pageSizeLocal) - pageSizeLocal : 0;
			selectQuery.setFirstResult(startRecord);
			
			List<?> objectList = selectQuery.getResultList();
			totalResultCount = ((Number)countQuery.getSingleResult()).longValue();
			if(objectList != null && !objectList.isEmpty()){
			 Iterator<?> iterator = 	objectList.iterator();
			 Object[] monthlyIRSErrorReportDTOObj = null;
			 while(iterator.hasNext()){
				 monthlyIRSErrorReportDTOObj = (Object[]) iterator.next();
				 if(monthlyIRSErrorReportDTOObj != null){
					 MonthlyIRSErrorReportDTO monthlyIRSErrorReportDTO = new MonthlyIRSErrorReportDTO();
					 monthlyIRSErrorReportDTO.setHouseholdCaseId(monthlyIRSErrorReportDTOObj[0] != null ? ((Number)monthlyIRSErrorReportDTOObj[0]).intValue() : null);
					 monthlyIRSErrorReportDTO.setErrorCode(monthlyIRSErrorReportDTOObj[1] != null ? ((Number) monthlyIRSErrorReportDTOObj[1]).toString() : null);
					 monthlyIRSErrorReportDTO.setErrorMessage((String)monthlyIRSErrorReportDTOObj[2]);
					 monthlyIRSErrorReportDTO.setErrorFieldName((String)monthlyIRSErrorReportDTOObj[3]);
					 monthlyIRSErrorReportDTO.setXpathContent((String)monthlyIRSErrorReportDTOObj[4]);
					 monthlyIRSErrorReportDTO.setBatchId((String)monthlyIRSErrorReportDTOObj[5]);
					 Date creationTimestamp = (Date) monthlyIRSErrorReportDTOObj[6];
					 if(creationTimestamp != null){
						 Calendar creationDateTime = TSCalendar.getInstance();
						 creationDateTime.setTime(creationTimestamp);
						 monthlyIRSErrorReportDTO.setMonth(creationDateTime.get(Calendar.MONTH)); 
						 monthlyIRSErrorReportDTO.setYear(creationDateTime.get(Calendar.YEAR));
					 }
					 monthlyIRSErrorReportDTOList.add(monthlyIRSErrorReportDTO);
					 
				 }
			 }
			}
			monthlyIRSErrorReportDTOData = new PageImpl<MonthlyIRSErrorReportDTO>(monthlyIRSErrorReportDTOList, new PageRequest(pageNumberLocal, pageSizeLocal), totalResultCount);
		}
		catch(Exception ex){
			LOGGER.error("MONTHLY_IRS_ERROR_REPORT:: Exception caught while processing report request ",ex);
		}
		finally{
			if(entityManager != null && entityManager.isOpen()){
				try{
					entityManager.clear();
					entityManager.close();
				}
				catch(IllegalStateException ex){
					//Suppress exception
					LOGGER.error("MONTHLY_IRS_ERROR_REPORT:: Exception caught while closing connection ",ex);
				}
			}
		}
		return monthlyIRSErrorReportDTOData;
	}
	
	/**
	 * 
	 * @param initialWhereClauseQuery
	 * @param entityMap
	 * @param cmsXmlRequestDTO
	 * @return
	 */
	private String createWhereClauseQueryString(final String initialWhereClauseQuery, final Map<String, String> entityMap, final CMSXmlRequestDTO cmsXmlRequestDTO){
		StringBuilder whereClauseQueryString = new StringBuilder(initialWhereClauseQuery);
		if(cmsXmlRequestDTO != null) {
			if(cmsXmlRequestDTO.getFilterExact() != null && !cmsXmlRequestDTO.getFilterExact().isEmpty()) {
				for(String column : cmsXmlRequestDTO.getFilterExact().keySet()) {
					whereClauseQueryString.append(" AND " + (entityMap.get(column) != null ? entityMap.get(column) : column) + " = '" + cmsXmlRequestDTO.getFilterExact().get(column) + "'");
				}
			}
			if(cmsXmlRequestDTO.getFilterLike() != null && !cmsXmlRequestDTO.getFilterLike().isEmpty()) {
				for(String column : cmsXmlRequestDTO.getFilterLike().keySet()) {
					whereClauseQueryString.append(" AND " + (entityMap.get(column) != null ? entityMap.get(column) : column) + " like '%" + cmsXmlRequestDTO.getFilterLike().get(column) + "%'");
				}
			}
		}
		return whereClauseQueryString.toString();
	}
	
	/**
	 * 
	 * @param initalOrderByQueryString
	 * @param entityMap
	 * @param cmsXmlRequestDTO
	 * @return
	 */
	private String createOrderByQueryString(final String defaultOrderByQueryString, final Map<String, String> entityMap, final CMSXmlRequestDTO cmsXmlRequestDTO){
		StringBuilder orderByString = new StringBuilder(" ORDER BY ");
		boolean isSortRequestPresent = false;
		if(cmsXmlRequestDTO != null) {
			if(cmsXmlRequestDTO.getAsc() != null && !cmsXmlRequestDTO.getAsc().isEmpty()) {
				isSortRequestPresent = true;
				for(String column : cmsXmlRequestDTO.getAsc()) {
					orderByString.append((entityMap.get(column) != null ? entityMap.get(column) : column) + " " + SORT_ORDER_ASC);
					orderByString.append(",");
				}
			}
			if(cmsXmlRequestDTO.getDesc() != null && !cmsXmlRequestDTO.getDesc().isEmpty()) {
				isSortRequestPresent = true;
				for(String column : cmsXmlRequestDTO.getDesc()) {
					orderByString.append((entityMap.get(column) != null ? entityMap.get(column) : column) + " " + SORT_ORDER_DESC);
					orderByString.append(",");
				}
			}
		}
		if(!isSortRequestPresent){
			orderByString.append(defaultOrderByQueryString);
		}
		else{
			orderByString.deleteCharAt(orderByString.length() -1);
		}
		return orderByString.toString();
	}
}
