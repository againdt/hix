import { DataKeepService } from './../../services/data-keep.service';
import { Component, OnInit } from '@angular/core';
import { PersonalizedState } from '../../models/data.model';
@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
    errorMsg: string;
    personalization: PersonalizedState;
    constructor(
        private datakeep: DataKeepService
    ) { }

    ngOnInit() {

        this.datakeep.whiteScreenMsg$.subscribe(msg => {
           // console.log(msg);
            this.errorMsg = msg;
            this.personalization = this.datakeep.personalization;
            //console.log("preloaded data:");
            //console.log(this.personalization);
        });
    }

    onZipcodeChange(zipcode: string) {
        this.personalization.doctor.nearZipcode = zipcode;
    }

}
