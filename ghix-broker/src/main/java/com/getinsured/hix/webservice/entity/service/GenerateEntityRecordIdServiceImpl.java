package com.getinsured.hix.webservice.entity.service;

import java.sql.Timestamp;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.agency.repository.IAgencyRepository;
import com.getinsured.hix.agency.service.AgencyService;
import com.getinsured.hix.agency.service.AgencySiteService;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.util.BrokerConstants;
import com.getinsured.hix.broker.util.BrokerUtils;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.agency.Agency;
import com.getinsured.hix.model.agency.AgencySite;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdRequest;
import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdResponse;
import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdResponse.Fault;
import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdResponse.Fault.Detail;
import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdResponse.Fault.Faultstring;

@Service
public class GenerateEntityRecordIdServiceImpl implements GenerateEntityRecordIdService{
	
	public static final Logger LOGGER = LoggerFactory.getLogger(GenerateEntityRecordIdServiceImpl.class);
	
	@Autowired
	private AgencyService agencyService;
	@Autowired
	private BrokerService brokerService;
	@Autowired
	private AgencySiteService agencySiteService;
	@Autowired
	private IAgencyRepository iAgencyRepository;
	@Autowired
	private ZipCodeService zipCodeService;
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public GenerateEntityRecordIdResponse createAgencyAndAgentRecords(GenerateEntityRecordIdRequest request, GenerateEntityRecordIdResponse response) throws Exception{
		String businessLegalName = request.getBusinessLegalName();
		String federalEIN = request.getFederalEIN();
		
		try {
			//Agency Record Creation
			Agency agency = createAgencyRecord(federalEIN, businessLegalName);
			
			if(agency!=null){
				//Agency Primary Site and Location Record Creation
				createAgencySite(agency, request.getAddressLine1(), request.getAddressLine2(), request.getCity(), request.getState(), request.getZipCode());
				
				//Agency Manager/Agent Record Creation
				int recordId = generateBrokerRecordId(request.getAgentLicenseNo(), federalEIN, businessLegalName,
						request.getEmail(), request.getStateEIN(), agency.getId());
				setSuccessResponse(recordId, response);
			} else {
				setFailureResponse(BrokerConstants.RESPONSECODEAGENCYRECORDCREATIONFAILED, BrokerConstants.RESPONSEDESCAGENCYGENERATIONERROR, response);
			}
		} catch (Exception exception) {
			throw new GIRuntimeException(exception);
		}
		
		return response;
	}
	
	@Override
	public int generateBrokerRecordId(String licenseNo, String federalEIN, String businessLegalName, String email, String stateEIN, Long agencyId) {
		Broker broker = new Broker();
		broker.setLicenseNumber(licenseNo);
		broker.setCreated(new TSDate());
		broker.setUpdated(new TSDate());
		if (federalEIN != null) {
			broker.setFederalEIN(federalEIN);
		}
		if (stateEIN != null) {
			broker.setStateEIN(stateEIN);
		}
		if (!BrokerUtils.isEmpty(businessLegalName)) {
			broker.setCompanyName(businessLegalName);
		}
		
		if(agencyId!=null){
			broker.setAgencyId(agencyId);
		}

		return brokerService.generateBrokerRecordId(broker);
	}
	
	private Agency createAgencyRecord(String federalEIN, String businessLegalName) {
		Agency agency = new Agency();
		agency.setCreationTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));
		
		if (federalEIN != null) {
			agency.setFederalTaxId(federalEIN);
		}

		if (!BrokerUtils.isEmpty(businessLegalName)) {
			agency.setBusinessLegalName(businessLegalName);
		}

		return agencyService.saveOrUpdate(agency);
	}
	
	private AgencySite createAgencySite(Agency agency, String address1, String address2, String city, String state, String zipcode) {
		Location location = new Location();
		location.setAddress1(address1);
		location.setAddress2(address2);
		location.setCity(city);
		location.setState(state);
		location.setZip(zipcode);
		
		ZipCode zipCode=zipCodeService.findByZipCode(zipcode); 
		if(zipCode!=null){
			location.setLat(zipCode.getLat());
			location.setLon(zipCode.getLon());
		}
		
		AgencySite agencySite = new AgencySite();
		agencySite.setSiteType(AgencySite.SiteType.PRIMARY.toString());
		agencySite.setAgency(agency);
		agencySite.setLocation(location);
		agencySite.setCreationTimestamp(new Timestamp(TimeShifterUtil.currentTimeMillis()));

		return agencySiteService.saveOrUpdate(agencySite);
	}
	
	@Override
	public boolean checkAgencyFederalTaxId(String federalTaxId){
		int count = iAgencyRepository.findByFederalTaxId(federalTaxId);
		if(count==0){
			return true;
		} else {
			return false;
		}
	}
	
	private GenerateEntityRecordIdResponse setSuccessResponse(int recordId, GenerateEntityRecordIdResponse response) {
		response.setRecordID(recordId);
		response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
		response.setResponseDescription(BrokerConstants.RESPONSEDESCSUCCESS);
		return response;
	}
	
	private GenerateEntityRecordIdResponse setFailureResponse(int faultCode, String str, GenerateEntityRecordIdResponse response) {

		Fault fault = new Fault();
		Faultstring faultString = new Faultstring();
		fault.setFaultcode(String.valueOf(faultCode));

		faultString.setValue(str);
		fault.setFaultstring(faultString);

		response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);
		response.setResponseDescription(BrokerConstants.RESPONSEDESCFAILURE);
		response.setFault(fault);

		LOGGER.info("Fault Code : " + fault.getFaultcode() + " : Fault String : " + fault.getFaultstring().getValue());

		Detail detail = new Detail();
		List<String> list = new ArrayList<String>();
		list.add(str);
		detail.setValidationError(list);
		fault.setDetail(detail);
		return response;
	}
}
