<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<div class="row-fluid">
    <div class="gutter10">
    	<ul class="page-breadcrumb">
        	<li><a href="<c:url value="/planmgmt/issuer/application"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
            <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.newIssuerApplication"/></a></li>
            <li><spring:message  code="pgheader.step"/> 3</li>
        </ul><!--page-breadcrumb ends-->
		<h1><issuerLogo:view issuerName="" /><spring:message  code="pgheader.newIssuerApplication"/></h1>
    </div>
</div>
		<div class="row-fluid">
        	<div class="gutter10">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.steps"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ol class="nav nav-list">
                    <li><a href="#"><span class="completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.addRepresentative"/></a></li>
                    <li><a href="#"><span class="completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.issuerApplication"/></a></li>
                    <li class="active"><a href="#"><spring:message  code="pgheader.financialInformation"/></a></li>
                    <li><a href="#"><spring:message  code="pgheader.updateProfile"/></a></li>
                    <li><a href="#"><spring:message  code="pgheader.signApplication"/></a></li>
                </ol>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="span10"><spring:message  code="pgheader.step"/> 3: <spring:message  code="pgheader.financialInformation"/></h4>
                </div>
				<div class="gutter10">
					<p><spring:message code="label.bankInformation"/></p>
			
				<form class="form-horizontal" id="frmIssuerFinInfo" name="frmIssuerFinInfo" action="<c:url value="/planmgmt/issuer/financial/info/save" />" method="POST">
                    <df:csrfToken/>
                    <input type="hidden" id="redirectTo" name="redirectTo" value="${redirectUrl}"/>
                    <input type="hidden" id="paymentId" name="paymentId" value=""/>
                    
                    <div class="control-group">
						<label class="control-label" for="bank-acc-id"><spring:message  code="label.accountName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="bankAccountName" id="bankAccountName" class="input-large" />
                            <div id="bankAccountName_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="bank-acc-id"><spring:message  code="label.bankAcctType"/></label>
						<div class="controls">
							<select class="input-small" name="bankAccountType" id="bankAccountType">
                            	<option value="S"><spring:message  code="label.bankAcctTypeSaving"/></option>
                                <option value="C"><spring:message  code="label.bankAcctTypeChecking"/></option>
                            </select>
                       </div>
					</div><!-- end of control-group-->
					
                    <div class="control-group">
						<label class="control-label" for="bank-name"><spring:message  code="label.bankName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="bankName" id="bankName" class="input-large" />
                            <div id="bankName_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="bank-address-1"><spring:message  code="label.addressLine1"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="addressLine1" id="addressLine1" class="input-large" />
                            <div id="addressLine1_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="bank-address-2"><spring:message  code="label.addressLine2"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="addressLine2" id="addressLine2" class="input-large" />
                            <div id="addressLine2_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="nank-city"><spring:message  code="label.city"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="city" id="city" class="input-large" />
                            <div id="city_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    
                    <div class="control-group">
						<label class="control-label" for="bank-state"><spring:message  code="label.state"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<select class="input-medium" name="state" id="state">
                            	<c:forEach var="stateObj" items="${stateObj}">
						    		<option value="${stateObj.code}">${stateObj.name}</option>
								</c:forEach>
                            </select>
                            <div id="state_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="nank-ziopcode"><spring:message  code="label.zipCode"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="zipcode" id="zipcode" class="input-small" />
                            <div id="zipcode_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="bank-acc-num"><spring:message  code="label.bankAcctNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> <a class="ttclass" rel="tooltip" href="#" data-original-title="Text Holder 1"><i class="icon-question-sign"></i></a></label>
						<div class="controls">
							<input type="text" name="bankAccountNumber" id="bankAccountNumber" class="input-large" />
                            <div id="bankAccountNumber_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="bank-routing-num"><spring:message  code="label.abaRoutingNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> <a class="ttclass" rel="tooltip" href="#" data-original-title="Text Holder 2"><i class="icon-question-sign"></i></a></label>
						<div class="controls">
							<input type="text" name="bankABARoutingNumber" id="bankABARoutingNumber" class="input-large" />
                            <div id="bankABARoutingNumber_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="form-actions paddingLR20">
                    	<a href='<c:url value="/planmgmt/issuer/application"/>' class="btn"><spring:message  code="label.back"/></a>
                        <button type="button" class="btn btn-primary" id="btnFinancialInfo" onclick="javascript:submitForm();"><spring:message  code="label.next"/></button>
                   	</div>
				</form>
			</div>
		</div><!--  end of span9 -->
        	</div>
		</div><!-- end row-fluid -->

		<c:if test="${isFinancialInfoAdded == 'true'}">
			<!--dialog/modal box-->
			<div id="addbankaccount" class="modal hide fade">
				<div class="modal-header">
					<a class="close" data-dismiss="modal" data-original-title="">x</a>
					<h3 id="myModalLabel"><spring:message  code="pgheader.addMoreBankAccounts"/></h3>
				</div>
				<div class="modal-body">
					<p><spring:message  code="pgheader.doYouToAddMoreBankAccounts"/> </p>
				</div>
				<div class="modal-footer">
					<a href="<c:url value="/planmgmt/issuer/profile/update/company"/>" class="btn"><spring:message  code="label.no"/></a>
			        <a href='#' class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message  code="label.yes"/></a>
				</div>
			</div>
			 <script type="text/javascript">
		       $('#addbankaccount').modal("show");
			</script>	 
		</c:if>
<script type="text/javascript">
	$('.ttclass').tooltip();
</script>

<script type="text/javascript">

function submitForm(){
	if($("#bankAccountName").val() != ""){  // if file browsed then do upload 
		$('#frmIssuerFinInfo').ajaxSubmit({
			url: "<c:url value='/planmgmt/issuer/financial/info/exists'/>",
			data: { "bankAccountName": $("#bankAccountName").fieldValue(),
					"bankName": $("#bankName").fieldValue()
				  },
			success: function(responseText){
					if(responseText=="Yes"){
						$("#bankAccountName_error").html("<label class='error'><span><em class='excl'>!</em><spring:message  code='err.bankAcctNameExists' javaScriptEscape='true'/></span></label>"); // show original file name
						return false;
					}else{
						$("#frmIssuerFinInfo").submit();
					}
	       	}
		});
	}
	else{
		$("#frmIssuerFinInfo").submit();	
	}
	 
} 

	var validator = $("#frmIssuerFinInfo").validate({ 
		rules : {
			bankAccountName : {required : true},
			bankName : {required : true},
			addressLine1 : { required : true},
			//addressLine2 : { required : true},
			city : { required : true},
			zipcode : { required : true, number : true, minlength : 5, maxlength : 5},		
			bankAccountNumber : {required : true, number : true},
			bankABARoutingNumber : {required : true, number : true, minlength : 9, maxlength :9}
		},
		messages : {
			bankAccountName : { required : "<span><em class='excl'>!</em><spring:message  code='err.bankAcctName' javaScriptEscape='true'/></span>"},
			bankName: { required : "<span><em class='excl'>!</em><spring:message  code='err.bankName' javaScriptEscape='true'/></span>"},
			addressLine1 : { required : "<span><em class='excl'>!</em><spring:message  code='err.addressLine1' javaScriptEscape='true'/></span>"},
			addressLine2 : { required : "<span><em class='excl'>!</em><spring:message  code='err.addressLine2' javaScriptEscape='true'/></span>"},
			city: { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerCity' javaScriptEscape='true'/></span>"},
			zipcode : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerZip' javaScriptEscape='true'/></span>",
				        number : "<span><em class='excl'>!</em><spring:message  code='err.issuervalidZip' javaScriptEscape='true'/></span>",
				        minlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerZipMinLength' javaScriptEscape='true'/></span>",
				        //maxlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerZipMaxLength' javaScriptEscape='true'/></span>"
			          },		
			bankAccountNumber : { required : "<span><em class='excl'>!</em><spring:message  code='err.bankAcctNumber' javaScriptEscape='true'/></span>"},
			bankABARoutingNumber : { required : "<span><em class='excl'>!</em><spring:message  code='err.bankABARoutingNumber' javaScriptEscape='true'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});

</script>
