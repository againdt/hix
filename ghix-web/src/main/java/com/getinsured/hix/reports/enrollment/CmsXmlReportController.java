package com.getinsured.hix.reports.enrollment;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.reports.enrollment.dto.CMSErrorReportDTO;
import com.getinsured.hix.reports.enrollment.dto.CMSFileTransmissionLogDTO;
import com.getinsured.hix.reports.enrollment.dto.CMSXmlRequestDTO;
import com.getinsured.hix.reports.enrollment.dto.CMSXmlSumryReportDTO;
import com.getinsured.hix.reports.enrollment.dto.MonthlyIRSErrorReportDTO;
import com.getinsured.hix.reports.enrollment.service.CMSService;
import com.getinsured.hix.reports.enrollment.util.CmsXmlReportConstants;

@Controller
public class CmsXmlReportController {

	@Autowired private CMSService cmsService;
	@Autowired private GIMonitorService giMonitorService;
	@Autowired private UserService userService;
	private static final Logger LOGGER = Logger.getLogger(CmsXmlReportController.class);
	private static final String VIEW_CMS_TRANFER_LOG_ERROR = "10001";
	private static final String VIEW_MONTHLY_IRS_ERROR = "10002";
	
	@RequestMapping(value = "/admin/reports/cmsxmlsummaryreport", method = RequestMethod.GET)
    @PreAuthorize(CmsXmlReportConstants.VIEW_CMS_XML_SUMMARY_REPORT)
	public String getCmsXMLSummaryReport(HttpServletRequest request) throws GIException {
		return "admin/reports/cmsxmlsummaryreport";
		
	}
	
	@RequestMapping(value = "/admin/reports/cmserrorreport", method = RequestMethod.GET)
    @PreAuthorize(CmsXmlReportConstants.VIEW_CMS_XML_ERROR_REPORT)
	public String getCmsErrorReport(HttpServletRequest request) throws GIException {
		return "admin/reports/cmserrorreport";
		
	}
	
	@RequestMapping(value = "/admin/reports/cmsfiletransmissionlogs", method = RequestMethod.GET)
	@PreAuthorize(CmsXmlReportConstants.VIEW_CMS_FILE_TRANSMISSION_LOGS)
	public String getCMSFileTransmissionLogs(HttpServletRequest request){
		return "admin/reports/cmsfiletransmissionlogs";
	}
	
	@RequestMapping(value = "/admin/reports/monthlyirserrorreport", method = RequestMethod.GET)
	@PreAuthorize(CmsXmlReportConstants.VIEW_MONTHLY_IRS_ERROR_REPORT)
	public String getMonthlyIRSErrorReport(HttpServletRequest request){
		return "admin/reports/monthlyirserrorreport";
	}
	
	/**
	 * Fetches CMS XML Summary Records for report
	 * @param request
	 * @return
	 * @throws GIException 
	 */
	@RequestMapping(value = "/admin/reports/getcmsxmlsummaryreport", method = RequestMethod.POST, produces= "application/json")
	@PreAuthorize(CmsXmlReportConstants.VIEW_CMS_XML_SUMMARY_REPORT)
	@ResponseBody
	public Page<CMSXmlSumryReportDTO> cmsXMLSummaryReport(@RequestParam("page") String pageStr, @RequestParam("size") String sizeStr,
			@RequestBody(required=false) CMSXmlRequestDTO cmsXmlRequestDTO) throws GIException {	
		int page = 1, size = 1;
    	
    	if(StringUtils.isNotBlank(pageStr)) {
    		page = Integer.parseInt(pageStr);
    	}
    	if(StringUtils.isNotBlank(sizeStr)) {
    		size = Integer.parseInt(sizeStr);
    	}
	
		Page<CMSXmlSumryReportDTO> resultPage = cmsService.findCmsXmlSumryReportRecords(page, size, cmsXmlRequestDTO);
		if (page > resultPage.getTotalPages()) {
            throw new GIException("Page requested is invalid");
        }
		return resultPage;
	}
	
	/**
	 * Fetches CMS Error Records for report
	 * @param request
	 * @return
	 * @throws GIException 
	 */
	@RequestMapping(value = "/admin/reports/getcmserrorreport", method = RequestMethod.POST, produces= "application/json")
	@PreAuthorize(CmsXmlReportConstants.VIEW_CMS_XML_ERROR_REPORT)
	@ResponseBody
    public Page<CMSErrorReportDTO> findErrorReport(@RequestParam("page") String pageStr, @RequestParam("size") String sizeStr,
    		@RequestBody(required=false) CMSXmlRequestDTO cmsXmlRequestDTO) throws GIException {
 

		int page = 1, size = 1;
    	
    	if(StringUtils.isNotBlank(pageStr)) {
    		page = Integer.parseInt(pageStr);
    	}
    	if(StringUtils.isNotBlank(sizeStr)) {
    		size = Integer.parseInt(sizeStr);
    	}
    	 
    	Page<CMSErrorReportDTO> resultPage = cmsService.getCMSErrorReport(page, size, cmsXmlRequestDTO);
    	 if (page > resultPage.getTotalPages()) {
             throw new GIException("Page requested is invalid");
         }
    	return resultPage;
    }
	
	/**
	 * 
	 * @param String pageStr
	 * @param String sizeStr
	 * @param Object<CMSXmlRequestDTO> cmsXmlRequestDTO
	 * @return
	 * Jira Id: HIX-105327
	 */
	@RequestMapping(value = "/admin/reports/cmsfiletransmissionlogs", method = RequestMethod.POST)
	@PreAuthorize(CmsXmlReportConstants.VIEW_CMS_FILE_TRANSMISSION_LOGS)
	@ResponseBody
	public Page<CMSFileTransmissionLogDTO> fetchCMSFileTransmissionLogData(@RequestParam("pageNumber") String pageNumber, @RequestParam("pageSize") String pageSize,
    		@RequestBody(required=false) CMSXmlRequestDTO cmsXmlRequestDTO){
		Page<CMSFileTransmissionLogDTO> cmsFileTransmissionLogData = null;
		try{
			cmsFileTransmissionLogData = cmsService.getCMSFileTransmissionLogData(pageNumber, pageSize, cmsXmlRequestDTO);
		}
		catch(Exception ex){
			logException(VIEW_CMS_TRANFER_LOG_ERROR, ex);
			LOGGER.error("Exception occurred while processing CMS File Transmission log ", ex);
		}
		return cmsFileTransmissionLogData;
	}
	
	/**
	 * 
	 * @param String pageStr
	 * @param String sizeStr
	 * @param Object<CMSXmlRequestDTO> cmsXmlRequestDTO
	 * @return
	 * Jira Id: HIX-105328
	 */
	@RequestMapping(value = "/admin/reports/monthlyirserrorreport", method = RequestMethod.POST)
	@PreAuthorize(CmsXmlReportConstants.VIEW_CMS_FILE_TRANSMISSION_LOGS)
	@ResponseBody
	public Page<MonthlyIRSErrorReportDTO> fetchMonthlyIRSErrorReportData(@RequestParam("pageNumber") String pageNumber, @RequestParam("pageSize") String pageSize,
    		@RequestBody(required=false) CMSXmlRequestDTO cmsXmlRequestDTO){
		Page<MonthlyIRSErrorReportDTO> monthlyIRSErrorReportDTOData = null;
		try{
			monthlyIRSErrorReportDTOData = cmsService.getMonthlyIRSErrorReportData(pageNumber, pageSize, cmsXmlRequestDTO);
		}
		catch(Exception ex){
			logException(VIEW_MONTHLY_IRS_ERROR, ex);
			LOGGER.error("Exception occurred while processing Monthly IRS Error Report ",ex);
		}
		return monthlyIRSErrorReportDTOData;
	}
	
	/**
	 * 
	 * @return
	 */
	private AccountUser getLoggedInUserInfo(){
		AccountUser accountUser = null;
		try{
			accountUser = userService.getLoggedInUser();
		}
		catch(InvalidUserException inUE){
			LOGGER.error("Enrollment_CMS_UI_Report:: unable to find login user info ", inUE);
		}
		return accountUser;
	}
	
	/**
	 * 
	 * @param errorCode
	 * @param exception
	 */
	private void logException(final String errorCode, final Exception exception){
		try{
			giMonitorService.saveOrUpdateErrorLog(errorCode, new TSDate(), exception.getClass().getName(), ExceptionUtils.getStackTrace(exception), getLoggedInUserInfo());
		}
		catch(Exception ex){
			LOGGER.error("Enrollment_CMS_UI_Report:: exception occurred while saving exception logs ",ex);
		}
	}
}
