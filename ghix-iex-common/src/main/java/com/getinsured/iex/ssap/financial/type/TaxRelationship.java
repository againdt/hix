package com.getinsured.iex.ssap.financial.type;

/**
 * Describes tax relationship between household members.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-07
 */
public enum TaxRelationship {
  UNSPECIFIED,
  FILER,
  DEPENDENT,
  FILER_DEPENDENT;
}
