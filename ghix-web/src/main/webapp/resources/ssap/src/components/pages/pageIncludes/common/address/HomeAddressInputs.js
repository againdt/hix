import React, { Fragment } from 'react';
import _ from 'lodash';

import UniversalInput from '../../../../../lib/UniversalInput';

import INPUT_TYPES from '../../../../../constants/inputTypes';
import DROPDOWN_OPTIONS, { getOptionsForHardcodedState } from '../../../../../constants/dropdownOptions';
import VALIDATION_TYPES from '../../../../../constants/validationTypes';

const HomeAddressInputs = props => {

    const {
        postalCode,
        countyCode,
        streetAddress1,
        streetAddress2,
        state,
        city,
        currentData,
        counties,
        pageName,
        stateCode,
        ...restProps
    } = props;

    const cleanedRestProps = _.omit(restProps, 'children');
    const inputActionsWithoutOnBlur = _.omit(cleanedRestProps.inputActions, ['onFieldBlur', 'onFieldFocus']);
    const cleanedRestPropsWithoutOnBlur = {
        ...cleanedRestProps,
        inputActions: inputActionsWithoutOnBlur
    };

    return (
        <Fragment>
            <UniversalInput
                label={{
                    text: 'Address 1',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'streetAddress1',
                    helperClasses: [],
                    value: streetAddress1,
                    placeholder: 'Address 1',
                    validationType: VALIDATION_TYPES.ADDRESS_LINE1,
                    currentData: currentData,
                    fields: ['householdContact', 'homeAddress']
                }}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'Address 2',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'streetAddress2',
                    helperClasses: [],
                    value: streetAddress2,
                    placeholder: 'Address 2',
                    validationType: VALIDATION_TYPES.ADDRESS_LINE2,
                    currentData: currentData,
                    fields: ['householdContact', 'homeAddress']
                }}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'City',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'city',
                    helperClasses: [],
                    value: city,
                    placeholder: 'City',
                    validationType: VALIDATION_TYPES.CITY,
                    currentData: currentData,
                    fields: ['householdContact', 'homeAddress']
                }}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'Zip',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'postalCode',
                    helperClasses: [],
                    value: postalCode,
                    placeholder: 'Zip',
                    validationType: VALIDATION_TYPES.ZIP_CODE,
                    currentData: currentData,
                    fields: ['householdContact', 'homeAddress']
                }}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'State',
                    isLabelRequired: true
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.DROPDOWN,
                    name: 'state',
                    value: stateCode ? stateCode : state,
                    options: stateCode ? getOptionsForHardcodedState(stateCode) : DROPDOWN_OPTIONS.STATE,
                    validationType: VALIDATION_TYPES.CHOSEN,
                    currentData: JSON.parse(JSON.stringify(currentData)),
                    fields: ['householdContact', 'homeAddress']
                }}
                {...cleanedRestPropsWithoutOnBlur}
            />
            <UniversalInput
                label={{
                    text: 'County',
                    isLabelRequired: true
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.DROPDOWN,
                    name: 'countyCode',
                    value: countyCode,
                    options: DROPDOWN_OPTIONS.COUNTIES(counties, pageName),
                    validationType: VALIDATION_TYPES.CHOSEN,
                    currentData: currentData,
                    fields: ['householdContact', 'homeAddress'],
                    disabled: !postalCode
                }}
                {...cleanedRestPropsWithoutOnBlur}
            />
        </Fragment>
    );
};

export default HomeAddressInputs;