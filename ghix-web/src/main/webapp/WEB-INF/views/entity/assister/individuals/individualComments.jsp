<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/style.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chosen.css" />" />


<!-- File upload scripts -->
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld" %>
<c:set var="isConsumerActivated" value="${isConsumerActivated}" ></c:set>
<c:set var="isUserCreated" value="${isIndividualUserExists}" ></c:set>
<c:set var="externalIndividualNewId" ><encryptor:enc value="${externalIndividual.id}" isurl="true"/> </c:set>
<script type="text/javascript">
	$(function() {
		$(".newcommentiframe").click(function(e){
	        e.preventDefault();
	        var href = $(this).attr('href');
	        if (href.indexOf('#') != 0) {
	        	$('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="'+href+'" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal({backdrop:"static"});
			}
		});
	});
	
	function closeCommentBox() {
		//$("#newcommentiframe").remove(); //not working in FF changed to below line also HIX-57529 fixed
		$("#newcommentiframe").modal('hide');
		window.location.reload(true);
	}
	function closeIFrame() {
		$("#newcommentiframe").modal('hide');
		window.location.reload(true);
	}
</script>


	
<div class="gutter10-lr">
	<div class="l-page-breadcrumb">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message
							code="label.back" /></a></li>
				<li><spring:message	code="label.assister.active"/></li>
				<li>${externalIndividual.firstName} ${externalIndividual.lastName}</li>
				<li><spring:message	code="label.assister.comments"/></li>
			</ul>
		</div>
		<!--  end of row-fluid -->
	</div>
	<!--  end l-page-breadcrumb -->
	<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != 'false'}">
				<c:out value="${errorMsg}"></c:out>
			</c:if>
		</div>
	</div>
	

<!--  Latest UI -->

<c:if test="${message != null}">
<div class="errorblock alert alert-info">
	<p>
		${message}
	</p>
</div>
</c:if>

		<h1>${externalIndividual.firstName} ${externalIndividual.lastName}</h1>
	<div class="row-fluid">
			<div id="sidebar" class="span3">
					<h4 class="header"><spring:message code="label.assister.aboutThisIndividual"/></h4>
						<ul class="nav nav-list">
						<c:set var="encryptedEntryId" ><encryptor:enc value="${externalIndividual.id}" isurl="true"/> </c:set>
							<li class="link"><a href="/hix/entity/assister/individualinfo/${encryptedEntryId}"><spring:message	code="label.assister.summary"/></a></li>
		                  	<li class="active"><spring:message	code="label.assister.comments"/></li>
		                </ul>
				<br>
					<h4 class="header"><i class="icon-cog icon-white"></i><spring:message code="label.assister.actions"/></h4>
						<ul class="nav nav-list">
							<c:set var="externalIndividualIdCommentsParam" ><encryptor:enc value="${externalIndividual.id}" isurl="true"/> </c:set>	
						   	<li><a name="addComment" href="<c:url value="/entity/assister/newcomment?target_id=${externalIndividualIdCommentsParam}&target_name=DESIGNATEASSISTER&individualName=${externalIndividual.firstName} ${externalIndividual.lastName}"/>" id ="addComment" class="newcommentiframe"> <i class="icon-comment"></i><spring:message code="label.assister.newComment"/></a></li>
						   
						   <c:if test="${checkDilog == null}">
						   	<li><a href="#viewindModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.assister.switchToIndividualView"/></a></li>
							</c:if>
							<c:if test="${checkDilog != null}">
								<li class="navmenu"><a href="/hix/switchToIndividualView/dashboard?switchToModuleName=<encryptor:enc  value="individual"/>&switchToModuleId=<encryptor:enc value="${externalIndividual.id}" />&switchToResourceName=${externalIndividual.firstName} ${externalIndividual.lastName}" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.assister.switchToIndividualView"/></a></li>
							</c:if>
						   	
						   <c:if test="${isConsumerActivated == false}">
				  				<li><a name ="send-activation-link"  href="#"  id="send-activation-link"><i class="icon-envelope"></i><spring:message code="label.assister.resendActivationEmail"/></a></li>
				 		   </c:if> 	
		                </ul>
		</div>
		
		
		<div id="rightpanel" class="span9">
		
			<form class="form-vertical" id="commentForm" name="commentForm" action="" method="POST">
			<df:csrfToken/>
				<table class="table">
					<thead>
						<tr class="header">
							<th><h4><spring:message	code="label.back" /><spring:message	code="label.assister.comments"/></h4></th>
							<th class="txt-right"><a class="btn btn-primary showcomments"><spring:message code="label.assister.addComments"/></a></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="2" class="">	
							    <comment:view targetId="${externalIndividual.id}" targetName="DESIGNATEASSISTER">
								</comment:view> 
								
								<jsp:include page="../../../platform/comment/addcomments.jsp">
									<jsp:param value="" name=""/>
								</jsp:include>
								<input type="hidden" value="DESIGNATEASSISTER" id="target_name" name="target_name" /> 
								<input type="hidden" value="<encryptor:enc value="${externalIndividual.id}"/>" id="target_id" name="target_id" />
								
							</td>
						</tr>
					</tbody>
				</table>
				<span id="processing" class="margin10-l hide"> <spring:message code="ssap.footer.processing" text="Processing please wait"/><img class="margin10-l" src='/hix/resources/img/loader_greendots.gif' width='10%' alt='loader dots'/></span>
				<input type="hidden" name="individualName" id="individualName" value="${externalIndividual.firstName} ${externalIndividual.lastName}" />
				
			</form>
		 
		<!-- Modal -->
			<jsp:include page="individualViewModal.jsp" />
		<!-- Modal end -->	
		
		</div>
	</div>	
	</div>
<!--  Latest UI -->
<script type="text/javascript">

$("document").ready(function (){
	var csrValue = $("#csrftoken").val();	
	$("#send-activation-link").click(function (){
		if(${isConsumerActivated}) {
	         $('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0;"><h3>Failure!</h3></div><div class="modal-body" style="max-height:470px;"><p> ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName} has already created an account. A new'  
	            + 'activation email cannot be sent.</p></div><div class="modal-footer txt-center"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
	        } 
		
		else if(${isUserCreated}) {
	         $('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0;"><h3>Failure!</h3></div><div class="modal-body" style="max-height:470px;"><p> ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName} has already created an account. A new'  
	 	            + 'activation email cannot be sent.</p></div><div class="modal-footer txt-center"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
	 	        } 
		else {
	        	 $("#processing").show();
	        	 var  strPath =  "<%=  request.getContextPath()  %>";
                 var  pathURL=strPath+"/assister/individual/sendactivationlink/${externalIndividualNewId}";
                 $.ajax({ 	type: "POST",
                	 		url: pathURL,
                	 		 data:{ csrftoken:'<df:csrfToken plainToken="true"/>' },
                	 		success: function (data,status) {
                	 			if(status === 'success') {
    	              				if(-1 == data.search("SUCCESS")) { 
    	              					$("#processing").hide();
    	               					$('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3></div><div class="modal-body" style="max-height:470px;"><p>A new activation email could not be sent to ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}.</p></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
    	              				}else {
    	              					$("#processing").hide();
    	               					$('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Success!</h3></div><div class="modal-body" style="max-height:470px; "><p>A new activation email has been sent to ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}.</p></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
    	              				}
    	             			}else {
    	             				$("#processing").hide();
    	              				$('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3></div><div class="modal-body" style="max-height:470px;"><p>A new activation email could not be sent to ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}.</p></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
    	             			}
                	           
                	        },
                	        error: function(e) {
                	            console.log("Error:" + e);
                	        }
                 });
	        }
		
		
	  });
	
});
</script>