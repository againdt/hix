<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.joda.time.DateTime"%>
<%@page import="org.joda.time.LocalDate"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld"%>
<%@page import="com.getinsured.timeshift.util.TSDate"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="org.apache.commons.lang3.time.DateUtils"%>
<style>
  #menu {
    display: none;
  }
  #contactRequestAckSuccess, #contactRequestAckFailure {
    width: 75%;
    text-align: center;
    padding: 10px 0 30px;
    margin: 0 auto;
  }
  #contactRequestAckSuccess p, #contactRequestAckFailure p {
    margin-bottom: 20px;
  }
  .grid-display{
		display: grid;
		display: -ms-flexbox;
		justify-content: space-between;
		grid-template-columns: 1fr 1fr 1fr;
  }

</style>

<div class="gutter10"  id="consumerHistory">
	<div class="row-fluid">
		<h1 id="skip" class="householdInfo">${household.firstName} ${household.lastName} (ID: ${householdId})</h1>
	</div>${encHouseHoldId}
	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<jsp:include page="consumerSummaryNavigation.jsp"></jsp:include>
		</div>
		<div class="span9" id="rightpanel">
			<div class="row-fluid">
				<div ng-app="indPortalApp" ng-controller="indPortalAppCtrl">
				
<div class="gutter10 hidden" id="individual-portal"> 
  <input id="aptc" name="aptc" type="hidden" value="${aptc}"/>
	<input id="stateSubsidy" name="stateSubsidy" type="hidden" value="${stateSubsidy}"/>
  <input id="csr" name="csr" type="hidden" value="${csr}"/>
  <input id="stage" name="stage" type="hidden" value="${stage}"/>
  <input id="issuerLogo" name="issuerLogo" type="hidden" value="${issuerLogo}"/>
  <input id="planName" name="planName" type="hidden" value="${planName}"/>
  <input id="permonth" name="permonth" type="hidden" value="${permonth}"/>
  <input id="monthlyPremium" name="monthlyPremium" type="hidden" value="${monthlyPremium}"/>
  <input id="favoritePlanSelected" name="favoritePlanSelected" type="hidden" value="${favoritePlanSelected}"/>
  <input id="planType" name="planType" type="hidden" value="${planType}"/>
  <input id="appliedCredit" name="appliedCredit" type="hidden" value="${appliedCredit}"/>
  <input id="officeVisit" name="officeVisit" type="hidden" value="${officeVisit}"/>
  <input id="genericMedication" name="genericMedication" type="hidden" value="${genericMedication}"/>
  <input id="deductible" name="deductible" type="hidden" value="${deductible}"/>
  <input id="outOfPocket" name="outOfPocket" type="hidden" value="${outOfPocket}"/>
  <input id="applicationStartLabel" name="applicationStartLabel" type="hidden" value="${applicationStartLabel}"/>
  <input id="enableEnrollment" name="enableEnrollment" type="hidden" value="${enableEnrollment}"/>
  <input id="enrollCaseNumber" name="enrollCaseNumber" type="hidden" value="${enrollCaseNumber}"/>
  <input id="isInsideOe" name="isInsideOe" type="hidden" value="${isInsideOe}"/>
  <input id="isOeApproaching" name="isOeApproaching" type="hidden" value="${isOeApproaching}"/>
  <input id="daysTostartOe" name="daysTostartOe" type="hidden" value="${daysTostartOe}"/>
  <input id="daysEndOe" name="daysEndOe" type="hidden" value="${daysEndOe}"/>
  <input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>
  <input id="sCode" name="sCode" type="hidden" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>"/>
  <input id="enableEnrollOnOE" name="enableEnrollOnOE" type="hidden" value="${enableEnrollOnOE}"/>	
  <input id="isRecTribe" name="isRecTribe" type="hidden" value="${isRecTribe}"/>
  <input id="showEnrollButton" name="showEnrollButton" type="hidden" value="${showEnrollButton}"/>
  <input id="isPlanEffective" name="isPlanEffective" type="hidden" value="${isPlanEffective}"/>
  <input id="coverageStartDate" name="coverageStartDate" type="hidden" value="${coverageStartDate}"/>
  <input id="coverageEndDate" name="coverageEndDate" type="hidden" value="${coverageEndDate}"/>
  <input id="applicationStatus" name="applicationStatus" type="hidden" value="${applicationStatus}"/>
  <input id="qepFlag" name="qepFlag" type="hidden" value="${qepFlag}"/>
  <input id="allowChangePlan" name="allowChangePlan" type="hidden" value="${allowChangePlan}"/>
  <input id="changePlanInSEP" name="changePlanInSEP" type="hidden" value="${changePlanInSEP}"/>
  <input id="coverageYear" name="coverageYear" type="hidden" value="${coverageYear}"/>
  <input id="showChangePlanInOE" name="showChangePlanInOE" type="hidden" value="${showChangePlanInOE}"/>
  <input id="renewed" name="renewed" type="hidden" value="${markedOtr}"/>
  <input id="applicationValidationStatus" name="applicationValidationStatus" type="hidden" value="${applicationValidationStatus}"/>
  <input id="switchToModuleName" type="hidden" name="switchToModuleName" value='<encryptor:enc value="individual"/>' />
  <input id="switchToModuleId" type="hidden" name="switchToModuleId"  value='<encryptor:enc value="${householdId}"/>' />
   <input id="switchToResourceName" type="hidden" name="switchToResourceName" value="${household.firstName} ${household.lastName}" />
 </div>

<script src="<gi:cdnurl value="/resources/angular/angular-file-upload-shim.min.js"/>"></script>
<script src="<gi:cdnurl value="/resources/angular/angular-file-upload.min.js"/>"></script>
<script src="<gi:cdnurl value="/resources/angular/mask.js"/>"></script>
<script src="<gi:cdnurl value="/resources/angular/ui-bootstrap-tpls-0.9.0.min.js"/>"></script>
<script src="<gi:cdnurl value="/resources/js/cap/myApplications.js"/>?v=3.0" type="text/javascript"></script>
<script src="<gi:cdnurl value="/resources/js/angular-sanitize.min.js"/>"></script>
<script type="text/javascript" src="/hix/resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/hix/resources/js/spring-security-csrf-token-interceptor.js"></script>

<script src="<c:url value="/resources/js/moment.min.js" />"></script>
<script src='<c:url value="/resources/js/preferences/communicationPreferences.controller.js" />'></script>
<script src='<c:url value="/resources/js/preferences/communicationPreferences.directive.js" />'></script>
<script src='<c:url value="/resources/js/preferences/communicationPreferences.service.js" />'></script>
<script src='<c:url value="/resources/js/addressValidation.module.js" />'></script>

<script
  src="<c:url value="/resources/js/angular-bootstrap-datepicker.js"/>"></script>

<%
	String gracePeriod = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);	
	String maCurrentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
	String coverageYearOptionCutOffDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_SHOW_CURRENT_YEAR_TAB);
	try{
		    if(coverageYearOptionCutOffDate != null && coverageYearOptionCutOffDate.trim().length() != 0){
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				Date todaysDate = new TSDate();
				Date cutOffDate = DateUtils.parseDateStrictly(coverageYearOptionCutOffDate, new String[]{"MM/dd/yyyy"});
				if (todaysDate.before(cutOffDate)) {
					maCurrentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
				}
		    }
		}
		catch(Exception ex){}
	String maRenewalCoverageYear = maCurrentCoverageYear;
	String maServerDateTime = new TSDate().toString();
	String renewalCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_RENEWAL_COVERAGE_YEAR);
	if(null!=renewalCoverageYear && renewalCoverageYear.trim().length()>0){
		maRenewalCoverageYear = renewalCoverageYear;
	}
	String exchangeStartYear = "2015";
	String exchangeStartYearProperty = DynamicPropertiesUtil.getPropertyValue("global.exchangeStartYear");
	if(null!=exchangeStartYearProperty && exchangeStartYearProperty.trim().length()>0){
	    exchangeStartYear = exchangeStartYearProperty;
	}
%>

<input id="maCurrentCoverageYear" name="maCurrentCoverageYear" type="hidden" value="<%=maCurrentCoverageYear%>"/>
<input id="gracePeriod" name="gracePeriod" type="hidden" value="<%=gracePeriod%>"/>
<input id="maServerDateTime" name="maServerDateTime" type="hidden" value="<%=maServerDateTime%>" />
<input id="gExchangeStartYear" name="gExchangeStartYear" type="hidden" value="<%=exchangeStartYear%>" />

<script>
// data retriving from spring source has to be in jsp files
var csrStorage = {
		edit:{header: '<spring:message code="indportal.portalhome.editapplication2"/>', content: '<spring:message code="indportal.portalhome.editapplicationcontent" javaScriptEscape="true"/> '},
		initiate:{header: '<spring:message code="indportal.portalhome.initiateverifications2" />', content: '<spring:message code="indportal.portalhome.initiateverificationscontent" javaScriptEscape="true"/> '},
		rerun:{header: '<spring:message code="indportal.portalhome.reruneligibility2"/>', content: '<spring:message code="indportal.portalhome.reruneligibilitycontent" javaScriptEscape="true"/> '},
		cancelTerm:{header: '<spring:message code="indportal.portalhome.cancelorterminatemyplan2" />', content: '<spring:message code="indportal.portalhome.cancelorterminatemyplancontent" javaScriptEscape="true"/>'},
		update:{header: '<spring:message code="indportal.portalhome.updatecarrier2" />', content: '<spring:message code="indportal.portalhome.updatecarriercontent" javaScriptEscape="true"/>'},
		view:{header: 'view', content: 'csrStorage.view.content'},
		specialEnroll:{header: '<spring:message code="indportal.portalhome.openspecialenorollment2"/>', content: '<spring:message code="indportal.portalhome.openspecialenorollmentcontent" javaScriptEscape="true"/>'},
		coverageDate:{header: '<spring:message code="indportal.portalhome.changecoveragedate2" />', content: '<spring:message code="indportal.portalhome.changecoveragedatecontent" javaScriptEscape="true"/>'},
		reinstate:{header: '<spring:message code="indportal.portalhome.reinstateenrollment2" />', content: '<spring:message code="indportal.portalhome.reinstateenrollmentcontent" javaScriptEscape="true"/> '},
		overrideSEPDenial:{header: '<spring:message code="indportal.portalhome.overridesepdenial2"/>', content: '<spring:message code="indportal.portalhome.overridesepdenialcontent" javaScriptEscape="true"/>'},
		sendObAt:{header: '<spring:message code="indportal.portalhome.sendobat"/>', content: '<spring:message code="indportal.portalhome.sendobatcontent" javaScriptEscape="true"/>'},
		overrideProgElig:{header: '<spring:message code="indportal.portalhome.overrideprogelig"/>', content: '<spring:message code="indportal.portalhome.overrideprogeligcontent" javaScriptEscape="true"/>'}
};

var encHouseholdId = '<encryptor:enc value="${householdId}" isurl="true" />';
$(document).ready(function(){
	$("#applicationsMenu").addClass("active navmenu");
});

</script>

<!-- <script type="text/template" id="myApplications"> -->
<!-- <div  ng-controller="indPortalAppCtrl"> -->

	<div class="row-fluid" ng-init="init()" ng-cloak>
	<!-- <div spinning-loader="loader"></div> -->
		<div class="header">
            <h4  class="col-xs-12 col-sm-7 col-md-8 col-lg-8 no-pb pt-10"><spring:message code="indportal.portalhome.currentapplication" javaScriptEscape="true"/></h4>
			<span class="col-xs-12 col-sm-5 col-md-4 col-lg-4 dropdown coverage-year coverage-year_dropdown coverage-year_right">
                <label for="application_year" class="bs3-control-label-inline"><spring:message code="indportal.portalhome.applicationYear"/></label>
				<select id="application_year" class="input-small" ng-model="maCurrentCoverageYear" ng-options="coverageYear for coverageYear in maCurrentCoverageYearArray" ng-change="init()"></select>
			</span>
		</div>
		
		<div class="dashboard-rightpanel" id="rightpanel">
			<div ng-if="currentApplications.length === 0 && queuedApplications.length === 0 && pastApplications.length === 0" class="noApplications">
				<p><spring:message code="indportal.portalhome.noapplications" javaScriptEscape="true"/></p>
			</div>
			<div ng-if="currentApplications.length === 0 && (queuedApplications.length > 0 || pastApplications.length > 0)" class="noApplications">
				<p><spring:message code="indportal.portalhome.nocurrentapplications" javaScriptEscape="true"/></p>
			</div>
			
			<div class="applicationState margin20-b" ng-repeat="application in currentApplications"  ng-if="currentApplications.length>0">
				<div class="well-step" id="currentAppStatus">
                    <div class="appsHeaders margin10-b row-fluid">
		          		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
											<b><spring:message code="indportal.portalhome.applicationName" arguments='{{application.coverageYear}}' javaScriptEscape="true"/></b>
					     	<div><b><spring:message code="indportal.portalhome.type" javaScriptEscape="true"/>:</b> {{application.appType}}</div>
					     	<div>
					     		<b><spring:message code="indportal.portalhome.primarytaxfiler" javaScriptEscape="true"/>:</b> <td>{{application.ptfFirstName}} {{application.ptfMiddleName}} {{application.ptfLastName}}</td>
					     	</div>
					     	<div><b><spring:message code="indportal.portalhome.applicationstatus" javaScriptEscape="true"/>:</b> {{application.applicationStatus}}</div>
		          		</div>
		          		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		          			<div><b><spring:message code="indportal.portalhome.eligstartdate" javaScriptEscape="true"/>:</b> {{application.eligibilityStartDate}}</div>
							<div><b><spring:message code="indportal.portalhome.eligenddate" javaScriptEscape="true"/>:</b> {{application.eligibilityEndDate}}</div>
		          			<div ng-if="application.isNonFinancial === false">
								<div><b><spring:message code="indportal.portalhome.aptc" javaScriptEscape="true"/>:</b> {{application.aptc === null ? "Not Eligible" : (application.aptc | currency)}}</div>
								<div ng-if="(application.stateSubsidy)"><b><spring:message code="indportal.portalhome.StateSubsidy" javaScriptEscape="true"/>:</b> {{application.stateSubsidy | currency}}</div>
								<div><b><spring:message code="indportal.portalhome.csr" javaScriptEscape="true"/>:</b> {{application.csr === null ? "Not Eligible" : application.csr}}</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					        <div><b><spring:message code="indportal.portalhome.casenumber" javaScriptEscape="true"/>:</b> {{application.caseNumber}}</div>
							<div><b><spring:message code="indportal.portalhome.creationdate" javaScriptEscape="true"/>:</b> {{application.createdDate}}</div>
							<div><b><spring:message code="indportal.portalhome.lastupdated" javaScriptEscape="true"/>:</b> {{application.lastUpdated}}</div>
							<div ng-if="(application.queuedStatus)">
		        				<b><spring:message code="indportal.portalhome.processingstatus" javaScriptEscape="true"/>:</b> {{application.queuedStatus}}
		        			</div>
				        </div>
                    </div>
		  		</div>
				<sec:accesscontrollist hasPermission="MYAPP_ACTIONS" domainObject="user">
					<!-- CSR MENU -->
					<div id="csr_wrapper_{{$index}}" class="margin40-b margin5-r" ng-if="isUserAllowedForActions(userPermissionsList, application)">
					  <div id="csrButton" class="row-fluid">
						<div id="opencsrMenu" class="pull-right center" ng-click="csractions($index)"  data-toggle="tooltip" rel="tooltip" data-placement="top" data-original-title="<strong>Override Functions</strong>" data-html="true" tabindex="-1">
					    	<div class="gutter5">
					    		<i class="icon-gear" id="csrActionGear"></i> <span id="csrActionButton"><spring:message code="indportal.portalhome.actions" javaScriptEscape="true"/></span>
					    	</div>
					  	</div>
					  </div>
					</div>
					<!-- CSR MENU -->
					
					<!-- CSR OPTIONS -->
					<div class="well-step" id="csrMenu_{{$index}}" style="display:none">
					  <div>
							<div class="row-fluid">
								<div ng-if="userPermissionsList.length < 1">
									<div>There are no actions.</div>
								</div>
								<div ng-if="userPermissionsList.indexOf('IND_PORTAL_EDIT_SPCL_ENRLMNT_PERIOD') !== -1">
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 center csr-btns">
												<a id="aid_OEP" data-autamation-id="OEP" ng-click="openCSR(application.caseNumber, 'specialEnroll')" class="btn csractions"><spring:message code="indportal.portalhome.openspecialenorollment" javaScriptEscape="true"/></a>
										</div>
								</div>
							
								<div ng-if="userPermissionsList.indexOf('MYAPP_CHANGE_COVRAGE_STARTDATE') !== -1">
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 center csr-btns" ng-if="application.applicationStatus != 'Closed'">
										<a id="aid_changeCoverageDate" data-autamation-id="changeCoverageDate" ng-click="openCSR(application.caseNumber, 'coverageDate')"  class="btn csractions"><spring:message code="indportal.portalhome.changecoveragedate" javaScriptEscape="true"/></a>
									</div>
								</div>

								<div ng-if="userPermissionsList.indexOf('MYAPP_VIEW_OVERRIDE_HIST') !== -1">
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 center csr-btns">
											<a ng-click="getComments(application.caseNumber)" id="aid_viewOverrideHistory" data-autamation-id="viewOverrideHistory" class="btn csractions"><span><spring:message code="indportal.portalhome.viewoverridehistory" javaScriptEscape="true"/></span></a>
									</div>
								</div>
								
								<div ng-if="userPermissionsList.indexOf('MYAPP_EDIT_NONFIN_APP') !== -1">
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 center csr-btns" ng-if="application.applicationStatus === 'Eligibility Received' || application.applicationStatus === 'Partially Enrolled' || application.applicationStatus === 'Enrolled (Or Active)' || application.applicationStatus === 'Signed'">
										<a ng-click="openCSR(application.caseNumber, 'edit')" id="aid_editapplication" data-autamation-id="editapplication" class="btn csractions"><spring:message code="indportal.portalhome.editapplication" javaScriptEscape="true"/></a>
									</div>
								</div>
								
								<div ng-if="userPermissionsList.indexOf('MYAPP_INIT_NONFIN_VERIFICATION') !== -1">
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 center csr-btns" ng-if ="application.isNonFinancial">
											<a ng-click="openCSR(application.caseNumber, 'initiate')" id="aid_initiateverifications" data-autamation-id="initiateverifications" class="btn csractions"><spring:message code="indportal.portalhome.initiateverifications" javaScriptEscape="true"/></a>
									</div>
								</div>
								
								<div ng-if="userPermissionsList.indexOf('MYAPP_RTN_NONFIN_ELIGIBILITY') !== -1">
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 center csr-btns" ng-if ="application.isNonFinancial">
										<a ng-click="openCSR(application.caseNumber, 'rerun')" id="aid_reruneligibility" data-autamation-id="reruneligibility" class="btn csractions"><spring:message code="indportal.portalhome.reruneligibility" javaScriptEscape="true"/></a>
									</div>
								</div>
								
								<div ng-if="userPermissionsList.indexOf('MYAPP_CANCEL_TERMPLAN') !== -1 && (application.applicationStatus === 'Partially Enrolled' || application.applicationStatus === 'Enrolled (Or Active)')">
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 center csr-btns">
											<a ng-click="openCSR(application.caseNumber, 'cancelTerm')" id="aid_cancelorterminatemyplan" data-autamation-id="cancelorterminatemyplan" class="btn csractions"><spring:message code="indportal.portalhome.cancelorterminatemyplan" javaScriptEscape="true"/></a>
									</div>
								</div>
								
								<div ng-if="userPermissionsList.indexOf('MYAPP_SEND_DEMOGRPHCUPDT_CARRIER') !== -1">
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 center csr-btns" ng-if ="application.isNonFinancial">
										<a ng-click="openCSR(application.caseNumber, 'update')" id="aid_updatecarrier" data-autamation-id="updatecarrier" class="btn csractions"><spring:message code="indportal.portalhome.updatecarrier" javaScriptEscape="true"/></a>
									</div>
								</div>
								
								<div ng-if="userPermissionsList.indexOf('MYAPP_OVERRD_SPCL_ENROLLMENT') !== -1">
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 center csr-btns" ng-if="application.applicationStatus === 'Eligibility Received' && (application.isQep || application.isSepApp) && !(application.eligibilityStatus === 'DE' && application.isNonFinancial)">
											<a ng-click="getSepEventDetails(application)" id="aid_OSE" data-autamation-id="OSE" class="btn csractions">Override Special Enrollment</a>
									</div>
								
									<div ng-if="userPermissionsList.indexOf('MYAPP_OVERRD_APTCELGBLT_STARTDT') !== -1">
										<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 center csr-btns" ng-if="application.isSepApp && application.applicationStatus === 'Eligibility Received' && !application.isNonFinancial">
											<a ng-click="getOverrideEligibilityStartDate(application)" id="aid_APTC_EligibilityStarteDate" data-autamation-id="APTC_EligibilityStarteDate" class="btn csractions">Override APTC Eligibility Start Date</a>
										</div>
									</div>
								</div>
								<div ng-if="userPermissionsList.indexOf('MYAPP_SEND_OUTBOUND_AT') !== -1">
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 center csr-btns" ng-if ="outBoundButtonStatus">
										<a ng-click="openCSR(application.caseNumber, 'sendObAt')" id="aid_sendOutboundAt" data-autamation-id="sendOutboundAt" class="btn csractions">Send Outbound AT</a>
									</div>
								</div>
								
								<div ng-if="userPermissionsList.indexOf('MYAPP_OVERRIDE_PROG_ELIG') !== -1">
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 center csr-btns" ng-if ="(application.applicationStatus === 'Eligibility Received' || application.applicationStatus === 'Partially Enrolled' || application.applicationStatus === 'Enrolled (Or Active)')"> <!-- Check response of some API to determine show / hide of this button -->
										<a ng-click="getProgrmeEligibility(application)" id="aid_overrideProgElig" data-autamation-id="overrideProgElig" class="btn csractions">Override Program Eligibility</a>
									</div>
								</div>
					  	</div>
					</div>
				</sec:accesscontrollist>
				<!-- CSR OPTIONS -->
		    </div>
		</div>
		
		<!-- Queued Applications Starts-->
		<div class="header margin30-t" ng-if="queuedApplications.length>0">
			<h4><spring:message code="indportal.portalhome.queuedapplications" javaScriptEscape="true"/>
				<span class="pull-right coverage-year">
					<spring:message code="indportal.portalhome.applicationYear"/> {{maCurrentCoverageYear}}
				</span>
			</h4>
		</div>
		<div class="well-step" id="queuedAppStatus" ng-repeat="application in queuedApplications">
		
			<div class="appsHeaders grid-display">
		    	<div alt="Shop" aria-label="Shop">
								<b><spring:message code="indportal.portalhome.applicationName" arguments='{{application.coverageYear}}' javaScriptEscape="true"/></b>

					<div><b><spring:message code="indportal.portalhome.type" javaScriptEscape="true"/>:</b> {{application.appType}}</div>
					<div>
					     <b><spring:message code="indportal.portalhome.primarytaxfiler" javaScriptEscape="true"/>:</b> <td>{{application.ptfFirstName}} {{application.ptfMiddleName}} {{application.ptfLastName}}</td>
					</div>
					<div><b><spring:message code="indportal.portalhome.applicationstatus" javaScriptEscape="true"/>:</b> {{application.applicationStatus}}</div>
				</div>
				<div class="margin10-l">
					<div><b><spring:message code="indportal.portalhome.eligstartdate" javaScriptEscape="true"/>:</b> {{application.eligibilityStartDate}}</div>
					<div><b><spring:message code="indportal.portalhome.eligenddate" javaScriptEscape="true"/>:</b> {{application.eligibilityEndDate}}</div>
				</div>	
				<div class="margin30-l">
		        	<div>
		          		<b><spring:message code="indportal.portalhome.casenumber" javaScriptEscape="true"/>:</b> {{application.caseNumber}}
		        	</div>
					<div>
				 		<b><spring:message code="indportal.portalhome.creationdate" javaScriptEscape="true"/>:</b> {{application.createdDate}}
		        	</div>
					<div>
				 		<b><spring:message code="indportal.portalhome.lastupdated" javaScriptEscape="true"/>:</b> {{application.lastUpdated}}
		        	</div>
		         	<div>
				 		<b><spring:message code="indportal.portalhome.expectedprocessingdate" javaScriptEscape="true"/>:</b> {{application.dueDate}}
				 	</div>
				 	<div>
				 		<b><spring:message code="indportal.portalhome.processingstatus" javaScriptEscape="true"/>:</b> {{application.queuedStatus}}
				 	</div>
				</div>
			</div>
		</div>
		
		 <!-- Past Applications Starts-->
		<div class="header margin30-t" ng-if="pastApplications.length>0">
			<h4 class="col-xs-12 col-sm-7 col-md-8 col-lg-8"><spring:message code="indportal.portalhome.pastapplications" javaScriptEscape="true"/></h4>
				<span class="col-xs-12 col-sm-5 col-md-4 col-lg-4 margin10-t coverage-year_right coverage-year">
					<spring:message code="indportal.portalhome.applicationYear"/> {{maCurrentCoverageYear}}
				</span>

		</div>

		<div class="applicationState margin20-b" ng-repeat="application in pastApplications"  ng-if="pastApplications.length>0">
				<div class="well-step" id="pastAppStatus">
		
			<div class="appsHeaders row-fluid">
		    	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		            <b><spring:message code="indportal.portalhome.applicationName" arguments='{{application.coverageYear}}' javaScriptEscape="true"/></b>
					<div><b><spring:message code="indportal.portalhome.type" javaScriptEscape="true"/>:</b> {{application.appType}}</div>
					<div>
					     <b><spring:message code="indportal.portalhome.primarytaxfiler" javaScriptEscape="true"/>:</b>	<td>{{application.ptfFirstName}} {{application.ptfMiddleName}} {{application.ptfLastName}}</td>
					</div>
					<div><b><spring:message code="indportal.portalhome.applicationstatus" javaScriptEscape="true"/>:</b> {{application.applicationStatus}}</div>
				</div>
				<div  class="col-xs-12 col-sm-12 col-md-4 col-lg-4" >
					<div><b><spring:message code="indportal.portalhome.eligstartdate" javaScriptEscape="true"/>:</b> {{application.eligibilityStartDate}}</div>
					<div><b><spring:message code="indportal.portalhome.eligenddate" javaScriptEscape="true"/>:</b> {{application.eligibilityEndDate}}</div>
					<div ng-if="application.isNonFinancial === false">
						<div><b><spring:message code="indportal.portalhome.aptc" javaScriptEscape="true"/>:</b> {{application.aptc === null ? "Not Eligible" : (application.aptc | currency)}}</div>
						<div ng-if="(application.stateSubsidy)"><b><spring:message code="indportal.portalhome.StateSubsidy" javaScriptEscape="true"/>:</b> {{application.stateSubsidy | currency}}</div>
						<div><b><spring:message code="indportal.portalhome.csr" javaScriptEscape="true"/>:</b> {{application.csr === null ? "Not Eligible" : application.csr}}</div>
					</div>
				</div>
				<div  class="col-xs-12 col-sm-12 col-md-4 col-lg-4" >
		        	<div>
		          		<b><spring:message code="indportal.portalhome.casenumber" javaScriptEscape="true"/>:</b> {{application.caseNumber}}
		        	</div>
					<div>
				 		<b><spring:message code="indportal.portalhome.creationdate" javaScriptEscape="true"/>:</b> {{application.createdDate}}
		        	</div>
					<div>
				 		<b><spring:message code="indportal.portalhome.lastupdated" javaScriptEscape="true"/>:</b> {{application.lastUpdated}}
		        	</div>
		         	<div ng-if="(application.dueDate)">
				 		<b><spring:message code="indportal.portalhome.expectedprocessingdate" javaScriptEscape="true"/>:</b> {{application.dueDate}}
				 	</div>
		        	<div ng-if="(application.queuedStatus)">
		        		<b><spring:message code="indportal.portalhome.processingstatus" javaScriptEscape="true"/>:</b> {{application.queuedStatus}}
		        	</div>
		        </div>
			</div>
			</div>
			<sec:accesscontrollist hasPermission="MYAPP_ACTIONS" domainObject="user">
				<!-- CSR MENU -->
				<div id="csr_pwrapper_{{$index}}" class="margin40-b" ng-if="userPermissionsList.indexOf('IND_PORTAL_OVERRIDE_SEP_DENIAL') !== -1 && $index==0 && application.isSepQepDenied">
				  <div id="csrPButton">
					<div id="opencsrMenu" class="pull-right center"style="margin-top:-20px;" ng-click="csrPastActions($index)"  data-toggle="tooltip" rel="tooltip" data-placement="top" data-original-title="<strong>Override Functions</strong>" data-html="true" tabindex="-1">
				    	<div class="gutter5">
				    		<i class="icon-gear" id="csrActionGear"></i> <span id="csrPActionButton"><spring:message code="indportal.portalhome.actions" javaScriptEscape="true"/></span>
				    	</div>
				  	</div>
				  </div>
				</div>
				<!-- CSR MENU -->
				<!-- CSR OPTIONS -->
				<div class="well-step" id="csrPMenu_{{$index}}" style="display:none;margin-top: 35px;">
					<div class="gutter10 margin10-t">
						<div class="row-fluid">
							<div ng-if="userPermissionsList.indexOf('IND_PORTAL_OVERRIDE_SEP_DENIAL') !== -1 && $index==0">
								<div class="center csr-btns pull-left" ng-if ="application.isSepQepDenied">
									<a href="javascript:void(0)" ng-click="openOverrideSEPCSR(application.caseNumber, 'overrideSEPDenial', application.coverageYear)" id="aid_overrideBtnHeading" data-automation-id="overrideBtnHeading" class="btn btn-primary btn-small"><spring:message code="indportal.portalhome.overridesepdenialbtnheading" javaScriptEscape="true"/></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</sec:accesscontrollist>
		</div>
	</div>
	</div>
<!-- </div> -->

  <!-- Cancel Application Modal-->
  <div ng-show="modalForm.openDialog">
   <div modal-show="modalForm.openDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="cancelApplicationModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" id="aid_cancelApplicationModal">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="cancelApplicationModal" data-automation-id="cancelApplicationModalHeader" ><spring:message code="indportal.portalhome.cancelapplication" htmlEscape="false" javaScriptEscape="true"/></h3>
          </div>
          <div class="modal-body">
		  <p id="aid_selectedtocancel" data-autamation-id="selectedtocancel"><spring:message code="indportal.portalhome.youhaveselectedtocancel" javaScriptEscape="true"/></p> <br>
          <p id="aid_cancelapplicationmodal" data-autamation-id="cancelapplicationmodal"><spring:message code="indportal.portalhome.cancelapplicationmodal" javaScriptEscape="true"/></p>
          </div>
          <div class="modal-footer">
            <a href="javascript:void(0)" id="aid_nogoback" data-autamation-id="nogoback" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.nogoback" javaScriptEscape="true"/></a>
            <a href="javascript:void(0)" id="aid_yescancel" data-autamation-id="yescancel" class="btn btn-primary" ng-click="cancelApplication(caseNumber)" class=""><spring:message code="indportal.portalhome.yescancel" javaScriptEscape="true"/></a>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
  </div>
  <!-- Cancel Application Modal-->
	<!-- Pending application Dialog -->
			<div ng-show="pendingAppDialog">
			 <div modal-show="pendingAppDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="pendingAppModal" aria-hidden="true">
			    <div class="modal-dialog">
			      <div class="modal-content">
			        <div class="modal-header">
			          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			          <h3 id="pendingAppModal" style="font-size: 18px;border: none;"><spring:message code="indportal.pendingAppModal.header" javaScriptEscape="true"/></h3>
				</div>
			        <div class="modal-body">
						<spring:message code="indportal.pendingAppModalForCancel.body"/>
			</div>
			        <div class="modal-footer">
			          <a href="javascript:void(0);" class="btn btn-secondary" data-automation-id="pendingAppDialogOk" ata-dismiss="modal"><spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/></a>
		</div>
			      </div><!-- /.modal-content-->
			    </div> <!-- /.modal-dialog-->
	</div>
</div>


			<!-- Active enrollment Dialog -->
			<div ng-show="activeEnrollmentDialog">
			 <div modal-show="activeEnrollmentDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="activeEnrollmentModal" aria-hidden="true">
			    <div class="modal-dialog">
			      <div class="modal-content">
			        <div class="modal-header">
			          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			          <h3 id="activeEnrollmentModal" style="font-size: 18px;border: none;"><spring:message code="indportal.activeEnrollmentModal.header" javaScriptEscape="true"/></h3>
			        </div>
			        <div class="modal-body">
						<spring:message code="indportal.activeEnrollmentModal.body" javaScriptEscape="true"/>
			        </div>
			        <div class="modal-footer">
			          <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/></a>
			        </div>
			      </div><!-- /.modal-content-->
			    </div> <!-- /.modal-dialog-->
			   </div>
			</div>
			<!-- Event Summary modal -->
			<div ng-show="eventSummaryDialog">
			 	<div modal-show="eventSummaryDialog" class="modal fade eventSummaryDialog" tabindex="-1" role="dialog" aria-labelledby="eventSummaryModal" aria-hidden="true">
			    	<div class="modal-dialog">
				      	<div class="modal-content">
  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					          <h3 id="eventSummaryModal" style="font-size: 18px;border: none;"><spring:message code="indportal.eventSummaryModal.header" javaScriptEscape="true"/></h3>
	</div> 
	<div class="modal-body">
								<table class="table table-bordered table-striped" ng-if ="applicantEvents.length > 0">
									<thead>
										<tr><td>Name</td><td>Event</td><td>Event Date</td></tr>
									</thead>
									 <tbody>
										<tr ng-repeat="event in applicantEvents"><td>{{event.applicantName}}</td><td>{{event.eventName}}</td><td>{{event.eventDate}}</td></tr>
									</tbody>
								</table>
                                <div ng-if ="applicantEvents.length == 0">
                                    <spring:message code="indportal.eventSummaryModal.empty" javaScriptEscape="true"/>
                                </div>
  </div>
  <div class="modal-footer">
					          <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/></a>
					        </div>
						</div><!-- /.modal-content-->
			      </div><!-- /.modal-dialog-->
  </div>
</div>
<!-- Modal for failing init-->
<div ng-show= "openInitFail">
      <div modal-show="openInitFail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedInitModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 id="failedInitModal"><spring:message code="indportal.portalhome.technicalissue" javaScriptEscape="true"/></h3>
	</div> 
	<div class="modal-body">
					<p><spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p>
  	</div>
  	<div class="modal-footer">
            <button class="btn pull-left" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
  	</div>
</div>
<!-- Modal for failing init-->


  <!-- CSR Modal-->
  <div ng-show="modalForm.csr">
     <div modal-show="modalForm.csr" class="modal fade" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancelCsrAction()">&times;</button>
              <h3 id="myModalLabel"  ng-if="currentCsr.header">
				<span>{{currentCsr.header}}</span>
			  </h3>
            </div>

		  <div ng-hide ="csrInputOverride || csrInputTermDate || SPEopened || ReinstateenrollmentData.showReinstateEnrollmentOptions || changeCovStart || modalForm.subResult || overrideNewSEPDate || activefailuremsg">
            <div class="modal-body">
							<p ng-class="{fadeFont: fadeFont}">{{currentCsr.content}}</p>
            </div>

            <div class="modal-body" >
                <span class="oRReason"><spring:message code="indportal.portalhome.viewoverridehistorycontent" javaScriptEscape="true"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></span>
                <br>
                <textarea aria-label="change reason" class="overrideText" ng-model="modalForm.overrideComment" placeholder="Please explain the change you are making and why"></textarea>
                <br>
                <div>
                  <a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelCsrAction()" ng-disabled = "csrInputTermDate"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
                  <a class="btn btn-primary pull-right" ng-click="modalForm.overrideComment.length > 0 && submitOverrideComment(modalForm.overrideComment)" class="" ng-disabled = "csrInputTermDate || modalForm.overrideComment.length < 1"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
                  <span class="pull-left">{{maxLength - modalForm.overrideComment.length}} <spring:message code="indportal.portalhome.charactersleft" javaScriptEscape="true"/></span>
                </div>
            </div>

            </div>


	        <div ng-show="csrInputTermDate  && !modalForm.subResult">
	       	  <div class="modal-body">
	            <span><spring:message code="indportal.portalhome.csrtermdatecontent" javaScriptEscape="true"/></span>
	          	<br>
	            <br>
	            <span class="termDate"><spring:message code="indportal.portalhome.csrentertermdate" javaScriptEscape="true"/></span>
	            <br>
	           	<input aria-label="cancel date" type="text" class="dateinput" placeholder="mm/dd/yyyy" ui-mask="99/99/9999" ui-mask-use-viewvalue="true" ng-model="modalForm.cancelDate" ng-blur="dateCheck($event, 'termDateErr')"  num-only></input>
							<span ng-show="termDateErr" class="popover-content ng-binding zip-warning redBorder"><spring:message code="indportal.portalhome.csrspedateerror" javaScriptEscape="true"/></span>
							<div spinning-loader="loader"></div>
		      	</div>

	       	  <div class="modal-footer">
	            <a href="javascript:void(0)" class="btn btn-primary" ng-click="termDateErr || !modalForm.cancelDate || getSubsq()" class="" ng-disabled="termDateErr || !modalForm.cancelDate"><spring:message code="indportal.portalhome.disenroll" javaScriptEscape="true"/></a>
	          	<a href="javascript:void(0)" class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>

	          </div>
	        </div>

	        <div ng-if="SPEopened && !modalForm.subResult">
	          <div class="modal-body">
						 <form name="modalForm.sepDates">
		         <span class="termDate"><spring:message code="indportal.portalhome.csrenterspestart" javaScriptEscape="true"/></span>
		         <br>
		         <input type="text" class="dateinput" placeholder="mm/dd/yyyy" name="start" ui-mask="99/99/9999" ui-mask-use-viewvalue="true"  ng-model="SEPData.sepStartDate" ng-disabled="!SEPData.enableEditing" ng-blur="dateCheck($event, 'SEPStartErr')" num-only></input>
						 <span ng-show="SEPStartErr" class="popover-content ng-binding zip-warning redBorder"><spring:message code="indportal.portalhome.csrspedateerror" javaScriptEscape="true"/></span>
						<br>

		         <span class="termDate"><spring:message code="indportal.portalhome.csrenterspeend" javaScriptEscape="true"/></span>
		         <br>
		         <input type="text" class="dateinput" placeholder="mm/dd/yyyy" name="end" ui-mask="99/99/9999" ui-mask-use-viewvalue="true" ng-model="SEPData.sepEndDate" ng-disabled="!SEPData.enableEditing" ng-blur="dateCheck($event, 'SEPEndErr')" num-only></input>
						<span ng-show="SEPEndErr" class="popover-content ng-binding zip-warning redBorder"><spring:message code="indportal.portalhome.csrspedateerror" javaScriptEscape="true"/></span>
							</form>
							<div ng-show="!SEPData.enableEditing" >
									<spring:message code="indportal.portalhome.csrspedisabled" javaScriptEscape="true"/>
							</div>
						<div spinning-loader="loader"></div>
						</div>

		        <div class="modal-footer">
							<a class="btn btn-primary" ng-click="SEPStartErr || SEPEndErr || !SEPData.sepStartDate || !SEPData.sepEndDate || getSubsq()" ng-show="SEPData.enableEditing" ng-disabled="SEPStartErr || SEPEndErr || !SEPData.sepStartDate || !SEPData.sepEndDate"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
	            <a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
	          </div>

	       </div>


	        <div ng-if="changeCovStart && !modalForm.subResult">
	          <div class="modal-body">
						 <form name="modalForm.sepDates">
				 <span class="termDate"><spring:message code="indportal.portalhome.csrcovstartinfo" javaScriptEscape="true"/></span>
				 <br><br>
		         <span class="termDate"><spring:message code="indportal.portalhome.csrentercovstart" javaScriptEscape="true"/></span>
		         <br>
		         <input type="text" class="dateinput" placeholder="mm/dd/yyyy" name="cov" ui-mask="99/99/9999" ui-mask-use-viewvalue="true" ng-model="SEPData.covStartDate" ng-focus="event = $event" ng-change="covStartdateCheck(event, 'SEPCovErr')" ng-blur="covStartdateCheck($event, 'SEPCovErr')" num-only></input>
						 <span ng-show="SEPCovErr" class="popover-content ng-binding zip-warning redBorder"><spring:message code="indportal.portalhome.csrspedateerror" javaScriptEscape="true"/></span>
						 </form>
						<div spinning-loader="loader"></div>
						</div>

		        <div class="modal-footer">
	            <a class="btn btn-primary" ng-click="SEPCovErr || !SEPData.covStartDate || getSubsq($event)" ng-disabled="SEPCovErr || !SEPData.covStartDate"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
	            <a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
	          </div>

	       </div>

	        <div ng-if="ReinstateenrollmentData.showReinstateEnrollmentOptions && !modalForm.subResult">
	          	 <div class="modal-body">
						<div ng-if="ReinstateenrollmentData.dentalEnrollmentId || ReinstateenrollmentData.healthEnrollmentId">
						<span><spring:message code="indportal.portalhome.reinstateenrollmentMessage" javaScriptEscape="true"/></span><br><br>
				 		<form name="modalForm.reinstateEnrollmentOptions">
				 			<div>
								<input type="hidden" name="healthEnrollmentId" id="healthEnrollmentId" value="{{ReinstateenrollmentData.healthEnrollmentId}}" ng-model="ReinstateenrollmentData.healthEnrollmentId">
								<label class="checkbox" for="reinstateHealth">
									<input type="checkbox" name="reinstateHealth" id="reinstateHealth" ng-model="ReinstateenrollmentData.reinstateHealth" ng-disabled="!ReinstateenrollmentData.healthEnrollmentId">
									<spring:message code="indportal.portalhome.reinstateHealth" javaScriptEscape="true"/>
								</label>
      						</div>
      						<div>
								<input type="hidden" name="dentalEnrollmentId" id="dentalEnrollmentId" value="{{ReinstateenrollmentData.dentalEnrollmentId}}" ng-model="ReinstateenrollmentData.dentalEnrollmentId">
      							<label class="checkbox" for="reinstateDental">
									<input type="checkbox" name="reinstateDental" id="reinstateDental" ng-model="ReinstateenrollmentData.reinstateDental" ng-disabled="!ReinstateenrollmentData.dentalEnrollmentId">
									<spring:message code="indportal.portalhome.reinstateDental" javaScriptEscape="true"/>
								</label>
      						</div>
				 		</form>
					</div>
					<div ng-if="ReinstateenrollmentData.noenrollmentsPresent">
						<spring:message code="indportal.portalhome.noenrollmentsMessage" javaScriptEscape="true"/></span>
					</div>
					<div spinning-loader="loader"></div>
				</div>

		        <div class="modal-footer">
					<a class="btn btn-primary" ng-click="getSubsq()" ng-show="!ReinstateenrollmentData.noenrollmentsPresent" ng-disabled="!ReinstateenrollmentData.reinstateDental && !ReinstateenrollmentData.reinstateHealth"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
	            	<a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
	          	</div>
	       </div>

			<div ng-show="overrideNewSEPDate  && !modalForm.subResult">
	       	  <div class="modal-body">
	            <br>
	            <span class="termDate"><spring:message code="indportal.portalhome.overridesepdenialdate" javaScriptEscape="true"/></span>
	            <br>
				<br>
				<input type="hidden" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD)%>" id="sepDenialGracePeriod" name="sepDenialGracePeriod">
				<div class="input-append date newSEPEndDate"  data-date="">
	           	<input aria-label="end date" type="text" name="newSEPEndDate" id="aid_newSEPEndDate" class="dateinput newSEPEndDate" placeholder="mm/dd/yyyy" ui-mask="99/99/9999" ui-mask-use-viewvalue="true" ng-model="modalForm.newSEPEndDate" ng-blur="validateSepEndDate($event, 'newSEPEndDateErr')"  num-only></input>
				<span class="add-on dateSpan js-dateTrigger"><i class="icon-calendar"></i></span>
				</div>
				<span ng-show="newSEPEndDateErr" class="popover-content ng-binding zip-warning redBorder">{{newSEPEndDateErrMsg}}</span>
							<div spinning-loader="loader"></div>
		      	</div>

	       	  <div class="modal-footer">
	            <a href="javascript:void(0)" id="aid_newSEPEndDateErr" class="btn btn-primary" ng-click="newSEPEndDateErr || !modalForm.newSEPEndDate || getSubsq()" class="" ng-disabled="newSEPEndDateErr || !modalForm.newSEPEndDate"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
	          	<a href="javascript:void(0)" id="aid_newSEPEndDateCancel" class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>

	          </div>
	        </div>
			<div ng-if="activefailuremsg  && !modalForm.subResult">
	       	  <div class="modal-body">
				<br>
				<p><spring:message code="indportal.portalhome.overridesepdenialactivemsg" javaScriptEscape="true"/></p>
				</div>

	       	  <div class="modal-footer">
				<a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.communicationPreferences.close" javaScriptEscape="true"/></a>
	          </div>
	        </div>

					<div ng-if = "modalForm.subResult == 'success'">
					  <div class="modal-body">
			    		<h3 ng-if="currentCsr.title!== 'Initiate Verifications'"><spring:message code="indportal.portalhome.csrsubmissionsucceed" javaScriptEscape="true"/></h3>
			        <h3 ng-if="currentCsr.title=== 'Initiate Verifications'"><spring:message code="indportal.portalhome.csrprocesssubmitted" javaScriptEscape="true"/></h3>
			    		<p> <spring:message code="indportal.portalhome.csroktogoback" javaScriptEscape="true"/></p>
			    	  </div>

			       	  <div class="modal-footer">
			          	<a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/></a>
			          </div>
					</div>

					<div ng-if = "modalForm.subResult == 'failure'">
					  <div class="modal-body">
			    		<h3> <spring:message code="indportal.portalhome.csrsubmissionfailtitle" javaScriptEscape="true"/></h3>
			    		<p> <spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p>
						<p>	<spring:message code="indportal.portalhome.csrsubmissionfailcontent2" javaScriptEscape="true"/></p>
			    	  </div>

			       	  <div class="modal-footer">
			          	<a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()">OK</a>
			          </div>
					</div>

        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
    </div>
  </div>
  <!-- CSR Modal-->

	<!-- Eligibility Result modal -->
	<div ng-show="eligResultDialog">
	<div modal-show="eligResultDialog" id="aid_eligResultDialog" class="modal fade eligResultDialog" tabindex="-1" role="dialog" aria-labelledby="aid_eligResultDialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			    <div class="modal-header">
			        <button type="button" id="aid_eligResultDialogClose" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			    </div>
			    <div class="modal-body">
					<spring:message code="indportal.eligibilitysummary.noresultsfound" javaScriptEscape="true"/>
				</div>
			</div>
			<div class="modal-footer">
			    <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.eligibilitysummary.close" javaScriptEscape="true"/></a>
			</div>
		</div><!-- /.modal-content-->
	</div> <!-- /.modal-dialog-->
	</div>

		<!-- Modal for Override History-->
		<div ng-show= "modalForm.ORHist">
	    <div modal-show="modalForm.ORHist" class="modal fade">
		    <div class="modal-dialog">
		      <div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						  <h3 id="myModalLabel"><spring:message code="indportal.portalhome.overridehistory" javaScriptEscape="true"/></h3>
						</div>
						<div class="modal-body">

              <div ng-if="modalForm.oComments.length === 0">
                <spring:message code="indportal.portalhome.nohistory" javaScriptEscape="true"/>
              </div>

							<div class="well-step gutter10 nomargin" ng-repeat= "record in modalForm.oComments | orderBy:'commentedDate':true">
		          	<div>
		          	  <span class="commentdetail"><strong>{{record.commenterName}}</strong> <spring:message code="indportal.portalhome.addedcomment" javaScriptEscape="true"/>  <i>{{record.commentedDate | date:'medium'}}</i></span>
									<br>
		       				<span>{{record.overrideComment}}</span>
								</div>
          		</div>
          </div>

			    	<div class="modal-footer">
			      	<button class="btn pull-left" data-dismiss="modal">Close</button>
			      </div>
		    	</div><!-- /.modal-content-->
		    </div> <!-- /.modal-dialog-->
	  	</div>
		</div>
		<!-- Modal for Override History-->


<div id="overrideSepModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="overrideSepModalHeader" aria-hidden="true">
  <div class="modal-header" id="overrideSepModalHeader">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="myModalLabel">Override Special Enrollment Details</h3>
  </div>

  <form name="sepEventDetailsForm" id="aid_sepEventDetailsForm" class="form-horizontal" ng-show="isSepEventDetailsForm === true" novalidate>
    <div class="modal-body">
      <p class="alert alert-info" id="aid_sepEventDetailsFormAlert">
		<spring:message code="indportal.specialEnrollment.helpText"/><a class="" target="_blank" href="${specialEnrollmentInfoUrl}">here.</a><spring:message code="indportal.specialEnrollment.helpText2"/>
	  </p>

		<div class="row-fluid header">
			<h4>
				<span class="span3">Members</span>
				<span class="span5">Event Type <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></span>
				<span class="span4">Event Date <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></span>
			</h4>
		</div>

		<div ng-repeat="applicant in sepDetails.applicants | orderBy: 'personId'">
			<div class="row-fluid gutter10-b" ng-repeat="event in applicant.events track by $index" ng-form="eventForm" ng-if="sepDetails.applicationType === 'SEP' || (sepDetails.applicationType === 'QEP' && applicant.personId === 1)">
				<div class="span3 gutter10-l">{{applicant.firstName}} {{applicant.middleName}} {{applicant.lastName}} <span ng-if="applicant.personId === 1">(Primary)</span></div>
				<div class="span5">
					<select class="input-xlarge" id="aid_sepEventDetailsEvent" name="eventType" ng-model="event.eventId" required ng-change="validateEventAndDate(eventForm, event, applicant)" ng-options='event.eventId as event.eventNameLabel for event in event.eventList'></select>
				</div>
				<div class="span4 input-append date" date-picker>
					<input type="text" name="eventDate" id="aid_sepEventDetailsEventDate" class="input-small eventDate" placeholder="mm/dd/yyyy" ng-model="event.eventDate" ui-mask="99/99/9999" model-view-value="true" required sep-date-validation="date" ng-change="eventDateValidation(eventForm, event, applicant)">
					<span class="add-on"><i class="icon-calendar"></i></span>
				</div>
				<div ng-if="eventForm.$invalid">
					<div class="span3"></div>
					<div class="span9 sep-override-error-block">
						<div ng-if="!eventForm.eventDate.$error.required">
							<div id="aid_sepEventDetailsDateFormat" class="error-message block" ng-if="eventForm.eventDate.$error.date">
								Please enter a valid event date.
							</div>

							<div id="aid_sepEventDetailsDateSameYear"class="error-message block" ng-if="!eventForm.eventDate.$error.date && eventForm.eventDate.$error.sameYear">
								Please enter a valid event date based on the application year.
							</div>

							<div ng-if="!eventForm.eventDate.$error.date">
								<div id="aid_sepEventDetailsDatePastDays" class="error-message block" ng-if="eventForm.eventDate.$error.withinPast60Days">
									Event date should be within 60 days in the past.
								</div>
								<div id="aid_sepEventDetailsDatewithinFuture60Days" class="error-message block" ng-if="eventForm.eventDate.$error.withinFuture60Days">
									Event date should be within 60 days in the future.
								</div>
								<div id="aid_sepEventDetailsDatewithin60Days" class="error-message block" ng-if="eventForm.eventDate.$error.within60Days">
									Event date should be within 60 days in the past or future.
								</div>
							</div>
						</div>
						<div class="error-message block" ng-if="eventForm.eventDate.$error.required" >
							Please enter an event date.
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="error-message block margin10-t" ng-if="sameEventIsSelected">
			Member cannot report same event multiple times.
		</div>

    </div>
    <div class="modal-footer">
	     <button class="btn btn-primary" ng-click="getSepDateDetails()" ng-if="isSepEventDetailsForm === true" ng-disabled="sepEventDetailsForm.$invalid || sameEventIsSelected">Continue</button>
       <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
     </div>
  </form>

  <form name="sepDateDetailsForm" id="aid_sepDateDetailsForm" class="form-horizontal" ng-show="isSepDateDetailsForm === true" novalidate>
    <div class="modal-body">
	  <p id="aid_sepDateDetailsFormAlert" class="alert alert-info">
		Please confirm dates for the Special Enrollment Period.
	  </p>
      <div class="header margin20-tb">
        <h4 id="aid_sepDateDetailsFormHeader">SEP Enrollment Period</h4>
      </div>
      	<div class="control-group gutter10-lr">
			<div class="row-fluid">
	       	 <label class="span6">
    	    	  Start Date <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
        	 	 <span class="input-append date" date-picker>
            		<input type="text" id="aid_sepDateDetailsFormDate" class="input-small" name="sepStartDate" ng-model="sepDetails.sepStartDate" placeholder="mm/dd/yyyy" ui-mask="99/99/9999" model-view-value="true" sep-date-validation="date" required ng-change="validateStartBeforeEnd()">
	           	 <span class="add-on"><i class="icon-calendar"></i></span>
    	     	 </span>
        		</label>

	      	  <label class="span6">
    	  	    End Date <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
        		  <span class="input-append date" date-picker>
           	 	<input type="text" id="aid_sepDateDetailsFormEndDate" class="input-small" name="sepEndDate" ng-model="sepDetails.sepEndDate" placeholder="mm/dd/yyyy" ui-mask="99/99/9999" model-view-value="true" sep-date-validation="date_future" required ng-change="validateStartBeforeEnd()">
  	        	  <span class="add-on"><i class="icon-calendar"></i></span>
   	      	 </span>
     	   </label>
	</div>

		<div id="aid_sepDateDetailsFormFormatError" class="error-message block margin10-t" ng-if="sepDateDetailsForm.sepStartDate.$error.required">
				Please enter a special enrollment start date.
		</div>
		<div id="aid_sepDateDetailsFormStartDate" class="error-message block margin10-t" ng-if="!sepDateDetailsForm.sepStartDate.$error.required && sepDateDetailsForm.sepStartDate.$error.date">
				Please enter a valid special enrollment start date.
		</div>
		<div id="aid_sepDateDetailsFormEndDate" class="error-message block margin10-t" ng-if="sepDateDetailsForm.sepEndDate.$error.required">
				Please enter a special enrollment end date.
		</div>
		<div ng-if="!sepDateDetailsForm.sepEndDate.$error.required">
			<div id="aid_sepDateDetailsFormNoEndDate" class="error-message block margin10-t" ng-if="sepDateDetailsForm.sepEndDate.$error.date">
					Please enter a valid special enrollment end date.
			</div>
			<div id="aid_sepDateDetailsFormFutureDate" class="error-message block margin10-t" ng-if="sepDateDetailsForm.sepEndDate.$error.future">
					Special enrollment end date is in the past. Please enter a future date.
			</div>
		</div>
		<div id="aid_sepDateDetailsFormSEPEndDate" class="error-message block margin10-t" ng-if="!sepDateDetailsForm.sepStartDate.$error.date && !sepDateDetailsForm.sepEndDate.$error.date && sepDateDetailsForm.sepEndDate.$error.startBeforeEnd">
				Special enrollment start date cannot be after special enrollment end date.
		</div>
      </div>


      <div class="margin50-t">
        <div class="header margin10-b">
          <h4 id="aid_sepOverrideReasonHeader">Override Reason</h4>
        </div>

        <div class="gutter10-lr">
          <p>
            Please specify the reason for overriding the special enrollment for this household.
            <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
          </p>

          <div class="control-group">
            <textarea aria-label="change reason" id="aid_sepOverrideReason" class="override-reason" ng-model="sepDetails.overrideCommentText" placeholder="Please explain the change you are making and why" maxlength="4000" required></textarea>
            <span>{{4000 - sepDetails.overrideCommentText.length}} characters left</span>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
    	<button id="aid_backToSepDetails" class="btn pull-left" ng-click="backToSepDetails()">Back</button>
      <button id="aid_saveSepOverride" class="btn btn-primary" data-dismiss="modal" ng-click="saveSepOverride()" ng-disabled="sepDateDetailsForm.$invalid">Save</button>
      <button id="aid_SepOverrideCancel" class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </div>
  </form>
</div>


<div id="overrideStartDateModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="overrideStartDateModalLabel" aria-hidden="true">
  <div class="modal-header">
		<button type="button" id="aid_enrollmentEndDate" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3 id="overrideStartDateModalLabel">
			<spring:message code="indportal.portalhome.overrideEligibilityStartDate" javaScriptEscape="true"/>
		</h3>
	</div>
	<form class="form-horizontal" name="overrideEligibilityStartDateForm">
		<div class="modal-body">
			<p>The consumer is enrolled with the following dates:</p>

			<div class="row-fluid">
				<span id="aid_nrollmentStartDate" class="span5 txt-right">Enrollment Begin Date: </span> <strong class="span7">{{overrideEligibilityStartDateData.enrollmentStartDate}}</strong>
			</div>
			<div class="row-fluid">
				<span id="aid_enrollmentEndDate" class="span5 txt-right">Enrollment End Date: </span> <strong class="span7">{{overrideEligibilityStartDateData.enrollmentEndDate}}</strong>
			</div>

			<div ng-if="overrideEligibilityStartDateData.status === 'OVERRIDDEN' || overrideEligibilityStartDateData.status === 'REQUIRED'">
			<div class="control-group margin10-t">
				<label id="aid_eligibilityStartDateLbl" class="control-label">Eligibility Start Date</label>
				<div class="controls">
					<input type="text" id="aid_eligibilityStartDate" class="input-small" placeholder="mm/dd/yyyy" name="eligibilityStartDate" ui-mask="99/99/9999" required sep-date-validation="date_sameCoverageYear_eligibilityStartDate_firstDate" model-view-value="true" ng-model="overrideEligibilityStartDateData.financialEffectiveDate">
				</div>
				<div ng-if="!overrideEligibilityStartDateForm.eligibilityStartDate.$error.required">
					<div id="aid_eligibilityStartDateFormat" class="error-message margin50-l" ng-if="overrideEligibilityStartDateForm.eligibilityStartDate.$error.date">
						Please enter a valid eligibility start date.
					</div>
					<div id="aid_eligibilityStartDatefirstDate" class="error-message" ng-if="overrideEligibilityStartDateForm.eligibilityStartDate.$error.firstDate">
						Eligibility start date should be the 1st day of the month.
					</div>
					<div id="aid_sameCoverageYear" class="error-message" ng-if="overrideEligibilityStartDateForm.eligibilityStartDate.$error.sameCoverageYear">
						The year of eligibility start date should be the same as coverage year.
					</div>
					<div id="aid_eligibilityStartDateEndDate" class="error-message" ng-if="overrideEligibilityStartDateForm.eligibilityStartDate.$error.eligibilityStartDate">
						Eligibility start date should not be greater than enrollment end date.
					</div>
				</div>
				<div id="aid_eligibilityStartDateEmpty" class="error-message margin50-l" ng-if="overrideEligibilityStartDateForm.eligibilityStartDate.$error.required">
					Please enter an eligibility start date.
				</div>
			</div>

			<div>
          		<p class="margin30-t">
            		Please specify the reason for overriding the APTC eligibility start date for this household.
           		 	<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
          		</p>

				<div class="control-group">
            		<textarea class="override-reason" ng-model="overrideEligibilityStartDateData.comments" placeholder="Please explain the change you are making and why" maxlength="4000" required></textarea>
            		<span>{{4000 - overrideEligibilityStartDateData.comments.length}} characters left</span>
          		</div>
			</div>

			<p class="alert alert-danger">
					<spring:message code="indportal.portalhome.esdBeforeEnmtBeginDateNote" htmlEscape="false"/>
			</p>
			</div>



		    <div class="row-fluid margin10-t" ng-if="overrideEligibilityStartDateData.status === 'NOTREQUIRED' || overrideEligibilityStartDateData.status === 'VALID'">
				<span class="span5 txt-right">Eligibility Start Date: </span> <strong class="span7">{{overrideEligibilityStartDateData.financialEffectiveDate}}</strong>
			</div>

		</div>
		<div class="modal-footer">
			<button class="btn btn-primary" data-dismiss="modal" ng-disabled="overrideEligibilityStartDateForm.$invalid" ng-if="overrideEligibilityStartDateData.status === 'OVERRIDDEN' || overrideEligibilityStartDateData.status === 'REQUIRED'" ng-click="saveOverrideEligibilityDate()">Approve</button>
			<button class="btn" data-dismiss="modal" ng-click="cancelCsrAction()">Cancel</button>
		</div>
	</form>
</div>

<div id="overrideProgEligModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="overrideProgEligModalHeader" aria-hidden="true" style="width: 95%;">
  <div class="modal-header" id="overrideSepModalHeader">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="myModalLabel" ng-show="!cnfOvrProgEligModal" >Override Program Eligibility</h3>
    <h3 id="myModalLabel" ng-show="cnfOvrProgEligModal" >Override Program Eligibility Confirmation</h3>
  </div>

  <form name="progELigForm" id="aid_progELigFormForm" class="form-horizontal" novalidate>
  	<div id="editProgElig" ng-show="!cnfOvrProgEligModal">
	    <div class="modal-body">
			<div class="row-fluid">
				<h5>
					Values shown on this page show the consumers and any dependant's current eligibility. Use the fields and dropdowns to override. Changes made here will update the eligibility for the household. Be aware that this override will only affect the current year's application - it will be overwritten the next time eligibility is run. For non-financial applications, the CSR eligibility is applicable only for AI/AN members.
				</h5>
				<span class="span5" ng-if="programEligibility.financialAssistanceFlag === 'Y'">
					<label class="inlineLabel">Household Maximum APTC Amount</label>
					<input type="text" id="hhMaxAPTC" name="eligMaxAPTC"  ng-value={{programEligibility.maximumAPTC}} class="input-small" ng-model="programEligibility.maximumAPTC" required sep-date-validation="required_valid"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" model-view-value="true"></input>
					<span id="aid_eligStartDateEmpty_OPE" class="error-message margin50-l" ng-show="!programEligibility.maximumAPTC">Please Enter Maximum Valid APTC Amount >= 0</span>
				</span>
				<span class="span5">
					<label class="inlineLabel">Eligibility<span ng-if="programEligibility.financialAssistanceFlag === 'Y'"> (APTC & CSR)</span> Start Date</label>
					<div class="input-append date" date-picker>
							<input type="text" id="aid_eligStartDateDate" class="input-small eventDate" placeholder="mm/dd/yyyy" name="eligStartDate" ui-mask="99/99/9999" required sep-date-validation="date_required" model-view-value="true" ng-model="programEligibility.eligibilityStartDate" ng-change="eligibilityStartDateValidation(programEligibility, progELigForm)"></input>
							<span class="add-on"><i class="icon-calendar"></i></span>
					</div>
					<div id="aid_eligStartDateEmpty_OPE" class="error-message margin50-l" ng-if="progELigForm.eligStartDate.$error.required">
							Please enter an eligibility start date.
					</div>
					<div id="aid_eligStartDateSameYear_OPE" class="error-message margin50-l" ng-if="!progELigForm.eligStartDate.$error.date && progELigForm.eligStartDate.$error.sameYear">
							Enter Date between 01/01/{{maCurrentCoverageYear}} to 12/31/{{maCurrentCoverageYear}}.
					</div>					
					<div ng-if="!progELigForm.eligStartDate.$error.required">
						<span id="aid_eligibilityStartDateFormat_OPE" class="error-message margin50-l" ng-if="progELigForm.eligStartDate.$error.date">
							Please enter a valid eligibility start date.
						</span>
					</div>					
				</span>
			</div>
			<br />
			<table class="table table-borderless">
				<thead>
					<tr>
						<td>Member Name</div>
						<td>Exchange Eligibility</div>
						<td ng-if="programEligibility.financialAssistanceFlag === 'Y'">APTC Eligibility</td>
						<td>CSR Eligibility</div>
						<td ng-if="programEligibility.financialAssistanceFlag === 'Y'">Medicaid Eligibility</div>
						<td ng-if="programEligibility.financialAssistanceFlag === 'Y'">CHIP Eligibility</div>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="applicant in programEligibility.applicantEligibility | orderBy: 'id'">
						<td>{{applicant.firstName}} {{applicant.middleName}} {{applicant.lastName}} <span ng-if="applicant.personId === 1">(Primary)</span></td>
						<td>
							<select class="input-small" id="aid_exchangeElig" name="exchangeElig" ng-model="applicant.exchangeEligibility" required ng-options='obj.name as obj.name for obj in yesno'></select>
						</td>
						<td ng-if="programEligibility.financialAssistanceFlag === 'Y'">
							<select class="input-small" id="aid_aptcElig" name="aptcElig" ng-model="applicant.aptcEligibility" required ng-options='obj.name as obj.name for obj in yesno'></select>
						</td>
						<td>
							<select class="input-medium" ng-if="applicant.nativeAmericanFlag === 'Y'" id="aid_csrElig" name="csrElig" ng-model="applicant.csrLevel" required ng-options='obj.id as obj.name for obj in csrvaluesIfNativeAmerican'></select>
							<select class="input-medium" ng-if="applicant.nativeAmericanFlag === 'N'" id="aid_csrElig" name="csrElig" ng-model="applicant.csrLevel" required ng-options='obj.id as obj.name for obj in csrvaluesIfNotNativeAmerican'></select>
						</td>
						<td ng-if="programEligibility.financialAssistanceFlag === 'Y'">
							{{applicant.medicaidEligibility}}
						</td>
						<td ng-if="programEligibility.financialAssistanceFlag === 'Y'">
							{{applicant.chipEligibility}}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	    <div class="modal-footer">
	    	<button class="btn btn-primary" ng-click="confirmOverrideProgElig()" ng-disabled="progELigForm.$invalid"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></button>
	       	<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
		</div>
	</div>
	<div id="confirmProgElig" ng-show="cnfOvrProgEligModal">
		<div class="modal-body">
			<div class="row-fluid">
				<h5>
					Please confirm the following eligibility for this consumer on any dependants
				</h5>
				<div class="span5" ng-if="programEligibility.financialAssistanceFlag === 'Y'">
					<label class="inlineLabel">Household Maximum APTC Amount</label>
					<input type="text" id="hhMaxAPTC" class="input-small" readonly ng-model="programEligibility.maximumAPTC"></input>
				</div>
				<div class="span5">
					<label class="inlineLabel">Eligibility<span ng-if="programEligibility.financialAssistanceFlag === 'Y'"> (APTC & CSR)</span> Start Date</label>
					<div class="input-append date" date-picker>
							<input type="text" name="eligStartDate" id="aid_eligStartDateDate" readonly class="input-small eventDate" placeholder="mm/dd/yyyy" ng-model="programEligibility.eligibilityStartDate" ui-mask="99/99/9999" model-view-value="true" required sep-date-validation="date">
					</div>
				</div>
			</div>
			<br />
			<table class="table table-borderless">
				<thead>
					<tr>
						<td>Member Name</div>
						<td>Exchange Eligibility</div>
						<td ng-if="programEligibility.financialAssistanceFlag === 'Y'">APTC Eligibility</div>
						<td>CSR Eligibility</div>
						<td ng-if="programEligibility.financialAssistanceFlag === 'Y'">Medicaid Eligibility</div>
						<td ng-if="programEligibility.financialAssistanceFlag === 'Y'">CHIP Eligibility</div>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="applicant in programEligibility.applicantEligibility | orderBy: 'id'">
						<td>{{applicant.firstName}} {{applicant.middleName}} {{applicant.lastName}} <span ng-if="applicant.personId === 1">(Primary)</span></td>
						<td>{{applicant.exchangeEligibility}}</td>
						<td ng-if="programEligibility.financialAssistanceFlag === 'Y'">{{applicant.aptcEligibility}}</td>
						<td>
							<span ng-if="(applicant.csrLevel=='CS2')">CS Level 2</span>
							<span ng-if="(applicant.csrLevel=='CS3')">CS Level 3</span>
							<span ng-if="(applicant.csrLevel=='CS4')">CS Level 4</span>
							<span ng-if="(applicant.csrLevel=='CS5')">CS Level 5</span>
							<span ng-if="(applicant.csrLevel=='CS6')">CS Level 6</span>
							<span ng-if="(applicant.csrLevel=='No')">No</span>
						
						</td>
						<td ng-if="programEligibility.financialAssistanceFlag === 'Y'">
							{{applicant.medicaidEligibility}}
						</td>
						<td ng-if="programEligibility.financialAssistanceFlag === 'Y'">
							{{applicant.chipEligibility}}
						</td>					
					</tr>
				</tbody>
			</table>
			<div id="orPrElReason">
				<span class="oRReason">
					Provide a reason for overriding the program eligibility and then click submit if everything is correct.
					<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
				</span>
	            <textarea aria-label="change reason" class="overrideText" ng-model="modalForm.overrideComment" placeholder="Please explain the change you are making and why"></textarea>
	            <br>
	            <div>
	                <span class="pull-left">{{maxLength - modalForm.overrideComment.length}} <spring:message code="indportal.portalhome.charactersleft" javaScriptEscape="true"/></span>
				</div>
			</div>
		</div>
	    <div class="modal-footer">
	    	<button class="btn btn-primary" ng-click="backToOverrideProgElig()" ng-disabled="isProgEligForm.$invalid">Back</button>
	    	<button class="btn btn-primary" ng-click="modalForm.overrideComment.length > 0 && submitOverrideProgElig(programEligibility,modalForm.overrideComment)" ng-disabled="modalForm.overrideComment.length < 1">Submit</button>
	       	<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	     </div>
     </div>
  </form>
</div>



<div id="errorModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="errorModalLabel" aria-hidden="true">
  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3 id="errorModalLabel">
			<spring:message code="indportal.portalhome.technicalissue" javaScriptEscape="true"/>
		</h3>
	</div> 
	<div class="modal-body">
    <p><spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p> 
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>


<div id="successModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="successModal" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3 id="successModalLabel" ng-if="successModalTitle">
			{{successModalTitle}}
		</h3>
	</div> 
	<div class="modal-body">
    	<h3><spring:message code="indportal.portalhome.csrsubmissionsucceed" javaScriptEscape="true"/></h3>
		<p><spring:message code="indportal.portalhome.csroktogoback" javaScriptEscape="true"/></p>
  	</div>
  	<div class="modal-footer">
  		<a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.ok" /></a>
	</div>
			</div>
		</div>

		
	</div>
  </div>
</div>
