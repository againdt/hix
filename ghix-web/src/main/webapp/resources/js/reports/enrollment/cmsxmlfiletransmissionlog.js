var app = angular.module('cmsXmlFileTransmissionLog', ['ui.grid','ui.grid.exporter', 'ui.bootstrap','ui.router', 'ui.grid.resizeColumns', 'ui.grid.pagination']);


app.controller('CMSXMLFileTransmissionCtrl', ['$scope', '$filter', '$timeout', 'CMSXmlFileTransmissionService', 'uiGridConstants','uiGridExporterConstants','uiGridExporterService', function ($scope,$filter,$timeout,CMSXmlFileTransmissionService, uiGridConstants,uiGridExporterConstants,uiGridExporterService) {
	var paginationOptions = {
			pageNumber: 1,
			pageSize: 25,
			sort: null
	};
	
	$scope.colDef = [];
	$scope.externalConfig = {};
	$scope.dropDownColumns = [];
	$scope.data = null;
	
	
	var displayName = {id : 'ID', fileName: 'File Name', fileSize : 'File Size(mb)', 
			reportType : 'Report Type', direction : 'Direction',
			batchExecutionId : 'Batch Execution Id', status : 'Status', errorDescription : 'Error Description',
			creationTimestamp : 'Created On'};
	
	var selectOptions = function(data) {
		var selectOption = [];
		if(data != null){
			for (var i=0; i<data.length; i++) {
				selectOption.push({ value: data[i], label : data[i]});
			}
		}
		return selectOption;
	};
	
	var setPropertiesFn = function (elem, key) {
		var filter = { type: uiGridConstants.filter.SELECT, condition : uiGridConstants.filter.EXACT };
		
        switch (key) {
            case 'reportType':
            	elem.filter = filter;
            	elem.filter.selectOptions = selectOptions(['IRS', 'PLR', 'CMS', 'ANNUAL']);
            	$scope.dropDownColumns.push(key);
            	break;
            case 'direction':
            	elem.filter = filter;
            	elem.filter.selectOptions = selectOptions(['UPLOAD', 'DOWNLOAD']);
            	$scope.dropDownColumns.push(key);
                break;
            case 'creationTimestamp':
            	elem.enableFiltering = false;
            case 'errorDescription':
            	elem.enableFiltering = false;
            	elem.enableSorting = false;
            default:
            	//elem.enableSorting = true;
            	//elem.enableFiltering = false;
//            	if(key != 'sbmsSbmrRcvd' && key != 'missingPolicyIds' && key != 'skipRecords' && key != 'fileName')
//            		elem.enableFiltering = false;
//            	if(key != 'hoisIssuerId')
//            		elem.enableSorting = false;

        }
    };
	
	CMSXmlFileTransmissionService.getReport(paginationOptions.pageNumber,
			paginationOptions.pageSize, null).success(function(data){
				$scope.gridOptions.data = data.content;
				$scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
				
				if(data.size != 0) {
					angular.forEach(data.content[0], function(value, key) {
						var elem = {name:key};
						setPropertiesFn(elem,key);
						$scope.colDef.push(elem);
					});
					
					$scope.colDef.push({ name: 'creationTimestamp', displayName: 'Creation time', enableFiltering: false, cellFilter: 'date:\'yyyy-MM-dd HH:mm\'', width: 200});
				}
			});
	
	$scope.gridOptions = {
		paginationPageSizes: [25, 50, 75, 100],
		paginationPageSize: paginationOptions.pageSize,
		useExternalPagination: true,
		columnDefs:  $scope.colDef,
//		columnDefs: [
//		    { field: 'creationTimestamp', displayName: 'Created On', cellFilter: 'date:\'yyyy-MM-dd HH:mm\'' }],
		enableFiltering: true,
		enableGridMenu: true, 
		useExternalFiltering: true,
		useExternalSorting: true,
		exporterMenuPdf: false,
		exporterMenuCsv: false,
		exporterPdfFilename: 'download.pdf',
		exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
		onRegisterApi: function(gridApi) {
			$scope.gridApi = gridApi;
			
			$scope.gridApi.core.on.filterChanged( $scope, function() {
                var grid = this.grid;
                var filterExact = {}, filterLike = {};
                angular.forEach(grid.columns, function (column, index) {
                        if (column.filters && column.filters[0].term) {
                                if($scope.dropDownColumns.indexOf(column.field) !== -1)
                                        filterExact[column.field] = column.filters[0].term;
                                else
                                        filterLike[column.field] = column.filters[0].term;
                        }
                });
                $scope.externalConfig.filterExact = filterExact;
                $scope.externalConfig.filterLike = filterLike;
                CMSXmlFileTransmissionService.getReport(paginationOptions.pageNumber, paginationOptions.pageSize, $scope.externalConfig).success(function(data){
                                      $scope.data = $scope.gridOptions.data = data.content;
                                      $scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
                              });
               });
              
              $scope.gridApi.core.on.sortChanged( $scope, function(grid, sortColumns){
                      var grid = this.grid;
                var asc = [], desc = [];
                angular.forEach(grid.columns, function (column, index) {
                        if (column.sort && column.sort.direction) {
                                if(column.sort.direction == uiGridConstants.ASC)
                                        asc.push(column.field);
                                else if(column.sort.direction == uiGridConstants.DESC)
                                        desc.push(column.field);
                        }
                    });
                $scope.externalConfig[uiGridConstants.ASC] = asc;
                $scope.externalConfig[uiGridConstants.DESC] = desc;
                CMSXmlFileTransmissionService.getReport(paginationOptions.pageNumber, paginationOptions.pageSize, $scope.externalConfig).success(function(data){
                                      $scope.data = $scope.gridOptions.data = data.content;
                                      $scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
                              });
              });
              
			gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
				paginationOptions.pageNumber = newPage;
				paginationOptions.pageSize = pageSize;
				CMSXmlFileTransmissionService.getReport(newPage,pageSize, $scope.externalConfig).success(function(data){
					$scope.data = $scope.gridOptions.data = data.content;
					$scope.totalElem = $scope.gridOptions.totalItems = data.totalElements;
				});
			});
		}
	};
	
	$scope.gridOptions.gridMenuCustomItems= [
        {
       	 title:'Export all data as CSV',
            action: function ($event) {
            	CMSXmlFileTransmissionService.getReport(1,$scope.gridOptions.totalItems,null).success(function(data) {
                    $scope.gridOptions.totalItems = data.totalElements;
                    $scope.gridOptions.data = data.content;
                    $timeout(function()
                            {
                                var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
                                $scope.gridApi.exporter.csvExport( uiGridExporterConstants.ALL, uiGridExporterConstants.ALL, myElement );
                                $scope.gridOptions.data =  $scope.data;
                                $scope.gridOptions.totalItems = $scope.totalElem;
                            }, 1000);
                    

                    });
            },
            order:0
          },
          {
           	 title:'Export all data as PDF',
                action: function ($event) {
                	CMSXmlFileTransmissionService.getReport(1,$scope.gridOptions.totalItems,null).success(function(data) {
                        $scope.gridOptions.totalItems = data.totalElements;
                        $scope.gridOptions.data = data.content;
                        $timeout(function()
                                {
                                    $scope.gridApi.exporter.pdfExport( uiGridExporterConstants.ALL, uiGridExporterConstants.ALL );
                                    $scope.gridOptions.data =  $scope.data;
                                    $scope.gridOptions.totalItems = $scope.totalElem;
                                }, 1000);
                        

               	});
                },
                order:0
          }
         ];
	
}]);

app.service('CMSXmlFileTransmissionService',['$http', function ($http) {

	function getReport(pageNumber,size,requestdata) {
		return  $http({
			method: 'POST',
			url: '/hix/admin/reports/cmsfiletransmissionlogs?pageNumber='+pageNumber+'&pageSize='+size,
            dataType: "json",
            headers: {
                'Content-Type': "application/json",
                "csrftoken": $('#tokid').val()
            },
            data: JSON.stringify(requestdata)
		});
	}
	return {
		getReport:getReport
	};

}]);