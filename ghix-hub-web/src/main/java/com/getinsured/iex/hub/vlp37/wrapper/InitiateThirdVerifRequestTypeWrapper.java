package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.platform.FDSHDataTypeWrapper;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv.InitiateThirdVerifRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv.ObjectFactory;


public class InitiateThirdVerifRequestTypeWrapper implements JSONAware, FDSHDataTypeWrapper{
	
	private static ObjectFactory factory = null;
	
	private InitiateThirdVerifRequestType request = null;
	
    private String caseNumber;
    private String alienNumber;
    private String i94Number;
    private PassportCountryWrapper passportCountry;
    private String receiptNumber;
    private String aka;
    private String comments;
    private boolean requestSponsorDataIndicator;
    private boolean requestGrantDateIndicator;
    private String casePOCFullName;
    private String casePOCPhoneNumber;
    private String casePOCPhoneNumberExtension;
    private Boolean suspectedCounterfeitAlteredDocumentIndicator;
    private Boolean requestCubanHaitianEntrantIndicator;
    private String casePOCAddress1;
    private String casePOCAddress2;
    private String casePOCCity;
    private String casePOCState;
    private String casePOCZipCode;
    private byte[] documentBinaryAttachment;

		
    private static final String	CASE_NUMBER_KEY =  "CaseNumber";
    private static final String	ALIEN_NUMBER_KEY = "AlienNumber";
    private static final String	I94_NUMBER_KEY = "I94Number";
    private static final String	PASSPORT_COUNTRY_KEY = "PassportCountry";
    private static final String	RECEIPT_NUMBER_KEY = "ReceiptNumber";
    private static final String	AKA_KEY = "AKA";
    private static final String	COMMENTS_KEY = "Comments";
    private static final String	REQUESTSPONSORDATAINDICATOR_KEY = "RequestSponsorDataIndicator";
    private static final String	REQUESTGRANTDATEINDICATOR_KEY = "RequestGrantDateIndicator";
    private static final String	CASE_POC_FULLENAME_KEY= "CasePOCFullName";
    private static final String	CASE_POC_PHONE_NUMBER_KEY = "CasePOCPhoneNumber";
    private static final String	CASE_POC_PHONE_NUMBER_EXT_KEY = "CasePOCPhoneNumberExtension";
    private static final String	SUSPECTEDCOUNTERFEITALTEREDDOCUMENTINDICATOR_KEY = "SuspectedCounterfeitAlteredDocumentIndicator";
    private static final String	REQUESTCUBANHAITIANENTRANTINDICATOR_KEY = "RequestCubanHaitianEntrantIndicator";
    private static final String	CASEPOCADDRESS1_KEY = "CasePOCAddress1";
    private static final String	CASEPOCADDRESS2_KEY = "CasePOCAddress2";
    private static final String	CASEPOCCITY_KEY="CasePOCCity";
    private static final String	CASEPOCSTATE_KEY="CasePOCState";
    private static final String	CASEPOCZIPCODE_KEY="CasePOCZipCode";
    private static final String	DOCUMENTBINARYATTACHMENT_KEY="DocumentBinaryAttachment";


	
	
	public InitiateThirdVerifRequestTypeWrapper(){
		factory = new ObjectFactory();
		request = factory.createInitiateThirdVerifRequestType();
	}

	/**
	 * This method will also do the request specific validations
	 */
	public InitiateThirdVerifRequestType getRequest() throws HubServiceException {
		return this.request;
	}
	
	public String getAKA() {
		return aka;
	}

	public void setAKA(String aka) {
		this.aka = aka;
		request.setAKA(aka);
	}

	public String getAlienNumber() {
		return alienNumber;
	}

	public void setAlienNumber(String alienNumber) {
		this.alienNumber = alienNumber;
		request.setAlienNumber(alienNumber);
	}

	public String getI94Number() {
		return i94Number;
	}

	public void setI94Number(String i94Number) {
		this.i94Number = i94Number;
		request.setI94Number(i94Number);
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
		request.setReceiptNumber(receiptNumber);
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
		request.setComments(comments);
	}

	public boolean isRequestSponsorDataIndicator() {
		return requestSponsorDataIndicator;
	}

	public void setRequestSponsorDataIndicator(boolean requestSponsorDataIndicator) {
		this.requestSponsorDataIndicator = requestSponsorDataIndicator;
		request.setRequestSponsorDataIndicator(requestSponsorDataIndicator);
	}

	public boolean isRequestGrantDateIndicator() {
		return requestGrantDateIndicator;
	}

	public void setRequestGrantDateIndicator(boolean requestGrantDateIndicator) {
		this.requestGrantDateIndicator = requestGrantDateIndicator;
		request.setRequestGrantDateIndicator(requestGrantDateIndicator);
	}

	public String getCasePOCFullName() {
		return casePOCFullName;
	}

	public void setCasePOCFullName(String casePOCFullName) {
		this.casePOCFullName = casePOCFullName;
		request.setCasePOCFullName(casePOCFullName);
	}

	public String getCasePOCPhoneNumber() {
		return casePOCPhoneNumber;
	}

	public void setCasePOCPhoneNumber(String casePOCPhoneNumber) {
		this.casePOCPhoneNumber = casePOCPhoneNumber;
		request.setCasePOCPhoneNumber(casePOCPhoneNumber);
	}

	public String getCasePOCPhoneNumberExtension() {
		return casePOCPhoneNumberExtension;
	}

	public void setCasePOCPhoneNumberExtension(String casePOCPhoneNumberExtension) {
		this.casePOCPhoneNumberExtension = casePOCPhoneNumberExtension;
		request.setCasePOCPhoneNumberExtension(casePOCPhoneNumberExtension);
	}
	
	public Boolean getSuspectedCounterfeitAlteredDocumentIndicator() {
		return suspectedCounterfeitAlteredDocumentIndicator;
	}

	public void setSuspectedCounterfeitAlteredDocumentIndicator(Boolean suspectedCounterfeitAlteredDocumentIndicator) {
		this.suspectedCounterfeitAlteredDocumentIndicator = suspectedCounterfeitAlteredDocumentIndicator;
		request.setSuspectedCounterfeitAlteredDocumentIndicator(suspectedCounterfeitAlteredDocumentIndicator);
	}

	public Boolean getRequestCubanHaitianEntrantIndicator() {
		return requestCubanHaitianEntrantIndicator;
	}

	public void setRequestCubanHaitianEntrantIndicator(Boolean requestCubanHaitianEntrantIndicator) {
		this.requestCubanHaitianEntrantIndicator = requestCubanHaitianEntrantIndicator;
		request.setRequestCubanHaitianEntrantIndicator(requestCubanHaitianEntrantIndicator);
	}

	public String getCasePOCAddress1() {
		return casePOCAddress1;
	}

	public void setCasePOCAddress1(String casePOCAddress1) {
		this.casePOCAddress1 = casePOCAddress1;
		request.setCasePOCAddress1(casePOCAddress1);
	}

	public String getCasePOCAddress2() {
		return casePOCAddress2;
	}

	public void setCasePOCAddress2(String casePOCAddress2) {
		this.casePOCAddress2 = casePOCAddress2;
		request.setCasePOCAddress2(casePOCAddress2);
	}

	public String getCasePOCCity() {
		return casePOCCity;
	}

	public void setCasePOCCity(String casePOCCity) {
		this.casePOCCity = casePOCCity;
		request.setCasePOCCity(casePOCCity);
	}

	public String getCasePOCState() {
		return casePOCState;
	}

	public void setCasePOCState(String casePOCState) {
		this.casePOCState = casePOCState;
		request.setCasePOCState(casePOCState);
	}

	public String getCasePOCZipCode() {
		return casePOCZipCode;
	}

	public void setCasePOCZipCode(String casePOCZipCode) {
		this.casePOCZipCode = casePOCZipCode;
		request.setCasePOCZipCode(casePOCZipCode);
	}

	public byte[] getDocumentBinaryAttachment() {
		return documentBinaryAttachment;
	}

	public void setDocumentBinaryAttachment(byte[] documentBinaryAttachment) {
		this.documentBinaryAttachment = documentBinaryAttachment;
		request.setDocumentBinaryAttachment(documentBinaryAttachment);
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
		request.setCaseNumber(caseNumber);
	}
	
	public PassportCountryWrapper getPassportCountry() {
		return passportCountry;
	}

	public void setPassportCountry(PassportCountryWrapper passportCountry) {
		this.passportCountry = passportCountry;
	//	request.setPassportCountry(passportCountry.getPassportCountryType());
	}

	public void setRequest(InitiateThirdVerifRequestType request) {
		this.request = request;
	}

	/**
	 * Converts the JSON String to Agency3InitVerifRequestTypeWrapper object
	 * 
	 * @param jsonString - String representation for Agency3InitVerifRequestTypeWrapper
	 * @return Agency3InitVerifRequestTypeWrapper Object 
	 */
	public static InitiateThirdVerifRequestTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I327DocumentID3Wrapper from null or empty input");
		}
	
		InitiateThirdVerifRequestTypeWrapper wrapper = new InitiateThirdVerifRequestTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		Object param = null;
		
		param = obj.get(CASE_NUMBER_KEY);
		if(param != null){
			wrapper.setCaseNumber((String)param);
		}
	
		param = obj.get(COMMENTS_KEY);
		if(param != null){
			wrapper.setComments((String)param);
		}
		
		param = obj.get(RECEIPT_NUMBER_KEY);
		if(param != null){
			wrapper.setReceiptNumber((String)param);
		}

		param = obj.get(ALIEN_NUMBER_KEY);
		if(param != null){
			wrapper.setAlienNumber((String)param);
		}

		param = obj.get(I94_NUMBER_KEY);
		if(param != null){
			wrapper.setI94Number((String)param);
		}
		
		param = obj.get(PASSPORT_COUNTRY_KEY);
		if(param != null){
			JSONObject passportCountry = (JSONObject)param;
			wrapper.setPassportCountry(PassportCountryWrapper.fromJSONObject(passportCountry));
		}

		param = obj.get(AKA_KEY);
		if(param != null){
			wrapper.setAKA((String)param);
		}
		
		param = obj.get(REQUESTSPONSORDATAINDICATOR_KEY);
		if(param != null){
			wrapper.setRequestSponsorDataIndicator((Boolean)param);
		}
		
		param = obj.get(REQUESTGRANTDATEINDICATOR_KEY);
		if(param != null){
			wrapper.setRequestGrantDateIndicator((Boolean)param);
		}
		
		param = obj.get(SUSPECTEDCOUNTERFEITALTEREDDOCUMENTINDICATOR_KEY);
		if(param != null){
			wrapper.setSuspectedCounterfeitAlteredDocumentIndicator((Boolean)param);
		}
		
		param = obj.get(REQUESTCUBANHAITIANENTRANTINDICATOR_KEY);
		if(param != null){
			wrapper.setRequestCubanHaitianEntrantIndicator((Boolean)param);
		}
		
		param = obj.get(CASEPOCADDRESS1_KEY);
		if(param != null){
			wrapper.setCasePOCAddress1((String)param);
		}
		
		param = obj.get(CASEPOCADDRESS2_KEY);
		if(param != null){
			wrapper.setCasePOCAddress2((String)param);
		}
		
		param = obj.get(CASEPOCCITY_KEY);
		if(param != null){
			wrapper.setCasePOCCity((String)param);
		}
		
		param = obj.get(CASEPOCSTATE_KEY);
		if(param != null){
			wrapper.setCasePOCState((String)param);
		}
		
		param = obj.get(CASEPOCZIPCODE_KEY);
		if(param != null){
			wrapper.setCasePOCZipCode((String)param);
		}
		
		param = obj.get(CASE_POC_FULLENAME_KEY);
		if(param != null){
			wrapper.setCasePOCFullName((String)param);
		}
		
		param = obj.get(CASE_POC_PHONE_NUMBER_KEY);
		if(param != null){
			wrapper.setCasePOCPhoneNumber((String)param);
		}
		
		param = obj.get(CASE_POC_PHONE_NUMBER_EXT_KEY);
		if(param != null){
			wrapper.setCasePOCPhoneNumberExtension((String)param);
		}
		
	    //wrapper.setDocumentBinaryAttachment((String) obj.get(DOCUMENTBINARYATTACHMENT_KEY));

		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(AKA_KEY, this.aka);
		obj.put(REQUESTSPONSORDATAINDICATOR_KEY, this.requestSponsorDataIndicator);
		obj.put(REQUESTGRANTDATEINDICATOR_KEY, this.requestGrantDateIndicator);
		obj.put(CASE_NUMBER_KEY, this.caseNumber);			
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);		
		obj.put(I94_NUMBER_KEY, this.i94Number	);		
		if(this.passportCountry != null){
			obj.put(PASSPORT_COUNTRY_KEY, this.passportCountry.toJSONObject());
		}
		obj.put(RECEIPT_NUMBER_KEY, this.receiptNumber	);	
		obj.put(COMMENTS_KEY, this.comments);			
		obj.put(CASE_POC_FULLENAME_KEY, this.casePOCFullName);
		obj.put(CASE_POC_PHONE_NUMBER_KEY, this.casePOCPhoneNumber	);
		obj.put(CASE_POC_PHONE_NUMBER_EXT_KEY, this.casePOCPhoneNumberExtension);
		
		obj.put(SUSPECTEDCOUNTERFEITALTEREDDOCUMENTINDICATOR_KEY , this.suspectedCounterfeitAlteredDocumentIndicator);
	    obj.put(REQUESTCUBANHAITIANENTRANTINDICATOR_KEY , this.requestCubanHaitianEntrantIndicator);
	    obj.put(CASEPOCADDRESS1_KEY , this.casePOCAddress1);
	    obj.put(CASEPOCADDRESS2_KEY , this.casePOCAddress2);
	    obj.put(CASEPOCCITY_KEY, this.casePOCCity);
	    obj.put(CASEPOCSTATE_KEY, this.casePOCState);
	    obj.put(CASEPOCZIPCODE_KEY, this.casePOCZipCode);
	    obj.put(DOCUMENTBINARYATTACHMENT_KEY, this.documentBinaryAttachment);

		return obj.toJSONString();

	}
	
	public InitiateThirdVerifRequestTypeWrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		
		InitiateThirdVerifRequestTypeWrapper wrapper = new InitiateThirdVerifRequestTypeWrapper();
		wrapper.setAKA((String)jsonObj.get(AKA_KEY));
		wrapper.setRequestSponsorDataIndicator(Boolean.parseBoolean((String)jsonObj.get(REQUESTSPONSORDATAINDICATOR_KEY)));
		wrapper.setRequestGrantDateIndicator(Boolean.parseBoolean((String)jsonObj.get(REQUESTGRANTDATEINDICATOR_KEY)));
		wrapper.setCaseNumber((String)jsonObj.get(CASE_NUMBER_KEY));
		
		wrapper.setAlienNumber((String)jsonObj.get(ALIEN_NUMBER_KEY));
		wrapper.setI94Number((String)jsonObj.get(I94_NUMBER_KEY));
		Object param = jsonObj.get(PASSPORT_COUNTRY_KEY);
		if(param != null){
			JSONObject passportCountry = (JSONObject)param;
			wrapper.setPassportCountry(PassportCountryWrapper.fromJSONObject(passportCountry));
		}
		wrapper.setReceiptNumber((String)jsonObj.get(RECEIPT_NUMBER_KEY));
		wrapper.setComments((String)jsonObj.get(COMMENTS_KEY));
		wrapper.setCasePOCFullName((String)jsonObj.get(CASE_POC_FULLENAME_KEY));
		wrapper.setCasePOCPhoneNumber((String)jsonObj.get(CASE_POC_PHONE_NUMBER_KEY));
		wrapper.setCasePOCPhoneNumberExtension((String)jsonObj.get(CASE_POC_PHONE_NUMBER_EXT_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(AKA_KEY, this.aka);
		obj.put(REQUESTSPONSORDATAINDICATOR_KEY, this.requestSponsorDataIndicator);
		obj.put(REQUESTGRANTDATEINDICATOR_KEY, this.requestGrantDateIndicator);
		obj.put(CASE_NUMBER_KEY, this.caseNumber);			
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);		
		obj.put(I94_NUMBER_KEY, this.i94Number	);		
		obj.put(PASSPORT_COUNTRY_KEY, this.passportCountry	);	
		obj.put(RECEIPT_NUMBER_KEY, this.receiptNumber	);	
		obj.put(COMMENTS_KEY, this.comments);			
		obj.put(CASE_POC_FULLENAME_KEY, this.casePOCFullName);
		obj.put(CASE_POC_PHONE_NUMBER_KEY, this.casePOCPhoneNumber	);
		obj.put(CASE_POC_PHONE_NUMBER_EXT_KEY, this.casePOCPhoneNumberExtension);
		obj.put(SUSPECTEDCOUNTERFEITALTEREDDOCUMENTINDICATOR_KEY , this.suspectedCounterfeitAlteredDocumentIndicator);
	    obj.put(REQUESTCUBANHAITIANENTRANTINDICATOR_KEY , this.requestCubanHaitianEntrantIndicator);
	    obj.put(CASEPOCADDRESS1_KEY , this.casePOCAddress1);
	    obj.put(CASEPOCADDRESS2_KEY , this.casePOCAddress2);
	    obj.put(CASEPOCCITY_KEY, this.casePOCCity);
	    obj.put(CASEPOCSTATE_KEY, this.casePOCState);
	    obj.put(CASEPOCZIPCODE_KEY, this.casePOCZipCode);
	    obj.put(DOCUMENTBINARYATTACHMENT_KEY, this.documentBinaryAttachment);
	    
		return obj;
	}

	@Override
	public InitiateThirdVerifRequestType getSystemRepresentation() {
		return request;
	}
}
