package com.getinsured.eligibility.benchmark.service;

import java.math.BigDecimal;

public class EligibilityBenchMarkServiceResponse {

	public enum ResponseCode {
		SUCCESS, FAILURE, WARNING
	}

	private String caseNumber;

	private BigDecimal benchMarkPremiumAmount;

	private ResponseCode responseCode;

	private String errorDescription;

	private String errorPrintStack;

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public BigDecimal getBenchMarkPremiumAmount() {
		return benchMarkPremiumAmount;
	}

	public void setBenchMarkPremiumAmount(BigDecimal benchMarkPremiumAmount) {
		this.benchMarkPremiumAmount = benchMarkPremiumAmount;
	}

	public ResponseCode getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(ResponseCode responseCode) {
		this.responseCode = responseCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getErrorPrintStack() {
		return errorPrintStack;
	}

	public void setErrorPrintStack(String errorPrintStack) {
		this.errorPrintStack = errorPrintStack;
	}



}
