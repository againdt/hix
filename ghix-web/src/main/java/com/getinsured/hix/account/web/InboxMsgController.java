package com.getinsured.hix.account.web;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.InboxMsg;
import com.getinsured.hix.model.InboxMsgDoc;
import com.getinsured.hix.model.InboxMsgResponse;
import com.getinsured.hix.model.InboxUserAddressBookResp;
import com.getinsured.hix.model.MessageEncryptor;
import com.getinsured.hix.platform.account.service.InboxMsgService;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Controller class for Secure Inbox
 *
 * @author Nikhil Talreja, Bhavin Parmar, Geetha Chandran
 * @since 10 December, 2012
 */
@Controller
public class InboxMsgController {

	private static final String CLASS_NAME = "InboxMsgController";
	private static final String RETURN_INBOX_HOME_PAGE = "inbox/home";
	private static final String REDIRECT_SECURE_INBOX_PAGE = "redirect:/inbox/secureInbox";
	private static final String REDIRECT_SECURE_INBOX_SENT_PAGE = "redirect:/inbox/secureInboxSent";
	private static final String REDIRECT_SECURE_INBOX_DRAFT_PAGE = "redirect:/inbox/secureInboxDraft";
	private static final String REDIRECT_SECURE_INBOX_ARCHIVE_PAGE = "redirect:/inbox/secureInboxArchive";
	private static final String REDIRECT_LOGIN_PAGE = "redirect:/account/user/login";
	private static final String INCLUDE_PAGE = "includePage";
	private static final String INCLUDE_INBOX_PAGE = "secureInbox.jsp";
	private static final String INCLUDE_SENT_PAGE = "secureInboxSent.jsp";
	private static final String INCLUDE_DRAFT_PAGE = "secureInboxDraft.jsp";
	private static final String INCLUDE_ARCHIVE_PAGE = "secureInboxArchive.jsp";
	private static final String INCLUDE_SEARCH_PAGE = "secureInboxSearch.jsp";
	private static final String INCLUDE_READ_PAGE = "secureInboxRead.jsp";
	private static final String FAILURE = "failure";
	private static final String SUCCESS = "success";
	private static final String INBOX_DATE_FORMAT = "MMMM dd, yyyy HH:mm a";

	// private static final String ERROR_MSG_MODEL = "errorMsg";
	private static final String INBOX_MSG_LIST_MODEL = "inboxMsgList";
	private static final String INBOX_MSG_LIST_SESSION = "inboxMsgSessionList";
	private static final String PAGE_SIZE = "pageSize";
	private static final String PAGE_NUMBER = "pageNumber";
	private static final String PAGE_ROWS = "pageRows";
	private static final String TOTAL_PAGES = "totalPages";
	private static final String TOTAL_RECORDS = "totalRecords";
	private static final String TOTAL_UNREAD_RECORDS = "totalUnreadRecords";
	private static final String SEARCH_TEXT = "searchText";
	private static final String PAGE_TITLE = "page_title";
	private static final String SORT_BY = "sortBy";
	private static final String SORT_ORDER = "sortOrder";
	private static final String CHANGE_ORDER = "changeOrder";
	private static final String MESSAGE_ATTRIBUTE = "message";
	private static final String DRAFT_MESSAGES = "Message Drafts";
	private static final String NONE = "(None)";

	// FIXME - Move this to configuration file.
	private static final int PAGE_SIZE_NUM = 10;

	private static final Logger LOGGER = LoggerFactory.getLogger(InboxMsgController.class);
	private static final String EXCEPTION_STRING = "Exception has occured : ";
	private static final String USER_EXCEPTION_STRING = "Invalid user";

	private static final String REQEUST_PARAMETER_ID = "id";
	private static final String REQEUST_PARAMETER_TO = "to";
	private static final String REQUEST_PARAMETER_SUBJECT = "subject";
	private static final String REQUEST_PARAMETER_BODY = "body";
	private static final String REQUEST_PARAMTER_NAMES = "names";
	private static final String REQEUST_PARAMETER_PREVIOUS_ID = "previousId";
	private static final String REQEUST_PARAMETER_PREVIOUS_STATUS = "status";
	// private static final String REQEUST_PARAMETER_FORWARDED_ATTACHMENTS = "forwardedAttachments";

	private static final int UNIT_SIZE = 1024;
	private static final int MAX_NO_OF_ATTACHMENTS = 5;

	private static final int MAX_MESSAGE_COUNT = 100;
//	private static final String DAILY_LIMIT_CROSSED = "OVERLIMIT";
	private static final String DAILY_LIMIT_MESSAGE = "Sorry ! your allowed to send/save only 100 messages per day";

	@Autowired private InboxMsgService inboxMsgService;
	@Autowired private UserService userService;
	@Autowired private ContentManagementService ecmService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

	private static final String ELIGIBILITY_STATUS_NEW = "New";

	@RequestMapping(value = "/inbox/demo", method = RequestMethod.GET)
	public String showDemo(@ModelAttribute("msg") InboxMsg message,
			Model model, HttpServletRequest request) {

		LOGGER.info(CLASS_NAME + ".showDemo Begin");

		try {

			validateUser();

			if (message.getId() != 0) {
				LOGGER.info("Message already exists : " + message.getId());
			}
			else {

				// Model attribute for Compose message page
				InboxMsg msg = new InboxMsg();
				msg.setId(0L);

				model.addAttribute("msg", msg);
				LOGGER.info("Added Model attribute to page");
			}
			showInbox(model, request);
		}
		catch (InvalidUserException e) {
			LOGGER.info(USER_EXCEPTION_STRING, e);
			return REDIRECT_LOGIN_PAGE;
		}
		finally {
			model.addAttribute(SEARCH_TEXT, "test%");
			LOGGER.info(CLASS_NAME + ".showDemo End");
		}
		return "inbox/demo";
	}

	@RequestMapping(value = "/inbox/home", method = RequestMethod.GET)
	public String showHome(@ModelAttribute("msg") InboxMsg message,
			Model model, HttpServletRequest request) {

		LOGGER.info(CLASS_NAME + ".showHome Begin");
		AccountUser accountUser = null;
		Long totalUnreadRecords;
		try {

			accountUser = validateUser();
			//If active module Id/name is set for user, use it to fetch messages
			if(accountUser.getActiveModuleId() != 0 && StringUtils.isNotBlank(accountUser.getActiveModuleName())){
				totalUnreadRecords = inboxMsgService.countUnreadMessagesByModule(accountUser.getActiveModuleId(), accountUser.getActiveModuleName());
			}
			else{
				totalUnreadRecords = inboxMsgService.countUnreadMessagesByOwner(accountUser.getId());
			}
			request.getSession().setAttribute("unreadRecords", totalUnreadRecords);
			
			if (message.getId() != 0) {
				LOGGER.info("Message already exists : " + message.getId());
			}
			else {

				// Model attribute for Compose message page
				InboxMsg msg = new InboxMsg();
				msg.setId(0L);

				model.addAttribute("msg", msg);
				LOGGER.info("Added Model attribute to page");
			}
			showInbox(model, request);
		}
		catch (InvalidUserException e) {
			LOGGER.info(USER_EXCEPTION_STRING, e);
			return REDIRECT_LOGIN_PAGE;
		}
		finally {
			LOGGER.info(CLASS_NAME + ".showHome End");
		}
		return RETURN_INBOX_HOME_PAGE;
	}

	/**
	 * AJAX call for getting the address book of a user
	 *
	 * FIXME - This is temporary logic
	 *
	 * @author Nikhil Talreja
	 * @since 11 December, 2012
	 * @return Map<Long, String> - Map containing contacts for the user
	 * 
	 * 
	 * Removed @RequestMapping - HIX-65253
	 * 
	 */
	@ResponseBody
	public Map<Long, String> getAddressBook() {

		LOGGER.info(CLASS_NAME + ".getAddressBook Begin ");

		InboxUserAddressBookResp inboxUserAddressBookResp = new InboxUserAddressBookResp();
		AccountUser sender = new AccountUser();
		Map<Long, String> addresses = null;

		try {

			sender = validateUser();

			// Checking the logged in user's role
			/*
			 * if(moduleUserService.findModuleUserByUser(sender.getId(),
			 * ModuleUserService.EMPLOYER_MODULE) != null){
			 * LOGGER.info("Fetching contacts for employer");
			 * inboxUserAddressBookResp =
			 * inboxMsgService.getUserAddressBook(sender.getId(),
			 * ModuleUserService.EMPLOYER_MODULE); } else
			 * if(employeeService.findByUser(sender) != null){
			 * LOGGER.info("Fetching contacts for employee");
			 * inboxUserAddressBookResp =
			 * inboxMsgService.getUserAddressBook(sender.getId(),
			 * ModuleUserService.EMPLOYEE_MODULE); }
			 */

			// FIXME - For time being we are fetching the first 100 users from
			// USERS table
			inboxUserAddressBookResp = inboxMsgService.getUserAddressBook(
					sender.getId(), "");

			if (inboxUserAddressBookResp != null
					&& inboxUserAddressBookResp.getAddressBook() != null) {

				addresses = inboxUserAddressBookResp.getAddressBook();
				LOGGER.info("Contacts found : " + addresses.size());

			}
		}
		catch (InvalidUserException e) {
			LOGGER.error(USER_EXCEPTION_STRING, e);
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTION_STRING, e);
		}
		finally {
			inboxUserAddressBookResp = null;
			sender = null;
			LOGGER.info(CLASS_NAME + ".getAddressBook End");
		}
		return addresses;
	}

	/**
	 * AJAX call for saving the draft of a message
	 *
	 * @author Nikhil Talreja
	 * @since 11 December, 2012
	 * @return Map<Long, String> - Map containing contacts for the user
	 *
	 */
	@RequestMapping(value = "/inbox/secureInboxSaveDraft", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> saveDraft(final HttpServletRequest request,
			final RedirectAttributes redirectAttributes) {

		LOGGER.info(CLASS_NAME + ".saveDraft Begin");
		AccountUser sender = null;
		InboxMsg msg = null;
		InboxMsgResponse response = null;
		Map<String, Object> messageProperties = null;

		try {

			sender = validateUser();

			// Checking message count per day for user
			if (!checkMessageCount(sender.getId())) {
				redirectAttributes.addFlashAttribute(MESSAGE_ATTRIBUTE,
						DAILY_LIMIT_MESSAGE);
				return null;
			}

			String id = request.getParameter(REQEUST_PARAMETER_ID);
			String to = request.getParameter(REQEUST_PARAMETER_TO);
			String subject = request.getParameter(REQUEST_PARAMETER_SUBJECT);
			String names = request.getParameter(REQUEST_PARAMTER_NAMES);
			String body = request.getParameter(REQUEST_PARAMETER_BODY);
			String previousId = request
					.getParameter(REQEUST_PARAMETER_PREVIOUS_ID);

			//LOGGER.info("Message Id : " + id);
			//LOGGER.info("Recipients : " + to);
			//LOGGER.info("Recipients Names :" + names);
			//LOGGER.info("Message Subject : " + subject);
			//LOGGER.info("Message Body : " + body);
			//LOGGER.info("Previous Message Id : " + previousId);

			// Message is being saved for the first time

			if (id == null || id.trim().length() == 0
					|| id.equalsIgnoreCase("0")) {
				msg = createNewMessage(sender, response);
				if (previousId != null && previousId.trim().length() != 0
						&& !previousId.equalsIgnoreCase("0")) {
					msg.setPreviousId(Long.parseLong(previousId));
				}
			}
			else {
				msg = inboxMsgService.findMessageById(Long.parseLong(id));
				// LOGGER.info("Message found with Id : " + msg.getId());
			}

			if (names != null && names.length() != 0) {
				names = names.substring(0, names.length() - 1);
			}

			msg.setToUserNameList(names);
			msg.setToUserIdList(to);
			msg.setMsgSub(subject);
			msg.setMsgBody(body);

			response = inboxMsgService.saveMessage(msg);

			msg = validateResponse(response);

			// Adding attachments from forwarded messages
			// addAttachmentsFromForwardedMessage(request,msg);

			messageProperties = new HashMap<String, Object>();
			messageProperties.put(REQEUST_PARAMETER_ID, msg.getId());
			messageProperties.put(REQUEST_PARAMETER_SUBJECT, SecurityUtil.encodeForHTML(msg.getMsgSub()));
			messageProperties.put(REQEUST_PARAMETER_TO, msg.getToUserIdList());
			messageProperties.put(REQUEST_PARAMTER_NAMES,
					msg.getToUserNameList());
			messageProperties.put(REQUEST_PARAMETER_BODY, msg.getMsgBody());
		}
		catch (InvalidUserException e) {
			LOGGER.error(USER_EXCEPTION_STRING, e);
		}
		catch (GIException e) {
			LOGGER.error("Error saving message", e);
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTION_STRING, e);
		}
		finally {
			sender = null;
			msg = null;
			response = null;
			LOGGER.info(CLASS_NAME + ".saveDraft End");

		}
		return messageProperties;
	}

	/**
	 * AJAX call to add attachment to a message
	 *
	 * @author Nikhil Talreja
	 * @since 17 December, 2012
	 * @return String - Re-direction URL
	 *
	 */
	@RequestMapping(value = "/inbox/secureInboxAddAttachment", method = RequestMethod.POST)
	@ResponseBody
	public String addAttachment(final HttpServletRequest request,
			final @RequestParam("fileupload") MultipartFile file) {

		LOGGER.info(CLASS_NAME + ".addAttachment Begin");

		InboxMsg msg = null;
		InboxMsgResponse response = null;
		List<InboxMsgDoc> attachments = null;

		try {

			validateUser();
			String id = request.getParameter(REQEUST_PARAMETER_ID);
			String fileName = file.getOriginalFilename();
			long fileSize = file.getSize();

			//LOGGER.info("Id : " + id);
			//LOGGER.info("File Name : " + fileName);
			LOGGER.info("File Size : " + fileSize);

			LOGGER.info("Finding message to attach file to");

			if(StringUtils.isEmpty(id) || !StringUtils.isNumeric(id)) {
				LOGGER.info("Invalid Message found");
				return null;
			}

			msg = inboxMsgService.findMessageById(Long.parseLong(id));
			if (msg == null) {
				LOGGER.info("Invalid Message found");
				return null;
			}

			if (file == null || fileSize == 0) {
				LOGGER.info("Invalid file for attachment");
				return null;
			}

			LOGGER.info("Message to add attachment found with Id : "
					+ msg.getId());

			attachments = msg.getMessageDocs();

			if (attachments == null || attachments.size() == 0) {
				LOGGER.info("Message has no attachments");
				attachments = new ArrayList<InboxMsgDoc>();
				msg.setMessageDocs(attachments);
			}

			InboxMsgDoc attachment = new InboxMsgDoc();
			// File extension is sent in lower case since Alfresco validates
			// file extensions which should be in lower case
			attachment.setDocName(validateFileName(fileName));
			attachment.setDocType(InboxMsgDoc.TYPE.ASCII);
			attachment.setCreatedOn(new TSDate());
			attachment.setDocSize(fileSize);

			msg.getMessageDocs().add(attachment);

			LOGGER.info("Adding attachments to message");
			response = inboxMsgService.addAttachment(msg, file.getBytes());

			validateResponse(response);
		}
		catch (InvalidUserException e) {
			LOGGER.error(USER_EXCEPTION_STRING, e);
			return null;
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTION_STRING, e);
			return null;
		}
		finally {
			response = null;
			LOGGER.info(CLASS_NAME + ".addAttachment End");
		}
		return file.getName();
	}

	/**
	 * AJAX call to remove attachments from a message
	 *
	 * @author Nikhil Talreja
	 * @since 30 December, 2012
	 * @return String - Re-direction URL
	 *
	 */
	@RequestMapping(value = "/inbox/secureInboxRemoveAttachment", method = RequestMethod.POST)
	@ResponseBody
	public String removeAttachment(final HttpServletRequest request) {

		LOGGER.info(CLASS_NAME + ".removeAttachment Begin");

		try {
			String[] msgParameters = request.getParameter("msg").split("~");
			String docName = null;
			String id = null;

			for (int i = 0; i < msgParameters.length; i++) {
				if (i == 0) {
					docName = msgParameters[i];
				}
				if (i == 1) {
					id = msgParameters[i];
				}
			}

			//LOGGER.info("Doc Name :" + docName);
			//LOGGER.info("ID : " + id);

			InboxMsg msg = inboxMsgService.findMessageById(Long.parseLong(id));
			if (msg == null) {
				LOGGER.info("Invalid message");
			}
			else {
				inboxMsgService.removeAttachment(msg, docName);
			}
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTION_STRING, e);
		}
		finally {
			LOGGER.info(CLASS_NAME + ".removeAttachment End");
		}
		return SUCCESS;
	}

	/**
	 * Method to send a message
	 *
	 * @author Nikhil Talreja
	 * @since 14 December, 2012
	 * @return String - Re-direction URL
	 *
	 */
	@RequestMapping(value = "/inbox/secureInboxSend", method = RequestMethod.POST)
	public String sendMessage(final Model model,
			final HttpServletRequest request,
			final MultipartHttpServletRequest mrequest,
			final RedirectAttributes redirectAttributes) {

		LOGGER.info(CLASS_NAME + ".sendMessage Begin");

		AccountUser sender = null;
		InboxMsg msg = null;
		InboxMsgResponse response = null;
		String currentURL = request.getParameter("currentURL");

		try {

			sender = validateUser();

			if(null == currentURL || 0 == currentURL.trim().length() || currentURL.indexOf("secureInboxRead") > -1) {
				currentURL = REDIRECT_SECURE_INBOX_PAGE;
			}
			else {
				currentURL = "redirect:" + currentURL;
			}
			//LOGGER.info("currentURL: " + currentURL);

			// Checking message count per day for user
			if (!checkMessageCount(sender.getId())) {
				redirectAttributes.addFlashAttribute(MESSAGE_ATTRIBUTE, DAILY_LIMIT_MESSAGE);
				return currentURL;
			}

			String id = request.getParameter(REQEUST_PARAMETER_ID);
			String to[] = request.getParameterValues("addressBook");
			String subject = request.getParameter(REQUEST_PARAMETER_SUBJECT);
			String names = request.getParameter(REQUEST_PARAMTER_NAMES);
			String body = request.getParameter(REQUEST_PARAMETER_BODY);
			String previousId = request
					.getParameter(REQEUST_PARAMETER_PREVIOUS_ID);
			String status = request
					.getParameter(REQEUST_PARAMETER_PREVIOUS_STATUS);

			//LOGGER.info("Message Id : " + id);
			//LOGGER.info("Recipients : " + Arrays.toString(to));
			//LOGGER.info("Recipients Names :" + names);
			//LOGGER.info("Message Subject : " + subject);
			//LOGGER.info("Message Body : " + body);
			//LOGGER.info("Previous Id : " + previousId);
			//LOGGER.info("Status : " + status);

			if (id == null || id.trim().length() == 0
					|| id.equalsIgnoreCase("0")) {
				msg = createNewMessage(sender, response);
				response = saveMessageAttachments(mrequest, msg, response);
				if (previousId != null && previousId.trim().length() != 0
						&& !previousId.equalsIgnoreCase("0")) {
					msg.setPreviousId(Long.parseLong(previousId));
				}
			}
			else {
				msg = inboxMsgService.findMessageById(Long.parseLong(id));
				LOGGER.info("Message found with Id : " + msg.getId());

			}

			// Checking message status
			if (status != null && status.trim().length() != 0) {
				msg.setStatus(InboxMsg.STATUS.valueOf(status));
			}
			else {
				msg.setStatus(InboxMsg.STATUS.S);
			}

			if (names != null && names.length() != 0) {
				names = names.replaceAll(",$", "");
			}

			msg.setToUserNameList(names);
			msg.setToUserIdList(Arrays.toString(to).replaceAll("\\[", "")
					.replaceAll("\\]", "").replaceAll(" ", ""));
			msg.setMsgSub(subject);
			msg.setMsgBody(body);

			// addAttachmentsFromForwardedMessage(request, msg);

			// Actual saving of the message for owner and recipients
			response = inboxMsgService.saveMessage(msg);

			msg = validateResponse(response);
		}
		catch (InvalidUserException e) {
			LOGGER.error(USER_EXCEPTION_STRING, e);
			return currentURL;
		}
		catch (GIException e) {
			LOGGER.error("Error Sending Message", e);
			redirectAttributes.addFlashAttribute(MESSAGE_ATTRIBUTE,
					"There was an error saving your message");
			return currentURL;
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTION_STRING, e);
			redirectAttributes.addFlashAttribute(MESSAGE_ATTRIBUTE,
					"There was an error sending your message");
			return currentURL;
		}
		finally {
			sender = null;
			response = null;
			LOGGER.info(CLASS_NAME + ".sendMessage End");
		}
		//FIXME  - All ui messages must come from language packs...TBD
		redirectAttributes.addFlashAttribute(MESSAGE_ATTRIBUTE,
				"Your message has been sent");
		return currentURL;
	}

	private InboxMsgResponse saveMessageAttachments(
			final MultipartHttpServletRequest mrequest, InboxMsg msg,
			InboxMsgResponse response) throws GIException,
			ContentManagementServiceException, IOException {
		if(null!=mrequest.getFileMap()&&mrequest.getFileMap().size()>0){
			for (Entry<String, MultipartFile> requestMultiFileMap : mrequest.getFileMap().entrySet()) {
				MultipartFile  file = requestMultiFileMap.getValue();
				if(!StringUtils.isEmpty(file.getOriginalFilename())){
					String fileName = file.getOriginalFilename();
					long fileSize = file.getSize();
					InboxMsgDoc attachment = new InboxMsgDoc();
					// File extension is sent in lower case since Alfresco validates
					// file extensions which should be in lower case
					attachment.setDocName(validateFileName(fileName));
					attachment.setDocType(InboxMsgDoc.TYPE.ASCII);
					attachment.setCreatedOn(new TSDate());
					attachment.setDocSize(fileSize);

					if(null==msg.getMessageDocs()){
						List <InboxMsgDoc> list = new ArrayList<InboxMsgDoc>();
						list.add(attachment);
						msg.setMessageDocs(list);
					}
					else{
						msg.getMessageDocs().add(attachment);
					}

					LOGGER.info("Adding attachments to message");
					response = inboxMsgService.addAttachment(msg, file.getBytes());

					validateResponse(response);
				}
			}
		}
		return response;
	}

	/**
	 * Method is used to get all Inbox Messages which has status N-New and R-Read.
	 *
	 * @param model reference of Model interface
	 * @return String object
	 */
	@RequestMapping(value = "/inbox/secureInbox", method = RequestMethod.GET)
	public String showInbox(Model model, HttpServletRequest request) {

		LOGGER.info(CLASS_NAME + ".showInbox Begin");

		List<InboxMsg.STATUS> statusList = null;
		AccountUser accountUser = null;

		try {
			accountUser = validateUser();
			statusList = new ArrayList<InboxMsg.STATUS>();
			statusList.add(InboxMsg.STATUS.N);
			statusList.add(InboxMsg.STATUS.R);
			statusList.add(InboxMsg.STATUS.C);

			if(accountUser.getActiveModuleId() != 0 && StringUtils.isNotBlank(accountUser.getActiveModuleName())){
				this.getInboxMsgList(accountUser.getActiveModuleId(), accountUser.getActiveModuleName(), statusList, model, request);
			}
			else{
				this.getInboxMsgList(accountUser.getId(), statusList, model, request);
			}


		}
		catch (InvalidUserException ex) {
			LOGGER.error(USER_EXCEPTION_STRING);
			return REDIRECT_LOGIN_PAGE;
		}
		catch (GIException e) {
			LOGGER.error(EXCEPTION_STRING + e.getMessage());
		}
		finally {
			model.addAttribute(PAGE_TITLE, "Inbox");
			model.addAttribute(INCLUDE_PAGE, INCLUDE_INBOX_PAGE);

			if (null != statusList) {
				statusList.clear();
				statusList = null;
			}
			accountUser = null;
			LOGGER.info(CLASS_NAME + ".showInbox End");
		}
		return RETURN_INBOX_HOME_PAGE;
	}

	/**
	 * Method is used to get all Inbox Messages which has status P-Replied, F-Forwarded and S-Sent.
	 *
	 * @param model reference of Model interface
	 * @return String object
	 */
	@RequestMapping(value = "/inbox/secureInboxSent", method = RequestMethod.GET)
	public String showSentItems(Model model, HttpServletRequest request) {

		LOGGER.info(CLASS_NAME + ".showSentItems Begin");

		List<InboxMsg.STATUS> statusList = null;
		AccountUser accountUser = null;

		try {
			accountUser = validateUser();
			statusList = new ArrayList<InboxMsg.STATUS>();
			statusList.add(InboxMsg.STATUS.P);
			statusList.add(InboxMsg.STATUS.F);
			statusList.add(InboxMsg.STATUS.S);
			this.getInboxMsgList(accountUser.getId(), statusList, model, request);
		}
		catch (InvalidUserException ex) {
			LOGGER.error(USER_EXCEPTION_STRING);
			return REDIRECT_LOGIN_PAGE;
		}
		catch (GIException e) {
			LOGGER.error(EXCEPTION_STRING + e.getMessage());
		}
		finally {
			model.addAttribute(PAGE_TITLE, "Sent Messages");
			model.addAttribute(INCLUDE_PAGE, INCLUDE_SENT_PAGE);

			if (null != statusList) {
				statusList.clear();
				statusList = null;
			}
			accountUser = null;
			LOGGER.info(CLASS_NAME + ".showSentItems End");
		}
		return RETURN_INBOX_HOME_PAGE;
	}

	/**
	 * Method is used to get all Inbox Messages which has status C-Compose.
	 *
	 * @param model reference of Model interface
	 * @return String object
	 */
	@RequestMapping(value = "/inbox/secureInboxDraft", method = RequestMethod.GET)
	public String showDrafts(Model model, HttpServletRequest request) {

		LOGGER.info(CLASS_NAME + ".showDrafts Begin");

		List<InboxMsg.STATUS> statusList = null;
		AccountUser accountUser = null;

		try {
			accountUser = validateUser();
			statusList = new ArrayList<InboxMsg.STATUS>();
			statusList.add(InboxMsg.STATUS.C);
			this.getInboxMsgList(accountUser.getId(), statusList, model, request);
		}
		catch (InvalidUserException ex) {
			LOGGER.error(USER_EXCEPTION_STRING);
			return REDIRECT_LOGIN_PAGE;
		}
		catch (GIException e) {
			LOGGER.error(EXCEPTION_STRING + e.getMessage());
		}
		finally {
			model.addAttribute(PAGE_TITLE, "Message Drafts");
			model.addAttribute(INCLUDE_PAGE, INCLUDE_DRAFT_PAGE);

			if (null != statusList) {
				statusList.clear();
				statusList = null;
			}
			accountUser = null;
			LOGGER.info(CLASS_NAME + ".showDrafts End");
		}
		return RETURN_INBOX_HOME_PAGE;
	}

	/**
	 * Method is used to get all Inbox Messages which has status A-Archived.
	 *
	 * @param model reference of Model interface
	 * @return String object
	 */
	@RequestMapping(value = "/inbox/secureInboxArchive", method = RequestMethod.GET)
	public String showArchives(Model model, HttpServletRequest request) {

		LOGGER.info(CLASS_NAME + ".showArchives Begin");

		List<InboxMsg.STATUS> statusList = null;
		AccountUser accountUser = null;

		try {
			accountUser = validateUser();
			statusList = new ArrayList<InboxMsg.STATUS>();
			statusList.add(InboxMsg.STATUS.A);

			if(accountUser.getActiveModuleId() != 0 && StringUtils.isNotBlank(accountUser.getActiveModuleName())){
				this.getInboxMsgList(accountUser.getActiveModuleId(), accountUser.getActiveModuleName(), statusList, model, request);
			}
			else{
				this.getInboxMsgList(accountUser.getId(), statusList, model, request);
			}
		}
		catch (InvalidUserException ex) {
			LOGGER.error(USER_EXCEPTION_STRING);
			return REDIRECT_LOGIN_PAGE;
		}
		catch (GIException e) {
			LOGGER.error(EXCEPTION_STRING + e.getMessage());
		}
		finally {
			model.addAttribute(PAGE_TITLE, "Archived messages");
			model.addAttribute(INCLUDE_PAGE, INCLUDE_ARCHIVE_PAGE);

			if (null != statusList) {
				statusList.clear();
				statusList = null;
			}
			accountUser = null;
			LOGGER.info(CLASS_NAME + ".showArchives End");
		}
		return RETURN_INBOX_HOME_PAGE;
	}

	/**
	 * Method is used to get messages from InboxMsg base on search criteria with status is not a Delete.
	 *
	 * @param model reference of Model interface
	 * @return String object
	 */
	@RequestMapping(value = "/inbox/secureInboxSearch", method = RequestMethod.GET)
	public String showSearchMessages(Model model, HttpServletRequest request) {

		LOGGER.info(CLASS_NAME + ".showSearchMessages Begin");

		List<InboxMsg.STATUS> statusList = null;
		AccountUser accountUser = null;

		try {
			accountUser = validateUser();

			if(accountUser.getActiveModuleId() != 0 && StringUtils.isNotBlank(accountUser.getActiveModuleName())){
				this.getInboxMsgList(accountUser.getActiveModuleId() ,accountUser.getActiveModuleName(), null, model, request);
			}
			else{
				this.getInboxMsgList(accountUser.getId(), null, model, request);
			}
		}
		catch (InvalidUserException ex) {
			LOGGER.error(USER_EXCEPTION_STRING);
			return REDIRECT_LOGIN_PAGE;
		}
		catch (GIException e) {
			LOGGER.error(EXCEPTION_STRING + e.getMessage());
		}
		finally {
			model.addAttribute(PAGE_TITLE, "Searched messages");
			model.addAttribute(INCLUDE_PAGE, INCLUDE_SEARCH_PAGE);

			if (null != statusList) {
				statusList.clear();
				statusList = null;
			}
			accountUser = null;
			LOGGER.info(CLASS_NAME + ".showSearchMessages End");
		}
		return RETURN_INBOX_HOME_PAGE;
	}

	/**
	 * Method is used to get count of Unread messages from inbox for login user.
	 *
	 * @param model reference of Model interface
	 * @return String object
	 */
	@RequestMapping(value = "/inbox/secureInboxUnread", method = RequestMethod.POST)
	@ResponseBody
	public long showUnreadMessages(Model model, HttpServletRequest request) {

		LOGGER.debug(CLASS_NAME + ".showUnreadMessages Begin");

		AccountUser accountUser = null;
		long totalUnreadRecords = 0;

		try {
			accountUser = validateUser();

			//If active module Id/name is set for user, use it to fetch messages
			if(accountUser.getActiveModuleId() != 0 && StringUtils.isNotBlank(accountUser.getActiveModuleName())){
				totalUnreadRecords = inboxMsgService.countUnreadMessagesByModule(accountUser.getActiveModuleId(), accountUser.getActiveModuleName());
			}
			else{
				totalUnreadRecords = inboxMsgService.countUnreadMessagesByOwner(accountUser.getId());
			}
			request.getSession().setAttribute("unreadRecords", totalUnreadRecords);
			request.getSession().setAttribute("lastUpdatedTime", request.getParameter("currentTime")==null? 0:request.getParameter("currentTime"));
			//totalUnreadRecords = inboxMsgService.countUnreadMessagesByOwner(accountUser.getId());
			LOGGER.debug("totalUnreadRecords: " + totalUnreadRecords);
		}
		catch (InvalidUserException ex) {
			//LOGGER.error(USER_EXCEPTION_STRING);
			return totalUnreadRecords;
		}
		finally {
			model.addAttribute(TOTAL_UNREAD_RECORDS, totalUnreadRecords);
			accountUser = null;
			LOGGER.debug(CLASS_NAME + ".showUnreadMessages End");
		}
		return totalUnreadRecords;
	}

	private void getInboxMsgList(int userId, List<InboxMsg.STATUS> statusList,
			Model model, HttpServletRequest request) throws GIException, InvalidUserException {

		LOGGER.info(CLASS_NAME + ".getInboxMsgList Begin");

		int pageRows = 0, pageNumber = 0, totalPages = 0;
		long totalRecords = 0, totalUnreadRecords = 0;
		String searchText = null;

		List<InboxMsg> inboxMsgList = null;
		Pageable pageable = null;

		try {

			if (StringUtils.isNotEmpty(request.getParameter(PAGE_NUMBER)) &&
					StringUtils.isNumeric(request.getParameter(PAGE_NUMBER))) {
				pageNumber = Integer.parseInt(request.getParameter(PAGE_NUMBER));
			}

			if (pageNumber < 1) {
				pageNumber = 1;
			}
			//LOGGER.info("pageNumber number: " + pageNumber);
			//LOGGER.info("page size: " + PAGE_SIZE);

			Page<InboxMsg> inboxMsgPageList = null;
			pageable = new PageRequest(pageNumber - 1, PAGE_SIZE_NUM, this.getSorting(model, request));

			// In this condition, if statusList is null then search action has been invoked otherwise vice versa.
			if(null != statusList && !statusList.isEmpty()) {
				inboxMsgPageList = inboxMsgService.findByOwnerUserIdAndStatusIn(userId, statusList, pageable);
			}
			else {
				searchText = request.getParameter(SEARCH_TEXT);

				//If active module Id/name is present for user, use it to search messages
				if(searchText != null && searchText.trim().length() > 0) {
					searchText = searchText.trim();
					inboxMsgPageList = inboxMsgService.getMessagesBySearchCriteria(userId,0,null, searchText, pageable);
				}
				else {
					searchText = null;
				}
			}

			HttpSession session = request.getSession();
			// Adding searchText to session for readMessage() to use it.
			session.setAttribute(SEARCH_TEXT, searchText);

			// Method is used to get count of Unread messages from inbox for login user.
			totalUnreadRecords = inboxMsgService.countUnreadMessagesByOwner(userId);

			if (null != inboxMsgPageList && inboxMsgPageList.hasContent()) {
				pageNumber = inboxMsgPageList.getNumber();
				pageRows = inboxMsgPageList.getNumberOfElements();
				totalPages = inboxMsgPageList.getTotalPages();
				totalRecords = inboxMsgPageList.getTotalElements();
				inboxMsgList = inboxMsgPageList.getContent();

				if (null != inboxMsgList && !inboxMsgList.isEmpty()) {
					// Adding inboxMsgList data to session for readMessage() to use it.
					session.setAttribute(INBOX_MSG_LIST_SESSION, inboxMsgList);
					//LOGGER.info("inboxMsgList added to session data");
				}
				else {
					session.setAttribute(INBOX_MSG_LIST_SESSION, null);
					//LOGGER.info("empty inboxMsgList added to session data");
				}
			}
		}
		finally {
			model.addAttribute(INBOX_MSG_LIST_MODEL, inboxMsgList);
			model.addAttribute(PAGE_NUMBER, pageNumber);
			model.addAttribute(PAGE_ROWS, pageRows);
			model.addAttribute(TOTAL_PAGES, totalPages);
			model.addAttribute(TOTAL_UNREAD_RECORDS, totalUnreadRecords);
			model.addAttribute(TOTAL_RECORDS, totalRecords);
			model.addAttribute(PAGE_SIZE, PAGE_SIZE_NUM);
			try {
				if(userService.getLoggedInUser().getActiveModuleName().equalsIgnoreCase("employee")) {
					model.addAttribute("showMenu", false);
				}
			}
			catch(InvalidUserException ex) {
				throw new InvalidUserException();
			}
			pageable = null;
			LOGGER.info(CLASS_NAME + ".getInboxMsgList End");
		}
	}

	private void getInboxMsgList(int moduleId, String moduleName, List<InboxMsg.STATUS> statusList,
			Model model, HttpServletRequest request) throws GIException {

		LOGGER.info(CLASS_NAME + ".getInboxMsgList for Module Begin");

		int pageRows = 0, pageNumber = 0, totalPages = 0;
		long totalRecords = 0, totalUnreadRecords = 0;

		List<InboxMsg> inboxMsgList = null;
		Pageable pageable = null;
		String searchText = null;

		try {

			if (StringUtils.isNotEmpty(request.getParameter(PAGE_NUMBER)) &&
					StringUtils.isNumeric(request.getParameter(PAGE_NUMBER))) {
				pageNumber = Integer.parseInt(request.getParameter(PAGE_NUMBER));
			}

			if (pageNumber < 1) {
				pageNumber = 1;
			}
			LOGGER.info("pageNumber number: " + pageNumber);
			LOGGER.info("page size: " + PAGE_SIZE);

			Page<InboxMsg> inboxMsgPageList = null;
			pageable = new PageRequest(pageNumber - 1, PAGE_SIZE_NUM, this.getSorting(model, request));

			// In this condition, if statusList is null then search action has been invoked otherwise vice versa.
			if(null != statusList && !statusList.isEmpty()) {
				inboxMsgPageList = inboxMsgService.findByModuleIdAndModuleNameAndStatusIn(moduleId, moduleName, statusList, pageable);
			}
			else {
				searchText = request.getParameter(SEARCH_TEXT);

				if(searchText != null && searchText.trim().length() > 0) {
					searchText = searchText.trim();
					inboxMsgPageList = inboxMsgService.getMessagesBySearchCriteria(0,moduleId,moduleName,searchText, pageable);
				}
				else {
					searchText = null;
				}
			}
			HttpSession session = request.getSession();
			// Adding searchText to session for readMessage() to use it.
			session.setAttribute(SEARCH_TEXT, searchText);

			// Method is used to get count of Unread messages from inbox for login user.
			totalUnreadRecords = inboxMsgService.countUnreadMessagesByModule(moduleId,moduleName);

			if (null != inboxMsgPageList && inboxMsgPageList.hasContent()) {
				pageNumber = inboxMsgPageList.getNumber();
				pageRows = inboxMsgPageList.getNumberOfElements();
				totalPages = inboxMsgPageList.getTotalPages();
				totalRecords = inboxMsgPageList.getTotalElements();
				inboxMsgList = inboxMsgPageList.getContent();

				if (null != inboxMsgList && !inboxMsgList.isEmpty()) {
					// Adding inboxMsgList data to session for readMessage() to use it.
					session.setAttribute(INBOX_MSG_LIST_SESSION, inboxMsgList);
					//LOGGER.info("inboxMsgList added to session data");
				}
				else {
					session.setAttribute(INBOX_MSG_LIST_SESSION, null);
					//LOGGER.info("empty inboxMsgList added to session data");
				}
			}
		}
		finally {
			model.addAttribute(INBOX_MSG_LIST_MODEL, inboxMsgList);
			model.addAttribute(PAGE_NUMBER, pageNumber);
			model.addAttribute(PAGE_ROWS, pageRows);
			model.addAttribute(TOTAL_PAGES, totalPages);
			model.addAttribute(TOTAL_UNREAD_RECORDS, totalUnreadRecords);
			model.addAttribute(TOTAL_RECORDS, totalRecords);
			model.addAttribute(PAGE_SIZE, PAGE_SIZE_NUM);

			if(moduleName.equalsIgnoreCase("employee")) {// Fux this later to avoid if block
				model.addAttribute("showMenu", false);
			}
			pageable = null;
			LOGGER.info(CLASS_NAME + ".getInboxMsgList for Module End");
		}
	}

	private Sort getSorting(Model model, HttpServletRequest request) {

		LOGGER.info(CLASS_NAME + ".getSorting Begin");
		Map<String, Object> searchCriteria = new HashMap<String, Object>();
		Sort sortObject = null;

		try {
			String sortBy = (StringUtils.isEmpty(request.getParameter(SORT_BY))) ? "dateCreated" : request.getParameter(SORT_BY);
			String sortOrder = (StringUtils.isEmpty(request.getParameter(SORT_ORDER))) ? "DESC" : (request.getParameter(SORT_ORDER).equalsIgnoreCase("ASC") ? "ASC" : "DESC");
			String changeOrder = (StringUtils.isEmpty(request.getParameter(CHANGE_ORDER))) ? "false" : request.getParameter(CHANGE_ORDER);
			sortOrder = (changeOrder.equals("true")) ? ((sortOrder.equals("DESC")) ? "ASC" : "DESC") : sortOrder;
			request.removeAttribute(CHANGE_ORDER);

			//LOGGER.info("sortBy: " + sortBy);
			//LOGGER.info("changeOrder: " + changeOrder);
			//LOGGER.info("sortOrder: " + sortOrder);
			searchCriteria.put(SORT_ORDER, sortOrder);
			model.addAttribute(SORT_ORDER, sortOrder);
			model.addAttribute(SORT_BY, sortBy);
			model.addAttribute("searchCriteria", searchCriteria);

			Sort.Direction direction = Sort.Direction.ASC;

			if (sortOrder.equalsIgnoreCase("DESC")) {
				direction = Sort.Direction.DESC;
			}
			//LOGGER.info("direction: " + direction);

			if (sortBy.equalsIgnoreCase("msgSub")) {
				sortObject = new Sort(new Sort.Order(direction, "msgSub"));
			}
			else if (sortBy.equalsIgnoreCase("fromUserName")) {
				sortObject = new Sort(new Sort.Order(direction, "fromUserName"));
			}
			else {
				sortObject = new Sort(new Sort.Order(direction, "dateCreated"));
			}
		}
		finally {
			LOGGER.info(CLASS_NAME + ".getSorting End");
		}
		return sortObject;
	}

	/**
	 * Method is used for Secure inbox action.
	 *
	 * @param model reference of Model interface
	 * @param request object of HttpServletRequest
	 * @return
	 */
	@RequestMapping(value = "/inbox/secureInboxAction", method = RequestMethod.POST)
	public String secureInboxAction(Model model, HttpServletRequest request) {

		LOGGER.info(CLASS_NAME + ".secureInboxAction Begin");

		AccountUser accountUser = null;
		InboxMsg.STATUS inboxStatus = null;
		String messageIds = null;
		String actionName = null;

		try {
			accountUser = validateUser();
			messageIds = request.getParameter("messageId");
			actionName = request.getParameter("actionName");
			//LOGGER.info("messageIds: " + messageIds);
			//LOGGER.info("actionName: " + actionName);

			if (!StringUtils.isEmpty(messageIds) && !StringUtils.isEmpty(actionName)) {

				if (actionName.equalsIgnoreCase("Archive")) {
					inboxStatus = InboxMsg.STATUS.A;
				}
				else if (actionName.equalsIgnoreCase("Read")) {
					inboxStatus = InboxMsg.STATUS.R;
				}
				else if (actionName.equalsIgnoreCase("Unread")) {
					inboxStatus = InboxMsg.STATUS.N;
				}
				InboxMsgResponse inboxMsgResponse = inboxMsgService.secureInboxAction(accountUser.getId(), messageIds, inboxStatus);
				LOGGER.info("inboxMsgResponse.getErrCode():" + inboxMsgResponse.getErrCode());
			}
			request.removeAttribute("messageId");
			request.removeAttribute("actionName");
		}
		catch (InvalidUserException ex) {
			LOGGER.error(USER_EXCEPTION_STRING);
			return REDIRECT_LOGIN_PAGE;
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTION_STRING + e.getMessage());
		}
		finally {
			messageIds = null;
			actionName = null;
			inboxStatus = null;
			accountUser = null;
			LOGGER.info(CLASS_NAME + ".secureInboxAction End");
		}
		return getRedirectPage(model, request);
	}

	private String getRedirectPage(Model model, HttpServletRequest request) {

		String redirectPage = REDIRECT_SECURE_INBOX_PAGE;
		String pageTitle = (StringUtils.isEmpty(request.getParameter(PAGE_TITLE))) ? null : request.getParameter(PAGE_TITLE);
		//LOGGER.info("pageTitle: " + pageTitle);

		if (null != pageTitle) {

			if (pageTitle.equalsIgnoreCase("Sent Messages")) {
				redirectPage = REDIRECT_SECURE_INBOX_SENT_PAGE;
			}
			else if (pageTitle.equalsIgnoreCase("Message Drafts")) {
				redirectPage = REDIRECT_SECURE_INBOX_DRAFT_PAGE;
			}
			else if (pageTitle.equalsIgnoreCase("Archived messages")) {
				redirectPage = REDIRECT_SECURE_INBOX_ARCHIVE_PAGE;
			}
		}
		LOGGER.info("redirectPage: " + redirectPage);
		return redirectPage;
	}

	/**
	 * Author Geetha Chandran Description This method gets message details for
	 * the input message id.
	 *
	 * @param messageId - Message id for which details need to be fetched
	 * @param model - Model Object
	 * @param page_title - The page from where the read message is called
	 * @param request - The HttpServletRequest object
	 * @return String - redirection URL
	 * @throws GIException 
	 */
	@RequestMapping(value = "/inbox/secureInboxRead", method = RequestMethod.POST)
	@GiAudit(transactionName = "Secure Inbox", eventType = EventTypeEnum.SECURE_INBOX, eventName = EventNameEnum.PII_READ)
	public String readMessage(@RequestParam(required = false) Long messageId, @RequestParam String page_title,
			Model model, HttpServletRequest request, HttpServletResponse response) throws GIException {

		LOGGER.info(CLASS_NAME + ".readMessage Begin");

		InboxMsg inboxMsg = null;
		Map<String, String> msgDetail = null;
		AccountUser accountUser = null;
		long totalRecords = 0, totalUnreadRecords = 0;
		
		/*
		 * Message id is purposely marked as false in request mapping to handle HIX-101966
		 * We can't allow controller to proceed unless we have messageId hence it's been handled in following code.
		 * */
		if(messageId == null || messageId <= 0){
			String source = request.getHeader("referer");
			LOGGER.error("MESSAGE ID PARAMETER NOT FOUND. SOURCE IS:"+source);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			throw new GIException(HttpServletResponse.SC_BAD_REQUEST,"REQUIRED MESSAGE ID PARAMETER IS NOT PRESENT IN REQUEST. SOURCE OF REQUEST IS:"+source,"HIGH");
		}
		try {
			
			accountUser = validateUser();

			@SuppressWarnings("unchecked")
			List<InboxMsg> inboxMsgList = (List<InboxMsg>) request.getSession().getAttribute(INBOX_MSG_LIST_SESSION);
			// Above line is commented as data would be retrieved from session

			// Map to hold details for display, apart from inboxMsg object
			msgDetail = new HashMap<String, String>();

			if (null != inboxMsgList) {

				for (InboxMsg inboxMessage : inboxMsgList) {

					if (inboxMessage.getId() == messageId) {
						inboxMsg = inboxMessage;
						break;
					}
				}
			}

			if (null != inboxMsg) {

				if (null != inboxMsg.getDateUpdated()) {
					if (page_title.equalsIgnoreCase(DRAFT_MESSAGES)) {
						msgDetail.put("sentDate", NONE);
					} else {
						String sentDate = new SimpleDateFormat(
								INBOX_DATE_FORMAT).format(inboxMsg
								.getDateCreated());
						msgDetail.put("sentDate", sentDate);
					}
				}

				String inboxMsgClob = null;
				if (null != inboxMsg.getIsClobUsed() && inboxMsg.getIsClobUsed().equals(InboxMsg.CLOB_USED.Y)) {
					LOGGER.debug("Retreiving of message clob begins");
					inboxMsgClob = inboxMsgService.getMessageCLOB(messageId);
					LOGGER.debug("Retreiving of message clob ends");
				}

				// Converting attachment size (in bytes) to required
				// display. e.g KB, MB, GB
				List<InboxMsgDoc> inboxMsgDocs = inboxMsg.getMessageDocs();
				if (null != inboxMsgDocs && inboxMsgDocs.size() > 0) {

					int unit = UNIT_SIZE;
					String docDisplaySize = "";
					String prefix = "";
					long docSize;

					for (InboxMsgDoc inboxMsgDoc : inboxMsg.getMessageDocs()) {

						docSize = inboxMsgDoc.getDocSize();

						if (docSize < unit) {
							docDisplaySize = docSize + " B";
						}
						else {
							int exp = (int) (Math.log(docSize) / Math.log(unit));
							prefix = ("KMGTPE").charAt(exp - 1) + "";
							docDisplaySize = String.format("%.0f %s", docSize / Math.pow(unit, exp), prefix);
						}
						msgDetail.put(inboxMsgDoc.getDocId(), docDisplaySize);
					}
				}
				GiAuditParameterUtil.add(new GiAuditParameter("Message ID of inbox msg read", messageId),
						new GiAuditParameter("User ID who read inbox msg", accountUser.getId()));


				LOGGER.debug("Message status updated to READ");
				if (inboxMsg.getStatus() == InboxMsg.STATUS.N) {
					inboxMsgService.updateMessage(inboxMsg);
				}

				if (!StringUtils.isEmpty(inboxMsgClob)) {

					// Here we are decrypting CLOB object since it is stored in encrypted form
					/*String decryptedBody = null;

					try {
						decryptedBody = MessageEncryptor.decrypt(inboxMsgClob);
					}
					catch (Exception e) {
						LOGGER.error("Unable to dencrypt message", e);
					}*/

					LOGGER.debug("Successfully retrieved clob for given message id");
					inboxMsg.setMsgBody(inboxMsgClob);
				}
				else {
					LOGGER.debug("No clob found for given message id");
					// TODO Code to handle negative test case scenarios
				}
			}
			// else {
			// TODO Code to handle negative test case scenarios
			// }

			if (StringUtils.isNumeric(request.getParameter(TOTAL_RECORDS))) {
				totalRecords = Integer.parseInt(request.getParameter(TOTAL_RECORDS));
			}
			if(accountUser.getActiveModuleId() != 0 && StringUtils.isNotBlank(accountUser.getActiveModuleName())){
				totalUnreadRecords = inboxMsgService.countUnreadMessagesByModule(accountUser.getActiveModuleId(), accountUser.getActiveModuleName());
			}
			else{
				totalUnreadRecords = inboxMsgService.countUnreadMessagesByOwner(accountUser.getId());
			}
			request.getSession().setAttribute("unreadRecords", totalUnreadRecords);
		}
		catch (InvalidUserException ex) {
			LOGGER.error(USER_EXCEPTION_STRING);
			return REDIRECT_LOGIN_PAGE;
		}
		catch (ContentManagementServiceException cmse) {
			LOGGER.error("Error occured while updating status as READ");
		}
		catch (GIException gie) {
			LOGGER.error("Error occured while updating status as READ");
		}
		finally {
			model.addAttribute(TOTAL_UNREAD_RECORDS, totalUnreadRecords);
			model.addAttribute(TOTAL_RECORDS, totalRecords);
			model.addAttribute(INCLUDE_PAGE, INCLUDE_READ_PAGE);
			model.addAttribute("inboxMsg", inboxMsg);
			model.addAttribute("msgDetail", msgDetail);
			model.addAttribute(PAGE_TITLE, page_title);
			LOGGER.info(CLASS_NAME + ".readMessage End");
		}
		return RETURN_INBOX_HOME_PAGE;
	}

	@RequestMapping(value = "/inbox/secureInboxDiscard", method = RequestMethod.POST)
	@ResponseBody
	public String discardMessage(final HttpServletRequest request,
			final RedirectAttributes redirectAttributes) {

		LOGGER.info(CLASS_NAME + ".discardMessage Begin");

		InboxMsg msg = null;
		InboxMsgResponse response = new InboxMsgResponse();

		try {

			validateUser();

			String id = request.getParameter(REQEUST_PARAMETER_ID);

			//LOGGER.info("Id : " + id);

			if (id == null || id.trim().length() == 0
					|| id.equalsIgnoreCase("0")) {
				return SUCCESS;
			}
			else {
				msg = inboxMsgService.findMessageById(Long.parseLong(id));

				if(msg == null){
					return SUCCESS;
				}

				LOGGER.info("Dicarding message with Id : " + msg.getId());
				msg.setStatus(InboxMsg.STATUS.D);
			}

			response = inboxMsgService.cancelMessage(msg);

			validateResponse(response);

			return SUCCESS;

		}
		catch (InvalidUserException e) {
			LOGGER.info(USER_EXCEPTION_STRING, e);
			return FAILURE;
		}
		catch (GIException e) {
			LOGGER.info("Error discarding message", e);
			return FAILURE;
		}
		catch (Exception e) {
			LOGGER.info(EXCEPTION_STRING, e);
			return FAILURE;
		}
		finally {
			response = null;
			msg = null;
			LOGGER.info(CLASS_NAME + ".discardMessage End");
		}
	}

	/**
	 * Utility method to validate sender
	 *
	 * @author Nikhil Talreja
	 * @since 12 December, 2012
	 * @return AccountUser - User Object
	 *
	 */
	private AccountUser validateUser() throws InvalidUserException {

		AccountUser user = userService.getLoggedInUser();

		if (user == null) {
			LOGGER.info("Invalid user or user is not logged in");
			throw new InvalidUserException();
		}
		else{
			LOGGER.info("Active user module id " + user.getActiveModuleId());
			LOGGER.info("Active user module name " + user.getActiveModuleName());
		}
		return user;
	}

	/**
	 * @author Geetha Chandran
	 *
	 * Description This method gets attachment from
	 * Alfresco
	 *
	 * @param documentId
	 *            - Message id for which details need to be fetched
	 * @param docName
	 *            - Model Object
	 * @param viewType
	 *            - Describes if attachment should be displayed within browser
	 *            or as get displayed as open/save attachment
	 * @return String - redirection URL
	 */
	@RequestMapping(value = "/inbox/getAttachment", method = RequestMethod.GET)
	public void getAttachment(@RequestParam String ref,Model model, HttpServletResponse response) {

		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Initiating get attachment");
		}

		try {
			Map<String,String> refMap = GhixAESCipherPool.decryptParameterMap(ref);
			String documentId = refMap.get("documentId".intern());
			String documentName = refMap.get("documentName".intern());
			String viewType = refMap.get("viewType".intern());
			String messageId = refMap.get("msgId".intern());
			AccountUser user = userService.getLoggedInUser();
			
			if(StringUtils.isBlank(messageId) || !StringUtils.isNumeric(messageId)){
				throw new GIException("Invalid message Id");
			}
			
			InboxMsg msg = inboxMsgService.findMessageById(Long.parseLong(messageId));
			
			if(msg == null){
				throw new GIException("Invalid message");
			}
			
			boolean allowAccessToAttachment = false;
			//Match logged in user's details with the ones in message
			if (msg.getModuleId() == user.getActiveModuleId()
					&& StringUtils.equalsIgnoreCase(msg.getModuleName(),
							user.getActiveModuleName())) {
				allowAccessToAttachment = true;
			}
			else if(msg.getOwnerUserId() == user.getId()){
				allowAccessToAttachment = true;
			}
			
			if(!allowAccessToAttachment){
				throw new AccessDeniedException("Logged in user is not authorized to view this attachment");
			}
			//END
			
			InboxMsgResponse inboxMsgResponse = new InboxMsgResponse();

			byte[] attachment = inboxMsgService.getAttachment(inboxMsgResponse, documentId);
			if(attachment == null){
				LOGGER.error("No attachment returned for document id:"+documentId+" Could be a data issue");
			}

			if(inboxMsgResponse.getErrCode() == 0 && attachment != null) {

				if(null != documentName && documentName.contains(".pdf")) {
					response.setContentType("application/pdf");
				}
				else {
					response.setContentType("application/octet-stream");
				}
				response.setContentLength(attachment.length);
				//HP-FOD fix: Validate header values before setting to header
				response.setHeader("Content-Disposition", (viewType.replaceAll("[\n\r]", "") + "; filename=\"" + documentName.replaceAll("[\n\r]", "") + "\""));
				FileCopyUtils.copy(attachment, response.getOutputStream());
			}
		}
		catch (Exception e) {
			LOGGER.error("Error {} while retrieving attacgment ",e.getMessage(),e);
		}
		model.addAttribute(INCLUDE_PAGE, INCLUDE_READ_PAGE);
	}

	@RequestMapping(value = "/inbox/secureInboxGetMessage", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getMessage(final HttpServletRequest request) {

		LOGGER.info(CLASS_NAME + ".getMessage Begin");

		InboxMsg msg = null;
		Map<String, Object> messageProperties = null;

		try {

			validateUser();

			String id = request.getParameter(REQEUST_PARAMETER_ID);
			String status = request
					.getParameter(REQEUST_PARAMETER_PREVIOUS_STATUS);

			//LOGGER.info("Original Message : " + id);
			//LOGGER.info("Status : " + status);

			if (id == null || id.trim().length() == 0
					|| id.equalsIgnoreCase("0")) {
				throw new GIException();
			}

			msg = inboxMsgService.findMessageById(Long.parseLong(id));

			if (msg == null) {
				throw new GIException();
			}

			String subject = msg.getMsgSub();
			String to = null;
			String names = null;

			// Getting original body for editing message
			String body = null;
			String inboxMsgClob = null;
			String decryptedBody = null;

			if (status != null && status.trim().length() != 0
					&& status.equalsIgnoreCase("C")) {

				// Checking if body is CLOB
				if (null != msg.getIsClobUsed()
						&& msg.getIsClobUsed().equals(InboxMsg.CLOB_USED.Y)) {
					LOGGER.debug("Retreiving of message clob begins");

					inboxMsgClob = inboxMsgService.getMessageCLOB(msg.getId());

					LOGGER.debug("Retreiving of message clob ends");
				}

				if (!StringUtils.isEmpty(inboxMsgClob)) {

					decryptedBody = MessageEncryptor.decrypt(inboxMsgClob);
					body = decryptedBody;

					LOGGER.debug("Successfully retrieved clob for given message id");
				}
				else {
					LOGGER.debug("No clob found for given message id");
					body = msg.getMsgBody();
				}
			}
			// Creating reply/Forward body
			else {
				body = createReplyOrForwardBody(msg);
			}

			// Fetching to field based on status
			if (status != null && status.trim().length() != 0
					&& status.equalsIgnoreCase("C")) {
				to = msg.getToUserIdList();
				names = msg.getToUserNameList();
			}
			else {
				to = new Long(msg.getFromUserId()).toString();
				names = msg.getFromUserName();
			}

			messageProperties = new HashMap<String, Object>();
			messageProperties.put(REQUEST_PARAMETER_SUBJECT, SecurityUtil.encodeForHTML(subject));
			messageProperties.put(REQUEST_PARAMETER_BODY, body);
			messageProperties.put(REQEUST_PARAMETER_TO, to);
			messageProperties.put(REQUEST_PARAMTER_NAMES, names);

			// If status is F or C, check for message attachments
			if (status != null
					&& status.trim().length() != 0
					&& (status.equalsIgnoreCase("F") || status
							.equalsIgnoreCase("C"))) {

				if ((msg.getMessageDocs() != null && msg.getMessageDocs()
						.size() != 0)) {
					String docIds[] = new String[MAX_NO_OF_ATTACHMENTS];
					String docNames[] = new String[MAX_NO_OF_ATTACHMENTS];
					String docSizes[] = new String[MAX_NO_OF_ATTACHMENTS];
					for (int i = 0; i < msg.getMessageDocs().size(); i++) {
						InboxMsgDoc attachment = msg.getMessageDocs().get(i);
						if (attachment != null) {
							docIds[i] = attachment.getDocId();
							docNames[i] = SecurityUtil.encodeForHTML(attachment.getDocName());

							// Formatting Document size
							int unit = UNIT_SIZE;
							String docDisplaySize = "";
							String prefix = "";
							long docSize = attachment.getDocSize();

							if (docSize < unit) {
								docDisplaySize = docSize + " B";
							}
							else {
								int exp = (int) (Math.log(docSize) / Math
										.log(unit));
								prefix = ("KMGTPE").charAt(exp - 1) + "";
								docDisplaySize = String.format("%.0f %s",
										docSize / Math.pow(unit, exp), prefix)
										+ "B";
							}
							docSizes[i] = docDisplaySize;
						}

					}
					messageProperties.put("docIds", docIds);
					messageProperties.put("docNames", docNames);
					messageProperties.put("docSizes", docSizes);
				}

			}

		}
		catch (InvalidUserException e) {
			LOGGER.error(USER_EXCEPTION_STRING, e);
		}
		catch (GIException e) {
			LOGGER.error("Error getting message", e);
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTION_STRING, e);
		}
		finally {
			LOGGER.info(CLASS_NAME + ".getMessage End");
		}
		return messageProperties;
	}

	private InboxMsg validateResponse(InboxMsgResponse response)
			throws GIException {

		InboxMsg returnedMsg = null;

		if (response != null) {

			if (response.getMessageList() != null
					&& response.getMessageList().size() != 0
					&& response.getMessageList().get(0) != null) {
				returnedMsg = response.getMessageList().get(0);
			}

			if (returnedMsg == null) {
				throw new GIException();
			}
			LOGGER.info("Message saved with Id " + returnedMsg.getId());
		}

		return returnedMsg;
	}

	/**
	 * Utility method to check the message count per user per day This count
	 * cannot be more than 100
	 *
	 * @author Nikhil Talreja
	 * @since 21 December, 2012
	 * @param - senderId : User Id of the logged in user
	 * @return boolean - true if count is less than 100, false otherwise
	 *
	 */
	private boolean checkMessageCount(long senderId) {

		// Checking message count per day for user
		Long messageCount = inboxMsgService
				.countMessagesPerDayForUser(senderId);
		if (messageCount != null
				&& messageCount.longValue() > MAX_MESSAGE_COUNT) {

			LOGGER.info("Message count for " + senderId + " is " + messageCount);

			return false;
		}
		return true;
	}

	/**
	 * Method to create initial record for the message
	 *
	 * @author Nikhil Talreja
	 * @since 24 December, 2012
	 * @param AccountUser
	 *            - logged in user, InboxMsgResponse - response object
	 * @return boolean - true if count is less than 100, false otherwise
	 * @throws ContentManagementServiceException
	 * @throws GIException
	 *
	 */
	private InboxMsg createNewMessage(AccountUser sender,
			InboxMsgResponse response) throws GIException,
			ContentManagementServiceException {

		InboxMsg msg = new InboxMsg();
		msg.setFromUserId(sender.getId());
		msg.setFromUserName(sender.getFullName());
		msg.setOwnerUserId(sender.getId());
		msg.setOwnerUserName(sender.getFullName());
		

		// Here the message is being saved just to create initial record hence
		// the status is passed as C
		msg.setStatus(InboxMsg.STATUS.C);
		LOGGER.info("Saving message for first time to create record");
		response = inboxMsgService.saveMessage(msg);
		msg = validateResponse(response);

		return msg;
	}

	/**
	 * Method to create body message for a reply or forward
	 *
	 * @author Nikhil Talreja
	 * @since 24 December, 2012
	 * @param InboxMsg
	 *            object to get data from the message being forwarded
	 * @return Message Body for reply/forward
	 */
	private String createReplyOrForwardBody(InboxMsg msg) {

		StringBuffer body = new StringBuffer();

		body.append("\n\n______________________________________________________________________________________________\n");
		body.append("Original Message : \n");
		body.append("From: " + msg.getFromUserName() + "\n");

		if (msg.getDateUpdated() != null) {
			String sentDate = new SimpleDateFormat(INBOX_DATE_FORMAT)
					.format(msg.getDateUpdated());
			body.append("Sent: " + sentDate + "\n");
		}

		body.append("To: " + msg.getToUserNameList() + "\n");
		body.append("Subject: " + msg.getMsgSub() + "\n\n");
		body.append(msg.getMsgBody());

		return body.toString();

	}

	/**
	 * Method to check file extension case File extension should be in lower
	 * case since it is validated in Alfresco
	 *
	 * @author Nikhil Talreja
	 * @since 03 January, 2013
	 * @param String
	 *            fileName - Name of the file
	 * @return String - file name with extension in lower case
	 */
	private String validateFileName(String fileName) {

		if (StringUtils.isEmpty(fileName)) {
			return fileName;
		}

		String[] file = fileName.split("\\.");
		
		String newFileName = null;
		if (file != null && file.length >= 2) {
			newFileName = file[0] + "." + file[1].toLowerCase();
		}

		return newFileName;
	}

	/**
	 * Method to add attachments from a forwarded message
	 *
	 * @author Nikhil Talreja
	 * @since 27 December, 2012
	 * @param HttpServletRequest
	 *            request - Request object
	 *
	 */

	@RequestMapping(value = "/inbox/secureInboxSaveForwardedAttachment", method = RequestMethod.POST)
	@ResponseBody
	public String addAttachmentsFromForwardedMessage(
			final HttpServletRequest request) {

		LOGGER.info(CLASS_NAME + ".addAttachmentsFromForwardedMessage Begin");

		InboxMsg msg = null;
		List<InboxMsgDoc> attachments = new ArrayList<InboxMsgDoc>();

		try {

			validateUser();

			String id = request.getParameter(REQEUST_PARAMETER_ID);
			String docId = request.getParameter("docId");

			//LOGGER.info("Original Message : " + id);
			//LOGGER.info("Doc Id : " + docId);

			if (id == null || id.trim().length() == 0
					|| id.equalsIgnoreCase("0")) {
				LOGGER.info("Invalid message for adding attachment");
				throw new GIException();
			}
			else {
				msg = inboxMsgService.findMessageById(Long.parseLong(id));
				if (msg == null) {
					throw new GIException();
				}
			}

			if (docId != null && docId.trim().length() != 0) {

				//LOGGER.info("Fetching " + docId + " from Alfresco");

				byte[] doc = ecmService.getContentDataById(docId.trim());
				Content docMetaData = ecmService.getContentById(docId.trim());

				if (doc != null && docMetaData != null) {

					InboxMsgDoc attachment = new InboxMsgDoc();
					attachment.setDocName(docMetaData.getTitle());
					attachment.setDocType(InboxMsgDoc.TYPE.ASCII);
					attachment.setCreatedOn(new TSDate());
					attachment.setDocSize(doc.length);

					attachments = msg.getMessageDocs();

					if (attachments == null || attachments.size() == 0) {
						LOGGER.info("Message has no previous attachments");
						attachments = new ArrayList<InboxMsgDoc>();
						msg.setMessageDocs(attachments);
					}
					//FIXME - Are we setting docs or Are we copying them in Alfresco?
					msg.getMessageDocs().add(attachment);
					LOGGER.info("Adding attachments from forwarded message to message");
					inboxMsgService.addAttachment(msg, doc);
				}
			}
		}
		catch (InvalidUserException e) {
			LOGGER.error(USER_EXCEPTION_STRING, e);
			return FAILURE;
		}
		catch (ContentManagementServiceException e) {
			LOGGER.error("Unable to add attachment", e);
			return FAILURE;
		}
		catch (GIException e) {
			LOGGER.error("Error adding attachemnt", e);
			return FAILURE;
		}
		finally {
			msg = null;
			attachments = null;
			LOGGER.info(CLASS_NAME + ".addAttachmentsFromForwardedMessage End");

		}
		return SUCCESS;
	}
}
