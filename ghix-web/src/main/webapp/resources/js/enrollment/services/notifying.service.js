(function() {
    'use strict';
    angular
        .module('reconciliationWorkbenchApp')
        .factory('NotifyingService', NotifyingService);

    NotifyingService.$inject = ['$rootScope'];

    function NotifyingService ($rootScope) {
        return {
            subscribe: function(scope, callback) {
                var handler = $rootScope.$on('monthlyActivityChanged', callback);
                scope.$on('$destroy', handler);
            },

            notify: function() {
                $rootScope.$emit('monthlyActivityChanged');
            }
        };
    }
})();