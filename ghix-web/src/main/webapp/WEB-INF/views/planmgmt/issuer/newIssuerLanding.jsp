<!-- 
 @author raja
 newIssuerLanding.jsp is the requirement of the new Issuer Portal
 @UserStory : HIX-1810
 @Jira-id : HIX-2450
 -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript" src="<gi:cdnurl value="/resources/js/preserveImgAspectRatio.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.validate.min.js" />"></script>

<%@ page isELIgnored="false"%>
	<div class="row-fluid">
		<div class="gutter10">
			<%-- <h1><issuerLogo:view/>${issuerObj.name}</h1> --%>
			<h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
		</div>
	</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.welcome"/></h4>
                </div>
                <!--  beginning of side bar -->
                <!--<ul class="nav nav-stacked paddingRL5">
                    <li>Please review your company details and complete the form to list it on the exchange.</li>
                </ul>-->
                <!--  beginning of side bar -->
                	<jsp:include page="profile/issuerDetailsLeftNav.jsp">
 	 	 	 			<jsp:param name="pageName" value="issuerDetails"/>
 	 	 	 	    </jsp:include>
                 <!-- show anonymous plan selection link start here  --> 
	            	<!--  show date picker start here -->
	                <div class="margin10">
	                 	<strong><spring:message  code="label.effectiveStartDate"/></strong>
	                 	<fmt:formatDate pattern="MM/dd/yyyy" value="${sysDate}" var="systemDate" />
						<div class="input-append date date-picker" id="date" data-date-format="mm/dd/yyyy">
	                		<input class="span10" type="text" id="effectiveStartDate" name="effectiveStartDate" pattern="MM/DD/YYYY">
	                		<span class="add-on"><i class="icon-calendar"></i></span>
						</div>
	                </div>
	                <!--  show date picker end here -->
             	    <div class="margin10">
             	   	  <a class="btn btn-primary btn-addpayment" id="redirectTo" onclick="checkeffectiveDate();" target="_blank"><spring:message  code="pgheader.viewConsumerShopping"/></a> 
                	</div>
                <!-- show anonymous plan selection link end here --> 	
                
                <!-- end of side bar -->
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="span10"><spring:message  code="pgheader.issuerInfo"/></h4>
                </div>
				<div class="gutter10">
					<form class="form-horizontal" id="frmIssuerLandingInfo" name="frmIssuerLandingInfo" action="<c:url value="/planmgmt/issuer/addrepresentatives" />" method="POST">
					<df:csrfToken/>
					<div class="profile">
						<table class="table table-border-none">
							<tbody>
								<tr>
									<td class="txt-right span4"><spring:message  code="label.name"/></td>
									<td><strong>${issuerObj.name}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.NAICCompanyCode"/>
										<a class="ttclass" rel="tooltip" href="#" data-original-title="<spring:message code='title.NAICCompanyCode'/>"><i class="icon-question-sign"></i></a>
									</td>
									<td><strong>${issuerObj.naicCompanyCode}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.NAICGroupCode"/>
										<a class="ttclass" rel="tooltip" href="#" data-original-title="<spring:message code='title.NAICGroupCode'/>"><i class="icon-question-sign"></i></a>
									</td>
									<td><strong>${issuerObj.naicGroupCode}</strong>
									</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.FederalEmployerId"/>
										<a class="ttclass" rel="tooltip" href="#" data-original-title="<spring:message code='title.FederalEmployerId'/>"><i class="icon-question-sign"></i></a>
									</td>
									<td><strong>${issuerObj.federalEin}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.HIOSIssuerId"/>
										<a class="ttclass" rel="tooltip" href="#" data-original-title="<spring:message code='title.HIOSIssuerId'/>"><i class="icon-question-sign"></i></a>
									</td>
									<td><strong>${issuerObj.hiosIssuerId}</strong></td>
								</tr>
							</tbody>
						</table><br />
                        <div class="header">
                            <h4 class="span10"><spring:message  code="pgheader.issuerAddress"/></h4>
                        </div>
                        <table class="table table-border-none">
                            <tr>
                                <td class="txt-right span4"><spring:message  code="label.addressLine1"/></td>
                                <td><strong>${issuerObj.addressLine1}</strong></td>
                            </tr>
                            <tr>
                                <td class="txt-right span4"><spring:message  code="label.addressLine2"/></td>
                                <td><strong>${issuerObj.addressLine2}</strong></td>
                            </tr>
                            <tr>
                                <td class="txt-right"><spring:message  code="label.city"/></td>
                                <td><strong>${issuerObj.city}</strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right"><spring:message  code="label.state"/></td>
                                <td><strong>${issuerObj.state}</strong></td>
                            </tr>
                            <tr>
                                <td class="txt-right"><spring:message  code="label.zipCode"/></td>
                                <td><strong>${issuerObj.zip}</strong></td>
                            </tr>
                           <!-- don't show start application button except issuer status = 'register'  --> 
                          <%--  <c:if test="${issuerObj.certificationStatus eq status_register && displayStartButton == 'YES'}" >
                             <tr>
                                <td class="txt-right span4">&nbsp;</td>
                                <td><a href='<c:url value="/planmgmt/issuer/representatives/add"/>' class="btn btn-primary"><spring:message code="label.startApplication"/></a></td>
                            </tr>
                            </c:if> --%>
						</table>
					</div>			
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
<script type="text/javascript">
$('.ttclass').tooltip();
$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();	
		var replaceExpr = /html"\>/gi;
	$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	})
				
	$('.date-picker').datepicker({
		startDate: '+'+ '${systemDate}' + 'd',
		autoclose: true,
	    format: 'mm/dd/yyyy'
	});	
		
	});
	
function checkeffectiveDate(){
	var effectiveStartDate = $('#effectiveStartDate').val();
	if(isDate(effectiveStartDate)){
		if(isValidDate(effectiveStartDate)){
			if(effectiveStartDate != ''){           
				$.ajax({
					url: "<c:url value='/setEffectiveDate/anonymous'/>",
					data: {"effectiveStartDate": effectiveStartDate, "csrftoken": $("#csrftoken").val()},
					type: "POST",
					success: function(response){
					if(response == "sessionExpire"){
						resultResp = response;
						window.location.href = "${pageContext.request.contextPath}" + "/account/user/login?confirmId=Y";
					}else{
						resultResp = response;
						if('${STATE_CODE}'.toUpperCase() == 'CA'){
							window.open("${pageContext.request.contextPath}" + "/private/quoteform#/");
						}else{
							window.open("${pageContext.request.contextPath}" + "/preeligibility#/");
						}
					}
				},
				error: function (response){
					resultResp = response;
					alert("Technical Issue Occured for Effective Start Date");
					window.location.href = "${pageContext.request.contextPath}" + "/account/user/login?confirmId=Y";
					}
				});
			}else{
				alert("Please Select Effective Start Date");
				$('#redirectTo').attr('href',"#");
				$('#redirectTo').attr('target',"_top");
			}
		}else{
			alert("Effective Date is always Current Date or Future Date");
			$('#redirectTo').attr('href',"#");
			$('#redirectTo').attr('target',"_top");			
		}
	}else{
		alert("Enter Valid Effective Date");
		$('#redirectTo').attr('href',"#");
		$('#redirectTo').attr('target',"_top");
	}
} 

function isValidDate(effectiveStartDate){
	var effectiveStartDateArr = effectiveStartDate.split('/');
	var sysDate =  '${systemDate}';
	var sysDateArr = sysDate.split('/');
	
	var mydate = new Date(effectiveStartDateArr[2], effectiveStartDateArr[0]-1, effectiveStartDateArr[1]);
	var currentDate = new Date(sysDateArr[2], sysDateArr[0]-1, sysDateArr[1]);
	
	if(currentDate <= mydate){
		return true;
	}else{
		return false;
	}
}
function isDate(txtDate)
{
  var currVal = txtDate;
  if(currVal == '')
    return false;
  
  //Declare Regex  
  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
  var dtArray = currVal.match(rxDatePattern); // is format OK?

  if (dtArray == null)
     return false;
 
  //Checks for mm/dd/yyyy format.
  dtMonth = dtArray[1];
  dtDay= dtArray[3];
  dtYear = dtArray[5];

  if(dtMonth < 1)
      return false;
  else if(dtMonth > 12)
	  return false;
  else if (dtDay < 1)
      return false;
  else if(dtDay > 31)
	  return false;
  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
      return false;
  else if (dtMonth == 2)
  {
     var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
     if (dtDay> 29 || (dtDay ==29 && !isleap))
          return false;
  }
  return true;
}
</script>
