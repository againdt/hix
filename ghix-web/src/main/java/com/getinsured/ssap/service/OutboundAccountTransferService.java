package com.getinsured.ssap.service;

/**
 * Outbound account transfer service.
 *
 * @author Yevgen Golubenko
 * @since 2019-07-10
 */
public interface OutboundAccountTransferService {
  /**
   * Invokes outbound account transfer.
   * @param applicationId SSAP id.
   */
  void invokeOutboundAccountTransfer(final long applicationId);
}
