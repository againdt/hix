package com.getinsured.eligibility.ssap.integration.household.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

/**
 * This class is used to log and eat up HTTP errors returned by Hub module REST API calls.
 * In case of HTTP errors default spring integration error handler throws MessagingException which stops the flow in between.
 * We need the flow to continue to the next endpoint which can decide based on the response received from Hub API's how to proceed further.
 * So, this class does nothing but logs the HTTP response status and allows the flow to continue normally. The Hub API's return a json string 
 * which is then sent to the downstream rule service activator's which decide how to proceed further using the json error response.
 */
@Component(value = "integrationErrorLoggingHandler")
public class IntegrationErrorLoggingHandler implements ResponseErrorHandler {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(IntegrationErrorLoggingHandler.class);

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        if (response.getStatusCode().value() != org.springframework.http.HttpStatus.OK.value()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        LOGGER.info(new StringBuilder().append("In IntegrationErrorLoggingHandler response code: ")
                .append(response.getStatusCode()).append("(").append(response.getStatusText())
                .append(")").toString());
    }

}
