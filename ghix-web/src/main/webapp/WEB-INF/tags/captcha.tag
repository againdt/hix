<%@ tag import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ tag import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<%@ tag import="com.getinsured.hix.platform.util.GhixConstants" %>

<%@ attribute name="privateKey" required="true" rtexprvalue="false" %>
<%@ attribute name="publicKey" required="true" rtexprvalue="false" %>
 
<%

	
	String env = (String) GhixConstants.ENVIRONMENT;
	
 	ReCaptcha c = null;
 	if(null!=env && env.equalsIgnoreCase("PROD")){
 		c = ReCaptchaFactory.newSecureReCaptcha(publicKey, privateKey, false);
 	}
 	else{
 		c = ReCaptchaFactory.newReCaptcha(publicKey, privateKey, false);		
 	}
 	
 	out.print(c.createRecaptchaHtml(null, null));
 %>
