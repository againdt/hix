package com.getinsured.hix.batch.enrollment.skip;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component("enrlReconSnapshotGenerationData")
public class EnrlReconSnapshotGenerationData {
	private  Map<String,List<Long>> enrollmentIdMap=null;
	
	public EnrlReconSnapshotGenerationData(){
		enrollmentIdMap= new HashMap<>();
	}
	
	public synchronized void putToEnrollmentIdMap(String key, List<Long> value){
		enrollmentIdMap.put(key, value);
	}
	public synchronized void putAllToEnrollmentIdMap(Map<String,List<Long>> enrollmentIdMap){
		this.enrollmentIdMap.putAll(enrollmentIdMap);
	}	
	
	public Map<String,List<Long>> getEnrollmentIdMap(){
		return enrollmentIdMap;
	}
	
	public synchronized void clearEnrollmentIdMap(){
		if(enrollmentIdMap!=null && !enrollmentIdMap.isEmpty()){
			this.enrollmentIdMap.clear();
		}
	}


}
