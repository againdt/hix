package com.getinsured.iex.hub.ifsv.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.ifsv.cms.hix._0_1.hix_ee.TaxFilerType;
import com.getinsured.iex.hub.ifsv.cms.hix._0_1.hix_ee.TaxReturnType;
import com.getinsured.iex.hub.ifsv.niem.niem.proxy.xsd._2.GYear;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

/**
 * Wrapper class for com.getinsured.iex.hub.ifsv.cms.hix._0_1.hix_ee.TaxReturnType;
 * 
 * @author Nikhil Talreja
 * @since 27-Feb-2014
 *
 */
public class TaxReturnTypeWrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TaxReturnTypeWrapper.class);
	
	private TaxReturnType taxReturnType;
	
	private TaxFilerTypeWrapper primaryTaxFiler;
	private TaxFilerTypeWrapper spouseTaxFiler;
	private String taxReturnYear;
	private String taxReturnFilingStatusCode;
	private String taxReturnAGIAmount;
	private String taxReturnMAGIAmount;
	private String taxReturnTaxableSocialSecurityBenefitsAmount;
	private String taxReturnTotalExemptionsQuantity;
	
	private static final String PRIMARY_TAX_FILER_KEY = "PrimaryTaxFiler";
	private static final String SPOUSE_TAX_FILER_KEY = "SpouseTaxFiler";
	private static final String RETURN_YEAR_KEY = "TaxReturnYear";
	private static final String RETURN_STATUS_CODE_KEY = "TaxReturnFilingStatusCode";
	private static final String AGI_KEY = "TaxReturnAGIAmount";
	private static final String MAGI_KEY = "TaxReturnMAGIAmount";
	private static final String SS_AMOUNT_KEY = "TaxReturnTaxableSocialSecurityBenefitsAmount";
	private static final String EXEMEPTION_QUANTITY_KEY = "TaxReturnTotalExemptionsQuantity";
	
	
	public TaxReturnTypeWrapper(TaxReturnType taxReturnType){
		
		TaxFilerType primaryType = taxReturnType.getPrimaryTaxFiler();
		if(primaryType != null){
			this.primaryTaxFiler = new TaxFilerTypeWrapper(primaryType);
		}
		TaxFilerType spouseFiler = taxReturnType.getSpouseTaxFiler();
		if(spouseFiler != null){
			this.spouseTaxFiler = new TaxFilerTypeWrapper(spouseFiler);
		}
		
		try {
			GYear taxReturnYearType = taxReturnType.getTaxReturnYear();
			if(taxReturnYearType != null){
				this.taxReturnYear = PlatformServiceUtil.xmlDateToString(taxReturnType.getTaxReturnYear().getValue(), "yyyy");
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while setting tax return year ",e);
		} 
		
		if(taxReturnType.getTaxReturnFilingStatusCode() != null){
			this.taxReturnFilingStatusCode = taxReturnType.getTaxReturnFilingStatusCode().getValue();
		}
		
		if(taxReturnType.getTaxReturnAGIAmount() != null){
			this.taxReturnAGIAmount = taxReturnType.getTaxReturnAGIAmount().getValue().toString();
		}
		
		if(taxReturnType.getTaxReturnMAGIAmount() != null){
			this.taxReturnMAGIAmount = taxReturnType.getTaxReturnMAGIAmount().getValue().toString();
		}
		
		if(taxReturnType.getTaxReturnTaxableSocialSecurityBenefitsAmount() != null){
			this.taxReturnTaxableSocialSecurityBenefitsAmount = taxReturnType.getTaxReturnTaxableSocialSecurityBenefitsAmount().getValue().toString();
		}
		
		if(taxReturnType.getTaxReturnTotalExemptionsQuantity() != null){
			this.taxReturnTotalExemptionsQuantity = taxReturnType.getTaxReturnTotalExemptionsQuantity().getValue().toString();
		}
	}

	public TaxReturnType getTaxReturnType() {
		return taxReturnType;
	}

	public TaxFilerTypeWrapper getPrimaryTaxFiler() {
		return primaryTaxFiler;
	}

	public TaxFilerTypeWrapper getSpouseTaxFiler() {
		return spouseTaxFiler;
	}

	public String getTaxReturnYear() {
		return taxReturnYear;
	}

	public String getTaxReturnFilingStatusCode() {
		return taxReturnFilingStatusCode;
	}

	public String getTaxReturnAGIAmount() {
		return taxReturnAGIAmount;
	}

	public String getTaxReturnMAGIAmount() {
		return taxReturnMAGIAmount;
	}

	public String getTaxReturnTaxableSocialSecurityBenefitsAmount() {
		return taxReturnTaxableSocialSecurityBenefitsAmount;
	}


	public String getTaxReturnTotalExemptionsQuantity() {
		return taxReturnTotalExemptionsQuantity;
	}


	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.primaryTaxFiler != null){
			obj.put(PRIMARY_TAX_FILER_KEY, this.primaryTaxFiler);
		}
		if(this.spouseTaxFiler != null){
			obj.put(SPOUSE_TAX_FILER_KEY, this.spouseTaxFiler);
		}
		if(this.taxReturnYear != null){
			obj.put(RETURN_YEAR_KEY, this.taxReturnYear);
		}
		if(this.taxReturnFilingStatusCode != null){
			obj.put(RETURN_STATUS_CODE_KEY, this.taxReturnFilingStatusCode);
		}
		if(this.taxReturnAGIAmount != null){
			obj.put(AGI_KEY, this.taxReturnAGIAmount);
		}
		if(this.taxReturnMAGIAmount != null){
			obj.put(MAGI_KEY, this.taxReturnMAGIAmount);
		}
		if(this.taxReturnTaxableSocialSecurityBenefitsAmount != null){
			obj.put(SS_AMOUNT_KEY, this.taxReturnTaxableSocialSecurityBenefitsAmount);
		}
		if(this.taxReturnTotalExemptionsQuantity != null){
			obj.put(EXEMEPTION_QUANTITY_KEY, this.taxReturnTotalExemptionsQuantity);
		}
		return obj.toJSONString();
	}
	
}
