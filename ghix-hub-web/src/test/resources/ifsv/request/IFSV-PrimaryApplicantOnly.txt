{
   "taxHousehold": [
      {
		"householdMember": [
			{
		         "personId": 1,
		         "name": {
						"firstName": "carl",
		                "middleName": "C",
		                "lastName": "Jose"
		          },
		          "socialSecurityCard": {
		                "socialSecurityNumber": "660188401"
		          },
		         "taxFilerCategoryCode": "PRIMARY"
			}
		]
	  }
   ],
   "requestID": "111"
}