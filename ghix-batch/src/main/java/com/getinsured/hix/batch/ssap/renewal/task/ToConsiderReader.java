package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.ssap.renewal.service.ToConsiderService;
import com.getinsured.hix.batch.ssap.renewal.util.RenewalUtils;
import com.getinsured.hix.batch.ssap.renewal.util.ToConsiderPartitionerParams;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * ToConsiderReader class is used to get Application IDs from Partitioner and get data from database.
 * 
 * @since July 19, 2020
 */
public class ToConsiderReader implements ItemReader<SsapApplication> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ToConsiderReader.class);
	private List<SsapApplication> readerSsapApplicationList;
	private int partition;
	private int loopCount;
	private int startIndex;
	private int endIndex;

	private ToConsiderService toConsiderService;
	private ToConsiderPartitionerParams toConsiderPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.info(Thread.currentThread().getName() + " : before Step execution for Reader ");
		
		ExecutionContext executionContext = stepExecution.getExecutionContext();

		if (executionContext != null) {
			partition = executionContext.getInt("partition");
			startIndex = executionContext.getInt("startIndex");
			endIndex = executionContext.getInt("endIndex");
			LOGGER.info("partition: {}, startIndex: {}, endIndex: {}", partition, startIndex, endIndex);

			List<Long> readerSsapApplicationIdList = null;
			if (toConsiderPartitionerParams != null && CollectionUtils.isNotEmpty(toConsiderPartitionerParams.getSsapApplicationIdList())) {

				if (startIndex < toConsiderPartitionerParams.getSsapApplicationIdList().size()
						&& endIndex <= toConsiderPartitionerParams.getSsapApplicationIdList().size()) {
					readerSsapApplicationIdList = new CopyOnWriteArrayList<Long>(toConsiderPartitionerParams.getSsapApplicationIdList().subList(startIndex, endIndex));
				}
			}
			
			if(CollectionUtils.isNotEmpty(readerSsapApplicationIdList))
			{
				readerSsapApplicationList = new CopyOnWriteArrayList<SsapApplication>();
				List<List<Long>>  chunkedList = RenewalUtils.chunkList(readerSsapApplicationIdList,RenewalUtils.INNER_QUERY_CHUNK_SIZE);
				
				for(List<Long> list : chunkedList)
				{
					List<SsapApplication> applicationList = toConsiderService.getSsapApplicationListByIds(list);
					readerSsapApplicationList.addAll(new CopyOnWriteArrayList<SsapApplication>(applicationList) );
				}
			}
		}
	}

	@Override
	public SsapApplication read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		SsapApplication readerSsapApplication = null;

		if (CollectionUtils.isNotEmpty(readerSsapApplicationList)) {

			if (loopCount < readerSsapApplicationList.size()) {
				loopCount++;
				readerSsapApplication = readerSsapApplicationList.get(loopCount - 1);
			}
		}
		else{
			LOGGER.error("No SsapApplications found in ToConsiderReader.read()");
		}
		
		return readerSsapApplication;
	}

	public ToConsiderService getToConsiderService() {
		return toConsiderService;
	}

	public void setToConsiderService(ToConsiderService toConsiderService) {
		this.toConsiderService = toConsiderService;
	}

	public ToConsiderPartitionerParams getToConsiderPartitionerParams() {
		return toConsiderPartitionerParams;
	}

	public void setToConsiderPartitionerParams(
			ToConsiderPartitionerParams toConsiderPartitionerParams) {
		this.toConsiderPartitionerParams = toConsiderPartitionerParams;
	}
}
