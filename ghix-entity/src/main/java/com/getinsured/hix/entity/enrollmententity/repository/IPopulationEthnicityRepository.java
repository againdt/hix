package com.getinsured.hix.entity.enrollmententity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.entity.PopulationEthnicity;

@Repository("populationEthnicityRepository")
public interface IPopulationEthnicityRepository extends
		JpaRepository<PopulationEthnicity, Integer> {

	PopulationEthnicity findById(int ethnicityId);

	List<PopulationEthnicity> findByEntityId(int entityId);

	@Modifying
	@Transactional
	@Query("delete from PopulationEthnicity pe where pe.entity.id = :entityId")
	int deleteAssociatedEthnicities(@Param("entityId") int entityId);
}
