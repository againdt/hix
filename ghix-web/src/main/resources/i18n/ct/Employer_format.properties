# Employer FAQ
#----------------------------------------------------------------------
label.employergroup1 = Benefits of YHI SHOP
label.employergroup2 = Manage plans for employee
label.employergroup3 = Manage plans for employer
label.employergroup4 = Manage plans for business
label.employergroup5 = About reference plans


label.employerWhatBenefits = What are the benefits of YHI SHOP?
label.employerBenefits1 = YHI provides an online marketplace where small businesses can shop for group health insurance plans. YHI offers:
label.employerBenefits1.offers1 = Single point of entry for small businesses and their employees to apply for coverage.
label.employerBenefits1.offers2 = Small businesses with 25 or fewer full-time equivalent employees and an average salary less than $50,000 per year may qualify for the Small Business Tax Credit worth up to 50% of the employer's premium contribution.
label.employerBenefits2 = Small businesses select a level of plan benefits that they would like to offer their employees.   Benefits levels are bronze, silver, gold, or platinum and are referred to as "metal levels". Employers will select a metal level of coverage and employees will select from a choice of plans within that level.

label.employerHowChooseInsurance = How do I choose insurance plans to offer my employees? 
label.employerChooseInsurance = Once you register as an Employer and you have uploaded your employee information, you may select a metal level of benefits that you would like to offer your employees. Your employees may choose any plan within the level you have selected.

label.employerHowExtraHelp = Can I get extra help selecting plans to offer my employees? 
label.employerExtraHelp = Yes, just click <strong>Get Assistance</strong> at the top of the page to find a local insurance agent to help you.

label.employerHowEmployeesSelect = Can my employees select any plan they want?
label.employerEmployeesSelect = Your employees have the option to select any plan <strong>within</strong> the metal level you selected. 

label.employerWhatAffordabilityIssues = What do I do if some of my employees have affordability issues?
label.employerAffordabilityIssues.youCan  = You can: 
label.employerAffordabilityIssues.youCan1  = Increase the employer contribution towards employee coverage.
label.employerAffordabilityIssues.youCan2  = Choose a lower metal level insurance plan from among the Bronze, Silver, Gold, and Platinum levels.
label.employerAffordabilityIssues.youCan3  = Ignore the affordability issues. There are no associated tax penalties if you have fewer than 

label.employerHowActivateEnrollment = How do I activate employee open enrollment? 
label.employerActivateEnrollment1 = Start by selecting "Set Up Coverage" from the "Coverage" menu.
label.employerActivateEnrollment2 =	Click <strong>"Confirm"</strong> after making your choices and reviewing your selections and associated costs. 
label.employerActivateEnrollment3 = Then, click <strong>"Yes"</strong> on the <strong>"Start open enrollment?"</strong> confirmation box to confirm your selections for employee coverage.
label.employerActivateEnrollmentStartTime = Employee open enrollment will start on the first day of the next month.

label.employerWhatAffordableHealthcare = What is considered affordable employer sponsored healthcare?
label.employerAffordableHealthcare = If an employee's share of the premium for the least expensive plan is less than 9.5% of their gross income, the plan is considered affordable. 

label.employerWhatIfUnaffordable = What happens if an employer doesn't offer affordable coverage to a full-time employee?
label.employerUnaffordable = The employee is eligible to purchase insurance coverage on their own through the health insurance exchange instead of through the company. Additionally, the employee may be eligible for government subsidies or tax credits based on their gross income. 

label.employerHowQualifySmallBusiness = How do I qualify for the Small Business Tax Credit? 
label.employerQualifySmallBusiness = Details vary according to specific circumstances but in general you must:
label.employerQualifySmallBusiness.detail1 = Employ fewer than 25 full-time equivalent employees.
label.employerQualifySmallBusiness.detail2 = Provide an average annual salary of less than $50,000.
label.employerQualifySmallBusiness.detail3 = Offer healthcare to all full-time employees working 30 hours or more per week.
label.employerQualifySmallBusiness.detail4 = Contribute at least 50% towards the cost of healthcare for full-time employees.


label.employerHowApplySmallBusiness = How do I apply for Small Business Tax Credit for healthcare?
label.employerApplySmallBusiness = Complete the Internal Revenue Service form 8941 and include it when you file taxes for your business. This form is available at http://www.irs.gov/pub/irs-pdf/f8941.pdf

label.employerHowAvoidPenalties = How can my business avoid tax penalties?
label.employerAvoidPenalties = The tax penalty for not providing healthcare coverage applies only to employers with over 50 employees and won't go into effect until 2015. 

label.employerWhetherMonthlyCosts = Can my monthly costs go up?
label.employerMonthlyCosts = Once you've confirmed your plan choices, your costs are set for the plan year unless you add employees or dependents.

label.employerWhoSignedUp = How do I see which of my employees have signed up for coverage?
label.employerSignedUp = On the "Reports" menu, click "Participation Report", then click "Detail View". You'll see a list of employees and the enrollment status of each person, as well as the participation rate for the whole group.

label.employerIfNotMeetMinimum = What happens if my group doesn't meet the minimum participation rate?
label.employerNotMeetMinimum = You may need to select a different metal level of plans or a higher employer contribution percentage In order to incentivize a greater number of your employees to participate. If your group does not meet the minimum participation rate set by YHI, you will have the opportunity to start the plan selection process again. 

label.employerCanOfferDental = Can I offer dental benefits to my employees via YHI? 
label.employerOfferDental = As part of the shopping experience on YHI, your employees are currently offered the opportunity to purchase dental coverage for their minor children only. Family dental coverage will be available for them to select in the near future. In the meantime, if you\u2019d like your employees to shop for dental coverage outside of YHI, click <a href="#" target="_blank">here</a> for a list of dental insurance carriers in Idaho.

label.employerWhatIsReferencePlanQ = What is a Reference Plan and why do I need to choose one?
label.employerWhatIsReferencePlanA = A reference plan is used only to calculate your premiums. The metallic plan level and the average cost of the reference plan you choose are important factors in determining your health insurance expenses as an employer. The reference plan allows you to \u201Clock in\u201D your monthly cost as long as new employees and dependents are not added to your roster. After you select your reference plan your employees can pick any plan within the metallic level you select. All plans within a given level have the same actuarial value, so they are roughly equivalent in terms of percentage of treatments costs that will be covered. For example, all Silver plans have an actuarial value of 70%, which means that, on average, they cover 70% of the cost of healthcare visits and treatments.  

label.employerHowDoesTheReferencePlanWorkQ = How does the Reference Plan work?
label.employerHowDoesTheReferencePlanWorkA-part1 = Each employee is quoted against the reference plan based on age to determine the cost of the premium for that specific employee.  Then, your contribution for that employee is calculated by multiplying that employee\u2019s premium by the contribution percentage.  The resulting dollar amount is your contribution for that employee, regardless of which plan the employee eventually chooses.  If the employee chooses a cheaper plan, the contribution covers a higher percentage, but the dollar amount remains the same(up to the maximum contribution amount.)
label.employerHowDoesTheReferencePlanWorkA-part2 = For example, Acme Widget has 3 employees, as shown below. The business owner chose Silver Plan A as the reference plan and 50% as the contribution.
label.employerTableHeader-1 = Employee Contribution
label.employerTableHeader-2 = Premium for Silver Plan A
label.employerTableHeader-3 = Employer
label.employerHowDoesTheReferencePlanWorkA-part3 = If Judy picks Silver Plan B, and her premium for that specific plan is $200, she still only gets $75 contribution from her employer.  She pays the difference.

label.employerCanISeeAllPlansQ = Can I see all the plans my employees may have access to?
label.employerCanISeeAllPlansA = Yes, click one of the links below to see a summary of the plans available in Idaho within each metallic level.  When employees shop, they will see only plans available in your county.

label.employerBronzePlans = Bronze Plans
label.employerSilverPlans = Silver Plans
label.employerGoldPlans = Gold Plans
label.employerPlatinumPlans = Platinum Plans
label.sendActivationLink=Send Activation Link

label.employer.companynotsetforemployermessage = No company found for employer. Please complete the company setup process before designating an Agent.
 



