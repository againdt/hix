<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script type="text/ng-template" id="multipleHousehold">
    <form ng-init="initMultiHousehold()" ng-controller="indPortalAppCtrl" name="multiHouseholdForm" id="multiHouseholdForm"
          ng-submit="setActiveHousehold(multiHouseholdForm)">
        <div id="multiHouseholdModal" class="modal hide fade ng-scope" tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="confirmApplyModal" aria-hidden="true">
            <div ng-if="userHouseholds === null">
                <div class="modal-header">
                    <h3><spring:message code="indportal.multiHousehold.error.noHouseholdsHeader"/></h3>
                </div>
                <div class="modal-body">
                    <p><spring:message code="indportal.multiHousehold.error.noHouseholdsBody"/></p>
                </div>
                <div class="modal-footer form-actions">
                </div>
            </div>
            <div ng-if="userHouseholds !== null">
                <div class="modal-header">
                    <h3 id="confirmApplyModal"><spring:message code="indportal.multiHousehold.headerSelect"/></h3>
                    <p><spring:message code="indportal.multiHousehold.headerParagraph"/></p>
                    <p><small><spring:message code="indportal.multiHousehold.headerNote"/></small></p>
                </div>
                <div class="modal-body">
                    <div class="form-group" style="border: 1px solid #e5e5e5;margin: 0.5em 0em" ng-repeat="household in userHouseholds.households">
                        <div style="padding-top: 1em;">
                            <spring:message code="indportal.multiHousehold.householdAriaLabel" var="householdLabel"/>
                            <label ng-if="household.active" for="{{household.id}}" class="control-label">
                                <input id="{{household.id}}" style="margin-left: 1em" type="radio" name="households" ng-value="household.id" ng-model="multiHouseholdForm.householdId" ng-required="true" aria-label="${householdLabel}">
                                <spring:message code="indportal.multiHousehold.householdAriaLabelActive"/>
                            </label>
                            <label ng-if="!household.active" for="{{household.id}}" class="control-label">
                                <input id="{{household.id}}" style="margin-left: 1em" type="radio" name="households" ng-value="household.id" ng-model="multiHouseholdForm.householdId" ng-required="true" aria-label="${householdLabel}">
                                <spring:message code="indportal.multiHousehold.householdLabel"/> <b><spring:message code="indportal.multiHousehold.householdLabelInactive"/></b>
                            </label>
                        </div>
                        <div style="padding-left: 2.7em;">
                            <p ng-repeat="member in household.members" style="margin: 0 0 0 2px">
                                {{member.firstName}} {{member.middleName}} {{member.lastName}} {{member.suffix}}
                            </p>
                        </div>
                    </div>
                    <div ng-if="multiHouseholdError">
                        <p><b><spring:message code="indportal.multiHousehold.error.setHousehold"/></b></p>
                    </div>
                </div>
                <div class="modal-footer form-actions">
                    <button class="btn btn-primary" type="submit"><spring:message code="indportal.multiHousehold.continue"/></button>
                </div>
            </div>
        </div>
    </form>
</script>