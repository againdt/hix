package com.getinsured.hix.ridp.service;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.ridp.exception.RIDPServiceException;
import com.getinsured.hix.ridp.repository.ConsumerDocumentRepository;
import com.getinsured.hix.ridp.util.RIDPConstants;
import com.getinsured.hix.util.TicketMgmtUtils;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.util.TSDate;

/**
 * RIDP service to abstract RIDP hub module calls and store the RIDP verification input data 
 * and verification results to database 
 * 
 * @author Nikunj Singh
 * @since 3/16/2014
 */

@Service("ridpService")
public class RIDPServiceImpl implements RIDPService {
	
	private static final String SUCCESS_RESPONSE = "Success";

    private static final String YES = "YES";

    private static final String RIDP_CLAIM_URL = "http://wso2.org/claims/ridpFlag";

    private static final String PAYLOAD = "payload";

    private static final String CLAIM_VALUE = "claimValue";

    private static final String CLAIM_URI = "claimURI";

    private static final String USER_NAME = "userName";

    private static final String CRM_VIEWMEMBER_PATH = "/crm/member/viewmember/";

	private static final String CSR_ROLE_NAME = "CSR";
	
	private static final String SUPERVISOR_ROLE_NAME = "SUPERVISOR";

    private static final String SUCCESS_RESPONSE_CODE = "HS000000";
    
    private static final String ERROR_RESPONSE_CODE = "HE999999";

	private static final String SERVICE_CONTACT_CALL_CENTER_NUMBER = "(866) 578-5409";

	private static final String SERVICE_CONTACT_NAME = "Experian";

	private static final String RIDP_VERIFIED_CODE = "ACC";
	
    @Value("#{configProp['scim.enabled'] != null ? configProp['scim.enabled'] : 'false'}") 
	private String IS_RIDP_WSO2_SYNC_REQD;
    
    @Value("#{configProp['fdh.cms_partner'] != null ? configProp['fdh.cms_partner'] : 'NOT_PROVIDED'}") 
   	private String cms_partner;
	
	private final JSONParser jsonParser = new JSONParser();
	
	@Autowired private ContentManagementService ecmService;
	
	@Autowired private RoleService roleService;
	
	@Autowired private ConsumerDocumentRepository consumerDocumentRepository;

	@Autowired private TicketMgmtUtils ticketMgmtUtils;
	
	@Value("#{configProp['ridp.maxdays.in.fars']}")
	private int maxDaysInFARS;

	@Autowired
	private UserService userService;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ConsumerPortalUtil consumerUtil;

	private static final Logger LOGGER = Logger.getLogger(RIDPServiceImpl.class);

    private static final Object INDIVIDUAL_ROLE = "INDIVIDUAL";

	@SuppressWarnings("unchecked")
	@Override
	public String clearDHSReferenceNumber() throws RIDPServiceException {
		AccountUser currentUser = getLoggedInUser();
		Household household = consumerUtil.getHouseholdRecord(currentUser.getActiveModuleId());
		household.setDshReferenceNumber(null);
		household.setRidpSource(null);
		updateRidpDate(household, true);
		saveHouseHold(household);
		JSONObject response = new JSONObject();
		response.put(RIDPConstants.RESPONSE_CODE_FIELD, SUCCESS_RESPONSE_CODE);
		return response.toJSONString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public String validatePrimaryDetails(String primaryDetailJson) throws RIDPServiceException {
		LOGGER.info("Inside RIDPService validatePrimaryDetails");
		Household household = null;
		Location location = null;
		Object formFieldData = null;
		AccountUser currentUser = null;
		String errorMessage = null;
		String jsonResponse = null;
		//boolean hubDownFlag = false;
		JSONObject jsonResponseObj;
		
		JSONObject jsonRequestObj = removeBlankFields(primaryDetailJson);
		jsonRequestObj.put("cms_partner", this.cms_partner);
		try {
			jsonResponse = restTemplate.postForObject(GhixEndPoints.HubIntegrationEndpoints.RIDP_PRIMARY_URL, jsonRequestObj.toJSONString(), String.class);
			if(isHubDown(jsonResponse)) {
				return jsonResponse;
			}
			jsonResponseObj = (JSONObject) jsonParser.parse(jsonResponse);
		} catch (RestClientException | ParseException exception) {
			LOGGER.error("Error invoking hub service for validating RIDP Primary with root cause", exception);
			return PlatformServiceUtil.exceptionToJson(exception);
		}
		JSONObject responseMetadata = (JSONObject) jsonResponseObj.get(RIDPConstants.RESPONSE_METADATA);
		String responseCode = (String) responseMetadata.get(RIDPConstants.RESPONSE_CODE_FIELD);
		if(RIDPConstants.isError(responseCode)){
			return jsonResponse;
		}
		currentUser = getLoggedInUser();
		household = consumerUtil.getHouseholdRecord(currentUser.getActiveModuleId());
		
		if (null == household) {
			errorMessage = "No household record associated with user.";
			LOGGER.error(errorMessage);
			throw new RIDPServiceException(errorMessage);
		}

		household.setFirstName(getFieldValue(jsonRequestObj.get("personGivenName")));
		household.setMiddleName(getFieldValue(jsonRequestObj.get("personMiddleName")));
		household.setLastName(getFieldValue(jsonRequestObj.get("personSurName")));
		household.setNameSuffix(getFieldValue(jsonRequestObj.get("personNameSuffixText")));
		household.setPhoneNumber(getFieldValue(jsonRequestObj.get("fullTelephoneNumber")));

		location = household.getLocation();

		if (location == null) {
			location = new Location();
			household.setLocation(location);
		}

		location.setAddress1(getFieldValue(jsonRequestObj.get("streetName")));
		location.setCity(getFieldValue(jsonRequestObj.get("locationCityName")));
		location.setState(getFieldValue(jsonRequestObj.get("locationStateUSPostalServiceCode")));
		location.setZip(getFieldValue(jsonRequestObj.get("locationPostalCode")));

		formFieldData = getFieldValue(jsonRequestObj.get("locationPostalExtensionCode"));
		if(null != formFieldData && StringUtils.isNotBlank(formFieldData.toString())) {
			location.setAddOnZip(Integer.parseInt(formFieldData.toString()));
		} else {
			location.setAddOnZip(0);
		}
		
		formFieldData = getFieldValue(jsonRequestObj.get("personBirthDate"));
		if (null != formFieldData && StringUtils.isNotBlank(formFieldData.toString())) {
			String dateOfBirthString = formFieldData.toString();
			try {
				household.setBirthDate(new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH).parse(dateOfBirthString));
			} catch (java.text.ParseException parseException) {
				errorMessage = "Unable to parse date of birth from request " + dateOfBirthString;
				LOGGER.error(errorMessage, parseException);
				saveHouseHold(household);
				throw new RIDPServiceException(errorMessage, parseException);
			}
		} else {
			household.setBirthDate(null);
		}
		
		JSONObject verificationResponseJsonObj = (JSONObject) jsonResponseObj.get(RIDPConstants.VERIFICATION_RESPONSE);
		if(verificationResponseJsonObj != null){
			String finalDecisionCode = (String)verificationResponseJsonObj.get(RIDPConstants.FINAL_DECISION_FIELD);
			String refNumber = (String) verificationResponseJsonObj.get(RIDPConstants.DHS_REFERENCE_NUMBER);
			boolean farsResponse = RIDPConstants.isFarsVarificationRequired(finalDecisionCode);
			if(farsResponse){
				household.setDshReferenceNumber(refNumber);
				household.setRidpSource(RIDPConstants.RIDP_VERIFY_SOURCE_FARS);
				updateRidpDate(household, false);
				verificationResponseJsonObj.put(RIDPConstants.SERVICE_CONTACT_NAME, SERVICE_CONTACT_NAME);
				verificationResponseJsonObj.put(RIDPConstants.SERVICE_CONTACT_CALL_CENTER_NUMBER, SERVICE_CONTACT_CALL_CENTER_NUMBER);
				jsonResponseObj.put(RIDPConstants.VERIFICATION_RESPONSE, verificationResponseJsonObj);
				
			}
			if(RIDPConstants.isManualVarificationRequired(finalDecisionCode)) {
				household.setRidpSource(RIDPConstants.RIDP_VERIFY_SOURCE_MANUAL);
				updateRidpDate(household, false);
			}
		}
		saveHouseHold(household);
		//Mapping to UI response
		jsonResponseObj.put("responseCode", responseCode);
		jsonResponse = jsonResponseObj.toJSONString();
		jsonResponse=jsonResponse.replace(RIDPConstants.VERIFICATION_RESPONSE, RIDPConstants.UI_VERIFICATION_RESPONSE);
		jsonResponse=jsonResponse.replace(RIDPConstants.DHS_REFERENCE_NUMBER, RIDPConstants.UI_DHS_REFERENCE_NUMBER);
		jsonResponse=jsonResponse.replace(RIDPConstants.FINAL_DECISION_FIELD, RIDPConstants.RIDP_PRIMARY_FINAL_DECISION_FIELD);
		jsonResponse=jsonResponse.replace("SessionIdentifier", "sessionIdentifier");
		return jsonResponse;
	}
	

	private boolean isHubDown(String jsonResponse) {
//		if(null != jsonResponse && (jsonResponse.contains("WebServiceIOException") || 
//				jsonResponse.contains("HttpHostConnectException") || jsonResponse.contains("ConnectException"))) {
		if(null != jsonResponse && jsonResponse.contains("Exception")) {
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String validateSecondaryDetails(String secondaryDetailJson) throws RIDPServiceException {
		LOGGER.info("Inside RIDPService validateSecondaryDetails");
		JSONObject jsonRequest = removeBlankFields(secondaryDetailJson);
		jsonRequest.put("cms_partner", this.cms_partner);
		String jsonResponse = null;
		JSONObject jsonResponseObj = null;
		try {
			jsonResponse = restTemplate.postForObject(GhixEndPoints.HubIntegrationEndpoints.RIDP_SECONDARY_URL,	jsonRequest.toJSONString(), String.class);
			if(isHubDown(jsonResponse)) {
				return jsonResponse;
			}
			jsonResponseObj = (JSONObject) jsonParser.parse(jsonResponse);
		} catch (Exception exception) {
			LOGGER.error("Error invoking hub service for validating RIDP secondary", exception);
			return hubDownResponse();
		}
		JSONObject responseMetadata = (JSONObject) jsonResponseObj.get(RIDPConstants.RESPONSE_METADATA);
		String responseCode = (String) responseMetadata.get(RIDPConstants.RESPONSE_CODE_FIELD);
		if(RIDPConstants.isError(responseCode)){
			return jsonResponse;
		}
		
		AccountUser currentUser = getLoggedInUser();
		JSONObject verificationResponseJsonObj = (JSONObject) jsonResponseObj.get(RIDPConstants.VERIFICATION_RESPONSE);
		if(verificationResponseJsonObj != null){
			String finalDecisionCode = (String) verificationResponseJsonObj.get(RIDPConstants.FINAL_DECISION_FIELD);
			if(finalDecisionCode.equalsIgnoreCase(RIDP_VERIFIED_CODE)){
				Household household = consumerUtil.getHouseholdRecord(currentUser.getActiveModuleId());
				household.setRidpSource(RIDPConstants.RIDP_VERIFY_SOURCE_RIDP);
				updateRidpDate(household, false);
				setRidpVerified(household);
				saveHouseHold(household);
			}else if(RIDPConstants.isFarsVarificationRequired(finalDecisionCode)){
				Household household = consumerUtil.getHouseholdRecord(currentUser.getActiveModuleId());
				household.setDshReferenceNumber(verificationResponseJsonObj.get(RIDPConstants.DHS_REFERENCE_NUMBER).toString());
				household.setRidpSource(RIDPConstants.RIDP_VERIFY_SOURCE_FARS);
				updateRidpDate(household, false);
				saveHouseHold(household);
				verificationResponseJsonObj.put(RIDPConstants.SERVICE_CONTACT_NAME, SERVICE_CONTACT_NAME);
				verificationResponseJsonObj.put(RIDPConstants.SERVICE_CONTACT_CALL_CENTER_NUMBER, SERVICE_CONTACT_CALL_CENTER_NUMBER);
				jsonResponseObj.put(RIDPConstants.VERIFICATION_RESPONSE, verificationResponseJsonObj);
				
			}else if(RIDPConstants.isManualVarificationRequired(finalDecisionCode)){
				Household household = consumerUtil.getHouseholdRecord(currentUser.getActiveModuleId());
				household.setRidpSource(RIDPConstants.RIDP_VERIFY_SOURCE_MANUAL);
				updateRidpDate(household, false);
				saveHouseHold(household);
			}
		}
		//Mapping to UI response
		jsonResponseObj.put("responseCode", responseCode);
		jsonResponse = jsonResponseObj.toJSONString();
		jsonResponse=jsonResponse.replace(RIDPConstants.VERIFICATION_RESPONSE, RIDPConstants.UI_VERIFICATION_RESPONSE);
		jsonResponse=jsonResponse.replace(RIDPConstants.FINAL_DECISION_FIELD, RIDPConstants.RIDP_PRIMARY_FINAL_DECISION_FIELD);
		jsonResponse=jsonResponse.replace(RIDPConstants.DHS_REFERENCE_NUMBER, RIDPConstants.UI_DHS_REFERENCE_NUMBER);
		jsonResponse=jsonResponse.replace("SessionIdentifier", "sessionIdentifier");
		return jsonResponse;
	}

    private void setRidpVerified(Household household) throws RIDPServiceException {
        household.setRidpVerified(RIDPConstants.HOUSEHOLD_RIDP_VERIFIED);
        household.setRidpDate(new TSDate());
        sendRidpVerificationToWso2(household.getUser());
    }
	
	@Override
    @SuppressWarnings("unchecked")
    public String sendRidpVerificationToWso2(AccountUser user) throws RIDPServiceException  {
	    JSONObject jsonResponse = new JSONObject();
	    jsonResponse.put(RIDPConstants.RESPONSE_CODE_FIELD, SUCCESS_RESPONSE_CODE);
	    
	    if(user != null) {
	        if(Boolean.valueOf(IS_RIDP_WSO2_SYNC_REQD)) {
	            JSONObject request = new JSONObject();
	            try {
	                request.put("clientIp", InetAddress.getLocalHost().getHostAddress());
	                JSONObject payload = new JSONObject();
	                payload.put(USER_NAME, user.getEmail());
	                payload.put(CLAIM_URI, RIDP_CLAIM_URL);
	                payload.put(CLAIM_VALUE, YES);
	                request.put(PAYLOAD, payload);
	                String response = restTemplate.postForObject(GhixEndPoints.IdentityServiceEndPoints.WSO2_UPDATE_USER_URL, request.toJSONString(), String.class);
	                if(StringUtils.contains(SUCCESS_RESPONSE, response)) {
	                    jsonResponse.put(RIDPConstants.RESPONSE_CODE_FIELD, SUCCESS_RESPONSE_CODE);
	                } else {
	                    jsonResponse.put(RIDPConstants.RESPONSE_CODE_FIELD, ERROR_RESPONSE_CODE);
	                }
	            } catch (Exception exception) {
	                String errorMessage = "Error setting RIDP flag in SSO(wso2)";
	                LOGGER.error(errorMessage, exception);
	                throw new RIDPServiceException(errorMessage, exception); 
	            }
	        }
	    } else {
	        jsonResponse.put(RIDPConstants.RESPONSE_CODE_FIELD, ERROR_RESPONSE_CODE);
	        jsonResponse.put(RIDPConstants.RESPONSE_DESC_FIELD, "Invalid user provided");
	    }
        return jsonResponse.toJSONString();
    }

    @SuppressWarnings("unchecked")
	@Override
	public String currentUserInfo() throws RIDPServiceException {
		LOGGER.info("Inside RIDPService currentUserInfo");
		AccountUser currentUser = getLoggedInUser();
		Household household = consumerUtil.getHouseholdRecord(currentUser.getActiveModuleId());
		JSONObject response = new JSONObject();
		response.put("personGivenName", currentUser.getFirstName());
		response.put("personSurName", currentUser.getLastName());
		
		if (null != household) {
			// Set the RIDP source to MANUAL if the user is supposed to be manually verified so that UI can redirect to the manual verification screen
			if(null == household.getRidpVerified() && 
					StringUtils.isNotBlank(household.getRidpSource()) && 
					household.getRidpSource().equals(RIDPConstants.RIDP_VERIFY_SOURCE_MANUAL)) {
				response.put(RIDPConstants.RIDP_SOURCE, RIDPConstants.RIDP_VERIFY_SOURCE_MANUAL);
				response.put("documentsUploaded", getConsumerDocumentsUploaded(currentUser));
			// Set the RIDP source to FARS if the user is supposed to be FARS verified so that UI can redirect to the FARS screen
			} else if(null == household.getRidpVerified() && 
					StringUtils.isNotBlank(household.getRidpSource()) && 
					household.getRidpSource().equals(RIDPConstants.RIDP_VERIFY_SOURCE_FARS)) {
				DateTime currentDateTime = TSDateTime.getInstance();
				if(currentDateTime.minusDays(maxDaysInFARS).isAfter(household.getRidpDate().getTime())) {
				    response.put(RIDPConstants.ALLOW_MANUAL_VERIFICATION, true);
				}
				response.put(RIDPConstants.RIDP_SOURCE, RIDPConstants.RIDP_VERIFY_SOURCE_FARS);
				response.put(RIDPConstants.SERVICE_CONTACT_NAME, SERVICE_CONTACT_NAME);
				response.put(RIDPConstants.SERVICE_CONTACT_CALL_CENTER_NUMBER, SERVICE_CONTACT_CALL_CENTER_NUMBER);
				response.put(RIDPConstants.UI_DHS_REFERENCE_NUMBER, household.getDshReferenceNumber());
			} 
			
			if(StringUtils.isNotBlank(household.getRidpVerified()) && household.getRidpVerified().equals(RIDPConstants.HOUSEHOLD_RIDP_VERIFIED)) {
				response.put("ridpVerified", household.getRidpVerified());
			}
			
			// Show the paper override button on Contact info screen for CSR's
			if (currentUser.getDefRole().getName().contains(CSR_ROLE_NAME) || currentUser.getDefRole().getName().contains(SUPERVISOR_ROLE_NAME)) {
			    response.put("allowPaperVerification", true);
			}
			
			// Set all the data the user entered previously for UI to pre-populate it.
			if (null != household.getFirstName()) {
				response.put("personGivenName", household.getFirstName());
			}

			if (null != household.getLastName()) {
				response.put("personSurName", household.getLastName());
			}

			if (null != household.getMiddleName()) {
				response.put("personMiddleName", household.getMiddleName());
			}

			if (null != household.getNameSuffix()) {
				response.put("personNameSuffixText", household.getNameSuffix());
			}

			if (null != household.getBirthDate()) {
				response.put("personBirthDate", new SimpleDateFormat("MM/dd/yyyy").format(household.getBirthDate()));
			}

			response.put("fullTelephoneNumber", household.getPhoneNumber());
			Location location = getHouseholdLocation(household);
			if (null != location) {
				if (null != location.getAddress1()) {
					response.put("streetName", location.getAddress1());
				}

				if (null != location.getCity()) {
					response.put("locationCityName", location.getCity());
				}

				if (null != location.getState()) {
					response.put("locationStateUSPostalServiceCode",
							location.getState());
				}

				if (null != location.getZip()) {
					response.put("locationPostalCode", location.getZip());
				}

				if (location.getAddOnZip() != 0) {
					response.put("locationPostalExtensionCode",
							location.getAddOnZip());
				}
			}
		} 
		// else the user does not have any data saved in the household table so just send the first name and last name 
		return response.toJSONString();
	}

	private Location getHouseholdLocation(Household household) {
		Location contactLocation = household.getLocation();
		if(null != contactLocation && isAddressPresent(contactLocation)) {
			return contactLocation;
		} 			
		return household.getPrefContactLocation();
	}

	private boolean isAddressPresent(Location location) {
		return StringUtils.isNotBlank(location.getAddress1()) || StringUtils.isNotBlank(location.getCity()) || StringUtils.isNotBlank(location.getState()) || StringUtils.isNotBlank(location.getZip());
	}

	@SuppressWarnings("unchecked")
	private JSONArray getConsumerDocumentsUploaded(AccountUser user) {
		List<ConsumerDocument> consumerDocuments = consumerDocumentRepository.getDocumentsForHouseholdId(Long.valueOf(user.getActiveModuleId()));
		JSONArray manualVerificationFiles = new JSONArray();
		for(ConsumerDocument consumerDocument : consumerDocuments) {
			JSONObject fileObject = new JSONObject();
			fileObject.put("documentName", consumerDocument.getDocumentName());
			fileObject.put("documentType", consumerDocument.getDocumentType());
			if(null == consumerDocument.getAccepted()) {
				fileObject.put("status", "SUBMITTED");
			} else if(consumerDocument.getAccepted().equals("Y")) {
				fileObject.put("status", "ACCEPTED");
			} else if(consumerDocument.getAccepted().equals("N")) {
				fileObject.put("status", "REJECTED");
			}
			fileObject.put("comments", consumerDocument.getComments());
			fileObject.put("createdTimestamp", new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(consumerDocument.getCreatedDate()));
			manualVerificationFiles.add(fileObject);
		}
		return manualVerificationFiles;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String manualVerificationFileUpload(CommonsMultipartFile uploadedFile, String docType) throws RIDPServiceException {
        String ecmDocumentId = null;
		String fileName = null;
		String errorMessage = null;
		AccountUser user = getLoggedInUser();
		String relativePath = user.getId() + "/" + RIDPConstants.RIDP_MANUAL_VERIFICATION_DOCUMENTS;

		if(uploadedFile != null) {
			fileName = uploadedFile.getOriginalFilename();
	        try {
	        	Household household = consumerUtil.getHouseholdRecord(user.getActiveModuleId());
	        	//Upload file to ECM using the Content Management service
	        	fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(TSCalendar.getInstance().getTime()) + "." + uploadedFile.getOriginalFilename();
	        	ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
	        	ecmDocumentId = ecmService.createContent(relativePath,  fileName, uploadedFile.getBytes()
	        			,ECMConstants.Eligibility.DOC_CATEGORY , ECMConstants.Eligibility.RIDP, null);
	        	ConsumerDocument consumerDocument = createConsumerDocument(user, ecmDocumentId, fileName, docType);
	        	createNewRIDPTicket(consumerDocument, user, household);
	            household.setRidpSource(RIDPConstants.RIDP_VERIFY_SOURCE_MANUAL);
	            saveHouseHold(household);
	        } catch (ContentManagementServiceException contentManagementServiceException) {
				errorMessage = "Unable to upload file to ECM";
				LOGGER.error(errorMessage, contentManagementServiceException);
				throw new RIDPServiceException(errorMessage, contentManagementServiceException);
	        }
		}
		
		JSONObject response = new JSONObject();
		response.put("responseCode", SUCCESS_RESPONSE_CODE);
		response.put("documentsUploaded", getConsumerDocumentsUploaded(user));
		return response.toJSONString();
	}
	
	private ConsumerDocument createConsumerDocument(AccountUser user, String ecmDocumentId, String fileName, String documentType) {
		ConsumerDocument consumerDocument = new ConsumerDocument();
		consumerDocument.setCreatedBy(Long.valueOf(user.getId()));
		consumerDocument.setDocumentCategory(RIDPConstants.RIDP_TKM_CATEGORY);
		consumerDocument.setDocumentName(fileName);
		consumerDocument.setDocumentType(documentType);
		consumerDocument.setTargetName(RIDPConstants.RIDP_TKM_MODULE_NAME);
		consumerDocument.setTargetId(Long.valueOf(user.getActiveModuleId()));
		consumerDocument.setEcmDocumentId(ecmDocumentId);
		consumerDocumentRepository.save(consumerDocument);
		return consumerDocument;
	}

	private void createNewRIDPTicket(ConsumerDocument consumerDocument, AccountUser user, Household household) throws RIDPServiceException {
		TkmTicketsRequest tkmRequest = new TkmTicketsRequest();
	    List<String> docLinks = new ArrayList<String>();
	    docLinks.add(consumerDocument.getEcmDocumentId());
		Integer userId = user.getId();
		Integer requesterId = null;
		tkmRequest.setCreatedBy(userId);
		tkmRequest.setLastUpdatedBy(userId);
		tkmRequest.setCategory(RIDPConstants.RIDP_TKM_CATEGORY);
		tkmRequest.setType(RIDPConstants.RIDP_TKM_TYPE); 
		tkmRequest.setSubject("Ticket submission for: " + RIDPConstants.RIDP_TKM_CATEGORY);
		tkmRequest.setComment("Ticket submission for: " + RIDPConstants.RIDP_TKM_CATEGORY);
		tkmRequest.setDescription("");
		tkmRequest.setUserRoleId(roleService.findRoleByName(RIDPConstants.INDIVIDUAL_ROLE).getId());
		if(null != household && null != household.getUser()) {
			requesterId = household.getUser().getId();
		} else {
			requesterId = userId; // If Household does not have user, then change it to logged in user (CSR).
		}
		tkmRequest.setRequester(requesterId);
		tkmRequest.setModuleId(user.getActiveModuleId());
		tkmRequest.setModuleName(RIDPConstants.RIDP_TKM_MODULE_NAME);
	    tkmRequest.setDocumentLinks(docLinks);
	    tkmRequest.setRedirectUrl(CRM_VIEWMEMBER_PATH + user.getActiveModuleId());
        Map<String,String> ticketFormPropertiesMap=new HashMap<String,String>(); 
        ticketFormPropertiesMap.put("docId", consumerDocument.getId().toString()); //This will be used to update the status of the document later when the document is approved or rejected
        tkmRequest.setTicketFormPropertiesMap(ticketFormPropertiesMap);
		TkmTickets tkmResponse = ticketMgmtUtils.createNewTicket(tkmRequest);
		LOGGER.debug("RIDP Manual verification ticket ID is " + (tkmResponse != null ? tkmResponse.getNumber() : "null"));
	}

	@SuppressWarnings("unchecked")
	@Override
	public String handleException(Throwable exception) {
		if(null == exception) {
			return "Unknown";
		}
		JSONObject exceptionJSONObj = new JSONObject();
		exceptionJSONObj.put("exception", GIRuntimeException.class.getName());
		exceptionJSONObj.put("message", "Masked");
		exceptionJSONObj.put("cause", "Runtime Exception please refer server logs");
		LOGGER.error(exception);
		return exceptionJSONObj.toJSONString();
	}
	
	private AccountUser getLoggedInUser() throws RIDPServiceException {
		AccountUser currentUser;
		String errorMessage;
		try {
			currentUser = userService.getLoggedInUser();
		} catch (InvalidUserException invalidUserException) {
			errorMessage = "Error fetching currently logged in user details";
			LOGGER.error(errorMessage, invalidUserException);
			throw new RIDPServiceException(errorMessage, invalidUserException);
		}
		return currentUser;
	}
	
	private void saveHouseHold(Household household) throws RIDPServiceException {
		String errorMessage = null;
		try {
			Household hh = consumerUtil.saveHouseholdRecord(household);
			if(null==hh) {
				throw new RIDPServiceException("Failed to save Household");
			}
		} catch (ClientProtocolException clientProtocolException) {
			errorMessage = "Unable to save to household record, http exception encountered while making API call";
			LOGGER.error(errorMessage, clientProtocolException);
			throw new RIDPServiceException(errorMessage, clientProtocolException);
		} catch (IOException ioException) {
			errorMessage = "Unable to save to household record I/O Error";
			LOGGER.error(errorMessage, ioException);
			throw new RIDPServiceException(errorMessage, ioException);
		} catch (URISyntaxException uriSyntaxException) {
			errorMessage = "Unable to save to household record possibly because of Invalid API URL";
			LOGGER.error(errorMessage, uriSyntaxException);
			throw new RIDPServiceException(errorMessage, uriSyntaxException);
		}
	}

	@Override
	public String validateFARS(String jsonRequest) throws RIDPServiceException {
		String jsonResponse = null;
		JSONObject jsonResponseObj = null;
		try {
			jsonResponse = restTemplate.postForObject(GhixEndPoints.HubIntegrationEndpoints.FARS_URL, jsonRequest, String.class);
			if(isHubDown(jsonResponse)) {
				LOGGER.error("FARS Hub is down");
				return hubDownResponse();
			}
			jsonResponseObj = (JSONObject) jsonParser.parse(jsonResponse);
			
			JSONObject responseMetadata = (JSONObject) jsonResponseObj.get(RIDPConstants.RESPONSE_METADATA);
			String responseCode = (String) responseMetadata.get(RIDPConstants.RESPONSE_CODE_FIELD);
			if(RIDPConstants.isFarsError(responseCode)){
				LOGGER.error("FARS Validation Error, error code is "+responseCode);
				return jsonResponse;
			}
			
			AccountUser currentUser = getLoggedInUser();
			if(null != jsonResponseObj && 
			   null != jsonResponseObj.get(RIDPConstants.FARS_FINAL_DECISION_FIELD) && 
			   jsonResponseObj.get(RIDPConstants.FARS_FINAL_DECISION_FIELD).equals(RIDP_VERIFIED_CODE)) {
				
				Household household = consumerUtil.getHouseholdRecord(currentUser.getActiveModuleId());
				
				household.setRidpSource(RIDPConstants.RIDP_VERIFY_SOURCE_FARS);
				setRidpVerified(household);
				saveHouseHold(household);
				LOGGER.debug("household saved");
			}
		} catch (Exception exception) {
			LOGGER.error("Error invoking hub service for validating FARS", exception);
			return hubDownResponse();
		}
		jsonResponse.replace(RIDPConstants.RESPONSE_CODE_FIELD, RIDPConstants.UI_RESPONSE_CODE_FIELD);
		jsonResponse.replace(RIDPConstants.FINAL_DECISION_FIELD, RIDPConstants.UI_FINAL_DECISION_FIELD);
		return jsonResponse;
	}
	
    @SuppressWarnings("unchecked")
    @Override
    public String csrOverrideForPaperIdentification() throws RIDPServiceException {
        AccountUser currentUser = getLoggedInUser();
        Household household = null;
        JSONObject response = new JSONObject();
        household = consumerUtil.getHouseholdRecord(currentUser.getActiveModuleId());
        household.setRidpSource(RIDPConstants.RIDP_VERIFY_SOURCE_PAPER);
        updateRidpDate(household, false);
        setRidpVerified(household);
        saveHouseHold(household);
        response.put(RIDPConstants.RESPONSE_CODE_FIELD, SUCCESS_RESPONSE_CODE);
        return response.toJSONString();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public String pushToManualVerification() throws RIDPServiceException {
        AccountUser currentUser = getLoggedInUser();
        Household household = null;
        JSONObject response = new JSONObject();
        household = consumerUtil.getHouseholdRecord(currentUser.getActiveModuleId());
        household.setRidpSource(RIDPConstants.RIDP_VERIFY_SOURCE_MANUAL);
        updateRidpDate(household, false);
        response.put(RIDPConstants.RESPONSE_CODE_FIELD, SUCCESS_RESPONSE_CODE);
        return response.toJSONString();
    }
	

    @Override
    public Boolean isRidpRequired() throws RIDPServiceException {
        AccountUser currentUser = getLoggedInUser();
        Household household = consumerUtil.getHouseholdRecord(currentUser.getActiveModuleId());
        Boolean ridpRequired = Boolean.FALSE;

        if(household != null && RIDPConstants.HOUSEHOLD_RIDP_VERIFIED.equals(household.getRidpVerified())) {
            if(currentUser.getDefRole().getName().equals(INDIVIDUAL_ROLE) && RIDPConstants.RIDP_VERIFY_SOURCE_PAPER.equals(household.getRidpSource())) {
                household.setRidpDate(null);
                household.setRidpSource(null);
                household.setRidpVerified(null);
                saveHouseHold(household);
                ridpRequired = Boolean.TRUE;
            }
        } else {
            ridpRequired = Boolean.TRUE;  
        }
        return ridpRequired;
    }
    
	@SuppressWarnings("unchecked")
	private JSONObject removeBlankFields(String jsonString) throws RIDPServiceException {
		JSONObject jsonResponseObj = null;
		String errorMessage = null;
		try {
			jsonResponseObj = (JSONObject) jsonParser.parse(jsonString);
		} catch (ParseException parseException) {
			errorMessage = "Unable to parse json response";
			LOGGER.error(errorMessage, parseException);
			throw new RIDPServiceException(errorMessage, parseException);
		}
		
		List<String> keysToRemove = new ArrayList<String>();
		Set<String> keys = jsonResponseObj.keySet();
		for(String key : keys) {
			Object value = jsonResponseObj.get(key);
			if(null != value && StringUtils.isBlank(value.toString())) {
				keysToRemove.add(key);
			}
		}
		for(String key : keysToRemove) {
			jsonResponseObj.remove(key);
		}
		return jsonResponseObj;
	}
	
	@SuppressWarnings("unchecked")
	private String hubDownResponse() {
		JSONObject jsonResponseObj = new JSONObject();
		jsonResponseObj.put("responseCode", RIDPConstants.HUB_DOWN_RESPONSE_CODE);
		jsonResponseObj.put(RIDPConstants.RESPONSE_DESC_FIELD, RIDPConstants.HUB_DOWN_RESPONSE);
		return jsonResponseObj.toJSONString();
	}
	
	private String getFieldValue(Object formFieldData) {
		return ((null != formFieldData) ? formFieldData.toString() : null);
	}

    private void updateRidpDate(Household household, boolean nullify) {
        if(nullify) {
            household.setRidpDate(null);
        } else if(null == household.getRidpDate()) {
            household.setRidpDate(new TSDate());
        }
    }
}
