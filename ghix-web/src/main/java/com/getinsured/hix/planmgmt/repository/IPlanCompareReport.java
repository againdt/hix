package com.getinsured.hix.planmgmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.PlanCompareReport;

public interface IPlanCompareReport extends JpaRepository<PlanCompareReport, Integer> {

}
