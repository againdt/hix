import React, { Fragment } from 'react';
import { connect } from 'react-redux';

// Components

import ApplyingForHealthCoverageSummary from '../summary/ApplyingForHealthCoverageSummary';
import PageHeader from '../headers/PageHeader';
import AddressSummaryOfPerson from '../summary/AddressSummaryOfPerson';


const Page1_07_E2 = props => {

    const {
        buttonContainer : ButtonContainer,
        data,
        householdMembers
    } = props;

    /**
     *
     * @returns {React.Component} - Summary for 'Start Your Application' section
     */
    const getBloodRelationshipsFromHousehold = () => {
        const res = householdMembers.filter((item) => item.applicantPersonType === 'PTF' || item.applicantPersonType === 'PC_PTF');
        return res.length > 0 ? res[0].bloodRelationship : [];
    };

    const bloodRelationships = getBloodRelationshipsFromHousehold();

    const getHouseholdMemberSummary = () => {
        return (
            <Fragment>
                <ApplyingForHealthCoverageSummary
                    bloodRelationships={bloodRelationships}
                    additionalElement={(
                        <ButtonContainer
                            customButtonClassName="btn-small"
                            type="editButton_secondary"
                            args={[]}
                        />
                    )}
                />
            </Fragment>
        );
    };

    return (
        <Fragment>
            <div id="saPrimaryContact" className="">
                <div className="subsection">
                    <PageHeader
                        text="Household Members"
                        headerClassName={'usa-heading-alt'}
                    />
                    <div className="table-res-wrap">
                        {
                            getHouseholdMemberSummary()
                        }
                    </div>
                    <PageHeader
                        text="Household members addresses"
                        headerClassName={'usa-heading-alt'}
                    />
                    <div  className="usa-width-one-whole">
                        <AddressSummaryOfPerson
                            {...{
                                ButtonContainer,
                                data,
                                householdMembers
                            }}
                        />
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

Page1_07_E2.defaultProps = {
    name: 'Page1_07_E2'
};

export default connect(mapStateToProps)(Page1_07_E2);
