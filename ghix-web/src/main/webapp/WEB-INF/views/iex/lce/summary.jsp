<style type="text/css">
	@media print
	{
	  #non-printable { display: none; }
	  #printable { display: block; }
	}
</style>
<script type="text/ng-template" id="summary.jsp">
<div class="gutter10 jobIncome printable" ng-controller="summary">
  <h3 class="non-printable"><spring:message code="label.lce.finalreviewandconfirmation"/> <a ng-click="" href="javascript:window.print()" class="btn btn-primary pull-right"><spring:message code="label.lce.print"/></a> <a ng-click="generatePDF()"  class="btn btn-primary pull-right"><spring:message code="label.lce.download"/></a></h3>
  <div class="alert alert-info non-printable"><spring:message code="label.lce.reviewInfo"/></div>
  <div class="gutter10" ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold">
    <table role="presentation" class="table table-striped" ng-repeat="(fIndex, householdMember)  in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
      <tbody>
        <!-- HEADER WITH NAME -->
        <tr>
          <td class="primaryContact header span3">
            <h5>
              {{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}
            </h5>
          </td>
          <td class="primaryContact header">
            <h5>
            	{{getRelationShipWithPrimaryHousehold(householdMember.personId)}}
            </h5>
          </td>
        </tr>
        <!-- APPLYING FOR COVERAGE -->
        <tr>
          <td><spring:message code="label.lce.applyingforhealthcare"/></td>
          <td>
            <div ng-switch on="householdMember.applyingForCoverageIndicator">
              <div ng-switch-when="true">
                <spring:message code="label.lce.yes"/>
              </div>
              <div ng-switch-when="false">
                <spring:message code="label.lce.no"/>
              </div>
              <div ng-switch-default></div>
            </div>
          </td>
        </tr>
        <!-- SEX -->
        <tr>
          <td><spring:message code="label.lce.sex"/></td>
          <td>
            <div ng-switch on="householdMember.gender">
            <div ng-switch-when="male">
              <spring:message code="label.lce.male"/>
            </div>
            <div ng-switch-when="female">
              <spring:message code="label.lce.female"/>
            </div>
            <div ng-switch-default>
              {{ householdMember.gender | capitalize:true }}
           </div>
           </div>
          </td>
        </tr>
        <!-- SSN -->
        <tr>
          <td><spring:message code="label.lce.ssn"/></td>
          <td>
				<div ng-if="householdMember.socialSecurityCard.socialSecurityNumber=='' || householdMember.socialSecurityCard.socialSecurityNumber == undefined">
							<spring:message code="label.lce.na"/>
				</div>

				<div ng-if="householdMember.socialSecurityCard.socialSecurityNumber!= undefined && householdMember.socialSecurityCard.socialSecurityNumber!=''">
					<div ssn-Formatter-Non-Input ssn="householdMember.socialSecurityCard.socialSecurityNumber"></div>
				</div>
		  </td>
        </tr>
        <!-- NAME ON SSN -->
        <tr>
          <td>Name on SSN Card</td>
          <td>
				<div ng-if="householdMember.socialSecurityCard.socialSecurityNumber=='' || householdMember.socialSecurityCard.socialSecurityNumber == undefined">
							<spring:message code="label.lce.na"/>
				</div>
				<div ng-if="householdMember.socialSecurityCard.socialSecurityNumber != null && householdMember.socialSecurityCard.nameSameOnSSNCardIndicator">
					{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}
				</div>
				<div ng-if="householdMember.socialSecurityCard.socialSecurityNumber != null && !householdMember.socialSecurityCard.nameSameOnSSNCardIndicator">
					{{householdMember.socialSecurityCard.firstNameOnSSNCard}} {{householdMember.socialSecurityCard.middleNameOnSSNCard}} {{householdMember.socialSecurityCard.lastNameOnSSNCard}}
				</div>
		 </td>
        </tr>
        <!-- CITIZENSHIP -->
        <tr>
          <td><spring:message code="label.lce.uscitizenornational"/></td>
          <td>
				<div ng-if="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator">
		      		<spring:message code="label.lce.yes"/>
		      	</div>
		      	<div ng-if="!householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator">
					<spring:message code="label.lce.no"/>
			    </div>
		  </td>
        </tr>
        <!-- ADDRESS -->
        <tr>
          <td><spring:message code="label.lce.address"/></td>
          <td>
            <div ng-if="householdMember.livesAtOtherAddressIndicator == null || householdMember.livesAtOtherAddressIndicator==false || householdMember.livesAtOtherAddressIndicator =='no'">
              {{householdMember.householdContact.homeAddress.streetAddress1}} {{householdMember.householdContact.homeAddress.streetAddress2}} {{householdMember.householdContact.homeAddress.city}} {{householdMember.householdContact.homeAddress.state}} {{householdMember.householdContact.homeAddress.postalCode}}
            </div>
            <div ng-if="householdMember.livesAtOtherAddressIndicator==true ||  householdMember.livesAtOtherAddressIndicator =='yes'"">
              {{householdMember.otherAddress.address.streetAddress1}} {{householdMember.otherAddress.address.streetAddress2}} {{householdMember.otherAddress.address.city}} {{householdMember.otherAddress.address.state}} {{householdMember.otherAddress.address.postalCode}} </td>
            </div>
          </td>
        </tr>
        <!-- MAILING ADDRESS -->
        <tr>
          <td>Mailing Address</td>
          <td>
            {{mailingAddress.streetAddress1}} {{mailingAddress.streetAddress2}} {{mailingAddress.city}} {{mailingAddress.state}} {{mailingAddress.postalCode}}
          </td>
        </tr>
      </tbody>
    </table>
  </div>

  <!--NATIVE AMERICAN STATUS-->
  <div class="gutter10">
    <div>
       <h4><spring:message code="label.lce.moreHhInfo"/></h4>
    </div>
    <div id="nativeStatus">
      <table role="presentation" class="table table-striped" ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold">
        <tbody>
          <tr>
            <td class="primaryContact header span3" style="white-space: nowrap;">
              <h5><spring:message code="label.lce.hhMembersTribal"/></h5>
            </td>
            <td class="primaryContact header span3" style="white-space: nowrap;">
              <h5>&nbsp;<span class="aria-hidden">empty cell</span></h5>
            </td>
          </tr>
          <tr ng-repeat="(fIndex, householdMember)  in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
            <td>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}} </td>
            <td>
            <div ng-switch on="householdMember.specialCircumstances.americanIndianAlaskaNativeIndicator">
              <div ng-switch-when="true">
                <spring:message code="label.lce.yes"/>
              </div>
              <div ng-switch-when="false">
                <spring:message code="label.lce.no"/>
              </div>
              <div ng-switch-default></div>
            </div>
            </td>
          </tr>
      </table>
    </div>
  </div>

  <!-- BACK & NEXT BUTTON -->
  <dl class="dl-horizontal pull-right">
    <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
    <a class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
  </dl>
</div>
</script>