************************** Instructions to combine static template to existing notices using grunt-includes grunt task **********************

														Steps
*create folder containing static html to be included and existing notices where it needs to be added. (refer src/main/resources/notificationTemplate/mn/static_templates)
*include the static html in notices whereever needed using the following command - include "static_file_name"  
*run npm install inside src/main/resources - npm, grunt and grunt-includes gets installed
*change the working and destination directories in gruntfile.js according to your state specific directory
*from directory src/main/resources - run grunt
*the combined templates gets saved in the destination directory specified. (refer src/main/resources/notificationTemplate/mn)
*exclude the static template folder from target jar by using excludes in pom.xml


