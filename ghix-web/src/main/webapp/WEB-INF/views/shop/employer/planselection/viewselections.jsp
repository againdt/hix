<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld" prefix="util"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" type="text/css"  href="<c:url value="/resources/css/shopstyles.css"/>" />
<style>
.dialogCSS{background-color: white}
</style>

<div class="gutter10">
  <div class="row-fluid">
		<c:if test="${renewalExist=='Y'}">
	    <div class="span3" id="sidebar">
	    	<div class="header">
	      		<h4>Coverage for Employees</h4>
	      	</div>
	      	<p>The coverage selections in the <br/> two tabs to the right display what <br/> you selected for your employees</p>
	    </div><!-- #sidebar -->	
		</c:if>

      <div class="span9" id="rightpanel">
          
    <!--titlebar-->
	<%-- 	<input type="hidden" name="currentEnrollmentVal"  id="currentEnrollmentVal" value="${currentEnrollment}" />
		<input type="hidden" name="upcomingEnrollmentVal" id="upcomingEnrollmentVal" value="${upcomingEnrollment}" /> --%>
      <div class="gutter10">
			<c:if test="${renewalExist=='Y'}">
			  <div class="row-fluid bubble-nav-shop">
			     
			      <c:choose>
			        <c:when test="${employeeData.activeTabLabel == 'CURRENT' }">
			            <ul class="nav nav-tabs">
			              <li id="currentEnrollmentTab" class="active" ><a href="<c:url value="/shop/employer/planselection/viewselections?selectedEnrollmentId=${currentEnrollmentID}"/>">Current Coverage</a></li>
			              <li id="upcomingEnrollmentTab"><a href="<c:url value="/shop/employer/planselection/viewselections?selectedEnrollmentId=${upcomingEnrollmentID}"/>">Upcoming Coverage</a></li>
			            </ul>
			            <div class="alert">
			              <span id="currentEnrollmentAlert">The information below is related to your current coverage which ends on <strong><span id="currentCoverageEndDate">${employeeData.coverageEndDate}</span></strong>.</span>
			            </div>
			        </c:when>
			        <c:otherwise>
			          <ul class="nav nav-tabs">
			              <li id="currentEnrollmentTab" ><a href="<c:url value="/shop/employer/planselection/viewselections?selectedEnrollmentId=${currentEnrollmentID}"/>">Current Coverage</a></li>
			              <li id="upcomingEnrollmentTab" class="active"><a href="<c:url value="/shop/employer/planselection/viewselections?selectedEnrollmentId=${upcomingEnrollmentID}"/>">Upcoming Coverage</a></li>
			            </ul>
			            <div class="alert">
			              <span id="upcomingEnrollmentAlert">The information below is related to your upcoming coverage which begins on <strong><span id="upcomingCoverageEndDate">${employeeData.coverageStartDate}</span></strong>.</span>
			            </div>
			        </c:otherwise>
			      </c:choose>
			      
			    
			  </div>
			</c:if>
			<p></p>
			<div class="row-fluid" id="errorMsgDiv" style="display:none;"> 
				<div style="font-size: 15px; color: red">
						<!--&nbsp;<p><c:out value="${errorMsg}"></c:out><p/>-->
						&nbsp;<p><spring:message code = "label.noPlansFound"/></p>
					<br>
				</div>
			</div>
		
		<div class="row-fluid" id="dataDiv">
		<c:if test="${renewalExist!='Y'}">
          <p><spring:message code= "label.yourCovEffectiveDt"/> <span id="coverageStartDate">${employeeData.coverageStartDate}</span> </p>
        </c:if>
          <div class="header">
          	<h4><spring:message code= "label.yourSelections"/></h4>
          </div>
          <table class="table table-border-none">
	          <tr>
	            <td class="span5" scope="row"><spring:message code="label.contri"/> %</td>
	            <td><strong><spring:message code= "label.employees"/></strong></td>
	            <td><strong><span id="employerIndvContribution">${employeeData.employerIndvContribution}</span>%</strong></td>
	            
	          </tr>
	          <tr>
	            <td class="span5" scope="row">&nbsp;</td>
	            <td><strong><spring:message code= "label.dependent"/></strong></td>
	            <td><strong><span id="employerDepeContribution">${employeeData.employerDepeContribution}</span>%</strong></td>
	            
	          </tr>
	          <tr>
		          <td scope="row"><spring:message code = "label.planLevel"/></td>
		          <td><strong><span id="tierType">${employeeData.tierType}</span></strong></td>
		          <td>&nbsp;</td>
	          </tr>
	          <tr>
		          <td scope="row"><spring:message code = "label.refPlan"/></td>
		          <td><strong><span id="benchmarkPlanName">${employeeData.benchmarkPlanName}</span></strong></td>
		          <td>&nbsp;</td>
	          </tr>
          </table>
          <div class="header">
          	<h4><spring:message code = "label.monthlyContriCost"/></h4>
          </div>
          <table class="table table-border-none">
	          <tr>
	            <td class="span5" scope="row"><spring:message code = "label.contriForEmp"/></td>
	            <td><strong><span id="employerBenchmarkIndvPremium">${employeeData.employerBenchmarkIndvPremium}</span> /<spring:message code = "label.month"/> </strong> (<spring:message code = "label.basedOnRoster"/> <strong><span id="totalEmployeesCount">${employeeData.totalEmployeesCount}</span></strong> <spring:message code = "label.emp"/>)
				</td>
	          </tr>
	          <tr>
		          <td scope="row"><spring:message code = "label.contriForDependent"/></td>
		          <td><strong><span id="employerBenchmarkDeptPremium">${employeeData.employerBenchmarkDeptPremium}</span> / <spring:message code = "label.month"/></strong> (<spring:message code = "label.basedOnRoster"/> <strong><span id="totalDependentsCount">${employeeData.totalDependentsCount}</span></strong> <spring:message code = "label.dependnt"/>)</td>
	          </tr>
	          <tr>
		          <td scope="row"><spring:message code = "label.totContri"/></td>
		          <td><strong><span id="totalBenchMarkPremium">${employeeData.totalBenchMarkPremium}</span> / <spring:message code = "label.month"/></strong></td>
	          </tr>
          </table>
       
       	  <div class="header">
          	<h4><spring:message code = "label.potTaxImpact"/></h4>
          </div>
          <table class="table table-border-none">
            <tr>
			 <td class="span5" scope="row"><spring:message code = "label.estTaxCredit"/></td>
             <td><strong><span id="taxCredit">${employeeData.taxCredit}</span> / <spring:message code = "label.month"/></strong></td>
			</tr>
          </table>
          <div class="gutter10"><spring:message code = "label.estTaxPenaltyAmt"/> </div>
          </div>
        </div>
        <!--gutter10--> 
        </div>
      <!--rightpanel--> 
    </div>
    <!--row-fluid--> 
</div>
<!--main-->

<div id="waitDiv" class="modal hide fade"  tabindex="-1" role="dialog" aria-labelledby="waitingDiv" aria-hidden="true">
     <div class="modal-header"></div>
     <div class="modal-body">
       <h3 class="green"><spring:message code = "label.processingMsg"/></h3>
<img src="<c:url value="/resources/img/loader_greendots.gif"/>" alt="loader dots">
     </div>
     <div class="modal-footer"></div>
</div>

<script>
$(document).ready(function () {
    $('.info').tooltip();
});
</script>