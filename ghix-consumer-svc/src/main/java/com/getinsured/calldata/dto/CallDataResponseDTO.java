package com.getinsured.calldata.dto;

public class CallDataResponseDTO {

	private String Response;
	private String FailReason;
	
	public String getResponse() {
		return Response;
	}
	public void setResponse(String response) {
		Response = response;
	}
	public String getFailReason() {
		return FailReason;
	}
	public void setFailReason(String failReason) {
		FailReason = failReason;
	}
	
	
}
