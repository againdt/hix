package com.getinsured.hix.batch.enrollment.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

public class EnrlReconDiscrepancyReportReader implements ItemReader<String> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconDiscrepancyReportReader.class);
	String applicableId;
	int loopCount = 0;
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader ");

		ExecutionContext ec = stepExecution.getExecutionContext();
		if (ec != null) {
			if(ec.containsKey("fileId")) {
				applicableId = ec.getString("fileId");
				LOGGER.info("EnrlReconDiscrepancyReportReader : Thread Name: " + Thread.currentThread().getName()
						+ " fileId:: " + applicableId);
			}else {
				applicableId = ec.getString("hiosIssuerId");
				LOGGER.info("EnrlReconDiscrepancyReportReader : Thread Name: " + Thread.currentThread().getName()
						+ " hiosIssuerId :: " + applicableId);
			}
		}
	}
	
	@Override
	public String read() throws Exception, UnexpectedInputException,
			ParseException, NonTransientResourceException {
		if(loopCount++ == 0){
			return applicableId;
		}
		return null;
	}
}
