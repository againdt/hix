import { scrollActionTypes } from '../constants/actionTypes';

export const initialState = null;

export default (state = initialState, action) => {
    switch(action.type) {
    case scrollActionTypes.DEFINE_ELEMENT_FOR_SCROLLING:
        if(typeof action.element !== 'string') {
            throw new Error('Not a string has been passed to scrollReducer');
        }
        return action.element;
    case scrollActionTypes.CLEAR_ELEMENT_FOR_SCROLLING:
        return initialState;
    case scrollActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};
