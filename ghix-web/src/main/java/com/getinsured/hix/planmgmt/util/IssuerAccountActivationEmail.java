package com.getinsured.hix.planmgmt.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.notify.EmailService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.notify.NotificationAgent;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.util.GhixConfiguration;
import com.getinsured.hix.util.PlanMgmtConstants;

/**
 * Helper class to send Account Activation email in the creator-created flow.
 *
 * @author Deepa Selvaraj
 *
 */
@Component
public class IssuerAccountActivationEmail extends NotificationAgent {
	@Value("#{configProp.exchangename}")
	private String exchangeName;

	@Override
	public Map<String, String> getTokens(Map<String, Object> notificationContext) {
		ActivationJson activationJsonObj = (ActivationJson)notificationContext.get("ACTIVATION_JSON");
		AccountActivation accountActivation = (AccountActivation)notificationContext.get("ACCOUNT_ACTIVATION");
		Map<String,String> issuerData = new HashMap<String, String>();
		DateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
		String activationUrl = GhixConstants.APP_URL + "account/user/activation/" + accountActivation.getActivationToken();
		issuerData = activationJsonObj.getCreatedObject().getCustomeFields();
		issuerData.put("exchangename", exchangeName );
		issuerData.put("exchangeName" , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME) );
		issuerData.put("activationUrl", activationUrl );
		issuerData.put("issuerName", StringUtils.isNotEmpty(activationJsonObj.getCreatedObject().getFullName()) ? activationJsonObj.getCreatedObject().getFullName(): "");
		issuerData.put("host", GhixConstants.APP_URL);
		issuerData.put("exchangeAddress" , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL) );
		issuerData.put("exchangePhone" , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE) );
		issuerData.put(PlanMgmtConstants.SYSTEM_DATE, dateFormat.format(new TSDate()));
		return issuerData;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Map<String, String> getEmailData(Map<String, Object> notificationContext) {
		ActivationJson activationJsonObj = (ActivationJson)notificationContext.get("ACTIVATION_JSON");
		Map<String, String> data = new HashMap<String, String>();
		data.put("To", activationJsonObj.getCreatedObject().getEmailId());
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if(GhixConfiguration.STATE_CODE.equalsIgnoreCase("MS")){
			data.put("Subject", "Set up your " + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME) + " Issuer Account now" );
		}else if(!"NV".equalsIgnoreCase(stateCode)) {
			data.put("Subject", "A record has been created for you on the " + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME) );
		}
		return data;
	}
	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setNoticeTypeRepo(NoticeTypeRepository noticeTypeRepo) {
		this.noticeTypeRepo = noticeTypeRepo;
	}

	@Autowired
	public void setNoticeRepo(NoticeRepository noticeRepo) {
		this.noticeRepo = noticeRepo;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	@Autowired
	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	@Autowired
	public void setGhixDBSequenceUtil(GhixDBSequenceUtil ghixDBSequenceUtil) {
		this.ghixDBSequenceUtil = ghixDBSequenceUtil;
	}
	
	@Autowired
	public void setNoticeTemplateFactory(NoticeTemplateFactory templateFactory) {
		this.templateFactory = templateFactory;
	}
}
