package com.getinsured.hix.batch.ssap.redetermination.task;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.eligibility.renewal.service.SsapToConsiderService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * Spring batch Tasklet to mark applications as ToConsider or NotToConsider for Auto renewal of SSAP Applications. 
 * 
 * @author Dhananjay
 *  
 * 
 * 
 * This batch job is executed to pick up applications in 'ER' state and application type as 'OE' for next coverage year
 * and mark these applications as ToConsider or NotToConsider.
 */
@Deprecated
@DependsOn("dynamicPropertiesUtil")
public class ToConsider extends StepExecutionListenerSupport implements Tasklet {
	
	private static final String RENEWAL_TO_CONSIDER_BATCH = "RENEWAL_TO_CONSIDER_BATCH";
	private static final Logger LOGGER = Logger.getLogger(ToConsider.class);
	private static final String FAILURE = "failure";
	private static final String SUCCESS = "success";
	private SsapCloneApplicationService ssapCloneApplicationService;
	private GhixRestTemplate ghixRestTemplate;
	private ThreadPoolTaskExecutor taskExecutor;
	private GIMonitorService giMonitorService;
	private SsapToConsiderService ssapToConsiderService;
	
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		
		Long renewalYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
		String batchSize = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_BATCHSIZE);
		String processErrorRecords = chunkContext.getStepContext().getStepExecution().getJobParameters().getString("PROCESS_ERROR_RECORDS");
		LOGGER.info("Ssap Auto Renewal ToConsider Batch job to Process new Application for " + renewalYear);
		
		
		batchSize = chunkContext.getStepContext().getStepExecution().getJobParameters().getString("BATCH_SIZE",batchSize);
		
		Long batchSizeValue = Long.valueOf(batchSize);
				
		if(batchSizeValue == 0) {
			throw new GIException("Invalid Batch Size");
		}

		List<SsapApplication> renewalApplications = null;
		try 
		{
			// 
			if("Y".equalsIgnoreCase(processErrorRecords))
			{
				ssapToConsiderService.processErrorApplications(renewalYear, batchSizeValue);
			}
			
			renewalApplications = ssapCloneApplicationService.getAutoRenewalApplicationsForNextCoverageYear(renewalYear,batchSizeValue);
		} 
		catch (Exception giExceotion) {
			LOGGER.error("Exception in TO_CONSIDER job ",giExceotion);
			giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50007", new Date(), this.getClass().getName(), giExceotion.getLocalizedMessage(), null, giExceotion.getStackTrace().toString(), GIRuntimeException.Component.BATCH.getComponent(), null);
		}
		
		if(renewalApplications != null && renewalApplications.size() == 0) {
			String message = "No application found to renew for year : " + renewalYear;
			LOGGER.error(message);
			giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50008", new Date(), this.getClass().getName(), message, null, "No cloned application found to renew.", GIRuntimeException.Component.BATCH.getComponent(), null);
			return RepeatStatus.FINISHED;
		}
		
		if(renewalApplications != null && renewalApplications.size() > 0) {
			Set<Future<Map<String,String>>> tasks = new HashSet<Future<Map<String,String>>>(renewalApplications.size());
			for (SsapApplication ssapApplication : renewalApplications) {
				LOGGER.info("Processing Application Id = " + ssapApplication.getId() + " for renewal year " + renewalYear);
				try {
					tasks.add(taskExecutor.submit(new SsapRedeterminationProcessor(ssapApplication, renewalYear)));
					//new SsapApplicationProcessor(ssapApplication, renewalYear).cloneAndProcessSsapApplication(ssapApplication, renewalYear);
				} catch (Exception giExceotion) {
					try{
					giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50009", new Date(), this.getClass().getName(), giExceotion.getLocalizedMessage(), null, giExceotion.getStackTrace().toString(), GIRuntimeException.Component.BATCH.getComponent(), null);
					}catch(Exception ex){
							LOGGER.error("Error Processing application - "+ssapApplication.getId(), ex);
					}
				}
			}
			
			waitForAllTaskToComplete(tasks);
			ssapCloneApplicationService.logProcessData(tasks, RENEWAL_TO_CONSIDER_BATCH);
		}

		return RepeatStatus.FINISHED;

	}


	private void waitForAllTaskToComplete(Set<Future<Map<String,String>>> tasks) {
		boolean batchIsNotCompleted = true; 
		long currentNanoTime = System.nanoTime();
		long elapsedNanoTime = System.nanoTime();
		
		while(batchIsNotCompleted || (elapsedNanoTime/1000000000 >  60 * 15)) {
			boolean isAllTaskCompleted = true;
			for (Future<Map<String,String>> future : tasks) {
				if(!future.isDone()) {
					isAllTaskCompleted = false;
					break;
				}
			}
			batchIsNotCompleted = !isAllTaskCompleted;
			elapsedNanoTime = System.nanoTime() - currentNanoTime;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				LOGGER.error("Thread interrupted from sleep.");
			}
		}
	}
	
	private class SsapRedeterminationProcessor implements Callable<Map<String,String>> {
		
		private static final String EXCEPTION = "exception";
		private static final String PROCESSING_RESULT = "processingResult";
		private SsapApplication currentApplication = null;
		Long renewalYear = null;

		
		public SsapRedeterminationProcessor(SsapApplication currentApplication, Long renewalYear) {
			this.currentApplication = currentApplication;
			this.renewalYear = renewalYear;
		}


		@Override
		public Map<String,String> call() throws Exception {
			return processSsap(currentApplication, renewalYear);
		}

		private Map<String,String> processSsap(SsapApplication currentApplication, Long renewalYear) throws Exception, GIException {
			String processingResult = "ERROR";
			Map<String,String> result = new HashMap<String,String>();
			try 
			{
				LOGGER.info("Started Processing application = " + currentApplication.getId());
				result.put("applicationId", String.valueOf(currentApplication.getId()));
				processingResult = ssapToConsiderService.processSsap(currentApplication, renewalYear);
				
				
			} catch (GIException e) {
				result.put(EXCEPTION, ExceptionUtils.getFullStackTrace(e));
				giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_" + e.getErrorCode(), new Date(), this.getClass().getName(), e.getLocalizedMessage() + "\n" + e.getMessage()+"\n"+ExceptionUtils.getFullStackTrace(e), null, currentApplication.getCaseNumber(), GIRuntimeException.Component.BATCH.getComponent(), null);
				processingResult = RenewalStatus.ERROR.toString();
				
			} catch (Exception e) {
				giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50001", new Date(), this.getClass().getName(), e.getLocalizedMessage() + "\n" + e.getMessage()+"\n"+ExceptionUtils.getFullStackTrace(e), null, currentApplication.getCaseNumber(), GIRuntimeException.Component.BATCH.getComponent(), null);
				result.put(EXCEPTION, ExceptionUtils.getFullStackTrace(e));
				processingResult = RenewalStatus.ERROR.toString();
			}
			
			result.put(PROCESSING_RESULT,processingResult);
			return result;
		}
	}

	public String invokeSSAPIntegration(long applicationId) {
		String responseMessage=FAILURE;
		 LOGGER.debug("Invoking the SSAP orchestration flow");
		 try {
			 ghixRestTemplate.exchange(GhixEndPoints.SsapIntegrationEndpoints.RENEWAL_INTEGRATION_URL, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_XML, String.class, String.valueOf(applicationId));
			 //restTemplate.postForObject(GhixEndPoints.SsapIntegrationEndpoints.SSAP_INTEGRATION_URL,applicationId, String.class);
			 responseMessage = SUCCESS;
		 }catch(Exception e) {
			 LOGGER.error("Exception in invokeSSAPIntegration", e);
			 responseMessage = FAILURE;
		 }	     
	     return responseMessage;
	 }	


	public GhixRestTemplate getGhixRestTemplate() {
		return ghixRestTemplate;
	}

	public void setGhixRestTemplate(GhixRestTemplate ghixRestTemplate) {
		this.ghixRestTemplate = ghixRestTemplate;
	}


	public ThreadPoolTaskExecutor getTaskExecutor() {
		return taskExecutor;
	}


	public void setTaskExecutor(ThreadPoolTaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}

	public GIMonitorService getGiMonitorService() {
		return giMonitorService;
	}

	public void setGiMonitorService(GIMonitorService giMonitorService) {
		this.giMonitorService = giMonitorService;
	}

	public SsapCloneApplicationService getSsapCloneApplicationService() {
		return ssapCloneApplicationService;
	}


	public void setSsapCloneApplicationService(
			SsapCloneApplicationService ssapCloneApplicationService) {
		this.ssapCloneApplicationService = ssapCloneApplicationService;
	}

	public SsapToConsiderService getSsapToConsiderService() {
		return ssapToConsiderService;
	}


	public void setSsapToConsiderService(SsapToConsiderService ssapToConsiderService) {
		this.ssapToConsiderService = ssapToConsiderService;
	}

}
