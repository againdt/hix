package com.getinsured.consumer.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.consumer.repository.IHouseholdRepository;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountActivation.STATUS;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.accountactivation.repository.IAccountActivationRepository;
import com.getinsured.hix.platform.security.repository.IModuleUserRepository;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIException;

@Service("relinkHouseholdService")
@SuppressWarnings("rawtypes")
public class RelinkHouseholdServiceImpl implements RelinkHouseholdService {
	private static final String INDIVIDUAL = "INDIVIDUAL";
	private static final Logger LOGGER = LoggerFactory.getLogger(RelinkHouseholdServiceImpl.class);
	private static final String NAME = "name";
	@Autowired private IHouseholdRepository householdRepository;
	@Autowired private IModuleUserRepository iModuleUserRepository;
	@Autowired private IUserRepository iUserRepository;
	@Autowired private UserService userService;
	@Autowired private IAccountActivationRepository iAccountActivationRepository;
	
	@Override
	@Transactional
	public Map<String, Object> linkNewHousehold(Integer linkHouseholdId,
			Integer delinkHouseholdId, Integer userId) throws Exception{
		Map<String, Object> householdMap = new HashMap<String, Object>();
		try {
			LOGGER.info("Inside linkNewHousehold: with linkHouseholdId: " + linkHouseholdId + " delinkHouseholdId: "+ delinkHouseholdId + "userId: " + userId);
			Household linkHousehold = householdRepository.findOne(linkHouseholdId);
			Household delinkHousehold = householdRepository.findOne(delinkHouseholdId);
			if(null!=linkHousehold.getUser()){
				householdMap.put("oldUserId", linkHousehold.getUser().getId());
			}
			if(null!=delinkHousehold.getUser()){
				householdMap.put("newUserId", delinkHousehold.getUser().getId());
			}
			AccountUser user = iUserRepository.findOne(userId);
			if(user == null)
			{
				throw new Exception("Please select a user to relink to");
			}
			int danglingUserByHousehold = getDanglingUser(linkHousehold);
			//Actions:
			unlinkUser(delinkHousehold, userId);

			if(danglingUserByHousehold>0){
				unlinkUser(linkHousehold, danglingUserByHousehold);	
			}
			
			Household linkedHousehold = linkHouseholdToUser(linkHousehold,
					user);
			
			if(danglingUserByHousehold>0)
			{
				userService.updateIndividualUserAsDangling(danglingUserByHousehold);
			}
			if (null != linkedHousehold) {
				householdMap.put("household", linkedHousehold);
			}
		} catch (Exception e) {
			LOGGER.error("Error while linking new household to the user");
			throw e;
		}
		return householdMap;
	}

	private int getDanglingUser(Household linkHousehold) throws Exception {
		AccountUser danglingUser = linkHousehold.getUser();
		int danglingUserByHousehold  = -1;
		if(danglingUser!=null)
		{
		  danglingUserByHousehold  = linkHousehold.getUser().getId();
		}
		else{
			danglingUserByHousehold  = getModuleUserIdForHouseHold(linkHousehold);	
		}
		
		return danglingUserByHousehold;
	}

	private int getModuleUserIdForHouseHold(Household linkHousehold) throws Exception {
		List<ModuleUser> moduleUsers  = iModuleUserRepository.findModuleUsers(linkHousehold.getId(), "individual");
		//for individual we should have only one module user for the given moodule Id.
	if(moduleUsers == null || moduleUsers.size()==0)
	{
		return -1;
	}
	else if(moduleUsers.size()>1)
	{
		throw new Exception("More than one user found. Cannot delink");
	}
			return moduleUsers.get(0).getUser()!=null?moduleUsers.get(0).getUser().getId():-1;
	}

	private Household linkHouseholdToUser(Household linkHousehold, AccountUser user) throws GIException {
		LOGGER.info("Inside linkHouseholdToUser: with linkHousehold: "+linkHousehold+" user: "+user);
		ModuleUser mUser = userService.createModuleUser(linkHousehold.getId(), "individual", user, true);
		LOGGER.info("Created module user: " + ((mUser !=null) ? mUser.toString() : StringUtils.EMPTY));
		linkHousehold.setUser(user);
		linkHousehold.setEmail(user.getEmail());
		householdRepository.save(linkHousehold);
		return householdRepository.save(linkHousehold);
	}

	@Transactional
	private Household unlinkUser(Household delinkHousehold, Integer userId) {
		LOGGER.info("Inside unlinkUser: with linkHousehold: "+delinkHousehold+" userId: "+userId);
		
		ModuleUser moduleuser = iModuleUserRepository.findModuleUser(delinkHousehold.getId(), "individual", userId);
		if(moduleuser != null) {
			iModuleUserRepository.delete(moduleuser);
		} 
		
		if(moduleuser == null)
		{
			LOGGER.error("Unable to get the module user for household id: " + delinkHousehold.getId());
		}
		delinkHousehold.setEmail(null);
		delinkHousehold.setUser(null);
		return householdRepository.save(delinkHousehold);
	}
	
	

	@Override
	public Map<String, Object> linkNewHousehold(Integer linkHouseholdId,
			Integer delinkHouseholdId, Integer userId,
			Integer loggedInUserId) throws Exception {
		Map<String, Object> householdMap = new HashMap<String, Object>();
		try {
			LOGGER.info("Inside linkNewHousehold: with linkHouseholdId: " + linkHouseholdId + " delinkHouseholdId: "+ delinkHouseholdId + "userId: " + userId);
			Household linkHousehold = householdRepository.findOne(linkHouseholdId);
			Household delinkHousehold = householdRepository.findOne(delinkHouseholdId);
			if (null != loggedInUserId) {
				linkHousehold.setUpdatedBy(loggedInUserId);
				delinkHousehold.setUpdatedBy(loggedInUserId);
			}
			if(null!=linkHousehold.getUser()){
				householdMap.put("oldUserId", linkHousehold.getUser().getId());
			}
			if(null!=delinkHousehold.getUser()){
				householdMap.put("newUserId", delinkHousehold.getUser().getId());
			}
			AccountUser user = iUserRepository.findOne(userId);
			if(user == null)
			{
				throw new Exception("Please select a user to relink to");
			}
			int danglingUserByHousehold = getDanglingUser(linkHousehold);
			//Actions:
			unlinkUser(delinkHousehold, userId);

			if(danglingUserByHousehold>0){
				unlinkUser(linkHousehold, danglingUserByHousehold);	
			}
			
			Household linkedHousehold = linkHouseholdToUser(linkHousehold,
					user);
			
			if(danglingUserByHousehold>0)
			{
				userService.updateIndividualUserAsDangling(danglingUserByHousehold);
			}
			if (null != linkedHousehold) {
				householdMap.put("household", linkedHousehold);
			}
			
			// deactivate account activation links if exist..
			processPreviousActivation(delinkHouseholdId);
			processPreviousActivation(linkHouseholdId);
		} catch (Exception e) {
			LOGGER.error("Error while linking new household to the user");
			throw e;
		}
		return householdMap;
	}
	
	public void processPreviousActivation(Integer householdId){
		
		List<AccountActivation> prevActivations = iAccountActivationRepository.findByCreatedObjectTypeAndCreatorObjectId(INDIVIDUAL, householdId);
		
		boolean update = false;
		if(prevActivations != null){
			for( AccountActivation prevActivation : prevActivations){
				if(prevActivation != null &&  prevActivation.getStatus() == STATUS.NOTPROCESSED){
					prevActivation.setStatus(STATUS.PROCESSED);
					update = true;
				}
			}
			if (update){
				iAccountActivationRepository.save(prevActivations);
			}
			
		}
	}


}
