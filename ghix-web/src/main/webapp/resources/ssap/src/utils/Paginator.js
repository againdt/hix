import React from 'react';
import { connect } from 'react-redux';


// Components

import FlagsTooltip from './paginatorSubComponents/FlagsTooltip';
import HyperButtons from './paginatorSubComponents/HyperButtons';
import PagesTooltip from './paginatorSubComponents/PagesTooltip';
import Taxes from './paginatorSubComponents/Taxes';


const Paginator = props => {

    const {
        adjustedFlow,
        data,
        page
    } = props;

    return (
        <div
            className='usa-grid'
            style={{padding: 0,  margin: '0px 20px'}}
        >
            <div
                className='usa-width-one-sixth'
                style={{
                    fontWeight: 'bold',
                    fontSize: '1.0rem',
                    padding: '7px 0px'
                }}
            >
                Paginator: &nbsp;
                <PagesTooltip adjustedFlow={ adjustedFlow } />
                <FlagsTooltip/>
                <Taxes/>
            </div>
            <HyperButtons { ...{ adjustedFlow, page, data } }/>
        </div>
    );
};

const mapStateToProps = state => ({
    adjustedFlow: state.adjustedFlow,
    data: state.data,
    page: state.page,
    relations: state.relations
});

export default connect(mapStateToProps)(Paginator);