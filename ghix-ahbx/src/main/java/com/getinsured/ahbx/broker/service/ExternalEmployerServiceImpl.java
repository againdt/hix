package com.getinsured.ahbx.broker.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.ExternalEmployer;
import com.getinsured.ahbx.broker.repository.IExternalEmployerRepository;

/**
 * Implements {@link ExternalEmployerService} to find and save external
 * employers
 * 
 */
@Service("externalEmployerService")
@Transactional
public class ExternalEmployerServiceImpl implements ExternalEmployerService {

	@Autowired
	private IExternalEmployerRepository externalEmployerRepository;

	/**
	 * Saves external employer when external employer bob detail is fetched from AHBX
	 * 
	 * @see ExternalEmployerService#save(ExternalEmployer)
	 * 
	 * @param externalEmployer
	 * @return ExternalEmployer
	 */
	@Override
	@Transactional
	public ExternalEmployer save(ExternalEmployer externalEmployer) {
		return externalEmployerRepository.save(externalEmployer);
	}

	/**
	 * Finds external employer based on employer case id(same as external employer id)
	 * 
	 * @see ExternalEmployerService#findByExternalEmployerId(long)
	 * 
	 * @param externalEmpId
	 * @return ExternalEmployer
	 * 
	 */
	@Override
	public ExternalEmployer findByExternalEmployerId(long externalEmpId) {
		return externalEmployerRepository.getExternalEmployer(externalEmpId);
	}

	/**
	 * Updates external employer when external employer bob status is fetched from AHBX
	 * 
	 * @see ExternalEmployerService#update(ExternalEmployer)
	 * 
	 * @param externalEmployer
	 * @return ExternalEmployer
	 */
	@Override
	@Transactional
	public ExternalEmployer update(ExternalEmployer externalEmp) {
		ExternalEmployer externalEmployer = null;
		ExternalEmployer existingExternalEmployer = externalEmployerRepository.getExternalEmployer(externalEmp.getId());
		if (existingExternalEmployer != null) {
			existingExternalEmployer.setEligibilityStatus(externalEmp.getEligibilityStatus());
			existingExternalEmployer.setEnrollmentStatus(externalEmp.getEnrollmentStatus());
			existingExternalEmployer.setName(externalEmp.getName());
			existingExternalEmployer.setTotalEmployees(externalEmp.getTotalEmployees());

			externalEmployer = externalEmployerRepository.save(existingExternalEmployer);
		}
		return externalEmployer;
	}
}