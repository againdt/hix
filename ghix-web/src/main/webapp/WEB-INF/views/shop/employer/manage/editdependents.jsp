<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/editdependent.js" />"></script>

<%-- <script type="text/javascript" src="<c:url value="/resources/js/modal-template.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-address-util.js" />"></script> --%>

<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode-utils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<script type="text/javascript"> window.history.forward(); </script>
<body onunload=""></body>

<script type="text/javascript">
var ctx = "${pageContext.request.contextPath}";
var calenderimagepath = ctx+'/resources/images/calendar.gif';
var locationJSON = ${locationJSON};

var locationString = '';

var monthNames = [ "January", "February", "March", "April", "May", "June",
                   "July", "August", "September", "October", "November", "December" ];

               
if(locationJSON['zip'] != "null"){
	locationString = locationJSON['address']+", "+locationJSON['city'];
}

function validateLocation(){
	if( $("#frmhomeaddress").validate().form() ) {
		submitLocation();
		$('.modal-backdrop').detach();
	}
}

function submitLocation(){
	$('#worksite').modal('hide');
	var subUrl = '<c:url value="/shop/employer/manage/addHomeAddress">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
	$.ajax(
			{
        		type: "POST",
       			url: subUrl,
        		data: $("#frmhomeaddress").serialize(),
        		dataType:'json',
        		success: function(response,xhr){
			        if(isInvalidCSRFToken(xhr))
			          return;
        			populateLocations(response);
        		},
       			error: function(e){
        			alert("Failed to Add Address");
        		}
        });
}

jQuery.validator.addMethod("zipcheck", function(value, element, param) {
	elementId = element.id;
	var numzip = new Number($("#"+elementId).val());
    var filter = /^[0-9]+$/;
	if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() ) || numzip<=0 || (!filter.test($("#"+elementId).val()))){
		return false;
	}
	return true;
});

jQuery.validator.addMethod("dobcheck", function(value, element, param) {
	var today=new Date();
	var elementId = element.id;
	var dob = $('#'+elementId).val();
    dob_arr = dob.split(/\//);
	dob_mm = dob_arr[0]; dob_dd = dob_arr[1]; dob_yy = dob_arr[2];
	var birthDate=new Date();
	birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
	if( (today.getFullYear() - 100) >  birthDate.getFullYear() ) { return false; }
	if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) { return false; }
	if(today.getTime() < birthDate.getTime()){ return false; }
	return true;
});

jQuery.validator.addMethod("namecheck", function(value, element, param) {
	
	var elementId = element.id;
	var name = $('#'+elementId).val();
	var regex = /^[a-zA-Z]+$/;
  	if(name !=""){
		if(!regex.test(name) ){
	  		return false;	
	  	}
	}
	return true;
});

</script>
     
<div class="gutter10">
<div class="row-fluid">
	<ul class="breadcrumb txt-left">
	<li> <a href="javascript:window.location.href ='/hix/shop/employer/manage/dependents/${employee.id}?appId=${empappID}'">&lt; Back</a> <span class="divider">|</span></li>
		<li><a href="#">Employees</a> <span class="divider">/</span></li>
		<li><a href="/hix/shop/employer/manage/list">Manage List</a> <span class="divider">/</span></li>
		<li><a href="javascript:window.location.href ='/hix/shop/employer/manage/employeeinfo/${employee.id}?appId=${empappID}'">${employee.name}</a> <span class="divider">/</span></li>		
		<li> <a href="javascript:window.location.href ='/hix/shop/employer/manage/dependents/${employee.id}?appId=${empappID}'"> Dependents </a> <span class="divider">/</span></li>
		<li class="active">Edit</li>
	</ul>
	<h3 id="skip">${employee.name} Dependents</h3>
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        <div class="accordion graysidebar" id="accordion2">
          <div class="accordion-group">
            <div class="accordion-heading graydrkbg"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne"> About this Employee</a> </div>
            <div id="collapseOne" class="accordion-body collapse in">
              <div class="accordion-inner">
                <ul class="nav nav-list">
                  <li><a href="javascript:window.location.href ='/hix/shop/employer/manage/employeeinfo/${employee.id}?appId=${empappID}'">Employee Information</a></li>
                  <li class="active"><a>Dependent Information (${dependentCount})</a></li>
                  <li><a href="<c:url value="/shop/employer/manage/enrollment/${employee.id}?appId=${empappID}" />">Employee Coverage</a></li>
                <%-- <li><a href="<c:url value="/shop/employer/manage/notifications/${employee.id}" />">Notification History</a></li> --%>
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
        <h4 class="graydrkbg"><i class="icon-cog icon-white"></i> Actions</h4>
        <ul class="nav nav-list">
          <li id="addChild"><a href="javascript:Dependent.add_dependent(0,'CHILD',calenderimagepath, '', '', '', locationJSON['id'], locationString);"> <i class="icon-pencil"></i> Add Child</a></li>
          <c:if test="${employee.isMarried == 'YES'}">
          <li id="buttonSpouse"><a href="javascript:Dependent.add_dependent(0,'SPOUSE',calenderimagepath,'', '', '', locationJSON['id'], locationString);"> <i class="icon-pencil"></i> Add Spouse</a></li>
          </c:if>
        </ul>
      </div>
      <!--sidebar-->
      
	    <div class="span9" id="rightpanel">
	      <form class="form-horizontal" id="frmeditdependents" name="frmeditdependents" action="editdependentsSubmit" method="post" >
			<df:csrfToken/>
	      <input type="hidden" id="childCount" name="childCount" value="0" />
	      <input type="hidden" id="dependentCount" name="dependentCount" value="0" />
	       <input type="hidden" id="employeeStatus" name="employeeStatus" value="${employee.status}" />
	      <input type="hidden" id="id" name="id" value="${employee.id}" />
	      <input type="hidden" id="employer_id" name="employer.id" value="${employee.employer.id}"/>
	      <input type="hidden" id="empappID" name="empappID" value="${empappID}"/>
	      	<div class="header">
	        	<h4><spring:message  code='label.dependentsumm'/></h4>
	        </div>
		        <div id="mainDiv" class="gutter10">
			          <table class="table table-border-none">
			            <tr>
			              <td width="40%" align="right"><spring:message  code='label.associateemp'/></td>
			              <td><strong>${employee.name}</strong></td>
			            </tr>
			          </table>
			          
					  <div id="employeeSpouseDiv"> </div>
					  <div id="employeeChildDiv"> </div>
					  
					<div id="footerbuttons" class="form-actions margin-top50 hide">
						<input type="button" name="addButton" id="addButton"  class="btn clearForm" onclick="javascript:window.location.href ='/hix/shop/employer/manage/dependents/${employee.id}?appId=${empappID}'" value="<spring:message  code='label.cancel'/>" /> 
	        			<input type="submit" name="mainSubmitButton" id="mainSubmitButton" class="btn btn-primary margin10-l" value="<spring:message  code='label.save'/>" title="<spring:message  code='label.save'/>"/>
	        		</div>  
			    </div> <!--gutter10--> 
		    </form>
		    
		    <div id="worksite" class="modal hide fade"  tabindex="-1" role="dialog" aria-labelledby="Addanewaworksite" aria-hidden="true"><!-- modal -->
			<form class="form-horizontal margin0 addressValidator"  id="frmhomeaddress" name="frmhomeaddress">
				<df:csrfToken/>
				<div id="form-input-data" class="addressBlock">	
					<input type="hidden" name="homeaddress" id="homeaddress">
					<input type="hidden" name="address2" id="address2" value="">
					<input type="hidden" name="lat" id="lat" value="0.0"  />
					<input type="hidden" name="lon" id="lon" value="0.0"  />	
					<input type="hidden" name="rdi" id="rdi" value="" />
					<div class="modal-header">
						<button type="button" class="removeModal close"><span aria-hidden="true">&times;</span></button>
						<h3 id="worksiteHeading"><spring:message  code='label.addhomeaddress'/></h3>
					</div>
					<div class="modal-body">
						<div class="control-group">
							<label class="control-label" for="address1"><spring:message  code='label.street'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> </label>
							<div class="controls">
								<input type="text" class="input-medium" name="address1" id="address1"/>
								<div id="address1_error"></div>	
								<input type="hidden" id="address1_hidden" name="address1_hidden" >
								
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="city"><spring:message  code='label.emplCity'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" class="input-medium" name="city" id="city"  />
								<div id="city_error"></div>	
								<input type="hidden" id="city_hidden" name="city_hidden" >
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="state"><spring:message  code='label.emplState'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select class="input-medium"  name="state" id="state">
									 <option value="">Select</option>
									 <c:forEach var="state" items="${statelist}">
							    		<option <c:if test="${state.code == defaultStateCode}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
									</c:forEach>
								</select>
						    	<div id="state_error"></div>	
						    	<input type="hidden" id="state_hidden" name="state_hidden" >
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="zip"><spring:message  code='label.zip'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" class="input-small zipCode" name="zip" id="zip"  maxlength="5"/>
								<div id="zip_error"></div>
								<input type="hidden" value="0.0" id="zip_hidden" name="zip_hidden" >
							</div>
						</div>
						<div class="control-group" style="">
							<label for="county" class="control-label required"><spring:message  code="label.country"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select class="input-large" name="county"  id="county">
									<option value="">Select County...</option>
								</select>
								<div id="county_error"></div>		
							</div>
						</div>
						</div>
						<div class="modal-footer">
							<input type="button" class="btn removeModal clearForm" aria-hidden="true" id="cancelBtnModal" value="<spring:message  code='label.cancel'/>" />
							<input type="button" name="submitbutton" id="submitbutton" onClick="javascript:validateLocation();" class="btn btn-primary" value="<spring:message  code='label.ok'/>" />
						</div>
					</div>
				</form>
			</div><!-- /modal -->
	      </div><!--rightpanel--> 
    </div><!--row-fluid--> 
  </div> <!--row-fluid top-->
  </div> <!--gutter10--> 
  
  <script type="text/javascript">
  var locationsObj =  new Array();
  var empDetailsJSON = ${empDetailsJSON};
  
  $('#worksiteLink').live('click',function(){
	  $('#frmhomeaddress').trigger("reset");
	});
 
function postPopulateIndex(zipCodeValue, zipId){
	//console.log(zipCodeValue, zipId);
	//alert(" called parent's getCounty with values " + indexValue + " "+zipCodeValue); 
	jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
	getCountyList( zipCodeValue, '' );
}
  
  for(var i=0;i<empDetailsJSON.length;i++){
	    var obj = empDetailsJSON[i];
	    var locationid = obj['locationid'];
	    var locationName = obj['location'];
	    
	    locationsObj[locationid] = locationName;
  }
  if(locationString != ''){
  	locationsObj[locationJSON['id']] = locationString;
  }
  
  function populateLocations(locationJSON){
	  //clearCounty();
    var obj = locationJSON;
    
    //console.log(obj , locationJSON, $('#'+ $('#homeaddress').val()), $('#homeaddress').val())
    $('#'+ $('#homeaddress').val()).prepend('<option value="'+obj['id']+'">'+obj['address']+", "+obj['city']+'</option>');
    $('#'+ $('#homeaddress').val()).find('option[value="'+obj['id']+'"]').attr("selected",true);
    
    locationsObj[obj['id']] = obj['address']+", "+obj['city'];
    
	 $('.selecthomeaddress').each(function() {
			var elementid = $(this).attr('id');
			if(elementid != $('#homeaddress').val()){
				$(this).prepend('<option value="'+obj['id']+'">'+obj['address']+", "+obj['city']+'</option>');
			}
		});
  }
  
  var validator1 = $('#frmeditdependents').validate({ 
		onkeyup: false,
		onclick: false,
		onSubmit: true,
		onfocusout: false,
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		} 
	});
  

  populateDependentData(empDetailsJSON);
  
  var locationObj = new Object();
  function populateDependentData(empDetailsJSON){
	  var dependentCount = 0;
	  var employeeDependentCount = ${employee.dependentCount};
	  var isMarried = '${employee.isMarried}';
	  for(var i=0;i<empDetailsJSON.length;i++){
		    var obj = empDetailsJSON[i];
		    var type = obj['type'];
		    var firstName = obj['firstName'];
		    var lastName = obj['lastName'];
		    var dob = obj['dob'];
		    var locationid = obj['locationid'];
		    var location = obj['location'];
		    var id = obj['id'];
		    
		    Dependent.add_dependent(id,type, calenderimagepath, firstName, lastName, dob, locationid, location);
		    dependentCount++;
	  }
	  
	  if(dependentCount == 0 && employeeDependentCount > 0){
		  if(isMarried == 'YES'){
			  Dependent.add_dependent(0,'SPOUSE', calenderimagepath, '', '', '', locationJSON['id'], locationString);
			  employeeDependentCount--;
		  }
		  for(var i=0;i<employeeDependentCount;i++){
			Dependent.add_dependent(0,'CHILD', calenderimagepath, '', '', '', locationJSON['id'], locationString);  
		  }
	  }
  }

  var validator2 = $('#frmhomeaddress').validate({ 
		onkeyup: false,
		onclick: false,
		onSubmit: true,
		onfocusout: false,
		rules : {
			address1 : { required : true},
			zip : { required : true, zipcheck : true},
			city : {required: true},
			state : {required: true},
			county: {required: true}
		},
		messages : {
			address1 : { required : "<span> <em class='excl'>!</em> <spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"},
			zip : { required : "<span> <em class='excl'>!</em> <spring:message code='label.validateZip' javaScriptEscape='true'/></span>" ,
				zipcheck: "<span> <em class='excl'>!</em><spring:message code='label.validateempvzip' javaScriptEscape='true'/></span>"
					 },
			city : {required : "<span> <em class='excl'>!</em> <spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
			state : { required: "<span> <em class='excl'>!</em> <spring:message code='label.validateState' javaScriptEscape='true'/></span>"},
			county : { required: "<span> <em class='excl'>!</em> <spring:message code='label.validatecounty' javaScriptEscape='true'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		} 
	}); 

  
  $('.datepick').each(function() {
		$(this).datepicker({
			showOn : "button",
			buttonImage : calenderimagepath,
			buttonImageOnly : true
		});
	});
  
	function getCountyList(zip, county) {
		
		var subUrl = '<c:url value="/shop/employer/manage/addCounties">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';
		$.ajax({
			type : "POST",
			url : subUrl,
			data : {zipCode : zip}, 
			dataType:'json',
			success : function(response,xhr) {
		        if(isInvalidCSRFToken(xhr))
		          return;
				populateCounties(response, county);
			},error : function(e) {
				alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
			}
		});
	}

	function populateCounties(response, county) {
		
        var optionsstring = '<option value="">Select County...</option>';
			var i =0;
			$.each(response, function(key, value) {
				
				var optionVal = key+'#'+value;
				var options = '<option value="'+optionVal+'"' +'>'+ key +'</option>';
				optionsstring = optionsstring + options;
				i++;
			});
			$('#county').html(optionsstring);
	} // end populateCounties
  
  /*focus out event for the text input to call the lightbox*/
	/* $('#address1').focusin(function(e) {
		 if($('#zip').val().length >= 5){
		 	getCountyList($('#zip').val(), '');
		 }   
	}); */
	
   /* $('.zipCode').focusout(function(e) {
		
		var address1_e='address1'; var address2_e='address2'; var city_e= 'city'; var state_e='state'; var zip_e='zip';
		var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
		
		var model_address1 = address1_e + '_hidden' ;
		var model_address2 = address2_e + '_hidden' ;
		var model_city = city_e + '_hidden' ;
		var model_state = state_e + '_hidden' ;
		var model_zip = zip_e + '_hidden' ;
		
		var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
		if(($('#'+ address1_e).val() != "Address Line 1")&&($('#'+ city_e).val() != "City")&&($('#'+ state_e).val() !="State")&&($('#'+ zip_e).val() != "Zip Code")){
			if(($('#'+ address2_e).val())==="Address Line 2"){
				$('#'+ address2_e).val('');
			}	
			/*viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
				$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
				idsText);	
		}
		
		getCountyList($('#zip').val(), '');
	}); */
  
	/* $('#cancelBtnModal, #crossClose').click(function(){
		clearCounty();
		$('.modal-backdrop').remove();
	});  
	
	function clearCounty(){
		$('#county').html('');
		$('#county').html('<option value="">Select County...</option>');
	}
	
	$('#worksite').on('hidden', function () {
		$('.modal-backdrop').remove();
	}); */
	
	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$('.error label').hide();
			$('.modal-body input').val('');
		}  
	});
	
	$('li#addChild a').click(function(){
	    if($('#employeeChildDiv > .gutter10').length > 0){
	    	$("body, html").animate({ 
	    		scrollTop: ($( $('#employeeSpouseDiv') ).offset().top * (($('#employeeChildDiv > .gutter10').length) + 1))
		    }, 600);
	    }else{
	    	$("body, html").animate({ 
	    		scrollTop: $( $('#employeeSpouseDiv') ).offset().top
		    }, 600);
	    }
	});
  </script>
