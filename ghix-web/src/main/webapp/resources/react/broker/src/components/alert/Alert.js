import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Alert = ({ heading, hideIcon, variation, className, role, children }) => {
  let header = '';
  if (heading) {
    header = <p class="gutter10 btm-margin-0"><strong className="ds-c-alert__heading">{heading}</strong></p>;
  }

  const classes = classNames(
    'ds-c-alert',
    hideIcon && 'ds-c-alert--hide-icon',
    variation && `ds-c-alert--${variation}`,
    className
  );

  return (
    <div className={classes} role={role}>
      <div className="ds-c-alert__body">
        {header}
        {children}
      </div>
    </div>
  );
};

Alert.propTypes = {
  children: PropTypes.node.isRequired,
  heading: PropTypes.string,
  hideIcon: PropTypes.bool,
  /** ARIA `role` */
  role: PropTypes.oneOf(['alert', 'alertdialog']),
  variation: PropTypes.oneOf(['error', 'warn', 'success'])
};

export default Alert;
