package com.getinsured.hix.entity.enrollmententity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.entity.PopulationIndustry;

@Repository("populationIndustryRepository")
public interface IPopulationIndustryRepository extends
		JpaRepository<PopulationIndustry, Integer> {

	PopulationIndustry findById(int industryId);

	List<PopulationIndustry> findByEntityId(int entityId);

	@Modifying
	@Transactional
	@Query("delete from PopulationIndustry pi where pi.entity.id = :entityId")
	int deleteAssociatedIndustries(@Param("entityId") int entityId);
}
