package com.getinsured.eligibility.at.ref.si;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.at.ref.util.ExceptionUtil;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;

@Component("processorStepRouter")
@Scope("singleton")
public class ProcessorStepRouter {

	private static final Logger LOGGER = Logger.getLogger(ProcessorStepRouter.class);
	
	@Autowired QueuedApplicationHandler queuedApplicationHandler;
	
	@Autowired
	private ExceptionUtil exceptionUtil;
	
	/**
	 * This method will determine which STEP of the oe/qe/sep spring integration flow needs to be executed.
	 * In case of MES_BATCH - control should jump to step post cmr linking.
	 * In case of soap requests through soap ui / other apps, application creation is 1st step.
	 * Default behavior is application creation i.e step 1 of the integration. 
	 * 
	 * @param accountTransferRequest
	 * @return
	 */
	public String determineStep(AccountTransferRequestDTO accountTransferRequest) {
		LOGGER.info("DETERMINE REFERRAL PROCESSOR STEP STARTS");
		ReferralResponse referralResponse = new ReferralResponse();
		final String requester = StringUtils.trimToEmpty(accountTransferRequest.getRequester());
		referralResponse.setHeadermessage(ReferralResponse.AT_PROCESS);
		long ssapApplicationId = accountTransferRequest.getCurrentSpanApplicationId();
		try {
			if(ReferralProcessingConstants.REQUESTER_MES_BATCH.equalsIgnoreCase(requester) && !accountTransferRequest.isLCE()){
				referralResponse.setHeadermessage(ReferralResponse.MES_BATCH_PROCESS);
			}
			
			this.setAdditionalFields(accountTransferRequest, referralResponse);
			referralResponse.getData().put(ReferralProcessingConstants.ACCOUNT_TRANSFER_REQUEST_DTO, accountTransferRequest);
			referralResponse.getData().put(ReferralProcessingConstants.PROCESSOR_REQUESTER, requester);
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);


		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_DTERMINING_PROCESSOR_STEP).append(accountTransferRequest.getGiwsPayloadId()).append(ReferralProcessingConstants.REASON)
					.append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error("Process referral Step:"+errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_STEP_ROUTER);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		} 
		final String response = EligibilityUtils.marshal(referralResponse);
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("ProcessorStepRouter determineStep ends - Response is - " + response);
		}

		LOGGER.info("DETERMINE REFERRAL PROCESSOR STEP ENDS");
		return response;


	}

	
	/**
	 * 
	 * This method will set referral response map in the referral response. 
	 * In case we need to resume spring integration flow, the map is expected to have data when flow was paused.
	 * This will make sure the spring integration step has all the data it needs to act upon.
	 * @param accountTransferRequest
	 * @param referralResponse
	 */
	@SuppressWarnings("unchecked")
	private void setAdditionalFields(AccountTransferRequestDTO accountTransferRequest,
			ReferralResponse referralResponse) {
		// This method will be executed only if DTO has additional fields which needs to be set in spring integration flow.
		if(accountTransferRequest != null && accountTransferRequest.getAdditionalFieldsMapXmlString() != null){
			String referralResponseMapStr = accountTransferRequest.getAdditionalFieldsMapXmlString();
			// get the GI_WS_PAYLOAD_ID from the map
			Map<String,Object> referralResponseMap = null;
			try{
				XStream xStream = GhixUtils.getXStreamStaxObject();
				referralResponseMap = (Map<String,Object>)xStream.fromXML(referralResponseMapStr);
				referralResponse.getData().putAll(referralResponseMap);
				if(referralResponseMap != null && !referralResponseMap.isEmpty() && referralResponseMap.containsKey(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID)){
					accountTransferRequest.setCurrentSpanApplicationId((long)referralResponseMap.get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));	
				}
			}catch (Exception e) {
				LOGGER.error("EXCEPTION IN DESERIALIZING REFERRAL RESPONSE MAP FROM XML IN PROCESSOR STEP ROUTER STEP.");
			}
			if(referralResponseMap != null){
				referralResponse.getData().putAll(referralResponseMap);	
			}
			
			String mutipleEligibiltySpanEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_MULTIPLE_ELIGIBILITY_SPAN_CONFIG);
			if(Boolean.TRUE.toString().equalsIgnoreCase(mutipleEligibiltySpanEnabled)){
				queuedApplicationHandler.updateQueuedApplicationsForFurtherProcessing(accountTransferRequest, referralResponse);
			}
			
			}
		}

	}
