import { alertActionTypes } from '../constants/actionTypes';

const initialState = {
  status: false,
  alertTitle: null,
  alertMessage: null,
  alertType: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case alertActionTypes.SET_ALERT:
      return {
        status: true,
        ...action.alertData
      };
    case alertActionTypes.REMOVE_ALERT:
      return initialState;
    case alertActionTypes.CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
};
