<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<!--src="<c:url value="/resources/js/employer_location.js" />"  -->
<%
Date todaysDate = new java.util.Date();
SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
String formattedDate = formatter.format(todaysDate);
%>
	<form class="form-vertical" id="frmbrokerRenew" name="frmbrokerRenew" action="renew" method="POST">
		<div class="row-fluid">
			<div class="span3">
				<h1 class="nav-header"><a name="skip" class="skip">Registration:</a></h1>
			</div><!--  end of .span3 -->

			<div class="span9">
				<div class="page-header ">
					<h1><a name="skip" class="skip">Agent Contract With State Health Benefit Exchange</a></h1>
					<br />
				</div>

				<fieldset>
					<div class="gutter10">
						<div class="control-group">
							<label for="attestmessage" class="required control-label"><spring:message  code="label.attestmessage"/></label>
									<div class="controls">
										<textarea rows="15" cols="20" class="span12" id="disclaimer" readonly='readonly'>This document serves as an Agreement between the State of STATE Health Benefit Exchange and the Certified Agent. The Agent will agree to comply with all the policies and code of conduct of the State Health Benefit Exchange.
- Ensure the confidentiality of all applications, records and information received in written, graphic, oral or other tangible forms and to perform enrollment assistance,
- Never divulge to any unauthorized person, any information obtained while assisting individuals with their applications, or information obtained in conjunction with a referral,
- Never coach or recommend one plan/provider over another,
- Never invite or influence an employee or their dependents to separate from employer-based group health coverage, or arrange for this to occur,
- Comply with Managed Risk Medical Insurance Board and Department of Health Services fraud prevention policies and safeguards against fraudulent actions,
No provision of this Agreement shall be considered waived, amended, or modified by either party without prior written and signed authorization from State of State</textarea>
								</div>	
							</div>

							<div class="control-group">
								<div class="controls">
									<label for="attestmessageconfirm" class="required control-label checkbox">
										<input type="checkbox" name="attestmessageconfirm" id="attestmessageconfirm" />
										<spring:message  code="label.attestmessageconfirm"/>
								</label>
								<div id="attestmessageconfirm_error"></div>
							</div>	
						</div>

						<div class="control-group">
							<label for="esignName" class="required control-label"><spring:message  code="label.brkEsignName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="esignature" id="esignature" value="" class="input-xlarge" size="30">
								<input type="text" name="applicationDate" id="applicationDate" value="<%=formattedDate%>" class="xlarge" size="30" onkeyup="" readonly='readonly'>
								<div id="esignature_error"></div>
							</div>
						</div>

					</div>

					<div class="form-actions">
						<input type="button" name="continue" id="continue" title="<spring:message  code='label.signupBtn'/>" onClick="javascript:validateRenewForm();" value="<spring:message  code='label.signupBtn'/>" class="btn btn-primary" /> 
					</div>
				</fieldset>
			</div><!--  end of .span9 -->
		</div><!--  end of .row-fluid -->
	</form>

	<script type="text/javascript">
	var validator = $("#frmbrokerRenew").validate({ 
		rules : {
			esignature : { required : true},
			attestmessageconfirm : { required : true}
		},
		messages : {
			esignature : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEsignName' javaScriptEscape='true'/></span>"},
			attestmessageconfirm : { required : "<span> <em class='excl'>!</em>Please agree to the disclaimer.</span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		} 
	});
	
	function validateRenewForm(){
		if($("#frmbrokerRenew").validate().form() ) {
			var isChecked = $('#attestmessageconfirm').attr('checked')?true:false;
			if(!isChecked){
				error = "Bad";
				$('#attestmessageconfirm_error').html(error);
			 	return false;
			} else {
				$("#frmbrokerRenew").submit();
			}
		}
	}
	</script>