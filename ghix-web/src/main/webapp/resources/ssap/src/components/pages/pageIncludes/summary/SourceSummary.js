import React, {Fragment} from 'react';
import currencyTools from '../../../../utils/currencyTools';

// Utils


const SourceSummary = props => {

    // TODO Waiver for <NULL> as value for income/deduction type is added temporarily
    // TODO until we find the reason why we are getting it in production
    // TODO regarding line: <td>{ (is => is ? is.text : 'Unknown source')(dropdownSourcesList.find(is => is.value === source.type)) }</td>

    const {
        buttonContainer: ButtonContainer,
        sources,
        member,
        dropdownSourcesList,
        dropdownFrequencyList,
        index = undefined,
        summaryType
    } = props;

    return (
        <Fragment>
            <div className='table-res-wrap'>
                <table
                    className='usa-table-borderless'
                >
                    <thead>
                        <tr>
                            <th scope='col'>{`${summaryType} Type`}</th>
                            <th scope='col'>Amount</th>
                            <th scope='col'>Frequency</th>
                            <th scope='col'>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            sources.map((source, idx) =>

                                <tr
                                    key={`${source.type}_${idx}`}
                                >
                                    <td>{ (is => is ? is.text : 'Unknown source')(dropdownSourcesList.find(is => is.value === source.type)) }</td>
                                    <td>{ currencyTools.getMoneyFormat(source.amount, true) }</td>
                                    <td> { dropdownFrequencyList.find(fr => fr.value === source.frequency).text }</td>
                                    <td>
                                        <ButtonContainer
                                            customButtonClassName="btn-small"
                                            type="editButton_secondary"
                                            args={[index ? index : idx]}
                                        />
                                        <ButtonContainer
                                            customButtonClassName="btn-small"
                                            type="removeButton"
                                            args={[member, index ? index : idx]}
                                        />
                                    </td>
                                </tr>)

                        }
                    </tbody>
                </table>
            </div>
        </Fragment>
    );
};

export default SourceSummary;
